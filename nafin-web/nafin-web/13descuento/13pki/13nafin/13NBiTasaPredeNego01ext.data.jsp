<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*,
	javax.naming.*,	
	java.text.*,
	java.math.*,
	com.netro.pdf.*,
	java.sql.*,
	com.netro.exception.*,	
	net.sf.json.JSONArray,
	net.sf.json.JSONObject,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*,
	com.netro.mandatodoc.*,	
	com.netro.descuento.BusquedaAvanzadaI,
	netropology.utilerias.*,
	java.io.FileOutputStream,
	java.io.BufferedWriter,
	java.io.OutputStreamWriter" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf"%><%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String proceso = (request.getParameter("proceso")!=null)?request.getParameter("proceso"):"";
String ic_epo = (request.getParameter("ic_epo")!=null)?request.getParameter("ic_epo"):"";
String tipoTasa = (request.getParameter("tipoTasa")!=null)?request.getParameter("tipoTasa"):"P";
String ic_moneda = (request.getParameter("ic_moneda")!=null)?request.getParameter("ic_moneda"):"";
String ic_pyme = (request.getParameter("ic_pyme")!=null)?request.getParameter("ic_pyme"):"";
String txtPuntos = (request.getParameter("txtPuntos")!=null)?request.getParameter("txtPuntos"):"";
String lstPlazo = (request.getParameter("lstPlazo")!=null)?request.getParameter("lstPlazo"):"";
String dlpuntos2 = (request.getParameter("dlpuntos2")!=null)?request.getParameter("dlpuntos2"):"";
String ldTotal2 = (request.getParameter("ldTotal2")!=null)?request.getParameter("ldTotal2"):"";
String [] tasas		= request.getParameterValues("tasas");
String [] ValorTasas = request.getParameterValues("valorTasa");
String acuse2 = (request.getParameter("acuse2")!=null)?request.getParameter("acuse2"):"";
String fechaCarga = (request.getParameter("fechaCarga")!=null)?request.getParameter("fechaCarga"):"";
String horaCarga = (request.getParameter("horaCarga")!=null)?request.getParameter("horaCarga"):"";
String recibo = (request.getParameter("recibo")!=null)?request.getParameter("recibo"):"";
String acuse3 = (request.getParameter("acuse3")!=null)?request.getParameter("acuse3"):"";
String ic_if = (request.getParameter("ic_if")!= null)?request.getParameter("ic_if"):"";

double ldPuntos	= 0,  ldValorTotalTasa = 0,  ldTotal 	= 0;
Vector lovDatos 	= null;
Vector lovRegistro = null;
int numRegistros =0;
String  infoRegresar ="", lsCveEpo  ="", lsNombre = "", 	lsCalificacion  ="",  lsCadenaTasa 	=  "", 
lsTemp ="", lsPlazo	="", lsDescripPlazo ="", ic_tasa ="",  nueValorTasa  ="", consulta ="", lsValorTasaN 	= "", lsFecha ="",
mensajeEliminacion ="", estaEliminar ="";
String valorTasaP ="";

if (txtPuntos.equals(""))
	txtPuntos = "0";
ldPuntos = Double.parseDouble(txtPuntos);
if(tipoTasa.equals("N")) tipoTasa = "NG";
StringBuffer contenidoArchivo = new StringBuffer("");
CreaArchivo archivo = new CreaArchivo();
String nombreArchivo = null;
	
HashMap datos = new HashMap();	
JSONArray registros = new JSONArray();
JSONObject 	jsonObj	= new JSONObject();

AutorizacionTasas beanTasas = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);

if (informacion.equals("catalogoEPO")  ) {

	CatalogoEPO catalogo = new CatalogoEPO();
	catalogo.setCampoClave("ic_epo");
	catalogo.setCampoDescripcion("cg_razon_social");
	infoRegresar = catalogo.getJSONElementos();	
 
}else if (informacion.equals("catalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda desc");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
 
} else if ( informacion.equals("CatalogoIF") ){
	
	JSONObject	catalogo		= new JSONObject();
	registros					= new JSONArray();
	
	lovDatos = !ic_epo.equals("") && !ic_moneda.equals("")?beanTasas.getIntermediario(ic_moneda, ic_epo):new Vector();	
	for (int i=0; i < lovDatos.size(); i++) {
			
		lovRegistro       	= (Vector) lovDatos.elementAt(i);
		String lsClaveIF  	= lovRegistro.elementAt(0).toString();
		String lsNombreIF 	= lovRegistro.elementAt(1).toString();
 
		JSONObject	registro = new JSONObject();
		registro.put("clave",		lsClaveIF	);
		registro.put("descripcion",lsNombreIF	);
		registros.add(registro);
		
	} // for

	// Enviar resultado de la operacion
	catalogo.put("success", 	new Boolean(true)						);
	catalogo.put("total",    	String.valueOf(registros.size()) );
	catalogo.put("registros",	registros			 					);
	
	infoRegresar = catalogo.toString();	
	
} else if (	informacion.equals("BusquedaAvanzada")		){
		
	String 		claveEpo 			= request.getParameter("claveEpo")		== null?"":request.getParameter("claveEpo");
	String 		rfcPyme 				= request.getParameter("rfcPyme")		== null?"":request.getParameter("rfcPyme");
	String 		nombrePyme 			= request.getParameter("nombrePyme")	== null?"":request.getParameter("nombrePyme");
	String 		numeroPyme			= request.getParameter("numeroPyme")	== null?"":request.getParameter("numeroPyme");

	String 		ic_producto_nafin	= "";
	String 		ic_pymeArg			= "";
	String      pantalla				= "";
	String  		ic_ifArg				= "";
	String    	ret_num_pyme		= "";
	
	registros							= BusquedaAvanzadaI.getProveedores(
												ic_producto_nafin,
												claveEpo,
												numeroPyme,
												rfcPyme,
												nombrePyme,
												ic_pymeArg,
												pantalla,
												ic_ifArg,
												ret_num_pyme
											);
													
	JSONObject	resultado			= new JSONObject();
	
	resultado.put("success", 		new Boolean("true") 					);
	resultado.put("total",	 		String.valueOf(registros.size()) );
	resultado.put("registros",		registros 								);
	
	infoRegresar = resultado.toString();
	
} else if (informacion.equals("BusquedaProveedor")){
	
	String  		numeroNafinElectronico 	= (request.getParameter("numeroNafinElectronico") == null)?"":request.getParameter("numeroNafinElectronico");
	HashMap 		busqueda					 	= beanTasas.buscaProveedorTasasPreferencialesNegociadas( ic_if, ic_moneda, ic_epo, numeroNafinElectronico);
 
	// Revisar si la pyme seleccionada opera con Oferta de Tasas por Montos
	boolean operaConOfertaDeTasas = false;
	if( "P".equals(tipoTasa) && busqueda.size() > 0 ){ // Solo si es tasa preferencial
		operaConOfertaDeTasas = beanTasas.tipodeTasas( (String) busqueda.get("CLAVE_PYME"),"" );
	}
	
	// Enviar resultado
	JSONObject	resultado    = new JSONObject();
	if( busqueda.size() > 0 ){
		resultado.put("encontrado",						new Boolean(true)								);
		resultado.put("clavePyme",							(String) busqueda.get("CLAVE_PYME")		);
		resultado.put("nombrePyme",						(String) busqueda.get("NOMBRE_PYME")	);
		resultado.put("operaConOfertaDeTasas",			new Boolean(operaConOfertaDeTasas)   	);
		if( operaConOfertaDeTasas ){
			resultado.put("operaConOfertaDeTasasMsg",	"El Proveedor seleccionado opera con Oferta de Tasas por Montos, es necesario borrar su parametrización para poder aplicar Tasa Preferencial.");
		}
	} else {
		resultado.put("encontrado",	new Boolean(false)							);
		resultado.put("msg",				"El nafin electronico no corresponde a una\nPyME afiliada a la EPO o no existe.");
	}
	resultado.put("success",new Boolean(true));
	
	infoRegresar = resultado.toString();
	
} else if (informacion.equals("catalogoPlazo") ) {

	CatalogoPlazos catalogo = new CatalogoPlazos();
	catalogo.setCampoClave("ic_plazo");
	catalogo.setCampoDescripcion("cg_descripcion");	
	catalogo.setClaveProducto("1");	
	catalogo.setOrden("in_plazo_dias");	
	infoRegresar = catalogo.getJSONElementos();	
 
}else if (informacion.equals("ValidaPYME")  && tipoTasa.equals("P") ) {

	boolean sihayTasas = false;
	String sihayTasasC = "N";
	sihayTasas = 	beanTasas.tipodeTasas(ic_pyme,"");	
	if(sihayTasas) { sihayTasasC ="S";  }
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("sihayTasas",sihayTasasC);	
	infoRegresar  = jsonObj.toString();

}else if (informacion.equals("Consultar")   ) {

	if(tipoTasa.equals("NG")) tipoTasa   = "N";
	
	if(
		ic_pyme.equals("") || ic_if.equals("") || tipoTasa.equals("") || ic_epo.equals("") || ic_moneda.equals("") 
	){
		throw new AppException("Insuficientes parametros");
	}
	
	if(tipoTasa.equals("P")) {
 
		lovDatos = beanTasas.obtenerTasasNafin( tipoTasa, ic_epo, ic_moneda, ic_if, ic_pyme, txtPuntos, lstPlazo, ldPuntos, tipoTasa);

		JSONObject detalle = null;
		for (int i=0; i < lovDatos.size(); i++) {
			
			lovRegistro = (Vector) lovDatos.elementAt(i);
				
			lsCveEpo  			= lovRegistro.elementAt(0).toString();
			lsNombre 			= lovRegistro.elementAt(1).toString();
			lsCalificacion 	= (lovRegistro.elementAt(2) == null)?"":lovRegistro.elementAt(2).toString();
			lsCadenaTasa 		= lovRegistro.elementAt(3).toString();
			ldValorTotalTasa	= ((Double)lovRegistro.elementAt(4)).doubleValue();
			lsTemp 				= lovRegistro.elementAt(5).toString();
			lsPlazo				= (lovRegistro.elementAt(6) == null)?"":lovRegistro.elementAt(6).toString();
			ldTotal 				= ((Double)lovRegistro.elementAt(9)).doubleValue();				
			//lsTexto.append("\\n"+ lsNombre +"|"+ lsCalificacion + "|"+ lsTemp +"|");
			//lsTexto.append(ldValorTotalTasa +"|-|"+ lsPuntos +"|"+ ldTotal);
			lsDescripPlazo 	= (lovRegistro.elementAt(7) == null)?"" :lovRegistro.elementAt(7).toString();
			ic_tasa 				= (lovRegistro.elementAt(8) == null)?"0":lovRegistro.elementAt(8).toString();
			/*lstParamTasas.add(ic_tasa);*/
			nueValorTasa 		= (lovRegistro.elementAt(11)== null)?"0":lovRegistro.elementAt(11).toString();
			if(lovRegistro.size()>12) { 
				nueValorTasa 	= (lovRegistro.elementAt(11)== null)?"0":lovRegistro.elementAt(12).toString();
			}
 
			if(proceso.equals("Procesar") ){//|| ldPuntos!=0) {
				
				ldTotal  = ldTotal;
				
			} else if (proceso.equals("Consulta") ){ //|| ldPuntos==0 && !nueValorTasa.equals("0") ) {			
					
				//ldTotal 	=  ldTotal; //Double.parseDouble((String)nueValorTasa);
				ldTotal  = ( (double) (Math.round(ldValorTotalTasa*100000)  - Math.round(Double.parseDouble(nueValorTasa)*100000))) / 100000;
				//if(ldTotal!=0) {
				ldPuntos = ( (double) (Math.round(ldValorTotalTasa*100000)) - (double)(Math.round(ldTotal*100000)) ) / 100000;
				//}
				
			}
			
			detalle = new JSONObject();
			detalle.put("EPO_RELACIONADA",	lsNombre														);		 
			detalle.put("REFERENCIA",			lsCalificacion												);		 
			detalle.put("TIPO_TASA",		 	lsCadenaTasa												);
			detalle.put("VALOR",		 			Comunes.formatoDecimal(ldValorTotalTasa,5)		);
			detalle.put("PLAZO",		 			lsDescripPlazo												);
			detalle.put("REL_MAT",		 		"-"															);
			detalle.put("PUNTOS",		 		String.valueOf(ldPuntos)								);
			detalle.put("VALOR_TASA",			Comunes.formatoDecimal(ldTotal,5)					);
			
			detalle.put("TASAS",					lsCveEpo+"|"+ldPuntos+"|"+lsPlazo+"|"+ic_tasa+"|"+ldTotal);
			detalle.put("CAD_TASA",				"\n"+ lsNombre +"|"+ lsCalificacion + "|"+ lsTemp +"|"+ldValorTotalTasa +"|-|"+ String.valueOf(ldPuntos) +"|"+ ldTotal);
			detalle.put("LD_PUNTOS",			String.valueOf(ldPuntos)								);
			/* detalle.put("LST_PARAM_TASAS",lstParamTasas												); */
			detalle.put("LD_TOTAL",				String.valueOf(ldTotal)									);
			detalle.put("CLAVE_TIPO_TASA",	"P"															); // PREFERENCIAL
			registros.add(detalle);
 
		}
 
	} if(tipoTasa.equals("N")) {
		
		lovDatos = beanTasas.obtenerTasasNafin(tipoTasa,ic_epo, ic_moneda, ic_if, ic_pyme, txtPuntos, lstPlazo, ldPuntos,tipoTasa);
		
		JSONObject detalle = null;
		for (int i= 0; i < lovDatos.size(); i++) {
			
			lovRegistro = (Vector) lovDatos.elementAt(i);	
			
			lsCveEpo  			= lovRegistro.elementAt(0).toString();
			lsNombre 			= lovRegistro.elementAt(1).toString();
			lsCalificacion 	= lovRegistro.elementAt(2).toString();
			lsCadenaTasa 		= lovRegistro.elementAt(3).toString();
			ldValorTotalTasa 	= ((Double) lovRegistro.elementAt(4)).doubleValue();
			lsFecha 				= lovRegistro.elementAt(10).toString();
			lsTemp 				= lovRegistro.elementAt(5).toString();
			lsPlazo				= lovRegistro.elementAt(6).toString(); 
			lsDescripPlazo 	= lovRegistro.elementAt(7).toString();
			ic_tasa 				= (lovRegistro.elementAt(8) == null)?"0":lovRegistro.elementAt(8).toString();
			lsValorTasaN 		= lovRegistro.elementAt(9).toString();		
			nueValorTasa 		= (lovRegistro.elementAt(11)== null)?"0":lovRegistro.elementAt(11).toString();
	
			if( lovRegistro.size() >  12 ) {
				valorTasaP = (lovRegistro.elementAt(12)==null)?"0":lovRegistro.elementAt(12).toString();
			}	
			if( lovRegistro.size() == 14 ) {
				valorTasaP = (lovRegistro.elementAt(13)==null)?"0":lovRegistro.elementAt(13).toString();
			}
			/*else{
				//valorTasaP ="0";
			}*/
			//valorTasaP = (lovRegistro.elementAt(13)==null)?"0":lovRegistro.elementAt(13).toString();
 
			if(			proceso.equals("Procesar") || ldPuntos != 0 ) {
				lsValorTasaN 	= lsValorTasaN;
			}else if (	proceso.equals("Consulta") || ldPuntos == 0 ) {
				lsValorTasaN 	= nueValorTasa;
			}	
			if(nueValorTasa.equals(valorTasaP)){
				lsValorTasaN 	= "0";
			}
			//	out.println(ldValorTotalTasa +"---"+valorTasaP);
			//ldValorTotalTasa = (double) (Math.round(ldValorTotalTasa))-Double.parseDouble((String)valorTasaP);
	   	ldValorTotalTasa = (double) (Math.round(ldValorTotalTasa)) - Math.round(Double.parseDouble(valorTasaP));	
	   		
	   	detalle = new JSONObject();
			detalle.put("EPO_RELACIONADA",	lsNombre																			);
			detalle.put("REFERENCIA",			lsCalificacion																	);
			detalle.put("TIPO_TASA",			lsCadenaTasa																	);
			detalle.put("VALOR",					Comunes.formatoDecimal(ldValorTotalTasa,5)							);
			detalle.put("PLAZO",					lsDescripPlazo																	);
			detalle.put("FECHA",					lsFecha																			);
			detalle.put("VALOR_TASA",			lsValorTasaN																	);
			
			detalle.put("TASAS",					lsCveEpo+"|"+lsFecha+"|"+lsPlazo+"|"+ic_tasa+"|"+lsValorTasaN	);
			detalle.put("CAD_TASA",				"\n"+lsNombre+"|" + lsCalificacion +"|"+ lsTemp +"|" + ldValorTotalTasa +"|"+ lsFecha);
			detalle.put("CLAVE_TIPO_TASA",	"N"																				); // NEGOCIADA
			registros.add(detalle);
				
		}
		
	}
	
	jsonObj.put("success",		new Boolean(true)					); 
	jsonObj.put("total",			new Integer(registros.size())	);
	jsonObj.put("registros",	registros							);
	infoRegresar = jsonObj.toString();	
System.err.println("infoRegresar = <" + infoRegresar+ ">");	
}else if (informacion.equals("EliminarTasas")   ) {

	// String ic_if 		= (request.getParameter("ic_if")			== null)?"":request.getParameter("ic_if");
	// String ic_moneda 	= (request.getParameter("ic_moneda")	== null)?"":request.getParameter("ic_moneda");
	// String ic_pyme 	= (request.getParameter("ic_pyme")		== null)?"":request.getParameter("ic_pyme");
	// String tipoTasa 	= (request.getParameter("tipoTasa")		== null)?"":request.getParameter("tipoTasa");
	if(tipoTasa.equals("NG")) tipoTasa   = "N";
	// Leer []tasas	
	JSONArray	arregloTasas		= JSONArray.fromObject( (request.getParameter("arregloTasas")     == null)?"[]":request.getParameter("arregloTasas")  );
	tasas									= new String[arregloTasas.size()];
	for(int i=0;i<arregloTasas.size();        i++){ tasas[i]      = arregloTasas.getString(i);      }
	// Leer []ValorTasas
	if( "N".equals(tipoTasa) ){
		JSONArray 	arregloValorTasas = JSONArray.fromObject((request.getParameter("arregloValorTasas") == null)?"[]":request.getParameter("arregloValorTasas"));
		ValorTasas							= new String[arregloValorTasas.size()];
		for(int i=0;i<arregloValorTasas.size();i++){ ValorTasas[i] = arregloValorTasas.getString(i); }
	} else {
		ValorTasas = null;
	}
	// Nombre del usuario
	String 		nombreUsuario 		= strNombre;
	// Leer ldPuntos2: Primer valor del arreglo
	JSONArray 	arregloPuntos		= JSONArray.fromObject( (request.getParameter("arregloPuntos")    == null)?"[0]":request.getParameter("arregloPuntos") );
	String 		ldPuntos2			= arregloPuntos.getString(0);
	// ldTotal2: Este valor no se ocupa
	ldTotal2 							= (request.getParameter("ldTotal2") == null)?"":request.getParameter("ldTotal2");
 
	estaEliminar = beanTasas.eliminarTasas( ic_if, ic_moneda, ic_pyme, tipoTasa, tasas, ValorTasas, nombreUsuario, ldPuntos2, ldTotal2 );
   
	if(estaEliminar.equals("E")){
		mensajeEliminacion = "Las Tasas fueron Eliminadas";
	}
	jsonObj.put("success",    new Boolean(true)  );	
	jsonObj.put("eliminacion",mensajeEliminacion );	
	
	infoRegresar  = jsonObj.toString();
 
} else if (informacion.equals("GuardarTasas")      ){
	
	// LEER PARAMETROS
	
	String 		noUsuario 			= iNoCliente;
	String 		nombreUsuario 		= strNombre;
	// [no existe] String ic_pyme 		= (request.getParameter("lsPYME") 			== null)? ""  : request.getParameter("lsPYME");
	String 		lsCveMoneda 		= ic_moneda;
	String 		lstIF 				= ic_if;
	String 		lsPYME 				= ic_pyme;
	String 		lsTipoTasa 			= tipoTasa;
	String 	 	lsCadenaEpo 		= ic_epo;
	lstPlazo 							= (request.getParameter("lstPlazo")		== null)?"":request.getParameter("lstPlazo");
	// Leer []tasas	
	JSONArray	arregloTasas		= JSONArray.fromObject( (request.getParameter("arregloTasas")        == null)?"[]":request.getParameter("arregloTasas")  );
	tasas									= new String[arregloTasas.size()];
	for(int i=0;i<arregloTasas.size();        i++){ tasas[i]      = arregloTasas.getString(i);      }
	// Leer []ValorTasas
	if( "NG".equals(lsTipoTasa) ) lsTipoTasa = "N";
	if( "N".equals( lsTipoTasa) ){
		JSONArray 	arregloValorTasas = JSONArray.fromObject((request.getParameter("arregloValorTasas") == null)?"[]":request.getParameter("arregloValorTasas"));
		ValorTasas						= new String[arregloValorTasas.size()];
		for(int i=0;i<arregloValorTasas.size();i++){ ValorTasas[i] = arregloValorTasas.getString(i); }
	} else {
		ValorTasas						= null;
	}
	// Leer ldPuntos
	JSONArray 	arregloPuntos		= JSONArray.fromObject( (request.getParameter("arregloPuntos")       == null)?"[0]":request.getParameter("arregloPuntos") );
	String 		puntos				= arregloPuntos.getString(0);
	String 		_acuse 				= "";
	ldTotal 								= 0;
 
	// OBTENER FECHA DE HOY Y HORA ACTUAL
	
	String		fechaHoy				= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String		horaActual			= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
 
	// PREPARAR VARIABLES PARA REALIZAR LA AUTENTIFICACION DE LA FIRMA
	
	//	Variables de seguridad
	String 		pkcs7 				= request.getParameter("Pkcs7");
	//String 	serial 				= "";
	String 		folioCert 			= "";
	String 		externContent 		= request.getParameter("TextoFirmado");
	char 			getReceipt 			= 'Y';
	
	// AUTENTIFICACION
	
	Seguridad 	s 			= new Seguridad();
	Acuse 		acuse 	= new Acuse(Acuse.ACUSE_IF,"1");
	if( !_serial.equals("") && externContent != null && pkcs7 != null ){
		folioCert 			= acuse.toString();	// Este numero no importa en esta pantalla!
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
			
			_acuse 						= s.getAcuse();
			beanTasas.guardarTasas(lstIF, lsCveMoneda, lsPYME, lsTipoTasa, tasas, ValorTasas,acuse.toString(),iNoUsuario,_acuse, nombreUsuario, puntos, "N");
			
			jsonObj.put("recibo",         			_acuse            ); // Recibo Número
			jsonObj.put("acuseFormateado",			acuse.formatear() ); // Número de Acuse
			jsonObj.put("fechaDeAutorizacion", 		fechaHoy 			); // Fecha de Autorización
			jsonObj.put("horaDeAutorizacion",		horaActual 			); // Hora de Autorización
			jsonObj.put("usuarioDeCaptura", 			strNombreUsuario  ); // Usuario de Captura
			jsonObj.put("lsCadenaEpo",					lsCadenaEpo			);
			jsonObj.put("lstIF",							lstIF					);
			jsonObj.put("lsCveMoneda",					lsCveMoneda			);	
			jsonObj.put("lsPYME",						lsPYME				);	
			jsonObj.put("lsTipoTasa",					lsTipoTasa			);	
			jsonObj.put("inplazos",						lstPlazo				);
			jsonObj.put("lstPlazo",						lstPlazo				);
			jsonObj.put("acuse",							acuse.toString()	);
			jsonObj.put("horaActual",					horaActual			);	
			jsonObj.put("mensajeAutentificacion",	"<b>La autentificación se llevó a cabo con éxito <b>Recibo:"+_acuse+"</b>");
			
		} else {	//autenticación fallida
			
			String _error = s.mostrarError();
			jsonObj.put("mensajeAutentificacion",	"<b>La autentificación no se llevó a cabo </b>");
			jsonObj.put("acuse",							"N"	);
			
		}
	} else { //autenticación fallida
		
		jsonObj.put("mensajeAutentificacion",	"<b>La autentificación no se llevó a cabo </b>");
		jsonObj.put("acuse",							"N"	);
		
	}
		
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
 
} else if ( informacion.equals("ConsultarAcuse")     ) {
	
	String lsCadenaEpo 		= (request.getParameter("lsCadenaEpo") == null)?"":request.getParameter("lsCadenaEpo");
	String lstIF 				= (request.getParameter("lstIF")			== null)?"":request.getParameter("lstIF");
	String lsCveMoneda 		= (request.getParameter("lsCveMoneda")	== null)?"":request.getParameter("lsCveMoneda");
	String lsPYME 				= (request.getParameter("lsPYME")		== null)?"":request.getParameter("lsPYME");
	String lsTipoTasa 		= (request.getParameter("lsTipoTasa")	== null)?"":request.getParameter("lsTipoTasa");
	lstPlazo 					= (request.getParameter("lstPlazo")		== null)?"":request.getParameter("lstPlazo");
	String acuse 				= (request.getParameter("acuse")			== null)?"":request.getParameter("acuse");
	 	
	List   tasasGuardadas	= beanTasas.obtenerTasasNafinGuardadas( lsCadenaEpo, lstIF, lsCveMoneda, lsPYME, lsTipoTasa, lstPlazo, acuse );
   
	JSONArray 	registrosTasasGuardadas	= new JSONArray();
	JSONObject  registroTasa				= null;
	for(int i=0;i<tasasGuardadas.size();i++){
		
		HashMap 	registro 		= (HashMap) tasasGuardadas.get(i);
		String 	epoRelacionada = (String) registro.get("EPO_RELACIONADA");
		String 	nombrePyme 		= (String) registro.get("NOMBRE_PYME");
		String 	referencia 		= (String) registro.get("REFERENCIA");
		String 	tipoTasa01 		= (String) registro.get("TIPO_TASA");
		String 	plazo 			= (String) registro.get("PLAZO");
		String 	valor 			= (String) registro.get("VALOR");
		String 	valorTasa 		= (String) registro.get("VALOR_TASA");
		
		registroTasa = new JSONObject();
		registroTasa.put("EPO_RELACIONADA",	epoRelacionada	);
		registroTasa.put("NOMBRE_PYME",		nombrePyme		);
		registroTasa.put("REFERENCIA",		referencia		);
		registroTasa.put("TIPO_TASA",			tipoTasa01		);
		registroTasa.put("PLAZO",				plazo				);
		registroTasa.put("VALOR",				valor				);
		registroTasa.put("VALOR_TASA",		valorTasa		);
		
		registrosTasasGuardadas.add(registroTasa);
		
	}
	
	jsonObj.put("success",	 new Boolean(true)				 );
	jsonObj.put("total", 	 new Integer(registrosTasasGuardadas.size()) );
	jsonObj.put("registros", registrosTasasGuardadas				 			);
	infoRegresar  = jsonObj.toString();
	
}else if (informacion.equals("ArchivoPDF")   ) {
	
	recibo 							= (request.getParameter("recibo")					== null)?"":request.getParameter("recibo");
	String acuseFormateado 		= (request.getParameter("acuseFormateado")		== null)?"":request.getParameter("acuseFormateado");
	String fechaDeAutorizacion = (request.getParameter("fechaDeAutorizacion")	== null)?"":request.getParameter("fechaDeAutorizacion");
	String horaDeAutorizacion	= (request.getParameter("horaDeAutorizacion")	== null)?"":request.getParameter("horaDeAutorizacion");
	String usuarioDeCaptura 	= (request.getParameter("usuarioDeCaptura")		== null)?"":request.getParameter("usuarioDeCaptura");
								
	String lsCadenaEpo 			= (request.getParameter("lsCadenaEpo") 			== null)?"":request.getParameter("lsCadenaEpo");
	String lstIF 					= (request.getParameter("lstIF")						== null)?"":request.getParameter("lstIF");
	String lsCveMoneda 			= (request.getParameter("lsCveMoneda")				== null)?"":request.getParameter("lsCveMoneda");
	String lsPYME 					= (request.getParameter("lsPYME")					== null)?"":request.getParameter("lsPYME");
	String lsTipoTasa 			= (request.getParameter("lsTipoTasa")				== null)?"":request.getParameter("lsTipoTasa");
	lstPlazo 						= (request.getParameter("lstPlazo")					== null)?"":request.getParameter("lstPlazo");
	String acuse 					= (request.getParameter("acuse")						== null)?"":request.getParameter("acuse");
 
	// Consultar tasas guardadas 	
	List   tasasGuardadas		= beanTasas.obtenerTasasNafinGuardadas( lsCadenaEpo, lstIF, lsCveMoneda, lsPYME, lsTipoTasa, lstPlazo, acuse );
   
	// Crear archivo PDF
	// archivo 			= new CreaArchivo();
	nombreArchivo 					= archivo.nombreArchivo()+".pdf";
	ComunesPDF 	pdfDoc 			= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
	// Definir parametros del encabezado
	String 		meses[] 			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String 		fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String 		diaActual    	= fechaActual.substring(0,2);
	String 		mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String 		anioActual   	= fechaActual.substring(6,10);
	String 		horaActual		= Fecha.getHoraActual("HH24:MI:SS");
	
	// Crear encabezado
	pdfDoc.encabezadoConImagenes(
		pdfDoc,
		(String) session.getAttribute("strPais"),
      ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String) session.getAttribute("sesExterno"),
      (String) session.getAttribute("strNombre"),
      (String) session.getAttribute("strNombreUsuario"),
		(String) session.getAttribute("strLogo"),
		strDirectorioPublicacion
	);
	
	// Agregar tabla con acuse de la operación
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText("\n\n",					"formas",ComunesPDF.CENTER);
	
	pdfDoc.setTable(2,50);
	
	pdfDoc.setCell("Recibo Número","celda03",ComunesPDF.CENTER);
	pdfDoc.setCell(recibo,					"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Número de Acuse","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(acuseFormateado,		"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Fecha de Autorización","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(fechaDeAutorizacion,	"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Hora de Autorización","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(horaDeAutorizacion,	"formas",ComunesPDF.RIGHT);
	
	pdfDoc.setCell("Usuario de Captura","formas",ComunesPDF.RIGHT);
	pdfDoc.setCell(usuarioDeCaptura,		"formas",ComunesPDF.RIGHT);
	
	pdfDoc.addTable();
		
	// Agregar tabla con el detalle de las tasas guardadas
	float anchos0[]={8f,9f,8f,9f,8f,9f,8f};
	pdfDoc.setTable(7,100,anchos0);

	pdfDoc.setCell("EPO Relacionada",	"formasmenB",ComunesPDF.CENTER);		
	pdfDoc.setCell("Nombre de la PyME",	"formasmenB",ComunesPDF.CENTER);	
	pdfDoc.setCell("Referencia",			"formasmenB",ComunesPDF.CENTER);	
	pdfDoc.setCell("Tipo Tasa ",			"formasmenB",ComunesPDF.CENTER,1);	
	pdfDoc.setCell("Plazo",					"formasmenB",ComunesPDF.CENTER,1,1);	
	pdfDoc.setCell("Valor",					"formasmenB",ComunesPDF.CENTER,1,1);							
	pdfDoc.setCell("Valor Tasa",			"formasmenB",ComunesPDF.CENTER,1,1);	
 
	// Agregar detalle de cada una de las tasas guardadas
	for(int i=0;i<tasasGuardadas.size();i++){
		
		HashMap 	registro 		= (HashMap) tasasGuardadas.get(i);
		
		String epoRelacionada 	= (registro.get("EPO_RELACIONADA")	== null)?"":(String) registro.get("EPO_RELACIONADA");
		String nombrePyme 		= (registro.get("NOMBRE_PYME")		== null)?"":(String) registro.get("NOMBRE_PYME");
		String referencia 		= (registro.get("REFERENCIA")			== null)?"":(String) registro.get("REFERENCIA");
		String tipoTasa01			= (registro.get("TIPO_TASA")			== null)?"":(String) registro.get("TIPO_TASA");
		String plazo 				= (registro.get("PLAZO")				== null)?"":(String) registro.get("PLAZO");
		String valor 				= (registro.get("VALOR")				== null)?"":(String) registro.get("VALOR");
		String valorTasa 			= (registro.get("VALOR_TASA")			== null)?"":(String) registro.get("VALOR_TASA");
		
		tipoTasa01 = tipoTasa01.replaceAll("&nbsp;"," ");
		
		pdfDoc.setCell(epoRelacionada+"\n ","formasmen",ComunesPDF.CENTER);	
		pdfDoc.setCell(nombrePyme+"\n ",		"formasmen",ComunesPDF.CENTER);	
		pdfDoc.setCell(referencia+"\n ",		"formasmen",ComunesPDF.CENTER);	
		pdfDoc.setCell(tipoTasa01+"\n ",		"formasmen",ComunesPDF.CENTER);	
		pdfDoc.setCell(plazo+"\n ",			"formasmen",ComunesPDF.CENTER);	
		pdfDoc.setCell(valor+"\n ",			"formasmen",ComunesPDF.CENTER);
		pdfDoc.setCell(valorTasa+"\n ",		"formasmen",ComunesPDF.CENTER);
		
	}
	
	// Finalizar documento PDF
	pdfDoc.addTable();
	pdfDoc.endDocument();
	
	// Enviar respuesta
	jsonObj.put("success", 		new Boolean(true)						);
	jsonObj.put("urlArchivo", 	strDirecVirtualTemp+nombreArchivo);
	infoRegresar  = jsonObj.toString();
	
}else if (informacion.equals("ArchivoCSV")      ) {
	
	FileOutputStream		outputStream	= null;
	BufferedWriter 		csv 				= null;
	//String 				nombreArchivo 	= null;
	//CreaArchivo 			archivo 			= null;
	
	boolean					hayError			= false;
	
	// Leer parametros de entrada
	recibo 							= (request.getParameter("recibo")					== null)?"":request.getParameter("recibo");
	String acuseFormateado 		= (request.getParameter("acuseFormateado")		== null)?"":request.getParameter("acuseFormateado");
	String fechaDeAutorizacion = (request.getParameter("fechaDeAutorizacion")	== null)?"":request.getParameter("fechaDeAutorizacion");
	String horaDeAutorizacion	= (request.getParameter("horaDeAutorizacion")	== null)?"":request.getParameter("horaDeAutorizacion");
	String usuarioDeCaptura 	= (request.getParameter("usuarioDeCaptura")		== null)?"":request.getParameter("usuarioDeCaptura");
								
	String lsCadenaEpo 			= (request.getParameter("lsCadenaEpo") 			== null)?"":request.getParameter("lsCadenaEpo");
	String lstIF 					= (request.getParameter("lstIF")						== null)?"":request.getParameter("lstIF");
	String lsCveMoneda 			= (request.getParameter("lsCveMoneda")				== null)?"":request.getParameter("lsCveMoneda");
	String lsPYME 					= (request.getParameter("lsPYME")					== null)?"":request.getParameter("lsPYME");
	String lsTipoTasa 			= (request.getParameter("lsTipoTasa")				== null)?"":request.getParameter("lsTipoTasa");
	lstPlazo 						= (request.getParameter("lstPlazo")					== null)?"":request.getParameter("lstPlazo");
	String acuse 					= (request.getParameter("acuse")						== null)?"":request.getParameter("acuse");
 
	// Consultar tasas guardadas 	
	List   tasasGuardadas		= beanTasas.obtenerTasasNafinGuardadas( lsCadenaEpo, lstIF, lsCveMoneda, lsPYME, lsTipoTasa, lstPlazo, acuse );
   
	try {
		
		// Crear archivo CSV
		// archivo 			= new CreaArchivo();
		nombreArchivo	= archivo.nombreArchivo() + ".csv";
				
		outputStream 	= new FileOutputStream(strDirectorioTemp+nombreArchivo);
		csv 				= new BufferedWriter(new OutputStreamWriter(outputStream, "Cp1252"));
					
		// Agregar tabla con acuse de la operación
		csv.write(
			"Recibo Número,"				+ recibo 					+ "\n" +
			"Número de Acuse,"			+ acuseFormateado 		+ "\n" +
			"Fecha de Autorización,"	+ fechaDeAutorizacion 	+ "\n" +
			"Hora de Autorización,"		+ horaDeAutorizacion 	+ "\n" +
			"Usuario de Captura,"		+ usuarioDeCaptura.replace(',',' ')	 + "\n" +
			"\n,\n"+
			"EPO Relacionada,Nombre de la PyME,Referencia,Tipo Tasa,Plazo,Valor,Valor Tasa\n"
		);
		
		// Agregar detalle de cada una de las tasas guardadas
		for(int i=0;i<tasasGuardadas.size();i++){
			
			HashMap 	registro 		= (HashMap) tasasGuardadas.get(i);
			
			String epoRelacionada 	= (registro.get("EPO_RELACIONADA")	== null)?"":(String) registro.get("EPO_RELACIONADA");
			String nombrePyme 		= (registro.get("NOMBRE_PYME")		== null)?"":(String) registro.get("NOMBRE_PYME");
			String referencia 		= (registro.get("REFERENCIA")			== null)?"":(String) registro.get("REFERENCIA");
			String tipoTasa01			= (registro.get("TIPO_TASA")			== null)?"":(String) registro.get("TIPO_TASA");
			String plazo 				= (registro.get("PLAZO")				== null)?"":(String) registro.get("PLAZO");
			String valor 				= (registro.get("VALOR")				== null)?"":(String) registro.get("VALOR");
			String valorTasa 			= (registro.get("VALOR_TASA")			== null)?"":(String) registro.get("VALOR_TASA");
			
			tipoTasa01 = tipoTasa01.replaceAll("&nbsp;"," ");
			
			csv.write(epoRelacionada.replace(',',' ')	); csv.write(",");
			csv.write(nombrePyme.replace(',',' ')		); csv.write(",");
			csv.write(referencia.replace(',',' ')		); csv.write(",");
			csv.write(tipoTasa01.replace(',',' ')		); csv.write(",");
			csv.write("=\""); csv.write(plazo); 			csv.write("\",");
			csv.write(valor); 									csv.write(",");
			csv.write(valorTasa.replace(',',' ')); 
			
			csv.write("\n");
			
		}
		csv.flush();
	
	} catch(Exception e) {
			
		e.printStackTrace();
		hayError = true;
		
	} finally {
			
		// Cerrar archivo
		if(csv != null ){	try { csv.close();}catch(Exception e){} }
						
	}
		
	if( hayError ) {
		jsonObj.put("success", 		new Boolean(false)					);
		jsonObj.put("msg", 			"Error al generar el archivo"		);
	} else {	
		jsonObj.put("success", 	 	new Boolean(true)						);
		jsonObj.put("urlArchivo", 	strDirecVirtualTemp+nombreArchivo);
	}
		
	infoRegresar = jsonObj.toString();
	
}
%>
<%=infoRegresar%>

