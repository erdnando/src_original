<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>

<html>
	<head>
		<title>Nafinet</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<%@ include file="/extjs.jspf" %>
		<%if(esEsquemaExtJS) {%>
		<%@ include file="/01principal/menu.jspf"%>
		<%}%>
		<script type="text/javascript" src="13CambioEstatusExt.js?<%=session.getId()%>"></script>
		<%@ include file="/00utils/componente_firma.jspf" %>
	</head>

	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%if(esEsquemaExtJS) {%>
	
	<%@ include file="/01principal/01nafin/cabeza.jspf"%>
	<div id="_menuApp"></div>
		<div id="Contcentral">	
			<%@ include file="/01principal/01nafin/menuLateralFlotante.jspf"%>	
			<div id="areaContenido"><div style="height:230px"></div></div>                                                                                     
		</div>
	</div>
	<%@ include file="/01principal/01nafin/pie.jspf"%>
	<form id='formAux' name="formAux" target='_new'></form>
	
<%}else  if(!esEsquemaExtJS) {%>
	<div id="areaContenido" align="center"></div> <!-- dentro puede ir un style="margin-left: 3px; margin-top: 3px;" -->
	<form id='formAux' name="formAux" target='_new'></form>
        <!-- Valores iniciales recibidos como parametros-->
<input type="hidden" id="hidCboEpo" value="<%=(request.getParameter("cboEpo")==null)?"":request.getParameter("cboEpo")%>">
<input type="hidden" id="hidCboMoneda" value="<%=(request.getParameter("cboMoneda")==null)?"":request.getParameter("cboMoneda")%>">
<input type="hidden" id="hidTxtFechaVencDe" value="<%=(request.getParameter("txtFechaVencDe")==null)?"":request.getParameter("txtFechaVencDe")%>">
<input type="hidden" id="hidTxtFechaVenca" value="<%=(request.getParameter("txtFechaVenca")==null)?"":request.getParameter("txtFechaVenca")%>">
<%}%>
	</body>
	
</html>