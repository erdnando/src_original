<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.descuento.*,	
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%

	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
	String strPendiente = (request.getParameter("strPendiente") != null) ? request.getParameter("strPendiente") : "N";
	String strPreNegociable = (request.getParameter("strPreNegociable") != null) ? request.getParameter("strPreNegociable") : "S";

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
	String strFirmaManc = "N",  strHash = "N";
	if("S".equals(strPreNegociable)) {
		ArrayList alParamEPO = BeanParamDscto.getParamEPO(iNoCliente, 1);
		if (alParamEPO!=null) {  
		   strFirmaManc = (alParamEPO.get(5)==null)?"N":alParamEPO.get(5).toString();
		   strHash	  	= (alParamEPO.get(6)==null)?"N":alParamEPO.get(6).toString();
		}
		if("N".equals(strFirmaManc)) {
			throw new NafinException("DSCT0018");
		}
	}
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>

<script type="text/javascript" src="13forma01Ext.js?<%=session.getId()%>"></script>

<%@ include file="/00utils/componente_firma.jspf" %>
<%@ include file="../certificado.jspf" %>

</head>


<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>

<form id='formParametros' name="formParametros">
	<input type="hidden" id="strPendiente" name="strPendiente" value="<%=strPendiente%>"/>	
	<input type="hidden" id="strPreNegociable" name="strPreNegociable" value="<%=strPreNegociable%>"/>
	
</form>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
