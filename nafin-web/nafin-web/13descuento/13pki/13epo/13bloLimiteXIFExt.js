
	function selecESTATUS(check, rowIndex, colIds){
		var  gridConsulta = Ext.getCmp('gridConsulta');	
		var store = gridConsulta.getStore();
		var reg = gridConsulta.getStore().getAt(rowIndex);
	 
		if(check.checked==true)  {
			reg.set('ESTATUS', "S");	
			reg.set('AUX_ESTATUS', "checked");
			reg.set('SELECCIONADOS', "S");
			reg.set('IC_IF_EPO_BLOQ_DES', reg.data['IC_EPO']);
		}else  {
		 reg.set('ESTATUS', "N"); 
		 reg.set('AUX_ESTATUS', "");
		 reg.set('SELECCIONADOS', "S");	
		 reg.set('IC_IF_EPO_BLOQ_DES', "S");	
		 reg.set('IC_IF_EPO_BLOQ_DES', reg.data['IC_EPO']);
		}	
		store.commitChanges();	
	}
	
	
Ext.onReady(function() {
	
	var ic_reg_ifs= [];
	var cs_activacion = [];	
	var cambios=0;
	
	function cancelar() {
		ic_reg_ifs= [];
		cs_activacion = [];	
		cambios=0;
	}
	
	function transmite_guardar(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){	
			
				var fp = Ext.getCmp('forma');
				fp.el.unmask();
				
				Ext.MessageBox.alert('Mensaje',jsonData.mensaje,function(){
				
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{  						
						
						})
					});			
				});
				
					
				
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//**********funcion para guardar*****************++
	var procGuardarTransmite = function(pkcs7, vtextoFirmar, vic_reg_ifs, vcs_activacion){
		if (Ext.isEmpty(pkcs7)) {
				Ext.getCmp("btnGuardar").enable();	
			return;	//Error en la firma. Termina...
		}else  {	
			fp.el.mask('Procesando ...', 'x-mask-loading');			
		
			Ext.getCmp("btnGuardar").disable();				
			
			Ext.Ajax.request({
				url: '13bloLimiteXIF.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion: 'GuardarBloqueo',
					pkcs7: pkcs7,
					textoFirmado: vtextoFirmar,
					ic_reg_ifs: vic_reg_ifs,
					cs_activacion: vcs_activacion
				}),
				callback: transmite_guardar
			});
		}
	}
	
	var procGuardar = function () {
	
		var  gridConsulta = Ext.getCmp('gridConsulta');
		var store = gridConsulta.getStore();		
		var  textoFirmar ="";
		
		textoFirmar ='Los siguientes registros seran modificados  \n'+
						'Intermediario Financiero, Monto L�mite Moneda Nacional,  Porcentaje de Utilizaci�n Moneda Nacional, '+
						'Monto Disponible Moneda Nacional, Monto L�mite D�lar Americano, Porcentaje de Utilizaci�n D�lar Americano,Monto '+
						'Disponible D�lar Americano, Monto Comprometido Moneda Nacional, Monto Comprometido D�lar Americano, '+
						'Monto Disponible despu�s de Comprometido Moneda Nacional,Monto Disponible despu�s de  Comprometido D�lar Americano, '+
						'Fecha Vencimiento L�nea de Cr�dito, Fecha Cambio de Administraci�n, Validar Fechas L�mite, Fecha de Bloqueo/Desbloqueo, '+ 
						'Usuario de Bloqueo/Desbloqueo , Dependencia que Bloqueo/Desbloqueo\n';

		cancelar();//limpia variables
		store.each(function(record) {
			if(record.data['SELECCIONADOS']=='S'){	
				textoFirmar +=record.data['NOMBRE_IF'] +','+ record.data['MONTO_LIMITE_MN'] +','+	record.data['PORCENTAJE_UTL_MN'] +','+ record.data['MONTO_DISP_MN'] +','+
				record.data['MONTO_LIMITE_DL'] +','+	record.data['PORCENTAJE_UTL_DL'] +','+		record.data['MONTO_DISP_DL'] +','+	record.data['MONTO_COMPROMETIDO_MN'] +','+
				record.data['MONTO_COMPROMETIDO_DL'] +','+	record.data['MONTO_DIS_DES_MN'] +','+	record.data['MONTO_DIS_DES_DL'] +','+	record.data['FECHA_VEN_LINEA'] +','+
				record.data['FECHA_CAMB_ADM'] +','+		record.data['VALIDA_FECHA_LIM'] +','+	record.data['FECHA_BLOQ_DESB'] +','+	record.data['USUARIO_BLOQ_DESB'] +','+
				record.data['DEPENDENCIA_BLOQ_DESB'] +'\n';
			
				ic_reg_ifs.push(record.data['IC_IF']);
				cs_activacion.push(record.data['ESTATUS']);
				cambios++;				
			}
			
		});
		
		
		if(cambios>0){
			/*var pkcs7 = NE.util.firmar(textoFirmar);
			if (Ext.isEmpty(pkcs7)) {
				Ext.getCmp("btnGuardar").enable();	
				return;	//Error en la firma. Termina...
			}else  {	
				fp.el.mask('Procesando ...', 'x-mask-loading');			
			
				Ext.getCmp("btnGuardar").disable();				
				
				Ext.Ajax.request({
					url: '13bloLimiteXIF.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'GuardarBloqueo',
						pkcs7: pkcs7,
						textoFirmado: textoFirmar,
						ic_reg_ifs:ic_reg_ifs,
						cs_activacion:cs_activacion
					}),
					callback: transmite_guardar
				});
		
			}*/
			NE.util.obtenerPKCS7(procGuardarTransmite, textoFirmar, ic_reg_ifs, cs_activacion);
		
		}
	}
	
	//****************************Consulta*************************+


	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var gridConsulta = Ext.getCmp('gridConsulta');	
		var el = gridConsulta.getGridEl();	
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var cm = gridConsulta.getColumnModel();
			
						
			if(store.getTotalCount() > 0) {			
				el.unmask();	
				Ext.getCmp('btnGuardar').enable();
			} else {		
				Ext.getCmp('btnGuardar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	
	var consultaData   = new Ext.data.JsonStore({ 
		root : 'registros',
		url : '13bloLimiteXIF.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},		
		fields: [
			{	name: 'IC_EPO'},
			{	name: 'IC_IF'},
			{	name: 'NOMBRE_IF'},
			{	name: 'MONTO_LIMITE_MN'},
			{	name: 'PORCENTAJE_UTL_MN'},
			{	name: 'MONTO_DISP_MN'},
			{	name: 'MONTO_LIMITE_DL'},
			{	name: 'PORCENTAJE_UTL_DL'},
			{	name: 'MONTO_DISP_DL'},
			{	name: 'MONTO_COMPROMETIDO_MN'},
			{	name: 'MONTO_COMPROMETIDO_DL'},
			{	name: 'MONTO_DIS_DES_MN'},
			{	name: 'MONTO_DIS_DES_DL'},
			{	name: 'FECHA_VEN_LINEA'},
			{	name: 'FECHA_CAMB_ADM'},
			{	name: 'VALIDA_FECHA_LIM'},
			{	name: 'ESTATUS'},
			{	name: 'DESCRIPCION_ESTATUS'},			
			{	name: 'FECHA_BLOQ_DESB'},
			{	name: 'USUARIO_BLOQ_DESB'},
			{	name: 'DEPENDENCIA_BLOQ_DESB'},
			{	name: 'SELECCIONADOS'},
			{	name: 'AUX_ESTATUS'},
			{	name: 'IC_IF_EPO_BLOQ_DES'}				
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);					
				}
			}
		}		
	});
	
	
		//esto esta en duda como manejarlo
	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: ' ', colspan: 14, align: 'center'},
					{header: '<B>Cambio de Estatus</B>', colspan: 4, align: 'center'}					
				]
			]
	});
	
	var gridConsulta = new Ext.grid.EditorGridPanel({	
		store: consultaData,
		id: 'gridConsulta',
		margins: '20 0 0 0',		
		style: 'margin:0 auto;',
		title: 'Consulta',
		clicksToEdit: 1,
		plugins: grupos,
		hidden: true,		
		columns: [	
			{
				header: 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex: 'NOMBRE_IF',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},
			{
				header: '<center> Monto L�mite <BR>Moneda Nacional',
				tooltip: 'Monto L�mite  Moneda Nacional',
				dataIndex: 'MONTO_LIMITE_MN',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: '<center> Porcentaje de Utilizaci�n <BR> Moneda Nacional',
				tooltip: 'Porcentaje de Utilizaci�n Moneda Nacional',
				dataIndex: 'PORCENTAJE_UTL_MN',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')				
			},
			{
				header: ' <center> Monto Disponible <BR> Moneda Nacional',
				tooltip: 'Monto Disponible Moneda Nacional',
				dataIndex: 'MONTO_DISP_MN',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: ' <center> Monto L�mite <BR> D�lar Americano',
				tooltip: 'Monto L�mite D�lar Americano',
				dataIndex: 'MONTO_LIMITE_DL',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: ' <center> Porcentaje de Utilizaci�n <BR> D�lar Americano',
				tooltip: 'Porcentaje de Utilizaci�n D�lar Americano',
				dataIndex: 'PORCENTAJE_UTL_DL',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')				
			},
			{
				header: ' <center> Monto Disponible <BR> D�lar Americano',
				tooltip: 'Monto Disponible D�lar Americano',
				dataIndex: 'MONTO_DISP_DL',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: ' <center> Monto Comprometido <BR> Moneda Nacional',
				tooltip: 'Monto Comprometido Moneda Nacional',
				dataIndex: 'MONTO_COMPROMETIDO_MN',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: ' <center> Monto Comprometido <BR> D�lar Americano',
				tooltip: 'Monto Comprometido D�lar Americano',
				dataIndex: 'MONTO_COMPROMETIDO_DL',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: '<center> Monto Disponible despu�s de <BR> Comprometido Moneda Nacional',
				tooltip: 'Monto Disponible despu�s de <BR> Comprometido Moneda Nacional',
				dataIndex: 'MONTO_DIS_DES_MN',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: ' <center> Monto Disponible despu�s de <BR> Comprometido D�lar Americano',
				tooltip: 'Monto Disponible despu�s de <BR> Comprometido MD�lar Americano',
				dataIndex: 'MONTO_DIS_DES_DL',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: '<center> Fecha Vencimiento <BR>  L�nea de Cr�dito',
				tooltip: 'Fecha Vencimiento L�nea de Cr�dito',
				dataIndex: 'FECHA_VEN_LINEA',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'						
			},
			{
				header: ' <center> Fecha Cambio <BR>  de Administraci�n',
				tooltip: 'Fecha Cambio de Administraci�n',
				dataIndex: 'FECHA_CAMB_ADM',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'						
			},
			{
				header: 'Validar Fechas L�mite',
				tooltip: 'Validar Fechas L�mite',
				dataIndex: 'VALIDA_FECHA_LIM',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'						
			},			
			{				
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				 hideable: false,
				width: 150,			
				resizable: true,									
				align: 'center',
				renderer: function(value, metadata, record, rowIndex, colIndex, store){
				
				if(record.data['IC_IF_EPO_BLOQ_DES']!='') {
						if(record.data['IC_IF_EPO_BLOQ_DES']!=record.data['IC_EPO'] && record.data['ESTATUS'] =='N' ){					
							return '<input id="chkESTATUS"'+ rowIndex + 'value="S" type="checkbox" DISABLED '+record.data['AUX_ESTATUS']+' onclick="selecESTATUS(this,'+rowIndex +','+colIndex+');"/>'+ " &nbsp;"+record.data['DESCRIPCION_ESTATUS'] +"&nbsp;(IF)&nbsp;";            									
						}	else {
							return '<input id="chkESTATUS"'+ rowIndex + 'value="S" type="checkbox"   '+record.data['AUX_ESTATUS']+' onclick="selecESTATUS(this,'+rowIndex +','+colIndex+');"/>'+ " &nbsp;"+record.data['DESCRIPCION_ESTATUS']+ "&nbsp;(EPO)";            															
						}
					}else  {
						return '<input id="chkESTATUS"'+ rowIndex + 'value="S" type="checkbox"   '+record.data['AUX_ESTATUS']+' onclick="selecESTATUS(this,'+rowIndex +','+colIndex+');"/>'+ " &nbsp;"+record.data['DESCRIPCION_ESTATUS']+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";            													
					}
				}
			},
			{
				header: 'Fecha de <BR>  Bloqueo/Desbloqueo',
				tooltip: 'Fecha de Bloqueo/Desbloqueo',
				dataIndex: 'FECHA_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'center'				
			},
			{
				header: '<center> Usuario de  <BR>  Bloqueo/Desbloqueo ',
				tooltip: 'Usuario de Bloqueo/Desbloqueo ',
				dataIndex: 'USUARIO_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			},			
			{
				header: '<center> Dependencia que <BR>  Bloqueo/Desbloqueo',
				tooltip: 'Dependencia que Bloqueo/Desbloqueo ',
				dataIndex: 'DEPENDENCIA_BLOQ_DESB',
				sortable: true,
				width: 150,			
				resizable: true,					
				align: 'left',
				renderer:function(value, metadata, record, rowindex, colindex, store) {
					metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
					return value;
				}
			}
		],			
		displayInfo: true,		
		emptyMsg: "No hay registros.",		
		loadMask: true,
		stripeRows: true,
		height: 400,
		width: 900,
		align: 'center',
		frame: false,		
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Guardar',					
					tooltip:	'Guardar ',
					iconCls: 'icoGuardar',
					id: 'btnGuardar',
					handler: procGuardar
				}
			]
		}
	});
	
	//**************************Criterios de Busqueda ********************
	var catIntermediarioData = new Ext.data.JsonStore({
		id: 'catIntermediarioData',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13bloLimiteXIF.data.jsp',
		baseParams: {
			informacion: 'catIntermediarioData'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var  elementosForma  = [
		{
			xtype: 'combo',
			name: 'ic_if',
			id: 'ic_if1',
			fieldLabel: 'Intermediario Financiero',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_if',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			editable:true,
			minChars : 1,
			allowBlank: true,			
			store : catIntermediarioData,
			tpl : NE.util.templateMensajeCargaCombo
		}
	];	

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		monitorValid: true,
		title: 'Limites por IF',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 150,	
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,		
		buttons: [		
			{
				text: 'Buscar',
				id: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,				
				handler: function(boton, evento) {
				
					fp.el.mask('Enviando...', 'x-mask-loading');			
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{  						
						})
					});
					
				}
			}
		]
	});
	
	
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,		
		style: 'margin:0 auto;',
		items: [
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20)			
		]
	});
	
	catIntermediarioData.load();
	
	
} );