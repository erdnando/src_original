<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*, java.math.*,
		netropology.utilerias.*,netropology.utilerias.usuarios.*,
		com.netro.descuento.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ page import="com.netro.pdf.*"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

IMantenimiento BeanMantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB", IMantenimiento.class);


if (informacion.equals("Parametrizacion")) {
	//Nothing. . . 
	
}else if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion

	String cc_acuse = (request.getParameter("cc_acuse") == null)?"":request.getParameter("cc_acuse");
	String fechaCargaIni = (request.getParameter("fechaCargaIni") == null)?"":request.getParameter("fechaCargaIni");
	String fechaCargaFin = (request.getParameter("fechaCargaFin") == null)?"":request.getParameter("fechaCargaFin");
	String ic_usuario = (request.getParameter("ic_usuario") == null)?"":request.getParameter("ic_usuario");

	//JSONObject jsonObj = new JSONObject();
	List reg = new ArrayList();
	String num_acuse="",fecha="",sCveUsuario="",vclave_hash="", numDoctos="";
	UtilUsr utilUsr = new UtilUsr();
	Vector lovPNego = BeanMantenimiento.ovgetPreNegociables(iNoCliente, cc_acuse, fechaCargaIni, fechaCargaFin, ic_usuario,"");
	if (lovPNego.size()>0) {
		//Vector lovPreNegociables = null;
		int nRow = 0;
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

		String pais        = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
		String noCliente   = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
		String strnombre   = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
		String nombreUsr   = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
		String logo      	 = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
  
		pdfDoc.encabezadoConImagenes(pdfDoc,pais,noCliente,strnombre,nombreUsr, "", logo,strDirectorioPublicacion);
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formasrep",ComunesPDF.RIGHT);

		for (int i=0;i<lovPNego.size();i++) {
			Vector lovPreNegociables = (Vector)lovPNego.get(i);
			int doctosMN = 0;
			int doctosDL = 0;
			if(lovPreNegociables.get(3)!=null)
				doctosMN = (!("").equals(lovPreNegociables.get(3).toString()))?Integer.parseInt(lovPreNegociables.get(3).toString()):doctosMN;
			if(lovPreNegociables.get(5)!=null)
				doctosDL = (!("").equals(lovPreNegociables.get(5).toString()))?Integer.parseInt(lovPreNegociables.get(5).toString()):doctosDL;
			//
			num_acuse = lovPreNegociables.get(0).toString();
			fecha = lovPreNegociables.get(1).toString();
			sCveUsuario = lovPreNegociables.get(2).toString();
			vclave_hash 	 = lovPreNegociables.get(7).toString();			
			numDoctos	= String.valueOf(doctosMN+doctosDL); //fodea 053-2009 fvr
			//Para Obtener el Nombre del Usuario que Capturo el Documento Prenegociable
			Usuario usuario = utilUsr.getUsuario(sCveUsuario);
			strNombreUsuario = usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();

			HashMap hash = new HashMap();
			lovPreNegociables = (Vector)lovPNego.get(i);
			hash.put("CC_ACUSE",num_acuse);
			hash.put("FECHA",fecha);
			hash.put("USUARIO",sCveUsuario+" "+strNombreUsuario);
			hash.put("NUMERO_DOCTOS",numDoctos);
			reg.add(hash);

			if(nRow == 0){
				pdfDoc.setTable(4, 80);
				pdfDoc.setCell("Mantenimiento Doctos. Pre Negociables","celda01rep",ComunesPDF.CENTER,4);
				pdfDoc.setCell("Número de Acuse","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Usuario","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Número Documentos","celda01rep",ComunesPDF.CENTER);
			}
			pdfDoc.setCell(num_acuse,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fecha,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(sCveUsuario+" "+strNombreUsuario,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(numDoctos,"formasrep",ComunesPDF.CENTER);
			nRow++;
		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

		if (reg != null){
			JSONArray jsObjArray = new JSONArray();
			jsObjArray = JSONArray.fromObject(reg);
			jsonObj.put("registros", jsObjArray);
		}
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(	informacion.equals("Individual")	){
	int start = 0;
	int limit = 0;

	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsDoctosPreNegdDet());

	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request);
		}
		infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		
	} catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
	}

}else if(	informacion.equals("detalleConfirma")	){

	String num_acuse = (request.getParameter("num_acuse") == null)?"":request.getParameter("num_acuse");
	String cadDocSelec = (request.getParameter("cadDocSelec") == null)?"":request.getParameter("cadDocSelec");
	String strPreNegociable = request.getParameter("strPreNegociable");
	StringBuffer seguridad	=	new StringBuffer("");
	StringBuffer textoF_a	=	new StringBuffer("");
	StringBuffer textoF_b	=	new StringBuffer("");

	List regsNego = new ArrayList();
	List regsNoNego = new ArrayList();

	int regP=0;
	int regPN=0;
	boolean bOperaFactorajeVencido = false,	bOperaFactorajeDistribuido = false;
	
	CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
	
	bOperaFactorajeVencido = CargaDocumentos.autorizadaFactorajeVencido(iNoCliente);
	bOperaFactorajeDistribuido = CargaDocumentos.autorizadaFactorajeDistribuido(iNoCliente);

	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	
	String icEstatusP="28";
	Vector vDoctosCargados = BeanMantenimiento.mostrarDocumentosPreNegociables(strAforo,iNoCliente,icEstatusP,strAforoDL, num_acuse, cadDocSelec);
	
	String sPymeRazonSoc="", sNumeroDocto="", sFechaDocto="", sFechaVenc="", sNombreMoneda="";
	String sMontoDocto="", sTipoFactoraje="",	nomTipoFactoraje="", sAforoPorcAnti="", sMontoDesc="", sReferencia="", sNombreIf="";
	String sNombreBeneficiario="", sPorcentajeBeneficiario="", sMontoBeneficiario="" , sEstatus="";
	BigDecimal bdTotalDol=new BigDecimal("0.0");	BigDecimal bdTotalDescDol=new BigDecimal("0.0");
	BigDecimal bdTotalMN=new BigDecimal("0.0");		BigDecimal bdTotalDescMN=new BigDecimal("0.0");
	BigDecimal bdTotalDolNeg=new BigDecimal("0.0"); BigDecimal bdTotalDescDolNeg=new BigDecimal("0.0");
	BigDecimal bdTotalMNNeg=new BigDecimal("0.0");	BigDecimal bdTotalDescMNNeg=new BigDecimal("0.0");
	BigDecimal bdTotalDolNoNeg=new BigDecimal("0.0"); BigDecimal bdTotalDescDolNoNeg=new BigDecimal("0.0");
	BigDecimal bdTotalMNNoNeg=new BigDecimal("0.0"); BigDecimal bdTotalDescMNNoNeg=new BigDecimal("0.0");
	int iNumMoneda=0, iNoTotalDtosDol=0, iNoTotalDtosMN=0;

	for(int i=0; i<vDoctosCargados.size(); i++) {
		Vector vd 		= (Vector)vDoctosCargados.get(i);
		HashMap hash = new HashMap();
		sPymeRazonSoc 	= vd.get(0).toString();
		sNumeroDocto 	= vd.get(1).toString();
		sFechaDocto 	= vd.get(2).toString();
		sFechaVenc 		= vd.get(3)==null?"":vd.get(3).toString();
		sNombreMoneda 	= vd.get(4).toString();
		iNumMoneda 		= Integer.parseInt(vd.get(5).toString());
		sMontoDocto 	= vd.get(6).toString();
		sAforoPorcAnti 	= vd.get(8).toString();
		sMontoDesc 		= vd.get(9).toString();
		sReferencia 	= vd.get(10).toString();
		sTipoFactoraje 	= vd.get(7).toString();

		sNombreIf = "";
		if(bOperaFactorajeVencido) { // Para Factoraje Vencido
			if(sTipoFactoraje.equalsIgnoreCase("V")) {
				sAforoPorcAnti = "100";	// si factoraje vencido: se aplica 100% de anticipo.
				sMontoDesc = sMontoDocto;	// el monto del desc es igual al del docto.
				sNombreIf = vd.get(11).toString();
			}
		}
		sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
		if(bOperaFactorajeDistribuido) { // Para Factoraje Distribuido
			if(sTipoFactoraje.equalsIgnoreCase("D"))  {
				sAforoPorcAnti = "100";
				sMontoDesc = sMontoDocto;
				sNombreBeneficiario = vd.get(20).toString();
				sPorcentajeBeneficiario = vd.get(21).toString();
				sMontoBeneficiario = vd.get(22).toString();
			}
		}
		if(sTipoFactoraje.equals("N"))
			nomTipoFactoraje = "Normal";
		else if(sTipoFactoraje.equals("V"))
			nomTipoFactoraje = "Vencido";
		else if(sTipoFactoraje.equals("D"))
			nomTipoFactoraje = "Distribuido";
		else if(sTipoFactoraje.equals("I"))
			nomTipoFactoraje = "Vencimiento Infonavit";
		else if(sTipoFactoraje.equals("C")) {
			nomTipoFactoraje = "Nota de Credito";
			sAforoPorcAnti = "100";
			sMontoDesc = sMontoDocto;
		}
		hash.put("CG_RAZON_SOCIAL",sPymeRazonSoc);
		hash.put("IG_NUMERO_DOCTO",sNumeroDocto);
		hash.put("FECHA_DOCTO",sFechaDocto);
		hash.put("FECHA_VENC",sFechaVenc);
		hash.put("CD_NOMBRE",sNombreMoneda);
		hash.put("TIPO_FACTORAJE",nomTipoFactoraje);
		hash.put("FN_MONTO",sMontoDocto);
		hash.put("PORC_ANTICIPO",sAforoPorcAnti);
		hash.put("MONTO_DSCTO",sMontoDesc);
		hash.put("CT_REFERENCIA",sReferencia);
		hash.put("NOMBRE_IF",sNombreIf);
		hash.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
		hash.put("FN_PORC_BENEFICIARIO",sPorcentajeBeneficiario);
		hash.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
		regsNego.add(hash);
		textoF_b.append("Pre-Negociable\nNombre Proveedor|Num. Documento|Fecha de Emisión|Fecha de Vencimiento|Moneda|Tipo Factoraje|Monto|Porcentaje de Descuento|Monto a Descontar|Referencia|");
		if(bOperaFactorajeVencido) {
			textoF_b.append("Nombre IF|");
		}
		if(bOperaFactorajeDistribuido) {
			textoF_b.append("Nombre Beneficiario|Porcentaje Beneficiario|Monto Beneficiario|");
		}
		textoF_b.append("\n");
		textoF_b.append(sPymeRazonSoc.replace('"',' ')+"|"+sNumeroDocto+"|"+sFechaDocto+"|"+sFechaVenc+"|"+sNombreMoneda+"|");
		textoF_b.append(sTipoFactoraje+"|"+Comunes.formatoMN(sMontoDocto)+"|"+Comunes.formatoDecimal(sAforoPorcAnti,2)+"%"+"|"+Comunes.formatoMN(sMontoDesc)+"|"+sReferencia+"|");
		if(bOperaFactorajeVencido) { textoF_b.append(sNombreIf+"|"); }
		if(bOperaFactorajeDistribuido) { textoF_b.append(sNombreBeneficiario+"|"+(!sPorcentajeBeneficiario.equals("")?Comunes.formatoDecimal(sPorcentajeBeneficiario,4)+"%":"")+"|"+(!sMontoBeneficiario.equals("")?Comunes.formatoMN(sMontoBeneficiario):"")+"|" ); }
		textoF_b.append("\n");

		if(sMontoDocto.equals("")){sMontoDocto = "0";}
		if(sMontoDesc.equals("")){sMontoDesc = "0";}

		if (iNumMoneda == 54) {
			bdTotalDol = bdTotalDol.add(new BigDecimal(sMontoDocto));
			bdTotalDolNeg = bdTotalDolNeg.add(new BigDecimal(sMontoDocto));
			bdTotalDescDol = bdTotalDescDol.add(new BigDecimal(sMontoDesc));
			bdTotalDescDolNeg = bdTotalDescDolNeg.add(new BigDecimal(sMontoDesc));
			iNoTotalDtosDol++;
		} else if (iNumMoneda == 1) {
			bdTotalMN = bdTotalMN.add(new BigDecimal(sMontoDocto));
			bdTotalMNNeg = bdTotalMNNeg.add(new BigDecimal(sMontoDocto));
			bdTotalDescMN = bdTotalDescMN.add(new BigDecimal(sMontoDesc));
			bdTotalDescMNNeg = bdTotalDescMNNeg.add(new BigDecimal(sMontoDesc));
			iNoTotalDtosMN++;
		}
		regP++;
	}//Fin-For

	if (regsNego != null){
		JSONArray jaNego = new JSONArray();
		jaNego = JSONArray.fromObject(regsNego);
		jsonObj.put("regsNego", jaNego);
	}

	String icEstatusPRENN="29";
	Vector vDoctosCargadosNo = BeanMantenimiento.mostrarDocumentosPreNegociables(strAforo,iNoCliente,icEstatusPRENN,strAforoDL, num_acuse, cadDocSelec);

	for(int i=0; i<vDoctosCargadosNo.size(); i++) {
		Vector vd 		= (Vector)vDoctosCargadosNo.get(i);
		HashMap hash = new HashMap();
		sPymeRazonSoc 	= vd.get(0).toString();
		sNumeroDocto 	= vd.get(1).toString();
		sFechaDocto 	= vd.get(2).toString();
		sFechaVenc 		= vd.get(3).toString();
		sNombreMoneda 	= vd.get(4).toString();
		iNumMoneda 		= Integer.parseInt(vd.get(5).toString());
		sMontoDocto 	= vd.get(6).toString();
		sAforoPorcAnti 	= vd.get(8).toString();
		sMontoDesc 		= vd.get(9).toString();
		sReferencia 	= vd.get(10).toString();
		sTipoFactoraje 	= vd.get(7).toString();
		sNombreIf = "";
		if(bOperaFactorajeVencido) { // Para Factoraje Vencido
			if(sTipoFactoraje.equalsIgnoreCase("V")) {
				sAforoPorcAnti = "100";	// si factoraje vencido: se aplica 100% de anticipo.
				sMontoDesc = sMontoDocto;	// el monto del desc es igual al del docto.
				sNombreIf = vd.get(11).toString();
			}
		}
		sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
		if(bOperaFactorajeDistribuido) { // Para Factoraje Distribuido
			if(sTipoFactoraje.equalsIgnoreCase("D"))  {
				sAforoPorcAnti = "100";
				sMontoDesc = sMontoDocto;
				sNombreBeneficiario = vd.get(20).toString();
				sPorcentajeBeneficiario = vd.get(21).toString();
				sMontoBeneficiario = vd.get(22).toString();
			}
		}
		nomTipoFactoraje = "";
		if(sTipoFactoraje.equals("N"))
			nomTipoFactoraje = "Normal";
		else if(sTipoFactoraje.equals("V"))
			nomTipoFactoraje = "Vencido";
		else if(sTipoFactoraje.equals("D"))
			nomTipoFactoraje = "Distribuido";
		else if(sTipoFactoraje.equals("I"))
			nomTipoFactoraje = "Vencimiento Infonavit";
		else if(sTipoFactoraje.equals("C")) {
			nomTipoFactoraje = "Nota de Credito";
		}
		hash.put("CG_RAZON_SOCIAL",sPymeRazonSoc);
		hash.put("IG_NUMERO_DOCTO",sNumeroDocto);
		hash.put("FECHA_DOCTO",sFechaDocto);
		hash.put("FECHA_VENC",sFechaVenc);
		hash.put("CD_NOMBRE",sNombreMoneda);
		hash.put("TIPO_FACTORAJE",nomTipoFactoraje);
		hash.put("FN_MONTO",sMontoDocto);
		hash.put("PORC_ANTICIPO",sAforoPorcAnti);
		hash.put("MONTO_DSCTO",sMontoDesc);
		hash.put("CT_REFERENCIA",sReferencia);
		hash.put("NOMBRE_IF",sNombreIf);
		hash.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
		hash.put("FN_PORC_BENEFICIARIO",sPorcentajeBeneficiario);
		hash.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
		regsNoNego.add(hash);

		textoF_b.append("Pre-No Negociable\nNombre Proveedor|Num. Documento|Fecha de Emisión|Fecha de Vencimiento|Moneda|Tipo Factoraje|Monto|Porcentaje de Descuento|Monto a Descontar|Referencia|");
		if(bOperaFactorajeVencido) {
			textoF_b.append("Nombre IF|");
		}
		if(bOperaFactorajeDistribuido) {
			textoF_b.append("Nombre Beneficiario|Porcentaje Beneficiario|Monto Beneficiario|");
		}
		textoF_b.append("\n");
		textoF_b.append(sPymeRazonSoc.replace('"',' ')+"|"+sNumeroDocto+"|"+sFechaDocto+"|"+sFechaVenc+"|"+sNombreMoneda+"|");
		textoF_b.append(sTipoFactoraje+"|"+Comunes.formatoMN(sMontoDocto)+"|"+Comunes.formatoDecimal(sAforoPorcAnti,2)+"%"+"|"+Comunes.formatoMN(sMontoDesc)+"|"+sReferencia+"|");
		if(bOperaFactorajeVencido) { textoF_b.append(sNombreIf+"|"); }
		if(bOperaFactorajeDistribuido) { textoF_b.append(sNombreBeneficiario+"|"+(!sPorcentajeBeneficiario.equals("")?Comunes.formatoDecimal(sPorcentajeBeneficiario,4)+"%":"")+"|"+(!sMontoBeneficiario.equals("")?Comunes.formatoMN(sMontoBeneficiario):"")+"|" ); }
		textoF_b.append("\n");

		if(sMontoDocto.equals("")){sMontoDocto = "0";}
		if(sMontoDesc.equals("")){sMontoDesc = "0";}

		if (iNumMoneda == 54) {
			bdTotalDol = bdTotalDol.add(new BigDecimal(sMontoDocto));
			bdTotalDolNoNeg = bdTotalDolNoNeg.add(new BigDecimal(sMontoDocto));
			bdTotalDescDol = bdTotalDescDol.add(new BigDecimal(sMontoDesc));
			bdTotalDescDolNoNeg = bdTotalDescDolNoNeg.add(new BigDecimal(sMontoDesc));
			iNoTotalDtosDol++;
		} else if (iNumMoneda == 1) {
			bdTotalMN = bdTotalMN.add(new BigDecimal(sMontoDocto));
			bdTotalMNNoNeg = bdTotalMNNoNeg.add(new BigDecimal(sMontoDocto));
			bdTotalDescMN = bdTotalDescMN.add(new BigDecimal(sMontoDesc));
			bdTotalDescMNNoNeg = bdTotalDescMNNoNeg.add(new BigDecimal(sMontoDesc));
			iNoTotalDtosMN++;
		}
		regPN++;
	}//Fin-For

	if (regsNoNego != null){
		JSONArray jaNoNego = new JSONArray();
		jaNoNego = JSONArray.fromObject(regsNoNego);
		jsonObj.put("regsNoNego", jaNoNego);
	}

	jsonObj.put("bdTotalMN", bdTotalMN.toPlainString());
	jsonObj.put("bdTotalDol", bdTotalDol.toPlainString());
	jsonObj.put("bdTotalDescMN", bdTotalDescMN.toPlainString());
	jsonObj.put("bdTotalDescDol", bdTotalDescDol.toPlainString());
	// Estatus 28
	jsonObj.put("bdTotalMNNeg", bdTotalMNNeg.toPlainString());
	jsonObj.put("bdTotalDescMNNeg", bdTotalDescMNNeg.toPlainString());
	jsonObj.put("bdTotalDolNeg", bdTotalDolNeg.toPlainString());
	jsonObj.put("bdTotalDescDolNeg", bdTotalDescDolNeg.toPlainString());
	// Estatus 29
	jsonObj.put("bdTotalMNNoNeg", bdTotalMNNoNeg.toPlainString());
	jsonObj.put("bdTotalDolNoNeg", bdTotalDolNoNeg.toPlainString());
	jsonObj.put("bdTotalDescMNNoNeg", bdTotalDescMNNoNeg.toPlainString());
	jsonObj.put("bdTotalDescDolNoNeg", bdTotalDescDolNoNeg.toPlainString());

	jsonObj.put("iNoTotalDtosMN", Integer.toString(iNoTotalDtosMN));
	jsonObj.put("iNoTotalDtosDol", Integer.toString(iNoTotalDtosDol));
	String txtLegalNC = "Al transmitir este MENSAJE DE DATOS, usted está bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesión de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisión tendrá validez para todos los efectos legales. En caso de transmitir NOTAS DE CREDITO, usted está en el entendido de que aplicarán a los DOCUMENTOS una vez que a la MIPYME que correspondan decida efectuar el descuento.";
	String txtLegal = "Al transmitir este MENSAJE DE DATOS, usted está bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesión de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisión tendrá validez para todos los efectos legales. En caso de transmitir NOTAS DE CREDITO, usted está en el entendido de que aplicarán a los DOCUMENTOS una vez que a la MIPYME que correspondan decida efectuar el descuento.";
	textoF_a.append(
				"Moneda Nacional\tNo. total de documentos cargados\t"+iNoTotalDtosMN+" \t" +
				"Monto total de los documentos cargados\t"+bdTotalMN.toPlainString()+"\n" +
				"Dolares\t\tNo. total de documentos cargados\t"+iNoTotalDtosDol+" \t " +
				"Monto total de los documentos cargados\t"+bdTotalDol.toPlainString()+"\n" +
				"Moneda Nacional\tMonto total de los documentos cargados Negociables\t"+bdTotalMNNeg.toPlainString()+" \t" +
				"Monto total de los documentos cargados NO Negociables\t"+bdTotalMNNoNeg.toPlainString()+"\n" +
				"Dolares\t\tMonto total de los documentos cargados Negociables\t"+bdTotalDolNeg.toPlainString()+" \t" +
				"Monto total de los documentos cargados NO Negociables\t"+bdTotalDolNoNeg.toPlainString()+"\n" +
				(("S".equals(operaNC)&&!"S".equals(strPreNegociable))?txtLegalNC:txtLegal+"|\n")	);

	textoF_b.append(
				"Total Moneda Nacional Pre Negociables\t"+Comunes.formatoMN(bdTotalMNNeg.toPlainString())+"\t" +
				"Monto a Descontar Moneda Nacional Negociable\t"+Comunes.formatoMN(bdTotalDescMNNeg.toPlainString())+"\n" +
				"Total Moneda Nacional Pre Negociables:\t"+Comunes.formatoMN(bdTotalDolNeg.toPlainString())+"\t" +
				"Monto a Descontar Dolares Negociable\t"+Comunes.formatoMN(bdTotalDescDolNeg.toPlainString()) + "\n" +
				"Monto Total MN No Negociables:\t"+Comunes.formatoMN(bdTotalMNNoNeg.toPlainString())+"\t " +
				"Monto a Descontar Moneda Nacional No Negociable\t"+Comunes.formatoMN(bdTotalDescMNNoNeg.toPlainString())+"\n" +
				"Total Dólares No Negociables:\t"+Comunes.formatoMN(bdTotalDolNoNeg.toPlainString())+"\t " +
				"Monto a Descontar Dolares No Negociable\t"+Comunes.formatoMN(bdTotalDescDolNoNeg.toPlainString()) + "\n" +
				"Total de Documentos Moneda Nacional:\t"+iNoTotalDtosMN+"\n " +
				"Total de Documentos Dólares: \t"+iNoTotalDtosDol+"|\n"	);

	seguridad.append(textoF_a.toString() + textoF_b.toString());
	
	jsonObj.put("txtFirma", seguridad.toString());
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if(	informacion.equals("Confirma")	){
	
	String num_acuse		=	(request.getParameter("num_acuse") == null)?"":request.getParameter("num_acuse");
	String operacion		=	(request.getParameter("operacion")==null)?"":request.getParameter("operacion");
	String cadDocSelec	=	(request.getParameter("cadDocSelec") == null)?"":request.getParameter("cadDocSelec");
	String regPreneg		=	(request.getParameter("regPrenegociables")==null)?"":request.getParameter("regPrenegociables");
	String regPrenoneg	=	(request.getParameter("regPrenonegociables")==null)?"":request.getParameter("regPrenonegociables");
	String usuario = iNoUsuario+" "+strNombre;
	boolean errorUsuario = true;
	String	pkcs7	= (request.getParameter("Pkcs7")==null)?"":request.getParameter("Pkcs7");
	String 	serial = "";
	String 	folio= "";
	String 	externContent = (request.getParameter("TextoFirmado")==null)?"":request.getParameter("TextoFirmado");
	char 	getReceipt = 'Y';
	String recibo_e = "500";

	// Generacion del numero de acuse.
	Acuse no_acuse=new Acuse(Acuse.ACUSE_EPO,"1");
	String acuse = no_acuse.formatear();	


	if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
		folio = no_acuse.toString();
		Seguridad s = new Seguridad();

		if (s.autenticar(folio, _serial, pkcs7, externContent, getReceipt)) {
			recibo_e = s.getAcuse();	
			
			if("Eliminar".equals(operacion)){
				BeanMantenimiento.eliminaPrenegociables(iNoCliente, num_acuse);
				jsonObj.put("success", new Boolean(true));
			}//fin-Eliminar%

			if("Generar".equals(operacion)) {
				boolean bOkActualiza = true;
				try {
					if(Integer.parseInt(regPreneg)>0) {
						bOkActualiza = BeanMantenimiento.bactualizaEstatusPreNegociable(iNoCliente, num_acuse, "28", cadDocSelec, usuario);
					}
					if(Integer.parseInt(regPrenoneg)>0) {
						bOkActualiza = BeanMantenimiento.bactualizaEstatusPreNegociable(iNoCliente, num_acuse, "29", cadDocSelec, usuario);
					}
					jsonObj.put("success", new Boolean(true));

				} catch(Exception e) {
					bOkActualiza = false;
					String _error = s.mostrarError();
					jsonObj.put("success", new Boolean(false));
					jsonObj.put("msg", "Error al actualizar. " + _error + ".<br>Proceso CANCELADO");
				}
			}//fin-generar		 

		}  //if (La autenticacion)
		else { //autenticación fallida 
			String _error = s.mostrarError();
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
		}
		jsonObj.put("operacion", operacion);
	}// if (serial!="", extContents!=null)

	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>