
Ext.onReady(function() {

	var cancelacion  = Ext.getDom('cancelacion').value;
	var mensaje  = Ext.getDom('mensaje').value;
	var proceso  = Ext.getDom('proceso').value;
	var recibo  = Ext.getDom('recibo').value;
	var strPendiente  = Ext.getDom('strPendiente').value;
	var strPreNegociable  = Ext.getDom('strPreNegociable').value;	
	var operaNC  = Ext.getDom('operaNC').value;	
	var acuse  = Ext.getDom('acuse').value;	
	var usuario  = Ext.getDom('usuario').value;	
	var hidFechaCarga  = Ext.getDom('hidFechaCarga').value;	
	var hidHoraCarga  = Ext.getDom('hidHoraCarga').value;	
	var acuseFormateado  =  Ext.getDom('acuseFormateado').value;	
	var bVenSinOperarS =  Ext.getDom('bVenSinOperarS').value;
	
	var leyenda;
	var autentificacion;
	if(cancelacion=='S') {
		 autentificacion = '<table width="200" align="center" >'+
		'<tr><td><H1>El proceso fue cancelado</H1></td></tr></table>';		
	}else if(cancelacion=='A') {
	 autentificacion = '<table width="300">'+
		'<tr><td ><H1>'+mensaje+'</H1></td></tr></table>';
		leyenda =autentificacion;
	}else if(cancelacion=='N') {
	
	 autentificacion = '<table width="300">'+
		'<tr><td ><H1>'+mensaje+'</H1></td></tr>'+
		'<tr><td><H1>  </H1></td></tr> <tr><td><H1>  </H1></td></tr>'+		
		'<tr><td><H1>El proceso fue cancelado</H1></td></tr>'+
		'</table>';	
	}			
	if(operaNC=='S') {
		 leyenda = '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
		'<tr><td class="formas" style="text-align: justify;" colspan="3">Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesi�n de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales. En caso de transmitir NOTAS DE CREDITO, usted est� en el entendido de que aplicar�n a los DOCUMENTOS una vez que a la MIPYME '+
		'que correspondan decida efectuar el descuento.	</td></tr></table>';		
			
	}else {
		leyenda = '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
		'<tr><td class="formas" style="text-align: justify;" colspan="3">Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesi�n de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales.</td></tr>'+ 
		'</table>';	
	}
	
	
			//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	 
	//Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',		
		hidden: true,	
		align: 'center',
		columns: [
			{
				header : '',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true,
				align: 'left'	
			},
			{
				header : '',			
				dataIndex : 'informacion',
				width : 450,
				align: 'left',
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 655,
		style: 'margin:0 auto;',
		autoHeight : true,
		frame: true
	});
		
	
	var mensajeAcuse = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAcuse',							
		width:	'400',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		autentificacion				
			}			
		]
	});
	
	
	var leyendaLegal = new Ext.Container({
		layout: 'table',		
		id: 'leyendaLegal',	
		style: 'margin:0 auto;',
		layoutConfig: {
			columns: 1
		},
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		leyenda, 
				cls:		'x-form-item', 
				style: { 
					width:   		'300%', 
					textAlign: 		'center'
				} 
			}			
		]
	});
	
	//para descargar el archivo CSV
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var jsonData = Ext.util.JSON.decode(response.responseText);	
			
			if(jsonData.accion=='button')  {
		
				var btnBajarCSV = Ext.getCmp('btnBajarCSV');
				btnBajarCSV.show();
				btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajarCSV.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
					}	);
			}
		} else {
			Ext.MessageBox.alert("Mensaje","Los archivos de los acuses de la publicaci�n de documentos no se generaron correctamente "); 
			btnGenerarCSV.enable();
			//NE.util.mostrarConnError(response,opts);
		}
	}	
	
	//para desacargar el archivo PDF
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var jsonData = Ext.util.JSON.decode(response.responseText);	
		
			if(jsonData.accion=='Acuse')  {
					Ext.Ajax.request({
						url: '13forma1cExtCsv.jsp',
						params: {
							informacion:'ArchivoCSV',														
							proceso:proceso,
							acuse:acuse,
							strPendiente:strPendiente,
							strPreNegociable:strPreNegociable,
							accion:'Acuse'
						}					
						,callback: procesarGenerarCSV
					});
					
			}else  if(jsonData.accion=='button')  {
		
				var btnBajarPDF = Ext.getCmp('btnBajarPDF');
				btnBajarPDF.show();
				btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajarPDF.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
			}
			
			
		} else {
			Ext.MessageBox.alert("Mensaje","Los archivos de los acuses de la publicaci�n de documentos no se generaron correctamente "); 
			btnGenerarPDF.enable();
			//NE.util.mostrarConnError(response,opts);
		}
	}	
	
	//Contenedor para los botones de imprimir y Salir 
	var fpBotones = new Ext.Container({
		layout: 'table',
		id: 'fpBotones',			
		width:	'250',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [	
		{
			xtype: 'button',
			text: 'Generar PDF',
			tooltip:	'Generar PDF',
			id: 'btnGenerarPDF',
			handler: function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				var barraPaginacion = Ext.getCmp("barraPaginacion");
				Ext.Ajax.request({
					url: '13forma1cExtpdf.jsp',
					params: {
						informacion:'ArchivoPDF',														
						proceso:proceso,
						acuse:acuse,
						recibo:recibo,
						strPendiente:strPendiente,
						strPreNegociable:strPreNegociable,
						usuario:usuario,
						hidFechaCarga:hidFechaCarga,
						hidHoraCarga:hidHoraCarga,
						accion:'button'
					}					
					,callback: procesarGenerarPDF
				});
			}						
		},		
		{
			xtype: 'button',
			text: 'Bajar PDF',
			tooltip:	'Bajar PDF',
			id: 'btnBajarPDF',
			hidden: true
		},	
		{
				xtype: 'button',
				text: 'Generar Archivo',
				tooltip:	'Generar Archivo',
				id: 'btnGenerarCSV',
				handler: function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				var barraPaginacion = Ext.getCmp("barraPaginacion");
				Ext.Ajax.request({
					url: '13forma1cExtCsv.jsp',
					params: {
						informacion:'ArchivoCSV',														
						proceso:proceso,
						acuse:acuse,
						strPendiente:strPendiente,
						strPreNegociable:strPreNegociable,
						accion:'button'
					}					
					,callback: procesarGenerarCSV
				});
			}			
		},
		{
			xtype: 'button',
			text: 'Bajar Archivo',
			tooltip:	'Bajar Archivo',
			id: 'btnBajarCSV',
			hidden: true
		},	
		{
			xtype: 'button',
			text: 'Salir',			
			id: 'btnSalir',		 
			handler: function() {
				window.location = "13forma01Ext.jsp?strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable;
			}
		}	
	]
	});
	
	
	var procesarConsultaData3 = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;			
		
		if (arrRegistros != null) {
			if (!gridConsulta3.isVisible()) {
			gridConsulta3.show();
			}						
			hidMontoTotalMN = jsonData.hidMontoTotalMN;
			hidNumDoctosActualesMN = jsonData.hidNumDoctosActualesMN;
			hidMontoTotalDL = jsonData.hidMontoTotalDL;
			hidNumDoctosActualesDL = jsonData.hidNumDoctosActualesDL;
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;					
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta3.getColumnModel();			
			var el = gridConsulta3.getGridEl();				
			
			if(store.getTotalCount() > 0) {			
				if(jsonData.operaFVPyme =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEEDOR'), false);	
				}
				if(jsonData.bOperaFactorajeVencido =='S' || jsonData.bOperaFactConMandato =='S'  ||  jsonData.bOperaFactorajeVencidoInfonavit =='S' ) {					
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);	
				}				
				if(jsonData.bOperaFactorajeDistribuido  =='S' || jsonData.bOperaFactorajeVencidoInfonavit   =='S') {				
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'), false);	
				}

				if(jsonData.sPubEPOPEFFlagVal  =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('FECHA_RECEPCION'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COMPRA'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICADOR'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_MAXIMO'), false);	
				}				
				if(jsonData.bestaHabilitadoNumeroSIAFF =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DIGITO_IDENTIFICADOR'), false);	
				}		
				if(jsonData.bOperaFactConMandato =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('MANDANTE'), false);	
				}
				if(jsonData.bValidaDuplicidad =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('DUPLICADO'), false);
				}
				if(hayCamposAdicionales=='0'){
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.nomCampo4);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.nomCampo5);						
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}			
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}

	
	var procesarConsultaData2 = function(store, arrRegistros, opts) 	{	
		var jsonData = store.reader.jsonData;									
		if (arrRegistros != null) {
			if (!gridConsulta2.isVisible()) {
			gridConsulta2.show();
			}						
			hidMontoTotalMN = jsonData.hidMontoTotalMN;
			hidNumDoctosActualesMN = jsonData.hidNumDoctosActualesMN;
			hidMontoTotalDL = jsonData.hidMontoTotalDL;
			hidNumDoctosActualesDL = jsonData.hidNumDoctosActualesDL;
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;					
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta2.getColumnModel();			
			var el = gridConsulta2.getGridEl();	
			
			if(store.getTotalCount() > 0) {			
				if(jsonData.operaFVPyme =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEEDOR'), false);	
				}
				if(jsonData.bOperaFactorajeVencido =='S' || jsonData.bOperaFactConMandato =='S'  ||  jsonData.bOperaFactorajeVencidoInfonavit =='S' ) {					
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);	
				}				
				if(jsonData.bOperaFactorajeDistribuido  =='S' || jsonData.bOperaFactorajeVencidoInfonavit   =='S') {				
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'), false);	
				}

				if(jsonData.sPubEPOPEFFlagVal  =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('FECHA_RECEPCION'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COMPRA'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICADOR'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_MAXIMO'), false);	
				}
				if(jsonData.bestaHabilitadoNumeroSIAFF =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DIGITO_IDENTIFICADOR'), false);	
				}		
				if(jsonData.bOperaFactConMandato =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('MANDANTE'), false);	
				}
				if(jsonData.bValidaDuplicidad =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('DUPLICADO'), false);
				}
				if(hayCamposAdicionales=='0'){
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.nomCampo4);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.nomCampo5);						
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}			
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		
		var jsonData = store.reader.jsonData;									
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
			gridConsulta.show();
			}						
			hidMontoTotalMN = jsonData.hidMontoTotalMN;
			hidNumDoctosActualesMN = jsonData.hidNumDoctosActualesMN;
			hidMontoTotalDL = jsonData.hidMontoTotalDL;
			hidNumDoctosActualesDL = jsonData.hidNumDoctosActualesDL;
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;					
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();			
			var el = gridConsulta.getGridEl();				
			
			if(store.getTotalCount() > 0) {			
				if(jsonData.operaFVPyme =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEEDOR'), false);	
				}
				if(jsonData.bOperaFactorajeVencido =='S' || jsonData.bOperaFactConMandato =='S'  ||  jsonData.bOperaFactorajeVencidoInfonavit =='S' ) {					
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);	
				}				
				if(jsonData.bOperaFactorajeDistribuido  =='S' || jsonData.bOperaFactorajeVencidoInfonavit   =='S') {				
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'), false);	
				}
				if(jsonData.sPubEPOPEFFlagVal  =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_RECEPCION'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COMPRA'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICADOR'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_MAXIMO'), false);	
				}
				if(jsonData.bestaHabilitadoNumeroSIAFF =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DIGITO_IDENTIFICADOR'), false);	
				}				
				if(jsonData.bOperaFactConMandato =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MANDANTE'), false);	
				}
				if(jsonData.bValidaDuplicidad =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DUPLICADO'), false);
				}
				if(hayCamposAdicionales=='0'){
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.nomCampo4);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.nomCampo5);						
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}			
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}

	
	var procesarTotalesControl = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;			
		
		if (arrRegistros != null) {	
			if(store.getTotalCount() > 0) {		
				
				Ext.Ajax.request({
					url: '13forma1cExtpdf.jsp',
					params: {
						informacion:'ArchivoPDF',														
						proceso:proceso,
						acuse:acuse,
						recibo:recibo,
						strPendiente:strPendiente,
						strPreNegociable:strPreNegociable,
						usuario:usuario,
						hidFechaCarga:hidFechaCarga,
						hidHoraCarga:hidHoraCarga,
						accion:'Acuse'
					}					
					,callback: procesarGenerarPDF
				});								
			}
		}
	}


	//para los totales del grid Nomal 
	var totalesControl = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ControlTotales'	
		},								
		fields: [			
			{name: 'INFORMACION' , mapping: 'INFORMACION'},
			{name: 'MONEDA_NACIONAL',  mapping: 'MONEDA_NACIONAL'},
			{name: 'MONEDA_DL' , mapping: 'MONEDA_DL' }						
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarTotalesControl,	
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var gridControl = new Ext.grid.EditorGridPanel({	
		id: 'gridControl',
		store: totalesControl,	
		title: '',			
		columns: [	
			{
				header: '',
				dataIndex: 'INFORMACION',
				width: 350,
				align: 'left'				
			},
			{
				header: 'Moneda Nacional',
				dataIndex: 'MONEDA_NACIONAL',
				width: 150,
				align: 'right'				
			},
			{
				header: 'D�lares',
				dataIndex: 'MONEDA_DL',
				width: 150,
				align: 'right'				
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 150,
		width: 655,		
		style: 'margin:0 auto;',
		frame: false
	});
	
	
	//para los totales del grid Nomal 
	var totalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'TotalesPreAcuse1'	
		},								
		fields: [			
			{name: 'INFORMACION' , mapping: 'INFORMACION'},		
			{name: 'TOTAL_MONTO' ,  mapping: 'TOTAL_MONTO' },
			{name: 'TOTAL_MONTO_DESC',   mapping: 'TOTAL_MONTO_DESC'}				
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		id: 'gridTotales',
		store: totalesData,
		margins: '20 0 0 0',
		align: 'center',
		columns: [	
			{
				header: ' ',
				dataIndex: 'INFORMACION',
				width: 350,
				align: 'left'				
			},		
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'right'			
			},
			{
				header: 'Total Monto Descuento',
				dataIndex: 'TOTAL_MONTO_DESC',
				width: 150,
				align: 'right'				
			},
			{
				header: '',
				dataIndex: '',
				width: 240,
				align: 'right'			
			}		
		],
		height: 210,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	
	var consultaData3 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaAcuse3'
		},
		fields: [						
			{name: 'NOMBRE_PROVEEDOR'},	
			{name: 'NUM_DOCUMENTO'},	
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'FECHA_VENC_PROVEEDOR'},	
			{name: 'IC_MONEDA'},	
			{name: 'MONEDA'},	
			{name: 'TIPO_FACTORAJE'},	
			{name: 'MONTO'},	
			{name: 'PORC_DESCUENTO'},	
			{name: 'MONTO_DESCONTAR'},			
			{name: 'REFERENCIA'},	
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_BENEFICIARIO'},	
			{name: 'PORC_BENEFICIARIO'},	
			{name: 'MONTO_BENEFICIARIO'},	
			{name: 'CAMPO1'},	
			{name: 'CAMPO2'},	
			{name: 'CAMPO3'},	
			{name: 'CAMPO4'},	
			{name: 'CAMPO5'},	
			{name: 'FECHA_RECEPCION'},	
			{name: 'TIPO_COMPRA'},	
			{name: 'CLASIFICADOR'},	
			{name: 'PLAZO_MAXIMO'},	
			{name: 'DIGITO_IDENTIFICADOR'},
			{name: 'MANDANTE'},			
			{name: 'IC_PYME'},
			{name: 'IC_PROCESO'},
			{name: 'ESTATUS'},
			{name: 'DUPLICADO'}	
			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData3,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData3(null, null, null);						
					}
				}
			}			
	});
	
	var gridConsulta3 = new Ext.grid.GridPanel({
		id: 'gridConsulta3',
		title: 'Carga de Documentos',		
		store: consultaData3,	
		columns: [
			{							
				header : 'ESTATUS',
				tooltip: 'ESTATUS',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,			
				align: 'left'	
			},	
			{							
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUM_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex : 'FECHA_EMISION',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento Proveedor',
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex : 'FECHA_VENC_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				hidden: true
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORC_DESCUENTO',
				width : 150,
				sortable : true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')			
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCONTAR',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},		
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Nombre Beneficiario',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'NOMBRE_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Porcentaje Beneficiario',
				tooltip: 'Porcentaje Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Monto Beneficiario',
				tooltip: 'Monto Beneficiario',
				dataIndex : 'MONTO_BENEFICIARIO',
				hidden: true,
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Campo1',
				tooltip: 'Campo1',
				dataIndex : 'CAMPO1',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo2',
				tooltip: 'Campo2',
				dataIndex : 'CAMPO2',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Campo3',
				tooltip: 'Campo3',
				dataIndex : 'CAMPO3',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo4',
				tooltip: 'Campo4',
				dataIndex : 'CAMPO4',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo5',
				tooltip: 'Campo5',
				dataIndex : 'CAMPO5',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Fecha de Recepci�n de Bienes y Servicios',
				tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
				dataIndex : 'FECHA_RECEPCION',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Tipo de Compra (procedimiento)',
				tooltip: 'Tipo de Compra (procedimiento)',
				dataIndex : 'TIPO_COMPRA',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Clasificador por Objeto del Gasto',
				tooltip: 'Clasificador por Objeto del Gasto',
				dataIndex : 'CLASIFICADOR',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Plazo M�ximo ',
				tooltip: 'Plazo M�ximo ',
				dataIndex : 'PLAZO_MAXIMO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},			
			{							
				header : 'Digito Identificador',
				tooltip: 'Digito Identificador',
				dataIndex : 'DIGITO_IDENTIFICADOR',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},		
			{							
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'MANDANTE',
				width : 150,
				align: 'center',
				hidden: true,				
				sortable : false
			},
			{							
				header : 'Notificado como <br>posible Duplicado',
				tooltip: 'Notificado como posible Duplicado',
				dataIndex : 'DUPLICADO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	
	
	var consultaData2 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaAcuse2'
		},
		fields: [						
			{name: 'NOMBRE_PROVEEDOR'},	
			{name: 'NUM_DOCUMENTO'},	
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'FECHA_VENC_PROVEEDOR'},	
			{name: 'IC_MONEDA'},	
			{name: 'MONEDA'},	
			{name: 'TIPO_FACTORAJE'},	
			{name: 'MONTO'},	
			{name: 'PORC_DESCUENTO'},	
			{name: 'MONTO_DESCONTAR'},			
			{name: 'REFERENCIA'},	
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_BENEFICIARIO'},	
			{name: 'PORC_BENEFICIARIO'},	
			{name: 'MONTO_BENEFICIARIO'},	
			{name: 'CAMPO1'},	
			{name: 'CAMPO2'},	
			{name: 'CAMPO3'},	
			{name: 'CAMPO4'},	
			{name: 'CAMPO5'},	
			{name: 'FECHA_RECEPCION'},	
			{name: 'TIPO_COMPRA'},	
			{name: 'CLASIFICADOR'},	
			{name: 'PLAZO_MAXIMO'},	
			{name: 'MANDANTE'},
			{name: 'DIGITO_IDENTIFICADOR'},
			{name: 'IC_PYME'},
			{name: 'IC_PROCESO'},
			{name: 'ESTATUS'},
			{name: 'DUPLICADO'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData2,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData2(null, null, null);						
					}
				}
			}			
	});
	
	var gridConsulta2 = new Ext.grid.GridPanel({
		id: 'gridConsulta2',
		title: 'Carga de Documentos',		
		store: consultaData2,	
		columns: [
			{							
				header : 'ESTATUS',
				tooltip: 'ESTATUS',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUM_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex : 'FECHA_EMISION',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento Proveedor',
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex : 'FECHA_VENC_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				hidden: true
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORC_DESCUENTO',
				width : 150,
				sortable : true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')			
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCONTAR',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},		
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Nombre Beneficiario',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'NOMBRE_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Porcentaje Beneficiario',
				tooltip: 'Porcentaje Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Monto Beneficiario',
				tooltip: 'Monto Beneficiario',
				dataIndex : 'MONTO_BENEFICIARIO',
				hidden: true,
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Campo1',
				tooltip: 'Campo1',
				dataIndex : 'CAMPO1',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo2',
				tooltip: 'Campo2',
				dataIndex : 'CAMPO2',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Campo3',
				tooltip: 'Campo3',
				dataIndex : 'CAMPO3',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo4',
				tooltip: 'Campo4',
				dataIndex : 'CAMPO4',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo5',
				tooltip: 'Campo5',
				dataIndex : 'CAMPO5',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Fecha de Recepci�n de Bienes y Servicios',
				tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
				dataIndex : 'FECHA_RECEPCION',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Tipo de Compra (procedimiento)',
				tooltip: 'Tipo de Compra (procedimiento)',
				dataIndex : 'TIPO_COMPRA',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Clasificador por Objeto del Gasto',
				tooltip: 'Clasificador por Objeto del Gasto',
				dataIndex : 'CLASIFICADOR',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Plazo M�ximo ',
				tooltip: 'Plazo M�ximo ',
				dataIndex : 'PLAZO_MAXIMO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Digito Identificador',
				tooltip: 'Digito Identificador',
				dataIndex : 'DIGITO_IDENTIFICADOR',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},		
			{							
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'MANDANTE',
				width : 150,
				align: 'center',
				hidden: true,				
				sortable : false
			},
			{							
				header : 'Notificado como <br>posible Duplicado',
				tooltip: 'Notificado como posible Duplicado',
				dataIndex : 'DUPLICADO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaAcuse1'
		},
		fields: [						
			{name: 'NOMBRE_PROVEEDOR'},	
			{name: 'NUM_DOCUMENTO'},	
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'FECHA_VENC_PROVEEDOR'},	
			{name: 'IC_MONEDA'},	
			{name: 'MONEDA'},	
			{name: 'TIPO_FACTORAJE'},	
			{name: 'MONTO'},	
			{name: 'PORC_DESCUENTO'},	
			{name: 'MONTO_DESCONTAR'},			
			{name: 'REFERENCIA'},	
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_BENEFICIARIO'},	
			{name: 'PORC_BENEFICIARIO'},	
			{name: 'MONTO_BENEFICIARIO'},	
			{name: 'CAMPO1'},	
			{name: 'CAMPO2'},	
			{name: 'CAMPO3'},	
			{name: 'CAMPO4'},	
			{name: 'CAMPO5'},	
			{name: 'FECHA_RECEPCION'},	
			{name: 'TIPO_COMPRA'},	
			{name: 'CLASIFICADOR'},	
			{name: 'PLAZO_MAXIMO'},	
			{name: 'DIGITO_IDENTIFICADOR'},
			{name: 'MANDANTE'},
			{name: 'IC_PYME'},
			{name: 'IC_PROCESO'},
			{name: 'ESTATUS'},
			{name: 'DUPLICADO'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});

	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',
		title: 'Carga de Documentos',		
		store: consultaData,	
		columns: [
			{							
				header : 'ESTATUS',
				tooltip: 'ESTATUS',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUM_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex : 'FECHA_EMISION',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento Proveedor',
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex : 'FECHA_VENC_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				hidden: true
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORC_DESCUENTO',
				width : 150,
				sortable : true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')			
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCONTAR',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},		
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Nombre Beneficiario',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'NOMBRE_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Porcentaje Beneficiario',
				tooltip: 'Porcentaje Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Monto Beneficiario',
				tooltip: 'Monto Beneficiario',
				dataIndex : 'MONTO_BENEFICIARIO',
				hidden: true,
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Campo1',
				tooltip: 'Campo1',
				dataIndex : 'CAMPO1',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo2',
				tooltip: 'Campo2',
				dataIndex : 'CAMPO2',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Campo3',
				tooltip: 'Campo3',
				dataIndex : 'CAMPO3',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo4',
				tooltip: 'Campo4',
				dataIndex : 'CAMPO4',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo5',
				tooltip: 'Campo5',
				dataIndex : 'CAMPO5',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Fecha de Recepci�n de Bienes y Servicios',
				tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
				dataIndex : 'FECHA_RECEPCION',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Tipo de Compra (procedimiento)',
				tooltip: 'Tipo de Compra (procedimiento)',
				dataIndex : 'TIPO_COMPRA',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Clasificador por Objeto del Gasto',
				tooltip: 'Clasificador por Objeto del Gasto',
				dataIndex : 'CLASIFICADOR',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Plazo M�ximo ',
				tooltip: 'Plazo M�ximo ',
				dataIndex : 'PLAZO_MAXIMO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},			
			{							
				header : 'Digito Identificador',
				tooltip: 'Digito Identificador',
				dataIndex : 'DIGITO_IDENTIFICADOR',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},		
			{							
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'MANDANTE',
				width : 150,
				align: 'center',
				hidden: true,				
				sortable : false
			},
			{							
				header : 'Notificado como <br>posible Duplicado',
				tooltip: 'Notificado como posible Duplicado',
				dataIndex : 'DUPLICADO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	

	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			mensajeAcuse,
			NE.util.getEspaciador(20),
			gridControl	,			
			gridCifrasControl,				
			leyendaLegal,	
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20),
			gridConsulta,	
			NE.util.getEspaciador(20),
			gridConsulta2,	
			NE.util.getEspaciador(20),
			gridConsulta3,
			NE.util.getEspaciador(20),
			gridTotales,			
			NE.util.getEspaciador(20)
		]
	});
	
	
	if(cancelacion=='S' ||  cancelacion=='N' ) {			
		gridControl.hide();
		gridConsulta.hide();
		gridConsulta2.hide();
		gridConsulta3.hide();
		gridTotales.hide();		
		var btnGenerarCSV   = Ext.getCmp("btnGenerarCSV");
		var btnGenerarPDF   = Ext.getCmp("btnGenerarPDF");	
		btnGenerarCSV.hide();
		btnGenerarPDF.hide();
		leyendaLegal.hide();
	
	
	}	else  if(cancelacion=='A' ){
		
	
		var acuseCifras = [
			['No. de acuse ', acuseFormateado],
			['Fecha de carga ', hidFechaCarga],
			['Hora de carga ', hidHoraCarga],
			['Usuario ', usuario]
		];
		
		storeCifrasData.loadData(acuseCifras);	
		gridCifrasControl.show();
	
	
		if(strPendiente=='S'){
			gridConsulta.setTitle('<div><div style="float:left">Pendientes Negociables</div><div style="float:right"></div></div>');
			gridConsulta2.setTitle('<div><div style="float:left">Pendientes No Negociables</div><div style="float:right"></div></div>');
		}else if(strPreNegociable =='S'){
			gridConsulta.setTitle('<div><div style="float:left">Pre Negociables</div><div style="float:right"></div></div>');						
			gridConsulta2.setTitle('<div><div style="float:left">Pre No Negociables</div><div style="float:right"></div></div>');		
		}else  {
			gridConsulta.setTitle('<div><div style="float:left">Negociables</div><div style="float:right"></div></div>');						
			gridConsulta2.setTitle('<div><div style="float:left">No Negociables</div><div style="float:right"></div></div>');				
		}
		
		gridConsulta3.setTitle('<div><div style="float:left">Vencidos Sin Operar</div><div style="float:right"></div></div>');
				
		consultaData.load({  	params: {  informacion: 'ConsultaAcuse1', 	proceso:proceso, strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, acuse:acuse, 	pantalla:'Acuse'   }  	});	
			
		consultaData2.load({  params: {  informacion: 'ConsultaAcuse2', 	proceso:proceso, 	strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, acuse:acuse, pantalla:'Acuse'  	}  	});	
		if(bVenSinOperarS =='S') {
		consultaData3.load({  params: {  informacion: 'ConsultaAcuse3', 	proceso:proceso, strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, acuse:acuse, 	pantalla:'Acuse' 	}  	});	
		}
		totalesData.load({ 	params: { 	informacion: 'TotalesPreAcuse1',  proceso:proceso,	strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, acuse:acuse, pantalla:'PreAcuse' 	} });
		
		totalesControl.load({ 	params: { 		informacion: 'ControlTotales', 	proceso:proceso, 	strPendiente:strPendiente, 	strPreNegociable:strPreNegociable,		pantalla:'PreAcuse' 		} 	});	
	
	}
	
	if(bVenSinOperarS =='N') {
		gridConsulta3.hide();
	}
	
} );