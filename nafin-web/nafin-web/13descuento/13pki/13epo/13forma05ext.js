	
Ext.onReady(function() {
	var ObjGral = {
		paramEpoPef: false,
		operaFVPyme: '',
		strNombreUsuario:'',
		numeroProceso:'',
		Acuse:'',
		Fecha:'',
		Hora:'',
		Folio:''
	}
	
	/*Handlers*/
	var procesarSuccessDatosIniciales = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
						
			ObjGral.paramEpoPef = resp.tieneParamEpoPef;
			ObjGral.operaFVPyme = resp.operaFVPyme;
			ObjGral.strNombreUsuario = resp.strNombreUsuario;


			pnl.el.unmask();
			
		}else{
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	/*Handlers buttons*/
	var fnBtnLimpiar = function(){
		Ext.getCmp("hidCtrlNumDoctosMN1").setValue('');
		Ext.getCmp("hidCtrlMontoTotalMNDespliegue1").setValue('');
		Ext.getCmp("hidCtrlMontoMinMNDespliegue1").setValue('');
		Ext.getCmp("hidCtrlMontoMaxMNDespliegue1").setValue('');
		
		Ext.getCmp("hidCtrlNumDoctosDL1").setValue('');
		Ext.getCmp("hidCtrlMontoTotalDLDespliegue1").setValue('');
		Ext.getCmp("hidCtrlMontoMinDLDespliegue1").setValue('');
		Ext.getCmp("hidCtrlMontoMaxDLDespliegue1").setValue('');	
	}
	
	var fnBtnContinuar = function(){
		var continua = true;
		var hidCtrlNumDoctosMN = Ext.getCmp("hidCtrlNumDoctosMN1");
		var hidCtrlMontoTotalMNDespliegue = Ext.getCmp("hidCtrlMontoTotalMNDespliegue1");
		var hidCtrlMontoMinMNDespliegue = Ext.getCmp("hidCtrlMontoMinMNDespliegue1");
		var hidCtrlMontoMaxMNDespliegue  = Ext.getCmp("hidCtrlMontoMaxMNDespliegue1");
		
		var hidCtrlNumDoctosDL = Ext.getCmp("hidCtrlNumDoctosDL1");
		var hidCtrlMontoTotalDLDespliegue = Ext.getCmp("hidCtrlMontoTotalDLDespliegue1");
		var hidCtrlMontoMinDLDespliegue = Ext.getCmp("hidCtrlMontoMinDLDespliegue1");
		var hidCtrlMontoMaxDLDespliegue  = Ext.getCmp("hidCtrlMontoMaxDLDespliegue1");
			
		Ext.getCmp("hidCtrlNumDoctosMN1").setValue(eliminaFormatoFlotante(hidCtrlNumDoctosMN.getValue()));
		Ext.getCmp("hidCtrlMontoTotalMNDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoTotalMNDespliegue.getValue()));
		Ext.getCmp("hidCtrlMontoMinMNDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoMinMNDespliegue.getValue()));
		Ext.getCmp("hidCtrlMontoMaxMNDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoMaxMNDespliegue.getValue()));
		
		Ext.getCmp("hidCtrlNumDoctosDL1").setValue(eliminaFormatoFlotante(hidCtrlNumDoctosDL.getValue()));
		Ext.getCmp("hidCtrlMontoTotalDLDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoTotalDLDespliegue.getValue()));
		Ext.getCmp("hidCtrlMontoMinDLDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoMinDLDespliegue.getValue()));
		Ext.getCmp("hidCtrlMontoMaxDLDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoMaxDLDespliegue.getValue()));
				
		if (  ( Ext.isEmpty(hidCtrlNumDoctosMN.getValue())   ||  hidCtrlNumDoctosMN.getValue()==0  ) 
		&& ( Ext.isEmpty(hidCtrlNumDoctosDL.getValue()) ||  hidCtrlNumDoctosDL.getValue()==0 )	 ) {
			Ext.MessageBox.alert("Mensaje","Por favor escriba el  n�mero total documentos en Moneda Nacional y/o Dolares");
			continua = false;
			return;
		}
    
    var totalDoctos = Ext.getCmp("hidCtrlNumDoctosMN1").getValue() + Ext.getCmp("hidCtrlNumDoctosDL1").getValue();		
		if(totalDoctos>20000) {						
			Ext.MessageBox.alert("Mensaje","El n�mero m�ximo de documentos a publicar es de 20,000 registros ");			
			Ext.getCmp("hidCtrlNumDoctosMN1").setValue('');
			Ext.getCmp("hidCtrlNumDoctosDL1").setValue('');
			continua = false;
			return;
		}	
		
		//monedda Nacional
		if(!Ext.isEmpty(hidCtrlNumDoctosMN.getValue())  ||  hidCtrlNumDoctosMN.getValue()>0  ) {
		
			if(Ext.isEmpty(hidCtrlMontoTotalMNDespliegue.getValue()) || hidCtrlMontoTotalMNDespliegue.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(hidCtrlMontoMinMNDespliegue.getValue()) || hidCtrlMontoMinMNDespliegue.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(hidCtrlMontoMaxMNDespliegue.getValue()) || hidCtrlMontoMaxMNDespliegue.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
				continua = false;
				return;
			}
						
			if(  (parseFloat(hidCtrlMontoTotalMNDespliegue.getValue()) < parseFloat(hidCtrlMontoMaxMNDespliegue.getValue()) ) 
				|| (parseFloat(hidCtrlMontoMaxMNDespliegue.getValue()) < parseFloat(hidCtrlMontoMinMNDespliegue.getValue() )  )
				|| (parseFloat(hidCtrlMontoTotalMNDespliegue.getValue) < parseFloat(hidCtrlMontoMinMNDespliegue.getValue()) ) ){
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (Moneda Nacional)");
				continua = false;
				return;
			}		
		}
		
		//moneda Dolar
				
		if(!Ext.isEmpty(hidCtrlNumDoctosDL.getValue())  ) {	
		
			if(Ext.isEmpty(hidCtrlMontoTotalDLDespliegue.getValue()) || hidCtrlMontoTotalDLDespliegue.getValue()==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(hidCtrlMontoMinDLDespliegue.getValue()) || hidCtrlMontoMinDLDespliegue.getValue()==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(hidCtrlMontoMaxDLDespliegue.getValue()) || hidCtrlMontoMaxDLDespliegue.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
				continua = false;
				return;
			}
						
			if( (parseFloat(hidCtrlMontoTotalDLDespliegue.getValue()) < parseFloat(hidCtrlMontoMaxDLDespliegue.getValue())) 
				|| (parseFloat(hidCtrlMontoMaxDLDespliegue.getValue()) < parseFloat(hidCtrlMontoMinDLDespliegue.getValue()) )
				|| (parseFloat(hidCtrlMontoTotalDLDespliegue.getValue) < parseFloat(hidCtrlMontoMinDLDespliegue.getValue()))  ){		
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (Dolares)");
				continua = false;
				return;
			}			
			
		}
		
		if (Ext.isEmpty(hidCtrlNumDoctosMN.getValue()) ) {  	Ext.getCmp("hidCtrlNumDoctosMN1").setValue(0);  }
		if (Ext.isEmpty(hidCtrlMontoTotalMNDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoTotalMNDespliegue1").setValue(0); }
		if (Ext.isEmpty(hidCtrlMontoMinMNDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoMinMNDespliegue1").setValue(0); }
		if (Ext.isEmpty(hidCtrlMontoMaxMNDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoMaxMNDespliegue1").setValue(0); }
		
		if (Ext.isEmpty(hidCtrlNumDoctosDL.getValue()) ) {  	Ext.getCmp("hidCtrlNumDoctosDL1").setValue(0); }
		if (Ext.isEmpty(hidCtrlMontoTotalDLDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoTotalDLDespliegue1").setValue(0); }
		if (Ext.isEmpty(hidCtrlMontoMinDLDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoMinDLDespliegue1").setValue(0); }
		if (Ext.isEmpty(hidCtrlMontoMaxDLDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoMaxDLDespliegue1").setValue(0); }
			

		
		
		if(continua){
			
			/*Se genera forma para la carga del archivo*/
			var formaCargaArchivo = new NE.manto.FormaCargaArchivo({
				id: 'formaCargaArchivo1',
				title: 'Ruta del Archivo Origen'
			});
			
			/*Se define la funcion del boton de Ayuda */
			formaCargaArchivo.setHandlerBtnAyuda(function(){
				if (!gridLayoutCarga.isVisible()) {
					gridLayoutCarga.show();
				}else{
					gridLayoutCarga.hide();
				}
			});

			
			/*Se define la funcion del boton de Continuar en la carga del archivo */
			formaCargaArchivo.setHandlerBtnContinuarCarga(fnBtnContinuarCarga);
			
			formaCifrasCtrl.hide();
			pnl.add(formaCargaArchivo);
			pnl.add(gridLayoutCarga);
			pnl.doLayout();
		}
	}
	
	var fnBtnContinuarCarga = function(){
		var cargaArchivo = Ext.getCmp('archivo');
		if (!cargaArchivo.isValid()){
			cargaArchivo.focus();
			return;
		}
		var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
		var extArchivo = Ext.getCmp('hidExtension');
		var objMessage = Ext.getCmp('pnlMsgValid1');
		
		/*Se establecen valores para campos hidden*/
		var formaCargaArchivo = Ext.getCmp('formaCargaArchivo1');
		var frmC = formaCargaArchivo.getForm()
		frmC.findField('hidNumDoctosMN1').setValue(formaCifrasCtrl.getComponent('hidCtrlNumDoctosMN1').getValue());
		frmC.findField('hidMontoTotalMNDespliegue1').setValue(formaCifrasCtrl.getComponent('hidCtrlMontoTotalMNDespliegue1').getValue());
		frmC.findField('hidMontoMinMNDespliegue1').setValue(formaCifrasCtrl.getComponent('hidCtrlMontoMinMNDespliegue1').getValue());
		frmC.findField('hidMontoMaxMNDespliegue1').setValue(formaCifrasCtrl.getComponent('hidCtrlMontoMaxMNDespliegue1').getValue());
		frmC.findField('hidNumDoctosDL1').setValue(formaCifrasCtrl.getComponent('hidCtrlNumDoctosDL1').getValue());
		frmC.findField('hidMontoMinDLDespliegue1').setValue(formaCifrasCtrl.getComponent('hidCtrlMontoMinDLDespliegue1').getValue());
		frmC.findField('hidMontoMaxDLDespliegue1').setValue(formaCifrasCtrl.getComponent('hidCtrlMontoMaxDLDespliegue1').getValue());
		frmC.findField('hidMontoTotalDLDespliegue1').setValue(formaCifrasCtrl.getComponent('hidCtrlMontoTotalDLDespliegue1').getValue());
		
		
		objMessage.hide();

		
		if (/^.*\.(txt)$/.test(ifile)){
			extArchivo.setValue('txt');
		}
		
		if(Ext.getCmp('formaValidaArchivo1')){
			// se removera objeto del contenedor principal
			pnl.remove('formaValidaArchivo1', true);
		}
		
		
		
		pnl.el.mask('Cargando...','x-mask-loading');
		formaCargaArchivo.getForm().submit({
			url: '13forma05ext_file.data.jsp',
			waitMsg: 'Enviando datos...',
			success: function(form, action) {
				pnl.el.unmask();
				var resp = action.result;
				var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
				var objMsg = Ext.getCmp('pnlMsgValid1');

				ObjGral.numeroProceso = resp.numeroProceso;
				ObjGral.operaFVPyme = resp.operaFVPyme;
				
				if(!resp.codificacionValida){
					new NE.cespcial.AvisoCodificacionArchivo({codificacion:resp.codificacionArchivo}).show();
				}else if(resp.flag){
					if(!resp.ok){
						
						var sinError = (resp.sinError).replace(/\|/g, '\n');
						var error = (resp.error).replace(/\|/g, '\n');
						var errorOtros =  (resp.errorOtros).replace(/\|/g, '\n');
						/*Se genera forma para mostrar detalle de documentos validados*/
						var formaValidaArchivo = new NE.manto.FormaValidacionArchivo({
							id: 'formaValidaArchivo1',
							dataSinError: sinError,
							dataConError: error,
							otrosErrores: errorOtros
						});
						
						pnl.add(formaValidaArchivo);
						pnl.doLayout();
						pnl.el.mask('Cargando...','x-mask-loading');
						var btnDescargaValida = formaValidaArchivo.getBtnDescarga();
						if(resp.existeFile){
							//Ext.getCmp('btnDescargaErrores').enabled();
							//Ext.getCmp('btnDescargaErrores').el.highlight('FFF700', {duration: 8, easing:'bounceOut'});
							btnDescargaValida.enable();
							btnDescargaValida.setHandler(function(boton, evento) {
								var forma = Ext.getDom('formAux');
								forma.action = resp.urlArchivo;
								forma.submit();
							});
						
						}else{
							btnDescargaValida.disable();
						}
						gridLayoutCarga.hide();
						pnl.el.unmask();
						
					}else{
						pnl.el.unmask();
						var dataResumen = [
							['No. total de documentos cargados', 
							 Ext.getCmp("hidCtrlNumDoctosMN1").getValue(), 
							 Ext.getCmp("hidCtrlNumDoctosDL1").getValue()
							],
							['Monto total de los documentos cargados', 
							 Ext.util.Format.number(Ext.getCmp("hidCtrlMontoTotalMNDespliegue1").getValue(), '$0,0.00'),
							 Ext.util.Format.number(Ext.getCmp("hidCtrlMontoTotalDLDespliegue1").getValue(), '$0,0.00')
							]
						];
						
						storeResumenMantoData.loadData(dataResumen);
						
						formaCargaArchivo.hide();
						pnl.add(pnlAcuse);
						pnl.doLayout();
						
						pnl.el.mask('Cargando...','x-mask-loading');
						storeDoctosMantoData.load({
							params:{
								informacion: 'ConsultaPreAcuseManto',
								numeroProceso: ObjGral.numeroProceso
							}
						});
					
					}
				}else{
					pnl.el.unmask();
					
					if(resp.statusThread!='0' && resp.statusThread!='300'){
						Ext.MessageBox.alert('Error',resp.mensajeEstatus);	
					}else if(resp.hayCaractCtrl){
						formaCargaArchivo.hide();
						gridLayoutCarga.hide();
						caracteresEspeciales.show();
						caracteresEspeciales.setMensaje(resp.mensajeResumen);
						caracteresEspeciales.setHandlerDescargarArchivo(function(){
							var archivo = resp.urlArchivoCaractEsp
							archivo = archivo.replace('/nafin','');
							var params = {nombreArchivo: archivo};
							
							formaCargaArchivo.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
							formaCargaArchivo.getForm().getEl().dom.submit();
						});
						
					}else{
						Ext.MessageBox.alert('Error',resp.msgError);
					}
				}

			},
			failure: NE.util.mostrarSubmitError
		})	
	}
	
	
	var procesarConsMantoDoctos  = function(store, arrRegistros, opts) {

		if (arrRegistros != null) {
			var gridColumnMod = gridDoctosManto.getColumnModel();
			
			if(!ObjGral.paramEpoPef){
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('FEC_ENTREGA'),true);//13
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('TIPO_COMPRA'),true);// 14
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('CVE_PRESUPUESTAL'),true);// 15
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('PERIODO'),true);// 16
			}
			
			if(ObjGral.operaFVPyme == 'S'){
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('FECVENC_PROVANTE'),false);//13
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('FECVENC_PROVNUEVA'),false);// 14
			}


			var dataTotales = [
				['Total Moneda Nacional', Ext.util.Format.number(Ext.getCmp("hidCtrlMontoTotalMNDespliegue1").getValue(), '$0,0.00')],
				['Total Dolares', Ext.util.Format.number(Ext.getCmp("hidCtrlMontoTotalDLDespliegue1").getValue(), '$0,0.00')],
				['Total Documentos en Moneda Nacional', Ext.getCmp("hidCtrlNumDoctosMN1").getValue()],
				['Total Documentos en Dolares', Ext.getCmp("hidCtrlNumDoctosDL1").getValue()]
			];
			
			storeTotalesMantoData.loadData(dataTotales);
			
			pnl.add(NE.util.getEspaciador(20));
			pnl.add(gridDoctosManto);
			pnl.add(gridDoctosMantoAcuse);
			pnl.add(gridTotalesManto);
			
			pnl.doLayout();
		}
		
		pnl.el.unmask();
	}
	
	var fnBtnTransmitirCallback = function(vpkcs7, vtextoFirmar, vnumeroProceso, varrDoctosManto){
		if (Ext.isEmpty(vpkcs7)) {
			pnl.el.unmask();
			return;	//Error en la firma. Termina...
		}else{
			//alert("siiii");
			//pnl.el.unmask();
			var formaCarga = Ext.getCmp('formaCargaArchivo1');
			Ext.Ajax.request({
				url: '13forma05ext.data.jsp',
				params:Ext.apply(formaCarga.getForm().getValues(),{
					informacion: 'TransmitirDoctos',
					TextoFirmado: vtextoFirmar,
					Pkcs7: vpkcs7,
					numeroProceso: vnumeroProceso,
					registrosDoctos: varrDoctosManto
				}),
				callback: procesarSuccessTransmitir
			});
			
		}
	}
	
	var fnBtnTransmitir = function(btn){
		pnl.el.mask('Procesando...', 'x-mask-loading');
		
		var textoFirmar = ''
		var arrDoctosManto = [];
		
		textoFirmar = 'Moneda Nacional \t No.total de documentos cargados '+Ext.getCmp("hidCtrlNumDoctosMN1").getValue()+' \t';
		textoFirmar += 'Monto total de los documentos cargados '+Ext.util.Format.number(Ext.getCmp("hidCtrlMontoTotalMNDespliegue1").getValue(), '$0,0.00')+ '\n';
		textoFirmar += 'Dolares \t No.total de documentos cargados '+Ext.getCmp("hidCtrlNumDoctosDL1").getValue()+' \t';
		textoFirmar += 'Monto total de los documentos cargados '+Ext.util.Format.number(Ext.getCmp("hidCtrlMontoTotalDLDespliegue1").getValue(), '$0,0.00')+ '\n';
		
		textoFirmar += 'Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesi�n de los derechos de cobro de las PYMES al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales.\n\n';
		textoFirmar += 'Clave del Proveedor|N�mero de Documento|Monto Anterior|Monto Nuevo|Fecha de Vencimiento Anterior|Fecha de Vencimiento Nueva|Fecha de Vencimiento del Proveedor Anterior|Fecha de Vencimiento del Proveedor Nueva|Fecha Entrega|Tipo Compra|Clave Presupuestaria|Periodo|Estatus Anterior|Estatus Nuevo|Causa\n';
		
		storeDoctosMantoData.each(function(record){
			arrDoctosManto.push(record.data);
			textoFirmar += record.data['CVE_PROVEEDOR']+'|'+record.data['NUM_DOCTO']+'|';
			textoFirmar += record.data['MONTO_ANTERIOR']+'|'+record.data['MONTO_NUEVO']+'|';
			textoFirmar += record.data['FECVENC_ANTERIOR']+'|'+record.data['FECVENC_NUEVA']+'|';
			textoFirmar += record.data['FECVENC_PROVANTE']+'|'+record.data['FECVENC_PROVNUEVA']+'|';
			textoFirmar += record.data['ESTATUS_ANTERIOR']+'|'+record.data['ESTATUS_NUEVO']+'|';
			textoFirmar += record.data['CAUSA']+'|'+record.data['USUARIO']+'|';
			textoFirmar += record.data['FEC_ENTREGA']+'|'+record.data['TIPO_COMPRA']+'|';
			textoFirmar += record.data['CVE_PRESUPUESTAL']+'|'+record.data['PERIODO']+'|';
			textoFirmar += '\n';
		});
		
		textoFirmar += 'Total Moneda Nacional: '+Ext.getCmp("hidCtrlNumDoctosMN1").getValue()+'\n';
		textoFirmar += 'Total Dolares: '+Ext.getCmp("hidCtrlNumDoctosDL1").getValue()+'\n';
		textoFirmar += 'Total Documentos Moneda Nacional: '+Ext.util.Format.number(Ext.getCmp("hidCtrlMontoTotalMNDespliegue1").getValue(), '$0,0.00')+'\n';
		textoFirmar += 'Total Documentos Dolares: '+Ext.util.Format.number(Ext.getCmp("hidCtrlMontoTotalDLDespliegue1").getValue(), '$0,0.00')+'\n';
		
		arrDoctosManto = Ext.encode(arrDoctosManto);
		
		NE.util.obtenerPKCS7(fnBtnTransmitirCallback, textoFirmar, ObjGral.numeroProceso, arrDoctosManto);
		
	}
	

	var procesarSuccessTransmitir = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			
			ObjGral.Acuse = resp.Acuse;
			ObjGral.Fecha = resp.Fecha;
			ObjGral.Hora = resp.Hora;
			ObjGral.Usuario = resp.Usuario;
			ObjGral.Folio = resp.Folio;
			
			var acuse = [
				['No. de Acuse',ObjGral.Acuse],
				['Fecha de Entrega',ObjGral.Fecha],
				['Hora de Carga',ObjGral.Hora],
				['Usuario',ObjGral.Usuario]
			];
			
			storeAcuseMantoData.loadData(acuse);
			storeDoctosMantoAcuseData.loadData(resp);

			pnl.el.unmask();
			
		}else{
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarConsMantoAcuseDoctos  = function(store, arrRegistros, opts) {

		if (arrRegistros != null) {
			//Ext.getCmp('toolbarPreAcu1').hide();
			gridDoctosManto.hide();
			
			
			var gridColumnMod = gridDoctosMantoAcuse.getColumnModel();
			
			if(!ObjGral.paramEpoPef){
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('FEC_ENTREGA'),true);
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('TIPO_COMPRA'),true);
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('CVE_PRESUPUESTAL'),true);
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('PERIODO'),true);
			}
			
			if(ObjGral.operaFVPyme == 'S'){
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('FECVENC_PROVANTE'),false);
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('FECVENC_PROVNUEVA'),false);
			}
			
			gridAcuseManto.show();
			pnlAcuse.setTitle('La autentificai�n se llev� a cabo con �xito '+ObjGral.Folio);
			//pnl.add(gridDoctosMantoAcuse);
			gridDoctosMantoAcuse.show();
			pnl.doLayout();
			
			Ext.getCmp('btnCancelarTransmitir').hide();
			Ext.getCmp('btnTransmitir').hide();
			Ext.getCmp('btnImprimirAcuse').show();
			Ext.getCmp('btnGenerarArchivoAcuse').show();
			Ext.getCmp('btnSalirAcuse').show();
		}
	}
	
	var fnBtnGenerarArchivoAcuse = function(btn){
	
		btn.setIconClass('loading-indicator');
		
		var regResumenMantoData = [];
		var regAcuseMantoData = [];
		var regDoctosMantoAcuseData = [];
		var regTotalesMantoData = [];
		
		storeResumenMantoData.each(function(record) {
			regResumenMantoData.push(record.data);
		});
		
		storeAcuseMantoData.each(function(record) {
			regAcuseMantoData.push(record.data);
		});
		
		storeDoctosMantoAcuseData.each(function(record) {
			regDoctosMantoAcuseData.push(record.data);
		});
		
		storeTotalesMantoData.each(function(record) {
			regTotalesMantoData.push(record.data);
		});
		
		Ext.Ajax.request({
			url: '13forma05ext.data.jsp',
			params:{
				informacion: 'GenerarCsvAcuse',
				regResumenMantoData: Ext.encode(regResumenMantoData),
				regAcuseMantoData: Ext.encode(regAcuseMantoData),
				regDoctosMantoAcuseData: Ext.encode(regDoctosMantoAcuseData),
				regTotalesMantoData: Ext.encode(regTotalesMantoData),
        numeroProceso: ObjGral.numeroProceso
			},
			callback: procesarSuccessGenerarCsvAcuse
		});
	}
	

	var procesarSuccessGenerarCsvAcuse = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var btnGenerarArchivoAcuse = Ext.getCmp('btnGenerarArchivoAcuse');
					
			btnGenerarArchivoAcuse.setIconClass('');
			var btnAbrirArchivoAcuse = Ext.getCmp('btnAbrirArchivoAcuse');
			btnAbrirArchivoAcuse.show();
			btnAbrirArchivoAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnAbrirArchivoAcuse.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = resp.urlArchivo;
				forma.submit();
			});
			
		}else{
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var fnBtnImprimirAcuse = function(btn){
	
		btn.setIconClass('loading-indicator');
		
		var regResumenMantoData = [];
		var regAcuseMantoData = [];
		var regDoctosMantoAcuseData = [];
		var regTotalesMantoData = [];
		
		storeResumenMantoData.each(function(record) {
			regResumenMantoData.push(record.data);
		});
		
		storeAcuseMantoData.each(function(record) {
			regAcuseMantoData.push(record.data);
		});
		
		storeDoctosMantoAcuseData.each(function(record) {
			regDoctosMantoAcuseData.push(record.data);
		});
		
		storeTotalesMantoData.each(function(record) {
			regTotalesMantoData.push(record.data);
		});
		
		Ext.Ajax.request({
			url: '13forma05ext.data.jsp',
			params:{
				informacion: 'GenerarPdfAcuse',
				regResumenMantoData: Ext.encode(regResumenMantoData),
				regAcuseMantoData: Ext.encode(regAcuseMantoData),
				regDoctosMantoAcuseData: Ext.encode(regDoctosMantoAcuseData),
				regTotalesMantoData: Ext.encode(regTotalesMantoData),
        numeroProceso: ObjGral.numeroProceso
			},
			callback: procesarSuccessGenerarPdfAcuse
		});
	}
	
	var procesarSuccessGenerarPdfAcuse = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var btnImprimirAcuse = Ext.getCmp('btnImprimirAcuse');
					
			btnImprimirAcuse.setIconClass('');
			var btnAbrirPdfAcuse = Ext.getCmp('btnAbrirPdfAcuse');
			btnAbrirPdfAcuse.show();
			btnAbrirPdfAcuse.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnAbrirPdfAcuse.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = resp.urlArchivo;
				forma.submit();
			});
			
		}else{
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}

/*------------------------------------------------------------------------DATA*/

	var layout = [
		['1', 'No. Proveedor', 'c', '25', 'Deber� ser �nico en la cadena Productiva', 'SI'],
		['2', 'No Documento', 'c', '15', 'Deber� ser �nico en la cadena Productiva', 'SI'],
		['3', 'Fecha Emisi�n', 'd', '10', 'Formato: dd/', 'SI'],
		['4', 'Moneda', 'd', '10', '1= Pesos 54= D�lares', 'SI'],
		['5', 'Tipo de Cambio', 'n', '1', 'V: Fecha de Vencimiento<br>M: Monto<br>B: Baja<br>F: Descuento F�sico<br>P: Pago Anticipado<br>R: Boqueado o Negociable<br>L: Negociable a Bloqueado <br> D:Pendiente Duplicado a Negociable', 'SI'],
		['6', 'Monto Anterior', 'n', '20', 'Incluye 2 decimales', 'SI'],
		['7', 'Monto Nuevo', 'n', '20', 'Incluye 2 decimales', 'SI'],
		['8', 'Causa de Cambio', 'c', '260', ' ', 'SI'],
		['9', 'Fecha de Vencimiento Anterior ', 'd', '10', 'Formato: dd/mm/aaaa', 'SI para todo tipo de<br>Cambio "V" y "M"'],
		['10', 'Fecha de Vencimiento Nueva', 'd', '10', 'Formato: dd/mm/aaaa', 'SI para todo tipo de<br>Cambio "V" y "M"'],
		['11', 'Fecha de Vencimiento Proveedor', 'd', '10', 'Formato: dd/mm/aaaa<br>Debera ser menor a la fecha<br>de vencimiento', 'NO'],
		['12', 'Fecha Entrega', 'd', '10', 'Formato: dd/mm/aaaa', 'NO'],
		['13', 'Tipo Compra', 'c', '1', 'L=Licitaci�n<br>I= Invitaci�n<br>A= Asignaci�n Directa', 'NO'],
		['14', 'Clave Presupuestal', 'n', '4', 'Formato: dd/mm/aaaa', 'NO'],
		['15', 'Periodo', 'n', '3', ' ', 'NO']
	];

/*----------------------------------------------------------------------STORES*/
	var storeLayoutData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMERO'},
			{name: 'CAMPO'},
			{name: 'TIPO_DATO'},
			{name: 'LONGITUD'},
			{name: 'OBSERVACIONES'},
			{name: 'OBLIGATORIO'}
		],
		data:layout
	});
	
	
	var storeDoctosMantoData = new Ext.data.JsonStore({
		url: '13forma05ext.data.jsp',
		root : 'registrosDoctos',
		fields : [
			{name: 'CVE_PROVEEDOR'},
			{name: 'NUM_DOCTO'},
			{name: 'MONTO_ANTERIOR'},
			{name: 'MONTO_NUEVO'},
			{name: 'FECVENC_ANTERIOR'},
			{name: 'FECVENC_NUEVA'},
			{name: 'FECVENC_PROVANTE'},
			{name: 'FECVENC_PROVNUEVA'},
			{name: 'ESTATUS_ANTERIOR'},
			{name: 'ESTATUS_NUEVO'},
			{name: 'CAUSA'},
			{name: 'USUARIO'},
			{name: 'FEC_ENTREGA'},
			{name: 'TIPO_COMPRA'},
			{name: 'CVE_PRESUPUESTAL'},
			{name: 'PERIODO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarConsMantoDoctos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsMantoDoctos(null, null, null);
				}
			}
		}
	});
	
	var storeResumenMantoData = new Ext.data.ArrayStore({
		fields:[
			{name: 'TITULOS'},
			{name: 'NACIONAL'},
			{name: 'DOLAR'}
		]
	});
	
	var storeTotalesMantoData = new Ext.data.ArrayStore({
		fields:[
			{name: 'TITULOS'},
			{name: 'VALORES'}
		]
	});
	
	var storeAcuseMantoData = new Ext.data.ArrayStore({
		fields:[
			{name: 'TITULOS'},
			{name: 'VALORES'}
		]
	});
	
	var storeDoctosMantoAcuseData = new Ext.data.JsonStore({
		root : 'registrosMantoDoctos',
		fields : [
			{name: 'CVE_PROVEEDOR'},
			{name: 'NUM_DOCTO'},
			{name: 'MONTO_ANTERIOR'},
			{name: 'MONTO_NUEVO'},
			{name: 'FECVENC_ANTERIOR'},
			{name: 'FECVENC_NUEVA'},
			{name: 'FECVENC_PROVANTE'},
			{name: 'FECVENC_PROVNUEVA'},
			{name: 'CAMBIO_MOTIVO'},
			{name: 'CAUSA'},
			{name: 'NOM_USUARIO'},
			{name: 'FEC_ENTREGA'},
			{name: 'TIPO_COMPRA'},
			{name: 'CVE_PRESUPUESTAL'},
			{name: 'PERIODO'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarConsMantoAcuseDoctos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsMantoAcuseDoctos(null, null, null);
				}
			}
		}
	});
	
/*-----------------------------------------------------------------------GRIDS*/	

	var gridLayoutCarga = new Ext.grid.GridPanel({
		id:'gridLayoutCarga1',
		store: storeLayoutData,
		margins: '20 0 0 0',
		width: 600,
		height:400,
		columnLines: true,
		style: 'margin: 0 auto',
		hidden: true,
		columns: [
			{//1
				header: 'No.',
				tooltip: 'N�mero de Campo',
				dataIndex: 'NUMERO',
				sortable: true,
				width: 50,
				resizable: true,
				hidden: false
			},
			{//2
				header: 'CAMPO',
				tooltip: 'Nombre del Campo',
				dataIndex: 'CAMPO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			},
			{//2
				header: 'TIPO DATO',
				tooltip: 'Tipo de Dato',
				dataIndex: 'TIPO_DATO',
				sortable: true,
				width: 50,
				resizable: true,
				hidden: false
			},
			{//2
				header: 'LONGITUD MAXIMA',
				tooltip: 'Longitud M�xima',
				dataIndex: 'LONGITUD',
				sortable: true,
				width: 50,
				resizable: true,
				hidden: false
			},
			{//2
				header: 'OBSERVACIONES',
				tooltip: 'Observaciones',
				dataIndex: 'OBSERVACIONES',
				sortable: true,
				width: 220,
				resizable: true,
				hidden: false
			},
			{//2
				header: 'OBLIGATORIO',
				tooltip: 'Campo Obligatorio',
				dataIndex: 'OBLIGATORIO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false
			}
		]
	});
	
	
	var gridResumenManto = new Ext.grid.GridPanel({
		id:'gridResumenManto1',
		store: storeResumenMantoData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		width: 454,
		height:90,
		frame: true,
		columnLines: true,
		columns: [
			{ header: ' ', tooltip: '', dataIndex: 'TITULOS', sortable: true, width: 200,  hidden: false},
			{ header: 'Moneda Nacional', tooltip: 'Moneda Nacional', dataIndex: 'NACIONAL', width: 120, hidden: false},
			{ header: 'Dolares', tooltip: 'Dolares', dataIndex: 'DOLAR', sortable: true, width: 115,  hidden: false}
		]
	});
	
	
	
	var gridDoctosManto = new Ext.grid.GridPanel({
		id:'gridDoctosManto1',
		store: storeDoctosMantoData,
		style: 'margin:0 auto;',
		margins: '20 0 0 0',
		width: 840,
		height:300,
		columnLines: true,
		frame: true,
		columns: [
			{//
				header: 'Clave del Proveedor',
				tooltip: 'Clave del Proveedor',
				dataIndex: 'CVE_PROVEEDOR',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Monto Anterior',
				tooltip: 'Monto Anterior',
				dataIndex: 'MONTO_ANTERIOR',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Monto Nuevo',
				tooltip: 'Monto Nuevo',
				dataIndex: 'MONTO_NUEVO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Fecha de Vencimiento Anterior',
				tooltip: 'Fecha de Vencimiento Anterior',
				dataIndex: 'FECVENC_ANTERIOR',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Fecha de Vencimiento Nueva',
				tooltip: 'Fecha de Vencimiento Nueva',
				dataIndex: 'FECVENC_NUEVA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Fecha de Vencimiento Prov Anterior',
				tooltip: 'Fecha de Vencimiento Prov Anterior',
				dataIndex: 'FECVENC_PROVANTE',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: true
			},
			{//
				header: 'Fecha de Vencimiento Prov Nueva',
				tooltip: 'Fecha de Vencimiento Prov Nueva',
				dataIndex: 'FECVENC_PROVNUEVA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: true
			},
			{//
				header: 'Estatus Anterior',
				tooltip: 'Estatus Anterior',
				dataIndex: 'ESTATUS_ANTERIOR',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Estatus Nuevo',
				tooltip: 'Estatus Nuevo',
				dataIndex: 'ESTATUS_NUEVO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Causa',
				tooltip: 'Causa',
				dataIndex: 'CAUSA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Usuario',
				tooltip: 'Usuario',
				dataIndex: 'USUARIO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				renderer: function (causa, columna, registro){
					return ObjGral.strNombreUsuario;
				}
					
			},
			{//
				header: 'Fecha Entrega',
				tooltip: 'Fecha Entrega',
				dataIndex: 'FEC_ENTREGA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Tipo Compra',
				tooltip: 'Tipo Compra',
				dataIndex: 'TIPO_COMPRA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Clave Presupuestaria',
				tooltip: 'Clave Presupuestaria',
				dataIndex: 'CVE_PRESUPUESTAL',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Periodo',
				tooltip: 'Periodo',
				dataIndex: 'PERIODO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			}
		]
		
	});
	
	var gridTotalesManto = new Ext.grid.GridPanel({
		id:'gridTotalesManto1',
		store: storeTotalesMantoData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		width: 840,
		height:115,
		frame: true,
		hideHeaders: true,
		columnLines: true,
		columns: [
			{ header: ' ', tooltip: '', dataIndex: 'TITULOS', sortable: true, width: 380,  hidden: false},
			{ header: ' ', tooltip: '', dataIndex: 'VALORES', width: 430, hidden: false}
		]
	});
	
	var gridAcuseManto = new Ext.grid.GridPanel({
		id:'gridAcuseManto1',
		store: storeAcuseMantoData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		width: 454,
		height:115,
		hidden: true,
		frame: true,
		hideHeaders: true,
		columnLines: true,
		columns: [
			{ header: ' ', tooltip: '', dataIndex: 'TITULOS', sortable: true, width: 200,  hidden: false},
			{ header: ' ', tooltip: '', dataIndex: 'VALORES', width: 235, hidden: false}

		]
	});
	
	var gridDoctosMantoAcuse = new Ext.grid.GridPanel({
		id:'gridDoctosMantoAcuse1',
		store: storeDoctosMantoAcuseData,
		margins: '20 0 0 0',
		width: 840,
		height:300,
		style: 'margin:0 auto;',
		columnLines: true,
		frame: true,
		hidden: true,
		columns: [
			{//
				header: 'Clave del Proveedor',
				tooltip: 'Clave del Proveedor',
				dataIndex: 'CVE_PROVEEDOR',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Monto Anterior',
				tooltip: 'Monto Anterior',
				dataIndex: 'MONTO_ANTERIOR',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Monto Nuevo',
				tooltip: 'Monto Nuevo',
				dataIndex: 'MONTO_NUEVO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Fecha de Vencimiento Anterior',
				tooltip: 'Fecha de Vencimiento Anterior',
				dataIndex: 'FECVENC_ANTERIOR',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Fecha de Vencimiento Nueva',
				tooltip: 'Fecha de Vencimiento Nueva',
				dataIndex: 'FECVENC_NUEVA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Fecha de Vencimiento Prov Anterior',
				tooltip: 'Fecha de Vencimiento Prov Anterior',
				dataIndex: 'FECVENC_PROVANTE',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: true
			},
			{//
				header: 'Fecha de Vencimiento Prov Nueva',
				tooltip: 'Fecha de Vencimiento Prov Nueva',
				dataIndex: 'FECVENC_PROVNUEVA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: true
			},
			{//
				header: 'Movimiento',
				tooltip: 'Movimiento',
				dataIndex: 'CAMBIO_MOTIVO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Causa',
				tooltip: 'Causa',
				dataIndex: 'CAUSA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Usuario',
				tooltip: 'Usuario',
				dataIndex: 'NOM_USUARIO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				renderer: function (causa, columna, registro){
					return ObjGral.strNombreUsuario;
				}
			},
			{//
				header: 'Fecha Entrega',
				tooltip: 'Fecha Entrega',
				dataIndex: 'FEC_ENTREGA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Tipo Compra',
				tooltip: 'Tipo Compra',
				dataIndex: 'TIPO_COMPRA',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Clave Presupuestaria',
				tooltip: 'Clave Presupuestaria',
				dataIndex: 'CVE_PRESUPUESTAL',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//
				header: 'Periodo',
				tooltip: 'Periodo',
				dataIndex: 'PERIODO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			}
		]
		
	});


/*---------------------------------------------------------------------PANELES*/	
	
	var panelGeneral = new Ext.Panel({
		frame: false,
		border: true,
		title: 'Mantenimiento Masivo de Documentos',
		width: 940,
		items:[
			NE.util.getEspaciador(20)
		]
		
	});
	

/*------------------------------------------------IMPLEMENTACION DE LAS CLASES*/

/*Se genera forma para el control de cifras*/
	var formaCifrasCtrl = new NE.manto.FormaCifrasCtrl({id:'formaCifrasCtrl1'});
	
/*Se define funcionamiento del Bonton ENVIAR de la forma Cifras de Control*/
	formaCifrasCtrl.setHandlerBtnEnviar(fnBtnContinuar);

/*Se define funcionamiento del Bonton LIMPIAR de la forma Cifras de Control*/
	formaCifrasCtrl.setHandlerBtnLimpiar(fnBtnLimpiar);

/*--------------------------------------------------------PANEL*/
	var pnlAcuse = new Ext.Panel({
		id: 'pnlAcuse1',
		frame: true,
		title: ' ',
		width: 465,
		style: 'margin:0 auto;',
		items:[gridResumenManto,
				gridAcuseManto,
				{ 
					xtype:   'panel',  
					html:		'Al transmitir este MENSAJE DE DATOS, usted est&aacute; bajo su responsabilidad haciendo negociables '+
								'los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesi&oacute;n de los derechos de cobro '+
								'de las PYMES al INTERMEDIARIO FINANCIERO, dicha transmisi&oacute;n tendr&aacute; validez para todos los efectos legales', 
					//cls:		'x-form-item', 
					frame:true,
					style: { 
						width:   		'100%', 
						textAlign: 		'left'
					} 
				}
		],
		bbar: {
			xtype: 'toolbar',
			id:'toolbarAcu1',
			items: [
				'->',
				'-',
				{
					text: 'Transmitir Documentos Negociables',
					id: 'btnTransmitir',
					iconCls: 'icoAceptar',
					handler: fnBtnTransmitir
				},
				{
					text: 'Imprimir',
					id: 'btnImprimirAcuse',
					hidden: true,
					handler: fnBtnImprimirAcuse
				},
				{
					text: 'Abrir PDF',
					id: 'btnAbrirPdfAcuse',
					hidden: true
					//handler: fnBtnTransmitir
				},
				'-',
				{
					text: 'Cancelar',
					id: 'btnCancelarTransmitir',
					iconCls: 'cancelar',
					handler: function(btn){
						window.location.href='13forma05ext.jsp';
					}
				},
				{
					text: 'Generar Archivo',
					id: 'btnGenerarArchivoAcuse',
					handler: fnBtnGenerarArchivoAcuse,
					hidden: true
				},
				{
					text: 'Abrir Archivo',
					id: 'btnAbrirArchivoAcuse',
					hidden: true
				},
				'-',
				{
					text: 'Salir',
					id: 'btnSalirAcuse',
					handler: function(btn){
						window.location.href='13forma05ext.jsp';
					},
					hidden: true
				}
			]
		}
	});
	
	var caracteresEspeciales = new NE.cespcial.CaracteresEspeciales({hidden:true});
	caracteresEspeciales.setHandlerAceptar(function(){
		Ext.getCmp('formaCargaArchivo1').show();
		caracteresEspeciales.hide();
	});

/*--------------------------------------------------------CONTENEDOR PRINCIPAL*/
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto; text-align:center;',
		width: 949,
		items: [
			//panelGeneral,
			formaCifrasCtrl,
			caracteresEspeciales
		]
	});
	
	
	pnl.el.mask('Cargando...','x-mask-loading');	
	Ext.Ajax.request({
		url: '13forma05ext.data.jsp',
		params: {
			informacion: 'DatosIniciales'
		},
		callback: procesarSuccessDatosIniciales
	});  
	
	
} );