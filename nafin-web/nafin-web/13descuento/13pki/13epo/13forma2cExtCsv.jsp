<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,
	java.math.*,
	javax.naming.*, 
	netropology.utilerias.*, 
	com.netro.descuento.*,
	com.netro.pdf.*,
	netropology.utilerias.negocio.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp" 
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%
	JSONObject jsonObj = new JSONObject();

 String strPendiente = (request.getParameter("strPendiente") != null) ? request.getParameter("strPendiente") : "N";
 String strPreNegociable = (request.getParameter("strPreNegociable") != null) ? request.getParameter("strPreNegociable") : "N";
 String doctoDuplicados = (request.getParameter("doctoDuplicados") != null) ? request.getParameter("doctoDuplicados") : "N";	
 String accion = (request.getParameter("accion") != null) ? request.getParameter("accion") : "";


	if(doctoDuplicados.equals("S")){ doctoDuplicados = "'N','S'"; }
	if(doctoDuplicados.equals("N")){ doctoDuplicados = "'N'"; }
	
	String hidNumAcuse = request.getParameter("acuse");
	Acuse acuse = new Acuse(hidNumAcuse);
	String hidRecibo = request.getParameter("hidRecibo");
	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
	String ses_ic_epo = iNoCliente;
	
	CrearArchivosAcuses  generaAch =  new CrearArchivosAcuses();
	generaAch.setAcuse(hidNumAcuse);
	generaAch.setTipoArchivo("CSV");	
	
	generaAch.setAcuse(hidNumAcuse);
	generaAch.setTipoArchivo("CSV");
	generaAch.setStrDirectorioTemp(strDirectorioTemp);
	generaAch.setUsuario(iNoUsuario);	
	generaAch.setVersion("PANTALLA");	
	int existeArch= 	generaAch.existerArcAcuse();
	
try{ 
	
if(existeArch==0)  {

	//	Variables locales y de despliege
	int i =0,  numCamposAdicionales=0, iNumMoneda =0;
	String nombrePyme="", numeroDocumento="", fechaDocumento="", fechaVencimiento="", fechaVencimientoPyme="", monto="",  referencia="",
			 porcentajeAnticipo="", montoDescuento="", clavePyme="", claveMoneda="", campo1="", campo2="", campo3="", campo4="",
			 campo5="", monedaNombre="", tipoFactoraje="", NombreIF="", sNombreBeneficiario="", sPorcentajeBeneficiario="",
			 sMontoBeneficiario="", numeroSIAFF = "", fechaEntrega = "", tipoCompra = "", clavePresupuestaria = "",periodo = "",
			 Mandante = "", Factoraje = "", ic_estatus_docto 	= "",
			 fechaVencMin = "", fechaVencMax = "", spubdocto_venc = "",	 fechaPlazoPagoFVP = "",  plazoMax1FVP  = "", plazoMax2FVP  = "",duplicado ="";			 
	StringBuffer contenidoArchivoA = new StringBuffer();
	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = null;
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	CargaDocumento CargaDocumentos= ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
	
	boolean 	estaHabilitadoNumeroSIAFF 	= BeanParamDscto.estaHabilitadoNumeroSIAFF(ses_ic_epo); // VERIFICAR SI ESTA HABILITADO EL NUMERO SIAFF 
	//FODEA 050 - VALC - 10/2008	
	
		// Verificar si esta habilitada la Publicacion EPO PEF
	String bestaHabilitadaPublicacionEPOPEF ="N";
	boolean 	estaHabilitadaPublicacionEPOPEF 	=  CargaDocumentos.estaHabilitadaLaPublicacionEPOPEF(ses_ic_epo);
	if(estaHabilitadaPublicacionEPOPEF) bestaHabilitadaPublicacionEPOPEF ="S";


	boolean bOperaFactConMandato=false;
	boolean bTipoFactoraje=false;	
	boolean bOperaFactorajeVencido = false;
	boolean bOperaFactorajeDistribuido = false ;
	boolean bOperaFactorajeVencidoInfonavit = false; //Fodea-042-2009  Vencimiento Infonavit	
	boolean bValidaDuplicidad = false;  //Fodea 016-2015
	
	Hashtable alParamEPO1 = new Hashtable(); 
	alParamEPO1 = BeanParamDscto.getParametrosEPO(ses_ic_epo,1);	
	if (alParamEPO1!=null) {
		bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		bOperaFactorajeVencido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
		bOperaFactorajeDistribuido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
  	bOperaFactorajeVencidoInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;	  
		bValidaDuplicidad = ("N".equals(alParamEPO1.get("VALIDA_DUPLIC").toString()))?false:true;
	}
	bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido ||bOperaFactConMandato)?true:false;
	
	Vector vFechasVencimiento = CargaDocumentos.getFechasVencimiento(iNoCliente, 1);
	 fechaVencMin = vFechasVencimiento.get(0).toString();
	 fechaVencMax = vFechasVencimiento.get(1).toString();
	 spubdocto_venc = (vFechasVencimiento.get(2)==null)?"":vFechasVencimiento.get(2).toString();
	 fechaPlazoPagoFVP = (vFechasVencimiento.get(3)==null)?"":vFechasVencimiento.get(3).toString();
	 plazoMax1FVP = (vFechasVencimiento.get(4)==null)?"":vFechasVencimiento.get(4).toString();
	 plazoMax2FVP = (vFechasVencimiento.get(5)==null)?"":vFechasVencimiento.get(5).toString();
	 
	Vector vNombres = CargaDocumentos.getNombresCamposAdicionales(ses_ic_epo);		
	Calendar fecha = Calendar.getInstance();

	numCamposAdicionales=vNombres.size();

	try {
   
		if(strPendiente.equals("S"))
      contenidoArchivoA.append("Pendientes Negociables"+"\n");
    else if(strPreNegociable.equals("S"))
      contenidoArchivoA.append("Pre Negociables "+"\n");
    else
      contenidoArchivoA.append("Negociables"+"\n");

		contenidoArchivoA.append("Nombre Proveedor,");
		contenidoArchivoA.append("Número de Documento,");
		contenidoArchivoA.append("Fecha Emisión,");
		contenidoArchivoA.append("Fecha Vencimiento ,");
		if("S".equals(operaFVPyme)) {
			contenidoArchivoA.append("Fecha Vencimiento Proveedor,");
		}//if("S".equals(operaFVPyme))
		contenidoArchivoA.append("Moneda"+",");
		contenidoArchivoA.append("Tipo Factoraje ,");
		contenidoArchivoA.append("Monto,");
		contenidoArchivoA.append("Porcentaje de Descuento ,");
		contenidoArchivoA.append("Monto a Descontar,");
		contenidoArchivoA.append("Referencia,");
		if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) { //Factoraje Vencido, Mandato, Vencimiento Infonavit
			contenidoArchivoA.append("Nombre IF,");
		}
		if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit) { //Factoraje Distribuido,  Vencimiento Infonavit
			contenidoArchivoA.append("Nombre Beneficiario,");
			contenidoArchivoA.append("Porcentaje Beneficiario,");
			contenidoArchivoA.append("Monto Beneficiario ,");
		}
		for(int a=0; a<vNombres.size(); a++) {
			contenidoArchivoA.append((String)vNombres.get(a)+",");
		}
		if(estaHabilitadoNumeroSIAFF) {
			contenidoArchivoA.append("Digito Identificador,");
		}
		if(bestaHabilitadaPublicacionEPOPEF.equalsIgnoreCase("S")){
			contenidoArchivoA.append("Fecha Entrega,");
			contenidoArchivoA.append("Tipo Compra,");
			contenidoArchivoA.append("Clave Presupuestaria ,");
			contenidoArchivoA.append("Periodo,");
		}
		
		if (bOperaFactConMandato) {
			contenidoArchivoA.append("Mandante"+",");
		}
		
		if(bValidaDuplicidad){
		contenidoArchivoA.append("Notificado como posible Duplicado"+",");
		}
		
		contenidoArchivoA.append("\n");

    if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
        ic_estatus_docto = "30"; // Pendientes Negociables
    else if(strPreNegociable.equals("S"))   // Si se trata de documentos PreNegociables
        ic_estatus_docto = "28"; // Pendientes Negociables
    else
        ic_estatus_docto = "2"; // Negociables
		//Despliegue de los documentos
		System.out.println("ic_estatus_docto  "+ic_estatus_docto);
		
		Vector vDoctosCargados = CargaDocumentos.getComDoctosCargadosConIcDocumento(hidNumAcuse, strAforo, ses_ic_epo, ic_estatus_docto, strAforoDL, sesIdiomaUsuario);
		int registros = 0;
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{
			Vector vd = (Vector)e.nextElement();
			nombrePyme = vd.get(0).toString().replace(',',' ');
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			monedaNombre = vd.get(4).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10)==null?"":vd.get(10).toString().replace(',',' ');
			tipoFactoraje = vd.get(7).toString();
			Mandante 	= vd.get(27).toString().replace(',',' ');
			fechaVencimientoPyme = vd.get(20)==null?"":vd.get(20).toString();
			Factoraje = vd.get(28).toString();		
			 duplicado = vd.get(29).toString(); //F038-2014
			NombreIF = "";
			if(bOperaFactorajeVencido ) { // Para factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF 	= vd.get(11).toString().replace(',',' ');
				}
			}			
			// Fodea 023 2009  Mandato 
			if( bOperaFactConMandato &&  tipoFactoraje.equals("M") )  {
				NombreIF 	= vd.get(11).toString().replace(',',' ');
			}				
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido  ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(17).toString().replace(',',' ');
					sPorcentajeBeneficiario = vd.get(18).toString();
					sMontoBeneficiario = vd.get(19).toString();
				}
			}			
			// Fodea 042-2009 Vencimiento Infonavit
			if( bOperaFactorajeVencidoInfonavit &&  tipoFactoraje.equals("I") )  {
					NombreIF 	= vd.get(11).toString().replace(',',' ');
					porcentajeAnticipo = "100";			
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(17).toString().replace(',',' ');
					sPorcentajeBeneficiario = vd.get(18).toString();
					sMontoBeneficiario = vd.get(19).toString();
			}			
			//FODEA 050 - VALC - 10/2008			
			fechaEntrega			= vd.get(23).toString();
			tipoCompra = vd.get(24).toString();			
			clavePresupuestaria = vd.get(25).toString();
			periodo = vd.get(26).toString();
			
			if(tipoFactoraje.equals("N"))			tipoFactoraje = "Normal";
			else if(tipoFactoraje.equals("M")) 	tipoFactoraje = "Mandato";
			else if(tipoFactoraje.equals("V")) 	tipoFactoraje = "Vencido";
			else if(tipoFactoraje.equals("D")) 	tipoFactoraje = "Distribuido";
			else if(tipoFactoraje.equals("C")) {
				tipoFactoraje = "Notas de Credito";
				porcentajeAnticipo = "100";
				montoDescuento = monto;
			}
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if(estaHabilitadoNumeroSIAFF){
				numeroSIAFF = getNumeroSIAFF(vd.get(22).toString(),vd.get(21).toString());
			}			
			
		
			contenidoArchivoA.append( nombrePyme+",");
			contenidoArchivoA.append( numeroDocumento+",");
			contenidoArchivoA.append(fechaDocumento+",");
			contenidoArchivoA.append(fechaVencimiento+",");
			if("S".equals(operaFVPyme)) {
				contenidoArchivoA.append(fechaVencimientoPyme+",");
			}//if("S".equals(operaFVPyme))
			contenidoArchivoA.append( monedaNombre+",");
			contenidoArchivoA.append(Factoraje+",");
			contenidoArchivoA.append(monto+",");
			contenidoArchivoA.append(Comunes.formatoDecimal(porcentajeAnticipo,0)+" %,");
			contenidoArchivoA.append(montoDescuento+",");
			contenidoArchivoA.append(referencia+",");
			if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) {  //Factoraje Vencido, Mandato, Vencimiento Infonavit
				contenidoArchivoA.append(NombreIF+",");
			}
			if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit) { //Factoraje Distribuido, Vencimiento Infonavit
				contenidoArchivoA.append(sNombreBeneficiario+",");
				contenidoArchivoA.append((sPorcentajeBeneficiario.equals("")?sPorcentajeBeneficiario:Comunes.formatoDecimal(sPorcentajeBeneficiario,2)+" %")+",");
				contenidoArchivoA.append((sMontoBeneficiario.equals("")?sMontoBeneficiario:sMontoBeneficiario)+",");
			}
			for (i=1;i<=numCamposAdicionales;i++){
				contenidoArchivoA.append((String)vd.get(11+i)+",");
			}
			if(estaHabilitadoNumeroSIAFF){
				contenidoArchivoA.append(numeroSIAFF+",");
			}
			//FODEA 050 - VALC - 10/2008
			if(bestaHabilitadaPublicacionEPOPEF.equalsIgnoreCase("S")){
				contenidoArchivoA.append(fechaEntrega+",");
				contenidoArchivoA.append(tipoCompra.replace(',',' ')+",");
				contenidoArchivoA.append(clavePresupuestaria+",");
				contenidoArchivoA.append(periodo+",");
			}
			
		if (bOperaFactConMandato) {
				contenidoArchivoA.append( Mandante+",");
		}
		
		if(bValidaDuplicidad){
		contenidoArchivoA.append( duplicado+",");
		}
		
			registros++;
			contenidoArchivoA.append("\n");
		}//fin for

		contenidoArchivoA.append("   "+"\n");
		/* INICIO NO NEGOCIABLES*/
    if(strPendiente.equals("S"))
      contenidoArchivoA.append("Pendientes No Negociables"+"\n");
    else if(strPreNegociable.equals("S"))
      contenidoArchivoA.append("Pre No Negociables"+"\n");
    else
      contenidoArchivoA.append("No Negociables"+"\n");
		contenidoArchivoA.append("Nombre Proveedor,");
		contenidoArchivoA.append("Número de Documento ,");
		contenidoArchivoA.append("Fecha Emisión ,");
		contenidoArchivoA.append("Fecha Vencimiento,");
		if("S".equals(operaFVPyme)) {
			contenidoArchivoA.append("Fecha Vencimiento Proveedor,");
		}//if("S".equals(operaFVPyme))
		contenidoArchivoA.append("Moneda"+",");
		contenidoArchivoA.append("Tipo Factoraje,");
		contenidoArchivoA.append("Monto,");
		contenidoArchivoA.append("Porcentaje de Descuento,");
		contenidoArchivoA.append("Monto a Descontar,");
		contenidoArchivoA.append("Referencia,");
		if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) {
			contenidoArchivoA.append("Nombre IF ,");
		}
		if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit) {
			contenidoArchivoA.append("Nombre Beneficiario,");
			contenidoArchivoA.append("Porcentaje Beneficiario,");
			contenidoArchivoA.append("Monto Beneficiario,");
		}		
		for(int a=0; a<vNombres.size(); a++) {
			contenidoArchivoA.append((String)vNombres.get(a)+",");
		}
		if(estaHabilitadoNumeroSIAFF) {
			contenidoArchivoA.append("Digito Identificador,");
		}
		if(bestaHabilitadaPublicacionEPOPEF.equalsIgnoreCase("S")){
			contenidoArchivoA.append("Fecha Entrega,");
			contenidoArchivoA.append("Tipo Compra,");
			contenidoArchivoA.append("Clave Presupuestaria"+",");
			contenidoArchivoA.append("Periodo,");
		}
		
		if (bOperaFactConMandato) {
		contenidoArchivoA.append("Mandante"+",");
		}
		
		if(bValidaDuplicidad){
		contenidoArchivoA.append("Notificado como posible Duplicado"+",");
		}
		
		contenidoArchivoA.append("\n");
    
		//Despliegue de los documentos
		vDoctosCargados.clear();
    if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
      ic_estatus_docto = "31"; // Pendientes Negociables
    else if(strPreNegociable.equals("S"))   // Si se trata de documentos Pendientes
      ic_estatus_docto = "29"; // Pendientes Negociables
    else
      ic_estatus_docto = "1"; // Negociables
		vDoctosCargados = CargaDocumentos.getComDoctosCargadosConIcDocumento(hidNumAcuse, strAforo, ses_ic_epo, ic_estatus_docto, strAforoDL, sesIdiomaUsuario);
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{
			registros = 0;
			Vector vd = (Vector)e.nextElement();
			nombrePyme = vd.get(0).toString().replace(',',' ');
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			monedaNombre = vd.get(4).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10)==null?"":vd.get(10).toString().replace(',',' ');
			tipoFactoraje = vd.get(7).toString();
			NombreIF = "";
			Mandante 	= vd.get(27).toString().replace(',',' ');
			Factoraje = vd.get(28).toString();
			fechaVencimientoPyme = vd.get(20)==null?"":vd.get(20).toString();
			 duplicado = vd.get(29).toString(); //F038-2014
			
			if(bOperaFactorajeVencido ) { // Para factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF 	= vd.get(11).toString().replace(',',' ');
				}
			}
			// Fodea 023 2009  Mandato 
			if( bOperaFactConMandato &&  tipoFactoraje.equals("M") )  {
					NombreIF 	= vd.get(11).toString().replace(',',' ');
				}
				
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(17).toString().replace(',',' ');
					sPorcentajeBeneficiario = vd.get(18).toString();
					sMontoBeneficiario = vd.get(19).toString();
				}
			}
			//FODEA 050 - VALC - 10/2008			
			fechaEntrega			= vd.get(23).toString();
			tipoCompra = vd.get(24).toString();		
			clavePresupuestaria = vd.get(25).toString();
			periodo = vd.get(26).toString();
			
			tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": 
			(tipoFactoraje.equals("V"))?"Vencido": 
			(tipoFactoraje.equals("C"))?"Nota Credito": 
			(tipoFactoraje.equals("M"))?"Mandato": 
			(tipoFactoraje.equals("D"))?"Distribuido": "";
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if(estaHabilitadoNumeroSIAFF){
				numeroSIAFF = getNumeroSIAFF(vd.get(22).toString(),vd.get(21).toString());
			}
						
			contenidoArchivoA.append(nombrePyme+",");
			contenidoArchivoA.append(numeroDocumento+",");
			contenidoArchivoA.append(fechaDocumento+",");
			contenidoArchivoA.append(fechaVencimiento+",");
			if("S".equals(operaFVPyme)) {
				contenidoArchivoA.append(fechaVencimientoPyme+",");
			}//if("S".equals(operaFVPyme))
			contenidoArchivoA.append(monedaNombre+",");
			contenidoArchivoA.append(Factoraje+",");
			contenidoArchivoA.append(monto+",");
			contenidoArchivoA.append(Comunes.formatoDecimal(porcentajeAnticipo,0)+" %,");
			contenidoArchivoA.append(montoDescuento+",");
			contenidoArchivoA.append(referencia+",");
			if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) {
				contenidoArchivoA.append(NombreIF+",");
			}
			if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit) {
				contenidoArchivoA.append(sNombreBeneficiario+",");
				contenidoArchivoA.append((sPorcentajeBeneficiario.equals("")?sPorcentajeBeneficiario:Comunes.formatoDecimal(sPorcentajeBeneficiario,2)+" %")+",");
				contenidoArchivoA.append((sMontoBeneficiario.equals("")?sMontoBeneficiario:Comunes.formatoMN(sMontoBeneficiario))+",");
			}
			for (i=1;i<=numCamposAdicionales;i++){
				contenidoArchivoA.append((String)vd.get(11+i)+",");
			}
			if(estaHabilitadoNumeroSIAFF){
				contenidoArchivoA.append(numeroSIAFF+",");
			}
			//FODEA 050 - VALC - 10/2008
			if(bestaHabilitadaPublicacionEPOPEF.equalsIgnoreCase("S")){
				contenidoArchivoA.append(fechaEntrega+",");
				contenidoArchivoA.append(tipoCompra.replace(',',' ')+",");
				contenidoArchivoA.append(clavePresupuestaria+",");
				contenidoArchivoA.append(periodo+",");
			}			
			
		if (bOperaFactConMandato) {
		contenidoArchivoA.append(Mandante+",");
		}
		if(bValidaDuplicidad){
			contenidoArchivoA.append( duplicado+",");
		}
			registros++;
      contenidoArchivoA.append("\n");
		}//fin for
		
		numCamposAdicionales=vNombres.size();
		contenidoArchivoA.append("   "+"\n");
		if("S".equals(spubdocto_venc)){
		/* FIN NO NEGOCIABLES*/
		/* INICIO VENCIDO SIN OPERAR*/
		contenidoArchivoA.append("Vencido Sin Operar"+"\n");
		contenidoArchivoA.append("Nombre Proveedor,");
		contenidoArchivoA.append("Número de Documento,");
		contenidoArchivoA.append("Fecha Emisión,");
		contenidoArchivoA.append("Fecha Vencimiento,");
		if("S".equals(operaFVPyme)) {
			contenidoArchivoA.append("Fecha Vencimiento Proveedor,");
		}//if("S".equals(operaFVPyme))
		contenidoArchivoA.append("Moneda"+",");
		contenidoArchivoA.append("Tipo Factoraje,");
		contenidoArchivoA.append("Monto"+",");
		contenidoArchivoA.append("Porcentaje de Descuento,");
		contenidoArchivoA.append("Monto a Descontar"+",");
		contenidoArchivoA.append("Referencia,");
		if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit ) { //Factoraje Vencido, Mandato, Vencimiento Infonavit
			contenidoArchivoA.append("Nombre IF,");
		}
		if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit) { //Factoraje Distribuido,  Vencimiento Infonavit
			contenidoArchivoA.append("Nombre Beneficiario,");
			contenidoArchivoA.append("Porcentaje Beneficiario"+",");
			contenidoArchivoA.append("Monto Beneficiario,");
		}
		
		for(int a=0; a<vNombres.size(); a++) {
			contenidoArchivoA.append((String)vNombres.get(a)+",");
		}
		if(estaHabilitadoNumeroSIAFF) {
			contenidoArchivoA.append("Digito Identificador,");
		}	
		//FODEA 050 - VALC - 10/2008
		if(bestaHabilitadaPublicacionEPOPEF.equalsIgnoreCase("S")){
			contenidoArchivoA.append("Fecha Entrega,");
			contenidoArchivoA.append("Tipo Compra ,");
			contenidoArchivoA.append("Clave Presupuestaria ,");
			contenidoArchivoA.append("Periodo,");
		}
				
		if (bOperaFactConMandato) {
		contenidoArchivoA.append( "Mandante"+",");
		}
		
		if(bValidaDuplicidad){
		contenidoArchivoA.append("Notificado como posible Duplicado"+",");
		}
		
    contenidoArchivoA.append("\n");
    
		//Despliegue de los documentos
		vDoctosCargados.clear(); /*9 DOC VENCIDOS SIN OPERAR*/
		vDoctosCargados = CargaDocumentos.getComDoctosCargadosConIcDocumento(hidNumAcuse, strAforo, ses_ic_epo, "9", strAforoDL, sesIdiomaUsuario);
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{
			registros = 0;
			Vector vd = (Vector)e.nextElement();
			nombrePyme = vd.get(0).toString().replace(',',' ');
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			monedaNombre = vd.get(4).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10)==null?"":vd.get(10).toString().replace(',',' ');
			tipoFactoraje = vd.get(7).toString();
			NombreIF = "";
			Mandante 	= vd.get(27).toString().replace(',',' ');
			Factoraje = vd.get(28).toString();
			fechaVencimientoPyme = vd.get(20)==null?"":vd.get(20).toString();
			 duplicado = vd.get(29).toString(); //F038-2014
			
			if(bOperaFactorajeVencido ) { // Para factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF 	= vd.get(11).toString().replace(',',' ');
				}
			}
			// Fodea 023 2009  Mandato 
			if( bOperaFactConMandato &&  tipoFactoraje.equals("M") )  {
					NombreIF 	= vd.get(11).toString().replace(',',' ');
				}
				
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(17).toString().replace(',',' ');
					sPorcentajeBeneficiario = vd.get(18).toString();
					sMontoBeneficiario = vd.get(19).toString();
				}
			}
			
			//Fodea 042-2009 Vencimiento Infonavit
			if( bOperaFactorajeVencidoInfonavit &&  tipoFactoraje.equals("I") )  {
					NombreIF 	= vd.get(11).toString().replace(',',' ');
					montoDescuento = monto;
					porcentajeAnticipo = "100";
					sNombreBeneficiario = vd.get(17).toString().replace(',',' ');
					sPorcentajeBeneficiario = vd.get(18).toString();
					sMontoBeneficiario = vd.get(19).toString();
			}
			
			//FODEA 050 - VALC - 10/2008			
			fechaEntrega			= vd.get(23).toString();
			tipoCompra = vd.get(24).toString();			
			clavePresupuestaria = vd.get(25).toString();
			periodo = vd.get(26).toString();
			
			tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": 
			(tipoFactoraje.equals("V"))?"Vencido": 
			(tipoFactoraje.equals("M"))?"Mandato": 
			(tipoFactoraje.equals("C"))?"Nota Credito": 
			(tipoFactoraje.equals("D"))?"Distribuido" : "";
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			
			if(estaHabilitadoNumeroSIAFF){
				numeroSIAFF = getNumeroSIAFF(vd.get(22).toString(),vd.get(21).toString());
			}
		
			contenidoArchivoA.append(nombrePyme+",");
			contenidoArchivoA.append(numeroDocumento+",");
			contenidoArchivoA.append(fechaDocumento+",");
			contenidoArchivoA.append(fechaVencimiento+",");
			if("S".equals(operaFVPyme)) {
				contenidoArchivoA.append(fechaVencimientoPyme+",");
			}//if("S".equals(operaFVPyme))
			contenidoArchivoA.append(monedaNombre+",");
			contenidoArchivoA.append(Factoraje+",");
			contenidoArchivoA.append(monto+",");
			contenidoArchivoA.append(Comunes.formatoDecimal(porcentajeAnticipo,0)+" %,");
			contenidoArchivoA.append(montoDescuento+",");
			contenidoArchivoA.append(referencia+",");
			if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit ) { //Factoraje Vencido, Mandato, Vencimiento Infonavit
				contenidoArchivoA.append(NombreIF+",");
			}
			if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit ) { //Factoraje Distribuido, Vencimiento Infonavit
				contenidoArchivoA.append(sNombreBeneficiario+",");
				contenidoArchivoA.append((sPorcentajeBeneficiario.equals("")?sPorcentajeBeneficiario:Comunes.formatoDecimal(sPorcentajeBeneficiario,2)+" %")+",");
				contenidoArchivoA.append((sMontoBeneficiario.equals("")?sMontoBeneficiario:sMontoBeneficiario)+",");
			}
			for (i=1;i<=numCamposAdicionales;i++){
				contenidoArchivoA.append((String)vd.get(11+i)+",");
			}
			
			if(estaHabilitadoNumeroSIAFF){
				contenidoArchivoA.append(numeroSIAFF+",");
			}
			//FODEA 050 - VALC - 10/2008
			if(bestaHabilitadaPublicacionEPOPEF.equalsIgnoreCase("S")){
				contenidoArchivoA.append(fechaEntrega+",");
				contenidoArchivoA.append(tipoCompra.replace(',',' ')+",");
				contenidoArchivoA.append(clavePresupuestaria+",");
				contenidoArchivoA.append(periodo+",");
			}
			
			if (bOperaFactConMandato) {
				contenidoArchivoA.append(Mandante+",");
			}
			
			if(bValidaDuplicidad){
			contenidoArchivoA.append( duplicado+",");	
			}
			registros++;
      
      contenidoArchivoA.append("\n");
		}//fin for
		}//"S".equals(spubdocto_venc)
		

	} catch (Exception e) {
		e.printStackTrace();
	}
	
	if(!archivo.make(contenidoArchivoA.toString(), strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	} else {
		nombreArchivo = archivo.nombre;	
		
		generaAch.setRutaArchivo(strDirectorioTemp+nombreArchivo);
		generaAch.guardarArchivo() ;	 
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("accion",accion);
	}

}else 	if(existeArch>0)  {

	String nombreArchivo = generaAch.desArchAcuse( ) ;

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("accion",accion);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

}

} catch (Exception exception) {
		
		java.io.StringWriter outSW = new java.io.StringWriter();
		exception.printStackTrace(new java.io.PrintWriter(outSW));
		String stackTrace = outSW.toString();
			
		generaAch.setDesError(stackTrace);	
		
		generaAch.guardaBitacoraArch();	 
	
//	throw new NafinException("SIST0001");
	
} 

%>

<%=jsonObj%>
<%!
	public String getNumeroSIAFF(String ic_epo,String ic_documento){
		return getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(ic_documento);		
	}
	
	public String getNumeroDeCuatroDigitos(String numero){

		String 			claveEPO 	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();
		
      for(int i = 0;i<(4-claveEPO.length());i++){
			resultado.append("0");
      }
      resultado.append(claveEPO);

      return resultado.toString();
  }
  
  	public String getNumeroDeOnceDigitos(String numero){

		String 			claveDocto	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();
		
      for(int i = 0;i<(11-claveDocto.length());i++){
			resultado.append("0");
      }
      resultado.append(claveDocto);

      return resultado.toString();
  }
%>
