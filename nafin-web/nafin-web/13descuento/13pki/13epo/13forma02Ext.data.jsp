<%@ page contentType="application/json;charset=UTF-8"
	import="	
	java.io.*, 		
	javax.naming.*,
	java.util.*,
	java.math.*, 
	java.text.*, 
	java.sql.*,
	com.netro.zip.*, 
	com.jspsmart.upload.*, 
	netropology.utilerias.*,			
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,		
	net.sf.json.JSONArray,
	java.security.*, 
	com.netro.descuento.*,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload" />
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String operacion = (request.getParameter("operacion") != null)?request.getParameter("operacion"):"";	
	String infoRegresar =""; 
	int start= 0, limit =0;
	String archivo  = (request.getParameter("archivo") != null) ? request.getParameter("archivo") : "";
	String txttotdoc = (request.getParameter("txttotdoc") != null) ? request.getParameter("txttotdoc") : "0";
	String txtmtodoc = (request.getParameter("txtmtodoc") != null) ? request.getParameter("txtmtodoc") : "0";
	String txtmtomindoc = (request.getParameter("txtmtomindoc") != null) ? request.getParameter("txtmtomindoc") : "0";
	String txtmtomaxdoc = (request.getParameter("txtmtomaxdoc") != null) ? request.getParameter("txtmtomaxdoc") : "0";
	
	String txttotdocdo = (request.getParameter("txttotdocdo") != null) ? request.getParameter("txttotdocdo") : "0";
	String txtmtodocdo = (request.getParameter("txtmtodocdo") != null) ? request.getParameter("txtmtodocdo") : "0";
	String txtmtomindocdo = (request.getParameter("txtmtomindocdo") != null) ? request.getParameter("txtmtomindocdo") : "0";
	String txtmtomaxdocdo = (request.getParameter("txtmtomaxdocdo") != null) ? request.getParameter("txtmtomaxdocdo") : "0";
	
	String strPendiente = (request.getParameter("strPendiente") != null) ? request.getParameter("strPendiente") : "N";
	String strPreNegociable = (request.getParameter("strPreNegociable") != null) ? request.getParameter("strPreNegociable") : "N";
	String strFirmaManc = (request.getParameter("strFirmaManc") != null) ? request.getParameter("strFirmaManc") : "N";
	String strHash = (request.getParameter("strHash") != null) ? request.getParameter("strHash") : "N";
	String proceso = (request.getParameter("proceso") != null) ? request.getParameter("proceso") : "0";
	String NumLineas = (request.getParameter("NumLineas") != null) ? request.getParameter("NumLineas") : "0";
	String pantalla = (request.getParameter("pantalla") != null) ? request.getParameter("pantalla") : "";
	
	String ic_proc_docto_det = (request.getParameter("ic_proc_docto_det") != null) ? request.getParameter("ic_proc_docto_det") : "0";
		
	String acuse1 = (request.getParameter("acuse") != null) ? request.getParameter("acuse") : "";
	String acuseFormateado = (request.getParameter("acuseFormateado") != null) ? request.getParameter("acuseFormateado") : "";
	String usuario = (request.getAttribute("usuario")==null)?request.getParameter("usuario"):request.getAttribute("usuario").toString();
	String hidFechaCarga = (request.getAttribute("hidFechaCarga")==null)?request.getParameter("hidFechaCarga"):request.getAttribute("hidFechaCarga").toString();
	String hidHoraCarga = (request.getAttribute("hidHoraCarga")==null)?request.getParameter("hidHoraCarga"):request.getAttribute("hidHoraCarga").toString();
	String strClaveHash = (request.getParameter("strClaveHash") != null) ? request.getParameter("strClaveHash") : "";	
	String hayErrores = (request.getParameter("hayErrores") != null) ? request.getParameter("hayErrores") : "N";	

	
	
	JSONArray registrosTotales = new JSONArray();		
	String ses_ic_epo			= iNoCliente;

	CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
	CargaDoctoDes cargaMasiva   = new   CargaDoctoDes();
	
	// Verificar si esta habilitada la Publicacion EPO PEF
	String bestaHabilitadaPublicacionEPOPEF ="N";
	boolean 	estaHabilitadaPublicacionEPOPEF 	=  CargaDocumentos.estaHabilitadaLaPublicacionEPOPEF(ses_ic_epo);
	if(estaHabilitadaPublicacionEPOPEF) bestaHabilitadaPublicacionEPOPEF ="S";
	int 		offsetPubublicacionEPOPEF 			= (estaHabilitadaPublicacionEPOPEF)?4:0;
	
	// variables a usar	
	int  numCamposAdicionalesParametrizados =0, NumLinea = 0, NumProceso = 0 , RegLinea=0;
	numCamposAdicionalesParametrizados =  BeanParamDscto.getNumeroCamposAdicionales(ses_ic_epo);
	String campo_ad1 = "",  campo_ad2 = "",  campo_ad3 =  "", campo_ad4 =  "", campo_ad5 = "", 
					fechaEntrega = "", tipoCompra = "", clavePresupuestaria	= "", periodo		= "",
					NoBeneficiario="", PorcBeneficiario="", MontoBeneficiario="", IcBenef="", NoMandante = "",
					MensajeError= "", StatusRegistro = "", ic_pyme ="", numero_docto ="", fecha_docto ="",  fecha_venc="",  
					fecha_venc_pyme ="",  ic_moneda ="",	fn_monto ="", dscto_especial ="",  ct_referencia ="",
					InterFinanciero = "", csCaractEspecial = "";
	
	String  nombrePyme ="", numeroDocumento ="", fechaDocumento ="",  fechaVencimiento ="", fechaVencimientoPyme ="", 
			monedaNombre ="",  monto ="", porcentajeAnticipo ="",  montoDescuento ="", referencia="",  consecutivo ="", 
			clavePyme ="",  estatusDocumento ="",  icEstatusDocumento ="",  tipoFactoraje ="", claveMoneda ="",  
			nombremandante ="",  factoraje ="",   NombreIF ="", duplicado ="", 
			sNombreBeneficiario="",  sPorcentajeBeneficiario="",  sMontoBeneficiario="", campo1 ="", campo2 = "", campo3="",
			campo4="", campo5 ="", ic_monedad ="", numeroSIAFF ="",  consulta =""; 		
			String ic_estatus_docto ="";
			int  iNumMoneda=0,  numRegistrosMN = 0,  numRegistrosDL = 0;
			BigDecimal montoTotalMN = new BigDecimal("0.00");
			BigDecimal montoTotalDL = new BigDecimal("0.00");
			BigDecimal montoDescuentoMN = new BigDecimal("0.00");
			BigDecimal montoDescuentoDL = new BigDecimal("0.00");
			BigDecimal montoTotalMNNeg = new BigDecimal("0.00");
			BigDecimal montoTotalMNSinOpera = new BigDecimal("0.00");
			BigDecimal montoTotalDsctoMNNeg = new BigDecimal("0.00");
			BigDecimal montoTotalDsctoMNSinOpera = new BigDecimal("0.00");
			BigDecimal montoTotalDLNeg = new BigDecimal("0.00");
			BigDecimal montoTotalDsctoDLNeg = new BigDecimal("0.00");
			BigDecimal montoTotalMNNoNeg = new BigDecimal("0.00");
			BigDecimal montoTotalDsctoMNNoNeg = new BigDecimal("0.00");
			BigDecimal montoTotalDLNoNeg = new BigDecimal("0.00");
			BigDecimal montoTotalDsctoDLNoNeg = new BigDecimal("0.00");	
			BigDecimal montoTotalDLSinOper = new BigDecimal("0.00");
			BigDecimal montoTotalDsctoDLSinOper = new BigDecimal("0.00");
			
		
	String   bOperaFactorajeVencido ="", bOperaFactorajeDistribuido ="";
	String bValidaDuplicidad ="";
	String  bOperaFactConMandato="", bTipoFactoraje ="", bOperaFactorajeVencidoInfonavit ="";	
	boolean bVenSinOperar=false;
	
	bVenSinOperar = CargaDocumentos.publicarDocVencidos(ses_ic_epo);	
	boolean estaHabilitadaLaPublicacionEPOPEF = CargaDocumentos.estaHabilitadaLaPublicacionEPOPEF(ses_ic_epo);
	//bOperaFactorajeVencido = CargaDocumentos.autorizadaFactorajeVencido(ses_ic_epo);
	//bOperaFactorajeDistribuido = CargaDocumentos.autorizadaFactorajeDistribuido(ses_ic_epo);
		
	Hashtable alParamEPO1 = new Hashtable(); 
	alParamEPO1 = BeanParamDscto.getParametrosEPO(ses_ic_epo,1);	
	if (alParamEPO1!=null) {
		  bOperaFactConMandato = alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString();
		  bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
		  bOperaFactorajeDistribuido = alParamEPO1.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString();
   	  bOperaFactorajeVencidoInfonavit = alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString();
		bValidaDuplicidad = alParamEPO1.get("VALIDA_DUPLIC").toString();
		
	}
	bTipoFactoraje = (bOperaFactorajeVencido.equals("S") || bOperaFactorajeDistribuido.equals("S") || bOperaFactConMandato.equals("S"))?"S":"N";		
	boolean 	estaHabilitadoNumeroSIAFF 	= BeanParamDscto.estaHabilitadoNumeroSIAFF(ses_ic_epo);
	String bestaHabilitadoNumeroSIAFF ="N";
	if(estaHabilitadoNumeroSIAFF)		bestaHabilitadoNumeroSIAFF ="S";
	bVenSinOperar = CargaDocumentos.publicarDocVencidos(ses_ic_epo);	
	String cbVenSinOperar ="N";
	if(bVenSinOperar){			
		cbVenSinOperar = "S";	
	}
	
	//se obtienes los campos adicionales
	Vector nombresCampo = new Vector(5);
	int numeroCampos= 0;
	nombresCampo = CargaDocumentos.getCamposAdicionales(iNoCliente);
	numeroCampos=  nombresCampo.size();	
	String strCampos  ="", nomCampo1 ="", nomCampo2 ="", nomCampo3="", nomCampo4="", nomCampo5 ="";
	for (int i=0; i<nombresCampo.size(); i++) { 			
		Vector lovRegistro = (Vector) nombresCampo.get(i);
		strCampos += (String) lovRegistro.get(0);	
		if(i==0) 	nomCampo1 = (String) lovRegistro.get(1);
		if(i==1) 	nomCampo2 = (String) lovRegistro.get(1);
		if(i==2) 	nomCampo3 = (String) lovRegistro.get(1); 
		if(i==3) 	nomCampo4 = (String) lovRegistro.get(1);
		if(i==4) 	nomCampo5 = (String) lovRegistro.get(1);			
	}		
				
	JSONObject 	jsonObj	= new JSONObject();
	HashMap datos = new HashMap();	
	JSONArray registros = new JSONArray();
	
	
	if(informacion.equals("ValidaCargaArchivo")) {
		boolean commit = true;
		AccesoDB con = new AccesoDB();
		
		try {
	
			con.conexionDB();
			CargaDocumentos.estaHabilitadaLaPublicacionEPOPEF(ses_ic_epo);
				
			boolean ok_file=true;
			//se genera el numero de proceso	
			String ic_proc_docto = CargaDocumentos.getNumaxProcesoDocto();
			
			NumProceso = CargaDocumentos.noProcesoCM();
					
			String rutaArchivo = strDirectorioTemp +archivo; 		
			java.io.File ft = new java.io.File(rutaArchivo);
				
			if ( "zip".equals(Comunes.getExtensionNombreArchivo(rutaArchivo)) ) {
				FileInputStream fis = new FileInputStream(ft);
				try {
					rutaArchivo = ComunesZIP.descomprimirArchivoSimple(fis, strDirectorioTemp);			
					if ( rutaArchivo == null || rutaArchivo.equals("") || !"txt".equals(Comunes.getExtensionNombreArchivo(rutaArchivo)) ) {
						ok_file = false;	
					}			
				} catch (AppException appe) {
					throw appe;
				} catch(Throwable t) {
					throw new AppException("Error inesperado al procesar el archivo comprimido.", t);
							//Error processing compressed file
				}
			}
				
			try {
		
				con.conexionDB();
				
				ft = new java.io.File(rutaArchivo);
					
				if("S".equals(strHash)) {
					int i = 0;		
					byte[] aByte = new byte[4096];
					try {
						FileInputStream 	fis	= new FileInputStream(ft);
						MessageDigest 		md 	= MessageDigest.getInstance("MD5");
						md.reset();
						while((i = fis.read(aByte, 0, aByte.length)) != -1)
							md.update(aByte, 0, i);
						byte[] digest = md.digest();
						StringBuffer hexString = new StringBuffer();
						for (i=0;i<digest.length;i++) {
							String aux = Integer.toHexString(0xFF & digest[i]);
							if(aux.length()==1)
								aux = "0"+aux;
							hexString.append(aux);
						}
						strClaveHash = hexString.toString();
					} catch (Exception e) {
						e.printStackTrace();
						out.println("Error al generar clave HASH: "+e);
					}
				}//if("S".equals(strHash))
				
				
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(ft), "ISO-8859-1"));	
				VectorTokenizer vtd = null;
				Vector vecdet = null;
				String linea =null;
				VectorTokenizer vt=null; 
				Vector vecdat=null; 
			
				
							
				while((linea=br.readLine())!=null) {
					vtd		= new VectorTokenizer(linea,"|");
					vecdet	= vtd.getValuesVector();		
					
					NumLinea ++ ;					
					RegLinea ++;
					vt = new VectorTokenizer(linea, "|");
					vecdat = vt.getValuesVector();
						
					ic_pyme	= numero_docto = fecha_docto = fecha_venc = fecha_venc_pyme = "";
					ic_moneda = fn_monto = dscto_especial = ct_referencia = "";
					campo_ad1 = campo_ad2 = campo_ad3 = campo_ad4 = campo_ad5 = "";
					NoBeneficiario = IcBenef = PorcBeneficiario = MontoBeneficiario = "";
					NoMandante = InterFinanciero = "";  // FODEA 023 Mandato
					 
					try {
					
						vt=new VectorTokenizer(linea,"|");
						vecdat=vt.getValuesVector();	
						
						for(int i=0; i<vecdat.size();i++) {
							String campo = Comunes.quitaComitasSimples(vecdat.get(i).toString()).trim();
							campo = campo.replace('"',' ').trim();
							vecdat.set(i, campo);
						}
						
						int columna = 1;
						//CLAVE DE PROVEEDOR
						if (vecdat.size() >= columna) { 	ic_pyme = (String)vecdat.get(columna-1); 		columna++; 			}
						//NUMERO DE DOCUMENTO
						if (vecdat.size() >= columna) {		numero_docto = (String)vecdat.get(columna-1); 		columna++; 		}
						
						//FECHA DE EMISION DEL DOCUMENTO
						if (vecdat.size() >= columna) { 	fecha_docto = (String)vecdat.get(columna-1); 		columna++; 			}
						
						//FECHA DE VENCIMIENTO DEL DOCUMENTO
						if (vecdat.size() >= columna) { 	fecha_venc = (String)vecdat.get(columna-1); 	columna++; 			}	
									
						//FECHA DE VENCIMIENTO PYME (Sólo si está parametrizado)
						if ("S".equals(operaFVPyme) && vecdat.size() >= columna) { 	fecha_venc_pyme = (String)vecdat.get(columna-1); 		columna++; 		}
									
						//CLAVE DE LA MONEDA
						if (vecdat.size() >= columna) {			ic_moneda = (String)vecdat.get(columna-1); 			columna++; 			}
						
						//MONTO DEL DOCUMENTO
						if (vecdat.size() >= columna) { 	fn_monto = (String)vecdat.get(columna-1);  		columna++; 		}
						
						//TIPO DE DESCUENTO: N Normal, D Distribuido, V Vencido, M Mandato I Vencimiento Infonavit
						if (vecdat.size() >= columna) {  	dscto_especial = (String)vecdat.get(columna-1); 		columna++; 			}
								
						//REFERENCIA
						if (vecdat.size() >= columna) { 	ct_referencia = (String)vecdat.get(columna-1); 		columna++; 		}
									
						//--------------------------------- Campos Adicionales -------------------------------------------
						//Si existe el campo y se cumple cualquiera de las siguientes condiciones se lee el campo adicional
						//Es factoraje Vencido (V)
						//Es factoraje Distribuido (D)
						//Es factoraje vencimiento Infonavit (I) Fodea 042-2009
						//Está parametrizado el Campo Adicional y el tipo de factoraje es Normal (N), Notas de Crédito (C), Mandato (M) ó NO negociable (X) (Este ultimo no es un tipo de factoraje en si, pero se ocupo el mismo campo en el layout para definirlo)
						//CAMPO ADICIONAL 1
						if (vecdat.size() >= columna && 		( "V".equals(dscto_especial) || "D".equals(dscto_especial) || "I".equals(dscto_especial) || 		( numCamposAdicionalesParametrizados>=1 && ("N".equals(dscto_especial) || "C".equals(dscto_especial) || "M".equals(dscto_especial) || "X".equals(dscto_especial) ) ) ) ) {
								campo_ad1 = (String)vecdat.get(columna-1);
								columna++;
						}
						
						//CAMPO ADICIONAL 2
						if (vecdat.size() >= columna && 	( "V".equals(dscto_especial) || "D".equals(dscto_especial) || "I".equals(dscto_especial) || 	( numCamposAdicionalesParametrizados>=2 && ("N".equals(dscto_especial) || "C".equals(dscto_especial) || "M".equals(dscto_especial) || "X".equals(dscto_especial) ) ) ) ) {
							campo_ad2 = (String)vecdat.get(columna-1);
							columna++;
						}
						//CAMPO ADICIONAL 3
						if (vecdat.size() >= columna && 	( "V".equals(dscto_especial) || "D".equals(dscto_especial) || "I".equals(dscto_especial) ||		( numCamposAdicionalesParametrizados>=3 && ("N".equals(dscto_especial) || "C".equals(dscto_especial) || "M".equals(dscto_especial) || "X".equals(dscto_especial) ) ) ) ) {
							campo_ad3 = (String)vecdat.get(columna-1);
							columna++;
						}
						//CAMPO ADICIONAL 4
						if (vecdat.size() >= columna && ( "V".equals(dscto_especial) || "D".equals(dscto_especial) || "I".equals(dscto_especial) ||	( numCamposAdicionalesParametrizados>=4 && ("N".equals(dscto_especial) || "C".equals(dscto_especial) || "M".equals(dscto_especial) || "X".equals(dscto_especial) ) ) ) ) {
							campo_ad4 = (String)vecdat.get(columna-1);
							columna++;
						}
						//CAMPO ADICIONAL 5
						if (vecdat.size() >= columna && 	( "V".equals(dscto_especial) || "D".equals(dscto_especial) || "I".equals(dscto_especial) ||		( numCamposAdicionalesParametrizados>=5 && ("N".equals(dscto_especial) || "C".equals(dscto_especial) || "M".equals(dscto_especial) || "X".equals(dscto_especial) ) ) ) ) {
							campo_ad5 = (String)vecdat.get(columna-1);
							columna++;
						}
						//FECHA DE ENTREGA (Sólo si está parametrizado Publicacion como EPO PEF)
						if (vecdat.size() >= columna && 	estaHabilitadaPublicacionEPOPEF    ) {
							String valor = (String)vecdat.get(columna-1);
							fechaEntrega = (valor.length() > 10) ?valor.substring(0,11):valor;
							columna++;
						}				
						//TIPO DE COMPRA (Sólo si está parametrizado Publicacion como EPO PEF)
						if (vecdat.size() >= columna && estaHabilitadaPublicacionEPOPEF  ) {
							String valor = (String)vecdat.get(columna-1);
							tipoCompra = (valor.length() > 1) ?valor.substring(0,2):valor;
							columna++;
						}
						//CLAVE PRESUPUESTAL (Sólo si está parametrizado Publicacion como EPO PEF)
						if (vecdat.size() >= columna && estaHabilitadaPublicacionEPOPEF  ) {
							String valor = (String)vecdat.get(columna-1);
							clavePresupuestaria = (valor.length() > 5) ?valor.substring(0,6):valor;
							columna++;
						}
						//PERIODO (Sólo si está parametrizado Publicacion como EPO PEF)
						if (vecdat.size() >= columna &&  estaHabilitadaPublicacionEPOPEF ) {
							String valor = (String)vecdat.get(columna-1);
							periodo = (valor.length() > 3) ?valor.substring(0,4):valor;
							columna++;
						}
						//NUMERO ELECTRONICO DEL IF BENEFICIARIO (Sólo interesa si dscto_especial=V. Si es D, el campo también es requerido pero debe ir vacio)
						//tambien es requerido factoraje Vencimiento Infonavit  Fodea 042-2009
						if (vecdat.size() >= columna && ( "V".equals(dscto_especial) || "M".equals(dscto_especial) || "D".equals(dscto_especial)  || "I".equals(dscto_especial) ) ) {
							NoBeneficiario = (String)vecdat.get(columna-1);
							columna++;
						}
						//NUMERO ELECTRONICO DEL IF (Sólo interesa si dscto_especial=D y dscto_especial= I  Fodea 042-2009 )						
						if (vecdat.size() >= columna && ( "D".equals(dscto_especial) || "I".equals(dscto_especial) ) ) {
							IcBenef = (String)vecdat.get(columna-1);
							columna++;
						}
						//PORCENTAJE DEL BENEFICIARIO (Sólo interesa si dscto_especial=D  y dscto_especial= I Fodea 042-2009 )
						if (vecdat.size() >= columna && ( "D".equals(dscto_especial) || "I".equals(dscto_especial) )  )   {
							PorcBeneficiario = (String)vecdat.get(columna-1);
							columna++;
						}
						//MONTO BENEFICIARIO (Sólo interesa si dscto_especial=D   y dscto_especial= I Fodea 042-2009 )
						if (vecdat.size() >= columna && ( "D".equals(dscto_especial) || "I".equals(dscto_especial) )  ) {
							MontoBeneficiario = (String)vecdat.get(columna-1);
							columna++;
						}
						//******************FODEA 023 Mandato
						//Numero de Mandante solo si esl Tipo de Descuento es M
						if (vecdat.size() >= columna && ("M".equals(dscto_especial)  ) ) {
							NoMandante = (String)vecdat.get(columna-1);
							columna++;
						}		
							/*
							System.out.println("--------------------------------------------------------------");						
							System.out.println("RegLinea: "+RegLinea);						
							System.out.println("operaFVPyme: "+operaFVPyme);
							System.out.println("numProveedor: "+ic_pyme);
							System.out.println("numero_docto: "+numero_docto);
							System.out.println("fecha_docto: "+fecha_docto);
							System.out.println("fecha_venc: "+fecha_venc);
							System.out.println("fecha_venc_pyme: "+fecha_venc_pyme);
							System.out.println("ic_moneda: "+ic_moneda);
							System.out.println("fn_monto: "+fn_monto);
							System.out.println("dscto_especial: "+dscto_especial);
							System.out.println("ct_referencia: "+ct_referencia);										
							System.out.println("campo_ad1: "+campo_ad1);
							System.out.println("campo_ad2: "+campo_ad2);
							System.out.println("campo_ad3: "+campo_ad3);
							System.out.println("campo_ad4: "+campo_ad4);
							System.out.println("campo_ad5: "+campo_ad5);
							System.out.println("NoBeneficiario: "+NoBeneficiario);
							System.out.println("IcBenef: "+IcBenef);
							System.out.println("PorcBeneficiario: "+PorcBeneficiario);
							System.out.println("MontoBeneficiario: "+MontoBeneficiario);
							System.out.println("fechaEntrega: "+fechaEntrega);
							System.out.println("tipoCompra: "+tipoCompra);
							System.out.println("periodo: "+periodo);								
							System.out.println("strPreNegociable: "+strPreNegociable);
							System.out.println("strPendiente: "+strPendiente);
							System.out.println("NoMandante: "+NoMandante);
							
							System.out.println("txttotdoc: "+txttotdoc);
							System.out.println("txtmtodoc: "+txtmtodoc);
							System.out.println("txtmtomindoc: "+txtmtomindoc);
							System.out.println("txtmtomaxdoc: "+txtmtomaxdoc);
							
							System.out.println("txttotdocdo: "+txttotdocdo);
							System.out.println("txtmtodocdo: "+txtmtodocdo);
							System.out.println("txtmtomindocdo: "+txtmtomindocdo);
							System.out.println("txtmtomaxdocdo: "+txtmtomaxdocdo);						
							
							System.out.println("strPendiente: "+strPendiente);	
							System.out.println("strPreNegociable: "+strPreNegociable);		

							System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
							*/
						
						//En caso de que el registro no contenga el numero minimo de campos solicitados
						int minCamposPresentes = 7; //(Originalmente de "numero de proveedor" hasta "tipo de descuento")
						if (estaHabilitadaPublicacionEPOPEF) {
							minCamposPresentes = minCamposPresentes + 1 /* Referencia */ + numCamposAdicionalesParametrizados + 4 /* Campos de Fecha de Recepcion */;
						}
									
						csCaractEspecial = "N";			
						if(vecdat.size()<minCamposPresentes) {
							MensajeError = mensaje_param.getMensaje("13forma2.NoContieneNumMinCampos",sesIdiomaUsuario);
							StatusRegistro = "ER";
						}else {
							StatusRegistro = "OK";							
						}		
					
						if(Comunes.tieneCaracterControlNoPermitidoEnXML1(linea)){
							csCaractEspecial = "S";			
						}
					
						//Ejecucion del metodo que inserta en COMTMP_CARGA_DOCTO
						cargaMasiva.insertaDocumentoCM(
							ic_pyme, 			numero_docto,		fecha_docto, 		fecha_venc, 		ic_moneda, 		fn_monto,
							dscto_especial, 	ct_referencia,		campo_ad1, 			campo_ad2, 			campo_ad3, 		campo_ad4, 
							campo_ad5, 			NoBeneficiario, 	PorcBeneficiario,	MontoBeneficiario, 	NumProceso, 	StatusRegistro, 
							MensajeError, 		NumLinea, 			vecdat.size(), 		IcBenef, 			fecha_venc_pyme, NoMandante,
							fechaEntrega, tipoCompra, clavePresupuestaria, periodo, estaHabilitadaPublicacionEPOPEF,  csCaractEspecial, con);
						
						
						if(RegLinea >= 5000){
							con.terminaTransaccion(true);
							RegLinea = 0;							
							System.out.println("<<<<<<<<<<<Da Commit cada 5000 registros  <<<<<<<<<<<<<<<<<");
						}
						
										
					} catch (StringIndexOutOfBoundsException sioobe) { 
						commit = false;
						System.out.println("Exception Linea sin valores. "+sioobe);
						throw sioobe;
					} catch (NafinException ne)	{
						commit = false;
						throw ne;
					}	
				}//while
				con.terminaTransaccion(true);
									
					CargaDocumentos.cierraConexionDB();					
							
					new CicloThread(	NumProceso, 	Integer.parseInt(ses_ic_epo), 
					Double.parseDouble(txtmtomindoc), 		Double.parseDouble(txtmtomaxdoc), 
							Double.parseDouble(txtmtomindocdo), 	Double.parseDouble(txtmtomaxdocdo),
							Integer.parseInt(txttotdoc), 			Integer.parseInt(txttotdocdo), 		
							Double.parseDouble(txtmtodoc), 		Double.parseDouble(txtmtodocdo),  	
							sesIdiomaUsuario, 		strPreNegociable, 		strPendiente,  iNoUsuario,  strNombreUsuario ).start();
			} catch (NullPointerException npe) { 
				commit = false;
				System.out.println("Error Grave el Archivo no cumple con el Formato Requerido, por favor reviselo.");
				throw npe;
			} catch (NafinException e) { 
				commit = false;
				System.out.println("Error al insertar registros en tabal temporal en carga masiva de documentos descuento electronico." + e); 
				throw e;
			} catch (Exception e) { 
				commit = false;
				System.out.println("Exception General." + e); 
				throw e;
			}
			
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("proceso",String.valueOf(NumProceso));			
			jsonObj.put("RegLinea",String.valueOf(RegLinea));	
			jsonObj.put("NumLineas",String.valueOf(NumLinea));	
			jsonObj.put("NumLinea",String.valueOf(NumLinea));		
			jsonObj.put("strClaveHash",strClaveHash);				
			infoRegresar = jsonObj.toString();	
				
			
		} catch(NafinException ne) {
			ne.printStackTrace();
			throw ne;
		
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(false);
			con.cierraConexionDB();
		}
		}
		
		
	}else  if(informacion.equals("ProcesaCargaArchivo")) {
	
			
			List procesos =  CargaDocumentos.porcentajeProcesoCarga(Integer.parseInt(proceso)  , String.valueOf(NumLineas));
					
			String EstatusProceso =procesos.get(0).toString();
			String NumProcesados=  procesos.get(1).toString();
			String NumeroLineas=  procesos.get(2).toString();
			String Porcentaje=  procesos.get(3).toString();
			String ImageSize=  procesos.get(4).toString();	
			if(EstatusProceso.equals("")) EstatusProceso ="PR";
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("estatusProceso",EstatusProceso);
			jsonObj.put("numProcReg",NumProcesados);			
			jsonObj.put("numReg",NumeroLineas);		
			jsonObj.put("strClaveHash",strClaveHash);	
			jsonObj.put("proceso",proceso);	
			infoRegresar = jsonObj.toString();	
			
			
	}else  if(informacion.equals("ResumendeEjecucion") ||  informacion.equals("ControlTotales") ) {
	
		
		String EstatusProc = "",  ErroresControl = "", NumCorrectos = "", NumErrores = "",  MontoTotalNeg = "",
		MontoTotalNoNeg = "",  MontoTotalNegD = "", MontoTotalNoNegD = "", NumCorrectosDol = "", MontoTotalVen   ="",
		MontoTotalVenD  = "", monto_tot_mnErr ="", monto_tot_dolarErr ="", monto_tot_mnOK ="",monto_tot_dolarOK ="",
		errorCifras  = "",	monto_tot_mnNoNegOK = "", monto_tot_dolarNoNegOK = "", 	monto_tot_mnVenOK = "",	
		monto_tot_mnNegOK ="", monto_tot_dolarVenOK = "", monto_tot_dolarNegOK ="",		
		num_duplicado  ="", num_duplicado_dl="", monto_dup ="", monto_dup_dl ="", 	MontoTotalNeg_total =  "", MontoTotalNegD_total ="", 
		monto_tot_MN_OK =   "",  monto_tot_DL_OK =""; /*F038-2014*/
		String doctoDuplicados = (request.getParameter("doctoDuplicados") != null) ? request.getParameter("doctoDuplicados") : "N";
	
		System.out.println("doctoDuplicados  ------->"+doctoDuplicados); 
	
					
		String  bBotonProcesar = "N";
		String sinError		= "", 	error	="", lineasErrores ="", duplicados ="", bandera_duplicados ="N";
		int totdoc_mnOK=0, totdoc_dolarOK=0, totdoc_mnErr = 0, totdoc_dolarErr = 0;
		Vector vBitacora = CargaDocumentos.mostrarDocumentosBitacoraCM(Integer.parseInt(proceso), doctoDuplicados  );
		/*-- Lectura de los datos contenidos en el vector --*/
		EstatusProc 		= vBitacora.get(0).toString();
		NumCorrectos 		= vBitacora.get(1).toString();
		NumErrores 			= vBitacora.get(2).toString();
		ErroresControl 		= vBitacora.get(3).toString();
		MontoTotalNeg 		= vBitacora.get(4).toString();
		MontoTotalNoNeg 	= vBitacora.get(5).toString();
		MontoTotalNegD 		= vBitacora.get(6).toString();
		MontoTotalNoNegD 	= vBitacora.get(7).toString();
		monto_tot_mnErr 	= vBitacora.get(8).toString();
		monto_tot_dolarErr 	= vBitacora.get(9).toString();
		monto_tot_mnOK 		= vBitacora.get(10).toString();
		monto_tot_dolarOK 	= vBitacora.get(11).toString();
		NumCorrectosDol 	= vBitacora.get(12).toString();
		MontoTotalVen		= vBitacora.get(13).toString();
		MontoTotalVenD		= vBitacora.get(14).toString();
		
		num_duplicado  = vBitacora.get(15).toString(); /*F038-2014*/
		num_duplicado_dl = vBitacora.get(16).toString(); /*F038-2014*/
		monto_dup = vBitacora.get(17).toString();  /*F038-2014*/
		monto_dup_dl  = vBitacora.get(18).toString();   /*F038-2014*/
		
		MontoTotalNeg_total  = vBitacora.get(19).toString();   /*F038-2014*/
		MontoTotalNegD_total  = vBitacora.get(20).toString();   /*F038-2014*/
		
		monto_tot_MN_OK  = vBitacora.get(21).toString();   /*F038-2014*/ 
		monto_tot_DL_OK  = vBitacora.get(22).toString();   /*F038-2014*/
	
		errorCifras = ErroresControl.trim();
		
		if(doctoDuplicados.equals("S")){  // Para sumar los duplicados 
			totdoc_mnOK = Integer.parseInt(NumCorrectos)  + Integer.parseInt(num_duplicado) ;
			totdoc_dolarOK = Integer.parseInt(NumCorrectosDol) + Integer.parseInt(num_duplicado_dl) ;	
		}else  if(doctoDuplicados.equals("N")){ 
			totdoc_mnOK = Integer.parseInt(NumCorrectos);
			totdoc_dolarOK = Integer.parseInt(NumCorrectosDol) ;	
		}
		// doctos Negociables		
		monto_tot_mnNegOK = MontoTotalNeg ;
		monto_tot_dolarNegOK = MontoTotalNegD;
		
		// doctos No  Negociables
		monto_tot_mnNoNegOK = MontoTotalNoNeg;
		monto_tot_dolarNoNegOK = MontoTotalNoNegD;
		
		// doctos vencidos sin operar   Negociables
		monto_tot_mnVenOK = MontoTotalVen;
		monto_tot_dolarVenOK = MontoTotalVenD;
		
		int total = totdoc_mnOK+totdoc_dolarOK+ Integer.parseInt(num_duplicado) +Integer.parseInt(num_duplicado_dl);
		
		boolean bBorraCargaMas = true;
		if(bBotonProcesar.equals("N")  ) {
			bBorraCargaMas = CargaDocumentos.borraDoctosCargados(proceso, "M", ses_ic_epo);
		}

		if(!EstatusProc.equals("OK")) {
			Vector vDoctosDetalle = CargaDocumentos.ObtenerDetalleCargaM(Integer.parseInt(proceso), sesIdiomaUsuario);
			sinError		= vDoctosDetalle.get(0).toString();
			error			= vDoctosDetalle.get(1).toString();
			lineasErrores	= vDoctosDetalle.get(2).toString();		
			duplicados	= vDoctosDetalle.get(3).toString();	  /*F038-2014*/	
		}		
		
		if( total== 0 || error.length()>0  ||  errorCifras.length()>0  || lineasErrores.length()>0  ) {
				bBotonProcesar =  "N";
		} 
		if(total>0  || duplicados.length()>0) {		
				bBotonProcesar =  "S";
		} 		
		if(duplicados.length()>0) {	
			bandera_duplicados =  "S";
		}
		
		if(!lineasErrores.equals(""))		hayErrores ="S";		
		
		if(informacion.equals("ResumendeEjecucion")	) {	
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("estatusProceso", EstatusProc);
			jsonObj.put("sinError", sinError);
			jsonObj.put("error", error);
			jsonObj.put("errorCifras", errorCifras);
			jsonObj.put("monto_tot_mnOK", monto_tot_mnOK);		
			jsonObj.put("monto_tot_mnErr", monto_tot_mnErr);
			jsonObj.put("monto_tot_dolarOK", monto_tot_dolarOK);
			jsonObj.put("monto_tot_dolarErr", monto_tot_dolarErr);	
			jsonObj.put("proceso", proceso);	
			jsonObj.put("bBotonProcesar", bBotonProcesar);	
			jsonObj.put("lineasErrores", lineasErrores);
			jsonObj.put("strClaveHash", strClaveHash);
			jsonObj.put("hayErrores", hayErrores);
			jsonObj.put("duplicados", duplicados);	  /*F038-2014*/			
			jsonObj.put("num_duplicado", num_duplicado);	  /*F038-2014*/
			jsonObj.put("num_duplicado_dl", num_duplicado_dl);	  /*F038-2014*/
			jsonObj.put("monto_dup", monto_dup);	  /*F038-2014*/
			jsonObj.put("monto_dup_dl", monto_dup_dl);	  /*F038-2014*/
			jsonObj.put("bandera_duplicados", bandera_duplicados);	  /*F038-2014*/
			jsonObj.put("strPerfil", strPerfil);	  /*F038-2014*/
			jsonObj.put("bValidaDuplicidad", bValidaDuplicidad);	  /*F016-2015*/
			
					
			
			infoRegresar = jsonObj.toString();
		}		
		
	if(informacion.equals("ControlTotales")	) {				
		
			HashMap	registrosTot = new HashMap();	
			int reg = 3;
			if(bVenSinOperar)  reg +=1;	
		
			
			for(int t =0; t<=reg; t++) {		
				registrosTot = new HashMap();		
				if(t==0){ 		
					registrosTot.put("INFORMACION", "No. total de documentos cargados");		  
					registrosTot.put("MONEDA_NACIONAL", String.valueOf(totdoc_mnOK)  );	
					registrosTot.put("MONEDA_DL", String.valueOf(totdoc_dolarOK) );	
				}
				if(t==1){ 
					if(strPendiente.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pendientes Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pre Negociables");	
					}else{
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Negociables");	
					}					
					registrosTot.put("MONEDA_NACIONAL", "$"+Comunes.formatoDecimal(MontoTotalNeg_total,2)  );	
					registrosTot.put("MONEDA_DL", "$"+Comunes.formatoDecimal(MontoTotalNegD_total,2)  );					
				}			
				if(t==2){ 				
					if(strPendiente.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pendientes NO Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pre NO Negociables");	
					}else{
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados NO Negociables");	
					}						
					registrosTot.put("MONEDA_NACIONAL",  "$"+Comunes.formatoDecimal(MontoTotalNoNeg,2) );	
					registrosTot.put("MONEDA_DL","$"+Comunes.formatoDecimal(MontoTotalNoNegD,2)  );						
				}			
				if(t==3){ 							
					if(bVenSinOperar){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Vencidos Sin Operar");
						registrosTot.put("MONEDA_NACIONAL", "$"+Comunes.formatoDecimal(MontoTotalVen,2)  );	
						registrosTot.put("MONEDA_DL", "$"+Comunes.formatoDecimal(MontoTotalVenD,2) );	
					}else {			
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados");
						registrosTot.put("MONEDA_NACIONAL", "$"+Comunes.formatoDecimal(monto_tot_MN_OK,2) );	
						registrosTot.put("MONEDA_DL", "$"+Comunes.formatoDecimal(monto_tot_DL_OK,2) );			
					}		
				}		
				if(t==4){ 										
					registrosTot.put("INFORMACION", "Monto total de los documentos cargados"); 
					registrosTot.put("MONEDA_NACIONAL", "$"+Comunes.formatoDecimal(monto_tot_MN_OK,2) );	
					registrosTot.put("MONEDA_DL", "$"+Comunes.formatoDecimal(monto_tot_DL_OK,2) );				
				}			
				registrosTotales.add(registrosTot); 
			}//for

					
			consulta =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registros\": " + registrosTotales.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);	
			infoRegresar = jsonObj.toString();		
		}						
		
		
	}else  if(informacion.equals("Cancelar")) {
		
	
		boolean bBorraDoctos = CargaDocumentos.borraDoctosCargados(proceso, "M", iNoCliente);
		
		if(!ic_proc_docto_det.equals("0")) {
			CargaDocumentos.borraDoctosDetalleCargados(ic_proc_docto_det, "M"); 
		}
				
		jsonObj.put("success", new Boolean(true));		
		infoRegresar = jsonObj.toString();		
	
}else if (  ( informacion.equals("ConsultaPreAcuse") 
			|| informacion.equals("ConsultaPreAcuse2") 
			|| informacion.equals("ConsultaPreAcuse3") 	 	)  			
			&& pantalla.equals("PreAcuse") ) {
	
	int  numRegistros1 =0, numRegistros2 =0, numRegistros3 =0;
	String botonTrans ="N";
	
	String doctoDuplicados = (request.getParameter("doctoDuplicados") != null) ? request.getParameter("doctoDuplicados") : "N";
	if(doctoDuplicados.equals("S")){ doctoDuplicados = "'N','S'"; }
	if(doctoDuplicados.equals("N")){ doctoDuplicados = "'N'"; }
	
	if( informacion.equals("ConsultaPreAcuse") ) {
			
		//Despliegue de los documentos
	 if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "30"; // Pendientes Negociables
		else if(strPreNegociable.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "28"; // Pendientes Negociables
		else
			ic_estatus_docto = "2"; // Negociables	
		Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargados(proceso, strAforo, ses_ic_epo, ic_estatus_docto, "M", strAforoDL, sesIdiomaUsuario, doctoDuplicados);		   
				
		int hasmil =	vDoctosCargados.size();			
		if(hasmil>1000){ hasmil =1000; }		
		for (int i=0; i<hasmil; i++) { 			
			Vector vd            = (Vector) vDoctosCargados.get(i);
			nombrePyme           = vd.get(0).toString();
			numeroDocumento      = vd.get(1).toString().replace('"',' ').trim();
			fechaDocumento       = vd.get(2).toString().replace('"',' ').trim();
			fechaVencimiento     = vd.get(3)==null?"":vd.get(3).toString();
			fechaVencimientoPyme = vd.get(24)==null?"":vd.get(24).toString();
			monedaNombre         = vd.get(4).toString();
			monto                = vd.get(6).toString().replace('"',' ').trim();
			porcentajeAnticipo   = vd.get(8).toString().replace('"',' ').trim();
			montoDescuento       = vd.get(9).toString().replace('"',' ').trim();
			referencia           = vd.get(10).toString().replace('"',' ').trim();
			tipoFactoraje        = vd.get(7).toString().replace('"',' ').trim();
			nombremandante       = vd.get(29).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje            = vd.get(30).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			clavePresupuestaria  = vd.get(27).toString();
			estatusDocumento     = vd.get(14).toString();
			periodo = vd.get(28).toString();
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();
			duplicado = vd.get(31).toString(); //F038-2014
			NombreIF 				= "";
			if(bOperaFactorajeVencido.equals("S") ) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo = "100"; // si factoraje vencido: se aplica 100% de anticipo
					montoDescuento     = monto; // y el monto del desc es igual al del docto.
					NombreIF           = vd.get(11).toString().replace('"',' ').trim();
				}
			}
			//Mandato Fodea 023 2009
			if(bOperaFactConMandato.equals("S")  && tipoFactoraje.equals("M")) {					
				NombreIF 		= vd.get(11).toString();
			}
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S")   ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo      = "100";
					montoDescuento          = monto;
					sNombreBeneficiario     = vd.get(20).toString().replace('"',' ').trim();
					sPorcentajeBeneficiario = vd.get(21).toString().replace('"',' ').trim();
					sMontoBeneficiario      = vd.get(22).toString().replace('"',' ').trim();
				}
			}
			//Fodea 042-2009 Vencimiento Infonavit
			if(bOperaFactorajeVencidoInfonavit.equals("S")  && tipoFactoraje.equals("I")) {					
				NombreIF                = vd.get(11).toString();
				porcentajeAnticipo      = "100";
				montoDescuento          = monto;
				sNombreBeneficiario     = vd.get(20).toString().replace('"',' ').trim();
				sPorcentajeBeneficiario = vd.get(21).toString().replace('"',' ').trim();
				sMontoBeneficiario      = vd.get(22).toString().replace('"',' ').trim();
			}
			//FODEA 050 - VALC - 10/2008			
			fechaEntrega			= vd.get(25).toString();
			tipoCompra = "";
			if(vd.get(26).toString().equals("L")) 	tipoCompra = "Licitación";
			if(vd.get(26).toString().equals("I")) tipoCompra = "Invitación";
			if(vd.get(26).toString().equals("D")) tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(26).toString().equals("C")) tipoCompra = "CAAS, CAAS delegado";
				
			if(tipoFactoraje.equals("N")) 	    tipoFactoraje = "Normal";
			else if(tipoFactoraje.equals("M")) 	tipoFactoraje = "Mandato";	
			else if(tipoFactoraje.equals("V")) 	tipoFactoraje ="Vencido";
			else if(tipoFactoraje.equals("D")) 	tipoFactoraje = "Distribuido";
			else if(tipoFactoraje.equals("C")) {
				tipoFactoraje = "Nota Credito";
				porcentajeAnticipo = "100";
				montoDescuento = monto;
			}
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if (iNumMoneda==1) {
				numRegistrosMN++;
			} else {//IC_MONEDA=54
				numRegistrosDL++;				
			}
			
			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("MANDANTE",nombremandante);	
			datos.put("CONSECUTIVO",consecutivo);	
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);					
			datos.put("DUPLICADO",duplicado);
			
			registros.add(datos);	
			numRegistros1++;
		}//for
		
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		
	} else if( informacion.equals("ConsultaPreAcuse2") ) {
	
		if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "31"; // Pendientes Negociables
    else if(strPreNegociable.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "29"; // Pendientes Negociables
    else 
			ic_estatus_docto = "1"; // Negociables
		
		Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargados(proceso, strAforo, ses_ic_epo, ic_estatus_docto, "M", strAforoDL, sesIdiomaUsuario, doctoDuplicados);		
		
		int hasmil =	vDoctosCargados.size();			
		if(hasmil>1000){ hasmil =1000; }		
		for (int i=0; i<hasmil; i++) { 			
			Vector vd            = (Vector) vDoctosCargados.get(i);
			nombrePyme           = vd.get(0).toString();
			numeroDocumento      = vd.get(1).toString();
			fechaDocumento       = vd.get(2).toString();
			fechaVencimiento     = vd.get(3)==null?"":vd.get(3).toString();
			fechaVencimientoPyme = vd.get(24)==null?"":vd.get(24).toString();
			monedaNombre         = vd.get(4).toString();
			monto                = vd.get(6).toString();
			porcentajeAnticipo   = vd.get(8).toString();
			montoDescuento       = vd.get(9).toString();
			referencia           = vd.get(10).toString();
			tipoFactoraje        = vd.get(7).toString();
			nombremandante       = vd.get(29).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje            = vd.get(30).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			fechaEntrega         = vd.get(25).toString();
			clavePresupuestaria  = vd.get(27).toString();
			periodo = vd.get(28).toString();
			estatusDocumento = vd.get(14).toString();
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();
		   duplicado = vd.get(31).toString(); //F038-2014
			
			NombreIF = "";
			if(bOperaFactorajeVencido.equals("S") ) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo 	= "100"; // si factoraje vencido: se aplica 100% de anticipo
					montoDescuento 		= monto;		// y el monto del desc es igual al del docto.
					NombreIF 			= vd.get(11).toString();
				}
			}
			//Mandato Fodea 023 2009
			if(bOperaFactConMandato.equals("S") && tipoFactoraje.equals("M")) {					
				NombreIF 		= vd.get(11).toString();
			}				
			//FODEA 050 - VALC - 10/2008
		
			if(vd.get(26).toString().equals("L")) 	tipoCompra = "Licitación";
			if(vd.get(26).toString().equals("I"))  tipoCompra = "Invitación";
			if(vd.get(26).toString().equals("D")) 	tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(26).toString().equals("C")) tipoCompra = "CAAS, CAAS delegado";
			
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S") ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo 		= "100";
					montoDescuento 			= monto;
					sNombreBeneficiario 	= vd.get(20).toString();
					sPorcentajeBeneficiario = vd.get(21).toString();
					sMontoBeneficiario 		= vd.get(22).toString();
				}
			}
			
		  tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": (tipoFactoraje.equals("V"))?"Vencido" : 	
			(tipoFactoraje.equals("D"))?"Distribuido": (tipoFactoraje.equals("C"))?"Nota Credito": 
			(tipoFactoraje.equals("M"))?"Mandato": "";
		
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if (iNumMoneda==1) {
				numRegistrosMN++;				
			} else {//IC_MONEDA=54
				numRegistrosDL++;				
			}
			
			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("MANDANTE",nombremandante);	
			datos.put("CONSECUTIVO",consecutivo);	
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);
			datos.put("DUPLICADO",duplicado); //F038-2014
			registros.add(datos);	
			numRegistros2++;
		}//for
			
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
	
	} else if( informacion.equals("ConsultaPreAcuse3")   ) {

		Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargados(proceso, strAforo, ses_ic_epo, "9", "M", strAforoDL, sesIdiomaUsuario, doctoDuplicados);
		int hasmil =	vDoctosCargados.size();			
		if(hasmil>1000){ hasmil =1000; }		
		for (int i=0; i<hasmil; i++) { 			
			Vector vd            = (Vector) vDoctosCargados.get(i);
			nombrePyme           = vd.get(0).toString();
			numeroDocumento      = vd.get(1).toString();
			fechaDocumento       = vd.get(2).toString();
			fechaVencimiento     = vd.get(3)==null?"":vd.get(3).toString();
			fechaVencimientoPyme = vd.get(24)==null?"":vd.get(24).toString();
			monedaNombre         = vd.get(4).toString();
			monto                = vd.get(6).toString();
			porcentajeAnticipo   = vd.get(8).toString();
			montoDescuento       = vd.get(9).toString();
			referencia           = vd.get(10).toString();
			tipoFactoraje        = vd.get(7).toString();
			nombremandante       = vd.get(29).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje            = vd.get(30).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			fechaEntrega         = vd.get(25).toString();
			clavePresupuestaria  = vd.get(27).toString();
			periodo = vd.get(28).toString();
			estatusDocumento = vd.get(14).toString();
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();
			duplicado = vd.get(31).toString(); //F038-2014

			NombreIF = "";
			if(bOperaFactorajeVencido.equals("S") ) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo 	= "100"; // si factoraje vencido: se aplica 100% de anticipo
					montoDescuento 		= monto;		// y el monto del desc es igual al del docto.
					NombreIF 			= vd.get(11).toString();
				}
			}
			//Mandato Fodea 023 2009
			if(bOperaFactConMandato.equals("S") && tipoFactoraje.equals("M")) {					
				NombreIF 		= vd.get(11).toString();
			}
			//FODEA 050 - VALC - 10/2008
			tipoCompra = "";
			if(vd.get(26).toString().equals("L")) tipoCompra = "Licitación";
			if(vd.get(26).toString().equals("I")) tipoCompra = "Invitación";
			if(vd.get(26).toString().equals("D"))	tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(26).toString().equals("C")) tipoCompra = "CAAS, CAAS delegado";
			
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S") ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo 		= "100";
					montoDescuento 			= monto;
					sNombreBeneficiario 	= vd.get(20).toString();
					sPorcentajeBeneficiario = vd.get(21).toString();
					sMontoBeneficiario 		= vd.get(22).toString();
				}
			}
			// Fodea 042-2009 Vencimiento Infonavit
			if(bOperaFactorajeVencidoInfonavit.equals("S") && tipoFactoraje.equals("I")) {					
				NombreIF 		= vd.get(11).toString();
				porcentajeAnticipo 		= "100";
				montoDescuento 			= monto;
				sNombreBeneficiario 	= vd.get(20).toString();
				sPorcentajeBeneficiario = vd.get(21).toString();
				sMontoBeneficiario 		= vd.get(22).toString();				
			}						
			tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": (tipoFactoraje.equals("V"))?"Vencido" : 	
								 (tipoFactoraje.equals("D"))?"Distribuido" : (tipoFactoraje.equals("C"))?"Nota Credito":
								 (tipoFactoraje.equals("M"))?"Mandato": "";			
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if (iNumMoneda==1) {
				numRegistrosMN++;
		
			} else {//IC_MONEDA=54
				numRegistrosDL++;
			}
			
			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("MANDANTE",nombremandante);	
			datos.put("CONSECUTIVO",consecutivo);	
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);							
			datos.put("DUPLICADO",duplicado);//F038-2014
			registros.add(datos);
			numRegistros3++;
		}//for
		
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	}
	if(numRegistros1 >0 ||	numRegistros2>0 ||	numRegistros3>0 ){
		botonTrans ="S";
	}
			
			jsonObj = JSONObject.fromObject(consulta);
			jsonObj.put("hidMontoTotalMN", String.valueOf(montoTotalMN));
			jsonObj.put("hidNumDoctosActualesMN", Double.toString(numRegistrosMN));
			jsonObj.put("hidMontoTotalDL", String.valueOf(montoTotalDL));
			jsonObj.put("hidNumDoctosActualesDL",  Double.toString(numRegistrosDL));			
			jsonObj.put("hayCamposAdicionales", String.valueOf(numeroCampos));
			jsonObj.put("nomCampo1",String.valueOf(nomCampo1));
			jsonObj.put("nomCampo2",String.valueOf(nomCampo2));
			jsonObj.put("nomCampo3",String.valueOf(nomCampo3));
			jsonObj.put("nomCampo4",String.valueOf(nomCampo4));
			jsonObj.put("nomCampo5",String.valueOf(nomCampo5));			
			jsonObj.put("operaFVPyme",  operaFVPyme);
			jsonObj.put("bOperaFactorajeVencido",  bOperaFactorajeVencido);
			jsonObj.put("bOperaFactConMandato",  bOperaFactConMandato);
			jsonObj.put("bOperaFactorajeVencidoInfonavit",  bOperaFactorajeVencidoInfonavit);
			jsonObj.put("bOperaFactorajeDistribuido",  bOperaFactorajeDistribuido);
			jsonObj.put("bestaHabilitadaPublicacionEPOPEF",  bestaHabilitadaPublicacionEPOPEF);
			jsonObj.put("bOperaFactConMandato",  bOperaFactConMandato);
			jsonObj.put("botonTrans",  botonTrans);
			jsonObj.put("bValidaDuplicidad", bValidaDuplicidad);	  /*F016-2015*/
				
		infoRegresar  = jsonObj.toString();		

// para el acuse 

}else if (  ( informacion.equals("ConsultaAcuse") 
			|| informacion.equals("ConsultaAcuse2") 
			|| informacion.equals("ConsultaAcuse3") 	 	)  			
			&& pantalla.equals("Acuse") ) {

		
	if( informacion.equals("ConsultaAcuse") ) {

		if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
     ic_estatus_docto = "30"; // Pendientes Negociables
    else if(strPreNegociable.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "28"; // Pendientes Negociables
    else
		ic_estatus_docto = "2"; // Negociables
			
		Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargadosConIcDocumento(proceso, strAforo, ses_ic_epo, ic_estatus_docto, "M", strAforoDL, sesIdiomaUsuario,acuse1);
		
		int hasmil =	vDoctosCargados.size();			
		if(hasmil>1000){ hasmil =1000; }		
		for (int i=0; i<hasmil; i++) { 			
			Vector vd = (Vector) vDoctosCargados.get(i);
			nombrePyme = vd.get(0).toString();
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			fechaVencimientoPyme = vd.get(24)==null?"":vd.get(24).toString();							
			monedaNombre = vd.get(4).toString();
			estatusDocumento 		= vd.get(14).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10).toString();
			tipoFactoraje = vd.get(7).toString();
			NombreIF 				= "";
			nombremandante   = vd.get(31).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje  = vd.get(32).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			clavePresupuestaria = vd.get(29).toString();
			periodo = vd.get(30).toString();
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();
			if(bOperaFactorajeVencido.equals("S") ) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF = vd.get(11).toString();
				}
			}
			// Fodea 023 Mandato
			if(bOperaFactConMandato.equals("S") && tipoFactoraje.equals("M"))  {								
				NombreIF = vd.get(11).toString();
			}			
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S") ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D"))  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(20).toString();
					sPorcentajeBeneficiario = vd.get(21).toString();
					sMontoBeneficiario = vd.get(22).toString();
				}
			}			
			//Fodea 042-2009 Vencimiento Infonavit
			if(bOperaFactorajeVencidoInfonavit.equals("S") && tipoFactoraje.equals("I"))  {								
				NombreIF = vd.get(11).toString();
				porcentajeAnticipo = "100";
				montoDescuento = monto;
				sNombreBeneficiario = vd.get(20).toString();
				sPorcentajeBeneficiario = vd.get(21).toString();
				sMontoBeneficiario = vd.get(22).toString();
			}
			//FODEA 050 - VALC - 10/2008			
			fechaEntrega			= vd.get(27).toString();
			tipoCompra = "";
			if(vd.get(28).toString().equals("L"))		tipoCompra = "Licitación";
			if(vd.get(28).toString().equals("I")) 	tipoCompra = "Invitación";
			if(vd.get(28).toString().equals("D"))		tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(28).toString().equals("C"))
			tipoCompra = "CAAS, CAAS delegado"; 
			if(tipoFactoraje.equals("N"))				tipoFactoraje = "Normal";
			else if(tipoFactoraje.equals("M"))	tipoFactoraje = "Mandato";	
			else if(tipoFactoraje.equals("V"))	tipoFactoraje = "Vencido";
			else if(tipoFactoraje.equals("D"))	tipoFactoraje = "Distribuido";
			else if(tipoFactoraje.equals("C")) {
				tipoFactoraje = "Nota de Crédito";
				porcentajeAnticipo = "100";
				montoDescuento = monto;
			}
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if (iNumMoneda==1) {
				numRegistrosMN++;
			} else { //IC_MONEDA=54
				numRegistrosDL++;
			}
			if(estaHabilitadoNumeroSIAFF){			
				numeroSIAFF = getNumeroSIAFF(vd.get(26).toString(),vd.get(25).toString());
			}
			
			duplicado = vd.get(33).toString(); //F038-2014

			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("DIGITO_IDENTIFICADOR",numeroSIAFF);
			datos.put("MANDANTE",nombremandante);				
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);	
			datos.put("DUPLICADO",duplicado);//F038-2014
			registros.add(datos);				
			
		}//for
		
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
	}else if( informacion.equals("ConsultaAcuse2") ) {
	
		if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "31"; // Pendientes Negociables
    else if(strPreNegociable.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "29"; // Pendientes Negociables
    else
			ic_estatus_docto = "1"; // Negociables
		//Despliegue de los documentos
	
		Vector	vDoctosCargados = CargaDocumentos.mostrarDocumentosCargadosConIcDocumento(proceso, strAforo, ses_ic_epo, ic_estatus_docto, "M", strAforoDL, sesIdiomaUsuario,acuse1);
		
		int hasmil =	vDoctosCargados.size();		
		if(hasmil>1000){ hasmil =1000; } 	
		for (int i=0; i<hasmil; i++) { 			
			Vector vd = (Vector) vDoctosCargados.get(i);			
			nombrePyme = vd.get(0).toString();
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			fechaVencimientoPyme = vd.get(24)==null?"":vd.get(24).toString();
			monedaNombre = vd.get(4).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10).toString();
			tipoFactoraje = vd.get(7).toString();
			estatusDocumento 		= vd.get(14).toString();
			NombreIF = "";
			nombremandante  = vd.get(31).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje  = vd.get(32).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();
			if(bOperaFactorajeVencido.equals("S")) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V") ) {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF = vd.get(11).toString();
				}
			}
			// Fodea 023 Mandato
			if(bOperaFactConMandato.equals("S") && tipoFactoraje.equals("M"))  {								
				NombreIF = vd.get(11).toString();
			}
			//FODEA 050 - VALC - 10/2008
			fechaEntrega			= vd.get(27).toString();
			if(vd.get(28).toString().equals("L"))	tipoCompra = "Licitación";
			if(vd.get(28).toString().equals("I")) tipoCompra = "Invitación";
			if(vd.get(28).toString().equals("D"))	tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(28).toString().equals("C")) 	tipoCompra = "CAAS, CAAS delegado";
			clavePresupuestaria = vd.get(29).toString();
			periodo = vd.get(30).toString();
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S")) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D"))  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(20).toString();
					sPorcentajeBeneficiario = vd.get(21).toString();
					sMontoBeneficiario = vd.get(22).toString();
				}
			}
			
			tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": (tipoFactoraje.equals("V"))?"Vencido" : 	
			(tipoFactoraje.equals("D"))?"Distribuido": (tipoFactoraje.equals("C"))?"Nota Credito": 
			(tipoFactoraje.equals("M"))?"Mandato": "";
			
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if (iNumMoneda==1) {
				numRegistrosMN++;
			} else { //IC_MONEDA=54
				numRegistrosDL++;
			}
			if(estaHabilitadoNumeroSIAFF){
				numeroSIAFF = getNumeroSIAFF(vd.get(26).toString(),vd.get(25).toString());
			}
			
			duplicado = vd.get(33).toString(); //F038-2014

			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("DIGITO_IDENTIFICADOR",numeroSIAFF);
			datos.put("MANDANTE",nombremandante);				
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);	
			datos.put("DUPLICADO",duplicado); //F038-2014
			registros.add(datos);				
							
		}//for
		
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		
	}else if( informacion.equals("ConsultaAcuse3") ) {
	
		Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargadosConIcDocumento(proceso, strAforo, ses_ic_epo, "9", "M", strAforoDL, sesIdiomaUsuario, acuse1);
			
		int hasmil =	vDoctosCargados.size();			
		if(hasmil>1000){ hasmil =1000; } 		
		for (int i=0; i<hasmil; i++) { 			
			Vector vd = (Vector) vDoctosCargados.get(i);
			nombrePyme = vd.get(0).toString();
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			fechaVencimientoPyme = vd.get(24)==null?"":vd.get(24).toString();
			monedaNombre = vd.get(4).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10).toString();
			tipoFactoraje = vd.get(7).toString();
			estatusDocumento 		= vd.get(14).toString();
			NombreIF = "";
			nombremandante   = vd.get(31).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje  = vd.get(32).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();
			if(bOperaFactorajeVencido.equals("S") ) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V"))  {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF = vd.get(11).toString();
				}
			}
			// Fodea 023 Mandato
			if(bOperaFactConMandato.equals("S") && tipoFactoraje.equals("M"))  {								
				NombreIF = vd.get(11).toString();
			}
			//FODEA 050 - VALC - 10/2008
			fechaEntrega			= vd.get(27).toString();
			tipoCompra = "";
			if(vd.get(28).toString().equals("L")) 	tipoCompra = "Licitación";
			if(vd.get(28).toString().equals("I")) 	tipoCompra = "Invitación";
			if(vd.get(28).toString().equals("D"))		tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(28).toString().equals("C"))		tipoCompra = "CAAS, CAAS delegado";
			clavePresupuestaria = vd.get(29).toString();
			periodo = vd.get(30).toString();
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S") )  { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D"))  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(20).toString();
					sPorcentajeBeneficiario = vd.get(21).toString();
					sMontoBeneficiario = vd.get(22).toString();
				}
			}
			// Fodea -042-2009 Vencimiento Infonavit
			if(bOperaFactorajeVencidoInfonavit.equals("S") && tipoFactoraje.equals("I"))  {								
				NombreIF = vd.get(11).toString();
				porcentajeAnticipo = "100";
				montoDescuento = monto;
				sNombreBeneficiario = vd.get(20).toString();
				sPorcentajeBeneficiario = vd.get(21).toString();
				sMontoBeneficiario = vd.get(22).toString();								
			}
			tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": 
			(tipoFactoraje.equals("M"))?"Mandato":	(tipoFactoraje.equals("V"))?"Vencido":	(tipoFactoraje.equals("C"))?"Nota Credito":
			(tipoFactoraje.equals("D"))?"Distribuido":"";
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			
			if(estaHabilitadoNumeroSIAFF){
				numeroSIAFF = getNumeroSIAFF(vd.get(26).toString(),vd.get(25).toString());
			}			
			if (iNumMoneda==1) {
				numRegistrosMN++;
			} else { //IC_MONEDA=54
				numRegistrosDL++;
			}
			duplicado = vd.get(33).toString(); //F038-2014
			
			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("DIGITO_IDENTIFICADOR",numeroSIAFF);
			datos.put("MANDANTE",nombremandante);				
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);	
			datos.put("DUPLICADO",duplicado);//F038-2014

			registros.add(datos);					
		}//for	
	
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	}
	
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("hidMontoTotalMN", String.valueOf(montoTotalMN));
	jsonObj.put("hidNumDoctosActualesMN", Double.toString(numRegistrosMN));
	jsonObj.put("hidMontoTotalDL", String.valueOf(montoTotalDL));
	jsonObj.put("hidNumDoctosActualesDL",  Double.toString(numRegistrosDL));			
	jsonObj.put("hayCamposAdicionales", String.valueOf(numeroCampos));
	jsonObj.put("nomCampo1",String.valueOf(nomCampo1));
	jsonObj.put("nomCampo2",String.valueOf(nomCampo2));
	jsonObj.put("nomCampo3",String.valueOf(nomCampo3));
	jsonObj.put("nomCampo4",String.valueOf(nomCampo4));
	jsonObj.put("nomCampo5",String.valueOf(nomCampo5));			
	jsonObj.put("operaFVPyme",  operaFVPyme);
	jsonObj.put("bOperaFactorajeVencido",  bOperaFactorajeVencido);
	jsonObj.put("bOperaFactConMandato",  bOperaFactConMandato);
	jsonObj.put("bOperaFactorajeVencidoInfonavit",  bOperaFactorajeVencidoInfonavit);
	jsonObj.put("bOperaFactorajeDistribuido",  bOperaFactorajeDistribuido);
	jsonObj.put("bestaHabilitadaPublicacionEPOPEF",  bestaHabilitadaPublicacionEPOPEF);
	jsonObj.put("bOperaFactConMandato",  bOperaFactConMandato);	
	jsonObj.put("bestaHabilitadoNumeroSIAFF",  bestaHabilitadoNumeroSIAFF);	
	jsonObj.put("bValidaDuplicidad", bValidaDuplicidad);	  /*F016-2015*/
	
	infoRegresar  = jsonObj.toString();			

}else  if(  informacion.equals("TotalesPreAcuse")  && pantalla.equals("PreAcuse")  ) {

	String doctoDuplicados = (request.getParameter("doctoDuplicados") != null) ? request.getParameter("doctoDuplicados") : "N";
	if(doctoDuplicados.equals("S")){ doctoDuplicados = "'N','S'"; }
	if(doctoDuplicados.equals("N")){ doctoDuplicados = "'N'"; }
	
	//Despliegue de los documentos
	Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargados(proceso, strAforo, ses_ic_epo, "", "M", strAforoDL, sesIdiomaUsuario, doctoDuplicados);
	for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();) {
		Vector vd 				= (Vector)e.nextElement();
		monto 					= vd.get(6).toString();
		porcentajeAnticipo 		= vd.get(8).toString();
		montoDescuento 			= vd.get(9).toString();
		iNumMoneda = Integer.parseInt(vd.get(5).toString());
		icEstatusDocumento 		= vd.get(23).toString();
		if (iNumMoneda==1) {
			numRegistrosMN++;
			if (monto!=null) {
				montoTotalMN = montoTotalMN.add(new BigDecimal(monto));
				montoDescuentoMN = montoDescuentoMN.add(new BigDecimal(montoDescuento));
				if(icEstatusDocumento.equals("2") || icEstatusDocumento.equals("30")|| icEstatusDocumento.equals("28")){  // Estatus 2 Negociable, Estatus 30 Pendiente Negociable, Estatus 28 Pre Negociable
					montoTotalMNNeg   = montoTotalMNNeg.add(new BigDecimal(monto));
					montoTotalDsctoMNNeg = montoTotalDsctoMNNeg.add(new BigDecimal(montoDescuento));
				} else if(icEstatusDocumento.equals("9")){
					montoTotalMNSinOpera   = montoTotalMNSinOpera.add(new BigDecimal(monto));
					montoTotalDsctoMNSinOpera = montoTotalDsctoMNSinOpera.add(new BigDecimal(montoDescuento));
				} else {
					montoTotalMNNoNeg = montoTotalMNNoNeg.add(new BigDecimal(monto));
					montoTotalDsctoMNNoNeg = montoTotalDsctoMNNoNeg.add(new BigDecimal(montoDescuento));
				}
			}
		} else if (iNumMoneda == 54) {//IC_MONEDA=54
			numRegistrosDL++;
			if (monto!=null) {
				montoTotalDL = montoTotalDL.add(new BigDecimal(monto));
				montoDescuentoDL = montoDescuentoDL.add(new BigDecimal(montoDescuento));
				if (icEstatusDocumento.equals("2") || icEstatusDocumento.equals("30")|| icEstatusDocumento.equals("28")){ //Estatus 2 Negociables, Estatus 30 Pendientes Negociables, Estatus 28 Pre Negociable
					montoTotalDLNeg   = montoTotalDLNeg.add(new BigDecimal(monto));
					montoTotalDsctoDLNeg = montoTotalDsctoDLNeg.add(new BigDecimal(montoDescuento));
				} else if(icEstatusDocumento.equals("9")) {
					montoTotalDLSinOper = montoTotalDLSinOper.add(new BigDecimal(monto));
					montoTotalDsctoDLSinOper = montoTotalDsctoDLSinOper.add(new BigDecimal(montoDescuento));
				} else {
					montoTotalDLNoNeg = montoTotalDLNoNeg.add(new BigDecimal(monto));
					montoTotalDsctoDLNoNeg = montoTotalDsctoDLNoNeg.add(new BigDecimal(montoDescuento));
				}
			}
		}
	}//for 
		
		int reg =6;		
		if("S".equals(cbVenSinOperar)){
			reg +=2;
		}
					

		HashMap	registrosTot1 = new HashMap();
		JSONArray registrosTotales1 = new JSONArray();	
		for(int t =0; t<reg; t++) {	
			
			registrosTot1 = new HashMap();	
			if("S".equals(cbVenSinOperar)){			
				if(t==0){ 
					registrosTot1.put("MONEDA", "1");
					registrosTot1.put("MONTO", String.valueOf(montoTotalMN));
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosMN));
					if(strPendiente.equals("S")){
							registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes Negociables");		
						}else if(strPreNegociable.equals("S")){
							registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre Negociables ");	
						}else{
							registrosTot1.put("INFORMACION", "Total Moneda Nacional Negociables");	
						}					
						registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalMNNeg,2)   );	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNeg,2) );				
				}		
				if(t==1){ 
					registrosTot1.put("MONEDA", "1");
					registrosTot1.put("MONTO", String.valueOf(montoTotalMN));
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosMN));
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes No Negociables ");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre No Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Moneda Nacional No Negociables ");	
					}			
					registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalMNNoNeg,2));	
					registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNoNeg,2) );			
				}		
				if(t==2){
					registrosTot1.put("MONEDA", "1");
					registrosTot1.put("MONTO", String.valueOf(montoTotalMN));
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosMN));
					if("S".equals(cbVenSinOperar)){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Vencido Sin Operar ");	
						registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalMNSinOpera,2));	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNSinOpera,2) );	
					}
				}		
				
				//DOLARES
				if(t==3){ 
					registrosTot1.put("MONEDA", "54");
					registrosTot1.put("MONTO", String.valueOf(montoTotalDL));
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosDL));
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pendientes Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pre Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Dólares Negociables");	
					}					
					registrosTot1.put("TOTAL_MONTO",  "$"+Comunes.formatoDecimal(montoTotalDLNeg,2)   );	
					registrosTot1.put("TOTAL_MONTO_DESC", "$"+Comunes.formatoDecimal(montoTotalDsctoDLNeg,2));				
				}		
				if(t==4){ 				
					registrosTot1.put("MONEDA", "54");
					registrosTot1.put("MONTO", String.valueOf(montoTotalDL));
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosDL));
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pendientes No Negociables ");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pre No Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Dólares No Negociables ");	
					}								
					registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalDLNoNeg,2));	
					registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoDLNoNeg,2));			
				}			
				if(t==5){
					registrosTot1.put("MONEDA", "54");
					registrosTot1.put("MONTO", String.valueOf(montoTotalDL));
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosDL));
					if("S".equals(cbVenSinOperar)){
						registrosTot1.put("INFORMACION", " Total Dólares Vencido Sin Operar ");	
						registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalDLSinOper,2));	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoDLSinOper,2) );	
					}
				}			
					
				if(t==6){	
					registrosTot1.put("MONEDA", "1");
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosMN));
					registrosTot1.put("MONTO", String.valueOf(montoTotalMN));
					registrosTot1.put("INFORMACION", "Total Documentos en Moneda Nacional ");	
					registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosMN) );	
				}		
			
				if(t==7){	
					registrosTot1.put("MONEDA", "54");
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosDL));
					registrosTot1.put("MONTO", String.valueOf(montoTotalDL));
					registrosTot1.put("INFORMACION", "Total Documentos en Dólares ");	
					registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosDL) );	
				}			
			}
			
			if("N".equals(cbVenSinOperar)){			
				if(t==0){ 	
					registrosTot1.put("MONEDA", "1");
					registrosTot1.put("MONTO", String.valueOf(montoTotalMN));
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosMN));
					if(strPendiente.equals("S")){
							registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes Negociables");		
						}else if(strPreNegociable.equals("S")){
							registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre Negociables ");	
						}else{
							registrosTot1.put("INFORMACION", "Total Moneda Nacional Negociables");	
						}					
						registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalMNNeg,2)   );	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNeg,2) );				
				}		
				if(t==1){ 			
					registrosTot1.put("MONEDA", "1");
					registrosTot1.put("MONTO", String.valueOf(montoTotalMN));
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosMN));
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes No Negociables ");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre No Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Moneda Nacional No Negociables ");	
					}			
					registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalMNNoNeg,2));	
					registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNoNeg,2) );			
				}		
							
				//DOLARES
				if(t==2){ 
					registrosTot1.put("MONEDA", "54");
					registrosTot1.put("MONTO", String.valueOf(montoTotalDL));
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosDL));
					if(strPendiente.equals("S")){						
						registrosTot1.put("INFORMACION", "Total Dólares Pendientes Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pre Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Dólares Negociables");	
					}					
					registrosTot1.put("TOTAL_MONTO",  "$"+Comunes.formatoDecimal(montoTotalDLNeg,2)   );	
					registrosTot1.put("TOTAL_MONTO_DESC", "$"+Comunes.formatoDecimal(montoTotalDsctoDLNeg,2));				
				}		
				if(t==3){ 		
					registrosTot1.put("MONEDA", "54");
					registrosTot1.put("MONTO", String.valueOf(montoTotalDL));
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosDL));
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pendientes No Negociables ");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pre No Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Dólares No Negociables ");	
					}								
					registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalDLNoNeg,2));	
					registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoDLNoNeg,2));			
				}			
								
				if(t==4){	
					registrosTot1.put("MONEDA", "1");
					registrosTot1.put("MONTO", String.valueOf(montoTotalMN));
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosMN));
					registrosTot1.put("INFORMACION", "Total Documentos en Moneda Nacional ");	
					registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosMN) );	
				}		
			
				if(t==5){	
					registrosTot1.put("MONEDA", "54");
					registrosTot1.put("MONTO", String.valueOf(montoTotalDL));
					registrosTot1.put("INFORMACION", "Total Documentos en Dólares ");	
					registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosDL) );
					registrosTot1.put("TOTAL_DOCTO", String.valueOf(numRegistrosDL));
				}			
			}
				registrosTotales1.add(registrosTot1);
		
		}//FOR
		
		consulta =  "{\"success\": true, \"total\": \"" + registrosTotales1.size() + "\", \"registros\": " + registrosTotales1.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
	
	infoRegresar  = jsonObj.toString();
	
	
}else  if(informacion.equals("ConfirmaCarga") ) {
	
		Acuse acuse = null;
		acuse = new Acuse(Acuse.ACUSE_EPO, "1");
		String ses_ic_usuario = iNoUsuario;
		boolean bInsertaAcuse = false;	
		String mensajeError="";
		String _acuse  ="";
		String mostrarAcuse ="N";
		String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
		String externContent = (request.getParameter("textoFirmado")==null)?"":request.getParameter("textoFirmado");
		
		String txttotdocM = (request.getParameter("txttotdocM") != null) ? request.getParameter("txttotdocM") : "0";
		String txtmtodocM = (request.getParameter("txtmtodocM") != null) ? request.getParameter("txtmtodocM") : "0";
		String txttotdocdoD = (request.getParameter("txttotdocdoD") != null) ? request.getParameter("txttotdocdoD") : "0";
		String txtmtodocdoD = (request.getParameter("txtmtodocdoD") != null) ? request.getParameter("txtmtodocdoD") : "0";
		if(txttotdocM.equals("")) {  txttotdocM ="0"; }
		if(txtmtodocM.equals("")) {  txtmtodocM ="0"; }
		if(txttotdocdoD.equals("")) {  txttotdocdoD ="0"; }
		if(txtmtodocdoD.equals("")) {  txtmtodocdoD ="0"; }
		
		String doctoDuplicados = (request.getParameter("doctoDuplicados") != null) ? request.getParameter("doctoDuplicados") : "N";	
		
		char getReceipt = 'Y';
		String folioCert  ="";
		
		if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
			folioCert = acuse.toString();			
			Seguridad s = new Seguridad();
					
			if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
				_acuse = s.getAcuse();
				System.out.println(" folioCert = " + folioCert + " , _acuse = " + _acuse);// Debug info				
										
				try {
					//-- Ejecucion del Store Procedure --
				   CargaDocumentos.EjecutaSPInsertaDocto(
										Integer.parseInt(proceso), 	folioCert, 
										Integer.parseInt(txttotdocM),			Double.parseDouble(txtmtodocM), 
										Integer.parseInt(txttotdocdoD), 		Double.parseDouble(txtmtodocdoD), 
										iNoUsuario, 								_acuse,	strClaveHash,
										strNombreUsuario,							(String) session.getAttribute("strCorreoUsuario"), doctoDuplicados ); 
										
					
					mensajeError = "La autentificación se llevó a cabo con éxito "+"Recibo: "+_acuse;
					mostrarAcuse="S";
				
				} catch (NafinException ne){
					out.println(ne.getMsgError(sesIdiomaUsuario));	
					mensajeError = ne.getMsgError(sesIdiomaUsuario);	
				}			
			
			}	else  {
				mensajeError = "La autentificación no se llevo acabo con éxito "+s.mostrarError();
				mostrarAcuse="N";
				
				boolean bBorraDoctos = CargaDocumentos.borraDoctosCargados(proceso, "M", iNoCliente);
				if(!ic_proc_docto_det.equals("") && !ic_proc_docto_det.equals("0")) {
					CargaDocumentos.borraDoctosDetalleCargados(ic_proc_docto_det, "M");
				}							
			}
			
			
			}	else  {
				mensajeError = "La autentificación no se llevo acabo con éxito ";
				mostrarAcuse="N";	
			}

		hidFechaCarga = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		hidHoraCarga = (new SimpleDateFormat ("hh:mm:ss")).format(new java.util.Date());
		usuario = iNoUsuario+" - "+strNombreUsuario;
		
		jsonObj.put("success", new Boolean(true));	
		jsonObj.put("mostrarAcuse",mostrarAcuse);
		jsonObj.put("acuseFormateado",acuse.formatear());
		jsonObj.put("acuse",folioCert);
		jsonObj.put("recibo",_acuse);	
		jsonObj.put("proceso",proceso);	
		jsonObj.put("mensajeError",mensajeError);	
		jsonObj.put("hidFechaCarga",hidFechaCarga);	
		jsonObj.put("hidHoraCarga",hidHoraCarga);			
		jsonObj.put("usuario",usuario);
		jsonObj.put("doctoDuplicados",doctoDuplicados);
		
		infoRegresar  = jsonObj.toString();



}else  if(informacion.equals("ArchivoErrores") ) {

	StringBuffer contenidoArchivoA = new StringBuffer();
	CreaArchivo archivos = new CreaArchivo();
	String nombreArchivo = null;
	
	Vector vDoctosDetalle = CargaDocumentos.ObtenerDetalleCargaM(Integer.parseInt(proceso), sesIdiomaUsuario);
	String lineasErrores	= vDoctosDetalle.get(2).toString();	
	

	if(!archivos.make(lineasErrores.toString(), strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo Errores ");
	} else {
		nombreArchivo = archivos.nombre;		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} 
 
		infoRegresar = jsonObj.toString();
		
}else if( informacion.equals("GuardaDuplicidad") ) {

	CargaDocumentos.insertaBitacoraDuplicadosCa("", "", 
		"",  "",  ses_ic_epo,  "",  iNoUsuario,  strNombreUsuario,
		 "2", proceso ); 

	jsonObj.put("success", new Boolean(true));
	infoRegresar  = jsonObj.toString();	

}

%>
<%=infoRegresar%>

<%!
	public String getNumeroSIAFF(String ic_epo,String ic_documento){
		return getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(ic_documento);		
	}
	
	public String getNumeroDeCuatroDigitos(String numero){

		String 			claveEPO 	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();
		
      for(int i = 0;i<(4-claveEPO.length());i++){
			resultado.append("0");
      }
      resultado.append(claveEPO);

      return resultado.toString();
  }
  
  	public String getNumeroDeOnceDigitos(String numero){

		String 			claveDocto	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();
		
      for(int i = 0;i<(11-claveDocto.length());i++){
			resultado.append("0");
      }
      resultado.append(claveDocto);

      return resultado.toString();
  }
%>

<%
//System.out.println(informacion+"    infoRegresar  --->"+infoRegresar); 

%>
