<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%
	
  String strPendiente = (request.getParameter("strPendiente") != null) ? request.getParameter("strPendiente") : "N";
	String strPreNegociable = (request.getParameter("strPreNegociable") != null) ? request.getParameter("strPreNegociable") : "N";

	String strFirmaManc = (request.getParameter("strFirmaManc") != null) ? request.getParameter("strFirmaManc") : "N";
	String strHash = (request.getParameter("strHash") != null) ? request.getParameter("strHash") : "N";

	String txttotdoc = (request.getParameter("txttotdoc") != null) ? request.getParameter("txttotdoc") : "0";
	String txtmtodoc = (request.getParameter("txtmtodoc") != null) ? request.getParameter("txtmtodoc") : "0";
	String txtmtomindoc = (request.getParameter("txtmtomindoc") != null) ? request.getParameter("txtmtomindoc") : "0";
	String txtmtomaxdoc = (request.getParameter("txtmtomaxdoc") != null) ? request.getParameter("txtmtomaxdoc") : "0";
	
	String txttotdocdo = (request.getParameter("txttotdocdo") != null) ? request.getParameter("txttotdocdo") : "0";
	String txtmtodocdo = (request.getParameter("txtmtodocdo") != null) ? request.getParameter("txtmtodocdo") : "0";
	String txtmtomindocdo = (request.getParameter("txtmtomindocdo") != null) ? request.getParameter("txtmtomindocdo") : "0";
	String txtmtomaxdocdo = (request.getParameter("txtmtomaxdocdo") != null) ? request.getParameter("txtmtomaxdocdo") : "0";

		
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<script type="text/javascript" src="13forma02aExt.js?<%=session.getId()%>"></script> 

<%@ include file="/00utils/componente_firma.jspf" %>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
	
	
<form id='formParametros' name="formParametros">
	<input type="hidden" id="strPendiente" name="strPendiente" value="<%=strPendiente%>"/>	
	<input type="hidden" id="strPreNegociable" name="strPreNegociable" value="<%=strPreNegociable%>"/>
	<input type="hidden" id="strFirmaManc" name="strFirmaManc" value="<%=strFirmaManc%>"/>
	<input type="hidden" id="strHash" name="strHash" value="<%=strHash%>"/>
	
	<input type="hidden" id="txttotdoc" name="txttotdoc" value="<%=txttotdoc%>"/>
	<input type="hidden" id="txtmtodoc" name="txtmtodoc" value="<%=txtmtodoc%>"/>
	<input type="hidden" id="txtmtomindoc" name="txtmtomindoc" value="<%=txtmtomindoc%>"/>
	<input type="hidden" id="txtmtomaxdoc" name="txtmtomaxdoc" value="<%=txtmtomaxdoc%>"/>
	
	<input type="hidden" id="txttotdocdo" name="txttotdocdo" value="<%=txttotdocdo%>"/>
	<input type="hidden" id="txtmtodocdo" name="txtmtodocdo" value="<%=txtmtodocdo%>"/>
	<input type="hidden" id="txtmtomindocdo" name="txtmtomindocdo" value="<%=txtmtomindocdo%>"/>
	<input type="hidden" id="txtmtomaxdocdo" name="txtmtomaxdocdo" value="<%=txtmtomaxdocdo%>"/>
	<input type="hidden" id="strPerfil" name="strPerfil" value="<%=strPerfil%>"/>

	
</form>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
