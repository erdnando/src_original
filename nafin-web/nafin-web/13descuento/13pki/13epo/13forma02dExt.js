
Ext.onReady(function() {
	
	var claveEPO =  Ext.getDom('claveEPO').value;	
	var proceso; 
	
	var textoForma ="Carga de Archivo";
	if(claveEPO==211) {
	textoForma ="Cifras de Control del Detalle";
	}
		
	//cancelar la captura de documentos
	function procesarCancelar(opts, success, response) {
		if(!confirm("�Est� seguro de querer cancelar la operaci�n?")) {
			return;
		}else {		
			document.location.href  = "13forma02dExt.jsp";
		}	
	}
	
	
	var procesarSuccessFailureArchivoPDF =  function(opts, success, response) {
		var btnGenerarPdf = Ext.getCmp('btnGenerarPdf');
		btnGenerarPdf.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPdf = Ext.getCmp('btnBajarPdf');
			btnBajarPdf.show();
			btnBajarPdf.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPdf.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPdf.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;			
		var fp = Ext.getCmp('fpCarga');
		fp.el.unmask();
					
		if (arrRegistros != null) {
			if (!gridResultado.isVisible()) {
				gridResultado.show();
			}	
			
			fpCarga.hide();
			
			proceso = jsonData.proceso;
			var el = gridResultado.getGridEl();	
		
			if(store.getTotalCount() > 0) {
		
		
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
			
		}			
	}
		//procesa  el archivo de Carga de Documentos Detalle 
	var procesarSuccessFailureCarga =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			
			var resp = 	Ext.util.JSON.decode(response.responseText);	
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');	
			var table;
			
			if(resp.numReg!='0'){
				procentaje = parseFloat(resp.numProcReg)/parseFloat(resp.numReg);
				procentaje = Math.round(procentaje * Math.pow(10, 1)) / Math.pow(10, 1);
			}
					
			fpCarga.setTitle('Proceso de Validaci�n');			
			fpCarga.add(barraProgreso);
					
			
			barraProgreso.updateProgress(procentaje,'Validando '+resp.numProcReg+' de '+resp.numReg);
						
			if(resp.EstatusReg == 'PR'){	
			 ajaxPeticionCarga(resp.proceso,resp.EstatusReg,resp.nocampos_detalle,
												resp.txttotdoc,	resp.txtmtodoc, resp.txtmtomindoc,resp.txtmtomaxdoc,					
												resp.txttotdocdo,resp.txtmtodocdo,resp.txtmtomindocdo,resp.txtmtomaxdocdo);
			}else if(resp.EstatusReg =='ER') {	
			
			fpCarga.remove(barraProgreso);
			
				if(claveEPO!=211) {
					table ='<table align="center" width="550" class="formas"  cellspacing="0" cellpadding="0">'+
										 '<tr>'+
												'<td width="275"  align="center">Errores Generados Archivo Detalle:</td> '+											
											'</tr>'+									
											'<tr>'+
												'<td valign="middle" align="center" class="formas" width="275" >'+
													'<textarea name="conerrores" cols="50" wrap="hard" rows="20" class="formas">'+resp.resultado+'</textarea>'+
												'</td>'+											
											'</tr>'+	
										'</table>';
										
				}else if(claveEPO==211) {
				
					table = '<table align="center" width="550" class="formas"  cellspacing="0" cellpadding="0">'+
										'<tr>'+
											'<td width="275"  align="center">Documentos sin Errores:</td> '+
											'<td width="275"  align="center">Errores Generados Archivo Detalle::</td> '+
										 '</tr>'+									
										 '<tr>'+
											'<td valign="middle" align="center" class="formas" width="275" >'+
												'<textarea name="conerrores" cols="50" wrap="hard" rows="20" class="formas">'+resp.detallesValidos+'</textarea>'+
											'</td>'+
											'<td valign="middle" align="center" class="formas" width="275" >'+
												'<textarea name="conerrores" cols="50" wrap="hard" rows="20" class="formas">'+resp.resultado+'</textarea>'+
											'</td>'+
										'</tr>'+																				
										'<tr>'+
											'<td valign="middle" align="center" colspan="4" class="formas" width="550">'+
												'<table cellpadding="1" align="center"   cellspacing="2" border="0">'+						
													'<tr>'+
															'<td valign="middle" align="center" class="formas" width="130">Total Moneda Nacional:</td> '+
															'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnOK" size="18" value="'+resp.totalMNValidos+'" readonly onFocus="this.blur()"></td>'+
															'<td valign="middle" align="center" class="formas" width="130">Total Moneda Nacional:</td> '+
															'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnErr" size="18" value="'+resp.totalUSValidos+'" readonly onFocus="this.blur()"></td>'+
														'</tr>'+
														'<tr>'+
															'<td valign="middle" align="center" class="formas" width="130">Total D�lares:</td> '+
															'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnOK" size="18" value="'+resp.totalMNError+'" readonly onFocus="this.blur()"></td>'+
															'<td valign="middle" align="center" class="formas" width="130">Total D�lares:</td> '+
															'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnErr" size="18" value="'+resp.totalUSDError+'" readonly onFocus="this.blur()"></td>'+
														'</tr>'+
													'<table>'+
												'</td> '+	
											'</tr>'+											
											'<tr>'+
												'<td width="275" align="center" >Errores vs Cifras de Control:</td> '+											
											'</tr>'+										
											'<tr>'+
												'<td valign="middle" align="center" class="formas" width="275"  >'+
													'<textarea name="conerrores" cols="100" wrap="hard" rows="6" class="formas">'+resp.erroresProceso+'</textarea>'+
												'</td>'+
											'</tr>'+										
										'</table>';		
										
										Ext.getCmp("txttotdoc1").setValue(resp.txttotdoc);
										Ext.getCmp("txtmtodoc1").setValue(resp.txtmtodoc);
										Ext.getCmp("txtmtomindoc1").setValue(resp.txtmtomindoc);
										Ext.getCmp("txtmtomaxdoc1").setValue(resp.txtmtomaxdoc);
										
										Ext.getCmp("txttotdocdol").setValue(resp.txttotdocdo);
										Ext.getCmp("txtmtodocdol").setValue(resp.txtmtodocdo);
										Ext.getCmp("txtmtomindocdol").setValue(resp.txtmtomindocdo);
										Ext.getCmp("txtmtomaxdocdol").setValue(resp.txtmtomaxdocdo);
										
										var cancelar = Ext.getCmp("cancelar");																			
										var txttotdoc1 = Ext.getCmp("txttotdoc1");
										var txtmtodoc1 = Ext.getCmp("txtmtodoc1");
										var txtmtomindoc1 = Ext.getCmp("txtmtomindoc1");
										var txtmtomaxdoc1  = Ext.getCmp("txtmtomaxdoc1");
										
										var txttotdocdol = Ext.getCmp("txttotdocdol");
										var txtmtodocdol = Ext.getCmp("txtmtodocdol");
										var txtmtomindocdol = Ext.getCmp("txtmtomindocdol");
										var txtmtomaxdocdol  = Ext.getCmp("txtmtomaxdocdol");
										
										var tituloNacional  = Ext.getCmp("tituloNacional");
										var tituloDorales  = Ext.getCmp("tituloDorales");
										
										txttotdoc1.hide();
										txtmtodoc1.hide();
										txtmtomindoc1.hide();
										txtmtomaxdoc1.hide();
										
										txttotdocdol.hide();
										txtmtodocdol.hide();
										txtmtomindocdol.hide();
										txtmtomaxdocdol.hide();
										
										tituloNacional.hide();
										tituloDorales.hide();
										cancelar.show();
										
				}
				
			}
		
			
			var fpError = new Ext.Container({
				layout: 'table',
				id: 'fpError',				
				width:	550,
				heigth:	300,
				hidden: true,	
				style: 'margin:0 auto;',
				items: [		
					{ 
						xtype:   'label',  
						html:		table, 
						cls:		'x-form-item'					
					}				
				]
			});	
					
			if(resp.EstatusReg =='ER') {				
				fpError.show();			
				contenedorPrincipalCmp.remove(fpError);				
				contenedorPrincipalCmp.add(fpError);
				contenedorPrincipalCmp.doLayout();
				fpCarga.doLayout();	
			}
								
			if(resp.EstatusReg =='OK') {	
				fpError.hide();
				contenedorPrincipalCmp.remove(fpError);			
				contenedorPrincipalCmp.doLayout();
				fpCarga.doLayout();	
				
				consultaData.load({
					params: {  
						informacion: 'ConsultaResultado', 	
						proceso:resp.proceso 		
					}  
				});		
			
			}
			
			
			
		} else {		
			NE.util.mostrarConnError(response,opts);
		}
	}
	

//procesa  el archivo de Carga de Documentos 

	var ajaxPeticionCarga = function (proceso,EstatusReg,nocampos_detalle,	txttotdoc,	txtmtodoc, txtmtomindoc,txtmtomaxdoc,					
												txttotdocdo,txtmtodocdo,txtmtomindocdo,txtmtomaxdocdo){
	
		Ext.Ajax.request({
				url: '13forma02dExt.data.jsp',
				params:{														
					informacion: 'ProcesaCargaArchivo',
					proceso:proceso,
					EstatusReg:EstatusReg,
					nocampos_detalle:nocampos_detalle,
					txttotdoc:txttotdoc,
					txtmtodoc:txtmtodoc,
					txtmtomindoc:txtmtomindoc,
					txtmtomaxdoc:txtmtomaxdoc,					
					txttotdocdo:txttotdocdo,
					txtmtodocdo:txtmtodocdo,
					txtmtomindocdo:txtmtomindocdo,
					txtmtomaxdocdo:txtmtomaxdocdo			
				},
				callback: procesarSuccessFailureCarga
			});
	}		
	
	//para mostrar los archivos con errores
	var procesarSuccessFailureArchivo =  function(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var leyendaLegal  = Ext.getCmp("leyendaLegal");
			leyendaLegal.hide();
				
			var resp = 	Ext.util.JSON.decode(response.responseText);	
			if(resp.mensaje !='') {			
				leyendaLegal.show();
			}else {
			
				ajaxPeticionCarga(resp.proceso,resp.EstatusReg,resp.nocampos_detalle,
												resp.txttotdoc,	resp.txtmtodoc, resp.txtmtomindoc,resp.txtmtomaxdoc,					
												resp.txttotdocdo,resp.txtmtodocdo,resp.txtmtomindocdo,resp.txtmtomaxdocdo);	
			}
		} else {
		
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var leyenda = '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
					'<tr><td class="formas" style="text-align: justify;" colspan="3">No ha definido campos para la carga de detalles. Por favor, verif�quelo.	</td></tr></table>';
		
	var leyendaLegal = new Ext.Container({
		layout: 'table',
		id: 'leyendaLegal',	
		hidden: true,
		layoutConfig: {
			columns: 20
		},
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		leyenda, 
				cls:		'x-form-item', 
				style: { 
					width:   		'700%', 
					textAlign: 		'left'
				} 
			}			
		]
	});
	

	
	
	var barraProgreso = new Ext.ProgressBar({
		text:'Iniciando Validaci�n...',
		 id:'pbar2',
		cls:'left-align',
		width: 690
		//hidden: true
 });
 
	//funcion para valdiar el formato
	var myValidFn = function(v) {	
		var myRegex = /^.+\.([tT][xX][tT])$/;
		return myRegex.test(v);	
	}
	
	Ext.apply(Ext.form.VTypes, {
			archivotxt 		: myValidFn,
			archivotxtText 	: 'El formato del archivo de origen no es el correcto para el tipo de archivo seleccionado'
	});
	
	//funcion para enviar el archivoa validar 	
	var enviarArchivo = function() {
	
			var txttotdoc2;
			var txtmtodoc2;
			var txtmtomindoc2 ;
			var txtmtomaxdoc2;						
			var txttotdocdo2;
			var txtmtodocdo2;
			var txtmtomindocdo2;
			var txtmtomaxdocdo2;
			
		if(claveEPO==211) {
			var continua = true;
			var txttotdoc1 = Ext.getCmp("txttotdoc1");
			var txtmtodoc1 = Ext.getCmp("txtmtodoc1");
			var txtmtomindoc1 = Ext.getCmp("txtmtomindoc1");
			var txtmtomaxdoc1  = Ext.getCmp("txtmtomaxdoc1");
			
			var txttotdocdol = Ext.getCmp("txttotdocdol");
			var txtmtodocdol = Ext.getCmp("txtmtodocdol");
			var txtmtomindocdol = Ext.getCmp("txtmtomindocdol");
			var txtmtomaxdocdol  = Ext.getCmp("txtmtomaxdocdol");
				
			Ext.getCmp("txttotdoc1").setValue(eliminaFormatoFlotante(txttotdoc1.getValue()));
			Ext.getCmp("txtmtodoc1").setValue(eliminaFormatoFlotante(txtmtodoc1.getValue()));
			Ext.getCmp("txtmtomindoc1").setValue(eliminaFormatoFlotante(txtmtomindoc1.getValue()));
			Ext.getCmp("txtmtomaxdoc1").setValue(eliminaFormatoFlotante(txtmtomaxdoc1.getValue()));
			
			Ext.getCmp("txttotdocdol").setValue(eliminaFormatoFlotante(txttotdocdol.getValue()));
			Ext.getCmp("txtmtodocdol").setValue(eliminaFormatoFlotante(txtmtodocdol.getValue()));
			Ext.getCmp("txtmtomindocdol").setValue(eliminaFormatoFlotante(txtmtomindocdol.getValue()));
			Ext.getCmp("txtmtomaxdocdol").setValue(eliminaFormatoFlotante(txtmtomaxdocdol.getValue()));
		
			if (  ( Ext.isEmpty(txttotdoc1.getValue())   ||  txttotdoc1.getValue()==0  ) 
			&& ( Ext.isEmpty(txttotdocdol.getValue()) ||  txttotdocdol.getValue()==0 )	 ) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el  n�mero total documentos en Moneda Nacional y/o Dolares");
				continua = false;
				return;
			}	
			
			//monedda Nacional
		
		
			if(!Ext.isEmpty(txttotdoc1.getValue())  ||  txttotdoc1.getValue()>0  ) {
			
				if(Ext.isEmpty(txtmtodoc1.getValue()) || txtmtodoc1.getValue() ==0) {
					Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
					continua = false;
					return;
				}
				
				if(Ext.isEmpty(txtmtomindoc1.getValue()) || txtmtomindoc1.getValue() ==0) {
					Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
					continua = false;
					return;
				}
				
				if(Ext.isEmpty(txtmtomaxdoc1.getValue()) || txtmtomaxdoc1.getValue() ==0) {
					Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
					continua = false;
					return;
				}
							
				if(  (parseFloat(txtmtodoc1.getValue()) < parseFloat(txtmtomaxdoc1.getValue()) ) 
					|| (parseFloat(txtmtomaxdoc1.getValue()) < parseFloat(txtmtomindoc1.getValue() )  )
					|| (parseFloat(txtmtodoc1.getValue) < parseFloat(txtmtomindoc1.getValue()) ) ){
					Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (Moneda Nacional)");
					continua = false;
					return;
				}		
			}
			
			//moneda Dolar		
			
			
			if(!Ext.isEmpty(txttotdocdol.getValue())  ) {	
			
				if(Ext.isEmpty(txtmtodocdol.getValue()) || txtmtodocdol.getValue()==0) {
					Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
					continua = false;
					return;
				}
				
				if(Ext.isEmpty(txtmtomindocdol.getValue()) || txtmtomindocdol.getValue()==0) {
					Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
					continua = false;
					return;
				}
				
				if(Ext.isEmpty(txtmtomaxdocdol.getValue()) || txtmtomaxdocdol.getValue() ==0) {
					Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
					continua = false;
					return;
				}
							
				if( (parseFloat(txttotdocdol.getValue()) < parseFloat(txtmtomaxdocdol.getValue())) 
					|| (parseFloat(txtmtomaxdocdol.getValue()) < parseFloat(txtmtomindocdol.getValue()) )
					|| (parseFloat(txttotdocdol.getValue) < parseFloat(txtmtomindocdol.getValue()))  ){		
					Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (Dolares)");
					continua = false;
					return;
				}				
			}
			
		 if (Ext.isEmpty(txttotdoc1.getValue()) ) {  	Ext.getCmp("txttotdoc1").setValue(0);  }
			if (Ext.isEmpty(txtmtodoc1.getValue()) ) {  	Ext.getCmp("txtmtodoc1").setValue(0); }
			if (Ext.isEmpty(txtmtomindoc1.getValue()) ) {  	Ext.getCmp("txtmtomindoc1").setValue(0); }
			if (Ext.isEmpty(txtmtomaxdoc1.getValue()) ) {  	Ext.getCmp("txtmtomaxdoc1").setValue(0); }
			
			if (Ext.isEmpty(txttotdocdol.getValue()) ) {  	Ext.getCmp("txttotdocdol").setValue(0); }
			if (Ext.isEmpty(txtmtodocdol.getValue()) ) {  	Ext.getCmp("txtmtodocdol").setValue(0); }
			if (Ext.isEmpty(txtmtomindocdol.getValue()) ) {  	Ext.getCmp("txtmtomindocdol").setValue(0); }
			if (Ext.isEmpty(txtmtomaxdocdol.getValue()) ) {  	Ext.getCmp("txtmtomaxdocdol").setValue(0); }

			 txttotdoc2 = txttotdoc1.getValue();
			 txtmtodoc2 = txtmtodoc1.getValue();
			 txtmtomindoc2 =txtmtomindoc1.getValue();
			 txtmtomaxdoc2=txtmtomaxdoc1.getValue();	
			 
			 txttotdocdo2 =txttotdocdol.getValue();
			 txtmtodocdo2 =txtmtodocdol.getValue();
			 txtmtomindocdo2 =txtmtomindocdol.getValue();
			 txtmtomaxdocdo2 =txtmtomaxdocdol.getValue();
		
		} //claveEPO
		
		var archivo = Ext.getCmp("archivo");
		if (Ext.isEmpty(archivo.getValue()) ) {
			archivo.markInvalid('El valor de la Ruta es requerida.');	
			return;
		}
		
							
		fpCarga.getForm().submit({
			url: '13forma02file.data.jsp',									
			waitMsg: 'Enviando datos...',
			success: function(form, action) {								
				var resp = action.result;
				var mArchivo =resp.archivo;
				var error_tam = resp.error_tam;									
				if(mArchivo!='') {
					Ext.Ajax.request({
						url: '13forma02dExt.data.jsp',
						params:{														
							informacion: 'CargaArchivoDetalle',
							archivo:mArchivo,
							txttotdoc:txttotdoc2,
							txtmtodoc:txtmtodoc2,
							txtmtomindoc:txtmtomindoc2,
							txtmtomaxdoc:txtmtomaxdoc2,							
							txttotdocdo:txttotdocdo2,
							txtmtodocdo:txtmtodocdo2,
							txtmtomindocdo:txtmtomindocdo2,
							txtmtomaxdocdo:txtmtomaxdocdo2
						},
						callback: procesarSuccessFailureArchivo
					});			
				}	else{
					Ext.MessageBox.alert("Mensaje","El Archivo es muy Grande, excede el L�mite que es de 2 MB.");
					return;
				}
				
				}
				,failure: NE.util.mostrarSubmitError
			});		
	}
		
	
	var elementosFormaCifras = [		
		{ 
			xtype:   'label',  
			html:		'Moneda Nacional', 
			id: 'tituloNacional',
			hidden: true,
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},		
		{
			xtype: 'numberfield',
			name: 'txttotdoc',
			id: 'txttotdoc1',
			hidden: true,
			fieldLabel: 'No. total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 7,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0'		
		},
		{
			xtype: 'textfield',
			name: 'txtmtodoc',
			id: 'txtmtodoc1',
			hidden: true,
			fieldLabel: 'Monto total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomindoc',
			id: 'txtmtomindoc1',
			hidden: true,
			fieldLabel: 'Monto m�nimo de los documentos: ',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomaxdoc',
			id: 'txtmtomaxdoc1',
			hidden: true,
			fieldLabel: 'Monto m�ximo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		},
		{ 
			xtype:   'label',  
			html:		'Dolares', 
			id: 'tituloDorales',
			hidden: true,
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},	
		{
			xtype: 'numberfield',
			name: 'txttotdocdo',
			id: 'txttotdocdol',
			hidden: true,
			fieldLabel: 'No. total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 7,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0'		
		},
		{
			xtype: 'textfield',
			name: 'txtmtodocdo',
			id: 'txtmtodocdol',
			hidden: true,
			fieldLabel: 'Monto total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomindocdo',
			id: 'txtmtomindocdol',
			hidden: true,
			fieldLabel: 'Monto m�nimo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomaxdocdo',
			id: 'txtmtomaxdocdol',
			hidden: true,
			fieldLabel: 'Monto m�ximo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
			}
		}
	];
		
	var elementosFormaCarga = [		
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[				
				{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: .95,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 100,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									emptyText: 'Ruta del Archivo Detalle',
									fieldLabel: 'Ruta del Archivo Detalle',
									name: 'archivoFormalizacion',   
									buttonText: 'Examinar...',
									width: 320,	  																		
									vtype: 'archivotxt'
								}						
							]
						}
					]
				}	
			]
		}	
	];
	
	var fpCarga = new Ext.form.FormPanel({
	  id: 'fpCarga',
		width: 550,
		title: textoForma,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},		
		items:[
			elementosFormaCifras, 
			elementosFormaCarga
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Continuar',
				id: 'continuar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:enviarArchivo
			},
			{
				text: 'Cancelar',
				id: 'cancelar',
				iconCls: 'icoAceptar',
				formBind: true,
				hidden: true,
				handler:procesarCancelar
			}
		]
	});
	
	  var pbar1 = new Ext.ProgressBar({
       text:'Initializing...'
    });
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma02dExt.data.jsp',
		baseParams: {
			informacion: 'ConsultaResultado'
		},
		fields: [				
			{name: 'NUMERO_DOCUMENTO'},	
			{name: 'NUMERO_DETALLES'}		
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
	var gridResultado = new Ext.grid.EditorGridPanel({
		id: 'gridResultado',
		title: 'Carga de Detalles',
		hidden: true,
		store: consultaData,
		clicksToEdit: 1,				
		columns: [	
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUMERO_DOCUMENTO',
				width : 300,
				align: 'left',
				sortable : true
			},
			{							
				header : 'N�mero de Detalles Cargados',
				tooltip: 'N�mero de Detalles Cargados',
				dataIndex : 'NUMERO_DETALLES',
				width : 200,
				align: 'center',
				sortable : true
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 520,
		style: 'margin:0 auto;',
		frame: false,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdf',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13forma02dExt.data.jsp',
							params: {
								informacion:'ArchivoPDF',
								proceso:proceso
							}
							,callback: procesarSuccessFailureArchivoPDF
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar PDF',
					id: 'btnBajarPdf',
					hidden: true
				},				
				{
					xtype: 'button',
					text: 'Regresar',
					tooltip:	'Regresar',
					id: 'btnRegresar',
					handler: function() {
						window.location = '13forma02Ext.jsp';
					}					
				}
			]
		}			
	});
			
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fpCarga,
			NE.util.getEspaciador(20),
			gridResultado, 
			leyendaLegal, 
			NE.util.getEspaciador(20)
		]
	});
	
	if(claveEPO==211) {
		var tituloNacional = Ext.getCmp("tituloNacional");
		var txttotdoc1 = Ext.getCmp("txttotdoc1");
		var txtmtodoc1 = Ext.getCmp("txtmtodoc1");
		var txtmtomindoc1 = Ext.getCmp("txtmtomindoc1");
		var txtmtomaxdoc1  = Ext.getCmp("txtmtomaxdoc1");			
		var txttotdocdol = Ext.getCmp("txttotdocdol");
		var txtmtodocdol = Ext.getCmp("txtmtodocdol");
		var txtmtomindocdol = Ext.getCmp("txtmtomindocdol");
		var txtmtomaxdocdol  = Ext.getCmp("txtmtomaxdocdol");
		var tituloDorales = Ext.getCmp("tituloDorales");
		tituloNacional.show();
		txttotdoc1.show();
		txtmtodoc1.show();
		txtmtomindoc1.show();
		txtmtomaxdoc1.show();
		
		txttotdocdol.show();
		txtmtodocdol.show();
		txtmtomindocdol.show();
		txtmtomaxdocdol.show();
		tituloDorales.show();
		
		
}

} );