<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.math.*, 
	java.text.*, 
	java.sql.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	org.apache.commons.logging.Log,
	java.util.Date,	
	com.netro.descuento.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>
<%!
	private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<% 
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";	
String ic_if = (request.getParameter("ic_if")!=null)?request.getParameter("ic_if"):"";	

String infoRegresar ="", consulta ="";
String  ic_epo =iNoCliente;
JSONObject jsonObj = new JSONObject();

ConsBloqueoLimitesXIF paginador = new ConsBloqueoLimitesXIF (); 
paginador.setIc_epo(ic_epo);
paginador.setIc_if(ic_if);


if (informacion.equals("catIntermediarioData")){

	CatalogoIFDescuento catalogo = new CatalogoIFDescuento();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");
	catalogo.setClaveEpo(ic_epo);
	catalogo.setOrden("cg_razon_social");
	
	infoRegresar = catalogo.getJSONElementos();	
	
	
}else  if (informacion.equals("Consultar")){

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	Registros reg	=	queryHelper.doSearch();
	consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	jsonObj = JSONObject.fromObject(consulta);
	infoRegresar = jsonObj.toString();  
	
}else  if (informacion.equals("GuardarBloqueo")){

	String ic_reg_ifs[] = request.getParameterValues("ic_reg_ifs");
	String cs_activacion[] = request.getParameterValues("cs_activacion");
	
	
	String pkcs7 = (request.getParameter("pkcs7")!=null)?request.getParameter("pkcs7"):"";	
	String textoFirmado = (request.getParameter("textoFirmado")!=null)?request.getParameter("textoFirmado"):"";	
	
	String externContent = (request.getParameter("textoFirmado")==null)?"":request.getParameter("textoFirmado");
	char getReceipt = 'Y';
	String folioCert  ="", _acuse  ="", mensaje ="";
	Acuse acuse = new Acuse(iNoUsuario);
	
	String  usuarioBloqueo = strLogin +"-"+strNombreUsuario;
	String  dependenciaBloqueo = strNombre;

	if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
		folioCert = acuse.toString();			
		Seguridad s = new Seguridad();
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			_acuse = s.getAcuse();
			
			try {			
					
				paginador.bactualizaBloqueo( ic_reg_ifs,  cs_activacion ,  ic_epo, iNoUsuario,  _acuse, usuarioBloqueo,  dependenciaBloqueo ) ;
			
				jsonObj.put("success", new Boolean(true));	
				mensaje = "La actualización ha sido realizada con éxito";					
				
			} catch (AppException ne){
				jsonObj.put("success", new Boolean(false));	
				mensaje = "Error en la Actualización  "+s.mostrarError();				
			}
		}else  {
			jsonObj.put("success", new Boolean(false));	
			mensaje = "Error en la Actualización  "+s.mostrarError();			
		}
	}else  {
		jsonObj.put("success", new Boolean(true));	
		mensaje = "Error en la Actualización  ";
		
	}	
		
	jsonObj.put("mensaje",mensaje );		
	infoRegresar  = jsonObj.toString();	
	
}

%>
<%=infoRegresar%>


