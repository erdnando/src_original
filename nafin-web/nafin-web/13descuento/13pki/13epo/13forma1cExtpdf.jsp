<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,
	java.math.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*,
	netropology.utilerias.negocio.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%

	JSONObject jsonObj      = new JSONObject();
	String proceso = (request.getAttribute("proceso")==null)?request.getParameter("proceso"):request.getAttribute("proceso").toString(); 
	String strPendiente = (request.getParameter("strPendiente") != null) ? request.getParameter("strPendiente") : "N";
	String strPreNegociable = (request.getParameter("strPreNegociable") != null) ? request.getParameter("strPreNegociable") : "N";
	String usuario = (request.getAttribute("usuario")==null)?request.getParameter("usuario"):request.getAttribute("usuario").toString();
	String hidFechaCarga = (request.getAttribute("hidFechaCarga")==null)?request.getParameter("hidFechaCarga"):request.getAttribute("hidFechaCarga").toString();
	String hidHoraCarga = (request.getAttribute("hidHoraCarga")==null)?request.getParameter("hidHoraCarga"):request.getAttribute("hidHoraCarga").toString();
	 String accion = (request.getParameter("accion") != null) ? request.getParameter("accion") : "";
	 
 	String hidNumAcuse = request.getParameter("acuse");
	Acuse acuse = new Acuse(hidNumAcuse);
	String hidRecibo = request.getParameter("recibo");
	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
	String ses_ic_epo = iNoCliente;
	
	CrearArchivosAcuses  generaAch =  new CrearArchivosAcuses();
	generaAch.setAcuse(hidNumAcuse);
	generaAch.setTipoArchivo("PDF");	
	generaAch.setUsuario(iNoUsuario);
	generaAch.setVersion("PANTALLA");	
	generaAch.setStrDirectorioTemp(strDirectorioTemp);
	
	int existeArch= 	generaAch.existerArcAcuse(); //verifica si existe 
			
try {
	if(existeArch==0)  {
	
	//	Variables locales y 	De despliegue
	String nombrePyme="", numeroDocumento="", fechaDocumento="", fechaVencimiento="", fechaVencimientoPyme="",
		 monto="", referencia="", porcentajeAnticipo="0", montoDescuento="0", clavePyme="", claveMoneda="", campo1="",
		 campo2="", campo3="", campo4="", campo5="", monedaNombre="", tipoFactoraje="", NombreIF="", sNombreBeneficiario="",
		 sPorcentajeBeneficiario="", sMontoBeneficiario="", numeroSIAFF = "", fechaEntrega = "", tipoCompra = "",
		 clavePresupuestaria = "", periodo = "", mandante = "", Factoraje = "", ic_estatus_docto 	= "", duplicado ="";
		 
	Vector vDoctosCargados = new Vector(); 
	int i=0,  iNumMoneda=0, numRegistrosMN = 0, numRegistrosDL = 0,  registros = 0, numCamposAdicionales =0;	

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
	
	boolean 	estaHabilitadoNumeroSIAFF 	= BeanParamDscto.estaHabilitadoNumeroSIAFF(ses_ic_epo);// VERIFICAR SI ESTA HABILITADO EL NUMERO SIAFF --

	//FODEA 050 - VALC - 10/2008	
	// Obtiene el valor de la parametrización que tiene la EPO.
	String 		sPubEPOPEFFlag		= new String(""),	sPubEPOPEFFlagVal						= new String(""),
				sPubEPOPEFDocto1						= new String(""),	sPubEPOPEFDocto1Val					= new String(""),
				sPubEPOPEFDocto2						= new String(""),	sPubEPOPEFDocto2Val					= new String(""),
				sPubEPOPEFDocto3						= new String(""),	sPubEPOPEFDocto3Val					= new String(""),
				sPubEPOPEFDocto4						= new String(""),	sPubEPOPEFDocto4Val					= new String("");

	
	ArrayList alParamEPO = BeanParamDscto.getParamEPO(ses_ic_epo, 1);
	if (alParamEPO!=null) {  
		//estos son los valores que se consultaron
		//este es el orden en el que me van a llegar los datos pues ordene la consulta por asc de nombre del campo
		sPubEPOPEFDocto3						= (alParamEPO.get(15)==null)?"":alParamEPO.get(15).toString();//PUB_EPO_PEF_CLAVE_PRESUPUESTAL
		sPubEPOPEFDocto3Val					= (alParamEPO.get(16)==null)?"":alParamEPO.get(16).toString();
		sPubEPOPEFDocto1						= (alParamEPO.get(17)==null)?"":alParamEPO.get(17).toString();//PUB_EPO_PEF_FECHA_ENTREGA
		sPubEPOPEFDocto1Val					= (alParamEPO.get(18)==null)?"":alParamEPO.get(18).toString();
		sPubEPOPEFFlag							= (alParamEPO.get(19)==null)?"":alParamEPO.get(19).toString();//PUB_EPO_PEF_FECHA_RECEPCION
		sPubEPOPEFFlagVal						= (alParamEPO.get(20)==null)?"":alParamEPO.get(20).toString();
		sPubEPOPEFDocto4						= (alParamEPO.get(21)==null)?"":alParamEPO.get(21).toString();//PUB_EPO_PEF_PERIODO
		sPubEPOPEFDocto4Val					= (alParamEPO.get(22)==null)?"":alParamEPO.get(22).toString();
		sPubEPOPEFDocto2						= (alParamEPO.get(23)==null)?"":alParamEPO.get(23).toString();//PUB_EPO_PEF_TIPO_COMPRA
		sPubEPOPEFDocto2Val					= (alParamEPO.get(24)==null)?"":alParamEPO.get(24).toString();
	}
	
	String fechaVencMin = "", fechaVencMax = "", spubdocto_venc = "",	 fechaPlazoPagoFVP = "",
	 plazoMax1FVP  = "", plazoMax2FVP  = "";
	 
	Vector vFechasVencimiento = CargaDocumentos.getFechasVencimiento(iNoCliente, 1);
	 fechaVencMin = vFechasVencimiento.get(0).toString();
	 fechaVencMax = vFechasVencimiento.get(1).toString();
	 spubdocto_venc = (vFechasVencimiento.get(2)==null)?"":vFechasVencimiento.get(2).toString();
	 fechaPlazoPagoFVP = (vFechasVencimiento.get(3)==null)?"":vFechasVencimiento.get(3).toString();
	 plazoMax1FVP = (vFechasVencimiento.get(4)==null)?"":vFechasVencimiento.get(4).toString();
	 plazoMax2FVP = (vFechasVencimiento.get(5)==null)?"":vFechasVencimiento.get(5).toString();
	 

	CreaArchivo archivo = new CreaArchivo();
	String nombreArchivo = archivo.nombreArchivo()+".pdf";

// Fodea 023  Mandante Parametrización
	boolean bOperaFactConMandato=false;
	boolean bTipoFactoraje=false;	
	boolean bOperaFactorajeVencido = false;
	boolean bOperaFactorajeDistribuido = false ;
	boolean bOperaFactorajeVencidoInfonavit = false;  //Fodea 042-2009 Vencimiento Infonavit
	boolean bValidaDuplicidad = false;  //Fodea 016-2015
	
	Hashtable alParamEPO1 = new Hashtable(); 
	alParamEPO1 = BeanParamDscto.getParametrosEPO(ses_ic_epo,1);	
	if (alParamEPO1!=null) {
		bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		bOperaFactorajeVencido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
		bOperaFactorajeDistribuido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
    bOperaFactorajeVencidoInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;	 		 
		bValidaDuplicidad = ("N".equals(alParamEPO1.get("VALIDA_DUPLIC").toString()))?false:true;
	}
	bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido ||bOperaFactConMandato)?true:false;
	
	Calendar fecha = Calendar.getInstance();
	boolean	hayTitulosEnDosLineas = false;
	
	String aforo = strAforo;
	String aforoDL = strAforoDL;
	
	//Despliegue de los documentos
	
		BigDecimal montoTotalMN = new BigDecimal("0.00");
		BigDecimal montoTotalDL = new BigDecimal("0.00");
		BigDecimal montoDescuentoMN = new BigDecimal("0.00");
		BigDecimal montoDescuentoDL = new BigDecimal("0.00");
		BigDecimal montoTotalMNNeg = new BigDecimal("0.00");
		BigDecimal montoTotalMNSinOpera = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoMNNeg = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoMNSinOpera = new BigDecimal("0.00");
		BigDecimal montoTotalDLNeg = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoDLNeg = new BigDecimal("0.00");
		BigDecimal montoTotalMNNoNeg = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoMNNoNeg = new BigDecimal("0.00");
		BigDecimal montoTotalDLNoNeg = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoDLNoNeg = new BigDecimal("0.00");	
		BigDecimal montoTotalDLSinOper = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoDLSinOper = new BigDecimal("0.00");
		String icEstatusDocumento ="";
	 vDoctosCargados = CargaDocumentos.mostrarDocumentosCargados(proceso, aforo, ses_ic_epo, "", "", aforoDL, sesIdiomaUsuario,"'N','S'");
	for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();) {
		Vector vd 				= (Vector)e.nextElement();
		monto 					= vd.get(6).toString();
		porcentajeAnticipo 		= vd.get(8).toString();
		montoDescuento 			= vd.get(9).toString();
		iNumMoneda = Integer.parseInt(vd.get(5).toString());
		icEstatusDocumento 		= vd.get(23).toString();
		if(montoDescuento.equals("") ) montoDescuento ="0";
		if (iNumMoneda==1) {
			numRegistrosMN++;
			if (monto!=null) {
				montoTotalMN = montoTotalMN.add(new BigDecimal(monto));
				montoDescuentoMN = montoDescuentoMN.add(new BigDecimal(montoDescuento));
				if(icEstatusDocumento.equals("2") || icEstatusDocumento.equals("30")|| icEstatusDocumento.equals("28")){  // Estatus 2 Negociable, Estatus 30 Pendiente Negociable, Estatus 28 Pre Negociable
					montoTotalMNNeg   = montoTotalMNNeg.add(new BigDecimal(monto));
					montoTotalDsctoMNNeg = montoTotalDsctoMNNeg.add(new BigDecimal(montoDescuento));
				} else if(icEstatusDocumento.equals("9")){
					montoTotalMNSinOpera   = montoTotalMNSinOpera.add(new BigDecimal(monto));
					montoTotalDsctoMNSinOpera = montoTotalDsctoMNSinOpera.add(new BigDecimal(montoDescuento));
				} else {
					montoTotalMNNoNeg = montoTotalMNNoNeg.add(new BigDecimal(monto));
					montoTotalDsctoMNNoNeg = montoTotalDsctoMNNoNeg.add(new BigDecimal(montoDescuento));
				}
			}
		} else if (iNumMoneda == 54) {//IC_MONEDA=54
			numRegistrosDL++;
			if (monto!=null) {
				montoTotalDL = montoTotalDL.add(new BigDecimal(monto));
				montoDescuentoDL = montoDescuentoDL.add(new BigDecimal(montoDescuento));
				if (icEstatusDocumento.equals("2") || icEstatusDocumento.equals("30")|| icEstatusDocumento.equals("28")){ //Estatus 2 Negociables, Estatus 30 Pendientes Negociables, Estatus 28 Pre Negociable
					montoTotalDLNeg   = montoTotalDLNeg.add(new BigDecimal(monto));
					montoTotalDsctoDLNeg = montoTotalDsctoDLNeg.add(new BigDecimal(montoDescuento));
				} else if(icEstatusDocumento.equals("9")) {
					montoTotalDLSinOper = montoTotalDLSinOper.add(new BigDecimal(monto));
					montoTotalDsctoDLSinOper = montoTotalDsctoDLSinOper.add(new BigDecimal(montoDescuento));
				} else {
					montoTotalDLNoNeg = montoTotalDLNoNeg.add(new BigDecimal(monto));
					montoTotalDsctoDLNoNeg = montoTotalDsctoDLNoNeg.add(new BigDecimal(montoDescuento));
				}
			}
		}
	}//for 
	
	
	
	try {
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String nombreMesIngles[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		int mes = fecha.get(Calendar.MONTH);

	   pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),  session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"), (String) session.getAttribute("strNombre"), (String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
									
		if (sesIdiomaUsuario.equals("EN")) {
    		pdfDoc.addText("Mexico City  "+ nombreMesIngles[mes]+" "+ diaActual+" "+anioActual+"-----------------"+horaActual,"formas",ComunesPDF.RIGHT);
		} else {
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		}
		
		pdfDoc.addText("La autentificación se llevó a cabo con éxito","formasB",ComunesPDF.CENTER);
		pdfDoc.addText("Recibo"+" "+hidRecibo,"formasB",ComunesPDF.CENTER);

		List titulos = new ArrayList();
		titulos.add("Carga Individual");
		titulos.add("Moneda Nacional");
		titulos.add("Dólares");			
		float widths[] = {2,1,1};
		pdfDoc.setTable(titulos,"formasB",50,widths);
		pdfDoc.setCell("No. total de documentos cargados","celda02");
		pdfDoc.setCell(String.valueOf(numRegistrosMN),"formas",ComunesPDF.CENTER);
		pdfDoc.setCell(String.valueOf(numRegistrosDL),"formas",ComunesPDF.CENTER);
    if(strPendiente.equals("S"))
      pdfDoc.setCell("Monto total de los documentos cargados Pendientes Negociables","celda02");
    else if(strPreNegociable.equals("S"))
      pdfDoc.setCell("Monto total de los documentos cargados Pre Negociables","celda02");
    else
      pdfDoc.setCell("Monto total de los documentos cargados Negociables","celda02");
		pdfDoc.setCell(Comunes.formatoMN(montoTotalMNNeg.toString()),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMN(montoTotalDLNeg.toString()),"formas",ComunesPDF.RIGHT);
		if(strPendiente.equals("S"))
      pdfDoc.setCell("Monto total de los documentos cargados Pendientes NO Negociables","celda02");
    else if(strPreNegociable.equals("S"))
      pdfDoc.setCell("Monto total de los documentos cargados Pre NO Negociables","celda02");
    else
      pdfDoc.setCell("Monto total de los documentos cargados NO Negociables","celda02");
		pdfDoc.setCell(Comunes.formatoMN(montoTotalMNNoNeg.toString()),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMN(montoTotalDLNoNeg.toString()),"formas",ComunesPDF.RIGHT);
		/****/////hay que//*/		
		if("S".equals(spubdocto_venc)){
			pdfDoc.setCell("Monto total de los documentos cargados Vencidos Sin Operar","celda02");
			pdfDoc.setCell(Comunes.formatoMN(montoTotalMNSinOpera.toString()),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoMN(montoTotalDLSinOper.toString()),"formas",ComunesPDF.RIGHT); 
		}
		pdfDoc.setCell("Monto total de los documentos cargados","celda02");
		pdfDoc.setCell(Comunes.formatoMN(montoTotalMN.toString()),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell(Comunes.formatoMN(montoTotalDL.toString()),"formas",ComunesPDF.RIGHT);
		pdfDoc.setCell("No. de acuse","celda03");
		pdfDoc.setCell(acuse.formatear(),"celda03",ComunesPDF.LEFT,2);
		pdfDoc.setCell("Fecha de carga","celda03");
		pdfDoc.setCell(hidFechaCarga,"celda03",ComunesPDF.LEFT,2);
		pdfDoc.setCell("Hora de carga","celda03");
		pdfDoc.setCell(hidHoraCarga,"celda03",ComunesPDF.LEFT,2);
		pdfDoc.setCell("Usuario","celda03");
		pdfDoc.setCell(usuario,"celda03",ComunesPDF.LEFT,2);
		String mensaje ="Al transmitir este MENSAJE DE DATOS, usted está bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesión de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisión tendrá validez para todos los efectos legales.";
		
		if("S".equals(operaNC)) {
			mensaje =" Al transmitir este MENSAJE DE DATOS, usted está bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesión de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisión tendrá validez para todos los efectos legales. En caso de transmitir NOTAS DE CREDITO, usted está en el entendido de que aplicarán a los DOCUMENTOS una vez que a la MIPYME que correspondan decida efectuar el el descuento.";
		}
		//******************
		//Negociables
		//**********************
		
		pdfDoc.setCell(mensaje,"formas",ComunesPDF.LEFT,3);
		pdfDoc.addTable();
   
		int numCols = 10;
				
		if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) // Factoraje Vencido, Mandato, Vencimiento Infonavit
			numCols++;
		if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit) //Factoraje Distribuido, Vencimiento Infonavit
			numCols+=3;		
		Vector vNombres = CargaDocumentos.getNombresCamposAdicionales(ses_ic_epo);			
		numCamposAdicionales=vNombres.size();

		if(numCamposAdicionales>0 || bOperaFactConMandato || estaHabilitadoNumeroSIAFF || sPubEPOPEFFlagVal.equalsIgnoreCase("S") || bValidaDuplicidad)
			hayTitulosEnDosLineas = true;
		if("S".equals(operaFVPyme))
			numCols++;
		if(hayTitulosEnDosLineas)
			numCols++;	//Letra A	
		float widthsDocs[] = new float[numCols];
		for(int d=0;d<numCols;d++) {
			if(d>0)
				widthsDocs[d] = 1;
			else if(d==0 && hayTitulosEnDosLineas)			
				widthsDocs[d] = (float)0.5;
			else
				widthsDocs[d] = 1;
		}//for
		
		if(!hayTitulosEnDosLineas) {
			numCols++; 
		}
		
	 if(strPendiente.equals("S"))
      pdfDoc.addText("Pendientes Negociables","celda04");
    else if(strPreNegociable.equals("S"))
      pdfDoc.addText("Pre Negociables","celda04");
    else
      pdfDoc.addText("Negociables","celda04");
		pdfDoc.setTable(numCols);
		if(hayTitulosEnDosLineas)
			pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Emisión","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
		if("S".equals(operaFVPyme)) {
			pdfDoc.setCell("Fecha Vencimiento Proveedor","celda01",ComunesPDF.CENTER);
		}//if("S".equals(operaFVPyme))
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
		if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) { //Factorraje Vencido, Mandato, Vencimiento Infonavit
			pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
		}
		if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit) { //Factoraje Distribuido, Vencimiento Infonavit
			pdfDoc.setCell("Nombre Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Porcentaje Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Beneficiario","celda01",ComunesPDF.CENTER);
		}	
		if (hayTitulosEnDosLineas) {
			pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
			for(int a=0; a<vNombres.size(); a++) {				
			  pdfDoc.setCell((String)vNombres.get(a),"celda01",ComunesPDF.CENTER);
			}
		}	
		if(hayTitulosEnDosLineas && sPubEPOPEFFlagVal.equalsIgnoreCase("S")){
			pdfDoc.setCell("Fecha de Recepción de Bienes y Servicios","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Compra (procedimiento)","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Clasificador por Objeto del Gasto","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo Máximo ","celda01",ComunesPDF.CENTER);
		}
		if(hayTitulosEnDosLineas && estaHabilitadoNumeroSIAFF) {
			pdfDoc.setCell("Digito Identificador","celda01",ComunesPDF.CENTER);
		}
		
		if (hayTitulosEnDosLineas && bOperaFactConMandato) {			
			pdfDoc.setCell("Mandante","celda01",ComunesPDF.CENTER);			
		}
		
		if (hayTitulosEnDosLineas && bValidaDuplicidad) {
		pdfDoc.setCell("Notificado como posible Duplicado","celda01",ComunesPDF.CENTER);	 //F038-2014
		}
		
		if(hayTitulosEnDosLineas) {			
				int columnas_restantes = numCols-1;	//Letra B
				columnas_restantes = columnas_restantes - numCamposAdicionales;
				if (sPubEPOPEFFlagVal.equalsIgnoreCase("S")) columnas_restantes-= 4;
				if(estaHabilitadoNumeroSIAFF) columnas_restantes--;
				if(bOperaFactConMandato) columnas_restantes--;		
				if(bValidaDuplicidad) columnas_restantes--;
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER,columnas_restantes);				
		}
		
		//pdfDoc.setHeaders();
		//Despliegue de los documentos
    if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
       ic_estatus_docto = "30"; // Pendientes Negociables
    else if(strPreNegociable.equals("S"))   // Si se trata de documentos PreNegociables
       ic_estatus_docto = "28"; // Pendientes Negociables
    else
       ic_estatus_docto = "2"; // Negociables
		 vDoctosCargados.clear();
		 vDoctosCargados = CargaDocumentos.getComDoctosCargadosConIcDocumento(hidNumAcuse, strAforo, ses_ic_epo, ic_estatus_docto, strAforoDL, sesIdiomaUsuario);
		
		
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{
			Vector vd = (Vector)e.nextElement();
			nombrePyme = vd.get(0).toString();
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			monedaNombre = vd.get(4).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10).toString();
			tipoFactoraje = vd.get(7).toString();
			mandante = vd.get(27)==null?"":vd.get(27).toString();
			Factoraje = vd.get(28)==null?"":vd.get(28).toString();
			clavePresupuestaria = vd.get(25).toString();
		   duplicado = vd.get(29).toString(); //F038-2014

			
			periodo = vd.get(26).toString();
			if(montoDescuento.equals("") ) montoDescuento ="0";
			NombreIF = "";
			if(bOperaFactorajeVencido ) { // Para factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF = vd.get(11).toString();
				}
			}			
			//Fodea 023 2009 Mandato
			if( hayTitulosEnDosLineas && bOperaFactConMandato && tipoFactoraje.equals("M"))  {
					NombreIF = vd.get(11).toString();
			}			
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(17).toString();
					sPorcentajeBeneficiario = vd.get(18).toString();
					sMontoBeneficiario = vd.get(19).toString();
				}
			}			
			//Fodea 042-2009 Vencimiento Infonavit
			if( bOperaFactorajeVencidoInfonavit && tipoFactoraje.equals("I"))  {
					NombreIF = vd.get(11).toString();
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(17).toString();
					sPorcentajeBeneficiario = vd.get(18).toString();
					sMontoBeneficiario = vd.get(19).toString();
			}
			//FODEA 050 - VALC - 10/2008			
			fechaEntrega			= vd.get(23).toString();
			tipoCompra = "";
			if(vd.get(24).toString().equals("L"))		tipoCompra = "Licitación"; 
			if(vd.get(24).toString().equals("I")) 	tipoCompra = "Invitación";
			if(vd.get(24).toString().equals("D")) 	tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(24).toString().equals("C"))   tipoCompra = "CAAS, CAAS delegado";
					
			if(tipoFactoraje.equals("N"))  tipoFactoraje = "Normal";
			else if (tipoFactoraje.equals("M")) tipoFactoraje = "Mandato"; 				
			else if(tipoFactoraje.equals("V"))  tipoFactoraje = "Vencido";
			else if(tipoFactoraje.equals("D")) 	tipoFactoraje = "Distribuido";
			else if(tipoFactoraje.equals("C")) {
				tipoFactoraje = "Nota de Crédito";
				porcentajeAnticipo = "100";
				montoDescuento = monto;
			}
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
		
			
			if(hayTitulosEnDosLineas && estaHabilitadoNumeroSIAFF){
				numeroSIAFF = getNumeroSIAFF(vd.get(22).toString(),vd.get(21).toString());
			}			
			String clase = "formas";
			if(registros%5==4)
				clase = "celda02";
			else
				clase = "formas";
			if(hayTitulosEnDosLineas)
				pdfDoc.setCell("A",clase,ComunesPDF.CENTER);
			pdfDoc.setCell(nombrePyme,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(numeroDocumento,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(fechaDocumento,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(fechaVencimiento,clase,ComunesPDF.CENTER);
			if("S".equals(operaFVPyme)) {
				pdfDoc.setCell(fechaVencimiento,clase,ComunesPDF.CENTER);
			}//if("S".equals(operaFVPyme))
			pdfDoc.setCell(monedaNombre,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(Factoraje,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMN(monto),clase,ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoDecimal(porcentajeAnticipo,0)+" %",clase,ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMN(montoDescuento),clase,ComunesPDF.RIGHT);
			pdfDoc.setCell(referencia,clase,ComunesPDF.CENTER);
			if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) { //Factoraje Vencido, Mandato, Vencimiento Infonavit
				pdfDoc.setCell(NombreIF,clase,ComunesPDF.CENTER);
			}
			if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit) { //Factoraje Distriuido, Vencimiento Infonavit
				pdfDoc.setCell(sNombreBeneficiario,clase,ComunesPDF.CENTER);
				pdfDoc.setCell((sPorcentajeBeneficiario.equals("")?sPorcentajeBeneficiario:Comunes.formatoDecimal(sPorcentajeBeneficiario,2)+" %"),clase,ComunesPDF.CENTER);
				pdfDoc.setCell((sMontoBeneficiario.equals("")?sMontoBeneficiario:Comunes.formatoMN(sMontoBeneficiario)),clase,ComunesPDF.CENTER);
			}
			
			if (hayTitulosEnDosLineas) {
				pdfDoc.setCell("B",clase,ComunesPDF.CENTER);	
				for (i=1;i<=numCamposAdicionales;i++){				
					//if(i==1)
					pdfDoc.setCell((String)vd.get(11+i),clase,ComunesPDF.CENTER);
				}
			}					
			//FODEA 050 - VALC - 10/2008
			if( hayTitulosEnDosLineas && sPubEPOPEFFlagVal.equalsIgnoreCase("S")){
				pdfDoc.setCell(fechaEntrega,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(tipoCompra,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(clavePresupuestaria,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(periodo,clase,ComunesPDF.CENTER);
			}
			if( hayTitulosEnDosLineas && estaHabilitadoNumeroSIAFF){
				pdfDoc.setCell(numeroSIAFF,clase,ComunesPDF.CENTER);
			}
			
			if (hayTitulosEnDosLineas && bOperaFactConMandato) {
				pdfDoc.setCell(mandante,clase,ComunesPDF.CENTER);
			}
			
			if (hayTitulosEnDosLineas && bValidaDuplicidad) {
			pdfDoc.setCell(duplicado,clase,ComunesPDF.CENTER); //F038-2014
			}
			
			if(hayTitulosEnDosLineas) {
				int columnas_restantes = numCols-1;	//Letra B
				columnas_restantes = columnas_restantes - numCamposAdicionales;
				if (sPubEPOPEFFlagVal.equalsIgnoreCase("S"))  columnas_restantes -= 4;
				if(estaHabilitadoNumeroSIAFF) columnas_restantes--;
				if(bOperaFactConMandato) columnas_restantes--;
				if(bValidaDuplicidad) columnas_restantes--;
				pdfDoc.setCell("",clase,ComunesPDF.CENTER,columnas_restantes);
			}
			registros++;
		}//fin for
		pdfDoc.addTable();
		
		//**************************
		//INICIO NO NEGOCIABLES
		//***************************
		if(strPendiente.equals("S"))
      pdfDoc.addText("Pendientes No Negociables","celda04");
		 else if(strPreNegociable.equals("S"))
			pdfDoc.addText("Pre No Negociables","celda04");
		 else
      pdfDoc.addText("No Negociables","celda04");
		pdfDoc.setTable(numCols);
		if(hayTitulosEnDosLineas)
			pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER);		
		pdfDoc.setCell("Número de Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Emisión","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
		if("S".equals(operaFVPyme)) {
			pdfDoc.setCell("Fecha Vencimiento Proveedor","celda01",ComunesPDF.CENTER);
		}//if("S".equals(operaFVPyme))
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
		if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) { //Factoraje Vencido, Mandato, Vencimiento Infonavit
			pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
		}
		if(bOperaFactorajeDistribuido  || bOperaFactorajeVencidoInfonavit) { //Factoraje Distribuido, Vencimiento Infonavit
			pdfDoc.setCell("Nombre Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Porcentaje Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Beneficiario","celda01",ComunesPDF.CENTER);
		}
		if (hayTitulosEnDosLineas) {
			pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
			for(int a=0; a<vNombres.size(); a++) {
				//if(a==0)					
				pdfDoc.setCell((String)vNombres.get(a),"celda01",ComunesPDF.CENTER);
			}
		}
		if(hayTitulosEnDosLineas && sPubEPOPEFFlagVal.equalsIgnoreCase("S")){
			pdfDoc.setCell("Fecha de Recepción de Bienes y Servicios","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Compra (procedimiento)","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Clasificador por Objeto del Gasto","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo Máximo ","celda01",ComunesPDF.CENTER);
		}
		if(hayTitulosEnDosLineas && estaHabilitadoNumeroSIAFF) {
			pdfDoc.setCell("Digito Identificador","celda01",ComunesPDF.CENTER);
		}	
		if (hayTitulosEnDosLineas && bOperaFactConMandato) {
			pdfDoc.setCell("Mandante","celda01",ComunesPDF.CENTER);
		}
		if (hayTitulosEnDosLineas && bValidaDuplicidad) {
		pdfDoc.setCell("Notificado como posible Duplicado","celda01",ComunesPDF.CENTER); //F038-2014
		}
		
		if(hayTitulosEnDosLineas) {			
				int columnas_restantes = numCols-1;	//Letra B
				columnas_restantes = columnas_restantes - numCamposAdicionales;
				if (sPubEPOPEFFlagVal.equalsIgnoreCase("S")) columnas_restantes-= 4;
				if(estaHabilitadoNumeroSIAFF) columnas_restantes--;
				if(bOperaFactConMandato) columnas_restantes--;				
				if(bValidaDuplicidad) columnas_restantes--;
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER,columnas_restantes);				
		}		
		//pdfDoc.setHeaders();
		//Despliegue de los documentos
		vDoctosCargados.clear();
    if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
      ic_estatus_docto = "31"; // Pendientes Negociables
    else if(strPreNegociable.equals("S"))   // Si se trata de documentos PreNegociable
      ic_estatus_docto = "29"; // Pendientes Negociables
    else
      ic_estatus_docto = "1"; // Negociables
		vDoctosCargados = CargaDocumentos.getComDoctosCargadosConIcDocumento(hidNumAcuse, strAforo, ses_ic_epo, ic_estatus_docto, strAforoDL, sesIdiomaUsuario);
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{
			registros = 0;
			Vector vd = (Vector)e.nextElement();
			nombrePyme = vd.get(0).toString();
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			monedaNombre = vd.get(4).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10).toString();
			tipoFactoraje = vd.get(7).toString();
			mandante = vd.get(27)==null?"":vd.get(27).toString();
			Factoraje = vd.get(28)==null?"":vd.get(28).toString();
			clavePresupuestaria = vd.get(25).toString();
			periodo = vd.get(26).toString();
			duplicado = vd.get(29).toString(); //F038-2014
			
			if(montoDescuento.equals("") ) montoDescuento ="0";
			NombreIF = "";
			if(bOperaFactorajeVencido) { // Para factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF = vd.get(11).toString();
				}
			}
			//Fodea 023 2009 Mandato
			if( bOperaFactConMandato && tipoFactoraje.equals("M"))  {
					NombreIF = vd.get(11).toString();
			}			
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(17).toString();
					sPorcentajeBeneficiario = vd.get(18).toString();
					sMontoBeneficiario = vd.get(19).toString();
				}
			}
			//FODEA 050 - VALC - 10/2008			
			fechaEntrega			= vd.get(23).toString();
			if(vd.get(24).toString().equals("L")) 	tipoCompra = "Licitación";
			if(vd.get(24).toString().equals("I")) 	tipoCompra = "Invitación";
			if(vd.get(24).toString().equals("D")) 	tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(24).toString().equals("C")) 	tipoCompra = "CAAS, CAAS delegado";
			
			
			tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": 
			(tipoFactoraje.equals("M"))?"Mandato": 
			(tipoFactoraje.equals("C"))?"Nota Credito":
			(tipoFactoraje.equals("V"))?"Vencido": 
			(tipoFactoraje.equals("D"))?"Distribuido" : "";
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			
			if(hayTitulosEnDosLineas && estaHabilitadoNumeroSIAFF){
				numeroSIAFF = getNumeroSIAFF(vd.get(22).toString(),vd.get(21).toString());
			}
			
			String clase = "formas";
			if(registros%5==4)
				clase = "celda02";
			else
				clase = "formas";
			if(hayTitulosEnDosLineas)
				pdfDoc.setCell("A",clase,ComunesPDF.CENTER);
			pdfDoc.setCell(nombrePyme,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(numeroDocumento,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(fechaDocumento,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(fechaVencimiento,clase,ComunesPDF.CENTER);
			if("S".equals(operaFVPyme)) {
				pdfDoc.setCell(fechaVencimiento,clase,ComunesPDF.CENTER);
			}//if("S".equals(operaFVPyme))
			pdfDoc.setCell(monedaNombre,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(Factoraje,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMN(monto),clase,ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoDecimal(porcentajeAnticipo,0)+" %",clase,ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMN(montoDescuento),clase,ComunesPDF.RIGHT);
			pdfDoc.setCell(referencia,clase,ComunesPDF.CENTER);
			if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) {  //Factoraje, Vencido, Mandato, Vencimiento Infonavit
				pdfDoc.setCell(NombreIF,clase,ComunesPDF.CENTER);
			}
			if(bOperaFactorajeDistribuido  || bOperaFactorajeVencidoInfonavit) { //Factoraje Distribuido, Vencimiento Infonavit
				pdfDoc.setCell(sNombreBeneficiario,clase,ComunesPDF.CENTER);
				pdfDoc.setCell((sPorcentajeBeneficiario.equals("")?sPorcentajeBeneficiario:Comunes.formatoDecimal(sPorcentajeBeneficiario,2)+" %"),clase,ComunesPDF.CENTER);
				pdfDoc.setCell((sMontoBeneficiario.equals("")?sMontoBeneficiario:Comunes.formatoMN(sMontoBeneficiario)),clase,ComunesPDF.CENTER);
			}			
			if( hayTitulosEnDosLineas) {
				pdfDoc.setCell("B",clase,ComunesPDF.CENTER);
				for (i=1;i<=numCamposAdicionales;i++){
					//if(i==1)						
					pdfDoc.setCell((String)vd.get(11+i),clase,ComunesPDF.CENTER);
				}
			}			
			//FODEA 050 - VALC - 10/2008
			if( hayTitulosEnDosLineas && sPubEPOPEFFlagVal.equalsIgnoreCase("S")){
				pdfDoc.setCell(fechaEntrega,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(tipoCompra,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(clavePresupuestaria,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(periodo,clase,ComunesPDF.CENTER);
			}
			
			if(hayTitulosEnDosLineas && estaHabilitadoNumeroSIAFF){
				pdfDoc.setCell(numeroSIAFF,clase,ComunesPDF.CENTER);
			}
			
			if (hayTitulosEnDosLineas && bOperaFactConMandato) {
			pdfDoc.setCell(mandante,clase,ComunesPDF.CENTER);
			}
			
			if (hayTitulosEnDosLineas && bValidaDuplicidad) {
			pdfDoc.setCell(duplicado,clase,ComunesPDF.CENTER); //F038-2014
			}
			
			if(hayTitulosEnDosLineas) {
				int columnas_restantes = numCols-1;	//Letra B
				columnas_restantes = columnas_restantes - numCamposAdicionales;
				if (sPubEPOPEFFlagVal.equalsIgnoreCase("S"))  columnas_restantes -= 4;
				if(estaHabilitadoNumeroSIAFF) columnas_restantes--;
				if(bOperaFactConMandato) columnas_restantes--;
				if(bValidaDuplicidad) columnas_restantes--;
				pdfDoc.setCell("",clase,ComunesPDF.CENTER,columnas_restantes);
			}
			
			registros++;
		}//fin for
		
			
	if("S".equals(spubdocto_venc)){	
	 pdfDoc.addTable();
	//*****************************
	// INICIO VENCIDO SIN OPERAR
	//**********************************
		pdfDoc.addText("Vencidos Sin Operar","celda04");
		pdfDoc.setTable(numCols);
		if(hayTitulosEnDosLineas)
			pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Número de Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Emisión","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
		if("S".equals(operaFVPyme)) {
			pdfDoc.setCell("Fecha Vencimiento Proveedor","celda01",ComunesPDF.CENTER);
		}//if("S".equals(operaFVPyme))
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
		if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) { //Factoraje, Vencido, Mandato, Vencimiento Infonavit
			pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
		}
		if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit) {//Factoraje Distribuido,  Vencimiento Infonavit
			pdfDoc.setCell("Nombre Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Porcentaje Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Beneficiario","celda01",ComunesPDF.CENTER);
		}		
		if( hayTitulosEnDosLineas) {
			pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
			for(int a=0; a<vNombres.size(); a++) {
				//if(a==0)				
				pdfDoc.setCell((String)vNombres.get(a),"celda01",ComunesPDF.CENTER);
			}
		}
		//FODEA 050 - VALC - 10/2008
		if( hayTitulosEnDosLineas && sPubEPOPEFFlagVal.equalsIgnoreCase("S")){
			pdfDoc.setCell("Fecha de Recepción de Bienes y Servicios","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Compra (procedimiento)","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Clasificador por Objeto del Gasto","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo Máximo ","celda01",ComunesPDF.CENTER);
		}		
		if(hayTitulosEnDosLineas && estaHabilitadoNumeroSIAFF) {
			pdfDoc.setCell("Digito Identificador","celda01",ComunesPDF.CENTER);
		}	
		
		if (hayTitulosEnDosLineas && bOperaFactConMandato) {		
		pdfDoc.setCell("Mandante","celda01",ComunesPDF.CENTER);		
		}
	
		if (hayTitulosEnDosLineas && bValidaDuplicidad) {
		pdfDoc.setCell("Notificado como posible Duplicado","celda01",ComunesPDF.CENTER);	 //F038-2014
		}
		
		
		if(hayTitulosEnDosLineas) {			
				int columnas_restantes = numCols-1;	//Letra B
				columnas_restantes = columnas_restantes - numCamposAdicionales;
				if (sPubEPOPEFFlagVal.equalsIgnoreCase("S")) columnas_restantes-= 4;
				if(estaHabilitadoNumeroSIAFF) columnas_restantes--;
				if(bOperaFactConMandato) columnas_restantes--;				
				if(bValidaDuplicidad) columnas_restantes--;
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER,columnas_restantes);				
		}
			
		//pdfDoc.setHeaders();
		//Despliegue de los documentos
		
		vDoctosCargados.clear(); /*9 DOC VENCIDOS SIN OPERAR*/
		vDoctosCargados = CargaDocumentos.getComDoctosCargadosConIcDocumento(hidNumAcuse, strAforo, ses_ic_epo, "9", strAforoDL, sesIdiomaUsuario);
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{		
			registros = 0;
			Vector vd = (Vector)e.nextElement();
			nombrePyme = vd.get(0).toString();
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			monedaNombre = vd.get(4).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10).toString();
			tipoFactoraje = vd.get(7).toString();
			mandante = vd.get(27)==null?"":vd.get(27).toString();
			Factoraje = vd.get(28)==null?"":vd.get(28).toString();
			clavePresupuestaria = vd.get(25).toString();
			periodo = vd.get(26).toString();
			duplicado = vd.get(29).toString(); //F038-2014
			
			if(montoDescuento.equals("") ) montoDescuento ="0";
			NombreIF = "";
			if(bOperaFactorajeVencido ) { // Para factoraje Vencido
				if(tipoFactoraje.equals("V") ) {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF = vd.get(11).toString();
				}
			}
				//Fodea 023 2009 Mandato
			if(bOperaFactConMandato && tipoFactoraje.equals("M"))  {
					NombreIF = vd.get(11).toString();
			}			
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(17).toString();
					sPorcentajeBeneficiario = vd.get(18).toString();
					sMontoBeneficiario = vd.get(19).toString();
				}
			}			
				//Fodea 042-2009 Vencimiento Infonavit
			if(bOperaFactorajeVencidoInfonavit && tipoFactoraje.equals("I"))  {
					NombreIF = vd.get(11).toString();
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(17).toString();
					sPorcentajeBeneficiario = vd.get(18).toString();
					sMontoBeneficiario = vd.get(19).toString();		
			}			
			//FODEA 050 - VALC - 10/2008			
			fechaEntrega			= vd.get(23).toString();
			tipoCompra = "";
			if(vd.get(24).toString().equals("L")) 	tipoCompra = "Licitación";
			if(vd.get(24).toString().equals("I")) 	tipoCompra = "Invitación";
			if(vd.get(24).toString().equals("D")) 	tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(24).toString().equals("C")) 	tipoCompra = "CAAS, CAAS delegado";
			
			
			tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": 
			(tipoFactoraje.equals("M"))?"Mandato": 
			(tipoFactoraje.equals("C"))?"Nota Credito":
			(tipoFactoraje.equals("V"))?"Vencido":
			(tipoFactoraje.equals("D"))?"Distribuido": "";
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			
			if (hayTitulosEnDosLineas  && estaHabilitadoNumeroSIAFF){
				numeroSIAFF = getNumeroSIAFF(vd.get(22).toString(),vd.get(21).toString());
			}
			
					
			String clase = "formas";
			if(registros%5==4)
				clase = "celda02";
			else
				clase = "formas";
			if(hayTitulosEnDosLineas)
				pdfDoc.setCell("A",clase,ComunesPDF.CENTER);
			pdfDoc.setCell(nombrePyme,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(numeroDocumento,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(fechaDocumento,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(fechaVencimiento,clase,ComunesPDF.CENTER);
			if("S".equals(operaFVPyme)) {
				pdfDoc.setCell(fechaVencimiento,clase,ComunesPDF.CENTER);
			}//if("S".equals(operaFVPyme))
			pdfDoc.setCell(monedaNombre,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(Factoraje,clase,ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMN(monto),clase,ComunesPDF.RIGHT);
			pdfDoc.setCell(Comunes.formatoDecimal(porcentajeAnticipo,0)+" %",clase,ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoMN(montoDescuento),clase,ComunesPDF.RIGHT);
			pdfDoc.setCell(referencia,clase,ComunesPDF.CENTER);
			if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) { //Factoraje Vencido, Mandato, Vencimiento Infonavit
				pdfDoc.setCell(NombreIF,clase,ComunesPDF.CENTER);
			}
			if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit ) { //Factoraje Distribuido, Vencimiento Infonavit
				pdfDoc.setCell(sNombreBeneficiario,clase,ComunesPDF.CENTER);
				pdfDoc.setCell((sPorcentajeBeneficiario.equals("")?sPorcentajeBeneficiario:Comunes.formatoDecimal(sPorcentajeBeneficiario,2)+" %"),clase,ComunesPDF.CENTER);
				pdfDoc.setCell((sMontoBeneficiario.equals("")?sMontoBeneficiario:Comunes.formatoMN(sMontoBeneficiario)),clase,ComunesPDF.CENTER);
			}			
			if( hayTitulosEnDosLineas) {
				pdfDoc.setCell("B",clase,ComunesPDF.CENTER);
				for (i=1;i<=numCamposAdicionales;i++){
					//if(i==1)						
					pdfDoc.setCell((String)vd.get(11+i),clase,ComunesPDF.CENTER);
				}
			}			
			//FODEA 050 - VALC - 10/2008
			if (hayTitulosEnDosLineas && sPubEPOPEFFlagVal.equalsIgnoreCase("S")){
				pdfDoc.setCell(fechaEntrega,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(tipoCompra,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(clavePresupuestaria,clase,ComunesPDF.CENTER);
				pdfDoc.setCell(periodo,clase,ComunesPDF.CENTER);
			}			
			if(hayTitulosEnDosLineas && estaHabilitadoNumeroSIAFF){
				pdfDoc.setCell(numeroSIAFF,clase,ComunesPDF.CENTER);
			}				
			if (bOperaFactConMandato) {
			pdfDoc.setCell(mandante,clase,ComunesPDF.CENTER);
			}	
			
			if (hayTitulosEnDosLineas && bValidaDuplicidad) {
			pdfDoc.setCell(duplicado,clase,ComunesPDF.CENTER); //F038-2014
			}
			
			if(hayTitulosEnDosLineas ) {				
				int columnas_restantes = numCols-1;	//Letra B
				columnas_restantes = columnas_restantes - numCamposAdicionales;
				if (sPubEPOPEFFlagVal.equalsIgnoreCase("S"))  columnas_restantes -= 4;
				if(estaHabilitadoNumeroSIAFF) columnas_restantes--;
				if(bOperaFactConMandato) columnas_restantes--;
				if(bValidaDuplicidad) columnas_restantes--;
				pdfDoc.setCell("",clase,ComunesPDF.CENTER,columnas_restantes);
			}			
			registros++;
		}//fin for
		
		
		}//"S".equals(spubdocto_venc)
		
		int countAD=2;	
		if(hayTitulosEnDosLineas) {
			countAD++;	
		}
		
		int iColSpan = 4;
		if("S".equals(operaFVPyme)) {
			iColSpan++;
		}
		
		if("S".equals(spubdocto_venc)){
			iColSpan++;
		}
		
		if(bOperaFactorajeDistribuido || bOperaFactorajeVencidoInfonavit ) {  //Factoraje Vencido, Mandato, Vencimiento Infonavit
		iColSpan = iColSpan+3;
		}
		
		if(bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencidoInfonavit) //Factoraje Distribuido,  Vencimiento Infonavit
			iColSpan++;
		
		//if (bValidaDuplicidad)
		//	iColSpan++;
		
		if(!hayTitulosEnDosLineas) {
			countAD++; 
		}
		
		int resta = 0;
		resta = numCols - (iColSpan+(countAD-1+3));	
		
			
		if (numRegistrosMN!=0) {
      if(strPendiente.equals("S"))
        pdfDoc.setCell("Total Moneda Nacionales Pendientes Negociables","formasB",ComunesPDF.RIGHT,iColSpan);
      else if(strPreNegociable.equals("S"))
        pdfDoc.setCell("Total Moneda Nacional Pre Negociables","formasB",ComunesPDF.RIGHT,iColSpan);
      else
        pdfDoc.setCell("Total Moneda Nacional Negociables","formasB",ComunesPDF.RIGHT,iColSpan);
			pdfDoc.setCell("","celda01",ComunesPDF.CENTER,countAD-1);
			pdfDoc.setCell(Comunes.formatoMN(montoTotalMNNeg.toString()),"celda01",ComunesPDF.RIGHT);
			pdfDoc.setCell("","celda01");
			pdfDoc.setCell(Comunes.formatoMN(montoTotalDsctoMNNeg.toString()),"celda01",ComunesPDF.RIGHT);
			pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,resta);
			
			if(strPendiente.equals("S"))
        pdfDoc.setCell("Total Moneda Nacional Pendientes no Negociables","formasB",ComunesPDF.RIGHT,iColSpan);
      else if(strPreNegociable.equals("S"))
        pdfDoc.setCell("Total Moneda Nacional Pre no Negociables ","formasB",ComunesPDF.RIGHT,iColSpan);
      else
        pdfDoc.setCell("Total Moneda Nacional no Negociables","formasB",ComunesPDF.RIGHT,iColSpan);
			pdfDoc.setCell("","celda01",ComunesPDF.CENTER,countAD-1);
			pdfDoc.setCell(Comunes.formatoMN(montoTotalMNNoNeg.toString()),"celda01",ComunesPDF.RIGHT);
			pdfDoc.setCell("","celda01");
			pdfDoc.setCell(Comunes.formatoMN(montoTotalDsctoMNNoNeg.toString()),"celda01",ComunesPDF.RIGHT);
			pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,resta);
			if("S".equals(spubdocto_venc)){
				pdfDoc.setCell("Total Moneda Nacional Vencida sin Operar","formasB",ComunesPDF.RIGHT,iColSpan);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER,countAD-1);
				pdfDoc.setCell(Comunes.formatoMN(montoTotalMNSinOpera.toString()),"celda01",ComunesPDF.RIGHT);
				pdfDoc.setCell("","celda01");
				pdfDoc.setCell(Comunes.formatoMN(montoTotalDsctoMNSinOpera.toString()),"celda01",ComunesPDF.RIGHT);
				pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,resta);				
			}//			
		}
		
		if (numRegistrosDL!=0) {
      if(strPendiente.equals("S"))
        pdfDoc.setCell("Total Dólares Pendientes Negociables","formasB",ComunesPDF.RIGHT,iColSpan);
      else if(strPreNegociable.equals("S"))
        pdfDoc.setCell("Total Dólares Pre Negociables ","formasB",ComunesPDF.RIGHT,iColSpan);
      else
        pdfDoc.setCell("Total Dólares  Negociables ","formasB",ComunesPDF.RIGHT,iColSpan);
			pdfDoc.setCell("","celda01",ComunesPDF.CENTER,countAD-1);
			pdfDoc.setCell(Comunes.formatoMN(montoTotalDLNeg.toString()),"celda01",ComunesPDF.RIGHT);
			pdfDoc.setCell("","celda01");
			pdfDoc.setCell(Comunes.formatoMN(montoTotalDsctoDLNeg.toString()),"celda01",ComunesPDF.RIGHT);
			pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,resta);
			///TENGO UNA DUDA AQUI
      if(strPendiente.equals("S"))
        pdfDoc.setCell("Total Dólares  Pendientes no Negociabes ","formasB",ComunesPDF.RIGHT,iColSpan);
      else if(strPreNegociable.equals("S"))
        pdfDoc.setCell("Total Dólares  Pre no Negociables","formasB",ComunesPDF.RIGHT,iColSpan);
      else
        pdfDoc.setCell("Total Dólares  no Negociables","formasB",ComunesPDF.RIGHT,iColSpan);
			pdfDoc.setCell("","celda01",ComunesPDF.CENTER,countAD-1);
			pdfDoc.setCell(Comunes.formatoMN(montoTotalDLNoNeg.toString()),"celda01",ComunesPDF.RIGHT);
			pdfDoc.setCell("","celda01");
			pdfDoc.setCell(Comunes.formatoMN(montoTotalDsctoDLNeg.toString()),"celda01",ComunesPDF.RIGHT);
			pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,resta);
			if("S".equals(spubdocto_venc)){
				pdfDoc.setCell("Total Dólares Vencidos sin Operar","formasB",ComunesPDF.RIGHT,iColSpan);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER,countAD-1);
				pdfDoc.setCell(Comunes.formatoMN(montoTotalDLSinOper.toString()),"celda01",ComunesPDF.RIGHT);
				pdfDoc.setCell("","celda01");
				pdfDoc.setCell(Comunes.formatoMN(montoTotalDsctoDLSinOper.toString()),"celda01",ComunesPDF.RIGHT);
				pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,resta);	
			}	
		}
		if (numRegistrosMN!=0) {
			pdfDoc.setCell("Total Documentos en Moneda Nacional ","formasB",ComunesPDF.RIGHT,iColSpan);
			pdfDoc.setCell(new Integer(numRegistrosMN).toString(),"formasB",ComunesPDF.RIGHT,3);
			pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,resta+2);
			//pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,countAD);
			//pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,numCols-9);					
		}
		
		
		if (numRegistrosDL!=0) {
			pdfDoc.setCell("Total Documentos en Dólares ","formasB",ComunesPDF.RIGHT,iColSpan);
			pdfDoc.setCell(new Integer(numRegistrosDL).toString(),"formasB",ComunesPDF.RIGHT,3);			
			pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,resta+2);
			//pdfDoc.setCell("","celda01",ComunesPDF.RIGHT,numCols-9);
		}
		if (numRegistrosMN!=0 || numRegistrosDL!=0) {
			pdfDoc.addTable();
		}
		pdfDoc.endDocument();
				
	
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	generaAch.setRutaArchivo(strDirectorioTemp+nombreArchivo);
	
	generaAch.guardarArchivo() ; //Guardar	
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("accion",accion);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	
}else 	if(existeArch>0)  {


	String nombreArchivo = generaAch.desArchAcuse() ; //Descargar	

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("accion",accion);
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

}

} catch (Exception exception) {
		
		java.io.StringWriter outSW = new java.io.StringWriter();
		exception.printStackTrace(new java.io.PrintWriter(outSW));
		String stackTrace = outSW.toString();
			
		generaAch.setDesError(stackTrace);	
		
		generaAch.guardaBitacoraArch();	
	
	//throw new NafinException("SIST0001");
	
} 

%>

<%=jsonObj%>

<%!
	public String getNumeroSIAFF(String ic_epo,String ic_documento){
		return getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(ic_documento);		
	}
	
	public String getNumeroDeCuatroDigitos(String numero){

		String 			claveEPO 	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();
		
      for(int i = 0;i<(4-claveEPO.length());i++){
			resultado.append("0");
      }
      resultado.append(claveEPO);

      return resultado.toString();
  }
  
  	public String getNumeroDeOnceDigitos(String numero){

		String 			claveDocto	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();
		
      for(int i = 0;i<(11-claveDocto.length());i++){
			resultado.append("0");
      }
      resultado.append(claveDocto);

      return resultado.toString();
  }
%>
