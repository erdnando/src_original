
Ext.onReady(function() {
	
	var cancelacion  = Ext.getDom('cancelacion').value;
	var strPendiente =  Ext.getDom('strPendiente').value;
	var strPreNegociable =  Ext.getDom('strPreNegociable').value;
	var operaNC =  Ext.getDom('operaNC').value;	
	var proceso =  Ext.getDom('proceso').value;
	var hayErrores  =  Ext.getDom('hayErrores').value;
	var bVenSinOperarS =  Ext.getDom('bVenSinOperarS').value;
	var acuse  = Ext.getDom('acuse').value;	
	var usuario  = Ext.getDom('usuario').value;	
	var hidFechaCarga  = Ext.getDom('hidFechaCarga').value;	
	var hidHoraCarga  = Ext.getDom('hidHoraCarga').value;	
	var acuseFormateado  =  Ext.getDom('acuseFormateado').value;	
	
	var txttotdoc =  Ext.getDom('txttotdoc').value;
	var txtmtodoc =  Ext.getDom('txtmtodoc').value;
	var txtmtomindoc =  Ext.getDom('txtmtomindoc').value;
	var txtmtomaxdoc =  Ext.getDom('txtmtomaxdoc').value;
	
	var txttotdocdo =  Ext.getDom('txttotdocdo').value;
	var txtmtodocdo =  Ext.getDom('txtmtodocdo').value;
	var txtmtomindocdo =  Ext.getDom('txtmtomindocdo').value;
	var txtmtomaxdocdo =  Ext.getDom('txtmtomaxdocdo').value;
	
	var mensajeC = Ext.getDom('mensaje').value;
	var mensaje;
	var autentificacion;
	var leyenda; 
	var autentificacionAcuse;
	var doctoDuplicados =  Ext.getDom('doctoDuplicados').value; 
		
			//para mostrar el Acuse 
	var storeCifrasData = new Ext.data.ArrayStore({
		  fields: [
			  {name: 'etiqueta'},
			  {name: 'informacion'}
		  ]
	 });
	 
	 	//Grid del Acuse 
	var gridCifrasControl = new Ext.grid.GridPanel({
		id: 'gridCifrasControl',
		store: storeCifrasData,
		margins: '20 0 0 0',		
		hidden: true,	
		align: 'center',
		columns: [
			{
				header : '',
				dataIndex : 'etiqueta',
				width : 200,
				sortable : true,
				align: 'left'	
			},
			{
				header : '',			
				dataIndex : 'informacion',
				width : 450,
				align: 'left',
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 655,
		style: 'margin:0 auto;',
		autoHeight : true,
		frame: true
	});

	if(strPendiente=='S'){
		mensaje = 	"Transmitir documentos pendientes negociables";
	}else if(strPreNegociable =='S'){
		mensaje ="Transmitir documentos Pre negociables ";
	}else  {
		mensaje ="Transmitir documentos negociables";	
	}
	
	
	if (operaNC=='S' && strPreNegociable!='S'  && strPendiente !='S') {
		 leyenda = '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
		'<tr><td class="formas" style="text-align: justify;" colspan="3">Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesi�n de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales. En caso de transmitir NOTAS DE CREDITO, usted est� en el entendido de que aplicar�n a los DOCUMENTOS una vez que a la MIPYME '+
		'que correspondan decida efectuar el descuento.	</td></tr></table>';
		
	}else {
		leyenda = '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
		'<tr><td class="formas" style="text-align: justify;" colspan="3">'+
		'Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesi�n de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales. En caso de transmitir NOTAS DE CREDITO, usted est� en el entendido de que aplicar�n a los DOCUMENTOS una vez que a la MIPYME que correspondan decida efectuar el descuento. '+
		'</table>';			
	}
	var leyendaLegal = new Ext.Container({
		layout: 'table',
		id: 'leyendaLegal',		
		layoutConfig: {
			columns: 20
		},
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		leyenda, 
				cls:		'x-form-item', 
				style: { 
					width:   		'700%', 
					textAlign: 		'left'
				} 
			}			
		]
	});
	
	
	if(cancelacion=='S') {
		 autentificacion = '<table width="400" align="center">'+
		'<tr><td><H1>Operaci�n Cancelada</H1></td></tr></table>';
	}
	if(cancelacion=='N') {
	 autentificacion = '<table width="400" align="center">'+
		'<tr><td><H1>'+mensajeC+'</H1></td></tr></table>';
	}
	if(cancelacion=='A') {
	 autentificacionAcuse = '<table width="300" align="center">'+
		'<tr><td width="300"><H1>'+mensajeC+'</H1></td></tr></table>';
	}
	
	var vacio  = '<table width="100" align="center">'+
		'<tr><td width="300"><H1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</H1></td></tr></table>';
	
	var mensajeAcuse = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAcuse',							
		width:	'400',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		autentificacion				
			}			
		]
	});
	
	var mensajeAcuse1 = new Ext.Container({
		layout: 'table',		
		id: 'mensajeAcuse1',							
		width:	'300',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		autentificacionAcuse				
			}			
		]
	});
	
	
	//para descargar el archivo CSV
	var procesarGenerarCSV =  function(opts, success, response) {
		var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
		btnGenerarCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			
			var jsonData = Ext.util.JSON.decode(response.responseText);	
			
			if(jsonData.accion=='button')  {
				var btnBajarCSV = Ext.getCmp('btnBajarCSV');
				btnBajarCSV.show();
				btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajarCSV.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
					}	);
			}
		} else {
			Ext.MessageBox.alert("Mensaje","Los archivos de los acuses de la publicaci�n de documentos no se generaron correctamente "); 
			btnGenerarCSV.enable();
			//NE.util.mostrarConnError(response,opts);
		}
	}	
	
	
	//para desacargar el archivo con errores
	var procesarGenerarError =  function(opts, success, response) {
		var btnArchivoErrores = Ext.getCmp('btnArchivoErrores');
		btnArchivoErrores.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarError = Ext.getCmp('btnBajarError');
			btnBajarError.show();
			btnBajarError.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarError.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnArchivoErrores.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	//para desacargar el archivo PDF
	var procesarGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonData = Ext.util.JSON.decode(response.responseText);	
		
			if(jsonData.accion=='Acuse')  {
			
					Ext.Ajax.request({
						url: '13forma2cExtCsv.jsp',
						params: {
							informacion:'ArchivoCSV',														
							proceso:proceso,
							acuse:acuse,
							strPendiente:strPendiente,
							strPreNegociable:strPreNegociable,
							doctoDuplicados:doctoDuplicados,
							accion:'Acuse'
						}					
						,callback: procesarGenerarCSV
					}); 
					
			
			}else  if(jsonData.accion=='button')  {	
			
				var btnBajarPDF = Ext.getCmp('btnBajarPDF');
				btnBajarPDF.show();
				btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
				btnBajarPDF.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
			}
		} else {
			Ext.MessageBox.alert("Mensaje","Los archivos de los acuses de la publicaci�n de documentos no se generaron correctamente "); 
			btnGenerarPDF.enable();
			//NE.util.mostrarConnError(response,opts); 
		}
	}	
	
	
	var fpBotonesRegresar = new Ext.Container({
		layout: 'table',
		id: 'fpBotonesRegresar',			
		width:	'5',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [		
			{
				xtype: 'button',
				text: 'Regresar',			
				id: 'btnRegresarC',
				align: 'center',
				hidden: true,
				handler: function() {
					window.location = "13forma02Ext.jsp?strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable;
				}
			}	
		]
	});
	
	//Contenedor para los botones de imprimir y Salir 
	var fpBotones = new Ext.Container({
		layout: 'table',		
		id: 'fpBotones',			
		width:	'400',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [		
			{
				xtype: 'button',
				text: 'Descargar Archivo con Errores',
				tooltip:	'Descargar Archivo con Errores',
				id: 'btnArchivoErrores',
				hidden: true,
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					var barraPaginacion = Ext.getCmp("barraPaginacion");
					Ext.Ajax.request({
						url: '13forma02Ext.data.jsp',
						params: {
							informacion:'ArchivoErrores',
							proceso:proceso
						}					
						,callback: procesarGenerarError
					});
				}						
			},		
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarError',
				hidden: true
			},		
			{ 
				xtype:   'label', 
				id: 'id_vacio',
				html:	vacio,
				hidden: true
			}	,			
			{
				xtype: 'button',
				text: 'Generar PDF',
				tooltip:	'Generar PDF',
				id: 'btnGenerarPDF',
				hidden: true,
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					var barraPaginacion = Ext.getCmp("barraPaginacion");
					Ext.Ajax.request({
						url: '13forma2cExtpdf.jsp',
						params: {
							informacion:'ArchivoPDF',														
							proceso:proceso,
							acuse:acuse,
							recibo:acuse,
							strPendiente:strPendiente,
							strPreNegociable:strPreNegociable,
							usuario:usuario,
							hidFechaCarga:hidFechaCarga,
							hidHoraCarga:hidHoraCarga,
							txttotdoc:txttotdoc,
							txtmtodoc:txtmtodoc,
							txtmtomindoc:txtmtomindoc,
							txtmtomaxdoc:txtmtomaxdoc,
							txttotdocdo:txttotdocdo,
							txtmtodocdo:txtmtodocdo,
							txtmtomindocdo:txtmtomindocdo,
							txtmtomaxdocdo:txtmtomaxdocdo,
							doctoDuplicados:doctoDuplicados,
							accion:'button'
						}					
						,callback: procesarGenerarPDF
					});
				}						
			},		
			{
				xtype: 'button',
				text: 'Bajar PDF',
				tooltip:	'Bajar PDF',
				id: 'btnBajarPDF',
				hidden: true
			},	
			{
				xtype: 'button',
				text: 'Generar Archivo',
				tooltip:	'Generar Archivo',
				id: 'btnGenerarCSV',
				hidden: true,
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					var barraPaginacion = Ext.getCmp("barraPaginacion");
					Ext.Ajax.request({
						url: '13forma2cExtCsv.jsp',
						params: {
							informacion:'ArchivoCSV',														
							proceso:proceso,
							acuse:acuse,
							strPendiente:strPendiente,
							strPreNegociable:strPreNegociable,
							doctoDuplicados:doctoDuplicados,
							accion:'button'
						}					
						,callback: procesarGenerarCSV
					});
				}			
			},
			{
				xtype: 'button',
				text: 'Bajar Archivo',
				tooltip:	'Bajar Archivo',
				id: 'btnBajarCSV',
				hidden: true
			},		
			{
				xtype: 'button',
				text: 'Salir',			
				id: 'btnSalir',	
				hidden: true,
				handler: function() {
					window.location = "13forma02Ext.jsp?strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable;
				}
			}	
		]
	});
	

		
	var procesarConsultaData3 = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;			
		
		if (arrRegistros != null) {
			if (!gridConsulta3.isVisible()) {
			gridConsulta3.show();
			}						
		
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;					
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta3.getColumnModel();			
			var el = gridConsulta3.getGridEl();				
			
			if(store.getTotalCount() > 0) {	
			
				if(jsonData.operaFVPyme =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEEDOR'), false);	
				}
				if(jsonData.bOperaFactorajeVencido =='S' || jsonData.bOperaFactConMandato =='S'  ||  jsonData.bOperaFactorajeVencidoInfonavit =='S' ) {					
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);	
				}				
				if(jsonData.bOperaFactorajeDistribuido  =='S' || jsonData.bOperaFactorajeVencidoInfonavit   =='S') {				
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'), false);	
				}

				if(jsonData.bestaHabilitadaPublicacionEPOPEF  =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('FECHA_RECEPCION'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COMPRA'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICADOR'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_MAXIMO'), false);	
				}
				if(jsonData.bestaHabilitadoNumeroSIAFF =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('DIGITO_IDENTIFICADOR'), false);	
				}					
				if(jsonData.bOperaFactConMandato =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('MANDANTE'), false);	
				}
				if(jsonData.bValidaDuplicidad =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('DUPLICADO'), false);
				}
				if(hayCamposAdicionales=='0'){
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.nomCampo4);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.nomCampo5);						
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}			
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var procesarConsultaData2 = function(store, arrRegistros, opts) 	{	
		var jsonData = store.reader.jsonData;									
		if (arrRegistros != null) {
			if (!gridConsulta2.isVisible()) {
			gridConsulta2.show();
			}						
		
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;					
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta2.getColumnModel();			
			var el = gridConsulta2.getGridEl();	
			
			if(store.getTotalCount() > 0) {			
				if(jsonData.operaFVPyme =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEEDOR'), false);	
				}
				if(jsonData.bOperaFactorajeVencido =='S' || jsonData.bOperaFactConMandato =='S'  ) {					
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);	
				}				
				if(jsonData.bOperaFactorajeDistribuido  =='S' ) {				
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'), false);	
				}

				if(jsonData.bestaHabilitadaPublicacionEPOPEF  =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('FECHA_RECEPCION'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COMPRA'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICADOR'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_MAXIMO'), false);	
				}
				if(jsonData.bestaHabilitadoNumeroSIAFF =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('DIGITO_IDENTIFICADOR'), false);	
				}
				
				if(jsonData.bOperaFactConMandato =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('MANDANTE'), false);	
				}
				if(jsonData.bValidaDuplicidad =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('DUPLICADO'), false);
				}
				if(hayCamposAdicionales=='0'){
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.nomCampo4);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.nomCampo5);						
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}			
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		
		var jsonData = store.reader.jsonData;									
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
			gridConsulta.show();
			}						
	
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();			
			var el = gridConsulta.getGridEl();				
			
			if(store.getTotalCount() > 0) {			
				if(jsonData.operaFVPyme =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEEDOR'), false);	
				}
				if(jsonData.bOperaFactorajeVencido =='S' || jsonData.bOperaFactConMandato =='S'  ||  jsonData.bOperaFactorajeVencidoInfonavit =='S' ) {					
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);	
				}				
				if(jsonData.bOperaFactorajeDistribuido  =='S' || jsonData.bOperaFactorajeVencidoInfonavit   =='S') {				
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'), false);	
				}
			
				
				if(jsonData.bestaHabilitadaPublicacionEPOPEF  =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_RECEPCION'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COMPRA'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICADOR'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_MAXIMO'), false);	
				}
				
				if(jsonData.bestaHabilitadoNumeroSIAFF =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DIGITO_IDENTIFICADOR'), false);	
				}
				
				if(jsonData.bOperaFactConMandato =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MANDANTE'), false);	
				}
				if(jsonData.bValidaDuplicidad =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DUPLICADO'), false);
				}
				if(hayCamposAdicionales=='0'){
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.nomCampo4);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.nomCampo5);						
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}			
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}

	
	//para los totales del grid Nomal 
	var totalesControl = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13forma02Ext.data.jsp',
		baseParams: {
			informacion: 'ControlTotales'	
		},								
		fields: [			
			{name: 'INFORMACION' , mapping: 'INFORMACION'},
			{name: 'MONEDA_NACIONAL',  mapping: 'MONEDA_NACIONAL'},
			{name: 'MONEDA_DL' , mapping: 'MONEDA_DL' }						
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	  
	
	var gridControl = new Ext.grid.EditorGridPanel({	
		id: 'gridControl',
		store: totalesControl,	
		title: '',			
		columns: [	
			{
				header: '',
				dataIndex: 'INFORMACION',
				width: 350,
				align: 'left'				
			},
			{
				header: 'Moneda Nacional',
				dataIndex: 'MONEDA_NACIONAL',
				width: 150,
				align: 'right'				
			},
			{
				header: 'D�lares',
				dataIndex: 'MONEDA_DL',
				width: 150,
				align: 'right'				
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 160,
		width: 655,		
		style: 'margin:0 auto;',
		frame: false
	});
	
	
	var procesarTotalesData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;			
		
		if (arrRegistros != null) {	
			if(store.getTotalCount() > 0) {						
				Ext.Ajax.request({
					url: '13forma2cExtpdf.jsp',
					params: {
						informacion:'ArchivoPDF',														
						proceso:proceso,
						acuse:acuse,
						recibo:acuse,
						strPendiente:strPendiente,
						strPreNegociable:strPreNegociable,
						usuario:usuario,
						hidFechaCarga:hidFechaCarga,
						hidHoraCarga:hidHoraCarga,
						txttotdoc:txttotdoc,
						txtmtodoc:txtmtodoc,
						txtmtomindoc:txtmtomindoc,
						txtmtomaxdoc:txtmtomaxdoc,
						txttotdocdo:txttotdocdo,
						txtmtodocdo:txtmtodocdo,
						txtmtomindocdo:txtmtomindocdo,
						txtmtomaxdocdo:txtmtomaxdocdo,
						doctoDuplicados:doctoDuplicados,
						accion:'Acuse'
					}					
					,callback: procesarGenerarPDF
				});					
											
			}
		}
	}
	
			//para los totales del grid Nomal 
	var totalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13forma02Ext.data.jsp',
		baseParams: {
			informacion: 'TotalesPreAcuse'	
		},								
		fields: [			
			{name: 'INFORMACION' , mapping: 'INFORMACION'},		
			{name: 'TOTAL_MONTO' ,  mapping: 'TOTAL_MONTO' },
			{name: 'TOTAL_MONTO_DESC',   mapping: 'TOTAL_MONTO_DESC'}				
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarTotalesData,	
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		id: 'gridTotales',
		store: totalesData,
		margins: '20 0 0 0',
		align: 'center',
		columns: [	
			{
				header: ' ',
				dataIndex: 'INFORMACION',
				width: 350,
				align: 'right'				
			},		
			{
				header: 'Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'right'				
			},
			{
				header: 'Monto a Descontar',
				dataIndex: 'TOTAL_MONTO_DESC',
				width: 150,
				align: 'right'				
			},
			{
				header: '',
				dataIndex: '',
				width: 245,
				align: 'right'			
			}		
		],
		height: 220,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	
	
	var consultaData3 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma02Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaAcuse3'
		},
		fields: [						
			{name: 'NOMBRE_PROVEEDOR'},	
			{name: 'NUM_DOCUMENTO'},	
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'FECHA_VENC_PROVEEDOR'},	
			{name: 'IC_MONEDA'},	
			{name: 'MONEDA'},	
			{name: 'TIPO_FACTORAJE'},	
			{name: 'MONTO'},	
			{name: 'PORC_DESCUENTO'},	
			{name: 'MONTO_DESCONTAR'},			
			{name: 'REFERENCIA'},	
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_BENEFICIARIO'},	
			{name: 'PORC_BENEFICIARIO'},	
			{name: 'MONTO_BENEFICIARIO'},	
			{name: 'CAMPO1'},	
			{name: 'CAMPO2'},	
			{name: 'CAMPO3'},	
			{name: 'CAMPO4'},	
			{name: 'CAMPO5'},	
			{name: 'FECHA_RECEPCION'},	
			{name: 'TIPO_COMPRA'},	
			{name: 'CLASIFICADOR'},	
			{name: 'PLAZO_MAXIMO'},
			{name: 'DIGITO_IDENTIFICADOR'},
			{name: 'MANDANTE'},
			{name: 'CONSECUTIVO'},
			{name: 'IC_PYME'},
			{name: 'IC_PROCESO'},
			{name: 'ESTATUS'},
			{name: 'DUPLICADO'}
			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData3,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData3(null, null, null);						
					}
				}
			}			
	});
	
	//var gridConsulta = new Ext.grid.EditorGridPanel({
	var gridConsulta3 = new Ext.grid.GridPanel({
		id: 'gridConsulta3',
		title: 'Carga de Documentos',		
		store: consultaData3,	
		columns: [
			{							
				header : 'ESTATUS',
				tooltip: 'ESTATUS',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				hidden: true,
				resizable: true,			
				align: 'left'	
			},	
			{							
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUM_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex : 'FECHA_EMISION',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento Proveedor',
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex : 'FECHA_VENC_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				hidden: true
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORC_DESCUENTO',
				width : 150,
				sortable : true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')			
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCONTAR',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},		
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Nombre Beneficiario',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'NOMBRE_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Porcentaje Beneficiario',
				tooltip: 'Porcentaje Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Monto Beneficiario',
				tooltip: 'Monto Beneficiario',
				dataIndex : 'MONTO_BENEFICIARIO',
				hidden: true,
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{							
				header : 'Campo1',
				tooltip: 'Campo1',
				dataIndex : 'CAMPO1',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo2',
				tooltip: 'Campo2',
				dataIndex : 'CAMPO2',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Campo3',
				tooltip: 'Campo3',
				dataIndex : 'CAMPO3',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo4',
				tooltip: 'Campo4',
				dataIndex : 'CAMPO4',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo5',
				tooltip: 'Campo5',
				dataIndex : 'CAMPO5',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Fecha Entrega ',
				tooltip: 'Fecha Entrega ',
				dataIndex : 'FECHA_RECEPCION',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Tipo de Compra',
				tooltip: 'Tipo de Compra',
				dataIndex : 'TIPO_COMPRA',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Clave Presupuestaria ',
				tooltip: 'Clave Presupuestaria ',
				dataIndex : 'CLASIFICADOR',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Periodo',
				tooltip: 'Periodo',
				dataIndex : 'PLAZO_MAXIMO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Digito Identificador',
				tooltip: 'Digito Identificador',
				dataIndex : 'DIGITO_IDENTIFICADOR',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'MANDANTE',
				width : 150,
				align: 'center',
				hidden: true,				
				sortable : false
			},
			{							
				header : 'Notificado como <br>posible Duplicado',
				tooltip: 'Notificado como posible Duplicado',
				dataIndex : 'DUPLICADO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	
	var consultaData2 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma02Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaAcuse2'
		},
		fields: [						
			{name: 'NOMBRE_PROVEEDOR'},	
			{name: 'NUM_DOCUMENTO'},	
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'FECHA_VENC_PROVEEDOR'},	
			{name: 'IC_MONEDA'},	
			{name: 'MONEDA'},	
			{name: 'TIPO_FACTORAJE'},	
			{name: 'MONTO'},	
			{name: 'PORC_DESCUENTO'},	
			{name: 'MONTO_DESCONTAR'},			
			{name: 'REFERENCIA'},	
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_BENEFICIARIO'},	
			{name: 'PORC_BENEFICIARIO'},	
			{name: 'MONTO_BENEFICIARIO'},	
			{name: 'CAMPO1'},	
			{name: 'CAMPO2'},	
			{name: 'CAMPO3'},	
			{name: 'CAMPO4'},	
			{name: 'CAMPO5'},	
			{name: 'FECHA_RECEPCION'},	
			{name: 'TIPO_COMPRA'},	
			{name: 'CLASIFICADOR'},	
			{name: 'PLAZO_MAXIMO'},	
			{name: 'MANDANTE'},
			{name: 'DIGITO_IDENTIFICADOR'},
			{name: 'CONSECUTIVO'},
			{name: 'IC_PYME'},
			{name: 'IC_PROCESO'},
			{name: 'ESTATUS'},
			{name: 'DUPLICADO'}
			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData2,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData2(null, null, null);						
					}
				}
			}			
	});
	
var gridConsulta2 = new Ext.grid.GridPanel({
		id: 'gridConsulta2',
		title: 'Carga de Documentos',		
		store: consultaData2,	
		columns: [
			{							
				header : 'ESTATUS',
				tooltip: 'ESTATUS',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left'	
			},	
			{							
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUM_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex : 'FECHA_EMISION',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento Proveedor',
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex : 'FECHA_VENC_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				hidden: true
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORC_DESCUENTO',
				width : 150,
				sortable : true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')			
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCONTAR',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},		
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Nombre Beneficiario',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'NOMBRE_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Porcentaje Beneficiario',
				tooltip: 'Porcentaje Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Monto Beneficiario',
				tooltip: 'Monto Beneficiario',
				dataIndex : 'MONTO_BENEFICIARIO',
				hidden: true,
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')						
			},
			{							
				header : 'Campo1',
				tooltip: 'Campo1',
				dataIndex : 'CAMPO1',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo2',
				tooltip: 'Campo2',
				dataIndex : 'CAMPO2',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Campo3',
				tooltip: 'Campo3',
				dataIndex : 'CAMPO3',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo4',
				tooltip: 'Campo4',
				dataIndex : 'CAMPO4',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo5',
				tooltip: 'Campo5',
				dataIndex : 'CAMPO5',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Fecha Entrega ',
				tooltip: 'Fecha Entrega ',
				dataIndex : 'FECHA_RECEPCION',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Tipo de Compra ',
				tooltip: 'Tipo de Compra ',
				dataIndex : 'TIPO_COMPRA',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Clave Presupuestaria ',
				tooltip: 'Clave Presupuestaria ',
				dataIndex : 'CLASIFICADOR',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Periodo',
				tooltip: 'Periodo',
				dataIndex : 'PLAZO_MAXIMO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Digito Identificador',
				tooltip: 'Digito Identificador',
				dataIndex : 'DIGITO_IDENTIFICADOR',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'MANDANTE',
				width : 150,
				align: 'center',
				hidden: true,				
				sortable : false
			},
			{							
				header : 'Notificado como <br>posible Duplicado',
				tooltip: 'Notificado como posible Duplicado',
				dataIndex : 'DUPLICADO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma02Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaAcuse'
		},
		fields: [						
			{name: 'NOMBRE_PROVEEDOR'},	
			{name: 'NUM_DOCUMENTO'},	
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'FECHA_VENC_PROVEEDOR'},	
			{name: 'IC_MONEDA'},	
			{name: 'MONEDA'},	
			{name: 'TIPO_FACTORAJE'},	
			{name: 'MONTO'},	
			{name: 'PORC_DESCUENTO'},	
			{name: 'MONTO_DESCONTAR'},			
			{name: 'REFERENCIA'},	
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_BENEFICIARIO'},	
			{name: 'PORC_BENEFICIARIO'},	
			{name: 'MONTO_BENEFICIARIO'},	
			{name: 'CAMPO1'},	
			{name: 'CAMPO2'},	
			{name: 'CAMPO3'},	
			{name: 'CAMPO4'},	
			{name: 'CAMPO5'},	
			{name: 'FECHA_RECEPCION'},	
			{name: 'TIPO_COMPRA'},	
			{name: 'CLASIFICADOR'},	
			{name: 'PLAZO_MAXIMO'},	
			{name: 'DIGITO_IDENTIFICADOR'},
			{name: 'MANDANTE'},
			{name: 'CONSECUTIVO'},
			{name: 'IC_PYME'},
			{name: 'IC_PROCESO'},
			{name: 'ESTATUS'},
			{name: 'DUPLICADO'}			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});
	
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',
		title: 'Carga de Documentos',		
		store: consultaData,	
		columns: [
			{							
				header : 'ESTATUS',
				tooltip: 'ESTATUS',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				hidden: true,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUM_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex : 'FECHA_EMISION',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento Proveedor',
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex : 'FECHA_VENC_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				hidden: true
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORC_DESCUENTO',
				width : 150,
				sortable : true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')			
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCONTAR',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},		
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Nombre Beneficiario',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'NOMBRE_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Porcentaje Beneficiario',
				tooltip: 'Porcentaje Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Monto Beneficiario',
				tooltip: 'Monto Beneficiario',
				dataIndex : 'MONTO_BENEFICIARIO',
				hidden: true,
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')						
			},
			{							
				header : 'Campo1',
				tooltip: 'Campo1',
				dataIndex : 'CAMPO1',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo2',
				tooltip: 'Campo2',
				dataIndex : 'CAMPO2',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Campo3',
				tooltip: 'Campo3',
				dataIndex : 'CAMPO3',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo4',
				tooltip: 'Campo4',
				dataIndex : 'CAMPO4',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo5',
				tooltip: 'Campo5',
				dataIndex : 'CAMPO5',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Fecha Entrega ',
				tooltip: 'Fecha Entrega ',
				dataIndex : 'FECHA_RECEPCION',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Tipo de Compra',
				tooltip: 'Tipo de Compra',
				dataIndex : 'TIPO_COMPRA',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Clave Presupuestaria ',
				tooltip: 'Clave Presupuestaria ',
				dataIndex : 'CLASIFICADOR',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Periodo',
				tooltip: 'Periodo',
				dataIndex : 'PLAZO_MAXIMO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Digito Identificador',
				tooltip: 'Digito Identificador',
				dataIndex : 'DIGITO_IDENTIFICADOR',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'MANDANTE',
				width : 150,
				align: 'center',
				hidden: true,				
				sortable : false
			},
			{							
				header : 'Notificado como <br>posible Duplicado',
				tooltip: 'Notificado como posible Duplicado',
				dataIndex : 'DUPLICADO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			mensajeAcuse,	
			mensajeAcuse1,
			NE.util.getEspaciador(20),
			gridControl,										
			gridCifrasControl,
			NE.util.getEspaciador(20),
			leyendaLegal,			
			NE.util.getEspaciador(20),
			fpBotones,
			fpBotonesRegresar,
			NE.util.getEspaciador(20),
			gridConsulta,
			NE.util.getEspaciador(20),
			gridConsulta2,
			NE.util.getEspaciador(20),
			gridConsulta3,
			NE.util.getEspaciador(20),
			gridTotales,
			NE.util.getEspaciador(20)
		]
	});
	
	if(cancelacion=='S' ||  cancelacion=='N' ) {	
			leyendaLegal.hide();
			gridControl.hide();
			gridConsulta.hide();
			gridConsulta2.hide();
			gridConsulta3.hide();	
			gridTotales.hide();
			
			var btnRegresarC   = Ext.getCmp("btnRegresarC");
			btnRegresarC.show();			
						
	}	else  if(cancelacion=='A' ){
		
		
		var btnGenerarPDF   = Ext.getCmp("btnGenerarPDF");
		var btnGenerarCSV   = Ext.getCmp("btnGenerarCSV");
		var btnSalir   = Ext.getCmp("btnSalir");
		var btnArchivoErrores   = Ext.getCmp("btnArchivoErrores");	
		var id_vacio   = Ext.getCmp("id_vacio");
		
		btnGenerarPDF.show();
		btnGenerarCSV.show();
		btnSalir.show();			
		
		if(hayErrores =='S'){
			btnArchivoErrores.show();		
		}else {
			id_vacio.show();
		}
		
					
		var acuseCifras = [
			['No. de acuse ', acuseFormateado],
			['Fecha de carga ', hidFechaCarga],
			['Hora de carga ', hidHoraCarga],
			['Usuario ', usuario]
		];
		
		storeCifrasData.loadData(acuseCifras);	
		gridCifrasControl.show();
	
	
		if(strPendiente=='S'){
			gridConsulta.setTitle('<div><div style="float:left">Pendientes Negociables</div><div style="float:right"></div></div>');
			gridConsulta2.setTitle('<div><div style="float:left">Pendientes No Negociables</div><div style="float:right"></div></div>');
		}else if(strPreNegociable =='S'){
			gridConsulta.setTitle('<div><div style="float:left">Pre Negociables</div><div style="float:right"></div></div>');						
			gridConsulta2.setTitle('<div><div style="float:left">Pre No Negociables</div><div style="float:right"></div></div>');		
		}else  {
			gridConsulta.setTitle('<div><div style="float:left">Negociables</div><div style="float:right"></div></div>');						
			gridConsulta2.setTitle('<div><div style="float:left">No Negociables</div><div style="float:right"></div></div>');				
		}
		gridConsulta3.setTitle('<div><div style="float:left">Vencidos Sin Operar</div><div style="float:right"></div></div>');
		
	
		consultaData.load({  	
			params: {  
				informacion: 'ConsultaAcuse',
				operacion: 'Generar',
				start:0,
				limit:15,
				proceso:proceso, 
				strPendiente:strPendiente,  	
				strPreNegociable:strPreNegociable, 	
				pantalla:'Acuse',
				acuse:acuse,
				doctoDuplicados:doctoDuplicados
			}  
		});	
		
		consultaData2.load({  	
			params: {  
				informacion: 'ConsultaAcuse2',
				operacion: 'Generar',
				start:0,
				limit:15,
				proceso:proceso, 
				strPendiente:strPendiente,  	
				strPreNegociable:strPreNegociable, 	
				pantalla:'Acuse',
				acuse:acuse,
				doctoDuplicados:doctoDuplicados
			}  
		});	
	if(bVenSinOperarS =='S') {		
		consultaData3.load({  	
			params: {  
				informacion: 'ConsultaAcuse3',
				operacion: 'Generar',
				start:0,
				limit:15,
				proceso:proceso, 
				strPendiente:strPendiente,  	
				strPreNegociable:strPreNegociable, 	
				pantalla:'Acuse',
				acuse:acuse,
				doctoDuplicados:doctoDuplicados
			}  
		});	
	}
	
	
		totalesControl.load({ 	
			params: { 	
				informacion: 'ControlTotales',  	
				proceso:proceso, 
				strPendiente:strPendiente,  	
				strPreNegociable:strPreNegociable,
				txttotdoc:txttotdoc,
				txtmtodoc:txtmtodoc,
				txtmtomindoc:txtmtomindoc,
				txtmtomaxdoc:txtmtomaxdoc,
				txttotdocdo:txttotdocdo,
				txtmtodocdo:txtmtodocdo,
				txtmtomindocdo:txtmtomindocdo,
				txtmtomaxdocdo:txtmtomaxdocdo,
				doctoDuplicados:doctoDuplicados
			} 
		});
		
		totalesData.load({ 	params: { 	informacion: 'TotalesPreAcuse',  proceso:proceso,	strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, acuse:acuse, pantalla:'PreAcuse' , doctoDuplicados:doctoDuplicados	} });
	
	}
	
	if(bVenSinOperarS =='N') {	
		gridConsulta3.hide();
	}

	
} );