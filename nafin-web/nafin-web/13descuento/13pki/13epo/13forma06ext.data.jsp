<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.descuento.*, java.math.*,
		com.netro.model.catalogos.CatalogoIF,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoPYME,
		com.netro.model.catalogos.CatalogoSimple,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>
<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

if (informacion.equals("Parametrizacion")) {	//	*-*-*-*-*-*--*-*-*-*-*-*-*-*	Parametrizacion	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

	JSONObject jsonObj = new JSONObject();
	String sFechaVencPyme="";
	ArrayList alParamEPO = BeanParamDscto.getParamEPO(iNoCliente, 1);
	if(alParamEPO.size()>0) {
		sFechaVencPyme = (alParamEPO.get(7)==null)?"N":alParamEPO.get(7).toString();
		jsonObj.put("sFechaVencPyme", sFechaVencPyme);
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("CatalogoNombrePyme")){

	CatalogoPYME cat = new CatalogoPYME();
	cat.setCampoClave("ic_pyme");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveEpo(iNoCliente);
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoIf")) {

	CatalogoIF cat = new CatalogoIF();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveEpo(iNoCliente);
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("CatalogoMoneda")) {

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	infoRegresar = cat.getJSONElementos();
 
} else  if ("catTipoFactorajeData".equals(informacion)   ) {
    
    Hashtable alParamEPO = new Hashtable();
    StringBuilder fac = new StringBuilder(); 
    fac.append("N");
    alParamEPO = BeanParamDscto.getParametrosEPO(iNoCliente, 1);
    if (alParamEPO!=null) {    
	if ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString())  ) {
	    fac.append(fac.length() > 0 ? ", " : "").append("V");
	}
	if ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString())  ) {
	    fac.append(fac.length() > 0 ? ", " : "").append("D");
	}
    }
 
    CatalogoSimple catalogo = new CatalogoSimple();
    catalogo.setCampoClave("CC_TIPO_FACTORAJE");
    catalogo.setCampoDescripcion("CG_NOMBRE");
    catalogo.setTabla("COMCAT_TIPO_FACTORAJE");		
    catalogo.setOrden("CG_NOMBRE");
    catalogo.setValoresCondicionIn(fac.toString(), String.class);
    infoRegresar = catalogo.getJSONElementos();	


}else if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion

	IMantenimiento BeanMantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB", IMantenimiento.class);

	String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
	String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
	String ig_numero_docto = (request.getParameter("ig_numero_docto") == null)?"":request.getParameter("ig_numero_docto");
	String df_fecha_venc = (request.getParameter("df_fecha_venc") == null)?"":request.getParameter("df_fecha_venc");
	JSONObject jsonObj = new JSONObject();
	String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
	String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
	String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
	String sParamFechaVencPyme = (request.getParameter("sParamFechaVencPyme") == null)?"":request.getParameter("sParamFechaVencPyme");
	String cmbFactoraje = (request.getParameter("cmbFactoraje") == null)?"":request.getParameter("cmbFactoraje"); 
	
	List regs = new ArrayList();
	String claveMoneda="", claveDocumento = "", nombrePyme = "", numeroDocumento = "", fechaVencimiento = "";
	String df_fecha_venc_pyme = "";
	String estatusDocumento = "", monedaNombre = "", monto = "", nombreIF = "";
	int numRegistrosMN = 0, numRegistrosDL = 0;
	BigDecimal montoTotalMN = new BigDecimal("0.00");
	BigDecimal montoTotalDL = new BigDecimal("0.00");
	BigDecimal montoTotalDescuentoMN = new BigDecimal("0.00");
	BigDecimal montoTotalDescuentoDL = new BigDecimal("0.00");
	BigDecimal importeTotalInteresMN = new BigDecimal("0.00");
	BigDecimal importeTotalInteresDL = new BigDecimal("0.00");
	BigDecimal importeTotalRecibirMN = new BigDecimal("0.00");
	BigDecimal importeTotalRecibirDL = new BigDecimal("0.00");
	BigDecimal importeTotalRecGarMN = new BigDecimal("0.00");
	BigDecimal importeTotalRecGarDL = new BigDecimal("0.00");
	double Recurso = 0, Porcentaje = 0;
	String montoDescuento = "", importeInteres = "", importeRecibir = "";
	Vector vDoctosCamEstus = BeanMantenimiento.ovgetDocumentosCambioEstatusNeg(iNoCliente, ic_pyme, ic_if, ig_numero_docto, df_fecha_venc, ic_moneda, fn_montoMin, fn_montoMax, cmbFactoraje);
	if (vDoctosCamEstus.size()>0) {
		Vector vDatosEstatus = null;
		
		for (int i=0;i<vDoctosCamEstus.size();i++) {
			HashMap hash = new HashMap();
			vDatosEstatus = (Vector)vDoctosCamEstus.get(i);
			claveMoneda = vDatosEstatus.get(0).toString();
			claveDocumento = vDatosEstatus.get(1).toString();
			nombrePyme = vDatosEstatus.get(2).toString();
			nombreIF = vDatosEstatus.get(3).toString();
			numeroDocumento = vDatosEstatus.get(4).toString();
			fechaVencimiento = vDatosEstatus.get(5).toString()==null?"":vDatosEstatus.get(5).toString();
			estatusDocumento = vDatosEstatus.get(6).toString();
			monedaNombre = vDatosEstatus.get(7).toString();
			monto = vDatosEstatus.get(8).toString();
				String rs_porc = vDatosEstatus.get(9).toString()==null?"":vDatosEstatus.get(9).toString();
			if("".equals(rs_porc))
				rs_porc = "0";
			Porcentaje = new Double(rs_porc).doubleValue();
			montoDescuento = vDatosEstatus.get(10).toString()==null?"":vDatosEstatus.get(10).toString();
			if("".equals(montoDescuento))
				montoDescuento = "0";				
			importeInteres = vDatosEstatus.get(11).toString();
			importeRecibir = vDatosEstatus.get(12).toString();
			Recurso = new Double(monto).doubleValue() - new Double(montoDescuento).doubleValue();
			df_fecha_venc_pyme = vDatosEstatus.get(13).toString()==null?"":vDatosEstatus.get(13).toString();
			String tipoFactoraje = vDatosEstatus.get(14).toString()==null?"":vDatosEstatus.get(14).toString();
			hash.put("IC_MONEDA",claveMoneda);
			hash.put("IC_DOCUMENTO",claveDocumento);
			hash.put("CG_RAZON_SOCIAL",nombrePyme);
			hash.put("IF_CG_RAZON_SOCIAL",nombreIF);
			hash.put("IG_NUMERO_DOCTO",numeroDocumento);
			hash.put("DF_FECHA_VENC",fechaVencimiento);
			hash.put("CD_DESCRIPCION",estatusDocumento);
			hash.put("CD_NOMBRE",monedaNombre);
			hash.put("FN_MONTO",monto);
			hash.put("FN_PORC_ANTICIPO",rs_porc);
			hash.put("FN_MONTO_DSCTO",montoDescuento);
			hash.put("IN_IMPORTE_INTERES",importeInteres);
			hash.put("IN_IMPORTE_RECIBIR",importeRecibir);
			hash.put("DF_FECHA_VENC_PYME",df_fecha_venc_pyme);
			hash.put("FLAG_CHECK","");
			hash.put("NUEVO_ESTATUS","Negociable");
			hash.put("TIPO_FACTORAJE",tipoFactoraje);
			String user = strLogin + " " + strNombreUsuario;
			hash.put("USUARIO",user);
			regs.add(hash);
			if(claveMoneda.equals("1")) {
				numRegistrosMN++;
				if (!monto.equals("")) montoTotalMN = montoTotalMN.add(new BigDecimal(monto));
				if (!montoDescuento.equals("")) montoTotalDescuentoMN = montoTotalDescuentoMN.add(new BigDecimal(montoDescuento));
				if (!importeInteres.equals("")) importeTotalInteresMN = importeTotalInteresMN.add(new BigDecimal(importeInteres));
				if (!importeRecibir.equals("")) importeTotalRecibirMN = importeTotalRecibirMN.add(new BigDecimal(importeRecibir));
				if (!importeRecibir.equals("")) importeTotalRecGarMN = importeTotalRecGarMN.add(new BigDecimal(Recurso));
			}
			else if(claveMoneda.equals("54")) {	//IC_MONEDA=54
				numRegistrosDL++;
				if (!monto.equals("")) montoTotalDL = montoTotalDL.add(new BigDecimal(monto));
				if (!montoDescuento.equals("")) montoTotalDescuentoDL = montoTotalDescuentoDL.add(new BigDecimal(montoDescuento));
				if (!importeInteres.equals("")) importeTotalInteresDL = importeTotalInteresDL.add(new BigDecimal(importeInteres));
				if (!importeRecibir.equals("")) importeTotalRecibirDL = importeTotalRecibirDL.add(new BigDecimal(importeRecibir));
				if (!importeRecibir.equals("")) importeTotalRecGarDL = importeTotalRecGarDL.add(new BigDecimal(Recurso));
			}
		}

		if (regs != null){
			JSONArray jsObjArray = new JSONArray();
			jsObjArray = JSONArray.fromObject(regs);
			jsonObj.put("registros", jsObjArray);
			/*
			List totalReg = new ArrayList();
			HashMap hTot = new HashMap();
			if(numRegistrosMN>0){
				hTot.put("NOMMONEDA","MONEDA NACIONAL");
				hTot.put("TOTAL_REGISTROS",Double.toString(numRegistrosMN));
				hTot.put("TOTAL_MONTO_DOCUMENTOS",montoTotalMN.toPlainString());
				hTot.put("TOTAL_MONTO_DESCUENTO",montoTotalDescuentoMN.toPlainString());
				hTot.put("TOTAL_MONTO_INTERES",importeTotalInteresMN.toPlainString());
				hTot.put("TOTAL_MONTO_RECIBIR",importeTotalRecibirMN.toPlainString());
				hTot.put("TOTAL_MONTO_RECGAR",importeTotalRecGarMN.toPlainString());
				totalReg.add(hTot);
			}
			if(numRegistrosDL>0){
				hTot = new HashMap();
				hTot.put("NOMMONEDA","DOLARES AMERICANOS");
				hTot.put("TOTAL_REGISTROS",Double.toString(numRegistrosDL));
				hTot.put("TOTAL_MONTO_DOCUMENTOS",montoTotalDL);
				hTot.put("TOTAL_MONTO_DESCUENTO",montoTotalDescuentoDL.toPlainString());
				hTot.put("TOTAL_MONTO_INTERES",importeTotalInteresDL.toPlainString());
				hTot.put("TOTAL_MONTO_RECIBIR",importeTotalRecibirDL.toPlainString());
				hTot.put("TOTAL_MONTO_RECGAR",importeTotalRecGarDL.toPlainString());
				totalReg.add(hTot);
			}
			
			if (totalReg != null){
				JSONArray jsObjArr = new JSONArray();
				jsObjArr = JSONArray.fromObject(totalReg);
				jsonObj.put("totales", jsObjArr);
			}
			*/
		}
	}
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
/*	JSONArray jsObjArray = new JSONArray();
	if (regs != null){
		jsObjArray	=	JSONArray.fromObject(regs);
		infoRegresar = "{\"success\": true, \"total\": " + jsObjArray.size() + ", \"registros\": " + jsObjArray.toString()+"}";
	}else{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
*/

}else if (informacion.equals("Captura")) {

	IMantenimiento BeanMantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB", IMantenimiento.class);

	JSONObject jsonObj = new JSONObject();
	String seleccionados[] = request.getParameterValues("seleccionados");
	String estatus[] = request.getParameterValues("estatus");
	String ct_cambio_motivo = request.getParameter("ct_cambio_motivo");
	String claves = "";
	boolean errorUsuario = true;
	//	Variables de seguridad
	String pkcs7 = request.getParameter("Pkcs7");
	String folioCert = "";
	String externContent = request.getParameter("TextoFirmado");
	char getReceipt = 'Y';

	Seguridad s = new Seguridad();
	if (!_serial.equals("") && externContent!=null && pkcs7!=null) {
		folioCert = "123456789";	//Este numero no importa en esta pantalla!
		
		errorUsuario = false;
		if (errorUsuario == false && s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt))	{
			String _acuse = s.getAcuse();
			jsonObj.put("_acuse", _acuse);
			
			ArrayList alClaves = null;
			alClaves = (ArrayList)BeanMantenimiento.osactualizaHistoricoCambioEstatus(seleccionados, estatus, ct_cambio_motivo, strLogin + " " + strNombreUsuario);//FODEA 015 - 2009 ACF
			claves = (String)alClaves.get(0);
			boolean bOkInsert = BeanMantenimiento.bactualizaEstatusNegociable(claves);
			jsonObj.put("success", new Boolean(true));
	
		} else {	//autenticación fallida
			String _error = s.mostrarError();
			jsonObj.put("success", new Boolean(false));
			jsonObj.put("msg", "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO");
		}
	}
	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>