<%@ page contentType="application/json;charset=UTF-8"
	import="
	javax.naming.*,
	java.util.*,
	java.math.*, 
	java.text.*, 
	java.sql.*,
	netropology.utilerias.*,	
	com.netro.model.catalogos.*,	
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,	
	com.netro.descuento.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String cg_pyme_epo_interno = (request.getParameter("cg_pyme_epo_interno") != null) ? request.getParameter("cg_pyme_epo_interno") : "";
	String ic_pyme = (request.getParameter("ic_pyme") != null) ? request.getParameter("ic_pyme") : "";
	String ig_numero_docto = (request.getParameter("ig_numero_docto") != null) ? request.getParameter("ig_numero_docto") : "";
	String df_fecha_docto = (request.getParameter("df_fecha_docto") != null) ? request.getParameter("df_fecha_docto") : "";
	String df_fecha_venc = (request.getParameter("df_fecha_venc") != null) ? request.getParameter("df_fecha_venc") : "";
	String df_fecha_venc_pyme = (request.getParameter("df_fecha_venc_pyme") != null) ? request.getParameter("df_fecha_venc_pyme") : "";
	String ic_moneda = (request.getParameter("ic_moneda") != null) ? request.getParameter("ic_moneda") : "1";
	String fn_monto = (request.getParameter("fn_monto") != null) ? request.getParameter("fn_monto") : "";
	String ic_tipo_fact = (request.getParameter("ic_tipo_fact") != null) ? request.getParameter("ic_tipo_fact") : "N";
	String ic_estatus_docto = (request.getParameter("ic_estatus_docto") != null) ? request.getParameter("ic_estatus_docto") : "S";
	String ic_mandante = (request.getParameter("ic_mandante") != null) ? request.getParameter("ic_mandante") : "";
	String ic_if = (request.getParameter("ic_if") != null) ? request.getParameter("ic_if") : "";
	String ct_referencia = (request.getParameter("ct_referencia") != null) ? request.getParameter("ct_referencia") : "";
	String ic_beneficiario = (request.getParameter("ic_beneficiario") != null) ? request.getParameter("ic_beneficiario") : "";
	String fn_porc_beneficiario = (request.getParameter("fn_porc_beneficiario") != null) ? request.getParameter("fn_porc_beneficiario") : "";
	String fn_monto_beneficiario = (request.getParameter("fn_monto_beneficiario") != null) ? request.getParameter("fn_monto_beneficiario") : "";
	String df_fecha_entrega = (request.getParameter("df_fecha_entrega") != null) ? request.getParameter("df_fecha_entrega") : "";
	String cg_tipo_compra = (request.getParameter("cg_tipo_compra") != null) ? request.getParameter("cg_tipo_compra") : "";
	String cg_clave_presupuestaria = (request.getParameter("cg_clave_presupuestaria") != null) ? request.getParameter("cg_clave_presupuestaria") : "";
	String cg_periodo = (request.getParameter("cg_periodo") != null) ? request.getParameter("cg_periodo") : "";
	String cg_campo1 = (request.getParameter("cg_campo1") != null) ? request.getParameter("cg_campo1") : "";
	String cg_campo2 = (request.getParameter("cg_campo2") != null) ? request.getParameter("cg_campo2") : "";
	String cg_campo3 = (request.getParameter("cg_campo3") != null) ? request.getParameter("cg_campo3") : "";
	String cg_campo4 = (request.getParameter("cg_campo4") != null) ? request.getParameter("cg_campo4") : "";
	String cg_campo5 = (request.getParameter("cg_campo5") != null) ? request.getParameter("cg_campo5") : "";
	String proceso = (request.getParameter("proceso") != null) ? request.getParameter("proceso") : "";
	String pantalla = (request.getParameter("pantalla") != null) ? request.getParameter("pantalla") : "";
	String noConsecutivo = (request.getParameter("noConsecutivo") != null) ? request.getParameter("noConsecutivo") : "";
	String acuse1 = (request.getParameter("acuse") != null) ? request.getParameter("acuse") : "";
	String acuseFormateado = (request.getParameter("acuseFormateado") != null) ? request.getParameter("acuseFormateado") : "";
	String usuario = (request.getAttribute("usuario")==null)?request.getParameter("usuario"):request.getAttribute("usuario").toString();
	String hidFechaCarga = (request.getAttribute("hidFechaCarga")==null)?request.getParameter("hidFechaCarga"):request.getAttribute("hidFechaCarga").toString();
	String hidHoraCarga = (request.getAttribute("hidHoraCarga")==null)?request.getParameter("hidHoraCarga"):request.getAttribute("hidHoraCarga").toString();
	String hidFechaVencMin = (request.getParameter("hidFechaVencMin") != null) ? request.getParameter("hidFechaVencMin") : "";	
	String strPendiente = (request.getParameter("strPendiente") != null) ? request.getParameter("strPendiente") : "N";
	String strPreNegociable = (request.getParameter("strPreNegociable") != null) ? request.getParameter("strPreNegociable") : "N";
	String operacion = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
	String duplicado = (request.getParameter("duplicado") != null) ? request.getParameter("duplicado") : "N";
	
	//if(ic_moneda.equals("MONEDA NACIONAL"))  ic_moneda ="1";
	//if(ic_tipo_fact.equals("NORMAL"))  ic_tipo_fact ="N";
	
	if(ic_tipo_fact.equals("C")) ic_estatus_docto ="S";	
		
	SimpleDateFormat fecha_hoy = new SimpleDateFormat("dd/MM/yyyy");
	String fecha_h = fecha_hoy.format(new java.util.Date());
		
	String infoRegresar =""; 
	String  ses_ic_epo =iNoCliente;
	String aforo = strAforo;
	String aforoDL = strAforoDL;
	
	HashMap datos = new HashMap();	
	JSONArray registros = new JSONArray();	
	HashMap  registrosTot = new HashMap();	
	JSONArray registrosTotales = new JSONArray();	
	JSONObject 	jsonObj	= new JSONObject();
	
	String  bOperaFactConMandato = "N",  bOperaFactorajeVencido  ="N", bOperaFactorajeDistribuido = "", 
	bOperaNotasCredito   ="N", bTipoFactoraje ="N",  bOperaFactorajeVencidoInfonavit  ="N",
	bValidaDuplicidad = "N"; //Fodea 042-2009 Vencimiento Infonavit
		
	try {
	Hashtable alParamEPO1 = new Hashtable();
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
	CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
	
	alParamEPO1 = BeanParamDscto.getParametrosEPO(ses_ic_epo,1);	
		
	if (alParamEPO1!=null) {
		bOperaFactConMandato = alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString();
		bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
		bOperaFactorajeDistribuido = alParamEPO1.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString();
		bOperaNotasCredito = alParamEPO1.get("OPERA_NOTAS_CRED").toString();
		bOperaFactorajeVencidoInfonavit =alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString();
		bValidaDuplicidad = alParamEPO1.get("VALIDA_DUPLIC").toString();
	}
	if(bOperaFactorajeVencido.equals("S") || bOperaFactorajeDistribuido.equals("S")  || bOperaFactConMandato.equals("S")) {
		bTipoFactoraje ="S";
	}
	
	boolean 	estaHabilitadoNumeroSIAFF 	= BeanParamDscto.estaHabilitadoNumeroSIAFF(ses_ic_epo);
	String bestaHabilitadoNumeroSIAFF ="N";
	if(estaHabilitadoNumeroSIAFF)		bestaHabilitadoNumeroSIAFF ="S";
	
	String 	sPubEPOPEFFlag	= new String(""),		sPubEPOPEFFlagVal	= new String(""),		sPubEPOPEFDocto1		= new String(""),
					sPubEPOPEFDocto1Val	= new String(""),		sPubEPOPEFDocto2	= new String(""),	sPubEPOPEFDocto2Val	= new String(""),
					sPubEPOPEFDocto3		= new String(""),	sPubEPOPEFDocto3Val		= new String(""),	sPubEPOPEFDocto4	= new String(""),	
					sPubEPOPEFDocto4Val		= new String(""),  sOperaFactConMandato   = new String("");
						
	ArrayList alParamEPO = BeanParamDscto.getParamEPO(ses_ic_epo, 1);
	if (alParamEPO!=null) {  
		//estos son los valores que se consultaron
		//este es el orden en el que me van a llegar los datos pues ordene la consulta por asc de nombre del campo
		sPubEPOPEFDocto3						= (alParamEPO.get(15)==null)?"":alParamEPO.get(15).toString();//PUB_EPO_PEF_CLAVE_PRESUPUESTAL
		sPubEPOPEFDocto3Val					= (alParamEPO.get(16)==null)?"":alParamEPO.get(16).toString();
		sPubEPOPEFDocto1						= (alParamEPO.get(17)==null)?"":alParamEPO.get(17).toString();//PUB_EPO_PEF_FECHA_ENTREGA
		sPubEPOPEFDocto1Val					= (alParamEPO.get(18)==null)?"":alParamEPO.get(18).toString();
		sPubEPOPEFFlag							= (alParamEPO.get(19)==null)?"":alParamEPO.get(19).toString();//PUB_EPO_PEF_FECHA_RECEPCION
		sPubEPOPEFFlagVal						= (alParamEPO.get(20)==null)?"":alParamEPO.get(20).toString();
		sPubEPOPEFDocto4						= (alParamEPO.get(21)==null)?"":alParamEPO.get(21).toString();//PUB_EPO_PEF_PERIODO
		sPubEPOPEFDocto4Val					= (alParamEPO.get(22)==null)?"":alParamEPO.get(22).toString();
		sPubEPOPEFDocto2						= (alParamEPO.get(23)==null)?"":alParamEPO.get(23).toString();//PUB_EPO_PEF_TIPO_COMPRA
		sPubEPOPEFDocto2Val					= (alParamEPO.get(24)==null)?"":alParamEPO.get(24).toString();
		sPubEPOPEFDocto2Val					= (alParamEPO.get(24)==null)?"":alParamEPO.get(24).toString();			
	}
	
	Vector vFechasVencimiento = CargaDocumentos.getFechasVencimiento(ses_ic_epo, 1);
	
	String fechaVencMin = "", fechaVencMax = "", spubdocto_venc = "",	 fechaPlazoPagoFVP = "", plazoMax1FVP  = "", plazoMax2FVP  = "";
	
	fechaVencMin = vFechasVencimiento.get(0).toString();
	fechaVencMax = vFechasVencimiento.get(1).toString();
	spubdocto_venc = (vFechasVencimiento.get(2)==null)?"":vFechasVencimiento.get(2).toString();
	fechaPlazoPagoFVP = (vFechasVencimiento.get(3)==null)?"":vFechasVencimiento.get(3).toString();
	plazoMax1FVP = (vFechasVencimiento.get(4)==null)?"":vFechasVencimiento.get(4).toString();
	plazoMax2FVP = (vFechasVencimiento.get(5)==null)?"":vFechasVencimiento.get(5).toString();
		
	Vector nombresCampo = new Vector(5);
	int numeroCampos= 0;

	if(ic_estatus_docto.equals(""))  ic_estatus_docto = "S";

	if(strPendiente.equals("S")){
		if(bOperaFactorajeVencido.equals("S") && ic_tipo_fact.equals("V")  ) {			
      ic_estatus_docto = "30";
		} else if(bOperaFactorajeDistribuido.equals("S") && ic_tipo_fact.equals("D") ) { 			
				ic_estatus_docto  = "30";
    } else if(bOperaFactorajeVencidoInfonavit.equals("S")  && ic_tipo_fact.equals("I") ) {  // Factoraje Vencimiento Infonavit
			ic_estatus_docto ="30";
		} else {
			if(ic_estatus_docto.equals("S")) {
				ic_estatus_docto ="30";
			}else if(ic_estatus_docto.equals("N")) {
				ic_estatus_docto ="31";
			}
		}
	}else if(strPreNegociable.equals("S")){
		if(bOperaFactorajeVencido.equals("S")  && ic_tipo_fact.equals("V")  ) {
			ic_estatus_docto = "28";
		} else if(bOperaFactorajeDistribuido.equals("S")  && ic_tipo_fact.equals("D") ) { 
			ic_estatus_docto = "28";
    } else if(bOperaFactorajeVencidoInfonavit.equals("S")  && ic_tipo_fact.equals("I")  ) {  // Factoraje Vencimiento Infonavit%
			ic_estatus_docto = "28";
    } else {
			if(ic_estatus_docto.equals("S")) {
				ic_estatus_docto ="28";
			}else if(ic_estatus_docto.equals("N")) {
				ic_estatus_docto ="29";
			}		
		}
  }else{
		if(bOperaFactorajeVencido.equals("S") && ic_tipo_fact.equals("V")  ) {
			ic_estatus_docto ="2";
  	} else if(bOperaFactorajeDistribuido.equals("S") && ic_tipo_fact.equals("D") ) { 
			ic_estatus_docto ="2";
    } else if(bOperaFactorajeVencidoInfonavit.equals("S") && ic_tipo_fact.equals("I") ) {  // Factoraje Vencimiento Infonavit				
			ic_estatus_docto ="2";
    } else {
			if(ic_estatus_docto.equals("S")) {
				ic_estatus_docto ="2";
			}else if(ic_estatus_docto.equals("N")) {
				ic_estatus_docto ="1";
			}		
  	}
  }
	
	String  nombrePyme ="", numeroDocumento ="", fechaDocumento ="",  fechaVencimiento ="", fechaVencimientoPyme ="", 
			monedaNombre ="",  monto ="", porcentajeAnticipo ="",  montoDescuento ="", referencia="",  consecutivo ="", 
			clavePyme ="",  estatusDocumento ="",  icEstatusDocumento ="",  tipoFactoraje ="", claveMoneda ="", fechaEntrega ="", 
			nombremandante ="",  factoraje ="", tipoCompra ="", clavePresupuestaria ="", periodo ="",NombreIF ="", 
			sNombreBeneficiario="",  sPorcentajeBeneficiario="",  sMontoBeneficiario="", campo1 ="", campo2 = "", campo3="",
			campo4="", campo5 ="", ic_monedad ="", numeroSIAFF ="",  consulta ="";		
		int  iNumMoneda=0,  numRegistrosMN = 0,  numRegistrosDL = 0;
		
		BigDecimal montoTotalMN = new BigDecimal("0.00");
		BigDecimal montoTotalDL = new BigDecimal("0.00");
		BigDecimal montoDescuentoMN = new BigDecimal("0.00");
		BigDecimal montoDescuentoDL = new BigDecimal("0.00");
		BigDecimal montoTotalMNNeg = new BigDecimal("0.00");
		BigDecimal montoTotalMNSinOpera = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoMNNeg = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoMNSinOpera = new BigDecimal("0.00");
		BigDecimal montoTotalDLNeg = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoDLNeg = new BigDecimal("0.00");
		BigDecimal montoTotalMNNoNeg = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoMNNoNeg = new BigDecimal("0.00");
		BigDecimal montoTotalDLNoNeg = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoDLNoNeg = new BigDecimal("0.00");	
		BigDecimal montoTotalDLSinOper = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoDLSinOper = new BigDecimal("0.00");
		
		nombresCampo = CargaDocumentos.getCamposAdicionales(iNoCliente);
		numeroCampos=  nombresCampo.size();
		//se obtienes los campos adicionales
		String strCampos  ="", nomCampo1 ="", nomCampo2 ="", nomCampo3="", nomCampo4="", nomCampo5 ="";
		for (int i=0; i<nombresCampo.size(); i++) { 			
			Vector lovRegistro = (Vector) nombresCampo.get(i);
			strCampos += (String) lovRegistro.get(0);	
			if(i==0) 	nomCampo1 = (String) lovRegistro.get(1);
			if(i==1) 	nomCampo2 = (String) lovRegistro.get(1);
			if(i==2) 	nomCampo3 = (String) lovRegistro.get(1); 
			if(i==3) 	nomCampo4 = (String) lovRegistro.get(1);
			if(i==4) 	nomCampo5 = (String) lovRegistro.get(1);			
		}		


/************************************************************************************/
//   Lista de  Catalogos 
/***********************************************************************************/
if(informacion.equals("CatalogoPyme")  ) {
	  
	CatalogoPYMED catalogo = new CatalogoPYMED();
	catalogo.setCampoClave("cp.ic_pyme");
	catalogo.setCampoDescripcion("pe.cg_pyme_epo_interno || ' ' || cp.cg_razon_social");
	catalogo.setGroupBy("S");
	catalogo.setClaveEpo(ses_ic_epo);	
	catalogo.setConNumeroProveedor(false); 
	infoRegresar = catalogo.getJSONElementos();		
		
} else  if (informacion.equals("CatalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	

} else  if (informacion.equals("CatalogoFactoraje")  ) {

	tipoFactoraje =  "N";
	if(bOperaFactConMandato.equals("S")) { 	 tipoFactoraje +=  ",M"; }
	if(bOperaFactConMandato.equals("S")) { 	 tipoFactoraje +=  ",M"; }
	if(bOperaFactorajeVencido.equals("S")) { tipoFactoraje +=  ",V"; }
	if(bOperaFactorajeDistribuido.equals("S")) { tipoFactoraje +=  ",D"; }
	if(bOperaNotasCredito.equals("S")) { tipoFactoraje +=  ",C"; }
	if(bOperaFactorajeVencidoInfonavit.equals("S")) { tipoFactoraje +=  ",I"; }
	
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("CC_TIPO_FACTORAJE");
	catalogo.setCampoDescripcion("CG_NOMBRE");
	catalogo.setTabla("COMCAT_TIPO_FACTORAJE");		
	catalogo.setOrden("CG_NOMBRE");
	catalogo.setValoresCondicionIn(tipoFactoraje, String.class);
	infoRegresar = catalogo.getJSONElementos();	
	

} else  if (informacion.equals("CatalogoTipoCompra")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("cc_clave");
	catalogo.setCampoDescripcion("cg_descripcion");
	catalogo.setTabla("comcat_tipo_compra");		
	catalogo.setOrden("cg_descripcion");	
	infoRegresar = catalogo.getJSONElementos();	

} else  if (informacion.equals("CatalogoIF")  ) {

	CatalogoIFDescuento catalogo = new CatalogoIFDescuento();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");		
	catalogo.setOrden("cg_razon_social");	
	catalogo.setClaveEpo(ses_ic_epo);	
	infoRegresar = catalogo.getJSONElementos();	

} else  if (informacion.equals("ValidaFactVencidoIF_EPO")  ) {

	System.out.println("  ses_ic_epo" + ses_ic_epo);
	System.out.println("  ic_if" + ic_if);
	

	String  operaFactVencido =  BeanParamDscto.getOperaFactVencido( ses_ic_epo, ic_if  );

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("operaFactVencido",operaFactVencido);		
	infoRegresar = jsonObj.toString();	


} else  if (informacion.equals("CatalogoMandante")  ) {

	try {
		AccesoDB con = new AccesoDB();
		con.conexionDB();
		String query = " select m.IC_MANDANTE, m.CG_RAZON_SOCIAL "+
									 " from COMCAT_MANDANTE m, COMREL_MANDANTE_EPO r " +
									 " WHERE m.IC_MANDANTE = r.IC_MANDANTE  and r.ic_epo ="+ses_ic_epo+
									 " order by m.CG_RAZON_SOCIAL ";
		PreparedStatement pstmt = null;
		pstmt = con.queryPrecompilado(query);
		ResultSet rs = pstmt.executeQuery();
		pstmt.clearParameters();
	
		while (rs.next()) {
			String clave = rs.getString(1);
			String descripcion = rs.getString(2);				
			datos = new HashMap();			
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);	
			registros.add(datos);	
		} 
		rs.close(); 

		} catch(Exception e) { 
			e.printStackTrace();
			out.println("Error: " + e.getMessage()); 
		}
	
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";


} else  if (informacion.equals("CatalogoBeneficiario")  ) {

	String numeroBeneficiario="", nombreBeneficiario="", sel="";
	Vector vComboBenef = CargaDocumentos.getComboBeneficiarios("",ses_ic_epo, "");
	
	for(int i=0;i<vComboBenef.size();i++){
		Vector vd = (Vector) vComboBenef.get(i);
		String clave = vd.get(0).toString();
		String descripcion = vd.get(1).toString();
		datos = new HashMap();			
		datos.put("clave", clave);
		datos.put("descripcion", descripcion);	
		registros.add(datos);							
	}
	infoRegresar =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
/************************************************************************************/
//   Para Obtener los campos adicionales  parametrizados  de la EPO
/***********************************************************************************/

} else  if (informacion.equals("valoresIniciales") ) {

	nombresCampo = CargaDocumentos.getCamposAdicionales(iNoCliente);	
	numeroCampos=  nombresCampo.size();	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("numeroCampos",String.valueOf(numeroCampos));	
	
	for(int i=0;i<nombresCampo.size();i++){
		Vector detalleCampo = (Vector) nombresCampo.get(i);
		
		int numCampo = Integer.parseInt((String)detalleCampo.get(0));
		String nombreCampo = (String) detalleCampo.get(1);
		String tipoDato = (String)detalleCampo.get(2);
		int longitudDato = Integer.parseInt((String)detalleCampo.get(3));
		String 	obligatorio = (String)detalleCampo.get(4);
		String tipoCobranza = (String)detalleCampo.get(5);			
		String tiposdDato ="";
		if(tipoDato.equals("A"))  {  tiposdDato = "textfield"; }
		if(tipoDato.equals("N"))  {  tiposdDato = "numberfield"; }		
		String idCampo = "cg_campo"+numCampo;
		String idCampoO = "cg_campoO"+numCampo;	
		
		String  campos ="	{ "+
				" xtype: 'compositefield', "+
				" fieldLabel: '"+nombreCampo+"',"+
				"	combineErrors: false,"+
				"	msgTarget: 'side',"+
				" width: 500,"+
				"	id: '"+idCampo+"',"+
				"	items: ["+
				"		{ "+
						"	xtype: '"+tiposdDato+"',"+
						"	name: '"+idCampo+"',"+
						"	id: '"+idCampo+"',"+
						"	fieldLabel: '"+nombreCampo+"',"+
									
						"	allowBlank: true, "+								
						"	maxLength: "+longitudDato+","+
						"	width: 150,"+
						"	msgTarget: 'side',"+							
						"	margins: '0 20 0 0'  "+
						" },	"+
						" { "+
						" xtype: 'displayfield',"+
						" value: '',"+
						" width: 150 "+
						" }	"+		
				" ]"+
			"} ";
			jsonObj.put(idCampo,campos.toString());	
			jsonObj.put(idCampoO,obligatorio.toString());	
				
	} //fin del for
	
	
	infoRegresar = jsonObj.toString();	
	
	
	

/************************************************************************************/
//   Captura  el registro en la tabla temporal 
/***********************************************************************************/

}else  if (informacion.equals("Valida_Captura")  ) {
	
	String msgError ="", 	ic_proc_docto ="", duplicidad  ="N";
	
	int numRegistros = 0;
	try {
		
		if(proceso.equals("")){
			ic_proc_docto = CargaDocumentos.getNumaxProcesoDocto();
		}else {
			ic_proc_docto = proceso;
		}
		
		if (!cg_pyme_epo_interno.equals("")) {	//Se especificó numero interno?
			ic_pyme = CargaDocumentos.getPyme(ses_ic_epo, cg_pyme_epo_interno);
		}
		
		if (ic_pyme.length()==0){ 
			
			msgError  = "La clave del proveedor no existe "+cg_pyme_epo_interno;	
		
			// Fodea 059 - 2010. Agregar documento a bitacora de errores en la publicacion
			CargaDocumentos.registraDoctoConErrorEnPublicacion(	ses_ic_epo,		cg_pyme_epo_interno,
							ig_numero_docto,	df_fecha_venc,	ic_moneda, 
							fn_monto, ic_tipo_fact, 	strNombreUsuario,							
							(String) session.getAttribute("strCorreoUsuario")		);		
		}
		
		
		
		if (ic_pyme.length()>0) {
		
			numRegistros = CargaDocumentos.existenDoctosCargadosTmp(ic_proc_docto, ig_numero_docto, df_fecha_docto, ic_moneda, ic_pyme);
			numRegistros = numRegistros + CargaDocumentos.existenDoctosCargados(ses_ic_epo, ig_numero_docto, df_fecha_docto, ic_moneda, ic_pyme);
			
			//NOTA: En teoria la publicacion de documentos Vencidos sin operar solo aplica para el Estatus Negociable, ya que para las epos con firma mancomunada 
			//      no aplica la publicacion de ese tipo de doctos debido a que su paramterización no se los permite
			if(!ic_estatus_docto.equals("1")	&&  !ic_estatus_docto.equals("29") && !ic_estatus_docto.equals("31")){ 
			if("S".equals(spubdocto_venc)){
				DateFormat myDateFormat = new SimpleDateFormat("dd/MM/yyyy");
				java.util.Date fecha_ven	= myDateFormat.parse(df_fecha_venc);
				DateFormat myDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
				java.util.Date fecha_min	= myDateFormat1.parse(hidFechaVencMin);
				DateFormat myDateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
				java.util.Date dFechhoy	= myDateFormat2.parse(fecha_h);
				if(fecha_ven.compareTo(fecha_min)<0 && fecha_ven.compareTo(dFechhoy)>=0){				
					ic_estatus_docto = "9";									
				}		
			}
			}
			
		System.out.println("strPendiente  "+strPendiente );
		System.out.println("strPreNegociable  "+strPreNegociable ); 
		
			//if(strPendiente.equals("N")  &&  strPreNegociable.equals("N")){
			
				if(!ic_estatus_docto.equals("1")	&&  !ic_estatus_docto.equals("9") && !ic_estatus_docto.equals("29") && !ic_estatus_docto.equals("31")){     //estatus diferente de  No Negociable y vencido sin  Operar 
					if( ic_tipo_fact.equals("N")  ||  ic_tipo_fact.equals("M")  ||  ic_tipo_fact.equals("A")  ||  ic_tipo_fact.equals("D")  ||  ic_tipo_fact.equals("I")   ||  ic_tipo_fact.equals("V") ) {
						if("S".equals(bValidaDuplicidad)){
						duplicidad =  CargaDocumentos.validaDoctoDuplicado(ses_ic_epo, df_fecha_docto, ic_moneda, ic_pyme,  fn_monto, ic_proc_docto); //Fodea 038-2014 			
						if( duplicidad.equals("S")){					 
							CargaDocumentos.insertaBitacoraDuplicadosCa(ig_numero_docto, df_fecha_docto, 
								ic_moneda,  fn_monto,  ses_ic_epo,  ic_pyme,  iNoUsuario,  strNombreUsuario,
								 ic_estatus_docto, ""); 
						}
						}
					
					}
				}			
			//}
		}		
			
	}catch(NafinException ne){
		ne.printStackTrace();	
		 msgError = ne.getMsgError();
	}catch(Exception e){
		e.printStackTrace();		
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("msgError",msgError);	
	jsonObj.put("proceso",ic_proc_docto);	
	jsonObj.put("duplicidad",duplicidad);
	jsonObj.put("ic_pyme",ic_pyme);	
	infoRegresar = jsonObj.toString();	


}else  if (informacion.equals("Captura")  ) {
	
	String msgError ="";
	
	try {
	
		ic_if = (ic_if.equals(""))?"NULL":ic_if;
		fn_porc_beneficiario = (fn_porc_beneficiario.equals(""))?"NULL":fn_porc_beneficiario;
		ic_beneficiario = (ic_beneficiario.equals(""))?"NULL":ic_beneficiario;
		
		if(!ic_estatus_docto.equals("1")	&&  !ic_estatus_docto.equals("29") && !ic_estatus_docto.equals("31")){ 
		if("S".equals(spubdocto_venc)){
			DateFormat myDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date fecha_ven	= myDateFormat.parse(df_fecha_venc);
			DateFormat myDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date fecha_min	= myDateFormat1.parse(hidFechaVencMin);
			DateFormat myDateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date dFechhoy	= myDateFormat2.parse(fecha_h);
			if(fecha_ven.compareTo(fecha_min)<0 && fecha_ven.compareTo(dFechhoy)>=0){				
				ic_estatus_docto = "9";									
			}					
		}
		}
		
		boolean bInserta = CargaDocumentos.insertaDocumento(
								proceso, ic_pyme, ig_numero_docto, df_fecha_docto, df_fecha_venc, ic_moneda,
								fn_monto, ic_tipo_fact, ct_referencia,	cg_campo1, cg_campo2, cg_campo3, 
								cg_campo4, cg_campo5, ic_if, 	ses_ic_epo, fn_porc_beneficiario, ic_beneficiario, 
								ic_estatus_docto,df_fecha_venc_pyme,df_fecha_entrega,cg_tipo_compra,cg_clave_presupuestaria,	cg_periodo,ic_mandante, duplicado );
								
	}catch(NafinException ne){
		ne.printStackTrace();	
		 msgError = ne.getMsgError();
	}catch(Exception e){
		e.printStackTrace();		
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("msgError",msgError);	
	jsonObj.put("proceso",proceso);		
	infoRegresar = jsonObj.toString();
/************************************************************************************/
//  Consulta  y resumen de totales de la table temporal
/***********************************************************************************/	
}else  if ( ( informacion.equals("Consulta") || informacion.equals("ResumenTotales") )   && pantalla.equals("Captura")  ) {
	
		//Despliegue de los documentos
		Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargados(proceso, aforo, ses_ic_epo, "", "", aforoDL, sesIdiomaUsuario, "'N','S'");
		
		System.out.println("vDoctosCargados>>>>>>>"+vDoctosCargados);
		
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();) {
			Vector vd 				= (Vector)e.nextElement();		
			 nombrePyme 				= vd.get(0).toString();
			 numeroDocumento 		= vd.get(1).toString();
			 fechaDocumento 			= vd.get(2).toString();
			 fechaVencimiento 		= vd.get(3)==null?"":vd.get(3).toString();			 
			 fechaVencimientoPyme 	= vd.get(24)==null?"":vd.get(24).toString();			
			 monedaNombre 			= vd.get(4).toString();
			 monto 					= vd.get(6).toString();
			 porcentajeAnticipo 		= vd.get(8).toString();
			 montoDescuento 			=  vd.get(9)==null?"0":vd.get(9).toString(); 			
			 referencia 				= vd.get(10).toString();
			 consecutivo 			= vd.get(12).toString();
			 clavePyme 				= vd.get(13).toString();
			 estatusDocumento 		= vd.get(14).toString();			 
			 campo1 = vd.get(15).toString();
			 campo2 = vd.get(16).toString();
			 campo3 = vd.get(17).toString();
			 campo4 = vd.get(18).toString();
			 campo5 = vd.get(19).toString();			 
			 icEstatusDocumento 		= vd.get(23).toString();
			tipoFactoraje 			= vd.get(7).toString();
			claveMoneda 			= vd.get(5).toString();
			fechaEntrega			= vd.get(25).toString();
			nombremandante			= vd.get(29).toString();
			factoraje			= vd.get(30).toString();
			clavePresupuestaria = vd.get(27).toString();			
			periodo = vd.get(28).toString();			
			ic_monedad = vd.get(5).toString();
			if(vd.get(26).toString().equals("L"))
				tipoCompra = "Licitación";
			if(vd.get(26).toString().equals("I"))
				tipoCompra = "Invitación";
			if(vd.get(26).toString().equals("D"))
				tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(26).toString().equals("C"))
				tipoCompra = "CAAS, CAAS delegado";			
			NombreIF 				= "";
			if(bOperaFactorajeVencido.equals("S") ) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo 	= "100"; // si factoraje vencido: se aplica 100% de anticipo
					montoDescuento 		= monto;		// y el monto del desc es igual al del docto.
					NombreIF 			= vd.get(11).toString();
				}
			}
			//Mandato Fodea 023 2009
			if(bOperaFactConMandato.equals("S") &&  tipoFactoraje.equals("M")) {					
				NombreIF 		= vd.get(11).toString();
			}
			
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario=""; 
			if(bOperaFactorajeDistribuido.equals("S")  ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo 		= "100";
					montoDescuento 			= monto;
					sNombreBeneficiario 	= vd.get(20).toString();
					sPorcentajeBeneficiario = vd.get(21).toString();
					sMontoBeneficiario 		= vd.get(22).toString();
				}
			}			
			//Fodea 042-2009 Vencimiento Infonavit
			if(bOperaFactorajeVencidoInfonavit.equals("S") &&  tipoFactoraje.equals("I")) {					
				NombreIF 		= vd.get(11).toString();
				porcentajeAnticipo 		= "100";
				montoDescuento 			= monto;
				sNombreBeneficiario 	= vd.get(20).toString();
				sPorcentajeBeneficiario = vd.get(21).toString();
				sMontoBeneficiario 		= vd.get(22).toString();
			}		
			
			if(tipoFactoraje.equals("N"))
				tipoFactoraje = "Normal";
			else if(tipoFactoraje.equals("M"))
				tipoFactoraje = "Mandato";
			else if(tipoFactoraje.equals("V"))
				tipoFactoraje = "Vencido";
			else if(tipoFactoraje.equals("D"))
				tipoFactoraje = "Distribuido";
			else if(tipoFactoraje.equals("C")) {
				tipoFactoraje = "Nota de Crédito";
				porcentajeAnticipo = "100";
				montoDescuento = monto;
			}
			if(montoDescuento.equals("") ) montoDescuento ="0";
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if (iNumMoneda==1) {
				numRegistrosMN++;
				if (monto!=null) {
					montoTotalMN = montoTotalMN.add(new BigDecimal(monto));
					montoDescuentoMN = montoDescuentoMN.add(new BigDecimal(montoDescuento));
          if(icEstatusDocumento.equals("2") || icEstatusDocumento.equals("30")|| icEstatusDocumento.equals("28")){  // Estatus 2 Negociable, Estatus 30 Pendiente Negociable, Estatus 28 Pre Negociable
						montoTotalMNNeg   = montoTotalMNNeg.add(new BigDecimal(monto));
						montoTotalDsctoMNNeg = montoTotalDsctoMNNeg.add(new BigDecimal(montoDescuento));
					} else if(icEstatusDocumento.equals("9")){
						montoTotalMNSinOpera   = montoTotalMNSinOpera.add(new BigDecimal(monto));
						montoTotalDsctoMNSinOpera = montoTotalDsctoMNSinOpera.add(new BigDecimal(montoDescuento));
					} else {
						montoTotalMNNoNeg = montoTotalMNNoNeg.add(new BigDecimal(monto));
						montoTotalDsctoMNNoNeg = montoTotalDsctoMNNoNeg.add(new BigDecimal(montoDescuento));
					}
				}
			} else if (iNumMoneda == 54) {//IC_MONEDA=54
				numRegistrosDL++;
				if (monto!=null) {
					montoTotalDL = montoTotalDL.add(new BigDecimal(monto));
					montoDescuentoDL = montoDescuentoDL.add(new BigDecimal(montoDescuento));
					if (icEstatusDocumento.equals("2") || icEstatusDocumento.equals("30")|| icEstatusDocumento.equals("28")){ //Estatus 2 Negociables, Estatus 30 Pendientes Negociables, Estatus 28 Pre Negociable
						montoTotalDLNeg   = montoTotalDLNeg.add(new BigDecimal(monto));
						montoTotalDsctoDLNeg = montoTotalDsctoDLNeg.add(new BigDecimal(montoDescuento));
					} else if(icEstatusDocumento.equals("9")) {
						montoTotalDLSinOper = montoTotalDLSinOper.add(new BigDecimal(monto));
						montoTotalDsctoDLSinOper = montoTotalDsctoDLSinOper.add(new BigDecimal(montoDescuento));
					} else {
						montoTotalDLNoNeg = montoTotalDLNoNeg.add(new BigDecimal(monto));
						montoTotalDsctoDLNoNeg = montoTotalDsctoDLNoNeg.add(new BigDecimal(montoDescuento));
					}
				}
			}
			duplicado = vd.get(31).toString();	 //F038-2104
			
			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("MANDANTE",nombremandante);	
			datos.put("CONSECUTIVO",consecutivo);	
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);		
			datos.put("DUPLICADO",duplicado);	
			registros.add(datos);	
			
		}//fin for
		
		for(int t =0; t<2; t++) {		
			registrosTot = new HashMap();		
		
			if(t==0){ 			
				if(strPendiente.equals("S")){
					registrosTot.put("MONEDA", "Total Pendientes Moneda Nacional:");		
				}else if(strPreNegociable.equals("S")){
					registrosTot.put("MONEDA", "Total Moneda Nacional Pre Negociable:");	
				}else{
					registrosTot.put("MONEDA", "Total Moneda Nacional:");	
				}					
				registrosTot.put("TOTAL", Double.toString(numRegistrosMN) );	
				registrosTot.put("TOTAL_MONTO", String.valueOf(montoTotalMN));	
				registrosTot.put("TOTAL_MONTO_DESC", String.valueOf(montoDescuentoMN)  );		
				registrosTot.put("IC_MONEDA", "1"  );	
			}		
			if(t==1){ 				
				if(strPendiente.equals("S")){
					registrosTot.put("MONEDA", "Total Pendientes Dólares:");		
				}else if(strPreNegociable.equals("S")){
					registrosTot.put("MONEDA", "Total Dólares Pre Negociable:");	
				}else{
					registrosTot.put("MONEDA", "Total Dólares:");	
				}						
				registrosTot.put("TOTAL", Double.toString(numRegistrosDL) );	
				registrosTot.put("TOTAL_MONTO", String.valueOf(montoTotalDL));	
				registrosTot.put("TOTAL_MONTO_DESC", String.valueOf(montoDescuentoDL) );
				registrosTot.put("IC_MONEDA", "54"  );
			}
				
			registrosTotales.add(registrosTot);
	}
		
		if( informacion.equals("Consulta")  ) {
			consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);
			jsonObj.put("hidMontoTotalMN", String.valueOf(montoTotalMN));
			jsonObj.put("hidNumDoctosActualesMN", Double.toString(numRegistrosMN));
			jsonObj.put("hidMontoTotalDL", String.valueOf(montoTotalDL));
			jsonObj.put("hidNumDoctosActualesDL",  Double.toString(numRegistrosDL));			
			jsonObj.put("hayCamposAdicionales", String.valueOf(numeroCampos));
			jsonObj.put("nomCampo1",String.valueOf(nomCampo1));
			jsonObj.put("nomCampo2",String.valueOf(nomCampo2));
			jsonObj.put("nomCampo3",String.valueOf(nomCampo3));
			jsonObj.put("nomCampo4",String.valueOf(nomCampo4));
			jsonObj.put("nomCampo5",String.valueOf(nomCampo5));			
			jsonObj.put("operaFVPyme",  operaFVPyme);
			jsonObj.put("bOperaFactorajeVencido",  bOperaFactorajeVencido);
			jsonObj.put("bOperaFactConMandato",  bOperaFactConMandato);
			jsonObj.put("bOperaFactorajeVencidoInfonavit",  bOperaFactorajeVencidoInfonavit);
			jsonObj.put("bOperaFactorajeDistribuido",  bOperaFactorajeDistribuido);
			jsonObj.put("sPubEPOPEFFlagVal",  sPubEPOPEFFlagVal);
			jsonObj.put("bOperaFactConMandato",  bOperaFactConMandato);		
			jsonObj.put("bValidaDuplicidad",  bValidaDuplicidad);	
			
		}
		if( informacion.equals("ResumenTotales")  ) {
			consulta =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registros\": " + registrosTotales.toString()+"}";
			jsonObj = JSONObject.fromObject(consulta);
		}
		infoRegresar  = jsonObj.toString();
		
		System.out.println("infoRegresar>>>>>>>"+infoRegresar);

}else if (informacion.equals("validaPymeBloqXNumProv")) {

	boolean bBloqueada = CargaDocumentos.validaPymeXEpoBloqXNumProv(cg_pyme_epo_interno, ses_ic_epo, "1"); 
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("pymeBloqueada", new Boolean(bBloqueada));
	infoRegresar = jsonObj.toString();
	
}else if (informacion.equals("validaPymeBloqueada")) {

	boolean bBloqueada = CargaDocumentos.validaPymeXEpoBloqueda(ses_ic_epo, ic_pyme, "1"); 
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("pymeBloqueada", new Boolean(bBloqueada));
	infoRegresar = jsonObj.toString();

/************************************************************************************/
//  Eliminación  de la tabla temporal
/***********************************************************************************/		
}else if (informacion.equals("Eliminar")) {	

	boolean bBorrOk = CargaDocumentos.borraDoctosCargados(noConsecutivo,"I",ses_ic_epo); //(I)ndividual

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("proceso", proceso);
	infoRegresar = jsonObj.toString();

/************************************************************************************/
//  Modificar  de la tabla temporal
/***********************************************************************************/		
}else if (informacion.equals("Modificar")) {	
	
	String  mod_ic_pyme ="", mod_ig_numero_docto ="", mod_df_fecha_docto ="", mod_df_fecha_venc ="", mod_ic_moneda="",
						mod_fn_monto="",  mod_ic_tipo_fact = "", mod_ic_estatus_docto ="",  mod_ct_referencia  ="", mod_ic_if ="", 
						mod_porc_beneficiario="",  mod_mto_beneficiario ="",  mod_ic_beneficiario ="", mod_df_fecha_venc_pyme ="", 
						mod_df_fecha_entrega ="", mod_cg_tipo_compra ="", mod_cg_clave_presupuestaria ="", mod_cg_periodo ="", 
						mod_NombreMandate  ="";

	Vector vDoctos = CargaDocumentos.getDoctosCargados(noConsecutivo, "I");	
	
	boolean bBorrOk = CargaDocumentos.borraDoctosCargados(noConsecutivo,"I",ses_ic_epo); //(I)ndividual

	if(vDoctos.size()>0) {				
		Vector vdi = (Vector)vDoctos.get(0);		
		mod_ic_pyme 			= vdi.get(0).toString();
		mod_ig_numero_docto 	= vdi.get(1).toString();
		mod_df_fecha_docto 		= vdi.get(2).toString();
		mod_df_fecha_venc 		= vdi.get(3).toString();
		mod_ic_moneda 			= vdi.get(4).toString();
		mod_fn_monto 			= vdi.get(5).toString();
		mod_ic_tipo_fact 		= vdi.get(6).toString();
		mod_ic_estatus_docto 	= vdi.get(7).toString();
		mod_ct_referencia 		= vdi.get(8).toString();
		mod_ic_if 				= vdi.get(9).toString();
		cg_campo1 				= vdi.get(10).toString();
		cg_campo2 				= vdi.get(11).toString();
		cg_campo3 				= vdi.get(12).toString();
		cg_campo4 				= vdi.get(13).toString();
		cg_campo5 				= vdi.get(14).toString();
		mod_porc_beneficiario 	= vdi.get(15).toString();
		mod_mto_beneficiario 	= (!mod_porc_beneficiario.equals(""))?Comunes.formatoDecimal((((new BigDecimal(mod_porc_beneficiario)).divide((new BigDecimal("100")),8,6)).multiply(new BigDecimal(mod_fn_monto))).toString(),2):"";
		mod_ic_beneficiario 	= vdi.get(16).toString();
		mod_df_fecha_venc_pyme	= vdi.get(17).toString();
		//FODEA 050 - VALC - 10/2008
		mod_df_fecha_entrega = 			vdi.get(18).toString();
		mod_cg_tipo_compra 	= 			vdi.get(19).toString();
		mod_cg_clave_presupuestaria =	vdi.get(20).toString();
		mod_cg_periodo = 					vdi.get(21).toString();
		mod_NombreMandate = vdi.get(22).toString();
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("mod_ic_pyme", mod_ic_pyme);
		jsonObj.put("mod_ig_numero_docto", mod_ig_numero_docto);
		jsonObj.put("mod_df_fecha_docto", mod_df_fecha_docto);
		jsonObj.put("mod_df_fecha_venc", mod_df_fecha_venc);
		jsonObj.put("mod_ic_moneda", mod_ic_moneda);
		jsonObj.put("mod_fn_monto", mod_fn_monto);
		jsonObj.put("mod_ic_tipo_fact", mod_ic_tipo_fact);
		jsonObj.put("mod_ic_estatus_docto", mod_ic_estatus_docto);
		jsonObj.put("mod_ct_referencia", mod_ct_referencia);
		jsonObj.put("mod_ic_if", mod_ic_if);
		jsonObj.put("cg_campo1", cg_campo1);
		jsonObj.put("cg_campo2", cg_campo2);
		jsonObj.put("cg_campo3", cg_campo3);
		jsonObj.put("cg_campo4", cg_campo4);
		jsonObj.put("cg_campo5", cg_campo5);
		jsonObj.put("mod_porc_beneficiario", mod_porc_beneficiario);
		jsonObj.put("mod_mto_beneficiario", mod_mto_beneficiario);
		jsonObj.put("mod_ic_beneficiario", mod_ic_beneficiario);
		jsonObj.put("mod_df_fecha_venc_pyme", mod_df_fecha_venc_pyme);
		jsonObj.put("mod_df_fecha_entrega", mod_df_fecha_entrega);
		jsonObj.put("mod_cg_tipo_compra", mod_cg_tipo_compra);		
		jsonObj.put("mod_cg_clave_presupuestaria", mod_cg_clave_presupuestaria);
		jsonObj.put("mod_cg_periodo", mod_cg_periodo);
		jsonObj.put("mod_NombreMandate", mod_NombreMandate);		
	}
	infoRegresar = jsonObj.toString();
	
/************************************************************************************/
//  Pantalla de Captura de Detalles 
/***********************************************************************************/		
}else if (informacion.equals("Detalles")) {	

	Vector vCamposDet = CargaDocumentos.getCamposDetalle(ses_ic_epo);	
	numeroCampos  = vCamposDet.size(); 
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("numeroCampos",String.valueOf(numeroCampos));	
	
	for(int d=0; d<vCamposDet.size(); d++) {
		Vector vcd = (Vector)vCamposDet.get(d);
		int numCampo = Integer.parseInt(vcd.get(0).toString());
		String nombreCampo = vcd.get(1).toString();
		String tipoDato = vcd.get(2).toString();
		int longitudDato = Integer.parseInt(vcd.get(3).toString());			
		String tiposdDato ="";
		if(tipoDato.equals("A"))  {  tiposdDato = "textfield"; }
		if(tipoDato.equals("N"))  {  tiposdDato = "numberfield"; }		
		String idCampo = "campo"+numCampo;	
			
		String  campos ="	{ "+
				" xtype: 'compositefield', "+
				" fieldLabel: '"+nombreCampo+"',"+
				"	combineErrors: false,"+
				"	msgTarget: 'side',"+
				" width: 500,"+
				"	id: '"+idCampo+"',"+
				"	items: ["+
				"		{ "+
						"	xtype: '"+tiposdDato+"',"+
						"	name: '"+idCampo+"',"+
						"	id: '"+idCampo+"',"+
						"	fieldLabel: '"+nombreCampo+"',"+
						"	allowBlank: true, "+								
						"	maxLength: "+longitudDato+","+
						"	width: 150,"+
						"	msgTarget: 'side',"+							
						"	margins: '0 20 0 0'  "+
						" },	"+
						" { "+
						" xtype: 'displayfield',"+
						" value: '',"+
						" width: 150 "+
						" }	"+		
				" ]"+
			"} ";
			
			jsonObj.put(idCampo,campos.toString());	
		} //fin del for	
		
		jsonObj.put("ig_numero_docto",ig_numero_docto);
		jsonObj.put("ic_pyme",ic_pyme);	
		jsonObj.put("ic_moneda",ic_moneda);	
		jsonObj.put("df_fecha_docto",df_fecha_docto);	
		
		infoRegresar = jsonObj.toString();	
/************************************************************************************/
//  realiza la captura de los campos del Detalle del Documento
/***********************************************************************************/		

}else if (informacion.equals("CapturaDetalle")) {	

	campo1= (request.getParameter("campo1") != null) ? request.getParameter("campo1") : "";
	campo2= (request.getParameter("campo2") != null) ? request.getParameter("campo2") : "";
	campo3= (request.getParameter("campo3") != null) ? request.getParameter("campo3") : "";
	campo4= (request.getParameter("campo4") != null) ? request.getParameter("campo4") : "";
	campo5= (request.getParameter("campo5") != null) ? request.getParameter("campo5") : "";
	String campo6= (request.getParameter("campo6") != null) ? request.getParameter("campo6") : "";
	String campo7= (request.getParameter("campo7") != null) ? request.getParameter("campo7") : "";
	String campo8= (request.getParameter("campo8") != null) ? request.getParameter("campo8") : "";
	String campo9= (request.getParameter("campo9") != null) ? request.getParameter("campo9") : "";
	String campo10= (request.getParameter("campo10") != null) ? request.getParameter("campo10") : "";
		
	
	CargaDocumentos.insertaDocumentoDetalle(proceso, ic_pyme, ig_numero_docto, 
			null, df_fecha_docto, ic_moneda,	campo1, campo2, campo3, campo4,
			campo5, campo6, campo7, campo8,	campo9, campo10);
														
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("proceso", proceso);
	jsonObj.put("ic_pyme", ic_pyme);
	jsonObj.put("ig_numero_docto", ig_numero_docto);
	jsonObj.put("df_fecha_docto", df_fecha_docto);
	jsonObj.put("ic_moneda", ic_moneda);
	
	infoRegresar = jsonObj.toString();

/************************************************************************************/
//  consulta los registros capturados
//	el Gid se forma desde aqui ya que son campos dinamicos
/***********************************************************************************/	

}else if (informacion.equals("ConsultaDetalle")) {	

	StringBuffer 	columnasGrid	= new StringBuffer();
	StringBuffer 	columnasStore	= new StringBuffer();
	StringBuffer 	columnasRecords	= new StringBuffer();
	
	//Se Agregran los Titulos */
	Vector vCamposDet = CargaDocumentos.getCamposDetalle(ses_ic_epo);	
	
	columnasGrid.append("	[  ");

	columnasGrid.append("  {  ");
	columnasGrid.append(" header: 'PROCESO', ");
	columnasGrid.append(" tooltip: 'PROCESO', ");						
	columnasGrid.append(" width: 130, ");
	columnasGrid.append(" align: 'center', ");
	columnasGrid.append(" hidden: true, ");		
	columnasGrid.append("	dataIndex: 'PROCESO' ");
	columnasGrid.append(" } , ");
	
	columnasGrid.append("  {  ");
	columnasGrid.append(" header: 'CONSECUTIVO', ");
	columnasGrid.append(" tooltip: 'CONSECUTIVO', ");						
	columnasGrid.append(" width: 130, ");
	columnasGrid.append(" align: 'center', ");	
	columnasGrid.append(" hidden: true, ");	
	columnasGrid.append("	dataIndex: 'CONSECUTIVO' ");
	columnasGrid.append(" } , ");
	
	columnasGrid.append("  {  ");
	columnasGrid.append(" header: 'FECHA', ");
	columnasGrid.append(" tooltip: 'FECHA', ");						
	columnasGrid.append(" width: 130, ");
	columnasGrid.append(" align: 'center', ");	
	columnasGrid.append(" hidden: true, ");	
	columnasGrid.append("	dataIndex: 'FECHA' ");
	columnasGrid.append(" } , ");
	
	columnasGrid.append("  {  ");
	columnasGrid.append(" header: 'IC_MONEDA', ");
	columnasGrid.append(" tooltip: 'IC_MONEDA', ");						
	columnasGrid.append(" width: 130, ");
	columnasGrid.append(" align: 'center', ");	
	columnasGrid.append(" hidden: true, ");	
	columnasGrid.append("	dataIndex: 'IC_MONEDA' ");
	columnasGrid.append(" } , ");
	
	columnasGrid.append("  {  ");
	columnasGrid.append(" header: 'IC_PYME', ");
	columnasGrid.append(" tooltip: 'IC_PYME', ");						
	columnasGrid.append(" width: 130, ");
	columnasGrid.append(" align: 'center', ");	
	columnasGrid.append(" hidden: true, ");	
	columnasGrid.append("	dataIndex: 'IC_PYME' ");
	columnasGrid.append(" } , ");
	
	columnasStore.append("  [    ");	
	columnasStore.append("  {  name: 'NUM_DOCTO',  mapping : 'NUM_DOCTO' }, ");
	columnasStore.append("  {  name: 'CONSECUTIVO',  mapping : 'CONSECUTIVO' }, ");
	columnasStore.append("  {  name: 'FECHA',  mapping : 'FECHA' }, ");
	columnasStore.append("  {  name: 'IC_MONEDA',  mapping : 'IC_MONEDA' }, ");
	columnasStore.append("  {  name: 'IC_PYME',  mapping : 'IC_PYME' }, ");
	columnasStore.append("  {  name: 'PROCESO',  mapping : 'PROCESO' }, ");
	
	
	for(int d=0; d<vCamposDet.size(); d++) {
		Vector vcd = (Vector)vCamposDet.get(d);
		int numCampo = Integer.parseInt(vcd.get(0).toString());
		String nombreCampo = vcd.get(1).toString();	
		String idCampo = "campo"+numCampo;
		
		columnasStore.append(" {  name: '"+idCampo+"',  mapping : '"+idCampo+"' },");
		
		columnasGrid.append("	{ 	header: '"+nombreCampo+"', ");
		columnasGrid.append("		tooltip: '"+nombreCampo+"', ");
		columnasGrid.append("		dataIndex: '"+idCampo+"', ");
		columnasGrid.append("		sortable: true, ");
		columnasGrid.append("		resizable: true	, ");
		columnasGrid.append("		width: 130,		 ");		
		columnasGrid.append("		align: 'left' ");
		columnasGrid.append("	},");	
		
	} //fin del for	
	
	columnasGrid.append(" 	{  ");
	columnasGrid.append(" xtype: 'actioncolumn', ");
	columnasGrid.append(" header: 'ELIMINAR', ");
	columnasGrid.append(" tooltip: 'ELIMINAR', ");						
	columnasGrid.append(" width: 130, ");
	columnasGrid.append(" align: 'center' ");					
	columnasGrid.append(" , items: [ ");
	columnasGrid.append("	{ ");
	columnasGrid.append("		getClass: function(valor, metadata, registro, rowIndex, colIndex, store) { ");
	columnasGrid.append("			this.items[0].tooltip = 'Eliminar'; ");
	columnasGrid.append("			return 'borrar';		");			 					
	columnasGrid.append("		}	");
	columnasGrid.append(" , handler:  eliminarDetalle  ");											
	columnasGrid.append(" }	");
	columnasGrid.append(" ]		");		
	columnasGrid.append(" }, ");
	
	columnasGrid.append("  {  ");
	columnasGrid.append(" xtype: 'actioncolumn', ");
	columnasGrid.append(" header: 'MODIFICAR', ");
	columnasGrid.append(" tooltip: 'MODIFICAR', ");						
	columnasGrid.append(" width: 130, ");
	columnasGrid.append(" align: 'center' ");					
	columnasGrid.append(" , items: [ ");
	columnasGrid.append("	{ ");
	columnasGrid.append("		getClass: function(valor, metadata, registro, rowIndex, colIndex, store) { ");
	columnasGrid.append("			this.items[0].tooltip = 'Modificar'; ");
	columnasGrid.append("			return 'modificar';		");			 					
	columnasGrid.append("		}	");
	columnasGrid.append(" , handler:  modificarDetalle  ");											
	columnasGrid.append(" }	");
	columnasGrid.append(" ]		");		
	columnasGrid.append(" } ");
		
	columnasGrid.append("	] ");				
		
	columnasStore.append(" {  name: 'MODIFICAR',  mapping : 'MODIFICAR' },");
	columnasStore.append(" {  name: 'ELIMINAR',  mapping : 'ELIMINAR' } ");	
	columnasStore.append("	] ");
	
	//* Se Agregran los  valores de los registros editables y  no editables  */
	Vector vDocDet = CargaDocumentos.getDoctosDetalles(proceso, ig_numero_docto, df_fecha_docto, ic_moneda, ic_pyme);
	
	columnasRecords.append("  [   ");
	for(int j=0; j<vDocDet.size(); j++) {
		Vector vdd = (Vector)vDocDet.get(j);	
		noConsecutivo = vdd.get(0).toString();
		
		columnasRecords.append(" { ");
		columnasRecords.append("PROCESO"+":'"+proceso+"',");
		columnasRecords.append("NUM_DOCTO"+":'"+ig_numero_docto+"',"); 
		columnasRecords.append("CONSECUTIVO"+":'"+noConsecutivo+"',"); 
		columnasRecords.append("FECHA"+":'"+df_fecha_docto+"',"); 
		columnasRecords.append("IC_MONEDA"+":'"+ic_moneda+"',"); 	
		columnasRecords.append("IC_PYME"+":'"+ic_pyme+"',"); 	
			
		int x=0;
		for(int d=0; d<vCamposDet.size(); d++) {	
			x++;
			Vector vcd = (Vector)vCamposDet.get(d);				
			int numCampo = Integer.parseInt(vcd.get(0).toString());
			String idCampo = "campo"+numCampo;	
			
			columnasRecords.append(idCampo+":'"+vdd.get(x)+"',"); //aqui agrego el valor de regitros 		
		}			
		columnasRecords.append(" },");	
	}
	columnasRecords.deleteCharAt(columnasRecords.length()-1);
	columnasRecords.append("   ] ");	
		
	
	infoRegresar = "{\"success\": true, \"columnasGrid\": " + columnasGrid.toString() + 
				", \"columnasStore\": " + columnasStore.toString() +			
				", \"columnasRecords\": {\"registros\": " + columnasRecords.toString() +
				", \"total\":"+vDocDet.size()+" }  }";
	
	
/************************************************************************************/
//  se elimina registro de del Detalle  de documentos 
/***********************************************************************************/	

}else if (informacion.equals("EliminarDetalle")) {	

	CargaDocumentos.borraDoctosDetalleCargados(noConsecutivo, "I");
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("proceso", proceso);	
	jsonObj.put("ig_numero_docto",  ig_numero_docto);
	jsonObj.put("df_fecha_docto",  df_fecha_docto);
	jsonObj.put("ic_moneda",  ic_moneda);
	jsonObj.put("ic_pyme",  ic_pyme);
	infoRegresar  = jsonObj.toString();
		

}else if (informacion.equals("ModificarDetalle")) {	
	
	Vector vCamposDet = CargaDocumentos.getCamposDetalle(ses_ic_epo);	
	Vector mod_cg_campo = new Vector(10);	
	mod_cg_campo = CargaDocumentos.getDoctosDetalle(noConsecutivo);
	
	jsonObj.put("success", new Boolean(true));
	for(int d=0; d<vCamposDet.size(); d++) {
	
		Vector vcd = (Vector)vCamposDet.get(d);
		int numCampo = Integer.parseInt(vcd.get(0).toString());
		String idCampo = "campo"+numCampo;
		String datosD = mod_cg_campo.get(numCampo-1).toString();	
		jsonObj.put(idCampo, datosD);	
	
	}
	//se elimina el registro capturado 
	CargaDocumentos.borraDoctosDetalleCargados(noConsecutivo, "I");
	
	jsonObj.put("proceso", proceso);	
	jsonObj.put("ig_numero_docto",  ig_numero_docto);
	jsonObj.put("df_fecha_docto",  df_fecha_docto);
	jsonObj.put("ic_moneda",  ic_moneda);
	jsonObj.put("ic_pyme",  ic_pyme);
	
	infoRegresar = jsonObj.toString();

/************************************************************************************/
//  Para la pantalla de PreAcuse
/***********************************************************************************/	
}else if (  ( informacion.equals("ConsultaPreAcuse1") 
			|| informacion.equals("ConsultaPreAcuse2") 
			|| informacion.equals("ConsultaPreAcuse3") 	 	)  			
			&& pantalla.equals("PreAcuse") ) {
	
	if( informacion.equals("ConsultaPreAcuse1") ) {
	
		//Despliegue de los documentos
	 if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "30"; // Pendientes Negociables
		else if(strPreNegociable.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "28"; // Pendientes Negociables
		else
			ic_estatus_docto = "2"; // Negociables	
		Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargados(proceso, strAforo, ses_ic_epo, ic_estatus_docto, "", strAforoDL, sesIdiomaUsuario, "'N','S'");		   
	
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{
			Vector vd 				= (Vector)e.nextElement();
			nombrePyme 				= vd.get(0).toString();
			numeroDocumento 		= vd.get(1).toString().replace('"',' ').trim();
			fechaDocumento 			= vd.get(2).toString().replace('"',' ').trim();		
			fechaVencimiento	= vd.get(3)==null?"":vd.get(3).toString();	
			fechaVencimientoPyme	= vd.get(24)==null?"":vd.get(24).toString();				
			monedaNombre 			= vd.get(4).toString();
			monto 					= vd.get(6).toString().replace('"',' ').trim();
			porcentajeAnticipo 		= vd.get(8).toString().replace('"',' ').trim();
			montoDescuento 			= vd.get(9).toString().replace('"',' ').trim();
			referencia 				= vd.get(10).toString().replace('"',' ').trim();
			tipoFactoraje 			= vd.get(7).toString().replace('"',' ').trim();
			nombremandante              = vd.get(29).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje             = vd.get(30).toString().replace('"',' ').trim(); // FODEA 023 Mandato	
			clavePresupuestaria = vd.get(27).toString();
			estatusDocumento =   vd.get(14).toString();			
			periodo = vd.get(28).toString();		
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();	
			NombreIF 				= "";
			if(bOperaFactorajeVencido.equals("S") ) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo 	= "100"; // si factoraje vencido: se aplica 100% de anticipo
					montoDescuento 		= monto;		// y el monto del desc es igual al del docto.
					NombreIF 			= vd.get(11).toString().replace('"',' ').trim();
				}
			}
			//Mandato Fodea 023 2009
			if(bOperaFactConMandato.equals("S")  && tipoFactoraje.equals("M")) {					
				NombreIF 		= vd.get(11).toString();
			}
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S")   ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo 		= "100";
					montoDescuento 			= monto;
					sNombreBeneficiario 	= vd.get(20).toString().replace('"',' ').trim();
					sPorcentajeBeneficiario = vd.get(21).toString().replace('"',' ').trim();
					sMontoBeneficiario 		= vd.get(22).toString().replace('"',' ').trim();
				}
			}
			//Fodea 042-2009 Vencimiento Infonavit
			if(bOperaFactorajeVencidoInfonavit.equals("S")  && tipoFactoraje.equals("I")) {					
				NombreIF 		= vd.get(11).toString();
				porcentajeAnticipo 		= "100";
				montoDescuento 			= monto;
				sNombreBeneficiario 	= vd.get(20).toString().replace('"',' ').trim();
				sPorcentajeBeneficiario = vd.get(21).toString().replace('"',' ').trim();
				sMontoBeneficiario 		= vd.get(22).toString().replace('"',' ').trim();
			}
			//FODEA 050 - VALC - 10/2008			
			fechaEntrega			= vd.get(25).toString();
			tipoCompra = "";
			if(vd.get(26).toString().equals("L")) 	tipoCompra = "Licitación";
			if(vd.get(26).toString().equals("I")) tipoCompra = "Invitación";
			if(vd.get(26).toString().equals("D")) tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(26).toString().equals("C")) tipoCompra = "CAAS, CAAS delegado";
				
			if(tipoFactoraje.equals("N")) 	    tipoFactoraje = "Normal";
			else if(tipoFactoraje.equals("M")) 	tipoFactoraje = "Mandato";	
			else if(tipoFactoraje.equals("V")) 	tipoFactoraje ="Vencido";
			else if(tipoFactoraje.equals("D")) 	tipoFactoraje = "Distribuido";
			else if(tipoFactoraje.equals("C")) {
				tipoFactoraje = "Nota Credito";
				porcentajeAnticipo = "100";
				montoDescuento = monto;
			}
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if (iNumMoneda==1) {
				numRegistrosMN++;
			} else {//IC_MONEDA=54
				numRegistrosDL++;				
			}
			duplicado = vd.get(31).toString(); //F038-2014
			
			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("MANDANTE",nombremandante);	
			datos.put("CONSECUTIVO",consecutivo);	
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);		
			datos.put("DUPLICADO",duplicado);//F038-2014
			
			registros.add(datos);	
		}//for
		
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		
	} else if( informacion.equals("ConsultaPreAcuse2") ) {
	
		if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "31"; // Pendientes Negociables
    else if(strPreNegociable.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "29"; // Pendientes Negociables
    else 
			ic_estatus_docto = "1"; // Negociables
		
		Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargados(proceso, strAforo, ses_ic_epo, ic_estatus_docto, "", strAforoDL, sesIdiomaUsuario, "'N','S'");		
		
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{
			Vector vd 				= (Vector)e.nextElement();
			nombrePyme 				= vd.get(0).toString();
			numeroDocumento			= vd.get(1).toString();
			fechaDocumento 			= vd.get(2).toString();			
			fechaVencimiento	= vd.get(3)==null?"":vd.get(3).toString();			
			fechaVencimientoPyme	= vd.get(24)==null?"":vd.get(24).toString();
			monedaNombre 			= vd.get(4).toString();
			monto 					= vd.get(6).toString();
			porcentajeAnticipo 		= vd.get(8).toString();
			montoDescuento 			= vd.get(9).toString();
			referencia 				= vd.get(10).toString();
			tipoFactoraje 			= vd.get(7).toString();
			nombremandante    = vd.get(29).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje             = vd.get(30).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			fechaEntrega			= vd.get(25).toString();
			clavePresupuestaria = vd.get(27).toString();
			periodo = vd.get(28).toString();
			estatusDocumento =   vd.get(14).toString();	
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();	
			NombreIF 		= "";
			if(bOperaFactorajeVencido.equals("S") ) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo 	= "100"; // si factoraje vencido: se aplica 100% de anticipo
					montoDescuento 		= monto;		// y el monto del desc es igual al del docto.
					NombreIF 			= vd.get(11).toString();
				}
			}
			//Mandato Fodea 023 2009
			if(bOperaFactConMandato.equals("S") && tipoFactoraje.equals("M")) {					
				NombreIF 		= vd.get(11).toString();
			}				
			//FODEA 050 - VALC - 10/2008
		
			if(vd.get(26).toString().equals("L")) 	tipoCompra = "Licitación";
			if(vd.get(26).toString().equals("I"))  tipoCompra = "Invitación";
			if(vd.get(26).toString().equals("D")) 	tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(26).toString().equals("C")) tipoCompra = "CAAS, CAAS delegado";
			
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S") ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo 		= "100";
					montoDescuento 			= monto;
					sNombreBeneficiario 	= vd.get(20).toString();
					sPorcentajeBeneficiario = vd.get(21).toString();
					sMontoBeneficiario 		= vd.get(22).toString();
				}
			}
			
		  tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": (tipoFactoraje.equals("V"))?"Vencido" : 	
			(tipoFactoraje.equals("D"))?"Distribuido": (tipoFactoraje.equals("C"))?"Nota Credito": 
			(tipoFactoraje.equals("M"))?"Mandato": "";
		
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if (iNumMoneda==1) {
				numRegistrosMN++;				
			} else {//IC_MONEDA=54
				numRegistrosDL++;				
			}
			
			duplicado = vd.get(31).toString(); //F038-2014
			
			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("MANDANTE",nombremandante);	
			datos.put("CONSECUTIVO",consecutivo);	
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);		
			datos.put("DUPLICADO",duplicado); //F038-2014

			registros.add(datos);	
			
		}//for
			
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
	
	} else if( informacion.equals("ConsultaPreAcuse3")   ) {

		Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargados(proceso, strAforo, ses_ic_epo, "9", "", strAforoDL, sesIdiomaUsuario, "'N','S'");
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{
			Vector vd 				= (Vector)e.nextElement();
			nombrePyme 				= vd.get(0).toString();
			numeroDocumento			= vd.get(1).toString();
			fechaDocumento 			= vd.get(2).toString();
			fechaVencimiento	= vd.get(3)==null?"":vd.get(3).toString();
			fechaVencimientoPyme	= vd.get(24)==null?"":vd.get(24).toString();			
			monedaNombre 			= vd.get(4).toString();
			monto 					= vd.get(6).toString();
			porcentajeAnticipo 		= vd.get(8).toString();
			montoDescuento 			= vd.get(9).toString();
			referencia 				= vd.get(10).toString();
			tipoFactoraje 			= vd.get(7).toString();
			nombremandante              = vd.get(29).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje             = vd.get(30).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			fechaEntrega			= vd.get(25).toString();
			clavePresupuestaria = vd.get(27).toString();
			periodo = vd.get(28).toString();
			estatusDocumento =   vd.get(14).toString();	
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();	
			
			NombreIF 				= "";
			if(bOperaFactorajeVencido.equals("S") ) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo 	= "100"; // si factoraje vencido: se aplica 100% de anticipo
					montoDescuento 		= monto;		// y el monto del desc es igual al del docto.
					NombreIF 			= vd.get(11).toString();
				}
			}
			//Mandato Fodea 023 2009
			if(bOperaFactConMandato.equals("S") && tipoFactoraje.equals("M")) {					
				NombreIF 		= vd.get(11).toString();
			}
			//FODEA 050 - VALC - 10/2008
			tipoCompra = "";
			if(vd.get(26).toString().equals("L")) tipoCompra = "Licitación";
			if(vd.get(26).toString().equals("I")) tipoCompra = "Invitación";
			if(vd.get(26).toString().equals("D"))	tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(26).toString().equals("C")) tipoCompra = "CAAS, CAAS delegado";
			
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S") ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D") )  {
					porcentajeAnticipo 		= "100";
					montoDescuento 			= monto;
					sNombreBeneficiario 	= vd.get(20).toString();
					sPorcentajeBeneficiario = vd.get(21).toString();
					sMontoBeneficiario 		= vd.get(22).toString();
				}
			}
			// Fodea 042-2009 Vencimiento Infonavit
			if(bOperaFactorajeVencidoInfonavit.equals("S") && tipoFactoraje.equals("I")) {					
				NombreIF 		= vd.get(11).toString();
				porcentajeAnticipo 		= "100";
				montoDescuento 			= monto;
				sNombreBeneficiario 	= vd.get(20).toString();
				sPorcentajeBeneficiario = vd.get(21).toString();
				sMontoBeneficiario 		= vd.get(22).toString();				
			}						
			tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": (tipoFactoraje.equals("V"))?"Vencido" : 	
								 (tipoFactoraje.equals("D"))?"Distribuido" : (tipoFactoraje.equals("C"))?"Nota Credito":
								 (tipoFactoraje.equals("M"))?"Mandato": "";			
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if (iNumMoneda==1) {
				numRegistrosMN++;
		
			} else {//IC_MONEDA=54
				numRegistrosDL++;
			}

			duplicado = vd.get(31).toString(); //F038-2014

			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("MANDANTE",nombremandante);	
			datos.put("CONSECUTIVO",consecutivo);	
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);
			datos.put("DUPLICADO",duplicado); //F038-2014

			registros.add(datos);				
		}//for
		
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	}
			jsonObj = JSONObject.fromObject(consulta);
			jsonObj.put("hidMontoTotalMN", String.valueOf(montoTotalMN));
			jsonObj.put("hidNumDoctosActualesMN", Double.toString(numRegistrosMN));
			jsonObj.put("hidMontoTotalDL", String.valueOf(montoTotalDL));
			jsonObj.put("hidNumDoctosActualesDL",  Double.toString(numRegistrosDL));			
			jsonObj.put("hayCamposAdicionales", String.valueOf(numeroCampos));
			jsonObj.put("nomCampo1",String.valueOf(nomCampo1));
			jsonObj.put("nomCampo2",String.valueOf(nomCampo2));
			jsonObj.put("nomCampo3",String.valueOf(nomCampo3));
			jsonObj.put("nomCampo4",String.valueOf(nomCampo4));
			jsonObj.put("nomCampo5",String.valueOf(nomCampo5));			
			jsonObj.put("operaFVPyme",  operaFVPyme);
			jsonObj.put("bOperaFactorajeVencido",  bOperaFactorajeVencido);
			jsonObj.put("bOperaFactConMandato",  bOperaFactConMandato);
			jsonObj.put("bOperaFactorajeVencidoInfonavit",  bOperaFactorajeVencidoInfonavit);
			jsonObj.put("bOperaFactorajeDistribuido",  bOperaFactorajeDistribuido);
			jsonObj.put("sPubEPOPEFFlagVal",  sPubEPOPEFFlagVal);
			jsonObj.put("bOperaFactConMandato",  bOperaFactConMandato);
			jsonObj.put("bValidaDuplicidad",  bValidaDuplicidad);
	
		infoRegresar  = jsonObj.toString();		

}else  if( ( informacion.equals("ControlTotales")   || informacion.equals("TotalesPreAcuse1") ) && pantalla.equals("PreAcuse")  ) {
	//Despliegue de los documentos
	Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargados(proceso, aforo, ses_ic_epo, "", "", aforoDL, sesIdiomaUsuario, "'N','S'");
	for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();) {
		Vector vd 				= (Vector)e.nextElement();
		monto 					= vd.get(6).toString();
		porcentajeAnticipo 		= vd.get(8).toString();
		montoDescuento 			= vd.get(9).toString();
		iNumMoneda = Integer.parseInt(vd.get(5).toString());
		icEstatusDocumento 		= vd.get(23).toString();
		if(montoDescuento.equals("") ) montoDescuento ="0";
		if(porcentajeAnticipo.equals("") ) porcentajeAnticipo ="0";
		
		if (iNumMoneda==1) {
			numRegistrosMN++;
			if (monto!=null) {
				montoTotalMN = montoTotalMN.add(new BigDecimal(monto));
				montoDescuentoMN = montoDescuentoMN.add(new BigDecimal(montoDescuento));
				if(icEstatusDocumento.equals("2") || icEstatusDocumento.equals("30")|| icEstatusDocumento.equals("28")){  // Estatus 2 Negociable, Estatus 30 Pendiente Negociable, Estatus 28 Pre Negociable
					montoTotalMNNeg   = montoTotalMNNeg.add(new BigDecimal(monto));
					montoTotalDsctoMNNeg = montoTotalDsctoMNNeg.add(new BigDecimal(montoDescuento));
				} else if(icEstatusDocumento.equals("9")){
					montoTotalMNSinOpera   = montoTotalMNSinOpera.add(new BigDecimal(monto));
					montoTotalDsctoMNSinOpera = montoTotalDsctoMNSinOpera.add(new BigDecimal(montoDescuento));
				} else {
					montoTotalMNNoNeg = montoTotalMNNoNeg.add(new BigDecimal(monto));
					montoTotalDsctoMNNoNeg = montoTotalDsctoMNNoNeg.add(new BigDecimal(montoDescuento));
				}
			}
		} else if (iNumMoneda == 54) {//IC_MONEDA=54
			numRegistrosDL++;
			if (monto!=null) {
				montoTotalDL = montoTotalDL.add(new BigDecimal(monto));
				montoDescuentoDL = montoDescuentoDL.add(new BigDecimal(montoDescuento));
				if (icEstatusDocumento.equals("2") || icEstatusDocumento.equals("30")|| icEstatusDocumento.equals("28")){ //Estatus 2 Negociables, Estatus 30 Pendientes Negociables, Estatus 28 Pre Negociable
					montoTotalDLNeg   = montoTotalDLNeg.add(new BigDecimal(monto));
					montoTotalDsctoDLNeg = montoTotalDsctoDLNeg.add(new BigDecimal(montoDescuento));
				} else if(icEstatusDocumento.equals("9")) {
					montoTotalDLSinOper = montoTotalDLSinOper.add(new BigDecimal(monto));
					montoTotalDsctoDLSinOper = montoTotalDsctoDLSinOper.add(new BigDecimal(montoDescuento));
				} else {
					montoTotalDLNoNeg = montoTotalDLNoNeg.add(new BigDecimal(monto));
					montoTotalDsctoDLNoNeg = montoTotalDsctoDLNoNeg.add(new BigDecimal(montoDescuento));
				}
			}
		}
	}//for 
	
	if( informacion.equals("ControlTotales")) {
		int reg = 0;	
		if("S".equals(spubdocto_venc)){		
				reg = 4;			
		}
		if("N".equals(spubdocto_venc) || "".equals(spubdocto_venc)){	
			reg = 3;		
		}		
					
		for(int t =0; t<=reg; t++) {		
			registrosTot = new HashMap();	
			if("S".equals(spubdocto_venc)){		
			
				if(t==0){ 		
					registrosTot.put("INFORMACION", "No. total de documentos cargados");		
					registrosTot.put("MONEDA_NACIONAL", String.valueOf(numRegistrosMN) );	
					registrosTot.put("MONEDA_DL", String.valueOf(numRegistrosDL));	
				}
				if(t==1){ 
					if(strPendiente.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pendientes Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pre Negociables");	
					}else{
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Negociables");	
					}					
					registrosTot.put("MONEDA_NACIONAL", "$"+Comunes.formatoDecimal(montoTotalMNNeg,2)  );	
					registrosTot.put("MONEDA_DL", "$"+Comunes.formatoDecimal(montoTotalDLNeg,2)  );					
				}			
				if(t==2){ 				
					if(strPendiente.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pendientes NO Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pre NO Negociables");	
					}else{
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados NO Negociables");	
					}						
					registrosTot.put("MONEDA_NACIONAL",  "$"+Comunes.formatoDecimal(montoTotalMNNoNeg,2) );	
					registrosTot.put("MONEDA_DL","$"+Comunes.formatoDecimal(montoTotalDLNoNeg,2)  );						
				}			
				if(t==3){ 							
					if("S".equals(spubdocto_venc)){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Vencidos Sin Operar");
						registrosTot.put("MONEDA_NACIONAL", "$"+Comunes.formatoDecimal(montoTotalMNSinOpera,2)  );	
						registrosTot.put("MONEDA_DL", "$"+Comunes.formatoDecimal(montoTotalDLSinOper,2) );	
					}else {			
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados");
						registrosTot.put("MONEDA_NACIONAL", "$"+Comunes.formatoDecimal(montoTotalMNNeg,2) );	
						registrosTot.put("MONEDA_DL", "$"+Comunes.formatoDecimal(montoTotalDLNeg,2) );			
					}		
				}		
				if(t==4){ 										
					registrosTot.put("INFORMACION", "Monto total de los documentos cargados");
					registrosTot.put("MONEDA_NACIONAL", "$"+Comunes.formatoDecimal(montoTotalMN,2) );	
					registrosTot.put("MONEDA_DL", "$"+Comunes.formatoDecimal(montoTotalDL,2) );				
				}	
			}
			if("N".equals(spubdocto_venc) || "".equals(spubdocto_venc)){	
			
				if(t==0){ 		
					registrosTot.put("INFORMACION", "No. total de documentos cargados");		
					registrosTot.put("MONEDA_NACIONAL", String.valueOf(numRegistrosMN) );	
					registrosTot.put("MONEDA_DL", String.valueOf(numRegistrosDL));	
				}
				if(t==1){ 
					if(strPendiente.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pendientes Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pre Negociables");	
					}else{
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Negociables");	
					}					
					registrosTot.put("MONEDA_NACIONAL", "$"+Comunes.formatoDecimal(montoTotalMNNeg,2)  );	
					registrosTot.put("MONEDA_DL", "$"+Comunes.formatoDecimal(montoTotalDLNeg,2)  );					
				}			
				if(t==2){ 				
					if(strPendiente.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pendientes NO Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados Pre NO Negociables");	
					}else{
						registrosTot.put("INFORMACION", "Monto total de los documentos cargados NO Negociables");	
					}						
					registrosTot.put("MONEDA_NACIONAL",  "$"+Comunes.formatoDecimal(montoTotalMNNoNeg,2) );	
					registrosTot.put("MONEDA_DL","$"+Comunes.formatoDecimal(montoTotalDLNoNeg,2)  );						
				}			
				if(t==3){ 										
					registrosTot.put("INFORMACION", "Monto total de los documentos cargados");
					registrosTot.put("MONEDA_NACIONAL", "$"+Comunes.formatoDecimal(montoTotalMN,2) );	
					registrosTot.put("MONEDA_DL", "$"+Comunes.formatoDecimal(montoTotalDL,2) );				
				}			
			}
			registrosTotales.add(registrosTot);
		}//for
				
		consulta =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registros\": " + registrosTotales.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
	}
		
		
		
	if( informacion.equals("TotalesPreAcuse1")) {		
		int regN =0;	
		int regD =0;		
		if("S".equals(spubdocto_venc)){		
				if(numRegistrosMN>0) regN += 4;
				if(numRegistrosDL>0) regD += 4;		
		}
		if("N".equals(spubdocto_venc) ||  "".equals(spubdocto_venc) ){	
			if(numRegistrosMN>0)regN +=3;
			if(numRegistrosDL>0)regD +=3;		
		}
		
		int reg =regN+regD;			
		HashMap	registrosTot1 = new HashMap();
		JSONArray registrosTotales1 = new JSONArray();	
		for(int t =0; t<reg; t++) {	
			
		registrosTot1 = new HashMap();	
		
		if("S".equals(spubdocto_venc)){
			
			if(reg==8) {
				if(t==0){ 	
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Negociables");	
					}					
					registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalMNNeg,2)   );	
					registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNeg,2) );				
				}		
				if(t==1){ 				
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes No Negociables ");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre No Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Moneda Nacional No Negociables ");	
					}			
						registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalMNNoNeg,2));	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNoNeg,2) );			
				}		
				if(t==2){	
					if("S".equals(spubdocto_venc)){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Vencido Sin Operar ");	
						registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalMNSinOpera,2));	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNSinOpera,2) );	
					}
				}				
				//DOLARES
				if(t==3){ 
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pendientes Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pre Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Dólares Negociables");	
					}					
						registrosTot1.put("TOTAL_MONTO",  "$"+Comunes.formatoDecimal(montoTotalDLNeg,2)   );	
						registrosTot1.put("TOTAL_MONTO_DESC", "$"+Comunes.formatoDecimal(montoTotalDsctoDLNeg,2));				
				}		
				if(t==4){ 				
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pendientes No Negociables ");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total DólaresPre No Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Dólares No Negociables ");	
					}								
					registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalDLNoNeg,2));	
					registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoDLNoNeg,2));			
				}			
				if(t==5){	
					if("S".equals(spubdocto_venc)){
						registrosTot1.put("INFORMACION", " Total Dólares Vencido Sin Operar ");	
						registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalDLSinOper,2));	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoDLSinOper,2) );	
					}
				}		
				if(t==6){	
					registrosTot1.put("INFORMACION", "Total Documentos en Moneda Nacional ");	
					registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosMN) );	
				}
				if(t==7){	
					registrosTot1.put("INFORMACION", "Total Documentos en Dólares ");	 
					registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosDL) );	
				}			
			
			} else { //	if(reg ==8)	
			if(regN==4 &&  numRegistrosMN>0) {
				if(t==0){ 	
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Negociables");	
					}					
					registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalMNNeg,2)   );	
					registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNeg,2) );				
				}		
				if(t==1){ 				
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes No Negociables ");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre No Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Moneda Nacional No Negociables ");	
					}			
					registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalMNNoNeg,2));	
					registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNoNeg,2) );			
				}		
				if(t==2){	
					if("S".equals(spubdocto_venc)){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Vencido Sin Operar ");	
						registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalMNSinOpera,2));	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNSinOpera,2) );	
					}
				}						
				if(t==3){	
					registrosTot1.put("INFORMACION", "Total Documentos en Moneda Nacional ");	
					registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosMN) );	
				}		
			}// Moneda Nacional 
				
			if(regD==4 &&  numRegistrosDL>0)  {
			//DOLARES
				if(t==0){ 
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pendientes Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pre Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Dólares Negociables");	
					}					
						registrosTot1.put("TOTAL_MONTO",  "$"+Comunes.formatoDecimal(montoTotalDLNeg,2)   );	
						registrosTot1.put("TOTAL_MONTO_DESC", "$"+Comunes.formatoDecimal(montoTotalDsctoDLNeg,2));				
				}		
				if(t==1){ 				
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pendientes No Negociables ");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total DólaresPre No Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Dólares No Negociables ");	
					}								
					registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalDLNoNeg,2));	
					registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoDLNoNeg,2));			
				}			
				if(t==2){	
					if("S".equals(spubdocto_venc)){
						registrosTot1.put("INFORMACION", " Total Dólares Vencido Sin Operar ");	
						registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalDLSinOper,2));	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoDLSinOper,2) );	
					}
				}		
				if(t==3){	
					registrosTot1.put("INFORMACION", "Total Documentos en Dólares ");	
					registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosDL) );	
				}
			}			
		 }	
		}	//	if("S".equals(spubdocto_venc)){
		
		
		if("N".equals(spubdocto_venc) || "".equals(spubdocto_venc)){
			
			if(reg==6) {
				if(t==0){ 	
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Negociables");	
					}					
					registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalMNNeg,2)   );	
					registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNeg,2) );				
				}		
				if(t==1){ 				
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes No Negociables ");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre No Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Moneda Nacional No Negociables ");	
					}			
						registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalMNNoNeg,2));	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNoNeg,2) );			
				}		
						
				//DOLARES
				if(t==2){ 
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pendientes Negociables");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pre Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Dólares Negociables");	
					}					
						registrosTot1.put("TOTAL_MONTO",  "$"+Comunes.formatoDecimal(montoTotalDLNeg,2)   );	
						registrosTot1.put("TOTAL_MONTO_DESC", "$"+Comunes.formatoDecimal(montoTotalDsctoDLNeg,2));				
				}		
				if(t==3){ 				
					if(strPendiente.equals("S")){
						registrosTot1.put("INFORMACION", "Total Dólares Pendientes No Negociables ");		
					}else if(strPreNegociable.equals("S")){
						registrosTot1.put("INFORMACION", "Total DólaresPre No Negociables ");	
					}else{
						registrosTot1.put("INFORMACION", "Total Dólares No Negociables ");	
					}								
					registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalDLNoNeg,2));	
					registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoDLNoNeg,2));			
				}			
				if(t==4){	
					registrosTot1.put("INFORMACION", "Total Documentos en Moneda Nacional ");	
					registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosMN) );	
				}
				if(t==5){	
					registrosTot1.put("INFORMACION", "Total Documentos en Dólares");	
					registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosDL) );	
				}			
			} else { //	if(reg ==6)
				
				if(regN==3 &&  numRegistrosMN>0) {
					if(t==0){ 	
						if(strPendiente.equals("S")){
							registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes Negociables");		
						}else if(strPreNegociable.equals("S")){
							registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre Negociables ");	
						}else{
							registrosTot1.put("INFORMACION", "Total Moneda Nacional Negociables");	
						}					
						registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalMNNeg,2)   );	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNeg,2) );				
					}		
					if(t==1){ 				
						if(strPendiente.equals("S")){
							registrosTot1.put("INFORMACION", "Total Moneda Nacional Pendientes No Negociables ");		
						}else if(strPreNegociable.equals("S")){
							registrosTot1.put("INFORMACION", "Total Moneda Nacional Pre No Negociables ");	
						}else{
							registrosTot1.put("INFORMACION", "Total Moneda Nacional No Negociables ");	
						}			
						registrosTot1.put("TOTAL_MONTO", "$"+Comunes.formatoDecimal(montoTotalMNNoNeg,2));	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoMNNoNeg,2) );			
					}		
							
					if(t==2){	
						registrosTot1.put("INFORMACION", "Total Documentos en Moneda Nacional ");	
						registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosMN) );	
					}		
				}// Moneda Nacional 
				
				if(regD==3 &&  numRegistrosDL>0)  {
				//DOLARES
					if(t==0){ 
						if(strPendiente.equals("S")){
							registrosTot1.put("INFORMACION", "Total Dólares Pendientes Negociables");		
						}else if(strPreNegociable.equals("S")){
							registrosTot1.put("INFORMACION", "Total Dólares Pre Negociables ");	
						}else{
							registrosTot1.put("INFORMACION", "Total Dólares Negociables");	
						}					
							registrosTot1.put("TOTAL_MONTO",  "$"+Comunes.formatoDecimal(montoTotalDLNeg,2)   );	
							registrosTot1.put("TOTAL_MONTO_DESC", "$"+Comunes.formatoDecimal(montoTotalDsctoDLNeg,2));				
					}		
					if(t==1){ 				
						if(strPendiente.equals("S")){
							registrosTot1.put("INFORMACION", "Total Dólares Pendientes No Negociables ");		
						}else if(strPreNegociable.equals("S")){
							registrosTot1.put("INFORMACION", "Total DólaresPre No Negociables ");	
						}else{
							registrosTot1.put("INFORMACION", "Total Dólares No Negociables ");	
						}								
						registrosTot1.put("TOTAL_MONTO","$"+Comunes.formatoDecimal(montoTotalDLNoNeg,2));	
						registrosTot1.put("TOTAL_MONTO_DESC","$"+Comunes.formatoDecimal(montoTotalDsctoDLNoNeg,2));			
					}		
						
					if(t==2){	
						registrosTot1.put("INFORMACION", "Total Documentos Dólares ");		   					
						registrosTot1.put("TOTAL_MONTO", String.valueOf(numRegistrosDL) );								
					}
				}			
			}
		}	//	if("N".equals(spubdocto_venc)){
			
			
			registrosTotales1.add(registrosTot1);
				
		}//FOR
		
		consulta =  "{\"success\": true, \"total\": \"" + registrosTotales1.size() + "\", \"registros\": " + registrosTotales1.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
	
	}
	
	infoRegresar  = jsonObj.toString();

}else 	if( informacion.equals("Cancelar")) {

	boolean bBorraDoctos = CargaDocumentos.borraDoctosCargados(proceso, "M", ses_ic_epo);
	CargaDocumentos.borraDoctosDetalleCargados(proceso, "M");
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("proceso", proceso);
	infoRegresar = jsonObj.toString();
	

}else 	if( informacion.equals("ConfirmaCarga")) {
	
	Acuse acuse = null;
	acuse = new Acuse(Acuse.ACUSE_EPO, "1");
	String ses_ic_usuario = iNoUsuario;
	boolean bInsertaAcuse = false;	
	String mensajeError="";
	String _acuse  ="";
	String mostrarAcuse ="N";
	String hidCtrlNumDoctosMN = (request.getParameter("hidCtrlNumDoctosMN") != null) ? request.getParameter("hidCtrlNumDoctosMN") : "";
	String hidCtrlMontoTotalMN = (request.getParameter("hidCtrlMontoTotalMN") != null) ? request.getParameter("hidCtrlMontoTotalMN") : "";
	String hidCtrlNumDoctosDL = (request.getParameter("hidCtrlNumDoctosDL") != null) ? request.getParameter("hidCtrlNumDoctosDL") : "";
	String hidCtrlMontoTotalDL = (request.getParameter("hidCtrlMontoTotalDL") != null) ? request.getParameter("hidCtrlMontoTotalDL") : "";
	String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
	String externContent = (request.getParameter("textoFirmado")==null)?"":request.getParameter("textoFirmado");
	char getReceipt = 'Y';
	String folioCert  ="";
	if (!_serial.equals("") && !externContent.equals("") && !pkcs7.equals("")) {
		 folioCert = acuse.toString();			
		Seguridad s = new Seguridad();
				
		if (s.autenticar(folioCert, _serial, pkcs7, externContent, getReceipt)) {
			_acuse = s.getAcuse();
			System.out.println(" folioCert = " + folioCert + " , _acuse = " + _acuse);// Debug info
			try {
				bInsertaAcuse = CargaDocumentos.insertaAcuse1(folioCert, hidCtrlNumDoctosMN, hidCtrlMontoTotalMN, hidCtrlNumDoctosDL, hidCtrlMontoTotalDL, ses_ic_usuario, _acuse);
				CargaDocumentos.insertaDoctosConDetalles(proceso, null, ses_ic_epo, folioCert, iNoUsuario , strNombreUsuario );
			} catch (NafinException ne){
			out.println(ne.getMsgError(sesIdiomaUsuario));
				
			}
		}	
		if(bInsertaAcuse ==false) {
			mensajeError = "La autentificación no se llevo acabo con éxito "+s.mostrarError();
			mostrarAcuse="N";
			boolean bBorraDoctos = CargaDocumentos.borraDoctosCargados(proceso, "M", ses_ic_epo);
			CargaDocumentos.borraDoctosDetalleCargados(proceso, "M");				
		}else {
			mensajeError = "La autentificación se llevó a cabo con éxito. Recibo: "+_acuse;
			mostrarAcuse="S";
		}		
	}	
	
	hidFechaCarga = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
	hidHoraCarga = (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
	usuario = iNoUsuario+" - "+strNombreUsuario;
	
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("mostrarAcuse",mostrarAcuse);
	jsonObj.put("acuseFormateado",acuse.formatear());
	jsonObj.put("acuse",folioCert);
	jsonObj.put("recibo",_acuse);	
	jsonObj.put("proceso",proceso);	
	jsonObj.put("mensajeError",mensajeError);	
	jsonObj.put("hidFechaCarga",hidFechaCarga);	
	jsonObj.put("hidHoraCarga",hidHoraCarga);	
	jsonObj.put("usuario",usuario);
	
	infoRegresar  = jsonObj.toString();
	

}else if (  ( informacion.equals("ConsultaAcuse1") 
			|| informacion.equals("ConsultaAcuse2") 
			|| informacion.equals("ConsultaAcuse3") 	 	)  			
			&& pantalla.equals("Acuse") ) {

		
	if( informacion.equals("ConsultaAcuse1") ) {

		if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
     ic_estatus_docto = "30"; // Pendientes Negociables
    else if(strPreNegociable.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "28"; // Pendientes Negociables
    else
		ic_estatus_docto = "2"; // Negociables
			
		Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargadosConIcDocumento(proceso, strAforo, ses_ic_epo, ic_estatus_docto, "", strAforoDL, sesIdiomaUsuario,acuse1);
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{
			Vector vd = (Vector)e.nextElement();
			nombrePyme = vd.get(0).toString();
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			fechaVencimientoPyme = vd.get(24)==null?"":vd.get(24).toString();							
			monedaNombre = vd.get(4).toString();
			estatusDocumento 		= vd.get(14).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10).toString();
			tipoFactoraje = vd.get(7).toString();
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();	
			
			NombreIF 				= "";
			nombremandante   = vd.get(31).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje  = vd.get(32).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			clavePresupuestaria = vd.get(29).toString();
			periodo = vd.get(30).toString();
			if(bOperaFactorajeVencido.equals("S") ) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V") )  {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF = vd.get(11).toString();
				}
			}
			// Fodea 023 Mandato
			if(bOperaFactConMandato.equals("S") && tipoFactoraje.equals("M"))  {								
				NombreIF = vd.get(11).toString();
			}			
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S") ) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D"))  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(20).toString();
					sPorcentajeBeneficiario = vd.get(21).toString();
					sMontoBeneficiario = vd.get(22).toString();
				}
			}			
			//Fodea 042-2009 Vencimiento Infonavit
			if(bOperaFactorajeVencidoInfonavit.equals("S") && tipoFactoraje.equals("I"))  {								
				NombreIF = vd.get(11).toString();
				porcentajeAnticipo = "100";
				montoDescuento = monto;
				sNombreBeneficiario = vd.get(20).toString();
				sPorcentajeBeneficiario = vd.get(21).toString();
				sMontoBeneficiario = vd.get(22).toString();
			}
			//FODEA 050 - VALC - 10/2008			
			fechaEntrega			= vd.get(27).toString();
			tipoCompra = "";
			if(vd.get(28).toString().equals("L"))		tipoCompra = "Licitación";
			if(vd.get(28).toString().equals("I")) 	tipoCompra = "Invitación";
			if(vd.get(28).toString().equals("D"))		tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(28).toString().equals("C"))
			tipoCompra = "CAAS, CAAS delegado"; 
			if(tipoFactoraje.equals("N"))				tipoFactoraje = "Normal";
			else if(tipoFactoraje.equals("M"))	tipoFactoraje = "Mandato";	
			else if(tipoFactoraje.equals("V"))	tipoFactoraje = "Vencido";
			else if(tipoFactoraje.equals("D"))	tipoFactoraje = "Distribuido";
			else if(tipoFactoraje.equals("C")) {
				tipoFactoraje = "Nota de Crédito";
				porcentajeAnticipo = "100";
				montoDescuento = monto;
			}
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if (iNumMoneda==1) {
				numRegistrosMN++;
			} else { //IC_MONEDA=54
				numRegistrosDL++;
			}
			if(estaHabilitadoNumeroSIAFF){			
				numeroSIAFF = getNumeroSIAFF(vd.get(26).toString(),vd.get(25).toString());
			}
			
			duplicado = vd.get(33).toString(); //F038-2014

			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("DIGITO_IDENTIFICADOR",numeroSIAFF);
			datos.put("MANDANTE",nombremandante);				
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);	
			datos.put("DUPLICADO",duplicado); //F038-2014

			registros.add(datos);				
			
		}//for
		
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	
	}else if( informacion.equals("ConsultaAcuse2") ) {
	
		if(strPendiente.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "31"; // Pendientes Negociables
    else if(strPreNegociable.equals("S"))   // Si se trata de documentos Pendientes
			ic_estatus_docto = "29"; // Pendientes Negociables
    else
			ic_estatus_docto = "1"; // Negociables
		//Despliegue de los documentos
	
		Vector	vDoctosCargados = CargaDocumentos.mostrarDocumentosCargadosConIcDocumento(proceso, strAforo, ses_ic_epo, ic_estatus_docto, "", strAforoDL, sesIdiomaUsuario,acuse1);
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{
			Vector vd = (Vector)e.nextElement();
			nombrePyme = vd.get(0).toString();
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			fechaVencimientoPyme = vd.get(24)==null?"":vd.get(24).toString();
			monedaNombre = vd.get(4).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10).toString();
			tipoFactoraje = vd.get(7).toString();
			estatusDocumento 		= vd.get(14).toString();
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();	
			NombreIF = "";
			nombremandante  = vd.get(31).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje  = vd.get(32).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			if(bOperaFactorajeVencido.equals("S")) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V") ) {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF = vd.get(11).toString();
				}
			}
			// Fodea 023 Mandato
			if(bOperaFactConMandato.equals("S") && tipoFactoraje.equals("M"))  {								
				NombreIF = vd.get(11).toString();
			}
			//FODEA 050 - VALC - 10/2008
			fechaEntrega			= vd.get(27).toString();
			if(vd.get(28).toString().equals("L"))	tipoCompra = "Licitación";
			if(vd.get(28).toString().equals("I")) tipoCompra = "Invitación";
			if(vd.get(28).toString().equals("D"))	tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(28).toString().equals("C")) 	tipoCompra = "CAAS, CAAS delegado";
			clavePresupuestaria = vd.get(29).toString();
			periodo = vd.get(30).toString();
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S")) { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D"))  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(20).toString();
					sPorcentajeBeneficiario = vd.get(21).toString();
					sMontoBeneficiario = vd.get(22).toString();
				}
			}
			
			tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": (tipoFactoraje.equals("V"))?"Vencido" : 	
			(tipoFactoraje.equals("D"))?"Distribuido": (tipoFactoraje.equals("C"))?"Nota Credito": 
			(tipoFactoraje.equals("M"))?"Mandato": "";
			
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			if (iNumMoneda==1) {
				numRegistrosMN++;
			} else { //IC_MONEDA=54
				numRegistrosDL++;
			}
			if(estaHabilitadoNumeroSIAFF){
				numeroSIAFF = getNumeroSIAFF(vd.get(26).toString(),vd.get(25).toString());
			}
			
			 duplicado = vd.get(33).toString(); //F038-2014

			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("DIGITO_IDENTIFICADOR",numeroSIAFF);
			datos.put("MANDANTE",nombremandante);				
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);					
			datos.put("DUPLICADO",duplicado); //F038-2014

			registros.add(datos);				
							
		}//for
		
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		
	}else if( informacion.equals("ConsultaAcuse3") ) {
	
		Vector vDoctosCargados = CargaDocumentos.mostrarDocumentosCargadosConIcDocumento(proceso, strAforo, ses_ic_epo, "9", "", strAforoDL, sesIdiomaUsuario, acuse1);
		for(Enumeration e=vDoctosCargados.elements(); e.hasMoreElements();)	{
			Vector vd = (Vector)e.nextElement();
			nombrePyme = vd.get(0).toString();
			numeroDocumento = vd.get(1).toString();
			fechaDocumento = vd.get(2).toString();
			fechaVencimiento = vd.get(3)==null?"":vd.get(3).toString();
			fechaVencimientoPyme = vd.get(24)==null?"":vd.get(24).toString();
			monedaNombre = vd.get(4).toString();
			monto = vd.get(6).toString();
			porcentajeAnticipo = vd.get(8).toString();
			montoDescuento = vd.get(9).toString();
			referencia = vd.get(10).toString();
			tipoFactoraje = vd.get(7).toString();
			estatusDocumento 		= vd.get(14).toString();
			campo1 = vd.get(15).toString();
			campo2 = vd.get(16).toString();
			campo3 = vd.get(17).toString();
			campo4 = vd.get(18).toString();
			campo5 = vd.get(19).toString();	
			NombreIF = "";
			nombremandante   = vd.get(31).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			factoraje  = vd.get(32).toString().replace('"',' ').trim(); // FODEA 023 Mandato
			if(bOperaFactorajeVencido.equals("S") ) { // Para Factoraje Vencido
				if(tipoFactoraje.equals("V"))  {
					porcentajeAnticipo = "100";	// si factoraje vencido: se aplica 100% de anticipo
					montoDescuento = monto;		// y el monto del desc es igual al del docto.
					NombreIF = vd.get(11).toString();
				}
			}
			// Fodea 023 Mandato
			if(bOperaFactConMandato.equals("S") && tipoFactoraje.equals("M"))  {								
				NombreIF = vd.get(11).toString();
			}
			//FODEA 050 - VALC - 10/2008
			fechaEntrega			= vd.get(27).toString();
			tipoCompra = "";
			if(vd.get(28).toString().equals("L")) 	tipoCompra = "Licitación";
			if(vd.get(28).toString().equals("I")) 	tipoCompra = "Invitación";
			if(vd.get(28).toString().equals("D"))		tipoCompra = "Directa, Directa en el Extranjero, Entidades, Ampliación Art. 52 y pago a Notarios";
			if(vd.get(28).toString().equals("C"))		tipoCompra = "CAAS, CAAS delegado";
			clavePresupuestaria = vd.get(29).toString();
			periodo = vd.get(30).toString();
			sNombreBeneficiario=""; sPorcentajeBeneficiario=""; sMontoBeneficiario="";
			if(bOperaFactorajeDistribuido.equals("S") )  { // Para Factoraje Distribuido
				if(tipoFactoraje.equals("D"))  {
					porcentajeAnticipo = "100";
					montoDescuento = monto;
					sNombreBeneficiario = vd.get(20).toString();
					sPorcentajeBeneficiario = vd.get(21).toString();
					sMontoBeneficiario = vd.get(22).toString();
				}
			}
			// Fodea -042-2009 Vencimiento Infonavit
			if(bOperaFactorajeVencidoInfonavit.equals("S") && tipoFactoraje.equals("I"))  {								
				NombreIF = vd.get(11).toString();
				porcentajeAnticipo = "100";
				montoDescuento = monto;
				sNombreBeneficiario = vd.get(20).toString();
				sPorcentajeBeneficiario = vd.get(21).toString();
				sMontoBeneficiario = vd.get(22).toString();								
			}
			tipoFactoraje = (tipoFactoraje.equals("N"))?"Normal": 
			(tipoFactoraje.equals("M"))?"Mandato":	(tipoFactoraje.equals("V"))?"Vencido":	(tipoFactoraje.equals("C"))?"Nota Credito":
			(tipoFactoraje.equals("D"))?"Distribuido":"";
			iNumMoneda = Integer.parseInt(vd.get(5).toString());
			
			if(estaHabilitadoNumeroSIAFF){
				numeroSIAFF = getNumeroSIAFF(vd.get(26).toString(),vd.get(25).toString());
			}			
			if (iNumMoneda==1) {
				numRegistrosMN++;
			} else { //IC_MONEDA=54
				numRegistrosDL++;
			}
			
			//duplicado = vd.get(31).toString(); //F038-2014
			duplicado = vd.get(33).toString(); //F038-2014
			
			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR",nombrePyme);
			datos.put("NUM_DOCUMENTO",numeroDocumento);
			datos.put("FECHA_EMISION",fechaDocumento);
			datos.put("FECHA_VENCIMIENTO",fechaVencimiento);
			datos.put("FECHA_VENC_PROVEEDOR",fechaVencimientoPyme);
			datos.put("IC_MONEDA",ic_monedad);
			datos.put("MONEDA",monedaNombre);
			datos.put("TIPO_FACTORAJE",factoraje);
			datos.put("MONTO",monto);
			datos.put("PORC_DESCUENTO",porcentajeAnticipo);
			datos.put("MONTO_DESCONTAR",montoDescuento);
			datos.put("ESTATUS",estatusDocumento);
			datos.put("REFERENCIA",referencia);
			datos.put("NOMBRE_IF",NombreIF);
			datos.put("NOMBRE_BENEFICIARIO",sNombreBeneficiario);
			datos.put("PORC_BENEFICIARIO",sPorcentajeBeneficiario);
			datos.put("MONTO_BENEFICIARIO",sMontoBeneficiario);
			datos.put("CAMPO1",campo1);
			datos.put("CAMPO2",campo2);
			datos.put("CAMPO3",campo3);
			datos.put("CAMPO4",campo4);
			datos.put("CAMPO5",campo5);
			datos.put("FECHA_RECEPCION",fechaEntrega);
			datos.put("TIPO_COMPRA",tipoCompra);
			datos.put("CLASIFICADOR",clavePresupuestaria);
			datos.put("PLAZO_MAXIMO",periodo);
			datos.put("DIGITO_IDENTIFICADOR",numeroSIAFF);
			datos.put("MANDANTE",nombremandante);				
			datos.put("IC_PYME",clavePyme);	
			datos.put("IC_PROCESO",proceso);							
			datos.put("DUPLICADO",duplicado);  //F038-2014
			registros.add(datos);					
		}//for	
	
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
	}
	
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("hidMontoTotalMN", String.valueOf(montoTotalMN));
	jsonObj.put("hidNumDoctosActualesMN", Double.toString(numRegistrosMN));
	jsonObj.put("hidMontoTotalDL", String.valueOf(montoTotalDL));
	jsonObj.put("hidNumDoctosActualesDL",  Double.toString(numRegistrosDL));			
	jsonObj.put("hayCamposAdicionales", String.valueOf(numeroCampos));
	jsonObj.put("nomCampo1",String.valueOf(nomCampo1));
	jsonObj.put("nomCampo2",String.valueOf(nomCampo2));
	jsonObj.put("nomCampo3",String.valueOf(nomCampo3));
	jsonObj.put("nomCampo4",String.valueOf(nomCampo4));
	jsonObj.put("nomCampo5",String.valueOf(nomCampo5));			
	jsonObj.put("operaFVPyme",  operaFVPyme);
	jsonObj.put("bOperaFactorajeVencido",  bOperaFactorajeVencido);
	jsonObj.put("bOperaFactConMandato",  bOperaFactConMandato);
	jsonObj.put("bOperaFactorajeVencidoInfonavit",  bOperaFactorajeVencidoInfonavit);
	jsonObj.put("bOperaFactorajeDistribuido",  bOperaFactorajeDistribuido);
	jsonObj.put("sPubEPOPEFFlagVal",  sPubEPOPEFFlagVal);
	jsonObj.put("bOperaFactConMandato",  bOperaFactConMandato);	
	jsonObj.put("bestaHabilitadoNumeroSIAFF",  bestaHabilitadoNumeroSIAFF);	
	jsonObj.put("bValidaDuplicidad",  bValidaDuplicidad);	
	
	infoRegresar  = jsonObj.toString();		
	
}	


} catch(Exception e) {
    out.println(e);
		System.out.println("error "+e);
} finally {
	
	
}

%>
<%!
	public String getNumeroSIAFF(String ic_epo,String ic_documento){
		return getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(ic_documento);		
	}
	
	public String getNumeroDeCuatroDigitos(String numero){

		String 			claveEPO 	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();
		
      for(int i = 0;i<(4-claveEPO.length());i++){
			resultado.append("0");
      }
      resultado.append(claveEPO);

      return resultado.toString();
  }
  
  	public String getNumeroDeOnceDigitos(String numero){

		String 			claveDocto	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();
		
      for(int i = 0;i<(11-claveDocto.length());i++){
			resultado.append("0");
      }
      resultado.append(claveDocto);

      return resultado.toString();
  }
%>
<%=infoRegresar%>

<% 
//System.out.println("infoRegresar "+infoRegresar);
%>