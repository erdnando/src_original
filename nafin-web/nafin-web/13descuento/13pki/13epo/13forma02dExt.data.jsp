<%@ page contentType="application/json;charset=UTF-8"
	import="	
	java.io.*, 		
	javax.naming.*,
	java.util.*,
	java.math.*, 
	java.text.*, 
	java.sql.*,
	com.netro.pdf.*,	 
	com.jspsmart.upload.*, 
	netropology.utilerias.*,			
	com.netro.exception.*,
	java.text.SimpleDateFormat,
	java.util.Date,		
	net.sf.json.JSONArray,
	java.security.*, 
	com.netro.descuento.*,
	net.sf.json.JSONObject"	
	errorPage="/00utils/error_extjs.jsp"
%>

<%@ include file="/appComun.jspf" %>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload" />
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
	String infoRegresar =""; 
	String archivo  = (request.getParameter("archivo") != null) ? request.getParameter("archivo") : "";
	
	String txttotdoc = (request.getParameter("txttotdoc") != null) ? request.getParameter("txttotdoc") : "";  if(txttotdoc.equals("0")) txttotdoc ="";
	String txtmtodoc = (request.getParameter("txtmtodoc") != null) ? request.getParameter("txtmtodoc") : "";  if(txtmtodoc.equals("0")) txtmtodoc ="";
	String txtmtomindoc = (request.getParameter("txtmtomindoc") != null) ? request.getParameter("txtmtomindoc") : "";  if(txtmtomindoc.equals("0")) txtmtomindoc ="";
	String txtmtomaxdoc = (request.getParameter("txtmtomaxdoc") != null) ? request.getParameter("txtmtomaxdoc") : "";  if(txtmtomaxdoc.equals("0")) txtmtomaxdoc ="";
	
	String txttotdocdo = (request.getParameter("txttotdocdo") != null) ? request.getParameter("txttotdocdo") : "";    if(txttotdocdo.equals("0")) txttotdocdo ="";
	String txtmtodocdo = (request.getParameter("txtmtodocdo") != null) ? request.getParameter("txtmtodocdo") : ""; 		if(txtmtodocdo.equals("0")) txtmtodocdo ="";
	String txtmtomindocdo = (request.getParameter("txtmtomindocdo") != null) ? request.getParameter("txtmtomindocdo") : "";  if(txtmtomindocdo.equals("0")) txtmtomindocdo ="";
	String txtmtomaxdocdo = (request.getParameter("txtmtomaxdocdo") != null) ? request.getParameter("txtmtomaxdocdo") : ""; if(txtmtomaxdocdo.equals("0")) txtmtomaxdocdo ="";
	
	
	String 	EstatusReg= (request.getParameter("EstatusReg") != null) ? request.getParameter("EstatusReg") : "ER";
	String proceso = (request.getParameter("proceso") != null) ? request.getParameter("proceso") : "";
	String nocampos_detalle = (request.getParameter("nocampos_detalle") != null) ? request.getParameter("nocampos_detalle") : "0";
	
	String ses_ic_epo			= iNoCliente;
	String mensajeError = ""; 	
	AccesoDB con = new AccesoDB();

	CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
	
	String sLineaDetalle="", mensaje =""; 
	int numLinea =0, no_proceso = 0;
	VectorTokenizer vt = null;
	Vector vdata=null;
	Vector nums_doctos = new Vector();
	String strNoDocto="",strFechaDocto="",strMoneda="",strConsecutivo="",strCampo1="",strCampo2="",strCampo3="",
					strCampo4="",strCampo5="",strCampo6="",strCampo7="",strCampo8="",strCampo9="",strCampo10="",strCgPymeEpoInterno="",
				  ErrorReg="";		
	JSONObject 	jsonObj	= new JSONObject();

	if(informacion.equals("CargaArchivoDetalle")) {
		int no_campos_detalle =0;
		
		try {
			
			con.conexionDB();
			// Para saber si tiene la epo Campos Detalle definidos.
			no_campos_detalle = CargaDocumentos.getNumCamposDetalle(ses_ic_epo);
					
			if(no_campos_detalle > 0) {
					
				String rutaArchivo = strDirectorioTemp +archivo; 		
				java.io.File ft = new java.io.File(rutaArchivo);	
				
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));			
				
				no_proceso = CargaDocumentos.getNumProcesoDetalle();
				
				//Barrido del archivo
				while((sLineaDetalle=br.readLine())!=null) {
					try {	
						numLinea++;
						
						System.out.println("Inicio de insercion con carga "+no_proceso+" la linea "+numLinea+".");					
						
						vt = new VectorTokenizer(sLineaDetalle,"|");
						vdata = vt.getValuesVector();
												
						System.out.println( "la linea "+vdata.size()+"---"+vdata);		
						
						int adicional = 0;
						if (vdata.size()==(4+no_campos_detalle+1)) {
							strCgPymeEpoInterno=(vdata.size()==(4+no_campos_detalle+1))?Comunes.quitaComitasSimples(vdata.get((4+no_campos_detalle)).toString()).trim():"";
							adicional = 1;
						}else{
							strCgPymeEpoInterno = "";
						}
						strNoDocto=(vdata.size()>=1)?Comunes.quitaComitasSimples(vdata.get(0).toString()).trim():"";
						strFechaDocto=(vdata.size()>=2)?Comunes.quitaComitasSimples(vdata.get(1).toString()).trim():"";
						strMoneda=(vdata.size()>=3)?Comunes.quitaComitasSimples(vdata.get(2).toString()).trim():"";
						strConsecutivo=(vdata.size()>=4)?Comunes.quitaComitasSimples(vdata.get(3).toString()).trim():"";					
						
						if (4+no_campos_detalle >= 5){
							strCampo1=(vdata.size()>=5)?Comunes.quitaComitasSimples(vdata.get(4).toString()).trim():"";
						}
						if (4+no_campos_detalle >= 6){
							strCampo2=(vdata.size()>=6)?Comunes.quitaComitasSimples(vdata.get(5).toString()).trim():"";
						}
						if (4+no_campos_detalle >= 7){
							strCampo3=(vdata.size()>=7)?Comunes.quitaComitasSimples(vdata.get(6).toString()).trim():"";
						}
						if (4+no_campos_detalle >= 8){
							strCampo4=(vdata.size()>=8)?Comunes.quitaComitasSimples(vdata.get(7).toString()).trim():"";
						}
						if (4+no_campos_detalle >= 9){
							strCampo5=(vdata.size()>=9)?Comunes.quitaComitasSimples(vdata.get(8).toString()).trim():"";
						}
						if (4+no_campos_detalle >= 10){
							strCampo6=(vdata.size()>=10)?Comunes.quitaComitasSimples(vdata.get(9).toString()).trim():"";
						}
						if (4+no_campos_detalle >= 11){
							strCampo7=(vdata.size()>=11)?Comunes.quitaComitasSimples(vdata.get(10).toString()).trim():"";
						}
						if (4+no_campos_detalle >= 12){
							strCampo8=(vdata.size()>=12)?Comunes.quitaComitasSimples(vdata.get(11).toString()).trim():"";
						}
						if (4+no_campos_detalle >= 13){
							strCampo9=(vdata.size()>=13)?Comunes.quitaComitasSimples(vdata.get(12).toString()).trim():"";
						}
						if (4+no_campos_detalle >= 14){
							strCampo10=(vdata.size()>=14)?Comunes.quitaComitasSimples(vdata.get(13).toString()).trim():"";
						}
						
						int aux_ = 4+no_campos_detalle;
						if (vdata.size()<4) {	
							EstatusReg="ER";
							ErrorReg=mensaje_param.getMensaje("13forma2.NoCumpleMinimoNumDatosReq",sesIdiomaUsuario);
						} else if (vdata.size()<(4+no_campos_detalle)) {	
							EstatusReg="ER";
							ErrorReg=mensaje_param.getMensaje("13forma2.MenosNumDatosAdicionalesQueDefinidos",sesIdiomaUsuario);
						} else if (vdata.size()>(4+no_campos_detalle+1)) {	
							EstatusReg="ER";
							ErrorReg=mensaje_param.getMensaje("13forma2.MasNumDatosAdicionalesQueDefinidos",sesIdiomaUsuario);
						} else {	
							EstatusReg="OK";
							ErrorReg="";
						}
													
					
						CargaDocumentos.insertDetallesTMP(no_proceso, numLinea, strNoDocto,
								strFechaDocto, strMoneda, strConsecutivo,
								strCampo1, strCampo2, strCampo3,
								strCampo4, strCampo5, strCampo6,
								strCampo7, strCampo8, strCampo9,
								strCampo10, EstatusReg, ErrorReg, strCgPymeEpoInterno);
								
					} catch (StringIndexOutOfBoundsException sioobe) {	
						System.out.println("Error por linea vacia:"+sioobe.toString());
						throw new NafinException("DSCT0030");
					} catch (NafinException ne) {
						throw ne;
					}
			
				
				} // while del archivo2
											
											
				//Procesamiento de los documentos // Aqui es donde se van al store procedure SP_CARGA_MASIVADET
				new CicloThreadCMD(no_proceso, Integer.parseInt(ses_ic_epo), no_campos_detalle, 
																txttotdoc, 	txtmtodoc, txtmtomindoc, txtmtomaxdoc, 
																txttotdocdo, txtmtodocdo, txtmtomindocdo, 
																txtmtomaxdocdo, sesIdiomaUsuario, con).start();		
																
				
			}else { //if campos_detalle
				mensaje = " No ha definido campos para la carga de detalles. Por favor, verifíquelo.";				
			}
			
		} catch(NafinException ne) {
			System.out.println(" ERROR: 	" + ne.toString());
			ne.printStackTrace();			
		} catch (Exception e) {
			System.out.println(" ERROR Exception: 	" + e.toString());
			e.printStackTrace();	
			mensajeError = mensaje_param.getMensaje("13forma2.ErrorCargarArchivo",sesIdiomaUsuario)+":"+e.toString();			
		} finally {//No se cierra la conexion porque se manda como parametro a un método que se encargará de ello		
			//if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("Termino del jsp de procesamiento...");
		}

		jsonObj.put("success", new Boolean(true));
		
		jsonObj.put("EstatusReg",String.valueOf(EstatusReg));	
		jsonObj.put("proceso",String.valueOf(no_proceso));
		jsonObj.put("ErrorReg",String.valueOf(ErrorReg));
		jsonObj.put("nocampos_detalle",String.valueOf(no_campos_detalle));
		jsonObj.put("mensaje",mensaje);	
		
		jsonObj.put("txttotdoc",String.valueOf(txttotdoc) );
		jsonObj.put("txtmtodoc",String.valueOf(txtmtodoc) );
		jsonObj.put("txtmtomindoc",String.valueOf(txtmtomindoc) );
		jsonObj.put("txtmtomaxdoc",String.valueOf(txtmtomaxdoc) );
		
		jsonObj.put("txttotdocdo",String.valueOf(txttotdocdo) );
		jsonObj.put("txtmtodocdo",String.valueOf(txtmtodocdo) );
		jsonObj.put("txtmtomindocdo",String.valueOf(txtmtomindocdo) );
		jsonObj.put("txtmtomaxdocdo",String.valueOf(txtmtomaxdocdo) );
		
		infoRegresar =	jsonObj.toString();


	}else  if(informacion.equals("ProcesaCargaArchivo")) {
	
		double NumeroLineas = numLinea;
		double Porcentaje = 0;
		Vector VEstado = null;
		int   NumProcesados =0, NumCorrectos  =0, NumErrores =0; 
		
		VEstado = CargaDocumentos.mostrarEstadoProcesoCMD(Integer.parseInt(proceso));
		
		if(VEstado.size()>0) {
			EstatusReg = (VEstado.get(0)).toString();
			NumProcesados  = Integer.parseInt((VEstado.get(1)).toString());
			NumCorrectos   = Integer.parseInt((VEstado.get(2)).toString());
			NumErrores	   = Integer.parseInt((VEstado.get(3)).toString());
		} else {
			EstatusReg = "PR";
			NumProcesados  = 0;			
		}
			
		String resultado=CargaDocumentos.getResultadoErrorCMD(Integer.parseInt(proceso), sesIdiomaUsuario);
		
			/*esta parte es para la epo 211	*/
			String erroresProceso = CargaDocumentos.getResultadoErrorCMDProc(Integer.parseInt(proceso));
			Vector totolesValidos = CargaDocumentos.getTotalesValidosCMD(Integer.parseInt(proceso));
			Vector totolesError = CargaDocumentos.getTotalesErrorCMD(Integer.parseInt(proceso));

				int totalMNValidos = 0 , totalUSValidos = 0;
				String totalMNError = "0", totalUSDError = "0", detallesValidos = "",  detallesValidosMN = "",  detallesValidosUSD = "";
				
				for(int j=0;j<totolesValidos.size();j++){
					Vector datos = (Vector)totolesValidos.get(j);
					String rs_moneda = (String)datos.get(0)==null?"":(String)datos.get(0);
					String rs_numeroDocto = (String)datos.get(1)==null?"":(String)datos.get(1);
					String rs_totalDetalles = (String)datos.get(2)==null?"":(String)datos.get(2);
					detallesValidos += "\n ";
					if("1".equals(rs_moneda)) {
						totalMNValidos+=Integer.parseInt(rs_totalDetalles);
						detallesValidosMN += "\n " +
								mensaje_param.getMensaje("13forma2.NumDocumento",sesIdiomaUsuario) + ": "+rs_numeroDocto+ " " +
								mensaje_param.getMensaje("13forma2.NumDetallesCargados",sesIdiomaUsuario) + ": "+rs_totalDetalles;
					}
					else if("54".equals(rs_moneda)) {
						totalUSValidos+=Integer.parseInt(rs_totalDetalles);
						detallesValidosUSD += "\n" +
								mensaje_param.getMensaje("13forma2.NumDocumento",sesIdiomaUsuario) + ": "+rs_numeroDocto+" " +
								mensaje_param.getMensaje("13forma2.NumDetallesCargados",sesIdiomaUsuario) + ": "+rs_totalDetalles;
					}
				}
				
				detallesValidos = detallesValidosMN + detallesValidosUSD;

				for(int j=0;j<totolesError.size();j++){
					Vector datos = (Vector)totolesError.get(j);
					if("1".equals((String)datos.get(0)))
						totalMNError = (String)datos.get(1);
					else if("54".equals((String)datos.get(0)))
						totalUSDError = (String)datos.get(1);
				}
							
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("NumProcesados",String.valueOf(NumProcesados));	
			jsonObj.put("numReg",String.valueOf(NumeroLineas));	
			jsonObj.put("EstatusReg",EstatusReg);
			jsonObj.put("proceso",String.valueOf(proceso) );
			jsonObj.put("resultado",resultado );	
			jsonObj.put("nocampos_detalle",nocampos_detalle );
			
			//esta parte es para la epo 211
			jsonObj.put("detallesValidos",detallesValidos );	
			jsonObj.put("totalMNValidos",String.valueOf(totalMNValidos) );
			jsonObj.put("totalUSValidos",String.valueOf(totalUSValidos) );			
			jsonObj.put("totalMNError",String.valueOf(totalMNError) );
			jsonObj.put("totalUSDError",String.valueOf(totalUSDError) );
			jsonObj.put("erroresProceso",String.valueOf(erroresProceso) );
			
			jsonObj.put("txttotdoc",String.valueOf(txttotdoc) );
			jsonObj.put("txtmtodoc",String.valueOf(txtmtodoc) );
			jsonObj.put("txtmtomindoc",String.valueOf(txtmtomindoc) );
			jsonObj.put("txtmtomaxdoc",String.valueOf(txtmtomaxdoc) );
			
			jsonObj.put("txttotdocdo",String.valueOf(txttotdocdo) );
			jsonObj.put("txtmtodocdo",String.valueOf(txtmtodocdo) );
			jsonObj.put("txtmtomindocdo",String.valueOf(txtmtomindocdo) );
			jsonObj.put("txtmtomaxdocdo",String.valueOf(txtmtomaxdocdo) );
			
						
			infoRegresar =		jsonObj.toString();
			
	}else  if(informacion.equals("Cancelar")) {
	
		jsonObj.put("success", new Boolean(true));
		infoRegresar =		jsonObj.toString();
	
	
	}else  if(informacion.equals("ConsultaResultado")) {
		
		HashMap datos = new HashMap();	
		JSONArray registros = new JSONArray();
		datos = new HashMap();
		String consulta = ""; 	
		Vector  VFilas = CargaDocumentos.getResumenOkCMD(Integer.parseInt(proceso));
		int tamanio = VFilas.size();
		
		for (int i=0; i <VFilas.size(); i++) {
			Vector	VColumnas= new Vector();
			VColumnas=(Vector) VFilas.get(i);
			String 	NumDocto=VColumnas.get(1).toString();
			String NumDetalles=VColumnas.get(2).toString();
			
			datos = new HashMap();
			datos.put("NUMERO_DOCUMENTO",NumDocto);
			datos.put("NUMERO_DETALLES",NumDetalles);
			
			registros.add(datos);	
		}//Fin del for por Numero de Doctos
		
		datos.put("NUMERO_DOCUMENTO","No. total de detalles cargados");
		datos.put("NUMERO_DETALLES",String.valueOf(tamanio) );
					
		registros.add(datos);	
		
		consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
		jsonObj = JSONObject.fromObject(consulta);
		jsonObj.put("proceso",String.valueOf(proceso) );
		infoRegresar =		jsonObj.toString();
		
		
		
}else  if(informacion.equals("ArchivoPDF")) {

		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
		
		pdfDoc.setTable(2, 50);
		pdfDoc.setCell("Número de Documento", "celda01", ComunesPDF.CENTER);
		pdfDoc.setCell("Número de Detalles Cargados", "celda01", ComunesPDF.CENTER);
		
		
		Vector  VFilas = CargaDocumentos.getResumenOkCMD(Integer.parseInt(proceso));
		int tamanio = VFilas.size();
		
		for (int i=0; i <VFilas.size(); i++) {
			Vector	VColumnas= new Vector();
			VColumnas=(Vector) VFilas.get(i);
			String 	NumDocto=VColumnas.get(1).toString();
			String NumDetalles=VColumnas.get(2).toString();
		
			pdfDoc.setCell(NumDocto, "formas", ComunesPDF.CENTER);
			pdfDoc.setCell(NumDetalles, "formas", ComunesPDF.CENTER);	
			
		}//Fin del for por Numero de Doctos
		pdfDoc.setCell("No. total de detalles cargados", "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(String.valueOf(tamanio) , "formas", ComunesPDF.CENTER);	
		
		pdfDoc.addTable();
		pdfDoc.endDocument();
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

		infoRegresar =		jsonObj.toString();
}

%>
<%=infoRegresar%>