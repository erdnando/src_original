
Ext.onReady(function() {
	
	
	var strPendiente =  Ext.getDom('strPendiente').value;
	var strPreNegociable =  Ext.getDom('strPreNegociable').value;
	
	var limpiar  = function() {
	
		Ext.getCmp("hidCtrlNumDoctosMN1").setValue('');
		Ext.getCmp("hidCtrlMontoTotalMNDespliegue1").setValue('');
		Ext.getCmp("hidCtrlMontoMinMNDespliegue1").setValue('');
		Ext.getCmp("hidCtrlMontoMaxMNDespliegue1").setValue('');
		
		Ext.getCmp("hidCtrlNumDoctosDL1").setValue('');
		Ext.getCmp("hidCtrlMontoTotalDLDespliegue1").setValue('');
		Ext.getCmp("hidCtrlMontoMinDLDespliegue1").setValue('');
		Ext.getCmp("hidCtrlMontoMaxDLDespliegue1").setValue('');	
	}
	
	var enviar = function() {
	
		var continua = true;
		var hidCtrlNumDoctosMN = Ext.getCmp("hidCtrlNumDoctosMN1");
		var hidCtrlMontoTotalMNDespliegue = Ext.getCmp("hidCtrlMontoTotalMNDespliegue1");
		var hidCtrlMontoMinMNDespliegue = Ext.getCmp("hidCtrlMontoMinMNDespliegue1");
		var hidCtrlMontoMaxMNDespliegue  = Ext.getCmp("hidCtrlMontoMaxMNDespliegue1");
		
		var hidCtrlNumDoctosDL = Ext.getCmp("hidCtrlNumDoctosDL1");
		var hidCtrlMontoTotalDLDespliegue = Ext.getCmp("hidCtrlMontoTotalDLDespliegue1");
		var hidCtrlMontoMinDLDespliegue = Ext.getCmp("hidCtrlMontoMinDLDespliegue1");
		var hidCtrlMontoMaxDLDespliegue  = Ext.getCmp("hidCtrlMontoMaxDLDespliegue1");
			
		Ext.getCmp("hidCtrlNumDoctosMN1").setValue(eliminaFormatoFlotante(hidCtrlNumDoctosMN.getValue()));
		Ext.getCmp("hidCtrlMontoTotalMNDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoTotalMNDespliegue.getValue()));
		Ext.getCmp("hidCtrlMontoMinMNDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoMinMNDespliegue.getValue()));
		Ext.getCmp("hidCtrlMontoMaxMNDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoMaxMNDespliegue.getValue()));
		
		Ext.getCmp("hidCtrlNumDoctosDL1").setValue(eliminaFormatoFlotante(hidCtrlNumDoctosDL.getValue()));
		Ext.getCmp("hidCtrlMontoTotalDLDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoTotalDLDespliegue.getValue()));
		Ext.getCmp("hidCtrlMontoMinDLDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoMinDLDespliegue.getValue()));
		Ext.getCmp("hidCtrlMontoMaxDLDespliegue1").setValue(eliminaFormatoFlotante(hidCtrlMontoMaxDLDespliegue.getValue()));
				
		if (  ( Ext.isEmpty(hidCtrlNumDoctosMN.getValue())   ||  hidCtrlNumDoctosMN.getValue()==0  ) 
		&& ( Ext.isEmpty(hidCtrlNumDoctosDL.getValue()) ||  hidCtrlNumDoctosDL.getValue()==0 )	 ) {
			Ext.MessageBox.alert("Mensaje","Por favor escriba el  n�mero total documentos en Moneda Nacional y/o D�lares");
			continua = false;
			return;
		}	
		//monedda Nacional
				
		if(!Ext.isEmpty(hidCtrlNumDoctosMN.getValue())  &&   hidCtrlNumDoctosMN.getValue()!=0  ) {
		
			if(Ext.isEmpty(hidCtrlMontoTotalMNDespliegue.getValue()) || hidCtrlMontoTotalMNDespliegue.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(hidCtrlMontoMinMNDespliegue.getValue()) || hidCtrlMontoMinMNDespliegue.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(hidCtrlMontoMaxMNDespliegue.getValue()) || hidCtrlMontoMaxMNDespliegue.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
				continua = false;
				return;
			}
						
			if(  (parseFloat(hidCtrlMontoTotalMNDespliegue.getValue()) < parseFloat(hidCtrlMontoMaxMNDespliegue.getValue()) ) 
				|| (parseFloat(hidCtrlMontoMaxMNDespliegue.getValue()) < parseFloat(hidCtrlMontoMinMNDespliegue.getValue() )  )
				|| (parseFloat(hidCtrlMontoTotalMNDespliegue.getValue) < parseFloat(hidCtrlMontoMinMNDespliegue.getValue()) ) ){
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (Moneda Nacional)");
				continua = false;
				return;
			}		
		}
		
		//moneda Dolar			
			
		if(!Ext.isEmpty(hidCtrlNumDoctosDL.getValue()) &&  hidCtrlNumDoctosDL.getValue()!=0 ) {	
		
			if(Ext.isEmpty(hidCtrlMontoTotalDLDespliegue.getValue()) || hidCtrlMontoTotalDLDespliegue.getValue()==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(hidCtrlMontoMinDLDespliegue.getValue()) || hidCtrlMontoMinDLDespliegue.getValue()==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(hidCtrlMontoMaxDLDespliegue.getValue()) || hidCtrlMontoMaxDLDespliegue.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
				continua = false;
				return;
			}
						
			if( (parseFloat(hidCtrlMontoTotalDLDespliegue.getValue()) < parseFloat(hidCtrlMontoMaxDLDespliegue.getValue())) 
				|| (parseFloat(hidCtrlMontoMaxDLDespliegue.getValue()) < parseFloat(hidCtrlMontoMinDLDespliegue.getValue()) )
				|| (parseFloat(hidCtrlMontoTotalDLDespliegue.getValue) < parseFloat(hidCtrlMontoMinDLDespliegue.getValue()))  ){		
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (D�lares)");
				continua = false;
				return;
			}			
			
		}
		
	 if (Ext.isEmpty(hidCtrlNumDoctosMN.getValue()) ) {  	Ext.getCmp("hidCtrlNumDoctosMN1").setValue(0);  }
		if (Ext.isEmpty(hidCtrlMontoTotalMNDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoTotalMNDespliegue1").setValue(0); }
		if (Ext.isEmpty(hidCtrlMontoMinMNDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoMinMNDespliegue1").setValue(0); }
		if (Ext.isEmpty(hidCtrlMontoMaxMNDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoMaxMNDespliegue1").setValue(0); }
		
		if (Ext.isEmpty(hidCtrlNumDoctosDL.getValue()) ) {  	Ext.getCmp("hidCtrlNumDoctosDL1").setValue(0); }
		if (Ext.isEmpty(hidCtrlMontoTotalDLDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoTotalDLDespliegue1").setValue(0); }
		if (Ext.isEmpty(hidCtrlMontoMinDLDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoMinDLDespliegue1").setValue(0); }
		if (Ext.isEmpty(hidCtrlMontoMaxDLDespliegue.getValue()) ) {  	Ext.getCmp("hidCtrlMontoMaxDLDespliegue1").setValue(0); }
			
				
		var parametros = "hidCtrlNumDoctosMN="+hidCtrlNumDoctosMN.getValue()+"&hidCtrlMontoTotalMNDespliegue="+hidCtrlMontoTotalMNDespliegue.getValue()
		+"&hidCtrlMontoMinMNDespliegue="+hidCtrlMontoMinMNDespliegue.getValue()	+"&hidCtrlMontoMaxMNDespliegue="+hidCtrlMontoMaxMNDespliegue.getValue()
		+"&hidCtrlNumDoctosDL="+hidCtrlNumDoctosDL.getValue()	+"&hidCtrlMontoTotalDLDespliegue="+hidCtrlMontoTotalDLDespliegue.getValue()
		+"&hidCtrlMontoMinDLDespliegue="+hidCtrlMontoMinDLDespliegue.getValue()	+"&hidCtrlMontoMaxDLDespliegue="+hidCtrlMontoMaxDLDespliegue.getValue()
		+"&strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable;
		
		if(continua ==true) {
			document.location.href  = "13forma01aExt.jsp?"+parametros;	
		}				
	}	
	
	var elementosForma = [	
		{ 
			xtype:   'label',  
			html:		'Moneda Nacional', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},		
		{
			xtype: 'numberfield',
			name: 'hidCtrlNumDoctosMN',
			id: 'hidCtrlNumDoctosMN1',
			fieldLabel: 'No. total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 7,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',
			allowNegative : false,			
			margins: '0 20 0 0'		
		},
		{
			xtype: 'textfield',
			name: 'hidCtrlMontoTotalMNDespliegue',
			id: 'hidCtrlMontoTotalMNDespliegue1',
			fieldLabel: 'Monto total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("hidCtrlMontoTotalMNDespliegue1").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		},
		{
			xtype: 'textfield',
			name: 'hidCtrlMontoMinMNDespliegue',
			id: 'hidCtrlMontoMinMNDespliegue1',
			fieldLabel: 'Monto m�nimo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("hidCtrlMontoMinMNDespliegue1").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		},
		{
			xtype: 'textfield',
			name: 'hidCtrlMontoMaxMNDespliegue',
			id: 'hidCtrlMontoMaxMNDespliegue1',
			fieldLabel: 'Monto m�ximo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("hidCtrlMontoMaxMNDespliegue1").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		},
		{ 
			xtype:   'label',  
			html:		'D�lares', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 'numberfield',
			name: 'hidCtrlNumDoctosDL',
			id: 'hidCtrlNumDoctosDL1',
			fieldLabel: 'No. total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 7,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',	
			allowNegative : false,			
			margins: '0 20 0 0'		
		},
		{
			xtype: 'textfield',
			name: 'hidCtrlMontoTotalDLDespliegue',
			id: 'hidCtrlMontoTotalDLDespliegue1',
			fieldLabel: 'Monto total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("hidCtrlMontoTotalDLDespliegue1").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		},
		{
			xtype: 'textfield',
			name: 'hidCtrlMontoMinDLDespliegue',
			id: 'hidCtrlMontoMinDLDespliegue1',
			fieldLabel: 'Monto m�nimo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("hidCtrlMontoMinDLDespliegue1").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		},
		{
			xtype: 'textfield',
			name: 'hidCtrlMontoMaxDLDespliegue',
			id: 'hidCtrlMontoMaxDLDespliegue1',
			fieldLabel: 'Monto m�ximo de los documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("hidCtrlMontoMaxDLDespliegue1").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		}
	];
	
	var fpCifras = new Ext.form.FormPanel({
		id: 'fpCifras',
		width: 500,
		title: 'Carga Individual',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Enviar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: enviar
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: limpiar			
			}			
		]
	});
	
	
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fpCifras				
		]
	});
	
	
} );