Ext.onReady(function(){
	Ext.QuickTips.init(); 
//-----------------------------------HANDLERS-----------------------------------
	var procesarConsultaData = function (store,arrRegistros,opts){
		if(arrRegistros != null){
			var grid = Ext.getCmp('grid');
			var el = grid.getGridEl();
			if(store.getTotalCount()>0){
				el.unmask();
			}else{
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	var procesarSuccessFailureActualizarDatos = function (opts, success, response) {
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			Ext.Msg.alert('Mensaje informativo',Ext.util.JSON.decode(response.responseText).mensaje,function(btn){   
				location.href="/nafin/13descuento/13pki/13epo/13forma11ext.jsp";
			});
		} else {
			Ext.Msg.alert('Mensaje de Error','�Error al actualizar los datos!',function(btn){  
					return;	 
			});
		}
	}
//------------------------------------STORE-------------------------------------
	var consultaData = new Ext.data.JsonStore({
		root: 'registros',
		url: '13forma11ext.data.jsp',
		baseParams: {
			informacion : 'Consulta'
		},
		fields:[
				{name: 'IC_IF'},
				{name: 'CG_RAZON_SOCIAL'},
				{name: 'FG_PUNTOS_REBATE',convert: function(value, row) { 
													return (value == 0) ? null : parseFloat(value);}
				},
				{name: 'PUNTOS_REBATE'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
							fn: function(proxy,type,action,optionRequest,response,args){
								NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
								procesarConsultaData(null,null,null);
							}
			}
		}
	});
//----------------------------------COMPONENTES---------------------------------
	var grid = new Ext.grid.EditorGridPanel({
			id: 'grid',
			title: 'Captura Puntos Rebate',
			style: 'margin:0 auto;',
			store: consultaData,
			hidden: false,
			clicksToEdit: 1,
			columns: [			
							{
								header: 'Intermediario Financiero',
								tooltip: 'Intermediario Financiero',
								dataIndex: 'CG_RAZON_SOCIAL',
								sortable: true,
								resiazable: true,
								width: 400
							},
							{
								header: 'Puntos Rebate',
								tooltip: 'Puntos Rebate',
								dataIndex: 'FG_PUNTOS_REBATE',
								sortable: 'true',
								resiazable: true,
								width: 200,
								align: 'center',
								editor: {
									xtype: 'numberfield',
									name: 'puntos_rebate',
									id: 'puntosRebate',
									allowBlank: true,
									allowNegative: false,
									msgTarget: 'side',
									decimalPrecision: 5,
									maxValue: 99.99999,
									minValue:0.00001
								}
							}
						],
			stripeRows: true,
			loadMask: true,
			height: 400,
			width: 600,
			title: 'Datos Documento Inicial',
			frame: true,
			bbar: {
				pageSize: 15,
				buttonAlign: 'left',
				displayInfo: true,
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: "No hay registros.",
				items: [
							'-',
							{
								//xtype: 'button',
								text: 'Guardar Cambios',
								id: 'btnGuardar',
								formBind: true,
								handler: function(boton,evento){
									var store = consultaData;
									var modifiedRecords = store.getModifiedRecords();
									if (modifiedRecords.length > 0){
										//Creamos un array con los datos de los registros que han cambiado
										var puntosRebate = new Array();
										for(var i=0; i<modifiedRecords.length; i++){
											var ic_if = modifiedRecords[i].data.IC_IF;
											if(modifiedRecords[i].data.FG_PUNTOS_REBATE==""){
												modifiedRecords[i].data.PUNTOS_REBATE = 'null' + '|' + ic_if;
											}else{
												modifiedRecords[i].data.PUNTOS_REBATE = modifiedRecords[i].data.FG_PUNTOS_REBATE + '|' + ic_if;
											}
											puntosRebate.push(modifiedRecords[i].data.PUNTOS_REBATE);
										}
										//Codificamos los cambios en JSON para poderlos enviar
										puntosRebate = Ext.util.JSON.encode(puntosRebate);
										Ext.Ajax.request({
											url: '13forma11ext.data.jsp',
											params: {
												informacion: 'Guardar',
												puntosRebate: puntosRebate
											},
											callback: procesarSuccessFailureActualizarDatos
										});
									}
								}
							},
							'-',
							{
								xtype: 'button',
								text: 'Cancelar',
								id: 'btnCancelar',
								handler: function (boton,evento){
									var StoreGeneral = consultaData;
									StoreGeneral.rejectChanges();
								}
							},
							'-'
					]
			}
	});
//----------------------------------PRINCIPAL-----------------------------------	
		var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			//NE.util.getEspaciador(20),
			grid
		]
	});
	consultaData.load();
}); 