<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.netro.exception.*,
	com.netro.descuento.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>

<%
	String proceso = (request.getParameter("proceso")!=null)?request.getParameter("proceso"):"0";
	String hidCtrlNumDoctosMN = (request.getParameter("hidCtrlNumDoctosMN")!=null)?request.getParameter("hidCtrlNumDoctosMN"):"0";
	String hidCtrlMontoTotalMN = (request.getParameter("hidCtrlMontoTotalMN")!=null)?request.getParameter("hidCtrlMontoTotalMN"):"0";
	String hidCtrlNumDoctosDL = (request.getParameter("hidCtrlNumDoctosDL")!=null)?request.getParameter("hidCtrlNumDoctosDL"):"0";
	String hidCtrlMontoTotalDL = (request.getParameter("hidCtrlMontoTotalDL")!=null)?request.getParameter("hidCtrlMontoTotalDL"):"0";
		
	String strPendiente = (request.getParameter("strPendiente") != null) ? request.getParameter("strPendiente") : "N";
	String strPreNegociable = (request.getParameter("strPreNegociable") != null) ? request.getParameter("strPreNegociable") : "N";

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
	CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);

	Vector nombresCampo = CargaDocumentos.getCamposAdicionales(iNoCliente);
	int numeroCampos=  nombresCampo.size();

	//se obtienes los campos adicionales
	String strCampos  ="", nomCampo1 ="", nomCampo2 ="", nomCampo3="", nomCampo4="", nomCampo5 ="";
	for (int i=0; i<nombresCampo.size(); i++) { 			
		Vector lovRegistro = (Vector) nombresCampo.get(i);
		strCampos += (String) lovRegistro.get(0);	
		if(i==0) 	nomCampo1 = (String) lovRegistro.get(1);
		if(i==1) 	nomCampo2 = (String) lovRegistro.get(1);
		if(i==2) 	nomCampo3 = (String) lovRegistro.get(1); 
		if(i==3) 	nomCampo4 = (String) lovRegistro.get(1);
		if(i==4) 	nomCampo5 = (String) lovRegistro.get(1);			
	}
	
	String  bOperaFactConMandato = "N",  bOperaFactorajeVencido  ="N", bOperaFactorajeDistribuido = "", 
	bOperaNotasCredito   ="N", bTipoFactoraje ="N",  bOperaFactorajeVencidoInfonavit  ="N"; //Fodea 042-2009 Vencimiento Infonavit
	String bValidaDuplicidad = "";
	
	Hashtable alParamEPO1 = new Hashtable();
	alParamEPO1 = BeanParamDscto.getParametrosEPO(iNoCliente,1);	
		
	if (alParamEPO1!=null) {
		bOperaFactConMandato = alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString();
		bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
		bOperaFactorajeDistribuido = alParamEPO1.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString();
		bOperaNotasCredito = alParamEPO1.get("OPERA_NOTAS_CRED").toString();
		bOperaFactorajeVencidoInfonavit =alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString();
		bValidaDuplicidad = alParamEPO1.get("VALIDA_DUPLIC").toString();
	}
	if(bOperaFactorajeVencido.equals("S") || bOperaFactorajeDistribuido.equals("S")  || bOperaFactConMandato.equals("S")) {
		bTipoFactoraje ="S";
	}
	
	String 	sPubEPOPEFFlag	= new String(""),		sPubEPOPEFFlagVal	= new String(""),		sPubEPOPEFDocto1		= new String(""),
	sPubEPOPEFDocto1Val	= new String(""),		sPubEPOPEFDocto2	= new String(""),	sPubEPOPEFDocto2Val	= new String(""),
	sPubEPOPEFDocto3		= new String(""),	sPubEPOPEFDocto3Val		= new String(""),	sPubEPOPEFDocto4	= new String(""),	
	sPubEPOPEFDocto4Val		= new String(""),  sOperaFactConMandato   = new String("");
					
	ArrayList alParamEPO = BeanParamDscto.getParamEPO(iNoCliente, 1);
	if (alParamEPO!=null) {  
		//estos son los valores que se consultaron
		//este es el orden en el que me van a llegar los datos pues ordene la consulta por asc de nombre del campo
		sPubEPOPEFDocto3						= (alParamEPO.get(15)==null)?"":alParamEPO.get(15).toString();//PUB_EPO_PEF_CLAVE_PRESUPUESTAL
		sPubEPOPEFDocto3Val					= (alParamEPO.get(16)==null)?"":alParamEPO.get(16).toString();
		sPubEPOPEFDocto1						= (alParamEPO.get(17)==null)?"":alParamEPO.get(17).toString();//PUB_EPO_PEF_FECHA_ENTREGA
		sPubEPOPEFDocto1Val					= (alParamEPO.get(18)==null)?"":alParamEPO.get(18).toString();
		sPubEPOPEFFlag							= (alParamEPO.get(19)==null)?"":alParamEPO.get(19).toString();//PUB_EPO_PEF_FECHA_RECEPCION
		sPubEPOPEFFlagVal						= (alParamEPO.get(20)==null)?"":alParamEPO.get(20).toString();
		sPubEPOPEFDocto4						= (alParamEPO.get(21)==null)?"":alParamEPO.get(21).toString();//PUB_EPO_PEF_PERIODO
		sPubEPOPEFDocto4Val					= (alParamEPO.get(22)==null)?"":alParamEPO.get(22).toString();
		sPubEPOPEFDocto2						= (alParamEPO.get(23)==null)?"":alParamEPO.get(23).toString();//PUB_EPO_PEF_TIPO_COMPRA
		sPubEPOPEFDocto2Val					= (alParamEPO.get(24)==null)?"":alParamEPO.get(24).toString();
		sPubEPOPEFDocto2Val					= (alParamEPO.get(24)==null)?"":alParamEPO.get(24).toString();			
	}
	boolean bVenSinOperar=false;	String bVenSinOperarS ="N";
	bVenSinOperar = CargaDocumentos.publicarDocVencidos(iNoCliente);	
	if(bVenSinOperar) bVenSinOperarS ="S";
	
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>

<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>

<script type="text/javascript" src="13forma01bExt.js?<%=session.getId()%>"></script>

<%@ include file="/00utils/componente_firma.jspf" %>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
	
<form id='formParametros' name="formParametros">
	<input type="hidden" id="proceso" name="proceso" value="<%=proceso%>"/>
	<input type="hidden" id="strPendiente" name="strPendiente" value="<%=strPendiente%>"/>	
	<input type="hidden" id="strPreNegociable" name="strPreNegociable" value="<%=strPreNegociable%>"/>	
	<input type="hidden" id="operaNC" name="operaNC" value="<%=operaNC%>"/>	
	
	
	<input type="hidden" id="hayCamposAdicionales" name="hayCamposAdicionales" value="<%= String.valueOf(numeroCampos)%>"/>	
	<input type="hidden" id="nomCampo1" name="nomCampo1" value="<%=nomCampo1%>"/>	
	<input type="hidden" id="nomCampo2" name="nomCampo2" value="<%=nomCampo2%>"/>
	<input type="hidden" id="nomCampo3" name="nomCampo3" value="<%=nomCampo3%>"/>	
	<input type="hidden" id="nomCampo4" name="nomCampo4" value="<%=nomCampo4%>"/>	
	<input type="hidden" id="nomCampo5" name="nomCampo5" value="<%=nomCampo5%>"/>	
	<input type="hidden" id="operaFVPyme" name="operaFVPyme" value="<%=operaFVPyme%>"/>	
	<input type="hidden" id="bOperaFactorajeVencido" name="bOperaFactorajeVencido" value="<%=bOperaFactorajeVencido%>"/>	
	<input type="hidden" id="bOperaFactorajeVencido" name="bOperaFactorajeVencido" value="<%=bOperaFactorajeVencido%>"/>	
	<input type="hidden" id="bOperaFactConMandato" name="bOperaFactConMandato" value="<%=bOperaFactConMandato%>"/>	
	<input type="hidden" id="bOperaFactorajeVencidoInfonavit" name="bOperaFactorajeVencidoInfonavit" value="<%=bOperaFactorajeVencidoInfonavit%>"/>	
	<input type="hidden" id="bOperaFactorajeDistribuido" name="bOperaFactorajeDistribuido" value="<%=bOperaFactorajeDistribuido%>"/>	
	<input type="hidden" id="sPubEPOPEFFlagVal" name="sPubEPOPEFFlagVal" value="<%=sPubEPOPEFFlagVal%>"/>	
	<input type="hidden" id="bOperaFactConMandato" name="sPubEPOPEFFlagVal" value="<%=bOperaFactConMandato%>"/>	
	<input type="hidden" id="bValidaDuplicidad" name="bValidaDuplicidad" value="<%=bValidaDuplicidad%>"/>
	
	
	<input type="hidden" id="hidCtrlNumDoctosMN" name="hidCtrlNumDoctosMN" value="<%=hidCtrlNumDoctosMN%>"/>	
	<input type="hidden" id="hidCtrlMontoTotalMN" name="hidCtrlMontoTotalMN" value="<%=hidCtrlMontoTotalMN%>"/>	
	<input type="hidden" id="hidCtrlNumDoctosDL" name="hidCtrlNumDoctosDL" value="<%=hidCtrlNumDoctosDL%>"/>	
	<input type="hidden" id="hidCtrlMontoTotalDL" name="hidCtrlMontoTotalDL" value="<%=hidCtrlMontoTotalDL%>"/>	
	<input type="hidden" id="bVenSinOperarS" name="bVenSinOperarS" value="<%=bVenSinOperarS%>"/>	
	
	
	
</form>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
