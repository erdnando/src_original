Ext.onReady(function() {
  
    var procesarSuccessValoresIni = function(opts, success, response) {
	if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
	    var resp = 	Ext.util.JSON.decode(response.responseText);	
	    var objMsg = Ext.getCmp('mensajes1');
	    objMsg.body.update('La revisi�n jur�dica de los documentos est� acotada a la identificaci�n de la existencia de alg�n embargo trabado.<br>'+
			    '<br> Los documentos est�n bloqueados y no podr�n ser operados por el Intermediario Financiero hasta en tanto no se realice la revisi�n jur�dica. ');
	    objMsg.show();
	    objGral.mapNombres = resp.mapNombres;
	    objGral.numCamposAdic = resp.numCamposAdic;	   
	  
	    for(var i= 0; i<objGral.numCamposAdic; i++) {	
		var nombreColumna = eval("resp.mapNombres.CAMPO"+i);
		if(i===0){
		    catalogoCampo1Data.load();    
		    Ext.getCmp('campo_1').show();   
		    Ext.getCmp('campo_1').label.update(nombreColumna);
		}
		if(i===1){
		    catalogoCampo2Data.load();    
		    Ext.getCmp('campo_2').show();   
		    Ext.getCmp('campo_2').label.update(nombreColumna);
		}
		if(i===2){
		    catalogoCampo3Data.load();    
		    Ext.getCmp('campo_3').show();  
		    Ext.getCmp('campo_3').label.update(nombreColumna);
		}
		if(i===3){
		    catalogoCampo4Data.load();   
		    Ext.getCmp('campo_4').show();   
		    Ext.getCmp('campo_4').label.update(nombreColumna);
		}
		if(i===4){
		    catalogoCampo5Data.load();  
		    Ext.getCmp('campo_5').show();   
		    Ext.getCmp('campo_4').label.update(nombreColumna);
		}
		
		
		fp.el.mask('Enviando...', 'x-mask-loading');
		    consultaData.load({
			params: Ext.apply(fp.getForm().getValues())
		    });
	   }	
	}
    };
        
   var catalogoCampo1Data = new Ext.data.JsonStore({
	id: 'catalogoCampo1Data',root : 'registros',
	fields : ['clave', 'descripcion','icPyme','loadMsg'],
	url : '13formaDistribuido.data.jsp',
	baseParams: {
	    informacion: 'catalogoCampo1Data'
	},
	totalProperty : 'total',autoLoad: false,
	listeners: {	    
	    exception: NE.util.mostrarDataProxyError,
	    beforeload: NE.util.initMensajeCargaCombo
	}
    });
    
   var catalogoCampo2Data = new Ext.data.JsonStore({
	id: 'catalogoCampo2Data',root : 'registros',
	fields : ['clave', 'descripcion','icPyme','loadMsg'],
	url : '13formaDistribuido.data.jsp',
	baseParams: {
	    informacion: 'catalogoCampo2Data'
	},
	totalProperty : 'total',autoLoad: false,
	listeners: {	    
	    exception: NE.util.mostrarDataProxyError,
	    beforeload: NE.util.initMensajeCargaCombo
	}
    });
  
  var catalogoCampo3Data = new Ext.data.JsonStore({
	id: 'catalogoCampo3Data',root : 'registros',
	fields : ['clave', 'descripcion','icPyme','loadMsg'],
	url : '13formaDistribuido.data.jsp',
	baseParams: {
	    informacion: 'catalogoCampo3Data'
	},
	totalProperty : 'total',autoLoad: false,
	listeners: {	    
	    exception: NE.util.mostrarDataProxyError,
	    beforeload: NE.util.initMensajeCargaCombo
	}
    });
    
    var catalogoCampo4Data = new Ext.data.JsonStore({
	id: 'catalogoCampo4Data',root : 'registros',
	fields : ['clave', 'descripcion','icPyme','loadMsg'],
	url : '13formaDistribuido.data.jsp',
	baseParams: {
	    informacion: 'catalogoCampo4Data'
	},
	totalProperty : 'total',autoLoad: false,
	listeners: {	    
	    exception: NE.util.mostrarDataProxyError,
	    beforeload: NE.util.initMensajeCargaCombo
	}
    });
    
     var catalogoCampo5Data = new Ext.data.JsonStore({
	id: 'catalogoCampo5Data',root : 'registros',
	fields : ['clave', 'descripcion','icPyme','loadMsg'],
	url : '13formaDistribuido.data.jsp',
	baseParams: {
	    informacion: 'catalogoCampo5Data'
	},
	totalProperty : 'total',autoLoad: false,
	listeners: {	    
	    exception: NE.util.mostrarDataProxyError,
	    beforeload: NE.util.initMensajeCargaCombo
	}
    });
  
    var objGral={
	mapNombres:null,
	numCamposAdic:null,
	acuse:null,
	acuseFormateado:null,
	fechaValida:null,
	horaValida:null,	
	recibo:null
    };
  
    var modificados = [];
    var totalDoctosMN =0;
    var totalMontoDoctosMN = 0;
    var	totalMontoDescontarMN = 0;
    var monedaMN ='';
    var totalDoctosDL =0;
    var totalMontoDoctosDL = 0;
    var	totalMontoDescontarDL = 0;
    var monedaDL ='';
    
    var limpiaSelec =  function() {  
	modificados = [];	
	totalDoctosMN =0;
	 totalMontoDoctosMN = 0;
	totalMontoDescontarMN = 0;
	monedaMN ='';
	totalDoctosDL =0;
	totalMontoDoctosDL = 0;
	totalMontoDescontarDL = 0;
	monedaDL ='';
    };
        
    function procesarArchivos(opts, success, response){
	if (success === true && Ext.util.JSON.decode(response.responseText).success ===  true){
	    var infoR = Ext.util.JSON.decode(response.responseText);
	    var archivo = infoR.urlArchivo;
	    archivo = archivo.replace('/nafin', '');
	    var params = {nombreArchivo: archivo};
	    fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
	    fp.getForm().getEl().dom.submit();	    
	    if(objGral.acuse===null){
		var botonCvs=Ext.getCmp('btnGeneraCSV');  
		botonCvs.setIconClass('icoXls'); 
		botonCvs.enable();
	    }else  if(objGral.acuse!==null){	    
		var botonPdf=Ext.getCmp('btnImpPDF'); 	
		botonPdf.setIconClass('icoPdf');  
		botonPdf.enable();		
		var botoncsvA=Ext.getCmp('btnImpCSV'); 	
		botoncsvA.setIconClass('icoXls');  
		botoncsvA.enable();
		
	    }  
	} else{
	    NE.util.mostrarConnError(response, opts);
	}
    }; 
    
    var procesarSuccessFailureAcuse =  function(opts, success, response) {
	if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
	   var  resp = Ext.util.JSON.decode(response.responseText);			
	    if (resp != null){		
		 var menAcuse1 = Ext.getCmp('menAcuse1');
		 Ext.getCmp('mensajes1').hide();
		 
		 
		if( resp.mostrarAcuse ==='S')  {		  
		    Ext.getCmp("gridPreAcuse").setTitle('Acuse Validaci�n de Documentos'); 
		    objGral.acuse = resp.folioCert;
		    objGral.fechaValida = resp.fecha;
		    objGral.horaValida = resp.hora;		   
		    objGral.recibo = resp.recibo;
		    objGral.acuseFormateado=resp.acuse;    
		    
		    var acuseCifras = [
			['N�mero de Acuse', resp.acuse],
			['Fecha de Validaci�n', resp.fecha],
			['Hora de Validaci�n', resp.hora],
			['Usuario de Validaci�n', resp.usuario]
		    ];		    
		    storeCifrasData.loadData(acuseCifras);
		    Ext.getCmp("gridCifrasCtrl").show();   
		    menAcuse1.update(resp.mensaje);  		
		    Ext.getCmp("btnRegresar").hide();    
		    Ext.getCmp("btnRevisionJR").hide();
		    Ext.getCmp("btnCancelar").hide();	 
		    Ext.getCmp("btnImpPDF").show();
		    Ext.getCmp("btnImpCSV").show();		    
		    Ext.getCmp("btnSalir").show();

		}else  if( resp.mostrarAcuse ==='N')  {		
		   menAcuse1.update(resp.mensaje);	
		   Ext.getCmp("gridPreAcuse").hide();
		   Ext.getCmp("gridTotales").hide();		   
		}
		menAcuse1.show();	
	    }					
	} else {
	    NE.util.mostrarConnError(response,opts);
	   
	}
    };
	    
	var procesaRevision = function() {
	
		var textoFirmar ='La revisi�n jur�dica de los documentos est� acotada a la identificaci�n de la existencia de alg�n embargo trabado.\n'+
				'Los documentos est�n bloqueados y no podr�n ser operados por el Intermediario Financiero hasta en tanto no se realice la revisi�n jur�dica.';
		
		var gridDoc = Ext.getCmp('gridConsulta');
		var store = gridDoc.getStore();		
		store.each(function(record) {	    		
			if(record.data['SELECCION']==='S'){			
				textoFirmar += 	'Clave del Proveedor: '+record.data['CLAVE_PROVEEDOR']+','+'Nombre del Proveedor: ' +record.data['NOMBRE_PROVEEDOR']+','+'N�mero de Documento: '+record.data['NUM_DOCUMENTO']+','+'Fecha de Emisi�n: '+record.data['FECHA_EMISION']+','+
						'Fecha de Vencimiento: '+record.data['FECHA_VENC']+','+	'Moneda: '+record.data['MONEDA']+','+'Monto del Documento: '+record.data['MONTO_DOCTO']+','+'Monto a Descontar: '+record.data['MONTO_DESCONTAR']+','+
						'N�mero de Contrato: '+record.data['NUM_CONTRATO']+','+	'N�mero COPADE: '+record.data['NUM_COPADE']+','+'Clave Cesionario: '+record.data['CLAVE_CESIONARIO']+','+'Nombre Cesionario (Beneficiario): '+record.data['NOMBRE_CESIONARIO']+','+
						'Clave del Int. Financiero: '+record.data['CLAVE_INTERMEDIARIO']+','+'IF Fondeador: '+record.data['IF_FONDEADOR']+','+'\n';
			}	    
		});	
		NE.util.obtenerPKCS7(confirmarRevision, textoFirmar);
	};

	var confirmarRevision = function(pkcs7, textoFirmar) {
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnRevisionJR").enable();
			return;
		} else {
			Ext.getCmp("btnRevisionJR").disable();
			Ext.Ajax.request({
				url : '13formaDistribuido.data.jsp',
				params : {
					pkcs7: pkcs7,
					textoFirmado: textoFirmar,
					informacion: 'ConfirmaRevision',
					modificados:modificados,
					totalDoctosMN:totalDoctosMN,
					totalMontoDoctosMN:totalMontoDoctosMN,
					totalMontoDescontarMN:totalMontoDescontarMN,
					totalDoctosDL:totalDoctosDL,
					totalMontoDoctosDL:totalMontoDoctosDL,
					totalMontoDescontarDL:totalMontoDescontarDL
				},
				callback: procesarSuccessFailureAcuse
			});	
		}
	}


    var proContinuar = function() {
    
	var store = Ext.getCmp('gridConsulta').getStore();	
	limpiaSelec();
	var registrosPreAcu = [];
	var dataTotales = [];	
	var selec =0;
	
	store.each(function(record) {	
	    if(record.data['SELECCION']==='S'){			
		modificados.push(record.data['IC_DOCUMENTO']);
		registrosPreAcu.push(record);	
		
		if( record.data['IC_MONEDA']==='1') {	
		    totalDoctosMN++;
		    totalMontoDoctosMN += parseFloat(record.data['MONTO_DOCTO']);
		    totalMontoDescontarMN += parseFloat(record.data['MONTO_DESCONTAR']);
		    monedaMN =record.data['MONEDA'];
		    
		}else if( record.data['IC_MONEDA']==='54') {
		    totalDoctosDL++;
		    totalMontoDoctosDL += parseFloat(record.data['MONTO_DOCTO']);
		    totalMontoDescontarDL += parseFloat(record.data['MONTO_DESCONTAR']);
		    monedaDL =record.data['MONEDA'];
		}
		if(totalDoctosMN>0 && totalDoctosDL>0  ){
		    dataTotales = [
		    [ monedaMN,totalDoctosMN, totalMontoDoctosMN, totalMontoDescontarMN ],
		    [ monedaDL,totalDoctosDL, totalMontoDoctosDL, totalMontoDescontarDL ]
		    ];
		}
		if(totalDoctosMN>0 && totalDoctosDL===0){
		    dataTotales = [[ monedaMN,totalDoctosMN, totalMontoDoctosMN, totalMontoDescontarMN ]];
		}		
		if(totalDoctosMN===0 && totalDoctosDL>0){
		    dataTotales = [[ monedaDL,totalDoctosDL, totalMontoDoctosDL, totalMontoDescontarDL ]];
		}
		selec++;
	    }	    
	});
		
	if(selec>0){
	
	    consultaPreAcuseData.removeAll();
	    consultaPreAcuseData.add(registrosPreAcu);
	    
	    storeMontosData.removeAll();
	    storeMontosData.loadData(dataTotales);	
		
	    fp.hide();
	    gridPreAcuse.show();
	    gridConsulta.hide();
	    gridTotales.show();	   
	
	}else  {
	    Ext.MessageBox.alert('Mensaje','Debe seleccionar por lo menos un documento para continuar');
	    return false;	  
	}    
    };
    
    
  //  --- GRID DE TOTALES  PREACUSE Y ACUSE --------
    var storeCifrasData = new Ext.data.ArrayStore({
	fields: [   {	name: 'etiqueta'},   {	name: 'informacion'}  ]
    });    
    
    var gridCifrasCtrl = new Ext.grid.GridPanel({
	id: 'gridCifrasCtrl',
	store: storeCifrasData,
	margins: '20 0 0 0',
	hidden: true,
	hideHeaders : true,
	columns: [
	    {
		header : 'Etiqueta',  dataIndex : 'etiqueta',  width : 150, sortable : true   },
	    {   header : 'Informacion',	tooltip: 'Informacion',	dataIndex : 'informacion',width : 230,	sortable : true, renderer:  function (causa, columna, registro){
		    columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
		    return causa;
		}
	    }
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 400,
	style: 'margin:0 auto;',
	autoHeight : true,
	title: 'Cifras de Control',
	frame: true
    });
       
  //  --- GRID DE TOTALES  PREACUSE Y ACUSE --------  
    var storeMontosData = new Ext.data.ArrayStore({
	fields: [    {	name: 'moneda' },   {	name: 'totalDoctos', type: 'float'},   { name: 'totalMontoDoctos', type: 'float'},  {name: 'totalMontoDescontar', type: 'float'} ]
    });
    
    var gridTotales = new Ext.grid.EditorGridPanel({	
	id: 'gridTotales',		
	store: storeMontosData,
	margins: '20 0 0 0',	
	hidden: true,
	align: 'center',
	columns: [
	    {	header : 'Moneda',  tooltip: 'moneda',	dataIndex : 'moneda',	width : 150, align: 'center', 	hidden: false,	sortable : false     },
	    {	header : 'Total de documentos.',  tooltip: 'totalDoctos',  dataIndex : 'totalDoctos', width : 150, align: 'center', sortable : false },
	    {	header : 'Total Monto de documentos.',	tooltip: 'totalMontoDoctos',dataIndex : 'totalMontoDoctos',width : 150,	sortable : false,align:'right',	renderer: Ext.util.Format.numberRenderer('$0,0.00')    },
	    {	header : 'Total Monto a Descontar.',	tooltip: 'totalMontoDescontar',	dataIndex : 'totalMontoDescontar', width : 150,	sortable : false, align:'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')   }
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 100,
	width: 610,
	autoHeight : true,
	style: 'margin:0 auto;',
	title: 'Totales',
	frame: true
    });
  
 //  --- GRID DE PREACUSE Y ACUSE --------
  
    var consultaPreAcuseData = new Ext.data.ArrayStore({
	fields: [ {name: 'SELECCION'}, {name: 'IC_DOCUMENTO'}, {name: 'CLAVE_PROVEEDOR'}, {name: 'NOMBRE_PROVEEDOR'},
	    {name: 'NUM_DOCUMENTO'},  {name: 'FECHA_EMISION'}, {name: 'FECHA_VENC'},  {name: 'IC_MONEDA'},  {name: 'MONEDA'},
	    {name: 'MONTO_DOCTO'},  {name: 'MONTO_DESCONTAR'}, {name: 'CAMPO0'},  {name: 'CAMPO1'},  {name: 'CAMPO2'},  {name: 'CAMPO3'},
	    {name: 'CAMPO4'},  {name: 'NUM_CONTRATO'},  {name: 'NUM_COPADE'},  {name: 'CLAVE_CESIONARIO'},  {name: 'NOMBRE_CESIONARIO'},
	    {name: 'CLAVE_INTERMEDIARIO'},   {name: 'IF_FONDEADOR'}
	]
    });
      
    var gridPreAcuse = new Ext.grid.GridPanel({
	id: 'gridPreAcuse',
	store: consultaPreAcuseData,
	margins: '20 0 0 0',
	//licksToEdit: 1,	
	title: 'PreAcuse Validaci�n de Documentos',
	hidden: true,
	columns: [	   		
	    { header: 'Clave del Proveedor', 	tooltip: 'Clave del Proveedor', dataIndex: 'CLAVE_PROVEEDOR', sortable: true, width: 150, resizable: true, align:'center'    },
	    { header: 'Nombre del Proveedor', tooltip: 'Nombre del Proveedor', dataIndex: 'NOMBRE_PROVEEDOR',	sortable: true,	width: 150,resizable: true   },
	    { header: 'N�mero de Documento',	tooltip: 'N�mero de Documento',	dataIndex: 'NUM_DOCUMENTO',sortable: true, width: 150, 	resizable: true, align:'center'    },
	    { header: 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n', 	dataIndex: 'FECHA_EMISION',  sortable: true,  width: 150, resizable: true, align:'center'    },
	    { header: 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	dataIndex: 'FECHA_VENC', sortable: true,width: 150,resizable: true,align:'center'   },
	    { header: 'Moneda', tooltip: 'Moneda', dataIndex: 'MONEDA', sortable: true, width: 150,resizable: true   },
	    { header: 'Monto del Documento', tooltip: 'Monto del Documento', dataIndex: 'MONTO_DOCTO', 	sortable: true, width: 150, resizable: true, align:'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')  },
	    { header: 'Monto a Descontar', tooltip: 'Monto a Descontar', dataIndex: 'MONTO_DESCONTAR', 	sortable: true, width: 150, resizable: true,  align:'right', renderer: Ext.util.Format.numberRenderer('$0,0.00') },
	    { header: 'Campo1', tooltip: 'Campo1', dataIndex: 'CAMPO0', sortable: true, hidden: true, 	width: 150, resizable: true  },
	    { header: 'Campo2', tooltip: 'Campo2', dataIndex: 'CAMPO1', sortable: true, hidden: true, 	width: 150, resizable: true  },
	    { header: 'Campo3', tooltip: 'Campo3', dataIndex: 'CAMPO2', sortable: true, hidden: true, 	width: 150, resizable: true  },
	    { header: 'Campo4', tooltip: 'Campo4', dataIndex: 'CAMPO3', sortable: true, hidden: true, 	width: 150, resizable: true  },
	    { header: 'Campo5', tooltip: 'Campo5', dataIndex: 'CAMPO4', sortable: true, hidden: true, 	width: 150, resizable: true  },
	    { header: 'N�mero de Contrato', tooltip: 'N�mero de Contrato',dataIndex: 'NUM_CONTRATO', sortable: true, width: 150,resizable: true, align:'center'    },
	    { header: 'N�mero COPADE', 	tooltip: 'N�mero COPADE',dataIndex: 'NUM_COPADE', sortable: true, width: 150,  resizable: true,	align:'center'   },	  
	    { header: 'Clave Cesionario', tooltip: 'Clave Cesionario',	dataIndex: 'CLAVE_CESIONARIO',	sortable: true,	width: 150, resizable: true, align:'center'     },	  
	    { header: 'Nombre Cesionario (Beneficiario)', tooltip: 'Nombre Cesionario (Beneficiario)',	dataIndex: 'NOMBRE_CESIONARIO',	sortable: true,	width: 200,resizable: true   },
	    { header: 'Clave del Int. Financiero', tooltip: 'Clave del Int. Financiero', dataIndex: 'CLAVE_INTERMEDIARIO', sortable: true,width: 150, resizable: true, 	align:'center'  },
	    { header: 'IF Fondeador', tooltip: 'IF Fondeador',	dataIndex: 'IF_FONDEADOR',sortable: true, width: 200,resizable: true   }
	],
	displayInfo: true,		
	emptyMsg: "No hay registros.",
	loadMask: true,	
	stripeRows: true,
	columnLines : true,
	height: 400,
	width: 943,
	frame: false,
	bbar: {
	    xtype: 'toolbar',
	    items: [
	        '->',   '-',
		{   text: 'Regresar',   iconCls: 'icoRegresar',   id: 'btnRegresar',
		    handler: function(boton, evento){		    
			Ext.getCmp('gridConsulta').show();
			Ext.getCmp('forma').show();
			Ext.getCmp('gridPreAcuse').hide();
			Ext.getCmp('gridTotales').hide();
		    }
		},
	        {   text: 'Revisi�n Juridica',    iconCls: 'icoAutorizar',    id: 'btnRevisionJR',    handler: procesaRevision	},
		{   text: 'Cancelar',    iconCls: 'cancelar',    id: 'btnCancelar',
		    handler: function(boton, evento){
			Ext.Msg.confirm('Confirmaci�n', '�Est� usted seguro de cancelar la operaci�n?',	function(botonConf) {
			    if (botonConf === 'ok' || botonConf === 'yes') {
				window.location = '13formaDistribuidoExt.jsp';
			    }
			});				
		    }
		},
		{
		    text: 'Generar Archivo',    iconCls: 'icoXls',    id: 'btnImpCSV',    hidden: true,
		    handler: function(boton, evento){
			boton.disable();
			boton.setIconClass('loading-indicator');
			Ext.Ajax.request({
			    url : '13formaDistribuido.data.jsp',
			    params : {
				informacion: 'ArchivoCSV',
				acuse: objGral.acuse,
				acuseFormateado:objGral.acuseFormateado,
				fechaValida: objGral.fechaValida,
				horaValida:objGral.horaValida ,				
				recibo:objGral.recibo,
				totalDoctosMN:totalDoctosMN,
				totalMontoDoctosMN:totalMontoDoctosMN,
				totalMontoDescontarMN:totalMontoDescontarMN,
				totalDoctosDL:totalDoctosDL,
				totalMontoDoctosDL:totalMontoDoctosDL,
				totalMontoDescontarDL:totalMontoDescontarDL,
				tipoArch: 'CSV'
			    },
			    callback: procesarArchivos
			});
		    }	
		},	
		{
		    text: 'Imprimir',    iconCls: 'icoPdf',    id: 'btnImpPDF',    hidden: true,
		    handler: function(boton, evento){
			boton.disable();
			boton.setIconClass('loading-indicator');
			Ext.Ajax.request({
			    url : '13formaDistribuido.data.jsp',
			    params : {
				informacion: 'GeneraPDF',
				acuse: objGral.acuse,
				acuseFormateado:objGral.acuseFormateado,
				fechaValida: objGral.fechaValida,
				horaValida:objGral.horaValida ,				
				recibo:objGral.recibo,
				totalDoctosMN:totalDoctosMN,
				totalMontoDoctosMN:totalMontoDoctosMN,
				totalMontoDescontarMN:totalMontoDescontarMN,
				totalDoctosDL:totalDoctosDL,
				totalMontoDoctosDL:totalMontoDoctosDL,
				totalMontoDescontarDL:totalMontoDescontarDL,
				tipoArch: 'PDF'
			    },
			    callback: procesarArchivos
			});
		    }	
		},		
		{
		    text: 'Salir',    iconCls: 'cancelar',    id: 'btnSalir',    hidden: true,
		    handler: function(boton, evento){
			window.location = '13formaDistribuidoExt.jsp';
		    }
		}
	    ]
	}
    });
		
   // -------------------------Consulta  -----------------------------------------------
    
    var procesarConsultaData = function(store, arrRegistros, opts) {
	var fp = Ext.getCmp('forma');		
	fp.el.unmask();
	var el = gridConsulta.getGridEl();
	var gridCons = gridConsulta.getColumnModel();
	
	if (arrRegistros != null) {
	    if (!gridConsulta.isVisible()) {
		gridConsulta.show();
	    }
	   
	    if(store.getTotalCount() > 0) {	
	    
		var numCamposAdic =  objGral.numCamposAdic;	
		var indice = 9;		
		for(var x=0; x<numCamposAdic; x++){				
		    var nombreColumna = eval("objGral.mapNombres.CAMPO"+x);								
		    //Grid de consulta
		    gridCons.setColumnHeader(indice,nombreColumna);
		    gridCons.setHidden(indice,false);		    
		    indice = indice+1;							
		}					    
	    
	    // Para el Acuse 	   
		var gridPreAcuse =  Ext.getCmp('gridPreAcuse');
		var gridPreA = gridPreAcuse.getColumnModel();
		var indicei = 8;	
		for(var i=0; i<numCamposAdic; i++){				
		    var nombreColumnai = eval("objGral.mapNombres.CAMPO"+i);								
		    //Grid de preAcuse y acuse 
		    gridPreA.setColumnHeader(indicei,nombreColumnai);
		    gridPreA.setHidden(indicei,false);
		    indicei = indicei+1;							
		}
		
		
		Ext.getCmp('btnGeneraCSV').enable();
		Ext.getCmp('btnContinuar').enable();
		el.unmask();
	    }else  {
	    
		Ext.getCmp('btnGeneraCSV').disable();
		Ext.getCmp('btnContinuar').disable();
		el.mask('No se encontr� ning�n registro', 'x-mask');
	    }
			
	}
		
    };
   
    var consultaData = new Ext.data.JsonStore({
	root : 'registros',
	url : '13formaDistribuido.data.jsp',
	baseParams: {
	    informacion: 'Consultar'
	},
	fields: [ {name: 'SELECCION'}, {name: 'IC_DOCUMENTO'}, {name: 'CLAVE_PROVEEDOR'}, {name: 'NOMBRE_PROVEEDOR'},
	    {name: 'NUM_DOCUMENTO'},  {name: 'FECHA_EMISION'}, {name: 'FECHA_VENC'},  {name: 'IC_MONEDA'},  {name: 'MONEDA'},
	    {name: 'MONTO_DOCTO'},  {name: 'MONTO_DESCONTAR'}, {name: 'CAMPO0'},  {name: 'CAMPO1'},  {name: 'CAMPO2'},  {name: 'CAMPO3'},
	    {name: 'CAMPO4'},  {name: 'NUM_CONTRATO'},  {name: 'NUM_COPADE'},  {name: 'CLAVE_CESIONARIO'},  {name: 'NOMBRE_CESIONARIO'},
	    {name: 'CLAVE_INTERMEDIARIO'},   {name: 'IF_FONDEADOR'}   
	],
	totalProperty : 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
	    load: procesarConsultaData,			
	    exception: {
		fn: function(proxy, type, action, optionsRequest, response, args) {
		    NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
		    procesarConsultaData(null, null, null);						
		}
	    }
	}
    });	
   
    
    var selectModel = new Ext.grid.CheckboxSelectionModel({
	checkOnly: true,	
	listeners: {	    
	    rowdeselect: function(selectModel, rowIndex, record) {
		record.data['SELECCION']='N';
		record.commit();	
		return true;			
            },
	    beforerowselect: function( selectModel, rowIndex, keepExisting, record ){				
		record.data['SELECCION']='S';
		record.commit();	
		return true;		
	    }
        }
    });
    
    var gridConsulta = new Ext.grid.EditorGridPanel({
	id: 'gridConsulta',
	store: consultaData,
	margins: '20 0 0 0',
	licksToEdit: 1,	
	sm: selectModel,
	hidden: true,
	title: 'Consulta',
	columns: [
	    selectModel,	    
	    { header: 'Clave del Proveedor', 	tooltip: 'Clave del Proveedor', dataIndex: 'CLAVE_PROVEEDOR', sortable: true, width: 150, resizable: true, align:'center'    },
	    { header: 'Nombre del Proveedor', tooltip: 'Nombre del Proveedor', dataIndex: 'NOMBRE_PROVEEDOR',	sortable: true,	width: 150,resizable: true   },
	    { header: 'N�mero de Documento',	tooltip: 'N�mero de Documento',	dataIndex: 'NUM_DOCUMENTO',sortable: true, width: 150, 	resizable: true, align:'center'    },
	    { header: 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n', 	dataIndex: 'FECHA_EMISION',  sortable: true,  width: 150, resizable: true, align:'center'    },
	    { header: 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	dataIndex: 'FECHA_VENC', sortable: true,width: 150,resizable: true,align:'center'   },
	    { header: 'Moneda', tooltip: 'Moneda', dataIndex: 'MONEDA', sortable: true, width: 150,resizable: true   },
	    { header: 'Monto del Documento', tooltip: 'Monto del Documento', dataIndex: 'MONTO_DOCTO', 	sortable: true, width: 150, resizable: true, align:'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')  },
	    { header: 'Monto a Descontar', tooltip: 'Monto a Descontar', dataIndex: 'MONTO_DESCONTAR', 	sortable: true, width: 150, resizable: true,  align:'right', renderer: Ext.util.Format.numberRenderer('$0,0.00') },
	    { header: 'Campo1', tooltip: 'Campo1', dataIndex: 'CAMPO0', sortable: true, hidden: true, 	width: 150, resizable: true  },
	    { header: 'Campo2', tooltip: 'Campo2', dataIndex: 'CAMPO1', sortable: true, hidden: true, 	width: 150, resizable: true  },
	    { header: 'Campo3', tooltip: 'Campo3', dataIndex: 'CAMPO2', sortable: true, hidden: true, 	width: 150, resizable: true  },
	    { header: 'Campo4', tooltip: 'Campo4', dataIndex: 'CAMPO3', sortable: true, hidden: true, 	width: 150, resizable: true  },
	    { header: 'Campo5', tooltip: 'Campo5', dataIndex: 'CAMPO4', sortable: true, hidden: true, 	width: 150, resizable: true  },
	    { header: 'N�mero de Contrato', tooltip: 'N�mero de Contrato',dataIndex: 'NUM_CONTRATO', sortable: true, width: 150,resizable: true, align:'center'    },
	    { header: 'N�mero COPADE', 	tooltip: 'N�mero COPADE',dataIndex: 'NUM_COPADE', sortable: true, width: 150,  resizable: true,	align:'center'   },	  
	    { header: 'Clave Cesionario', tooltip: 'Clave Cesionario',	dataIndex: 'CLAVE_CESIONARIO',	sortable: true,	width: 150, resizable: true, align:'center'     },	  
	    { header: 'Nombre Cesionario (Beneficiario)', tooltip: 'Nombre Cesionario (Beneficiario)',	dataIndex: 'NOMBRE_CESIONARIO',	sortable: true,	width: 200,resizable: true   },
	    { header: 'Clave del Int. Financiero', tooltip: 'Clave del Int. Financiero', dataIndex: 'CLAVE_INTERMEDIARIO', sortable: true,width: 150, resizable: true, 	align:'center'  },
	    { header: 'IF Fondeador', tooltip: 'IF Fondeador',	dataIndex: 'IF_FONDEADOR',sortable: true, width: 200,resizable: true   }
	],
	stripeRows: true,
	displayInfo: true,		
	emptyMsg: "No hay registros.",
	loadMask: true,	
	height: 400,
	width: 943,
	frame: false,	
	bbar: {
	    xtype: 'toolbar',
	    items: [
	        '->',  '-',  
		{
		    text: 'Generar Archivo',   iconCls: 'icoXls',   id: 'btnGeneraCSV',
		    handler: function(boton, evento){
			boton.disable();
			boton.setIconClass('loading-indicator');
			Ext.Ajax.request({
			    url: '13formaDistribuido.data.jsp',
			    params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'ArchivoCSV',
				tipoArch: 'CSV'
			    }),
			    callback: procesarArchivos
			});
		    }					
		},
		{   text: 'Continuar',   iconCls: 'icoContinuar',   id: 'btnContinuar',    handler: proContinuar }			
	    ]
	}
    });
	
   
   // -------------------------------- Criterios de B�squeda-------------------------------
   
    var successAjaxFn = function(opts, success, response) {
	if (success === true && Ext.util.JSON.decode(response.responseText).success === true) {
	    var jsonObj = Ext.util.JSON.decode(response.responseText);			
	    Ext.getCmp('hidNombre').setValue(jsonObj.txtNombre);
	    Ext.getCmp('icPyme').setValue(jsonObj.icPyme);			
	}
    };

    var procesarCatalogoNombreData = function(store, arrRegistros, opts) {
	var jsonData = store.reader.jsonData;
	if (jsonData.success){
	    Ext.getCmp('btnBuscar').enable();
	    if (jsonData.total === "excede"){
		Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
		Ext.getCmp('fpWinBusca').getForm().reset();
		Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
	    }else if (jsonData.total === "0"){
		Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
		Ext.getCmp('fpWinBusca').getForm().reset();
		Ext.getCmp('fpWinBuscaB').getForm().reset();
		return;
	    }
	    Ext.getCmp('cmbNumNe').focus();
	}
    };
	

    var busqAvanzadaSelect=0;
    
    var catalogoNombreData = new Ext.data.JsonStore({
	id: 'catalogoNombreStore',
	root : 'registros',
	fields : ['clave', 'descripcion','icPyme','loadMsg'],
	url : '13formaDistribuido.data.jsp',
	baseParams: {
	    informacion: 'CatalogoBuscaAvanzada'
	},
	totalProperty : 'total',
	autoLoad: false,
	listeners: {
	    load: procesarCatalogoNombreData,
	    exception: NE.util.mostrarDataProxyError,
	    beforeload: NE.util.initMensajeCargaCombo
	}
    });
  	
    var  elementosForma = [
	{  xtype: 'hidden',   name: 'icPyme',    id: 'icPyme'  },
	{  xtype: 'compositefield',   width: 900,    fieldLabel: 'Nombre de la PYME',  labelWidth: 200, combineErrors: false,   msgTarget: 'side',
	    items:[			
			{
			    xtype: 'numberfield',    name: 'txtNafelec',    id:'txtNafelec',    width:90, maxLength: 38, fieldLabel: 'Nombre de la PYME', msgTarget: 'side',  margins: '0 20 0 0', allowBlank: true,
			    listeners: {
				'blur': function(){			
				    Ext.getCmp('hidNombre').setValue('');
				    Ext.getCmp('icPyme').setValue('');				
				    Ext.Ajax.request({  
					url: '13formaDistribuido.data.jsp',  
					method: 'POST',  
					callback: successAjaxFn,  
					params: {  
					    informacion: 'pymeNombre' ,
					    txtNafelec: Ext.getCmp('txtNafelec').getValue()
					}  
				    });
				}
			    }
			},
			{ xtype: 'textfield',  value: '',  width:290, disabled: true,  id:'hidNombre',name:'hidNombre',   hiddenName : 'hidNombre',  mode: 'local',   resizable: true,   triggerAction : 'all' }
	    ]
	},
    	{
	    xtype: 'compositefield',
	    combineErrors: false,
	    msgTarget: 'side',
	    items: [
		{  xtype:'displayfield',  id:'disEspacio',    text:''},
		{    xtype: 'button',  text: 'Busqueda Avanzada',   id: 'btnAvanzada',    width: 100,    iconCls:'icoBuscar',    handler: function(boton, evento) {
			var winVen = Ext.getCmp('winBuscaA');
			if (winVen){
			    Ext.getCmp('fpWinBusca').getForm().reset();
			    Ext.getCmp('fpWinBuscaB').getForm().reset();
			    Ext.getCmp('cmbNumNe').store.removeAll();
			    Ext.getCmp('cmbNumNe').reset();
			    winVen.show();										
			}else{
			    new Ext.Window ({
				id:'winBuscaA',
				height: 300,
				x: 300,
				y: 100,
				width: 600,
				modal: true,
				closeAction: 'hide',
				title: '',
				items:[
				    {
					xtype:'form',
					id:'fpWinBusca',
					frame: true,
					border: false,
					style: 'margin: 0 auto',
					bodyStyle:'padding:10px',
					defaults: {
					    msgTarget: 'side',
					    anchor: '-20'
					},
					labelWidth: 140,
					items:[
					    { xtype:'displayfield', frame:true, border: false, value:'Utilice el * para b�squeda gen�rica'   },
					    { xtype:'displayfield', frame:true, border: false, value:'' },
					    { xtype: 'textfield', name: 'nombrePyme', id: 'txtNombre', fieldLabel:'Nombre', maxLength:40 },
					    { xtype: 'textfield', name: 'rfcPyme', id:'txtRfc',	fieldLabel:'RFC',maxLength:20   },
					    { xtype: 'textfield',name: 'numPyme', id:'txtNe', fieldLabel:'N�mero Proveedor', maxLength:25 }
					],
					buttons:[
					    {	text:'Buscar', id:'btnBuscar',iconCls:'icoBuscar',
						handler: function(boton) {
						    catalogoNombreData.load({ 
							params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues(),{					    
							})																																				
						    });
						}
					    },
					    {
						text:'Cancelar',iconCls: 'icoLimpiar',
						handler: function() {
						    Ext.getCmp('winBuscaA').hide();
						}
					    }
					]
				    },
				    {
					xtype:'form',frame: true, id:'fpWinBuscaB',style: 'margin: 0 auto',bodyStyle:'padding:10px', monitorValid: true,
					defaults: {    msgTarget: 'side',   anchor: '-20'},
					items:[
					    {
						xtype: 'combo', id:'cmbNumNe',  name: 'cmbNumNe',hiddenName : 'HcmbNumNe',fieldLabel: 'Nombre',emptyText: 'Seleccione . . .',
						displayField: 'descripcion', valueField: 'clave',triggerAction : 'all',	forceSelection:true,
						allowBlank: false,typeAhead: true,mode: 'local',minChars : 1, 	store: catalogoNombreData,
						tpl : NE.util.templateMensajeCargaCombo,
						listeners: {
						    select: function(combo, record, index) {
							busqAvanzadaSelect=index;
						    }
						}
					    }
					],
					buttons:[
					    {
						text:'Aceptar',	iconCls:'aceptar',formBind:true,
						handler: function() {
						    if (!Ext.isEmpty(Ext.getCmp('cmbNumNe').getValue())){
							var reg = Ext.getCmp('cmbNumNe').getStore().getAt(busqAvanzadaSelect).get('icPyme');
							Ext.getCmp('icPyme').setValue(reg);
							var disp = Ext.getCmp('cmbNumNe').lastSelectionText;
							var desc = disp.slice(disp.indexOf(" ")+1);							
							Ext.getCmp('txtNafelec').setValue(Ext.getCmp('cmbNumNe').getValue());							
							Ext.getCmp('hidNombre').setValue(desc);
							Ext.getCmp('winBuscaA').hide();
						    }
						}
					    },
					    {
						text:'Cancelar',iconCls: 'icoLimpiar',
						handler: function() {	
						    Ext.getCmp('winBuscaA').hide();	
						}
					    }
					]
				    }
				]
			    }).show();	
			}
		    }
		}
	    ]
	},		
	{
	    xtype: 'compositefield',   fieldLabel: 'Fecha de Vencimiento',   combineErrors: false,   msgTarget: 'side',   width: 500,
	    items: [
		{  xtype: 'datefield',   name: 'dfFechaVencMin',   id: 'dfFechaVencMin',   allowBlank: true,    startDay: 0,   width: 150,    msgTarget: 'side',   vtype: 'rangofecha',    campoFinFecha: 'dfFechaVencMax',   margins: '0 20 0 0' },
		{  xtype: 'displayfield',  value: 'a',   width: 20 },
		{  xtype: 'datefield',    name: 'dfFechaVencMax',   id: 'dfFechaVencMax',   allowBlank: true,   startDay: 1,   width: 150,   msgTarget: 'side',  vtype: 'rangofecha',  campoInicioFecha: 'dfFechaVencMin',  margins: '0 20 0 0'  }
	    ]
	},	
	{
	    xtype: 'fieldset',
	    title: 'Utilice el * para b�squeda gen�rica',
	    autoHeight: true,
	    items: [
		{   xtype: 'compositefield',  fieldLabel: 'N�mero de Contrato',  id: 'numContrato_',   combineErrors: false,   msgTarget: 'side', width: 500,
		    items: [
			{ xtype: 'textfield', name: 'numContrato', id: 'numContrato1', fieldLabel: 'N�mero de Contrato',  allowBlank: true,  maxLength: 10, width: 150, msgTarget: 'side',  margins: '0 20 0 0'	}
		    ]
		},
		{ xtype: 'compositefield', fieldLabel: 'N�mero de Documento', id: 'numDocumento_', combineErrors: false,  msgTarget: 'side', width: 500,
		    items: [
			{   xtype: 'textfield',    name: 'numDocumento',   id: 'numDocumento1', fieldLabel: 'N�mero de Documento',   allowBlank: true,  maxLength: 15,  width: 150,   msgTarget: 'side',   margins: '0 20 0 0'  }
		    ]
		},
		{
		    xtype: 'compositefield',   fieldLabel: 'N�mero de COPADE', id: 'numCopade_',   combineErrors: false,   msgTarget: 'side',  width: 500,
		    items: [
			{   xtype: 'textfield',   name: 'numCopade',   id: 'numCopade1',   fieldLabel: 'N�mero de COPADE',  allowBlank: true,   maxLength: 10,	 width: 150, msgTarget: 'side', margins: '0 20 0 0'  }
		    ]
		}
	    ]
	},
	{  xtype: 'compositefield',  fieldLabel: 'Campo 1 ',   combineErrors: false,  msgTarget: 'side', id: 'campo_1', width: 500, hidden:true,
	    items: [
		{   xtype: 'combo',   name: 'campo1',   id: 'campo1',   fieldLabel: 'Campo 1',   mode: 'local',    displayField : 'descripcion',  valueField : 'clave', hiddenName : 'campo1',
		    emptyText: 'Seleccione...',	 forceSelection : true, triggerAction : 'all',  typeAhead: true,  minChars : 1, allowBlank: true,  store : catalogoCampo1Data, tpl : NE.util.templateMensajeCargaCombo, width: 400   }
	    ]
	},
	{   xtype: 'compositefield',  fieldLabel: 'Campo 2 ',  combineErrors: false,  msgTarget: 'side',  id: 'campo_2', width: 500, hidden:true,
	    items: [
		{  xtype: 'combo',   name: 'campo2',   id: 'campo2',  fieldLabel: 'Campo 2',mode: 'local',    displayField : 'descripcion',   valueField : 'clave', hiddenName : 'campo2',   emptyText: 'Seleccione...', forceSelection : true, 
		triggerAction : 'all', typeAhead: true, minChars : 1,  allowBlank: true, store : catalogoCampo2Data, tpl : NE.util.templateMensajeCargaCombo,  width: 400}
	    ]
	},
	{   xtype: 'compositefield',  fieldLabel: 'Campo 3 ', combineErrors: false, msgTarget: 'side',  id: 'campo_3', width: 500,  hidden:true,
	    items: [
		{   xtype: 'combo',   name: 'campo3', id: 'campo3', fieldLabel: 'Campo 3', mode: 'local', displayField : 'descripcion', valueField : 'clave', hiddenName : 'campo3',   emptyText: 'Seleccione...', forceSelection : true, 
		triggerAction : 'all',typeAhead: true, minChars : 1, allowBlank: true,  store : catalogoCampo3Data,   tpl : NE.util.templateMensajeCargaCombo,   width: 400 }
	    ]
	},
	{   xtype: 'compositefield',   fieldLabel: 'Campo 4 ', combineErrors: false,  msgTarget: 'side',   id: 'campo_4', width: 500, hidden:true,
	    items: [
		{  xtype: 'combo',  name: 'campo4',  id: 'campo4',  fieldLabel: 'Campo 4',  mode: 'local',  displayField : 'descripcion',  valueField : 'clave',  hiddenName : 'campo4',  emptyText: 'Seleccione...', forceSelection : true,
		    triggerAction : 'all',  typeAhead: true,  minChars : 1, allowBlank: true, store : catalogoCampo4Data, tpl : NE.util.templateMensajeCargaCombo,width: 400 }
	    ]
	},
	{  xtype: 'compositefield',  fieldLabel: 'Campo 5 ', combineErrors: false, msgTarget: 'side', id: 'campo_5', width: 500,  hidden:true,
	    items: [
		{ xtype: 'combo',  name: 'campo5',  id: 'campo5',  fieldLabel: 'Campo 5',mode: 'local',   displayField : 'descripcion',  valueField : 'clave',   hiddenName : 'campo5',  emptyText: 'Seleccione...',	forceSelection : true,
		    triggerAction : 'all', typeAhead: true, minChars : 1, allowBlank: true, store : catalogoCampo5Data, tpl : NE.util.templateMensajeCargaCombo, width: 400 }
	    ]
	}	
    ];

    var fp = new Ext.form.FormPanel({
	id: 'forma',width: 600,	title: 'Criterios de B�squeda',	frame: true, titleCollapse: false,style: 'margin:0 auto;',
	bodyStyle: 'padding: 6px',labelWidth: 150,defaultType: 'textfield',defaults: {   msgTarget: 'side',  anchor: '-20'},
	items: elementosForma,			
	monitorValid: true,
	buttons: [
	    {
		text: 'Consultar', id: 'btnAConsultar',	iconCls: 'icoBuscar',formBind: true,
		handler: function() {		    
		    var dfFechaVencMin = Ext.getCmp('dfFechaVencMin');
		    var dfFechaVencMax = Ext.getCmp('dfFechaVencMax');
					
		    if(!Ext.isEmpty(dfFechaVencMin.getValue()) || !Ext.isEmpty(dfFechaVencMax.getValue())){
			if(Ext.isEmpty(dfFechaVencMin.getValue())){
			    dfFechaVencMin.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			    dfFechaVencMin.focus();
			    return;
			}
			if(Ext.isEmpty(dfFechaVencMax.getValue())){
			    dfFechaVencMax.markInvalid('Debe capturar ambas fechas o dejarlas en blanco');
			    dfFechaVencMax.focus();
			    return;
			}
		    }			
					
		    fp.el.mask('Enviando...', 'x-mask-loading');
		    consultaData.load({
			params: Ext.apply(fp.getForm().getValues())
		    });
		}
	    },					
	    {
		text: 'Limpiar',iconCls: 'icoLimpiar',handler: function() {
		  Ext.getCmp('forma').getForm().reset();		  
		}			
	    }
	]
    });	    
    
    var pnl = new Ext.Container  ({
	id: 'contenedorPrincipal',applyTo: 'areaContenido',style: 'margin:0 auto;',width: 949,height: 'auto',
	items: 	[
	    NE.util.getEspaciador(20),
	    { xtype: 'panel',name: 'mensajes',id: 'mensajes1',width: 600,frame: true,hidden: true,style: 'margin:0 auto;'   },  
	    { xtype: 'label',name: 'menAcuse',id: 'menAcuse1',	hidden: true, style: 'font-weight:bold;text-align:center;display:block;', fieldLabel: '', text: ''  },
	    NE.util.getEspaciador(20),
	    gridCifrasCtrl,
	    fp,
	    NE.util.getEspaciador(20),
	    gridPreAcuse,
	    gridConsulta,
	    NE.util.getEspaciador(20),
	    gridTotales,
	    NE.util.getEspaciador(20)
	]
    });	
    
    Ext.Ajax.request({
	url: '13formaDistribuido.data.jsp',
	params: {
	    informacion: 'valoresIniciales'
	},
	callback: procesarSuccessValoresIni
    });
		
} );