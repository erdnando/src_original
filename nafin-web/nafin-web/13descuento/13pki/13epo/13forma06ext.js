Ext.onReady(function() {

	var param;
	function procesarSuccessFailureParametrizacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			param = Ext.util.JSON.decode(response.responseText);
			pnl.el.unmask();
			//fp.el.unmask();
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureCaptura =  function(opts, success, response) {
		Ext.getCmp('btnAceptar').hide();
		Ext.getCmp('btnCancelar').hide();
		Ext.getCmp('btnRegresar').show();
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			grid.setTitle('<div align="center">Recibo: ' + infoR._acuse + '</div>');
			Ext.MessageBox.alert('Cambios Guardados','<b>La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito.<br>Recibo: '+infoR._acuse+'</b>');
		} else {
			fp.hide();
			fpCausa.hide();
			grid.hide();
			if (Ext.getCmp('gridTotalesA').isVisible()){
				Ext.getCmp('gridTotalesA').hide();
			}
			var jsonE = Ext.util.JSON.decode(response.responseText);
			var mensaje = jsonE.msg;
			mensaje = (!mensaje)?'':Ext.util.Format.nl2br(mensaje);
			Ext.getCmp('tabla').hide();
			Ext.getCmp('panelError').body.update('<b>'+mensaje+'</b>');
			Ext.getCmp('panelError').show();
			//NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaConsulta(opts, success, response) {
		//fp.el.unmask();
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR != undefined){
				consultaData.loadData('');
				if (!grid.isVisible()){
					grid.show();
				}
				if (Ext.getCmp('gridTotalesA').isVisible()){
					Ext.getCmp('gridTotalesA').hide();
				}
				if (infoR.registros != undefined){
					consultaData.loadData(infoR.registros);
					var cm = grid.getColumnModel();
					cm.setColumnHeader(cm.findColumnIndex('CD_DESCRIPCION'), 'Estatus');
					cm.setColumnTooltip(cm.findColumnIndex('CD_DESCRIPCION'), 'Estatus');
					cm.setHidden(cm.findColumnIndex('DF_FECHA_VENC_PYME'), true);
					cm.setHidden(cm.findColumnIndex('NUEVO_ESTATUS'), true);
					cm.setHidden(cm.findColumnIndex('USUARIO'), true);
					cm.setHidden(cm.findColumnIndex('FLAG_CHECK'), false);
					if (param.sFechaVencPyme != undefined && param.sFechaVencPyme != ""){
						cm.setHidden(cm.findColumnIndex('DF_FECHA_VENC_PYME'), false);
					}
					grid.getGridEl().unmask();
					Ext.getCmp('btnTotales').enable();
					if (!fpCausa.isVisible()){
						fpCausa.show();
					}
				}else{
					grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					Ext.getCmp('btnTotales').disable();
				}
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'IF_CG_RAZON_SOCIAL'},	//Nombre if
			{name: 'IG_NUMERO_DOCTO'},              //Num docto.
			{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CD_DESCRIPCION'},
			{name: 'CD_NOMBRE'},                    //Moneda
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_PORC_ANTICIPO'},
			{name: 'FN_MONTO_DSCTO', type: 'float'},
			{name: 'IN_IMPORTE_INTERES', type: 'float'},
			{name: 'IN_IMPORTE_RECIBIR', type: 'float'},
			{name: 'DF_FECHA_VENC_PYME', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FLAG_CHECK'},
			{name: 'NUEVO_ESTATUS'},
			{name: 'USUARIO'},
			{name: 'TIPO_FACTORAJE'}
		],
		data:[{
			'IC_MONEDA':'',	'IC_DOCUMENTO':'',	'CG_RAZON_SOCIAL':'',	'IF_CG_RAZON_SOCIAL':'',	'IG_NUMERO_DOCTO':'',	'DF_FECHA_VENC':'',	'CD_DESCRIPCION':'',	'CD_NOMBRE':'',
			'FN_MONTO':'',	'FN_PORC_ANTICIPO':'',	'FN_MONTO_DSCTO':'',	'IN_IMPORTE_INTERES':'',	'IN_IMPORTE_RECIBIR':'',	'DF_FECHA_VENC_PYME':'',	'FLAG_CHECK':'',
			'NUEVO_ESTATUS':'','USUARIO':''	}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var misTotales = [
			{'MONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_DOCUMENTOS':'','TOTAL_MONTO_RECGAR':'','TOTAL_MONTO_DESCUENTO':'','TOTAL_MONTO_INTERES':'','TOTAL_MONTO_RECIBIR':''},
			{'MONEDA':'','TOTAL_REGISTROS':'','TOTAL_MONTO_DOCUMENTOS':'','TOTAL_MONTO_RECGAR':'','TOTAL_MONTO_DESCUENTO':'','TOTAL_MONTO_INTERES':'','TOTAL_MONTO_RECIBIR':''}
	];
	
	var resumenTotalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'NOMMONEDA'},
			{name: 'TOTAL_REGISTROS'},
			{name: 'TOTAL_MONTO_DOCUMENTOS'},
			{name: 'TOTAL_MONTO_RECGAR'},
			{name: 'TOTAL_MONTO_DESCUENTO'},
			{name: 'TOTAL_MONTO_INTERES'},
			{name: 'TOTAL_MONTO_RECIBIR'}
		],
		data: misTotales,
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

    var catTipoFactorajeData = new Ext.data.JsonStore({
	root : 'registros',
	fields : ['clave', 'descripcion', 'loadMsg'],
	url : '13forma06ext.data.jsp',
	baseParams: {
	    informacion: 'catTipoFactorajeData'
	},
	totalProperty : 'total',
	autoLoad: false,
	listeners: {
	    exception: NE.util.mostrarDataProxyError,
	    beforeload: NE.util.initMensajeCargaCombo
	}
    });
    
	var catalogoNombrePymeData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma06ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoNombrePyme'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoIfData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma06ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIf'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma06ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var gridTotalesA = {
		xtype: 'grid',
		store: resumenTotalesData,
		id: 'gridTotalesA',
		hidden:	true,
		view: new Ext.grid.GridView({markDirty: false, forceFit: true}),
		columns: [
			{
				header: 'TOTALES',	dataIndex: 'NOMMONEDA',	align: 'left',	width: 190
			},{
				header: 'Registros',	dataIndex: 'TOTAL_REGISTROS',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('0,000')
			},{
				header: 'Monto Documentos',dataIndex: 'TOTAL_MONTO_DOCUMENTOS',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header: 'Monto Recurso',	dataIndex: 'TOTAL_MONTO_RECGAR',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header: 'Monto Descuento',	dataIndex: 'TOTAL_MONTO_DESCUENTO',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header: 'Monto Interes',	dataIndex: 'TOTAL_MONTO_INTERES',	width: 120,	align: 'right', renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},{
				header: 'Monto Operar',	dataIndex: 'TOTAL_MONTO_RECIBIR',	width: 120,	align: 'right',	renderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			}
		],
		width: 940,
		height: 100,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	};

	var grid = new Ext.grid.EditorGridPanel({
		store: consultaData,
		hidden: true,
		header: true,
		view: new Ext.grid.GridView({markDirty: false}),
		columns: [
			{
				header: 'Nombre de PYME', tooltip: 'Nombre de PYME',	dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true, width: 200, resizable: true, hidden: false
			},{
				header: 'Nombre de IF', tooltip: 'Nombre de IF',	dataIndex: 'IF_CG_RAZON_SOCIAL',
				sortable: true, width: 200, resizable: true, hidden: false
			},{
				header: 'N�mero del documento', tooltip: 'N�mero del documento',	dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true, width: 150, resizable: true, hidden: false,align: 'center'
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	dataIndex : 'DF_FECHA_VENC',
				sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: false
			},{
				header : 'Fecha Vencimiento Pyme', tooltip: 'Fecha Vencimiento Pyme',	dataIndex : 'DF_FECHA_VENC_PYME',
				sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: true, hideable: false
			},{
				header : 'Estatus', tooltip: 'Estatus',	dataIndex : 'CD_DESCRIPCION',	// && Estatus Anterior
				sortable : true, width : 200, hidden: false,hideable: false, align: 'center'
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'CD_NOMBRE',
				sortable : true, width : 150, hidden: false,align: 'center'
			},
			{
				header : 'Tipo de Factoraje', tooltip: 'Tipo de Factoraje',	dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 150, hidden: false,align: 'center'
			},			
			{
				header : 'Monto', tooltip: 'Monto',	dataIndex : 'FN_MONTO',
				sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('0,0.00'), hidden: false, hideable: true
			},{
				header : 'Porcentaje de Descuento', tooltip: 'Porcentaje de Descuento',	dataIndex : 'FN_PORC_ANTICIPO',
				sortable : true, width : 150,	align: 'right', resizable: true, hidden: false,hideable: true,
				renderer: Ext.util.Format.numberRenderer('0 %')
			},{
				header : 'Recurso en garant�a', tooltip: 'Recurso en garant�a',	dataIndex : '',
				sortable : true, width : 100, align: 'right', hidden: false, hideable: true,
				renderer:function(value, metaData, registro, rowIndex, colIndex, store) {
								value = 0;
								value = (parseFloat(registro.get('FN_MONTO')) - parseFloat(registro.get('FN_MONTO_DSCTO')));
								return Ext.util.Format.number(value, '0,0.00');
							}
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'FN_MONTO_DSCTO',
				sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('0,0.00'), hidden: false, hideable: true
			},{
				header : 'Monto a Interes', tooltip: 'Monto a Interes',	dataIndex : 'IN_IMPORTE_INTERES',
				sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('0,0.00'), hidden: false, hideable: true
			},{
				header : 'Monto a Operar', tooltip: 'Monto a Operar',	dataIndex : 'IN_IMPORTE_RECIBIR',
				sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('0,0.00'), hidden: false, hideable: true
			},{
				xtype: 'checkcolumn',
				header : 'Seleccionar',	tooltip:	'Seleccionar',	dataIndex : 'FLAG_CHECK',
				width : 100, align:'center',	sortable : false, hideable: false
			},{	//	14
				header : 'Nuevo Estatus', tooltip: 'Nuevo Estatus',	dataIndex : 'NUEVO_ESTATUS',
				sortable : true, width : 200, hidden: true,hideable: false, align: 'center'
			},{	//	15
				header : 'Usuario', tooltip: 'Usuario',	dataIndex : 'USUARIO',
				sortable : true, width : 250, hidden: true,hideable: false, align: 'center'
			}
		],
		clicksToEdit: 1,
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: '',
		frame: false,
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'center',
			items: ['-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					align:'center',
					disabled: true,
					hidden: false,
					handler: function(boton, evento) {
						var totalesACmp = Ext.getCmp('gridTotalesA');
						if (!totalesACmp.isVisible()) {
							totalesACmp.show();
							totalesACmp.el.dom.scrollIntoView();
						}
						resumenTotalesData.loadData(misTotales);
						var storeGrid = consultaData;
						var recurso = 0;
						var sumMN=0, montoTotalMN=0,montoTotalDescuentoMN=0,importeTotalInteresMN=0,importeTotalRecibirMN=0,importeTotalRecGarMN=0;
						var sumDL=0, montoTotalDL=0,montoTotalDescuentoDL=0,importeTotalInteresDL=0,importeTotalRecibirDL=0,importeTotalRecGarDL=0;

						storeGrid.each(function(registro){
								recurso = 0;
								if (	Ext.isEmpty(registro.get('FN_MONTO_DSCTO'))	){
									registro.get('FN_MONTO_DSCTO') = 0;
								}
								recurso = registro.get('FN_MONTO') - registro.get('FN_MONTO_DSCTO');
								if(registro.get('IC_MONEDA') == "1"){
									sumMN++;
									montoTotalMN				+=	registro.get('FN_MONTO');
									montoTotalDescuentoMN	+= registro.get('FN_MONTO_DSCTO');
									importeTotalInteresMN	+= registro.get('IN_IMPORTE_INTERES');
									importeTotalRecibirMN	+=	registro.get('IN_IMPORTE_RECIBIR');
									importeTotalRecGarMN		+=	recurso;
								}else if(registro.get('IC_MONEDA') == "54"){
									sumDL++;
									montoTotalDL				+=	registro.get('FN_MONTO');
									montoTotalDescuentoDL	+= registro.get('FN_MONTO_DSCTO');
									importeTotalInteresDL	+= registro.get('IN_IMPORTE_INTERES');
									importeTotalRecibirDL	+=	registro.get('IN_IMPORTE_RECIBIR');
									importeTotalRecGarDL		+=	recurso;
								}
						});
						var regMN = resumenTotalesData.getAt(0);
						var regDL = resumenTotalesData.getAt(1);
						if (sumMN > 0){
							regMN.set('NOMMONEDA','MONEDA NACIONAL');
							regMN.set('TOTAL_REGISTROS',sumMN);
							regMN.set('TOTAL_MONTO_DOCUMENTOS',montoTotalMN);
							regMN.set('TOTAL_MONTO_RECGAR',importeTotalRecGarMN);
							regMN.set('TOTAL_MONTO_DESCUENTO',montoTotalDescuentoMN);
							regMN.set('TOTAL_MONTO_INTERES',importeTotalInteresMN);
							regMN.set('TOTAL_MONTO_RECIBIR',importeTotalRecibirMN);
						}
						if (sumDL > 0){
							regDL.set('NOMMONEDA','DOLARES AMERICANOS');
							regDL.set('TOTAL_REGISTROS',sumDL);
							regDL.set('TOTAL_MONTO_DOCUMENTOS',montoTotalDL);
							regDL.set('TOTAL_MONTO_RECGAR',importeTotalRecGarDL);
							regDL.set('TOTAL_MONTO_DESCUENTO',montoTotalDescuentoDL);
							regDL.set('TOTAL_MONTO_INTERES',importeTotalInteresDL);
							regDL.set('TOTAL_MONTO_RECIBIR',importeTotalRecibirDL);
						}
					}
				},'-'
			]
		}
	});

	var fpCausa2 = new Ext.form.FormPanel({
		id: 'formaCausa2',	width: 700,	autoHeight: true,	style: ' margin:0 auto;',	hidden:true,	frame: false,
		items: [
			{
				xtype: 'panel',	frame: false,	id:'tabla',	layout:'table',	layoutConfig:{ columns:2 },defaults: {frame:false, border: true,autoHeight: true,bodyStyle:'padding:5px'},
				items: [
					{	width:200,	border: false,	html:'<div align="center">Motivo de cambio de estatus:</div>'	},
					{	width:500,	id: 'disCausa',	html: '&nbsp;'	}
				]
			},{
				xtype:'panel',	frame:false, id:'panelError', layout:'fit', border:true, autoHeight: true,	bodyStyle:'padding:5px',hidden:true,
				html:	'&nbsp'
			}
		],
		buttons: [
			{
				text:'Aceptar',
				id:	'btnAceptar',
				iconCls:'icoAceptar',
				handler:	function(){
								var estatus=[];
								var seleccionados = [];
								var fechaVenc='';
								var recurso = 0,	sumMN=0, sumDL=0;
								var textoFirmar =	"Nombre Pyme|Nombre IF|Numero de Documento|Fecha de Vencimiento|Estatus Anterior|Moneda|Monto|" +
													"Monto a Descontar|Monto Interes|Monto a operar|Nuevo Estatus|Porcentaje Anticipo|Recurso Garantia|Tipo de Factoraje|\n";
								var jsonData = consultaData.data.items;
								Ext.each(jsonData, function(item,index,arrItem){
										recurso=0;
										estatus.push('3');
										seleccionados.push(item.data.IC_DOCUMENTO);
										recurso = item.data.FN_MONTO - item.data.FN_MONTO_DSCTO;
										if ( Ext.isEmpty(item.data.DF_FECHA_VENC) ){
											fechaVenc = '';
										}else{
											fechaVenc = (item.data.DF_FECHA_VENC).format('d/m/Y');
										}
										textoFirmar +=	item.data.CG_RAZON_SOCIAL+"|"+item.data.IF_CG_RAZON_SOCIAL+"|"+item.data.IG_NUMERO_DOCTO+"|"+fechaVenc+
										"|"+item.data.CD_DESCRIPCION+"|"+item.data.CD_NOMBRE+"|"+Ext.util.Format.number(item.data.FN_MONTO, '0,0.00')+
										"|"+Ext.util.Format.number(item.data.FN_PORC_ANTICIPO, '0 %')+
										"|"+Ext.util.Format.number(recurso, '0,0.00')+
										"|"+Ext.util.Format.number(item.data.FN_MONTO_DSCTO, '0,0.00')+
										"|"+Ext.util.Format.number(item.data.IN_IMPORTE_INTERES, '0,0.00')+
										"|"+Ext.util.Format.number(item.data.IN_IMPORTE_RECIBIR, '0,0.00')+
                                                                                "|"+item.data.TIPO_FACTORAJE+
										"|"+"\n";

										if(item.data.IC_MONEDA == "1"){
											sumMN++;
										}else if(item.data.IC_MONEDA == "54"){
											sumDL++;
										}
								});

								if (sumMN > 0){
									textoFirmar += "\nNumero de documentos MN: "+sumMN+"|";
								}
								if (sumDL > 0){
									textoFirmar += "\nNumero de documentos DL: "+sumDL+"|";
								}

								NE.util.obtenerPKCS7(confirmarCambioEstatus, textoFirmar, seleccionados, estatus);
								
				}
			},{
				text:'Cancelar',
				id:	'btnCancelar',
				iconCls:'borrar',
				handler: function() {
					window.location = '13forma06ext.jsp';
				}
			},{
				text:'Regresar',
				id:	'btnRegresar',
				hidden: true,
				iconCls:'icoLimpiar',
				handler: function() {
					window.location = '13forma06ext.jsp';
				}
			}
		]
	});
	
	var confirmarCambioEstatus = function(pkcs7, textoFirmar, seleccionados, estatus) {
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}
		pnl.el.mask('Procesando...', 'x-mask-loading');
		Ext.Ajax.request({
			url : '13forma06ext.data.jsp',
			params : {
				informacion:'Captura',
				seleccionados: seleccionados,
				estatus:	estatus,
				ct_cambio_motivo: Ext.getCmp('ct_cambio_motivo').getValue(),
				Pkcs7: pkcs7,
				TextoFirmado: textoFirmar
			},
			callback: procesarSuccessFailureCaptura
		});
	}

	var fpCausa = new Ext.form.FormPanel({
		id: 'formaCausa',
		width: 700,
		style: ' margin:0 auto;',
		hidden:true,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 50,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype:'textarea',
				id:	'ct_cambio_motivo',
				name:	'ct_cambio_motivo',
				height:	40,
				width:	490,
				maxLength:260,
				fieldLabel:'Causa',
				allowBlank: false,
				enableKeyEvents: true,
				listeners:{
					'keydown':	function(txtA){
									if (	!Ext.isEmpty(txtA.getValue())	){
										var numero = (txtA.getValue().length);
										Ext.getCmp('contador').setValue(260-numero);
										if (	Ext.getCmp('contador').getValue() < 0	) {
											var cadena = (txtA.getValue()).substring(0,260);
											txtA.setValue(cadena);
											Ext.getCmp('contador').setValue(0);
										}
									}
								},
					'keyup':	function(txtA){
									if (	Ext.isEmpty(txtA.getValue())	){
										Ext.getCmp('contador').reset();
										fpCausa.getForm().reset();
									}
								},
					'blur':	function(txtA){
									if (	Ext.isEmpty(txtA.getValue())	){
										fpCausa.getForm().reset();
									}									
								}
				}
			},{
				xtype: 'compositefield',
				combineErrors: false,
				msgTarget: 'side',
				items: [
					{
						xtype:'displayfield',	id:'disEspacio',	width: 1,	text:''
					},{
						xtype:'displayfield',	id:'disChar',	width: 120,	value:'Caracteres Restantes:'
					},{
						xtype: 'numberfield',	maxLength:3,	value:	260,	width: 30,	id:	'contador',	name:	'contador',	readOnly: true
					}
				]
			}
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Cambiar Estatus a Negociable',
				id: 'btnCambia',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: function(boton, evento) {
							var flag = false;
							var jsonData = consultaData.data.items;
							Ext.each(jsonData, function(item,index,arrItem){
								if (item.data.FLAG_CHECK){
									flag = true;
									return false;
								}
							});
							if (!flag){
								Ext.Msg.alert(boton.text,'Para poder cambiar el estatus debe seleccionar por lo menos un documento');
								return;
							}
							boton.setIconClass('loading-indicator');
							Ext.getCmp('disCausa').body.update('');
							var newData = [];
							Ext.each(jsonData, function(item,index,arrItem){
								if (item.data.FLAG_CHECK){
										newData.push(item.data);
								}
							});
							if (newData.length > 0) {
								consultaData.loadData(newData);
							}
							var cm = grid.getColumnModel();
							cm.setColumnHeader(cm.findColumnIndex('CD_DESCRIPCION'), 'Estatus Anterior');
							cm.setColumnTooltip(cm.findColumnIndex('CD_DESCRIPCION'), 'Estatus Anterior');
							cm.setHidden(cm.findColumnIndex('NUEVO_ESTATUS'), false);
							cm.setHidden(cm.findColumnIndex('USUARIO'), false);
							cm.setHidden(cm.findColumnIndex('FLAG_CHECK'), true);
							
							Ext.getCmp('disCausa').body.update(Ext.getCmp('ct_cambio_motivo').getValue());
							fpCausa.hide();
							if (Ext.getCmp('gridTotalesA').isVisible()){
								Ext.getCmp('gridTotalesA').hide();
							}
							fpCausa2.show();
				} //fin handler
			}
		]
	});

	var elementosForma = [
		{
			xtype: 'combo',
			id:	'ic_pyme',
			name: 'ic_pyme',
			hiddenName : 'ic_pyme',
			fieldLabel: 'Nombre de la Pyme',
			emptyText: 'Seleccionar Pyme. . .',
			displayField: 'descripcion',
			valueField: 'clave',
			triggerAction : 'all',
			forceSelection:true,
			typeAhead: true,
			mode: 'local',
			minChars : 1,
			store: catalogoNombrePymeData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'combo',
			id:	'ic_if',
			name: 'ic_if',
			hiddenName : 'ic_if',
			fieldLabel: 'Nombre del IF',
			emptyText: 'Seleccionar IF. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIfData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'textfield',
			name: 'ig_numero_docto',
			id: 'ig_numero_docto',
			allowBlank: true,
			vtype:'alphanum',
			fieldLabel:'N�mero de documento',
			anchor:'40%',
			maxLength: 15
		},{
			xtype:'datefield',
			id:	'df_fecha_venc',
			name:	'df_fecha_venc',
			startDay: 0,
			anchor:'37%',
			fieldLabel: 'Fecha de vencimiento'
		},{
			xtype: 'combo',
			id:	'ic_moneda',
			name: 'ic_moneda',
			hiddenName : 'ic_moneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccionar Moneda. . .',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'compositefield',
			fieldLabel: 'Monto entre',
			msgTarget: 'side',
			combineErrors: false,
			items: [
				{
					xtype: 'numberfield',
					name: 'fn_montoMin',
					id: 'fn_montoMin',
					allowBlank: true,
					maxLength: 15,
					width: 110,
					msgTarget: 'side',
					vtype:	'rangoValor',
					campoFinValor:	'fn_montoMax',
					margins: '0 20 0 0'
				},{
					xtype: 'displayfield',
					value: 'y',
					width: 25
				},{
					xtype: 'numberfield',
					name: 'fn_montoMax',
					id: 'fn_montoMax',
					allowBlank: true,
					maxLength: 15,
					width: 110,
					msgTarget: 'side',
					vtype:	'rangoValor',
					campoInicioValor:	'fn_montoMin',
					margins: '0 20 0 0'
				}
			]
		},
		{
		    xtype: 'compositefield',
		    fieldLabel: 'Tipo de Factoraje',	
		    combineErrors: false,
		    msgTarget: 'side',
		    items: [
			{
			    xtype: 'combo',
			    id:	'cmbFactoraje',
			    name: 'cmbFactoraje',
			    hiddenName : 'cmbFactoraje',			
			    fieldLabel: 'Tipo de Factoraje',
			    emptyText: 'Seleccionar ',
			    mode: 'local',
			    displayField: 'descripcion',
			    valueField: 'clave',
			    forceSelection : false,
			    triggerAction : 'all',
			    typeAhead: true,
			    minChars : 1,
			    width: 170,
			    store: catTipoFactorajeData,
			    tpl : NE.util.templateMensajeCargaCombo
			}				
		    ]
		}
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		style: ' margin:0 auto;',
		collapsible: true,
		titleCollapse: true,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					fpCausa.hide();
					fpCausa2.hide();
					Ext.getCmp('btnCambia').setIconClass('icoAceptar');
					fpCausa.getForm().reset();
					grid.hide();
					if (Ext.getCmp('gridTotalesA').isVisible()){
						Ext.getCmp('gridTotalesA').hide();
					}
					if(!verificaPanel()) {
						return;
					}
					var montoMin = Ext.getCmp("fn_montoMin");
					var montoMax = Ext.getCmp("fn_montoMax");
					if (!Ext.isEmpty(montoMin.getValue()) ) {
						if (Ext.isEmpty(montoMax.getValue())){
							montoMax.markInvalid('Debe capturar el monto final');
							montoMax.focus();
							return;
						}
					}
					//fp.el.mask('Procesando...', 'x-mask-loading');
					pnl.el.mask('Procesando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '13forma06ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{informacion: 'Consulta'}),
						callback: procesaConsulta
					});
				} //fin handler
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13forma06ext.jsp';
				}
			}
		]
	});

	function verificaPanel(){
		var myPanel = Ext.getCmp('forma');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype != 'label')	{	//El metodo isValid() no aplica para los tipos de objetos label.
				if (!panelItem.isValid())	{
					valid = false;
					return false;
				}
			}
		});
		return valid;
	}

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(5),
			grid,	gridTotalesA,	NE.util.getEspaciador(5),
			fpCausa,fpCausa2
		]
	});

	catalogoNombrePymeData.load();
	catalogoIfData.load();
	catalogoMonedaData.load();
	catTipoFactorajeData.load();

	//fp.el.mask('Procesando...', 'x-mask-loading');
	pnl.el.mask('Procesando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '13forma06ext.data.jsp',
		params: { informacion: "Parametrizacion" },
		callback: procesarSuccessFailureParametrizacion
	});

});
