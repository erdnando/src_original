<% String version = (String)session.getAttribute("version"); %>
<%if(version!=null){ %>
<!DOCTYPE html>
<%} %>
<%@ page import="java.util.*,
                netropology.utilerias.*"
			contentType="text/html;charset=windows-1252"
         errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs.jspf" %>
<%if(version!=null){ %>
<%@ include file="/01principal/menu.jspf"%>
<%} %>
<script type="text/javascript" src="13forma11ext.js?<%=session.getId()%>"></script>
</head>

<%if(version!=null){ %>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
</body>

<%} %>
<%if(version==null){ %>
	<body>
	<div id='areaContenido' style="margin-left: 3px; margin-top: 3px; text-align: center"></div>
	<form id='formAux' name="formAux" target='_new'></form>
	</body>
<%}%>

</html>
