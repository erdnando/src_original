<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.anticipos.*,
	com.netro.distribuidores.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%

	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
  String strPendiente = (request.getParameter("strPendiente") != null) ? request.getParameter("strPendiente") : "N";
	String strPreNegociable = (request.getParameter("strPreNegociable") != null) ? request.getParameter("strPreNegociable") : "N";

	String strFirmaManc = (request.getParameter("strFirmaManc") != null) ? request.getParameter("strFirmaManc") : "N";
	String strHash = (request.getParameter("strHash") != null) ? request.getParameter("strHash") : "N";

	String txttotdoc = (request.getParameter("txttotdoc") != null) ? request.getParameter("txttotdoc") : "0";
	String txtmtodoc = (request.getParameter("txttotdoc") != null) ? request.getParameter("txttotdoc") : "0";
	String txtmtomindoc = (request.getParameter("txtmtomindoc") != null) ? request.getParameter("txtmtomindoc") : "0";
	String txtmtomaxdoc = (request.getParameter("txtmtomaxdoc") != null) ? request.getParameter("txtmtomaxdoc") : "0";
	
	String txttotdocdo = (request.getParameter("txttotdocdo") != null) ? request.getParameter("txttotdocdo") : "0";
	String txtmtodocdo = (request.getParameter("txtmtodocdo") != null) ? request.getParameter("txtmtodocdo") : "0";
	String txtmtomindocdo = (request.getParameter("txtmtomindocdo") != null) ? request.getParameter("txtmtomindocdo") : "0";
	String txtmtomaxdocdo = (request.getParameter("txtmtomaxdocdo") != null) ? request.getParameter("txtmtomaxdocdo") : "0";
	String proceso = (request.getParameter("proceso")!=null)?request.getParameter("proceso"):"0";
	String cancelacion = (request.getParameter("cancelacion")!=null)?request.getParameter("cancelacion"):"";

	String acuse = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
	String usuario = (request.getParameter("usuario")!=null)?request.getParameter("usuario"):"";
	String hidFechaCarga = (request.getParameter("hidFechaCarga")!=null)?request.getParameter("hidFechaCarga"):"";
	String hidHoraCarga = (request.getParameter("hidHoraCarga")!=null)?request.getParameter("hidHoraCarga"):"";
	String acuseFormateado = (request.getParameter("acuseFormateado")!=null)?request.getParameter("acuseFormateado"):"";
	String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
	String hayErrores = (request.getParameter("hayErrores")!=null)?request.getParameter("hayErrores"):"";
	String bVenSinOperarS = (request.getParameter("bVenSinOperarS")!=null)?request.getParameter("bVenSinOperarS"):"";		
	String doctoDuplicados = (request.getParameter("doctoDuplicados")!=null)?request.getParameter("doctoDuplicados"):"";		
	
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<script type="text/javascript" src="13forma02cExt.js?<%=session.getId()%>"></script>

<%@ include file="/00utils/componente_firma.jspf" %>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>
	
<form id='formParametros' name="formParametros">
	<input type="hidden" id="strPendiente" name="strPendiente" value="<%=strPendiente%>"/>	
	<input type="hidden" id="strPreNegociable" name="strPreNegociable" value="<%=strPreNegociable%>"/>
	<input type="hidden" id="strFirmaManc" name="strFirmaManc" value="<%=strFirmaManc%>"/>
	<input type="hidden" id="strHash" name="strHash" value="<%=strHash%>"/>
	
	<input type="hidden" id="txttotdoc" name="txttotdoc" value="<%=txttotdoc%>"/>
	<input type="hidden" id="txtmtodoc" name="txtmtodoc" value="<%=txtmtodoc%>"/>
	<input type="hidden" id="txtmtomindoc" name="txtmtomindoc" value="<%=txtmtomindoc%>"/>
	<input type="hidden" id="txtmtomaxdoc" name="txtmtomaxdoc" value="<%=txtmtomaxdoc%>"/>
	
	<input type="hidden" id="txttotdocdo" name="txttotdocdo" value="<%=txttotdocdo%>"/>
	<input type="hidden" id="txtmtodocdo" name="txtmtodocdo" value="<%=txtmtodocdo%>"/>
	<input type="hidden" id="txtmtomindocdo" name="txtmtomindocdo" value="<%=txtmtomindocdo%>"/>
	<input type="hidden" id="txtmtomaxdocdo" name="txtmtomaxdocdo" value="<%=txtmtomaxdocdo%>"/>

	<input type="hidden" id="cancelacion" name="cancelacion" value="<%=cancelacion%>"/>
	<input type="hidden" id="proceso" name="proceso" value="<%=proceso%>"/>
	<input type="hidden" id="operaNC" name="operaNC" value="<%=operaNC%>"/>
	
	<input type="hidden" id="acuse" name="acuse" value="<%=acuse%>"/>
	<input type="hidden" id="usuario" name="usuario" value="<%=usuario%>"/>
	<input type="hidden" id="hidFechaCarga" name="hidFechaCarga" value="<%=hidFechaCarga%>"/>
	<input type="hidden" id="hidHoraCarga" name="hidHoraCarga" value="<%=hidHoraCarga%>"/>
	<input type="hidden" id="acuseFormateado" name="acuseFormateado" value="<%=acuseFormateado%>"/>
	<input type="hidden" id="mensaje" name="mensaje" value="<%=mensaje%>"/>
	<input type="hidden" id="hayErrores" name="hayErrores" value="<%=hayErrores%>"/>	
	<input type="hidden" id="bVenSinOperarS" name="bVenSinOperarS" value="<%=bVenSinOperarS%>"/>	
	<input type="hidden" id="doctoDuplicados" name="doctoDuplicados" value="<%=doctoDuplicados%>"/>	
	
</form>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
