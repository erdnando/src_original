<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.math.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.dispersion.*, 
		com.netro.descuento.*,
		javax.naming.*,
		net.sf.json.*,
		com.netro.pdf.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>
<%!public static final boolean SIN_COMAS = false;%>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String msgError = "";
try{


	if (informacion.equals("DatosIniciales")) {
		JSONObject jsonObj = new JSONObject();
		SimpleDateFormat 	sdf 			= new SimpleDateFormat("dd/MM/yyyy");
		String 				sFechaHoy 	= sdf.format(new java.util.Date());
		
		CargaDocumento cargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
		
		boolean tieneParamEpoPef = cargaDocumentos.tieneParamEpoPef(iNoCliente);
		
		jsonObj.put("tieneParamEpoPef", new Boolean(tieneParamEpoPef));
		jsonObj.put("operaFVPyme", operaFVPyme);
		jsonObj.put("strNombreUsuario",strLogin + " " + strNombreUsuario);
		jsonObj.put("success", new Boolean(true));
		
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("ConsultaPreAcuseManto")) {
		JSONArray jsObjArray =  new JSONArray();
		CargaDocumento cargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
		String numeroProceso = (request.getParameter("numeroProceso")!=null)?request.getParameter("numeroProceso"):"";

		List lstDoctosManto = cargaDocumentos.getDoctosCambioImporteTmpExt(numeroProceso, true);
		jsObjArray = JSONArray.fromObject(lstDoctosManto);
		
		infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registrosDoctos\": " + jsObjArray.toString() + " }";
	}else if(informacion.equals("TransmitirDoctos")){
		JSONObject jsonObj = new JSONObject();
		CargaDocumento cargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
		
		String hidNumDoctosMN = (request.getParameter("hidNumDoctosMN") == null)? "" : request.getParameter("hidNumDoctosMN");
		String hidMontoTotalMNDespliegue = (request.getParameter("hidMontoTotalMNDespliegue") == null)? "" : request.getParameter("hidMontoTotalMNDespliegue");
		String hidMontoMinMNDespliegue = (request.getParameter("hidMontoMinMNDespliegue") == null)? "" : request.getParameter("hidMontoMinMNDespliegue");
		String hidMontoMaxMNDespliegue = (request.getParameter("hidMontoMaxMNDespliegue") == null)? "" : request.getParameter("hidMontoMaxMNDespliegue");
		String hidNumDoctosDL = (request.getParameter("hidNumDoctosDL") == null)? "" : request.getParameter("hidNumDoctosDL");
		String hidMontoMinDLDespliegue = (request.getParameter("hidMontoMinDLDespliegue") == null)? "" : request.getParameter("hidMontoMinDLDespliegue");
		String hidMontoMaxDLDespliegue = (request.getParameter("hidMontoMaxDLDespliegue") == null)? "" : request.getParameter("hidMontoMaxDLDespliegue");
		String hidMontoTotalDLDespliegue = (request.getParameter("hidMontoTotalDLDespliegue") == null)? "" : request.getParameter("hidMontoTotalDLDespliegue");
		String ic_proceso_cambio = (request.getParameter("numeroProceso") == null)? "" : request.getParameter("numeroProceso");
		String acuse="";
		String msgErrorCertificado = "";
		String folio = "";
		char getReceipt = 'Y';
		
		String pkcs7 = (request.getParameter("Pkcs7")==null)?"":request.getParameter("Pkcs7");
		String externContent = (request.getParameter("TextoFirmado")==null)?"":request.getParameter("TextoFirmado");
		externContent = (request.getHeader("User-Agent").indexOf("MSIE") == -1 && request.getHeader("User-Agent").indexOf("Mozilla") != -1)?java.net.URLDecoder.decode(externContent):externContent;
		
		
		Acuse no_acuse = new Acuse(Acuse.ACUSE_EPO_MODIF,"1");
		acuse = no_acuse.formatear();
		
		jsonObj.put("Acuse",acuse);
		
		if(!_serial.equals("") && externContent!=null && pkcs7!=null) {
			folio = no_acuse.toString();
			Seguridad s = new Seguridad();
			
			if(s.autenticar(folio, _serial, pkcs7, externContent, getReceipt)) {
				String _acuse = s.getAcuse();
				
				cargaDocumentos.insertaDoctosCambioEstatus(folio, hidNumDoctosMN, hidMontoTotalMNDespliegue, hidNumDoctosDL, hidMontoMinDLDespliegue, iNoUsuario, _acuse, ic_proceso_cambio, strLogin + " " + strNombreUsuario);
				boolean bBorra = cargaDocumentos.borraDoctosCambioImporte(ic_proceso_cambio);
				
				String Fecha=(new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date());
				String Hora=(new SimpleDateFormat("hh:mm:ss")).format(new java.util.Date());
				
				List lstMantoDoctos = cargaDocumentos.getDoctosCambioImporteExt(folio ,true);
				JSONArray jsonArray = new JSONArray();
				jsonArray = JSONArray.fromObject(lstMantoDoctos);
				
				jsonObj.put("Fecha",Fecha);
				jsonObj.put("Hora",Hora);
				jsonObj.put("Usuario",iNoUsuario+" - "+strNombreUsuario);
				jsonObj.put("registrosMantoDoctos",jsonArray.toString());
				jsonObj.put("total",jsonArray.size()+"");
				jsonObj.put("Folio",_acuse);


			}else{
				String _error = s.mostrarError();
				msgErrorCertificado = "La autentificación no se llevó a cabo. " + _error + ".<br>Proceso CANCELADO";
			}
		}else {
				
				msgErrorCertificado = "No se pudo obtener el certificado.";
		}
		
		jsonObj.put("msgErrorCertificado",msgErrorCertificado);
		jsonObj.put("success", new Boolean(true));
		
		infoRegresar = jsonObj.toString();
		
	}else if(informacion.equals("GenerarCsvAcuse")){
		JSONObject jsonObj = new JSONObject();
		CargaDocumento cargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
	
		String regResumenMantoData = (request.getParameter("regResumenMantoData") == null)?"":request.getParameter("regResumenMantoData");
		String regAcuseMantoData = (request.getParameter("regAcuseMantoData") == null)?"":request.getParameter("regAcuseMantoData");
		String regDoctosMantoAcuseData = (request.getParameter("regDoctosMantoAcuseData") == null)?"":request.getParameter("regDoctosMantoAcuseData");
		String regTotalesMantoData = (request.getParameter("regTotalesMantoData") == null)?"":request.getParameter("regTotalesMantoData");
    
		String numeroProceso = (request.getParameter("numeroProceso")!=null)?request.getParameter("numeroProceso"):"";
		List lstDoctosManto = cargaDocumentos.getDoctosCambioImporteTmpExt(numeroProceso, false);
		
		List lstResumenMantoData = JSONArray.fromObject(regResumenMantoData);
		List lstAcuseMantoData = JSONArray.fromObject(regAcuseMantoData);
		List lstDoctosMantoAcuseData = JSONArray.fromObject(regDoctosMantoAcuseData);
		List lstTotalesMantoData = JSONArray.fromObject(regTotalesMantoData);
		
		StringBuffer sbContenido = new StringBuffer();
		
		sbContenido.append(",Moneda Nacional,Dolares\n");
		
		Iterator iteraData = lstResumenMantoData.iterator();
		while (iteraData.hasNext()) {
			JSONObject registro = (JSONObject)iteraData.next();
			sbContenido.append(registro.getString("TITULOS")+",");
			sbContenido.append(Comunes.reemplazarCaracteres(registro.getString("NACIONAL"),",","")+",");
			sbContenido.append(Comunes.reemplazarCaracteres(registro.getString("DOLAR"),",","")+"\n");
		}
		
		iteraData = lstAcuseMantoData.iterator();
		while (iteraData.hasNext()) {
			JSONObject registro = (JSONObject)iteraData.next();
			sbContenido.append(registro.getString("TITULOS")+",");
			sbContenido.append(registro.getString("VALORES")+"\n");
		}
		
		sbContenido.append("Al transmitir este MENSAJE DE DATOS, usted está bajo su responsabilidad haciendo negociables "+
								"los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesión de los derechos de cobro "+
								"de las PYMES al INTERMEDIARIO FINANCIERO, dicha transmisión tendrá validez para todos los efectos legales)\n");
		
		sbContenido.append("Clave del Proveedor,Número de Documento,Monto Anterior,Monto Nuevo,Fecha de Vencimiento Anterior,");
		sbContenido.append("Fecha de Vencimiento Nueva");
		if("S".equals(operaFVPyme)){
			sbContenido.append(",Fecha de Vencimiento Prov Anterior,Fecha de Vencimiento Prov Nueva");
		}
		sbContenido.append(",Movimiento,Causa,Usuario");
		if(cargaDocumentos.tieneParamEpoPef(iNoCliente)){
			sbContenido.append(",Fecha Entrega,Tipo Compra,Clave Presupuestaria,Periodo\n");
		}
		sbContenido.append("\n");
		
		//iteraData = lstDoctosMantoAcuseData.iterator();
		//while (iteraData.hasNext()) {
    for(int e=0; e < lstDoctosManto.size(); e++){
      HashMap registro = (HashMap)lstDoctosManto.get(e);
			//JSONObject registro = (JSONObject)iteraData.next();
			sbContenido.append((String)registro.get("CVE_PROVEEDOR")+",");
      sbContenido.append((String)registro.get("NUM_DOCTO")+",");
      sbContenido.append((String)registro.get("MONTO_ANTERIOR")+",");
      sbContenido.append((String)registro.get("MONTO_NUEVO")+",");
      sbContenido.append((String)registro.get("FECVENC_ANTERIOR")+",");
      sbContenido.append((String)registro.get("FECVENC_NUEVA")+",");
      if("S".equals(operaFVPyme)){
        sbContenido.append((String)registro.get("FECVENC_PROVANTE")+",");
        sbContenido.append((String)registro.get("FECVENC_PROVNUEVA")+",");
      }
      sbContenido.append((String)registro.get("CAMBIO_MOTIVO")+",");
      sbContenido.append((String)registro.get("CAUSA")+",");
      sbContenido.append((String)registro.get("NOM_USUARIO")+",");
      if(cargaDocumentos.tieneParamEpoPef(iNoCliente)){
        sbContenido.append((String)registro.get("FEC_ENTREGA")+",");
        sbContenido.append((String)registro.get("TIPO_COMPRA")+",");
        sbContenido.append((String)registro.get("CVE_PRESUPUESTAL")+",");
        sbContenido.append((String)registro.get("PERIODO"));
      }
			sbContenido.append("\n");
			
		}
		
		iteraData = lstTotalesMantoData.iterator();
		while (iteraData.hasNext()) {
			JSONObject registro = (JSONObject)iteraData.next();
			sbContenido.append(registro.getString("TITULOS")+" ,");
			sbContenido.append(Comunes.reemplazarCaracteres(registro.getString("VALORES"),",","")+"\n");
			
		}
		
		String nombreArchivo 	= "";
		CreaArchivo archivo = new CreaArchivo();
		if (archivo.make(sbContenido.toString(), strDirectorioTemp, ".csv"))
			nombreArchivo = archivo.nombre;	
			
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	
	}else if(informacion.equals("GenerarPdfAcuse")){
		JSONObject jsonObj = new JSONObject();
	
		String regResumenMantoData = (request.getParameter("regResumenMantoData") == null)?"":request.getParameter("regResumenMantoData");
		String regAcuseMantoData = (request.getParameter("regAcuseMantoData") == null)?"":request.getParameter("regAcuseMantoData");
		//String regDoctosMantoAcuseData = (request.getParameter("regDoctosMantoAcuseData") == null)?"":request.getParameter("regDoctosMantoAcuseData");
		String regTotalesMantoData = (request.getParameter("regTotalesMantoData") == null)?"":request.getParameter("regTotalesMantoData");
    

		CargaDocumento cargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
		String numeroProceso = (request.getParameter("numeroProceso")!=null)?request.getParameter("numeroProceso"):"";

		List lstDoctosManto = cargaDocumentos.getDoctosCambioImporteTmpExt(numeroProceso, false);
		
		List lstResumenMantoData = JSONArray.fromObject(regResumenMantoData);
		List lstAcuseMantoData = JSONArray.fromObject(regAcuseMantoData);
		//List lstDoctosMantoAcuseData = JSONArray.fromObject(regDoctosMantoAcuseData);
		List lstTotalesMantoData = JSONArray.fromObject(regTotalesMantoData);
		
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = archivo.nombreArchivo()+".pdf";	
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
												session.getAttribute("iNoNafinElectronico").toString(),
												(String)session.getAttribute("sesExterno"),
												(String) session.getAttribute("strNombre"),
												(String) session.getAttribute("strNombreUsuario"),
												(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	
		
		pdfDoc.setTable(3,70);
		pdfDoc.setCell("","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Moneda Nacional","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Dolares","celda01",ComunesPDF.CENTER,1);
		
		Iterator iteraData = lstResumenMantoData.iterator();
		while (iteraData.hasNext()) {
			JSONObject registro = (JSONObject)iteraData.next();
			pdfDoc.setCell(registro.getString("TITULOS"),"formas",ComunesPDF.LEFT,1);
			pdfDoc.setCell(registro.getString("NACIONAL"),"formas",ComunesPDF.LEFT,1);
			pdfDoc.setCell(registro.getString("DOLAR"),"formas",ComunesPDF.LEFT,1);
		}
		
		iteraData = lstAcuseMantoData.iterator();
		while (iteraData.hasNext()) {
			JSONObject registro = (JSONObject)iteraData.next();
			pdfDoc.setCell(registro.getString("TITULOS"),"formas",ComunesPDF.LEFT,1);
			pdfDoc.setCell(registro.getString("VALORES"),"formas",ComunesPDF.LEFT,2);
		}
		
		
		String textoLegal = "Al transmitir este MENSAJE DE DATOS, usted está bajo su responsabilidad haciendo negociables "+
								"los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesión de los derechos de cobro "+
								"de las PYMES al INTERMEDIARIO FINANCIERO, dicha transmisión tendrá validez para todos los efectos legales)\n";
		
		pdfDoc.setCell(textoLegal,"formas",ComunesPDF.LEFT,3);
		pdfDoc.addTable();
		
		int tamColum = 13;
		
		
		if(!cargaDocumentos.tieneParamEpoPef(iNoCliente)) tamColum = tamColum-4;
		if("S".equals(operaFVPyme)) tamColum = tamColum+2;
		
		
		pdfDoc.setTable(tamColum,100);
		
		pdfDoc.setCell("Clave del Proveedor","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Número de Documento","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Monto Anterior","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Monto Nuevo","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Fecha de Vencimiento Anterior","celda01",ComunesPDF.CENTER,1);
		
		pdfDoc.setCell("Fecha de Vencimiento Nueva","celda01",ComunesPDF.CENTER,1);
		if("S".equals(operaFVPyme)){
			pdfDoc.setCell("Fecha de Vencimiento Prov Anterior","celda01",ComunesPDF.CENTER,1);
			pdfDoc.setCell("Fecha de Vencimiento Prov Nueva","celda01",ComunesPDF.CENTER,1);
		}
		pdfDoc.setCell("Movimiento","celda01",ComunesPDF.CENTER,1);
		pdfDoc.setCell("Causa","celda01",ComunesPDF.CENTER,1);
		
		pdfDoc.setCell("Usuario","celda01",ComunesPDF.CENTER,1);
		if(cargaDocumentos.tieneParamEpoPef(iNoCliente)){
			pdfDoc.setCell("Fecha Entrega","celda01",ComunesPDF.CENTER,1);
			pdfDoc.setCell("Tipo Compra","celda01",ComunesPDF.CENTER,1);
			pdfDoc.setCell("Clave Presupuestaria","celda01",ComunesPDF.CENTER,1);
			pdfDoc.setCell("Periodo","celda01",ComunesPDF.CENTER,1);
		}
		
		//iteraData = lstDoctosMantoAcuseData.iterator();
    
    for(int e=0; e < lstDoctosManto.size(); e++){
		//while (iteraData.hasNext()) {
			//JSONObject registro = (JSONObject)iteraData.next();
      HashMap registro = (HashMap)lstDoctosManto.get(e);
			pdfDoc.setCell((String)registro.get("CVE_PROVEEDOR"),"formas",ComunesPDF.LEFT,1);
			pdfDoc.setCell((String)registro.get("NUM_DOCTO"),"formas",ComunesPDF.LEFT,1);
      pdfDoc.setCell((String)registro.get("MONTO_ANTERIOR"),"formas",ComunesPDF.LEFT,1);
      pdfDoc.setCell((String)registro.get("MONTO_NUEVO"),"formas",ComunesPDF.LEFT,1);
      pdfDoc.setCell((String)registro.get("FECVENC_ANTERIOR"),"formas",ComunesPDF.LEFT,1);
      pdfDoc.setCell((String)registro.get("FECVENC_NUEVA"),"formas",ComunesPDF.LEFT,1);
      
      if("S".equals(operaFVPyme)){
        pdfDoc.setCell((String)registro.get("FECVENC_PROVANTE"),"formas",ComunesPDF.LEFT,1);
        pdfDoc.setCell((String)registro.get("FECVENC_PROVNUEVA"),"formas",ComunesPDF.LEFT,1);
      }
      
      pdfDoc.setCell((String)registro.get("CAMBIO_MOTIVO"),"formas",ComunesPDF.LEFT,1);
      pdfDoc.setCell((String)registro.get("CAUSA"),"formas",ComunesPDF.LEFT,1);
      pdfDoc.setCell((String)registro.get("NOM_USUARIO"),"formas",ComunesPDF.LEFT,1);
      
      if(cargaDocumentos.tieneParamEpoPef(iNoCliente)){
        pdfDoc.setCell((String)registro.get("FEC_ENTREGA"),"formas",ComunesPDF.LEFT,1);
        pdfDoc.setCell((String)registro.get("TIPO_COMPRA"),"formas",ComunesPDF.LEFT,1);
        pdfDoc.setCell((String)registro.get("CVE_PRESUPUESTAL"),"formas",ComunesPDF.LEFT,1);
        pdfDoc.setCell((String)registro.get("PERIODO"),"formas",ComunesPDF.LEFT,1);
      }
			
		}
		
		iteraData = lstTotalesMantoData.iterator();
		while (iteraData.hasNext()) {
			JSONObject registro = (JSONObject)iteraData.next();
			pdfDoc.setCell(registro.getString("TITULOS"),"formas",ComunesPDF.LEFT,4);
			pdfDoc.setCell(registro.getString("VALORES"),"formas",ComunesPDF.LEFT,(tamColum-4));
			
		}
		
		pdfDoc.addTable();
		pdfDoc.endDocument();
		
			
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	}
	
}catch(Throwable t){
	t.printStackTrace();
	throw new AppException("Error en la peticion",t);
}finally{
	System.out.println("infoRegresar = " + infoRegresar);
}
%>


<%=infoRegresar%>