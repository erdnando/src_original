<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.netro.exception.*,
	com.netro.descuento.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>

<%
	String proceso = (request.getParameter("proceso")!=null)?request.getParameter("proceso"):"";
	String cancelacion = (request.getParameter("cancelacion")!=null)?request.getParameter("cancelacion"):"";
	String acuse = (request.getParameter("acuse")!=null)?request.getParameter("acuse"):"";
	String acuseFormateado = (request.getParameter("acuseFormateado")!=null)?request.getParameter("acuseFormateado"):"";
	String recibo = (request.getParameter("recibo")!=null)?request.getParameter("recibo"):"";
	String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";	
	String usuario = (request.getAttribute("usuario")==null)?request.getParameter("usuario"):request.getAttribute("usuario").toString();
	String hidFechaCarga = (request.getAttribute("hidFechaCarga")==null)?request.getParameter("hidFechaCarga"):request.getAttribute("hidFechaCarga").toString();
	String hidHoraCarga = (request.getAttribute("hidHoraCarga")==null)?request.getParameter("hidHoraCarga"):request.getAttribute("hidHoraCarga").toString();

	String strPendiente = (request.getParameter("strPendiente") != null) ? request.getParameter("strPendiente") : "N";
	String strPreNegociable = (request.getParameter("strPreNegociable") != null) ? request.getParameter("strPreNegociable") : "N";
	String bVenSinOperarS = (request.getParameter("bVenSinOperarS") != null) ? request.getParameter("bVenSinOperarS") : "N";

%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>

<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>

<script type="text/javascript" src="13forma01cExt.js?<%=session.getId()%>"></script>

<%@ include file="/00utils/componente_firma.jspf" %>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>

<form id='formParametros' name="formParametros">
	<input type="hidden" id="proceso" name="proceso" value="<%=proceso%>"/>
	<input type="hidden" id="strPendiente" name="strPendiente" value="<%=strPendiente%>"/>	
	<input type="hidden" id="strPreNegociable" name="strPreNegociable" value="<%=strPreNegociable%>"/>	
	<input type="hidden" id="operaNC" name="operaNC" value="<%=operaNC%>"/>	
	<input type="hidden" id="cancelacion" name="cancelacion" value="<%=cancelacion%>"/>		
	<input type="hidden" id="acuse" name="acuse" value="<%=acuse%>"/>	
	<input type="hidden" id="acuseFormateado" name="acuseFormateado" value="<%=acuseFormateado%>"/>		
	<input type="hidden" id="recibo" name="recibo" value="<%=recibo%>"/>	
	<input type="hidden" id="mensaje" name="mensaje" value="<%=mensaje%>"/>	
	<input type="hidden" id="operaNC" name="operaNC" value="<%=operaNC%>"/>	
	<input type="hidden" id="usuario" name="usuario" value="<%=usuario%>"/>	
	<input type="hidden" id="hidFechaCarga" name="hidFechaCarga" value="<%=hidFechaCarga%>"/>	
	<input type="hidden" id="hidHoraCarga" name="hidHoraCarga" value="<%=hidHoraCarga%>"/>	
	<input type="hidden" id="bVenSinOperarS" name="bVenSinOperarS" value="<%=bVenSinOperarS%>"/>	
	

</form>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
