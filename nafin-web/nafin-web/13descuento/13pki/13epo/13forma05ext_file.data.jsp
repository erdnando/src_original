<%@ page contentType="text/html; charset=UTF-8"
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.math.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
		netropology.utilerias.caracterescontrol.*,
		com.netro.parametrosgrales.*,
		com.netro.descuento.*,
		com.netro.zip.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs_fileupload.jsp"
%>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%

	boolean flag = true;
	boolean existeFile = false;
	
	ParametrosRequest req = null;
	String rutaArchivo = "";
	String PATH_FILE	=	strDirectorioTemp;
	String itemArchivo = "";
	String extension = "";
	String folio = "";
	String fechaHora = "";
	String numProceso = "";
	String numTotal = "";
	String totalMonto = "";
	String msgError = "";
	int totalRegistros = 0;
	int tamanio = 0;
	
	String ic_epo = iNoCliente;
	
	
	String hidNumDoctosMN = "";
	String hidMontoTotalMNDespliegue = "";
	String hidMontoMinMNDespliegue = "";
	String hidMontoMaxMNDespliegue = "";
	String hidNumDoctosDL = "";
	String hidMontoMinDLDespliegue = "";
	String hidMontoMaxDLDespliegue = "";
	String hidMontoTotalDLDespliegue = "";
	
	String ic_proceso_cambio = "";
	String nombreArchivo = "";
	BigDecimal montoTotal = new BigDecimal("0.0");
	
	
	boolean ok = 					false;
	boolean bBotonProcesar =	false;
	String lineasErrores =		"";
	String error =					"";
	String errorOtros =			"";
	String sinError =				"";
	int totdoc_mnOK =				0;
	int totdoc_dolarOK =			0;
	double monto_tot_mnOK =		0.0;
	double monto_tot_dolarOK = 0.0;
	
	String [] mensajeEstatus = {""};
	String mensajeResumen = "";
	String nombreArchivoCaractEsp = "";
	int status = 0;
	boolean hayCaractCtrl = false;
	
	boolean codificacionValida = true;
	String codificacionArchivo = "";
	
	if (ServletFileUpload.isMultipartContent(request)) {
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(PATH_FILE));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));
		extension = (req.getParameter("hidExtension") == null)? "" : req.getParameter("hidExtension");
		hidNumDoctosMN = (req.getParameter("hidNumDoctosMN") == null)? "" : req.getParameter("hidNumDoctosMN");
		hidMontoTotalMNDespliegue = (req.getParameter("hidMontoTotalMNDespliegue") == null)? "" : req.getParameter("hidMontoTotalMNDespliegue");
		hidMontoMinMNDespliegue = (req.getParameter("hidMontoMinMNDespliegue") == null)? "" : req.getParameter("hidMontoMinMNDespliegue");
		hidMontoMaxMNDespliegue = (req.getParameter("hidMontoMaxMNDespliegue") == null)? "" : req.getParameter("hidMontoMaxMNDespliegue");
		hidNumDoctosDL = (req.getParameter("hidNumDoctosDL") == null)? "" : req.getParameter("hidNumDoctosDL");
		hidMontoMinDLDespliegue = (req.getParameter("hidMontoMinDLDespliegue") == null)? "" : req.getParameter("hidMontoMinDLDespliegue");
		hidMontoMaxDLDespliegue = (req.getParameter("hidMontoMaxDLDespliegue") == null)? "" : req.getParameter("hidMontoMaxDLDespliegue");
		hidMontoTotalDLDespliegue = (req.getParameter("hidMontoTotalDLDespliegue") == null)? "" : req.getParameter("hidMontoTotalDLDespliegue");
	
		
		try{
			int tamaPer = 0;
			if (extension.equals("txt")){
				upload.setSizeMax(200 * 1024);
				tamaPer = (2000 * 1024);
			}

			FileItem fItem = (FileItem)req.getFiles().get(0);
			itemArchivo		= (String)fItem.getName();
			tamanio			= (int)fItem.getSize();

			rutaArchivo = PATH_FILE+ "/" + Comunes.cadenaAleatoria(16) + ".txt";
			
			// Process the uploaded items
	
			fItem.write(new File(rutaArchivo));
			
			System.out.println("tamanio=="+tamanio);
			System.out.println("tamaPer=="+tamaPer);
			System.out.println("Nombre itemArchivo=="+itemArchivo);
			
			ParametrosGrales paramGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
			if(paramGrales.validaCodificacionArchivoHabilitado()){
				String codificacionDetectada[] = {""};
				CodificacionArchivo codificaArchivo = new CodificacionArchivo();
				codificaArchivo.setProcessTxt(true);
				//codificaArchivo.setProcessZip(true);
				
				if(codificaArchivo.esCharsetNoSoportado(rutaArchivo,codificacionDetectada)){
					codificacionArchivo = codificacionDetectada[0];
					codificacionValida = false;
					flag = false;
				}
			}
			
			if (flag && tamanio > tamaPer){
				flag = false;
				msgError = "El tama�o del archivo TXT es mayor al permitido. <br> "+
								" Tama�o Actual = "+tamanio  +" bytes  <br> "+
								" Tama�o Permitido= "+tamaPer+" bytes " ;
			}
			
			if (flag){
				ResumenBusqueda resumenBusqueda = new ResumenBusqueda();
				BuscaCaracteresControlThread buscaCCtrlThread = new BuscaCaracteresControlThread();
				buscaCCtrlThread.setRutaArchivo(rutaArchivo);
				buscaCCtrlThread.setRegistrador(resumenBusqueda);
				buscaCCtrlThread.setCreaArchivoDetalle(true);
				buscaCCtrlThread.setDirectorioTemporal(strDirectorioTemp);
				buscaCCtrlThread.setSeparadorCampo("|");
				buscaCCtrlThread.start();
				buscaCCtrlThread.join();
				
				status = buscaCCtrlThread.getStatus(mensajeEstatus);
				ResumenBusqueda resumen = (ResumenBusqueda)buscaCCtrlThread.getRegistrador();
				hayCaractCtrl = resumen.hayCaracteresControl();
				mensajeResumen = resumen.getMensaje()==null?"":resumen.getMensaje();
				nombreArchivoCaractEsp = resumen.getNombreArchivoDetalle();
				

				flag = (hayCaractCtrl || status!=300 )?false:flag;
			}
			
			if (flag){

				fechaHora = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date()) + " " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				try{
					
					CargaDocumento cargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);

					System.out.println("rutaArchivo====="+rutaArchivo);
					java.io.File f=new java.io.File(rutaArchivo);
					ic_proceso_cambio = cargaDocumentos.getNumaxProcesoCambioImporte();
					
					String linea="";
					StringBuffer sbDoctos = new StringBuffer();
					BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(f)));
					
					while((linea=br.readLine())!=null) {
						sbDoctos.append(linea.trim()+"\n");
					} // while.
					
					Hashtable hMtoDocto = cargaDocumentos.procesarMantenimientoDocumentos(
								sbDoctos.toString(), hidNumDoctosMN, hidNumDoctosDL,
								hidMontoTotalMNDespliegue, hidMontoTotalDLDespliegue, hidMontoMinMNDespliegue,
								hidMontoMinDLDespliegue, hidMontoMaxMNDespliegue,
								hidMontoMaxDLDespliegue, ic_proceso_cambio, ic_epo, 
								mensaje_param, sesIdiomaUsuario, operaFVPyme);
								
					ok = 					((Boolean)hMtoDocto.get("ok")).booleanValue();
					bBotonProcesar =	((Boolean)hMtoDocto.get("bBotonProcesar")).booleanValue();
					lineasErrores =		hMtoDocto.get("lineasErrores").toString();
					error =					hMtoDocto.get("error").toString();
					errorOtros =			hMtoDocto.get("errorOtros").toString();
					sinError =				hMtoDocto.get("sinError").toString();
					totdoc_mnOK =				((Integer)hMtoDocto.get("totdoc_mnOK")).intValue();
					totdoc_dolarOK =			((Integer)hMtoDocto.get("totdoc_dolarOK")).intValue();
					monto_tot_mnOK =		((Double)hMtoDocto.get("monto_tot_mnOK")).doubleValue();
					monto_tot_dolarOK = ((Double)hMtoDocto.get("monto_tot_dolarOK")).doubleValue();
					
					
					if(lineasErrores.length() > 0) {
						CreaArchivo archivo = new CreaArchivo();
						if (archivo.make(lineasErrores, strDirectorioTemp, "Errores", ".csv")){
							existeFile = true;
							nombreArchivo = archivo.nombre;
						}else{
							existeFile = false;
							//out.println(mensaje_param.getMensaje("13forma5.ErrorGenerarArchivo",sesIdiomaUsuario));
						}
					}
					
					lineasErrores =		lineasErrores.replaceAll("[\n\r]","|");
					error =					error.replaceAll("[\n\r]","|");
					errorOtros =			errorOtros.replaceAll("[\n\r]","|");
					sinError =				sinError.replaceAll("[\n\r]","|");
					
				}catch (Exception e){
						throw new AppException("Ocurrio un error al generar el archivo", e);
				}
			
			}
		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
	}
	
%>	
{
	"success": true,
	"flag":	<%=(flag)?"true":"false"%>,
	"ok":	<%=(ok)?"true":"false"%>,
	"existeFile":	<%=(existeFile)?"true":"false"%>,
	"bBotonProcesar":	<%=(bBotonProcesar)?"true":"false"%>,
	"numeroProceso": '<%=ic_proceso_cambio%>',
	"operaFVPyme": '<%=operaFVPyme%>',
	"lineasErrores":	'<%=lineasErrores%>',
	"error":	'<%=error%>',
	"errorOtros":	'<%=errorOtros%>',
	"sinError":	'<%=sinError%>',
	"totdoc_mnOK":	'<%=totdoc_mnOK%>',
	"totdoc_dolarOK":	'<%=totdoc_dolarOK%>',
	"monto_tot_mnOK":	'<%=monto_tot_mnOK%>',
	"monto_tot_dolarOK":	'<%=monto_tot_dolarOK%>',
	"urlArchivo": '<%=strDirecVirtualTemp + nombreArchivo%>',
	"msgError":	'<%=msgError%>',
	"statusThread": '<%=String.valueOf(status)%>',
	"mensajeEstatus":	'<%=mensajeEstatus[0]%>',
	"mensajeResumen":	'<%=net.sf.json.util.JSONUtils.quote(mensajeResumen)%>',
	"codificacionValida": <%=(codificacionValida)?"true":"false"%>,
	"codificacionArchivo": '<%=codificacionArchivo%>',
	"hayCaractCtrl":	<%=(hayCaractCtrl)?"true":"false"%>,
	"urlArchivoCaractEsp":	'<%=strDirecVirtualTemp+nombreArchivoCaractEsp%>'
}