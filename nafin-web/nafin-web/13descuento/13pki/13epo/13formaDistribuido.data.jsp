<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,		
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.afiliacion.*,
		org.apache.commons.logging.Log,
		com.netro.descuento.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="../certificado.jspf" %>

<%!
    private final Log log = ServiceLocator.getInstance().getLog("JSPS." + this.getClass().getName());
%>
<%
    String informacion = (request.getParameter("informacion"))!=null?request.getParameter("informacion"):"";
    String nombrePyme = (request.getParameter("nombrePyme"))!=null?request.getParameter("nombrePyme"):"";
    String rfcPyme = (request.getParameter("rfcPyme"))!=null?request.getParameter("rfcPyme"):"";
    String numPyme = (request.getParameter("numPyme"))!=null?request.getParameter("numPyme"):"";
    String txtNafelec = (request.getParameter("txtNafelec"))!=null?request.getParameter("txtNafelec"):"";
    String icPyme = (request.getParameter("icPyme"))!=null?request.getParameter("icPyme"):"";
        
    String dfFechaVencMin = (request.getParameter("dfFechaVencMin"))!=null?request.getParameter("dfFechaVencMin"):"";
    String dfFechaVencMax = (request.getParameter("dfFechaVencMax"))!=null?request.getParameter("dfFechaVencMax"):"";
    String numContrato = (request.getParameter("numContrato"))!=null?request.getParameter("numContrato"):"";
    String numDocumento = (request.getParameter("numDocumento"))!=null?request.getParameter("numDocumento"):"";
    String numCopade = (request.getParameter("numCopade"))!=null?request.getParameter("numCopade"):"";
    String campo1 = (request.getParameter("campo1"))!=null?request.getParameter("campo1"):"";
    String campo2 = (request.getParameter("campo2"))!=null?request.getParameter("campo2"):"";
    String campo3 = (request.getParameter("campo3"))!=null?request.getParameter("campo3"):"";
    String campo4 = (request.getParameter("campo4"))!=null?request.getParameter("campo4"):"";
    String campo5 = (request.getParameter("campo5"))!=null?request.getParameter("campo5"):"";
    
    String tipoArch = (request.getParameter("tipoArch"))!=null?request.getParameter("tipoArch"):"";
    
    String totalDoctosMN = (request.getParameter("totalDoctosMN")==null)?"0":request.getParameter("totalDoctosMN");
    String totalMontoDoctosMN = (request.getParameter("totalMontoDoctosMN")==null)?"":request.getParameter("totalMontoDoctosMN");
    String totalMontoDescontarMN = (request.getParameter("totalMontoDescontarMN")==null)?"":request.getParameter("totalMontoDescontarMN");
    
    String totalDoctosDL = (request.getParameter("totalDoctosDL")==null)?"0":request.getParameter("totalDoctosDL");
    String totalMontoDoctosDL = (request.getParameter("totalMontoDoctosDL")==null)?"":request.getParameter("totalMontoDoctosDL");
    String totalMontoDescontarDL = (request.getParameter("totalMontoDescontarDL")==null)?"":request.getParameter("totalMontoDescontarDL");
        
 
    String acusejur = (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
    String fechaValida = (request.getParameter("fechaValida")==null)?"":request.getParameter("fechaValida");
    String horaValida = (request.getParameter("horaValida")==null)?"":request.getParameter("horaValida");
    String recibo = (request.getParameter("recibo")==null)?"":request.getParameter("recibo");   
    String acuseFormateado = (request.getParameter("acuseFormateado")==null)?"":request.getParameter("acuseFormateado");
    						
		
    String nomUsuario = iNoUsuario+" - "+strNombreUsuario;
    String infoRegresar = "";
    String mensaje ="";
    JSONArray jsonArr = new JSONArray();
    JSONObject jsonObj =  new JSONObject();
    Map<String,Object> datos = new HashMap<>();
    
    CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
    
    // Obtengo los campos adicioanles 
    Vector nombresCampo = new Vector(5);
    nombresCampo = CargaDocumentos.getCamposAdicionales(iNoCliente);
    int  numCamposAdic =0;
    numCamposAdic =  nombresCampo.size();
    StringBuilder camposDes = new StringBuilder();  
    Map mapNombres = new LinkedHashMap();
    for (int i=0; i<numCamposAdic; i++) { 			
	Vector lovRegistro = (Vector) nombresCampo.get(i);
	camposDes.append(camposDes.length() > 0 ? ", " : "").append((String) lovRegistro.get(1) ); 
	mapNombres.put("CAMPO"+i,(String) lovRegistro.get(1));
    }			
    
    
			    
    com.netro.descuento.ConsRevisionJuridica paginador =new com.netro.descuento.ConsRevisionJuridica();
        
    log.debug ("mapNombres---"+mapNombres+"---" );
    
    log.debug ("informacion---"+informacion+"---" );
    log.debug ("nombrePyme  "+nombrePyme );
    log.debug ("rfcPyme  "+rfcPyme );
    log.debug ("numPyme  "+numPyme );
    log.debug ("txtNafelec  "+txtNafelec );
    log.debug ("icPyme  "+icPyme );
    
    //Para criterrios de busqueda  de la consulta
    paginador.setIcPyme(icPyme);
    paginador.setIcEpo(iNoCliente);
    paginador.setDfFechaVencMin(dfFechaVencMin);
    paginador.setDfFechaVencMax(dfFechaVencMax);
    paginador.setNumContrato(numContrato);
    paginador.setNumDocumento(numDocumento);
    paginador.setNumCopade(numCopade);
    paginador.setCampo1(campo1.replace("*", ""));
    paginador.setCampo2(campo2.replace("*", ""));
    paginador.setCampo3(campo3.replace("*", ""));
    paginador.setCampo4(campo4.replace("*", ""));
    paginador.setCampo5(campo5.replace("*", ""));
    paginador.setNumCamposAdic(numCamposAdic);
    paginador.setCamposAdicionales(camposDes.toString());
    
    // para generar el pdf de Acuse 
    paginador.setAcuse(acusejur);
    paginador.setAcuseFormateado(acuseFormateado);
    paginador.setRecibo(recibo);
    paginador.setFechaValida(fechaValida);
    paginador.setHoraValida(horaValida);
    paginador.setUsuarioValida(nomUsuario);
    paginador.setTotalDoctosMN( Integer.parseInt(totalDoctosMN)  );
    paginador.setTotalMontoDoctosMN(totalMontoDoctosMN);
    paginador.setTotalMontoDescontarMN(totalMontoDescontarMN);
     
    paginador.setTotalDoctosDL( Integer.parseInt(totalDoctosDL) );
    paginador.setTotalMontoDoctosDL(totalMontoDoctosDL);
    paginador.setTotalMontoDescontarDL(totalMontoDescontarDL);
   
    
if ("valoresIniciales".equals(informacion)   ) {

    jsonObj = new JSONObject();
    jsonObj.put("success", new Boolean(true));
    jsonObj.put("numCamposAdic", String.valueOf(numCamposAdic));
    jsonObj.put("mapNombres", mapNombres);    
    infoRegresar = jsonObj.toString();
     
}else  if ("CatalogoBuscaAvanzada".equals(informacion)   ) {
    
    Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
    
    Registros registros = afiliacion.getProveedores("1", "",numPyme,rfcPyme,nombrePyme,"","","");
      
    List coleccionElementos = new ArrayList();
    JSONObject jsonObjP =  new JSONObject();		
    while (registros.next()){
	
	jsonObjP= new JSONObject();		
	jsonObjP.put("clave", registros.getString("IC_NAFIN_ELECTRONICO"));
	jsonObjP.put("descripcion", registros.getString("IC_NAFIN_ELECTRONICO")+" "+registros.getString("CG_RAZON_SOCIAL"));
	jsonObjP.put("icPyme",registros.getString("IC_PYME"));
	jsonArr.add(jsonObjP);		
    }	
	
    jsonObj = new JSONObject();
    jsonObj.put("success", new Boolean(true));
    if (jsonArr.size() == 0){
        jsonObj.put("total", "0");
        jsonObj.put("registros", "" );
    }else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
        jsonObj.put("total",  Integer.toString(jsonArr.size()));
        jsonObj.put("registros", jsonArr.toString() );
    }else if (jsonArr.size() > 1000 ){
        jsonObj.put("total", "excede" );
        jsonObj.put("registros", "" );
    }
    infoRegresar = jsonObj.toString();

}else  if ("pymeNombre".equals(informacion)   && !"".equals(txtNafelec)   ) {

    jsonObj = new JSONObject();
    jsonObj.put("success", new Boolean(true));    
   
    datos = new HashMap<>();
    datos = paginador.getNombrePyme(Integer.parseInt(txtNafelec));
    if(datos.size()>0) {
	jsonObj.put("icPyme", datos.get("icPyme").toString());
	jsonObj.put("txtNombre", datos.get("txtNombre").toString());
    }else {
	mensaje = "El nafin electronico no corresponde a una PyME o no existe.";
	jsonObj.put("success", new Boolean(true));   
	jsonObj.put("mensaje", mensaje);    

    }
    infoRegresar = jsonObj.toString();



}else  if ("catalogoCampo1Data".equals(informacion)  || "catalogoCampo2Data".equals(informacion)  || "catalogoCampo3Data".equals(informacion)  
|| "catalogoCampo4Data".equals(informacion) || "catalogoCampo5Data".equals(informacion) ) {

    CatalogoCamposAdicionales catalogo = new CatalogoCamposAdicionales();
    if ("catalogoCampo1Data".equals(informacion)){
	catalogo.setCampoClave("CG_CAMPO1");
	catalogo.setCampoDescripcion("CG_CAMPO1");
    }else  if ("catalogoCampo2Data".equals(informacion)) {
	catalogo.setCampoClave("CG_CAMPO2");
	catalogo.setCampoDescripcion("CG_CAMPO2");
    }else  if ("catalogoCampo3Data".equals(informacion)) {
	catalogo.setCampoClave("CG_CAMPO3");
	catalogo.setCampoDescripcion("CG_CAMPO3");
    }else  if ("catalogoCampo4Data".equals(informacion)) {
	catalogo.setCampoClave("CG_CAMPO4");
	catalogo.setCampoDescripcion("CG_CAMPO4");
    }else  if ("catalogoCampo5Data".equals(informacion)) {
	catalogo.setCampoClave("CG_CAMPO5");
	catalogo.setCampoDescripcion("CG_CAMPO5");
    }
    
    catalogo.setTabla("com_documento");	
     catalogo.setClaveEpo(iNoCliente);	
    catalogo.setIcEstatusDocto("3");
    catalogo.setValidaJR("N");
   
    infoRegresar = catalogo.getJSONElementos();	

}else  if ("Consultar".equals(informacion)    ||  "ArchivoCSV".equals(informacion)  ||  "GeneraPDF".equals(informacion) ) {

    CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
    
    if ("Consultar".equals(informacion) ) {
	try{
	    Registros reg = queryHelper.doSearch();
	    infoRegresar = "{\"success\": true, \"total\": \"" + reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
	} catch(Exception e){
	throw new AppException("Error en la paginación", e);
	}
    }else  if ("ArchivoCSV".equals(informacion)    ||  "GeneraPDF".equals(informacion)  ) {

	try{
	    String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, tipoArch);
	    jsonObj.put("success", new Boolean(true));
	    jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	    infoRegresar = jsonObj.toString();
	} catch(Throwable e){
	    throw new AppException("Error al generar el archivo CSV. ", e);
	}
    }
    
    
}else  if ("ConfirmaRevision".equals(informacion)   ) { 
    Acuse acuse = null;
    acuse = new Acuse(Acuse.ACUSE_EPO, "1");
    char getReceipt = 'Y';
    String folioCert  ="";
    String _acuse  ="";

    String pkcs7 = (request.getParameter("pkcs7") != null) ? request.getParameter("pkcs7") : "";
    String textoFirmado = (request.getParameter("textoFirmado")==null)?"":request.getParameter("textoFirmado");
    String seleccionados[] = request.getParameterValues("modificados");
    
       
    String fecha = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
    String horario = (new SimpleDateFormat ("hh:mm:ss a")).format(new java.util.Date());
   
    boolean exito = false ;
    mensaje ="";
    String  mostrarAcuse="N";
   
    if (!"".equals(_serial)   && !"".equals(textoFirmado)   && !"".equals(pkcs7)    ) { 
	
	folioCert = acuse.toString();			
	Seguridad s = new Seguridad();
				
	if (s.autenticar(folioCert, _serial, pkcs7, textoFirmado, getReceipt)) {
	    _acuse = s.getAcuse();
	    
	    datos = new HashMap();
	    datos.put("RECIBO",_acuse);
	    datos.put("ACUSE",folioCert);
	    datos.put("FECHA",fecha);
	    datos.put("HORA",horario);
	    datos.put("USUARIO",iNoUsuario);
	    
	    datos.put("TOTALMN",totalDoctosMN);
	    datos.put("TMONTOMN",totalMontoDoctosMN);
	    datos.put("TOTALDL",totalDoctosDL);
	    datos.put("TMONTODL",totalMontoDoctosDL);
	   
	   
	    exito = paginador.procesaRevJuridica( datos ,  seleccionados );
	   	
	    if(exito==true){
	    
	     // para generar el pdf de Acuse 
		paginador.setAcuse(folioCert);
		paginador.setAcuseFormateado(acuse.formatear());
		paginador.setRecibo(_acuse);
		paginador.setFechaValida(fecha);
		paginador.setHoraValida(horario);
		paginador.setUsuarioValida(nomUsuario);
		
		paginador.setTotalDoctosMN( Integer.parseInt(totalDoctosMN)  );
		paginador.setTotalMontoDoctosMN(totalMontoDoctosMN);
		paginador.setTotalMontoDescontarMN(totalMontoDescontarMN);
		 
		paginador.setTotalDoctosDL( Integer.parseInt(totalDoctosDL) );
		paginador.setTotalMontoDoctosDL(totalDoctosDL);
		paginador.setTotalMontoDescontarDL(totalMontoDoctosDL);
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		
		String email = (request.getParameter("email")==null)?"":request.getParameter("email");
		try {
		    UtilUsr usuario = new UtilUsr();
		    Usuario user = new Usuario(); 
		    user = usuario.getUsuario(iNoUsuario);
		    email = user.getEmail();
		    
		StringBuilder msgHTML = new StringBuilder();   
		msgHTML.append(" Estimado(a) usuario(a) de Cadenas Productivas NAFIN.");
		msgHTML.append(" \n\n La(s) siguiente(s) operación(es) ha(n) sido validada(s) jurídicamente en el servicio de Cadenas Productivas "+
		" de Nacional Financiera S.N.C. por internet: \n\n ");
	      
		msgHTML.append(" \t Operado(s) el día "+fecha+ "  a las "+horario +" horas ");
		
		msgHTML.append(" \n\n Nota: La fecha y hora de este mensaje ('Enviado el') podrá variar respecto a la fecha/hora real "+
			    " de operación de su transacción ('Operado el'). " );
		msgHTML.append(" Esto dependerá de la configuración de sus servicios de correo y/o computadora en cuanto a su zona horaria.");
		msgHTML.append( " \n\n  Se anexa archivo de ACUSE DE RECIBO. ");
		msgHTML.append( " \n\n Atentamente. ");
		msgHTML.append( " \n\n Nacional Financiera, S.N.C ");
		
		//CorreoArchivoAdjunto.AttachFile("cadenas@nafin.gob.mx",email,"Documentos a descontar",msgHTML.toString(),nombreArchivo,strDirectorioTemp);
		
		
		} catch (Exception e) {
		    System.out.println("Exception: " + e.getCause());// Debug info
		    e.printStackTrace();
		}	
								
		mensaje = "La autentificación se llevó a cabo con éxito. Recibo: "+_acuse;
		mostrarAcuse ="S";
	    }else  {
		mensaje = "La autentificación no se llevo acabo con éxito "+s.mostrarError();
		mostrarAcuse ="N";
	    }
	}
    }   
   
    jsonObj.put("success", new Boolean(true));
    jsonObj.put("mensaje",mensaje);
    jsonObj.put("mostrarAcuse",mostrarAcuse);	
    jsonObj.put("recibo",_acuse);
    jsonObj.put("acuse",acuse.formatear());
    jsonObj.put("folioCert",folioCert);
    jsonObj.put("fecha",fecha);
    jsonObj.put("hora",horario);
    jsonObj.put("usuario",nomUsuario);	 
    infoRegresar  = jsonObj.toString();
    
}

log.debug("infoRegresar  "+infoRegresar );

%>
<%=infoRegresar%>