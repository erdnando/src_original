<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion"))!=null?request.getParameter("informacion"):"";
String infoRegresar = "";

com.netro.descuento.ConsPuntosRebate paginador = new com.netro.descuento.ConsPuntosRebate();
paginador.setClaveEPO(iNoEPO);
CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
if(informacion.equals("Consulta")){
	try{
		Registros reg	= queryHelper.doSearch();
		infoRegresar	= 
				"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";		
	}catch(Exception e) {
		throw new AppException("Error en la paginación",e);
	}
}else if(informacion.equals("Guardar")){
	try{
		ParametrosDescuento BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
		String puntosReb = (request.getParameter("puntosRebate") == null)?"":request.getParameter("puntosRebate");
		JSONArray jsonArray = JSONArray.fromObject(puntosReb);
		Object puntos_Rebate[] = jsonArray.toArray();
		String puntosRebate[] = new String[puntos_Rebate.length];
		
		for(int i=0;i<puntos_Rebate.length;i++){
			puntosRebate[i] = puntos_Rebate[i].toString();
		}
		boolean transac = BeanParametros.actualizaPuntosRebate(iNoEPO,puntosRebate);
		if(transac){
			infoRegresar = "{\"success\": true, \"mensaje\": \"Parametros actualizados satisfactoriamente.\"}";
		}else{
			infoRegresar = "{\"success\": true, \"mensaje\": \"Hubo problemas al actualizar los parametros.\"}";
		}	
	}catch(Exception e){
		throw new AppException("Error al actualizar los campos.",e);
	}
}
%>
<%=infoRegresar%>