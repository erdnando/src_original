var eliminarDetalle;
var modificarDetalle;
	
Ext.onReady(function() {
		
	var strPendiente =  Ext.getDom('strPendiente').value;
	var strPreNegociable =  Ext.getDom('strPreNegociable').value;
	
	var hidCtrlNumDoctosMN =  Ext.getDom('hidCtrlNumDoctosMN').value;
	var hidCtrlMontoTotalMN = Ext.getDom('hidCtrlMontoTotalMNDespliegue').value;
	var hidCtrlMontoMinMN =  Ext.getDom('hidCtrlMontoMinMNDespliegue').value;
	var hidCtrlMontoMaxMN  = Ext.getDom('hidCtrlMontoMaxMNDespliegue').value;
	
	var hidCtrlNumDoctosDL =  Ext.getDom('hidCtrlNumDoctosDL').value;
	var hidCtrlMontoTotalDL =  Ext.getDom('hidCtrlMontoTotalDLDespliegue').value;
	var hidCtrlMontoMinDL =  Ext.getDom('hidCtrlMontoMinDLDespliegue').value;
	var hidCtrlMontoMaxDL  =  Ext.getDom('hidCtrlMontoMaxDLDespliegue').value;

	var bOperaFactConMandato =  Ext.getDom('bOperaFactConMandato').value;
	var bOperaFactorajeVencido =  Ext.getDom('bOperaFactorajeVencido').value;
	var bOperaFactorajeDistribuido =  Ext.getDom('bOperaFactorajeDistribuido').value;
	var bOperaNotasCredito =  Ext.getDom('bOperaNotasCredito').value;
	var bOperaFactorajeVencidoInfonavit =  Ext.getDom('bOperaFactorajeVencidoInfonavit').value;

	var sPubEPOPEFDocto3 =  Ext.getDom('sPubEPOPEFDocto3').value;
	var sPubEPOPEFDocto3Val =  Ext.getDom('sPubEPOPEFDocto3Val').value;
	var sPubEPOPEFDocto1 =  Ext.getDom('sPubEPOPEFDocto1').value;
	var sPubEPOPEFDocto1Val =  Ext.getDom('sPubEPOPEFDocto1Val').value;
	var sPubEPOPEFFlag =  Ext.getDom('sPubEPOPEFFlag').value;
	var sPubEPOPEFFlagVal =  Ext.getDom('sPubEPOPEFFlagVal').value;
	var sPubEPOPEFDocto4 =  Ext.getDom('sPubEPOPEFDocto4').value;
	var sPubEPOPEFDocto4Val =  Ext.getDom('sPubEPOPEFDocto4Val').value;
	var sPubEPOPEFDocto2 =  Ext.getDom('sPubEPOPEFDocto2').value;
	var sPubEPOPEFDocto2Val =  Ext.getDom('sPubEPOPEFDocto2Val').value;
	
	var campos =  Ext.getDom('campos').value;
	var hidFechaActual =  Ext.getDom('hidFechaActual').value;
	var hidFechaVencMin =  Ext.getDom('hidFechaVencMin').value;
	var hidFechaVencMax =  Ext.getDom('hidFechaVencMax').value;
	var hidFechaPlazoPagoFVP =  Ext.getDom('hidFechaPlazoPagoFVP').value;
	var hidPlazoMax1FVP =  Ext.getDom('hidPlazoMax1FVP').value;
	var hidPlazoMax2FVP =  Ext.getDom('hidPlazoMax2FVP').value;
	 
	var operaFVPyme =  Ext.getDom('operaFVPyme').value;
	
	var spubdocto_venc =  Ext.getDom('spubdocto_venc').value;
	var noCampos =  Ext.getDom('noCampos').value;
	var noCamposDetalle =  Ext.getDom('noCamposDetalle').value;

	var hidMontoTotalDL =0;
	var hidNumDoctosActualesDL =0;
	var hidMontoTotalMN =0;
	var hidNumDoctosActualesMN =0;	
	var proceso;	
	var cg_campo01 =  Ext.getDom('cg_campo01').value;
	var cg_campo02 =  Ext.getDom('cg_campo02').value;
	var cg_campo03 =  Ext.getDom('cg_campo03').value;
	var cg_campo04 =  Ext.getDom('cg_campo04').value;
	var cg_campo05 =  Ext.getDom('cg_campo05').value;
			
	var campoFechas = new Ext.form.DateField({
		startDay: 0		
	});
			
	////////////////////////INICIA  CAPTURA DE DETALLES /////////////////////////////////////////////////////	
	
	var procesaPymeBloqXNumProv = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);	
			if(resp.pymeBloqueada){
				Ext.getCmp("cg_pyme_epo_interno1").markInvalid('Pyme Bloqueada');
				Ext.getCmp('numProvValidHid1').setValue('N');
			}
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesaPymeBloqueada = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);	
			if(resp.pymeBloqueada){
				Ext.getCmp("ic_pyme1").markInvalid('Pyme Bloqueada');
				Ext.getCmp('cvePymeValidHid1').setValue('N');
			}
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Modificar el registro  del Detalle de Captura 
	var procesarSuccessFailureModificarDetalle =  function(opts, success, response) {		
		var gridDetalles = Ext.getCmp('gridDetalles');
		gridDetalles.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			//realiza nuevamente la consulta del grid de detalles 
			var datos = Ext.util.JSON.decode(response.responseText);			
			
			for(var i= 1; i<=noCamposDetalle; i++) {	
				if(i==1){	 	campo1 = 	Ext.getCmp("campo1").setValue(datos.campo1);   }
				if(i==2){	 	campo2 = Ext.getCmp("campo2").setValue(datos.campo2); }
				if(i==3){	 	campo3 = Ext.getCmp("campo3").setValue(datos.campo3); }
				if(i==4){	 	campo4 = Ext.getCmp("campo4").setValue(datos.campo4);}
				if(i==5){	 	campo5 = Ext.getCmp("campo5").setValue(datos.campo5); }
				if(i==6){	 	campo6 = Ext.getCmp("campo6").setValue(datos.campo6); }
				if(i==7){	 	campo7 = Ext.getCmp("campo7").setValue(datos.campo7); }
				if(i==8){	 	campo8 = Ext.getCmp("campo8").setValue(datos.campo8); }
				if(i==9){	 	campo9 = Ext.getCmp("campo9").setValue(datos.campo9); }
				if(i==10){	campo10 = Ext.getCmp("campo10").setValue(datos.campo10); }			
		}
			//realiza nuevamente la consulta del grid de detalles 
			var datos = Ext.util.JSON.decode(response.responseText);			
			Ext.Ajax.request({
				url: '13forma01Ext.data.jsp',
				params: {
					informacion: 'ConsultaDetalle',
					proceso:datos.proceso,
					ic_pyme:datos.ic_pyme,
					ig_numero_docto:datos.ig_numero_docto,
					df_fecha_docto:datos.df_fecha_docto,
					ic_moneda:datos.ic_moneda					
				},
				callback: procesarSuccessFailureConsultaDetalle
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Modificar el registro  del Detalle de Captura 
	modificarDetalle = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var noConsecutivo = registro.get('CONSECUTIVO');
		var  ic_pyme = registro.get('IC_PYME');
		var  ig_numero_docto = registro.get('NUM_DOCTO');
		var  df_fecha_docto = registro.get('FECHA');
		var  ic_moneda = registro.get('IC_MONEDA');		
		proceso = registro.get('PROCESO');
			
		var gridDetalles = Ext.getCmp('gridDetalles');
		var store = gridDetalles.getStore();
		gridDetalles.el.mask('Actualizando...', 'x-mask-loading');
		gridDetalles.stopEditing();
		
		Ext.Ajax.request({
			url : '13forma01Ext.data.jsp',
			params : {
				informacion: 'ModificarDetalle',
				noConsecutivo:noConsecutivo,
				proceso:proceso,
				ic_pyme:ic_pyme,
				ig_numero_docto:ig_numero_docto,
				df_fecha_docto:df_fecha_docto,
				ic_moneda:ic_moneda	
			},
			callback: procesarSuccessFailureModificarDetalle
		});
 }

	//Elimina el registro  del Detalle de Captura 
	var procesarSuccessFailureBorrarDetalle =  function(opts, success, response) {		
		var gridDetalles = Ext.getCmp('gridDetalles');
		gridDetalles.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			//realiza nuevamente la consulta del grid de detalles 
			var datos = Ext.util.JSON.decode(response.responseText);			
			Ext.Ajax.request({
				url: '13forma01Ext.data.jsp',
				params: {
					informacion: 'ConsultaDetalle',
					proceso:datos.proceso,
					ic_pyme:datos.ic_pyme,
					ig_numero_docto:datos.ig_numero_docto,
					df_fecha_docto:datos.df_fecha_docto,
					ic_moneda:datos.ic_moneda					
				},
				callback: procesarSuccessFailureConsultaDetalle
			});					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Elimina el registro  del Detalle de Captura 
	eliminarDetalle = function(grid, rowIndex, colIndex, item, event) {

		var registro = grid.getStore().getAt(rowIndex);
		var noConsecutivo = registro.get('CONSECUTIVO');
		var  ic_pyme = registro.get('IC_PYME');
		var  ig_numero_docto = registro.get('NUM_DOCTO');
		var  df_fecha_docto = registro.get('FECHA');
		var  ic_moneda = registro.get('IC_MONEDA');		
		proceso = registro.get('PROCESO');
			
		var gridDetalles = Ext.getCmp('gridDetalles');
		var store = gridDetalles.getStore();
		gridDetalles.el.mask('Actualizando...', 'x-mask-loading');
		gridDetalles.stopEditing();
		Ext.Ajax.request({
			url : '13forma01Ext.data.jsp',
			params : {
				informacion: 'EliminarDetalle',
				noConsecutivo:noConsecutivo,
				proceso:proceso,
				ic_pyme:ic_pyme,
				ig_numero_docto:ig_numero_docto,
				df_fecha_docto:df_fecha_docto,
				ic_moneda:ic_moneda	
			},
			callback: procesarSuccessFailureBorrarDetalle
		});	
	}
	
	var terminaDetalle = function() {
		var ventana = Ext.getCmp('capDetalles');
		if (ventana) {	
			ventana.destroy();	
		}
	}
	
	var procesarSuccessFailureConsultaDetalle =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
		
			var jsonObj = Ext.util.JSON.decode(response.responseText)
			var fpDetalle = Ext.getCmp('fpDetalle');							
				
			consultaDataDetalles.fields =  jsonObj.columnasStore;
			consultaDataDetalles.data =  jsonObj.columnasRecords;
			gridDetalles.columns = jsonObj.columnasGrid;	
						
			fpDetalle.remove("gridDetalles");			
			fpDetalle.add(gridDetalles);
			fpDetalle.el.unmask();
			fpDetalle.doLayout();	
		
		} else {		
			NE.util.mostrarConnError(response,opts);
		}		
	}
	
	
	var procesarSuccessFailureCapturaDetalle =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var datos = Ext.util.JSON.decode(response.responseText);
					
			Ext.Ajax.request({
				url: '13forma01Ext.data.jsp',
				params: {
					informacion: 'ConsultaDetalle',
					proceso:datos.proceso,
					ic_pyme:datos.ic_pyme,
					ig_numero_docto:datos.ig_numero_docto,
					df_fecha_docto:datos.df_fecha_docto,
					ic_moneda:datos.ic_moneda					
				},
				callback: procesarSuccessFailureConsultaDetalle
			});	
					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	
	var limpiarDetalle  = function() {
	
		var campo1; var campo2;  var campo3;  var campo4;  var campo5;  var campo6;  var campo7;  var campo8;  var campo9;  var campo10;	
		for(var i= 1; i<=noCamposDetalle; i++) {	
			if(i==1){	 	campo1 = 	Ext.getCmp("campo1").setValue('');   }
			if(i==2){	 	campo2 = Ext.getCmp("campo2").setValue(''); }
			if(i==3){	 	campo3 = Ext.getCmp("campo3").setValue(''); }
			if(i==4){	 	campo4 = Ext.getCmp("campo4").setValue('');}
			if(i==5){	 	campo5 = Ext.getCmp("campo5").setValue(''); }
			if(i==6){	 	campo6 = Ext.getCmp("campo6").setValue(''); }
			if(i==7){	 	campo7 = Ext.getCmp("campo7").setValue(''); }
			if(i==8){	 	campo8 = Ext.getCmp("campo8").setValue(''); }
			if(i==9){	 	campo9 = Ext.getCmp("campo9").setValue(''); }
			if(i==10){	campo10 = Ext.getCmp("campo10").setValue(''); }			
		}			
	}
	
	var agregarDetalle = function() {
		var noConsecutivo; 	var ic_pyme; 		var df_fecha_docto; 		var ic_moneda;
		var campo1; var campo2;  var campo3;  var campo4;  var campo5;  var campo6;  var campo7;  var campo8;  var campo9;  var campo10;
		
		var ig_numero_docto =  Ext.getCmp("ig_numero_docto").getValue();
		var ic_pyme =  Ext.getCmp("ic_pyme").getValue();
		var df_fecha_docto =  Ext.util.Format.date( Ext.getCmp("df_fecha_docto").getValue(),'d/m/Y'); 
		var ic_moneda =  Ext.getCmp("ic_moneda").getValue();
					
		for(var i= 1; i<=noCamposDetalle; i++) {	
			if(i==1){	 
				campo1 = Ext.getCmp("campo1").getValue(); 				
				if (campo1=='' ){
					Ext.MessageBox.alert("Mensaje","Por favor introduzca al menos un valor ");
					return;
				}
			}
			if(i==2){	 	
				campo1 = Ext.getCmp("campo1").getValue(); 
				campo2 = Ext.getCmp("campo2").getValue(); 				
				if (campo1 ==''  &&  campo2=='' ){
					Ext.MessageBox.alert("Mensaje","Por favor introduzca al menos un valor ");
					return;
				}				
			}
			if(i==3){	 	
				campo1 = Ext.getCmp("campo1").getValue(); 
				campo2 = Ext.getCmp("campo2").getValue(); 
				campo3 = Ext.getCmp("campo3").getValue(); 
				if (campo1 ==''  && campo2 ==''   && campo3 =='' ){
					Ext.MessageBox.alert("Mensaje","Por favor introduzca al menos un valor ");
					return;
				}	
			}
			if(i==4){	 					
				campo1 = Ext.getCmp("campo1").getValue(); 
				campo2 = Ext.getCmp("campo2").getValue(); 
				campo3 = Ext.getCmp("campo3").getValue(); 
				campo4 = Ext.getCmp("campo4").getValue(); 
				if (campo1 =='' && campo2==''   && campo3 =='' && campo4 =='' ){
					Ext.MessageBox.alert("Mensaje","Por favor introduzca al menos un valor ");
					return;
				}				
			}
			if(i==5){	 	
				campo1 = Ext.getCmp("campo1").getValue(); 
				campo2 = Ext.getCmp("campo2").getValue(); 
				campo3 = Ext.getCmp("campo3").getValue(); 
				campo4 = Ext.getCmp("campo4").getValue(); 
				campo5 = Ext.getCmp("campo5").getValue();
				if ( campo1 ==''  && campo2 ==''   && campo3 =='' && campo4 =='' 	&& campo5 ==''){
					Ext.MessageBox.alert("Mensaje","Por favor introduzca al menos un valor ");
					return;
				}				
			}
			
			if(i==6){	 	
				campo1 = Ext.getCmp("campo1").getValue(); 
				campo2 = Ext.getCmp("campo2").getValue(); 
				campo3 = Ext.getCmp("campo3").getValue(); 
				campo4 = Ext.getCmp("campo4").getValue(); 
				campo5 = Ext.getCmp("campo5").getValue();
				campo6 = Ext.getCmp("campo6").getValue(); 
			if ( campo1 ==''  && campo2 ==''   && campo3 =='' && campo4 =='' 	&& campo5 =='' && campo6 ==''){
					Ext.MessageBox.alert("Mensaje","Por favor introduzca al menos un valor ");
					return;
				}				
			}
			if(i==7){	 				
				campo1 = Ext.getCmp("campo1").getValue(); 
				campo2 = Ext.getCmp("campo2").getValue(); 
				campo3 = Ext.getCmp("campo3").getValue(); 
				campo4 = Ext.getCmp("campo4").getValue(); 
				campo5 = Ext.getCmp("campo5").getValue();
				campo6 = Ext.getCmp("campo6").getValue(); 
				campo7 = Ext.getCmp("campo7").getValue(); 
				if ( campo1 ==''  && campo2 ==''   && campo3 =='' && campo4 =='' 	&& campo5 =='' && campo6 =='' && campo7 ==''){
					Ext.MessageBox.alert("Mensaje","Por favor introduzca al menos un valor ");
					return;
				}						
			}
			if(i==8){	 	
				campo1 = Ext.getCmp("campo1").getValue(); 
				campo2 = Ext.getCmp("campo2").getValue(); 
				campo3 = Ext.getCmp("campo3").getValue(); 
				campo4 = Ext.getCmp("campo4").getValue(); 
				campo5 = Ext.getCmp("campo5").getValue();
				campo6 = Ext.getCmp("campo6").getValue(); 
				campo7 = Ext.getCmp("campo7").getValue(); 
				campo8 = Ext.getCmp("campo8").getValue(); 				
				if ( campo1 ==''  && campo2 ==''   && campo3 =='' && campo4 =='' 	&& campo5 =='' && campo6 =='' && campo7 =='' && campo8 ==''){
					Ext.MessageBox.alert("Mensaje","Por favor introduzca al menos un valor ");
					return;
				}					
			}
			
			if(i==9){	 
				campo1 = Ext.getCmp("campo1").getValue(); 
				campo2 = Ext.getCmp("campo2").getValue(); 
				campo3 = Ext.getCmp("campo3").getValue(); 
				campo4 = Ext.getCmp("campo4").getValue(); 
				campo5 = Ext.getCmp("campo5").getValue();
				campo6 = Ext.getCmp("campo6").getValue(); 
				campo7 = Ext.getCmp("campo7").getValue(); 
				campo8 = Ext.getCmp("campo8").getValue(); 
				campo9 = Ext.getCmp("campo9").getValue(); 
				if ( campo1 ==''  && campo2 ==''   && campo3 =='' && campo4 =='' 	&& campo5 =='' && campo6 =='' && campo7 =='' && campo8 =='' && campo9 ==''){
					Ext.MessageBox.alert("Mensaje","Por favor introduzca al menos un valor ");
					return;
				}				
			}
			
			if(i==10){
				campo1 = Ext.getCmp("campo1").getValue(); 
				campo2 = Ext.getCmp("campo2").getValue(); 
				campo3 = Ext.getCmp("campo3").getValue(); 
				campo4 = Ext.getCmp("campo4").getValue(); 
				campo5 = Ext.getCmp("campo5").getValue();
				campo6 = Ext.getCmp("campo6").getValue(); 
				campo7 = Ext.getCmp("campo7").getValue(); 
				campo8 = Ext.getCmp("campo8").getValue(); 
				campo9 = Ext.getCmp("campo9").getValue(); 
				campo10 = Ext.getCmp("campo10").getValue();
				if ( campo1 ==''  && campo2 ==''   && campo3 =='' && campo4 =='' 	&& campo5 =='' && campo6 =='' && campo7 =='' && campo8 =='' && campo9 =='' && campo10 ==''){
					Ext.MessageBox.alert("Mensaje","Por favor introduzca al menos un valor ");
					return;
				}				
			}			
		}//for
					
		var fpDetalle = Ext.getCmp('fpDetalle');				
		Ext.Ajax.request({
			url : '13forma01Ext.data.jsp',
			params: {
				informacion:'CapturaDetalle',
				proceso:proceso,				
				ig_numero_docto:ig_numero_docto,
				ic_pyme:ic_pyme,
				df_fecha_docto:df_fecha_docto,
				ic_moneda:ic_moneda,					
				campo1:campo1,
				campo2:campo2,
				campo3:campo3,
				campo4:campo4,
				campo5:campo5,
				campo6:campo6,
				campo7:campo7,
				campo8:campo8,
				campo9:campo9,
				campo10:campo10
			},
			callback: procesarSuccessFailureCapturaDetalle
		});	
			
		//limpia variables 	
			limpiarDetalle();
	}
	
	//esto es para mostrar los campos adicionales
	function procesaCamposDetalle(opts, success, response) {
		var fpDetalle = Ext.getCmp('fpDetalle');
		fpDetalle.el.unmask();
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresDetalle = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresDetalle != null){
				var  noCamposDe = jsonValoresDetalle.numeroCampos;
										
				Ext.getCmp("ig_numero_docto").setValue(jsonValoresDetalle.ig_numero_docto);
				Ext.getCmp("ic_pyme").setValue(jsonValoresDetalle.ic_pyme);
				Ext.getCmp("df_fecha_docto").setValue(jsonValoresDetalle.df_fecha_docto);
				Ext.getCmp("ic_moneda").setValue(jsonValoresDetalle.ic_moneda);
				
				for(var i= 1; i<=noCamposDe; i++) {				
					if(i==1){		
						var campo1 =  Ext.decode(jsonValoresDetalle.campo1);						
						var indice1 = fpDetalle.items.indexOfKey('xxx1')+1;
						fpDetalle.insert(indice1,campo1);
						fpDetalle.doLayout();
					
					}		
					if(i==2){		
						var campo2 =  Ext.decode(jsonValoresDetalle.campo2);						
						var indice1 = fpDetalle.items.indexOfKey('campo1')+1;
						fpDetalle.insert(indice1,campo2);
						fpDetalle.doLayout();						
					}	
					if(i==3){		
						var campo3 =  Ext.decode(jsonValoresDetalle.campo3);						
						var indice1 = fpDetalle.items.indexOfKey('campo2')+1;
						fpDetalle.insert(indice1,campo3);
						fpDetalle.doLayout();						
					}		
					if(i==4){		
						var campo4 =  Ext.decode(jsonValoresDetalle.campo4);						
						var indice1 = fpDetalle.items.indexOfKey('campo3')+1;
						fpDetalle.insert(indice1,campo4);
						fpDetalle.doLayout();						
					}	
					if(i==5){		
						var campo5 =  Ext.decode(jsonValoresDetalle.campo5);						
						var indice1 = fpDetalle.items.indexOfKey('campo4')+1;
						fpDetalle.insert(indice1,campo5);
						fpDetalle.doLayout();						
					}	
					if(i==6){		
						var campo6 =  Ext.decode(jsonValoresDetalle.campo6);						
						var indice1 = fpDetalle.items.indexOfKey('campo5')+1;
						fpDetalle.insert(indice1,campo6);
						fpDetalle.doLayout();						
					}	
					if(i==7){		
						var campo7 =  Ext.decode(jsonValoresDetalle.campo7);						
						var indice1 = fpDetalle.items.indexOfKey('campo6')+1;
						fpDetalle.insert(indice1,campo7);
						fpDetalle.doLayout();						
					}	
					if(i==8){		
						var campo8 =  Ext.decode(jsonValoresDetalle.campo8);						
						var indice1 = fpDetalle.items.indexOfKey('campo7')+1;
						fpDetalle.insert(indice1,campo8);
						fpDetalle.doLayout();						
					}	
					if(i==9){		
						var campo9 =  Ext.decode(jsonValoresDetalle.campo9);						
						var indice1 = fpDetalle.items.indexOfKey('campo8')+1;
						fpDetalle.insert(indice1,campo9);
						fpDetalle.doLayout();						
					}		
					if(i==10){		
						var campo10 =  Ext.decode(jsonValoresDetalle.campo10);						
						var indice1 = fpDetalle.items.indexOfKey('campo9')+1;
						fpDetalle.insert(indice1,campo10);
						fpDetalle.doLayout();						
					}				
				}//for				
			}
			fpDetalle.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}	
		
	//se muestra la pantalla de envio de correo
	var procesaCapturaDetalle = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var proceso = registro.get('IC_PROCESO');
		var ic_pyme = registro.get('IC_PYME');		
		var ig_numero_docto = registro.get('NUM_DOCUMENTO');		
		var df_fecha_docto = registro.get('FECHA_EMISION');	
		var ic_moneda = registro.get('IC_MONEDA');	
		//realiza consulta para mostrar los campos de captura
		Ext.Ajax.request({
			url: '13forma01Ext.data.jsp',
			params: {
				informacion: "Detalles",
				ig_numero_docto:ig_numero_docto,
				ic_pyme:ic_pyme,			
				df_fecha_docto:df_fecha_docto,
				ic_moneda:ic_moneda	
			},
			callback: procesaCamposDetalle
		});		
			//realiza consulta para mostrar el grid	
		Ext.Ajax.request({
				url: '13forma01Ext.data.jsp',
				params: {
					informacion: 'ConsultaDetalle',
					proceso:proceso,
					ic_pyme:ic_pyme,
					ig_numero_docto:ig_numero_docto,
					df_fecha_docto:df_fecha_docto,
					ic_moneda:ic_moneda					
				},
				callback: procesarSuccessFailureConsultaDetalle
			});			
			
		var ventana = Ext.getCmp('capDetalles');	
		if (ventana) {
			ventana.show();
		}else {	
			new Ext.Window({			
				width: 850,
				height: 'auto',
				autoDestroy:true,
				closable:false,
				resizable: false,
				autoScroll:true, 
				closeAction: 'hide',
				id: 'capDetalles',
				align: 'center',
				items: [					
					fpDetalle										
				],
				title: 'Captura Detalles'						
			}).show();
		}
	}	
	
	
	var consultaDataDetalles = {
		xtype: 'jsonstore',
		id:'consultaDataDetalles',
		root: 'registros',
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		fields: null,
		data: null
	};
	
	var gridDetalles = {		
		xtype: 'grid',	
		id:'gridDetalles',	
		store:consultaDataDetalles,		
		columns: [],		
		margins: '20 0 0 0',
		stripeRows: true,
		loadMask: true,
		frame: true,
		height: 200,
		width: 800,	
		style: 'margin:0 auto;',
		title: 'Detalles'
	};
		
	var elementosFormaDetalle = [
		{
				xtype: 'textfield',
				name: 'ig_numero_docto',
				id: 'ig_numero_docto',				
				allowBlank: true,
				hidden: true,			
				margins: '0 20 0 0'  
		},
		{
				xtype: 'textfield',
				name: 'ic_pyme',
				id: 'ic_pyme',				
				allowBlank: true,
				hidden: true,			
				margins: '0 20 0 0'  
		},
		{
				xtype: 'datefield',
				name: 'df_fecha_docto',
				id: 'df_fecha_docto',				
				allowBlank: true,
				hidden: true,			
				margins: '0 20 0 0'  
		},
		{
				xtype: 'textfield',
				name: 'ic_moneda',
				id: 'ic_moneda',				
				allowBlank: true,
				hidden: true,			
				margins: '0 20 0 0'  
		},				
		{
				xtype: 'numberfield',
				name: 'xxx',
				id: 'xxx1',				
				allowBlank: true,
				hidden: true,			
				margins: '0 20 0 0'  
		}
	];
	
	var elementosFormaDetalleB = [
		{
			xtype: 		'panel',
			bodyStyle: 	'padding: 10px',
			layout: {
				type: 	'hbox',
				pack: 	'center',
				align: 	'middle'
			},
			items: [
				{
					xtype: 'button',
					text: 'Agregar',
					id: 'btnAgregarD',
					iconCls: 'icoAceptar',
					xtype: 'button',
					width: 	75,
					handler: agregarDetalle,
					style: { 
						marginBottom:  '10px' 
					} 
				},
				{
					xtype: 'button',
					text: 'Limpiar',
					iconCls: 'icoLimpiar',
					id: 'btnLimpiarD',			
					xtype: 'button',
					width: 	75,
					handler: limpiarDetalle,
					style: { 
						marginBottom:  '10px' 
					} 
				}
			]
		}	
	];
	
	var fpDetalle = {
		xtype: 'panel',
		id: 'fpDetalle',
		layout: 'form',
		width: 800,	
		title: '',
		frame: true,
		collapsible: false,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
			elementosFormaDetalle,
			elementosFormaDetalleB
		],
		monitorValid: true,
		buttons: [			
			{
				xtype: 'button',
				iconCls: 'icoAceptar',
				text: 'Terminar captura',
				id: 'btnTerminarCa',
				handler: terminaDetalle
			}
		]
	};
	

	
	////////////////////////PANTALLA PRINCIPAL/////////////////////////////////
	
	// modifica el registro 
	var procesarSuccessFailureModificar =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var datos = Ext.util.JSON.decode(response.responseText);
			var estatus;
			if(datos.mod_ic_estatus_docto==2){
				estatus ='S';
			}else if(datos.mod_ic_estatus_docto==1){
				estatus ='N';
			}
			
			Ext.getCmp("ic_pyme1").setValue(datos.mod_ic_pyme);
			Ext.getCmp("ig_numero_docto1").setValue(datos.mod_ig_numero_docto);
			Ext.getCmp("df_fecha_docto1").setValue(datos.mod_df_fecha_docto);
			Ext.getCmp("df_fecha_venc1").setValue(datos.mod_df_fecha_venc);
			Ext.getCmp("ic_moneda1").setValue(datos.mod_ic_moneda);
			Ext.getCmp("fn_monto1").setValue(datos.mod_fn_monto);
			Ext.getCmp("ic_tipo_fact1").setValue(datos.mod_ic_tipo_fact);
			Ext.getCmp("ic_estatus_docto1").setValue(estatus);
			Ext.getCmp("ic_mandante1").setValue(datos.mod_NombreMandate);
			Ext.getCmp("ic_if1").setValue(datos.mod_ic_if);
			Ext.getCmp("ct_referencia1").setValue(datos.mod_ct_referencia);
			Ext.getCmp("ic_beneficiario1").setValue(datos.mod_ic_beneficiario);
			Ext.getCmp("fn_porc_beneficiario1").setValue(datos.mod_porc_beneficiario);
			Ext.getCmp("fn_monto_beneficiario1").setValue(datos.mod_mto_beneficiario);
			Ext.getCmp("df_fecha_entrega1").setValue(datos.mod_df_fecha_entrega);
			Ext.getCmp("cg_tipo_compra1").setValue(datos.mod_cg_tipo_compra);
			Ext.getCmp("cg_clave_presupuestaria1").setValue(datos.mod_cg_clave_presupuestaria);
			Ext.getCmp("cg_periodo1").setValue(datos.mod_cg_periodo);
			
			var ic_mandante = Ext.getCmp("ic_mandante1");
			var ic_if = Ext.getCmp("ic_if1");
			var ic_mandante2 = Ext.getCmp("ic_mandante2");
			var ic_if2 = Ext.getCmp("ic_if2");
			var ic_beneficiario = Ext.getCmp("ic_beneficiario1");
			var ic_beneficiario2 = Ext.getCmp("ic_beneficiario2");						
			var fn_porc_beneficiario = Ext.getCmp("fn_porc_beneficiario1");
			var fn_monto_beneficiario = Ext.getCmp("fn_monto_beneficiario1");
			var fn_porc_beneficiario02 = Ext.getCmp("fn_porc_beneficiario2");
			var fn_monto_beneficiario02 = Ext.getCmp("fn_monto_beneficiario2");
			var ic_estatus_docto   = Ext.getCmp("ic_estatus_docto1");
			var ic_estatus_docto2   = Ext.getCmp("ic_estatus_docto2");
			var tipo_fatoraje = datos.mod_ic_tipo_fact;
			
			if(tipo_fatoraje=='M' && bOperaFactConMandato =='S') {
				catalogoMandanteData.load();
				ic_mandante.show();
				ic_mandante2.show();
			}else {
				ic_mandante.hide();
				ic_mandante2.hide();
			}
			if( ( bOperaFactorajeVencido =='S' && tipo_fatoraje=='V')  
				||   (bOperaFactConMandato =='S'  &&  tipo_fatoraje=='M') 
				||   (bOperaFactorajeVencidoInfonavit =='S'  &&  tipo_fatoraje=='I')) {
				catalogoIFData.load();
				ic_if.show();
				ic_if2.show();
			}else {
				ic_if.hide();
				ic_if2.hide();
			}	
			
			if(  ( bOperaFactorajeDistribuido =='S' && tipo_fatoraje=='D' )  
				|| ( bOperaFactorajeVencidoInfonavit && tipo_fatoraje=='I') ) {
				catalogoBeneficiarioData.load();
				ic_beneficiario.show();
				ic_beneficiario2.show();
				fn_porc_beneficiario.show();
				fn_monto_beneficiario.show();	
				fn_porc_beneficiario02.show();
				fn_monto_beneficiario02.show()
			}else {
				ic_beneficiario.hide();
				ic_beneficiario2.hide();
				fn_porc_beneficiario.hide();
				fn_monto_beneficiario.hide();		
				fn_porc_beneficiario02.hide();
				fn_monto_beneficiario02.hide()
			}		
			//para mostrar los radio de Negociable
			if(tipo_fatoraje=='I'  || tipo_fatoraje=='V'  || tipo_fatoraje=='D'){
				ic_estatus_docto.hide();
				ic_estatus_docto2.hide();
			}else if(tipo_fatoraje=='N'  || tipo_fatoraje=='M' ){
				ic_estatus_docto.show();
				ic_estatus_docto2.show();		
				ic_estatus_docto.enable();	
			}else if( tipo_fatoraje=='C' ){
				ic_estatus_docto.show();	
				ic_estatus_docto.disable();				
				ic_estatus_docto2.show();										
			}	
							
			if(campos=='S'){
				for(var i= 1; i<=noCampos; i++) {
					if(i==1){		Ext.getCmp("cg_campo1").setValue(datos.cg_campo1);	}	
					if(i==2){		Ext.getCmp("cg_campo2").setValue(datos.cg_campo2);	}
					if(i==3){		Ext.getCmp("cg_campo3").setValue(datos.cg_campo3);	}
					if(i==4){		Ext.getCmp("cg_campo4").setValue(datos.cg_campo4);	}
					if(i==5){		Ext.getCmp("cg_campo5").setValue(datos.cg_campo5);	}
				}
			}
			consultaData.load({ params: {  informacion: 'Consulta', 	proceso:proceso,  strPendiente:strPendiente,  	strPreNegociable:strPreNegociable,		pantalla:'Captura' 		}  });		
			
			totalesData.load({ 	params: { informacion: 'ResumenTotales', 	proceso:proceso,	strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, pantalla:'Captura'	}   });			
		}	
	}
	
	var procesaAccionModificar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var noConsecutivo = registro.get('CONSECUTIVO');		
		Ext.Ajax.request({
			url: '13forma01Ext.data.jsp',
			params: {
				informacion: "Modificar",
				proceso:proceso,
				noConsecutivo:noConsecutivo	
			},
			callback: procesarSuccessFailureModificar
		});			
	}
		
		//Elimina el registro 
	var procesarSuccessFailureBorrar =  function(opts, success, response) {		
		var gridCaptura = Ext.getCmp('gridCaptura');
		gridCaptura.el.unmask();
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			gridCaptura.getStore().commitChanges();
			fpCarga.el.mask('Cargando Consulta ...', 'x-mask-loading');	
			
			consultaData.load({  params: { 	informacion: 'Consulta', 		proceso:proceso,	strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, 	pantalla:'Captura'		}  	});	
				
			totalesData.load({ 	params: { 	informacion: 'ResumenTotales', 	proceso:proceso, 	strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, 	pantalla:'Captura' 		} 	});
				
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
		//Elimina el registro 
	var procesaAccionBorrar = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var noConsecutivo = registro.get('CONSECUTIVO');
		var gridCaptura = Ext.getCmp('gridCaptura');
		var store = gridCaptura.getStore();
		gridCaptura.el.mask('Actualizando...', 'x-mask-loading');
		gridCaptura.stopEditing();
			Ext.Ajax.request({
			url : '13forma01Ext.data.jsp',
				params : {
					informacion: 'Eliminar',
					proceso:proceso,
					noConsecutivo:noConsecutivo	
				},
				callback: procesarSuccessFailureBorrar
			});			
	}
	
	//termina proceso de captura 
	function procesarTerminaCaptura(opts, success, response) {	
		var ic_moneda;
		var hidNumDoctosActualesMN =0;
		var hidNumDoctosActualesDL =0; 
		var hidMontoTotalMN = 0;
		var hidMontoTotalDL =0;
	
		var  gridTotales = Ext.getCmp('gridTotales');
		var store = gridTotales.getStore();
		
		store.each(function(record) {
		 if(record.data['IC_MONEDA']==1){
			hidNumDoctosActualesMN =record.data['TOTAL'];
			hidMontoTotalMN =record.data['TOTAL_MONTO'];
		 }
		 if(record.data['IC_MONEDA']==54){
			hidNumDoctosActualesDL=  record.data['TOTAL'];
			hidMontoTotalDL = record.data['TOTAL_MONTO'];
		}	
	});
	
	if(!(parseInt(hidNumDoctosActualesMN,10)==parseInt(hidCtrlNumDoctosMN,10)
			&& parseInt(hidNumDoctosActualesDL,10)==parseInt(hidCtrlNumDoctosDL,10)
			&& parseFloat(hidMontoTotalMN)==parseFloat(hidCtrlMontoTotalMN)
			&& parseFloat(hidMontoTotalDL)==parseFloat(hidCtrlMontoTotalDL))){
			
			Ext.MessageBox.alert("Mensaje","Los datos capturados no concuerdan con los de control."	+"\nNumero Documentos M.N.: "+hidNumDoctosActualesMN+ " debe ser "+ hidCtrlNumDoctosMN+	"\nNumero Documentos D�lares: "	+hidNumDoctosActualesDL+ " debe ser "+hidCtrlNumDoctosDL	 
			+"\nMonto Total M.N.: "+  Ext.util.Format.number(hidMontoTotalMN, '0,00.00') + " debe ser "	+ Ext.util.Format.number(hidCtrlMontoTotalMN, '0,00.00') +	"\nMonto Total D�lares.: "+  Ext.util.Format.number(hidMontoTotalDL, '0,00.00') + " debe ser "+ Ext.util.Format.number(hidCtrlMontoTotalDL, '0,00.00') );
			return;
		}		
		var parametros = "pantalla= PreAcuse"+"&proceso="+proceso+"&strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable+
										"&hidCtrlNumDoctosMN="+hidCtrlNumDoctosMN+"&hidCtrlMontoTotalMN="+hidCtrlMontoTotalMN+
										"&hidCtrlNumDoctosDL="+hidCtrlNumDoctosDL+"&hidCtrlMontoTotalDL="+hidCtrlMontoTotalDL;				
				
		document.location.href = "13forma01bExt.jsp?"+parametros;			
	}
		
	//cancelar la captura de documentos
	var procesarSuccessFailureCancelar =  function(opts, success, response) {		
			//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
						
			document.location.href  = "13forma01cExt.jsp?cancelacion=S"+"&strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable;
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//cancelar la captura de documentos
	function procesarCancelar(opts, success, response) {
		if(!confirm("�Est� seguro de querer cancelar la operaci�n?")) {
			return;
		}else {			
			Ext.Ajax.request({
			url : '13forma01Ext.data.jsp',
				params : {
					informacion: 'Cancelar',
					proceso:proceso				
				},
				callback: procesarSuccessFailureCancelar
			});	
		}	
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;			
		var fp = Ext.getCmp('fpCarga');
		fp.el.unmask();
					
		if (arrRegistros != null) {
			if (!gridCaptura.isVisible()) {
				gridCaptura.show();
			}			
			
			hidMontoTotalMN = jsonData.hidMontoTotalMN;
			hidNumDoctosActualesMN = jsonData.hidNumDoctosActualesMN;
			hidMontoTotalDL = jsonData.hidMontoTotalDL;
			hidNumDoctosActualesDL = jsonData.hidNumDoctosActualesDL;
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;	
	
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridCaptura.getColumnModel();			
			var el = gridCaptura.getGridEl();	
			
			if(store.getTotalCount() > 0) {
			
				if(jsonData.operaFVPyme =='S') {
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEEDOR'), false);	
				}
				if(jsonData.bOperaFactorajeVencido =='S' || jsonData.bOperaFactConMandato =='S'  ||  jsonData.bOperaFactorajeVencidoInfonavit =='S' ) {					
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);	
				}				
				if(jsonData.bOperaFactorajeDistribuido  =='S' || jsonData.bOperaFactorajeVencidoInfonavit   =='S') {				
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'), false);	
				}

				if(jsonData.sPubEPOPEFFlagVal  =='S') {
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('FECHA_RECEPCION'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COMPRA'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICADOR'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_MAXIMO'), false);	
				}
				if(jsonData.bOperaFactConMandato =='S') {
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('MANDANTE'), false);	
				}
				
				if(jsonData.bValidaDuplicidad =='S') {
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('DUPLICADO'), false);	
				}
				
				if(hayCamposAdicionales=='0'){
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.nomCampo4);
					gridCaptura.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.nomCampo5);						
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridCaptura.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}			
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}

	var limpiar = function() {
		//esta parte es para limpiar los componentes de la forma cuando se agregan 
		Ext.getCmp("cg_pyme_epo_interno1").setValue('');
		Ext.getCmp("ic_pyme1").setValue('');
		Ext.getCmp("ig_numero_docto1").setValue('');
		Ext.getCmp("df_fecha_docto1").setValue('');
		Ext.getCmp("df_fecha_venc1").setValue('');
		Ext.getCmp("df_fecha_venc_pyme1").setValue('');
		Ext.getCmp("ic_moneda1").setValue('1');
		Ext.getCmp("fn_monto1").setValue('');
		Ext.getCmp("ic_tipo_fact1").setValue('N');		
		Ext.getCmp("ic_estatus_docto1").setValue('S');
		Ext.getCmp("ic_mandante1").setValue('');
		Ext.getCmp("ic_if1").setValue('');
		Ext.getCmp("ct_referencia1").setValue('');
		Ext.getCmp("ic_beneficiario1").setValue('');
		Ext.getCmp("fn_porc_beneficiario1").setValue('');
		Ext.getCmp("fn_monto_beneficiario1").setValue('');
		Ext.getCmp("df_fecha_entrega1").setValue('');
		Ext.getCmp("cg_tipo_compra1").setValue('');
		Ext.getCmp("cg_clave_presupuestaria1").setValue('');
		Ext.getCmp("cg_periodo1").setValue('');
		if(campos=='S'){
			for(var i= 1; i<=noCampos; i++) {
				if(i==1){		Ext.getCmp("cg_campo1").setValue('');	}	
				if(i==2){		Ext.getCmp("cg_campo2").setValue('');	}
				if(i==3){		Ext.getCmp("cg_campo3").setValue('');	}
				if(i==4){		Ext.getCmp("cg_campo4").setValue('');	}
				if(i==5){		Ext.getCmp("cg_campo5").setValue('');	}
			}
		}				
	}
	
	var procesarSuccessFailureCaptura =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var datos = Ext.util.JSON.decode(response.responseText);
			var msgError =datos.msgError;	
			proceso =datos.proceso;	
			
			if(msgError==''){
				
				consultaData.load({  		params: { 	informacion: 'Consulta', 	proceso:proceso, strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, 	pantalla:'Captura'	}  	});	
				
				totalesData.load({  params: { 	informacion: 'ResumenTotales', 		proceso:proceso,	 strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, pantalla:'Captura'	} });
				
				var btnTerminaCap = Ext.getCmp('btnTerminaCap');	
				var btnCancelar = Ext.getCmp('btnCancelar');				
				var ic_mandante = Ext.getCmp("ic_mandante1");
				var ic_if = Ext.getCmp("ic_if1");
				var ic_mandante2 = Ext.getCmp("ic_mandante2");
				var ic_if2 = Ext.getCmp("ic_if2");							
				var ic_beneficiario = Ext.getCmp("ic_beneficiario1");
				var ic_beneficiario2 = Ext.getCmp("ic_beneficiario2");						
				var fn_porc_beneficiario = Ext.getCmp("fn_porc_beneficiario1");
				var fn_monto_beneficiario = Ext.getCmp("fn_monto_beneficiario1");
				var fn_porc_beneficiario02 = Ext.getCmp("fn_porc_beneficiario2");
				var fn_monto_beneficiario02 = Ext.getCmp("fn_monto_beneficiario2");
				var ic_estatus_docto   = Ext.getCmp("ic_estatus_docto1");
				var ic_estatus_docto2   = Ext.getCmp("ic_estatus_docto2");
				ic_mandante.hide();
				ic_if.hide();
				ic_mandante2.hide();
				ic_if2.hide();					
				ic_beneficiario.hide();
				ic_beneficiario2.hide();					 
				fn_monto_beneficiario.hide();		
				fn_porc_beneficiario02.hide();		
				fn_monto_beneficiario02.hide();		
				ic_estatus_docto.show();		
				ic_estatus_docto2.show();	
				ic_estatus_docto.enable();						
				gridTotales.show();					
				fpConfirmar.show();
				btnTerminaCap.show();
				btnCancelar.show();	
								
				//esta parte es para limpiar los componentes de la forma cuando se agregan 
				limpiar();
		
			}else {			
				Ext.MessageBox.alert("Mensaje",msgError);				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarSuccessFailureValidaCaptura =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var datos = Ext.util.JSON.decode(response.responseText);
			var msgError =datos.msgError;	
			proceso =datos.proceso;	
			var fp = Ext.getCmp('fpCarga');	
			
			if(datos.msgError==''){
			
				if( datos.duplicidad =='N'){			
								
					Ext.Ajax.request({
						url : '13forma01Ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'Captura',
							proceso:proceso,
							ic_pyme:datos.ic_pyme,
							spubdocto_venc:spubdocto_venc,
							hidFechaVencMin:hidFechaVencMin,
							strPendiente:strPendiente,
							strPreNegociable:strPreNegociable
						}),
						callback: procesarSuccessFailureCaptura
					});	
			}else  if( datos.duplicidad =='S'){
				
				Ext.MessageBox.confirm('Mensaje','Existen documentos registrados con la misma Fecha de emisi�n, Moneda y Monto, que podr�an estar duplicados para alg�n Proveedor �Desea continuar? ',
					function showResult(btn){
						if (btn == 'yes') {
							Ext.Ajax.request({
								url : '13forma01Ext.data.jsp',
								params: Ext.apply(fp.getForm().getValues(),{
									informacion:'Captura',
									proceso:proceso,	
									ic_pyme:datos.ic_pyme,
									spubdocto_venc:spubdocto_venc,
									hidFechaVencMin:hidFechaVencMin,
									strPendiente:strPendiente,
									strPreNegociable:strPreNegociable,
									duplicado:'S'
								}),
								callback: procesarSuccessFailureCaptura
							});	  
						}else {
							limpiar();						
						}
					});	
				}
							
			}else {			
				Ext.MessageBox.alert("Mensaje",msgError);				
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
		
	var actualizaMontoBenefi = function() {	
	
		var mto_benefi = 0.0;
		var fn_porc_beneficiario =  Ext.getCmp("fn_porc_beneficiario1");
		var fn_monto =  Ext.getCmp("fn_monto1");
		var fn_monto_beneficiario= 0.0;
		
		if (!Ext.isEmpty(fn_porc_beneficiario.getValue()) ){
			if(parseFloat(fn_porc_beneficiario.getValue()) <= 0.0) {
				return false;
			}				
			
			if(parseFloat(fn_porc_beneficiario.getValue()) > 100) {
				Ext.getCmp("fn_porc_beneficiario1").setValue("100");				
			} 
			
			mto_benefi = parseFloat(fn_monto.getValue()) * (parseFloat(fn_porc_beneficiario.getValue()) / 100);
			fn_monto_beneficiario = roundOff(mto_benefi, 2);					
			Ext.getCmp("fn_monto_beneficiario1").setValue(fn_monto_beneficiario);
			return true;
			
		} else {
			Ext.getCmp("fn_monto_beneficiario1").setValue(fn_monto_beneficiario);
			return false;
		}
	}

	var actualizaPorcenBenefi = function() {	
		var porcen_benefi = 0.0;
		var fn_monto_beneficiario =  Ext.getCmp("fn_monto_beneficiario1");
		var fn_monto =  Ext.getCmp("fn_monto1");
		var fn_porc_beneficiario= 0.0;
		
	if (!Ext.isEmpty(fn_monto_beneficiario.getValue()) ){
			var fn_monto_beneficiario2 = roundOff(parseFloat(fn_monto_beneficiario.getValue()), 2);
			Ext.getCmp("fn_monto_beneficiario1").setValue(fn_monto_beneficiario2);
			
			if(parseFloat(fn_monto_beneficiario.getValue()) <= 0.0) {
				return false;
			}
			if(parseFloat(fn_monto_beneficiario.getValue()) > parseFloat(fn_monto.getValue())) {
				return false;
			} else {
				porcen_benefi = (parseFloat(fn_monto_beneficiario.getValue()) / parseFloat(fn_monto.getValue())) * 100;
				fn_porc_beneficiario = roundOff(porcen_benefi, 2);			
				Ext.getCmp("fn_porc_beneficiario1").setValue(fn_porc_beneficiario);				
				return true;
			}
		} else {	
			Ext.getCmp("fn_porc_beneficiario1").setValue(fn_porc_beneficiario);
			return false;
		}
	}
	
	var agregar = function() {
		
		var ic_pyme = Ext.getCmp("ic_pyme1");
		var cg_pyme_epo_interno = Ext.getCmp("cg_pyme_epo_interno1");
		var ig_numero_docto = Ext.getCmp("ig_numero_docto1");
		var df_fecha_docto = Ext.getCmp("df_fecha_docto1");
		var df_fecha_venc = Ext.getCmp("df_fecha_venc1");
		var df_fecha_venc_pyme = Ext.getCmp("df_fecha_venc_pyme1");
		var ic_moneda = Ext.getCmp("ic_moneda1");
		var ic_tipo_fact = Ext.getCmp("ic_tipo_fact1");
		var fn_monto = Ext.getCmp("fn_monto1");
		var ic_tipo_fact = Ext.getCmp("ic_tipo_fact1");
		var ic_if  = Ext.getCmp("ic_if1");
		var df_fecha_entrega  = Ext.getCmp("df_fecha_entrega1");
		var ic_beneficiario = Ext.getCmp("ic_beneficiario1");
		var fn_porc_beneficiario =  Ext.getCmp("fn_porc_beneficiario1");
		var fn_monto_beneficiario  = Ext.getCmp("fn_monto_beneficiario1");
		var cg_tipo_compra  = Ext.getCmp("cg_tipo_compra1");
		var cg_clave_presupuestaria = Ext.getCmp("cg_clave_presupuestaria1");	
		var cg_periodo  = Ext.getCmp("cg_periodo1");				
		var fechaV	=  Ext.util.Format.date( '01/01/2001','d/m/Y');		
	
		
		if(Ext.getCmp('numProvValidHid1').getValue()=='N' || Ext.getCmp('cvePymeValidHid1').getValue()=='N'){
			return;
		}
		
		if (!Ext.isEmpty(df_fecha_docto.getValue()) ){	
			var fecha =  Ext.util.Format.date(df_fecha_docto.getValue(),'d/m/Y'); 			
			if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
				df_fecha_docto.markInvalid('La fecha emisi�n no puede ser menor al a�o 2001');
				return;
			}
		}
		
		if (!Ext.isEmpty(df_fecha_venc.getValue()) ){	
			var fecha =  Ext.util.Format.date(df_fecha_venc.getValue(),'d/m/Y'); 
			if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
				df_fecha_venc.markInvalid('La fecha vencimiento no puede ser menor al a�o 2001');
				return;
			}
		}
		
		if (!Ext.isEmpty(df_fecha_venc_pyme.getValue()) ){	
			var fecha =  Ext.util.Format.date(df_fecha_venc_pyme.getValue(),'d/m/Y'); 
			if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
				df_fecha_venc_pyme.markInvalid('La fecha Vencimiento Proveedor no puede ser menor al a�o 2001');
				return;
			}
		}
		
		if (!Ext.isEmpty(df_fecha_entrega.getValue()) ){	
			var fecha =  Ext.util.Format.date(df_fecha_entrega.getValue(),'d/m/Y'); 
			if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
				df_fecha_entrega.markInvalid('La fecha Vencimiento Proveedor no puede ser menor al a�o 2001');
				return;
			}
		}
		
								
		if (Ext.isEmpty(ic_pyme.getValue()) &&  Ext.isEmpty(cg_pyme_epo_interno.getValue()) ){
			ic_pyme.markInvalid('Por favor elija un proveedor.');
			return;
		}	
		
		if (Ext.isEmpty(ig_numero_docto.getValue()) ){
			ig_numero_docto.markInvalid('No debe dejar el campo en blanco. Por favor capt�relo.');
			return;
		}
		if (Ext.isEmpty(df_fecha_docto.getValue()) ){
			df_fecha_docto.markInvalid('Por favor capture la Fecha.');
			return;
		}
		
		if (Ext.isEmpty(df_fecha_venc.getValue()) ){
			df_fecha_venc.markInvalid('Por favor capture la Fecha.');
			return;
		}
		if(operaFVPyme=='S'){
			if(ic_tipo_fact.getValue()=='N' || ic_tipo_fact.getValue() =='V'){
				if (Ext.isEmpty(df_fecha_venc_pyme.getValue()) ){
					df_fecha_venc_pyme.markInvalid('Debe capturar la Fecha Vencimiento Proveedor.');
					return;
				}
					
				var resultado = datecomp( Ext.util.Format.date(df_fecha_venc.getValue(),'d/m/Y'), Ext.util.Format.date(df_fecha_venc_pyme.getValue(),'d/m/Y') );
				//  0 si son iguales.  		//  1 si la primera es mayor que la segunda	
				//  2 si la segunda mayor que la primera 		// -1 si son fechas invalidas						
				if (resultado==2) {
					df_fecha_venc.markInvalid("La fecha de vencimiento del documento debe ser mayor que la fecha de vencimiento del proveedor");			
					return;
				}		
			}		
		}
				
		if (Ext.isEmpty(ic_moneda.getValue()) ){
			ic_moneda.markInvalid('Por favor elija una Moneda.');
			return;
		}
		
		if (Ext.isEmpty(fn_monto.getValue()) ){		
				fn_monto.markInvalid("El monto debe ser mayor a cero");
				return;	
		}
				
		if (!Ext.isEmpty(fn_monto.getValue()) ){
			if(parseFloat(fn_monto.getValue())<=0){
				fn_monto.markInvalid("El monto debe ser mayor a cero");
				return;
			} 
		}
		
		//validacion del Monto
		if (ic_moneda.getValue() ==1 ){
			if (parseFloat(fn_monto.getValue(),10) > parseFloat(hidCtrlMontoMaxMN,10) 	|| parseFloat(fn_monto.getValue(),10) < parseFloat(hidCtrlMontoMinMN,10))  	{
				fn_monto.markInvalid('Verifique el monto \n Cifras de Control del Detalle \n Monto m�ximo de los documentos' +  Ext.util.Format.number(hidCtrlMontoMaxMN, '0,00.00') +' \n Monto m�nimo de los documentos '+ Ext.util.Format.number(hidCtrlMontoMinMN, '0,00.00'));
					return;
			}
			if(roundOff(parseFloat(hidMontoTotalMN,10)+parseFloat(fn_monto.getValue(),10),2)>parseFloat(hidCtrlMontoTotalMN,10))	{
				fn_monto.markInvalid('Monto total excedido \n Cifras de Control del Detalle  \n Monto Total M.N.: ' +  Ext.util.Format.number(hidCtrlMontoTotalMN, '0,00.00')  );				
				return;
			}
			if(parseInt(hidNumDoctosActualesMN,10)+1 > parseInt(hidCtrlNumDoctosMN,10))	{
					fn_monto.markInvalid('Cantidad de Documentos en M.N. Excedida  \n Cifras de Control del Detalle  \n Numero de Documentos M.N.:'+hidCtrlNumDoctosMN);				
					return;
			}
			
		}	else  if (ic_moneda.getValue() ==54 ) {   //moneda = 54  D�lares
					
			if (parseFloat(fn_monto.getValue(),10) > parseFloat(hidCtrlMontoMaxDL,10) || parseFloat(fn_monto.getValue(),10) < parseFloat(hidCtrlMontoMinDL,10)) 	{
				fn_monto.markInvalid(' Verifique el monto  \n Cifras de Control del Detalle \n  Monto M�ximo D�lares: '+ Ext.util.Format.number(hidCtrlMontoMaxDL, '0,00.00') +'\n El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (D�lares)'+Ext.util.Format.number(hidCtrlMontoMinDL, '0,00.00') );				
				return;
			}

			if(roundOff(parseFloat(hidMontoTotalDL,10)+parseFloat(fn_monto,10),2)>parseFloat(hidCtrlMontoTotalDL,10))  	{
				fn_monto.markInvalid(' Monto total excedido \n Cifras de Control del Detalle \n Monto Total D�lares.: ' +  Ext.util.Format.number(hidCtrlMontoTotalDL, '0,00.00')  );				
				return;
			}

			if(parseInt(hidNumDoctosActualesDL,10)+1>parseInt(hidCtrlNumDoctosDL,10)) 	{
				fn_monto.markInvalid('Cantidad de Documentos en D�lares Excedida  \n Cifras de Control del Detalle \n  Numero de Documentos D�lares: '+hidCtrlNumDoctosDL);				
				return;
			}
		}			
		
		if (Ext.isEmpty(ic_tipo_fact.getValue()) ){
			ic_tipo_fact.markInvalid('Por favor elija un Tipo Factoraje.');
			return;
		}	
				
		if(  ( bOperaFactorajeVencido =='S'  && ic_tipo_fact.getValue() =='V' )  
			|| ( bOperaFactorajeVencidoInfonavit =='S'  && ic_tipo_fact.getValue() =='I' )  ) {
			if (Ext.isEmpty(ic_if.getValue()) ){	
				ic_if.markInvalid('Por favor elija un IF.');
				return;
			}
		}	
		
		if(   bOperaFactorajeVencido =='S'  && ic_tipo_fact.getValue() =='V' )   {
			if ( Ext.getCmp('operaFactVencidoIF_EPO').getValue() =='N' ){	
				Ext.getCmp('ic_if1').markInvalid('El Intermediario financiero no opera Factoraje al Vencimiento, favor de  seleccionar otro');
				return;
			}
		}
			
		if( bOperaFactorajeDistribuido =='S'  &&  ic_tipo_fact.getValue() =='D' ){
			if (Ext.isEmpty(ic_beneficiario.getValue()) ){	
				ic_beneficiario.markInvalid('Por favor elija un Beneficiario.');
			}	
			if ( Ext.isEmpty(fn_porc_beneficiario.getValue())  &&   Ext.isEmpty(fn_monto_beneficiario.getValue())    ){
				fn_porc_beneficiario.markInvalid('Por favor introduzca un porcentaje o monto del beneficiario.');						
			}	
			
			
			if (parseFloat(fn_porc_beneficiario.getValue()) <=0){
				fn_porc_beneficiario.markInvalid('El Porcentaje del Beneficiario no puede ser Menor o Igual a Cero.');
			}
			if (parseFloat(fn_porc_beneficiario.getValue()) >100){
				fn_porc_beneficiario.markInvalid('El Porcentaje del Beneficiario no puede ser mayor al 100%');
			}
			
			if(!actualizaMontoBenefi()) {
				fn_porc_beneficiario.markInvalid('El Porcentaje del Beneficiario no es un Valor V�lido.');					
				return;
			}
								
			if (parseFloat(fn_monto_beneficiario.getValue()) <=0){
				fn_monto_beneficiario.markInvalid('El Monto del Beneficiario no puede ser mayor al Monto del Documento');
			}
			if (parseFloat(fn_monto_beneficiario.getValue())  >  parseFloat(fn_monto.getValue())  ){
				fn_monto_beneficiario.markInvalid('El Monto del Beneficiario no es un Monto V�lido');
			}
			
			if(!actualizaPorcenBenefi()) {
					fn_monto_beneficiario.markInvalid('El Monto del Beneficiario no es un Monto V�lido');				
					return;
				}
		}	
			
		if( bOperaFactorajeVencidoInfonavit =='S'  &&  ic_tipo_fact.getValue() =='I' ){
			if (Ext.isEmpty(ic_beneficiario.getValue()) ){	
				ic_beneficiario.markInvalid('Por favor elija un Beneficiario.');
				return;
			}	
			if ( Ext.isEmpty(fn_porc_beneficiario.getValue())  &&   Ext.isEmpty(fn_monto_beneficiario.getValue())    ){
				fn_porc_beneficiario.markInvalid('Por favor introduzca un porcentaje o monto del beneficiario.');						
				return;
			}	
			
			
			if (parseFloat(fn_porc_beneficiario.getValue()) <=0){
				fn_porc_beneficiario.markInvalid('El Porcentaje del Beneficiario no puede ser Menor o Igual a Cero.');
				return;
			}
			if (parseFloat(fn_porc_beneficiario.getValue()) >100){
				fn_porc_beneficiario.markInvalid('El Porcentaje del Beneficiario no puede ser mayor al 100%');
				return;
			}
			
			if(!actualizaMontoBenefi()) {
				fn_porc_beneficiario.markInvalid('El Porcentaje del Beneficiario no es un Valor V�lido.');					
				return;
			}
					
			
			if (parseFloat(fn_monto_beneficiario.getValue()) <=0){
				fn_monto_beneficiario.markInvalid('El Monto del Beneficiario no puede ser mayor al Monto del Documento');
				return;
			}
			if (parseFloat(fn_monto_beneficiario.getValue())  >  parseFloat(fn_monto.getValue())  ){
				fn_monto_beneficiario.markInvalid('El Monto del Beneficiario no es un Monto V�lido');
				return;
			}
			
			if(!actualizaPorcenBenefi()) {
					fn_monto_beneficiario.markInvalid('El Monto del Beneficiario no es un Monto V�lido');				
					return;
				}
		}
		
		//validaciones de los campos adicionales
			
		if(sPubEPOPEFFlagVal =='S'  &&  ic_tipo_fact.getValue() !='I'){ 
			if(sPubEPOPEFDocto1Val =='S'){
				if (Ext.isEmpty(df_fecha_entrega.getValue()) ){
					df_fecha_entrega.markInvalid('Debe capturar la Fecha de Entrega');
					return;
				}	
			}
			if(sPubEPOPEFDocto2Val =='S'){
				if (Ext.isEmpty(cg_tipo_compra.getValue()) ){
					cg_tipo_compra.markInvalid('Debe capturar el Tipo de Compra');		
					return;
				}
			}
			if(sPubEPOPEFDocto3Val =='S'){
				if (Ext.isEmpty(cg_clave_presupuestaria.getValue()) ){
					cg_clave_presupuestaria.markInvalid('Debe capturar la Clave Presupuestaria');			
					return;
				}
			}
			if(sPubEPOPEFDocto4Val =='S'){
				if (Ext.isEmpty(cg_periodo.getValue()) ){
					cg_periodo.markInvalid('Debe capturar el periodo');					
					return;
				}
			}
					
			if (Ext.isEmpty(df_fecha_entrega.getValue()) ){
					df_fecha_entrega.markInvalid('Debe capturar la Fecha de Entrega');
					return;
			}	
		}	
					
		if (ic_tipo_fact.getValue()   !='C' ){
			var resultado = datecomp( Ext.util.Format.date(df_fecha_docto.getValue(),'d/m/Y') , Ext.util.Format.date(df_fecha_venc.getValue(),'d/m/Y') );
			//  0 si son iguales.
			//  1 si la primera es mayor que la segunda
			//  2 si la segunda mayor que la primera
			// -1 si son fechas invalidas
			if (resultado==1) {
				df_fecha_venc.markInvalid('La fecha de vencimiento debe ser posterior a la de emisi�n');				
				return;
			}
			
			/* Agregada la fecha de vencimiento no puede ser anterior al d�a de hoy*/
			var result = datecomp(hidFechaActual,Ext.util.Format.date(df_fecha_venc.getValue(),'d/m/Y'));
			if (result==1) {
				df_fecha_venc.markInvalid('La fecha de vencimiento no puede ser anterior al dia de hoy ');			
				return;
			}	
			var ic_estatus_docto   = Ext.getCmp("ic_estatus_docto01");	//para los negociables		
			
					
			if(ic_estatus_docto.getValue()==true) {
				if(spubdocto_venc != "S" ){
					var resultado = datecomp( Ext.util.Format.date(df_fecha_venc.getValue(),'d/m/Y'),hidFechaVencMin);
				
					if (resultado==2) {
						df_fecha_venc.markInvalid("La fecha de vencimiento no puede ser anterior al  "+hidFechaVencMin);						
						return;
					}
				}
			}else if(spubdocto_venc != "S"){
				var resultado = datecomp(Ext.util.Format.date(df_fecha_venc.getValue(),'d/m/Y'),hidFechaVencMin);
				if (resultado==2) {
				df_fecha_venc.markInvalid("La fecha de vencimiento no puede ser anterior al "+hidFechaVencMin);							
				return;
				}
			}	
			
			var resultado = datecomp(hidFechaVencMax,Ext.util.Format.date(df_fecha_venc.getValue(),'d/m/Y'));			
			if (resultado==2) {
				df_fecha_venc.markInvalid('La fecha de vencimiento no puede ser posterior al '+hidFechaVencMax);				
				return;
			}			
		}//if(f.ic_tipo_fact!="C")

		var resultado = datecomp(hidFechaActual, Ext.util.Format.date(df_fecha_docto.getValue(),'d/m/Y') );
		if (resultado==2) {
			df_fecha_docto.markInvalid('La fecha de emisi�n no puede ser posterior a la fecha actual');		
			return;
		}
	
		if(operaFVPyme=='S') {
			var resultado = datecomp(hidFechaActual, hidFechaPlazoPagoFVP);
			if (resultado==2) {
				var diasMaxFVP = 0;
				if (datecomp(hidFechaPlazoPagoFVP,  Ext.util.Format.date(df_fecha_venc_pyme.getValue(),'d/m/Y'))==2) {
					diasMaxFVP = hidPlazoMax2FVP;
				}else{
					diasMaxFVP = hidPlazoMax1FVP;
				}
				var fechaVP =  Ext.util.Format.date(df_fecha_venc_pyme.getValue(),'d/m/Y'); 
				var arFechaVP = fechaVP.split("/");
				var TDate = new Date(arFechaVP[2], arFechaVP[1]-1, arFechaVP[0]);
				var diaMes = TDate.getDate();
				var AddDays = diasMaxFVP;
				diaMes = parseInt(diaMes, 10)+ parseInt(AddDays, 10);
				TDate.setDate(diaMes);
				var CurYear = TDate.getFullYear();
				var CurDay = TDate.getDate();
				var CurMonth = TDate.getMonth()+1;
				if(CurDay<10)
					CurDay = '0'+CurDay;
				if(CurMonth<10)
					CurMonth = '0'+CurMonth;
				if (datecomp(CurDay +'/'+CurMonth+'/'+CurYear,Ext.util.Format.date(df_fecha_venc.getValue(),'d/m/Y') )==2) {
					df_fecha_venc.markInvalid('La fecha de vencimiento del documento sobrepasa el parametro maximo entre Fecha Vencimiento Proveedor y Fecha Vencimiento');
					return;
				}
			}
		}	
		
		//valida campos Adicionales 
		if(campos=='S'){
			for(var i= 1; i<=noCampos; i++) {
				if(i==1){		
					var cg_campo1 = 	Ext.getCmp("cg_campo1").getValue();	
					if (cg_campo1==''  && cg_campo01 !='' ){
						Ext.MessageBox.alert("Mensaje","Debe capturar el campo "+cg_campo01);	
						return;
					}	
				}
				if(i==2){						
					var cg_campo2 = 	Ext.getCmp("cg_campo2").getValue();	
					if (cg_campo2=='' && cg_campo02 !='' ){
						Ext.MessageBox.alert("Mensaje","Debe capturar el campo "+cg_campo02);		
						return;
					}
				}
				if(i==3){						
					var cg_campo3 = 	Ext.getCmp("cg_campo3").getValue();	
					if (cg_campo3=='' && cg_campo03 !='' ){
						Ext.MessageBox.alert("Mensaje","Debe capturar el campo "+cg_campo03);		
						return;
					}
				}
				if(i==4){						
					var cg_campo4 = 	Ext.getCmp("cg_campo4").getValue();	
					if (cg_campo4=='' && cg_campo04 !='' ){
						Ext.MessageBox.alert("Mensaje","Debe capturar el campo "+cg_campo04);		
						return;
					}
				}
				if(i==5){						
					var cg_campo5 = 	Ext.getCmp("cg_campo5").getValue();	
					if (cg_campo5=='' && cg_campo05 !='' ){
						Ext.MessageBox.alert("Mensaje","Debe capturar el campo "+cg_campo05);		
						return;
					}
				}			
			}//for
		}
	
		var fp = Ext.getCmp('fpCarga');				
		Ext.Ajax.request({
			url : '13forma01Ext.data.jsp',
				params: Ext.apply(fp.getForm().getValues(),{
					informacion:'Valida_Captura',
					proceso:proceso,	
					spubdocto_venc:spubdocto_venc,
					hidFechaVencMin:hidFechaVencMin,
					strPendiente:strPendiente,
					strPreNegociable:strPreNegociable
				}),
				callback: procesarSuccessFailureValidaCaptura
			});	
					
	}
	
	var catalogoBeneficiarioData = new Ext.data.JsonStore({
		id: 'catalogoBeneficiarioDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBeneficiario'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var catalogoMandanteData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMandante'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var catalogoTipoCompraData = new Ext.data.JsonStore({
		id: 'catalogoFactorajeDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoTipoCompra'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var procesarCatalogoFactorajedaData= function(store, records, oprion){
   if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
    var ic_tipo_fact1 = Ext.getCmp('ic_tipo_fact1');
	 if(ic_tipo_fact1.getValue()==''){
	  ic_tipo_fact1.setValue('N');
	 }
   }
  }
	
	var catalogoFactorajedaData = new Ext.data.JsonStore({
		id: 'catalogoFactorajeDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoFactoraje'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatalogoFactorajedaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var catalogoPymeData = new Ext.data.JsonStore({
		id: 'catalogoPYMEDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoPyme'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	 var procesarCatalogoMonedaData= function(store, records, oprion){
   if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
    var ic_moneda1 = Ext.getCmp('ic_moneda1');
	 if(ic_moneda1.getValue()==''){
	  ic_moneda1.setValue(records[0].data['clave']);
	 }
   }
  }
	
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			load: procesarCatalogoMonedaData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
		
	
		//para los totales del grid Nomal 
	var totalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'	
		},								
		fields: [			
			{name: 'MONEDA' , mapping: 'MONEDA'},
			{name: 'IC_MONEDA' , mapping: 'IC_MONEDA'},
			{name: 'TOTAL', type: 'float', mapping: 'TOTAL'},
			{name: 'TOTAL_MONTO' , type: 'float', mapping: 'TOTAL_MONTO' },
			{name: 'TOTAL_MONTO_DESC',type: 'float',   mapping: 'TOTAL_MONTO_DESC'}				
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		id: 'gridTotales',
		store: totalesData,
		margins: '20 0 0 0',
		align: 'center',
		title: '',	
		hidden: true,
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 150,
				align: 'left'				
			},
			{
				header: 'Total de Documentos ',
				dataIndex: 'TOTAL',
				width: 150,
				align: 'right'				
			},
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: 'Total Monto Descuento',
				dataIndex: 'TOTAL_MONTO_DESC',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') 
			},
			{
				header: '',
				dataIndex: '',
				width: 334,
				align: 'right'			
			}		
			],
			height: 100,
			width: 943,
			style: 'margin:0 auto;',
			frame: false
	});
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [						
			{name: 'NOMBRE_PROVEEDOR'},	
			{name: 'NUM_DOCUMENTO'},	
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'FECHA_VENC_PROVEEDOR'},	
			{name: 'IC_MONEDA'},	
			{name: 'MONEDA'},	
			{name: 'TIPO_FACTORAJE'},	
			{name: 'MONTO'},	
			{name: 'PORC_DESCUENTO'},	
			{name: 'MONTO_DESCONTAR'},	
			{name: 'ESTATUS'},	
			{name: 'REFERENCIA'},	
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_BENEFICIARIO'},	
			{name: 'PORC_BENEFICIARIO'},	
			{name: 'MONTO_BENEFICIARIO'},	
			{name: 'CAMPO1'},	
			{name: 'CAMPO2'},	
			{name: 'CAMPO3'},	
			{name: 'CAMPO4'},	
			{name: 'CAMPO5'},	
			{name: 'FECHA_RECEPCION'},	
			{name: 'TIPO_COMPRA'},	
			{name: 'CLASIFICADOR'},	
			{name: 'PLAZO_MAXIMO'},	
			{name: 'MANDANTE'},
			{name: 'CONSECUTIVO'},
			{name: 'IC_PYME'},
			{name: 'IC_PROCESO'},
			{name: 'DUPLICADO'}	
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});

	var gridCaptura = new Ext.grid.EditorGridPanel({
		id: 'gridCaptura',
		title: 'Carga de Documentos',
		hidden: true,
		store: consultaData,
		clicksToEdit: 1,				
		columns: [	
			{							
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUM_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex : 'FECHA_EMISION',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento Proveedor',
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex : 'FECHA_VENC_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				hidden: true
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORC_DESCUENTO',
				width : 150,
				sortable : true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')			
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCONTAR',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},
			{							
				header : 'Estatus',
				tooltip: 'Estatus',
				dataIndex : 'ESTATUS',
				width : 150,
				sortable : true,
				align:'center'							
			},
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Nombre Beneficiario',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'NOMBRE_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Porcentaje Beneficiario',
				tooltip: 'Porcentaje Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Monto Beneficiario',
				tooltip: 'Monto Beneficiario',
				dataIndex : 'MONTO_BENEFICIARIO',
				hidden: true,
				width : 150,
				sortable : true,
				align:'left'							
			},
			{
				xtype: 'actioncolumn',
				header: 'Eliminar',
				tooltip: 'Eliminar',
				dataIndex : 'ELIMINAR',
        width: 130,
				align: 'center',					
				items: [
					{
						getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
							this.items[0].tooltip = 'Eliminar';
							return 'borrar';										
						}
						,	handler: procesaAccionBorrar
					}
				]				
			}	,
			{
				xtype: 'actioncolumn',
					header: 'Modificar',
					tooltip: 'Modificar',
					dataIndex : 'MODIFICAR',
					width: 130,
					align: 'center',					
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								this.items[0].tooltip = 'Modificar';
								return 'modificar';										
							}
							,	handler: procesaAccionModificar
						}
					]				
			},
			{							
				header : 'Campo1',
				tooltip: 'Campo1',
				dataIndex : 'CAMPO1',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo2',
				tooltip: 'Campo2',
				dataIndex : 'CAMPO2',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Campo3',
				tooltip: 'Campo3',
				dataIndex : 'CAMPO3',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo4',
				tooltip: 'Campo4',
				dataIndex : 'CAMPO4',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo5',
				tooltip: 'Campo5',
				dataIndex : 'CAMPO5',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Fecha de Recepci�n de Bienes y Servicios',
				tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
				dataIndex : 'FECHA_RECEPCION',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Tipo de Compra (procedimiento)',
				tooltip: 'Tipo de Compra (procedimiento)',
				dataIndex : 'TIPO_COMPRA',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Clasificador por Objeto del Gasto',
				tooltip: 'Clasificador por Objeto del Gasto',
				dataIndex : 'CLASIFICADOR',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Plazo M�ximo ',
				tooltip: 'Plazo M�ximo ',
				dataIndex : 'PLAZO_MAXIMO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},
			{
				xtype: 'actioncolumn',
					header: 'Detalles',
					tooltip: 'Detalles',
					dataIndex : 'DETALLES',
					width: 130,
					align: 'center',					
					items: [
						{
							getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {
								this.items[0].tooltip = 'Modificar';
								return 'modificar';										
							},
							handler: procesaCapturaDetalle
						}
					]				
			}	,
			{							
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'MANDANTE',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},			
			{							
				header : 'Notificado como <br>posible Duplicado',
				tooltip: 'Notificado como posible Duplicado',
				dataIndex : 'DUPLICADO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 943,
		style: 'margin:0 auto;',
		frame: false
	});
	
	var elementosFormaCarga = [	
		{
			xtype: 'compositefield',
			fieldLabel: ' * Campos Obligatorios',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [		
				{
					xtype: 'displayfield',
					value: '',
					id: 'tituloC', 
					width: 80
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: ' Clave del Proveedor',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{	
					xtype: 'hidden',
					name: 'numProvValidHid',
					id: 'numProvValidHid1'
				},
				{
					xtype: 'textfield',
					name: 'cg_pyme_epo_interno',
					id: 'cg_pyme_epo_interno1',
					fieldLabel: 'Clave del Proveedor',
					allowBlank: true,
					hidden: false,
					maxLength: 25,	
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners: {
						blur: {
							fn: function(objTxt) {
								Ext.getCmp('numProvValidHid1').setValue('S');
								if(objTxt.getValue()!='') {
									Ext.Ajax.request({
										url: '13forma01Ext.data.jsp',
										params: {
											informacion: "validaPymeBloqXNumProv",
											cg_pyme_epo_interno: objTxt.getValue()
										},
										callback: procesaPymeBloqXNumProv
									});
								}							
							}
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* Nombre Proveedor',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{	
					xtype: 'hidden',
					name: 'cvePymeValidHid',
					id: 'cvePymeValidHid1'
				},
				{
					xtype: 'combo',
					name: 'ic_pyme',
					id: 'ic_pyme1',
					fieldLabel: '* Nombre Proveedor',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_pyme',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',
					store : catalogoPymeData,
					tpl : NE.util.templateMensajeCargaCombo,
					width: 400,
					listeners: {
						change: {
							fn: function(combo, newVal, oldVal) {	
								Ext.getCmp('cvePymeValidHid1').setValue('S');
								if(newVal!='') {
									Ext.getCmp("cg_pyme_epo_interno1").setValue("");
									Ext.getCmp('numProvValidHid1').setValue('S');
									
									Ext.Ajax.request({
										url: '13forma01Ext.data.jsp',
										params: {
											informacion: "validaPymeBloqueada",
											ic_pyme: newVal
										},
										callback: procesaPymeBloqueada
									});
								}							
							}
						}
					}
				}	
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* N�mero de Documento',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [			
				{		
					xtype: 'textfield',
					name: 'ig_numero_docto',
					id: 'ig_numero_docto1',
					fieldLabel: 'N�mero de Documento',
					allowBlank: true,
					hidden: false,
					maxLength: 15,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* Fecha Emisi�n',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [			
				{
					xtype: 'datefield',
					name: 'df_fecha_docto',
					id: 'df_fecha_docto1',
					fieldLabel: ' * ',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha',					
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 																										
							if(field.getValue() != '') {
								var fecha	=  Ext.util.Format.date(field.getValue(),'d/m/Y'); 
								var fechaV	=  Ext.util.Format.date( '01/01/2001','d/m/Y');		
								var df_fecha_docto1 = Ext.getCmp("df_fecha_docto1");
								if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
									df_fecha_docto1.markInvalid('La fecha emisi�n no puede ser menor al a�o 2001');
									return;
								}								
							}
						}
					}						
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* Fecha Vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'datefield',
					name: 'df_fecha_venc',
					id: 'df_fecha_venc1',
					fieldLabel: ' * Fecha Vencimiento',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha',					
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 																										
							if(field.getValue() != '') {
								var fecha	=  Ext.util.Format.date(field.getValue(),'d/m/Y'); 
								var fechaV	=  Ext.util.Format.date( '01/01/2001','d/m/Y');		
								var df_fecha_venc1 = Ext.getCmp("df_fecha_venc1");
								if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
									df_fecha_venc1.markInvalid('La fecha vencimiento no puede ser menor al a�o 2001');
									return;
								}								
							}
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* Fecha Vencimiento Proveedor',
			id: 'df_fecha_venc_pyme2',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_pyme',
					id: 'df_fecha_venc_pyme1',
					fieldLabel: '* Fecha Vencimiento Proveedor',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha',					
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 																										
							if(field.getValue() != '') {
								var fecha	=  Ext.util.Format.date(field.getValue(),'d/m/Y'); 
								var fechaV	=  Ext.util.Format.date( '01/01/2001','d/m/Y');		
								var df_fecha_venc_pyme1 = Ext.getCmp("df_fecha_venc_pyme1");
								if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
									df_fecha_venc_pyme1.markInvalid('La fecha vencimiento proveedor no puede ser menor al a�o 2001');
									return;
								}								
							}
						}
					}
				}
			]
		},
		
		{
			xtype: 'compositefield',
			fieldLabel: '* Moneda',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					fieldLabel: '* Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',					
					store : catalogoMonedaData,
					tpl : NE.util.templateMensajeCargaCombo,
					width: 200				
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* Monto',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [	
				{
					xtype: 'numberfield',
					name: 'fn_monto',
					id: 'fn_monto1',
					fieldLabel: ' * Monto',
					allowBlank: true,
					hidden: false,
					maxLength: 25,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'					
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* Tipo Factoraje',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'combo',
					name: 'ic_tipo_fact',
					id: 'ic_tipo_fact1',
					fieldLabel: '* Tipo Factoraje',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_tipo_fact',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					//value:'NORMAL',
					allowBlank: true,
					msgTarget: 'side',
					store : catalogoFactorajedaData,
					tpl : NE.util.templateMensajeCargaCombo,
					width: 200,
					listeners: {
						select: {
							fn: function(combo) {	
							var ic_mandante = Ext.getCmp("ic_mandante1");
							var ic_if = Ext.getCmp("ic_if1");
							var ic_mandante2 = Ext.getCmp("ic_mandante2");
							var ic_if2 = Ext.getCmp("ic_if2");
							
							var ic_beneficiario = Ext.getCmp("ic_beneficiario1");
							var ic_beneficiario2 = Ext.getCmp("ic_beneficiario2");						
							var fn_porc_beneficiario = Ext.getCmp("fn_porc_beneficiario1");
							var fn_monto_beneficiario = Ext.getCmp("fn_monto_beneficiario1");
							var fn_porc_beneficiario02 = Ext.getCmp("fn_porc_beneficiario2");
							var fn_monto_beneficiario02 = Ext.getCmp("fn_monto_beneficiario2");
							var ic_estatus_docto   = Ext.getCmp("ic_estatus_docto1");
							var ic_estatus_docto2   = Ext.getCmp("ic_estatus_docto2");
						
							if(combo.getValue()=='M' && bOperaFactConMandato =='S') {
								catalogoMandanteData.load();
								ic_mandante.show();
								ic_mandante2.show();
							}else {
								ic_mandante.hide();
								ic_mandante2.hide();
							}
							if( ( bOperaFactorajeVencido =='S' && combo.getValue()=='V')  
								||   (bOperaFactConMandato =='S'  &&  combo.getValue()=='M') 
								||   (bOperaFactorajeVencidoInfonavit =='S'  &&  combo.getValue()=='I')) {
								catalogoIFData.load();
								ic_if.show();
								ic_if2.show();
							}else {
								ic_if.hide();
								ic_if2.hide();
							}	
							if(  ( bOperaFactorajeDistribuido =='S' && combo.getValue()=='D' )  
								|| ( bOperaFactorajeVencidoInfonavit &&combo.getValue()=='I') ) {
								catalogoBeneficiarioData.load();
								ic_beneficiario.show();
								ic_beneficiario2.show();
								fn_porc_beneficiario.show();
								fn_monto_beneficiario.show();	
								fn_porc_beneficiario02.show();
								fn_monto_beneficiario02.show()
							}else {
								ic_beneficiario.hide();
								ic_beneficiario2.hide();
								fn_porc_beneficiario.hide();
								fn_monto_beneficiario.hide();		
								fn_porc_beneficiario02.hide();
								fn_monto_beneficiario02.hide()
							}		
							//para mostrar los radio de Negociable
							if(combo.getValue()=='I'  || combo.getValue()=='V'  || combo.getValue()=='D'){
								ic_estatus_docto.hide();
								ic_estatus_docto2.hide();
							}else if(combo.getValue()=='N'  || combo.getValue()=='M'  ){
								ic_estatus_docto.show();
								ic_estatus_docto.enable();
								ic_estatus_docto2.show();	
							}else if( combo.getValue()=='C' ){
								ic_estatus_docto.disable();	
								ic_estatus_docto.show();
								ic_estatus_docto2.show();										
							}	
							}
						}	
					}
				},
				{
					xtype: 'displayfield',
					value: ' Negociable',
					id: 'ic_estatus_docto2', 
					width: 80
				},
				{  
					xtype:  'radiogroup',   
					fieldLabel: "Negociable",    
					name: 'ic_estatus_docto',   
					id: 'ic_estatus_docto1',  
					value: 'S',  	
					width: 100,
					columns: 2,			
					items:         
						[         
							{ 
								boxLabel:    "SI",             
								name:        'ic_estatus_docto', 
								id: 'ic_estatus_docto01',
								inputValue:  "S" ,
								value: 'S',
								checked: true,
								width: 20
							},         
							{           
								boxLabel: "NO",             
								name: 'ic_estatus_docto', 
								id: 'ic_estatus_docto02',
								inputValue:  "N", 
								value: 'N',
								width: 20	
							}   
						],   
					style: {      
						paddingLeft: '10px'   
					}		
				}				
			]	
		},	
		{
			xtype: 'compositefield',
			fieldLabel: ' Mandante',
			combineErrors: false,
			msgTarget: 'side',
			id: 'ic_mandante2',
			hidden: true,
			width: 500,
			items: [			
				{
					xtype: 'combo',
					name: 'ic_mandante',
					id: 'ic_mandante1',
					fieldLabel: 'Mandante',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_mandante',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					hidden: true,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',
					store : catalogoMandanteData,
					tpl : NE.util.templateMensajeCargaCombo,
					width: 300				
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: ' Nombre IF ',
			combineErrors: false,
			msgTarget: 'side',
			id: 'ic_if2',
			hidden: true,
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'ic_if',
					id: 'ic_if1',
					fieldLabel: 'Nombre IF',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_if',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					hidden: true,
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',
					store : catalogoIFData,
					tpl : NE.util.templateMensajeCargaCombo,
					width: 300,	
					msgTarget	: 'side',
					margins		: '0 20 0 0',
					listeners: {
						select: {
							fn: function(combo) {	
							
								Ext.Ajax.request({
									url: '13forma01Ext.data.jsp',
									params: {
										informacion: "ValidaFactVencidoIF_EPO",
										ic_if:combo.getValue()
									},
									callback: procesaValidaFactVencidoIF_EPO
								});					
							
							}
						}
					}					
					
				}
			]
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'operaFactVencidoIF_EPO', 	value: '' },
		{
			xtype: 'compositefield',
			fieldLabel: 'Referencia (opcional) 260 caract.max.',
			combineErrors: false,
			msgTarget: 'side',
			id: 'ct_referencia2',
			hidden: false,
			width: 500,
			items: [
				{
					xtype: 'textarea',
					name: 'ct_referencia',
					id: 'ct_referencia1',
					fieldLabel: 'Referencia (opcional) 260 caract.max.',
					allowBlank: true,
					hidden: false,
					maxLength: 260,	
					width: 300,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Beneficiario',
			combineErrors: false,
			msgTarget: 'side',
			id: 'ic_beneficiario2',
			hidden: true,
			width: 500,
			items: [		
				{
					xtype: 'combo',
					name: 'ic_beneficiario',
					id: 'ic_beneficiario1',
					fieldLabel: 'Beneficiario',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_beneficiario',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					hidden: true,
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					msgTarget: 'side',
					store : catalogoBeneficiarioData,
					tpl : NE.util.templateMensajeCargaCombo,
					width: 300				
				}
			]	
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Porcentaje Beneficiario',
			combineErrors: false,
			msgTarget: 'side',
			id: 'fn_porc_beneficiario2',
			hidden: true,
			width: 500,
			items: [		
				{
					xtype: 'numberfield',
					name: 'fn_porc_beneficiario',
					id: 'fn_porc_beneficiario1',
					fieldLabel: 'Porcentaje Beneficiario',
					allowBlank: true,
					hidden: true,
					maxLength: 25,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 																									
							if(field.getValue() != '') {
								actualizaMontoBenefi();
							}
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Monto Beneficiario',
			combineErrors: false,
			id: 'fn_monto_beneficiario2',
			msgTarget: 'side',
			hidden: true,
			width: 500,
			items: [		
				{
					xtype: 'numberfield',
					name: 'fn_monto_beneficiario',
					id: 'fn_monto_beneficiario1',
					fieldLabel: 'Monto Beneficiario',
					allowBlank: true,
					hidden: true,
					maxLength: 25,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 																									
							if(field.getValue() != '') {
								actualizaPorcenBenefi();
							}							
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* Fecha de Recepci�n de Bienes y Servicios',
			combineErrors: false,
			msgTarget: 'side',
			id: 'df_fecha_entrega2',
			width: 500,
			items: [	
				{
					xtype: 'datefield',
					name: 'df_fecha_entrega',
					id: 'df_fecha_entrega1',
					fieldLabel: ' *  Fecha de Recepci�n de Bienes y Servicios',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha',					
					margins: '0 20 0 0',
					listeners:{
						blur: function(field){ 																										
							if(field.getValue() != '') {
								var fecha	=  Ext.util.Format.date(field.getValue(),'d/m/Y'); 
								var fechaV	=  Ext.util.Format.date( '01/01/2001','d/m/Y');		
								var df_fecha_entrega1 = Ext.getCmp("df_fecha_entrega1");
								if(datecomp(fechaV,fecha) == 1 ||  datecomp(fechaV,fecha) == -1){
									df_fecha_entrega1.markInvalid('La fecha recepci�n de bienes y servicios proveedor no puede ser menor al a�o 2001');
									return;
								}								
							}
						}
					}
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* Tipo de Compra (procedimiento)',
			combineErrors: false,
			msgTarget: 'side',
			id: 'cg_tipo_compra2',
			width: 500,
			items: [	
				{
					xtype: 'combo',
					name: 'cg_tipo_compra',
					id: 'cg_tipo_compra1',
					fieldLabel: '* Tipo de Compra (procedimiento)',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'cg_tipo_compra',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					allowBlank: true,
					store : catalogoTipoCompraData,
					tpl : NE.util.templateMensajeCargaCombo,
					width: 400				
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '* Clasificador por Objeto del Gasto',
			combineErrors: false,
			msgTarget: 'side',
			id: 'cg_clave_presupuestaria2',
			width: 500,
			items: [	
				{
					xtype: 'textfield',
					name: 'cg_clave_presupuestaria',
					id: 'cg_clave_presupuestaria1',
					fieldLabel: '*  Clasificador por Objeto del Gasto ',
					allowBlank: true,
					hidden: false,
					maxLength: 5,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: ' * Plazo M�ximo',
			combineErrors: false,
			msgTarget: 'side',
			id: 'cg_periodo2',
			width: 500,
			items: [	
				{
					xtype: 'numberfield',
					name: 'cg_periodo',
					id: 'cg_periodo1',
					fieldLabel: ' * Plazo M�ximo ',
					allowBlank: true,
					hidden: false,
					maxLength: 3,	
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		}	
	];
		
		
	var fpCarga = new Ext.form.FormPanel({
		id: 'fpCarga',
		width: 800,
		title: 'Carga Individual',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormaCarga,			
		monitorValid: true,
		buttons: [
		{
				text: 'Agregar',
				id: 'btnAgregar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: agregar
			},					
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: limpiar			
			}
		]
	});
	
	
	//Contenedor para la pantalla de Carga Inicial
	var fpConfirmar = new Ext.Container({
		layout: 'table',
		id: 'fpConfirmar',
		hidden: true,
		layoutConfig: {
			columns: 20
		},
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
		{
			xtype: 'button',
			text: 'Cancelar',			
			iconCls: 'icoLimpiar',
			id: 'btnCancelar',
			hidden: true,
			handler: procesarCancelar
		},
		{
			xtype: 'button',
			text: 'Terminar captura',
			id: 'btnTerminaCap',
			iconCls: 'icoAceptar',
			handler: procesarTerminaCaptura,	
			hidden: true
		}
	]
	});
	
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fpCarga	,
			NE.util.getEspaciador(20),
			gridCaptura,
			gridTotales,
			NE.util.getEspaciador(20),
			fpConfirmar,
			NE.util.getEspaciador(20)
		]
	});
	
	
	catalogoPymeData.load();
	catalogoMonedaData.load();
	catalogoFactorajedaData.load();	
	catalogoTipoCompraData.load();
	
		
	function procesaValidaFactVencidoIF_EPO(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			
			Ext.getCmp('operaFactVencidoIF_EPO').setValue(jsonValoresIniciales.operaFactVencido);
			if(jsonValoresIniciales.operaFactVencido =='N')  {
				Ext.getCmp('ic_if1').markInvalid('El Intermediario financiero no opera Factoraje al Vencimiento, favor de  seleccionar otro');
				
				Ext.getCmp('ic_if1').focus();
				return;	
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}


	
	//esto es para mostrar los campos adicionales
	function procesaValoresIniciales(opts, success, response) {
	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
				var  numeroCampos = jsonValoresIniciales.numeroCampos;	
				for(var i= 1; i<=numeroCampos; i++) {			
					if(i==1){		
						var cg_campo1 =  Ext.decode(jsonValoresIniciales.cg_campo1);					
						var indice1 = fpCarga.items.indexOfKey('ct_referencia2')+1;
						fpCarga.insert(indice1,cg_campo1);
						fpCarga.doLayout();
					
					}		
					if(i==2){		
						var cg_campo2 =  Ext.decode(jsonValoresIniciales.cg_campo2);						
						var indice1 = fpCarga.items.indexOfKey('cg_campo1')+1;
						fpCarga.insert(indice1,cg_campo2);
						fpCarga.doLayout();						
					}	
					if(i==3){		
						var cg_campo3 =  Ext.decode(jsonValoresIniciales.cg_campo3);						
						var indice1 = fpCarga.items.indexOfKey('cg_campo2')+1;
						fpCarga.insert(indice1,cg_campo3);
						fpCarga.doLayout();						
					}		
					if(i==4){		
						var cg_campo4 =  Ext.decode(jsonValoresIniciales.cg_campo4);						
						var indice1 = fpCarga.items.indexOfKey('cg_campo3')+1;
						fpCarga.insert(indice1,cg_campo4);
						fpCarga.doLayout();						
					}	
					if(i==5){		
						var cg_campo5 =  Ext.decode(jsonValoresIniciales.cg_campo5);							
						var indice1 = fpCarga.items.indexOfKey('cg_campo4')+1;
						fpCarga.insert(indice1,cg_campo5);
						fpCarga.doLayout();						
					}											
				}//for				
			}
			fpCarga.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

		
	//Peticion para obtener valores de los campos adicionales
	if(campos=='S'){
		fpCarga.el.mask('Cargando Campos Adicionales...', 'x-mask-loading');	
		Ext.Ajax.request({
			url: '13forma01Ext.data.jsp',
			params: {
				informacion: "valoresIniciales"
			},
			callback: procesaValoresIniciales
		});
	}
	
	var df_fecha_entrega = Ext.getCmp("df_fecha_entrega1");	
	var cg_tipo_compra2 = Ext.getCmp("cg_tipo_compra2");
	var cg_tipo_compra = Ext.getCmp("cg_tipo_compra1");
	var cg_clave_presupuestaria = Ext.getCmp("cg_clave_presupuestaria1");	
	var cg_periodo = Ext.getCmp("cg_periodo1");
	
	var df_fecha_entrega3 = Ext.getCmp("df_fecha_entrega2");
	var cg_clave_presupuestaria3 = Ext.getCmp("cg_clave_presupuestaria2");	
	var cg_periodo3 = Ext.getCmp("cg_periodo2");
	var df_fecha_venc_pyme2 = Ext.getCmp('df_fecha_venc_pyme2');
	var df_fecha_venc_pyme1 = Ext.getCmp('df_fecha_venc_pyme1');
	
	 	
	if(sPubEPOPEFFlagVal =='S'  || sPubEPOPEFFlagVal =='s'){
		df_fecha_entrega.show();
		cg_tipo_compra.show();
		cg_clave_presupuestaria.show();
		cg_periodo.show();
		df_fecha_entrega3.show();
		cg_clave_presupuestaria3.show();
		cg_periodo3.show();
	}else {
		df_fecha_entrega.hide();
		cg_tipo_compra.hide();
		cg_tipo_compra2.hide();
		cg_clave_presupuestaria.hide();
		cg_periodo.hide();
		df_fecha_entrega3.hide();
		cg_clave_presupuestaria3.hide();
		cg_periodo3.hide();
	}
		
	if(operaFVPyme=='N'){
		df_fecha_venc_pyme2.hide(); 
		df_fecha_venc_pyme1.hide(); 
	}
		
	
} );