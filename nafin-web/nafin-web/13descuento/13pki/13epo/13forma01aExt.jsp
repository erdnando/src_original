<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.netro.exception.*,
	com.netro.descuento.*,
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
 
<%
 String hidCtrlNumDoctosMN = (request.getParameter("hidCtrlNumDoctosMN")!=null)?request.getParameter("hidCtrlNumDoctosMN"):"0";
 String hidCtrlMontoTotalMNDespliegue = (request.getParameter("hidCtrlMontoTotalMNDespliegue")!=null)?request.getParameter("hidCtrlMontoTotalMNDespliegue"):"0";
 String hidCtrlMontoMinMNDespliegue = (request.getParameter("hidCtrlMontoMinMNDespliegue")!=null)?request.getParameter("hidCtrlMontoMinMNDespliegue"):"0";
 String hidCtrlMontoMaxMNDespliegue = (request.getParameter("hidCtrlMontoMaxMNDespliegue")!=null)?request.getParameter("hidCtrlMontoMaxMNDespliegue"):"0";

 String hidCtrlNumDoctosDL = (request.getParameter("hidCtrlNumDoctosDL")!=null)?request.getParameter("hidCtrlNumDoctosDL"):"0";
 String hidCtrlMontoTotalDLDespliegue = (request.getParameter("hidCtrlMontoTotalDLDespliegue")!=null)?request.getParameter("hidCtrlMontoTotalDLDespliegue"):"0";
 String hidCtrlMontoMinDLDespliegue = (request.getParameter("hidCtrlMontoMinDLDespliegue")!=null)?request.getParameter("hidCtrlMontoMinDLDespliegue"):"0";
 String hidCtrlMontoMaxDLDespliegue = (request.getParameter("hidCtrlMontoMaxDLDespliegue")!=null)?request.getParameter("hidCtrlMontoMaxDLDespliegue"):"0";
 
 String strPendiente = (request.getParameter("strPendiente") != null) ? request.getParameter("strPendiente") : "N";
 String strPreNegociable = (request.getParameter("strPreNegociable") != null) ? request.getParameter("strPreNegociable") : "N";

	String ic_epo = iNoCliente;
			
	//Para la Parametrizaci?n Fodea 023  Mandato 2009
	String  bOperaFactConMandato =  "N",  bOperaFactorajeVencido  ="N", bOperaFactorajeDistribuido  ="N",
	bOperaNotasCredito  ="N",  bTipoFactoraje  ="N",  bOperaFactorajeVencidoInfonavit  ="N";
		
	Hashtable alParamEPO1 = new Hashtable();
try{

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	alParamEPO1 = BeanParamDscto.getParametrosEPO(ic_epo,1);	
		
	if (alParamEPO1!=null) {
		bOperaFactConMandato = alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString();
		bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
		bOperaFactorajeDistribuido = alParamEPO1.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString();
		bOperaNotasCredito = alParamEPO1.get("OPERA_NOTAS_CRED").toString();
		bOperaFactorajeVencidoInfonavit = alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString();	 
	}

	//verifico si hay campos
	CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
	
	Vector Vcampos = CargaDocumentos.getCamposAdicionales(ic_epo);
	String  campos ="N", noCampos  ="0" ,cg_campo01 ="",cg_campo02 ="",cg_campo03 ="",cg_campo04 ="", cg_campo05 ="", camobligatorio ="";
	if(Vcampos.size()>0) {  
		campos ="S"; 	
		noCampos  =String.valueOf(Vcampos.size()); 		
		for(int i=0;i<Vcampos.size();i++){ 
			Vector detalleCampo = (Vector) Vcampos.get(i);		
			int numCampo = Integer.parseInt((String)detalleCampo.get(0));	
			String nombreCampo = (String) detalleCampo.get(1);
			String 	obligatorio = (String)detalleCampo.get(4);
			camobligatorio= "";
			if(obligatorio.equals("S"))  camobligatorio =nombreCampo;
			if(i==0) cg_campo01 =camobligatorio;
			if(i==1) cg_campo02 =camobligatorio;
			if(i==2) cg_campo03 =camobligatorio;
			if(i==3) cg_campo04 =camobligatorio;
			if(i==4) cg_campo05 =camobligatorio;			
		}	
	}
	
	 String sPubEPOPEFFlag							= new String(""),		sPubEPOPEFFlagVal						= new String(""),
				sPubEPOPEFDocto1						= new String(""), 		sPubEPOPEFDocto1Val					= new String(""),
				sPubEPOPEFDocto2						= new String(""),			sPubEPOPEFDocto2Val					= new String(""),
				sPubEPOPEFDocto3						= new String(""),			sPubEPOPEFDocto3Val					= new String(""),
				sPubEPOPEFDocto4						= new String(""),			sPubEPOPEFDocto4Val					= new String(""),
	      sOperaFactConMandato             = new String("");
 
	ArrayList alParamEPO = BeanParamDscto.getParamEPO(ic_epo, 1);
	if (alParamEPO!=null) {  
		//estos son los valores que se consultaron
		//este es el orden en el que me van a llegar los datos pues ordene la consulta por asc de nombre del campo
		sPubEPOPEFDocto3						= (alParamEPO.get(15)==null)?"":alParamEPO.get(15).toString();//PUB_EPO_PEF_CLAVE_PRESUPUESTAL
		sPubEPOPEFDocto3Val					= (alParamEPO.get(16)==null)?"":alParamEPO.get(16).toString();
		sPubEPOPEFDocto1						= (alParamEPO.get(17)==null)?"":alParamEPO.get(17).toString();//PUB_EPO_PEF_FECHA_ENTREGA
		sPubEPOPEFDocto1Val					= (alParamEPO.get(18)==null)?"":alParamEPO.get(18).toString();
		sPubEPOPEFFlag							= (alParamEPO.get(19)==null)?"":alParamEPO.get(19).toString();//PUB_EPO_PEF_FECHA_RECEPCION
		sPubEPOPEFFlagVal						= (alParamEPO.get(20)==null)?"":alParamEPO.get(20).toString();
		sPubEPOPEFDocto4						= (alParamEPO.get(21)==null)?"":alParamEPO.get(21).toString();//PUB_EPO_PEF_PERIODO
		sPubEPOPEFDocto4Val					= (alParamEPO.get(22)==null)?"":alParamEPO.get(22).toString();
		sPubEPOPEFDocto2						= (alParamEPO.get(23)==null)?"":alParamEPO.get(23).toString();//PUB_EPO_PEF_TIPO_COMPRA
		sPubEPOPEFDocto2Val					= (alParamEPO.get(24)==null)?"":alParamEPO.get(24).toString();					
	}		
	String fechaActual=(new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date());
	Vector vFechasVencimiento = CargaDocumentos.getFechasVencimiento(ic_epo, 1);
	
	String fechaVencMin = "", fechaVencMax = "", spubdocto_venc = "",	 fechaPlazoPagoFVP = "",
	 plazoMax1FVP  = "", plazoMax2FVP  = "";

	 fechaVencMin = vFechasVencimiento.get(0).toString();
	 fechaVencMax = vFechasVencimiento.get(1).toString();
	 spubdocto_venc = (vFechasVencimiento.get(2)==null)?"":vFechasVencimiento.get(2).toString();
	 fechaPlazoPagoFVP = (vFechasVencimiento.get(3)==null)?"":vFechasVencimiento.get(3).toString();
	 plazoMax1FVP = (vFechasVencimiento.get(4)==null)?"":vFechasVencimiento.get(4).toString();
	 plazoMax2FVP = (vFechasVencimiento.get(5)==null)?"":vFechasVencimiento.get(5).toString();
	
	
	//campos para capturar el Detalle del Documento
	String noCamposDetalle  ="0";
	Vector camposDetalle = CargaDocumentos.getCamposDetalle(ic_epo);
	if(camposDetalle.size()>0) {  		
		noCamposDetalle  =String.valueOf(camposDetalle.size()); 
	}
		
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>

<script type="text/javascript" src="13forma01aExt.js?<%=session.getId()%>"></script>

<%@ include file="/00utils/componente_firma.jspf" %>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>

<form id='formParametros' name="formParametros">
	<input type="hidden" id="strPendiente" name="strPendiente" value="<%=strPendiente%>"/>	
	<input type="hidden" id="strPreNegociable" name="strPreNegociable" value="<%=strPreNegociable%>"/>

	<input type="hidden" id="hidCtrlNumDoctosMN" name="hidCtrlNumDoctosMN" value="<%=hidCtrlNumDoctosMN%>"/>	
	<input type="hidden" id="hidCtrlMontoTotalMNDespliegue" name="hidCtrlMontoTotalMNDespliegue" value="<%=hidCtrlMontoTotalMNDespliegue%>"/>	
	<input type="hidden" id="hidCtrlMontoMinMNDespliegue" name="hidCtrlMontoMinMNDespliegue" value="<%=hidCtrlMontoMinMNDespliegue%>"/>	
	<input type="hidden" id="hidCtrlMontoMaxMNDespliegue" name="hidCtrlMontoMaxMNDespliegue" value="<%=hidCtrlMontoMaxMNDespliegue%>"/>	
	
	<input type="hidden" id="hidCtrlNumDoctosDL" name="hidCtrlNumDoctosMN" value="<%=hidCtrlNumDoctosDL%>"/>	
	<input type="hidden" id="hidCtrlMontoTotalDLDespliegue" name="hidCtrlMontoTotalDLDespliegue" value="<%=hidCtrlMontoTotalDLDespliegue%>"/>	
	<input type="hidden" id="hidCtrlMontoMinDLDespliegue" name="hidCtrlMontoMinDLDespliegue" value="<%=hidCtrlMontoMinDLDespliegue%>"/>	
	<input type="hidden" id="hidCtrlMontoMaxDLDespliegue" name="hidCtrlMontoMaxDLDespliegue" value="<%=hidCtrlMontoMaxDLDespliegue%>"/>	

	<input type="hidden" id="bOperaFactConMandato" name="bOperaFactConMandato" value="<%=bOperaFactConMandato%>"/>
	<input type="hidden" id="bOperaFactorajeVencido" name="bOperaFactorajeVencido" value="<%=bOperaFactorajeVencido%>"/>
	<input type="hidden" id="bOperaFactorajeDistribuido" name="bOperaFactorajeDistribuido" value="<%=bOperaFactorajeDistribuido%>"/>
	<input type="hidden" id="bOperaNotasCredito" name="bOperaNotasCredito" value="<%=bOperaNotasCredito%>"/>
	<input type="hidden" id="bOperaFactorajeVencidoInfonavit" name="bOperaFactorajeVencidoInfonavit" value="<%=bOperaFactorajeVencidoInfonavit%>"/>
	
	<input type="hidden" id="sPubEPOPEFDocto3" name="sPubEPOPEFDocto3" value="<%=sPubEPOPEFDocto3%>"/>
	<input type="hidden" id="sPubEPOPEFDocto3Val" name="sPubEPOPEFDocto3Val" value="<%=sPubEPOPEFDocto3Val%>"/>
	<input type="hidden" id="sPubEPOPEFDocto1" name="sPubEPOPEFDocto1" value="<%=sPubEPOPEFDocto1%>"/>
	<input type="hidden" id="sPubEPOPEFDocto1Val" name="sPubEPOPEFDocto1Val" value="<%=sPubEPOPEFDocto1Val%>"/>
	<input type="hidden" id="sPubEPOPEFFlag" name="sPubEPOPEFFlag" value="<%=sPubEPOPEFFlag%>"/>
	<input type="hidden" id="sPubEPOPEFFlagVal" name="sPubEPOPEFFlagVal" value="<%=sPubEPOPEFFlagVal%>"/>
	<input type="hidden" id="sPubEPOPEFDocto4" name="sPubEPOPEFDocto4" value="<%=sPubEPOPEFDocto4%>"/>
	<input type="hidden" id="sPubEPOPEFDocto4Val" name="sPubEPOPEFDocto4Val" value="<%=sPubEPOPEFDocto4Val%>"/>
	<input type="hidden" id="sPubEPOPEFDocto2" name="sPubEPOPEFDocto2" value="<%=sPubEPOPEFDocto2%>"/>
	<input type="hidden" id="sPubEPOPEFDocto2Val" name="sPubEPOPEFDocto2Val" value="<%=sPubEPOPEFDocto2Val%>"/>
				
	<input type="hidden" id="campos" name="campos" value="<%=campos%>"/>
	
	<input type="hidden" id="hidFechaActual"   name="hidFechaActual"  value="<%=fechaActual%>">
	<input type="hidden" id="hidFechaVencMin"   name="hidFechaVencMin" value="<%=fechaVencMin%>">
	<input type="hidden" id="hidFechaVencMax"  name="hidFechaVencMax" value="<%=fechaVencMax%>">
	<input type="hidden" id="hidFechaPlazoPagoFVP"   name="hidFechaPlazoPagoFVP" value="<%=fechaPlazoPagoFVP%>">
	<input type="hidden" id="hidPlazoMax1FVP"  name="hidPlazoMax1FVP" value="<%=plazoMax1FVP%>">
	<input type="hidden" id="hidPlazoMax2FVP"  name="hidPlazoMax2FVP" value="<%=plazoMax2FVP%>">
	
	<input type="hidden" id="operaFVPyme"  name="operaFVPyme" value="<%=operaFVPyme%>">	
	<input type="hidden" id="spubdocto_venc"  name="spubdocto_venc" value="<%=spubdocto_venc%>">	
	<input type="hidden" id="noCampos"  name="noCampos" value="<%=noCampos%>">	
	<input type="hidden" id="noCamposDetalle"  name="noCamposDetalle" value="<%=noCamposDetalle%>">	
	<input type="hidden" id="cg_campo01"  name="cg_campo01" value="<%=cg_campo01%>">	
	<input type="hidden" id="cg_campo02"  name="cg_campo02" value="<%=cg_campo02%>">	
	<input type="hidden" id="cg_campo03"  name="cg_campo03" value="<%=cg_campo03%>">	
	<input type="hidden" id="cg_campo04"  name="cg_campo04" value="<%=cg_campo04%>">	
	<input type="hidden" id="cg_campo05"  name="cg_campo05" value="<%=cg_campo05%>">	
</form>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
<%
}catch(NafinException ne){
	out.println(ne.getMsgError());
}catch(Exception e){
	e.printStackTrace();
	out.println(" Error: "+e);
}
%>