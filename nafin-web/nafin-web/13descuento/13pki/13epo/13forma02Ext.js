
Ext.onReady(function() {
	
	
	var strPendiente =  Ext.getDom('strPendiente').value;
	var strPreNegociable =  Ext.getDom('strPreNegociable').value;
	
	var strFirmaManc =  Ext.getDom('strFirmaManc').value;
	var strHash =  Ext.getDom('strHash').value;
	var detalles =  Ext.getDom('detalles').value;
	
	var limpiar  = function() {
	
		Ext.getCmp("txttotdoc1").setValue('');
		Ext.getCmp("txtmtodoc1").setValue('');
		Ext.getCmp("txtmtomindoc1").setValue('');
		Ext.getCmp("txtmtomaxdoc1").setValue('');
		
		Ext.getCmp("txttotdocdol").setValue('');
		Ext.getCmp("txtmtodocdol").setValue('');
		Ext.getCmp("txtmtomindocdol").setValue('');
		Ext.getCmp("txtmtomaxdocdol").setValue('');
	}

//-----------------------------------------------------
	var validaLongDecim = function(value, longEntera, longDecimal){
    var msg = true;
    var RE = /^-{0,1}\d*\.{0,1}\d+$/; 
    var valor = value.replace(/[\,|\$]/g, '');
    //value = value.replace(/[\,]/g, '')
    if(RE.test(valor)){
      var n = valor.indexOf(".");
      if(n >= 0){
        var entero = valor.substring(0, n);
        var decimal = valor.substring(n+1);
        if(entero.length>longEntera | decimal.length>longDecimal)msg='El tama�o m�ximo para este campo es de '+longEntera+' enteros y '+longDecimal+' decimales';
      }else{
         if(valor.length>longEntera)msg='El tama�o m�ximo para este campo es de '+longEntera+' enteros y '+longDecimal+' decimales';
      }
    }
    return msg;
    
  }
  var formatNumber = {
    separador: ",", // separador para los miles
    sepDecimal: '.', // separador para los decimales
    formatear:function (num){
      num +='';
      var splitStr = num.split('.');
      var splitLeft = splitStr[0];
      var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
      var regx = /(\d+)(\d{3})/;
      while (regx.test(splitLeft)) {
        splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
      }
      return this.simbol + splitLeft +splitRight;
    },
    news:function(num, simbol){
      this.simbol = simbol ||'';
      return this.formatear(num);
    }
  }
  
  var esNumero = function(field){ 
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    if(RE.test(field.getValue().replace(/[\,|\$]/g, ''))){
      field.setValue(formatNumber.news(field.getValue().replace(/[\,|\$]/g, '')));
    }else{
      field.setValue('');
    }
    
  }
//---------------------------------------------------------  


	var cargaDetalle = function() {
		document.location.href  = "13forma02dExt.jsp?";	  
	}
	
	
	var enviar = function() {
	
	var continua = true;
		var txttotdoc1 = Ext.getCmp("txttotdoc1");
		var txtmtodoc1 = Ext.getCmp("txtmtodoc1");
		var txtmtomindoc1 = Ext.getCmp("txtmtomindoc1");
		var txtmtomaxdoc1  = Ext.getCmp("txtmtomaxdoc1");
		
		var txttotdocdol = Ext.getCmp("txttotdocdol");
		var txtmtodocdol = Ext.getCmp("txtmtodocdol");
		var txtmtomindocdol = Ext.getCmp("txtmtomindocdol");
		var txtmtomaxdocdol  = Ext.getCmp("txtmtomaxdocdol");
			
		Ext.getCmp("txttotdoc1").setValue(eliminaFormatoFlotante(txttotdoc1.getValue()));
		Ext.getCmp("txtmtodoc1").setValue(eliminaFormatoFlotante(txtmtodoc1.getValue()));
		Ext.getCmp("txtmtomindoc1").setValue(eliminaFormatoFlotante(txtmtomindoc1.getValue()));
		Ext.getCmp("txtmtomaxdoc1").setValue(eliminaFormatoFlotante(txtmtomaxdoc1.getValue()));
		
		Ext.getCmp("txttotdocdol").setValue(eliminaFormatoFlotante(txttotdocdol.getValue()));
		Ext.getCmp("txtmtodocdol").setValue(eliminaFormatoFlotante(txtmtodocdol.getValue()));
		Ext.getCmp("txtmtomindocdol").setValue(eliminaFormatoFlotante(txtmtomindocdol.getValue()));
		Ext.getCmp("txtmtomaxdocdol").setValue(eliminaFormatoFlotante(txtmtomaxdocdol.getValue()));
	
		if (  ( Ext.isEmpty(txttotdoc1.getValue())   ||  txttotdoc1.getValue()==0  ) 
		&& ( Ext.isEmpty(txttotdocdol.getValue()) ||  txttotdocdol.getValue()==0 )	 ) {
			Ext.MessageBox.alert("Mensaje","Por favor escriba el  n�mero total documentos en Moneda Nacional y/o D�lares");
			continua = false;
			return;
		}	
		
		var totalDoctos = txttotdoc1.getValue() + txttotdocdol.getValue();		
		if(totalDoctos>20000) {						
			Ext.MessageBox.alert("Mensaje","El n�mero m�ximo de documentos a publicar es de 20,000 registros ");			
			Ext.getCmp("txttotdoc1").setValue('');
			Ext.getCmp("txttotdocdol").setValue('');
			continua = false;
			return;
		}	
		
		//monedda Nacional
		
		if(!Ext.isEmpty(txttotdoc1.getValue())  ||  txttotdoc1.getValue()>0  ) {
		
			if(Ext.isEmpty(txtmtodoc1.getValue()) || txtmtodoc1.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomindoc1.getValue()) || txtmtomindoc1.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomaxdoc1.getValue()) || txtmtomaxdoc1.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
				continua = false;
				return;
			}
						
			if(  (parseFloat(txtmtodoc1.getValue()) < parseFloat(txtmtomaxdoc1.getValue()) ) 
				|| (parseFloat(txtmtomaxdoc1.getValue()) < parseFloat(txtmtomindoc1.getValue() )  )
				|| (parseFloat(txtmtodoc1.getValue) < parseFloat(txtmtomindoc1.getValue()) ) ){
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (Moneda Nacional)");
				continua = false;
				return;
			}		
		}
		
		//moneda Dolar		
		
		if(!Ext.isEmpty(txttotdocdol.getValue())  ) {	
		
			if(Ext.isEmpty(txtmtodocdol.getValue()) || txtmtodocdol.getValue()==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el Monto total de Documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomindocdol.getValue()) || txtmtomindocdol.getValue()==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�nimo de documentos");
				continua = false;
				return;
			}
			
			if(Ext.isEmpty(txtmtomaxdocdol.getValue()) || txtmtomaxdocdol.getValue() ==0) {
				Ext.MessageBox.alert("Mensaje","Por favor escriba el monto m�ximo de documentos");
				continua = false;
				return;
			}
			
			if( (parseFloat(txtmtodocdol.getValue()) < parseFloat(txtmtomaxdocdol.getValue())) 
				|| (parseFloat(txtmtomaxdocdol.getValue()) < parseFloat(txtmtomindocdol.getValue()) )
				|| (parseFloat(txtmtodocdol.getValue) < parseFloat(txtmtomindocdol.getValue()))  ){		
				Ext.MessageBox.alert("Mensaje","El monto m�ximo debe ser menor al monto total y mayor al monto m�nimo (D�lares)");
				continua = false;
				return;
			}		
			
		}
		
	 if (Ext.isEmpty(txttotdoc1.getValue()) ) {  	Ext.getCmp("txttotdoc1").setValue(0);  }
		if (Ext.isEmpty(txtmtodoc1.getValue()) ) {  	Ext.getCmp("txtmtodoc1").setValue(0); }
		if (Ext.isEmpty(txtmtomindoc1.getValue()) ) {  	Ext.getCmp("txtmtomindoc1").setValue(0); }
		if (Ext.isEmpty(txtmtomaxdoc1.getValue()) ) {  	Ext.getCmp("txtmtomaxdoc1").setValue(0); }
		
		if (Ext.isEmpty(txttotdocdol.getValue()) ) {  	Ext.getCmp("txttotdocdol").setValue(0); }
		if (Ext.isEmpty(txtmtodocdol.getValue()) ) {  	Ext.getCmp("txtmtodocdol").setValue(0); }
		if (Ext.isEmpty(txtmtomindocdol.getValue()) ) {  	Ext.getCmp("txtmtomindocdol").setValue(0); }
		if (Ext.isEmpty(txtmtomaxdocdol.getValue()) ) {  	Ext.getCmp("txtmtomaxdocdol").setValue(0); }

	
		var parametros = "txttotdoc="+txttotdoc1.getValue()+"&txtmtodoc="+txtmtodoc1.getValue()
										+"&txtmtomindoc="+txtmtomindoc1.getValue() +"&txtmtomaxdoc="+txtmtomaxdoc1.getValue()
										+"&txttotdocdo="+txttotdocdol.getValue() +"&txtmtodocdo="+txtmtodocdol.getValue()
										+"&txtmtomindocdo="+txtmtomindocdol.getValue() +"&txtmtomaxdocdo="+txtmtomaxdocdol.getValue()
										+"&strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable
										+"&strFirmaManc="+strFirmaManc+"&strHash="+strHash;												
		if(continua ==true) {
			document.location.href  = "13forma02aExt.jsp?"+parametros;	
		}		
	
	}
	
	
	var elementosForma = [	
		{ 
			xtype:   'label',  
			html:		'Moneda Nacional', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},		
		{
			xtype: 'numberfield',
			name: 'txttotdoc',
			id: 'txttotdoc1',
			fieldLabel: 'No. total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 5,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',
			allowNegative : false,			
			margins: '0 20 0 0'		
		},
		{
			xtype: 'textfield',
			name: 'txtmtodoc',
			id: 'txtmtodoc1',
			fieldLabel: 'Monto total de documentos',
			allowBlank: true,
			startDay: 0,
			//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
      validator:function(value){ return validaLongDecim(value,17,2)},
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("txtmtodoc1").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomindoc',
			id: 'txtmtomindoc1',
			fieldLabel: 'Monto m�nimo de los documentos',
			allowBlank: true,
			startDay: 0,
			//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
      validator:function(value){ return validaLongDecim(value,12,2)},
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("txtmtomindoc1").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomaxdoc',
			id: 'txtmtomaxdoc1',
			fieldLabel: 'Monto m�ximo de los documentos',
			allowBlank: true,
			startDay: 0,
			//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
      validator:function(value){ return validaLongDecim(value,12,2)},
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("txtmtomaxdoc1").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		},
		{ 
			xtype:   'label',  
			html:		'D�lares', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},	
		{
			xtype: 'numberfield',
			name: 'txttotdocdo',
			id: 'txttotdocdol',
			fieldLabel: 'No. total de documentos',
			allowBlank: true,
			startDay: 0,
			maxLength: 5,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 200,
			msgTarget: 'side',	
			allowNegative : false,			
			margins: '0 20 0 0'		
		},
		{
			xtype: 'textfield',
			name: 'txtmtodocdo',
			id: 'txtmtodocdol',
			fieldLabel: 'Monto total de documentos',
			allowBlank: true,
			startDay: 0,
			//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
      validator:function(value){ return validaLongDecim(value,17,2)},
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("txtmtodocdol").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomindocdo',
			id: 'txtmtomindocdol',
			fieldLabel: 'Monto m�nimo de los documentos',
			allowBlank: true,
			startDay: 0,
			//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
      validator:function(value){ return validaLongDecim(value,12,2)},
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("txtmtomindocdol").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		},
		{
			xtype: 'textfield',
			name: 'txtmtomaxdocdo',
			id: 'txtmtomaxdocdol',
			fieldLabel: 'Monto m�ximo de los documentos',
			allowBlank: true,
			startDay: 0,
			//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
      validator:function(value){ return validaLongDecim(value,12,2)},
			width: 200,
			msgTarget: 'side',						
			margins: '0 20 0 0',
			listeners:{
				blur: function(field){ 
					if(!isDec(field.getValue() )){
						Ext.getCmp("txtmtomaxdocdol").setValue('');
					}else  {
						field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  
					}
				}
			}
		}
	];
	
	var fpCifras = new Ext.form.FormPanel({
		id: 'fpCifras',
		width: 500,
		title: 'Carga Masiva',
		frame: true,		
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 200,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,			
		monitorValid: true,
		buttons: [
			{
				text: 'Enviar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: enviar
			},
			{
				text: 'Carga de Detalle ',
				id: 'cargaDetalle',
				iconCls: 'icoAceptar',
				formBind: true,
				hidden: true,	
				handler: cargaDetalle
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: limpiar			
			}			
		]
	});
	
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fpCifras				
		]
	});
	
	
	if(detalles !=0 && strPreNegociable !='S'   &&  strPendiente !='S') {
		var cargaDetalle = Ext.getCmp("cargaDetalle");
		cargaDetalle.show();
	
	}
	
	
} );