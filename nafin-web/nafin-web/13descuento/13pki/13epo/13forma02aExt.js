
Ext.onReady(function() {
	
	
	var strPendiente =  Ext.getDom('strPendiente').value;
	var strPreNegociable =  Ext.getDom('strPreNegociable').value;

	var strFirmaManc =  Ext.getDom('strFirmaManc').value;
	var strHash =  Ext.getDom('strHash').value;
	
	var txttotdoc =  Ext.getDom('txttotdoc').value;
	var txtmtodoc =  Ext.getDom('txtmtodoc').value;
	var txtmtomindoc =  Ext.getDom('txtmtomindoc').value;
	var txtmtomaxdoc =  Ext.getDom('txtmtomaxdoc').value;
	
	var txttotdocdo =  Ext.getDom('txttotdocdo').value;
	var txtmtodocdo =  Ext.getDom('txtmtodocdo').value;
	var txtmtomindocdo =  Ext.getDom('txtmtomindocdo').value;
	var txtmtomaxdocdo =  Ext.getDom('txtmtomaxdocdo').value;
	var tipoArchivoC;
	var proceso;
	var strClaveHash;
	var procentaje = 0;
	var hayErrores;
	var vacio  = '<table width="100" align="center">'+
		'<tr><td width="300"><H1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</H1></td></tr></table>';

	var strPerfil =  Ext.getDom('strPerfil').value;
	var tooltip_Pro = 'Cargar� todo, �Documentos Sin Errores� y �Posibles Documentos Duplicados�. Ambos tipos con estatus �Negociable�';
	
	if (strPerfil == 'ADMIN EPO MAN1') {		
		tooltip_Pro = 'Cargar� todo, �Documentos Sin Errores� y �Posibles Documentos Duplicados�. Ambos tipos con estatus �Pre Negociables�';	
	}else if (strPerfil == 'ADMIN EPO MAN0') {		
		tooltip_Pro = 'Cargar� todo, �Documentos Sin Errores� y �Posibles Documentos Duplicados�. Ambos tipos con estatus �Pendiente Negociable�';	
	}	
	
			
	
		//cancelar la captura de documentos
	var procesarCancelar =  function () {
		if(!confirm("�Est� seguro de querer cancelar la operaci�n?")) {
			return;
		}else {			
			Ext.Ajax.request({
			url : '13forma02Ext.data.jsp',
				params : {
					informacion: 'Cancelar',
					proceso:proceso				
				},
				callback: procesarSuccessFailureCancelar
			});	
		}	
	} 
	
	var procesarSuccessFailureDuplicidad =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var datos = Ext.util.JSON.decode(response.responseText);
			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarRegistrosTodo =  function() {
	
		bandera_duplicados  =  Ext.getCmp('bandera_duplicados').getValue();
		strPerfil  =  Ext.getCmp('strPerfil').getValue();
		
		var parametros = "pantalla= PreAcuse"+"&proceso="+proceso+"&strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable+
											"&txttotdoc="+txttotdoc+"&txtmtodoc="+txtmtodoc+"&txtmtomindoc="+txtmtomindoc+"&txtmtomaxdoc="+txtmtomaxdoc+	
											"&txttotdocdo="+txttotdocdo+"&txtmtodocdo="+txtmtodocdo+"&txtmtomindocdo="+txtmtomindocdo+"&txtmtomaxdocdo="+txtmtomaxdocdo+
											"&hayErrores="+hayErrores+"&doctoDuplicados=S";	
											
		var mensajeDu;
		if (bandera_duplicados == 'S') {
			if (strPerfil == 'ADMIN EPO') {
				mensajeDu='Los documentos se�alados en la tabla �Posibles Documentos Duplicados� tambi�n ser�n publicados con estatus �Negociable�. <br> �Est� seguro de querer continuar con el proceso?'; 			
			}else if (strPerfil == 'ADMIN EPO MAN1') {			
				mensajeDu='Los documentos se�alados en la tabla �Posibles Documentos Duplicados� tambi�n ser�n publicados con estatus �Pre Negociables�. <br> �Est� seguro de querer continuar con el proceso?';
			}else if (strPerfil == 'ADMIN EPO MAN0') {			
				mensajeDu='Los documentos se�alados en la tabla �Posibles Documentos Duplicados� tambi�n ser�n publicados con estatus �Pendiente Negociable�. <br> �Est� seguro de querer continuar con el proceso?';
			}
			
			Ext.MessageBox.confirm('Mensaje',mensajeDu, 
			function showResult(btn){
				if (btn == 'yes') {													
					document.location.href = "13forma02bExt.jsp?"+parametros;		
				}
			});
			
		}else {
			document.location.href = "13forma02bExt.jsp?"+parametros; 			
		}
	}
	
	var procesarRegistros =  function() {
		
		bandera_duplicados  =  Ext.getCmp('bandera_duplicados').getValue();
		var parametros = "pantalla= PreAcuse"+"&proceso="+proceso+"&strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable+
										"&txttotdoc="+txttotdoc+"&txtmtodoc="+txtmtodoc+"&txtmtomindoc="+txtmtomindoc+"&txtmtomaxdoc="+txtmtomaxdoc+	
										"&txttotdocdo="+txttotdocdo+"&txtmtodocdo="+txtmtodocdo+"&txtmtomindocdo="+txtmtomindocdo+"&txtmtomaxdocdo="+txtmtomaxdocdo+
										"&hayErrores="+hayErrores+"&doctoDuplicados=N";
		
		if (bandera_duplicados == 'S') {								
			Ext.MessageBox.confirm('Mensaje','Los documentos se�alados en la tabla �Posibles Documentos Duplicados� no ser�n publicados. <br> Si desea continuar de clic en S�, si desea publicar los �Posibles Documentos Duplicados� entonces de clic en No y presione la opci�n �Procesar Todo�.', 
			function showResult(btn){
				if (btn == 'yes') {				
					document.location.href = "13forma02bExt.jsp?"+parametros;		
				}			
			});
		}else {
			document.location.href = "13forma02bExt.jsp?"+parametros;			
		}
	} 
	
	
	
	//Contenedor para los botones de procesar 
	var fpBotones = new Ext.Container({
		layout: 'table',		
		id: 'fpBotones',			
		width:	'1',
		heigth:	'auto',
		style: 'margin:0 auto;',			
		items: [			
		{
			xtype: 'button',
			text: 'Procesar',			
			id: 'btnProcesar',
			tooltip: ' Cargar� �nicamente los �Documentos Sin Errores�. Los posibles documentos duplicados no ser�n publicados',
			handler: procesarRegistros,
			style: { 
				width:   		'1', 
				textAlign: 		'right'
			} 
		},	
		{
			xtype: 'displayfield',
			value: '&nbsp;&nbsp;'				
		},
		{
			xtype: 'button',
			text: 'Procesar Todo',			
			id: 'btnProcesarTodo',
			tooltip: tooltip_Pro, 
			handler: procesarRegistrosTodo,
			style: { 
				width:   		'1', 
				textAlign: 		'right'
			} 
		},	
		{
			xtype: 'displayfield',
			value: '&nbsp;&nbsp;'		 		
		},
		{
			xtype: 'button',
			text: 'Cancelar',			
			id: 'btnCancelarTodo',  
			tooltip: 'Cancelar',
			handler:procesarCancelar,
			style: { 
				width:   		'1', 
				textAlign: 		'right'
			} 
		}
	]
	}); 
	
	var procesarSuccessFailureResumen =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var resp = 	Ext.util.JSON.decode(response.responseText);	
				
			proceso =resp.proceso;
			strClaveHash = resp.strClaveHash;
			hayErrores =resp.hayErrores;
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var table;
			if(resp.estatusProceso !='OK' ) {
				fpCarga.el.unmask();
				var display = '';
				Ext.getCmp('bandera_duplicados').setValue(resp.bandera_duplicados); 
				Ext.getCmp('strPerfil').setValue(resp.strPerfil); 
				
				//se muestra el mensaje bajo los totales en caso de haber			
				if(resp.bValidaDuplicidad=='S'){
					table ='<table align="center" width="800" class="formas"  cellspacing="0" cellpadding="0">'+
								'<tr>'+
									'<td width="300"  align="center">Documentos sin Errores:</td> '+
									'<td width="300"  align="center">Posibles Documentos Duplicados:</td> '+
									'<td width="300"  align="center"  >Detalle de Documentos con Errores:</td> '+
								'</tr>'+									
								'<tr >'+
									'<td valign="middle" align="center" class="formas" width="300" >'+
										'<textarea name="conerrores" cols="40" wrap="hard" rows="20" class="formas">'+resp.sinError+'</textarea>'+
									'</td>'+
									
									'<td valign="middle" align="center" class="formas" width="300" >'+
										'<textarea name="duplicados" cols="40" wrap="hard" rows="20" class="formas">'+resp.duplicados+'</textarea>'+
									'</td>'+
									
									'<td valign="middle" align="center" class="formas" width="300" >'+
										'<textarea name="conerrores" cols="40" wrap="hard" rows="20" class="formas">'+resp.error+'</textarea>'+
									'</td>'+
									
								'</tr>'+																				
								'<tr>'+
									'<td valign="middle" align="center" colspan="4" class="formas" width="800">'+
									'<table cellpadding="1" align="center"   cellspacing="2" border="0">'+						
									'<tr>'+
										'<td valign="middle" align="center" class="formas" width="130">Total Moneda Nacional:</td> '+
										'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnOK" size="18" value="'+resp.monto_tot_mnOK+'" readonly onFocus="this.blur()"></td>'+
										
										'<td valign="middle" align="center" class="formas" width="130">Total Moneda Nacional:</td> '+
										'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnOK" size="18" value="'+resp.monto_dup+'" readonly onFocus="this.blur()"></td>'+
									
									
										'<td valign="middle" align="center" class="formas" width="130">Total Moneda Nacional:</td> '+
										'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnErr" size="18" value="'+resp.monto_tot_mnErr+'" readonly onFocus="this.blur()"></td>'+
										'</tr>'+
										
										'<tr>'+
										'<td valign="middle" align="center" class="formas" width="130">Total D�lares:</td> '+
										'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnOK" size="18" value="'+resp.monto_tot_dolarOK+'" readonly onFocus="this.blur()"></td>'+
										
										'<td valign="middle" align="center" class="formas" width="130">Total D�lares:</td> '+
										'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnOK" size="18" value="'+resp.monto_dup_dl+'" readonly onFocus="this.blur()"></td>'+
										
										
										'<td valign="middle" align="center" class="formas" width="130">Total D�lares:</td> '+
										'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnErr" size="18" value="'+resp.monto_tot_dolarErr+'" readonly onFocus="this.blur()"></td>'+
									'</tr>'+
									'<table>'+
									'</td> '+	
									'</tr>'+											
								'<tr>'+
									'<td width="300" align="center" >Errores vs Cifras de Control:</td> '+											
								'</tr>'+										
								'<tr>'+
									'<td valign="middle" align="center" class="formas" width="300"  >'+
										'<textarea name="conerrores" cols="100" wrap="hard" rows="6" class="formas">'+resp.errorCifras+'</textarea>'+
									'</td>'+
								'</tr>'+										
								'</table>';		
				}else{
					table = '<table align="center" width="800" class="formas"  cellspacing="0" cellpadding="0">'+
										'<tr>'+
											'<td align="center">'+
							'<table align="center" width="600" class="formas"  cellspacing="0" cellpadding="0">'+
										'<tr>'+
											'<td width="300"  align="center">Documentos sin Errores:</td> '+
											'<td width="300"  align="center"  >Detalle de Documentos con Errores:</td> '+
										'</tr>'+									
										'<tr>'+
											'<td valign="middle" align="center" class="formas" width="300" >'+
												'<textarea name="conerrores" cols="50" wrap="hard" rows="20" class="formas">'+resp.sinError+'</textarea>'+
											'</td>'+
											'<td valign="middle" align="center" class="formas" width="300" >'+
												'<textarea name="conerrores" cols="50" wrap="hard" rows="20" class="formas">'+resp.error+'</textarea>'+
											'</td>'+
										'</tr>'+																				
										'<tr>'+
											'<td valign="middle" align="center" colspan="4" class="formas" width="600">'+
											'<table cellpadding="1" align="center"   cellspacing="2" border="0">'+						
											'<tr>'+
												'<td valign="middle" align="center" class="formas" width="130">Total Moneda Nacional:</td> '+
												'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnOK" size="18" value="'+resp.monto_tot_mnOK+'" readonly onFocus="this.blur()"></td>'+
												'<td valign="middle" align="center" class="formas" width="130">Total Moneda Nacional:</td> '+
												'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnErr" size="18" value="'+resp.monto_tot_mnErr+'" readonly onFocus="this.blur()"></td>'+
												'</tr>'+
												'<tr>'+
												'<td valign="middle" align="center" class="formas" width="130">Total D�lares:</td> '+
												'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnOK" size="18" value="'+resp.monto_tot_dolarOK+'" readonly onFocus="this.blur()"></td>'+
												'<td valign="middle" align="center" class="formas" width="130">Total D�lares:</td> '+
												'<td valign="middle" align="center" class="formas" width="130"><input type="text" class="formas" name="tot_mnErr" size="18" value="'+resp.monto_tot_dolarErr+'" readonly onFocus="this.blur()"></td>'+
											'</tr>'+
											'<table>'+
											'</td> '+	
											'</tr>'+											
										'<tr>'+
											'<td width="300" align="center" >Errores vs Cifras de Control:</td> '+											
										'</tr>'+										
										'<tr>'+
											'<td valign="middle" align="center" class="formas" width="300"  >'+
												'<textarea name="conerrores" cols="100" wrap="hard" rows="6" class="formas">'+resp.errorCifras+'</textarea>'+
											'</td>'+
										'</tr>'+										
										'</table>'+
									'</td>'+
										'</tr>'+										
										'</table>';
				}
					
				var fpError = new Ext.Container({
					layout: 'table',
					id: 'fpError',				
					width:	800,
					heigth:	300,
					style: 'margin:0 auto;',
					bodyStyle: 'padding: 6px',
					items: [		
					{ 
						xtype:   'label',  
						html:		table, 
						cls:		'x-form-item',
						align: 'center'
					}				
				]
				});
								
				var cancelar = Ext.getCmp('cancelar');
				cancelar.show();
				var pbar2 = Ext.getCmp('pbar2');				
				pbar2.hide();
				//contenedorPrincipalCmp.add(NE.util.getEspaciador(20));	
				contenedorPrincipalCmp.remove(fpError);				
				contenedorPrincipalCmp.add(fpError);				
									
				//para mostrar el boton de procesar			
				if(resp.bBotonProcesar =='S'){
					contenedorPrincipalCmp.remove(fpBotones);	
					contenedorPrincipalCmp.add(fpBotones);	
					
					if( resp.errorCifras  !='')  {						
						Ext.getCmp('btnProcesar').hide();
						Ext.getCmp('btnProcesarTodo').hide();	
					}	 
				
					if(resp.sinError =='' ){						
						Ext.getCmp('btnProcesar').hide();					
					}else  if(resp.duplicados ==''){								
						Ext.getCmp('btnProcesarTodo').hide();	
					} 
					
				}	
				contenedorPrincipalCmp.doLayout();
			}	
		} else {		
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	
	//procesa  el archivo de Carga de Documentos 
	var procesarSuccessFailureCarga =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var resp = 	Ext.util.JSON.decode(response.responseText);				
			
			proceso =resp.proceso;	
						
			if(resp.numReg!='0'){
				procentaje = parseFloat(resp.numProcReg)/parseFloat(resp.numReg);
				//procentaje = Math.round(procentaje * Math.pow(10, 1)) / Math.pow(10, 1);
			}
					
			fpCarga.setTitle('Proceso de Validaci�n');			
			fpCarga.add(barraProgreso);
			fpCarga.doLayout();			
			
			//barraProgreso.updateProgress(procentaje,'Validando '+resp.numProcReg+' de '+resp.numReg);
      barraProgreso.wait({
            interval:200,
            duration:5000,
            increment:15,
            text: 'Validando '+resp.numReg+' registros...',
            fn:function(){
              if(resp.estatusProceso == 'PR'){	
               ajaxPeticionCarga(resp.proceso, resp.numReg);
              }
              
              if(resp.estatusProceso == 'ER'){				
                barraProgreso.updateText('Finalizado');				
                Ext.Ajax.request({
                  url: '13forma02Ext.data.jsp',
                  params:{														
                    informacion: 'ResumendeEjecucion',
                    proceso:resp.proceso,
                    strClaveHash:resp.strClaveHash
                  },
                  callback: procesarSuccessFailureResumen
                });		
              }
            }
        });
							
			/*if(resp.estatusProceso == 'PR'){	
			 ajaxPeticionCarga(resp.proceso, resp.numReg);
			}
			
			if(resp.estatusProceso == 'ER'){				
				barraProgreso.updateText('Finalizado');				
				Ext.Ajax.request({
				url: '13forma02Ext.data.jsp',
				params:{														
					informacion: 'ResumendeEjecucion',
					proceso:resp.proceso,
					strClaveHash:resp.strClaveHash
				},
				callback: procesarSuccessFailureResumen
			});		
		}
    */
		
		if(resp.estatusProceso =='OK') {				
		
				var parametros = "pantalla= PreAcuse"+"&proceso="+proceso+"&strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable+
											"&txttotdoc="+txttotdoc+"&txtmtodoc="+txtmtodoc+"&txtmtomindoc="+txtmtomindoc+"&txtmtomaxdoc="+txtmtomaxdoc+	
											"&txttotdocdo="+txttotdocdo+"&txtmtodocdo="+txtmtodocdo+"&txtmtomindocdo="+txtmtomindocdo+"&txtmtomaxdocdo="+txtmtomaxdocdo
											"&strClaveHash="+resp.strClaveHash;
					document.location.href = "13forma02bExt.jsp?"+parametros;		
		}
		
			
		} else {		
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//procesa  el archivo de Carga de Documentos 
	var ajaxPeticionCarga =  function(proceso, NumLinea, strClaveHash) {
		Ext.Ajax.request({
			url: '13forma02Ext.data.jsp',
			params:{														
				informacion: 'ProcesaCargaArchivo',
				proceso:proceso,
				NumLineas:NumLinea,
				strClaveHash:strClaveHash
			},
			callback: procesarSuccessFailureCarga
		});
	}		
	
	//procesa  el archivo de Carga de Documentos 
	var procesarSuccessFailureArchivo =  function(opts, success, response) {	
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
			var resp = 	Ext.util.JSON.decode(response.responseText);	
								
			ajaxPeticionCarga(resp.proceso, resp.NumLinea, resp.strClaveHash);
		} else {
			fpCarga.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}


 var barraProgreso = new Ext.ProgressBar({
	text:'Iniciando Validaci�n...',
   id:'pbar2',
	width: 480,
	margins: '20 0 0 0',
	//align: 'center',
	hidden: false
 });
 
  	
	//cancelar la captura de documentos
	var procesarSuccessFailureCancelar =  function(opts, success, response) {		
			//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {			
						
			document.location.href  = "13forma02cExt.jsp?cancelacion=S"+"&strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable;
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
 
	//funcion para valdiar el formato
	var myValidFn = function(v) {
		var tipoArchivo01 = Ext.getCmp("tipoArchivo01");
		var tipoArchivo02 = Ext.getCmp("tipoArchivo02");
		
		if(tipoArchivo01.getValue() == false  &&  tipoArchivo02.getValue() ==false ){
			return true;		
		}
		
		if(tipoArchivo01.getValue() ==true ) {			
			var myRegex = /^.+\.([tT][xX][tT])$/;			
			return myRegex.test(v);
		}
		if(tipoArchivo02.getValue() ==true ) {			
			var myRegex = /^.+\.([zZ][iI][pP])$/;
			return myRegex.test(v);
		}
		
	}
	
	Ext.apply(Ext.form.VTypes, {
			archivotxt 		: myValidFn,
			archivotxtText 	: 'El formato del archivo de origen no es el correcto para el tipo de archivo seleccionado'
	});
	
	
	//funcion para enviar el archivoa validar 	
	var enviarArchivo = function() {
		var archivo = Ext.getCmp("archivo");
		var tipoArchivo01 = Ext.getCmp("tipoArchivo01");
		var tipoArchivo02 = Ext.getCmp("tipoArchivo02");
							
		if(tipoArchivo01.getValue() ==false &&  tipoArchivo02.getValue() ==false) {			
			Ext.MessageBox.alert("Mensaje","Seleccione un tipo de archivo ");
			return;
		}
				
		if (Ext.isEmpty(archivo.getValue()) ) {
			archivo.markInvalid('El valor de la Ruta es requerida.');	
			return;
		}
		if(tipoArchivo01.getValue() ==true) 	tipoArchivoC ="txt";
		if(tipoArchivo02.getValue() ==true) 	tipoArchivoC ="zip";
		
		Ext.getCmp('continuar').hide();
		
		fpCarga.el.mask('Procesando Carga de informaci�n..', 'x-mask-loading');	
		fpCarga.getForm().submit({
			url: '13forma02file.data.jsp?tipoArchivoC='+tipoArchivoC,									
			 waitMsg: 'Enviando datos...',
			 waitTitle :'Por favor, espere',			
			success: function(form, action) {								
				var resp = action.result;
				var mArchivo =resp.archivo;
				var error_tam = resp.error_tam;																		
				if(!resp.codificacionValida){
					fpCarga.el.unmask();
					Ext.getCmp('continuar').show();
					new NE.cespcial.AvisoCodificacionArchivo({codificacion:resp.codificacionArchivo}).show();
				}else if(mArchivo!='') {
					Ext.Ajax.request({
						url: '13forma02Ext.data.jsp',
						params:{														
							informacion: 'ValidaCargaArchivo',
							archivo:mArchivo,
							txttotdoc:txttotdoc,
							txtmtodoc:txtmtodoc,
							txtmtomindoc:txtmtomindoc,
							txtmtomaxdoc:txtmtomaxdoc,
							txttotdocdo:txttotdocdo,
							txtmtodocdo:txtmtodocdo,
							txtmtomindocdo:txtmtomindocdo,
							txtmtomaxdocdo:txtmtomaxdocdo,
							strFirmaManc:strFirmaManc,
							strHash:strHash,
							strPendiente:strPendiente,
							strPreNegociable:strPreNegociable
						},
						callback: procesarSuccessFailureArchivo
					});			
				}	else{
					Ext.MessageBox.alert("Mensaje","El Archivo es muy Grande, excede el L�mite que es de 2 MB.");
					return;
				}
			}
			,failure: NE.util.mostrarSubmitError
		})
		//fpCarga.el.unmask();
	}
		

	
	var elementosFormaCarga = [
		{  
			xtype:  'radiogroup',   
			fieldLabel: "",    
			name: 'tipoArchivo',   
			id: 'tipoArchivo',  
			value: 'S', 
			align: 'center',
			width: 200,							
				items: [						
					{ 
						boxLabel:    "TXT",             
						name:        'tipoArchivo', 
						id: 'tipoArchivo01',
						inputValue:  "TXT" ,
						value: 'TXT',
						checked: true,
						width: 20,
						listeners: {
							'blur': function(){
								Ext.getCmp('continuar').show();									
							}
						}
					}, 				
					{           
						boxLabel: "ZIP",             
						name: 'tipoArchivo', 
						id: 'tipoArchivo02',
						inputValue:  "ZIP", 
						value: 'ZIP',							
						width: 20,
						listeners: {
							'blur': function(){
								Ext.getCmp('continuar').show();									
							}
						}
					} 				
				]
			},
		{ 
			xtype:   'label',  			
			html:		'Si la carga de sus registros es mayor de 2,000 se recomienda ingresar un archivo comprimido (.zip)', 
			cls:		'x-form-item',
			width: 600
		},	
		{ 
			xtype:   'label',  			
			html:		vacio, 
			cls:		'x-form-item'	
		},						
		{
			xtype:	'panel',
			layout:	'column',
			width: 600,	
			align: 'center',
			id:'cargaArchivo1',			
			items:	[				
				{
					xtype: 'panel',
					id:		'pnlArchivo',
					//columnWidth: .95,
					//anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 150,				
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo',
							combineErrors: false,
							msgTarget: 'side',
							width: 400,
							items: [														
								{
									xtype: 'fileuploadfield',
									id: 'archivo',
									emptyText: 'Ruta del Archivo de Origen',
									fieldLabel: 'Ruta del Archivo de Origen',
									name: 'archivoFormalizacion',   
									buttonText: 'Examinar..',
									width: 320,	  																		
									vtype: 'archivotxt'
								}						
							]
						}
					]
				}	
			]
		},
		{ 	xtype: 'textfield',  hidden: true, id: 'bandera_duplicados', 	value: '' },
		{ 	xtype: 'textfield',  hidden: true, id: 'strPerfil', 	value: '' }	 
	];
	
	var fpCarga = new Ext.form.FormPanel({
	  id: 'fpCarga',
		width: 600,
		title: 'Ubicaci�n del archivo de datos',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',		
		items: elementosFormaCarga,			
		monitorValid: true,
		buttons: [
			{
				text: 'Continuar',
				id: 'continuar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler:enviarArchivo
			},
			{
				text: 'Cancelar',
				id: 'cancelar',
				iconCls: 'icoLimpiar',				
				formBind: true,
				hidden: true,
				handler:procesarCancelar				
			}
		]
	});
	
	 
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			fpCarga,
			NE.util.getEspaciador(20)
		]
	});
	
	
} );