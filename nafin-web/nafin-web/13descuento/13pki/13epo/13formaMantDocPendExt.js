Ext.onReady(function() {

	var form = {hayCheck:false,	num_acuse:null, regP:0, regPN:0, cadDocSelec:null, textoFirmar:null, pkcs7:null}
	//----------------------------------- Funciones a utilizar ----------------------------------

	function regresar(pagina){
		window.location = pagina;
	}

	function processResult(res){
		if (res.toLowerCase() == 'ok' || res.toLowerCase() == 'yes'){
			regresar('13formaMantDocPendExt.jsp');
		}
	}

	function processResultElimina(res){
		if (res.toLowerCase() == 'ok' || res.toLowerCase() == 'yes'){
			pnl.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url : '13formaMantDocPendExt.data.jsp',
				params : {
					informacion:			'Confirma',
					num_acuse:				form.num_acuse,
					cadDocSelec:			form.cadDocSelec,
					regPrenegociables:	form.regP,
					regPrenonegociables:	form.regPN,
					operacion:				Ext.getCmp('hidOperacion').getValue(),
					Pkcs7:					form.pkcs7,
					TextoFirmado:			form.textoFirmar
				},
				callback: procesaConfirma
			});
		}
	}

	//----------------------------------- Handlers ------------------------------
	function procesaConfirma(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			if (resp.operacion != undefined && resp.operacion == "Eliminar"){

				Ext.Msg.show({
					title:	'Confirmar',
					msg:		'Se eliminaron los registros seleccionados de manera satisfactoria.',
					buttons:	Ext.Msg.OK,
					fn: processResult,
					closable:false,
					icon: Ext.MessageBox.INFO
				});

			}else if (resp.operacion != undefined && resp.operacion == "Generar") {

				Ext.Msg.show({
					title:	'Confirmar',
					msg:		'Se ha cambiado el estatus satisfactoriamente a los documentos seleccionados.',
					buttons:	Ext.Msg.OK,
					fn: processResult,
					closable:false,
					icon: Ext.MessageBox.INFO
				});
			}
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}

	var Automatizacion = function(grid, rowIndex, colIndex, item, event) {
			form.cadDocSelec="";
			form.textoFirmar = "";
			Ext.getCmp('hidOperacion').setValue('');
			var registro = grid.getStore().getAt(rowIndex);
			form.num_acuse = registro.get('CC_ACUSE');
			fp.hide();
			grid.hide();
			if (	colIndex == 4	){	//	Individual

				Ext.getCmp('disAcuse').body.update('<div align="center">'+form.num_acuse+'</div>');
				Ext.getCmp('disFecha').body.update('<div align="center">'+registro.get('FECHA')+'</div>');
				Ext.getCmp('disUsuario').body.update('<div align="center">'+registro.get('USUARIO')+'</div>');
				Ext.getCmp('disDoctos').body.update('<div align="center">'+registro.get('NUMERO_DOCTOS')+'</div>');
				pnlIndividual.show();
				individualData.load({
						params: Ext.apply({
							operacion: 'Generar',
							start: 0,
							limit: 15
						})
					});

			}else if (	colIndex == 5 || colIndex == 6	){	//Masiva y/o eliminar

				if (colIndex == 6){
					Ext.getCmp('hidOperacion').setValue('Eliminar');
				}else{
					Ext.getCmp('hidOperacion').setValue('Generar');
				}

				Ext.Ajax.request({
					url : '13formaMantDocPendExt.data.jsp',
					params : {
						informacion:'detalleConfirma',
						num_acuse:form.num_acuse
					},
					callback: procesarDetalleConfirma
				});

			}
	}

	var procesarIndividualData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			if (!gridIndividual.isVisible()) {
				gridIndividual.show();
			}
			var el = gridIndividual.getGridEl();
			if(store.getTotalCount() > 0) {
				if (form.hayCheck) {
					Ext.getCmp('btnConfirma').enable();
					Ext.getCmp('btnTodos').enable();
					Ext.getCmp('btnQuitar').enable();
				}
				el.unmask();
			}else{
				Ext.getCmp('btnConfirma').disable();
				Ext.getCmp('btnTodos').disable();
				Ext.getCmp('btnQuitar').disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	function procesaConsulta(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR != undefined){
				consultaData.loadData('');
				if (!grid.isVisible()){
					grid.show();
				}
				if (infoR.registros != undefined){
					consultaData.loadData(infoR.registros);
					Ext.getCmp('btnImprimirPDF').enable();
					Ext.getCmp('btnImprimirPDF').setHandler( function(boton, evento) {
						var forma = Ext.getDom('formAux');
						forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
						forma.submit();
					});
					grid.getGridEl().unmask();
				}else{
					grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					Ext.getCmp('btnImprimirPDF').disable();
				}
			}			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarDetalleConfirma(opts, success, response) {
		pnl.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			pnlIndividual.hide();
			if ( Ext.getCmp('hidOperacion').getValue() == 'Eliminar'){
				Ext.getCmp('botonConfirmar').setText('Eliminar');
			}
			pnlConfirma.show();
			Ext.getCmp('nMn').body.update('<div align="center">0</div>');
			Ext.getCmp('nDl').body.update('<div align="center">0</div>');
			Ext.getCmp('totMnNe').body.update('<div align="right">$0.00</div>');
			Ext.getCmp('totDlNe').body.update('<div align="right">$0.00</div>');
			Ext.getCmp('totMnNo').body.update('<div align="right">$0.00</div>');
			Ext.getCmp('totDlNo').body.update('<div align="right">$0.00</div>');
			Ext.getCmp('totMn').body.update('<div align="right">$0.00</div>');
			Ext.getCmp('totDl').body.update('<div align="right">$0.00</div>');
			var infoR = Ext.util.JSON.decode(response.responseText);
			
			if (infoR != undefined){

				gridConfirmaPendNegoData.loadData('');
				gridConfirmaPendNoNegoData.loadData('');
				if (!gridConfirmaPendNego.isVisible()){
					gridConfirmaPendNego.show();
				}
				if (infoR.regsNego != undefined && infoR.regsNego.length > 0){	// Grid de Pendientes Negociables
					Ext.getCmp('totMnNe').body.update('<div align="right">'+Ext.util.Format.number(infoR.bdTotalMNNeg, '$0,0.00')+'</div>');
					Ext.getCmp('totDlNe').body.update('<div align="right">'+Ext.util.Format.number(infoR.bdTotalDolNeg, '$0,0.00')+'</div>');
					form.regP = infoR.regsNego.length;
					gridConfirmaPendNegoData.loadData(infoR.regsNego);
					var cmNeg = gridConfirmaPendNego.getColumnModel();
					cmNeg.setHidden(cmNeg.findColumnIndex('NOMBRE_IF'), 				true);
					cmNeg.setHidden(cmNeg.findColumnIndex('NOMBRE_BENEFICIARIO'),	true);
					cmNeg.setHidden(cmNeg.findColumnIndex('FN_PORC_BENEFICIARIO'),	true);
					cmNeg.setHidden(cmNeg.findColumnIndex('MONTO_BENEFICIARIO'),	true);
					
					if (infoR.bOperaFactorajeVencido != undefined && infoR.bOperaFactorajeVencido){
						cmNeg.setHidden(cmNeg.findColumnIndex('NOMBRE_IF'), false);
					}
					if (infoR.bOperaFactorajeDistribuido != undefined && infoR.bOperaFactorajeDistribuido){
						cmNeg.setHidden(cmNeg.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
						cmNeg.setHidden(cmNeg.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
						cmNeg.setHidden(cmNeg.findColumnIndex('MONTO_BENEFICIARIO'), false);
					}
					gridConfirmaPendNego.getGridEl().unmask();
				}else{
					form.regP = 0;
					gridConfirmaPendNego.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				}
				
				if (!gridConfirmaPendNoNego.isVisible()){
					gridConfirmaPendNoNego.show();
				}
				if (infoR.regsNoNego != undefined && infoR.regsNoNego.length > 0){	// Grid de Pendientes No Negociables
					Ext.getCmp('totMnNo').body.update('<div align="right">'+Ext.util.Format.number(infoR.bdTotalMNNoNeg, '$0,0.00')+'</div>');
					Ext.getCmp('totDlNo').body.update('<div align="right">'+Ext.util.Format.number(infoR.bdTotalDolNoNeg, '$0,0.00')+'</div>');
					form.regPN = infoR.regsNoNego.length;
					gridConfirmaPendNoNegoData.loadData(infoR.regsNoNego);
					var cmNoNego = gridConfirmaPendNoNego.getColumnModel();
					cmNoNego.setHidden(cmNoNego.findColumnIndex('NOMBRE_IF'),				true);
					cmNoNego.setHidden(cmNoNego.findColumnIndex('NOMBRE_BENEFICIARIO'),	true);
					cmNoNego.setHidden(cmNoNego.findColumnIndex('FN_PORC_BENEFICIARIO'),	true);
					cmNoNego.setHidden(cmNoNego.findColumnIndex('MONTO_BENEFICIARIO'),	true);
					if (infoR.bOperaFactorajeVencido != undefined && infoR.bOperaFactorajeVencido){
						cmNoNego.setHidden(cmNoNego.findColumnIndex('NOMBRE_IF'),false);
					}
					if (infoR.bOperaFactorajeDistribuido != undefined && infoR.bOperaFactorajeDistribuido){
						cmNoNego.setHidden(cmNoNego.findColumnIndex('NOMBRE_BENEFICIARIO'),	false);
						cmNoNego.setHidden(cmNoNego.findColumnIndex('FN_PORC_BENEFICIARIO'),	false);
						cmNoNego.setHidden(cmNoNego.findColumnIndex('MONTO_BENEFICIARIO'),	false);
					}
					gridConfirmaPendNoNego.getGridEl().unmask();
				}else{
					form.regPN = 0;
					gridConfirmaPendNoNego.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				}

				Ext.getCmp('gridTotales').show();
				resumenTotalesData.loadData('');

				if ( infoR.iNoTotalDtosMN != undefined && parseFloat(infoR.iNoTotalDtosMN) > 0 ){
					Ext.getCmp('nMn').body.update('<div align="center">'+infoR.iNoTotalDtosMN+'</div>');
					Ext.getCmp('totMn').body.update('<div align="right">'+Ext.util.Format.number(infoR.bdTotalMN, '$0,0.00')+'</div>');

					var reg = Ext.data.Record.create(['MONEDA','TOTAL_REG', 'MONTO_NEGO','MONTO_NEGO_DESC','MONTO_NO_NEGO','MONTO_NO_NEGO_DESC']);
					resumenTotalesData.add(
						new reg({
							MONEDA:'MONEDA NACIONAL',
							TOTAL_REG: infoR.iNoTotalDtosMN,
							MONTO_NEGO: infoR.bdTotalMNNeg,
							MONTO_NEGO_DESC: infoR.bdTotalDescMNNeg,
							MONTO_NO_NEGO: infoR.bdTotalMNNoNeg,
							MONTO_NO_NEGO_DESC: infoR.bdTotalDescMNNoNeg
						})
					);
				}
				if ( infoR.iNoTotalDtosDol != undefined && parseFloat(infoR.iNoTotalDtosDol) > 0 ){
					Ext.getCmp('nDl').body.update('<div align="center">'+infoR.iNoTotalDtosDol+'</div>');
					Ext.getCmp('totDl').body.update('<div align="right">'+Ext.util.Format.number(infoR.bdTotalDol, '$0,0.00')+'</div>');

					var reg = Ext.data.Record.create(['MONEDA',	'TOTAL_REG',	'MONTO_NEGO',	'MONTO_NEGO_DESC',	'MONTO_NO_NEGO',	'MONTO_NO_NEGO_DESC']);
					resumenTotalesData.add(
						new reg({
							MONEDA:'D�LARES',
							TOTAL_REG: infoR.iNoTotalDtosDol,
							MONTO_NEGO: infoR.bdTotalDolNeg,
							MONTO_NEGO_DESC: infoR.bdTotalDescDolNeg,
							MONTO_NO_NEGO: infoR.bdTotalDolNoNeg,
							MONTO_NO_NEGO_DESC: infoR.bdTotalDescDolNoNeg
						})
					);
				}

				if ( (infoR.iNoTotalDtosMN != undefined && parseFloat(infoR.iNoTotalDtosMN) < 1) && (infoR.iNoTotalDtosDol != undefined && parseFloat(infoR.iNoTotalDtosDol) < 1) ){
					Ext.getCmp('gridTotales').getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				}
			}

			if ( infoR.txtFirma != undefined ){
				form.textoFirmar = infoR.txtFirma;
				//Ext.Msg.alert('Texto a firmar',form.textoFirmar);
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var consultaData = new Ext.data.JsonStore({
		fields: [
			{name: 'CC_ACUSE'},
			{name: 'FECHA'},
			{name: 'USUARIO'},
			{name: 'NUMERO_DOCTOS'}
		],
		data:[	{'CC_ACUSE':'',	'FECHA':'',	'USUARIO':'',	'NUMERO_DOCTOS':''}	],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var individualData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13formaMantDocPendExt.data.jsp',
		baseParams: {
			informacion: 'Individual'
		},
		fields: [
			{name: 'NOMPYME'},
			{name: 'NUMDOCTO'},
			{name: 'FECEMISION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECVENC', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'NOMMONEDA'},
			{name: 'MONTO', type: 'float'},
			{name: 'NOMFACTORAJE'},
			{name: 'ESTATUS'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
										Ext.apply(options.params, {num_acuse: form.num_acuse});
									}
							},
			load: procesarIndividualData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarIndividualData(null, null, null);
				}
			}
		}
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		fields: [
			{name: 'MONEDA'},
			{name: 'TOTAL_REG'},
			{name: 'MONTO_NEGO'},
			{name: 'MONTO_NEGO_DESC'},
			{name: 'MONTO_NO_NEGO'},
			{name: 'MONTO_NO_NEGO_DESC'}
		],
		data:[
			{'MONEDA':'',	'TOTAL_REG':'',	'FECHA_DOCTO':'',	'MONTO_NEGO':'',	'MONTO_NEGO_DESC':'',	'MONTO_NO_NEGO':'',	'MONTO_NO_NEGO_DESC':''}//,//{'MONEDA':'',	'TOTAL_REG':'',	'FECHA_DOCTO':'',	'MONTO_NEGO':'',	'MONTO_NEGO_DESC':'',	'MONTO_NO_NEGO':'',	'MONTO_NO_NEGO_DESC':''},
		],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridConfirmaPendNegoData = new Ext.data.JsonStore({
		fields: [
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'FECHA_DOCTO'},
			{name: 'FECHA_VENC'},
			{name: 'CD_NOMBRE'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'FN_MONTO'},
			{name: 'PORC_ANTICIPO'},
			{name: 'MONTO_DSCTO'},
			{name: 'CT_REFERENCIA'},
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'MONTO_BENEFICIARIO'}
		],
		data:[
			{'CG_RAZON_SOCIAL':'',	'IG_NUMERO_DOCTO':'',	'FECHA_DOCTO':'',	'FECHA_VENC':'',	'CD_NOMBRE':'',	'TIPO_FACTORAJE':'',	'FN_MONTO':'',
			'PORC_ANTICIPO':'',	'MONTO_DSCTO':'',	'CT_REFERENCIA':'',	'NOMBRE_IF':'','NOMBRE_BENEFICIARIO':'','FN_PORC_BENEFICIARIO':'',	'MONTO_BENEFICIARIO':''	}
		],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridConfirmaPendNoNegoData = new Ext.data.JsonStore({
		fields: [
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'FECHA_DOCTO'},
			{name: 'FECHA_VENC'},
			{name: 'CD_NOMBRE'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'FN_MONTO'},
			{name: 'PORC_ANTICIPO'},
			{name: 'MONTO_DSCTO'},
			{name: 'CT_REFERENCIA'},
			{name: 'NOMBRE_IF'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'MONTO_BENEFICIARIO'}
		],
		data:[
			{'CG_RAZON_SOCIAL':'',	'IG_NUMERO_DOCTO':'',	'FECHA_DOCTO':'',	'FECHA_VENC':'',	'CD_NOMBRE':'',	'TIPO_FACTORAJE':'',	'FN_MONTO':'',
			'PORC_ANTICIPO':'',	'MONTO_DSCTO':'',	'CT_REFERENCIA':'',	'NOMBRE_IF':'','NOMBRE_BENEFICIARIO':'','FN_PORC_BENEFICIARIO':'',	'MONTO_BENEFICIARIO':''	}
		],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan: 4, align: 'center'},
				{header: 'Autorizaci�n', colspan: 3, align: 'center'}
			]
		]
	});

	var grid = new Ext.grid.GridPanel({
		store: 	consultaData,
		id:		'gridDoctosPNegociables',
		hidden: 	true,
		autoExpandColumn: 'Usuario',
		plugins: grupos,
		columns: [
			{
				header: 		'N�mero de Acuse',	tooltip:'N�mero de Acuse',	dataIndex:'CC_ACUSE',
				sortable: 	true,	resizable:true,	width:150,	hidden:false,	align:'center'
			},{
				header: 		'Fecha',	tooltip: 	'Fecha',	dataIndex:'FECHA',
				sortable: 	true,	resizable:true,	width:90,	hidden:false,	align:'center'
			},{
				header: 		'Usuario',	id: 'Usuario',	tooltip:'Usuario',dataIndex: 	'USUARIO',
				sortable: 	true,	resizable:true,	hidden:false,	align:'center'
			},{
				header: 		'N�mero Documentos',	tooltip:'N�mero Documentos',	dataIndex:'NUMERO_DOCTOS',
				sortable: 	true,	resizable: 	true,	width:120,	hidden:false,	align:'center'
			},{
            xtype: 		'actioncolumn',
				header:'Individual', tooltip:'Individual',	width:60,	hidden:false,	align:'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('CC_ACUSE') != '')	{
								this.items[0].tooltip = 'Ver';
								return 'modificar';
							}else	{
								return value;
							}
						},
						handler:	Automatizacion
					}
				]
			},{
            xtype: 		'actioncolumn',
				header:'Masiva', tooltip:'Masiva',	width:60,	hidden:false,	align:'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('CC_ACUSE') != '')	{
								this.items[0].tooltip = 'Ver';
								return 'modificar';
							}else	{
								return value;
							}
						},
						handler:	Automatizacion
					}
				]
			},{
            xtype: 		'actioncolumn',
				header:'Eliminar', tooltip:'Eliminar',	width:60,	hidden:false,	align:'center',
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('CC_ACUSE') != '')	{
								this.items[0].tooltip = 'Ver';
								return 'rechazar';
							}else	{
								return value;
							}
						},
						handler:	Automatizacion
					}
				]
         }
		],
		stripeRows: true,
		loadMask: 	true,
		height: 		400,
		width: 		943,		
		frame: 		true,
		columnLines: true,
		bbar: {
			xtype: 			'toolbar',
			id: 				'barraToolbar',
			items: [
					'->',
					'-',
					{
						xtype:	'button',
						text: 	'Imprimir PDF',
						id: 		'btnImprimirPDF'
					}
				]
			}
	});

	var gridTotales = new Ext.grid.GridPanel({
		store: resumenTotalesData,
		id: 'gridTotales',
		hidden:true,
		columns: [
			{
				//header: '&nbsp;',
				dataIndex: 'MONEDA',
				align: 'left',width: 200, resizable: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header: 'Registros',
				dataIndex: 'TOTAL_REG',
				width: 100,	align: 'center',renderer: Ext.util.Format.numberRenderer('0,000'), resizable: false
			},{
				header: 'Monto Pendientes Negociables', tooltip:'Monto Pendientes Negociables',
				dataIndex: 'MONTO_NEGO',
				width: 200,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'), resizable: false
			},{
				header: 'Monto a Descontar Pendientes Negociables',	tooltip:'Monto a Descontar Pendientes Negociables',
				dataIndex: 'MONTO_NEGO_DESC',
				width: 250,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'), resizable: false
			},{
				header: 'Monto Pendientes No Negociables',	tooltip:'Monto Pendientes No Negociables',
				dataIndex: 'MONTO_NO_NEGO',
				width: 200,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'), resizable: false
			},{
				header: 'Monto a Descontar Pendientes No Negociables',	tooltip:'Monto a Descontar Pendientes No Negociables',
				dataIndex: 'MONTO_NO_NEGO_DESC',
				width: 260,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'), resizable: false
			}
		],
		view: new Ext.grid.GridView({markDirty: false}),		//,forceFit: true
		width: 940,
		height: 120,
		columnLines: true,
		style: 'margin:0 auto;',
		loadMask: true,
		title: 'Totales',
		frame: false
	});

	var gridConfirmaPendNoNego = new Ext.grid.GridPanel({
		title:'Pendientes No Negociables',
		store: gridConfirmaPendNoNegoData,
		columns: [
			{
				header: 'Nombre Proveedor', tooltip: 'Nombre Proveedor',	dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true, width: 250, resizable: true, align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header: 'Num. Documento', tooltip: 'Num. Documento',	dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true, width: 130, resizable: true, hidden: false,align: 'center'
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	dataIndex : 'FECHA_DOCTO',
				sortable : true, width : 110, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: false
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	dataIndex : 'FECHA_VENC',
				sortable : true, width : 110, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: false
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'CD_NOMBRE',
				sortable : true, width : 150, hidden: false,align: 'center'
			},{
				header: 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	dataIndex: 'TIPO_FACTORAJE',
				sortable: true, width: 130, resizable: true, hidden: false,align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header : 'Monto', tooltip: 'Monto',	dataIndex : 'FN_MONTO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('0,0.00'), hidden: false
			},{
				header : 'Porcentaje de Descuento', tooltip: 'Porcentaje de Descuento',	dataIndex : 'PORC_ANTICIPO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('0 %'), hidden: false
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'MONTO_DSCTO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('0,0.00'), hidden: false
			},{
				header : 'Referencia', tooltip: 'Referencia',	dataIndex : 'CT_REFERENCIA',
				sortable : true, width : 150, hidden: false,align: 'center'
			},{
				header : 'Nombre IF', tooltip: 'Nombre IF',	dataIndex : 'NOMBRE_IF',
				sortable : true, width : 150, hidden: true,align: 'center', hideable:true
			},{
				header : 'Nombre Beneficiario', tooltip: 'Nombre Beneficiario',	dataIndex : 'NOMBRE_BENEFICIARIO',
				sortable : true, width : 150, hidden: true,align: 'center'
			},{
				header : 'Porcentaje Beneficiario', tooltip: 'Porcentaje Beneficiario',	dataIndex : 'FN_PORC_BENEFICIARIO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('0 %'), hidden: true
			},{
				header : 'Monto Beneficiario', tooltip: 'Monto Beneficiario',	dataIndex : 'MONTO_BENEFICIARIO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('0,0.00'), hidden: true
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 180,
		width: 940,
		columnLines: true,
		style: 'margin:0 auto;'
	});

	var gridConfirmaPendNego = new Ext.grid.GridPanel({
		title:'Pendientes Negociables',
		store: gridConfirmaPendNegoData,
		columns: [
			{
				header: 'Nombre Proveedor', tooltip: 'Nombre Proveedor',	dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true, width: 250, resizable: true, align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header: 'Num. Documento', tooltip: 'Num. Documento',	dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true, width: 130, resizable: true, hidden: false,align: 'center'
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	dataIndex : 'FECHA_DOCTO',
				sortable : true, width : 110, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: false
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	dataIndex : 'FECHA_VENC',
				sortable : true, width : 110, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: false
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'CD_NOMBRE',
				sortable : true, width : 150, hidden: false,align: 'center'
			},{
				header: 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	dataIndex: 'TIPO_FACTORAJE',
				sortable: true, width: 130, resizable: true, hidden: false,align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header : 'Monto', tooltip: 'Monto',	dataIndex : 'FN_MONTO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('0,0.00'), hidden: false
			},{
				header : 'Porcentaje de Descuento', tooltip: 'Porcentaje de Descuento',	dataIndex : 'PORC_ANTICIPO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('0 %'), hidden: false
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'MONTO_DSCTO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('0,0.00'), hidden: false
			},{
				header : 'Referencia', tooltip: 'Referencia',	dataIndex : 'CT_REFERENCIA',
				sortable : true, width : 150, hidden: false,align: 'center'
			},{
				header : 'Nombre IF', tooltip: 'Nombre IF',	dataIndex : 'NOMBRE_IF',
				sortable : true, width : 150, hidden: true,align: 'center', hideable:true
			},{
				header : 'Nombre Beneficiario', tooltip: 'Nombre Beneficiario',	dataIndex : 'NOMBRE_BENEFICIARIO',
				sortable : true, width : 150, hidden: true,align: 'center'
			},{
				header : 'Porcentaje Beneficiario', tooltip: 'Porcentaje Beneficiario',	dataIndex : 'FN_PORC_BENEFICIARIO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('0 %'), hidden: true
			},{
				header : 'Monto Beneficiario', tooltip: 'Monto Beneficiario',	dataIndex : 'MONTO_BENEFICIARIO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('0,0.00'), hidden: true
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 180,
		width: 940,
		columnLines: true,
		style: 'margin:0 auto;'
	});

	var elementosConfirma = [
		{
			xtype: 'panel',layout:'table',	width:550,	border:true,	layoutConfig:{ columns: 3 },
			defaults: {frame:false, border: true,width:165, height: 35,bodyStyle:'padding:2px'},
			items:[
				{	width:230,	frame:true,	border:false,	html:'&nbsp;'	},
				{	border:false,	frame:true,	html:'<div align="center">Moneda Nacional</div>'	},
				{	border:false,	frame:true,	html:'<div align="center">D�lares</div>'	},
				{	width:230,	html:'<div align="right">No. Total de documentos cargados</div>'	},
				{	id:'nMn',	html: '&nbsp;'	},
				{	id:'nDl',	html:'&nbsp;'	},
				{	width:230,	html:'<div align="right">Monto total Documentos Cargados Pendientes Negociables</div>'	},
				{	id:'totMnNe',	html: '&nbsp;'	},
				{	id:'totDlNe',	html: '&nbsp;'	},
				{	width:230,	html:'<div align="right">Monto total de los documentos cargados Pendientes No Negociables</div>'	},
				{	id:'totMnNo',	html: '&nbsp;'	},
				{	id:'totDlNo',	html: '&nbsp;'	},
				{	width:230,	html:'<div align="right">Monto total de los documentos cargados</div>'	},
				{	id:'totMn',	html: '&nbsp;'	},
				{	id:'totDl',	html: '&nbsp;'	},
				{	width:550,	height: 100,	colspan:3,	html:'<div align="left">Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesi�n de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales. En caso de transmitir NOTAS DE CREDITO, usted est� en el entendido de que aplicar�n a los DOCUMENTOS una vez que a la MIPYME que correspondan decida efectuar el descuento.</div>' }
			]
		},{
			xtype: 'hidden', id:'hidOperacion',	value:''
		}
	];

	var fpConfirma	=	new Ext.FormPanel({style: 'margin:0 auto;',	autoHeight:true,	bodyStyle:'padding:2px',	width:556,	border:true,	frame:false,	items:elementosConfirma,	hidden:false,
		buttons: [
			{
				id:		'botonConfirmar',
				text: 	'Confirmar',
				iconCls: 'correcto',
				handler:	function(btn) {
					NE.util.obtenerPKCS7(confirmarMantenimiento, form.textoFirmar, btn.text); //btn.text. Bajo ciertas condiciones, el texto del bot�n cambia.
				}
			},{
				text: 	'Cancelar',
				iconCls: 'rechazar',
				handler: function() {
								Ext.Msg.show({
									msg: '�Est� usted seguro de cancelar la operaci�n?',
									buttons: Ext.Msg.OKCANCEL,
									fn: processResult,
									icon: Ext.MessageBox.INFO
								});
				}
			}
		]
	});
	
	var confirmarMantenimiento = function(pkcs7, textoFirmar, operacion) {
		if (Ext.isEmpty(pkcs7)) {
			return;	//Error en la firma. Termina...
		}
		form.pkcs7 = pkcs7;
		if (operacion === 'Eliminar'){
			Ext.Msg.show({
				msg: '�Est� usted seguro de eliminar los registros seleccionados?',
				buttons: Ext.Msg.OKCANCEL,
				fn: processResultElimina,
				icon: Ext.MessageBox.INFO
			});
		}else{
			pnl.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url : '13formaMantDocPendExt.data.jsp',
				params : {
					informacion:			'Confirma',
					num_acuse:				form.num_acuse,
					cadDocSelec:			form.cadDocSelec,
					regPrenegociables:	form.regP,
					regPrenonegociables:	form.regPN,
					operacion:				Ext.getCmp('hidOperacion').getValue(),
					Pkcs7:					pkcs7,
					TextoFirmado:			textoFirmar
				},
				callback: procesaConfirma
			});
		}
	}

	var gridIndividual = new Ext.grid.GridPanel({
		store: individualData,
		columns: [
			{
				header: 'Proveedor', tooltip: 'Proveedor',	dataIndex: 'NOMPYME',
				sortable: true, width: 250, resizable: true, align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header: 'Num. Documento', tooltip: 'Num. Documento',	dataIndex: 'NUMDOCTO',
				sortable: true, width: 130, resizable: true, hidden: false,align: 'center'
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	dataIndex : 'FECEMISION',
				sortable : true, width : 110, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: false
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	dataIndex : 'FECVENC',
				sortable : true, width : 110, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: false
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'NOMMONEDA',
				sortable : true, width : 150, hidden: false,align: 'center'
			},{
				header : 'Monto', tooltip: 'Monto',	dataIndex : 'MONTO',
				sortable : true, width : 110, align: 'right', renderer: Ext.util.Format.numberRenderer('0,0.00'), hidden: false
			},{
				header: 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	dataIndex: 'NOMFACTORAJE',
				sortable: true, width: 130, resizable: true, hidden: false,align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header: 'Seleccionar',	tooltip:	'Seleccionar',	dataIndex:	'ESTATUS',
				sortable:	true,	width:	100,	resizable:	true,	hideable:	false, align:	'center',
				renderer:function(value, metadata, record, rowIndex, colIndex, store){
								var numdocto = record.get('NUMDOCTO');
								if(	value ==	"30"	||	value == "31"	){
									form.hayCheck = true;
									return '<input type="checkbox" id="selecDoc'+ rowIndex +'" name="selecDoc" value="' + numdocto + '"></input>';
								}else{
									return 'Confirmado'
								}
							}
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		columnLines: true,
		style: 'margin:0 auto;',
		view: new Ext.grid.GridView({forceFit: true}),
		bbar:{
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'bbarIndividual',
			displayInfo: true,
			store: individualData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: ['->','-',
				{
					xtype: 'button',
					text: 'Confirmar',
					id: 'btnConfirma',
					disabled: true,
					handler: function(boton, evento) {
									var conf = false;
									var ds = individualData;
									var jsonData = ds.reader.jsonData;
									Ext.each(jsonData.registros, function(item,index,arrItem){
										var chk = Ext.getDom('selecDoc'+index.toString());
										if (chk){
											if (chk.checked){
												conf = true;
												return conf;
											}
										}
									});
									if (!conf){
										Ext.Msg.alert(boton.text,'Seleccione al menos un documento');
										return;
									}
									var selecDoc = [];
									var cadDocSelec = "";
									var i=0;
									Ext.each(jsonData.registros, function(item,index,arrItem){
										var chk = Ext.getDom('selecDoc'+index.toString());
										if (chk){
											if (chk.checked){
												if(i==0){
													cadDocSelec += "'" + chk.value + "'";
												}else{
													cadDocSelec += ",'" + chk.value + "'";
												}
												i++;
											}
										}
									});
									form.cadDocSelec = cadDocSelec;
									Ext.getCmp('hidOperacion').setValue("Generar");
									pnl.el.mask('Procesando...', 'x-mask-loading');
									Ext.Ajax.request({
										url : '13formaMantDocPendExt.data.jsp',
										params : {
											informacion:'detalleConfirma',
											cadDocSelec:form.cadDocSelec,
											num_acuse:	form.num_acuse
										},
										callback: procesarDetalleConfirma
									});
					}
				},'-',{
					xtype: 'button',
					text: 'Seleccionar Todos',
					id: 'btnTodos',
					disabled: true,
					handler: function(boton, evento) {
							var ds = individualData;
							var jsonData = ds.reader.jsonData;
							Ext.each(jsonData.registros, function(item,index,arrItem){
								var chk = Ext.getDom('selecDoc'+index.toString());
								if (chk){
									if (!chk.checked){
										chk.checked = true;
									}
								}
							});
					}
				},'-',{
					xtype: 'button',
					text: 'Quitar Selecci�n',
					id: 'btnQuitar',
					disabled: true,
					handler: function(boton, evento) {
							var ds = individualData;
							var jsonData = ds.reader.jsonData;
							Ext.each(jsonData.registros, function(item,index,arrItem){
								var chk = Ext.getDom('selecDoc'+index.toString());
								if (chk){
									if (chk.checked){
										chk.checked = false;
									}
								}
							});
					}
				},'-',{
					xtype: 'button',
					text: 'Regresar',
					id: 'btnRegInd',
					handler: function(boton, evento) {
						regresar('13formaMantDocPendExt.jsp');
					}
				}
			]
		}
	});

	var elementosIndividual = [
		{
			xtype: 'panel',layout:'table',	width:700,	border:true,	layoutConfig:{ columns: 4 },
			defaults: {frame:false, border: true,width:150, height: 25,bodyStyle:'padding:5px', align:'center'},
			items:[
				{	frame:true,	border:false,	html:'<div align="center">N�mero de Acuse</div>'	},
				{	frame:true,	border:false,	html:'<div align="center">Fecha</div>'	},
				{	frame:true,	border:false,	width:300,	html:'<div align="center">Usuario</div>'	},
				{	frame:true,	border:false,	width:100,	html:'<div align="center">N�mero Doctos</div>'	},
				{	id:'disAcuse',	html:'&nbsp;'	},
				{	id:'disFecha',	html:'&nbsp;'	},
				{	id:'disUsuario',	width:300,	html:'&nbsp;'	},
				{	id:'disDoctos',	width:100,	html:'&nbsp;'	}
			]
		}
	];

	var fpIndividual=new Ext.FormPanel({style:'margin: 0 auto',	autoHeight:true,	bodyStyle:'padding:2px',	width:706,	border:true,	frame:false,	items:elementosIndividual,	hidden:false 	});

	var elementosFormaDoctosPNegociables = [
		/*
			1. N&uacute;mero de Acuse:
				<input type="text" class="formas" name="cc_acuse" size="15" value="<%=cc_acuse%>" maxlength="15">
		*/
		{
			xtype: 		'textfield',
			name: 		'cc_acuse',
			id: 			'numeroAcuse',
			fieldLabel: 'N�mero de Acuse',
			allowBlank: true,
			regex : /^[-_\w|\s]*$/i,
			hidden: 		false,
			maxLength: 	15,	
			msgTarget: 	'side',
			anchor:		'65%'
		},
		  /*
			2. Fecha de Carga:
				<input type="text" class="formas" name="fechaCargaIni" size="15" value="<%=fechaCargaIni%>" maxlength="15">
				<input type="text" class="formas" name="fechaCargaFin" size="15" value="<%=fechaCargaFin%>" maxlength="15">
			*/
		{
			xtype: 						'compositefield',
			fieldLabel: 				'Fecha de Carga',
			combineErrors: 			false,
			msgTarget: 					'side',
			items: [
				{
					xtype: 				'datefield',
					name: 				'fechaCargaIni',
					id: 					'fechaCargaIni',
					allowBlank: 		true,
					startDay: 			0,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoFinFecha: 	'fechaCargaFin',
					margins: 			'0 20 0 0'  
				},{
					xtype: 				'displayfield',
					value: 				'a',
					width: 				20
				},{
					xtype: 				'datefield',
					name: 				'fechaCargaFin',
					id: 					'fechaCargaFin',
					allowBlank: 		true,
					startDay: 			1,
					width: 				100,
					msgTarget: 			'side',
					vtype: 				'rangofecha', 
					campoInicioFecha: 'fechaCargaIni',
					margins: 			'0 20 0 0'  
				}
			]
		},
			/*
			3. Usuario
				<input type="text" name="ic_usuario" size="15" value="<%=ic_usuario%>" maxlength="15">
			*/
		{ 
			xtype: 		'textfield',
			name: 		'ic_usuario',
			id: 			'numeroUsuario',
			fieldLabel: 'Usuario',
			regex : /^[\d]*$/i,
			allowBlank: true,
			hidden: 		false,
			maxLength: 	15,	
			msgTarget: 	'side',
			anchor:		'70%'
		}
	];
	
	var fp = new Ext.form.FormPanel({
		id: 				'forma',
		width: 			420,
		title: 			'Consulta de Doctos Pendientes',
		frame: 			true,
		collapsible: 	true,
		titleCollapse: true,
		style: 			'margin: 0 auto',
		bodyStyle:		'padding:10px',
		defaults: {
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		labelWidth: 	115,
		defaultType: 	'textfield',
		items: 			elementosFormaDoctosPNegociables,
		monitorValid: 	false,
		buttons: [
			/*
			4. Boton Consultar
				<td class="celda02" align="center" width="50" height="25"><a href="javascript:consultar();" onmouseout="window.status=''; return true;" onmouseover ="window.status='Consultar'; return true;">Consultar</a></td>
				
			*/
			{
				id:				'botonConsultar',
				text: 			'Consultar',
				errorActivo:	false,
				iconCls: 		'icoBuscar',
				handler: 		function() {
					form.hayCheck = false;
					pnl.el.mask('Procesando...', 'x-mask-loading');
					Ext.Ajax.request({
						url: '13formaMantDocPendExt.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{informacion: 'Consulta'}),
						callback: procesaConsulta
					});
				}
			}
			,
			/*
			5. Boton Limpiar
				<td class="celda02" align="center" width="50" height="25"><a href="13forma8.jsp" onmouseout="window.status=''; return true;" onmouseover ="window.status='Limpiar'; return true;">Limpiar</a></td>
			*/
			{
				text: 		'Limpiar',
				iconCls: 	'icoLimpiar',
				handler: 	function() {
					regresar('13formaMantDocPendExt.jsp');
				}
			}
		]
	});

	var pnlConfirma = new Ext.Container({
		id: 		'contenedorConfirma',
		width: 	949,
		height: 	'auto',
		hidden: true,
		items: 	[
			fpConfirma,
			NE.util.getEspaciador(5),
			gridConfirmaPendNego,
			NE.util.getEspaciador(10),
			gridConfirmaPendNoNego,
			NE.util.getEspaciador(10),
			gridTotales
		]
	});

	var pnlIndividual = new Ext.Container({
		id: 		'contenedorIndividual',
		width: 	949,
		height: 	'auto',
		hidden: true,
		items: 	[
			fpIndividual,
			NE.util.getEspaciador(10),
			gridIndividual
		]
	});

	//-------------------------------- FORMA PRINCIPAL ------------------------------
	var pnl = new Ext.Container({
		id: 		'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 	949,
		height: 	'auto',
		items: 	[
			NE.util.getEspaciador(10),
			fp,
			NE.util.getEspaciador(10),
			grid,
			pnlIndividual,
			pnlConfirma
		]
	});
	
});