<!DOCTYPE html>
<%@ page import="java.util.*,
	javax.naming.*,	
	java.sql.*,
	com.jspsmart.upload.*,
	com.netro.exception.*,
	com.netro.afiliacion.*,
	com.netro.descuento.*,	
	netropology.utilerias.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%

	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
  String strPendiente = (request.getParameter("strPendiente") != null) ? request.getParameter("strPendiente") : "N";
	String strPreNegociable = (request.getParameter("strPreNegociable") != null) ? request.getParameter("strPreNegociable") : "N";

	String strFirmaManc = (request.getParameter("strFirmaManc") != null) ? request.getParameter("strFirmaManc") : "N";
	String strHash = (request.getParameter("strHash") != null) ? request.getParameter("strHash") : "N";

	String txttotdoc = (request.getParameter("txttotdoc") != null) ? request.getParameter("txttotdoc") : "0";
	String txtmtodoc = (request.getParameter("txtmtodoc") != null) ? request.getParameter("txtmtodoc") : "0";
	String txtmtomindoc = (request.getParameter("txtmtomindoc") != null) ? request.getParameter("txtmtomindoc") : "0";
	String txtmtomaxdoc = (request.getParameter("txtmtomaxdoc") != null) ? request.getParameter("txtmtomaxdoc") : "0";
	
	String txttotdocdo = (request.getParameter("txttotdocdo") != null) ? request.getParameter("txttotdocdo") : "0";
	String txtmtodocdo = (request.getParameter("txtmtodocdo") != null) ? request.getParameter("txtmtodocdo") : "0";
	String txtmtomindocdo = (request.getParameter("txtmtomindocdo") != null) ? request.getParameter("txtmtomindocdo") : "0";
	String txtmtomaxdocdo = (request.getParameter("txtmtomaxdocdo") != null) ? request.getParameter("txtmtomaxdocdo") : "0";
	
	String proceso = (request.getParameter("proceso") != null) ? request.getParameter("proceso") : "0";
	String strClaveHash = (request.getParameter("strClaveHash") != null) ? request.getParameter("strClaveHash") : "0";
	String hayErrores = (request.getParameter("hayErrores") != null) ? request.getParameter("hayErrores") : "N";
	String doctoDuplicados = (request.getParameter("doctoDuplicados") != null) ? request.getParameter("doctoDuplicados") : "N";
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	CargaDocumento CargaDocumentos = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);

	//se obtienes los campos adicionales
	Vector nombresCampo = CargaDocumentos.getCamposAdicionales(iNoCliente);
	int numeroCampos=  nombresCampo.size();
	String strCampos  ="", nomCampo1 ="", nomCampo2 ="", nomCampo3="", nomCampo4="", nomCampo5 ="";
	for (int i=0; i<nombresCampo.size(); i++) { 			
		Vector lovRegistro = (Vector) nombresCampo.get(i);
		strCampos += (String) lovRegistro.get(0);	
		if(i==0) 	nomCampo1 = (String) lovRegistro.get(1);
		if(i==1) 	nomCampo2 = (String) lovRegistro.get(1);
		if(i==2) 	nomCampo3 = (String) lovRegistro.get(1); 
		if(i==3) 	nomCampo4 = (String) lovRegistro.get(1);
		if(i==4) 	nomCampo5 = (String) lovRegistro.get(1);			
	}
	
	String  bOperaFactConMandato = "N",  bOperaFactorajeVencido  ="N", bOperaFactorajeDistribuido = "", 
	bOperaNotasCredito   ="N", bTipoFactoraje ="N",  bOperaFactorajeVencidoInfonavit  ="N"; //Fodea 042-2009 Vencimiento Infonavit
	String bValidaDuplicidad = "";
	
	Hashtable alParamEPO1 = new Hashtable();
	alParamEPO1 = BeanParamDscto.getParametrosEPO(iNoCliente,1);	
		
	if (alParamEPO1!=null) {
		bOperaFactConMandato = alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString();
		bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
		bOperaFactorajeDistribuido = alParamEPO1.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString();
		bOperaNotasCredito = alParamEPO1.get("OPERA_NOTAS_CRED").toString();
		bOperaFactorajeVencidoInfonavit =alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString();
		bValidaDuplicidad = alParamEPO1.get("VALIDA_DUPLIC").toString();
	}
	if(bOperaFactorajeVencido.equals("S") || bOperaFactorajeDistribuido.equals("S")  || bOperaFactConMandato.equals("S")) {
		bTipoFactoraje ="S";
	}
	
		// Verificar si esta habilitada la Publicacion EPO PEF
	String bestaHabilitadaPublicacionEPOPEF ="N";
	boolean 	estaHabilitadaPublicacionEPOPEF 	=  CargaDocumentos.estaHabilitadaLaPublicacionEPOPEF(iNoCliente);
	if(estaHabilitadaPublicacionEPOPEF) bestaHabilitadaPublicacionEPOPEF ="S";
	
	boolean bVenSinOperar=false;	String bVenSinOperarS ="N";
	bVenSinOperar = CargaDocumentos.publicarDocVencidos(iNoCliente);	
	if(bVenSinOperar) bVenSinOperarS ="S";
	
%>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script language="JavaScript" src="/nafin/00utils/valida.js?<%=session.getId()%>"></script>
<link rel="stylesheet" type="text/css" href="/nafin/00utils/extjs/resources/css/fileuploadfield.css"/>
<script type="text/javascript" src="/nafin/00utils/extjs/FileUploadField.js"></script>
<script type="text/javascript" src="13forma02bExt.js?<%=session.getId()%>"></script>

<%@ include file="/00utils/componente_firma.jspf" %>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%>

<form id='formParametros' name="formParametros">
	<input type="hidden" id="strPendiente" name="strPendiente" value="<%=strPendiente%>"/>	
	<input type="hidden" id="strPreNegociable" name="strPreNegociable" value="<%=strPreNegociable%>"/>
	<input type="hidden" id="strFirmaManc" name="strFirmaManc" value="<%=strFirmaManc%>"/>
	<input type="hidden" id="strHash" name="strHash" value="<%=strHash%>"/>
	<input type="hidden" id="operaNC" name="operaNC" value="<%=operaNC%>"/>
	
	<input type="hidden" id="txttotdoc" name="txttotdoc" value="<%=txttotdoc%>"/>
	<input type="hidden" id="txtmtodoc" name="txtmtodoc" value="<%=txtmtodoc%>"/>
	<input type="hidden" id="txtmtomindoc" name="txtmtomindoc" value="<%=txtmtomindoc%>"/>
	<input type="hidden" id="txtmtomaxdoc" name="txtmtomaxdoc" value="<%=txtmtomaxdoc%>"/>
	
	<input type="hidden" id="txttotdocdo" name="txttotdocdo" value="<%=txttotdocdo%>"/>
	<input type="hidden" id="txtmtodocdo" name="txtmtodocdo" value="<%=txtmtodocdo%>"/>
	<input type="hidden" id="txtmtomindocdo" name="txtmtomindocdo" value="<%=txtmtomindocdo%>"/>
	<input type="hidden" id="txtmtomaxdocdo" name="txtmtomaxdocdo" value="<%=txtmtomaxdocdo%>"/>
	
	<input type="hidden" id="proceso" name="proceso" value="<%=proceso%>"/>
	<input type="hidden" id="strClaveHash" name="strClaveHash" value="<%=strClaveHash%>"/>

	<input type="hidden" id="hayCamposAdicionales" name="hayCamposAdicionales" value="<%= String.valueOf(numeroCampos)%>"/>	
	<input type="hidden" id="nomCampo1" name="nomCampo1" value="<%=nomCampo1%>"/>	
	<input type="hidden" id="nomCampo2" name="nomCampo2" value="<%=nomCampo2%>"/>
	<input type="hidden" id="nomCampo3" name="nomCampo3" value="<%=nomCampo3%>"/>	
	<input type="hidden" id="nomCampo4" name="nomCampo4" value="<%=nomCampo4%>"/>	
	<input type="hidden" id="nomCampo5" name="nomCampo5" value="<%=nomCampo5%>"/>	
	<input type="hidden" id="operaFVPyme" name="operaFVPyme" value="<%=operaFVPyme%>"/>	
	<input type="hidden" id="bOperaFactorajeVencido" name="bOperaFactorajeVencido" value="<%=bOperaFactorajeVencido%>"/>	
	<input type="hidden" id="bOperaFactorajeVencido" name="bOperaFactorajeVencido" value="<%=bOperaFactorajeVencido%>"/>	
	<input type="hidden" id="bOperaFactConMandato" name="bOperaFactConMandato" value="<%=bOperaFactConMandato%>"/>	
	<input type="hidden" id="bOperaFactorajeVencidoInfonavit" name="bOperaFactorajeVencidoInfonavit" value="<%=bOperaFactorajeVencidoInfonavit%>"/>	
	<input type="hidden" id="bOperaFactorajeDistribuido" name="bOperaFactorajeDistribuido" value="<%=bOperaFactorajeDistribuido%>"/>	
	<input type="hidden" id="bestaHabilitadaPublicacionEPOPEF" name="bestaHabilitadaPublicacionEPOPEF" value="<%=bestaHabilitadaPublicacionEPOPEF%>"/>	
	<input type="hidden" id="bOperaFactConMandato" name="bOperaFactConMandato" value="<%=bOperaFactConMandato%>"/>	
	<input type="hidden" id="bValidaDuplicidad" name="bValidaDuplicidad" value="<%=bValidaDuplicidad%>"/>
	
	<input type="hidden" id="hayErrores" name="hayErrores" value="<%=hayErrores%>"/>	
	<input type="hidden" id="bVenSinOperarS" name="bVenSinOperarS" value="<%=bVenSinOperarS%>"/>	
	<input type="hidden" id="doctoDuplicados" name="doctoDuplicados" value="<%=doctoDuplicados%>"/>		
	
</form>
<form id='formAux' name="formAux" target='_new'></form>
</body>
</html>
