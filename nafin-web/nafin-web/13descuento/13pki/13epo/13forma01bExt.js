
Ext.onReady(function() {

	var proceso  = Ext.getDom('proceso').value;
	var strPendiente  = Ext.getDom('strPendiente').value;
	var strPreNegociable  = Ext.getDom('strPreNegociable').value;
	var operaNC = Ext.getDom('operaNC').value;
	var bVenSinOperarS =  Ext.getDom('bVenSinOperarS').value;
	var hidCtrlNumDoctosMN = Ext.getDom('hidCtrlNumDoctosMN').value;
	var hidCtrlMontoTotalMN =  Ext.getDom('hidCtrlMontoTotalMN').value;
	var hidCtrlNumDoctosDL =  Ext.getDom('hidCtrlNumDoctosDL').value;
	var hidCtrlMontoTotalDL =  Ext.getDom('hidCtrlMontoTotalDL').value;
		
	var hayCamposAdicionales  = Ext.getDom('hayCamposAdicionales').value;
	var nomCampo1 = Ext.getDom('nomCampo1').value;
	var nomCampo2 = Ext.getDom('nomCampo2').value;
	var nomCampo3 = Ext.getDom('nomCampo3').value;
	var nomCampo4 = Ext.getDom('nomCampo4').value;
	var nomCampo5 = Ext.getDom('nomCampo5').value;
	var operaFVPyme = Ext.getDom('operaFVPyme').value;
	var bOperaFactorajeVencido = Ext.getDom('bOperaFactorajeVencido').value;
	var bOperaFactorajeVencido = Ext.getDom('bOperaFactorajeVencido').value;
	var bOperaFactConMandato = Ext.getDom('bOperaFactConMandato').value;
	var bOperaFactorajeVencidoInfonavit = Ext.getDom('bOperaFactorajeVencidoInfonavit').value;
	var bOperaFactorajeDistribuido = Ext.getDom('bOperaFactorajeDistribuido').value;
	var sPubEPOPEFFlagVal = Ext.getDom('sPubEPOPEFFlagVal').value;
	var bOperaFactConMandato = Ext.getDom('bOperaFactConMandato').value;
	var bValidaDuplicidad = Ext.getDom('bValidaDuplicidad').value;
	
	var mensaje;
	var leyenda;
	var mleyenda;
	
	if(strPendiente=='S'){
		mensaje = 	"Transmitir documentos pendientes negociables";
	}else if(strPreNegociable =='S'){
		mensaje ="Transmitir documentos Pre negociables ";
	}else  {
		mensaje ="Transmitir documentos negociables";	
	}
	
	if(operaNC=='S') {
		 leyenda = '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
		'<tr><td class="formas" style="text-align: justify;" colspan="3">Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesi�n de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales. En caso de transmitir NOTAS DE CREDITO, usted est� en el entendido de que aplicar�n a los DOCUMENTOS una vez que a la MIPYME '+
		'que correspondan decida efectuar el descuento.	</td></tr></table>';
		mleyenda ='Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesi�n de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales. En caso de transmitir NOTAS DE CREDITO, usted est� en el entendido de que aplicar�n a los DOCUMENTOS una vez que a la MIPYME '+
		'que correspondan decida efectuar el descuento.';
			
	}else {
		leyenda = '<table width="500" cellpadding="3" cellspacing="1" border="0">'+
		'<tr><td class="formas" style="text-align: justify;"  colspan="3">Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesi�n de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales.</td></tr>'+ 
		'</table>';
		mleyenda = 'Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad haciendo negociables los DOCUMENTOS que constan en el mismo, aceptando de esta forma la cesi�n de los derechos de cobro de las MIPYMES al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales.';	
	}
	
	
	var procesarSuccessFailureAcuse =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsondeAcuse = Ext.util.JSON.decode(response.responseText);			
			if (jsondeAcuse != null){
				 var acuse  = jsondeAcuse.acuse;
				 var mensaje  = jsondeAcuse.mensajeError;			
				 var proceso = jsondeAcuse.proceso;
				 var recibo = jsondeAcuse.recibo;
				 var acuseFormateado  = jsondeAcuse.acuseFormateado;
				 var hidFechaCarga  = jsondeAcuse.hidFechaCarga;
				 var hidHoraCarga  = jsondeAcuse.hidHoraCarga;
				 	var usuario  = jsondeAcuse.usuario;
			
	
				if(jsondeAcuse.mostrarAcuse=='S') {
					var parametros = "pantalla= Acuse"+
														"&proceso="+proceso+
														"&acuse="+acuse+
														"&acuseFormateado="+acuseFormateado+
														"&mensaje="+encodeURIComponent(mensaje)+
														"&recibo="+recibo+
														"&hidHoraCarga="+hidHoraCarga+
														"&hidFechaCarga="+hidFechaCarga+
														"&usuario="+usuario+													
														"&cancelacion=A"+
														"&strPendiente="+strPendiente+
														"&strPreNegociable="+strPreNegociable+
														"&bVenSinOperarS="+bVenSinOperarS;	
														
					document.location.href = "13forma01cExt.jsp?"+parametros;	
			
				} else 	if(jsondeAcuse.mostrarAcuse=='N') {
					document.location.href = "13forma01cExt.jsp?cancelacion=N&mensaje="+mensaje+"&strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable; 									
				}
				
			}					
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
        
	var generaTextoFormat = function(){
		var monedaNacional ="";
		var dolares ="";
		var titulos = '\n Nombre Proveedor|N�mero de Documento|Fecha Emisi�n|Fecha Vencimiento';
		if(operaFVPyme =='S') {
				titulos +'|Fecha Vencimiento Proveedor';
		}
		titulos +='|Moneda|Tipo Factoraje|Monto|Porcentaje de Descuento|Monto a Descontar|Referencia';
		if(bOperaFactorajeVencido =='S' || bOperaFactConMandato =='S'  ||  bOperaFactorajeVencidoInfonavit =='S' ) {					
				titulos +='|Nombre IF';
		}
		if(bOperaFactorajeDistribuido  =='S' || bOperaFactorajeVencidoInfonavit   =='S') {
				titulos += '|Nombre Beneficiario|Porcentaje Beneficiario|Monto Beneficiario';
		}			
		if(hayCamposAdicionales=='1'){ 	titulos +='|'+nomCampo1; 	}
		if(hayCamposAdicionales=='2'){ 	titulos +='|'+nomCampo1+'|'+nomCampo2; 	}
		if(hayCamposAdicionales=='3'){ 	titulos +='|'+nomCampo1+'|'+nomCampo2+'|'+nomCampo3; 	}
		if(hayCamposAdicionales=='4'){ 	titulos +='|'+nomCampo1+'|'+nomCampo2+'|'+nomCampo3+'|'+nomCampo4;  	}
		if(hayCamposAdicionales=='5'){ 	titulos +='|'+nomCampo1+'|'+nomCampo2+'|'+nomCampo3+'|'+nomCampo4+'|'+nomCampo5;	}
		if(sPubEPOPEFFlagVal  =='S') {
				titulos += '|Fecha de Recepci�n de Bienes y Servicios|Tipo de Compra (procedimiento)|Clasificador por Objeto del Gasto|Plazo Maximo';
		}
		if(bOperaFactConMandato =='S') {
				titulos += '|Mandante';
		}
		if(bValidaDuplicidad =='S') {
				titulos += '|Notificado como Posible Duplicado';
		}
		titulos +='\n';		
												
		var negociables =titulos;		
		var noNegociables =titulos;
		var vencidos =titulos;

		var gridControl = Ext.getCmp('gridControl');		
		var store = gridControl.getStore();
				store.each(function(record) {
				 monedaNacional += record.data['INFORMACION'] +": "+record.data['MONEDA_NACIONAL']+" \n ";
				 dolares += record.data['INFORMACION'] +" DL : "+record.data['MONEDA_DL']+" \n ";			 
		});
		
		//negociables
		var gridConsulta = Ext.getCmp('gridConsulta');		
		var store = gridConsulta.getStore();
		store.each(function(record) {
				negociables +=record.data['NOMBRE_PROVEEDOR']+"|"+ record.data['NUM_DOCUMENTO']+"|"+ record.data['FECHA_EMISION']+"|"+ record.data['FECHA_VENCIMIENTO'];
				if(operaFVPyme =='S') {
						negociables +="|"+ record.data['FECHA_VENC_PROVEEDOR'] ; 
				}
				negociables +="|"+ record.data['MONEDA']+"|"+ record.data['TIPO_FACTORAJE']+"|"+ record.data['MONTO']+"|"+ record.data['PORC_DESCUENTO']+"|"+ record.data['MONTO_DESCONTAR']+"|"+ record.data['REFERENCIA'];
				if(bOperaFactorajeVencido =='S' || bOperaFactConMandato =='S'  ||  bOperaFactorajeVencidoInfonavit =='S' ) {	
						negociables +="|"+ record.data['NOMBRE_IF']; 
				}
				if(bOperaFactorajeDistribuido  =='S' || bOperaFactorajeVencidoInfonavit   =='S') {
						negociables +="|"+ record.data['NOMBRE_BENEFICIARIO'] 	+"|"+ record.data['PORC_BENEFICIARIO'] +"|"+ record.data['MONTO_BENEFICIARIO'];
				}
				if(hayCamposAdicionales=='1'){  	negociables +="|"+ record.data['CAMPO1'];  }
				if(hayCamposAdicionales=='2'){  	negociables +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'];  }
				if(hayCamposAdicionales=='3'){  	negociables +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'] +"|"+ record.data['CAMPO3'] ;  }
				if(hayCamposAdicionales=='4'){  	negociables +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'] +"|"+ record.data['CAMPO3'] +"|"+ record.data['CAMPO4'] ;  }
				if(hayCamposAdicionales=='5'){  	negociables +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'] +"|"+ record.data['CAMPO3'] +"|"+ record.data['CAMPO4'] +"|"+ record.data['CAMPO5'] ;  }
				
				if(sPubEPOPEFFlagVal  =='S') {
						negociables +="|"+ record.data['FECHA_RECEPCION']+"|"+ record.data['TIPO_COMPRA']+"|"+ record.data['CLASIFICADOR']+"|"+ record.data['CLASIFICADOR'] 	+"|"+ record.data['PLAZO_MAXIMO'];
				}
				if(bOperaFactConMandato =='S') {
						negociables +="|"+ record.data['MANDANTE'];
				}
				
				if(bValidaDuplicidad =='S') {
						negociables +="|"+ record.data['DUPLICADO'];
				}
				negociables +="\n";
		});

		//noNegociables
		var gridConsulta2 = Ext.getCmp('gridConsulta2');		
		var store = gridConsulta2.getStore();
				store.each(function(record) {
				noNegociables +=record.data['NOMBRE_PROVEEDOR']+"|"+ record.data['NUM_DOCUMENTO']+"|"+ record.data['FECHA_EMISION']+"|"+ record.data['FECHA_VENCIMIENTO'];
				if(operaFVPyme =='S') {
						noNegociables +="|"+ record.data['FECHA_VENC_PROVEEDOR'] ; 
				}
				negociables +="|"+ record.data['MONEDA']+"|"+ record.data['TIPO_FACTORAJE']+"|"+ record.data['MONTO']+"|"+ record.data['PORC_DESCUENTO']+"|"+ record.data['MONTO_DESCONTAR']+"|"+ record.data['REFERENCIA'];
				if(bOperaFactorajeVencido =='S' || bOperaFactConMandato =='S'  ||  bOperaFactorajeVencidoInfonavit =='S' ) {	
						noNegociables +="|"+ record.data['NOMBRE_IF']; 
				}
				if(bOperaFactorajeDistribuido  =='S' || bOperaFactorajeVencidoInfonavit   =='S') {
						noNegociables +="|"+ record.data['NOMBRE_BENEFICIARIO'] 	+"|"+ record.data['PORC_BENEFICIARIO'] +"|"+ record.data['MONTO_BENEFICIARIO'];
				}
				if(hayCamposAdicionales=='1'){  	noNegociables +="|"+ record.data['CAMPO1'];  }
				if(hayCamposAdicionales=='2'){  	noNegociables +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'];  }
				if(hayCamposAdicionales=='3'){  	noNegociables +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'] +"|"+ record.data['CAMPO3'] ;  }
				if(hayCamposAdicionales=='4'){  	noNegociables +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'] +"|"+ record.data['CAMPO3'] +"|"+ record.data['CAMPO4'] ;  }
				if(hayCamposAdicionales=='5'){  	noNegociables +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'] +"|"+ record.data['CAMPO3'] +"|"+ record.data['CAMPO4'] +"|"+ record.data['CAMPO5'] ;  }
				
				if(sPubEPOPEFFlagVal  =='S') {
						noNegociables +="|"+ record.data['FECHA_RECEPCION']+"|"+ record.data['TIPO_COMPRA']+"|"+ record.data['CLASIFICADOR']+"|"+ record.data['CLASIFICADOR'] 	+"|"+ record.data['PLAZO_MAXIMO'];
				}
				if(bOperaFactConMandato =='S') {
						noNegociables +="|"+ record.data['MANDANTE'];
				}
				if(bValidaDuplicidad =='S') {
						noNegociables +="|"+ record.data['DUPLICADO'];
				}
				noNegociables +="\n";
		});
		//vencidos
		var gridConsulta3 = Ext.getCmp('gridConsulta3');		
		var store = gridConsulta3.getStore();
				store.each(function(record) {
				vencidos +=record.data['NOMBRE_PROVEEDOR']+"|"+ record.data['NUM_DOCUMENTO']+"|"+ record.data['FECHA_EMISION']+"|"+ record.data['FECHA_VENCIMIENTO'];
				if(operaFVPyme =='S') {
						vencidos +="|"+ record.data['FECHA_VENC_PROVEEDOR'] ; 
				}
				vencidos +="|"+ record.data['MONEDA']+"|"+ record.data['TIPO_FACTORAJE']+"|"+ record.data['MONTO']+"|"+ record.data['PORC_DESCUENTO']+"|"+ record.data['MONTO_DESCONTAR']+"|"+ record.data['REFERENCIA'];
				if(bOperaFactorajeVencido =='S' || bOperaFactConMandato =='S'  ||  bOperaFactorajeVencidoInfonavit =='S' ) {	
						vencidos +="|"+ record.data['NOMBRE_IF']; 
				}
				if(bOperaFactorajeDistribuido  =='S' || bOperaFactorajeVencidoInfonavit   =='S') {
						vencidos +="|"+ record.data['NOMBRE_BENEFICIARIO'] 	+"|"+ record.data['PORC_BENEFICIARIO'] +"|"+ record.data['MONTO_BENEFICIARIO'];
				}
				if(hayCamposAdicionales=='1'){  	vencidos +="|"+ record.data['CAMPO1'];  }
				if(hayCamposAdicionales=='2'){  	vencidos +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'];  }
				if(hayCamposAdicionales=='3'){  	vencidos +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'] +"|"+ record.data['CAMPO3'] ;  }
				if(hayCamposAdicionales=='4'){  	vencidos +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'] +"|"+ record.data['CAMPO3'] +"|"+ record.data['CAMPO4'] ;  }
				if(hayCamposAdicionales=='5'){  	vencidos +="|"+ record.data['CAMPO1']+"|"+ record.data['CAMPO2'] +"|"+ record.data['CAMPO3'] +"|"+ record.data['CAMPO4'] +"|"+ record.data['CAMPO5'] ;  }
				
				if(sPubEPOPEFFlagVal  =='S') {
						vencidos +="|"+ record.data['FECHA_RECEPCION']+"|"+ record.data['TIPO_COMPRA']+"|"+ record.data['CLASIFICADOR']+"|"+ record.data['CLASIFICADOR'] 	+"|"+ record.data['PLAZO_MAXIMO'];
				}
				if(bOperaFactConMandato =='S') {
						vencidos +="|"+ record.data['MANDANTE'];
				}
				if(bValidaDuplicidad =='S') {
						vencidos +="|"+ record.data['DUPLICADO'];
				}
				vencidos +="\n";
		});
						
		var totales ="";
		var gridTotales = Ext.getCmp('gridTotales');		
		var store = gridTotales.getStore();
				store.each(function(record) {
				 totales += record.data['INFORMACION'] +": "+record.data['TOTAL_MONTO']+"  "+record.data['TOTAL_MONTO_DESC']+" \n ";
		});
		
		return textoFirmar = monedaNacional +dolares+mleyenda+negociables+noNegociables+vencidos +totales;
	}
        
        
	var confirmarCarga = function(pkcs7, textoFirmaCarga){
            
		if (Ext.isEmpty(pkcs7)) {
			Ext.getCmp("btnTerminaCap").enable();	
			return;
		}else  {			
			Ext.getCmp("btnTerminaCap").disable();		
			
			Ext.Ajax.request({
					url : '13forma01Ext.data.jsp',
					params : {
							pkcs7: pkcs7,
							textoFirmado: textoFirmaCarga,
							informacion: 'ConfirmaCarga',
							hidCtrlNumDoctosMN:hidCtrlNumDoctosMN,
							hidCtrlMontoTotalMN:hidCtrlMontoTotalMN,
							hidCtrlNumDoctosDL:hidCtrlNumDoctosDL,
							hidCtrlMontoTotalDL:hidCtrlMontoTotalDL,
							proceso:proceso,
							strPendiente:strPendiente,
							strPreNegociable:strPreNegociable
					},
					callback: procesarSuccessFailureAcuse
			});
		}
	}
        
	// proceso para  generar el acuse 
	var procesarTerminaCaptura =  function(opts, success, response) {	
		
		NE.util.obtenerPKCS7(confirmarCarga, generaTextoFormat());

	}
		
		//Elimina el registro 
	var procesarSuccessFailureCancelar =  function(opts, success, response) {		
			//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {				
			document.location.href  = "13forma01cExt.jsp?cancelacion=S"+"&strPendiente="+strPendiente+"&strPreNegociable="+strPreNegociable;;				
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//cancelar la captura de documentos
	function procesarCancelar(opts, success, response) {
		if(!confirm("�Est� seguro de querer cancelar la operaci�n?")) {
			return;
		}else {			
			Ext.Ajax.request({
			url : '13forma01Ext.data.jsp',
				params : {
					informacion: 'Cancelar',
					proceso:proceso				
				},
				callback: procesarSuccessFailureCancelar
			});	
		}	
	}
	
	//Contenedor para la pantalla de Carga Inicial
	var fpConfirmar = new Ext.Container({
		layout: 'table',
		id: 'fpConfirmar',	
		layoutConfig: {
			columns: 2
		},
		width:	'150',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [
		{
			xtype: 'button',
			text: mensaje,
			id: 'btnTerminaCap',
			iconCls: 'icoAceptar',
			handler: procesarTerminaCaptura
		},
		{
			xtype: 'button',
			text: 'Cancelar',			
			iconCls: 'icoLimpiar',
			id: 'btnCancelar',			
			handler: procesarCancelar
		}
	]
	});
	
	
	var leyendaLegal = new Ext.Container({
		layout: 'table',
		id: 'leyendaLegal',		
		layoutConfig: {
			columns: 20
		},
		width:	'500',
		heigth:	'auto',
		style: 'margin:0 auto;',
		items: [			
			{ 
				xtype:   'label',  
				html:		leyenda, 
				cls:		'x-form-item', 
				style: { 
					width:   		'700%', 
					textAlign: 		'left'
				} 
			}			
		]
	});
	
	
	var procesarConsultaData3 = function(store, arrRegistros, opts) 	{
		var jsonData = store.reader.jsonData;			
		
		if (arrRegistros != null) {
			if (!gridConsulta3.isVisible()) {
			gridConsulta3.show();
			}						
			hidMontoTotalMN = jsonData.hidMontoTotalMN;
			hidNumDoctosActualesMN = jsonData.hidNumDoctosActualesMN;
			hidMontoTotalDL = jsonData.hidMontoTotalDL;
			hidNumDoctosActualesDL = jsonData.hidNumDoctosActualesDL;
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;					
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta3.getColumnModel();			
			var el = gridConsulta3.getGridEl();				
			
			if(store.getTotalCount() > 0) {			
				if(jsonData.operaFVPyme =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEEDOR'), false);	
				}
				if(jsonData.bOperaFactorajeVencido =='S' || jsonData.bOperaFactConMandato =='S'  ||  jsonData.bOperaFactorajeVencidoInfonavit =='S' ) {					
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);	
				}				
				if(jsonData.bOperaFactorajeDistribuido  =='S' || jsonData.bOperaFactorajeVencidoInfonavit   =='S') {				
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'), false);	
				}

				if(jsonData.sPubEPOPEFFlagVal  =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('FECHA_RECEPCION'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COMPRA'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICADOR'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_MAXIMO'), false);	
				}
				if(jsonData.bOperaFactConMandato =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('MANDANTE'), false);	
				}
				if(jsonData.bValidaDuplicidad =='S') {
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('DUPLICADO'), false);	
				}
				if(hayCamposAdicionales=='0'){
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.nomCampo4);
					gridConsulta3.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.nomCampo5);						
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta3.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}			
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}

	
	var procesarConsultaData2 = function(store, arrRegistros, opts) 	{	
		var jsonData = store.reader.jsonData;									
		if (arrRegistros != null) {
			if (!gridConsulta2.isVisible()) {
			gridConsulta2.show();
			}						
			hidMontoTotalMN = jsonData.hidMontoTotalMN;
			hidNumDoctosActualesMN = jsonData.hidNumDoctosActualesMN;
			hidMontoTotalDL = jsonData.hidMontoTotalDL;
			hidNumDoctosActualesDL = jsonData.hidNumDoctosActualesDL;
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;					
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta2.getColumnModel();			
			var el = gridConsulta2.getGridEl();	
			
			if(store.getTotalCount() > 0) {			
				if(jsonData.operaFVPyme =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEEDOR'), false);	
				}
				if(jsonData.bOperaFactorajeVencido =='S' || jsonData.bOperaFactConMandato =='S'  ||  jsonData.bOperaFactorajeVencidoInfonavit =='S' ) {					
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);	
				}				
				if(jsonData.bOperaFactorajeDistribuido  =='S' || jsonData.bOperaFactorajeVencidoInfonavit   =='S') {				
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'), false);	
				}

				if(jsonData.sPubEPOPEFFlagVal  =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('FECHA_RECEPCION'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COMPRA'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICADOR'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_MAXIMO'), false);	
				}
				if(jsonData.bOperaFactConMandato =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('MANDANTE'), false);	
				}
				if(jsonData.bValidaDuplicidad =='S') {
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('DUPLICADO'), false);	
				}
				if(hayCamposAdicionales=='0'){
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.nomCampo4);
					gridConsulta2.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.nomCampo5);						
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta2.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}			
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
	var procesarConsultaData = function(store, arrRegistros, opts) 	{	
		
		var jsonData = store.reader.jsonData;									
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
			gridConsulta.show();
			}						
			hidMontoTotalMN = jsonData.hidMontoTotalMN;
			hidNumDoctosActualesMN = jsonData.hidNumDoctosActualesMN;
			hidMontoTotalDL = jsonData.hidMontoTotalDL;
			hidNumDoctosActualesDL = jsonData.hidNumDoctosActualesDL;
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;					
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();			
			var el = gridConsulta.getGridEl();				
			
			if(store.getTotalCount() > 0) {			
				if(jsonData.operaFVPyme =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEEDOR'), false);	
				}
				if(jsonData.bOperaFactorajeVencido =='S' || jsonData.bOperaFactConMandato =='S'  ||  jsonData.bOperaFactorajeVencidoInfonavit =='S' ) {					
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);	
				}				
				if(jsonData.bOperaFactorajeDistribuido  =='S' || jsonData.bOperaFactorajeVencidoInfonavit   =='S') {				
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PORC_BENEFICIARIO'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'), false);	
				}
				if(jsonData.sPubEPOPEFFlagVal  =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_RECEPCION'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COMPRA'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICADOR'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_MAXIMO'), false);	
				}
				if(jsonData.bOperaFactConMandato =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MANDANTE'), false);	
				}
				if(jsonData.bValidaDuplicidad =='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DUPLICADO'), false);	
				}
				
				if(hayCamposAdicionales=='0'){
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.nomCampo4);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.nomCampo5);						
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}			
				el.unmask();					
			} else {									
				el.mask('No se encontr� ning�n registro', 'x-mask');				
			}
		}
	}
		
	//para los totales del grid Nomal 
	var totalesControl = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ControlTotales'	
		},								
		fields: [			
			{name: 'INFORMACION' , mapping: 'INFORMACION'},
			{name: 'MONEDA_NACIONAL',  mapping: 'MONEDA_NACIONAL'},
			{name: 'MONEDA_DL' , mapping: 'MONEDA_DL' }						
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var gridControl = new Ext.grid.EditorGridPanel({	
		id: 'gridControl',
		store: totalesControl,	
		title: '',			
		columns: [	
			{
				header: '',
				dataIndex: 'INFORMACION',
				width: 350,
				align: 'left'				
			},
			{
				header: 'Moneda Nacional',
				dataIndex: 'MONEDA_NACIONAL',
				width: 150,
				align: 'right'				
			},
			{
				header: 'D�lares',
				dataIndex: 'MONEDA_DL',
				width: 150,
				align: 'right'				
			}		
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 140,
		width: 655,		
		style: 'margin:0 auto;',
		frame: false
	});
	
	
	//para los totales del grid Nomal 
	var totalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'TotalesPreAcuse1'	
		},								
		fields: [			
			{name: 'INFORMACION' , mapping: 'INFORMACION'},		
			{name: 'TOTAL_MONTO' ,  mapping: 'TOTAL_MONTO' },
			{name: 'TOTAL_MONTO_DESC',   mapping: 'TOTAL_MONTO_DESC'}				
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	
	var gridTotales = new Ext.grid.EditorGridPanel({	
		id: 'gridTotales',
		store: totalesData,
		margins: '20 0 0 0',
		align: 'center',
		columns: [	
			{
				header: ' ',
				dataIndex: 'INFORMACION',
				width: 350,
				align: 'right'				
			},		
			{
				header: 'Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'right'				
			},
			{
				header: 'Monto a Descontar',
				dataIndex: 'TOTAL_MONTO_DESC',
				width: 150,
				align: 'right'				
			},
			{
				header: '',
				dataIndex: '',
				width: 245,
				align: 'right'			
			}		
		],
		height: 220,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	
	var consultaData3 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaPreAcuse3'
		},
		fields: [						
			{name: 'NOMBRE_PROVEEDOR'},	
			{name: 'NUM_DOCUMENTO'},	
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'FECHA_VENC_PROVEEDOR'},	
			{name: 'IC_MONEDA'},	
			{name: 'MONEDA'},	
			{name: 'TIPO_FACTORAJE'},	
			{name: 'MONTO'},	
			{name: 'PORC_DESCUENTO'},	
			{name: 'MONTO_DESCONTAR'},			
			{name: 'REFERENCIA'},	
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_BENEFICIARIO'},	
			{name: 'PORC_BENEFICIARIO'},	
			{name: 'MONTO_BENEFICIARIO'},	
			{name: 'CAMPO1'},	
			{name: 'CAMPO2'},	
			{name: 'CAMPO3'},	
			{name: 'CAMPO4'},	
			{name: 'CAMPO5'},	
			{name: 'FECHA_RECEPCION'},	
			{name: 'TIPO_COMPRA'},	
			{name: 'CLASIFICADOR'},	
			{name: 'PLAZO_MAXIMO'},	
			{name: 'MANDANTE'},
			{name: 'CONSECUTIVO'},
			{name: 'IC_PYME'},
			{name: 'IC_PROCESO'},
			{name: 'ESTATUS'},
			{name: 'DUPLICADO'}
			
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData3,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData3(null, null, null);						
					}
				}
			}			
	});
	
	//var gridConsulta = new Ext.grid.EditorGridPanel({
	var gridConsulta3 = new Ext.grid.GridPanel({
		id: 'gridConsulta3',
		title: 'Carga de Documentos',		
		store: consultaData3,	
		columns: [
			{							
				header : 'ESTATUS',
				tooltip: 'ESTATUS',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				hidden: true,
				resizable: true,			
				align: 'left'	
			},	
			{							
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUM_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex : 'FECHA_EMISION',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento Proveedor',
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex : 'FECHA_VENC_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				hidden: true
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORC_DESCUENTO',
				width : 150,
				sortable : true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')			
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCONTAR',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},		
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Nombre Beneficiario',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'NOMBRE_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Porcentaje Beneficiario',
				tooltip: 'Porcentaje Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Monto Beneficiario',
				tooltip: 'Monto Beneficiario',
				dataIndex : 'MONTO_BENEFICIARIO',
				hidden: true,
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Campo1',
				tooltip: 'Campo1',
				dataIndex : 'CAMPO1',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo2',
				tooltip: 'Campo2',
				dataIndex : 'CAMPO2',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Campo3',
				tooltip: 'Campo3',
				dataIndex : 'CAMPO3',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo4',
				tooltip: 'Campo4',
				dataIndex : 'CAMPO4',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo5',
				tooltip: 'Campo5',
				dataIndex : 'CAMPO5',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Fecha de Recepci�n de Bienes y Servicios',
				tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
				dataIndex : 'FECHA_RECEPCION',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Tipo de Compra (procedimiento)',
				tooltip: 'Tipo de Compra (procedimiento)',
				dataIndex : 'TIPO_COMPRA',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Clasificador por Objeto del Gasto',
				tooltip: 'Clasificador por Objeto del Gasto',
				dataIndex : 'CLASIFICADOR',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Plazo M�ximo ',
				tooltip: 'Plazo M�ximo ',
				dataIndex : 'PLAZO_MAXIMO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'MANDANTE',
				width : 150,
				align: 'center',
				hidden: true,				
				sortable : false
			},
			{							
				header : 'Notificado como <br>posible Duplicado',
				tooltip: 'Notificado como posible Duplicado',
				dataIndex : 'DUPLICADO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	
	//
	var consultaData2 = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaPreAcuse2'
		},
		fields: [						
			{name: 'NOMBRE_PROVEEDOR'},	
			{name: 'NUM_DOCUMENTO'},	
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'FECHA_VENC_PROVEEDOR'},	
			{name: 'IC_MONEDA'},	
			{name: 'MONEDA'},	
			{name: 'TIPO_FACTORAJE'},	
			{name: 'MONTO'},	
			{name: 'PORC_DESCUENTO'},	
			{name: 'MONTO_DESCONTAR'},			
			{name: 'REFERENCIA'},	
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_BENEFICIARIO'},	
			{name: 'PORC_BENEFICIARIO'},	
			{name: 'MONTO_BENEFICIARIO'},	
			{name: 'CAMPO1'},	
			{name: 'CAMPO2'},	
			{name: 'CAMPO3'},	
			{name: 'CAMPO4'},	
			{name: 'CAMPO5'},	
			{name: 'FECHA_RECEPCION'},	
			{name: 'TIPO_COMPRA'},	
			{name: 'CLASIFICADOR'},	
			{name: 'PLAZO_MAXIMO'},	
			{name: 'MANDANTE'},
			{name: 'CONSECUTIVO'},
			{name: 'IC_PYME'},
			{name: 'IC_PROCESO'},
			{name: 'ESTATUS'},
			{name: 'DUPLICADO'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData2,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData2(null, null, null);						
					}
				}
			}			
	});
	
	//var gridConsulta = new Ext.grid.EditorGridPanel({
	var gridConsulta2 = new Ext.grid.GridPanel({
		id: 'gridConsulta2',
		title: 'Carga de Documentos',		
		store: consultaData2,	
		columns: [
			{							
				header : 'ESTATUS',
				tooltip: 'ESTATUS',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left'	
			},	
			{							
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUM_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex : 'FECHA_EMISION',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento Proveedor',
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex : 'FECHA_VENC_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				hidden: true
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORC_DESCUENTO',
				width : 150,
				sortable : true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')			
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCONTAR',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},		
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Nombre Beneficiario',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'NOMBRE_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Porcentaje Beneficiario',
				tooltip: 'Porcentaje Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Monto Beneficiario',
				tooltip: 'Monto Beneficiario',
				dataIndex : 'MONTO_BENEFICIARIO',
				hidden: true,
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Campo1',
				tooltip: 'Campo1',
				dataIndex : 'CAMPO1',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo2',
				tooltip: 'Campo2',
				dataIndex : 'CAMPO2',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Campo3',
				tooltip: 'Campo3',
				dataIndex : 'CAMPO3',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo4',
				tooltip: 'Campo4',
				dataIndex : 'CAMPO4',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo5',
				tooltip: 'Campo5',
				dataIndex : 'CAMPO5',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Fecha de Recepci�n de Bienes y Servicios',
				tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
				dataIndex : 'FECHA_RECEPCION',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Tipo de Compra (procedimiento)',
				tooltip: 'Tipo de Compra (procedimiento)',
				dataIndex : 'TIPO_COMPRA',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Clasificador por Objeto del Gasto',
				tooltip: 'Clasificador por Objeto del Gasto',
				dataIndex : 'CLASIFICADOR',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Plazo M�ximo ',
				tooltip: 'Plazo M�ximo ',
				dataIndex : 'PLAZO_MAXIMO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'MANDANTE',
				width : 150,
				align: 'center',
				hidden: true,				
				sortable : false
			},
			{							
				header : 'Notificado como <br>posible Duplicado',
				tooltip: 'Notificado como posible Duplicado',
				dataIndex : 'DUPLICADO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma01Ext.data.jsp',
		baseParams: {
			informacion: 'ConsultaPreAcuse1'
		},
		fields: [						
			{name: 'NOMBRE_PROVEEDOR'},	
			{name: 'NUM_DOCUMENTO'},	
			{name: 'FECHA_EMISION'},	
			{name: 'FECHA_VENCIMIENTO'},	
			{name: 'FECHA_VENC_PROVEEDOR'},	
			{name: 'IC_MONEDA'},	
			{name: 'MONEDA'},	
			{name: 'TIPO_FACTORAJE'},	
			{name: 'MONTO'},	
			{name: 'PORC_DESCUENTO'},	
			{name: 'MONTO_DESCONTAR'},			
			{name: 'REFERENCIA'},	
			{name: 'NOMBRE_IF'},	
			{name: 'NOMBRE_BENEFICIARIO'},	
			{name: 'PORC_BENEFICIARIO'},	
			{name: 'MONTO_BENEFICIARIO'},	
			{name: 'CAMPO1'},	
			{name: 'CAMPO2'},	
			{name: 'CAMPO3'},	
			{name: 'CAMPO4'},	
			{name: 'CAMPO5'},	
			{name: 'FECHA_RECEPCION'},	
			{name: 'TIPO_COMPRA'},	
			{name: 'CLASIFICADOR'},	
			{name: 'PLAZO_MAXIMO'},	
			{name: 'MANDANTE'},
			{name: 'CONSECUTIVO'},
			{name: 'IC_PYME'},
			{name: 'IC_PROCESO'},
			{name: 'ESTATUS'},
			{name: 'DUPLICADO'}
		],	
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,		
		listeners: {
			load: procesarConsultaData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.					
						procesarConsultaData(null, null, null);						
					}
				}
			}			
	});

	
	//var gridConsulta = new Ext.grid.EditorGridPanel({
	var gridConsulta = new Ext.grid.GridPanel({
		id: 'gridConsulta',
		title: 'Carga de Documentos',		
		store: consultaData,	
		columns: [
			{							
				header : 'ESTATUS',
				tooltip: 'ESTATUS',
				dataIndex : 'ESTATUS',
				sortable: true,
				width: 130,
				hidden: true,
				resizable: true,				
				align: 'left'	
			},	
			{							
				header : 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'N�mero de Documento',
				tooltip: 'N�mero de Documento',
				dataIndex : 'NUM_DOCUMENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex : 'FECHA_EMISION',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento',
				tooltip: 'Fecha Vencimiento',
				dataIndex : 'FECHA_VENCIMIENTO',
				width : 150,
				align: 'center',
				sortable : true
			},
			{							
				header : 'Fecha Vencimiento Proveedor',
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex : 'FECHA_VENC_PROVEEDOR',
				width : 150,
				align: 'center',
				sortable : true,
				hidden: true
			},
			{							
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				width : 150,
				align: 'left',
				sortable : true
			},
			{							
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{							
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORC_DESCUENTO',
				width : 150,
				sortable : true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')			
			},
			{							
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTO_DESCONTAR',
				width : 150,
				sortable : true,
				align:'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')			
			},		
			{							
				header : 'Referencia',
				tooltip: 'Referencia',
				dataIndex : 'REFERENCIA',
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBRE_IF',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Nombre Beneficiario',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'NOMBRE_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'left'							
			},
			{							
				header : 'Porcentaje Beneficiario',
				tooltip: 'Porcentaje Beneficiario',
				dataIndex : 'PORC_BENEFICIARIO',
				width : 150,
				sortable : true,
				hidden: true,
				align:'center',
				renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{							
				header : 'Monto Beneficiario',
				tooltip: 'Monto Beneficiario',
				dataIndex : 'MONTO_BENEFICIARIO',
				hidden: true,
				width : 150,
				sortable : true,
				align:'left'							
			},
			{							
				header : 'Campo1',
				tooltip: 'Campo1',
				dataIndex : 'CAMPO1',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo2',
				tooltip: 'Campo2',
				dataIndex : 'CAMPO2',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Campo3',
				tooltip: 'Campo3',
				dataIndex : 'CAMPO3',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo4',
				tooltip: 'Campo4',
				dataIndex : 'CAMPO4',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Campo5',
				tooltip: 'Campo5',
				dataIndex : 'CAMPO5',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Fecha de Recepci�n de Bienes y Servicios',
				tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
				dataIndex : 'FECHA_RECEPCION',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Tipo de Compra (procedimiento)',
				tooltip: 'Tipo de Compra (procedimiento)',
				dataIndex : 'TIPO_COMPRA',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},	
			{							
				header : 'Clasificador por Objeto del Gasto',
				tooltip: 'Clasificador por Objeto del Gasto',
				dataIndex : 'CLASIFICADOR',
				width : 150,
				align: 'left',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Plazo M�ximo ',
				tooltip: 'Plazo M�ximo ',
				dataIndex : 'PLAZO_MAXIMO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'MANDANTE',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			},
			{							
				header : 'Notificado como <br>posible Duplicado',
				tooltip: 'Notificado como posible Duplicado',
				dataIndex : 'DUPLICADO',
				width : 150,
				align: 'center',
				hidden: true,
				sortable : false
			}
		],
		displayInfo: true,		
		emptyMsg: "No hay registros.",
		loadMask: true,
		clicksToEdit: 1, 
		margins: '20 0 0 0',
		stripeRows: true,
		height: 200,
		width: 900,
		style: 'margin:0 auto;',
		frame: false
	});
	
	//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 949,
		items: [
			NE.util.getEspaciador(20),
			gridControl	,
			NE.util.getEspaciador(20),
			leyendaLegal,
			NE.util.getEspaciador(20),
			fpConfirmar,
			NE.util.getEspaciador(20),
			gridConsulta,	
			NE.util.getEspaciador(20),
			gridConsulta2,	
			NE.util.getEspaciador(20),
			gridConsulta3,
			NE.util.getEspaciador(20),
			gridTotales,
			NE.util.getEspaciador(20)
		]
	});
	
	
	if(strPendiente=='S'){
		gridConsulta.setTitle('<div><div style="float:left">Pendientes Negociables</div><div style="float:right"></div></div>');
		gridConsulta2.setTitle('<div><div style="float:left">Pendientes No Negociables</div><div style="float:right"></div></div>');
	}else if(strPreNegociable =='S'){
		gridConsulta.setTitle('<div><div style="float:left">Pre Negociables</div><div style="float:right"></div></div>');						
		gridConsulta2.setTitle('<div><div style="float:left">Pre No Negociables</div><div style="float:right"></div></div>');		
	}else  {
		gridConsulta.setTitle('<div><div style="float:left">Negociables</div><div style="float:right"></div></div>');						
		gridConsulta2.setTitle('<div><div style="float:left">No Negociables</div><div style="float:right"></div></div>');				
	}
	gridConsulta3.setTitle('<div><div style="float:left">Vencidos Sin Operar</div><div style="float:right"></div></div>');
			
	consultaData.load({  	params: {  informacion: 'ConsultaPreAcuse1', 	proceso:proceso, strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, 	pantalla:'PreAcuse'   }  	});	
		
	consultaData2.load({  params: {  informacion: 'ConsultaPreAcuse2', 	proceso:proceso, strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, 	pantalla:'PreAcuse'  	}  	});	
	if(bVenSinOperarS =='S') {
	consultaData3.load({  params: {  informacion: 'ConsultaPreAcuse3', 	proceso:proceso, strPendiente:strPendiente,  	strPreNegociable:strPreNegociable,	pantalla:'PreAcuse' 	}  	});	
	}
	totalesData.load({ 	params: {  informacion: 'TotalesPreAcuse1',  proceso:proceso,  strPendiente:strPendiente,  	strPreNegociable:strPreNegociable,   pantalla:'PreAcuse' 	 } });
	
	totalesControl.load({ 	params: { 	informacion: 'ControlTotales',  	proceso:proceso, strPendiente:strPendiente,  	strPreNegociable:strPreNegociable, 		pantalla:'PreAcuse' 	} });
		
		
	if(bVenSinOperarS =='N') {
		gridConsulta3.hide();
	}
});