Ext.ns('NE.manto');

var validaLongDecim = function(value, longEntera, longDecimal){
    var msg = true;
    var RE = /^-{0,1}\d*\.{0,1}\d+$/; 
    var valor = value.replace(/[\,|\$]/g, '');
    //value = value.replace(/[\,]/g, '')
    if(RE.test(valor)){
      var n = valor.indexOf(".");
      if(n >= 0){
        var entero = valor.substring(0, n);
        var decimal = valor.substring(n+1);
        if(entero.length>longEntera | decimal.length>longDecimal)msg='El tama�o m�ximo para este campo es de '+longEntera+' enteros y '+longDecimal+' decimales';
      }else{
         if(valor.length>longEntera)msg='El tama�o m�ximo para este campo es de '+longEntera+' enteros y '+longDecimal+' decimales';
      }
    }
    return msg;
    
  }
  var formatNumber = {
    separador: ",", // separador para los miles
    sepDecimal: '.', // separador para los decimales
    formatear:function (num){
      num +='';
      var splitStr = num.split('.');
      var splitLeft = splitStr[0];
      var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
      var regx = /(\d+)(\d{3})/;
      while (regx.test(splitLeft)) {
        splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
      }
      return this.simbol + splitLeft +splitRight;
    },
    news:function(num, simbol){
      this.simbol = simbol ||'';
      return this.formatear(num);
    }
  }
  
   var esNumero = function(field){ 
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    if(RE.test(field.getValue().replace(/[\,|\$]/g, ''))){
      field.setValue(formatNumber.news(field.getValue().replace(/[\,|\$]/g, '')));
    }else{
      field.setValue('');
    }
    
  }
  

NE.manto.FormaCifrasCtrl =  Ext.extend(Ext.form.FormPanel,{
	initComponent: function(){
		Ext.apply(this, {
			title: 'Cifras de Control',
			width: 500,
			frame: true,		
			titleCollapse: false,
			style: 'margin:0 auto;',
			bodyStyle: 'padding: 6px',
			labelWidth: 200,
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			monitorValid: true,
			items:[
				{ 
					xtype:   'label',  
					html:		'Moneda Nacional', 
					cls:		'x-form-item', 
					style: { 
						width:   		'100%', 
						textAlign: 		'center'
					} 
				},		
				{
					xtype: 'numberfield',
					name: 'hidCtrlNumDoctosMN',
					id: 'hidCtrlNumDoctosMN1',
					fieldLabel: 'No. total de documentos',
					allowBlank: true,
					startDay: 0,
					maxLength: 5,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 200,
					msgTarget: 'side',						
					margins: '0 20 0 0'		
				},
				{
					xtype: 'textfield',
					name: 'hidCtrlMontoTotalMNDespliegue',
					id: 'hidCtrlMontoTotalMNDespliegue1',
					fieldLabel: 'Monto total de documentos',
					allowBlank: true,
					startDay: 0,
					//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
          			validator:function(value){ return validaLongDecim(value,17,2)},
					width: 200,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					listeners:{
						//blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
            			blur: esNumero
					}
				},
				{
					xtype: 'textfield',
					name: 'hidCtrlMontoMinMNDespliegue',
					id: 'hidCtrlMontoMinMNDespliegue1',
					fieldLabel: 'Monto m�nimo de los documentos',
					allowBlank: true,
					startDay: 0,
					//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
          			validator:function(value){ return validaLongDecim(value,12,2)},
					width: 200,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					listeners:{
						//blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
           				blur: esNumero
					}
				},
				{
					xtype: 'textfield',
					name: 'hidCtrlMontoMaxMNDespliegue',
					id: 'hidCtrlMontoMaxMNDespliegue1',
					fieldLabel: 'Monto m�ximo de los documentos',
					allowBlank: true,
					startDay: 0,
					//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
          			validator:function(value){ return validaLongDecim(value,12,2)},
					width: 200,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					listeners:{
						//blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
            			blur: esNumero
					}
				},
				{ 
					xtype:   'label',  
					html:		'D�lares', 
					cls:		'x-form-item', 
					style: { 
						width:   		'100%', 
						textAlign: 		'center'
					} 
				},
				{
					xtype: 'numberfield',
					name: 'hidCtrlNumDoctosDL',
					id: 'hidCtrlNumDoctosDL1',
					fieldLabel: 'No. total de documentos',
					allowBlank: true,
					startDay: 0,
					maxLength: 5,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 200,
					msgTarget: 'side',						
					margins: '0 20 0 0'		
				},
				{
					xtype: 'textfield',
					name: 'hidCtrlMontoTotalDLDespliegue',
					id: 'hidCtrlMontoTotalDLDespliegue1',
					fieldLabel: 'Monto total de documentos',
					allowBlank: true,
					startDay: 0,
					//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
          			validator:function(value){ return validaLongDecim(value,17,2)},
					width: 200,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					listeners:{
						//blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
            			blur: esNumero
					}
				},
				{
					xtype: 'textfield',
					name: 'hidCtrlMontoMinDLDespliegue',
					id: 'hidCtrlMontoMinDLDespliegue1',
					fieldLabel: 'Monto m�nimo de los documentos',
					allowBlank: true,
					startDay: 0,
					//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
          			validator:function(value){ return validaLongDecim(value,12,2)},
					width: 200,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					listeners:{
						//blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
            			blur: esNumero
					}
				},
				{
					xtype: 'textfield',
					name: 'hidCtrlMontoMaxDLDespliegue',
					id: 'hidCtrlMontoMaxDLDespliegue1',
					fieldLabel: 'Monto m�ximo de los documentos',
					allowBlank: true,
					startDay: 0,
					//maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
          			validator:function(value){ return validaLongDecim(value,12,2)},
					width: 200,
					msgTarget: 'side',						
					margins: '0 20 0 0',
					listeners:{
						//blur: function(field){ field.setValue(Ext.util.Format.number(field.getValue().replace(/[\$]/g, ''), '0,00.00'));  }
            			blur: esNumero
					}
				}
			],
			buttons: [
				{
					text: 'Enviar',
					id: 'btnCtrlEnviar',
					iconCls: 'icoAceptar',
					formBind: true
				},
				{
					text: 'Limpiar',
					id: 'btnCtrlLimpiar',
					iconCls: 'icoLimpiar'
				}			
			]
		});
		NE.manto.FormaCifrasCtrl.superclass.initComponent.call(this);
	},
	setHandlerBtnEnviar: function(fn){
		Ext.getCmp('btnCtrlEnviar').setHandler(fn);
	},
	setHandlerBtnLimpiar: function(fn){
		Ext.getCmp('btnCtrlLimpiar').setHandler(fn);
	}
});
Ext.reg('form_ctrl_cifras', NE.manto.FormaCifrasCtrl);


NE.manto.FormaCargaArchivo =  Ext.extend(Ext.form.FormPanel,{
	initComponent: function(){
		Ext.apply(this, {
			width: 600,
			frame: true,
			fileUpload: true,
			style: 'margin:0 auto;',
			bodyStyle: 'padding: 6px; text-align:center;',
			monitorValid: true,
			items:[
					{
						xtype:	'panel',
						layout:	'table',
						//frame: true,
						//width: 500,
						anchor: '100%',
						id:'cargaArchivo1',
						layoutConfig: {
							columns: 3
					    	},
						defaults: {
							bodyStyle:'padding:4px'
						},
						items:	[
							{
								xtype: 'button',
								id: 'btnAyuda',
								//columnWidth: .05,
								autoWidth: true,
								autoHeight: true,
								iconCls: 'icoAyuda'
							},
							{
								xtype: 'panel',
								id:		'pnlArchivo',
								//columnWidth: .9,
								anchor: '100%',
								layout: 'form',
								fileUpload: true,
								labelWidth: 100,
								defaults: {
									bodyStyle:'padding:5px',
									msgTarget: 'side'
									//anchor: '-20'
								},
								items: [
									{
										xtype: 'compositefield',
										fieldLabel: '',
										//id: 'cargaArchivo1',
										combineErrors: false,
										msgTarget: 'side',
										items: [
											{
												xtype: 'fileuploadfield',
												id: 'archivo',
												width: 320,
												emptyText: 'Ruta del Archivo',
												fieldLabel: 'Ruta del Archivo',
												name: 'archivoCesion',
												allowBlank: false,
												buttonCfg: {
												  iconCls: 'upload-icon'
												},
												buttonText: null,
												//anchor: '95%',
												regex: /^.*\.(txt|TXT)$/,
												regexText:'Solo se admiten archivos TXT'
											},
											{
												xtype: 'hidden', id:	'hidExtension', name:	'hidExtension', value:	''
											},
											{
												xtype: 'hidden', id:	'hidNumDoctosMN1', name:	'hidNumDoctosMN', value:	''
											},
											{
												xtype: 'hidden', id:	'hidMontoTotalMNDespliegue1', name:	'hidMontoTotalMNDespliegue', value:	''
											},
											{
												xtype: 'hidden', id:	'hidMontoMinMNDespliegue1', name:	'hidMontoMinMNDespliegue', value:	''
											},
											{
												xtype: 'hidden', id:	'hidMontoMaxMNDespliegue1', name:	'hidMontoMaxMNDespliegue', value:	''
											},
											{
												xtype: 'hidden', id:	'hidNumDoctosDL1', name:	'hidNumDoctosDL', value:	''
											},
											{
												xtype: 'hidden', id:	'hidMontoMinDLDespliegue1', name:	'hidMontoMinDLDespliegue', value:	''
											},
											{
												xtype: 'hidden', id:	'hidMontoMaxDLDespliegue1', name:	'hidMontoMaxDLDespliegue', value:	''
											},
											{
												xtype: 'hidden', id:	'hidMontoTotalDLDespliegue1', name:	'hidMontoTotalDLDespliegue', value:	''
											}
											
										]
									}
								]
							},
							{
								xtype: 	'button',
								text: 	'Continuar',
								//columnWidth: .05,
								autoWidth: true,
								id: 'btnCargaContinuar',
								iconCls: 'icoContinuar',
								formBind: true,
								style: { 
									  marginBottom:  '10px' 
								}
							},
							{
								xtype: 'panel',
								name: 'pnlMsgValid',
								id: 'pnlMsgValid1',
								colspan: 3,
								width: 500,
								style: 'margin:0 auto;',
								frame: true,
								hidden: true
							}
						]
					}
				]
		});
		
		NE.manto.FormaCargaArchivo.superclass.initComponent.call(this);
	},
	setHandlerBtnAyuda:function(fn){
		Ext.getCmp('btnAyuda').setHandler(fn);
	},
	setHandlerBtnContinuarCarga: function(fn){
		Ext.getCmp('btnCargaContinuar').setHandler(fn);
	}
});
Ext.reg('form_carga_archivo', NE.manto.FormaCargaArchivo);



NE.manto.FormaValidacionArchivo =  Ext.extend(Ext.form.FormPanel, {
	dataSinError: '',
	dataConError: '',
	otrosErrores: '',
	initComponent: function(){
		Ext.apply(this,{
			frame: true,
			width: 600,
			heigth: 400,
			layout: 'table',
			style: 'margin:0 auto;',
			defaults: {
			  bodyStyle:'padding:20px'
			},
			layoutConfig: {
			  // The total column count must be specified here
			  columns: 2
			},
			items:[
				{ 
					xtype:   'label',  
					html:		'Documentos sin Errores', 
					cls:		'x-form-item', 
					style: { 
						width:   		'100%', 
						textAlign: 		'center',
						marginBottom:  '10px'
					} 
				},
				{ 
					xtype:   'label',  
					html:		'Detalle de Documentos con Errores', 
					cls:		'x-form-item', 
					style: { 
						width:   		'100%', 
						textAlign: 		'center',
						marginBottom:  '10px'
					} 
				},
				{
					xtype: 'textarea',
					id: 'dataSinError',
					width: 290,
					height: 300,
					value: this.dataSinError
				},
				{
					xtype: 'textarea',
					id: 'b',
					width: 290,
					height: 300,
					value: this.dataConError
				},
				{ 
					xtype:   'label',  
					html:		'Otros errores', 
					cls:		'x-form-item', 
					style: { 
						width:   		'100%', 
						textAlign: 		'center',
						marginBottom:  '10px'
					},
					colspan: 2
				},
				{
					xtype: 'textarea',
					labelField: 'Otros errores',
					id: 'c',
					width: 580,
					colspan: 2,
					value: this.otrosErrores
				}
			],
			buttons: [
				{
					text: 'Descargar Archivo',
					id: 'btnDescargaErrores',
					iconCls: 'icoXls'
				}
			]
			
		});
		NE.manto.FormaValidacionArchivo.superclass.initComponent.call(this);
	},
	setHandlerBtnDescarga:function(fn){
		Ext.getCmp('btnDescargaErrores').setHandler(fn);
	},
	getBtnDescarga:function(){
		return Ext.getCmp('btnDescargaErrores');
	}
});
Ext.reg('form_valida_archivo', NE.manto.FormaValidacionArchivo);