<%@ page import="
	java.util.*"
%>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br><br><b>Atributos en el request</b><br><hr>
<%

Enumeration e = request.getAttributeNames();
    while (e.hasMoreElements()) {
      String name = (String) e.nextElement();
      out.println(name + ": " + request.getAttribute(name) + "<BR>");
    }
java.security.cert.X509Certificate certs [] = (java.security.cert.X509Certificate []) request.getAttribute("javax.servlet.request.X509Certificate");

%>
<hr>
<b>Certificado</b>
<hr>
<%
if (certs !=null) {
	for (int i = 0; i < certs.length; i++) {
		out.println("Client Certificate [" + i + "] = " + certs[i].getSerialNumber());
		byte x [] = certs[i].getSerialNumber().toByteArray();
		out.println("--" + x + "--");
	}
} else {
	if ("https".equals(request.getScheme())) {
		out.println("This was an HTTPS request, " + "but no client certificate is available");
	} else {
		out.println("This was not an HTTPS request, " + "so no client certificate is available");
	}
}
%>
	
</body>

</html>
