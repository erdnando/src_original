<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.sql.*,
		java.util.*,
		netropology.utilerias.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
BusquedaPymeGral busqPymeGral = new BusquedaPymeGral();

String sNombrePyme = (session.getAttribute("strNombrePymeAsigna")==null)?"":session.getAttribute("strNombrePymeAsigna").toString();
String sNEPyme = (session.getAttribute("strNePymeAsigna")==null)?"":session.getAttribute("strNePymeAsigna").toString();
String sTipoDscto = (request.getParameter("sTipoDscto")==null)?"'N','V','D','M','I'":request.getParameter("sTipoDscto");
// Utilizado para el acuse en la selección de documentos.
session.setAttribute("sOrigenDscto", (request.getParameter("sOrigenDscto")==null?"":request.getParameter("sOrigenDscto")) );

if (informacion.equals("resumenDoctos")) {

	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	List lstResDoctos = new ArrayList();
	String ic_pyme  	= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	
	session.setAttribute("iNoCliente",ic_pyme);
	
	lstResDoctos = busqPymeGral.getResumenDoctos(ic_pyme, sTipoDscto);
	jsObjArray = JSONArray.fromObject(lstResDoctos);
	infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";
	
	System.out.println("infoRegresar==="+infoRegresar);

}
%>
<%=infoRegresar%>