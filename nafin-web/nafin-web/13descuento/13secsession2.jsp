<%@ page import=" java.sql.*, java.text.*,netropology.utilerias.*"%>
<%
//El siguiente c�digo debe ser lo primero en ejecutarse, el cual determina si la sesi�n est� activa. 
//De lo contrario en lugar de mostrar "Su sesi�n ha expirado..." lanzar� un NullPointer Exception
String gsCveUsuario = (String) session.getAttribute("sesCveUsuario");
if(gsCveUsuario == null){
%>
		<script language="JavaScript">
			alert("Su sesi�n ha expirado, por favor vuelva a entrar.");
			top.location="/nafin/15cadenas/15salir.jsp"
		</script>
<%
	return;
}
//Variables para los archivos
	String strDirectorioPublicacion = (String)application.getAttribute("strDirectorioPublicacion");
	String strDirectorioTemp = strDirectorioPublicacion+"00tmp/13descuento/";
	String strDirecVirtualTemp = "/nafin/00tmp/13descuento/";
	String strDirTrabajo = strDirectorioPublicacion+"00archivos/13descuento/";
	String strDirecVirtualTrabajo = "/nafin/00archivos/13descuento/";

	String strDirecCSS = "/nafin/14seguridad/Seguridad";
	
	String strTipoUsuario = (String)session.getAttribute("strTipoUsuario"); // POSIBLES VALORES NAFIN-EPO-PYME-IF
	String iNoUsuario = (String)session.getAttribute("iNoUsuario"); // 
	String iNoCliente = (String)session.getAttribute("iNoCliente");//NO DE CLIENTE DE LAS TABLAS COMCAT_PYME, COMCAT_EPO, COMCAT_IF
	String strNombre = (String)session.getAttribute("strNombre"); // NOMBRE COMERCIAL O DE LA PERSONA DE LAS TABLAS COMCAT_PYME, COMCAT_EPO, COMCAT_IF
	String strNombreUsuario = (String)session.getAttribute("strNombreUsuario"); //NOMBRE DEL USUARIO DE TABLA 
	String iNoEPO = (String)session.getAttribute("iNoEPO");//NUMERO DE LA EPO A LA CUAL PERTENECE LA PYME
	Long iNoNafinElectronico = (Long)session.getAttribute("iNoNafinElectronico");//NUMERO DE LA EPO A LA CUAL PERTENECE LA PYME
	String strLogin = (String)session.getAttribute("Clave_usuario");//LOGIN DEL USUARIO QUE SE LOGUEA
	String strLogo = (String)session.getAttribute("strLogo");//Logotipo
	String strClase = (String)session.getAttribute("strClase");//Clase css
	String bIfRelacionada = (String)session.getAttribute("bIfRelacionada");//Indica si la If esta relacionada o no.
	String strTipoAfiliacion = (String)session.getAttribute("strTipoAfiliacion");//TIPO DE AFILIACION DEL USUARIO B(BAJO),A(ALTO)
	String strFacultad = null; // Variable que almacena la facultad
	String strSerial = (String)session.getAttribute("strSerial");
	String strAforo = (String)session.getAttribute("strAforo"); // Variable que determina el aforo a aplicar a los documentos
	String strAforoDL = (String)session.getAttribute("strAforoDL"); // Variable que determina el aforo a aplicar a los documentos	
	String strPerfil = (String)session.getAttribute("sesPerfil"); // VARIABLE QUE TRAE EL PERFIL DEL USUARIO QUE ESTA EN SESION
	
	String strDirecVirtualCSS = "/nafin/14seguridad/Seguridad/"; //VARIABLE  PARA CSS

	String sesIdiomaUsuario = (session.getAttribute("sesIdiomaUsuario") == null)?"ES":(String)session.getAttribute("sesIdiomaUsuario");
	
	
try {

	
	if(iNoUsuario == null || iNoUsuario.equals("") || strTipoUsuario == null || strTipoUsuario.equals("")){
		response.sendRedirect("/nafin/20secure/ne.jsp");
	}
} catch(Exception e) {
	String strErr = e.toString();
%>
	<html><script language="JavaScript">alert("Su sesión ha expirado, por favor vuelva a entrar.");top.window.close();</script></html>
<%}%>
