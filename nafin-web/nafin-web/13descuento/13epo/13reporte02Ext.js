Ext.onReady(function(){

	var cveEstatus;
	var fecha  = Ext.getDom('fecha').value;
	var usuario  = Ext.getDom('usuario').value;
			
	//ESTATUS Pre negociable a Negociable 
	// Baja archivo PDF
	var procesarSuccessFailureArchivoPreNe =  function(opts, success, response) {
		var btnGenerarPdfPre = Ext.getCmp('btnGenerarPdfPre');
		btnGenerarPdfPre.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPdfPre = Ext.getCmp('btnBajarPdfPre');
			btnBajarPdfPre.show();
			btnBajarPdfPre.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPdfPre.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPdfPre.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaPreNegociable = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridPreNegociable.isVisible()) {
				gridPreNegociable.show();
			}			
		
			var jsonData = store.reader.jsonData;	
			var sFechaVencPyme = jsonData.sFechaVencPyme;	
			var cm = gridPreNegociable.getColumnModel();
						
			if(sFechaVencPyme==''){
					gridPreNegociable.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEDOR'), true);	
			}
			
			var btnGenerarPdfPre = Ext.getCmp('btnGenerarPdfPre');
			var btnBajarPdfPre = Ext.getCmp('btnBajarPdfPre');
			var btnTotales40 =	Ext.getCmp('btnTotales40');		
			
			var el = gridPreNegociable.getGridEl();						
			if(store.getTotalCount() > 0) {		
				btnTotales40.enable();	
				btnGenerarPdfPre.enable();				
				btnBajarPdfPre.hide();
				el.unmask();
			} else {	
				btnTotales40.disable();	
				btnGenerarPdfPre.disable();				
				btnBajarPdfPre.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}			
		}
	}
		
	var totales40Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			cveEstatus: 40
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_DESCONTAR', type: 'float', mapping: 'MONTO_DESCONTAR'}	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var gridTotales40 = {
		xtype: 'grid',
		store: totales40Data,
		id: 'gridTotales40',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},						
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	}
	
	
	var consultaPreNegociable = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NOMBRE_PROVEEDOR'},				
			{name: 'NO_DOCUMENTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC'},
			{name: 'FECHA_VENC_PROVEDOR'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORC_DESCUENTO'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'NUMERO_ACUSE'},
			{name: 'BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaPreNegociable,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaPreNegociable(null, null, null);						
				}
			}
		}
	});	
		
	var gridPreNegociable = new Ext.grid.EditorGridPanel({	 
		id: 'gridPreNegociable',
		store: consultaPreNegociable,		
		margins: '20 0 0 0',
		hidden: true,
		title: 'Estatus: Pre negociable a Negociable ',
		columns: [			
			{
				header: 'Nombre Proveedor', 
				tooltip: 'Nombre Proveedor',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'N�mero de Documento', 
				tooltip: 'N�mero de Documento',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},			
			{
				header: 'Fecha Emisi�n', 
				tooltip: 'Fecha Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento', 
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento Proveedor', 
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex: 'FECHA_VENC_PROVEDOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},				
			{
				header: 'Moneda', 
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Monto', 
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{
				header: 'Porcentaje de Descuento', 
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto a Descontar', 
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},		
			{
				header: 'N�mero de Acuse', 
				tooltip: 'N�mero de Acuse',
				dataIndex: 'NUMERO_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'			
			},
			{
				header: 'Beneficiario', 
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Porcentaje Beneficiario', 
				tooltip: 'Porcentaje Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Importe a Recibir Beneficiario', 
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMPORTE_RECIBIR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales40',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales40');
						if (ventana) {
							ventana.show();
						} else {
							totales40Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales40',
								closeAction: 'hide',
								items: [
									gridTotales40
								],								
								title: 'TotalesPre negociable a Negociable'
							}).show().alignTo(gridPreNegociable.el);
						}
					}
				},
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfPre',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02Ext.data.jsp',
							params: {
								informacion:'ArchivoPDF',
								cveEstatus:	cveEstatus
							}
							,callback: procesarSuccessFailureArchivoPreNe
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar PDF',
					id: 'btnBajarPdfPre',
					hidden: true
				}
			]
		}
	});
	
	//ESTATUS Programado a Negociable
	
	// Baja archivo PDF
	var procesarSuccessFailureArchivoProgr =  function(opts, success, response) {
		var btnGenerarPdfPro = Ext.getCmp('btnGenerarPdfPro');
		btnGenerarPdfPro.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPdfPro = Ext.getCmp('btnBajarPdfPro');
			btnBajarPdfPro.show();
			btnBajarPdfPro.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPdfPro.setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				});
		} else {
			btnGenerarPdfPro.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaProgramado = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridProgramado.isVisible()) {
				gridProgramado.show();
			}			
			var jsonData = store.reader.jsonData;	
			var sFechaVencPyme = jsonData.sFechaVencPyme;	
			var cm = gridProgramado.getColumnModel();
						
			if(sFechaVencPyme==''){
				gridProgramado.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEDOR'), true);	
			}
			
			var btnGenerarPdfPro = Ext.getCmp('btnGenerarPdfPro');
			var btnBajarPdfPro = Ext.getCmp('btnBajarPdfPro');
			var btnTotales29 =	Ext.getCmp('btnTotales29');		
						
			var el = gridProgramado.getGridEl();						
			if(store.getTotalCount() > 0) {	
				btnTotales29.enable();
				btnGenerarPdfPro.enable();				
				btnBajarPdfPro.hide();
				el.unmask();
			} else {	
				btnTotales29.disable();
				btnGenerarPdfPro.disable();				
				btnBajarPdfPro.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}		
		}
	}
	
	var totales29Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			cveEstatus: 29
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_DESCONTAR', type: 'float', mapping: 'MONTO_DESCONTAR'}	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var gridTotales29 = {
		xtype: 'grid',
		store: totales29Data,
		id: 'gridTotales29',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},						
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	}	
	
	var consultaProgramado = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NOMBRE_PROVEEDOR'},				
			{name: 'NO_DOCUMENTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC'},
			{name: 'FECHA_VENC_PROVEDOR'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORC_DESCUENTO'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'NUMERO_ACUSE'},
			{name: 'BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaProgramado,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaProgramado(null, null, null);						
				}
			}
		}
	});	
		
	var gridProgramado = new Ext.grid.EditorGridPanel({	 
		id: 'gridProgramado',
		store: consultaProgramado,	
		hidden: true,
		margins: '20 0 0 0',
		title: 'Estatus: Programado a Negociable',
		columns: [			
			{
				header: 'Nombre Proveedor', 
				tooltip: 'Nombre Proveedor',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'N�mero de Documento', 
				tooltip: 'N�mero de Documento',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},			
			{
				header: 'Fecha Emisi�n', 
				tooltip: 'Fecha Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento', 
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento Proveedor', 
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex: 'FECHA_VENC_PROVEDOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},				
			{
				header: 'Moneda', 
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Monto', 
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')	
			},		
			{
				header: 'Porcentaje de Descuento', 
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto a Descontar', 
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},		
			{
				header: 'N�mero de Acuse', 
				tooltip: 'N�mero de Acuse',
				dataIndex: 'NUMERO_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'			
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales29',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales29');
						if (ventana) {
							ventana.show();
						} else {
								totales29Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales29',
								closeAction: 'hide',
								items: [
									gridTotales29
								],								
								title: 'Totales Programado a Negociable'
							}).show().alignTo(gridProgramado.el);
						}
					}
				},				
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfPro',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02Ext.data.jsp',
							params: {
								informacion:'ArchivoPDF',
								cveEstatus:	cveEstatus
							}
							,callback: procesarSuccessFailureArchivoProgr
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar PDF',
					id: 'btnBajarPdfPro',
					hidden: true
				}
			]
		}
	});
	
	
	
	//ESTATUS Aplicaci�n de Nota de Cr�dito	
	
		// Baja archivo PDF
	var procesarSuccessFailureArchivoNota =  function(opts, success, response) {
		var btnGenerarPdfN = Ext.getCmp('btnGenerarPdfN');
		btnGenerarPdfN.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPdfN = Ext.getCmp('btnBajarPdfN');
			btnBajarPdfN.show();
			btnBajarPdfN.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPdfN.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPdfN.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaNota = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridNota.isVisible()) {
				gridNota.show();
			}						
			var jsonData = store.reader.jsonData;	
			var sFechaVencPyme = jsonData.sFechaVencPyme;	
			var operaFactConMandato = jsonData.operaFactConMandato;				
			var cm = gridNota.getColumnModel();
			
			if(sFechaVencPyme==''){
				gridNota.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEDOR'), true);	
			}
			if(operaFactConMandato!='S'){
				gridNota.getColumnModel().setHidden(cm.findColumnIndex('MANDATORIO'), true);	
			}
								
			var btnGenerarPdfN = Ext.getCmp('btnGenerarPdfN');
			var btnBajarPdfN = Ext.getCmp('btnBajarPdfN');			
						
			var el = gridNota.getGridEl();						
			if(store.getTotalCount() > 0) {		
				btnGenerarPdfN.enable();				
				btnBajarPdfN.hide();
				el.unmask();
			} else {	
				btnGenerarPdfN.disable();				
				btnBajarPdfN.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}				
		}
	}
	
	var consultaNota = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NOMBRE_PROVEEDOR'},				
			{name: 'NO_DOCUMENTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC'},
			{name: 'FECHA_VENC_PROVEDOR'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORC_DESCUENTO'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'NUMERO_ACUSE'},
			{name: 'BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR'},					
			{name: 'FECHA_VENC_ANTERIOR'},
			{name: 'FECHA_VENC_PROV_ANTERIOR'},
			{name: 'CAUSA'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO_ANTERIOR'},
			{name: 'MONTO_NUEVO'},
			{name: 'MANDATORIO'}										
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaNota,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaNota(null, null, null);						
				}
			}
		}
	});	
		
	var gridNota = new Ext.grid.EditorGridPanel({	 
		id: 'gridNota',
		store: consultaNota,		
		margins: '20 0 0 0',
		hidden: true,
		title: 'Estatus: Aplicaci�n de Nota de Cr�dito',
		columns: [			
			{
				header: 'Nombre Proveedor', 
				tooltip: 'Nombre Proveedor',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'N�mero de Documento', 
				tooltip: 'N�mero de Documento',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'N�mero de Acuse', 
				tooltip: 'N�mero de Acuse',
				dataIndex: 'NUMERO_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'			
			},
			{
				header: 'Fecha Emisi�n', 
				tooltip: 'Fecha Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento', 
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento Proveedor', 
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex: 'FECHA_VENC_PROVEDOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Tipo Factoraje', 
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},			
			{
				header: 'Moneda', 
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Monto Anterior', 
				tooltip: 'Monto Anterior',
				dataIndex: 'MONTO_ANTERIOR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},	
			{
				header: 'Monto Nuevo', 
				tooltip: 'Monto Nuevo',
				dataIndex: 'MONTO_NUEVO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento', 
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto a Descontar', 
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},			
			{
				header: 'Causa', 
				tooltip: 'Causa',
				dataIndex: 'CAUSA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'			
			},
			{
				header: 'Mandatario', 
				tooltip: 'Mandatario',
				dataIndex: 'MANDATORIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			}	
			],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfN',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02Ext.data.jsp',
							params: {
								informacion:'ArchivoPDF',
								cveEstatus:	cveEstatus
							}
							,callback: procesarSuccessFailureArchivoNota
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar PDF',
					id: 'btnBajarPdfN',
					hidden: true
				}
			]
		}
	});
	
	//ESTATUS Modificaci�n de Montos y/o Fechas de Vencimiento
	
	// Baja archivo PDF
	var procesarSuccessFailureArchivoModifi =  function(opts, success, response) {
		var btnGenerarPdfM = Ext.getCmp('btnGenerarPdfM');
		btnGenerarPdfM.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPdfM = Ext.getCmp('btnBajarPdfM');
			btnBajarPdfM.show();
			btnBajarPdfM.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPdfM.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPdfM.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}


	var procesarConsultaModificado = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridModificado.isVisible()) {
				gridModificado.show();
			}			
		
			var jsonData = store.reader.jsonData;	
			var sFechaVencPyme = jsonData.sFechaVencPyme;						
			var cm = gridModificado.getColumnModel();
			
			if(sFechaVencPyme==''){
					gridModificado.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEDOR'), true);	
					gridModificado.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROV_ANTERIOR'), true);	
			}
						
			var btnGenerarPdfM = Ext.getCmp('btnGenerarPdfM');
			var btnBajarPdfM = Ext.getCmp('btnBajarPdfM');
			var btnTotales8 =	Ext.getCmp('btnTotales8');	
				
			var el = gridModificado.getGridEl();						
			if(store.getTotalCount() > 0) {	
				btnTotales8.enable();	
				btnGenerarPdfM.enable();				
				btnBajarPdfM.hide();
				el.unmask();
			} else {	
				btnTotales8.disable();	
				btnGenerarPdfM.disable();				
				btnBajarPdfM.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}				
		}
	}
	
	
	
	var totales8Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			cveEstatus: 8
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},	
			{name: 'MONTO_ANTERIOR', type: 'float', mapping: 'MONTO_ANTERIOR'},
			{name: 'MONTO_DESCONTAR', type: 'float', mapping: 'MONTO_DESCONTAR'}		
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var gridTotales8 = {
		xtype: 'grid',
		store: totales8Data,
		id: 'gridTotales8',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},						
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Total Monto anterior',
				dataIndex: 'MONTO_ANTERIOR',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},		
			{
				header: 'Total Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	}
	
	
	
	var consultaModificado = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NOMBRE_PROVEEDOR'},
			{name: 'NO_DOCUMENTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC'},
			{name: 'FECHA_VENC_PROVEDOR'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORC_DESCUENTO'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'NUMERO_ACUSE'},
			{name: 'BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR'},					
			{name: 'FECHA_VENC_ANTERIOR'},
			{name: 'FECHA_VENC_PROV_ANTERIOR'},
			{name: 'CAUSA'},
			{name: 'MONTO_ANTERIOR'}				
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaModificado,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaModificado(null, null, null);						
				}
			}
		}
	});	
		
	var gridModificado = new Ext.grid.EditorGridPanel({	 
		id: 'gridModificado',
		store: consultaModificado,		
		margins: '20 0 0 0',
		hidden: true,
		title: 'Estatus: Modificaci�n de Montos y/o Fechas de Vencimiento',
		columns: [			
			{
				header: 'Nombre Proveedor', 
				tooltip: 'Nombre Proveedor',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'N�mero de Documento', 
				tooltip: 'N�mero de Documento',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Fecha Emisi�n', 
				tooltip: 'Fecha Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento', 
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento Proveedor', 
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex: 'FECHA_VENC_PROVEDOR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Moneda', 
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Monto', 
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento', 
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Fecha Vencimiento Anterior', 
				tooltip: 'Fecha Vencimiento Anterior',
				dataIndex: 'FECHA_VENC_ANTERIOR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento Proveedor Anterior', 
				tooltip: 'Fecha Vencimiento Proveedor Anterior',
				dataIndex: 'FECHA_VENC_PROV_ANTERIOR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
				{
				header: 'Monto anterior', 
				tooltip: 'Monto anterior',
				dataIndex: 'MONTO_ANTERIOR',
				sortable: true,
				resizable: true	,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'Monto a Descontar', 
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'N�mero de Acuse', 
				tooltip: 'N�mero de Acuse',
				dataIndex: 'NUMERO_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'			
			},
			{
				header: 'Causa', 
				tooltip: 'Causa',
				dataIndex: 'CAUSA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'			
			},
			{
				header: 'Beneficiario', 
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Porcentaje Beneficiario', 
				tooltip: 'Porcentaje Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Importe a Recibir Beneficiario', 
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMPORTE_RECIBIR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
			],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {
			items: [
			{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales8',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales8');
						if (ventana) {
							ventana.show();
						} else {
								totales8Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales8',
								closeAction: 'hide',
								items: [
									gridTotales8
								],								
								title: 'Totales Modificaci�n de Montos y/o Fechas de Vencimiento'
							}).show().alignTo(gridModificado.el);
						}
					}
				},
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfM',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02Ext.data.jsp',
							params: {
								informacion:'ArchivoPDF',
								cveEstatus:	cveEstatus
							}
							,callback: procesarSuccessFailureArchivoModifi
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar PDF',
					id: 'btnBajarPdfM',
					hidden: true
				}
			]
		}
	});
	
	//ESTATUS Pagado Anticipado
	
	// Baja archivo PDF
	var procesarSuccessFailureArchivoPagado =  function(opts, success, response) {
		var btnGenerarPdfP = Ext.getCmp('btnGenerarPdfP');
		btnGenerarPdfP.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPdfP = Ext.getCmp('btnBajarPdfP');
			btnBajarPdfP.show();
			btnBajarPdfP.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPdfP.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPdfP.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaPagado = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridPagado.isVisible()) {
				gridPagado.show();
			}			
		
			var jsonData = store.reader.jsonData;	
			var sFechaVencPyme = jsonData.sFechaVencPyme;	
			var firmaMancomunada = jsonData.firmaMancomunada;				
			var cm = gridPagado.getColumnModel();
			
			if(sFechaVencPyme==''){
					gridPagado.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEDOR'), true);	
			}
			if(firmaMancomunada!='S'){
				gridPagado.getColumnModel().setHidden(cm.findColumnIndex('CAMBIO_ESTATUS'), true);	
			}
		
			var el = gridPagado.getGridEl();						
			if(store.getTotalCount() > 0) {					
				el.unmask();
			} else {	
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
			var btnGenerarPdfP = Ext.getCmp('btnGenerarPdfP');
			var btnBajarPdfP = Ext.getCmp('btnBajarPdfP');
			var btnTotales6 =	Ext.getCmp('btnTotales6');	
			
			var el = gridPagado.getGridEl();						
			if(store.getTotalCount() > 0) {	
				btnTotales6.enable();	
				btnGenerarPdfP.enable();				
				btnBajarPdfP.hide();
				el.unmask();
			} else {
				btnTotales6.disable();	
				btnGenerarPdfP.disable();				
				btnBajarPdfP.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	}
	
	var totales6Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			cveEstatus: 6
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_DESCONTAR', type: 'float', mapping: 'MONTO_DESCONTAR'}		
	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var gridTotales6 = {
		xtype: 'grid',
		store: totales6Data,
		id: 'gridTotales6',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},						
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	}
	
	var consultaPagado = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NOMBRE_PROVEEDOR'},
			{name: 'NO_DOCUMENTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC'},
			{name: 'FECHA_VENC_PROVEDOR'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORC_DESCUENTO'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'NUMERO_ACUSE'},
			{name: 'BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR'},
			{name: 'CAMBIO_ESTATUS'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaPagado,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaPagado(null, null, null);						
				}
			}
		}
	});	
		
	var gridPagado = new Ext.grid.EditorGridPanel({	 
		id: 'gridPagado',
		store: consultaPagado,		
		margins: '20 0 0 0',
		hidden: true,
		title: 'Estatus: Pagado Anticipado',
		columns: [			
			{
				header: 'Nombre Proveedor', 
				tooltip: 'Nombre Proveedor',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'N�mero de Documento', 
				tooltip: 'N�mero de Documento',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Fecha Emisi�n', 
				tooltip: 'Fecha Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento', 
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento Proveedor', 
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex: 'FECHA_VENC_PROVEDOR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Moneda', 
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Monto', 
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento', 
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto a Descontar', 
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'N�mero de Acuse', 
				tooltip: 'N�mero de Acuse',
				dataIndex: 'NUMERO_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'			
			},
			{
				header: 'Beneficiario', 
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Porcentaje Beneficiario', 
				tooltip: 'Porcentaje Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Importe a Recibir Beneficiario', 
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMPORTE_RECIBIR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo de Cambio de Estatus No se encontr� registros', 
				tooltip: 'Tipo de Cambio de Estatus No se encontr� registros',
				dataIndex: 'CAMBIO_ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'				
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales6',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales6');
						if (ventana) {
							ventana.show();
						} else {
								totales6Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales6',
								closeAction: 'hide',
								items: [
									gridTotales6
								],								
								title: 'Totales Pagado Anticipado'
							}).show().alignTo(gridPagado.el);
						}
					}
				},
				
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfP',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02Ext.data.jsp',
							params: {
								informacion:'ArchivoPDF',
								cveEstatus:	cveEstatus
							}
							,callback: procesarSuccessFailureArchivoPagado
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar PDF',
					id: 'btnBajarPdfP',
					hidden: true
				}
			]
		}
	});
	

	//ESTATUS BAJA
	
		// Baja archivo PDF
	var procesarSuccessFailureArchivoBaja =  function(opts, success, response) {
		var btnGenerarPdfB = Ext.getCmp('btnGenerarPdfB');
		btnGenerarPdfB.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPdfB = Ext.getCmp('btnBajarPdfB');
			btnBajarPdfB.show();
			btnBajarPdfB.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPdfB.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPdfB.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarConsultaBaja = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridBaja.isVisible()) {
				gridBaja.show();
			}			
		
			var jsonData = store.reader.jsonData;	
			var sFechaVencPyme = jsonData.sFechaVencPyme;	
			var firmaMancomunada = jsonData.firmaMancomunada;				
			var cm = gridBaja.getColumnModel();
			
			if(sFechaVencPyme==''){
					gridBaja.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEDOR'), true);	
			}
			if(firmaMancomunada!='S'){
				gridBaja.getColumnModel().setHidden(cm.findColumnIndex('CAMBIO_ESTATUS'), true);	
			}
			
			var btnGenerarPdfB = Ext.getCmp('btnGenerarPdfB');
			var btnBajarPdfB = Ext.getCmp('btnBajarPdfB');
			var btnTotales4 =	Ext.getCmp('btnTotales4');	
				
			var el = gridBaja.getGridEl();						
			if(store.getTotalCount() > 0) {	
				btnTotales4.enable();
				btnGenerarPdfB.enable();				
				btnBajarPdfB.hide();
				el.unmask();
			} else {	
				btnTotales4.disable();
				btnGenerarPdfB.disable();				
				btnBajarPdfB.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	}
	
	var totales4Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			cveEstatus: 4
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_DESCONTAR', type: 'float', mapping: 'MONTO_DESCONTAR'}		
	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var gridTotales4 = {
		xtype: 'grid',
		store: totales4Data,
		id: 'gridTotales4',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},						
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	};
	
	var consultaBaja = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NOMBRE_PROVEEDOR'},
			{name: 'NO_DOCUMENTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENC'},
			{name: 'FECHA_VENC_PROVEDOR'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORC_DESCUENTO'},
			{name: 'MONTO_DESCUENTO'},
			{name: 'NUMERO_ACUSE'},
			{name: 'BENEFICIARIO'},
			{name: 'PORC_BENEFICIARIO'},
			{name: 'IMPORTE_RECIBIR'},
			{name: 'CAMBIO_ESTATUS'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaBaja,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaBaja(null, null, null);						
				}
			}
		}
	});	
		
	var gridBaja = new Ext.grid.EditorGridPanel({	 
		id: 'gridBaja',
		store: consultaBaja,		
		margins: '20 0 0 0',
		title: 'Estatus: Baja',
		hidden: true,
		columns: [			
			{
				header: 'Nombre Proveedor', 
				tooltip: 'Nombre Proveedor',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'N�mero de Documento', 
				tooltip: 'N�mero de Documento',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Fecha Emisi�n', 
				tooltip: 'Fecha Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento', 
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento Proveedor', 
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex: 'FECHA_VENC_PROVEDOR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Moneda', 
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Monto', 
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento', 
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto a Descontar', 
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'N�mero de Acuse', 
				tooltip: 'N�mero de Acuse',
				dataIndex: 'NUMERO_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'			
			},
			{
				header: 'Beneficiario', 
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Porcentaje Beneficiario', 
				tooltip: 'Porcentaje Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Importe a Recibir Beneficiario', 
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMPORTE_RECIBIR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo de Cambio de Estatus No se encontr� registros', 
				tooltip: 'Tipo de Cambio de Estatus No se encontr� registros',
				dataIndex: 'CAMBIO_ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'				
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {
			items: [
					{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales4',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales4');
						if (ventana) {
							ventana.show();
						} else {
								totales4Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales5',
								closeAction: 'hide',
								items: [
									gridTotales4
								],								
								title: 'Totales Baja'
							}).show().alignTo(gridBaja.el);
						}
					}
				},
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfB',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02Ext.data.jsp',
							params: {
								informacion:'ArchivoPDF',
								cveEstatus:	cveEstatus
							}
							,callback: procesarSuccessFailureArchivoBaja
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar PDF',
					id: 'btnBajarPdfB',
					hidden: true
				}
			]
		}
	});
	
//ESTA DESCUENTO FISICO

	// Baja archivo PDF
	var procesarSuccessFailureArchivoDescuento =  function(opts, success, response) {
		var btnGenerarPdfD = Ext.getCmp('btnGenerarPdfD');
		btnGenerarPdfD.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPdfD = Ext.getCmp('btnBajarPdfD');
			btnBajarPdfD.show();
			btnBajarPdfD.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPdfD.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPdfD.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarConsultaDescuento = function(store, arrRegistros, opts) 	{	
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridDescuento.isVisible()) {
				gridDescuento.show();
			}	
			
			var jsonData = store.reader.jsonData;	
			var sFechaVencPyme = jsonData.sFechaVencPyme;	
			var firmaMancomunada = jsonData.firmaMancomunada;				
			var cm = gridDescuento.getColumnModel();
			
			if(sFechaVencPyme==''){
					gridDescuento.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENC_PROVEDOR'), true);	
			}
			if(firmaMancomunada!='S'){
				gridDescuento.getColumnModel().setHidden(cm.findColumnIndex('CAMBIO_ESTATUS'), true);	
			}
		
			var btnGenerarPdfD = Ext.getCmp('btnGenerarPdfD');
			var btnBajarPdfD = Ext.getCmp('btnBajarPdfD');
			var btnTotales5 =	Ext.getCmp('btnTotales5');
				
			var el = gridDescuento.getGridEl();						
			if(store.getTotalCount() > 0) {		
				btnTotales5.enable();	
				btnGenerarPdfD.enable();				
				btnBajarPdfD.hide();
				el.unmask();
			} else {	
				btnTotales5.disable();	
				btnGenerarPdfD.disable();				
				btnBajarPdfD.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	
	var totales5Data = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales',
			cveEstatus: 5
		},
		fields: [
			{name: 'MONEDA', mapping: 'MONEDA'},			
			{name: 'TOTAL_MONTO', type: 'float', mapping: 'TOTAL_MONTO'},			
			{name: 'MONTO_DESCONTAR', type: 'float', mapping: 'MONTO_DESCONTAR'}		
	
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});
	
	var gridTotales5 = {
		xtype: 'grid',
		store: totales5Data,
		id: 'gridTotales5',		
		columns: [	
			{
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				width: 200,
				align: 'left'			
			},						
			{
				header: 'Total Monto',
				dataIndex: 'TOTAL_MONTO',
				width: 150,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},			
			{
				header: 'Total Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 300,
		width: 943,
		title: '',
		frame: false
	};
	
	var consultaDescuento = new Ext.data.JsonStore({
		root : 'registros',
		url : '13reporte02Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
			fields: [
				{name: 'NOMBRE_PROVEEDOR'},
				{name: 'NO_DOCUMENTO'},
				{name: 'FECHA_EMISION'},
				{name: 'FECHA_VENC'},
				{name: 'FECHA_VENC_PROVEDOR'},
				{name: 'MONEDA'},
				{name: 'MONTO'},
				{name: 'PORC_DESCUENTO'},
				{name: 'MONTO_DESCUENTO'},
				{name: 'NUMERO_ACUSE'},
				{name: 'BENEFICIARIO'},
				{name: 'PORC_BENEFICIARIO'},
				{name: 'IMPORTE_RECIBIR'},
				{name: 'CAMBIO_ESTATUS'}
			],
			totalProperty : 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
				load: procesarConsultaDescuento,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarConsultaDescuento(null, null, null);						
					}
				}
			}
		});	
		
	var gridDescuento = new Ext.grid.EditorGridPanel({	 
		id: 'gridDescuento',
		store: consultaDescuento,		
		margins: '20 0 0 0',
		hidden: true,
		title: 'Estatus: Descuento F�sico',
		columns: [			
			{
				header: 'Nombre Proveedor', 
				tooltip: 'Nombre Proveedor',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'
			},
			{
				header: 'N�mero de Documento', 
				tooltip: 'N�mero de Documento',
				dataIndex: 'NO_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Fecha Emisi�n', 
				tooltip: 'Fecha Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento', 
				tooltip: 'Fecha Vencimiento',
				dataIndex: 'FECHA_VENC',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'center'
			},
			{
				header: 'Fecha Vencimiento Proveedor', 
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex: 'FECHA_VENC_PROVEDOR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'
			},
			{
				header: 'Moneda', 
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'
			},
			{
				header: 'Monto', 
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento', 
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'PORC_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Monto a Descontar', 
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')				
			},
			{
				header: 'N�mero de Acuse', 
				tooltip: 'N�mero de Acuse',
				dataIndex: 'NUMERO_ACUSE',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center'			
			},
			{
				header: 'Beneficiario', 
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,			
				align: 'left'			
			},
			{
				header: 'Porcentaje Beneficiario', 
				tooltip: 'Porcentaje Beneficiario',
				dataIndex: 'PORC_BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Importe a Recibir Beneficiario', 
				tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'IMPORTE_RECIBIR',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo de Cambio de Estatus No se encontr� registros', 
				tooltip: 'Tipo de Cambio de Estatus No se encontr� registros',
				dataIndex: 'CAMBIO_ESTATUS',
				sortable: true,
				resizable: true	,
				width: 130,				
				align: 'left'				
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,		
		frame: true,
		bbar: {
			items: [
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales5',
					hidden: false,
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winTotales5');
						if (ventana) {
							ventana.show();
						} else {
								totales5Data.load();						
							new Ext.Window({
								layout: 'fit',
								width: 943,
								height: 105,
								id: 'winTotales5',
								closeAction: 'hide',
								items: [
									gridTotales5
								],								
								title: 'Totales Descuento F�sico'
							}).show().alignTo(gridDescuento.el);
						}
					}
				},				
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfD',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02Ext.data.jsp',
							params: {
								informacion:'ArchivoPDF',
								cveEstatus:	cveEstatus
							}
							,callback: procesarSuccessFailureArchivoDescuento
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar PDF',
					id: 'btnBajarPdfD',
					hidden: true
				}
			]
		}
	});


	//Panel de valores iniciales
	var dataStatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['5','Descuento f�sico'],
			['4','Baja'],
			['6','Pagado Anticipado'],
			['8','Modificaci�n de Montos y/o Fechas de Vencimiento'],
			['28','Aplicaci�n de Nota de Cr�dito'],
			['29','Programado a Negociable'],
			['40','Pre negociable a Negociable']		
		 ]
	});


	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'status',	
			id: 'cmbStatus',	
			fieldLabel: 'Estatus', 
			mode: 'local',	
			hiddenName : 'status',	
			emptyText: 'Seleccione Estatus . . . ',
			forceSelection : true,	triggerAction : 'all',	typeAhead: true,
			minChars : 1,	
			store : dataStatus,	
			displayField : 'descripcion',	valueField : 'clave',
			listeners: {
				select: {
					fn: function(combo) {
						cveEstatus = combo.getValue();					
						fp.el.mask('Enviando...', 'x-mask-loading');
						
						Ext.getCmp('btnGenerarPdfD').enable();
						Ext.getCmp('btnBajarPdfD').hide();
						Ext.getCmp('btnGenerarPdfB').enable();
						Ext.getCmp('btnBajarPdfB').hide();
						Ext.getCmp('btnGenerarPdfP').enable();
						Ext.getCmp('btnBajarPdfP').hide();
						Ext.getCmp('btnGenerarPdfN').enable();
						Ext.getCmp('btnBajarPdfN').hide();						
						Ext.getCmp('btnGenerarPdfPro').enable();
						Ext.getCmp('btnBajarPdfPro').hide();
						Ext.getCmp('btnGenerarPdfPre').enable();
						Ext.getCmp('btnBajarPdfPre').hide();
						
						// se ocultan todos los grid 
						Ext.getCmp('gridDescuento').hide();
						Ext.getCmp('gridBaja').hide();
						Ext.getCmp('gridPagado').hide();
						Ext.getCmp('gridModificado').hide();
						Ext.getCmp('gridNota').hide();
						Ext.getCmp('gridProgramado').hide();
						Ext.getCmp('gridPreNegociable').hide();
						
						var winTotales5 = Ext.getCmp('winTotales5');
						if (winTotales5) {	 winTotales5.destroy();  		}						
						var winTotales4 = Ext.getCmp('winTotales4');
						if (winTotales4) {	winTotales4.destroy(); 			}
						var winTotales6 = Ext.getCmp('winTotales6');
						if (winTotales6) {	winTotales6.destroy(); 			}
						var winTotales29 = Ext.getCmp('winTotales29');
						if (winTotales29) {	winTotales29.destroy(); 		}						
						var winTotales8 = Ext.getCmp('winTotales8');
						if (winTotales8) {  winTotales8.destroy();  		}						
						var winTotales40 = Ext.getCmp('winTotales40');
						if (winTotales40) {	 winTotales40.destroy(); 		}
						
						if (combo.value == "5")	{			
							gridDescuento.setTitle('<div><div style="float:left">Estatus: Descuento f�sico</div><div style="float:right">Fecha: ' + fecha + '</div></div>');						
							consultaDescuento.load({ 	params: {  	cveEstatus:cveEstatus		 }	});
						}
						if (combo.value == "4")	{		
							gridBaja.setTitle('<div><div style="float:left">Estatus: Baja</div><div style="float:right">Fecha: ' + fecha + '</div></div>');
							consultaBaja.load({  params: {	cveEstatus:cveEstatus		}	 });
						}
						if (combo.value == "6")	{	
							gridPagado.setTitle('<div><div style="float:left">Estatus: Pagado Anticipado</div><div style="float:right">Fecha: ' + fecha + '</div></div>');
							consultaPagado.load({ 	params: { cveEstatus:cveEstatus		}		});
						}		
						
						if (combo.value == "8")	{			
							gridModificado.setTitle('<div><div style="float:left">Estatus: Modificaci�n de Montos y/o Fechas de Vencimiento</div><div style="float:right">Fecha: ' + fecha + '</div></div>');
							consultaModificado.load({ 	params: { 	cveEstatus:cveEstatus				}		});
						}		
						
						if (combo.value == "28")	{			
							gridNota.setTitle('<div><div style="float:left">Estatus: Aplicaci�n de Nota de Cr�dito</div><div style="float:right">Fecha: ' + fecha + '</div></div>');
							consultaNota.load({ 	params: {  cveEstatus:cveEstatus	 }	});
						}		
						
						if (combo.value == "29")	{		
							gridProgramado.setTitle('<div><div style="float:left">Estatus: Programado a Negociable</div><div style="float:right">Fecha: ' + fecha + '</div></div>');
							consultaProgramado.load({ 	params: { 	cveEstatus:cveEstatus			}	 });
						}	
						if (combo.value == "40")	{	
							gridPreNegociable.setTitle('<div><div style="float:left">Estatus: Pre negociable a Negociable</div><div style="float:right">Fecha: ' + fecha + '</div></div>');					
							consultaPreNegociable.load({ 	params: { 	cveEstatus:cveEstatus		}	 	});
						}							
					}
				}
			}
		}
	];	



//----------------------------------PRINCIPAL-----------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		frame: true,
		border: false,
		title: '<div><center>Cambios de Estatus</div>',
		collapsible: true,
		titleCollapse: false,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',		
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:	elementosForma,
		monitorValid: true
	});

//Simulacion en un contenedor
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	
			NE.util.getEspaciador(20),
			gridDescuento,
			gridBaja,
			gridPagado,
			gridModificado,
			gridNota,
			gridProgramado,
			gridPreNegociable,
			NE.util.getEspaciador(20)
			
		]
	});

});