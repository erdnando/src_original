<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,
		netropology.utilerias.*,			
		com.netro.cadenas.*,			
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>  
<%@ include file="/appComun.jspf" %>
<%@ include file="/01principal/01secsession.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null) ? request.getParameter("informacion") : "";
String infoRegresar ="", consulta ="",  mensaje ="";
JSONObject jsonObj = new JSONObject();

if (informacion.equals("Consultar") ) {
	
	ConsSinNumProv paginador = new ConsSinNumProv();
	paginador.setTxtCadProductiva(iNoCliente);
	paginador.setNum_electronico("");
	paginador.setFec_ini("");
	paginador.setFec_fin("");
	paginador.setConvenio_unico("");
		  
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador );

	try {
		Registros reg	=	queryHelper.doSearch();
		consulta	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		mensaje =  "Cuenta con "+ reg.getNumeroRegistros() +" Pymes que requieren número de proveedor ";
	}	catch(Exception e) {
		throw new AppException("Error en generación de la consulta ", e);
	}	
		
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("mensaje",mensaje);
	infoRegresar= jsonObj.toString();
	
}else if (informacion.equals("Continuar")) {
	List pantallasNavegacionComplementaria = (List)session.getAttribute("inicializar.PantallasComplementarias");
	pantallasNavegacionComplementaria.remove("/13descuento/13epo/13pymeSinNumProv_popExt.jsp");  //El nombre debe coincidir con el especificado en Navegacion

	infoRegresar = "{\"success\": true, \"urlPagina\": \"" + appWebContextRoot + pantallasNavegacionComplementaria.get(0) + "\"}";
}

%>
<%=infoRegresar%>