Ext.onReady(function() {

	// Variables para renderizar los campos del grid
	var SOperaFactConMandato = '';
	var SOperaFactorajeVencInfonavit = '';
	var SOperaFactorajeVencido = '';

	var renderNombreIF = function (value, metaData, record, rowIndex, colIndex, store){
		var nombreIf = '';
		if(SOperaFactorajeVencido == 'S' || SOperaFactorajeVencInfonavit == 'S'){
			if(record.data.CS_DSCTO_ESPECIAL == 'V' || record.data.CS_DSCTO_ESPECIAL == 'I' || record.data.CS_DSCTO_ESPECIAL == 'A'){
				nombreIf = value;
			}
		}
		if(SOperaFactConMandato == 'S' && record.data.CS_DSCTO_ESPECIAL == 'M'){
			nombreIf = value;
		}
		if(record.data.IC_CAMBIO_ESTATUS == '29'){
			nombreIf = value;
		}
		if(record.data.IC_CAMBIO_ESTATUS == '8'){
			if(SOperaFactConMandato == 'S' || SOperaFactorajeVencInfonavit == 'S' || SOperaFactorajeVencido == 'S'){
				return nombreIf;
			}
		} else{
			if(SOperaFactConMandato == 'S' || SOperaFactorajeVencInfonavit == 'S' || SOperaFactorajeVencido == 'S' || record.data.IC_CAMBIO_ESTATUS == '29'){
				return nombreIf;
			} else{
				return '';
			}
		}
		return value;
	};

	var renderMontoAnterior = function (value, metaData, record, rowIndex, colIndex, store){
		var res= Ext.util.Format.numberRenderer('$0,0.00');
		if(record.data.IC_CAMBIO_ESTATUS == '28')
			return res(value);
		else
			return res('0.0');
	};

	//DESCARGA ARCHIVO CSV TODOS LOS REGISTROS
	var procesarSuccessFailureArchivoCSV = function(opts, success, response){
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
			var resp = Ext.util.JSON.decode(response.responseText);
			if(resp.estatusArchivo == 'FINAL'){
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
				btnGenerarCSV.setText('Generar Archivo');
				btnGenerarCSV.enable();
				btnGenerarCSV.setIconClass('icoXls');
			} else if(resp.estatusArchivo == 'ERROR'){
				Ext.Msg.alert('Error','No se gener� el archivo',function(btn){
					btnGenerarCSV.setText('Generar Archivo');
					btnGenerarCSV.enable();
					btnGenerarCSV.setIconClass('icoXls')
				});
			} else{
				btnGenerarCSV.setIconClass('loading-indicator');
				btnGenerarCSV.setText(resp.porcentaje);
				Ext.Ajax.request({
					url: '13consulta03ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion:'ArchivoCSV',
						estatusArchivo: 'PROCESANDO'
					})
					,callback: procesarSuccessFailureArchivoCSV
				});
			}
		} else{
			btnGenerarCSV.setText('Generar Archivo');
			btnGenerarCSV.enable();
			btnGenerarCSV.setIconClass('icoXls')
			NE.util.mostrarConnError(response,opts);
		}
	}

	//DESCARGA ARCHIVO PDF POR PAGINA 
	var procesarSuccessFailureArchivoPDF =  function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = Ext.util.JSON.decode(response.responseText);
			if(resp.estatusArchivo == 'FINAL'){
				var infoR = Ext.util.JSON.decode(response.responseText);
				var archivo = infoR.urlArchivo;
				archivo = archivo.replace('/nafin','');
				var params = {nombreArchivo: archivo};
				fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
				fp.getForm().getEl().dom.submit();
				Ext.getCmp('btnGenerarPDF').setText('Generar Todo');
				Ext.getCmp('btnGenerarPDF').enable();
				Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
			} else if(resp.estatusArchivo == 'ERROR'){
				Ext.Msg.alert('Error','No se gener� el archivo',function(btn){
					Ext.getCmp('btnGenerarPDF').setText('Generar Todo');
					Ext.getCmp('btnGenerarPDF').enable();
					Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf')
				});
			} else{
				Ext.getCmp('btnGenerarPDF').setIconClass('loading-indicator');
				Ext.getCmp('btnGenerarPDF').setText(resp.porcentaje);
				Ext.Ajax.request({
					url: '13consulta03ext.data.jsp',
					params: Ext.apply(fp.getForm().getValues(),{
						informacion:'ArchivoPDF',
						estatusArchivo: 'PROCESANDO'
					})
					,callback: procesarSuccessFailureArchivoPDF
				});
			}
		} else{
			Ext.getCmp('btnGenerarPDF').setText('Generar Todo');
			Ext.getCmp('btnGenerarPDF').enable();
			Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf')
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}
			var jsonData = store.reader.jsonData;
			var sFechaVencPyme = jsonData.sFechaVencPyme;
			var STipoFactoraje = jsonData.STipoFactoraje;
			var ic_cambio_estatus = jsonData.ic_cambio_estatus;
			SOperaFactConMandato = jsonData.SOperaFactConMandato;
			SOperaFactorajeVencInfonavit = jsonData.SOperaFactorajeVencInfonavit;
			SOperaFactorajeVencido = jsonData.SOperaFactorajeVencido;

			var cm = gridConsulta.getColumnModel();

			if(sFechaVencPyme!=''){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('FECHA_VENCIMIENTO_PYME'), false);
			}
			if(STipoFactoraje==='S' ||  ic_cambio_estatus ==29){
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
			}

			if(STipoFactoraje =='S'  || ic_cambio_estatus ===29  || SOperaFactConMandato=='S' || SOperaFactorajeVencInfonavit =='S') {
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NOMBRE_IF'), false);
			}
			if(SOperaFactConMandato=='S'){
				gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('MANDANTE'), false);
			}

			var btnGenerarCSV = Ext.getCmp('btnGenerarCSV');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			
			var el = gridConsulta.getGridEl();
			if(store.getTotalCount() > 0) {

			resumenTotalesData.load({
					params: Ext.apply(fp.getForm().getValues(),{
						informacion: 'ResumenTotales'	
					})
				});

				btnGenerarCSV.enable();
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarCSV.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarResumenTotalesData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		//fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridTotales.isVisible()) {
				gridTotales.show();
			}
						
			var jsonData = store.reader.jsonData;				
			
			var el = gridTotales.getGridEl();			
			if(store.getTotalCount() > 0) {					
				el.unmask();
			} else {			
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var resumenTotalesData = new Ext.data.JsonStore({			
			root : 'registros',
			url : '13consulta03ext.data.jsp',
			baseParams: {
				informacion: 'ResumenTotales'			
			},
			fields: [
				{name: 'MONEDA', mapping: 'MONEDA'},
				{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'TOTAL_REGISTROS'},
				{name: 'MONTO_TOTAL', type: 'float', mapping: 'MONTO_TOTAL'},
				{name: 'MONTO_DESCUENTO', type: 'float', mapping: 'MONTO_DESCUENTO'}		
				
			],
			totalProperty : 'total',
			autoLoad: false,	
			listeners: {
				load: procesarResumenTotalesData,			
				exception: {
					fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
						//LLama procesar consulta, para que desbloquee los componentes.
						procesarResumenTotalesData(null, null, null);						
					}
				}
			}
		});

	
	var gridTotales = new Ext.grid.EditorGridPanel({
		id: 'gridTotales',
		title: '',
		hidden: true,
		margins: '20 0 0 0',
		align: 'center',
		store: resumenTotalesData,
		clicksToEdit: 1,		
			columns: [	
				{
					header: 'Moneda',
					dataIndex: 'MONEDA',
					width: 200,
					align: 'left'			
				},
				{
					header: 'N�mero de documentos',
					dataIndex: 'TOTAL_REGISTROS',
					width: 150,
					align: 'center'						
				},
				{
					header: 'Total Monto',
					dataIndex: 'MONTO_TOTAL',
					width: 150,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')		
				},
					{
					header: 'Total Monto con descuento',
					dataIndex: 'MONTO_DESCUENTO',
					width: 250,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')		
				}			
			],
			height: 120,		
			width: 900,			
			frame: false
		});
		
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NOMBRE_PYME'},
			{name: 'NUM_DOCUMENTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'FECHA_VENCIMIENTO_PYME'},
			{name: 'FECHA_CAMBIO'},
			{name: 'MONEDA'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'TIPO_CAMBIO'},
			{name: 'CAUSA'},
			{name: 'USUARIO'},
			{name: 'MONTO_ANTERIOR'},
			{name: 'NOMBRE_IF'},
			{name: 'MANDANTE'},
			{name: 'IC_CAMBIO_ESTATUS'},
			{name: 'CS_DSCTO_ESPECIAL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});	
	

	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		title: 'Cambios de Estatus',
		hidden: true,
		margins: '20 0 0 0',
		align: 'center',
		store: consultaData,
		clicksToEdit: 1,		
		columns: [			
			{
				header: 'Proveedor', 
				tooltip: 'Proveedor',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�m. Documento', 
				tooltip: 'N�m. Documento',
				dataIndex: 'NUM_DOCUMENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
		},
		{
			header: 'Fecha Emisi�n', 
			tooltip: 'Fecha Emisi�n',
			dataIndex: 'FECHA_EMISION',
			sortable: true,
			resizable: true	,
			width: 130,
			hidden: false,
			align: 'center'
		},
		{
			header: 'Fecha de Vencimiento', 
			tooltip: 'Fecha de Vencimiento',
			dataIndex: 'FECHA_VENCIMIENTO',
			sortable: true,
			resizable: true	,
			width: 130,
			hidden: false,
			align: 'center'
		}	,
		{
			header: 'Fecha Vencimiento Proveedor', 
			tooltip: 'Fecha Vencimiento Proveedor',
			dataIndex: 'FECHA_VENCIMIENTO_PYME',
			sortable: true,
			resizable: true	,
			width: 130,
			hidden: true,
			align: 'center'
		}	,
		{
			header: 'Fecha de cambio de estatus', 
			tooltip: 'Fecha de cambio de estatus',
			dataIndex: 'FECHA_CAMBIO',
			sortable: true,
			resizable: true	,
			width: 130,
			hidden: false,
			align: 'center'
		},
		{
			header: 'Moneda', 
			tooltip: 'Moneda',
			dataIndex: 'MONEDA',
			sortable: true,
			resizable: true	,
			width: 130,
			hidden: false,
			align: 'center'
		},
		{
			header: 'Tipo Factoraje',
			tooltip: 'Tipo Factoraje',
			dataIndex: 'TIPO_FACTORAJE',
			sortable: true,
			resizable: true	,
			width: 130,
			hidden: true,
			align: 'left'
		},
		{
			header: 'Monto',
			tooltip: 'Monto',
			dataIndex: 'MONTO',
			sortable: true,
			resizable: true,
			width: 130,
			hidden: false,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header: 'Monto a Descontar', 
			tooltip: 'Monto a Descontar',
			dataIndex: 'MONTO_DESCONTAR',
			sortable: true,
			resizable: true	,
			width: 130,
			hidden: false,
			align: 'right',
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{
			header: 'Tipo de cambio de estatus', 
			tooltip: 'Tipo de cambio de estatus',
			dataIndex: 'TIPO_CAMBIO',
			sortable: true,
			resizable: true	,
			width: 130,
			hidden: false,
			align: 'left'		
		},
		{
			header: 'Causa', 
			tooltip: 'Causa',
			dataIndex: 'CAUSA',
			sortable: true,
			resizable: true	,
			width: 130,
			hidden: false,
			align: 'left'		
		},
		{
			header: 'Usuario', 
			tooltip: 'Usuario',
			dataIndex: 'USUARIO',
			sortable: true,
			resizable: true	,
			width: 130,
			hidden: false,
			align: 'left'		
		},		
		{
			header: 'Monto Anterior', 
			tooltip: 'Monto Anterior',
			dataIndex: 'MONTO_ANTERIOR',
			sortable: true,
			resizable: true,
			width: 130,
			hidden: false,
			align: 'right',
			renderer: renderMontoAnterior
		}	,
		{
			header: 'Nombre IF',
			tooltip: 'Nombre IF',
			dataIndex: 'NOMBRE_IF',
			sortable: true,
			resizable: true,
			width: 130,
			hidden: true,
			align: 'left',
			renderer: renderNombreIF
		},
		{
			header: 'Mandante', 
			tooltip: 'Mandante',
			dataIndex: 'MANDANTE',
			sortable: true,
			resizable: true,
			width: 130,
			hidden: true,
			align: 'left'
		}	
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 900,
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->',
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id: 'btnGenerarCSV',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta03ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV',
							estatusArchivo: 'INICIO'
						})
						,callback: procesarSuccessFailureArchivoCSV
						});
					}
				},'-',{
					xtype:   'button',
					text:    'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id:      'btnGenerarPDF',
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta03ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								informacion:'ArchivoPDF',
								estatusArchivo: 'INICIO'
							})
							,callback: procesarSuccessFailureArchivoPDF
						});
					}
				}
			]
		}
	});

	//valida La Clave del Proveedor:  
	function procesaValidaPYME(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			var mensajeBusquedaDatosPYME = jsonValoresIniciales.mensajeBusquedaDatosPYME;
			var ic_pyme = jsonValoresIniciales.ic_pyme ;
			var cg_pyme_epo_interno = jsonValoresIniciales.cg_pyme_epo_interno;
			var txtCadenasPymes = jsonValoresIniciales.txtCadenasPymes; 			
			
			if(mensajeBusquedaDatosPYME !=''){
				Ext.MessageBox.alert("Mensaje",mensajeBusquedaDatosPYME);  
				Ext.getCmp("cg_pyme_epo_interno1").setValue('');
				Ext.getCmp("txtCadenasPymes1").setValue('');			
				Ext.getCmp("ic_pyme1").setValue('');	
			}else {
				Ext.getCmp("cg_pyme_epo_interno1").setValue(cg_pyme_epo_interno);
				Ext.getCmp("txtCadenasPymes1").setValue(txtCadenasPymes);			
				Ext.getCmp("ic_pyme1").setValue(ic_pyme);	
			}
			fp.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
			
	var catalogoMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo			
		}		
	});
	
	var catalogoEstatusData = new Ext.data.JsonStore({
		id: 'catalogoMonedaDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	
	var procesarBusqueda = function(store, arrRegistros, opts) 	{
		if (arrRegistros == '') {
			Ext.MessageBox.alert("Mensaje","No existe informaci�n con los criterios determinados ");				
		}
	}
	
	var storeBusqAvanzPyme = new Ext.data.JsonStore({
		id: 'storeBusqAvanzPyme',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'busquedaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,	
		listeners: {			
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo	,
			load : procesarBusqueda
		}		
	});
	
	var elementsFormBusq = [
		{
			xtype: 'textfield',
			name: 'nombrePyme',
			id: 'nombrePyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcPyme',
			id: 'rfcPyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'noPyme',
			id: 'noPyme1',
			fieldLabel: 'N�mero Proveedor',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 'button',
						text: 'Buscar',
						id: 'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cbPyme1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);							
							storeBusqAvanzPyme.load({
									params: Ext.apply(fpBusqAvanzada.getForm().getValues())
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{
			xtype: 'combo',
			name: 'cbPyme',
			id: 'cbPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cbPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusqAvanzPyme
		}
		
	];
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: elementsFormBusq,
		monitorValid: true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cmbPyme= Ext.getCmp("cbPyme1");
					var storeCmb = cmbPyme.getStore();
					var cg_pyme_epo_interno = Ext.getCmp('cg_pyme_epo_interno1');
					var txtCadenasPymes = Ext.getCmp('txtCadenasPymes1');
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cmbPyme.getValue())) {
						cmbPyme.markInvalid('Seleccione Proveedor');
						return;
					}
					var record = cmbPyme.findRecord(cmbPyme.valueField, cmbPyme.getValue());
					record =  record ? record.get(cmbPyme.displayField) : cmbPyme.valueNotFoundText;
					cg_pyme_epo_interno.setValue(cmbPyme.getValue());					
				
					txtCadenasPymes.setValue(record);				 
							
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
				
			}
		]
	});		
	
	
	var elementosForma = [	
		{
			xtype: 'compositefield',
			fieldLabel: 'Clave del Proveedor',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'textfield',
					name: 'cg_pyme_epo_interno',
					id: 'cg_pyme_epo_interno1',
					fieldLabel: 'Clave del Proveedor',
					allowBlank: true,
					maxLength: 25,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0',  //necesario para mostrar el icono de error
					listeners:{
						blur: function(field){ 												
								ValidaPyme(field.getValue());						
						}			
					}
				},
				{
					xtype: 'textfield',
					name: 'ic_pyme',
					id: 'ic_pyme1',
					fieldLabel: 'ic_pyme',
					allowBlank: true,
					maxLength: 25,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					hidden: true,
					msgTarget: 'side',
					margins: '0 20 0 0' //necesario para mostrar el icono de error				
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Nombre Proveedor',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'textfield',
					name: 'txtCadenasPymes',
					id: 'txtCadenasPymes1',
					fieldLabel: 'Nombre Proveedor',
					allowBlank: true,
					maxLength: 250,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 350,
					disabled: true,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}		
			]
		},
		{
			xtype: 'compositefield',
			//fieldLabel: ' ',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'numberfield',
					name: 'num_electronico',
					id: 'num_electronico1',
					//fieldLabel: 'N�mero Nafin Electr�nico',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					hidden: true,
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
				xtype: 'button',
				text: 'B�squeda Avanzada...',
				id: 'btnBusqAv',
				handler: function(boton, evento) {
					var ventana = Ext.getCmp('winBusqAvan');
					if (ventana) {
						ventana.show();
					} else {
						new Ext.Window({
								title: 			'B�squeda Avanzada',
								layout: 			'fit',
								width: 			400,
								height: 			300,
								minWidth: 		400,
								minHeight: 		300,
								buttonAlign: 	'center',
								id: 				'winBusqAvan',
								closeAction: 	'hide',
								items: 	fpBusqAvanzada
							}).show();
					}
				}
			}
			]
		},
		
	
		{
			xtype: 'compositefield',
			fieldLabel: 'Num. Documento',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'textfield',
					name: 'ig_numero_docto',
					id: 'ig_numero_docto1',
					fieldLabel: 'Num. Documento',
					allowBlank: true,
					maxLength: 25,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento de',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_vencMin',
					id: 'df_fecha_vencMin',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_vencMax',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_vencMax',
					id: 'df_fecha_vencMax',
					allowBlank: true,
					startDay: 1,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_vencMin',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Moneda',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'combo',
					name: 'ic_moneda',
					id: 'ic_moneda1',
					fieldLabel: 'Moneda',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_moneda',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 250,
					minChars : 1,
					allowBlank: true,
					store : catalogoMonedaData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Monto de',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'numberfield',
					name: 'fn_montoMin',
					id: 'fn_montoMin',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',					
					campoFinFecha: 'fn_montoMax',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'numberfield',
					name: 'fn_montoMax',
					id: 'fn_montoMax',
					allowBlank: true,
					startDay: 1,
					width: 150,
					msgTarget: 'side',					
					campoInicioFecha: 'fn_montoMin',
					margins: '0 20 0 0'  
				}
			]
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Tipo de cambio de estatus ',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'combo',
					name: 'ic_cambio_estatus',
					id: 'ic_cambio_estatus1',
					fieldLabel: 'Tipo de cambio de estatus',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_cambio_estatus',
					emptyText: 'Seleccione...',					
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 250,
					minChars : 1,
					allowBlank: true,
					store : catalogoEstatusData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de cambio de estatus',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'dc_fecha_cambioMin',
					id: 'dc_fecha_cambioMin',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'dc_fecha_cambioMax',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'dc_fecha_cambioMax',
					id: 'dc_fecha_cambioMax',
					allowBlank: true,
					startDay: 1,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'dc_fecha_cambioMin',
					margins: '0 20 0 0'  
				}
			]
		}				
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Criterios de B�squeda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,				
		buttons: [
			{
				text: 'Buscar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
					
					var ic_cambio_estatus = Ext.getCmp("ic_cambio_estatus1");
					var fn_montoMin = Ext.getCmp("fn_montoMin");
					var fn_montoMax = Ext.getCmp("fn_montoMax");
				
					if(parseFloat(fn_montoMin.getValue()) > parseFloat(fn_montoMax.getValue())){ 
						fn_montoMax.markInvalid("El monto final debe de ser mayor o igual al inicial");		
					return;
					}
					
					if (Ext.isEmpty(ic_cambio_estatus.getValue()) ) {
						ic_cambio_estatus.markInvalid('Debe seleccionar un tipo de cambio de estatus.');
						return;
					}	
					
					var df_fecha_vencMin = Ext.getCmp('df_fecha_vencMin');
					var df_fecha_vencMax = Ext.getCmp('df_fecha_vencMax');
					var dc_fecha_cambioMin = Ext.getCmp('dc_fecha_cambioMin');
					var dc_fecha_cambioMax = Ext.getCmp('dc_fecha_cambioMax');
					
					if ( ( !Ext.isEmpty(df_fecha_vencMin.getValue())  &&   Ext.isEmpty(df_fecha_vencMax.getValue()) )
						||  ( Ext.isEmpty(df_fecha_vencMin.getValue())  &&   !Ext.isEmpty(df_fecha_vencMax.getValue()) ) ){
						df_fecha_vencMin.markInvalid('Debe capturar ambas Fechas  o dejarlas en blanco');
						df_fecha_vencMax.markInvalid('Debe capturar ambas Fechas o dejarlas en blanco');
						return;
					}
					
					if ( ( !Ext.isEmpty(dc_fecha_cambioMin.getValue())  &&   Ext.isEmpty(dc_fecha_cambioMax.getValue()) )
						||  ( Ext.isEmpty(dc_fecha_cambioMin.getValue())  &&   !Ext.isEmpty(dc_fecha_cambioMax.getValue()) ) ){
						dc_fecha_cambioMin.markInvalid('Debe capturar ambas Fechas  o dejarlas en blanco');
						dc_fecha_cambioMax.markInvalid('Debe capturar ambas Fechas o dejarlas en blanco');
						return;
					}
					
					
					fn_montoMin = Ext.getCmp('fn_montoMin');
					fn_montoMax = Ext.getCmp('fn_montoMax');
					
					if ( ( !Ext.isEmpty(fn_montoMin.getValue())  &&   Ext.isEmpty(fn_montoMax.getValue()) )
						||  ( Ext.isEmpty(fn_montoMin.getValue())  &&   !Ext.isEmpty(fn_montoMax.getValue()) ) ){
						fn_montoMin.markInvalid('Debe capturar ambas Montos o dejarlas en blanco');
						fn_montoMax.markInvalid('Debe capturar ambas Montos o dejarlas en blanco');
						return;
					}
					
					Ext.getCmp('gridTotales').hide();
					
					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							informacion: 'Consultar',
							start:0,
							limit:15
						})
					});
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta03ext.jsp';
				}
			}
		]
	});
	
//-------------------------------- PRINCIPAL -----------------------------------
  var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 949,
	  height: 'auto',
	  items: 
	    [		  
		  NE.util.getEspaciador(20),
			fp,		
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales,
			NE.util.getEspaciador(20)
	    ]
  });
	

	catalogoMonedaData.load();
	catalogoEstatusData.load();


	// valida la Captura dela Clave de Pyme 
	var ValidaPyme=  function(cg_pyme_epo_interno) {	
	
		fp.el.mask('Validando PYME ...', 'x-mask-loading');	
		Ext.Ajax.request({
			url: '13consulta03ext.data.jsp',
			params: {
				informacion: "ValidaClavePYME",
				cg_pyme_epo_interno: cg_pyme_epo_interno		
			},
			callback: procesaValidaPYME				
		});	
	}
	
});