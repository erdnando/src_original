Ext.onReady(function() 
{
//--------------------------------- HANDLERS -------------------------------	    
  var param;
  var form = {
	 camp_dina1:null
	}
  var jsonValoresIniciales = null;
	function procesaValoresIniciales(opts,success,response){
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
		}else{
			NE.util.mostrarConnError(response,opts);
		}
   }
  
  function procesarSuccessFailureParametrizacion(opts, success, response) {
   if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
	 param = Ext.util.JSON.decode(response.responseText);		
	
	if (param.camp_dina1 != undefined){
				Ext.each(param.camp_dina1, function(item, index, arrItems){
					if (item.IC_NO_CAMPO == "1") {
						if (item.CG_TIPO_OBJETO == "T")	{
							if (item.CG_TIPO_DATO == "N")	{
								var config = {xtype: 'numberfield',name: 'camp_dina1',id: 'campDina1',allowBlank: true, anchor: '45%', fieldLabel: item.NOMBRE_CAMPO,
									maxLength: item.IG_LONGITUD,hidden:	true}
							}else{
								var config = {xtype: 'textfield',name: 'camp_dina1',id: 'campDina1',allowBlank: true, anchor: '45%', fieldLabel: item.NOMBRE_CAMPO,
									maxLength: item.IG_LONGITUD,hidden:	true, regex : /^[A-Z0-9 a-z]*$/} //vtype:	'alphanum'
							}
							fp.add(config);
							fp.insert(10,config);
							fp.doLayout();
						}else	{
	                 var config = {
								xtype: 'combo',
								name: 'camp_dina1',
								id: 'campDina1',
								fieldLabel: item.NOMBRE_CAMPO,
								emptyText: 'Seleccionar',
								mode: 'local',
								displayField: 'descripcion',
								valueField: 'clave',
								hiddenName : 'camp_dina1',
								forceSelection : false,
								triggerAction : 'all',
								typeAhead: true,
								minChars : 1,
								store: catalogoCamposAdicionalesData,
								tpl : NE.util.templateMensajeCargaCombo
							}
							fp.add(config);
							fp.insert(10,config);
							fp.doLayout();
							catalogoCamposAdicionalesData.load();
						}
                }
   });
  }
  fp.el.unmask();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
  
  var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) { gridGeneral.show(); }

		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		var btnTotales = Ext.getCmp('btnTotales');
		
		var el = gridGeneral.getGridEl();	
		
		var cm = gridGeneral.getColumnModel();
		if(jsonValoresIniciales.bOperaFactConMandato == true){
		 cm.setHidden(cm.findColumnIndex('MANDANTE'),false);
		}

		if(store.getTotalCount() > 0) {
		   btnTotales.show();
		   btnGenerarArchivo.show();
		   btnGenerarPDF.show();
		   btnGenerarArchivo.enable();
		   btnGenerarPDF.enable();
			
  			el.unmask();
		} else {
		  btnTotales.hide();
		  btnGenerarArchivo.hide();
		  btnGenerarPDF.hide();
		  btnGenerarArchivo.disable();
		  btnGenerarPDF.disable();
		  el.mask('No se encontro ning�n registro', 'x-mask'); 
		  }
	   }
  }

  function procesarMuestraDetalle(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var data = Ext.util.JSON.decode(response.responseText);

			var camposDinamicosDetalle = data.dinamicosDetalle;
			if (camposDinamicosDetalle != undefined){
				var datosDinamicos = data.datos_detalle;
				var count = 1;
				if (datosDinamicos != undefined){
					gridDetalleData.loadData(datosDinamicos);
					if (!gridDetalle.isVisible()) {
						gridDetalle.show();
					}
				}
				
				var cm = gridDetalle.getColumnModel();
				Ext.each(camposDinamicosDetalle, function(item, index, arrItems){
					if (item.IC_NO_CAMPO == "1"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO1'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO1'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO1'), false);}
					if (item.IC_NO_CAMPO == "2"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO2'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO2'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO2'), false);}
					if (item.IC_NO_CAMPO == "3"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO3'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO3'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO3'), false);}
					if (item.IC_NO_CAMPO == "4"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO4'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO4'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO4'), false);}
					if (item.IC_NO_CAMPO == "5"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO5'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO5'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO5'), false);}
					if (item.IC_NO_CAMPO == "6"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO6'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO6'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO6'), false);}
					if (item.IC_NO_CAMPO == "7"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO7'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO7'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO7'), false);}
					if (item.IC_NO_CAMPO == "8"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO8'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO8'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO8'), false);}
					if (item.IC_NO_CAMPO == "9"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO9'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO9'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO9'), false);}
					if (item.IC_NO_CAMPO == "10"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO10'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO10'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO10'), false);}
				});
			}
			
			var ventana = Ext.getCmp('winDetalle');
			if (ventana) {
				Ext.getCmp('btnPdfDetalle').enable();
				Ext.getCmp('btnBajarPdfDetalle').hide();
				ventana.show();
			}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				layout: 'form',
				width: 800,
				autoHeight: true,
				id: 'winDetalle',
				closeAction: 'hide',
				items: [fpDetalle,NE.util.getEspaciador(2),gridDetalle],
				title: 'Detalles del Documento',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',{xtype: 'button',	text: 'Cerrar',	id: 'btnCloseDet'},
										'-',{xtype: 'button',	text: 'Generar PDF',	id: 'btnPdfDetalle'},
										{xtype: 'button',	text: 'Bajar PDF',	id: 'btnBajarPdfDetalle',hidden: true}	]
				}
				}).show();
			}

			Ext.getCmp('disDocto').body.update('');
			Ext.getCmp('disEmision').body.update('');
			Ext.getCmp('disVto').body.update('');
			Ext.getCmp('disImporte').body.update('');
			Ext.getCmp('disMoneda').body.update('');

			if (data.datos_doctos != undefined)	{
				Ext.each(data.datos_doctos, function(item, index, arrItems){
					var disNombre	= Ext.getCmp('disNombre');
					var disRfc		= Ext.getCmp('disRfc');
					var disCalle	= Ext.getCmp('disCalle');
					var disColonia	= Ext.getCmp('disColonia');
					var disCp		= Ext.getCmp('disCp');
					var disEstado	= Ext.getCmp('disEstado');
					var disDelomun	= Ext.getCmp('disDelomun');
					disNombre.setValue('<strong>'+item.CG_RAZON_SOCIAL+'</strong>');
					disRfc.setValue(item.CG_RFC);
					disCalle.setValue(item.CG_CALLE);
					disColonia.setValue(item.CG_COLONIA);
					disCp.setValue(item.CG_CP);
					disEstado.setValue(item.CG_ESTADO);
					disDelomun.setValue(item.CG_MUNICIPIO);

					Ext.getCmp('disDocto').body.update('<div align="right">'+item.IG_NUMERO_DOCTO+'</div>');
					Ext.getCmp('disEmision').body.update('<div align="right">'+item.FECHA_DOCTO+'</div>');
					Ext.getCmp('disVto').body.update('<div align="right">'+item.FECHA_VENC+'</div>');
					Ext.getCmp('disImporte').body.update('<div align="right">'+Ext.util.Format.number(item.FN_MONTO, '$0,0.00')+'</div>');
					Ext.getCmp('disMoneda').body.update('<div align="right">'+item.MONEDA);

					var campo1 = item.CG_CAMPO1;
					var campo2 = item.CG_CAMPO2;
					var campo3 = item.CG_CAMPO3;
					var campo4 = item.CG_CAMPO4;
					var campo5 = item.CG_CAMPO5;
					var camposDinamicos = param.camp_dina1;
					var contenedorIzq = Ext.getCmp('Izquierda');
					if (camposDinamicos != undefined){
						var config = [];
						var cont = 1;
						Ext.each(camposDinamicos, function(item, index, arrItems){
							if (item.IC_NO_CAMPO == "1") {
								config = {xtype: 'displayfield',name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),allowBlank: true,fieldLabel: item.NOMBRE_CAMPO,
								maxLength: item.IG_LONGITUD,hidden:	false, vtype:	'alphanum',value:	campo1}
							}
							if (item.IC_NO_CAMPO == "2") {
								config = {xtype: 'displayfield',name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),allowBlank: true,fieldLabel: item.NOMBRE_CAMPO,
								maxLength: item.IG_LONGITUD,hidden:	false, vtype:	'alphanum',value:	campo2}
							}
							if (item.IC_NO_CAMPO == "3") {
								config = {xtype: 'displayfield',name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),allowBlank: true,fieldLabel: item.NOMBRE_CAMPO,
								maxLength: item.IG_LONGITUD,hidden:	false, vtype:	'alphanum',value:	campo3}
							}
							if (item.IC_NO_CAMPO == "4") {
								config = {xtype: 'displayfield',name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),allowBlank: true,fieldLabel: item.NOMBRE_CAMPO,
								maxLength: item.IG_LONGITUD,hidden:	false, vtype:	'alphanum',value:	campo4}
							}
							if (item.IC_NO_CAMPO == "5") {
								config = {xtype: 'displayfield',name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),allowBlank: true,fieldLabel: item.NOMBRE_CAMPO,
								maxLength: item.IG_LONGITUD,hidden:	false, vtype:	'alphanum',value:	campo5}
							}
							contenedorIzq.add(config);
							contenedorIzq.insert(8+index,config);
							contenedorIzq.doLayout();
						});
					}
				});
			}
					
			Ext.getCmp('btnPdfDetalle').setHandler(
				function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta6ext_det_pdf.jsp',
						params: Ext.apply({ic_docto:	opts.params.ic_documento }),
						callback: procesarGenerarPdfDetalle
					});
				}
			)

			Ext.getCmp('btnCloseDet').setHandler(
				function(boton, evento) {
					Ext.getCmp('winDetalle').hide();
				}
			);

		}else {
			NE.util.mostrarConnError(response,opts);
		}
  }
  
  var catalogoCamposAdicionalesData = new Ext.data.JsonStore({
		id: 'catalogoCamposAdicionalesDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta6ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCamposAdicionales'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
/*  
  var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) 
  {
	 var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
	 btnGenerarArchivo.setIconClass('');
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
		btnBajarArchivo.show();
		btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnBajarArchivo.setHandler(function(boton, evento) 
		{
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
	  } 
	 else 
	 {
		btnGenerarArchivo.enable();
		NE.util.mostrarConnError(response,opts);
    }
  }

  var procesarSuccessFailureGenerarPDF = function (opts, success, response) 
  {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
		{
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
			
			btnBajarPDF.setHandler(function (boton, evento) 
			{
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		}
		else 
		{
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
  }
*/
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}

	var procesarSuccessFailureGenerarPDF = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}

  var procesarGenerarPdfDetalle =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfDetalle');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfDetalle');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
  
  var muestraDetalle = function(grid, rowIndex, colIndex, item, event)
  {  
	  var registro = grid.getStore().getAt(rowIndex);
	  var ic_documento = registro.get('IC_DOCUMENTO');
	   Ext.getCmp('disNombre').setValue('');
		Ext.getCmp('disRfc').setValue('');
		Ext.getCmp('disCalle').setValue('');
		Ext.getCmp('disColonia').setValue('');
		Ext.getCmp('disCp').setValue('');
		Ext.getCmp('disEstado').setValue('');
		Ext.getCmp('disDelomun').setValue('');
	  
	  var contenedorIzq = Ext.getCmp('Izquierda');
		var ciclo = 0;
		for (ciclo=0; ciclo<5; ciclo ++){
			var dina = Ext.getCmp('dina'+ciclo.toString());
			if (dina != undefined) {contenedorIzq.remove(dina);}
		}
	  
	  Ext.Ajax.request({
	                    url: '13consulta6ext.data.jsp',
							  params: Ext.apply({informacion: "obtenDetalles",ic_documento:ic_documento}),
							  callback: procesarMuestraDetalle
							});
  }
  
//-------------------------------- STORES -----------------------------------
  var consultaData = new Ext.data.JsonStore
  ({
			root: 'registros',
			url: '13consulta6ext.data.jsp',
			baseParams: { informacion: 'Consulta' },
			fields: [
			         {name: 'IC_DOCUMENTO'},
			         {name: 'DF_ALTA'},
						{name: 'CG_PYME_EPO_INTERNO'},
						{name: 'IG_NUMERO_DOCTO'},
						{name: 'DF_FECHA_DOCTO'},
						{name: 'DF_FECHA_VENC'},
						{name: 'CD_NOMBRE'},
						{name: 'FN_MONTO'},
						{name: 'TIPO_FACTORAJE'},
						{name: 'CT_REFERENCIA'},
						{name: 'CG_CAMPO1'},
						{name: 'CG_CAMPO2'},
						{name: 'CS_DETALLE'},
						{name: 'MANDANTE'}
			        ],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
			      beforeLoad:	{fn: function(store, options){
					Ext.apply(options.params, {
					 camp_dina1:	form.camp_dina1
					 });
					}
					},
					load: procesarConsultaData,
					exception: {
								fn: function(proxy,type,action,optionsRequest,response,args){
										NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
										procesarConsultaData(null,null,null); //Llama procesar consulta, para que desbloquee los componentes.
								}
					}
			}
  }); 
  
  var totalesData = new Ext.data.JsonStore({
			root: 'registros',
			url: '13consulta6ext.data.jsp',
			baseParams: { informacion: 'ConsultaTotales' },
			fields: [						
			         {name: 'CD_NOMBRE'},
			         {name: 'TOTALREGISTROS'},
						{name: 'TOTALMONTO'}
			        ],
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
						exception: NE.util.mostrarDataProxyError
			}
	});
	
  	
  var gridDetalleData = new Ext.data.JsonStore({
		fields: [
		         {name: 'CG_CAMPO1'},
					{name: 'CG_CAMPO2'},
					{name: 'CG_CAMPO3'},
					{name: 'CG_CAMPO4'},
					{name: 'CG_CAMPO5'},
					{name: 'CG_CAMPO6'},
					{name: 'CG_CAMPO7'},
					{name: 'CG_CAMPO8'},
					{name: 'CG_CAMPO9'},
					{name: 'CG_CAMPO10'}
				  ],
		data: [
		       {'CG_CAMPO1':'',
				  'CG_CAMPO2':'',
				  'CG_CAMPO3':'',
				  'CG_CAMPO4':'',
				  'CG_CAMPO5':'',
				  'CG_CAMPO6':'',
				  'CG_CAMPO7':'',
				  'CG_CAMPO8':'',
				  'CG_CAMPO9':'',
				  'CG_CAMPO10':''
				 }
				],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
  }); 
  	
//---------------------------------COMPONENTES-------------------------------
  var elementosForma =
  [{
         xtype: 'compositefield',
	      fieldLabel: 'Fecha de Registro',
			combineErrors: false,
			msgTarget: 'side',
			items: [
           {
			   // Fecha Inicio
			   xtype: 'datefield',
            name: 'dFechaRegIni',
            id: 'dFechaRegIni',
				vtype: 'rangofecha',
            campoFinFecho: 'dFechaRegFin',
				allowBlank: false,
				width: 100,
				margins: '0 20 0 0'
           },
			  {
					xtype: 'displayfield',
					value: 'a',
					width: 20
			  },
		     {
			   // Fecha Final
			   xtype: 'datefield',
				vtype: 'rangofecha',
            campoInicioFecha: 'dFechaRegIni',
            name: 'dFechaRegFin',
            id: 'dFechaRegFin',
				allowBlank: true,
				width: 100,
				margins: '0 20 0 0'
           }    
                ]
  }];
  
  var gridGeneral = new Ext.grid.GridPanel
  ({
		store: consultaData,
		stateful: true,
		hidden: true,
      id: 'gridGeneral',
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		title: '',
		frame: true,
		columns: [
		   {
				header: 'Documento',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'center',
				hidden: true,
				dataIndex: 'IC_DOCUMENTO'
			},
			{
				header: 'Fecha de publicaci�n',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'center',
				hidden: false,
				dataIndex: 'DF_ALTA'
			},
			{
				header: 'Proveedor',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'left',
				hidden: false,
				dataIndex: 'CG_PYME_EPO_INTERNO'
			},
			{
				header: 'N�mero de Documento',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'left',
				hidden: false,
				dataIndex: 'IG_NUMERO_DOCTO'
			},
			{
				header: 'Fecha Emisi�n',
				sortable: true,
				resizable: true,
			   width: 130,	
				align: 'center',
				hidden: false,
				dataIndex: 'DF_FECHA_DOCTO'
			},
			{
				header: 'Fecha Vencimiento',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'center',
				hidden: false,
				dataIndex: 'DF_FECHA_VENC'
			},
			{
				header: 'Moneda',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'CD_NOMBRE'
			},
			{
				header: 'Monto',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'FN_MONTO',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Tipo',
				sortable: true,
				resizable: true,
				width: 100,	
				align: 'left',
				hidden: false,
				dataIndex: 'TIPO_FACTORAJE'
			},
			{
				header: 'Referencia',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'CT_REFERENCIA'
			},
			{
				header: 'Adicional 1',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'center',
				hidden: false,
				dataIndex: 'CG_CAMPO1'
			},
			{
				header: 'Adicional 2',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'center',
				hidden: false,
				dataIndex: 'CG_CAMPO2'
			},
			{
				xtype: 'actioncolumn',
				header: 'Detalles',
				tooltip: 'Detalles',
				dataIndex: 'CS_DETALLE',
            width: 130,
				align: 'center',
				items: [
					     {
						   getClass: function(value, metadata, registro, rowIndex, colIndex, store) {								
							 if(registro.get('CS_DETALLE') != 0) {
							  this.items[0].tooltip = 'Ver';								
							  return 'iconoLupa';	
							 }
						   },
						  handler: muestraDetalle
					     }
				       ]	
			},
			{
				header: 'Mandante',
				sortable: true,
				resizable: true,
				width: 130,
				align: 'center',
				hidden: true,
				dataIndex: 'MANDANTE'
			}
		],
			bbar:{		
				xtype: 'paging',
				pageSize: 15,
				buttonAlign: 'left',
				id: 'barraPaginacion',
				displayInfo: true,
				store: consultaData,
				displayMsg: '{0} - {1} de {2}',
				emptyMsg: 'No hay registros.',
				items: ['->', '-',{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function (boton,evento) {
						totalesData.load({ params: Ext.apply(fp.getForm().getValues()) });
						var gridTotales = Ext.getCmp('gridTotales');
						if(!gridTotales.isVisible()){
							gridTotales.show();
							gridTotales.el.dom.scrollIntoView();
						};
					}
				},'-',{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url:'13consulta6ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{ informacion: 'ArchivoCSV'}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},'-',{
					xtype: 'button',
					text: 'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id: 'btnGenerarPDF',
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta6ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{ informacion: 'ArchivoPDF'}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				}]
			}
	});

  var gridTotales = {
	xtype: 'grid',
	store: totalesData,
	id: 'gridTotales',
	style: ' margin:0 auto;',
	hidden: true,
	columns: [
			    {
					header: 'Moneda',
					dataIndex: 'CD_NOMBRE',
					align: 'center',
					width: 150
				 },
				 {
					header: 'Total Registros',
					dataIndex: 'TOTALREGISTROS',
					align: 'center',
					width: 150,
					renderer: Ext.util.Format.numberRenderer('0,000')
				 },
				 {
					header: 'Total Monto',
					dataIndex: 'TOTALMONTO',
					width: 200,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				 }
				],
		width: 940,
		height: 93,
		title: 'Totales',
		tools: [
					{
						id: 'close',
						handler: function(evento, toolEl, panel, tc){
						var gGridTotales = Ext.getCmp('gridTotales');
			         gGridTotales.hide();
						}
					}
		],
		frame: false
  };
  
  var gridDetalle = new Ext.grid.GridPanel({
		store: gridDetalleData,
		columns: [
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO1',	width: 100,	align: 'center', hidden: false},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO2',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO3',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO4',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO5',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO6',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO7',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO8',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO9',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO10',width: 100,	align: 'center', hidden: true}
		],
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 110,
		width: 790,
		frame: false,
		hidden: true
	});
  
  var fp = new Ext.form.FormPanel
  ({
		width: 500,
		labelWidth: 120,
		collapsible: true,
		title: 'Documentos Publicados con Detalle',
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle:'padding:6px',
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
			  {
			   var gGridTotales = Ext.getCmp('gridTotales');
			   gGridTotales.hide();
				fp.el.mask('Enviando...', 'x-mask-loading');
			   consultaData.load({ params: Ext.apply(fp.getForm().getValues(),{
				//modalidad_plazo:modo,
									operacion: 'Generar', //Generar datos para la consulta
									start: 0,
									limit: 15})
							         });
				form = {
						camp_dina1:null
				}
				var cd1 = Ext.getCmp('campDina1');
				if  (cd1){
				 form.camp_dina1 = cd1.getValue();
				}
			  }
			 },
			 {
			  //Bot�n Limpiar
			  xtype: 'button',
			  text: 'Limpiar',
			  name: 'btnLimpiar',
			  hidden: false,
			  //formBind: true,
			  iconCls: 'icoLimpiar',
			  handler: function(boton, evento) 
			  {
			   window.location = '13consulta6ext.jsp'
			  }
			 }
		  ]
  });
  
  var elementosDetalle = [
		{
			xtype:	'container',
			layout:	'column',
			anchor:	'100%',
				items:[
				{
					xtype: 'container',
					id:	'Izquierda',
					width:	420,
					layout: 'form',
					labelWidth: 50,
					bodyStyle: 'padding: 2px',
					items: [
						{
						  xtype: 'displayfield',	
						  id: 'disNombre',	
						  fieldLabel: '', 
						  anchor: '90%',
						  value: ''
						},
						{
						  xtype: 'displayfield',	
						  id: 'disRfc',		
						  fieldLabel: '',
						  anchor: '90%',	
						  value: ''
						},
						{
						  xtype: 'displayfield',	
						  id: 'disCalle',	
						  fieldLabel: '',
						  anchor: '90%',	
						  value: ''
						},
						{
						  xtype: 'displayfield',	
						  id: 'disColonia',	
						  fieldLabel: '',
						  anchor: '90%',	
						  value: ''
						},
						{
						  xtype: 'displayfield',	
						  id: 'disCp',		
						  fieldLabel: '',
						  anchor: '90%',	
						  value: ''
						},
						{
						  xtype: 'displayfield',	
						  id: 'disEstado',	
						  fieldLabel: '',
						  anchor: '90%',	
						  value: ''
						},
						{
						  xtype: 'displayfield',	
						  id: 'disDelomun',	
						  fieldLabel: '',
						  anchor: '90%',	
						  value: ''
						}
					]
				},
				{
					xtype: 'panel',	
					id:'derecha',	
					layout:'table',	
					layoutConfig:{ columns:2 },
					defaults: {
					           frame:false, 
								  border: true,
								  width:340, 
								  height: 30,
								  bodyStyle:'padding:5px'
								 },
					items: [
						     {	width:340,	border:false,	frame:true,	colspan:2,	html:'<div align="center">Datos del Documento</div>'	},
						     {	width:170,	html:'<div>No. Documento</div>'	},
						     {	width:170,	id: 'disDocto',	html: '&nbsp;'	},
						     {	width:170,	html:'<div>Fecha de Emisi�n</div>'	},
						     {	width:170,	id: 'disEmision',	html: '&nbsp;'	},
						     {	width:170,	html:'<div>Fecha de Vencimiento</div>'	},
						     {	width:170,	id: 'disVto',	html: '&nbsp;'	},
						     {	width:170,	html:'<div>Importe</div>'	},
						     {	width:170,	id: 'disImporte',	html: '&nbsp;'	},
						     {	width:170,	html:'<div>Moneda</div>'	},
						     {	width:170,	id: 'disMoneda',	html: '&nbsp;'	}
					       ]
			   }]
		}
	];	

  var fpDetalle = new Ext.FormPanel({	
   autoHeight:true,	
	width:800,	
	bodyStyle: 'padding:6px',	
	border:false,	labelWidth: 130,	
	items:elementosDetalle,	
	hidden:false	
  });


//-------------------------------- PRINCIPAL -----------------------------------
  
  var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 949,
	  items: 
	    [ 
	     fp,
	     NE.util.getEspaciador(20),
		  gridGeneral,
		  gridTotales
	    ]
  });
  
  Ext.Ajax.request({
		url: '13consulta6ext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
	});
  
	Ext.Ajax.request({
		url: '13consulta6ext.data.jsp',
		params: { informacion: "Parametrizacion" },
		callback: procesarSuccessFailureParametrizacion
	});
 
});