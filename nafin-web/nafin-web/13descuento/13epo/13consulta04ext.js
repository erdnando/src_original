Ext.onReady(function() {

	//Variables globales
	var mensaje  = Ext.getDom('mensaje').value;
	var iNoCliente  = Ext.getDom('iNoCliente').value;
	var vivienda  = Ext.getDom('vivienda').value;
	var botones  = Ext.getDom('botones').value;	
	var habilitadoNumeroSIAFF  = Ext.getDom('habilitadoNumeroSIAFF').value;	
	
	// Exportar Interfase con Detalle solo aplica para la EPO 2011 Gigante
	var procesarSuccessFailureEPO211 =  function(opts, success, response) {
		var btnGenerarEPO211 = Ext.getCmp('btnGenerarEPO211');
		btnGenerarEPO211.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarEPO211 = Ext.getCmp('btnBajarEPO211');
			btnBajarEPO211.show();
			btnBajarEPO211.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarEPO211.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarEPO211.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
			
	// Exportar Interfase RFC
	var procesarSuccessFailureRFC =  function(opts, success, response) {
		var btnGenerarRFC = Ext.getCmp('btnGenerarRFC');
		btnGenerarRFC.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarRFC = Ext.getCmp('btnBajarRFC');
			btnBajarRFC.show();
			btnBajarRFC.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarRFC.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarRFC.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}	
	
	//Generar Archivo con Cuenta Clabe
	var procesarSuccessFailureExpClabe =  function(opts, success, response) {
		var btnArchivoExpClabe = Ext.getCmp('btnArchivoExpClabe');
		btnArchivoExpClabe.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarExpClabe = Ext.getCmp('btnBajarExpClabe');
			btnBajarExpClabe.show();
			btnBajarExpClabe.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarExpClabe.el.dom.scrollIntoView();
			btnBajarExpClabe.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnArchivoExpClabe.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Descargar Archivo Exportar Interfase Hash
	var procesarSuccessFailureExpHash =  function(opts, success, response) {
		var btnArchivoExpHash = Ext.getCmp('btnArchivoExpHash');
		btnArchivoExpHash.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarExpHash = Ext.getCmp('btnBajarExpHash');
			btnBajarExpHash.show();
			btnBajarExpHash.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarExpHash.el.dom.scrollIntoView();
			btnBajarExpHash.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnArchivoExpHash.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//Descargar Archivo Exportar Interfase
	var procesarSuccessFailureExpInterfase =  function(opts, success, response) {
		var btnArchivoExporta = Ext.getCmp('btnArchivoExporta');
		btnArchivoExporta.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarExporta = Ext.getCmp('btnBajarExporta');
			btnBajarExporta.show();
			btnBajarExporta.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarExporta.el.dom.scrollIntoView();
			btnBajarExporta.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnArchivoExporta.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
		//Descargar Archivo Interface Detalle
	var procesarSuccessFailureArchivoHash =  function(opts, success, response) {
		var btnArchivoHash = Ext.getCmp('btnArchivoHash');
		btnArchivoHash.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarHash = Ext.getCmp('btnBajarHash');
			btnBajarHash.show();
			btnBajarHash.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarHash.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnArchivoHash.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Descargar Archivo Interface Detalle
	var procesarSuccessFailureInterfase =  function(opts, success, response) {
		var btnArchivoInter = Ext.getCmp('btnArchivoInter');
		btnArchivoInter.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarInter = Ext.getCmp('btnBajarInter');
			btnBajarInter.show();
			btnBajarInter.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarInter.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnArchivoInter.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	var procesarSuccessFailureDetalle =  function(opts, success, response) {
		var btnGenerarDetalle = Ext.getCmp('btnGenerarDetalle');
		btnGenerarDetalle.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarDetalle = Ext.getCmp('btnBajarDetalle');
			btnBajarDetalle.show();
			btnBajarDetalle.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarDetalle.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarDetalle.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	////DESCARGA ARCHIVO PDF TODO EL CONTENIDO 
	var procesarSuccessFailureArchivoClabe =  function(opts, success, response) {
		var btnArchivoClabe = Ext.getCmp('btnArchivoClabe');
		btnArchivoClabe.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarClabe = Ext.getCmp('btnBajarClabe');
			btnBajarClabe.show();
			btnBajarClabe.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarClabe.el.dom.scrollIntoView();
			btnBajarClabe.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnArchivoClabe.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
	//DESCARGA ARCHIVO PDF TODO EL CONTENIDO 
	var procesarSuccessFailureArchivoCSV =  function(opts, success, response) {
		var btnArchivoCSV = Ext.getCmp('btnArchivoCSV');
		btnArchivoCSV.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarCSV = Ext.getCmp('btnBajarCSV');
			btnBajarCSV.show();			
			btnBajarCSV.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarCSV.el.dom.scrollIntoView();
			btnBajarCSV.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnArchivoCSV.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	//Exportar Interfase	
	var procesarGenerarExportar = function() {
		var ventana = Ext.getCmp('GeneraExportar');		
		if (ventana) {
			ventana.show();
		}else {	
			new Ext.Window({			
				width: 430,				
				height: 180,
				resizable: true,
				closeAction: 'hide',
				id: 'GeneraExportar',
				autoScroll:true, 
				align: 'center',
				closable:false,
				items: [
					NE.util.getEspaciador(20),
					fpExportar,
					NE.util.getEspaciador(20)
				],
				title: 'Exportar Interfase',
				buttons: [
					'-',
					'->',
					{
						xtype: 'button', 	buttonAlign:'right', text: 'Cerrar',id: 'btnCerrar', 
						handler: function(){
							Ext.getCmp('btnBajarExporta').hide();
							Ext.getCmp('btnBajarExpHash').hide();
							Ext.getCmp('btnBajarExpClabe').hide();							
							Ext.getCmp('GeneraExportar').destroy();
						}
					}
				],
				listeners:{
					'render':	function(txtA){
					
						var  bOperaPubHas =   Ext.getCmp("bOperaPubHas1").getValue();
						var btnArchivoExpHash = Ext.getCmp('btnArchivoExpHash');		
					
						if(bOperaPubHas =='N'){
							btnArchivoExpHash.hide();
						}else if(bOperaPubHas =='S'){
							btnArchivoExpHash.enable();
						}			
					}
				}
			}).show();
		}
	}
	
	var fpExportar = {
		xtype: 'container',
		id: 'fpExportar',
		layout: 'table',		
		align: 'center',
		style: 'margin:0 auto;',			
		layoutConfig: {
			columns: 120
		},
		width:	400,
		heigth:	100,
		style: 'margin:0 auto;',
		items:[
			{
				xtype: 'button',
				scale: 'large',
				width: 80,
				height:10,
				text: 'Generar Archivo',
				id: 'btnArchivoExporta',
				anchor: '',				
				handler: function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV',
							tipo:'Interfase'
						})					
						,callback: procesarSuccessFailureExpInterfase
					});
				}
			},				
			{
				xtype: 'button',
				scale: 'large',
				text: 'Bajar Archivo',
				id: 'btnBajarExporta',
				hidden: true
			},				
			{
				xtype: 'button',
				scale: 'large',
				width: 80,
				height:10,
				text: 'Generar Archivo <br>Clave Hash',
				id: 'btnArchivoExpHash',
				anchor: '',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoTXT',
							tipo:'Interfase ExpHash'
						})					
						,callback: procesarSuccessFailureExpHash
					});
				}
			},				
			{
				xtype: 'button',
				scale: 'large',
				text: 'Bajar Archivo <br>Clave Hash',
				id: 'btnBajarExpHash',
				hidden: true
			},
			{
				xtype: 'button',
				scale: 'large',
				width: 80,
				height:10,
				text: 'Generar Archivo <br>con Cuenta Clabe',
				id: 'btnArchivoExpClabe',
				anchor: '',				
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV',
							tipo:'Interfase ExpClabe'
						})					
						,callback: procesarSuccessFailureExpClabe
					});
				}
			},				
			{
				xtype: 'button',
				scale: 'large',
				text: 'Bajar Archivo <br>con Cuenta Clabe',
				id: 'btnBajarExpClabe',
				hidden: true
			}
		],
		monitorValid: true
	};
	
	
	
	//Exportar Interfase Detalle de Notas de Cr�dito
	var procesarGenerarInterfase = function() {
		var ventana = Ext.getCmp('GeneraInterfase');
		if (ventana) {
			ventana.show();
		}else {	
		
			new Ext.Window({			
				width: 300,				
				height: 180,
				autoScroll:true, 
				resizable: true,
				closeAction: 'hide',
				id: 'GeneraInterfase',
				align: 'center',
				closable:false,
				items: [
					NE.util.getEspaciador(20),
					fpInterfase,
					NE.util.getEspaciador(20)
				],
				title: 'Exportar Interfase Detalle de Notas de Cr�dito',
				buttons: [
					'-',
					'->',
					{
						xtype: 'button', 	buttonAlign:'right', text: 'Cerrar',id: 'btnCerrar', 
						handler: function(){							
							Ext.getCmp('btnBajarInter').hide();
							Ext.getCmp('btnBajarHash').hide();														
							Ext.getCmp('GeneraInterfase').destroy();
						} 
					}
				],
				listeners:{
					'render':	function(txtA){
					
						var  bOperaPubHas =   Ext.getCmp("bOperaPubHas1").getValue();
						var btnArchivoHash = Ext.getCmp('btnArchivoHash');		
					
						if(bOperaPubHas =='N'){
							btnArchivoHash.hide();
						}else if(bOperaPubHas =='S'){
							btnArchivoHash.enable();
						}			
					}
				}
			}).show();
		}
	}
	
	var fpInterfase = {
		xtype: 'container',
		id: 'fpInterfase',
		layout: 'table',		
		align: 'center',
		style: 'margin:0 auto;',			
		layoutConfig: {
			columns: 100,
			align: 'center'
		},
		width:	300,
		heigth:	100,
		items:[
			{
				xtype: 'displayfield',
				value: '       ',
				width: 30
			},
			{
				xtype: 'button',
				scale: 'large',
				text: 'Generar Archivo',
				id: 'btnArchivoInter',
				handler: function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV',
							tipo:'Interfase Detalle'
						})					
						,callback: procesarSuccessFailureInterfase
					});
				}
			},				
			{
				xtype: 'button',
				scale: 'large',
				text: 'Bajar Archivo',
				id: 'btnBajarInter',
				hidden: true
			},				
			{
				xtype: 'button',
				scale: 'large',
				text: 'Generar Archivo <br>Clave Hash ',
				id: 'btnArchivoHash',
				anchor: '',				
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoTXT',
							tipo:'Interfase Hash'
						})					
						,callback: procesarSuccessFailureArchivoHash
					});
				}
			},				
			{
				xtype: 'button',
				scale: 'large',
				text: 'Bajar Archivo <br>Clave Hash',
				id: 'btnBajarHash',
				hidden: true
			}
		],
		monitorValid: true
	};
	
	//Generar Archivo
	var procesarGenerarArchivo = function() {
		var ventana = Ext.getCmp('GeneraArchivo');		
		if (ventana) {
			ventana.show();
		}else {	
			new Ext.Window({			
				width: 320,				
				height: 180,
				resizable: true,
				closeAction: 'hide',
				id: 'GeneraArchivo',
				autoScroll:true, 
				align: 'center',
				closable:false,
				items: [
					NE.util.getEspaciador(20),
					fpArchivos,
					NE.util.getEspaciador(20)
				],
				title: 'Generar Archivos',
				buttons: [
					'-',
					'->',
					{
						xtype: 'button', 	buttonAlign:'right', text: 'Cerrar',id: 'btnCerrar', 
						handler: function(){							
							Ext.getCmp('btnBajarCSV').hide();
							Ext.getCmp('btnBajarClabe').hide();
							Ext.getCmp('GeneraArchivo').destroy();
						} 
					}
				]
			}).show();
		}
	}
	
	var fpArchivos = {
		xtype: 'container',
		id: 'fpArchivos',
		layout: 'table',		
		align: 'center',
		style: 'margin:0 auto;',			
		layoutConfig: {
			columns: 120
		},
		width:	400,
		heigth:	100,
		items:[
		{
				xtype: 'displayfield',
				value: '   ',
				width: 20
			},
			{
				xtype: 'button',
				scale: 'large',
				width: 80,
				height:10,
				text: 'Generar Archivo',
				id: 'btnArchivoCSV',
				anchor: '',
				handler: function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV',
							tipo:'Archivo'
						})					
						,callback: procesarSuccessFailureArchivoCSV
					});
				}
			},				
			{
				xtype: 'button',
				scale: 'large',
				text: 'Bajar Archivo',
				id: 'btnBajarCSV',
				hidden: true
			},				
			{
				xtype: 'button',
				scale: 'large',
				width: 80,
				height:10,
				text: 'Generar Archivo <br>con Cuenta Clabe',
				id: 'btnArchivoClabe',
				anchor: '',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV',
							tipo:'ArchivoCLABE'
						})					
						,callback: procesarSuccessFailureArchivoClabe
					});
				}
			},				
			{
				xtype: 'button',
				scale: 'large',
				text: 'Bajar Archivo <br>con Cuenta Clabe',
				id: 'btnBajarClabe',
				hidden: true
			}
		],
		monitorValid: true
	};
/*
	//DESCARGA ARCHIVO PDF por Pagina
	var procesarSuccessFailureArchivoXPag =  function(opts, success, response) {
		var btnGenerarPDFxP = Ext.getCmp('btnGenerarPDFxP');
		btnGenerarPDFxP.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDFxP = Ext.getCmp('btnBajarPDFxP');
			btnBajarPDFxP.show();
			btnBajarPDFxP.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDFxP.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPDFxP.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	//DESCARGA ARCHIVO PDF TODO EL CONTENIDO 
/*	var procesarSuccessFailureArchivoPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( 
				function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
					forma.submit();
				}
			);
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}*/
	var procesarSuccessFailureArchivoPDF =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}


	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');		
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}
			
			var jsonData = store.reader.jsonData;	
			var hayCamposAdicionales = jsonData.hayCamposAdicionales;	
			
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			//var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			//var btnGenerarPDFxP = Ext.getCmp('btnGenerarPDFxP');
			//var btnBajarPDFxP = Ext.getCmp('btnBajarPDFxP');
			var btnGenerarAr = Ext.getCmp('btnGenerarAr');
			var btnGenerarDetalle = Ext.getCmp('btnGenerarDetalle');
			var btnBajarDetalle  = Ext.getCmp('btnBajarDetalle');
			var btnGenerarInterfase  = Ext.getCmp('btnGenerarInterfase');
			var fpBotones  = Ext.getCmp('fpBotones');			
			var btnGenerarExportar  = Ext.getCmp('btnGenerarExportar');
			var btnGenerarRFC = Ext.getCmp('btnGenerarRFC');
			var btnBajarRFC  = Ext.getCmp('btnBajarRFC');
			var btnGenerarEPO211 = Ext.getCmp('btnGenerarEPO211');
			var btnBajarEPO211 = Ext.getCmp('btnBajarEPO211');
			
			Ext.getCmp("bOperaPubHas1").setValue(jsonData.bOperaPubHas);
			
			//edito el titulo de la columna  de clasificacion EPO y la muestro las columnas de Campos Adicionales
			var cm = gridConsulta.getColumnModel();
			var el = gridConsulta.getGridEl();			
			if(store.getTotalCount() > 0) {		
					
							
				//para ocultar y mostrar botones
				if(botones =='S') {
				
					var numero_siaff = Ext.getCmp('numero_siaff1');
					var numero_siaff2 = Ext.getCmp('numero_siaff2');
				
					if(habilitadoNumeroSIAFF=='N'){
						numero_siaff.hide(); 
						numero_siaff2.hide(); 
					}else if (habilitadoNumeroSIAFF=='S'){
						numero_siaff.show(); 
						numero_siaff2.show();			
					}
					
					if(jsonData.operaNC=='S'){
						btnGenerarDetalle.enable();
						btnGenerarInterfase.enable();
					}else {
						btnGenerarDetalle.hide();
						btnGenerarInterfase.hide();
					}
					
					if(iNoCliente==16 ||  iNoCliente==vivienda ){ 	
						btnGenerarRFC.enable();   
					}else {
						btnGenerarRFC.hide();
					}
					if(iNoCliente==211) {  	
						btnGenerarEPO211.enable();   
					}else {
						btnGenerarEPO211.hide();
					}
				}	
	
				if(jsonData.noDocumento!='0') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('NUM_DOCTO'), false);						
				}
				if(jsonData.operaFVPyme=='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DF_FECHA_VEN_PYME'), false);						
				}
			
				if(jsonData.operaNC=='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DOCT_APLICADO'), false);
				}
			
				if(jsonData.bOperaFactorajeDistribuido=='S' || jsonData.bOperaFactorajeVencInfonavit=='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('POR_BENEFICIARIO'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('IMP_RECIBIR_BENE'), false);
				}
				if(jsonData.sestaHabilitadoNumeroSIAFF=='S') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DIG_IDENTIFICADOR'), false);
				}
				
				
				if(jsonData.banderaTablaDoctos=='1') {
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('DF_ENTREGA'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('TIPO_COMPRA'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CLASIFICADOR'), false);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('PLAZO_MAXIMO'), false);
				}		
			
				if(hayCamposAdicionales=='0'){
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='1'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='2'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='3'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), true);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='4'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);					
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), true);
				}
				if(hayCamposAdicionales=='5'){
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO1'),jsonData.nomCampo1);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO2'),jsonData.nomCampo2);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO3'),jsonData.nomCampo3);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO4'),jsonData.nomCampo4);
					gridConsulta.getColumnModel().setColumnHeader(cm.findColumnIndex('CAMPO5'),jsonData.nomCampo5);						
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO1'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO2'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO3'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO4'), false);	
					gridConsulta.getColumnModel().setHidden(cm.findColumnIndex('CAMPO5'), false);
				}
					
				btnGenerarPDF.enable();
				//btnBajarPDF.hide();
				//btnGenerarPDFxP.enable();
				//btnBajarPDFxP.hide();
				btnGenerarAr.enable();
				btnGenerarDetalle.enable();
				btnBajarDetalle.hide();	
				btnGenerarInterfase.enable();				
				fpBotones.show();
				btnGenerarExportar.enable();
				btnGenerarRFC.enable();
				btnBajarRFC.hide();	
				btnGenerarEPO211.enable();
				btnBajarEPO211.hide();
						
				resumenTotalesData.load();
				gridTotales.show();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				//btnBajarPDF.hide();
				//btnGenerarPDFxP.disable();
				//btnBajarPDFxP.hide();
				btnGenerarAr.disable();
				btnGenerarDetalle.disable();
				btnBajarDetalle.hide();
				btnGenerarInterfase.disable();
				btnGenerarExportar.disable();	
				btnGenerarRFC.disable();
				btnBajarRFC.hide();	
				btnGenerarEPO211.disable();
				btnBajarEPO211.hide();	
				gridTotales.hide();
				fpBotones.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var resumenTotalesData = new Ext.data.JsonStore({			
		root : 'registros',
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'			
		},
		fields: [
			{name: 'CD_NOMBRE', mapping: 'CD_NOMBRE'},
			{name: 'FN_MONTO', type: 'float', mapping: 'FN_MONTO'},
			{name: 'FN_MONTO_DSCTO', type: 'float', mapping: 'FN_MONTO_DSCTO'},
			{name: 'RESULT_SIZE', type: 'float', mapping: 'RESULT_SIZE'}						
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}		
	});

	
	var gridTotales = new Ext.grid.EditorGridPanel({
		id: 'gridTotales',
		title: '',
		hidden: true,
		margins: '20 0 0 0',
		align: 'center',
		store: resumenTotalesData,
		clicksToEdit: 1,		
			columns: [	
				{
					header: 'Moneda',
					dataIndex: 'CD_NOMBRE',
					width: 200,
					align: 'left'			
				},
				{
					header: 'N�mero de documentos',
					dataIndex: 'RESULT_SIZE',
					width: 150,
					align: 'center'				
				},
				{
					header: 'Total Monto',
					dataIndex: 'FN_MONTO',
					width: 150,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')		
				},
					{
					header: 'Total Monto con descuento',
					dataIndex: 'FN_MONTO_DSCTO',
					width: 250,
					align: 'right',
					renderer: Ext.util.Format.numberRenderer('$0,0.00')		
				}			
			],
			height: 100,		
			width: 943,			
			frame: false
		});
		
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'INTERMEDIARIO'},
			{name: 'FECHA_NOTIFICACION'},
			{name: 'ACUSE_NOTIFICACION'},
			{name: 'NUM_PROVEEDOR'},
			{name: 'NOMBRE_PROVEEDOR'},
			{name: 'NUM_DOCTO'},
			{name: 'FECHA_EMISION'},
			{name: 'FECHA_VENCIMIENTO'},
			{name: 'DF_FECHA_VEN_PYME'},		
			{name: 'MONEDA'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'MONTO'},
			{name: 'POR_DESCUENTO'},
			{name: 'MONTO_DESCONTAR'},
			{name: 'CLAVE_BANCO'},
			{name: 'BANCO'},
			{name: 'NUM_CUENTA'},
			{name: 'REFERENCIA'},
			{name: 'DOCT_APLICADO'},
			{name: 'CAMPO1'},
			{name: 'CAMPO2'},
			{name: 'CAMPO3'},
			{name: 'CAMPO4'},
			{name: 'CAMPO5'},
			{name: 'BENEFICIARIO'},
			{name: 'POR_BENEFICIARIO'},
			{name: 'IMP_RECIBIR_BENE'},
			{name: 'DIG_IDENTIFICADOR'},
			{name: 'DF_ENTREGA'},
			{name: 'TIPO_COMPRA'},
			{name: 'CLASIFICADOR'},
			{name: 'PLAZO_MAXIMO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
		load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});	
	

	var gridConsulta = new Ext.grid.EditorGridPanel({
		id: 'gridConsulta',
		title: 'Avisos de Notificaci�n',
		hidden: true,
		margins: '20 0 0 0',
		align: 'center',
		store: consultaData,
		clicksToEdit: 1,		
		columns: [			
			{
				header: 'Intermediario Financiero', 
				tooltip: 'Intermediario Financiero',
				dataIndex: 'INTERMEDIARIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Fecha de Notificaci�n', 
				tooltip: 'Fecha de Notificaci�n',
				dataIndex: 'FECHA_NOTIFICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Acuse de Notificaci�n', 
				tooltip: 'Acuse de Notificaci�n',
				dataIndex: 'ACUSE_NOTIFICACION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'N�mero de Proveedor', 
				tooltip: 'N�mero de Proveedor',
				dataIndex: 'NUM_PROVEEDOR',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Nombre del proveedor', 
				tooltip: 'Nombre del proveedor ',
				dataIndex: 'NOMBRE_PROVEEDOR',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de Documento', 
				tooltip: 'N�mero de Documento',
				dataIndex: 'NUM_DOCTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Fecha de Emisi�n', 
				tooltip: 'Fecha de Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Fecha de Vencimiento', 
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'FECHA_VENCIMIENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},			
			{
				header: 'Fecha de Vencimiento del Proveedor', 
				tooltip: 'Fecha de Vencimiento del Proveedor',
				dataIndex: 'DF_FECHA_VEN_PYME',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Moneda', 
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Tipo Factoraje', 
				tooltip: 'Tipo Factoraje',
				dataIndex: 'TIPO_FACTORAJE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'center'
			},
			{
				header: 'Monto', 
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Porcentaje de Descuento', 
				tooltip: 'Porcentaje de Descuento',
				dataIndex: 'POR_DESCUENTO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			
			{
				header: 'Monto a Descontar', 
				tooltip: 'Monto a Descontar',
				dataIndex: 'MONTO_DESCONTAR',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Clave del Banco', 
				tooltip: 'Clave del Banco',
				dataIndex: 'CLAVE_BANCO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'center'
			},
			{
				header: 'Banco', 
				tooltip: 'Banco',
				dataIndex: 'BANCO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'N�mero de Cuenta', 
				tooltip: 'N�mero de Cuenta',
				dataIndex: 'NUM_CUENTA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Referencia', 
				tooltip: 'Referencia',
				dataIndex: 'REFERENCIA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'Documento Aplicado', 
				tooltip: 'Documento Aplicado',
				dataIndex: 'DOCT_APLICADO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'center'
			},
			{
				header: 'CAMPO1', 
				tooltip: 'CAMPO1',
				dataIndex: 'CAMPO1',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'CAMPO2', 
				tooltip: 'CAMPO2',
				dataIndex: 'CAMPO2',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'CAMPO3', 
				tooltip: 'CAMPO3',
				dataIndex: 'CAMPO3',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'CAMPO4', 
				tooltip: 'CAMPO4',
				dataIndex: 'CAMPO4',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},
			{
				header: 'CAMPO5', 
				tooltip: 'CAMPO5',
				dataIndex: 'CAMPO5',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: false,
				align: 'left'
			},			
			{
				header: 'Beneficiario', 
				tooltip: 'Beneficiario',
				dataIndex: 'BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'
			},
			{
				header: '%Beneficiario', 
				tooltip: '%Beneficiario',
				dataIndex: 'POR_BENEFICIARIO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			},
			{
				header: 'Importe a recibir beneficiario', 
				tooltip: 'Importe a recibir beneficiario',
				dataIndex: 'IMP_RECIBIR_BENE',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'D�gito Identificador', 
				tooltip: 'D�gito Identificador',
				dataIndex: 'DIG_IDENTIFICADOR',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'center'				
			},
			{
				header: 'Fecha de Recepci�n de Bienes y Servicios', 
				tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
				dataIndex: 'DF_ENTREGA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'center'				
			},
			{
				header: 'Tipo de Compra (procedimiento)', 
				tooltip: 'Tipo de Compra (procedimiento)',
				dataIndex: 'TIPO_COMPRA',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'center'				
			},
			{
				header: 'Clasificador por Objeto del Gasto', 
				tooltip: 'Clasificador por Objeto del Gasto',
				dataIndex: 'CLASIFICADOR',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'left'				
			},
			{
				header: 'Plazo M�ximo', 
				tooltip: 'Plazo M�ximo',
				dataIndex: 'PLAZO_MAXIMO',
				sortable: true,
				resizable: true	,
				width: 130,
				hidden: true,
				align: 'center'				
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,	
		frame: false,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros."
		}
	});
	
	mensajes2 = '<table width="550">'+
							'<tr><td class="formas" colspan="4">'+mensaje+'</td></tr>'+ 
							'</table>';
			
	var catalogoIFData = new Ext.data.JsonStore({
		id: 'catalogoIFDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIF'
		},
		totalProperty : 'total',
		autoLoad: false,		
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}		
	});
	
	var elementosForma = [				
		{ 	xtype: 'displayfield', 	hidden: true, id: 'limitarRangoFechas', 	value: '' },
                { 
			xtype:   'label',  
			html:		mensajes2, 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center'
			} 
		},
		{
			xtype: 'textfield',
			name: 'bOperaPubHas',
			id: 'bOperaPubHas1',
			fieldLabel: 'bOperaPubHas',					
			allowBlank: true,
			hidden: true,
			startDay: 0,
			maxLength: 15,	
			width: 100,
			msgTarget: 'side',						
			margins: '0 20 0 0'			
		},		
		{
			xtype: 'compositefield',
			fieldLabel: 'Intermediario Finaciero',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'combo',
					name: 'ic_if',
					id: 'ic_if1',
					fieldLabel: 'Intermediario Finaciero',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'ic_if',
					emptyText: 'Seleccione...',
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					width: 345,
					minChars : 1,
					allowBlank: true,
					store : catalogoIFData,
					//tpl : NE.util.templateMensajeCargaCombo
					tpl: '<tpl for=".">' +
							'<tpl if="!Ext.isEmpty(loadMsg)">'+
							'<div class="loading-indicator">{loadMsg}</div>'+
							'</tpl>'+
							'<tpl if="Ext.isEmpty(loadMsg)">'+
							'<div class="x-combo-list-item">' +
							'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
							'</div></tpl></tpl>'
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Notificaci�n',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_notMin',
					id: 'df_fecha_notMin',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_notMax',
					margins: '0 20 0 0'  
				},					
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_notMax',
					id: 'df_fecha_notMax',
					allowBlank: true,
					startDay: 1,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_notMin',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_vencMin',
					id: 'df_fecha_vencMin',
					allowBlank: true,
					startDay: 0,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_fecha_vencMax',
					margins: '0 20 0 0'  
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_vencMax',
					id: 'df_fecha_vencMax',
					allowBlank: true,
					startDay: 1,
					width: 150,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'df_fecha_vencMin',
					margins: '0 20 0 0'  
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Acuse de Notificaci�n',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'textfield',
					name: 'cc_acuse3',
					id: 'cc_acuse31',
					fieldLabel: 'Acuse de Notificaci�n',
					allowBlank: true,
					maxLength: 14,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'D�gito Identificador',
			id: 'numero_siaff2',
			combineErrors: false,
			hidden: true,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'textfield',
					name: 'numero_siaff',
					id: 'numero_siaff1',
					fieldLabel: 'D�gito Identificador',
					allowBlank: true,
					hidden: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'No. de Proveedor',
			combineErrors: false,
			msgTarget: 'side',
			width: 500,
			items: [
				{
					xtype: 'textfield',
					name: 'numeroProveedor',
					id: 'numeroProveedor1',
					fieldLabel: 'No. de Proveedor',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		}
	];
	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Criterios de B�squeda',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true,				
		buttons: [
			{
				text: 'Consultar',
				id: 'btnBuscar',
				iconCls: 'icoAceptar',
				formBind: true,
				handler: function(boton, evento) {
					var df_fecha_notMin = Ext.getCmp('df_fecha_notMin');
					var df_fecha_notMax = Ext.getCmp('df_fecha_notMax');					
					var limitarRangoFechas = Ext.getCmp('limitarRangoFechas').getValue();
					var df_fecha_vencMin = Ext.getCmp('df_fecha_vencMin');
					var df_fecha_vencMax = Ext.getCmp('df_fecha_vencMax');	

					
					var ventana1 = Ext.getCmp('GeneraArchivo');
					if (ventana1) {	
						ventana1.destroy();	
					}			
					var ventana2 = Ext.getCmp('GeneraInterfase');
					if (ventana2) {	
						ventana2.destroy();	
					}						
					var ventana3 = Ext.getCmp('GeneraExportar');
					if (ventana3) {	
						ventana3.destroy();	
					}	
					
					/*var df_fecha_notMin = Ext.getCmp("df_fecha_notMin");
					var df_fecha_notMax = Ext.getCmp("df_fecha_notMax");					*/
					if (!Ext.isEmpty(df_fecha_notMin.getValue())   &&  Ext.isEmpty(df_fecha_notMax.getValue()) ) {
						df_fecha_notMin.markInvalid('Debe capturar ambas fechas de  Notificaci�n  o dejarlas en blanco');	
						df_fecha_notMax.markInvalid('Debe capturar ambas fechas de Notificaci�n o dejarlas en blanco');	
						return;
					}	
					if (Ext.isEmpty(df_fecha_notMin.getValue())   &&  !Ext.isEmpty(df_fecha_notMax.getValue()) ) {
						df_fecha_notMin.markInvalid('Debe capturar ambas fechas de  Notificaci�n  o dejarlas en blanco');	
						df_fecha_notMax.markInvalid('Debe capturar ambas fechas de Notificaci�n o dejarlas en blanco');	
						return;
					}	
					
					/*var df_fecha_vencMin = Ext.getCmp("df_fecha_vencMin");
					var df_fecha_vencMax = Ext.getCmp("df_fecha_vencMax");					*/
					if (!Ext.isEmpty(df_fecha_vencMin.getValue())   &&  Ext.isEmpty(df_fecha_vencMax.getValue()) ) {
						df_fecha_vencMin.markInvalid('Debe capturar ambas fechas de Vencimiento o dejarlas en blanco');	
						df_fecha_vencMax.markInvalid('Debe capturar ambas fechas de Vencimiento o dejarlas en blanco');	
						return;
					}	
					if (Ext.isEmpty(df_fecha_vencMin.getValue())   &&  !Ext.isEmpty(df_fecha_vencMax.getValue()) ) {
						df_fecha_vencMin.markInvalid('Debe capturar ambas fechas de Vencimiento o dejarlas en blanco');	
						df_fecha_vencMax.markInvalid('Debe capturar ambas fechas de Vencimiento o dejarlas en blanco');	
						return;
					}	

					var Diferencia = Math.abs(getDiferenciaEnDias(df_fecha_notMax.getValue(),df_fecha_notMin.getValue() ));
					if(limitarRangoFechas =='S') {			
						if ( Diferencia >7 )		{
							df_fecha_notMax.markInvalid('El rango de Fecha de notificacion no debe sobrepasar 7 dias');
							return;
						}
					}

					Diferencia = Math.abs(getDiferenciaEnDias(df_fecha_vencMax.getValue(),df_fecha_vencMin.getValue() ));
					if(limitarRangoFechas =='S') {			
						if ( Diferencia >7 )		{
							df_fecha_vencMax.markInvalid('El rango de fechas de vencimiento no debe sobrepasar 7 dias');							
							return;
						}
					}

		
					fp.el.mask('Enviando...', 'x-mask-loading');			
					
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start:0,
							limit:15
						})
					});						
				}
			},			
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta04ext.jsp';
				}
			}
		]
	});
	
	//Contenedor de Botones de Impresion
	var fpBotones = new Ext.Toolbar({
		id:'fpBotones',	style: 'margin:0 auto;',	autoScroll:true,	hidden:true,	height:60,
		items: [
			'->','-',
			{
				xtype: 'button',
				scale: 'large',
				text: 'Generar Archivo',
				tooltip:	'Generar Archivo',
				id: 'btnGenerarAr',
				handler: procesarGenerarArchivo
			},'-',{
				xtype: 'button',
				scale: 'large',
				text: 'Generar Archivo <br>Detalle Nota de Cr�dito',
				tooltip:	'Generar Archivo <br>Detalle Nota de Cr�dito',
				id: 'btnGenerarDetalle',			
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					var barraPaginacion = Ext.getCmp("barraPaginacion");
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV',
							tipo:'Archivo Detalle'								
						})					
						,callback: procesarSuccessFailureDetalle
					});
				}
			},{
				xtype: 'button',
				scale: 'large',
				text:	'Bajar Archivo <br>Detalle Nota de Cr�dito',
				tooltip:	'Bajar Archivo <br>Detalle Nota de Cr�dito',
				id: 'btnBajarDetalle',
				hidden: true
			},'-',{
				xtype: 'button',
				scale: 'large',
				text: 'Exportar Interfase <br>Detalle de Notas de Cr�dito',
				tooltip:	'Exportar Interfase <br>Detalle de Notas de Cr�dito',
				id: 'btnGenerarInterfase',
				handler: procesarGenerarInterfase
			},'-',{
				xtype: 'button',
				scale: 'large',
				text: 'Exportar Interfase',
				tooltip:	'Exportar Interfase',
				id: 'btnGenerarExportar',
				handler: procesarGenerarExportar
			},'-',{
				xtype: 'button',
				scale: 'large',
				text: 'Exportar Interfase <br>con Detalle',
				tooltip:	'Exportar Interfase <br>con Detalle',
				id: 'btnGenerarEPO211',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					var barraPaginacion = Ext.getCmp("barraPaginacion");
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoEPO211',
							tipo:'Interfase-EPO211'								
						})					
						,callback: procesarSuccessFailureEPO211
					});
				}
			},{
				xtype: 'button',
				scale: 'large',
				text: 'Bajar Interfase <br>con Detalle',
				tooltip:	'Bajar Interfase <br>con Detalle',
				id: 'btnBajarEPO211',
				hidden: true
			},'-',{
				xtype: 'button',
				scale: 'large',
				text: 'Exportar Interfase RFC',
				tooltip:	'Exportar Interfase RFC',
				id: 'btnGenerarRFC',				
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					var barraPaginacion = Ext.getCmp("barraPaginacion");
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoCSV',
							tipo:'Interfase-RFC'
						})					
						,callback: procesarSuccessFailureRFC
					});
				}
			},{
				xtype: 'button',
				scale: 'large',
				text: 'Bajar Interfase RFC',
				tooltip:	'Bajar Interfase RFC',
				id: 'btnBajarRFC',
				hidden: true
			},'-',/*{
				xtype: 'button',
				scale: 'large',
				text: 'Imprimir x <br> pagina PDF',
				tooltip:	'Imprimir x pagina PDF',
				id: 'btnGenerarPDFxP',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					var barraPaginacion = Ext.getCmp("barraPaginacion");
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoPDFxPagina',
							start: barraPaginacion.cursor,
							limit: barraPaginacion.pageSize
						})
						,callback: procesarSuccessFailureArchivoXPag
					});
				}
			},{
				xtype: 'button',
				scale: 'large',
				text: 'Bajar x <br>p�gina PDF',
				tooltip:	'Bajar x p�gina PDF',
				id: 'btnBajarPDFxP',
				hidden: true
			},'-',*/{
				xtype: 'button',
				//scale: 'large',
				//text: 'Imprimir <br>todo PDF',
				//tooltip:	'Imprimir <br>todo PDF ',
				text:    'Generar Todo',
				tooltip: 'Imprime los registros en formato PDF.',
				iconCls: 'icoPdf',
				id: 'btnGenerarPDF',
				handler: function(boton, evento) {
					boton.disable();
					boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta04ext.data.jsp',
						params: Ext.apply(fp.getForm().getValues(),{
							informacion:'ArchivoPDF'
						})					
						,callback: procesarSuccessFailureArchivoPDF
					});
				}
			}/*,{
				xtype: 'button',
				scale: 'large',
				text: 'Bajar Imprimir <br>todo PDF',
				tooltip:	'Bajar Imprimir <br>todo PDF',
				id: 'btnBajarPDF',
				hidden: true
			},'-'*/
		]
	});
	
//-------------------------------- PRINCIPAL -----------------------------------
  var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 949,
	  height: 'auto',
	  items: 
	    [		  
		  NE.util.getEspaciador(20),
			fp,	
			NE.util.getEspaciador(20),
			gridConsulta,
			gridTotales,
			NE.util.getEspaciador(20),
			fpBotones,
			NE.util.getEspaciador(20)
	    ]
  });
	

	catalogoIFData.load();
	
	//Peticion para obtener valores iniciales y la parametrizaci�n
	fp.el.mask('Inicializando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '13consulta04ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});

	
	//se ejecuta al inicio de la pantalla
	consultaData.load({
		params: Ext.apply(fp.getForm().getValues(),{
			operacion: 'Generar',
			start:0,
			limit:15
		})
	});

	function procesaValoresIniciales(opts, success, response) {
		fp.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonData = Ext.util.JSON.decode(response.responseText);
			if (jsonData != null){			
				//Ext.getCmp("df_fecha_notMin").setValue(jsonData.fechaHoy);
				//Ext.getCmp("df_fecha_notMax").setValue(jsonData.fechaHoy);	
				Ext.getCmp("limitarRangoFechas").setValue(jsonData.limitarRangoFechas);					
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var getDiferenciaEnDias = function(fechaIni, fechaFin) { 
		fechaIni =	Ext.util.Format.date(fechaIni,'d/m/Y');
		fechaFin =	Ext.util.Format.date(fechaFin,'d/m/Y');
		var string1 			= "";
		var temp 				= "";
		var diferenciaEnDias	= "";
		string1 = fechaIni;
		string = "" + string1;
		splitstring = string.split("/");
		var fechaInicial= new Date();
		fechaInicial.setDate(splitstring[0]);
		fechaInicial.setMonth(splitstring[1]-1);
		fechaInicial.setYear(splitstring[2]);
		string1 = fechaFin;
		string = "" + string1;
		splitstring = string.split("/");
		var fechaFinal= new Date();
		fechaFinal.setDate(splitstring[0]);
		fechaFinal.setMonth(splitstring[1]-1);
		fechaFinal.setYear(splitstring[2]);
		//Calcular el numero de milisegundos de un dia
		var milisegundosPorDia=1000*60*60*24;
		//Calculate difference btw the two dates, and convert to days
		diferenciaEnDias = Math.ceil((fechaFinal.getTime()-fechaInicial.getTime())/(milisegundosPorDia));
		return diferenciaEnDias;
	}


					
});