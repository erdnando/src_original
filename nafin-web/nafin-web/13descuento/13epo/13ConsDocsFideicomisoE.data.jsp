<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		com.netro.exception.*, 
		com.netro.descuento.*,
		com.netro.afiliacion.*,  
		netropology.utilerias.*,
		net.sf.json.JSONArray,  
		net.sf.json.JSONObject,  
		netropology.utilerias.ElementoCatalogo,  
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/15cadenas/015secsession_extjs.jspf" %>

<%
	String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar	= "";
	JSONObject resultado = new JSONObject();
	
	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);

 if (informacion.equals("catalogoEstatus")  ) {
	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_estatus_docto");
	catalogo.setCampoDescripcion("cd_descripcion");
	catalogo.setTabla("comcat_estatus_docto");		
	catalogo.setOrden("cd_descripcion");
	catalogo.setValoresCondicionIn("4,11,12", Integer.class);
	infoRegresar = catalogo.getJSONElementos();

}
 else if (informacion.equals("catalogoMoneda")  ) {

	CatalogoSimple catalogo = new CatalogoSimple();
	catalogo.setCampoClave("ic_moneda");
	catalogo.setCampoDescripcion("cd_nombre");
	catalogo.setTabla("comcat_moneda");		
	catalogo.setOrden("ic_moneda");
	catalogo.setValoresCondicionIn("1,54", Integer.class);
	infoRegresar = catalogo.getJSONElementos();	
	
}
else if (informacion.equals("catalogoIF")  ) {
	String ic_epo = iNoCliente;
	List lista = new ArrayList();
	JSONArray registros = new JSONArray();
	CatalogoIF catalogo = new CatalogoIF();
	catalogo.setCampoClave("ic_if");
	catalogo.setCampoDescripcion("cg_razon_social");	
	catalogo.setFideicomiso("N");
	catalogo.setClaveEpo(ic_epo);
	lista = catalogo.getListaElementosIF();
	
	if(lista.size() >0 ){	
		for (int i= 0; i < lista.size(); i++) {			
			String registro =  lista.get(i).toString();
			int posC=registro.indexOf('-');		
			int posD=registro.length();		
			String clave=registro.substring(0, posC);		
			String descripcion  	=   registro.substring(posC+1,posD );							
			HashMap datos = new HashMap();		
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);	
		}	
	}
	
	String consulta =  "{\"success\": true, \"total\": \"" + lista.size() + "\", \"registros\": " + registros.toString()+"}";
	resultado = JSONObject.fromObject(consulta);	
	infoRegresar = resultado.toString();		
	
}else if (	informacion.equals("CatalogoBuscaAvanzada") ) {
	
	String ic_epo		= iNoCliente;	
	String num_pyme	= (request.getParameter("num_pyme")==null)?"":request.getParameter("num_pyme");
	String rfc_pyme	= (request.getParameter("rfc_prov")==null)?"":request.getParameter("rfc_prov");
	String nombre_pyme= (request.getParameter("nombre_pyme")==null)?"":request.getParameter("nombre_pyme");
	String ic_pymes		= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");

	Registros regis = afiliacion.getProveedores("",ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pymes,"","");

	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	while (regis.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		elementoCatalogo.setClave(regis.getString("IC_PYME"));
		//elementoCatalogo.setDescripcion(regis.getString("IC_NAFIN_ELECTRONICO")+" "+regis.getString("CG_RAZON_SOCIAL"));
		elementoCatalogo.setDescripcion(regis.getString("CG_PYME_EPO_INTERNO")+" "+regis.getString("CG_RAZON_SOCIAL"));
		coleccionElementos.add(elementoCatalogo);
	}	
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	resultado.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		resultado.put("total", "0");
		resultado.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		resultado.put("total",  Integer.toString(jsonArr.size()));
		resultado.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		resultado.put("total", "excede" );
		resultado.put("registros", "" );
	}
	
	infoRegresar = resultado.toString();
	
}else if( informacion.equals("CargaNom_Prov") ){
	String cad = "";
	String ic_pyme= "";	
	Registros regis	= new Registros();
	String num_pyme	= (request.getParameter("num_Nae")==null)?"":request.getParameter("num_Nae");

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
	HashMap	datosPYME	= BeanParamDscto.getParametrosPYME(num_pyme,iNoCliente);
	
	if(datosPYME != null){
		num_pyme = (String) datosPYME.get("IC_PYME");
		//jsonObj.put("cg_pyme_epo_interno", (String) datosPYME.get("NUMERO_PROVEEDOR"));
		cad = (String) datosPYME.get("DESCRIPCION");
	}
	
	if(!cad.equals("")){
		resultado.put("success", new Boolean(true));
		resultado.put("nom_Nae", cad);
		resultado.put("num_pyme", num_pyme);
	}else
		resultado.put("success", new Boolean(false));
		
	infoRegresar = resultado.toString();

	
}else if (informacion.equals("consultaDatos") ||  informacion.equals("ArchivoCSV") ||  informacion.equals("ArchivoXPDF") || informacion.equals("ConsultarTotales") ) {
	
	int start = 0;
	int limit = 0;

	String  num_epo     = iNoCliente;
	String operacion    = (request.getParameter("operacion")      == null)?"":request.getParameter("operacion");
	String tipo         = (request.getParameter("tipo")           == null)?"":request.getParameter("tipo");
	String num_pyme     = (request.getParameter("ic_pyme")        == null) ? "" : request.getParameter("ic_pyme");
	String nom_if       = (request.getParameter("cmb_nom_if")     == null) ? "" : request.getParameter("cmb_nom_if");
	String num_docto    = (request.getParameter("num_doc")        == null) ? "" : request.getParameter("num_doc");
	String fec_noti_ini = (request.getParameter("fecha_noti_ini") == null) ? "" : request.getParameter("fecha_noti_ini");
	String fec_noti_fin = (request.getParameter("fecha_noti_fin") == null) ? "" : request.getParameter("fecha_noti_fin");
	String cve_moneda   = (request.getParameter("cmb_moneda")     == null) ? "" : request.getParameter("cmb_moneda");
	String monto_ini    = (request.getParameter("monto_ini")      == null) ? "" : request.getParameter("monto_ini");
	String monto_fin    = (request.getParameter("monto_fin")      == null) ? "" : request.getParameter("monto_fin");
	String cve_estatus  = (request.getParameter("estatus")        == null) ? "" : request.getParameter("estatus");
	String fec_ven_ini  = (request.getParameter("fecha_ven_ini")  == null) ? "" : request.getParameter("fecha_ven_ini");
	String fec_ven_fin  = (request.getParameter("fecha_ven_fin")  == null) ? "" : request.getParameter("fecha_ven_fin");
	String fec_ope_ini  = (request.getParameter("fecha_ope_ini")  == null) ? "" : request.getParameter("fecha_ope_ini");
	String fec_ope_fin  = (request.getParameter("fecha_ope_fin")  == null) ? "" : request.getParameter("fecha_ope_fin");

	ConsDocsFide paginador = new ConsDocsFide();
	paginador.setNum_epo(num_epo);
	paginador.setNum_pyme(num_pyme);
	paginador.setNom_if(nom_if);
	paginador.setNum_docto(num_docto);
	paginador.setFec_noti_ini(fec_noti_ini);
	paginador.setFec_noti_fin(fec_noti_fin);
	paginador.setCve_moneda(cve_moneda);
	paginador.setMonto_ini(monto_ini);
	paginador.setMonto_fin(monto_fin);
	paginador.setCve_estatus(cve_estatus);
	paginador.setFec_ven_ini(fec_ven_ini);
	paginador.setFec_ven_fin(fec_ven_fin);
	paginador.setFec_ope_ini(fec_ope_ini);
	paginador.setFec_ope_fin(fec_ope_fin);
	paginador.setConsulta(tipo);
	paginador.setPerfil("EPO");

	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	if(informacion.equals("consultaDatos")){
		try{
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));
		} catch(Exception e){
			throw new AppException("Error en los parametros recibidos", e);
		}
		try{
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
			//System.out.println("infoRegresar "+infoRegresar);
		} catch(Exception e){
			throw new AppException("Error en la paginacion", e);
		}

	} else if(informacion.equals("ConsultarTotales")){//Datos para el Resumen de Totales
		//Debe ser llamado despues de Consulta
		queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion		
	} else if (informacion.equals("ArchivoXPDF")) {
		JSONObject jsonObj = new JSONObject();
		try {
			//String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");	
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString();
	}else if (informacion.equals("ArchivoCSV")) {
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			infoRegresar = jsonObj.toString();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		}
	}
	
}

%>
<%=infoRegresar%>
