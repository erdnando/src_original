<!DOCTYPE html>
<%@ page import="
		javax.naming.*,	
		com.netro.exception.*,	  
		com.netro.descuento.*,		
		java.util.*, 
    netropology.utilerias.*"
	 contentType="text/html;charset=windows-1252"
    errorPage="/00utils/error.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%
String mensaje = "", habilitadoNumeroSIAFF ="N"; 
String vivienda = (application.getInitParameter("EpoEntregaContinuaDeVivienda"));

	if (application.getInitParameter("EpoEntregaAnticipadaDeVivienda").equals(iNoCliente)) {

	mensaje = "	Para efectos del art�culo 2038 del C�digo Civil Federal me doy por notificado "+
						" de la cesi�n de derechos de las operaciones descritas en la presente pantalla. "+
						" \n  Y por lo tanto me obligo incondicionalmente a hacer el pago al beneficiario "+
						"	de los derechos de cobro de los documentos que aparecen en esta pantalla "+
						" en las fechas y por los montos establecidos.";

	} else {
		mensaje = mensaje_param.getMensaje("13consulta4.ParaEfectos", sesIdiomaUsuario) +" "+mensaje_param.getMensaje("13consulta4.MeObligo", sesIdiomaUsuario)+" <br/><br/>"+
        mensaje_param.getMensaje("13consulta4.PorOtra_Parte", sesIdiomaUsuario);
        mensaje = "<p style='text-align: justify;'>"+ mensaje + "</p>";

	}
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	boolean estaHabilitadoNumeroSIAFF 	= BeanParamDscto.estaHabilitadoNumeroSIAFF(iNoCliente); // VERIFICAR SI ESTA HABILITADO EL NUMERO SIAFF 
	if(estaHabilitadoNumeroSIAFF==true)  habilitadoNumeroSIAFF  = "S";	

%>

<html>
<head>
<title>Nafinet</title>
<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="13consulta04ext.js?<%=session.getId()%>"></script>
</head>
<body>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<%@ include file="/01principal/01epo/cabeza.jspf"%>
	<div id="_menuApp"></div>
	<div id="Contcentral">
	<%@ include file="/01principal/01epo/menuLateralFlotante.jspf"%>
	<div id="areaContenido"></div>						
	</div>
	</div>
	<%@ include file="/01principal/01epo/pie.jspf"%><form id='formAux' name="formAux" target='_new'></form>
			
<form id='formParametros' name="formParametros">	
	<input type="hidden" id="mensaje" name="mensaje" value="<%=mensaje%>"/>		
	<input type="hidden" id="iNoCliente" name="iNoCliente" value="<%=iNoCliente%>"/>
	<input type="hidden" id="vivienda" name="vivienda" value="<%=vivienda%>"/>
	<input type="hidden" id="botones" name="botones" value="S"/>
	<input type="hidden" id="habilitadoNumeroSIAFF" name="habilitadoNumeroSIAFF" value="<%=habilitadoNumeroSIAFF%>"/>
	
	
	
</form>
	
</body>
</html>