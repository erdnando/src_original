<%@ page contentType="application/json;charset=UTF-8" import="   
   java.util.*, java.sql.*,   
	netropology.utilerias.*,   
	com.netro.exception.*,
	com.netro.descuento.*, 
	com.netro.model.catalogos.*, 
	net.sf.json.JSONArray,   
	net.sf.json.JSONObject" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/13descuento/13secsession_extjs.jspf"%>
<% 

int start                     = 0;
int limit                     = 0;
JSONObject resultado 	      = new JSONObject();

String icIF                   = request.getParameter("cIntermediarioFinanciero"); if (icIF    == null) { icIF    = ""; }
String claveIF                = request.getParameter("claveIF");               if (claveIF == null) { claveIF = ""; }

String informacion            =(request.getParameter("informacion")!=null) ? request.getParameter("informacion"):"";
String operacion              =(request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String infoRegresar           = "";
	

 if (informacion.equals("Consulta") || informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")) {
	ConsOrdenLimiteIf paginador = new ConsOrdenLimiteIf(); 
	paginador.setIcIF(icIF);
	paginador.setINoCliente(iNoCliente);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String consulta = "";
	
	if(informacion.equals("Consulta")) {
	 try {
	  start = Integer.parseInt(request.getParameter("start"));
	  limit = Integer.parseInt(request.getParameter("limit"));	
	 } catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	 }
			
    try {
	  if (operacion.equals("Generar")) {	//Nueva consulta
		queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
	  }
	  consulta = queryHelper.getJSONPageResultSet(request,start,limit);															   		
	 } catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
    }
		
	 resultado = JSONObject.fromObject(consulta);
	 infoRegresar = resultado.toString();
	}
	else if (informacion.equals("ArchivoCSV")) {
	 try {
	  String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
	  JSONObject jsonObj = new JSONObject();
	  jsonObj.put("success", new Boolean(true));
	  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	  infoRegresar = jsonObj.toString();
	  } catch(Throwable e) {
		 throw new AppException("Error al generar el archivo CSV", e);
	  }
	}
	else if (informacion.equals("ArchivoPDF")) {
	 try {
	  String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"PDF");
	  JSONObject jsonObj = new JSONObject();
	  jsonObj.put("success", new Boolean(true));
	  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	  infoRegresar = jsonObj.toString();
	  } 
	  catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
	  }
	}
}else if(informacion.equals("Guardar")){
	String DatoIf[]	= request.getParameterValues("ArrayIf");
	String DatoEpo[]	= request.getParameterValues("ArrayEpo");
	String DatoOrden[]	= request.getParameterValues("ArrayOrden");
	String total = (request.getParameter("total")!=null) ? request.getParameter("total"):"";
	
	ConsOrdenLimiteIf paginador = new ConsOrdenLimiteIf(); 
	boolean  exito = paginador.guardarModificaciones(DatoIf,DatoEpo,DatoOrden,total);
	
	String mensaje = "";
	if(exito){
		mensaje = "Exito al guardar";
	}else{
		mensaje = "Fracaso al guardar";
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("MENSAJE",mensaje );
	infoRegresar = jsonObj.toString();
}

%>
<%=infoRegresar%>