<%@ page contentType="application/json;charset=UTF-8"
	import="
	java.util.*, java.sql.*,
	netropology.utilerias.*,
	com.netro.exception.*,
	com.netro.descuento.*,
	com.netro.model.catalogos.*,
	net.sf.json.JSONArray,
	net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/13descuento/13secsession_extjs.jspf"%>
<%

int start            = 0;
int limit            = 0;
JSONObject resultado = new JSONObject();

String icIF         = request.getParameter("cIntermediarioFinanciero"); if (icIF    == null) { icIF    = ""; }
String claveIF      = request.getParameter("claveIF");               if (claveIF == null) { claveIF = ""; }
String informacion  = (request.getParameter("informacion")!=null) ? request.getParameter("informacion"):"";
String operacion    = (request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String infoRegresar = "";

if (informacion.equals("catologoIntermediarioFinanciero")) {
 CatalogoIF cat = new CatalogoIF();
 cat.setCampoClave("ic_if");
 cat.setCampoDescripcion("cg_razon_social");
 cat.setClaveEpo(iNoCliente);
 //cat.setMandatoDocumentos("S");
 cat.setOrden("cg_razon_social");
 infoRegresar = cat.getJSONElementos();
}
else if (informacion.equals("Consulta") || informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")){
	com.netro.descuento.LimitesPorIF paginador = new com.netro.descuento.LimitesPorIF();
	paginador.setIcIF(icIF);
	paginador.setINoCliente(iNoCliente);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String consulta = "";

	if(informacion.equals("Consulta")) {
		try {
			start = Integer.parseInt(request.getParameter("start"));
			limit = Integer.parseInt(request.getParameter("limit"));	
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}

		try {
			if (operacion.equals("Generar")) {	//Nueva consulta
				queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
			}
			consulta = queryHelper.getJSONPageResultSet(request,start,limit);															   		
		} catch(Exception e) {
			throw new AppException("Error en la paginacion", e);
		}

		resultado = JSONObject.fromObject(consulta);
		infoRegresar = resultado.toString();
	} else if (informacion.equals("ArchivoCSV")){
		JSONObject jsonObj = new JSONObject();
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo CSV", e);
		}
		infoRegresar = jsonObj.toString();
	} else if (informacion.equals("ArchivoPDF")){
		JSONObject jsonObj = new JSONObject();
		try {
			String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"PDF");
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		} catch(Throwable e){
			jsonObj.put("success", new Boolean(false));
			throw new AppException("Error al generar el archivo PDF", e);
		}
		infoRegresar = jsonObj.toString();
	}
} else if (informacion.equals("ConsultaDetalle") || informacion.equals("ConsultaDetalleTotales") || informacion.equals("ArchivoDetalleCSV") || informacion.equals("ArchivoDetallePDF")) {
   com.netro.descuento.ConsDoctosProgPyme paginador = new com.netro.descuento.ConsDoctosProgPyme();
	paginador.setIc_if(claveIF);
	paginador.setIc_epo(iNoCliente);
	paginador.setTipoUsuario(strTipoUsuario);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String consulta = "";
	
	if(informacion.equals("ConsultaDetalle")) {
	 try {
	  start = Integer.parseInt(request.getParameter("start"));
	  limit = Integer.parseInt(request.getParameter("limit"));	
	 } catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	 }
			
    try {
	  if (operacion.equals("Generar")) {	//Nueva consulta
		queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
	  }
	  consulta = queryHelper.getJSONPageResultSet(request,start,limit);															   		
	 } catch(Exception e) {
		throw new AppException("Error en la paginacion", e);
    }
		
	 resultado = JSONObject.fromObject(consulta);
	 infoRegresar = resultado.toString();
	}
	else if (informacion.equals("ConsultaDetalleTotales") ) {
		
		queryHelper = new CQueryHelperRegExtJS(paginador); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion
	}
	else if (informacion.equals("ArchivoDetalleCSV")) {
	 try {
	  String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
	  JSONObject jsonObj = new JSONObject();
	  jsonObj.put("success", new Boolean(true));
	  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	  infoRegresar = jsonObj.toString();
	  } catch(Throwable e) {
		 throw new AppException("Error al generar el archivo CSV", e);
	  }
	}
}
%>
<%=infoRegresar%>
