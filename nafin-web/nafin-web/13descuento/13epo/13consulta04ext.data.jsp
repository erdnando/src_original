<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		java.math.*,
		java.io.File,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		netropology.utilerias.*,		
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%

	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String operacion = (request.getParameter("operacion") != null)?request.getParameter("operacion"):"";	
	String ic_if = (request.getParameter("ic_if") != null)?request.getParameter("ic_if"):"";
	String df_fecha_notMin = (request.getParameter("df_fecha_notMin") != null)?request.getParameter("df_fecha_notMin"):"";
	String df_fecha_notMax = (request.getParameter("df_fecha_notMax") != null)?request.getParameter("df_fecha_notMax"):"";
	String df_fecha_vencMin = (request.getParameter("df_fecha_vencMin") != null)?request.getParameter("df_fecha_vencMin"):"";
	String df_fecha_vencMax = (request.getParameter("df_fecha_vencMax") != null)?request.getParameter("df_fecha_vencMax"):"";
	String cc_acuse3 = (request.getParameter("cc_acuse3") != null)?request.getParameter("cc_acuse3"):"";
	String numero_siaff = (request.getParameter("numero_siaff") != null)?request.getParameter("numero_siaff"):"";
	String numeroProveedor = (request.getParameter("numeroProveedor") != null)?request.getParameter("numeroProveedor"):"";
	String tipo = (request.getParameter("tipo") != null)?request.getParameter("tipo"):""; //es para saber que tipo de archivo se va a descarga 
	String clavePyme = request.getAttribute("clavePyme")==null?"":(String)request.getAttribute("clavePyme");//FODEA 028 - 2010 ACF	
	String infoRegresar =""; 
	String 	claveEPO 						= iNoCliente;
	boolean operaNotasDeCredito                = false;	// Fodea 002 - 2010
	boolean aplicarNotasDeCreditoAVariosDoctos = false;	// Fodea 002 - 2010
	boolean estaHabilitadoNumeroSIAFF  = false;
	String bOperaFactorajeVencInfonavit  = "N";	
	String soperaNotasDeCredito                =  "N";	// Fodea 002 - 2010
	String saplicarNotasDeCreditoAVariosDoctos =  "N";	// Fodea 002 - 2010
	String sestaHabilitadoNumeroSIAFF  =  "N";	 
	String bOperaFactorajeDistribuido  = "N";
	String banderaTablaDoctos  = "";
	String bOperaFideicomiso = "N";
        String fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	
	int start= 0, limit =0;
	AccesoDB con = new AccesoDB();
	
	String bOperaPubHas  = 	bOperaPubHash(claveEPO);		
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
	
	bOperaFactorajeDistribuido = BeanParamDscto.getfacVencido( claveEPO,  "1") ;
	
	estaHabilitadoNumeroSIAFF 	= BeanParamDscto.estaHabilitadoNumeroSIAFF(claveEPO); // VERIFICAR SI ESTA HABILITADO EL NUMERO SIAFF 

	Hashtable alParamEPO = new Hashtable();
	if(!iNoCliente.equals("")) {
		alParamEPO = BeanParamDscto.getParametrosEPO(iNoCliente, 1);
		if (alParamEPO!=null) {
			bOperaFactorajeVencInfonavit = alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString();
			bOperaFideicomiso = alParamEPO.get("CS_OPERA_FIDEICOMISO").toString();	
		}
	}
	
	// VERIFICAR SI LA EPO TIENE PARAMETRIZADO EL USO DE UNA NOTA DE CREDITO EN MULTIPLES DOCUMENTOS	
	// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Operacion de las Notas de Credito
	operaNotasDeCredito                = BeanParamDscto.operaNotasDeCreditoParaDescuentoElectronico(claveEPO);
	// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Opcion de Aplicar una Nota de Credito a Varios Documentos
	aplicarNotasDeCreditoAVariosDoctos = BeanParamDscto.hasAplicarNotaDeCreditoAVariosDoctosEnabled(claveEPO);
	
	if(operaNotasDeCredito==true)  soperaNotasDeCredito  = "S";	
	if(aplicarNotasDeCreditoAVariosDoctos==true)  saplicarNotasDeCreditoAVariosDoctos  = "S";	
	if(estaHabilitadoNumeroSIAFF==true)  sestaHabilitadoNumeroSIAFF  = "S";	
	
	if(BeanSeleccionDocumento.bgetExisteParamDocs(iNoCliente)){
		banderaTablaDoctos = "1";
	}

	sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";

	JSONObject 	resultado	= new JSONObject();		
	HashMap	datos = new HashMap();
	JSONArray registros = new JSONArray();
	
        if (informacion.equals("valoresIniciales")  ) {

	String  limitarRangoFechas =	getLimitarRangoFechas();
        JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));	
	jsonObj.put("fechaHoy", fechaHoy);
	jsonObj.put("limitarRangoFechas", limitarRangoFechas);
	infoRegresar = jsonObj.toString();	
	}else if (informacion.equals("CatalogoIF") ) {

		CatalogoIFDescuento catalogo = new CatalogoIFDescuento();
		catalogo.setCampoClave("ic_if");
		catalogo.setCampoDescripcion("cg_razon_social");
		catalogo.setClaveEpo(claveEPO);		
		catalogo.setOrden("i.ic_if");	
		
		//bOperaFideicomiso = "N"; //Quitar!!!
		
		if(bOperaFideicomiso.equals("S")){
			catalogo.setFideicomiso("N");
		}		
		infoRegresar = catalogo.getJSONElementos();	
		
	}else if (informacion.equals("Consultar") ||  informacion.equals("ResumenTotales")
		||  informacion.equals("ArchivoPDF") ||  informacion.equals("ArchivoPDFxPagina")
		||  informacion.equals("ArchivoCSV")   ||  informacion.equals("ArchivoTXT")  
		|| informacion.equals("ArchivoEPO211")  ) {
		
		ParametrosDescuento ejbParam = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

		java.util.Map paramDescuento = ejbParam.getParametrosModuloDescuento(iNoCliente, "E", null);				
						
		String consulta ="";		
		String operaNC2 = paramDescuento.get("operaNC").toString();
		//System.out.println("operaNC2>>>>>>>>"+operaNC2);
		SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
		String fechaActual = fecha2.format(new java.util.Date());
		 
		//se obtienes los campos adicionales
		String strCampos  ="", nomCampo1 ="", nomCampo2 ="", nomCampo3="", nomCampo4="", nomCampo5 ="";
		List  campos = BeanParamDscto.getCamposAdicionales( claveEPO,  "1");
		int hayCamposAdicionales = campos.size();
		
		for (int i=0; i<campos.size(); i++) { 
			List lovRegistro = (List) campos.get(i);
			strCampos += (String) lovRegistro.get(0);	
			if(i==0) 	nomCampo1 = (String) lovRegistro.get(1);
			if(i==1) 	nomCampo2 = (String) lovRegistro.get(1);
			if(i==2) 	nomCampo3 = (String) lovRegistro.get(1); 
			if(i==3) 	nomCampo4 = (String) lovRegistro.get(1);
			if(i==4) 	nomCampo5 = (String) lovRegistro.get(1);			
		}	
	int noDocumento =  5-campos.size(); // es para mostrar la columna de Número de Documento
	String mensaje = "";
	if (application.getInitParameter("EpoEntregaAnticipadaDeVivienda").equals(iNoCliente)) {
	mensaje = "	Para efectos del artículo 2038 del Código Civil Federal me doy por notificado "+
						" de la cesión de derechos de las operaciones descritas en la presente pantalla. "+
						" \n  Y por lo tanto me obligo incondicionalmente a hacer el pago al beneficiario "+
						"	de los derechos de cobro de los documentos que aparecen en esta pantalla "+
						" en las fechas y por los montos establecidos.";
	} else {
        mensaje = "De acuerdo a lo dispuesto por los artículos 427 de la Ley General de Títulos y Operaciones de Crédito, 32 c del Código Fiscal de la Federación y 2038 del Código Civil Federal,"+
                "le notifico que el documento cuyos datos aparecen en esta página, fue descontado /factoreado por el PROVEEDOR a mi favor. Por lo que en la fecha de su vencimiento,"+
                "deberá cubrirme la cantidad total del mismo, en tiempo y forma, sin deducción ni tardanza alguna.\n\n"+
                "Por otra parte, a partir del 17 de octubre de 2018 del 2018 el PROVEEDOR ha manifestado bajo protesta de decir verdad, que sí ha emitido o emitirá a la EMPRESA DE PRIMER ORDEN el "+
                "CFDI por la operación comercial que le dio origen a esta transacción, según sea el caso y conforme establezcan las disposiciones fiscales vigentes.";
	}    
	
	 if (!numeroProveedor.equals("")) {
		clavePyme = BeanParamDscto.obtenerPymeConNumeroDeProveedor(numeroProveedor, iNoCliente);
	}		  
	
	
		AvisNotiEpoDE paginador = new AvisNotiEpoDE();
		paginador.setIc_if(ic_if);
		paginador.setDf_fecha_notMin(df_fecha_notMin);
		paginador.setDf_fecha_notMax(df_fecha_notMax);
		paginador.setDf_fecha_vencMin(df_fecha_vencMin);
		paginador.setDf_fecha_vencMax(df_fecha_vencMax);
		paginador.setCc_acuse3(cc_acuse3);
		paginador.setNumero_siaff(numero_siaff);
		paginador.setClavePyme(clavePyme);
		paginador.setClaveEpo(claveEPO);
		paginador.setOperaNC(operaNC2);
		paginador.setFechaActual(fechaActual);
		paginador.setIdioma(idioma);
		paginador.setStrCampos(strCampos);
		paginador.setOperaFVPyme(operaFVPyme);
		paginador.setBanderaTablaDoctos(banderaTablaDoctos);
		paginador.setMensaje(mensaje);
		paginador.setTipo(tipo);
	
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		Registros rsConsulta  =null;
		
		int totalRegistros =0;
		
		if (informacion.equals("Consultar")  ) {	
			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));				
				
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				
				rsConsulta = queryHelper.getPageResultSet(request,start,limit);
				totalRegistros = queryHelper.getIdsSize();
					
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
			
			while (rsConsulta.next())	{
				
			String intermediario =	(rsConsulta.getString("INTERMEDIARIO")==null)?"":rsConsulta.getString("INTERMEDIARIO").trim();
			String fecha_notificacion =	(rsConsulta.getString("FECHA_NOTIFICACION")==null)?"":rsConsulta.getString("FECHA_NOTIFICACION").trim();
			String acuse_notificacion =	(rsConsulta.getString("ACUSE_NOTIFICACION")==null)?"":rsConsulta.getString("ACUSE_NOTIFICACION").trim();
			String num_proveedor =	(rsConsulta.getString("NUM_PROVEEDOR")==null)?"":rsConsulta.getString("NUM_PROVEEDOR").trim();
			String nombre_proveedor =	(rsConsulta.getString("NOMBRE_PROVEEDOR")==null)?"":rsConsulta.getString("NOMBRE_PROVEEDOR").trim();
			String num_docto =	(rsConsulta.getString("NUM_DOCTO")==null)?"":rsConsulta.getString("NUM_DOCTO").trim();
			String fecha_emision =	(rsConsulta.getString("FECHA_EMISION")==null)?"":rsConsulta.getString("FECHA_EMISION").trim();
			String fecha_vencimiento =	(rsConsulta.getString("FECHA_VENCIMIENTO")==null)?"":rsConsulta.getString("FECHA_VENCIMIENTO").trim();
			String df_fecha_ven_pyme =	(rsConsulta.getString("DF_FECHA_VEN_PYME")==null)?"":rsConsulta.getString("DF_FECHA_VEN_PYME").trim();
			String moneda =	(rsConsulta.getString("MONEDA")==null)?"":rsConsulta.getString("MONEDA").trim();
			String tipo_factoraje =	(rsConsulta.getString("TIPO_FACTORAJE")==null)?"":rsConsulta.getString("TIPO_FACTORAJE").trim();
			String monto =	(rsConsulta.getString("MONTO")==null)?"":rsConsulta.getString("MONTO").trim();
			String por_descuento =	(rsConsulta.getString("POR_DESCUENTO")==null)?"":rsConsulta.getString("POR_DESCUENTO").trim();
			String monto_descontar =	(rsConsulta.getString("MONTO_DESCONTAR")==null)?"":rsConsulta.getString("MONTO_DESCONTAR").trim();
			String clave_banco =	(rsConsulta.getString("CLAVE_BANCO")==null)?"":rsConsulta.getString("CLAVE_BANCO").trim();
			String banco =	(rsConsulta.getString("BANCO")==null)?"":rsConsulta.getString("BANCO").trim();
			String num_cuenta =	(rsConsulta.getString("NUM_CUENTA")==null)?"":rsConsulta.getString("NUM_CUENTA").trim();
			String referencia =	(rsConsulta.getString("REFERENCIA")==null)?"":rsConsulta.getString("REFERENCIA").trim();
			String doct_aplicado =	(rsConsulta.getString("DOCT_APLICADO")==null)?"":rsConsulta.getString("DOCT_APLICADO").trim();
			String beneficiario =	(rsConsulta.getString("BENEFICIARIO")==null)?"":rsConsulta.getString("BENEFICIARIO").trim();
			String por_beneficiario =	(rsConsulta.getString("POR_BENEFICIARIO")==null)?"":rsConsulta.getString("POR_BENEFICIARIO").trim();
			String imp_recibir_bene =	(rsConsulta.getString("IMP_RECIBIR_BENE")==null)?"":rsConsulta.getString("IMP_RECIBIR_BENE").trim();
			String dig_identificador =	(rsConsulta.getString("DIG_IDENTIFICADOR")==null)?"":rsConsulta.getString("DIG_IDENTIFICADOR").trim();
			String df_entrega =	(rsConsulta.getString("DF_ENTREGA")==null)?"":rsConsulta.getString("DF_ENTREGA").trim();
			String tipo_compra =	(rsConsulta.getString("TIPO_COMPRA")==null)?"":rsConsulta.getString("TIPO_COMPRA").trim();
			String clasificador =	(rsConsulta.getString("CLASIFICADOR")==null)?"":rsConsulta.getString("CLASIFICADOR").trim();
			String plazo_maximo =	(rsConsulta.getString("PLAZO_MAXIMO")==null)?"":rsConsulta.getString("PLAZO_MAXIMO").trim();
			String ic_documento =	(rsConsulta.getString("IC_DOCUMENTO")==null)?"":rsConsulta.getString("IC_DOCUMENTO").trim();
			String campo1 ="", campo2 ="",campo3 ="",campo4 ="",campo5 ="";
					int cam=1;
					for (int x=0; x<campos.size(); x++) { 
						String nombre ="CAMPO"+cam;
						String strCampo = rsConsulta.getString(nombre);	
						if(cam==1)  campo1 =strCampo;
						if(cam==2)  campo2 =strCampo;
						if(cam==3)  campo3 =strCampo;
						if(cam==4)  campo4 =strCampo;
						if(cam==5)  campo5 =strCampo;
						cam++;						
					} 
					
				if(tipo_factoraje.equals("C") && operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos ){
					String icNotaCredito = ic_documento;
					doct_aplicado = BeanSeleccionDocumento.getNumerosDeDocumento(icNotaCredito,",");
				}
				
				datos = new HashMap();
				datos.put("INTERMEDIARIO", intermediario);
				datos.put("FECHA_NOTIFICACION",fecha_notificacion);
				datos.put("ACUSE_NOTIFICACION", acuse_notificacion);
				datos.put("NUM_PROVEEDOR", num_proveedor);
				datos.put("NOMBRE_PROVEEDOR", nombre_proveedor);
				datos.put("NUM_DOCTO", num_docto);
				datos.put("FECHA_EMISION", fecha_emision);
				datos.put("FECHA_VENCIMIENTO", fecha_vencimiento);
				datos.put("DF_FECHA_VEN_PYME", df_fecha_ven_pyme);
				datos.put("MONEDA", moneda);
				datos.put("TIPO_FACTORAJE", tipo_factoraje);
				datos.put("MONTO", monto);
				datos.put("POR_DESCUENTO", por_descuento);
				datos.put("MONTO_DESCONTAR", monto_descontar);
				datos.put("CLAVE_BANCO",clave_banco);
				datos.put("BANCO", banco);
				datos.put("NUM_CUENTA", num_cuenta);
				datos.put("REFERENCIA", referencia);
				datos.put("DOCT_APLICADO", doct_aplicado);
				datos.put("CAMPO1", campo1);
				datos.put("CAMPO2", campo2);
				datos.put("CAMPO3", campo3);
				datos.put("CAMPO4", campo4);
				datos.put("CAMPO5", campo5);
				datos.put("BENEFICIARIO", beneficiario);
				datos.put("POR_BENEFICIARIO", por_beneficiario);
				datos.put("IMP_RECIBIR_BENE", imp_recibir_bene);
				datos.put("DIG_IDENTIFICADOR", dig_identificador);
				datos.put("DF_ENTREGA", df_entrega);
				datos.put("TIPO_COMPRA", tipo_compra);
				datos.put("CLASIFICADOR", clasificador);
				datos.put("PLAZO_MAXIMO", plazo_maximo);			
				registros.add(datos);		
			}
							
	
			String consulta2 =  "{\"success\": true, \"total\": \"" + totalRegistros + "\", \"registros\": " + registros.toString()+"}";
			resultado = JSONObject.fromObject(consulta2);			
			resultado.put("hayCamposAdicionales",String.valueOf(hayCamposAdicionales));			
			resultado.put("noDocumento",String.valueOf(noDocumento));
			resultado.put("nomCampo1",String.valueOf(nomCampo1));
			resultado.put("nomCampo2",String.valueOf(nomCampo2));
			resultado.put("nomCampo3",String.valueOf(nomCampo3));
			resultado.put("nomCampo4",String.valueOf(nomCampo4));
			resultado.put("nomCampo5",String.valueOf(nomCampo5));			
			resultado.put("operaFVPyme",operaFVPyme);
			resultado.put("operaNC",operaNC2);
			resultado.put("bOperaFactorajeDistribuido", bOperaFactorajeDistribuido);
			resultado.put("bOperaFactorajeVencInfonavit", bOperaFactorajeVencInfonavit);
			resultado.put("sestaHabilitadoNumeroSIAFF", sestaHabilitadoNumeroSIAFF);	
			resultado.put("banderaTablaDoctos", banderaTablaDoctos);	
			resultado.put("bOperaPubHas", bOperaPubHas);	
			
			
			infoRegresar = resultado.toString();						
		
		} else if (informacion.equals("ResumenTotales")) {		
			//Debe ser llamado despues de Consulta
			queryHelper = new CQueryHelperRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
			infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion			
				
		} else if (informacion.equals("ArchivoPDF")) {
			try {
				
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");	
				
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}		
		
		} else if (informacion.equals("ArchivoPDFxPagina")) {
			try {
				
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				String nombreArchivo = queryHelper.getCreatePageCustomFile(request, start,  limit, strDirectorioTemp, "PDF");			
				
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF por Pagina", e);
			}		
		
		} else if (informacion.equals("ArchivoCSV")) {
					try {
				
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	
				
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}	
			
		} else if (informacion.equals("ArchivoTXT")) { //esto es para los archivos HASH
					try {			
			
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	
							
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo TXT", e);
			}			
		} else if (informacion.equals("ArchivoEPO211")) {
					try {
				
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "CSV");	
				
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}	
			
		}
	}	
	
%>

<%=infoRegresar%>

<%!



/**
 * Determina si es necesario limitar el rango de fechas que se especifique como
 * criterios de búsqueda, con el fin de evitar consultas demasiado pesadas en
 * horario de alta demanda.
 * @return Si la hora actual es menor a las 15 h, regresa true
 * 		false de lo contrario.
 */
private String getLimitarRangoFechas() {
	SimpleDateFormat sdf = new SimpleDateFormat("HH");	//HH es formato 24 horas.
	String hora = sdf.format(new java.util.Date());
	int iHora = Integer.parseInt(hora);
	if (iHora < 15) {
		return "S";
	} else {
		return "N";
	}
}

%>

<%!
	public String  bOperaPubHash(String  ic_epo ){
		
		String 				qrySentencia 	= null;
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		String bOperaPubHash = "N";
		
		try{	
			con.conexionDB();	
			qrySentencia =" SELECT  CS_PUB_hash"   +
								"   FROM comrel_producto_epo"   +
								"  WHERE ic_producto_nafin = 1"   +
								"    AND ic_epo = ? ";
								  
					
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,Integer.parseInt(ic_epo));
				rs = ps.executeQuery();
				if(rs.next()){
					 bOperaPubHash =(rs.getString(1));					
				} 
				rs.close();
				ps.close();
			
	} catch(Exception e) {
	
		e.printStackTrace();
	} finally {	
		if(con.hayConexionAbierta()) 
			con.cierraConexionDB();	
	}
		return bOperaPubHash;
	}
%>


