<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.sql.*,
		netropology.utilerias.*,
		com.netro.descuento.*,
		com.netro.afiliacion.*,
		com.netro.threads.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.CatalogoIFEpo,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoCamposAdicionales,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ page import="com.netro.pdf.*"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";
ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
Hashtable alParamEPO = new Hashtable();
ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
double dblPorciento = 0, dblPorcientoDL = 0;

System.out.println ("informacion -----> "+informacion);


if (informacion.equals("Parametrizacion")) {	//	*-*-*-*-*-*--*-*-*-*-*-*-*-*	Parametrizacion	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	Registros regCamposAdicionales = null;
	String sFechaVencPyme		=	""; 
	JSONObject jsonObj = new JSONObject();
	int totalcd=0;

	alParamEPO = new Hashtable();
	boolean bOperaFirmaMancomunada		= false;
   boolean bOperaFactorajeVencido		= false;
	boolean bOperaFactorajeDistribuido	= false;
	boolean bOperaFactorajeNotaDeCredito= false;
	boolean bOperaFactorajeVencInfonavit= false;
	boolean bOperaFactConMandato			= false;
	boolean bTipoFactoraje					= false;
	boolean bFactorajeIf						= false;
	boolean bMostrarFechaAutorizacionIFInfoDoctos = false; // Fodea 040 - 2011
	
	if(!iNoCliente.equals("")) {
		jsonObj.put("iNoCliente", iNoCliente);
		alParamEPO = BeanParamDscto.getParametrosEPO(iNoCliente, 1);
		if (alParamEPO!=null) {
			bOperaFirmaMancomunada					=	("N".equals(alParamEPO.get("PUB_FIRMA_MANC").toString())) ? false : true ;
			bOperaFactConMandato						=	("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
			bOperaFactorajeVencido					=	("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
			bOperaFactorajeDistribuido				=	("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
			bOperaFactorajeNotaDeCredito			=	("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true; 
			bOperaFactorajeVencInfonavit			=	("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true; //FODEA 042 - Agosto/2009
			bMostrarFechaAutorizacionIFInfoDoctos=	"S".equals((String) alParamEPO.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS") )?true:false; // Fodea 040 - 2011
			bFactorajeIf								=	("N".equals(alParamEPO.get("FACTORAJE_IF").toString()))?false:true; //FODEA Noviembre 2012
		}
		bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaDeCredito || bOperaFactConMandato || bOperaFactorajeVencInfonavit||bFactorajeIf)?true:false;
		sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(iNoCliente);
		String banderaTablaDoctos = "";
		if(BeanSeleccionDocumento.bgetExisteParamDocs(iNoCliente)){
			banderaTablaDoctos = "1";
		}
		
		regCamposAdicionales = new Registros();
		regCamposAdicionales = BeanParamDscto.getCamposAdicionales(iNoCliente);
		totalcd = regCamposAdicionales.getNumeroRegistros();
		jsonObj.put("bOperaFirmaMancomunada", new Boolean(bOperaFirmaMancomunada));
		jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));		
		jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
		jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
		jsonObj.put("bOperaFactorajeNotaDeCredito", new Boolean(bOperaFactorajeNotaDeCredito));
		jsonObj.put("bOperaFactorajeVencInfonavit", new Boolean(bOperaFactorajeVencInfonavit));
		jsonObj.put("bMostrarFechaAutorizacionIFInfoDoctos", new Boolean(bMostrarFechaAutorizacionIFInfoDoctos));
		jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
		jsonObj.put("sFechaVencPyme", sFechaVencPyme);
		jsonObj.put("banderaTablaDoctos", banderaTablaDoctos);
		java.util.Map paramDescuento = BeanParamDscto.getParametrosModuloDescuento(iNoCliente, "P", iNoEPO);
		jsonObj.put("strAforo", paramDescuento.get("strAforo"));
		jsonObj.put("strAforoDL", paramDescuento.get("strAforoDL"));
		//jsonObj.put("strAforo", strAforo);
		//jsonObj.put("strAforoDL", strAforoDL);

		if (regCamposAdicionales != null){
			jsonObj.put("camp_dina1", regCamposAdicionales.getJSONData() );
			jsonObj.put("totalcd", Integer.toString(totalcd));			
		}
		if(BeanParamDscto.estaHabilitadoNumeroSIAFF(iNoCliente)){
			jsonObj.put("esNumeroSIAFF", "true");
		}
	}

	jsonObj.put("strPerfil", strPerfil);

	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("obtenNombrePyme")) {

	JSONObject jsonObj = new JSONObject();
	String numeroDeProveedor = (request.getParameter("numeroDeProveedor")!=null)?request.getParameter("numeroDeProveedor"):"";
	HashMap	datosPYME	= BeanParamDscto.getParametrosPYME(numeroDeProveedor,iNoCliente);

	if(datosPYME != null){
		jsonObj.put("ic_pyme", (String) datosPYME.get("IC_PYME"));
		jsonObj.put("cg_pyme_epo_interno", (String) datosPYME.get("NUMERO_PROVEEDOR"));
		jsonObj.put("txtCadenasPymes", (String) datosPYME.get("DESCRIPCION"));
	}

	if(datosPYME == null && !numeroDeProveedor.equals("")){
		jsonObj.put("muestraMensaje", new Boolean(true));
	}

	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("CatalogoIf")) {

	CatalogoIFEpo cat = new CatalogoIFEpo();
	cat.setCampoClave("ic_if");
	cat.setCampoDescripcion("cg_razon_social");
	cat.setClaveEpo(iNoCliente);
	cat.setOrden("1");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("CatalogoMoneda")) {

	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	infoRegresar = cat.getJSONElementos();
  
}else if (informacion.equals("CatalogoCambioEstatus")) {

	String bOperaFirmaMancomunada = BeanParamDscto.getOperaFirmaMancomunada(iNoCliente,1);
	String estatus_in="";

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_docto");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_estatus_docto");

	if("ADMIN EPO MAN0".equals(strPerfil) || "ADMIN EPO MAN1".equals(strPerfil)){
		estatus_in= "8,13,14,15,16,17,18,19,20,22,23,25,27";
		if("ADMIN EPO MAN0".equals(strPerfil)){
			//estatus_in= "30,31";
			estatus_in= estatus_in+",28,29";
		}
		//if("ADMIN EPO MAN1".equals(strPerfil)){
		//	estatus_in= "28,29,30,31";
		//}
		//cat.setValoresCondicionIn(estatus_in, Integer.class);	
		cat.setValoresCondicionNotIn(estatus_in, Integer.class);
	}else{
		estatus_in= "8,13,14,15,16,17,18,19,20,22,23,25,27";
		if (!bOperaFirmaMancomunada.equals("S")){
			estatus_in= estatus_in+"28,29,30,31";
		}
		cat.setValoresCondicionNotIn(estatus_in, Integer.class);
	}
	cat.setOrden("ic_estatus_docto");
	infoRegresar = cat.getJSONElementos();

}else if(informacion.equals("CatalogoCamposAdicionales")){

		CatalogoCamposAdicionales cat = new CatalogoCamposAdicionales();
		cat.setCampoClave("cg_campo1");
		cat.setCampoDescripcion("cg_campo1");
		//sgParametrosvePyme(iNoCliente);
		cat.setClaveEpo(iNoCliente);
		cat.setOrden("cg_campo1");
		infoRegresar = cat.getJSONElementos();
		
} else  if ("catTipoFactorajeData".equals(informacion)   ) {
    
    StringBuilder fac = new StringBuilder(); 
    fac.append("N");
    alParamEPO = BeanParamDscto.getParametrosEPO(iNoCliente, 1);
    if (alParamEPO!=null) {    
	if ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString())  ) {
	    fac.append(fac.length() > 0 ? ", " : "").append("V");
	}
	if ("S".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString())  ) {
	    fac.append(fac.length() > 0 ? ", " : "").append("D");
	}
    }
 
    CatalogoSimple catalogo = new CatalogoSimple();
    catalogo.setCampoClave("CC_TIPO_FACTORAJE");
    catalogo.setCampoDescripcion("CG_NOMBRE");
    catalogo.setTabla("COMCAT_TIPO_FACTORAJE");		
    catalogo.setOrden("CG_NOMBRE");
    catalogo.setValoresCondicionIn(fac.toString(), String.class);
    infoRegresar = catalogo.getJSONElementos();	


}else if(informacion.equals("CatalogoCamposAdicionales_2")){

		CatalogoCamposAdicionales cat = new CatalogoCamposAdicionales();
		cat.setCampoClave("cg_campo2");
		cat.setCampoDescripcion("cg_campo2");
		//cat.setClavePyme(iNoCliente);
		cat.setClaveEpo(iNoCliente);
		cat.setOrden("cg_campo2");
		infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("CatalogoBuscaAvanzada")) {

	String num_pyme	= (request.getParameter("num_pyme")==null)?"":request.getParameter("num_pyme");
	String rfc_pyme		= (request.getParameter("rfc_pyme")==null)?"":request.getParameter("rfc_pyme");
	String nombre_pyme	= (request.getParameter("nombre_pyme")==null)?"":request.getParameter("nombre_pyme");
	
	String ic_producto_nafin = (request.getParameter("ic_producto_nafin")==null)?"":request.getParameter("ic_producto_nafin");
	String ic_epo = iNoCliente;
	String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	String pantalla = "InformacionDocumentos";
	String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");

	Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);

	Registros registros = afiliacion.getProveedores(ic_producto_nafin, ic_epo,num_pyme,rfc_pyme,nombre_pyme,ic_pyme,pantalla,ic_if);
	
	List coleccionElementos = new ArrayList();
	JSONArray jsonArr = new JSONArray();
	while (registros.next()){
		ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
		//elementoCatalogo.setClave(registros.getString("CG_PYME_EPO_INTERNO"));
		elementoCatalogo.setClave(registros.getString("IC_PYME"));
		elementoCatalogo.setDescripcion(registros.getString("CG_PYME_EPO_INTERNO")+" "+registros.getString("CG_RAZON_SOCIAL"));
		coleccionElementos.add(elementoCatalogo);
	}	
	Iterator it = coleccionElementos.iterator();
	while(it.hasNext()) {
		Object obj = it.next();
		if (obj instanceof netropology.utilerias.ElementoCatalogo) {
			ElementoCatalogo ec = (ElementoCatalogo)obj;
			jsonArr.add(JSONObject.fromObject(ec));
		}
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (jsonArr.size() == 0){
		jsonObj.put("total", "0");
		jsonObj.put("registros", "" );
	}else if (jsonArr.size() > 0 && jsonArr.size() < 1001 ){
		jsonObj.put("total",  Integer.toString(jsonArr.size()));
		jsonObj.put("registros", jsonArr.toString() );
	}else if (jsonArr.size() > 1000 ){
		jsonObj.put("total", "excede" );
		jsonObj.put("registros", "" );
	}
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion

	int start = 0;
	int limit = 0;

	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	String noEstatusDocto = (request.getParameter("ic_estatus_docto") == null) ? "" : request.getParameter("ic_estatus_docto");
	
	boolean ocultarDoctosAplicados = (
			noEstatusDocto.equals("") 	||
			noEstatusDocto.equals("1") ||
			noEstatusDocto.equals("2") ||
			noEstatusDocto.equals("5")	||
			noEstatusDocto.equals("6") ||
			noEstatusDocto.equals("7") ||
			noEstatusDocto.equals("9")	||
			noEstatusDocto.equals("10")||
			noEstatusDocto.equals("21")||
			noEstatusDocto.equals("23")||
			noEstatusDocto.equals("28")||
			noEstatusDocto.equals("29")||
			noEstatusDocto.equals("30")||
			noEstatusDocto.equals("31") )?true:false;
	String nombreTipoFactoraje	=	"";
	String icDocumento 			=	"";
	String numeroDocumento		=	"";
	String esNotaDeCreditoSimpleAplicada					=	"";
	String esDocumentoConNotaDeCreditoSimpleAplicada	=	"";
	String esNotaDeCreditoAplicada							=	"";
	String esDocumentoConNotaDeCreditoMultipleAplicada	=	"";
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new com.netro.descuento.ConsDoctosDE());
	
	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		Registros reg  = new Registros();
		reg = queryHelper.getPageResultSet(request,start,limit);
		int cont = 0;
		while(reg.next())	{
			if (cont >= 1){
				nombreTipoFactoraje = (reg.getString("CS_DSCTO_ESPECIAL")==null)?"":reg.getString("CS_DSCTO_ESPECIAL");
				icDocumento = reg.getString(1);
				if(!ocultarDoctosAplicados){
					esNotaDeCreditoSimpleAplicada						= ( nombreTipoFactoraje.equals("C") && (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("11")) && BeanParamDscto.existeDocumentoAsociado(icDocumento) )?"S":"N";
					esDocumentoConNotaDeCreditoSimpleAplicada		= ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(icDocumento))?"S":"N";
		
					esNotaDeCreditoAplicada 							= (nombreTipoFactoraje.equals("C") && (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(icDocumento))?"S":"N";
					esDocumentoConNotaDeCreditoMultipleAplicada	= (!nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(icDocumento))?"S":"N";
		
					if(esNotaDeCreditoSimpleAplicada.equals("S")){
						if (icDocumento != null && !icDocumento.equals("")){numeroDocumento	=	BeanSeleccionDocumento.getNumeroDoctoAsociado(icDocumento);}
					}else if(esDocumentoConNotaDeCreditoSimpleAplicada.equals("S")){
						if (icDocumento != null && !icDocumento.equals("")){numeroDocumento	=	BeanSeleccionDocumento.getNumeroNotaCreditoAsociada(icDocumento);}
					}
				}
				/*
				*	Se llenan las columnas que se agregaron a la clase ConsDoctosPymeDE()
				*/
				reg.setObject("NOTA_SIMPLE",esNotaDeCreditoSimpleAplicada);
				reg.setObject("NOTA_SIMPLE_DOCTO",esDocumentoConNotaDeCreditoSimpleAplicada);
				reg.setObject("NOTA_MULTIPLE",esNotaDeCreditoAplicada);
				reg.setObject("NOTA_MULTIPLE_DOCTO",esDocumentoConNotaDeCreditoMultipleAplicada);
				reg.setObject("NUMERO_DOCTO",numeroDocumento);
				esNotaDeCreditoSimpleAplicada="";
				esDocumentoConNotaDeCreditoSimpleAplicada="";
				esNotaDeCreditoAplicada="";
				esDocumentoConNotaDeCreditoMultipleAplicada="";
				numeroDocumento = "";
			}
		cont++;
		}
		infoRegresar	=	"{\"success\": true, \"total\": \"" + queryHelper.getResultSetSize(request) + "\", \"registros\": " + reg.getJSONData()+"}";

	} catch(Exception e) {	
		throw new AppException("Error en la paginacion", e);
	}
} else if (informacion.equals("ResumenTotales")) {		//Datos para el Resumen de Totales

	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion

}else if (informacion.equals("obtenAcuse")) {

	String cc_acuse = (request.getParameter("acuse")==null)?"":request.getParameter("acuse");
	CargaDocumento bean = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);

	if(!cc_acuse.equals("")){
		Registros reg = bean.getDatosAcuse(cc_acuse);
		JSONObject jsonObj = new JSONObject();

		if (reg != null){

			jsonObj.put("datosAcuse", reg.getJSONData() );

			int nRow = 0;
			CreaArchivo archivo = new CreaArchivo();
			String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText(" ","formasrep",ComunesPDF.RIGHT);
			String usuario="";
			if (reg.next()) {
				if(nRow == 0){
					String clave_usuario="", nombreUsuario="";
					UtilUsr utilUsr = new UtilUsr();
					clave_usuario = reg.getString("ic_usuario");
					nombreUsuario = utilUsr.getUsuario(reg.getString("ic_usuario")).getNombreCompleto();
					usuario = clave_usuario+" - "+nombreUsuario;
					jsonObj.put("usuario", usuario);
					pdfDoc.setTable(3, 60);
					pdfDoc.setCell("Datos del Acuse","celda01rep",ComunesPDF.CENTER,3);
					pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda Nacional","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Dólares","celda01rep",ComunesPDF.CENTER);
				}
				pdfDoc.setCell("No. Total de documentos cargados","celda01rep",ComunesPDF.RIGHT);
				pdfDoc.setCell(Comunes.formatoDecimal(reg.getString("in_total_docto_mn"),0),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell(Comunes.formatoDecimal(reg.getString("in_total_docto_dl"),0),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("Monto total de los documentos cargados","celda01rep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(reg.getString("fn_total_monto_mn"),2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(reg.getString("fn_total_monto_dl"),2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("Numero de Acuse","celda01rep",ComunesPDF.RIGHT);
				pdfDoc.setCell(cc_acuse,"formasrep",ComunesPDF.LEFT,2);
				pdfDoc.setCell("Fecha de carga","celda01rep",ComunesPDF.RIGHT);
				pdfDoc.setCell(reg.getString("fecha_carga"),"formasrep",ComunesPDF.LEFT,2);
				pdfDoc.setCell("Hora de carga","celda01rep",ComunesPDF.RIGHT);
				pdfDoc.setCell(reg.getString("hora_carga"),"formasrep",ComunesPDF.LEFT,2);
				pdfDoc.setCell("Usuario","celda01rep",ComunesPDF.RIGHT);
				pdfDoc.setCell(usuario,"formasrep",ComunesPDF.LEFT,2);

				nRow++;
			}
			pdfDoc.addTable();
			pdfDoc.addText("* Si desea consultar el detalle del acuse, dirigirse a realizar la consulta con base al número de acuse mostrado.","formasrep",ComunesPDF.CENTER);
			pdfDoc.endDocument();
			jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

} else if (informacion.equals("obtenDetalles")) {
	Registros regDatos = new Registros();
	Registros regDatosDetalle = new Registros();
	Registros regCamposDetalle = null;
	String ic_documento = (request.getParameter("ic_docto")==null)?"":request.getParameter("ic_docto");
	if(!ic_documento.equals("") ){
		regDatos = BeanParamDscto.getDatosDocumentos("",ic_documento);
		regDatosDetalle = BeanParamDscto.getDatosDocumentosDetalle(ic_documento);
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		if (regDatos != null){
			jsonObj.put("datos_doctos", regDatos.getJSONData() );
		}
		if (regDatosDetalle != null){
			jsonObj.put("datos_detalle", regDatosDetalle.getJSONData() );
		}
		regCamposDetalle = new Registros();
		regCamposDetalle = BeanParamDscto.getCamposAdicionalesDetalle(iNoCliente);
		if (regCamposDetalle != null){
			jsonObj.put("dinamicosDetalle", regCamposDetalle.getJSONData() );
		}

		infoRegresar = jsonObj.toString();

	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
} else if (informacion.equals("obtenModifica")) {

	//String sFechaVencPyme = (request.getParameter("sFechaVencPyme")==null)?"":request.getParameter("sFechaVencPyme");
	String claveDocumento = (request.getParameter("ic_docto")==null)?"":request.getParameter("ic_docto");

	if(!claveDocumento.equals("")){
		Registros regModifica = new Registros();
		regModifica = BeanParamDscto.getModificacionMontos("", claveDocumento);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		if (regModifica != null){
			jsonObj.put("datosModifica", regModifica.getJSONData() );
		}
		infoRegresar = jsonObj.toString();

	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
	
} else if(informacion.equals("Genera_PDF")){
	String ic_epo              = (request.getParameter("ic_epo")             ==null)? "" :request.getParameter("ic_epo");
	String ic_documento        = (request.getParameter("ic_documento")       ==null)? "" :request.getParameter("ic_documento");
	String cg_pyme_epo_interno = (request.getParameter("cg_pyme_epo_interno")==null)? "" :request.getParameter("cg_pyme_epo_interno").trim();
	String ic_pyme             = (request.getParameter("ic_pyme")            ==null)? "" :request.getParameter("ic_pyme").trim();
	String ic_if               = (request.getParameter("ic_if")              ==null)? "" :request.getParameter("ic_if").trim();
	String ic_estatus_docto    = (request.getParameter("ic_estatus_docto")   ==null)? "" :request.getParameter("ic_estatus_docto").trim();
	String ic_moneda           = (request.getParameter("ic_moneda")          ==null)? "" :request.getParameter("ic_moneda");
	String cc_acuse            = (request.getParameter("cc_acuse")           ==null)? "" :request.getParameter("cc_acuse");
	String no_docto            = (request.getParameter("no_docto")           ==null)? "" :request.getParameter("no_docto");
	String df_fecha_docto_de   = (request.getParameter("df_fecha_docto_de")  ==null)? "" :request.getParameter("df_fecha_docto_de");
	String df_fecha_docto_a    = (request.getParameter("df_fecha_docto_a")   ==null)? "" :request.getParameter("df_fecha_docto_a");
	String fn_monto_de         = (request.getParameter("fn_monto_de")        ==null)? "" :request.getParameter("fn_monto_de");
	String fn_monto_a          = (request.getParameter("fn_monto_a")         ==null)? "" :request.getParameter("fn_monto_a");
	String df_fecha_venc_de    = (request.getParameter("df_fecha_venc_de")   ==null)? "" :request.getParameter("df_fecha_venc_de");
	String df_fecha_venc_a     = (request.getParameter("df_fecha_venc_a")    ==null)? "" :request.getParameter("df_fecha_venc_a");
	String ord_proveedor       = (request.getParameter("ord_proveedor")      ==null)? "" :request.getParameter("ord_proveedor");
	String ord_if              = (request.getParameter("ord_if")             ==null)? "" :request.getParameter("ord_if");
	String ord_monto           = (request.getParameter("ord_monto")          ==null)? "" :request.getParameter("ord_monto");
	String ord_fec_venc        = (request.getParameter("ord_fec_venc")       ==null)? "" :request.getParameter("ord_fec_venc");
	String ord_estatus         = (request.getParameter("ord_estatus")        ==null)? "" :request.getParameter("ord_estatus");
	String camp_dina1          = (request.getParameter("camp_dina1")         ==null)? "" : request.getParameter("camp_dina1");
	String camp_dina2          = (request.getParameter("camp_dina2")         ==null)? "" : request.getParameter("camp_dina2");
	String operaFirmaM         = (request.getParameter("operaFirmaM")        ==null)? "N": request.getParameter("operaFirmaM");
	String sFechaVencPyme      = (request.getParameter("sFechaVencPyme")     ==null)? "" : request.getParameter("sFechaVencPyme");
	String numero_siaff        = (request.getParameter("numero_siaff")       ==null)? "" : request.getParameter("numero_siaff");
	String estatusArchivo      = (request.getParameter("estatusArchivo")     ==null)? "" : request.getParameter("estatusArchivo");
	String banderaTablaDoctos  = (request.getParameter("txtbanderaTablaDoctos") ==null)? "" :request.getParameter("txtbanderaTablaDoctos");
	String cmbFactoraje  = (request.getParameter("cmbFactoraje") ==null)? "" :request.getParameter("cmbFactoraje");
	String validaDoctoEPO  = (request.getParameter("validaDoctoEPO") ==null)? "" :request.getParameter("validaDoctoEPO");
	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 
	String strPerfilCons = (String)request.getSession().getAttribute("sesPerfil");
	String operado= (sesIdiomaUsuario.equals("EN"))?"Operate":"Operado";
	String contrato  = (request.getParameter("contrato") ==null)? "" :request.getParameter("contrato");
	String copade  = (request.getParameter("copade") ==null)? "" :request.getParameter("copade");
	
	
	ConsDoctosDE paginador = new ConsDoctosDE();
	paginador.setCg_pyme_epo_interno(cg_pyme_epo_interno);
	paginador.setIc_pyme(ic_pyme);
	paginador.setIc_if(ic_if);
	paginador.setIc_estatus_docto(ic_estatus_docto);
	paginador.setIc_moneda(ic_moneda);
	paginador.setCc_acuse(cc_acuse);
	paginador.setNo_docto(no_docto);
	paginador.setDf_fecha_docto_de(df_fecha_docto_de);
	paginador.setDf_fecha_docto_a(df_fecha_docto_a);
	paginador.setFn_monto_de(fn_monto_de);
	paginador.setFn_monto_a(fn_monto_a);
	paginador.setDf_fecha_venc_de(df_fecha_venc_de);
	paginador.setDf_fecha_venc_a(df_fecha_venc_a);
	paginador.setOrd_proveedor(ord_proveedor);
	paginador.setOrd_if(ord_if);
	paginador.setOrd_monto(ord_monto);
	paginador.setOrd_fec_venc(ord_fec_venc);
	paginador.setOrd_estatus(ord_estatus);
	paginador.setCamp_dina1(camp_dina1);
	paginador.setCamp_dina2(camp_dina2);
	paginador.setOperaFirmaM(operaFirmaM);
	paginador.setSFechaVencPyme(sFechaVencPyme);
	paginador.setNumero_siaff(numero_siaff);
	paginador.setIc_documento(ic_documento);
	paginador.setIc_epo(ic_epo);
	paginador.setStrAforo(strAforo);
	paginador.setStrAforoDL(strAforoDL);
	paginador.setIdioma(idioma);
	paginador.setSesIdiomaUsuario((sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario);
	paginador.setStrPerfilCons(strPerfilCons);
	paginador.setINoCliente(iNoCliente);
	paginador.setOperado(operado);
	paginador.setBanderaTablaDoctos(banderaTablaDoctos);
	paginador.setNumero_siaff(numero_siaff);
	paginador.setCmbFactoraje(cmbFactoraje);
	paginador.setValidaDoctoEPO(validaDoctoEPO);
	paginador.setContrato(contrato);
	paginador.setCopade(copade);	
	
        
        boolean bOperaFactorajeDistribuido	= false;
        alParamEPO = new Hashtable();
        alParamEPO = BeanParamDscto.getParametrosEPO(iNoCliente, 1);
        if (alParamEPO!=null) {
            bOperaFactorajeDistribuido	=	("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
	}
                
        paginador.setBOperaFactorajeDistribuido(bOperaFactorajeDistribuido);
	
	//CQueryHelperThreadRegExtJS queryHelper = new CQueryHelperThreadRegExtJS(paginador);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	String porcentaje = "";
	String nombreArchivo = "";
	JSONObject jsonObj = new JSONObject();

	try{
		nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF", e);
	}
	infoRegresar = jsonObj.toString();

}
%>
<%=infoRegresar%>