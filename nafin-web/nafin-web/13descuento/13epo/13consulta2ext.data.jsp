<%@ page contentType="application/json;charset=UTF-8" import="   
   java.util.*, java.sql.*,   
	netropology.utilerias.*,   
	com.netro.exception.*,
	net.sf.json.JSONArray,   
	net.sf.json.JSONObject" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/13descuento/13secsession_extjs.jspf"%>
<% 

String idioma                  = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
String fechaAnterior           = (request.getParameter("dc_fecha_tasa")      == null) ?"" : request.getParameter("dc_fecha_tasa");
String informacion             =(request.getParameter("informacion" ) != null) ?     request.getParameter("informacion" ): "";
String informacionG            =(request.getParameter("informacionG") != null) ?     request.getParameter("informacionG"): "";
String informacionH            =(request.getParameter("informacionH") != null) ?     request.getParameter("informacionH"): "";
String operacion               =(request.getParameter("operacion")    != null) ?     request.getParameter("operacion"): "";
String infoRegresar           = "";

com.netro.descuento.ConsTasas paginador = new com.netro.descuento.ConsTasas();
paginador.setINoCliente(iNoCliente);	

if(informacion.equals("valoresIniciales")){
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("strNombre",strNombre);
		infoRegresar = jsonObj.toString();
}
else if (informacionG.equals("Consulta")) {
	paginador.setTipoConsulta("GeneralT");
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String consulta = "";

	 try {
	  Registros reg = queryHelper.doSearch();
	  infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
	 } 
	 catch(Exception e) {
	  throw new AppException("Error en la paginacion", e);
	 }
	}
else if (informacionH.equals("ConsultaHistorico")) {
	paginador.setTipoConsulta("HistoricoT");
	paginador.setFechaAnterior(fechaAnterior);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String consulta = "";

	 try {
	  Registros reg = queryHelper.doSearch();
	  infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
	 } 
	 catch(Exception e) {
	  throw new AppException("Error en la paginacion", e);
	 }
}
else if(informacionH.equals("ConsultaHistoricoGeneral")) {
	paginador.setTipoConsulta("HistoricoGeneralT");
	paginador.setIdioma(idioma);
	paginador.setFechaAnterior(fechaAnterior);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String consulta = "";

	 try {
	  Registros reg = queryHelper.doSearch();
	  infoRegresar = "{\"success\": true, \"total\": " + reg.getNumeroRegistros() + ", \"registros\": " + reg.getJSONData() + "}";
	 } 
	 catch(Exception e) {
	  throw new AppException("Error en la paginacion", e);
	 }
}

%>
<%=infoRegresar%>
