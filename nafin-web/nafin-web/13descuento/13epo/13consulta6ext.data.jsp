<%@ page contentType="application/json;charset=UTF-8" 
	import="   
   java.util.*, java.sql.*,   
	netropology.utilerias.*,   
	com.netro.exception.*,
	com.netro.descuento.*,
	net.sf.json.JSONArray, 
	com.netro.model.catalogos.CatalogoCamposAdicionales,  
	net.sf.json.JSONObject" 
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf"%>
<%@ include file="/13descuento/13secsession_extjs.jspf"%>
<% 

String fechaRegIni            = request.getParameter("dFechaRegIni");         if (fechaRegIni            == null) { fechaRegIni           = ""; }
String fechaRegFin            = request.getParameter("dFechaRegFin");         if (fechaRegFin            == null) { fechaRegFin           = ""; }

String icDocto                = request.getParameter("ic_documento");         if (icDocto                == null) { icDocto               = ""; }

int start                     = 0;
int limit                     = 0;
JSONObject resultado 	      = new JSONObject();

String informacion            =(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion              =(request.getParameter("operacion") != null) ? request.getParameter("operacion") : "";
String infoRegresar           = "";

ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

boolean _bOperaFactConMandato = false;
String ses_ic_epo = iNoCliente;
 
Hashtable alParamEPO1 = new Hashtable(); 
alParamEPO1 = BeanParamDscto.getParametrosEPO(ses_ic_epo,1);	
 
if (alParamEPO1!=null) {
 _bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;		
}

if (informacion.equals("Parametrizacion")) {	//	*-*-*-*-*-*--*-*-*-*-*-*-*-*	Parametrizacion	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	Registros regCamposAdicionales = null;
	String sFechaVencPyme		=	""; 
	JSONObject jsonObj = new JSONObject();
	int totalcd=0;

	Hashtable alParamEPO = new Hashtable();
	boolean bOperaFirmaMancomunada		= false;
   boolean bOperaFactorajeVencido		= false;
	boolean bOperaFactorajeDistribuido	= false;
	boolean bOperaFactorajeNotaDeCredito= false;
	boolean bOperaFactorajeVencInfonavit= false;
	boolean bOperaFactConMandato			= false;
	boolean bTipoFactoraje					= false;
	boolean bMostrarFechaAutorizacionIFInfoDoctos = false; // Fodea 040 - 2011
	
	if(!iNoCliente.equals("")) {
		jsonObj.put("iNoCliente", iNoCliente);
		alParamEPO = BeanParamDscto.getParametrosEPO(iNoCliente, 1);
		if (alParamEPO!=null) {
			bOperaFirmaMancomunada					=	("N".equals(alParamEPO.get("PUB_FIRMA_MANC").toString())) ? false : true ;
			bOperaFactConMandato						=	("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
			bOperaFactorajeVencido					=	("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
			bOperaFactorajeDistribuido				=	("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
			bOperaFactorajeNotaDeCredito			=	("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true; 
			bOperaFactorajeVencInfonavit			=	("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true; //FODEA 042 - Agosto/2009
			bMostrarFechaAutorizacionIFInfoDoctos=	"S".equals((String) alParamEPO.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS") )?true:false; // Fodea 040 - 2011
		}
		bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaDeCredito || bOperaFactConMandato || bOperaFactorajeVencInfonavit)?true:false;
		sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(iNoCliente);
		String banderaTablaDoctos = "";
		if(BeanSeleccionDocumento.bgetExisteParamDocs(iNoCliente)){
			banderaTablaDoctos = "1";
		}
		
		regCamposAdicionales = new Registros();
		regCamposAdicionales = BeanParamDscto.getCamposAdicionales(iNoCliente);
		totalcd = regCamposAdicionales.getNumeroRegistros();
		jsonObj.put("bOperaFirmaMancomunada", new Boolean(bOperaFirmaMancomunada));
		jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));		
		jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
		jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
		jsonObj.put("bOperaFactorajeNotaDeCredito", new Boolean(bOperaFactorajeNotaDeCredito));
		jsonObj.put("bOperaFactorajeVencInfonavit", new Boolean(bOperaFactorajeVencInfonavit));
		jsonObj.put("bMostrarFechaAutorizacionIFInfoDoctos", new Boolean(bMostrarFechaAutorizacionIFInfoDoctos));
		jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
		jsonObj.put("sFechaVencPyme", sFechaVencPyme);
		jsonObj.put("banderaTablaDoctos", banderaTablaDoctos);
		/*java.util.Map paramDescuento = BeanParamDscto.getParametrosModuloDescuento(iNoCliente, "P", iNoEPO);
		jsonObj.put("strAforo", paramDescuento.get("strAforo"));
		jsonObj.put("strAforoDL", paramDescuento.get("strAforoDL"));*/
		jsonObj.put("strAforo", strAforo);
		jsonObj.put("strAforoDL", strAforoDL);	
		
		if (regCamposAdicionales != null){
			jsonObj.put("camp_dina1", regCamposAdicionales.getJSONData() );
			jsonObj.put("totalcd", Integer.toString(totalcd));			
		}
		if(BeanParamDscto.estaHabilitadoNumeroSIAFF(iNoCliente)){
			jsonObj.put("esNumeroSIAFF", "true");
		}
	}

	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}
else if(informacion.equals("CatalogoCamposAdicionales")){

		CatalogoCamposAdicionales cat = new CatalogoCamposAdicionales();
		cat.setCampoClave("cg_campo1");
		cat.setCampoDescripcion("cg_campo1");
		//sgParametrosvePyme(iNoCliente);
		cat.setClaveEpo(iNoCliente);
		cat.setOrden("cg_campo1");
		infoRegresar = cat.getJSONElementos();

}
else if(informacion.equals("valoresIniciales")){
 
 JSONObject jsonObj = new JSONObject();
 jsonObj.put("success",new Boolean(true));
 jsonObj.put("bOperaFactConMandato",new Boolean(_bOperaFactConMandato));
 infoRegresar = jsonObj.toString();
}
else if (informacion.equals("Consulta") || informacion.equals("ConsultaTotales") || informacion.equals("ArchivoCSV") || informacion.equals("ArchivoPDF")) {
   com.netro.descuento.DesPubEpoDET paginador = new com.netro.descuento.DesPubEpoDET();
	paginador.setFechaRegIni(fechaRegIni);
	paginador.setFechaRegFin(fechaRegFin);
	paginador.setINoCliente(iNoCliente);
	paginador.setMandante(_bOperaFactConMandato);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
	String consulta = "";
	
	if(informacion.equals("Consulta")) {
	 try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
				
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);															   		
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
		
				resultado = JSONObject.fromObject(consulta);
				infoRegresar = resultado.toString();
	}
	else if (informacion.equals("ConsultaTotales") ) {
		
		queryHelper = new CQueryHelperRegExtJS(paginador); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
		infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion	
	}
	else if (informacion.equals("ArchivoCSV")) {
	 try {
	  String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"CSV");
	  JSONObject jsonObj = new JSONObject();
	  jsonObj.put("success", new Boolean(true));
	  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	  infoRegresar = jsonObj.toString();
	  } 
	  catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
	  }
	}
	else if (informacion.equals("ArchivoPDF")) {
	 try {
	  String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp,"PDF");
	  JSONObject jsonObj = new JSONObject();
	  jsonObj.put("success", new Boolean(true));
	  jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	  infoRegresar = jsonObj.toString();
	  } 
	  catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
	  }
	}
}
else if (informacion.equals("obtenDetalles")) {
	Registros regDatos = new Registros();
	Registros regDatosDetalle = new Registros();
	Registros regCamposDetalle = null;
	if(!icDocto.equals("") ){
		regDatos = BeanParamDscto.getDatosDocumentos("",icDocto);
		regDatosDetalle = BeanParamDscto.getDatosDocumentosDetalle(icDocto);
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		if (regDatos != null){
			jsonObj.put("datos_doctos", regDatos.getJSONData() );
		}
		if (regDatosDetalle != null){
			jsonObj.put("datos_detalle", regDatosDetalle.getJSONData() );
		}
		regCamposDetalle = new Registros();
		regCamposDetalle = BeanParamDscto.getCamposAdicionalesDetalle(iNoCliente);
		if (regCamposDetalle != null){
			jsonObj.put("dinamicosDetalle", regCamposDetalle.getJSONData() );
		}

		infoRegresar = jsonObj.toString();

	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
}
%>
<%=infoRegresar%>
