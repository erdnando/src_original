Ext.onReady(function() 
{
	var arrDatos ;
	var OrdenMaximo = 0;
	var OrdenLinea = 0;
	var VienenDatos = false;
	//EnBlanco = false;
//--------------------------------- HANDLERS -------------------------------
 
	//llamar en procesar consulta
	var hayDatosOrden = function (){
		var grid = Ext.getCmp('grid');
		var columnModelGrid = grid.getColumnModel();
		var store = grid.getStore();
		
		var datos = false;
		
		store.each(function(record) {	
			numRegistro = store.indexOf(record);
			if(record.data['ORDEN'] != ''){
				datos = true;
			   return ; 
			}
			
		});
		
		return datos;
		
	}


	var procesarOrden = function (valor){
		var grid = Ext.getCmp('grid');
		var columnModelGrid = grid.getColumnModel();
		var store = grid.getStore();
		
		if(valor <= (OrdenLinea + 1)){
			OrdenLinea++;
			return 'ok';
		}
		else{
			//alert('Debia continuar con '+(OrdenLinea+1))
			Ext.MessageBox.alert('Error de validaci�n','Recuerde que debe parametrizar el Orden con n�meros consecutivos comenzando con el 1');
			return 'error'
		}			
	}
	
	
	var validarGuardar = function (){
		var EnBlanco = false;
		var arrOrden= new Array();
		var arrNumeros = new Array();
		
		var grid = Ext.getCmp('grid');
		var columnModelGrid = grid.getColumnModel();
		var store = grid.getStore();
		var errorValidacion = false;
		var hayRepetido = false;
		var numDatos = 0;
		
		var numDatosDuros = 0;
		var guardarBlanco = false;
		var numRegistro = 0;
		
		Ext.getCmp('btnGuardar').disable();
		
		store.each(function(record) {
			numRegistro = store.indexOf(record);
			arrOrden.push(record.data['ORDEN']);			
			numDatos ++;
			
			if (record.data['ORDEN'] == ''){
				EnBlanco = true;
			}
			else{
				var valor = parseInt(record.data['ORDEN']);
				numDatosDuros++;
				arrNumeros.push(valor);
				if(valor > OrdenMaximo){
					errorValidacion = true;
					Ext.MessageBox.alert('Error de validaci�n','La l�nea de cr�dito no puede tener un orden posterior a ' +OrdenMaximo,
						function(){					
							//grid.startEditing(numRegistro, columnModelGrid.findColumnIndex('ORDEN'));
						});
					Ext.getCmp('btnGuardar').enable();
					//alert(numRegistro);
					return ;
				}
			}
		});
		
		if (errorValidacion){
			Ext.getCmp('btnGuardar').enable();
			return ;
		}

		for(i=0;i<numDatos;i++){
			if(arrOrden[i] != ''){
				for(j=i+1;j<numDatos;j++){				
					if(arrOrden[i] == arrOrden[j]  ){
						hayRepetido = true;
						break;
					} 
				}
			}
		}
		
		if(hayRepetido){
			Ext.MessageBox.alert('Error de validaci�n','El orden de las l�neas para seleccionar documentos no se puede repetir, favor de verificar');
			Ext.getCmp('btnGuardar').enable();
			return ;
		}
		
		arrNumeros.sort();
		//alert(arrNumeros);
		var error = false;
		for(j=1;j<=numDatosDuros;j++){
			if(arrNumeros[j-1] != j)
				error = true;
		}
		
		if(error){
			Ext.MessageBox.alert('Error de validaci�n','Recuerde que debe parametrizar el Orden con n�meros consecutivos');
			Ext.getCmp('btnGuardar').enable();
			return ;
		}
		
		var arrDatosIf = new Array();
		var arrDatosModif = new Array();
		var arrDatosEpo = new Array();

		var modifs = 0;
		store.each(function(record) {
			numRegistro = store.indexOf(record);
			if(arrDatos[numRegistro] != record.data['ORDEN']){
				modifs ++;
				arrDatosModif.push(record.data['ORDEN']);
				arrDatosIf.push(record.data['CLAVEIF']);
				arrDatosEpo.push(record.data['CLAVEEPO']);
			}		
		});
	
		if(modifs == 0){
			Ext.MessageBox.alert("Mensaje",'No hay ningun cambio a realizar');
			Ext.getCmp('btnGuardar').enable();
			return;	
		}
		if (EnBlanco){
			guardarBlanco = true;

			Ext.Msg.confirm('Mensaje', 
							'Existen l�neas de cr�dito sin ORDEN asignado. �Desea continuar?',
							function(botonConf) {
								if (botonConf == 'ok' || botonConf == 'yes' || botonConf == 'si' ) {
									Ext.getCmp('btnGuardar').disable();
									
									Ext.Ajax.request({
										url: '13ordenLimiteIfExt.data.jsp',
										params: {
											ArrayIf: arrDatosIf,
											ArrayEpo: arrDatosEpo,
											ArrayOrden: arrDatosModif,
											total: modifs, 
											informacion:'Guardar'
										},	
										callback: procesarGuardar
									});												
								}else{
									Ext.getCmp('btnGuardar').enable();
								}
								return ; 
							}
						);																	
		}
		if(!guardarBlanco){
			Ext.Ajax.request({
				url: '13ordenLimiteIfExt.data.jsp',
				params: {
							ArrayIf: arrDatosIf,
							ArrayEpo: arrDatosEpo,
							ArrayOrden: arrDatosModif,
							total: modifs, 
							informacion:'Guardar'
				},	
				callback: procesarGuardar
			});
		}
	}
	

	var procesarGuardar = function (opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var info = Ext.util.JSON.decode(response.responseText);
			Ext.MessageBox.alert("Mensaje",info.MENSAJE);
			Ext.getCmp('btnGuardar').enable();;		
		}else{
			NE.util.mostrarConnError(response,opts);
		}		
	}	
	
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var el = gridGeneral.getGridEl();	
				
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) {
				gridGeneral.show();
			}
		
		 if(store.getTotalCount() > 0) {				
			var tam = store.getTotalCount();
			OrdenMaximo = store.getTotalCount();
			arrDatos = new Array();
			store.each(function(record) {
				arrDatos.push(record.data['ORDEN']);
			});
		  
		  VienenDatos = hayDatosOrden(); 
		  el.unmask();
		 } else {
			el.mask('No se encontr� ning�n registro', 'x-mask');
		 }
	  }
	}

  
//-------------------------------- STORES -----------------------------------    
  var consultaData = new Ext.data.JsonStore ({
	 root: 'registros',
	 url: '13ordenLimiteIfExt.data.jsp',
	 baseParams: { informacion: 'Consulta' },
	 fields: [
			      {name: 'NOMBRE_IF'},
					{name: 'CLAVEEPO'},
					{name: 'ORDEN'},
					{name: 'LIMITE'},
					{name: 'PORCENTAJE'},
					{name: 'DISPONIBLE'},
					{name: 'ESTATUS'},
					{name: 'MONTO_COMPROMETIDO'},
					{name: 'DISPONIBLE'},
					{name: 'FECHALINEA'},
					{name: 'FECHACAMBIO'},
					{name: 'CSFECHALIMITE'},
					{name: 'CLAVEIF'},					
					{name: 'LIMITE_DL'},
					{name: 'PORCENTAJE_DL'},
					{name: 'DISPONIBLE_DL'},
					{name: 'MONTO_COMPROMETIDO_DL'},
					{name: 'DISPONIBLE_DES'},
					{name: 'DISPONIBLE_DES_DL'}
			   ],
	 totalProperty: 'total',
	 messageProperty: 'msg',
	 autoLoad: false,
	 listeners: {
					  load: procesarConsultaData,
					  exception: {
						  fn: function(proxy,type,action,optionsRequest,response,args){
									 NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
									 procesarConsultaData(null,null,null);
							}
					  }
			      }
  });
  
 //---------------------------------COMPONENTES-------------------------------
  var gridGeneral = new Ext.grid.EditorGridPanel ({
	 store: consultaData,
	 clicksToEdit:1,
	 hidden: false,
	 id: 'grid',
	 columns: [		
			{
				header: '<center>Orden L�nea</center>',
				tooltip: 'Orden L�nea',
				dataIndex: 'ORDEN',
				align: 'center',
				editor: { 
								xtype:'textfield',
								height: 10,
								id:'txtCausa',							
								listeners:{
									blur: function(field){
										//numFocus ++;
										if(field.getValue() != "" && !VienenDatos ){	
											var res = procesarOrden(field.getValue());
											if(res == 'error' )
												field.setValue('');
										}
									}								
								}				
				},
				renderer:function(value,metadata,registro, rowIndex, colIndex){
					metadata.attr='style="border: thin solid #3399CC;  color: black;"';
               return value;
				},
				width: 90
			},
			{
				header: '<center>Intermediario Financiero</center>',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'left',
				hidden: false,
				dataIndex: 'NOMBRE_IF'
			 },
			{
				header: '<center>Monto L�mite <br> Moneda Nacional</center>',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'right',
				hidden: false,
				dataIndex: 'LIMITE',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			 },
			 {
				header: 'Porcentaje de Utilizaci�n <br> Moneda Nacional',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'center',
				hidden: false,
				dataIndex: 'PORCENTAJE',
				renderer: Ext.util.Format.numberRenderer('0.000000 %')
			 },
			 {
				header: '<center>Monto Disponible <br> Moneda Nacional</center>',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'right',
				hidden: false,
				dataIndex: 'DISPONIBLE',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			 },			 
			 {
				header: '<center>Monto L�mite <br>D�lar Americano </center>',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'right',
				hidden: false,
				dataIndex: 'LIMITE_DL',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			 },
			 	 {
				header: 'Porcentaje de Utilizaci�n <br> D�lar Americano',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'center',
				hidden: false,
				dataIndex: 'PORCENTAJE_DL',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			 },
			 {
				header: '<center>Monto Disponible <br> D�lar Americano</center>',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'right',
				hidden: false,
				dataIndex: 'DISPONIBLE_DL',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			 },
			 {
				header: '<center>Estatus</center>',
				sortable: true,
				resizable: true,
				width: 130,	
				align: 'left',
				hidden: false,
				dataIndex: 'ESTATUS'
			 },
			 {
				header: '<center>Monto Comprometido  <br>  Moneda Nacional</center>',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'right',
				hidden: false,
				dataIndex: 'MONTO_COMPROMETIDO',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			 },
			 {
				header: '<center>Monto Disponible despu�s de <br> Comprometido Moneda Nacional</center>',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'right',
				hidden: false,
				dataIndex: 'DISPONIBLE_DES',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			 },
			 {
				header: '<center>Monto Disponible despu�s de <br> Comprometido D�lar Americano</center>',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'right',
				hidden: false,
				dataIndex: 'DISPONIBLE_DES_DL',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			 },
			  {
				header: '<center>Monto Comprometido  <br> D�lar Americano </center>',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'right',
				hidden: false,
				dataIndex: 'MONTO_COMPROMETIDO_DL',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			 },
			 {
				header: 'Fecha Vencimiento L�nea de Cr�dito',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'center',
				hidden: false,
				dataIndex: 'FECHALINEA'
			 },
			 {
				header: 'Fecha Cambio de Administraci�n',
				sortable: true,
				resizable: true,
				width: 200,	
				align: 'center',
				hidden: false,
				dataIndex: 'FECHACAMBIO'
			 },
			 {
				header: 'Validar Fechas L�mite',
				sortable: true,
				resizable: true,
				width: 150,	
				align: 'center',
				hidden: false,
				dataIndex: 'CSFECHALIMITE'
			 }
		],
		stripeRows: true,
		loadMask: true,
		height: 450,
		width: 930,
		title: '',
		frame: true,
		bbar: 
	      {
			   xtype: 'paging',
			   pageSize: 15,
			   buttonAlign: 'left',
			   id: 'barraPaginacion',
			   displayInfo: true,
			   store: consultaData,
			   displayMsg: '{0} - {1} de {2}',
			   emptyMsg: 'No hay registros.',
			   items: [
					     {
							xtype: 'button',
					        text: 'Guardar',
					        id: 'btnGuardar',
							  iconCls: 'icoGuardar',
							  handler: function(boton, evento) 
					        {
								validarGuardar();
							  }
						  }
						]
			 }
  });
  
  
//-------------------------------- PRINCIPAL -----------------------------------
  var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 940,
	  items: 
	    [ 
	     NE.util.getEspaciador(20),
		   gridGeneral
	    ]
  });
  			   
		consultaData.load({ params:{
			operacion: 'Generar', //Generar datos para la consulta
			start: 0,
			limit: 15 }
		 });
});