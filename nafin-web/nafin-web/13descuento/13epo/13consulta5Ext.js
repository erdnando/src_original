Ext.onReady(function() 
{
//--------------------------------- HANDLERS -------------------------------

	var procesarConsultaData = function(store, arrRegistros, opts){
		var fp = Ext.getCmp('forma');
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		var el = gridGeneral.getGridEl();
		fp.el.unmask();
		if (arrRegistros != null){
			if (!gridGeneral.isVisible()){
				gridGeneral.show();
			}
			if(store.getTotalCount() > 0){
				btnGenerarArchivo.enable();
				btnGenerarArchivo.show();
				btnGenerarPDF.enable();
				btnGenerarPDF.show();
				el.unmask();
			} else{
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
/*
 var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) 
 {
	 var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
	 var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
	 
	 btnGenerarArchivo.setIconClass('');
	 
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
		btnBajarArchivo.show();
		btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnBajarArchivo.setHandler(function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
	 } else {
		 btnGenerarArchivo.enable();
		 NE.util.mostrarConnError(response,opts);
   }
  }

   var procesarSuccessFailureGenerarPDF = function (opts, success, response) 
	{
	 var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
	 var btnBajarPDF = Ext.getCmp('btnBajarPDF');
	 var forma = Ext.getDom('formAux');
	 
	 btnGenerarPDF.setIconClass('');
		
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
	  btnBajarPDF.show();
	  btnBajarPDF.el.highlight('FFF700',{duration: 5, easing: 'bounceOut'});
	  btnBajarPDF.setHandler(function (boton, evento) {	
		forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
		forma.submit();
	  });
	 } else {
		btnGenerarPDF.enable();
		NE.util.mostrarConnError(response,opts);
	 }
  }
*/
	var procesarSuccessFailureGenerarArchivo = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}

	var procesarSuccessFailureGenerarPDF = function(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}

	var procesarDetalle = function(grid, rowIndex, colIndex, item, event) {  
	var registro = grid.getStore().getAt(rowIndex);
	claveIF = registro.get('CLAVEIF');
	  
	consultaDataDetalle.load({
				                 params:{
					               operacion: 'Generar', //Generar datos para la consulta
					               start: 0,
					               limit: 15, claveIF : claveIF 
									  } 
									 });
	
	var ventana = Ext.getCmp('ventanaDetalle');
	if (ventana) {
	 ventana.destroy();
	}
   new Ext.Window({
					    //layout: 'fit',
					    modal: true,
					    width: 946,
					    height: 530,
					    id: 'ventanaDetalle',
					    closeAction: 'hide',
					    items: [ 
					            gridDetalle,
									gridDetalleTotales
								  ],
					    title: ''
				      }).show().setTitle('');
  }
  
  var procesarConsultaDetalleData = function(store, arrRegistros, opts) {
		var gridDetalle = Ext.getCmp('gridDetalle');
		var btnBajarArchivoDetalle = Ext.getCmp('btnBajarArchivoDetalle');
		var btnGenerarArchivoDetalle = Ext.getCmp('btnGenerarArchivoDetalle');
		
		gridDetalle.el.unmask();
		if (arrRegistros != null) {
			if (!gridDetalle.isVisible()) { gridDetalle.show(); }
		
		var el = gridDetalle.getGridEl();		
		var store = consultaDataDetalle;
		if(store.getTotalCount() > 0) {	
		  btnGenerarArchivoDetalle.enable();
		  btnGenerarArchivoDetalle.show();
		  
		  btnBajarArchivoDetalle.hide();
  		  el.unmask();
		} else {
		  btnGenerarArchivoDetalle.hide();
		  btnGenerarArchivoDetalle.disable();
		  el.mask('No se encontr� ning�n registro', 'x-mask'); 
		  }
	   }
  }
  
   var totalesDataDetalle = new Ext.data.JsonStore({
			root: 'registros',
			url: '13consulta5Ext.data.jsp',
			baseParams: { informacion: 'ConsultaDetalleTotales' },
			fields: [
			         {name: 'NOMBRE_MONEDA'},
						{name: 'TOTALDOCTOS'},
						{name: 'TOTALMONTODOCTO'},
						{name: 'TOTALMONTODSCTO'},
						{name: 'TOTALIMPORTEINT'},
						{name: 'TOTALIMPORTERECIBIR'}
			        ],
			totalProperty: 'total',
			autoLoad: false,
			listeners: {
						exception: NE.util.mostrarDataProxyError
			}
	});
  
  var procesarSuccessFailureGenerarArchivoDetalle =  function(opts, success, response) 
  {
	 var btnGenerarArchivoDetalle = Ext.getCmp('btnGenerarArchivoDetalle');
	 var btnBajarArchivoDetalle = Ext.getCmp('btnBajarArchivoDetalle');
	 
	 btnGenerarArchivoDetalle.setIconClass('');
	 
	 if (success == true && Ext.util.JSON.decode(response.responseText).success == true) 
	 {
		btnBajarArchivoDetalle.show();
		btnBajarArchivoDetalle.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
		btnBajarArchivoDetalle.setHandler(function(boton, evento) {
			var forma = Ext.getDom('formAux');
			forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
			forma.submit();
		});
	 } else {
		 btnGenerarArchivoDetalle.enable();
		 NE.util.mostrarConnError(response,opts);
   }
  }
  
//-------------------------------- STORES -----------------------------------
  var catologoIntermediarioFinanciero = new Ext.data.JsonStore ({
	 id: 'catologoIntermediarioFinancieroDataStore',
	 root : 'registros',
	 fields : ['clave', 'descripcion', 'loadMsg'],
	 url : '13consulta5Ext.data.jsp',
	 baseParams: { informacion: 'catologoIntermediarioFinanciero' },
	 totalProperty: 'total',
	 autoLoad: false,
	 listeners: {
		exception: NE.util.mostrarDataProxyError,
		beforeload: NE.util.initMensajeCargaCombo
	 }
  });
  
  var consultaData = new Ext.data.JsonStore ({
	 root: 'registros',
	 url: '13consulta5Ext.data.jsp',
	 baseParams: { informacion: 'Consulta' },
	 fields: [
			      {name: 'NOMBRE_IF'},
					{name: 'LIMITE'},
					{name: 'PORCENTAJE'},
					{name: 'DISPONIBLE'},
					{name: 'ESTATUS'},
					{name: 'MONTO_COMPROMETIDO'},
					{name: 'DISPONIBLE'},
					{name: 'FECHALINEA'},
					{name: 'FECHACAMBIO'},
					{name: 'CSFECHALIMITE'},
					{name: 'CLAVEIF'},
					{name: 'LIMITE_DL'},
					{name: 'PORCENTAJE_DL'},
					{name: 'DISPONIBLE_DL'},
					{name: 'MONTO_COMPROMETIDO_DL'},
					{name: 'DISPONIBLE_DES'},
					{name: 'DISPONIBLE_DES_DL'},
					{name: 'DESCRIPCION_ESTATUS'},			
					{name: 'FECHA_BLOQ_DESB'},
					{name: 'USUARIO_BLOQ_DESB'},
					{name: 'DEPENDENCIA_BLOQ_DESB'}					
			     ],
	 totalProperty: 'total',
	 messageProperty: 'msg',
	 autoLoad: false,
	 listeners: {
					     load: procesarConsultaData,
					     exception: {
							  fn: function(proxy,type,action,optionsRequest,response,args){
										 NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
										 procesarConsultaData(null,null,null);
								}
					     }
			        }
  });
  
  var consultaDataDetalle = new Ext.data.JsonStore
  ({
			root: 'registros',
			url: '13consulta5Ext.data.jsp',
			baseParams: { informacion: 'ConsultaDetalle' },
			root : 'registros',
			totalProperty: 'total',
			fields: [
			         {name: 'NOMBREPYME'},
						{name: 'NUMERODOCTO'},
						{name: 'FECHADOCTO'},
						{name: 'FECHAVENC'},
						{name: 'NOMBREMONEDA'},
						{name: 'NOMBRE_TIPO_FACTORAJE'},
						{name: 'MONTODOCTO'},
						{name: 'PORCANTICIPO'},
						{name: 'MONTODSCTO'},
						{name: 'NOMBREIF'},
						{name: 'TASAACEPTADA'},
						{name: 'PLAZO'},
						{name: 'IMPORTEINTERES'},
						{name: 'IMPORTERECIBIR'},
						{name: 'ACUSE'},			
					   {name: 'REFERENCIA'},
						{name: 'IMPORTENETO'},
						{name: 'BENEFICIARIO'},
						{name: 'PORCBENEFICIARIO'},
						{name: 'NOMBRE_MANDANTE'},
						{name: 'FECHASELECCION'},
						{name: 'DISPONIBLE_DES'},
						{name: 'DISPONIBLE_DES_DL'}						
			        ],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
			      load: procesarConsultaDetalleData,
					exception: {
								fn: function(proxy,type,action,optionsRequest,response,args)
								     { 
									   NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args); 
										procesarConsultaData(null,null,null); //Llama procesar consulta, para que desbloquee los componentes.
									  }
								  }
					     }    
  });
  
 	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
			rows: [
				[
					{header: ' ', colspan: 16, align: 'center'},
					{header: '<B>Cambio de Estatus</B>', colspan: 4, align: 'center'}					
				]
			]
	});
	
  var gridGeneral = new Ext.grid.GridPanel ({
	 store: consultaData,
	 hidden: true,
   id: 'grid',
	plugins: grupos,
	 columns: [		
		         {
				      header: 'Intermediario Financiero',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'left',
				      hidden: false,
				      dataIndex: 'NOMBRE_IF'
			       },
			       {
				      header: '<center> Monto L�mite <br>Moneda Nacional',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'right',
				      hidden: false,
				      dataIndex: 'LIMITE',
				      renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },
			       {
				      header: '<center> Porcentaje de Utilizaci�n <br>Moneda Nacional',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'center',
				      hidden: false,
				      dataIndex: 'PORCENTAJE',
						renderer: Ext.util.Format.numberRenderer('0,0.000000 %')
				      //Pendiente
			       },
			       {
				      header: '<center> Monto Disponible <br>Moneda Nacional',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'right',
				      hidden: false,
				      dataIndex: 'DISPONIBLE',
				      renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },
					 
					  {
				      header: '<center> Monto L�mite <br>D�lar Americano',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'right',
				      hidden: false,
				      dataIndex: 'LIMITE_DL',
				      renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },
			       {
				      header: '<center> Porcentaje de Utilizaci�n <br>D�lar Americano ',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'center',
				      hidden: false,
				      dataIndex: 'PORCENTAJE_DL',
						renderer: Ext.util.Format.numberRenderer('0,0.000000 %')
				      //Pendiente
			       },
			       {
				      header: '<center> Monto Disponible <br>D�lar Americano',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'right',
				      hidden: false,
				      dataIndex: 'DISPONIBLE_DL',
				      renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },					 
			       {
				      header: 'Estatus',
				      sortable: true,
				      resizable: true,
				      width: 130,	
				      align: 'left',
				      hidden: false,
				      dataIndex: 'ESTATUS'
			       },
			       {
				      header: 'Monto Comprometido  <br> Moneda Nacional',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'right',
				      hidden: false,
				      dataIndex: 'MONTO_COMPROMETIDO',
				      renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },
					 
					 {
				      header: 'Monto Comprometido <br> D�lar Americano',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'right',
				      hidden: false,
				      dataIndex: 'MONTO_COMPROMETIDO_DL',
				      renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },					
					 
			       {
				      header: 'Monto Disponible despu�s de <br> Comprometido Moneda Nacional',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'right',
				      hidden: false,
				      dataIndex: 'DISPONIBLE_DES',
				      renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },
					  {
				      header: 'Monto Disponible despu�s de <br>Comprometido D�lar Americano',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'right',
				      hidden: false,
				      dataIndex: 'DISPONIBLE_DES_DL',
				      renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },
					 
			       {
				     xtype: 'actioncolumn',
				     header: 'Detalle Doctos Programados',
				     tooltip: 'Detalle Doctos Programados',
				     dataIndex: 'MONTO_COMPROMETIDO',
                 width: 150,
				     align: 'center',
				     items: [{
						        getClass: function(value, metadata, registro, rowIndex, colIndex, store) {								
							      if(registro.get('MONTO_COMPROMETIDO') > 0 && registro.get('MONTO_COMPROMETIDO')!= null) {
							       this.items[0].tooltip = 'Ver';								
							       return 'iconoLupa';	
							      }
						        }
						      ,handler: procesarDetalle
					         }]	
			       },
			       {
				      header: 'Fecha Vencimiento L�nea de Cr�dito',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'center',
				      hidden: false,
				      dataIndex: 'FECHALINEA'
			       },
			       {
				      header: 'Fecha Cambio de Administraci�n',
				      sortable: true,
				      resizable: true,
				      width: 200,	
				      align: 'center',
				      hidden: false,
				      dataIndex: 'FECHACAMBIO'
			       },
			       {
				      header: 'Validar Fechas L�mite',
				      sortable: true,
				      resizable: true,
				      width: 150,	
				      align: 'center',
				      hidden: false,
				      dataIndex: 'CSFECHALIMITE'
			       },
					  {
						header: 'Estatus',
						tooltip: 'Estatus',
						dataIndex: 'DESCRIPCION_ESTATUS',
						sortable: true,
						width: 150,			
						resizable: true,					
						align: 'center'				
					},					 
					 {
						header: 'Fecha de  <br> Bloqueo/Desbloqueo',
						tooltip: 'Fecha de Bloqueo/Desbloqueo',
						dataIndex: 'FECHA_BLOQ_DESB',
						sortable: true,
						width: 150,			
						resizable: true,					
						align: 'center'				
					},
					{
						header: '<center> Usuario <br> de Bloqueo/Desbloqueo ',
						tooltip: 'Usuario de Bloqueo/Desbloqueo ',
						dataIndex: 'USUARIO_BLOQ_DESB',
						sortable: true,
						width: 150,			
						resizable: true,					
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
							metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
							return value;
						}
					},			
					{
						header: '<center> Dependencia que <br> Bloqueo/Desbloqueo',
						tooltip: 'Dependencia que Bloqueo/Desbloqueo ',
						dataIndex: 'DEPENDENCIA_BLOQ_DESB',
						sortable: true,
						width: 150,			
						resizable: true,					
						align: 'left',
						renderer:function(value, metadata, record, rowindex, colindex, store) {
							metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
							return value;
						}
					}			
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		title: '',
		frame: true,
		bbar: 
	      {
			   xtype: 'paging',
			   pageSize: 15,
			   buttonAlign: 'left',
			   id: 'barraPaginacion',
			   displayInfo: true,
			   store: consultaData,
			   displayMsg: '{0} - {1} de {2}',
			   emptyMsg: 'No hay registros.',
			   items: [ '->','-',{
					xtype:   'button',
					text:    'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id:      'btnGenerarArchivo',
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url:'13consulta5Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{ informacion: 'ArchivoCSV'}),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},'-',{
					xtype:   'button',
					text:    'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id:      'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
						url: '13consulta5Ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{ informacion: 'ArchivoPDF'}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				}]
			}
	});

	var gridDetalle = {
		xtype: 'grid',
		store: consultaDataDetalle,
		id: 'gridDetalle',
		closable: true,
		hidden: true,
		stateful: true,
		columns: [		
			       {
				     header: 'Nombre Pyme',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'left',
				     hidden: false,
				     dataIndex: 'NOMBREPYME'
			       },
					 {
				     header: 'N�mero de Documento',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'left',
				     hidden: false,
				     dataIndex: 'NUMERODOCTO'
			       },
					 {
				     header: 'Fecha de Emisi�n',
				     sortable: true,
				     resizable: true,
				     width: 150,	
				     align: 'center',
				     hidden: false,
				     dataIndex: 'FECHADOCTO'
			       },
					 {
				     header: 'Fecha de Vencimiento',
				     sortable: true,
				     resizable: true,
				     width: 150,	
				     align: 'center',
				     hidden: false,
				     dataIndex: 'FECHAVENC'
			       },
					 {
				     header: 'Moneda',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'left',
				     hidden: false,
				     dataIndex: 'NOMBREMONEDA'
			       },
					 {
				     header: 'Tipo de Factoraje',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'left',
				     hidden: false,
				     dataIndex: 'NOMBRE_TIPO_FACTORAJE'
			       },
					 {
				     header: 'Monto de Documento',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'right',
				     hidden: false,
				     dataIndex: 'MONTODOCTO',	  
					  renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },
					 {
				     header: 'Porcentaje de Descuento',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'center',
				     hidden: false,
				     dataIndex: 'PORCANTICIPO',
					  renderer: Ext.util.Format.numberRenderer('0,0%')
			       },
					 {
				     header: 'Monto a Descontar',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'right',
				     hidden: false,
				     dataIndex: 'MONTODSCTO',
					  renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },
					 {
				     header: 'Intermediario Financiero',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'left',
				     hidden: false,
				     dataIndex: 'NOMBREIF'
			       },
					 {
				     header: 'Tasa',
				     sortable: true,
				     resizable: true,
				     width: 130,	
				     align: 'left',
				     hidden: false,
				     dataIndex: 'TASAACEPTADA'
			       },
					 {
				     header: 'Plazo D�as',
				     sortable: true,
				     resizable: true,
				     width: 150,	
				     align: 'left',
				     hidden: false,
				     dataIndex: 'PLAZO'
			       },
					 {
				     header: 'Monto Inter�s',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'right',
				     hidden: false,
				     dataIndex: 'IMPORTEINTERES',
					  renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },
					 {
				     header: 'Importe a Recibir',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: ' right',
				     hidden: false,
				     dataIndex: 'IMPORTERECIBIR',
					  renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },
					 {
				     header: 'N�mero de Acuse',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'left',
				     hidden: false,
				     dataIndex: 'ACUSE'
			       },
					 {
				     header: 'Referencia',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'left',
				     hidden: false,
				     dataIndex: 'REFERENCIA'
			       },
					 {
				     header: 'Neto a Recibir',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: ' right',
				     hidden: false,
				     dataIndex: 'IMPORTENETO',
					  renderer: Ext.util.Format.numberRenderer('$0,0.00')
			       },
					 {
				     header: 'Beneficiario',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'left',
				     hidden: false,
				     dataIndex: 'BENEFICIARIO'
			       },
					 {
				     header: 'Porcentaje del Beneficiario',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'center',
				     hidden: false,
				     dataIndex: 'PORCBENEFICIARIO',
					  renderer: Ext.util.Format.numberRenderer('0,0.00%')
			       },
					 {
				     header: 'Mandatario',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'left',
				     hidden: false,
				     dataIndex: 'NOMBRE_MANDANTE'
			       },
					 {
				     header: 'Fecha del Registro de la operaci�n',
				     sortable: true,
				     resizable: true,
				     width: 200,	
				     align: 'center',
				     hidden: false,
				     dataIndex: 'FECHASELECCION'
			       } 
		        ],	
		stripeRows: true,
	   loadMask: true,
		frame: true,
		width: 943,
		height: 400,
		title: 'Documentos Programados Pyme',
		bbar: 
	      {
			 xtype: 'paging',
			 pageSize: 15,
			 buttonAlign: 'left',
			 id: 'barraPaginacionD',
			 displayInfo: true,
			 store: consultaDataDetalle,
			 displayMsg: '{0} - {1} de {2}',
			 emptyMsg: 'No hay registros.',
			 items: [  
					     '->',
			           '-',
							{
								xtype: 'button',
								text: 'Totales',
								id: 'btnTotalesD',
								hidden: false,
								handler: function (boton,evento) {
											totalesDataDetalle.load({ params: Ext.apply(fp.getForm().getValues()) });
											var gridDetalleTotales = Ext.getCmp('gridDetalleTotales');
											if(!gridDetalleTotales.isVisible()){
													gridDetalleTotales.show();
													gridDetalleTotales.el.dom.scrollIntoView();
											};
								}
							},
					     '-',
			           {
					        xtype: 'button',
					        text: 'Generar Archivo',
					        id: 'btnGenerarArchivoDetalle',
					        handler: function(boton, evento) 
					        {
					         boton.disable();
						      boton.setIconClass('loading-indicator');
						      Ext.Ajax.request({
						       url:'13consulta5Ext.data.jsp',
						       params: Ext.apply(fp.getForm().getValues(),{ informacion: 'ArchivoDetalleCSV'}),
						       callback: procesarSuccessFailureGenerarArchivoDetalle
						      });
					        }
				         },
					      {
						     xtype: 'button',
						     text: 'Bajar Archivo',
						     id: 'btnBajarArchivoDetalle',
						     hidden: true
					      },
							'-'
			        ]
			}
  };
  
   var gridDetalleTotales = {
	xtype: 'grid',
	store: totalesDataDetalle,
	id: 'gridDetalleTotales',
	style: ' margin:0 auto;',
	hidden: true,
	columns: [
	          {
					header: 'Moneda',
					dataIndex: 'NOMBRE_MONEDA',
					align: 'center',
					width: 150
				 },
			    {
					header: 'Total de Documentos',
					dataIndex: 'TOTALDOCTOS',
					align: 'right',
					width: 150,
					renderer: Ext.util.Format.numberRenderer('0,000')
				 },
				 {
					header: 'Total Monto de Documento',
					dataIndex: 'TOTALMONTODOCTO',
					align: 'right',
					width: 150,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				 },
				 {
					header: 'Total a Descontar',
					dataIndex: 'TOTALMONTODSCTO',
					align: 'right',
					width: 150,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				 },
				 {
					header: 'Total de Interes',
					dataIndex: 'TOTALIMPORTEINT',
					align: 'right',
					width: 150,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				 },
				  {
					header: 'Importe a Recibir',
					dataIndex: 'TOTALIMPORTERECIBIR',
					align: 'right',
					width: 150,
					renderer: Ext.util.Format.numberRenderer('$0,0.00')
				 }			 
				],
		width: 933,
		height: 100,
		title: 'Totales',
		tools: [
					{
						id: 'close',
						handler: function(evento, toolEl, panel, tc){
						var gGridDetalleTotales = Ext.getCmp('gridDetalleTotales');
			         gGridDetalleTotales.hide();
						}
					}
		],
		frame: false
  };
	
//---------------------------------COMPONENTES-------------------------------
  var elementosForma = 
  [
	 {
	  //Seleccionar Intermediario Financiero
	  xtype: 'combo',
	  fieldLabel: 'Seleccionar Intermediario Financiero',
	  emptyText: 'Seleccionar Todos',
	  displayField: 'descripcion',
	  valueField: 'clave',
	  triggerAction: 'all',
	  forceSelection : true,	
	  typeAhead: true,
	  minChars: 1,
	  store: catologoIntermediarioFinanciero,
	  tpl: NE.util.templateMensajeCargaCombo,
	  name:'cIntermediarioFinanciero',
	  Id: 'intermediariFinanciero',
	  mode: 'local',
	  hiddenName: 'cIntermediarioFinanciero'
	 }
	];
  
  var fp = new Ext.form.FormPanel
  ({
		hidden: false,
		height: 'auto',
		width: 600,
		labelWidth: 130,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		frame: true,
		id: 'forma',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		monitorValid: true,
		items: elementosForma,
		buttons:
		  [
			 {
			  //Bot�n BUSCAR
			  xtype: 'button',
			  text: 'Buscar',
			  name: 'btnBuscar',
			  iconCls: 'icoBuscar',
			  hidden: false,
			  formBind: true,
			  handler: function(boton, evento) 
			  {
			   consultaData.load({ params: Ext.apply(fp.getForm().getValues(),{
					operacion: 'Generar', //Generar datos para la consulta
					start: 0,
					limit: 15})
				 });
			  }
			 },
			 {
			  //Bot�n Limpiar
			  xtype: 'button',
			  text: 'Limpiar',
			  name: 'btnLimpiar',
			  hidden: false,
			  iconCls: 'icoLimpiar',
			  handler: function(boton, evento) 
			  {
			   window.location = '13consulta5Ext.jsp'
			  }
			 }
		  ]
  });


//-------------------------------- PRINCIPAL -----------------------------------
  var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 949,
	  items: 
	    [ 
	     fp,
	     NE.util.getEspaciador(20),
		   gridGeneral
	    ]
  });
  
  catologoIntermediarioFinanciero.load();

});