<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		 java.math.*,
		java.io.File,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
String cveEstatus = (request.getParameter("cveEstatus") != null)?request.getParameter("cveEstatus"):"";
String infoRegresar = "";
String claveEPO = iNoEPO;
	
AccesoDB con = new AccesoDB();
try{
	con.conexionDB();

	boolean bOperaFactorajeVencido = false;
	boolean bOperaFactConMandato = false;
	boolean bOperaFactVencimientoInfonavit = false;
	String sFechaVencPyme="";
	
	// Verifica si la epo acepta "Factoraje Vencido"	
	String qrySentencia=
			" SELECT COUNT (1)"   +
			"   FROM comrel_producto_epo"   +
			"  WHERE ic_producto_nafin = 1"   +
			"    AND cs_factoraje_vencido = 'S'"   +
			"    AND ic_epo = ?"  ;
	PreparedStatement ps = con.queryPrecompilado(qrySentencia);
	ps.setString(1,iNoCliente);
	ResultSet rs = ps.executeQuery();
	bOperaFactorajeVencido = false;
	if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeVencido = true; }
	ps.close();

	ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

	sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(iNoCliente);
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
	Hashtable alParamEPO1 = new Hashtable(); 
	alParamEPO1 = BeanParamDscto.getParametrosEPO(claveEPO,1);	
	if (alParamEPO1!=null) {
		 bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		 bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;
		}
	String firmaMancomunada= BeanParamDscto.getOperaFirmaMancomunada(iNoCliente, 1);
	String operaFactConMandato =  "N";
	String operaFactVencimientoInfonavit = "N";
	if(bOperaFactConMandato==true){  operaFactConMandato =  "S"; }
	if(bOperaFactVencimientoInfonavit==true){  operaFactVencimientoInfonavit =  "S"; }
	
	
	int i = 0 ,ic_moneda = 0;
	double fn_monto = 0,  dblPorcentaje = 0, df_total_mn = 0, df_total_dolares = 0, MontoAnterior2 = 0.0,
				 MontoNuevo = 0.0,  montoDescuento = 0.0, total_MontoAnterior_mn=0.0, total_MontoNuevo_mn=0.0,
				 total_MontoAnterior_dolares=0.0, total_MontoNuevo_dolares=0.0, total_Monto_descontar_mn=0.0,
				 total_Monto_descontar_dolares =0.0, df_total_ant_mn = 0.0, df_total_ant_dol = 0.0;
	
	String cg_razon_social = "", ig_numero_docto = "", cc_acuse = "", df_fecha_docto = "", df_fecha_venc = "",
				df_fecha_venc_pyme = "", cd_nombre = "", beneficiario   = "", por_beneficiario  = "", importe_a_recibir = "",
				mandante = "", cambioEstatus ="", tipoFactoraje = "", tipoFactorajeDesc = "", fechaVencAnt ="",
				fechaVencAnt_pyme ="",  montoAnterior ="", causa ="";	

	boolean dolaresDocto = false;
	boolean nacionalDocto = false;
	BigDecimal porcentaje=new BigDecimal(strAforo); 
	BigDecimal porcentajeDL=new BigDecimal(strAforoDL); 
	BigDecimal mtodoc=null;
	BigDecimal mtodesc=new BigDecimal("0.0");
	BigDecimal nac_desc=new BigDecimal("0.0"); 
	BigDecimal dol_desc=new BigDecimal("0.0");	
	BigDecimal mnMontoModif=new BigDecimal("0.0");
	BigDecimal dolMontoModif=new BigDecimal("0.0");
	
	
	HashMap datos = new HashMap();
	JSONArray registros = new JSONArray();
	JSONObject 	resultado	= new JSONObject(); 
	HashMap registrosTot = new HashMap();	
	JSONArray registrosTotales = new JSONArray();

	if(informacion.equals("Consultar") || informacion.equals("ArchivoPDF")  || informacion.equals("ResumenTotales")){
			
		RepCEstatusEpoDEbean paginador = new RepCEstatusEpoDEbean();
		paginador.setiNoCambioEstatus(Integer.parseInt(cveEstatus));
		paginador.setClaveEPO(claveEPO);		
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS( paginador);
	
		Registros reg	=	queryHelper.doSearch();		
		
		while (reg.next()) {
			cg_razon_social = reg.getString(1);
			ig_numero_docto = reg.getString(2);
			cc_acuse = (reg.getString(3)==null)?"":reg.getString(3);
			df_fecha_docto = reg.getString(4);
			df_fecha_venc = (reg.getString(5)==null)?"":reg.getString(5);
			df_fecha_venc_pyme = reg.getString("df_fecha_venc_pyme");
			cd_nombre = reg.getString("cd_nombre"); 
			fn_monto = Double.parseDouble((String)reg.getString(7));
			ic_moneda = Integer.parseInt(reg.getString("ic_moneda"));
				
			if(cveEstatus.equals("5") ||  cveEstatus.equals("4") ||  cveEstatus.equals("6") ||  cveEstatus.equals("8")  ||  cveEstatus.equals("40")  ) {
				beneficiario      = reg.getString("beneficiario");
				por_beneficiario  = reg.getString("por_beneficiario");
				importe_a_recibir = reg.getString("importe_a_recibir");
			}
			if(cveEstatus.equals("8")) {
				fechaVencAnt = (reg.getString("fechaVencAnt")==null)?"":reg.getString("fechaVencAnt");
				fechaVencAnt_pyme = (reg.getString("df_fecha_venc_p_anterior")==null)?"":reg.getString("df_fecha_venc_p_anterior");
				montoAnterior = (reg.getString("fn_monto_anterior")==null)?"":reg.getString("fn_monto_anterior").trim();
				causa = reg.getString(10).trim();
			}
			if(cveEstatus.equals("5") ||  cveEstatus.equals("4") ||  cveEstatus.equals("6") ) {
				cambioEstatus = reg.getString("cambioEstatus");
			}
			if(cveEstatus.equals("5") ||  cveEstatus.equals("6")  ||  cveEstatus.equals("29") ) {
				if(ic_moneda==1)
					dblPorcentaje = new Double(strAforo).doubleValue() * 100;
				else if(ic_moneda==54)
					dblPorcentaje = new Double(strAforoDL).doubleValue() * 100;
					mtodoc=new BigDecimal(reg.getString(7));
				if(ic_moneda==1)
					mtodesc=mtodoc.multiply(porcentaje);
				else if(ic_moneda==54)
					mtodesc=mtodoc.multiply(porcentajeDL);						
			}	
				
			if(cveEstatus.equals("4") ||  cveEstatus.equals("40") ) {
				dblPorcentaje = Double.parseDouble((String)reg.getString(("fn_porc_anticipo")));
				mtodoc=new BigDecimal(reg.getString(7));
				mtodesc=mtodoc.multiply(porcentaje);
			}
				
			if(cveEstatus.equals("28")) {
				mandante=  (reg.getString("mandante")==null)?"":reg.getString("mandante");
				montoAnterior = (reg.getString("fn_monto_anterior")==null)?"":reg.getString("fn_monto_anterior").trim();
				MontoAnterior2 = Double.parseDouble(montoAnterior); 
				MontoNuevo = Double.parseDouble((String)reg.getString(("fn_monto_nuevo"))); 
				causa = (reg.getString("ct_cambio_motivo")==null)?"":reg.getString("ct_cambio_motivo").trim();
				tipoFactoraje= reg.getString("CS_DSCTO_ESPECIAL");
				tipoFactorajeDesc = (reg.getString("TIPO_FACTORAJE")==null)?"":reg.getString("TIPO_FACTORAJE");
					
				if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("C") || tipoFactoraje.equals("I"))  { //para factoraje vencido y distribuido
					dblPorcentaje =  100;
				}
				montoDescuento =  (MontoNuevo*dblPorcentaje)/100;						
			}
				
			if(bOperaFactorajeVencido || bOperaFactVencimientoInfonavit) { // Para Factoraje Vencido o Vencimiento Infonavit
				tipoFactoraje = (reg.getString("CS_DSCTO_ESPECIAL")==null)?"":reg.getString("CS_DSCTO_ESPECIAL");
				if(tipoFactoraje.equals("V") || tipoFactoraje.equals("I"))  {
					dblPorcentaje =  100;
					mtodesc=new BigDecimal (fn_monto);
				}
			}	
			
			// para los totales
			if(cveEstatus.equals("5")  ||  cveEstatus.equals("4") ||  cveEstatus.equals("6")  ||  cveEstatus.equals("29") ||  cveEstatus.equals("40")) {
				if (ic_moneda == 1) {
					df_total_mn+=fn_monto;
					nac_desc = nac_desc.add(mtodesc);
					nacionalDocto = true;
				}
				if (ic_moneda == 54) {
					df_total_dolares+=fn_monto;
					dol_desc = dol_desc.add(mtodesc);
					dolaresDocto = true;
				}							
			}
			if(cveEstatus.equals("8")  ) {
				if (ic_moneda == 1) {
					df_total_mn+=fn_monto;
					mnMontoModif = mnMontoModif.add(mtodesc);
					nacionalDocto = true;
					df_total_ant_mn += Double.parseDouble((String)reg.getString(9)); 
				}
				if (ic_moneda == 54) {
					df_total_dolares+=fn_monto;
					dolMontoModif = dolMontoModif.add(mtodesc);
					dolaresDocto = true;
					df_total_ant_dol +=  Double.parseDouble((String)reg.getString(9)); 
				}
			}
		
			if(cveEstatus.equals("28")  ) {
				if (ic_moneda == 1) {					
					total_MontoAnterior_mn +=MontoAnterior2;
					total_MontoNuevo_mn +=MontoNuevo;
					total_Monto_descontar_mn += montoDescuento;
					nacionalDocto = true;					
				}
				if (ic_moneda == 54) {					
					total_MontoAnterior_dolares +=MontoAnterior2;
					total_MontoNuevo_dolares +=MontoNuevo;
					total_Monto_descontar_dolares += montoDescuento;
					dolaresDocto = true;				
				}		
			}
			
			datos = new HashMap();
			datos.put("NOMBRE_PROVEEDOR", cg_razon_social.trim());
			datos.put("NO_DOCUMENTO", ig_numero_docto.trim());
			datos.put("FECHA_EMISION", df_fecha_docto.trim());
			datos.put("FECHA_VENC",  df_fecha_venc.trim());
			datos.put("FECHA_VENC_PROVEDOR",df_fecha_venc_pyme.trim());
			datos.put("MONEDA", cd_nombre.trim());
			datos.put("MONTO", Double.toString (fn_monto));
			datos.put("PORC_DESCUENTO", Double.toString (dblPorcentaje) );
			if(!cveEstatus.equals("28")) {
			datos.put("MONTO_DESCUENTO", mtodesc.toPlainString().trim());
			}else	if(cveEstatus.equals("28")) {
			datos.put("MONTO_DESCUENTO", Double.toString (montoDescuento));
			}			
			datos.put("NUMERO_ACUSE", cc_acuse.trim());
			datos.put("BENEFICIARIO", beneficiario);
			datos.put("PORC_BENEFICIARIO", por_beneficiario);
			datos.put("IMPORTE_RECIBIR", importe_a_recibir);
			datos.put("CAMBIO_ESTATUS", cambioEstatus);				
			datos.put("FECHA_VENC_ANTERIOR", fechaVencAnt);	
			datos.put("FECHA_VENC_PROV_ANTERIOR", fechaVencAnt_pyme);	
			datos.put("MONTO_ANTERIOR", montoAnterior);
			datos.put("CAUSA", causa);		
			datos.put("MANDATORIO", mandante);
			datos.put("MONTO_NUEVO", Double.toString (MontoNuevo) );
			datos.put("TIPO_FACTORAJE", tipoFactorajeDesc);			
			registros.add(datos);	
		}// while
		
		if(cveEstatus.equals("5")  ||  cveEstatus.equals("4") ||  cveEstatus.equals("6")  ||  cveEstatus.equals("29") ||  cveEstatus.equals("40")) {
			for(int t =0; t<2; t++) {		
				registrosTot = new HashMap();		
				if(t==0){ 				
					registrosTot.put("MONEDA", "Total Moneda Nacional");
					registrosTot.put("ICMONEDA", "1");		
					registrosTot.put("TOTAL_MONTO", Double.toString(df_total_mn));					
					registrosTot.put("MONTO_DESCONTAR", nac_desc );		
				}
				if(t==1){ 			
					registrosTot.put("MONEDA", "Total Dólar");
					registrosTot.put("ICMONEDA", "54");
					registrosTot.put("TOTAL_MONTO", Double.toString(df_total_dolares)  );			
					registrosTot.put("MONTO_DESCONTAR",dol_desc );				
				}
				registrosTotales.add(registrosTot);		
			}
		}
	
		if(cveEstatus.equals("8") ) {
			for(int t =0; t<2; t++) {		
				registrosTot = new HashMap();		
				if(t==0){ 				
					registrosTot.put("MONEDA", "Total Moneda Nacional");
					registrosTot.put("ICMONEDA", "1");		
					registrosTot.put("TOTAL_MONTO", Double.toString(df_total_mn));					
					registrosTot.put("MONTO_ANTERIOR", Double.toString (df_total_ant_mn) );	
					registrosTot.put("MONTO_DESCONTAR", mnMontoModif);
				}
				if(t==1){ 			
					registrosTot.put("MONEDA", "Total Dólar");
					registrosTot.put("ICMONEDA", "54");
					registrosTot.put("TOTAL_MONTO", Double.toString(df_total_dolares)  );
					registrosTot.put("MONTO_ANTERIOR", Double.toString (df_total_ant_dol) );	
					registrosTot.put("MONTO_DESCONTAR",dolMontoModif );					
				}
				registrosTotales.add(registrosTot);		
			}
		}
	
		if (informacion.equals("Consultar")) {	
			String consulta2 =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
			resultado = JSONObject.fromObject(consulta2);
			resultado.put("sFechaVencPyme",sFechaVencPyme);
			resultado.put("firmaMancomunada",firmaMancomunada);
			resultado.put("operaFactConMandato",operaFactConMandato);
			infoRegresar	=	resultado.toString();	
	
		} else if (informacion.equals("ResumenTotales")) {
		
			infoRegresar =  "{\"success\": true, \"total\": \"" + registrosTotales.size() + "\", \"registros\": " + registrosTotales.toString()+"}";
		
		} else if (informacion.equals("ArchivoPDF")) {
			try {
			
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");			
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();
					
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}				
		}
		
	}
}catch(Exception e){ 
	e.printStackTrace();
	out.print(e); 
} finally { if(con.hayConexionAbierta()) con.cierraConexionDB(); }
%>

<%=infoRegresar%>
	