Ext.onReady(function(){
 
	var strSerial =  Ext.getDom('strSerial').value;	
	
	function procesarSuccessFailureContinuar(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonObj = Ext.util.JSON.decode(response.responseText);
			
			window.location = jsonObj.urlPagina;
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) 	{		
		if (arrRegistros != null) {
			if (!gridConsulta.isVisible()) {
				gridConsulta.show();
			}	
		}		
		var el = gridConsulta.getGridEl();	
		var jsonData = store.reader.jsonData;	
		if(store.getTotalCount() > 0) {	
			gridConsulta.setTitle(jsonData.mensaje);
			el.unmask();			
		} else {				
			el.mask('No se encontr� ning�n registro', 'x-mask');				
		}	
	}
	
	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13pymeSinNumProv_popExt.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		fields: [
			{name: 'NUM_ELECTRONICO'},
			{name: 'FECHA_AFILIACION'},			
			{name: 'RFC'},
			{name: 'NOMBRE_PROVEEDOR'}						
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,			
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);						
				}
			}
		}
	});	
	
	var gridConsulta = new  Ext.grid.GridPanel({		
		title: 'Pymes sin n�mero de Proveedor',		
		store: consultaData,
		columns: [		
			{							
				header : 'N�mero Electr�nico',
				tooltip: 'N�mero Electr�nico',
				dataIndex : 'NUM_ELECTRONICO',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true	
			},
			{							
				header : 'Fecha Afiliaci�n',
				tooltip: 'Fecha Afiliaci�n',
				dataIndex : 'FECHA_AFILIACION',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true	
			},			
			{							
				header : 'R.F.C',
				tooltip: 'R.F.C',
				dataIndex : 'RFC',
				width : 150,
				align: 'center',
				sortable : true,
				resizable: true	
			},
			{							
				header : 'Nombre',
				tooltip: 'Nombre',
				dataIndex : 'NOMBRE_PROVEEDOR',
				width : 250,
				align: 'left',
				sortable : true,
				resizable: true	
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 700,			
		frame: false,
		bbar: [				
			'->',
			'-',				
			{
				xtype: 'button',
				id: 'btnSalir',
				text: 'Continuar',
				iconCls: 'icoContinuar',
				handler: function(boton, evento) {
					Ext.Ajax.request({
						url: '13pymeSinNumProv_popExt.data.jsp',
						params: {
							informacion: "Continuar"
						},
						callback: procesarSuccessFailureContinuar
					});
				}
			},
			{
				xtype: 'button',
				id: 'btnCapturar',
				text: 'Capturar',
				iconCls: 'icoAceptar',
				handler: function(boton, evento) {
				if(strSerial=='') {
						window.location = '/nafin/20secure/20generarCertificadoExt.jsp';
					}else  {
						window.location = '/nafin/15cadenas/15pki/15clientes/15consSinNumProvExt.jsp?emergente=S';
					}
				}
			}
		]		
	});
	
	
//-----------------------------------PRINCIPAL----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,	
		style: 'margin:0 auto;',
		items: [
			gridConsulta
		]
	});
	
	
	consultaData.load();
					
});