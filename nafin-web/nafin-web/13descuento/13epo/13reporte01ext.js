Ext.onReady(function(){
	Ext.QuickTips.init();
	var jsonValoresIniciales = null;
	var estatus = "";
	var montoDescontarMN = 0;
	var montoDescontarUSD = 0;
//--------------------------------------HANDLERS--------------------------------

	function procesaValoresIniciales(opts, success, response){
		fp.el.mask();
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if(jsonValoresIniciales != null){
				var lblUsuario = Ext.getCmp('lblUsuario');
				var lblFecha = Ext.getCmp('lblFecha');
				lblUsuario.getEl().update(jsonValoresIniciales.strNombreUsuario);
				lblFecha.getEl().update(jsonValoresIniciales.fechaHora);
				fp.el.unmask();
			}else{
				NE.util.mostrarConnError(response,opts);
			}
		}
	}
	var procesarSuccessFailureGenerarPDFNegociable =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFNegociable');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFNegociable');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPDFSeleccionada =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFSeleccionada');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFSeleccionada');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPDFOperada =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFOperada');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFOperada');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPDFOperadaPagada =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFOperadaPagada');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFOperadaPagada');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarConsultaGridNegociable = function(store,arrRegistros,opts){
		fp.el.unmask();
		var gridNegociable = Ext.getCmp('gridNegociable');
		if(arrRegistros != null){
			if(!gridNegociable.isVisible()){
				gridNegociable.show();
			}
			var btnGenerarPDFNegociable = Ext.getCmp('btnGenerarPDFNegociable');
			var btnBajarPDFOperadaPagada = Ext.getCmp('btnBajarPDFOperadaPagada');
			var btnBajarPDFOperada = Ext.getCmp('btnBajarPDFOperada');
			var btnBajarPDFSeleccionada = Ext.getCmp('btnBajarPDFSeleccionada');
			var btnBajarPDFNegociable = Ext.getCmp('btnBajarPDFNegociable');
			
			btnBajarPDFOperadaPagada.hide();
			btnBajarPDFOperada.hide();
			btnBajarPDFSeleccionada.hide();
			btnBajarPDFNegociable.hide();
			
			var el = gridNegociable.getGridEl();
			if(store.getTotalCount()>0){
				btnGenerarPDFNegociable.enable();
				btnBajarPDFNegociable.hide();
				//Calcular el valor del Monto Total Descontar
				montoDescontarMN = 0;
				montoDescontarUSD = 0;
				store.each(function(registro){
					var icMoneda = registro.get('IC_MONEDA');
					if(icMoneda == 1){
						montoDescontarMN += registro.get('FN_MONTO_DSCTO');
					}else{
						montoDescontarUSD += registro.get('FN_MONTO_DSCTO');
					}
				});				
				var cm = gridNegociable.getColumnModel();
				cm.setHidden(cm.findColumnIndex('FECHA_VENC_PYME'),true);
				cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'),true);
				cm.setHidden(cm.findColumnIndex('NOMBREIF'),true);
				cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('MANDANTE'),true);
				
				if(jsonValoresIniciales.sFechaVencPyme!=""){
					cm.setHidden(cm.findColumnIndex('FECHA_VENC_PYME'),false);
				}
				if(jsonValoresIniciales.bTipoFactoraje == true){/* Desplegar solo si opera "Factoraje Vencido" o "Factoraje Distribuido" */
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'),false);
				}
				if(jsonValoresIniciales.bOperaFactorajeVencido == true || 
					jsonValoresIniciales.bOperaFactConMandato == true || 
					jsonValoresIniciales.bOperaFactVencimientoInfonavit == true) { /* Desplegar solo si opera "Factoraje Vencido" */
					cm.setHidden(cm.findColumnIndex('NOMBREIF'),false);
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido == true || 
					jsonValoresIniciales.bOperaFactVencimientoInfonavit == true) { /* Desplegar solo si opera "Factoraje Distribuido" */
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'),false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'),false);
					cm.setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'),false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato == true) {
					cm.setHidden(cm.findColumnIndex('MANDANTE'),false);					
				}
				el.unmask();
			}else{
				btnGenerarPDFNegociable.disable();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	var procesarConsultaGridProgSel = function(store,arrRegistros,opts){
		fp.el.unmask();
		var gridAutProgSel = Ext.getCmp('gridAutProgSel');
		var el = gridAutProgSel.getGridEl();
		if(estatus == 3){
			gridAutProgSel.setTitle('Estatus Seleccionada Pyme');
		}else if(estatus == 24){
			gridAutProgSel.setTitle('Estatus En Proceso de Autorizacion IF');
		}else if(estatus == 26){
			gridAutProgSel.setTitle('Estatus Programado PyME');
		}
		if(arrRegistros != null){
			if(!gridAutProgSel.isVisible()){
				gridAutProgSel.show();
			}
			var btnGenerarPDFSeleccionada = Ext.getCmp('btnGenerarPDFSeleccionada');
			var btnBajarPDFOperadaPagada = Ext.getCmp('btnBajarPDFOperadaPagada');
			var btnBajarPDFOperada = Ext.getCmp('btnBajarPDFOperada');
			var btnBajarPDFSeleccionada = Ext.getCmp('btnBajarPDFSeleccionada');
			var btnBajarPDFNegociable = Ext.getCmp('btnBajarPDFNegociable');
			
			btnBajarPDFOperadaPagada.hide();
			btnBajarPDFOperada.hide();
			btnBajarPDFSeleccionada.hide();
			btnBajarPDFNegociable.hide();
			
			if(store.getTotalCount()>0){
				btnBajarPDFSeleccionada.hide();
				btnGenerarPDFSeleccionada.enable();
				var cm = gridAutProgSel.getColumnModel();
				cm.setHidden(cm.findColumnIndex('DF_PROGRAMACION'),true);
				cm.setHidden(cm.findColumnIndex('FECHA_VENC_PYME'),true);
				cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'),true);
				cm.setHidden (cm.findColumnIndex('NETO_RECIBIR'),true);
				cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('MANDANTE'),true);
				
				if(estatus == 26){
					cm.setHidden(cm.findColumnIndex('DF_PROGRAMACION'),false);
				}
				if(jsonValoresIniciales.sFechaVencPyme!=""){
					cm.setHidden(cm.findColumnIndex('FECHA_VENC_PYME'),false);
				}
				if(jsonValoresIniciales.bTipoFactoraje){/* Desplegar solo si opera "Factoraje Vencido" o "Factoraje Distribuido" */
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'),false);
				}
				if(((jsonValoresIniciales.bOperaFactorajeDistribuido == true || 
					jsonValoresIniciales.bOperaFactVencimientoInfonavit == true) && estatus == 24)
					||((jsonValoresIniciales.bOperaFactorajeDistribuido == true) && estatus != 24)){ /* Desplegar solo si opera "Factoraje Distribuido" */
					cm.setHidden (cm.findColumnIndex('NETO_RECIBIR'),false);
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'),false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'),false);
					cm.setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'),false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato == true) {
					cm.setHidden(cm.findColumnIndex('MANDANTE'),false);					
				}				
				el.unmask();
				//Calcular el valor del Monto Total Descontar
				montoDescontarMN = 0;
				montoDescontarUSD = 0;
				store.each(function(registro){
					var icMoneda = registro.get('IC_MONEDA');
					if(icMoneda == 1){
						montoDescontarMN += registro.get('FN_MONTO_DSCTO');
					}else{
						montoDescontarUSD += registro.get('FN_MONTO_DSCTO');
					}
				});
			}else{
				btnGenerarPDFSeleccionada.disable();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	var procesarConsultaDataOperada = function(store,arrRegistros,opts){
		fp.el.unmask();
		var gridOperada = Ext.getCmp('gridOperada');
		var el = gridOperada.getGridEl();
		if(arrRegistros != null){
			if(!gridOperada.isVisible()){
				gridOperada.show();
			}
			var btnGenerarPDFOperada = Ext.getCmp('btnGenerarPDFOperada');
			var btnBajarPDFOperadaPagada = Ext.getCmp('btnBajarPDFOperadaPagada');
			var btnBajarPDFOperada = Ext.getCmp('btnBajarPDFOperada');
			var btnBajarPDFSeleccionada = Ext.getCmp('btnBajarPDFSeleccionada');
			var btnBajarPDFNegociable = Ext.getCmp('btnBajarPDFNegociable');
			
			btnBajarPDFOperadaPagada.hide();
			btnBajarPDFOperada.hide();
			btnBajarPDFSeleccionada.hide();
			btnBajarPDFNegociable.hide();
			
			if(store.getTotalCount()>0){
				btnGenerarPDFOperada.enable();
				btnBajarPDFOperada.hide();
				var cm = gridOperada.getColumnModel();
				cm.setHidden(cm.findColumnIndex('NOMBREIF'),true);
				cm.setHidden(cm.findColumnIndex('FECHA_VENC_PYME'),true);
				cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'),true);
				cm.setHidden (cm.findColumnIndex('NETO_RECIBIR'),true);
				cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('MANDANTE'),true);
				
				if(jsonValoresIniciales.bOperaFactVencimientoInfonavit == true){
					cm.setHidden(cm.findColumnIndex('NOMBREIF'),false);
				}
				if(jsonValoresIniciales.sFechaVencPyme!=""){
					cm.setHidden(cm.findColumnIndex('FECHA_VENC_PYME'),false);
				}				
				if(jsonValoresIniciales.bTipoFactoraje == true){/* Desplegar solo si opera "Factoraje Vencido" o "Factoraje Distribuido" */
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'),false);
				}

				if(jsonValoresIniciales.bOperaFactorajeDistribuido == true || 
					jsonValoresIniciales.bOperaFactVencimientoInfonavit == true){ /* Desplegar solo si opera "Factoraje Distribuido" */
					cm.setHidden (cm.findColumnIndex('NETO_RECIBIR'),false);
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'),false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'),false);
					cm.setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'),false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato == true) {
					cm.setHidden(cm.findColumnIndex('MANDANTE'),false);					
				}
				//calcular monto a descontar
				montoDescontarMN = 0;
				montoDescontarUSD = 0;
				store.each(function(registro){
					var icMoneda = registro.get('IC_MONEDA');
					if(icMoneda == 1){
						montoDescontarMN += registro.get('FN_MONTO_DSCTO');
					}else{
						montoDescontarUSD += registro.get('FN_MONTO_DSCTO');
					}
				});
				el.unmask();
			}else{
				btnGenerarPDFOperada.disable();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	var procesarConsultaDataOperadaPagada = function(store,arrRegistros,opts){
		fp.el.unmask();
		var gridOperadaPagada = Ext.getCmp('gridOperadaPagada');
		var el = gridOperadaPagada.getGridEl();
		if(arrRegistros != null){
			if(!gridOperadaPagada.isVisible()){
				gridOperadaPagada.show();
			}
		
			var btnGenerarPDFOperadaPagada = Ext.getCmp('btnGenerarPDFOperadaPagada');
			var btnBajarPDFOperadaPagada = Ext.getCmp('btnBajarPDFOperadaPagada');
			var btnBajarPDFOperada = Ext.getCmp('btnBajarPDFOperada');
			var btnBajarPDFSeleccionada = Ext.getCmp('btnBajarPDFSeleccionada');
			var btnBajarPDFNegociable = Ext.getCmp('btnBajarPDFNegociable');
			
			btnBajarPDFOperadaPagada.hide();
			btnBajarPDFOperada.hide();
			btnBajarPDFSeleccionada.hide();
			btnBajarPDFNegociable.hide();
			
			if(store.getTotalCount()>0){
				btnGenerarPDFOperadaPagada.enable();
				btnBajarPDFOperadaPagada.hide();
				var cm = gridOperadaPagada.getColumnModel();
				cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'),true);
				cm.setHidden(cm.findColumnIndex('FECHA_VENC_PYME'),true);
				cm.setHidden (cm.findColumnIndex('NETO_RECIBIR'),true);
				cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'),true);
				cm.setHidden(cm.findColumnIndex('MANDANTE'),true);
				
				if(jsonValoresIniciales.bOperaFactorajeVencido == true||
					jsonValoresIniciales.bOperaFactorajeDistribuido == true||
					jsonValoresIniciales.bOperaFactorajeNotaDeCredito == true||
					jsonValoresIniciales.bOperaFactVencimientoInfonavit == true){/* Desplegar solo si opera "Factoraje Vencido" o "Factoraje Distribuido" */
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'),false);
				}
				if(jsonValoresIniciales.sFechaVencPyme!=""){
					cm.setHidden(cm.findColumnIndex('FECHA_VENC_PYME'),false);
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido == true || 
					jsonValoresIniciales.bOperaFactVencimientoInfonavit == true){ /* Desplegar solo si opera "Factoraje Distribuido" */
					cm.setHidden (cm.findColumnIndex('NETO_RECIBIR'),false);
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'),false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'),false);
					cm.setHidden(cm.findColumnIndex('MONTO_BENEFICIARIO'),false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato == true ) {
					cm.setHidden(cm.findColumnIndex('MANDANTE'),false);					
				}
				//Calcular monto a descontar
				montoDescontarMN = 0;
				montoDescontarUSD = 0;
				store.each(function(registro){
					var icMoneda = registro.get('IC_MONEDA');
					if(icMoneda == 1){
						montoDescontarMN += registro.get('FN_MONTO_DSCTO');
					}else{
						montoDescontarUSD += registro.get('FN_MONTO_DSCTO');
					}
				});
				el.unmask();
			}else{
				btnGenerarPDFOperadaPagada.disable();
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
//----------------------------------------STORES--------------------------------
	var consultaDataNegociable = new Ext.data.JsonStore({
		root: 'registros',
		url: '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},		
		fields: [
					{name: 'NOMBREPYME'},
					{name: 'IG_NUMERO_DOCTO'},
					{name: 'FECHA_DOCTO'},
					{name: 'FECHA_VENC'},
					{name: 'FECHA_VENC_PYME'},
					{name: 'NOMBREMONEDA'},
					{name: 'TIPO_FACTORAJE'},
					{name: 'FN_MONTO'},
					{name: 'DBL_PORCENTAJE'},
					{name: 'FN_MONTO_DSCTC'},
					{name: 'CC_ACUSE'},
					{name: 'NOMBREIF'},
					{name: 'NOMBRE_BENEFICIARIO'},
					{name: 'FN_PORC_BENEFICIARIO'},
					{name: 'MONTO_BENEFICIARIO'},
					{name: 'MANDANTE'},
					{name: 'FN_MONTO_DSCTO', type: 'float'},
					{name: 'CS_DSCTO_ESPECIAL'},
					{name: 'IC_MONEDA', type: 'int'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaGridNegociable,
			exception: {
				fn: function(proxy,type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					procesarConsultaGridNegociable(null,null,null);
				}
			}
		}
	});
	var consultaDataAutProgSel = new Ext.data.JsonStore({
		root: 'registros',
		url: '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},		
		fields: [
					{name: 'NOMBREPYME'},
					{name: 'IG_NUMERO_DOCTO'},
					{name: 'FECHA_DOCTO'},
					{name: 'FECHA_VENC'},
					{name: 'FECHA_VENC_PYME'},
					{name: 'NOMBREMONEDA'},
					{name: 'TIPO_FACTORAJE'},
					{name: 'FN_MONTO'},
					{name: 'DBL_PORCENTAJE'},
					{name: 'FN_PORC_ANTICIPO'},
					{name: 'FN_MONTO_DOCTO'},
					{name: 'NOMBREIF'},
					{name: 'IN_TASA_ACEPTADA'},
					{name: 'PLAZO'},
					{name: 'IN_IMPORTE_INTERES'},
					{name: 'IN_IMPORTE_RECIBIR'},
					{name: 'CC_ACUSE'},
					{name: 'CT_REFERENCIA'},
					{name: 'NETO_RECIBIR'},
					{name: 'NOMBRE_BENEFICIARIO'},
					{name: 'FN_PORC_BENEFICIARIO'},
					{name: 'MONTO_BENEFICIARIO'},
					{name: 'MANDANTE'},
					{name: 'FN_MONTO_DSCTO', type: 'float'},
					{name: 'DF_PROGRAMACION'},
					{name: 'CS_DSCTO_ESPECIAL'},
					{name: 'IC_MONEDA', type: 'int'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaGridProgSel,
			exception: {
				fn: function(proxy,type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					procesarConsultaGridProgSel(null,null,null);
				}
			}
		}
	});	
	var consultaDataOperada = new Ext.data.JsonStore({
		root: 'registros',
		url: '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},		
		fields: [
					{name: 'NOMBREPYME'},
					{name: 'NOMBREIF'},
					{name: 'IG_NUMERO_DOCTO'},
					{name: 'FECHA_DOCTO'},
					{name: 'FECHA_VENC'},
					{name: 'FECHA_VENC_PYME'},
					{name: 'NOMBREMONEDA'},
					{name: 'TIPO_FACTORAJE'},
					{name: 'FN_MONTO'},
					{name: 'FN_PORC_ANTICIPO'},
					{name: 'FN_MONTO_DSCTO', type: 'float'},
					{name: 'IN_TASA_ACEPTADA'},
					{name: 'IN_IMPORTE_RECIBIR'},
					{name: 'CT_REFERENCIA'},
					{name: 'NETO_RECIBIR'},
					{name: 'NOMBRE_BENEFICIARIO'},
					{name: 'FN_PORC_BENEFICIARIO'},
					{name: 'MONTO_BENEFICIARIO'},
					{name: 'MANDANTE'},
					{name: 'CS_DSCTO_ESPECIAL'},
					{name: 'IC_MONEDA', type: 'int'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataOperada,
			exception: {
				fn: function(proxy,type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					procesarConsultaDataOperada(null,null,null);
				}
			}
		}
	});
	var consultaDataOperadaPagada = new Ext.data.JsonStore({
		root: 'registros',
		url: '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},		
		fields: [
					{name: 'NOMBREPYME'},
					{name: 'NOMBREIF'},
					{name: 'IC_FOLIO'},
					{name: 'IG_NUMERO_DOCTO'},
					{name: 'NOMBREMONEDA'},
					{name: 'TIPO_FACTORAJE'},
					{name: 'FN_MONTO'},
					{name: 'FN_PORC_ANTICIPO'},
					{name: 'FN_MONTO_DSCTO', type: 'float'},
					{name: 'FECHA_VENC'},
					{name: 'FECHA_VENC_PYME'},
					{name: 'NETO_RECIBIR'},
					{name: 'NOMBRE_BENEFICIARIO'},
					{name: 'FN_PORC_BENEFICIARIO'},
					{name: 'MONTO_BENEFICIARIO'},			
					{name: 'MANDANTE'},
					{name: 'CS_DSCTO_ESPECIAL'},
					{name: 'IC_MONEDA', type: 'int'}
		],
		totalProperty: 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataOperadaPagada,
			exception: {
				fn: function(proxy,type,action,optionsRequest,response,args){
					NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
					procesarConsultaDataOperadaPagada(null,null,null);
				}
			}
		}
	});

	var catalogoEstatus = new Ext.data.JsonStore({
		root: 'registros',
		fields: ['clave','descripcion','loadMsg'],
		url: '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEstatusDist'
		},
		totalProperty: 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//---------------------------------COMPONENTES----------------------------------

	var gridNegociable = new Ext.grid.GridPanel({
		store: consultaDataNegociable,
		id: 'gridNegociable',
		hidden: true,
		columns: [
						{
							header: 'Nombre Proveedor',
							tooltip: 'Nombre Proveedor',
							dataIndex: 'NOMBREPYME',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'N�mero de Documento',
							tooltip: 'N�mero de Documento',
							dataIndex: 'IG_NUMERO_DOCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Fecha Emisi�n',
							tooltip: 'Fecha Emisi�n',
							dataIndex: 'FECHA_DOCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Fecha Vencimiento',
							tooltip: 'Fecha Vencimiento',
							dataIndex: 'FECHA_VENC',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Fecha Vencimiento Proveedor',
							tooltip: 'Fecha Vencimiento Proveedor',
							dataIndex: 'FECHA_VENC_PYME',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Moneda',
							tooltip: 'Moneda',
							dataIndex: 'NOMBREMONEDA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Tipo Factoraje',
							tooltip: 'Tipo Factoraje',
							dataIndex: 'TIPO_FACTORAJE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Monto',
							tooltip: 'Monto',
							dataIndex: 'FN_MONTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer: Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Porcentaje de Descuento',
							tooltip: 'Porcentaje de Descuento',
							dataIndex: 'DBL_PORCENTAJE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Monto a Descontar',
							tooltip: 'Monto a Descontar',
							dataIndex: 'FN_MONTO_DSCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'N�mero de Acuse',
							tooltip: 'N�mero de Acuse',
							dataIndex: 'CC_ACUSE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Nombre IF',
							tooltip: 'Nombre IF',
							dataIndex: 'NOMBREIF',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Beneficiario',
							tooltip: 'Beneficiario',
							dataIndex: 'NOMBRE_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Porcentaje Beneficiario',
							tooltip: 'Porcentaje Beneficiario',
							dataIndex: 'FN_PORC_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true,
							renderer:Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Importe a recibir Beneficiario',
							tooltip: 'Importe a recibir Beneficiario',
							dataIndex: 'MONTO_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Mandatario',
							tooltip: 'Mandatario',
							dataIndex: 'MANDANTE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		style: 'margin:0 auto;',
		title: 'Estatus Negociable',
		frame: true,
		bbar: {
					items:[
								'->','-',							
								
									{
										xtype: 'button',
										text: 'Generar PDF',
										id: 'btnGenerarPDFNegociable',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											Ext.Ajax.request({
													url: '13reporte01ext.data.jsp',
													params: Ext.apply(fp.getForm().getValues(),{
														informacion: 'ArchivoPDF',
														sFechaVencPyme: jsonValoresIniciales.sFechaVencPyme,
														bOperaFactorajeVencido: jsonValoresIniciales.bOperaFactorajeVencido,
														bOperaFactorajeDistribuido: jsonValoresIniciales.bOperaFactorajeDistribuido,
														bOperaFactorajeNotaDeCredito: jsonValoresIniciales.bOperaFactorajeNotaDeCredito,
														bOperaFactConMandato: jsonValoresIniciales.bOperaFactConMandato,
														bOperaFactVencimientoInfonavit: jsonValoresIniciales.bOperaFactVencimientoInfonavit,
														bTipoFactoraje: jsonValoresIniciales.bTipoFactoraje,
														sOperaNotas: jsonValoresIniciales.sOperaNotas
													}),
													callback: procesarSuccessFailureGenerarPDFNegociable
											});
										}
									},
									{
										xtype: 'button',
										text: 'Bajar PDF',
										id: 'btnBajarPDFNegociable',
										hidden: true
									}								
					]
		}
	});
	var gridAutProgSel = new Ext.grid.GridPanel({
		store: consultaDataAutProgSel,
		id: 'gridAutProgSel',
		hidden: true,
		columns: [
						{
							header: 'Nombre PYME',
							tooltip: 'Nombre PYME',
							dataIndex: 'NOMBREPYME',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'N�mero de Documento',
							tooltip: 'N�mero de Documento',
							dataIndex: 'IG_NUMERO_DOCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Fecha Emisi�n',
							tooltip: 'Fecha Emisi�n',
							dataIndex: 'FECHA_DOCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Fecha Vencimiento',
							tooltip: 'Fecha Vencimiento',
							dataIndex: 'FECHA_VENC',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Fecha Vencimiento Proveedor',
							tooltip: 'Fecha Vencimiento Proveedor',
							dataIndex: 'FECHA_VENC_PYME',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Moneda',
							tooltip: 'Moneda',
							dataIndex: 'NOMBREMONEDA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Tipo Factoraje',
							tooltip: 'Tipo Factoraje',
							dataIndex: 'TIPO_FACTORAJE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Monto de Documento',
							tooltip: 'Monto de Documento',
							dataIndex: 'FN_MONTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Porcentaje de Descuento',
							tooltip: 'Porcentaje de Descuento',
							dataIndex: 'FN_PORC_ANTICIPO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Monto a Descontar',
							tooltip: 'Monto a Descontar',
							dataIndex: 'FN_MONTO_DSCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Intermediario Financiero',
							tooltip: 'Intermediario Financiero',
							dataIndex: 'NOMBREIF',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 300
						},
						{
							header: 'Tasa',
							tooltip: 'Tasa',
							dataIndex: 'IN_TASA_ACEPTADA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Plazo (D�as)',
							tooltip: 'Plazo (D�as)',
							dataIndex: 'PLAZO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Monto Int',
							tooltip: 'Monto Int',
							dataIndex: 'IN_IMPORTE_INTERES',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Importe a Recibir',
							tooltip: 'Importe a Recibir',
							dataIndex: 'IN_IMPORTE_RECIBIR',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'N�mero de Acuse',
							tooltip: 'N�mero de Acuse',
							dataIndex: 'CC_ACUSE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Referencia',
							tooltip: 'Referencia',
							dataIndex: 'CT_REFERENCIA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Neto a Recibir Pyme',
							tooltip: 'Neto a Recibir Pyme',
							dataIndex: 'NETO_RECIBIR',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Beneficiario',
							tooltip: 'Beneficiario',
							dataIndex: 'NOMBRE_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Porcentaje Beneficiario',
							tooltip: 'Porcentaje Beneficiario',
							dataIndex: 'FN_PORC_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true,
							renderer:Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Importe a Recibir Beneficiario',
							tooltip: 'Porcentaje Beneficiario',
							dataIndex: 'MONTO_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Mandatario',
							tooltip: 'Mandatario',
							dataIndex: 'MANDANTE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Fecha Registro Operaci�n',
							tooltip: 'Fecha Registro Operaci�n',
							dataIndex: 'DF_PROGRAMACION',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		style: 'margin:0 auto;',
		title: 'Estatus',
		frame: true,
		bbar: {
					items:[
							'->','-',	
								
									{
										xtype: 'button',
										text: 'Generar PDF',
										id: 'btnGenerarPDFSeleccionada',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											Ext.Ajax.request({
													url: '13reporte01ext.data.jsp',
													params: Ext.apply(fp.getForm().getValues(),{
														informacion: 'ArchivoPDF',
														sFechaVencPyme: jsonValoresIniciales.sFechaVencPyme,
														bOperaFactorajeVencido: jsonValoresIniciales.bOperaFactorajeVencido,
														bOperaFactorajeDistribuido: jsonValoresIniciales.bOperaFactorajeDistribuido,
														bOperaFactorajeNotaDeCredito: jsonValoresIniciales.bOperaFactorajeNotaDeCredito,
														bOperaFactConMandato: jsonValoresIniciales.bOperaFactConMandato,
														bOperaFactVencimientoInfonavit: jsonValoresIniciales.bOperaFactVencimientoInfonavit,
														bTipoFactoraje: jsonValoresIniciales.bTipoFactoraje,
														sOperaNotas: jsonValoresIniciales.sOperaNotas
													}),
													callback: procesarSuccessFailureGenerarPDFSeleccionada
											});
										}
									},
									{
										xtype: 'button',
										text: 'Bajar PDF',
										id: 'btnBajarPDFSeleccionada',
										hidden: true
									}								
					]
		}
	});	
	var gridOperada = new Ext.grid.GridPanel({
		store: consultaDataOperada,
		id: 'gridOperada',
		hidden: true,
		columns: [
						{
							header: 'Nombre PYME',
							tooltip: 'Nombre PYME',
							dataIndex: 'NOMBREPYME',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Nombre IF',
							tooltip: 'Nombre IF',
							dataIndex: 'NOMBREIF',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Nombre de Documento',
							tooltip: 'Nombre de Documento',
							dataIndex: 'IG_NUMERO_DOCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Fecha Emisi�n',
							tooltip: 'Fecha Emisi�n',
							dataIndex: 'FECHA_DOCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Fecha Vencimiento',
							tooltip: 'Fecha Vencimiento',
							dataIndex: 'FECHA_VENC',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Fecha Vencimiento Proveedor',
							tooltip: 'Fecha Vencimiento Proveedor',
							dataIndex: 'FECHA_VENC_PYME',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Moneda',
							tooltip: 'Moneda',
							dataIndex: 'NOMBREMONEDA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Tipo Factoraje',
							tooltip: 'Tipo Factoraje',
							dataIndex: 'TIPO_FACTORAJE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Monto de Documento',
							tooltip: 'Monto de Documento',
							dataIndex: 'FN_MONTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Porcentaje de Descuento',
							tooltip: 'Porcentaje de Descuento',
							dataIndex: 'FN_PORC_ANTICIPO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Monto a Descontar',
							tooltip: 'Monto a Descontar',
							dataIndex: 'FN_MONTO_DSCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Tasa',
							tooltip: 'Tasa',
							dataIndex: 'IN_TASA_ACEPTADA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Importe a Recibir',
							tooltip: 'Importe a Recibir',
							dataIndex: 'IN_IMPORTE_RECIBIR',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Referencia',
							tooltip: 'Referencia',
							dataIndex: 'CT_REFERENCIA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Neto a Recibir PyME',
							tooltip: 'Neto a Recibir PyME',
							dataIndex: 'NETO_RECIBIR',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Beneficiario',
							tooltip: 'Beneficiario',
							dataIndex: 'NOMBRE_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Porcentaje Beneficiario',
							tooltip: 'Porcentaje Beneficiario',
							dataIndex: 'FN_PORC_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true,
							renderer:Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Importe a Recibir Beneficiario',
							tooltip: 'Importe a Recibir Beneficiario',
							dataIndex: 'MONTO_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Mandatario',
							tooltip: 'Mandatario',
							dataIndex: 'MANDANTE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		style: 'margin:0 auto;',
		title: 'Estatus Operada',
		frame: true,
		bbar: {
					items:[
								'->','-',								
									{
										xtype: 'button',
										text: 'Generar PDF',
										id: 'btnGenerarPDFOperada',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											Ext.Ajax.request({
													url: '13reporte01ext.data.jsp',
													params: Ext.apply(fp.getForm().getValues(),{
														informacion: 'ArchivoPDF',
														sFechaVencPyme: jsonValoresIniciales.sFechaVencPyme,
														bOperaFactorajeVencido: jsonValoresIniciales.bOperaFactorajeVencido,
														bOperaFactorajeDistribuido: jsonValoresIniciales.bOperaFactorajeDistribuido,
														bOperaFactorajeNotaDeCredito: jsonValoresIniciales.bOperaFactorajeNotaDeCredito,
														bOperaFactConMandato: jsonValoresIniciales.bOperaFactConMandato,
														bOperaFactVencimientoInfonavit: jsonValoresIniciales.bOperaFactVencimientoInfonavit,
														bTipoFactoraje: jsonValoresIniciales.bTipoFactoraje,
														sOperaNotas: jsonValoresIniciales.sOperaNotas
													}),
													callback: procesarSuccessFailureGenerarPDFOperada
											});
										}
									},
									{
										xtype: 'button',
										text: 'Bajar PDF',
										id: 'btnBajarPDFOperada',
										hidden: true
									}								
					]
		}
	});
	var gridOperadaPagada = new Ext.grid.GridPanel({
		store: consultaDataOperadaPagada,
		id: 'gridOperadaPagada',
		hidden: true,
		columns: [
						{
							header: 'Nombre PYME',
							tooltip: 'Nombre PYME',
							dataIndex: 'NOMBREPYME',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Nombre IF',
							tooltip: 'Nombre IF',
							dataIndex: 'NOMBREIF',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Nombre de Solicitud',
							tooltip: 'Nombre de Solicitud',
							dataIndex: 'IC_FOLIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Nombre de Documento',
							tooltip: 'Nombre de Documento',
							dataIndex: 'IG_NUMERO_DOCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Moneda',
							tooltip: 'Moneda',
							dataIndex: 'NOMBREMONEDA',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Tipo Factoraje',
							tooltip: 'Tipo Factoraje',
							dataIndex: 'TIPO_FACTORAJE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Monto',
							tooltip: 'Monto',
							dataIndex: 'FN_MONTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Porcentaje de Descuento',
							tooltip: 'Porcentaje de Descuento',
							dataIndex: 'FN_PORC_ANTICIPO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Tasa',
							tooltip: 'Tasa',
							dataIndex: 'FN_PORC_ANTICIPO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Monto a Descontar',
							tooltip: 'Monto a Descontar',
							dataIndex: 'FN_MONTO_DSCTO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Fecha Vencimiento',
							tooltip: 'Fecha Vencimiento',
							dataIndex: 'FECHA_VENC',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Fecha Vencimiento Proveedor',
							tooltip: 'Fecha Vencimiento Proveedor',
							dataIndex: 'FECHA_VENC_PYME',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Fecha de Pago',
							tooltip: 'Fecha de Pago',
							dataIndex: 'FECHA_VENC',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200
						},
						{
							header: 'Neto a Recibir Pyme',
							tooltip: 'Neto a Recibir Pyme',
							dataIndex: 'NETO_RECIBIR',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Beneficiario',
							tooltip: 'Beneficiario',
							dataIndex: 'NOMBRE_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						},
						{
							header: 'Porcentaje Beneficiario',
							tooltip: 'Porcentaje Beneficiario',
							dataIndex: 'FN_PORC_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true,
							renderer:Ext.util.Format.numberRenderer('0,0.00%')
						},
						{
							header: 'Importe a Recibir Beneficiario',
							tooltip: 'Importe a Recibir Beneficiario',
							dataIndex: 'MONTO_BENEFICIARIO',
							sortable: true,
							resiazable: true,
							align: 'center',	
							width: 200,
							hidden: true,
							renderer:Ext.util.Format.numberRenderer('$0,0.00')
						},
						{
							header: 'Mandatario',
							tooltip: 'Mandatario',
							dataIndex: 'MANDANTE',
							sortable: true,
							resiazable: true,
							align: 'center',
							width: 200,
							hidden: true
						}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		style: 'margin:0 auto;',
		title: 'Estatus Operada Pagada',
		frame: true,
		bbar: {
					items:[
								'->','-',
									{
										xtype: 'button',
										text: 'Generar PDF',
										id: 'btnGenerarPDFOperadaPagada',
										handler: function(boton, evento) {
											boton.disable();
											boton.setIconClass('loading-indicator');
											Ext.Ajax.request({
													url: '13reporte01ext.data.jsp',
													params: Ext.apply(fp.getForm().getValues(),{
														informacion: 'ArchivoPDF',
														sFechaVencPyme: jsonValoresIniciales.sFechaVencPyme,
														bOperaFactorajeVencido: jsonValoresIniciales.bOperaFactorajeVencido,
														bOperaFactorajeDistribuido: jsonValoresIniciales.bOperaFactorajeDistribuido,
														bOperaFactorajeNotaDeCredito: jsonValoresIniciales.bOperaFactorajeNotaDeCredito,
														bOperaFactConMandato: jsonValoresIniciales.bOperaFactConMandato,
														bOperaFactVencimientoInfonavit: jsonValoresIniciales.bOperaFactVencimientoInfonavit,
														bTipoFactoraje: jsonValoresIniciales.bTipoFactoraje,
														sOperaNotas: jsonValoresIniciales.sOperaNotas
													}),
													callback: procesarSuccessFailureGenerarPDFOperadaPagada
											});
										}
									},
									{
										xtype: 'button',
										text: 'Bajar PDF',
										id: 'btnBajarPDFOperadaPagada',
										hidden: true
									}								
					]
		}
	});
	var elementosForma = [
		{
			xtype: 'label',
			id: 'lblUsuario',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldlabel: 'Usuario',
			text: '--'
		},
		{
			xtype: 'label',
			id: 'lblFecha',
			style: 'font-weight:bold;color:#006699;text-align:center;display:block;',
			fieldlabel: 'Fecha',
			text: '--'
		},
		NE.util.getEspaciador(10),
		{
			xtype: 'combo',
			name: 'status',
			id: 'cmbStatus',
			fieldLabel: 'Estatus',
			mode: 'local',
			hiddenName: 'status',
			emptyText: 'Seleccione Estatus...',
			forceSelection: true,
			triggerAction: 'all',
			typeAhead: true,
			minChars: 1,
			store: catalogoEstatus,
			displayField: 'descripcion',
			valueField: 'clave',
			tpl: NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
						fn: function(combo){

							var gridNegociable = Ext.getCmp('gridNegociable');
							var gridAutProgSel = Ext.getCmp('gridAutProgSel');
							var gridOperada = Ext.getCmp('gridOperada');
							var gridOperadaPagada = Ext.getCmp('gridOperadaPagada');
							gridNegociable.hide();
							gridAutProgSel.hide();
							gridOperada.hide();
							gridOperadaPagada.hide();
							valIniciales();
							estatus = combo.getValue();
							if(estatus == "2"){
								consultaDataNegociable.load({
									params: Ext.apply(fp.getForm().getValues(),{
												sFechaVencPyme: jsonValoresIniciales.sFechaVencPyme,
												bOperaFactorajeVencido: jsonValoresIniciales.bOperaFactorajeVencido,
												bOperaFactorajeDistribuido: jsonValoresIniciales.bOperaFactorajeDistribuido,
												bOperaFactorajeNotaDeCredito: jsonValoresIniciales.bOperaFactorajeNotaDeCredito,
												bOperaFactConMandato: jsonValoresIniciales.bOperaFactConMandato,
												bOperaFactVencimientoInfonavit: jsonValoresIniciales.bOperaFactVencimientoInfonavit,
												sOperaNotas: jsonValoresIniciales.sOperaNotas,
												bTipoFactoraje: jsonValoresIniciales.bTipoFactoraje
									})
								});
							}else if(estatus == "3" || estatus == "24" || estatus == "26"){
								consultaDataAutProgSel.load({
									params: Ext.apply(fp.getForm().getValues(),{
												sFechaVencPyme: jsonValoresIniciales.sFechaVencPyme,
												bOperaFactorajeVencido: jsonValoresIniciales.bOperaFactorajeVencido,
												bOperaFactorajeDistribuido: jsonValoresIniciales.bOperaFactorajeDistribuido,
												bOperaFactorajeNotaDeCredito: jsonValoresIniciales.bOperaFactorajeNotaDeCredito,
												bOperaFactConMandato: jsonValoresIniciales.bOperaFactConMandato,
												bOperaFactVencimientoInfonavit: jsonValoresIniciales.bOperaFactVencimientoInfonavit,
												sOperaNotas: jsonValoresIniciales.sOperaNotas,
												bTipoFactoraje: jsonValoresIniciales.bTipoFactoraje
									})
								});
							}else if(estatus == "4"){
								consultaDataOperada.load({
									params: Ext.apply(fp.getForm().getValues(),{
												sFechaVencPyme: jsonValoresIniciales.sFechaVencPyme,
												bOperaFactorajeVencido: jsonValoresIniciales.bOperaFactorajeVencido,
												bOperaFactorajeDistribuido: jsonValoresIniciales.bOperaFactorajeDistribuido,
												bOperaFactorajeNotaDeCredito: jsonValoresIniciales.bOperaFactorajeNotaDeCredito,
												bOperaFactConMandato: jsonValoresIniciales.bOperaFactConMandato,
												bOperaFactVencimientoInfonavit: jsonValoresIniciales.bOperaFactVencimientoInfonavit,
												sOperaNotas: jsonValoresIniciales.sOperaNotas,
												bTipoFactoraje: jsonValoresIniciales.bTipoFactoraje
									})
								});								
							}else if(estatus == "11"){
								consultaDataOperadaPagada.load({
									params: Ext.apply(fp.getForm().getValues(),{
												sFechaVencPyme: jsonValoresIniciales.sFechaVencPyme,
												bOperaFactorajeVencido: jsonValoresIniciales.bOperaFactorajeVencido,
												bOperaFactorajeDistribuido: jsonValoresIniciales.bOperaFactorajeDistribuido,
												bOperaFactorajeNotaDeCredito: jsonValoresIniciales.bOperaFactorajeNotaDeCredito,
												bOperaFactConMandato: jsonValoresIniciales.bOperaFactConMandato,
												bOperaFactVencimientoInfonavit: jsonValoresIniciales.bOperaFactVencimientoInfonavit,
												sOperaNotas: jsonValoresIniciales.sOperaNotas,
												bTipoFactoraje: jsonValoresIniciales.bTipoFactoraje
									})
								});								
							}
						}
				}
			}
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: 'margin:0 auto;',
		frame: true,
		border: false,
		title: '<div><center>Documentos por Estatus</div>',
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		hidden: false,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: true
	});
	
//-----------------------------------PRINCIPAL----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
					fp,
					NE.util.getEspaciador(20),
					gridNegociable,
					gridAutProgSel,
					gridOperada,
					gridOperadaPagada					
		]
	});
	//Petici�n para obtener los valores iniciales
	var valIniciales = function(){
		Ext.Ajax.request({
			url: '13reporte01ext.data.jsp',
			params: {
						informacion: "valoresIniciales"
			},
			callback: procesaValoresIniciales
		});
	}
	valIniciales();
	catalogoEstatus.load();
});