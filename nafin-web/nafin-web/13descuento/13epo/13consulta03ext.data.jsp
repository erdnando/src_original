<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		java.math.*,
		java.io.File,
		com.netro.exception.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		netropology.utilerias.*,
		com.netro.threads.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%

	String informacion         = (request.getParameter("informacion")         != null)?request.getParameter("informacion"):"";
	String cg_pyme_epo_interno = (request.getParameter("cg_pyme_epo_interno") != null)?request.getParameter("cg_pyme_epo_interno"):"";
	String num_electronico     = (request.getParameter("num_electronico")     != null)?request.getParameter("num_electronico"):"";
	String ig_numero_docto     = (request.getParameter("ig_numero_docto")     != null)?request.getParameter("ig_numero_docto"):"";
	String df_fecha_vencMin    = (request.getParameter("df_fecha_vencMin")    != null)?request.getParameter("df_fecha_vencMin"):"";
	String df_fecha_vencMax    = (request.getParameter("df_fecha_vencMax")    != null)?request.getParameter("df_fecha_vencMax"):"";
	String ic_moneda           = (request.getParameter("ic_moneda")           != null)?request.getParameter("ic_moneda"):"";
	String fn_montoMin         = (request.getParameter("fn_montoMin")         != null)?request.getParameter("fn_montoMin"):"";
	String fn_montoMax         = (request.getParameter("fn_montoMax")         != null)?request.getParameter("fn_montoMax"):"";
	String ic_cambio_estatus   = (request.getParameter("ic_cambio_estatus")   != null)?request.getParameter("ic_cambio_estatus"):"";
	String dc_fecha_cambioMin  = (request.getParameter("dc_fecha_cambioMin")  != null)?request.getParameter("dc_fecha_cambioMin"):"";
	String ic_pyme             = (request.getParameter("ic_pyme")             != null)?request.getParameter("ic_pyme"):"";
	String operacion           = (request.getParameter("operacion")           != null) ? request.getParameter("operacion"):"";
	String dc_fecha_cambioMax  = (request.getParameter("dc_fecha_cambioMax")  != null) ? request.getParameter("dc_fecha_cambioMax"):"";
	SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
	String fechaActual=  fecha2.format(new java.util.Date());
	String txt_opera_fac_venc = "F";
	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
	
	int start= 0, limit =0;
	String infoRegresar = "";
	String estatus ="", mensajeBusquedaDatosPYME 	= "", mostrarMensaje ="",  txtCadenasPymes	="";
	String claveEPO = iNoCliente; 
	boolean bOperaFactConMandato=false;
	boolean bTipoFactoraje = false;
	boolean bOperaFactorajeVencInfonavit = false;
	boolean bOperaFactorajeVencido = false;
	boolean bFactorajeIf						= false;
	String  SOperaFactConMandato="N";
	String  SOperaFactorajeVencInfonavit = "N";
	String  SOperaFactorajeVencido = "N";
	String  STipoFactoraje = "N";
	
	JSONObject 	resultado	= new JSONObject();		
	HashMap	datos = new HashMap();
	JSONArray registros = new JSONArray();
	int totalRegistros =0;
try{

	ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
	String sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(iNoCliente);
	
	Hashtable alParamEPO1 = new Hashtable(); 
	alParamEPO1 = BeanParamDscto.getParametrosEPO(claveEPO,1);
	if (alParamEPO1!=null) {
		bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		bOperaFactorajeVencido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
		bOperaFactorajeVencInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;
		bFactorajeIf								=	("N".equals(alParamEPO1.get("FACTORAJE_IF").toString()))?false:true; //FODEA Noviembre 2012
	}
	bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencInfonavit||bFactorajeIf)?true:false;
	
		if(bOperaFactConMandato == true) SOperaFactConMandato ="S";
		if(bOperaFactorajeVencido == true) SOperaFactorajeVencido ="S";
		if(bOperaFactorajeVencInfonavit == true) SOperaFactorajeVencInfonavit ="S";
		if(bTipoFactoraje == true||bFactorajeIf) STipoFactoraje ="S";
		
	
	
	String firmaMancomunada= BeanParamDscto.getOperaFirmaMancomunada(iNoCliente, 1);
		estatus ="4,5,6,7,8,16,24,28,29,31,45,46";
	if(firmaMancomunada.equals("S")){
		estatus ="4,5,6,7,8,16,24,28,29,31,37,38,39,40,45,46";
	}
	
		
	if (informacion.equals("CatalogoMoneda") ) {
	
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_moneda");
		catalogo.setCampoDescripcion("cd_nombre");
		catalogo.setTabla("comcat_moneda");		
		catalogo.setOrden("ic_moneda");
		catalogo.setValoresCondicionIn("1,54", Integer.class);
		infoRegresar = catalogo.getJSONElementos();	
		
	}else if (informacion.equals("CatalogoEstatus") ) {
	
		CatalogoSimple catalogo = new CatalogoSimple();
		catalogo.setCampoClave("ic_cambio_estatus");
		catalogo.setCampoDescripcion("cd_descripcion");
		catalogo.setTabla("comcat_cambio_estatus");		
		catalogo.setOrden("ic_cambio_estatus");
		catalogo.setValoresCondicionIn(estatus, Integer.class);
		infoRegresar = catalogo.getJSONElementos();	

	} else if (informacion.equals("busquedaAvanzada") ) {
	
		String nombrePyme = request.getParameter("nombrePyme")==null?"":request.getParameter("nombrePyme");
		String rfcPyme = request.getParameter("rfcPyme")==null?"":request.getParameter("rfcPyme");
		String noPyme = request.getParameter("noPyme")==null?"":request.getParameter("noPyme");
			
		CatalogoPymeBusCD cat = new CatalogoPymeBusCD();
		cat.setCampoClave("pe.cg_pyme_epo_interno ");
		cat.setCampoDescripcion(" pe.cg_pyme_epo_interno || ' ' || p.cg_razon_social ");
		cat.setOrden("p.cg_razon_social");
		cat.setClaveEpo(iNoCliente);
		cat.setNombrePyme(nombrePyme);
		cat.setRfcPyme(rfcPyme);
		cat.setClavePyme(noPyme);	
		
		infoRegresar = cat.getJSONElementos();

	} else if (informacion.equals("ValidaClavePYME") ) {
	
		//Obtener parametros de la PYME, en caso el usuario especifique un numero de nafin electronico
		HashMap	datosPYME 						= BeanParamDscto.getParametrosPYME(cg_pyme_epo_interno,claveEPO);
		if(datosPYME != null){
			ic_pyme 					= (String) datosPYME.get("IC_PYME");
			cg_pyme_epo_interno 	= (String) datosPYME.get("NUMERO_PROVEEDOR"); // Debug CHANGE
			txtCadenasPymes		= (String) datosPYME.get("DESCRIPCION");
		}
	// Mostrar mensaje
		if(datosPYME == null && !cg_pyme_epo_interno.equals("")){
			mensajeBusquedaDatosPYME = "La PYME con Numero de Proveedor: " + cg_pyme_epo_interno + " no existe o no esta relacionada con la EPO.";
		}

		resultado.put("success", new Boolean(true));	
		resultado.put("mensajeBusquedaDatosPYME",mensajeBusquedaDatosPYME);	
		resultado.put("ic_pyme",ic_pyme);	
		resultado.put("cg_pyme_epo_interno",cg_pyme_epo_interno);	
		resultado.put("txtCadenasPymes",txtCadenasPymes);
		infoRegresar = resultado.toString();	
			
	} else if(informacion.equals("Consultar") || informacion.equals("ArchivoPDF") || informacion.equals("ArchivoCSV") || informacion.equals("ResumenTotales")){

		String consulta  ="";

		///Obtener parametros de la PYME, en caso el usuario especifique un numero de nafin electronico
		if(!cg_pyme_epo_interno.equals("")){
			HashMap	datosPYME = BeanParamDscto.getParametrosPYME(cg_pyme_epo_interno,claveEPO);
			if(datosPYME != null){
				ic_pyme             = (String) datosPYME.get("IC_PYME");
				cg_pyme_epo_interno = (String) datosPYME.get("NUMERO_PROVEEDOR");
				txtCadenasPymes     = (String) datosPYME.get("DESCRIPCION");
			}
		}
		CambioEstatusEpoDE paginador = new CambioEstatusEpoDE();
		paginador.setIcPyme(ic_pyme);
		paginador.setIc_epo(claveEPO);
		paginador.setIgNumeroDocto(ig_numero_docto);
		paginador.setDfFechaVencMin(df_fecha_vencMin);
		paginador.setDfFechaVencMax(df_fecha_vencMax);
		paginador.setIcMoneda(ic_moneda);
		paginador.setIcCambioEstatus(ic_cambio_estatus);
		paginador.setFnMontoMin(fn_montoMin);
		paginador.setFnMontoMax(fn_montoMax);
		paginador.setDcFechaCambioMin(dc_fecha_cambioMin);
		paginador.setDcFechaCambioMax(dc_fecha_cambioMax);	
		paginador.setFechaActual(fechaActual);		
		paginador.setTxt_opera_fac_venc(txt_opera_fac_venc);
		paginador.setStrAforo(strAforo);
		paginador.setStrAforoDL(strAforo);
		paginador.setIdioma(idioma);

		CQueryHelperThreadRegExtJS queryHelper = new CQueryHelperThreadRegExtJS(paginador);
		Registros registrosC =null;

		if(informacion.equals("Consultar")){

			try {
				start = Integer.parseInt(request.getParameter("start"));
				limit = Integer.parseInt(request.getParameter("limit"));
			} catch(Exception e) {
				throw new AppException("Error en los parametros recibidos", e);
			}
			try {
				if (operacion.equals("Generar")) {	//Nueva consulta
					queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
				}
				consulta = queryHelper.getJSONPageResultSet(request,start,limit);
			} catch(Exception e) {
				throw new AppException("Error en la paginacion", e);
			}
				resultado = JSONObject.fromObject(consulta);
				resultado.put("sFechaVencPyme", sFechaVencPyme);
				resultado.put("ic_cambio_estatus", ic_cambio_estatus);
				resultado.put("SOperaFactConMandato", SOperaFactConMandato);
				resultado.put("SOperaFactorajeVencInfonavit", SOperaFactorajeVencInfonavit);
				resultado.put("SOperaFactorajeVencido", SOperaFactorajeVencido);
				resultado.put("STipoFactoraje", STipoFactoraje);
				infoRegresar = resultado.toString();

		} else if (informacion.equals("ResumenTotales")){

			//Debe ser llamado despues de Consulta
			queryHelper = new CQueryHelperThreadRegExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion	
			infoRegresar = queryHelper.getJSONResultCount(request); // los saca de sesion
			System.out.println("infoRegresar: "+ infoRegresar);

		} else if (informacion.equals("ArchivoPDF")){
/*
			JSONObject jsonObj = new JSONObject();
			try {
				String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
			} catch(Throwable e) {
				jsonObj.put("success", new Boolean(false));
				throw new AppException("Error al generar el archivo PDF", e);
			}		
			infoRegresar = jsonObj.toString();
*/
			try {
				String estatusArchivo = request.getParameter("estatusArchivo");
				String porcentaje = "";
				String nombreArchivo = "";

				if(estatusArchivo.equals("INICIO")){
					session.removeAttribute("13consulta03ThreadCreateFiles");
					ThreadCreateFiles threadCreateFiles = queryHelper.getThreadCreateCustomFile(request, strDirectorioTemp, "PDF");
					session.setAttribute("13consulta03ThreadCreateFiles", threadCreateFiles);
					estatusArchivo = "PROCESANDO";
				}else if(estatusArchivo.equals("PROCESANDO")){
					ThreadCreateFiles threadCreateFiles = (ThreadCreateFiles)session.getAttribute("13consulta03ThreadCreateFiles");
					System.out.println("queryHelper.getPercent() = "+ threadCreateFiles.getPercent());
					porcentaje = String.valueOf(threadCreateFiles.getPercent()) + " %";
					if(!threadCreateFiles.isRunning() && !threadCreateFiles.hasError()){
						nombreArchivo = threadCreateFiles.getNombreArchivo();
						estatusArchivo = "FINAL";
					} else if(threadCreateFiles.hasError()){
						estatusArchivo = "ERROR";
					}
				}

				if(porcentaje.equals("") || porcentaje.equals("0") || porcentaje.equals("0 %"))
					porcentaje = "Procesando...";
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				jsonObj.put("estatusArchivo", estatusArchivo);
				jsonObj.put("porcentaje", porcentaje);
				infoRegresar = jsonObj.toString();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF", e);
			}

		} else if (informacion.equals("ArchivoCSV")) {

			try {
				String estatusArchivo = request.getParameter("estatusArchivo");
				String porcentaje = "";
				String nombreArchivo = "";

				if(estatusArchivo.equals("INICIO")){
					session.removeAttribute("13consulta03ThreadCreateFiles");
					ThreadCreateFiles threadCreateFiles = queryHelper.getThreadCreateCustomFile(request, strDirectorioTemp, "CSV");
					session.setAttribute("13consulta03ThreadCreateFiles", threadCreateFiles);
					estatusArchivo = "PROCESANDO";
				}else if(estatusArchivo.equals("PROCESANDO")){
					ThreadCreateFiles threadCreateFiles = (ThreadCreateFiles)session.getAttribute("13consulta03ThreadCreateFiles");
					System.out.println("queryHelper.getPercent() = "+ threadCreateFiles.getPercent());
					porcentaje = String.valueOf(threadCreateFiles.getPercent()) + " %";
					if(!threadCreateFiles.isRunning() && !threadCreateFiles.hasError()){
						nombreArchivo = threadCreateFiles.getNombreArchivo();
						estatusArchivo = "FINAL";
					} else if(threadCreateFiles.hasError()){
						estatusArchivo = "ERROR";
					}
				}

				if(porcentaje.equals("") || porcentaje.equals("0") || porcentaje.equals("0 %"))
					porcentaje = "Procesando...";
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success", new Boolean(true));
				jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				jsonObj.put("estatusArchivo", estatusArchivo);
				jsonObj.put("porcentaje", porcentaje);
				infoRegresar = jsonObj.toString();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV", e);
			}
		}
	}

}catch(NafinException ne){
	out.println(ne.getMsgError());
}catch(Exception e){
	out.println("Error: "+ e);
}

%>
<%=infoRegresar%>