<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,
		com.netro.descuento.InformacionTOIC,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="../13secsession.jspf" %>
<jsp:useBean id="mensaje_param" scope="application" type="netropology.utilerias.MensajeParam" />
<%
String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar = "";

if(informacion.equals("Consultar")) {
	
		List reporte = null;
		try {
			//Instanciación para uso del EJB
			InformacionTOIC bean = ServiceLocator.getInstance().lookup("InformacionTOICEJB", InformacionTOIC.class);
			
			String fecha_de = (request.getParameter("fecha_Inicio")!=null)?request.getParameter("fecha_Inicio"):"";
			String fecha_a = (request.getParameter("fecha_Fin")!=null)?request.getParameter("fecha_Fin"):"";
			
			reporte = bean.getResumenCargaFFON(fecha_de, fecha_a);
			JSONArray jsonArr = new JSONArray();		
			JSONObject jsonObjP;
			if(reporte.isEmpty()){
				request.setAttribute("NO_HAY_DATOS", "NO_HAY_DATOS");
			
			}else{
				
			
			HashMap reg=null;	
				for (int i = 0; i <reporte.size(); i++) {
					reg=(HashMap)reporte.get(i);
					jsonObjP= new JSONObject();
					jsonObjP.put("CARGADOS", reg.get("CARGADOS"));
					jsonObjP.put("CLAVE_CARGA", reg.get("CLAVE_CARGA"));
					jsonObjP.put("FECHA_PROCESO", reg.get("FECHA_PROCESO"));
					jsonObjP.put("NO_ENCONTRADOS", reg.get("NO_ENCONTRADOS"));
					jsonObjP.put("ENCONTRADOS", reg.get("ENCONTRADOS"));
					jsonArr.add(jsonObjP);
				}
			}
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("registros", jsonArr.toString() );
			System.out.println(jsonObj.toString());
			infoRegresar = jsonObj.toString();
			
			
		} catch(Exception e) {
			System.out.println("ReporteFFONAction::consulta::Exception");
			e.printStackTrace();
			//return mapping.getInputForward();
		} finally {
			System.out.println("ReporteFFONAction::consulta(S)"+strDirecVirtualTemp);
		}
		
		
	}
	else if(informacion.equals("DescargaArch")) {
	//String strDirectorioTemp	=	(request.getParameter("directorio")!=null)?request.getParameter("directorio"):"";	
	String tipo	=	(request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";	
	String clave_carga	=	(request.getParameter("clave_carga")!=null)?request.getParameter("clave_carga"):"";	
	
		//String nombreArchivo = Comunes.cadenaAleatoria(16)+".txt";
		String nombreArchivo = "";
		List detalleReporte = null;
		JSONObject jsonObj = new JSONObject();
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer("");
		
		//Instanciación para uso del EJB
		InformacionTOIC bean = ServiceLocator.getInstance().lookup("InformacionTOICEJB", InformacionTOIC.class);
		
		detalleReporte = bean.getDetalleCarga(tipo, clave_carga);
		
		//Si existen registros para el detalle
		if(detalleReporte.size()>0){
			for(int i=0; i<detalleReporte.size(); i++){
				HashMap det = (HashMap)detalleReporte.get(i);
				contenidoArchivo.append(det.get("NUMDOCTO_NAE").equals("")?" ":det.get("NUMDOCTO_NAE")+"|");
				contenidoArchivo.append(det.get("NUMDOCTO_FFON").equals("")?" ":det.get("NUMDOCTO_FFON")+"|");
				contenidoArchivo.append(det.get("FECHA_VENCIMIENTO").equals("")?" ":det.get("FECHA_VENCIMIENTO")+"|");
				contenidoArchivo.append(det.get("MONTO").equals("")?" ":det.get("MONTO")+"|");
				contenidoArchivo.append(det.get("MONEDA").equals("")?" ":det.get("MONEDA")+"|");
				contenidoArchivo.append(det.get("USUARIO").equals("")?" ":det.get("USUARIO")+"|");
				contenidoArchivo.append(det.get("AREACOMPRADORA").equals("")?" ":det.get("AREACOMPRADORA")+"|");
				contenidoArchivo.append(det.get("RFCBENEFICIARIO").equals("")?" ":det.get("RFCBENEFICIARIO")+"|");
				contenidoArchivo.append(det.get("BENEFICIARIO").equals("")?" ":det.get("BENEFICIARIO")+"\n");
			}
		}else{
			contenidoArchivo.append("NO HAY DATOS");
		}

		if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
			System.out.print("<--!Error al generar el archivo de interfase fija-->");
		} else {
			
			nombreArchivo = archivo.nombre;
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
		}
		
	infoRegresar = jsonObj.toString();
	}
	
%>
<%=infoRegresar%>