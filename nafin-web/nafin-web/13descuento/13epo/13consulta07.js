Ext.onReady(function() {

//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
function procesaDetalles(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			formAux.action = NE.appWebContextRoot+'/DescargaArchivo';
			formAux.nombreArchivo.value=archivo;
			formAux.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store,arrRegistros,opts){
			if(arrRegistros!=null){
					var el = gridGeneral.getGridEl();
					if(store.getTotalCount()>0){
							el.unmask();
					}else{
						el.mask('No se encontr� ning�n registro', 'x-mask');
			}
	}

}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT

	var consultaData = new Ext.data.JsonStore({
		xtype: 'jsonstore',
		root : 'registros',
		fields: [
			{name: 'CARGADOS'},
			{name: 'CLAVE_CARGA'},
			{name: 'FECHA_PROCESO'},
			{name: 'NO_ENCONTRADOS'},
			{name: 'ENCONTRADOS'}
		],
		url : '13consulta07Ext.data.jsp',
		baseParams: {
			informacion: 'Consultar'
		},
		autoLoad: false,
		listeners: {
		load: procesarConsultaData,
		exception: {
					fn: function(proxy,type,action,optionsRequest,response,args){
							NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args);
							}
					}
		}
	});

//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN
	var elementosFecha = [
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Proceso',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_Inicio',
					id: 'df_consultaMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'df_consultaMax',
					margins: '0 20 0 0' ,
					formBind: true},
				{
					xtype: 'displayfield',
					value: 'a:',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'fecha_Fin',
					id: 'df_consultaMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'df_consultaMin',
					margins: '0 20 0 0',
					formBind: true
				}
			]
		}
	];



	var fp = {
		xtype: 'form',
		id: 'forma',
		width: 600,
		style: ' margin:0 auto;',
		collapsible: true,
		frame:true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[elementosFecha],
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnBuscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
				
					var cmpForma = Ext.getCmp('forma');
					paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
					gridGeneral.show();
					consultaData.load({
							params: Ext.apply(paramSubmit)
					});
				} //fin handler
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function(boton, evento) {
					window.location.reload();
					//window.location= '13consulta07Ext.jsp';
				}
				
			}
		]
	};//FIN DE LA FORMA
	
	
	//CREA EL GRID  
		var gridGeneral = new Ext.grid.GridPanel({
		store: consultaData,
		id: 'gridGeneral',
		style: ' margin:0 auto;',
		hidden: true,
		columns: [		
			{
				header: 'Fecha de Proceso',
				tooltip: 'Fecha de Proceso',
				dataIndex: 'FECHA_PROCESO',
				sortable: true,				
				align: 'left',
				width: 170
			},
			{
				xtype:	'actioncolumn',
				header: 'Registros Cargados', tooltip: 'Registros Cargados',
				dataIndex: 'CARGADOS',	align: 'center',	sortable: true,	resizable: true,	width: 120,	hideable: false, hidden: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return value+'&nbsp;&nbsp;';
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							
						},
						handler:function(grid, rowIndex, colIndex, item, event){
							var registro = grid.getStore().getAt(rowIndex);
							var icBitacora = registro.get('CLAVE_CARGA');
												
							Ext.Ajax.request({
							url: '13consulta07Ext.data.jsp',
							params: Ext.apply({
							informacion: "DescargaArch",
							clave_carga: icBitacora,
							tipo:''}),
							callback: procesaDetalles});
					
						}
					}
				]
			},
			{
				xtype:	'actioncolumn',
				header: 'Registros Validos',
				tooltip: 'Registros Validos',
				dataIndex: 'ENCONTRADOS',
				sortable: true,
				width: 120,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return value+'&nbsp;&nbsp;';
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							
						},
						handler:function(grid, rowIndex, colIndex, item, event){
							var registro = grid.getStore().getAt(rowIndex);
							var icBitacora = registro.get('CLAVE_CARGA');
						
							Ext.Ajax.request({
							url: '13consulta07Ext.data.jsp',
							params: Ext.apply({
							informacion: "DescargaArch",
							clave_carga: icBitacora,
							tipo:'S'}),
							callback: procesaDetalles});
					
						}
					}
				]
			},
			{
				xtype:	'actioncolumn',
				header: 'Registros No Validos',
				tooltip: 'Registros No Validos',
				dataIndex: 'NO_ENCONTRADOS',
				sortable: true,
				width: 120,
				resizable: true,
				hidden: false,
				align: 'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return value+'&nbsp;&nbsp;';
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							
						},
						handler:function(grid, rowIndex, colIndex, item, event){
							var registro = grid.getStore().getAt(rowIndex);
							var icBitacora = registro.get('CLAVE_CARGA');
						
						Ext.Ajax.request({
							url: '13consulta07Ext.data.jsp',
							params: Ext.apply({
							informacion: "DescargaArch",
							clave_carga: icBitacora,
							tipo:'N'}),
							callback: procesaDetalles});
					
						}
					}
				]
			}
			
		],	
		
		stripeRows: true,
		loadMask: true,
		deferRowRender: false,
		height: 400,
		width: 550,
		title: '',
		frame: true
			
		
	});//FIN DEL GRID	
	

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 'auto',
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(5),
			gridGeneral
		]
	});
		
	});
