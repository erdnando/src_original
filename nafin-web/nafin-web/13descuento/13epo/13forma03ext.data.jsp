<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.math.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.dispersion.*, 
		com.netro.descuento.*,
		net.sf.json.*,
		com.netro.pdf.*,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoPymeBusqAvanzadaManto,
		com.netro.model.catalogos.CatalogoSimple"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%!public static final boolean SIN_COMAS = false;%>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
String msgError = "";

String ic_pyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme").trim();
String txtNumProveedor 	= (request.getParameter("txtNumProveedor") 	== null) ? "" : request.getParameter("txtNumProveedor").trim();
String txtCadenasPymes 	= (request.getParameter("txtCadenasPymes") 	== null) ? "" : request.getParameter("txtCadenasPymes").trim();
String mostrarMensaje 	= (request.getParameter("mostrarMensaje") 	== null) ? "" : request.getParameter("mostrarMensaje").trim();

String ig_numero_docto = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto").trim();
String df_fecha_docto = (request.getParameter("df_fecha_docto") == null) ? "" : request.getParameter("df_fecha_docto");
String df_fecha_venc = (request.getParameter("df_fecha_venc") == null) ? "" : request.getParameter("df_fecha_venc");
String df_fecha_venc_pyme = (request.getParameter("df_fecha_venc_pyme") == null) ? "" : request.getParameter("df_fecha_venc_pyme");
String ic_moneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
String fn_montoMin = (request.getParameter("fn_montoMin") == null) ? "" : request.getParameter("fn_montoMin");
String fn_montoMax = (request.getParameter("fn_montoMax") == null) ? "" : request.getParameter("fn_montoMax");
String ic_estatus_consulta = (request.getParameter("ic_estatus_consulta") == null)?"":request.getParameter("ic_estatus_consulta");
String sParamFechaVencPyme = (request.getParameter("sParamFechaVencPyme") == null)?"":request.getParameter("sParamFechaVencPyme");
String tipoConsulta = (request.getParameter("tipoConsulta") == null)?"Consulta":request.getParameter("tipoConsulta");
String montoDocto = (request.getParameter("montoDocto") == null)?"":request.getParameter("montoDocto");
String claveEPO = iNoCliente;

JSONObject jsonObj = new JSONObject();
		
try{

		ConsMantoDoctos consMantoDoctos = new ConsMantoDoctos();		
		consMantoDoctos.setEsClavePyme(ic_pyme);
		consMantoDoctos.setEsNumeroDocto(ig_numero_docto);
		consMantoDoctos.setEsFechaDocumento(df_fecha_docto);
		consMantoDoctos.setEsFechaVencimiento(df_fecha_venc);
		consMantoDoctos.setEsClaveMoneda(ic_moneda);
		consMantoDoctos.setEsMontoMinimo(fn_montoMin);
		consMantoDoctos.setEsMontoMaximo(fn_montoMax);
		consMantoDoctos.setEsEstatus(ic_estatus_consulta);
		consMantoDoctos.setEsAforo(strAforo);
		consMantoDoctos.setEsClaveEpo(claveEPO);
		consMantoDoctos.setEsAforoDL(strAforoDL);
		consMantoDoctos.setTipoConsulta(tipoConsulta);
		consMantoDoctos.setMontoDocto(montoDocto);
		
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(consMantoDoctos);
		
		
		IMantenimiento BeanMantenimiento = ServiceLocator.getInstance().lookup("MantenimientoEJB", IMantenimiento.class);
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	if (informacion.equals("DatosIniciales")) {
		
		
		boolean bOperaFactorajeVencido = false;
		boolean bOperaFactConMandato=false;
		boolean bTipoFactoraje=false;
		boolean bOperaFactorajeVencidoInfonavit = false; //Fodea 042-2009 Vencimiento Infonavit
		boolean tieneParamEpoPef = false;
		boolean bFactorajeIf = false;
	
	
		jsonObj =  new JSONObject();
		JSONArray jsObjArrayCatalogos = new JSONArray();
		
		//Fodea 060-2010
		String estatus = (request.getParameter("estatus") == null) ? "" : request.getParameter("estatus").trim();
		String firmaMancomunada= BeanParamDscto.getOperaFirmaMancomunada(iNoCliente, 1);

		
		ArrayList lstIcEstatus = new ArrayList();
		lstIcEstatus.add(new Integer(1));
		lstIcEstatus.add(new Integer(2));
		lstIcEstatus.add(new Integer(21));
		lstIcEstatus.add(new Integer(33));
		if(firmaMancomunada.equals("S")){
			lstIcEstatus.add(new Integer(28));
		}
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setTabla("comcat_estatus_docto");
		cat.setCampoClave("ic_estatus_docto");
		cat.setCampoDescripcion("cd_descripcion");
		cat.setCondicionIn(lstIcEstatus);
		cat.setOrden("cd_descripcion");
		jsObjArrayCatalogos = cat.getJSONArray();
		jsonObj.put("registrosEstatus",jsObjArrayCatalogos.toString());
		
		CatalogoMoneda catMoneda = new CatalogoMoneda();
		catMoneda.setCampoClave("ic_moneda");
		catMoneda.setCampoDescripcion("cd_nombre"); 
		catMoneda.setValoresCondicionIn("1,54", Integer.class);

		jsObjArrayCatalogos = catMoneda.getJSONArray();
		jsonObj.put("registrosMoneda",jsObjArrayCatalogos.toString());
		
		String fechaActual = request.getParameter("fechaActual");
		if (fechaActual==null)
			fechaActual=(new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date());
		
		try {
			BeanMantenimiento.vvalidaFV(iNoCliente);
			bOperaFactorajeVencido = true;
			
			ArrayList alParamEPO = BeanParamDscto.getParamEPO(iNoCliente, 1);
			if(alParamEPO.size()>0) {
				sParamFechaVencPyme = (alParamEPO.get(7)==null)?"N":alParamEPO.get(7).toString();
			}
			
		} catch (Exception ne){
			bOperaFactorajeVencido = false;
		}
		
	
		String ses_ic_epo = iNoCliente; 
		Hashtable alParamEPO1 = new Hashtable(); 
		alParamEPO1 = BeanParamDscto.getParametrosEPO(ses_ic_epo,1);	
		if (alParamEPO1!=null) {
			  bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
			  bOperaFactorajeVencido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
			  bOperaFactorajeVencidoInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;	 
			  bFactorajeIf								=	("N".equals(alParamEPO1.get("FACTORAJE_IF").toString()))?false:true; //FODEA Noviembre 2012
		}
		
		tieneParamEpoPef = BeanMantenimiento.tieneParamEpoPef(iNoCliente);
	
		jsonObj.put("strLogin", strLogin);
		jsonObj.put("strNombreUsuario", strNombreUsuario);
		jsonObj.put("hidFechaActual", fechaActual);
		jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
		jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));
		jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
		jsonObj.put("bOperaFactorajeVencidoInfonavit", new Boolean(bOperaFactorajeVencidoInfonavit));
		jsonObj.put("tieneParamEpoPef", new Boolean(tieneParamEpoPef));
		jsonObj.put("firmaMancomunada", firmaMancomunada);
		jsonObj.put("sParamFechaVencPyme", sParamFechaVencPyme);
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
	
	}else if (informacion.equals("ConsultaClaveProv")) {
		JSONObject jsonObject = new JSONObject();
		
		boolean blBusquedaDatosPYME 	=  false;
		
		HashMap	datosPYME = BeanParamDscto.getParametrosPYME(txtNumProveedor,iNoCliente);
		
		if(datosPYME != null){
			ic_pyme 				= (String) datosPYME.get("IC_PYME");
			txtNumProveedor 	= (String) datosPYME.get("NUMERO_PROVEEDOR"); // Debug CHANGE
			txtCadenasPymes	= (String) datosPYME.get("DESCRIPCION");
		}

		// Mostrar mensaje 
		if(datosPYME == null && !txtNumProveedor.equals("")){
			blBusquedaDatosPYME = true; //"La PYME con Numero de Proveedor: " + txtNumProveedor + " no existe o no esta relacionada con la EPO.";
		}
		// Si no se encontro ninguna EPO limpiar los parametros ingresados
		if(datosPYME == null ){
			ic_pyme 				= "";
			txtNumProveedor 	= "";
			txtCadenasPymes	= "";
		}
		
		
		jsonObject.put("ic_pyme",ic_pyme);
		jsonObject.put("txtNumProveedor",txtNumProveedor);
		jsonObject.put("txtCadenasPymes",txtCadenasPymes);
		jsonObject.put("blBusquedaDatosPYME", new Boolean(blBusquedaDatosPYME));
		
		jsonObject.put("success", new Boolean(true));
		infoRegresar = jsonObject.toString();
	
	}else if (informacion.equals("BusquedaAvanzada")) {
		
		String nombreProv 	= (request.getParameter("nombreProv") 	== null) ? "" : request.getParameter("nombreProv").trim();
		String rfcFiado 	= (request.getParameter("rfcFiado") 	== null) ? "" : request.getParameter("rfcFiado").trim();
		String numProveedor 	= (request.getParameter("numProveedor") 	== null) ? "" : request.getParameter("numProveedor").trim();
		jsonObj =  new JSONObject();

		
		CatalogoPymeBusqAvanzadaManto cat = new CatalogoPymeBusqAvanzadaManto();
		cat.setCampoClave("ic_pyme");
		cat.setCampoDescripcion("(pe.cg_pyme_epo_interno ||'|'|| p.cg_razon_social)"); 
		cat.setCveEpo(iNoCliente);
		cat.setPantalla("MantenimientoDoctos");

		if(!"".equals(nombreProv)){
			cat.setNombrePyme(nombreProv);
		}
		if(!"".equals(rfcFiado)){
			cat.setRfcPyme(rfcFiado);
		}
		if(!"".equals(numProveedor)){
			cat.setNumPyme(numProveedor);
		}
		
		cat.setOrden("2");
		
		infoRegresar = cat.getJSONElementos();
		
	
	}else if (informacion.equals("ConsultaDoctosManto")) {
		JSONObject jsonObject = new JSONObject();
		
		
		//Paranetros del ObjGral de JavaScript
		
		boolean  bOperaFactConMandato = false;
		boolean  bOperaFactorajeVencido = false;
		boolean  bOperaFactorajeVencidoInfonavit = false;
		
		// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
		//String paginaNo = (request.getParameter("paginaNo") == null) ? "1" : request.getParameter("paginaNo");
			
		
		String ses_ic_epo = iNoCliente; 
		Hashtable alParamEPO1 = new Hashtable(); 
		alParamEPO1 = BeanParamDscto.getParametrosEPO(ses_ic_epo,1);	
		if (alParamEPO1!=null) {
			  bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
			  bOperaFactorajeVencido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
			  bOperaFactorajeVencidoInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;	 
		}
		
		//Variables de uso local
		List lstDataTotales  = new ArrayList();
		String condiciones="";
		int numRegistros=0;
		int numRegistrosMN=0;
		int numRegistrosDL=0;
		BigDecimal montoTotalMN = new BigDecimal("0.00");
		BigDecimal montoTotalDL = new BigDecimal("0.00");
		
		String Porcentaje = "";		

		Registros reg = new Registros();
		
		reg = queryHelper.doSearch();

		while(reg.next()){
			String claveDocumento = reg.getString("IC_DOCUMENTO");
			String sClavePyme = reg.getString("CLAVE_PYME");
			String sNombreIf = reg.getString("NOMBREIF");
			String sTipoFactoraje = reg.getString("CS_DSCTO_ESPECIAL");
			
			if(sTipoFactoraje.equals("N"))
				sTipoFactoraje = "Normal";
			else if(sTipoFactoraje.equals("M"))
				sTipoFactoraje = "Mandato";
			else if(sTipoFactoraje.equals("V"))
				sTipoFactoraje = "Vencido";
			else if(sTipoFactoraje.equals("D"))
				sTipoFactoraje = "Distribuido";
			else if(sTipoFactoraje.equals("C"))
				sTipoFactoraje = "Nota de Crédito";
			else if(sTipoFactoraje.equals("A"))//FODEA FACTORAJE IF
				sTipoFactoraje = "Automático";
				
				
			if ((bOperaFactorajeVencido && sTipoFactoraje.equals("Vencido"))||sTipoFactoraje.equals("Automático") )  {
				reg.setObject("NOMBREIF", sNombreIf);
			}else	if ( bOperaFactConMandato && sTipoFactoraje.equals("Mandato") ) {
				reg.setObject("NOMBREIF", sNombreIf);
			}else{
				reg.setObject("NOMBREIF", "");
			}
			
			if (bOperaFactorajeVencidoInfonavit  &&  sTipoFactoraje.equals("I") ) {
				Porcentaje = "100";
				reg.setObject("MONTOPORCENTAJE",Porcentaje);
				reg.setObject("NOMBREIF", sNombreIf);
			}
			
			if (reg.getString("IC_MONEDA").equals("1")) {
				numRegistrosMN++;
				if (reg.getString("FN_MONTO")!=null) montoTotalMN = montoTotalMN.add(new BigDecimal(reg.getString("FN_MONTO")));
				Porcentaje 	= new Double(new Double(strAforo).doubleValue() * 100).toString();
				reg.setObject("MONTOPORCENTAJE",Porcentaje);
			} else { //IC_MONEDA=54
				numRegistrosDL++;
				if (reg.getString("FN_MONTO")!=null) montoTotalDL = montoTotalDL.add(new BigDecimal(reg.getString("FN_MONTO")));
				Porcentaje 	= new Double(new Double(strAforoDL).doubleValue() * 100).toString();
				reg.setObject("MONTOPORCENTAJE",Porcentaje);
			}
			numRegistros++;
					
			if(BeanMantenimiento.tieneParamEpoPef(iNoCliente)){
				System.out.println("entra tres veces se supone");
				List valoresEpoPef = BeanMantenimiento.getDoctosMantFechaRecepcion(claveDocumento, iNoCliente, sClavePyme);
				if(valoresEpoPef.size() > 0){
					System.out.println("valoresEpoPef.get(0)=== "+valoresEpoPef.get(0)==null?"":(String)valoresEpoPef.get(0));
					reg.setObject("FECRECEP",  valoresEpoPef.get(0)==null?"":(String)valoresEpoPef.get(0));
					reg.setObject("TIPOCOMPRA",  valoresEpoPef.get(1)==null?"":(String)valoresEpoPef.get(1));
					reg.setObject("CLASIFICADOR",  valoresEpoPef.get(2)==null?"":(String)valoresEpoPef.get(2));
					reg.setObject("PLAZOMAX",  valoresEpoPef.get(3)==null?"":(String)valoresEpoPef.get(3));
				}
			}
		}
		
		if(numRegistros>0){
			HashMap dataTotales = new HashMap();
			dataTotales.put("MONEDA", "MONEDA NACIONAL");
			dataTotales.put("TOTAL_DOCUMENTOS", String.valueOf(numRegistrosMN));
			dataTotales.put("MONTO_TOTAL",montoTotalMN.toPlainString());
			
			lstDataTotales.add(dataTotales);
			
			dataTotales = new HashMap();
			dataTotales.put("MONEDA", "DOLAR AMERICANO");
			dataTotales.put("TOTAL_DOCUMENTOS", String.valueOf(numRegistrosDL));
			dataTotales.put("MONTO_TOTAL",montoTotalDL.toPlainString());
			
			lstDataTotales.add(dataTotales);
		
		}
		
		//SE GENERA COMBO DE ESTATUS POR ASIGNAR
		List lstEstatusAsignar = BeanMantenimiento.getEstatusAsignar(ic_estatus_consulta);


		JSONArray jsonArray = new JSONArray();
		jsonArray = JSONArray.fromObject(lstEstatusAsignar);
		jsonObject.put("registrosEstatus", jsonArray.toString());
		
		jsonArray = JSONArray.fromObject(lstDataTotales);
		jsonObject.put("registrosTotales", jsonArray.toString());
		
		jsonObject.put("success", new Boolean(true));
		jsonObject.put("total", String.valueOf(reg.getNumeroRegistros()));
		jsonObject.put("registros", reg.getJSONData());
		
		infoRegresar = jsonObject.toString();


	}else if(informacion.equals("CambioEstatus")){
		
	
		
		//Parámetros provenienen de la página actual
		String seleccionados[] =  null;
		String cveNuevoEstatus 	= (request.getParameter("ic_estatus_asignar1") 	== null) ? "" : request.getParameter("ic_estatus_asignar1").trim();
		String ct_cambio_motivo 	= (request.getParameter("txtCausa") 	== null) ? "" : request.getParameter("txtCausa").trim();
		
		String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
		List arrRegistrosModificados = JSONArray.fromObject(jsonRegistros);
		Iterator itReg = arrRegistrosModificados.iterator();
		
		seleccionados = new String[arrRegistrosModificados.size()];
		
		int index = 0;
		while (itReg.hasNext()) {
			JSONObject registro = (JSONObject)itReg.next();
			seleccionados[index] = registro.getString("IC_DOCUMENTO");
			index++;
		}
		
		BeanMantenimiento.vrealizarCambioEstatusDocto( cveNuevoEstatus, seleccionados, ct_cambio_motivo, ic_estatus_consulta, strLogin + " " + strNombreUsuario);//FODEA 015 - 2009 ACF
		
		jsonObj.put("success", new Boolean(true));
		infoRegresar = jsonObj.toString();
		
		
	}else if(informacion.equals("consDetalleDoctos")){
	
		try {
			Registros reg	=	queryHelper.doSearch();
			infoRegresar	=	"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
		}	catch(Exception e) {
			throw new AppException("Error en la paginación", e);
		}
	
	
	}
}catch(Throwable t){
	t.printStackTrace();
	throw new AppException("Error en la peticion",t);
}finally{
	System.out.println("infoRegresar = " + infoRegresar);
}




%>

<%=infoRegresar%>