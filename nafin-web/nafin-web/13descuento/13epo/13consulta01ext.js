Ext.onReady(function() {

//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT

	var VisorAcusesDeCarga = function(rec){
		var ic_acuse = rec.get('CC_ACUSE');
		if(ic_acuse != ''){
			var win = new NE.AcusesDeCarga.VisorAcusesDeCarga(ic_acuse);
		}
	}

	var param;
	var ocultarDoctosAplicados  =	false;
	var layOutAserca = false;
	var notasA = {ic_documento:null,	notaAplica:null, notaCredito:null,	notasDocto:null}
	var form = {
		cg_pyme_epo_interno:null,	ic_pyme:null,	ic_if:null,	ic_estatus_docto:null,	ic_moneda:null,	cc_acuse:null,
		no_docto:null,	df_fecha_docto_de:null,	df_fecha_docto_a:null,	fn_monto_de:null,	fn_monto_a:null,	df_fecha_venc_de:null,
		df_fecha_venc_a:null,	ord_proveedor:null,	ord_if:null,	ord_monto:null,	ord_fec_venc:null,	ord_estatus:null,
		camp_dina1:null,	camp_dina2:null,	operaFirmaM:null,	operaNotasC:null,	numero_siaff:null,
		cmbFactoraje:null,  validaDoctoEPO:null
	}

	function procesarSuccessFailureParametrizacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			param = Ext.util.JSON.decode(response.responseText);
			
			if(param.strPerfil ==='ADMIN PEMEX AMP') {	//2018_02		
				Ext.getCmp('idContrato').show();
				Ext.getCmp('idCopade').show();
			}
			
			if (param.esNumeroSIAFF != undefined && param.esNumeroSIAFF == "true"){
				param.esNumeroSIAFF = true;
				if (!Ext.getCmp('numero_siaff').isVisible()){
					Ext.getCmp('numero_siaff').show();
				}
			}else{
				if (Ext.getCmp('numero_siaff').isVisible()){
					Ext.getCmp('numero_siaff').hide();
				}
			}
			//var camposDinamicos = param.camp_dina1;
			if (param.camp_dina1 != undefined){
				Ext.each(param.camp_dina1, function(item, index, arrItems){
					if (item.IC_NO_CAMPO == "1") {
						if (item.CG_TIPO_OBJETO == "T")	{
							if (item.CG_TIPO_DATO == "N")	{
								var config = {xtype: 'textfield',name: 'camp_dina1',id: 'campDina1',allowBlank: true, anchor: '45%', fieldLabel: item.NOMBRE_CAMPO,
									maxLength: item.IG_LONGITUD,hidden:	false, maskRe:/[0-9.]/}
							}else{
								var config = {xtype: 'textfield',name: 'camp_dina1',id: 'campDina1',allowBlank: true, anchor: '45%', fieldLabel: item.NOMBRE_CAMPO,
									maxLength: item.IG_LONGITUD,hidden:	false}
							}
							fp.add(config);
							fp.insert(10,config);
							fp.doLayout();
						}else	{
							var config = {
								xtype: 'combo',
								name: 'camp_dina1',
								id: 'campDina1',
								fieldLabel: item.NOMBRE_CAMPO,
								emptyText: 'Seleccionar',
								mode: 'local',
								displayField: 'descripcion',
								valueField: 'clave',
								hiddenName : 'camp_dina1',
								forceSelection : false,
								triggerAction : 'all',
								typeAhead: true,
								minChars : 1,
								store: catalogoCamposAdicionalesData,
								tpl : NE.util.templateMensajeCargaCombo
							}
							fp.add(config);
							fp.insert(10,config);
							fp.doLayout();
							catalogoCamposAdicionalesData.load();
						}
					}
					if (item.IC_NO_CAMPO == "2") {
						if (item.CG_TIPO_OBJETO == "T")	{
							if (item.CG_TIPO_DATO == "N")	{
								var config = {xtype: 'textfield',name: 'camp_dina2',id: 'campDina2',allowBlank: true, anchor: '45%', fieldLabel: item.NOMBRE_CAMPO,
									maxLength: item.IG_LONGITUD,hidden:	false, maskRe:/[0-9.]/}
							}else{
								var config = {xtype: 'textfield',name: 'camp_dina2',id: 'campDina2',allowBlank: true, anchor: '45%' ,fieldLabel: item.NOMBRE_CAMPO,
									maxLength: item.IG_LONGITUD,hidden:	false}
							}
							fp.add(config);
							fp.insert(11,config);
							fp.doLayout();
						}else	{
							var config = {
								xtype: 'combo',
								name: 'camp_dina2',
								id: 'campDina2',
								fieldLabel: item.NOMBRE_CAMPO,
								emptyText: 'Seleccionar',
								mode: 'local',
								displayField: 'descripcion',
								valueField: 'clave',
								hiddenName : 'camp_dina2',
								forceSelection : false,
								triggerAction : 'all',
								typeAhead: true,
								minChars : 1,
								store: catalogoCamposAdicionales_2_Data,
								tpl : NE.util.templateMensajeCargaCombo
							}
							fp.add(config);
							fp.insert(11,config);
							fp.doLayout();
							catalogoCamposAdicionales_2_Data.load();
						}
					}
				});
			}
			fp.el.unmask();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaNombrePyme(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR.txtCadenasPymes != undefined){
				Ext.getCmp('hid_ic_pyme').setValue(infoR.ic_pyme)
				//Ext.getCmp('cg_pyme_epo_interno').setValue(infoR.cg_pyme_epo_interno);
				Ext.getCmp('txtCadenasPymes').setValue(infoR.txtCadenasPymes);
			}
			if (infoR.muestraMensaje != undefined && infoR.muestraMensaje){
				Ext.Msg.alert(Ext.getCmp('cg_pyme_epo_interno').fieldLabel,'La PYME con Numero de Proveedor: ' + Ext.getCmp('cg_pyme_epo_interno').getValue() + ' no tiene Publicacion.');
				Ext.getCmp('hid_ic_pyme').setValue('')
				Ext.getCmp('cg_pyme_epo_interno').setValue('');
				Ext.getCmp('txtCadenasPymes').setValue('');
			}
			fp.el.unmask();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

/*
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}
/*
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPDFTodo =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDFTodo');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDFTodo');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarPDFTodo = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDFTodo').enable();
		Ext.getCmp('btnGenerarPDFTodo').setIconClass('icoPdf');
	}


	var procesarGenerarPdfDetalle =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfDetalle');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfDetalle');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarPdfNotas =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfNotas');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfNotas');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarPdfModifica =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfModifica');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfModifica');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			if (gridTotales.isVisible()) {
				gridTotales.hide();
			}
			//Ext.getCmp('btnBajarPDF').hide();
			Ext.getCmp('btnBajarArchivo').hide();
			Ext.getCmp('btnBajarPDFTodo').hide();
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				var cm = grid.getColumnModel();
				cm.setHidden(cm.findColumnIndex('DF_FECHA_VENC_PYME'), true);
				cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
				cm.setHidden(cm.findColumnIndex('CG_RAZON_SOCIAL_IF'), true);
				cm.setHidden(cm.findColumnIndex('IC_FOLIO'), true);
				cm.setHidden(cm.findColumnIndex('CT_REFERENCIA'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO1'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO2'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO3'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO4'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
				cm.setHidden(cm.findColumnIndex('DETALLES'), true);
				cm.setHidden(cm.findColumnIndex('CS_CAMBIO_IMPORTE'), true);
				
				cm.setHidden(cm.findColumnIndex('NUMERO_DOCTO'), true);
				
				//cm.setHidden(cm.findColumnIndex('NETO_REC_PYME'), true);
				cm.setHidden(cm.findColumnIndex('BENEFICIARIO'), true);
				cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
				cm.setHidden(cm.findColumnIndex('RECIBIR_BENEF'), true);
				cm.setHidden(27, true);
				cm.setHidden(cm.findColumnIndex('DF_ENTREGA'), true);
				cm.setHidden(cm.findColumnIndex('CG_TIPO_COMPRA'), true);
				cm.setHidden(cm.findColumnIndex('CG_CLAVE_PRESUPUESTARIA'), true);
				cm.setHidden(cm.findColumnIndex('CG_PERIODO'), true);
				cm.setHidden(cm.findColumnIndex('CG_RAZON_SOCIAL'), true);
				cm.setHidden(cm.findColumnIndex('FECHA_REGISTRO_OPERACION'), true);
				cm.setHidden(cm.findColumnIndex('FECHA_AUTORIZACION_IF'), true);

				if (param.sFechaVencPyme != undefined && param.sFechaVencPyme != ""){
					cm.setHidden(cm.findColumnIndex('DF_FECHA_VENC_PYME'), false);
				}
				
				if(param.strPerfil ==='ADMIN PEMEX AMP') {
					cm.setHidden(cm.findColumnIndex('COPADE'), false);
					cm.setHidden(cm.findColumnIndex('CONTRATO'), false);
				}
				

				if (!layOutAserca){
					if(param.bTipoFactoraje) {
						cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
					}
					cm.setHidden(cm.findColumnIndex('CG_RAZON_SOCIAL_IF'), false);
					cm.setHidden(cm.findColumnIndex('IC_FOLIO'), false);
					cm.setHidden(cm.findColumnIndex('CT_REFERENCIA'), false);
					cm.setHidden(cm.findColumnIndex('DETALLES'), false);
					cm.setHidden(cm.findColumnIndex('CS_CAMBIO_IMPORTE'), false);
					
					if(param.bOperaFactorajeDistribuido || param.bOperaFactVencimientoInfonavit) {
						cm.setHidden(cm.findColumnIndex('BENEFICIARIO'), false);
						cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
						cm.setHidden(cm.findColumnIndex('RECIBIR_BENEF'), false);
					}
				}

				if(!ocultarDoctosAplicados){
					cm.setHidden(cm.findColumnIndex('NUMERO_DOCTO'), false);
				}

				var camposDinamicos = param.camp_dina1;
				if (camposDinamicos != undefined){
					var cont = 1;
					Ext.each(camposDinamicos, function(item, index, arrItems){
						if (item.IC_NO_CAMPO == "1" && cont == 1) {
							cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO1'), item.NOMBRE_CAMPO);
							cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO1'), item.NOMBRE_CAMPO);
							cm.setHidden(cm.findColumnIndex('CG_CAMPO1'), false);
						}
						if (item.IC_NO_CAMPO == "2" && cont == 2) {
							cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO2'), item.NOMBRE_CAMPO);
							cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO2'), item.NOMBRE_CAMPO);
							cm.setHidden(cm.findColumnIndex('CG_CAMPO2'), false);	
						}
						if (item.IC_NO_CAMPO == "3" && cont == 3) {
							cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO3'), item.NOMBRE_CAMPO);
							cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO3'), item.NOMBRE_CAMPO);
							cm.setHidden(cm.findColumnIndex('CG_CAMPO3'), false);
						}
						if (item.IC_NO_CAMPO == "4" && cont == 4) {
							cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO4'), item.NOMBRE_CAMPO);
							cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO4'), item.NOMBRE_CAMPO);
							cm.setHidden(cm.findColumnIndex('CG_CAMPO4'), false);
						}
						if (item.IC_NO_CAMPO == "5" && cont == 5) {
							cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO5'), item.NOMBRE_CAMPO);
							cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO5'), item.NOMBRE_CAMPO);
							cm.setHidden(cm.findColumnIndex('CG_CAMPO5'), false);
						}
						cont ++;
					});
				}

				if (param.esNumeroSIAFF){
					cm.setHidden(27, false);
				}
				if (param.banderaTablaDoctos == "1"){
					cm.setHidden(cm.findColumnIndex('DF_ENTREGA'), false);
					cm.setHidden(cm.findColumnIndex('CG_TIPO_COMPRA'), false);
					cm.setHidden(cm.findColumnIndex('CG_CLAVE_PRESUPUESTARIA'), false);
					cm.setHidden(cm.findColumnIndex('CG_PERIODO'), false);
				}

				if (param.bOperaFactConMandato){
					cm.setHidden(cm.findColumnIndex('CG_RAZON_SOCIAL'), false);
				}
				if (form.ic_estatus_docto == "26"){
					cm.setHidden(cm.findColumnIndex('FECHA_REGISTRO_OPERACION'), false);
				}
				if (param.bMostrarFechaAutorizacionIFInfoDoctos){
					cm.setHidden(cm.findColumnIndex('FECHA_AUTORIZACION_IF'), false);
				}
		//termina de ocultar/mostrar columnas
				Ext.getCmp('btnTotales').enable();
				Ext.getCmp('btnGenerarArchivo').enable();
				//Ext.getCmp('btnGenerarPDF').enable();
				/***** El siguiente bot�n se muestra s�lo si son 1000 registros o menos *****/
				if(store.getTotalCount() <= 1000){
					Ext.getCmp('btnGenerarPDFTodo').show();
					Ext.getCmp('btnGenerarPDFTodo').enable();
				} else{
					Ext.getCmp('btnGenerarPDFTodo').hide();
				}
				el.unmask();
			} else {
				Ext.getCmp('btnTotales').disable();
				Ext.getCmp('btnGenerarArchivo').disable();
				//Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('btnGenerarPDFTodo').disable();
				Ext.getCmp('btnGenerarPDFTodo').hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarNotaVarias = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.getCmp('winNotas').show();
			if (!gridNotaVarias.isVisible()){
				gridNotaVarias.show();
			}
			var elG = gridNotaVarias.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnPdfNotas').setDisabled(false);
				elG.unmask();
			}else{
				elG.mask('No encontro ning�n registro');
			}
		}
	}

	var procesarCatalogoNombreProvData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		if (jsonData.success){
			if (jsonData.total == "excede"){
				Ext.Msg.alert('Buscar','Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}else if (jsonData.total == "0"){
				Ext.Msg.alert('Buscar','No existe informaci�n con los criterios determinados');
				Ext.getCmp('fpWinBusca').getForm().reset();
				Ext.getCmp('fpWinBuscaB').getForm().reset();
				return;
			}
			Ext.getCmp('cmb_num_ne').focus();
		}
	}

	var procesarResumenTotalesData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var gridTotales = Ext.getCmp('gridTotales');
			var elTotal = gridTotales.getGridEl();
			var cm = gridTotales.getColumnModel();

			if(store.getTotalCount() > 0) {
				var storeGeneral = consultaData;
				var hayDoctosNegociablesMN=false,	hayDoctosNegociablesDOLARES = false;
				if(storeGeneral.getTotalCount() > 0) {
					var contMandante = 0;
					storeGeneral.each(function(registro){
						if (registro.get('IC_ESTATUS_DOCTO') == "2" && registro.get('IC_MONEDA') == "1")	{
							hayDoctosNegociablesMN = true;
						}
						if (registro.get('IC_ESTATUS_DOCTO') == "2" && registro.get('IC_MONEDA') == "54")	{
							hayDoctosNegociablesDOLARES=true;
						}
						
					});
				}
				var existenDocMN=false,existenDocNegociablesMN=false,existenNotasNegociablesMN=false,existenDocDolares=false,
					existenDocNegociablesDolares=false,existenNotasNegociablesDolares=false,mostrarSaldoMN=false,mostrarSaldoDolares=false;
				var montoMN_NC="0.0",montoDolares_NC="0.0",montoMN_Nego="0.0",montoDolares_Nego="0.0",montoMN="0.0",montoDolares="0.0";
				
				var jsonData = store.reader.jsonData.registros;
				
				Ext.each(jsonData, function(registro, index, arrItems){
					var reg = resumenTotalesData.getAt(index);
					if (registro.col4	== "D 1"		) {existenDocMN=true; montoMN=registro.col1; }
					else if (registro.col4	== "D 54"	) {existenDocDolares=true;  montoDolares=registro.col1; }
					else if (registro.col4	== "DN1"	) {existenDocNegociablesMN=true; mostrarSaldoMN=true; montoMN_Nego=registro.col1; }
					else if(registro.col4	== "DN54"	){existenDocNegociablesDolares=true; mostrarSaldoDolares=true; montoDolares_Nego=registro.col1; }
					else if(registro.col4	== "NC1"	) {existenNotasNegociablesMN=true;mostrarSaldoMN=true; montoMN_NC = registro.col1;}
					else if(registro.col4	== "NC54"	) {existenNotasNegociablesDolares=true;mostrarSaldoDolares=true; montoDolares_NC = registro.col1;}

					if(param.bOperaFactorajeNotaDeCredito && hayDoctosNegociablesDOLARES){
						mostrarSaldoDolares=true;
					}
					
					if (param.bOperaFactorajeNotaDeCredito && hayDoctosNegociablesMN)	{
						mostrarSaldoMN=true;
					}

					if (existenDocMN == true){
						reg.set('MONEDA',"MONTO TOTAL DOCUMENTOS " + registro.col3);
						existenDocMN=false;
					}
					if (existenDocDolares == true){
						reg.set('MONEDA',"MONTO TOTAL DOCUMENTOS " + registro.col3);
						existenDocDolares=false;
					}
					if(existenDocNegociablesMN == true){
						reg.set('MONEDA',"MONTO TOTAL DOCUMENTOS NEGOCIABLES " + registro.col3);
						existenDocNegociablesMN=false;
					}
					if(existenDocNegociablesDolares == true){
						reg.set('MONEDA',"MONTO TOTAL DOCUMENTOS NEGOCIABLES " + registro.col3);
						existenDocNegociablesDolares=false;
					}
					if(existenNotasNegociablesMN == true){
						reg.set('MONEDA',"MONTO TOTAL NOTAS DE CREDITO " + registro.col3);
						existenNotasNegociablesMN=false;
					}
					if(existenNotasNegociablesDolares == true){
						reg.set('MONEDA',"MONTO TOTAL NOTAS DE CREDITO " + registro.col3);
						existenNotasNegociablesDolares=false;
					}
				});
				if(mostrarSaldoMN == true){
				var saldoMN = 0;
					if(montoMN_Nego == "0.0"){ montoMN_Nego = montoMN;}
					if(parseFloat(montoMN_NC) > parseFloat(montoMN_Nego)){
						saldoMN = Math.abs((parseFloat(montoMN_Nego))-(parseFloat(montoMN_NC)));
					} else {
						saldoMN = ((parseFloat(montoMN_Nego))-(parseFloat(montoMN_NC)));
					}
					var reg = Ext.data.Record.create(['TOTAL_DOCUMENTOS', 'MONTO_DOCUMENTOS','MONTO_DOCUMENTOS_DESCONTAR','MONEDA']);
					store.add(
						new reg({
							TOTAL_DOCUMENTOS: '',
							MONTO_DOCUMENTOS: saldoMN,
							MONTO_DOCUMENTOS_DESCONTAR: '',
							//MONEDA:'Total Saldo Moneda Nacional Documentos Negociables'
							MONEDA:'TOTAL SALDO MONEDA NACIONAL DOCUMENTOS NEGOCIABLES'
						})
					);
				}
				if(mostrarSaldoDolares == true){
					var saldoDL = 0;
					if(montoDolares_Nego == "0.0"){ montoDolares_Nego = montoDolares;}
					if(parseFloat(montoDolares_NC) > parseFloat(montoDolares_Nego)){
						saldoDL = Math.abs((parseFloat(montoDolares_Nego))-(parseFloat(montoDolares_NC)));
					}else	{
						saldoDL = ((parseFloat(montoDolares_Nego))-(parseFloat(montoDolares_NC)));
					}
					var reg = Ext.data.Record.create(['TOTAL_DOCUMENTOS', 'MONTO_DOCUMENTOS','MONTO_DOCUMENTOS_DESCONTAR','MONEDA']);
					store.add(
						new reg({
							TOTAL_DOCUMENTOS: '',
							MONTO_DOCUMENTOS: saldoDL,
							MONTO_DOCUMENTOS_DESCONTAR: '',
							//MONEDA:'Total Saldo Dolar Americano Documentos Negociables'
							MONEDA:'TOTAL SALDO DOLAR AMERICANO DOCUMENTOS NEGOCIABLES'
						})
					);
				}
				if (!gridTotales.isVisible()) {
					gridTotales.show();
					gridTotales.el.dom.scrollIntoView();
				}
				elTotal.unmask();
				Ext.getCmp('btnTotales').enable();
			}else {
				Ext.getCmp('btnTotales').disable();
				elTotal.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	function procesarMuestraPanelAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var ventana = Ext.getCmp('winAcuse');
			if (ventana) {
				ventana.show();
			}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				bodyStyle: 'padding: 10px',
				width: 587,
				height: 370,
				id: 'winAcuse',
				closeAction: 'hide',
				items: [fpAcuse],
				title: '<div align="center">Datos del Acuse</div>',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',{xtype: 'button',	text: 'Imprimir PDF',		id: 'btnPdfAcuse', disabled: true}
										,'-',{xtype: 'button',	text: 'Cerrar',id: 'btnClose'}	]
				}
				}).show();	
			}
			Ext.getCmp('nMn').body.update('&nbsp');
			Ext.getCmp('nDl').body.update('&nbsp');
			Ext.getCmp('totMn').body.update('&nbsp');
			Ext.getCmp('totDl').body.update('&nbsp');
			Ext.getCmp('disAcuse').body.update('&nbsp');
			Ext.getCmp('disFechaCarga').body.update('&nbsp');
			Ext.getCmp('disHoraCarga').body.update('&nbsp');
			Ext.getCmp('disUsuario').body.update('&nbsp');
			var dataAcuse = Ext.util.JSON.decode(response.responseText);
			if (dataAcuse.datosAcuse != undefined)	{
				Ext.each(dataAcuse.datosAcuse, function(item, index, arrItems){
					Ext.getCmp('nMn').body.update('<div align="right">'+item.IN_TOTAL_DOCTO_MN+'</div>');
					Ext.getCmp('nDl').body.update('<div align="right">'+item.IN_TOTAL_DOCTO_DL+'</div>');
					Ext.getCmp('totMn').body.update('<div align="right">'+Ext.util.Format.number(item.FN_TOTAL_MONTO_MN, '$0,0.00')+'</div>');
					Ext.getCmp('totDl').body.update('<div align="right">'+Ext.util.Format.number(item.FN_TOTAL_MONTO_DL, '$0,0.00')+'</div>');
					Ext.getCmp('disAcuse').body.update(opts.params.acuse);
					Ext.getCmp('disFechaCarga').body.update(item.FECHA_CARGA);
					Ext.getCmp('disHoraCarga').body.update(item.HORA_CARGA);
				});
			}
			if (dataAcuse.usuario != undefined)	{
				Ext.getCmp('disUsuario').body.update(dataAcuse.usuario);
			}
			if (dataAcuse.urlArchivo != undefined){
				Ext.getCmp('btnPdfAcuse').setDisabled(false);
				Ext.getCmp('btnPdfAcuse').setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = dataAcuse.urlArchivo;
					forma.submit();
				});
			}
			
			Ext.getCmp('btnClose').setHandler(
				function(boton, evento) {
					Ext.getCmp('winAcuse').hide();
				}
			);
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarMuestraDetalle(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var data = Ext.util.JSON.decode(response.responseText);

			var camposDinamicosDetalle = data.dinamicosDetalle;
			if (camposDinamicosDetalle != undefined){
				var datosDinamicos = data.datos_detalle;
				var count = 1;
				if (datosDinamicos != undefined){
					gridDetalleData.loadData(datosDinamicos);
					if (!gridDetalle.isVisible()) {
						gridDetalle.show();
					}
				}
				
				var cm = gridDetalle.getColumnModel();
				Ext.each(camposDinamicosDetalle, function(item, index, arrItems){
					if (item.IC_NO_CAMPO == "1"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO1'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO1'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO1'), false);}
					if (item.IC_NO_CAMPO == "2"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO2'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO2'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO2'), false);}
					if (item.IC_NO_CAMPO == "3"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO3'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO3'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO3'), false);}
					if (item.IC_NO_CAMPO == "4"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO4'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO4'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO4'), false);}
					if (item.IC_NO_CAMPO == "5"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO5'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO5'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO5'), false);}
					if (item.IC_NO_CAMPO == "6"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO6'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO6'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO6'), false);}
					if (item.IC_NO_CAMPO == "7"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO7'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO7'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO7'), false);}
					if (item.IC_NO_CAMPO == "8"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO8'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO8'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO8'), false);}
					if (item.IC_NO_CAMPO == "9"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO9'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO9'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO9'), false);}
					if (item.IC_NO_CAMPO == "10"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO10'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO10'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO10'), false);}
				});
			}
			
			var ventana = Ext.getCmp('winDetalle');
			if (ventana) {
				Ext.getCmp('btnPdfDetalle').enable();
				Ext.getCmp('btnBajarPdfDetalle').hide();
				ventana.show();
			}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				layout: 'form',
				width: 800,
				autoHeight: true,
				//height: 400,
				id: 'winDetalle',
				closeAction: 'hide',
				items: [fpDetalle,NE.util.getEspaciador(2),gridDetalle],
				title: 'Detalles del Documento',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',{xtype: 'button',	text: 'Cerrar',	id: 'btnCloseDet'},
										'-',{xtype: 'button',	text: 'Generar PDF',	id: 'btnPdfDetalle'},
										{xtype: 'button',	text: 'Bajar PDF',	id: 'btnBajarPdfDetalle',hidden: true}	]
				}
				}).show();
			}

			Ext.getCmp('disDocto').body.update('&nbsp');
			Ext.getCmp('disEmision').body.update('&nbsp');
			Ext.getCmp('disVto').body.update('&nbsp');
			Ext.getCmp('disImporte').body.update('&nbsp');
			Ext.getCmp('disMoneda').body.update('&nbsp');

			Ext.getCmp('disNombre').body.update('&nbsp');
			Ext.getCmp('disRfc').body.update('&nbsp');
			Ext.getCmp('disCalle').body.update('&nbsp');
			Ext.getCmp('disColonia').body.update('&nbsp');
			Ext.getCmp('disCp').body.update('&nbsp');
			Ext.getCmp('disEstado').body.update('&nbsp');
			Ext.getCmp('disDelomun').body.update('&nbsp');

			if (data.datos_doctos != undefined)	{
				Ext.each(data.datos_doctos, function(item, index, arrItems){
					Ext.getCmp('disNombre').body.update('<div><b>'+item.CG_RAZON_SOCIAL+'</b></div>');
					Ext.getCmp('disRfc').body.update(item.CG_RFC);
					Ext.getCmp('disCalle').body.update(item.CG_CALLE);
					Ext.getCmp('disColonia').body.update(item.CG_COLONIA);
					Ext.getCmp('disCp').body.update(item.CN_CP);
					Ext.getCmp('disEstado').body.update(item.CG_ESTADO);
					Ext.getCmp('disDelomun').body.update(item.CG_MUNICIPIO);

					Ext.getCmp('disDocto').body.update('<div align="right">'+item.IG_NUMERO_DOCTO+'</div>');
					Ext.getCmp('disEmision').body.update('<div align="right">'+item.FECHA_DOCTO+'</div>');
					Ext.getCmp('disVto').body.update('<div align="right">'+item.FECHA_VENC+'</div>');
					Ext.getCmp('disImporte').body.update('<div align="right">'+Ext.util.Format.number(item.FN_MONTO, '$0,0.00')+'</div>');
					Ext.getCmp('disMoneda').body.update('<div align="right">'+item.MONEDA);

					var campo1 = item.CG_CAMPO1;
					var campo2 = item.CG_CAMPO2;
					var campo3 = item.CG_CAMPO3;
					var campo4 = item.CG_CAMPO4;
					var campo5 = item.CG_CAMPO5;
					if (Ext.isEmpty(campo1)){campo1 = '&nbsp;'}
					if (Ext.isEmpty(campo2)){campo2 = '&nbsp;'}
					if (Ext.isEmpty(campo3)){campo3 = '&nbsp;'}
					if (Ext.isEmpty(campo4)){campo4 = '&nbsp;'}
					if (Ext.isEmpty(campo5)){campo5 = '&nbsp;'}
					var camposDinamicos = param.camp_dina1;
					var contenedorIzq = Ext.getCmp('Izquierda');
					if (camposDinamicos != undefined){
						var config = [];
						var cont = 1;
						Ext.each(camposDinamicos, function(item, index, arrItems){
							if (item.IC_NO_CAMPO == "1") {
								config = {xtype: 'panel',name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),html:	'&nbsp;&nbsp;&nbsp;&nbsp;<b>'+item.NOMBRE_CAMPO + ' ' + campo1+'</b>'}
							}
							if (item.IC_NO_CAMPO == "2") {
								config = {xtype: 'panel',name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),html:	'&nbsp;&nbsp;&nbsp;&nbsp;<b>'+item.NOMBRE_CAMPO + ' ' + campo2+'</b>'}
							}
							if (item.IC_NO_CAMPO == "3") {
								config = {xtype: 'panel',name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),html: '&nbsp;&nbsp;&nbsp;&nbsp;<b>'+item.NOMBRE_CAMPO +' '+ campo3+'</b>'}
							}
							if (item.IC_NO_CAMPO == "4") {
								config = {xtype: 'panel',name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),html: '&nbsp;&nbsp;&nbsp;&nbsp;<b>'+item.NOMBRE_CAMPO + ' ' + campo4+'</b>'}
							}
							if (item.IC_NO_CAMPO == "5") {
								config = {xtype: 'panel',name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),html: '&nbsp;&nbsp;&nbsp;&nbsp;<b>'+item.NOMBRE_CAMPO + ' ' + campo5+'</b>'}
							}
							contenedorIzq.add(config);
							contenedorIzq.insert(8+index,config);
							contenedorIzq.doLayout();
						});
					}
				});
			}
			
			
			Ext.getCmp('btnPdfDetalle').setHandler(
				function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta01ext_det_pdf.jsp',
						params: Ext.apply({ic_docto:	opts.params.ic_docto	}),
						callback: procesarGenerarPdfDetalle
					});
				}
			);

			Ext.getCmp('btnCloseDet').setHandler(
				function(boton, evento) {
					Ext.getCmp('winDetalle').hide();
				}
			);

		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarMuestraPanelModifica(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var dataModifica = Ext.util.JSON.decode(response.responseText);
			
			if (dataModifica.datosModifica != undefined)	{
				gridModificaData.loadData(dataModifica.datosModifica);
			}

			var ventana = Ext.getCmp('winModifica');
			if (ventana) {
				Ext.getCmp('btnPdfModifica').enable();
				Ext.getCmp('btnBajarPdfModifica').hide();
				ventana.show();
				var storeGrid = gridModificaData.data;
				if(storeGrid.length < 1) {
					gridModifica.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					Ext.getCmp('btnPdfModifica').disable();
				}
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN

	var muestraPanelModifica = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var ic_documen = registro.get('IC_DOCUMENTO');
		var cg_pyme_epo_int = registro.get('CG_PYME_EPO_INTERNO');
		var nombreProv = registro.get('CG_RAZON_SOCIAL_PYME');
		var nombre_if = registro.get('CG_RAZON_SOCIAL_IF');
		var icDocto = registro.get('IG_NUMERO_DOCTO');
		var fecha_emis =	Ext.util.Format.date(registro.get("DF_FECHA_DOCTO"),'d/m/Y');
		var fecha_venc = Ext.util.Format.date(registro.get("DF_FECHA_VENC"),'d/m/Y');
		var nombreMoneda = registro.get('CD_NOMBRE');
		var fnMonto = registro.get('FN_MONTO');
		var estatusDocto = registro.get('CD_DESCRIPCION');
		//
		gridModificaData.loadData('');
		//
		Ext.getCmp('disProv').setValue(cg_pyme_epo_int + "   " +nombreProv);
		Ext.getCmp('disIfM').setValue(nombre_if);
		Ext.getCmp('disDoctoM').setValue(icDocto);
		Ext.getCmp('disFecEmiM').setValue(fecha_emis);
		Ext.getCmp('disFecVencM').setValue(fecha_venc);
		Ext.getCmp('disMonedaM').setValue(nombreMoneda);
		Ext.getCmp('disMontoM').setValue(Ext.util.Format.number(fnMonto,'$ 0,0.00'));
		Ext.getCmp('disEstatusM').setValue(estatusDocto);
		var ventana = Ext.getCmp('winModifica');
		if (ventana) {
			ventana.hide();
		}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				width: 803,
				//height: 480,
				autoHeight:true,
				id: 'winModifica',
				closeAction: 'hide',
				items: [fpModifica,NE.util.getEspaciador(10),gridModifica],
				title: 'Modificaci�n montos y/o Fechas',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',{xtype: 'button',text: 'Generar PDF',	id: 'btnPdfModifica'},{xtype: 'button',text: 'Bajar PDF',id: 'btnBajarPdfModifica',hidden: true}
									,'-',{xtype: 'button',	text: 'Cerrar',id: 'btnCloseMod'}]
				}
			});
		}
		
		Ext.getCmp('btnPdfModifica').setHandler(
			function(boton, evento) {
			boton.disable();
			boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '13consulta01ext_mod_pdf.jsp',
					params: Ext.apply({ic_docto: ic_documen, cg_pyme_epo_interno:cg_pyme_epo_int,	nombreProv:nombreProv, ic_if: nombre_if,ig_numero_docto:icDocto,fch_emision:fecha_emis,fch_venc:fecha_venc,moneda:nombreMoneda,
											monto:fnMonto,estatus:estatusDocto}),
					callback: procesarGenerarPdfModifica
				});
			}
		);

		Ext.getCmp('btnCloseMod').setHandler(
			function(boton, evento) {
				Ext.getCmp('winModifica').hide();
			}
		);

		Ext.Ajax.request({url: '13consulta01ext.data.jsp',params: Ext.apply(
		{informacion: "obtenModifica",ic_docto: ic_documen	}),callback: procesarMuestraPanelModifica});

	}

	var muestraPanelNotas = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		notasA.notaAplica = false;
		notasA.notaCredito = "";
		notasA.notasDocto = false;
		notasA.ic_documento = "";
		if(registro.get('NOTA_MULTIPLE') == "N" || registro.get('NOTA_MULTIPLE_DOCTO') == "N")	{
			if(registro.get('NOTA_MULTIPLE') == "N")	{
				//	verNotaDeCredito();
				notasA.notaAplica = true;
				notasA.notaCredito = registro.get('IC_DOCUMENTO');
				
			}else	{
				//		verNotasDocumento();
				notasA.notasDocto = true;
				notasA.ic_documento = registro.get('IC_DOCUMENTO');
			}
		}

		var ventana = Ext.getCmp('winNotas');
		if (ventana) {
			Ext.getCmp('btnBajarPdfNotas').hide();
		}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				x: 200,
				width: 800,
				height: 360,
				id: 'winNotas',
				closeAction: 'hide',
				items: [gridNotaVarias],
				title: 'Documentos Aplicados a Nota de Cr�dito',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',{xtype: 'button',text: 'Generar PDF',	id: 'btnPdfNotas',	disabled: true},{xtype: 'button',text: 'Bajar PDF',id: 'btnBajarPdfNotas',hidden: true}]
				}
			});
		}

		Ext.getCmp('btnPdfNotas').setHandler(
			function(boton, evento) {
			boton.disable();
			boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '13consulta01ext_not_pdf.jsp',
					params: Ext.apply({icDocumento: notasA.ic_documento,searchNotaAplicada: notasA.notaAplica,icNotaCredito: notasA.notaCredito,searchNotasDocto: notasA.notasDocto}),
					callback: procesarGenerarPdfNotas
				});
			}
		);
		gridNotaVariasData.load();
	}

	var muestraDetalle = function(grid, rowIndex, colIndex, item, event)	{
		gridDetalleData.loadData('');
		gridDetalle.hide();
		var registro = grid.getStore().getAt(rowIndex);
		//DETALLES
		var ic_document = registro.get('IC_DOCUMENTO');
		var contenedorIzq = Ext.getCmp('Izquierda');
		var ciclo = 0;
		for (ciclo=0; ciclo<5; ciclo ++){
			var dina = Ext.getCmp('dina'+ciclo.toString());
			if (dina != undefined) {contenedorIzq.remove(dina);}
		}
		Ext.Ajax.request({url: '13consulta01ext.data.jsp',params: Ext.apply({informacion: "obtenDetalles",ic_docto:	ic_document}),callback: procesarMuestraDetalle});
	}

	var muestraPanelAcuse = function(grid, rowIndex, colIndex, item, event) {
			var registro = grid.getStore().getAt(rowIndex);
			cc_acuse = registro.get('CC_ACUSE');
			var ventana = Ext.getCmp('winAcuse');
			if (ventana){
				ventana.hide();
			}
			Ext.Ajax.request({url: '13consulta01ext.data.jsp',params: Ext.apply({informacion: "obtenAcuse",acuse: cc_acuse}),callback: procesarMuestraPanelAcuse});
	}
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT



    var catTipoFactorajeData = new Ext.data.JsonStore({
	root : 'registros',
	fields : ['clave', 'descripcion', 'loadMsg'],
	url : '13consulta01ext.data.jsp',
	baseParams: {
	    informacion: 'catTipoFactorajeData'
	},
	totalProperty : 'total',
	autoLoad: false,
	listeners: {
	    exception: NE.util.mostrarDataProxyError,
	    beforeload: NE.util.initMensajeCargaCombo
	}
    });
	
    var storeValidaEPO = new Ext.data.SimpleStore({
    fields: ['clave', 'descripcion'],
    data : [
	['S','Si'],
	['N','No']
    ]
    });

	var catalogoIfData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIf'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCambioEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,	//	el valor inicial es false
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,//	El valor inicial es false
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoNombreProvData = new Ext.data.JsonStore({
		id: 'catalogoNombreStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoBuscaAvanzada'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoNombreProvData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoCamposAdicionalesData = new Ext.data.JsonStore({
		id: 'catalogoCamposAdicionalesDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCamposAdicionales'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoCamposAdicionales_2_Data = new Ext.data.JsonStore({
		id: 'catalogoCamposAdicionales_2_DataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCamposAdicionales_2'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'IC_DOCUMENTO'},
			{name: 'CG_RAZON_SOCIAL_PYME'},
			{name: 'IC_EPO'},
			{name: 'CC_ACUSE'},
			{name: 'IG_NUMERO_DOCTO'},              //Num docto.
			{name: 'DF_FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CD_NOMBRE'},                    //Moneda
			{name: 'FN_MONTO', type: 'float'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'FN_PORC_ANTICIPO'},
			{name: 'FN_MONTO_DSCTO', type: 'float'},
			{name: 'CD_DESCRIPCION'},
			{name: 'CG_RAZON_SOCIAL_IF'},	//Nombre if
			{name: 'IC_FOLIO'},
			{name: 'IC_ESTATUS_DOCTO'},
			{name: 'IC_MONEDA'},
			{name: 'CG_PYME_EPO_INTERNO'},
			{name: 'CT_REFERENCIA'},
			{name: 'CS_CAMBIO_IMPORTE'},
			{name: 'DETALLES'},
			{name: 'CG_CAMPO1'},
			{name: 'CG_CAMPO2'},
			{name: 'CG_CAMPO3'},
			{name: 'CG_CAMPO4'},
			{name: 'CG_CAMPO5'},
			{name: 'BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEF', type: 'float'},
			{name: 'NETO_REC_PYME', type: 'float'},
			{name: 'DF_FECHA_VENC_PYME', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_ENTREGA', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CG_TIPO_COMPRA'},
			{name: 'CG_CLAVE_PRESUPUESTARIA'},
			{name: 'CG_PERIODO'},
			{name: 'CG_RAZON_SOCIAL'},		//Mandante
			{name: 'TIPO_FACTORAJE'},
			{name: 'FECHA_REGISTRO_OPERACION', type: 'date', dateFormat: 'd/m/Y'},
			//{name: 'FECHA_AUTORIZACION_IF', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_AUTORIZACION_IF'},
			{name: 'NUMERO_DOCTO'},
			{name: 'NOTA_SIMPLE'},
			{name: 'NOTA_SIMPLE_DOCTO'},
			{name: 'NOTA_MULTIPLE'},
			{name: 'NOTA_MULTIPLE_DOCTO'},
			{name: 'MUESTRA_VISOR'},
			{name: 'VALIDACION_JUR'},
			{name: 'CONTRATO'}, 
			{name: 'COPADE'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
					Ext.apply(options.params, {
									cg_pyme_epo_interno:	form.cg_pyme_epo_interno,
									ic_pyme:	form.ic_pyme,
									ic_if:	form.ic_if,
									ic_estatus_docto:	form.ic_estatus_docto,
									ic_moneda:	form.ic_moneda,
									cc_acuse:	form.cc_acuse,
									no_docto:	form.no_docto,
									df_fecha_docto_de:	form.df_fecha_docto_de,
									df_fecha_docto_a:	form.df_fecha_docto_a,
									df_fecha_venc_de:	form.df_fecha_venc_de,
									df_fecha_venc_a:	form.df_fecha_venc_a,
									fn_monto_de:	form.fn_monto_de,
									fn_monto_a:	form.fn_monto_a,
									ord_proveedor:	form.ord_proveedor,
									ord_if:	form.ord_if,
									ord_monto:	form.ord_monto,
									ord_fec_venc:	form.ord_fec_venc,
									ord_estatus:	form.ord_estatus,
									camp_dina1:	form.camp_dina1,
									camp_dina2:	form.camp_dina2,
									operaFirmaM:	form.operaFirmaM,
									operaNotasC:	form.operaNotasC,
									numero_siaff:	form.numero_siaff,
									sFechaVencPyme:param.sFechaVencPyme,
									cmbFactoraje:form.cmbFactoraje, 
									validaDoctoEPO:form.validaDoctoEPO,
									contrato:form.contrato,
									copade:form.copade
								});
			}		
			},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta01ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'TOTAL_DOCUMENTOS', type: 'float', mapping: 'col0'},
			{name: 'MONTO_DOCUMENTOS', type: 'float', mapping: 'col1'},
			{name: 'MONTO_DOCUMENTOS_DESCONTAR', type: 'float', mapping: 'col2'},
			{name: 'MONEDA', mapping: 'col3'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarResumenTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarResumenTotalesData(null, null, null);
				}
			}
		}
	});

	var gridModificaData = new Ext.data.JsonStore({
		fields:	[{name:'FECHA_CAMBIO',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'FN_MONTO_NUEVO',type: 'float'},
					{name: 'FN_MONTO_ANTERIOR',	type: 'float'},
					{name: 'FEC_VENC_NUEVA',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'FEC_VENC_ANTERIOR',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'CT_CAMBIO_MOTIVO'},
					{name: 'CC_ACUSE'}],
		data:		[{'CC_ACUSE':'','CT_CAMBIO_MOTIVO':'','FEC_VENC_ANTERIOR':'','FEC_VENC_NUEVA':'','FECHA_CAMBIO':'','FN_MONTO_ANTERIOR':0,'FN_MONTO_NUEVO':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridNotaVariasData = new Ext.data.GroupingStore({
		root : 'registros',
		url : '/nafin/13descuento/13forma1ext.data.jsp',
		baseParams: {
			informacion: 'obtenNotaVarias'
		},
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields: [
			{name: 'NOTA_MONTO_CONCAT'},
			{name: 'IG_NUMERO_DOCTO_CREDITO'},
			{name: 'IG_NUMERO_DOCTO_DOCTO'},
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_MONTO',type: 'float'},
			{name: 'MONEDA'},
			{name: 'MONTO_DOCTO',type: 'float'},
			{name: 'MONTO_APLICA',	type: 'float'},
			{name: 'SALDO_DOCTO',	type: 'float'},
			{name: 'TIPO_FACTORAJE'}
      ]
		}),
		groupField: 'NOTA_MONTO_CONCAT',	//sortInfo:{field: 'NOTA_MONTO_CONCAT', direction: "ASC"},
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
										Ext.apply(options.params, {icDocumento: notasA.ic_documento,searchNotaAplicada: notasA.notaAplica,icNotaCredito: notasA.notaCredito,searchNotasDocto: notasA.notasDocto});
									}
							},
			load: procesarNotaVarias,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarNotaVarias(null, null, null);
				}
			}
		}
	});

	var gridDetalleData = new Ext.data.JsonStore({
		fields: [{name: 'CG_CAMPO1'},{name: 'CG_CAMPO2'},{name: 'CG_CAMPO3'},{name: 'CG_CAMPO4'},{name: 'CG_CAMPO5'},{name: 'CG_CAMPO6'},{name: 'CG_CAMPO7'},{name: 'CG_CAMPO8'},{name: 'CG_CAMPO9'},{name: 'CG_CAMPO10'}],
		data: [{'CG_CAMPO1':'','CG_CAMPO2':'','CG_CAMPO3':'','CG_CAMPO4':'','CG_CAMPO5':'','CG_CAMPO6':'','CG_CAMPO7':'','CG_CAMPO8':'','CG_CAMPO9':'','CG_CAMPO10':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridModifica = new Ext.grid.GridPanel({
		id: 'gridModifica',
		store: gridModificaData,
		columns: [
			{header: 'Fecha Movimiento',tooltip: 'Fecha Movimiento',	dataIndex: 'FECHA_CAMBIO',	sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Monto',tooltip: 'Monto',	dataIndex: 'FN_MONTO_NUEVO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto Anterior',tooltip: 'Monto Anterior',	dataIndex: 'FN_MONTO_ANTERIOR',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Fecha de Vencimiento',tooltip: 'Fecha de Vencimiento',	dataIndex: 'FEC_VENC_NUEVA',	sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Fecha de Vencimiento Anterior',tooltip: 'Fecha de Vencimiento Anterior',	dataIndex: 'FEC_VENC_ANTERIOR',	sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Causa',tooltip: 'Causa',	dataIndex: 'CT_CAMBIO_MOTIVO',	width: 200,	align: 'left'},
			{header: 'Acuse',tooltip: 'Acuse',	dataIndex: 'CC_ACUSE',	width: 150,	align: 'left'}
		],
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 200,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});

	var gridNotaVarias = new Ext.grid.GridPanel({
		id: 'gridNotaVarias',
		store: gridNotaVariasData,
		columns: [
			{header: 'No. Nota',tooltip: 'No. Nota',	dataIndex: 'NOTA_MONTO_CONCAT',	width: 250,	align: 'left', hidden:true, hideable:false},
			{header: 'No. Docto.',tooltip: 'No. Docto.',	dataIndex: 'IG_NUMERO_DOCTO_DOCTO',	width: 120,	align: 'left'},
			{header: 'Fecha de Emisi�n',tooltip: 'Fecha de Emisi�n',	dataIndex: 'FECHA_EMISION',	sortable : true, width : 120, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Moneda',tooltip: 'Moneda',	dataIndex: 'MONEDA',	width: 140,	align: 'left'},
			{header: 'Monto Docto',tooltip: 'Monto Docto',	dataIndex: 'MONTO_DOCTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto Aplicado',tooltip: 'Monto Aplicado',	dataIndex: 'MONTO_APLICA',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Saldo Docto',tooltip: 'Saldo Docto',	dataIndex: 'SALDO_DOCTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Tipo Factoraje',tooltip: 'Tipo Factoraje',	dataIndex: 'TIPO_FACTORAJE',	width: 140,	align: 'left'}
		],
		stripeRows: true,
		view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),
		deferRowRender: false,
		loadMask: true,
		monitorResize: true,
		height: 300,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});

	var gridDetalle = new Ext.grid.GridPanel({
		store: gridDetalleData,
		//viewConfig: {forceFit: true},
		columns: [
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO1',	width: 100,	align: 'center', hidden: false},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO2',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO3',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO4',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO5',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO6',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO7',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO8',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO9',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO10',width: 100,	align: 'center', hidden: true}
		],
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 250,
		width: 790,
		frame: false,
		hidden: true
	});

	var gridTotales = new Ext.grid.GridPanel({
		store: resumenTotalesData,
		id: 'gridTotales',
		hidden: true,
		columns: [
			{
				header: '&nbsp;',
				dataIndex: 'MONEDA',
				align: 'left',width: 360, resizable: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				header: 'Registros',
				dataIndex: 'TOTAL_DOCUMENTOS',
				width: 110,	align: 'right',renderer: Ext.util.Format.numberRenderer('0,000'), hidden: false, resizable: false
			},{
				header: 'Monto',
				dataIndex: 'MONTO_DOCUMENTOS',
				width: 201,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: false, resizable: false
			},{
				header: 'Monto a Descontar',
				dataIndex: 'MONTO_DOCUMENTOS_DESCONTAR',
				width: 201,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: false, resizable: false
			}
		],
		//view: new Ext.grid.GridView({markDirty: false}),
		view: new Ext.grid.GridView({markDirty: false, forceFit: true}),
		width: 940,
		height: 240,
		loadMask: true,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	});

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header: 'Nombre del proveedor', tooltip: 'Nombre del proveedor',	dataIndex: 'CG_RAZON_SOCIAL_PYME',
				sortable: true, width: 200, resizable: true, hidden: false
			},{
				header: 'N�mero del documento', tooltip: 'N�mero del documento',	dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true, width: 150, resizable: true, hidden: false,align: 'center'
			},{
				xtype:	'actioncolumn',
				header : 'N�mero de Acuse', tooltip: 'N�mero de Acuse',	dataIndex : 'CC_ACUSE',
				width:	150,	align: 'center', hidden: false, hideable: false,
				renderer: function(value, metadata, record, rowindex, colindex, store){
								return (record.get('CC_ACUSE'));
							 },
				items: [{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store){
						if(registro.get('CC_ACUSE') != '' && registro.get('MUESTRA_VISOR') == 'S'){
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
						} else{
								return value;
						}
					},
					handler: function(grid, rowIndex, colIndex){
						var rec = consultaData.getAt(rowIndex);
						VisorAcusesDeCarga(rec);
					}
				}]
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	dataIndex : 'DF_FECHA_DOCTO',
				sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: false
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	dataIndex : 'DF_FECHA_VENC',
				sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: false
			},{
				header : 'Fecha Vencimiento Proveedor', tooltip: 'Fecha Vencimiento Proveedor',	dataIndex : 'DF_FECHA_VENC_PYME',
				sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: true
			},{
				header : 'Moneda', tooltip: 'Moneda',	dataIndex : 'CD_NOMBRE',
				sortable : true, width : 150, hidden: false,align: 'center'
			},{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true, hideable: false,align: 'center'
			},{
				header : 'Monto', tooltip: 'Monto del Documento',	dataIndex : 'FN_MONTO',
				sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: false, hideable: false
			},{
				header : 'Porcentaje de Descuento', tooltip: 'Porcentaje de Descuento',	dataIndex : 'FN_PORC_ANTICIPO',
				sortable : true, width : 150,	align: 'right', resizable: true, hidden: false,hideable: false,
				renderer:function(value, metaData, registro, rowIndex, colIndex, store) {
								var dblPorciento = 0;
								if ( registro.get('IC_ESTATUS_DOCTO') == ("2") || registro.get('IC_ESTATUS_DOCTO') == ("5") || registro.get('IC_ESTATUS_DOCTO') == ("6") || 
										registro.get('IC_ESTATUS_DOCTO') == ("7") || registro.get('IC_ESTATUS_DOCTO') == ("9") || registro.get('IC_ESTATUS_DOCTO') == ("10") ){
										if (registro.get('IC_MONEDA') == "1"){
											dblPorciento = parseFloat(param.strAforo) * 100;
										}else if (registro.get('IC_MONEDA') == "54"){
											dblPorciento = parseFloat(param.strAforoDL) * 100;
										}
								}else{
									dblPorciento = registro.get('FN_PORC_ANTICIPO');
								}
								value = dblPorciento;
								if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
								if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
								if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
								if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
								if(registro.get('CS_DSCTO_ESPECIAL') == "A")	{value = 100;}
								
								return Ext.util.Format.number(value,'0%');
							}
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	dataIndex : 'FN_MONTO_DSCTO',  //Calculado
				sortable : true, width : 100,	align: 'right', hidden: false,hideable: false,
				renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
								var dblPorciento = 0;
								var dblMontoDescuento = 0;
								if ( registro.get('IC_ESTATUS_DOCTO') == ("2") || registro.get('IC_ESTATUS_DOCTO') == ("5") || registro.get('IC_ESTATUS_DOCTO') == ("6") || 
										registro.get('IC_ESTATUS_DOCTO') == ("7") || registro.get('IC_ESTATUS_DOCTO') == ("9") || registro.get('IC_ESTATUS_DOCTO') == ("10") ){
										if (registro.get('IC_MONEDA') == "1"){
											dblPorciento = parseFloat(param.strAforo) * 100;
										}else if (registro.get('IC_MONEDA') == "54"){
											dblPorciento = parseFloat(param.strAforoDL) * 100;
										}
								}else{
									dblPorciento = registro.get('FN_PORC_ANTICIPO');
								}
								dblMontoDescuento = registro.get('FN_MONTO') * (dblPorciento / 100); //FN_MONTO_DSCTO;

								if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{dblMontoDescuento=registro.get('FN_MONTO');}
								if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{dblMontoDescuento=registro.get('FN_MONTO');}
								if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{dblMontoDescuento=registro.get('FN_MONTO');}
								if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{dblMontoDescuento=registro.get('FN_MONTO');}
								if(registro.get('CS_DSCTO_ESPECIAL') == "A")	{dblMontoDescuento=registro.get('FN_MONTO');}

								if (!layOutAserca){
									value = dblMontoDescuento;
								}else{
									value = registro.get('FN_MONTO');
								}

							return Ext.util.Format.number(value, '$0,0.00');
							}
			},{
				header : 'Estatus', tooltip: 'Estatus',	dataIndex : 'CD_DESCRIPCION',
				sortable : true, width : 200, hidden: false,hideable: false, align: 'center'
			},{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero',	dataIndex: 'CG_RAZON_SOCIAL_IF',
				sortable: true,	width: 200,	resizable: true, hidden: true, hideable: false,align: 'center'
			},{
				header: 'N�mero de solicitud', tooltip: 'N�mero de solicitud',	dataIndex: 'IC_FOLIO',
				sortable: true,	width: 100,	resizable: true, hidden: true, hideable: false,align: 'center'
			},{
				header : 'Referencia', tooltip: 'Referencia',	dataIndex : 'CT_REFERENCIA',
				width : 100, sortable : true, hidden: true,hideable: false
			},{
				header : '', tooltip: '',	dataIndex : 'CG_CAMPO1',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false,align: 'center'
			},{
				header : '', tooltip: '',	dataIndex : 'CG_CAMPO2',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false,align: 'center',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (layOutAserca)	{
										return Ext.util.Format.number(value, '$0,0.00');
									} else	{
										return value;
									}
								}
			},{
				header : '', tooltip: '',	dataIndex : 'CG_CAMPO3',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false,align: 'center'
			},{
				header : '', tooltip: '',	dataIndex : 'CG_CAMPO4',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (layOutAserca)	{
										return Ext.util.Format.number(value, '$0,0.00');
									} else	{
										return value;
									}
								}
			},{
				header : '', tooltip: '',	dataIndex : 'CG_CAMPO5',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false,align: 'center',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (layOutAserca)	{
										return Ext.util.Format.number(value, '$0,0.00');
									} else	{
										return value;
									}
								}
			},{
				xtype:	'actioncolumn',
				header : 'Detalles', tooltip: 'Detalles',	dataIndex : 'DETALLES',
				width:	100,	align: 'center',  hidden: true,hideable: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('DETALLES')	!=	0) {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraDetalle
					}
				]
			},{
				xtype:	'actioncolumn',
				header : 'Modificaci�n Montos y/o Fechas', tooltip: 'Modificaci�n Montos y/o Fechas',	dataIndex : 'CS_CAMBIO_IMPORTE',
				width:	130,	align: 'center', resizable: true, hidden: true,hideable: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('CS_CAMBIO_IMPORTE')	==	'S') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraPanelModifica
					}
				]
			},{
				xtype:	'actioncolumn',
				header : 'Doctos Aplicados a Nota de Credito', tooltip: 'Doctos Aplicados a Nota de Credito',	dataIndex : 'NUMERO_DOCTO',
				width:	200,	align: 'center', resizable: true, hideable: false,	hidden: true,
				renderer:function(value, metadata, registro, rowindex, colindex, store) {
								if (registro.get('NOTA_SIMPLE') == 'S' || registro.get('NOTA_SIMPLE_DOCTO')=='S'){
									return registro.get('NUMERO_DOCTO');
								}
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('NOTA_MULTIPLE') == "S" || registro.get('NOTA_MULTIPLE_DOCTO') == "S")	{
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							} else	{
								return '';
							}
						},
						handler:	muestraPanelNotas
					}
				]
			},{
				header: 'Neto Recibir Pyme', tooltip: 'Neto Recibir Pyme',	dataIndex: 'NETO_REC_PYME',
				sortable: true,	width: 150,	align: 'right', resizable: true, hidden: true,hideable: false
			},{
				header: 'Beneficiario', tooltip: 'Beneficiario',	dataIndex: 'BENEFICIARIO',
				sortable: true,	width: 150,	align: 'center', resizable: true, hidden: true,hideable: false
			},{
				header: '% Beneficiario', tooltip: '% Beneficiario',	dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true, width: 100, resizable: true, hideable: false, align: 'right',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (!layOutAserca)	{
										if (registro.get('CS_DSCTO_ESPECIAL') == "D" || param.bOperaFactVencimientoInfonavit){
											if (registro.get('FN_PORC_BENEFICIARIO') >= 0){
												value = registro.get('FN_PORC_BENEFICIARIO');
											}
										}
										return Ext.util.Format.number(value,'0%');
									} else	{
										value = "";
										return value;
									}
								}
			},{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario',	dataIndex: 'RECIBIR_BENEF',
				sortable: true,	width: 120,	resizable: true, hideable : false, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (!layOutAserca)	{
										if (registro.get('CS_DSCTO_ESPECIAL') == "D" || param.bOperaFactVencimientoInfonavit){
											if (registro.get('RECIBIR_BENEF') >= 0){
												value = registro.get('RECIBIR_BENEF');
											}
										}
										return Ext.util.Format.number(value, '$0,0.00');
									} else	{
										value = "";
										return value;
									}
								}
			},{//28
				header : 'Digito Identificador', tooltip: 'Digito Identificador',	dataIndex : '',
				sortable : true, width : 200, hidden: true,hideable: false,align: 'center',
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store){
									value = ponerCeros(registro.get('IC_EPO'),4) + ponerCeros(registro.get('IC_DOCUMENTO'),11);
									return value;
								}
			},{
				header : 'Fecha de Recepcion de Bienes y Servicios', tooltip: 'Fecha de Recepcion de Bienes y Servicios',	dataIndex : 'DF_ENTREGA',
				sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: true
			},{
				header : 'Tipo de Compra (procedimiento)', tooltip: 'Tipo de Compra (procedimiento)',	dataIndex : 'CG_TIPO_COMPRA',
				sortable : true, width : 100, align: 'center', hidden: true
			},{
				header : 'Clasificador por objeto del gasto', tooltip: 'Clasificador por objeto del gasto',	dataIndex : 'CG_CLAVE_PRESUPUESTARIA',
				sortable : true, width : 100, align: 'center', hidden: true
			},{
				header : 'Plazo M�ximo', tooltip: 'Plazo M�ximo',	dataIndex : 'CG_PERIODO',
				sortable : true, width : 100, hidden: true, hideable: false,align: 'center'
			},{
				header: 'Mandante', tooltip: 'Mandante',	dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true, width: 250, resizable: true, hidden: true,hideable: false,align: 'center'
			},{
				header : 'Fecha Registro Operaci�n', tooltip: 'Fecha Registro Operaci�n',	dataIndex : 'FECHA_REGISTRO_OPERACION',
				sortable : true, width : 100, align: 'center', resizable: true, renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: true,hideable: false
			},{
				header : 'Fecha de Autorizaci�n IF', tooltip: 'Fecha de Autorizaci�n IF',	dataIndex : 'FECHA_AUTORIZACION_IF',
				sortable : true, width : 100, align: 'center',	hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (value == 'N/A'){
										return 'N/A';
									}else if (value == ''){
										return '';
									}else{
										return Ext.util.Format.date(value,'d/m/Y');
									}
								}
			},			
			{
				header: 'Validaci�n de <br> Documentos EPO', tooltip: 'Validaci�n de Documentos EPO', dataIndex: 'VALIDACION_JUR',
				sortable: true, width: 110, resizable: true, hideable: false,align: 'center'
			},
			{
				header : 'Contrato', tooltip: 'Contrato',	dataIndex : 'CONTRATO',
				sortable : true, width : 100, align: 'center', hidden: true
			},
			{
				header : 'COPADE', tooltip: 'COPADE',	dataIndex : 'COPADE',
				sortable : true, width : 100, align: 'center'	, hidden: true					
			}	
			
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: '',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					disabled: true,
					hidden: false,
					handler: function(boton, evento) {
						resumenTotalesData.load();
					}
				},
				'->','-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					iconCls: 'icoXls',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta01ext_csv.jsp',
							params: {
								cg_pyme_epo_interno: form.cg_pyme_epo_interno,
								ic_pyme:             form.ic_pyme,
								ic_if:               form.ic_if,
								ic_estatus_docto:    form.ic_estatus_docto,
								ic_moneda:           form.ic_moneda,
								cc_acuse:            form.cc_acuse,
								no_docto:            form.no_docto,
								df_fecha_docto_de:   form.df_fecha_docto_de,
								df_fecha_docto_a:    form.df_fecha_docto_a,
								df_fecha_venc_de:    form.df_fecha_venc_de,
								df_fecha_venc_a:     form.df_fecha_venc_a,
								fn_monto_de:         form.fn_monto_de,
								fn_monto_a:          form.fn_monto_a,
								ord_proveedor:       form.ord_proveedor,
								ord_if:              form.ord_if,
								ord_monto:           form.ord_monto,
								ord_fec_venc:        form.ord_fec_venc,
								ord_estatus:         form.ord_estatus,
								camp_dina1:          form.camp_dina1,
								camp_dina2:          form.camp_dina2,
								operaFirmaM:         form.operaFirmaM,
								operaNotasC:         form.operaNotasC,
								numero_siaff:        form.numero_siaff,
								sFechaVencPyme:      param.sFechaVencPyme,
								txtbanderaTablaDoctos:param.banderaTablaDoctos,
								cmbFactoraje:form.cmbFactoraje, 
								validaDoctoEPO:form.validaDoctoEPO,
								contrato:form.contrato,
								copade:form.copade
							},
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',/*
				{
					xtype: 'button',
					text: 'Generar PDF por p�gina',
					id: 'btnGenerarPDF',
					hidden: true,
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '13consulta01ext_pdf.jsp',
							params: {
									start: cmpBarraPaginacion.cursor,
									limit: cmpBarraPaginacion.pageSize,
									cg_pyme_epo_interno:	form.cg_pyme_epo_interno,
									ic_pyme:	form.ic_pyme,
									ic_if:	form.ic_if,
									ic_estatus_docto:	form.ic_estatus_docto,
									ic_moneda:	form.ic_moneda,
									cc_acuse:	form.cc_acuse,
									no_docto:	form.no_docto,
									df_fecha_docto_de:	form.df_fecha_docto_de,
									df_fecha_docto_a:	form.df_fecha_docto_a,
									df_fecha_venc_de:	form.df_fecha_venc_de,
									df_fecha_venc_a:	form.df_fecha_venc_a,
									fn_monto_de:	form.fn_monto_de,
									fn_monto_a:	form.fn_monto_a,
									ord_proveedor:	form.ord_proveedor,
									ord_if:	form.ord_if,
									ord_monto:	form.ord_monto,
									ord_fec_venc:	form.ord_fec_venc,
									ord_estatus:	form.ord_estatus,
									camp_dina1:	form.camp_dina1,
									camp_dina2:	form.camp_dina2,
									operaFirmaM:	form.operaFirmaM,
									operaNotasC:	form.operaNotasC,
									numero_siaff:	form.numero_siaff,
									sFechaVencPyme:param.sFechaVencPyme,
									txtbanderaTablaDoctos:param.banderaTablaDoctos
								},
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},'-',*/
				{
					xtype:   'button',
					text:    'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					iconCls: 'icoPdf',
					id:      'btnGenerarPDFTodo',
					hidden:  true,
					handler: function(boton, evento){
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta01ext.data.jsp',
							params: {
									informacion:           'Genera_PDF',
									estatusArchivo:        'INICIO',
									cg_pyme_epo_interno:   form.cg_pyme_epo_interno,
									ic_pyme:               form.ic_pyme,
									ic_if:                 form.ic_if,
									ic_estatus_docto:      form.ic_estatus_docto,
									ic_moneda:             form.ic_moneda,
									cc_acuse:              form.cc_acuse,
									no_docto:              form.no_docto,
									df_fecha_docto_de:     form.df_fecha_docto_de,
									df_fecha_docto_a:      form.df_fecha_docto_a,
									df_fecha_venc_de:      form.df_fecha_venc_de,
									df_fecha_venc_a:       form.df_fecha_venc_a,
									fn_monto_de:           form.fn_monto_de,
									fn_monto_a:            form.fn_monto_a,
									ord_proveedor:         form.ord_proveedor,
									ord_if:                form.ord_if,
									ord_monto:             form.ord_monto,
									ord_fec_venc:          form.ord_fec_venc,
									ord_estatus:           form.ord_estatus,
									camp_dina1:            form.camp_dina1,
									camp_dina2:            form.camp_dina2,
									operaFirmaM:           form.operaFirmaM,
									operaNotasC:           form.operaNotasC,
									numero_siaff:          form.numero_siaff,
									sFechaVencPyme:        param.sFechaVencPyme,
									txtbanderaTablaDoctos: param.banderaTablaDoctos,
									cmbFactoraje:form.cmbFactoraje, 
									validaDoctoEPO:form.validaDoctoEPO,
									contrato:form.contrato,
								   copade:form.copade
								},
							callback: procesarSuccessFailureGenerarPDFTodo
						});
					}
				},
				{
					xtype:  'button',
					text:   'Bajar PDF',
					id:     'btnBajarPDFTodo',
					hidden: true
				},
				'-'
			]
		}
	});

	var elementosForma = [
		{
			xtype:'hidden',
			id:	'hid_ic_pyme',
			value:''
		},{
			xtype: 'textfield',
			name: 'cg_pyme_epo_interno',
			id: 'cg_pyme_epo_interno',
			fieldLabel: 'Clave del Proveedor',
			allowBlank: true,
			anchor: '45%',
			maxLength: 25,
			listeners:	{
				'change':function(field){
								if ( !Ext.isEmpty(field.getValue()) ) {
									fp.el.mask('Procesando...', 'x-mask-loading');
									Ext.Ajax.request({	url: '13consulta01ext.data.jsp',	params: { numeroDeProveedor: field.getValue(),informacion: "obtenNombrePyme" },callback: procesaNombrePyme});
								}
							}
			}
		},{
			xtype: 'textfield',
			name: 'txtCadenasPymes',
			id: 'txtCadenasPymes',
			fieldLabel: 'Nombre Proveedor',
			allowBlank: true,
			readOnly:	true,
			maxLength: 100,
			listeners: {
				'render': function(field) {
								field.getEl().on('click', function(){Ext.Msg.alert(field.fieldLabel,'Este campo es de solo lectura. Use la b�squeda avanzada.');}); 
							}
			}
		},{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype:'displayfield',
					id:'disEspacio',
					text:''
				},
				{
					xtype:	'button',
					id:		'btnBuscaA',
					iconCls:	'icoBuscar',
					text:		'B�squeda Avanzada',
					handler: function(boton, evento) {
								var winVen = Ext.getCmp('winBuscaA');
									if (winVen){
										Ext.getCmp('fpWinBusca').getForm().reset();
										Ext.getCmp('fpWinBuscaB').getForm().reset();
										
										Ext.getCmp('cmb_num_ne').setValue();
										Ext.getCmp('cmb_num_ne').store.removeAll();
										Ext.getCmp('cmb_num_ne').reset();
										winVen.show();
									}else{
										var winBuscaA = new Ext.Window ({
											id:'winBuscaA',
											height: 320,
											x: 300,
											y: 100,
											width: 550,
											heigth: 100,
											modal: true,
											closeAction: 'hide',
											title: 'B�squeda Avanzada',
											items:[
												{
													xtype:'form',
													id:'fpWinBusca',
													frame: true,
													border: false,
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													labelWidth: 130,
													items:[
														{
															xtype:'displayfield',
															frame:true,
															border: false,
															value:'Utilice el * para b�squeda gen�rica'
														},{
															xtype:'displayfield',
															frame:true,
															border: false,
															value:''
														},{
															xtype: 'textfield',
															name: 'nombre_pyme',
															id:	'txtNombre',
															fieldLabel:'Nombre',
															maxLength:	100
														},{
															xtype: 'textfield',
															name: 'rfc_pyme',
															id:	'txtRfc',
															fieldLabel:'RFC',
															maxLength:	20
														},{
															xtype: 'textfield',
															name: 'num_pyme',
															id:	'txtNe',
															fieldLabel:'N�mero Proveedor',
															maxLength:	25
														}
													],
													buttonAlign:'center',
													buttons:[
														{
															text:'Buscar',
															iconCls:'icoBuscar',
															handler: function(boton) {
																			catalogoNombreProvData.load({ params: Ext.apply(Ext.getCmp('fpWinBusca').getForm().getValues()) });
																		}
														},{
															text:'Cancelar',
															iconCls: 'icoLimpiar',
															handler: function() {
																			Ext.getCmp('winBuscaA').hide();
																		}
														}
													]
												},{
													xtype:'form',
													frame: true,
													id:'fpWinBuscaB',
													style: 'margin: 0 auto',
													bodyStyle:'padding:10px',
													monitorValid: true,
													defaults: {
														msgTarget: 'side',
														anchor: '-20'
													},
													labelWidth: 80,
													items:[
														{
															xtype: 'combo',
															id:	'cmb_num_ne',
															name: 'ic_pyme',
															hiddenName : 'ic_pyme',
															fieldLabel: 'Nombre',
															emptyText: 'Seleccione Proveedor. . .',
															displayField: 'descripcion',
															valueField: 'clave',
															triggerAction : 'all',
															forceSelection:true,
															allowBlank: false,
															typeAhead: true,
															mode: 'local',
															minChars : 1,
															store: catalogoNombreProvData,
															tpl : NE.util.templateMensajeCargaCombo
														}
													],
													buttonAlign:'center',
													buttons:[
														{
															text:'Aceptar',
															iconCls:'aceptar',
															formBind:true,
															//hidden:true,
															handler: function() {
																			if (!Ext.isEmpty(Ext.getCmp('cmb_num_ne').getValue())){
																				var disp = Ext.getCmp('cmb_num_ne').lastSelectionText;
																				var cveP = disp.substr(0,disp.indexOf(" "));
																				var desc = disp.slice(disp.indexOf(" ")+1);
																				Ext.getCmp('hid_ic_pyme').setValue(Ext.getCmp('cmb_num_ne').getValue())
																				Ext.getCmp('cg_pyme_epo_interno').setValue(cveP);
																				Ext.getCmp('txtCadenasPymes').setValue(desc);
																				Ext.getCmp('winBuscaA').hide();
																			}
																		}
														},{
															text:'Cancelar',
															iconCls: 'icoLimpiar',
															handler: function() {	Ext.getCmp('winBuscaA').hide();	}
														}
													]
												}
											]
										}).show();
									}
								}
				}
			]
		},{
			xtype: 'compositefield',
			combineErrors: false,
			msgTarget: 'side',
			fieldLabel: 'N�mero de Acuse',
			items: [
				{
					xtype: 'textfield',
					name: 'cc_acuse',
					id: 'cc_acuse',
					allowBlank: true,
					maxLength: 15,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},{
					xtype:	'displayfield',
					value:	'N�mero de Documento:',
					width: 150
				},{
					xtype: 'textfield',
					name: 'no_docto',
					id: 'no_docto',
					allowBlank: true,
					maxLength: 15,
					msgTarget: 'side',
					margins: '0 20 0 0'
				}
			]
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Emisi�n',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_docto_de',
					id: 'dc_fecha_emisionMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'dc_fecha_emisionMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 25
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_docto_a',
					id: 'dc_fecha_emisionMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'dc_fecha_emisionMin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},{
			xtype: 'compositefield',
			fieldLabel: 'Monto',
			msgTarget: 'side',
			combineErrors: false,
			items: [
				{
					xtype: 'numberfield',
					name: 'fn_monto_de',
					id: 'montoMin',
					allowBlank: true,
					maxLength: 14,
					width: 100,
					msgTarget: 'side',
					vtype:	'rangoValor',
					campoFinValor:	'montoMax',
					margins: '0 20 0 0'
				},
				{
					xtype: 'displayfield',
					value: 'a', 
					width: 25
				},
				{
					xtype: 'numberfield',
					name: 'fn_monto_a',
					id: 'montoMax',
					allowBlank: true,
					maxLength: 14,
					width: 100,
					msgTarget: 'side',
					vtype:	'rangoValor',
					campoInicioValor:	'montoMin',
					margins: '0 20 0 0'
				}
			]
		},{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de vencimiento',	
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_venc_de',
					id: 'dc_fecha_vencMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'dc_fecha_vencMax',
					margins: '0 20 0 0'
				},{
					xtype: 'displayfield',
					value: 'a',
					width: 25
				},{
					xtype: 'datefield',
					name: 'df_fecha_venc_a',
					id: 'dc_fecha_vencMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'dc_fecha_vencMin',
					margins: '0 20 0 0'
				}
			]
		},{
			xtype: 'combo',
			id:	'cmbIF',
			name: 'ic_if',
			hiddenName : 'ic_if',			
			fieldLabel: 'Intermediario Financiero',
			emptyText: 'Seleccionar IF',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoIfData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'combo',
			id: 'cambioEstatusCmb',
			name: 'ic_estatus_docto',
			hiddenName : 'ic_estatus_docto',
			fieldLabel: 'Estatus',
			emptyText: 'Seleccione Estatus',
			allowBlank: true,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEstatusData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype: 'combo',
			id:	'_ic_moneda',
			name: 'ic_moneda',
			hiddenName : 'ic_moneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccione Moneda',	
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoMonedaData, 
			tpl : NE.util.templateMensajeCargaCombo
		},{
		  xtype: 'label',
		  id: 'lblCriterios',
		  fieldLabel: 'Criterios ordenamiento',
		  text: ''
		},{
			xtype: 'compositefield',
			fieldLabel: '',
			msgTarget: 'side',
			combineErrors: false,
			items: [
				{
					xtype: 'numberfield',
					name: 'ord_proveedor',
					id: 'ord_proveedor',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},{
					xtype: 'displayfield',
					value: 'Proveedor',
					width: 70
				},{
					xtype: 'numberfield',
					name: 'ord_if',
					id: 'ord_if',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},{
					xtype: 'displayfield',
					value: 'Intermediario Financiero'
				}
			]
		},{
			xtype: 'compositefield',
			fieldLabel: '',
			msgTarget: 'side',
			combineErrors: false,
			items: [
				{
					xtype: 'numberfield',
					name: 'ord_monto',
					id: 'ord_monto',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},{
					xtype: 'displayfield',
					value: 'Monto',
					width: 70
				},{
					xtype: 'numberfield',
					name: 'ord_fec_venc',
					id: 'ordFecVenc',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},{
					xtype: 'displayfield',
					value: 'Fecha de vencimiento',
					width: 150
				},{
					xtype: 'numberfield',
					name: 'ord_estatus',
					id: 'ord_estatus',
					allowBlank: true,
					maxLength: 1,
					width: 30,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},{
					xtype: 'displayfield',
					value: 'Estatus'
				}
			]
		},{
			xtype:'textfield',
			name:'numero_siaff',
			id:'numero_siaff',
			fieldLabel: 'Digito Identificador',
			regex:/^[0-9]*$/,
			hidden: true,
			maxLength:15,
			anchor:'45%',
			validator: function(value){
				if ( !Ext.isEmpty(Ext.getCmp('numero_siaff').getValue()) ){
				  if((Ext.getCmp('numero_siaff').getValue()).length == 15) {
						return true;
				  } else {
						return 'El Acuse de Registro Individual proporcionado no es v�lido, recuerde que este debe contener 15 digitos.';
				  }
				}
			}
		},
		{
		    xtype: 'compositefield',
		    fieldLabel: 'Tipo de Factoraje',	
		    combineErrors: false,
		    msgTarget: 'side',
		    items: [
			{
			    xtype: 'combo',
			    id:	'cmbFactoraje',
			    name: 'cmbFactoraje',
			    hiddenName : 'cmbFactoraje',			
			    fieldLabel: 'Tipo de Factoraje',
			    emptyText: 'Seleccionar ',
			    mode: 'local',
			    displayField: 'descripcion',
			    valueField: 'clave',
			    forceSelection : false,
			    triggerAction : 'all',
			    typeAhead: true,
			    minChars : 1,
			    width: 170,
			    store: catTipoFactorajeData,
			    tpl : NE.util.templateMensajeCargaCombo
			},
			{
			    xtype: 'displayfield',
			    value: '  Validaci�n de Documentos EPO:',
			    width: 150
			},
			{
			    xtype: 'combo',
			    id	: 'validaDoctoEPO',
			    name: 'validaDoctoEPO',
			    hiddenName	:'validaDoctoEPO', 
			    fieldLabel: 'Validaci�n de Documento EPO',
			    forceSelection: true,
			    triggerAction: 'all',
			    mode: 'local',
			     width: 100,
			    valueField	: 'clave',
			    displayField: 'descripcion',
			    store: storeValidaEPO										
			}					
		    ]
		},
		{
		    xtype: 'compositefield',
		    fieldLabel: 'COPADE',	
		    combineErrors: false,
		    msgTarget: 'side',
			 id: 'idCopade',
			  hidden: true,
		    items: [
				{
						xtype:               'textfield',
						name:                'copade',
						id:                  'copade',
						allowBlank:          true,
						anchor:              '100%',
						fieldLabel:          'COPADE',
						maxLength:           10,				
						msgTarget:           'side'
					}
			]
		},
		{
		    xtype: 'compositefield',
		    fieldLabel: 'Contrato',	
		    combineErrors: false,
		    msgTarget: 'side',
			 id: 'idContrato',
			 hidden: true,
		    items: [
				{
						xtype:               'textfield',
						name:                'contrato',
						id:                  'contrato',
						allowBlank:          true,
						anchor:              '100%',
						fieldLabel:          'Contrato',
						maxLength:           10,				
						msgTarget:           'side'
					}
			]
		}
	];

	var elementosModifica = [
		{
			xtype: 'container',
			layout:'column',
				items:[{
					xtype: 'panel',id:	'izqModifica',
					width:	200,
					//layout: 'form',
					border:false,
					//bodyStyle: 'padding: 2px',
					//items: [{html: ' '}]
					items: [{xtype: 'displayfield', value:''}]
				},{
					xtype: 'panel',	id:	'centroModifica',
					//title:	'Cifras de Control',
					width:	400,
					layout: 'form',
					bodyStyle: 'padding: 2px',
					border: false,
					items: [
						{
							xtype: 'displayfield',	id: 'disProv',		fieldLabel: 'Proveedor',				value: ''
						},{
							xtype: 'displayfield',	id: 'disIfM',		fieldLabel: 'Intermediario Financiero',value: ''
						},{
							xtype: 'displayfield',	id: 'disDoctoM',	fieldLabel: 'N�mero de Documento',	value: ''
						},{
							xtype: 'displayfield',	id: 'disFecEmiM',	fieldLabel: 'Fecha de Emisi�n',		value: ''
						},{
							xtype: 'displayfield',	id: 'disFecVencM',fieldLabel: 'Fecha de Vencimiento',	value: ''
						},{
							xtype: 'displayfield',	id: 'disMonedaM',	fieldLabel: 'Moneda',					value: ''
						},{
							xtype: 'displayfield',	id: 'disMontoM',	fieldLabel: 'Monto',						value: ''
						},{
							xtype: 'displayfield',	id: 'disEstatusM',fieldLabel: 'Estatus',					value: ''
						}
					]
			},{
					xtype: 'panel',id:	'derModifica',
					width:	180,
					//layout: 'form',
					border:false,
					items: []
			}]
		}
	];	

	var fpModifica=new Ext.FormPanel({
		autoHeight:true,
		//height:212,
		width: 790,
		border: true,
		labelWidth: 150,
		items: elementosModifica,
		hidden: false
	});

	var elementosDetalle = [
		{
			xtype:	'container',
			layout:	'column',
			anchor:	'100%',
				items:[{
					xtype: 'container',id:	'Izquierda',
					width:	420,
					layout: 'form',
					labelWidth: 50,
					bodyStyle: 'padding: 5px',
					defaults: {frame:false, border: false,height: 18,bodyStyle:'padding:5px'},
					items: [
						{
						  xtype: 'panel',	id: 'disNombre',	html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disRfc',		html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disCalle',	html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disColonia',	html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disCp',		html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disEstado',	html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disDelomun',	html: '&nbsp;'
						}
					]
				},{
					xtype: 'panel',	id:'derecha',	layout:'table',	layoutConfig:{ columns:2 },
							defaults: {frame:false, border: true,width:340, height: 30,bodyStyle:'padding:5px'},
					items: [
						{	width:340,	border:false,	frame:true,	colspan:2,	html:'<div align="center">Datos del Documento</div>'	},
						{	width:170,	html:'<div>No. Documento</div>'	},
						{	width:170,	id: 'disDocto',	html: '&nbsp;'	},
						{	width:170,	html:'<div>Fecha de Emisi�n</div>'	},
						{	width:170,	id: 'disEmision',	html: '&nbsp;'	},
						{	width:170,	html:'<div>Fecha de Vencimiento</div>'	},
						{	width:170,	id: 'disVto',	html: '&nbsp;'	},
						{	width:170,	html:'<div>Importe</div>'	},
						{	width:170,	id: 'disImporte',	html: '&nbsp;'	},
						{	width:170,	html:'<div>Moneda</div>'	},
						{	width:170,	id: 'disMoneda',	html: '&nbsp;'	}
					]
			}]
		}
	];	

	var fpDetalle=new Ext.FormPanel({	autoHeight:true,	width:800,	bodyStyle: 'padding:6px',	border:false,	labelWidth: 130,	items:elementosDetalle,	hidden:false	});//height:230,

	var elementosAcuse = [
		{
			xtype: 'panel',layout:'table',	width:550,	border:true,	layoutConfig:{ columns: 3 },
			defaults: {frame:false, border: true,width:220, height: 35,bodyStyle:'padding:3px'},
			items:[
				{	frame:true,	border:false,	html:'&nbsp;'	},
				{	width:165,	border:false,	frame:true,	html:'<div align="center">Moneda Nacional</div>'	},
				{	width:165,	border:false,	frame:true,	html:'<div align="center">D�lares</div>'	},
				{	frame:true,	border:false,	html:'<div align="right">No. Total de documentos cargados</div>'	},
				{	id:'nMn',	width:165,	html: '&nbsp;'	},
				{	id:'nDl',	width:165,	border:true,	html:'&nbsp;'	},
				{	frame:true,	border:false,	html:'<div align="right">Monto total de los documentos cargados</div>'	},
				{	id:'totMn',	width:165,	html: '&nbsp;'	},
				{	id:'totDl',	width:165,	html: '&nbsp;'	},
				{	frame:true,	border:false,	html:'<div align="right">N�mero de Acuse</div>'	},
				{	id:'disAcuse',	width:330,	colspan:2,	html:'&nbsp;'	},
				{	frame:true,	border:false,	html:'<div align="right">Fecha de carga</div>'	},
				{	id:'disFechaCarga',	width:330,	colspan:2,	html:'&nbsp;'	},
				{	frame:true,	border:false,	html:'<div align="right">Hora de carga</div>'	},
				{	id:'disHoraCarga',	width:330,	colspan:2,	html:'&nbsp;'},
				{	frame:true,	html:'<div align="right">Usuario</div>'	},
				{	id:'disUsuario',	width:330,	colspan:2,	html: '&nbsp;'	},
				{	width:550,	colspan:3,	border:true,	html:'<div align="center">* Si desea consultar el detalle del acuse, dirigirse a realizar la consulta con base al n�mero de acuse mostrado.</div>' }
			]
		}
	];

	var fpAcuse=new Ext.FormPanel({	height:282,	width:552,	border:true,	frame:false,	labelWidth:130,	items:elementosAcuse,	hidden:false 	});	//title:'<div align="center">Datos del Acuse</div>'

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 700,
		style: ' margin:0 auto;',
		title:	'',
		collapsible: true,
		titleCollapse: true,
		hidden: false,
		frame: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 150,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					var totalesCmp = Ext.getCmp('gridTotales');
					if (totalesCmp.isVisible()) {
						totalesCmp.hide();
					}
					if(!verificaPanel()) {
						return;
					}
					var fechaEmisionMin = Ext.getCmp('dc_fecha_emisionMin');
					var fechaEmisionMax = Ext.getCmp('dc_fecha_emisionMax');
					var fechaVenceMin = Ext.getCmp("dc_fecha_vencMin");
					var fechaVenceMax = Ext.getCmp("dc_fecha_vencMax");
					var montoMin = Ext.getCmp("montoMin");
					var montoMax = Ext.getCmp("montoMax");

					if (!Ext.isEmpty(fechaEmisionMin.getValue()) || !Ext.isEmpty(fechaEmisionMax.getValue()) ) {
						if(Ext.isEmpty(fechaEmisionMin.getValue()))	{
							fechaEmisionMin.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
							fechaEmisionMin.focus();
							return;
						}else if (Ext.isEmpty(fechaEmisionMax.getValue())){
							fechaEmisionMax.markInvalid('Debe capturar ambas fechas de emisi�n o dejarlas en blanco');
							fechaEmisionMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(fechaVenceMin.getValue()) || !Ext.isEmpty(fechaVenceMax.getValue()) ) {
						if(Ext.isEmpty(fechaVenceMin.getValue()))	{
							fechaVenceMin.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMin.focus();
							return;
						}else if (Ext.isEmpty(fechaVenceMax.getValue())){
							fechaVenceMax.markInvalid('Debe capturar ambas fechas de vencimiento o dejarlas en blanco');
							fechaVenceMax.focus();
							return;
						}
					}
					if (!Ext.isEmpty(montoMin.getValue()) || !Ext.isEmpty(montoMax.getValue()) ) {
						if(Ext.isEmpty(montoMin.getValue()))	{
							montoMin.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							montoMin.focus();
							return;
						}else if (Ext.isEmpty(montoMax.getValue())){
							montoMax.markInvalid('Debe capturar ambos montos o dejarlos en blanco');
							montoMax.focus();
							return;
						}
					}

					var o_py = Ext.getCmp('ord_proveedor').getValue();
					var o_if = Ext.getCmp('ord_if').getValue();
					var o_mt = Ext.getCmp('ord_monto').getValue();
					var o_fv = Ext.getCmp('ordFecVenc').getValue();
					var o_oe = Ext.getCmp('ord_estatus').getValue();

					if (o_py != "" || o_if != "" || o_mt != "" || o_fv != "" || o_oe != "") {
						if ( (o_py >= 1 && o_py <= 5) || (o_if >= 1 && o_if <= 5) || (o_mt >= 1 && o_mt <= 5) || (o_fv >= 1 && o_fv <= 5) || (o_oe >= 1 && o_oe <= 5)) {
							if ( ((o_if == o_py) && o_if != "") || ((o_if == o_mt) && o_if != "") || ((o_if == o_fv) && o_if != "") || ((o_if == o_oe) && o_if != "") || ((o_mt == o_py) && o_mt !="") || ((o_mt == o_fv) && o_mt !="") || ((o_mt == o_oe) && o_mt !="") || ((o_fv == o_py) && o_py != "") || ((o_fv == o_oe) && o_fv !="") || ((o_py == o_oe) && o_py !="") ) {
								Ext.getCmp('ord_proveedor').markInvalid('La Prioridad no puede ser la Misma en el Criterio de Ordenamiento');
								return;
							}
						}
						if ( ((o_py > 5 || o_py == 0) && o_py != "") || ((o_if > 5 || o_if == 0) && o_if != "") || ((o_mt > 5 || o_mt == 0) && o_mt != "") || ((o_fv > 5 || o_fv == 0) && o_fv != "") || ((o_oe > 5 || o_oe == 0) && o_oe != "") ) {
							Ext.getCmp('ord_proveedor').markInvalid('El Criterio de Ordenamiento debe de estar entre los valores del 1 al 5');
							return;
						}
					}
/////
					fp.el.mask('Enviando...', 'x-mask-loading');
					form = {
						cg_pyme_epo_interno:null,	ic_pyme:null,	ic_if:null,	ic_estatus_docto:null,	ic_moneda:null,	cc_acuse:null,
						no_docto:null,	df_fecha_docto_de:null,	df_fecha_docto_a:null,	fn_monto_de:null,	fn_monto_a:null,	df_fecha_venc_de:null,
						df_fecha_venc_a:null,	ord_proveedor:null,	ord_if:null,	ord_monto:null,	ord_fec_venc:null,	ord_estatus:null,
						camp_dina1:null,	camp_dina2:null,	operaFirmaM:null,	operaNotasC:null,	numero_siaff:null, 
						cmbFactoraje:null, validaDoctoEPO:null, contrato:null, copade:null
					}
					form.cg_pyme_epo_interno= Ext.getCmp('cg_pyme_epo_interno').getValue();
					//form.ic_pyme				= Ext.getCmp('hid_ic_pyme').getValue();
					form.ic_pyme				= Ext.getCmp('hid_ic_pyme').value;
					form.ic_if					= Ext.getCmp('cmbIF').getValue();
					form.ic_moneda				= Ext.getCmp('_ic_moneda').getValue();
					form.cc_acuse				= Ext.getCmp('cc_acuse').getValue();
					form.no_docto				= Ext.getCmp('no_docto').getValue();
					var f_emi_ini	=	fechaEmisionMin.getValue();
					var f_emi_fin	=	fechaEmisionMax.getValue();
					var f_venc_ini	=	fechaVenceMin.getValue();
					var f_venc_fin	=	fechaVenceMax.getValue();
					if (	!Ext.isEmpty(f_emi_ini)	){
						form.df_fecha_docto_de	= f_emi_ini.format('d/m/Y');
					}
					if (	!Ext.isEmpty(f_emi_fin)	){
						form.df_fecha_docto_a	= f_emi_fin.format('d/m/Y');
					}
					form.fn_monto_de			= montoMin.getValue();
					form.fn_monto_a			= montoMax.getValue();
					if (	!Ext.isEmpty(f_venc_ini)	){
						form.df_fecha_venc_de	= f_venc_ini.format('d/m/Y');
					}
					if (	!Ext.isEmpty(f_venc_fin)	){
						form.df_fecha_venc_a		= f_venc_fin.format('d/m/Y');
					}
					form.ord_proveedor		= o_py;
					form.ord_if					= o_if;
					form.ord_monto				= o_mt;
					form.ord_fec_venc			= o_fv;
					form.ord_estatus			= o_oe;
					form.cmbFactoraje= Ext.getCmp('cmbFactoraje').getValue(); 
					form.validaDoctoEPO=Ext.getCmp('validaDoctoEPO').getValue();
					form.contrato=Ext.getCmp('contrato').getValue();
					form.copade=Ext.getCmp('copade').getValue();
										
					var cd1 = Ext.getCmp('campDina1');
					var cd2 = Ext.getCmp('campDina2');
					if  (cd1){
						form.camp_dina1 = cd1.getValue();
					}
					if (cd2){
						form.camp_dina2	= cd2.getValue();
					}
					form.operaFirmaM			= param.bOperaFirmaMancomunada?"S":"N";
					form.operaNotasC			= param.bOperaFactorajeNotaDeCredito?"S":"N";
					form.numero_siaff			= Ext.getCmp('numero_siaff').getValue();
					form.ic_estatus_docto = Ext.getCmp('cambioEstatusCmb').getValue();
					if (form.ic_estatus_docto == "" || form.ic_estatus_docto == "1" || form.ic_estatus_docto == "2" || form.ic_estatus_docto == "5" || 
						form.ic_estatus_docto == "6" || form.ic_estatus_docto == "7" || form.ic_estatus_docto == "9" ||	form.ic_estatus_docto == "10" || 
						form.ic_estatus_docto == "21" || form.ic_estatus_docto == "23" || form.ic_estatus_docto == "28" || form.ic_estatus_docto == "29" || 
						form.ic_estatus_docto == "30" || form.ic_estatus_docto == "31"){
						ocultarDoctosAplicados = true;
					}else{
						ocultarDoctosAplicados = false;
					}
					var ic_epo_aserca = "314";
					layOutAserca = false;
					//if (form.ic_estatus_docto == "1" && ic_epo_aserca == param.iNoCliente)	{
					if (ic_epo_aserca == param.iNoCliente)	{
						layOutAserca = true;
					}
					
					
					
					consultaData.load({
						//params: Ext.apply(fp.getForm().getValues(),{
						params: Ext.apply({
							operacion: 'Generar',
							start: 0,
							limit: 15
						})
					});
				} //fin handler
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta01ext.jsp';
				}
			}
		]
	});

	function ponerCeros(obj,tam) {
		while (obj.toString().length<tam){
			obj = '0'+obj.toString();
		}
		return obj;
	}

	function verificaPanel(){
		var myPanel = Ext.getCmp('forma');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype != 'label')	{	//El metodo isValid() no aplica para los tipos de objetos label.
				if (!panelItem.isValid())	{
					valid = false;
					return false;
				}
			}
		});
		return valid;
	}

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(5),
			grid,
			gridTotales
		]
	});

	catalogoIfData.load();
	catalogoEstatusData.load();
	catalogoMonedaData.load();
	catTipoFactorajeData.load();

	fp.el.mask('Procesando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '13consulta01ext.data.jsp',
		params: { informacion: "Parametrizacion" },
		callback: procesarSuccessFailureParametrizacion
	});
	

});
