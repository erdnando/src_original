<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,java.math.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%

AccesoDB con = new AccesoDB();
CQueryHelperExtJS	queryHelper = null;
JSONObject jsonObj = new JSONObject();

String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
String ic_pyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
String ig_numero_docto = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto");
String df_fecha_docto_de = (request.getParameter("df_fecha_docto_de") == null) ? "" : request.getParameter("df_fecha_docto_de");
String df_fecha_docto_a = (request.getParameter("df_fecha_docto_a") == null) ? "" : request.getParameter("df_fecha_docto_a");
String df_fecha_venc_de = (request.getParameter("df_fecha_venc_de") == null) ? "" : request.getParameter("df_fecha_venc_de");
String df_fecha_venc_a = (request.getParameter("df_fecha_venc_a") == null) ? "" : request.getParameter("df_fecha_venc_a");
String ic_moneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
String fn_monto_de = (request.getParameter("fn_monto_de") == null) ? "" : request.getParameter("fn_monto_de");
String fn_monto_a = (request.getParameter("fn_monto_a") == null) ? "" : request.getParameter("fn_monto_a");
String ic_estatus_docto = (request.getParameter("ic_estatus_docto") == null) ? "" : request.getParameter("ic_estatus_docto");
String df_fecha_seleccion_de = (request.getParameter("df_fecha_seleccion_de") == null) ? "" : request.getParameter("df_fecha_seleccion_de");
String df_fecha_seleccion_a = (request.getParameter("df_fecha_seleccion_a") == null) ? "" : request.getParameter("df_fecha_seleccion_a");
String in_tasa_aceptada_de = (request.getParameter("in_tasa_aceptada_de") == null) ? "" : request.getParameter("in_tasa_aceptada_de");
String in_tasa_aceptada_a = (request.getParameter("in_tasa_aceptada_a") == null) ? "" : request.getParameter("in_tasa_aceptada_a");
String ord_if = (request.getParameter("ord_if") == null) ? "" : request.getParameter("ord_if");
String ord_pyme = (request.getParameter("ord_pyme") == null) ? "" : request.getParameter("ord_pyme");
String ord_epo = (request.getParameter("ord_epo") == null) ? "" : request.getParameter("ord_epo");
String ord_fec_venc = (request.getParameter("ord_fec_venc") == null) ? "" : request.getParameter("ord_fec_venc");
String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
String strGenerarArchivo = (request.getParameter("txtGeneraArchivo") == null) ? "" : request.getParameter("txtGeneraArchivo");
String numero_siaff= (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
String cmbFactoraje   = (request.getParameter("cmbFactoraje") == null) ? "" : request.getParameter("cmbFactoraje");	
String validaDoctoEPO   = (request.getParameter("validaDoctoEPO") == null) ? "" : request.getParameter("validaDoctoEPO");
    
// Fodea 002 - 2010
boolean  ocultarDoctosAplicados = (
			ic_estatus_docto.equals("") 	|| 
			ic_estatus_docto.equals("1") 	|| 
			ic_estatus_docto.equals("2") 	|| 
			ic_estatus_docto.equals("5")	|| 
			ic_estatus_docto.equals("6") 	|| 
			ic_estatus_docto.equals("7") 	|| 
			ic_estatus_docto.equals("9")	|| 
			ic_estatus_docto.equals("10") || 
			ic_estatus_docto.equals("21") || 
			ic_estatus_docto.equals("23")	|| 
			ic_estatus_docto.equals("28") || 
			ic_estatus_docto.equals("29")	|| 
			ic_estatus_docto.equals("30")	|| 
			ic_estatus_docto.equals("31") )?true:false;

String banderaTablaDoctos = (request.getParameter("txtbanderaTablaDoctos") == null) ? "" : request.getParameter("txtbanderaTablaDoctos");
String fechaEntrega = "", tipoCompra = "", clavePresupuestaria = "", periodo = "";

int registros = 0, numRegistros = 0, numRegistrosMN = 0, numRegistrosDL = 0;
BigDecimal montoTotalMN = new BigDecimal("0.00");
BigDecimal montoTotalDL = new BigDecimal("0.00");
BigDecimal montoTotalDsctoMN = new BigDecimal("0.00");
BigDecimal montoTotalDsctoDL = new BigDecimal("0.00");

int vtmn = 0, vesp = 0, vnumd = 0;

int coma=0;
double total_dolares = 0, total_mn = 0;
int total_doctos_dol = 0, total_doctos_mn = 0, val = 0;
boolean gen_archivo=false, dolaresDocto = false, nacionalDocto = false;
int IC_DOCUMENTO = 0, IC_EPO = 0;
String IC_PYME = "", CC_ACUSE = "", IG_NUMERO_DOCTO = "", DF_FECHA_DOCTO = "", DF_FECHA_VENC = "", DF_FECHA_VENC_PYME = "";
String IC_MONEDA = "", CS_DSCTO_ESPECIAL = "", IC_ESTATUS_DOCTO = "", IC_IF = "", IC_FOLIO = "";
double FN_MONTO = 0, FN_PORC_ANTICIPO = 0, FN_MONTO_DSCTO = 0;
int IC2_ESTATUS_DOCTO = 0, NO_MONEDA = 0, DETALLES = 0;
String NO_INT_PYME="", CT_REFERENCIA="", CS_CAMBIO_IMPORTE="";
String CG_CAMPO1="", CG_CAMPO2="", CG_CAMPO3="", CG_CAMPO4="", CG_CAMPO5="";
String linea_cd = "";
//String cgCampo1 = "", cgCampo2 = "", cgCampo3 = "", cgCampo4 = "", cgCampo5 = "";
String numeroSIAFF="";
String mandante ="";
String tipoFactorajeDesc  =  "";
String fecha_registro_operacion = "";//FODEA 005 - 2009 ACF
String fechaAutorizacionIF = ""; // Fodea 040 - 2011 By JSHD
double dblPorciento = 0, dblMontoDescuento = 0, dblTotalDescuentoPesos = 0, dblTotalDescuentoDolares = 0;
String beneficiario = "";
double porciento_beneficiario = 0;
double importe_a_recibir_beneficiario = 0;
double neto_a_recibir_pyme = 0;
boolean bandera_neto_a_recibir_pyme = false ;
String sFechaVencPyme="";
String cd17="", cd18="", cd19="", cd20="", cd21="";
//CQueryHelper	queryHelper = null;
ResultSet		rsReporte	= null;
boolean bOperaFactorajeVencido = false;
boolean bOperaFactorajeDistribuido = false;
boolean bOperaFactorajeNotaDeCredito = false;
boolean bOperaFactorajeVencInfonavit = false;

// Fodea 002 - 2010
boolean esNotaDeCreditoAplicada								= false;
boolean esDocumentoConNotaDeCreditoMultipleAplicada	= false;

boolean esNotaDeCreditoSimpleAplicada						= false;
boolean esDocumentoConNotaDeCreditoSimpleAplicada		= false;
String  numeroDocumento											= "";	

/************A S E R C A*/
//String ic_epo_aserca = (String)session.getAttribute("sesEpoAserca");
String ic_epo_aserca = "314";
boolean layOutAserca = false;
if(ic_epo_aserca.equals(iNoCliente))
	layOutAserca = true;
/************A S E R C A*/

try {
	con.conexionDB();
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;

	sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(iNoCliente);
	boolean estaHabilitadoNumeroSIAFF = BeanParamDscto.estaHabilitadoNumeroSIAFF(iNoCliente);
	queryHelper = new CQueryHelperExtJS(new ConsDoctosDE());
	Registros regCamposAdicionales = null;
   // Mandante  Fodea 023
	boolean bOperaFactConMandato=false;
	boolean bTipoFactoraje=false;
	boolean bMostrarFechaAutorizacionIFInfoDoctos = false; // Fodea 040 - 2011

	Hashtable alParamEPO1 = new Hashtable(); 
	alParamEPO1 = BeanParamDscto.getParametrosEPO(iNoCliente,1);
	if (alParamEPO1!=null) {
		  bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		   bOperaFactorajeVencido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
		   bOperaFactorajeDistribuido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
		   bOperaFactorajeNotaDeCredito = ("N".equals(alParamEPO1.get("OPERA_NOTAS_CRED").toString()))?false:true; 
			bOperaFactorajeVencInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true; //FODEA 042 - Agosto/2009
			
			bMostrarFechaAutorizacionIFInfoDoctos = "S".equals((String) alParamEPO1.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS") )?true:false; // Fodea 040 - 2011
	}

	bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaDeCredito || bOperaFactConMandato || bOperaFactorajeVencInfonavit)?true:false;

	String campos_dinamicos = "";
	String noms_cad="";
	int total_cd = 0;
	int cd = 0;
	total_cd = BeanSeleccionDocumento.getTotalCamposAdicionales(iNoCliente);
	
	int[] ic_no_campo = new int[total_cd];
	int[] ig_longitud = new int[total_cd];
	String[] nombre_campo = new String[total_cd];
	String[] cg_tipo_dato = new String[total_cd];
	String[] cg_tipo_objeto = new String[total_cd];

	if(total_cd > 0) {
		cd = 0;
		regCamposAdicionales = new Registros();
		regCamposAdicionales = BeanParamDscto.getCamposAdicionales(iNoCliente);
		if (regCamposAdicionales != null){
			while (regCamposAdicionales.next()){
				ic_no_campo[cd]	= Integer.parseInt(regCamposAdicionales.getString("IC_NO_CAMPO"));
				nombre_campo[cd]	= regCamposAdicionales.getString("NOMBRE_CAMPO");
				cg_tipo_dato[cd]	= regCamposAdicionales.getString("CG_TIPO_DATO");
				ig_longitud[cd]	= Integer.parseInt(regCamposAdicionales.getString("IG_LONGITUD"));
				cg_tipo_objeto[cd]= regCamposAdicionales.getString("CG_TIPO_OBJETO");

				if(ic_no_campo[cd] == 1) { campos_dinamicos+=", D.CG_CAMPO1"; noms_cad+=", "+nombre_campo[cd]+" "; cd17="Si";}
				if(ic_no_campo[cd] == 2) { campos_dinamicos+=", D.CG_CAMPO2"; noms_cad+=", "+nombre_campo[cd]+" "; cd18="Si";}
				if(ic_no_campo[cd] == 3) { campos_dinamicos+=", D.CG_CAMPO3"; noms_cad+=", "+nombre_campo[cd]+" "; cd19="Si";}
				if(ic_no_campo[cd] == 4) { campos_dinamicos+=", D.CG_CAMPO4"; noms_cad+=", "+nombre_campo[cd]+" "; cd20="Si";}
				if(ic_no_campo[cd] == 5) { campos_dinamicos+=", D.CG_CAMPO5"; noms_cad+=", "+nombre_campo[cd]+" "; cd21="Si";}
				cd++;
			}
		}
	}
	if(!cd17.equals("Si")) { noms_cad+=",Campo Adicional 1"; }
	if(!cd18.equals("Si")) { noms_cad+=",Campo Adicional 2"; }
	if(!cd19.equals("Si")) { noms_cad+=",Campo Adicional 3"; }
	if(!cd20.equals("Si")) { noms_cad+=",Campo Adicional 4"; }
	if(!cd21.equals("Si")) { noms_cad+=",Campo Adicional 5"; }
	rsReporte = queryHelper.getCreateFile(request,con);
	while(rsReporte.next()){
		IC_DOCUMENTO = rsReporte.getInt(1); //out.println("IC_DOCUMENTO: "+IC_DOCUMENTO+"<br>");
		IC_PYME = rsReporte.getString(2); //out.println("IC_PYME: "+IC_PYME+"<br>");
		IC_EPO = rsReporte.getInt(3); //out.println("IC_EPO: "+IC_EPO+"<br>");
		CC_ACUSE = (rsReporte.getString(4)==null)?"":rsReporte.getString(4); //out.println("CC_ACUSE: "+CC_ACUSE+"<br>");
		IG_NUMERO_DOCTO = rsReporte.getString(5); //out.println("IG_NUMERO_DOCTO: "+IG_NUMERO_DOCTO+"<br>");
		DF_FECHA_DOCTO = rsReporte.getString(6); //out.println("DF_FECHA_DOCTO: "+DF_FECHA_DOCTO+"<br>");
		DF_FECHA_VENC = (rsReporte.getString(7)==null)?"":rsReporte.getString(7);
		IC_MONEDA = rsReporte.getString(8); //out.println("IC_MONEDA: "+IC_MONEDA+"<br>");
		FN_MONTO = rsReporte.getDouble(9); //out.println("FN_MONTO: "+FN_MONTO+"<br>");
		CS_DSCTO_ESPECIAL = rsReporte.getString(10); //out.println("CS_DSCTO_ESPECIAL: "+CS_DSCTO_ESPECIAL+"<br>");
		FN_PORC_ANTICIPO = rsReporte.getDouble(11); //out.println("FN_PORC_ANTICIPO: "+FN_PORC_ANTICIPO+"<br>");
		FN_MONTO_DSCTO = rsReporte.getDouble(12); //out.println("FN_MONTO_DSCTO: "+FN_MONTO_DSCTO+"<br>");
		IC_ESTATUS_DOCTO = rsReporte.getString(13); //out.println("IC_ESTATUS_DOCTO: "+IC_ESTATUS_DOCTO+"<br>");
		IC_IF = rsReporte.getString(14); //out.println("IC_IF: "+IC_IF+"<br>");
		IC_FOLIO = (rsReporte.getString(15)==null)?"":rsReporte.getString(15); //out.println("IC_FOLIO: "+IC_FOLIO+"<br>");
		IC2_ESTATUS_DOCTO = rsReporte.getInt(16); //out.println("IC2_ESTATUS_DOCTO: "+IC2_ESTATUS_DOCTO+"<br>");
		NO_MONEDA = rsReporte.getInt(17); //out.println("NO_MONEDA: "+NO_MONEDA+"<br>");
		NO_INT_PYME = rsReporte.getString(18);
		CT_REFERENCIA = (rsReporte.getString(19)==null)?"":rsReporte.getString(19);
		CS_CAMBIO_IMPORTE = (rsReporte.getString(20)==null)?"":rsReporte.getString(20).trim();
		DETALLES = rsReporte.getInt(21);
		//df_programacion = (rsReporte.getString("df_programacion")==null)?"":rsReporte.getString("df_programacion").trim();
		DF_FECHA_VENC_PYME = (rsReporte.getString("DF_FECHA_VENC_PYME")==null)?"":rsReporte.getString("DF_FECHA_VENC_PYME");
		String validacionJUR = (rsReporte.getString("VALIDACION_JUR")==null)?"":rsReporte.getString("VALIDACION_JUR");
	
		mandante	= (rsReporte.getString(38)==null)?"":rsReporte.getString(38); // Fodea 023
		
		tipoFactorajeDesc = (rsReporte.getString("TIPO_FACTORAJE")==null)?"":rsReporte.getString("TIPO_FACTORAJE").trim();
		fecha_registro_operacion = rsReporte.getString("fecha_registro_operacion")==null?"":rsReporte.getString("fecha_registro_operacion");//FODEA 005 - 2009 ACF
		fechaAutorizacionIF      = rsReporte.getString("fecha_autorizacion_if")   ==null?"":rsReporte.getString("fecha_autorizacion_if");   //FODEA 040 - 2011 By JSHD
		linea_cd = "";
	
		if(cd17.equals("Si")) { CG_CAMPO1=(rsReporte.getString(22)==null)?"":rsReporte.getString(22); linea_cd+=","+CG_CAMPO1.replace(',',' ')+""; } else { linea_cd+=","; }
		if(cd18.equals("Si")) { CG_CAMPO2=(rsReporte.getString(23)==null)?"":rsReporte.getString(23); linea_cd+=","+CG_CAMPO2.replace(',',' ')+""; } else { linea_cd+=","; }
		if(cd19.equals("Si")) { CG_CAMPO3=(rsReporte.getString(24)==null)?"":rsReporte.getString(24); linea_cd+=","+CG_CAMPO3.replace(',',' ')+""; } else { linea_cd+=","; }
		if(cd20.equals("Si")) { CG_CAMPO4=(rsReporte.getString(25)==null)?"":rsReporte.getString(25); linea_cd+=","+CG_CAMPO4.replace(',',' ')+""; } else { linea_cd+=","; }
		if(cd21.equals("Si")) { CG_CAMPO5=(rsReporte.getString(26)==null)?"":rsReporte.getString(26); linea_cd+=","+CG_CAMPO5.replace(',',' ')+""; } else { linea_cd+=","; }
	
		numeroSIAFF = getNumeroSIAFF(Integer.toString(IC_EPO),Integer.toString(IC_DOCUMENTO));
		beneficiario = rsReporte.getString("beneficiario")==null?"":rsReporte.getString("beneficiario");
		porciento_beneficiario = rsReporte.getDouble("fn_porc_beneficiario");
		importe_a_recibir_beneficiario = rsReporte.getDouble("RECIBIR_BENEF");
		if(bandera_neto_a_recibir_pyme){
			neto_a_recibir_pyme = rsReporte.getDouble("NETO_REC_PYME");
		}
	
		if( banderaTablaDoctos.equals("1") ){
			fechaEntrega 		= (rsReporte.getString("DF_ENTREGA")==null)?"":rsReporte.getString("DF_ENTREGA");
			tipoCompra 			= (rsReporte.getString("CG_TIPO_COMPRA")==null)?"":rsReporte.getString("CG_TIPO_COMPRA");
			clavePresupuestaria = (rsReporte.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rsReporte.getString("CG_CLAVE_PRESUPUESTARIA");
			periodo 			= (rsReporte.getString("CG_PERIODO")==null)?"":rsReporte.getString("CG_PERIODO");
		}
	
		// Fodea 002 - 2010
		if(!ocultarDoctosAplicados){
			
			//esNotaDeCreditoSimpleAplicada						= ( (CS_DSCTO_ESPECIAL != null && CS_DSCTO_ESPECIAL.equals("C")) && (IC2_ESTATUS_DOCTO == 3 || IC2_ESTATUS_DOCTO == 4 || IC2_ESTATUS_DOCTO == 11) && BeanParamDscto.existeDocumentoAsociado(String.valueOf(IC_DOCUMENTO)) )?true:false;
			//esDocumentoConNotaDeCreditoSimpleAplicada		= ( (CS_DSCTO_ESPECIAL != null && !CS_DSCTO_ESPECIAL.equals("C")) && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(String.valueOf(IC_DOCUMENTO)))?true:false;
		
			//esNotaDeCreditoAplicada 							= ( (CS_DSCTO_ESPECIAL != null && CS_DSCTO_ESPECIAL.equals("C")) && (IC2_ESTATUS_DOCTO == 3 || IC2_ESTATUS_DOCTO == 4 || IC2_ESTATUS_DOCTO == 11) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(String.valueOf(IC_DOCUMENTO)))?true:false;
			//esDocumentoConNotaDeCreditoMultipleAplicada	= ( (CS_DSCTO_ESPECIAL != null && !CS_DSCTO_ESPECIAL.equals("C")) && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(String.valueOf(IC_DOCUMENTO)))?true:false;
			
			esNotaDeCreditoSimpleAplicada 						= "true".equals(rsReporte.getString("existeDocumentoAsociado"))		?true:false;
			esDocumentoConNotaDeCreditoSimpleAplicada 		= "true".equals(rsReporte.getString("esDocConNotaDeCredSimpleApl"))	?true:false;
			
			esNotaDeCreditoAplicada 								= "true".equals(rsReporte.getString("existeRefEnComrelNotaDocto"))	?true:false;
			esDocumentoConNotaDeCreditoMultipleAplicada		= "true".equals(rsReporte.getString("esDocConNotaDeCredMultApl"))		?true:false;
	
			if(esNotaDeCreditoSimpleAplicada){
				numeroDocumento									= BeanSeleccionDocumento.getNumeroDoctoAsociado(String.valueOf(IC_DOCUMENTO));
			}else if(esDocumentoConNotaDeCreditoSimpleAplicada){
				numeroDocumento									= BeanSeleccionDocumento.getNumeroNotaCreditoAsociada(String.valueOf(IC_DOCUMENTO));
			}
			
		}
		String contratoArch = (rsReporte.getString("CONTRATO")==null)?"":rsReporte.getString("CONTRATO");
		String copadeArch = (rsReporte.getString("COPADE")==null)?"":rsReporte.getString("COPADE");
					
					
		if(registros==0){
			contenidoArchivo.append("Nombre del proveedor,Número del documento,Número de Acuse,Fecha de Emisión,Fecha de Vencimiento");
			if(!"".equals(sFechaVencPyme)) {
				contenidoArchivo.append(",Fecha de Vencimiento Proveedor");
			}
			contenidoArchivo.append(",Moneda");
			if(!layOutAserca) {
				if(bTipoFactoraje){
					contenidoArchivo.append(",Tipo Factoraje");
				}
				contenidoArchivo.append(",Monto,Estatus,Intermediario Financiero,Número de solicitud"+noms_cad+",Número de Proveedor,Referencia,Clave de Estatus,Porcentaje de Descuento,Monto a Descontar");
			}
			// Fodea 002 - 2010
			if(!ocultarDoctosAplicados){
				contenidoArchivo.append(",Doctos Aplicados a Nota de Credito");
			}
	
			if(!layOutAserca){
				if(bOperaFactorajeDistribuido || bOperaFactorajeVencInfonavit){
					if(bandera_neto_a_recibir_pyme){
						contenidoArchivo.append(",Neto Recibir Pyme");
					}
					contenidoArchivo.append(",Beneficiario,% Beneficiario,Importe a Recibir Beneficiario");
				}
	/*//FODEA 005 - 2009 ACF
				if(ic_estatus_docto.equals("26")) {
					contenidoArchivo.append(","+mensaje_param.getMensaje("13consulta1.FechaDescto", sesIdiomaUsuario));
				}
	*/
			} else {
				contenidoArchivo.append(",Monto,Porcentaje de Descuento,Monto a Descontar,Estatus"+noms_cad);
			}
			if(estaHabilitadoNumeroSIAFF){
				contenidoArchivo.append(",Digito Identificador");
			}
	
			if(banderaTablaDoctos.equals("1")) {
				contenidoArchivo.append(",Fecha de Recepción de Bienes y Servicios,Tipo de Compra (procedimiento),Clasificador por Objeto del Gasto,Plazo Máximo");
			}
	
			if(bOperaFactConMandato){
				contenidoArchivo.append(",Mandante");  // FODEA 023 2009
			}
			
			if(ic_estatus_docto.equals("26")){contenidoArchivo.append(",Fecha Registro Operación");}//FODEA 005 - 2009 ACF
	
			// Fodea 040 - 2011 By JSHD
			if(bMostrarFechaAutorizacionIFInfoDoctos){
				contenidoArchivo.append(",Fecha de Autorización IF");
			}
                        
                        //QC-2018
                        if(bOperaFactorajeDistribuido){
                            contenidoArchivo.append(",Validación de Documentos EPO");
                        }
			if ("ADMIN PEMEX AMP".equals(strPerfil)  ) {
				contenidoArchivo.append(", Contrato ");
				contenidoArchivo.append(", COPADE ");
			}
	  
			contenidoArchivo.append( "\n");
		} //if registros == 0"
		if (IC2_ESTATUS_DOCTO == 2 || IC2_ESTATUS_DOCTO == 5 || IC2_ESTATUS_DOCTO == 6 || IC2_ESTATUS_DOCTO == 7 || IC2_ESTATUS_DOCTO == 9 || IC2_ESTATUS_DOCTO == 10) {
			if(NO_MONEDA == 1) {
				dblPorciento = new Double(strAforo).doubleValue() * 100;
			} else
				if(NO_MONEDA == 54) {
					dblPorciento = new Double(strAforoDL).doubleValue() * 100;
				}
		}else{
			dblPorciento = FN_PORC_ANTICIPO;
		}
		dblMontoDescuento = FN_MONTO * (dblPorciento / 100); //FN_MONTO_DSCTO;
		/* Para factoraje Vencido */
		CS_DSCTO_ESPECIAL = (CS_DSCTO_ESPECIAL==null)?"":CS_DSCTO_ESPECIAL.trim();
		/* si factoraje vencido y distribuido: se aplica 100% de anticipo */
		if(CS_DSCTO_ESPECIAL.equals("V") || CS_DSCTO_ESPECIAL.equals("D") || CS_DSCTO_ESPECIAL.equals("C") || CS_DSCTO_ESPECIAL.equals("I"))  {
			dblMontoDescuento=FN_MONTO;
			dblPorciento = 100;
		}
	/*FODEA 005 - 2009 ACF
		if(IC2_ESTATUS_DOCTO == 26){//26=Programado
			if(NO_MONEDA == 1) {
				dblPorciento = new Double(strAforo).doubleValue() * 100;
			}else if(NO_MONEDA == 54) {
				dblPorciento = new Double(strAforoDL).doubleValue() * 100;
			}
			dblMontoDescuento = FN_MONTO * (dblPorciento / 100);
		}
	*/
		CS_DSCTO_ESPECIAL = tipoFactorajeDesc ;
		
		 val++;
		 gen_archivo = true;
		 if(IC_PYME!=null && !IC_PYME.equals("")) { IC_PYME=IC_PYME.replace(',',' '); }
		 if(IC_IF == null){IC_IF="";}else{ IC_IF=IC_IF.replace(',',' '); } if(CT_REFERENCIA==null){CT_REFERENCIA="";}
	
		 contenidoArchivo.append(IC_PYME+","+IG_NUMERO_DOCTO+","+CC_ACUSE+","+DF_FECHA_DOCTO+","+DF_FECHA_VENC+",");
		
		if(!"".equals(sFechaVencPyme)){
			contenidoArchivo.append(DF_FECHA_VENC_PYME+",");
		}
		contenidoArchivo.append(IC_MONEDA+",");
		
		if(!layOutAserca) {
			if(bTipoFactoraje) { contenidoArchivo.append( CS_DSCTO_ESPECIAL + ",");}
			contenidoArchivo.append(Comunes.formatoDecimal(FN_MONTO,2,false)+","+IC_ESTATUS_DOCTO+","+IC_IF+","+IC_FOLIO+linea_cd+","+NO_INT_PYME+","+CT_REFERENCIA+","+IC2_ESTATUS_DOCTO+","+Comunes.formatoDecimal(String.valueOf(dblPorciento),0,false)+","+dblMontoDescuento);
		}
		else {
			contenidoArchivo.append(
				Comunes.formatoDecimal(FN_MONTO,2,false)+","+
				Comunes.formatoDecimal(String.valueOf(dblPorciento),0,false)+","+
				Comunes.formatoDecimal(FN_MONTO,2,false)+","+
				IC_ESTATUS_DOCTO+linea_cd);
		}
		
		// Fodea 002 - 2010 
		if(!ocultarDoctosAplicados){
			contenidoArchivo.append(",");
			// Notas de Credito Simple   
			if(esNotaDeCreditoSimpleAplicada || esDocumentoConNotaDeCreditoSimpleAplicada ){contenidoArchivo.append("\""+numeroDocumento+"\"");}
			// Notas de Credito Multiple 
			if(esNotaDeCreditoAplicada || esDocumentoConNotaDeCreditoMultipleAplicada){contenidoArchivo.append("Si");}
		}
		
		if(bOperaFactorajeDistribuido || bOperaFactorajeVencInfonavit){
			if(bandera_neto_a_recibir_pyme && 0 != neto_a_recibir_pyme)
				contenidoArchivo.append(","+Comunes.formatoDecimal(neto_a_recibir_pyme,2,false));
			//else
				//contenidoArchivo += ", ";
				 contenidoArchivo.append(","+ beneficiario.replace(',',' '));
			if(0 != porciento_beneficiario)
				contenidoArchivo.append(","+porciento_beneficiario);
			else
				contenidoArchivo.append(", ");
			if(0 != importe_a_recibir_beneficiario)
				contenidoArchivo.append(","+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false));
			else
				contenidoArchivo.append(", ");
		}
	/*//FODEA 005 - 2009 ACF
		if(ic_estatus_docto.equals("26")) {
			contenidoArchivo.append(","+df_programacion);
		}
	*/
		if(estaHabilitadoNumeroSIAFF){
			contenidoArchivo.append((","+numeroSIAFF));
		}
	
		if(banderaTablaDoctos.equals("1")) {
			contenidoArchivo.append(","+fechaEntrega+","+tipoCompra+","+clavePresupuestaria+","+periodo);
		}
		if(bOperaFactConMandato){
			contenidoArchivo.append(","+mandante);
		}
		
		if(ic_estatus_docto.equals("26")){contenidoArchivo.append(","+fecha_registro_operacion);}//FODEA 005 - 2009 ACF
		
		//Fodea 040 - 2011 By JSHD
		if(bMostrarFechaAutorizacionIFInfoDoctos){
			contenidoArchivo.append(","+fechaAutorizacionIF);
		}
                
                //QC-2018
		if(bOperaFactorajeDistribuido){
                    contenidoArchivo.append(","+validacionJUR);
                }
		if ("ADMIN PEMEX AMP".equals(strPerfil)  ) {
			contenidoArchivo.append(","+contratoArch);
			contenidoArchivo.append(","+copadeArch);
		}
		contenidoArchivo.append("\n");
		linea_cd = "";
	
		registros++;
	} // while
	rsReporte.close();

	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	} else {
		nombreArchivo = archivo.nombre;
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	if (con.hayConexionAbierta() == true) {
		con.cierraConexionDB();
	}
}

%>
<%=jsonObj%>

<%!
	public String getNumeroSIAFF(String ic_epo,String ic_documento){
		return getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(ic_documento);
	}

	public String getNumeroDeCuatroDigitos(String numero){

		String 			claveEPO 	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();

      for(int i = 0;i<(4-claveEPO.length());i++){
			resultado.append("0");
      }
      resultado.append(claveEPO);

      return resultado.toString();
	}

  	public String getNumeroDeOnceDigitos(String numero){

		String 			claveDocto	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();

      for(int i = 0;i<(11-claveDocto.length());i++){
			resultado.append("0");
      }
      resultado.append(claveDocto);

      return resultado.toString();
	}
%>