<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,java.math.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%

CQueryHelperExtJS	queryHelper = null;
JSONObject jsonObj = new JSONObject();

String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
String ic_pyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
String ig_numero_docto = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto");
String df_fecha_docto_de = (request.getParameter("df_fecha_docto_de") == null) ? "" : request.getParameter("df_fecha_docto_de");
String df_fecha_docto_a = (request.getParameter("df_fecha_docto_a") == null) ? "" : request.getParameter("df_fecha_docto_a");
String df_fecha_venc_de = (request.getParameter("df_fecha_venc_de") == null) ? "" : request.getParameter("df_fecha_venc_de");
String df_fecha_venc_a = (request.getParameter("df_fecha_venc_a") == null) ? "" : request.getParameter("df_fecha_venc_a");
String ic_moneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
String fn_monto_de = (request.getParameter("fn_monto_de") == null) ? "" : request.getParameter("fn_monto_de");
String fn_monto_a = (request.getParameter("fn_monto_a") == null) ? "" : request.getParameter("fn_monto_a");
String ic_estatus_docto = (request.getParameter("ic_estatus_docto") == null) ? "" : request.getParameter("ic_estatus_docto");
String df_fecha_seleccion_de = (request.getParameter("df_fecha_seleccion_de") == null) ? "" : request.getParameter("df_fecha_seleccion_de");
String df_fecha_seleccion_a = (request.getParameter("df_fecha_seleccion_a") == null) ? "" : request.getParameter("df_fecha_seleccion_a");
String in_tasa_aceptada_de = (request.getParameter("in_tasa_aceptada_de") == null) ? "" : request.getParameter("in_tasa_aceptada_de");
String in_tasa_aceptada_a = (request.getParameter("in_tasa_aceptada_a") == null) ? "" : request.getParameter("in_tasa_aceptada_a");
String ord_if = (request.getParameter("ord_if") == null) ? "" : request.getParameter("ord_if");
String ord_pyme = (request.getParameter("ord_pyme") == null) ? "" : request.getParameter("ord_pyme");
String ord_epo = (request.getParameter("ord_epo") == null) ? "" : request.getParameter("ord_epo");
String ord_fec_venc = (request.getParameter("ord_fec_venc") == null) ? "" : request.getParameter("ord_fec_venc");
String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
String strGenerarArchivo = (request.getParameter("txtGeneraArchivo") == null) ? "" : request.getParameter("txtGeneraArchivo");
String numero_siaff= (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
String cc_acuse = (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
String no_docto = (request.getParameter("no_docto")==null)?"":request.getParameter("no_docto");
String camp_dina1= (request.getParameter("camp_dina1") == null) ? "" : request.getParameter("camp_dina1");
String camp_dina2= (request.getParameter("camp_dina2") == null) ? "" : request.getParameter("camp_dina2");

// Fodea 002 - 2010
boolean  ocultarDoctosAplicados = (
			ic_estatus_docto.equals("") 	|| 
			ic_estatus_docto.equals("1") 	|| 
			ic_estatus_docto.equals("2") 	|| 
			ic_estatus_docto.equals("5")	|| 
			ic_estatus_docto.equals("6") 	|| 
			ic_estatus_docto.equals("7") 	|| 
			ic_estatus_docto.equals("9")	|| 
			ic_estatus_docto.equals("10") || 
			ic_estatus_docto.equals("21") || 
			ic_estatus_docto.equals("23")	|| 
			ic_estatus_docto.equals("28") || 
			ic_estatus_docto.equals("29")	|| 
			ic_estatus_docto.equals("30")	|| 
			ic_estatus_docto.equals("31") )?true:false;

String banderaTablaDoctos = (request.getParameter("txtbanderaTablaDoctos") == null) ? "" : request.getParameter("txtbanderaTablaDoctos");
String fechaEntrega = "", tipoCompra = "", clavePresupuestaria = "", periodo = "";

int nRegistros = 0, numRegistros = 0, numRegistrosMN = 0, numRegistrosDL = 0;
BigDecimal montoTotalMN = new BigDecimal("0.00");
BigDecimal montoTotalDL = new BigDecimal("0.00");
BigDecimal montoTotalDsctoMN = new BigDecimal("0.00");
BigDecimal montoTotalDsctoDL = new BigDecimal("0.00");

int vtmn = 0, vesp = 0, vnumd = 0;
int cont = 0;
int coma=0;
double total_dolares = 0, total_mn = 0;
int total_doctos_dol = 0, total_doctos_mn = 0, val = 0;
boolean gen_archivo=false, dolaresDocto = false, nacionalDocto = false;
int IC_DOCUMENTO = 0, IC_EPO = 0;
String IC_PYME = "", CC_ACUSE = "", IG_NUMERO_DOCTO = "", DF_FECHA_DOCTO = "", DF_FECHA_VENC = "", DF_FECHA_VENC_PYME = "";
String IC_MONEDA = "", CS_DSCTO_ESPECIAL = "", IC_ESTATUS_DOCTO = "", IC_IF = "", IC_FOLIO = "";
double FN_MONTO = 0, FN_PORC_ANTICIPO = 0, FN_MONTO_DSCTO = 0;
int IC2_ESTATUS_DOCTO = 0, NO_MONEDA = 0; //DETALLES = 0;
String NO_INT_PYME="", CT_REFERENCIA="", CS_CAMBIO_IMPORTE="";
String CG_CAMPO1="", CG_CAMPO2="", CG_CAMPO3="", CG_CAMPO4="", CG_CAMPO5="";//, linea_cd="";
//String cgCampo1 = "", cgCampo2 = "", cgCampo3 = "", cgCampo4 = "", cgCampo5 = "";
String nombreTipoFactoraje="",icDocumento="";
String numeroSIAFF="";
String mandante ="";
String tipoFactorajeDesc  =  "";
String fecha_registro_operacion = "";//FODEA 005 - 2009 ACF
String fechaAutorizacionIF = ""; // Fodea 040 - 2011 By JSHD
double dblPorciento = 0, dblMontoDescuento = 0, dblTotalDescuentoPesos = 0, dblTotalDescuentoDolares = 0;
String beneficiario = "";
double porciento_beneficiario = 0;
double importe_a_recibir_beneficiario = 0;
double neto_a_recibir_pyme = 0;
boolean bandera_neto_a_recibir_pyme = false ;
String sFechaVencPyme="";
boolean muestraNegociables = false;
String cd17="", cd18="", cd19="", cd20="", cd21="";
//CQueryHelper	queryHelper = null;
//ResultSet		rsReporte	= null;
boolean bOperaFactorajeVencido = false;
boolean bOperaFactorajeDistribuido = false;
boolean bOperaFactorajeNotaDeCredito = false;
boolean bOperaFactorajeVencInfonavit = false;

boolean bFactorajeIf						= false;

// Fodea 002 - 2010
boolean esNotaDeCreditoAplicada								= false;
boolean esDocumentoConNotaDeCreditoMultipleAplicada	= false;

boolean esNotaDeCreditoSimpleAplicada						= false;
boolean esDocumentoConNotaDeCreditoSimpleAplicada		= false;


String  numeroDocumento											= "";	

/************A S E R C A*/
//String ic_epo_aserca = (String)session.getAttribute("sesEpoAserca");
String ic_epo_aserca = "314";
boolean layOutAserca = false;
if(ic_epo_aserca.equals(iNoCliente))
	layOutAserca = true;
/************A S E R C A*/

try {

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

	sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(iNoCliente);
	boolean estaHabilitadoNumeroSIAFF = BeanParamDscto.estaHabilitadoNumeroSIAFF(iNoCliente);
	queryHelper = new CQueryHelperExtJS(new ConsDoctosDE());
	Registros regCamposAdicionales = null;
	
	muestraNegociables = "2".equals(ic_estatus_docto)?true:false;
	if(ic_pyme.equals("") && cc_acuse.equals("") && no_docto.equals("") &&
			ic_moneda.equals("") && df_fecha_docto_de.equals("") &&
			df_fecha_docto_a.equals("") && fn_monto_de.equals("") && fn_monto_a.equals("") &&
			df_fecha_venc_de.equals("") && df_fecha_venc_a.equals("") && ic_if.equals("") &&
			ic_estatus_docto.equals("") && camp_dina1.equals("") && camp_dina2.equals("")) {
		muestraNegociables = true;
	}
	if (!ic_moneda.equals("") && ic_estatus_docto.equals("")) {
		muestraNegociables = true;
	}

   // Mandante  Fodea 023
	boolean bOperaFactConMandato=false;
	boolean bTipoFactoraje=false;
	boolean bMostrarFechaAutorizacionIFInfoDoctos = false; // Fodea 040 - 2011
	
	Hashtable alParamEPO1 = new Hashtable(); 
	alParamEPO1 = BeanParamDscto.getParametrosEPO(iNoCliente,1);
	if (alParamEPO1!=null) {
		  bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		   bOperaFactorajeVencido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
		   bOperaFactorajeDistribuido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
		   bOperaFactorajeNotaDeCredito = ("N".equals(alParamEPO1.get("OPERA_NOTAS_CRED").toString()))?false:true; 
			bOperaFactorajeVencInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true; //FODEA 042 - Agosto/2009
			bFactorajeIf								=	("N".equals(alParamEPO1.get("FACTORAJE_IF").toString()))?false:true; //FODEA Noviembre 2012
			bMostrarFechaAutorizacionIFInfoDoctos = "S".equals((String) alParamEPO1.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS") )?true:false; // Fodea 040 - 2011
	}

	bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaDeCredito || bOperaFactConMandato || bOperaFactorajeVencInfonavit||bFactorajeIf)?true:false;

	int totalcd = 0;
	totalcd = BeanSeleccionDocumento.getTotalCamposAdicionales(iNoCliente);
	if(totalcd >=1) {
		regCamposAdicionales = new Registros();
		regCamposAdicionales = BeanParamDscto.getCamposAdicionales(iNoCliente);
	}

	int start = 0;
	int limit = 0;
	int nRow = 0;
	Registros rsReporte = new Registros();
	ResultSet rsReportepdf = null;
	String informacion = (request.getParameter("informacion")==null)?"":request.getParameter("informacion").trim();

	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
		// Limita el número de registros a 1000
		if(limit > 1000){
			limit = 1000;
		}
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	if(informacion.equals("Genera_PDF")){
		try{
			AccesoDB con = new AccesoDB();
			con.conexionDB();
			rsReportepdf = queryHelper.getCreateFile(request,con);
		} catch(Exception e){
			rsReporte = queryHelper.getPageResultSet(request,start,limit);
		}
	} else{
		rsReporte = queryHelper.getPageResultSet(request,start,limit);
	}

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	List lEncabezados = new ArrayList();
	int tam = 0, totCol = 0,	nCol = 0;

if(!informacion.equals("Genera_PDF")){
	while(rsReporte.next()){
		IC_DOCUMENTO       = Integer.parseInt((rsReporte.getString(1)==null)?"0":rsReporte.getString(1));
		icDocumento        = (rsReporte.getString(1)==null)?"0":rsReporte.getString(1);
		IC_PYME            = (rsReporte.getString(2)==null)?"":rsReporte.getString(2);
		IC_EPO             = Integer.parseInt((rsReporte.getString(3)==null)?"0":rsReporte.getString(3));
		CC_ACUSE           = (rsReporte.getString(4)==null)?"":rsReporte.getString(4);
		IG_NUMERO_DOCTO    = (rsReporte.getString(5)==null)?"":rsReporte.getString(5);
		DF_FECHA_DOCTO     = (rsReporte.getString(6)==null)?"":rsReporte.getString(6);
		DF_FECHA_VENC      = (rsReporte.getString(7)==null)?"":rsReporte.getString(7);
		IC_MONEDA          = (rsReporte.getString(8)==null)?"":rsReporte.getString(8);
		FN_MONTO           = Double.parseDouble((rsReporte.getString(9)==null)?"0":rsReporte.getString(9));
		CS_DSCTO_ESPECIAL  = (rsReporte.getString(10)==null)?"":rsReporte.getString(10);
		FN_PORC_ANTICIPO   = Double.parseDouble((rsReporte.getString(11)==null)?"0":rsReporte.getString(11));
		FN_MONTO_DSCTO     = Double.parseDouble((rsReporte.getString(12)==null)?"0":rsReporte.getString(12));
		IC_ESTATUS_DOCTO   = (rsReporte.getString(13)==null)?"":rsReporte.getString(13);
		IC_IF              = (rsReporte.getString(14)==null)?"":rsReporte.getString(14);
		IC_FOLIO           = (rsReporte.getString(15)==null)?"":rsReporte.getString(15);
		IC2_ESTATUS_DOCTO  = Integer.parseInt((rsReporte.getString(16)==null)?"0":rsReporte.getString(16));
		NO_MONEDA          = Integer.parseInt((rsReporte.getString(17)==null)?"0":rsReporte.getString(17));
		NO_INT_PYME        = (rsReporte.getString(18)==null)?"":rsReporte.getString(18);
		CT_REFERENCIA      = (rsReporte.getString(19)==null)?"":rsReporte.getString(19);
		CS_CAMBIO_IMPORTE  = (rsReporte.getString(20)==null)?"":rsReporte.getString(20).trim();
		//DETALLES         = rsReporte.getInt(21);
		//df_programacion  = (rsReporte.getString("df_programacion")==null)?"":rsReporte.getString("df_programacion").trim();
		DF_FECHA_VENC_PYME = (rsReporte.getString("DF_FECHA_VENC_PYME")==null)?"":rsReporte.getString("DF_FECHA_VENC_PYME");
	
		mandante	= (rsReporte.getString(38)==null)?"":rsReporte.getString(38); // Fodea 023
		
		tipoFactorajeDesc = (rsReporte.getString("TIPO_FACTORAJE")==null)?"":rsReporte.getString("TIPO_FACTORAJE").trim();
		fecha_registro_operacion = rsReporte.getString("fecha_registro_operacion")==null?"":rsReporte.getString("fecha_registro_operacion");//FODEA 005 - 2009 ACF
		fechaAutorizacionIF      = rsReporte.getString("fecha_autorizacion_if")   ==null?"":rsReporte.getString("fecha_autorizacion_if");   //FODEA 040 - 2011 By JSHD

		numeroSIAFF = getNumeroSIAFF(Integer.toString(IC_EPO),Integer.toString(IC_DOCUMENTO));
		beneficiario = rsReporte.getString("beneficiario")==null?"":rsReporte.getString("beneficiario");
		porciento_beneficiario = Double.parseDouble(rsReporte.getString("fn_porc_beneficiario")==null?"0":rsReporte.getString("fn_porc_beneficiario"));
		importe_a_recibir_beneficiario = Double.parseDouble(rsReporte.getString("RECIBIR_BENEF")==null?"0":rsReporte.getString("RECIBIR_BENEF"));
		if(bandera_neto_a_recibir_pyme)
			neto_a_recibir_pyme = Double.parseDouble(rsReporte.getString("NETO_REC_PYME")==null?"0":rsReporte.getString("NETO_REC_PYME"));
	
		if( banderaTablaDoctos.equals("1") ){
			fechaEntrega 		= (rsReporte.getString("DF_ENTREGA")==null)?"":rsReporte.getString("DF_ENTREGA");
			tipoCompra 			= (rsReporte.getString("CG_TIPO_COMPRA")==null)?"":rsReporte.getString("CG_TIPO_COMPRA");
			clavePresupuestaria = (rsReporte.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rsReporte.getString("CG_CLAVE_PRESUPUESTARIA");
			periodo 			= (rsReporte.getString("CG_PERIODO")==null)?"":rsReporte.getString("CG_PERIODO");
		}
		nombreTipoFactoraje = (rsReporte.getString("CS_DSCTO_ESPECIAL")==null)?"":rsReporte.getString("CS_DSCTO_ESPECIAL");
		// Fodea 002 - 2010
		if(!ocultarDoctosAplicados){
			esNotaDeCreditoSimpleAplicada						= ( nombreTipoFactoraje.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeDocumentoAsociado(icDocumento) )?true:false;
			esDocumentoConNotaDeCreditoSimpleAplicada		= ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(icDocumento))?true:false;
	
			esNotaDeCreditoAplicada 							= (nombreTipoFactoraje.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(icDocumento))?true:false;
			esDocumentoConNotaDeCreditoMultipleAplicada	= (!nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(icDocumento))?true:false;
	
			if(esNotaDeCreditoSimpleAplicada){
				numeroDocumento									= BeanSeleccionDocumento.getNumeroDoctoAsociado(icDocumento); 
			}else if(esDocumentoConNotaDeCreditoSimpleAplicada){
				numeroDocumento									= BeanSeleccionDocumento.getNumeroNotaCreditoAsociada(icDocumento);
			}
		}
		if(nRegistros==0){
			lEncabezados.add("A");
			lEncabezados.add("Nombre Proveedor");
			lEncabezados.add("No. Documento");
			lEncabezados.add("No. Acuse");
			lEncabezados.add("Fecha Emisión");
			lEncabezados.add("Fecha Vencimiento");
			if(!"".equals(sFechaVencPyme)) {
				lEncabezados.add("Fecha Vencimiento Proveedor");
			}
			lEncabezados.add("Moneda");
			if(!layOutAserca) {
				if(bTipoFactoraje){
					lEncabezados.add("Tipo Factoraje");
				}
				lEncabezados.add("Monto");
				lEncabezados.add("Estatus");
				lEncabezados.add("IF");
				lEncabezados.add("No. Solicitud");
				if (regCamposAdicionales != null){
					while (regCamposAdicionales.next()){
						lEncabezados.add(regCamposAdicionales.getString("NOMBRE_CAMPO").trim());
					}
				}
				lEncabezados.add("No. Proveedor");
			}
			pdfDoc.setTable(lEncabezados, "formasmenB", 100);
			totCol = lEncabezados.size();
			pdfDoc.setCell("B", "formasmenB", ComunesPDF.CENTER);
			nCol++;
			if(!layOutAserca) {
				pdfDoc.setCell("Referencia", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Clave Estatus", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell(" % Descuento", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar", "formasmenB", ComunesPDF.CENTER);
				nCol+=4;
				//lEncabezados.add("Referencia");
				//lEncabezados.add("Clave Estatus");
				//lEncabezados.add(" % Descuento");
				//lEncabezados.add("Monto a Descontar");
			}
			// Fodea 002 - 2010
			if(!ocultarDoctosAplicados){
				pdfDoc.setCell("Doctos Aplicados a Nota de Credito", "formasmenB", ComunesPDF.CENTER);
				nCol++;
				//lEncabezados.add("Doctos Aplicados a Nota de Credito");
			}
	
			if(!layOutAserca){
				if(bOperaFactorajeDistribuido || bOperaFactorajeVencInfonavit){
					if(bandera_neto_a_recibir_pyme){
						//lEncabezados.add("Neto Recibir Pyme");
						pdfDoc.setCell("Neto Recibir Pyme", "formasmenB", ComunesPDF.CENTER);
						nCol++;
					}
					pdfDoc.setCell("Beneficiario", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell(" % Beneficiario", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir BeneficiarioB", "formasmenB", ComunesPDF.CENTER);
					nCol+=3;
					//lEncabezados.add("Beneficiario");
					//lEncabezados.add(" % Beneficiario");
					//lEncabezados.add("Importe a Recibir Beneficiario");
				}
	/*//FODEA 005 - 2009 ACF
				if(ic_estatus_docto.equals("26")) {
					contenidoArchivo.append(","+mensaje_param.getMensaje("13consulta1.FechaDescto", sesIdiomaUsuario));
				}
	*/
			} else {
				pdfDoc.setCell("Monto", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell(" % Descuento", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus", "formasmenB", ComunesPDF.CENTER);
				nCol+=4;
				//lEncabezados.add("Monto");
				//lEncabezados.add(" % Descuento");
				//lEncabezados.add("Monto a Descontar");
				//lEncabezados.add("Estatus");
				if (regCamposAdicionales != null){
					while (regCamposAdicionales.next()){
						pdfDoc.setCell(regCamposAdicionales.getString("NOMBRE_CAMPO").trim(), "formasmenB", ComunesPDF.CENTER);
						nCol++;
						//lEncabezados.add(regCamposAdicionales.getString("NOMBRE_CAMPO").trim());
					}
				}
			}
			if(estaHabilitadoNumeroSIAFF){
				pdfDoc.setCell("Digito Identificador", "formasmenB", ComunesPDF.CENTER);
				nCol++;
				//lEncabezados.add("Digito Identificador");
			}
	
			if(banderaTablaDoctos.equals("1")) {
				pdfDoc.setCell("Fecha de Recepción de Bienes y Servicios", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Compra (procedimiento)", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Clasificador por Objeto del Gasto", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo Máximo", "formasmenB", ComunesPDF.CENTER);
				nCol+=4;
				//lEncabezados.add("Fecha de Recepción de Bienes y Servicios");
				//lEncabezados.add("Tipo de Compra (procedimiento)");
				//lEncabezados.add("Clasificador por Objeto del Gasto");
				//lEncabezados.add("Plazo Máximo");
			}
		  
			if(bOperaFactConMandato){
				pdfDoc.setCell("Mandante", "formasmenB", ComunesPDF.CENTER);
				nCol++;
				//lEncabezados.add("Mandante");
			}

			if(ic_estatus_docto.equals("26")){
				pdfDoc.setCell("Fecha Registro Operación", "formasmenB", ComunesPDF.CENTER);
				nCol++;
				//lEncabezados.add("Fecha Registro Operación");
			}//FODEA 005 - 2009 ACF

			// Fodea 040 - 2011 By JSHD
			if(bMostrarFechaAutorizacionIFInfoDoctos){
				pdfDoc.setCell("Fecha Autorización IF", "formasmenB", ComunesPDF.CENTER);
				nCol++;
				//lEncabezados.add("Fecha Autorización IF");
			}
			tam = totCol-nCol;
			if (tam > 0){
				pdfDoc.setCell("", "formasmenB", ComunesPDF.CENTER,tam);
			}
			//pdfDoc.setTable(lEncabezados, "formasmenB", 100);
		} //if registros == 0"
		if (IC2_ESTATUS_DOCTO == 2 || IC2_ESTATUS_DOCTO == 5 || IC2_ESTATUS_DOCTO == 6 || IC2_ESTATUS_DOCTO == 7 || IC2_ESTATUS_DOCTO == 9 || IC2_ESTATUS_DOCTO == 10) {
			if(NO_MONEDA == 1) {
				dblPorciento = new Double(strAforo).doubleValue() * 100;
			} else
				if(NO_MONEDA == 54) {
					dblPorciento = new Double(strAforoDL).doubleValue() * 100;
				}
		}
		else
			dblPorciento = FN_PORC_ANTICIPO;
	
		dblMontoDescuento = FN_MONTO * (dblPorciento / 100); //FN_MONTO_DSCTO;
		/* Para factoraje Vencido */
		CS_DSCTO_ESPECIAL = (CS_DSCTO_ESPECIAL==null)?"":CS_DSCTO_ESPECIAL.trim();
		/* si factoraje vencido y distribuido: se aplica 100% de anticipo */
		if(CS_DSCTO_ESPECIAL.equals("V") || CS_DSCTO_ESPECIAL.equals("D") || CS_DSCTO_ESPECIAL.equals("C") || CS_DSCTO_ESPECIAL.equals("I"))  {
			dblMontoDescuento=FN_MONTO;
			dblPorciento = 100;
		}
	/*FODEA 005 - 2009 ACF
		if(IC2_ESTATUS_DOCTO == 26){//26=Programado
			if(NO_MONEDA == 1) {
				dblPorciento = new Double(strAforo).doubleValue() * 100;
			}else if(NO_MONEDA == 54) {
				dblPorciento = new Double(strAforoDL).doubleValue() * 100;
			}
			dblMontoDescuento = FN_MONTO * (dblPorciento / 100);
		}
	*/
		CS_DSCTO_ESPECIAL = tipoFactorajeDesc ;

		 val++;
		 gen_archivo = true;
		if(IC_PYME!=null && !IC_PYME.equals("")) { IC_PYME=IC_PYME.replace(',',' '); }
		if(IC_IF == null){IC_IF="";}else{ IC_IF=IC_IF.replace(',',' '); } if(CT_REFERENCIA==null){CT_REFERENCIA="";}
		pdfDoc.setCell("A", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell(IC_PYME, "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(IG_NUMERO_DOCTO, "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(CC_ACUSE, "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(DF_FECHA_DOCTO, "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(DF_FECHA_VENC, "formasmen", ComunesPDF.CENTER);
		
		if(!"".equals(sFechaVencPyme)){
			pdfDoc.setCell(DF_FECHA_VENC_PYME, "formasmen", ComunesPDF.CENTER);
		}
		pdfDoc.setCell(IC_MONEDA, "formasmen", ComunesPDF.CENTER);
		if(!layOutAserca) {
			if(bTipoFactoraje) { pdfDoc.setCell(CS_DSCTO_ESPECIAL, "formasmen", ComunesPDF.CENTER);}
			pdfDoc.setCell(Comunes.formatoDecimal(FN_MONTO,2,false), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(IC_ESTATUS_DOCTO, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(IC_IF, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(IC_FOLIO, "formasmen", ComunesPDF.CENTER);

			if (regCamposAdicionales != null){
				cont = 1;
				while (regCamposAdicionales.next()){
					if ( cont == 1 ) {
						CG_CAMPO1 = (rsReporte.getString(22)==null)?"":rsReporte.getString(22).trim();
						pdfDoc.setCell(CG_CAMPO1,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 2) {
						CG_CAMPO2 = (rsReporte.getString(23)==null)?"":rsReporte.getString(23).trim();
						pdfDoc.setCell(CG_CAMPO2,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 3) {
						CG_CAMPO3 = (rsReporte.getString(24)==null)?"":rsReporte.getString(24).trim();
						pdfDoc.setCell(CG_CAMPO3,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 4) {
						CG_CAMPO4 = (rsReporte.getString(25)==null)?"":rsReporte.getString(25).trim();
						pdfDoc.setCell(CG_CAMPO4,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 5) {
						CG_CAMPO5 = (rsReporte.getString(26)==null)?"":rsReporte.getString(26).trim();
						pdfDoc.setCell(CG_CAMPO5,"formasmen",ComunesPDF.CENTER);
					}
					cont ++;
				}
			}
			pdfDoc.setCell(NO_INT_PYME, "formasmen", ComunesPDF.CENTER);
		}
		pdfDoc.setCell("B", "formasmenB", ComunesPDF.CENTER);
		if(!layOutAserca) {
			pdfDoc.setCell(CT_REFERENCIA, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Integer.toString(IC2_ESTATUS_DOCTO), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorciento),0,false), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(String.valueOf(dblMontoDescuento), "formasmen", ComunesPDF.CENTER);
		}
		if(!ocultarDoctosAplicados){
			if(esNotaDeCreditoSimpleAplicada || esDocumentoConNotaDeCreditoSimpleAplicada ){
				// Notas de Credito Simple   
				pdfDoc.setCell(numeroDocumento, "formasmen", ComunesPDF.CENTER);
			}else if(esNotaDeCreditoAplicada || esDocumentoConNotaDeCreditoMultipleAplicada){
				// Notas de Credito Multiple 
				pdfDoc.setCell("Si", "formasmen", ComunesPDF.CENTER);
			}else{
				pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER);
			}
		}
		if(!layOutAserca){
			if(bOperaFactorajeDistribuido || bOperaFactorajeVencInfonavit){
				if(bandera_neto_a_recibir_pyme && 0 != neto_a_recibir_pyme){
					pdfDoc.setCell(Comunes.formatoDecimal(neto_a_recibir_pyme,2,false), "formasmen", ComunesPDF.CENTER);
				}
				//else
					//contenidoArchivo += ", ";
					 pdfDoc.setCell(beneficiario, "formasmen", ComunesPDF.CENTER);
				if(0 != porciento_beneficiario)
					pdfDoc.setCell(String.valueOf(porciento_beneficiario), "formasmen", ComunesPDF.CENTER);
				else
					pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER);
				if(0 != importe_a_recibir_beneficiario)
					pdfDoc.setCell(Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false), "formasmen", ComunesPDF.CENTER);
				else
					pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER);
			}
		}else {
			pdfDoc.setCell(Comunes.formatoDecimal(FN_MONTO,2,false), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorciento),0,false), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(FN_MONTO,2,false), "formasmen", ComunesPDF.CENTER);
			if (regCamposAdicionales != null){
				cont = 1;
				while (regCamposAdicionales.next()){
					if ( cont == 1 ) {
						CG_CAMPO1 = (rsReporte.getString(22)==null)?"":rsReporte.getString(22).trim();
						pdfDoc.setCell(CG_CAMPO1,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 2) {
						CG_CAMPO2 = (rsReporte.getString(23)==null)?"":rsReporte.getString(23).trim();
						pdfDoc.setCell(CG_CAMPO2,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 3) {
						CG_CAMPO3 = (rsReporte.getString(24)==null)?"":rsReporte.getString(24).trim();
						pdfDoc.setCell(CG_CAMPO3,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 4) {
						CG_CAMPO4 = (rsReporte.getString(25)==null)?"":rsReporte.getString(25).trim();
						pdfDoc.setCell(CG_CAMPO4,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 5) {
						CG_CAMPO5 = (rsReporte.getString(26)==null)?"":rsReporte.getString(26).trim();
						pdfDoc.setCell(CG_CAMPO5,"formasmen",ComunesPDF.CENTER);
					}
					cont ++;
				}
			}
			//pdfDoc.setCell(IC_ESTATUS_DOCTO+linea_cd, "formasmen", ComunesPDF.CENTER);
		}

		if(estaHabilitadoNumeroSIAFF){
			pdfDoc.setCell(numeroSIAFF, "formasmen", ComunesPDF.CENTER);
		}
	
		if(banderaTablaDoctos.equals("1")) {
			pdfDoc.setCell(fechaEntrega, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(tipoCompra, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(clavePresupuestaria, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(periodo, "formasmen", ComunesPDF.CENTER);
		}
		if(bOperaFactConMandato){
			pdfDoc.setCell(mandante, "formasmen", ComunesPDF.CENTER);
		}
		
		if(ic_estatus_docto.equals("26")){pdfDoc.setCell(fecha_registro_operacion, "formasmen", ComunesPDF.CENTER);}//FODEA 005 - 2009 ACF
		
		//Fodea 040 - 2011 By JSHD
		if(bMostrarFechaAutorizacionIFInfoDoctos){
			pdfDoc.setCell(fechaAutorizacionIF, "formasmen", ComunesPDF.CENTER);
		}
		if (tam > 0){
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,tam);
		}
		nRegistros++;
	} // while
} else {
	int contadorLineas = 0;
	while(rsReportepdf.next() && contadorLineas < 1000){
		IC_DOCUMENTO       = Integer.parseInt((rsReportepdf.getString(1)==null)?"0":rsReportepdf.getString(1));
		icDocumento        = (rsReportepdf.getString(1)==null)?"0":rsReportepdf.getString(1);
		IC_PYME            = (rsReportepdf.getString(2)==null)?"":rsReportepdf.getString(2);
		IC_EPO             = Integer.parseInt((rsReportepdf.getString(3)==null)?"0":rsReportepdf.getString(3));
		CC_ACUSE           = (rsReportepdf.getString(4)==null)?"":rsReportepdf.getString(4);
		IG_NUMERO_DOCTO    = (rsReportepdf.getString(5)==null)?"":rsReportepdf.getString(5);
		DF_FECHA_DOCTO     = (rsReportepdf.getString(6)==null)?"":rsReportepdf.getString(6);
		DF_FECHA_VENC      = (rsReportepdf.getString(7)==null)?"":rsReportepdf.getString(7);
		IC_MONEDA          = (rsReportepdf.getString(8)==null)?"":rsReportepdf.getString(8);
		FN_MONTO           = Double.parseDouble((rsReportepdf.getString(9)==null)?"0":rsReportepdf.getString(9));
		CS_DSCTO_ESPECIAL  = (rsReportepdf.getString(10)==null)?"":rsReportepdf.getString(10);
		FN_PORC_ANTICIPO   = Double.parseDouble((rsReportepdf.getString(11)==null)?"0":rsReportepdf.getString(11));
		FN_MONTO_DSCTO     = Double.parseDouble((rsReportepdf.getString(12)==null)?"0":rsReportepdf.getString(12));
		IC_ESTATUS_DOCTO   = (rsReportepdf.getString(13)==null)?"":rsReportepdf.getString(13);
		IC_IF              = (rsReportepdf.getString(14)==null)?"":rsReportepdf.getString(14);
		IC_FOLIO           = (rsReportepdf.getString(15)==null)?"":rsReportepdf.getString(15);
		IC2_ESTATUS_DOCTO  = Integer.parseInt((rsReportepdf.getString(16)==null)?"0":rsReportepdf.getString(16));
		NO_MONEDA          = Integer.parseInt((rsReportepdf.getString(17)==null)?"0":rsReportepdf.getString(17));
		NO_INT_PYME        = (rsReportepdf.getString(18)==null)?"":rsReportepdf.getString(18);
		CT_REFERENCIA      = (rsReportepdf.getString(19)==null)?"":rsReportepdf.getString(19);
		CS_CAMBIO_IMPORTE  = (rsReportepdf.getString(20)==null)?"":rsReportepdf.getString(20).trim();
		//DETALLES         = rsReportepdf.getInt(21);
		//df_programacion  = (rsReportepdf.getString("df_programacion")==null)?"":rsReportepdf.getString("df_programacion").trim();
		DF_FECHA_VENC_PYME = (rsReportepdf.getString("DF_FECHA_VENC_PYME")==null)?"":rsReportepdf.getString("DF_FECHA_VENC_PYME");
	
		mandante	= (rsReportepdf.getString(38)==null)?"":rsReportepdf.getString(38); // Fodea 023
		
		tipoFactorajeDesc = (rsReportepdf.getString("TIPO_FACTORAJE")==null)?"":rsReportepdf.getString("TIPO_FACTORAJE").trim();
		fecha_registro_operacion = rsReportepdf.getString("fecha_registro_operacion")==null?"":rsReportepdf.getString("fecha_registro_operacion");//FODEA 005 - 2009 ACF
		fechaAutorizacionIF      = rsReportepdf.getString("fecha_autorizacion_if")   ==null?"":rsReportepdf.getString("fecha_autorizacion_if");   //FODEA 040 - 2011 By JSHD

		numeroSIAFF = getNumeroSIAFF(Integer.toString(IC_EPO),Integer.toString(IC_DOCUMENTO));
		beneficiario = rsReportepdf.getString("beneficiario")==null?"":rsReportepdf.getString("beneficiario");
		porciento_beneficiario = Double.parseDouble(rsReportepdf.getString("fn_porc_beneficiario")==null?"0":rsReportepdf.getString("fn_porc_beneficiario"));
		importe_a_recibir_beneficiario = Double.parseDouble(rsReportepdf.getString("RECIBIR_BENEF")==null?"0":rsReportepdf.getString("RECIBIR_BENEF"));
		if(bandera_neto_a_recibir_pyme)
			neto_a_recibir_pyme = Double.parseDouble(rsReportepdf.getString("NETO_REC_PYME")==null?"0":rsReportepdf.getString("NETO_REC_PYME"));
	
		if( banderaTablaDoctos.equals("1") ){
			fechaEntrega 		= (rsReportepdf.getString("DF_ENTREGA")==null)?"":rsReportepdf.getString("DF_ENTREGA");
			tipoCompra 			= (rsReportepdf.getString("CG_TIPO_COMPRA")==null)?"":rsReportepdf.getString("CG_TIPO_COMPRA");
			clavePresupuestaria = (rsReportepdf.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rsReportepdf.getString("CG_CLAVE_PRESUPUESTARIA");
			periodo 			= (rsReportepdf.getString("CG_PERIODO")==null)?"":rsReportepdf.getString("CG_PERIODO");
		}
		nombreTipoFactoraje = (rsReportepdf.getString("CS_DSCTO_ESPECIAL")==null)?"":rsReportepdf.getString("CS_DSCTO_ESPECIAL");
		// Fodea 002 - 2010
		if(!ocultarDoctosAplicados){
			esNotaDeCreditoSimpleAplicada						= ( nombreTipoFactoraje.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeDocumentoAsociado(icDocumento) )?true:false;
			esDocumentoConNotaDeCreditoSimpleAplicada		= ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(icDocumento))?true:false;
	
			esNotaDeCreditoAplicada 							= (nombreTipoFactoraje.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(icDocumento))?true:false;
			esDocumentoConNotaDeCreditoMultipleAplicada	= (!nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(icDocumento))?true:false;
	
			if(esNotaDeCreditoSimpleAplicada){
				numeroDocumento									= BeanSeleccionDocumento.getNumeroDoctoAsociado(icDocumento); 
			}else if(esDocumentoConNotaDeCreditoSimpleAplicada){
				numeroDocumento									= BeanSeleccionDocumento.getNumeroNotaCreditoAsociada(icDocumento);
			}
		}
		if(nRegistros==0){
			lEncabezados.add("A");
			lEncabezados.add("Nombre Proveedor");
			lEncabezados.add("No. Documento");
			lEncabezados.add("No. Acuse");
			lEncabezados.add("Fecha Emisión");
			lEncabezados.add("Fecha Vencimiento");
			if(!"".equals(sFechaVencPyme)) {
				lEncabezados.add("Fecha Vencimiento Proveedor");
			}
			lEncabezados.add("Moneda");
			if(!layOutAserca) {
				if(bTipoFactoraje){
					lEncabezados.add("Tipo Factoraje");
				}
				lEncabezados.add("Monto");
				lEncabezados.add("Estatus");
				lEncabezados.add("IF");
				lEncabezados.add("No. Solicitud");
				if (regCamposAdicionales != null){
					while (regCamposAdicionales.next()){
						lEncabezados.add(regCamposAdicionales.getString("NOMBRE_CAMPO").trim());
					}
				}
				lEncabezados.add("No. Proveedor");
			}
			pdfDoc.setTable(lEncabezados, "formasmenB", 100);
			totCol = lEncabezados.size();
			pdfDoc.setCell("B", "formasmenB", ComunesPDF.CENTER);
			nCol++;
			if(!layOutAserca) {
				pdfDoc.setCell("Referencia", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Clave Estatus", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell(" % Descuento", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar", "formasmenB", ComunesPDF.CENTER);
				nCol+=4;
				//lEncabezados.add("Referencia");
				//lEncabezados.add("Clave Estatus");
				//lEncabezados.add(" % Descuento");
				//lEncabezados.add("Monto a Descontar");
			}
			// Fodea 002 - 2010
			if(!ocultarDoctosAplicados){
				pdfDoc.setCell("Doctos Aplicados a Nota de Credito", "formasmenB", ComunesPDF.CENTER);
				nCol++;
				//lEncabezados.add("Doctos Aplicados a Nota de Credito");
			}
	
			if(!layOutAserca){
				if(bOperaFactorajeDistribuido || bOperaFactorajeVencInfonavit){
					if(bandera_neto_a_recibir_pyme){
						//lEncabezados.add("Neto Recibir Pyme");
						pdfDoc.setCell("Neto Recibir Pyme", "formasmenB", ComunesPDF.CENTER);
						nCol++;
					}
					pdfDoc.setCell("Beneficiario", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell(" % Beneficiario", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir BeneficiarioB", "formasmenB", ComunesPDF.CENTER);
					nCol+=3;
					//lEncabezados.add("Beneficiario");
					//lEncabezados.add(" % Beneficiario");
					//lEncabezados.add("Importe a Recibir Beneficiario");
				}
	/*//FODEA 005 - 2009 ACF
				if(ic_estatus_docto.equals("26")) {
					contenidoArchivo.append(","+mensaje_param.getMensaje("13consulta1.FechaDescto", sesIdiomaUsuario));
				}
	*/
			} else {
				pdfDoc.setCell("Monto", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell(" % Descuento", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus", "formasmenB", ComunesPDF.CENTER);
				nCol+=4;
				//lEncabezados.add("Monto");
				//lEncabezados.add(" % Descuento");
				//lEncabezados.add("Monto a Descontar");
				//lEncabezados.add("Estatus");
				if (regCamposAdicionales != null){
					while (regCamposAdicionales.next()){
						pdfDoc.setCell(regCamposAdicionales.getString("NOMBRE_CAMPO").trim(), "formasmenB", ComunesPDF.CENTER);
						nCol++;
						//lEncabezados.add(regCamposAdicionales.getString("NOMBRE_CAMPO").trim());
					}
				}
			}
			if(estaHabilitadoNumeroSIAFF){
				pdfDoc.setCell("Digito Identificador", "formasmenB", ComunesPDF.CENTER);
				nCol++;
				//lEncabezados.add("Digito Identificador");
			}
	
			if(banderaTablaDoctos.equals("1")) {
				pdfDoc.setCell("Fecha de Recepción de Bienes y Servicios", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Compra (procedimiento)", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Clasificador por Objeto del Gasto", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo Máximo", "formasmenB", ComunesPDF.CENTER);
				nCol+=4;
				//lEncabezados.add("Fecha de Recepción de Bienes y Servicios");
				//lEncabezados.add("Tipo de Compra (procedimiento)");
				//lEncabezados.add("Clasificador por Objeto del Gasto");
				//lEncabezados.add("Plazo Máximo");
			}
		  
			if(bOperaFactConMandato){
				pdfDoc.setCell("Mandante", "formasmenB", ComunesPDF.CENTER);
				nCol++;
				//lEncabezados.add("Mandante");
			}

			if(ic_estatus_docto.equals("26")){
				pdfDoc.setCell("Fecha Registro Operación", "formasmenB", ComunesPDF.CENTER);
				nCol++;
				//lEncabezados.add("Fecha Registro Operación");
			}//FODEA 005 - 2009 ACF

			// Fodea 040 - 2011 By JSHD
			if(bMostrarFechaAutorizacionIFInfoDoctos){
				pdfDoc.setCell("Fecha Autorización IF", "formasmenB", ComunesPDF.CENTER);
				nCol++;
				//lEncabezados.add("Fecha Autorización IF");
			}
			tam = totCol-nCol;
			if (tam > 0){
				pdfDoc.setCell("", "formasmenB", ComunesPDF.CENTER,tam);
			}
			//pdfDoc.setTable(lEncabezados, "formasmenB", 100);
		} //if registros == 0"
		if (IC2_ESTATUS_DOCTO == 2 || IC2_ESTATUS_DOCTO == 5 || IC2_ESTATUS_DOCTO == 6 || IC2_ESTATUS_DOCTO == 7 || IC2_ESTATUS_DOCTO == 9 || IC2_ESTATUS_DOCTO == 10) {
			if(NO_MONEDA == 1) {
				dblPorciento = new Double(strAforo).doubleValue() * 100;
			} else
				if(NO_MONEDA == 54) {
					dblPorciento = new Double(strAforoDL).doubleValue() * 100;
				}
		}
		else
			dblPorciento = FN_PORC_ANTICIPO;
	
		dblMontoDescuento = FN_MONTO * (dblPorciento / 100); //FN_MONTO_DSCTO;
		/* Para factoraje Vencido */
		CS_DSCTO_ESPECIAL = (CS_DSCTO_ESPECIAL==null)?"":CS_DSCTO_ESPECIAL.trim();
		/* si factoraje vencido y distribuido: se aplica 100% de anticipo */
		if(CS_DSCTO_ESPECIAL.equals("V") || CS_DSCTO_ESPECIAL.equals("D") || CS_DSCTO_ESPECIAL.equals("C") || CS_DSCTO_ESPECIAL.equals("I"))  {
			dblMontoDescuento=FN_MONTO;
			dblPorciento = 100;
		}
	/*FODEA 005 - 2009 ACF
		if(IC2_ESTATUS_DOCTO == 26){//26=Programado
			if(NO_MONEDA == 1) {
				dblPorciento = new Double(strAforo).doubleValue() * 100;
			}else if(NO_MONEDA == 54) {
				dblPorciento = new Double(strAforoDL).doubleValue() * 100;
			}
			dblMontoDescuento = FN_MONTO * (dblPorciento / 100);
		}
	*/
		CS_DSCTO_ESPECIAL = tipoFactorajeDesc ;

		 val++;
		 gen_archivo = true;
		if(IC_PYME!=null && !IC_PYME.equals("")) { IC_PYME=IC_PYME.replace(',',' '); }
		if(IC_IF == null){IC_IF="";}else{ IC_IF=IC_IF.replace(',',' '); } if(CT_REFERENCIA==null){CT_REFERENCIA="";}
		pdfDoc.setCell("A", "formasmenB", ComunesPDF.CENTER);
		pdfDoc.setCell(IC_PYME, "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(IG_NUMERO_DOCTO, "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(CC_ACUSE, "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(DF_FECHA_DOCTO, "formasmen", ComunesPDF.CENTER);
		pdfDoc.setCell(DF_FECHA_VENC, "formasmen", ComunesPDF.CENTER);
		
		if(!"".equals(sFechaVencPyme)){
			pdfDoc.setCell(DF_FECHA_VENC_PYME, "formasmen", ComunesPDF.CENTER);
		}
		pdfDoc.setCell(IC_MONEDA, "formasmen", ComunesPDF.CENTER);
		if(!layOutAserca) {
			if(bTipoFactoraje) { pdfDoc.setCell(CS_DSCTO_ESPECIAL, "formasmen", ComunesPDF.CENTER);}
			pdfDoc.setCell(Comunes.formatoDecimal(FN_MONTO,2,false), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(IC_ESTATUS_DOCTO, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(IC_IF, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(IC_FOLIO, "formasmen", ComunesPDF.CENTER);

			if (regCamposAdicionales != null){
				cont = 1;
				while (regCamposAdicionales.next()){
					if ( cont == 1 ) {
						CG_CAMPO1 = (rsReportepdf.getString(22)==null)?"":rsReportepdf.getString(22).trim();
						pdfDoc.setCell(CG_CAMPO1,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 2) {
						CG_CAMPO2 = (rsReportepdf.getString(23)==null)?"":rsReportepdf.getString(23).trim();
						pdfDoc.setCell(CG_CAMPO2,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 3) {
						CG_CAMPO3 = (rsReportepdf.getString(24)==null)?"":rsReportepdf.getString(24).trim();
						pdfDoc.setCell(CG_CAMPO3,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 4) {
						CG_CAMPO4 = (rsReportepdf.getString(25)==null)?"":rsReportepdf.getString(25).trim();
						pdfDoc.setCell(CG_CAMPO4,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 5) {
						CG_CAMPO5 = (rsReportepdf.getString(26)==null)?"":rsReportepdf.getString(26).trim();
						pdfDoc.setCell(CG_CAMPO5,"formasmen",ComunesPDF.CENTER);
					}
					cont ++;
				}
			}
			pdfDoc.setCell(NO_INT_PYME, "formasmen", ComunesPDF.CENTER);
		}
		pdfDoc.setCell("B", "formasmenB", ComunesPDF.CENTER);
		if(!layOutAserca) {
			pdfDoc.setCell(CT_REFERENCIA, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Integer.toString(IC2_ESTATUS_DOCTO), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorciento),0,false), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(String.valueOf(dblMontoDescuento), "formasmen", ComunesPDF.CENTER);
		}
		if(!ocultarDoctosAplicados){
			if(esNotaDeCreditoSimpleAplicada || esDocumentoConNotaDeCreditoSimpleAplicada ){
				// Notas de Credito Simple   
				pdfDoc.setCell(numeroDocumento, "formasmen", ComunesPDF.CENTER);
			}else if(esNotaDeCreditoAplicada || esDocumentoConNotaDeCreditoMultipleAplicada){
				// Notas de Credito Multiple 
				pdfDoc.setCell("Si", "formasmen", ComunesPDF.CENTER);
			}else{
				pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER);
			}
		}
		if(!layOutAserca){
			if(bOperaFactorajeDistribuido || bOperaFactorajeVencInfonavit){
				if(bandera_neto_a_recibir_pyme && 0 != neto_a_recibir_pyme){
					pdfDoc.setCell(Comunes.formatoDecimal(neto_a_recibir_pyme,2,false), "formasmen", ComunesPDF.CENTER);
				}
				//else
					//contenidoArchivo += ", ";
					 pdfDoc.setCell(beneficiario, "formasmen", ComunesPDF.CENTER);
				if(0 != porciento_beneficiario)
					pdfDoc.setCell(String.valueOf(porciento_beneficiario), "formasmen", ComunesPDF.CENTER);
				else
					pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER);
				if(0 != importe_a_recibir_beneficiario)
					pdfDoc.setCell(Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false), "formasmen", ComunesPDF.CENTER);
				else
					pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER);
			}
		}else {
			pdfDoc.setCell(Comunes.formatoDecimal(FN_MONTO,2,false), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorciento),0,false), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(FN_MONTO,2,false), "formasmen", ComunesPDF.CENTER);
			if (regCamposAdicionales != null){
				cont = 1;
				while (regCamposAdicionales.next()){
					if ( cont == 1 ) {
						CG_CAMPO1 = (rsReportepdf.getString(22)==null)?"":rsReportepdf.getString(22).trim();
						pdfDoc.setCell(CG_CAMPO1,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 2) {
						CG_CAMPO2 = (rsReportepdf.getString(23)==null)?"":rsReportepdf.getString(23).trim();
						pdfDoc.setCell(CG_CAMPO2,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 3) {
						CG_CAMPO3 = (rsReportepdf.getString(24)==null)?"":rsReportepdf.getString(24).trim();
						pdfDoc.setCell(CG_CAMPO3,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 4) {
						CG_CAMPO4 = (rsReportepdf.getString(25)==null)?"":rsReportepdf.getString(25).trim();
						pdfDoc.setCell(CG_CAMPO4,"formasmen",ComunesPDF.CENTER);
					}
					if (cont == 5) {
						CG_CAMPO5 = (rsReportepdf.getString(26)==null)?"":rsReportepdf.getString(26).trim();
						pdfDoc.setCell(CG_CAMPO5,"formasmen",ComunesPDF.CENTER);
					}
					cont ++;
				}
			}
			//pdfDoc.setCell(IC_ESTATUS_DOCTO+linea_cd, "formasmen", ComunesPDF.CENTER);
		}

		if(estaHabilitadoNumeroSIAFF){
			pdfDoc.setCell(numeroSIAFF, "formasmen", ComunesPDF.CENTER);
		}
	
		if(banderaTablaDoctos.equals("1")) {
			pdfDoc.setCell(fechaEntrega, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(tipoCompra, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(clavePresupuestaria, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(periodo, "formasmen", ComunesPDF.CENTER);
		}
		if(bOperaFactConMandato){
			pdfDoc.setCell(mandante, "formasmen", ComunesPDF.CENTER);
		}
		
		if(ic_estatus_docto.equals("26")){pdfDoc.setCell(fecha_registro_operacion, "formasmen", ComunesPDF.CENTER);}//FODEA 005 - 2009 ACF
		
		//Fodea 040 - 2011 By JSHD
		if(bMostrarFechaAutorizacionIFInfoDoctos){
			pdfDoc.setCell(fechaAutorizacionIF, "formasmen", ComunesPDF.CENTER);
		}
		if (tam > 0){
			pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER,tam);
		}
		nRegistros++;
		contadorLineas++;
	}
}
	BigDecimal saldoMN = new BigDecimal(0);
	BigDecimal saldoUSD = new BigDecimal(0);
	BigDecimal totalDocsNegMN = new BigDecimal(0);
	BigDecimal totalDocsNegUSD = new BigDecimal(0);
	BigDecimal totalNCNegMN = new BigDecimal(0);
	BigDecimal totalNCNegUSD = new BigDecimal(0);

	//Vector vecTotales = queryHelper.getResultCount(request);
	List vecTotales = queryHelper.getResultCount(request);
	if (vecTotales != null) {
		if(vecTotales.size()>0){
			int i = 0;
			int col=0;
			List vecColumnas = new ArrayList();
			for(i=0;i<vecTotales.size();i++){
				vecColumnas = (List)vecTotales.get(i);
				//vecColumnas = (Vector)vecTotales.get(i);
				String tipoDoc = vecColumnas.get(4).toString();
				String descTipoDoc = " Documentos ";
				if(bOperaFactorajeNotaDeCredito) {
					String moneda = tipoDoc.substring(2,tipoDoc.length());
					BigDecimal monto = new BigDecimal(vecColumnas.get(1).toString());
					if(tipoDoc.length()>2)
						tipoDoc = vecColumnas.get(4).toString().substring(0,2);
					if("NC".equals(tipoDoc)) {
						descTipoDoc = " Notas de Credito ";
						if("1".equals(moneda))
							totalNCNegMN=monto;
						else if("54".equals(moneda))
							totalNCNegUSD=monto;
					} else if("DN".equals(tipoDoc)) {
						descTipoDoc = " Documentos Negociables ";
						if("1".equals(moneda))
							totalDocsNegMN=monto;
						else if("54".equals(moneda))
							totalDocsNegUSD=monto;
					} else {
						if("1".equals(moneda))
							totalDocsNegMN=monto;
						else if("54".equals(moneda))
							totalDocsNegUSD=monto;
					}
				}
	
				if(!"".equals(sFechaVencPyme)) {
					col=8;
				}else{
					col=7;
				}
				if(bOperaFactorajeVencido||bOperaFactorajeDistribuido||bOperaFactorajeNotaDeCredito || bOperaFactorajeVencInfonavit) { col=col+1;}
	
				pdfDoc.setCell("Monto Total" + descTipoDoc + vecColumnas.get(3).toString(), "formasmen", ComunesPDF.RIGHT, col);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecColumnas.get(1).toString(),2), "formasmen", ComunesPDF.LEFT,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecColumnas.get(2).toString(),2), "formasmen", ComunesPDF.LEFT,	totCol - (col+2)	);
				
				pdfDoc.setCell("Total" + descTipoDoc + vecColumnas.get(3).toString(), "formasmen", ComunesPDF.RIGHT, col);
				pdfDoc.setCell(vecColumnas.get(0).toString(), "formasmen", ComunesPDF.LEFT, totCol - col);
			}
			if(bOperaFactorajeNotaDeCredito && muestraNegociables) {
				saldoMN = saldoMN.add(totalDocsNegMN);
				saldoUSD = saldoUSD.add(totalDocsNegUSD);
				saldoMN = saldoMN.subtract(totalNCNegMN);
				saldoUSD = saldoUSD.subtract(totalNCNegUSD);
				if(totalDocsNegMN.floatValue()>0) {
					pdfDoc.setCell("Total Saldo Moneda Nacional Documentos Negociables", "formasmen", ComunesPDF.RIGHT,col);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(saldoMN.toPlainString(),2), "formasmen", ComunesPDF.LEFT,2);
					pdfDoc.setCell("", "formasmen", ComunesPDF.LEFT, totCol - (col+2) );
				}
				if(totalDocsNegUSD.floatValue()>0) {
					pdfDoc.setCell("Total Saldo Dolares Documentos Negociables", "formasmen", ComunesPDF.RIGHT, col);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(saldoUSD.toPlainString(),2), "formasmen", ComunesPDF.LEFT,2);
					pdfDoc.setCell("", "formasmen", ComunesPDF.LEFT, totCol - (col+2) );
				}
			}
		}
	}else{
		pdfDoc.setCell("No se encontro registros", "formasmen", ComunesPDF.CENTER);
	}
	pdfDoc.addTable();
	pdfDoc.endDocument();

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {

}

%>
<%=jsonObj%>

<%!
	public String getNumeroSIAFF(String ic_epo,String ic_documento){
		return getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(ic_documento);
	}

	public String getNumeroDeCuatroDigitos(String numero){

		String 			claveEPO 	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();

      for(int i = 0;i<(4-claveEPO.length());i++){
			resultado.append("0");
      }
      resultado.append(claveEPO);

      return resultado.toString();
	}

  	public String getNumeroDeOnceDigitos(String numero){

		String 			claveDocto	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();

      for(int i = 0;i<(11-claveDocto.length());i++){
			resultado.append("0");
      }
      resultado.append(claveDocto);

      return resultado.toString();
	}
%>