Ext.onReady(function() 
{
//--------------------------------- HANDLERS -------------------------------
  var jsonValoresIniciales = null;
	function procesaValoresIniciales(opts,success,response){
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
		}else{
			NE.util.mostrarConnError(response,opts);
		}
	}
  
  var procesarConsultaData = function(store, arrRegistros, opts) {
		var gridGeneral = Ext.getCmp('gridGeneral');		
		gridGeneral.el.unmask();
		gridGeneral.setTitle(jsonValoresIniciales.strNombre);
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) {
				gridGeneral.show();
			}

		var el = gridGeneral.getGridEl();			
		if(store.getTotalCount() > 0) {
  			el.unmask();
		} else {
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
	   }
  }
  	
  var procesarConsultaDetalleData = function(store, arrRegistros, opts) {
		var gridDetalle = Ext.getCmp('gridDetalle');
		gridDetalle.el.unmask();
		gridDetalle.setTitle(jsonValoresIniciales.strNombre);
		
		if (arrRegistros != null) {
			if (!gridDetalle.isVisible()) { gridDetalle.show(); }
		
		var el = gridDetalle.getGridEl();		
		var store = consultaDataDetalle;
		if(store.getTotalCount() > 0) {					
  			el.unmask();
		} else {
		  el.mask('No existe alguna operaci�n por el momento Aceptada, En Proceso o Rechazada.', 'x-mask'); 
		  }
	   }
  }
  
  var procesarDetalle = function(grid, rowIndex, colIndex, item, event)
  {  
	  var registro = grid.getStore().getAt(rowIndex);
	  dc_fecha_tasa = registro.get('DC_FECHA_TASA');
	  
	  consultaDataDetalle.load({
				          params:{ dc_fecha_tasa : dc_fecha_tasa }
							        });
	var ventana = Ext.getCmp('ventanaDetalle');
	if (ventana) {
		ventana.destroy();
	}
  new Ext.Window({
					  layout: 'fit',
					  modal: true,
					  width: 933,
					  height: 400,
					  id: 'ventanaDetalle',
					  closeAction: 'hide',
					  items: [ 
					          gridDetalle
								],
					  title: ''
				    }).show().setTitle('TASA BASE');
  }
  
//-------------------------------- STORES -----------------------------------
  var consultaData = new Ext.data.GroupingStore
  ({
			root: 'registros',
			url: '13consulta2ext.data.jsp',
			baseParams: {
				informacionG: 'Consulta'
			},
			reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
			         {name: 'NOMBREIF'},
						{name: 'NOMBREMONEDA'},
						{name: 'NOMBRETASA'},
						{name: 'RANGOPLAZO'},
						{name: 'VALOR'},
						{name: 'RELMAT'},
						{name: 'PTOS'},
						{name: 'TASAAPLICAR'}
			        ]
			}),
			groupField:'NOMBREIF',
			sortInfo:{field: 'NOMBREIF',direction:"ASC"},
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
			      load: procesarConsultaData,
					exception: {
								   fn: function(proxy,type,action,optionRequest,response,args)	{
					             NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
									 procesarConsultaData(null,null,null);
					            }
					           }
			           }
  });
  
  var consultaDataH = new Ext.data.JsonStore
  ({
			root: 'registros',
			url: '13consulta2ext.data.jsp',
			baseParams: {
				informacionH: 'ConsultaHistorico'
			},
			fields: [
			         {name: 'DC_FECHA_TASA'}
			],
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: true,
			listeners: {
			     exception: {
								   fn: function(proxy,type,action,optionRequest,response,args)	{
					             NE.util.mostrarDataProxyError(proxy,type,action,optionRequest,response,args);
					            }
					           }
			           }
  });
  
  var consultaDataDetalle = new Ext.data.GroupingStore
  ({
			root: 'registros',
			url: '13consulta2ext.data.jsp',
			baseParams: {
				informacionH: 'ConsultaHistoricoGeneral'
			},
			reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
			         {name: 'NOMBREIF'},
						{name: 'NOMBREMONEDA'},
						{name: 'NOMBRETASA'},
						{name: 'RANGOPLAZO'},
						{name: 'VALOR'},
						{name: 'RELMAT'},
						{name: 'PTOS'},
						{name: 'TASAAPLICAR'}
			        ]
			}),
			groupField:'NOMBREIF',
			sortInfo:{field: 'NOMBREIF',direction:"ASC"},
			totalProperty: 'total',
			messageProperty: 'msg',
			autoLoad: false,
			listeners: {
			      load: procesarConsultaDetalleData,
					exception: {
								fn: function(proxy,type,action,optionsRequest,response,args)
								     { 
									   NE.util.mostrarDataProxyError(proxy,type,action,optionsRequest,response,args); 
										procesarConsultaData(null,null,null); //Llama procesar consulta, para que desbloquee los componentes.
									  }
								  }
					     }    
  });

  var gridGeneral = new Ext.grid.GridPanel
  ({
		store: consultaData,
		style: 'margin: 0 auto;',
		hidden: false,
      id: 'gridGeneral',
		columns: [		
			{
				header: 'Intermediario Financiero',
				sortable: true,
				resizable: true,
				width: 500,	
				align: 'left',
				hidden: true,
				dataIndex: 'NOMBREIF'
			},
			{
				header: 'Moneda',
				sortable: true,
				resizable: true,
				width: 400,	
				align: 'left',
				hidden: false,
				dataIndex: 'NOMBREMONEDA'
			},
			{
				header: 'Tasa Base',
				sortable: true,
				resizable: true,
				width: 400,	
				align: 'left',
				hidden: false,
				dataIndex: 'NOMBRETASA'
			},
			{
				header: 'Plazo',
				sortable: true,
				resizable: true,
			   width: 100,	
				align: 'left',
				hidden: false,
				dataIndex: 'RANGOPLAZO'
			},
			{
				header: 'Valor',
				sortable: true,
				resizable: true,
				width: 100,	
				align: 'center',
				hidden: false,
				dataIndex: 'VALOR'
			},
			{
				header: 'Relaci�n Matem�tica',
				sortable: true,
				resizable: true,
				width: 300,	
				align: 'center',
				hidden: false,
				dataIndex: 'RELMAT'
			},
			{
				header: 'Puntos Adicionales',
				sortable: true,
				resizable: true,
				width: 250,	
				align: 'center',
				hidden: false,
				dataIndex: 'PTOS'
			},
			{
				header: 'Tasa a Aplicar',
				sortable: true,
				resizable: true,
				width: 250,	
				align: 'left',
				hidden: false,
				dataIndex: 'TASAAPLICAR',
				renderer: Ext.util.Format.numberRenderer('0,0.000000 %')
			}
		],	
		view: new Ext.grid.GroupingView({ forceFit: true, groupTextTpl: '{text}'}),
		stripeRows: true,
	   loadMask: true,
		frame: true,
		autoWidth: true,
		height: 400,
		title: 'T�tulo',
		bbar: 
	      {
			}
	});
	
	var gridGeneralHistorico = new Ext.grid.GridPanel
  ({
		store: consultaDataH,
		hidden: false,
      id: 'gridGeneralHistorico',
		style: 'margin: 0 auto;',
		columns: [		
			{
				header: 'Fecha',
				sortable: true,
				resizable: true,
				width: 100,	
				align: 'center',
				hidden: false,
				dataIndex: 'DC_FECHA_TASA'
			},
			{
				xtype: 'actioncolumn',
				header: 'Ver Tasa',
				tooltip: 'Ver Tasa',
				dataIndex: '',
            width: 130,
				align: 'center',
				items: [
					     {
						   getClass: function(valor, metadata, registro, rowIndex, colIndex, store) {								
							 this.items[0].tooltip = 'Ver';								
							 return 'iconoLupa';											
						   },
						  handler: procesarDetalle
					     }
				       ]	
			}
		],	
		stripeRows: true,
	   loadMask: true,
		frame: true,
		width: 255,
		autoHeight: true,
		title: 'Hist�rico de Tasas',
		bbar: 
	      {
			}
  });
	
  var gridDetalle = //new Ext.grid.GridPanel
  {
		xtype: 'grid',
		store: consultaDataDetalle,
		id: 'gridDetalle',
		hidden: true,
		//stateful: true,
		columns: [		
			{
				header: 'Intermediario Financiero',
				sortable: true,
				resizable: true,
				width: 500,	
				align: 'left',
				hidden: true,
				dataIndex: 'NOMBREIF'
			},
			{
				header: 'Moneda',
				sortable: true,
				resizable: true,
				width: 400,	
				align: 'left',
				hidden: false,
				dataIndex: 'NOMBREMONEDA'
			},
			{
				header: 'Tasa Base',
				sortable: true,
				resizable: true,
				width: 400,	
				align: 'left',
				hidden: false,
				dataIndex: 'NOMBRETASA'
			},
			{
				header: 'Plazo',
				sortable: true,
				resizable: true,
			   width: 100,	
				align: 'left',
				hidden: false,
				dataIndex: 'RANGOPLAZO'
			},
			{
				header: 'Valor',
				sortable: true,
				resizable: true,
				width: 100,	
				align: 'center',
				hidden: false,
				dataIndex: 'VALOR'
			},
			{
				header: 'Relaci�n Matem�tica',
				sortable: true,
				resizable: true,
				width: 300,	
				align: 'center',
				hidden: false,
				dataIndex: 'RELMAT'
			},
			{
				header: 'Puntos Adicionales',
				sortable: true,
				resizable: true,
				width: 250,	
				align: 'center',
				hidden: false,
				dataIndex: 'PTOS'
			},
			{
				header: 'Tasa a Aplicar',
				sortable: true,
				resizable: true,
				width: 250,	
				align: 'left',
				hidden: false,
				dataIndex: 'TASAAPLICAR',
				renderer: Ext.util.Format.numberRenderer('0,0.000000 %')
			}
		],	
		view: new Ext.grid.GroupingView({ forceFit: true, groupTextTpl: '{text}'}),
		stripeRows: true,
	   loadMask: true,
		frame: true,
		width: 943,
		height: 400,
		title: 'Titulo',
		bbar: 
	      {
			}
	};
//---------------------------------COMPONENTES-------------------------------

//-------------------------------- PRINCIPAL -----------------------------------
  var pnl = new Ext.Container
  ({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 949,
	  height: 'auto',
	  items: 
	    [
		  gridGeneral,
		  NE.util.getEspaciador(20),
		  gridGeneralHistorico
	    ]
  });
  
  Ext.Ajax.request({
		url: '13consulta2ext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		},
		callback:procesaValoresIniciales
	});
  
  consultaData.load();
});