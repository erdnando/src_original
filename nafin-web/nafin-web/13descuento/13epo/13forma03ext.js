Ext.onReady(function() {
	
	var ObjGral = {
		strLogin : '',
		strNombreUsuario : '',
		hidFechaActual: '',
		bOperaFactorajeVencido: false,
		bOperaFactConMandato: false,
		bOperaFactorajeVencido: false,
		bOperaFactorajeVencidoInfonavit: false,
		tieneParamEpoPef: false,
		firmaMancomunada: '',
		sParamFechaVencPyme: '',
		ic_pyme: '',
		txtNumProveedor: '',
		txtCadenasPymes: '',
		blBusquedaDatosPYME: false
	};


//HANDLERS----------------------------------------------------------------------
	var procesarSuccessDatosIniciales = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			
			ObjGral.hidFechaActual = resp.hidFechaActual;
			ObjGral.bOperaFactorajeVencido = resp.bOperaFactorajeVencido;
			ObjGral.bOperaFactConMandato = resp.bOperaFactConMandato;
			ObjGral.bOperaFactorajeVencido = resp.bOperaFactorajeVencido;
			ObjGral.bOperaFactorajeVencidoInfonavit = resp.bOperaFactorajeVencidoInfonavit;
			ObjGral.tieneParamEpoPef = resp.tieneParamEpoPef;
			ObjGral.firmaMancomunada = resp.firmaMancomunada;
			ObjGral.sParamFechaVencPyme = resp.sParamFechaVencPyme;
			ObjGral.strNombreUsuario = resp.strNombreUsuario;
			ObjGral.strLogin = resp.strLogin;
			
			
			storeMonedaData.loadData(resp);
			storeEstatusData.loadData(resp);

			pnl.el.unmask();
			
		}else{
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}


	var procesarSuccessFailureBtnConsultar	 = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			stroreMantoDoctos.loadData(resp);
			storeTotales.loadData(resp);
			storeEstatusAsignar.loadData(resp);

			pnl.el.unmask();
			
		}else{
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureConsCveProv	 = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var numProv = Ext.getCmp('txtNumProveedor1').getValue();
			
			ObjGral.ic_pyme = resp.ic_pyme;
			ObjGral.blBusquedaDatosPYME = resp.blBusquedaDatosPYME;
			Ext.getCmp('txtNumProveedor1').setValue(resp.txtNumProveedor);
			Ext.getCmp('txtCadenasPymes1').setValue(resp.txtCadenasPymes);
			
			
			if(ObjGral.blBusquedaDatosPYME){
				Ext.MessageBox.alert('Aviso', 'La PYME con Numero de Proveedor: '+numProv+' no existe o no esta relacionada con la EPO.');
			}

			pnl.el.unmask();
			
		}else{
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureCambioEstatus	 = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var pnlMantoDoctos1 = Ext.getCmp('pnlMantoDoctos1');
			var gridColumnMod = gridMantoDoctosAcuse.getColumnModel();

			formMantoDocto.hide();
			gridMantoDoctos.hide();
			gridMantoDoctosTotales.hide();
			pnlCommentEstatus.hide();
			
			if(ObjGral.sParamFechaVencPyme == 'S'){
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('DF_FECHA_VENC_PYME'),false);//DF_FECHA_VENC_PYME
			}
			
			if(ObjGral.bOperaFactorajeVencido || ObjGral.bOperaFactConMandato || ObjGral.bOperaFactorajeVencidoInfonavit ) { 
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('NOMBREIF'),false);//12
			}
			
			if(ObjGral.tieneParamEpoPef){
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('FECRECEP'),false);//13
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('TIPOCOMPRA'),false);// 14
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('CLASIFICADOR'),false);// 15
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('PLAZOMAX'),false);// 16
			}
			
			if(ObjGral.bOperaFactConMandato || ObjGral.firmaMancomunada=='S' ){
				gridColumnMod.setHidden(gridColumnMod.findColumnIndex('MANDANTE'),false);// 17
			}
			
			
			pnlMantoDoctos1.add(gridMantoDoctosAcuse);
			pnlMantoDoctos1.add(gridMantoDoctosTotalesAcuse);
			pnlMantoDoctos1.doLayout();

			pnl.el.unmask();
			
		}else{
			pnl.el.unmask();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarBusqAvanzada = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {

			if(store.getTotalCount() > 0) {
				Ext.getCmp('cboPyme1').enable();
				//Ext.getCmp('cboPyme1').emptyText = 'Seleccione...';
				fpBusqAvanzada.el.unmask();
			}else {
				//Ext.getCmp('cboPyme1').emptyText = 'No hay registros';
				fpBusqAvanzada.el.unmask();
			}
		}
	}
	
	//CALLBACKS STORES
	
	var procesarConsMantoDoctos  = function(store, arrRegistros, opts) {
		var pnlMantoDoctos1 = Ext.getCmp('pnlMantoDoctos1');
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var gridColumnMod = gridMantoDoctos.getColumnModel();
		//var fp = Ext.getCmp('forma');
		//fp.el.unmask();
		if (arrRegistros != null) {
			if (!gridMantoDoctos.isVisible()) {
				pnlMantoDoctos1.add(gridMantoDoctos);
				pnlMantoDoctos1.add(gridMantoDoctosTotales);
				pnlMantoDoctos1.add(NE.util.getEspaciador(20));
				pnlMantoDoctos1.add(pnlCommentEstatus);
				pnlMantoDoctos1.doLayout();
			}
			
			var el = gridMantoDoctos.getGridEl();
			if(store.getTotalCount() > 0) {
				
				if(ObjGral.sParamFechaVencPyme == 'S'){
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('DF_FECHA_VENC_PYME'),false);//DF_FECHA_VENC_PYME
				}
				
				if(ObjGral.bOperaFactorajeVencido || ObjGral.bOperaFactConMandato || ObjGral.bOperaFactorajeVencidoInfonavit ) { 
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('NOMBREIF'),false);//12
				}
				
				if(ObjGral.tieneParamEpoPef){
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('FECRECEP'),false);//13
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('TIPOCOMPRA'),false);// 14
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('CLASIFICADOR'),false);// 15
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('PLAZOMAX'),false);// 16
				}
				
				if(ObjGral.bOperaFactConMandato || ObjGral.firmaMancomunada=='S' ){
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('MANDANTE'),false);// 17
				}
				
				pnlCommentEstatus.show();
				gridMantoDoctosTotales.show();
				el.unmask();
			}else {
				pnlCommentEstatus.hide();
				gridMantoDoctosTotales.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	
	var procesarConsMantoDoctosAcuse  = function(store, arrRegistros, opts) {
		var pnlMantoDoctos1 = Ext.getCmp('pnlMantoDoctos1');
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var gridColumnMod = gridMantoDoctos.getColumnModel();

		if (arrRegistros != null) {
			if (!gridMantoDoctos.isVisible()) {
				pnlMantoDoctos1.add(gridMantoDoctos);
				pnlMantoDoctos1.add(NE.util.getEspaciador(20));
				pnlMantoDoctos1.add(pnlCommentEstatus);
				pnlMantoDoctos1.doLayout();
			}
			
			var el = gridMantoDoctos.getGridEl();
			if(store.getTotalCount() > 0) {
				
				if(ObjGral.sParamFechaVencPyme == 'S'){
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('DF_FECHA_VENC_PYME'),false);//DF_FECHA_VENC_PYME
				}
				
				if(ObjGral.bOperaFactorajeVencido || ObjGral.bOperaFactConMandato || ObjGral.bOperaFactorajeVencidoInfonavit ) { 
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('NOMBREIF'),false);//12
				}
				
				if(ObjGral.tieneParamEpoPef){
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('FECRECEP'),false);//13
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('TIPOCOMPRA'),false);// 14
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('CLASIFICADOR'),false);// 15
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('PLAZOMAX'),false);// 16
				}
				
				if(ObjGral.bOperaFactConMandato || ObjGral.firmaMancomunada=='S' ){
					gridColumnMod.setHidden(gridColumnMod.findColumnIndex('MANDANTE'),false);// 17
				}
				
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	
	//HANDLER BUTTONS
	var fnBtnCambiarEstatus = function(btn){
		var valido = true;
		var registrosAcuse = [];
		var registrosEnviar = [];
		var noRegistros = false;
	
		var montoMnAcuse = 0.0;
		var montoUsdAcuse = 0.0;
		var totalDoctoMn = 0;
		var totalDoctoUsd = 0;
		
		if(Ext.isEmpty(Ext.getCmp('txtCausa1').getValue())){
			Ext.getCmp('txtCausa1').markInvalid('Debe capturar una causa para el cambio de estatus');
			valido = false;
		}
		
		if(Ext.isEmpty(Ext.getCmp('ic_estatus_asignar1').getValue())){
			Ext.getCmp('ic_estatus_asignar1').markInvalid('Es necesario elegir el estatus que desea aplicar');
			valido = false;
		}
		
		if(valido){
			stroreMantoDoctos.each(function(record) {
				if(record.data['SELECCIONESTATUS']){
					registrosEnviar.push(record.data);
					registrosAcuse.push(record);
					if(record.data['IC_MONEDA']=='1'){
						montoMnAcuse += parseFloat(record.data['FN_MONTO']);
						totalDoctoMn += 1;
					}else{
						montoUsdAcuse += parseFloat(record.data['FN_MONTO']);
						totalDoctoUsd += 1;
					}
					
					noRegistros = true;
				}
			});
			
			
			var totalesAcuse = [
				['MONEDA NACIONAL', totalDoctoMn, montoMnAcuse],
				['DOLAR AMERICANO', totalDoctoUsd, montoUsdAcuse]
			];
		
			
			storeTotalesAcuse.loadData(totalesAcuse);
			stroreMantoDoctosAcuse.add(registrosAcuse);
			
			if(noRegistros){
				Ext.MessageBox.confirm('Cambio de Estatus','Este proceso cambiar� el estatus de los documentos seleccionados. �Desea continuar?',function(msb){
						if(msb=='yes'){
							registrosEnviar = Ext.encode(registrosEnviar);
							gridMantoDoctos.stopEditing();
							gridMantoDoctos.el.mask('Enviando...', 'x-mask-loading');
							
							Ext.Ajax.request({
								url: '13forma03ext.data.jsp',
								params: Ext.apply(formMantoDocto.getForm().getValues(),{
									informacion: 'CambioEstatus',
									registros : registrosEnviar,
									txtCausa : Ext.getCmp('txtCausa1').getValue(),
									ic_estatus_asignar1 : Ext.getCmp('ic_estatus_asignar1').getValue()
								}),
								callback: procesarSuccessFailureCambioEstatus
							});
							
						}//fin if(msb=='NO')
					});//fin Confirm
			}else{
				Ext.MessageBox.alert('Aviso', 'Debe seleccionar documentos a los cuales se les cambiar� el estatus');
			}
		}
		
	}
	
	//FUNCIONES DE VALIDACIONES EXTRAS
	var validaMontos = function(ObjMontoMin, ObjMontoMax){
		var valido = true;
		ObjMontoMin.clearInvalid();
		ObjMontoMax.clearInvalid();
		
		if(ObjMontoMin.getValue()!=''){
			if(ObjMontoMax.getValue()!='')
			{
				if (parseFloat(ObjMontoMin.getValue()) > parseFloat(ObjMontoMax.getValue()))
				{
					ObjMontoMin.markInvalid('El monto final debe ser mayor o igual al inicial');
					valido = false;
				}
			}
			else
			{
				ObjMontoMax.markInvalid('Debe capturar el monto final');
				valido = false;
			}
		}
		
		return valido;
	}
	
	var validaFechas = function(ObjFecDocto, ObjFecVenc){
		var valido = true;
		ObjFecDocto.clearInvalid();
		ObjFecVenc.clearInvalid();
		
		if(ObjFecDocto.getValue()!=''){
			var resultado = datecomp(ObjGral.hidFechaActual, ObjFecDocto.value);
			if (resultado==2)
			{
				ObjFecDocto.markInvalid('La fecha de emisi�n no puede ser posterior a la actual');
				valido = false;
			}
		
		}
		
		if (ObjFecVenc.getValue()!=''  && ObjFecDocto.getValue()!='' ){
			if (!esVacio(ObjFecDocto.value))
			{
				var resultado = datecomp(ObjFecDocto.value, ObjFecVenc.value);
				if (resultado!=2)
				{
					ObjFecVenc.markInvalid('La fecha de vencimiento debe ser mayor a la fecha de emisi�n');
					valido = false;
				}
			}
		}
		
		return valido;
	}
	

	/***/

	var verDetalle = function(grid, rowIndex, colIndex, item, event) {  
		var registro = grid.getStore().getAt(rowIndex);
		
		var ic_pyme = registro.get('CLAVE_PYME');  
		var ic_moneda = registro.get('IC_MONEDA');  
		var montoDocto = registro.get('FN_MONTO');  
		var df_fecha_docto =	Ext.util.Format.date(registro.get('DF_FECHA_DOCTO'),'d/m/Y'); 	
		
		consDetalleDoctosData.load({
			params: {
				informacion: 'consDetalleDoctos',	
				tipoConsulta: 'Detalle',
				ic_pyme:ic_pyme,
				ic_moneda:ic_moneda,
				montoDocto:montoDocto,
				df_fecha_docto:df_fecha_docto												
			}			
		});
		
		var verDetalle = Ext.getCmp('verDetalle');
		if(verDetalle){
			verDetalle.show();
		}else{
			new Ext.Window({
				layout: 'fit',
				modal: true,
				width: 700,
				height: 400,												
				resizable: false,
				closable:false,
				id: 'verDetalle',
				closeAction: 'hide',
				items: [					
					gridDetalleDoctos				
				],
				bbar: {
					xtype: 'toolbar',	buttonAlign:'center',	
					buttons: [
						{	xtype: 'button',	text: 'Cerar',	iconCls: 'icoLimpiar', 	id: 'btnCerraP', handler: function(){Ext.getCmp('verDetalle').hide();} }
													
					]
				}
			}).show().setTitle('Detalle Doctos');
		}		
	}
	
	
	var procesarConsDetalleDoctosDataData = function(store,arrRegistros,opts){
	
		var fp = Ext.getCmp('formMantoDocto1');
		fp.el.unmask();
		if(arrRegistros != null){			
			var gridDetalleDoctos = Ext.getCmp('gridDetalleDoctos');
			if (!gridDetalleDoctos.isVisible()) {
				gridDetalleDoctos.show();
			}	
			var el = gridDetalleDoctos.getGridEl();			
			var info = store.reader.jsonData;				
		
			
			if(store.getTotalCount()>0){	
			
			}else{				
				el.mask('No se encontr� ning�n registro','x-mask');
			}
		}
	}
	
	var consDetalleDoctosData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13forma03ext.data.jsp',
		baseParams: {
			informacion: 'consDetalleDoctos'	,
			tipoConsulta: 'Detalle'
		},		
		fields: [				
			{ name: 'NOMBRE_EPO'},
			{ name: 'NOMBRE_PYME'},
			{ name: 'NUM_DOCUMENTO'},
			{ name: 'ESTATUS'},
			{ name: 'FECHA_EMISION' },
			{ name: 'MONEDA'},
			{ name: 'MONTO'}
		],		
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsDetalleDoctosDataData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsDetalleDoctosDataData(null, null, null);					
				}
			}
		}					
	});
	
	
	var gridDetalleDoctos = {		
		xtype: 'grid',
		store: consDetalleDoctosData,
		id: 'gridDetalleDoctos',		
		hidden: false,
		title: '',	
		columns: [	
			{
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'			
			},
			{
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'NOMBRE_PYME',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'left'			
			},
			{
				header: 'No. Documento',
				tooltip: 'No. Documento',
				dataIndex: 'NUM_DOCUMENTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'Estatus',
				tooltip: 'Estatus',
				dataIndex: 'ESTATUS',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'Fecha Emisi�n',
				tooltip: 'Fecha Emisi�n',
				dataIndex: 'FECHA_EMISION',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'center'			
			},
			{
				header: 'Monto',
				tooltip: 'Monto',
				dataIndex: 'MONTO',
				sortable: true,
				width: 150,			
				resizable: true,				
				align: 'right',
				sortable : true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')		
			}			
		],
		stripeRows: true,
		loadMask: true,
		height: 360,
		width: 320,		
		frame: true
	}
	
//STORES------------------------------------------------------------------------

	var storeMonedaData = new Ext.data.JsonStore({
		root : 'registrosMoneda',
		fields : ['clave', 'descripcion','loadMsg'],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeEstatusData = new Ext.data.JsonStore({
		root : 'registrosEstatus',
		fields : ['clave', 'descripcion','loadMsg'],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeCatPymeData = new Ext.data.JsonStore({
		url: '13forma03ext.data.jsp',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load: procesarBusqAvanzada
		}
	});
	
	var stroreMantoDoctos = new Ext.data.JsonStore({
		root : 'registros',
		fields: [
			{name: 'IC_DOCUMENTO'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'DF_FECHA_DOCTO',	type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'DF_FECHA_VENC',	type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FN_MONTO'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'CD_DESCRIPCION'},
			{name: 'IC_MONEDA'},
			{name: 'CD_NOMBRE'},
			{name: 'CLAVE_PYME'},
			{name: 'MONTOPORCENTAJE',		type: 'float'},
			{name: 'MONTODESCUENTO',		type: 'float'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'NOMBREIF'},
			{name: 'IC_ESTATUS_DOCTO'},
			{name: 'DF_FECHA_VENC_PYME'},
			{name: 'MANDANTE'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'SELECCIONESTATUS'},
			{name: 'FECRECEP', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'TIPOCOMPRA'},
			{name: 'CLASIFICADOR'},
			{name: 'PLAZOMAX'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			//beforeLoad: enviaParametros,
			load: procesarConsMantoDoctos,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsMantoDoctos(null, null, null);	//LLama procesar consulta, para que desbloquee los componentes.
				}
			}
		}
	});
	
	var storeTotales = new Ext.data.JsonStore({
		root : 'registrosTotales',
		fields : [
			{name: 'MONEDA'},
			{name: 'TOTAL_DOCUMENTOS'},
			{name: 'MONTO_TOTAL'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var storeTotalesAcuse = new Ext.data.ArrayStore({
		fields : [
			{name: 'MONEDA'},
			{name: 'TOTAL_DOCUMENTOS'},
			{name: 'MONTO_TOTAL'}
		]
	});
	
	var stroreMantoDoctosAcuse = new Ext.data.ArrayStore({
		fields: [
			{name: 'IC_DOCUMENTO'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'DF_FECHA_DOCTO',	type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'DF_FECHA_VENC',	type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'FN_MONTO'},
			{name: 'CG_RAZON_SOCIAL'},
			{name: 'CD_DESCRIPCION'},
			{name: 'IC_MONEDA'},
			{name: 'CD_NOMBRE'},
			{name: 'USUARIO'},
			{name: 'ESTATUSACTUAL'},
			{name: 'CLAVE_PYME'},
			{name: 'MONTOPORCENTAJE',		type: 'float'},
			{name: 'MONTODESCUENTO',		type: 'float'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'NOMBREIF'},
			{name: 'IC_ESTATUS_DOCTO'},
			{name: 'DF_FECHA_VENC_PYME'},
			{name: 'MANDANTE'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'FECRECEP'},
			{name: 'TIPOCOMPRA'},
			{name: 'CLASIFICADOR'},
			{name: 'PLAZOMAX'}
		]
	});
	
	
	
	var storeEstatusAsignar = new Ext.data.JsonStore({
		root : 'registrosEstatus',
		fields : ['clave', 'descripcion','loadMsg'],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	

//COMPONENTES------------------------------------------------------------------
	var elementsFormBusq = [ 
		{ 
			xtype:   'label',  
			html:		'<b>Campos de B�squeda:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'textfield',
			name: 'nombreProv',
			id: 'nombreProv1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'rfcFiado',
			id: 'rfcFiado1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{
			xtype: 'textfield',
			name: 'numProveedor',
			id: 'numProveedor1',
			fieldLabel: 'N�mero Proveedor',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			width: 80,
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
		},
		{ 
			xtype:   'label',  
			html:		'Utilice el * para realizar una b�squeda gen�rica.', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
			  items: [
					{
						xtype: 	'button',
						text: 	'Buscar',
						id: 		'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							fpBusqAvanzada.el.mask('Buscando...','x-mask-loading');
							storeCatPymeData.load({
								params: Ext.apply(fpBusqAvanzada.getForm().getValues(),{
									informacion: 'BusquedaAvanzada'
								})
							});
						},
						style: { 
							  marginBottom:  '10px' 
						} 
					}
			  ]
		},
		{ 
			xtype:   'label',  
			html:		'<hr>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
		},
		{ 
			xtype:   'label',  
			html:		'<b>Resultados:</b>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'center',
				marginBottom:  '10px'
			} 
		},
		{
			xtype: 'combo',
			name: 'cboPyme',
			id: 'cboPyme1',
			mode: 'local',
			autoLoad: false,
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'ic_fiado',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeCatPymeData
		}
	];




//Grids----------------------------------

	var gridMantoDoctos = new Ext.grid.EditorGridPanel({
	id: 'gridMantoDoctos1',
	store: stroreMantoDoctos,
	margins: '20 0 0 0',
	clicksToEdit: 1,
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	columns: [
		{//1
			header: 'Nombre Proveedor',
			tooltip: 'Nombre Proveedor',
			dataIndex: 'CG_RAZON_SOCIAL',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false
		},
		{//2
			header: 'N�mero de Documento ',
			tooltip: 'N�mero de Documento',
			dataIndex: 'IG_NUMERO_DOCTO',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false
		},
		{//3
			header: 'Fecha de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex: 'DF_FECHA_DOCTO',
			sortable: true,
			width: 100,
			align: 'left',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//4
			header: 'Fecha de Vencimiento',
			tooltip: 'Fecha de Vencimiento',
			dataIndex: 'DF_FECHA_VENC',
			sortable: true,
			width: 100,
			align: 'left',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//5
			header: 'Fecha Vencimiento Proveedor',
			tooltip: 'Fecha Vencimiento Proveedor',
			dataIndex: 'DF_FECHA_VENC_PYME',
			sortable: true,
			hidden : true,
			width: 100,
			align: 'left',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//6
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'CD_NOMBRE',
			width : 150,
			sortable : true
		},
		{//7
			header : 'Tipo Factoraje',
			tooltip: 'Tipo Factoraje',
			dataIndex : 'TIPO_FACTORAJE',
			width : 150,
			sortable : true
		},
		{//8
			header : 'Monto',
			tooltip: 'Monto',
			dataIndex : 'FN_MONTO',
			width : 150,
			align: 'right',
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//9
			header : 'Porcentaje de Descuento',
			tooltip: 'Porcentaje de Descuento',
			dataIndex : 'MONTOPORCENTAJE',//pendiente calculo de porcentaje
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.numberRenderer('0,0.00%')
		},
		{//10
			header : 'Monto a Descontar',
			tooltip: 'Monto a Descontar',
			dataIndex : 'MONTODESCUENTO',
			align: 'right',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},		
		{
			xtype:	'actioncolumn',
			header: 'Estatus',
			tooltip: 'Estatus',					
			align: 'center',				
			width: 150,
			renderer: function(value, metadata, record, rowindex, colindex, store) {
				return (record.get('CD_DESCRIPCION'));
			},
			items: [
				{
					getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
						if(registro.get('IC_ESTATUS_DOCTO')==33) {
							this.items[0].tooltip = 'Ver';
							return 'iconoLupa';							
						}
					}
					,handler:	verDetalle  
				}
			]
		},		
		{//12
			xtype: 'checkcolumn',
			header : 'Seleccion Estatus',
			dataIndex : 'SELECCIONESTATUS',
			width : 50,
			sortable : false
		},
		{//13
			header : 'Nombre IF',
			tooltip: 'Nombre IF',
			dataIndex : 'NOMBREIF',
			width : 150,
			sortable : true,
			hidden : true
		},
		{//14
			header : 'Fecha de Recepci�n de Bienes y Servicios',
			tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
			dataIndex : 'FECRECEP',
			width : 150,
			sortable : true,
			hidden : true,
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//15
			header : 'Tipo de Compra',
			tooltip: 'Tipo de Compra (Procedimientos)',
			dataIndex : 'TIPOCOMPRA',
			sortable : true,
			hidden : true,
			width : 100,
			align: 'center'
		},
		{//16
			header : 'Clasificador por Objeto del Gasto',
			tooltip: 'Clasificador por Objeto del Gasto',
			dataIndex : 'CLASIFICADOR',
			sortable : true,
			hidden : true,
			width : 80,
			align: 'center'
		},
		{//17
			header : 'Plazo M�ximo',
			tooltip: 'Plazo M�ximo',
			dataIndex : 'PLAZOMAX',
			width : 80,
			sortable : true,
			hidden : true,
			align: 'center'
		},
		{//18
			header : 'Mandante',
			tooltip: 'Mandante',
			dataIndex : 'MANDANTE',
			hidden: true,
			sortable : true,
			hidden : true,
			width : 100,
			align: 'center'
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 300,
	width: 870,
	//autoHeight: true,
	style: 'margin:0 auto;',
	title: '',
	frame: true
	});
	
	var gridMantoDoctosAcuse = new Ext.grid.GridPanel({
		id: 'gridMantoDoctosAcuse1',
		store: stroreMantoDoctosAcuse,
		margins: '20 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{//1
				header: 'Nombre Proveedor',
				tooltip: 'Nombre Proveedor',
				dataIndex: 'CG_RAZON_SOCIAL',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//2
				header: 'N�mero de Documento ',
				tooltip: 'N�mero de Documento',
				dataIndex: 'IG_NUMERO_DOCTO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false
			},
			{//3
				header: 'Fecha de Emisi�n',
				tooltip: 'Fecha de Emisi�n',
				dataIndex: 'DF_FECHA_DOCTO',
				sortable: true,
				hideable: false,
				width: 100,
				align: 'left',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{//4
				header: 'Fecha de Vencimiento',
				tooltip: 'Fecha de Vencimiento',
				dataIndex: 'DF_FECHA_VENC',
				sortable: true,
				hideable: false,
				width: 100,
				align: 'left',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{//5
				header: 'Fecha Vencimiento Proveedor',
				tooltip: 'Fecha Vencimiento Proveedor',
				dataIndex: 'DF_FECHA_VENC_PYME',
				sortable: true,
				hideable: false,
				hidden : true,
				width: 100,
				align: 'left',
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{//6
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'CD_NOMBRE',
				width : 150,
				sortable : true
			},
			{//7
				header : 'Tipo Factoraje',
				tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				width : 150,
				sortable : true
			},
			{//8
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'FN_MONTO',
				width : 150,
				align: 'right',
				sortable : true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{//9
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'MONTOPORCENTAJE',//pendiente calculo de porcentaje
				sortable : true,
				width : 100,
				renderer: Ext.util.Format.numberRenderer('0,0.00%')
			},
			{//10
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTODESCUENTO',
				sortable : true,
				align: 'right',
				width : 100,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{//11
				header : 'Estatus Anterior',
				tooltip: 'Estatus Anterior',
				dataIndex : 'CD_DESCRIPCION',
				width : 150,
				sortable : true
			},
			{//11
				header : 'Usuario',
				tooltip: 'Usuario',
				dataIndex : 'USUARIO',
				width : 150,
				sortable : true,
				renderer : function(causa, columna, registro){
					return ObjGral.strLogin+ ' ' +ObjGral.strNombreUsuario;
				}
			},
			{//11
				header : 'Estatus Actual',
				tooltip: 'Estatus Actual',
				dataIndex : 'ESTATUSACTUAL',
				width : 150,
				sortable : true,
				renderer:  function (causa, columna, registro){
					var cboEstatusNuevo = Ext.getCmp('ic_estatus_asignar1');
					var record = cboEstatusNuevo.findRecord(cboEstatusNuevo.valueField, cboEstatusNuevo.getValue());
					return record.get(cboEstatusNuevo.displayField);
				}
			},
			{//13
				header : 'Nombre IF',
				tooltip: 'Nombre IF',
				dataIndex : 'NOMBREIF',
				width : 150,
				sortable : true,
				hidden : true
			},
			{//14
				header : 'Fecha de Recepci�n de Bienes y Servicios',
				tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
				dataIndex : 'FECRECEP',
				width : 150,
				sortable : true,
				hidden : true,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{//15
				header : 'Tipo de Compra',
				tooltip: 'Tipo de Compra (Procedimientos)',
				dataIndex : 'TIPOCOMPRA',
				sortable : true,
				hidden : true,
				width : 100,
				align: 'center'
			},
			{//16
				header : 'Clasificador por Objeto del Gasto',
				tooltip: 'Clasificador por Objeto del Gasto',
				dataIndex : 'CLASIFICADOR',
				sortable : true,
				hidden : true,
				width : 80,
				align: 'center'
			},
			{//17
				header : 'Plazo M�ximo',
				tooltip: 'Plazo M�ximo',
				dataIndex : 'PLAZOMAX',
				width : 80,
				sortable : true,
				hidden : true,
				align: 'center'
			},
			{//18
				header : 'Mandante',
				tooltip: 'Mandante',
				dataIndex : 'MANDANTE',
				hidden: true,
				sortable : true,
				hidden : true,
				width : 100,
				align: 'center'
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 300,
		width: 870,
		//autoHeight: true,
		style: 'margin:0 auto;',
		title: '',
		frame: true
	});
	
	
	var gridMantoDoctosTotales = new Ext.grid.GridPanel({
		id: 'gridMantoDoctosTotales1',
		store: storeTotales,
		margins: '20 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{//1
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 283,
				resizable: true,
				hidden: false
			},
			{//2
				header : 'TOTAL DE DOCUMENTOS',
				dataIndex : 'TOTAL_DOCUMENTOS',
				width : 300,
				sortable : true,
				align: 'center'
			},
			{//3
				header : 'MONTO TOTAL',
				dataIndex : 'MONTO_TOTAL',
				sortable : true,
				width : 270,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 100,
		width: 870,
		//autoHeight: true,
		style: 'margin:0 auto;',
		title: '',
		frame: true
	});
	
	var gridMantoDoctosTotalesAcuse = new Ext.grid.GridPanel({
		id: 'gridMantoDoctosTotalesAcuse1',
		store: storeTotalesAcuse,
		margins: '20 0 0 0',
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{//1
				header: 'MONEDA',
				dataIndex: 'MONEDA',
				sortable: true,
				width: 283,
				resizable: true,
				hidden: false
			},
			{//2
				header : 'TOTAL DE DOCUMENTOS',
				dataIndex : 'TOTAL_DOCUMENTOS',
				width : 300,
				sortable : true,
				align: 'center'
			},
			{//3
				header : 'MONTO TOTAL',
				dataIndex : 'MONTO_TOTAL',
				sortable : true,
				width : 270,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 130,
		width: 870,
		//autoHeight: true,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'center',
			items: [
				'-',
				{
					text: 'Regresar',
					handler: function(){
						window.location.href='13forma03ext.jsp';
					}
				},
				'-'
			]
		}
	});
	
	
//Forms----------------------------------
	var formMantoDocto = new Ext.form.FormPanel({
		id: 'formMantoDocto1',
		title: '',
		frame: true,
		width: 500,
		bodyStyle: 'padding: 6px',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		//monitorValid: true,
		items:[
			{
				xtype: 'textfield',
				name: 'txtNumProveedor',
				id: 'txtNumProveedor1',
				fieldLabel: 'Clave del Proveedor',
				allowBlank: true,
				maxLength: 25,	//ver el tama�o maximo del numero en BD para colocar este igual
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0',  //necesario para mostrar el icono de error
				listeners: {
					change: function(objText, valorNuevo, valorViejo){
						if(valorNuevo != valorViejo){
							pnl.el.mask('Buscando...','x-mask-loading');
							Ext.Ajax.request({
								url: '13forma03ext.data.jsp',
								params:{
									informacion: 'ConsultaClaveProv',
									txtNumProveedor: valorNuevo
								},
								callback: procesarSuccessFailureConsCveProv
							});
						}
						
					}
				}
			},
			{
				xtype: 'textfield',
				name: 'txtCadenasPymes',
				id: 'txtCadenasPymes1',
				fieldLabel: 'Nombre del Proveedor',
				allowBlank: true,
				disabled: true,
				maxLength: 25,
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0'
			},
			{
				xtype: 'compositefield',
				fieldLabel: '',
				//anchor: '-400',
				msgTarget: 'side',
				combineErrors: false,
				items: [
				{
					xtype: 'button',
					text: 'Busqueda Avanzada...',
					id: 'btnBusqAv',
					handler: function(boton, evento) {
						
						var ventana = Ext.getCmp('winBusqAvan');
						if (ventana) {
							ventana.show();
						} else {
							new Ext.Window({
									title: 			'B�squeda Avanzada',
									layout: 			'fit',
									modal:			true,
									width: 			400,
									height: 			350,
									minWidth: 		400,
									minHeight: 		300,
									buttonAlign: 	'center',
									id: 				'winBusqAvan',
									closeAction: 	'hide',
									items: 			fpBusqAvanzada
								}).show();
						}
						
					}
				},
				{
						xtype: 'displayfield',
						value: ''
				}
				]
			},
			{
				xtype: 'textfield',
				name: 'ig_numero_docto',
				id: 'ig_numero_docto1',
				fieldLabel: 'N�mero de Documento',
				allowBlank: true,
				maxLength: 15,
				width: 150,
				msgTarget: 'side',
				margins: '0 20 0 0' 
			},
			{
				xtype: 'datefield',
				name: 'df_fecha_docto',
				id: 'df_fecha_docto',
				fieldLabel: 'Fecha de Emisi�n',
				allowBlank: true,
				startDay: 0,
				width: 100,
				msgTarget: 'side',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},
			{
				xtype: 'datefield',
				name: 'df_fecha_venc',
				id: 'df_fecha_venc',
				fieldLabel: 'Fecha de Vencimiento',
				allowBlank: true,
				startDay: 0,
				width: 100,
				msgTarget: 'side',
				margins: '0 20 0 0'  //necesario para mostrar el icono de error
			},
			{
				xtype: 'combo',
				name: 'ic_moneda',
				id: 'ic_moneda1',
				fieldLabel: 'Moneda',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'ic_moneda',
				emptyText: 'Seleccione...',
				autoSelect :true,
				width: 400,
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				tpl : NE.util.templateMensajeCargaCombo,
				store : storeMonedaData
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Monto entre',
				combineErrors: false,
				hidden: false,
				msgTarget: 'side',
				items: [
					{
						xtype: 'numberfield',
						name: 'fn_montoMin',
						id: 'fn_montoMin1',
						allowBlank: true,
						maxLength: 15,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'y',
						width: 100
					},
					{
						xtype: 'numberfield',
						name: 'fn_montoMax',
						id: 'fn_montoMax1',
						maxLength: 15,
						allowBlank: true,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}
				]
			},
			{
				xtype: 'combo',
				name: 'ic_estatus_consulta',
				id: 'ic_estatus_consulta1',
				fieldLabel: 'Estatus',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'ic_estatus_consulta',
				emptyText: 'Seleccione...',
				autoSelect :true,
				width: 400,
				forceSelection : true,
				allowBlank: true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				tpl : NE.util.templateMensajeCargaCombo,
				store : storeEstatusData
			}
		],
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				handler: function(btn){
					//validacione de fecha
					var valido = true;
					
					valido = validaFechas(Ext.getCmp('df_fecha_docto'), Ext.getCmp('df_fecha_venc'));
					
					if(valido)
						valido = validaMontos(Ext.getCmp('fn_montoMin1'), Ext.getCmp('fn_montoMax1'));
					
					if (Ext.isEmpty(Ext.getCmp('ic_estatus_consulta1').getValue())) {
						valido = false;
						Ext.getCmp('ic_estatus_consulta1').markInvalid('Debe seleccionar un estatus');
						
					}
					
					if(valido){
						pnl.el.mask('Cargando...','x-mask-loading');
						Ext.Ajax.request({
							url: '13forma03ext.data.jsp',
								params: Ext.apply(formMantoDocto.getForm().getValues(),{
									ic_pyme: ObjGral.ic_pyme,
									informacion: 'ConsultaDoctosManto'
							 }),
							callback: procesarSuccessFailureBtnConsultar
						});
					}
				}
			},
			{
				text: 'Limpiar',
				id: 'btnLimpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location.href='13forma03ext.jsp';
					//fp.getForm().reset();
				}
			}
		]
	});
	
	
	var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 			'fBusqAvanzada',
      labelWidth: 57,
		frame: 		true,
		bodyStyle: 	'padding: 8px; padding-right:0px;padding-left:16px;',
		layout:		'form',
      defaults: {
         xtype: 		'textfield',
			msgTarget: 	'side',
			anchor: 		'-20'
		},
		items: 			elementsFormBusq,
		monitorValid: 	true,
		buttons: [
			{
				text: 'Aceptar',
				iconCls: 'icoAceptar',
				formBind: true,
				disabled: true,
				handler: function(boton, evento) {					
					var cboPyme1= Ext.getCmp("cboPyme1");
					var storeCmb = cboPyme1.getStore();
					var ventana = Ext.getCmp('winBusqAvan');
					
					if (Ext.isEmpty(cboPyme1.getValue())) {
						cboPyme1.markInvalid('Seleccione Proveedor');
						return;
					}else{
						ObjGral.ic_pyme = cboPyme1.getValue();
					}
					var record = cboPyme1.findRecord(cboPyme1.valueField, cboPyme1.getValue());
					var descripcion = record.data['descripcion'];
					
					if(descripcion.indexOf('|')>0){
						Ext.getCmp('txtNumProveedor1').setValue(descripcion.substring(0,descripcion.indexOf('|')));
						Ext.getCmp('txtCadenasPymes1').setValue(descripcion.substring(descripcion.indexOf('|')+1));
					}
					
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
			},
			{
				text: 'Cancelar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					var ventana = Ext.getCmp('winBusqAvan');
					fpBusqAvanzada.getForm().reset();
					ventana.hide();
				}
				
			}
		]
	});
			
	
//panels---------------------------------
	var pnlMantoDoctos = new Ext.Panel({
		id: 'pnlMantoDoctos1',
		frame: true,
		title: 'Mantenimiento de Documentos',
		items: [
			formMantoDocto,
			NE.util.getEspaciador(20)
		]
	})
	
	var pnlCommentEstatus = new Ext.form.FormPanel({
		id: 'pnlCommentEstatus',
		frame: false,
		border: false,
		title: '',
		monitorValid: true,
		width: 870,
		items: [
			{
				xtype: 'compositefield',
				fieldLabel: 'Causa',
				combineErrors: false,
				defaults: {
					msgTarget: 'side',
					anchor: '-20'
				},
				items: [
					 {xtype: 'textarea', name: 'txtCausa', id:'txtCausa1', width:300, maxLenght:259, allowBlank: false, enableKeyEvents: true,
						 listeners:{
								'keydown':	function(txtA){
												if (	!Ext.isEmpty(txtA.getValue())	){
													var numero = (txtA.getValue().length);
													Ext.getCmp('contador').setValue(260-numero);
													if (	Ext.getCmp('contador').getValue() < 0	) {
														var cadena = (txtA.getValue()).substring(0,260);
														txtA.setValue(cadena);
														Ext.getCmp('contador').setValue(0);
													}
												}
											},
								'keyup':	function(txtA){
												if (	Ext.isEmpty(txtA.getValue())	){
													Ext.getCmp('contador').reset();
													pnlCommentEstatus.getForm().reset();
												}
											},
								'blur':	function(txtA){
												if (	Ext.isEmpty(txtA.getValue())	){
													pnlCommentEstatus.getForm().reset();
												}									
											}
							}
					 },
					 {xtype:'displayfield',	id:'disEspacio1',	width:50,	text:''},
					 {xtype: 'displayfield', value: 'Estatus por Asignar'},
					 {xtype: 'combo', name: 'ic_estatus_asignar', id: 'ic_estatus_asignar1',
						mode: 'local', displayField : 'descripcion', valueField : 'clave', hiddenName : 'ic_estatus_asignar',
						emptyText: 'Seleccione...', autoSelect :true, width: 200, forceSelection : true, triggerAction : 'all',
						allowBlank: false,
						typeAhead: true, minChars : 1, tpl : NE.util.templateMensajeCargaCombo, store : storeEstatusAsignar
					 }
				]
		  },
		  {
				xtype: 'compositefield',
				combineErrors: false,
				msgTarget: 'side',
				items: [
					{
						xtype:'displayfield',	id:'disEspacio',	width: 1,	text:''
					},
					{
						xtype:'displayfield',	id:'disChar',	width: 120,	value:'Caracteres Restantes:'
					},
					{
						xtype: 'numberfield',
						maxLength:3,
						value:	260,
						width: 30,
						id: 'contador',
						name:	'contador',
						readOnly: true	
				  }
				]
			}
		],
		buttons: [
				{
				text: 'Cambiar Estatus',
				id: 'btnCambiarEstatus',
				//formBind: true,
				handler: fnBtnCambiarEstatus
				}
			]
		
	})

//Containers-----------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		items: [
			NE.util.getEspaciador(20),
			pnlMantoDoctos
		]
	});
	
	
	pnl.el.mask('Cargando...','x-mask-loading');
	
	Ext.Ajax.request({
		url: '13forma03ext.data.jsp',
		params: {
			informacion: 'DatosIniciales'
		},
		callback: procesarSuccessDatosIniciales
	});  



});