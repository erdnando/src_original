<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		java.math.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.descuento.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>
<%
	String informacion = (request.getParameter("informacion") != null)?request.getParameter("informacion"):"";
	String infoRegresar = "";
	if(informacion.equals("valoresIniciales")){
		boolean bOperaFactorajeVencido = false;
		boolean bOperaFactorajeDistribuido = false;
		boolean bOperaFactorajeNotaDeCredito=false; 
		boolean bOperaFactConMandato=false;
		boolean bOperaFactVencimientoInfonavit = false;
		boolean bTipoFactoraje = false;
		boolean bFactorajeIf = false;
		
		String sOperaNotas = "";
		
		SimpleDateFormat formatoHora2 = new SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a");
		
		ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
		String sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(iNoCliente);
		
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		
		HashMap map = BeanParamDscto.getOperaFactoraje(iNoCliente);
		bOperaFactorajeVencido = ((Boolean)map.get("bOperaFactorajeVencido")).booleanValue();
		bOperaFactorajeDistribuido = ((Boolean)map.get("bOperaFactorajeDistribuido")).booleanValue();
		bOperaFactorajeNotaDeCredito = ((Boolean)map.get("bOperaFactorajeNotaDeCredito")).booleanValue();
		
		if(bOperaFactorajeNotaDeCredito){
			sOperaNotas = "S";
		}
		
		Hashtable alParamEPO1 = new Hashtable();
		alParamEPO1 = BeanParamDscto.getParametrosEPO(iNoCliente,1);
		if(alParamEPO1!=null){
			bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
			bOperaFactorajeVencido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
			bOperaFactorajeDistribuido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
			bOperaFactorajeNotaDeCredito = ("N".equals(alParamEPO1.get("OPERA_NOTAS_CRED").toString()))?false:true;
			bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;
			bFactorajeIf								=	("N".equals(alParamEPO1.get("FACTORAJE_IF").toString()))?false:true; //FODEA Noviembre 2012
		}
		bTipoFactoraje = ((bOperaFactorajeVencido||bOperaFactorajeDistribuido||bOperaFactorajeNotaDeCredito||bOperaFactConMandato||bOperaFactVencimientoInfonavit||bFactorajeIf||bFactorajeIf)==true)?true:false;
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success",new Boolean(true));
		jsonObj.put("strNombreUsuario", strNombreUsuario);
		jsonObj.put("fechaHora",formatoHora2.format(new java.util.Date()));
		jsonObj.put("sFechaVencPyme",sFechaVencPyme);
		jsonObj.put("bOperaFactConMandato",new Boolean(bOperaFactConMandato));
		jsonObj.put("bOperaFactorajeVencido",new Boolean(bOperaFactorajeVencido));
		jsonObj.put("bOperaFactorajeDistribuido",new Boolean(bOperaFactorajeDistribuido));
		jsonObj.put("bOperaFactorajeNotaDeCredito",new Boolean(bOperaFactorajeNotaDeCredito));
		jsonObj.put("bOperaFactVencimientoInfonavit",new Boolean(bOperaFactVencimientoInfonavit));
		jsonObj.put("bTipoFactoraje",new Boolean(bTipoFactoraje));
		jsonObj.put("bFactorajeIf",new Boolean(bFactorajeIf));
		jsonObj.put("sOperaNotas", sOperaNotas);
		jsonObj.put("strAforo",strAforo);
		jsonObj.put("strAforoDL",strAforoDL);
		infoRegresar = jsonObj.toString();
	}
 	else if(informacion.equals("CatalogoEstatusDist")){
		CatalogoSimple cat1 = new CatalogoSimple();
		cat1.setCampoClave("ic_estatus_docto");
		cat1.setCampoDescripcion("cd_descripcion");
		cat1.setTabla("comcat_estatus_docto");
		cat1.setValoresCondicionIn("2,3,4,11,24,26",Integer.class);
		cat1.setOrden("1");
		List elementos = cat1.getListaElementos();
		JSONArray jsonArr = new JSONArray();
		JSONObject jsonObj = new JSONObject();
		jsonArr = jsonArr.fromObject(elementos);
		infoRegresar = "{\"success\": true, \"total\": \"" + elementos.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	}else if(informacion.equals("Consulta")||informacion.equals("ArchivoPDF")
	||informacion.equals("ResumenTotales")){
		double dblPorcentaje = 0;
		String tipoFactoraje = "";
		BigDecimal mtodesc=new BigDecimal("0.0");
		BigDecimal nac_desc=new BigDecimal("0.0"); 
		BigDecimal dol_desc=new BigDecimal("0.0");
		String netoRecibir="";
		com.netro.descuento.RepDoctosEpoDEbean paginador = new com.netro.descuento.RepDoctosEpoDEbean();
		String estatusDocto		= (request.getParameter("status") != null)?request.getParameter("status"):"";
		String sFechaVencPyme	= (request.getParameter("sFechaVencPyme") != null)?request.getParameter("sFechaVencPyme"):""; 
		String sOperaNotas		= (request.getParameter("sOperaNotas") != null)?request.getParameter("sOperaNotas"):""; 
		
		Boolean bOperaFactorajeVencido1 = Boolean.valueOf(request.getParameter("bOperaFactorajeVencido"));
		Boolean bOperaFactorajeDistribuido1 = Boolean.valueOf(request.getParameter("bOperaFactorajeDistribuido"));
		Boolean bOperaFactorajeNotaDeCredito1 = Boolean.valueOf(request.getParameter("bOperaFactorajeNotaDeCredito"));
		Boolean bOperaFactConMandato1 = Boolean.valueOf(request.getParameter("bOperaFactConMandato"));
		Boolean bOperaFactVencimientoInfonavit1 = Boolean.valueOf(request.getParameter("bOperaFactVencimientoInfonavit"));
		Boolean bTipoFactoraje1 = Boolean.valueOf(request.getParameter("bTipoFactoraje"));
		
		boolean bOperaFactorajeVencido = bOperaFactorajeVencido1.booleanValue(); 
		boolean bOperaFactorajeDistribuido = bOperaFactorajeDistribuido1.booleanValue();
		boolean bOperaFactorajeNotaDeCredito = bOperaFactorajeNotaDeCredito1.booleanValue();
		boolean bOperaFactConMandato = bOperaFactConMandato1.booleanValue();
		boolean bOperaFactVencimientoInfonavit = bOperaFactVencimientoInfonavit1.booleanValue();
		boolean bTipoFactoraje = bTipoFactoraje1.booleanValue();
		
		if(estatusDocto.equals("4")){
			estatusDocto = "4,16";
		}
		paginador.setiNoEstatusDocto(estatusDocto);
		paginador.setiNoEpo(iNoCliente);
		paginador.setsIdiomaUsuario(sesIdiomaUsuario);
		paginador.setsFechaVencPyme(sFechaVencPyme);
		paginador.setBOperaFactorajeVencido(bOperaFactorajeVencido);
		paginador.setBOperaFactorajeDistribuido(bOperaFactorajeDistribuido);
		paginador.setBOperaFactorajeNotaDeCredito(bOperaFactorajeNotaDeCredito);
		paginador.setBOperaFactConMandato(bOperaFactConMandato);
		paginador.setBOperaFactVencimientoInfonavit(bOperaFactVencimientoInfonavit);
		paginador.setsOperaNotas(sOperaNotas);
		paginador.setBTipoFactoraje(bTipoFactoraje);
		
		String nombreIf = "";
		String nombreBeneficiario = "";
		String porcBeneficiario = "";
		String montoBeneficiario = "";
		//Negociable
		if(estatusDocto.equals("2")){
			paginador.setPorcentaje(strAforo);
			paginador.setPorcentajeDL(strAforoDL);
		}
		CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
		if(informacion.equals("Consulta")){
			try{
				Registros reg	= queryHelper.doSearch();
				//Negociable
				while(reg.next()){
					int moneda = Integer.parseInt(reg.getString("IC_MONEDA").trim());
					double monto = Double.parseDouble(reg.getString("FN_MONTO").trim());
					if(estatusDocto.equals("2")){//Negociable
						if(moneda == 1){
							dblPorcentaje = Double.parseDouble(strAforo)*100;
						}else if(moneda == 54){
							dblPorcentaje = Double.parseDouble(strAforoDL)*100;
						}
						mtodesc = new BigDecimal(reg.getString("FN_MONTO_DSCTO"));
						tipoFactoraje = (reg.getString("CS_DSCTO_ESPECIAL")==null)?"":reg.getString("CS_DSCTO_ESPECIAL");
						nombreIf = "";
						if(bOperaFactorajeVencido){//Para factoraje vencido
							if(tipoFactoraje.equals("V")||tipoFactoraje.equals("A")||tipoFactoraje.equals("M")||bOperaFactConMandato||bOperaFactVencimientoInfonavit){
								dblPorcentaje = 100;
								mtodesc = new BigDecimal(monto);
								nombreIf = (reg.getString("NOMBREIF")==null)?"":reg.getString("NOMBREIF").trim();
							}
						}
						if(bOperaFactVencimientoInfonavit){// para Factoraje Vencimiento Infonavit FODEA 042 - Agosto/2009
							nombreIf = (reg.getString("NOMBREIF")==null)?"":reg.getString("NOMBREIF").trim();
						}
						nombreBeneficiario = "";
						porcBeneficiario = "";
						montoBeneficiario = "";
						if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit){//Para Factoraje Distribuido
							if(tipoFactoraje.equals("D")||tipoFactoraje.equals("I")){
								dblPorcentaje = 100;
								mtodesc = new BigDecimal(monto);
								nombreBeneficiario = (reg.getString("NOMBRE_BENEFICIARIO")==null)?"":reg.getString("NOMBRE_BENEFICIARIO");
								porcBeneficiario = (reg.getString("FN_PORC_BENEFICIARIO")==null)?"":reg.getString("FN_PORC_BENEFICIARIO");
								montoBeneficiario = (reg.getString("MONTO_BENEFICIARIO")==null)?"":reg.getString("MONTO_BENEFICIARIO");							
							}
						}
						if(bOperaFactorajeNotaDeCredito){// aqui new para factoraje Nota de Credito
							if(tipoFactoraje.equals("C")){
								dblPorcentaje = 100;
								mtodesc = new BigDecimal(monto);
							}
						}
						reg.setObject("DBL_PORCENTAJE",String.valueOf(dblPorcentaje));
						reg.setObject("FN_MONTO_DSCTO",mtodesc.toPlainString());
						reg.setObject("NOMBREIF",nombreIf);
						reg.setObject("NOMBRE_BENEFICIARIO",nombreBeneficiario);
						reg.setObject("FN_PORC_BENEFICIARIO",porcBeneficiario);
						reg.setObject("MONTO_BENEFICIARIO",montoBeneficiario);
					}else if(estatusDocto.equals("3")||estatusDocto.equals("26")){//Seleccionada Pyme
						dblPorcentaje = Double.parseDouble(reg.getString("FN_PORC_ANTICIPO").trim());
						mtodesc = new BigDecimal(Double.parseDouble(reg.getString("FN_MONTO_DSCTO").trim()));
						
						if(bOperaFactorajeVencido || bOperaFactConMandato){
							nombreIf = "";
							if(tipoFactoraje.equals("V")||tipoFactoraje.equals("A") || bOperaFactConMandato || bOperaFactVencimientoInfonavit)  {
								dblPorcentaje =  100;
								mtodesc = new BigDecimal(monto);
							}
						}
						netoRecibir=""; 
						nombreBeneficiario = ""; 
						porcBeneficiario=""; 
						montoBeneficiario = "";
						if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { // Para Factoraje Distribuido
							if(tipoFactoraje.equals("D") || tipoFactoraje.equals("I")) {
								dblPorcentaje =  100;
								mtodesc= new BigDecimal(monto);
								netoRecibir = (reg.getString("NETO_RECIBIR")==null)?"":reg.getString("NETO_RECIBIR");
								nombreBeneficiario = (reg.getString("NOMBRE_BENEFICIARIO")==null)?"":reg.getString("NOMBRE_BENEFICIARIO");
								porcBeneficiario = (reg.getString("FN_PORC_BENEFICIARIO")==null)?"":reg.getString("FN_PORC_BENEFICIARIO");
								montoBeneficiario = (reg.getString("MONTO_BENEFICIARIO")==null)?"":reg.getString("MONTO_BENEFICIARIO");
							}
						}
						if(bOperaFactorajeNotaDeCredito){ // aqui new para factoraje Nota de Credito
							if(tipoFactoraje.equals("C")){
								dblPorcentaje =  100;
								mtodesc=new BigDecimal(monto);
							}
						}
						reg.setObject("FN_PORC_ANTICIPO",String.valueOf(dblPorcentaje));
						reg.setObject("FN_MONTO_DSCTO",mtodesc.toPlainString());
						reg.setObject("NETO_RECIBIR",netoRecibir);
						reg.setObject("NOMBRE_BENEFICIARIO",nombreBeneficiario);
						reg.setObject("FN_PORC_BENEFICIARIO",porcBeneficiario);
						reg.setObject("MONTO_BENEFICIARIO",montoBeneficiario);
						
					}else if(estatusDocto.equals("24")||estatusDocto.equals("11")||estatusDocto.equals("4")){//Proceso de Autorizacion IF
						dblPorcentaje = Double.parseDouble(reg.getString("FN_PORC_ANTICIPO").trim());
						mtodesc = new BigDecimal(Double.parseDouble(reg.getString("FN_MONTO_DSCTO").trim()));
						
						if(bOperaFactorajeVencido){// Para Factoraje Vencido
							if(tipoFactoraje.equals("V")){
								dblPorcentaje = 100;
								mtodesc = new BigDecimal(monto);
							}
						}
						netoRecibir=""; 
						nombreBeneficiario = ""; 
						porcBeneficiario=""; 
						montoBeneficiario = "";
						if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { // Para Factoraje Distribuido
							if(tipoFactoraje.equals("D") || tipoFactoraje.equals("I")) {
								dblPorcentaje = 100;
								mtodesc = new BigDecimal(monto);
								netoRecibir = (reg.getString("NETO_RECIBIR")==null)?"":reg.getString("NETO_RECIBIR");
								nombreBeneficiario = (reg.getString("NOMBRE_BENEFICIARIO")==null)?"":reg.getString("NOMBRE_BENEFICIARIO");
								porcBeneficiario = (reg.getString("FN_PORC_BENEFICIARIO")==null)?"":reg.getString("FN_PORC_BENEFICIARIO");
								montoBeneficiario = (reg.getString("MONTO_BENEFICIARIO")==null)?"":reg.getString("MONTO_BENEFICIARIO");							
							}
						}
						if(bOperaFactorajeNotaDeCredito){ // aqui new para factoraje Nota de Credito
							if(tipoFactoraje.equals("C")){
								dblPorcentaje =  100;
								mtodesc = new BigDecimal(monto);							
							}
						}
						reg.setObject("FN_PORC_ANTICIPO",String.valueOf(dblPorcentaje));
						reg.setObject("FN_MONTO_DSCTO",mtodesc.toPlainString());
						reg.setObject("NETO_RECIBIR",netoRecibir);
						reg.setObject("NOMBRE_BENEFICIARIO",nombreBeneficiario);
						reg.setObject("FN_PORC_BENEFICIARIO",porcBeneficiario);
						reg.setObject("MONTO_BENEFICIARIO",montoBeneficiario);
					}
				}
				infoRegresar	= 
					"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";
			}catch(Exception e){
				throw new AppException("Error al intentar mostrar los datos",e);
			}
		}
		else if(informacion.equals("ArchivoPDF")){
			try{
				String nombreArchivo = queryHelper.getCreateCustomFile(request,strDirectorioTemp,"PDF");
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("success",new Boolean(true));
				jsonObj.put("urlArchivo",strDirecVirtualTemp+nombreArchivo);
				infoRegresar = jsonObj.toString();				
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF", e);
			}
		}
		else if(informacion.equals("ResumenTotales")){
			session.setAttribute("_resumenTotales", "S");
			Registros reg = queryHelper.getResultCount(request);
				infoRegresar	=
					"{\"success\": true, \"total\": \""	+	reg.getNumeroRegistros() + "\", \"registros\": " + reg.getJSONData()+ "}";	
		}
	}
%>
<%=infoRegresar%>
