
Ext.onReady(function() {
	var strUsuario = Ext.getDom('hidStrUsuario').value;
	var strUsr = Ext.getDom('hidStrUsr').value;
	var validaCder = true;
	//Objeto para validar si ya estan cargados los combos necesarios antes de realizar
	//una consulta automatica, basandose en los parametros recibidos
	var Inicializacion = {
		catalogoMoneda : false,
		catalogoEpo: false
	};


	var objGral={
		inicial:'S',
		strTipoUsuario:null,
		tipo_servicio:null,
		urlCapturaDatos:null,
		fecDiaSigHabil:null,
		msgDiaSigHabil:null,
		msgOperEpoFact24hrs:null,
		msgFecVencPyme:null,
		iNoEPO:null,
		valorTC: null,
		maximos: null,
		utilizados: null,
		intermediarios: null,
		ifValidar: null,
		lineasDisp: null,
		tipoLimite: null,
		montoComprometido: null,
		sNombrePyme: null,
		operaNotasDeCredito: null,
		aplicarNotasDeCreditoAVariosDoctos: null,
		shownotas: null,
		valEPO: null,
		valIF: null,
		msgError: null
	}

	var objCalc={
		totalMonto:0,
		totalDescuento:0,
		totalIntereses:0,
		totalRecibir:0,
		totaldocs:0
	}

//-----------------------------FUNCTIONS-------------------------------------

	var realizaConfirmacionNAFIN= function(okResp, pkcs7, textoFirmar){
		var registrosEnviar = [];
		if(okResp=='S'){

				storePreAcuData.each(function(record){
					registrosEnviar.push(record.data);
				});

				Ext.Ajax.request({
					url: '13forma2ext.data.jsp',
					params: {
						informacion: 'validaHorario',
						registros: Ext.encode(registrosEnviar),
						cboEpo: objGral.valEPO

					},
					callback: function(opts, success, response) {
						if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
							var resp = 	Ext.util.JSON.decode(response.responseText);
							if(resp.existenCambios){
								Ext.MessageBox.alert('Aviso', 'Debido al horario de cierre de servicio, es necesario volver a seleccionar los documentos,<br> la operaci�n fue cancelada', function(){
									window.location.href='13forma2ext.jsp';
								});
							}else{
								Ext.Ajax.request({
									url: '13forma2ext.data.jsp',
									params: {
										informacion: 'confirmaClaveCesion',
										registros: Ext.encode(registrosEnviar),
										//cesionUser: resp.cesionUser,
										//cesionPassword: resp.cesionPassword,
										totalDescuento: objCalc.totalDescuento,
										totalMonto: objCalc.totalMonto,
										totalDescuento: objCalc.totalDescuento,
										totalIntereses: objCalc.totalIntereses,
										totalRecibir: objCalc.totalRecibir,
										svalorTC: objGral.valorTC,
										cboEpo: objGral.valEPO,
										cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
										valCve:okResp,
										strUsr:strUsr,
										correo:'',
										email: '',
										Pkcs7: pkcs7,
										TextoFirmado: textoFirmar,
										cvePyme: Ext.getDom('cvePyme').value
									},
									callback: procesarSuccessConfirmaCesion
								});
							}
						} else {
							NE.util.mostrarConnError(response,opts);
						}
					}
				});
			}

	}

	var resulValidCesion= function(okResp, siCorreo, email, errorMessage, vTipoUsuario){
		var registrosEnviar = [];
		if(okResp=='S'){
			if(vTipoUsuario!='NAFIN'){
				storePreAcuData.each(function(record){
					registrosEnviar.push(record.data);
				});


				Ext.Ajax.request({
					url: '13forma2ext.data.jsp',
					params: {
						informacion: 'validaHorario',
						registros: Ext.encode(registrosEnviar),
						cboEpo: objGral.valEPO

					},
					callback: function(opts, success, response) {
						if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
							var resp = 	Ext.util.JSON.decode(response.responseText);
							if(resp.existenCambios){
								Ext.MessageBox.alert('Aviso', 'Debido al horario de cierre de servicio, es necesario volver a seleccionar los documentos,<br> la operaci�n fue cancelada', function(){
									window.location.href='13forma2ext.jsp';
								});
							}else{
								Ext.Ajax.request({
									url: '13forma2ext.data.jsp',
									params: {
										informacion: 'confirmaClaveCesion',
										registros: Ext.encode(registrosEnviar),
										//cesionUser: resp.cesionUser,
										//cesionPassword: resp.cesionPassword,
										totalDescuento: objCalc.totalDescuento,
										totalMonto: objCalc.totalMonto,
										totalDescuento: objCalc.totalDescuento,
										totalIntereses: objCalc.totalIntereses,
										totalRecibir: objCalc.totalRecibir,
										svalorTC: objGral.valorTC,
										cboEpo: objGral.valEPO,
										cboMoneda: Ext.getCmp('cboMoneda1').getValue(),
										valCve:okResp,
										correo:siCorreo,
										email: email
									},
									callback: procesarSuccessConfirmaCesion
								});
							}
						} else {
							NE.util.mostrarConnError(response,opts);
						}
					}
				});

			}else{
				transmitir(Ext.getCmp('btnTransmitir'), true);
			}
		}if(okResp=='R'){
			Ext.Msg.alert('Aviso', 'El folio de seguridad se envi� exitosamente al Sujeto de Apoyo', function(btn){
			var cboEpo = Ext.getCmp('cboEpo1');
			var record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
			var valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
			window.location.href='13forma2ext.jsp?tipoFact=V&cvePyme='+cvePyme.value+'&cboEpo='+valEPO+'&cboMoneda='+hidCboMoneda.value+"&strUsr="+strUsr;
			});
		}

	}
	//FUNCION: selecciona los documentos por deafult debido a las notas de credito
	var selectDefaultDoctos = function(grid){
		var indiceSm = 0;
		grid.getStore().each(function(record) {
			if(record.data['SELECBOOL']){
				if(record.data['SELECNOTAS']!=''){
					selectModel.selectRow(indiceSm,true);
					//record.data['SELECCION']=true;
				}
			}
			indiceSm = indiceSm+1;
		});
	}

	var recalcular = function(selModel,record, rowIndex, isSelect){
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var cboMoneda = Ext.getCmp('cboMoneda1');
		var notnotas = record.data['SELECBOOL'];//f.aplicado;

		if( record.data['FAC_VENCIDO']=='N')  {
				Ext.MessageBox.alert('Aviso','El Intermediario Financiero del documento no opera Factoraje al Vencimiento, el documento no se puede seleccionar');
				record.data['SELECCION']='N';
				isSelect = false;
				selModel.deselectRow(rowIndex);
				return false;
		}else  {

			var mensaje =
					"Por el momento no se puede realizar el descuento de este documento con el Intermediario indicado en el mismo.\n\n"+
					"Por favor seleccione otro o comuniquese al Centro de Atencion a Clientes al telefono 50-89-61-07 o\n"+
					"del interior al 01-800-NAFINSA (01-800-6234672).";
			var msgFactorajeIf24hrs = record.data['LSTMSG24HRS'];//FODEA 016-2010 FVR
			var msgFactorajeEpo24hrs = objGral.msgOperEpoFact24hrs;
			var notascred = record.data['SELECNOTAS'];
			var aplicado = record.data['APLICADO'];
			var limiteActivo = record.data['LSTLIMACT'];
			var intermediario = record.data['LSTIF'];

			if (!panelTotales.isVisible()) {
				var recordMoneda = cboMoneda.findRecord(cboMoneda.valueField, cboMoneda.getValue());
				//return record ? record.get(comboAccion.displayField) : comboAccion.valueNotFoundText;
				panelTotales.setTitle('TOTALES - '+recordMoneda.get(cboMoneda.displayField));
				contenedorPrincipalCmp.add(panelTotales);
				contenedorPrincipalCmp.doLayout();
			}

			if(notascred!=''){
				selModel.selectRow(rowIndex,true);
				isSelect = true;
			}

			if (isSelect && aplicado=="N") { //true --> Suma
				var valorDocto = 0;
				var valorLimiteUtilizado = 0;
				var nuevoValorUtilizado = 0;
				var valorLimiteMaximo = 0;
				var validarlDisponible = "N";
				var valorDisponible = 0;
				var nuevoValorDisponible = 0;
				//FODEA 005-2009
				var valorMontoComprometido = 0;

				if(notascred ==''){
					for (var z = 0; z < objGral.intermediarios.length; z++) {
						if (parseInt(intermediario) == parseInt(objGral.intermediarios[z]) ) {
							valorDocto = roundOff(parseFloat(record.data['IMPORTREC']) ,2);
							valorLimiteUtilizado = roundOff(parseFloat(objGral.utilizados[z]),2);
							valorMontoComprometido = roundOff(parseFloat(objGral.montoComprometido[z]),2);//FODEA 005-2009 FVR
							nuevoValorUtilizado = roundOff(parseFloat(valorLimiteUtilizado) +parseFloat(valorMontoComprometido)+ parseFloat(valorDocto),2);
							valorLimiteMaximo = roundOff(parseFloat(objGral.maximos[z]),2);
							validarlDisponible = objGral.ifValidar[z];

							record.data['SELECHID'] = record.data['SELECHID'] + "|"+objGral.tipoLimite[z];

							if (parseFloat(nuevoValorUtilizado) > parseFloat(valorLimiteMaximo)) {
								//alert(mensaje);
								//objeto.checked = false;
								Ext.MessageBox.alert('Aviso',mensaje);
								record.data['SELECCION']='N';
								isSelect = false;
								selModel.deselectRow(rowIndex);
								return false;
							} else if (validarlDisponible == 'S') {
								valorDisponible = roundOff(parseFloat(objGral.lineasDisp[z]),2);
								nuevoValorDisponible = roundOff(parseFloat(valorDisponible) - parseFloat(valorDocto),2);
								if (nuevoValorDisponible < 0){
									//alert(mensaje);
									Ext.MessageBox.alert('Aviso',mensaje);
									record.data['SELECCION']='N';
									isSelect = false;
									selModel.deselectRow(rowIndex);
									return false;
								} else {
									objGral.lineasDisp[z] = nuevoValorDisponible;
								}
							}
							objGral.utilizados[z] = nuevoValorUtilizado;
						}
					}

					if(isSelect){
						objCalc.totalMonto 		= roundOff(parseFloat(objCalc.totalMonto) + parseFloat(record.data['MONTO']),2);
						objCalc.totalDescuento 	= roundOff(parseFloat(objCalc.totalDescuento) + parseFloat(record.data['MONTODESC']),2);
						objCalc.totalIntereses 	= roundOff(parseFloat(objCalc.totalIntereses) + parseFloat(record.data['IMPORTINT']),2);
						objCalc.totalRecibir 	= roundOff(parseFloat(objCalc.totalRecibir) + parseFloat(record.data['IMPORTREC']),2);
						objCalc.totaldocs 		= parseInt(objCalc.totaldocs) + 1;
					}
				}else{

					// TODO: PENDIENTE REVISAR ESTA PARTE.
					for (var z = 0; z < objGral.intermediarios.length; z++) {
						if (parseInt(intermediario) == parseInt(objGral.intermediarios[z]) ) {
							record.data['SELECHID'] = record.data['SELECHID'] + "|"+objGral.tipoLimite[z];
						}
					}
					objCalc.totalMonto 		= roundOff(parseFloat(objCalc.totalMonto) + parseFloat(record.data['MONTO']),2);
					objCalc.totalDescuento 	= roundOff(parseFloat(objCalc.totalDescuento) + parseFloat(record.data['MONTODESC']),2);
					objCalc.totalIntereses 	= roundOff(parseFloat(objCalc.totalIntereses) + parseFloat(record.data['IMPORTINT']),2);
					objCalc.totalRecibir 	= roundOff(parseFloat(objCalc.totalRecibir) + parseFloat(record.data['IMPORTREC']),2);
					objCalc.totaldocs 		= parseInt(objCalc.totaldocs) + 1;
				}

				if(notascred!=''){
					record.data['APLICADO']="S";
					aplicado=record.data['APLICADO'];
				}

			}else if(aplicado == 'N'){
				//alert("Resta");
				for (var z = 0; z < objGral.intermediarios.length; z++) {
					if (parseInt(intermediario) == parseInt(objGral.intermediarios[z]) ) {
						valorDocto = roundOff(parseFloat(record.data['IMPORTREC'])  ,2);
						valorLimiteUtilizado = roundOff(parseFloat(objGral.utilizados[z]),2);
						valorMontoComprometido = roundOff(parseFloat(objGral.montoComprometido[z]),2);//FODEA 005-2009 FVR
						nuevoValorUtilizado = roundOff(parseFloat(valorLimiteUtilizado)-parseFloat(valorMontoComprometido) - parseFloat(valorDocto),2);
						validarlDisponible = objGral.ifValidar[z];
						if ('|'+objGral.tipoLimite[z] == record.data['SELECHID'].substring((record.data['SELECHID'].length-2), (record.data['SELECHID'].length)) ){
							record.data['SELECHID'] = record.data['SELECHID'].substring(0, ((record.data['SELECHID'].length)-2));
						}
						if (validarlDisponible == 'S') {
							valorDisponible = roundOff(parseFloat(objGral.lineasDisp[z]),2);
							nuevoValorDisponible = roundOff(parseFloat(valorDisponible) + parseFloat(valorDocto),2);
							objGral.lineasDisp[z] = nuevoValorDisponible;
						}
						objGral.utilizados[z] = nuevoValorUtilizado;
					}
				}

				objCalc.totalMonto 		= roundOff(parseFloat(objCalc.totalMonto) - parseFloat(record.data['MONTO']),2);
				objCalc.totalDescuento 	= roundOff(parseFloat(objCalc.totalDescuento) - parseFloat(record.data['MONTODESC']),2);
				objCalc.totalIntereses 	= roundOff(parseFloat(objCalc.totalIntereses) - parseFloat(record.data['IMPORTINT']),2);
				objCalc.totalRecibir 	= roundOff(parseFloat(objCalc.totalRecibir) - parseFloat(record.data['IMPORTREC']),2);
				objCalc.totaldocs = parseInt(objCalc.totaldocs) - 1;
			}
			Ext.getCmp('totalDoctosDesp1').setValue(objCalc.totaldocs);
			Ext.getCmp('totalMontoDesp1').setValue('$'+formatoFlotante(objCalc.totalMonto,"aplicar"));
			Ext.getCmp('totalMontoDescDesp1').setValue('$'+formatoFlotante(objCalc.totalDescuento,"aplicar"));
			Ext.getCmp('totalImpIntDesp1').setValue('$'+formatoFlotante(objCalc.totalIntereses,"aplicar"));
			Ext.getCmp('totalImpRecibDesp1').setValue('$'+formatoFlotante(objCalc.totalRecibir,"aplicar"));
			return true;
		}
	}



	var terminar = function terminar() {
		var f = document.frmdescontar;

		if (objCalc.totalMonto == 0) {
			Ext.MessageBox.alert ('Aviso','No ha seleccionado documentos');
		} else {
			generaPreAcuse();
		}
	}

	//FUNCION: genera la pantalla de Preacuse manipulando los objetos involucrados en el pantalla
	// y generando el grid de preacuse para verificar que los documentos son los seleccionados
	var generaPreAcuse = function() {
		var store = grid.getStore();
		var columnModelGrid = grid.getColumnModel();
		var noRegistros = false;
		var numRegistro = -1;
		var registrosPreAcu = [];
		var registrosEnviar = [];

		//se recorre grid principal para detectar los documentos seleccionados
		store.each(function(record) {
			numRegistro = store.indexOf(record);
			if(record.data['SELECCION']=='S'){
				registrosPreAcu.push(record);
				registrosEnviar.push(record.data);
				noRegistros = true;
			}
		});


		//si existen doctos seleccionados, entonces se muestra el grid de preacuse
		if(noRegistros){

			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
			var gridColumnMod = gridPreAcu.getColumnModel();
			var strTipoUsuario = objGral.strTipoUsuario;

			grid.stopEditing();

			var ftotales = Ext.getCmp('panelTotales1');
			var objTotales = ftotales.getForm().getValues();
			var vcboEpo = Ext.getCmp('cboEpo1');
			var vrecord = vcboEpo.findRecord(vcboEpo.valueField, vcboEpo.getValue());
			
			var dataTotales = [
					[vrecord.get(vcboEpo.displayField), objTotales.totalMontoDesp, objTotales.totalMontoDescDesp, objTotales.totalImpIntDesp, objTotales.totalImpRecibDesp ]
				];


			//se le carga informacion al grid de preacuse y al grid de totales
			storePreAcuData.add(registrosPreAcu);
			storeTotalData.loadData(dataTotales);
			//Se incian validaciones para determinar el layout que se mostrara en el grid preacuse

			//se valida si el grid de preacuse ya es visible
			if (!gridPreAcu.isVisible()) {
				contenedorPrincipalCmp.findById('forma').hide();
				contenedorPrincipalCmp.findById('gridDoctos').hide();
				contenedorPrincipalCmp.findById('panelTotales1').hide();

				if(contenedorPrincipalCmp.findById('gridDoctosPreAcu')){
					contenedorPrincipalCmp.findById('gridDoctosPreAcu').show();
					contenedorPrincipalCmp.findById('gridTotales1').show();
					contenedorPrincipalCmp.findById('pnmsgLegal1').show();
				}else{
					contenedorPrincipalCmp.insert(4,gridPreAcu);
					contenedorPrincipalCmp.insert(5,gridTotales);
					contenedorPrincipalCmp.add(panelMsgLegal);
				}


				contenedorPrincipalCmp.doLayout();
			}

		}

	}//fin function generaPreAcuse

//-----------------------------HANDLERS-------------------------------------
	var procesarSuccessValoresIni = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			validaCder = resp.validaCder;
			objGral.inicial = 'N'
			objGral.strTipoUsuario = resp.strTipoUsuario;
			objGral.tipo_servicio = resp.tipo_servicio;
			objGral.urlCapturaDatos = resp.urlCapturaDatos;
			objGral.fecDiaSigHabil = resp.fecDiaSigHabil;
			objGral.msgDiaSigHabil = resp.msgDiaSigHabil;
			objGral.msgOperEpoFact24hrs = resp.msgOperEpoFact24hrs;
			objGral.msgFecVencPyme = resp.msgFecVencPyme;
			objGral.iNoEPO = resp.iNoEPO;

			if(resp.bloqueoPymeEpo !='B'){

				if(resp.pymeBloq !='S'){

					if(resp.msgError==''){

						var valorInicialCboEpo = Ext.getDom("hidCboEpo").value;
						var cboEpo = Ext.getCmp('cboEpo1');
						if(cboEpo.getValue()=='' && valorInicialCboEpo == ''){
							if (storeCatEpoData.findExact("clave", objGral.iNoEPO) != -1 ) {
								cboEpo.setValue(objGral.iNoEPO);
							}
							Inicializacion.catalogoEpo = true;
						} else if (valorInicialCboEpo != ''){
							if (storeCatEpoData.findExact("clave", valorInicialCboEpo) != -1 ) {
								cboEpo.setValue(valorInicialCboEpo);
							}
							Inicializacion.catalogoEpo = true;
							if(NE.util.allTrue(Inicializacion)) {
								//Si cuando llega aqui ya estan inicializados los valores iniciales necesarios,
								//realiza la consulta. Esta secci�n solo aplica cuando la consulta recibe parametros iniciales
								consultar();
							}
						}

						if(resp.msgError!=''){
							var objMsg = Ext.getCmp('mensajes1');
							objMsg.body.update(resp.msgError);
							objMsg.show();
							fp.hide();
						}else if(objGral.msgDiaSigHabil!='' || objGral.msgFecVencPyme!=''){
							var objMsg = Ext.getCmp('mensajes1');
							objMsg.body.update(objGral.msgDiaSigHabil+'<br>'+objGral.msgFecVencPyme);
							objMsg.show();
							fp.show();
							Ext.getCmp('cfFechaVenc').doLayout();
						}else{
							fp.show();
							Ext.getCmp('cfFechaVenc').doLayout();
						}
					}else{
						var objMsg = Ext.getCmp('mensajes1');

						objMsg.body.update(resp.msgError);
						objMsg.show();
						fp.hide();
					}
				}else  {
					var objMsg = Ext.getCmp('mensajes1');
					objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
									'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
									'Cd. De M�xico 50-89-61-07. <br>'+
									'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
					objMsg.show();
					fp.hide();

				}
			}else  {
				var objMsg = Ext.getCmp('mensajes1');
				objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
								'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
								'Cd. De M�xico 50-89-61-07. <br>'+
								'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
				objMsg.show();
				fp.hide();

			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	//FUNCION: asigna valores generales que se detrminan por la epo, if y moneda seleccionados,
	// esto sucede cada vez que se consulta o se hace un cambio en la seleccion del IF
	var procesarSuccessValoresGrales = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			objGral.tipo_servicio = resp.tipo_servicio;
			objGral.urlCapturaDatos = resp.urlCapturaDatos;
			objGral.fecDiaSigHabil = resp.fecDiaSigHabil;
			objGral.msgDiaSigHabil = resp.msgDiaSigHabil;
			objGral.msgOperEpoFact24hrs = resp.msgOperEpoFact24hrs;
			objGral.msgFecVencPyme = resp.msgFecVencPyme;
			objGral.iNoEPO = resp.iNoEPO;
			objGral.msgError = resp.msgError;

			objCalc.totalMonto = 0;
			objCalc.totalDescuento = 0;
			objCalc.totalIntereses = 0;
			objCalc.totalRecibir = 0;
			objCalc.totaldocs = 0;
			objGral.valorTC = resp.valorTC;
			//array's
			objGral.maximos = resp.maximos;
			objGral.utilizados = resp.utilizados;
			objGral.intermediarios = resp.intermediarios;
			objGral.ifValidar = resp.ifValidar;
			objGral.lineasDisp = resp.lineasDisp;
			objGral.tipoLimite = resp.tipoLimite;
			objGral.montoComprometido = resp.montoComprometido;

			objGral.sNombrePyme = resp.sNombrePyme;
			objGral.operaNotasDeCredito = resp.operaNotasDeCredito;
			objGral.aplicarNotasDeCreditoAVariosDoctos = resp.aplicarNotasDeCreditoAVariosDoctos;
			objGral.shownotas = resp.shownotas;


			if(objGral.msgDiaSigHabil=='' && objGral.msgFecVencPyme==''){
				var objMsg = Ext.getCmp('mensajes1');
				objMsg.hide();
			}else{
				var objMsg = Ext.getCmp('mensajes1');
				objMsg.body.update(objGral.msgDiaSigHabil+'<br>'+objGral.msgFecVencPyme);
				objMsg.show();
			}
			if(objGral.operaNotasDeCredito || objGral.aplicarNotasDeCreditoAVariosDoctos){
				objGral.icDoctosNotas = resp.icDoctosNotas;
				objGral.icNotasAp = resp.icNotasAp;
				if (!Ext.isEmpty(objGral.icDoctosNotas) || !Ext.isEmpty(objGral.icNotasAp)){
					Ext.getCmp('fpNotas').show();
				}
			}
			storeDoctosData.loadData(resp);

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	//FUNCION: despues de cargar la informacion del grid, realiza la vaidacion en
	//base a la parametrizacion y tipo de usuario, para determinar los campos a mostrar
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		pnl.el.unmask();

		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		var resp = store.reader.jsonData;

		if(resp.bloqueoPymeEpo !='B'){
			if(resp.pymeBloq !='S'){
				grid.show();
				if (arrRegistros != null) {
					if (!grid.isVisible()) {
						contenedorPrincipalCmp.add(grid);
						contenedorPrincipalCmp.doLayout();
					}else{
						selectDefaultDoctos(grid);
					}

					var el = grid.getGridEl();
					if(store.getTotalCount() > 0) {
						el.unmask();


						if(strUsuario=='NAFIN'){

							var cboEPO = Ext.getCmp('cboEpo1');
							var recordEPO = cboEPO.findRecord(cboEPO.valueField, cboEPO.getValue());
							var nombreEpo = recordEPO.get(cboEPO.displayField);
							grid.setTitle('<p align="center">'+nombreEpo+'<br>'+Ext.getDom("strNombrePymeAsigna").value+'</p>');
						}

						var existeRegSH = false;
						store.each(function(record) {
							if(record.data['LSTTIPOSERV']=='SH'){
								existeRegSH = true;
							}
						});

						if(existeRegSH){
							Ext.getCmp('msg24HrsDisplay').setValue('* Los documentos en verde seran programados para el siguiente d�a h�bil '+objGral.fecDiaSigHabil);
						}

						Ext.getCmp('btnTerminar').enable();
					}else {
						Ext.getCmp('msg24HrsDisplay').setValue('');
						Ext.getCmp('btnTerminar').disable();
						if(objGral.msgError!=''){
							el.mask(objGral.msgError, 'x-mask');
						}else{
							el.mask('No se encontr� ning�n registro', 'x-mask');
						}
					}
				}
			}else  {
				var objMsg = Ext.getCmp('mensajes1');
				objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
									'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
									'Cd. De M�xico 50-89-61-07. <br>'+
									'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
				objMsg.show();
				fp.hide();

			}
		}else  {
			var objMsg = Ext.getCmp('mensajes1');
			objMsg.body.update('Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado.<br>'+
								'Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes '+
								'Cd. De M�xico 50-89-61-07. <br>'+
								'Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)');
			objMsg.show();
			fp.hide();

		}
	}

	//FUNCION:retorna de la pabtalla de Pre Acuse a la parte de seleccion de documentos
	//para poder verificar la seleccion que se realizo
	var regresaAseleccion = function(){
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		contenedorPrincipalCmp.findById('forma').show();
		contenedorPrincipalCmp.findById('gridDoctos').show();
		contenedorPrincipalCmp.findById('panelTotales1').show();

		contenedorPrincipalCmp.findById('gridDoctosPreAcu').hide();
		contenedorPrincipalCmp.findById('gridTotales1').hide();
		contenedorPrincipalCmp.findById('gridDoctosPreAcu').getStore().removeAll(true);
		contenedorPrincipalCmp.findById('gridTotales1').getStore().removeAll(true);

		contenedorPrincipalCmp.doLayout();
	}

	var fnTransmitirCallback = function(vpkcs7, vtextoFirmar, vokResp, vbtn){
		if (Ext.isEmpty(vpkcs7)) {
			vbtn.enable();
			return;	//Error en la firma. Termina...
		}else{
			console.log("wiiiii");
			realizaConfirmacionNAFIN(vokResp, vpkcs7, vtextoFirmar);
		}
	}

	var transmitir = function(btn, csCertificado){
		var ventana = Ext.getCmp('winCesionDerechos');

		if((strUsuario=='PYME' || strUsuario=='NAFIN') && csCertificado!=true && validaCder){
		//if(strUsuario=='PYME'){
			var win = new NE.cesion.WinCesionDerechos({
				getResultValid:resulValidCesion,
				cvePerfProt: 'PPYMAUTOR',
				cveFacultad: '13PYME13AUTORIZA',
				strUsuario: strUsr,
				tipoUsuario: strUsuario
			}).show();
			win.el.dom.scrollIntoView();
		}else{
			btn.disable();
			var cboEPO = Ext.getCmp('cboEpo1');
			var recordEPO = cboEPO.findRecord(cboEPO.valueField, cboEPO.getValue());
			var valor = recordEPO.get(cboEPO.displayField);

			var textoFirmar = Ext.getDom("strNombrePymeAsigna").value;
			storeTotalData.each(function(record) {
				textoFirmar += '\nE P O  -  DATOS: '+valor;
				textoFirmar += '\nTotal Monto Documento: $'+record.data['TOTALMONTO'];
				textoFirmar += '\nTotal Monto Descuento: $'+record.data['TOTALDESCTO'];
				textoFirmar += '\nTotal Monto de Interes: $'+record.data['TOTALINTERES'];
				textoFirmar += '\nTotal Importe a Recibir: $'+record.data['TOTALIMPORT'];
			});


			textoFirmar += "Al solicitar el factoraje electr�nico o descuento electr�nico del documento\n"+
			"que selecciono e identifico, transmito los derechos que sobre el mismo ejerzo,\n"+
			"de acuerdo con lo establecido por los art�culos 427 de la ley general de t�tulos\n"+
			"y operaciones de cr�dito, 32 c del c�digo fiscal de la federaci�n y 2038 del c�digo\n"+
			"civil federal, haci�ndome sabedor de su contenido y alcance, condicionado a que se\n"+
			"efect�e el descuento electr�nico o el factoraje electr�nico.\n\n";

			NE.util.obtenerPKCS7(fnTransmitirCallback, textoFirmar, 'S', btn);

		}
	}

	var procesarSuccessConfirmaCesion = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');

			if(resp.msgError=='' && resp.esCveCorrecta){

				var acuseCifras = [
					['N�mero de Acuse', resp.objCifras.numAcuse],
					['Fecha de Carga', resp.objCifras.fecCarga],
					['Hora de Carga', resp.objCifras.horaCarga],
					['Usuario de Captura', resp.objCifras.captUser]
				];

				storeCifrasData.loadData(acuseCifras);

				//se muestran componentes para el acuse de la seleccion de doctos
				if(strUsuario=='NAFIN'){
					panelMsgAcuse.html='<p align="center"><b>La autentificaci&oacute;n se llev&oacute; a cabo con &eacute;xito<br>Recibo: '+resp._acuse+'</b></p>';
					contenedorPrincipalCmp.insert(1,panelMsgAcuse);
				}
				contenedorPrincipalCmp.findById('gridDoctosPreAcu').setTitle('Acuse - Selecci�n Documentos');
				contenedorPrincipalCmp.remove(panelMsgLegal);
				contenedorPrincipalCmp.insert(2,gridCifrasCtrl);
				contenedorPrincipalCmp.insert(3, panelMsgCifrado);
				contenedorPrincipalCmp.insert(4, NE.util.getEspaciador(10));
				contenedorPrincipalCmp.doLayout();
				//se muestran botones para salir o generar acuse en pdf
				Ext.getCmp('btnRevisar').hide();
				//Ext.getCmp('btnGenerarPDF').show();
				Ext.getCmp('btnAbrirPDF').show();
				Ext.getCmp('btnTransmitir').hide()
				Ext.getCmp('btnSalir').show();
				Ext.getCmp('btnCancelar').hide();

				Ext.getCmp('btnAbrirPDF').setHandler( function(boton, evento) {
					var forma = Ext.getDom('formAux');
					forma.action = resp.urlArchivo;
					forma.submit();
				});
			}

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var consultar = function(boton, evento) {
		pnl.el.mask('Consultando...', 'x-mask-loading');
		grid.hide();
		Ext.getCmp('fpNotas').hide();
		var cboEpo = Ext.getCmp('cboEpo1');
		var txtFechaVencDe = Ext.getCmp('txtFechaVencDe');
		var txtFechaVenca = Ext.getCmp('txtFechaVenca');
		var record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
		var valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);

		objGral.valEPO = valEPO;

		Ext.Ajax.request({
			url: '13forma2ext.data.jsp',
			params: Ext.apply(fp.getForm().getValues(),{
				informacion: 'ConsultarGenera',
				cboEpo: objGral.valEPO,
				cvePyme: Ext.getDom('cvePyme').value
			}),
			callback: procesarSuccessValoresGrales
		});
	}

	var procesarNotaSimple = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var elG = gridNotaSimple.getGridEl();
			if(store.getTotalCount() > 0) {
				elG.unmask();
			}else{
				elG.mask('No encontro ning�n cambio para este documento');
			}
		}
	}

	var procesarNotaVarias = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var elG = gridNotaVarias.getGridEl();
			if(store.getTotalCount() > 0) {
				elG.unmask();
			}else{
				elG.mask('No encontro ning�n registro');
			}
		}
	}

	var procesarGenerarPdfNotas =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfNotas');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfNotas');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//-----------------------------STORES-------------------------------------

	var gridTotalNotasData = new Ext.data.JsonStore({
		fields:	[{name: 'TOTAL_MONTO',type: 'float'},{name: 'SALDO_DOCTO',type: 'float'}],
		data:		[{'TOTAL_MONTO':0,'SALDO_DOCTO':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	// define a custom summary function
    Ext.ux.grid.GroupSummary.Calculations['totalSaldo'] =  function(value, record, field){
					var doctoAplicado = record.data.DOCTOAPLICADOMONTO
					if (doctoAnt != doctoAplicado){
						totalSaldo = 0;
					}
					totalSaldo += record.data.MONTONOTA;
					doctoAnt = doctoAplicado;
					return record.data.MONTODOCTO-totalSaldo;
    };

	var summary = new Ext.ux.grid.GroupSummary();

	var gridNotaSimpleData = new Ext.data.GroupingStore({
		root : 'registros',
		url : '13forma1ext.data.jsp',
		baseParams: {
			informacion: 'obtenNotaSimple'
		},
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields:	[{name: 'NUMNOTA'},
					{name: 'FECHAEMISION',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'MONEDA'},
					{name: 'DOCTOAPLICADO'},
					{name: 'DOCTOAPLICADOMONTO'},
					{name: 'MONTONOTA',	type: 'float'},
					{name: 'MONTODOCTO',type: 'float'},
					{name: 'SALDO',	type: 'float'},
					{name: 'TIPOFACTORAJE'}
					]
		}),
		groupField: 'DOCTOAPLICADOMONTO',	//sortInfo:{field: 'DOCTOAPLICADOMONTO', direction: "DESC"},
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
				Ext.apply(options.params, {icDocto:objGral.icDoctosNotas,icNotas: objGral.icNotasAp});
				}
			},
			load: procesarNotaSimple,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarNotaSimple(null, null, null);
				}
			}
		}
	});

	var gridNotaVariasData = new Ext.data.GroupingStore({
		root : 'registros',
		url : '13forma1ext.data.jsp',
		baseParams: {
			informacion: 'obtenNotaVarias'
		},
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields: [
			{name: 'NOTA_MONTO_CONCAT'},
			{name: 'IG_NUMERO_DOCTO_CREDITO'},
			{name: 'IG_NUMERO_DOCTO_DOCTO'},
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_MONTO',type: 'float'},
			{name: 'MONEDA'},
			{name: 'MONTO_DOCTO',type: 'float'},
			{name: 'MONTO_APLICA',	type: 'float'},
			{name: 'SALDO_DOCTO',	type: 'float'},
			{name: 'TIPO_FACTORAJE'}
      ]
		}),
		groupField: 'NOTA_MONTO_CONCAT',	//sortInfo:{field: 'NOTA_MONTO_CONCAT', direction: "ASC"},
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			load: procesarNotaVarias,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarNotaVarias(null, null, null);
				}
			}
		}
	});

	var storeCatEpoData = new Ext.data.JsonStore({
		id: 'catalogoEpoStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma2ext.data.jsp',
		baseParams: {
			informacion: 'catalogoEpo'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store,records, oprion){
					if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
						if(objGral.inicial=='S'){
							iniRequest(store);
						}
					}
			}
		}
	});

	var storeCatMonedaData = new Ext.data.JsonStore({
		id: 'catalogoMonedaStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13forma2ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo,
			load:function(store, records, oprion){
				if(store.getTotalCount()>0 &&  Ext.isEmpty(records[0].data.loadMsg)){
					var valorInicialCboMoneda = Ext.getDom("hidCboMoneda").value;
					var cboMoneda = Ext.getCmp('cboMoneda1');
					if(cboMoneda.getValue()=='' && valorInicialCboMoneda == ''){
						cboMoneda.setValue(records[0].data['clave']);
						Inicializacion.catalogoMoneda = true;
					} else if (valorInicialCboMoneda != '') {
						cboMoneda.setValue(valorInicialCboMoneda);
						Inicializacion.catalogoMoneda = true;
						if(NE.util.allTrue(Inicializacion)) {
							//Si cuando llega aqui ya estan inicializados los valores iniciales necesarios,
							//realiza la consulta. Esta secci�n solo aplica cuando la consulta recibe parametros iniciales
							consultar();
						}
					}
				}
			}
		}
	});

	var storeDoctosData = new Ext.data.JsonStore({
		root : 'registros',
		fields: [
			{name: 'NUMDOCTO'},
			{name: 'FECEMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECVENC',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORCDESC', type: 'float'},
			{name: 'MONTODESC', type: 'float'},
			{name: 'SELECCION'},  //--   aqui va el check de seleccion
			{name: 'IMPORTINT', type: 'float'},
			{name: 'IMPORTREC', type: 'float'},
			{name: 'NOMBREIF'},
			{name: 'BANCODEP'},
			{name: 'SUCURSAL'},
			{name: 'CUENTADEP'},
			{name: 'SELECBOOL'},
			{name: 'SELECHID'},
			{name: 'SELECNOTAS'},
			{name: 'APLICADO'},
			{name: 'LSTIF'},
			{name: 'LSTLIMACT'},
			{name: 'LSTMSG24HRS'},
			{name: 'LSTTIPOSERV'},
			{name: 'LSTMSGDIASIG'},
			{name: 'FAC_VENCIDO'},
			{name: 'BLO_PYME_EPO_IF'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
	});

	var storePreAcuData = new Ext.data.ArrayStore({
		fields: [
			{name: 'NUMDOCTO'},
			{name: 'FECEMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECVENC',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'PLAZO'},
			{name: 'MONEDA'},
			{name: 'MONTO'},
			{name: 'PORCDESC', type: 'float'},
			{name: 'MONTODESC', type: 'float'},
			{name: 'SELECCION'},  //--   aqui va el check de seleccion
			{name: 'IMPORTINT', type: 'float'},
			{name: 'IMPORTREC', type: 'float'},
			{name: 'NOMBREIF'},
			{name: 'BANCODEP'},
			{name: 'SUCURSAL'},
			{name: 'CUENTADEP'},
			{name: 'SELECBOOL'},
			{name: 'SELECHID'},
			{name: 'SELECNOTAS'},
			{name: 'APLICADO'},
			{name: 'LSTIF'},
			{name: 'LSTLIMACT'},
			{name: 'LSTMSG24HRS'},
			{name: 'LSTTIPOSERV'},
			{name: 'LSTMSGDIASIG'}
		]
	});

	var storeTotalData = new Ext.data.ArrayStore({
	  fields: [
		  {name: 'EPO'},
		  {name: 'TOTALMONTO', type: 'float'},
		  {name: 'TOTALDESCTO', type: 'float'},
		  {name: 'TOTALINTERES', type: 'float'},
		  {name: 'TOTALIMPORT', type: 'float'}
	  ]
	});

	var storeCifrasData = new Ext.data.ArrayStore({
	  fields: [
		  {name: 'etiqueta'},
		  {name: 'informacion'}
	  ]
	});

//-----------------------------COMPONENTES-------------------------------------

	var gridTotalNotas = {
		xtype: 'grid',
		store: gridTotalNotasData,
		id: 'gridTotalNotas',
		hidden:	false,
		columns: [
			{header: 'Monto Total de Notas', tooltip:'Monto Total de Notas',dataIndex: 'TOTAL_MONTO',width: 325,	align: 'right',renderer: Ext.util.Format.numberRenderer('$ 0,0.00')},
			{header: 'Saldo del documento',tooltip:'Saldo del documento',dataIndex: 'SALDO_DOCTO',width: 325,	align: 'right',renderer: Ext.util.Format.numberRenderer('$ 0,0.00')}
		],
		view: new Ext.grid.GridView({forceFit:true,markDirty: false}),
		columnLines:true,
		width: 655,
		height: 50,
		//title: 'Totales',
		frame: false
	};

	// define a custom summary function
    Ext.ux.grid.GroupSummary.Calculations['totalSaldo'] =  function(value, record, field){
					var doctoAplicado = record.data.DOCTOAPLICADOMONTO
					if (doctoAnt != doctoAplicado){
						totalSaldo = 0;
					}
					totalSaldo += record.data.MONTONOTA;
					doctoAnt = doctoAplicado;
					return record.data.MONTODOCTO-totalSaldo;
    };

	var summary = new Ext.ux.grid.GroupSummary();

	var gridNotaSimple = new Ext.grid.GridPanel({
		id: 'gridNotaSimple',
		store: gridNotaSimpleData,
		columns: [
			{	header: 'No. Docto. Aplicado',tooltip: 'No. Docto. Aplicado',	dataIndex: 'DOCTOAPLICADOMONTO',	width: 150,	align: 'left', hidden: true, 	hideable: false},
			{	header: 'No. Nota',tooltip: 'No. Nota',	dataIndex: 'NUMNOTA',	width: 130,	align: 'left',
				summaryRenderer: function(value, summaryData, dataIndex) {
					 return 'Saldo del documento';
				}
			},
			{	header: 'Fecha de Emisi�n',tooltip: 'Fecha de Emisi�n',	dataIndex: 'FECHAEMISION',	sortable : true, width : 120, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'),
				summaryType: 'totalSaldo',
				summaryRenderer: Ext.util.Format.numberRenderer('$ 0,0.00')
			},
			{	header: 'Moneda',tooltip: 'Moneda',	dataIndex: 'MONEDA',	width: 140,	align: 'left',
				summaryRenderer: function(value, summaryData, dataIndex) {
					 return 'Monto Total de Notas';
				}
			},
			{	header: 'Monto',tooltip: 'Monto',	dataIndex: 'MONTONOTA',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$ 0,0.00'),
				summaryType: 'sum',
				summaryRenderer: function(value, summaryData, dataIndex) {
					 return Ext.util.Format.number(value,'$ 0,0.00');
				}
			},
			{	header: 'Tipo Factoraje',tooltip: 'Tipo Factoraje',	dataIndex: 'TIPOFACTORAJE',	width: 150,	align: 'left'}
		],
		stripeRows: true,
		view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),
		plugins: summary,
		deferRowRender: false,
		columnLines:true,
		loadMask: true,
		monitorResize: true,
		height: 280,
		width: 655,
		title: '',
		frame: false,
		hidden: false
	});

	var gridNotaVarias = new Ext.grid.GridPanel({
		id: 'gridNotaVarias',
		store: gridNotaVariasData,
		columns: [
			{header: 'No. Nota',tooltip: 'No. Nota',	dataIndex: 'NOTA_MONTO_CONCAT',	width: 250,	align: 'left', hidden:true, hideable:false},
			{header: 'No. Docto.',tooltip: 'No. Docto.',	dataIndex: 'IG_NUMERO_DOCTO_DOCTO',	width: 120,	align: 'left'},
			{header: 'Fecha de Emisi�n',tooltip: 'Fecha de Emisi�n',	dataIndex: 'FECHA_EMISION',	sortable : true, width : 120, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Moneda',tooltip: 'Moneda',	dataIndex: 'MONEDA',	width: 140,	align: 'left'},
			{header: 'Monto Docto',tooltip: 'Monto Docto',	dataIndex: 'MONTO_DOCTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto Aplicado',tooltip: 'Monto Aplicado',	dataIndex: 'MONTO_APLICA',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Saldo Docto',tooltip: 'Saldo Docto',	dataIndex: 'SALDO_DOCTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Tipo Factoraje',tooltip: 'Tipo Factoraje',	dataIndex: 'TIPO_FACTORAJE',	width: 140,	align: 'left'}
		],
		stripeRows: true,
		view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),
		deferRowRender: false,
		loadMask: true,
		monitorResize: true,
		height: 300,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});

	var elementosForma = [
		{
			xtype: 'displayfield',
			fieldLabel: 'No. Nafin El�ctronico Pyme',
			value: strUsuario=='NAFIN'?(Ext.getDom("strNePymeAsigna").value+' '+Ext.getDom("strNombrePymeAsigna").value):'',
			hidden: strUsuario=='NAFIN'?false:true
		},
		{
			xtype: 'combo',
			name: 'cboEpo',
			id: 'cboEpo1',
			fieldLabel: 'EPO',
			mode: 'local',
			autoLoad: false,
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboEpo',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatEpoData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'combo',
			name: 'cboMoneda',
			id: 'cboMoneda1',
			fieldLabel: 'Moneda',
			mode: 'local',
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboMoneda',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			id: 'cfFechaVenc',
			fieldLabel: 'Fecha Vencimiento de',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'txtFechaVencDe',
					id: 'txtFechaVencDe',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoFinFecha: 'txtFechaVenca',
					margins: '0 20 0 0',  //necesario para mostrar el icono de error
					value: Ext.getDom('hidTxtFechaVencDe').value
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'txtFechaVenca',
					id: 'txtFechaVenca',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'txtFechaVencDe',
					margins: '0 20 0 0',  //necesario para mostrar el icono de error
					value: Ext.getDom('hidTxtFechaVenca').value
				},
				{
					xtype: 'displayfield',
					value: 'dd/mm/aaaa',
					width: 50
				}
			]
		},{
			xtype: 'panel',
			id:	'fpNotas',
			hidden: true,
			layout:  'form',
			buttonAlign: 'center',
			html: '<div><center><b>Se&nbsp;aplicaran&nbsp;Notas&nbsp;de&nbsp;Cr&eacute;dito&nbsp;a&nbsp;sus&nbsp;documentos,&nbsp;para</br>'+
					'&nbsp;consultarlos&nbsp;presione&nbsp;el&nbsp;boton</b></br></div>',
			buttons: [
				{
					id: 'btnNotas',
					tooltip: 'Notas de Cr�dito',
					iconCls: 'icoBuscar',
					text: 'Notas de Cr�dito',
					handler: function() {
						if(objGral.operaNotasDeCredito && !objGral.aplicarNotasDeCreditoAVariosDoctos){
							doctoAnt = "";
							totalSaldo = 0;
							var ventana = Ext.getCmp('verNotas');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									modal: true,
									resizable: false,
									layout: 'form',
									x: 300,
									width: 667,
									height: 310,
									id: 'verNotas',
									closeAction: 'hide',
									items: [gridNotaSimple],
									title: 'Cr�ditos'
								}).show();
							}
							gridNotaSimpleData.load();

						}else if(objGral.operaNotasDeCredito && objGral.aplicarNotasDeCreditoAVariosDoctos){

							var ventana = Ext.getCmp('winNotas');
							var btnPdfNotas = Ext.getCmp('btnPdfNotas');
							var btnBajarPdfNotas = Ext.getCmp('btnBajarPdfNotas');
							if (ventana) {
								btnPdfNotas.enable();
								btnBajarPdfNotas.hide();
								ventana.show();
							}else{
								new Ext.Window({
									modal: true,
									resizable: false,
									x: 200,
									width: 800,
									height: 360,
									id: 'winNotas',
									closeAction: 'hide',
									items: [gridNotaVarias],
									title: 'Cr�ditos',
									bbar: {
										xtype: 'toolbar',
										buttons: ['->','-',{xtype: 'button',text: 'Generar PDF',	id: 'btnPdfNotas'},{xtype: 'button',text: 'Bajar PDF',id: 'btnBajarPdfNotas',hidden: true}]
									}
								}).show();
							}

							var btnGenerar = Ext.getCmp('btnPdfNotas');
							btnGenerar.setHandler(
								function(boton, evento) {
								boton.disable();
								boton.setIconClass('loading-indicator');
									Ext.Ajax.request({
										url: '/nafin/13descuento/13pyme/13consulta02ext_not_pdf.jsp',
										callback: procesarGenerarPdfNotas
									});
								}
							);
							gridNotaVariasData.load();
						}
					}
				}
			]
		}
		/*{//PENDIENTE: posible link para ver documentos negociables
			xtype: 'panel',
			//title: 'test',
			hidden: true,
			html: '<a href="#">Aqui</a>',
			flex: 1
		}*/
	]

	var selectModel = new Ext.grid.CheckboxSelectionModel({
	  checkOnly: true,
	  renderer: function(v, p, record){
				if (record.data['SELECBOOL']){
					return '<div class="x-grid3-row-checker">&#160;</div>';
				}else{
					return '<div>&#160;</div>';
				}
		},
	  listeners: {
			rowselect: function(selectModel, rowIndex, record) {
				 if(record.data['SELECCION']!='S')
					if(recalcular(selectModel, record, rowIndex, true))
						record.data['SELECCION']='S';
				 //Ext.MessageBox.alert('Totales Calculados', objCalc.totalMont+'--'+objCalc.totalDescuento+'--'+objCalc.totalIntereses+'--'+objCalc.totalRecibir+'--'+	objCalc.totaldocs);
			},
			rowdeselect: function(selectModel, rowIndex, record) {
				if(record.data['SELECBOOL'] && record.data['SELECNOTAS']!='' && record.data['SELECCION']=='S'){
					selectModel.selectRow(rowIndex,true);
				}else{
				 if(record.data['SELECCION']!='N')
					if(recalcular(selectModel, record, rowIndex, false))
						record.data['SELECCION']='N';
				}
			},
			beforerowselect: function( selectModel, rowIndex, keepExisting, record ){
				var mensaje = '';
				if(record.data['SELECBOOL']){

					if(record.data['LSTLIMACT']=='N')	{
						mensaje  =
						'Por el momento no se puede realizar el descuento de este documento con el Intermediario indicado en el mismo.'+
						'Por favor seleccione otro o comuniquese al Centro de Atencion a Clientes al telefono 50-89-61-07 o '+
						'del interior al 01-800-NAFINSA (01-800-6234672).';

					}else if(record.data['LSTTIPOSERV']=='ERROR')	{
						mensaje  = 'Fuera del horario de servicio para el Intermediario Financiero del documento seleccionado.';
					}else if(record.data['LSTTIPOSERV']=='SH'){
						if(record.data['LSTMSG24HRS']!=''){
							mensaje = record.data['LSTMSG24HRS'];
						}
					}

					if(record.data['BLO_PYME_EPO_IF']=='S')	{
						mensaje ="Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado- IF. Para cualquier aclaraci�n o duda, favor de comunicarse al Centro de Atenci�n a Clientes Cd. De M�xico 50-89-61-07. Sin costo desde el interior de la Rep�blica 01-800-NAFINSA (01-800-623-4672)";
					}

				}else{
					mensaje  = 'No es posible seleccionar el documento';
				}

				if(mensaje!=''){
					if(mensaje=='DS'){
						Ext.MessageBox.alert('Aviso',record.data['LSTMSGDIASIG']);
					}else{
						Ext.MessageBox.alert('Aviso',mensaje);
						return false;
					}
				}
			}
		}
	});

	var panelMsgAcuse = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'pnmsgAcuse',
		id: 'pnmsgAcuse1',
		width: 400,
		style: 'margin:0 auto;',
		frame: true
	});

	var panelMsgLegal = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'pnmsgLegal',
		id: 'pnmsgLegal1',
		width: 900,
		style: 'margin:0 auto;',
		frame: true,
		html:'Al solicitar el factoraje electr&oacute;nico o descuento electr&oacute;nico del documento que selecciono e identifico, '+
				'transmito los derechos que sobre el mismo ejerzo, de acuerdo con lo establecido por los art&iacute;culos 427 de la Ley General de T&iacute;tulos y Operaciones de Cr&eacute;dito, '+
				'32 c del C&oacute;digo Fiscal de la Federaci&oacute;n y 2038 del C&oacute;digo Civil Federal, haci&eacute;ndome sabedor de su contenido y alcance, condicionado a que se efect&uacute;e '+
				'el descuento electr&oacute;nico o el factoraje electr&oacute;nico.<br/>'+
                'Asimismo, en este acto manifiesto, bajo protesta de decir verdad, que s&iacute; he emitido o emitir&eacute; a la Empresa de Primer Orden el CFDI '+
                'por la operaci&oacute;n comercial que le dio origen a esta transacci&oacute;n, seg&uacuten sea el caso y conforme se establezcan las disposiciones fiscales vigentes.'
	});

	var panelMsgCifrado = new Ext.Panel({//FUNCION: Panel para mostrar avisos
		name: 'panelMsgCifrado',
		id: 'panelMsgCifrado1',
		width: 900,
		style: 'margin:0 auto;',
		frame: true,
		html:'Al solicitar el factoraje electr&oacute;nico o descuento electr&oacute;nico del documento que selecciono e identifico, '+
				'transmito los derechos que sobre el mismo ejerzo, de acuerdo con lo establecido por los art&iacute;culos 427 de la ley general de t&iacute;tulos y operaciones de cr&eacute;dito, '+
				'32 c del c&oacute;digo fiscal de la federaci&oacute;n y 2038 del c&oacute;digo civil federal, haci&eacute;ndome sabedor de su contenido y alcance, condicionado a que se efect&uacute;e '+
				'el descuento electr&oacute;nico o el factoraje electr&oacute;nico.<br/>'+
                'Asimismo, en este acto manifiesto, bajo protesta de decir verdad, que s&iacute; he emitido o emitir&eacute; a la Empresa de Primer Orden el CFDI '+
                'por la operaci&oacute;n comercial que le dio origen a esta transacci&oacute;n, seg&uacuten sea el caso y conforme se establezcan las disposiciones fiscales vigentes.'

	});

//------------------------CONTENEDORES--------------------------------------

	var gridView = new Ext.grid.GridView({
		//forceFit: true, //Comentar si no.
		getRowClass : function (row, index) {
			var cls ='';
			var data = row.data.LSTTIPOSERV; //Color es el campo que contiene la simbologia para la leyenda
			if(data == 'SH'){
				return cls = 'greenrow'
			}
		}
	});


	var grid = new Ext.grid.EditorGridPanel({
	id: 'gridDoctos',
	store: storeDoctosData,
	margins: '20 0 0 0',
	clicksToEdit: 1,
	view: gridView,
	viewConfig: {
      templates: {
         cell: new Ext.Template(
            '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
            '<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
            '</td>'
         )
      }
   },
	sm: selectModel,
	columns: [
		selectModel,
		{//1
			header: 'N�mero Documento',
			tooltip: 'N�mero Documento',
			dataIndex: 'NUMDOCTO',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false
		},
		{//2
			header: 'Fecha de Emisi�n',
			tooltip: 'Fecha de Emisi�n',
			dataIndex: 'FECEMISION',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//3
			header: 'Fecha de Vencimiento',
			tooltip: 'Fecha de Vencimiento',
			dataIndex: 'FECVENC',
			sortable: true,
			hideable: false,
			width: 100,
			align: 'left',
			renderer: Ext.util.Format.dateRenderer('d/m/Y')
		},
		{//4
			header : 'Plazo',
			tooltip: 'Plazo',
			dataIndex : 'PLAZO',
			sortable : true,
			width : 100,
			align: 'right'
		},
		{//6
			header : 'Moneda',
			tooltip: 'Moneda',
			dataIndex : 'MONEDA',
			width : 150,
			sortable : true
		},
		{//7
			header : 'Monto',
			tooltip: 'Monto',
			dataIndex : 'MONTO',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//8
			header : 'Porcentaje de Descuento',
			tooltip: 'Porcentaje de Descuento',
			dataIndex : 'PORCDESC',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.numberRenderer('%0,0.00')
		},
		{//9
			header : 'Monto a Descontar',
			tooltip: 'Monto a Descontar',
			dataIndex : 'MONTODESC',
			sortable : true,
			width : 100,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//11
			header : 'Importe de Intereses',
			tooltip: 'Importe de Intereses',
			dataIndex : 'IMPORTINT',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//12
			header : 'Importe a Recibir',
			tooltip: 'Importe a Recibir',
			dataIndex : 'IMPORTREC',
			width : 150,
			sortable : true,
			renderer: Ext.util.Format.numberRenderer('$0,0.00')
		},
		{//13
			header : 'Nombre del IF',
			tooltip: 'Nombre del IF',
			dataIndex : 'NOMBREIF',
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//14
			header : 'Banco Dep�sito',
			tooltip: 'Banco Dep�sito',
			dataIndex : 'BANCODEP',
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//15
			header : 'Sucursal',
			tooltip: 'Sucursal',
			dataIndex : 'SUCURSAL',
			sortable : true,
			width : 100,
			align: 'center'
		},
		{//16
			header : 'Cuenta Dep�sito',
			tooltip: 'Cuenta de Dep�sito',
			dataIndex : 'CUENTADEP',
			sortable : true,
			width : 100,
			align: 'center'
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	height: 400,
	width: 900,
	style: 'margin:0 auto;',
	title:  ' ',
	frame: true,
	listeners: {
		viewReady: selectDefaultDoctos
	},
	bbar: {
		xtype: 'toolbar',
		items: [
			{
				xtype:'displayfield',
				id: 'msg24HrsDisplay',
				value: ' '
			},
			'->',
			'-',
			{
			text: 'Terminar',
			id: 'btnTerminar',
			handler: terminar
			}
		]
	}
	});

	var gridPreAcu = new Ext.grid.GridPanel({
		id: 'gridDoctosPreAcu',
		store: storePreAcuData,
		margins: '20 0 0 0',
		view: gridView,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{//1
			header: 'IF Seleccionado',
			tooltip: 'IF Seleccionado',
			dataIndex: 'NOMBREIF',
			sortable: true,
			width: 150,
			resizable: true,
			hidden: false,
			renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			},
			{//14
				header : 'Banco Dep�sito',
				tooltip: 'Banco Dep�sito',
				dataIndex : 'BANCODEP',
				sortable : true,
				width : 100,
				align: 'center',
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{//15
				header : 'Sucursal',
				tooltip: 'Sucursal',
				dataIndex : 'SUCURSAL',
				sortable : true,
				width : 100,
				align: 'center',
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			},
			{//16
				header : 'Cuenta Dep�sito',
				tooltip: 'Cuenta de Dep�sito',
				dataIndex : 'CUENTADEP',
				sortable : true,
				width : 100,
				align: 'center',
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			},
			{//2
				header: 'EPO',
				tooltip: 'EPO',
				dataIndex: 'EPOSELECT',
				sortable: true,
				hideable: false,
				width: 100,
				align: 'left',
				renderer:  function (causa, columna, registro){
						var cboEPO = Ext.getCmp('cboEpo1');
						var recordEPO = cboEPO.findRecord(cboEPO.valueField, cboEPO.getValue());
						causa = recordEPO.get(cboEPO.displayField);
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			},
			{//3 -- se queda pendiente para la parte NAFIN
				header: 'PYME',
				tooltip: 'PYME',
				dataIndex: 'PYMESELECT',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: true,
				renderer:  function (causa, columna, registro){
							if(strUsuario=='NAFIN'){
								causa = Ext.getDom("strNombrePymeAsigna").value;
							}
							columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
							return causa;
							}
			},
			{//1
				header: 'N�mero Documento',
				tooltip: 'N�mero Documento',
				dataIndex: 'NUMDOCTO',
				sortable: true,
				width: 150,
				resizable: true,
				hidden: false,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			},
			{//6
				header : 'Moneda',
				tooltip: 'Moneda',
				dataIndex : 'MONEDA',
				width : 150,
				sortable : true
			},
			{//7
				header : 'Monto',
				tooltip: 'Monto',
				dataIndex : 'MONTO',
				width : 150,
				sortable : true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{//8
				header : 'Porcentaje de Descuento',
				tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORCDESC',
				sortable : true,
				width : 100,
				renderer: Ext.util.Format.numberRenderer('%0,0.00')
			},
			{//9
				header : 'Monto a Descontar',
				tooltip: 'Monto a Descontar',
				dataIndex : 'MONTODESC',
				sortable : true,
				width : 100,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{//11
				header : 'Importe de Intereses',
				tooltip: 'Importe de Intereses',
				dataIndex : 'IMPORTINT',
				width : 150,
				sortable : true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{//12
				header : 'Importe a Recibir',
				tooltip: 'Importe a Recibir',
				dataIndex : 'IMPORTREC',
				width : 150,
				sortable : true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 400,
		width: 900,
		style: 'margin:0 auto;',
		title: ' <p align="center">PRE ACUSE - SELECCION DE DOCUMENTOS'+
				(strUsuario=='NAFIN'?'<br>'+Ext.getDom("strNombrePymeAsigna").value+'</p>':' </p>'),
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
				text: 'Revisar',
				id: 'btnRevisar',
				handler: regresaAseleccion
				},
				{
				text: 'Generar PDF',
				id: 'btnGenerarPDF',
				hidden: true
				//handler:
				},
				{
				text: 'Abrir PDF',
				id: 'btnAbrirPDF',
				hidden: true
				//handler:
				},
				'-',
				{
				text: 'Transmitir Derecho',
				id: 'btnTransmitir',
				handler: transmitir
				},
				{
				text: 'Salir',
				id: 'btnSalir',
				hidden: true,
				handler: function(){
						var cboEpo = Ext.getCmp('cboEpo1');
						var record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
						var valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
						window.location.href='13forma2ext.jsp?tipoFact=V&cvePyme='+cvePyme.value+'&cboEpo='+valEPO+'&cboMoneda='+hidCboMoneda.value+"&strUsr="+strUsr;

					}
				},
				'-',
				{
				text: 'Cancelar',
				id: 'btnCancelar',
				handler: function(){
						Ext.Msg.confirm('Confirmaci�n', '�Est� usted seguro de cancelar la operaci�n ?', function(btn){
							if(btn=='yes'){
								var cboEpo = Ext.getCmp('cboEpo1');
								var record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
								var valEPO = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
								window.location.href='13forma2ext.jsp?tipoFact=V&cvePyme='+cvePyme.value+'&cboEpo='+valEPO+'&cboMoneda='+hidCboMoneda.value+"&strUsr="+strUsr;

							}
						});
					}
				}

			]
		}
	});

	var gridTotales = new Ext.grid.GridPanel({
		id: 'gridTotales1',
		store: storeTotalData,
		margins: '20 0 0 0',
		columns: [
			{
				header : 'EPO',
				dataIndex : 'EPO',
				width : 200,
				sortable : true,
				renderer:  function (causa, columna, registro){
							var cboEPO = Ext.getCmp('cboEpo1');
							var recordEPO = cboEPO.findRecord(cboEPO.valueField, cboEPO.getValue());
							causa = recordEPO.get(cboEPO.displayField);
							columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
							return causa;
							}
			},
			{
				header : 'PYME',
				dataIndex : 'PYME',
				width : 200,
				sortable : true,
				hidden: true,
				renderer:  function (causa, columna, registro){
						if(strUsuario=='NAFIN'){
							causa = Ext.getDom("strNombrePymeAsigna").value;
						}
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
				}
			},
			{
				header : 'Total Monto Documento',
				tooltip: 'Total Monto Documento',
				dataIndex : 'TOTALMONTO',
				width : 169,
				sortable : true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Total Monto Descuento',
				tooltip: 'Total Monto Descuento',
				dataIndex : 'TOTALDESCTO',
				width : 166,
				sortable : true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Total Monto Interes',
				tooltip: 'Total Monto Interes',
				dataIndex : 'TOTALINTERES',
				width : 166,
				sortable : true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Total Importe Recibir',
				tooltip: 'Total Importe Recibir',
				dataIndex : 'TOTALIMPORT',
				width : 166,
				sortable : true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 100,
		width: 900,
		style: 'margin:0 auto;',
		title: strUsuario=='NAFIN'?'<p align="center">'+Ext.getDom("strNombrePymeAsigna").value+'</p>':' ',
		frame: true
	});

	var gridCifrasCtrl = new Ext.grid.GridPanel({
		id: 'gridCifrasCtrl1',
		store: storeCifrasData,
		margins: '20 0 0 0',
		style: 'margin:0 auto;',
		hideHeaders : true,
		columns: [
			{
				header : 'Etiqueta',
				dataIndex : 'etiqueta',
				width : 150,
				sortable : true
			},
			{
				header : 'Informacion',
				tooltip: 'Nombre Beneficiario',
				dataIndex : 'informacion',
				width : 230,
				sortable : true,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 400,
		autoHeight : true,
		title: 'Cifras de Control',
		frame: true
	});

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Selecci�n Documentos Descuento a Vencimiento',
		frame: true,
		collapsible: true,
		style: 'margin:0 auto;',
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Consultar',
				iconCls: 'icoBuscar',
				id:'btnConsultar',
				formBind: true,
				handler: consultar
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					Ext.getCmp('mensajes1').hide();
					Ext.getCmp('panelTotales1').hide();
					Ext.getCmp('gridTotales1').hide();
					Ext.getCmp('gridDoctos').hide();
					Ext.getCmp('cboMoneda1').setValue('');
					Ext.getCmp('cboEpo1').setValue('');
					Ext.getCmp('txtFechaVencDe').setValue('');
					Ext.getCmp('txtFechaVenca').setValue('');
				}

			}
		]
	});


	var panelTotales = new Ext.form.FormPanel({
		name: 'panelTotales',
		id: 'panelTotales1',
		title:'-',
		width: 900,
		height: 90,
		frame: true,
		layout:'absolute',
		defaultType: 'textfield',
      labelWidth: 0,
		items:[
			{
			  xtype:'label',
			  y:5,
			  x:142,
			  text:'Total Documentos',
			  style:'color:#777777'
			},
			{
			  xtype:'label',
			  y:5,
			  x:262,
			  text:'Monto',
			  style:'color:#777777'
			},
			{
			  xtype:'label',
			  y:5,
			  x:382,
			  text:'Monto a Descontar',
			  style:'color:#777777'
			},
			{
			  xtype:'label',
			  y:5,
			  x:502,
			  text:'Importe de Interes',
			  style:'color:#777777'
			},
			{
			  xtype:'label',
			  y:5,
			  x:622,
			  text:'Importe a Recibir',
			  style:'color:#777777'
			},
			{
			  name: 'totalDoctosDesp', // campo ID
			  id:'totalDoctosDesp1',
			  x:142,
			  y:25,
			  style:'padding:4px 3px;',
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			{
			  name: 'totalMontoDesp', // campo ID
			  id:'totalMontoDesp1',
			  x:262,
			  y:25,
			  style:'padding:4px 3px;',
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {
			  name: 'totalMontoDescDesp', // campo ID
			  id:'totalMontoDescDesp1',
			  x:382,
			  y:25,
			  style:'padding:4px 3px;',
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {
			  name: 'totalImpIntDesp', // campo ID
			  id:'totalImpIntDesp1',
			  x:502,
			  y:25,
			  style:'padding:4px 3px;',
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 },
			 {
			  name: 'totalImpRecibDesp', // campo ID
			  id:'totalImpRecibDesp1',
			  x:622,
			  y:25,
			  style:'padding:4px 3px;',
			  allowBlank: true,
			  width:100,
			  readOnly: true
			 }

		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		style: 'margin:0 auto;',
		width: 890,
		height: 'auto',
		items: [
			{//FUNCION: Panel para mostrar avisos
				xtype: 'panel',
				name: 'mensajes',
				id: 'mensajes1',
				width: 600,
				style: 'margin:0 auto;',
				frame: true,
				hidden: true
			},
			NE.util.getEspaciador(20),
			fp,
			NE.util.getEspaciador(20)
		]
	});

	storeCatEpoData.load();
	storeCatMonedaData.load();

	var iniRequest = function(storeEpo){
		var valorEpo = '';
		var cboEpo = Ext.getCmp('cboEpo1');
		if(strUsuario=='NAFIN'){
			var record = '';
			if(Ext.getDom("hidCboEpo").value!=''){
				if(storeEpo.findExact("clave", Ext.getDom("hidCboEpo").value) != -1 ){
					cboEpo.setValue(Ext.getDom("hidCboEpo").value);
					record = cboEpo.findRecord(cboEpo.valueField, cboEpo.getValue());
					valorEpo = cboEpo.getValue()+'|'+record.get(cboEpo.displayField);
				}else{
					record = storeEpo.getAt(0);
					cboEpo.setValue(record.data['clave']);
					valorEpo = cboEpo.getValue()+'|'+record.data['descripcion'];
				}
			}
		}

		Ext.Ajax.request({
			url: '13forma2ext.data.jsp',
			params: {
				informacion: 'valoresIniciales',
				cboEpo: valorEpo,
				cvePyme: Ext.getDom('cvePyme').value,
				strUsr: strUsr
				},
			callback: procesarSuccessValoresIni
		});
	}

});