<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String 	searchNotaAplicada  				= (request.getParameter("searchNotaAplicada")	== null)?"":request.getParameter("searchNotaAplicada");
String 	icNotaCredito  					= (request.getParameter("icNotaCredito")			== null)?"":request.getParameter("icNotaCredito");
boolean 	buscarNotaDeCreditoAplicada 	= searchNotaAplicada.equals("true")?true:false;
String 	searchNotasAplicadasADocto		= (request.getParameter("searchNotasDocto")		== null)?"":request.getParameter("searchNotasDocto");
String 	icDocumento							= (request.getParameter("icDocumento")				== null)?"":request.getParameter("icDocumento");
boolean 	buscarNotasAplicadasADocto		= searchNotasAplicadasADocto.equals("true")?true:false;
String 	searchNotasAplicadasADoctos	= (request.getParameter("searchNotasDoctos")		== null)?"":request.getParameter("searchNotasDoctos");
String 	icDocumentos[]						= (request.getParameterValues("claveDocumento")	== null)?null:request.getParameterValues("claveDocumento");
boolean 	buscarNotasAplicadasADoctos	= searchNotasAplicadasADoctos.equals("true")?true:false;

try {

	ArrayList lista = null;
	NotaCredito clase_nota = new NotaCredito();
	
	// CONSULTAR DETALLE NOTA DE CREDITO QUE YA HA SIDO APLICADA
	if(buscarNotaDeCreditoAplicada){
		lista = (ArrayList)clase_nota.getDetalleNotaAplicada(icNotaCredito);
	// CONSULTAR QUE NOTAS SE APLICARON AL DOCUMENTO
	}else if(buscarNotasAplicadasADocto){
		// Obtener notas de credito donde aparece el documento
		ArrayList listaIdsNotasDeCredito = (ArrayList) clase_nota.getNotasDeCredito(icDocumento);
		// Obtener lista con las notas de credito a mostrar
		lista = (ArrayList) clase_nota.getDetalleNotaAplicada(listaIdsNotasDeCredito);
	// CONSULTAR QUE NOTAS SE APLICARON A LOS DOCUMENTOS
	}else if(buscarNotasAplicadasADoctos){
		// Obtener notas de credito donde aparece el documento
		ArrayList listaIdsNotasDeCredito = (ArrayList) clase_nota.getNotasDeCredito(icDocumentos);
		// Obtener lista con las notas de credito a mostrar
		lista = (ArrayList) clase_nota.getDetalleNotaAplicada(listaIdsNotasDeCredito);
	// TOMAR DETALLE DE SESION DE LAS NOTAS DE CREDITO QUE SERAN APLICADAS
	}else{
		lista = (ArrayList)	session.getAttribute("listaNotasCreditoMultiple");
		lista = lista != null?lista:new ArrayList();
	}

	// 1. OBTENER LISTA DE LAS NOTAS DE CREDITO 
	HashSet listaClavesNotas = new HashSet();
	for(int k=0;k<lista.size();k++){
		NotaCreditoMultiple n = (NotaCreditoMultiple)lista.get(k);
		listaClavesNotas.add(n.getIcNotaCredito());
	}
	// Construir lista con las claves de las Notas de Credito a consultar
	String listaNotasDeCredito = "";
	Iterator it = listaClavesNotas.iterator();
	for (int k = 0; it.hasNext(); k++) {
		if(k>0){
			listaNotasDeCredito += ",";
		}
		listaNotasDeCredito += it.next();
	}
	// 2. OBTENER LISTA DE LOS DOCUMENTOS 
	HashSet listaClavesDocumentos = new HashSet();
	for(int k=0;k<lista.size();k++){
		NotaCreditoMultiple n = (NotaCreditoMultiple)lista.get(k);
		listaClavesDocumentos.add(n.getIcDocumento());
	}
	// Construir lista con las claves de los Documentos a consultar
	String listaDocumentos = "";
	it = listaClavesDocumentos.iterator();
	for (int k = 0; it.hasNext(); k++) {
		if(k>0){
			listaDocumentos += ",";
		}
		listaDocumentos += it.next();
	}
	// 3. OBTENER MONTOS DE LAS NOTAS DE CREDITO 
	HashMap notasCredito = clase_nota.getDatosNotasCredito(listaClavesNotas);
	// 4. OBTENER DATOS ADICIONALES DE LOS DOCUMENTOS 	
	HashMap documentos 	= clase_nota.getDatosDocumentos(listaClavesDocumentos);

	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
	String igNota				= "";
	String igDocto				= "";
	String fechaEmision		= "";
	String montoNotaCredito = "";
	String moneda				= "";
	String tipoFactoraje		= "";	
	String montoAplicado		= "";
	String saldoDocto			= "";
	String montoDocto			= "";
	String		doctoAnt			= "";
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	session.getAttribute("iNoNafinElectronico").toString(),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.setTable(7, 100);
	int i=0;
	for(i=0;i<lista.size();i++){
		NotaCreditoMultiple n = (NotaCreditoMultiple)lista.get(i);
		
		igNota				= clase_nota.getPropiedad(n.getIcNotaCredito(),"IG_NUMERO_DOCTO",notasCredito);
		igDocto				= clase_nota.getPropiedad(n.getIcDocumento(),"IG_NUMERO_DOCTO",documentos);
		fechaEmision		= clase_nota.getPropiedad(n.getIcDocumento(),"FECHA_EMISION",documentos);
		montoNotaCredito =  clase_nota.getPropiedad(n.getIcNotaCredito(),"FN_MONTO",notasCredito);
		moneda				= clase_nota.getPropiedad(n.getIcDocumento(),"MONEDA",documentos);
		tipoFactoraje		= clase_nota.getPropiedad(n.getIcDocumento(),"TIPO_FACTORAJE",documentos);	
		montoAplicado		= (new java.math.BigDecimal(n.getMontoNotaAplicado())).toPlainString();
		saldoDocto			= (new java.math.BigDecimal(n.getSaldoDocto())).toPlainString();
		montoDocto			= Double.toString(Double.parseDouble(montoAplicado) + Double.parseDouble(saldoDocto));
		if (!doctoAnt.equals(n.getIcNotaCredito())){
			pdfDoc.setCell("Número Nota: " + igNota, "celda01", ComunesPDF.CENTER,3);
			//pdfDoc.setCell(igNota, "celda01", ComunesPDF.CENTER,2);
			pdfDoc.setCell("Monto: " + " $ "+Comunes.formatoDecimal(montoNotaCredito,2), "celda01", ComunesPDF.LEFT,4);
			//pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoNotaCredito,2), "celda01", ComunesPDF.CENTER,3);
			pdfDoc.setCell("No. Docto.", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Emisión", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Docto", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Aplicado", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Saldo Docto", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo Factoraje", "celda01", ComunesPDF.CENTER);
		}
		pdfDoc.setCell(igDocto, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(fechaEmision, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(moneda, "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoDocto,2), "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoAplicado,2), "formas", ComunesPDF.CENTER);
		pdfDoc.setCell("$ "+Comunes.formatoDecimal(saldoDocto,2), "formas", ComunesPDF.CENTER);
		pdfDoc.setCell(tipoFactoraje, "formas", ComunesPDF.CENTER);
		
		doctoAnt = n.getIcNotaCredito();
		
	}

	if (i == 0){
		pdfDoc.setCell("No se encontraron registros", "formas", ComunesPDF.CENTER,7);
	}

	pdfDoc.addTable();
	pdfDoc.endDocument();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {}
%>
<%=jsonObj%>