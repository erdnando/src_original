var redirecciona = "";
Ext.onReady(function() {
	var contenido = '';
	
	function procesarGenerarArchivo(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;				
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};				
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var datos = Ext.util.JSON.decode(response.responseText);
			if (datos.botonAcepta != undefined){
				contenido = '<div class="formas" align="center"><b><font color="#FF0000">' + datos.tituloAviso + '</font></b></div>'	+
								'<div class="formas" align="left">' + datos.contenido + '</div>';
				var imagen = "";
				if (datos.extension == "doc") {
					imagen = "icoDoc";
				} else if (datos.extension == "ppt" || datos.extension == "pps") {
					imagen = "icoPpt";
				} else if (datos.extension == "pdf") {
					imagen = "icoPdf";
				}
				
				var elementosForma = [
					{
						xtype:'panel',
						id:	'panelCont',
						style: ' margin:0 auto;',
						bodyStyle: 'padding: 6px',
						defaultType: 'textfield',
						layout: 'form',
						html: contenido,
						buttonAlign: 'center',
						buttons: [
							{
								text: datos.botonAviso,
								iconCls: imagen,
								handler: function() {
								
									Ext.Ajax.request({
										url: 'contrato_ext.data.jsp',
										params: {
											informacion:'Archivo',
											nombreArchivo:datos.nombreArchivo						
										}					
										,callback: procesarGenerarArchivo
									});					
								}
							}
						]
					},{
					xtype: 'checkboxgroup', id: 'cbg', name: 'cbg', columns: 1,items: [{xtype: 'checkbox', boxLabel: datos.textCheck, name:'acepto',id:'contrato_aceptado'}],
							blankText: 'Para poder continuar debe aceptar el contrato', allowBlank: false, anchor: '95%'
					},{
						xtype:'hidden',
						id:'consecu',
						name:'consecutivo', 
						value:datos.consecutivo
					}
				];
				fp.add(elementosForma);
				btnAceptar = Ext.getCmp('btnAceptar');
				btnAceptar.setText(datos.botonAcepta);
				fp.doLayout();
				fp.el.unmask();
			} else{
				var config = {xtype: 'panel',layout: 'form',html: '<div class="formas" align="center"><b><font color="#FF0000">Ocurrio un error al obtener la informacion</font></b></div>' }
				var btnAceptar = Ext.getCmp('btnAceptar');
				if (btnAceptar){btnAceptar.hide();}
				fp.add(config);
				fp.doLayout();
			}
		} else {
			var btnAceptar = Ext.getCmp('btnAceptar');
			if (btnAceptar){btnAceptar.hide();}
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaContinuar(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			fpConfirma.el.unmask();
			redirecciona = Ext.util.JSON.decode(response.responseText).urlPagina;
			Ext.Msg.show({	msg: 'Su informaci�n fue actualizada con �xito',buttons: Ext.Msg.OK,	fn: processResultIns,animEl: 'elId',icon: Ext.MessageBox.INFO});
		} else {
			NE.util.mostrarConnError(response, opts);
		}
	}

	function procesaConfirmaClave(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR != undefined){
				if (infoR.resultado != 'S'){
						fpConfirma.el.unmask();
						redirecciona = 'contrato_ext.jsp';
						Ext.Msg.show({	msg: 'La confirmaci&oacute;n de la clave y contrase&ntilde;a es incorrecta, el proceso ha sido cancelado.',buttons: Ext.Msg.OK,fn: processResult,animEl: 'elId',icon: Ext.MessageBox.ERROR});
				} else {
					Ext.Ajax.request({
						url:'contrato_ext.data.jsp',
						params: {informacion: 'Continuar'},
						callback: procesaContinuar
					});
				}
			} 
		} else {
			NE.util.mostrarConnError(response, opts);
		}
	}

	function processResultIns(){
		window.location = redirecciona;
	}

	function processResult(){
		window.location = redirecciona;
	}

	var fpConfirma = new Ext.form.FormPanel({
			id: 'formLogin',
			width: 250,
			height:123,
			frame: true,
			border: true,
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:6px;,text-align:left',
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : [
				{	fieldLabel: 'Clave Usuario',
					name:'cesionUser',
					id: 'cesionUser1',
					style:'padding:4px 3px;',
					enableKeyEvents: true,
					allowBlank: false					
				},{
					fieldLabel: 'Contrase�a',
					name: 'cesionPassword',
					id: 'cesionPassword1',
					inputType: 'password',
					style:'padding:4px 3px;',					
					blankText: 'Digite su contrase�a',
					allowBlank: false
				}
			],
			buttons:[{
						text: 'Aceptar',id: 'btnLoginAcept', iconCls: 'icoAceptar', hidden:false,formBind: true,
						handler: function() {
							var consecu = Ext.getCmp('consecu');
							fpConfirma.el.mask('Procesando...', 'x-mask-loading');
							Ext.Ajax.request({
								url:'contrato_ext.data.jsp',
								params: Ext.apply(fpConfirma.getForm().getValues(),{informacion: 'confirmaClave', consecutivo: consecu.value}),
								callback: procesaConfirmaClave
							});
						}
					}]
	});

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 650,
		title:	'',
		hidden: false,
		frame: true,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 1,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [],
		buttonAlign: 'center',
		monitorValid: true,
		buttons: [			
			{
			text: '',
			id:'btnAceptar',
			iconCls: 'icoContinuar',
			handler: function() {
							if(!verificaPanel('forma')) {
								return;
							}
							var ventana = Ext.getCmp('winConfirma');
							if (ventana) {
								ventana.show();
							}else{
								new Ext.Window({
									modal: true,
									resizable: false,
									layout: 'form',
									width: 258,
									height: 150,
									x:500,
									y:200,
									id: 'winConfirma',
									closeAction: 'hide',
									items: [fpConfirma],
									bodyStyle: 'text-align:center',
									title: 'Confirmaci�n'
								}).show();
							}
							fpConfirma.getForm().reset();
						}
			}
		]
	});

	function verificaPanel(panel){
		var myPanel = Ext.getCmp(panel);
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype != 'panel') {
				if (!panelItem.isValid())	{
					valid = false;
					return false;
				}
			}
		});
		return valid;
	}

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(10)
		]
	});
	fp.el.mask();
	Ext.Ajax.request({
		url: 'contrato_ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});

});
