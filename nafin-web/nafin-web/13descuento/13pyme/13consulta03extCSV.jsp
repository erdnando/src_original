<%@ page contentType="application/json;charset=UTF-8"
import="
  java.text.*, 
  java.math.*, 
	javax.naming.*,
	java.util.*, 
	java.sql.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%!public static final boolean SIN_COMAS = false;%>
<%System.out.println("13consulta03extCSV.jsp (E)");%>

<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String noEpo = request.getParameter("no_epo") == null?"":(String)request.getParameter("no_epo");
String noPyme = iNoCliente; 
String infoRegresar = "";
String noIfs ="";
String moneda ="1,54";
AccesoDB con = new AccesoDB();
CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer("");
String nombreArchivo=null;
JSONObject jsonObj = new JSONObject();
int numRegistrosIf = 0;
String tasa_aplicar = "";
String valor_tasa = "";
String relmat_tasa = "";
String puntos_tasa = "";
Vector registros;
String nombreEpo = "";
try{
	con.conexionDB();
	
	AutorizacionTasas tasa = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
		
	String Epo = " select cg_razon_social as nombreEpo from comcat_epo  where ic_epo = "+noEpo;
  ResultSet rs = con.queryDB(Epo);
	while(rs.next()){
		nombreEpo  = rs.getString("nombreEpo");
	}
	rs.close();
		
	contenidoArchivo.append( nombreEpo+"\n");
		String nuevoQuery = " SELECT DISTINCT (pi.ic_if) as ic_if, i.cg_razon_social as cg_razon_social"   +
							"            FROM comrel_pyme_if pi, comcat_if i"   +
							"           WHERE ic_epo IN ("   +
							"                    SELECT rpe.ic_epo"   +
							"                      FROM comcat_epo e, comrel_pyme_epo rpe"   +
							"                     WHERE rpe.ic_epo = e.ic_epo"   +
							"                       AND e.cs_habilitado = 'S'"   +
							"                       AND rpe.ic_pyme = "+iNoCliente+""   +
							"                       AND rpe.ic_epo = "+noEpo+")"   +
							"             AND pi.ic_if = i.ic_if"  ;
		ResultSet rsnq = con.queryDB(nuevoQuery);


	while(rsnq.next()){
			numRegistrosIf++;
			for(int m=0; m < 2; m++) {
				String ic_moneda = (m==0)?"1":"54";
				registros = tasa.getTasaAutIF(noEpo, rsnq.getString("ic_if"), ic_moneda, "");
				
							
				if(registros.size() > 0){
				
 				contenidoArchivo.append( "Intermediario Financiero:,,,, "+ rsnq.getString("cg_razon_social").toString().replace(',',' ') + ",\n");
				contenidoArchivo.append(((Vector)registros.get(0)).get(6).toString().replace(',',' ')+ ",,,,,\n");
				contenidoArchivo.append("Tasa Base,Plazo,Valor,Rel. Mat.,Puntos Adicionales,Tasa a Aplicar\n");
 				for (int i = 0; i < registros.size(); i++) {
						Vector registro = (Vector) registros.get(i);
						relmat_tasa = registro.get(9).toString();
						puntos_tasa = registro.get(10).toString();
						valor_tasa = registro.get(11).toString();
						tasa_aplicar = "";

						if(relmat_tasa.equals("+")) {
							tasa_aplicar = (new BigDecimal(valor_tasa).add(new BigDecimal(puntos_tasa))).toPlainString();
						}
						if(relmat_tasa.equals("-")) {
							tasa_aplicar = (new BigDecimal(valor_tasa).subtract(new BigDecimal(puntos_tasa))).toPlainString();
						}
						if(relmat_tasa.equals("/")) {
							if (!puntos_tasa.equals("0")) {
								tasa_aplicar = (new BigDecimal(valor_tasa).divide(new BigDecimal(puntos_tasa),5)).toPlainString();
							}
						}
						if(relmat_tasa.equals("*")) {
							tasa_aplicar = (new BigDecimal(valor_tasa).multiply(new BigDecimal(puntos_tasa))).toPlainString();
						}
						
						contenidoArchivo.append(registro.get(5).toString()+","+registro.get(8).toString()+"',"+registro.get(11).toString()+","+registro.get(9).toString()+","+registro.get(10).toString()+","+tasa_aplicar+"\n");
			
					} //for
				} // if(registros.size() > 0)
			} // for m < 2
		} // while 
			
		
		if(numRegistrosIf!=0){
			if (!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
					jsonObj.put("success", new Boolean(false));
					jsonObj.put("msg", "Error al generar el archivo CSV ");
				} else {
					nombreArchivo = archivo.nombre;		
					jsonObj.put("success", new Boolean(true));
					jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
				}
		}


}catch(Exception e){
	out.println(e);
} finally {
	if (con.hayConexionAbierta()) con.cierraConexionDB();
}
%>
<%=jsonObj%>

<%System.out.println("13consulta03extCSV.jsp (E)");%>
