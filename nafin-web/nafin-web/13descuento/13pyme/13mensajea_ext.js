var titulo = '<div  class="titulos" align="center" style="font-weight:bold;">' +
					'Solicitud de afiliaci�n con Intermediarios Financieros</br>' +
				'</div>';

var global = {
	selecciona:null,
	imageIF:null,
	tipo_doc:null,
	ic_epo:null,
	ic_if:null,
	cta_banca:null,
	nombre_if:null
}

Ext.onReady(function() {

	function procesaValoresIniciales(opts, success, response) {
		fp.hide();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			if(infoR.tieneConvUnico!='S'){
          var objMsg = Ext.getCmp('mensajes1');
					objMsg.body.update('Lo sentimos no se puede realizar el proceso de afiliaci�n con el(os) Intermediario(s) Financiero(s), porque no opera con Convenio �nico');
					objMsg.show();
      }else if(Number(infoR.existCtasConvUnico)<=0){
          var objMsg = Ext.getCmp('mensajes1');
					objMsg.body.update('Lo sentimos no se puede realizar el proceso de afiliaci�n con el(os) Intermediario(s) Financiero(s), porque no tiene cuentas con Convenio �nico');
					objMsg.show();
      }else{
        consultaInicialData.loadData(infoR.NuevoReg);
        if (infoR.strPerfil == 'ADMIN PYME'){	//'ADMIN PYME BAJO'
          var textoA =	'<div  class="formas" align="justify" >'+
                        'A trav�s de esta opci�n tiene la oportunidad de reafiliarse de manera electr�nica con los intermediarios financieros '+
                        'que a continuaci�n se listan, si lo desea hacer debe leer la informaci�n que se presenta en el icono <img src="/nafin/00utils/gif/page_white_acrobat.png" />; posteriormente '+
                        'verifique que la cuenta presentada sea correcta y  finalmente seleccione: Enviar la solicitud' +
                        '</div></br>';
          var panelA = {xtype: 'panel', html:textoA};
        } else if (infoR.strPerfil == 'NANET'){
          var textoA =	'<div  class="formas" align="justify" >Con la finalidad de tener mayores alternativas en la opci&oacute;n  de Descuento Electr�nico, lo invitamos a que se afilie' +
             'con m�s de un Intermediario Financiero,  seleccionandolo de la siguiente lista:'+
             '<br>'+
             '<p>'+
             'Le recordamos que es muy importante leer las condiciones de operaci�n de cada IF. Cualquier duda llamar al Centro'+
             'de Atenci�n a Clientes 01-800 NAFINSA o en el DF al 5089-61-07'+
             '<br>'+
             '<p></div></br>';
          var panelA = {xtype: 'panel', html:textoA};
        }
        fp.insert(0,panelA);
        fp.doLayout();
        fp.show();
        if (Ext.isEmpty(infoR.NuevoReg)){
          var grid = Ext.getCmp("grid");
          grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
        }
      }
			fp.el.unmask();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaEnviaSolicitud2(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var infoR = Ext.util.JSON.decode(response.responseText);
			//consultaEnviarData.loadData(myData);
			consultaEnviarData.loadData(infoR.solic2);
			if (Ext.isEmpty(infoR.solic2)){
				var grid = Ext.getCmp("gridSelec");
				grid.getGridEl().mask('Lo sentimos no se puede realizar el proceso de afiliaci�n con el(os) Intermediario(s) Financiero(s), porque no tiene cuentas con Convenio �nico', 'x-mask');
        Ext.getCmp('cbg').disable();
        Ext.getCmp('btnEnvSolic').hide();
			}else{
        Ext.getCmp('btnEnvSolic').show();
      }
			fpSelec.el.unmask();
		} else {
			fp.show();
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaEnviaSolicitud1(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var elementosSelecUno = [
				{
					xtype:'panel',
					html:	'</br><div class="formas" align="center">'+global.imageIF+'</div>'+
							'</br><div class="forma"  height="10" colspan="3" style="text-align: justify;">'+
							'Su solicitud fue enviada al Intermediario Financiero seleccionado a efecto de ser atendido para su operaci�n de Descuento.'+
							'<p></br>En caso de  dudas llamar al centro de Atenci�n a clientes 01-800-NAFINSA, en el D.F. al 5089-61-07 o bien al tel�fono del Intermediario solicitado para mayor informaci�n sobre su tr�mite</div></br>'
				}
			];
			fpSelecUno.add(elementosSelecUno);
			fpSelecUno.doLayout();
			fpSelecUno.el.unmask();
		} else {
			fp.show();
			NE.util.mostrarConnError(response,opts);
		}
	}


	function procesaConfirmaClave(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR != undefined){
				var pError = Ext.getCmp('panelError');
				if (infoR.resultado != 'S'){
						fpConfirma.el.unmask();
						pError.show();
				} else {
					pError.hide();
					var ventana = Ext.getCmp('winConfirma');
					if (ventana) {
						ventana.hide();
					}
					fpSelec.hide();
					var elementosSelecNext = [
						{
							xtype: 'panel',
							html: '<div  class="formas" align="center" ><b>Su informaci&oacute;n fue enviada al Intermediario Financiero seleccionado a efecto'+
										'de analizarla y en su caso autorizarla para operar Descuento Electr�nico. <br>'+
										'<p>'+
										'En caso de dudas llamar al Centro de Atenci�n a Clientes <br>'+
										'01-800-NAFINSA o en el D.F. al 5089-61-07'+
										'</p>'+
										'</b></div>'+
										'</br><div class="formas" align="left">'+global.imageIF+'</div>'+
										'<div class="formas" align="left"><b>Moneda Nacional</b></div></br>'
						},{
						xtype:           'grid',
						id:              'gridSelecNext',
						store:           consultaConfirmaData,
						hidden:          false,
						columns: [
							{
								width:     300,
								header:    'Banco de Servicio',
								tooltip:   'Banco de Servicio',
								dataIndex: 'CG_BANCO',
								align:     'center',
								resizable: true,
								sortable:  true,
								hideable:  false
							},{
								width:     250,
								header:    'No. de Cuenta',
								tooltip:   'No. de Cuenta',
								dataIndex: 'NUMERO_CUENTA',
								align:     'center',
								sortable:  true,
								resizable: true,
								hideable:  false
							},{
								width:     200,
								header:    'No. de Cuenta CLABE',
								tooltip:   'No. de Cuenta CLABE',
								dataIndex: 'CUENTA_CLABE',
								align:     'center',
								sortable:  true,
								resizable: true,
								hideable:  false
							},{
								width:     200,
								header:    'Sucursal',
								tooltip:   'Sucursal',
								dataIndex: 'SUCURSAL',
								align:     'center',
								sortable:  true,
								resizable: true,
								hideable:  false
							}
						],
						height:          200,
						width:           700,
						viewConfig:      {forceFit: true},
						stripeRows:      true,
						columnLines:     true,
						loadMask:        true,
						frame:           false
						},{
							xtype:'panel',
							html:	'&nbsp;'
						},{
							xtype: 'checkboxgroup', id: 'cbg', name: 'cbg', columns: 1,blankText: 'Tiene que aceptar el env�o de su informaci�n', allowBlank: false, anchor: '80%',msgTarget: 'side',anchor: '-80',
							items:[{xtype: 'checkbox', name:'acepto',id:'contrato_aceptado',
										boxLabel: '<div align="center"><b>ENVIAR   SOLICITUD</b></br></div>'+
														'<div align="left">Acepto el env�o de mi informaci�n al Intermediario Financiero en base a los Arts. 80, 89 al 93 BIS del C�digo de Comercio, 210-A del C�digo Federal de Procedimientos Civiles y 1803 del C�digo Civil Federal</div>'
							}]
						}
					];
					fpSelecNext.add(elementosSelecNext);
					fpSelecNext.doLayout();
					fpSelecNext.show();
					
					if (Ext.isEmpty(infoR.nuevoSolic2)){
						var grid = Ext.getCmp("gridSelecNext");
						grid.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					} else {
						consultaConfirmaData.loadData(infoR.nuevoSolic2);
					}
				}
			}
		} else {
			NE.util.mostrarConnError(response, opts);
		}
	}


var procesarConsEnvData = function(store, arrRegistros, opts) {
    var elG = Ext.getCmp('gridSelec').getGridEl()
		if (arrRegistros != null) {
			var elG = gridNotaSimple.getGridEl();
			if(store.getTotalCount() > 0) {
				elG.unmask();
			}else{
				elG.mask('Lo sentimos no se puede realizar el proceso de afiliaci�n con el(os) Intermediario(s) Financiero(s), porque no tiene cuentas con Convenio �nico');
			}
		}
	}

//------------------------------------------------------------------------------
	var muestraEnviarSolicitud = function(grid, rowIndex, item, event){
		global.selecciona = false;
		fp.hide();
		var registro= grid.getStore().getAt(rowIndex);
		global.tipo_doc	=	registro.get('TIPODOCUMENTO');
		global.ic_epo		=	registro.get('IC_EPOR');
		global.ic_if		=	registro.get('IC_IF');
		//global.cta_banca	=	registro.get('CUENTA_BANCARIA');
		global.nombre_if	=	registro.get('NOMBRE_IF');
    if(global.tipo_doc){
      if (registro.get('REG') == 0){
        global.imageIF = registro.get('NOMBRE_IF');
      }else{
        global.imageIF = '<img src="../../00archivos/if/logos/'+registro.get('NOMBREARCHIVO')+'">';
      };
  
      if (global.tipo_doc == "2"){
  
        var elementosSelec = [
          {
            xtype: 'panel',
            html: '<div  class="formas" align="justify" >Reconozco expresamente que he le&iacute;do y estoy de acuerdo en los t&eacute;rminos y condiciones '+
                'que ofrece el Intermediario Financiero seleccionado y comprendo los derechos, obligaciones, '+
                'cl&aacute;usulas y condiciones estipulados.  Por lo que acepto expresamente que al hacer clic '+
                'en el bot&oacute;n �ENVIAR SOLICITUD" y/o utilizar el sistema de Cadenas Productivas la operaci&oacute;n '+
                'se regir&aacute; por los t&eacute;rminos y condiciones ofrecidos por el Intermediario Financiero.  '+
                'Lo anterior, en t&eacute;rminos de la legislaci&oacute;n civil y mercantil aplicable, que otorga '+
                'validez al consentimiento otorgado a trav&eacute;s de medios electr&oacute;nicos, &oacute;pticos o '+
                'de cualquier otra tecnolog&iacute;a, para que el consentimiento surta efectos.</br></br></div>'+
                '<div class="formas" align="center"><b>Seleccione la cuenta bancaria que tiene registrada, <br>en donde el Intermediario le depositara sus recursos</b></br></br></div>'+
                '</br><div class="formas" align="left">'+global.imageIF+'</div>'+
                '</br><div class="formas" align="left"><b>Moneda Nacional</b></div></br>'
          },{
          xtype: 'grid',
          id: 'gridSelec',
          store: consultaEnviarData,
          hidden: false,
          //sm: selecModel,
          columns: [
            {
              header:	'Banco de Servicio',	tooltip:	'Banco de Servicio',	dataIndex:	'CG_BANCO',
              sortable: true,	width:	250,	align:	'center',	hideable:	false,	resizable: true
            },{
              header: 'No. de Cuenta',	tooltip:	'No. de Cuenta',	dataIndex:	'NUMERO_CUENTA',
              sortable:	true,	width:	200,	resizable:	true,	hideable:	false,	align:	'center'
            },{
              header: 'No. de Cuenta CLABE',	tooltip:	'No. de Cuenta CLABE',	dataIndex:	'CUENTA_CLABE',
              sortable:	true,	width:	200,	resizable:	true,	hideable:	false,	align:	'center'
            },{
              header:	'Sucursal',	tooltip:	'Sucursal',	dataIndex:	'SUCURSAL',
              sortable:	true,	width:	110,	resizable:	true,	hideable:	false, align:	'center'
            },{
              header: 'Estatus',	tooltip:	'Seleccione',	dataIndex:	'CUENTA_BANCARIA',
              sortable:	true,	width:	130,	resizable:	true,	hideable:	false, align:	'center',
              renderer:function(value, metadata, record, rowIndex, colIndex, store){                            
                      return '<input type="radio" id="'+ rowIndex +'" name="seleccionado" value="' + value + '"></input>';
                    }
            }
          ],
          viewConfig: {forceFit: true},
          stripeRows: true,
          columnLines : true,
          loadMask: true,
          //height:'auto',
          height:200,
          width: 700,
          frame: false
          },{
            xtype:'panel',
            html:	'&nbsp;'
          },{
            xtype: 'checkboxgroup', id: 'cbg', name: 'cbg', columns: 1,blankText: 'Tiene que aceptar el env�o de su informaci�n', allowBlank: false, anchor: '80%',msgTarget: 'side',anchor: '-80',
            items:[{xtype: 'checkbox', name:'acepto',id:'contrato_aceptado',
                  boxLabel: '<div align="center"><b>ENVIAR   SOLICITUD</b></br></div>'+
                          '<div align="left">Acepto el env�o de mi informaci�n al Intermediario Financiero en base a los Arts. 80, 89 al 93 BIS del C�digo de Comercio, 210-A del C�digo Federal de Procedimientos Civiles y 1803 del C�digo Civil Federal</div>'
            }]
          }
        ];
  
        fpSelec.add(elementosSelec);
        fpSelec.doLayout();
        fpSelec.show();
        fpSelec.el.mask('Procesando...', 'x-mask-loading');
        Ext.Ajax.request({
          url: '13mensajea_ext.data.jsp',
          params: {
            informacion: "enviaSolicitud2"
          },
          callback: procesaEnviaSolicitud2
        });
      }else if (global.tipo_doc == "1"){
        fpSelecUno.show();
        fpSelecUno.el.mask('Procesando...', 'x-mask-loading');
        Ext.Ajax.request({
          url: '13mensajea_ext.data.jsp',
          params: Ext.apply({informacion: 'enviaSolicitud1', ic_if: global.ic_if, ic_epo: global.ic_epo}),
          callback: procesaEnviaSolicitud1
        });
      }
    }
	}

	var muestraVerInformacion = function(grid, rowIndex, colIndex, item, event){
		var registro = grid.getStore().getAt(rowIndex);
		var archivoConvenio = registro.get('ARCHIVOCONVENIO');
		ruta = '../../00archivos/if/conv/'+archivoConvenio;
		var winConvenio = new Ext.Window ({
		  x: 400,
		  y: 100,
		  width: 800,
		  height: 500,
		  modal: true,
		  closeAction: 'hide',
		  title: 'Convenio',
		  html: '<iframe style="width:100%;height:100%;border:0" src="'+ruta+'"></iframe>'
		}).show();
	}

	var myData = [
		{'CUENTA_BANCARIA':'0001','CG_BANCO':'mibanco','NUMERO_CUENTA':'00010','SUCURSAL':'df'},
		{'CUENTA_BANCARIA':'0002','CG_BANCO':'banamex','NUMERO_CUENTA':'00011','SUCURSAL':'edomex'},
		{'CUENTA_BANCARIA':'0003','CG_BANCO':'bancomer','NUMERO_CUENTA':'00012','SUCURSAL':'hidalgo'},
		{'CUENTA_BANCARIA':'0004','CG_BANCO':'hsbc','NUMERO_CUENTA':'00013','SUCURSAL':'xalapa'},
		{'CUENTA_BANCARIA':'0005','CG_BANCO':'scotia','NUMERO_CUENTA':'00014','SUCURSAL':'hermosillo'}
	];

	var consultaConfirmaData = new Ext.data.JsonStore({
		fields: [
			//{name: 'CUENTA_BANCARIA'},
			{name: 'CG_BANCO'     },
			{name: 'NUMERO_CUENTA'},
			{name: 'CUENTA_CLABE' },
			{name: 'SUCURSAL'     }
		],
		data:[{'CG_BANCO':'','NUMERO_CUENTA':'','CUENTA_CLABE':'','SUCURSAL':''}],
		autoload: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var consultaEnviarData = new Ext.data.JsonStore({
		fields: [
			{name: 'CUENTA_BANCARIA'},
			{name: 'CG_BANCO'},
			{name: 'NUMERO_CUENTA'},
			{name: 'SUCURSAL'},
      {name: 'CUENTA_CLABE'}
		],
		data:[{'CUENTA_BANCARIA':'','CG_BANCO':'','NUMERO_CUENTA':'','SUCURSAL':''}],
		autoload: true,
		listeners: {lexception: NE.util.mostrarDataProxyError}
	});

	var consultaInicialData = new Ext.data.JsonStore({
		fields:[	{name: 'IC_IF'},
					{name: 'IC_EPOR'},
					{name: 'NOMBRE_IF'},
					{name: 'NOMBRE_EPO'},
					{name: 'NOMBREARCHIVO'},
					{name: 'REG'},
					{name: 'FECHACONVENIO'},
					{name: 'TIPODOCUMENTO'},
					{name: 'ARCHIVOCONVENIO'}
		],
		data:	[{'NOMBRE_IF':'','NOMBRE_EPO':'','NOMBREARCHIVO':'','REG':'','FECHACONVENIO':'','TIPODOCUMENTO':'','ARCHIVOCONVENIO':''}],
		autoload:true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var panelInfo = new Ext.Panel({
		id: 'panelInfo',
		height: 'auto',
		hidden: false,
		align: 'center',
		autoScroll: true	
	});

	var fpConfirma = new Ext.form.FormPanel({
			id: 'formLogin',
			width: 270,
			height:'auto',
			//height:165,
			frame: true,
			border: true,
			monitorValid: true,
			buttonAlign: 'center',
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : [
				{
					xtype:'panel',
					id:	'panelError',
					hidden: true,
					html:	'<div align="center" class="titulos">Datos de confirmaci�n incorrectos. El usuario no es valido.</div></br>'
				},
				{	fieldLabel: 'Clave Usuario',
					name:'cesionUser',
					id: 'cesionUser1',
					style:'padding:4px 3px;',
					enableKeyEvents: true,
					allowBlank: false,
					maxLength : 8
				},{
					fieldLabel: 'Contrase�a',
					name: 'cesionPassword',
					id: 'cesionPassword1',
					inputType: 'password',
					style:'padding:4px 3px;',
					//maxLength: 8,
					blankText: 'Digite su contrase�a',
					allowBlank: false
				}
			],
			buttons:[{
						text: 'Aceptar',id: 'btnLoginAcept', iconCls: 'icoAceptar', hidden:false,formBind: true,
						handler: function() {
							fpConfirma.el.mask('Procesando...', 'x-mask-loading');
							Ext.Ajax.request({
								url:'13mensajea_ext.data.jsp',
								params: Ext.apply(fpConfirma.getForm().getValues(),{informacion: 'confirmaClave', ic_if: global.ic_if, ic_epo: global.ic_epo, ic_cuenta_bancaria: global.cta_banca, intermediario: global.nombre_if}),
								callback: procesaConfirmaClave
							});
						}
					}]
	});

	var elementosForma = [
		{
		xtype: 'grid',	id:'grid',	store:consultaInicialData,	hidden:false,	stripeRows:true,	columnLines:true,
		loadMask: true,	height:400,	width:700,	frame:false,	anchor:'100%',
		columns: [
			{
				header:	'Intermediario Financiero',	tooltip:	'Intermediario Financiero',	dataIndex:	'NOMBRE_IF',
				sortable: true,	width:	250,	align:	'center',	hideable:false,	resizable: true,
				renderer:	function(value, metadata, record, rowindex, colindex, store) {
									var regresaValor = "";
									if (record.get('REG') == '0'){
										regresaValor = value;
									} else{
										regresaValor = '<img width=150 height=60 src="../../00archivos/if/logos/'+record.get('NOMBREARCHIVO')+'">';
									}
									return regresaValor;
								}
			},{
				header:'EPO',	tooltip:'Nombre EPO',	dataIndex:'NOMBRE_EPO',	sortable:true,	width:200,	resizable:true,	hideable:false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								metadata.attr = 'ext:qtip="' + Ext.util.Format.nl2br(value) + '"';
								return value;
				}
			},{
				xtype:	'actioncolumn',
				header:	'Ver Informaci�n',	tooltip:	'Ver Informaci�n',	dataIndex:	'FECHACONVENIO',
				sortable:	true,	width:	105,	resizable:	true,	hideable:	false, align:	'center',
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								if (Ext.isEmpty(value)){
									value = 'No existe convenio';
								}
								return value;
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (!Ext.isEmpty(value)){
								this.items[0].tooltip = 'Ver';
								return 'icoPdf';
							}else	{
								return value;
							}
						},
						handler:	muestraVerInformacion
					}
				]
			},{
				xtype: 'actioncolumn',
				header: 'Estatus',	tooltip:	'Estatus',	dataIndex:	'TIPODOCUMENTO',
				sortable:	true,	width:	120,	resizable:	true,	hideable:	false, align:	'center',
				renderer:	function(value, metadata, record, rowindex, colindex, store) {
								return 'Enviar Solicitud';
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (!Ext.isEmpty(value)){
								this.items[0].tooltip = 'Enviar Solicitud';
								return 'icoContinuar';
							}else	{
								return value;
							}
						},
						handler:	muestraEnviarSolicitud
					}
				]
			}
		]
		}/*,{
			xtype: 'panel',
			html: '<div class="formas" align="justify"></br>Si ya tiene un Intermediario Autorizado, para consultar sus documentos negociables para descuento, por favor seleccione la opci&oacute;n <b>Capturas</b> del men&uacute; superior <b>Descuento</b>, y posteriormente <b>Selecci&oacute;n  documentos a descontar</b>.</div>'
		}*/
	];

	var fpSelecUno	=	new Ext.FormPanel({
		height:'auto',
		width: 700,
		style: ' margin:0 auto;',
		defaults:	{bodyStyle: 'padding: 3px',msgTarget: 'side',anchor: '-1'},
		title: titulo,
		border: true,
		frame: true,
		items: [],
		buttonAlign: 'center',
		monitorValid: true,
		buttons: [
			{
			text: 'Salir',
			iconCls: 'icoContinuar',
			handler: function() {
							window.location = '13mensajea_ext.jsp';
						}
			}
		],
		hidden: true
	});

	var fpSelecNext	=	new Ext.FormPanel({
		height:'auto',
		width: 700,
		style: ' margin:0 auto;',
		defaults:	{bodyStyle: 'padding: 3px',msgTarget: 'side',anchor: '-1'},
		title: titulo,
		border: true,
		frame: true,
		items: [],
		buttonAlign: 'center',
		monitorValid: true,
		buttons: [
			{
			text: 'Salir',
			iconCls: 'icoContinuar',
			handler: function() {
							window.location = '13mensajea_ext.jsp';
						}
			}
		],
		hidden: true
	});

	function processResult(res){
		if (res == 'ok'){
			var ventana = Ext.getCmp('winConfirma');
			if (ventana) {
				ventana.show();
			}else{
				new Ext.Window({
					modal: true,
					resizable: false,
					layout: 'form',
					width: 270,
					height: 200,
					x:500,
					y:200,
					id: 'winConfirma',
					closeAction: 'hide',
					items: [fpConfirma],
					bodyStyle: 'text-align:center',
					title: 'Confirmaci�n'
				}).show();
			}
		}
	}


	var fpSelec	=	new Ext.FormPanel({
		height:'auto',
		width: 700,
		style: ' margin:0 auto;',
		defaults:	{bodyStyle: 'padding: 3px',msgTarget: 'side',anchor: '-1'},
		title: titulo,
		border: true,
		frame: true,
		items: [],
		buttonAlign: 'center',
		monitorValid: true,
		buttons: [
			{
				text: 'Enviar Solicitud',
				iconCls: 'correcto',
        id: 'btnEnvSolic',
				formBind: true,
				handler: function() {
							var ds = consultaEnviarData;
							var jsonData = ds.reader.jsonData;
							Ext.each(jsonData, function(item,index,arrItem){
								var radio = Ext.getDom(index.toString());
								if (radio){
									if (radio.checked){
										global.cta_banca = radio.value;
										global.selecciona = true;
									}
								}
							});

				
							if (!global.selecciona){
								Ext.Msg.alert('','Debe seleccionar una cuenta bancaria');
								return;
							}else{
								Ext.Msg.show({
									msg: '�Esta seguro de seleccionar esta cuenta bancaria?',
									buttons: Ext.Msg.OKCANCEL,
									fn: processResult,
									animEl: 'elId',
									icon: Ext.MessageBox.QUESTION
								});
							}
							fpConfirma.getForm().reset();
				}
			}
		],
		hidden: true
	});

	var fp	=	new Ext.FormPanel({
		height:'auto',
		width: 700,
		style: ' margin:0 auto;',
		defaults:	{bodyStyle: 'padding: 3px',msgTarget: 'side',anchor: '-1'},
		title: titulo,
		border: true,
		frame: true,
		items: elementosForma//,
		//hidden: true
	});
  
  var panelMsg = new Ext.Panel({//Panel para mostrar avisos
				name: 'mensajes',
				id: 'mensajes1',
				width: 500,
				frame: true,
				style: 'margin:0 auto;',
				hidden: true
			});
      
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		height: 'auto',
		items: [
      NE.util.getEspaciador(10),
			panelMsg,fp,fpSelec,fpSelecNext,fpSelecUno,
			NE.util.getEspaciador(5)
		]
	});
	fp.el.mask('Cargando...', 'x-mask-loading');
	Ext.Ajax.request({
		url: '13mensajea_ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});	
});
