<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.sql.*,
		netropology.utilerias.*,com.netro.descuento.*,
		com.netro.descuento.RepCEstatusPymeDEbean,
		com.netro.model.catalogos.CatalogoMandante,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String sOperaFactConMandato = "";
String sFechaVencPyme = "";
String infoRegresar = "";
boolean bOperaFactConMandato					= false;
boolean bOperaFactorajeVencido				= false;
boolean bOperaFactorajeDistribuido			= false;
boolean bOperaFactorajeNotaCredito			= false;
boolean bOperaFactVencimientoInfonavit		= false;
boolean bTipoFactoraje							= false;
boolean mostrarOpcionPignorado				=false;
boolean bOperaFactorajeIF =false;
String consulta ="";
JSONObject jsonObj = new JSONObject();
java.text.SimpleDateFormat formatoHora2 = new java.text.SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a"); 
String hoyFecha = (formatoHora2.format(new java.util.Date()));

if(iNoEPO != null && !iNoEPO.equals("")) {
	ISeleccionDocumento beanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
	sFechaVencPyme = beanSeleccionDocumento.operaFechaVencPyme(iNoEPO);
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	Hashtable alParamEPO = new Hashtable();
	alParamEPO = BeanParamDscto.getParametrosEPO(iNoEPO, 1);
	if (alParamEPO!=null) {
		bOperaFactConMandato				= ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		bOperaFactorajeVencido			= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
		bOperaFactorajeDistribuido		= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
		bOperaFactorajeNotaCredito		= ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
		bOperaFactVencimientoInfonavit= ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString())) ? false : true ;
	   bOperaFactorajeIF= ("N".equals(alParamEPO.get("FACTORAJE_IF").toString())) ? false : true ;
	}
	sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
	mostrarOpcionPignorado = BeanParamDscto.mostrarOpcionPignorado(iNoEPO, iNoCliente);
	bTipoFactoraje = (bOperaFactorajeIF || bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaCredito || bOperaFactConMandato || bOperaFactVencimientoInfonavit )?true:false;

}

if (informacion.equals("dataIniciales")) {

	bTipoFactoraje = (bOperaFactorajeIF || bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaCredito || bOperaFactConMandato || bOperaFactVencimientoInfonavit )?true:false;
	bOperaFactorajeVencido = true;
	jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("Usuario", strNombreUsuario);
	jsonObj.put("Fecha", hoyFecha);
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));	
	jsonObj.put("mostrarOpcionPignorado", new Boolean(mostrarOpcionPignorado));	
	jsonObj.put("strAforo", new Double(strAforo));
	jsonObj.put("strAforoDL", new Double(strAforoDL));
	jsonObj.put("bOperaFactorajeIF", new Boolean(bOperaFactorajeIF));
	
	infoRegresar = jsonObj.toString();
	
	}else if	(informacion.equals("dataDescuento"))	{

	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepCEstatusPymeDEbean repDscto = new RepCEstatusPymeDEbean();
		repDscto.setIccambioestatusdocto("6");
		repDscto.setFechaVencPyme(sFechaVencPyme);
		repDscto.setOperaFactConMandato(sOperaFactConMandato);
		repDscto.setIcePyme(iNoCliente);
		repDscto.setIceEpo(iNoEPO);
		repDscto.setQrysentencia("");
		Registros registros = repDscto.executeQuery();
		
		consulta = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		consulta = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("Usuario", strNombreUsuario);
	jsonObj.put("Fecha", hoyFecha);
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));	
	jsonObj.put("mostrarOpcionPignorado", new Boolean(mostrarOpcionPignorado));	
	jsonObj.put("strAforo", new Double(strAforo));
	jsonObj.put("strAforoDL", new Double(strAforoDL));
	jsonObj.put("bOperaFactorajeIF", new Boolean(bOperaFactorajeIF));
	infoRegresar = jsonObj.toString();
	
}else if	(informacion.equals("dataBaja"))	{

	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepCEstatusPymeDEbean repBaja = new RepCEstatusPymeDEbean();
		repBaja.setIccambioestatusdocto("5");
		repBaja.setFechaVencPyme(sFechaVencPyme);
		repBaja.setOperaFactConMandato(sOperaFactConMandato);
		repBaja.setIcePyme(iNoCliente);
		repBaja.setIceEpo(iNoEPO);
		repBaja.setQrysentencia("");
		Registros registros = repBaja.executeQuery();
		consulta = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		consulta = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("Usuario", strNombreUsuario);
	jsonObj.put("Fecha", hoyFecha);
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));	
	jsonObj.put("mostrarOpcionPignorado", new Boolean(mostrarOpcionPignorado));	
	jsonObj.put("strAforo", new Double(strAforo));
	jsonObj.put("strAforoDL", new Double(strAforoDL));
	jsonObj.put("bOperaFactorajeIF", new Boolean(bOperaFactorajeIF));
	infoRegresar = jsonObj.toString();

}else if	(informacion.equals("dataPagadoAnticipado"))	{

	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepCEstatusPymeDEbean repPanticipado = new RepCEstatusPymeDEbean();
		repPanticipado.setIccambioestatusdocto("7");
		repPanticipado.setFechaVencPyme(sFechaVencPyme);
		repPanticipado.setOperaFactConMandato(sOperaFactConMandato);
		repPanticipado.setIcePyme(iNoCliente);
		repPanticipado.setIceEpo(iNoEPO);
		repPanticipado.setQrysentencia("");
		Registros registros = repPanticipado.executeQuery();
		consulta = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		consulta = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("Usuario", strNombreUsuario);
	jsonObj.put("Fecha", hoyFecha);
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));	
	jsonObj.put("mostrarOpcionPignorado", new Boolean(mostrarOpcionPignorado));	
	jsonObj.put("strAforo", new Double(strAforo));
	jsonObj.put("strAforoDL", new Double(strAforoDL));
	jsonObj.put("bOperaFactorajeIF", new Boolean(bOperaFactorajeIF));
	infoRegresar = jsonObj.toString();
	
}else if	(informacion.equals("dataSelecPymeNegociable"))	{

	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepCEstatusPymeDEbean repPymeNego = new RepCEstatusPymeDEbean();
		repPymeNego.setIccambioestatusdocto("2");
		repPymeNego.setFechaVencPyme(sFechaVencPyme);
		repPymeNego.setOperaFactConMandato(sOperaFactConMandato);
		repPymeNego.setIcePyme(iNoCliente);
		repPymeNego.setIceEpo(iNoEPO);
		repPymeNego.setQrysentencia("");
		Registros registros = repPymeNego.executeQuery();
		consulta = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		consulta = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("Usuario", strNombreUsuario);
	jsonObj.put("Fecha", hoyFecha);
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));	
	jsonObj.put("mostrarOpcionPignorado", new Boolean(mostrarOpcionPignorado));	
	jsonObj.put("strAforo", new Double(strAforo));
	jsonObj.put("strAforoDL", new Double(strAforoDL));
	jsonObj.put("bOperaFactorajeIF", new Boolean(bOperaFactorajeIF));
	infoRegresar = jsonObj.toString();

}else if	(informacion.equals("dataPignoradaSelecPyme"))	{

	if(iNoEPO != null && !iNoEPO.equals("")) {
		//RepCEstatusPymeDEbean repProgram = new RepCEstatusPymeDEbean();
		//repProgram.setOperaFactConMandato(sOperaFactConMandato);
		//repProgram.setIcePyme(iNoCliente);
		//repProgram.setIceEpo(iNoEPO);
		//repProgram.setQrysentencia("");
		//Registros registros = repProgram.executeQuery();
		//infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if	(informacion.equals("dataPignoradaNegociable"))	{

   if(iNoEPO != null	&&	!iNoEPO.equals("")){
		if (mostrarOpcionPignorado)	{
			RepCEstatusPymeDEbean repPignorado = new RepCEstatusPymeDEbean();
			repPignorado.setIccambioestatusdocto("26");
			repPignorado.setFechaVencPyme(sFechaVencPyme);
			repPignorado.setIcePyme(iNoCliente);
			repPignorado.setIceEpo(iNoEPO);
			repPignorado.setQrysentencia("");
			Registros registros = repPignorado.executeQuery();
			consulta = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
		}else {
			consulta = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
		}
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("Usuario", strNombreUsuario);
	jsonObj.put("Fecha", hoyFecha);
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));	
	jsonObj.put("mostrarOpcionPignorado", new Boolean(mostrarOpcionPignorado));	
	jsonObj.put("strAforo", new Double(strAforo));
	jsonObj.put("strAforoDL", new Double(strAforoDL));
	jsonObj.put("bOperaFactorajeIF", new Boolean(bOperaFactorajeIF));
	infoRegresar = jsonObj.toString();
	
}else if	(informacion.equals("dataModificaImporte"))	{

	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepCEstatusPymeDEbean repModifica = new RepCEstatusPymeDEbean();
		repModifica.setIccambioestatusdocto("8");
		repModifica.setFechaVencPyme(sFechaVencPyme);
		repModifica.setOperaFactConMandato(sOperaFactConMandato);
		repModifica.setIcePyme(iNoCliente);
		repModifica.setIceEpo(iNoEPO);	
		repModifica.setQrysentencia("");
		Registros registros = repModifica.executeQuery();
		consulta = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		consulta = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("Usuario", strNombreUsuario);
	jsonObj.put("Fecha", hoyFecha);
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));	
	jsonObj.put("mostrarOpcionPignorado", new Boolean(mostrarOpcionPignorado));	
	jsonObj.put("strAforo", new Double(strAforo));
	jsonObj.put("strAforoDL", new Double(strAforoDL));
	jsonObj.put("bOperaFactorajeIF", new Boolean(bOperaFactorajeIF));
	infoRegresar = jsonObj.toString();
	
}else if	(informacion.equals("dataAplicaNotaCredito"))	{

	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepCEstatusPymeDEbean repNotaCredito = new RepCEstatusPymeDEbean();
		repNotaCredito.setIccambioestatusdocto("28");
		repNotaCredito.setFechaVencPyme(sFechaVencPyme);
		repNotaCredito.setOperaFactConMandato(sOperaFactConMandato);
		repNotaCredito.setIcePyme(iNoCliente);
		repNotaCredito.setIceEpo(iNoEPO);	
		repNotaCredito.setQrysentencia("");
		Registros registros = repNotaCredito.executeQuery();
		consulta = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		consulta = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("Usuario", strNombreUsuario);
	jsonObj.put("Fecha", hoyFecha);
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));	
	jsonObj.put("mostrarOpcionPignorado", new Boolean(mostrarOpcionPignorado));	
	jsonObj.put("strAforo", new Double(strAforo));
	jsonObj.put("strAforoDL", new Double(strAforoDL));
	jsonObj.put("bOperaFactorajeIF", new Boolean(bOperaFactorajeIF));
	infoRegresar = jsonObj.toString();
	
}else if	(informacion.equals("dataProgramado"))	{

	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepCEstatusPymeDEbean repProgram = new RepCEstatusPymeDEbean();
		repProgram.setIccambioestatusdocto("29");
		repProgram.setFechaVencPyme(sFechaVencPyme);
		repProgram.setOperaFactConMandato(sOperaFactConMandato);
		repProgram.setIcePyme(iNoCliente);
		repProgram.setIceEpo(iNoEPO);
		repProgram.setQrysentencia("");
		Registros registros = repProgram.executeQuery();
		consulta = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		consulta = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
	
	jsonObj.put("success", new Boolean(true));
	jsonObj = JSONObject.fromObject(consulta);
	jsonObj.put("Usuario", strNombreUsuario);
	jsonObj.put("Fecha", hoyFecha);
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));	
	jsonObj.put("mostrarOpcionPignorado", new Boolean(mostrarOpcionPignorado));	
	jsonObj.put("strAforo", new Double(strAforo));
	jsonObj.put("strAforoDL", new Double(strAforoDL));
	jsonObj.put("bOperaFactorajeIF", new Boolean(bOperaFactorajeIF));
	infoRegresar = jsonObj.toString();
  
}
System.out.println("infoRegresar "+infoRegresar);
%>
<%=infoRegresar%>
