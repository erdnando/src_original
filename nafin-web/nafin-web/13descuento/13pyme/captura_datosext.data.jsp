<%@ page contentType="application/json;charset=UTF-8"
   import="java.util.*,
		netropology.utilerias.usuarios.*,
		netropology.utilerias.*,
		com.netro.model.catalogos.*,
		com.netro.model.catalogos.CatalogoEPOPyme,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.descuento.*, 
		com.netro.afiliacion.*,
		com.netro.exception.*,
		com.netro.seguridadbean.*,
		com.netro.pdf.*,
		net.sf.json.*" errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="/13descuento/13secsession.jspf" %>

<%
String informacion = (request.getParameter("informacion") == null) ? "" : request.getParameter("informacion");

String archivo =  (request.getParameter("archivo") == null) ? "" : request.getParameter("archivo");
String ic_grupo = (request.getParameter("ic_grupo") == null) ? "" : request.getParameter("ic_grupo");

String calle_numero = (request.getParameter("calle_numero") == null) ? "" : request.getParameter("calle_numero");
String colonia = (request.getParameter("colonia") == null) ? "" : request.getParameter("colonia");

String pais = (request.getParameter("pais") == null || request.getParameter("pais").equals("")) ? "0" : request.getParameter("pais");
String estado = (request.getParameter("estado") == null || request.getParameter("estado").equals("")) ? "0" : request.getParameter("estado");
String delegacion_o_municipio = (request.getParameter("delegacion_o_municipio") == null) ? "0" : request.getParameter("delegacion_o_municipio");

String cp = (request.getParameter("cp") == null) ? "" : request.getParameter("cp");
String telefono = (request.getParameter("telefono") == null) ? "" : request.getParameter("telefono");
String fax = (request.getParameter("fax") == null) ? "" : request.getParameter("fax");
String email = (request.getParameter("email") == null) ? "" : request.getParameter("email");
String nombrec = (request.getParameter("nombrec") == null) ? "" : request.getParameter("nombrec");
String telefonoc = (request.getParameter("telefonoc") == null) ? "" : request.getParameter("telefonoc");
String faxc = (request.getParameter("faxc") == null) ? "" : request.getParameter("faxc");
String emailc = (request.getParameter("emailc") == null) ? "" : request.getParameter("emailc");

String accion = (request.getParameter("accion") == null) ? "" : request.getParameter("accion");

AccesoDB con = new AccesoDB();
ResultSet rs;
PreparedStatement	ps 	= null;
String qry = "";
String grupo = "", razonsocial="", rfc="";
String regresa = "";

try{    

   con.conexionDB();
   if(accion.equals("G")){
      Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
      List lDatos = new ArrayList();
      lDatos.add(iNoCliente);
      lDatos.add(calle_numero);
      lDatos.add(colonia);
      lDatos.add(estado);
      lDatos.add(delegacion_o_municipio);
      lDatos.add(cp);
      lDatos.add(pais);
      lDatos.add(telefono);
      lDatos.add(fax);
      lDatos.add(email);
      lDatos.add(nombrec);
      lDatos.add(telefonoc);
      lDatos.add(faxc);
      lDatos.add(emailc);
      lDatos.add(ic_grupo);
      afiliacion.insertaDatosExternosPyme(lDatos);
      
      String msgRecepcion = "Sus Datos han sido recibidos";
      String urlForma = new String();
      
      if(   archivo.equals("1")  ){
         urlForma = "13forma1ext.jsp";
      } else if(  archivo.equals("2") ) {
         urlForma = "13forma2ext.jsp";
      }
      
      JSONObject jsonObj = new JSONObject();
      jsonObj.put("success", new Boolean(true));
      jsonObj.put("msgRecepcion",msgRecepcion);
      jsonObj.put("urlForma",urlForma);
      regresa = jsonObj.toString();
   }
      
   else if(   informacion.equals("IniciaPrograma")  ){   
      JSONObject jsonObj = new JSONObject();
      jsonObj.put("success", new Boolean(true));
      
      // Obtiene Grupo
      qry = "select cd_nombre_grupo from com_param_grupo where ic_grupo = ?";
      ps = con.queryPrecompilado(qry);
      ps.setString(1,ic_grupo);
      rs = ps.executeQuery();
      if(rs.next()){
         grupo = rs.getString("cd_nombre_grupo");
      }
      rs.close();
      ps.close();	
      
      // Obtiene Datos empresa
      qry = "select cg_razon_social, cg_rfc from comcat_pyme where ic_pyme = ?";
      ps = con.queryPrecompilado(qry);
      ps.setString(1,iNoCliente);
      rs = ps.executeQuery();
      if(rs.next()){
         razonsocial = rs.getString("cg_razon_social");
         rfc = rs.getString("cg_rfc");
      }
      rs.close();
      ps.close();
      
      jsonObj.put("grupo",       grupo);
      jsonObj.put("razonsocial", razonsocial);
      jsonObj.put("rfc",         rfc);
      jsonObj.put("calle_numero",calle_numero);
      jsonObj.put("comcat_pais", pais);
      jsonObj.put("colonia",     colonia);
      jsonObj.put("cp",          cp);
      jsonObj.put("telefono",    telefono);
      jsonObj.put("fax",         fax);
      jsonObj.put("email",       email);
      jsonObj.put("nombrec",     nombrec);
      jsonObj.put("telefonoc",   telefonoc);
      jsonObj.put("faxc",        faxc);
      jsonObj.put("emailc",      emailc);
      
      regresa = jsonObj.toString();
   }
   
   else if (   informacion.equals("CatalogoPais")    ){
      CatalogoSimple cat = new CatalogoSimple();
      cat.setTabla("comcat_pais");
      cat.setCampoClave("ic_pais");
      cat.setCampoDescripcion("cd_descripcion");
      cat.setOrden("ic_pais");
      regresa = cat.getJSONElementos();
   }
   
   else if (   informacion.equals("CatalogoEstado")    ){
      CatalogoEstado cat = new CatalogoEstado();
      cat.setCampoClave("ic_estado");
      cat.setCampoDescripcion("cd_nombre"); 
      cat.setClavePais(pais);
      cat.setOrden("ic_estado");
      regresa = cat.getJSONElementos();
   }
   
   else if (   informacion.equals("CatalogoDelegMunicipio")    ){
      CatalogoMunicipio cat = new CatalogoMunicipio();
      cat.setClave("ic_municipio");
      cat.setDescripcion("cd_nombre"); 
      cat.setPais(pais);
      cat.setEstado(estado);
      cat.setOrden("cd_nombre");
      
      List reg = cat.getListaElementos();
      JSONObject jsonObj = new JSONObject();
      jsonObj.put("success", new Boolean(true));
      jsonObj.put("registros", reg);
      regresa = jsonObj.toString();
   }

}catch(Exception e){

}finally{
	if (con.hayConexionAbierta()) {
		con.cierraConexionDB();
	}
}

%>

<%=regresa%>