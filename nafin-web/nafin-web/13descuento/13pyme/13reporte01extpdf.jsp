<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	com.netro.descuento.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
try {
	if(iNoEPO != null && !iNoEPO.equals("")) {
		ISeleccionDocumento beanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
		String sOperaFactConMandato = (request.getParameter("sOperaFactConMandato") == null) ? "" : request.getParameter("sOperaFactConMandato");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") ==null) ? "" : request.getParameter("tipoFactoraje");
		String mandant = (request.getParameter("mandant") == null) ? "" : request.getParameter("mandant");
		boolean bOperaFactConMandato					= false;
		boolean bOperaFactorajeVencido				= false;
		boolean bOperaFactorajeDistribuido			= false;
		boolean bOperaFactorajeNotaCredito			= false;
		boolean bOperaFactVencimientoInfonavit		= false;
		boolean bTipoFactoraje							= false;
		boolean bOperaFactorajeIF						= false;
		sOperaFactConMandato								= new String("");
		String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");
		sFechaVencPyme = beanSeleccionDocumento.operaFechaVencPyme(iNoEPO);
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		Hashtable alParamEPO = new Hashtable();
		alParamEPO = BeanParamDscto.getParametrosEPO(iNoEPO, 1);
		if (alParamEPO!=null) {
			bOperaFactConMandato				= ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
			bOperaFactorajeVencido			= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
			bOperaFactorajeDistribuido		= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
			bOperaFactorajeNotaCredito		= ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
			bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString())) ? false : true ;
			bOperaFactorajeIF = ("N".equals(alParamEPO.get("FACTORAJE_IF").toString())) ? false : true ;
		}
		bTipoFactoraje = ( bOperaFactorajeIF || bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaCredito || bOperaFactConMandato || bOperaFactVencimientoInfonavit)?true:false;
		sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
		bOperaFactorajeVencido = true;
		int ic_moneda = 0;
		String plazo = "";
		double fn_monto_dscto = 0;
		double in_importe_interes = 0;
		double in_importe_recibir = 0;
		double in_tasa_aceptada = 0;
		double fn_monto = 0;
		double dblPorciento = 0;
		double dblMontoDescuento = 0;
		double dblImporteAplicado = 0;
		String cg_razon_social = "";
		String cg_razon_social_if = "";
		String ig_numero_docto = "";
		String cc_acuse = "";
		String df_fecha_docto = "";
		String df_fecha_venc = "";
		String cd_nombre = "";
		String tipoFactorajeDesc="";
		boolean dolaresDocto = false;
		boolean nacionalDocto = false;
		String nombreIf = "", f_registro_operacion = "";
		String ig_numero_prestamo = "", netoRecibirPyme = "", nombreBeneficiario = "", porcBeneficiario = "", recibirBeneficiario = "";
		String lsNumeroPedido = "", lsNumPrestamoNafin = "", lsImporteAplicado = "", lsPorcentajeDocto = "", lsImporteDepositar = "";
		String cveEstatus = (request.getParameter("claveStatus")!=null)?request.getParameter("claveStatus"):"";
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);

		if (cveEstatus.equals("1")){		//-----[gridNegociable]
			Registros regNegociable = new Registros();
			int rowNegociable = 0;
			RepDoctosPymeDEbean repNegociable = new RepDoctosPymeDEbean();
			repNegociable.setIcestatusdocto("2");
			repNegociable.setIcepo(iNoEPO);
			repNegociable.setIcePyme(iNoCliente);
			repNegociable.setFechaVencPyme(sFechaVencPyme);
			repNegociable.setOperaFactConMandato(sOperaFactConMandato);
			repNegociable.setQrysentencia("");
			regNegociable = repNegociable.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/
			pdfDoc.addText("Estatus:		Negociable","formasB",ComunesPDF.LEFT);
			while (regNegociable.next()) {
				if(rowNegociable == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Num. de Documento");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Moneda");
					if(bTipoFactoraje) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					if(bOperaFactorajeVencido || bOperaFactorajeIF) {
						lEncabezados.add("Nombre IF");
					}
					if(bOperaFactorajeDistribuido  || bOperaFactVencimientoInfonavit) {
						lEncabezados.add("Beneficiario");
						lEncabezados.add("% Beneficiario");
						lEncabezados.add("Importe a Recibir Beneficiario");
					}
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				cg_razon_social 	=	regNegociable.getString(2).trim();
				ig_numero_docto 	=	regNegociable.getString("ig_numero_docto").trim();
				cc_acuse 			=	(regNegociable.getString("cc_acuse")==null)?"":regNegociable.getString("cc_acuse").trim(); //aqui new 16/12/2004
				df_fecha_docto 	= 	regNegociable.getString("FECHA_DOCTO").trim();
				df_fecha_venc 		= 	(regNegociable.getString("FECHA_VENC")==null)?"":regNegociable.getString("FECHA_VENC").trim();	
				cd_nombre 			=	regNegociable.getString("cd_nombre").trim();
				ic_moneda 			= 	Integer.parseInt(regNegociable.getString("ic_moneda"));
				fn_monto 			= 	Double.parseDouble(regNegociable.getString("fn_monto"));
				dblPorciento 		=	Double.parseDouble(regNegociable.getString("fn_aforo"));
				nombreBeneficiario	=	(regNegociable.getString("NOMBRE_BENEFICIARIO")==null)?"":regNegociable.getString("NOMBRE_BENEFICIARIO").trim();
				porcBeneficiario		=	(regNegociable.getString("FN_PORC_BENEFICIARIO")==null)?"":regNegociable.getString("FN_PORC_BENEFICIARIO").trim();
				recibirBeneficiario	=	(regNegociable.getString("RECIBIR_BENEFICIARIO")==null)?"":regNegociable.getString("RECIBIR_BENEFICIARIO").trim();
				String smandant 		=	"";
				if(bOperaFactConMandato) {
					smandant = (regNegociable.getString("mandante")==null)?"":regNegociable.getString("mandante").trim();
				}
				if(bTipoFactoraje) { // Para Factoraje Vencido o Distribuido
					tipoFactoraje = (regNegociable.getString("CS_DSCTO_ESPECIAL")==null)?"":regNegociable.getString("CS_DSCTO_ESPECIAL").trim();
					tipoFactorajeDesc = (regNegociable.getString("TIPO_FACTORAJE")==null)?"":regNegociable.getString("TIPO_FACTORAJE").trim();
					nombreIf = "";
					if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("C") || tipoFactoraje.equals("I") || tipoFactoraje.equals("A"))	{ //aqui
						 nombreIf = (regNegociable.getString("NOMBREIF")==null)?"":regNegociable.getString("NOMBREIF").trim();
						 dblMontoDescuento = fn_monto;
						 dblPorciento = 100;
					}
				}
				dblMontoDescuento = fn_monto * (dblPorciento / 100);
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bTipoFactoraje) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorciento,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
					pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
					pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2) + "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowNegociable++;
			} //fin del while
			if (rowNegociable == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("2")){		////-----[gridPignorado]
			Registros regPignorado = new Registros();
			int rowPignorado = 0;
			boolean mostrarOpcionPignorado=false;
			String infor = "";
			mostrarOpcionPignorado = BeanParamDscto.mostrarOpcionPignorado(iNoEPO, iNoCliente);
			if (mostrarOpcionPignorado)	{
				RepDoctosPymeDEbean repPignorado = new RepDoctosPymeDEbean();
				repPignorado.setIcestatusdocto("23");	//23
				repPignorado.setIcepo(iNoEPO);
				repPignorado.setIcePyme(iNoCliente);
				repPignorado.setQrysentencia("");
				repPignorado.setFechaVencPyme(sFechaVencPyme);
				regPignorado = repPignorado.executeQuery();
	/***************************/	/* Generacion del archivo */	/***************************/
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Estatus:		Proceso Ignorado","formasB",ComunesPDF.LEFT);
				while (regPignorado.next()) {
					if(rowPignorado == 0)	{
						List lEncabezados = new ArrayList();
						lEncabezados.add("Nombre EPO");
						lEncabezados.add("Nombre IF");
						lEncabezados.add("Num. de Documento");
						lEncabezados.add("Fecha Emisión");
						lEncabezados.add("Fecha Vencimiento");
						lEncabezados.add("Moneda");
						lEncabezados.add("Monto");
						lEncabezados.add("Porcentaje de Descuento");
						lEncabezados.add("Monto a Descontar");
						pdfDoc.setTable(lEncabezados,"formasmenB", 100);
					}
					nombreIf = "";
					cg_razon_social 	=	regPignorado.getString(2).trim();
					ig_numero_docto 	=	regPignorado.getString("ig_numero_docto").trim();
					cc_acuse 			=	(regPignorado.getString("cc_acuse")==null)?"":regPignorado.getString("cc_acuse").trim(); //aqui new 16/12/2004
					df_fecha_docto 	= 	regPignorado.getString("FECHA_DOCTO").trim();
					df_fecha_venc 		= 	(regPignorado.getString("FECHA_VENC")==null)?"":regPignorado.getString("FECHA_VENC").trim();	
					cd_nombre 			=	regPignorado.getString("cd_nombre").trim();
					ic_moneda 			= 	Integer.parseInt(regPignorado.getString("ic_moneda"));
					fn_monto 			= 	Double.parseDouble(regPignorado.getString("fn_monto"));
					dblPorciento 		=	Double.parseDouble(regPignorado.getString("fn_aforo"));
					nombreIf = (regPignorado.getString("NOMBREIF")==null)?"":regPignorado.getString("NOMBREIF").trim();
					if(bTipoFactoraje) {
						tipoFactoraje = (regPignorado.getString("CS_DSCTO_ESPECIAL")==null)?"":regPignorado.getString("CS_DSCTO_ESPECIAL").trim();
						tipoFactorajeDesc = (regPignorado.getString("TIPO_FACTORAJE")==null)?"":regPignorado.getString("TIPO_FACTORAJE").trim();
						if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("C") || tipoFactoraje.equals("I"))	{ //aqui
							dblMontoDescuento = fn_monto;
							dblPorciento = 100;
						}
					 }
					 dblMontoDescuento = fn_monto * (dblPorciento / 100);
	/*************************/ /* Contenido del archivo */ /**************************/
					pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(dblPorciento,0)+ "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
					rowPignorado++;
				}//fin de while()
				if (rowPignorado == 0)	{
					pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
					pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
				}else	{
					pdfDoc.addTable();
				}
			}//fin mostrarOpcionPignorado
		}else if (cveEstatus.equals("3")){		////-----[gridSelecPyme]
			Registros regSelecPyme = new Registros();
			RepDoctosPymeDEbean repSelecPyme = new RepDoctosPymeDEbean();
			repSelecPyme.setIcestatusdocto("3");	// == 3
			repSelecPyme.setIcepo(iNoEPO);
			repSelecPyme.setIcePyme(iNoCliente);
			repSelecPyme.setFechaVencPyme(sFechaVencPyme);
			repSelecPyme.setOperaFactConMandato(sOperaFactConMandato);
			repSelecPyme.setQrysentencia("");
			regSelecPyme = repSelecPyme.executeQuery();
			int rowSelecPyme = 0;
			/***************************/	/* Generacion del archivo */	/***************************/			
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Estatus:		Seleccionada PYME","formasB",ComunesPDF.LEFT);
			while (regSelecPyme.next()) {
				if(rowSelecPyme == 0)	{
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Num. de Documento");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Moneda");
					if(bTipoFactoraje) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto Documento");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Intermediario Financiero");
					lEncabezados.add("Tasa");
					lEncabezados.add("Plazo (días)");
					lEncabezados.add("Monto Int.");
					lEncabezados.add("Importe a Recibir");
					lEncabezados.add("Número de Acuse");
					if(bOperaFactorajeDistribuido  || bOperaFactVencimientoInfonavit) {
						lEncabezados.add("Neto a Recibir Pyme");
						lEncabezados.add("Beneficiario");
						lEncabezados.add("% Beneficiario");
						lEncabezados.add("Importe a Recibir Beneficiario");
					}
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				nombreIf = "";
				cg_razon_social 	=	regSelecPyme.getString(2).trim();
				ig_numero_docto 	=	regSelecPyme.getString("ig_numero_docto").trim();
				df_fecha_docto 	= 	regSelecPyme.getString("FECHA_DOCTO").trim();
				df_fecha_venc 		= 	(regSelecPyme.getString("FECHA_VENC")==null)?"":regSelecPyme.getString("FECHA_VENC").trim();
				nombreIf 			=	(regSelecPyme.getString("NOMBREIF")==null)?"":regSelecPyme.getString("NOMBREIF").trim();
				System.out.println("dddd---->>>>>>>>>>>"+regSelecPyme.getString("in_tasa_aceptada"));
				if(!regSelecPyme.getString("in_tasa_aceptada").equals("")){
				in_tasa_aceptada 	=	Double.parseDouble(regSelecPyme.getString("in_tasa_aceptada"));
				}else {
					in_tasa_aceptada=0;
				}
				plazo					=	regSelecPyme.getString("plazo");
				cd_nombre 			=	regSelecPyme.getString("cd_nombre").trim();
				ic_moneda 			= 	Integer.parseInt(regSelecPyme.getString("ic_moneda"));
				fn_monto 			= 	Double.parseDouble(regSelecPyme.getString("fn_monto"));
				if(!regSelecPyme.getString("fn_porc_anticipo").equals("")){
				dblPorciento 		=	Double.parseDouble(regSelecPyme.getString("fn_porc_anticipo"));
				}else {
					dblPorciento = 0;
				}
				if(!regSelecPyme.getString("fn_monto_dscto").equals("")){
					fn_monto_dscto		=	Double.parseDouble(regSelecPyme.getString("fn_monto_dscto"));
				}else {
					fn_monto_dscto=0;
				}
				if(!regSelecPyme.getString("in_importe_recibir").equals("")){
					in_importe_recibir=	Double.parseDouble(regSelecPyme.getString("in_importe_recibir"));
				}else {
					in_importe_recibir = 0;
				}
				cc_acuse 			=	(regSelecPyme.getString("cc_acuse")==null)?"":regSelecPyme.getString("cc_acuse").trim(); //aqui new 16/12/2004
				netoRecibirPyme	=	(regSelecPyme.getString("NETO_RECIBIR")==null)?"":regSelecPyme.getString("NETO_RECIBIR");
				nombreBeneficiario	=	(regSelecPyme.getString("NOMBRE_BENEFICIARIO")==null)?"":regSelecPyme.getString("NOMBRE_BENEFICIARIO").trim();
				porcBeneficiario		=	(regSelecPyme.getString("FN_PORC_BENEFICIARIO")==null)?"":regSelecPyme.getString("FN_PORC_BENEFICIARIO").trim();
				recibirBeneficiario	=	(regSelecPyme.getString("RECIBIR_BENEFICIARIO")==null)?"":regSelecPyme.getString("RECIBIR_BENEFICIARIO").trim();
				String smandant 		=	"";
				if(bOperaFactConMandato) {
					smandant = (regSelecPyme.getString("mandante")==null)?"":regSelecPyme.getString("mandante").trim();
				}
				if(bTipoFactoraje) { // Para Factoraje Vencido o Distribuido
					tipoFactoraje = (regSelecPyme.getString("CS_DSCTO_ESPECIAL")==null)?"":regSelecPyme.getString("CS_DSCTO_ESPECIAL").trim();
					tipoFactorajeDesc = (regSelecPyme.getString("TIPO_FACTORAJE")==null)?"":regSelecPyme.getString("TIPO_FACTORAJE").trim();
					//nombreIf = "";
					if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("C") || tipoFactoraje.equals("I"))	{ //aqui
						 fn_monto_dscto = fn_monto;
						 dblPorciento = 100;
					}
				}
			/*************************/ /* Contenido del archivo */ /**************************/				
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bTipoFactoraje) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorciento,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto_dscto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(in_tasa_aceptada,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(plazo,0), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(in_importe_interes,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(in_importe_recibir,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
					pdfDoc.setCell("$" + Comunes.formatoDecimal(netoRecibirPyme,2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2) + "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowSelecPyme++;
			}//fin del while()
			if (rowSelecPyme == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}

		}else if (cveEstatus.equals("4")){		////-----[gridAutorIF]
			Registros regAutorIF = new Registros();
			RepDoctosPymeDEbean repAutorIF = new RepDoctosPymeDEbean();
			repAutorIF.setIcestatusdocto("24");	//24
			repAutorIF.setIcepo(iNoEPO);
			repAutorIF.setIcePyme(iNoCliente);
			repAutorIF.setFechaVencPyme(sFechaVencPyme);
			repAutorIF.setOperaFactConMandato(sOperaFactConMandato);
			repAutorIF.setQrysentencia("");
			regAutorIF = repAutorIF.executeQuery();
			int rowAutorIF = 0;
			/***************************/	/* Generacion del archivo */	/***************************/			
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Estatus:		En Proceso de Autorización IF","formasB",ComunesPDF.LEFT);
			while (regAutorIF.next()) {
				if(rowAutorIF == 0)	{
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Num. de Documento");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Moneda");
					if(bTipoFactoraje) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto Documento");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Intermediario Financiero");
					lEncabezados.add("Tasa");
					lEncabezados.add("Plazo (días)");
					lEncabezados.add("Monto Int.");
					lEncabezados.add("Importe a Recibir");
					lEncabezados.add("Número de Acuse");
					if(bOperaFactorajeDistribuido  || bOperaFactVencimientoInfonavit) {
						lEncabezados.add("Neto a Recibir Pyme");
						lEncabezados.add("Beneficiario");
						lEncabezados.add("% Beneficiario");
						lEncabezados.add("Importe a Recibir Beneficiario");
					}
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				nombreIf = "";
				cg_razon_social 	=	regAutorIF.getString(2).trim();
				ig_numero_docto 	=	regAutorIF.getString("ig_numero_docto").trim();
				df_fecha_docto 	= 	regAutorIF.getString("FECHA_DOCTO").trim();
				df_fecha_venc 		= 	(regAutorIF.getString("FECHA_VENC")==null)?"":regAutorIF.getString("FECHA_VENC").trim();
				nombreIf 			=	(regAutorIF.getString("NOMBREIF")==null)?"":regAutorIF.getString("NOMBREIF").trim();
				in_tasa_aceptada 	=	Double.parseDouble(regAutorIF.getString("in_tasa_aceptada"));
				plazo					=	regAutorIF.getString("plazo");
				cd_nombre 			=	regAutorIF.getString("cd_nombre").trim();
				ic_moneda 			= 	Integer.parseInt(regAutorIF.getString("ic_moneda"));
				fn_monto 			= 	Double.parseDouble(regAutorIF.getString("fn_monto"));
				dblPorciento 		=	Double.parseDouble(regAutorIF.getString("fn_porc_anticipo"));
				fn_monto_dscto		=	Double.parseDouble(regAutorIF.getString("fn_monto_dscto"));
				in_importe_recibir=	Double.parseDouble(regAutorIF.getString("in_importe_recibir"));
				cc_acuse 			=	(regAutorIF.getString("cc_acuse")==null)?"":regAutorIF.getString("cc_acuse").trim(); //aqui new 16/12/2004
				netoRecibirPyme	=	(regAutorIF.getString("NETO_RECIBIR")==null)?"":regAutorIF.getString("NETO_RECIBIR");
				nombreBeneficiario	=	(regAutorIF.getString("NOMBRE_BENEFICIARIO")==null)?"":regAutorIF.getString("NOMBRE_BENEFICIARIO").trim();
				porcBeneficiario		=	(regAutorIF.getString("FN_PORC_BENEFICIARIO")==null)?"":regAutorIF.getString("FN_PORC_BENEFICIARIO").trim();
				recibirBeneficiario	=	(regAutorIF.getString("RECIBIR_BENEFICIARIO")==null)?"":regAutorIF.getString("RECIBIR_BENEFICIARIO").trim();
				String smandant 		=	"";
				if(bOperaFactConMandato) {
					smandant = (regAutorIF.getString("mandante")==null)?"":regAutorIF.getString("mandante").trim();
				}
				if(bTipoFactoraje) { // Para Factoraje Vencido o Distribuido
					tipoFactoraje = (regAutorIF.getString("CS_DSCTO_ESPECIAL")==null)?"":regAutorIF.getString("CS_DSCTO_ESPECIAL").trim();
					tipoFactorajeDesc = (regAutorIF.getString("TIPO_FACTORAJE")==null)?"":regAutorIF.getString("TIPO_FACTORAJE").trim();
					//nombreIf = "";
					if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("C") || tipoFactoraje.equals("I"))	{ //aqui
						 fn_monto_dscto = fn_monto;
						 dblPorciento = 100;
					}
				}
			/*************************/ /* Contenido del archivo */ /**************************/				
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bTipoFactoraje) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorciento,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto_dscto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(in_tasa_aceptada,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(plazo,0), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(in_importe_interes,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(in_importe_recibir,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
					pdfDoc.setCell("$" + Comunes.formatoDecimal(netoRecibirPyme,2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2) + "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowAutorIF++;
			}//fin del while()
			if (rowAutorIF == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("5"))	{			////-----[gridOperada]
			Registros regOperada = new Registros();
			RepDoctosPymeDEbean repOperada = new RepDoctosPymeDEbean();
			repOperada.setIcestatusdocto("4");		//4
			repOperada.setIcepo(iNoEPO);
			repOperada.setOperafactoraje(bOperaFactorajeNotaCredito);
			repOperada.setIcePyme(iNoCliente);
			repOperada.setFechaVencPyme(sFechaVencPyme);
			repOperada.setOperaFactConMandato(sOperaFactConMandato);
			repOperada.setQrysentencia("");
			regOperada = repOperada.executeQuery();
			int rowOperada = 0;
			/***************************/	/* Generacion del archivo */	/***************************/			
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Estatus:		Operada","formasB",ComunesPDF.LEFT);
			while (regOperada.next()) {
				if(rowOperada == 0)	{
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Num. de Documento");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Moneda");
					if(bTipoFactoraje) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto Documento");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Intermediario Financiero");
					lEncabezados.add("Tasa");
					lEncabezados.add("Plazo (días)");
					lEncabezados.add("Monto Int.");
					lEncabezados.add("Importe a Recibir");
					lEncabezados.add("Número de Acuse");
					if(bOperaFactorajeDistribuido  || bOperaFactVencimientoInfonavit) {
						lEncabezados.add("Neto a Recibir Pyme");
						lEncabezados.add("Beneficiario");
						lEncabezados.add("% Beneficiario");
						lEncabezados.add("Importe a Recibir Beneficiario");
					}
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				nombreIf = "";
				cg_razon_social 	=	regOperada.getString(2).trim();
				ig_numero_docto 	=	regOperada.getString("ig_numero_docto").trim();
				df_fecha_docto 	= 	regOperada.getString("FECHA_DOCTO").trim();
				df_fecha_venc 		= 	(regOperada.getString("FECHA_VENC")==null)?"":regOperada.getString("FECHA_VENC").trim();
				nombreIf 			=	(regOperada.getString("NOMBREIF")==null)?"":regOperada.getString("NOMBREIF").trim();
				in_tasa_aceptada 	=	Double.parseDouble(regOperada.getString("in_tasa_aceptada"));
				plazo					=	regOperada.getString("plazo");
				cd_nombre 			=	regOperada.getString("cd_nombre").trim();
				ic_moneda 			= 	Integer.parseInt(regOperada.getString("ic_moneda"));
				fn_monto 			= 	Double.parseDouble(regOperada.getString("fn_monto"));
				dblPorciento 		=	Double.parseDouble(regOperada.getString("fn_porc_anticipo"));
				fn_monto_dscto		=	Double.parseDouble(regOperada.getString("fn_monto_dscto"));
				in_importe_recibir=	Double.parseDouble(regOperada.getString("in_importe_recibir"));
				cc_acuse 			=	(regOperada.getString("cc_acuse")==null)?"":regOperada.getString("cc_acuse").trim(); //aqui new 16/12/2004
				netoRecibirPyme	=	(regOperada.getString("NETO_RECIBIR")==null)?"":regOperada.getString("NETO_RECIBIR");
				nombreBeneficiario	=	(regOperada.getString("NOMBRE_BENEFICIARIO")==null)?"":regOperada.getString("NOMBRE_BENEFICIARIO").trim();
				porcBeneficiario		=	(regOperada.getString("FN_PORC_BENEFICIARIO")==null)?"":regOperada.getString("FN_PORC_BENEFICIARIO").trim();
				recibirBeneficiario	=	(regOperada.getString("RECIBIR_BENEFICIARIO")==null)?"":regOperada.getString("RECIBIR_BENEFICIARIO").trim();
				String smandant 		=	"";
				if(bOperaFactConMandato) {
					smandant = (regOperada.getString("mandante")==null)?"":regOperada.getString("mandante").trim();
				}
				if(bTipoFactoraje) { // Para Factoraje Vencido o Distribuido
					tipoFactoraje = (regOperada.getString("CS_DSCTO_ESPECIAL")==null)?"":regOperada.getString("CS_DSCTO_ESPECIAL").trim();
					tipoFactorajeDesc = (regOperada.getString("TIPO_FACTORAJE")==null)?"":regOperada.getString("TIPO_FACTORAJE").trim();
					//nombreIf = "";
					if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("C") || tipoFactoraje.equals("I"))	{ //aqui
						 fn_monto_dscto = fn_monto;
						 dblPorciento = 100;
					}
				}
			/*************************/ /* Contenido del archivo */ /**************************/				
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bTipoFactoraje) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorciento,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto_dscto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(in_tasa_aceptada,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(plazo,0), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(in_importe_interes,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(in_importe_recibir,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
					pdfDoc.setCell("$" + Comunes.formatoDecimal(netoRecibirPyme,2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2) + "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowOperada++;
			}//fin del while()
			if (rowOperada == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("6"))	{			//-----[INIT-gridAcredito]
			String ig_tipo_piso="",fn_remanente="";
			String ic_estatus_docto = null;
			String fecha_operacion_if = null;
			Registros regAcredito = new Registros();
			RepDoctosPymeDEbean repAcredito = new RepDoctosPymeDEbean();
			repAcredito.setIcestatusdocto("16");		//16
			repAcredito.setIcepo(iNoEPO);
			repAcredito.setIcePyme(iNoCliente);
			repAcredito.setFechaVencPyme(sFechaVencPyme);
			repAcredito.setOperaFactConMandato(sOperaFactConMandato);
			repAcredito.setQrysentencia("");
			regAcredito = repAcredito.executeQuery();
			int rowAcredito = 0;
			/***************************/	/* Generacion del archivo */	/***************************/			
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Estatus:		Aplicado a Credito","formasB",ComunesPDF.LEFT);
			while (regAcredito.next()) {
				if(rowAcredito == 0)	{
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Nombre IF");
					lEncabezados.add("Número de Documento");
					lEncabezados.add("Número de Acuse");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Plazo");
					lEncabezados.add("Moneda");
					if(bTipoFactoraje) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto Documento");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Intereses del documento");
					lEncabezados.add("Monto a Recibir");
					lEncabezados.add("Tasa");
					lEncabezados.add("Estatus");
					lEncabezados.add("Fecha Operación IF");
					lEncabezados.add("Importe Aplicado a Crédito");
					lEncabezados.add("Porcentaje del Documento Aplicado");
					lEncabezados.add("Importe a Depositar PyME");
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				nombreIf = "";
				cg_razon_social 	=	regAcredito.getString(2).trim();
				ig_numero_docto 	=	regAcredito.getString("ig_numero_docto").trim();
				df_fecha_docto 	= 	regAcredito.getString("FECHA_DOCTO").trim();
				df_fecha_venc 		= 	(regAcredito.getString("FECHA_VENC")==null)?"":regAcredito.getString("FECHA_VENC").trim();
				nombreIf 			=	(regAcredito.getString("NOMBREIF")==null)?"":regAcredito.getString("NOMBREIF").trim();
				in_tasa_aceptada 	=	Double.parseDouble(regAcredito.getString("in_tasa_aceptada"));
				plazo					=	regAcredito.getString("plazo");
				cd_nombre 			=	regAcredito.getString("cd_nombre").trim();
				ic_moneda 			= 	Integer.parseInt(regAcredito.getString("ic_moneda"));
				fn_monto 			= 	Double.parseDouble(regAcredito.getString("fn_monto"));
				dblPorciento 		=	Double.parseDouble(regAcredito.getString("fn_porc_anticipo"));
				fn_monto_dscto		=	Double.parseDouble(regAcredito.getString("fn_monto_dscto"));
				in_importe_interes=	Double.parseDouble(regAcredito.getString("in_importe_intereses"));
				in_importe_recibir=	Double.parseDouble(regAcredito.getString("in_importe_recibir"));
				cc_acuse 			=	(regAcredito.getString("cc_acuse")==null)?"":regAcredito.getString("cc_acuse").trim(); //aqui new 16/12/2004
				ic_estatus_docto	=	regAcredito.getString("ESTATUSDOCUMENTO");
				fecha_operacion_if=	regAcredito.getString("DF_FECHA_SOLICITUD");
				dblImporteAplicado=	Double.parseDouble(regAcredito.getString("FN_MONTO_PAGO"));
				lsPorcentajeDocto	=	(regAcredito.getString("PORCDOCTOAPLICADO")==null)?"":regAcredito.getString("PORCDOCTOAPLICADO");
				ig_tipo_piso		=	(regAcredito.getString("ig_tipo_piso")==null)?"":regAcredito.getString("ig_tipo_piso");
				fn_remanente		=	(regAcredito.getString("fn_remanente")==null)?"":regAcredito.getString("fn_remanente");
				String smandant 		=	"";
				if(bOperaFactConMandato) {
					smandant = (regAcredito.getString("mandante")==null)?"":regAcredito.getString("mandante").trim();
				}
				if (in_importe_recibir - dblImporteAplicado > 0 ) {
					lsImporteDepositar = String.valueOf(in_importe_recibir - dblImporteAplicado);
				} else {
					lsImporteDepositar = "0";
				}
				if(bTipoFactoraje) { // Para Factoraje Vencido o Distribuido
					tipoFactoraje = (regAcredito.getString("CS_DSCTO_ESPECIAL")==null)?"":regAcredito.getString("CS_DSCTO_ESPECIAL").trim();
					tipoFactorajeDesc = (regAcredito.getString("TIPO_FACTORAJE")==null)?"":regAcredito.getString("TIPO_FACTORAJE").trim();
					//nombreIf = "";
					if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("C") || tipoFactoraje.equals("I"))	{ //aqui
						 fn_monto_dscto = fn_monto;
						 dblPorciento = 100;
					}
				}
			/*************************/ /* Contenido del archivo */ /**************************/				
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(plazo,0), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bTipoFactoraje) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorciento,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto_dscto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(in_importe_interes,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(in_importe_recibir,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(in_tasa_aceptada,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ic_estatus_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(fecha_operacion_if, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(dblImporteAplicado,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(lsPorcentajeDocto, "formasmen", ComunesPDF.CENTER);
				if (ig_tipo_piso.equals("1"))	{
					if (!fn_remanente.equals(""))	{
						pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_remanente,2), "formasmen", ComunesPDF.CENTER);
					}else	{
						pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER);
					}
				}else	{
					pdfDoc.setCell("$" + Comunes.formatoDecimal(lsImporteDepositar,2), "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowAcredito++;
			}//fin del while()
			if (rowAcredito == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("7"))	{				//-----[gridProgramado]
			Registros regProgramado = new Registros();
			RepDoctosPymeDEbean repProgram = new RepDoctosPymeDEbean();
			repProgram.setIcestatusdocto("26");	//26
			repProgram.setIcepo(iNoEPO);
			repProgram.setIcePyme(iNoCliente);
			repProgram.setFechaVencPyme(sFechaVencPyme);
			repProgram.setOperaFactConMandato(sOperaFactConMandato);
			repProgram.setQrysentencia("");
			regProgramado = repProgram.executeQuery();
			int rowProgramado = 0;
			/***************************/	/* Generacion del archivo */	/***************************/			
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Estatus:		Programado PyME","formasB",ComunesPDF.LEFT);
			while (regProgramado.next()) {
				if(rowProgramado == 0)	{
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Num. de Documento");
					lEncabezados.add("Fecha Emisión");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Moneda");
					if(bTipoFactoraje) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto Documento");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Intermediario Financiero");
					lEncabezados.add("Tasa");
					lEncabezados.add("Plazo (días)");
					lEncabezados.add("Monto Int.");
					lEncabezados.add("Importe a Recibir");
					lEncabezados.add("Número de Acuse");
					if(bOperaFactorajeDistribuido  || bOperaFactVencimientoInfonavit) {
						lEncabezados.add("Neto a Recibir Pyme");
						lEncabezados.add("Beneficiario");
						lEncabezados.add("% Beneficiario");
						lEncabezados.add("Importe a Recibir Beneficiario");
					}
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					lEncabezados.add("Fecha Registro Operación");
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				nombreIf = "";
				cg_razon_social 	=	regProgramado.getString(2).trim();
				ig_numero_docto 	=	regProgramado.getString("ig_numero_docto").trim();
				df_fecha_docto 	= 	regProgramado.getString("FECHA_DOCTO").trim();
				df_fecha_venc 		= 	(regProgramado.getString("FECHA_VENC")==null)?"":regProgramado.getString("FECHA_VENC").trim();
				nombreIf 			=	(regProgramado.getString("NOMBREIF")==null)?"":regProgramado.getString("NOMBREIF").trim();
				in_tasa_aceptada 	=	Double.parseDouble(regProgramado.getString("in_tasa_aceptada"));
				plazo					=	regProgramado.getString("plazo");
				cd_nombre 			=	regProgramado.getString("cd_nombre").trim();
				ic_moneda 			= 	Integer.parseInt(regProgramado.getString("ic_moneda"));
				fn_monto 			= 	Double.parseDouble(regProgramado.getString("fn_monto"));
				dblPorciento 		=	Double.parseDouble(regProgramado.getString("fn_porc_anticipo"));
				fn_monto_dscto		=	Double.parseDouble(regProgramado.getString("fn_monto_dscto"));
				in_importe_recibir=	Double.parseDouble(regProgramado.getString("in_importe_recibir"));
				cc_acuse 			=	(regProgramado.getString("cc_acuse")==null)?"":regProgramado.getString("cc_acuse").trim(); //aqui new 16/12/2004
				netoRecibirPyme	=	(regProgramado.getString("NETO_RECIBIR")==null)?"":regProgramado.getString("NETO_RECIBIR");
				nombreBeneficiario	=	(regProgramado.getString("NOMBRE_BENEFICIARIO")==null)?"":regProgramado.getString("NOMBRE_BENEFICIARIO").trim();
				porcBeneficiario		=	(regProgramado.getString("FN_PORC_BENEFICIARIO")==null)?"":regProgramado.getString("FN_PORC_BENEFICIARIO").trim();
				recibirBeneficiario	=	(regProgramado.getString("RECIBIR_BENEFICIARIO")==null)?"":regProgramado.getString("RECIBIR_BENEFICIARIO").trim();
				f_registro_operacion	=	(regProgramado.getString("df_programacion")==null)?"":regProgramado.getString("df_programacion").trim();	 // Fodea 005-2009 24 hrs
				String smandant 		=	"";
				if(bOperaFactConMandato) {
					smandant = (regProgramado.getString("mandante")==null)?"":regProgramado.getString("mandante").trim();
				}
				if(bTipoFactoraje) { // Para Factoraje Vencido o Distribuido
					tipoFactoraje = (regProgramado.getString("CS_DSCTO_ESPECIAL")==null)?"":regProgramado.getString("CS_DSCTO_ESPECIAL").trim();
					tipoFactorajeDesc = (regProgramado.getString("TIPO_FACTORAJE")==null)?"":regProgramado.getString("TIPO_FACTORAJE").trim();
					//nombreIf = "";
					if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("C") || tipoFactoraje.equals("I"))	{ //aqui
						 dblMontoDescuento = fn_monto;
						 dblPorciento = 100;
					}
				}
			/*************************/ /* Contenido del archivo */ /**************************/				
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bTipoFactoraje) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorciento,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto_dscto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(in_tasa_aceptada,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(plazo,0), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(in_importe_interes,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(in_importe_recibir,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
					pdfDoc.setCell("$" + Comunes.formatoDecimal(netoRecibirPyme,2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2) + "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell(f_registro_operacion, "formasmen", ComunesPDF.CENTER);
				rowProgramado++;
			}//fin del while()
			if (rowProgramado == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}

		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>