var myData = [];
var numberTry = -1;
var texto1 = '<div  class="titulos" align="center" style="font-weight:bold;">' +
					'DESCUENTO&nbsp;AUTOM&Aacute;TICO&nbsp;DE&nbsp;DOCUMENTOS' +
				'</div>' + 
				'<div  class="subtitulos" align="center" style="float:center;font-weight:bold;">' +
					'Proceso&nbsp;de&nbsp;Afiliaci&oacute;n' +
				'</div>' ;
var texto2 = '<br/><div class="formas" style="text-align:justify">' +
				'<b>T&eacute;rminos y Condiciones:</b><br>' +
				'Autorizo expresamente que todos los DOCUMENTOS que me sean publicados en las CADENAS PRODUCTIVAS con quienes opero Factoraje Electr&oacute;nico, me sean descontados autom&aacute;ticamente en la modalidad &ldquo;Primer D&iacute;a de Publicaci&oacute;n&rdquo; con el INTERMEDIARIO FINANCIERO en el orden ' +
				'que a continuaci&oacute;n se&ntilde;alo y que los recursos que se generen por dicho descuento me sean depositados en la cuenta que tengo habilitada para dicho fin, en el entendido de que en cualquier momento podr&eacute; dar de baja la operaci&oacute;n del factoraje autom&aacute;tico.<br>' +
				'&nbsp;<br>' +
				'<b>Bancos con quienes tiene cuenta bancaria autorizada (seleccione el orden de operaci&oacute;n):</b><br>' +
				'&nbsp;<br>' +
			'<div align="left">'+
				'<table cellpadding="4" cellspacing="0" border="0" >'+
					'<tr>'+
						'<td class="formas" style="text-align:left;font-weight:bold;padding-right:330px;">'+
							'IF'+
						'</td>'+
						'<td class="formas" style="text-align:left;font-weight:bold;padding-right:75px;">'+
							'Fecha y Hora de  Liberaci&oacute;n'+
						'</td>'+
						'<td class="formas" style="text-align:center;font-weight:bold;">'+
							'Orden'+
						'</td>'+
					'</tr>'+
				'</table>'+
			'</div>'+
				'</div>';

var valoresIniciales = null;
Ext.onReady(function() {

	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			valoresIniciales = Ext.util.JSON.decode(response.responseText);
			var panelIzq = Ext.getCmp('panelIzq');
			var panelCen = Ext.getCmp('panelCen');
			var panelDer = Ext.getCmp('panelDer');
			var panelHid1 = Ext.getCmp('panelHidden1');
			var panelHid2 = Ext.getCmp('panelHidden2');
			var panelAba = Ext.getCmp('panelAbajo');
			var contrato = valoresIniciales.CONTRATO_ACEPTADO;
			var nombre_pyme = valoresIniciales.nombrePyme;
			var ifs = valoresIniciales.listaIntermediariosFinancieros;
			var descAtm = valoresIniciales.descuentoAutomaticoRechazado;
			var btnContinuar = Ext.getCmp('btnContinuar');
			var tBar = Ext.getCmp('tbarFp');

			if (ifs != undefined && ifs != null && ifs.length > 0){
				var storeCombo = listaPosicionData;
				var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
				var orden = 1;
				Ext.each(ifs,function(item, index, arrItems){
					if (item.ORDEN != ""){
						storeCombo.add(new reg({clave: item.ORDEN,descripcion: item.ORDEN,loadMsg: null}));
					}else{
						storeCombo.add(new reg({clave: orden,descripcion: orden,loadMsg: null}));
					}
					orden ++;
				});
				Ext.each(ifs,function(item, index, arrItems){
					var configIf = 	{xtype: 'displayfield',	name: 'ifs'+index,id: 'ifs'+index,	value: item.NOMBRE_IF, anchor: '100%'};
					var configFecha = {xtype: 'displayfield',	name: 'fec'+index,id: 'fec'+index,	value: item.FECHA_Y_HORA_LIBERACION, anchor: '100%'};
					var configCombo = {xtype: 'combo',name: 'orden_de_intermediarios'+index,id: 'orden_de_intermediarios'+index,
											mode: 'local',	
											displayField: 'descripcion', 
											anchor: '80%', 
											maxlength: 1,
											valueField: 'clave',
											emptyText: 'Seleccionar. . .',
											hiddenName : 'orden_de_intermediarios'+index,
											forceSelection : true,
											typeAhead: true, 
											allowBlank: false,
											editable:false,
											triggerAction : 'all',
											minChars : 1,
											tpl : NE.util.templateMensajeCargaCombo,
											store: listaPosicionData,
											value: item.ORDEN};
					var configHiddenIf=	{xtype:	'hidden', name:'clave_de_intermediarios'+index, id: 'claveIf'+index,value: item.IC_IF}
					var configHiddenCta=	{xtype:	'hidden', name:'cuentas_bancarias'+index, id: 'claveCue'+index,		value: item.CUENTA_BANCARIA}
					panelIzq.insert(index+1,configIf);
					panelCen.insert(index+1,configFecha);
					panelDer.insert(index+1,configCombo);
					panelHid1.insert(index+1,configHiddenIf);
					panelHid2.insert(index+1,configHiddenCta);
				});
				if (contrato == false){
					var configCheck =	{xtype: 'checkboxgroup', id: 'cbg', name: 'cbg', columns: 1,items: [{xtype: 'checkbox', boxLabel: 'Acepto t&eacute;rminos y condiciones', name:'contrato_aceptado',id:'contrato_aceptado'}]}
					panelAba.insert(0,configCheck);
					panelAba.doLayout();
					tBar.hide();
				} else {
					if (descAtm != undefined){
						if (descAtm == "true"){
							tBar.hide();
						} else {
							tBar.show();
						}
					}
					var checkCon = Ext.getCmp('cbg');
					if (checkCon){
						panelAba.remove(checkCon);
					}
				}
				btnContinuar.show();
			}else{
				var config = {xtype: 'displayfield', value: '<font color=red>No se encontr� ning�n registro.</font>'}
				panelIzq.insert(1,config);
			}
			if (nombre_pyme != undefined){
				var configPyme = {xtype: 'displayfield', id: 'nombre_pyme', name: 'nombre_pyme',anchor:'100%',html: '<div  align="left" style="font-weight:bold;color:#003399;">'+nombre_pyme+'</div>'}
				panelAba.insert(1,configPyme);
				panelAba.doLayout();
			}
			panelIzq.doLayout();
			panelCen.doLayout();
			panelDer.doLayout();
			fp.getForm().reset();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesaRechazaDescAtm(opts, success, response) {
		var tBar = Ext.getCmp('tbarFp');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			fp.el.unmask();
			Ext.Msg.show({	msg: 'El Descuento Autom�tico ha sido desactivado.',buttons: Ext.Msg.OK,fn: processResultDesc,animEl: 'elId',icon: Ext.MessageBox.INFO});
		}else{
			tBar.show();
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesaInsertaOrden(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			Ext.Msg.show({	msg: 'Su informaci�n fue actualizada con �xito',buttons: Ext.Msg.OK,	fn: processResultIns,animEl: 'elId',icon: Ext.MessageBox.INFO});
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	function procesaConfirmaClave(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			fpConfirma.el.unmask();
			var infoR = Ext.util.JSON.decode(response.responseText);
			if (infoR != undefined){
				if (infoR.resultado != 'true'){
					if (numberTry > 1){
						numberTry = -1;
						Ext.Msg.show({	msg: 'La confirmaci&oacute;n de la clave y contrase&ntilde;a es incorrecta, el proceso ha sido cancelado.',buttons: Ext.Msg.OK,	animEl: 'elId',icon: Ext.MessageBox.ERROR});
						window.location = '13seleccionOrden_ext.jsp';
					}else	{
						numberTry ++;
						Ext.getCmp('lblConfirma').show();
					}
				} else {
					Ext.Ajax.request({
						url:'13seleccionOrden_ext.data.jsp',
						params: {
							informacion: 'insertaOrden',
							listaIntermediarios: Ext.encode(myData)
						},
						callback: procesaInsertaOrden
					});
				}
			} 
		} else {
			NE.util.mostrarConnError(response, opts);
		}
	}

	var listaPosicionData = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion']
	});

	var consultaData = new Ext.data.JsonStore({
		fields:[	{name: 'NOMBRE_IF'},
					{name: 'ORDEN'},
					{name: 'FECHA_Y_HORA_LIBERACION'},
					{name: 'CUENTA_BANCARIA'},
					{name: 'CLAVE'}
		],
		data:	[{'NOMBRE_IF':'','ORDEN':'0'}],
		autoload:true,
		sortInfo:{field: 'ORDEN', direction: "ASC"}, //Ordena por defecto la fila ORDEN -- ASC ascendentemente � DESC descendentemente  
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var elementosGrid = [
		{
		xtype: 'grid',
		store: consultaData,
		hidden: false,
		columns: [
			{
				header: '<p align="center">Intermediario Financiero</p>', tooltip: 'Intermediario Financiero',
				dataIndex: 'NOMBRE_IF',
				sortable: false,	width: 383,	resizable: true, hidden: false
			},
			{
				header: 'Orden', tooltip: 'Orden',
				dataIndex: 'ORDEN',
				sortable: false,	width: 110,	resizable: false, align: 'center', hidden: false
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 200,
		width: 500,
		//title: '<div><center>Usted seleccion� al (los) INTERMEDIARIO(S) FINANCIERO(S) en el siguiente orden:</div>',
		columnLines : true,
		frame: false,
		bbar: {
			xtype: 'toolbar',
			buttonAlign: 'center',
			id: 'barra',
			items: [
				{
					xtype: 'button',
					text: 'Aceptar',
					id: 'btnAceptar',
					iconCls: 'correcto',
					handler: function(boton, evento) {
						var ventana = Ext.getCmp('winConfirma');
						if (ventana) {
							ventana.show();
						}else{
							new Ext.Window({
								modal: true,
								resizable: false,
								layout: 'form',
								width: 255,
								height: 160,
								x:300,
								y:100,
								id: 'winConfirma',
								closeAction: 'hide',
								items: [fpConfirma],
								bodyStyle: 'text-align:center',
								title: 'Confirmaci�n'
							}).show();//.el.dom.scrollIntoView();
						}
						Ext.getCmp('lblConfirma').hide();
						fpConfirma.getForm().reset();
					}
				},'-',{
					xtype: 'button',
					text: 'Modificar',
					id: 'btnModifica',
					iconCls: 'modificar',
					handler: function(boton, evento) {
						fpGrid.hide();
						fp.show();
					}
				}
			]
		}
		}
	];	

	var fpGrid	=	new Ext.FormPanel({
		height:'auto',
		width: 600,
		style: ' margin:0 auto;',
		bodyStyle: 'padding-left:50px;padding-top: 15px',
		title: '<div><center>Usted seleccion� al (los) INTERMEDIARIO(S) FINANCIERO(S) en el siguiente orden:</div>',
		border: false,
		frame: false,
		items: elementosGrid,
		hidden: true
	});

	var elementosForma = [
		{	xtype: 'panel',
			width:	940,
			border: false,
			frame: false,
			hidden:	false,
			html:	texto1
		},{
			xtype: 'panel',
			border: false,
			frame: false,
			width:	890,
			hidden:	false,
			html:	texto2
		},{
			xtype: 'container',
			layout: 'table',
			defaults:	{bodyStyle: 'padding: 6px',msgTarget: 'side',anchor: '-30'},
			layoutConfig:	{columns: 5},
			width:	940,
			hidden:	false,
			items:[
				{
					xtype: 'panel',
					id:	'panelIzq',
					colspan: 1,
					layout: 'form',
					width:	350,
					hidden:	false,
					border: false
				},{
					xtype: 'panel',
					id:	'panelCen',
					colspan: 1,
					layout: 'form',
					width:	200,
					hidden:	false,
					border: false
				},{
					xtype: 'panel',
					id:	'panelDer',
					colspan: 1,
					layout: 'form',
					defaults: {
						msgTarget: 'side',
						anchor: '-20'
					},
					width:	180,
					hidden:	false,
					border: false
				},{
					xtype: 'panel',
					id: 'panelHidden1',
					colspan: 1,
					width: 5,
					layout:'form',
					frame:false,
					border:false
				},{
					xtype: 'panel',
					id: 'panelHidden2',
					colspan: 1,
					width: 5,
					layout:'form',
					frame:false,
					border:false
				},{
					xtype: 'panel',
					id: 'panelAbajo',
					rowspan: 5,
					layout:'form',
					defaults: {
						msgTarget: 'side',
						anchor: '-20'
					},
					labelWidth: 10,
					frame:false,
					border:false
				}
			]
		}
	];

	var fpConfirma = new Ext.form.FormPanel({
			id: 'formLogin',
			width: 250,
			height:125,
			frame: true,
			border: true,
			monitorValid: true,
			//buttonAlign: 'center',
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-20'
			},
			items : [
				{	fieldLabel: 'Clave Usuario',
					name:'cesionUser',
					id: 'cesionUser1',
					//style:'padding:4px 3px;',
					enableKeyEvents: true,
					allowBlank: false,
					maxLength : 8
				},{
					fieldLabel: 'Contrase�a',
					name: 'cesionPassword',
					id: 'cesionPassword1', //campo password   
					inputType: 'password', // el tipo de Input Password ( los caracteres del campo apareceran ocultos)   
					//style:'padding:4px 3px;',
					maxLength : 128,
					blankText: 'Digite su contrase�a',
					allowBlank: false
				},{
					xtype: 'panel', html:'<font color=red>La Clave y/o Contrase�a no son validos.</font>', id:'lblConfirma', hidden:true, border: false, frame:false
				}
			],
			buttons:[{
						text: 'Continuar',id: 'btnLoginAcept', iconCls: 'enviar', hidden:false,
						handler: function() {
							if(!verificaPanel('formLogin')) {
								return;
							}
							Ext.getCmp('lblConfirma').hide();
							fpConfirma.el.mask('Procesando...', 'x-mask-loading');
							Ext.Ajax.request({
								url:'13seleccionOrden_ext.data.jsp',
								params: Ext.apply(fpConfirma.getForm().getValues(),{informacion: 'confirmaClave'}),
								callback: procesaConfirmaClave
							});
						}
					}]
	});

	function processResultIns(){
		if (valoresIniciales.esNAFIN == true){
			Ext.Msg.alert('Redireccion de p�gina', 'Aqui se direcciona a la pagina de NAFIN');
		}else if (valoresIniciales.esPYME == true){
			window.location = '13seleccionOrden_ext.jsp';
		}
	}

	function processResultDesc(){
		if (valoresIniciales.esNAFIN == true){
			Ext.Msg.alert('Redireccion de p�gina', 'Aqui se direcciona a la pagina de NAFIN');
		}else if (valoresIniciales.esPYME == true){
			window.location = '13seleccionOrden_ext.jsp';
		}
	}

	function processResult(res){
		if (res == 'ok'){
			fp.el.mask('Procesando...', 'x-mask-loading');
			Ext.Ajax.request({
				url: '13seleccionOrden_ext.data.jsp',
				params: {
					informacion: "rechazaDescuentoAutomatico"
				},
				callback: procesaRechazaDescAtm
			});
		}
	}

	function verificaChecks(){
		var myPanel = Ext.getCmp('panelAbajo');
		var valid = false;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype == 'checkboxgroup')	{	//El metodo isValid() no aplica para los tipos de objetos label.
				var cbGroup = panelItem;
				cbGroup.items.each(function(check){
					if (check.checked == true)	{
						valid = true;
						return true;
					}
				});
			}
		});
		return valid;
	}

	function verificaPanel(panel){
		var myPanel = Ext.getCmp(panel);
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (panelItem.xtype != 'panel') {
				if (!panelItem.isValid())	{
					valid = false;
					return false;
				}
			}
		});
		return valid;
	}

	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 940,
		title:	'',
		hidden: false,
		frame: true,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 1,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		tbar:{
			xtype: 'toolbar',
			id: 'tbarFp',
			hidden: true,
			items: ['->',	{text:'<p align="center">Desactivar&nbsp;Descuento&nbsp;Autom&aacute;tico</p>', 
								handler: function(){
												Ext.Msg.show({
													//title:'�Desea deshabilitar el descuento autom�tico?',
													msg: '�Desea deshabilitar el descuento autom�tico?',
													buttons: Ext.Msg.OKCANCEL,
													fn: processResult,
													animEl: 'elId',
													icon: Ext.MessageBox.QUESTION
												});
											}
								}
			]
		},
		monitorValid: false,
		buttons: [{text: 'Continuar', id: 'btnContinuar',iconCls:'icoContinuar', hidden: true,
					handler: function() {
						var panelIzq = Ext.getCmp('panelIzq');
						var panelCen = Ext.getCmp('panelCen');
						var panelDer = Ext.getCmp('panelDer');
						var panelHid1 = Ext.getCmp('panelHidden1');
						var panelHid2 = Ext.getCmp('panelHidden2');
						var panelAba = Ext.getCmp('panelAbajo');
						if(!verificaPanel('panelDer')) {
							return;
						}
						var cbg = Ext.getCmp('cbg');
						if (cbg != undefined){
							if(!verificaChecks()) {
								cbg.markInvalid('Se requiere que se acepten los t�rminos y condiciones.');
								panelAba.el.dom.scrollIntoView();
								return;
							}
						}

						var valid = true;
						panelDer.items.each(function(panelItem, index, totalCount){
							var valCombo = panelItem.value;
							var valorCombo;
							panelDer.items.each(function(item, _index, _totalCount){
								if (index != _index){
									valorCombo = item.value;
								}
								if (valCombo == valorCombo){
									panelItem.markInvalid('No se puede repetir el Orden de Operaci�n');
									panelItem.focus();
									valid = false;
									return false;
								}
							});
							if (!valid){
							return false;
							}
						});
						if (!valid){
						return;
						}
						var nuevo_orden = "";
						myData = [];
						panelDer.items.each(function(itemD, indexD, totalCountD){
							nuevo_orden = itemD.value;
							var fechaLibera = "";
							var nombre_if = "";
							var claveIf = "";
							var cuenta = "";
							/*panelCen.items.each(function(itemC, indexC, totalCountC){
								if (indexD == indexC){
									fechaLibera = itemC.value;
									return;
								}
							});*/
							panelIzq.items.each(function(itemI, indexI, totalCountI){
								if (indexD == indexI){
									nombre_if = itemI.value;
									return;
								}
							});
							panelHid1.items.each(function(itemH1, indexH1, totalCountH1){
								if (indexD == indexH1){
									claveIf = itemH1.initialConfig.value;
									return;
								}
							});
							panelHid2.items.each(function(itemH2, indexH2, totalCountH2){
								if (indexD == indexH2){
									cuenta = itemH2.initialConfig.value;
									return;
								}
							});
						//myData.push({'ORDEN':nuevo_orden,'CLAVE':claveIf,'NOMBRE_IF':nombre_if,'FECHA_Y_HORA_LIBERACION':fechaLibera,'CUENTA_BANCARIA':cuenta});
						myData.push({'CLAVE':claveIf,'ORDEN':nuevo_orden.toString(),'NOMBRE_IF':nombre_if,'CUENTA_BANCARIA':cuenta});
						});
						consultaData.loadData(myData);
						if (!fpGrid.isVisible()){
							fpGrid.show();
						}
						
						fp.hide();
					}
			}]
		});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,fpGrid,
			NE.util.getEspaciador(10)
		]
	});

	Ext.Ajax.request({
		url: '13seleccionOrden_ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});	
});
