Ext.onReady(function() {

	var jsonValoresIniciales = null;
	var cveEstatus = "";
	
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
				var cmbStatus = Ext.getCmp('cmbStatus');
				if (jsonValoresIniciales.mostrarOpcionPignorado == false)	{
					cmbStatus.store.remove(cmbStatus.store.getAt(5));		//Oculta Pignorado Negociable
				}
				if (!fp.isVisible())	{
					fp.show();
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
//Termina valoresIniciales.

//--------Handlers---------
	var procesarSuccessFailureGenerarPdfDscto =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfDscto');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfDscto');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var procesarSuccessFailureGenerarPdfBaja =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfBaja');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfBaja');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfAnticipa =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfAnticipa');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfAnticipa');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfPymeN =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfPymeN');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfPymeN');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfIgnoPyme =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfIgnoPyme');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfIgnoPyme');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfIgnoNego =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfIgnoNego');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfIgnoNego');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfModif =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfModif');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfModif');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfNota =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfNota');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfNota');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfProgra =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfProgra');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfProgra');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaDescuento = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfDscto');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfDscto');
			if (!gridDescuento.isVisible()) {
				gridDescuento.show();
			}
			var el = gridDescuento.getGridEl();

			var cm = gridDescuento.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			cm.setHidden(10, true);		//cm.setHidden(cm.findColumnIndex('NOMBREIF'), true);
			cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
			
			if(store.getTotalCount() > 0)	{
				if (jsonValoresIniciales.bTipoFactoraje)	{
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				if (jsonValoresIniciales.bOperaFactorajeVencido  ||  jsonValoresIniciales.bOperaFactorajeIF )	{
					cm.setHidden(10, false);		//10 = cm.findColumnIndex('NOMBREIF')
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido  || jsonValoresIniciales.bOperaFactVencimientoInfonavit)	{
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var procesarConsultaBaja = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfBaja');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfBaja');
			if (!gridBaja.isVisible()) {
				gridBaja.show();
			}
			var el = gridBaja.getGridEl();

			var cm = gridBaja.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			cm.setHidden(10, true);		// 10 = cm.findColumnIndex('NOMBREIF')
			cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
			
			if(store.getTotalCount() > 0)	{
				if (jsonValoresIniciales.bTipoFactoraje)	{
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				if (jsonValoresIniciales.bOperaFactorajeVencido  ||  jsonValoresIniciales.bOperaFactorajeIF)	{
					cm.setHidden(10, false);		// 10 = cm.findColumnIndex('NOMBREIF')
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido  || jsonValoresIniciales.bOperaFactVencimientoInfonavit)	{
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaAnticipa = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfAnticipa');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfAnticipa');
			if (!gridAnticipa.isVisible()) {
				gridAnticipa.show();
			}
			var el = gridAnticipa.getGridEl();

			var cm = gridAnticipa.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			cm.setHidden(10, true);		// 10 = cm.findColumnIndex('NOMBREIF')
			cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
			
			if(store.getTotalCount() > 0)	{
				if (jsonValoresIniciales.bTipoFactoraje)	{
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				if (jsonValoresIniciales.bOperaFactorajeVencido  ||  jsonValoresIniciales.bOperaFactorajeIF )	{
					cm.setHidden(10, false);		// 10 = cm.findColumnIndex('NOMBREIF')
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido  || jsonValoresIniciales.bOperaFactVencimientoInfonavit)	{
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaPymeNego = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfPymeN');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfPymeN');
			if (!gridPymeNego.isVisible()) {
				gridPymeNego.show();
			}
			var el = gridPymeNego.getGridEl();

			var cm = gridPymeNego.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			cm.setHidden(10, true);		// 10 = cm.findColumnIndex('NOMBREIF')
			cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
			
			if(store.getTotalCount() > 0)	{
				if (jsonValoresIniciales.bTipoFactoraje)	{
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				if (jsonValoresIniciales.bOperaFactorajeVencido ||  jsonValoresIniciales.bOperaFactorajeIF)	{
					cm.setHidden(10, false);		// 10 = cm.findColumnIndex('NOMBREIF')
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido  || jsonValoresIniciales.bOperaFactVencimientoInfonavit)	{
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaIgnoraPyme = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfIgnoPyme');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfIgnoPyme');
			if (!gridIgnoraPyme.isVisible()) {
				gridIgnoraPyme.show();
			}
			var el = gridIgnoraPyme.getGridEl();

			var cm = gridIgnoraPyme.getColumnModel();
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
			
			if(store.getTotalCount() > 0)	{
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaIgnoraNego = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfIgnoNego');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfIgnoNego');
			if (jsonData.mostrarOpcionPignorado != undefined){
				mostrarOpcionPignorado = jsonData.mostrarOpcionPignorado;
				if (jsonData.mostrarOpcionPignorado)	{
					if (!gridIgnoraNego.isVisible()) {gridIgnoraNego.show();}
				}else	{
					if (gridIgnoraNego.isVisible())	{gridIgnoraNego.hide();}
				}
				var el = gridIgnoraNego.getGridEl();
				
				if(store.getTotalCount() > 0) {
					if (!btnGenerarPDF.isVisible()) {
						btnGenerarPDF.show();
					}
					btnGenerarPDF.enable();
					el.unmask();
				} else {
					btnGenerarPDF.disable();
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
	}

	var procesarConsultaModifica = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfModif');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfModif');
			if (!gridModifica.isVisible()) {
				gridModifica.show();
			}
			var el = gridModifica.getGridEl();

			var cm = gridModifica.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			cm.setHidden(13, true);		// 13 = cm.findColumnIndex('NOMBREIF')
			cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
			
			if(store.getTotalCount() > 0)	{
				if (jsonValoresIniciales.bTipoFactoraje)	{
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				if (jsonValoresIniciales.bOperaFactorajeVencido ||  jsonValoresIniciales.bOperaFactorajeIF)	{
					cm.setHidden(13, false);	// 13 = cm.findColumnIndex('NOMBREIF')
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido  || jsonValoresIniciales.bOperaFactVencimientoInfonavit)	{
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaNotaCredito = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfNota');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfNota');
			if (!gridNotaCredito.isVisible()) {
				gridNotaCredito.show();
			}
			var el = gridNotaCredito.getGridEl();

			var cm = gridNotaCredito.getColumnModel();
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
			
			if(store.getTotalCount() > 0)	{
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaProgramado = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfProgra');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfProgra');
			if (!gridProgramado.isVisible()) {
				gridProgramado.show();
			}
			var el = gridProgramado.getGridEl();

			var cm = gridProgramado.getColumnModel();
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
			
			if(store.getTotalCount() > 0)	{
				if (jsonValoresIniciales.bTipoFactoraje)	{
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
//Fin de handlers.------

//---------------------------store--------------------------

	var consultaDescuento = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte02ext.data.jsp',
		baseParams: {
			informacion: 'dataDescuento'
		},
		fields: [
			{name: 'NOMBREEPO'},                   					 		//Nom. EPO
			{name: 'IG_NUMERO_DOCTO'},              							//Num docto.
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},                   							 //Moneda
			{name: 'TIPO_FACTORAJE'},           								 //Tipo factoraje	
			{name: 'FN_MONTO', type: 'float'},
			//{name: 'dblPorcentaje'},														//CALCULADO
			//{name: 'dblMontoDescuento', type: 'float'},							//CALCULADO
			{name: 'CC_ACUSE'},
			{name: 'NOMBREIF'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'IC_MONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDescuento,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDescuento(null, null, null);
				}
			}
		}
	});

	var consultaBaja = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte02ext.data.jsp',
		baseParams: {
			informacion: 'dataBaja'
		},
		fields: [
			{name: 'NOMBREEPO'},                   					 		//Nom. EPO
			{name: 'IG_NUMERO_DOCTO'},              							//Num docto.
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},                   							 //Moneda
			{name: 'TIPO_FACTORAJE'},           								 //Tipo factoraje	
			{name: 'FN_MONTO', type: 'float'},
			//{name: 'dblPorcentaje'},														//CALCULADO
			//{name: 'dblMontoDescuento', type: 'float'},							//CALCULADO
			{name: 'CC_ACUSE'},
			{name: 'NOMBREIF'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'IC_MONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaBaja,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaBaja(null, null, null);
				}
			}
		}
	});

	var consultaAnticipa = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte02ext.data.jsp',
		baseParams: {
			informacion: 'dataPagadoAnticipado'
		},
		fields: [
			{name: 'NOMBREEPO'},                   					 		//Nom. EPO
			{name: 'IG_NUMERO_DOCTO'},              							//Num docto.
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},                   							 //Moneda
			{name: 'TIPO_FACTORAJE'},           								 //Tipo factoraje	
			{name: 'FN_MONTO', type: 'float'},
			//{name: 'dblPorcentaje'},														//CALCULADO
			//{name: 'dblMontoDescuento', type: 'float'},							//CALCULADO
			{name: 'CC_ACUSE'},
			{name: 'NOMBREIF'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'IC_MONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaAnticipa,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaAnticipa(null, null, null);
				}
			}
		}
	});

	var consultaPymeNego = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte02ext.data.jsp',
		baseParams: {
			informacion: 'dataSelecPymeNegociable'
		},
		fields: [
			{name: 'NOMBREEPO'},                   					 		//Nom. EPO
			{name: 'IG_NUMERO_DOCTO'},              							//Num docto.
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},                   							 //Moneda
			{name: 'TIPO_FACTORAJE'},           								 //Tipo factoraje	
			{name: 'FN_MONTO', type: 'float'},
			//{name: 'dblPorcentaje'},														//CALCULADO
			//{name: 'dblMontoDescuento', type: 'float'},							//CALCULADO
			{name: 'CC_ACUSE'},
			{name: 'NOMBREIF'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'IC_MONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaPymeNego,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaPymeNego(null, null, null);
				}
			}
		}
	});

	var consultaIgnoraPyme = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte02ext.data.jsp',
		baseParams: {
			informacion: 'dataPignoradaSelecPyme'
		},
		fields: [
			{name: 'NOMBREEPO'},                    //NOMBRE. EPO
			{name: 'IG_NUMERO_DOCTO'},              //Num docto.
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},                    								//Moneda
			{name: 'TIPO_FACTORAJE'},            									//Tipo factoraje	
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_PORC_ANTICIPO',	type: 'float'},							//Porcentaje
			{name: 'FN_MONTO_DSCTO', type: 'float'},								//MONTO_DSCTO
			{name: 'NOMBREIF'},
			{name: 'IN_TASA_ACEPTADA', type: 'float'},
			{name: 'PLAZO', type: 'integer'},
			{name: 'IN_IMPORTE_INTERES', type: 'float'},
			{name: 'IN_IMPORTE_RECIBIR', type: 'float'},
			{name: 'CC_ACUSE'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'IC_MONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaIgnoraPyme,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaIgnoraPyme(null, null, null);
				}
			}
		}
	});

	var consultaIgnoraNego = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte02ext.data.jsp',
		baseParams: {
			informacion: 'dataPignoradaNegociable'
		},
		fields: [
			{name: 'NOMBREEPO'},                    //NOMBRE. EPO
			{name: 'IG_NUMERO_DOCTO'},              //Num docto.
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},                    								//Moneda
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_PORC_ANTICIPO',	type: 'float'},							//Porcentaje
			{name: 'FN_MONTO_DSCTO', type: 'float'},								//MONTO_DSCTO
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'IC_MONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaIgnoraNego,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaIgnoraNego(null, null, null);
				}
			}
		}
	});

	var consultaModifica = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte02ext.data.jsp',
		baseParams: {
			informacion: 'dataModificaImporte'
		},
		fields: [
			{name: 'NOMBREEPO'},                   					 		//Nom. EPO
			{name: 'IG_NUMERO_DOCTO'},              							//Num docto.
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},                   							 //Moneda
			{name: 'TIPO_FACTORAJE'},           								 //Tipo factoraje	
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_MONTO_ANTERIOR', type: 'float'},
			{name: 'FECHAVENCANT', type: 'date', dateFormat: 'd/m/Y'}, 
			//{name: 'dblPorcentaje'},														//CALCULADO
			//{name: 'dblMontoDescuento', type: 'float'},							//CALCULADO
			{name: 'CC_ACUSE'},
			{name: 'CT_CAMBIO_MOTIVO'},
			{name: 'NOMBREIF'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'IC_MONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaModifica,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaModifica(null, null, null);
				}
			}
		}
	});

	var consultaNotaCredito = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte02ext.data.jsp',
		baseParams: {
			informacion: 'dataAplicaNotaCredito'
		},
		fields: [
			{name: 'NOMBREEPO'},                   					 		//Nom. EPO
			{name: 'IG_NUMERO_DOCTO'},              							//Num docto.
			{name: 'CC_ACUSE'},
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'TIPO_FACTORAJE'},           								 //Tipo factoraje	
			{name: 'CD_NOMBRE'},                   							 //Moneda
			{name: 'FN_MONTO_ANTERIOR', type: 'float'},
			{name: 'FN_MONTO_NUEVO', type: 'float'},
			//{name: 'dblPorcentaje'},														//CALCULADO
			//{name: 'dblMontoDescuento', type: 'float'},							//CALCULADO
			{name: 'CT_CAMBIO_MOTIVO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'IC_MONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaNotaCredito,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaNotaCredito(null, null, null);
				}
			}
		}
	});

//	consultaProgramado
	var consultaProgramado = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte02ext.data.jsp',
		baseParams: {
			informacion: 'dataProgramado'
		},
		fields: [
			{name: 'NOMBREEPO'},                   					 		//Nom. EPO
			{name: 'IG_NUMERO_DOCTO'},              							//Num docto.
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},                   							 //Moneda
			{name: 'TIPO_FACTORAJE'},           								 //Tipo factoraje	
			{name: 'FN_MONTO', type: 'float'},
			//{name: 'dblPorcentaje'},														//CALCULADO
			//{name: 'dblMontoDescuento', type: 'float'},							//CALCULADO
			{name: 'CC_ACUSE'},
			{name: 'CT_CAMBIO_MOTIVO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'IC_MONEDA'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaProgramado,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaProgramado(null, null, null);
				}
			}
		}
	});

//----------------------------------Contenido----------------------------------

	var gridDescuento = new Ext.grid.GridPanel({
		store: consultaDescuento,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 120
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	//5
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',	//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				hidden: false,	align: 'right', 
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//7
				//dataIndex : '',		//CALCULADO
				width : 100, hideable: true, hidden: false,	align: 'right', 
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if(registro.get('IC_MONEDA') == 1)	{value = (jsonValoresIniciales.strAforo * 100);}
								if(registro.get('IC_MONEDA') == 54)	{value = (jsonValoresIniciales.strAforoDL * 100);}
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	//8
				//dataIndex : '',		//CALCULADO
				sortable : true,
				width : 120,
				hidden: false,	align: 'right', 
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){ 
									var porcentaje = 0;
									if(registro.get('IC_MONEDA') == 1)	{porcentaje = (jsonValoresIniciales.strAforo * 100);}
									if(registro.get('IC_MONEDA') == 54)	{porcentaje = (jsonValoresIniciales.strAforoDL * 100);}
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											var dato = registro.get('CS_DSCTO_ESPECIAL');
											if(dato == "V")	{porcentaje = 100;}
											if(dato == "D")	{porcentaje = 100;}
											if(dato == "C")	{porcentaje = 100;}
											if(dato == "I")	{porcentaje = 100;}
										}
									}
									value = (registro.get('FN_MONTO') * (porcentaje / 100));
									return Ext.util.Format.number(value, '$0,0.00');	
								}
			},
			{
				header : 'N�mero de Acuse', tooltip: 'N�mero de Acuse',	//9
				dataIndex : 'CC_ACUSE',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header: 'Nombre IF', tooltip: 'Nombre IF',	//10
				//dataIndex: 'NOMBREIF',	//Calculado
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: true,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								value = "";
								var jsonData = store.reader.jsonData;
								if (jsonData.bTipoFactoraje != undefined){
									if (jsonData.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "A")	{value = registro.get('NOMBREIF');}
									}
								}
								return value;
							}
			},
			{
				header: 'Beneficiario', tooltip: 'Beneficiario', 	//11
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true//false
			},
			{
				header: '% Beneficiario', tooltip: '% Beneficiario', //12
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	width: 100,	resizable: true, hideable: true, align: 'right',
				hidden: true,	renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario', 	//13
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true//false
			},
			{
				header: 'Mandante', tooltip: 'Mandante', 	//14
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true//false
			}
		],
		stripeRows: false,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Descuento f�sico',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfDscto',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfDscto
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfDscto',
					hidden: true
				}
			]
		}
	});

	var gridBaja = new Ext.grid.GridPanel({
		store: consultaBaja,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 120
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	//5
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',	//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//7
				//dataIndex : '',			//CALCULADO
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if(registro.get('IC_MONEDA') == 1)	{value = (jsonValoresIniciales.strAforo * 100);}
								if(registro.get('IC_MONEDA') == 54)	{value = (jsonValoresIniciales.strAforoDL * 100);}
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	//8
				//dataIndex : '',		//CALCULADO
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){ 
									var porcentaje = 0;
									if(registro.get('IC_MONEDA') == 1)	{porcentaje = (jsonValoresIniciales.strAforo * 100);}
									if(registro.get('IC_MONEDA') == 54)	{porcentaje = (jsonValoresIniciales.strAforoDL * 100);}
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											var dato = registro.get('CS_DSCTO_ESPECIAL');
											if(dato == "V")	{porcentaje = 100;}
											if(dato == "D")	{porcentaje = 100;}
											if(dato == "C")	{porcentaje = 100;}
											if(dato == "I")	{porcentaje = 100;}
										}
									}
									value = (registro.get('FN_MONTO') * (porcentaje / 100));
									return Ext.util.Format.number(value, '$0,0.00');	
								}
			},
			{
				header : 'N�mero de Acuse', tooltip: 'N�mero de Acuse',	//9
				dataIndex : 'CC_ACUSE',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header: 'Nombre IF', tooltip: 'Nombre IF',	//10
				//dataIndex: 'NOMBREIF',	//Calculado
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: true,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								value = "";
								var jsonData = store.reader.jsonData;
								if (jsonData.bTipoFactoraje != undefined){
									if (jsonData.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "A")	{value = registro.get('NOMBREIF');}
									}
								}
								return value;
							}
			},
			{
				header: 'Beneficiario', tooltip: 'Beneficiario', 	//11
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true//false
			},
			{
				header: '% Beneficiario', tooltip: '% Beneficiario', //12
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	width: 100,	resizable: true, hideable: true, align: 'right',
				hidden: true,	renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario', 	//13
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true//false
			},
			{
				header: 'Mandante', tooltip: 'Mandante', 	//14
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true//false
			}
		],
		stripeRows: false,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Baja',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfBaja',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfBaja
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfBaja',
					hidden: true
				}
			]
		}
	});

	var gridAnticipa = new Ext.grid.GridPanel({
		store: consultaAnticipa,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 120
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	//5
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',							//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//7
				//dataIndex : '',		//CALCULADO
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if(registro.get('IC_MONEDA') == 1)	{value = (jsonValoresIniciales.strAforo * 100);}
								if(registro.get('IC_MONEDA') == 54)	{value = (jsonValoresIniciales.strAforoDL * 100);}
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',				//8
				//dataIndex : '',		//CALCULADO
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){ 
									var porcentaje = 0;
									if(registro.get('IC_MONEDA') == 1)	{porcentaje = (jsonValoresIniciales.strAforo * 100);}
									if(registro.get('IC_MONEDA') == 54)	{porcentaje = (jsonValoresIniciales.strAforoDL * 100);}
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											var dato = registro.get('CS_DSCTO_ESPECIAL');
											if(dato == "V")	{porcentaje = 100;}
											if(dato == "D")	{porcentaje = 100;}
											if(dato == "C")	{porcentaje = 100;}
											if(dato == "I")	{porcentaje = 100;}
										}
									}
									value = (registro.get('FN_MONTO') * (porcentaje / 100));
									return Ext.util.Format.number(value, '$0,0.00');	
								}
			},
			{
				header : 'N�mero de Acuse', tooltip: 'N�mero de Acuse',					//9
				dataIndex : 'CC_ACUSE',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header: 'Nombre IF', tooltip: 'Nombre IF',									//10
				//dataIndex: 'NOMBREIF',	//Calculado
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: true,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								value = "";
								var jsonData = store.reader.jsonData;
								if (jsonData.bTipoFactoraje != undefined){
									if (jsonData.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "A")	{value = registro.get('NOMBREIF');}
									}
								}
								return value;
							}
			},
			{
				header: 'Beneficiario', tooltip: 'Beneficiario', 							//11
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true//false
			},
			{
				header: '% Beneficiario', tooltip: '% Beneficiario', 						//12
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	width: 100,	resizable: true, hideable: true, align: 'right',
				hidden: true,	renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario', 	//13
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true//false
			},
			{
				header: 'Mandante', tooltip: 'Mandante', 	//14
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true//false
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Pagado Anticipado',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfAnticipa',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfAnticipa
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfAnticipa',
					hidden: true
				}
			]
		}
	});

	var gridPymeNego = new Ext.grid.GridPanel({
		store: consultaPymeNego,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 120
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	//5
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',	//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//7
				//dataIndex : '',		//CALCULADO
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if(registro.get('IC_MONEDA') == 1)	{value = (jsonValoresIniciales.strAforo * 100);}
								if(registro.get('IC_MONEDA') == 54)	{value = (jsonValoresIniciales.strAforoDL * 100);}
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	//8
				//dataIndex : '',		//CALCULADO
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){ 
									var porcentaje = 0;
									if(registro.get('IC_MONEDA') == 1)	{porcentaje = (jsonValoresIniciales.strAforo * 100);}
									if(registro.get('IC_MONEDA') == 54)	{porcentaje = (jsonValoresIniciales.strAforoDL * 100);}
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											var dato = registro.get('CS_DSCTO_ESPECIAL');
											if(dato == "V")	{porcentaje = 100;}
											if(dato == "D")	{porcentaje = 100;}
											if(dato == "C")	{porcentaje = 100;}
											if(dato == "I")	{porcentaje = 100;}
										}
									}
									value = (registro.get('FN_MONTO') * (porcentaje / 100));
									return Ext.util.Format.number(value, '$0,0.00');	
								}
			},
			{
				header : 'N�mero de Acuse', tooltip: 'N�mero de Acuse',	//9
				dataIndex : 'CC_ACUSE',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header: 'Nombre IF', tooltip: 'Nombre IF',	//10
				//dataIndex: 'NOMBREIF',	//Calculado
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: true,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								value = "";
								var jsonData = store.reader.jsonData;
								if (jsonData.bTipoFactoraje != undefined){
									if (jsonData.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "A")	{value = registro.get('NOMBREIF');}
									}
								}
								return value;
							}
			},
			{
				header: 'Beneficiario', tooltip: 'Beneficiario', 	//11
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true//false
			},
			{
				header: '% Beneficiario', tooltip: '% Beneficiario', //12
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	width: 100,	resizable: true, hideable: true, align: 'right',
				hidden: true,	renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario', 	//13
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true//false
			},
			{
				header: 'Mandante', tooltip: 'Mandante', 	//14
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true//false
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Seleccionada PYME a Negociable',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfPymeN',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfPymeN
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfPymeN',
					hidden: true
				}
			]
		}
	});

	var gridIgnoraPyme = new Ext.grid.GridPanel({
		store: consultaIgnoraPyme,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 120
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',	//5
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//6
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: Ext.util.Format.numberRenderer('0%')
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',						//7
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero',				//8
				dataIndex: 'NOMBREIF',
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Tasa', tooltip: 'Tasa',														//9
				dataIndex : 'IN_TASA_ACEPTADA',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
				//renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo (d�as)', tooltip: 'Plazo (d�as)',									//10
				dataIndex : 'PLAZO',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
			},
			{
				header : 'Monto Int.', tooltip: 'Monto Int.',										//11
				dataIndex : 'IN_IMPORTE_INTERES',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Importe a Recibir', tooltip: 'Importe a Recibir',							//12
				dataIndex : 'IN_IMPORTE_RECIBIR',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Causa', tooltip: 'Causa',																//13
				dataIndex: 'CT_CAMBIO_MOTIVO',
				sortable: true,	width: 250,	resizable: true, hidden: false
			},
			{
				header: 'Mandante', tooltip: 'Mandante',																	//14
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Pignorado a Seleccionada PYME',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfIgnoPyme',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfIgnoPyme
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfIgnoPyme',
					hidden: true
				}
			]
		}
	});

	var gridIgnoraNego = new Ext.grid.GridPanel({
		store: consultaIgnoraNego,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 120
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',	//5
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//6
				//dataIndex : '',		//CALCULADO
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if(registro.get('IC_MONEDA') == 1)	{value = (jsonValoresIniciales.strAforo * 100);}
								if(registro.get('IC_MONEDA') == 54)	{value = (jsonValoresIniciales.strAforoDL * 100);}
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',						//7
				//dataIndex : '',		//CALCULADO
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){ 
									var porcentaje = 0;
									if(registro.get('IC_MONEDA') == 1)	{porcentaje = (jsonValoresIniciales.strAforo * 100);}
									if(registro.get('IC_MONEDA') == 54)	{porcentaje = (jsonValoresIniciales.strAforoDL * 100);}
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											var dato = registro.get('CS_DSCTO_ESPECIAL');
											if(dato == "V")	{porcentaje = 100;}
											if(dato == "D")	{porcentaje = 100;}
											if(dato == "C")	{porcentaje = 100;}
											if(dato == "I")	{porcentaje = 100;}
										}
									}
									value = (registro.get('FN_MONTO') * (porcentaje / 100));
									return Ext.util.Format.number(value, '$0,0.00');	
								}
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Pignorado a Negociable',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfIgnoNego',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfIgnoNego
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfIgnoNego',
					hidden: true
				}
			]
		}
	});


	var gridModifica = new Ext.grid.GridPanel({
		store: consultaModifica,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',									//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',									//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',							//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',																//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 120
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',											//5
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto Documento', tooltip: 'Monto del Documento',									//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto Anterior', tooltip: 'Monto Anterior',									//7
				dataIndex : 'FN_MONTO_ANTERIOR',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Fecha de Vencimiento Anterior', tooltip: 'Fecha de Vencimiento Anterior',							//8
				dataIndex : 'FECHAVENCANT',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento',					//9
				//dataIndex : '',		//CALCULADO
				width : 100, align: 'right', hideable: true, hidden: false,	sortable : true,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								value = 0;
								if(registro.get('IC_MONEDA') == 1)	{value = (jsonValoresIniciales.strAforo * 100);}
								if(registro.get('IC_MONEDA') == 54)	{value = (jsonValoresIniciales.strAforoDL * 100);}
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',									//10
				//dataIndex : '',		//CALCULADO
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){ 
									var porcentaje = 0;
									if(registro.get('IC_MONEDA') == 1)	{porcentaje = (jsonValoresIniciales.strAforo * 100);}
									if(registro.get('IC_MONEDA') == 54)	{porcentaje = (jsonValoresIniciales.strAforoDL * 100);}
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											var dato = registro.get('CS_DSCTO_ESPECIAL');
											if(dato == "V")	{porcentaje = 100;}
											if(dato == "D")	{porcentaje = 100;}
											if(dato == "C")	{porcentaje = 100;}
											if(dato == "I")	{porcentaje = 100;}
										}
									}
									value = (registro.get('FN_MONTO') * (porcentaje / 100));
									return Ext.util.Format.number(value, '$0,0.00');	
								}
			},
			{
				header: 'N�mero de Acuse', tooltip: 'N�mero de Acuse',																//11
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 150,	resizable: true, hidden: true
			},
			{
				header: 'Causa', tooltip: 'Causa',																							//12
				dataIndex: 'CT_CAMBIO_MOTIVO',
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header: 'Nombre IF', tooltip: 'Nombre IF',																				//13
				//dataIndex: 'NOMBREIF',	//Calculado
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: true,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								value = "";
								var jsonData = store.reader.jsonData;
								if (jsonData.bTipoFactoraje != undefined){
									if (jsonData.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "A")	{value = registro.get('NOMBREIF');}
									}
								}
								return value;
							}
			},
			{
				header: 'Beneficiario', tooltip: 'Beneficiario', 													//14
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true//false
			},
			{
				header: '% Beneficiario', tooltip: '% Beneficiario', 												//15
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	width: 100,	resizable: true, hideable: true, align: 'right',
				hidden: true,	renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario', 	//16
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true//false
			},
			{
				header: 'Mandante', tooltip: 'Mandante', 																//17
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true//false
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Modificaci�n de montos y/o Fechas de Vencimiento',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfModif',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfModif
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfModif',
					hidden: true
				}
			]
		}
	});

	var gridNotaCredito = new Ext.grid.GridPanel({
		store: consultaNotaCredito,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',									//2
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header: 'N�mero de Acuse', tooltip: 'N�mero de Acuse',										//3
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 120,	resizable: true, hideable : true ,hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',									//4
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',							//5
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',											//6
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',																//7
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 120
			},
			{
				header : 'Monto Anterior', tooltip: 'Monto Anterior',											//8
				dataIndex : 'FN_MONTO_ANTERIOR',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto Nuevo', tooltip: 'Monto Nuevo',													//9
				dataIndex : 'FN_MONTO_NUEVO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento',					//10
				//dataIndex : '',		//CALCULADO
				width : 100, align: 'right', hideable: true, hidden: false,	sortable : true,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								value = 0;
								if(registro.get('IC_MONEDA') == 1)	{value = (jsonValoresIniciales.strAforo * 100);}
								if(registro.get('IC_MONEDA') == 54)	{value = (jsonValoresIniciales.strAforoDL * 100);}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',									//11
				//dataIndex : '',		//CALCULADO
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){ 
									var porcentaje = 0;
									if(registro.get('IC_MONEDA') == 1)	{porcentaje = (jsonValoresIniciales.strAforo * 100);}
									if(registro.get('IC_MONEDA') == 54)	{porcentaje = (jsonValoresIniciales.strAforoDL * 100);}
									value = (registro.get('FN_MONTO_NUEVO') * (porcentaje / 100));
									return Ext.util.Format.number(value, '$0,0.00');	
								}
			},
			{
				header: 'Causa', tooltip: 'Causa',																//12
				dataIndex: 'CT_CAMBIO_MOTIVO',
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header: 'Mandante', tooltip: 'Mandante',																	//13
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Aplicaci�n de Nota de Cr�dito',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfNota',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfNota
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfNota',
					hidden: true
				}
			]
		}
	});


	var gridProgramado = new Ext.grid.GridPanel({
		store: consultaProgramado,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',				//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 120,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',				//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',		//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',											//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 120
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',						//5
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',							//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,			//7
				//dataIndex : '',		//CALCULADO
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								value = 0;
								if(registro.get('IC_MONEDA') == 1)	{value = (jsonValoresIniciales.strAforo * 100);}
								if(registro.get('IC_MONEDA') == 54)	{value = (jsonValoresIniciales.strAforoDL * 100);}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',													//8
				//dataIndex : '',		//CALCULADO
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){ 
									var porcentaje = 0;
									if(registro.get('IC_MONEDA') == 1)	{porcentaje = (jsonValoresIniciales.strAforo * 100);}
									if(registro.get('IC_MONEDA') == 54)	{porcentaje = (jsonValoresIniciales.strAforoDL * 100);}
									value = (registro.get('FN_MONTO') * (porcentaje / 100));
									return Ext.util.Format.number(value, '$0,0.00');	
								}
			},
			{
				header: 'N�mero de Acuse', tooltip: 'N�mero de Acuse',														//9
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 120,	resizable: true, hideable : true ,hidden: false
			},
			{
				header: 'Causa', tooltip: 'Causa',																					//10
				dataIndex: 'CT_CAMBIO_MOTIVO',
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header: 'Mandante', tooltip: 'Mandante',																			//11
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Programado a Negociable',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfProgra',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte02extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfProgra
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfProgra',
					hidden: true
				}
			]
		}
	});
// Fin de declaraci�n de Grid�s

//Panel de valores iniciales
	var dataStatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			['1','Descuento f�sico'],
			['2','Baja'],
			['3','Pagado Anticipado'],
			['4','Seleccionada PYME a Negociable'],
			['5','Pignorado a Seleccionada PYME'],
			['6','Pignorado a Negociable'],
			['7','Modificaci�n de Montos y/o Fechas de Vencimiento'],
			['8','Aplicaci�n de Nota de Cr�dito'],
			['9','Programado a Negociable']
		 ]
	});
	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'status',	
			id: 'cmbStatus',	
			fieldLabel: 'Estatus', 
			mode: 'local',	
			hiddenName : 'status',	
			emptyText: 'Seleccione Estatus . . . ',
			forceSelection : true,	triggerAction : 'all',	typeAhead: true,
			minChars : 1,	
			store : dataStatus,	
			displayField : 'descripcion',	valueField : 'clave',
			listeners: {
				select: {
					fn: function(combo) {
						cveEstatus = combo.getValue();
						gridDescuento.hide();
						gridBaja.hide();
						gridAnticipa.hide();
						gridPymeNego.hide();
						gridIgnoraPyme.hide();
						gridIgnoraNego.hide();
						gridModifica.hide();
						gridNotaCredito.hide();
						gridProgramado.hide();
						fp.el.mask('Enviando...', 'x-mask-loading');

						if (combo.value == "1")	{
							gridDescuento.setTitle('<div><div style="float:left">Estatus: Descuento f�sico</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaDescuento.load();
						}
						if (combo.value == "2")	{
							gridBaja.setTitle('<div><div style="float:left">Estatus: Baja</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');						
							consultaBaja.load();
						}
						if (combo.value == "3")	{
							gridAnticipa.setTitle('<div><div style="float:left">Estatus: Pagado Anticipado</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaAnticipa.load();
						}
						if (combo.value == "4")	{
							gridPymeNego.setTitle('<div><div style="float:left">Estatus: Seleccionada PYME a Negociable</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaPymeNego.load();
						}
						if (combo.value == "5")	{
							gridIgnoraPyme.setTitle('<div><div style="float:left">Estatus: Pignorado a Seleccionada PYME</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaIgnoraPyme.load();
						}
						if (combo.value == "6")	{
							gridIgnoraNego.setTitle('<div><div style="float:left">Estatus: Pignorado a Negociable</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaIgnoraNego.load();
						}
						if (combo.value == "7")	{
							gridModifica.setTitle('<div><div style="float:left">Estatus: Modificaci�n de Montos y/o Fechas de Vencimiento</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaModifica.load();
						}
						if (combo.value == "8")	{
							gridNotaCredito.setTitle('<div><div style="float:left">Estatus: Aplicaci�n de Nota de Cr�dito</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaNotaCredito.load();
						}
						if (combo.value == "9")	{
							gridProgramado.setTitle('<div><div style="float:left">Estatus: Programado a Negociable</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaProgramado.load();
						}
					}
				}
			}
		}
	];	
	
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		frame: true,
		border: false,
		title: '<div><center>Cambios de Estatus</div>',
		collapsible: true,
		titleCollapse: false,
		style: ' margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		hidden: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:	elementosForma,
		monitorValid: true
	});

//Simulacion en un contenedor
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(20),
			gridDescuento,		
			gridBaja,			
			gridAnticipa,		
			gridPymeNego,		
			gridIgnoraPyme,	
			gridIgnoraNego,	
			gridModifica,		
			gridNotaCredito,	
			gridProgramado
		]
	});
	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '13reporte02ext.data.jsp',
		params: {
			informacion: "dataIniciales"
		},
		callback: procesaValoresIniciales
	});

});