<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String cc_acuse = (request.getParameter("ccAcuse") == null) ? "" : request.getParameter("ccAcuse");

try {
	if (!cc_acuse.equals(""))	{
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
		StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
		Registros regDatosAcuse = new Registros();
		Registros regDatosAcuse2 = new Registros();
		Registros regDatosAcuseTotal = new Registros();
		regDatosAcuse = BeanParamDscto.getDatosAcuse(cc_acuse);
		regDatosAcuse2 = BeanParamDscto.getDatosAcuse2(cc_acuse);
		regDatosAcuseTotal = BeanParamDscto.getDatosTotalesAcuse(cc_acuse);		

		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		"",
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	/************/
	/**Cabecera**/
	/************/
		if (regDatosAcuse != null)	{
			pdfDoc.setTable(9, 100);
			pdfDoc.setCell("IF Seleccionado", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("EPO", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Número de Documento", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Documento", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Porcentaje de Descuento", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto a Descontar", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Intereses", "formasmenB", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto a Recibir", "formasmenB", ComunesPDF.CENTER);
			int i=0;
			while (regDatosAcuse.next()){
				pdfDoc.setCell((regDatosAcuse.getString("NOMBRE_IF")==null)?"":regDatosAcuse.getString("NOMBRE_IF"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((regDatosAcuse.getString("NOMBRE_EPO")==null)?"":regDatosAcuse.getString("NOMBRE_EPO"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((regDatosAcuse.getString("IG_NUMERO_DOCTO")==null)?"":regDatosAcuse.getString("IG_NUMERO_DOCTO"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((regDatosAcuse.getString("CD_NOMBRE")==null)?"":regDatosAcuse.getString("CD_NOMBRE"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal((regDatosAcuse.getString("FN_MONTO")==null)?"":regDatosAcuse.getString("FN_MONTO"),2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal((regDatosAcuse.getString("FN_PORC_ANTICIPO")==null)?"":regDatosAcuse.getString("FN_PORC_ANTICIPO")+"%",0), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal((regDatosAcuse.getString("FN_MONTO_DSCTO")==null)?"":regDatosAcuse.getString("FN_MONTO_DSCTO"),2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal((regDatosAcuse.getString("IN_IMPORTE_INTERES")==null)?"":regDatosAcuse.getString("IN_IMPORTE_INTERES"),2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal((regDatosAcuse.getString("IN_IMPORTE_RECIBIR")==null)?"":regDatosAcuse.getString("IN_IMPORTE_RECIBIR"),2), "formasmen", ComunesPDF.CENTER);
				i ++;
			}
			if (regDatosAcuseTotal != null)	{
				String icMoneda = "";
				String icMonedaAnt = "";
				int count = 0;
				while (regDatosAcuseTotal.next()){
					icMoneda = (regDatosAcuseTotal.getString("IC_MONEDA")==null)?"":regDatosAcuseTotal.getString("IC_MONEDA");
					if(icMoneda!=icMonedaAnt){
						pdfDoc.setCell((regDatosAcuseTotal.getString("CD_NOMBRE")==null)?"":regDatosAcuseTotal.getString("CD_NOMBRE"), "formasmenB", ComunesPDF.CENTER,9);
					}
					if (count == 0) {
						pdfDoc.setCell("E P O - DATOS", "formasmenB", ComunesPDF.CENTER,4);
						pdfDoc.setCell("Total Monto Documento", "formasmenB", ComunesPDF.CENTER,2);
						pdfDoc.setCell("Total Monto Descuento", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Total Monto de Interés", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Total Importe a Recibir", "formasmenB", ComunesPDF.CENTER);
					}
					pdfDoc.setCell((regDatosAcuseTotal.getString("NOMBRE_EPO")==null)?"":regDatosAcuseTotal.getString("NOMBRE_EPO"), "formasmenB", ComunesPDF.CENTER,4);
					//pdfDoc.setCell(regDatosAcuseTotal.getString("CD_NOMBRE"), "formasmenB", ComunesPDF.CENTER,9);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal((regDatosAcuseTotal.getString("TOTAL_MONTO")==null)?"":regDatosAcuseTotal.getString("TOTAL_MONTO"),2), "formasmenB", ComunesPDF.CENTER,2);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal((regDatosAcuseTotal.getString("TOTAL_MONTO_DSCTO")==null)?"":regDatosAcuseTotal.getString("TOTAL_MONTO_DSCTO"),2), "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal((regDatosAcuseTotal.getString("TOTAL_INTERES")==null)?"":regDatosAcuseTotal.getString("TOTAL_INTERES"),2), "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal((regDatosAcuseTotal.getString("TOTAL_RECIBIR")==null)?"":regDatosAcuseTotal.getString("TOTAL_RECIBIR"),2), "formasmenB", ComunesPDF.CENTER);
					count ++;
					icMonedaAnt = icMoneda;
				}
			}
			if (i == 0){
				pdfDoc.addText("No se encontraron registros","formasrep", ComunesPDF.CENTER);
			}
		}
		if (regDatosAcuse2 != null)	{
			String fechaCarga		= "";
			String horaCarga		= "";
			String numeroUsuario	= "";
			String nombreUsuario	= "";
			if (regDatosAcuse2.next())	{
				fechaCarga		= regDatosAcuse2.getString("FECHA_CARGA");
				horaCarga		= regDatosAcuse2.getString("HORA_CARGA");
				numeroUsuario	= regDatosAcuse2.getString("IC_USUARIO");
				pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
				pdfDoc.addText("Cifras de Control","formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
				pdfDoc.addText("Número de Acuse:		"+cc_acuse	,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("Fecha de Carga:		"+fechaCarga,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("Hora de Carga:		"+horaCarga,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("Usuario de Captura:	"+numeroUsuario,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("","formas",ComunesPDF.CENTER);
				pdfDoc.addText("Al transmitir este MENSAJE DE DATOS, usted está bajo su responsabilidad cediendo los derechos de crédito de los DOCUMENTOS que constan en el mismo al INTERMEDIARIO FINANCIERO, dicha transmisión tendrá validez para todos los efectos legales","formas",ComunesPDF.CENTER);
			}
		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {}
%>
<%=jsonObj%>