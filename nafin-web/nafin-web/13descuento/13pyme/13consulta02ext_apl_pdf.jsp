<%@ page contentType="application/json;charset=UTF-8"
import="java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String claveDocumento = (request.getParameter("ic_documento") == null) ? "" : request.getParameter("ic_documento");

try {
	if (!claveDocumento.equals(""))	{
		
		ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
		//	Variables de uso local
		int numRegistros = 0;
		int numRegistrosMN = 0;
		int numRegistrosDL = 0;
		int ordenAplicacion = 0;
				//	De despliegue
		String nombreEpo = "";
		String nombreIF = "";
		String numeroDocto = "";
		String fechaEmision = "";
		String fechaVencimiento = "";
		double dblImporteRecibir = 0;
		String producto = null;
		String fechaSeleccion = null;
		String fechaOperacion = null;
		String numMensualidad = null;
		String numCredito = null;
		String tipoContrato = null;
		String numPrestamo = null;
		double dblImporteAplicadoCredito = 0;
		double dblSaldoCredito = 0;
		String nombreMoneda = "";
		double dblMontoPago = 0;
		double dblInteresPago = 0;
		String monedaClave = "";
		double dblTotalSaldoCreditoPesos = 0;
		double dblTotalSaldoCreditoDolares = 0;
		double dblTotalAplicadoCreditoPesos = 0;
		double dblTotalAplicadoCreditoDolares = 0;
		String importeAplicado = "";
		StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
		String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

		Vector vDetalleDocumentoAplicado = BeanSeleccionDocumento.ovgetDetalleDocumentoAplicado(iNoCliente, claveDocumento);

		//ordenAplicacion = 0;	
		dblTotalAplicadoCreditoPesos = 0;
		dblTotalSaldoCreditoPesos = 0;
		dblTotalAplicadoCreditoDolares = 0;
		dblTotalSaldoCreditoDolares = 0;
		numRegistros = 0;
		numRegistrosMN = 0;
		numRegistrosDL = 0;

		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.setTable(11, 100);
		for (int i = 0; i < vDetalleDocumentoAplicado.size(); i++) {
			numRegistros++;
			ordenAplicacion++;
			Vector vDatosDetalleDocumentoAplicado = (Vector) vDetalleDocumentoAplicado.get(i);

			numeroDocto = vDatosDetalleDocumentoAplicado.get(0).toString();
			dblImporteRecibir = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(1).toString());
			producto = vDatosDetalleDocumentoAplicado.get(2).toString();
			nombreIF = vDatosDetalleDocumentoAplicado.get(3).toString();
			fechaSeleccion = vDatosDetalleDocumentoAplicado.get(4).toString();
			fechaOperacion = vDatosDetalleDocumentoAplicado.get(5).toString();
			numMensualidad = vDatosDetalleDocumentoAplicado.get(6).toString();
			numCredito = vDatosDetalleDocumentoAplicado.get(7).toString();
			tipoContrato = vDatosDetalleDocumentoAplicado.get(8).toString();
			numPrestamo = vDatosDetalleDocumentoAplicado.get(9).toString();
			dblImporteAplicadoCredito = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(10).toString());
			dblSaldoCredito = Double.parseDouble(vDatosDetalleDocumentoAplicado.get(11).toString());
			monedaClave = vDatosDetalleDocumentoAplicado.get(12).toString();
			if(numRegistros == 1) {
				pdfDoc.setCell("Número de documento", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell(numeroDocto, "celda01rep", ComunesPDF.CENTER,8);
				pdfDoc.setCell("Monto Aplicado", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblImporteRecibir,2), "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("        ", "celda02rep", ComunesPDF.CENTER,11);
				pdfDoc.setCell("Producto", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("IF", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Orden de Aplicación", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Aplicación", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Operación", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Número de Mensualidad", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Número de Pedido / Disposición", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Contrato / Operación", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Número de Préstamo NAFIN", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Importe Aplicado a Crédito", "celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Saldo del Crédito", "celda01rep", ComunesPDF.CENTER);
			}
			if ("1".equals(monedaClave)){	//moneda nacional
				numRegistrosMN++;
				dblTotalAplicadoCreditoPesos += dblImporteAplicadoCredito;
				dblTotalSaldoCreditoPesos += dblSaldoCredito;
			} else if("54".equals(monedaClave) ) {	//dolar
				numRegistrosDL++;
				dblTotalAplicadoCreditoDolares += dblImporteAplicadoCredito;
				dblTotalSaldoCreditoDolares += dblSaldoCredito;
			}
			pdfDoc.setCell(producto, "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell(nombreIF, "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(ordenAplicacion,0), "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell(fechaSeleccion, "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell(fechaOperacion, "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell(numMensualidad, "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell(numCredito, "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell(tipoContrato, "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell(numPrestamo, "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblImporteAplicadoCredito,2), "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblSaldoCredito,2), "celda02rep", ComunesPDF.CENTER);
		}
		if (numRegistrosMN != 0) {
			pdfDoc.setCell("Total Moneda Nacional", "celda01rep", ComunesPDF.CENTER);
			pdfDoc.setCell(" ", "celda01rep", ComunesPDF.CENTER,8);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalAplicadoCreditoPesos, 2), "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalSaldoCreditoPesos, 2), "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell("Importe a Depositar a la Pyme", "celda01rep", ComunesPDF.CENTER);
			double dblImporteDepositarPyme = dblImporteRecibir - dblTotalAplicadoCreditoPesos;
			if (dblImporteDepositarPyme <= 0) {
				dblImporteDepositarPyme = 0;
			}
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblImporteDepositarPyme, 2), "celda01rep", ComunesPDF.LEFT,10);
		}
		if (numRegistrosDL != 0) {
			pdfDoc.setCell("Total Dólares", "celda01rep", ComunesPDF.CENTER);
			pdfDoc.setCell(" ", "celda01rep", ComunesPDF.CENTER,8);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalAplicadoCreditoDolares, 2), "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(dblTotalSaldoCreditoDolares, 2), "celda02rep", ComunesPDF.CENTER);
			pdfDoc.setCell("Importe a Depositar a la Pyme", "celda01rep", ComunesPDF.CENTER);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal( dblImporteRecibir - dblTotalAplicadoCreditoDolares, 2), "celda01rep", ComunesPDF.LEFT,10);
		}
		if (numRegistros == 0){
			pdfDoc.setCell("No se encontraron registros", "celda02rep", ComunesPDF.CENTER,11);
		}

		pdfDoc.addTable();
		pdfDoc.endDocument();
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {}
%>
<%=jsonObj%>