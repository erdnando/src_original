<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		java.sql.*, 
		javax.naming.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,
		com.netro.servicios.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
	String informacion	=	(request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
	String infoRegresar = "";
	String			nomEpo			= "";
	String 			nomEjecutivo	= "";
	String 			telefono 		= "";
	String 			correoElec 		= "";
	String 			fg_monto			= "";
	Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB", Servicios.class);
	
	Map map = null;
	
	if(informacion.equals("valoresIniciales")){
		try{
			map = new HashMap();
			map = servicios.getDatosCreditoProductivo(iNoEPO,iNoCliente);
			//Prueba
			//map = servicios.getDatosCreditoProductivo("256","4519");
			if (map!=null){
				nomEpo			= String.valueOf(map.get("NOMBRE_EPO"));
				nomEjecutivo	= String.valueOf(map.get("NOM_EJECUTIVO"));
				telefono			= String.valueOf(map.get("TELEFONO"));
				correoElec 		= String.valueOf(map.get("CORREO"));
				fg_monto			= String.valueOf(map.get("MONTO"));
			}
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("success",new Boolean(true));
			jsonObj.put("NOMBRE_EPO",nomEpo);
			jsonObj.put("NOM_EJECUTIVO",nomEjecutivo);
			jsonObj.put("TELEFONO",telefono);
			jsonObj.put("CORREO",correoElec);
			jsonObj.put("MONTO",Comunes.formatoDecimal(Double.parseDouble(fg_monto),0));
			infoRegresar = jsonObj.toString();
		}catch(Exception e){
			throw new AppException("Error al intentar mostrar algunos datos",e);
		}
	}else if(informacion.equals("ActualizarDatos")){
		String cs_estatus_envio = (request.getParameter("cs_estatus_envio")!=null)?request.getParameter("cs_estatus_envio"):"";
		String cs_estatus_respuesta = (request.getParameter("cs_estatus_respuesta")!=null)?request.getParameter("cs_estatus_respuesta"):"";
		try{
			servicios.actualizaRespuestaCreditoProductivo(cs_estatus_envio,cs_estatus_respuesta,iNoEPO,iNoCliente);
			infoRegresar = "{\"success\":true,\"mensaje\":\"Los datos han sido actualizados correctamente\"}";
		} catch(Exception e){
			throw new AppException("Hubo un error al actualizar los datos",e);
		}
	}else if (informacion.equals("Continuar")){
		try{
			List pantallasNavegacionComplementaria = (List)session.getAttribute("inicializar.PantallasComplementarias");
			pantallasNavegacionComplementaria.remove("/13descuento/13pyme/13creditoext.jsp"); //El nombre debe coincidir con el especificado en Navegación
			infoRegresar = "{\"success\": true, \"urlPagina\": \"" + appWebContextRoot + pantallasNavegacionComplementaria.get(0) + "\"}";
		}	catch(Exception e){
			throw new AppException("Hubo un error al tratar de mostrar la siguiente página",e);
		}
	}
%>
<%=infoRegresar%>

