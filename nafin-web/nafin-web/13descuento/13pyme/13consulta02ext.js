Ext.onReady(function() {

//*-*-*-*-INIT*-*-*-*-HANDLERS*-*-*-*-INIT
	var jsonParametrizacion = {};
	var ocultarDoctosAplicados  =	false;
	var cc_acuse = "";
	var notasA = {ic_documento:null,	notaAplica:null, notaCredito:null,	notasDocto:null}
	function procesarSuccessFailureParametrizacion(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonParametrizacion = Ext.util.JSON.decode(response.responseText);
			var vencePyme = Ext.getCmp('hidenVencePyme');
			var operaNota = Ext.getCmp('hidenOperaNota');
			if (jsonParametrizacion.sFechaVencPyme != undefined) {vencePyme.setValue(jsonParametrizacion.sFechaVencPyme);}
			if(jsonParametrizacion.bTipoFactoraje && jsonParametrizacion.bOperaNotasDeCredito != undefined) {
				if (jsonParametrizacion.bOperaNotasDeCredito == true)	{
					operaNota.setValue('S');
				} else{
					operaNota.setValue('N');
				}
			}
			var cambioEstatusCmb = Ext.getCmp('cambioEstatusCmb');
			if (jsonParametrizacion.mostrarOpcionPignorado){
				var store = catalogoEstatusData;
				var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
				store.add(
					new reg({
						clave: '23',
						descripcion: 'Documento Pignorado',
						loadMsg: null
					})
				);
			}
			var camposDinamicos = jsonParametrizacion.camp_dina1;
			var contenedorIzq = Ext.getCmp('camposDic');
			var cmbEpo = Ext.getCmp('cmbEpo');
		
			if (camposDinamicos != undefined){
				Ext.each(camposDinamicos, function(item, index, arrItems){
					if (item.IC_NO_CAMPO == "1") {
						if (item.CG_TIPO_OBJETO == "T")	{
							if (item.CG_TIPO_DATO == "N")	{
								var config = {xtype: 'numberfield', width: 100, name: 'camp_dina1',id: 'campDina1',allowBlank: true,fieldLabel: item.NOMBRE_CAMPO,
									maxLength: item.IG_LONGITUD,hidden:	false}
							}else{
								var config = {xtype: 'textfield', width: 100, name: 'camp_dina1',id: 'campDina1',allowBlank: true,fieldLabel: item.NOMBRE_CAMPO,
									maxLength: item.IG_LONGITUD,hidden:	false, vtype:	'alphanum'}
							}
							contenedorIzq.add(config);
							contenedorIzq.insert(6,config);
							contenedorIzq.doLayout();
						}	else	{
							var config = {
								xtype: 'combo',
								name: 'camp_dina1',
								id: 'campDina1',
								fieldLabel: item.NOMBRE_CAMPO,
								emptyText: 'Seleccionar',
								mode: 'local',
								displayField: 'descripcion',
								valueField: 'clave',
								hiddenName : 'camp_dina1',
								forceSelection : false,
								triggerAction : 'all',
								typeAhead: true,
								width: 250,
								minChars : 1,
								store: catalogoCamposAdicionalesData,
								tpl : NE.util.templateMensajeCargaCombo
							}
							contenedorIzq.add(config);
							contenedorIzq.insert(6,config);
							contenedorIzq.doLayout();
							catalogoCamposAdicionalesData.load({params: Ext.apply({no_epo:	cmbEpo.value})});
						}
					}
					if (item.IC_NO_CAMPO == "2") {
						if (item.CG_TIPO_OBJETO == "T")	{
							if (item.CG_TIPO_DATO == "N")	{
								var config = {xtype: 'numberfield', width: 100, name: 'camp_dina2',id: 'campDina2',allowBlank: true,fieldLabel: item.NOMBRE_CAMPO,
									maxLength: item.IG_LONGITUD,hidden:	false}
							}else{
								var config = {xtype: 'textfield', width: 100, name: 'camp_dina2',id: 'campDina2',allowBlank: true,fieldLabel: item.NOMBRE_CAMPO,
									maxLength: item.IG_LONGITUD,hidden:	false, vtype:	'alphanum'}
							}
							contenedorIzq.add(config);
							contenedorIzq.insert(7,config);
							contenedorIzq.doLayout();
						}	else	{
							var config = {
								xtype: 'combo',
								name: 'camp_dina2',
								id: 'campDina2',
								width: 250,
								fieldLabel: item.NOMBRE_CAMPO,
								emptyText: 'Seleccionar',
								mode: 'local',
								displayField: 'descripcion',
								valueField: 'clave',
								hiddenName : 'camp_dina2',
								forceSelection : false,
								triggerAction : 'all',
								typeAhead: true,
								minChars : 1,
								store: catalogoCamposAdicionales_2_Data,
								tpl : NE.util.templateMensajeCargaCombo
							}
							contenedorIzq.add(config);
							contenedorIzq.insert(7,config);
							contenedorIzq.doLayout();
							catalogoCamposAdicionales_2_Data.load({params: Ext.apply({no_epo:	cmbEpo.value})});
						}
					}
					if (index > 2){ return false;}
				});
			}	else {
				var dina1 =  Ext.getCmp('campDina1');
				var dina2 =  Ext.getCmp('campDina2');
				if (dina1 != undefined) {contenedorIzq.remove(dina1);}
				if (dina2 != undefined) {contenedorIzq.remove(dina2);}
			}
			
			//Fodea026
			Ext.getCmp('tipoFactorajeCmb').reset();
				catalogoFactorajeData.load({
					params: Ext.apply({ epo : (Ext.getCmp('cmbEpo')).getValue() })
			});
			fp.el.unmask();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	function procesarMuestraDetalle(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var data = Ext.util.JSON.decode(response.responseText);

			var camposDinamicosDetalle = jsonParametrizacion.camp_dina2;
			if (camposDinamicosDetalle != undefined){
				var datosDinamicos = data.datos_detalle;
				var count = 1;
				if (datosDinamicos != undefined){
					gridDetalleData.loadData(datosDinamicos);
					if (!gridDetalle.isVisible()) {
						gridDetalle.show();
					}
				}
				
				var cm = gridDetalle.getColumnModel();
				Ext.each(camposDinamicosDetalle, function(item, index, arrItems){
					if (item.IC_NO_CAMPO == "1"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO1'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO1'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO1'), false);}
					if (item.IC_NO_CAMPO == "2"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO2'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO2'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO2'), false);}
					if (item.IC_NO_CAMPO == "3"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO3'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO3'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO3'), false);}
					if (item.IC_NO_CAMPO == "4"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO4'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO4'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO4'), false);}
					if (item.IC_NO_CAMPO == "5"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO5'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO5'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO5'), false);}
					if (item.IC_NO_CAMPO == "6"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO6'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO6'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO6'), false);}
					if (item.IC_NO_CAMPO == "7"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO7'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO7'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO7'), false);}
					if (item.IC_NO_CAMPO == "8"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO8'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO8'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO8'), false);}
					if (item.IC_NO_CAMPO == "9"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO9'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO9'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO9'), false);}
					if (item.IC_NO_CAMPO == "10"){cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO10'), item.CG_NOMBRE_CAMPO);cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO10'), item.CG_NOMBRE_CAMPO);cm.setHidden(cm.findColumnIndex('CG_CAMPO10'), false);}
				});
			}
			
			var ventana = Ext.getCmp('winDetalle');
			var btnPdfDetalle = Ext.getCmp('btnPdfDetalle');
			var btnBajarPdfDetalle = Ext.getCmp('btnBajarPdfDetalle');
			if (ventana) {
				btnPdfDetalle.enable();
				btnBajarPdfDetalle.hide();
				ventana.show();
			}else{
				new Ext.Window({
					modal: true,
					resizable: false,
					layout: 'form',
					width: 800,
					autoHeight: true,
					//height: 350,
					id: 'winDetalle',
					closeAction: 'hide',
					items: [fpDetalle,NE.util.getEspaciador(1),gridDetalle],
					title: 'Detalles del Documento',
					bbar: {
						xtype: 'toolbar',
						buttons: ['->','-',{xtype: 'button',	text: 'Cerrar',	id: 'btnCloseDet'},
											'-',{xtype: 'button',	text: 'Generar PDF',	id: 'btnPdfDetalle'},
											{xtype: 'button',	text: 'Bajar PDF',	id: 'btnBajarPdfDetalle',hidden: true}]
					}
				}).show();
			}

			Ext.getCmp('disDoctoDet').body.update('&nbsp');
			Ext.getCmp('disEmision').body.update('&nbsp');
			Ext.getCmp('disVto').body.update('&nbsp');
			Ext.getCmp('disImporte').body.update('&nbsp');
			Ext.getCmp('disMoneda').body.update('&nbsp');

			Ext.getCmp('disNombre').body.update('&nbsp');
			Ext.getCmp('disRfc').body.update('&nbsp');
			Ext.getCmp('disCalle').body.update('&nbsp');
			Ext.getCmp('disColonia').body.update('&nbsp');
			Ext.getCmp('disCp').body.update('&nbsp');
			Ext.getCmp('disEstado').body.update('&nbsp');
			Ext.getCmp('disDelomun').body.update('&nbsp');

			if (data.datos_doctos != undefined)	{
				Ext.each(data.datos_doctos, function(item, index, arrItems){
					Ext.getCmp('disNombre').body.update('<div><b>'+item.CG_RAZON_SOCIAL+'</b></div>');
					Ext.getCmp('disRfc').body.update(item.CG_RFC);
					Ext.getCmp('disCalle').body.update(item.CG_CALLE);
					Ext.getCmp('disColonia').body.update(item.CG_COLONIA);
					Ext.getCmp('disCp').body.update(item.CN_CP);
					Ext.getCmp('disEstado').body.update(item.CG_ESTADO);
					Ext.getCmp('disDelomun').body.update(item.CG_MUNICIPIO);

					Ext.getCmp('disDoctoDet').body.update('<div align="right">'+item.IG_NUMERO_DOCTO+'</div>');
					Ext.getCmp('disEmision').body.update('<div align="right">'+item.FECHA_DOCTO+'</div>');
					Ext.getCmp('disVto').body.update('<div align="right">'+item.FECHA_VENC+'</div>');
					Ext.getCmp('disImporte').body.update('<div align="right">'+Ext.util.Format.number(item.FN_MONTO, '$0,0.00')+'</div>');
					Ext.getCmp('disMoneda').body.update('<div align="right">'+item.MONEDA);

					var campo1 = item.CG_CAMPO1;
					var campo2 = item.CG_CAMPO2;
					var campo3 = item.CG_CAMPO3;
					var campo4 = item.CG_CAMPO4;
					var campo5 = item.CG_CAMPO5;
					if (Ext.isEmpty(campo1)){campo1 = '&nbsp;'}
					if (Ext.isEmpty(campo2)){campo2 = '&nbsp;'}
					if (Ext.isEmpty(campo3)){campo3 = '&nbsp;'}
					if (Ext.isEmpty(campo4)){campo4 = '&nbsp;'}
					if (Ext.isEmpty(campo5)){campo5 = '&nbsp;'}
					var camposDinamicos = jsonParametrizacion.camp_dina1;
					var contenedorIzq = Ext.getCmp('camposDic');
					if (camposDinamicos != undefined){
						var config = [];
						var cont = 1;
						Ext.each(camposDinamicos, function(item, index, arrItems){
							if (item.IC_NO_CAMPO == "1") {
								config = {xtype: 'panel',  width: 100, name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),html:	'&nbsp;&nbsp;&nbsp;&nbsp;<b>'+item.NOMBRE_CAMPO + ' ' + campo1+'</b>'}
							}
							if (item.IC_NO_CAMPO == "2") {
								config = {xtype: 'panel',  width: 100,name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),html:	'&nbsp;&nbsp;&nbsp;&nbsp;<b>'+item.NOMBRE_CAMPO + ' ' + campo2+'</b>'}
							}
							if (item.IC_NO_CAMPO == "3") {
								config = {xtype: 'panel',  width: 100,name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),html: '&nbsp;&nbsp;&nbsp;&nbsp;<b>'+item.NOMBRE_CAMPO +' '+ campo3+'</b>'}
							}
							if (item.IC_NO_CAMPO == "4") {
								config = {xtype: 'panel',  width: 100, name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),html: '&nbsp;&nbsp;&nbsp;&nbsp;<b>'+item.NOMBRE_CAMPO + ' ' + campo4+'</b>'}
							}
							if (item.IC_NO_CAMPO == "5") {
								config = {xtype: 'panel',  width: 100, name: 'dinamico'+index.toString(),id: 'dina'+index.toString(),html: '&nbsp;&nbsp;&nbsp;&nbsp;<b>'+item.NOMBRE_CAMPO + ' ' + campo5+'</b>'}
							}
							contenedorIzq.add(config);
							contenedorIzq.insert(8+index,config);
							contenedorIzq.doLayout();
						});
					}
				});
			}

			if (datosDinamicos != undefined){
				Ext.getCmp('btnPdfDetalle').enable();
			}else{
				Ext.getCmp('btnPdfDetalle').disable();
			}
			

			var btnGenerar = Ext.getCmp('btnPdfDetalle');
			var noEpo = Ext.getCmp('cmbEpo');
			btnGenerar.setHandler(
				function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta02ext_det_pdf.jsp',
						params: Ext.apply({no_epo: noEpo.value,ic_docto:	opts.params.ic_docto}),
						callback: procesarGenerarPdfDetalle
					});
				}
			);

			Ext.getCmp('btnCloseDet').setHandler(
				function(boton, evento) {
					Ext.getCmp('winDetalle').hide();
				}
			);

		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var muestraGridDetalle = function(grid, rowIndex, colIndex, item, event)	{
		gridDetalleData.loadData('');
		gridDetalle.hide();
		var registro = grid.getStore().getAt(rowIndex);
		var noEpo = Ext.getCmp('cmbEpo');
		var ic_documento = registro.get('IC_DOCUMENTO');
		var contenedorIzq = Ext.getCmp('Izquierda');
		var ciclo = 0;
		for (ciclo=0; ciclo<5; ciclo ++){
			var dina = Ext.getCmp('dina'+ciclo.toString());
			if (dina != undefined) {contenedorIzq.remove(dina);}
		}
		Ext.Ajax.request({url: '13consulta02ext.data.jsp',params: Ext.apply({informacion: "obtenDetalles",no_epo: noEpo.value,ic_docto:	ic_documento}),callback: procesarMuestraDetalle});
	}
	
	function procesarMuestraPanelAcuse(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var dataAcuse = Ext.util.JSON.decode(response.responseText);
			if (dataAcuse.datosAcuse2 != undefined)	{
				Ext.each(dataAcuse.datosAcuse2, function(item, index, arrItems){
					var disFechaCarga = Ext.getCmp('disFechaCarga');
					var disHoraCarga  = Ext.getCmp('disHoraCarga');
					var disUsuario    = Ext.getCmp('disUsuario');
					disFechaCarga.setValue(item.FECHA_CARGA);
					disHoraCarga.setValue(item.HORA_CARGA);
					disUsuario.setValue(item.IC_USUARIO);
				});
			}

			if (dataAcuse.datosAcuse != undefined){
				gridAcuseData.loadData(dataAcuse.datosAcuse);
				if (!gridAcuse.isVisible()) {
					gridAcuse.show();
				}
			}
			if (dataAcuse.datosAcuseTotal != undefined){
				gridTotalAcuseData.loadData(dataAcuse.datosAcuseTotal);
			}
			var ventana = Ext.getCmp('winAcuse');
			if (ventana) {
				ventana.show();
			}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				width: 800,
				height: 540,
				id: 'winAcuse',
				closeAction: 'hide',
				items: [gridAcuse,NE.util.getEspaciador(10),gridTotalAcuse,NE.util.getEspaciador(10),fpAcuse],
				title: 'Acuse',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',{xtype: 'button',	text: 'Generar PDF',		id: 'btnPdfAcuse'}
											,{xtype: 'button',	text: 'Bajar PDF',		id: 'btnBajarPdfAcuse',hidden: true}
										,'-',{xtype: 'button',	text: 'Generar Archivo',id: 'btnCsvAcuse'}
											,{xtype: 'button',	text: 'Bajar Archivo',	id: 'btnBajarCsvAcuse',hidden: true}]
				}
				}).show();
			}
			var storeGrid = gridAcuseData.data;
			var btnPdfAcuse = Ext.getCmp('btnPdfAcuse');
			var btnBajarPdfAcuse = Ext.getCmp('btnBajarPdfAcuse');
			var btnCsvAcuse = Ext.getCmp('btnCsvAcuse');
			var btnBajarCsvAcuse = Ext.getCmp('btnBajarCsvAcuse');
			if(storeGrid.length < 1) {
				gridAcuse.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				btnPdfAcuse.disable();
				btnCsvAcuse.disable();
				gridTotalAcuseData.loadData('');
				gridTotalAcuse.getGridEl().mask('');
			} else	{
				btnPdfAcuse.enable();
				btnCsvAcuse.enable();
				btnBajarPdfAcuse.hide();
				btnBajarCsvAcuse.hide();
			}
			var btnGenerar = Ext.getCmp('btnPdfAcuse');
			btnGenerar.setHandler(
				function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta02ext_acu_pdf.jsp',
						params: Ext.apply({ccAcuse: cc_acuse}),
						callback: procesarGenerarPdfAcuse
					});
				}
			);

			var btnGenerarCSV = Ext.getCmp('btnCsvAcuse');
			btnGenerarCSV.setHandler(
			function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				
				Ext.Ajax.request({
						url: '13consulta02ext_acu_csv.jsp',
						params: Ext.apply({ccAcuse: cc_acuse}),
						callback: procesarGenerarCsvAcuse
					});
				}
			);

		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	var muestraPanelAcuse = function(grid, rowIndex, colIndex, item, event) {
	var registro = grid.getStore().getAt(rowIndex);
	cc_acuse = registro.get('CC_ACUSE');
	var ventana = Ext.getCmp('winAcuse');
	if (ventana){
		ventana.hide();
	}
	gridTotalAcuseData.loadData('');
	gridAcuseData.loadData('');
	var disAcuse      = Ext.getCmp('disAcuse');
	var disFechaCarga = Ext.getCmp('disFechaCarga');
	var disHoraCarga  = Ext.getCmp('disHoraCarga');
	var disUsuario    = Ext.getCmp('disUsuario');
	disAcuse.setValue(cc_acuse);
	disFechaCarga.setValue('');
	disHoraCarga.setValue('');
	disUsuario.setValue('');
	
	Ext.Ajax.request({url: '13consulta02ext.data.jsp',params: Ext.apply({informacion: "obtenAcuse",ccAcuse: cc_acuse}),callback: procesarMuestraPanelAcuse});
		
	}

	function procesarMuestraPanelAplica(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var dataAplica = Ext.util.JSON.decode(response.responseText);
			
			if (dataAplica.datosAplica != undefined)	{
				gridAplicaData.loadData(dataAplica.datosAplica);
				var disDocto	= Ext.getCmp('disDocto');
				var disMontoAplica	= Ext.getCmp('disMontoAplica');
				Ext.each(dataAplica.datosAplica, function(item, index, arrItems){
					disDocto.setValue(item.IG_NUMERO_DOCTO);
					disMontoAplica.setValue(item.IN_IMPORTE_RECIBIR);
					if (index > 0){return false;}
				});
				var storeGrid = gridAplicaData.data;
				var sumImporteAplica=0;
				var sumSaldoCre=0;
				var sumImporteDepo=0;
				var sumImporteAplicaDL=0;
				var sumSaldoCreDL=0;
				var sumImporteDepoDL=0;
				if(storeGrid.length > 0) {
					var orden = 1;
					storeGrid.each(function(registro,items){
						registro.set('ORDEN_APLICA',orden);
						if (registro.get('IC_MONEDA') == "1"){
							sumImporteAplica += registro.get('IMPORTE_APLICADO_CREDITO');
							sumSaldoCre += registro.get('SALDO_CREDITO');
						}
						if (registro.get('IC_MONEDA') == "54"){
							sumImporteAplicaDL += registro.get('IMPORTE_APLICADO_CREDITO');
							sumSaldoCreDL += registro.get('SALDO_CREDITO');								
						}
						orden++;
					});
					sumImporteDepo = disMontoAplica - sumImporteAplica;
					sumImporteDepoDL = disMontoAplica - sumImporteAplicaDL;
					if (sumImporteDepo <= 0 ){sumImporteDepo = 0;}
					if (sumImporteDepoDL <= 0 ){sumImporteDepoDL = 0;}

					var reg = gridTotalAplicaData.getAt(0);//Primer registro de conteo
					reg.set('TOTAL_IMPORTE_APLICADO',sumImporteAplica);
					reg.set('TOTAL_SALDO_CREDITO',sumSaldoCre);
					reg.set('TOTAL_IMPORTE_RECIBIR',sumImporteDepo);
					
					var regDL = gridTotalAplicaData.getAt(1);//Segundo registro de conteo
					regDL.set('TOTAL_IMPORTE_APLICADO',sumImporteAplicaDL);
					regDL.set('TOTAL_SALDO_CREDITO',sumSaldoCreDL);
					regDL.set('TOTAL_IMPORTE_RECIBIR',sumImporteDepoDL);
				}
			}
			var ventana = Ext.getCmp('winAplica');
			
			if (ventana) {
				ventana.show();
			}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				width: 803,
				height: 480,
				id: 'winAplica',
				closeAction: 'hide',
				items: [fpAplica,NE.util.getEspaciador(5),gridAplica,NE.util.getEspaciador(5),gridTotalAplica],
				title: 'Detalle de los cr�ditos pagados con el documento',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',	{xtype: 'button',	text: 'Generar PDF',		id: 'btnPdfAplica'}
												,{xtype: 'button',	text: 'Bajar PDF',		id: 'btnBajarPdfAplica',hidden: true}
										,'-',	{xtype: 'button',	text: 'Generar Archivo',id: 'btnCsvAplica'}
												,{xtype: 'button',	text: 'Bajar Archivo',	id: 'btnBajarCsvAplica',hidden: true}	]
				}
				}).show();
			}
			var storeGrid = gridAplicaData.data;
			var btnPdfAplica = Ext.getCmp('btnPdfAplica');
			var btnBajarPdfAplica = Ext.getCmp('btnBajarPdfAplica');
			var btnCsvAplica = Ext.getCmp('btnCsvAplica');
			var btnBajarCsvAplica = Ext.getCmp('btnBajarCsvAplica');
			gridTotalAplica = Ext.getCmp('gridTotalAplica');
			if(storeGrid.length < 1) {
				gridAplica.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
				gridTotalAplica.hide();
				btnPdfAplica.disable();
				btnCsvAplica.disable();
			}else	{
				btnPdfAplica.enable();
				btnCsvAplica.enable();
				btnBajarPdfAplica.hide();
				btnBajarCsvAplica.hide();
				if (!gridTotalAplica.isVisible()){
					gridTotalAplica.show();
				}
			}
			var btnGenerar = Ext.getCmp('btnPdfAplica');
			btnGenerar.setHandler(
				function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
					Ext.Ajax.request({
						url: '13consulta02ext_apl_pdf.jsp',
						params: Ext.apply({ic_documento: ic_docto}),
						callback: procesarGenerarPdfAplica
					});
				}
			);
		
			var btnGenerarCSV = Ext.getCmp('btnCsvAplica');
			btnGenerarCSV.setHandler(
			function(boton, evento) {
				boton.disable();
				boton.setIconClass('loading-indicator');
				
				Ext.Ajax.request({
						url: '13consulta02ext_apl_csv.jsp',
						params: Ext.apply({ic_documento: ic_docto}),
						callback: procesarGenerarCsvAplica
					});
				}
			);

		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	

	var muestraDetalleAplicacion = function(grid, rowIndex, colIndex, item, event) {
		var registro = grid.getStore().getAt(rowIndex);
		var ic_docto = registro.get('IC_DOCUMENTO');
		var ventana = Ext.getCmp('winAplica');
		if (ventana){
			ventana.hide();
		}
		gridAplicaData.loadData('');
		Ext.getCmp('disDocto').setValue('');
		Ext.getCmp('disMontoAplica').setValue('');
		
		Ext.Ajax.request({url: '13consulta02ext.data.jsp',params: Ext.apply({informacion: "obtenAplica",ic_documento: ic_docto}),callback: procesarMuestraPanelAplica});
	}

	function procesarMuestraPanelModifica(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var dataModifica = Ext.util.JSON.decode(response.responseText);
			
			if (dataModifica.datosModifica != undefined)	{
				gridModificaData.loadData(dataModifica.datosModifica);
			}
			//gridModificaData.loadData(myData);
			var ventana = Ext.getCmp('winModifica');
			var btnPdfModifica = Ext.getCmp('btnPdfModifica');
			var btnBajarPdfModifica = Ext.getCmp('btnBajarPdfModifica');
			if (ventana) {
				btnPdfModifica.enable();
				btnBajarPdfModifica.hide();
				ventana.show();
				var storeGrid = gridModificaData.data;
				if(storeGrid.length < 1) {
					gridModifica.getGridEl().mask('No se encontr� ning�n registro', 'x-mask');
					btnPdfModifica.disable();
				}
			}
		}else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var muestraPanelModifica = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		var ic_documento = registro.get('IC_DOCUMENTO');
		var nombre_if = registro.get('NOMBRE_IF');
		var descEpo = registro.get('NOMBRE_EPO');
		var icDocto = registro.get('IG_NUMERO_DOCTO');
		var fecha_emis =	Ext.util.Format.date(registro.get("DF_FECHA_DOCTO"),'d/m/Y');
		var fecha_venc = Ext.util.Format.date(registro.get("DF_FECHA_VENC"),'d/m/Y');
		var nombreMoneda = registro.get('CD_NOMBRE');
		var fnMonto = registro.get('FN_MONTO');
		var estatusDocto = registro.get('CD_DESCRIPCION');
		var vencePyme = Ext.getCmp('hidenVencePyme');
		var cmbEpo = Ext.getCmp('cmbEpo');
		//
		gridModificaData.loadData('');
		//
		var disEpoM		=	Ext.getCmp('disEpoM');
		var disIfM		=	Ext.getCmp('disIfM');
		var disDoctoM	=	Ext.getCmp('disDoctoM');
		var disFecEmiM	=	Ext.getCmp('disFecEmiM');
		var disFecVencM=	Ext.getCmp('disFecVencM');
		var disMonedaM	=	Ext.getCmp('disMonedaM');
		var disMontoM	=	Ext.getCmp('disMontoM');
		var disEstatusM	=	Ext.getCmp('disEstatusM');
		disEpoM.setValue(descEpo);
		disIfM.setValue(nombre_if);
		disDoctoM.setValue(icDocto);
		disFecEmiM.setValue(fecha_emis);
		disFecVencM.setValue(fecha_venc);
		disMonedaM.setValue(nombreMoneda);
		disMontoM.setValue(fnMonto);
		disEstatusM.setValue(estatusDocto);
		var ventana = Ext.getCmp('winModifica');
		var btnPdfModifica = Ext.getCmp('btnPdfModifica');
		var btnBajarPdfModifica = Ext.getCmp('btnBajarPdfModifica');
		if (ventana) {
			ventana.hide();
		}else{
		new Ext.Window({
			modal: true,
			resizable: false,
			width: 803,
			height: 480,
			id: 'winModifica',
			closeAction: 'hide',
			items: [fpModifica,NE.util.getEspaciador(10),gridModifica],
			title: 'Modificaci�n montos y/o Fechas',
			bbar: {
				xtype: 'toolbar',
				buttons: ['->','-',{xtype: 'button',text: 'Generar PDF',	id: 'btnPdfModifica'},{xtype: 'button',text: 'Bajar PDF',id: 'btnBajarPdfModifica',hidden: true}]
			}
			});
		}
		var btnGenerar = Ext.getCmp('btnPdfModifica');
		btnGenerar.setHandler(
			function(boton, evento) {
			boton.disable();
			boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '13consulta02ext_mod_pdf.jsp',
					params: Ext.apply({ic_docto: ic_documento,ic_if: nombre_if,desc_epo: descEpo,ig_numero_docto:icDocto,fch_emision:fecha_emis,fch_venc:fecha_venc,moneda:nombreMoneda,
											monto:fnMonto,estatus:estatusDocto,noEpo: cmbEpo.value,sFechaVencPyme:vencePyme.value}),
					callback: procesarGenerarPdfModifica
				});
			}
		);

		Ext.Ajax.request({url: '13consulta02ext.data.jsp',params: Ext.apply(
		{informacion: "obtenModifica",ic_docto: ic_documento,sFechaVencPyme:vencePyme.value}),callback: procesarMuestraPanelModifica});
		
		/*Ext.Ajax.request({url: '13consulta02ext.data.jsp',params: Ext.apply(
		{informacion: "obtenModifica",ic_docto: ic_documento,ic_if: nombre_if,desc_epo: descEpo,ig_numero_docto:icDocto,fch_emision:fecha_emis,fch_venc:fecha_venc,moneda:nombreMoneda,
		monto:fnMonto,estatus:estatusDocto,noEpo: cmbEpo.value,sFechaVencPyme:vencePyme.value}),callback: procesarMuestraPanelModifica});		*/
	}

	var muestraPanelNotas = function(grid, rowIndex, colIndex, item, event)	{
		var registro = grid.getStore().getAt(rowIndex);
		notasA.notaAplica = false;
		notasA.notaCredito = "";
		notasA.notasDocto = false;
		notasA.ic_documento = "";
		if(registro.get('NOTA_MULTIPLE') == "N" || registro.get('NOTA_MULTIPLE_DOCTO') == "N")	{
			if(registro.get('NOTA_MULTIPLE') == "N")	{
				//	verNotaDeCredito();
				notasA.notaAplica = true;
				notasA.notaCredito = registro.get('IC_DOCUMENTO');
				
			}else	{
				//		verNotasDocumento();
				notasA.notasDocto = true;
				notasA.ic_documento = registro.get('IC_DOCUMENTO');
			}
		}

		var ventana = Ext.getCmp('winNotas');
		if (ventana) {
			Ext.getCmp('btnBajarPdfNotas').hide();
		}else{
			new Ext.Window({
				modal: true,
				resizable: false,
				x: 200,
				width: 800,
				height: 360,
				id: 'winNotas',
				closeAction: 'hide',
				items: [gridNotaVarias],
				title: 'Documentos Aplicados a Nota de Cr�dito',
				bbar: {
					xtype: 'toolbar',
					buttons: ['->','-',{xtype: 'button',text: 'Generar PDF',	id:'btnPdfNotas', disabled:true},{xtype: 'button',text: 'Bajar PDF',id: 'btnBajarPdfNotas',hidden: true}]
				}
			});
		}

		var btnGenerar = Ext.getCmp('btnPdfNotas');
		btnGenerar.setHandler(
			function(boton, evento) {
			boton.disable();
			boton.setIconClass('loading-indicator');
				Ext.Ajax.request({
					url: '13consulta02ext_not_pdf.jsp',
					params: Ext.apply({icDocumento: notasA.ic_documento,searchNotaAplicada: notasA.notaAplica,icNotaCredito: notasA.notaCredito,searchNotasDocto: notasA.notasDocto}),
					callback: procesarGenerarPdfNotas
				});
			}
		);
		gridNotaVariasData.load();
		/*Ext.Ajax.request({url: '13consulta02ext.data.jsp',params: Ext.apply(
		{informacion: "obtenNotas",icDocumento: ic_documento,searchNotaAplicada: notaAplica,icNotaCredito: notaCredito,searchNotasDocto: notasDocto}),callback: procesarMuestraPanelNotas});
		*/
	}

	var procesarCatalogoFactorajeData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		var noAplicaCatalogoFactoraje = jsonData.noAplica;
		var factorajeCmb = Ext.getCmp('tipoFactorajeCmb_1');		
		if (noAplicaCatalogoFactoraje) {
			factorajeCmb.hide();
		} else {
			factorajeCmb.show();
		}
	}
/*
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnBajarPDF.setIconClass('icoPdf');
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarPDF').enable();
		Ext.getCmp('btnGenerarPDF').setIconClass('icoPdf');
	}

	var procesarGenerarPdfDetalle =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfDetalle');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfDetalle');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarPdfAcuse =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfAcuse');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfAcuse');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarCsvAcuse =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnCsvAcuse');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerar.disable();
			var btnBajar = Ext.getCmp('btnBajarCsvAcuse');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajar.focus();
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarPdfAplica =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfAplica');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfAplica');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarCsvAplica =  function(opts, success, response) {
		var btnGenerar = Ext.getCmp('btnCsvAplica');
		btnGenerar.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerar.disable();
			var btnBajar = Ext.getCmp('btnBajarCsvAplica');
			btnBajar.show();
			btnBajar.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajar.focus();
			btnBajar.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerar.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarPdfModifica =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfModifica');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfModifica');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarGenerarPdfNotas =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnPdfNotas');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPdfNotas');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarNotaVarias = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			Ext.getCmp('winNotas').show();
			var elG = gridNotaVarias.getGridEl();
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnPdfNotas').setDisabled(false);
				elG.unmask();
			}else{
				elG.mask('No encontro ning�n registro');
			}
		}
	}
/*
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.setIconClass('icoXls');
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
*/
	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			var infoR = Ext.util.JSON.decode(response.responseText);
			var archivo = infoR.urlArchivo;
			archivo = archivo.replace('/nafin','');
			var params = {nombreArchivo: archivo};
			fp.getForm().getEl().dom.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
			fp.getForm().getEl().dom.submit();
		} else{
			NE.util.mostrarConnError(response,opts);
		}
		Ext.getCmp('btnGenerarArchivo').enable();
		Ext.getCmp('btnGenerarArchivo').setIconClass('icoXls');
	}

	var procesarResumenTotalesData = function(store, arrRegistros, opts) {
		if (arrRegistros != null) {
			var gridTotales = Ext.getCmp('gridTotales');
			var elTotal = gridTotales.getGridEl();
			var cm = gridTotales.getColumnModel();

			if(store.getTotalCount() > 0) {
				var storeGeneral = consultaData;
				var hayDoctosNegociablesMN=false,	hayDoctosNegociablesDOLARES = false;
				if(storeGeneral.getTotalCount() > 0) {
					var estatusCmb = Ext.getCmp('cambioEstatusCmb').getValue();
					var contMandante = 0;
					storeGeneral.each(function(registro){
						if (estatusCmb == ("") || estatusCmb == ("1") || estatusCmb == ("2") || estatusCmb == ("5") || estatusCmb == ("6") || 
							estatusCmb == ("7") || estatusCmb == ("9") || estatusCmb == ("10") || estatusCmb == ("21") || estatusCmb == ("23") )	{
								if(jsonParametrizacion.bTipoFactoraje) {
									contMandante ++;
								}
							}
						if (registro.get('IC_ESTATUS_DOCTO') == "2" && registro.get('IC_MONEDA') == "1")	{
							hayDoctosNegociablesMN = true;
						}
						if (registro.get('IC_ESTATUS_DOCTO') == "2" && registro.get('IC_MONEDA') == "54")	{
							hayDoctosNegociablesDOLARES=true;
						}
						
					});
				}
				var existenDocMN=false,existenDocNegociablesMN=false,existenNotasNegociablesMN=false,existenDocDolares=false,
					existenDocNegociablesDolares=false,existenNotasNegociablesDolares=false,mostrarSaldoMN=false,mostrarSaldoDolares=false;
				var montoMN_NC="0.0",montoDolares_NC="0.0",montoMN_Nego="0.0",montoDolares_Nego="0.0",montoMN="0.0",montoDolares="0.0";
				
				var jsonData = store.reader.jsonData.registros;
				
				Ext.each(jsonData, function(registro, index, arrItems){
					var reg = resumenTotalesData.getAt(index);
					if (registro.col4	== "D 1"		) {existenDocMN=true; montoMN=registro.col1; }
					else if (registro.col4	== "D 54"	) {existenDocDolares=true;  montoDolares=registro.col1; }
					else if (registro.col4	== "DN 1"	) {existenDocNegociablesMN=true; mostrarSaldoMN=true; montoMN_Nego=registro.col1; }
					else if(registro.col4	== "DN 54"	){existenDocNegociablesDolares=true; mostrarSaldoDolares=true; montoDolares_Nego=registro.col1; }
					else if(registro.col4	== "NC 1"	) {existenNotasNegociablesMN=true;mostrarSaldoMN=true; montoMN_NC = registro.col1;}
					else if(registro.col4	== "NC 54"	) {existenNotasNegociablesDolares=true;mostrarSaldoDolares=true; montoDolares_NC = registro.col1;}

					if(jsonParametrizacion.bOperaFactorajeNotaDeCredito && hayDoctosNegociablesDOLARES){
						mostrarSaldoDolares=true;
					}
					
					if (jsonParametrizacion.bOperaFactorajeNotaDeCredito && hayDoctosNegociablesMN)	{
						mostrarSaldoMN=true;
					}

					if (existenDocMN == true){
						reg.set('MONEDA',"MONTO TOTAL DOCUMENTOS " + registro.col3);
						var cmbFactoraje = Ext.getCmp("tipoFactorajeCmb");
						if (cmbFactoraje.getValue() == 'M') {
							reg.set('TOTAL_DOCUMENTOS',contMandante);
						}else	{
							reg.set('TOTAL_DOCUMENTOS',registro.col0);
						}
						existenDocMN=false;
					}
					if (existenDocDolares == true){
						reg.set('MONEDA',"MONTO TOTAL DOCUMENTOS " + registro.col3);
						existenDocDolares=false;
					}
					if(existenDocNegociablesMN == true){
						reg.set('MONEDA',"MONTO TOTAL DOCUMENTOS NEGOCIABLES " + registro.col3);
						existenDocNegociablesMN=false;
					}
					if(existenDocNegociablesDolares == true){
						reg.set('MONEDA',"MONTO TOTAL DOCUMENTOS NEGOCIABLES " + registro.col3);
						existenDocNegociablesDolares=false;
					}
					if(existenNotasNegociablesMN == true){
						reg.set('MONEDA',"MONTO TOTAL NOTAS DE CREDITO " + registro.col3);
						existenNotasNegociablesMN=false;
					}
					if(existenNotasNegociablesDolares == true){
						reg.set('MONEDA',"MONTO TOTAL NOTAS DE CREDITO " + registro.col3);
						existenNotasNegociablesDolares=false;
					}
				});
				if(mostrarSaldoMN == true){
				var saldoMN = 0;
					if(montoMN_Nego == "0.0"){ montoMN_Nego = montoMN;}
					if(parseFloat(montoMN_NC) > parseFloat(montoMN_Nego)){
						saldoMN = Math.abs((parseFloat(montoMN_Nego))-(parseFloat(montoMN_NC)));
					} else {
						saldoMN = ((parseFloat(montoMN_Nego))-(parseFloat(montoMN_NC)));
					}
					var reg = Ext.data.Record.create(['TOTAL_DOCUMENTOS', 'MONTO_DOCUMENTOS','MONTO_DOCUMENTOS_DESCONTAR','MONEDA']);
					store.add(
						new reg({
							TOTAL_DOCUMENTOS: '',
							MONTO_DOCUMENTOS: saldoMN,
							MONTO_DOCUMENTOS_DESCONTAR: '',
							MONEDA:'TOTAL MONEDA NACIONAL SALDO NEGOCIABLE'
						})
					);
				}
				if(mostrarSaldoDolares == true){
					var saldoDL = 0;
					if(montoDolares_Nego == "0.0"){ montoDolares_Nego = montoDolares;}
					if(parseFloat(montoDolares_NC) > parseFloat(montoDolares_Nego)){
						saldoDL = Math.abs((parseFloat(montoDolares_Nego))-(parseFloat(montoDolares_NC)));
					}else	{
						saldoDL = ((parseFloat(montoDolares_Nego))-(parseFloat(montoDolares_NC)));
					}
					var reg = Ext.data.Record.create(['TOTAL_DOCUMENTOS', 'MONTO_DOCUMENTOS','MONTO_DOCUMENTOS_DESCONTAR','MONEDA']);
					store.add(
						new reg({
							TOTAL_DOCUMENTOS: '',
							MONTO_DOCUMENTOS: saldoDL,
							MONTO_DOCUMENTOS_DESCONTAR: '',
							MONEDA:'TOTAL DOLAR AMERICANO SALDO NEGOCIABLE'
						})
					);
				}
				if (!gridTotales.isVisible()) {
					gridTotales.show();
					gridTotales.el.dom.scrollIntoView();
				}
				elTotal.unmask();
				Ext.getCmp('btnTotales').enable();
			}else {
				Ext.getCmp('btnTotales').disable();
				elTotal.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			//var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			//var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			var btnTotales = Ext.getCmp('btnTotales');
			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnTotales').disable();
				Ext.getCmp('btnGenerarArchivo').disable();
				//Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnGenerarPDF').disable();
				//Ext.getCmp('btnBajarPDF').hide();

		//Columnas que utilizamos para ocultar/mostrar
				var cm = grid.getColumnModel();
				cm.setHidden(cm.findColumnIndex('NOMBRE_IF'), true);
				cm.setHidden(cm.findColumnIndex('IG_NUMERO_DOCTO'), true);
				cm.setHidden(cm.findColumnIndex('FN_MONTO'), true);
				cm.setHidden(cm.findColumnIndex('FN_PORC_ANTICIPO'), true);
				cm.setHidden(cm.findColumnIndex('FN_MONTO_DSCTO'), true);
				cm.setHidden(cm.findColumnIndex('IN_TASA_ACEPTADA'), true);
				cm.setHidden(cm.findColumnIndex('DF_FECHA_SOLICITUD'), true);
				cm.setHidden(cm.findColumnIndex('IC_DOCUMENTO'), true);
				cm.setHidden(cm.findColumnIndex('IN_IMPORTE_RECIBIR'), true);
				cm.setHidden(cm.findColumnIndex('CS_CAMBIO_IMPORTE'), true);
				cm.setHidden(cm.findColumnIndex('FN_MONTO_PAGO'), true);
				cm.setHidden(cm.findColumnIndex('CD_DESCRIPCION'), true);
				//cm.setHidden(cm.findColumnIndex('CS_DETALLE'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO1'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO2'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO3'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO4'), true);
				cm.setHidden(cm.findColumnIndex('CG_CAMPO5'), true);
				cm.setHidden(cm.findColumnIndex('CS_DSCTO_ESPECIAL'), true);
				cm.setHidden(cm.findColumnIndex('CC_ACUSE'), true);
				cm.setHidden(cm.findColumnIndex('NETO_RECIBIR'), true);
				cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
				cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
				cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
				cm.setHidden(cm.findColumnIndex('PORCDOCTOAPLICADO'), true);
				cm.setHidden(cm.findColumnIndex('IG_PLAZO'), true);
				cm.setHidden(cm.findColumnIndex('IN_IMPORTE_INTERES'), true);
				cm.setHidden(cm.findColumnIndex('FN_REMANENTE'), true);
				cm.setHidden(cm.findColumnIndex('CT_REFERENCIA'), true);
				cm.setHidden(cm.findColumnIndex('DF_ENTREGA'), true);
				cm.setHidden(cm.findColumnIndex('CG_TIPO_COMPRA'), true);
				cm.setHidden(cm.findColumnIndex('CG_CLAVE_PRESUPUESTARIA'), true);
				cm.setHidden(cm.findColumnIndex('CG_PERIODO'), true);
				cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
				cm.setHidden(cm.findColumnIndex('FECHA_REGISTRO_OPERACION'), true);
				cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
				cm.setHidden(cm.findColumnIndex('NUMERO_DOCTO'), true);
				var estatusCmb = Ext.getCmp('cambioEstatusCmb').getValue();
				var noEpo = Ext.getCmp('cmbEpo');
				if (estatusCmb == ("") || estatusCmb == ("1") || estatusCmb == ("2") || estatusCmb == ("5") || estatusCmb == ("6") || 
					estatusCmb == ("7") || estatusCmb == ("9") || estatusCmb == ("10") || estatusCmb == ("21") || estatusCmb == ("23") )	{
					var ic_epo_aserca = "314";
					var layOutAserca = false;
					if (estatusCmb == "1" && ic_epo_aserca == noEpo.value)	{
						layOutAserca = true;
					}
					if (!layOutAserca)	{
						cm.setHidden(cm.findColumnIndex('NOMBRE_IF'), false);
						cm.setHidden(cm.findColumnIndex('IG_NUMERO_DOCTO'), false);
						if(jsonParametrizacion.bTipoFactoraje) {
							cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
						}
						if(jsonParametrizacion.bOperaFactorajeDistribuido || jsonParametrizacion.bOperaFactVencimientoInfonavit) {
							cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
							cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
							cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
						}
						cm.setHidden(cm.findColumnIndex('CT_REFERENCIA'), false);
						cm.setHidden(cm.findColumnIndex('IC_DOCUMENTO'), false);
						cm.setHidden(cm.findColumnIndex('CS_CAMBIO_IMPORTE'), false);
					}
					cm.setHidden(cm.findColumnIndex('FN_MONTO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_ANTICIPO'), false);
					cm.setHidden(cm.findColumnIndex('FN_MONTO_DSCTO'), false);
					cm.setHidden(cm.findColumnIndex('CD_DESCRIPCION'), false);

					var camposDinamicos = jsonParametrizacion.camp_dina1;
					var contenedorIzq = Ext.getCmp('camposDic');
					var cmbEpo = Ext.getCmp('cmbEpo');
					if (camposDinamicos != undefined){
						var cont = 1;
						Ext.each(camposDinamicos, function(item, index, arrItems){
							if (item.IC_NO_CAMPO == "1" && cont == 1) {
								cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO1'), item.NOMBRE_CAMPO);
								cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO1'), item.NOMBRE_CAMPO);
								cm.setHidden(cm.findColumnIndex('CG_CAMPO1'), false);
							}
							if (item.IC_NO_CAMPO == "2" && cont == 2) {
								cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO2'), item.NOMBRE_CAMPO);
								cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO2'), item.NOMBRE_CAMPO);
								cm.setHidden(cm.findColumnIndex('CG_CAMPO2'), false);	
							}
							if (item.IC_NO_CAMPO == "3" && cont == 3) {
								cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO3'), item.NOMBRE_CAMPO);
								cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO3'), item.NOMBRE_CAMPO);
								cm.setHidden(cm.findColumnIndex('CG_CAMPO3'), false);
							}
							if (item.IC_NO_CAMPO == "4" && cont == 4) {
								cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO4'), item.NOMBRE_CAMPO);
								cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO4'), item.NOMBRE_CAMPO);
								cm.setHidden(cm.findColumnIndex('CG_CAMPO4'), false);
							}
							if (item.IC_NO_CAMPO == "5" && cont == 5) {
								cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO5'), item.NOMBRE_CAMPO);
								cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO5'), item.NOMBRE_CAMPO);
								cm.setHidden(cm.findColumnIndex('CG_CAMPO5'), false);
							}
							cont ++;
						});
					}

					if(!ocultarDoctosAplicados){
						cm.setHidden(cm.findColumnIndex('NUMERO_DOCTO'), false);
					}
					if (jsonParametrizacion.banderaTablaDoctos != undefined && jsonParametrizacion.banderaTablaDoctos == "1")	{
						cm.setHidden(cm.findColumnIndex('DF_ENTREGA'), false);
						cm.setHidden(cm.findColumnIndex('CG_TIPO_COMPRA'), false);
						cm.setHidden(cm.findColumnIndex('CG_CLAVE_PRESUPUESTARIA'), false);
						cm.setHidden(cm.findColumnIndex('CG_PERIODO'), false);
					}
					var cmbFactoraje = Ext.getCmp("tipoFactorajeCmb");
					if (cmbFactoraje.getValue() == 'M' || cmbFactoraje.getValue() == '') {
						cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
					}
				}
				if (estatusCmb == ("3") || estatusCmb == ("4") || estatusCmb == ("8") || estatusCmb == ("11") || 
					estatusCmb == ("12") || estatusCmb == ("16") || estatusCmb == ("24") || estatusCmb == ("26") )	{
					cm.setHidden(cm.findColumnIndex('NOMBRE_IF'), false);
					cm.setHidden(cm.findColumnIndex('IG_NUMERO_DOCTO'), false);
					cm.setHidden(cm.findColumnIndex('CC_ACUSE'), false);
					cm.setHidden(cm.findColumnIndex('IG_PLAZO'), false);
					if(jsonParametrizacion.bTipoFactoraje) {
						cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
					}
					cm.setHidden(cm.findColumnIndex('FN_MONTO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_ANTICIPO'), false);
					cm.setHidden(cm.findColumnIndex('FN_MONTO_DSCTO'), false);
					cm.setHidden(cm.findColumnIndex('IN_IMPORTE_INTERES'), false);
					cm.setHidden(cm.findColumnIndex('IN_IMPORTE_RECIBIR'), false);
					cm.setHidden(cm.findColumnIndex('IN_TASA_ACEPTADA'), false);
					cm.setHidden(cm.findColumnIndex('CD_DESCRIPCION'), false);
					cm.setHidden(cm.findColumnIndex('DF_FECHA_SOLICITUD'), false);
					cm.setHidden(cm.findColumnIndex('FN_MONTO_PAGO'), false);
					cm.setHidden(cm.findColumnIndex('PORCDOCTOAPLICADO'), false);
					cm.setHidden(cm.findColumnIndex('FN_REMANENTE'), false);
					if(jsonParametrizacion.bOperaFactorajeDistribuido || jsonParametrizacion.bOperaFactVencimientoInfonavit) {
						cm.setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);
						cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
						cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
						cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
					}
					cm.setHidden(cm.findColumnIndex('CS_DSCTO_ESPECIAL'), false);
					var camposDinamicos = jsonParametrizacion.camp_dina1;
					var contenedorIzq = Ext.getCmp('camposDic');
					var cmbEpo = Ext.getCmp('cmbEpo');
					if (camposDinamicos != undefined){
						var cont = 1;
						Ext.each(camposDinamicos, function(item, index, arrItems){
							if (item.IC_NO_CAMPO == "1" && cont == 1) {
								cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO1'), item.NOMBRE_CAMPO);
								cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO1'), item.NOMBRE_CAMPO);
								cm.setHidden(cm.findColumnIndex('CG_CAMPO1'), false);
							}
							if (item.IC_NO_CAMPO == "2" && cont == 2) {
								cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO2'), item.NOMBRE_CAMPO);
								cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO2'), item.NOMBRE_CAMPO);
								cm.setHidden(cm.findColumnIndex('CG_CAMPO2'), false);	
							}
							if (item.IC_NO_CAMPO == "3" && cont == 3) {
								cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO3'), item.NOMBRE_CAMPO);
								cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO3'), item.NOMBRE_CAMPO);
								cm.setHidden(cm.findColumnIndex('CG_CAMPO3'), false);
							}
							if (item.IC_NO_CAMPO == "4" && cont == 4) {
								cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO4'), item.NOMBRE_CAMPO);
								cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO4'), item.NOMBRE_CAMPO);
								cm.setHidden(cm.findColumnIndex('CG_CAMPO4'), false);
							}
							if (item.IC_NO_CAMPO == "5" && cont == 5) {
								cm.setColumnHeader(cm.findColumnIndex('CG_CAMPO5'), item.NOMBRE_CAMPO);
								cm.setColumnTooltip(cm.findColumnIndex('CG_CAMPO5'), item.NOMBRE_CAMPO);
								cm.setHidden(cm.findColumnIndex('CG_CAMPO5'), false);
							}
							cont ++;
						});
					}
					cm.setHidden(cm.findColumnIndex('CT_REFERENCIA'), false);
					cm.setHidden(cm.findColumnIndex('IC_DOCUMENTO'), false);
					cm.setHidden(cm.findColumnIndex('CS_CAMBIO_IMPORTE'), false);
					if(!ocultarDoctosAplicados){
						cm.setHidden(cm.findColumnIndex('NUMERO_DOCTO'), false);
					}
					if (jsonParametrizacion.banderaTablaDoctos != undefined && jsonParametrizacion.banderaTablaDoctos == "1")	{
						cm.setHidden(cm.findColumnIndex('DF_ENTREGA'), false);
						cm.setHidden(cm.findColumnIndex('CG_TIPO_COMPRA'), false);
						cm.setHidden(cm.findColumnIndex('CG_CLAVE_PRESUPUESTARIA'), false);
						cm.setHidden(cm.findColumnIndex('CG_PERIODO'), false);
					}
					var cmbFactoraje = Ext.getCmp("tipoFactorajeCmb");
					if (cmbFactoraje.getValue() == 'M' || cmbFactoraje.getValue() == '') {
						cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
					}
					if (estatusCmb == "26")	{
						cm.setHidden(cm.findColumnIndex('FECHA_REGISTRO_OPERACION'), false);
					}
				}
		//termina de ocultar/mostrar columnas			
				btnTotales.enable();
				btnGenerarPDF.setIconClass('icoPdf');
				btnGenerarPDF.enable();
				btnGenerarArchivo.setIconClass('icoXls');
				btnGenerarArchivo.enable();
				/*btnBajarPDF.hide();
				if(!btnBajarArchivo.isVisible()) {
					btnGenerarArchivo.setIconClass('icoXls');
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}*/
				el.unmask();
			} else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				btnBajarPDF.hide();
				//btnBajarArchivo.hide();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
//*-*-*-*-FIN*-*-*-*-*HANDLERS*-*-*-*-*-*FIN
//*-*-INIT*-*-*-*-*-*STORE�S-*-*-*-*-*-*-INIT
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPOPyme'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoIfData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoIf'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,//	El valor inicial es false
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCambioEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,	//	el valor inicial es false
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoFactorajeData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoFactoraje'
		},
		totalProperty : 'total',
		autoLoad: false,		//el valor inicial es false
		listeners: {
			load: procesarCatalogoFactorajeData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//catalogoMandanteData,

	var catalogoMandanteData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMandante'
		},
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoCamposAdicionalesData = new Ext.data.JsonStore({
		id: 'catalogoCamposAdicionalesDataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCamposAdicionales'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoCamposAdicionales_2_Data = new Ext.data.JsonStore({
		id: 'catalogoCamposAdicionales_2_DataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCamposAdicionales_2'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NOMBRE_EPO'}, //Nom. EPO
			{name: 'NOMBRE_IF'}, //Nombre if
			{name: 'IG_NUMERO_DOCTO'}, //Num docto.
			{name: 'DF_FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'}, //Moneda
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_PORC_ANTICIPO'},
			{name: 'FN_MONTO_DSCTO', type: 'float'},
			{name: 'IN_TASA_ACEPTADA'},
			{name: 'DF_FECHA_SOLICITUD', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'IC_MONEDA'},
			{name: 'IC_DOCUMENTO'},
			{name: 'IN_IMPORTE_RECIBIR', type: 'float'},
			{name: 'CS_CAMBIO_IMPORTE'},
			{name: 'IC_ESTATUS_DOCTO'},
			{name: 'FN_MONTO_PAGO', type: 'float'},
			{name: 'CD_DESCRIPCION'},
			{name: 'CS_DETALLE'},
			{name: 'CG_CAMPO1'},
			{name: 'CG_CAMPO2'},
			{name: 'CG_CAMPO3'},
			{name: 'CG_CAMPO4'},
			{name: 'CG_CAMPO5'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'CC_ACUSE'},
			{name: 'NETO_RECIBIR', type: 'float'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO', type: 'float'},
			{name: 'PORCDOCTOAPLICADO'},
			{name: 'IG_PLAZO'},
			{name: 'IN_IMPORTE_INTERES', type: 'float'},
			{name: 'IG_TIPO_PISO'},
			{name: 'FN_REMANENTE', type: 'float'},
			{name: 'CT_REFERENCIA'},
			{name: 'DF_ENTREGA'},
			{name: 'CG_TIPO_COMPRA'},
			{name: 'CG_CLAVE_PRESUPUESTARIA'},
			{name: 'CG_PERIODO'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'FECHA_REGISTRO_OPERACION', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'MANDANTE'},
			{name: 'NOTA_SIMPLE'},
			{name: 'NOTA_SIMPLE_DOCTO'},
			{name: 'NOTA_MULTIPLE'},
			{name: 'NOTA_MULTIPLE_DOCTO'},
			{name: 'NUMERO_DOCTO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
							var noEpo = Ext.getCmp('cmbEpo');
							var _mandanteCmb = Ext.getCmp('mandanteCmb');
							var tipoFactoraje = Ext.getCmp('tipoFactorajeCmb');
							var combo_estatus = Ext.getCmp('cambioEstatusCmb');
							var vencePyme = Ext.getCmp('hidenVencePyme');
							Ext.apply(options.params, {no_epo: noEpo.value,mandant:_mandanteCmb.value,tipoFactoraje:tipoFactoraje.value,no_estatus_docto: combo_estatus.value,sFechaVencPyme:vencePyme.value});
							}
			},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaData(null, null, null);
				}
			}
		}
		
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta02ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'TOTAL_DOCUMENTOS', type: 'float', mapping: 'col0'},
			{name: 'MONTO_DOCUMENTOS', type: 'float', mapping: 'col1'},
			{name: 'MONTO_DOCUMENTOS_DESCONTAR', type: 'float', mapping: 'col2'},
			{name: 'MONEDA', mapping: 'col3'}
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarResumenTotalesData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarResumenTotalesData(null, null, null);
				}
			}
		}
		
	});

	var gridDetalleData = new Ext.data.JsonStore({
		fields: [{name: 'CG_CAMPO1'},{name: 'CG_CAMPO2'},{name: 'CG_CAMPO3'},{name: 'CG_CAMPO4'},{name: 'CG_CAMPO5'},{name: 'CG_CAMPO6'},{name: 'CG_CAMPO7'},{name: 'CG_CAMPO8'},{name: 'CG_CAMPO9'},{name: 'CG_CAMPO10'}],
		data: [{'CG_CAMPO1':'','CG_CAMPO2':'','CG_CAMPO3':'','CG_CAMPO4':'','CG_CAMPO5':'','CG_CAMPO6':'','CG_CAMPO7':'','CG_CAMPO8':'','CG_CAMPO9':'','CG_CAMPO10':''}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridAcuseData = new Ext.data.JsonStore({
		fields:	[{name: 'NOMBRE_IF'},{name: 'NOMBRE_EPO'},{name: 'IG_NUMERO_DOCTO'},{name: 'CD_NOMBRE'},{name: 'FN_MONTO',type: 'float'},{name: 'FN_PORC_ANTICIPO',type: 'float'},{name: 'FN_MONTO_DSCTO',type: 'float'},{name: 'IN_IMPORTE_INTERES',	type: 'float'},{name: 'IN_IMPORTE_RECIBIR',	type: 'float'}],
		data:		[{'NOMBRE_IF':'','NOMBRE_EPO':'','IG_NUMERO_DOCTO':'','CD_NOMBRE':'','FN_MONTO':0,'FN_PORC_ANTICIPO':0,'FN_MONTO_DSCTO':0,'IN_IMPORTE_INTERES':0,'IN_IMPORTE_RECIBIR':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridTotalAcuseData = new Ext.data.JsonStore({
		fields:	[{name: 'CD_NOMBRE'},{name: 'NOMBRE_EPO'},{name: 'TOTAL_REGISTROS',type: 'float'},{name: 'TOTAL_MONTO',type: 'float'},{name: 'TOTAL_MONTO_DSCTO',type: 'float'},{name: 'TOTAL_INTERES',type: 'float'},{name: 'TOTAL_RECIBIR',type: 'float'}],
		data:		[{'CD_NOMBRE':'','NOMBRE_EPO':'','TOTAL_REGISTROS':0,'TOTAL_MONTO':0,'TOTAL_MONTO_DSCTO':0,'TOTAL_INTERES':0,'TOTAL_RECIBIR':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridAplicaData = new Ext.data.JsonStore({
		fields:	[{name: 'IC_MONEDA'},{name: 'NOMBRE_IF'},{name: 'IC_NOMBRE'},{name: 'DF_FECHA_SELECCION',type: 'date', dateFormat: 'd/m/Y'},{name: 'DF_OPERACION',type: 'date', dateFormat: 'd/m/Y'},{name: 'IC_MENSUALIDAD'},{name: 'NUMERO_CREDITO'},{name: 'CG_TIPO_CONTRATO'},{name: 'IG_NUMERO_PRESTAMO',type: 'float'},{name: 'IMPORTE_APLICADO_CREDITO',	type: 'float'},{name: 'SALDO_CREDITO',	type: 'float'},{name: 'ORDEN_APLICA',type: 'float'}],
		data:		[{'IC_MONEDA':'','NOMBRE_IF':'','IC_NOMBRE':'','DF_FECHA_SELECCION':'','DF_OPERACION':'','IC_MENSUALIDAD':'','NUMERO_CREDITO':'','CG_TIPO_CONTRATO':'','IG_NUMERO_PRESTAMO':0,'IMPORTE_APLICADO_CREDITO':0,'SALDO_CREDITO':0,'ORDEN_APLICA':0}],
		totalProperty: 'total',
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridTotalAplicaData = new Ext.data.JsonStore({
		fields:	[{name: 'TOTAL'},{name: 'TOTAL_IMPORTE_APLICADO',type: 'float'},{name: 'TOTAL_SALDO_CREDITO',type: 'float'},{name: 'TOTAL_IMPORTE_DEPOSITAR',type: 'float'}],
		data:		[{'TOTAL':'MONEDA NACIONAL','TOTAL_IMPORTE_APLICADO':0,'TOTAL_SALDO_CREDITO':0,'TOTAL_IMPORTE_DEPOSITAR':0},
						{'TOTAL':'DOLAR AMERICANOS','TOTAL_IMPORTE_APLICADO':0,'TOTAL_SALDO_CREDITO':0,'TOTAL_IMPORTE_DEPOSITAR':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}		
	});
	
	var myData = [	{'CC_ACUSE':'ASD','CT_CAMBIO_MOTIVO':'ASD','FEC_VENC_ANTERIOR':'ASD','FEC_VENC_NUEVA':'AD','FECHA_CAMBIO':'ASD','FN_MONTO_ANTERIOR':0,'FN_MONTO_NUEVO':0},
					{'CC_ACUSE':'ASD','CT_CAMBIO_MOTIVO':'ASD','FEC_VENC_ANTERIOR':'ASD','FEC_VENC_NUEVA':'AD','FECHA_CAMBIO':'ASD','FN_MONTO_ANTERIOR':1,'FN_MONTO_NUEVO':2}];

	var gridModificaData = new Ext.data.JsonStore({
		fields:	[{name: 'FECHA_CAMBIO',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'FN_MONTO_NUEVO',type: 'float'},
					{name: 'FN_MONTO_ANTERIOR',	type: 'float'},
					{name: 'FEC_VENC_NUEVA',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'FEC_VENC_ANTERIOR',type: 'date', dateFormat: 'd/m/Y'},
					{name: 'CT_CAMBIO_MOTIVO'},
					{name: 'CC_ACUSE'}],
		data:		[{'CC_ACUSE':'','CT_CAMBIO_MOTIVO':'','FEC_VENC_ANTERIOR':'','FEC_VENC_NUEVA':'','FECHA_CAMBIO':'','FN_MONTO_ANTERIOR':0,'FN_MONTO_NUEVO':0}],
		autoLoad: true,
		listeners: {exception: NE.util.mostrarDataProxyError}
	});

	var gridNotaVariasData = new Ext.data.GroupingStore({
		root : 'registros',
		url : '/nafin/13descuento/13forma1ext.data.jsp',
		//url : '13forma1ext.data.jsp',
		baseParams: {
			informacion: 'obtenNotaVarias'
		},
		reader: new Ext.data.JsonReader({
		root : 'registros',	totalProperty: 'total',
		fields: [
			{name: 'NOTA_MONTO_CONCAT'},
			{name: 'IG_NUMERO_DOCTO_CREDITO'},
			{name: 'IG_NUMERO_DOCTO_DOCTO'},
			{name: 'FECHA_EMISION',type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FN_MONTO',type: 'float'},
			{name: 'MONEDA'},
			{name: 'MONTO_DOCTO',type: 'float'},
			{name: 'MONTO_APLICA',	type: 'float'},
			{name: 'SALDO_DOCTO',	type: 'float'},
			{name: 'TIPO_FACTORAJE'}
      ]
		}),
		groupField: 'NOTA_MONTO_CONCAT',	//sortInfo:{field: 'NOTA_MONTO_CONCAT', direction: "ASC"},
		totalProperty : 'total', messageProperty: 'msg', autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
				Ext.apply(options.params, {icDocumento: notasA.ic_documento,searchNotaAplicada: notasA.notaAplica,icNotaCredito: notasA.notaCredito,searchNotasDocto: notasA.notasDocto});
				}
			},
			load: procesarNotaVarias,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarNotaVarias(null, null, null);
				}
			}
		}
	});

//*-*-FIN*-*-*-*-*-*STORE�S-*-*-*-*-*-*-*FIN

	var gridTotales = new Ext.grid.GridPanel({
		//xtype: 'gridTotales',
		store: resumenTotalesData,
		id: 'gridTotales',
		hidden: true,
		columns: [
			{
				header: '&nbsp;',
				dataIndex: 'MONEDA',
				align: 'left',width: 360, resizable: false
			},{
				header: 'Registros',
				dataIndex: 'TOTAL_DOCUMENTOS',
				width: 110,	align: 'right',renderer: Ext.util.Format.numberRenderer('0,000'), hidden: false, resizable: false
			},{
				header: 'Monto',
				dataIndex: 'MONTO_DOCUMENTOS',
				width: 201,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: false, resizable: false
			},{
				header: 'Monto a Descontar',
				dataIndex: 'MONTO_DOCUMENTOS_DESCONTAR',
				width: 201,align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: false, resizable: false
			}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		width: 940,
		height: 240,
		loadMask: true,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	});

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'EPO',
				dataIndex: 'NOMBRE_EPO',
				sortable: true, width: 200, resizable: true, hidden: false
			},{
				header: 'IF', tooltip: 'Nombre IF',
				dataIndex: 'NOMBRE_IF',
				sortable: true,	width: 200,	resizable: true, hidden: true, hideable: false
			},{
				header : 'Num. Documento', tooltip: 'Numero de Documento',
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 100, sortable : true, hidden: true, hideable: false
			},{
				xtype:	'actioncolumn',
				header : 'Num. Acuse', tooltip: 'Num. Acuse',
				dataIndex : 'CC_ACUSE',
				width:	150,	align: 'center', hidden: true, hideable: false,
				renderer: function(value, metadata, record, rowindex, colindex, store) {
								return (record.get('CC_ACUSE'));
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('CS_DSCTO_ESPECIAL') != "C")	{
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}else	{
								return value;
							}
						},
						handler:	muestraPanelAcuse
					}
				]
			},{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',
				dataIndex : 'DF_FECHA_DOCTO',
				sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: false
			},{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'DF_FECHA_VENC',
				sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: false
			},{
				header : 'Plazo', tooltip: 'Plazo',
				dataIndex : 'IG_PLAZO',
				sortable : true, width : 100, hidden: true, hideable: false
			},{
				header : 'Moneda', tooltip: 'Moneda',
				dataIndex : 'CD_NOMBRE',
				sortable : true, width : 150, hidden: false
			},{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true, hideable: false
			},{
				header : 'Monto', tooltip: 'Monto del Documento',
				dataIndex : 'FN_MONTO',
				sortable : true, width : 100, align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00'), hidden: true, hideable: false
			},{
				header : 'Porcentaje de Descuento', tooltip: 'Porcentaje de Descuento',
				dataIndex : 'FN_PORC_ANTICIPO',
				sortable : true, width : 150,	align: 'right', resizable: true, hidden: true,hideable: false,
				renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
							var estatusCmb = Ext.getCmp('cambioEstatusCmb').getValue();
							if (estatusCmb == ("") || estatusCmb == ("1") || estatusCmb == ("2") || estatusCmb == ("5") || estatusCmb == ("6") || 
								 estatusCmb == ("7") || estatusCmb == ("9") || estatusCmb == ("10") || estatusCmb == ("21") || estatusCmb == ("23") )	{
								var porciento = 0;
								var fnPorciento = value;
								if (jsonParametrizacion.dblPorciento != undefined){
										porciento = jsonParametrizacion.dblPorciento;
								}
								if (registro.get('IC_MONEDA') == "54"){
									if (jsonParametrizacion.dblPorcientoDL != undefined){
										porciento = jsonParametrizacion.dblPorcientoDL;
									}
								}
								if ( registro.get('IC_ESTATUS_DOCTO') == ("2") || registro.get('IC_ESTATUS_DOCTO') == ("5") || registro.get('IC_ESTATUS_DOCTO') == ("6") || registro.get('IC_ESTATUS_DOCTO') == ("7") ){
									value = porciento * 100;
								} else	{
									value = fnPorciento;
								}
								if (jsonParametrizacion.bTipoFactoraje != undefined){
									if (jsonParametrizacion.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}	
								}
							}
							if (estatusCmb == ("3") || estatusCmb == ("4") || estatusCmb == ("8") || estatusCmb == ("11") || 
								 estatusCmb == ("12") || estatusCmb == ("16") || estatusCmb == ("24") || estatusCmb == ("26") )	{
								if (jsonParametrizacion.bTipoFactoraje != undefined){
									if (jsonParametrizacion.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
							}
							return Ext.util.Format.number(value,'0%');
							}
			},{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	
				dataIndex : 'FN_MONTO_DSCTO',  //Calculado
				sortable : true, width : 100,	align: 'right', hidden: true,hideable: false,
				renderer: function(value, metaData, registro, rowIndex, colIndex, store) {
							var estatusCmb = Ext.getCmp('cambioEstatusCmb').getValue();
							if (estatusCmb == ("") || estatusCmb == ("1") || estatusCmb == ("2") || estatusCmb == ("5") || estatusCmb == ("6") || 
								 estatusCmb == ("7") || estatusCmb == ("9") || estatusCmb == ("10") || estatusCmb == ("21") || estatusCmb == ("23") )	{
								var porciento = 0;
								if (jsonParametrizacion.dblPorciento != undefined){
										porciento = jsonParametrizacion.dblPorciento;
								}
								if (registro.get('IC_MONEDA') == "54"){
									if (jsonParametrizacion.dblPorcientoDL != undefined){
										porciento = jsonParametrizacion.dblPorcientoDL;
									}
								}
								if ( registro.get('IC_ESTATUS_DOCTO') == ("2") || registro.get('IC_ESTATUS_DOCTO') == ("5") || registro.get('IC_ESTATUS_DOCTO') == ("6") || registro.get('IC_ESTATUS_DOCTO') == ("7") ){
									value = (registro.get('FN_MONTO')*porciento);
								} else	{
									//value = registro.get('FN_MONTO_DSCTO');
								}
								if (jsonParametrizacion.bTipoFactoraje != undefined){
									if (jsonParametrizacion.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('FN_MONTO');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('FN_MONTO');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('FN_MONTO');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('FN_MONTO');}
									}
								}
							}else if (estatusCmb == ("3") || estatusCmb == ("4") || estatusCmb == ("8") || estatusCmb == ("11") || 
								estatusCmb == ("12") || estatusCmb == ("16") || estatusCmb == ("24") || estatusCmb == ("26") )	{
								var porciento = registro.get('FN_PORC_ANTICIPO');
								if (jsonParametrizacion.bTipoFactoraje != undefined){
									if (jsonParametrizacion.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V" || registro.get('CS_DSCTO_ESPECIAL') == "D" || 
											registro.get('CS_DSCTO_ESPECIAL') == "C" ||	registro.get('CS_DSCTO_ESPECIAL') == "I")	{
											porciento = 100;
											value = registro.get('FN_MONTO');
										}
									}
								}
								value = (registro.get('FN_MONTO')*(porciento/100))
							} else {
								value = 0;
							}
							return Ext.util.Format.number(value, '$0,0.00');
							}
			},{
				header : 'Intereses del documento', tooltip: 'Intereses del documento',
				dataIndex : 'IN_IMPORTE_INTERES',
				sortable : true, width : 100, align: 'right', resizable: true, hidden: true,hideable: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (registro.get('CS_DSCTO_ESPECIAL') == "C"){
										value = 0;
									}
								return Ext.util.Format.number(value,'$0,0.00');
								}
			},{
				header : 'Monto a Recibir', tooltip: 'Monto a Recibir',
				dataIndex : 'IN_IMPORTE_RECIBIR',
				sortable : true, width : 100, align: 'right', hidden: true,hideable: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (registro.get('CS_DSCTO_ESPECIAL') == "C"){
										value = 0;
									}
								return Ext.util.Format.number(value,'$0,0.00');
								}
			},{
				header : 'Tasa', tooltip: 'Tasa',
				dataIndex : 'IN_TASA_ACEPTADA',
				sortable : true, width : 100, align: 'right', hidden: true,hideable: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (registro.get('CS_DSCTO_ESPECIAL') == "C"){
										value = 0;
									}
								return Ext.util.Format.number(value,'0%');
								}
			},{
				header : 'Estatus', tooltip: 'Cambio de Estatus',
				dataIndex : 'CD_DESCRIPCION',
				sortable : true, width : 200, hidden: true,hideable: false
			},{
				header: 'Beneficiario', tooltip: 'Beneficiario',
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : false ,hidden: true
			},{
				header : 'Fecha Operaci�n IF', tooltip: 'Fecha Operaci�n IF',
				dataIndex : 'DF_FECHA_SOLICITUD',
				sortable : true, width : 100, resizable: true, hidden: true,hideable: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									var estatusCmb = Ext.getCmp('cambioEstatusCmb').getValue();
									if (estatusCmb != "16"){
										value = '';
									}
								return Ext.util.Format.date(value,'d/m/Y');
								}
			},{
				header: 'Importe Aplicado a Cr�dito', tooltip: 'Importe Aplicado a Cr�dito',
				dataIndex: 'FN_MONTO_PAGO',
				sortable: true, width: 130, align: 'right', resizable: true, hideable : false ,hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									var estatusCmb = Ext.getCmp('cambioEstatusCmb').getValue();
									if (estatusCmb != "16"){
										value = 0;
									}
								return Ext.util.Format.number(value,'$0,0.00')
								}
			},{
				header : 'Porcentaje del Documento Aplicado', tooltip: 'Porcentaje del Documento Aplicado', 
				dataIndex : 'PORCDOCTOAPLICADO',
				sortable : true, width : 100, align: 'right', resizable: true, hidden: true,hideable: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									var estatusCmb = Ext.getCmp('cambioEstatusCmb').getValue();
									if (estatusCmb != "16"){
										value = 0;
									}
								return Ext.util.Format.number(value,'0%');
								}
			},{
				header: 'Importe a Depositar PyME', tooltip: 'Importe a Depositar PyME',
				dataIndex: 'FN_REMANENTE',
				sortable: true, width: 100, align: 'right', resizable: true, hidden: true,hideable: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (registro.get('IG_TIPO_PISO') == "1")	{
										if (registro.get('FN_REMANENTE') != "" && registro.get('FN_REMANENTE') != null)	{
											value = registro.get('FN_REMANENTE');
										}else	{
											value = "0";
										}
									}else	{
										if (registro.get('IC_ESTATUS_DOCTO') == 16)	{
											value = (registro.get('IN_IMPORTE_RECIBIR') - registro.get('FN_MONTO_PAGO')).toString();
										}else	{
											value = "0";
										}
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},{
				header: 'Neto a Recibir Pyme', tooltip: 'Neto a Recibir Pyme',
				dataIndex: 'NETO_RECIBIR',
				sortable: true,	width: 150,	align: 'right', resizable: true, hidden: true,hideable: false
			},{
				header: '% Beneficiario', tooltip: '% Beneficiario',
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true, width: 100, resizable: true, hideable: false, align: 'right',
				renderer: Ext.util.Format.numberRenderer('0.00%') ,hidden: true
			},{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 120,	resizable: true, hideable : false, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true
			},{
				xtype:	'actioncolumn',
				header : 'Detalle de Aplicaci�n', tooltip: 'Detalle de Aplicaci�n',
				dataIndex : 'CS_DSCTO_ESPECIAL',
				width:	110,	align: 'center',  resizable: true, hidden: true,hideable: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (	!Ext.isEmpty(registro.get('CS_DSCTO_ESPECIAL')) && registro.get('CS_DSCTO_ESPECIAL') != "C"	) {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraDetalleAplicacion
					}
				]
			////////		aqui van las columnas dinamicas	//////////////
			},{
				header : '', tooltip: '',
				dataIndex : 'CG_CAMPO1',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false
			},{
				header : '', tooltip: '',
				dataIndex : 'CG_CAMPO2',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									var ic_epo_aserca = "314";
									var layOutAserca = false;
									var estatusCmb = Ext.getCmp('cambioEstatusCmb').getValue();
									if (estatusCmb == "1" && ic_epo_aserca == noEpo.value)	{
										layOutAserca = true;
									}
									if (layOutAserca)	{
										return Ext.util.Format.number(value, '$0,0.00');
									} else	{
										return value;
									}
								}
			},{
				header : '', tooltip: '',
				dataIndex : 'CG_CAMPO3',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false
			},{
				header : '', tooltip: '',
				dataIndex : 'CG_CAMPO4',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									var ic_epo_aserca = "314";
									var layOutAserca = false;
									var estatusCmb = Ext.getCmp('cambioEstatusCmb').getValue();
									if (estatusCmb == "1" && ic_epo_aserca == noEpo.value)	{
										layOutAserca = true;
									}
									if (layOutAserca)	{
										return Ext.util.Format.number(value, '$0,0.00');
									} else	{
										return value;
									}
								}
			},{
				header : '', tooltip: '',
				dataIndex : 'CG_CAMPO5',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									var ic_epo_aserca = "314";
									var layOutAserca = false;
									var estatusCmb = Ext.getCmp('cambioEstatusCmb').getValue();
									if (estatusCmb == "1" && ic_epo_aserca == noEpo.value)	{
										layOutAserca = true;
									}
									if (layOutAserca)	{
										return Ext.util.Format.number(value, '$0,0.00');
									} else	{
										return value;
									}
								}
			},{
				header : 'Referencia', tooltip: 'Referencia',
				dataIndex : 'CT_REFERENCIA',
				width : 100, sortable : true, hidden: true,hideable: false
			},{
				xtype:	'actioncolumn',
				header : 'Detalles', tooltip: 'Detalles',
				dataIndex : 'IC_DOCUMENTO',
				width:	100,	align: 'center',  hidden: true,hideable: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('CS_DETALLE') == 'S' && registro.get('IC_DOCUMENTO')	!=	'') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraGridDetalle
					}
				]
			},{
				xtype:	'actioncolumn',
				header : 'Modificaci�n Montos y/o Fechas', tooltip: 'Modificaci�n Montos y/o Fechas',
				dataIndex : 'CS_CAMBIO_IMPORTE',
				width:	130,	align: 'right', resizable: true, hidden: true,hideable: false,
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if (registro.get('CS_CAMBIO_IMPORTE')	==	'S') {
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							}
						},
						handler:	muestraPanelModifica
					}
				]
			},{
				xtype:	'actioncolumn',
				header : 'Doctos Aplicados a Nota de Credito', tooltip: 'Doctos Aplicados a Nota de Credito',
				dataIndex : 'NUMERO_DOCTO',
				width:	200,	align: 'center', resizable: true, hidden: true,hideable: false,
				renderer: function(value, metadata, registro, rowindex, colindex, store) {
								if(registro.get('NOTA_SIMPLE') == "S" || registro.get('NOTA_SIMPLE_DOCTO') == "S")	{
									return value;
								} else if (registro.get('NOTA_MULTIPLE') == "S" || registro.get('NOTA_MULTIPLE_DOCTO') == "S")	{
									return '';
								}
							 },
				items: [
					{
						getClass: function(value, metadata, registro, rowIndex, colIndex, store) {
							if(registro.get('NOTA_SIMPLE') == "S" || registro.get('NOTA_SIMPLE_DOCTO') == "S")	{
								return value;
							} else if(registro.get('NOTA_MULTIPLE') == "S" || registro.get('NOTA_MULTIPLE_DOCTO') == "S")	{
								this.items[0].tooltip = 'Ver';
								return 'iconoLupa';
							} else	{
								return value;
							}
						},
						handler:	muestraPanelNotas
					}
				]
			},{
				header : 'Fecha de Recepci�n de Bienes y Servicios', tooltip: 'Fecha de Recepci�n de Bienes y Servicios',
				dataIndex : 'DF_ENTREGA',
				sortable : true, width : 100, resizable: true, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: true,hideable: false
			},{
				header : 'Tipo Compra (procedimiento)', tooltip: 'Tipo Compra (procedimiento)',
				dataIndex : 'CG_TIPO_COMPRA',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false
			},{
				header : 'Clasificador por Objeto del Gasto', tooltip: 'Clasificador por Objeto del Gasto',
				dataIndex : 'CG_CLAVE_PRESUPUESTARIA',
				width : 100, sortable : true, resizable: true, hidden: true,hideable: false
			},{
				header : 'Plazo M�ximo', tooltip: 'Plazo M�ximo',
				dataIndex : 'CG_PERIODO',
				width : 100, sortable : true, hidden: true,hideable: false
			},{
				header: 'Mandante', tooltip: 'Mandante',
				dataIndex: 'MANDANTE',
				sortable: true, width: 250, resizable: true, hidden: true,hideable: false
			},{
				header : 'Fecha Registro Operaci�n', tooltip: 'Fecha Registro Operaci�n',
				dataIndex : 'FECHA_REGISTRO_OPERACION',
				sortable : true, width : 100, align: 'center', resizable: true, renderer: Ext.util.Format.dateRenderer('d/m/Y'), hidden: true,hideable: false
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: '',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'->',{xtype: 'tbspacer', width: 10},'-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					disabled: true,
					hidden: false,
					handler: function(boton, evento) {
						resumenTotalesData.load();
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					tooltip: 'Imprime los registros en formato CSV.',
					id: 'btnGenerarArchivo',
					iconCls :'icoXls',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta02ext_csv.jsp',
							params: fp.getForm().getValues(),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},/*
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},*/
				'-',
				{
					xtype: 'button',
					text: 'Generar Todo',
					tooltip: 'Imprime los registros en formato PDF.',
					id: 'btnGenerarPDF',
					iconCls :'icoPdf',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '13consulta02ext.data.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
							informacion: 'Genera_PDF',
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				}/*,
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-'*/
			]
		}
	});

	var gridDetalle = new Ext.grid.GridPanel({
		store: gridDetalleData,
		columns: [
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO1',	width: 100,	align: 'center', hidden: false},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO2',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO3',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO4',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO5',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO6',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO7',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO8',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO9',	width: 100,	align: 'center', hidden: true},
			{header: '',tooltip: '',	dataIndex: 'CG_CAMPO10',width: 100,	align: 'center', hidden: true}
		],
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 250,
		width: 790,
		title: '',
		frame: false,
		hidden: true
	});

	var gridAcuse = new Ext.grid.GridPanel({
		store: gridAcuseData,
		columns: [
			{header: 'IF Seleccionado',tooltip: 'IF Seleccionado',	dataIndex: 'NOMBRE_IF',	width: 150,	align: 'left'},
			{header: 'EPO',tooltip: 'EPO',	dataIndex: 'NOMBRE_EPO',	width: 150,	align: 'left'},
			{header: 'N�mero de Documento',tooltip: 'N�mero de Documento',	dataIndex: 'IG_NUMERO_DOCTO',	width: 110,	align: 'left'},
			{header: 'Moneda',tooltip: 'Moneda',	dataIndex: 'CD_NOMBRE',	width: 130,	align: 'left'},
			{header: 'Monto Documento',tooltip: 'Monto Documento',	dataIndex: 'FN_MONTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Porcentaje de Descuento',tooltip: 'Porcentaje de Descuento',	dataIndex: 'FN_PORC_ANTICIPO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('0%')},
			{header: 'Monto a Descontar',tooltip: 'Monto a Descontar',	dataIndex: 'FN_MONTO_DSCTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto Intereses',tooltip: 'Monto Intereses',	dataIndex: 'IN_IMPORTE_INTERES',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto a Recibir',tooltip: 'Monto a Recibir',	dataIndex: 'IN_IMPORTE_RECIBIR',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')}
		],
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 175,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});

	var gridTotalAcuse = {
		xtype: 'grid',
		store: gridTotalAcuseData,
		id: 'gridTotalAcuse',
		hidden:	false,
		columns: [
			{header: 'TOTALES',dataIndex: 'CD_NOMBRE',align: 'left',	width: 125},
			{header: 'E P O  - DATOS', tooltip:'E P O  - DATOS', dataIndex: 'NOMBRE_EPO',align: 'left',	width: 150},
			{header: 'Total Monto Documento', tooltip:'Total Monto Documento',dataIndex: 'TOTAL_MONTO',width: 130,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Total Monto Descuento',tooltip:'Total Monto Descuento',dataIndex: 'TOTAL_MONTO_DSCTO',width: 130,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Total Monto de Inter�s',tooltip:'Total Monto de Inter�s',dataIndex: 'TOTAL_INTERES',width: 125,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Total Importe a Recibir',tooltip:'Total Importe a Recibir',dataIndex: 'TOTAL_RECIBIR',width: 125,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		width: 790,
		height: 78,
		//title: 'Totales',
		frame: false
	};

	var gridAplica = new Ext.grid.GridPanel({
		store: gridAplicaData,
		columns: [
			{header: 'Producto',tooltip: 'Producto',	dataIndex: 'IC_NOMBRE',	width: 150,	align: 'left'},
			{header: 'IF',tooltip: 'IF',	dataIndex: 'NOMBRE_IF',	width: 150,	align: 'left'},
			{header: 'Orden de Aplicaci�n',tooltip: 'Orden de Aplicaci�n',	dataIndex: 'ORDEN_APLICA',	width: 100,	align: 'center'},
			{header: 'Fecha de Aplicaci�n',tooltip: 'Fecha de Aplicaci�n',	dataIndex: 'DF_FECHA_SELECCION',	sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Fecha de Operaci�n Cr�dito',tooltip: 'Fecha de Operaci�n Cr�dito',	dataIndex: 'DF_OPERACION',sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'N�mero de Mensualidad',tooltip: 'N�mero de Mensualidad',	dataIndex: 'IC_MENSUALIDAD',	width: 110,	align: 'center'},
			{header: 'N�mero de Pedido / Disposici�n',tooltip: 'N�mero de Pedido/Disposici�n',	dataIndex: 'NUMERO_CREDITO',	width: 110,	align: 'center'},
			{header: 'Tipo de Contrato / Operaci�n',tooltip: 'Tipo de Contrato / Operaci�n',	dataIndex: 'CG_TIPO_CONTRATO',	width: 110,	align: 'center'},
			{header: 'N�mero de Pr�stamo NAFIN',tooltip: 'N�mero de Pr�stamo NAFIN',dataIndex: 'IG_NUMERO_PRESTAMO',	width: 110,	align: 'center'},
			{header: 'Importe Aplicado al Cr�dito',tooltip: 'Importe Aplicado al Cr�dito',	dataIndex: 'IMPORTE_APLICADO_CREDITO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Saldo del Cr�dito',tooltip: 'Saldo del Cr�dito',dataIndex: 'SALDO_CREDITO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 300,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});

	var gridTotalAplica = {
		xtype: 'grid',
		store: gridTotalAplicaData,
		id: 'gridTotalAplica',
		hidden:	false,
		columns: [
			{header: 'Totales',tooltip:'Totales',dataIndex: 'TOTAL',align: 'left',	width: 200},
			{header: 'Total Importe Aplicado a Cr�dito',tooltip:'Total Importe Aplicado a Cr�dito',dataIndex: 'TOTAL_IMPORTE_APLICADO',width: 190,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Total Saldo del Cr�dito',tooltip:'Total Saldo del Cr�dito',dataIndex: 'TOTAL_SALDO_CREDITO',width: 190,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Total Importe a Depositar PYME',tooltip:'Total Importe a Depositar PYME',dataIndex: 'TOTAL_IMPORTE_DEPOSITAR',width: 190,	align: 'right',renderer: Ext.util.Format.numberRenderer('$0,0.00')}
		],
		view: new Ext.grid.GridView({markDirty: false}),
		width: 790,
		height: 100,
		//title: 'Totales',
		frame: false
	};

	var gridModifica = new Ext.grid.GridPanel({
		id: 'gridModifica',
		store: gridModificaData,
		columns: [
			{header: 'Fecha Movimiento',tooltip: 'Fecha Movimiento',	dataIndex: 'FECHA_CAMBIO',	sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Monto',tooltip: 'Monto',	dataIndex: 'FN_MONTO_NUEVO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto Anterior',tooltip: 'Monto Anterior',	dataIndex: 'FN_MONTO_ANTERIOR',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Fecha de Vencimiento',tooltip: 'Fecha de Vencimiento',	dataIndex: 'FEC_VENC_NUEVA',	sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Fecha de Vencimiento Anterior',tooltip: 'Fecha de Vencimiento Anterior',	dataIndex: 'FEC_VENC_ANTERIOR',	sortable : true, width : 100, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Causa',tooltip: 'Causa',	dataIndex: 'CT_CAMBIO_MOTIVO',	width: 200,	align: 'left'},
			{header: 'Acuse',tooltip: 'Acuse',	dataIndex: 'CC_ACUSE',	width: 150,	align: 'left'}
		],
		stripeRows: true,
		loadMask: true,
		monitorResize: true,
		height: 200,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});

	var gridNotaVarias = new Ext.grid.GridPanel({
		id: 'gridNotaVarias',
		store: gridNotaVariasData,
		columns: [
			{header: 'No. Nota',tooltip: 'No. Nota',	dataIndex: 'NOTA_MONTO_CONCAT',	width: 250,	align: 'left', hidden:true, hideable:false},
			{header: 'No. Docto.',tooltip: 'No. Docto.',	dataIndex: 'IG_NUMERO_DOCTO_DOCTO',	width: 120,	align: 'left'},
			{header: 'Fecha de Emisi�n',tooltip: 'Fecha de Emisi�n',	dataIndex: 'FECHA_EMISION',	sortable : true, width : 120, align: 'center', renderer: Ext.util.Format.dateRenderer('d/m/Y')},
			{header: 'Moneda',tooltip: 'Moneda',	dataIndex: 'MONEDA',	width: 140,	align: 'left'},
			{header: 'Monto Docto',tooltip: 'Monto Docto',	dataIndex: 'MONTO_DOCTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Monto Aplicado',tooltip: 'Monto Aplicado',	dataIndex: 'MONTO_APLICA',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Saldo Docto',tooltip: 'Saldo Docto',	dataIndex: 'SALDO_DOCTO',	width: 110,	align: 'right', renderer: Ext.util.Format.numberRenderer('$0,0.00')},
			{header: 'Tipo Factoraje',tooltip: 'Tipo Factoraje',	dataIndex: 'TIPO_FACTORAJE',	width: 140,	align: 'left'}
		],
		stripeRows: true,
		view: new Ext.grid.GroupingView({forceFit:true, groupTextTpl: '{text}'}),
		deferRowRender: false,
		loadMask: true,
		monitorResize: true,
		height: 300,
		width: 790,
		title: '',
		frame: false,
		hidden: false
	});

	var elementosDetalle = [
		{
			xtype:	'container',
			layout:	'column',
			anchor:	'100%',
				items:[{
					xtype: 'container',id:	'Izquierda',
					width:	420,
					layout: 'form',
					labelWidth: 50,
					bodyStyle: 'padding: 5px',
					defaults: {frame:false, border: false,height: 18,bodyStyle:'padding:5px'},
					items: [
						{
						  xtype: 'panel',	id: 'disNombre',	html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disRfc',		html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disCalle',	html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disColonia',	html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disCp',		html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disEstado',	html: '&nbsp;'
						},{
						  xtype: 'panel',	id: 'disDelomun',	html: '&nbsp;'
						}
					]
				},{
					xtype: 'panel',	id:'derecha',	layout:'table',	layoutConfig:{ columns:2 },
							defaults: {frame:false, border: true,width:340, height: 30,bodyStyle:'padding:5px'},
					items: [
						{	width:340,	border:false,	frame:true,	colspan:2,	html:'<div align="center">Datos del Documento</div>'	},
						{	width:170,	html:'<div>No. Documento</div>'	},
						{	width:170,	id: 'disDoctoDet',	html: '&nbsp;'	},
						{	width:170,	html:'<div>Fecha de Emisi�n</div>'	},
						{	width:170,	id: 'disEmision',	html: '&nbsp;'	},
						{	width:170,	html:'<div>Fecha de Vencimiento</div>'	},
						{	width:170,	id: 'disVto',	html: '&nbsp;'	},
						{	width:170,	html:'<div>Importe</div>'	},
						{	width:170,	id: 'disImporte',	html: '&nbsp;'	},
						{	width:170,	html:'<div>Moneda</div>'	},
						{	width:170,	id: 'disMoneda',	html: '&nbsp;'	}
					]
			}]
		}
	];	

	var fpDetalle=new Ext.FormPanel({	autoHeight:true,	width:800,	bodyStyle: 'padding:6px',	border:false,	labelWidth: 130,	items:elementosDetalle,	hidden:false	});//height:230,

	var elementosAcuse = [
		{
			xtype: 'container',
			layout:'column',
				items:[{
					xtype: 'panel',id:	'izqAcuse',
					width:	215,
					layout: 'form',
					bodyStyle: 'padding: 1px',
					border: false,
					items: [{html: ' '}]
				},{
					xtype: 'panel',
					id:	'centro',
					title:	'Cifras de Control',
					width:	350,
					layout: 'form',
					bodyStyle: 'padding: 1px',
					items: [
						{
							xtype: 'displayfield',	id: 'disAcuse',	fieldLabel: 'No. Acuse',	value: ''
						},{
							xtype: 'displayfield',	id: 'disFechaCarga',	fieldLabel: 'Fecha de Carga',	value: ''
						},{
							xtype: 'displayfield',	id: 'disHoraCarga',		fieldLabel: 'Hora de Carga',	value: ''
						},{
							xtype: 'displayfield',	id: 'disUsuario',	fieldLabel: 'Usuario de Captura',	value: ''
						},{
							xtype: 'panel',	id: 'centro2', border: false,bodyborder: false,	bodyStyle: 'padding: 2px', items: [{html:'Al transmitir este MENSAJE DE DATOS, usted est� bajo su responsabilidad cediendo los derechos de cr�dito de los DOCUMENTOS que constan en el mismo al INTERMEDIARIO FINANCIERO, dicha transmisi�n tendr� validez para todos los efectos legales'}]
						}
					]
			},{
					xtype: 'panel',id:	'derAcuse',
					width:	215,
					layout: 'form',
					bodyStyle: 'padding: 1px',
					border: false,
					items: [{html: ' '}]
			}]
		}
	];	

	var fpAcuse=new Ext.FormPanel({
		height:500,
		width: 790,
		bodyStyle: 'padding: 1px',
		border: false,
		labelWidth: 130,
		items: elementosAcuse,
		hidden: false
	});

	var elementosAplica = [
		{
			xtype: 'container',
			layout:'column',
			border: true,
				items:[{
					xtype: 'panel',id:	'izqAplica',
					columnWidth:.5,
					layout: 'form',
					bodyStyle: 'padding: 2px',
					items: [{xtype: 'displayfield',	id: 'disDocto',	fieldLabel: 'N�mero de documento',	anchor:'95%',	value: ''}]
				},{
					xtype: 'panel',id:	'derAplica',
					columnWidth:.5,
					layout: 'form',
					bodyStyle: 'padding: 2px',
					items: [{xtype: 'displayfield',	id: 'disMontoAplica',	fieldLabel: 'Monto Aplicado',	anchor:'95%',	value: ''}]
			}]
		}
	];	

	var fpAplica=new Ext.FormPanel({
		height:34,
		width: 790,
		bodyStyle: 'padding: 2px',
		border: false,
		labelWidth: 150,
		items: elementosAplica,
		hidden: false
	});

	var elementosModifica = [
		{
			xtype: 'container',
			layout:'column',
				items:[{
					xtype: 'panel',id:	'izqModifica',
					width:	215,
					layout: 'form',
					border:false,
					bodyStyle: 'padding: 2px',
					items: [{html: ' '}]
				},{
					xtype: 'panel',	id:	'centroModifica',
					//title:	'Cifras de Control',
					width:	350,
					layout: 'form',
					bodyStyle: 'padding: 2px',
					border: false,
					items: [{html: ' '},
						{
							xtype: 'displayfield',	id: 'disEpoM',		fieldLabel: 'EPO',						value: ''
						},{
							xtype: 'displayfield',	id: 'disIfM',		fieldLabel: 'IF',							value: ''
						},{
							xtype: 'displayfield',	id: 'disDoctoM',	fieldLabel: 'N�mero de Documento',	value: ''
						},{
							xtype: 'displayfield',	id: 'disFecEmiM',	fieldLabel: 'Fecha de Emisi�n',		value: ''
						},{
							xtype: 'displayfield',	id: 'disFecVencM',fieldLabel: 'Fecha de Vencimiento',	value: ''
						},{
							xtype: 'displayfield',	id: 'disMonedaM',	fieldLabel: 'Moneda',					value: ''
						},{
							xtype: 'displayfield',	id: 'disMontoM',	fieldLabel: 'Monto',						value: ''
						},{
							xtype: 'displayfield',	id: 'disEstatusM',fieldLabel: 'Estatus',					value: ''
						}
					]
			},{
					xtype: 'panel',id:	'derModifica',
					width:	215,
					layout: 'form',
					border:false,
					bodyStyle: 'padding: 2px',
					items: [{html: ' '}]
			}]
		}
	];	

	var fpModifica=new Ext.FormPanel({
		height:212,
		width: 790,
		bodyStyle: 'padding: 2px',
		border: false,
		labelWidth: 150,
		items: elementosModifica,
		hidden: false
	});

	var elementosForma = [
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',			
			width: 900,
			items: [			
				{
						xtype: 'combo',
						name: 'no_epo',
						id: 'cmbEpo',
						allowBlank: false,
						fieldLabel: 'Nombre de la EPO',
						mode: 'local', 
						width: 250,
						displayField : 'descripcion',
						valueField : 'clave',
						hiddenName : 'no_epo',
						emptyText: 'Seleccione EPO',
						forceSelection : true,
						triggerAction : 'all',
						typeAhead: true,						
						minChars : 1,
						store : catalogoEPOData,
						tpl:'<tpl for=".">' +
							'<tpl if="!Ext.isEmpty(loadMsg)">'+
							'<div class="loading-indicator">{loadMsg}</div>'+
							'</tpl>'+
							'<tpl if="Ext.isEmpty(loadMsg)">'+
							'<div class="x-combo-list-item">' +
							'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
							'</div></tpl></tpl>',
						setNewEmptyText: function(emptyTextMsg){
							this.emptyText = emptyTextMsg;
							this.setValue('');
							this.applyEmptyText();
							this.clearInvalid();
						},
						//tpl : NE.util.templateMensajeCargaCombo,
						
						listeners: {
							select: {
								fn: function(combo) {
									var ventanaDetalle = Ext.getCmp('winDetalle');
									if (ventanaDetalle) {
										ventanaDetalle.hide();
									}
									var contenedorIzq = Ext.getCmp('camposDic');
									var dina1 =  Ext.getCmp('campDina1');
									var dina2 =  Ext.getCmp('campDina2');
									if (dina1 != undefined) {contenedorIzq.remove(dina1);}
									if (dina2 != undefined) {contenedorIzq.remove(dina2);}
									grid.hide();
									var totalesCmp = Ext.getCmp('gridTotales');
									if (totalesCmp.isVisible()) {
										totalesCmp.hide();
									}
									var mandanteCmb = Ext.getCmp('mandanteCmb_1');
									if (mandanteCmb) {mandanteCmb.hide();}
									var comboIF = Ext.getCmp('cmbIF');
									comboIF.setValue('');
									comboIF.store.removeAll();
									comboIF.setDisabled(false);
									comboIF.emptyText = 'Seleccione IF';
									comboIF.store.reload({
											params: {
												no_epo: combo.getValue()
											}
									});
									comboIF.reset();
									fp.el.mask('Procesando...', 'x-mask-loading');
									Ext.Ajax.request({
										url: '13consulta02ext.data.jsp',
										params: {
											informacion: "Parametrizacion",
											no_epo:  combo.getValue()
										},
										callback: procesarSuccessFailureParametrizacion
									});
								}
							}
						}
					},
					{
						xtype: 'displayfield',
						value: '&nbsp;&nbsp;&nbsp;Fecha de vencimiento:',
						width: 150 
					},
					{
						xtype: 'datefield',
						name: 'df_fecha_venc_de',
						id: 'dc_fecha_vencMin',
						allowBlank: true,
						startDay: 0,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha', 
						campoFinFecha: 'dc_fecha_vencMax',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'a',
						width: 25
					},
					{
						xtype: 'datefield',
						name: 'df_fecha_venc_a',
						id: 'dc_fecha_vencMax',
						allowBlank: true,
						startDay: 1,
						width: 100,
						msgTarget: 'side',
						vtype: 'rangofecha',
						campoInicioFecha: 'dc_fecha_vencMin',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					}						
				]			
		}, 		
		{
			xtype: 'compositefield',
			fieldLabel: 'Nombre del IF',
			combineErrors: false,
			msgTarget: 'side',			
			width: 900,
			items: [		
				{
						xtype: 'combo',
						name: 'no_if',
						id:	'cmbIF',
						fieldLabel: 'Nombre del IF',
						//emptyText: 'Seleccione IF',
						emptyText: 'Debe de seleccionar un IF',
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'no_if',
						forceSelection : false,
						//disabled:	true,
						width: 250,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1,
						store: catalogoIfData,
						tpl:'<tpl for=".">' +
							'<tpl if="!Ext.isEmpty(loadMsg)">'+
							'<div class="loading-indicator">{loadMsg}</div>'+
							'</tpl>'+
							'<tpl if="Ext.isEmpty(loadMsg)">'+
							'<div class="x-combo-list-item">' +
							'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
							'</div></tpl></tpl>',
						setNewEmptyText: function(emptyTextMsg){
							this.emptyText = emptyTextMsg;
							this.setValue('');
							this.applyEmptyText();
							this.clearInvalid();
						}
					},
					{
						xtype: 'displayfield',
						value: '&nbsp;&nbsp;&nbsp; Moneda:',
						width: 150
					},
					{
						xtype: 'combo',
						name: 'no_moneda',
						fieldLabel: '',
						emptyText: 'Seleccione Moneda',	
						mode: 'local',
						displayField: 'descripcion',
						width: 250,
						valueField: 'clave',
						hiddenName : 'no_moneda',
						forceSelection : false,
						triggerAction : 'all',
						typeAhead: true,
						minChars : 1, anchor: '86%',
						store: catalogoMonedaData, 
						tpl : NE.util.templateMensajeCargaCombo
					}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: '',
			combineErrors: false,
			msgTarget: 'side',			
			width: 900,
			items: [		
				{
					xtype: 'textfield',
					name: 'no_docto',
					id: 'noDocto',
					fieldLabel: 'N�mero Documento',
					allowBlank: true,
					width: 250,
					maxLength: 15
				},
				{
					xtype: 'displayfield',
					value: '&nbsp;&nbsp;&nbsp;&nbsp;Monto:', 
					width: 150
				},
				{
					xtype: 'numberfield',
					name: 'monto_de',
					id: 'montoMin',
					allowBlank: true,
					maxLength: 14,
					width: 100,
					msgTarget: 'side',
					vtype:	'rangoValor',
					campoFinValor:	'montoMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a', 
					width: 5
				},
				{
					xtype: 'numberfield',
					name: 'monto_a',
					id: 'montoMax',
					allowBlank: true,
					maxLength: 14,
					width: 100,
					msgTarget: 'side',
					vtype:	'rangoValor',
					campoInicioValor:	'montoMin',
					margins: '0 20 0 0'	  //necesario para mostrar el icono de error
				}
			]
		},	
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Emisi�n',
			combineErrors: false,
			msgTarget: 'side',			
			width: 900,
			items: [	
				{
					xtype: 'datefield',
					name: 'df_fecha_docto_de',
					id: 'dc_fecha_emisionMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'dc_fecha_emisionMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 25
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_docto_a',
					id: 'dc_fecha_emisionMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'dc_fecha_emisionMin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'Estatus:', 
					width: 140
				},
				{
						xtype: 'combo',
						name: 'no_estatus_docto',
						id: 'cambioEstatusCmb',
						fieldLabel: 'Estatus',
						emptyText: 'Seleccione Estatus',
						allowBlank: true,
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'no_estatus_docto',
						forceSelection : false,
						triggerAction : 'all',
						typeAhead: true,
						width: 250,
						minChars : 1, anchor: '86%',
						store: catalogoEstatusData,
						tpl : NE.util.templateMensajeCargaCombo
					}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha Aut. IF',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'fecha_if_de',
					id: 'fechaIfde',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'fechaIfa',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 25
				},
				{
					xtype: 'datefield',
					name: 'fecha_if_a',
					id: 'fechaIfa',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'fechaIfde',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
						xtype: 'displayfield',
						value: 'Tasa:',
						width: 140
				},
				{
						xtype: 'numberfield',
						name: 'tasa_de',
						id: 'tasaMin',
						allowBlank: true,
						maxLength: 13,
						width: 110,
						msgTarget: 'side',
						vtype:	'rangoValor',
						campoFinValor:	'tasaMax',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'a',
						width: 5
					},
					{
						xtype: 'numberfield',
						name: 'tasa_a',
						id: 'tasaMax',
						allowBlank: true,
						maxLength: 13,
						width: 110,
						msgTarget: 'side',
						vtype:	'rangoValor',
						campoInicioValor:	'tasaMin',
						margins: '0 20 0 0'	  //necesario para mostrar el icono de error
					}
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Tipo Factoraje',
				combineErrors: false,
				msgTarget: 'side',
				id: 'tipoFactorajeCmb_1',
				items: [
					{
						xtype: 'combo',
						name: 'tipoFactoraje',
						id: 'tipoFactorajeCmb',
						fieldLabel: 'Tipo Factoraje',
						emptyText: 'Seleccionar',
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'tipoFactoraje',
						forceSelection : false,
						triggerAction : 'all',
						width: 250,
						typeAhead: true,
						minChars : 1,
						hidden: false,
						store: catalogoFactorajeData,
						tpl : NE.util.templateMensajeCargaCombo,
						listeners: {
							select: {
								fn: function(combo) {
									var vManda = combo.getValue();
									var _mandanteCmb = Ext.getCmp('mandanteCmb');
									_mandanteCmb.setValue('');
									_mandanteCmb.store.removeAll();
									_mandanteCmb.setDisabled(false);
									if (vManda == 'M') {		//VALOR ORIGINAL = 'M'
									_mandanteCmb.store.load({
											params: {
												claveMandante: combo.getValue()
											}
									});								
										Ext.getCmp('mandanteCmb_1').show();
									} else {
										Ext.getCmp('mandanteCmb_1').hide();
									}
								}
							}
						}
					}
					]
				},	
				{
				xtype: 'compositefield',
				fieldLabel: 'Mandante',
				combineErrors: false,
				msgTarget: 'side',
				id: 'mandanteCmb_1',
				hidden: true,
				items: [
					{
						xtype: 'combo',
						name: 'mandant',
						id: 'mandanteCmb',
						fieldLabel: 'Mandante',
						emptyText: 'Seleccionar',
						mode: 'local',
						displayField: 'descripcion',
						valueField: 'clave',
						hiddenName : 'mandant',
						forceSelection : false,
						triggerAction : 'all',
						typeAhead: true,
						width: 250,
						minChars : 1,
						//hidden: true,
						store: catalogoMandanteData,
							tpl:'<tpl for=".">' +
							'<tpl if="!Ext.isEmpty(loadMsg)">'+
							'<div class="loading-indicator">{loadMsg}</div>'+
							'</tpl>'+
							'<tpl if="Ext.isEmpty(loadMsg)">'+
							'<div class="x-combo-list-item">' +
							'<div style="white-space: normal; word-wrap:break-word;">{descripcion}</div>' +
							'</div></tpl></tpl>',
						setNewEmptyText: function(emptyTextMsg){
							this.emptyText = emptyTextMsg;
							this.setValue('');
							this.applyEmptyText();
							this.clearInvalid();
						}
					}
					]
				},							
				
				{
				xtype: 'compositefield',
				fieldLabel: 'Criterios ordenamiento',
				msgTarget: 'side',
				combineErrors: false,
				items: [
					{
						xtype: 'numberfield',
						name: 'ord_if',
						id: 'ordif',
						allowBlank: true,
						maxLength: 1,
						width: 30,
						msgTarget: 'side',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'I.F.',
						width: 30
					},
					{
						xtype: 'numberfield',
						name: 'ord_no_docto',
						id: 'ordNoDocto',
						allowBlank: true,
						maxLength: 1,
						width: 30,
						msgTarget: 'side',
						margins: '0 20 0 0'	  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'No. Documento'
					}
				]
			},					
			{
				xtype: 'compositefield',
				fieldLabel: '',
				msgTarget: 'side',
				combineErrors: false,
				items: [
					{
						xtype: 'numberfield',
						name: 'ord_epo',
						id: 'ordepo',
						allowBlank: true,
						maxLength: 1,
						width: 30,
						msgTarget: 'side',
						margins: '0 20 0 0'  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'EPO',
						width: 30
					},
					{
						xtype: 'numberfield',
						name: 'ord_fec_venc',
						id: 'ordFecVenc',
						allowBlank: true,
						maxLength: 1,
						width: 30,
						msgTarget: 'side',
						margins: '0 20 0 0'	  //necesario para mostrar el icono de error
					},
					{
						xtype: 'displayfield',
						value: 'Fecha de vencimiento'
					}
				]
			},
			{
					xtype: 'fieldset',					
					combineErrors: false,
					msgTarget: 'side',
					id: 'camposDic',
					border: false,
					width: 400,
					items: [	 ]				
				},
			{
				xtype:'hidden',
				id:'hidenVencePyme',
				name:'sFechaVencPyme', 
				value:''
			},
			{
				xtype:'hidden',
				id:'hidenOperaNota',
				name:'OperaFactorajeNotaDeCredito',
				value:''
			}		
	];

	var fp = new Ext.form.FormPanel({
		id: 'forma', 
		width: 900,
		style	: ' margin:0 auto;',
		title:	'Nafinet',
		hidden: false,
		frame: true,
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 130,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {
					var cmbEpo = Ext.getCmp('cmbEpo');
					if (cmbEpo.value == "")	{
						cmbEpo.markInvalid('Debe seleccionar una EPO');
						cmbEpo.focus();
						return;
					}
					grid.hide();
					var totalesCmp = Ext.getCmp('gridTotales');
					if (totalesCmp.isVisible()) {
						totalesCmp.hide();
					}
					
					var fechaEmisionMin = Ext.getCmp('dc_fecha_emisionMin');
					var fechaEmisionMax = Ext.getCmp('dc_fecha_emisionMax');
					var fechaVenceMin = Ext.getCmp("dc_fecha_vencMin");
					var fechaVenceMax = Ext.getCmp("dc_fecha_vencMax");
					var fechaAutIFMin = Ext.getCmp("fechaIfde");
					var fechaAutIFMax = Ext.getCmp("fechaIfa");
					var ordif = Ext.getCmp('ordif');
					var ordnd = Ext.getCmp('ordNoDocto');
					var ordep = Ext.getCmp('ordepo');
					var ordfv = Ext.getCmp('ordFecVenc');
					var tasaMin = Ext.getCmp("tasaMin");
					var tasaMax = Ext.getCmp("tasaMax");

					if (!Ext.isEmpty(fechaEmisionMin.getValue()) && Ext.isEmpty(fechaEmisionMax.getValue()) ) {
						fechaEmisionMax.markInvalid('Debe capturar la fecha de emisi�n final');
						fechaEmisionMax.focus();
						return;
					}
					if (!Ext.isEmpty(fechaVenceMin.getValue()) && Ext.isEmpty(fechaVenceMax.getValue()) ) {
						fechaVenceMax.markInvalid('Debe capturar la fecha de vencimiento final');
						fechaVenceMax.focus();
						return;
					}
					if (!Ext.isEmpty(fechaAutIFMin.getValue()) && Ext.isEmpty(fechaAutIFMax.getValue()) ) {
						fechaAutIFMax.markInvalid('Debe capturar la fecha Aut. IF final');
						fechaAutIFMax.focus();
						return;
					}

					var noDocto = Ext.getCmp('noDocto');
					no_documento = noDocto.getValue();
					if (no_documento !="") {
					 for(n=0; n<no_documento.length; n++){
					 c=no_documento.substring(n,n+1);
						  if (c=='.' || c==',' || c==';' || c==':' || c=='?' || c=='�' || c=='!' || c=='�' || c=='*' || c=='+' || c=='' || c=='/' || c=='(' || c==')' || c=='&' || c=='$' || c=='%' || c=='"' || c=='�' || c=='#' || c=='=' || c=='{' || c=='}' || c=='[' || c==']' || c=='|' || c=='�' || c=='~' || c=='�') {
							  noDocto.markInvalid("El Documento solo puede tener valores Num�ricos - Letras");
							  noDocto.focus();
							  return;
						  }
					 }
					}

					if(fechaVenceMin.getValue() != "" && fechaVenceMax.getValue() != "" && fechaEmisionMin.getValue() != "" && fechaEmisionMax.getValue() != "")	{
						var resultado = datecomp(fechaVenceMin.value,fechaEmisionMin.value);
						var resultado1 = datecomp(fechaVenceMax.value,fechaEmisionMax.value);
						if (resultado != 1 || resultado1 != 1)	{
							if (resultado != 1 && resultado1 != 1)	{
								fechaVenceMin.setValue("");
								fechaVenceMin.focus();
								fechaVenceMin.markInvalid('La fecha de vencimiento debe ser mayor a la de emisi�n');
							}else{
								if (resultado != 1){
									fechaVenceMin.setValue("");
									fechaVenceMin.focus();
									fechaVenceMin.markInvalid('La fecha de vencimiento debe ser mayor a la de emisi�n');
								}else{
									fechaVenceMax.setValue("");
									fechaVenceMax.focus();
									fechaVenceMax.markInvalid('La fecha de vencimiento debe ser mayor a la de emisi�n');
								}
							}
							return;
						}
					}

					if(fechaVenceMin.getValue() != "" && fechaVenceMax.getValue() != "" && fechaAutIFMin.getValue() != "" && fechaAutIFMax.getValue() != "")	{
						var resultado = datecomp(fechaAutIFMin.value,fechaVenceMin.value);
						var resultado1 = datecomp(fechaAutIFMax.value,fechaVenceMax.value);
						if (resultado != 2 || resultado1 != 2)	{
							if (resultado != 2 && resultado1 != 2)	{
								fechaAutIFMin.setValue("");
								fechaAutIFMin.focus();
								fechaAutIFMin.markInvalid('La fecha de Aut. IF debe ser menor a la vencimiento');
							}else{
								if (resultado != 1){
									fechaAutIFMin.setValue("");
									fechaAutIFMin.focus();
									fechaAutIFMin.markInvalid('La fecha de Aut. IF debe ser menor a la vencimiento');
								}else{
									fechaAutIFMax.setValue("");
									fechaAutIFMax.focus();
									fechaAutIFMax.markInvalid('La fecha de Aut. IF debe ser menor a la vencimiento');
								}
							}
							return;
						}
					}

					if(fechaEmisionMin.getValue() != "" && fechaEmisionMax.getValue() != "" && fechaAutIFMin.getValue() != "" && fechaAutIFMax.getValue() != "")	{
						var resultado = datecomp(fechaAutIFMin.value,fechaEmisionMin.value);
						var resultado1 = datecomp(fechaAutIFMax.value,fechaEmisionMax.value);
						if (resultado != 1 || resultado1 != 1)	{
							if (resultado != 1 && resultado1 != 1)	{
								fechaAutIFMin.setValue("");
								fechaAutIFMin.focus();
								fechaAutIFMin.markInvalid('La fecha de Aut. IF debe ser mayor a la de emisi�n');
							}else{
								if (resultado != 1){
									fechaAutIFMin.setValue("");
									fechaAutIFMin.focus();
									fechaAutIFMin.markInvalid('La fecha de Aut. IF debe ser mayor a la de emisi�n');
								}else{
									fechaAutIFMax.setValue("");
									fechaAutIFMax.focus();
									fechaAutIFMax.markInvalid('La fecha de Aut. IF debe ser mayor a la de emisi�n');
								}
							}
							return;
						}
					}

					var o_if = ordif.getValue();
					var o_nd = ordnd.getValue();
					var o_oe = ordep.getValue();
					var o_fv = ordfv.getValue();
					if (o_if != "" || o_nd != "" || o_oe != "" || o_fv != "") {
						if ( (o_if >= 1 && o_if <= 4) || (o_nd >= 1 && o_nd <= 4) || (o_oe >= 1 && o_oe <= 4) || (o_fv >= 1 && o_fv <= 4) ) {
							if ( ((o_if == o_nd) && o_if != "") || ((o_if == o_oe) && o_oe != "") || ((o_if == o_fv) && o_if != "") || ((o_nd == o_oe) && o_nd !="") || ((o_nd == o_fv) && o_nd !="") || ((o_oe == o_fv) && o_oe != "") ) {
								ordif.markInvalid('La Prioridad no puede ser la Misma en el Criterio de Ordenamiento');
								return;
							}
						}
						if ( ((o_if > 4 || o_if == 0) && o_if != "") || ((o_nd > 4 || o_nd == 0) && o_nd != "") || ((o_oe > 4 || o_oe == 0) && o_oe != "") || ((o_fv > 4 || o_fv == 0) && o_fv != "") ) {
							ordif.markInvalid('El Criterio de Ordenamiento debe de estar entre los valores del 1 al 4');
							return;
						}
					}

					if (!Ext.isEmpty(tasaMin.getValue()) && Ext.isEmpty(tasaMax.getValue()) ) {
						tasaMax.markInvalid('Debe capturar la tasa final');
						tasaMax.focus();
						return;
					}

					fp.el.mask('Enviando...', 'x-mask-loading');
					var estatusCmb = Ext.getCmp('cambioEstatusCmb').getValue();
					if (estatusCmb == "" || estatusCmb == "1" || estatusCmb == "2" || estatusCmb == "5" || estatusCmb == "6" || estatusCmb == "7" || estatusCmb == "9" ||
						estatusCmb == "10" || estatusCmb == "21" || estatusCmb == "23" || estatusCmb == "28" || estatusCmb == "29" || estatusCmb == "30" || estatusCmb == "31"){
						ocultarDoctosAplicados = true;
					} else	{
						ocultarDoctosAplicados = false;
					}
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar',
							start: 0,
							limit: 15
						})
					});
				} //fin handler
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta02ext.jsp';
				}
			}
		]
	});

	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		height: 'auto',
		items: [
			fp,				
			NE.util.getEspaciador(5),
			grid,
			gridTotales
		]
	});

	catalogoEPOData.load();
	catalogoMonedaData.load();
	catalogoEstatusData.load();

});
