var verTasas;

var texto = ['Habilitada para operar Factoraje Vencido con pago de intereses por cuenta de la empresa de Primer Orden'];						


Ext.onReady(function() {
	
//_--------------------------------- HANDLERS -------------------------------

//HANDLERS GENERAL	
	var procesarGeneralData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		var valor = jsonData.VALOR;	
		
		var fp = Ext.getCmp('forma');	
		fp.el.unmask();			
				
		if(valor =='S'){	
			fpT.show();			
		}else{
			fpT.hide();
		}
		
		if (arrRegistros != null) {
			if (!gridGeneral.isVisible()) {
					gridGeneral.show();
			}				
		}				
		
		var el = gridGeneral.getGridEl();
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		var btnBajarPDF = Ext.getCmp('btnBajarPDF');
		var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');		
					
			
		if (arrRegistros == '') {
			el.mask('No se encontr� ning�n registro', 'x-mask');
			btnGenerarPDF.disable();
			btnGenerarArchivo.disable();	
			
		}else if (arrRegistros != null){
			el.unmask();
			btnGenerarPDF.enable();
			btnGenerarArchivo.enable();
			btnBajarPDF.hide();
			btnBajarArchivo.hide();			
		}
	}	
		
	//GENERAR ARCHIVO DE PDF DEL GRID GENERAL
	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
			btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
			if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}		
//GENERAR ARCHIVO DE GRID GENERAL
	var procesarSuccessFailureArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		  btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	
	
//-------------------------------- STORES -----------------------------------	

	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion','loadMsg'],
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPO'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}	
	});
	
	
	
	// ELEMENTOS DE LA FORMA 										
	var elementosForma = [	
			{
			xtype: 'combo',
			name: 'no_epo',
			id:'cmbEPO',
			fieldLabel: 'Nombre de la EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'no_epo',
			emptyText: 'Seleccione...',					
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo	
		}
		];

		//CONSULTA PARA LOS DEL GRID GENERAL
	var consultaGeneralData   = new Ext.data.GroupingStore({	 
		root : 'registros',
		url : '13consulta03ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},		
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
			{name: 'NOMBREIF'},
			{name: 'NOMBREMONEDA'},					
			{name: 'NOMBRETASA'},					
			{name: 'RANGOPLAZO'},					
			{name: 'VALOR'},					
			{name: 'RELMAT'},					
			{name: 'PTOS'},					
			{name: 'TASA_APLICAR'}								
			]
		}),
		groupField: 'NOMBREIF',
		sortInfo:{field: 'NOMBREIF', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarGeneralData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarGeneralData(null, null, null);
				}
			}
		}		
	});
	
	
	//CREA EL GRID PARA LA OPCION DE VER TASAS 
		var gridGeneral = new Ext.grid.GridPanel({
		store: consultaGeneralData,
		id: 'gridGeneral',
		margins: '20 0 0 0',
		hidden: true,
		columns: [		
			{
				header: 'Intermediario Financiero',
				tooltip: 'Intermediario Financiero',
				dataIndex: 'NOMBREIF',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: true,
				align: 'left'	
			},			
			{
				header: 'Moneda',
				tooltip: 'Moneda',
				dataIndex: 'NOMBREMONEDA',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false, 	
				align: 'left'	
			},						
			{
				header: 'Tasa Base',
				tooltip: 'Tasa Base',
				dataIndex: 'NOMBRETASA',
				sortable: true,
				width: 130,
				resizable: true,
				hidden: false,
				align: 'center'	
			},			
			{
				header: 'Plazo',
				tooltip: 'Plazo',
				dataIndex: 'RANGOPLAZO',
				sortable: false,
				width: 130,
				resizable: true,
				hidden: false,
				align: 'center'	
			},				
			{
				header: 'Valor',
				tooltip: 'Valor',
				dataIndex: 'VALOR',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'
			},						
			{
				header: 'Rel. Mat.',
				tooltip: 'Rel. Mat.',
				dataIndex: 'RELMAT',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'			
			},			
			{
				header: 'Puntos Adicionales',
				tooltip: 'Puntos Adicionales',
				dataIndex: 'PTOS',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center'				
			},				
			{
				header: 'Tasa a Aplicar',
				tooltip: 'Tasa a Aplicar',
				dataIndex: 'TASA_APLICAR',
				sortable: true,
				hideable: false,
				width: 130,
				align: 'center',
				renderer: Ext.util.Format.numberRenderer('0.0000%')
			}
		],	
			view: new Ext.grid.GroupingView({
            forceFit:true,
            groupTextTpl: '{text}'					 
        }),
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		title: '',
		frame: true,
		bbar: {
			xtype: 'toolbar',			
			items: [
				'->',
				'-',
					{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta03extCSV.jsp',
							params: fp.getForm().getValues(),
							callback: procesarSuccessFailureArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '13consulta03extPDF.jsp',	
							params: fp.getForm().getValues(),
							callback: procesarSuccessFailureGenerarPDF											  
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				}
			]
		}
		
	});	
	

	//-------------------------------- PRINCIPAL -----------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 600,
		title: 'Tasas',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: false,
		
		items: elementosForma,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				formBind: true,
				handler: function(boton, evento) {	
						
				var no_epo = Ext.getCmp("cmbEPO");
				if (Ext.isEmpty(no_epo.getValue()) ) {
						no_epo.markInvalid('Por favor, especifique la Epo');
						return;
				}		
				
				fp.el.mask('Enviando...', 'x-mask-loading');					
					consultaGeneralData.load({					
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar' //Generar datos para la consulta							
						})
					});												
					Ext.getCmp('btnGenerarPDF').disable();
					Ext.getCmp('btnBajarPDF').hide();
					Ext.getCmp('btnGenerarArchivo').disable();
					Ext.getCmp('btnBajarArchivo').hide();						
				}
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {					
					fp.getForm().reset();
					gridGeneral.hide();
					fpT.hide();
				}
			}					
		]		
	});
	
	var fpT = new Ext.form.FormPanel({		
			id: 'forma1',
			width: 949,			
			frame: true,		
			titleCollapse: false,
			hidden: true,
			bodyStyle: 'padding: 6px',			
			defaultType: 'textfield',
			html: texto.join('')			
		});
		
//Dado que la aplicaci�n se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',		
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			gridGeneral,
			NE.util.getEspaciador(20),
			fpT
		]
	});
	
	
catalogoEPOData.load();

});