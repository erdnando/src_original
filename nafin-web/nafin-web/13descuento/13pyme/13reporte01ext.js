Ext.onReady(function() {

	var jsonValoresIniciales = null;
	var cveEstatus = "";
	
	function procesaValoresIniciales(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
			if (jsonValoresIniciales != null){
				var cmbStatus = Ext.getCmp('cmbStatus');
				if (jsonValoresIniciales.mostrarOpcionPignorado == false)	{
					cmbStatus.store.remove(cmbStatus.store.getAt(1));
				}
				if (!fp.isVisible())	{
					fp.show();
				}
			}
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

//--------Handlers---------
	var procesarSuccessFailureGenerarPdfNego =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfNego');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfNego');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarPdfPigno =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfPigno');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfPigno');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPdfSelec =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfSelec');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfSelec');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPdfAutor =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfAutor');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfAutor');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPdfOpera =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfOpera');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfOpera');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPdfAcred =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfAcred');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfAcred');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}
	var procesarSuccessFailureGenerarPdfProgra =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPdfProgra');
		btnGenerarPDF.setIconClass('');
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			btnGenerarPDF.disable();
			var btnBajarPDF = Ext.getCmp('btnBajarPdfProgra');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', { duration: 5, easing:'bounceOut'});
			btnBajarPDF.focus();
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarConsultaNegociable = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfNego');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfNego');
			if (!gridNegociable.isVisible()) {
				gridNegociable.show();
			}
			var el = gridNegociable.getGridEl();
			var cm = gridNegociable.getColumnModel();
			
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			cm.setHidden(9, true);						//NOMBREIF
			cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
			
			if(store.getTotalCount() > 0)	{
				if (jsonValoresIniciales.bTipoFactoraje)	{
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				if (jsonValoresIniciales.bOperaFactorajeVencido || jsonValoresIniciales.bOperaFactorajeIF )	{
					cm.setHidden(9, false);						//NOMBREIF
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido  || jsonValoresIniciales.bOperaFactVencimientoInfonavit)	{
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
	
	var procesarConsultaPignorado = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfPigno');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfPigno');
			if (jsonValoresIniciales.mostrarOpcionPignorado != undefined){
				if (jsonValoresIniciales.mostrarOpcionPignorado)	{
					if (!gridPignorado.isVisible()) {gridPignorado.show();}
				}else	{
					if (gridPignorado.isVisible())	{gridPignorado.hide();}
				}
				var el = gridPignorado.getGridEl();
				
				if(store.getTotalCount() > 0) {
					if (!btnGenerarPDF.isVisible()) {
						btnGenerarPDF.show();
					}
					btnGenerarPDF.enable();
					el.unmask();
				} else {
					btnGenerarPDF.disable();
					el.mask('No se encontr� ning�n registro', 'x-mask');
				}
			}
		}
	}

	var procesarConsultaSelecPyme = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfSelec');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfSelec');
			if (!gridSelecPyme.isVisible()) {
				gridSelecPyme.show();
			}
			var el = gridSelecPyme.getGridEl();
			var cm = gridSelecPyme.getColumnModel();
			
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			cm.setHidden(cm.findColumnIndex('NETO_RECIBIR'), true);
			cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
			
			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.bTipoFactoraje)	{
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido  || jsonValoresIniciales.bOperaFactVencimientoInfonavit)	{
					cm.setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaAutorIF = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfAutor');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfAutor');
			if (!gridAutorIF.isVisible()) {
				gridAutorIF.show();
			}
			var el = gridAutorIF.getGridEl();
			var cm = gridAutorIF.getColumnModel();
			
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			cm.setHidden(cm.findColumnIndex('NETO_RECIBIR'), true);
			cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);

			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.bTipoFactoraje)	{
					gridAutorIF.getColumnModel().setHidden(5, false);				//TIPO_FACTORAJE
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido  || jsonValoresIniciales.bOperaFactVencimientoInfonavit)	{
					cm.setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaOperada = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfOpera');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfOpera');
			if (!gridOperada.isVisible()) {
				gridOperada.show();
			}
			var el = gridOperada.getGridEl();
			var cm = gridOperada.getColumnModel();
			
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			cm.setHidden(cm.findColumnIndex('NETO_RECIBIR'), true);
			cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);

			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.bTipoFactoraje)	{
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido  || jsonValoresIniciales.bOperaFactVencimientoInfonavit)	{
					cm.setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaAcredito = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfAcred');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfAcred');
			if (!gridAcredito.isVisible()) {
				gridAcredito.show();
			}
			var el = gridAcredito.getGridEl();
			var cm = gridAcredito.getColumnModel();
			
			cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);

			if(store.getTotalCount() > 0)	{
				if (jsonValoresIniciales.bTipoFactoraje)	{
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarConsultaProgramado = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();
		var btnBajarPDF = Ext.getCmp('btnBajarPdfProgra');
		btnBajarPDF.hide();
		if (arrRegistros != null) {
			var btnGenerarPDF = Ext.getCmp('btnGenerarPdfProgra');
			if (!gridProgramado.isVisible()) {
				gridProgramado.show();
			}
			var el = gridProgramado.getGridEl();
			var cm = gridOperada.getColumnModel();
			
			cm.setHidden(cm.findColumnIndex('NETO_RECIBIR'), true);
			cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
			cm.setHidden(cm.findColumnIndex('MANDANTE'), true);

			if(store.getTotalCount() > 0) {
				if (jsonValoresIniciales.bTipoFactoraje)	{
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				if(jsonValoresIniciales.bOperaFactorajeDistribuido  || jsonValoresIniciales.bOperaFactVencimientoInfonavit)	{
					cm.setHidden(cm.findColumnIndex('NETO_RECIBIR'), false);
					cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
					cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
				}
				if(jsonValoresIniciales.bOperaFactConMandato)	{
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
				if (!btnGenerarPDF.isVisible()) {
					btnGenerarPDF.show();
				}
				btnGenerarPDF.enable();
				el.unmask();
			} else {
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}
//Fin de handlers.------

//---------------------------store--------------------------

	var consultaNegociable = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridNegociable'
		},
		fields: [
			{name: 'NOMBREEPO'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_AFORO'},
			{name: 'FN_AFORO_DSCTO', type: 'float'},
			{name: 'NOMBREIF'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaNegociable,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaNegociable(null, null, null);
				}
			}
		}
	});

	var consultaPignorado = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridPignorado'
		},
		fields: [
			{name: 'NOMBREEPO'},                    							//Nom. EPO
			{name: 'NOMBREIF'},														//NombreIF
			{name: 'IG_NUMERO_DOCTO'},              							//Num docto.
			{name: 'FECHA_DOCTO',	type: 'date', dateFormat: 'd/m/Y'},	//fecha_emision
			{name: 'FECHA_VENC',		type: 'date', dateFormat: 'd/m/Y'}, //fecha_vencimiento
			{name: 'CD_NOMBRE'},                    							//Moneda
			{name: 'FN_MONTO',		type: 'float'},							//Monto
			{name: 'FN_AFORO'},														//Porcentaje
			{name: 'FN_AFORO_DSCTO',type: 'float'},							//Monto Dscto
			{name: 'CS_DSCTO_ESPECIAL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaPignorado,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaPignorado(null, null, null);
				}
			}
		}
	});

	var consultaSelecPyme = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridSelecPyme'
		},
		fields: [
			{name: 'NOMBREEPO'},
			{name: 'IG_NUMERO_DOCTO'},
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},
			{name: 'TIPO_FACTORAJE'},
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_PORC_ANTICIPO',	type: 'float'},							//Porcentaje
			{name: 'FN_MONTO_DSCTO', type: 'float'},								//MONTO_DSCTO
			{name: 'NOMBREIF'},
			{name: 'IN_TASA_ACEPTADA', type: 'float'},
			{name: 'PLAZO', type: 'integer'},
			{name: 'IN_IMPORTE_INTERES', type: 'float'},
			{name: 'IN_IMPORTE_RECIBIR', type: 'float'},
			{name: 'CC_ACUSE'},
			{name: 'NETO_RECIBIR', type: 'float'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaSelecPyme,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaSelecPyme(null, null, null);
				}
			}
		}
	});

	var consultaAutorIF = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridAutorIF'
		},
		fields: [
			{name: 'NOMBREEPO'},                    //NOMBRE. EPO
			{name: 'IG_NUMERO_DOCTO'},              //Num docto.
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},                    								//Moneda
			{name: 'TIPO_FACTORAJE'},            									//Tipo factoraje	
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_PORC_ANTICIPO',	type: 'float'},							//Porcentaje
			{name: 'FN_MONTO_DSCTO', type: 'float'},								//MONTO_DSCTO
			{name: 'NOMBREIF'},
			{name: 'IN_TASA_ACEPTADA', type: 'float'},
			{name: 'PLAZO', type: 'integer'},
			{name: 'IN_IMPORTE_INTERES', type: 'float'},
			{name: 'IN_IMPORTE_RECIBIR', type: 'float'},
			{name: 'CC_ACUSE'},
			{name: 'NETO_RECIBIR', type: 'float'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaAutorIF,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaAutorIF(null, null, null);
				}
			}
		}
	});

	var consultaOperada = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridOperada'
		},
		fields: [
			{name: 'NOMBREEPO'},                    //NOMBRE. EPO
			{name: 'IG_NUMERO_DOCTO'},              //Num docto.
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},                    								//Moneda
			{name: 'TIPO_FACTORAJE'},            									//Tipo factoraje	
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_PORC_ANTICIPO',	type: 'float'},							//Porcentaje
			{name: 'FN_MONTO_DSCTO', type: 'float'},								//MONTO_DSCTO
			{name: 'NOMBREIF'},
			{name: 'IN_TASA_ACEPTADA', type: 'float'},
			{name: 'PLAZO', type: 'integer'},
			{name: 'IN_IMPORTE_INTERES', type: 'float'},
			{name: 'IN_IMPORTE_RECIBIR', type: 'float'},
			{name: 'CC_ACUSE'},
			{name: 'NETO_RECIBIR', type: 'float'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaOperada,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaOperada(null, null, null);
				}
			}
		}
	});

	var consultaAcredito = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridAcredito'
		},
		fields: [
			{name: 'NOMBREEPO'},                    	//Nom. EPO
			{name: 'NOMBREIF'},								//INTERMEDIARIO
			{name: 'IG_NUMERO_DOCTO'},              	//Num docto.
			{name: 'CC_ACUSE', type: 'float'},
			{name: 'DF_FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'IG_PLAZO', type: 'float'},
			{name: 'NOMBREMONEDA'},                    //Moneda
			{name: 'TIPO_FACTORAJE'},            //Tipo factoraje	
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_PORC_ANTICIPO'},											//Porcentaje
			{name: 'FN_MONTO_DSCTO', type: 'float'},				//FN_MONTO_DSCTO
			{name: 'IN_IMPORTE_INTERES', type: 'float'},
			{name: 'IN_IMPORTE_RECIBIR', type: 'float'},
			{name: 'IN_TASA_ACEPTADA', type: 'float'},
			{name: 'ESTATUSDOCUMENTO'},
			{name: 'DF_FECHA_SOLICITUD', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'NETO_RECIBIR', type: 'float'},
			{name: 'FN_MONTO_PAGO', type: 'float'},
			{name: 'PORCDOCTOAPLICADO'},											//OTRO_Porcentaje
			{name: 'IG_TIPO_PISO'},
			{name: 'FN_REMANENTE'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'IC_ESTATUS_DOCTO'},
			{name: 'IC_DOCUMENTO'},
			{name: 'CS_CAMBIO_IMPORTE'},
			{name: 'DETALLES'},
			{name: 'CS_DSCTO_ESPECIAL'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaAcredito,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaAcredito(null, null, null);
				}
			}
		}
	});

	var consultaProgramado = new Ext.data.JsonStore({
		root: 'registros',
		url : '13reporte01ext.data.jsp',
		baseParams: {
			informacion: 'consultaGridProgramado'
		},
		fields: [
			{name: 'NOMBREEPO'},                    //NOMBRE. EPO
			{name: 'IG_NUMERO_DOCTO'},              //Num docto.
			{name: 'FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'CD_NOMBRE'},                    								//Moneda
			{name: 'TIPO_FACTORAJE'},            									//Tipo factoraje	
			{name: 'FN_MONTO', type: 'float'},
			{name: 'FN_PORC_ANTICIPO',	type: 'float'},							//Porcentaje
			{name: 'FN_MONTO_DSCTO', type: 'float'},								//MONTO_DSCTO
			{name: 'NOMBREIF'},
			{name: 'IN_TASA_ACEPTADA', type: 'float'},
			{name: 'PLAZO', type: 'integer'},
			{name: 'IN_IMPORTE_INTERES', type: 'float'},
			{name: 'IN_IMPORTE_RECIBIR', type: 'float'},
			{name: 'CC_ACUSE'},
			{name: 'NETO_RECIBIR', type: 'float'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO'},
			{name: 'MANDANTE'},
			{name: 'CS_DSCTO_ESPECIAL'},
			{name: 'DF_PROGRAMACION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaProgramado,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					procesarConsultaProgramado(null, null, null);
				}
			}
		}
	});

//----------------------------------Contenido----------------------------------

	var gridNegociable = new Ext.grid.GridPanel({
		store: consultaNegociable,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 150,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 150
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	//5
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',	//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//7
				dataIndex : 'FN_AFORO',
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	//8
				//dataIndex : 'FN_AFORO_DSCTO',		//Calculado
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){ 
									var porcentaje = registro.get('FN_AFORO');
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											var dato = registro.get('CS_DSCTO_ESPECIAL');
											if(dato == "V")	{porcentaje = 100;}
											if(dato == "D")	{porcentaje = 100;}
											if(dato == "C")	{porcentaje = 100;}
											if(dato == "I")	{porcentaje = 100;}
										}
									}
									value = (registro.get('FN_MONTO') * (porcentaje / 100));
									return Ext.util.Format.number(value, '$0,0.00');	
								}
			},
			{
				header: 'Nombre IF', tooltip: 'Nombre IF',	//9
				//dataIndex: 'NOMBREIF',	//Calculado
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: true,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								value = "";
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('NOMBREIF');}
										if(registro.get('CS_DSCTO_ESPECIAL') == "A")	{value = registro.get('NOMBREIF');}
									}
								}
								return value;
							}
			},
			{
				header: 'Beneficiario', tooltip: 'Beneficiario', 	//10
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true//false
			},
			{
				header: '% Beneficiario', tooltip: '% Beneficiario', //11
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	width: 100,	resizable: true, hideable: true, align: 'right',
				hidden: true,	renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario', 	//12
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true//false
			},
			{
				header: 'Mandante', tooltip: 'Mandante', 	//13
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true//false
			},
			{
				header: 'Clave_tipoFactoraje', tooltip: 'Clave_tipoFactoraje', 	//14
				dataIndex: 'CS_DSCTO_ESPECIAL', renderer: 'ejemploRenderer',
				sortable: true,	width: 250,	resizable: true, hidden: true
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Negociable',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfNego',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfNego
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfNego',
					hidden: true
				}
			]
		}
	});

	var gridPignorado = new Ext.grid.GridPanel({
		store: consultaPignorado,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header: 'Nombre IF', tooltip: 'Nombre IF',	//1
				dataIndex: 'NOMBREIF',
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//2
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 150,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//3
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//4
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//5
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 150
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',	//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//7
				dataIndex : 'FN_AFORO',
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',	//8
				//dataIndex : 'FN_AFORO_DSCTO',		//Calculado
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){ 
									var porcentaje = registro.get('FN_AFORO');
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											var dato = registro.get('CS_DSCTO_ESPECIAL');
											if(dato == "V")	{porcentaje = 100;}
											if(dato == "D")	{porcentaje = 100;}
											if(dato == "C")	{porcentaje = 100;}
											if(dato == "I")	{porcentaje = 100;}
										}
									}
									value = (registro.get('FN_MONTO') * (porcentaje / 100));
									return Ext.util.Format.number(value, '$0,0.00');	
								}
		}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Proceso Ignorado',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfPigno',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfPigno
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfPigno',
					hidden: true
				}
			]
		}
	});

	var gridSelecPyme = new Ext.grid.GridPanel({
		store: consultaSelecPyme,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 150,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 150
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	//5
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',	//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//7
				dataIndex : 'FN_PORC_ANTICIPO',
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',						//8
				dataIndex : 'FN_MONTO_DSCTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('FN_MONTO');}
										}
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},
			{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero',				//9
				dataIndex: 'NOMBREIF',
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Tasa', tooltip: 'Tasa',														//10
				dataIndex : 'IN_TASA_ACEPTADA',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
				//renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo (d�as)', tooltip: 'Plazo (d�as)',									//11
				dataIndex : 'PLAZO',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
			},
			{
				header : 'Monto Int.', tooltip: 'Monto Int.',										//12
				dataIndex : 'IN_IMPORTE_INTERES',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Importe a Recibir', tooltip: 'Importe a Recibir',							//13
				dataIndex : 'IN_IMPORTE_RECIBIR',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'N�mero de Acuse', tooltip: 'N�mero de Acuse',									//14
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: false
			},
			{
				header: 'Neto a Recibir Pyme', tooltip: 'Neto a Recibir Pyme',							//15
				dataIndex: 'NETO_RECIBIR',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true
			},
			{
				header: 'Beneficiario', tooltip: 'Beneficiario',											//16
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true
			},
			{
				header: '% Beneficiario', tooltip: '% Beneficiario',										//17
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	width: 100,	resizable: true, hideable: true, align: 'right',
				hidden: true,	renderer: Ext.util.Format.numberRenderer('0.00%') 
			},
			{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario',		//18
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true
			},
			{
				header: 'Mandante', tooltip: 'Mandante',																	//19
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header: 'Clave_tipoFactoraje', tooltip: 'Clave_tipoFactoraje', 									//20
				dataIndex: 'CS_DSCTO_ESPECIAL',
				sortable: true,	width: 250,	resizable: true, hidden: true
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Seleccionada PYME',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfSelec',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfSelec
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfSelec',
					hidden: true
				}
			]
		}
	});

	var gridAutorIF = new Ext.grid.GridPanel({
		store: consultaAutorIF,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 150,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 150
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	//5
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',	//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//7
				dataIndex : 'FN_PORC_ANTICIPO',
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',						//8
				dataIndex : 'FN_MONTO_DSCTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('FN_MONTO');}
										}
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},
			{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero',				//9
				dataIndex: 'NOMBREIF',
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Tasa', tooltip: 'Tasa',														//10
				dataIndex : 'IN_TASA_ACEPTADA',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
				//renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo (d�as)', tooltip: 'Plazo (d�as)',									//11
				dataIndex : 'PLAZO',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
			},
			{
				header : 'Monto Int.', tooltip: 'Monto Int.',										//12
				dataIndex : 'IN_IMPORTE_INTERES',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Importe a Recibir', tooltip: 'Importe a Recibir',							//13
				dataIndex : 'IN_IMPORTE_RECIBIR',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'N�mero de Acuse', tooltip: 'N�mero de Acuse',									//14
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: false
			},
			{
				header: 'Neto a Recibir Pyme', tooltip: 'Neto a Recibir Pyme',							//15
				dataIndex: 'NETO_RECIBIR',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true
			},
			{
				header: 'Beneficiario', tooltip: 'Beneficiario',											//16
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true
			},
			{
				header: '% Beneficiario', tooltip: '% Beneficiario',										//17
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	width: 100,	resizable: true, hideable: true, align: 'right',
				hidden: true,	renderer: Ext.util.Format.numberRenderer('0.00%') 
			},
			{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario',		//18
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true
			},
			{
				header: 'Mandante', tooltip: 'Mandante',																	//19
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header: 'Clave_tipoFactoraje', tooltip: 'Clave_tipoFactoraje', 									//20
				dataIndex: 'CS_DSCTO_ESPECIAL',
				sortable: true,	width: 250,	resizable: true, hidden: true
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: En Proceso de Autorizaci�n IF',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfAutor',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfAutor
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfAutor',
					hidden: true
				}
			]
		}
	});

	var gridOperada = new Ext.grid.GridPanel({
		store: consultaOperada,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 150,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 150
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	//5
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',	//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//7
				dataIndex : 'FN_PORC_ANTICIPO',
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',						//8
				dataIndex : 'FN_MONTO_DSCTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('FN_MONTO');}
										}
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},
			{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero',				//9
				dataIndex: 'NOMBREIF',
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Tasa', tooltip: 'Tasa',														//10
				dataIndex : 'IN_TASA_ACEPTADA',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
				//renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo (d�as)', tooltip: 'Plazo (d�as)',									//11
				dataIndex : 'PLAZO',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
			},
			{
				header : 'Monto Int.', tooltip: 'Monto Int.',										//12
				dataIndex : 'IN_IMPORTE_INTERES',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Importe a Recibir', tooltip: 'Importe a Recibir',							//13
				dataIndex : 'IN_IMPORTE_RECIBIR',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'N�mero de Acuse', tooltip: 'N�mero de Acuse',									//14
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: false
			},
			{
				header: 'Neto a Recibir Pyme', tooltip: 'Neto a Recibir Pyme',							//15
				dataIndex: 'NETO_RECIBIR',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true
			},
			{
				header: 'Beneficiario', tooltip: 'Beneficiario',											//16
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true
			},
			{
				header: '% Beneficiario', tooltip: '% Beneficiario',										//17
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	width: 100,	resizable: true, hideable: true, align: 'right',
				hidden: true,	renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario',		//18
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true
			},
			{
				header: 'Mandante', tooltip: 'Mandante',																	//19
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header: 'Clave_tipoFactoraje', tooltip: 'Clave_tipoFactoraje', 									//20
				dataIndex: 'CS_DSCTO_ESPECIAL',
				sortable: true,	width: 250,	resizable: true, hidden: true
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Operada',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfOpera',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfOpera
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfOpera',
					hidden: true
				}
			]
		}
	});

	var gridAcredito = new Ext.grid.GridPanel({
		store: consultaAcredito,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero',				//1
				dataIndex: 'NOMBREIF',
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',									//2
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 150,
				sortable : true, hidden: false
			},
			{
				header: 'N�mero de Acuse', tooltip: 'N�mero de Acuse',										//3
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',									//4
				dataIndex : 'DF_FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',							//5
				dataIndex : 'DF_FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Plazo', tooltip: 'Plazo',																	//6
				dataIndex : 'IG_PLAZO',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
			},
			{
				header : 'Moneda', tooltip: 'Moneda',																//7
				dataIndex : 'NOMBREMONEDA',
				sortable : true,
				hideable : false, hidden: false,
				width : 150
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',											//8
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto Documento', tooltip: 'Monto del Documento',									//9
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento',					//10
				dataIndex : 'FN_PORC_ANTICIPO',
				width : 100, align: 'right', hideable: true, hidden: false,	sortable : true,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',									//11
				dataIndex : 'FN_MONTO_DSCTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('FN_MONTO');}
										}
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},
			{
				header : 'Intereses del documento', tooltip: 'Intereses del documento',					//12
				dataIndex : 'IN_IMPORTE_INTERES',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Monto a Recibir', tooltip: 'Monto a Recibir',										//13
				dataIndex : 'IN_IMPORTE_RECIBIR',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Tasa', tooltip: 'Tasa',																	//14
				dataIndex : 'IN_TASA_ACEPTADA',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
				//renderer: Ext.util.Format.number('0.0000%')
				//renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Estatus', tooltip: 'Estatus',																//15
				dataIndex: 'ESTATUSDOCUMENTO',
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header : 'Fecha de Operaci�n IF', tooltip: 'Fecha de Operaci�n IF',						//16
				dataIndex : 'DF_FECHA_SOLICITUD',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Importe Aplicado a Cr�dito', tooltip: 'Importe Aplicado a Cr�dito',			//17
				dataIndex: 'FN_MONTO_PAGO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje del Documento Aplicado',	sortable : true,							//18
				tooltip: 'Porcentaje del Documento Aplicado', 
				dataIndex : 'PORCDOCTOAPLICADO',
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: Ext.util.Format.number('0.0000%')
			},
			{
				header: 'Importe a Depositar PyME', tooltip: 'Importe a Depositar PyME',				//19
				//dataIndex: 'FN_REMANENTE',		//Calculado
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true,
				renderer:	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (registro.get('IG_TIPO_PISO') == "1")	{
										if (registro.get('FN_REMANENTE') != "" && registro.get('FN_REMANENTE') != null)	{
											value = registro.get('FN_REMANENTE');
										}else	{
											value = "";
										}
									}else	{
										if ((registro.get('IN_IMPORTE_RECIBIR') - registro.get('IN_IMPORTE_RECIBIR')) > 0 )	{
											value = (registro.get('IN_IMPORTE_RECIBIR') - registro.get('IN_IMPORTE_RECIBIR')).toString();
										}else	{
											value = "0";
										}
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},
			{
				header: 'Mandante', tooltip: 'Mandante',																	//20
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header: 'Clave_tipoFactoraje', tooltip: 'Clave_tipoFactoraje', 									//20
				dataIndex: 'CS_DSCTO_ESPECIAL',
				sortable: true,	width: 250,	resizable: true, hidden: true
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Aplicado a Credito',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfAcred',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfAcred
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfAcred',
					hidden: true
				}
			]
		}
	});

	var gridProgramado = new Ext.grid.GridPanel({
		store: consultaProgramado,
		hidden: true,
		columns: [
			{
				header: 'Nombre EPO', tooltip: 'Nombre EPO',	//0
				dataIndex: 'NOMBREEPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',	//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 150,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',	//2
				dataIndex : 'FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',	//3
				dataIndex : 'FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',	//4
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 150
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',	//5
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',	//6
				dataIndex : 'FN_MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento',	tooltip: 'Porcentaje de Descuento', sortable : true,		//7
				dataIndex : 'FN_PORC_ANTICIPO',
				width : 100, align: 'right', hideable: true, hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store){
								if (jsonValoresIniciales.bTipoFactoraje != undefined){
									if (jsonValoresIniciales.bTipoFactoraje)	{
										if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = 100;}
										if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = 100;}
									}
								}
								return Ext.util.Format.number(value,'0%');
							}
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',						//8
				dataIndex : 'FN_MONTO_DSCTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: 	function (value, metaData, registro, rowIndex, colIndex, store)	{
									if (jsonValoresIniciales.bTipoFactoraje != undefined){
										if (jsonValoresIniciales.bTipoFactoraje)	{
											if(registro.get('CS_DSCTO_ESPECIAL') == "V")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "D")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "C")	{value = registro.get('FN_MONTO');}
											if(registro.get('CS_DSCTO_ESPECIAL') == "I")	{value = registro.get('FN_MONTO');}
										}
									}
									return Ext.util.Format.number(value, '$0,0.00');
								}
			},
			{
				header: 'Intermediario Financiero', tooltip: 'Intermediario Financiero',				//9
				dataIndex: 'NOMBREIF',
				sortable: true,	width: 250,	resizable: true, hideable : true, hidden: false
			},
			{
				header : 'Tasa', tooltip: 'Tasa',														//10
				dataIndex : 'IN_TASA_ACEPTADA',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
				//renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Plazo (d�as)', tooltip: 'Plazo (d�as)',									//11
				dataIndex : 'PLAZO',
				sortable : true,
				width : 100,
				align: 'right', hidden: false
			},
			{
				header : 'Monto Int.', tooltip: 'Monto Int.',										//12
				dataIndex : 'IN_IMPORTE_INTERES',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Importe a Recibir', tooltip: 'Importe a Recibir',							//13
				dataIndex : 'IN_IMPORTE_RECIBIR',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'N�mero de Acuse', tooltip: 'N�mero de Acuse',									//14
				dataIndex: 'CC_ACUSE',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: false
			},
			{
				header: 'Neto a Recibir Pyme', tooltip: 'Neto a Recibir Pyme',							//15
				dataIndex: 'NETO_RECIBIR',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true
			},
			{
				header: 'Beneficiario', tooltip: 'Beneficiario',											//16
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true
			},
			{
				header: '% Beneficiario', tooltip: '% Beneficiario',										//17
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	width: 100,	resizable: true, hideable: true, align: 'right',
				hidden: true,	renderer: Ext.util.Format.numberRenderer('0.00%')
			},
			{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario',		//18
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true
			},
			{
				header: 'Mandante', tooltip: 'Mandante',																	//19
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header: 'Clave_tipoFactoraje', tooltip: 'Clave_tipoFactoraje', 									//20
				dataIndex: 'CS_DSCTO_ESPECIAL',
				sortable: true,	width: 250,	resizable: true, hidden: true
			},
			{
				header : 'Fecha Registro Operaci�n', tooltip: 'Fecha Registro Operaci�n',	//2
				dataIndex : 'DF_PROGRAMACION',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 940,
		title: 'Estatus: Programado PyME',
		frame: true,
		bbar: {
			items: [
				'->','-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					tooltip:	'Generar archivo PDF',
					id: 'btnGenerarPdfProgra',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13reporte01extpdf.jsp',
							params: {claveStatus:	cveEstatus},
							callback: procesarSuccessFailureGenerarPdfProgra
						});
					}
				},'-',
				{
					xtype: 'button',
					text: 'Bajar PDF',
					tooltip:	'Descargar archivo PDF',
					id: 'btnBajarPdfProgra',
					hidden: true
				}
			]
		}
	});
// Fin de declaraci�n de Grid�s
	var dataStatus = new Ext.data.ArrayStore({
		 fields: ['clave', 'descripcion'],
		 data : [
			["1",'Negociable'],
			["2",'Pignorado'],
			["3",'Seleccionada PYME'],
			["4",'En Proceso de Autorizaci�n IF'],
			["5",'Operada'],
			//["6",'Aplicado a Cr�dito'],	FODEA 17
			["7",'Programado PyME']
		 ]
	});
	
	var elementosForma = [
		{
			xtype: 'combo',	
			name: 'status',	
			id: 'cmbStatus',	
			fieldLabel: 'Estatus', 
			mode: 'local',	
			hiddenName : 'status',	
			emptyText: 'Seleccione Estatus . . . ',
			forceSelection : true,	triggerAction : 'all',	typeAhead: true,
			minChars : 1,	
			store : dataStatus,	
			displayField : 'descripcion',	valueField : 'clave',
			listeners: {
				select: {
					fn: function(combo) {
						cveEstatus = combo.getValue();
						gridNegociable.hide();
						gridPignorado.hide();
						gridSelecPyme.hide();
						gridAutorIF.hide();
						gridOperada.hide();
						gridAcredito.hide();
						gridProgramado.hide();
						fp.el.mask('Enviando...', 'x-mask-loading');
						if (combo.value == "1")	{
							gridNegociable.setTitle('<div><div style="float:left">Estatus: Negociable</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaNegociable.load();
						}
						if (combo.value == "2")	{
							gridPignorado.setTitle('<div><div style="float:left">Estatus: Proceso Ignorado</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');						
							consultaPignorado.load();
						}
						if (combo.value == "3")	{
							gridSelecPyme.setTitle('<div><div style="float:left">Estatus: Seleccionada PYME</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaSelecPyme.load();
						}
						if (combo.value == "4")	{
							gridAutorIF.setTitle('<div><div style="float:left">Estatus: En Proceso de Autorizaci�n IF</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaAutorIF.load();
						}
						if (combo.value == "5")	{
							gridOperada.setTitle('<div><div style="float:left">Estatus: Operada</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaOperada.load();
						}
						if (combo.value == "6")	{
							gridAcredito.setTitle('<div><div style="float:left">Estatus: Aplicado a Cr�dito</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaAcredito.load();
						}
						if (combo.value == "7")	{
							gridProgramado.setTitle('<div><div style="float:left">Estatus: Programado PyME</div><div style="float:right">Fecha: ' + jsonValoresIniciales.Fecha + '</div></div>');
							consultaProgramado.load();
						}
					}
				}
			}
		}
	];
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 500,
		style: ' margin:0 auto;',
		frame: true,
		border: false,
		title: '<div><center>Solic. por Estatus</div>',
		collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 60,
		defaultType: 'textfield',
		hidden: true,
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:	elementosForma,
		monitorValid: true
	});

//Simulacion en un contenedor
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,	NE.util.getEspaciador(20),
			gridNegociable,	
			gridPignorado,		
			gridSelecPyme,		
			gridAutorIF,		
			gridOperada,		
			gridAcredito,		
			gridProgramado
		]
	});
	//Peticion para obtener valores iniciales y la parametrizaci�n
	Ext.Ajax.request({
		url: '13reporte01ext.data.jsp',
		params: {
			informacion: "valoresIniciales"
		},
		callback: procesaValoresIniciales
	});

});//Fin de funcion principal