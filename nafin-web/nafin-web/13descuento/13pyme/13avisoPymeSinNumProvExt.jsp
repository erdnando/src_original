<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252" errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<%@ include file="/extjs.jspf" %>
<%@ include file="/01principal/menu.jspf"%>
<script type="text/javascript" src="13avisoPymeSinNumProvExt.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<%@ include file="/01principal/01pyme/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01pyme/menuLateralFlotante.jspf"%>
						<div id="areaContenido"><div style="height:190px"></div></div>
						<!--div id='areaContenido' style="margin-left: 3px; margin-top: 3px;"></div-->
					</div>
				</div>
				<%@ include file="/01principal/01pyme/pie.jspf"%>

<%-- Como el contenido es completamente est�tico, se realiza de esta manera... de lo contrario usar un .data. --%>
<div id="mensaje" style="display:none">
Por el momento no puede operar el servicio en la Cadena Productiva seleccionada,<br>
ya que no cuenta con su n�mero de proveedor.
<br><br><br><br>
<span style="font-weight:bold">Favor de contactar al Administrador de su Cadena Productiva.</span>
</div>
<form id='formAux' name="formAux" target='_new'></form>

</body>
</html>
