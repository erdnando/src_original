Ext.onReady(function(){
	Ext.QuickTips.init(); 
//-------------------------------VARIABLES--------------------------------------
	var jsonValoresIniciales = null;
	var hidAction = "";
//-------------------------------FUNCIONES--------------------------------------
	var procesaActualizacion = function(opts, success, response){
		var lblEnunciado3 = Ext.getCmp('lblEnunciado3');
		var lblEnunciado4 = Ext.getCmp('lblEnunciado4');
		var lblEnunciado5 = Ext.getCmp('lblEnunciado5');
		var chkMostrarPromocion = Ext.getCmp('chkMostrarPromocion');
		var btnMostrarDatos = Ext.getCmp('btnMostrarDatos');
		if(success == true && Ext.util.JSON.decode(response.responseText).success == true){
			if(hidAction=="M"){			
					lblEnunciado3.show();
					lblEnunciado4.show();
					lblEnunciado5.show();
					chkMostrarPromocion.hide();
					btnMostrarDatos.hide();
					return;
			}
			//Al presionar el bot�n continuar y marcar el checkbox de no volver a mostrar esta promoci�n
			//se actualizan los datos y se va a la siguiente p�gina.
			if(hidAction=="C"){
					Ext.Ajax.request({
						url:'13creditoext.data.jsp',
						params: {
								informacion: 'Continuar'
						},
						callback: procesaContinuar
					});
			}
		}else{
			Ext.Msg.alert('Mensaje de Error',"�Error al actualizar!",function(btn){  
					return;	 
			});
		}
	}
	function procesaValoresIniciales(opts, success, response) {
		fp.el.mask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
				jsonValoresIniciales = Ext.util.JSON.decode(response.responseText);
				var lblEnunciado1 = Ext.getCmp('lblEnunciado1');
				var lblEnunciado2 = Ext.getCmp('lblEnunciado2');
				var lblEnunciado3 = Ext.getCmp('lblEnunciado3');
				var lblEnunciado4 = Ext.getCmp('lblEnunciado4');
				var lblEnunciado5 = Ext.getCmp('lblEnunciado5');
				lblEnunciado3.hide();
				lblEnunciado4.hide();
				lblEnunciado5.hide();
				if(jsonValoresIniciales != null){	
					lblEnunciado1.getEl().update('Para ti, que formas parte de la cadena Productiva <b>'+jsonValoresIniciales.NOMBRE_EPO+'</b>, te informamos que:');
					if(jsonValoresIniciales.MONTO!=''){
						lblEnunciado2.getEl().update('$'+jsonValoresIniciales.MONTO+' pesos*');
					}
					if(jsonValoresIniciales.NOM_EJECUTIVO!=''){
						lblEnunciado3.getEl().update('Su nombre es: '+jsonValoresIniciales.NOM_EJECUTIVO);
					}
					if(jsonValoresIniciales.TELEFONO!=''){
						lblEnunciado4.getEl().update('Tel�fono: '+jsonValoresIniciales.TELEFONO);
					}
					if(jsonValoresIniciales.CORREO!=''){
						lblEnunciado5.getEl().update('Correo electr�nico: '+jsonValoresIniciales.CORREO);
					}
					fp.el.unmask();
				}
		} else {
				NE.util.mostrarConnError(response,opts);
		}
	}
	function procesaContinuar(opts, success, response){
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true){
			fp.el.unmask();
			redirecciona = Ext.util.JSON.decode(response.responseText).urlPagina;
			Ext.Msg.show({	msg: 'Su informaci�n fue actualizada con �xito',buttons: Ext.Msg.OK,	fn: processResultIns,animEl: 'elId',icon: Ext.MessageBox.INFO});
		} else {
			NE.util.mostrarConnError(response, opts);
		}
	}
	function processResultIns(){
		window.location = redirecciona;
	}
//------------------------------COMPONENTES---------------------------------------	
	var elementosFormaEncabezado = [
		{
			xtype: 'container',
			anchor: '100%',
			layout: {
						type: 'table',
						columns: 3
			},
			items: [
						{
							html:'<img src="/nafin/00utils/gif/cred01.gif" alt="" border="0">',
							width: 142,
							height: 65,
							colspan: 1
						},
						{
							html:'</br>&nbsp;&nbsp;Con Cadenas Productivas, tu historial de cuentas por cobrar te brinda beneficios...',
							style: 'background-color:#16165D;font-family:Arial;font-size:1.3em;text-align:center;color:#FFFFFF;font-weight:bold;display:block;',
							width: 383,
							height: 65,
							colspan: 1
						},
						{
							html:'<img src="/nafin/00archivos/15cadenas/15archcadenas/logos/nafin.gif" alt="" border="0">',
							width: 175,
							height: 65,
							colspan: 1
						}			
			]
		}
	];
	var elementosForma = [
		{
			xtype: 'container',
			anchor: '100%',
			layout: {
						type: 'table',
						columns: 2
						},
						items: [
									{
										html:'<div style="height:30px"></div>',
										width: 694,
										colspan: 2
									},
									{
										xtype: 'label',
										id: 'lblEnunciado1',
										style: 'font-family:Arial;font-size:1.2em;text-align:left;display:block;',
										text: 'Para ti, que formas parte de la cadena Productiva, te informamos que:',
										width: 694,
										colspan:2
									},
									{
										html:'<div style="height:30px"></div>',
										width: 694,
										colspan: 2
									},
									{
										html:'En Banca Mifel tenemos un cr�dito, para ti hasta por:',
										style: 'font-family:Arial;font-size:1.6em;text-align:center;font-weight:bold;display:block;',
										width: 694,
										height: 30,
										colspan: 2
									},
									{
										xtype: 'label',
										id: 'lblEnunciado2',
										style: 'font-family:Arial;font-size:1.6em;font-weight:bold;text-align:center;color:#FF870F;display:block;',
										text: '$ pesos*',
										width: 694,
										colspan: 2
									},
									{
										html:'para pagar hasta en 36 meses</br>Sin Garant�a',
										style: 'font-family:Arial;font-size:1.2em;text-align:center;color:#FF870F;display:block;',
										width: 694,
										height: 65,
										colspan: 2
									},
									{
										html:'<font size="small" face="Arial" color="#FF870F"><b>Es muy F�cil</b></font>,�s�lo contacta a tu ejecutivo',
										style: 'font-family:Arial;font-size:1.2em;text-align:center;display:block;',
										width: 347,
										colspan: 1
									},
									{
										html:'<font size="small" face="Arial" color="#FF870F"><b>L�quidez para tu Negocio,</b></font>�ya que podr�s:</br>',
										style: 'font-family:Arial;font-size:1.2em;text-align:center;display:block;',
										width: 347,
										colspan: 1
									},
									{
										html:'<div style="height:15px"></div>',
										width: 347,
										colspan: 1
									},
									{
										html:'<ul type="disk"><li>Adquirir materia prima.<li>Solventar gastos de fabricaci�n y operaci�n.<li>Pagar sueldos y salarios.<li>Pagar a proveedores.</ul>',
										style: 'list-style-type:square;font-family:Arial;font-size:1.2em;text-align:left;text-indent:5em;display:block;',
										width: 347,
										colspan: 1,
										rowspan: 5
									},
									{
										xtype:'button',
										style: 'margin:0 auto;',
										id: 'btnMostrarDatos',
										text: 'Mostrar datos del <br>ejecutivo a contactar',
										scale: 'medium',
										iconCls:'icoMostrarDatos',
										formBind: true,
										colspan: 1,
										handler: function(boton,evento){
														hidAction				= "M";
														Ext.Ajax.request({
															url: '13creditoext.data.jsp',
															params: {
																			informacion: 'ActualizarDatos',
																			cs_estatus_envio: "N",
																			cs_estatus_respuesta: "S"
															},
															callback: procesaActualizacion
														});											
										}
									},
									{
										xtype: 'label',
										id: 'lblEnunciado3',
										style: 'font-family:Arial;font-weight:bold;font-size:1.2em;text-align:left;text-indent:5em;display:block;',
										width: 347,
										text: 'Su nombre es: ',
										colspan:1
									},
									{
										xtype: 'label',
										id: 'lblEnunciado4',
										style: 'font-family:Arial;font-size:1.2em;text-align:left;text-indent:5em;display:block;',
										width: 347,
										text: 'Tel�fono: ',
										colspan:1
									},
									{
										xtype: 'label',
										id: 'lblEnunciado5',
										style: 'font-family:Arial;font-size:1.2em;text-align:left;text-indent:5em;display:block;',
										width: 347,
										text: 'Correo electr�nico: ',
										colspan:1
									},
									{
										html:'<div style="height:30px"></div>',
										width: 694,
										colspan: 2
									},
									{
										html:'Los pagos son mensuales y con tasa de inter�s preferencial.</br>M�nimos requisitos y plazos para pagar a 36 meses',
										style: 'font-family:Arial;font-size:1.2em;text-align:left;display:block;',
										width: 347,
										colspan: 1
									},
									{
										html: '<img src="/nafin/00archivos/15cadenas/15archcadenas/logos/mifel.gif" alt="Banca Mifel" border="0">',
										style: 'text-align:center',
										colspan: 1,
										width: 347,
										rowspan: 2
									},
									{
										html:'�Solicita tu Cr�dito Productivo ahora!',
										style: 'font-family:Arial;font-size:1.6em;color:#FF870F;text-align:left;font-weight:bold;display:block;',
										width: 347,
										colspan: 1
									},
									{
										html:'<div style="height:30px"></div>',
										width: 694,
										colspan: 2
									},
									{
										html:'Para mayor informaci�n.</br>Desde el D.F. al 50 89 61 07 o del interior al 01-800-NAFINSA(623 4672)',
										style: 'font-family:Arial;font-size:1.2em;text-align:left;display:block;',
										width: 347,
										colspan: 1,
										rowspan: 2
									},
									{
										html:'*Sujeto a aprobaci�n de cr�dito por parte del banco.',
										style: 'font-family:Arial;font-size:1.2em;text-align:left;display:block;',
										width: 347,
										colspan: 1
									},
									{
										xtype: 'checkbox',
										boxLabel:'<font size="2" face="Arial">NO volver a mostrar esta promoci�n.</font>',
										name:'chkMostrar',
										id:'chkMostrarPromocion',
										inputValue:'N',
										msgTarget: 'side',
										anchor : '-25',
										width: 347,
										colspan: 1
									},
									{
										xtype:'button',
										style: 'margin-left:530px;margin-top:10px;margin-bottom:10px',
										id: 'btnContinuar',
										text: 'Continuar con <br>Descuento Electr�nico',
										iconCls: 'icoMedAbrirMenu',
										scale: 'medium',
										colspan: 2,
										handler: function(boton,evento){
														var chkMostrarPromocion = Ext.getCmp('chkMostrarPromocion');
														hidAction				= "C";
														if(chkMostrarPromocion.getValue()==true){
															Ext.Ajax.request({
																url: '13creditoext.data.jsp',
																params: {
																			informacion: 'ActualizarDatos',
																			cs_estatus_envio: "N",
																			cs_estatus_respuesta: "R"
																},
																callback: procesaActualizacion
															});	
														}else{
																Ext.Ajax.request({
																	url:'13creditoext.data.jsp',
																	params: {
																		informacion: 'Continuar'
																	},
																	callback: procesaContinuar
																});
														}
										}
									}
						]
						
			}
	];
	var fp = new Ext.FormPanel({
		id: 'forma',
		style: 'margin:0 auto;',
		title: '',
		hidden: false,
		frame: true,
		titleCollapse: false,
		bodyStyle: 'padding: 0px',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-50'
		},
		width: 702,
		bodyBorder: false, 
		border: false, 
		hideBorders: true,
		items: [elementosFormaEncabezado,elementosForma],
		monitorValid: false
	});
//-----------------------------------PRINCIPAL----------------------------------
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 710,
		height: 'auto',
		items:[
			fp,
			NE.util.getEspaciador(20)
		]
	});
//Petici�n para obtener valores iniciales y la parametrizaci�n.
	Ext.Ajax.request({
		url: '13creditoext.data.jsp',
		params: {
					informacion: "valoresIniciales"
		}, 
		callback: procesaValoresIniciales
	});
});