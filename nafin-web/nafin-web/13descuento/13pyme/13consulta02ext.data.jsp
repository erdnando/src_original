<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.sql.*,
		netropology.utilerias.*,com.netro.descuento.*,
		com.netro.model.catalogos.CatalogoEPOPyme,
		com.netro.model.catalogos.CatalogoIF,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMandante,
		com.netro.model.catalogos.CatalogoCamposAdicionales,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String noEpo = (request.getParameter("no_epo")!=null)?request.getParameter("no_epo"):"";
String infoRegresar	=	"";

ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

Hashtable alParamEPO = new Hashtable();
double dblPorciento = 0, dblPorcientoDL = 0;
	
if (!noEpo.equals("")) {
	Registros reg = new Registros();
	reg = BeanParamDscto.getTipoAforoPantalla(noEpo);
	while(reg.next()){
		dblPorciento = Double.parseDouble(reg.getString(1));
		dblPorcientoDL = Double.parseDouble(reg.getString(2));
	}
}
if (informacion.equals("CatalogoEPOPyme")) {
	CatalogoEPOPyme cat = new CatalogoEPOPyme();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClavePyme(iNoCliente);
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("CatalogoIf")) {
	try {
		if (noEpo.equals("")) {
			throw new Exception("Los parametros son requeridos");
		}
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	CatalogoIF cat = new CatalogoIF();
	cat.setClave("ic_if");
	cat.setDescripcion("cg_razon_social");
	cat.setIc_epo(noEpo);
	cat.setG_orden("ic_if");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("CatalogoMoneda")) {
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
//		cat.setOrden("cg_razon_social"); //Si existiera ambigüedad se puede usar cat.getPrefijoTablaPrincipal() para usarlo al armar el orden.
	infoRegresar = cat.getJSONElementos();
  
}else if (informacion.equals("CatalogoCambioEstatus")) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_estatus_docto");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_estatus_docto");
	cat.setValoresCondicionIn("1,2,5,6,7,9,10,21,3,4,8,11,12,16,24,26", Integer.class);
	cat.setOrden("ic_estatus_docto");
	infoRegresar = cat.getJSONElementos();
  
}else if (informacion.equals("CatalogoFactoraje")) {
	
	String iEpo =  (request.getParameter("epo")!=null)?request.getParameter("epo"):"";
	boolean bOperaFactorajeVencido = false;
	boolean bOperaFactorajeIF = false;
	boolean bOperaFactVencimientoInfonavit = false;
	boolean bOperaFactorajeDistribuido = false;
	boolean bOperaNotasDeCredito = false;
	boolean bOperaFactConMandato = false;
	boolean bTipoFactoraje = false;
	
	alParamEPO = new Hashtable();
		
	if(!iEpo.equals("")) { 
		alParamEPO = BeanParamDscto.getParametrosEPO(iEpo, 1);
	}
	else if(!iNoEPO.equals("")) {
		alParamEPO = BeanParamDscto.getParametrosEPO(iNoEPO, 1);
	}
	if (alParamEPO!=null) {
			bOperaFactConMandato   		  = ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
			bOperaFactorajeVencido 		  = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
			bOperaFactorajeIF = ("N".equals(alParamEPO.get("FACTORAJE_IF").toString())) ? false : true ;
			bOperaFactorajeDistribuido   = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
			bOperaNotasDeCredito 		  = ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
			bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString())) ? false : true ;
	}
	
	bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaNotasDeCredito || bOperaFactConMandato || bOperaFactVencimientoInfonavit || bOperaFactorajeIF)?true:false;

	if (bTipoFactoraje) {
		StringBuffer tipoFactCombo = new StringBuffer();
		//tipoFactCombo.append("N,V,D"); // estos tipos de factoraje aparecen por default en el combo (NORMAL, VENCIDO, DISTRIBUIDO)
		tipoFactCombo.append("N"); // el factoraje por default es NORMAL F026-2012
		
		if(bOperaFactorajeVencido) {
			tipoFactCombo.append(",V");
		}
		if(bOperaFactorajeDistribuido) {
			tipoFactCombo.append(",D");
		}		
		if(bOperaNotasDeCredito){
			tipoFactCombo.append(",C");
		}
		if(bOperaFactConMandato){
			tipoFactCombo.append(",M");
		}
		if(bOperaFactVencimientoInfonavit){
			tipoFactCombo.append(",I");
		}
		if(bOperaFactorajeIF){
			tipoFactCombo.append(",A");
		}
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("cc_tipo_factoraje");
		cat.setCampoDescripcion("cg_nombre");
		cat.setTabla("comcat_tipo_factoraje");
		cat.setValoresCondicionIn(tipoFactCombo.toString(), String.class);
		cat.setOrden("cc_tipo_factoraje");
		infoRegresar = cat.getJSONElementos();
		
	} else {
		infoRegresar = "{\"success\": true, \"total\": 0, \"registros\": [], \"noAplica\": true }";
	}
//Thread.sleep(15*1000);
}else if(informacion.equals("CatalogoMandante")){

	String claveMandante = (request.getParameter("claveMandante")!=null)?request.getParameter("claveMandante"):"";
	if (claveMandante != null && !claveMandante.equals("")){
		CatalogoMandante cat = new CatalogoMandante();
		cat.setCampoClave("ic_mandante");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setTipoAfiliado("M");
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	}
}else if(informacion.equals("CatalogoCamposAdicionales")){
		CatalogoCamposAdicionales cat = new CatalogoCamposAdicionales();
		cat.setCampoClave("cg_campo1");
		cat.setCampoDescripcion("cg_campo1");
		cat.setClavePyme(iNoCliente);
		cat.setClaveEpo(noEpo);
		cat.setOrden("cg_campo1");
		infoRegresar = cat.getJSONElementos();

}else if(informacion.equals("CatalogoCamposAdicionales_2")){
		CatalogoCamposAdicionales cat = new CatalogoCamposAdicionales();
		cat.setCampoClave("cg_campo2");
		cat.setCampoDescripcion("cg_campo2");
		cat.setClavePyme(iNoCliente);
		cat.setClaveEpo(noEpo);
		cat.setOrden("cg_campo2");
		infoRegresar = cat.getJSONElementos();

}else if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion

	int start = 0;
	int limit = 0;

	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	String noEstatusDocto = (request.getParameter("no_estatus_docto") == null) ? "" : request.getParameter("no_estatus_docto");
	boolean ocultarDoctosAplicados = (
			noEstatusDocto.equals("")  ||
			noEstatusDocto.equals("1") ||
			noEstatusDocto.equals("2") ||
			noEstatusDocto.equals("5") ||
			noEstatusDocto.equals("6") ||
			noEstatusDocto.equals("7") ||
			noEstatusDocto.equals("9") ||
			noEstatusDocto.equals("10")||
			noEstatusDocto.equals("21")||
			noEstatusDocto.equals("23")||
			noEstatusDocto.equals("28")||
			noEstatusDocto.equals("29")||
			noEstatusDocto.equals("30")||
			noEstatusDocto.equals("31") )?true:false;
	String nombreTipoFactoraje                         = "";
	String icDocumento                                 = "";
	String numeroDocumento                             = "";
	String esNotaDeCreditoSimpleAplicada               = "";
	String esDocumentoConNotaDeCreditoSimpleAplicada   = "";
	String esNotaDeCreditoAplicada                     = "";
	String esDocumentoConNotaDeCreditoMultipleAplicada = "";
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new com.netro.descuento.ConsDoctosPymeDE());
	
	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			session.setAttribute("aforoPantalla",String.valueOf(dblPorciento));
			session.setAttribute("aforoPantallaDL",String.valueOf(dblPorcientoDL));
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		Registros reg  = new Registros();
		reg = queryHelper.getPageResultSet(request,start,limit);
		int cont = 0;
		while(reg.next())	{
			if (cont >= 1){
				nombreTipoFactoraje = (reg.getString("CS_DSCTO_ESPECIAL")==null)?"":reg.getString("CS_DSCTO_ESPECIAL");
				icDocumento = reg.getString(14);
				if(!ocultarDoctosAplicados){
					esNotaDeCreditoSimpleAplicada						= ( nombreTipoFactoraje.equals("C") && (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("11")) && BeanParamDscto.existeDocumentoAsociado(icDocumento) )?"S":"N";
					esDocumentoConNotaDeCreditoSimpleAplicada		= ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(icDocumento))?"S":"N";
		
					esNotaDeCreditoAplicada 							= (nombreTipoFactoraje.equals("C") && (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(icDocumento))?"S":"N";
					esDocumentoConNotaDeCreditoMultipleAplicada	= (!nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(icDocumento))?"S":"N";
		
					if(esNotaDeCreditoSimpleAplicada.equals("S")){
						if (icDocumento != null && !icDocumento.equals("")){numeroDocumento	=	BeanSeleccionDocumento.getNumeroDoctoAsociado(icDocumento);}
					}else if(esDocumentoConNotaDeCreditoSimpleAplicada.equals("S")){
						if (icDocumento != null && !icDocumento.equals("")){numeroDocumento	=	BeanSeleccionDocumento.getNumeroNotaCreditoAsociada(icDocumento);}
					}
				}
				/*
				*	Se llenan las columnas que se agregaron a la clase ConsDoctosPymeDE()
				*/
				reg.setObject("NOTA_SIMPLE",esNotaDeCreditoSimpleAplicada);
				reg.setObject("NOTA_SIMPLE_DOCTO",esDocumentoConNotaDeCreditoSimpleAplicada);
				reg.setObject("NOTA_MULTIPLE",esNotaDeCreditoAplicada);
				reg.setObject("NOTA_MULTIPLE_DOCTO",esDocumentoConNotaDeCreditoMultipleAplicada);
				reg.setObject("NUMERO_DOCTO",numeroDocumento);
				esNotaDeCreditoSimpleAplicada="";
				esDocumentoConNotaDeCreditoSimpleAplicada="";
				esNotaDeCreditoAplicada="";
				esDocumentoConNotaDeCreditoMultipleAplicada="";
				numeroDocumento = "";
			}
		cont++;
		}
		infoRegresar	=	"{\"success\": true, \"total\": \"" + queryHelper.getResultSetSize(request) + "\", \"registros\": " + reg.getJSONData()+"}";

	} catch(Exception e) {	
		throw new AppException("Error en la paginacion", e);
	}
} else if (informacion.equals("ResumenTotales")) {		//Datos para el Resumen de Totales
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion

} else if (informacion.equals("Parametrizacion")) {	//	*-*-*-*-*-*--*-*-*-*-*-*-*-*	Parametrizacion	*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
	Registros regCamposAdicionales = null;
	Registros regCamposDetalle = null;
	Registros nombresAdd = null;
	int totalcd=0;
	String banderaTablaDoctos 	=	"";
	String sFechaVencPyme		=	""; 
	boolean mostrarOpcionPignorado=false;
	Registros regPorcentaje = null;

	try {
		if (noEpo.equals("")) {
			throw new Exception("Los parametros son requeridos");
		}
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	alParamEPO = new Hashtable();
	boolean bOperaFactorajeVencido = false;
	boolean bOperaFactorajeIF = false;
	boolean bOperaFactVencimientoInfonavit = false;
	boolean bOperaFactorajeDistribuido = false;
	boolean bTipoFactoraje = false;
	boolean bOperaFactConMandato = false;
	boolean bOperaNotasDeCredito = false;
	
	
	if(!iNoEPO.equals("")) {
		alParamEPO = BeanParamDscto.getParametrosEPO(iNoEPO, 1);
		if (alParamEPO!=null) {
			bOperaFactConMandato   		  = ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
			bOperaFactorajeVencido 		  = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
			bOperaFactorajeIF = ("N".equals(alParamEPO.get("FACTORAJE_IF").toString())) ? false : true ;
			bOperaFactorajeDistribuido   = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
			bOperaNotasDeCredito 		  = ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
			bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString())) ? false : true ;
			
		}
	}
	bTipoFactoraje = (bOperaFactConMandato || bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaNotasDeCredito || bOperaFactVencimientoInfonavit ||bOperaFactorajeIF)?true:false;

	if(!noEpo.equals("")){
		mostrarOpcionPignorado = BeanParamDscto.mostrarOpcionPignorado(noEpo, iNoCliente);
		sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(noEpo);
		
		if(BeanSeleccionDocumento.bgetExisteParamDocs(noEpo)){
			banderaTablaDoctos = "1";
		}
		totalcd = BeanSeleccionDocumento.getTotalCamposAdicionales(iNoCliente);
		if(totalcd >=1) {
			regCamposAdicionales = new Registros();
			regCamposAdicionales = BeanParamDscto.getCamposAdicionales(iNoCliente);
		}
		regCamposDetalle = new Registros();
		regCamposDetalle = BeanParamDscto.getCamposAdicionalesDetalle(noEpo);
	}

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bOperaFactorajeIF", new Boolean(bOperaFactorajeIF));
	jsonObj.put("bOperaNotasDeCredito", new Boolean(bOperaNotasDeCredito));
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
	jsonObj.put("mostrarOpcionPignorado", new Boolean(mostrarOpcionPignorado));
	jsonObj.put("banderaTablaDoctos", banderaTablaDoctos);
	jsonObj.put("sFechaVencPyme", sFechaVencPyme);
	jsonObj.put("dblPorciento", String.valueOf(dblPorciento));
	jsonObj.put("dblPorcientoDL", String.valueOf(dblPorcientoDL));
	if (regCamposAdicionales != null){
		jsonObj.put("camp_dina1", regCamposAdicionales.getJSONData() );
	}
	if (regCamposDetalle != null){
		jsonObj.put("camp_dina2", regCamposDetalle.getJSONData() );
	}	
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("obtenDetalles")) {
	Registros regDatos = new Registros();
	Registros regDatosDetalle = new Registros();
	String ic_documento = (request.getParameter("ic_docto")==null)?"":request.getParameter("ic_docto");
	
	if(!noEpo.equals("") && !ic_documento.equals("") ){
		regDatos = BeanParamDscto.getDatosDocumentos(noEpo,ic_documento);
		regDatosDetalle = BeanParamDscto.getDatosDocumentosDetalle(ic_documento);
		
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		if (regDatos != null){
			jsonObj.put("datos_doctos", regDatos.getJSONData() );
		}
		if (regDatosDetalle != null){
			jsonObj.put("datos_detalle", regDatosDetalle.getJSONData() );
		}
		
		infoRegresar = jsonObj.toString();
		
	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
} else if (informacion.equals("obtenAcuse")) {
	Registros regDatosAcuse = new Registros();
	Registros regDatosAcuse2 = new Registros();
	Registros regDatosAcuseTotal = new Registros();
	String cc_acuse = (request.getParameter("ccAcuse")==null)?"":request.getParameter("ccAcuse");
	
	if(!cc_acuse.equals("")){
		regDatosAcuse = BeanParamDscto.getDatosAcuse(cc_acuse);
		regDatosAcuse2 = BeanParamDscto.getDatosAcuse2(cc_acuse);
		regDatosAcuseTotal = BeanParamDscto.getDatosTotalesAcuse(cc_acuse);

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		if (regDatosAcuse != null){
			jsonObj.put("datosAcuse", regDatosAcuse.getJSONData() );
		}
		if (regDatosAcuse2 != null){
			jsonObj.put("datosAcuse2", regDatosAcuse2.getJSONData() );
		}
		if (regDatosAcuseTotal != null){
			jsonObj.put("datosAcuseTotal", regDatosAcuseTotal.getJSONData() );
		}

		infoRegresar = jsonObj.toString();

	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
} else if (informacion.equals("obtenAplica")) {

	String claveDocumento = (request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
	if(!claveDocumento.equals("")){
		List nombreColumnas = new ArrayList();
		List dataColumnas = new ArrayList();
		nombreColumnas.add("IG_NUMERO_DOCTO");
		nombreColumnas.add("IN_IMPORTE_RECIBIR");
		nombreColumnas.add("IC_NOMBRE");
		nombreColumnas.add("NOMBRE_IF");
		nombreColumnas.add("DF_FECHA_SELECCION");
		nombreColumnas.add("DF_OPERACION");
		nombreColumnas.add("IC_MENSUALIDAD");
		nombreColumnas.add("NUMERO_CREDITO");
		nombreColumnas.add("CG_TIPO_CONTRATO");
		nombreColumnas.add("IG_NUMERO_PRESTAMO");
		nombreColumnas.add("IMPORTE_APLICADO_CREDITO");
		nombreColumnas.add("SALDO_CREDITO");
		nombreColumnas.add("IC_MONEDA");
		nombreColumnas.add("ORDEN_APLICA");

		Vector vDetalleDocumentoAplicado = BeanSeleccionDocumento.ovgetDetalleDocumentoAplicado(iNoCliente, claveDocumento);
		Registros  regAplica = new Registros();
		regAplica.setNombreColumnas(nombreColumnas);
		regAplica.setData(vDetalleDocumentoAplicado);

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		if (regAplica != null){
			jsonObj.put("datosAplica", regAplica.getJSONData() );
		}
		infoRegresar = jsonObj.toString();

	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
}else if (informacion.equals("obtenModifica")) {

	String sFechaVencPyme = (request.getParameter("sFechaVencPyme")==null)?"":request.getParameter("sFechaVencPyme");
	String claveDocumento = (request.getParameter("ic_docto")==null)?"":request.getParameter("ic_docto");

	if(!claveDocumento.equals("")){
		Registros regModifica = new Registros();
		regModifica = BeanParamDscto.getModificacionMontos(sFechaVencPyme, claveDocumento);
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("success", new Boolean(true));
		if (regModifica != null){
			jsonObj.put("datosModifica", regModifica.getJSONData() );
		}
		infoRegresar = jsonObj.toString();

	}else	{
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}	

} else if(informacion.equals("Genera_PDF")){

	String noIf                 = (request.getParameter("no_if")                == null) ? "" : request.getParameter("no_if");
	String noDocto              = (request.getParameter("no_docto")             == null) ? "" : request.getParameter("no_docto");
	String dfFechaDoctode       = (request.getParameter("df_fecha_docto_de")    == null) ? "" : request.getParameter("df_fecha_docto_de");
	String dfFechaDoctoa        = (request.getParameter("df_fecha_docto_a")     == null) ? "" : request.getParameter("df_fecha_docto_a");
	String campDina1            = (request.getParameter("camp_dina1")           == null) ? "" : request.getParameter("camp_dina1");
	String campDina2            = (request.getParameter("camp_dina2")           == null) ? "" : request.getParameter("camp_dina2");
	String ordIf                = (request.getParameter("ord_if")               == null) ? "" : request.getParameter("ord_if");
	String ordNoDocto           = (request.getParameter("ord_no_docto")         == null) ? "" : request.getParameter("ord_no_docto");
	String ordEpo               = (request.getParameter("ord_epo")              == null) ? "" : request.getParameter("ord_epo");
	String ordFecVenc           = (request.getParameter("ord_fec_venc")         == null) ? "" : request.getParameter("ord_fec_venc");
	String dfFechaVencde        = (request.getParameter("df_fecha_venc_de")     == null) ? "" : request.getParameter("df_fecha_venc_de");
	String dfFechaVenca         = (request.getParameter("df_fecha_venc_a")      == null) ? "" : request.getParameter("df_fecha_venc_a");
	String noMoneda             = (request.getParameter("no_moneda")            == null) ? "" : request.getParameter("no_moneda");
	String montoDe              = (request.getParameter("monto_de")             == null) ? "" : request.getParameter("monto_de");
	String montoA               = (request.getParameter("monto_a")              == null) ? "" : request.getParameter("monto_a");
	String noEstatusDocto       = (request.getParameter("no_estatus_docto")     == null) ? "" : request.getParameter("no_estatus_docto");
	String fechaIfde            = (request.getParameter("fecha_if_de")          == null) ? "" : request.getParameter("fecha_if_de");
	String fechaIfa             = (request.getParameter("fecha_if_a")           == null) ? "" : request.getParameter("fecha_if_a");
	String tasade               = (request.getParameter("tasa_de")              == null) ? "" : request.getParameter("tasa_de");
	String tasaa                = (request.getParameter("tasa_a")               == null) ? "" : request.getParameter("tasa_a");
	String tipoFactoraje        = (request.getParameter("tipoFactoraje")        == null) ? "" : request.getParameter("tipoFactoraje");
	String sFechaVencPyme       = (request.getParameter("sFechaVencPyme")       == null) ? "" : request.getParameter("sFechaVencPyme");
	String sOperaFactConMandato = (request.getParameter("sOperaFactConMandato") == null) ? "" : request.getParameter("sOperaFactConMandato");
	String mandant              = (request.getParameter("mandant")              == null) ? "" : request.getParameter("mandant");
	String sOperaFactVencimientoInfonavit = (request.getParameter("sOperaFactVencimientoInfonavit") == null) ? "" : request.getParameter("sOperaFactVencimientoInfonavit");
	String idiomaUsuario        = (request.getSession().getAttribute("sesIdiomaUsuario") == null ) ? "ES" : (String) request.getSession().getAttribute("sesIdiomaUsuario");
	String ic_epo_aserca        = (String)session.getAttribute("sesEpoAserca");

	JSONObject jsonObj = new JSONObject();
	ConsDoctosPymeDE paginador  = new ConsDoctosPymeDE();
	paginador.setNoEpo(noEpo);
	paginador.setNoIf(noIf);
	paginador.setNoDocto(noDocto);
	paginador.setDfFechaDoctode(dfFechaDoctode);
	paginador.setDfFechaDoctoa(dfFechaDoctoa);
	paginador.setCampDina1(campDina1);
	paginador.setCampDina2(campDina2);
	paginador.setOrdIf(ordIf);
	paginador.setOrdNoDocto(ordNoDocto);
	paginador.setOrdEpo(ordEpo);
	paginador.setOrdFecVenc(ordFecVenc);
	paginador.setDfFechaVencde(dfFechaVencde);
	paginador.setDfFechaVenca(dfFechaVenca);
	paginador.setNoMoneda(noMoneda);
	paginador.setMontoDe(montoDe);
	paginador.setMontoA(montoA);
	paginador.setNoEstatusDocto(noEstatusDocto);
	paginador.setFechaIfde(fechaIfde);
	paginador.setFechaIfa(fechaIfa);
	paginador.setTasade(tasade);
	paginador.setTasaa(tasaa);
	paginador.setTipoFactoraje(tipoFactoraje);
	paginador.setINoCliente(iNoCliente);
	paginador.setSFechaVencPyme(sFechaVencPyme);
	paginador.setSOperaFactConMandato(sOperaFactConMandato);
	paginador.setMandant(mandant);
	paginador.setIdiomaUsuario(idiomaUsuario);
	paginador.setSOperaFactVencimientoInfonavit(sOperaFactVencimientoInfonavit);
	paginador.setINoEPO(iNoEPO);
	paginador.setIc_epo_aserc(ic_epo_aserca);
	CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);

	try {
		String nombreArchivo = queryHelper.getCreateCustomFile(request, strDirectorioTemp, "PDF");
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	} catch(Throwable e) {
		jsonObj.put("success", new Boolean(false));
		throw new AppException("Error al generar el archivo PDF", e);
	}
	infoRegresar = jsonObj.toString();

}
%>
<%=infoRegresar%>