<%@ page contentType="application/json;charset=UTF-8"
import="java.util.*,
	com.netro.descuento.*, netropology.utilerias.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession.jspf" %>
<%
JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
try {
	if(iNoEPO != null && !iNoEPO.equals("")) {
		ISeleccionDocumento beanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
		String sOperaFactConMandato = (request.getParameter("sOperaFactConMandato") == null) ? "" : request.getParameter("sOperaFactConMandato");
		//String tipoFactoraje = (request.getParameter("tipoFactoraje") ==null) ? "" : request.getParameter("tipoFactoraje");
		String mandant = (request.getParameter("mandant") == null) ? "" : request.getParameter("mandant");
		boolean bOperaFactConMandato					= false;
		boolean bOperaFactorajeVencido				= false;
		boolean bOperaFactorajeDistribuido			= false;
		boolean bOperaFactorajeNotaCredito			= false;
		boolean bOperaFactVencimientoInfonavit		= false;
		boolean bTipoFactoraje							= false;
		boolean bOperaFactorajeIF						= false;
		sOperaFactConMandato								= new String("");
		String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");
		sFechaVencPyme = beanSeleccionDocumento.operaFechaVencPyme(iNoEPO);
		ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		Hashtable alParamEPO = new Hashtable();
		alParamEPO = BeanParamDscto.getParametrosEPO(iNoEPO, 1);
		if (alParamEPO!=null) {
			bOperaFactConMandato				= ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
			bOperaFactorajeVencido			= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
			bOperaFactorajeDistribuido		= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
			bOperaFactorajeNotaCredito		= ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
			bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString())) ? false : true ;
			bOperaFactorajeIF= ("N".equals(alParamEPO.get("FACTORAJE_IF").toString())) ? false : true ;
		}
		bTipoFactoraje = ( bOperaFactorajeIF || bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaCredito || bOperaFactConMandato || bOperaFactVencimientoInfonavit)?true:false;
		sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
		bOperaFactorajeVencido = true;
		int ic_moneda = 0;
		double fn_monto = 0;
	   double dblPorcentaje = 0;
		double dblMontoDescuento = 0;
		String cg_razon_social = "";
		String ig_numero_docto = "";
		String cc_acuse = "";
		String df_fecha_docto = "";
		String df_fecha_venc = "";
		String cd_nombre = "";

		String tipoFactoraje = "", tipoFactorajeDesc = "", nombreIf = "", netoRecibirPyme = "", nombreBeneficiario = "", porcBeneficiario = "", recibirBeneficiario = "";;

		String cveEstatus = (request.getParameter("claveStatus")!=null)?request.getParameter("claveStatus"):"";
		ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
		
		pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formasMini",ComunesPDF.RIGHT);

		if (cveEstatus.equals("1")){		//-----[gridDescuento]
			int rowDscto = 0;
			RepCEstatusPymeDEbean repDscto = new RepCEstatusPymeDEbean();
			repDscto.setIccambioestatusdocto("6");
			repDscto.setFechaVencPyme(sFechaVencPyme);
			repDscto.setOperaFactConMandato(sOperaFactConMandato);
			repDscto.setIcePyme(iNoCliente);
			repDscto.setIceEpo(iNoEPO);
			repDscto.setQrysentencia("");
			Registros rsDescFisico = repDscto.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/
			pdfDoc.addText("Estatus:		Descuento físico","formasB",ComunesPDF.LEFT);
			while (rsDescFisico.next()) {
				if(rowDscto == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Número Documento");
					lEncabezados.add("Fecha Documento");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Moneda");
					if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Número de Acuse");
					if(bOperaFactorajeVencido || bOperaFactorajeIF ) {
						lEncabezados.add("Nombre IF");
					}
					if(bOperaFactorajeDistribuido  || bOperaFactVencimientoInfonavit) {
						lEncabezados.add("Beneficiario");
						lEncabezados.add("% Beneficiario");
						lEncabezados.add("Importe a Recibir Beneficiario");
					}
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				cg_razon_social = rsDescFisico.getString(1).trim();
				ig_numero_docto = rsDescFisico.getString(2).trim();
				df_fecha_docto = rsDescFisico.getString(3).trim();
				df_fecha_venc = (rsDescFisico.getString(4)==null)?"":rsDescFisico.getString(4).trim();
				cd_nombre = rsDescFisico.getString(5).trim();
				fn_monto = Double.parseDouble(rsDescFisico.getString(6));
				cc_acuse = rsDescFisico.getString(7).trim();
				ic_moneda = Integer.parseInt(rsDescFisico.getString(8));
				nombreBeneficiario = (rsDescFisico.getString("NOMBRE_BENEFICIARIO")==null)?"":rsDescFisico.getString("NOMBRE_BENEFICIARIO").trim();
				porcBeneficiario = (rsDescFisico.getString("FN_PORC_BENEFICIARIO")==null)?"":rsDescFisico.getString("FN_PORC_BENEFICIARIO").trim();
				recibirBeneficiario = (rsDescFisico.getString("RECIBIR_BENEFICIARIO")==null)?"":rsDescFisico.getString("RECIBIR_BENEFICIARIO").trim();
				String smandant = "";
				if(bOperaFactConMandato) {
					smandant = (rsDescFisico.getString("mandante")==null)?"":rsDescFisico.getString("mandante").trim();
				}
				if(ic_moneda==1){
					dblPorcentaje = new Double(strAforo).doubleValue() * 100;
				}else if(ic_moneda==54){
					dblPorcentaje = new Double(strAforoDL).doubleValue() * 100;
				}
				if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { // Para Factoraje Vencido o Distribuido o Venc Infonavit
				  tipoFactoraje = (rsDescFisico.getString("CS_DSCTO_ESPECIAL")==null)?"":rsDescFisico.getString("CS_DSCTO_ESPECIAL");
				  nombreIf = "";
				  if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I"))  {
					  nombreIf = (rsDescFisico.getString("NOMBREIF")==null)?"":rsDescFisico.getString("NOMBREIF").trim();
					  dblMontoDescuento = fn_monto;
					  dblPorcentaje = 100;
				  }
				}		
				if(tipoFactoraje.equals("A"))  {
					  nombreIf = (rsDescFisico.getString("NOMBREIF")==null)?"":rsDescFisico.getString("NOMBREIF").trim();
				}
				
				tipoFactorajeDesc = (rsDescFisico.getString("TIPO_FACTORAJE")==null)?"":rsDescFisico.getString("TIPO_FACTORAJE").trim();
				dblMontoDescuento = fn_monto * (dblPorcentaje / 100);
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorcentaje,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido || bOperaFactorajeIF) {
					pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
					pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2) + "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowDscto++;
			} //fin del while
			if (rowDscto == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("2")){		////-----[gridBaja]
			int rowBaja = 0;
			RepCEstatusPymeDEbean repBaja = new RepCEstatusPymeDEbean();
			repBaja.setIccambioestatusdocto("5");
			repBaja.setFechaVencPyme(sFechaVencPyme);
			repBaja.setOperaFactConMandato(sOperaFactConMandato);
			repBaja.setIcePyme(iNoCliente);
			repBaja.setIceEpo(iNoEPO);
			repBaja.setQrysentencia("");
			Registros regBaja = repBaja.executeQuery();
			/***************************/	/* Generacion del archivo *//***************************/
			pdfDoc.addText("Estatus:		Baja","formasB",ComunesPDF.LEFT);
			while (regBaja.next()) {
				if(rowBaja == 0){
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Número Documento");
					lEncabezados.add("Fecha Documento");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Moneda");
					 if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF ) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Número de Acuse");
					if(bOperaFactorajeVencido || bOperaFactorajeIF) {
						lEncabezados.add("Nombre IF");
					}
					if(bOperaFactorajeDistribuido  || bOperaFactVencimientoInfonavit) {
						lEncabezados.add("Beneficiario");
						lEncabezados.add("% Beneficiario");
						lEncabezados.add("Importe a Recibir Beneficiario");
					}
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				cg_razon_social = regBaja.getString(1).trim();
				ig_numero_docto = regBaja.getString(2).trim();
				df_fecha_docto = regBaja.getString(3).trim();
				df_fecha_venc = (regBaja.getString(4)==null)?"":regBaja.getString(4).trim();
				cd_nombre = regBaja.getString(5).trim();
				fn_monto = Double.parseDouble(regBaja.getString(6));
				cc_acuse = regBaja.getString(7).trim();
				ic_moneda = Integer.parseInt(regBaja.getString(8));
				nombreBeneficiario = (regBaja.getString("NOMBRE_BENEFICIARIO")==null)?"":regBaja.getString("NOMBRE_BENEFICIARIO").trim();
				porcBeneficiario = (regBaja.getString("FN_PORC_BENEFICIARIO")==null)?"":regBaja.getString("FN_PORC_BENEFICIARIO").trim();
				recibirBeneficiario = (regBaja.getString("RECIBIR_BENEFICIARIO")==null)?"":regBaja.getString("RECIBIR_BENEFICIARIO").trim();
				String smandant = "";
				if(bOperaFactConMandato) {
					smandant = (regBaja.getString("mandante")==null)?"":regBaja.getString("mandante").trim();
				}
				if(ic_moneda==1){
					dblPorcentaje = new Double(strAforo).doubleValue() * 100;
				}else if(ic_moneda==54){
					dblPorcentaje = new Double(strAforoDL).doubleValue() * 100;
				}
				if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { // Para Factoraje Vencido o Distribuido o Venc Infonavit
				  tipoFactoraje = (regBaja.getString("CS_DSCTO_ESPECIAL")==null)?"":regBaja.getString("CS_DSCTO_ESPECIAL");
				  nombreIf = "";
				  if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I"))  {
					  nombreIf = (regBaja.getString("NOMBREIF")==null)?"":regBaja.getString("NOMBREIF").trim();
					  dblMontoDescuento = fn_monto;
					  dblPorcentaje = 100;
				  }
				  }
				  if(tipoFactoraje.equals("A") )  {
					  nombreIf = (regBaja.getString("NOMBREIF")==null)?"":regBaja.getString("NOMBREIF").trim();					  
					}
				tipoFactorajeDesc = (regBaja.getString("TIPO_FACTORAJE")==null)?"":regBaja.getString("TIPO_FACTORAJE").trim();
				dblMontoDescuento = fn_monto * (dblPorcentaje / 100);
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF ) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorcentaje,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF ) {
					pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
					pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2) + "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowBaja++;
			} //fin del while
			if (rowBaja == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formas",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("3")){		////-----[gridAnticipa]
			RepCEstatusPymeDEbean repPanticipado = new RepCEstatusPymeDEbean();
			repPanticipado.setIccambioestatusdocto("7");
			repPanticipado.setFechaVencPyme(sFechaVencPyme);
			repPanticipado.setOperaFactConMandato(sOperaFactConMandato);
			repPanticipado.setIcePyme(iNoCliente);
			repPanticipado.setIceEpo(iNoEPO);
			repPanticipado.setQrysentencia("");
			Registros regAnticipa = repPanticipado.executeQuery();
			int rowAnticipa = 0;
			/***************************/	/* Generacion del archivo */	/***************************/			
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Estatus:		Pagado Anticipado","formasB",ComunesPDF.LEFT);
			while (regAnticipa.next()) {
				if(rowAnticipa == 0)	{
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Número Documento");
					lEncabezados.add("Fecha Documento");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Moneda");
					 if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF ) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Número de Acuse");
					if(bOperaFactorajeVencido || bOperaFactorajeIF) {
						lEncabezados.add("Nombre IF");
					}
					if(bOperaFactorajeDistribuido  || bOperaFactVencimientoInfonavit) {
						lEncabezados.add("Beneficiario");
						lEncabezados.add("% Beneficiario");
						lEncabezados.add("Importe a Recibir Beneficiario");
					}
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				cg_razon_social = regAnticipa.getString(1);
				ig_numero_docto = regAnticipa.getString(2);
				df_fecha_docto = regAnticipa.getString(3);
				df_fecha_venc = (regAnticipa.getString(4)==null)?"":regAnticipa.getString(4);
				cd_nombre = regAnticipa.getString(5);
				fn_monto = Double.parseDouble(regAnticipa.getString(6));
				cc_acuse = regAnticipa.getString(7);
				ic_moneda = Integer.parseInt(regAnticipa.getString(8));
				nombreBeneficiario = (regAnticipa.getString("NOMBRE_BENEFICIARIO")==null)?"":regAnticipa.getString("NOMBRE_BENEFICIARIO");
				porcBeneficiario = (regAnticipa.getString("FN_PORC_BENEFICIARIO")==null)?"":regAnticipa.getString("FN_PORC_BENEFICIARIO");
				recibirBeneficiario = (regAnticipa.getString("RECIBIR_BENEFICIARIO")==null)?"":regAnticipa.getString("RECIBIR_BENEFICIARIO");
				String smandant = "";
				if(bOperaFactConMandato) {
					smandant = (regAnticipa.getString("mandante")==null)?"":regAnticipa.getString("mandante");
				}
				if(ic_moneda==1){
					dblPorcentaje = new Double(strAforo).doubleValue() * 100;
				}else if(ic_moneda==54){
					dblPorcentaje = new Double(strAforoDL).doubleValue() * 100;
				}
				if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { // Para Factoraje Vencido o Distribuido o Venc Infonavit
				  tipoFactoraje = (regAnticipa.getString("CS_DSCTO_ESPECIAL")==null)?"":regAnticipa.getString("CS_DSCTO_ESPECIAL");
				  nombreIf = "";
				  if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I"))  {
					  nombreIf = (regAnticipa.getString("NOMBREIF")==null)?"":regAnticipa.getString("NOMBREIF").trim();
					  dblMontoDescuento = fn_monto;
					  dblPorcentaje = 100;
				  }
				}
				 if(tipoFactoraje.equals("A"))  {
					  nombreIf = (regAnticipa.getString("NOMBREIF")==null)?"":regAnticipa.getString("NOMBREIF").trim();
				}
				tipoFactorajeDesc = (regAnticipa.getString("TIPO_FACTORAJE")==null)?"":regAnticipa.getString("TIPO_FACTORAJE");
				dblMontoDescuento = fn_monto * (dblPorcentaje / 100);
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorcentaje,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
					pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
					pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2) + "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowAnticipa++;
			}//fin del while()
			if (rowAnticipa == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("4")){		////-----[gridPymeNego]
			RepCEstatusPymeDEbean repPymeNego = new RepCEstatusPymeDEbean();
			repPymeNego.setIccambioestatusdocto("2");
			repPymeNego.setFechaVencPyme(sFechaVencPyme);
			repPymeNego.setOperaFactConMandato(sOperaFactConMandato);
			repPymeNego.setIcePyme(iNoCliente);
			repPymeNego.setIceEpo(iNoEPO);
			repPymeNego.setQrysentencia("");
			Registros regPymeNego = repPymeNego.executeQuery();
			int rowPymeNego = 0;
			/***************************/	/* Generacion del archivo */	/***************************/			
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Estatus:		Seleccionada PYME a Negociable","formasB",ComunesPDF.LEFT);
			while (regPymeNego.next()) {
				if(rowPymeNego == 0)	{
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Número Documento");
					lEncabezados.add("Fecha Documento");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Moneda");
					 if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Número de Acuse");
					if(bOperaFactorajeVencido || bOperaFactorajeIF) {
						lEncabezados.add("Nombre IF");
					}
					if(bOperaFactorajeDistribuido  || bOperaFactVencimientoInfonavit) {
						lEncabezados.add("Neto a Recibir Pyme");
						lEncabezados.add("Beneficiario");
						lEncabezados.add("% Beneficiario");
						lEncabezados.add("Importe a Recibir Beneficiario");
					}
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				cg_razon_social = regPymeNego.getString(1).trim();
				ig_numero_docto = regPymeNego.getString(2).trim();
				df_fecha_docto = regPymeNego.getString(3).trim();
				df_fecha_venc = (regPymeNego.getString(4)==null)?"":regPymeNego.getString(4).trim();
				cd_nombre = regPymeNego.getString(5).trim();
				fn_monto = Double.parseDouble(regPymeNego.getString(6));
				cc_acuse = regPymeNego.getString(7).trim();
				ic_moneda = Integer.parseInt(regPymeNego.getString(8));
				netoRecibirPyme = (regPymeNego.getString("NETO_RECIBIR")==null)?"":regPymeNego.getString("NETO_RECIBIR").trim();
				nombreBeneficiario = (regPymeNego.getString("NOMBRE_BENEFICIARIO")==null)?"":regPymeNego.getString("NOMBRE_BENEFICIARIO").trim();
				porcBeneficiario = (regPymeNego.getString("FN_PORC_BENEFICIARIO")==null)?"":regPymeNego.getString("FN_PORC_BENEFICIARIO").trim();
				recibirBeneficiario = (regPymeNego.getString("RECIBIR_BENEFICIARIO")==null)?"":regPymeNego.getString("RECIBIR_BENEFICIARIO").trim();
				String smandant = "";
				if(bOperaFactConMandato) {
					smandant = (regPymeNego.getString("mandante")==null)?"":regPymeNego.getString("mandante").trim();
				}
				if(ic_moneda==1){
					dblPorcentaje = new Double(strAforo).doubleValue() * 100;
				}else if(ic_moneda==54){
					dblPorcentaje = new Double(strAforoDL).doubleValue() * 100;
				}
				if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { // Para Factoraje Vencido o Distribuido o Venc Infonavit
				  tipoFactoraje = (regPymeNego.getString("CS_DSCTO_ESPECIAL")==null)?"":regPymeNego.getString("CS_DSCTO_ESPECIAL");
				  nombreIf = "";
				  if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I"))  {
					  nombreIf = (regPymeNego.getString("NOMBREIF")==null)?"":regPymeNego.getString("NOMBREIF").trim();
					  dblMontoDescuento = fn_monto;
					  dblPorcentaje = 100;
				  }
				}
				if(tipoFactoraje.equals("A"))  {
					  nombreIf = (regPymeNego.getString("NOMBREIF")==null)?"":regPymeNego.getString("NOMBREIF").trim();
				}
				tipoFactorajeDesc = (regPymeNego.getString("TIPO_FACTORAJE")==null)?"":regPymeNego.getString("TIPO_FACTORAJE").trim();
				dblMontoDescuento = fn_monto * (dblPorcentaje / 100);
			/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorcentaje,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
					pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
					pdfDoc.setCell("$" + Comunes.formatoDecimal(netoRecibirPyme,2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2) + "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowPymeNego++;
			}//fin del while()
			if (rowPymeNego == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("5"))	{			////-----[gridIgnoraPyme]
			int rowIgnoraPyme = 0;
			/***************************/	/* Generacion del archivo */	/***************************/			
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Estatus:		Pignorado a Seleccionada PYME","formasB",ComunesPDF.LEFT);
			if (rowIgnoraPyme == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("6"))	{			//-----[gridIgnoraNego]
			Registros regIgnoraNego = new Registros();
			int rowIgnoraNego = 0;
			boolean mostrarOpcionPignorado=false;
			String infor = "";
			mostrarOpcionPignorado = BeanParamDscto.mostrarOpcionPignorado(iNoEPO, iNoCliente);
			if (mostrarOpcionPignorado)	{
				RepCEstatusPymeDEbean repIgnoraNego = new RepCEstatusPymeDEbean();
				repIgnoraNego.setIccambioestatusdocto("26");
				repIgnoraNego.setFechaVencPyme(sFechaVencPyme);
				repIgnoraNego.setIcePyme(iNoCliente);
				repIgnoraNego.setIceEpo(iNoEPO);
				repIgnoraNego.setQrysentencia("");
				regIgnoraNego = repIgnoraNego.executeQuery();
	/***************************/	/* Generacion del archivo */	/***************************/
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Estatus:		Pignorado a Negociable","formasB",ComunesPDF.LEFT);
				while (regIgnoraNego.next()) {
					if(rowIgnoraNego == 0)	{
						List lEncabezados = new ArrayList();
						lEncabezados.add("Nombre EPO");
						lEncabezados.add("Número Documento");
						lEncabezados.add("Fecha Documento");
						lEncabezados.add("Fecha Vencimiento");
						lEncabezados.add("Moneda");
						lEncabezados.add("Monto");
						lEncabezados.add("Porcentaje de Descuento");
						lEncabezados.add("Monto a Descontar");
						pdfDoc.setTable(lEncabezados,"formasmenB", 100);
					}
					cg_razon_social = regIgnoraNego.getString(1).trim();
					ig_numero_docto = regIgnoraNego.getString(2).trim();
					df_fecha_docto = regIgnoraNego.getString(3).trim();
					df_fecha_venc = (regIgnoraNego.getString(4)==null)?"":regIgnoraNego.getString(4).trim();
					cd_nombre = regIgnoraNego.getString(5).trim();
					fn_monto = Double.parseDouble(regIgnoraNego.getString(6));
					cc_acuse = regIgnoraNego.getString(7).trim();
					ic_moneda = Integer.parseInt(regIgnoraNego.getString(8));
					if(ic_moneda==1){
						dblPorcentaje = new Double(strAforo).doubleValue() * 100;
					}else if(ic_moneda==54){
						dblPorcentaje = new Double(strAforoDL).doubleValue() * 100;
					}
					if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { // Para Factoraje Vencido o Distribuido o Venc Infonavit
					  tipoFactoraje = (regIgnoraNego.getString("CS_DSCTO_ESPECIAL")==null)?"":regIgnoraNego.getString("CS_DSCTO_ESPECIAL");
					  nombreIf = "";
					  if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I"))  {
						  nombreIf = (regIgnoraNego.getString("NOMBREIF")==null)?"":regIgnoraNego.getString("NOMBREIF").trim();
						  dblMontoDescuento = fn_monto;
						  dblPorcentaje = 100;
					  }
					}
					if(tipoFactoraje.equals("A"))  {
						  nombreIf = (regIgnoraNego.getString("NOMBREIF")==null)?"":regIgnoraNego.getString("NOMBREIF").trim();
					}
					tipoFactorajeDesc = (regIgnoraNego.getString("TIPO_FACTORAJE")==null)?"":regIgnoraNego.getString("TIPO_FACTORAJE").trim();
					dblMontoDescuento = fn_monto * (dblPorcentaje / 100);
	/*************************/ /* Contenido del archivo */ /**************************/
					pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(dblPorcentaje,0)+ "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
					rowIgnoraNego++;
				}//fin de while()
				if (rowIgnoraNego == 0)	{
					pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
					pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
				}else	{
					pdfDoc.addTable();
				}
			}
		}else if (cveEstatus.equals("7"))	{				//-----[gridModifica]
			RepCEstatusPymeDEbean repModifica = new RepCEstatusPymeDEbean();
			repModifica.setIccambioestatusdocto("8");
			repModifica.setFechaVencPyme(sFechaVencPyme);
			repModifica.setOperaFactConMandato(sOperaFactConMandato);
			repModifica.setIcePyme(iNoCliente);
			repModifica.setIceEpo(iNoEPO);	
			repModifica.setQrysentencia("");
			Registros regModifica = repModifica.executeQuery();
			int rowModifica = 0;
			/***************************/	/* Generacion del archivo */	/***************************/			
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Estatus:		Modificación de montos y/o Fechas de Vencimiento","formasB",ComunesPDF.LEFT);
			while (regModifica.next()) {
				if(rowModifica == 0)	{
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Número Documento");
					lEncabezados.add("Fecha Documento");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Moneda");
					if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto");
					lEncabezados.add("Monto Anterior");
					lEncabezados.add("Fecha de Vencimiento Anterior");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Número de Acuse");
					lEncabezados.add("Causa");
					if(bOperaFactorajeVencido || bOperaFactorajeIF) {
						lEncabezados.add("Nombre IF");
					}
					if(bOperaFactorajeDistribuido  || bOperaFactVencimientoInfonavit) {
						lEncabezados.add("Beneficiario");
						lEncabezados.add("% Beneficiario");
						lEncabezados.add("Importe a Recibir Beneficiario");
					}
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				String MontoAnterior = "", fechaVencAnt = "", causa = "";
				cg_razon_social = regModifica.getString(1);
				ig_numero_docto = regModifica.getString(2);
				df_fecha_docto = regModifica.getString(3);
				df_fecha_venc = (regModifica.getString(4)==null)?"":regModifica.getString(4);
				cd_nombre = regModifica.getString(5);
				fn_monto = Double.parseDouble(regModifica.getString(6));
				cc_acuse = regModifica.getString(7).trim();
				ic_moneda = Integer.parseInt(regModifica.getString(8));
				cc_acuse = (regModifica.getString(7)==null)?"":regModifica.getString(7).trim();
				MontoAnterior = (regModifica.getString(9)==null)?"":regModifica.getString(9).trim();
				nombreBeneficiario = (regModifica.getString("NOMBRE_BENEFICIARIO")==null)?"":regModifica.getString("NOMBRE_BENEFICIARIO");
				porcBeneficiario = (regModifica.getString("FN_PORC_BENEFICIARIO")==null)?"":regModifica.getString("FN_PORC_BENEFICIARIO");
				recibirBeneficiario = (regModifica.getString("RECIBIR_BENEFICIARIO")==null)?"":regModifica.getString("RECIBIR_BENEFICIARIO");
				fechaVencAnt = (regModifica.getString("fechaVencAnt")==null)?"":regModifica.getString("fechaVencAnt");
				causa = cd_nombre = regModifica.getString(10);
				String smandant = "";
				if(bOperaFactConMandato) {
					smandant = (regModifica.getString("mandante")==null)?"":regModifica.getString("mandante");
				}
				if(ic_moneda==1){
					dblPorcentaje = new Double(strAforo).doubleValue() * 100;
				}else if(ic_moneda==54){
					dblPorcentaje = new Double(strAforoDL).doubleValue() * 100;
				}
				if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { // Para Factoraje Vencido o Distribuido o Venc Infonavit
				  tipoFactoraje = (regModifica.getString("CS_DSCTO_ESPECIAL")==null)?"":regModifica.getString("CS_DSCTO_ESPECIAL");
				  nombreIf = "";
				  if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I"))  {
					  nombreIf = (regModifica.getString("NOMBREIF")==null)?"":regModifica.getString("NOMBREIF").trim();
					  dblMontoDescuento = fn_monto;
					  dblPorcentaje = 100;
				  }
				}
				if(tipoFactoraje.equals("A"))  {
					  nombreIf = (regModifica.getString("NOMBREIF")==null)?"":regModifica.getString("NOMBREIF").trim();
				}
				tipoFactorajeDesc = (regModifica.getString("TIPO_FACTORAJE")==null)?"":regModifica.getString("TIPO_FACTORAJE");
				dblMontoDescuento = fn_monto * (dblPorcentaje / 100);
		/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(MontoAnterior,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVencAnt, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorcentaje,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(causa, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido|| bOperaFactorajeIF) {
					pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
					pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2) + "%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowModifica++;
			}//fin del while()
			if (rowModifica == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("8"))	{
			RepCEstatusPymeDEbean repNotaCredito = new RepCEstatusPymeDEbean();
			repNotaCredito.setIccambioestatusdocto("28");
			repNotaCredito.setFechaVencPyme(sFechaVencPyme);
			repNotaCredito.setOperaFactConMandato(sOperaFactConMandato);
			repNotaCredito.setIcePyme(iNoCliente);
			repNotaCredito.setIceEpo(iNoEPO);	
			repNotaCredito.setQrysentencia("");
			Registros regNotaC = repNotaCredito.executeQuery();
			int rowNotaC = 0;
			/***************************/	/* Generacion del archivo */	/***************************/			
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Estatus:		Aplicación de Nota de Crédito","formasB",ComunesPDF.LEFT);
			while (regNotaC.next()) {
				if(rowNotaC == 0)	{
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Número Documento");
					lEncabezados.add("Número de Acuse");
					lEncabezados.add("Fecha Emision");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Tipo Factoraje");
					lEncabezados.add("Moneda");
					lEncabezados.add("Monto Anterior");
					lEncabezados.add("Monto Nuevo");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Causa");
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				double dMontoNuevo = 0, dMontoAnterior = 0;
				String sCausa = "";
				cg_razon_social = (regNotaC.getString(1)==null)?"":regNotaC.getString(1);
				ig_numero_docto = (regNotaC.getString(2)==null)?"":regNotaC.getString(2);
				cc_acuse = (regNotaC.getString("cc_acuse")==null)?"":regNotaC.getString("cc_acuse");
				df_fecha_docto = (regNotaC.getString(3)==null)?"":regNotaC.getString(3);
				df_fecha_venc = (regNotaC.getString(4)==null)?"":regNotaC.getString(4);
				cd_nombre = (regNotaC.getString("cd_nombre")==null)?"":regNotaC.getString("cd_nombre");
				ic_moneda = Integer.parseInt(regNotaC.getString(8));
				dMontoAnterior = Double.parseDouble(regNotaC.getString("fn_monto_anterior"));
				dMontoNuevo = Double.parseDouble(regNotaC.getString("fn_monto_nuevo"));
				sCausa = (regNotaC.getString("ct_cambio_motivo")==null)?"":regNotaC.getString("ct_cambio_motivo");
				tipoFactorajeDesc = (regNotaC.getString("TIPO_FACTORAJE")==null)?"":regNotaC.getString("TIPO_FACTORAJE");
				String smandant = "";
				if("S".equals(sOperaFactConMandato)) {
					smandant = (regNotaC.getString("mandante")==null)?"":regNotaC.getString("mandante");
				}
				if(ic_moneda==1){
					dblPorcentaje = new Double(strAforo).doubleValue() * 100;
				}else if(ic_moneda==54){
					dblPorcentaje = new Double(strAforoDL).doubleValue() * 100;
				}
				dblMontoDescuento = dMontoNuevo * (dblPorcentaje / 100);
				/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(dMontoAnterior,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(dMontoNuevo,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorcentaje,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(sCausa, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowNotaC++;
			}//fin del while()
			if (rowNotaC == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}else if (cveEstatus.equals("9"))	{
			RepCEstatusPymeDEbean repProgram = new RepCEstatusPymeDEbean();
			repProgram.setIccambioestatusdocto("29");
			repProgram.setFechaVencPyme(sFechaVencPyme);
			repProgram.setOperaFactConMandato(sOperaFactConMandato);
			repProgram.setIcePyme(iNoCliente);
			repProgram.setIceEpo(iNoEPO);
			repProgram.setQrysentencia("");
			Registros regProgram = repProgram.executeQuery();
			int rowProgram = 0;
	/***************************/	/* Generacion del archivo */	/***************************/			
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Estatus:		Programado a Negociable","formasB",ComunesPDF.LEFT);
			while (regProgram.next()) {
				if(rowProgram == 0)	{
					List lEncabezados = new ArrayList();
					lEncabezados.add("Nombre EPO");
					lEncabezados.add("Número Documento");
					lEncabezados.add("Fecha Documento");
					lEncabezados.add("Fecha Vencimiento");
					lEncabezados.add("Moneda");
					if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
						lEncabezados.add("Tipo Factoraje");
					}
					lEncabezados.add("Monto");
					lEncabezados.add("Porcentaje de Descuento");
					lEncabezados.add("Monto a Descontar");
					lEncabezados.add("Número de Acuse");
					lEncabezados.add("Causa");
					if(bOperaFactConMandato){
						lEncabezados.add("Mandante");
					}
					pdfDoc.setTable(lEncabezados,"formasmenB", 100);
				}
				String sCausa = "";
				cg_razon_social = regProgram.getString(1).trim();
				ig_numero_docto = regProgram.getString(2).trim();
				df_fecha_docto = regProgram.getString(3).trim();
				df_fecha_venc = (regProgram.getString(4)==null)?"":regProgram.getString(4).trim();
				cd_nombre = regProgram.getString(5).trim();
				fn_monto = Double.parseDouble(regProgram.getString(6));
				cc_acuse = (regProgram.getString(7)==null)?"":regProgram.getString(7).trim();
				ic_moneda = Integer.parseInt(regProgram.getString(8));
				sCausa = (regProgram.getString("ct_cambio_motivo")==null)?"":regProgram.getString("ct_cambio_motivo").trim();
				String smandant = "";
				if(bOperaFactConMandato) {
					smandant = (regProgram.getString("mandante")==null)?"":regProgram.getString("mandante").trim();
				}
				if(ic_moneda==1){
					dblPorcentaje = new Double(strAforo).doubleValue() * 100;
				}else if(ic_moneda==54){
					dblPorcentaje = new Double(strAforoDL).doubleValue() * 100;
				}
				tipoFactorajeDesc = (regProgram.getString("TIPO_FACTORAJE")==null)?"":regProgram.getString("TIPO_FACTORAJE").trim();
				dblMontoDescuento = fn_monto * (dblPorcentaje / 100);
	/*************************/ /* Contenido del archivo */ /**************************/
				pdfDoc.setCell(cg_razon_social, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(ig_numero_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(df_fecha_venc, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cd_nombre, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit || bOperaFactorajeIF) {
					pdfDoc.setCell(tipoFactorajeDesc, "formasmen", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$" + Comunes.formatoDecimal(fn_monto,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(dblPorcentaje,0)+ "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(cc_acuse, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(sCausa, "formasmen", ComunesPDF.CENTER);
				if(bOperaFactConMandato){
					pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
				}
				rowProgram++;
			}//fin del while()
			if (rowProgram == 0)	{
				pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
				pdfDoc.addText("No se Encontro Ningún Registro","formasmen",ComunesPDF.LEFT);
			}else	{
				pdfDoc.addTable();
			}
		}

		pdfDoc.endDocument();
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}
} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	//Termina proceso de imprimir
}
%>
<%=jsonObj%>