<%@ page contentType="application/json;charset=UTF-8"
import="
  java.text.*, 
  java.math.*, 
	javax.naming.*,
	java.util.*,
	java.sql.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"%>
<%@ include file="../13secsession_extjs.jspf" %>
<%@ include file="../../appComun.jspf" %>
<%!public static final boolean SIN_COMAS = false;%>
<%System.out.println("13consulta03extPDF.jsp (E)");%>
<%
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String noEpo = request.getParameter("no_epo") == null?"":(String)request.getParameter("no_epo");
String noPyme = iNoCliente; 
String infoRegresar = "";
String noIfs ="";
String moneda ="1,54";
AccesoDB con = new AccesoDB();
CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer("");
JSONObject jsonObj = new JSONObject();
int numRegistrosIf = 0;
String tasa_aplicar = "";
String valor_tasa = "";
String relmat_tasa = "";
String puntos_tasa = "";
Vector registros;
String nombreEpo = "";
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

try{
	con.conexionDB();
	AutorizacionTasas tasa = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
		

	String Epo = " select cg_razon_social as nombreEpo from comcat_epo  where ic_epo = "+noEpo;
  ResultSet rs = con.queryDB(Epo);
	while(rs.next()){
		nombreEpo  = rs.getString("nombreEpo");
	}
	rs.close();
	
	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
	
	String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String diaActual    = fechaActual.substring(0,2);
	String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
	String anioActual   = fechaActual.substring(6,10);
	String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
	((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
	(String)session.getAttribute("sesExterno"),
	(String) session.getAttribute("strNombre"),
	(String) session.getAttribute("strNombreUsuario"),
	(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	

	pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
	pdfDoc.addText(nombreEpo,"formas",ComunesPDF.CENTER);
	
	String nuevoQuery = " SELECT DISTINCT (pi.ic_if) as ic_if, i.cg_razon_social as cg_razon_social"   +
							"            FROM comrel_pyme_if pi, comcat_if i"   +
							"           WHERE ic_epo IN ("   +
							"                    SELECT rpe.ic_epo"   +
							"                      FROM comcat_epo e, comrel_pyme_epo rpe"   +
							"                     WHERE rpe.ic_epo = e.ic_epo"   +
							"                       AND e.cs_habilitado = 'S'"   +
							"                       AND rpe.ic_pyme = "+iNoCliente+""   +
							"                       AND rpe.ic_epo = "+noEpo+")"   +
							"             AND pi.ic_if = i.ic_if"  ;
		ResultSet rsnq = con.queryDB(nuevoQuery);

			
	while(rsnq.next()){
			numRegistrosIf++;
			for(int m=0; m < 2; m++) {
				String ic_moneda = (m==0)?"1":"54";
				registros = tasa.getTasaAutIF(noEpo, rsnq.getString("ic_if"), ic_moneda, "");
				if(registros.size() > 0){
				
				pdfDoc.setTable(2, 100);
				pdfDoc.addText(" ","formas",ComunesPDF.LEFT);			
				pdfDoc.addText("Intermediario Financiero: "+ rsnq.getString("cg_razon_social").toString().replace(',',' '),"formas",ComunesPDF.LEFT);
				pdfDoc.addText(((Vector)registros.get(0)).get(6).toString().replace(',',' '),"formas",ComunesPDF.LEFT);
				pdfDoc.addTable();				
				pdfDoc.setTable(6, 100);
				pdfDoc.setCell("Tasa Base","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Rel. Mat.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Puntos Adicionales.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa a Aplicar","celda01",ComunesPDF.CENTER);
					
				
 				for (int i = 0; i < registros.size(); i++) {
						Vector registro = (Vector) registros.get(i);
						relmat_tasa = registro.get(9).toString();
						puntos_tasa = registro.get(10).toString();
						valor_tasa = registro.get(11).toString();
						tasa_aplicar = "";

						if(relmat_tasa.equals("+")) {
							tasa_aplicar = (new BigDecimal(valor_tasa).add(new BigDecimal(puntos_tasa))).toPlainString();
						}
						if(relmat_tasa.equals("-")) {
							tasa_aplicar = (new BigDecimal(valor_tasa).subtract(new BigDecimal(puntos_tasa))).toPlainString();
						}
						if(relmat_tasa.equals("/")) {
							if (!puntos_tasa.equals("0")) {
								tasa_aplicar = (new BigDecimal(valor_tasa).divide(new BigDecimal(puntos_tasa),5)).toPlainString();
							}
						}
						if(relmat_tasa.equals("*")) {
							tasa_aplicar = (new BigDecimal(valor_tasa).multiply(new BigDecimal(puntos_tasa))).toPlainString();
						}
						
					pdfDoc.setCell(registro.get(5).toString(),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(registro.get(8).toString(),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(registro.get(11).toString(),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(registro.get(9).toString(),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(registro.get(10).toString(),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tasa_aplicar,"formas",ComunesPDF.CENTER);
									
					
					} //for
					pdfDoc.addTable();
				} // if(registros.size() > 0)
			} // for m < 2
		} // while 
		//pdfDoc.addTable();	
		pdfDoc.endDocument();
		
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);


}catch(Exception e){
	throw new AppException("Error al generar el archivo", e);
} finally {
	if (con.hayConexionAbierta()) con.cierraConexionDB();
}
%>
<%=jsonObj%>

<%System.out.println("13consulta03extPDF.jsp (S)");%>
