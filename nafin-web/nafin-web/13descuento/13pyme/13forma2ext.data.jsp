<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.model.catalogos.CatalogoEPOPyme,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.descuento.*,
		com.netro.afiliacion.*,
		com.netro.exception.*,
		javax.naming.*,
		com.netro.pdf.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%@ include file="/13descuento/13pki/certificado.jspf" %>
<%@ include file="/13descuento/13forma2ext.data.jspf" %>