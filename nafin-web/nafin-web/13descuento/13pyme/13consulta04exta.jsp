<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String envia = (request.getParameter("envia") == null) ? "" : request.getParameter("envia");
String operacion = (request.getParameter("operacion") == null) ? "" : request.getParameter("operacion");
String icPyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
String icEpo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
String igNumeroDocto = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto");
String dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
String dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
String icMoneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
String icCambioEstatus = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
String fnMontoMin = (request.getParameter("fn_montoMin") == null) ? "" : request.getParameter("fn_montoMin");
String fnMontoMax = (request.getParameter("fn_montoMax") == null) ? "" : request.getParameter("fn_montoMax");
String dcFechaCambioMin = (request.getParameter("dc_fecha_cambioMin") == null) ? "" : request.getParameter("dc_fecha_cambioMin");
String dcFechaCambioMax = (request.getParameter("dc_fecha_cambioMax") == null) ? "" : request.getParameter("dc_fecha_cambioMax");
String dfFechaDoctoMin = (request.getParameter("df_fecha_doctoMin") == null) ? "" : request.getParameter("df_fecha_doctoMin");
String dfFechaDoctoMax = (request.getParameter("df_fecha_doctoMax") == null) ? "" : request.getParameter("df_fecha_doctoMax");
String fechaActual = (request.getParameter("fechaActual") == null) ? "" : request.getParameter("fechaActual");
String tipoFactoraje = (request.getParameter("tipoFactoraje") ==null) ? "" : request.getParameter("tipoFactoraje");
String sOperaFactConMandato = (request.getParameter("sOperaFactConMandato") == null) ? "" : request.getParameter("sOperaFactConMandato");
String mandant = (request.getParameter("mandant") == null) ? "" : request.getParameter("mandant");

// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
String paginaNo = (request.getParameter("paginaNo") == null) ? "1" : request.getParameter("paginaNo");
String nomArchivo = (request.getParameter("nomArchivo") == null) ? "" : request.getParameter("nomArchivo");
int offset = 0;

String strEpo = "", strNumDocto = "", strFechaEmision = "", strFechaVencimiento = "";
String strFechaCambio = "", strClaveMoneda = "", strMoneda = "";
String nombreBeneficiario = "", porcBeneficiario = "", recibirBeneficiario = "";
double dblMonto = 0, dblPorcentaje = 0, dblMontoDescuento = 0;
String strEstatus = "", strTipoEstatus = "", strCausa = "", nombreTipoFactoraje="", tipoFactorajeDesc = "", nombreIf="";
boolean bOperaFactorajeVencido = false;
boolean bOperaFactorajeVencidoGral = false;
boolean bOperaFactVencimientoInfonavit = false;
boolean bOperaFactorajeDistribuido = false;
boolean bOperaFactorajeDistribuidoGral = false;
boolean bOperaNotasDeCredito = false;
boolean bOperaNotasDeCreditoGral = false;
boolean bOperaFactConMandato = false;
boolean bTipoFactoraje = false;
int colspanTF=0, colspanFV=0, colspanFD=0;
double montoTotalMN=0, montoTotalDL=0, montoTotalDsctoMN=0, montoTotalDsctoDL=0;
int numeroTotalDocsMN=0, numeroTotalDocsDols=0, i=0;
ResultSet rsConsulta;

CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
String nombreArchivo = null;

AccesoDB con = new AccesoDB();
CQueryHelper	queryHelper = null;
JSONObject jsonObj = new JSONObject();

try {
	con.conexionDB();

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	sOperaFactConMandato 		= new String("");

	Hashtable alParamEPO = new Hashtable(); 
	//System.out.println("«««« icEpo ««»» "+icEpo+" »»»»");
	if(!icEpo.equals("")) {
		alParamEPO = BeanParamDscto.getParametrosEPO(icEpo, 1);
		if (alParamEPO!=null) {
		bOperaFactConMandato   		  = ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		bOperaFactorajeVencido 		  = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
		bOperaFactorajeDistribuido   = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
		bOperaNotasDeCredito 		  = ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
		bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString())) ? false : true ;
		}
	}
	bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaNotasDeCredito || bOperaFactConMandato || bOperaFactVencimientoInfonavit)?true:false;
	sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
	//System.out.println("«««« sOperaFactConMandato -:generar archivo:- «» "+ sOperaFactConMandato+" »»»»");
	
	queryHelper = new CQueryHelper(new CambioEstatusPymeDE());
 //	queryHelper.cleanSession(request);

	String selected = "";
	String qrySentencia= 
		" SELECT COUNT (1)"   +
		"   FROM comrel_pyme_epo pe, comrel_producto_epo e"   +
		"  WHERE ic_pyme = "+iNoCliente+
		"    AND pe.ic_epo = e.ic_epo"   +
		"    AND e.ic_producto_nafin = 1"   +
		"    AND cs_factoraje_vencido = 'S'"   +
		" UNION ALL"   +
		" SELECT COUNT (1)"   +
		"   FROM comrel_pyme_epo pe, comrel_producto_epo e"   +
		"  WHERE ic_pyme = "+iNoCliente+ 
		"    AND pe.ic_epo = e.ic_epo"   +
		"    AND e.ic_producto_nafin = 1"   +
		"    AND cs_factoraje_distribuido = 'S'"  +
		" UNION ALL"   +
		" SELECT COUNT (1)"   +
		"   FROM comrel_pyme_epo pe, comrel_producto_epo e"+
		"  WHERE ic_pyme = "+iNoCliente+
		"    AND pe.ic_epo = e.ic_epo "   +
		"    AND e.ic_producto_nafin = 1"   +
		"    AND cs_opera_notas_cred = 'S'";
	
	ResultSet rs = con.queryDB(qrySentencia);
	if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeVencidoGral = true; }
	if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeDistribuidoGral = true; }
	if (rs.next()) { if(rs.getInt(1)>0) bOperaNotasDeCreditoGral = true; }
	con.cierraStatement();
	
    if(bOperaFactorajeVencidoGral || bOperaFactorajeDistribuidoGral || bOperaNotasDeCreditoGral) {
        bOperaFactorajeVencido = false;
	 	bOperaFactorajeDistribuido = false;
        if(icEpo.equals("")) {
			if (bOperaFactorajeVencidoGral)		bOperaFactorajeVencido = true;
			if (bOperaFactorajeDistribuidoGral)	bOperaFactorajeDistribuido = true;
			if (bOperaNotasDeCreditoGral)		bOperaNotasDeCredito = true;
		} 
	}
	/*******************
	Cabecera del archivo
	********************/			
	  contenidoArchivo.append("EPO,Num. de Documento,Fecha Emisión,Fecha Vencimiento,"+((icCambioEstatus.equals("29"))?"Nombre IF,":"")+"Fecha Cambio de Estatus,Moneda,");
	  
	  if(bTipoFactoraje) { /* Desplegar si opera algun Factoraje especial */ 
		 	colspanTF = 1;
			contenidoArchivo.append("Tipo Factoraje,");
	  }
	  
	  contenidoArchivo.append("Monto,Porcentaje de Descuento,Monto a Descontar,Tipo Cambio de Estatus,Causa"+((icCambioEstatus.equals("29"))?"":",Monto Anterior"));
	 if(!icCambioEstatus.equals("29")){
		if(bOperaFactorajeVencido)/* Desplegar solo si opera "Factoraje Vencido" */ 
				contenidoArchivo.append(",Nombre IF");
		  
		if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { /* Desplegar solo si opera "Factoraje Distribuido" ó "Factoraje Vencimiento Infonavit"*/ 
				colspanFD=3;
				contenidoArchivo.append(",Beneficiario,% Beneficiario,Importe a Recibir Beneficiario");
		}
	 }//////29=Programado a Negociable
	 
	 if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
		 contenidoArchivo.append(", Mandante");								
	  }
	  contenidoArchivo.append("\n");

	/***************************/
	/* Generacion del archivo */
	/***************************/
	rsConsulta = queryHelper.getCreateFile(request,con);
    while (rsConsulta.next()) {

            strEpo = rsConsulta.getString(1).trim();
            strNumDocto = rsConsulta.getString(2).trim();
            strFechaEmision = rsConsulta.getString(4).trim();
            strFechaVencimiento = (rsConsulta.getString(3)==null)?"":rsConsulta.getString(3).trim();
            strFechaCambio = rsConsulta.getString(5).trim();
            strClaveMoneda = rsConsulta.getString(6).trim();
            strMoneda = rsConsulta.getString(7).trim();
            strEstatus = rsConsulta.getString(13);
            //dblMonto = rsConsulta.getDouble(8);
			if(strEstatus.equals("28")) //aplicacion de nota de credito
			   dblMonto=rsConsulta.getDouble("fn_monto_nuevo");
			else
			   dblMonto = rsConsulta.getDouble(8);
			
			nombreBeneficiario = (rsConsulta.getString("NOMBRE_BENEFICIARIO")==null)?"":rsConsulta.getString("NOMBRE_BENEFICIARIO");
			porcBeneficiario = (rsConsulta.getString("FN_PORC_BENEFICIARIO")==null)?"":rsConsulta.getString("FN_PORC_BENEFICIARIO");
			recibirBeneficiario = (rsConsulta.getString("RECIBIR_BENEFICIARIO")==null)?"":rsConsulta.getString("RECIBIR_BENEFICIARIO");
			String smandant = "";
			 if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
				smandant = (rsConsulta.getString("mandante")==null)?"":rsConsulta.getString("mandante");
			}
            //dblPorcentaje = rsConsulta.getDouble(12);
            if (strEstatus.equals("4") || strEstatus.equals("5") || strEstatus.equals("6") || strEstatus.equals("8")|| strEstatus.equals("28")){
				dblPorcentaje = rsConsulta.getDouble(12);
				dblMontoDescuento = dblMonto * (dblPorcentaje / 100);
			}else{
				dblPorcentaje = rsConsulta.getDouble(15);
				dblMontoDescuento = rsConsulta.getDouble(9);
			}

            strTipoEstatus = rsConsulta.getString(10).trim();
            strCausa = (rsConsulta.getString(11)==null)?"":rsConsulta.getString(11);

            nombreTipoFactoraje = (rsConsulta.getString("CS_DSCTO_ESPECIAL")==null)?"":rsConsulta.getString("CS_DSCTO_ESPECIAL");
            nombreIf = "";
            if(nombreTipoFactoraje.equals("V") || nombreTipoFactoraje.equals("D") || nombreTipoFactoraje.equals("I")) {
               nombreIf = (rsConsulta.getString("NOMBREIF")==null)?"":rsConsulta.getString("NOMBREIF").trim();
               dblMontoDescuento = dblMonto;
               dblPorcentaje = 100;
            }
			
			if(strEstatus.equals("29")){//29=Programado a Negociable
				nombreIf = (rsConsulta.getString("NOMBREIF")==null)?"":rsConsulta.getString("NOMBREIF").trim();
				dblPorcentaje = rsConsulta.getDouble(12);
				dblMontoDescuento = dblMonto * (dblPorcentaje / 100);
			}

            tipoFactorajeDesc = (rsConsulta.getString("TIPO_FACTORAJE")==null)?"":rsConsulta.getString("TIPO_FACTORAJE");
				nombreTipoFactoraje = tipoFactorajeDesc;

	 /*************************/
	 /* Contenido del archivo */
	 /**************************/
	
    if(strEstatus.equals("8")||strEstatus.equals("28")) { /* estatus "Modificacion de Montos"  y Aplicacion de nota de credito*/
		contenidoArchivo.append(strEpo.replace(',',' ')+","+strNumDocto+","+strFechaEmision+","+strFechaVencimiento+",");
		contenidoArchivo.append(strFechaCambio+","+strMoneda);
		
		if(bTipoFactoraje) /* Desplegar si opera algun Factoraje especial */
        	contenidoArchivo.append(","+nombreTipoFactoraje.replace(',',' '));
		
		contenidoArchivo.append(",$"+Comunes.formatoDecimal(dblMonto,2,false)+","+dblPorcentaje+",$"+Comunes.formatoDecimal(dblMontoDescuento,2,false)+","+strTipoEstatus +"  "+ (strEstatus.equals("28")?strFechaCambio:"")+","+strCausa.replace(',',' '));
		
		if(rsConsulta.getString(14)!=null)
			contenidoArchivo.append(","+rsConsulta.getString(14));
		
		if(bOperaFactorajeVencido)/* Desplegar solo si opera "Factoraje Vencido" */
			contenidoArchivo.append(","+nombreIf.replace(',',' ')); 
		
		if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { /* Desplegar solo si opera "Factoraje Distribuido" ó "Factoraje Vencimiento Infonavit" */
			contenidoArchivo.append(","+nombreBeneficiario.replace(',',' '));
			contenidoArchivo.append(","+porcBeneficiario+",$"+Comunes.formatoDecimal(recibirBeneficiario,2,false));
		}
	} else {
		
		contenidoArchivo.append(strEpo.replace(',',' ')+","+strNumDocto+","+strFechaEmision+","+strFechaVencimiento+",");
		
		if(strEstatus.equals("29"))
			contenidoArchivo.append(nombreIf.replace(',',' ')+",");
		
		contenidoArchivo.append(strFechaCambio+","+strMoneda);
		
        if(bTipoFactoraje) /* Desplegar si opera algun Factoraje especial */
			contenidoArchivo.append(","+nombreTipoFactoraje.replace(',',' '));
		
		contenidoArchivo.append(",$"+Comunes.formatoDecimal(dblMonto,2,false)+","+dblPorcentaje+",$"+Comunes.formatoDecimal(dblMontoDescuento,2,false)+","+strTipoEstatus+","+strCausa.replace(',',' ')+",");
		if(!strEstatus.equals("29")){
			if(bOperaFactorajeVencido)/* Desplegar solo si opera "Factoraje Vencido" */
				contenidoArchivo.append(","+nombreIf.replace(',',' ')); 
			
			if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { /* Desplegar solo si opera "Factoraje Distribuido" ó "Factoraje Vencimiento Infonavit" */
				contenidoArchivo.append(","+nombreBeneficiario.replace(',',' '));
				contenidoArchivo.append(","+porcBeneficiario+",$"+Comunes.formatoDecimal(recibirBeneficiario,2,false));
			}
		}
	}
   
	 if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
		contenidoArchivo.append(","+smandant);
	}
	 
	contenidoArchivo.append("\n");
	i++;
    } //fin del while

	con.cierraStatement();
	
	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	} else {
		nombreArchivo = archivo.nombre;
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	if (con.hayConexionAbierta() == true) {
		con.cierraConexionDB();
	}
}
%>
<%=jsonObj%>