<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.sql.*,
		netropology.utilerias.*,com.netro.descuento.*,
		com.netro.model.catalogos.CatalogoEPOPyme,
		net.sf.json.JSONArray,net.sf.json.JSONObject, net.sf.json.JSONSerializer"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
if (informacion.equals("valoresIniciales")) {
	boolean				esNAFIN			= false;
	boolean				esPYME			= false;
	String 				direccion 		= null;
	
	// Obtener tipo de usuario
	if(strTipoUsuario.equals("NAFIN")){
		esNAFIN = true;
	}else{
		esPYME  = true;
	}
	boolean CONTRATO_ACEPTADO = false;

	// Verificar si el descuento automatico ha sido rechazado por el usuario
	String 	descuentoAutomaticoRechazado 	= BeanParamDscto.haSidoRechazadoElDescuentoAutomatico(iNoCliente)?"true":"false";

	System.out.println(" clavePYME = " + iNoCliente );
	System.out.println(" descuentoAutomaticoRechazado = " + descuentoAutomaticoRechazado );
	
	String nombrePyme = BeanParamDscto.getNombrePyme(iNoCliente); 
	List listaIntermediariosFinancieros = BeanParamDscto.getListaDeIntermediariosFinancieros(iNoEPO,iNoCliente);
	
	if(BeanParamDscto.hayOrdenDeOperacion(iNoEPO,iNoCliente)){
		CONTRATO_ACEPTADO = true;
	}

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("descuentoAutomaticoRechazado", descuentoAutomaticoRechazado);
	jsonObj.put("CONTRATO_ACEPTADO", new Boolean(CONTRATO_ACEPTADO));
	jsonObj.put("listaIntermediariosFinancieros", listaIntermediariosFinancieros);
	jsonObj.put("nombrePyme", nombrePyme);
	jsonObj.put("esNAFIN", new Boolean(esNAFIN));
	jsonObj.put("esPYME", new Boolean(esPYME));	
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("confirmaClave")) {

	/*if(esPYME){}*/
	String clave_usuario = (request.getParameter("cesionUser")!=null)?request.getParameter("cesionUser"):"";
	String contrasena = (request.getParameter("cesionPassword")!=null)?request.getParameter("cesionPassword"):"";

	String resultado	=	BeanParamDscto.validaContrasenna(clave_usuario, contrasena, iNoEPO, iNoCliente ,sesIdiomaUsuario, strTipoUsuario,  request.getRemoteAddr(), request.getRemoteHost(), "CAPTU");

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", resultado);
	infoRegresar = jsonObj.toString();

}  else if (informacion.equals("rechazaDescuentoAutomatico")) {

	List		valoresAnteriores = BeanParamDscto.getBancosOrden(iNoCliente);
	BeanParamDscto.rechazarDescuentoAutomatico(iNoCliente);
	List		valoresNuevos 		= BeanParamDscto.getBancosOrden(iNoCliente);

	BeanParamDscto.actualizaBitacoraOrdenIF(iNoEPO,iNoUsuario,iNoCliente,valoresAnteriores,valoresNuevos);

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString(); 
} else if (informacion.equals("insertaOrden")){

	String cadList = (request.getParameter("listaIntermediarios")!=null)?request.getParameter("listaIntermediarios"):"";
	JSONArray listaIntermediarios = JSONArray.fromObject(cadList);

	List valoresAnteriores = BeanParamDscto.getBancosOrden(iNoEPO, iNoCliente);
	BeanParamDscto.actualizaOrdenDeIntermediariosFinancieros(iNoEPO,iNoCliente,listaIntermediarios);
	BeanParamDscto.actualizaBitacoraOrdenIF(iNoEPO,iNoUsuario,iNoCliente,valoresAnteriores,listaIntermediarios);

	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>