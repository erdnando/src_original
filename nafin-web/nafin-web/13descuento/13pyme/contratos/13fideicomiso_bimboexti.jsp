
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<script language="JavaScript">
function enviar() {
	var f = document.forma; 
	if (!f.acepto.checked) {
		alert("Para poder continuar debe aceptar el contrato");
		return;
	}
	f.submit();
}

function fnImprimir(){
	window.open("ConvModDsctoAAAGpoBimbo.pdf","","titlebar=no,scrollbars=yes,resizable=yes,toolbar=no,status=no,top=300,left=50,height=400,width=500");
}

function fnAbrirDocto(valor){
	if(valor=='pdf_pf'){
		window.open("CaratulaDescPdf_pf.pdf","","titlebar=no,scrollbars=yes,resizable=yes,toolbar=no,status=no,top=300,left=50,height=400,width=500")
	}else if(valor=='doc_pf'){
		window.open("CaratulaDescWord_pf.doc","","titlebar=no,scrollbars=yes,resizable=yes,toolbar=no,status=no,top=300,left=50,height=400,width=500")
	}else if(valor=='doc_c'){
		window.open("CartaAutoTransferWord.doc","","titlebar=no,scrollbars=yes,resizable=yes,toolbar=no,status=no,top=300,left=50,height=400,width=500")
	}else if(valor=='pdf_pm'){
		window.open("CaratulaDescPdf_pm.pdf","","titlebar=no,scrollbars=yes,resizable=yes,toolbar=no,status=no,top=300,left=50,height=400,width=500")
	}else if(valor=='doc_pm'){
		window.open("CaratulaDescWord_pm.doc","","titlebar=no,scrollbars=yes,resizable=yes,toolbar=no,status=no,top=300,left=50,height=400,width=500")
	}
}
</script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="600" cellpadding="10" cellspacing="0" border="0">
	<tr>
		<td valign="top" align="center"><table width="100%" border="0">
				<tr>
					<td class="formas">
						<table>
							<tr>
								<td class="formas">
									<strong>Favor de Imprimir y Conservar el</strong> <a class="Estilo1" href="javascript:fnImprimir()"><u>Convenio Modificatorio</u></a>			
								</td>
							</tr>
						</table>
						<br />
						<hr>
					</td>
				</tr>
				<tr>
					<td class="formas">
					En caso de operar D�lares Am�ricanos, favor de requisitar, firmar y enviar a la siguiente direcci�n los documentos abajo se�alados:

					
					<br /><br /><strong>Favor de llenar, firmar y enviar los siguientes documentos a:<br /><br />
						<center>
						</strong> &bull;GRUPO BIMBO S.A.B. DE C.V.<br />
						Prolongaci&oacute;n Paseo de la Reforma No. 1000<br />
						Col. Pe&ntilde;a Blanca Santa Fe<br />
						Delegaci&oacute;n &Aacute;lvaro Obreg&oacute;n C.P. 01210<br />
						M&eacute;xico D.F. <br />
						Atenci&oacute;n.&nbsp; Magali Casares Vadillo<br />
						Tel. 5268 6799 y Fax. 5268 6697<br />
						</center>
						<strong><br />
						1.- Caratula de Descuento Electr&oacute;nico (seg�n sea el caso)</strong><br />
						<br />
						<table border="1" align="center">
							<tr>
								<td class="formas">Persona F&iacute;sica</td>
								<!--td class="formas"><a href="javascript:fnAbrirDocto('doc_pf')"><img border="0" src="../../../00utils/gif/word.jpg" alt="Formato Word" width="28" height="28" /> Versi&oacute;n Word</a></td-->
								<td class="formas"><a href="javascript:fnAbrirDocto('pdf_pf')"><img border="0" src="../../../00utils/gif/acrobatreader.jpg" alt="Formato PDF" width="28" height="28" /> Versi&oacute;n PDF</a></td>
							</tr>
							<tr>
								<td class="formas">Persona Moral</td>
								<!--td class="formas"><a href="javascript:fnAbrirDocto('doc_pm')"><img border="0" src="../../../00utils/gif/word.jpg" alt="Formato Word" width="28" height="28" /> Versi&oacute;n Word</a></td-->
								<td class="formas"><a href="javascript:fnAbrirDocto('pdf_pm')"><img border="0" src="../../../00utils/gif/acrobatreader.jpg" alt="Formato PDF" width="28" height="28" /> Versi&oacute;n PDF</a></td>
							</tr>
						</table>
						<br />
						<br />
						<strong>2.- Carta de Autorizaci&oacute;n de Transferencias Electr&oacute;nicas</strong><br />
						<br />
						<table border="1" align="center">
							<tr>
								<td class="formas"><a href="javascript:fnAbrirDocto('doc_c')"><img border="0" src="../../../00utils/gif/word.jpg" alt="Formato Word" width="28" height="28" /> Versi&oacute;n Word</a></td>
							</tr>
						</table></td>
				</tr>
			</table></td>
	</tr>
</table>

</body>
</html>
<%System.out.println("Termina"); %>

