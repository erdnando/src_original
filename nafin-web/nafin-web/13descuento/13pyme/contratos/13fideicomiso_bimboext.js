Ext.onReady(function() {

	var mostrar = function(componente) {
		var x = Ext.getCmp('panel').body;
		var mgr = x.getUpdater();
		mgr.on('failure', 
		function(el, response) {
			x.update('');
			NE.util.mostrarErrorResponse(response);
		});		
		
		mgr.update({					
			url: '13fideicomiso_bimboexti.jsp',
			scripts: true,					
			indicatorText: 'Cargando...'
		});
	}
	
//Panel: llama a la variable mostrar para visualizar el contenido que tendrá la variable panel	
	var panel = {
		xtype: 'panel',
		id: 'panel',
		width: 600,
		height: 'auto',
		style: 'margin:0 auto;', 
		title: 'Convenio Modificatorio y Carátulas',
		frame : true,
		listeners: {
			afterrender: mostrar
		}
	}


//Dado que la aplicación se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 940,
		height: 'auto',
		items: [
			panel			
		]
	});
});