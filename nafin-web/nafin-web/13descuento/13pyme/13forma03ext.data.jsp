<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		net.sf.json.JSONObject,
		netropology.utilerias.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%

String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar = "";

if (informacion.equals("ParametrosIniciales")) {
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("url", session.getAttribute("sesRegresarAPagina"));
	
	infoRegresar = jsonObj.toString();
}
%>
<%=infoRegresar%>
