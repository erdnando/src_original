﻿Ext.onReady(function(){

	/* Creacion de objetos JS */
	var elementosFormaObj 	= new Object();
	var statusAjaxFn			= new Object();
	var variablesObj			= new Object();
	
	variablesObj.grupo 		= getUrlVar()["ic_grupo"]; // Obtiene valor de variable ic_grupo (URL)
	variablesObj.archivo 	= getUrlVar()["archivo"]; // Obtiene valor de variable archivo (URL)
	variablesObj.pais			= '';	
	
	/* Funcion para obtener variable URL  */
	function getUrlVar() {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
			vars[key] = value;
		});
		return vars;
	}
	
	/* Error en el Ajax Request */  
   statusAjaxFn.failure = function(response, request) { 
		var errMessage = '<b>Error en la petición</b> ' + request.url + '<br> '  
							+ '<b>status</b> ' + response.status + '<br>'  
							+ '<b>statusText</b> ' + response.statusText;							  
		Ext.Msg.show({
			title:	'Error en la peticion',
			msg:		errMessage,
			icon:		Ext.Msg.ERROR,
			buttons:	Ext.Msg.OK
		});
   } 
	/* Success en el Ajax Request  */      
   statusAjaxFn.success = function(response, request) {  
      var jsonData = Ext.util.JSON.decode(response.responseText);  
      
		if (true == jsonData.success) {  
			variablesObj.grupo = jsonData.grupo; 
			/* Mensaje de Aviso */
			variablesObj.aviso = '<div class="aviso"><h1>Aviso importante</h1>Grupo: <b>' + variablesObj.grupo + '</b> requiere estar en contacto permanente con sus proveedores,<br />por esta razón es necesario que capture la siguiente información.</div>';
			Ext.getCmp('lblAviso').setText(variablesObj.aviso,false);
			Ext.getCmp('id_razonsocial').setValue(jsonData.razonsocial);
			Ext.getCmp('id_rfc').setValue(jsonData.rfc);
			Ext.getCmp('id_calle_numero').setValue(jsonData.calle_numero);
			Ext.getCmp('id_colonia').setValue(jsonData.colonia);
			Ext.getCmp('id_cp').setValue(parseInt(jsonData.cp));
			Ext.getCmp('id_telefono').setValue(parseInt(jsonData.telefono));
			Ext.getCmp('id_fax').setValue(parseInt(jsonData.fax));
			Ext.getCmp('id_email').setValue(jsonData.email);
			Ext.getCmp('id_nombrec').setValue(jsonData.nombrec);
			Ext.getCmp('id_telefonoc').setValue(parseInt(jsonData.telefonoc));
			Ext.getCmp('id_faxc').setValue(parseInt(jsonData.faxc));
			Ext.getCmp('id_emailc').setValue(jsonData.emailc);
		}
   }	
	
	/* Saving Data */
	statusAjaxFn.savingData = function(){
		if(variablesObj.urlForma!=''){
			window.location(variablesObj.urlForma);
		} else {
			/* Limpio Formulario */
			var form = Ext.getCmp('forma');
			form.getForm().reset();
			
			/* Inicia Programa **/
			iniciaPrograma();
		}
	}
	
	/* Catalogos que llenan los combo-Box */
	var catalogoPais = new Ext.data.JsonStore({
		xtype: 	'jsonstore',
		id:		'catalogoPais',
		root:    'registros',
      fields : ['clave', 'descripcion','msg'],
		url:		'captura_datosext.data.jsp',
		baseParams:{
			informacion:	'CatalogoPais'
		},
		totalProperty: 'total',
		autoLoad:		true,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var catalogoEstado = new Ext.data.JsonStore({
		xtype: 	'jsonstore',
		id:		'catalogoEstado',
		root:    'registros',
      fields : ['clave', 'descripcion','msg'],
		url:		'captura_datosext.data.jsp',
		baseParams:{
			informacion:	'CatalogoEstado'
		},
		totalProperty: 'total',
		autoLoad:		false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	
	var catalogoDelegMunicipio = new Ext.data.JsonStore({
		xtype: 	'jsonstore',
		id:		'catalogoDelegMunicipio',
		root:    'registros',
      fields : ['clave', 'descripcion','msg'],
		url:		'captura_datosext.data.jsp',
		baseParams:{
			informacion:	'CatalogoDelegMunicipio'
		},
		totalProperty: 'total',
		autoLoad:		false,
		listeners: {
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
						NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				}
			}
		}
	});
	/* Fin Catalogos que llenan los combo-box */
	
	elementosFormaObj.NombreEmpresa = {
		xtype	:			'fieldset',
		title:			'Nombre de la Empresa',
		collapsible:	false,
		items:	[{
			xtype:			'compositefield',
			combineErrors: false,
			fieldLabel:		'Nombre o <br/>Razón Social',
			msgTarget: 		'side',
			items:	[{
				xtype:		'textfield',
				id:			'id_razonsocial',
				name:			'razonsocial',
				style:      'background: none; border: 0px; border-bottom: 1px solid #c4c4c4; font-weight:bold',
				readOnly:	true,
				width:		350
			},{
				xtype:	'displayfield',
				value:	'RFC:',
				style:	'margin-left: 20px',
				width:	40
			},{
				xtype:		'textfield',
				id:			'id_rfc',
				name:			'rfc',
				style:		'background: none; border:0px; border-bottom: 1px solid #c4c4c4; font-weight:bold',
				readOnly:	true
			}]
		}]
	};
	
	elementosFormaObj.DomicilioEmpresa = {
		xtype:				'fieldset',
		title:				'Domicilio de la Empresa',
		collapsible:		false,
		items:	[{
			xtype:			'compositefield',
			msgTarget: 		'side',
			combineErrors: false,
			msgTarget: 		'side',
			fieldLabel:		'<span id="ob">*</span>Calle y Número',
			items:	[{
				xtype:		'textfield',
				id:			'id_calle_numero',
				name:			'calle_numero',
				allowBlank:	false,
				msgTarget: 		'side',
				width:		250,
				maxLength:	100
			},{
				xtype:		'displayfield',
				value:		'<span id="ob">*</span>País: ',
				style:		'margin-left: 20px',
				width:		150
			},{
				xtype:			'combo',
				id:				'id_comcat_pais',
				name:				'pais',
				hiddenName:		'pais',
				displayField:	'descripcion',
				valueField:		'clave',
				emptyText:		'- Seleccione -',
				triggerAction:	'all',
				typeAhead:		true,
				minChars:		1,
				allowBlank:		false,
				msgTarget: 		'side',
				forceSelection : true,
				mode: 			'local',
				store:			catalogoPais,
				listeners:	{
					select:	{
						fn:	function(comboField, record, index){
							variablesObj.pais = record.get('clave');
							Ext.getCmp('id_estado').enable();
							var cmpForma = Ext.getCmp('forma');            
							paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
							catalogoEstado.load({
								params: Ext.apply(paramSubmit, {
			                  pais: record.get('clave')
			               })
							});
						}
					}
				}
			}]
		},{
			xtype:			'compositefield',
			msgTarget: 		'side',
			combineErrors:	false,
			fieldLabel:		'<span id="ob">*</span>Estado',
			items:	[{
				xtype:		'combo',
				id:			'id_estado',
				name:			'estado',
				hiddenName:	'estado',
				disabled: 	true,
				displayField:'descripcion',
				valueField:	'clave',
				emptyText:	'- Seleccione -',
				triggerAction:'all',
				typeAhead:	true,
				minChars:	1,
				allowBlank:	false,
				msgTarget: 		'side',
				width:		250,
				forceSelection : true,
				mode: 		'local',
				store:		catalogoEstado,
				listeners:	{
					select:	{
						fn:	function(comboField, record, index){
							Ext.getCmp('id_delegacion_o_municipio').enable();
							var cmpForma = Ext.getCmp('forma');            
							paramSubmit = (cmpForma)?cmpForma.getForm().getValues():{};
							catalogoDelegMunicipio.load({
								params: Ext.apply(paramSubmit, {
			                  estado: record.get('clave'),
									pais: variablesObj.pais
			               })
							});
						}
					}
				}
			},{
				xtype:	'displayfield',
				value:	'<span id="ob">*</span>Delegación o <span style="margin-left:0px">Municipio: </span>',
				style:	'margin-left: 20px',
				width:	150
			},{
				xtype:			'combo',
				id:				'id_delegacion_o_municipio',
				name:				'delegacion_o_municipio',
				hiddenName:		'delegacion_o_municipio',
				disabled: 		true,
				displayField:	'descripcion',
				valueField:		'clave',
				emptyText:		'- Seleccione -',
				triggerAction:	'all',
				typeAhead:		true,
				minChars:		1,
				allowBlank:		false,
				msgTarget: 		'side',
				forceSelection : true,
				mode: 			'local',
				store:			catalogoDelegMunicipio
			}]
		},{
			xtype:		'compositefield',
			msgTarget: 'side',
			combineErrors: false,
			fieldLabel:	'<span id="ob">*</span>Colonia',
			items:	[{
				xtype:		'textfield',
				id:			'id_colonia',
				name:			'colonia',
				allowBlank:	false,
				msgTarget: 	'side',
				width:		250,
				maxLength:	100
			},{
				xtype:	'displayfield',
				value:	'<span id="ob">*</span>Código Postal:',
				style:	'margin-left: 20px',
				width:	150
			},{
				xtype:		'numberfield',
				id:			'id_cp',
				name:			'cp',
				allowBlank:	false,
				msgTarget: 	'side',
				width:		50,
				maxLength:	5
			}]
		},{
			xtype:		'compositefield',
			msgTarget: 'side',
			combineErrors: false,
			fieldLabel:	'<span id="ob">*</span>Teléfono',
			items:	[{
				xtype:		'numberfield',
				id:			'id_telefono',
				name:			'telefono',
				allowBlank: false,
				msgTarget: 	'side',
				maxLength:	30,
				width:		250
			},{
				xtype:	'displayfield',
				value:	'<span id="ob">*</span>Fax:',
				style:	'margin-left: 20px',
				width:	150
			},{
				xtype:		'numberfield',
				id:			'id_fax',
				name:			'fax',
				allowBlank:	false,
				msgTarget: 	'side',
				maxLength:	30
			}]
		},{
			xtype:		'textfield',
			id:			'id_email',
			name:			'email',
			vtype: 		'email',
			allowBlank:	false,
			msgTarget: 	'side',
			fieldLabel:	'<span id="ob">*</span>E-mail',
			width:		250,
			maxLength:	50
		}]
	};
	
	elementosFormaObj.DatosContacto = {
		xtype:		'fieldset',
		title:		'Datos del Contacto',
		collapsible:false,
		items:	[{
			xtype:		'compositefield',
			
			combineErrors: false,
			fieldLabel:	'<span id="ob">*</span>Nombre',
			items:	[{
				xtype:			'textfield',
				id:				'id_nombrec',
				name:				'nombrec',
				allowBlank:		false,
				msgTarget: 		'side',
				width:			250,
				maxLength:		100
			},{
				xtype:		'displayfield',
				value:		'<span id="ob">*</span>Teléfono: ',
				style:		'margin-left: 20px',
				width: 		150
			},{
				xtype:			'numberfield',
				id:				'id_telefonoc',
				name:				'telefonoc',
				allowBlank:		false,
				msgTarget: 		'side',
				maxLength:		30
			}]
		},{
			xtype:		'compositefield',
			msgTarget: 'side',
			combineErrors: false,
			fieldLabel:	'<span id="ob">*</span>Fax',
			items:	[{
				xtype:			'numberfield',
				id:				'id_faxc',
				name:				'faxc',
				allowBlank:		false,
				msgTarget: 		'side',
				width:			150,
				maxLength:		100
			},{
				xtype:		'displayfield',
				value:		'<span id="ob">*</span>E-mail: ',
				style:		'margin-left:120px',
				width: 		250
			},{
				xtype:			'textfield',
				fieldLabel:		'E-mail',
				id:				'id_emailc',
				name:				'emailc',
				vtype: 			'email',
				allowBlank:		false,
				msgTarget: 		'side',
				width:			200,
				maxLength:		50
			}]
		}]
	};
	
	elementosFormaObj.mensajes = {
		xtype:	'label',
		html:		'<div class="mensajes">* Campos Obligatorios</div>'
	};
	elementosFormaObj.aviso = {
		id:		'lblAviso',
		xtype:	'label',
		html:		variablesObj.aviso
	};
	/* Termina Objetos de elementos forma */
	
	/* Elementos de la forma */
	var elementosForma = {
		xtype:	'form',
		id:		'formularioElemental',
		title:	'',
		items:	[
			elementosFormaObj.aviso,
			elementosFormaObj.NombreEmpresa,
			elementosFormaObj.DomicilioEmpresa,
			elementosFormaObj.DatosContacto,
			elementosFormaObj.mensajes
		]
	}
	
	/* Forma contenedora */
	var fp = new Ext.form.FormPanel({
		id:				'forma',
		width:			800,
		title:			'',
		frame:			true,
		titleCollapse:	false,
		style:			'margin: 0 auto',
		bodyStyle:		'padding: 6px',
		labelWidth:		100,
		defaultType:	'textfield',
		items:			elementosForma, // Elementos de la  forma
		monitorValid:	true,
		defaults: {	msgTarget: 'side',	anchor: '-20'	},
		buttons:			[{
			text:		'Enviar Información',
			id:		'btnEnviar',
			iconCls:	'icoAceptar',
			formBind:true,
			handler: function(boton, evento){
				var form = Ext.getCmp('forma');
				form.getForm().submit({
					url:		'captura_datosext.data.jsp',
					method:	'POST',
					waitMsg : 'Salvando datos...',
					params:	{
						accion:	'G',
						ic_grupo:getUrlVar()["ic_grupo"],
						archivo:	variablesObj.archivo
					},
					success: function (form, request) {
						responseData = Ext.util.JSON.decode(request.response.responseText);  
						variablesObj.urlForma = responseData.urlForma;
	               Ext.MessageBox.show({  
	                  title: 		'Datos recibidos correctamente',  
	                  msg: 			responseData.msgRecepcion,  
	                  buttons: 	Ext.MessageBox.OK,  
	                  icon: 		Ext.MessageBox.INFO ,
							fn:			statusAjaxFn.savingData
	               });
					},
					failure: function (form, action) {  
	                Ext.MessageBox.show({  
	                    title: 'Error al recibir los datos',  
	                    msg: 'Error al recibir los datos.',  
	                    buttons: Ext.MessageBox.OK,  
	                    icon: Ext.MessageBox.ERROR  
	                });  
					}
				});	
				 // Falta aqui
				//window.location('13forma1ext.jsp');
			}
		}]
	});
	
	/* Contenedor Principal */
	var pnl = new Ext.Container({
		id:		'contenedorPrincipal',
		applyTo:	'areaContenido',
		style:	'margin: 0 auto',	
		width:	949,
		items:	[
			NE.util.getEspaciador(10),
			fp
		]
	});
	
	var iniciaPrograma = function(){
		Ext.getBody().mask("Cargando pantalla...", 'x-mask-loading');
		Ext.Ajax.request({
			url:		'captura_datosext.data.jsp',
			method:	'POST',
			success:	statusAjaxFn.success,
			failure:	statusAjaxFn.failure,
			callback: function(){	Ext.getBody().unmask();	},
			params:	{
				informacion:	'IniciaPrograma',
				ic_grupo:		variablesObj.grupo,
				archivo:			variablesObj.archivo
			}
		});
	};	
	
	/* Inicia Programa **/
	iniciaPrograma();
});