<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%

AccesoDB con = new AccesoDB();
CQueryHelperExtJS	queryHelper = null;
JSONObject jsonObj = new JSONObject();
String ic_epo_aserca = (String)session.getAttribute("sesEpoAserca");
String operacion = (request.getParameter("operacion") == null ) ? "" : request.getParameter("operacion");
String noEpo = (request.getParameter("no_epo") == null) ? "" : request.getParameter("no_epo");
String noIf = (request.getParameter("no_if") == null) ? "" : request.getParameter("no_if");
String noDocto = (request.getParameter("no_docto") == null) ? "" : request.getParameter("no_docto");
String dfFechaDoctode = (request.getParameter("df_fecha_docto_de") == null) ? "" : request.getParameter("df_fecha_docto_de");
String dfFechaDoctoa = (request.getParameter("df_fecha_docto_a") == null) ? "" : request.getParameter("df_fecha_docto_a");
String campDina1 = (request.getParameter("camp_dina1") == null) ? "" : request.getParameter("camp_dina1");
String campDina2 = (request.getParameter("camp_dina2") == null) ? "" : request.getParameter("camp_dina2");
String ordIf = (request.getParameter("ord_if") == null) ? "" : request.getParameter("ord_if");
String ordNoDocto = (request.getParameter("ord_no_docto") == null) ? "" : request.getParameter("ord_no_docto");
String ordEpo = (request.getParameter("ord_epo") == null) ? "" : request.getParameter("ord_epo");
String ordFecVenc = (request.getParameter("ord_fec_venc") == null) ? "" : request.getParameter("ord_fec_venc");
String dfFechaVencde = (request.getParameter("df_fecha_venc_de") == null) ? "" : request.getParameter("df_fecha_venc_de");
String dfFechaVenca = (request.getParameter("df_fecha_venc_a") == null) ? "" : request.getParameter("df_fecha_venc_a");
String noMoneda = (request.getParameter("no_moneda") == null) ? "" : request.getParameter("no_moneda");
String montoDe = (request.getParameter("monto_de") == null) ? "" : request.getParameter("monto_de");
String montoA = (request.getParameter("monto_a") == null) ? "" : request.getParameter("monto_a");
String noEstatusDocto = (request.getParameter("no_estatus_docto") == null) ? "" : request.getParameter("no_estatus_docto");
String fechaIfde = (request.getParameter("fecha_if_de") == null) ? "" : request.getParameter("fecha_if_de");
String fechaIfa = (request.getParameter("fecha_if_a") == null) ? "" : request.getParameter("fecha_if_a");
String tasade = (request.getParameter("tasa_de") == null) ? "" : request.getParameter("tasa_de");
String tasaa = (request.getParameter("tasa_a") == null) ? "" : request.getParameter("tasa_a");
String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
String sOperaFactConMandato = (request.getParameter("sOperaFactConMandato") == null) ? "" : request.getParameter("sOperaFactConMandato");
String sOperaFactVencimientoInfonavit = (request.getParameter("sOperaFactVencimientoInfonavit") == null) ? "" : 
														request.getParameter("sOperaFactVencimientoInfonavit");
String mandant = (request.getParameter("mandant") == null) ? "" : request.getParameter("mandant");
//String txtbanderaTablaDoctos = (request.getParameter("txtbanderaTablaDoctos") == null) ? "" : request.getParameter("txtbanderaTablaDoctos");
String fechaEntrega = "", tipoCompra = "", clavePresupuestaria = "", periodo = "";

// Fodea 002 - 2010
boolean ocultarDoctosAplicados = (
			noEstatusDocto.equals("") 	|| 
			noEstatusDocto.equals("1") || 
			noEstatusDocto.equals("2") || 
			noEstatusDocto.equals("5")	|| 
			noEstatusDocto.equals("6") || 
			noEstatusDocto.equals("7") || 
			noEstatusDocto.equals("9")	|| 
			noEstatusDocto.equals("10")|| 
			noEstatusDocto.equals("21")|| 
			noEstatusDocto.equals("23")|| 
			noEstatusDocto.equals("28")|| 
			noEstatusDocto.equals("29")|| 
			noEstatusDocto.equals("30")|| 
			noEstatusDocto.equals("31") )?true:false;

try {
	con.conexionDB();
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

	ResultSet rsConsulta;
	CreaArchivo archivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
	String nombreArchivo = null;
	String rs_fn_porc_docto_aplicado = "";
	boolean bOperaFactConMandato = false;
	boolean bOperaFactorajeVencido = false;
	boolean bOperaFactorajeIF = false;
	boolean bOperaFactorajeDistribuido = false;
	boolean bOperaFactorajeNotaDeCredito = false;
	boolean bOperaFactVencimientoInfonavit = false;
	boolean bTipoFactoraje = false;
	sOperaFactConMandato	= new String("");
	sOperaFactVencimientoInfonavit = new String("");

	// Fodea 002 - 2010
	String numeroDocumento		=	"";
	String esNotaDeCreditoSimpleAplicada					=	"";
	String esDocumentoConNotaDeCreditoSimpleAplicada	=	"";
	String esNotaDeCreditoAplicada							=	"";
	String esDocumentoConNotaDeCreditoMultipleAplicada	=	"";
	
	Hashtable alParamEPO = new Hashtable(); 
	if(!iNoEPO.equals("")) {
		alParamEPO = BeanParamDscto.getParametrosEPO(iNoEPO, 1);
		if (alParamEPO!=null) {
		bOperaFactConMandato   		  = ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		bOperaFactorajeVencido 		  = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
		bOperaFactorajeIF				  =  ("N".equals(alParamEPO.get("FACTORAJE_IF").toString()))?false:true;
		bOperaFactorajeDistribuido   = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
		bOperaFactorajeNotaDeCredito = ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
		bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;
		}
	}
	bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaDeCredito || bOperaFactConMandato || bOperaFactVencimientoInfonavit || bOperaFactorajeIF)?true:false;
	sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
	sOperaFactVencimientoInfonavit = (bOperaFactVencimientoInfonavit == true)?"S":"N";
	double dblPorciento = 0, dblPorcientoDL = 0;
	if (!noEpo.equals("")) {
		Registros reg = new Registros();
		reg = BeanParamDscto.getTipoAforoPantalla(noEpo);
		while(reg.next()){
			dblPorciento = Double.parseDouble(reg.getString(1));
			dblPorcientoDL = Double.parseDouble(reg.getString(2));
		}
	}
	queryHelper = new CQueryHelperExtJS(new ConsDoctosPymeDE());

	String banderaTablaDoctos = "";
	boolean mostrarOpcionPignorado=false;
	Registros regPorcentaje = null;
	Registros regCamposAdicionales = null;
	Registros regCamposDetalle = null;
	int totalcd=0;
	if(!noEpo.equals("")){
		mostrarOpcionPignorado = BeanParamDscto.mostrarOpcionPignorado(noEpo, iNoCliente);
		String sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(noEpo);
		
		if(BeanSeleccionDocumento.bgetExisteParamDocs(noEpo)){
			banderaTablaDoctos = "1";
		}
		totalcd = BeanSeleccionDocumento.getTotalCamposAdicionales(noEpo);
		if(totalcd >=1) {
			regCamposAdicionales = new Registros();
			regCamposAdicionales = BeanParamDscto.getCamposAdicionales(noEpo);
			regCamposDetalle = new Registros();
			regCamposDetalle = BeanParamDscto.getCamposAdicionalesDetalle(noEpo);
		}
	}
	bOperaFactorajeVencido = true;
	
	int val=0, registros=0, numDocumentosMN = 0, numDocumentosDL = 0, coma=0;
	double totalDolares = 0, totalMn = 0;
	String query = "", nombreEpo = "", nombreIf = "";
	String igNumeroDocto = "", dfFechaDocto = "", dfFechaVenc = "", nombreMoneda = "";
	double fnMonto = 0, fnPorciento = 0, fnMontoDscto = 0, importeRecibir = 0,importeDepositar = 0;
	String inTasaAceptada = "", estatusDocto = "", fechaSolicitud = "", icMoneda = "";
	String strEstatusDocto = "", icDocumento = "", cs_cambio_importe = "", cg_razon_social_IF = "";
	String cgCampo1 = "", cgCampo2 = "", cgCampo3 = "", cgCampo4 = "", cgCampo5 = "";
	String nombreTipoFactoraje = "", tipoFactorajeDesc = "", plazo = null, importeInteres = null, netoRecibirPyme = "", nombreBeneficiario = "", porcBeneficiario = "", recibirBeneficiario = "";
	String lineacd = "";
	double dblPorcientoActual = 0, dblMontoDescuento = 0;
	double dblTotalDescuentoPesos = 0, dblTotalDescuentoDolares = 0;
	double dblPagosRealizados = 0;
	double dblMontoCredito = 0;
	double dblIntereses = 0;
	double dblMontoPago = 0;
	double dblImporteDepositar = 0;
	String ccAcuse = "";
	String ct_referencia = "";
	String fecha_registro_operacion = "";//FODEA 005 - 2009 ACF
	rsConsulta = queryHelper.getCreateFile(request,con);
		/*		No Negociable			Negociable						Baja					Descuento Físico				Pagado Anticipado			Vencido sin Operar				Pagado sin Operar 				Bloqueado */
	if (noEstatusDocto.equals("") || noEstatusDocto.equals("1") || noEstatusDocto.equals("2") || noEstatusDocto.equals("5") || noEstatusDocto.equals("6") || noEstatusDocto.equals("7") || noEstatusDocto.equals("9") || noEstatusDocto.equals("10") || noEstatusDocto.equals("21") || noEstatusDocto.equals("23"))
	{
		boolean layOutAserca = false;
		if(noEstatusDocto.equals("1") && ic_epo_aserca.equals(noEpo)){
			layOutAserca = true;
		}
		while (rsConsulta.next()) {
			registros++;
			nombreEpo = rsConsulta.getString(1);
			igNumeroDocto = rsConsulta.getString(2);
			dfFechaDocto = rsConsulta.getString(3);
			dfFechaVenc = (rsConsulta.getString(4)==null)?"":rsConsulta.getString(4);
			nombreMoneda = rsConsulta.getString(5);
			fnMonto = rsConsulta.getDouble(6);
			fnPorciento = rsConsulta.getDouble(7);
			strEstatusDocto = rsConsulta.getString(8);
			fnMontoDscto = rsConsulta.getDouble(9);
	
			estatusDocto = rsConsulta.getString(10);
			icMoneda = rsConsulta.getString(11);
			icDocumento = rsConsulta.getString(12);
			cs_cambio_importe = rsConsulta.getString(13);
			cg_razon_social_IF = (rsConsulta.getString(14)==null)?"":rsConsulta.getString(14).trim();
	//					detalles = rsConsulta.getInt(15);
			nombreBeneficiario = (rsConsulta.getString("NOMBRE_BENEFICIARIO")==null)?"":rsConsulta.getString("NOMBRE_BENEFICIARIO");
			porcBeneficiario = (rsConsulta.getString("FN_PORC_BENEFICIARIO")==null)?"":rsConsulta.getString("FN_PORC_BENEFICIARIO");
			recibirBeneficiario = (rsConsulta.getString("RECIBIR_BENEFICIARIO")==null)?"":rsConsulta.getString("RECIBIR_BENEFICIARIO");
			lineacd = "";
			int cont = 1;
			if (regCamposAdicionales != null){
				while (regCamposAdicionales.next()){
					if ( cont == 1 ) {
						cgCampo1 = (rsConsulta.getString(16)==null)?"":rsConsulta.getString(16).trim(); lineacd +=","+cgCampo1;
					}
					if (cont == 2) {
						cgCampo2 = (rsConsulta.getString(17)==null)?"":rsConsulta.getString(17).trim(); lineacd +=","+cgCampo2;
					}
					if (cont == 3) {
						cgCampo3 = (rsConsulta.getString(18)==null)?"":rsConsulta.getString(18).trim(); lineacd +=","+cgCampo3;
					}
					if (cont == 4) {
						cgCampo4 = (rsConsulta.getString(19)==null)?"":rsConsulta.getString(19).trim(); lineacd +=","+cgCampo4;
					}
					if (cont == 5) {
						cgCampo5 = (rsConsulta.getString(20)==null)?"":rsConsulta.getString(20).trim(); lineacd +=","+cgCampo5;
					}
					cont ++;
				}
			}
			ct_referencia = (rsConsulta.getString("ct_referencia")==null)?"":rsConsulta.getString("ct_referencia");
	
			fechaEntrega 				= (rsConsulta.getString("DF_ENTREGA")==null)?"":rsConsulta.getString("DF_ENTREGA");
			tipoCompra 					= (rsConsulta.getString("CG_TIPO_COMPRA")==null)?"":rsConsulta.getString("CG_TIPO_COMPRA");
			clavePresupuestaria = (rsConsulta.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rsConsulta.getString("CG_CLAVE_PRESUPUESTARIA");
			periodo 						= (rsConsulta.getString("CG_PERIODO")==null)?"":rsConsulta.getString("CG_PERIODO");
			String smandant = "";
			if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
			smandant = (rsConsulta.getString("mandante")==null)?"":rsConsulta.getString("mandante");
			}
			if (icMoneda.equals("54")) {
				totalDolares = totalDolares + fnMonto;
				numDocumentosDL ++;
			} else if (icMoneda.equals("1")) {
				totalMn = totalMn + fnMonto;
				numDocumentosMN ++;
			}
	//Correccion de Porcentaje de Descuento
			if (strEstatusDocto.equals("2") || strEstatusDocto.equals("5") || strEstatusDocto.equals("6") || strEstatusDocto.equals("7")){
				dblMontoDescuento = (fnMonto * dblPorciento);
				dblPorcientoActual = dblPorciento * 100;
			} else {
				dblMontoDescuento = fnMontoDscto;
				dblPorcientoActual = fnPorciento;
			}
			if(bTipoFactoraje) { // Para Factoraje Vencido
				if(nombreTipoFactoraje.equals("V")|| nombreTipoFactoraje.equals("A")||nombreTipoFactoraje.equals("D")||nombreTipoFactoraje.equals("C")||nombreTipoFactoraje.equals("I")) { //aqui
					dblMontoDescuento = fnMonto;
					dblPorcientoActual = 100;
				}
			}
			/*******************
			Cabecera del archivo
			********************/
			if(layOutAserca) {
				if(registros == 1) {
					contenidoArchivo.append("\nNombre EPO,Fecha Emision,Fecha Vencimiento,Moneda" +
							",Monto,Porcentaje Descuento,Monto Descuento,Estatus");
					if (regCamposAdicionales != null){
						while (regCamposAdicionales.next()){
							contenidoArchivo.append("," + regCamposAdicionales.getString("NOMBRE_CAMPO"));
						}
					}
					if(banderaTablaDoctos.equals("1")) {
						contenidoArchivo.append(",Fecha de Recepción de Bienes y Servicios,Tipo de Compra (procedimiento),Clasificador por Objeto del Gasto,Plazo Máximo");
					}
				} //registros == 1

				contenidoArchivo.append(
					"\n" + nombreEpo +
					"," + dfFechaDocto +
					"," + dfFechaVenc +
					"," + nombreMoneda +
					"," + fnMonto +
					"," + Comunes.formatoDecimal(dblPorcientoActual,0,false)+
					"," + dblMontoDescuento +
					"," + estatusDocto+
					"," + cgCampo1+
					"," + cgCampo2+
					"," + cgCampo3+
					"," + cgCampo4+
					"," + cgCampo5);
	
				if(banderaTablaDoctos.equals("1")) {
					contenidoArchivo.append(","+fechaEntrega+","+tipoCompra+","+clavePresupuestaria+","+periodo);
				}
			} else {
				if(registros == 1) {
					contenidoArchivo.append("Nombre EPO,IF,Num. Documento,Fecha Emision,Fecha Vencimiento,Moneda");
					if(bOperaFactorajeVencido||bOperaFactorajeIF||bOperaFactorajeDistribuido || bOperaFactorajeNotaDeCredito || bOperaFactVencimientoInfonavit)
						contenidoArchivo.append(",Tipo Factoraje");
					contenidoArchivo.append(",Monto,Monto Descuento,Estatus");
					if (regCamposAdicionales != null){
						while (regCamposAdicionales.next()){
							contenidoArchivo.append("," + regCamposAdicionales.getString("NOMBRE_CAMPO").trim());
						}
					}
					contenidoArchivo.append(",Referencia,Porcentaje Descuento");
					if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { contenidoArchivo.append(",Beneficiario, % Beneficiario, Importe a Recibir Beneficiario"); }
						
					// Fodea 002 - 2010 
					if(!ocultarDoctosAplicados){
						contenidoArchivo.append(",Doctos Aplicados a Nota de Credito");
					}
					
					if(banderaTablaDoctos.equals("1")) {
						contenidoArchivo.append(",Fecha de Recepción de Bienes y Servicios,Tipo de Compra (procedimiento),Clasificador por Objeto del Gasto,Plazo Máximo");
					}
					
					if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
						contenidoArchivo.append(", Mandante");								
					}
					contenidoArchivo.append("\n");
				} //registros == 1
				nombreTipoFactoraje = (rsConsulta.getString("CS_DSCTO_ESPECIAL")==null)?"":rsConsulta.getString("CS_DSCTO_ESPECIAL");
	
				 if(nombreTipoFactoraje.equals("V")||nombreTipoFactoraje.equals("D")||nombreTipoFactoraje.equals("C")||nombreTipoFactoraje.equals("I")) {//aqui agregado
					dblMontoDescuento = fnMonto;
					  fnPorciento = 100;
				 }
				if(bTipoFactoraje) { // Para Factoraje Vencido
					tipoFactorajeDesc = (rsConsulta.getString("TIPO_FACTORAJE")==null)?"":rsConsulta.getString("TIPO_FACTORAJE");
					nombreTipoFactoraje = tipoFactorajeDesc;
				}
				contenidoArchivo.append("\"" + nombreEpo + "\", " + cg_razon_social_IF.replace(',',' ') +
						", " + igNumeroDocto + "," + dfFechaDocto +
						"," + dfFechaVenc + "," + nombreMoneda );
				if(bOperaFactorajeVencido||bOperaFactorajeIF || bOperaFactorajeDistribuido || bOperaFactorajeNotaDeCredito || bOperaFactVencimientoInfonavit)
				{ contenidoArchivo.append("," + nombreTipoFactoraje); }
	
				contenidoArchivo.append("," + fnMonto + "," + dblMontoDescuento + "," + estatusDocto +
						lineacd + "," + ct_referencia + "," + Comunes.formatoDecimal(dblPorcientoActual,0,false));
	
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { contenidoArchivo.append("," + nombreBeneficiario.replaceAll(","," ") + "," + porcBeneficiario + "," + Comunes.formatoDecimal(recibirBeneficiario,2, false));}
				 
				//	Fodea 002 - 2010
				//	Debido a que para esta condicion solo se traen doctos con estatus: 1, 2, 5, 6, 7, 9, 10, 23
				//	No hay ningun detalle de Notas de Credito aplicada a multiples documentos que se pueda ver.
				if(!ocultarDoctosAplicados){
					contenidoArchivo.append(",");
				}
				if(banderaTablaDoctos.equals("1")) {
					contenidoArchivo.append(","+fechaEntrega+","+tipoCompra+","+clavePresupuestaria+","+periodo);
				}
				if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
					contenidoArchivo.append(","+smandant);
				}
				contenidoArchivo.append("\n");
			}
		} // while
	} //if estatus="", Negociable, Baja, Descuento Físico, Pagado Anticipado, Vencido sin Operar, Pagado sin Operar.
		/*		Seleccionada Pyme					Operada					Seleccionada IF					Operada Pagada			Operada Pendiente de Pago       Aplicado a Credito           En Proceso de Autorizacion IF */
		//if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24"))
		if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24") || noEstatusDocto.equals("26"))//FODEA 005 - 2009 ACF
		{
			String ig_tipo_piso="",fn_remanente="";
			while (rsConsulta.next()) {
				registros++;
				nombreEpo = rsConsulta.getString(1);
				nombreIf = (rsConsulta.getString(2)==null)?"":rsConsulta.getString(2).trim();
				igNumeroDocto = rsConsulta.getString(3);
				dfFechaDocto = rsConsulta.getString(4);
				//dfFechaVenc = rsConsulta.getString(5);//aqui
				dfFechaVenc = (rsConsulta.getString(5)==null)?"":rsConsulta.getString(5);
				nombreMoneda = rsConsulta.getString(6);
				fnMonto = rsConsulta.getDouble(7);
				fnPorciento = rsConsulta.getDouble(8);
				fnMontoDscto = rsConsulta.getDouble(9);
				inTasaAceptada = (rsConsulta.getString(10)==null)?"0":rsConsulta.getString(10);
				estatusDocto = rsConsulta.getString(11);
				fechaSolicitud = (rsConsulta.getString(12)== null)? "" : rsConsulta.getString(12); //rsConsulta.getString(12);
				icMoneda = rsConsulta.getString(13);
				icDocumento = rsConsulta.getString(14);
				importeRecibir = rsConsulta.getDouble(15);
				importeDepositar = rsConsulta.getDouble(15);
				cs_cambio_importe = rsConsulta.getString(16);
	//					detalles = rsConsulta.getInt(17);
				ccAcuse = (rsConsulta.getString("cc_acuse")==null)?"":rsConsulta.getString("cc_acuse"); //15/12/2004
				netoRecibirPyme = (rsConsulta.getString("NETO_RECIBIR")==null)?"":rsConsulta.getString("NETO_RECIBIR");
				nombreBeneficiario = (rsConsulta.getString("NOMBRE_BENEFICIARIO")==null)?"":rsConsulta.getString("NOMBRE_BENEFICIARIO");
				porcBeneficiario = (rsConsulta.getString("FN_PORC_BENEFICIARIO")==null)?"":rsConsulta.getString("FN_PORC_BENEFICIARIO");
				recibirBeneficiario = (rsConsulta.getString("RECIBIR_BENEFICIARIO")==null)?"":rsConsulta.getString("RECIBIR_BENEFICIARIO");
				plazo = (rsConsulta.getString("IG_PLAZO")==null)?"":rsConsulta.getString("IG_PLAZO");
				importeInteres = (rsConsulta.getString("IN_IMPORTE_INTERES")==null)?"":rsConsulta.getString("IN_IMPORTE_INTERES");
				
				fechaEntrega 				= (rsConsulta.getString("DF_ENTREGA")==null)?"":rsConsulta.getString("DF_ENTREGA");
				tipoCompra 					= (rsConsulta.getString("CG_TIPO_COMPRA")==null)?"":rsConsulta.getString("CG_TIPO_COMPRA");
				clavePresupuestaria = (rsConsulta.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rsConsulta.getString("CG_CLAVE_PRESUPUESTARIA");
				periodo 						= (rsConsulta.getString("CG_PERIODO")==null)?"":rsConsulta.getString("CG_PERIODO");
				lineacd = "";
	
				ig_tipo_piso = (rsConsulta.getString("ig_tipo_piso")==null)?"":rsConsulta.getString("ig_tipo_piso");
				fn_remanente = (rsConsulta.getString("fn_remanente")==null)?"":rsConsulta.getString("fn_remanente");
				fecha_registro_operacion = (rsConsulta.getString("fecha_registro_operacion")==null)?"":rsConsulta.getString("fecha_registro_operacion");//FODEA 005 - 2009 ACF
				if (regCamposAdicionales != null){
					int cont = 0;
					while (regCamposAdicionales.next()){
						if ( cont == 1 ) {
							cgCampo1 = (rsConsulta.getString(16)==null)?"":rsConsulta.getString(16).trim(); lineacd +=","+cgCampo1;
						}
						if (cont == 2) {
							cgCampo2 = (rsConsulta.getString(17)==null)?"":rsConsulta.getString(17).trim(); lineacd +=","+cgCampo2;
						}
						if (cont == 3) {
							cgCampo3 = (rsConsulta.getString(18)==null)?"":rsConsulta.getString(18).trim(); lineacd +=","+cgCampo3;
						}
						if (cont == 4) {
							cgCampo4 = (rsConsulta.getString(19)==null)?"":rsConsulta.getString(19).trim(); lineacd +=","+cgCampo4;
						}
						if (cont == 5) {
							cgCampo5 = (rsConsulta.getString(20)==null)?"":rsConsulta.getString(20).trim(); lineacd +=","+cgCampo5;
						}
						cont ++;
					}
				}
				ct_referencia = (rsConsulta.getString("ct_referencia")==null)?"":rsConsulta.getString("ct_referencia");
				if (rsConsulta.getInt("IC_ESTATUS_DOCTO")==16) {
					rs_fn_porc_docto_aplicado	= (rsConsulta.getString("PORCDOCTOAPLICADO") == null) ? "" : rsConsulta.getString("PORCDOCTOAPLICADO");
					dblMontoPago = rsConsulta.getDouble("FN_MONTO_PAGO");
					dblImporteDepositar = importeRecibir - dblMontoPago;
				} else {
					dblImporteDepositar = 0;
				}
				if(registros == 1) {
					contenidoArchivo.append("Nombre EPO,IF,Num. Documento,Num. Acuse" +
							",Fecha Emision,Fecha Vencimiento,Plazo,Moneda");
					if(bTipoFactoraje) {
						contenidoArchivo.append(",Tipo Factoraje");
					}
					contenidoArchivo.append(",Monto Documento,Porcentaje de descuento" +
							",Monto a descontar,Intereses del documento,Monto a Recibir" +
							",Tasa,Estatus,Fecha Aut. IF,Importe aplicado a crédito" +
							",Porcentaje del documento aplicado,Importe a Depositar a la Pyme");
					if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
						contenidoArchivo.append(",Neto a Recibir Pyme, Beneficiario, % Beneficiario, Importe a Recibir Beneficiario");
					}
					if (regCamposAdicionales != null){
						while (regCamposAdicionales.next()){
							contenidoArchivo.append("," + regCamposAdicionales.getString("NOMBRE_CAMPO").trim());
						}
					}
					contenidoArchivo.append(",Referencia");
					
					// Fodea 002 - 2010 
					if(!ocultarDoctosAplicados){
						contenidoArchivo.append(",Doctos Aplicados a Nota de Credito");
					}

					if(banderaTablaDoctos.equals("1")) {
						contenidoArchivo.append(",Fecha de Recepción de Bienes y Servicios,Tipo de Compra (procedimiento),Clasificador por Objeto del Gasto,Plazo Máximo");
					}
					if(noEstatusDocto.equals("26")){contenidoArchivo.append(",Fecha Registro Operación");}//FODEA 005 - 2009 ACF
	
					contenidoArchivo.append("\n");
				} //registros == 1
	
				dblMontoDescuento = (fnMonto * (fnPorciento / 100));
				nombreTipoFactoraje = (rsConsulta.getString("CS_DSCTO_ESPECIAL")==null)?"":rsConsulta.getString("CS_DSCTO_ESPECIAL");
	
				// Fodea 002 - 2010
				if(!ocultarDoctosAplicados){
					esNotaDeCreditoSimpleAplicada						= ( nombreTipoFactoraje.equals("C") && (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("11")) && BeanParamDscto.existeDocumentoAsociado(icDocumento) )?"S":"N";
					esDocumentoConNotaDeCreditoSimpleAplicada		= ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(icDocumento))?"S":"N";
		
					esNotaDeCreditoAplicada 							= (nombreTipoFactoraje.equals("C") && (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(icDocumento))?"S":"N";
					esDocumentoConNotaDeCreditoMultipleAplicada	= (!nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(icDocumento))?"S":"N";
	
					if(esNotaDeCreditoSimpleAplicada.equals("S")){
						numeroDocumento									= BeanSeleccionDocumento.getNumeroDoctoAsociado(icDocumento); 
					}else if(esDocumentoConNotaDeCreditoSimpleAplicada == ("S")){
						numeroDocumento									= BeanSeleccionDocumento.getNumeroNotaCreditoAsociada(icDocumento);
					}
				}
				fnMontoDscto = dblMontoDescuento;//aqui agregado 15/12/2004
				 if(nombreTipoFactoraje.equals("V")||nombreTipoFactoraje.equals("D")||nombreTipoFactoraje.equals("C")||nombreTipoFactoraje.equals("I")) {//aqui agregado 15/12/2004
					fnMontoDscto = fnMonto;
					  fnPorciento = 100;
				 }
				tipoFactorajeDesc = (rsConsulta.getString("TIPO_FACTORAJE")==null)?"":rsConsulta.getString("TIPO_FACTORAJE");
				nombreTipoFactoraje = tipoFactorajeDesc;

			/***************************/
			/* Generacion del archivo */
			/***************************/
				contenidoArchivo.append("\"" + nombreEpo.replace(',',' ') + "\", " +
						nombreIf.replace(',',' ') + "," + igNumeroDocto + "," +ccAcuse +
						"," + dfFechaDocto + "," + dfFechaVenc + "," + plazo +
						"," + nombreMoneda);
				if(bTipoFactoraje) {
					contenidoArchivo.append("," + nombreTipoFactoraje);
				}
				contenidoArchivo.append("," + fnMonto + "," + fnPorciento +
						"," + fnMontoDscto + "," + importeInteres + "," + importeRecibir +
						"," + inTasaAceptada + "," + estatusDocto +
						"," + fechaSolicitud + "," +dblMontoPago+
						"," + rs_fn_porc_docto_aplicado + ",");
				if(noEstatusDocto.equals("16")){
					if(ig_tipo_piso.equals("1")){
						if(!fn_remanente.equals("")){
							contenidoArchivo.append(fn_remanente);
						}
					}else{
						contenidoArchivo.append(dblImporteDepositar);
					}
				}
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
					contenidoArchivo.append("," + Comunes.formatoDecimal(netoRecibirPyme,2, false) +
							"," + nombreBeneficiario.replaceAll(","," ") + "," + porcBeneficiario +
							"," + Comunes.formatoDecimal(recibirBeneficiario,2, false));
				}
				contenidoArchivo.append(lineacd + "," + ct_referencia );
				// Fodea 002 - 2010 
				if(!ocultarDoctosAplicados){
					contenidoArchivo.append(",");
					if(esNotaDeCreditoAplicada == ("S") || esDocumentoConNotaDeCreditoMultipleAplicada == ("S")){contenidoArchivo.append("Si");}
					if(esNotaDeCreditoSimpleAplicada == ("S") || esDocumentoConNotaDeCreditoSimpleAplicada == ("S") ){contenidoArchivo.append("\""+numeroDocumento+"\"");}
				}
				if(banderaTablaDoctos.equals("1")) {
					contenidoArchivo.append(","+fechaEntrega+","+tipoCompra+","+clavePresupuestaria+","+periodo);
				}
				if(noEstatusDocto.equals("26")){contenidoArchivo.append(","+fecha_registro_operacion);}//FODEA 005 - 2009 ACF
				contenidoArchivo.append("\n");
				val++;
			} // while
		} //if estatus=3,4,8,11,12, 16
	
	if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".csv")) {
		jsonObj.put("success", new Boolean(false));
		jsonObj.put("msg", "Error al generar el archivo");
	} else {
		nombreArchivo = archivo.nombre;
		
		jsonObj.put("success", new Boolean(true));
		jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);
	}

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	if (con.hayConexionAbierta() == true) {
		con.cierraConexionDB();
	}
}
%>
<%=jsonObj%>