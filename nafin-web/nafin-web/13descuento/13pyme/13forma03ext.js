Ext.onReady(function() {

//--------------------------------- HANDLERS -------------------------------

	var procesarSuccessFailureParametrosIniciales = function(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var jsonObj = Ext.util.JSON.decode(response.responseText);
			
			var btnContinuar = Ext.getCmp('btnContinuar');
			btnContinuar.setHandler(function(boton, evt) {
				window.location = jsonObj.url;
			});
			panel.show();

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}

	var obtenerParametrosIniciales = function() {
			Ext.Ajax.request({
			url: '13forma03ext.data.jsp',
			params: {
				informacion: "ParametrosIniciales"
			},
			callback: procesarSuccessFailureParametrosIniciales
		});
	}

//-------------------------------- STORES -----------------------------------

//-------------------------------------------------------------------
	var panel = new Ext.Panel({
		hidden: true,
		height: '300',
		width: 600,
		title: 'Aviso',
		style: 'margin: 0 auto',
		bodyStyle: 'text-align: center',
		frame: true,
		html: Ext.getDom('mensaje').innerHTML,
		bbar: [
			'->',
			{
				xtype: 'tbseparator',
				id: 'tbsInteresado',
				hidden: false
			},
			{
				xtype: 'button',
				id: 'btnContinuar',
				text: 'Continuar',
				hidden: false
			}
		]
	});

//-------------------------------- PRINCIPAL -----------------------------------

//Dado que la aplicación se pretende mostrarse en un div de cierto ancho, se simula con el siguiente contenedor:
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 700,
		items: [
			panel
		]
	});

//-------------------------------- ----------------- -----------------------------------
	obtenerParametrosIniciales();
});