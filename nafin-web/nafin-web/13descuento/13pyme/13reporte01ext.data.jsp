<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.sql.*,
		netropology.utilerias.*,com.netro.descuento.*,
		com.netro.descuento.RepDoctosPymeDEbean,
		com.netro.model.catalogos.CatalogoMandante,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String sOperaFactConMandato = "";
String sFechaVencPyme = "";
String infoRegresar = "";
boolean bOperaFactConMandato					= false;
boolean bOperaFactorajeVencido				= false;
boolean bOperaFactorajeDistribuido			= false;
boolean bOperaFactorajeNotaCredito			= false;
boolean bOperaFactVencimientoInfonavit		= false;
boolean bTipoFactoraje							= false;
boolean mostrarOpcionPignorado=false;
boolean bOperaFactorajeIF=false;

if(iNoEPO != null && !iNoEPO.equals("")) {
	ISeleccionDocumento beanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
	sFechaVencPyme = beanSeleccionDocumento.operaFechaVencPyme(iNoEPO);
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	Hashtable alParamEPO = new Hashtable();
	alParamEPO = BeanParamDscto.getParametrosEPO(iNoEPO, 1);
	if (alParamEPO!=null) {
		bOperaFactConMandato				= ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		bOperaFactorajeVencido			= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
		bOperaFactorajeDistribuido		= ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
		bOperaFactorajeNotaCredito		= ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
		bOperaFactVencimientoInfonavit= ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString())) ? false : true ;
		bOperaFactorajeIF= ("N".equals(alParamEPO.get("FACTORAJE_IF").toString())) ? false : true ;
	}
	sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
	mostrarOpcionPignorado = BeanParamDscto.mostrarOpcionPignorado(iNoEPO, iNoCliente);	
	
}

if (informacion.equals("valoresIniciales")) {
	bTipoFactoraje =    (  bOperaFactorajeIF ||  bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaCredito || bOperaFactConMandato || bOperaFactVencimientoInfonavit)?true:false;
	java.text.SimpleDateFormat formatoHora2 = new java.text.SimpleDateFormat ("dd/MM/yyyy; hh:mm:ss a"); 
	String hoyFecha = (formatoHora2.format(new java.util.Date()));
	bOperaFactorajeVencido = true;
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("Usuario", strNombreUsuario);
	jsonObj.put("Fecha", hoyFecha);
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bOperaFactConMandato", new Boolean(bOperaFactConMandato));	
	jsonObj.put("mostrarOpcionPignorado", new Boolean(mostrarOpcionPignorado));	
	jsonObj.put("bOperaFactorajeIF", new Boolean(bOperaFactorajeIF));
	infoRegresar = jsonObj.toString();
	
} else if (informacion.equals("consultaGridNegociable")) {
	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepDoctosPymeDEbean repNegociable = new RepDoctosPymeDEbean();
		repNegociable.setIcestatusdocto("2");
		repNegociable.setIcepo(iNoEPO);
		repNegociable.setIcePyme(iNoCliente);
		repNegociable.setFechaVencPyme(sFechaVencPyme);
		repNegociable.setOperaFactConMandato(sOperaFactConMandato);
		repNegociable.setQrysentencia("");
		Registros registros = repNegociable.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("consultaGridPignorado")) {

   if(iNoEPO != null	&&	!iNoEPO.equals("")){
		if (mostrarOpcionPignorado)	{
			RepDoctosPymeDEbean repPignorado = new RepDoctosPymeDEbean();
			repPignorado.setIcestatusdocto("23");	//23
			repPignorado.setIcepo(iNoEPO);
			repPignorado.setIcePyme(iNoCliente);
			repPignorado.setQrysentencia("");
			repPignorado.setFechaVencPyme(sFechaVencPyme);
			Registros registros = repPignorado.executeQuery();
			infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
		}else {
			infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] " + "}";
		}
	}

}else if (informacion.equals("consultaGridSelecPyme")) {

	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepDoctosPymeDEbean repSelecPyme = new RepDoctosPymeDEbean();
		repSelecPyme.setIcestatusdocto("3");	// == 3
		repSelecPyme.setIcepo(iNoEPO);
		repSelecPyme.setIcePyme(iNoCliente);
		repSelecPyme.setFechaVencPyme(sFechaVencPyme);
		repSelecPyme.setOperaFactConMandato(sOperaFactConMandato);
		repSelecPyme.setQrysentencia("");
		Registros registros = repSelecPyme.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("consultaGridAutorIF")) {

	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepDoctosPymeDEbean repAutorIF = new RepDoctosPymeDEbean();
		repAutorIF.setIcestatusdocto("24");	//24
		repAutorIF.setIcepo(iNoEPO);
		repAutorIF.setIcePyme(iNoCliente);
		repAutorIF.setFechaVencPyme(sFechaVencPyme);
		repAutorIF.setOperaFactConMandato(sOperaFactConMandato);
		repAutorIF.setQrysentencia("");
		Registros registros = repAutorIF.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if (informacion.equals("consultaGridOperada")) {

	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepDoctosPymeDEbean repOperada = new RepDoctosPymeDEbean();
		repOperada.setIcestatusdocto("4");		//4
		repOperada.setIcepo(iNoEPO);
		repOperada.setOperafactoraje(bOperaFactorajeNotaCredito);
		repOperada.setIcePyme(iNoCliente);
		repOperada.setFechaVencPyme(sFechaVencPyme);
		repOperada.setOperaFactConMandato(sOperaFactConMandato);
		repOperada.setQrysentencia("");
		Registros registros = repOperada.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}

}else if(informacion.equals("consultaGridAcredito")){

	if(iNoEPO != null && !iNoEPO.equals("")) {
		RepDoctosPymeDEbean repAcredito = new RepDoctosPymeDEbean();
		repAcredito.setIcestatusdocto("16");	//16
		repAcredito.setIcepo(iNoEPO);
		repAcredito.setIcePyme(iNoCliente);
		repAcredito.setFechaVencPyme(sFechaVencPyme);
		repAcredito.setOperaFactConMandato(sOperaFactConMandato);
		repAcredito.setQrysentencia("");
		Registros registros = repAcredito.executeQuery();
		infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
	}else {
		infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
	}
		
}else if (informacion.equals("consultaGridProgramado")) {

		if(iNoEPO != null && !iNoEPO.equals("")) {
			RepDoctosPymeDEbean repProgram = new RepDoctosPymeDEbean();
			repProgram.setIcestatusdocto("26");	//26
			repProgram.setIcepo(iNoEPO);
			repProgram.setIcePyme(iNoCliente);
			repProgram.setFechaVencPyme(sFechaVencPyme);
			repProgram.setOperaFactConMandato(sOperaFactConMandato);
			repProgram.setQrysentencia("");
			Registros registros = repProgram.executeQuery();
			infoRegresar = "{\"success\": true, \"total\": " + registros.getNumeroRegistros() + ", \"registros\": " + registros.getJSONData() + "}";
		}else {
			infoRegresar = "{\"success\": true, \"total\": 0 , \"registros\": [] }";
		}
		
}
%>
<%=infoRegresar%>