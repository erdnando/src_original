<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		netropology.utilerias.*,com.netro.descuento.*,netropology.utilerias.usuarios.*,
		net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

if (informacion.equals("valoresIniciales")) {

	JSONObject jsonObj = new JSONObject();
	ContratoEpo contrato = BeanParamDscto.getDatosUltimoClausulado(iNoEPO);
	ContratoEpoArchivo contratoArchivo = contrato.getContratoArchivo();
	
	String nombreArchivo =	contratoArchivo.getArchivoContratoEpo(strDirectorioTemp);
	String extension		=	contratoArchivo.getExtension().toLowerCase();
	String tituloAviso	=	contrato.getTituloAviso();
	String contenido		=	Comunes.reemplaza(contrato.getContenidoAviso(), "\r\n", "<br>");
	String botonAviso		=	contrato.getBotonAviso();
	String consecutivo 	=	contrato.getConsecutivo();
	String textCheck		=	contrato.getTextoAceptacion();
	String botonAcepta	=	contrato.getBotonAceptacion();

	jsonObj.put("nombreArchivo", nombreArchivo);
	jsonObj.put("extension", extension);
	jsonObj.put("tituloAviso", tituloAviso);
	jsonObj.put("contenido", contenido);
	jsonObj.put("botonAviso", botonAviso);
	jsonObj.put("consecutivo", consecutivo);
	jsonObj.put("textCheck", textCheck);
	jsonObj.put("botonAcepta", botonAcepta);
	jsonObj.put("strDirecVirtualTemp", strDirecVirtualTemp);
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
} else if(informacion.equals("confirmaClave")){

	String cgLogin = (request.getParameter("cesionUser")!=null)?request.getParameter("cesionUser"):"";
	String cgPassword = (request.getParameter("cesionPassword")!=null)?request.getParameter("cesionPassword"):"";
	String consecutivo =	(request.getParameter("consecutivo")!=null)?request.getParameter("consecutivo"):"";
	String resultado = "";
	UtilUsr utils = new UtilUsr();

	if (!iNoUsuario.equals(cgLogin) || !utils.esUsuarioValido(cgLogin, cgPassword)) {
		resultado = "N";
	} else {
		ContratoEpoArchivo contratoArchivo = new ContratoEpoArchivo();
		resultado = contratoArchivo.insertaContrato("EPO",iNoCliente,iNoEPO,consecutivo,iNoUsuario);
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", resultado);
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("Continuar")) {
	List pantallasNavegacionComplementaria = (List)session.getAttribute("inicializar.PantallasComplementarias");
	pantallasNavegacionComplementaria.remove("/13descuento/13pyme/contrato_ext.jsp");  //El nombre debe coincidir con el especificado en Navegacion

	infoRegresar = "{\"success\": true, \"urlPagina\": \"" + appWebContextRoot + pantallasNavegacionComplementaria.get(0) + "\"}";

}else if (informacion.equals("Archivo")) {
	
	String nombreArchivo = (request.getParameter("nombreArchivo")==null)?"":request.getParameter("nombreArchivo");	
	JSONObject 	jsonObj	= new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);	
	infoRegresar=jsonObj.toString();	

}
%>
<%=infoRegresar%>