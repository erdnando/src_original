<%@ page contentType="application/json;charset=UTF-8"
import="java.sql.*,
	java.text.*,
	java.util.*,
	netropology.utilerias.*,
	com.netro.descuento.*,
	com.netro.pdf.*, net.sf.json.JSONObject"
errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/13descuento/13secsession.jspf" %>
<%
String icEpo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
String icCambioEstatus = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
String tipoFactoraje = (request.getParameter("tipoFactoraje") ==null) ? "" : request.getParameter("tipoFactoraje");

String strEpo = "", strNumDocto = "", strFechaEmision = "", strFechaVencimiento = "";
String strFechaCambio = "", strClaveMoneda = "", strMoneda = "";
String nombreBeneficiario = "", porcBeneficiario = "", recibirBeneficiario = "";
double dblMonto = 0, dblPorcentaje = 0, dblMontoDescuento = 0;
String strEstatus = "", strTipoEstatus = "", strCausa = "", nombreTipoFactoraje="", tipoFactorajeDesc = "", nombreIf="";
boolean bOperaFactorajeVencido = false; 	
boolean bOperaFactorajeVencidoGral = false;
boolean bOperaFactVencimientoInfonavit = false;
boolean bOperaFactorajeDistribuido = false;
boolean bOperaFactorajeDistribuidoGral = false;
boolean bOperaNotasDeCredito = false; 	
boolean bOperaNotasDeCreditoGral = false;
boolean bOperaFactConMandato = false;
boolean bTipoFactoraje = false;
int colspanTF=0, colspanFV=0, colspanFD=0;
double montoTotalMN=0, montoTotalDL=0, montoTotalDsctoMN=0, montoTotalDsctoDL=0;
int numeroTotalDocsMN=0, numeroTotalDocsDols=0, i=0;
String sOperaFactConMandato = "";

AccesoDB con = new AccesoDB();
CQueryHelperExtJS	queryHelper = null;
JSONObject jsonObj = new JSONObject();
String nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";

try {
	con.conexionDB();
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

	sOperaFactConMandato 		= new String("");

	Hashtable alParamEPO = new Hashtable(); 
	//System.out.println("«««« icEpo ««»» "+icEpo+" »»»»");
	if(!icEpo.equals("")) {
		alParamEPO = BeanParamDscto.getParametrosEPO(icEpo, 1);
		if (alParamEPO!=null) {
		bOperaFactConMandato   		  = ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		bOperaFactorajeVencido 		  = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
		bOperaFactorajeDistribuido   = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
		bOperaNotasDeCredito 		  = ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
		bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString())) ? false : true ;
		}
	}
	bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaNotasDeCredito || bOperaFactConMandato || bOperaFactVencimientoInfonavit)?true:false;
	sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
	//System.out.println("«««« sOperaFactConMandato -:generar archivo:- «» "+ sOperaFactConMandato+" »»»»");
	
	queryHelper = new CQueryHelperExtJS(new CambioEstatusPymeDE());
 //	queryHelper.cleanSession(request);

	String selected = "";
	String qrySentencia= 
		" SELECT COUNT (1)"   +
		"   FROM comrel_pyme_epo pe, comrel_producto_epo e"   +
		"  WHERE ic_pyme = "+iNoCliente+
		"    AND pe.ic_epo = e.ic_epo"   +
		"    AND e.ic_producto_nafin = 1"   +
		"    AND cs_factoraje_vencido = 'S'"   +
		" UNION ALL"   +
		" SELECT COUNT (1)"   +
		"   FROM comrel_pyme_epo pe, comrel_producto_epo e"   +
		"  WHERE ic_pyme = "+iNoCliente+ 
		"    AND pe.ic_epo = e.ic_epo"   +
		"    AND e.ic_producto_nafin = 1"   +
		"    AND cs_factoraje_distribuido = 'S'"  +
		" UNION ALL"   +
		" SELECT COUNT (1)"   +
		"   FROM comrel_pyme_epo pe, comrel_producto_epo e"+
		"  WHERE ic_pyme = "+iNoCliente+
		"    AND pe.ic_epo = e.ic_epo "   +
		"    AND e.ic_producto_nafin = 1"   +
		"    AND cs_opera_notas_cred = 'S'";
	
	ResultSet rs = con.queryDB(qrySentencia);
	if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeVencidoGral = true; }
	if (rs.next()) { if(rs.getInt(1)>0) bOperaFactorajeDistribuidoGral = true; }
	if (rs.next()) { if(rs.getInt(1)>0) bOperaNotasDeCreditoGral = true; }
	con.cierraStatement();
	
    if(bOperaFactorajeVencidoGral || bOperaFactorajeDistribuidoGral || bOperaNotasDeCreditoGral) {
        bOperaFactorajeVencido = false;
			bOperaFactorajeDistribuido = false;
        if(icEpo.equals("")) {
			if (bOperaFactorajeVencidoGral)		bOperaFactorajeVencido = true;
			if (bOperaFactorajeDistribuidoGral)	bOperaFactorajeDistribuido = true;
			if (bOperaNotasDeCreditoGral)		bOperaNotasDeCredito = true;
		} 
	}

	/***************************/
	/* Generacion del archivo */
	/***************************/
	int start = 0;
	int limit = 0;
	int nRow = 0;
	Registros reg = new Registros();
	
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	reg = queryHelper.getPageResultSet(request,start,limit);

	ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);

	
	
	while (reg.next()) {
		if(nRow == 0){
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),strDirectorioPublicacion);	
			
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			List lEncabezados = new ArrayList();
			lEncabezados.add("EPO");
			lEncabezados.add("Num. de Documento");
			lEncabezados.add("Fecha Emisión");
			lEncabezados.add("Fecha Vencimiento");
			if (icCambioEstatus.equals("29")) {
				lEncabezados.add("Nombre IF");
			}
			lEncabezados.add("Fecha Cambio de Estatus");
			lEncabezados.add("Moneda");
			if(bTipoFactoraje) {
				lEncabezados.add("Tipo Factoraje");
			}
			lEncabezados.add("Monto");
			lEncabezados.add("Porcentaje de Descuento");
			lEncabezados.add("Monto a Descontar");
			lEncabezados.add("Tipo Cambio de Estatus");
			lEncabezados.add("Causa");
			if(!icCambioEstatus.equals("29")){  //NO ES 29
				lEncabezados.add("Monto Anterior");
				if(bOperaFactorajeVencido) {/* Desplegar solo si opera "Factoraje Vencido" */ 
					lEncabezados.add("Nombre IF");
				}
			
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { /* Desplegar solo si opera "Factoraje Distribuido" ó "Factoraje Vencimiento Infonavit"*/ 
					lEncabezados.add("Beneficiario");
					lEncabezados.add("% Beneficiario");
					lEncabezados.add("Importe a Recibir Beneficiario");
				}
			}
			if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
				lEncabezados.add("Mandante");								
			}
			pdfDoc.setTable(lEncabezados, "formasmenB", 100);
		}

		strEpo = reg.getString(1).trim();
		strNumDocto = reg.getString(2).trim();
		strFechaEmision = reg.getString(4).trim();
		strFechaVencimiento = (reg.getString(3)==null)?"":reg.getString(3).trim();
		strFechaCambio = reg.getString(5).trim();
		strClaveMoneda = reg.getString(6).trim();
		strMoneda = reg.getString(7).trim();
		strEstatus = reg.getString(13);
		//dblMonto = reg.getDouble(8);
		if(strEstatus.equals("28")) //aplicacion de nota de credito
			dblMonto=Double.parseDouble(reg.getString("fn_monto_nuevo"));
		else
			dblMonto = Double.parseDouble(reg.getString(8));
		
		nombreBeneficiario = (reg.getString("NOMBRE_BENEFICIARIO")==null)?"":reg.getString("NOMBRE_BENEFICIARIO");
		porcBeneficiario = (reg.getString("FN_PORC_BENEFICIARIO")==null)?"":reg.getString("FN_PORC_BENEFICIARIO");
		recibirBeneficiario = (reg.getString("RECIBIR_BENEFICIARIO")==null)?"":reg.getString("RECIBIR_BENEFICIARIO");
		String smandant = "";
		if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
			smandant = (reg.getString("mandante")==null)?"":reg.getString("mandante");
		}
		if (strEstatus.equals("4") || strEstatus.equals("5") || strEstatus.equals("6") || strEstatus.equals("8")|| strEstatus.equals("28")){
			dblPorcentaje = Double.parseDouble(reg.getString(12));
			dblMontoDescuento = dblMonto * (dblPorcentaje / 100);
		}else{
			dblPorcentaje = Double.parseDouble(reg.getString(15));
			dblMontoDescuento = Double.parseDouble(reg.getString(9));
		}
		
		strTipoEstatus = reg.getString(10).trim();
		strCausa = (reg.getString(11)==null)?"":reg.getString(11);
		
		nombreTipoFactoraje = (reg.getString("CS_DSCTO_ESPECIAL")==null)?"":reg.getString("CS_DSCTO_ESPECIAL");
		nombreIf = "";
		if(nombreTipoFactoraje.equals("V") || nombreTipoFactoraje.equals("D") || nombreTipoFactoraje.equals("I")) {
			nombreIf = (reg.getString("NOMBREIF")==null)?"":reg.getString("NOMBREIF").trim();
			dblMontoDescuento = dblMonto;
			dblPorcentaje = 100;
		}
		
		if(strEstatus.equals("29")){//29=Programado a Negociable
			nombreIf = (reg.getString("NOMBREIF")==null)?"":reg.getString("NOMBREIF").trim();
			dblPorcentaje = Double.parseDouble(reg.getString(12));
			dblMontoDescuento = dblMonto * (dblPorcentaje / 100);
		}
		
		tipoFactorajeDesc = (reg.getString("TIPO_FACTORAJE")==null)?"":reg.getString("TIPO_FACTORAJE");
		nombreTipoFactoraje = tipoFactorajeDesc;

	 /*************************/
	 /* Contenido del archivo */
	 /**************************/
	
		if(strEstatus.equals("8")||strEstatus.equals("28")) { /* estatus "Modificacion de Montos"  y Aplicacion de nota de credito*/
			pdfDoc.setCell(strEpo, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strNumDocto, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strFechaEmision, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strFechaVencimiento, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strFechaCambio, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strMoneda, "formasmen", ComunesPDF.CENTER);
			if(bTipoFactoraje) {
				pdfDoc.setCell(nombreTipoFactoraje, "formasmen", ComunesPDF.CENTER);
			}
			pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMonto,2), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(dblPorcentaje,0)+ "%", "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strTipoEstatus+ ((strEstatus.equals("28"))?" " + strFechaCambio:""), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strCausa, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell("$" + Comunes.formatoDecimal(reg.getString(14),2),"formasmen", ComunesPDF.CENTER);
			if(bOperaFactorajeVencido) {/* Desplegar solo si opera "Factoraje Vencido" */
				pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
			}
			if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { /* Desplegar solo si opera "Factoraje Distribuido" ó "Factoraje Vencimiento Infonavit" */
				pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2) + "%", "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
			}
	
		} else {
	
			pdfDoc.setCell(strEpo, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strNumDocto, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strFechaEmision, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strFechaVencimiento, "formasmen", ComunesPDF.CENTER);
			if(strEstatus.equals("29")) {
				pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
			}
			pdfDoc.setCell(strFechaCambio, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strMoneda, "formasmen", ComunesPDF.CENTER);
			if(bTipoFactoraje) { /* Desplegar si opera algun Factoraje especial */
				pdfDoc.setCell(nombreTipoFactoraje, "formasmen", ComunesPDF.CENTER);
			}
			pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMonto,2), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(dblPorcentaje,0) + "%", "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strTipoEstatus, "formasmen", ComunesPDF.CENTER);
			pdfDoc.setCell(strCausa, "formasmen", ComunesPDF.CENTER);
	
			if(!strEstatus.equals("29")){
				pdfDoc.setCell("$" + Comunes.formatoDecimal(reg.getString(14),2),"formasmen", ComunesPDF.CENTER);
				if(bOperaFactorajeVencido) {/* Desplegar solo si opera "Factoraje Vencido" */
					pdfDoc.setCell(nombreIf, "formasmen", ComunesPDF.CENTER);
				}
				
				if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { /* Desplegar solo si opera "Factoraje Distribuido" ó "Factoraje Vencimiento Infonavit" */
					pdfDoc.setCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2)+"%", "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
				}
			}
		}
		
		if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
			pdfDoc.setCell(smandant, "formasmen", ComunesPDF.CENTER);
		}
		nRow++;
	} //fin del while
	
	con.cierraStatement();
	
	pdfDoc.addTable();
	pdfDoc.endDocument();
	
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("urlArchivo", strDirecVirtualTemp+nombreArchivo);

} catch (Exception e) {
	throw new AppException("Error al generar el archivo", e);
} finally {
	if (con.hayConexionAbierta() == true) {
		con.cierraConexionDB();
	}
}
%>
<%=jsonObj%>