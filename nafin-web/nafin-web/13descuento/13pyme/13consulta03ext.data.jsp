<%@ page contentType="application/json;charset=UTF-8"
	import="
		javax.naming.*,
		java.util.*,
		java.sql.*,
		netropology.utilerias.*,
		com.netro.descuento.*,
		net.sf.json.JSONArray,
		net.sf.json.JSONObject,	
		com.netro.model.catalogos.CatalogoEPOPyme"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
//com.netro.etiquetas.CatalogoEPO, hay que poner esta clase
System.out.println("13consulta03ext.data.jsp (E)"); 
String informacion = request.getParameter("informacion") == null?"":(String)request.getParameter("informacion");
String noEpo = request.getParameter("no_epo") == null?"":(String)request.getParameter("no_epo");
String noPyme = iNoCliente; 
String infoRegresar = "";

AccesoDB con = new AccesoDB();


try {
	con.conexionDB();

if (informacion.equals("CatalogoEPO") ) {
 
	CatalogoEPOPyme cat = new CatalogoEPOPyme();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClavePyme(noPyme);
	infoRegresar = cat.getJSONElementos();	
	
		
} else if (informacion.equals("Consulta") && !noPyme.equals("") && !noEpo.equals("")) {
	
		ResultSet rs = null;	
		String noIfs ="";
		String moneda ="1,54";
		
		AutorizacionTasas tasa = ServiceLocator.getInstance().lookup("AutorizacionTasasEJB", AutorizacionTasas.class);
		
		JSONObject 	resultado	= new JSONObject();
			
		//se obtiene los IF's relacionados con la Pyme y EPO
		noIfs  = tasa.IfparaTasas( noEpo, noPyme);	
	
		String consulta = tasa.getTasaAutIFEXT(noEpo, noIfs, moneda);
		
		resultado = JSONObject.fromObject(consulta);
			// obtiene  la parametrizacion de la EPo
		int valorfven =tasa.operaFactorajeVencido(noEpo);
		int count = 0;
		if(valorfven>0) {
			count = valorfven;
			if(count>0) {
			resultado.put("VALOR","S");
			}else{
			resultado.put("VALOR","N");
			}
		}else{
			resultado.put("VALOR","N");
		}
				
		infoRegresar = resultado.toString();
	
}

} catch(Exception e) { 
	out.println(e.getMessage()); } 
	finally { 
		if (con.hayConexionAbierta()) {
				con.cierraConexionDB();	
		}
	}
%>

<%=infoRegresar%>


<%System.out.println("13consulta03ext.data.jsp (S)");%>


