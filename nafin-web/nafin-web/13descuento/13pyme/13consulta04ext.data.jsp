<%@ page language="java" %>   
<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,java.sql.*,
		netropology.utilerias.*,com.netro.descuento.*,
		com.netro.model.catalogos.CatalogoEPOPyme,
		com.netro.model.catalogos.CatalogoMoneda,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMandante,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%--@ include file="/13descuento/13secsession_extjs.jspf" --%>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar = "";
iNoCliente = (String)session.getAttribute("iNoCliente");
  
System.out.println("iNoCliente " + iNoCliente);

if (informacion.equals("CatalogoEPOPyme")) {
   System.out.println("CatalogoEPOPyme : " + iNoCliente);
	CatalogoEPOPyme cat = new CatalogoEPOPyme();
	cat.setCampoClave("ic_epo");
	cat.setCampoDescripcion("cg_razon_social"); 
	cat.setClavePyme(iNoCliente);	//iNoCliente viene de sesion.
	cat.setAceptacion("H");
	cat.setOrden("cg_razon_social");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("CatalogoMoneda")) {
	CatalogoMoneda cat = new CatalogoMoneda();
	cat.setCampoClave("ic_moneda");
	cat.setCampoDescripcion("cd_nombre");
	infoRegresar = cat.getJSONElementos();
  
}else if (informacion.equals("CatalogoCambioEstatus")) {

	CatalogoSimple cat = new CatalogoSimple();
	cat.setCampoClave("ic_cambio_estatus");
	cat.setCampoDescripcion("cd_descripcion");
	cat.setTabla("comcat_cambio_estatus");
	cat.setValoresCondicionIn("2,4,5,6,8,23,28,29", Integer.class);
	cat.setOrden("ic_cambio_estatus");
	infoRegresar = cat.getJSONElementos();
  
}else if (informacion.equals("CatalogoFactoraje")) {
	
	String claveEpo = (request.getParameter("claveEpo")!=null)?request.getParameter("claveEpo"):"";
	
	try {
		if (claveEpo.equals("")) {
			throw new Exception("Los parametros son requeridos");
		}
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	Hashtable alParamEPO = new Hashtable();
	alParamEPO = BeanParamDscto.getParametrosEPO(claveEpo, 1);
	//alParamEPO = BeanParamDscto.getParametrosEPO(claveEpo, Integer.parseInt(claveEpo));
	boolean bOperaFactorajeVencido = false;
   boolean bOperaFactorajeAutomatico = false; // FODEA 026-2012
	boolean bOperaFactVencimientoInfonavit = false;
	boolean bOperaFactorajeDistribuido = false;
	boolean bOperaNotasDeCredito = false;
	boolean bOperaFactConMandato = false;
	boolean bTipoFactoraje = false;
	if (alParamEPO!=null) {
		bOperaFactConMandato   		  = ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		bOperaFactorajeVencido 		  = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
      bOperaFactorajeAutomatico    = ("N".equals(alParamEPO.get("FACTORAJE_IF").toString()))?false:true; // FODEA 026-2012: Agregar Tipo Factoraje Autom�tico
		bOperaFactorajeDistribuido   = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
		bOperaNotasDeCredito 		  = ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
		bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString())) ? false : true ;
	}
	bTipoFactoraje = (bOperaFactConMandato || bOperaFactorajeVencido || bOperaFactorajeAutomatico || bOperaFactorajeDistribuido || bOperaNotasDeCredito || bOperaFactVencimientoInfonavit)?true:false;

	if (bTipoFactoraje) {
		StringBuffer tipoFactCombo = new StringBuffer();
		tipoFactCombo.append("N,V,D,A"); // estos tipos de factoraje aparecen por default en el combo (NORMAL, VENCIDO, DISTRIBUIDO, AUTOMATICO)
		if(bOperaNotasDeCredito){
			tipoFactCombo.append(",C");
		}
		if(bOperaFactConMandato){
			tipoFactCombo.append(",M");
		}
		if(bOperaFactVencimientoInfonavit){
			tipoFactCombo.append(",I");
		}
		
		CatalogoSimple cat = new CatalogoSimple();
		cat.setCampoClave("cc_tipo_factoraje");
		cat.setCampoDescripcion("cg_nombre");
		cat.setTabla("comcat_tipo_factoraje");
		cat.setValoresCondicionIn(tipoFactCombo.toString(), String.class);
		cat.setOrden("cc_tipo_factoraje");
		infoRegresar = cat.getJSONElementos();
		//infoRegresar =  + ",{\"bOperaFactorajeDistribuido\":" + bOperaFactorajeDistribuido + ",\"bOperaFactorajeVencido\": " + bOperaFactorajeVencido +", \"bOperaNotasDeCredito\": " + bOperaNotasDeCredito + "}";
		//JSONObject jsonObj = JSONObject.fromObject(cat.getJSONElementos());
		//jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
		//jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
		//jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
		//infoRegresar = jsonObj.toString();
		System.out.println(infoRegresar);
		
	} else {
		infoRegresar = "{\"success\": true, \"total\": 0, \"registros\": [], \"noAplica\": true }";
	}
//Thread.sleep(15*1000);
}else if(informacion.equals("CatalogoMandante")){

	String claveMandante = (request.getParameter("claveMandante")!=null)?request.getParameter("claveMandante"):"";
	if (claveMandante != null && !claveMandante.equals("")){
		CatalogoMandante cat = new CatalogoMandante();
		cat.setCampoClave("ic_mandante");
		cat.setCampoDescripcion("cg_razon_social");
		cat.setTipoAfiliado("M");
		cat.setOrden("cg_razon_social");
		infoRegresar = cat.getJSONElementos();
	}
}else if (informacion.equals("Consulta")) {		//Datos para la Consulta con Paginacion

	int start = 0;
	int limit = 0;

	String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
	try {
		start = Integer.parseInt(request.getParameter("start"));
		limit = Integer.parseInt(request.getParameter("limit"));
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}
	
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new com.netro.descuento.CambioEstatusPymeDE());
	
	try {
		if (operacion.equals("Generar")) {	//Nueva consulta
			queryHelper.executePKQuery(request); //Obtiene las llaves primarias con base a los parametros de consulta
		}
		
		infoRegresar = queryHelper.getJSONPageResultSet(request,start,limit);
		
	} catch(Exception e) {	
		throw new AppException("Error en la paginacion", e);
	}
} else if (informacion.equals("ResumenTotales")) {		//Datos para el Resumen de Totales
	//Debe ser llamado despues de Consulta
	CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(null); //No se requiere especificar clase de paginador para obtener totales por que se obtienen de sesion
	infoRegresar  = queryHelper.getJSONResultCount(request);	//los saca de sesion
} else if (informacion.equals("Parametrizacion")) {

	String claveEpo = (request.getParameter("claveEpo")!=null)?request.getParameter("claveEpo"):"";
	try {
		if (claveEpo.equals("")) {
			throw new Exception("Los parametros son requeridos");
		}
	} catch(Exception e) {
		throw new AppException("Error en los parametros recibidos", e);
	}

	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
	
	Hashtable alParamEPO = new Hashtable();
	//alParamEPO = BeanParamDscto.getParametrosEPO(claveEpo, Integer.parseInt(claveEpo));
	alParamEPO = BeanParamDscto.getParametrosEPO(claveEpo, 1);
	boolean bOperaFactorajeVencido = false;
	boolean bOperaFactorajeVencidoGral = false;
   boolean bOperaFactorajeAutomatico = false; // FODEA 026-2012
   boolean bOperaFactorajeAutomaticoGral = false; // FODEA 026-2012
	boolean bOperaFactVencimientoInfonavit = false;
	boolean bOperaFactorajeDistribuido = false;
	boolean bOperaFactorajeDistribuidoGral = false;
	boolean bTipoFactoraje = false;
	boolean bOperaFactConMandato = false;
	boolean bOperaNotasDeCredito = false;
	boolean bOperaNotasDeCreditoGral = false;    

	if (alParamEPO!=null) {
		bOperaFactConMandato   		  = ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
		bOperaFactorajeVencido 		  = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
      bOperaFactorajeAutomatico    = ("N".equals(alParamEPO.get("FACTORAJE_IF").toString()))?false:true; // FODEA 026-2012
		bOperaFactorajeDistribuido   = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
		bOperaNotasDeCredito 		  = ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
		bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString())) ? false : true ;
	}
	bTipoFactoraje = (bOperaFactConMandato || bOperaFactorajeVencido || bOperaFactorajeAutomatico || bOperaFactorajeDistribuido || bOperaNotasDeCredito || bOperaFactVencimientoInfonavit)?true:false;
	
	String sFechaVencPyme = "";
   int ino_cliente = Integer.parseInt(iNoCliente);
	sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(claveEpo);
	
	bOperaFactorajeVencidoGral = BeanParamDscto.getOperaFactorajeVencidoGral(ino_cliente);
   //bOperaFactorajeAutomaticoGral = BeanParamDscto.getOperaFactorajeAutomaticoGral(ino_cliente);
   
   // Reemplazo del m�todo < bOperaFactorajeAutomaticoGral = BeanParamDscto.getOperaFactorajeAutomaticoGral(ino_cliente) >   
   boolean factorajeAutomaticoGral = false;
   StringBuffer qrySentencia;     
   PreparedStatement	ps	= null;
   ResultSet rs	= null;
   AccesoDB con = new AccesoDB();
   try {
      con.conexionDB();    
      qrySentencia = new StringBuffer();
      qrySentencia.append(" SELECT COUNT (1)"   +
            "  FROM comrel_pyme_epo pe, comrel_producto_epo e"   +
            "	WHERE ic_pyme = ?  AND pe.ic_epo = e.ic_epo"   +
            "  AND e.ic_producto_nafin = 1"   +
            "  AND cs_factoraje_if = 'S'");
            
      ps = con.queryPrecompilado(qrySentencia.toString());
      ps.setInt(1, Integer.parseInt(iNoCliente));
      rs = ps.executeQuery();
      ps.clearParameters();
      if (rs.next()) { if(rs.getInt(1)>0) factorajeAutomaticoGral = true; }
      rs.close();
      ps.close();
   } catch (Exception e) {
      throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo.", e);
   } finally {
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();  
      }
   }
   bOperaFactorajeAutomaticoGral = factorajeAutomaticoGral;
   // Termina  bOperaFactorajeAutomaticoGral = BeanParamDscto.getOperaFactorajeAutomaticoGral(ino_cliente); 

	bOperaFactorajeDistribuidoGral = BeanParamDscto.getOperaFactorajeDistribuidoGral(ino_cliente);
	bOperaNotasDeCreditoGral = BeanParamDscto.getOperaNotasCreditoGral(ino_cliente);
	
	if(bOperaFactorajeAutomaticoGral || bOperaFactorajeVencidoGral || bOperaFactorajeDistribuidoGral || bOperaNotasDeCreditoGral) {
		bOperaFactorajeVencido = false;
      bOperaFactorajeAutomatico = false;   
		bOperaFactorajeDistribuido = false;
		bOperaNotasDeCredito = false;
		if(claveEpo.equals("")) {
			if (bOperaFactorajeVencidoGral)		bOperaFactorajeVencido = true;
			if (bOperaFactorajeAutomaticoGral)		bOperaFactorajeAutomatico = true; // fodea 026-2012
			if (bOperaFactorajeDistribuidoGral)	bOperaFactorajeDistribuido = true;
			if (bOperaNotasDeCreditoGral)		bOperaNotasDeCredito = true;
		}
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("sFechaVencPyme", sFechaVencPyme);
	jsonObj.put("bOperaFactorajeDistribuido", new Boolean(bOperaFactorajeDistribuido));
	jsonObj.put("bOperaFactorajeVencido", new Boolean(bOperaFactorajeVencido));
   jsonObj.put("bOperaFactorajeAutomatico" , new Boolean(bOperaFactorajeAutomatico));
	jsonObj.put("bOperaFactVencimientoInfonavit", new Boolean(bOperaFactVencimientoInfonavit));
	jsonObj.put("bTipoFactoraje", new Boolean(bTipoFactoraje));

	infoRegresar = jsonObj.toString();
//Thread.sleep(15*1000);
}

%>
<%=infoRegresar%>



