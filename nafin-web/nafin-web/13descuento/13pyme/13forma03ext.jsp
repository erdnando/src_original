<!DOCTYPE html>
<%@ page import="java.util.*" contentType="text/html;charset=windows-1252"	errorPage="/00utils/error.jsp"%>
<%@ include file="/appComun.jspf" %>

<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<%@ include file="/extjs.jspf" %>
<%@ include file="/13descuento/13secsession.jspf" %>

<script type="text/javascript" src="13forma03ext.js?<%=session.getId()%>"></script>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%@ include file="/01principal/01pyme/cabeza.jspf"%>
					<div id="_menuApp"></div>
					<div id="Contcentral">
						<%@ include file="/01principal/01pyme/menuLateral.jspf"%>
						<div id="areaContenido"></div>
					</div>
				</div>
				<%@ include file="/01principal/01pyme/pie.jspf"%>
<form id='formAux' name="formAux" target='_new'></form>


<%-- Como el contenido es basicamente est�tico, se realiza de esta manera... de lo contrario usar un .data. --%>
<div id="mensaje" style="display:none">
	<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td valign="center">
		<!--Tabla central-->
		<!--tabla de menu superior-->
			<table width="580" border="0" cellspacing="0" cellpadding="2" class="formas">
			<tr>
				<td align="center" colspan="2"><br><span class="titulos"><%=iNoNafinElectronico + " " + strNombre%></span></td>
			</tr>
			</table>	
		</td>
	</tr>
	<tr>
		<td align="center"><br>
		<!--tabla de contenidos-->
			<table width="450" cellpadding="2" cellspacing="0" border="0" class="formas">
			<tr>
				<td colspan="2" align="center"><span class="subtitulos">Aviso</span><br></td>
			</tr>
			<tr>
				<td width="7"><img src="../../00utils/gif/bullet.gif" width="5" height="5" border="0" alt=""></td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td></td>
				<td align="center">
					Usted ya realiz� el tr�mite de formalizaci�n para efectuar operaciones electr�nicas, 
					para habilitar su clave, por favor comun�quese al <span style="font-weight: bold;">Centro de Atenci�n a Clientes</span> 
					al tel�fono 50-89-61-07 � del interior al tel�fono <span style="font-weight: bold;">01-800-NAFINSA (01-800-6234672)</span>.
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>	
</div>
</body>
</html>

