Ext.onReady(function() {

//_--------------------------------- HANDLERS -------------------------------
	
	var jsonParametrizacion = null;
	function procesarSuccessFailureParametrizacion(opts, success, response) {
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			jsonParametrizacion = Ext.util.JSON.decode(response.responseText);
			fp.el.unmask();
			var vencePyme = Ext.getCmp('hidenVencePyme');
			if (jsonParametrizacion.sFechaVencPyme != undefined) {vencePyme.setValue(jsonParametrizacion.sFechaVencPyme);}
			var cmbStatus = Ext.getCmp('cambioEstatusCmb');
			cmbStatus.focus();
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
	var factoraje = {
		bOperaFactorajeVencido: false,
      bOperaFactorajeAutomatico: false,
		bOperaFactorajeDistribuido: false,
		bOperaFactVencimientoInfonavit: false
	}

	var procesarCatalogoFactorajeData = function(store, arrRegistros, opts) {
		var jsonData = store.reader.jsonData;
		var noAplicaCatalogoFactoraje = jsonData.noAplica;

		var factorajeCmb = Ext.getCmp('tipoFactorajeCmb');
		
		if (noAplicaCatalogoFactoraje) {
			factorajeCmb.hide();
		} else {
			factorajeCmb.show();
		}
	}
	
	var procesarConsultaData = function(store, arrRegistros, opts) {
		var fp = Ext.getCmp('forma');
		fp.el.unmask();

		if (arrRegistros != null) {
			if (!grid.isVisible()) {
				grid.show();
			}
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');

			var btnTotales = Ext.getCmp('btnTotales');

			var el = grid.getGridEl();
			
			if(store.getTotalCount() > 0) {
				Ext.getCmp('btnTotales').disable();
				Ext.getCmp('btnGenerarArchivo').disable();
				Ext.getCmp('btnBajarArchivo').hide();
				Ext.getCmp('btnGenerarPDF').disable();
				Ext.getCmp('btnBajarPDF').hide();

		//Columnas que utilizamos para ocultar/mostrar
				var cm = grid.getColumnModel();
				cm.setHidden(cm.findColumnIndex('NOMBREIF'), true);
				cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), true);
				cm.setHidden(cm.findColumnIndex('FN_MONTO_ANTERIOR'), true);
				cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), true);
				cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), true);
				cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), true);
				cm.setHidden(cm.findColumnIndex('MANDANTE'), true);
				
				var cmbEstatus = Ext.getCmp("cambioEstatusCmb").getValue();
				if (cmbEstatus == "29") {
					cm.setHidden(cm.findColumnIndex('NOMBREIF'), false);
				}
				if(jsonParametrizacion.bTipoFactoraje) {
					cm.setHidden(cm.findColumnIndex('TIPO_FACTORAJE'), false);
				}
				
				if (cmbEstatus != "29") {  //Si no es 29
					cm.setHidden(cm.findColumnIndex('FN_MONTO_ANTERIOR'), false);
					if(jsonParametrizacion.bOperaFactorajeVencido || jsonParametrizacion.bOperaFactorajeAutomatico) {
						cm.setHidden(cm.findColumnIndex('NOMBREIF'), false);
					}
					
					if(jsonParametrizacion.bOperaFactorajeDistribuido || jsonParametrizacion.bOperaFactVencimientoInfonavit) {
						cm.setHidden(cm.findColumnIndex('NOMBRE_BENEFICIARIO'), false);
						cm.setHidden(cm.findColumnIndex('FN_PORC_BENEFICIARIO'), false);
						cm.setHidden(cm.findColumnIndex('RECIBIR_BENEFICIARIO'), false);
					}
				}
				
				var cmbFactoraje = Ext.getCmp("tipoFactorajeCmb");
				if (cmbFactoraje.getValue() == 'M' || cmbFactoraje.getValue() == '') {
					cm.setHidden(cm.findColumnIndex('MANDANTE'), false);
				}
		//termina de ocultar/mostrar columnas			
				btnTotales.enable();
				btnGenerarPDF.enable();
				btnBajarPDF.hide();
				if(!btnBajarArchivo.isVisible()) {
					btnGenerarArchivo.enable();
				} else {
					btnGenerarArchivo.disable();
				}
				el.unmask();
			} else {
				btnTotales.disable();
				btnGenerarArchivo.disable();
				btnGenerarPDF.disable();
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
		}
	}

	var procesarSuccessFailureGenerarPDF =  function(opts, success, response) {
		var btnGenerarPDF = Ext.getCmp('btnGenerarPDF');
		btnGenerarPDF.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarPDF = Ext.getCmp('btnBajarPDF');
			btnBajarPDF.show();
			btnBajarPDF.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarPDF.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarPDF.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

	var procesarSuccessFailureGenerarArchivo =  function(opts, success, response) {
		var btnGenerarArchivo = Ext.getCmp('btnGenerarArchivo');
		btnGenerarArchivo.setIconClass('');
		//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var btnBajarArchivo = Ext.getCmp('btnBajarArchivo');
			btnBajarArchivo.show();
			btnBajarArchivo.el.highlight('FFF700', {duration: 5, easing:'bounceOut'});
			btnBajarArchivo.setHandler( function(boton, evento) {
				var forma = Ext.getDom('formAux');
				forma.action = Ext.util.JSON.decode(response.responseText).urlArchivo;
				forma.submit();
			});
		} else {
			btnGenerarArchivo.enable();
			NE.util.mostrarConnError(response,opts);
		}
	}

//-------------------------------- STORES -----------------------------------
	var catalogoEPOData = new Ext.data.JsonStore({
		id: 'catalogoEPODataStore',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoEPOPyme'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoMonedaData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMoneda'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoEstatusData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoCambioEstatus'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var catalogoFactorajeData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoFactoraje'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			load: procesarCatalogoFactorajeData,
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
//catalogoMandanteData,

	var catalogoMandanteData = new Ext.data.JsonStore({
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'CatalogoMandante'
		},
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});

	var consultaData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'Consulta'
		},
		fields: [
			{name: 'NOMBRE_EPO'},                    //Nom. EPO
			{name: 'IG_NUMERO_DOCTO'},              //Num docto.
			{name: 'DF_FECHA_DOCTO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'DF_FECHA_VENC', type: 'date', dateFormat: 'd/m/Y'}, 
			{name: 'NOMBREIF'},
			{name: 'DC_FECHA_CAMBIO', type: 'date', dateFormat: 'd/m/Y'},
			{name: 'CD_NOMBRE'},                    //Moneda
			{name: 'TIPO_FACTORAJE'},            //Tipo factoraje	
			{name: 'MONTO', type: 'float'},
			{name: 'FN_MONTO_DSCTO', type: 'float'},
			{name: 'PORCENTAJE_DESCUENTO'},
			{name: 'CD_DESCRIPCION'},
			{name: 'CT_CAMBIO_MOTIVO'},
			{name: 'FN_MONTO_ANTERIOR'},
			{name: 'NOMBRE_BENEFICIARIO'},
			{name: 'FN_PORC_BENEFICIARIO'},
			{name: 'RECIBIR_BENEFICIARIO'},
			{name: 'MANDANTE'},
			{name: 'IC_CAMBIO_ESTATUS'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			beforeLoad:	{fn: function(store, options){
							var _mandanteCmb = Ext.getCmp('mandanteCmb');
							var tipoFactoraje = Ext.getCmp('tipoFactorajeCmb');
							var vencePyme = Ext.getCmp('hidenVencePyme');
							Ext.apply(options.params, {mandant:_mandanteCmb.value,tipoFactoraje:tipoFactoraje.value,sFechaVencPyme:vencePyme.value});
							}
			},
			load: procesarConsultaData,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaData(null, null, null);
				}
			}
		}
		
	});

	var resumenTotalesData = new Ext.data.JsonStore({
		root : 'registros',
		url : '13consulta04ext.data.jsp',
		baseParams: {
			informacion: 'ResumenTotales'
		},
		fields: [
			{name: 'TOTAL_REGISTROS', type: 'float', mapping: 'col0'},
			{name: 'TOTAL_MONTO_DOCUMENTOS', type: 'float', mapping: 'col1'},
			{name: 'TOTAL_MONTO_DESCUENTO', type: 'float', mapping: 'col2'},
			{name: 'MONEDA', mapping: 'col4' }
		],
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError
		}
	});

//-------------------------------------------------------------------

	var elementosForma = [
		{
			xtype: 'combo',
			name: 'ic_epo',
			id: 'cmbEpo',
			allowBlank: false,
			fieldLabel: 'EPO',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'ic_epo',
			emptyText: 'Seleccionar EPO',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : catalogoEPOData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						grid.hide();
						var totalesCmp = Ext.getCmp('gridTotales');
						if (totalesCmp.isVisible()) {
							totalesCmp.hide();
						}
						var tipoFactorajeCmp = Ext.getCmp('tipoFactorajeCmb');
						var mandanteCmb = Ext.getCmp('mandanteCmb');
						mandanteCmb.hide();
						
						tipoFactorajeCmp.setValue('');
						tipoFactorajeCmp.store.removeAll();
						tipoFactorajeCmp.setDisabled(false);
						tipoFactorajeCmp.store.load({
								params: {
									claveEpo: combo.getValue()
								}
						});
						//Mientras no se cargue la informacion no permita realizar la consulta, dado que se necesitan los valores
						fp.el.mask('Procesando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '13consulta04ext.data.jsp',
							params: {
								informacion: "Parametrizacion",
								claveEpo:  combo.getValue()
							},
							callback: procesarSuccessFailureParametrizacion
						});
					}
				}/*,
				change :	function(combo, value){
						var cmbStatus = Ext.getCmp('cambioEstatusCmb');
						var btnConsul = Ext.getCmp('btnConsultar');
						if (value != undefined && value != "" && cmbStatus.value != undefined && cmbStatus.value != "")	{
							btnConsul.enable();
						}else	{
							btnConsul.disable();
						}
				}*/
			}
		},
		{
			xtype: 'textfield',
			name: 'ig_numero_docto',
			id: 'no_doc',
			fieldLabel: 'N�mero de documento',
			allowBlank: true,
			maxLength: 15	//ver el tama�o maximo del numero en BD para colocar este igual
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Vencimiento',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_vencMin',
					id: 'dc_fecha_vencMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'dc_fecha_vencMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_vencMax',
					id: 'dc_fecha_vencMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha',
					campoInicioFecha: 'dc_fecha_vencMin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},    
		{
			xtype: 'combo',
			name: 'ic_moneda',
			fieldLabel: 'Moneda',
			emptyText: 'Seleccionar Moneda',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'ic_moneda',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoMonedaData,
			tpl : NE.util.templateMensajeCargaCombo
		},    
		{
			xtype: 'compositefield',
			fieldLabel: 'Monto de',
			msgTarget: 'side',
			combineErrors: false,
			items: [
				{
					xtype: 'numberfield',
					name: 'fn_montoMin',
					id: 'montoMin',
					allowBlank: true,
					maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'a',
					width: 20
				},
				{
					xtype: 'numberfield',
					name: 'fn_montoMax',
					id: 'montoMax',
					allowBlank: true,
					maxLength: 15,
					width: 150,
					msgTarget: 'side',
					margins: '0 20 0 0'	  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'combo',
			name: 'ic_cambio_estatus',
			id: 'cambioEstatusCmb',
			fieldLabel: 'Tipo de Cambio de Estatus',
			emptyText: 'Seleccionar Estatus',
			allowBlank: false,
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'ic_cambio_estatus',
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: catalogoEstatusData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Emision',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'df_fecha_doctoMin',
					id: 'dc_fecha_emisionMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'dc_fecha_emisionMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'df_fecha_doctoMax',
					id: 'dc_fecha_emisionMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'dc_fecha_emisionMin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'compositefield',
			fieldLabel: 'Fecha de Cambio de Estatus',
			combineErrors: false,
			msgTarget: 'side',
			items: [
				{
					xtype: 'datefield',
					name: 'dc_fecha_cambioMin',
					id: 'dc_fecha_cambioMin',
					allowBlank: true,
					startDay: 0,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoFinFecha: 'dc_fecha_cambioMax',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				},
				{
					xtype: 'displayfield',
					value: 'al',
					width: 20
				},
				{
					xtype: 'datefield',
					name: 'dc_fecha_cambioMax',
					id: 'dc_fecha_cambioMax',
					allowBlank: true,
					startDay: 1,
					width: 100,
					msgTarget: 'side',
					vtype: 'rangofecha', 
					campoInicioFecha: 'dc_fecha_cambioMin',
					margins: '0 20 0 0'  //necesario para mostrar el icono de error
				}
			]
		},
		{
			xtype: 'combo',
			name: 'tipoFactoraje',
			id: 'tipoFactorajeCmb',
			fieldLabel: 'Tipo Factoraje',
			emptyText: 'Seleccionar',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'tipoFactoraje',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			hidden: true,
			store: catalogoFactorajeData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners: {
				select: {
					fn: function(combo) {
						var vManda = combo.getValue();
						var _mandanteCmb = Ext.getCmp('mandanteCmb');
						_mandanteCmb.setValue('');
						_mandanteCmb.store.removeAll();
						_mandanteCmb.setDisabled(false);
						if (vManda == 'M') {
						_mandanteCmb.store.load({
								params: {
									claveMandante: combo.getValue()
								}
						});
						_mandanteCmb.show();
						} else {
							_mandanteCmb.hide();
						}
					}
				}
			}
		},
		{
			xtype: 'combo',
			name: 'ic_mandante',
			id: 'mandanteCmb',
			fieldLabel: 'Mandante',
			emptyText: 'Seleccionar',
			mode: 'local',
			displayField: 'descripcion',
			valueField: 'clave',
			hiddenName : 'ic_mandante',
			forceSelection : false,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			hidden: true,
			store: catalogoMandanteData,
			tpl : NE.util.templateMensajeCargaCombo
		},{
			xtype:'hidden',
			id:'hidenVencePyme',
			name:'sFechaVencPyme', 
			value:''
		}
	];

	var gridTotales = new Ext.grid.GridPanel({
		store: resumenTotalesData,
		id: 'gridTotales',
		hidden: true,
		columns: [
			{
				header: 'TOTALES',
				dataIndex: 'MONEDA',
				align: 'left',
				width: 250
			},
			{
				header: 'Registros',
				dataIndex: 'TOTAL_REGISTROS',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('0,000')
			},
			{
				header: 'Monto Documentos',
				dataIndex: 'TOTAL_MONTO_DOCUMENTOS',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Monto Descuento',
				dataIndex: 'TOTAL_MONTO_DESCUENTO',
				width: 150,
				align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			}
		],
		height: 115,
		title: 'Totales',
		tools: [
			{
				id: 'close',
				handler: function(evento, toolEl, panel, tc) {
					panel.hide();
				}
			}
		],
		frame: false
	});

//---Aqui van los grid---

	var grid = new Ext.grid.GridPanel({
		store: consultaData,
		hidden: true,
		columns: [
			{
				header: 'EPO', tooltip: 'EPO',	//0
				dataIndex: 'NOMBRE_EPO',
				sortable: true,
				width: 250,
				resizable: true,
				hidden: false
			},
			{
				header : 'Num. Documento', tooltip: 'Numero de Documento',		//1
				dataIndex : 'IG_NUMERO_DOCTO',
				width : 150,
				sortable : true, hidden: false
			},
			{
				header : 'Fecha de Emisi�n', tooltip: 'Fecha de Emisi�n',		//2
				dataIndex : 'DF_FECHA_DOCTO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Fecha de Vencimiento', tooltip: 'Fecha de Vencimiento',
				dataIndex : 'DF_FECHA_VENC',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header: 'Nombre IF', tooltip: 'Nombre IF',
				dataIndex: 'NOMBREIF',
				sortable: true,	width: 250,	resizable: true, hideable : false, hidden: true
			},
			{
				header : 'Fecha del Cambio de Estatus', tooltip: 'Fecha del Cambio de Estatus',
				dataIndex : 'DC_FECHA_CAMBIO',
				sortable : true,
				width : 100, hidden: false,
				renderer: Ext.util.Format.dateRenderer('d/m/Y')
			},
			{
				header : 'Moneda', tooltip: 'Moneda',
				dataIndex : 'CD_NOMBRE',
				sortable : true,
				hideable : false, hidden: false,
				width : 150
			},
			{
				header : 'Tipo Factoraje', tooltip: 'Tipo Factoraje',
				dataIndex : 'TIPO_FACTORAJE',
				sortable : true, width : 80, hidden: true
			},
			{
				header : 'Monto', tooltip: 'Monto del Documento',
				dataIndex : 'MONTO',
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header : 'Porcentaje de Descuento', tooltip: 'Porcentaje de Descuento',
				dataIndex : 'PORCENTAJE_DESCUENTO',
				sortable : true,
				width : 100,
				align: 'right', hidden: false,
				renderer: Ext.util.Format.numberRenderer('0%')
			},
			{
				header : 'Monto a Descontar', tooltip: 'Monto a Descontar',
				//dataIndex : 'FN_MONTO_DSCTO',  //Calculado
				sortable : true,
				width : 120,
				align: 'right', hidden: false,
				renderer: function(valor, metaData, registro, rowIndex, colIndex, store) {
					if (registro.get("IC_CAMBIO_ESTATUS") == 4 ||
							registro.get("IC_CAMBIO_ESTATUS") == 5 ||
							registro.get("IC_CAMBIO_ESTATUS") == 6 ||
							registro.get("IC_CAMBIO_ESTATUS") == 8 ||
							registro.get("IC_CAMBIO_ESTATUS") == 28 ||
							registro.get("IC_CAMBIO_ESTATUS") == 29) {
						return Ext.util.Format.number(registro.get('MONTO')*(registro.get('PORCENTAJE_DESCUENTO')/100), '$0,0.00');
					} else {
						return Ext.util.Format.number(registro.get('FN_MONTO_DSCTO'), '$0,0.00');
					}
				}
			},
			{
				header : 'Tipo Cambio de Estatus', tooltip: 'Cambio de Estatus',
				dataIndex : 'CD_DESCRIPCION',
				sortable : true,
				hidden: false,
				renderer: function(valor, metaData, registro, rowIndex, colIndex, store) {
					if (registro.get("IC_CAMBIO_ESTATUS") == "28") {
						valor = valor + ' ' + Ext.util.Format.date(registro.get("DC_FECHA_CAMBIO"),'d/m/Y');
					}
					return valor;
				},
				width : 200
			},
			{
				header : 'Causa',
				tooltip: 'Causa',
				dataIndex : 'CT_CAMBIO_MOTIVO',
				sortable : true,
				width : 200, hidden: false,
				renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
				}
			},
			{
				header : 'Monto Anterior', tooltip: 'Monto Anterior',
				dataIndex : 'FN_MONTO_ANTERIOR',
				sortable : true, width : 120, align: 'right', hidden: true,
				renderer: Ext.util.Format.numberRenderer('$0,0.00')
			},
			{
				header: 'Beneficiario', tooltip: 'Beneficiario',
				dataIndex: 'NOMBRE_BENEFICIARIO',
				sortable: true,	width: 250,	resizable: true, hideable : true ,hidden: true
			},
			{
				header: '% Beneficiario', tooltip: '% Beneficiario',
				dataIndex: 'FN_PORC_BENEFICIARIO',
				sortable: true,	width: 100,	resizable: true, hideable: true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('0.00%') ,hidden: true
			},
			{
				header: 'Importe a Recibir Beneficiario', tooltip: 'Importe a Recibir Beneficiario',
				dataIndex: 'RECIBIR_BENEFICIARIO',
				sortable: true,	width: 150,	resizable: true, hideable : true, align: 'right',
				renderer: Ext.util.Format.numberRenderer('$0,0.00') ,hidden: true
			},
			{
				header: 'Mandante', tooltip: 'Mandante',
				dataIndex: 'MANDANTE',
				sortable: true,	width: 250,	resizable: true, hidden: true
			}
		],
		stripeRows: true,
		loadMask: true,
		height: 400,
		width: 943,
		title: '',
		frame: true,
		bbar: {
			xtype: 'paging',
			pageSize: 15,
			buttonAlign: 'left',
			id: 'barraPaginacion',
			displayInfo: true,
			store: consultaData,
			displayMsg: '{0} - {1} de {2}',
			emptyMsg: "No hay registros.",
			items: [
				'-',
				{
					xtype: 'button',
					text: 'Totales',
					id: 'btnTotales',
					hidden: false,
					handler: function(boton, evento) {
						resumenTotalesData.load();
						var totalesCmp = Ext.getCmp('gridTotales');
						if (!totalesCmp.isVisible()) {
							totalesCmp.show();
							totalesCmp.el.dom.scrollIntoView();
						}
					}
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar Archivo',
					id: 'btnGenerarArchivo',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						Ext.Ajax.request({
							url: '13consulta04exta.jsp',
							params: fp.getForm().getValues(),
							callback: procesarSuccessFailureGenerarArchivo
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar Archivo',
					id: 'btnBajarArchivo',
					hidden: true
				},
				'-',
				{
					xtype: 'button',
					text: 'Generar PDF',
					id: 'btnGenerarPDF',
					handler: function(boton, evento) {
						boton.disable();
						boton.setIconClass('loading-indicator');
						var cmpBarraPaginacion = Ext.getCmp("barraPaginacion");
						Ext.Ajax.request({
							url: '13consulta04extpdf.jsp',
							params: Ext.apply(fp.getForm().getValues(),{
								start: cmpBarraPaginacion.cursor,
								limit: cmpBarraPaginacion.pageSize
							}),
							callback: procesarSuccessFailureGenerarPDF
						});
					}
				},
				{
					xtype: 'button',
					text: 'Bajar PDF',
					id: 'btnBajarPDF',
					hidden: true
				},
				'-'
			]
		}
	});


//-------------------------------- PRINCIPAL -----------------------------------
	var fp = new Ext.form.FormPanel({
		id: 'forma',
		width: 885,
		title: 'Cambio de Estatus - Criterios de B�squeda.',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px',
		labelWidth: 170,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosForma,
		monitorValid: false,
		buttons: [
			{
				text: 'Consultar',
				id: 'btnConsultar',
				iconCls: 'icoBuscar',
				//disabled: true,
				//formBind: true,
				handler: function(boton, evento) {
					grid.hide();
					var totalesCmp = Ext.getCmp('gridTotales');
					if (totalesCmp.isVisible()) {
						totalesCmp.hide();
					}
					var cmbEpo = Ext.getCmp('cmbEpo');
					if (cmbEpo.value == "")	{
						cmbEpo.markInvalid('Debe seleccionar una EPO');
						cmbEpo.focus();
						return;
					}
					var cmbEstatus = Ext.getCmp("cambioEstatusCmb");
					if (cmbEstatus.value == "")	{
						cmbEstatus.markInvalid('Debe seleccionar un tipo de cambio de estatus');
						cmbEstatus.focus();
						return;
					}

					if(!verificaCampos()) {
						return;
					}

					var fechaVenceMin = Ext.getCmp("dc_fecha_vencMin");
					var fechaVenceMax = Ext.getCmp("dc_fecha_vencMax");
					if (!Ext.isEmpty(fechaVenceMin.getValue()) && Ext.isEmpty(fechaVenceMax.getValue()) ) {
						fechaVenceMax.markInvalid('Por favor, especifique la fecha de vencimiento final');
						fechaVenceMax.focus();
						return;
					}
          
					var fechaEmisionMin = Ext.getCmp("dc_fecha_emisionMin");
					var fechaEmisionMax = Ext.getCmp("dc_fecha_emisionMax");
					if (!Ext.isEmpty(fechaEmisionMin.getValue()) && Ext.isEmpty(fechaEmisionMax.getValue())) {
						fechaEmisionMax.markInvalid('Por favor, especifique la fecha de emision final');
						fechaEmisionMax.focus();
						return;
					}

					var fechaCambioMin = Ext.getCmp("dc_fecha_cambioMin");
					var fechaCambioMax = Ext.getCmp("dc_fecha_cambioMax");
					if (!Ext.isEmpty(fechaCambioMin.getValue()) && Ext.isEmpty(fechaCambioMax.getValue()) ) {
						fechaCambioMax.markInvalid('Por favor, especifique la fecha de cambio final');
						fechaCambioMax.focus();
						return;
					}
					
					var fn_montoMin = Ext.getCmp("montoMin");
					var fn_montoMax = Ext.getCmp("montoMax");
					if (!Ext.isEmpty(fn_montoMin.getValue()) && Ext.isEmpty(fn_montoMax.getValue()) ) {
						fn_montoMax.markInvalid('Por favor, especifique la fecha de cambio final');
						fn_montoMax.focus();
						return;
					}

					if(fechaVenceMin.getValue() != "" && fechaVenceMax.getValue() != "" && fechaEmisionMin.getValue() != "" && fechaEmisionMax.getValue() != "")	{
						var resultado = datecomp(fechaVenceMin.value,fechaEmisionMin.value);
						var resultado1 = datecomp(fechaVenceMax.value,fechaEmisionMax.value);
						if (resultado != 1 || resultado1 != 1)	{
							if (resultado != 1 && resultado1 != 1)	{
								fechaVenceMin.setValue("");
								//fechaVenceMax.setValue("");
								fechaVenceMin.focus();
								fechaVenceMin.markInvalid('La fecha de vencimiento debe ser mayor a la del documento');
							}else{
								if (resultado != 1){
									fechaVenceMin.setValue("");
									fechaVenceMin.focus();
									fechaVenceMin.markInvalid('La fecha de vencimiento debe ser mayor a la del documento');
								}else{
									fechaVenceMax.setValue("");
									fechaVenceMax.focus();
									fechaVenceMax.markInvalid('La fecha de vencimiento debe ser mayor a la del documento');
								}
							}
							return;
						}
					}
					
					if(fechaCambioMin.getValue() != "" && fechaCambioMax.getValue() != "" && fechaEmisionMin.getValue() != "" && fechaEmisionMax.getValue() != "")	{
						var resultado = datecomp(fechaCambioMin.value, fechaEmisionMin.value);
						var resultado1 = datecomp(fechaCambioMax.value, fechaEmisionMax.value);
						if (resultado != 1 || resultado1 != 1)	{
							if (resultado != 1 && resultado1 != 1)	{
								fechaCambioMin.setValue("");
								//fechaCambioMax.setValue("");
								fechaCambioMin.focus();
								fechaCambioMin.markInvalid('La fecha de cambio de estatus debe ser mayor a la del documento');
							}else{
								if (resultado != 1){
									fechaCambioMin.setValue("");
									fechaCambioMin.focus();
									fechaCambioMin.markInvalid('La fecha de cambio de estatus debe ser mayor a la del documento');
								}else{
									fechaCambioMax.setValue("");
									fechaCambioMax.focus();
									fechaCambioMax.markInvalid('La fecha de cambio de estatus debe ser mayor a la del documento');
								}
							}
							return;
						}
					}
					//Si llega aqui es porque pas� todas las validaciones
					fp.el.mask('Enviando...', 'x-mask-loading');
					consultaData.load({
						params: Ext.apply(fp.getForm().getValues(),{
							operacion: 'Generar', //Generar datos para la consulta
							start: 0,
							limit: 15
						})
					});
				} //fin handler
			},
			{
				text: 'Limpiar',
				iconCls: 'icoLimpiar',
				handler: function() {
					window.location = '13consulta04ext.jsp';
				}
			}
		]
	});
	
	function verificaCampos(){
		var myPanel = Ext.getCmp('forma');
		var valid = true;
		myPanel.items.each(function(panelItem, index, totalCount){
			if (!panelItem.isValid())	{
				valid = false;
				return false;
			}
		});
		return valid;
	}

//Simulacion en un contenedor
	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 949,
		height: 'auto',
		items: [
			fp,
			NE.util.getEspaciador(20),
			grid,
			gridTotales
		]
	});
//---Catalogos---	
	catalogoEPOData.load();
	catalogoMonedaData.load();
	catalogoEstatusData.load();

});