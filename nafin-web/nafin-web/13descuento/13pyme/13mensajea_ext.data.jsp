<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,netropology.utilerias.usuarios.*,
		netropology.utilerias.*,com.netro.afiliacion.*,com.netro.anticipos.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject, net.sf.json.JSONSerializer"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>

<%
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String infoRegresar	=	"";

Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);

System.out.println("iNoCliente ====== "+iNoCliente);
if (informacion.equals("valoresIniciales")) {
	String gsParSistema = (String) request.getParameter("parSistema");
	String nombreArchivo="", fechaConvenio="";
	String archivoConvenio = (request.getParameter("archivoConvenio")==null)?"":request.getParameter("archivoConvenio");
  String tieneConvUnico = BeanAfiliacion.validaPymeParamConvUnico(iNoCliente);
  List 	lRegCtas = BeanAfiliacion.getCuentasBancariasPyME(iNoCliente);
  int existCtasConvUnico = lRegCtas.size();
	int reg=0;
	List lRegistro = BeanAfiliacion.getPreafiliacionPyME(iNoCliente);
	List NuevoReg = new ArrayList();
	for(int i=0;i<lRegistro.size();i++) {
		HashMap hash = new HashMap();
		List lDatos = (ArrayList)lRegistro.get(i);
		String ic_if    		= lDatos.get(0).toString();
		String nombre_if		= lDatos.get(1).toString();
		String ic_epoR        		= lDatos.get(2).toString();
		String nombre_epo     		= lDatos.get(3).toString();
		String tipoDocumento     		= lDatos.get(4).toString();
		if(!"".equals(ic_if)){
			reg=BeanAfiliacion.getExisteArchivo(ic_if);
			nombreArchivo = ic_if+".gif";
			fechaConvenio = BeanAfiliacion.getExisteArchivoConvenio(ic_if, ic_epoR);
			archivoConvenio = ic_if+"_"+ic_epoR+".pdf";
		}
		hash.put("IC_IF",		ic_if);
		hash.put("NOMBRE_IF",		nombre_if);
		hash.put("IC_EPOR",			ic_epoR);
		hash.put("NOMBRE_EPO",		nombre_epo);
		hash.put("TIPODOCUMENTO",	tipoDocumento);
		hash.put("REG",				Integer.toString(reg));
		hash.put("NOMBREARCHIVO",	nombreArchivo);
		hash.put("FECHACONVENIO",	fechaConvenio);
		hash.put("ARCHIVOCONVENIO",archivoConvenio);
		NuevoReg.add(hash);
	}

	JSONObject jsonObj = new JSONObject();
  
	jsonObj.put("tieneConvUnico", tieneConvUnico);
  jsonObj.put("existCtasConvUnico", String.valueOf(existCtasConvUnico));
  jsonObj.put("success", new Boolean(true));
	jsonObj.put("strPerfil", strPerfil);
	if (NuevoReg != null){
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(NuevoReg);
		jsonObj.put("NuevoReg", jsObjArray);
	}

	infoRegresar = jsonObj.toString();
	
} else if (informacion.equals("confirmaClave")) {

	String cgLogin = (request.getParameter("cesionUser")!=null)?request.getParameter("cesionUser"):"";
	String cgPassword = (request.getParameter("cesionPassword")!=null)?request.getParameter("cesionPassword"):"";
	String ic_if = request.getParameter("ic_if") == null? "": request.getParameter("ic_if");
	String ic_epo= request.getParameter("ic_epo") == null? "": request.getParameter("ic_epo");
	String ic_cuenta_bancaria= request.getParameter("ic_cuenta_bancaria") == null? "": request.getParameter("ic_cuenta_bancaria");
	String intermediario = request.getParameter("intermediario") == null? "": request.getParameter("intermediario");
	String cg_ip =(request.getRemoteAddr());
	String resultado = "";
	List NuevoReg = new ArrayList();
	if(strLogin.equals(cgLogin)) {
		UtilUsr utils = new UtilUsr();
		if (utils.esUsuarioValido(cgLogin, cgPassword)) {
			resultado = "S";
			BeanAfiliacion.setCuentaBancaria(ic_epo, ic_if, ic_cuenta_bancaria,strLogin,iNoNafinElectronico.toString(),cg_ip, iNoCliente);
			List lRegistro = BeanAfiliacion.getCuentasBancariasPyME(iNoCliente);
			for(int i=0;i<lRegistro.size();i++) {
				HashMap hash = new HashMap();
				List lDatos = (ArrayList)lRegistro.get(i);
				//hash.put("CUENTA_BANCARIA",lDatos.get(0).toString());
				hash.put("CG_BANCO",       lDatos.get(1).toString());
				hash.put("NUMERO_CUENTA",  lDatos.get(2).toString());
				hash.put("SUCURSAL",       lDatos.get(3).toString());
				hash.put("CUENTA_CLABE",   lDatos.get(4).toString());
				NuevoReg.add(hash);
			}
		} else {
			resultado = "N";
		}
	} else {
		resultado = "N";
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	jsonObj.put("resultado", resultado);
	if (NuevoReg != null){
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(NuevoReg);
		jsonObj.put("nuevoSolic2", jsObjArray);
	}
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("enviaSolicitud1")){

	String ic_epo= request.getParameter("ic_epo") == null? "": request.getParameter("ic_epo");
	String ic_if = request.getParameter("ic_if") == null? "": request.getParameter("ic_if");
	BeanAfiliacion.setInformacion(ic_epo,  ic_if, iNoCliente);
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();
	
} else if (informacion.equals("enviaSolicitud2")){
  
	List lRegistro = BeanAfiliacion.getCuentasBancariasPyME(iNoCliente);
	List NuevoReg = new ArrayList();
	for(int i = 0; i < lRegistro.size(); i++){
		HashMap hash = new HashMap();
		List lDatos = (ArrayList)lRegistro.get(i);
		hash.put("CUENTA_BANCARIA", lDatos.get(0).toString());
		hash.put("CG_BANCO",        lDatos.get(1).toString());
		hash.put("NUMERO_CUENTA",   lDatos.get(2).toString());
		hash.put("SUCURSAL",        lDatos.get(3).toString());
		hash.put("CUENTA_CLABE",    lDatos.get(4).toString());
		NuevoReg.add(hash);
	}
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("success", new Boolean(true));
	if (NuevoReg != null){
		JSONArray jsObjArray = new JSONArray();
		jsObjArray = JSONArray.fromObject(NuevoReg);
		jsonObj.put("solic2", jsObjArray);
	}

	infoRegresar = jsonObj.toString();

}
%>
<%=infoRegresar%>