<%@ page import="oracle.security.sso.enabler.*" %>

<jsp:useBean id="ssoObj" scope="application" class="com.chermansolutions.oracle.sso.partnerapp.beans.SSOEnablerJspBean" />
<%
    
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Pragma", "no-cache");
	response.setHeader("Expires", "Thu, 29 Oct 2000 17:04:19 GMT");
	response.setContentType("text/html; charset=utf-8");

	//ssoObj.setDatasource("ssoApps");

	String usrInfo    = ssoObj.getSSOUserInfo(request, response);
	String logoutURL  = ssoObj.getSingleSignOffUrl(request);
	String requestURL = ssoObj.getRequestedUrl(request);
	
	//session.setAttribute("request_url",requestURL);
	//session.setAttribute("logoutURL",logoutURL);

	if(usrInfo == null) {
%>
	<center>Redireccionado a SSO Server...</center>
<%
		return;
	}
%>
