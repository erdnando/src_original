Ext.onReady(function() {

var vusuario = '';
var vnombreusr = '';
var vcorreousr = '';
//-------------------
var procesarConsultaData = function(store, arrRegistros, opts) {
	//var hidProducto = Ext.getDom('hidProducto').value;
	//var sOrigenDscto = Ext.getDom('sOrigenDscto').value;
	
	var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
	var panelContenido = Ext.getCmp('panelContenido1');
	if (arrRegistros != null) {
		if (!gridDomicilioPyme.isVisible()) {
			panelContenido.add(gridDomicilioPyme);
			panelContenido.add(botones);
			panelContenido.doLayout();
		}
		
		var el = gridDomicilioPyme.getGridEl();
		if(store.getTotalCount() > 0) {
			el.unmask();
		}else {
			el.mask('No se encontr� ning�n registro', 'x-mask');
		}
		
	}
}

var procesarConsultaPymes = function(store, arrRegistros, opts) {
	if (arrRegistros != null) {
		if(store.getTotalCount() <= 0) {
			Ext.MessageBox.alert('Aviso','No existe informaci�n con los criterios determinados');
		}
		if(store.getTotalCount() > 9999){
			Ext.getCmp('cboPyme1').setDisabled(true);
			Ext.MessageBox.alert('Error..','La consulta arroj� m�s datos de los soportados por la p�gina.<br> Favor de reafinar los criterios de b�squeda.');
	}
	}
	fpBusqAvanzada.el.unmask();
};

//-------------------
	var storeBusquedaPyme = new Ext.data.JsonStore({
	id: 'storeBusqAvanzFiado',
		root: 'registros',
		fields: ['clave', 'descripcion', 'cveEpoPyme', 'nombrePyme', 'nafinElec'],
		url: NE.appWebContextRoot+'/00utils/00buscapyme_ext.data.jsp',
	baseParams: {
		informacion: 'consultaPymes',
		hidProducto: Ext.getDom('hidProducto').value
	},
		totalProperty: 'total',
	autoLoad: false,
	listeners: {
		exception: NE.util.mostrarDataProxyError,
		beforeload: NE.util.initMensajeCargaCombo,
		load: procesarConsultaPymes
	}
	});

//-------------------

	var grupos = new Ext.ux.grid.ColumnHeaderGroup({
		rows: [
			[
				{header: '', colspan:1 , align: 'center'},
				{header: 'DOMICILIO', colspan:3 , align: 'center'},
				{header: '', colspan:1 , align: 'center'}
			]
		]
	});

	var elementsFormBusq = [{
			xtype: 'textfield',
			name: 'claveUsuario',
			id: 'claveUsuario1',
			fieldLabel: 'Clave de Usuario',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			anchor: '70%',
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
	},{
			xtype: 'numberfield',
			name: 'num_pyme',
			id: 'num_pyme1',
			fieldLabel: 'N@E PYME',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			anchor: '70%',
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
	},{
			xtype: 'textfield',
			name: 'nombre_pyme',
			id: 'nombre_pyme1',
			fieldLabel: 'Nombre',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			anchor: '70%',
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
	},{
			xtype: 'textfield',
			name: 'rfc_pyme',
			id: 'rfc_pyme1',
			fieldLabel: 'RFC',
			allowBlank: true,
			maxLength: 100,	//ver el tama�o maximo del numero en BD para colocar este igual
			anchor: '70%',
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
	},{
			xtype: 'numberfield',
			name: 'num_sirac_pyme',
			id: 'num_sirac_pyme1',
			fieldLabel: 'N�mero SIRAC',
			allowBlank: true,
			maxLength: 15,	//ver el tama�o maximo del numero en BD para colocar este igual
			anchor: '70%',
			msgTarget: 'side',
			margins: '0 20 0 0'  //necesario para mostrar el icono de error
	},{
			  xtype: 		'panel',
			  bodyStyle: 	'padding: 10px',
			  layout: {
					 type: 	'hbox',
					 pack: 	'center',
					 align: 	'middle'
			  	},
		items: [{
						xtype: 	'button',
						text: 	'Buscar',
						id: 		'btnBuscar',
						iconCls: 'icoBuscar',
						width: 	75,
						handler: function(boton, evento) {
							var pymeComboCmp = Ext.getCmp('cboPyme1');
							pymeComboCmp.setValue('');
							pymeComboCmp.setDisabled(false);
				fpBusqAvanzada.el.mask('Cargando...', 'x-mask-loading');
							storeBusquedaPyme.load({
									params: Ext.apply(fpBusqAvanzada.getForm().getValues())
								});				
						},
						style: { 
							  marginBottom:  '10px' 
						} 
		},{
						xtype: 'box',
						width: 20
		},{
						xtype: 	'button',
						text: 	'Cancelar',
						id: 		'btnCancelar2',
						iconCls: 'cancelar',
						width: 	75,
						handler: function(boton, evento) {
							window.location.href='13forma5_fwd_ext.jsp';
						},
						style: { 
							  marginBottom:  '10px' 
						} 
		}]
	},{ 
			xtype:   'label',  
			html:		'<hr>', 
			cls:		'x-form-item', 
			style: { 
				width:   		'100%', 
				textAlign: 		'left'
			} 
	},{
			xtype: 'combo',
			name: 'cboPyme',
			id: 'cboPyme1',
			mode: 'local',
			autoLoad: false,
			anchor: '95%',
			displayField: 'descripcion',
			emptyText: 'Seleccione...',
			valueField: 'clave',
			hiddenName : 'cboPyme',
			fieldLabel: 'Nombre',
			disabled: true,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store: storeBusquedaPyme,
			listeners: {
				select: function(cmb,record,index){
					gridDomicilioPyme.show();
				storeDomPymeData.load({
							params: {
								ic_pyme: record.get('clave')
							}
				});
			}
		}
	}];


//-------------------
var fpBusqAvanzada = new Ext.form.FormPanel({
		id: 'forma',
		width: 620,
		title: '',
		frame: false,
		//collapsible: true,
		titleCollapse: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		//defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items:[
			{ 
				xtype: 'fieldset',  
				title: 'Agregue un asteristico (*) al final para b�squeda gen�rica', 
				width: 620,
				items: elementsFormBusq
			}
		],
		monitorValid: true
		
	});
	

var storeDomPymeData = new Ext.data.JsonStore({
	root : 'registros',
	url : NE.appWebContextRoot+'/00utils/00buscapyme_ext.data.jsp',
	baseParams: {
		informacion: 'ConsultaInfoPyme'
	},
	fields: [
		{name: 'RFC'},
		{name: 'CALLE'},
		{name: 'COLONIA'},
		{name: 'CP'},
		{name: 'TELEFONO'},
		{name: 'USUARIO'},
		{name: 'NOMBREUSUARIO'},
		{name: 'CORREOUSUARIO'}
	],
	totalProperty : 'total',
	messageProperty: 'msg',
	autoLoad: false,
	listeners: {
		load: procesarConsultaData,
		exception: {
			fn: function(proxy, type, action, optionsRequest, response, args) {
				NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
				//LLama procesar consulta, para que desbloquee los componentes.
				procesarConsultaData(null, null, null);
			}
		}
	}
	
});

var gridDomicilioPyme = new Ext.grid.GridPanel({
	id: 'gridDomicilioPyme',
	store: storeDomPymeData,
	margins: '20 0 0 0',
	plugins: grupos,
	columns: [
		{
			header : 'Usuario',
			dataIndex : 'USUARIO',
			width : 110,
			sortable : true
		},
		{
			header : 'Nombre Usuario',
			dataIndex : 'NOMBREUSUARIO',
			width : 200,
			sortable : true
		},
		{
			header : 'RFC',
			dataIndex : 'RFC',
			width : 110,
			sortable : true
		},
		{
			header : 'Calle y n�mero',
			dataIndex : 'CALLE',
			width : 200,
			sortable : true
		},
		{
			header : 'Colonia',
			dataIndex : 'COLONIA',
			width : 200,
			sortable : true
		},
		{
			header : 'C.P.',
			dataIndex : 'CP',
			width : 60,
			sortable : true
		},
		{
			header : 'Tel�fono',
			dataIndex : 'TELEFONO',
			width : 100,
			sortable : true
		}
	],
	stripeRows: true,
	columnLines : true,
	loadMask: true,
	width: 600,
	height: 150,
	title: '',
	frame: true,
	listeners:{
		rowclick: function(grid, rowIndex, e){
			var store = grid.getStore();
			var record = store.getAt(rowIndex);
			vnombreusr = record.get("NOMBREUSUARIO");
			vusuario = record.get("USUARIO");
			vcorreousr = record.get("CORREOUSUARIO");
		}
	}
	});

//---

var botones = [
	{
		xtype: 		'panel',
		bodyStyle: 	'padding: 10px',
		layout: {
			 type: 	'hbox',
			 pack: 	'center',
			 align: 	'middle'
		},
		items: [
			{
				xtype: 	'button',
				text: 	'Siguiente',
				id: 		'btnSiguiente',
				iconCls: 'icoContinuar',
				width: 	75,
				handler: function(boton, evento) {
					var pymeCombo = Ext.getCmp('cboPyme1');
					var record = pymeCombo.findRecord(pymeCombo.valueField, pymeCombo.getValue());		
					var strNePymeAsigna = record.get('nafinElec');
					var strNombrePymeAsigna = record.get('nombrePyme');
					
					if(vusuario!=''){
						window.location = '13forma5_ext.jsp?ic_pyme='+pymeCombo.getValue()+'&strNombrePymeAsigna='+strNombrePymeAsigna+'&strNePymeAsigna='+strNePymeAsigna+'&hidProducto='+
										  Ext.getDom('hidProducto').value+'&sOrigenDscto='+Ext.getDom('sOrigenDscto').value+'&strUsr='+vusuario+'&strNombreUsr='+vnombreusr+'&strCorreoUsr='+vcorreousr;
					}
					
				},
				style: { 
					  marginBottom:  '10px' 
				} 
			},
			{
				xtype: 'box',
				width: 20
			},
			{
				xtype: 	'button',
				text: 	'Cancelar',
				id: 		'btnCancelar',
				iconCls: 'cancelar',
				width: 	75,
				handler: function(boton, evento) {
					window.location.href='13forma5_fwd_ext.jsp';
				},
				style: { 
					  marginBottom:  '10px' 
				} 
			}
			]
		}
	];

var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		//layout: 'vbox',
		width: 890,
		height: 'auto',
		layoutConfig: {
			align:'center'
		},
		items: [
			{
				xtype: 'panel',
				name:'panelContenido',
				id:'panelContenido1',
				frame:true,
				width: 620,
				style: 'margin: 0 auto;',
				items: fpBusqAvanzada
			},
			
			NE.util.getEspaciador(20)
		]
	});	

});