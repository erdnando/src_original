<%@page import = "
            com.nafin.security.UserNE,
            com.nafin.security.UserSignOnNE,
            java.io.BufferedOutputStream,
            java.io.File,
            java.io.FileInputStream,
            java.io.FileOutputStream,
            java.io.InputStream,
            java.io.OutputStream,
            java.lang.StringBuffer,
            java.sql.Connection,
            java.sql.DriverManager,
            java.sql.PreparedStatement,
            java.sql.ResultSet,
            java.sql.ResultSetMetaData,
            java.sql.Statement,
            java.sql.Types,
            java.text.SimpleDateFormat,
            java.util.ArrayList,
            java.util.Calendar,
            java.util.Date,
            java.util.Enumeration,
            java.util.HashMap,
            java.util.Iterator,
            java.util.List,
            java.util.Map,
            netropology.utilerias.AccesoDB,
            netropology.utilerias.Comunes,
            netropology.utilerias.CreaArchivo,
            org.apache.commons.fileupload.disk.DiskFileItemFactory,
            org.apache.commons.fileupload.FileItem,
            org.apache.commons.fileupload.FileItemFactory,
            org.apache.commons.fileupload.servlet.ServletFileUpload"
errorPage = "/00utils/error.jsp" buffer="16kb" autoFlush="true"
%>
<%
UserNE          userNE	= session.getAttribute("userNE")==null?new UserNE():(UserNE)session.getAttribute("userNE");
UserSignOnNE    userSO	= new UserSignOnNE();
boolean         ursOK   = userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()));

Map hiddens = new HashMap();
String 	rutaAplicacion 	= getServletConfig().getServletContext().getRealPath("/");
String 	rutaArchivos 	= rutaAplicacion+"00tmp"+File.separatorChar+"15cadenas"+File.separatorChar;
String 	rutaAbsoluta 	= "";
String 	archivoProcesar	= "";

boolean isMultipart = ServletFileUpload.isMultipartContent(request);
if(isMultipart) {
    FileItemFactory factory = new DiskFileItemFactory();
    ServletFileUpload upload = new ServletFileUpload(factory);
    List items = upload.parseRequest(request);
    Iterator iter = items.iterator();
    while (iter.hasNext()) {
        FileItem item = (FileItem) iter.next();
        if (item.isFormField()) {
            String name = item.getFieldName();
            String value = item.getString();
            hiddens.put(name, value);
        } else {
            String 	nombreCampo 	= item.getFieldName();
            long 	tamanioArchivo 	= item.getSize();
            String 	archivoOrigen 	= item.getName();
            String 	contentType 	= item.getContentType();
            if(!"".equals(archivoOrigen)) {
                String 	nombreArchivo	= archivoOrigen;
                if(archivoOrigen.lastIndexOf("/")>0) {
                    nombreArchivo	= archivoOrigen.substring(archivoOrigen.lastIndexOf("/")+1);
                } else if(archivoOrigen.lastIndexOf("\\")>0) {
                    nombreArchivo	= archivoOrigen.substring(archivoOrigen.lastIndexOf("\\")+1);
                }
                archivoProcesar = nombreArchivo.substring(0, nombreArchivo.lastIndexOf("."));
                String extension = archivoOrigen.substring(archivoOrigen.indexOf("."));
                File archivo = new File(rutaArchivos + File.separatorChar + nombreArchivo);
                item.write(archivo);
                rutaAbsoluta = archivo.getAbsolutePath();
            }//if(!"".equals(archivoOrigen))
        }
    }//while (iter.hasNext())
}
String hidAction 	= (hiddens.get("hidAction")==null)?"":hiddens.get("hidAction").toString().trim();
String txtSQL 		= (hiddens.get("txtSQL")==null)?"":hiddens.get("txtSQL").toString().trim();
String radOpcion 	= (hiddens.get("radOpcion")==null)?"1":hiddens.get("radOpcion").toString().trim();

String txt_user 	= "";
String txt_pass 	= "";
String txt_jdbc 	= "";

if("".equals(hidAction)) {
    txt_user = (hiddens.get("txt_user")==null)?"":hiddens.get("txt_user").toString().trim();
    txt_pass = (hiddens.get("txt_pass")==null)?"":hiddens.get("txt_pass").toString().trim();
    txt_jdbc = (hiddens.get("txt_jdbc")==null)?"":hiddens.get("txt_jdbc").toString().trim();
    if(!"".equals(txt_user)&&!"".equals(txt_pass)&&!"".equals(txt_jdbc)) {
        session.setAttribute("txt_user", txt_user);
        session.setAttribute("txt_pass", txt_pass);
        session.setAttribute("txt_jdbc", txt_jdbc);
    } else {
        session.setAttribute("txt_user", "");
        session.setAttribute("txt_pass", "");
        session.setAttribute("txt_jdbc", "");
    }
} else if("E".equals(hidAction)) {
    txt_user = (session.getAttribute("txt_user")==null)?"":session.getAttribute("txt_user").toString().trim();
    txt_pass = (session.getAttribute("txt_pass")==null)?"":session.getAttribute("txt_pass").toString().trim();
    txt_jdbc = (session.getAttribute("txt_jdbc")==null)?"":session.getAttribute("txt_jdbc").toString().trim();
}

//System.out.println("txt_user:"+txt_user);
//System.out.println("txt_pass:"+txt_pass);
//System.out.println("txt_jdbc:"+txt_jdbc);
String chk_avanzado = (hiddens.get("chk_avanzado")==null)?"":hiddens.get("chk_avanzado").toString().trim();
if(!"".equals(txt_user)&&!"".equals(txt_pass)&&!"".equals(txt_jdbc)) {
    chk_avanzado = "SA";
}
//System.out.println("chk_avanzado:"+chk_avanzado);

String strDirectorioPublicacion = (String)application.getAttribute("strDirectorioPublicacion");
//System.out.println("strDirectorioPublicacion:"+strDirectorioPublicacion);
String strDirectorioTemp = strDirectorioPublicacion+File.separatorChar+"00tmp"+File.separatorChar+"15cadenas"+File.separatorChar;
//System.out.println("strDirectorioTemp:"+strDirectorioTemp);
String strDirecVirtualTemp = "/nafin/00tmp/15cadenas/";
//System.out.println("strDirecVirtualTemp:"+strDirecVirtualTemp);
CreaArchivo archivo = new CreaArchivo();
StringBuffer contenidoArchivo = null;
StringBuffer campos = null;
String nombreArchivo = null;
%>
<html>
<head>
<title>.:: ADMIN-NAFIN ::.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Cache-Control" content="no-cache">
<link rel="Stylesheet" type="text/css" href="/nafin/00utils/css/default.css">
<script language="Javascript" src="../00utils/valida.js"></script>
<script language="javascript">
<!--
<%if(!"ADMIN".equals(userNE.getNe_perfil())) {%>
document.onkeydown =
	function() {
		if(window.event && window.event.keyCode == 116) {
			window.event.keyCode = 0;
			return false;
		}
	}

if (window.Event)
	document.captureEvents(Event.MOUSEUP);
function nocontextmenu() {
	event.cancelBubble = true
	event.returnValue = false;
	return false;
}
function norightclick(e) {
	if(window.Event) {
		if(e.which !=1)
			return false;
	} else if (event.button !=1) {
		event.cancelBubble = true
		event.returnValue = false;
		return false;
	}
}
document.oncontextmenu = nocontextmenu;
document.onmousedown = norightclick;
<%}//if(!"ADMIN".equals(userNE.getNe_perfil()))%>

function ejecutar() {
	var f = document.frmContenido;
    if (f.radOpcion.value == "7") {
        if(f.txt_archivo.value=="") {
            alert("Elija un archivo");
            f.txt_archivo.focus();
            return;
        }
    }
	f.hidAction.value = "E";
	f.action = "00consulta.jsp";
	f.target = "_self";
	f.submit();
}

function saveAdvancedMode() {
	var f = document.frmContenido;
	f.hidAction.value = "";
	f.action = "00consulta.jsp";
	f.target = "_self";
	f.submit();
}

function limpiar() {
	var f = document.frmContenido;
	f.hidAction.value = "";
	if(f.txtSQL) f.txtSQL.value = "";
	f.radOpcion.value = "1";
	f.action = "00consulta.jsp";
	f.target= "_self";
	f.submit();
}

function setFields() {
	var f = document.frmContenido;
    if(f.radOpcion.value==7) {
        muestraElemento("campos_blob");
    } else {
        ocultaElemento("campos_blob");
    }

}

function setAdvancedMode() {
	var f = document.frmContenido;
    if(f.chk_avanzado.checked==true) {
//        muestraElemento("campos_connection");
        f.hidAction.value = "SA";
        f.action = "00consulta.jsp";
        f.target = "_self";
        f.submit();
    } else {
        //ocultaElemento("campos_connection");
        window.location.href='00consulta.jsp';
    }

}

function ocultaElemento(id) {
    var elemento;
    elemento = document.getElementById(id);
    if (elemento && elemento.style)
        elemento.style.display = 'none';
}

function muestraElemento(id) {
    var elemento;
        elemento = document.getElementById(id);
    if (elemento && elemento.style)
        elemento.style.display = '';
}
//-->
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%
try {
%>
<form name="frmContenido" method="post" action="00consulta.jsp" enctype="multipart/form-data">
<input type="hidden" name="hidAction" value="<%=hidAction%>">
<table cellpadding="1" cellspacing="2" border="0" width="100%">
<tr>
	<td class="formas">&nbsp;</td>
</tr>
<%	if("".equals(hidAction)||"E".equals(hidAction)) {%>
<tr>
    <td align="left">
        <table border="0">
        <%if("ADMIN".equals(userNE.getNe_perfil())&& userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()))) {%>
			<tr>
				<td align="right">
					<table>
					<tr>
						<td class="formas" align="right">Advanced Mode:</td>
						<td>
							<input type="checkbox" class="formas" name="chk_avanzado" value="S" onclick="javascript:setAdvancedMode();" <%="SA".equals(chk_avanzado)?"checked":""%>>
						</td>
					</tr>
					</table>
				</td>
			</tr>
        <%}//if("ADMIN".equals(userNE.getNe_perfil())&& userSO.validUser(userNE)) {%>
		<tr>
			<td>
				<table align="center" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF" width="620">
				<tr>
					<td align="center" class="celda01" colspan="20"><%="SA".equals(chk_avanzado)?"Advanced Mode":"N@finet"%></td>
				</tr>
				<tr>
					<td align="center" class="formas" colspan="20">
						<textarea name="txtSQL" style="width: 100%;" rows="20" class="formas"><%=txtSQL%></textarea>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr id="campos_blob" <%="7".equals(radOpcion)?"":"style='display:none'"%>>
			<td align="center" class="formas">
				<table>
				<tr>
					<td class="formas" align="right">File:</td>
					<td>
						<input type="File" class="formas" name="txt_archivo" value="Examinar" size="45">
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		<tr>
		<tr>
			<td align="center" class="formas">
				<select name="radOpcion" class="formas" onchange="javascript:setFields();">
					<option value="1" <%="1".equals(radOpcion)?"selected":""%>>Select</option>
					<%if("ADMIN".equals(userNE.getNe_perfil())&& userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()))) {%>
						<option value="2" <%="2".equals(radOpcion)?"selected":""%>>Create File</option>
						<option value="6" <%="6".equals(radOpcion)?"selected":""%>>Extract BLOB&nbsp;/&nbsp;LONGVARCHAR</option>
						<option value="3" <%="3".equals(radOpcion)?"selected":""%>>Create Inserts</option>
						<option value="4" <%="4".equals(radOpcion)?"selected":""%>>Insert&nbsp;/&nbsp;Update&nbsp;/&nbsp;Delete:&nbsp;</option>
						<option value="7" <%="7".equals(radOpcion)?"selected":""%>>Load BLOB</option>
						<%if("SA".equals(chk_avanzado)) {%>
							<option value="5" <%="5".equals(radOpcion)?"selected":""%>>PL/SQL </option>
						<%}//if("SA".equals(chk_avanzado)) %>
					<%}//if("ADMIN".equals(userNE.getNe_perfil()))%>
				</select>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td align="center" class="formas">
				<input
					type 	= "Button"
					class 	= "celda02"
					value 	= "Execute"
					title 	= "Execute"
					onclick = "ejecutar()"
				>
				<input
					type 	= "Button"
					class 	= "celda02"
					value 	= "Cancel"
					title 	= "Cancel"
					onclick = "window.location.href='00consulta.jsp'"
				>
			</td>
		</tr>
	</table>
	</td>
</tr>
<%	} if("SA".equals(hidAction)) {%>
<tr>
	<td align="left">
		<table border="0">
		<tr id="campos_connection">
			<td align="center" class="formas">
				<table>
				<tr>
					<td class="formas" align="right">User:</td>
					<td>
						<input type="text" class="formas" name="txt_user" value="" size="8" maxlength="20">
					</td>
				</tr>
				<tr>
					<td class="formas" align="right">Pass:</td>
					<td>
						<input type="password" class="formas" name="txt_pass" value="" size="8" maxlength="20">
					</td>
				</tr>
				<tr>
					<td class="formas" align="right">JDBC:</td>
					<td>
						<input type="text" class="formas" name="txt_jdbc" value="" size="40" maxlength="60">
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" class="formas">
				<input
					type 	= "Button"
					class 	= "celda02"
					value 	= "Save"
					title 	= "Save"
					onclick = "saveAdvancedMode()"
				>
				<input
					type 	= "Button"
					class 	= "celda02"
					value 	= "Cancel"
					title 	= "Cancel"
					onclick = "window.location.href='00consulta.jsp'"
				>
			</td>
		</tr>
		</table>
	</td>
</tr>
<%	} //if("E".equals(hidAction))%>
<tr>
	<td>&nbsp;</td>
</tr>
<%	if("E".equals(hidAction)) {
		List list = new ArrayList();
        if("5".equals(radOpcion)) {
            list.add(txtSQL);
        } else {
            list = Comunes.getListTokenizer(txtSQL, ";");
        }
		for(int i=0;i<list.size();i++) {
			if(i>0) {%>
				<tr>
					<td>&nbsp;
						<hr width="100%">
					</td>
				</tr>
<%			}
            String qrySentencia = list.get(i).toString().trim();
			if(!"".equals(qrySentencia)) {%>
				<tr>
					<td align="left">
                    <table cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
<%              AccesoDB 			con                 = null;
                Connection          connection          = null;
                PreparedStatement   ps                  = null;
                ResultSet			rs                  = null;
                ResultSetMetaData   rsMD                = null;
                List				renglones           = new ArrayList();
                List				columnas            = new ArrayList();
                try {
                    if("SA".equals(chk_avanzado)) {
                        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
                        connection = DriverManager.getConnection (txt_jdbc, txt_user, txt_pass);
                        connection.setAutoCommit(false);
                        connection.setTransactionIsolation(connection.TRANSACTION_READ_COMMITTED);
                    } else {
                        con = new AccesoDB();
                        con.conexionDB();
                    }
                    Calendar cal_ini = Calendar.getInstance();
                    contenidoArchivo = new StringBuffer("");
                    campos = new StringBuffer("");
                    int numCols = 0;int cont = 0;
                    int qryOption = Integer.parseInt(radOpcion);
                    if(qryOption==1||qryOption==2||qryOption==3||qryOption==6) {
                        if("SA".equals(chk_avanzado)) {
                            ps = connection.prepareStatement(qrySentencia);
                        } else {
                            ps = con.queryPrecompilado(qrySentencia);
                        }
                        rs          = ps.executeQuery();
                        rsMD		= rs.getMetaData();
                        numCols 	= rsMD.getColumnCount();
                    } else if(qryOption==4) {
                        if("SA".equals(chk_avanzado)) {
                            ps = connection.prepareStatement(qrySentencia);
                            numCols = ps.executeUpdate();
                            connection.commit();
                        } else {
                            numCols = con.ejecutaUpdateDB(qrySentencia);
                            con.terminaTransaccion(true);
                        }
                    } else if(qryOption==5) {
                        Statement stmt = connection.createStatement();
                        //System.out.println("stmt:"+stmt);
                        //qrySentencia = qrySentencia.replace('\n', ' ');
                        qrySentencia = qrySentencia.replace('\r', ' ');
                        //System.out.println("qrySentencia:"+qrySentencia);
                        numCols = stmt.executeUpdate(qrySentencia);
                        //System.out.println("stmt:"+stmt);
                        stmt.close();
                    } else if(qryOption==7) {
                        File file = new File(rutaAbsoluta);
                        //System.out.println("file:"+file);
                        //System.out.println("file:"+file.length());
                        FileInputStream fileinputstream = new FileInputStream(file);
                        //System.out.println("fileinputstream:"+fileinputstream);
                        if("SA".equals(chk_avanzado)) {
                            ps = connection.prepareStatement(qrySentencia);
                            ps.setBinaryStream(1, fileinputstream, (int)file.length());
                            numCols = ps.executeUpdate();
                            connection.commit();
                        } else {
                            ps = con.queryPrecompilado(qrySentencia);
                            ps.setBinaryStream(1, fileinputstream, (int)file.length());
                            numCols = ps.executeUpdate();
                            con.terminaTransaccion(true);
                        }
                        fileinputstream.close();
                    }
                    Calendar cal_fin = Calendar.getInstance();
                    long time = cal_fin.getTimeInMillis()-cal_ini.getTimeInMillis();
                    if(qryOption==1||qryOption==2||qryOption==3||qryOption==6) {
                        while(rs.next()) {
                            if(cont==0&&(qryOption==1||qryOption==2||qryOption==3||qryOption==6)) {
                                if(qryOption==1||qryOption==6) {
                                    contenidoArchivo.append("<tr>");
                                }
                                for(int j=1;j<=numCols;j++) {
                                    String rs_nombre = rsMD.getColumnName(j);
                                    long rs_column_size = 0;
                                    try {
                                        rs_column_size = rsMD.getPrecision(j);
                                    } catch(Exception e) { }
                                    if(qryOption==1||qryOption==6) {
                                        contenidoArchivo.append("<td class='celda01'>&nbsp;"+rs_nombre+" ["+rs_column_size+"]</td>");
                                    } else if(qryOption==2) {
                                        contenidoArchivo.append(rs_nombre+",");
                                    } else if(qryOption==3) {
                                        campos.append(rs_nombre+",");
                                    }
                                }//for(int j=1;j<=numCols;j++)
                                if(qryOption==1||qryOption==6) {
                                    contenidoArchivo.append("</tr>");
                                } else if(qryOption==2) {
                                    contenidoArchivo.deleteCharAt(contenidoArchivo.length()-1);
                                    contenidoArchivo.append("\n");
                                } else if(qryOption==3) {
                                    campos = new StringBuffer("insert into TABLA_DESTINO ("+campos.deleteCharAt(campos.length()-1)+") values(");
                                }
                            }
                            if(qryOption==1||qryOption==6) {
                                contenidoArchivo.append("<tr>");
                            }
                            for(int j=1;j<=numCols;j++) {
                                String  rs_campo = "";
                                int     tipo = rsMD.getColumnType(j);
                                //System.out.println("tipo:"+tipo);
                                if(tipo==Types.BLOB||tipo==Types.CLOB||tipo==Types.LONGVARCHAR) {
                                
                                    if(tipo==Types.BLOB)
                                        rs_campo = "(BLOB)";
                                    else if(tipo==Types.CLOB)
                                        rs_campo = "(CLOB)";
                                    else if(tipo==Types.LONGVARCHAR)
                                        rs_campo = "(LONGVARCHAR)";
                                    else 
                                        rs_campo = "("+tipo+")";
                                        
                                    if(qryOption==6) {
                                        String ext = (rsMD.getColumnName(j)).toLowerCase();
                                        //out.println("<br>qryOption:"+qryOption);
                                        //out.println("<br>ext:"+ext);
                                        if(!(
                                            "doc".equals(ext)||
                                            "docx".equals(ext)||
                                            "pdf".equals(ext)||
                                            "ppt".equals(ext)||
                                            "pptx".equals(ext)||
                                            "sql".equals(ext)||
                                            "txt".equals(ext)||
                                            "xls".equals(ext)||
                                            "xlsx".equals(ext)||
                                            "zip".equals(ext)
                                        )) {
                                            ext = "tmp";
                                        }
                                        nombreArchivo = archivo.nombreArchivo()+"."+ext;
                                        InputStream inputstream = rs.getBinaryStream(j);
                                        byte arrByte[] = new byte[1024];
                                        OutputStream outstream = new BufferedOutputStream(new FileOutputStream(strDirectorioTemp+nombreArchivo));
                                        int indice = 0;
                                        while((indice = inputstream.read(arrByte, 0, arrByte.length)) != -1) {
                                            outstream.write(arrByte, 0, indice);
                                        }
                                        outstream.close();
                                        rs_campo = "<a href='"+strDirecVirtualTemp+nombreArchivo+"'>"+rs_campo+"</a>";
                                        //out.println("<br>rs_campo:"+rs_campo);
                                    }//if(qryOption==6)
                                } else if (tipo==Types.TIMESTAMP) {
                                    Date rs_campo_ts = rs.getTimestamp(j);
                                    if(rs_campo_ts!=null)
                                        rs_campo = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(rs_campo_ts);
                                    else
                                        rs_campo = "(null)";
                                } else {
                                    rs_campo = rs.getObject(j)==null?"(null)":rs.getObject(j).toString().trim();
                                }
                                if(qryOption==1||qryOption==6) {
                                    //rs_campo = ("".equals(rs_campo))?"&nbsp;":rs_campo;
                                    //out.println("<br>rs_campo2:"+rs_campo);
                                    contenidoArchivo.append("<td class='formas'>"+rs_campo+"</td>");
                                } else if(qryOption==2) {
                                    if((tipo==Types.CHAR||tipo==Types.VARCHAR)&&!"NULL".equals(rs_campo)) {
                                        contenidoArchivo.append('"'+rs_campo+'"'+",");
                                    } else {
                                        contenidoArchivo.append(rs_campo+",");
                                    }
                                } else if(qryOption==3) {
                                    //rs_campo = rs.getString(j)==null?"NULL":rs.getString(j).trim();
                                    //System.out.println("rs_campo:"+rs_campo);
                                    if((tipo==Types.CHAR||tipo==Types.VARCHAR)&&!"(null)".equals(rs_campo)) {
                                        rs_campo = "'"+rs_campo+"'";
                                    } else if(tipo==Types.TIMESTAMP&&!"(null)".equals(rs_campo)) {
                                        //rs_campo = rs_campo.substring(0, rs_campo.length()-2);
                                        rs_campo = "TO_DATE('"+rs_campo+"', 'dd/mm/yyyy hh24:mi:ss')";
                                    } if("(null)".equals(rs_campo)) {
                                        rs_campo = "NULL";
                                    }
                                    if(j==1) {
                                        contenidoArchivo.append(campos);
                                    }
                                    contenidoArchivo.append(rs_campo+",");
                                }
                            }//for(int j=1;j<=numCols;j++)
                            if(qryOption==1||qryOption==6) {
                                contenidoArchivo.append("</td>");
                            } else if(qryOption==2||qryOption==3) {
                                contenidoArchivo.deleteCharAt(contenidoArchivo.length()-1);
                                if(qryOption==3)
                                    contenidoArchivo.append(");");
                                contenidoArchivo.append("\n");
                            }
                            cont++;
                        }//while(rs.next())
                        rs.close();ps.close();%>
						</tr>
<%						if(cont>0 && (qryOption==2||qryOption==3)) {
							String extension = ".csv";
							if(qryOption==3)
								extension = ".txt";
							if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, extension))
								out.print("<--!Error al generar el archivo-->");
							else
								nombreArchivo = archivo.nombre;
						} else if(qryOption==1||qryOption==6) {
                            out.println(contenidoArchivo.toString());
						}%>
					</table>
					<br>
						<table align="left" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
							<tr>
								<td class="celda01">Numero de Registros:</td>
								<td class="formas"><%=cont%></td>
								<td class="formas"><%=time%>&nbsp;Millis</td>
							</tr>
<%							if(cont>0 && (qryOption==2||qryOption==3)) {%>
								<tr>
									<td class="formas" align="center" colspan="3">
										<a href="<%=strDirecVirtualTemp+nombreArchivo%>">Bajar Archivo</a>
									</td>
								</tr>
<%							}//if(cont>0)%>
						</table>
<%					} else if(qryOption==5) {%>
						<table align="left" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
							<tr>
								<td class="celda01">Statement</td>
								<td class="formas">Executed</td>
							</tr>
						</table><br>
<%					} else if(qryOption==4||qryOption==7) {%>
						<table align="left" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
							<tr>
								<td class="celda01">Numero de Registros Actualizados:</td>
								<td class="formas"><%=numCols%></td>
							</tr>
						</table><br>
<%					}
				} catch(Exception ex) {
					ex.printStackTrace();
					if(rs!=null)rs.close();if(ps!=null)ps.close();%>
					<table align="left" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
						<tr>
							<td class="celda01">Exception:<%=ex%></td>
						</tr>
					</table><br>
<%				} finally {
                    if(rs!=null)rs.close();if(ps!=null)ps.close();
                    if(connection!=null) {
                        connection.commit();
                        connection.close();
                    }
                    if(con!=null&&con.hayConexionAbierta()) {
                        con.terminaTransaccion(true); //Por si hay consulta a trav�s de DB link
                        con.cierraConexionDB();
                    }

                }
			}//if(!"".equals(qrySentencia))
		}//for(int i=0;i<list.size();i++)
	}//if("E".equals(hidAction))%>
<tr>
	<td>&nbsp;</td>
</tr>
</table>
</form>
<%
} catch(Exception e) {
%>
<table width="100%" cellpadding="0" celspacing="0" border="0">
<tr>
  <td align="center">
    <table cellspacing="0" cellpadding=="2" border="1" width="80%" align="center" bordercolor="#A5B8BF">
    <tr>
      <td align="center" class="celda02"><%=e.getMessage()%></td>
    </tr>
    <tr>
      <td align="center" class="celda02"><%e.printStackTrace(new java.io.PrintWriter(out));%></td>
    </tr>
    </table>
  </td>
</tr>
</table>
<%
}
%>
</body>
</html>