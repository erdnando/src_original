/*
Modificaciones
isdate(), 2 mayo 2001, 13:05 hrs.
*/

// ______________________________________________________________________
// Revisa si un campo tiene un valor, de lo contrario

//var  idioma = "";

function esVacio(valor)
{
 var string1=valor;
 var temp = "";

 string = '' + string1;
 splitstring = string.split(" ");
 for(i = 0; i < splitstring.length; i++)
  temp += splitstring[i];

 if (temp.length==0)
  return true;
 else
  return false;
}


// ______________________________________________________________________
// Revisa si un campo tiene un numero

function isdigit(InString)
{
  if(esVacio(InString))
	  return (false);
  
  var RefString="1234567890";
  for(Count=0;Count < InString.length;Count++)
  {
    TempChar = InString.substring(Count,Count+1);
    if(RefString.indexOf(TempChar,0)==-1) 
    return false;
  }
  return true;
}


// ____________________________________________________________________________
// Valida que los datos del campo sean n�meros
// PARAMETROS:
// forma: nombre de la forma (this.form.name)
// campo: nombre del campo (this.name)
//
// SALIDAS:
// True: Si el campo tiene datos validos (�nicamente n�meros)
// False: Si el campo tiene datos invalidos 

function isInt(str)
{
 // if(str.length==0) 
 // return false;
//  var str = eval("document."+forma+"."+campo+".value")
  var RefString="0123456789";
//  var invalidos="                   & ' \ * � % # + � ~ [ ] { } � ? � ! ";

  for(Count=0;Count < str.length;Count++)
  {
    TempChar = str.substring(Count,Count+1);
    if(RefString.indexOf(TempChar,0)==-1)
    {
//      alert("El campo solo puede contener n�meros enteros");
//      eval("document."+forma+"."+campo+".focus()");
//      eval("document."+forma+"."+campo+".select()");
      return false;
    }
  }
  return true;
}

// ____________________________________________________________________________

function isDec(str)
{
  var RefString=".0123456789";
  var point	= 0;
  for(Count=0;Count < str.length;Count++)
  {
    TempChar = str.substring(Count,Count+1);
    if(RefString.indexOf(TempChar,0)==-1)
    { return false; }
	if(TempChar==".")
		point= point+1;
	if(parseInt(point)>1)	
    { return false; }
  }
  return true;
}


// ____________________________________________________________________________

function isChar(str)
{
  var RefString=" A�BCDE�FGHI�JKLMN�O�PQRSTU�VWXYZa�bcde�fghi�jklmn�o�pqrst�uvwxyz&";

  for(Count=0;Count < str.length;Count++)
  {
    TempChar = str.substring(Count,Count+1);
    if(RefString.indexOf(TempChar,0)==-1)
    { return false; }
  }
  return true;
}

function isCharInt(str)
{
  var RefString="ABCDEFGHIJKLMN�OPQRSTUVWXYZ01234567890";

  for(Count=0;Count < str.length;Count++)
  {
    TempChar = str.substring(Count,Count+1);
    if(RefString.indexOf(TempChar,0)==-1)
    { return false; }
  }
  return true;
}

// ____________________________________________________________________________
// Valida que los datos del campo sean caracter�s para el Tel�fono o Fax
// PARAMETROS:
// forma: nombre de la forma (this.form.name)
// campo: nombre del campo (this.name)
//
// SALIDAS:
// True: Si el campo tiene datos validos (�nicamente caract�res del Tel�fono o Fax)
// False: Si el campo tiene datos invalidos 

function isTelFax(forma,campo)
{
 // if(str.length==0) 
 // return false;
  var str = eval("document."+forma+"."+campo+".value")
  var RefString="-()0123456789 ";
  var invalidos="                   & ' \ * � % # + � ~ [ ] { } � ? � ! ";

  for(Count=0;Count < str.length;Count++)
  {
    TempChar = str.substring(Count,Count+1);
    if(RefString.indexOf(TempChar,0)==-1)
    {
      alert("El campo solo puede contener n�meros");
      eval("document."+forma+"."+campo+".focus()");
      eval("document."+forma+"."+campo+".select()");
      return false;
    }
  }
  return true;
}



// ____________________________________________________________________________
// Valida que los datos del campo sean caract�res(char), 
//     n�meros (int) y simbolos (symb) permitidos.
// PARAMETROS:
// forma: nombre de la forma (this.form.name)
// campo: nombre del campo (this.name)
//
// SALIDAS:
// True: Si el campo tiene datos validos (�nicamente caract�res, n�meros y simbolos)
// False: Si el campo tiene datos invalidos 

function isCharIntSymb(forma,campo)
{
 // if(str.length==0) 
 // return false;
  var str = eval("document."+forma+"."+campo+".value")
  var RefString=" ()-/;.,01234567890Aa��BbCcDdEe��FfGgHhIi��JjKkLlMmNn��Oo��PpQqRrSsTtUu��VvWwXxYyZz&";
  var invalidos="                   ' \ * � % # + � ~ [ ] { } � ? � ! ";

  for(Count=0;Count < str.length;Count++)
  {
    TempChar = str.substring(Count,Count+1);
    if(RefString.indexOf(TempChar,0)==-1)
    {
      alert("El campo no puede contener ninguno de los siguientes caracteres: \n\n"+invalidos);
      eval("document."+forma+"."+campo+".focus()");
      eval("document."+forma+"."+campo+".select()");
      return false;
    }
  }
  return true;
}

// ____________________________________________________________________________
// Valida que los datos del campo sean caract�res(char), 
//     n�meros (int) y simbolos (symb) permitidos.
// PARAMETROS:
// forma: nombre de la forma (this.form.name)
// campo: nombre del campo (this.name)
//
// SALIDAS:
// True: Si el campo tiene datos validos (�nicamente caract�res, n�meros y simbolos)
// False: Si el campo tiene datos invalidos 
function nombre_archivo_valido(forma,campo){
	  var str = eval("document."+forma+"."+campo+".value")
	  var iPosIndex = PosBackSlash(str);
	  var strFile = "";
	  strFile = str.substr(iPosIndex+1);
	  var RefString=" .()-_01234567890AaBbCcDdEeFfGgHhIiJjKkLlMmNn��OoPpQqRrSsTtUuVvWwXxYyZz";
	  var invalidos="  ' \ * � % # + � ~ [ ] { } � ? � ! � � � � � � � � � � & ; , / & ";
	
	  for(Count=0;Count < strFile.length;Count++)
	  {
	    TempChar = strFile.substring(Count,Count+1);
	    if(RefString.indexOf(TempChar,0)==-1)
	    {
	      alert("El nombre del archivo no puede contener ninguno de los siguientes caracteres: \n\n"+invalidos);
	      eval("document."+forma+"."+campo+".focus()");
 	      eval("document."+forma+"."+campo+".select()");
	      return false;
	    }
	  }
	return true;
}



// ____________________________________________________________________________
//Funcion que compara dos fechas (dd/mm/aa)
//Regresa:
//  0 si son iguales.
//  1 si la primera es mayor que la segunda
//  2 si la segunda mayor que la primera
// -1 si son fechas invalidas


function datecomp(date1,date2)
{
  var exito,dd1,mm1,aa1,dd2,mm2,aa2,pos,tempdate;
  var d1,m1,a1,d2,m2,a2;

  if(!((isdate(date1)) && (isdate(date2))))
    exito = -1;
  else
  {
   pos = date1.indexOf("/",0);
   d1 = date1.substring(0,pos);
   date1 = date1.substring(pos+1,date1.length);
   pos = date1.indexOf("/",0);
   m1 = date1.substring(0,pos);
   a1 = date1.substring(pos+1,date1.length);
   pos = date2.indexOf("/",0);
   d2 = date2.substring(0,pos);
   date2 = date2.substring(pos+1,date2.length);
   pos = date2.indexOf("/",0);
   m2 = date2.substring(0,pos); 
   a2 = date2.substring(pos+1,date2.length);
   dd1 = parseInt(d1,10);
   mm1 = parseInt(m1,10);
   aa1 = parseInt(a1,10);
   dd2 = parseInt(d2,10);
   mm2 = parseInt(m2,10);
   aa2 = parseInt(a2,10);


 if((aa1 >= 0) && (aa1 <= 99))
   aa1 += 1900;

 if((aa2 >= 0) && (aa2 <= 99))
   aa2 += 1900;

 if(aa1 >= aa2)
    {
   if(aa1 == aa2)
      {
        if(mm1 >= mm2)
        {
    if(mm1 == mm2)
          {
            if(dd1 >= dd2)
            {
     if(dd1 ==  dd2)
                exito = 0; //son fechas iguales
              else
                exito = 1; //es mayor por dias
            }
            else // es menor por dias
     exito = 2;
          }
          else //es mayor por meses
            exito = 1;
        }
        else //es menor por meses
          exito = 2;
      }
      else //es mayor por anios
        exito = 1;
    }
    else //es menor por anios
      exito = 2;
  }
  return(exito);
}

// ____________________________________________________________________________
// Descripcion: Verifica que la fecha sea v�lida con el formato dd/mm/aaaa


function isdate(strValue)
{
//Para validar la fecha en el formato mm/dd/aaaa
//escribir en la variable strFormatoFecha = "US"

//Para validar la fecha en el formato dd/mm/aaaa
//escribir en la variable strFormatoFecha = ""
  var strFormatoFecha = "";
  var strFecha;
  var arrFecha;
  var strDia;
  var strMes;
  var strAnio;
  var intDia;
  var intMes;
  var intAnio;
  var booFound = false;
 // var cmpFecha = objFecha;
  var arrSeparador = new Array("-","/",".");
  var elementos;
  var error = 0;
  strFecha = strValue;
/////////////////////////////////////////////////
 // if (strFecha.length < 1)
 // { return true; }
//JAVIER	
//  alert(strValue);
  if (strFecha.length < 11 && strFecha.length > 5)
  { 
/////////////////////////////////////////////////
  for (elementos = 0; elementos < arrSeparador.length; elementos++)
  {
    if (strFecha.indexOf(arrSeparador[elementos]) != -1)
    {
      arrFecha = strFecha.split(arrSeparador[elementos]);
// Si los elementos del arreglo son diferente de 3 elementos ejemplo: 01/01/200/34/23/4
      if (arrFecha.length != 3)
      {
        error = 1;
        return false;
      }

      else
      {
        strDia = arrFecha[0];
        strMes = arrFecha[1];
        strAnio = arrFecha[2];
		if(strAnio.length != 4)
			return false;
      }
      booFound = true;
    }

  }

//alert(strDia+" "+strMes+" "+strAnio)

/////////////////////////////////////////////////

  if (booFound == false)
  {
    if (strFecha.length>5)
    {
      strDia = strFecha.substr(0, 2);
      strMes = strFecha.substr(2, 2);
      strAnio = strFecha.substr(4);
    }
  }
/////////////////////////////////////////////////
// Si el a�o es de 2 digitos agrega 20 ejemplo: si es 01 entonces 2001
  if (strAnio.length == 2)
  { strAnio = '20' + strAnio; }

// Si el a�o es 0000
  if (parseInt(strAnio) <= 1900 )
  { return false }

/////////////////////////////////////////////////

  // Formato (mm/dd/aaaa)
  if (strFormatoFecha == "US")
  {
    strTemp = strDia;
    strDia = strMes;
    strMes = strTemp;
  }



/////////////////////////////////////////////////

  intDia = parseInt(strDia, 10);

  if (isNaN(intDia))
  {
    error = 2;
    return false;
  }

/////////////////////////////////////////////////

  intMes = parseInt(strMes, 10);

  if (isNaN(intMes))
  {
    error = 3;
    return false;
  }


/////////////////////////////////////////////////

  intAnio = parseInt(strAnio, 10);

  if (isNaN(intAnio))
  {
    error = 4;
    return false;
  }

/////////////////////////////////////////////////

  if (intMes>12 || intMes<1)
  {
    error = 5;
    return false;
  }

/////////////////////////////////////////////////

  if ((intMes == 1 || intMes == 3 || intMes == 5 || intMes == 7 || intMes == 8 || intMes == 10 || intMes == 12) && (intDia > 31 || intDia < 1))
  {
    error = 6;
    return false;
  }

/////////////////////////////////////////////////

  if ((intMes == 4 || intMes == 6 || intMes == 9 || intMes == 11) && (intDia > 30 || intDia < 1))
  {
    error = 7;
    return false;
  }

/////////////////////////////////////////////////

  if (intMes == 2)
  {
    if (intDia < 1)
    {
      error = 8;
      return false;
    }

    if (esAnioBisiesto(intAnio) == true)
    {
      if (intDia > 29)
      {
        error = 9;
        return false;
      }
    }

    else
    {
      if (intDia > 28)
      {
        error = 10;
        return false;
      }
    }
  }
/////////////////////////////////////////////////
return true;
}

else
{ return false }

}
//_____________________________________________________________________________
// A�o bisiesto
function esAnioBisiesto(intAnio)
{
  if (intAnio % 100 == 0)
  {
    if (intAnio % 400 == 0)
    { return true; }
  }

  else
  {
    if ((intAnio % 4) == 0)
    { return true; }
  }

  return false;
}

/*
respaldo de isdate()

 function isdate()
{
  var pos,dia,mes,ano,fecha,longi,flag;

  fecha=strValue;
  //alert(fecha);
  pos=fecha.indexOf("/",0);
  if(pos==-1)
    return(false);

  dia=fecha.substring(0,pos);
  fecha=fecha.substring(pos+1,fecha.length);
  pos=fecha.indexOf("/",0);
  if(pos==-1 || (dia.length>2))
    return(false);

  mes=fecha.substring(0,pos);
  ano=fecha.substring(pos+1,fecha.length);
  if((mes.length > 2) || (ano.length > 4))
    return(false);
  if(ano.length != 4)
    return(false);
 
  flag=isdigit(dia);
  if(!flag)
    return(false);

  flag=isdigit(mes);
  if(!flag)
    return(false);

  flag=isdigit(ano);
  if(flag)
  {
    if((parseInt(mes,10)>=1 && parseInt(mes,10)<=12) && (parseInt(dia,10)>=1 && parseInt(dia,10)<=31))
    {
      if((",1,3,5,7,8,10,12,".indexOf(","+ parseInt(dia) +",",0)> -1) && (parseInt(mes)>31))
        return(false);
      else
     {
       if((",4,6,9,11,".indexOf(","+ parseInt(dia) +",",0)>-1) && (parseInt(mes)>30))
         return(false);

       if(parseInt(mes)==2)
       {
         if((parseInt(ano) % 4)==0 && (parseInt(dia)>29))
           return(false);
         else
         {
           if((parseInt(ano) % 4)!=0 && (parseInt(dia)>28))
             return(false);
         } // no bisiesto
       } // mes de febrero
     } //meses de 30 dias
   } //meses de 31 dias
   else
    return(false);
 }
  return true;    //no error
}
*/
// ____________________________________________________________________________
   ////////////////////////////////////////////////////////////
   // Funcion : roundOff
   // Desc    : redondea un valor flotante a los decimales pedidos
   // value  : es el valor a redondear
   // precision: son los decimales que se le asignarar a el valor
   ////////////////////////////////////////////////////////////

 function roundOff(valor, precision)
 {
  valor = "" + valor //convert value to string
  precision = parseInt(precision);

  var iBandera = 0;  
  if (valor < 0)
  {
   valor = Math.abs(valor);
   iBandera = 1;
  }   
  var whole = "" + Math.round(valor * Math.pow(10, precision));
  var decPoint  = whole.length - precision;
  if(decPoint > 0)
  {
   result = whole.substring(0, decPoint);
   result += ".";
   result += whole.substring(decPoint, whole.length);
  }
  else
  {
   if (decPoint < 0)
   {
    for (i = decPoint; i<0; i++)
    whole = "0" + whole;
   }
   whole = "0." + whole;
   result = whole;
  }
  if (isNaN(result) ||result == ".0")
         return "0.0";
      if(result.charAt(0) == ".")
       result = "0" + result;
  if (iBandera == 1)
   result = "-" + result;
  return result;
 }
 
// ____________________________________________________________________________

 function checaVacios(frm) {
  var i;
  var bandera=true;
  for(i=0; i<frm.elements.length; i++) {
   if(frm.elements[i].value=="") {
    bandera=false;
    alert('El Campo no puede estar vacio');
    frm.elements[i].focus();
    break;
   }
  }
  if(bandera==true)
   return true;
  else
   return false;
 }



function revisaFecha(campoFecha)
{
 if (!isdate(campoFecha.value) && campoFecha.value!='')
 {
  alert("\nLa fecha es incorrecta.\nVerifique que el formato sea dd/mm/aaaa");
  campoFecha.focus();
  return false;
 }
 return true;
}


// ______________________________________________________________________
// Revisa si un campo tiene un valor flotante, en caso negativo
// Se redirecciona al campo y se regresa un false



function revisaFlotante(campoFlotante, decimales) {
 var numero = campoFlotante.value;
 if (isNaN(numero) || numero == "") {
  alert ("El valor introducido no parece ser un n�mero v�lido");
  campoFlotante.focus();
 } else {
  if (numero.indexOf('.') == -1) numero += ".";
   dectext = numero.substring(numero.indexOf('.')+1, numero.length);
  if (dectext.length > decimales) {
   alert ("Por favor introduzca un n�mero con m�ximo " + decimales + " decimales.");
   campoFlotante.focus();
   return false;
  } else {
   return true;
	}
 }
}

// ______________________________________________________________________
// Revisa si un campo tiene un valor, de lo contrario
// Se redirecciona al campo y se regresa un false



function revisaCampo(campo)
{
 var cadena=campo.value;

 if (esVacio(cadena))
 {
  alert("\nNo debe dejar el campo en blanco.\n\n Por favor capt�relo.");
  campo.focus();
  return false;
 }
 else
  return true;
}

// ______________________________________________________________________
// Revisa si un campo tiene un valor, de lo contrario
// Se redirecciona al campo y se regresa un false



function revisaEntero(campo)
{
 var numero=campo.value;

 if (!isdigit(numero))
 {
  alert("El valor no es un n�mero v�lido.");
  campo.focus();
  return false;
 }
 else
  return true;
}

function revisaEnteroCaracter(campo)
{
 var valor=campo.value;

 if (!isCharInt(valor))
 {
  alert("El valor no es un n�mero v�lido.");
  campo.select();
  campo.focus();
  return false;
 }
 else
  return true;
}

function comparaFechas(fechaIni, fechaFin)
{
	var string1 = "";
 	var temp = "";

	string1 = fechaIni;
// 	string = '' + string1;	
 	string = "" + string1;	
	splitstring = string.split("/");
 	var fechaInicial= new Date();
	fechaInicial.setDate(splitstring[0]);
	fechaInicial.setMonth(splitstring[1]-1);
	fechaInicial.setYear(splitstring[2]);
	string1 = fechaFin;
// 	string = '' + string1;	
 	string = "" + string1;	
	splitstring = string.split("/");
 	var fechaFinal= new Date();
	fechaFinal.setDate(splitstring[0]);
	fechaFinal.setMonth(splitstring[1]-1);
	fechaFinal.setYear(splitstring[2]);

	if (fechaInicial<=fechaFinal)
	    return true;
	else
		return false;
}


  ////////////////////////////////////////////////////////////
  //	Funcion	: capturaFecha(strFecha)
  //	Desc	 	: valida que la sintaxis de la fecha sea correcta
  //				  en el formato dd/mm/yyyy
  // objeto	: es el objeto con el campo a validar
  ////////////////////////////////////////////////////////////
function	capturaFecha(objeto)
{
	var iDia;
	var iMes;
	var iAnio;
	
	var strDia 	= "";
	var strMes 	= "";
	var strAnio = "";
	var caso= 0;
	var strFecha = objeto.value;
	var error = false;
	
	formatoFecha = /\d{1,2}\/\d{1,2}\/\d{4}/;
	//alert("idioma="+idioma);
	if (formatoFecha.test(strFecha) == false)
	{
	
	alert("\nLa fecha es incorrecta.\nVerifique que el formato sea dd/mm/aaaa y que sea una fecha valida");
	objeto.focus();
	return false;
	}

	for( i= 0; i < strFecha.length; i++)
	{
		if (strFecha.charAt(i) != "/" )
		{
			switch(caso)
			{
				case 0: strDia = strDia + strFecha.charAt(i);
					break;
				case 1: strMes = strMes + strFecha.charAt(i);
					break;
				case 2: strAnio= strAnio + strFecha.charAt(i);
					break;				
			}				
		}
		else
			caso++;
	}

	iDia 	= parseInt(strDia);
	iMes	= parseInt(strMes);
	iAnio	= parseInt(strAnio);

	if (iMes <= 12 && iDia <= 31) {
		switch (iMes) {
			case 1: case 3: case 5: case 7: case 8: case 10: case 12: 
					if (iDia > 31)
						error = true;
					break;
			case 4: case 6: case 9: case 11: 
			 		if (iDia > 30)
						error = true;
					 break;
			case 2 : 
					if (iAnio % 4 == 0) {
						if (iDia > 29)
							error = true;
					}
				  	else {
					  if (iDia > 28)
							error = true;
				  }
				  break;
		}
	}
	else
  		error = true;
  	
  	if (error)
  	{
	alert("\nLa fecha es incorrecta.\nVerifique que el formato sea dd/mm/aaaa y que sea una fecha valida");
	objeto.focus();
	return false;			
	}
  		
	return true;
}




// ______________________________________________________________________
// Formato N�mero... Ej. 4,500.54

function formatoFlotante(num,operacion)
{
 if (operacion=="aplicar")  //Aplica formato 
 {
  num = num.toString().replace(/\$|\,/g,'');
  if(isNaN(num)) num = "0";
  cents = Math.floor((num*100+0.5)%100);
  num = Math.floor((num*100+0.5)/100).toString();
  if(cents < 10) cents = "0" + cents;
  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
  num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
  return (num + "." + cents);
 }
 else //operacion="quitar" --> Quitar el formato
 {
  num = num.toString().replace(/\$|\,/g,'');
  return num;
 }
}

	function formatoDecimal(numero,precision,agrupar){
			 
			  var numeroOriginal = numero;	
			  numero = numero.toString().replace(/\$\,/g,'');
			  if(precision < 0 	) precision 	= Math.abs(precision);
			  if(isNaN(numero)	) return numeroOriginal;
			  
			  var str				= numero.toString();
			  if(str.indexOf('e') != -1 || str.indexOf('E') != -1){
			  	  return numeroOriginal;
			  }
			  
			  var	esNegativo		= numero < 0?true:false;
			  
			  numero 				= Math.abs(numero).toString();
			  
			  var partes 			= numero.split('.');
			  var enteros 			= partes[0];
			  var decimales		= partes[1]; 
			  
			  var hayEnteros 		= partes.length >0?true:false;
			  var hayDecimales	= partes.length >1?true:false;
 
			  if(precision == 0 && hayDecimales){
				  var digitos 	= decimales.split("");
				  if(digitos[0] >= 5){
					  enteros = (parseInt(enteros,10) + 1).toString();
				  }
			  }
			  
			  if(precision > 0){
			  	  
			  	  if(hayDecimales){
				  
					  digitos 	= decimales.split("");
					  decimales = "";
	  
					  for( var i=0; i<precision && i<digitos.length; i++ ){
						  decimales += digitos[i];
					  }
						  
					  if(precision>digitos.length){
						  for(var i=precision-digitos.length;i>0;i--){
							  decimales += "0";
						  }
					  }else if(digitos.length>precision){
						  
						  if(digitos[precision] >= 5 ){
							  decimales = ( parseInt(decimales,10) + 1 ).toString();
						  }
						  
						  if(precision < decimales.length){
							  enteros = (parseInt(enteros,10) + parseInt(decimales.charAt(0),10)).toString();
							  decimales = decimales.substring(1,decimales.length);
						  }else if(precision > decimales.length){
						  	  for(var i=precision-decimales.length;i>0;i--){
						  	  	  decimales = "0"+decimales;
						  	  }
						  }
					  }  
				  
				  }else{
				  	  decimales = "";
				  	  for(var i=0;i<precision;i++){
				  	  	  decimales += "0";
				  	  }
				  	  
				  }
			  }
			  
			  var digitos 	= enteros.split("");
			  enteros 	= "";
			  for(var i=digitos.length-1,contador=1;i>=0;i--,contador++){
				  if(contador == 4 && agrupar == true){
					  enteros 	= "," + enteros ;
					  contador 	= 1;
				  }
				  enteros = digitos[i] + "" + enteros;
			  }
 
			  return (esNegativo?"-":"")+enteros + (precision > 0?"."+decimales:"");
			  
	}

//-----------------------------------------------------------------------//
// Valida la extension de un archivo

function formatoValido(str, tipo){
	var iPosIndex = PosBackSlash(str);
	var strTemp = "";
	var strFile = "";
	var bPto = false;
	var bExt = false;
	var strExt = "";
   
	strFile = str.substr(iPosIndex+1);
	
//	alert(strFile);
//	alert(str);

	for(var i=0;i<strFile.length-1;i++){
		strTemp = strFile.charAt(i);   
		if(strTemp == "."){
			if(bExt == false){
				bExt=true;
				strExt = strFile.substr(i+1);
//				alert(strExt);
				if(validaExt(strExt, tipo)) return true;
				else return false;
			}
			else return false;
		}
	}
}

// ____________________________________________________________________________
function validaExt(str, tipo){
	switch(tipo){
		case 1 :
			if(str.toLowerCase() == "txt") 
				return true;
			break;
		case 2 :
			if(str.toLowerCase() == "avi" || str.toLowerCase() == "mov" || str.toLowerCase() == "ram" || str.toLowerCase() == "rm" || str.toLowerCase() == "asf") 
				return true;
			break;   
		case 3 :
			if(str.toLowerCase() == "pdf") 
				return true;
			break;
		case 4 :
			if(str.toLowerCase() == "xls" || str.toLowerCase() == "txt" || str.toLowerCase() == "pdf" || str.toLowerCase() == "doc" || str.toLowerCase() == "dot" || str.toLowerCase() == "ppt" || str.toLowerCase() == "csv")
				return true;
			break;
		case 5 :
			if(str.toLowerCase() == "xls") 
				return true;
			break;
		case 6 :
			if(str.toLowerCase() == "zip" || str.toLowerCase() == "txt" )
				return true;
			break;
		case 7 :
			if(str.toLowerCase() == "zip") 
				return true;
			break;
		default :
			return false;
	}
}

// ____________________________________________________________________________
function PosBackSlash(str)
{
  var buscar = "\\";
  var n = str.length;
//alert(str.lastIndexOf(buscar,n-1));
  return(str.lastIndexOf(buscar,n-1));
}

//___________________________________________________________________
function getBrowserName(){
	var lsBrowser = navigator.appName;

	if (lsBrowser.indexOf("Microsoft") >= 0){
		lsBrowser = "MSIE";
	}
	else if (lsBrowser.indexOf("Netscape") >= 0){
		lsBrowser = "NETSCAPE";
	}
	else{
		lsBrowser = "DESCONOCIDO";
	}
   return lsBrowser;
}

// ____________________________________________________________________________

function FechaValida(forma,campo){  
	var hoy = new Date();
	var dia = hoy.getDate();
	var mes = hoy.getMonth() + 1;
  
	if(navigator.appName =='Microsoft Internet Explorer')
		var anio = hoy.getYear();
	else
		var anio = hoy.getYear() + 1900;
  
	var fechahoy = dia+"/"+mes+"/"+anio;
	var campo = eval("document."+forma+"."+campo);
	if(campo.value != ""){
		if(!isdate(campo.value) ){ 
			alert('Fecha invalida\n ejemplo: 01/12/2000\n dd/mm/aaaa');
			campo.focus();
			return false;
		}
		else{
			if(datecomp(campo.value, fechahoy) == 1){
				alert("La fecha debe ser menor al d�a de hoy."); 
				campo.focus();   
				return false;
			}
		}
		return true; 
	}
}

/*
respaldo del HTML
onBlur="if(document.form0.Fecha_del_poder_notarial.value !=''){ if(isdate(document.form0.Fecha_del_poder_notarial.value)==false){alert('Fecha invalida\n ejemplo: 01/12/2000\n dd/mm/aaaa');document.form0.Fecha_del_poder_notarial.focus()} };"
*/

//______________________________________________________________-
function decimales(campo, decim)
{

  var valor = campo.value
  //var Frm = forma;

  //decallowed = 6;  // N�mero de decimales permitidas
  //dectext = valor.substring(valor.indexOf('.')+1, valor.length);

  //primeros = valor.indexOf(".",0); //cantidad de n�meros antes del punto decimal
/*
  if (valor == "")
  {
    alert("Por favor escribe la cantidad con "+ decim +" decimales.");
    campo.focus();
  }
*/
// Si no es un n�mero
  if (isNaN(valor))
  {
    alert("Por favor escribe correctamente la cantidad con "+ decim +" decimales.");
    campo.focus();
    campo.select();
  }
/*
  else if (valor == 0.0)
  {
    alert("Por favor escribe la cantidad.");
   //campo.focus();
   // campo.select();
  }
*/
  else
  { campo.value = roundOff(valor,decim) }

}


//___________________________________________________________________




/**
 * Realiza la validaci�n del RFC
 * @param cadena Cadena con el RFC a validar
 * @param persona Tipo de persona a validar: 'fisica' o ' moral'
 * @return true si el RFC es valido o false de lo contrario
 */
function validaRFC(cadena,persona) {
	var strRFC = cadena;
	var arrRFC;
	var strNombre;
	var strFecha;
	var strClave;
	var elementos;
/////////////////////////////////////////////////
	if (strRFC == null || strRFC == "") {
		return false;
	}
	
	arrRFC = strRFC.split("-");
	
	if (arrRFC.length != 3) {
		return false;
	}
	
	strNombre = (arrRFC[0]==null)?"":arrRFC[0];
	strFecha = (arrRFC[1]==null)?"":arrRFC[1];
	strClave = (arrRFC[2]==null)?"":arrRFC[2];  // Homoclave
	
	if (persona == "moral") {
		if (strNombre.length != 3) {
			return false;
		}
	} else if (persona == "fisica") {
		if (strNombre.length != 4) {
			return false;
		}
	} else {
		return false;
	}
	
	if (isChar(strNombre) == false) {
		return false;
	}

	if (strFecha.length != 6 || isFechaRFC(strFecha) == false) {
		return false;
	}
	
	if (strClave.length != 3 || isCharInt(strClave) == false) {
		return false;
	}
	
	return true;
}



// ____________________________________________________________________________

function isFechaRFCHoy(fecha)
{  
  var hoy = new Date();
  var dia = hoy.getDate();
  var mes = hoy.getMonth() + 1;
  var anio = hoy.getYear();
  var fechahoy = dia+"/"+mes+"/"+anio;
  var strFecha = fecha;
    
    if (strFecha.length == 6)
    {
      strAnio = strFecha.substr(0,2);
      strMes = strFecha.substr(2,2);
      strDia = strFecha.substr(4,2);
     // alert("A�o: "+strAnio+" Mes: "+strMes+" Dia: "+strDia)
     if (strAnio <= 25) { strAnio = "20"+strAnio} 
     else { strAnio = "19"+strAnio} 
     
     strFecha = strDia+"/"+strMes+"/"+strAnio
   
  
      if (Date.parse(strFecha) >= Date.parse(fechahoy) )
      { 
        //alert("La fecha del RFC debe ser menor al d�a de hoy."); 
       // fecha.focus();   
      return false;
      }

    else
    { return true; }
    
    }
  
    else
    { return false; }
}


// ____________________________________________________________________________

function isFechaRFC(strValue)
{
  var strFecha = strValue;
  var strDia;
  var strMes;
  var strAnio;
  var intDia;
  var intMes;
  var intAnio;

/////////////////////////////////////////////////

  if (strFecha.length == 6)
  {
    strAnio = strFecha.substr(0,2);
    strMes = strFecha.substr(2,2);
    strDia = strFecha.substr(4,2);
   // alert("A�o: "+strAnio+" Mes: "+strMes+" Dia: "+strDia)
  }

  else
  {
    error = 1;
    return false;
  }

  /////////////////////////////////////////////////

  intDia = parseInt(strDia, 10);

  if (isNaN(intDia))
  {
    error = 2;
    return false;
  }

/////////////////////////////////////////////////

  intMes = parseInt(strMes, 10);

  if (isNaN(intMes))
  {
    error = 3;
    return false;
  }


/////////////////////////////////////////////////

  intAnio = parseInt(strAnio, 10);

  if (isNaN(intAnio))
  {
    error = 4;
    return false;
  }

/////////////////////////////////////////////////

  if (intMes>12 || intMes<1)
  {
    error = 5;
    return false;
  }

/////////////////////////////////////////////////

  if ((intMes == 1 || intMes == 3 || intMes == 5 || intMes == 7 || intMes == 8 || intMes == 10 || intMes == 12) && (intDia > 31 || intDia < 1))
  {
    error = 6;
    return false;
  }

/////////////////////////////////////////////////

  if ((intMes == 4 || intMes == 6 || intMes == 9 || intMes == 11) && (intDia > 30 || intDia < 1))
  {
    error = 7;
    return false;
  }

/////////////////////////////////////////////////

  if (intMes == 2)
  {
    if (intDia < 1)
    {
      error = 8;
      return false;
    }

    if (esAnioBisiesto(intAnio) == true)
    {
      if (intDia > 29)
      {
        error = 9;
        return false;
      }
    }

    else
    {
      if (intDia > 28)
      {
        error = 10;
        return false;
      }
    }
  }
  /////////////////////////////////////////////////
  return true;

}

//___________________________________________________________________

function campoRFC(campo,persona)
{
  if(campo.value != "")
  {
  
    switch (persona)
    {
      case "moral":

        if(validaRFC(campo.value,'moral') == false)
        {
          alert("Por favor escriba correctamente el RFC en mayusculas y separado por guiones\nen el formato NNN-AAMMDD-XXX donde:\n\nNNN: son las iniciales del nombre de la empresa\nAAMMDD: es la fecha de creaci�n de la empresa menor al d�a de hoy\nXXX: es la homoclave")
          campo.focus();
          campo.select();
        }

      break;

      case "fisica":

        if(validaRFC(campo.value,'fisica') == false)
        {
          alert("Por favor escriba correctamente el RFC en mayusculas y separado por guiones\nen el formato NNNN-AAMMDD-XXX donde:\n\nNNNN: son las iniciales del nombre y apellido\nAAMMDD: es la fecha de nacimiento menor al d�a de hoy\nXXX: es la homoclave")
          campo.focus();
          campo.select();
        }

      break;

    }
  }
}

function fnCampoRFC(campo, persona)	{
	var lbOK = true;
  if(campo.value != "")	{
  
    switch (persona)	{
      case "moral":

        if(validaRFC(campo.value,'moral') == false)	{
          alert("Por favor escriba correctamente el RFC en mayusculas y separado por guiones\nen el formato NNN-AAMMDD-XXX donde:\n\nNNN: son las iniciales del nombre de la empresa\nAAMMDD: es la fecha de creaci�n de la empresa menor al d�a de hoy\nXXX: es la homoclave")
          campo.focus();
          campo.select();
		  lbOK = false;
        }
		//\n\nNota: Si no conoce la homoclave puede omitirla.
      break;

      case "fisica":

        if(validaRFC(campo.value,'fisica') == false)	{
          alert("Por favor escriba correctamente el RFC en mayusculas y separado por guiones\nen el formato NNNN-AAMMDD-XXX donde:\n\nNNNN: son las iniciales del nombre y apellido\nAAMMDD: es la fecha de nacimiento menor al d�a de hoy\nXXX: es la homoclave")
          campo.focus();
          campo.select();
		  lbOK = false;
        }

      break;
    }
  }
  return lbOK;
}

//_________________________________________________________________________
// Funci�n que valida n�meros con enteros y decimales dados por el usuario
// Restricci�n: la validaci�n solo aplica a eventos onBlur de Javascript

// Sintaxis:
// forma(objeto): this.form.name;
// campo(objeto): si es normal el valor es: this.name
//                si es como arreglo el valor es: document.forms[0].elements['micampo[0]']
// formato(entero): es el formato del nombre del campo,
//                  si es normal el valor debe ser 0, si es como arreglo el valor debe ser 1.
// parte_entera(entero): n�mero m�ximo de posiciones antes del punto decimal
// parte_decimal(entero): n�mero m�ximo de posiciones despues del punto decimal

// Ejemplo: Valida que sea un n�mero con 10 enteros y 3 decimales (como arreglo)
// <form name="miforma">
// <input type="text" name="micampo[0]" size="20" onBlur="validaNumeroFlotante(this.form.name, document.forms[0].elements['micampo[0]'], 1, 10, 3);">
// <input type="text" name="micampo[1]" size="20" onBlur="validaNumeroFlotante(this.form.name, document.forms[0].elements['micampo[1]'], 1, 10, 3);">
// </form>

// Ejemplo: Valida que sea un n�mero con 10 enteros y 3 decimales (normal)
// <form name="miforma">
// <input type="text" name="micampo1" size="20" onBlur="validaNumeroFlotante(this.form.name, this.name, 0, 10, 3);">
// <input type="text" name="micampo2" size="20" onBlur="validaNumeroFlotante(this.form.name, this.name, 0, 10, 3);">
// </form>

// Creada el 28 de mayo del 2001 por rrosales@interimagen.com.mx
// Modificaciones:
// 31/may/2001 rrosales@interimagen.com.mx
// 01/ago/2001 rrosales@interimagen.com.mx


function validaNumeroFlotante(forma, campo, formato, parte_entera, parte_decimal)
{
  // si el formato del nombre es normal
  if (formato == 0)
  { var campo = eval("document."+forma+"."+campo); }

  // si el formato del nombre es como arreglo
  if (formato == 1)
  { var campo = campo; }

  var cadena_total = parte_entera + parte_decimal + 1;
  var cadena = campo.value;

  // si la cadena esta vacia
  if (cadena=="")
  {
    //cadena = "0.";
    //for (var i = 0 ; i < parte_decimal; i++)
    //{  cadena += "0"; }
    //campo.value = cadena;
  }

  // si la cadena tiene datos
  else
  {
  // Si tiene ceros a la izquierda se los elimina
    for (var h = 0 ; h < cadena.length; h++)
    {
      numero = cadena.substr(h,1)
      numero_siguiente = cadena.substr(h+1,1)

      if (numero == 0 && numero_siguiente != ".")
      {
        sin_cero = cadena.substring(h+1,cadena.length-1)
       // alert("sin_cero(if): "+sin_cero)
      }

      else
      {
        sin_cero = cadena.substring(h,cadena.length)
       // alert("sin_cero(else): "+sin_cero)
        break;
      }

    // alert("h: "+h+"\nCadena.length: "+cadena.length+"\nnumero: "+numero+"\nnumero_siguiente: "+numero_siguiente+"\nsin_cero: "+sin_cero)
    }

    cadena = sin_cero;
    campo.value = cadena;
    //return;

    // si es un numero valido...
    if (!isNaN(cadena))
    {
      var enteros = 0;
      var decimales = 0;
      var valor_decimal = cadena.substring(cadena.indexOf('.')+1, cadena.length);

      // si no tiene punto se agrega el punto y los ceros decimales
      if (cadena.indexOf('.') == -1)
      {
        cadena += ".";
        for (var i = 0 ; i < parte_decimal; i++)
        {  cadena += "0"; }

         campo.value = cadena;

        // si la cadena es mayor al tama�o permitido
        if (cadena.length > cadena_total)
        {
          alert("El n�mero es muy grande.\nPor favor escriba un n�mero menor o igual a "+parte_entera+" enteros y "+parte_decimal+" decimales.")
          campo.focus();
          campo.select();
          return;
        }
      }

      // si tiene punto
      else
      {
        // si no tiene enteros
        if (cadena.substring(0,cadena.indexOf('.')).length == 0)
        {
          cadena = "0" +cadena ;
          campo.value = cadena;
        }

        // si no tiene decimales
        if (valor_decimal.length < parte_entera)
        {
          for (var i = valor_decimal.length ; i < parte_decimal; i++)
          {  cadena += "0"; }

          campo.value = cadena;
        }

        valor_entero = cadena.substring(0,cadena.indexOf('.'))
        valor_decimal = cadena.substring(cadena.indexOf('.')+1, cadena.length)

        // alert("enteros: "+valor_entero.length+"\ndecimales:  "+valor_decimal.length)
        // return;

        // si la parte entera es mayor a la definida por el usuario
        if( valor_entero.length > parte_entera)
        {
          alert("El n�mero tiene: "+valor_entero.length+" enteros y "+valor_decimal.length+" decimales.\nPor favor escriba un n�mero menor o igual a "+parte_entera+" enteros y "+parte_decimal+" decimales.");
          campo.focus();
          campo.select();
          return;
        }

        // si la parte decimal es mayor a la definida por el usuario
        if(valor_decimal.length > parte_decimal)
        {
           decimal_valido = valor_decimal.substr(0,parte_decimal)
           cadena_correcta = valor_entero +"."+decimal_valido
           campo.value = cadena_correcta;
        }
      }
    }

    // si no es un n�mero v�lido
    else
    {
      cadena = "0.";
      for (var i = 0 ; i < parte_decimal; i++)
      {  cadena += "0"; }
      campo.value = cadena;
    }
  }
}




// Quita las comas a una determinada cantidad
function eliminaFormatoFlotante(valor)
{
 var string1=valor;
 var temp = "";

 string = '' + string1;
 splitstring = string.split(",");
 for(i = 0; i < splitstring.length; i++)
  temp += splitstring[i];
 
  return temp;
}

//____________________________________________________________________________
// Funci�n para Obtener el plazo en d�as que hay entre dos fechas

 var Tmes = new Array();
 var sCadena;
 Tmes[1] = "31"; Tmes[2] ="28"; Tmes[3]="31"; Tmes[4]="30"; Tmes[5]="31"; 
 Tmes[6]="30";
 Tmes[7] = "31"; Tmes[8] ="31"; Tmes[9]="30"; Tmes[10]="31";Tmes[11]="30"; 
 Tmes[12]="31";
 
  function fnDesentramaFecha(Cadena){
 
   var Dato;
   sCadena      = Cadena
   tamano       = sCadena.length;
   posicion     = sCadena.indexOf("/");
   Dato         = sCadena.substr(0,posicion);
   cadenanueva  = sCadena.substring(posicion + 1,tamano);
   sCadena      = cadenanueva;
 
   return Dato;
}
 

   function mtdCalculaPlazo(sFecha,sFecha2){
       var iResAno,DiaMes,DiasDif=0,iCont=0,iMes,DiaMesA,iBan=0;
       var DiasPar,Dato,VencDia,VencMes,AltaMes,AltaDia;
          sCadena   = sFecha
          AltaDia   = fnDesentramaFecha(sCadena);
            AltaMes  = fnDesentramaFecha(sCadena);
            AltaAno   = sCadena;
            sCadena     = sFecha2
            VencDia     = fnDesentramaFecha(sCadena);
            VencMes     = fnDesentramaFecha(sCadena);
            VencAno     = sCadena;
            Dato  = AltaMes.substr(0,1);
            if (Dato ==0){
               AltaMes = AltaMes.substr(1,2)
            }
            Dato  = AltaDia.substr(0,1);
            if (Dato ==0){
                      AltaDia = AltaDia.substr(1,2)
            }
            Dato  = VencDia.substr(0,1);
            if (Dato ==0){
               VencDia = VencDia.substr(1,2)
            }
            Dato  = VencMes.substr(0,1);
            if (Dato ==0){
              VencMes = VencMes.substr(1,2)
            }
            while (VencAno != AltaAno || VencMes != AltaMes || VencDia != AltaDia){
    iCont++;
                while(AltaMes != VencMes || VencAno != AltaAno){
         iBan=1;
          iResAno = AltaAno % 4;
          if (iResAno==0){
          Tmes[2] =29;
          }
          else{
          Tmes[2] =28;
          }
                            if (iCont == 1){
            DiaMesA = Tmes[AltaMes];
            DiasDif = DiaMesA - AltaDia;
            //alert("DiasDi1"+DiasDif);
          }
          else{
            DiaMesA  = Tmes[AltaMes];
            DiasDif += parseInt(DiaMesA);
            //alert("DiasDi2"+DiasDif);
          }
          if (AltaMes==12){
            AltaAno++;
            AltaMes=1;
          }else{
          AltaMes++;
          }
   
         iCont++;
    } // fin de while
        if (AltaMes == VencMes && VencAno == AltaAno){
        if (iBan==0){
         DiasDif = parseInt(VencDia) - parseInt(AltaDia);
         AltaDia = VencDia;
        }
        else if (iBan=1){
            DiasDif += parseInt(VencDia);
            AltaDia = VencDia;
         }
      }
 }
 return DiasDif;
 }

//  validaEntero(objeto)
//  
// Esta funci�n valida n�meros enteros
// Restricci�n: La validaci�n solo aplica a eventos onBlur de Javascript
//
// Sintaxis
// 	Objeto: Referencia al campo de entrada ( INPUT )
// 	
// Ejemplo:
// 	<form name="miforma">
// 		<input type="text" name="numeroEntero" size="20" onBlur="validaEntero(this);">
// 	</form>
//
// JSHD

function validaEntero(objeto){
	
	var numero = objeto.value.toString();
		
	// Remover espacios a ambos lados de los numeros
	numero=numero.replace(/^[ \t]*(\+)?[ \t]*([^ \t]*)[ \t]*$/,"$1$2");

	//Verificar que sea un entero valido
	if( !numero.match("^(\\+)?[0-9]*$" ) || numero.match("^[ +\t]+$") ){
		alert('El n�mero debe ser un entero positivo');
		objeto.focus();
		objeto.select();
		return;
	}
		
	//Quitar el signo de m�s
	numero=numero.replace(/^\+(.*)/,"$1");
	//Remover ceros a la izquierda
	if(numero.match("^[0]+$")) {
		numero="0";
	} else {
		numero=numero.replace(/^[0]*(.*)/,"$1");
	}
	objeto.value=numero;
	
	return;
}

/*
* IHJ
* Calcula fecha+dias
* fecha: dd/mm/yyyy
* dias: entero positivo o negativo
* ejem:  sumaFechaDias('01/01/2007', '60') 		01/01/2007+60 = 02/03/2007 
* ejem:  sumaFechaDias('01/01/2007', '-60') 	01/01/2007-60 = 02/11/2006
*/ 
function sumaFechaDias(fecha, dias) {
	dias = parseInt(dias, 10);
	
	var TDate = new Date();
	var dia = 	parseInt(fecha.substring(0,2), 10);
	var mes = 	parseInt(fecha.substring(3,5), 10);
	var agno = 	parseInt(fecha.substring(6,10), 10);
	
	TDate.setDate(1);
	TDate.setYear(agno);
	TDate.setMonth(mes-1);
	TDate.setDate(dia);
	
	TDate.setDate(TDate.getDate()+dias);
	
	var _agno 	= TDate.getYear();
	var _dia 	= TDate.getDate();
	var _mes	= TDate.getMonth()+1;
	if(_dia<10)
		_dia = '0'+_dia;
	if(_mes<10)
		_mes = '0'+_mes;
	var fechaMasPlazo = _dia +'/'+_mes+'/'+_agno;
	return fechaMasPlazo;
}

/*
* IHJ
* Calcula fechaInicio-fechaFin, resultado dias reales
* fechas: dd/mm/yyyy
*/ 
function restaFechas(fechaIni, fechaFin) {
	var TDateIni = new Date();
	var TDateFin = new Date();
	
	TDateIni.setDate(1);
	TDateIni.setYear(parseInt(fechaIni.substring(6,10),10));
	TDateIni.setMonth(parseInt(fechaIni.substring(3,5),10)-1);
	TDateIni.setDate(parseInt(fechaIni.substring(0,2),10));
	
	TDateFin.setDate(1);
	TDateFin.setYear(parseInt(fechaFin.substring(6,10),10));
	TDateFin.setMonth(parseInt(fechaFin.substring(3,5),10)-1);
	TDateFin.setDate(parseInt(fechaFin.substring(0,2),10));
	
	var resta = (TDateFin.getTime()-TDateIni.getTime())/86400000;
	
	return resta;
}

/*
* IHJ
* ventana
*/ 
function ventana(url) {
	window.open(url, TARGET='_blank', "name=popup,scrollbars=yes,resizable=yes,locationbar=no,toolbar=no,statusbar=no,titlebar=no");
}
//
//  validaCorreo(direccion)
//  
// Esta funcion valida una direcccion simple de correo, de acuerdo al RFC822, pero no permite
// usar cuentas de correo que vengan sin el nombre de dominio.
//
// Parametros
// 	direccion: Cadena de texto con la direccion de correo a validar
// 	
// Resultado:
//    Devuelve true (boolean) si la direccion de correo simple cumple con el estandar RFC822, 
// 	devuelve false (booelan) en caso contrario.
//
// JSHD
function validaCorreo(direccion){
	// Validar que existan datos
	if(direccion == null || direccion  == ""){
		return false;
	}
	// Existen nombre y dominio en la direccion
	var posicionArroba = direccion.indexOf("@");
	// Extraer nombre local
	var nombreLocal = null;
	if(posicionArroba == -1){
		nombreLocal = direccion;
	}else{
		nombreLocal = direccion.substring(0,posicionArroba);
	}	
	// Extraer dominio
	var dominio = null;
	if(posicionArroba != -1 ){
		dominio = direccion.slice(posicionArroba+1);
	}
	// Verificar que no haya espacios en blanco
	var espacios="\r\t\n ";
	for(var i=0;i<espacios.length;i++){
		var caracter = espacios.charAt(i);
		if( direccion.indexOf(caracter) != -1 ){
			return false;
		}
	}
	// Verificar que no haya caracteres ilegales en nombre local
	var caracteresIlegales='\"(),:;<>@[\\]';
	for(var i=0;i<caracteresIlegales.length;i++){
		var caracter = caracteresIlegales.charAt(i);
		if( nombreLocal.indexOf(caracter) != -1 ){
			return false;
		}
	}
	// Verificar que el nombre del dominio no venga vacio
	if(dominio == null || dominio == "" ){
		return false;
	}
	// Verificar que no haya caracteres ilegales en el dominio
	for(var i=0;i<caracteresIlegales.length;i++){
		var caracter = caracteresIlegales.charAt(i);
		if( dominio.indexOf(caracter) != -1 ){
			return false;
		}
	}
	return true;
}

//
// validaCorreoCompleto(direccion)
//  
// Esta funcion valida una direcccion completa de correo.
// nombre_usuario + @ + servidor + dominio
//
// Parametros
// 	direccion: Cadena de texto con la direccion de correo a validar
// 	
// Resultado:
//  Devuelve true (boolean) si la direccion de correo simple cumple con las validaciones de la expresi�n regular, 
// 	devuelve false (booelan) en caso contrario.
//
// JRSW
function validaCorreoCompleto(direccion){
	if (/^(?:(?:[\w\.\-_]+@[\w\d]+(?:\.[\w]{2,6})+)[,;]?\s?)+$/.test(direccion)){
		return true;
	} else {
		return false;
	}	
}

//--------------------------------------------------------------------
function Valida_Texto(myfield, e, tipovalida){

	  var key;
	  var keychar;
	  var cadena_valida;

	  if (window.event)
	    key = window.event.keyCode;
	  else if (e)
	    key = e.which;
	  else
	    return true;


	  keychar = String.fromCharCode(key);

	  if (tipovalida.toString().toLowerCase()=='r') //acepta s�lo reales positivos
	    cadena_valida = "0123456789.";
	  else if (tipovalida.toString().toLowerCase()=='ea') //acepta s�lo enteros positivos,letras y acentos
	    cadena_valida = "kgjwopbvc�kgjfabcdefghijklmn�opqrstuvwxyz�����1234567890,.- ";
	  else if (tipovalida.toString().toLowerCase()=='e') //acepta s�lo enteros positivos y letras
	    cadena_valida = "kgjwopbvc�kgjfabcdefghijklmn�opqrstuvwxyz1234567890,.- ";
	  else if (tipovalida.toString().toLowerCase()=='f') //acepta s�lo enteros positivos y letras
	    cadena_valida = "0123456789/";
    else if (tipovalida.toString().toLowerCase()=='sior') //acepta s�lo enteros positivos y letras
	    cadena_valida = "0123456789-";
    else if (tipovalida.toString().toLowerCase()=='n') //acepta s�lo enteros positivos y letras
	    cadena_valida = "0123456789";
    else if (tipovalida.toString().toLowerCase()=='np') //acepta s�lo enteros positivos y letras
	    cadena_valida = "0123456789-dm";
	else if (tipovalida.toString().toLowerCase()=='fac') //caracteres validos para una factura
	    cadena_valida = "0123456789-kgjwopbvc�kgjfabcdefghijklmn�opqrstuvwxyz";
	else if(tipovalida.toString().toLowerCase()=='obs'){
		var regExprEsAlfaNumerico = /^([a-z]|[0-9]|[_#&@������.:,-^\s*$)(])+$/i;
		if (regExprEsAlfaNumerico.test(keychar.toLowerCase()) || key==8 || key==13 || key==0)
	    	return true;
	  	else
	    	return false;
	}
  // --solo numeros

	  if ((cadena_valida).indexOf(keychar.toLowerCase()) > -1 || key==8 || key==13 || key==0)
	    return true;
	  else
	    return false;

   }

//Establece cuantos enteros y cuantos decimales podras escicbir en el campo
function ValidaFormatFloat(myfield, e, tamEnteros, tamDecimal){

	  var key;
	  var keychar;
	  var cadena_valida;

	  if (window.event)
	    key = window.event.keyCode;
	  else if (e)
	    key = e.which;
	  else
	    return true;


	  keychar = String.fromCharCode(key);
	  cadena_valida = "0123456789.";  

	  
	  if(myfield.value=='' && keychar=='.') return false;
	  if ((cadena_valida).indexOf(keychar.toLowerCase()) > -1 || key==8 || key==13 || key==0){
	    var valor = myfield.value
		 valor = sinformatoMoneda(valor);
		 
		 if(valor.indexOf('.')==-1){
			if(valor.length>=tamEnteros){
				if(keychar.toLowerCase()=='.' && valor.indexOf(keychar.toLowerCase())==-1)
					return true;
				else 
					return false				
			}
		 }else{
			if(valor.indexOf('.')!=-1 && keychar.toLowerCase()=='.') 
				return false;
			
			var valorEnt =valor.substring(0,valor.indexOf('.'));
			var valorDec =valor.substring(valor.indexOf('.'),valor.length);
			
			if(valorEnt.length>=tamEnteros){				
				if(keychar.toLowerCase()=='.' && valor.indexOf(keychar.toLowerCase())!=-1)				
					return false
			}
			if(valorDec.length>tamDecimal){
				return false;
			}
		 }
		 return true;
	  }else
	    return false;
   }

/*
Da formato de moneda en pesos con separacion de miles.
prefix.- define el signo que quieres se ponga en un principio ($)
decimales.- numero de decimales al que sera recortado el valor en caso de que sean mas de los indicados
*/
function formatoMoneda(numObj,prefix,decimales, enteros){
	num = numObj.value;

	if(num!=''){
		//num = num.replace(/([^0-9\.\-])/g,'');
		num = num.replace(/\$|\,/g,'');
		
		if(num=='' || isNaN(num)){
			alert("el valor no es num�rico");
			numObj.value= '';
			return false;
		}
		prefix = prefix || '';
		num += '';
		var splitStr = num.split('.');
		var splitLeft = splitStr[0];
		var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
		
		if(isNaN(splitLeft)) {
			splitLeft = "";
		}
		if(isNaN(splitRight)){
			splitRight = "";
		}else if(splitRight.length >3){
			if(decimales){
				if(isNaN(decimales)){
				  splitRight = splitRight.substring(0,3);
				}else{
				  splitRight = splitRight.substring(0,decimales+1);
				}
			}else{
				if(decimales==0){
					splitRight = "";
				}else{
					splitRight = splitRight.substring(0,3);
					
				}
			}
		}
		
		if(splitLeft.length > 0 ){
			if(enteros && enteros>0 && splitLeft.length>enteros){
				alert("valor num�rico incorrecto, m�ximo "+enteros+" enteros");
				numObj.value= '';
				//numObj.focus();
				return '';
			}
		}
		
		var regx = /(\d+)(\d{3})/;
		while (regx.test(splitLeft)) {
			splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
		}

		splitLeft = splitLeft.length > 0 && splitLeft=='0'? '0': splitLeft;
		numObj.value = prefix + splitLeft + splitRight;
		return prefix + splitLeft + splitRight;
	}else{
		return '';
	}
}


function sinformatoMoneda(text){
	if(text!=''){
	return text.replace(/\$|\,/g,'');
	}else{
	return '';
	}
}


/*Valida formato de hora en Formato 24hrs
*objHora: objeto donde se introduce la hora a validar
*tipoValidacion: 1.- valida formato HH:MM    2.-valida formato HH:MM:SS
*/
function validaHora24(objHora, tipoValidacion)
	{
		hora=objHora.value;
		if(tipoValidacion==1){
			if (hora=='') {return false;}
			if (hora.length>5 || hora.length!=5) {
				alert("Introducir Hora en formato HH:MM");
				objHora.focus();
				return false;
			}
			a=hora.charAt(0) //<=2
			b=hora.charAt(1) //<4
			c=hora.charAt(2) //:
			d=hora.charAt(3) //<=5
			e=hora.charAt(4) //
			if ( !isdigit(a) || !isdigit(b) || (a==2 && b>3) || (a>2)) {
				alert("Hora incorrecta, introduzca valor entre 00:00 Y 23:59 horas");
				objHora.focus();
				return false;
			}
			if (!isdigit(d) || !isdigit(e) || d>5) {
				alert("Hora incorrecta, introduzca valor entre 00:00 Y 23:59 horas");
				objHora.focus();
				return false;
			}
			if (c!=':') {
				alert("Introducir Hora en formato HH:MM");
				objHora.focus();
				return false;
			}
		}else if(tipoValidacion==2){
			if (hora=='') {return false;}
			if (hora.length>8 || hora.length!=8) {
				alert("Introducir Hora en formato HH:MM:SS");
				objHora.focus();
				return false;
			}
			a=hora.charAt(0); //<=2
			b=hora.charAt(1); //<4
			c=hora.charAt(2); //:
			d=hora.charAt(3); //<=5
			e=hora.charAt(4); //
			f=hora.charAt(5); //:
			g=hora.charAt(6); //<=5
			h=hora.charAt(7); //
			if (!isdigit(a) || !isdigit(b) || (a==2 && b>3) || (a>2)) {
				alert("Hora incorrecta, introduzca valor entre 00:00:00 Y 23:59:59 horas");
				objHora.focus();
				return false;
			}
			if (!isdigit(d) || !isdigit(e) || d>5) {
				alert("Hora incorrecta, introduzca valor entre 00:00:00 Y 23:59:59 horas");
				objHora.focus();
				return false;
			}
			if (!isdigit(g) || !isdigit(h) || g>5) {
				alert("Hora incorrecta, introduzca valor entre 00:00:00 Y 23:59:59 horas");
				objHora.focus();
				return false;
			}
			if (c!=':' || f!=':') {
				alert("Introducir Hora en formato HH:MM:SS");
				objHora.focus();
				return false;
			}
		}
		return true;
	}


