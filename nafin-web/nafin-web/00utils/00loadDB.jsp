<%@	page import = "
		com.nafin.security.UserNE,
		com.nafin.security.UserSignOnNE"
	errorPage = "/00utils/error.jsp" buffer="16kb" autoFlush="true"%>
<%
UserNE 		userNE	= session.getAttribute("userNE")==null?new UserNE():(UserNE)session.getAttribute("userNE");
UserSignOnNE 	userSO	= new UserSignOnNE();
boolean         ursOK   = userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()));
%>
<html>
<head>
<link rel="Stylesheet" type="text/css" href="/nafin/00utils/css/default.css">
<script language="JavaScript">
<!--
<%if(!"ADMIN".equals(userNE.getNe_perfil())) {%>
document.onkeydown = 
	function() {
		if(window.event && window.event.keyCode == 116) {
			window.event.keyCode = 0;
			return false;
		}
	}

if (window.Event) 
	document.captureEvents(Event.MOUSEUP); 
function nocontextmenu() { 
	event.cancelBubble = true 
	event.returnValue = false; 
	return false; 
} 
function norightclick(e) { 
	if(window.Event) { 
		if(e.which !=1)
			return false; 
	} else if (event.button !=1) { 
		event.cancelBubble = true 
		event.returnValue = false; 
		return false; 
	}
}
document.oncontextmenu = nocontextmenu; 
document.onmousedown = norightclick;
<%}//if(!"ADMIN".equals(userNE.getNe_perfil()))%>

	function valCadena(objeto) {
		if(objeto.value=="") {
			alert("Capture el campo: "+objeto.name+".");
			objeto.focus();
			objeto.select();
			throw "Error";
		}
		return true;
	}
   
   function valCadena(objeto,objetoFocus,nombreCampoMsg) {
		if(objeto.value=="") {
			alert("Capture el campo: "+nombreCampoMsg+".");
			objetoFocus.focus();
			objetoFocus.select();
			throw "Error";
		}
		return true;
	}

	function cargar () {
		var f    = document.frmContenido;
		var bOK  = true;
		
      var enlaceEstaSeleccionado = getShowStatusElemento("enlaces_tr");
      var textoEstaSeleccionado  = getShowStatusElemento("texto_tr"); 
		
      if(enlaceEstaSeleccionado){
         f.Tabla.value  = f.enlaces.value;
      }else if(textoEstaSeleccionado){
         f.Tabla.value = f.texto.value;
      }
      
		try {
			valCadena(f.Archivo);
			if(enlaceEstaSeleccionado)	valCadena(f.Tabla,f.enlaces,"Enlace");
			if(textoEstaSeleccionado) 	valCadena(f.Tabla,f.texto,"Tabla");
			valCadena(f.Separador);
			//valCadena(f.Password);
			
			if(f.Separador.value == ":"){
				alert("No se puede usar los dos puntos \":\" como separador de campos");
				return;
			}
			
		} catch(exception) {
			bOK = false;
		}
		
		if(bOK) {
			f.action = "00loadDBa.jsp";
			f.target = "_self";
			f.submit();
		}
	}

	function ocultaElemento(id) {
		var elemento;
		elemento = document.getElementById(id);
		if (elemento && elemento.style)
			elemento.style.display = 'none';
	}

	function muestraElemento(id) {
		var elemento;
			elemento = document.getElementById(id);
		if (elemento && elemento.style)
			elemento.style.display = '';
	}
	
	function muestraEnlace() {
		ocultaElemento("texto_tr");
		document.frmContenido.texto.value="";
		muestraElemento("enlaces_tr");
	}

	function muestraTabla() {
		ocultaElemento("enlaces_tr");
		document.frmContenido.enlaces.value="";
		muestraElemento("texto_tr");
	}

	function getShowStatusElemento(id){
		var elemento;
		elemento = document.getElementById(id);
		if (elemento && elemento.style.display == "")
			return true;
		return false;
	}
//-->
</script>
</head>
<body>
<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache");
response.setHeader("Expires", "Thu, 29 Oct 1969 17:04:19 GMT");
try {
%>
<form name="frmContenido" method="post" action="00loadDBa.jsp" enctype="multipart/form-data">
    <input type="hidden" name="Tabla" value="">
<table width="100%" cellpadding="1" cellspacing="2" border="0">
<tr>
	<td align="center">
	<table>
		<tr>
			<td class="formas" align="right">File:</td>
			<td>
				<input type="File" class="formas" name="Archivo" value="Examinar" size="45">
			</td>
		</tr>
		<tr id="enlaces_tr" >
			<td class="formas" align="right">Table:</td>
			<td>
				<select name="enlaces" class="formas"  STYLE="width: 20em">
					<option value="">Select Table</option>
					<option value="COMTMP_DOCTOS_PUB_BANSEFI">Bansefi</option>
					<option value="COMTMP_DOCTOS_PUB_CFE">CFE</option>
					<option value="COMTMP_DOCTOS_PUB_FIDE">FIDE</option>
					<option value="COMTMP_DOCTOS_PUB_FONACOT">Fonacot</option>
					<option value="COMTMP_DOCTOS_PUB_GDF">GDF</option>
					<option value="COMTMP_DOCTOS_PUB_IMSS">IMSS</option>
					<option value="COMTMP_DOCTOS_PUB_L01">Liverpool</option>
					<option value="COMTMP_DOCTOS_PUB_PRESIDENCIA">Presidencia</option>
					<!--option value="COMTMP_DOCTOS_PUB_P01" disabled >Falta especificar</option-->
					<!--option value="COMTMP_DOCTOS_PUB_P02" disabled >Falta especificar</option-->
					<!--option value="COMTMP_DOCTOS_PUB_P03" disabled >Falta especificar</option-->
					<option value="COMTMP_DOCTOS_PUB_SIOR">SIOR</option>
					<option value="COMTMP_DOCTOS_PUB_VITRO">VITRO</option>
					<option value="COMTMP_DOCTOS_PUB_VW">Volkswagen</option>
					<option value="COMTMP_DOCTOS_PUB_WS">Webservice</option>
				</select>
				<input type="Button" class="celda02" value="Set Table" title="Set Table" onclick="javascript:muestraTabla()" style="width: 150px">
			</td>
		</tr>
		<tr id="texto_tr" style="display:none">
			<td class="formas" align="right">Table:</td>
			<td>
				<input type="text" class="formas" name="texto" value="" STYLE="width: 20em">
				<input type="Button" class="celda02" value="Select Table" title="Select Table" onclick="javascript:muestraEnlace()" style="width: 130px">
			</td>
		</tr>
		<tr>
			<td class="formas" align="right">Token:</td>
			<td>
				<input type="text" class="formas" name="Separador" value="|" size="1" maxlength="1" style="text-align:center">
			</td>
		</tr>
<!--
		<tr>
			<td class="formas" align="right">PassWord:</td>
			<td>
				<input type="password" class="formas" name="Password" value="" size="8" maxlength="8" >
			</td>
		</tr>
-->		
	</table>
	</td>
</tr>
<%if("ADMIN".equals(userNE.getNe_perfil())) {%>
<tr>
	<td align="center">
		<input type="Button" class="celda02" value="Load File" title="Load File" onclick="cargar()">
		<input type="Button" class="celda02" value="Cancel" title="Cancel" onclick="window.location.href='00loadDB.jsp'">
	</td>
</tr>
<%}//if("ADMIN".equals(userNE.getNe_perfil()))%>
</table>
<%
} catch(Exception e) { 
%>
<table width="100%" cellpadding="0" celspacing="0" border="0">
<tr>
  <td align="center">
    <table cellspacing="0" cellpadding=="2" border="1" width="80%" align="center" bordercolor="#A5B8BF" class="tabla">
    <tr>
      <td align="center" class="celda02"><%=e.getMessage()%></td>
    </tr>
    </table>
  </td>
</tr>
<%e.printStackTrace();%>
</table>
<%
}
%>
</form>
</body>
</html>
