Ext.ns('NE.cespcial');

NE.cespcial.FormCaracterEspecial = Ext.extend(Ext.Panel,{
	mensaje: '',
	initComponent : function(){
		 Ext.apply(this, {
			width: 			590,
			height:390,
			title: '<p align="center">Validación carcateres de Control</p>',
			frame: true,   
			border: true,
			style: 'margin:0 auto;',
			tools: [
				{
					id:			'ayuda1',
					handler:		 function(event, toolEl, panel){
							Ext.getCmp('panelLayoutDeCaraControl').show();
					}
				}
			],
			toolTemplate: new Ext.XTemplate(
                            '<tpl>',
                            '<div class="x-tool x-tool-ayudaLayout">&#160</div>',
                            '</tpl>'
			),
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarItems()
			//buttons: this.generarBotones()
		});
		
		NE.cespcial.FormCaracterEspecial.superclass.initComponent.call(this);
	},
	generarItems: function(){
		return [
				  { 
						xtype: 			'panel',
						id:				'PanelResultadosControl',
						plain:			true,
						style: 		'margin: 8px;',
						defaults:{ 
							layout:		'fit',
							border:		false,
							autoScroll: true
						},
						items: [
							{             
								 xtype: 'textarea',
								 width:560,
								 id:'txtareaMensaje',
								 labelAlign: 'top',
								 height:230,
								 labelSeparator:"",
								 value: this.mensaje,
								 fieldLabel: "Menu List <span style=\"color:red;\">*</span>"
							}
						]
					},
					
					{
						xtype: 		'panel',
						width: 		'100%',
						style: 		'margin: 3px;',
						layout: {
							type: 'hbox',
							pack: 'center'
						},
						buttons:
					[
							{
								xtype: 		'button',
								width: 		100,
								text: 		'Descargar Archivo',
								iconCls: 	'icoTxt',
								id: 			'btnDescargarArchivo',
								handler:    function(boton, evento) {
									
								}
							},
							{
								xtype: 		'button',
								width: 		80,
								text: 		'Aceptar',
								iconCls: 	'icoAceptar',
								id: 			'btnAceptar',
								handler:    function(boton, evento) {
									 
								}
							}
						] 
					},
					{
						xtype: 		'panel',
						width: 		'100%',
						style: 		'margin:  2px; margin-left: 0px; margin-top: 2px;',
						layout: {
							type: 'hbox',
							pack: 'start'
						},
						items: [
							{
								xtype: 	'label',
								width: 560,
								html: 	'<p align="left">Nota.- El archivo de descarga tendrá todos los registros que se intentaron cargar, mostrando el mensaje de error solo a los registros que cuentan con caracteres de control inválidos.</p>',
								style: 	'padding: 4px; padding-left: 0px;'
							}
						]
					}
			]
	},
	setMensaje: function(mensaje){
		Ext.getCmp('txtareaMensaje').setValue(mensaje)
	},
	setHandlerBtnAceptar: function(fn){
		var btnAceptar = Ext.getCmp('btnAceptar')
			btnAceptar.setHandler(fn);
	},
	setHandlerBtnDescargaArchivo: function(fn){
		var btnDescargarArchivo = Ext.getCmp('btnDescargarArchivo')
			btnDescargarArchivo.setHandler(fn);
	}
});


NE.cespcial.PnlLayoutCaractControl = Ext.extend(Ext.Panel,{
		id:				'panelLayoutDeCaraControl',
		initComponent : function(){
			Ext.apply(this, {
				hidden:			true,
				width: 			700,
				title: 			'',
				frame: 			true,
				border: 			true,
				style: 			'margin: 0 auto',
				bodyStyle:		'padding:10px',
				tools: [
					{
						id:			'close',
						handler: function(event, toolEl, panel){
							Ext.getCmp('panelLayoutDeCaraControl').hide();
						}
					}
				],
				toolTemplate: new Ext.XTemplate(
				  '<tpl>',
						'<div class="x-tool x-tool-{id}">&#160;</div>',
				  '</tpl>'
				),
				items: this.generarLayout()
			});
			
			NE.cespcial.PnlLayoutCaractControl.superclass.initComponent.call(this);
		},
		generarLayout: function(){
			return[
				{
					xtype: 	'label',
					html: 	"<table>"  +
							
								"<tr>"  +
								"	<td colspan=\"2\" align=\"center\">"  +
								"		<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" style=\"background:#FFFFFF;\" >"  +
								"		<tr>"  +
								"			<td class=\"celda01\" align=\"left\" style=\"height:30px;width:100px;\" >N&uacute;m. Dec.</td>"  +
								"			<td class=\"celda01\" align=\"left\"	style=\"height:30px;width:200px;\" >Car&aacute;cter</td>"  +
								"			<td class=\"celda01\" align=\"left\"	style=\"height:30px;width:300px;\" >Nombre</td>"  +
								"		</tr> "  +
								"		<tr  >"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >00</td>"  +
								"			<td class=\"formas\" align=\"left\"	  style=\"height:30px;width:200px;\" >NULL</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >Car&aacute;cter nulo</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >01</td>"  +
								"			<td class=\"formas\" align=\"left\"	  style=\"height:30px;width:200px;\" >SOH</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >Inicio encabezado</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >02</td>"  +
								"			<td class=\"formas\" align=\"left\"	  style=\"height:30px;width:200px;\" >STX</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >Inicio texto</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >03</td>"  +
								"			<td class=\"formas\" align=\"left\"	  style=\"height:30px;width:200px;\" >ETX</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >fin texto</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >04</td>"  +
								"			<td class=\"formas\" align=\"left\"	  style=\"height:30px;width:200px;\" >EOT</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >fin transmisi&oacute;n</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >05</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >ENQ</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >Consulta</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >06</td>"  +
								"			<td class=\"formas\" align=\"left\"	  style=\"height:30px;width:200px;\" >ACK</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >reconocimiento</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >07</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >BEL</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >timbre</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >08</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >BS</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >retroceso</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;color:green;\" >09</td>"  +
								"			<td class=\"formas\" align=\"left\"	  style=\"height:30px;width:200px;color:green;\" >HT</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;color:green;\" >tab horizontal</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;color:green;\" >10</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;color:green;\" >LF</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;color:green;\" >nueva linea</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >11</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >VT</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >tab vertical</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >12</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >FF</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >nueva p&aacute;gina</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;color:green;\" >13</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;color:green;\" >CR</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;color:green;\" >retorno de carro</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >14</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >SO</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >desplaza afuera</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >15</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >SI</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >desplaza adentro</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >16</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >DLE</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >esc. vinculo datos</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >17</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >DC1</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >control disp.1</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >18</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >DC2</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >control disp.2</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >19</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >DC3</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >control disp.3</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >20</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >DC4</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >control disp.4</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >21</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >NAK</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >conf. Negativa</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >22</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >SYN</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >inactividad sinc</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >23</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >ETB</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >fin bloque trans</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >24</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >CAN</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >cancelar</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >25</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >EM</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >fin del medio</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >26</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >SUB</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >sustituci&oacute;n</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >27</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >ESC</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >escape</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >28</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >FS</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >sep. Archivo</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >29</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >GS</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >sep. Grupos</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >30</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >RS</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >sep. Registros</td>"  +
								"		</tr>"  +
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >31</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >US</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >sep. Unidades</td>"  +
								"		</tr>"  +
							
								"		<tr>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:100px;\" >127</td>"  +
								"			<td class=\"formas\" align=\"left\"   style=\"height:30px;width:200px;\" >DEL</td>"  +
								"			<td class=\"formas\" align=\"left\" style=\"height:30px;width:300px;\" >suprimir</td>"  +
								"		</tr>"  +
								"		</table>"  +
								"	</td>"  +
								"</tr>"  +
								"<tr>"  +
								"	<td>&nbsp;</td>"  +
								"</tr>"  +
								"</table>" ,
					cls:		'x-form-item',
					style: {
						width: 			'100%',
						marginBottom: 	'10px', 
						textAlign:		'center',
						color:			'#ff0000'
					}
				},
				{
					xtype: 		'panel',
					width: 		'100%',
					style: 		'margin:  2px; margin-left: 0px; margin-top: 2px;',
					layout: {
						type: 'hbox',
						pack: 'start'
					},
					items: [
						{
							xtype: 	'label',
							width:	700,
							html: 	'<p align="left">Los caracteres de color verde son los permitidos en la carga de archivos.</p><br>',
							style: 	'padding: 4px; padding-left: 0px;color:green;'
						}
					]
				},
				{
					xtype: 		'panel',
					width: 		'100%',
					style: 		'margin:  2px; margin-left: 0px; margin-top: 2px;',
					layout: {
						type: 'hbox',
						pack: 'start'
					},
					items: [
						{
							xtype: 	'label',
							width:	700,
							html: 	'<p align="left">Nota: Para  poder identificar los caracteres de control inválidos se sugiere utilizar un editor de texto como Notepad++, en dicho editor se mostrarán los caracteres de control con su nombre abreviado para que pueda borrarlos.</p>',
							style: 	'padding: 4px; padding-left: 0px;'
						}
					]
				},
				{
					xtype: 	'label',
					html: 	"<table width=\"590\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" bordercolor=\"#A5B8BF\" class=\"formas\" >"  +
								"<tr align=\"center\" >	<td width=\"590\" height=\"50\" align=\"center\"  ><img src=/nafin/01principal/imagenes/ejCaracteresControl.png align=\"center\" ></td></tr>"+
								"<tr><td colspan=\"5\">&nbsp;</td></tr>"+
								"</table>" ,
					cls:		'x-form-item',
					style: {
						width: 			'100%',
						marginBottom: 	'10px', 
						textAlign:		'center',
						color:			'#ff0000'
					}
				}
			]
		}
	});



NE.cespcial.CaracteresEspeciales = Ext.extend(Ext.Panel,{
		mensaje:'',
		initComponent : function(){
			obj = this;
			Ext.apply(this, {
				width: 			750,
				title: 			'',
				frame: 			false,
				border: 			false,
				style: 			'margin: 0 auto',
				bodyStyle:		'padding:10px',
				items: this.agregaComponentes()
			});
			
			NE.cespcial.CaracteresEspeciales.superclass.initComponent.call(this);
		},
		agregaComponentes: function(){
			fCaracteresEspeciales = new NE.cespcial.FormCaracterEspecial({mensaje:this.mensaje});
			layoutCaractEspoecial = new NE.cespcial.PnlLayoutCaractControl();
			
			return [
				fCaracteresEspeciales,
				{xtype:'box', height:20},
				layoutCaractEspoecial
			]
			
		},
		setMensaje: function(mensaje){
			fCaracteresEspeciales.setMensaje(mensaje)
		},
		setHandlerAceptar: function(fn){
			fCaracteresEspeciales.setHandlerBtnAceptar(fn);
		},
		setHandlerDescargarArchivo: function(fn){
			fCaracteresEspeciales.setHandlerBtnDescargaArchivo(fn);
		}
	}
);

Ext.reg('panel_caracteres_especiales', NE.cespcial.CaracteresEspeciales);

NE.cespcial.AvisoCodificacionArchivo = Ext.extend(Ext.Window,{
	codificacion:'',
	handler:null,
	initComponent : function(){
		 obj = this;
		 Ext.apply(this, {
			layout: 'form',
			autoHeight:true,
			buttonAlign: 'center',
			width:400,
			frame:true,
			resizable:false,
			modal: true,
			id:'winCesionDerechos1',
			title: 'Aviso',
			bodyStyle: 'text-align:center',
			items: [{xtype:'panel', frame:true, html:'La codificación del archivo proporcionado no es válida: <b>'+obj.codificacion +'</b>.<br><br>Codificaciones permitidas: ISO-8859-1 y Windoows-1252(CP1252).'}],
			listeners:{
				beforeclose:function(win){
					//win.handler();
				}
			},
			buttons:[
				{
					text: 'Aceptar',
					id: 'btnRegresarWin',
					handler: function(btn){
						obj.close();
					}
					
				}
			]
		});
		
		NE.cespcial.AvisoCodificacionArchivo.superclass.initComponent.call(this);
	}
});
Ext.reg('aviso_codificacion_archivo', NE.cespcial.AvisoCodificacionArchivo);