<%@page import = "
            com.nafin.security.UserNE,
            com.nafin.security.UserSignOnNE,
            java.io.BufferedReader,
            java.io.File,
            java.io.FileFilter,
            java.io.FileInputStream,
            java.io.FileReader,
            java.util.Arrays,
            java.util.Comparator"
errorPage = "/00utils/error.jsp" autoFlush="true"%>
<%
UserNE          userNE	= session.getAttribute("userNE")==null?new UserNE():(UserNE)session.getAttribute("userNE");
UserSignOnNE    userSO	= new UserSignOnNE();
boolean         ursOK   = userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()));
%>
<html>
<head>
<title>.:: ADMIN-NAFIN ::.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Cache-Control" content="no-cache">
<link rel="Stylesheet" type="text/css" href="/nafin/00utils/css/default.css">
<script language="JavaScript">
<!--
<%if(!"ADMIN".equals(userNE.getNe_perfil())) {%>
document.onkeydown = 
	function() {
		if(window.event && window.event.keyCode == 116) {
			window.event.keyCode = 0;
			return false;
		}
	}

if (window.Event) 
	document.captureEvents(Event.MOUSEUP); 
function nocontextmenu() { 
	event.cancelBubble = true 
	event.returnValue = false; 
	return false; 
} 
function norightclick(e) { 
	if(window.Event) { 
		if(e.which !=1)
			return false; 
	} else if (event.button !=1) { 
		event.cancelBubble = true 
		event.returnValue = false; 
		return false; 
	}
}
document.oncontextmenu = nocontextmenu; 
document.onmousedown = norightclick;
<%}//if(!"ADMIN".equals(userNE.getNe_perfil()))%>
//-->
</script>
</head>
<table width="620" cellpadding="0" cellspacing="0" border="0">
<form name="frmContenido" method="POST" action="00applog.jsp">
<%
try {
    String logpath = (request.getParameter("logpath")==null)?getServletConfig().getServletContext().getRealPath(File.separator):request.getParameter("logpath");
    //System.out.println("logpath:"+logpath);
    String logfile = (request.getParameter("logfile")==null)?"":request.getParameter("logfile");
    //System.out.println("logfile:"+logfile);
    java.io.File file;
    
    if("".equals(logfile)) {%>
        <tr>
            <td>
                <input type="text" name="logpath" class="formas" size="200" value="<%=logpath%>">
                <input type="submit" class="celda02" value="Set Path">
            </td>
        </tr>
        <tr>
            <td class="texto">
        <%        
        FileFilter fileFilter = new FileFilter() {
            public boolean accept(File file) {
                boolean valid = false;
                if(file.isFile()) {
                    String fileName=file.getName();  
                    String fileExtension=fileName.substring(fileName.lastIndexOf(".")+1);
                    fileExtension = fileExtension.toLowerCase();
                    if(
                        fileExtension.contains("log") 
                        || fileExtension.contains("out") 
                        //|| fileExtension.contains("jsp")
                        ) {
                        valid = true;
                    }
                }                
                return valid;
            }
        };
        
        file = new java.io.File(logpath);
        java.io.File[] archivos = file.listFiles(fileFilter);
        
        Arrays.sort(archivos, new Comparator() {
                public int compare(Object a, Object b) {
                    File filea = (File)a;
                    File fileb = (File)b;
                    return filea.getName().compareToIgnoreCase(fileb.getName());
                }
            }
        );
        
        //String archivos[] = file.list();
        String archivoLog = "";
        for(int i=0;i<archivos.length;i++) {
            out.println("<br><a href='00applog.jsp?logfile="+archivos[i]+"'>"+archivos[i].getName()+"</a>");
        }
        %>
            </td>
        </tr>
        <%
    }
    
    if(!"".equals(logfile)) {%>
        <input type="button" value="Recargar" class="celda02" onclick="javascript:location.reload();">
        <input type="button" value="Cancelar" class="celda02" onclick="window.location.href='00applog.jsp'">
<%      out.println("<br>"+logfile+"<br>");

        file = new java.io.File(logfile);
        //System.out.println("file:"+file.getAbsolutePath());
        
        if(file.isFile()) {
            FileInputStream fileinputstream = new FileInputStream(file);
            long nBytes = (long)fileinputstream.available();
            nBytes = nBytes - 75000;
            byte arrByte[] = new byte[4096];
            
            FileReader input = new FileReader(file);
            BufferedReader bufInput = new BufferedReader(input);
            String line;
            
            if(nBytes>0) {
                bufInput.skip(nBytes);
            }
            
            line = bufInput.readLine();
            while(line!=null) {
                out.println(line+"<br>");
                line = bufInput.readLine();
            }
        }
        %>
        <input type="button" value="Recargar" class="celda02" onclick="javascript:location.reload();">
        <input type="button" value="Cancelar" class="celda02" onclick="window.location.href='00applog.jsp'">
<%  }
    
} catch(Exception e) {
	e.printStackTrace();
}
%>
</form>
</table>
</body>
</html>