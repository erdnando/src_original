Ext.onReady(function() {


	var win = new Ext.Window({
		width: 500,
		height: 300,
		closeAction: 'destroy',
		layout: 'fit',
		title: 'Firma Digital',
		items: [
			{
				autoScroll: true,
				html: Ext.util.Format.nl2br(document.formAux.txtFirmar.value)
			}
		],
		buttons: [
			{
				text: 'Enterado',
				handler: function() {
					window.close();
				}
			}
		]
	}).show();

});