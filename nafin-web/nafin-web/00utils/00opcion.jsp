<%@	page import = "
		com.nafin.security.UserNE,
		com.nafin.security.UserSignOnNE,
		java.io.File,
		java.io.InputStream,
		java.util.HashMap,
		java.util.Iterator,
		java.util.List,
		java.util.Map,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileItemFactory,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload"
	errorPage = "/00utils/error.jsp" buffer="16kb" autoFlush="true"%>
<%
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache");
response.setHeader("Expires", "Thu, 29 Oct 1969 17:04:19 GMT");

InputStream is = null;
String is_name = "";
			
Map hiddens = new HashMap();
boolean isMultipart = ServletFileUpload.isMultipartContent(request);
if(isMultipart) {
	FileItemFactory 	factory 	= new DiskFileItemFactory();
	ServletFileUpload 	upload 		= new ServletFileUpload(factory);
	List                    items 		= upload.parseRequest(request);
	Iterator                iter 		= items.iterator();
	while (iter.hasNext()) {
            FileItem    item    = (FileItem) iter.next();
            String      name    = item.getFieldName();
            String      value   = item.getString();
            if (item.isFormField()) {
                hiddens.put(name, value);
            } else {
                is = item.getInputStream();
                is_name = item.getName();
            }
	}//while (iter.hasNext())
}//if(isMultipart)

String 			hidAction 	= (hiddens.get("hidAction")==null)?"":hiddens.get("hidAction").toString().trim();
String 			txt_pass 	= (hiddens.get("txt_pass")==null)?"":hiddens.get("txt_pass").toString().trim();
UserNE 			userNE		= session.getAttribute("userNE")==null?new UserNE():(UserNE)session.getAttribute("userNE");
UserSignOnNE 	userSO 		= new UserSignOnNE();
if("L".equals(hidAction)) {
    userNE = userSO.setUser(is_name, is);
    userSO.validUser(userNE, userSO.encryptText(userNE, txt_pass), userSO.sign(userNE, userNE.getNe_perfil()));
    session.setAttribute("userNE", userNE);   
}//if("L".equals(hidAction))
%>
<html>
<head>
<title>.:: ADMIN-LOGIN ::.</title>
<link rel="Stylesheet" type="text/css" href="/nafin/00utils/css/default.css">
<script language="Javascript" src="../00utils/valida.js"></script>
<script language="javascript">
<!--
	document.onkeydown = 
		function() {
			if(window.event && window.event.keyCode == 116) {
				window.event.keyCode = 0;
				return false;
			}
		}
	if (window.Event) 
		document.captureEvents(Event.MOUSEUP); 
	function nocontextmenu() { 
		event.cancelBubble = true 
		event.returnValue = false; 
		return false; 
	} 
	function norightclick(e) { 
		if(window.Event) { 
			if(e.which !=1)
				return false; 
		} else if (event.button !=1) { 
			event.cancelBubble = true 
			event.returnValue = false; 
			return false; 
		}
	}
	document.oncontextmenu = nocontextmenu; 
	document.onmousedown = norightclick; 

function login() {
	var f = document.frmContenido;
	f.hidAction.value = "L";
	f.action = "00opcion.jsp";
	f.target= "_self";
	f.submit();
}

function setPage() {
	var f = document.frmContenido;
	parent.facultad.location.replace(f.cmb_page.value);	
}
//-->
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="frmContenido" method="post" action="00opcion.jsp" enctype="multipart/form-data">
<input type="hidden" name="hidAction" value="<%=hidAction%>">
<table width="620" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td>&nbsp;</td>
</tr>
<%	if("".equals(userNE.getNe_perfil())) {%>
<tr>
	<td>
	<table align="center">
	<tr>
		<td class="formas" align="right">Pass:</td>
		<td>
			<input 
				type		= "password" 
				class		= "formas" 
				name		= "txt_pass" 
				size		= "8" 
				maxlength	="15"
			>
		</td>
		<td class="formas" align="right">File:</td>
		<td>
			<input 
				type		= "file" 
				class		= "formas" 
				name		= "txt_path" 
				value		= "Browse" 
				size		= "45">
		</td>
    </tr>
    <tr>
        <td align="center" colspan="2">
            <input type="Button" class="celda02" value="Login" title="Login" onclick="login()">
            <input type="Button" class="celda02" value="Cancel" title="Cancel" onclick="window.location.href='00opcion.jsp'">
        </td>
	</tr>
	</table>
	</td>
</tr>
<%	} else if(userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()))) {%>
<tr>
	<td>
	<table align="center">
	<tr>
		<td class="formas" align="right">Option:</td>
		<td>
			<select name="cmb_page" size="1" class="formas" onchange="setPage();">
				<option value="/nafin/14seguridad/Seguridad/Usuario/vacio.html">Select...</option>
				<option value="00viewer.jsp">Viewer</option>
				<option value="00consulta.jsp">Cons</option>
                                <!--
				<option value="00appclog.jsp">AppLog</option>
                                -->
				<option value="00applog.jsp">Log</option>
                                <%if("ADMIN".equals(userNE.getNe_perfil())) {%>
                                    <!--
                                    <option value="00applog.jsp?isla=2">Log2</option>
                                    -->
                                    <option value="00buscaarc.jsp">Browser</option>
                                    <%--
                                    <option value="00viewercc.jsp">Review CC</option>
                                    <option value="00viewerzip.jsp">Deploy ZIP</option>
                                    --%>
                                    <option value="00reload.jsp">ReLoad</option>
                                    <option value="00loadDB.jsp">LoadDB</option>
                                <%}//if("".equals(userNE.getNe_perfil()))%>
			</select>
		</td>
	</tr>
	</table>
	</td>
</tr>
<%	} %>
<tr>

</tr>
<tr>
	<td>&nbsp;</td>
</tr>
</table>
</form>
</body>
</html>
