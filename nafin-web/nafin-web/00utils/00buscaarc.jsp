<%@	page import = "
		com.nafin.security.UserNE,
		com.nafin.security.UserSignOnNE,
		java.io.File,
		java.io.FileInputStream,
		java.util.Date,
		java.text.SimpleDateFormat"
	errorPage = "/00utils/error.jsp" buffer="16kb" autoFlush="true"%>
<%
UserNE          userNE	= session.getAttribute("userNE")==null?new UserNE():(UserNE)session.getAttribute("userNE");
UserSignOnNE 	userSO	= new UserSignOnNE();
boolean         ursOK   = userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()));
	
try {
String appRoot = application.getRealPath("/");
String actual_dir = request.getParameter("actual_dir");
String toPatent = request.getParameter("toParent");
String accion = request.getParameter("accion");
String sel_file = request.getParameter("sel_file");

//System.out.println("Accion: " + accion);

if(accion != null && accion.equalsIgnoreCase("view") && sel_file != null)
{
  //System.out.println("sel_file: " + sel_file);
  File view = new File(sel_file);
  FileInputStream salida = new FileInputStream(view);

  while(salida.available() != 0)
  {
    out.write(salida.read());
  }
}
else
{

  File file = null;

  if(actual_dir == null || actual_dir.trim().equals("")) 
    actual_dir = appRoot;

  file = new File(actual_dir);

  if(toPatent != null && toPatent.equalsIgnoreCase("true")) {
    if(file.getParentFile() != null) file = file.getParentFile();
    actual_dir = file.getAbsolutePath();
  }
  
  char lastChar = actual_dir.charAt(actual_dir.length() - 1);
  if(!(lastChar == '\\') && !(lastChar == '/')) actual_dir += "/";
  //if(!(lastChar == '\\') && !(lastChar == '/')) actual_dir += "\\";

  //System.out.println("Actual Dir: " + actual_dir);
%>

  <html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <link rel="Stylesheet" type="text/css" href="/nafin/00utils/css/default.css">
  <title>Explorer</title>
  </head>
  <body>
  <hr><br>

  <script language="javascript">
    <!--

      function doNavegacion(dirName)
      {
        frmExplorer.accion.value = "list";
        frmExplorer.actual_dir.value += dirName;
        frmExplorer.submit();
      }

      function doGoParent()
      {
        frmExplorer.accion.value = "list";
        frmExplorer.toParent.value = "true";
        frmExplorer.submit();
      }

      function doView(fileName)
      {
      }

      function doDownload(fileName)
      {
      }

      function doDelete(fileName)
      {

      }

      function doAction(fileName, action)
      {
        frmExplorer.sel_file.value = frmExplorer.actual_dir.value + fileName;
        frmExplorer.accion.value = action;
        frmExplorer.submit();        
      }
    
    //-->
  </script>

  <form name="frmExplorer" method="post">

    <input type="hidden" id="actual_dir" name="actual_dir" value="<%=actual_dir%>">
    <input type="hidden" id="toParent" name="toParent" value="">
    <input type="hidden" id="sel_file" name="sel_file" value="">
    <input type="hidden" id="accion" name="accion" value="list">
    <%out.print("<h3>Path:<h3> " + actual_dir + "</h3></h2><br>");  %>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td><b>Name</b></td>
        <td><b>Size</b></td>
        <td><b>Modified</b></td>
        <td><b>Accion</b></td>
      </tr>
      <tr>
        <td><a href="javascript:doGoParent();">..(Go Up)</a></td>
      </tr>
      <%
      File[] list = file.listFiles();
		
      for(int i = 0; i < list.length; i++)
      {
        String strTama = "";
        long ilen = 0;    

        if (list[i].length() > 1024)
        {
          ilen = list[i].length()/1024;
          strTama = " KB";
        }
        else
        {
          strTama = " BYTES";
        }      

        String df = (new SimpleDateFormat ("dd-MM-yyyy 'at' hh:mm:ss a")).format(new Date(list[i].lastModified()));;
        %>
        <tr>
          <%if(list[i].isDirectory()) {%>
          <td><a href="javascript:doNavegacion('<%=list[i].getName()%>')"><%=list[i].getName()%></a></td>          
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <%} else {%>
          <td><%=list[i].getName()%></td>        
          <td><%=ilen + strTama%></td>
          <td><%=df%></td>
          <td><a href="javascript:doAction('<%=list[i].getName()%>', 'view');">View</a> <!--/ <a href="javascript:doAction('<%=list[i].getName()%>', 'download');">Download</a> / <a href="javascript:doAction('<%=list[i].getName()%>', 'delete');">Delete</a--></td>        
          <%}%>
        </tr>
        <%
      }
      %>
    </table>
  
  </form>

  <br><hr><br>

  <!--form name="frmUpload" method="post" enctype="multipart/form-data">
    <input type="hidden" id="actual_dir" name="actual_dir" value="<%=actual_dir%>">
    <input type="hidden" id="accion" name="accion" value="upload">

    <table border="0" cellpadding="1" cellspacing="2">
      <tr>
        <td>Archivo:</td>
      </tr>
      <tr>
        <td><input type="file" name="txtFile" size="60"></td>
      </tr>
      <tr>
        <td><input type="submit" value="Subir"></td>
      </tr>
    </table>
  
  </form-->

  </body>
  </html>
<%
}
}catch (Exception e) {
e.printStackTrace();
}
%>