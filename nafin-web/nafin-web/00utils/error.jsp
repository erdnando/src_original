<%@page import="java.util.Date, java.text.SimpleDateFormat" isErrorPage="true"%>

<%
SimpleDateFormat formatoHora = new SimpleDateFormat ("yyyyMMdd_HHmmss");
String fechaError = formatoHora.format(new java.util.Date());

System.out.println(fechaError + ": ERROR INESPERADO. " + exception.getMessage());
exception.printStackTrace();
%>
<html>
<head>
<title>Error Inesperado</title>
<style type="text/css">
<!--
.celda {  font-family: Arial, Helvetica, sans-serif; font-size: 11px}
-->
</style>
	<script language="JavaScript">

	function mostrarDetalleError() { 
		document.getElementById("stackTrace").style.display = '';
	}
	</script>
</head>
<body>
	<p>&nbsp;</p>
	<table width="600" align="center" border="0" cellspacing="1" cellpadding="5" bgcolor="#000000">
	<tr bgcolor="#CCCCCC">
		<td align="center" colspan="2" class="celda"><strong>Error Inesperado</strong></td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td align="center" width="10%" class="celda"><strong>Fecha</strong></td>
		<td align="center" class="celda"><strong>Mensaje</strong></td>
	</tr>
	<tr bgcolor="#FFFFFF">
		<td align="center" width="10%" class="celda">
                    <%=fechaError%>
                    <!--
                    <a href="javascript:mostrarDetalleError()">.</a>
                    -->
                </td>
		<td align="left" class="celda"><%=exception.getMessage()%></td>
	</tr>
        <%--
	<tr bgcolor="#FFFFFF">
		<td colspan="2">
			<div id="stackTrace" style="display:none;">
				<pre>
					<%exception.printStackTrace(new java.io.PrintWriter(out));%>
			-------------------------------------------------------------------------------
					<%
					if (exception.getCause() != null) {
						exception.getCause().printStackTrace(new java.io.PrintWriter(out));
					}
					%>
				</pre>
			</div>
		</td>
	</tr>
        --%>
	</table>
</body>
</html>
