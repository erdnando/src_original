<%@page import = "
		com.nafin.security.UserNE,
		com.nafin.security.UserSignOnNE,
		java.io.BufferedReader,
		java.io.File,
		java.io.FileInputStream,
		java.io.InputStreamReader,
		java.util.ArrayList,
		java.util.HashMap,
		java.util.Iterator,
		java.util.List,
		java.util.Map,
		java.net.URLDecoder,
		netropology.utilerias.AccesoDB,
		netropology.utilerias.Comunes,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileItemFactory,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload"
	errorPage = "/00utils/error.jsp" buffer="16kb" autoFlush="true"%>
<%
UserNE 		userNE	= session.getAttribute("userNE")==null?new UserNE():(UserNE)session.getAttribute("userNE");
UserSignOnNE 	userSO	= new UserSignOnNE();
boolean         ursOK   = userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()));
%>
<html>
<head>
<link rel="Stylesheet" type="text/css" href="/nafin/00utils/css/default.css">
<script language="JavaScript">
<!--
<%if(!"ADMIN".equals(userNE.getNe_perfil())) {%>
document.onkeydown = 
	function() {
		if(window.event && window.event.keyCode == 116) {
			window.event.keyCode = 0;
			return false;
		}
	}

if (window.Event) 
	document.captureEvents(Event.MOUSEUP); 
function nocontextmenu() { 
	event.cancelBubble = true 
	event.returnValue = false; 
	return false; 
} 
function norightclick(e) { 
	if(window.Event) { 
		if(e.which !=1)
			return false; 
	} else if (event.button !=1) { 
		event.cancelBubble = true 
		event.returnValue = false; 
		return false; 
	}
}
document.oncontextmenu = nocontextmenu; 
document.onmousedown = norightclick;
<%}//if(!"ADMIN".equals(userNE.getNe_perfil()))%>

function regresar () {
	var f = document.frmContenido;
	f.action = "00reload.jsp";
	f.target= "_self";
	f.submit();
//-->
}
</script>
</head>
<body>
<%

AccesoDB	con			= new AccesoDB();
try {
	con.conexionDB();

	Map 	hiddens 		= new HashMap();
	String 	rutaAplicacion 	= getServletConfig().getServletContext().getRealPath("/");
	String 	rutaArchivos 	= rutaAplicacion+File.separatorChar+"00tmp"+File.separatorChar+"15cadenas"+File.separatorChar;
			rutaAplicacion 	= rutaAplicacion.substring(0, rutaAplicacion.lastIndexOf("nafin"));
	String 	rutaAbsoluta 	= "";
	String 	archivoProcesar	= "";
	
	boolean isMultipart = ServletFileUpload.isMultipartContent(request);
	if(isMultipart) {
		FileItemFactory 	factory 	= new DiskFileItemFactory();
		ServletFileUpload 	upload 		= new ServletFileUpload(factory);
		List 				items 		= upload.parseRequest(request);
		Iterator 			iter 		= items.iterator();
		while (iter.hasNext()) {
			FileItem 	item 	= (FileItem) iter.next();
			String 		name 	= item.getFieldName();
			String 		value 	= item.getString();
			if (item.isFormField()) {
				hiddens.put(name, value);
			} else {
				String 	nombreCampo 	= item.getFieldName();
				long 	tamanioArchivo 	= item.getSize();
				String 	archivoOrigen 	= item.getName();
				String 	contentType 	= item.getContentType();
				String 	nombreArchivo	= archivoOrigen;
				if(archivoOrigen.lastIndexOf("/")>0) {
					nombreArchivo	= archivoOrigen.substring(archivoOrigen.lastIndexOf("/")+1);
				} else if(archivoOrigen.lastIndexOf("\\")>0) {
					nombreArchivo	= archivoOrigen.substring(archivoOrigen.lastIndexOf("\\")+1);
				}

                        archivoProcesar = nombreArchivo.substring(0, nombreArchivo.lastIndexOf("."));
				String 	extension 		= archivoOrigen.substring(archivoOrigen.indexOf("."));			
				File 	archivo 		= new File(rutaArchivos + File.separatorChar + nombreArchivo);
				item.write(archivo);
				rutaAbsoluta			= archivo.getAbsolutePath();
			}
		}//while (iter.hasNext())
	}//if(isMultipart)

	
	String 	txt_tabla 		= hiddens.get("Tabla").toString().trim();
	String 	txt_separador 	= hiddens.get("Separador").toString().trim();
	String 	line;
	int 	numRegProc 	= 0;
	int 	numRegIns 	= 0;
	if("ADMIN".equals(userNE.getNe_perfil())&& userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()))) {
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(rutaAbsoluta)), "8859_1"));//UTF-8

		List dato = new ArrayList();
		List tiposDeCampo = new ArrayList();
		StringBuffer campos = new StringBuffer();
		while((line = bufferedreader.readLine())!=null) {
			StringBuffer valores = new StringBuffer();
			line = line.replace('\'', ' ').replace('\"', ' ').replace('\\', ' ').trim();
			List list = Comunes.getListTokenizer(line, txt_separador);
			
			if(numRegProc==0) {
				for(int i=0;i<list.size();i++) {
					String campo = list.get(i).toString().trim();
					if(!"".equals(campo)) {
						dato.add(campo);
						String nombreCampo	= getCampoValue(campo);
						String tipoCampo		= getTipoCampo(campo);
						if(tipoCampo.equals("DESCONOCIDO"))
							throw new Exception("El  campo: " + nombreCampo + " posee un tipo de dato desconocido.");
						tiposDeCampo.add(tipoCampo);
						campos.append(nombreCampo+",");
					}
				}//for(int i=0;i<list.size();i++)
				campos.setLength(campos.length()-1);
			} else {
				for(int i=0;i<dato.size();i++) {
					String campo     = list.get(i).toString().trim();
					String tipoCampo = (String)tiposDeCampo.get(i);
					campo = campo.replace(',', ' ');
					if(tipoCampo.equals("FECHA")) {
						valores.append("to_date('"+campo+"','dd/mm/yyyy'),");
					} else if(tipoCampo.equals("NORMAL")){
						valores.append("'"+campo+"',");
					}
				}//for(int i=0;i<dato.size();i++)
				valores.setLength(valores.length()-1);
				StringBuffer qrySentencia = new StringBuffer("INSERT INTO "+txt_tabla+"("+campos+") VALUES("+valores+")");
				boolean bOK = true;
				try {
					numRegIns += con.ejecutaUpdateDB(qrySentencia.toString());
				} catch (Exception exc) {
					bOK = false;
					exc.printStackTrace();
				}
				con.terminaTransaccion(bOK);
			}

			numRegProc++;
		}//while((line = bufferedreader.readLine())!=null)
		boolean success = (new File(rutaAbsoluta)).delete();		
		numRegProc--;
	}%>
<form name="frmContenido" method="post" action="00reloada.jsp">
<table width="100%" cellpadding="1" cellspacing="2" border="0">
<tr>
	<td>&nbsp;</td>
</tr>
<%if(numRegProc>0) {%>
	<tr>
		<td align="center">
			<table align="center" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
				<tr>
					<td class="celda01" align="center" colspan="2">File Loaded</td>
				</tr>
				<tr>
					<td class="celda01" align="right">Processed Lines</td>
					<td class="formas" align="center"><%=numRegProc%></td>
				</tr>
				<tr>
					<td class="celda01" align="right">Rows Inserted</td>
					<td class="formas" align="center"><%=numRegIns%></td>
				</tr>
			</table>
		</td>
	</tr>
<%}//if(numReg>0 %>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td align="center">
		<input type="Button" class="celda02" value="Back" title="Back" onclick="window.location.href='00loadDB.jsp'">
	</td>
</tr>
<%
} catch(Exception e) { 
%>
<table width="100%" cellpadding="0" celspacing="0" border="0">
<tr>
  <td align="center">
    <table cellspacing="0" cellpadding=="2" border="1" width="80%" align="center" bordercolor="#31659C" class="tabla">
    <tr>
      <td align="center" class="celda02"><%=e.getMessage()%></td>
    </tr>
    </table>
  </td>
</tr>
<%e.printStackTrace();%>
</table>
<%
}finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
}
%>
</form>
</body>
</html>

<%!
	public String getTipoCampo(String campo){
		String []arry = campo.split(":");
		String tipo = "DESCONOCIDO";
		switch(arry.length){
			case 1:
				tipo="NORMAL";
				break;
			case 2:
				if(arry[0].trim().toUpperCase().equals("DATE")) 
					tipo="FECHA";
		}
		return tipo;	
	}
	
	public String getCampoValue(String campo){
		String []arry = campo.split(":");
		if(arry.length > 1)
			return arry[1].trim();
		return campo.trim();
	}
%>