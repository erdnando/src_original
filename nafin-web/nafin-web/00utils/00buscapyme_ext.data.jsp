<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.sql.*,
		java.util.*,
		com.netro.seguridadbean.*,
		netropology.utilerias.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.cadenas.*,
		com.netro.descuento.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/13descuento/13secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion  = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion    = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";
BusquedaPymeGral busqPymeGral = new BusquedaPymeGral();

if (informacion.equals("valoresIniciales")){

	JSONObject jsonObj = new JSONObject();

	jsonObj.put("success", new Boolean(true));
	jsonObj.put("usuario", strNombreUsuario);

	infoRegresar= jsonObj.toString();

} else if (informacion.equals("consultaPymes")){

	JSONObject jsonObj    = new JSONObject();
	JSONArray jsObjArray  = new JSONArray();
	String claveUsuario   = (request.getParameter("claveUsuario")   ==null)?"":request.getParameter("claveUsuario");
	String numElec        = (request.getParameter("numElec")        ==null)?"":request.getParameter("numElec");
	String nombrePyme     = (request.getParameter("nombrePyme")     ==null)?"":request.getParameter("nombrePyme");
	String rfcPyme        = (request.getParameter("rfcPyme")        ==null)?"":request.getParameter("rfcPyme");
	String numSirac       = (request.getParameter("numSirac")       ==null)?"":request.getParameter("numSirac");

	String ic_epo         = (request.getParameter("ic_epo")         ==null)?"":request.getParameter("ic_epo");
	String ic_pyme        = (request.getParameter("ic_pyme")        ==null)?"":request.getParameter("ic_pyme");
	String cg_pyme        = (request.getParameter("cg_pyme")        ==null)?"":request.getParameter("cg_pyme");
	String envia          = (request.getParameter("envia")          ==null)?"":request.getParameter("envia");
	String nombre_pyme    = (request.getParameter("nombre_pyme")    ==null)?"":request.getParameter("nombre_pyme");
	String rfc_pyme       = (request.getParameter("rfc_pyme")       ==null)?"":request.getParameter("rfc_pyme");
	String num_pyme       = (request.getParameter("num_pyme")       ==null)?"":request.getParameter("num_pyme");
	String num_sirac_pyme = (request.getParameter("num_sirac_pyme") ==null)?"":request.getParameter("num_sirac_pyme");

	String producto       = (request.getAttribute("producto")       ==null)?request.getParameter("hidProducto"):request.getAttribute("producto").toString();
	
	List lstPymes = busqPymeGral.getProveedores(ic_epo, num_pyme, rfc_pyme, nombre_pyme, ic_pyme, producto, num_sirac_pyme, claveUsuario);
	jsObjArray = JSONArray.fromObject(lstPymes);
	infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";

	//System.out.println("infoRegresar==="+infoRegresar);

} else if (informacion.equals("ConsultaInfoPyme")){

	JSONArray jsObjArray = new JSONArray();
	UtilUsr utilUsr = new UtilUsr();
	Usuario usuario;
	String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
	HashMap hmUsuarios;
	List lstUsuarioDom = new ArrayList();
	List lstDomicilioPyme = busqPymeGral.getDomicilioPyme(ic_pyme);
	List cuentas = utilUsr.getUsuariosxAfiliado(ic_pyme, "P");
	if(cuentas!=null && cuentas.size()>0){
		for(int i=0; i<cuentas.size(); i++){
			String numUsuario = (String)cuentas.get(i);
			usuario = utilUsr.getUsuario(numUsuario);

			if(lstDomicilioPyme!=null && lstDomicilioPyme.size()>0){
				hmUsuarios = (HashMap)((HashMap)lstDomicilioPyme.get(0)).clone();
				hmUsuarios.put("USUARIO",numUsuario);
				hmUsuarios.put("NOMBREUSUARIO",usuario.getNombre()+" "+usuario.getApellidoPaterno()+" "+usuario.getApellidoMaterno());
				hmUsuarios.put("CORREOUSUARIO ",usuario.getEmail());
				lstUsuarioDom.add(hmUsuarios);
			}
		}
	}

	jsObjArray = JSONArray.fromObject(lstUsuarioDom);
	infoRegresar = "({\"success\": true, \"total\": \"" + jsObjArray.size() + "\", \"registros\": " + jsObjArray.toString()+"})";

} else if (informacion.equals("verificaParamClaveSesion")) {
	JSONObject jsonObj = new JSONObject();
	String vusuario = (request.getParameter("vusuario")==null)?"":request.getParameter("vusuario");
	
	ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	com.netro.seguridadbean.Seguridad BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
	
	String csValidaCveCDer = BeanSegFacultad.consultaParamValidCveCder(vusuario);
	boolean validaCder = true;
	
	if("NAFIN".equals(strTipoUsuario)){
		if("".equals(csValidaCveCDer) || "X".equals(csValidaCveCDer)){
			validaCder = BeanParamDscto.getValidaCveCder();
		}else{
			validaCder = "S".equals(csValidaCveCDer)?true:false;
		}
	}
	
	if(validaCder){
		boolean ok = BeanSegFacultad.vvalidarCveCesion(vusuario);
		if(ok){
			jsonObj.put("existeClaveCesion", "S");
		}else{
			jsonObj.put("existeClaveCesion", "N");
		}
	}else{
		jsonObj.put("existeClaveCesion", "S");
	}
	
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

} else if (informacion.equals("generarFolio")) {
	JSONObject jsonObj = new JSONObject();
	com.netro.seguridadbean.Seguridad BeanSegFacultad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
	
	String folio = "";
	String folioGenerado = "";
	String vusuario = (request.getParameter("vusuario")==null)?"":request.getParameter("vusuario");
	
	try{
		folio = BeanSegFacultad.generaFolioClaveCesion(vusuario);
		folioGenerado = "S";
	}catch(Exception e){
		folioGenerado = "N";
	}
	
	jsonObj.put("folioGenerado", folioGenerado);			
	jsonObj.put("success", new Boolean(true));
	infoRegresar = jsonObj.toString();

}

%>
<%=infoRegresar%>
