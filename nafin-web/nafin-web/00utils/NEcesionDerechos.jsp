<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.text.*,
		netropology.utilerias.*,
		netropology.utilerias.usuarios.*,
		com.netro.exception.*,
		com.netro.seguridadbean.*,
		javax.naming.*,
		net.sf.json.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>

<%!

public  String  getGeneraClaves(String key, int length) {
	String pswd = "";
	String caracter = "";
	String ultimoCaracter = "";
	boolean claveIncorrecta = true;
	int inc = 0;
	do{
		inc = 0;
		pswd = "";
		claveIncorrecta = false;
		for (int i = 0; i < length; i++) {						
			ultimoCaracter = String.valueOf(key.charAt((int)(Math.random() * key.length())));
			if(!"".equals(caracter) && ultimoCaracter.equals(caracter))
				claveIncorrecta = true;
			
			caracter = ultimoCaracter;
			
			if(ultimoCaracter.matches("[+-]?\\d*(\\.\\d+)?") && ultimoCaracter.equals("")==false)
				inc++;
			pswd+=ultimoCaracter;
		}
		if(inc==0 || inc==length)
			claveIncorrecta = true;
		   
	}while(claveIncorrecta);
	
	return pswd;
}%>
<%
String CARACTERESPSSWD = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
String infoRegresar = "";
String datosIniciales = (request.getParameter("datosIniciales")!=null)?request.getParameter("datosIniciales"):"";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String strUsr = (request.getParameter("strUsuario")!=null)?request.getParameter("strUsuario"):"";

String msgError = "";
String strLogin = (String)session.getAttribute("Clave_usuario");
String strTipoUsuario = (String)session.getAttribute("strTipoUsuario");
String iNoCliente = (String)session.getAttribute("iNoCliente");

if("NAFIN".equals(strTipoUsuario)){
	strLogin = strUsr;
}

try{


	com.netro.seguridadbean.Seguridad BeanSegFacultad =netropology.utilerias.ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);

   if(informacion.equals("validaClaveCesionDerec")){
		
			JSONObject jsonObj 		= new JSONObject();
			String cesionUser 		= request.getParameter("cesionUser");
			String cesionPassword 	= request.getParameter("cesionPassword");
			String cesionAltaCve 	= request.getParameter("cesionAltaCve");
			String sCvePerfProt = 	request.getParameter("sCvePerfProt");
			String sCveFacultad = 	request.getParameter("sCveFacultad");
			
			// Metodo de Validacion
			String metodoValidacion = request.getParameter("metodo");
			
			UtilUsr usuario = new UtilUsr(); 
			Usuario user = new Usuario(); 
			user = usuario.getUsuario(strLogin);
			String nombreUsr = user.getNombre()+" "+user.getApellidoPaterno()+" "+user.getApellidoMaterno();
			String email = user.getEmail();
			
			boolean siCorreo = BeanSegFacultad.vvalidarEstatusCorreo(strLogin);
			jsonObj.put("siCorreo", new Boolean(siCorreo));
			
			System.out.println("cesionAltaCve  ====  "+cesionAltaCve);
			if("N".equals(cesionAltaCve)){
				jsonObj.put("capturadespues", "");
				if("PYME".equals(strTipoUsuario)){
					try{
						if("utilerias".equals(metodoValidacion)){
							/*if (!usuario.esUsuarioValido(cesionUser, cesionPassword)) {
								throw new Exception("La confirmacion no es valida");
							}*/
							BeanSegFacultad.validarUsuario(strLogin, cesionUser, cesionPassword);
						}else{
							//BeanSeleccionDocumento.vvalidarUsuario(iNoCliente, cesionUser, cesionPassword, (String) request.getRemoteAddr(), (String) request.getRemoteHost(), (String) session.getAttribute("sesCveSistema") );
							BeanSegFacultad.validarUsuario(strLogin, cesionUser, cesionPassword, (String) request.getRemoteAddr(), 
									(String) request.getRemoteHost(), (String) session.getAttribute("sesCveSistema"), sCvePerfProt,
									sCveFacultad, "S");
						}
						boolean ok = BeanSegFacultad.vvalidarCveCesion(cesionUser);
						if(ok){
							jsonObj.put("resultValidaCesion", "S");
						}else{
							jsonObj.put("resultValidaCesion", "G");
						}
					}catch(Throwable t ){
						jsonObj.put("resultValidaCesion", "N");
						msgError = "La confirmaci贸n de la clave y contrase帽a es incorrecta, el proceso ha sido cancelado";
					}
				}else if("NAFIN".equals(strTipoUsuario)){
					try{
						
						boolean ok = BeanSegFacultad.vvalidarCveCesion(strLogin);
						if(ok){
							jsonObj.put("resultValidaCesion", "S");
						}else{
							jsonObj.put("resultValidaCesion", "G");
						}
					}catch(Throwable t ){
						jsonObj.put("resultValidaCesion", "N");
						msgError = "Se presentaron dificultades t閏nicas al verificar si tiene clave de cesion el proveedor";
					}
				}
			}else if("S".equals(cesionAltaCve)){
				String cesionCve = "";
				if(!"NAFIN".equals(strTipoUsuario)){
					cesionCve = request.getParameter("cesionCve");
				jsonObj.put("capturadespues", "S");
				try{
					BeanSegFacultad.guardarCveCesEncriptada(strLogin,cesionCve);
					jsonObj.put("resultValidaCesion", "S");
				}catch(Exception nee){
					jsonObj.put("resultValidaCesion", "N");
					msgError = nee.getMessage();
				}
				}else{
					cesionCve = getGeneraClaves(CARACTERESPSSWD, 8);
					Correo objCorreo = new Correo();
					String style = "style='font-family:Calibri, Verdana, Arial, Helvetica, sans-serif; font-size:14px;'";
					String cuerpoCorreo = "<p "+style+"><b>Estimado(a) usuario(a): "+nombreUsr+"</b><br>"+
								"Por este conducto Nacional Financiera te ha asignado la siguiente Clave de Cesi贸n de Derechos "+
								"con la cual podr谩s concluir la operaci贸n v铆a telef贸nica de tus documentos.</p>" +
								"<table border='1' ><tr><td bgcolor='#6A69B2' style='color:white; font-family:Calibri, Verdana, Arial, Helvetica, sans-serif; font-size:14px;'>CLAVE DE CESI脫N</td>"+
								"</tr><tr><td align='center' "+style+"><b>"+cesionCve+"</b></td></tr></table>"+
								"<p "+style+">Favor de volver a llamar al 01-800-NAFINSA para continuar con la operaci贸n de tus documentos a "+
								"trav茅s del centro de atenci贸n telef贸nica de NAFIN.</p><br>"+
								"<p "+style+"><b>ATENTAMENTE</b></p>"+
								"<p "+style+">Coordinaci贸n de Informaci贸n Canales Alternos. </p><br>"+
								"<p "+style+"><b>Nota: Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos "+
								"electr贸nicos de entrada.<br>Favor de no responder este mensaje.</b></p>";
								
					try{
						objCorreo.enviarTextoHTML("cadenas@nafin.gob.mx",email,"Clave de Cesi贸n de Derechos",cuerpoCorreo,"");
						jsonObj.put("capturadespues", "S");
						try{
							BeanSegFacultad.eliminaCesion(strLogin);
							BeanSegFacultad.guardarCveCesEncriptada(strLogin,cesionCve);
							jsonObj.put("resultValidaCesion", "R");
						}catch(Exception nee){
							jsonObj.put("resultValidaCesion", "N");
							msgError = nee.getMessage();
						}
					}catch(Exception e){
						System.out.println("Exception: Crreo.enviarTextoHTML ");// Debug info
						e.printStackTrace();
						jsonObj.put("resultValidaCesion", "N");
						msgError = "No se pudo enviar el correo por lo que no se gener贸 la clave de cesi贸n";
					}
					
				}
				
				
			
				
			}
			
			jsonObj.put("success", new Boolean(true));
			jsonObj.put("cesionUser", cesionUser);
			//jsonObj.put("cesionPassword", cesionPassword);
			jsonObj.put("email", email);
			jsonObj.put("strTipoUsuario", strTipoUsuario);
			jsonObj.put("msgError", msgError);
			infoRegresar = jsonObj.toString();
			
		}else if(informacion.equals("confirmaClaveCesion")){
			
			JSONObject jsonObj = new JSONObject();

			String cesionUser = request.getParameter("cesionUser");
			//String cesionPassword = request.getParameter("cesionPassword");
			String capturadespues = (request.getParameter("capturadespues")==null)?"":request.getParameter("capturadespues");

		
			String caja1 = request.getParameter("caja1");
			String caja2 = request.getParameter("caja2");
			String caja3 = request.getParameter("caja3");
			String hcaja1 = request.getParameter("hcaja1");
			String hcaja2 = request.getParameter("hcaja2");
			String hcaja3 = request.getParameter("hcaja3");
		
			String correo = (request.getParameter("correo")==null)?"S":request.getParameter("correo");
			String email = (request.getParameter("email")==null)?"":request.getParameter("email");
			String sendEmail = (request.getParameter("sendEmail")==null)?"":request.getParameter("sendEmail");
			//email = "fvalenzuela@servicio.nafin.gob.mx";
			if("N".equals(correo)){
				if("S".equals(sendEmail)){
					correo = "S";
				}
			}
			
			List lValores = new ArrayList();
		
			lValores.add(caja1);
			lValores.add(caja2);
			lValores.add(caja3);
			lValores.add(hcaja1);
			lValores.add(hcaja2);
			lValores.add(hcaja3);
			
			/*int contadorAccesos = Integer.parseInt((String)session.getAttribute("no_accesos_seleccion_pyme"));
			if(contadorAccesos>=2) {
				response.sendRedirect("13forma2.jsp");
			} else {
				contadorAccesos++;
				session.setAttribute("no_accesos_seleccion_pyme",""+contadorAccesos+"");*/
				
					

				boolean valCve = true;
				if("PYME".equals(strTipoUsuario) || "NAFIN".equals(strTipoUsuario)){
					if(capturadespues.equals(""))
						valCve = BeanSegFacultad.vvalidarCveConfirmacion(strLogin,lValores);
				}
				
				if(valCve){
					if(capturadespues.equals(""))
						BeanSegFacultad.actualizarEstatusCorreo(strLogin,correo);
					
					jsonObj.put("esCveCorrecta",new Boolean(valCve));

				}else{
					msgError = "La confirmaci贸n de la clave es incorrecta, el proceso ha sido cancelado";
					jsonObj.put("esCveCorrecta",new Boolean(valCve));
				}
			//}
			
			jsonObj.put("email",email);
			jsonObj.put("correo",correo);
			jsonObj.put("msgError",msgError);
			jsonObj.put("success",new Boolean(true));
			infoRegresar = jsonObj.toString();
		}
}catch(NafinException ne){
	ne.printStackTrace();
	msgError = ne.getMsgError();//mensaje de horario y de validacion del manejo del servicio
	JSONObject jsonObj = new JSONObject();
	jsonObj.put("msgError",msgError);
	jsonObj.put("success",new Boolean(true));
	infoRegresar = jsonObj.toString();
}finally{

System.out.println("infoRegresar = " + infoRegresar);
}
%>


<%=infoRegresar%>