<%@	page import = "
                com.nafin.security.UserNE,
                com.nafin.security.UserSignOnNE,
                java.io.File,
                java.io.FileFilter,
                java.io.FileInputStream,
                java.io.FileOutputStream,
                java.io.InputStream,
                java.io.OutputStream,
                java.net.URLEncoder,
                java.text.DecimalFormat,
                java.text.NumberFormat,
                java.util.ArrayList,
                java.util.Arrays,
                java.util.Collections,
                java.util.Comparator,
                java.util.Date,
                java.util.List,
                java.util.zip.ZipEntry,
                java.util.zip.ZipOutputStream"
	errorPage = "/00utils/error.jsp" buffer="16kb" autoFlush="true"%>
<%
	String ruta 			= (request.getParameter("ruta")==null)?"":request.getParameter("ruta")+"/";
	String rutaVirtual		= "/nafin/";
	String hidAction 		= (request.getParameter("hidAction")==null)?"":request.getParameter("hidAction");
	String chkSelectFiles[]	= request.getParameterValues("chkSelectFiles");
	String zipName 			= "";
	
	UserNE          userNE	= session.getAttribute("userNE")==null?new UserNE():(UserNE)session.getAttribute("userNE");
	UserSignOnNE 	userSO	= new UserSignOnNE();
	boolean         ursOK   = userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()));
	
	if(!"".equals(hidAction)) {
		if("D".equals(hidAction)) {
			zipName = downloadZipFile(chkSelectFiles, getServletConfig().getServletContext().getRealPath("/")+File.separatorChar+"00tmp"+File.separatorChar+"15cadenas"+File.separatorChar);
		} else if("E".equals(hidAction)) { 
			deleteFiles(chkSelectFiles);
		} else if("B".equals(hidAction)) { 
			backUpFiles(chkSelectFiles);
		}
		hidAction = "";
	}//if(!"".equals(hidAction))
%>
<html>
<head>
<title>..::N@fin:::::::EXPLORER::..</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta http-equiv="Cache-Control" content="no-cache">
<link rel="Stylesheet" type="text/css" href="/nafin/00utils/css/default.css">
<script language="JavaScript">
<!--
<%if(!"ADMIN".equals(userNE.getNe_perfil())) {%>
document.onkeydown = 
	function() {
		if(window.event && window.event.keyCode == 116) {
			window.event.keyCode = 0;
			return false;
		}
	}

if (window.Event) 
	document.captureEvents(Event.MOUSEUP); 
function nocontextmenu() { 
	event.cancelBubble = true 
	event.returnValue = false; 
	return false; 
} 
function norightclick(e) { 
	if(window.Event) { 
		if(e.which !=1)
			return false; 
	} else if (event.button !=1) { 
		event.cancelBubble = true 
		event.returnValue = false; 
		return false; 
	}
}
document.oncontextmenu = nocontextmenu; 
document.onmousedown = norightclick;
<%}//if(!"ADMIN".equals(userNE.getNe_perfil()))%>
function changedir(txtruta) {
	var f = document.frmContenido;
	f.ruta.value = txtruta;
	f.submit();
}
function deleteFile() {
	var f = document.frmContenido;    
    //var elementos = f.chkSelectFiles;
    var elementos = document.getElementsByName("chkSelectFiles");
    var seleccionados = 0;
    var lista = "";
    for (i=0;i<elementos.length;i++) {
        if (elementos[i].checked) {
            if(seleccionados<10) {
                lista += "\n"+elementos[i].value;
            } else {
                lista += ".";
            }
            seleccionados++;
        }
    }
    if(seleccionados<1) {
        alert("Seleccione un elemento para Borrar");
        return false;
    } else {
        if(confirm("¿Cofirma el BORRADO de los archivos?")) {
            if(confirm("A continuación se BORRARÁN los siguientes archivos ["+seleccionados+"]:"+lista)) {
                f.hidAction.value = "E";
                f.action = "00viewer.jsp";
                f.target= "_self";
                f.submit();
            }
        }
    }
}
function backup() {
	var f = document.frmContenido;
    //var elementos = f.chkSelectFiles;
    var elementos = document.getElementsByName("chkSelectFiles");
    var seleccionados = 0;
    for (i=0;i<elementos.length;i++) {
        if (elementos[i].checked) {
            seleccionados++;
        }
    }
    if(seleccionados<1) {
        alert("Debe seleccionar al menos un elemento para BackUp");
        return false;
    } else {
        f.hidAction.value = "B";
        f.action = "00viewer.jsp";
        f.target= "_self";
        f.submit();
    }    
}
function createZip() {
    var f = document.frmContenido;
    //var elementos = f.chkSelectFiles;
    var elementos = document.getElementsByName("chkSelectFiles");
    //alert("elementos:"+elementos);
    //alert("elementos:"+elementos.length);
    var seleccionados = 0;
    for (i=0;i<elementos.length;i++) {
        if (elementos[i].checked) {
            seleccionados++;
        }
    }
    if(seleccionados<1) {
        alert("Debe seleccionar al menos un elemento para generar el ZIP");
        return false;
    } else {
        f.hidAction.value = "D";
        f.action = "00viewer.jsp";
        f.target= "_self";
        f.submit();
    }
}
function selectAll() {
	var f = document.frmContenido;
    //var elementos = f.chkSelectFiles;
    var elementos = document.getElementsByName("chkSelectFiles");
    var seleccionados = 0;
    for (i=0;i<elementos.length;i++) {
        if (elementos[i].checked == false) {
            elementos[i].checked = true;
        }
    }
}
function selectNone() {
	var f = document.frmContenido;
    //var elementos = f.chkSelectFiles;
    var elementos = document.getElementsByName("chkSelectFiles");
    var seleccionados = 0;
    for (i=0;i<elementos.length;i++) {
        if (elementos[i].checked == true) {
            elementos[i].checked = false;
        }
    }
}
//-->
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table cellpadding="1" cellspacing="2" border="0" width="100%">
<form name="frmContenido" method="POST" action="00viewer.jsp">
<%
try {
	if("".equals(ruta)) {
		ruta = getServletConfig().getServletContext().getRealPath("/");
	}
	java.io.File file;%>
	<input type="hidden" name="ruta" value="<%=ruta%>">
	<input type="hidden" name="hidAction" value="">
	<tr>
		<td>&nbsp;</td>
	</tr>
<%	if("".equals(hidAction)) {
		file = new java.io.File(ruta);
		ruta = file.getCanonicalPath()+File.separatorChar;
        char diagonal = '/';
		ruta = ruta.replace(File.separatorChar, diagonal);%>
	<tr>
		<td class="titulos" align="center" colspan="2">
			<a  
				href            = "00viewer.jsp"
				title           = "Home" 
				onclick         = "window.status=''; return true;" 
				onmouseout      = "window.status=''; return true;" 
				onmouseover     = "window.status='Home'; return true;"
			>Home</a>
		</td>
	</tr>
	<tr>
		<td align="center">
			<table cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF" width="100%">
				<tr>
					<td class="celda01">RUTA:</td>
					<td class="formas"><%=ruta%></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="left">
			<table cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF" width="100%">
			<tr>
				<td class="celda02" align="center" width="33%" colspan="2">Nombre</td>
				<td class="celda02" align="center" width="33%">Tama&ntilde;o</td>
				<td class="celda02" align="center">Fecha</td>
			</tr>
			<tr>
				<td class="celda01" colspan="5">
<%				if((ruta.indexOf("/nafin/")>=0&&"CONSULTA".equals(userNE.getNe_perfil()))||"ADMIN".equals(userNE.getNe_perfil())) {%>
						<a  
							href 			= "javascript:changedir('<%=ruta+".."%>');"
							title 			= "Arriba" 
							onmouseout 		= "window.status=''; return true;" 
							onmouseover 	= "window.status='Arriba'; return true;"
						>
							..
						</a>
<%				}%>&nbsp;
				</td>
			</tr>
<%			FileFilter fileFilter = new FileFilter() {
		        public boolean accept(File file) {
		            return file.isDirectory();
		        }
		    };
		    
		    java.io.File[] directorios = file.listFiles(fileFilter);
			
			Arrays.sort(directorios, new Comparator() {
					public int compare(Object a, Object b) {
						File filea = (File)a;
						File fileb = (File)b;
						return filea.getName().compareToIgnoreCase(fileb.getName());
					}
				}
			);			
			
			for(int d=0;d<directorios.length;d++) {
				String fileName = directorios[d].getName();%>
				<tr>
					<td class="celda01" colspan="2">
						<a  
							href 			= "javascript:changedir('<%=directorios[d].getCanonicalPath().replace('\\', '/')%>');"
							title 			= "<%=fileName%>" 
							onclick			= "window.status=''; return true;" 
							onmouseout 		= "window.status=''; return true;" 
							onmouseover 	= "window.status='<%=fileName%>'; return true;"
						>
							<%=fileName%>
						</a>
					</td>
					<td class="celda01" align="center">&nbsp</td>
					<td class="celda01" align="center">&nbsp</td>
				</tr>
<%			}

			fileFilter = new FileFilter() {
		        public boolean accept(File file) {
		            return file.isFile();
		        }
		    };
		    java.io.File[] archivos = file.listFiles(fileFilter);
			
			Arrays.sort(archivos, new Comparator() {
					public int compare(Object a, Object b) {
						File filea = (File)a;
						File fileb = (File)b;
						return filea.getName().compareToIgnoreCase(fileb.getName());
					}
				}
			);
			
			NumberFormat formatter = new DecimalFormat("###,###,###,###,###,###,###");
		    int numFiles = 0;
		    for(int a=0;a<archivos.length;a++) {
		    	numFiles++;
				String fileName = archivos[a].getName();
				String fecha 	= new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").format(new java.util.Date(archivos[a].lastModified()));%>
				<tr>
					<td class="formas" align="right">&nbsp;
						<%if("ADMIN".equals(userNE.getNe_perfil())) {%>
							<input 
								type 	= "CHECKBOX"
								name 	= "chkSelectFiles"
								value 	= "<%=archivos[a].getAbsolutePath()%>" 
								class 	= "formas"
							>
						<%}%>
					</td>
					<td class="formas">
						<%=fileName%>
<%--
						<a  
							target 			= "_blank"
							title 			= "<%=fileName%>" 
							onclick			= "window.open(encodeURIComponent('<%=archivos[a].toURL()%>'),'','width=400,height=150,resizable=yes,depend=yes');window.status=''; return true;" 
							onmouseout 		= "window.status=''; return true;" 
							onmouseover 	= "window.status='<%=fileName%>'; return true;"
						>
							<%=fileName%>
						</a>
--%>						
					</td>
					<td class="formas" align="right">
						<%=formatter.format(archivos[a].length())%>
					</td>
					<td class="formas" align="center">
						<%=fecha%>
					</td>
				</tr>
<%			}%>
			</table>
		</td>
	</tr>
<%if("ADMIN".equals(userNE.getNe_perfil())&& userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()))) {%>
	<tr>
		<td align="right">
		<table>
			<tr>
<%			if(!"".equals(zipName)) {
				String zipFile = "/nafin/00tmp/15cadenas/"+zipName;%>
				<td align="right" class="formas">
					<input 
						type 	= "Button" 
						class 	= "celda02" 
						value 	= "Download" 
						title 	= "Download" 
						onclick = "window.location.href='<%=zipFile%>'"
					>
				</td>
<%	
			}%>
			<td align="right" class="formas">
				<input 
					type 	= "Button" 
					class 	= "celda02" 
					value 	= "Select All" 
					title 	= "Select All" 
					onclick = "selectAll()"
				>
			</td>
			<td align="right" class="formas">
				<input 
					type 	= "Button" 
					class 	= "celda02" 
					value 	= "Select None" 
					title 	= "Select None" 
					onclick = "selectNone()"
				>
			</td>
			<td align="right" class="formas">
				<input 
					type 	= "Button" 
					class 	= "celda02" 
					value 	= "BackUp" 
					title 	= "BackUp" 
					onclick = "backup()"
				>
			</td>
			<td align="right" class="formas">
				<input 
					type 	= "Button" 
					class 	= "celda02" 
					value 	= "Create Zip" 
					title 	= "Create Zip" 
					onclick = "createZip()"
				>
			</td>
			<td align="right" class="formas">
				<input 
					type 	= "Button" 
					class 	= "celda02" 
					value 	= "Delete" 
					title 	= "Delete" 
					onclick = "deleteFile()"
				>
			</td>
			</tr>
		</table>
		</td>
	</tr>
<%}%>
	<tr>
		<td>&nbsp;</td>
	</tr>
<%	}
} catch(Exception e) {
	e.printStackTrace();
}%>
</form>
</table>
</body>
</html>
<%!
	private String downloadZipFile(String chkSelectFiles[], String destino) {
		String zipName = "Rev"+new java.text.SimpleDateFormat("ddMMyyyyHHmmss").format(new java.util.Date())+".zip";
		try {
			if(chkSelectFiles!=null) {
                                //System.out.println(destino+zipName);
				List<String> lFiles = Arrays.asList(chkSelectFiles);
				zipFiles(lFiles, destino+zipName);
				File revZip = new File(destino+zipName);
			}
		} catch(Exception e) {
			zipName="";
			e.printStackTrace();
		}
		return zipName;
	}
	
	private static void zipFiles(List<String> lFiles, String nameZipFile) {
		try {
                        FileOutputStream fos = new FileOutputStream(nameZipFile);
                        ZipOutputStream zipOutputStream = new ZipOutputStream(fos);
                        for(String nombre:lFiles) {
                                File file = new File(nombre);
				boolean bFile = file.isFile();
				if(bFile) {
					String 	zipPath         = file.getName();
					long	lastmodified    = file.lastModified();
					byte data[] = new byte[1024];
                                        FileInputStream in = new FileInputStream(file);
					ZipEntry zipEntry = new ZipEntry(zipPath);
					zipEntry.setTime(lastmodified);
					zipOutputStream.putNextEntry(zipEntry);
                                        int len;
                                        while ((len = in.read(data)) > 0) {
                                            zipOutputStream.write(data, 0, len);
                                        }
                                        in.close();
				}//if(bFile)
			}//for(int f=0;f<list.size();f++)
			zipOutputStream.close();
                        fos.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void deleteFiles(String chkSelectFiles[]) {
		if(chkSelectFiles!=null) {
			List lFiles = Arrays.asList(chkSelectFiles);
			for(int f=0;f<lFiles.size();f++) {
				boolean success = (new File(lFiles.get(f).toString())).delete();
				if(!success) {
					System.out.println("Error al borrar:"+lFiles.get(f).toString());
				}
			}
			
		}
	}
	
	private void backUpFiles(String chkSelectFiles[]) {
		try {
			if(chkSelectFiles!=null) {
				List lFiles = Arrays.asList(chkSelectFiles);
				for(int f=0;f<lFiles.size();f++) {
					File backF = new File(lFiles.get(f).toString());
					long lastmod = backF.lastModified();
					String fecha = new java.text.SimpleDateFormat("ddMMyyyy").format(new java.util.Date(lastmod));
					
					InputStream source = new FileInputStream(lFiles.get(f).toString());
					OutputStream destiny = new FileOutputStream(lFiles.get(f).toString()+"."+fecha);
					byte[] buf = new byte[1024];
					int len;
					while ((len = source.read(buf)) > 0) {
						destiny.write(buf, 0, len);
					}
					source.close();
					destiny.close();
				}
			}
		} catch(Exception e) {
			System.out.println("Exception:"+e);
			e.printStackTrace();
		}
	}
%>