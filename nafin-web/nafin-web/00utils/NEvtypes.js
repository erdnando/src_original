/**
 * rangoFecha:	Requiere que sea especificado campoFinFecha en el campo inicial y campoInicioFecha en el final
 *
 * rangoValor:	Requiere que sea especificado campoFinValor en el campo inicial y campoInicioValor en el final
 */
Ext.apply(Ext.form.VTypes, {
	rangofecha : 	function(valor, campo) {
	  var fecha = campo.parseDate(valor);

	  if(!fecha){
			return false;
	  }
	  if (campo.campoInicioFecha && (!this.dateRangeMax || (fecha.getTime() != this.dateRangeMax.getTime()))) {
			var fechaInicial = Ext.getCmp(campo.campoInicioFecha);
			fechaInicial.setMaxValue(fecha);
			this.dateRangeMax = fecha;
			fechaInicial.validate();
	  }else if (campo.campoFinFecha && (!this.dateRangeMin || (fecha.getTime() != this.dateRangeMin.getTime()))) {
			var fechaFinal = Ext.getCmp(campo.campoFinFecha);
			fechaFinal.setMinValue(fecha);
			this.dateRangeMin = fecha;
			fechaFinal.validate();
	  }
	  return true;
	},
	rangofechaText: 'Error en la fecha: formato dd/mm/aaaa',

	rangoValor: function(val, field) {
		/* 
		* Funcion basada en un ejemplo de Desarrollosweb.net (	Cookbook ExtJS: Creando nuestros propios validadores-11/12/2008 por Javier Caride	)
		*/
		var valor = parseFloat(val);
		
		if (field.campoInicioValor){
			var inicioMonto = Ext.getCmp(field.campoInicioValor);
			var inicioMontoValue = parseFloat(inicioMonto.getValue());
			
			if (valor < inicioMontoValue){
				return false;
			} else {
				field.clearInvalid();
				inicioMonto.clearInvalid();
				return true;
			}
			
		}else if (field.campoFinValor) {
			var finMonto = Ext.getCmp(field.campoFinValor);
			var finMontoValue = parseFloat(finMonto.getValue());

				if (valor > finMontoValue){
					return false;
				} else {
					field.clearInvalid();
					finMonto.clearInvalid();
					return true;
				}
		}
		return true;
	},
	rangoValorText:	'El valor final debe ser mayor al valor inicial',
	
rangoBigamount: function(val, field) {
		/* 
		* Funcion basada en un ejemplo de Desarrollosweb.net (	Cookbook ExtJS: Creando nuestros propios validadores-11/12/2008 por Javier Caride	)
		*/
		var valorSincomas=replaceAll(val,',','');
		
		var valor = parseFloat(valorSincomas);
		
		if (field.campoInicioValor){
			var inicioMonto = Ext.getCmp(field.campoInicioValor);
			var inicioMontoValue = parseFloat(replaceAll(inicioMonto.getValue(),',',''));
			
			if (valor < inicioMontoValue){
				return false;
			} else {
				field.clearInvalid();
				inicioMonto.clearInvalid();
				return true;
			}
			
		}else if (field.campoFinValor) {
			var finMonto = Ext.getCmp(field.campoFinValor);
			var finMontoValue = parseFloat(replaceAll(finMonto.getValue(),',',''));

				if (valor > finMontoValue){
					return false;
				} else {
					field.clearInvalid();
					finMonto.clearInvalid();
					return true;
				}
		}
		return true;
	},
	rangoBigamountText:	'El valor final debe ser mayor al valor inicial',
	
	validaRFC: function(val,field) {
	var strRFC = val;
	var arrRFC;
	var strNombre;
	var strFecha;
	var strClave;
	var elementos;
/////////////////////////////////////////////////
	if (strRFC == null || strRFC == "") {
		return false;
	}
	
	arrRFC = strRFC.split("-");
	
	if (arrRFC.length != 3) {
		return false;
	}
	
	strNombre = (arrRFC[0]==null)?"":arrRFC[0];
	strFecha = (arrRFC[1]==null)?"":arrRFC[1];
	strClave = (arrRFC[2]==null)?"":arrRFC[2];  // Homoclave
	
	if (field.tipoPersona == "moral") {
		if (strNombre.length != 3) {
			return false;
		}
	} else if (field.tipoPersona == "fisica") {
		if (strNombre.length != 4) {
			return false;
		}
	} /*else {
		return false;
	}*/
	
	if (isChar(strNombre) == false) {
		return false;
	}

	if (strFecha.length != 6 || isFechaRFC(strFecha) == false) {
		return false;
	}
	
	if (strClave.length != 3 || isCharInt(strClave) == false) {
		return false;
	}
	
	return true;
},
validaRFCText: 'El formato del RFC es Incorrecto'
	



	
});
	var replaceAll= function ( text, busca, reemplaza ){
	  while (text.toString().indexOf(busca) != -1)
			text = text.toString().replace(busca,reemplaza);
	  return text;
	}
	

//Metodos para validar un RFC
	
function isChar(str)
{
  var RefString=" A�BCDE�FGHI�JKLMN�O�PQRSTU�VWXYZa�bcde�fghi�jklmn�o�pqrst�uvwxyz&";

  for(Count=0;Count < str.length;Count++)
  {
    TempChar = str.substring(Count,Count+1);
    if(RefString.indexOf(TempChar,0)==-1)
    { return false; }
  }
  return true;
}

function isCharInt(str)
{
  var RefString="ABCDEFGHIJKLMN�OPQRSTUVWXYZ01234567890";

  for(Count=0;Count < str.length;Count++)
  {
    TempChar = str.substring(Count,Count+1);
    if(RefString.indexOf(TempChar,0)==-1)
    { return false; }
  }
  return true;
}

function isFechaRFC(strValue)
{
  var strFecha = strValue;
  var strDia;
  var strMes;
  var strAnio;
  var intDia;
  var intMes;
  var intAnio;

/////////////////////////////////////////////////
  if (strFecha.length == 6)
  {
    strAnio = strFecha.substr(0,2);
    strMes = strFecha.substr(2,2);
    strDia = strFecha.substr(4,2);
   // alert("A�o: "+strAnio+" Mes: "+strMes+" Dia: "+strDia)
  }
  else
  {
    error = 1;
    return false;
  }
  /////////////////////////////////////////////////
  intDia = parseInt(strDia, 10);
  if (isNaN(intDia))
  {
    error = 2;
    return false;
  }
/////////////////////////////////////////////////
  intMes = parseInt(strMes, 10);

  if (isNaN(intMes))
  {
    error = 3;
    return false;
  }
/////////////////////////////////////////////////
  intAnio = parseInt(strAnio, 10);
  if (isNaN(intAnio))
  {
    error = 4;
    return false;
  }
/////////////////////////////////////////////////
  if (intMes>12 || intMes<1)
  {
    error = 5;
    return false;
  }
/////////////////////////////////////////////////
  if ((intMes == 1 || intMes == 3 || intMes == 5 || intMes == 7 || intMes == 8 || intMes == 10 || intMes == 12) && (intDia > 31 || intDia < 1))
  {
    error = 6;
    return false;
  }
/////////////////////////////////////////////////
  if ((intMes == 4 || intMes == 6 || intMes == 9 || intMes == 11) && (intDia > 30 || intDia < 1))
  {
    error = 7;
    return false;
  }
/////////////////////////////////////////////////
  if (intMes == 2)
  {
    if (intDia < 1)
    {
      error = 8;
      return false;
    }

    if (esAnioBisiesto(intAnio) == true)
    {
      if (intDia > 29)
      {
        error = 9;
        return false;
      }
    }

    else
    {
      if (intDia > 28)
      {
        error = 10;
        return false;
      }
    }
  }
  /////////////////////////////////////////////////
  return true;

}

function esAnioBisiesto(intAnio)
{
  if (intAnio % 100 == 0)
  {
    if (intAnio % 400 == 0)
    { return true; }
  }
  else
  {
    if ((intAnio % 4) == 0)
    { return true; }
  }
  return false;
}
 //Fin Metodos para validar un RFC