<%@page contentType="text/html; charset=windows-1252"%>
<%@page import="java.util.*"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.sql.*"%>
<%@page import="netropology.utilerias.*"%>
<%@page import="com.netro.exception.*"%>

<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload"/>

<html>
	<head>
		<link rel="Stylesheet" type="text/css" href="/nafin/14seguridad/Seguridad/css/nafin.css">
		<script language="JavaScript">
		function regresar () {
			var f = document.forms[0];
			f.action = "00cargaArchivoBD.jsp";
			f.target= "_self";
			f.submit();
		}
		</script>
	</head>
	<body>
<%--************************************************************************--%>
<%boolean cargaOK = true;
	AccesoDB con = new AccesoDB();
	
	try {
		myUpload.initialize(pageContext);
		myUpload.upload();
	}catch(Exception e){
		if(myUpload.getSize() > 2097152) {
			throw new Exception("Error, el Archivo es muy Grande, excede el L�mite que es de 2 MB.");
		}else{
			throw new NafinException("DSCT0030");
		}
	}

	try {
		String txtUploadPath = getServletConfig().getServletContext().getRealPath("/")+"/00archivos/";
		Enumeration enumeration = myUpload.getRequest().getParameterNames();
		HashMap hiddens = new HashMap();
		boolean validaciones = true;
		
		while(enumeration.hasMoreElements()){
			String name = enumeration.nextElement().toString().trim();
			String value = myUpload.getRequest().getParameter(name);
			hiddens.put(name, value);
		}
		
		String bancoFondeo = hiddens.get("bancoFondeo").toString().trim();
		String password = hiddens.get("password").toString().trim();
		
		if(bancoFondeo.equals("")){validaciones = false;}
		if(password.equals("")){validaciones = false;}
		if(!password.equals("n3tro.01")){validaciones = false;}
		
		if(validaciones){
			try{
				con.conexionDB();
				StringBuffer strSQL = new StringBuffer();
				PreparedStatement pst = null;
				ResultSet rst = null;
				String icTerminos = "";
				
				//Obtiene el id de la version a cargar
				strSQL.append("SELECT NVL(MAX(ic_terminos_sms), 0) + 1 FROM comcat_terminos_sms");
				pst = con.queryPrecompilado(strSQL.toString());

				rst = pst.executeQuery();
				
				while(rst.next()){icTerminos = rst.getString(1);}
				
				rst.close();
				pst.close();
				
				strSQL = new StringBuffer();				
				int numFiles = myUpload.save(txtUploadPath);
				com.jspsmart.upload.File myFile = myUpload.getFiles().getFile(0);
				java.io.File file = new File(txtUploadPath+"/"+myFile.getFileName());
				int fileSize = myFile.getSize();
				String contentType = myFile.getContentType();
				String extension = myFile.getFileExt();
				
				strSQL.append(" INSERT INTO comcat_terminos_sms(ic_terminos_sms, ic_banco_fondeo, bi_terminos, cg_extension, cg_content_type, df_alta)");
				strSQL.append(" VALUES(?, ?, ?, ?, ?, SYSDATE)");
				//System.out.println("==========>> strSQL = "+strSQL.toString());
				pst = con.queryPrecompilado(strSQL.toString());
				pst.setInt(1, Integer.parseInt(icTerminos));
				pst.setInt(2, Integer.parseInt(bancoFondeo));
				pst.setBinaryStream(3, new FileInputStream(file), fileSize);
				pst.setString(4, extension);
				pst.setString(5, contentType);
				
				pst.executeUpdate();
				
				pst.close();
			}catch(Exception e){
				cargaOK = false;
				e.printStackTrace();
			}finally{
				if(con.hayConexionAbierta()){
					con.terminaTransaccion(cargaOK);
					con.cierraConexionDB();
				}
			}
%>
<table width="100%" cellpadding="0" celspacing="0" border="0">
	<tr>
		<td align="center">
			<table align="center" border="0" cellspacing="0" cellpadding="2" width="80%" >
			<tr>
				<td align="center" class="formas">Archivo cargado correctamente.</td>
			</tr>
			<tr>
				<td align="center" class="formas"><input type="Button" class="celda02" value="Regresar" title="Limpiar" onclick="window.location.href='00cargaArchivoBD.jsp'"></td>
			</tr>
			</table>
		</td>
	</tr>
</table>
<%
		}else{
%>
<table width="100%" cellpadding="0" celspacing="0" border="0">
	<tr>
		<td align="center">
			<table align="center" border="0" cellspacing="0" cellpadding="2" width="80%" >
			<tr>
				<td align="center" class="formas">Archivo no cargado. Uno o mas datos son incorrectos, por favor verifique e intente nuevamente.</td>
			</tr>
			<tr>
				<td align="center" class="formas"><input type="Button" class="celda02" value="Regresar" title="Limpiar" onclick="window.location.href='00cargaArchivoBD.jsp'"></td>
			</tr>
			</table>
		</td>
	</tr>
</table>
<%
		}
}catch(Exception e){
%>
<table width="100%" cellpadding="0" celspacing="0" border="0">
	<tr>
		<td align="center">
			<table cellspacing="0" cellpadding="2" border="1" width="80%" align="center" bordercolor="#31659C" class="tabla">
			<tr>
				<td align="center" class="celda02"><%=e.getMessage()%></td>
			</tr>
			</table>
		</td>
	</tr>
</table>
<%e.printStackTrace();%>
<%}finally{

}
%>
</body>
</html>
