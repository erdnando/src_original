<%@page contentType = "text/html; charset=windows-1252"%>
<%@page import = "java.util.*"%>
<%@page import = "netropology.utilerias.*"%>
<%@page import = "com.netro.exception.*"%>

<jsp:useBean id="myUpload" scope="page" class="com.jspsmart.upload.SmartUpload"/>

<html>
	<head>
		<link rel="Stylesheet" type="text/css" href="/nafin/14seguridad/Seguridad/css/nafin.css">
		
		<script language="JavaScript">
		function cargar(){
			if(document.forms[0].archivo.value == ""){
				alert("Por favor indique el archivo a cargar.");
				return;
			}
			
			var nombreArchivo = document.forms[0].archivo.value;
			
			if(nombreArchivo.substring(nombreArchivo.indexOf(".") + 1, nombreArchivo.length).toLowerCase() != "pdf"){
				alert("El archivo a cargar debe estar en formato PDF.");
				return;
			}

			if(document.forms[0].bancoFondeo.value == ""){
				alert("Por favor seleccione el banco de fondeo.");
				return;
			}

			if(document.forms[0].password.value == ""){
				alert("Por favor escriba la contraseņa.");
				return;
			}	

			document.forms[0].action = "00cargaArchivoBDa.jsp";
			document.forms[0].target = "_self";
			document.forms[0].submit();
		}
		</script>
	</head>

	<body>
<%response.setHeader("Cache-Control", "no-cache");%>
<%response.setHeader("Pragma", "no-cache");%>
<%response.setHeader("Expires", "Thu, 29 Oct 1969 17:04:19 GMT");%>
<%try{%>
	<form name="frmContenido" method="post" action="00cargaArchivoBDa.jsp" enctype="multipart/form-data">
	<table align="center" border="0" cellpadding="1" cellspacing="2" width="100%">
		<tr>
			<td>
				<table align="center" border="0" cellpadding="1" cellspacing="2" width="500">
					<tr>
						<td align="center" class="titulos" colspan="2">Carga de archivo a la base de datos:</td>
					</tr>
					<tr>
						<td align="center" class="formas" colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td align="right" class="formas" width="100">Archivo:</td>
						<td align="left" class="formas" width="400"><input type="file" class="formas" name="archivo" style="width:400"/></td>
					</tr>
					<tr>
						<td align="right" class="formas" width="300">Banco de Fondeo:</td>
						<td align="center" class="formas" width="200">
							<select name="bancoFondeo" class="formas" style="width:200">
								<option value="">Seleccione...</option>
								<option value="1">NAFIN</option>
								<option value="2">BANCOMEXT</option>
							</select>
						</td>
					</tr>
					<tr>
						<td align="right" class="formas" width="100">Password:</td>
						<td align="left" class="formas" width="400"><input type="password" class="formas" name="password" style="width:100"/></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<input type="Button" class="celda02" value="Cargar Archivo" title="Cargar Archivo" onclick="cargar()">
				<input type="Button" class="celda02" value="Limpiar" title="Limpiar" onclick="window.location.href='00cargaArchivoBD.jsp'">
			</td>
		</tr>
	</table>
<%}catch(Exception e){%>
	<table width="100%" cellpadding="0" celspacing="0" border="0">
		<tr>
			<td align="center">
				<table cellspacing="0" cellpadding=="2" border="1" width="80%" align="center" bordercolor="#A5B8BF" class="tabla">
				<tr>
					<td align="center" class="celda02"><%=e.getMessage()%></td>
				</tr>
				</table>
			</td>
		</tr>
		<%e.printStackTrace();%>
	</table>
<%}%>
	</form>
	</body>
</html>
