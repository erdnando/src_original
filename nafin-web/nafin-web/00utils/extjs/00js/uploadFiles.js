$(function () {
// Initialize the jQuery File Upload widget:
$('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: '/nafin/supervision/CargaArchivosMultiS'
        ,sequentialUploads:true
        ,dropZone: $('#dropzone')
        //,singleFileUploads:false
        //,acceptFileTypes: /(\.|\/)(pdf)$/i
    });
$('#fileupload').bind('fileuploadsend', function (e, data) {
          var noArchivos= $('#noArchivos').val();
          if(noArchivos == ''){
              alert("debe capturar numero de archivos");
              return false;
          }
          $('#primerA').val("1")
          //document.fileupload.primerA.value = $('#archivosC').val();  
         //alert(data.files.length);
         
        /*if(data.files.length==0){
            alert("Agregue archivos a cargar");
            return false;
        }
        var errorString = null;
        $.each(data.files, function (index, file) {
            var name = file.name;
            var index =name.toUpperCase().indexOf('.PDF');
            if(index!=-1){
              var number=name.substring(0,index-1);
              if(isNaN(number*1)){
                 errorString="Solo se permiten nombres de archivos con numeros";
              }
            }else{
                errorString="Solo se permiten archivos con formato PDF";
            }
        });
        if(errorString==null){
            var answer = confirm("�Est� seguro de guardar los archivos seleccionados?");
            if(answer == true){
                alert(true);
                return true;
            }else{
                alert(false);
                return false;
            }
        }else{
            alert(errorString);
            return false;
        }*/
        
    });
$('#fileupload').bind('fileuploadadd',  function (e, data) {
                        var fileType = data.files[0].name.toLowerCase().split('.').pop(), allowdtypes = 'pdf|PDF|zip|ZIP';
                        if (allowdtypes.indexOf(fileType) < 0) {
                            alert('Solo se permiten archivos PDF o archivos ZIP');
                            return false;
                        }
                        
                        var fileName=data.files[0].name.toLowerCase().split('.').shift();
                        var nombreA = $('#nombreA').val();
                        if(nombreA.indexOf(fileName+'|')>-1){
                            alert('El archivo ya fue cargado previamente');
                            return false;
                        }
                                            
                        //$('#primerA').val("1");
                        return validaFormatOrigen(fileName);
                        //return true;
                        
   });
    
    $('#fileupload').bind('fileuploadfail', function (e, data) {
        if(data.errorThrown!=null && data.errorThrown!='abort'){
            alert("Eror al conectar con el servidor");
        }
        try{
            console.log(data.errorThrown);
        }catch(e){
            try{
                console.log('*****Exception al interpretar error del servidor**');
            }catch(xx){
            }
        }
    })
    $('#fileupload').bind('fileuploadprogressall',function(){
      alert("dataLoadeD:"+ data.loaded +"dataTotal:"+ data.total);
    });
    
    $('#fileupload').bind('fileuploaddone', function (e, data) {
        var resultado = data.result;
        if(resultado.success){
            var noA =  $('#archivosC').val();
            noA = parseInt(noA) + parseInt(1);
            $('#archivosC').val(noA); 
            document.fileupload.primerA.value = "1";
            $('#primerA').val("1");
            //$('#noArchivos').attr('disabled','disabled');
            $('#anio').attr('disabled','disabled');
            $('#mes').attr('disabled','disabled');
            $('#tipoInformacion').attr('disabled','disabled');            
            
             var fileName=data.files[0].name.toLowerCase().split('.').shift();
             var nombreA = $('#nombreA').val();
             nombreA+= fileName+'|'; 
             $('#nombreA').val(nombreA);
            //doShowUploadStats();
        }else{
            //alert("ya entro "+resultado.mensaje);
            if($('#primerA').val()=="1"){
                //$('#noArchivos').attr('disabled','disabled');
                $('#anio').attr('disabled','disabled');
                $('#mes').attr('disabled','disabled');
                $('#tipoInformacion').attr('disabled','disabled');
            }
            
            try{
                console.log(resultado.error);
            }catch(e){
            }
        }
    });
    
    
$(document).bind('dragover', function (e) {
    var dropZone = $('#dropzone'),
        timeout = window.dropZoneTimeout;
    if (!timeout) {
        dropZone.addClass('in');
    } else {
        clearTimeout(timeout);
    }
    var found = false,
        node = e.target;
    do {
        if (node === dropZone[0]) {
            found = true;
            break;
        }
        node = node.parentNode;
    } while (node != null);
    if (found) {
        dropZone.addClass('hover');
    } else {
        dropZone.removeClass('hover');
    }
    window.dropZoneTimeout = setTimeout(function () {
        window.dropZoneTimeout = null;
        dropZone.removeClass('in hover');
    }, 100);
});


changeFormatLeyend();

});

function validaFormatOrigen(fileName){
    var resultado = true;
    var origen= $('#tipoInformacion :selected').val();
    /*switch(origen){
        case 1:
            if(isNaN(fileName*1)){
                alert("Solo se permiten archivos con nombres num�ricos para el tipo de expediente seleccionado");    
                resultado = false;
            }
        break;
        case 2:
                var expFisica='^(([^\u0000-\u007F]|[A-Z]|[a-z]|&|\s){1})(([^\u0000-\u007F]|[A-Z]|[a-z]|&){3})(-?)([0-9]{6})(-?)((([^\u0000-\u007F]|[A-Z]|[a-z]|&|[0-9]){3}))(_)([^_]+)(_)([^_]+)$';
                var expMoral='^(([^\u0000-\u007F]|[A-Z]|[a-z]|&){3})(-?)([0-9]{6})(-?)((([^\u0000-\u007F]|[A-Z]|[a-z]|&|[0-9]){3}))(_)([^_]+)(_)([^_]+)$';
                var validMoral=new RegExp(expMoral);
                var matchArray=fileName.match(validMoral);
                if (matchArray==null) {
                        var validFisica=new RegExp(expFisica);
                        matchArray=fileName.match(validFisica);
                        if (matchArray==null) {
                            alert("El nombre del archivo para este tipo de expediente se debe formar por el RFC de la empresa, seguido de gui�n bajo,raz�n social, gui�n bajo y finalmente el folio de solicitud del ECONTRACT");
                            resultado=false;
                        }
                }
        break;
        case 3:
                var expFisica='^(([^\u0000-\u007F]|[A-Z]|[a-z]|&|\s){1})(([^\u0000-\u007F]|[A-Z]|[a-z]|&){3})(-?)([0-9]{6})(-?)((([^\u0000-\u007F]|[A-Z]|[a-z]|&|[0-9]){3}))(_)([^_]+)$';
                var expMoral='^(([^\u0000-\u007F]|[A-Z]|[a-z]|&){3})(-?)([0-9]{6})(-?)((([^\u0000-\u007F]|[A-Z]|[a-z]|&|[0-9]){3}))(_)([^_]+)$';;
                var validMoral=new RegExp(expMoral);
                var matchArray=fileName.match(validMoral);
                if (matchArray==null) {
                        var validFisica=new RegExp(expFisica);
                        matchArray=fileName.match(validFisica);
                        if (matchArray==null) {
                            alert("El nombre del archivo para este tipo de expediente se debe formar por el RFC de la empresa, seguido de gui�n bajo y finalmente la raz�n social de la misma");
                            resultado=false;
                        }
                }
        break;
        default:
        alert("El formato del nombre del archivo no ha sido definido. Por favor contacte al administrador del sistema");
        resultado = false;
    }*/
    return resultado;
}

function changeFormatLeyend(){
    var formato= $('#tipoExpendiente :selected').attr('data-formatFile');
    $('#formatoRequerido').val(formato);
}
