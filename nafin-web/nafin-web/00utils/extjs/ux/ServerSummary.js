/*!
 * Ext JS Library 3.4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */
/**
 * @class Ext.ux.grid.ServerSummary
 * @extends Ext.util.Observable
 * A GridPanel plugin that adds capability to specify the summary data 
 * for the group via json as illustrated here:
 * <pre><code>
{
    data: [
        {
            projectId: 100,     project: 'House',
            taskId:    112, description: 'Paint',
            estimate:    6,        rate:     150,
            due:'06/24/2007'
        },
        ...
    ],

    summaryData: {
        'House': 
        [
			  {
					description: 15, 
					estimate: 10,
					rate: 56, 
					due: new Date(2013, 4, 24),
					cost: 999
			  },
			  {
			  		description: 14, 
					estimate: 9,
					rate: 99, 
					due: new Date(2013, 6, 19),
					cost: 999
			  }
        ]
    }
}
 * </code></pre>
 *
 * @constructor
 * <p>Creates a new ServerSummary grid plugin</p>
 * @xtype serversummary
 * @author Salim Hernandez
 * @since ( 06/19/2013 10:53:13 a.m. )
 * @note Some code taken from ExtJS classes.
 *
 */
Ext.namespace('Ext.ux.grid');
Ext.ux.grid.ServerSummary = Ext.extend(Ext.util.Observable, {
    /**
     * @cfg {Function} summaryRenderer Renderer example:<pre><code>
summaryRenderer: function(v, params, data){
    return ((v === 0 || v > 1) ? '(' + v +' Tasks)' : '(1 Task)');
},
     * </code></pre>
     */
    /**
     * @cfg {String} summaryType (Optional) The type of
     * calculation to be used for the column.  For options available see
     * {@link #Calculations}.
     */

    constructor : function(config){
        Ext.apply(this, config);
        Ext.ux.grid.ServerSummary.superclass.constructor.call(this);
    },
    
    init : function(grid){
        this.grid 	= grid;
        var v 			= this.view = grid.getView();
        v.doGroupEnd = this.doGroupEnd.createDelegate(this);

        v.afterMethod('onColumnWidthUpdated', 		this.doWidth, 		this);
        v.afterMethod('onAllColumnWidthsUpdated', 	this.doAllWidths, this);
        v.afterMethod('onColumnHiddenUpdated', 		this.doHidden, 	this);
        v.afterMethod('onUpdate', 						this.doUpdate, 	this);
        v.afterMethod('onRemove', 						this.doRemove, 	this);

        if(!this.tableTpl){
            this.tableTpl = new Ext.Template(
                '<div class="x-grid-group-server-summary" >',
                '{rows}',
                "</div>"
            );
            this.tableTpl.disableFormats = true;
        }
        this.tableTpl.compile();
        
        if(!this.rowTpl){
            this.rowTpl = new Ext.Template(
                '<div class="x-grid3-summary-row" style="{tstyle}">',
                '<table class="x-grid3-summary-table" border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
                    '<tbody><tr>{cells}</tr></tbody>',
                '</table></div>'
            );
            this.rowTpl.disableFormats = true;
        }
        this.rowTpl.compile();

        if(!this.cellTpl){
            this.cellTpl = new Ext.Template(
                '<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} {css}" style="{style}" {cellAttr}>', 
                '<div class="x-grid3-cell-inner x-grid3-col-{id}" unselectable="on" {attr}>{value}</div>',
                "</td>"
            );
            this.cellTpl.disableFormats = true;
        }
        this.cellTpl.compile();
 
    },

    /**
     * Toggle the display of the summary row on/off
     * @param {Boolean} visible <tt>true</tt> to show the summary, <tt>false</tt> to hide the summary.
     */
    toggleSummaries : function(visible){
        var el = this.grid.getGridEl();
        if(el){
            if(visible === undefined){
                visible = el.hasClass('x-grid-hide-summary');
            }
            el[visible ? 'removeClass' : 'addClass']('x-grid-hide-summary');
        }
    },

    renderSummary : function(a, cs){
        cs = cs || this.view.getColumnData();
        var cfg = this.grid.getColumnModel().config,
            buf, c, p = {}, cf, last = cs.length-1;
        var rowBuf = [];
        for(var k=0;k<a.length;k++){
        	  
        	  buf   = [];
        	  var o = a[k];
        	  
			  for(var i = 0, len = cs.length; i < len; i++){
					c 			= cs[i];		
					cf 		= cfg[i];	
					p.id 		= c.id;		
					p.style 	= c.style;	
					p.css 	= i == 0 ? 'x-grid3-cell-first ' : (i == last ? 'x-grid3-cell-last ' : '');
					if( cf.summaryRenderer || c.renderer ){ // cf.summaryType 
						 p.value = ( cf.summaryRenderer || c.renderer)(o.data[c.name], p, o); 
					}else{
						 p.value = o.data[c.name];
					}
					if(p.value == undefined || p.value === "") p.value = "&#160;";
					buf[buf.length] = this.cellTpl.apply(p);
			  }
	
			  rowBuf[rowBuf.length] = this.rowTpl.apply(
			  	  {
			  	  	  tstyle: 'width:'+this.view.getTotalWidth()+';',
			  	  	  cells: buf.join('')
			  	  }
			  );
			  
        }
        
        return this.tableTpl.apply( 
        	 	{
        	  		rows: rowBuf.join('') 
        	 	}
        	 );
        
    },

    /**
     * Returns the summaryData for the specified groupValue or null.
     * @param {String} groupValue
     * @return {Object} summaryData
     */
    getSummaryData : function(groupValue){
    	 
        var reader = this.grid.getStore().reader,
            json 	 = reader.jsonData,
            fields = reader.recordType.prototype.fields,
            v;
        
        if(json && json.summaryData){
            v = json.summaryData[groupValue];
            if(v){
            	var dataArray = []; 
            	for(var i=0;i<v.length;i++){
            		dataArray[dataArray.length] = 
            			{
            				data: reader.extractValues(v[i], fields.items, fields.length),
            				json: v[i]
            			};
               }
               return dataArray;
            }
        }
        
        return null;
    },
    
    /**
     * @private
     * @param {Object} rs
     * @param {Object} cs
     */ 
    calculate : function(rs, cs){
        var gcol 		= this.view.getGroupField(),
            gvalue 	= rs[0].data[gcol],
            gdata 	= this.getSummaryData(gvalue);
        return gdata; 
    },

    doGroupEnd : function(buf, g, cs, ds, colCount){
        var dataArray = this.calculate(g.rs, cs);
        buf.push('</div>', this.renderSummary(dataArray, cs), '</div>');
    },

    doWidth : function(col, w, tw){
        if(!this.isGrouped()){
            return;
        }
        var gs = this.view.getGroups(),
            len = gs.length,
            i = 0,
            j = 0,
            sg,
            sglen,
            s;
        for(; i < len; ++i){
        	  sg 		= gs[i].childNodes[2].childNodes;
        	  sglen 	= sg.length;
        	  for(j=0;j<sglen;j++){
        	  	  s = sg[j];
        	  	  s.style.width = tw;
        	  	  s.firstChild.style.width = tw;
        	  	  s.firstChild.rows[0].childNodes[col].style.width = w;
        	  }
        }
    },

    doAllWidths : function(ws, tw){
        if(!this.isGrouped()){
            return;
        }
        var gs = this.view.getGroups(),
            len = gs.length,
            i = 0,
            j, 
            s, 
            cells, 
            sg,
            sglen,
            k,
            wlen = ws.length;
            
        for(; i < len; i++){
        	  sg 		= gs[i].childNodes[2].childNodes;
        	  sglen 	= sg.length;
        	  for(k=0;k<sglen;k++){
        	  	  	s = sg[k];
        	  	  	s.style.width = tw;
					s.firstChild.style.width = tw;
					cells = s.firstChild.rows[0].childNodes;
					for(j = 0; j < wlen; j++){
						 cells[j].style.width = ws[j];
					}
        	  }            
        }
    },

    doHidden : function(col, hidden, tw){
        if(!this.isGrouped()){
            return;
        }
        var gs = this.view.getGroups(),
            len = gs.length,
            i = 0,
            s,
            sg,
            sglen,
            k,
            display = hidden ? 'none' : '';
        for(; i < len; i++){
        	   sg 		= gs[i].childNodes[2].childNodes;
        	   sglen 	= sg.length;
        	   for(k=0;k<sglen;k++){
        	  	  	s = sg[k];
        	  	  	s.style.width = tw;
        	  	  	s.firstChild.style.width = tw;
        	  	  	s.firstChild.rows[0].childNodes[col].style.display = display;
        	   }
        }
    },
    
    isGrouped : function(){
        return !Ext.isEmpty(this.grid.getStore().groupField);
    },

    // Note: requires that all (or the first) record in the
    // group share the same group value. Returns false if the group
    // could not be found.
    refreshSummary : function(groupValue){
        return this.refreshSummaryById(this.view.getGroupId(groupValue));
    },

    getSummaryNode : function(gid){
        var g = Ext.fly(gid, '_gsummary');
        if(g){
            return g.down('.x-grid-group-server-summary', true);
        }
        return null;
    },

    refreshSummaryById : function(gid){
        var g = Ext.getDom(gid);
        if(!g){
            return false;
        }
        var rs = [];
        this.grid.getStore().each(function(r){
            if(r._groupId == gid){
                rs[rs.length] = r;
            }
        });
        var cs = this.view.getColumnData(),
            dataArray = this.calculate(rs, cs),
            markup = this.renderSummary(dataArray, cs),
            existing = this.getSummaryNode(gid);
            
        if(existing){
            g.removeChild(existing);
        }
        Ext.DomHelper.append(g, markup);
        return true;
    },

    doUpdate : function(ds, record){
        this.refreshSummaryById(record._groupId);
    },

    doRemove : function(ds, record, index, isUpdate){
        if(!isUpdate){
            this.refreshSummaryById(record._groupId);
        }
    },

    /**
     * Show a message in the summary row.
     * <pre><code>
grid.on('afteredit', function(){
    var groupValue = 'Ext Forms: Field Anchoring';
    summary.showSummaryMsg(groupValue, 'Updating Summary...');
});
     * </code></pre>
     * @param {String} groupValue
     * @param {String} msg Text to use as innerHTML for the summary row.
     */
    showSummaryMsg : function(groupValue, msg){
        var gid = this.view.getGroupId(groupValue),
             node = this.getSummaryNode(gid);
        if(node){
        	  node.innerHTML = '<div class="x-grid3-summary-row" style="width:'+this.view.getTotalWidth()+';">'+
										'<table class="x-grid3-summary-table x-grid3-summary-table-msg" border="0" cellspacing="0" cellpadding="0" style="width:'+this.view.getTotalWidth()+';">'+
											'<tbody>'+
												'<tr>'+
													'<td class="x-grid3-col x-grid3-cell x-grid3-td-0 x-grid3-cell-first" >'+
														'<div class="x-grid3-cell-inner x-grid3-col-0 x-grid3-summary-msg" unselectable="on">'+ msg + '</div>'+
													'</td>'+
												'</tr>'+
											'</tbody>'+
										'</table>'+
									'</div>';
        }
    },
    
    /**
     * <pre><code>
grid.on('afteredit', function(){
    var groupValue = 'Ext Forms: Field Anchoring';
    summary.showSummaryMsg(groupValue, 'Updating Summary...');
    setTimeout(function(){ // simulate server call
        // ServerSummary class implements updateSummaryData
        summary.updateSummaryData(groupValue,
            // create data object based on configured dataIndex
            {description: 22, estimate: 888, rate: 888, due: new Date(), cost: 8});
    }, 2000);
});
     * </code></pre>
     * @param {String} groupValue
     * @param {Object} data data object
     * @param {Boolean} skipRefresh (Optional) Defaults to false
     */
    updateSummaryData : function(groupValue, data, skipRefresh){
        var json = this.grid.getStore().reader.jsonData;
        if(!json.summaryData){
            json.summaryData = {};
        }
        json.summaryData[groupValue] = data;
        if(!skipRefresh){
            this.refreshSummary(groupValue);
        }
    }
    
});

//backwards compat
Ext.grid.ServerSummary = Ext.ux.grid.ServerSummary;

Ext.reg('serversummary', Ext.ux.grid.ServerSummary);
