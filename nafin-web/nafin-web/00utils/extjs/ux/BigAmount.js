                Ext.form.BigAmount = Ext.extend(
                               Ext.form.TextField, 
                               {
                                               style:             'text-align:right;',
                                               allowNegative:                  true,
                                               allowDecimals:                 true,
                                               decimalSeparator:  ".",
                                               thousandSeparator: ",",
                                               baseChars:                                         "0123456789",
                                               autoStripChars:                true,
                                               maxLength:                                        2000,
															  //valido:								true,
                                               //controlRe1:                    '', // Expresiones regulares de control, favor de no usar
                                               //controlRe2:                    '', // Expresiones regulares de control, favor de no usar
                                               //controlRe3:                    '', // Expresiones regulares de control, favor de no usar
                                               //controlRe4:                    '', // Expresiones regulares de control, favor de no usar
                                               //controlRe5:                    '', // Expresiones regulares de control, favor de no usar
                                               //controlRe6:                    '', // Expresiones regulares de control, favor de no usar
                                               nanText:                                              "{0} is not a valid number",
                                               initComponent:                function() { 
 
                                                               this.baseChars += this.thousandSeparator;
                                                               
                                                               //call parent    
                                                               Ext.form.BigAmount.superclass.initComponent.call(this);
                               
                                                               //add listeners after parent called 
                                                               this.on({ 
                                                                              'blur' : { 
                                                                                              fn: this.onBlur, 
                                                                                              scope: this 
                                                                              } 
                                                               }); // end add listeners 
                               
                                               }, // end initComponent
                                               initEvents: function() {
                                                               
                                                                 var allowed =  this.baseChars + '';
                                                                 if (this.allowDecimals) {
                                                                                              allowed += this.decimalSeparator;
                                                                 }
                                                                 if (this.allowNegative) {
                                                                                              allowed += '-';
                                                                 }
                                                                 allowed = Ext.escapeRe(allowed);
                                                                 this.maskRe = new RegExp('[' + allowed + ']');
                                                                 if (this.autoStripChars) {
                                                                                              this.stripCharsRe = new RegExp('[^' + allowed + ']', 'gi');
                                                                 }
                                                                  
                                                                 Ext.form.BigAmount.superclass.initEvents.call(this);
                                                                 
                                               },
                                               onBlur:                                                function(event) { 
                                                               var bigamountv = this.getValue();
                                                               if(this.hasFocus){
																						this.hasFocus=false;
																					}
																					
																					
                                                               if( !Ext.isEmpty( bigamountv ) ){

                                                                              if(!this.isBigNumber(bigamountv)){
                                                                                              return;
                                                                              }
                                                                              
                                                                              if( !Ext.isDefined(this.controlRe5)){
                                                                                              this.controlRe5 = new RegExp('[' + Ext.escapeRe( this.thousandSeparator + "-" )  + ']','g');
                                                                              } else {
                                                                                              this.controlRe5.lastIndex = 0;
                                                                              }
                                                                              if( !Ext.isDefined(this.controlRe6)){
                                                                                              this.controlRe6 = new RegExp( Ext.escapeRe( this.decimalSeparator ) );
                                                                              }
                                               
                                                                              var isNegative  = bigamountv.charAt(0) == '-'?true:false;
                                                                              bigamountv      = bigamountv.replace(this.controlRe5,""); // Regular Expresion: /[,\-]/g
                                                                              var amount            = bigamountv.split(this.controlRe6);      // Regular Expresion: /\./
                                                                              var integer             = amount[0].split('');
                                                                              integer                                = integer.length == 0?"0":integer;
                                                                              var decimal            = amount.length > 1?amount[1].split(''):new Array();
                                                                              
                                                                              // Formatear parte entera
                                                                              var formattedAmout = "";
                                                                              for(var i=0;i<integer.length;i++){
                                                                                              var index                            =  integer.length-1-i;
                                                                                              formattedAmout += integer[i];
                                                                                              if( index > 0 && index % 3 == 0 ){
                                                                                                              formattedAmout += this.thousandSeparator;
                                                                                              }
                                                                              }
                                                                              // Formatear parte decimal
                                                                              if( this.allowDecimals ){
                                                                                              if(        decimal.length == 0 ){
                                                                                                              formattedAmout += this.decimalSeparator + "00";
                                                                                              } else if( decimal.length == 1 ){
                                                                                                              formattedAmout += this.decimalSeparator + decimal[0] + "0";
                                                                                              } else {
                                                                                                              formattedAmout += this.decimalSeparator + decimal[0] + decimal[1];
                                                                                              }
                                                                              }
                                                                              if(isNegative){
                                                                                              formattedAmout = "-" + formattedAmout;
                                                                              }
                                                                              this.setValue(formattedAmout);
                                                               }
																					
																					/*
																					var expReg = new RegExp('[' + Ext.escapeRe( this.thousandSeparator + "-" )  + ']','g');
																					var valor = this.getValue().replace(expReg,"");
																					
																					if(this.validateValue(valor)){
																						this.clearInvalid();
																					}else{
																						this.markInvalid();
																					}*/

                                                               
                                               },
                                               // private
                                               getErrors: function(value) {
                                                               
                                                                 var errors = Ext.form.BigAmount.superclass.getErrors.apply(this, arguments);
                                                                 
                                                                 value = Ext.isDefined(value) ? value : this.processValue(this.getRawValue());
                                                                 
                                                                 if (value.length < 1) { // if it's blank and textfield didn't flag it then it's valid
                                                                                              return errors;
                                                                 }
                                                                 
                                                                 if(!this.isBigNumber(value)){
                                                                                 errors.push(String.format(this.nanText, value));
                                                                 }

                                                                 return errors;
                                                                 
                                               },
                                               // private
                                               isBigNumber: function(bigNumberValue){

                                                               // Build Control RegExp
                                                               if( !Ext.isDefined(this.controlRe1) ){
                                                                              
                                                                              var special = "";
                                                                              special += this.thousandSeparator;
                                                                              if (this.allowDecimals) {
                                                                                              special += this.decimalSeparator;
                                                                              }
                                                                              if (this.allowNegative) {
                                                                                              special += '-';
                                                                              }
                                                                              special = Ext.escapeRe(special);
                                                                                
                                                                              this.controlRe1 = new RegExp('^[' + special + ']$');    // { "-", ".", "," }
                                                                              this.controlRe2 = new RegExp('[' + special + ']{2,}');  // { "-.", ".-", ",-" ... } 
                                                                              this.controlRe3 = new RegExp('[' + Ext.escapeRe(this.decimalSeparator) + '][^' + Ext.escapeRe(this.decimalSeparator) +']+[' + Ext.escapeRe(this.decimalSeparator) + ']');  // { ".****." }
                                                                              this.controlRe4 = new RegExp('[' + Ext.escapeRe('-') + '][^' + Ext.escapeRe('-') +']+[' + Ext.escapeRe('-') + ']');    // { "-****-"  }
                                                                              
                                                               }
                                                                 
                                                               if(        !this.maskRe.test(bigNumberValue)                          ){ // !E a { 1234567890.,- }
                                                                              return false;
                                                               } else if(  this.controlRe1.test(bigNumberValue)                      ){ // Se encontro { "-", ".", "," }
                                                                              return false;
                                                               } else if(  this.controlRe2.test(bigNumberValue)                      ){ // Se encontro { "-.", ".-", ",-" ... }                                                    
                                                                              return false;
                                                               } else if( this.allowDecimals && this.controlRe3.test(bigNumberValue) ){ // Se encontro { ".****." }
                                                                              return false;
                                                               } else if( this.allowDecimals && bigNumberValue.indexOf(".")     == 0 ){ // Se encontro { ".NNNN" }
                                                                              return false;
                                                               } else if( this.allowNegative && bigNumberValue.lastIndexOf("-") >  0 ){ // Se encontro { "N-" }
                                                                              return false;
                                                               } else if( this.allowNegative && this.controlRe4.test(bigNumberValue) ){ // Se encontro { "-****-"  }                                                        
                                                                              return false;
                                                               }  
 
                                                               return true;
                                                               
                                               }
                                               
                               }
                );
                Ext.reg('bigamount', Ext.form.BigAmount );
                
                if(Ext.form.BigAmount){
                  Ext.apply(Ext.form.BigAmount.prototype, {
                               nanText: "{0} no es un n�mero v�lido."
                  });
                }
