/*!
 * Ext JS Library 3.4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 * Modified by jshernandez, since: F012-2013, 04/06/2013 12:48:40 p.m.
 */
Ext.ns('Ext.ux.grid');

/**
 * @class Ext.ux.grid.MsgCheckColumn
 * @extends Ext.grid.Column
 * <p>A Column subclass which renders a checkbox ( with option to show personalized tooltip messages ) in each column cell which toggles the truthiness of the associated data field on click.</p>
 * <p><b>Note. As of ExtJS 3.3 this no longer has to be configured as a plugin of the GridPanel.</b></p>
 * <p>Example usage:</p>
 * <pre><code>
var cm = new Ext.grid.ColumnModel([{
       header: 'Foo',
       ...
    },{
       xtype: 'msgcheckcolumn',
       header: 'Indoor?',
       dataIndex: 'indoor',
       dataStatusIndex: 'indoorStatus' // this field will store checkcolumn status
       width: 55
    }
]);

// create the grid
var grid = new Ext.grid.EditorGridPanel({
    ...
    colModel: cm,
    ...
});
 * </code></pre>
 * In addition to toggling a Boolean value within the record data, this
 * class toggles a css class between <tt>'x-grid3-check-col'</tt> and
 * <tt>'x-grid3-check-col-on'</tt> to alter the background image used for
 * a column. 
 *
 * It can also show a different icon with a personalized tooltip message, you just
 * need to modify the record status string appending the message string:
 * ;MESSAGE:<iconClassName>:<MessageContent.>
 *
 * So, for a checkcolumn record with a status: "NORMAL", our message string
 * will look like:
 * 
 * NORMAL;MESSAGE:iconClassName:MessageContent.
 *
 * For "READONLY" status:
 * 
 * READONLY;MESSAGE:iconClassName:MessageContent.
 * 
 * It is important to note when showing a message the checkbox icon is replaced 
 * by the icon specified in the MESSAGE. 
 *
 */
Ext.ux.grid.MsgCheckColumn = Ext.extend(Ext.grid.Column, {

    /**
     * @private
     * Process and refire events routed from the GridView's processEvent method.
     */
    processEvent: function(name, e, grid, rowIndex, colIndex){
    	 
        if (name == 'mousedown') {
            var record       = grid.store.getAt(rowIndex);
            // If record status is different from "NORMAL", ignore mouse click.
            var recordDataStatus = record.data[this.dataStatusIndex];
            if( recordDataStatus.match(/^NORMAL[;]?$/) == null ){ // different from NORMAL
            	return false;
            }
            record.set(this.dataIndex, !record.data[this.dataIndex]); 
            return false; // Cancel row selection.
        } else {
            return Ext.grid.ActionColumn.superclass.processEvent.apply(this, arguments);
        }
        
    },

    renderer: function(v, p, record){
 
        var recordDataStatus = record.data[this.dataStatusIndex];
        
        if(        recordDataStatus.match(/^(NORMAL|READONLY);MESSAGE:[^:]+:/) != null ){
        	  
        	  // Get icon class & tooltip message
        	  var recordDataStatusFields = recordDataStatus.split(":");
        	  recordDataStatusFields.shift();
        	  var iconMessage = recordDataStatusFields.shift();
        	  var messageText = recordDataStatusFields.join(":");
        	  
        	  // Render component
        	  p.css += ' x-grid3-check-col-td'; 
        	  return String.format('<img alt="" src="{0}" class="{1}" ext:qtip="{2}" />',Ext.BLANK_IMAGE_URL,iconMessage,messageText);
					
         } else if( recordDataStatus.match(/^NORMAL[;]?$/)   != null ){

				p.css += ' x-grid3-check-col-td'; 
				return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
				
			} else if( recordDataStatus.match(/^READONLY[;]?$/) != null ){	
				
				p.css += ' x-grid3-check-col-td'; 
				return String.format('<div class="x-grid3-check-col{0} x-item-disabled">&#160;</div>', v ? '-on' : '');
				
			} 
 		
    },

    // Deprecate use as a plugin. Remove in 4.0
    init: Ext.emptyFn
    
});
			
// Remove message from checkbox
Ext.ux.grid.MsgCheckColumn.prototype.supressMessage = function(record,dataStatusIndex){
	
	var recordDataStatus = record.data[dataStatusIndex];
	var matchArray = recordDataStatus.match(/^(NORMAL|READONLY)[;]?/);
	if( matchArray != null ){
		record.set( dataStatusIndex, matchArray[0] );
	}
	
}

// Override a checkbox with an icon message
Ext.ux.grid.MsgCheckColumn.prototype.setMessage = function( record, dataStatusIndex, iconClass, messageText ){
	
	var recordDataStatus = record.data[dataStatusIndex];
	if(        recordDataStatus.match(/^NORMAL[;]?/)   != null ){
		record.set( dataStatusIndex, String.format("NORMAL;MESSAGE:{0}:{1}",   iconClass, messageText) );
	} else if( recordDataStatus.match(/^READONLY[;]?/) != null ){
		record.set( dataStatusIndex, String.format("READONLY;MESSAGE:{0}:{1}", iconClass, messageText) );
	}
	
}
					
// register ptype. Deprecate. Remove in 4.0
Ext.preg('msgcheckcolumn', Ext.ux.grid.MsgCheckColumn);

// backwards compat. Remove in 4.0
Ext.grid.MsgCheckColumn = Ext.ux.grid.MsgCheckColumn;

// register Column xtype
Ext.grid.Column.types.msgcheckcolumn = Ext.ux.grid.MsgCheckColumn;


