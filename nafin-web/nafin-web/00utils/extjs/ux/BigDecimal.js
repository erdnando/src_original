	Ext.namespace('Ext.ux.form');
	/**
	 * @class Ext.ux.form.BigDecimal
	 * @extends Ext.form.TextField
	 * <p>BigDecimal text field that provides automatic keystroke filtering, formatting, numeric validation, 
	 * and arbitrary-precision signed decimal numbers. The maximum number is actually restricted to maximum 
	 * input field length, which is 500, but it can be changed at will.</p><p><pre><code>
	 
	 	// Define a comma separated bigdecimal "integer" field
		var registers = { 
			xtype: 			'bigdecimal',
			name: 			'registers',
			id: 				'registers',
			allowDecimals: false,
			allowNegative: false,
			fieldLabel: 	'Number of records containing the application',
			blankText:		'Please capture the number of records',
			allowBlank: 	false,
			hidden: 			false,
			maxLength: 		9,	
			msgTarget: 		'side',
			anchor:			'-20',
			maxValue: 		'99,999',
			format:			'0,000'
		};
		
		// Define a money formatted bigdecimal field 
		var amount = { 
			xtype: 			'bigdecimal',
			name: 			'amountsSum',
			id: 				'amountsSum',
			fieldLabel:		'Sum of amounts',
			blankText:		'Please capture the sum of amounts',
			allowBlank:		false,
			allowNegative: false,
			hidden: 			false,
			maxLength: 		26,	
			msgTarget: 		'side',
			anchor:			'-20', 
			maxValue: 		'999,999,999,999,999,999.99',
			format:			'0,000.00'
		};
		
</code></pre></p>
	 *
    * @constructor
    * <p>Creates a new BigDecimal Field</p>
	 * @xtype bigdecimal
	 * @author Salim Hernandez
	 * @since ( 26/03/2013 06:42:41 p.m. )
	 * @note Some code taken from ExtJS classes.
	 *
	 */
	Ext.ux.form.BigDecimal = Ext.extend(
		Ext.form.TextField,  
		{
			/**
			 * @cfg {RegExp} stripCharsRe @hide
			 */
			/**
			 * @cfg {RegExp} maskRe @hide
			 */
			/**
			 * @cfg {RegExp} thousandSeparatorRe - Used to strip thousand separator chars from number @hide 
			 */
			/**
			 * @cfg {RegExp} validateRe - Used for BigDecimal validation @hide
			 */
			/**
			 * @cfg {String} style A custom style specification to be applied to this component's Element. BigDecimal 
			 *                     alignment to the right by default. 
			 */
			style:             'text-align:right;',
			/**
			 * @cfg {Boolean} allowNegative False to prevent entering a negative sign (defaults to true)
			 */
			allowNegative: 	 true,
			/**
          * @cfg {Boolean} allowDecimals False to disallow decimal values (defaults to true)
          */
			allowDecimals:		 true,
			/**
			 * @cfg {String} decimalSeparator Character(s) to allow as the decimal separator (defaults to '.')
			 */
			decimalSeparator:  ".",
			/**
			 * @cfg {String} thousandSeparator Character(s) to allow as the group separator (defaults to ',')
			 */
			thousandSeparator: ",",
			/**
			 * @cfg {String} baseChars The base set of characters to evaluate as valid numbers (defaults to '0123456789').
			 */
			baseChars: 			 "0123456789",
			/**
			 * @cfg {Boolean} autoStripChars True to automatically strip not allowed characters from the field. Defaults to <tt>true</tt>
			 */
			autoStripChars:	 true,
			/**
			 * @cfg {Number} maxLength Maximum input field length allowed by validation ( defaults to 500 chars ).
			 * This behavior is intended to provide instant feedback to the user by improving usability to allow pasting
			 * and editing or overtyping and back tracking. 
			 */
			maxLength: 			 500,
			/**
			 * @cfg {Number} minValue The minimum allowed value (defaults to none). You can use the 
			 *               thousand separator to present the number formatted
			 */
			minValue:			 undefined,
			/**
		    * @cfg {Number} maxValue The maximum allowed value (defaults to none). You can use the 
		    *               thousand separator to present the number formatted
		    */
			maxValue:			 undefined, 
			/**
			 * @cfg {String} format The way you would like to format this text. Examples (128453.789): 
			 *                      0         - (128454) show only digits, no precision
			 *								0.00      - (128453.79) show only digits, 2 precision
			 *								0.0000    - (128453.7890) show only digits, 4 precision
			 *								0,000     - (128,454) show comma and digits, no precision
			 *								0,000.00  - (128,453.79) show comma and digits, 2 precision
			 *								0,0.00    - (128,453.79) shortcut method, show comma and digits, 2 precision
			 *								To reverse the grouping (,) and decimal (.) for international numbers, add /i to 
			 *                      the end. For example: 0.000,00/i
			 *                It uses the same formating representation as defined in Ext.util.Format.number
			 */
			format:				 undefined, // Format as in Ext.util.Format.number, 0,000.00, 0000.00, etc.
			/**
			 * @cfg {String} nanText Error text to display if the value is not a valid number.  For example, this can happen
			 * if a valid character like '.' or '-' is left in the field with no number (defaults to "{value} is not a valid number")
			 */
			nanText: 			 '{0} is not a valid number',
			/**
			 * @cfg {String} minText Error text to display if the minimum value validation fails (defaults to "The minimum value for this field is {minValue}")
			 */
			minText: 			 'The minimum value for this field is {0}',
			/**
			 * @cfg {String} maxText Error text to display if the maximum value validation fails (defaults to "The maximum value for this field is {maxValue}")
			 */
			maxText:				 'The maximum value for this field is {0}',
			// private
			initComponent: function(){
				
				if(        Ext.isEmpty(this.decimalSeparator) ){
					throw "BigDecimalException: Decimal Separator char is required.";
				} else if( Ext.isEmpty(this.thousandSeparator) ){
					throw "BigDecimalException: Thousand Separator char is required.";
				} else if( this.decimalSeparator == this.thousandSeparator ){
					throw "BigDecimalException: Decimal  Separator char cannot be equal to Thousand Separator char.";
				}
				
				this.validateRe 				= new RegExp('^[\-]?[' + this.baseChars + ']+[' + Ext.escapeRe(this.decimalSeparator) + ']?[' + this.baseChars + ']*$');
				this.thousandSeparatorRe	= new RegExp(Ext.escapeRe(this.thousandSeparator),"g");
				this.minValue					= this.minValue?String(this.minValue):this.minValue;
				this.maxValue					= this.maxValue?String(this.maxValue):this.maxValue;
				//call parent    
				Ext.ux.form.BigDecimal.superclass.initComponent.call(this);
				
			},
			// private
			initEvents: function(){
				
				var allowed =   this.baseChars + '';
				if( this.thousandSeparator ){
					allowed +=  this.thousandSeparator;
				}
				if (this.allowDecimals) {
					allowed += this.decimalSeparator;
				}
				if (this.allowNegative) {
					allowed += '-';
				}
				allowed = Ext.escapeRe(allowed);
				this.maskRe = new RegExp('[' + allowed + ']');
				if (this.autoStripChars) {
					this.stripCharsRe = new RegExp('[^' + allowed + ']', 'gi');
				}
				
				Ext.ux.form.BigDecimal.superclass.initEvents.call(this);
				
			},
			// private
			parseValue: function(value) {
				value = String(value).replace(this.thousandSeparatorRe, "");
				
				if( !this.isBigDecimal(value)){
					return '';
				}
				// Remove zeroes to the right
				var neg 	= value.charAt(0) === '-'?true:false;
				value 	= neg?value.substring(1):value;
				if( value.match("^[0]{2,}[\.][0123456789]*$") ){
					value = value.replace(/^[0]{2,}/,"0");
				} else if( value.match("^[0]{2,}$") ){
					value = value.replace(/^[0]{2,}/,"0");
				} else if( value.match("^[0]{2,}") ){
					value = value.replace(/^[0]{2,}/,"");
				}
				value 	= ( neg?'-':'') + value;
				return value;
				
         },
          /**
			  * Runs all of BigDecimal validations and returns an array of any errors. Note that this first
			  * runs TextField's validations, so the returned array is an amalgamation of all field errors.
			  * The additional validations run test that the value is a number, and that it is within the
			  * configured min and max values.
			  * @param {Mixed} value The value to get errors for (defaults to the current field value)
			  * @return {Array} All validation errors for this field
			  */
			getErrors: function(value) {
				var errors = Ext.ux.form.BigDecimal.superclass.getErrors.apply(this, arguments);
			  
			  	value = Ext.isDefined(value) ? value : this.processValue(this.getRawValue());
			  
			  	if (value.length < 1) { // if it's blank and textfield didn't flag it then it's valid
					 return errors;
			  	}
 
			  	value = String(value).replace(this.thousandSeparatorRe, ""); 
 
			  	if(!this.isBigDecimal(value)){
			  	  errors.push(String.format(this.nanText, value));
			  	}
			  
			   var num 				= this.parseValue(value);
			   var minimumValue 	= this.parseValue(this.minValue); // This allows to present a formatted number
			   var maximumValue  = this.parseValue(this.maxValue); // This allows to present a formatted number
			   			   
				if (this.minValue && this.compareBigDecimals( num, minimumValue ) < 0 ) {
			  	  errors.push(String.format(this.minText, this.minValue));
				}
			  
				if (this.maxValue && this.compareBigDecimals( num, maximumValue ) > 0 ) {
				  errors.push(String.format(this.maxText, this.maxValue));
				}
			  
				return errors;
			},
			// private
			beforeBlur: function() {
			
			  var v = this.parseValue(this.getRawValue());
				  
			  if (!Ext.isEmpty(v)) {
			  	  this.setValue(
			  	  	  this.formatBigDecimal(v, this.format)
			  	  );
			  }
			  
			},
			/**
			 * @private
          * Returns true if the passed value is a number. Returns false for non-finite numbers.
          * @param {String} bigNumberValue The value to test
          * @return {Boolean}
          */
			isBigDecimal: function(bigNumberValue){
				
				if( !this.validateRe.test(bigNumberValue) ){ 
					return false;
				}
				return true;
				
			},
			/**
			 * @private
          * Formats the BigDecimal according to the format string.
          * This conforms Ext.util.Format.number format standard.
          *
          * <div style="margin-left:40px">examples (123456.789):
          * <div style="margin-left:10px">
          * 0        - (123457)      show only digits, no precision<br>
          * 0.00     - (123456.79)   show only digits, 2 precision<br>
          * 0.0000   - (123456.7890) show only digits, 4 precision<br>
          * 0,000    - (123,457)     show comma and digits, no precision<br>
          * 0,000.00 - (123,456.78)  show comma and digits, 2 precision<br>
          * 0,0.00   - (123,456.78)  shortcut method, show comma and digits, 2 precision<br>
          * To reverse the grouping (,) and decimal (.) for international numbers, add /i to the end.
          * For example: 0.000,00/i
          * </div></div>
          *
          * @param  {BigDecimal}  v The number to format.
          * @param  {String}      Format The way you would like to format this text.
          * @return {String}      The formatted number.
          */
        formatBigDecimal: function(v, format) {
        	  
            if (!format) {
                return v;
            }
            
            if (!this.isBigDecimal(v)) {
                return '';
            }
            
            var comma = ',',
                dec   = '.',
                i18n  = false,
                neg   = v.charAt(0) === '-'?true:false; // Check if the number is negative

            v = v.replace("-",""); // Get absolute value of the number
            if (format.substr(format.length - 2) == '/i') {
                format = format.substr(0, format.length - 2);
                i18n   = true;
                comma  = '.';
                dec    = ',';
            }

            var hasComma = format.indexOf(comma) != -1,
                psplit   = (i18n ? format.replace(/[^\d\,]/g, '') : format.replace(/[^\d\.]/g, '')).split(dec);

            if (1 < psplit.length) {       // The format string has integer and decimal part
                v = this.toFixed(v,psplit[1].length,dec);
            } else if(2 < psplit.length) { // The format string is invalid, formats should have no more than 1 period. 
                throw ('NumberFormatException: invalid format, formats should have no more than 1 period: ' + format);
            } else {								 // The format string only has integer part.
                v = this.toFixed(v,0,dec);
            }

            var fnum = v;

            psplit = fnum.split(dec);

            if (hasComma) {
                var cnum = psplit[0], 
                    parr = [], 
                    j    = cnum.length, 
                    m    = Math.floor(j / 3),
                    n    = cnum.length % 3 || 3,
                    i;

                for (i = 0; i < j; i += n) {
                    if (i != 0) {
                        n = 3;
                    }
                    
                    parr[parr.length] = cnum.substr(i, n);
                    m -= 1;
                }
                fnum = parr.join(comma);
                if (psplit[1]) {
                    fnum += dec + psplit[1];
                }
            } else {
                if (psplit[1]) {
                    fnum = psplit[0] + dec + psplit[1];
                }
            }

            return (neg ? '-' : '') + format.replace(/[\d,?\.?]+/, fnum);
            
        },
        /**
         * @private
         * Converts a BigDecimal number into a string with a specified number of decimals.
         * If the desired number of decimals are higher than the actual number, zeroes are added 
         * to create the desired decimal length. If the desired number of decimals are lower than 
         * the actual number, it gets rounded.
         *
         * @param {String} v The number to fix precision.
         * @param {String} x The number of digits after the decimal point. Default is 0 (no 
         *                   digits after the decimal point)
         * @param {String} dec The decimal separator character.
         * @return (String) The number, with the exact number of decimals.
         */
        toFixed: function(v,x,dec){
        	  
        	  if( x < 0){
        	  	  throw "RangeError: toFixed(v,x,dec) x digits argument must be grater or equal to zero";
        	  }
        	  
        	  var neg	= v.charAt(0) === '-'?true:false;
        	  v 			= neg?v.substring(1):v;
        	  
        	  var aux			= v.split(dec);
        	  var integerPart = aux.length > 0 && aux[0].length > 0?aux[0].split(""):new Array("0");
        	  var decimalPart = aux.length > 1 && aux[1].length > 0?aux[1].split(""):new Array("0");
 
        	  while( decimalPart.length < x + 1 ){
        	  	  decimalPart.push("0");
        	  }
        	  
        	  var lastDigit = parseInt(decimalPart[x],10);
        	  decimalPart   = decimalPart.slice(0,x);
        	 
        	  // Check if rounding up applies
        	  if( lastDigit >= 5 ){
        	  	  // Convert the decimal part characters to numbers
        	  	  for(var i=0;i<decimalPart.length;i++){
        	  	  	  decimalPart[i] = parseInt(decimalPart[i],10);
        	  	  }
        	  	  // Convert the integer part characters to numbers
        	  	  for(var i=0;i<integerPart.length;i++){
        	  	  	  integerPart[i] = parseInt(integerPart[i],10);
        	  	  }
        	  	  // Apply rounding up starting from decimal part
        	  	  var accumulated = 1;
        	  	  for(var i=x-1; accumulated > 0 && i>=0;i--){
        	  	  	  var sum = decimalPart[i] + accumulated;
        	  	  	  if( sum > 9 ){
        	  	  	  	  var auxSum 	  = (new String(sum)).split("");
        	  	  	  	  decimalPart[i] = parseInt(auxSum[1],10);
        	  	  	  	  accumulated    = parseInt(auxSum[0],10);
        	  	  	  } else {
        	  	  	  	  decimalPart[i] = sum;
        	  	  	  	  accumulated    = 0
        	  	  	  }
        	  	  }
        	  	  // Apply rounding up to integer part if required.
        	  	  for(var i=integerPart.length-1; accumulated > 0 && i>=0;i--){
        	  	  	  var sum = integerPart[i] + accumulated;
        	  	  	  if( sum > 9 ){
        	  	  	  	  var auxSum 	  = (new String(sum)).split("");
        	  	  	  	  integerPart[i] = parseInt(auxSum[1],10);
        	  	  	  	  accumulated    = parseInt(auxSum[0],10);
        	  	  	  } else {
        	  	  	  	  integerPart[i] = sum;
        	  	  	  	  accumulated    = 0
        	  	  	  }
        	  	  }
        	  	  // If something left over, add it as the most significant digit of the integer part
        	  	  if( accumulated > 0 ){
        	  	  	  integerPart.unshift(accumulated);
        	  	  }
        	  	  
        	  }
        	  
        	  return (neg?'-':'')+integerPart.join("") + (x > 0? dec + decimalPart.join(""):"");
        	  
        },
        /**
         * @private
         * Compares two BigDecimal. Two BigDecimal objects that are equal in value but have a different scale 
         * (like 1.0 and 1.00) are considered equal by this method.
         *
         * @param {String} number1 - First  BigDecimal to compare.
         * @param {String} number2 - Second BigDecimal to compare.
         *
         * @return (Number) -1, 0, or 1 as the first BigDecimal is numerically less than, equal to, or greater 
         *                  than the second BigDecimal.
         */
        compareBigDecimals: function(number1,number2){
		  	  
        	  var aux;
 
		  	  var negNumber1     = number1.charAt(0) === '-'?true:false;
		  	  var negNumber2		= number2.charAt(0) === '-'?true:false;
		  	  
		  	  number1 				= negNumber1?number1.substring(1):number1;
		  	  number2 				= negNumber2?number2.substring(1):number2;
		  	  	  	  
		  	  if( negNumber1 && number1.match("^[0]+[\.]?[0]*$") ){ // Negative zero
		  	  	  negNumber1 = false;
		  	  }
		  	  if( negNumber2 && number2.match("^[0]+[\.]?[0]*$") ){ // Negative zero
		  	  	  negNumber2 = false;
		  	  }
		  	  
		  	  // number1 > number2
		  	  if(  !negNumber1  && negNumber2 ){
		  	  	  return 1;
		  	  } 
		  	  
		  	  // number1 < number2
		  	  if( negNumber1  &&  !negNumber2 ){
		  	  	  return -1
		  	  }
		  	  
		  	  aux = number1.split(this.decimalSeparator);
		  	  var integerNumber1 = aux.length > 0 && aux[0].length > 0?aux[0].split(""):new Array("0");
		  	  var decimalNumber1 = aux.length > 1 && aux[1].length > 0?aux[1].split(""):new Array("0");
		  	  
		  	  aux = number2.split(this.decimalSeparator);
		  	  var integerNumber2 = aux.length > 0 && aux[0].length > 0?aux[0].split(""):new Array("0");
		  	  var decimalNumber2 = aux.length > 1 && aux[1].length > 0?aux[1].split(""):new Array("0");
 
		  	  // Adjust numbers size, so that they are equal.
		  	  while( integerNumber1.length > integerNumber2.length ){
		  	  	  integerNumber2.unshift(0);
		  	  } 
		  	  while( integerNumber2.length > integerNumber1.length ){
		  	  	  integerNumber1.unshift(0);
		  	  }
		  	  while( decimalNumber1.length > decimalNumber2.length ){
		  	  	  decimalNumber2.push(0);
		  	  } 
		  	  while( decimalNumber2.length > decimalNumber1.length ){
		  	  	  decimalNumber1.push(0);
		  	  }
		  	  	
		  	  // Since the decimal point is irrelevant, join both parts.
		  	  var n1 				= new Array();
        	  var n2 				= new Array();
		  	  // Convert to number the integer part characters.
        	  for(var i=0;i<integerNumber1.length;i++){
        	  	  n1[i] 								= parseInt(integerNumber1[i],10);
        	  }
		  	  // Convert to number the decimal part characters.
        	  for(var i=0;i<decimalNumber1.length;i++){
        	  	  n1[i+integerNumber1.length] = parseInt(decimalNumber1[i],10);
        	  }
        	  // Convert to number the integer part characters.
        	  for(var i=0;i<integerNumber2.length;i++){
        	  	  n2[i] 								= parseInt(integerNumber2[i],10);
        	  }
        	  // Convert to number the decimal part characters.
        	  for(var i=0;i<decimalNumber2.length;i++){
        	  	  n2[i+integerNumber2.length] = parseInt(decimalNumber2[i],10);
        	  }
 
        	  // number1 > number2
		  	  for(var i=0;i<n1.length;i++){
		  	  	  if( n1[i] > n2[i] ){
		  	  	  	  return (negNumber1 && negNumber2)?-1:1;
		  	  	  } else if( n1[i] < n2[i] ){
		  	  	  	  break;  
		  	  	  }
		  	  }
		  	  // number1 < number2
		  	  for(var i=0;i<n1.length;i++){
		  	  	  if( n1[i] < n2[i] ){
		  	  	  	  return (negNumber1 && negNumber2)?1:-1;
		  	  	  } else if( n1[i] > n2[i] ){
		  	  	  	  break;  
		  	  	  }
		  	  }
		  	  // number1 == number2
		  	  return 0; 
		  	  
		  }
		  
		}
	);
	Ext.reg('bigdecimal', Ext.ux.form.BigDecimal);
	
	if(Ext.ux.form.BigDecimal){
	  Ext.apply(Ext.ux.form.BigDecimal.prototype, {
		 nanText: "{0} no es un n�mero v�lido.",
		 minText: "Debe ser mayor o igual a {0}",
		 maxText: "El valor m�ximo para este campo es {0}"
	  });
	}