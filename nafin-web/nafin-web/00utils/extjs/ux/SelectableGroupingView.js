/*!
 * Ext JS Library 3.4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */
/**
 * @class Ext.grid.SelectableGroupingView
 * @extends Ext.grid.GroupingView
 * Adds the ability for single level grouping to the grid. A {@link Ext.data.GroupingStore GroupingStore}
 * must be used to enable grouping.  Some grouping characteristics may also be configured at the
 * {@link Ext.grid.Column Column level}<div class="mdetail-params"><ul>
 * <li><code>{@link Ext.grid.Column#emptyGroupText emptyGroupText}</code></li>
 * <li><code>{@link Ext.grid.Column#groupable groupable}</code></li>
 * <li><code>{@link Ext.grid.Column#groupName groupName}</code></li>
 * <li><code>{@link Ext.grid.Column#groupRender groupRender}</code></li>
 * </ul></div>
 * <p>Sample usage:</p>
 * <pre><code>
var grid = new Ext.grid.GridPanel({
    // A groupingStore is required for a SelectableGroupingView
    store: new {@link Ext.data.GroupingStore}({
        autoDestroy: true,
        reader: reader,
        data: xg.dummyData,
        sortInfo: {field: 'company', direction: 'ASC'},
        {@link Ext.data.GroupingStore#groupOnSort groupOnSort}: true,
        {@link Ext.data.GroupingStore#remoteGroup remoteGroup}: true,
        {@link Ext.data.GroupingStore#groupField groupField}: 'industry'
    }),
    colModel: new {@link Ext.grid.ColumnModel}({
        columns:[
            {id:'company',header: 'Company', width: 60, dataIndex: 'company'},
            // {@link Ext.grid.Column#groupable groupable}, {@link Ext.grid.Column#groupName groupName}, {@link Ext.grid.Column#groupRender groupRender} are also configurable at column level
            {header: 'Price', renderer: Ext.util.Format.usMoney, dataIndex: 'price', {@link Ext.grid.Column#groupable groupable}: false},
            {header: 'Change', dataIndex: 'change', renderer: Ext.util.Format.usMoney},
            {header: 'Industry', dataIndex: 'industry'},
            {header: 'Last Updated', renderer: Ext.util.Format.dateRenderer('m/d/Y'), dataIndex: 'lastChange'}
        ],
        defaults: {
            sortable: true,
            menuDisabled: false,
            width: 20
        }
    }),

    view: new Ext.grid.SelectableGroupingView({
        {@link Ext.grid.GridView#forceFit forceFit}: true,
        // custom grouping text template to display the number of items per group
        {@link #groupTextTpl}: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
    }),

    frame:true,
    width: 700,
    height: 450,
    collapsible: true,
    animCollapse: false,
    title: 'Grouping Example',
    iconCls: 'icon-grid',
    renderTo: document.body
});
 * </code></pre>
 * @constructor
 * <p>Creates a new SelectableGroupingView </p>
 * @param {Object} config
 * @xtype selgroupview
 * @author Salim Hernandez
 * @since ( 06/26/2013 12:53:40 p.m. )
 * @note Some code taken from ExtJS classes.
 *
 */
Ext.grid.SelectableGroupingView = Ext.extend(Ext.grid.GroupingView, {

    /**
     * @cfg {String} groupSelectText The string used to render the group header selection text.
     */
    groupSelectionText: 'Select this group', 
    
    /**
     * @cfg {String} groupSelectTextWidth The string used to render the group header selection text width.
     */
    groupSelectionTextWidth: 90, 

    /**
     * @cfg {Boolean} Render hidden the group checkbox field (default is false).
     */
    hideGroupCheckBox: false,
    
    // private
    doClearData: function(){
    	 // this.state = {}; // This line has ben comented to allow state object behave as in the original component
       this.checkBoxSelectedState = {};
       this.checkBoxReadOnlyState = {};
    },
    
    // private
    init: function(grid) {
    	 Ext.grid.SelectableGroupingView.superclass.init.call(this,grid);
       var s = grid.store;
       s.afterMethod('clearData', this.doClearData, this);
    },
    
    // private
    initTemplates: function(){
    	 
        Ext.grid.GroupingView.superclass.initTemplates.call(this);
        this.state = {}; 
        this.checkBoxSelectedState = {}; 
        this.checkBoxReadOnlyState = {}; 
        
        var sm = this.grid.getSelectionModel();
        sm.on(sm.selectRow ? 'beforerowselect' : 'beforecellselect',
                this.onBeforeRowSelect, this);

        if(!this.startGroup){ 
            this.startGroup = new Ext.XTemplate(
                '<div id="{groupId}" class="x-grid-group {cls} {clsSelectedCheckBox}" >', 
                	 '<div id="{groupId}-hd" class="x-grid-group-hd" style="{style}">'+
                    		'<div class="x-grid-group-title" >', 
                    			'<table cellspacing="0" cellpadding="0" border="0" width="100%" >',
                    				'<tr>',
                    					'<td style="vertical-align:top;" >', this.groupTextTpl, '</td>',
                    					'<td id="{groupId}-checkboxhd" width="'+this.groupSelectionTextWidth+'" class="x-grid-checkboxhd {clsReadOnlyCheckBox}" >',
                    						this.groupSelectionText,
                    					'</td>',
                    				'</tr>',
                    			'</table>',
                    		'</div>',
                    '</div>',
                    '<div id="{groupId}-bd" class="x-grid-group-body">'
            );
        }
        this.startGroup.compile();

        if (!this.endGroup) {
            this.endGroup = '</div></div>';
        }
        
        // Agregar eventos nuevos de seleccion de grupo
        this.grid.addEvents(	
        	  /**
             * @event groupselectclick
             * The raw click event for the grid group selection area.
             * @param {Ext.EventObject} e
             */
        	  'groupselectclick',
        	  /**
             * @event groupselectmousedown
             * The raw mousedown event for the grid group selection area.
             * @param {Ext.EventObject} e
             */
        	  'groupselectmousedown',
        	  /**
             * @event groupselectdblclick
             * The raw dblclick event for the grid group selection area.
             * @param {Ext.EventObject} e
             */
        	  'groupselectdblclick'
        );
        
    },

    processEvent: function(name, e){ 

        Ext.grid.GroupingView.superclass.processEvent.call(this, name, e);
        var hd = e.getTarget('.x-grid-group-hd', this.mainBody);
        if(hd){
            // group value is at the end of the string
            var field = this.getGroupField(),
                prefix = this.getPrefix(field),
                groupValue = hd.id.substring(prefix.length),
                emptyRe = new RegExp('gp-' + Ext.escapeRe(field) + '--hd');
            // remove trailing '-hd'
            groupValue = groupValue.substr(0, groupValue.length - 3);

            var groupIdRe 			= new RegExp( Ext.escapeRe( prefix + groupValue ) + '-checkboxhd' );
            var groupTarget		= e.getTarget();
            var groupSelectEvent	= false;
 
            if( groupValue && groupTarget && groupIdRe.test(groupTarget.id) ){
            	 // El componente est� deshabilitado, por lo que se cancela el evento
            	 var cbel			= Ext.get(groupTarget);
            	 if( cbel.hasClass('x-item-disabled')){
            	 	 return false;
            	 }
            	 // El componente fue seleccionado
            	 groupSelectEvent = true;
            	 this.grid.fireEvent('groupselect' + name, this.grid, field, groupValue, e);
            // also need to check for empty groups
            } else if( groupValue || emptyRe.test(hd.id)){
                this.grid.fireEvent('group' + name, this.grid, field, groupValue, e);
            } 
            
            if(        name == 'mousedown' && e.button == 0 && groupSelectEvent == true ){
            	 this.toggleGroupCheckbox(hd.parentNode);
            } else if( name == 'mousedown' && e.button == 0 ){
                this.toggleGroup(hd.parentNode);
            }
            
        }

    },
 
    // private
    getSelectedGroupList: function(){
 
    	 // Get html group componente prefix
    	 var groupField = this.getGroupField();
       var prefix 	 = this.getPrefix(groupField);
       
       // Get groups which are selectable => read only state = false
    	 var selectableGroupList = [];
    	 Ext.iterate(
    	 	 this.checkBoxReadOnlyState,
    	 	 function(gid,readOnly){
    	 	 	 if(!readOnly){
    	 	 	 	 selectableGroupList.push(gid);
    	 	 	 }
    	 	 }
    	 );
    	 
    	 // Get selected group list
    	 var selectedGroupList	 = [];
    	 for(var i=0;i<selectableGroupList.length;i++){
    	 	 var gid		  = selectableGroupList[i];
    	 	 var selected = this.checkBoxSelectedState[gid];
    	 	 if( selected ){
    	 	 	 selectedGroupList.push(gid.substring(prefix.length));
    	 	 }
    	 }
    	 
    	 return selectedGroupList;

    },
    
    /**
     * Returns the checkbox selected state data for the specified groupValue or false.
     * @param {String} groupValue
     * @return {Object} summaryData
     */
    getCheckBoxSelectedStateData : function(gid){
 
        var reader = this.grid.getStore().reader,
            json 	 = reader.jsonData,
            v;
            
        if(json && json.groupCheckBoxData){
        	   
        	  // Get html group componente prefix
        	   var groupField = this.getGroupField();
        	   var prefix 	   = this.getPrefix(groupField);
        	   var groupValue = gid.substring(prefix.length);
        
            v = json.groupCheckBoxData[groupValue];
            if(v && v.match(/^true[;]?/) != null ){
               return true;
            }
            
        }
        
        return false;
        
    },
    
    /**
     * Returns the checkbox read only state data for the specified groupValue or false.
     * @param {String} groupValue
     * @return {Object} summaryData
     */
    getCheckBoxReadOnlyStateData : function(gid){
    	 
        var reader = this.grid.getStore().reader,
            json 	 = reader.jsonData,
            v;
        
        if(json && json.groupCheckBoxData){
        	  
        	  // Get html group componente prefix
        	  var groupField = this.getGroupField();
        	  var prefix 	  = this.getPrefix(groupField);
        	  var groupValue = gid.substring(prefix.length);
        
           v = json.groupCheckBoxData[groupValue];
           if(v && v.match(/;readonly$/) != null ){
               return true;
           }
           
        }
        
        return false;
        
    },
 
    /**
     * Toggles the specified group checkbox if no value is passed, otherwise sets the selected state of the group to the value passed.
     * @param {String} groupId The groupId assigned to the group (see getGroupId)
     * @param {Boolean} groupSelected (optional)
     */
    toggleGroupCheckbox: function(group, groupSelected) { 
    	 
        var gel = Ext.get(group),
            id  = Ext.util.Format.htmlEncode(gel.id);
            
        // Determinar el nuevo estado
        groupSelected = Ext.isDefined(groupSelected) ? groupSelected: !gel.hasClass('x-grid-group-selected');
        // El nuevo estado es diferente, entonces actualizarlo
        if( this.checkBoxSelectedState[id] !== groupSelected ){ // Los estados son diferentes
            this.checkBoxSelectedState[id] = groupSelected;
            gel[groupSelected ?'addClass':'removeClass']('x-grid-group-selected');
        }
        
    },

    // private
    getGroupText: function(v, r, groupTextRenderer, rowIndex, colIndex, ds){
        var column = this.cm.config[colIndex],
            g = groupTextRenderer ? groupTextRenderer.call(column.scope, v, {}, r, rowIndex, colIndex, ds) : String(v);
        if(g === '' || g === '&#160;'){
            g = column.emptyGroupText || this.emptyGroupText;
        }
        return g;
    },
    
    // private
    doRender : function(cs, rs, ds, startRow, colCount, stripe){
    	  /*
    	  	cs: 				Propiedades de cada una de las columnas
    	  	rs: 				Cada uno de los registros del store
    	  	ds: 				Store
    	  	startRow: 		Indice del primer registro que se mostrar�
    	  	columnCount: 	Numero de columnas del grid
    	  	stripe:			Indica si se intercalaran los colores de fondo de cada uno de los registros.
    	  */
        if(rs.length < 1){ // No hay registros, cancelar la operacion
            return '';
        }

        // Si el grouping view no puede agrupar, o solo se est� actualizando el contenido usar gridview original
        if(!this.canGroup() || this.isUpdating){
            return Ext.grid.GroupingView.superclass.doRender.apply(this, arguments);
        }

        // Obtener nombre del groupfield
        var groupField = this.getGroupField(),
        // Obtener indice de columna del group field
            colIndex = this.cm.findColumnIndex(groupField),
            g,
        // Obtener longitud total
            gstyle = 'width:' + this.getTotalWidth() + ';',
        // Obtener configuracion del column model con respecto a la columna de agrupacion
            cfg = this.cm.config[colIndex],
        // Obtener function que realiza el dibujado del grupo
            groupRenderer = cfg.groupRenderer || cfg.renderer,
        // Obtener function que realiza el dibujado de la descripcion del grupo
            groupTextRenderer = cfg.groupTextRenderer,
        // Obtener prefix con el nombre del grupo
            prefix = this.showGroupName ? (cfg.groupName || cfg.header)+': ' : '',
            groups = [],
            curGroup, i, len, gid;
            
        // Determinar si se ocultar� el checkbox
        var hideGroupCheckBox = this.grid.getStore().reader.jsonData.hideGroupCheckBox === true?true:false;
        
        for(i = 0, len = rs.length; i < len; i++){
        	   // Obtener indice de registro
            var rowIndex = startRow + i,
            	 // Obtener registro asociado al renglon
                r = rs[i], 
                // Obtener valor del grupo
                gvalue = r.data[groupField];
                // Obtiene el valor del grupo usando para ello el groupRenderer
                g = this.getGroup(gvalue, r, groupRenderer, rowIndex, colIndex, ds),
                // Obtiene el valor del texto del grupo usando para ello el groupTextRenderer
                t = groupTextRenderer?this.getGroupText(gvalue, r, groupTextRenderer, rowIndex, colIndex, ds):prefix + g;
                // Si curGroup no est� definido � es un grupo nuevo
            if(!curGroup || curGroup.group != g){
            	 // Obtner group id
                gid = this.constructId(gvalue, groupField, colIndex);
                // if state is defined use it, however state is in terms of expanded
                // so negate it, otherwise use the default.
                this.state[gid] 						= !(Ext.isDefined(this.state[gid]) 					  ? !this.state[gid] 				  : this.startCollapsed);
                
                this.checkBoxSelectedState[gid] = Ext.isDefined(this.checkBoxSelectedState[gid]) ? this.checkBoxSelectedState[gid]: this.getCheckBoxSelectedStateData(gid);
                this.checkBoxReadOnlyState[gid] = Ext.isDefined(this.checkBoxReadOnlyState[gid]) ? this.checkBoxReadOnlyState[gid]: this.getCheckBoxReadOnlyStateData(gid);
                // Definir curgroup
                curGroup = {
                    group: g,
                    gvalue: gvalue,
                    text: t,
                    groupId: gid,
                    startRow: rowIndex,
                    rs: [r],
                    cls: 						this.state[gid]				 	  ? '': 'x-grid-group-collapsed',
                    clsSelectedCheckBox: 	this.checkBoxSelectedState[gid] ? 'x-grid-group-selected': '', 
                    clsReadOnlyCheckBox:  this.checkBoxReadOnlyState[gid] ? 'x-item-disabled'      : '',   
                    style: gstyle
                };
               
                if( this.hideGroupCheckBox || hideGroupCheckBox ){
                	 curGroup['cls'] += ' x-grid-checkboxhd-hidden';
                }
                // Agregar curgroup a la lista de grupos 
                groups.push(curGroup);
            }else{
            	 // Ir a�adiendo los demas registros a curgroup
                curGroup.rs.push(r);
            }
            // Al registro leido agregarle la propiedad group Id
            r._groupId = gid;
        }

        var buf = [];
        // Para cada uno de los grupos le�dos
        for(i = 0, len = groups.length; i < len; i++){
        	   // Leer grupo
            g = groups[i];
            // Pintar cabecera
            this.doGroupStart(buf, g, cs, ds, colCount);
            // Pintar renglones del grupo
            buf[buf.length] = Ext.grid.GroupingView.superclass.doRender.call(
                    this, cs, g.rs, ds, g.startRow, colCount, stripe);
            // Finalizar grupo
            this.doGroupEnd(buf, g, cs, ds, colCount);
        }
        // Regresar seccion pintada del grupo
        return buf.join('');
    }
    
});
// private
Ext.grid.SelectableGroupingView.GROUP_ID = 2000;

Ext.reg('selgroupview', Ext.grid.SelectableGroupingView);