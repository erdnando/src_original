var winwA = null;
var winwB = null;


Ext.onReady(function() {
  var urlServicioNetPay = Ext.getDom('urlServicioNetPay').value;
	var OrderNumber = Ext.getDom('OrderNumber').value;
	var OrderDescription = Ext.getDom('OrderDescription').value;
	var Amount = Ext.getDom('Amount').value;
	var MerchantResponseURL = Ext.getDom('MerchantResponseURL').value;
	var SID = Ext.getDom('SID').value;
	var REQ = Ext.getDom('REQ').value;
	var hidStrUsuario = Ext.getDom('hidStrUsuario').value;
	var fechaTrans = Ext.getDom('fechaTrans').value;
	var horaTrans = Ext.getDom('horaTrans').value;
	var totalReg = Ext.getDom('totalReg').value;
  var StoreID = Ext.getDom('StoreID').value;



var formDataTrans = new Ext.form.FormPanel({
		standardSubmit: true,
		url: urlServicioNetPay,
		hidden: false,
		height: 'auto',
		width: 480,
		collapsible: false,
		titleCollapse: false,
		style: 'margin:0 auto;',
		labelWidth: 10,
		defaultType: 'textfield',
		frame: true,
		title: 'Datos del Pago',
		id: 'formaMod',
		layout:'fit',
		defaults: 
		{
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype:'box',
				id:'mensajeXXX',
				style: 'margin:0 auto;',
				html:'<table  width=450><tr><td align="center"><table>'+
						'<tr><td class="formas" ><b>Usuario:</b></td><td class="formas">'+hidStrUsuario+'</td></tr>'+
						'<tr><td class="formas" ><b>Fecha:</b></td><td class="formas">'+fechaTrans+'</td></tr>'+
						'<tr><td class="formas" ><b>Hora:</b></td><td class="formas">'+horaTrans+'</td></tr>'+
						'<tr><td class="formas" ><b>N�mero de orden:</b></td><td class="formas">'+OrderNumber+'</td></tr>'+
						'<tr><td class="formas" ><b>N�mero de documentos:</b></td><td class="formas">'+totalReg+'</td></tr>'+
						'<tr><td class="formas" ><b>Monto:</b></td><td class="formas">'+Amount+'</td></tr>'+
						'<tr><td class="formas" ><b>Moneda:</b></td><td class="formas">Moneda Nacional</td></tr>'+
						'</table></td></tr></table>'
			},
			{ xtype: 'hidden', name: 'OrderNumber', id: 'OrderNumber', value: OrderNumber },
			{ xtype: 'hidden', name: 'OrderDescription', id: 'OrderDescription', value: OrderDescription },
			{ xtype: 'hidden', name: 'Amount', id: 'Amount', value: Amount },
			{ xtype: 'hidden', name: 'MerchantResponseURL', id: 'MerchantResponseURL', value: MerchantResponseURL },
			{ xtype: 'hidden', name: 'SID', id: 'SID', value: SID },
			{ xtype: 'hidden', name: 'REQ', id: 'REQ', value: REQ },
      { xtype: 'hidden', name: 'StoreID', id: 'StoreID', value: StoreID }
			
		],
		monitorValid: true,
		buttons: [
			{
				xtype: 'button',
				text: 'Aceptar',
				name: 'btnBuscar',
				iconCls: '',
				hidden: false,
				formBind: true,
				handler: function(boton, evento){
					pnl.el.mask('Enviando Transacci�n.. ', 'x-mask');
					window.top.window.realizaPeticionTrans(this.window, this.document, OrderNumber, 'PREGUARDADO');
          window.top.window.reSizeWinIframe(700, 600);
					formDataTrans.getForm().submit(); 
				}
			},
			{
				xtype: 'button',
				text: 'Cancelar',
				name: 'btnCancelar',
				iconCls: '',
				hidden: false,
				formBind: true,
				handler: function(boton, evento){
					window.top.window.realizaPeticionTrans(this.window, this.document, OrderNumber, 'CANCELAR');
				}
			}
		]
	});

	var pnl = new Ext.Container({
	  id: 'contenedorPrincipal',
	  applyTo: 'areaContenido',
	  style: 'margin:0 auto;',
	  width: 485,
    //layout:'fit',
	  items:
	   [
			formDataTrans
		]
  });
  
  var objSize = formDataTrans.getSize();
  window.top.window.reSizeWinIframe(objSize.width, objSize.height);

});