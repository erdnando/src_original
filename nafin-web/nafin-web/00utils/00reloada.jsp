<%@page import = "
		com.nafin.security.UserNE,
		com.nafin.security.UserSignOnNE,
		java.io.File,
		java.io.FileInputStream,
		java.io.FileOutputStream,
		java.io.InputStream,
		java.io.OutputStream,		
		java.util.HashMap,
		java.util.Iterator,
		java.util.List,
		java.util.Map,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileItemFactory,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload"
	errorPage = "/00utils/error.jsp" buffer="16kb" autoFlush="true"%>
<%
UserNE 		userNE	= session.getAttribute("userNE")==null?new UserNE():(UserNE)session.getAttribute("userNE");
UserSignOnNE 	userSO	= new UserSignOnNE();
boolean         ursOK   = userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()));
%>
<html>
<head>
<link rel="Stylesheet" type="text/css" href="/nafin/14seguridad/Seguridad/css/nafin.css">
<script language="JavaScript">
<!--
<%if(!"ADMIN".equals(userNE.getNe_perfil())) {%>
document.onkeydown = 
	function() {
		if(window.event && window.event.keyCode == 116) {
			window.event.keyCode = 0;
			return false;
		}
	}

if (window.Event) 
	document.captureEvents(Event.MOUSEUP); 
function nocontextmenu() { 
	event.cancelBubble = true 
	event.returnValue = false; 
	return false; 
} 
function norightclick(e) { 
	if(window.Event) { 
		if(e.which !=1)
			return false; 
	} else if (event.button !=1) { 
		event.cancelBubble = true 
		event.returnValue = false; 
		return false; 
	}
}
document.oncontextmenu = nocontextmenu; 
document.onmousedown = norightclick;
<%}//if(!"ADMIN".equals(userNE.getNe_perfil()))%>

function regresar () {
	var f = document.frmContenido;
	f.action = "00reload.jsp";
	f.target= "_self";
	f.submit();
}
//-->
</script>
</head>
<body>
<%
try {
	Map 	hiddens 		= new HashMap();
	String 	rutaAplicacion 	= getServletConfig().getServletContext().getRealPath("/");
	String 	rutaArchivos 	= rutaAplicacion+File.separatorChar+"00tmp"+File.separatorChar+"15cadenas"+File.separatorChar;
	String 	rutaAbsoluta 	= "";
	String 	archivoProcesar	= "";
	String 	nombreArchivo	= "";
	String 	extension 		= "";

	boolean isMultipart = ServletFileUpload.isMultipartContent(request);
	if(isMultipart) {
		FileItemFactory 	factory 	= new DiskFileItemFactory();
		ServletFileUpload 	upload 		= new ServletFileUpload(factory);
		List 				items 		= upload.parseRequest(request);
		Iterator 			iter 		= items.iterator();
		while (iter.hasNext()) {
			FileItem 	item 	= (FileItem) iter.next();
			String 		name 	= item.getFieldName();
			String 		value 	= item.getString();
			if (item.isFormField()) {
				hiddens.put(name, value);
			} else {
				String 	nombreCampo 	= item.getFieldName();
				long 	tamanioArchivo 	= item.getSize();
				String 	archivoOrigen 	= item.getName();
				String 	contentType 	= item.getContentType();
						nombreArchivo	= archivoOrigen;
				if(archivoOrigen.lastIndexOf("/")>0) {
					nombreArchivo	= archivoOrigen.substring(archivoOrigen.lastIndexOf("/")+1);
				} else if(archivoOrigen.lastIndexOf("\\")>0) {
					nombreArchivo	= archivoOrigen.substring(archivoOrigen.lastIndexOf("\\")+1);
				}
				archivoProcesar = nombreArchivo.substring(0, nombreArchivo.lastIndexOf("."));
				extension 		= archivoOrigen.substring(archivoOrigen.indexOf("."));
				File 	archivo 		= new File(rutaArchivos + File.separatorChar + nombreArchivo);
				item.write(archivo);
				rutaAbsoluta			= archivo.getAbsolutePath();
			}
		}//while (iter.hasNext())
	}//if(isMultipart)
	
	String txtPath 	= hiddens.get("txtPath").toString().trim();
	boolean success = false;
	if("ADMIN".equals(userNE.getNe_perfil())&& userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()))) {
		InputStream source = new FileInputStream(rutaAbsoluta);
		OutputStream destiny = new FileOutputStream(rutaAplicacion+txtPath+File.separatorChar+archivoProcesar+extension);
		byte[] buf = new byte[1024];
		int len;
		while ((len = source.read(buf)) > 0) {
			destiny.write(buf, 0, len);
		}
		source.close();
		destiny.close();
		success = (new File(rutaAbsoluta)).delete();
	}%>
<form name="frmContenido" method="post" action="00reloada.jsp" enctype="multipart/form-data">
<table width="100%" cellpadding="1" cellspacing="2" border="0">
<tr>
	<td>&nbsp;</td>
</tr>
<%if(success) {%>
	<tr>
		<td align="center">
			<table align="center" cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF">
				<tr>
					<td class="celda01" align="center" colspan="2">File Loaded</td>
				</tr>
			</table>
		</td>
	</tr>
<%}//if(numFiles>0)%>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td align="center">
		<input type="Button" class="celda02" value="Back" title="Back" onclick="regresar();">
	</td>
</tr>
<%
} catch(Exception e) { 
%>
<table width="100%" cellpadding="0" celspacing="0" border="0">
<tr>
  <td align="center">
    <table cellspacing="0" cellpadding=="2" border="1" width="80%" align="center" bordercolor="#31659C" class="tabla">
    <tr>
      <td align="center" class="celda02"><%=e.getMessage()%></td>
    </tr>
    </table>
  </td>
</tr>
<%e.printStackTrace();%>
</table>
<%
}
%>
</form>
</body>
</html>

