<%@ page import="java.lang.*" %>

<html>
<head>
	<title>Nafinet - Error</title>
	<style type="text/css">
		<!--
		.celda {  font-family: Arial, Helvetica, sans-serif; font-size: 11px}
		-->
	</style>
</head>

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<br><br>
<table align="center" cellpadding="4" cellspacing="2" border="0">
<tr>
	<td class="celda" align="center"><font color="red"><strong>Error en la autentificaci&oacute;n</strong></font></td>
</tr>
<tr>
	<td class="celda" align="center">El certificado digital seleccionado, no corresponde al usuario <%= session.getAttribute("strNombreUsuario")%>.<br>
	Para accesar a esta secci&oacute;n es necesario verificarlo.</td>
</tr>
</table>

</body>
</html>
