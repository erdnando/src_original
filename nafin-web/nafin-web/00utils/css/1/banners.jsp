<%@ page import="java.util.*, netropology.utilerias.*"%>
<%
try {
	java.util.Map banners = (java.util.Map)application.getAttribute("appBanners");
	java.util.List num = new java.util.ArrayList();
	if (banners.size() > 1) {
		for (int g = 2; g <= banners.size(); g++) {
			num.add(new Integer(g));
		}
		java.util.Collections.shuffle(num);
	}
	num.add(0, new Integer(1));
	if (banners.size() > 0) {
%>
<%@ include file="/appComun.jspf" %>
<div id="_banners" class="bannerSeccion" style="visibility:hidden; padding: 5px; height: 250px; width: 220px;">
<%
		for (int g=0; g<num.size(); g++) {
            Integer indiceRnd = (Integer)num.get(g);
            com.netro.admcontenido.Banner banner = (com.netro.admcontenido.Banner)banners.get(indiceRnd);
            int claveBanner = Integer.valueOf(banner.getClaveBanner());
            String[] enlacesWeb = { "",
                      "http://www.creditojoven.gob.mx",                                                         // 1 Credito Joven
                      "http://www.nafin.com/portalnf/content/financiamiento/mujeres-empresarias.html",          // 2 Mujeres empresarias  
                      "http://www.nafin.com/portalnf/content/financiamiento/crecer-juntos.html",               // 3 Crecer juntos        
                      "http://www.nafin.com/portalnf/content/financiamiento/financiamiento_construccion.html ",  // 4 Industria de la construcción 
                    }; 
            if ( claveBanner != 6 ){
%>
	<div class="bannerItem" title="">
		<a href="<%=enlacesWeb[claveBanner]%>" target="_blank"><img border="0" src="<%=appWebContextRoot%>/home/img/Banners/banner<%=((com.netro.admcontenido.Banner)banners.get(indiceRnd)).getClaveBanner()%>s.jpg"></a>
		<span class="title"><%=((com.netro.admcontenido.Banner)banners.get(indiceRnd)).getNombreBanner()%></span>
		<span class="subtitle">&nbsp;</span>
	</div>
<%
            }
            else{
%>
	<div class="bannerItem" title="">
		<a href="<%=appWebContextRoot%>/home/banners.jsp?id=banner_<%=((com.netro.admcontenido.Banner)banners.get(indiceRnd)).getClaveBanner()%>" target="_blank"><img border="0" src="<%=appWebContextRoot%>/home/img/Banners/banner<%=((com.netro.admcontenido.Banner)banners.get(indiceRnd)).getClaveBanner()%>s.jpg"></a>
		<span class="title"><%=((com.netro.admcontenido.Banner)banners.get(indiceRnd)).getNombreBanner()%></span>
		<span class="subtitle">&nbsp;</span>
	</div>
<%
            }
		} //fin for
%>
</div>
<%
	}//fin if
} catch(Throwable e) {
	System.out.println("ERROR al obtener los banners");
	e.printStackTrace();
}
%>