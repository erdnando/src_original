Ext.ns('NE.firma');

var privateKeyBuffer = new ArrayBuffer(0); // ArrayBuffer with loaded private key
var certificateBuffer = new ArrayBuffer(0); // ArrayBuffer with loaded certificate

NE.firma.FormSelectFirma = Ext.define('FormSelectFirma', {
	extend: 'Ext.form.FormPanel',
	textoAFirmar: null,
	initComponent : function(){
		
		 Ext.apply(this, {
			id: 'formSelectFirma1',
			//width: 549,
			height:250,
			title: '<p align="center">Selecci�n Firma Digital</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',  
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos()
			//buttons: this.generarBotones()
		});
		
		NE.firma.FormSelectFirma.superclass.initComponent.call(this);
	},
	generarCampos: function(){
		var objSelectFirma = this;
		return [
                    {
                        xtype: 'filefield',
                        id: 'cert_file',
                        emptyText: 'Ruta del Certificado',
                        fieldLabel: 'Archivo de certificado',
                        name: 'cert_file',   
                        buttonText: 'Examinar...',
                        width: 320,
                        listeners: {
                            change: function(fb, v){
								
								fb.setRawValue(v.substring(v.lastIndexOf("\\") + 1));
                                objSelectFirma.setHandlerFileUpload(fb.fileInputEl, v);
                            }
                        },
						regex: /^.*\.(cer|CER)$/,
						regexText:'Solo se admiten archivos con extensi�n *.cer'
                        //vtype: 'archivotxt'
                    },
                    {
                        xtype: 'filefield',
                        id: 'privkey_file',
                        emptyText: 'Ruta de Clave Privada',
                        fieldLabel: 'Archivo de Clave Privada',
                        name: 'privkey_file',   
                        buttonText: 'Examinar...',
                        width: 320,
                        listeners: {
                            change: function(fb, v){
								fb.setRawValue(v.substring(v.lastIndexOf("\\") + 1));
                                objSelectFirma.setHandlerFileUpload(fb.fileInputEl, v);
                            }
                        },
                        regex: /^.*\.(key|KEY)$/,
						regexText:'Solo se admiten archivos con extensi�n *.key'
                    },
                    {
                        fieldLabel: 'Contrase�a de Clave Privada',
                        xtype: 'textfield',
                        name: 'passwordFirma',
                        id: 'passwordFirma',				
                        inputType: 'password', 
                        style:'padding:4px 3px;',
                        allowBlank: false,
                        margins: '0 20 0 0'  
                    },
                    {
                        fieldLabel: 'Texto a firmar',
                        xtype: 'textarea',
                        name: 'textoAFirmar',
                        id: 'textoAFirmar',
						height: 100,
                        style:'padding:4px 3px;',
                        allowBlank: false,
                        margins: '0 20 0 0',
						readOnly: true,
						value: objSelectFirma.textoAFirmar
                    }
                ]
	},
	setHandlerFileUpload: function(evt, v){
		var temp_reader = new FileReader();
		var current_files = evt.dom.files;
		
		temp_reader.onload = function(event)
		{
			if (evt.id == 'privkey_file-button-fileInputEl') {
				privateKeyBuffer = event.target.result;
			} else if (evt.id == 'cert_file-button-fileInputEl') {
				certificateBuffer = event.target.result;
			}
		};
		
		temp_reader.readAsArrayBuffer(current_files[0]);
	}
	
});

//Ext.reg('form_select_firma', NE.firma.FormSelectFirma);


NE.firma.WinFirmaDigital = Ext.define('WinFirmaDigital', {
	extend: 'Ext.Window',
	//esFirmaCorrecta:'N',
	copiaParametros: null,
	getResultFirma:null,
	fnCallback: null,
	textoFirmar: null,
	objFormSelectFirma: null,
	initComponent : function(){
             var obj = this;
             Ext.apply(this, {
                    layout: 'form',
                    autoHeight:true,
                    //x:300,
                    //y:100,
                    width:650,
                    resizable:false,
                    modal: true,
                    id:'winFirmaDigital1',
                    title: ' ',
                    bodyStyle: 'text-align:center',
                    items: this.iniConfirmaFirmaDigital(obj),
                    listeners:{
                            beforeclose:function(win){
								
								if(win.getResultFirma!==null){
									win.getResultFirma(win.esFirmaCorrecta);
								}
                            }
                    },
                    buttons:[
                            '-',
                            '->',
                            {
                                xtype: 'button', buttonAlign:'right', text: 'Firmar', id: 'btnConfirmarFirmarPkcs7', 
                                handler: function(){
                                        var windowPkcs7 = obj;
										windowPkcs7.getEl().mask("Realizando Firma");
										
										setTimeout(function(){ 
											windowPkcs7.signData(windowPkcs7.textoFirmar, function(miPkcs7, miTextoFirmar){
													
													if (Ext.isEmpty(miPkcs7)) {
															windowPkcs7.getEl().unmask();
															return;
													} else {
															
															windowPkcs7.copiaParametros.splice(0, 1, miPkcs7);
															windowPkcs7.getEl().unmask();
															windowPkcs7.fnCallback.apply(null, windowPkcs7.copiaParametros);
															windowPkcs7.close();
													}
											});
										}, 1000);
										
                                }
                            },
                            {
                                text: 'Cancelar',
                                id: 'btnRegresarFirmaWin',
                                handler: function(btn){
									obj.close();
                                }
                                    
                            }
                    ]
            });
            
            NE.firma.WinFirmaDigital.superclass.initComponent.call(this);
	},
	iniConfirmaFirmaDigital: function(obj){
        
            obj.objFormSelectFirma = new NE.firma.FormSelectFirma({
				textoAFirmar: obj.textoFirmar
			});
            
            return [obj.objFormSelectFirma];
	},
	signData: function(textoFirmar, fnCallbackSignData) {
			var objWin = this;
			var formaValida = true;
			
			if(!isWCAPISupported()) {
				return;
			}
			
			//Validating form variables
			if(certificateBuffer.byteLength === 0) {
				var vCertFile = Ext.getCmp("cert_file");
				vCertFile.markInvalid('Debe seleccionar el certificado del firmante');
				formaValida = false;
			}			
		
			if(privateKeyBuffer.byteLength === 0) {
				var vPrivkeyFile = Ext.getCmp("privkey_file");
				vPrivkeyFile.markInvalid('Debe seleccionar la clave privada');
				formaValida = false;
			}
			
            var passwordFirma =  Ext.getCmp("passwordFirma").getValue();
			if(passwordFirma == '') {
				var vPasswordFirma = Ext.getCmp("passwordFirma");
				vPasswordFirma.markInvalid('Debe ingresar la contrase\u00f1a de clave privada password');
				formaValida = false;
			}

			if(formaValida && objWin.objFormSelectFirma.getForm().isValid()){
				var cipheredKey;
				var privateKeyBufferString = arrayBufferToString(privateKeyBuffer);
				var pKey = privateKeyBufferString.replace(/(-----(BEGIN|END) PRIVATE KEY-----|\r\n)/g, '');
				
				if(pKey.charAt(0) === "M") {
					cipheredKey = window.atob(pKey);
				}
				else {
					cipheredKey = privateKeyBufferString;
				}
				
				var certKey;
				var certKeyBufferString = arrayBufferToString(certificateBuffer);
				var pCert = certKeyBufferString.replace(/(-----(BEGIN|END) CERTIFICATE-----|\r\n)/g, '');
				
				if(pCert.charAt(0) === "M") {
					certKey = window.atob(pCert);
				}
				else {
					certKey = certKeyBufferString;
				}
				
				try {
					//Getting password and data to sign
					var password = Ext.getCmp("passwordFirma").getValue();//document.getElementById("password").value;
					var hashAlgorithm = "SHA-256";
					
					// Signing data
					if (window.Promise) {
						
						var pkcs7Promise = pkcs7FromContent(password, cipheredKey, certKey, hashAlgorithm, textoFirmar, false);
						pkcs7Promise.then(function(Pkcs7) {
							var b64PKCS7Message = window.btoa(arrayBufferToString(Pkcs7, false, true));
							fnCallbackSignData(b64PKCS7Message, textoFirmar);
						}, function(error) {
							var mensajePkcs7 = error.message;
							fnCallbackSignData("", textoFirmar);
							if(mensajePkcs7 === "Error during getting pkcs8: Error while decrypting private key"){
								mensajePkcs7 = "Error al descifrar la clave privada, favor de revisar la contrase�a.";
							}
							Ext.MessageBox.alert("Mensaje",mensajePkcs7);
						});
					} else {
						objWin.getEl().unmask();
						Ext.MessageBox.alert("Mensaje","El navegador utilizado no tiene soporte para realizar la firma electr�nica.");
					}
				} catch(err) {
					objWin.getEl().unmask();
					Ext.MessageBox.alert("Mensaje","SgDataCrypto Error:\n " + err.message + "\n" + err.stack);
				}
			}else{
				objWin.getEl().unmask();
				return;
			}
		} //End of signData 
});