<%@ page import="
java.sql.*,
java.math.*,
java.util.*,
netropology.utilerias.*,
netropology.utilerias.usuarios.*,
com.netro.exception.*"%>
<%@ include file="/15cadenas/015secsession2.jsp"%>

<%
session.setAttribute("iNoCliente",null);

//Par�metros provenienen de la p�gina anterior y actual
String ic_pyme = "";
String envia 			= (request.getParameter("envia")==null)?"":request.getParameter("envia");
String facultad		= (request.getAttribute("facultad")==null)?request.getParameter("facultad"):request.getAttribute("facultad").toString();


String datosPyme = "";

String nombre = "";
String login = (request.getParameter("login")==null)?"":request.getParameter("login");


//_________________________ INICIO DEL PROGRAMA _________________________
AccesoDB con = null;
try{
	con = new AccesoDB();
	con.conexionDB();
	if (envia.equals("CONSULTAR")) {
		System.out.println(" Inicio de la Busqueda de la pyme..." + login);
		datosPyme = getProveedor(login, con);
		
		ic_pyme = (datosPyme != null)?datosPyme.substring(0,datosPyme.indexOf('|')):null;
		
		System.out.println("Clave proveedor..." + ic_pyme);
	} else {
		ic_pyme = null;
	}
	if(envia.equals("ASIGNAR")){
		String nombrePymeAsigna = "";
		String nePymeAsigna = "";
		String pyme_asigna = request.getParameter("ic_pyme_asigna");
		String ic_pyme_asigna = pyme_asigna.substring(0,pyme_asigna.indexOf('|'));
		pyme_asigna = pyme_asigna.substring(pyme_asigna.indexOf('|')+1,pyme_asigna.length());
		nombrePymeAsigna = pyme_asigna.substring(0,pyme_asigna.indexOf('|'));
		pyme_asigna = pyme_asigna.substring(pyme_asigna.indexOf('|')+1,pyme_asigna.length());
		nePymeAsigna = pyme_asigna.substring(0,pyme_asigna.length());
		
		//System.out.println("ic_pyme_asigna=" + ic_pyme_asigna);
		//System.out.println("nombrePymeAsigna=" + nombrePymeAsigna);
		//System.out.println("nePymeAsigna=" + nePymeAsigna);
		//System.out.println("facultad=" + facultad);
		
		session.setAttribute("iNoCliente",ic_pyme_asigna);
		session.setAttribute("strNombrePymeAsigna",nombrePymeAsigna);
		session.setAttribute("strNePymeAsigna",nePymeAsigna);
		response.sendRedirect(facultad);
	}
%>
<html>
<head>
<title>Nafinet</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=strDirecVirtualCSS%>css/<%=strClase%>">
<script language="JavaScript" src="../../../00utils/valida.js?<%=session.getId()%>"></script>
<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
	window.open(theURL,winName,features);
}
function consultar(){   
	var f=document.frmContenido;
	//f.action = "/nafin/00utils/00buscapyme.jsp";
	f.target = "_self";
	f.envia.value="CONSULTAR"
	f.submit();
}

function cancelar(){
	window.close();
}

function seleccionar(ic_pyme) {  
	var f=document.frmContenido;
	f.target = "_self";
	f.envia.value="ASIGNAR";
	f.submit();
}

function recargar() {  
	var f=document.frmContenido;
	//f.action="00buscapyme_operador.jsp";
	f.submit();
}
//-->
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<div align="center">
<form name="frmContenido" method="post" action="<%=request.getRequestURI()%>">
<table width="400" cellpadding="0" cellspacing="0" border="0">
<tr>
	<td valign="top">
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td valign="top" align="center">
				<!--tabla de contenidos-->
				<table width="350" cellpadding="3" cellspacing="0" border="0">
				<input type="Hidden" name="envia" value="<%=envia%>">				
				<input type="Hidden" name="ic_pyme_asigna" value="<%=datosPyme%>">
				<input type="Hidden" name="facultad" value="<%=facultad%>">
				<tr>
					<td align="left" class="formas">Clave de Usuario:</td>
					<td class="formas">
						<input type="text" name="login" size="20" maxlength="8" value="<%=login%>" class="formas">
					</td>
				</tr>
				</table><br>
				<table cellpadding="0" cellspacing="5" border="0" class="formas">
				<tr>
					<td align="center" class="celda02" height="25" width="40"><a href="javascript:consultar();">Buscar</a></td>
					<td align="center" class="celda02" height="25" width="50"><a href="javascript:cancelar();">Cancelar</a></td>
				</tr>
				</table>
			</td>
		</tr>
<%			
		if(ic_pyme == null && envia.equals("CONSULTAR")) {
%>
						<script language="JavaScript">
						   alert("No existe informaci�n con los criterios determinados");
						   //window.location.href = "00buscapyme_operador.jsp";
						</script>
<%
		}
		
		if (ic_pyme != null) {
			Vector vecDomicilioPyme = getDomicilioPyme(ic_pyme, con);
			if(vecDomicilioPyme.size()>0) { %>
		<tr>
			<td valign="top" align="center"><br>
				<table width="450" cellpadding="3" cellspacing="0" border="1" bordercolor="#A5B8BF">
				<tr>
					<td align="center" class="celda01" rowspan="2">RFC</td>
					<td align="center" class="celda01" colspan="3">DOMICILIO</td>
					<td align="center" class="celda01" rowspan="2">Tel&eacute;fono</td>
				</tr>
				<tr>
					<td align="center" class="celda01">Calle y N&uacute;mero</td>
					<td align="center" class="celda01">Colonia</td>
					<td align="center" class="celda01">C.P.</td>
				</tr>
				<tr>
					<td align="center" class="formas"><%=vecDomicilioPyme.get(0)%>&nbsp;</td>
					<td align="center" class="formas"><%=vecDomicilioPyme.get(1)%>&nbsp;</td>
					<td align="center" class="formas"><%=vecDomicilioPyme.get(2)%>&nbsp;</td>
					<td align="center" class="formas"><%=vecDomicilioPyme.get(3)%>&nbsp;</td>
					<td align="center" class="formas"><%=vecDomicilioPyme.get(4)%>&nbsp;</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center"><br>
				<table cellpadding="0" cellspacing="5" border="0" class="formas">
				<tr>
					<td align="center" class="celda02" width="45" height="25"><a href="javascript:seleccionar('<%=ic_pyme%>');">Siguiente</a></td>
					<td align="center" class="celda02" width="50" height="25"><a href="javascript:cancelar();">Cancelar</a></td>
				</tr>
				</table>
			</td>
		</tr>
<%
			}
		}
%>
		</table>
	</td>
</tr>
</table>
</form>
</div>
<%
}catch(Exception e){
	System.out.println("\n00buscapyme.JSP EXCEPTION "+e);
	e.printStackTrace();
} finally{
	if(con.hayConexionAbierta()) con.cierraConexionDB();
}%>

<%!
/**
 * Obtiene la clave del proveedor de N@E. Solo contempla el producto 1 Descuento Electronico
 * @return  Cadena con la clave del afiliado o null si no existe o no esta habilitado el proveedor
 */
String getProveedor(String login, AccesoDB con) {
	System.out.println("\n getProveedor::(E) ");
	String qrySentencia = "";
	String datosPyme =  null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	Vector vecRows = new Vector();
	Vector vecCols = null;
	try {
		if(login != null && !login.equals("")) {
		
			UtilUsr utilUsr = new UtilUsr();
			Usuario usr = utilUsr.getUsuario(login);
			String claveAfiliado = usr.getClaveAfiliado();
			String tipoAfiliado = usr.getTipoAfiliado();
			
			if (tipoAfiliado != null && tipoAfiliado.equals("P") && 
					claveAfiliado != null && !claveAfiliado.equals("")) {
			
				qrySentencia = 
						" SELECT " +
						" 	distinct p.ic_pyme " + " || '|' || "+
						" 	p.cg_razon_social " + " || '|' || "+
						" 	crn.ic_nafin_electronico as datosPyme " +
						" FROM comrel_pyme_epo pe"   +
						" 	, comrel_nafin crn " +
						" 	, comcat_pyme p " +
						" WHERE p.ic_pyme = pe.ic_pyme " +
						" 	AND pe.ic_pyme = crn.ic_epo_pyme_if " +
						" 	AND crn.cg_tipo = 'P' " +
						" 	AND pe.cs_habilitado = 'S' " +
						" 	AND pe.cg_pyme_epo_interno IS NOT NULL " +
						" 	AND p.cs_habilitado = 'S' " +
						" 	AND p.ic_pyme = ? ";
				//System.out.println("\n qrySentencia: "+qrySentencia);
				ps = con.queryPrecompilado(qrySentencia);
				int cont = 0;

				cont++;ps.setInt(cont,Integer.parseInt(claveAfiliado));

				rs = ps.executeQuery();
				if (rs.next()) {
					datosPyme = rs.getString("datosPyme");
				}
				rs.close();
				ps.close();
				
			} //Tipo y clave de afiliado valido
		} //login	
	} catch(Exception e){
		System.out.println("getProveedores:: Exception "+e);
		e.printStackTrace();
	} finally {
		System.out.println("\n getProveedor::(S) ");
	}
	return datosPyme;
}
%>


<%!
	Vector getDomicilioPyme(String ic_pyme, AccesoDB con) {
	String condicion = "";
	Vector vecDomicilio = new Vector();
	try {
		String sQuery = " SELECT P.cg_rfc as rfcPyme, D.cg_calle||' '||D.cg_numero_ext||' '||D.cg_numero_int as domicilio, "+
						" D.cg_colonia as colonia, D.cn_cp as codigoPostal, D.cg_telefono1 as telefono1 "+
						" FROM comcat_pyme P, com_domicilio D "+
						" WHERE P.ic_pyme = D.ic_pyme "+
						" AND P.ic_pyme = ? ";
		PreparedStatement ps = con.queryPrecompilado(sQuery);
		ps.setString(1, ic_pyme);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			vecDomicilio.addElement((rs.getString("rfcPyme")==null?"":rs.getString("rfcPyme")));
			vecDomicilio.addElement((rs.getString("domicilio")==null?"":rs.getString("domicilio")));
			vecDomicilio.addElement((rs.getString("colonia")==null?"":rs.getString("colonia")));
			vecDomicilio.addElement((rs.getString("codigoPostal")==null?"":rs.getString("codigoPostal")));
			vecDomicilio.addElement((rs.getString("telefono1")==null?"":rs.getString("telefono1")));
		}
		rs.close();
		if(ps!=null) ps.close();
	} catch(Exception e){
		System.out.println("getDomicilioPyme:: Exception "+e);
		e.printStackTrace();
	}
	return vecDomicilio;
	}
%>

</body>
</html>