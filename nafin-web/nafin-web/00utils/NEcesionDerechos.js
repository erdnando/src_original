Ext.ns('NE.cesion');

NE.cesion.FormLoginCesion = Ext.extend(Ext.form.FormPanel,{
	initComponent : function(){
		 Ext.apply(this, {
			id: 'formLoginCesion1',
			width: 250,
			height:150,
			title: '<p align="center">Confirmaci�n de Claves</p>',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:left',  
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos(),
			buttons: this.generarBotones()
		});
		
		NE.cesion.FormLoginCesion.superclass.initComponent.call(this);
	},
	generarCampos: function(){
		return [
				  {   
					fieldLabel: 'Clave Usuario',
					name:'cesionUser',
					id: 'cesionUser1', //campo usuario
					style:'padding:4px 3px;',
					enableKeyEvents: true,
					allowBlank: false,
					maxLength : 8
				  },    
				  {   
					fieldLabel: 'Contrase�a',
					name: 'cesionPassword',
					id: 'cesionPassword1', //campo password   
					inputType: 'password', // el tipo de Input Password ( los caracteres del campo apareceran ocultos)   
					style:'padding:4px 3px;',
					maxLength : 128,
					blankText: 'Digite su contrase�a',
					allowBlank: false  
				  }   
			]
	},
	generarBotones: function(){
		return [
				{
					text: 'Aceptar',
					id: 'btnLoginAcept',
					formBind: true
				}
				/*{
					text: 'Cancelar',
					id: 'btnLoginCancel',
					iconCls: 'icoLimpiar'
				}*/
			]
	},
	setHandlerBtnAceptar: function(fn){
		var btnLoginAcept = Ext.getCmp('btnLoginAcept')
			btnLoginAcept.setHandler(fn);
	}
	/*setHandlerBtnCancelar: function(fn){
		var btnLoginCancel = Ext.getCmp('btnLoginCancel')
			btnLoginCancel.setHandler(fn);
	}*/
});

Ext.reg('form_login', NE.cesion.FormLoginCesion);


NE.cesion.FormAltaCveCesion = Ext.extend(Ext.form.FormPanel,{
	initComponent : function(){
		 Ext.apply(this, {
			id: 'formAltaCveCesion1',
			width: 250,
			height:150,
			title: 'Clave cesi�n de derechos',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:center',  
			defaultType: 'textfield',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos(),
			buttons: this.generarBotones()
		});
		
		NE.cesion.FormLoginCesion.superclass.initComponent.call(this);
	},
	generarCampos: function(){
		return [
				  {   
					xtype:'textfield',
					fieldLabel: 'Clave',
					name:'cesionCve',
					id: 'cesionCve1', //campo clave cesion
					inputType: 'password', 
					style:'padding:4px 3px;',
					allowBlank: false,
					maxLength : 8,
					minLength : 8,
					vtype:'alphanum',
					listeners:{
						blur: function(obj){
							var InString = obj.getValue();
							var f = document.form1;	
							var RefString="1234567890";
							var inc = 0;
							var Count = 0;
							var TempChar;
							var auxChar;
							if(InString != ''){
								for(Count=0;Count < InString.length;Count++) {
									TempChar = InString.substring(Count,Count+1);
									 if(RefString.indexOf(TempChar,0)==-1) 
										inc++;
								}
								if(inc == 0 || inc == 8){
									Ext.MessageBox.alert('Aviso','La clave debe contener tanto n�meros como letras. Por ejemplo nafe2009');
									obj.setValue("");
									obj.focus();
									return;
								}
								inc = 0;
								for(Count=0;Count < InString.length;Count++){
									TempChar = InString.substring(Count,Count+1);
									if(Count > 0){
										if(auxChar == TempChar){
											inc++;	
										}else{
											inc = 0;
										}
									}
									auxChar = TempChar;
									if(inc == 2){
										break;
									}
								}
								if(inc == 2){
									if(isdigit(auxChar)){
										Ext.MessageBox.alert('Aviso','La clave debe contener mas de 2 n�meros iguales consecutivos. (Ejemplo:111md5es)');
										obj.setValue("");
										obj.focus();
									}else{
										Ext.MessageBox.alert('Aviso','La clave debe contener mas de 2 letras iguales en forma consecutiva. (Ejemplo:fffmf23a)');
										obj.setValue("");
										obj.focus();
									}
									return;
								}
								/*if(InString == '<%=strLogin%>'){
									alert("<%=mensaje_param.getMensaje("P13forma1ba.UsuarioNoContenidoPassword", sesIdiomaUsuario)%>");
									f.cg_cve.value = "";
									f.cg_cve.focus();
									return;
								}*/
							}
						}
					}
				  },    
				  {   
					xtype:'textfield',
					fieldLabel: 'Confirmar Clave',
					name: 'cesionCveConfirm',
					id: 'cesionCveConfirm1', //campo clave de confirmacion
					inputType: 'password', 
					style:'padding:4px 3px;',   
					blankText: 'Digite su contrase�a',
					allowBlank: false,
					maxLength : 8,
					minLength : 8,
					vtype:'alphanum'
				  }
			]
	},
	generarBotones: function(){
		return [
				{
					text: 'Aceptar',
					id: 'btnAltaAcept',
					formBind: true
				}
			]
	},
	setHandlerBtnAceptar: function(fn){
		var btnLoginAcept = Ext.getCmp('btnAltaAcept')
			btnLoginAcept.setHandler(fn);
	}
});

Ext.reg('form_alta_cesion', NE.cesion.FormAltaCveCesion);


NE.cesion.FormClaveCesion = Ext.extend(Ext.form.FormPanel,{
	casillas: 1,
	email:'',
	tipoUsuario:'',
	initComponent : function(){
		 Ext.apply(this, {
			width: 600,
			height:300,   
			title: '<p align="center">Clave de cesi�n de derechos</p>',
			frame: true,   
			border: true,   
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:center',
			defaultType: 'textfield',
			items : this.generarCampos(),
			buttons: this.generarBotones()
		});
		
		NE.cesion.FormLoginCesion.superclass.initComponent.call(this);
	},
	generarCampos: function(){
		return [
				  {   
						xtype:'box',
						height:30,
						html: 'Para continuar con la operaci�n, es necesario que complemente los campos faltantes de su clave de confirmaci�n'
				 },
				{
					xtype: 'container',
					id: 'panelCasillas',
					//hideBorders :true,
					layout: 'hbox',
					layoutConfig: {
						 padding:'10',
						 pack:'center',
						 align:'middle'
					},
					bodyStyle: 'text-align:left',  
					height:33
				},
				{
					xtype: 'container',
					id: 'panelNumCasillas',
					//hideBorders :true,
					layout: 'hbox',
					layoutConfig: {
						 padding:'10',
						 pack:'center',
						 align:'middle'
					},
					bodyStyle: 'text-align:left',  
					height:30
				},
				{
					xtype: 'checkbox',
					name: 'sendEmail',
					id: 'sendEmail1',
					fieldLabel:'solo prueba',
					inputValue: 'S',
					hidden: true,
					hideLabel:true,
					boxLabel: 'Desea que se env�e el acuse al correo "'+this.email+'". Si el correo es incorrecto favor '+
								'de comunicarse al centro de atenci�n a clientes Cd. de M�xico 50-89-61-07 Sin costo del interior '+
								'de la rep�blica 01-800-NAFINSA (01-800-623-4672).'
								
				},
				{   
					xtype:'box',
					height:50,
					hidden: (this.tipoUsuario=='NAFIN')?true:false,
					bodyStyle: 'padding-top:20px',
					html: '�Olvido la clave de Cesi�n de derechos?. Favor de comunicarse al centro de atenci�n a clientes Cd. de M�xico '+
							'50-89-61-07 Sin costo del interior de la rep�blica 01-800-NAFINSA (01-800-623-4672)'
				},
				{   
					xtype:'box',
					height:50,
					hidden: (this.tipoUsuario=='NAFIN')?false:true,
					bodyStyle: 'padding-top:20px',
					html: 'Si olvid� la Clave de Cesi�n de Derechos, de clic en "Revocar" para que de forma autom�tica el sistema genere '+
					'dicha clave y se env�e al correo electr�nico "'+this.email+'"'
				},
				{   
					xtype:'box',
					height:50,
					hidden: (this.tipoUsuario=='NAFIN')?false:true,
					bodyStyle: 'padding-top:20px',
					html: 'Si el correo es incorrecto favor de comunicarse a centro de atenci�n a clientes Cd. de M�xico 50-89-61-07. Sin consto del interior '+
						  'de la rep�blica 01-800-NAFINSA(01-800-623-4672).'
				}
				
			]
	},
	generarBotones: function(){
		return [
				{
					text: 'Revocar',
					id: 'btnRevocar',
					hidden: (this.tipoUsuario=='NAFIN')?false:true,
					formBind:false
				},
				{
					text: 'Aceptar',
					id: 'btnClaveAcept',
					formBind:true
				}
			]
	},
	setHandlerBtnAceptar: function(fn){
		var btnLoginAcept = Ext.getCmp('btnClaveAcept')
			btnLoginAcept.setHandler(fn);
	},
	setHandlerBtnRevocar: function(fn){
		var btnClaveRevocar = Ext.getCmp('btnRevocar')
			btnClaveRevocar.setHandler(fn);
	},
	setEnvioCorreo: function(siCorreo, email){
		var chkEmail = Ext.getCmp('sendEmail1');
		if((this.tipoUsuario=='NAFIN')){
			chkEmail.hide();
		}else{
			if(siCorreo){
				chkEmail.hide();
			}else{
				chkEmail.show();
			}
		}
	},
	generaCasillas: function(numero){
		var x = 0;
		var y;
		var band = true;
		var arrayIndex = new Array();
		while(x<3){
			var randomnumber=Math.floor(Math.random()* (numero+1));
			randomnumber= randomnumber==0?1:randomnumber;
			for(var a=0;a<arrayIndex.length;a++){
				if(randomnumber == parseInt(arrayIndex[a])){
					band = false;
				}
			}
			if(band){ arrayIndex.push(randomnumber);x++;}
			band = true;
		}
		
		band = false;
		this.casillas = numero;
		var pCasillas = Ext.getCmp('panelCasillas');
		var pNumCasillas = Ext.getCmp('panelNumCasillas');
		if(pCasillas){
			var ind =1;
			for(var i=1; i <= this.casillas; i++){
				for(var c=0;c<arrayIndex.length;c++){
					if(i == parseInt(arrayIndex[c])){
						band = true;
						break;
					}
				}
				
				if(band){
					var casilla = new Ext.form.TextField({
						name:'caja'+ind,
						width:25,
						inputType: 'password',
						allowBlank:false,
						autoCreate: {tag: 'input', type: 'text', autocomplete: 'off', maxlength: '1'}
					});
					pCasillas.add(casilla);
					var cajah = new Ext.form.Hidden({
						name:'hcaja'+ind,
						value: (i-1)
					});
					pCasillas.add(cajah);
					
					var numCasilla = new Ext.form.Label({
						text: i,
						align:'center',
						width:25
					});
					pNumCasillas.add(numCasilla);
					
					ind++;
				}else{
					var casilla = new Ext.form.Label({
						text: '*',
						align:'center',
						width:25
					});
					pCasillas.add(casilla);
					
					var numCasilla = new Ext.form.Label({
						text: i,
						align:'center',
						width:25
					});
					pNumCasillas.add(numCasilla);
				}
				band = false;
			}
		}//if(pCasillas)
	}
});

Ext.reg('form_clave', NE.cesion.FormClaveCesion);


//componente para tipo usuario NAFIN
NE.cesion.FormGeneraCveCesion = Ext.extend(Ext.form.FormPanel,{
	email:null,
	initComponent : function(){
		 Ext.apply(this, {
			id: 'formGeneraCveCesion1',
			width: 610 ,
			height:150,
			title: '',
			frame: true,   
			border: true, 
			monitorValid: true,
			bodyStyle: 'padding-left:5px;,padding-top:10px;,text-align:center',  
			defaultType: 'textfield',
			buttonAlign: 'center',
			defaults: {
				msgTarget: 'side',
				anchor: '-30'
			},
			items : this.generarCampos(),
			buttons: this.generarBotones()
		});
		
		NE.cesion.FormGeneraCveCesion.superclass.initComponent.call(this);
	},
	generarCampos: function(){
		return [
				{   
					xtype:'box',
					html: '<p>Usted a�n no cuenta con la Clave Cesi�n de Derechos para la confirmaci�n de las operaciones.</p><br>'+
							'<p>De clic en la opci�n "Generar" si desea que el sistema genere una Clave de Cesi�n de Derechos '+
							'en autom�tico y la env�e al correo electr�nico "'+this.email+'".</p><br>'+
							'<p>Si el correo es incorrecto favor de comunicarse al centro de atenci�n a clientes '+
							'C. de M�xico 50-89-61-07. Sin costo del interior de la rep�blica '+
							'01-800-NAFINSA(01-800-623-4672).</p>'
				}
			]
	},
	generarBotones: function(){
		return [
				{
					text: 'Generar',
					id: 'btnGenerar',
					formBind: false
				}
			]
	},
	setHandlerBtnAceptar: function(fn){
		var btnGenerar = Ext.getCmp('btnGenerar')
			btnGenerar.setHandler(fn);
	}
});

Ext.reg('form_genera_cesion', NE.cesion.FormGeneraCveCesion);


NE.cesion.WinCesionDerechos = Ext.extend(Ext.Window,{
	esCveCorrecta:'N',
	correo: 'N',
	email: null,
	msgError: null,
	getResultValid:null,
	cvePerfProt:null,
	cveFacultad:null,
	metodo:null,
	tipoUsuario:null,
	strUsuario: null,
	initComponent : function(){
		 obj = this;
		 Ext.apply(this, {
			layout: 'form',
			autoHeight:true,
			x:300,
			y:100,
			width:260,
			resizable:false,
			modal: true,
			id:'winCesionDerechos1',
			title: ' ',
			bodyStyle: 'text-align:center',
			items: this.iniConfirmaCesion(),
			listeners:{
				beforeclose:function(win){
					win.getResultValid(win.esCveCorrecta,win.correo,win.email,win.msgError, win.tipoUsuario);
				}
			},
			buttons:[
				{
					text: 'Regresar',
					id: 'btnRegresarWin',
					hidden: true,
					handler: function(btn){
						this.close();
					}
					
				}
			]
		});
		
		NE.cesion.WinCesionDerechos.superclass.initComponent.call(this);
	},
	iniConfirmaCesion: function(){
		//obj = this;
		objFormLogCesion = new NE.cesion.FormLoginCesion();
		//objFormClaveCesion = new NE.cesion.FormClaveCesion();
		objFormAltaCveCesion = new NE.cesion.FormAltaCveCesion();
		
		if(obj.tipoUsuario!='NAFIN'){
		
			objFormLogCesion.getForm().reset();
			objFormLogCesion.setHandlerBtnAceptar(function(){
				objFormLogCesion.el.mask('Confirmando...', 'x-mask-loading');
				
				Ext.Ajax.request({
					url: '/nafin/00utils/NEcesionDerechos.jsp',
					params: Ext.apply(objFormLogCesion.getForm().getValues(),{
						informacion: 'validaClaveCesionDerec',
						cesionAltaCve: 'N',
						metodo: obj.metodo,
						sCvePerfProt: obj.cvePerfProt,
						sCveFacultad: obj.cveFacultad,
						strUsuario: obj.strUsuario
					}),
					callback: obj.procesarSuccessNEclaveCesion
				});
			});
			return [objFormLogCesion]
		}else{
			
			Ext.Ajax.request({
				url: '/nafin/00utils/NEcesionDerechos.jsp',
				params: {
					informacion: 'validaClaveCesionDerec',
					cesionAltaCve: 'N',
					metodo: obj.metodo,
					sCvePerfProt: obj.cvePerfProt,
					sCveFacultad: obj.cveFacultad,
					strUsuario: obj.strUsuario
				},
				callback: obj.procesarSuccessNEclaveCesion
			});
			return []
		}
	},
	setHandlerBtnLogAceptar: function(fn){
		objFormLogCesion.setHandlerBtnAceptar(fn);
	},
	setHandlerBtnCveAceptar: function(fn){
		objFormClaveCesion.setHandlerBtnAceptar(fn);
	},
	procesarSuccessNEclaveCesion: function(opts, success, response) {
		//var obj = this;
		if(obj.tipoUsuario!='NAFIN'){
			objFormLogCesion.el.unmask();
		}
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			obj.tipoUsuario = resp.strTipoUsuario;
			objFormClaveCesion = new NE.cesion.FormClaveCesion({email:resp.email, tipoUsuario:obj.tipoUsuario});
			if(resp.resultValidaCesion=='S'){

				var correo='N';
				if(resp.siCorreo)correo='S';
				objFormClaveCesion.setEnvioCorreo(resp.siCorreo, resp.email);
				objFormClaveCesion.generaCasillas(8);
				objFormClaveCesion.setHandlerBtnAceptar(function(){
							objFormClaveCesion.el.mask('Procesando...', 'x-mask-loading');
							Ext.Ajax.request({
								url: '/nafin/00utils/NEcesionDerechos.jsp',
								params: Ext.apply(objFormClaveCesion.getForm().getValues(),{
									informacion: 'confirmaClaveCesion',
									cesionUser: resp.cesionUser,
									//cesionPassword: resp.cesionPassword,
									capturadespues: resp. capturadespues,
									siCorreo: resp.siCorreo,
									email: resp.email,
									correo: correo,
									strUsuario: obj.strUsuario
									
								}),
								callback: obj.procesarSuccessNEconfirmaCesion
							});  
						});
				objFormClaveCesion.setHandlerBtnRevocar(function(){
							objFormClaveCesion.el.mask('Revocando...', 'x-mask-loading');
							Ext.Ajax.request({
								url: '/nafin/00utils/NEcesionDerechos.jsp',
								params: Ext.apply(objFormClaveCesion.getForm().getValues(),{
									informacion: 'validaClaveCesionDerec',
									cesionUser: resp.cesionUser,
									cesionAltaCve: 'S',
									strUsuario: obj.strUsuario
								}),
								callback: obj.procesarSuccessNEclaveCesion
							});
						});
				obj.setTitle(' ');
				obj.removeAll();
				obj.add(objFormClaveCesion);
				obj.syncSize();
				obj.setPosition(200,100);
				obj.setWidth(610);
				obj.doLayout();
			}else if(resp.resultValidaCesion=='G'){
				if(obj.tipoUsuario != 'NAFIN'){			
					objFormAltaCveCesion.setHandlerBtnAceptar(function(){
								if(objFormAltaCveCesion.findById('cesionCve1').getValue() != objFormAltaCveCesion.findById('cesionCveConfirm1').getValue()){
									Ext.MessageBox.alert('Aviso','La confirmaci�n de la clave no coincide, por favor verif�quelo');
									objFormAltaCveCesion.findById('cesionCveConfirm1').setValue('');
									objFormAltaCveCesion.findById('cesionCveConfirm1').focus();
								}else{
									Ext.Ajax.request({
										url: '/nafin/00utils/NEcesionDerechos.jsp',
										params: Ext.apply(objFormAltaCveCesion.getForm().getValues(),{
											informacion: 'validaClaveCesionDerec',
											cesionUser: resp.cesionUser,
											//cesionPassword: resp.cesionPassword,
											cesionAltaCve: 'S'
										}),
										callback: obj.procesarSuccessNEclaveCesion
									});
								}
							});
					obj.removeAll();
					
					obj.setTitle('<p align="center">Generaci�n de Clave de Confirmaci�n de Operaciones</p>')
					obj.add(new Ext.BoxComponent({ 
						html:'"Usted a�n no tiene la clave para la confirmaci�n de las operaciones de cesi�n de derechos, favor de capturarla".'
					}));
					obj.add(new Ext.BoxComponent({ 
						html:'<table class="formas" ><tr><td style="text-align:center"><b>Pol�ticas de Contrase�a.</b></td></tr>'+
								'<tr><td>'+
								'<tr><td>* Debe contener tanto n�meros como letras, y ser igual a <b>8</b> posiciones </td></tr>'+
								'<tr><td>* Se permite el uso de letras may�sculas y min�sculas. </td></tr>'+
								'<tr><td>* No se permite el uso de letras acentuadas, ni caracteres especiales como �, $, etc. </td></tr>'+
								'<tr><td>* El Usuario o parte del Usuario no debe estar contenido en el Password. </td></tr>'+
								'<tr><td>* No debe contener m�s de 2 n�meros iguales consecutivos. (ej. 12223445) </td></tr>'+
								'<tr><td>* No debe contener m�s de 2 letras iguales en forma consecutiva. (ej. bbbccd) </td></tr>'+
								'<table>'
					}));
					obj.add(objFormAltaCveCesion);
					obj.syncSize();
					obj.setPosition(200,100);
					obj.setWidth(610);
					obj.doLayout();
				}else{
					objFormGeneraCveCesion = new NE.cesion.FormGeneraCveCesion({email:resp.email});
					objFormGeneraCveCesion.setHandlerBtnAceptar(function(){
						objFormGeneraCveCesion.el.mask('Generando...', 'x-mask-loading');
						Ext.Ajax.request({
							url: '/nafin/00utils/NEcesionDerechos.jsp',
							params: Ext.apply(objFormGeneraCveCesion.getForm().getValues(),{
								informacion: 'validaClaveCesionDerec',
								cesionUser: resp.cesionUser,
								cesionAltaCve: 'S',
								strUsuario: obj.strUsuario
							}),
							callback: obj.procesarSuccessNEclaveCesion
						});
						
					});
					obj.removeAll();
					
					obj.setTitle('<p align="center">Generaci�n de Clave de Confirmaci�n de Operaciones</p>')
					obj.add(objFormGeneraCveCesion);
					obj.syncSize();
					obj.setPosition(200,100);
					obj.setWidth(610);
					obj.doLayout();
					
				}
			}else if(resp.resultValidaCesion=='R'){	
				obj.getResultValid('R','','','');
			}else {
				obj.removeAll();
				obj.add(new Ext.BoxComponent({ html:resp.msgError, width:400, bodyStyle:'text-align:"center"' }) );
				obj.syncSize();
				obj.setWidth(400);
				obj.setTitle('Error en Confirmaci�n');
				obj.doLayout();
			}//END - if(resultValidaCesion)
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	},
	procesarSuccessNEconfirmaCesion: function(opts, success, response) {
		objFormClaveCesion.el.unmask();
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			if(resp.msgError=='' && resp.esCveCorrecta){
				obj.esCveCorrecta = 'S';
				obj.msgError = resp.msgError;
				obj.email = resp.email;
				obj.correo = resp.correo;
				obj.close();
			}else{
				obj.esCveCorrecta = 'N';
				obj.msgError = resp.msgError;
				obj.removeAll();
				obj.add(new Ext.BoxComponent({ html:resp.msgError, width:400, bodyStyle:'text-align:"center"' }) );
				obj.syncSize();
				obj.setWidth(400);
				obj.setTitle('Error en Confirmaci�n');
				obj.doLayout();
			}//END - if(resultValidaCesion)
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	}
});