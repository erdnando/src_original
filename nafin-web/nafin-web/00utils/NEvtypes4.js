/**
 * rangoFecha:	Requiere que sea especificado campoFinFecha en el campo inicial y campoInicioFecha en el final
 *
 * rangoValor:	Requiere que sea especificado campoFinValor en el campo inicial y campoInicioValor en el final
 */
Ext.apply(Ext.form.VTypes, {
	rangofecha : 	function(valor, campo) {
	  var fecha = campo.parseDate(valor);
	  if(!fecha){
			return false;
	  }
	  if (campo.campoInicioFecha && (!this.dateRangeMax || (fecha.getTime() != this.dateRangeMax.getTime()))) {
			var fechaInicial = Ext.getCmp(campo.campoInicioFecha);
			fechaInicial.setMaxValue(fecha);
			this.dateRangeMax = fecha;
			fechaInicial.validate();
	  }else if (campo.campoFinFecha && (!this.dateRangeMin || (fecha.getTime() != this.dateRangeMin.getTime()))) {
			var fechaFinal = Ext.getCmp(campo.campoFinFecha);
			fechaFinal.setMinValue(fecha);
			this.dateRangeMin = fecha;
			fechaFinal.validate();
	  }
	  return true;
	},
	rangofechaText: 'Error en la fecha: formato dd/mm/aaaa',

	rangofecha2 : 	function(valor, campo) {
	  var fecha = campo.parseDate(valor);
	  if(!fecha){
			return false;
	  }
	  if (campo.campoInicioFecha && (!this.dateRangeMax2 || (fecha.getTime() != this.dateRangeMax2.getTime()))) {
			var fechaInicial = Ext.getCmp(campo.campoInicioFecha);
			fechaInicial.setMaxValue(fecha);
			this.dateRangeMax2 = fecha;
			fechaInicial.validate();
	  }else if (campo.campoFinFecha && (!this.dateRangeMin2 || (fecha.getTime() != this.dateRangeMin2.getTime()))) {
			var fechaFinal = Ext.getCmp(campo.campoFinFecha);
			fechaFinal.setMinValue(fecha);
			this.dateRangeMin2 = fecha;
			fechaFinal.validate();
	  }
	  return true;
	},
	rangofecha2Text: 'Error en la fecha: formato dd/mm/aaaa',
	
	rangoValor: function(val, field) {
		/* 
		* Funcion basada en un ejemplo de Desarrollosweb.net (	Cookbook ExtJS: Creando nuestros propios validadores-11/12/2008 por Javier Caride	)
		*/
		var valor = parseFloat(val);
		
		if (field.campoInicioValor){
			var inicioMonto = Ext.getCmp(field.campoInicioValor);
			var inicioMontoValue = parseFloat(inicioMonto.getValue());
			
			if (valor < inicioMontoValue){
				return false;
			} else {
				field.clearInvalid();
				inicioMonto.clearInvalid();
				return true;
			}
			
		}else if (field.campoFinValor) {
			var finMonto = Ext.getCmp(field.campoFinValor);
			var finMontoValue = parseFloat(finMonto.getValue());

				if (valor > finMontoValue){
					return false;
				} else {
					field.clearInvalid();
					finMonto.clearInvalid();
					return true;
				}
		}
		return true;
	},
	rangoValorText:	'El valor final debe ser mayor al valor inicial',

   emaillist: function(v) { 
   	
   	var valid = true; 
   	if(  Ext.isEmpty(v) ){
   		return valid;
   	}
   	
   	var array = v.replace(/;/g,',').split(',');
   	Ext.each(
   		array, 
   		function(value) { 
   			if (!this.email(value)) { 
   				valid = false; 
   				return false; 
   			}; 
   		}, 
   		this
   	); 
      return valid;
      
   }, 
   emaillistText: 'Este campo debe ser una direcci�n de correo o una lista de estas: usuario1@ejemplo.com,usuario2@ejemplo.com', 
   emaillistMask: /[a-z0-9_\.\-@\,;]/i ,

   archivopdf: function(v) {
		var    myRegex = /^.+\.([pP][dD][fF])$/;
		return myRegex.test(v);
	},
	archivopdfText: 'El formato del archivo no es v�lido. Formato Soportado: .pdf',
	
	archivodocx: function(v) {
		var    myRegex = /^.+\.([dD][oO][cC][xX])$/;
		return myRegex.test(v);
	},
	archivodocxText: 'El formato del archivo no es v�lido. Formato Soportado: .docx',

	archivosexcel: function(v) {
		var    myRegex = /^.+\.([xX][lL][sS])$/;
		var    myRegex1 = /^.+\.([xX][lL][sS][xX])$/;
		if(myRegex.test(v) || myRegex1.test(v)){
			return true;
		} else{
			return false;
		}
	},
	archivosexcelText: 'Favor de cargar un archivo con formato XLS o XLSX.',
	
	archivoZip: function(v) {
		var    myRegex = /^.+\.([zZ][iI][pP])$/;
		if(myRegex.test(v)){
			return true;
		} else{
			return false;
		}
	},
	archivoZipText: 'El formato del archivo no es v�lido. <br> Formatos soportados: ZIP.',
	
	grupoArchivos: function(v) {
		var myZip	= /^.+\.([zZ][iI][pP])$/;
		var myXls	= /^.+\.([xX][lL][sS])$/;
		var myXlsx	= /^.+\.([xX][lL][sS][xX])$/;
		var myDoc	= /^.+\.([dD][oO][cC])$/;
		var myDocx	= /^.+\.([dD][oO][cC][xX])$/;
		var myPdf	= /^.+\.([pP][dD][fF])$/;
		
		if( myZip.test(v) || myXls.test(v) || myXlsx.test(v)|| myDoc.test(v)|| myDocx.test(v)|| myPdf.test(v) ){
			return true;
		} else{
			return false;
		}
	},
	grupoArchivosText: 'El formato del archivo no es v�lido. <br> Formatos soportados: ZIP, PDF, DOC, DOCX, XLS, XLSX.',

	login: function(v){
		return /^[0-9]+$/.test(v);
	},
	
	loginText: 'S�lo se aceptan valores num�ricos'
	
});