Ext.namespace('NE','NE.util');

NE.util.colorCampoEdit = function (value, cell, row, rowIndex, colIndex, store){
  cell.attr = 'style="border: thin solid #3399CC;"';
  return value;
}


NE.util.mostrarErrorResponse = function(response, mensajeErrorPersonalizado, fn) {
	if (response.status == 200) {
		jsonObj = Ext.util.JSON.decode(response.responseText);
		var mensaje = jsonObj.msg;
		var stackTrace = jsonObj.stackTrace;
		var causeStackTrace = jsonObj.causeStackTrace;

		mensaje = (!mensaje)?'':Ext.util.Format.nl2br(mensaje);
		stackTrace = (!stackTrace)?'':Ext.util.Format.nl2br(stackTrace);
		causeStackTrace = (!causeStackTrace)?'':Ext.util.Format.nl2br(causeStackTrace);

		var windowUnexpectedError = new Ext.Window({
			id: 'windowUnexpectedError',
			title: 'Detalle del Error',
			autoScroll: true,
			width: 600,
			height: 300,
			y: 0,
			closeAction: 'hide',
			html: mensaje + '<hr>' + stackTrace + '<hr>' + causeStackTrace
		});

		Ext.MessageBox.alert('Error',
		"Error en los datos obtenidos: " + mensaje + "<a onclick=\"Ext.getCmp('windowUnexpectedError').show()\">...</a>",
			function(){
				Ext.getCmp('windowUnexpectedError').destroy();
				if(!Ext.isEmpty(fn)){
					fn();
				}
			}
		);

	} else {
		var mensajeError =  mensajeErrorPersonalizado + "<br>" +
				"Error en la peticion. Codigo: " + response.status;
				Ext.MessageBox.alert('Error', mensajeError, fn);
	}
}



/**
 * El metodo permite mostrar de una manera consistente los errores que se den
 * cuando se este teniendo acceso a un Store.
 *
 */
NE.util.mostrarDataProxyError = function (proxy, type, action, optionsRequest, response, args, fn) {
	if (type == 'response') {
		NE.util.mostrarErrorResponse(response, "URLx1:" + proxy.url , fn );
	} else if (type == 'remote') {
		Ext.MessageBox.alert('Error',"Error al cargar datos.  URLd2:" + proxy.url, fn );
	}
}

/**
 * Metodo para mostrar los errores ocurridos al realizar una petici�n de tipo
 * Ajax.  Ya sean errores de conexion o de obtenci�n de la informaci�n
 */
NE.util.mostrarConnError = function (response, opts, fn) {
	NE.util.mostrarErrorResponse(response, "", fn);
}

/**
 * El metodo permite mostrar de una manera consistente los errores que se den
 * cuando se este teniendo acceso a un TreeLoader.
 *
 */
NE.util.mostrarTreeLoaderError = function (treeloader, node, response) {
	NE.util.mostrarErrorResponse(response, "");
}



/**
 * Metodo para mostrar los errores ocurridos al realizar una petici�n de tipo
 * Submit.  Esta pensado en submits que realicen upload de archivos
 * en cuyo caso tienen solo ciertas propiedades por que la peticion no es AJAX
 */
NE.util.mostrarSubmitError = function (form, action) {
	var jsonObj = action.result;
	var mensaje = jsonObj.msg;
	var stackTrace = jsonObj.stackTrace;
	var causeStackTrace = jsonObj.causeStackTrace;

	mensaje = (!mensaje)?'':Ext.util.Format.nl2br(mensaje);
	stackTrace = (!stackTrace)?'':Ext.util.Format.nl2br(stackTrace);
	causeStackTrace = (!causeStackTrace)?'':Ext.util.Format.nl2br(causeStackTrace);

	var windowUnexpectedError = new Ext.Window({
		id: 'windowUnexpectedError',
		title: 'Detalle del Error',
		width: 600,
		autoScroll: true,
		height: 300,
		y: 0,
		closeAction: 'hide',
		html: mensaje + '<hr>' + stackTrace + '<hr>' + causeStackTrace
	});

	Ext.MessageBox.alert('Error',
			"Error en los datos obtenidos: " + mensaje + "<a onclick=\"Ext.getCmp('windowUnexpectedError').show()\">...</a>",
			function(){
				Ext.getCmp('windowUnexpectedError').destroy();
			}
	);
}
/**
 * Regresa true si el valor es S o s de lo contrario false.
 */
NE.util.string2boolean = function (valor, registro){
	return valor == 'S' || valor == 's';
}

/**
 * Regresa null si el valor es 0
 */
NE.util.zero2null = function (valor, registro) {
	return (valor == 0)?null:valor;
}

/**
 * Muestra el mensaje de sesi�n expirada y redirecciona a la salida
 */
NE.util.mostrarErrorSesion = function() {

	Ext.MessageBox.show({
		title: 'Error en la sesi�n del usuario',
		msg: 'La sesi�n ha expirado.<br><br>Es necesario volver a ingresar a la aplicaci�n',
		buttons: Ext.MessageBox.OK,
		fn: function(){
			window.location = NE.appWebContextRoot + '/15cadenas/15salir.jsp';
		},
		icon: Ext.MessageBox.ERROR
	});

	/*Ext.MessageBox.alert('Error en la sesi�n del usuario',
		"La sesi�n ha expirado.<br><br>Es necesario volver a ingresar a la aplicaci�n",
		function(){
			window.location = NE.appWebContextRoot + '/15cadenas/15salir.jsp';
		}
	);*/
}


/**
 * Metodo para firmar digitalmente.
 * El metodo regresa la cadena firmada en formato PKCS7 o en caso de error
 * en el proceso de firma regresa un null
 *
 */
NE.util.firmar = function (textoFirmar) {
	var pkcs7_ = null;
	var ie10 = (navigator.userAgent.indexOf("MSIE 10") != -1)?true:false;
	var ie11 = (navigator.userAgent.indexOf("Trident") != -1 && navigator.userAgent.indexOf("rv:11") != -1)?true:false;
	if (Ext.isIE && !ie10 && !ie11) {
		//SeguriSIGN es un activex que debe ser cargado antes de ser invocado
		SeguriSIGN.Who = document.domain;
		pkcs7_ = SeguriSIGN.Firma ( textoFirmar );
		if (SeguriSIGN.status != 2000 && SeguriSIGN.status != 0) {
				alert("Ocurri� un error durante el Proceso de Firma: " +("" + SeguriSIGN.status));
				pkcs7_ = null;
		}
	} else if (Ext.isChrome || ie10 || ie11) {
		var plugin = document.getElementById("pluginFirma");
		var error = false;
		var crypto;
		try {
			crypto = plugin.NewCryptoCAPI();
		} catch(e) {
			Ext.MessageBox.alert('Error Firma Digital','Verifique que tenga instalado el plugin de firma digital');
			pkcs7_ = null;
			error = true;
		}

		/*var certlist = crypto.getLocalStore();
		var cont = certlist.Count();
		var arrCert = [];
		for (var i = 0; i < cont; i++) {
			try{
				var cert = certlist.getCert(i);
				var usernames = cert.getSubjectNames;
				var issuernames = cert.getIssuerNames;
				var serie = cert.getSerial;

				//alert(usernames[cert.DN_CN] + "'s " + issuernames[cert.DN_CN]);
				//alert(serie);

				arrCert.push([usernames[cert.DN_CN], issuernames[cert.DN_CN], serie]);
			}catch(e){
				alert(e)
			}
		}
		alert(arrCert);

		var VERSION = "1.4.0.1";
		if(plugin.version < VERSION) {
			alert("An old version of SgDataCrypto Plugin is installed. Please install version " + VERSION + " or greater");
			return;
		}
		*/

		if (!error && confirm("\u00BFRealizar la firma digital de la informaci\u00F3n?")) {

			window.open("","_ventanaTextoFirma");
			//Si esl txtArea no existe, se crea solo una vez.
			if(!document.getElementById('_txtFirmar')){
				var txtArea = document.createElement("textarea");
				txtArea.setAttribute('style','display:none');
				txtArea.setAttribute('name','_txtFirmar');
				txtArea.setAttribute('id','_txtFirmar');
				document.formAux.appendChild(txtArea);
			}
			document.formAux._txtFirmar.value = textoFirmar;
			document.formAux.target = '_ventanaTextoFirma';
			document.formAux.action = NE.appWebContextRoot+"/00utils/textoFirmar.jsp";
			document.formAux.method = 'post';
			document.formAux.submit();

			var inputType = {DATA:1, FILE:2, HASH:3, BASE64DATA:4}
			var signatureAlgs = {SHA1WithRSA:0, SHA256WithRSA:2, SHA384WithRSA:3, SHA512WithRSA:4}
			try {
				
				
				var crypto=plugin.NewCryptoCAPI();
				var certlist = crypto.getLocalStore();
				var serie = _serial;  //_serial esta declarada en componente_firma.jspf y el valor viene de strSerial de sesi�n.

				var cert = (serie=="0")?certlist.getCert(0):certlist.find_by_Serial(serie);
				crypto.setUserCert( cert );
				crypto.setSignatureAlgorithm(signatureAlgs.SHA1WithRSA);
				crypto.setDoDetached(false);

				pkcs7_ =  crypto.signMessage(textoFirmar, inputType.DATA);				
				
			} catch(e) {
				//Puede ser que el error se deba a que el certificado con numero de serie como esta en sesi�n, no est� instalado en el navegador.
				Ext.MessageBox.alert('Error Firma Digital',
					"Verifique que tenga instalado el certificado con numero de serie " + _serial + " e intente nuevamente<br><br>Detalle error: " + e);
				pkcs7_ = null;
				error= true;
			}

		}

	} else if (Ext.isGecko) {
		pkcs7_ = crypto.signText(textoFirmar, "ask");
		if (pkcs7_ == "error:userCancel" || pkcs7_ == "error:internalError") {
			if (pkcs7_ == "error:internalError") {
				Ext.MessageBox.alert('Error Firma Digital','Verifique que la AC que emiti� el certificado est� instalada o habilitada');
			} else if (pkcs7_ == "error:userCancel") {
				Ext.MessageBox.alert('Error Firma Digital','El proceso de firma fue cancelado por el usuario');
			}
			pkcs7_ = null;
			error= true;
		}
	}
	return pkcs7_;
}

/**
 * Obtiene pkcs7. Ocupa distintos mecanismos dependiendo del navegador
 * @param textoFirmar texto afirmar
 * @param fnCallback Funcion que procesa el pkcs7. Esta funci�n
 * puede tener m�s par�metros, los cuales se pasan como parametros
 */
NE.util.obtenerPKCS7 = function(fnCallback, textoFirmar) {

	var pkcs7 = null;
	var copiaParametros = [].slice.call(arguments);
	
	if ((navigator.userAgent.indexOf("Trident") != -1 && navigator.userAgent.indexOf("rv:11") != -1)) { //IE11
		pkcs7 = NE.util.firmar(textoFirmar);
		if (Ext.isEmpty(pkcs7)) {
			return;
		} else {
			copiaParametros.splice(0, 1, pkcs7);
			fnCallback.apply(null, copiaParametros);
		}
	} else {
		var ventana = Ext.getCmp('winFirmaDigital1');
		
		if(ventana){
			ventana.show();
		}else{
			ventana = new NE.firma.WinFirmaDigital({
				copiaParametros:copiaParametros,
				textoFirmar: textoFirmar,
				fnCallback: fnCallback
			}).show();
		}				
	}
}


NE.util.initMensajeCargaCombo = function(store, options) {
	store.loadData({
		"total" : 0,
		"registros" : [{"loadMsg":"Cargando informaci�n..."}]
	});
}


NE.util.getTemplateMensajeCargaCombo = function(displayTemplate,mostrarDescripcionCompleta) {
	var template =
			'<tpl for=".">' +
			'<tpl if="!Ext.isEmpty(loadMsg)">'+
			'<div class="loading-indicator">{loadMsg}</div>'+
			'</tpl>'+
			'<tpl if="Ext.isEmpty(loadMsg)">'+
			'<div class="x-combo-list-item">' +
			'<div '+(mostrarDescripcionCompleta === true?' style="white-space: normal; word-wrap:break-word; " ':'')+'>' + displayTemplate + '</div>' +
			'</div></tpl></tpl>';
	return template
}

NE.util.templateMensajeCargaCombo = NE.util.getTemplateMensajeCargaCombo('{descripcion}');
NE.util.templateMensajeCargaComboConDescripcionCompleta = NE.util.getTemplateMensajeCargaCombo('{descripcion}',true);

NE.util.getEspaciador = function(alto,ancho) {
	return {
		xtype: 'box',
		height: alto,
		width: ((ancho)?ancho:800)
	}
}

NE.util.horaServidor = new Date(); 	//El valor se debe sobrescribir con la hora del servidor antes de llamar NE.util.startReloj

NE.util.taskReloj = {
	run: function(){
		NE.util.horaServidor = new Date(NE.util.horaServidor.getTime() + 1000 * 1); //Se le agrega un segundo
		Ext.get('_reloj').update(NE.util.horaServidor.format('H:i:s'));
	},
	interval: 1000 //1 second
}

NE.util.startReloj = function() {
	var runner = new Ext.util.TaskRunner();
	runner.start(NE.util.taskReloj);
}


/**
 * La funci�n permite ignorar la tecla backspace. La funci�n est� pensada
 * para usarse como listener del evento "keydown" de un campo marcado como
 * readonly, ya que de lo contrario, lo que hace es un history.back() que causa
 * confusi�n.
 */
NE.util.ignorarBackspace = function(campo, evento, eOpts) {
	if (evento.getKey() == evento.BACKSPACE) {
		evento.stopEvent();
	}
}

/**
 * Clone Function
 * @param {Object/Array} o Object or array to clone
 * @return {Object/Array} Deep clone of an object or an array
 */
NE.util.clone = function(o) {
    if(!o || 'object' !== typeof o) {
        return o;
    }
    if('function' === typeof o.clone) {
        return o.clone();
    }
    var c = '[object Array]' === Object.prototype.toString.call(o) ? [] : {};
    var p, v;
    for(p in o) {
        if(o.hasOwnProperty(p)) {
            v = o[p];
            if(v && 'object' === typeof v) {
                c[p] = NE.util.clone(v);
            }
            else {
                c[p] = v;
            }
        }
    }
    return c;
}; // eo function clone

//Recibe un objeto con puras propiedades de tipo booleano
//y regresa true si todos los valores del objeto son true,
//false de lo contrario.
NE.util.allTrue = function(obj) {
	var allTrue = false;
	try {
		for (var name in obj) {
			if (obj.hasOwnProperty(name)) {
				var valor = obj[name];
				if (valor == false) {
					allTrue = false;
					break;
				} else {
					allTrue = true;
				}
			}
		}
	} catch(err) {
		alert("Error al leer las propiedades del objeto: " + err);
		allTrue = false;
	}
	//alert(allTrue);
	return allTrue;
}
//Metodo para revisar que ambas fechas son obligatorias
//recibe como parametros las 2 fechas a comparar y opcional una bandera por default true
//La bandera indica si la captura de las fechas es obligatoria
//Si la fecha no es obligatoria valida que esten las 2 fechas o ninguna de las fechas.
//Automaticamente coloca un markinvalid en los campos si hay algun error.
//retorna resultado un boolean (true) si la validacion fue correcta y un (false) si la validacion es incorrecta.
/*
Ejemplo
if(NE.util.validatorDate(Ext.getCmp('fechaOper1'),Ext.getCmp('fechaOper2'),true)){
	//Si las fechas estan llenas se ejecuta el then
}
else{
	//Se marcan las fechas con markInvalid y se ejecuta el else
}
*/

NE.util.validatorDate = function(fecha1,fecha2,requerida){
if(requerida==undefined||requerida==null) requerida=true;
	if(requerida){
			if(fecha1.getValue()==''&&fecha2.getValue()==''){
				fecha1.markInvalid('La fecha es obligatoria');
				fecha2.markInvalid('La fecha es obligatoria');
				fecha1.focus();
				return false;
			}
			if(fecha1.getValue()==''){
				fecha1.markInvalid('La fecha es obligatoria');
				fecha1.focus();
				return false;
			}
			if(fecha2.getValue()==''){
				fecha2.markInvalid('La fecha es obligatoria');
				fecha2.focus();
				return false;
			}
	}else{
		if (fecha1.getValue()!=''||fecha2.getValue()!=''){
			if(fecha1.getValue()==''){
					fecha1.markInvalid('La fecha es obligatoria');
					fecha1.focus();
					return false;
				}
				if(fecha2.getValue()==''){
					fecha2.markInvalid('La fecha es obligatoria');
					fecha2.focus();
					return false;
				}
		}
	}
	return true;
}

/*Metodo para validar un rango de fechas
recibe como parametros las 2 fechas a comparar, opcional los d�as del rango de fechas y opcional una bandera por si las fechas son requeridas
Si no se indica el rango de fechas por default es 30, y si no se indica la bandera por default es true
Si la bandera es false solo revisa que ambas fechas esten llenas si solo una fecha se lleno retorna false y coloca un markInvalid En la fecha vacia, si las 2 estan vacias retorna true y no realiza la validacion
Si el rango de fechas no se cumple retorna un false y ademas manda una alerta ('El rango de fechas no debe exeder de n d�as') y retorna false
Si la bandera es true revisa que las 2 fechas esten llenas, de lo contrario retorna false y a las fechas les agrega un markinvalid
Si las 2 fechas estan llenas realiza la validacion de los dias
Si las fechas cumplen con el rango retorna true
Ejemplo
if(NE.util.validatorDateRange(Ext.getCmp('fechaOper1'),Ext.getCmp('fechaOper2'),30,true)){
	//Si las fechas estan llenas y cumplen con el rango de 30 d�as se ejecuta el then
}
else{
	//Se marcan las fechas con markInvalid y se ejecuta el else
}
*/
NE.util.validatorDateRange = function(fecha1,fecha2,dias,requerida){
	if (dias==undefined||dias==null)dias=30;
	if (requerida==undefined||requerida==null) requerida = true;
	if(requerida == true){
		if(!NE.util.validatorDate (fecha1,fecha2,true)){
			return false;
		}else{
			if(fecha1.getValue().add(Date.DAY,dias)>fecha2.getValue())
			{
				return true;
			}
			else{
				Ext.Msg.alert('Mensaje de error','El rango de fechas no debe exceder de '+dias+' d�as');
				return false;
			}
		}
	}else{
		var retorno=NE.util.validatorDate (fecha1,fecha2,false);
		if(retorno){
			if(fecha1.getValue().add(Date.DAY,dias)>fecha2.getValue())
			{
				return true;
			}
			else{
				Ext.Msg.alert('Mensaje de error','El rango de fechas no debe exceder de '+dias+' d�as');
				return false;
			}
		}else{
			return retorno;
		}

	}
}

// Metodo para codificar los caracteres especiales de HTML y
// que podrian causar ruido a la hora de enviarlos como json strings
NE.util.htmlEncode = function (str) {

	var htmlString = str;
	if (str == undefined || str == null ) return str;
	htmlString = htmlString.replace(/&/g,"&amp;"  );
	htmlString = htmlString.replace(/"/g,"&quot;" );
	htmlString = htmlString.replace(/</g,"&lt;" );
	htmlString = htmlString.replace(/>/g,"&gt;" );

	return htmlString;

}

// Metodo para decodificar espacios en HTML
NE.util.decodeHTMLWhiteSpace = function (str) {

	var decodedString = str;
	if (str == undefined || str == null ) return str;
	decodedString = decodedString.replace(/&nbsp;/g," ");

	return decodedString;

}

NE.util.UpperCaseTextField = new function() {

	// Este plugin solo es valido para campos de tipo: textfield
	function getUppercaseValue() {
      return this.constructor.prototype.getValue.apply(this, arguments).toUpperCase();
   }

   function onFieldRender() {
      var me = this;
      me.constructor.prototype.onRender.apply(this, arguments);
      me.mon(me.el.applyStyles({ textTransform: 'uppercase' }), 'blur', onFieldBlur, me);
   }

   function onFieldBlur() {
      this.setValue(getUppercaseValue.call(this));
   }

   this.init = function(field) {
      field.onRender = onFieldRender;
      field.getValue = getUppercaseValue;
   };

};
Ext.preg('uppercasetf', NE.util.UpperCaseTextField );


/**
 * La funcion es para su uso en GridPanels, para colocar el tootltip a las celdas
 * con la informaci�n de dicha celda. Para casos en los que el contenido es muy
 * largo y no se visualiza completo en la columna
 */
NE.util.addGridTooltip = function(value, metadata, record, rowIndex, colIndex, store){
	metadata.attr = 'ext:qtip="' + value + '"';
	return value;
}

//------------------------------------- Especificos EXTJS 4 ----------------------------------

NE.util.mostrarErrorResponse4 = function(response, mensajeErrorPersonalizado, fn) {
	if (response.status == 200) {
		jsonObj = Ext.JSON.decode(response.responseText);
		var mensaje = jsonObj.msg;
		var stackTrace = jsonObj.stackTrace;
		var causeStackTrace = jsonObj.causeStackTrace;

		mensaje = (!mensaje)?'':Ext.util.Format.nl2br(mensaje);
		stackTrace = (!stackTrace)?'':Ext.util.Format.nl2br(stackTrace);
		causeStackTrace = (!causeStackTrace)?'':Ext.util.Format.nl2br(causeStackTrace);

		var windowUnexpectedError = new Ext.Window({
			id: 'windowUnexpectedError',
			title: 'Detalle del Error',
			autoScroll: true,
			width: 600,
			height: 300,
			y: 0,
			closeAction: 'hide',
			html: mensaje + '<hr>' + stackTrace + '<hr>' + causeStackTrace
		});

		Ext.MessageBox.alert('Error',
		"Error en los datos obtenidos: " + mensaje + "<a onclick=\"Ext.getCmp('windowUnexpectedError').show()\">...</a>",
			function(){
				Ext.getCmp('windowUnexpectedError').destroy();
				if(!Ext.isEmpty(fn)){
					fn();
				}
			}
		);

	} else {
		var mensajeError =  mensajeErrorPersonalizado + "<br>" +
				"Error en la peticion. Codigo: " + response.status;
				Ext.MessageBox.alert('Error', mensajeError, fn);
	}
};


/**
 * El metodo permite mostrar de una manera consistente los errores que se den
 * en las peticiones via proxy Ajax.
 *
 */
NE.util.mostrarProxyAjaxError = function (proxy, response, operation, eOpts) {
	NE.util.mostrarErrorResponse4(response, "");
}

/**
 * Metodo para mostrar los errores ocurridos al realizar una petici�n de tipo
 * Ajax.  Ya sean errores de conexion o de obtenci�n de la informaci�n
 */
NE.util.mostrarErrorPeticion = function (response) {
	NE.util.mostrarErrorResponse4(response, "");
}

/**
 * La funcion es para su uso en GridPanels, para colocar el tootltip a las celdas
 * con la informaci�n de dicha celda. Para casos en los que el contenido es muy
 * largo y no se visualiza completo en la columna
 */
NE.util.addGridTooltip4 = function(value, metadata, record, rowIndex, colIndex, store){
	metadata.tdAttr = 'data-qtip="' + value + '"';
	return value;
}

NE.util.taskReloj4 = {
	run: function(){
		NE.util.horaServidor = new Date(NE.util.horaServidor.getTime() + 1000 * 1); //Se le agrega un segundo
		Ext.get('_reloj').update(Ext.Date.format(NE.util.horaServidor, 'H:i:s'));
	},
	interval: 1000 //1 second
}
NE.util.startReloj4 = function() {
	var runner = new Ext.util.TaskRunner();
	runner.start(NE.util.taskReloj4);
}

//--------------------------------- FIN Especificos EXTJS 4 ----------------------------------


