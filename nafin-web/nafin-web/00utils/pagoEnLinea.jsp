<%@ page import="
	java.util.Calendar,
	java.util.GregorianCalendar,
	java.security.NoSuchAlgorithmException,
	java.security.InvalidKeyException,
	javax.crypto.Mac,
	javax.crypto.spec.SecretKeySpec,
	java.io.UnsupportedEncodingException,
  java.io.IOException,
  java.util.Enumeration,
  java.util.Properties,
  java.util.ResourceBundle,
  netropology.utilerias.ServicioNetPay,
	java.net.URLEncoder"
%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ include file="/appComun.jspf" %>

<%


ServicioNetPay servicioNetPay = new ServicioNetPay();
String montoTotTC = request.getParameter("montoTotTC")==null?"0":request.getParameter("montoTotTC");
String totalReg = request.getParameter("totalReg")==null?"0":request.getParameter("totalReg");
String strNombreUsuario = request.getParameter("strNombreUsuario")==null?"":request.getParameter("strNombreUsuario");
String StoreID = request.getParameter("StoreID")==null?"":request.getParameter("StoreID");

String urlServicioNetPay = servicioNetPay.getUrlNetPay();
String SID = servicioNetPay.getSID();
String numOrden = servicioNetPay.getNumOrdenTrans();
String fechaTrans = servicioNetPay.getFechaTrans();
String horaTrans = servicioNetPay.getHoraTrans();
String resultSha1 = servicioNetPay.getCodificaSha1(montoTotTC, "adm0n2");

System.out.println("numOrden === " +numOrden);
System.out.println("resultSha1 == "+resultSha1);
%>

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title>untitled</title>
	 <%@ include file="/extjs.jspf"%>
	<script type="text/javascript" src="/nafin/00utils/pagoEnLinea.js?<%=session.getId()%>"></script>
  </head>
  <body>
		<div id="areaContenido"></div>
		<form method="POST" name="sForm">
			<input type="hidden" name="MerchantResponseURL" id="MerchantResponseURL" value="<%=servicioNetPay.getUrlCodificadaB64()%>"/>
			<input type="hidden" name="SID" id="SID" value="<%=SID%>"/>
			<input type="hidden" name="REQ" id="REQ" value="<%=resultSha1%>"/>
			<input type="hidden" id="hidStrUsuario" value="<%=strNombreUsuario%>">
			<input type="hidden" id="fechaTrans" value="<%=fechaTrans%>">
			<input type="hidden" id="horaTrans" value="<%=horaTrans%>">
			<input type="hidden" name="OrderNumber" id="OrderNumber" value="<%=numOrden%>"/>
			<input type="hidden" name="OrderDescription" id="OrderDescription"  value="<%=numOrden%>"/>
			<input type="hidden" name="Amount" id="Amount" value="<%=montoTotTC%>"/>
			<input type="hidden" name="totalReg" id="totalReg" value="<%=totalReg%>"/>
      <input type="hidden" name="urlServicioNetPay" id="urlServicioNetPay" value="<%=urlServicioNetPay%>"/>
      <input type="hidden" name="StoreID" id="StoreID" value="<%=StoreID%>"/>
		</form>
  </body>
</html>
