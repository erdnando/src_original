<%@ page import="java.io.*, java.util.*, java.util.zip.*"%>
<%
int maxNumBytes = 10000000;
int maxNumBytesApp = 5000000;
String logFile = application.getRealPath("/")+"/../../../../../logs/cadenas.log";
String logApp = application.getRealPath("/")+"/../../../../../logs/cadenas.out";
File f = new File(logFile);
File fApp = new File(logApp);
FileOutputStream fos = new FileOutputStream(application.getRealPath("/") + "/00tmp/log.zip");
ZipOutputStream zos = new ZipOutputStream(fos);

if (f.length() > 0) {
	ZipEntry ze = new ZipEntry("cadenas.log");
	zos.putNextEntry(ze);

	RandomAccessFile raf = new RandomAccessFile(f, "r");
	long posicion = f.length() - maxNumBytes;	//Ultimos n bytes del archivo
	posicion = (posicion < 0)?0:posicion;
	raf.seek(posicion);
	int bytesLeidos = 0;
	long bytesTotalesLeidos = 0;
	byte[] buffer = new byte[102400];	//100Kb
	while ( (bytesLeidos = raf.read(buffer)) != -1 ) {
		bytesTotalesLeidos += bytesLeidos;
		if (bytesTotalesLeidos <= maxNumBytes) {  //por si las dudas, no se que pase si el archivo que se lee sigue creciendo
			zos.write(buffer, 0, bytesLeidos);
		}
	}
	zos.closeEntry();
	raf.close();
}
if (fApp.length() > 0) {
	ZipEntry ze = new ZipEntry("cadenas.out");
	zos.putNextEntry(ze);

	RandomAccessFile raf = new RandomAccessFile(fApp, "r");
	long posicion = fApp.length() - maxNumBytesApp;	//Ultimos n bytes del archivo
	posicion = (posicion < 0)?0:posicion;
	raf.seek(posicion);
	int bytesLeidos = 0;
	long bytesTotalesLeidos = 0;
	byte[] buffer = new byte[10240];	//10Kb
	while ( (bytesLeidos = raf.read(buffer)) != -1 ) {
		bytesTotalesLeidos += bytesLeidos;
		if (bytesTotalesLeidos <= maxNumBytes) {  //por si las dudas, no se que pase si el archivo que se lee sigue creciendo
			zos.write(buffer, 0, bytesLeidos);
		}
	}
	zos.closeEntry();
	raf.close();
}
zos.flush();
zos.close();
%>

<html>
<head>
<title>Log</title>
</head>
<body>
Log from <%=logFile%><br>
App Log from <%=logApp%><br>
00tmp/log.zip
</body>
</html>
