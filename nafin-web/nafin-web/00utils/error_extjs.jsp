<%@ page contentType="application/json;charset=UTF-8"
	import="java.util.*, java.text.SimpleDateFormat, net.sf.json.*" 
	isErrorPage="true"%>
<%

String myContentType = (String) request.getAttribute("myContentType");
if(myContentType != null && !myContentType.trim().equals("")){
	response.setContentType(myContentType);    
}
	
SimpleDateFormat formatoHora = new SimpleDateFormat ("yyyyMMdd_HHmmss");
String fechaError = formatoHora.format(new java.util.Date());

System.out.println(fechaError + ": ERROR INESPERADO. " + exception.getMessage());
exception.printStackTrace();

JSONObject jsonObj = new JSONObject();
jsonObj.put("success", new Boolean(false));
jsonObj.put("msg", "Error Inesperado " + fechaError + ".\n" + exception.getMessage());

java.io.StringWriter outSW = new java.io.StringWriter();
exception.printStackTrace(new java.io.PrintWriter(outSW));
String stackTrace = outSW.toString();
 
jsonObj.put("stackTrace", stackTrace);

String causeStackTrace = "";
if (exception.getCause() != null) {
	outSW = new java.io.StringWriter();
	exception.getCause().printStackTrace(new java.io.PrintWriter(outSW));
	causeStackTrace = outSW.toString();
}
jsonObj.put("causeStackTrace", causeStackTrace);
%>
<%=jsonObj%>