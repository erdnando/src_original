<%@	page import = "com.nafin.security.UserNE,
		com.nafin.security.UserSignOnNE,
		java.io.BufferedInputStream, 
		java.io.BufferedOutputStream,
		java.io.BufferedWriter,
		java.io.File,
		java.io.FilenameFilter,
		java.io.FileOutputStream,
		java.io.FileWriter,
		java.text.DecimalFormat,
		java.text.NumberFormat,
		java.util.Enumeration,
		java.util.HashMap,
		java.util.Iterator,
		java.util.List,
		java.util.Map,
		java.util.StringTokenizer,
		java.util.zip.ZipEntry,
		java.util.zip.ZipFile,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileItemFactory,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload"
	errorPage = "/00utils/error.jsp" buffer="16kb" autoFlush="true"%>
<%
UserNE          userNE	= session.getAttribute("userNE")==null?new UserNE():(UserNE)session.getAttribute("userNE");
UserSignOnNE    userSO	= new UserSignOnNE();
boolean         ursOK   = userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()));

response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache");
response.setHeader("Expires", "Thu, 29 Oct 1969 17:04:19 GMT");
			
Map 	hiddens 		= new HashMap();
String 	rutaAplicacion 	= getServletConfig().getServletContext().getRealPath(File.separator);
String 	rutaArchivos 	= rutaAplicacion+"00tmp"+File.separatorChar+"15cadenas"+File.separatorChar;
//System.out.println("rutaArchivos:"+rutaArchivos);
        rutaAplicacion 	= rutaAplicacion.substring(0, rutaAplicacion.lastIndexOf("nafin"));
String 	rutaCache       = rutaAplicacion.substring(0, rutaAplicacion.lastIndexOf("applications"))+"application-deployments"+File.separatorChar+"nafin"+File.separatorChar;
String 	rutaAbsoluta 	= "";
String 	archivoProcesar	= "";

boolean isMultipart = ServletFileUpload.isMultipartContent(request);
if(isMultipart) {
	FileItemFactory 	factory 	= new DiskFileItemFactory();
	ServletFileUpload 	upload 		= new ServletFileUpload(factory);
	List 				items 		= upload.parseRequest(request);
	Iterator 			iter 		= items.iterator();
	while (iter.hasNext()) {
		FileItem 	item 	= (FileItem) iter.next();
		String 		name 	= item.getFieldName();
		String 		value 	= item.getString();
		if (item.isFormField()) {
			hiddens.put(name, value);
		} else {
			String 	nombreCampo 	= item.getFieldName();
			long 	tamanioArchivo 	= item.getSize();
			String 	archivoOrigen 	= item.getName();
			String 	contentType 	= item.getContentType();
			if(!"".equals(archivoOrigen)) {
				String 	nombreArchivo	= archivoOrigen;
				if(archivoOrigen.lastIndexOf("/")>0) {
					nombreArchivo	= archivoOrigen.substring(archivoOrigen.lastIndexOf("/")+1);
				} else if(archivoOrigen.lastIndexOf("\\")>0) {
					nombreArchivo	= archivoOrigen.substring(archivoOrigen.lastIndexOf("\\")+1);
				}
						archivoProcesar = nombreArchivo.substring(0, nombreArchivo.lastIndexOf("."));
				String 	extension 		= archivoOrigen.substring(archivoOrigen.indexOf("."));			
				File 	archivo 		= new File(rutaArchivos + File.separatorChar + nombreArchivo);
				item.write(archivo);
				rutaAbsoluta			= archivo.getAbsolutePath();
			}//if(!"".equals(archivoOrigen))
		}//if (item.isFormField())
	}//while (iter.hasNext())
}//if(isMultipart)
String hidAction 	= (hiddens.get("hidAction")==null)?"":hiddens.get("hidAction").toString().trim();
if("P".equals(hidAction)||"U".equals(hidAction)) {
    String txt_destiny = (hiddens.get("txt_destiny")==null)?"":hiddens.get("txt_destiny").toString().trim();
    if(!"".equals(txt_destiny))
        rutaAplicacion = txt_destiny;
}
if("U".equals(hidAction)||"D".equals(hidAction)) {
	rutaAbsoluta 	= (hiddens.get("rutaAbsoluta")==null)?"":hiddens.get("rutaAbsoluta").toString().trim();
}
//System.out.println("txt_destiny:"+hiddens.get("txt_destiny"));
//System.out.println("hidAction:"+hidAction);
//System.out.println("rutaAbsoluta:"+rutaAbsoluta);
//System.out.println("rutaAplicacion:"+rutaAplicacion);
char diagonal = '/';
%>
<html>
<head>
<title>.:: ADMIN-NAFIN-ZIP ::.</title>
<link rel="Stylesheet" type="text/css" href="/nafin/home/css/nafinet.css">
<script language="Javascript" src="../00utils/valida.js"></script>
<script language="javascript">
<!--
<%if(!"ADMIN".equals(userNE.getNe_perfil())) {%>
document.onkeydown = 
	function() {
		if(window.event && window.event.keyCode == 116) {
			window.event.keyCode = 0;
			return false;
		}
	}

if (window.Event) 
	document.captureEvents(Event.MOUSEUP); 
function nocontextmenu() { 
	event.cancelBubble = true 
	event.returnValue = false; 
	return false; 
} 
function norightclick(e) { 
	if(window.Event) { 
		if(e.which !=1)
			return false; 
	} else if (event.button !=1) { 
		event.cancelBubble = true 
		event.returnValue = false; 
		return false; 
	}
}
document.oncontextmenu = nocontextmenu; 
document.onmousedown = norightclick;
<%}//if(!"ADMIN".equals(userNE.getNe_perfil()))%>
function validaDestino() {
	var f 				= document.frmContenido;
    if(f.chkDestiny.checked == false) {
        f.txt_destiny.disabled = false;
        f.txt_destiny.focus();
    } else if(f.chkDestiny.checked == true) {
        f.txt_destiny.value = "<%=rutaAplicacion.replace(File.separatorChar, diagonal)%>";
        f.txt_destiny.disabled = true;        
        f.txt_destiny.focus();
    }
}

function procesar() {
	var f 				= document.frmContenido;    
    if(f.txt_path.value=="") {
        alert("Elija un archivo");
        f.txt_path.focus();
        return;
    }
    if(f.txt_destiny.value=="") {
        alert("La ruta destino no es valida");
        f.txt_destiny.focus();
        return;
    }
    
	f.hidAction.value 	= "P";
	f.action 			= "00viewerzip.jsp";
	f.target 			= "_self";
	f.submit();
}

function unzip() {
	var f 				= document.frmContenido;
	f.hidAction.value 	= "U";
	f.action 			= "00viewerzip.jsp";
	f.target 			= "_self";
	f.submit();
}

function delcache() {
	var f 				= document.frmContenido;
	f.hidAction.value 	= "D";
	f.action 			= "00viewerzip.jsp";
	f.target 			= "_self";
	f.submit();
}

function setPage() {
	var f = document.frmContenido;
	parent.facultad.location.replace(f.cmb_page.value);	
}
//-->
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="frmContenido" method="post" action="00viewerzip.jsp" enctype="multipart/form-data">
<input type="hidden" name="hidAction" value="<%=hidAction%>">
<input type="hidden" name="rutaAbsoluta" value="<%=rutaAbsoluta%>">
<table width="100%" cellpadding="1" cellspacing="2" border="0">
<tr>
	<td>&nbsp;</td>
</tr>
<%if("".equals(hidAction)) {%>
<tr>
	<td>
	<table align="center">
	<tr>
		<td class="formas" align="right">ZIP File:</td>
		<td>
			<input 
				type		= "file" 
				class		= "formas" 
				name		= "txt_path" 
				value		= "Examinar" 
				size		= "45">
		</td>
	</tr>
	<tr>
		<td class="formas" align="right">Destiny:</td>
		<td>
			<input 
				type		= "text" 
				class		= "formas" 
				name		= "txt_destiny" 
				value		= "<%=rutaAplicacion%>"
				size		= "100"
                disabled>
            <input 
                type 	= "checkbox"
                name 	= "chkDestiny"
                value 	= "S" 
                class 	= "formas"
                onclick = "validaDestino()"
                checked = true;
            >
                
		</td>
	</tr>
	</table>
	</td>
</tr>
<tr>
	<td align="center">
		<input 
			type 		= "Button" 
			class 		= "celda02" 
			value 		= "Process" 
			title		= "Process" 
			onclick 	= "procesar()">
		<input 
			type 		= "Button" 
			class 		= "celda02" 
			value 		= "Cancel" 
			title 		= "Cancel" 
			onclick 	= "window.location.href='00viewerzip.jsp'">
	</td>
</tr>
<%  }//if("".equals(hidAction))%>
<%
if("P".equals(hidAction)||"U".equals(hidAction)||"D".equals(hidAction)) {
%>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td class="formas" align="left">
        <%="U".equals(hidAction)?"UnZiped":"Zip"%> path:<%=rutaAplicacion%>
        <input 
            type		= "hidden" 
            name		= "txt_destiny" 
            value		= "<%=rutaAplicacion%>">
    </td>
</tr>
<tr>
	<td align="center">
		<table cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF" width="80%">
		<tr>
			<td class="celda02" align="center" colspan="2">Name</td>
			<td class="celda02" align="center">Size</td>
			<td class="celda02" align="center">Date</td>
		</tr>
<%		
        //System.out.println("\n rutaAbsoluta:"+rutaAbsoluta);
		ZipFile zf = new ZipFile(rutaAbsoluta);
		BufferedWriter 	bufferedwriter 	= new BufferedWriter(new FileWriter(rutaArchivos+archivoProcesar+".csv"));
        for (Enumeration entries = zf.entries(); entries.hasMoreElements();) {
			ZipEntry entry = (ZipEntry)entries.nextElement();
			if(!entry.isDirectory()) {
				NumberFormat formatter = new DecimalFormat("###,###,###,###,###,###,###");
				String 	nombre 			= entry.getName();
				String 	extension 		= (nombre.substring(nombre.indexOf(".")+1)).toUpperCase();
				String	tamano			= String.valueOf(entry.getSize());
				String 	fecha 			= new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").format(new java.util.Date(entry.getTime()));
				long 	lastmodified	= entry.getTime();
				
				bufferedwriter.write(nombre+","+tamano+","+fecha+"\n");
				
				String existeArchivo = "";
				if("P".equals(hidAction)) {
					File producto = new File(rutaAplicacion+nombre);
					if(!producto.isFile()) {
						existeArchivo = "*";
						if(nombre.indexOf("nafin-web")>=0) {		//JSP
							File existeRutaApp = new File(producto.getParent());
							if(!existeRutaApp.isDirectory()) {
								existeArchivo = "Ruta Nueva";
							}
						}
					}
				} else if("U".equals(hidAction)||"D".equals(hidAction)) {
					if(
						"JAR".equals(extension) ||
						"JSP".equals(extension) ||
						"JSPF".equals(extension)
						) {
						if(nombre.indexOf("nafin-web")>=0) {			//JSP
							String rutaTmpJsp = "";
							StringTokenizer st = new StringTokenizer(nombre, "/");
							while (st.hasMoreTokens()) {
								String token = st.nextToken();
								if("nafin-web".equals(token)) {
									rutaTmpJsp = token+File.separatorChar+"persistence"+File.separatorChar+"_pages";
								} else {
									rutaTmpJsp += File.separatorChar+"_"+token;
								}
							}
							File 	tmpFile = new File(rutaCache+rutaTmpJsp);
							File 	tmpDir	= new File(tmpFile.getParent());
							String 	filtro 	= tmpFile.getName();
							filtro 	= filtro.substring(0, filtro.lastIndexOf("."));
							final String filtroFinal = filtro;
							
							FilenameFilter filter = new FilenameFilter() {
								public boolean accept(File dir, String name) {
									return name.startsWith(filtroFinal);
								}
							};
							String[] childrenTmp = tmpDir.list(filter);
							if(childrenTmp!=null) {
								for (int i=0; i<childrenTmp.length; i++) {
									boolean successJsp = (new File(tmpDir.getAbsolutePath()+File.separatorChar+childrenTmp[i])).delete();
								}//for (int i=0; i<children.length; i++)
							}
						} else if("U".equals(hidAction)) {				//EJB
							File ejbDirCache = new File(rutaCache+nombre);
							if(ejbDirCache.isDirectory()) {
								String[] children = ejbDirCache.list();
								if(children!=null) {
									for (int i=0; i<children.length; i++) {
										boolean successEjb = (new File(ejbDirCache.getAbsolutePath()+File.separatorChar+children[i])).delete();
									}//for (int i=0; i<children.length; i++)
								}//if(children!=null)
							}//if(ejbDirCache.isDirectory())
						}
					}
					
					if("U".equals(hidAction)) {
						File unZipFile 	= new File(rutaAplicacion+nombre);
						File unZipDir	= new File(unZipFile.getParent());
						if(!unZipDir.isDirectory()) {
							unZipDir.mkdirs();
							existeArchivo = "RUTA CREADA";
						}
						BufferedInputStream is = new BufferedInputStream(zf.getInputStream(entry));
						int count;
						byte data[] = new byte[4096];
						FileOutputStream fos = new FileOutputStream(rutaAplicacion+nombre);
						BufferedOutputStream dest = new BufferedOutputStream(fos, data.length);
						while ((count = is.read(data, 0, data.length)) != -1) {
							dest.write(data, 0, count);
						}
						dest.flush();
						dest.close();
						is.close();
						unZipFile = new File(rutaAplicacion+nombre);
						unZipFile.setLastModified(lastmodified);
					}//if("U".equals(hidAction))
					
				}//if("U".equals(hidAction))%>
				<tr>
					<td class="formas" align="left">
						&nbsp;<%=existeArchivo%>
					</td>
					<td class="formas" align="left">
						<%=nombre%>
					</td>
					<td class="formas" align="right">
						<%="".equals(tamano)?"&nbsp;":formatter.format(new Long(tamano))%>
					</td>
					<td class="formas" align="center">
						<%="".equals(fecha)?"&nbsp;":fecha%>
					</td>
				</tr>
<%			}
        }
		bufferedwriter.close();%>
		</table>
	</td>
</tr>
<%
String zipFile = "/nafin/00tmp/15cadenas/"+archivoProcesar+".zip";
String csvFile = "/nafin/00tmp/15cadenas/"+archivoProcesar+".csv";%>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td align="right">
        <input 
            type 	= "Button" 
            class 	= "celda02" 
            value 	= "Download File" 
            title 	= "Download File" 
            onclick = "window.location.href='<%=csvFile%>'">
        <input 
            type 		= "Button" 
            class 		= "celda02" 
            value 		= "Delete Cache" 
            title		= "Delete Cache" 
            onclick 	= "delcache()">
        <input 
            type 		= "Button" 
            class 		= "celda02" 
            value 		= "UnZip" 
            title		= "UnZip" 
            onclick 	= "unzip()">
        <input 
            type 		= "Button" 
            class 		= "celda02" 
            value 		= "Cancel" 
            title 		= "Cancel" 
            onclick 	= "window.location.href='00viewerzip.jsp'">
	</td>
</tr>
<%
} //if("P".equals(hidAction))
%>
<tr>
	<td>&nbsp;</td>
</tr>
</table>
</form>
</body>
</html>
