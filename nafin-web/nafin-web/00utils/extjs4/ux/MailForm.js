
Ext.define(
	'Ext.ux.HtmlEditorView',
	{
		extend: 			'Ext.panel.Panel',
		xtype: 			'htmleditorview',
		value:			'',
		bodyStyle:		'overflow-x:auto;',
		initComponent: function(){
			var me = this;
			me.html = me.processRawValue(me.value);
			me.callParent();
		},
		getName: 		function(){
			return this.name;
		},
		getValue:		function(){
			return this.value; 
		},
		setValue:		function(value){
			var me 		= this;	
			me.value 	= value;	
			me.update(me.processRawValue(value)); 
		},
		processRawValue: function(value){
			var me		 	= this;
	
			var mailform 	= me.up("mailform"); 
			var mailId		= mailform && mailform.getMailId();
			if( !Ext.isEmpty(mailId) ){
							
				var blank 		= "[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000]";
				var httpLink	= "\$1" + NE.appWebContextRoot + "/cid?mailId="+mailId+"&file=\$2";
				var tagRe		= null;
							
				// 1. Convertir links cid en tag de imagenes
				tagRe = new RegExp("(<"+blank+ "*img"+blank+ "+[^<>]*src"+blank+ "*="+blank+ "*[\"'])cid:([^<>]*>)","ig");
				value = value.replace( tagRe, httpLink );
							
				// 2. Convertir links cid en tag de links
				tagRe = new RegExp("(<"+blank+ "*link"+blank+ "+[^<>]*href"+blank+ "*="+blank+ "*[\"'])cid:([^<>]*>)","ig");
				value = value.replace( tagRe, httpLink );
							
			}
			return value;				
		}
		
	}
);
	
//'htmleditorfield'
Ext.define(
	'Ext.ux.htmleditorfield',
	{
		extend: 			'Ext.form.field.HtmlEditor',
		xtype: 			'htmleditorfield',
		value:			'',
		allowBlank: 	false,
		msgTarget: 		'side',
		labelAlign: 	'top',
		hideLabel: 		false,
		blankText: 		'Este campo es obligatorio',
		validateOnChange: true,
		unfinishedDefaultFont: 	'Verdana', 
		constructor: function () {
      	var me = this;me.callParent(arguments);
      	return me;
		},
		initComponent: function (arguments) {
      	var me 	= this,
            ext 	= Ext;
      	ext.apply(me, me.initialConfig);
			me.callParent(arguments);        
		},
		initEvents: function () {
        var me 	= this,
            ext 	= Ext;
				me.callParent(arguments);        
		},
		unfinishedInitDefaultFontNew: function(){

        // It's not ideal to do this here since it's a write phase, but we need to know
        // what the font used in the textarea is so that we can setup the appropriate font
        // options in the select box. The select box will reflow once we populate it, so we want
        // to do so before we layout the first time.
        
        var me = this,
            selIdx = 0,
            fonts, font, select,
            option, i, len, lower;

         fonts = Ext.Array.clone(me.fontFamilies);
         if (!me.defaultFont) {
				font = me.textareaEl.getStyle('font-family');
				font = Ext.String.capitalize(font.split(',')[0]);
				Ext.Array.include(fonts, font);
				me.defaultFont = font;
         }
			fonts.sort();

         select = me.down('#fontSelect').selectEl.dom;
         for (i = 0, len = fonts.length; i < len; ++i) {
             font = fonts[i];
             lower = font.toLowerCase();
             option = new Option(font, lower);
             if (font == me.defaultFont) {
                selIdx = i;
             }
             option.style.fontFamily = lower;
                
             if (Ext.isIE) {
               select.add(option);
             } else {
               select.options.add(option); 
             }
          }
          // Old IE versions have a problem if we set the selected property
          // in the loop, so set it after.
          select.options[selIdx].selected = true;
        
    },
	 isValid: function () {
        var me = this,
            disabled = me.disabled,
            validate = me.forceValidation || !disabled;
				return validate ? me.validateValue(me.removeWhiteSpace(me.getValue())) : disabled;
    },
	 validateValue: function (value) {
        var me = this,
            errors = me.getErrors(value),
            isValid = Ext.isEmpty(errors);
			if ((value === null) || (value === undefined))
        { return true; }
		  if (!me.preventMark) {
            if (isValid) {
                me.clearInvalid();
            } else {
                me.markInvalid(errors);
            }
        }
		  return isValid;
    },
	 
	 removeWhiteSpace: function (value) { 
	 	 var me = this;

	 	 var editorBody = me.getEditorBody();
	 	 // Si no hay contenido, regresar cadena vac�a
	 	 var whiteSpace = /^[\x09\x0a\x0b\x0c\x0d\x20\xa0\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u2028\u2029\u202f\u205f\u3000]*$/g;
	 	 if( whiteSpace.test( editorBody.textContent || editorBody.innerText || "" ) ){
	 	 	 value = "";
	 	 } 
	 	 return value;
	 	 
    },	
	 processValue: function(value) { 
	 	 var me = this;

	 	 if( (/^\s*<\s*FONT/i).test(value) === false ){
	 	 	 value = '<font face="'+ (me.defaultFont).toLowerCase() +'" >' + value + '</font>';
	 	 }
	 	 return value;
	 	 
    },
    getSubmitData: function() {
        var me = this,
            data = null;
        if (!me.disabled && me.submitValue && !me.isFileUpload()) {
            data = {};
            data[me.getName()] = '' + me.processValue(me.getValue());
        }
        return data;
    },
    resetToolbar: function(){
    	  var me = this,
            i, l, btns, doc, name, queriedName, fontSelect,
            toolbarSubmenus;

        if (me.readOnly) {
            return;
        }

        if (!me.activated) {
            me.onFirstFocus();
            return;
        }

        btns = me.getToolbar().items.map;
        doc = me.getDoc();

        if( me.sourceEditMode === true ){
        	  me.toggleSourceEdit(!me.sourceEditMode);
		  }
						
        if (me.enableFont && !Ext.isSafari2) {
            // When querying the fontName, Chrome may return an Array of font names
            // with those containing spaces being placed between single-quotes.
            queriedName = doc.queryCommandValue('fontName');
            name 			= (me.defaultFont).toLowerCase(); 
            fontSelect = me.fontSelect.dom;
            if (name !== fontSelect.value ) { // || name != queriedName
                fontSelect.value = name;
                me.relayCmd('fontName', me.defaultFont);
            }
        }

        function resetButtons() {
            var state;
            
            for (i = 0, l = arguments.length, name; i < l; i++) {
                name  = arguments[i];
                
                // Firefox 18+ sometimes throws NS_ERROR_INVALID_POINTER exception
                // See https://sencha.jira.com/browse/EXTJSIV-9766
                try {
                    state = doc.queryCommandState(name);
                }
                catch (e) {
                    state = false;
                }
                                
                if( state ){
                	 btns[name].toggle(!state);
                }
                
            }
        }
        if(me.enableFormat){
            resetButtons('bold', 'italic', 'underline');
        }
        if(me.enableAlignments){
            resetButtons('justifyleft', 'justifycenter', 'justifyright');
        }
        if(!Ext.isSafari2 && me.enableLists){
            resetButtons('insertorderedlist', 'insertunorderedlist');
        }

        // Ensure any of our toolbar's owned menus are hidden.
        // The overflow menu must control itself.
        toolbarSubmenus = me.toolbar.query('menu');
        for (i = 0; i < toolbarSubmenus.length; i++) {
            toolbarSubmenus[i].hide();
        }
        me.syncValue();
    },
	 getErrors: function (value) {
        var errors = [];
        if ((!this.allowBlank) && (!value || value === '')) {
            errors.push(this.blankText);
        }
        return errors;
    },
	 markInvalid: function (errors) {
        var me = this,
            oldMsg = me.getActiveError();
        me.setActiveErrors(Ext.Array.from(errors));
        if (oldMsg !== me.getActiveError()) {
            me.updateLayout();
        }
		   
    },
	renderActiveError: function () {
        var me = this,
            hasError = me.hasActiveError();
        if (me.inputEl) {
            // Add/remove invalid class
            me.bodyEl[hasError ? 'addCls' : 'removeCls'](me.invalidCls + '-field');
        }
        me.mixins.labelable.renderActiveError.call(me);
    },
	 clearInvalid: function () {
        var me = this,
            hadError = me.hasActiveError();
        me.unsetActiveError();
        if (hadError) {
            me.updateLayout();
        }
    },
	 labelableRenderTpl: [
        // body row. If a heighted Field (eg TextArea, HtmlEditor, this must greedily consume height.
        '<tr id="{id}-inputRow" <tpl if="inFormLayout">id="{id}"</tpl>>',
            // Label cell
            '<tpl if="labelOnLeft">',
                '<td id="{id}-labelCell" style="{labelCellStyle}" {labelCellAttrs}>',
                    '{beforeLabelTpl}',
                    '<label id="{id}-labelEl" {labelAttrTpl}<tpl if="inputId"> for="{inputId}"</tpl> class="{labelCls}"',
                        '<tpl if="labelStyle"> style="{labelStyle}"</tpl>',
                        // Required for Opera
                        ' unselectable="on"',
                    '>',
                        '{beforeLabelTextTpl}',
                        '<tpl if="fieldLabel">{fieldLabel}{labelSeparator}</tpl>',
                        '{afterLabelTextTpl}',
                    '</label>',
                    '{afterLabelTpl}',
                '</td>',
            '</tpl>',
            // Body of the input. That will be an input element, or, from a TriggerField, a table containing an input cell and trigger cell(s)
            '<td class="{baseBodyCls} {fieldBodyCls}" id="{id}-bodyEl" colspan="{bodyColspan}" role="presentation">',
                '{beforeBodyEl}',
                // Label just sits on top of the input field if labelAlign === 'top'
                '<tpl if="labelAlign==\'top\'">',
                    '{beforeLabelTpl}',
                    '<div id="{id}-labelCell" style="{labelCellStyle}">',
                        '<label id="{id}-labelEl" {labelAttrTpl}<tpl if="inputId"> for="{inputId}"</tpl> class="{labelCls}"',
                            '<tpl if="labelStyle"> style="{labelStyle}"</tpl>',
                            // Required for Opera
                            ' unselectable="on"',
                        '>',
                            '{beforeLabelTextTpl}',
                            '<tpl if="fieldLabel">{fieldLabel}{labelSeparator}</tpl>',
                            '{afterLabelTextTpl}',
                        '</label>',
                    '</div>',
                    '{afterLabelTpl}',
                '</tpl>',

                '{beforeSubTpl}',
                '{[values.$comp.getSubTplMarkup(values)]}',
                '{afterSubTpl}',
            // Final TD. It's a side error element unless there's a floating external one
            '<tpl if="msgTarget===\'side\'">',
                '{afterBodyEl}',
                '</td>',
                '<td id="{id}-sideErrorCell" vAlign="\'top\'" style="{[values.autoFitErrors ? \'display:none\' : \'\']}" width="{errorIconWidth}">',
                    '<div id="{id}-errorEl" class="{errorMsgCls}" style="display:none"></div>',
                '</td>',
            '<tpl elseif="msgTarget==\'under\'">',
                '<div id="{id}-errorEl" class="{errorMsgClass}" colspan="2" style="display:none"></div>',
                '{afterBodyEl}',
                '</td>',
            '</tpl>',
        '</tr>',
        {
            disableFormats: true
        }
		]
	});
 
/**
 *
 * Forma para el env�o de correos.
 *
 * @author jshernandez
 * @since 08/05/2014 06:20:34 p.m.
 *
 */
Ext.define(
	'Ext.ux.MailForm',
	{
		extend: 			'Ext.form.Panel',
		xtype: 			'mailform',
		// Propiedades espec�ficas de la ventana
		title: 			'Cliente Correo',
		closable: 		false,
		frame:			true,
		width: 			600,
		height: 			600,
		bodyPadding: 	23,
		style:			'margin: 0 auto',
		autoScroll:		true,
		// Indica si la forma es editable
		editMode:		true,	
		closeAction:	'hide',
		// Atributos de configuracion
		config: {
			formaAuxiliar: 						'formAux', // ID de la forma que se utilizar� para descargar los archivos adjuntos
			url:										undefined, // Este campo es requerido
			informacionCreaArchivoAdjunto:	'CreaArchivoAdjunto', // Acci�n por default que se utilizar� para solicitar la creaci�n de archivos adjuntos.
			informacionSubmit:					'EnviaCorreo',
			mailId:									undefined,	// Id del correo, debe venir vac�o si se trata de un correo nuevo
			// Opciones de visibilidad
			hideArchivoAdjunto:  				true,  // Modo Edicion ( Editable    )
			hideVistaArchivosAdjuntos:			false, // Modo Edicion/Vista ( No Editable )
			hideRemitente:							false,
			hideDestinatarios:					false,
			hideCcDestinatarios:					true,
			hideBccDestinatarios:				true,
			hideAsunto:								false,
			hideCuerpoCorreo:						false, // Modo Edicion ( Editable )
			hideVistaCuerpoCorreo:				false, // Modo Vista   ( No Editable )
			hideBotonEnviarCorreo:				false,
			hideBotonCerrar:						false,
			// Configuracion de los diferentes componentes de la forma de correos
			archivoAdjuntoCfg:  					null,
			vistaArchivosAdjuntosCfg:			null,
			vistaArchivoAdjuntoCfg:				null,
			remitenteCfg:							null,
			destinatariosCfg:						null,
			ccDestinatariosCfg:					null,
			bccDestinatariosCfg:					null,
			asuntoCfg:								null,
			cuerpoCorreoCfg:						null,
			vistaCuerpoCorreoCfg:				null,
			botonEnviarCorreoCfg:				null,
			botonCerrarCfg:						null
		},
		messageBodyItems: [],
		items: 				[],
		buttons: 			[],
		initComponent: function(){
			
			var me 			= this;
			var item			= null;

			// Guardar copia de los items que se especificaron en la configuracion del componente
			var messageBodyItems = me.items || [];
			
			// Determinar si se tiene definido el elemento: cuerpoMensaje
			var tieneCuerpoCorreo = false;
			Ext.Array.each(
				messageBodyItems,
				function(componente){
					if ( 			componente && !componente.isComponent	&& componente.itemId		=== "cuerpoCorreo" ){
						tieneCuerpoCorreo = true;
						return false;
					} else if ( componente && componente.isComponent 	&& componente.itemId()	=== "cuerpoCorreo" ){
						tieneCuerpoCorreo = true;
						return false;
					}
				}
			);	
			
			// Determinar si se tiene definido el elemento: vistaCuerpoMensaje
			var tieneVistaCuerpoCorreo = false;
			Ext.Array.each(
				messageBodyItems,
				function(componente){
					if ( 			componente && !componente.isComponent	&& componente.itemId		=== "vistaCuerpoCorreo" ){
						tieneVistaCuerpoCorreo = true;
						return false;
					} else if ( componente && componente.isComponent 	&& componente.itemId() 	=== "vistaCuerpoCorreo" ){
						tieneVistaCuerpoCorreo = true;
						return false;
					}
				}
			);	
						
			// Construir los componentes de la forma
			var items		= [];
			
			// CABECERA
			
			// 1. Crear Campo: Datos Adjuntos
			var cmpArchivoAdjunto = me.createArchivoAdjunto();
			// Debido a un Bug en IE7, cuando el componente se encuentra oculto, este a�ade un offset vertical
			if( !Ext.isEmpty(cmpArchivoAdjunto) ){
				items.push(cmpArchivoAdjunto);
			}
			// 2. Crear Campo: Vista Archivo Adjunto
			items.push(me.createVistaArchivosAdjuntos());
			// 3. Crear Campo: Remitente
			items.push(me.createRemitente());
			// 4. Crear Campo: Destinatarios
			items.push(me.createDestinatarios());
			// 5. Crear Campo: CC Destinatarios
			items.push(me.createCcDestinatarios());
			// 6. Crear Campo: BCC Destinatarios
			items.push(me.createBccDestinatarios());
			// 7. Crear Campo: Asunto
			items.push(me.createAsunto());
			
			// CUERPO DEL MENSAJE
			
			// 8. Crear Campo: Cuerpo Correo
			if( !tieneCuerpoCorreo ){
				var cmp = me.createCuerpoCorreo(); 
				items.push(cmp);
				me.messageBodyItems.push(cmp);
			}
			// 9. Crear Campo: Vista Cuerpo Correo
			if( !tieneVistaCuerpoCorreo ){
				var cmp = me.createVistaCuerpoCorreo();
				items.push(cmp);
				me.messageBodyItems.push(cmp);
			}	
			// 9.1. Agregar los elementos que especific� el usuario
			Ext.Array.each(
				messageBodyItems,
				function(componente){
					
					if ( 			componente && !componente.isComponent	&& componente.itemId === "cuerpoCorreo" 		 ){
						
						me.cuerpoCorreoCfg 		= componente;
						var cmp = me.createCuerpoCorreo(); 
						items.push(cmp);
						me.messageBodyItems.push(cmp);
						
					} else if ( componente && !componente.isComponent	&& componente.itemId === "vistaCuerpoCorreo" ){
						
						me.vistaCuerpoCorreoCfg = componente;
						var cmp = me.createVistaCuerpoCorreo();
						items.push(cmp);
						me.messageBodyItems.push(cmp);
						
					} else if ( componente && !componente.isComponent ){
						
						componente.hidden = me.editMode?false:true;
						items.push(componente);	
						me.messageBodyItems.push(componente);
						
					} else {
						
						componente.setVisible(me.editMode?true:false);
						items.push(componente);	
						me.messageBodyItems.push(componente);
						
					}
				}
			);
			
			// Asignar los items reconfigurados al atributo items.
			me.items = items;
						
			// AGREGAR BOTONES
			
			// 12. Crear Bot�n Enviar Correo
			me.buttons.push(me.createBotonEnviarCorreo());
			// 13. Crear Bot�n Cerrar
			//me.buttons.push(me.createBotonCerrar());
			
			// AGREGAR EVENTO
			me.addEvents(
				'emailSubmitSuccess'
			);
			
			// Reset htmleditor toolbar components
			me.on(
				'show', 
				function(mailform){
					if( me.editMode === true ){
						var cuerpoCorreo	= mailform.query("#cuerpoCorreo")[0];
						cuerpoCorreo.resetToolbar();
					}
				}
			);
			
			// Registrar handler de evento hide para resetear los componentes que fueron cambiados de tama�o.
			me.on(
				'hide', 
				function(mailform){
					// suspend Layouts
					mailform.suspendLayout = true;
					// Reset resizable components height
					Ext.Array.each(
						mailform.query('#destinatarios,#ccDestinatarios,#bccDestinatarios,#asunto'),
						function(cmp){
							cmp.setHeight(cmp.initialConfig.height);
							if(cmp.resizer) cmp.resizer.resizeTo( undefined, cmp.initialConfig.height );
						}
					);
					// Turn the suspendLayout flag off.
					me.suspendLayout = false;
					// Trigger a layout.
					me.doLayout();
				}
			);
			
			// Invocar initComponent de la clase padre
			me.callParent();
			
		},	
		getValues: function(){
			
			var me		= this;
			var values 	= {};
			
			values['mailId'] = me.getMailId();
			
			me.items.each(
				function(componente){
					if( componente['getName'] &&  componente['getValue'] ){
						values[componente.getName()] = componente.getValue();
					}
				}
			);

			// No se puede enviar el contenido de un filefield
			delete values.archivoAdjunto;
		
			// No se podr� enviar el campo vistaCuerpoCorreo, ya que es
			// una visualizaci�n alterna a lo capturado en el htmleditor
			if( me.editMode === false ){
				values.cuerpoCorreo = values.vistaCuerpoCorreo;
			}
			delete values.vistaCuerpoCorreo;

			return values;
			
		},
		setValues: function(values){

			var me = this;
			
			var newValues = Ext.apply({},values);
			
			// Hacer ajustes dependiendo del modo de visualizacion
			if( me.editMode === false ){
				newValues.vistaCuerpoCorreo = newValues.cuerpoCorreo;
				delete newValues.cuerpoCorreo;
			}
			
			// Bajo ninguna circunstancia se permite asignar valor a un filefield
			delete newValues.archivoAdjunto;
			
			// Si se muestra el filefield y es modo de edici�n, no se podr� inicializar
			// el contenido de la vista de archivos adjuntos
			if( me.editMode === true && me.hideArchivoAdjunto === false ){
				newValues.vistaArchivosAdjuntos = null;
			}
			
			// Registrar id del correo, por el momento solo se ocupa para los correos almacenados en bitacora.
			me.setMailId( newValues['mailId'] );
				
			// Asignar los valores segun se requiera
			me.items.each(
				function(cmp){
					
					if(        !cmp['getName']  ){
						return true;
					} else if( !cmp['setValue'] ){
						return true;
					}
					
					var name  = cmp.getName();		
					if( name == "archivoAdjunto" ){
						cmp.clearValue(); // Bajo ninguna circunstancia se permite asignar valor a un filefield
					} else {
						cmp.setValue( newValues[name] );
					}
					
				}
			);
			
			me.clearInvalid();
			
		},
		clearInvalid: function(){

			var me 		= this;
			
			me.items.each(
				function(cmp){
					
					if( !cmp['clearInvalid']  ){
						return true;
					}
					
					cmp.clearInvalid();
					
				}
			);

		},
		isValid:	 function(){

			var me 		= this;
			var valid	= true;
			var isValid = true;
			
			me.items.each(
				function(cmp){
					
					if( !cmp['isValid']  ){
						return true;
					}
					
					isValid = cmp.isValid();
					
					if( isValid === false ){
						valid = false;
					}
					
				}
			);
				
			return valid;
			
		},
		submitCallback: function(form, action, success){
			
			var me = this;
			me.getEl().unmask();

			// Procesar respuesta
			if(success){
				
				if(!Ext.isEmpty(action.result.msg)){
					
					Ext.Msg.alert(
						'Mensaje',
						action.result.msg,
						function(btn, text){
							me.fireEvent('emailSubmitSuccess',form,action);
						}
					);
					
				} else {
					me.fireEvent('emailSubmitSuccess',form,action);
				}
				
			} else {
				
				if( action.response && !Ext.isDefined(action.response.status) ){
					action.response.status = 200;
				}
				NE.util.mostrarErrorResponse4(action.response);
				
			}

		},
		submit: function( options ){
			
			var me 					= this;	
			
			var values 				= me.getValues();
			if( options && options.params ){
				values = Ext.apply( me.getValues(), options.params );
			}

			// Formatear valores de todo campo que sea de tipo array,
			// para que pueda ser interpretado correcountment en el servidor
			Ext.Object.each(
				values, 
				function(key, value){
					if( Ext.isArray(value) ){
						for(var i=0;i<value.length;i++){
							value[i] = Ext.encode(value[i]);
						}
					}
				}
			);
			
			// Agregar orden en que ser�n leidos los elementos del cuerpo de correo
			var _ordenCuerpoCorreo 	= "";
			var count			  		= 0;
			var camposExcluidos		= [ 'remitente',  'destinatarios', 'ccDestinatarios', 'bccDestinatarios', 'asunto', 'archivoAdjunto', 'vistaArchivosAdjuntos', 'vistaCuerpoCorreo' ];
			me.items.each(
				function(cmp){
					
					if( !cmp['getName'] ){
						return true;
					}
					
					var fieldName = cmp.getName();
					if( Ext.Array.indexOf( camposExcluidos, fieldName ) != -1 ){
						return true;
					}
					
					if( count > 0 ){
						_ordenCuerpoCorreo += ","; 
					}
					_ordenCuerpoCorreo += fieldName;
					count++
					
				}
			);
			values["_ordenCuerpoCorreo"] = _ordenCuerpoCorreo;
			
			// Borrar el nombre del archivo, ya que podr�a interferir input tag del archivo.
			delete values.archivoAdjunto;
						
			me.getEl().mask("Enviando correo...",'x-mask-loading');
			me.getForm().submit({
				 clientValidation: true,
				 url: 		me.getUrl() + "?informacion=" + me.getInformacionSubmit(),
				 params: 	values,
				 success:	Ext.Function.bind( me.submitCallback, me, [ true  ], true),
				 failure: 	Ext.Function.bind( me.submitCallback, me, [ false ], true)
			});
			
		},
		getEditMode: function(){
			return this.editMode;
		},
		setEditMode: function(value){
			
			var me = this;

			if( me.editMode === value ){
				return;
			}
			
			// Remove all components from field container
			me.suspendLayout = true;
			
			// Remove fields content
			me.items.each(
				function(componente){
					if( componente['setValue'] ){
						componente.setValue(null);
					}
				}
			);
			me.clearInvalid();
			
			if( value === true ){
				
				Ext.Array.each(
					me.query("textarea, htmleditorview"),
					function(cmp){
						if( "remitente" == cmp.getName() ){
							return true;
						}
						cmp.removeCls('readOnly');
					}
				);
				
				// Debido a un bug en IE7 se crear� el archivo adjunto
				
				// 1. Crear Campo: Datos Adjuntos
				var cmpArchivoAdjunto = me.createArchivoAdjunto();
				// Debido a un Bug en IE7, cuando el componente se encuentra oculto, este a�ade un offset vertical
				if( !Ext.isEmpty(cmpArchivoAdjunto) ){
					me.insert(0,cmpArchivoAdjunto);
				}
								
				var listaItemIds = "";
				var count			  = 0;
				Ext.Array.each(
					me.messageBodyItems,
					function(cmp){
						if( count > 0 ){
							listaItemIds += ", "; 
						}
						listaItemIds += "#" + cmp.itemId;
						count++
					}
				);
				
				Ext.Array.each(
					me.query(listaItemIds),
					function( cmp ){

						switch( cmp.getItemId() ){
							case 'vistaCuerpoCorreo':
								cmp.hide();
								break;
							default:
								cmp.show();
						}
						
					}
				);

				me.query("#botonEnviarCorreo")[0].show();
				
				me.editMode = true;
				
			} else {
				
				Ext.Array.each(
					me.query("textarea, htmleditorview"),
					function(cmp){
						if( "remitente" == cmp.getName() ){
							return true;
						}
						cmp.addCls('readOnly');
					}
				);
				
				// Debido a un bug en IE7 se destruira el campo de archivos adjuntos
				Ext.Array.each(
					me.query('filefield'),
					function(cmp){
						me.remove(cmp,true);
					}
				)
				
				
				var listaItemIds = "";
				var count			  = 0;
				Ext.Array.each(
					me.messageBodyItems,
					function(cmp){
						if( count > 0 ){
							listaItemIds += ", "; 
						}
						listaItemIds += "#" + cmp.itemId;
						count++
					}
				);
				
				Ext.Array.each(
					me.query(listaItemIds),
					function( cmp ){

						switch( cmp.getItemId() ){
							case 'vistaCuerpoCorreo':
								cmp.show();
								break;
							default:
								cmp.hide();
						  
						}
						
					}
				);
				
				me.query("#botonEnviarCorreo")[0].hide();
				
				me.editMode = false;
				
			}
			
			// Turn the suspendLayout flag off.
			me.suspendLayout = false;
			// Trigger a layout.
			me.doLayout();
			
		},
		createArchivoAdjunto: function(){
			var me  = this;
			var cmp = Ext.apply(
				{
					xtype: 			'filefield',
					fieldLabel: 	'Datos Adjuntos',
					labelWidth: 	90,
					msgTarget: 		'side',
					allowBlank: 	true,
					anchor: 			'100%',
					//buttonText: 	'',
					clearOnSubmit: false,
					buttonConfig: {
						iconCls: 	'upload-icon'
					},
					listeners: {
						change: function(filefield, value) {
							
							var newValue = value;
							var slashesArr = newValue.split("/");
							if(slashesArr.length){
								newValue = slashesArr[slashesArr.length-1];
							}
							
							var backslashesArr = newValue.split("\\");
							if(backslashesArr.length){
								newValue = backslashesArr[backslashesArr.length-1];
							}
							
							filefield.setRawValue(newValue);
							
						}
					},
					getName:	function(){
						return this.name;
					},
					getValue: function(){
						return this.rawValue;
					},
					clearValue: function(){
						var me						= this;
						var tmpClearOnSubmit 	= me.clearOnSubmit;
						// Borrar contenido del campo
						me.clearOnSubmit 			= true;
						me.reset();
						// Restaurar configuracion original del atributo: clearOnSubmit
						me.clearOnSubmit 			= tmpClearOnSubmit;
					}
				},
				me.archivoAdjuntoCfg 
			);
			
			cmp.name		= 'archivoAdjunto';
			cmp.itemId	= 'archivoAdjunto';
			
			if( me.editMode === true && me.hideArchivoAdjunto === false ){
				cmp.hidden = false;
			} else {
				cmp.hidden = true;
			}
			
			if( cmp.hidden === true ){
				cmp = undefined;
			}
			
			return cmp;
		},
		getProcesarSuccessFailureGeneraArchivoAdjunto: function(){
		
			var me = this;
			return function(opts, success, response) {
	
				var button	= me.query('#' + opts.params.itemId )[0];
				button.enable();

				//Success = true si la peticion AJAX fue exitosa. Por otra parte el valor del success del Json puede ser distinto al de la peticion
				if (success == true && Ext.JSON.decode(response.responseText).success == true) {
		 
					var respuesta    = Ext.JSON.decode(response.responseText);
					var datosArchivo = respuesta.datosArchivo;
					
					// Asignar icono de archivo en caso el usuario haya especificado uno
					var iconoArchivo = 'icoDownloadFile';
					if(        respuesta.iconoArchivo     && !Ext.isEmpty(respuesta.iconoArchivo) ){
						iconoArchivo = respuesta.iconoArchivo;
					} else if( datosArchivo.nombreArchivo && !Ext.isEmpty(datosArchivo.nombreArchivo) ){
						if(/^.+\.([pP][dD][fF])$/.test(datosArchivo.nombreArchivo)){
							iconoArchivo = 'icoPdf';
						} else if(/^.+\.([dD][oO][cC])$/.test(datosArchivo.nombreArchivo)){
							iconoArchivo = 'icoDoc';
						} else if(/^.+\.([dD][oO][cC][xX])$/.test(datosArchivo.nombreArchivo)){
							iconoArchivo = 'icoDoc';
						} else if(/^.+\.([xX][lL][sS])$/.test(datosArchivo.nombreArchivo)){
							iconoArchivo = 'icoXls';
						} else if(/^.+\.([xX][lL][sS][xX])$/.test(datosArchivo.nombreArchivo)){
							iconoArchivo = 'icoXls';
						} else if(/^.+\.([zZ][iI][pP])$/.test(datosArchivo.nombreArchivo)){
							iconoArchivo = 'icoZip';
						} else if(/^.+\.([zZ[iI][pP])$/.test(datosArchivo.nombreArchivo)){
							iconoArchivo = 'icoZip';
						} else if(/^.+\.([tT][xX][tT])$/.test(datosArchivo.nombreArchivo)){
							iconoArchivo = 'icoTxt';
						}  
					}
					button.setIconCls(iconoArchivo);
					
					
					// Asignar nuevo titulo del archivo
					var vistaArchivoAdjuntoText 	= datosArchivo.tituloArchivo;
					//vistaArchivoAdjuntoText  		= vistaArchivoAdjuntoText.length > 70?vistaArchivoAdjuntoText.substring(0,70)+"...":vistaArchivoAdjuntoText;
					button.setText(vistaArchivoAdjuntoText); 
					
					// Actualizar parametros del archivo
					var vistaArchivoAdjuntoValue  = button.getValue();
					vistaArchivoAdjuntoValue.tituloArchivo	= datosArchivo.tituloArchivo;
					vistaArchivoAdjuntoValue.nombreArchivo	= datosArchivo.nombreArchivo;
					vistaArchivoAdjuntoValue.directorio		= datosArchivo.directorio;
					
					button.getValue(vistaArchivoAdjuntoValue);
					
					button.getEl().highlight('FFF700', {duration: 5, easing:'bounceOut'});
				
				} else {

					// Procesar respuesta
					if( !Ext.isEmpty(response) && !Ext.isEmpty(response.msg)){
						Ext.Msg.alert(
							'Mensaje',
							response.msg,
							function(btn, text){
								button.setIconCls('icoBuscar');
								NE.util.mostrarErrorPeticion(response); 
							}
						);
					} else {
						button.setIconCls('icoBuscar');
						NE.util.mostrarErrorPeticion(response);
					}
		 
				}
	 
			}
		},
		archivoAdjuntoHandler: function(button){ 
				
				var me 								= button.up('mailform');
				var parametrosArchivo			= Ext.apply({},button.getValue());
				
				if( 
					Ext.isEmpty( parametrosArchivo.tituloArchivo )
						||
					Ext.isEmpty( parametrosArchivo.nombreArchivo )
						||
					Ext.isEmpty( parametrosArchivo.directorio )
				){
				  	 	
					// Cambiar icono
					button.disable();
					button.setIconCls('x-mask-msg-text');
						
					parametrosArchivo.informacion	= me.getInformacionCreaArchivoAdjunto();
					parametrosArchivo.itemId		= button.getItemId();
					
					// Genera Archivo Adjunto en el Servidor
					Ext.Ajax.request({
						url: 			me.getUrl(),
						params: 		parametrosArchivo,
						callback: 	me.getProcesarSuccessFailureGeneraArchivoAdjunto()
					});
				  	  	  
				} else {
              	 
					var forma    		= Ext.getDom('formAux');
					/*forma.action 		= NE.appWebContextRoot+'/DescargaArchivoUsuario';
					forma.action 		= NE.appWebContextRoot+'/DescargaArchivo';					
					forma.method 		= 'post';
					forma.target 		= '_self';
					*/
					
					var archivo = parametrosArchivo.directorio+'/'+parametrosArchivo.tituloArchivo;				
					archivo = archivo.replace('/nafin','');
					var params = {nombreArchivo: archivo};				
					forma.action = NE.appWebContextRoot+'/DescargaArchivo?' + Ext.urlEncode(params);
					forma.method 		= 'post';
					forma.target 		= '_self'; 			
					
					// Preparar Archivo a Descargar
					// var archivo  = boton.urlArchivo;
					// archivo      = archivo.replace(new RegExp("^" + Ext.escapeRe(NE.appWebContextRoot)),'');
					// Insertar Titulo Archivo
					var inputTituloArchivo = Ext.DomHelper.insertFirst(
					  forma,
					  {
						 tag:  'input',
						 type: 'hidden',
						 id:   'tituloArchivo',
						 name: 'tituloArchivo',
						 value: parametrosArchivo.tituloArchivo
						},
						true
					);
					// Insertar Nombre Archivo
					var inputNombreArchivo = Ext.DomHelper.insertFirst(
						forma,
						{
							tag:  'input',
							type: 'hidden',
							id:   'nombreArchivo',
							name: 'nombreArchivo',
							value: parametrosArchivo.nombreArchivo
						},
						true
					);
					// Insertar Directorio
					var inputDirectorio = Ext.DomHelper.insertFirst(
						forma,
						{
							tag:  'input',
							type: 'hidden',
							id:   'directorio',
							name: 'directorio',
							value: parametrosArchivo.directorio
						},
						true
					);
					// Solicitar Archivo Al Servidor
					forma.submit();
					// Remover nodos agregados
					inputTituloArchivo.remove();
					inputNombreArchivo.remove();
					inputDirectorio.remove();
                                               
				}
				
		},
		createVistaArchivoAdjunto: function(detalleArchivo){

			var vistaArchivoAdjunto = Ext.apply(
				{
					xtype:	'button',
					text: 	'',
					tooltip:	'',
					iconCls:	'icoBuscar',
					maxWidth: 420,
					handler:  Ext.EmptyFn,
					getValue: function(){
						return this.value;
					}
				},
				this.mailform.vistaArchivoAdjuntoCfg
			);
			vistaArchivoAdjunto.name		= 'vistaArchivoAdjunto' + this.numeroArchivo;
			vistaArchivoAdjunto.itemId		= 'vistaArchivoAdjunto' + this.numeroArchivo;
			vistaArchivoAdjunto.hidden 	= false; 
			vistaArchivoAdjunto.text   	= detalleArchivo.tituloArchivo.length > 70?detalleArchivo.tituloArchivo.substring(0,70)+"...":detalleArchivo.tituloArchivo; 
			vistaArchivoAdjunto.tooltip	= detalleArchivo.tituloArchivo;
			vistaArchivoAdjunto.value		= Ext.apply({},detalleArchivo);
			vistaArchivoAdjunto.handler	= this.mailform.archivoAdjuntoHandler;// Ext.Function.bind(me.archivoAdjuntoHandler, me, []);
					  
			if(Ext.isArray(this.fieldcontainer.items)){
				this.fieldcontainer.items.push( vistaArchivoAdjunto );
			} else {
				this.fieldcontainer.add( vistaArchivoAdjunto );
			}
			
			this.numeroArchivo++;
					
		},
		getVistaArchivosAdjuntos: function(){
			return this.query('#vistaArchivosAdjuntos')[0];
		},
		createVistaArchivosAdjuntos: function(){
			
			var me  		 = this;
			var archivos = me.vistaArchivosAdjuntosCfg && me.vistaArchivosAdjuntosCfg.items?me.vistaArchivosAdjuntosCfg.items:null;
			
			var cmp = Ext.apply(
				{
					xtype: 		'fieldcontainer',
					fieldLabel: 'Datos Adjuntos',
					labelWidth: 90,
					width:	   '100%',
					layout: 		{ type:'vbox', align:'left' },
               defaults: 	{ margin:'0 0 2 0'},
					items: 		[],
					getName: function(){
						return this.name;
					},
					getValue: function(){
						var values = [];
						Ext.Array.each(
							this.query('button'),
							function(componente){
								values.push(componente.getValue());
							}
						);
						return values;
					},
					setValue: function(archivos){

						var me  	= this.up('mailform');
						var cmp 	= this;
						cmp.suspendLayout = true;
						// Remove all components from field container
						cmp.removeAll(true);
						// Add new components
						var numeroArchivo = 0;
						Ext.Array.each(
							archivos,
							me.createVistaArchivoAdjunto,
							{ mailform: me, numeroArchivo: numeroArchivo, fieldcontainer: cmp }
						);
						
						// Hide fieldcontainer if required
						if( me.editMode === true && me.hideArchivoAdjunto === false ){
							cmp.hide(); // cmp.hidden = true;
						} else if( me.hideVistaArchivosAdjuntos === false ){
							cmp.show(); // cmp.hidden = false;
						} else {
							cmp.hide(); // cmp.hidden = true;
						}
						// Hide fieldcontainer if requiered
						if( !archivos || archivos.length === 0){
							cmp.hide(); // cmp.hidden = true;
						}
						
						// Turn the suspendLayout flag off.
						cmp.suspendLayout = false;
						// Trigger a layout.
						cmp.doLayout();
						
					}
				},
				me.vistaArchivosAdjuntosCfg
			);
			cmp.name   	= 'vistaArchivosAdjuntos'; // Debug info: antes 'vistaArchivoAdjunto';
			cmp.itemId 	= 'vistaArchivosAdjuntos';
			cmp.items 	= [];
			if( me.editMode === true && me.hideArchivoAdjunto === false ){
				cmp.hidden = true;
			} else if( me.hideVistaArchivosAdjuntos === false ){
				cmp.hidden = false;
			} else {
				cmp.hidden = true;
			}
			
			var numeroArchivo = 0;
			Ext.Array.each(
				archivos,
				me.createVistaArchivoAdjunto,
				{ mailform: me, numeroArchivo: numeroArchivo, fieldcontainer: cmp }
			);
			
			if( !archivos || archivos.length == 0){
				cmp.hidden = true;
			}
			
			return cmp;
			
		},
		createRemitente: function(){
			var me  = this;
			var cmp = Ext.apply(
				{
					xtype:			'textarea',
					fieldLabel: 	'De',
					msgTarget:		'side',
					labelWidth: 	50,
					rows: 			1,
					height:			20,					
					allowBlank: 	false,
					vtype: 			'email',
					anchor:			'100%'  // anchor width by percentage
				}, 
				me.remitenteCfg 
			);
			cmp.name			= 'remitente';
			cmp.itemId		= 'remitente';
			if( me.hideRemitente === false ){
				cmp.hidden 	= false;
			} else {
				cmp.hidden 	= true;
			}
			// El componente obligatoriamente debe ser de solo lectura.
			cmp.readOnly 	=	true; 
			cmp.cls      	= 'readOnly';
			//cmp.validateOnBlur =	false; 
			return cmp;
		},
		createDestinatarios: function(){
			var me  = this;
			var cmp = Ext.apply(
				{
					xtype:		'textarea',
					fieldLabel: 'Para',
					msgTarget:	'side',
					labelWidth: 50,
					//rows: 		1,
					height:		34,
					allowBlank: false,
					vtype: 		'emaillist',
					anchor:		'100%',  // anchor width by percentage
					resizable:	{ handles: 's' }
				},
				me.destinatariosCfg 
			);
			cmp.name 			= 'destinatarios';
			cmp.itemId 			= 'destinatarios';
			if( me.hideDestinatarios === false ){
				cmp.hidden 		= false;
			} else {
				cmp.hidden 		= true;
			}
			if( me.editMode === true ){
				cmp.readOnly 	=	false; 
			} else {
				cmp.readOnly 	=	true; 
				cmp.cls      	= 'readOnly';
			}
			//cmp.validateOnBlur   =	false; 
			//cmp.validateOnChange =	false;
			return cmp;
		},
		createCcDestinatarios: function(){
			var me  = this;
			var cmp = Ext.apply(
				{
					xtype:		'textarea',
					fieldLabel: 'CC',
					msgTarget:	'side',
					labelWidth: 50,
					//rows: 		1,
					height:		34,
					allowBlank: true,
					vtype: 		'emaillist',
					anchor:		'100%',  // anchor width by percentage
					resizable:	{ handles: 's' }
				},
				me.ccDestinatariosCfg 
			);
			cmp.name 		= 'ccDestinatarios';
			cmp.itemId 		= 'ccDestinatarios';
			if( me.hideCcDestinatarios === false ){
				cmp.hidden 	= false;
			} else {
				cmp.hidden 	= true;
			}
			if( me.editMode === true ){
				cmp.readOnly =	false; 
			} else {
				cmp.readOnly =	true; 
				cmp.cls      = 'readOnly';
			}
			//cmp.validateOnBlur 	=	false; 
			//cmp.validateOnChange =	false;
			return cmp;
		},
		createBccDestinatarios: function(){
			var me  = this;
			var cmp = Ext.apply(
				{
					xtype:		'textarea',
					fieldLabel: 'BCC',
					msgTarget:	'side',
					labelWidth: 50,
					//rows: 		1,
					height:		34,
					allowBlank: true,
					vtype: 		'emaillist',
					anchor:		'100%',  // anchor width by percentage
					resizable:	{ handles: 's' }
				},
				me.bccDestinatariosCfg 
			);
			cmp.name 			= 'bccDestinatarios';
			cmp.itemId 			= 'bccDestinatarios';
			if( me.hideBccDestinatarios === false ){
				cmp.hidden 		= false;
			} else {
				cmp.hidden 		= true;
			}
			if( me.editMode === true ){
				cmp.readOnly 	=	false; 
			} else {
				cmp.readOnly 	=	true; 
				cmp.cls      	= 'readOnly';
			}
			//cmp.validateOnBlur 	=	false; 
			//cmp.validateOnChange =	false;
			return cmp;
		},
		createAsunto: function(){
			var me  = this;
			var cmp = Ext.apply(
				{
					xtype:		'textarea',
					msgTarget:	'side',
					labelWidth: 50,
					//rows: 		1,
					height:	20,
					allowBlank: true,
					fieldLabel: 'Asunto',
					anchor: 		'100%',  // anchor width by percentage
					resizable:	{ handles: 's' }					
				},
				me.asuntoCfg 
			);
			cmp.name 			= 'asunto';
			cmp.itemId 			= 'asunto';
			if( me.hideAsunto === false ){
				cmp.hidden 		= false;
			} else {
				cmp.hidden 		= true;
			}
			if( me.editMode === true ){
				cmp.readOnly 	= false; 
			} else {
				cmp.readOnly 	= true; 
				cmp.cls      	= 'readOnly';
			}
			//cmp.validateOnBlur =	false; 
			return cmp;
		},
		createCuerpoCorreo: function(){
			var me  = this;	
			var cmp = Ext.apply( 
				{
					xtype: 		'htmleditorfield', // TODO: Cambiar a htmleditorfield era htmleditor
					hideLabel: 	true,
					height:		230
					//anchor: 		'100% 100%'  // anchor width and height
				},
				me.cuerpoCorreoCfg
			);
			cmp.name 		= 'cuerpoCorreo';
			cmp.itemId 		= 'cuerpoCorreo';
			if( me.editMode === true && me.hideCuerpoCorreo === false ){
				cmp.hidden 	= false;
			} else {
				cmp.hidden 	= true;
			}
			//cmp.validateOnBlur =	false; 
			return cmp;
		},
		createVistaCuerpoCorreo: function(){
			var me  = this;	
			var cmp = Ext.apply(
				{
					xtype:			'htmleditorview',
					frame:			false,
					ownerCt: 		me, // requerido por el m�todo up
					margin:			'4 0 4 0',
					bodyPadding:	'23 23 23 23',
					value:			'',
					minHeight:		248
				},
				me.vistaCuerpoCorreoCfg
			);
			cmp.name 		= 'vistaCuerpoCorreo';
			cmp.itemId 		= 'vistaCuerpoCorreo';
			if( me.editMode === false && me.hideVistaCuerpoCorreo === false ){
				cmp.hidden 	= false;
			} else {
				cmp.hidden 	= true;
			}
			cmp.cls = 'readOnly';
			return cmp;
		},
		createBotonEnviarCorreo: function(){
			var me  = this;	
			var cmp = Ext.apply(
				{
					text: 	'Enviar',
					iconCls:	'icoBotonEnviarCorreo',
					handler:	function(button){
						var me = button.up('mailform');
						if( !me.isValid() ){
							return;
						}
						me.submit();
					}
				},
				me.botonEnviarCorreoCfg
			);
			cmp.name 		= 'botonEnviarCorreo';
			cmp.itemId 		= 'botonEnviarCorreo';
			if( me.editMode === true && me.hideBotonEnviarCorreo === false ){
				cmp.hidden 	= false;
			} else {
				cmp.hidden 	= true;
			}
			return cmp;
		},
		createBotonCerrar: function(){
			var me  = this;	
			var cmp = Ext.apply(
				{
					text: 	'Cerrar',
					iconCls:	'icoCerrar',
					handler: function(button){
						button.up('mailform').close();
					}
				},
				me.botonCerrarCfg
			);
			cmp.name   = 'botonCerrar';
			cmp.itemId = 'botonCerrar';
			if( me.hideBotonCerrar === false ){
				cmp.hidden = false;
			} else {
				cmp.hidden = true;
			}
			return cmp;
		},
		load: Ext.emptyFn // Se deja para una implementaci�n futura.
	}
);
