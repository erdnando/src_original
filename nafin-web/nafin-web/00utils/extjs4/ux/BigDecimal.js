   /**
	 *
	 * @docauthor Salim Hernandez <jshernandez@servicio.nafin.gob.mx>
	 *
	 * @class Ext.ux.form.BigDecimal
	 * @extends Ext.form.TextField
	 * <p>BigDecimal text field that provides automatic keystroke filtering, formatting, numeric validation, 
	 * and arbitrary-precision signed decimal numbers.</p>
	 * <p>Exponential number notation is disallowed.</p> 
	 * <p><pre><code>
	 
	 	// Define a comma separated bigdecimal "integer" field
		var registers = { 
			xtype: 			'bigdecimal',
			name: 			'registers',
			fieldLabel: 	'Number of records containing the application',
			blankText:		'Please capture the number of records',
			allowBlank: 	false,
			maxValue: 		'99,999',
			format:			'0,000'
		};
		
		// Define a money formatted bigdecimal field 
		var amount = { 
			xtype: 			'bigdecimal',
			name: 			'amountsSum',
			fieldLabel:		'Sum of amounts',
			blankText:		'Please capture the sum of amounts',
			allowBlank:		false,
			hidden: 			false,
			maxValue: 		'999,999,999,999,999,999.99',
			minValue: 		'0.00',
			format:			'$0,000.00 USD'
		};
		
</code></pre></p>
	 *
    * @constructor
    * <p>Creates a new BigDecimal Field</p>
	 * @xtype bigdecimal
	 * @author Salim Hernandez
	 * @since ( 06/06/2014 07:53:34 p.m. )
	 * @note Some code taken from ExtJS classes.
	 *
	 */
Ext.define('Ext.ux.BigDecimal', {
    extend:'Ext.form.field.Text',
    alias: 'widget.bigdecimalfield',
    xtype: 'bigdecimal',
    requires: [ 'Ext.util.Format' ],
    
    /**
     * @cfg {RegExp} stripCharsRe
     * @private
     */
     
    /**
	  * @cfg {RegExp} validateRe - Used for BigDecimal validation.
	  * @private
	  */
	  
    /**
     * @cfg {RegExp} maskRe
     * @private
     */
     
    /**
	  * @cfg {RegExp} thousandSeparatorRe - Used to strip thousand separator chars from number.
	  * It is generated automatically, but must be specified if unit chars have one of this chars: 1, 2, 3, 4, 5, 6, 7, 8, 9, '.', ',' and '#'.
	  * @private
	  */
	  
	 /**
	  * @cfg {RegExp} stripFormatUnitIniRe - Used to strip unit format chars from number start.
	  * It is generated automatically, but must be specified if unit chars have one of this chars: 1, 2, 3, 4, 5, 6, 7, 8, 9, '.', ',' and '#'.
	  * @private
	  */
	  
	 /**
	  * @cfg {RegExp} stripFormatUnitEndRe - Used to strip unit format chars chars from number end.
	  * @private
	  */
	  
    /**
	  * @cfg {String} style A custom style specification to be applied to this component's Element. BigDecimal 
	  * alignment to the right by default. 
	  */
	 fieldStyle: 'text-align:right;',
			
    /**
     * @cfg {Boolean} [allowDecimals=true]
     * False to disallow decimal values. If format atribute is specified this property is overriden.
     */
    allowDecimals: true,

    //<locale>
    /**
     * @cfg {String} decimalSeparator
     * Character(s) to allow as the decimal separator (defaults to '.'). If left unespecified it is overridden by locale file.
     */
     decimalSeparator: undefined, 
    //</locale>
    
    //<locale>
    /**
     * @cfg {String} thousandSeparator
     * Character(s) to allow as the group separator (defaults to ','). If left unespecified it is overridden by locale file.
     */
     thousandSeparator: undefined, 
    //</locale>
     
    //<locale>
    /**
     * @cfg {Boolean} [submitLocaleSeparator=true]
     * False to ensure that the {@link #getSubmitValue} method strips
     * always uses `.` as the separator, regardless of the {@link #decimalSeparator}
     * configuration.
     */
     submitLocaleSeparator: true,
    //</locale>

    /**
     * @cfg {Boolean} [submitRawValue=false]
     * True to ensure that the {@link #getSubmitValue} submits the formatted number.
     */
     submitRawValue: false,
     
    //<locale>
    /**
     * @cfg {Number} decimalPrecision
     * The maximum precision to display after the decimal separator
     */
    decimalPrecision: 2,  
    //</locale>

    //<locale>
    /**
	  * @cfg {String} format The way you would like to format this text. Examples (128453.789): 
	  *                        0          - (128454) show only digits, no precision
	  *								0.00       - (128453.79) show only digits, 2 precision
	  *								0.0000%    - (53.7890%) show only digits, 4 precision
	  *								$0,000     - ($128,454) show comma and digits, no precision
	  *								0,000.00   - (128,453.79) show comma and digits, 2 precision
	  *								$0,0.00    - ($128,453.79) shortcut method, show comma and digits, 2 precision
	  *								To reverse the grouping (,) and decimal (.) for international numbers, add /i to 
	  *                      the end. For example: 0.000,00/i
	  *                It uses the same formating representation as defined in Ext.util.Format.number,
	  *  					 but you can also specify unit chars: '$0,000.00', '0.00%' or '$0,000.00 USD',
	  *					 It is not allowed to use the following chars in the section of unit chars: digits, decimal point,
	  *					 thousand separator or number sign, if required stripFormatUnitIniRe or stripFormatUnitEndRe
	  *					 must be defined.
	  */
	format:				 undefined,
	//</locale>
	
    /**
     * @cfg {Number} minValue
     * The minimum allowed value. Will be used by the field's validation logic if defined.
     */
     
    /**
     * @cfg {Number} maxValue
     * The maximum allowed value. Will be used by the field's validation logic if defined.
     */
    //<locale>
    /**
     * @cfg {String} minText
     * Error text to display if the minimum value validation fails.
     */
    minText: 'The minimum value for this field is {0}',
    //</locale>

    //<locale>
    /**
     * @cfg {String} maxText
     * Error text to display if the maximum value validation fails.
     */
    maxText: 'The maximum value for this field is {0}',
    //</locale>

    //<locale>
    /**
     * @cfg {String} nanText
     * Error text to display if the value is not a valid number. For example, this can happen if a valid character like
     * '.' or '-' is left in the field with no number.
     */
    nanText: '{0} is not a valid number',
    //</locale>

    //<locale>
    /**
     * @cfg {String} negativeText
     * Error text to display if the value is negative and {@link #minValue} is set to 0. This is used instead of the
     * {@link #minText} in that circumstance only.
     */
    negativeText: 'The value cannot be negative',
    //</locale>

    /**
     * @cfg {String} baseChars
     * The base set of characters to evaluate as valid numbers.
     */
    baseChars: '0123456789',

    /**
     * @cfg {Boolean} autoStripChars
     * True to automatically strip not allowed characters from the field.
     */
    autoStripChars: false,
    initComponent: function() {
    	  
        var me 			= this;
        var UtilFormat 	= Ext.util.Format;
        
        // Initialize thousand separator using locale
        if( !me.thousandSeparator ){
        	  me.thousandSeparator = UtilFormat.thousandSeparator;
        }
        // Initialize decimal separator using locale
        if( !me.decimalSeparator ){
        	  me.decimalSeparator  = UtilFormat.decimalSeparator;
        }
        // Build regex to help remove thousand separator char from strings
        me.thousandSeparatorRe = new RegExp(Ext.String.escapeRegex(me.thousandSeparator),"g");
        // Build regex for validating big decimal numbers
        me.validateRe 	  		 = new RegExp('^[\-]?[' + me.baseChars + ']+[' + Ext.String.escapeRegex(me.decimalSeparator) + ']?[' + me.baseChars + ']*$');
		  // Build regex to help remove unit format chars 
		  var auxFormat			=  me.format && me.format.substr(me.format.length - 2) == '/i'?me.format.substr(0, me.format.length - 2):me.format;
		  var extraChars 			=  auxFormat ? auxFormat.split(/[\d\.,#]+/g):[];
        var matchExtraChars	=  auxFormat ? auxFormat.match(/[\d\.,#]+/) :{};

        if( extraChars.length == 0 ){
        	  extraChars.push("","");
        } else if( extraChars.length == 1 && matchExtraChars.index === 0 ){
        	  extraChars.unshift("");
        } else if( extraChars.length == 1 && matchExtraChars.index >   0 ){
        	  extraChars.push("");
        } 
         	
        if( extraChars.length > 2 ){
        	  //<debug>
           Ext.Error.raise({
              sourceClass:  "Ext.ux.BigDecimal",
              sourceMethod: "initComponent",
              format: 		 me.format,
              msg: "Invalid number format, should have no more than 1 decimal separator"
           });
           //</debug>
        }
         	
        if( !me.stripFormatUnitIniRe && extraChars[0] ){
        	  extraChars[0] = Ext.String.escapeRegex(extraChars[0]);
        	  me.stripFormatUnitIniRe = new RegExp( "^" + extraChars[0] );
        }
         	
        if( !me.stripFormatUnitEndRe && extraChars[1] ){
        	  extraChars[1] = Ext.String.escapeRegex(extraChars[1]);
        	  me.stripFormatUnitEndRe = new RegExp( extraChars[1] + "$" );
        }
        
        // If format especified, obtain decimal precision.
        if( auxFormat ){
        	  var auxFormat01 = auxFormat.replace(me.stripFormatUnitEndRe,'');
        	  var index 		= auxFormat01.indexOf(me.decimalSeparator);
			  if( index != -1 ){
			  	  me.decimalPrecision = auxFormat01.length - ( index + 1 );
			  } else {
				  me.decimalPrecision = 0;
			  }
		  }
		  
        me.callParent();
        me.setMinValue(me.minValue);
        me.setMaxValue(me.maxValue);

    },

    /**
     * Runs all of Number's validations and returns an array of any errors. Note that this first runs Text's
     * validations, so the returned array is an amalgamation of all field errors. The additional validations run test
     * that the value is a number, and that it is within the configured min and max values.
     * @param {Object} [value] The value to get errors for (defaults to the current field value)
     * @return {String[]} All validation errors for this field
     */
    getErrors: function(value) {

        var me = this,
            errors = me.callParent(arguments),
            format = Ext.String.format,
            num, 
            rawValue;

        rawValue 	= Ext.isDefined(value) ? value : this.getRawValue(); 
        value 		= this.processRawValue( rawValue );
        
        if (value.length < 1) { // if it's blank and textfield didn't flag it then it's valid
             return errors;
        }
         
        if( me.isNaN(value) ){ // isNaN(value)
            errors.push(format(me.nanText, rawValue));
            return errors;
        }

        num = me.parseValue(value); 
		  var minimumValue = me.parseValue(this.processRawValue(me.minValue)); // This allows to present a formatted number
		  var maximumValue = me.parseValue(this.processRawValue(me.maxValue)); // This allows to present a formatted number
		  
		  if ( me.minValue && me.compareBigDecimals( num, minimumValue ) < 0 ) { 
        	  if( me.compareBigDecimals( minimumValue, '0' ) == 0 ){
        	  	  errors.push(this.negativeText);
        	  } else {
        	  	  errors.push(format(this.minText, me.minValue));
           }
        }

        if ( me.maxValue && me.compareBigDecimals( num, maximumValue ) > 0 ) { 
            errors.push(format(this.maxText, me.maxValue)); 
        }

        return errors;
        
    },

    rawToValue: function(rawValue) {

    	 var value = this.processRawValue(rawValue);
       var value = this.fixPrecision(this.parseValue(value));
       if (value === null) {
       	  value = rawValue || null; 
       }
       return  value;
    },

    /**
     * Performs any necessary manipulation of a raw String value to prepare it for conversion and/or
     * {@link #validate validation}. For text fields this applies the configured {@link #stripCharsRe}
     * to the raw value.
     * @param {String} value The unprocessed string value
     * @return {String} The processed string value
     */
    processRawValue: function(value) {

    	 if( Ext.isDefined(value) ){

    	 	 value = String(value);
    	 	 
			 if( this.stripFormatUnitIniRe ){
				 value = value.replace( this.stripFormatUnitIniRe, "");
			 }
			 if( this.stripFormatUnitEndRe ){
				 value = value.replace( this.stripFormatUnitEndRe, "");
			 }
			 // TODO: Add functionality when submitLocaleSeparator=false, to interpret decimal separator: '.' and
			 // thousandSeparator as ','.
			 /*
			 if( this.thousandSeparatorRe && this.submitLocaleSeparator == false ){
			 	  var fnum  = value;
				  fnum 		= fnum.split('.');
        	  	  value    	= fnum[0].replace(',',""); 
        	 	  if( fnum.length > 1 ){
        	  	  		value += this.decimalSeparator + fnum[1];
        	  	  }
        	 } else if( this.thousandSeparatorRe ){
				 value = value.replace( this.thousandSeparatorRe, ''); 
			 }
			 */
			 if( this.thousandSeparatorRe ){
				 value = value.replace( this.thousandSeparatorRe, ''); 
			 }
			 
		 }

    	 return this.callParent(arguments);
    	 
    },
    
    valueToRaw: function(value) {

        var me = this;
       
        value  = me.processRawValue(value);
        value 	= me.parseValue(value); 
        value 	= me.fixPrecision(value);
        value  = me.formatBigDecimal(value, me.format)

        return value;
    },
    
    getSubmitValue: function(){
        var me 	= this,
            value = me.callParent();
        
        if( me.submitRawValue ){
        	  value = me.getRawValue();
        	  if( !me.submitLocaleSeparator){
        	  	  var fnum = value;
        	  	  // Supress unit chars
        	  	  var unitIni;
        	  	  if( me.stripFormatUnitIniRe ){
        	  	  	  var match = fnum.match(me.stripFormatUnitIniRe);
        	  	  	  if( match ){
        	  	  	  	  unitIni = match[0];
        	  	  	  	  fnum	 = fnum.replace(me.stripFormatUnitIniRe,"");
        	  	  	  }
        	  	  }
        	  	  var unitEnd;
        	  	  if( me.stripFormatUnitEndRe ){
        	  	  	  var match = fnum.match(me.stripFormatUnitEndRe);
        	  	  	  if( match ){
        	  	  	  	  unitEnd = match[0];
        	  	  	  	  fnum	 = fnum.replace(me.stripFormatUnitEndRe,"");
        	  	  	  }
        	  	  }
        	  	  // Change format: use ',' as thousand separator and '.' as decimal separator.
        	  	  fnum 		= fnum.split(me.decimalSeparator);
        	  	  value    	= fnum[0].replace( me.thousandSeparatorRe,','); 
        	 	  if( fnum.length > 1 ){
        	  	  		value += '.' + fnum[1];
        	  	  }
        	  	  // Add Stripped Unit Chars
        	  	  if( !Ext.isEmpty(unitIni) ){
        	  	  	  value = unitIni + value;
        	  	  }
        	  	  if( !Ext.isEmpty(unitEnd) ){
        	  	  	  value = value + unitEnd;
        	  	  }
        	  }
        } else if (!me.submitLocaleSeparator) {
        	  value = value.replace(me.decimalSeparator, '.');
        }

        return value;
        
    },

    /**
     * Replaces any existing {@link #minValue} with the new value.
     * @param {Number} value The minimum value
     */
    setMinValue: function(value) {

        var me = this,
            allowed,
            auxMinValue;
        
        auxMinValue = me.processRawValue(value);
        auxMinValue = me.parseValue(auxMinValue);
        me.minValue = me.formatBigDecimal(auxMinValue, me.format);
        
        // Build regexes for masking and stripping based on the configured options
        if (me.disableKeyFilter !== true) {
           allowed = me.baseChars + '';

           if (me.allowDecimals) {
           	  allowed += me.decimalSeparator;
           }
           if (me.thousandSeparator) {
           	  allowed += me.thousandSeparator;
           }

           if ( auxMinValue && me.compareBigDecimals(auxMinValue,'0') < 0) { 
           	  allowed += '-';
           }
            
           allowed = Ext.String.escapeRegex(allowed);
           me.maskRe = new RegExp('[' + allowed + ']');
           if (me.autoStripChars) {
           	  me.stripCharsRe = new RegExp('[^' + allowed + ']', 'gi');
           }
        }
                
    },

    /**
     * Replaces any existing {@link #maxValue} with the new value.
     * @param {Number} value The maximum value
     */
    setMaxValue: function(value) {
    	 value = this.processRawValue(value);
    	 this.maxValue = this.formatBigDecimal(this.parseValue(value), this.format);
    },

    /**
     * @private
     */
    parseValue: function(value) {

		if( !this.isBigDecimal(value)){
			return null;
		}
		
		// Remove zeroes to the right
		var neg 	= value.charAt(0) === '-'?true:false;
		value 	= neg?value.substring(1):value;
		if( value.match("^[0]{2,}["+Ext.String.escapeRegex(this.decimalSeparator)+"]["+this.baseChars+"]*$") ){
			value = value.replace(/^[0]{2,}/,"0");
		} else if( value.match("^[0]{2,}$") ){
			value = value.replace(/^[0]{2,}/,"0");
		} else if( value.match("^[0]{2,}") ){
			value = value.replace(/^[0]{2,}/,"");
		}
		value 	= ( neg?'-':'') + value;
		return value;
				
   },
   
   /**
    * @private
    */
   fixPrecision: function(value) {

        var me = this,
        nan = me.isNaN(value),
            precision = me.decimalPrecision; 

        if (nan || !value) {
            return nan ? '' : value;
        } else if (!me.allowDecimals || precision <= 0) {
            precision = 0;
        }

        return me.toFixed(value, precision, me.decimalSeparator);
    },

    beforeBlur: function() {
        var me = this, v;
        v = me.processRawValue(me.getRawValue());
        v = me.parseValue(v);

        if (!Ext.isEmpty(v)) {
            me.setValue(v);
        }
    },
        
	/**
	 * @private
	 * Returns true if the passed value is a number. Returns false for non-finite numbers.
	 * @param {String} bigNumberValue The value to test
	 * @return {Boolean}
	 */
	isBigDecimal: function(bigNumberValue){

		 var me = this;
		 if( !me.validateRe.test(bigNumberValue) ){
	 	 	 return false;
	 	 }
				
	 	 return true;
				
	},
	
	/**
	 * @private
	 * Determines whether a value is an illegal number (Not-a-Number).
	 * @param {String} The value to be tested.
	 * @return {Boolean} Returns "true" if the value is NaN, otherwise it returns false.
	 */
	isNaN: function(value){

		if( value === NaN ){
			return true;
		}
		
	 	return !this.isBigDecimal(value);
				
	},
	
	/**
	 * @cfg {formatFns} Cache ofg number formatting functions keyed by format string
    * @private
    */
   formatFns: {},
   
	/**
    * Formats the passed number according to the passed format string.
    *
    * The number of digits after the decimal separator character specifies the number of
    * decimal places in the resulting string. The *local-specific* decimal character is
    * used in the result.
    *
    * The *presence* of a thousand separator character in the format string specifies that
    * the *locale-specific* thousand separator (if any) is inserted separating thousand groups.
    *
    * By default, "," is expected as the thousand separator, and "." is expected as the decimal separator.
    *
    * ## New to Ext JS 4
    *
    * Locale-specific characters are always used in the formatted output when inserting
    * thousand and decimal separators.
    *
    * The format string must specify separator characters according to US/UK conventions ("," as the
    * thousand separator, and "." as the decimal separator)
    *
    * To allow specification of format strings according to local conventions for separator characters, add
    * the string `/i` to the end of the format string.
    *
    * examples (123456.789):
    * 
    * - `0` - (123456) show only digits, no precision
    * - `0.00` - (123456.78) show only digits, 2 precision
    * - `0.0000` - (123456.7890) show only digits, 4 precision
    * - `0,000` - (123,456) show comma and digits, no precision
    * - `$0,000.00 USD` - ($123,456.78 USD) show comma and digits, 2 precision, and unit chars.
    * - `0,0.00` - (123,456.78) shortcut method, show comma and digits, 2 precision
    * - `0.####` - (123,456,789) Allow maximum 4 decimal places, but do not right pad with zeroes
    *
    * @param {Number} v The number to format.
    * @param {String} format The way you would like to format this text.
    * @return {String} The formatted number.
    */
    formatBigDecimal: function(v, formatString) {
    	  var me = this;
        if (!formatString) {
            return v;
        }
        var formatPattern 	= /[\d,\.#]+/; 
         // A RegExp to remove from a number format string, all characters except digits and '.'
        var formatCleanRe  = /[^\d\.#]/g;
        var allHashes     	= /^#+$/; 
        var formatFn 		= me.formatFns[formatString]; 
        var UtilFormat 		= Ext.util.Format,
        // A RegExp to remove from a number format string, all characters except digits and the local decimal separator.
        // Created on first use. The local decimal separator character must be initialized for this to be created.
        I18NFormatCleanRe;
        
        // Generate formatting function to be cached and reused keyed by the format string.
        // This results in a 100% performance increase over analyzing the format string each invocation.
        if (!formatFn) {

            var originalFormatString 	= formatString,
                comma 						= me.thousandSeparator,
                decimalSeparator 		= me.decimalSeparator,
                hasComma,
                splitFormat,
                extraChars,
                precision = 0,
                multiplier,
                trimTrailingZeroes,
                code;

            // The "/i" suffix allows caller to use a locale-specific formatting string.
            // Clean the format string by removing all but numerals and the decimal separator.
            // Then split the format string into pre and post decimal segments according to *what* the
            // decimal separator is. If they are specifying "/i", they are using the local convention in the format string.
            if (formatString.substr(formatString.length - 2) == '/i') { // => se consideran los separadores de la implementación local
                if (!I18NFormatCleanRe) {
                    I18NFormatCleanRe = new RegExp('[^\\d\\' + UtilFormat.decimalSeparator + ']','g');
                }
                formatString 	= formatString.substr(0, formatString.length - 2);
                hasComma 		= formatString.indexOf(comma) != -1;
                splitFormat 	= formatString.replace(I18NFormatCleanRe, '').split(decimalSeparator);
            } else {
                hasComma 		= formatString.indexOf(',') != -1;
                splitFormat 	= formatString.replace(formatCleanRe, '').split('.');
            }
            extraChars = formatString.replace(formatPattern, ''); 

            if (splitFormat.length > 2) {
                //<debug>
                Ext.Error.raise({
                    sourceClass: "Ext.ux.BigDecimal",
                    sourceMethod: "formatBigDecimal",
                    value: v,
                    formatString: formatString,
                    msg: "Invalid number format, should have no more than 1 decimal"
                });
                //</debug>
            } else if (splitFormat.length === 2) {
                precision = splitFormat[1].length;

                // Formatting ending in .##### means maximum 5 trailing significant digits
                trimTrailingZeroes = allHashes.test(splitFormat[1]);
            }
            
            // The function we create is called immediately and returns a closure which has access to vars and some fixed values; RegExes and the format string.
            code = [
            	'var utilFormat=Ext.util.Format,me=bigdecimal,neg,fnum,parts' + 
                    (hasComma ? ',thousandSeparator,thousands=[],j,n,i' : '') +
                    (extraChars  ? ',formatString="' + formatString + '",formatPattern=/[\\d,\\.#]+/' : '') +
                    (trimTrailingZeroes ? ',trailingZeroes=/'+Ext.String.escapeRegex(me.decimalSeparator)+'?0+$/;' : ';') +
                'return function(v){' + 
                'if(!me.isBigDecimal(v))return"";' +
                'neg = v.charAt(0) === \'-\'?true:false;',
                'v   = neg?v.substring(1):v;',
                'fnum=me.toFixed(v,' + precision + ',utilFormat.decimalSeparator);'
            ];
            
            if (hasComma) {
                // If we have to insert commas...
                
                // split the string up into whole and decimal parts if there are decimals
                if (precision) {
                    code[code.length] = 'parts=fnum.split(me.decimalSeparator);';
                    code[code.length] = 'fnum=parts[0];';
                }
                code[code.length] = 
                'if( fnum.length > 3 ) {'; // => v >= 1000
                        code[code.length] = 'thousandSeparator=utilFormat.thousandSeparator;' +
                        'thousands.length=0;' +
                        'j=fnum.length;' +
                        'n=fnum.length%3||3;' +
                        'for(i=0;i<j;i+=n){' +
                            'if(i!==0){' +
                                'n=3;' +
                            '}' +
                            'thousands[thousands.length]=fnum.substr(i,n);' +
                        '}' +
                        'fnum=thousands.join(thousandSeparator);' + 
                    '}';
                if (precision) {
                    code[code.length] = 'fnum += utilFormat.decimalSeparator+parts[1];';
                }
                
            } else if (precision) {
                // If they are using a weird decimal separator, split and concat using it
                code[code.length] = 'if(utilFormat.decimalSeparator!=="."){' +
                    'parts=fnum.split(utilFormat.decimalSeparator);' +
                    'fnum=parts[0]+utilFormat.decimalSeparator+parts[1];' +
                '}';
            }

            if (trimTrailingZeroes) {
                code[code.length] = 'fnum=fnum.replace(trailingZeroes,"");';
            }

            /*
             * Edge case. If we have a very small negative number it will get rounded to 0,
             * however the initial check at the top will still report as negative. Replace
             * everything but 1-9 and check if the string is empty to determine a 0 value.
             */
             code[code.length] = 'if(neg&&fnum!=="' + ( precision  && !trimTrailingZeroes ? '0.' + Ext.String.repeat('0', precision) : '0') + '")fnum="-"+fnum;';

             code[code.length] = 'return ';

            // If there were extra characters around the formatting string, replace the format string part with the formatted number.
            if (extraChars) {
            	code[code.length] = 'formatString.replace(formatPattern, fnum);';
            } else {
                code[code.length] = 'fnum;';
            }
            code[code.length] = '};'
            
            formatFn = me.formatFns[originalFormatString] = Ext.functionFactory('Ext', 'bigdecimal', code.join(''))(Ext,this);
        }
        return formatFn(v);
  },
  
  /**
   * @private
   * Converts a BigDecimal number into a string with a specified number of decimals.
   * If the desired number of decimals are higher than the actual number, zeroes are added 
   * to create the desired decimal length. If the desired number of decimals are lower than 
   * the actual number, it gets rounded.
   *
   * @param {String} v The number to fix precision.
   * @param {String} x The number of digits after the decimal point. Default is 0 (no 
   *                   digits after the decimal point)
   * @param {String} dec The decimal separator character.
   * @return (String) The number, with the exact number of decimals.
   */
  toFixed: function( v, x, dec ){

       if( x < 0){
            throw "RangeError: toFixed(v,x,dec) x digits argument must be grater or equal to zero";
       }
       
       var neg   = v.charAt(0) === '-'?true:false;
       v          = neg?v.substring(1):v;
       
       var aux         = v.split(dec);
       var integerPart = aux.length > 0 && aux[0].length > 0?aux[0].split(""):new Array("0");
       var decimalPart = aux.length > 1 && aux[1].length > 0?aux[1].split(""):new Array("0");
 
       while( decimalPart.length < x + 1 ){
            decimalPart.push("0");
       }
       
       var lastDigit = parseInt(decimalPart[x],10);
       decimalPart   = decimalPart.slice(0,x);
      
       // Check if rounding up applies
       if( lastDigit >= 5 ){
            // Convert the decimal part characters to numbers
            for(var i=0;i<decimalPart.length;i++){
                 decimalPart[i] = parseInt(decimalPart[i],10);
            }
            // Convert the integer part characters to numbers
            for(var i=0;i<integerPart.length;i++){
                 integerPart[i] = parseInt(integerPart[i],10);
            }
            // Apply rounding up starting from decimal part
            var accumulated = 1;
            for(var i=x-1; accumulated > 0 && i>=0;i--){
                 var sum = decimalPart[i] + accumulated;
                 if( sum > 9 ){
                      var auxSum      = (new String(sum)).split("");
                      decimalPart[i] = parseInt(auxSum[1],10);
                      accumulated    = parseInt(auxSum[0],10);
                 } else {
                      decimalPart[i] = sum;
                      accumulated    = 0
                 }
            }
            // Apply rounding up to integer part if required.
            for(var i=integerPart.length-1; accumulated > 0 && i>=0;i--){
                 var sum = integerPart[i] + accumulated;
                 if( sum > 9 ){
                      var auxSum      = (new String(sum)).split("");
                      integerPart[i] = parseInt(auxSum[1],10);
                      accumulated    = parseInt(auxSum[0],10);
                 } else {
                      integerPart[i] = sum;
                      accumulated    = 0
                 }
            }
            // If something left over, add it as the most significant digit of the integer part
            if( accumulated > 0 ){
                 integerPart.unshift(accumulated);
            }
            
       }
       
       return (neg?'-':'')+integerPart.join("") + (x > 0? dec + decimalPart.join(""):"");
       
  },
  
  /**
   * @private
   * Compares two BigDecimal. Two BigDecimal objects that are equal in value but have a different scale 
   * (like 1.0 and 1.00) are considered equal by this method.
   *
   * @param {String} number1 - First  BigDecimal to compare.
   * @param {String} number2 - Second BigDecimal to compare.
   *
   * @return (Number) -1, 0, or 1 as the first BigDecimal is numerically less than, equal to, or greater 
   *                  than the second BigDecimal.
   */
  compareBigDecimals: function(number1,number2){

       var aux;
 
       var negNumber1     = number1.charAt(0) === '-'?true:false;
       var negNumber2      = number2.charAt(0) === '-'?true:false;
       
       number1             = negNumber1?number1.substring(1):number1;
       number2             = negNumber2?number2.substring(1):number2;
                 
       if( negNumber1 && number1.match("^[0]+[\.]?[0]*$") ){ // Negative zero
            negNumber1 = false;
       }
       if( negNumber2 && number2.match("^[0]+[\.]?[0]*$") ){ // Negative zero
            negNumber2 = false;
       }
       
       // number1 > number2
       if(  !negNumber1  && negNumber2 ){
            return 1;
       } 
       
       // number1 < number2
       if( negNumber1  &&  !negNumber2 ){
            return -1
       }
       
       aux = number1.split(this.decimalSeparator);
       var integerNumber1 = aux.length > 0 && aux[0].length > 0?aux[0].split(""):new Array("0");
       var decimalNumber1 = aux.length > 1 && aux[1].length > 0?aux[1].split(""):new Array("0");
       
       aux = number2.split(this.decimalSeparator);
       var integerNumber2 = aux.length > 0 && aux[0].length > 0?aux[0].split(""):new Array("0");
       var decimalNumber2 = aux.length > 1 && aux[1].length > 0?aux[1].split(""):new Array("0");
 
       // Adjust numbers size, so that they are equal.
       while( integerNumber1.length > integerNumber2.length ){
            integerNumber2.unshift(0);
       } 
       while( integerNumber2.length > integerNumber1.length ){
            integerNumber1.unshift(0);
       }
       while( decimalNumber1.length > decimalNumber2.length ){
            decimalNumber2.push(0);
       } 
       while( decimalNumber2.length > decimalNumber1.length ){
            decimalNumber1.push(0);
       }
          
       // Since the decimal point is irrelevant, join both parts.
       var n1             = new Array();
       var n2             = new Array();
       // Convert to number the integer part characters.
       for(var i=0;i<integerNumber1.length;i++){
            n1[i]                         = parseInt(integerNumber1[i],10);
       }
       // Convert to number the decimal part characters.
       for(var i=0;i<decimalNumber1.length;i++){
            n1[i+integerNumber1.length] = parseInt(decimalNumber1[i],10);
       }
       // Convert to number the integer part characters.
       for(var i=0;i<integerNumber2.length;i++){
            n2[i]                         = parseInt(integerNumber2[i],10);
       }
       // Convert to number the decimal part characters.
       for(var i=0;i<decimalNumber2.length;i++){
            n2[i+integerNumber2.length] = parseInt(decimalNumber2[i],10);
       }
 
       // number1 > number2
       for(var i=0;i<n1.length;i++){
            if( n1[i] > n2[i] ){
                 return (negNumber1 && negNumber2)?-1:1;
            } else if( n1[i] < n2[i] ){
                 break;  
            }
       }
       // number1 < number2
       for(var i=0;i<n1.length;i++){
            if( n1[i] < n2[i] ){
                 return (negNumber1 && negNumber2)?1:-1;
            } else if( n1[i] > n2[i] ){
                 break;  
            }
       }
       // number1 == number2
       return 0; 
       
  }

});
