<%@	page import = "
		com.nafin.security.UserNE,
		com.nafin.security.UserSignOnNE,
		java.io.BufferedInputStream, 
		java.io.BufferedOutputStream,
		java.io.BufferedReader,
		java.io.BufferedWriter,
		java.io.File,
		java.io.FileInputStream,
		java.io.FileOutputStream,
		java.io.FileReader,
		java.io.FileWriter,
		java.io.InputStream,
		java.text.DecimalFormat,
		java.text.NumberFormat,
		java.util.HashMap,
		java.util.Iterator,
		java.util.List,
		java.util.Map,
		java.util.zip.ZipEntry,
		java.util.zip.ZipFile,
		java.util.zip.ZipInputStream,
		java.util.zip.ZipOutputStream,
		org.apache.commons.fileupload.FileItem,
		org.apache.commons.fileupload.FileItemFactory,
		org.apache.commons.fileupload.disk.DiskFileItemFactory,
		org.apache.commons.fileupload.servlet.ServletFileUpload"
	errorPage = "/00utils/error.jsp" buffer="16kb" autoFlush="true"%>
<%
UserNE		userNE	= session.getAttribute("userNE")==null?new UserNE():(UserNE)session.getAttribute("userNE");
UserSignOnNE 	userSO	= new UserSignOnNE();
boolean         ursOK   = userSO.validUserSign(userNE, userSO.sign(userNE, userNE.getNe_perfil()));

response.setHeader("Cache-Control", "no-cache");
response.setHeader("Pragma", "no-cache");
response.setHeader("Expires", "Thu, 29 Oct 1969 17:04:19 GMT");
			
Map 	hiddens 		= new HashMap();
String 	rutaAplicacion 	= getServletConfig().getServletContext().getRealPath("/");
String 	rutaArchivos 	= rutaAplicacion+File.separatorChar+"00tmp"+File.separatorChar+"15cadenas"+File.separatorChar;
		rutaAplicacion 	= rutaAplicacion.substring(0, rutaAplicacion.lastIndexOf("nafin"));
String 	rutaAbsoluta 	= "";
String 	archivoProcesar	= "";

boolean isMultipart = ServletFileUpload.isMultipartContent(request);
if(isMultipart) {
	FileItemFactory 	factory 	= new DiskFileItemFactory();
	ServletFileUpload 	upload 		= new ServletFileUpload(factory);
	List 				items 		= upload.parseRequest(request);
	Iterator 			iter 		= items.iterator();
	while (iter.hasNext()) {
		FileItem 	item 	= (FileItem) iter.next();
		String 		name 	= item.getFieldName();
		String 		value 	= item.getString();
		if (item.isFormField()) {
			hiddens.put(name, value);
		} else {
			String 	nombreCampo 	= item.getFieldName();
			long 	tamanioArchivo 	= item.getSize();
			String 	archivoOrigen 	= item.getName();
			String 	contentType 	= item.getContentType();
			String 	nombreArchivo	= archivoOrigen;
			if(archivoOrigen.lastIndexOf("/")>0) {
				nombreArchivo	= archivoOrigen.substring(archivoOrigen.lastIndexOf("/")+1);
			} else if(archivoOrigen.lastIndexOf("\\")>0) {
				nombreArchivo	= archivoOrigen.substring(archivoOrigen.lastIndexOf("\\")+1);
			}
					archivoProcesar = nombreArchivo.substring(0, nombreArchivo.lastIndexOf("."));
			String 	extension 		= archivoOrigen.substring(archivoOrigen.indexOf("."));			
			File 	archivo 		= new File(rutaArchivos + File.separatorChar + nombreArchivo);
			item.write(archivo);
			rutaAbsoluta			= archivo.getAbsolutePath();
		}
	}//while (iter.hasNext())
}//if(isMultipart)
String 			hidAction 	= (hiddens.get("hidAction")==null)?"":hiddens.get("hidAction").toString().trim();
%>
<html>
<head>
<title>.:: ADMIN-NAFIN-ZIP ::.</title>
<link rel="Stylesheet" type="text/css" href="/nafin/home/css/nafinet.css">
<script language="Javascript" src="../00utils/valida.js"></script>
<script language="javascript">
<!--
<%if(!"ADMIN".equals(userNE.getNe_perfil())) {%>
document.onkeydown = 
	function() {
		if(window.event && window.event.keyCode == 116) {
			window.event.keyCode = 0;
			return false;
		}
	}

if (window.Event) 
	document.captureEvents(Event.MOUSEUP); 
function nocontextmenu() { 
	event.cancelBubble = true 
	event.returnValue = false; 
	return false; 
} 
function norightclick(e) { 
	if(window.Event) { 
		if(e.which !=1)
			return false; 
	} else if (event.button !=1) { 
		event.cancelBubble = true 
		event.returnValue = false; 
		return false; 
	}
}
document.oncontextmenu = nocontextmenu; 
document.onmousedown = norightclick;
<%}//if(!"ADMIN".equals(userNE.getNe_perfil()))%>

function procesar() {
	var f = document.frmContenido;
	f.hidAction.value = "P";
	f.action = "00viewercc.jsp";
	f.target= "_self";
	f.submit();
}

function setPage() {
	var f = document.frmContenido;
	parent.facultad.location.replace(f.cmb_page.value);	
}
//-->
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="frmContenido" method="post" action="00viewercc.jsp" enctype="multipart/form-data">
<input type="hidden" name="hidAction" value="<%=hidAction%>">
<table width="100%" cellpadding="1" cellspacing="2" border="0">
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>
	<table align="center">
	<tr>
		<td class="formas" align="right">CC File:</td>
		<td>
			<input 
				type		= "file" 
				class		= "formas" 
				name		= "txt_path" 
				value		= "Examinar" 
				size		= "45">
		</td>
	</tr>
	</table>
	</td>
</tr>
<tr>
	<td align="center">
		<input 
			type 		= "Button" 
			class 		= "celda02" 
			value 		= "Process" 
			title		= "Process" 
			onclick 	= "procesar()">
		<input 
			type 		= "Button" 
			class 		= "celda02" 
			value 		= "Cancel" 
			title 		= "Cancel" 
			onclick 	= "window.location.href='00viewercc.jsp'">
	</td>
</tr>
<%
if("P".equals(hidAction)) {
%>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td align="center">
		<table cellpadding="2" cellspacing="0" border="1" bordercolor="#A5B8BF" width="80%">
		<tr>
			<td class="celda02" align="center">Name</td>
			<td class="celda02" align="center">Size</td>
			<td class="celda02" align="center">Date</td>
		</tr>
<% 		BufferedReader 	bufferedreader 	= new BufferedReader(new FileReader(rutaAbsoluta));
		BufferedWriter 	bufferedwriter 	= new BufferedWriter(new FileWriter(rutaArchivos+archivoProcesar+".csv"));
		
		ZipOutputStream zipOutputStream = 
							new ZipOutputStream(
								new BufferedOutputStream(
									new FileOutputStream(rutaArchivos+archivoProcesar+".zip")));
		
		String line = bufferedreader.readLine().trim();
		NumberFormat formatter = new DecimalFormat("###,###,###,###,###,###,###");
		
		while(line!=null) {
			File 	file 			= new File(rutaAplicacion+line);
			String	tamano			= String.valueOf(file.length());
			String 	fecha 			= new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm").format(new java.util.Date(file.lastModified()));
			long	lastmodified 	= file.lastModified();
			String 	nombre 			= "";
			if(file.exists()) {
				nombre 	= line;
				
				String 	zipPath 		= file.getAbsolutePath();
				zipPath = zipPath.substring(zipPath.indexOf("nafin")+6, zipPath.length());
				
				byte data[] = new byte[4096];
				BufferedInputStream bufferedInputStream = 
										new BufferedInputStream(
											new FileInputStream(file));
				ZipEntry zipEntry = new ZipEntry(zipPath);
				zipEntry.setTime(lastmodified);
				zipOutputStream.putNextEntry(zipEntry);
				int count;
				while( ( count = bufferedInputStream.read(data, 0, data.length ) ) != -1 ) {
					zipOutputStream.write(data, 0, count);
				}
				bufferedInputStream.close();
				
			} else {
				nombre 	= "(*)"+line;
				tamano	= "";
				fecha 	= "";
			}
			bufferedwriter.write(nombre+","+tamano+","+fecha+"\n");%>
			<tr>
				<td class="formas" align="left">
					<%=nombre%>
				</td>
				<td class="formas" align="right">
					<%="".equals(tamano)?"&nbsp;":formatter.format(new Long(tamano))%>
				</td>
				<td class="formas" align="center">
					<%="".equals(fecha)?"&nbsp;":fecha%>
				</td>
			</tr>
<%			line = bufferedreader.readLine();
		}//while(line!=null)
		bufferedwriter.close();
		zipOutputStream.close();%>
		</table>
	</td>
</tr>

<%if(!"".equals(archivoProcesar)) {
	String zipFile = "/nafin/00tmp/15cadenas/"+archivoProcesar+".zip";
	String csvFile = "/nafin/00tmp/15cadenas/"+archivoProcesar+".csv";%>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="right">
		<table>
			<tr>
				<td align="right" class="formas">
					<input 
						type 	= "Button" 
						class 	= "celda02" 
						value 	= "Download File" 
						title 	= "Download File" 
						onclick = "window.location.href='<%=csvFile%>'"
					>
				</td>
				<td align="right" class="formas">
					<input 
						type 	= "Button" 
						class 	= "celda02" 
						value 	= "Download Zip" 
						title 	= "Download Zip" 
						onclick = "window.location.href='<%=zipFile%>'"
					>
				</td>
			</tr>
		</table>
		</td>
	</tr>
<%}%>
<%
} //if("P".equals(hidAction))
%>
<tr>
	<td>&nbsp;</td>
</tr>
</table>
</form>
</body>
</html>
