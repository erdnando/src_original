<%@ page contentType="application/json;charset=UTF-8"
	import="
		java.util.*,
		java.math.*,
		netropology.utilerias.*,
		net.sf.json.*,
		com.netro.contenido.*,
		com.netro.model.catalogos.CatalogoSimple,
		com.netro.model.catalogos.CatalogoMenuSeccion,
		com.netro.pdf.*"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/02admcont/02secsession_extjs.jspf" %>
<%
String infoRegresar = "";
String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
String operacion = (request.getParameter("operacion")!=null)?request.getParameter("operacion"):"";

if (informacion.equals("catalogoSecciones")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("admcon_seccion");
	cat.setCampoClave("ic_seccion");
	cat.setCampoDescripcion("cg_nombre");
	cat.setOrden("cg_nombre");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("catalogoMenuSeccion")) {
	String cveSecciones = (request.getParameter("cveSeccion")!=null)?request.getParameter("cveSeccion"):"";
	
	CatalogoMenuSeccion cat = new CatalogoMenuSeccion();
	cat.setClaveSeccion(cveSecciones);
	cat.setCampoClave("ic_opcion");
	cat.setCampoDescripcion("cg_descripcion");
	cat.setOrden("cg_descripcion");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("GetDataTextLogo")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	List lstDataTextoLog = admonContenido.getDataTextLogo();
	jsObjArray = JSONArray.fromObject(lstDataTextoLog);
	
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if (informacion.equals("AddNewTextLogo")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String newTextoLogo = (request.getParameter("newTextoLogo")!=null)?request.getParameter("newTextoLogo"):"";
	List lstDataTextLogo = new ArrayList();
	HashMap mp = new HashMap();
	
	mp.put("CVETEXTLOGO", "0");
	mp.put("TEXTOLOGO", newTextoLogo);
	lstDataTextLogo.add(mp);
	admonContenido.saveTextLogo(lstDataTextLogo);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("modifyTextLogo")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	List lstDataTextLogo = new ArrayList();
	String cveTextLogo = "";
	String textoLogo = "";
	
	while (itReg.hasNext()) {
			HashMap mp = new HashMap();
			JSONObject registro = (JSONObject)itReg.next();			
			cveTextLogo = registro.getString("CVETEXTLOGO");
			textoLogo = registro.getString("TEXTOLOGO");
			
			mp.put("CVETEXTLOGO",cveTextLogo);
			mp.put("TEXTOLOGO",textoLogo);
			lstDataTextLogo.add(mp);
			
	}
	
	admonContenido.saveTextLogo(lstDataTextLogo);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("deleteTextLogo")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	List lstDataTextLogo = new ArrayList();
	String cveTextLogo = "";
	String textoLogo = "";
	
	while (itReg.hasNext()) {
			HashMap mp = new HashMap();
			JSONObject registro = (JSONObject)itReg.next();			
			cveTextLogo = registro.getString("CVETEXTLOGO");
			textoLogo = registro.getString("TEXTOLOGO");
			
			mp.put("CVETEXTLOGO",cveTextLogo);
			mp.put("TEXTOLOGO",textoLogo);
			lstDataTextLogo.add(mp);
			
	}
	
	admonContenido.deleteTextLogo(lstDataTextLogo);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("GetDataSecciones")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();	
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	List lstDataSecciones = admonContenido.getDataSecciones();
	jsObjArray = JSONArray.fromObject(lstDataSecciones);
	
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if (informacion.equals("modifyTextSecciones")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	List lstDataNombreSeccion = new ArrayList();
	String cveTextLogo = "";
	String textoLogo = "";
	
	while (itReg.hasNext()) {
			HashMap mp = new HashMap();
			JSONObject registro = (JSONObject)itReg.next();			
			cveTextLogo = registro.getString("CVESECCION");
			textoLogo = registro.getString("NOMBRESECCION");
			
			mp.put("CVESECCION",cveTextLogo);
			mp.put("NOMBRESECCION",textoLogo);
			lstDataNombreSeccion.add(mp);
			
	}
	
	admonContenido.saveTextoSecciones(lstDataNombreSeccion);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("GetImageSeccion")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	String cveSeccion = (request.getParameter("cveSeccion") == null)?"":request.getParameter("cveSeccion");
	CargaArchivoAdmonCont cargaArchivo = new CargaArchivoAdmonCont();
	cargaArchivo.setClaveContenido(cveSeccion);

	
	List lstDataSecciones = cargaArchivo.getArchivosCargadoSeccion(strDirectorioTemp);
	List lItem = new ArrayList();
	for(int i=0;i<lstDataSecciones.size();i++) {
		lItem = (ArrayList)lstDataSecciones.get(i);
		
	}//for
	
	
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("urlImg", strDirecVirtualTemp+lItem.get(0).toString()+".jpg");
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("GetDataMenus")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();	
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String cveSecciones = (request.getParameter("cboSecciones") == null)?"":request.getParameter("cboSecciones");
	String cveMenu = (request.getParameter("cboMenus") == null)?"":request.getParameter("cboMenus");
	
	System.out.println("cveSecciones===="+cveSecciones);
	System.out.println("cveMenu===="+cveMenu);
	List lstDataSecciones = admonContenido.getDataMenus(cveSecciones,cveMenu);
	jsObjArray = JSONArray.fromObject(lstDataSecciones);
	
	jsonObj.put("registros", jsObjArray.toString());
	jsonObj.put("success",  new Boolean(true));
	infoRegresar= jsonObj.toString();

}else if (informacion.equals("modifyTextMenuSecciones")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	List lstDataMenuSeccion = new ArrayList();
	String cveSeccion = "";
	String cveMenu = "";
	String descOpcion = "";
	String textoTitulo = "";
	String csNuevo = "";
	
	while (itReg.hasNext()) {
			HashMap mp = new HashMap();
			JSONObject registro = (JSONObject)itReg.next();			
			cveSeccion = registro.getString("CVESECCION");
			cveMenu = registro.getString("OPCION");
			descOpcion = registro.getString("DESCOPCION");
			textoTitulo = registro.getString("TITULO");
			csNuevo = registro.getString("NUEVO");
			
			mp.put("CVESECCION",cveSeccion);
			mp.put("OPCION",cveMenu);
			mp.put("DESCOPCION",descOpcion);
			mp.put("TITULO",textoTitulo);
			
			if(registro.getBoolean("ACTIVADO") && (csNuevo).equals("N"))
				mp.put("NUEVO","S");
			else if(!registro.getBoolean("ACTIVADO") && (csNuevo).equals("S"))
				mp.put("NUEVO","N");
			else
				mp.put("NUEVO",csNuevo);
			
			lstDataMenuSeccion.add(mp);
			
	}
	
	admonContenido.saveTextoMenusSeccion(lstDataMenuSeccion);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("deleteMenuSeccion")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	List lstDataMenu = new ArrayList();
	String cveSeccion = "";
	String cveMenu = "";
	
	while (itReg.hasNext()) {
			HashMap mp = new HashMap();
			JSONObject registro = (JSONObject)itReg.next();			
			cveSeccion = registro.getString("CVESECCION");
			cveMenu = registro.getString("OPCION");
			
			mp.put("CVESECCION",cveSeccion);
			mp.put("OPCION",cveMenu);
			lstDataMenu.add(mp);
			
	}
	
	admonContenido.deleteMenusSeccion(lstDataMenu);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("GetDataVinetas")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();	
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String cveSeccion = (request.getParameter("cveSeccion") == null)?"":request.getParameter("cveSeccion");
	String cveMenu = (request.getParameter("cveMenu") == null)?"":request.getParameter("cveMenu");
	
	System.out.println("cveSeccion===="+cveSeccion);
	System.out.println("cveMenu===="+cveMenu);
	List lstDataVinetas = admonContenido.getDataVinetas(cveSeccion,cveMenu);
	jsObjArray = JSONArray.fromObject(lstDataVinetas);
	
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if (informacion.equals("AddNewVineta")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String textoDescripcion = (request.getParameter("textoDescripcion")!=null)?request.getParameter("textoDescripcion"):"";
	String cveSeccion = (request.getParameter("cveSeccion")!=null)?request.getParameter("cveSeccion"):"";
	String cveMenu = (request.getParameter("cveMenu")!=null)?request.getParameter("cveMenu"):"";
	
	HashMap mp = new HashMap();
	
	mp.put("CVESECCION", cveSeccion);
	mp.put("CVEOPCION", cveMenu);
	mp.put("TEXTVINETA", textoDescripcion);
	
	admonContenido.addMenuVinetas(mp);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ModifyVineta")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String textoDescripcion = (request.getParameter("textoDescripcion")!=null)?request.getParameter("textoDescripcion"):"";
	String cveSeccionDet = (request.getParameter("cveSeccionDet")!=null)?request.getParameter("cveSeccionDet"):"";
	
	admonContenido.modifyMenuVinetas(cveSeccionDet, textoDescripcion);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("deleteVinetas")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	List lstCveVinetas = new ArrayList();
	String cveSeccionDet = "";
	String cveMenu = "";
	
	while (itReg.hasNext()) {
			HashMap mp = new HashMap();
			JSONObject registro = (JSONObject)itReg.next();			
			cveSeccionDet = registro.getString("CEVESECCONTDET");

			lstCveVinetas.add(cveSeccionDet);
			
	}
	
	admonContenido.deleteVineta(lstCveVinetas);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("AddNewMenuSeccion")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	HashMap mp = new HashMap();
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String cveSeccion = (request.getParameter("cboSeccionesA")!=null)?request.getParameter("cboSeccionesA"):"";
	String nombreMenu = (request.getParameter("nombreMenu")!=null)?request.getParameter("nombreMenu"):"";
	String textoTitulo = (request.getParameter("textoTitulo")!=null)?request.getParameter("textoTitulo"):"";
	String numVinetas = (request.getParameter("numVinetas")!=null)?request.getParameter("numVinetas"):"";
	String csNuevo = (request.getParameter("csNuevo")!=null)?request.getParameter("csNuevo"):"";
	
	mp.put("CVESECCION",cveSeccion);
	mp.put("NOMBREMENU",nombreMenu);
	mp.put("TEXTOTITULO",textoTitulo);
	mp.put("NUMVINETAS",numVinetas);
	mp.put("CSNUEVO",csNuevo);
	
	int iVineta = Integer.parseInt(numVinetas);
	
	for(int x=0; x<iVineta; x++){
		String param = "newVineta"+x;
		String newVineta = (request.getParameter(param)!=null)?request.getParameter(param):"";
		mp.put(param.toUpperCase(),newVineta);
	}
	
	
	admonContenido.addNewMenuSeccion(mp);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("GetDataLinks")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();	
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	List lstDataLinks = admonContenido.getDataLinks();
	jsObjArray = JSONArray.fromObject(lstDataLinks);
	
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if (informacion.equals("SaveDataLinks")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	List lstDataLinks = new ArrayList();
	String cveLink = "";
	String nombreLink = "";
	String urlLink = "";
	
	while (itReg.hasNext()) {
			HashMap mp = new HashMap();
			JSONObject registro = (JSONObject)itReg.next();			
			cveLink = registro.getString("CVELINK");
			nombreLink = registro.getString("NOMBRELINK");
			urlLink = registro.getString("URLLINK");
			
			mp.put("CVELINK",cveLink);
			mp.put("NOMBRELINK",nombreLink);
			mp.put("URLLINK",urlLink);
			
			lstDataLinks.add(mp);

	}
	
	admonContenido.saveDataLinks(lstDataLinks);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("getComboBanners")) {
	CatalogoSimple cat = new CatalogoSimple();
	cat.setTabla("admcon_banner");
	cat.setCampoClave("ic_banner");
	cat.setCampoDescripcion("cg_nombre");
	cat.setOrden("cg_nombre");
	infoRegresar = cat.getJSONElementos();
	
}else if (informacion.equals("GetDataBanners")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();	
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	String cveBanner = (request.getParameter("cboBanner") == null)?"":request.getParameter("cboBanner");
	
	List lstDataBanners = admonContenido.getDataBanners(cveBanner);
	jsObjArray = JSONArray.fromObject(lstDataBanners);
	
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if (informacion.equals("GetImageBanner")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	String cveBanner = (request.getParameter("cveBanner") == null)?"":request.getParameter("cveBanner");
	CargaArchivoAdmonCont cargaArchivo = new CargaArchivoAdmonCont();
	cargaArchivo.setClaveContenido(cveBanner);

	
	List lstDataBanners = cargaArchivo.getArchivosCargadoBanner(strDirectorioTemp);
	List lItem = new ArrayList();
	for(int i=0;i<lstDataBanners.size();i++) {
		lItem = (ArrayList)lstDataBanners.get(i);
		
	}//for
	
	
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("urlImg", strDirecVirtualTemp+lItem.get(0).toString()+".jpg");
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("GetImageBannerFondo")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	String cveBanner = (request.getParameter("cveBanner") == null)?"":request.getParameter("cveBanner");
	CargaArchivoAdmonCont cargaArchivo = new CargaArchivoAdmonCont();
	cargaArchivo.setClaveContenido(cveBanner);

	
	List lstDataBanners = cargaArchivo.getArchivosCargadoBannerFondo(strDirectorioTemp);
	List lItem = new ArrayList();
	for(int i=0;i<lstDataBanners.size();i++) {
		lItem = (ArrayList)lstDataBanners.get(i);
		
	}//for
	
	
	jsonObj.put("success", Boolean.TRUE);
	jsonObj.put("urlImg", strDirecVirtualTemp+lItem.get(0).toString()+".jpg");
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("GetDataDetBanners")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();	
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	String cveBanner = (request.getParameter("cveBanner") == null)?"":request.getParameter("cveBanner");
	
	List lstDataDetBanners = admonContenido.getDataDetBanners(cveBanner);
	jsObjArray = JSONArray.fromObject(lstDataDetBanners);
	
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if (informacion.equals("getComboPosicion")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();	
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	String cveBanner = (request.getParameter("cveBanner") == null)?"":request.getParameter("cveBanner");
	
	List lstCboPosicion = admonContenido.getCboPosicion(cveBanner);
	jsObjArray = JSONArray.fromObject(lstCboPosicion);
	
	infoRegresar = "{\"success\": true, \"total\": "+ jsObjArray.size()+" , \"registros\": " + jsObjArray.toString() + " }";

}else if (informacion.equals("modifyTextBanners")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	List lstDataBanner = new ArrayList();
	String cveBanner = "";
	String textTexto = "";
	String textoTitulo = "";
	
	while (itReg.hasNext()) {
		HashMap mp = new HashMap();
		JSONObject registro = (JSONObject)itReg.next();			
		cveBanner = registro.getString("CVEBANNER");
		textoTitulo = registro.getString("NOMBREBANN");
		//textTexto = registro.getString("DESCBANN");

		mp.put("CVEBANNER",cveBanner);
		mp.put("NOMBREBANN",textoTitulo);
		//mp.put("DESCBANN",textoTitulo);
		
		lstDataBanner.add(mp);
			
	}
	
	admonContenido.modifyTextBanners(lstDataBanner);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("deleteBanners")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	List lstDataBanner = new ArrayList();
	String cveBanner = "";
	
	while (itReg.hasNext()) {
		JSONObject registro = (JSONObject)itReg.next();			
		cveBanner = registro.getString("CVEBANNER");

		lstDataBanner.add(cveBanner);
			
	}
	
	admonContenido.deleteBanners(lstDataBanner);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("invertirEstatusBanners")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	List lstDataBanner = new ArrayList();
	String cveBanner = "";
	String csEstatus = "";
	
	
	while (itReg.hasNext()) {
		HashMap mp = new HashMap();
		JSONObject registro = (JSONObject)itReg.next();			
		cveBanner = registro.getString("CVEBANNER");
		csEstatus = registro.getString("ACTIVOBANN");

		mp.put("CVEBANNER",cveBanner);
		mp.put("ACTIVOBANN",csEstatus);
		
		lstDataBanner.add(mp);
			
	}
	
	admonContenido.invertirEstatusBanners(lstDataBanner);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("deleteDetBanner")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String jsonRegistros = (request.getParameter("registros") == null)?"":request.getParameter("registros");
	String cveBanner = (request.getParameter("cveBanner") == null)?"":request.getParameter("cveBanner");
	List arrRegistrosEnviados = JSONArray.fromObject(jsonRegistros);
	Iterator itReg = arrRegistrosEnviados.iterator();
	
	List lstCveDetBanner = new ArrayList();
	String igPosicion = "";
	String cveMenu = "";
	
	while (itReg.hasNext()) {
			HashMap mp = new HashMap();
			JSONObject registro = (JSONObject)itReg.next();			
			igPosicion = registro.getString("POSICIONBANNEDIT");

			lstCveDetBanner.add(igPosicion);
			
	}
	
	admonContenido.deleteDetBanner(cveBanner, lstCveDetBanner);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("AddNewDetBanner")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	HashMap mp = new HashMap();
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String cveBanner = (request.getParameter("hidCveBanner")!=null)?request.getParameter("hidCveBanner"):"";
	String textoTitulo = (request.getParameter("textTituloDB")!=null)?request.getParameter("textTituloDB"):"";
	String textoTexto = (request.getParameter("textTextoDB")!=null)?request.getParameter("textTextoDB"):"";
	String posicion = (request.getParameter("cboPosicion")!=null)?request.getParameter("cboPosicion"):"";
	
	System.out.println("cveBanner==="+cveBanner);
	System.out.println("textoTitulo==="+textoTitulo);
	System.out.println("textoTexto==="+textoTexto);
	System.out.println("posicion==="+posicion);
	
	mp.put("CVEBANNER",cveBanner);
	mp.put("TEXTTITULO",textoTitulo);
	mp.put("TEXTTEXTO",textoTexto);
	mp.put("POSICION",posicion);
	
	
	admonContenido.addNewDetBanner(mp);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("addDetBannerNew")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();		
	HashMap mp = new HashMap();
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String cveBanner = (request.getParameter("newHidCveBanner")!=null)?request.getParameter("newHidCveBanner"):"";
	String textoTitulo = (request.getParameter("newTextTituloDB")!=null)?request.getParameter("newTextTituloDB"):"";
	String textoTexto = (request.getParameter("newTextTextoDB")!=null)?request.getParameter("newTextTextoDB"):"";
	String posicion = (request.getParameter("newCboPosicion")!=null)?request.getParameter("newCboPosicion"):"";
	
	System.out.println("cveBanner==="+cveBanner);
	System.out.println("textoTitulo==="+textoTitulo);
	System.out.println("textoTexto==="+textoTexto);
	System.out.println("posicion==="+posicion);
	
	mp.put("CVEBANNER",cveBanner);
	mp.put("TEXTTITULO",textoTitulo);
	mp.put("TEXTTEXTO",textoTexto);
	mp.put("POSICION",posicion);
	
	
	admonContenido.addNewDetBanner(mp);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}else if (informacion.equals("ModifyDetBanner")) {
	JSONObject jsonObj = new JSONObject();
	JSONArray jsObjArray = new JSONArray();
	HashMap mp = new HashMap();
	AdmonContenido admonContenido = ServiceLocator.getInstance().lookup("AdmonContenidoEJB", AdmonContenido.class);
	
	String cveBanner = (request.getParameter("hidCveBanner")!=null)?request.getParameter("hidCveBanner"):"";
	String textoTitulo = (request.getParameter("textTituloDB")!=null)?request.getParameter("textTituloDB"):"";
	String textoTexto = (request.getParameter("textTextoDB")!=null)?request.getParameter("textTextoDB"):"";
	String posicion = (request.getParameter("cboPosicion")!=null)?request.getParameter("cboPosicion"):"";
	String oldPosicion = (request.getParameter("posicionDetBanner")!=null)?request.getParameter("posicionDetBanner"):"";
	
	mp.put("CVEBANNER",cveBanner);
	mp.put("TEXTTITULO",textoTitulo);
	mp.put("TEXTTEXTO",textoTexto);
	mp.put("POSICION",posicion);
	
	admonContenido.modifyDetBanner(mp, oldPosicion);
	
	jsonObj.put("success", Boolean.TRUE);
	infoRegresar = jsonObj.toString();

}




System.out.println("infoRegresar = " + infoRegresar);

%>
<%=infoRegresar%>