<%@ page
	import="
		java.util.*,
		java.io.*,
		java.text.*,
		java.awt.*,
		javax.imageio.*,
		javax.imageio.stream.*,
		org.apache.commons.fileupload.disk.*,
		org.apache.commons.fileupload.servlet.*,
		org.apache.commons.fileupload.*,
		netropology.utilerias.*,
		com.netro.contenido.*,
		com.netro.pdf.*,
		net.sf.json.JSONArray,net.sf.json.JSONObject"
	errorPage="/00utils/error_extjs.jsp"
%>
<%@ include file="/appComun.jspf" %>
<%@ include file="/02admcont/02secsession_extjs.jspf" %>

<%
	JSONObject archivoCSV = new JSONObject();
	JSONObject archivoPDF = new JSONObject();
	JSONObject registroAcuse = new JSONObject();
	JSONArray arrAcuse = new JSONArray();
	boolean flag = true;
	ParametrosRequest req = null;
	String rutaArchivoTemporal = null;
	String itemArchivo = "";
	String folio = "";
	String fechaHora = "";
	
	String extension = "";
	String extensionB = "";
	String cveSeccion = "";
	
	String tipoContenidoS = "";
	String tipoContenidoB = "";
	String tipoContenidoBF = "";
	
	String newTipoContenidoB = "";
	String newNombreBanner = "";
	String newDescBanner = "";
	String errorDataFile = "";
	String errorDataFileB = "";
	String newCveBanner = "";
	
	String cveBanner = "";
	String cveBannerF = "";
	
	int tamanio = 0;
	int tamanioB = 0;
	
	int anchoMax = 0;
	int largoMax = 0;
	
	InputStream archivoImagen = null;
	InputStream archivoImagenB = null;
	
	if (ServletFileUpload.isMultipartContent(request)) {
		
		// Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		
		// Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(strDirectorioTemp));
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		req = new ParametrosRequest(upload.parseRequest(request));
		//extension = (req.getParameter("hidExtension") == null)? "" : req.getParameter("hidExtension");
		cveSeccion = (req.getParameter("hidCveSeccion") == null)? "" : req.getParameter("hidCveSeccion");
		cveBanner = (req.getParameter("hidCveBanner") == null)? "" : req.getParameter("hidCveBanner");
		cveBannerF = (req.getParameter("hidCveBannerF") == null)? "" : req.getParameter("hidCveBannerF");
		tipoContenidoS = (req.getParameter("hidTipoContSeccion") == null)? "" : req.getParameter("hidTipoContSeccion");
		tipoContenidoB = (req.getParameter("hidTipoContBanner") == null)? "" : req.getParameter("hidTipoContBanner");
		tipoContenidoBF = (req.getParameter("hidTipoContBannerF") == null)? "" : req.getParameter("hidTipoContBannerF");
		
		newNombreBanner = (req.getParameter("newNombreBanner") == null)? "" : req.getParameter("newNombreBanner");
		newDescBanner = (req.getParameter("newDescBanner") == null)? "" : req.getParameter("newDescBanner");
		newTipoContenidoB = (req.getParameter("hidNewTipoContBanner") == null)? "" : req.getParameter("hidNewTipoContBanner");
		
		
		try{
			long tamaPer = 0;
			long tamaPerB = 0;
			System.out.println("tipoContenidoS=="+tipoContenidoS);
			System.out.println("tipoContenidoB=="+tipoContenidoB);
			System.out.println("tipoContenidoBF=="+tipoContenidoBF);
			
			if(!"S".equals(newTipoContenidoB)){
				String ruta = strDirectorioTemp;
			
				if("S".equals(tipoContenidoS))
					extension = (req.getParameter("hidExtensionSeccion") == null)? "" : req.getParameter("hidExtensionSeccion");
				if("S".equals(tipoContenidoB))
					extension = (req.getParameter("hidExtensionBanner") == null)? "" : req.getParameter("hidExtensionBanner");
				if("S".equals(tipoContenidoBF))
					extension = (req.getParameter("hidExtensionBannerF") == null)? "" : req.getParameter("hidExtensionBannerF");
				
				extension = extension.toLowerCase();
				System.out.println("extension=="+extension);
				if (extension.equals("jpg") || extension.equals("jpeg")){
					if("S".equals(tipoContenidoS)){
						tamaPer = 256000;
						anchoMax = 710;
						largoMax = 354;
						ruta = ruta+"imgSeccion."+extension;
					}
					if("S".equals(tipoContenidoB)){
						tamaPer = 12288;
						anchoMax = 198;
						largoMax = 137;
						ruta = ruta+"imgBannerMini."+extension;
					}
					if("S".equals(tipoContenidoBF)){
						tamaPer = 159744;
						anchoMax = 951;
						largoMax = 355;
						ruta = ruta+"imgBannerFond."+extension;
					}
					
					
					upload.setSizeMax(tamaPer);	
					//tamaPer = (650 * 1024);
				}
	
				FileItem fItem = (FileItem)req.getFiles().get(0);
				itemArchivo		= (String)fItem.getName();
				archivoImagen = fItem.getInputStream();
				tamanio			= (int)fItem.getSize();
				
				
				File file = new File(ruta);
				fItem.write(file);

				HashMap mp = getDataImage(file, ruta);
				int ancho = Integer.parseInt((String)mp.get("ANCHO"));
				int largo = Integer.parseInt((String)mp.get("LARGO"));
				
				System.out.println("ancho=="+ancho);
				System.out.println("anchoMax=="+anchoMax);
				System.out.println("largo=="+largo);
				System.out.println("largoMax=="+largoMax);
				if(ancho != anchoMax || largo != largoMax){
					flag = false;
					errorDataFile = "Las dimensiones deben ser igual a : "+anchoMax+" x "+largoMax+" pixeles";
				}
				
				if (tamanio > tamaPer){
					flag = false;
					errorDataFile = "Excede el tama�o de "+tamaPer+" bytes permitidos";
				}
				if (flag){
					
					fechaHora = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date()) + " " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					CargaArchivoAdmonCont cargaArchivo = new CargaArchivoAdmonCont();
					cargaArchivo.setExtension(extension);
					cargaArchivo.setSize(tamanio);
					
					if("S".equals(tipoContenidoS)){
						cargaArchivo.setClaveContenido(cveSeccion);
						cargaArchivo.insertaCargaArchivoSeccion(archivoImagen);
						
					}else	if("S".equals(tipoContenidoB)){
						cargaArchivo.setClaveContenido(cveBanner);
						cargaArchivo.insertaCargaArchivoBanner(archivoImagen);
					
					}else	if("S".equals(tipoContenidoBF)){
						cargaArchivo.setClaveContenido(cveBannerF);
						cargaArchivo.insertaCargaArchivoBannerFondo(archivoImagen);
						
					}
	
				}
			}else{
				CargaArchivoAdmonCont cargaArchivo = new CargaArchivoAdmonCont();
				
				extension = (req.getParameter("hidNewExtensionBanner") == null)? "" : req.getParameter("hidNewExtensionBanner");
				extensionB = (req.getParameter("hidNewExtensionBannerF") == null)? "" : req.getParameter("hidNewExtensionBannerF");
				
				extension = extension.toLowerCase();
				extensionB = extensionB.toLowerCase();
			
				tamaPer = 12288;
				tamaPerB = 159744;
				
				System.out.println("extension==="+extension);
				System.out.println("extensionB==="+extensionB);
				System.out.println("req.getFiles()==="+req.getFiles());
				
				if (extension.equals("jpg") || extension.equals("jpeg")){
					tamaPer = 12288;
					anchoMax = 198;
					largoMax = 137;
					upload.setSizeMax(tamaPer);	
					
					FileItem fItem = (FileItem)req.getFiles().get(0);
					itemArchivo		= (String)fItem.getName();
					archivoImagen = fItem.getInputStream();
					tamanio			= (int)fItem.getSize();

					String ruta = strDirectorioTemp+"imgBannerMiniatura."+extension;
					File file = new File(ruta);
					fItem.write(file);

					HashMap mp = getDataImage(file, ruta);
					int ancho = Integer.parseInt((String)mp.get("ANCHO"));
					int largo = Integer.parseInt((String)mp.get("LARGO"));
					
					System.out.println("ancho=="+ancho);
					System.out.println("anchoMax=="+anchoMax);
					System.out.println("largo=="+largo);
					System.out.println("largoMax=="+largoMax);
					
					if(ancho != anchoMax || largo != largoMax){
						flag = false;
						errorDataFile = "Las dimensiones deben ser igual a : "+anchoMax+" x "+largoMax+" pixeles";
					}
					
					
				}
				
				if (extensionB.equals("jpg") || extensionB.equals("jpeg")){
					tamaPerB = 159744;
					anchoMax = 951;
					largoMax = 355;
					upload.setSizeMax(tamaPerB);	
					
					FileItem fItem = (FileItem)req.getFiles().get(1);
					itemArchivo		= (String)fItem.getName();
					archivoImagenB = fItem.getInputStream();
					tamanioB			= (int)fItem.getSize();
					
					String ruta = strDirectorioTemp+"imgBannerFondo."+extension;
					File file = new File(ruta);
					fItem.write(file);

					HashMap mp = getDataImage(file, ruta);
					int ancho = Integer.parseInt((String)mp.get("ANCHO"));
					int largo = Integer.parseInt((String)mp.get("LARGO"));
					
					System.out.println("ancho=="+ancho);
				System.out.println("anchoMax=="+anchoMax);
				System.out.println("largo=="+largo);
				System.out.println("largoMax=="+largoMax);
					
					if(ancho != anchoMax || largo != largoMax){
						flag = false;
						errorDataFileB = "Las dimensiones deben ser igual a : "+anchoMax+" x "+largoMax+" pixeles";
					}

				}
				
				if(tamanio > tamaPer ){
					flag = false;
					errorDataFile = "Excede el tama�o de "+tamaPer+" bytes permitidos";
				}
				
				
				if(tamanioB > tamaPerB){
					flag = false;
					errorDataFileB = "Excede el tama�o de "+tamaPerB+" bytes permitidos";
				}
				
				if (flag){
					
					fechaHora = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date()) + " " + new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					HashMap mp = new HashMap();
					mp.put("NOMBREBANN",newNombreBanner);
					//mp.put("DESCBANN",newDescBanner);
					
					newCveBanner = cargaArchivo.addNewBanner(mp, archivoImagen, archivoImagenB, tamanio, tamanioB);
					
				}

			}
		}catch (Exception e){
			throw new AppException("Ocurrio un error al obtener datos del archivo", e);
		}
	}
	
%>
<%!
public HashMap getDataImage(File file, String ruta){
	HashMap mp = new HashMap();
	Iterator iter = ImageIO.getImageReadersBySuffix(getFileSuffix(ruta)); 
	 if (iter.hasNext()) { 
		  ImageReader reader = (ImageReader)iter.next(); 
		  try { 
				ImageInputStream stream = new FileImageInputStream(file); 
				reader.setInput(stream); 
				int ancho = reader.getWidth(reader.getMinIndex()); 
				int largo = reader.getHeight(reader.getMinIndex());

				mp.put("ANCHO",String.valueOf(ancho));
				mp.put("LARGO",String.valueOf(largo));
				
				
				
		  } catch (IOException e) { 
				e.printStackTrace();
		  } finally { 
				reader.dispose(); 
		  } 
	 } else { 
		  System.out.println("No se pudo leer el formato del archivo: "); 
	 } 
	 return mp;
}
public String getFileSuffix(String path){
	String result = null; 
    if (path != null) { 
        result = ""; 
        if (path.lastIndexOf('.') != -1) { 
            result = path.substring(path.lastIndexOf('.')); 
            if (result.startsWith(".")) { 
                result = result.substring(1); 
            } 
        } 
    } 
    return result; 
}
%>
{
	"success": true,
	"flag":	<%=(flag)?"true":"false"%>,
	"msgError":	'<%=errorDataFile%>',
	"msgErrorB":	'<%=errorDataFileB%>',
	"newCveBanner":	'<%=newCveBanner%>'
}