Ext.onReady(function() {
	var objBanner={
		totReg: 0,
		regActivosIni: 0,
		regInactivosIni: 0,
		regActivos: 0,
		regInactivos: 0
	}
//TEXTO DEL LOGO..............................................................................

	var procesarDataTextoLogo = function(store, arrRegistros, opts) {
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!gridTextoLogo.isVisible()) {
				//contenedorPrincipalCmp.add(gridTextoLogo);
				//contenedorPrincipalCmp.doLayout();
			}
			
			var el = gridTextoLogo.getGridEl();

			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	};
	
	
	var procesarSuccessFailureAddNewTextLogo = function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			storeDataTextLogo.load();
			Ext.getCmp('btnAgregar').enable();
			Ext.getCmp('btnGuardar').enable();
			Ext.getCmp('btnEliminar').enable();
			fpTextLogo.getForm().reset();
			fpTextLogo.hide();
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var processSuccessFailureModifyTextLogo =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			storeDataTextLogo.load();
			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	
	var processSuccessFailureDeleteTextLogo =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			storeDataTextLogo.load();
			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var fnAgregar = function(){
		Ext.getCmp('btnAgregar').disable();
		Ext.getCmp('btnGuardar').disable();
		Ext.getCmp('btnEliminar').disable();
		fpTextLogo.show();
	};
	
	var fnGuardar = function(){
		var store = gridTextoLogo.getStore();
		var modificados = store.getModifiedRecords();
		var registrosEnviar = [];
		var bNoTexto = false;
		if(!Ext.isEmpty(modificados)){
			Ext.each(modificados, function(record) {
				if(record.data['TEXTOLOGO']=='') bNoTexto = true;
				registrosEnviar.push(record.data);
			});
			
			if(bNoTexto){
				Ext.MessageBox.alert('Mensaje','Favor de ingresar el Texto del Logo');
			}else{
			
				Ext.Ajax.request({
					url: '02admincont01ext.data.jsp',
					params: {
						informacion: 'modifyTextLogo',
						registros: Ext.encode(registrosEnviar)
					},
					callback: processSuccessFailureModifyTextLogo
				});
			}
		}else{
			Ext.MessageBox.alert('Mensaje','No existen registros modificados');
		}
	};
	
	var fnEliminar = function(){
		var bOk = false;
		var indiceSm = 0;
		var registrosEnviar = [];
		
		gridTextoLogo.getStore().each(function(record) {
			if(selectModel.isSelected(indiceSm)){
				registrosEnviar.push(record.data);
				bOk = true;
			}
			
			indiceSm = indiceSm+1;				
		});
		
		if(bOk){
			Ext.MessageBox.confirm('Confirmaci�n','�Esta seguro de eliminar los registrsos seleccionados?',function(msb){
				if(msb=='yes'){
					Ext.Ajax.request({
						url: '02admincont01ext.data.jsp',
						params: {
							informacion: 'deleteTextLogo',
							registros: Ext.encode(registrosEnviar)
						},
						callback: processSuccessFailureDeleteTextLogo
					});
				}
			});
		}else{
			Ext.MessageBox.alert('Mensaje','Favor de seleccionar un registro');
		}
		
	};
	
	
	var fnAddNewTextLog = function(){
		Ext.Ajax.request({
			url: '02admincont01ext.data.jsp',
			params: Ext.apply(fpTextLogo.getForm().getValues(),{
				informacion: 'AddNewTextLogo'
			}),
			callback: procesarSuccessFailureAddNewTextLogo
		});  
	
	}


	var selectModel = new Ext.grid.CheckboxSelectionModel({
      checkOnly: false
  });
	 

	var storeDataTextLogo = new Ext.data.JsonStore({
		id:'storeDataTextLogo1',
		root : 'registros',
		url : '02admincont01ext.data.jsp',
		baseParams: {
			informacion: 'GetDataTextLogo'
		},
		fields: [
			{name: 'CVETEXTLOGO'},
			{name: 'TEXTOLOGO'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDataTextoLogo,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarDataTextoLogo(null, null, null);
				}
			}
		}
	});
	
	var gridTextoLogo = new Ext.grid.EditorGridPanel({
		id: 'gridTextoLogo1',
		store: storeDataTextLogo,
		margins: '20 0 0 0',
		style: 'margin: 0 auto',
		clicksToEdit: 1,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		sm: selectModel,
		columns: [
			selectModel,
			{
				header: 'Clave',
				tooltip: 'Clave',
				dataIndex: 'CVETEXTLOGO',
				sortable: true,
				width: 100,
				resizable: true,
				hidden: false,
				renderer:  function (causa, columna, registro){
					columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
					return causa;
				}
			},
			{
				header : 'Texto Logo',
				tooltip: 'Texto Logo',
				dataIndex : 'TEXTOLOGO',
				width : 300,
				sortable : true,
				//align: 'center',
				editor: {
						 xtype: 'textfield'
					},
				renderer:  function (causa, columna, registro){
							columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
							return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 250,
		width: 440,
		title: '',
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
				text: 'Agregar',
				id: 'btnAgregar',
				iconCls: 'aceptar',
				handler: fnAgregar
				},
				'-',
				{
				text: 'Guardar',
				id: 'btnGuardar',
				iconCls: 'icoGuardar',
				handler: fnGuardar
				},
				'-',
				{
				text: 'Eliminar',
				id: 'btnEliminar',
				iconCls: 'borrar',
				handler: fnEliminar
				}
			]
		}
	});

	var fpTextLogo = new Ext.form.FormPanel({
		id: 'fpTextLogo1',
		width: 600,
		style: 'margin:0 auto;',
		title: 'Agregar',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		hidden: true,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: {
					xtype: 'textfield',
					name: 'newTextoLogo',
					id: 'newTextoLogo1',
					fieldLabel: 'Texto del Logo',
					allowBlank: false,
					hidden: false,
					maxLength: 25,	
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'  
				},
		monitorValid: true,
		buttons: [
			{
				text: 'Guardar',
				iconCls: 'icoBuscar',
				id:'btnConsultar',
				formBind: true,
				handler: fnAddNewTextLog 
			},
			{
				text: 'Cancelar',
				hidden: false,
				iconCls: 'cancelar',
				handler: function() {
					
					Ext.getCmp('btnAgregar').enable();
					Ext.getCmp('btnGuardar').enable();
					Ext.getCmp('btnEliminar').enable();
					fpTextLogo.getForm().reset();
					fpTextLogo.hide();
				}
				
			}
		]
	});


//TEXTO DEL LOGO..............................................................................FIN


//SECCIONES..............................................................................INICIO

	
	var procesarDataSecciones = function(store, arrRegistros, opts) {
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!gridSecciones.isVisible()) {
				//contenedorPrincipalCmp.add(gridTextoLogo);
				//contenedorPrincipalCmp.doLayout();
			}
			
			var el = gridSecciones.getGridEl();

			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	};
	
	var fnGuardarSecciones = function(){
		var store = gridSecciones.getStore();
		var modificados = store.getModifiedRecords();
		var registrosEnviar = [];
		var bNoTexto = false;
		if(!Ext.isEmpty(modificados)){
			Ext.each(modificados, function(record) {
				if(record.data['NOMBRESECCION']=='') bNoTexto = true;
				registrosEnviar.push(record.data);
			});
			
			if(bNoTexto){
				Ext.MessageBox.alert('Mensaje','Favor de ingresar el Nombre de la Secci�n');
			}else{
			
				Ext.Ajax.request({
					url: '02admincont01ext.data.jsp',
					params: {
						informacion: 'modifyTextSecciones',
						registros: Ext.encode(registrosEnviar)
					},
					callback: processSuccessFailureModifySeccion
				});
			}
			
		}else{
			Ext.MessageBox.alert('Mensaje','No existen registros modificados');
		}
	};
	
	var muestraImagen = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var cveSeccion = registro.data['CVESECCION'];
		
		Ext.Ajax.request({
			url: '02admincont01ext.data.jsp',
			params: {
				informacion: 'GetImageSeccion',
				cveSeccion: cveSeccion
			},
			callback: processSuccessFailureGetImageSeccion
		});

	};
	
	var muestraCargaImagen = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var cveSeccion = registro.data['CVESECCION'];
		
		var hidCveSeccion = Ext.getCmp('hidCveSeccion');
		hidCveSeccion.setValue(cveSeccion);
		
		var ventana = Ext.getCmp('winCargaImagen');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									modal: true,
									resizable: false,
									layout: 'form',
									x: 300,
									width: 667,
									//height: 310,
									id: 'winCargaImagen',
									closeAction: 'hide',
									items: [fpCarga],
									title: '',
									listeners:{
										beforehide: function(win){
												var cargaArchivo = Ext.getCmp('fileuploadImage');
												cargaArchivo.setValue('');
										}
									}
								}).show();
							}
	};
	
	var processSuccessFailureModifySeccion =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			storeDataSecciones.load();
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var processSuccessFailureGetImageSeccion =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var objMsg = Ext.getCmp('pnlImageSeccion1');
			
			objMsg.body.update('<img src="'+resp.urlImg+'" border="0" alt="">');
			objMsg.show();
			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};


	var storeDataSecciones = new Ext.data.JsonStore({
		id:'storeDataSecciones1',
		root : 'registros',
		url : '02admincont01ext.data.jsp',
		baseParams: {
			informacion: 'GetDataSecciones'
		},
		fields: [
			{name: 'CVESECCION'},
			{name: 'NOMBRESECCION'},
			{name: 'FECMODIF'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDataSecciones,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarDataSecciones(null, null, null);
				}
			}
		}
	});

	var gridSecciones = new Ext.grid.EditorGridPanel({
			id: 'gridSecciones1',
			store: storeDataSecciones,
			margins: '20 0 0 0',
			style: 'margin: 0 auto',
			title: 'Secciones',
			clicksToEdit: 1,
			viewConfig: {
				templates: {
					cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
						'</td>'
					)
				}
			},
			columns: [
				{
					header: 'Clave',
					tooltip: 'Clave',
					dataIndex: 'CVESECCION',
					sortable: true,
					width: 80,
					resizable: true,
					hidden: false,
					renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
				},
				{
					header : 'Nombre de la Secci�n',
					tooltip: 'Nombre de la Secci�n',
					dataIndex : 'NOMBRESECCION',
					width : 310,
					sortable : true,
					editor: {
							 xtype: 'textfield'
						},
					renderer:  function (causa, columna, registro){
								columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
								return causa;
					}
				},
				{
					header : 'Ver Imagen',
					tooltip: 'Ver Imagen',
					xtype:	'actioncolumn',
					dataIndex : 'CVESECCION',
					width:	80,	align: 'center', hidden: false,
					renderer: function(value, metadata, record, rowindex, colindex, store) {
								
					},
					items: [
						{
							getClass: function(value, metadata, record, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Ver';
									return 'icoImagen';
							},
							handler:	muestraImagen
						}
					]
				},
				{
					header : 'Cargar Imagen',
					tooltip: 'Cargar Imagen',
					xtype:	'actioncolumn',
					dataIndex : 'CVESECCION',
					width:	80,	align: 'center', hidden: false,
					renderer: function(value, metadata, record, rowindex, colindex, store) {
								
					},
					items: [
						{
							getClass: function(value, metadata, record, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Ver';
									return 'upload-icon';
							},
							handler: muestraCargaImagen
						}
					]
				},
				{
					header: 'Fecha de Modificaci�n',
					tooltip: 'Fecha de Modificaci�n', 
					dataIndex: 'FECMODIF',
					sortable: true,
					width: 143,
					resizable: true,
					hidden: false,
					renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
				}
			],
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 250,
			width: 710,
			frame: true,
			bbar: {
				xtype: 'toolbar',
				items: [
					'->',
					'-',
					{
					text: 'Guardar',
					id: 'btnGuardarSecciones',
					iconCls: 'icoGuardar',
					handler: fnGuardarSecciones
					}
				]
			}
	});
	
	var elementosFormCarga = [
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			id:'cargaArchivo1',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'panel',
					id:		'pnlArchivo',
					columnWidth: 1,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 100,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivo1',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'fileuploadImage',
									width: 400,
									labelWidth: 200,
									emptyText: 'Ruta del Archivo',
									fieldLabel: 'Seleccione Imagen',
									name: 'imagenSeccion',
									buttonText: 'Examinar...',
									buttonCfg: {
									  iconCls: 'upload-icon'
									},
									//anchor: '95%',
									regex: /^.*\.(jpg|JPG|jpeg|JPEG)$/,
									regexText:'Solo se admiten archivos JPG y JPEG'
								},
								{
									xtype: 'hidden',
									id:	'hidExtensionSeccion',
									name:	'hidExtensionSeccion',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidCveSeccion',
									name:	'hidCveSeccion',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidTipoContSeccion',
									name:	'hidTipoContSeccion',
									value:	''
								},
								{
									xtype: 	'button',
									text: 	'Continuar',
									id: 		'btnContinuar',
									formBind: true,
									iconCls: 'icoContinuar',
									style: { 
										  marginBottom:  '10px' 
									},
									handler: function(){

										var cargaArchivo = Ext.getCmp('fileuploadImage');
										if (!cargaArchivo.isValid()){
											cargaArchivo.focus();
											return;
										}
										if(cargaArchivo.getValue()==''){
											cargaArchivo.markInvalid("Favor de seleccionar una imagen");
											cargaArchivo.focus();
											return;
										}
										var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
										var extArchivo = Ext.getCmp('hidExtensionSeccion');
										var tipoContenido = Ext.getCmp('hidTipoContSeccion');
										
										if (/^.*\.(jpg|jpeg)$/.test(ifile)){
											extArchivo.setValue(ifile.substring(ifile.indexOf('.')+1));
										}

										fpCarga.el.mask('Procesando...', 'x-mask-loading');
										tipoContenido.setValue('S');
										fpCarga.getForm().submit({
											url: '02admincont01extfile.jsp',
											waitMsg: 'Enviando datos...',
											success: function(form, action) {
												if (action.result.flag){
													Ext.getCmp('fileuploadImage').setValue('');
													tipoContenido.setValue('');
													fpCarga.el.unmask();
													var ventana = Ext.getCmp('winCargaImagen');
													ventana.hide();
												
												}else{
													tipoContenido.setValue('');
													fpCarga.el.unmask();
													cargaArchivo.markInvalid(action.result.msgError);
													return;
												}
											},
											failure: NE.util.mostrarSubmitError
										})
											
									}
								}
							]
						}
							
					]
				}
			]
		}
	];
	
	var fpCarga = new Ext.form.FormPanel({
		id: 'formaCarga',
		width: 700,
		title: 'Agregar Imagen',
		frame: true,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px; text-align:left;',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [elementosFormCarga,
				{
					xtype: 'panel',
					name: 'pnlMsgReqCarga',
					id: 'pnlMsgReqCarga1',
					style: 'margin:0 auto;',
					frame: false,
					hidden: false,
					html: '<div class="formas" align="left" >'+
							'<i>-La imagen debe de tener formato JPEG.</i></br>'+
							'<i>-El tama�o debe ser menor a 256000 bytes.</i></br>'+
							'<i>-Las dimensiones deben ser igual a 710 x 354 pixeles.</i>'+
							'</div>'
				
				}
			],
		monitorValid: true
	});

//SECCIONES..............................................................................FIN



//MENUS .................................................................................INICIO

	var procesarSuccessFailureGetDataMenus =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var pnlMenus = Ext.getCmp('pnlMenus');
			pnlMenuEl = pnlMenus.getEl();

			storeDataMenus.rejectChanges();
			storeDataMenus.loadData(resp);
			
			pnlMenuEl.unmask();
			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var procesarConsultaDataMenus = function(store, arrRegistros, opts) {
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!gridMenus.isVisible()) {
				gridMenus.show();
			}
			
			var el = gridMenus.getGridEl();

			if(store.getTotalCount() > 0) {
				store.each(function(record) {
					
					if(record.data['NUEVO']=='S'){
						record.set('ACTIVADO',true);
					}else{
						record.set('ACTIVADO',false);
					}
				});
				store.commitChanges();
				
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	};
	
	var procesarSuccessFailureAddNewVineta =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			//var pnlMenus = Ext.getCmp('pnlMenus');
			//pnlMenuEl = pnlMenus.getEl();
			//pnlMenuEl.unmask();
			
			var textoDescripcion = Ext.getCmp('textoDescripcion1');
			var tipoEvento = Ext.getCmp('tipoEvento1');
			textoDescripcion.setValue('');
			tipoEvento.setValue('A');

			storeVinetasData.load({
				params: Ext.apply(fpEdicionVineta.getForm().getValues(),{
					informacion: 'GetDataVinetas'
				})
			});

			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var fnGuardarMenus = function(){
		var store = gridMenus.getStore();
		var modificados = store.getModifiedRecords();
		var registrosEnviar = [];
		var bNoTextoMenu = false;
		var bNoTextoTitulo = false;
		if(!Ext.isEmpty(modificados)){
			Ext.each(modificados, function(record) {
				if(record.data['DESCOPCION']=='' || record.data['TITULO']=='') bNoTextoMenu = true;
				registrosEnviar.push(record.data);
			});
			
			if(bNoTextoMenu){
				Ext.MessageBox.alert('Mensaje','Favor de ingresar texto de Men� y T�tulo');
			}else{
				var pnlMenus = Ext.getCmp('pnlMenus');
				pnlMenuEl = pnlMenus.getEl();				
				pnlMenuEl.mask('Guardando Modificaciones...','x-mask-loading');
				
				Ext.Ajax.request({
					url: '02admincont01ext.data.jsp',
					params: {
						informacion: 'modifyTextMenuSecciones',
						registros: Ext.encode(registrosEnviar)
					},
					callback: processSuccessFailureModifyMenuSeccion
				});
			}
		}else{
			Ext.MessageBox.alert('Mensaje','No existen registros modificados');
		}
	};
	
	var processSuccessFailureModifyMenuSeccion =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			var pnlMenus = Ext.getCmp('pnlMenus');
			pnlMenuEl = pnlMenus.getEl();
			pnlMenuEl.unmask();
			
			Ext.Ajax.request({
				url: '02admincont01ext.data.jsp',
				params: Ext.apply(fpMenus.getForm().getValues(),{
					informacion: 'GetDataMenus'
				}),
				callback: procesarSuccessFailureGetDataMenus
			});
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var fnEliminarMenus = function(){
		var bOk = false;
		var indiceSm = 0;
		var registrosEnviar = [];
		
		gridMenus.getStore().each(function(record) {
			if(record.data['SELECCION']){
				registrosEnviar.push(record.data);
				bOk = true;
			}		
		});
		
		if(bOk){

			Ext.MessageBox.confirm('Confirmaci�n','�Esta seguro de eliminar los registrsos seleccionados?',function(msb){
				if(msb=='yes'){
					var pnlMenus = Ext.getCmp('pnlMenus');
					pnlMenuEl = pnlMenus.getEl();
					pnlMenuEl.mask('Eliminando Registros...', 'x-mask-loading');
					
					Ext.Ajax.request({
						url: '02admincont01ext.data.jsp',
						params: {
							informacion: 'deleteMenuSeccion',
							registros: Ext.encode(registrosEnviar)
						},
						callback: processSuccessFailureDeleteMenuSeccion
					});
				}
			});
		}else{
			Ext.MessageBox.alert('Mensaje','Favor de seleccionar un registro');
		}
		
	};
	
	var processSuccessFailureDeleteMenuSeccion =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			Ext.Ajax.request({
				url: '02admincont01ext.data.jsp',
				params: Ext.apply(fpMenus.getForm().getValues(),{
					informacion: 'GetDataMenus'
				}),
				callback: procesarSuccessFailureGetDataMenus
			});
			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var muestraEdicionVineta = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var cveSeccion = registro.data['CVESECCION'];
		var cveMenu = registro.data['OPCION'];
		var nombreMenu = registro.data['DESCOPCION'];
		
		var hidCveSeccion = Ext.getCmp('hidCveSeccion1');
		var hidCveMenu = Ext.getCmp('hidCveMenu1');
		var textoDescripcion = Ext.getCmp('textoDescripcion1');
		var tipoEvento = Ext.getCmp('tipoEvento1');
		
		hidCveSeccion.setValue(cveSeccion);
		hidCveMenu.setValue(cveMenu);
		textoDescripcion.setValue('');
		tipoEvento.setValue('A');
		
		gridVinetas.setTitle(nombreMenu);
		
		storeVinetasData.load({
			params:{
				cveSeccion: cveSeccion,
				cveMenu: cveMenu
			}
		});
		
	};
	
	var procesarDataVinetas = function(store, arrRegistros, opts) {
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!gridVinetas.isVisible()) {
				//contenedorPrincipalCmp.add(gridTextoLogo);
				//contenedorPrincipalCmp.doLayout();
			}
			
			var ventana = Ext.getCmp('winEditVinetas');
				if (ventana) {
					ventana.show();
				} else {
					new Ext.Window({
						modal: true,
						resizable: false,
						layout: 'form',
						x: 300,
						width: 610,
						//height: 310,
						id: 'winEditVinetas',
						closeAction: 'hide',
						items: [fpEdicionVineta,gridVinetas],
						title: 'Edici�n de Vi�etas'
					}).show();
				}
			
			var el = gridVinetas.getGridEl();
			
			if(store.getTotalCount() > 0) {
				el.unmask();
				
				
				
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	};
	
	
	var processSuccessFailureDeleteVinetas =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			var textoDescripcion = Ext.getCmp('textoDescripcion1');
			textoDescripcion.setValue('');

			storeVinetasData.load({
				params: Ext.apply(fpEdicionVineta.getForm().getValues(),{
					informacion: 'GetDataVinetas'
				})
			});
			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	
	var fnAgregarMenus = function (){
		
		var ventana = Ext.getCmp('winAgregarVineta');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									modal: true,
									resizable: false,
									layout: 'form',
									x: 300,
									width: 667,
									//height: 310,
									id: 'winAgregarVineta',
									closeAction: 'hide',
									items: [fpAgregarVineta],
									title: '',
									listeners:{
										beforehide: function(win){
											fpAgregarVineta.getForm().reset();
											var fsAgregadoVinetas = Ext.getCmp('fsAgregadoVinetas1');
											fsAgregadoVinetas.removeAll();
											fsAgregadoVinetas.doLayout();
										}
									}
								}).show();
							}
	};
	
	
	var procesarSuccessFailureAddNewMenuSeccion =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var ventana = Ext.getCmp('winAgregarVineta');
			var fsAgregadoVinetas = Ext.getCmp('fsAgregadoVinetas1');
			var el = fpAgregarVineta.getEl();
			el.unmask();
			
			fsAgregadoVinetas.removeAll();
			fpAgregarVineta.getForm().reset();
			ventana.hide();

			var pnlMenus = Ext.getCmp('pnlMenus');
			pnlMenuEl = pnlMenus.getEl();				
			pnlMenuEl.mask('Actualizando...','x-mask-loading');
			
			Ext.Ajax.request({
				url: '02admincont01ext.data.jsp',
				params: Ext.apply(fpMenus.getForm().getValues(),{
					informacion: 'GetDataMenus'
				}),
				callback: procesarSuccessFailureGetDataMenus
			});
			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	

	var storeDataMenus = new Ext.data.GroupingStore({	 
		root : 'registros',
		reader: new Ext.data.JsonReader({
			root : 'registros',
			totalProperty: 'total',
			fields: [
				{name: 'NOMBRESECCION'},
				{name: 'CVESECCION'},
				{name: 'SELECCION'},
				{name: 'OPCION'},
				{name: 'DESCOPCION'},
				{name: 'TITULO'},
				{name: 'ACTIVADO'},
				{name: 'NUEVO'},
				{name: 'FECMODIF'}
			]
		}),
		groupField: 'NOMBRESECCION',
		sortInfo:{field: 'NOMBRESECCION', direction: "ASC"},
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarConsultaDataMenus,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarConsultaDataMenus(null, null, null);
				}
			}
		}		
	});
	
	var storeCatSeccionesData = new Ext.data.JsonStore({
		id: 'storeCatSeccionesData1',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '02admincont01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoSecciones'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeCatMenuSeccionData = new Ext.data.JsonStore({
		id: 'storeCatMenuSeccionData1',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '02admincont01ext.data.jsp',
		baseParams: {
			informacion: 'catalogoMenuSeccion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var storeVinetasData = new Ext.data.JsonStore({
		id:'storeVinetasData1',
		root : 'registros',
		url : '02admincont01ext.data.jsp',
		baseParams: {
			informacion: 'GetDataVinetas'
		},
		fields: [
			{name: 'CEVESECCONTDET'},
			{name: 'IGORDEN'},
			{name: 'DESCRIPCION'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDataVinetas,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarDataVinetas(null, null, null);
				}
			}
		}
	});
	
	var elementosFormMenus =
	[
		{
			xtype: 'combo',
			name: 'cboSecciones',
			id: 'cboSecciones1',
			fieldLabel: 'Nombre de la Secci�n',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboSecciones',
			emptyText: 'Seleccione...',
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatSeccionesData,
			tpl : NE.util.templateMensajeCargaCombo,
			listeners:{
				change: function(combo, newVal, oldVal){
					if(newVal != oldVal){
						storeCatMenuSeccionData.load({
							params: {
								cveSeccion: newVal
							}
						});
					}
				}
			}
		},
		
		{
			xtype: 'combo',
			name: 'cboMenus',
			id: 'cboMenus1',
			fieldLabel: 'Nombre del Menu',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboMenus',
			emptyText: 'Seleccione...',
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatMenuSeccionData,
			tpl : NE.util.templateMensajeCargaCombo
		}
	];
	
	var fpMenus = new Ext.form.FormPanel({
		id: 'fpMenus1',
		width: 800,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		hidden: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormMenus,
		monitorValid: true,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				id:'btnBuscarMenus',
				formBind: true,
				handler: function(){
					var pnlMenus = Ext.getCmp('pnlMenus');
					pnlMenuEl = pnlMenus.getEl();				
					pnlMenuEl.mask('Consultando...','x-mask-loading');
					
					Ext.Ajax.request({
						url: '02admincont01ext.data.jsp',
						params: Ext.apply(fpMenus.getForm().getValues(),{
							informacion: 'GetDataMenus'
						}),
						callback: procesarSuccessFailureGetDataMenus
					});
				}
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					fpMenus.getForm().reset();
					gridMenus.hide();
				}
				
			}
		]
	});
	
	
	var fpEdicionVineta = new Ext.form.FormPanel({
		id: 'fpEdicionVineta1',
		width: 600,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		hidden: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype: 'hidden',
				name: 'cveSeccion',
				id: 'hidCveSeccion1'
			},
			{
				xtype: 'hidden',
				name: 'cveMenu',
				id: 'hidCveMenu1'
			},
			{
				xtype: 'hidden',
				name: 'cveSeccionDet',
				id: 'hidCveSeccionDet1'
			},
			{
				xtype: 'hidden',
				name: 'tipoEvento',
				id: 'tipoEvento1',
				value: 'A'
			},
			{
				xtype: 'textarea',
				name: 'textoDescripcion',
				id: 'textoDescripcion1',
				fieldLabel: 'Descripcion',
				allowBlank: false,
				blankText: 'Favor de ingresar la descripci�n'	
				/*enableKeyEvents: true,
				listeners:{
					keypress: function(field, event){							
						var val = field.getValue();
						if(event.keyCode !=8 && event.keyCode !=46){//ignorar backspace and delete
							if (val.length >= 255 ){
								event.stopEvent();
							}
						}
					}
				}*/
			}
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Guardar',
				iconCls: 'icoGuardar',
				id:'btnGuardarEdicVine',
				formBind: true,
				handler: function(){
					var tipoEvento = Ext.getCmp('tipoEvento1');
					var valTipoEvento = tipoEvento.getValue();
					var accion = 'AddNewVineta';
					if(valTipoEvento=='G')
						accion = 'ModifyVineta';
						
					Ext.Ajax.request({
						url: '02admincont01ext.data.jsp',
						params: Ext.apply(fpEdicionVineta.getForm().getValues(),{
							informacion: accion
						}),
						callback: procesarSuccessFailureAddNewVineta
					});
				}
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					var tipoEvento = Ext.getCmp('tipoEvento1');
					var textoDescripcion = Ext.getCmp('textoDescripcion1');
					
					tipoEvento.setValue('A');
					textoDescripcion.setValue('');
					//gridMenus.hide();
				}
				
			}
		]
	});
	
	
	var elementosFormDataVinetas =
	[
		{
			xtype: 'combo',
			name: 'cboSeccionesA',
			id: 'cboSeccionesA1',
			fieldLabel: 'Nombre de la Secci�n',
			mode: 'local', 
			displayField : 'descripcion',
			valueField : 'clave',
			hiddenName : 'cboSeccionesA',
			emptyText: 'Seleccione...',
			allowBlank: false,
			width: 400,
			forceSelection : true,
			triggerAction : 'all',
			typeAhead: true,
			minChars : 1,
			store : storeCatSeccionesData,
			tpl : NE.util.templateMensajeCargaCombo
		},
		{
			xtype: 'textarea',
			name: 'nombreMenu',
			id: 'nombreMenu1',
			fieldLabel: 'Nombre del Men�',
			allowBlank: false,
			//blankText: 'Favor de ingresar la descripci�n'	
			enableKeyEvents: true,
			listeners:{
				keypress: function(field, event){							
					var val = field.getValue();
					if(event.keyCode !=8 && event.keyCode !=46){//ignorar backspace and delete
						if (val.length >= 45 ){
							event.stopEvent();
						}
					}
				}
			}
		},
		{
			xtype: 'textarea',
			name: 'textoTitulo',
			id: 'textoTitulo1',
			fieldLabel: 'T�tulo',
			allowBlank: false,
			//blankText: 'Favor de ingresar la descripci�n'	
			enableKeyEvents: true,
			listeners:{
				keypress: function(field, event){							
					var val = field.getValue();
					if(event.keyCode !=8 && event.keyCode !=46){//ignorar backspace and delete
						if (val.length >= 150 ){
							event.stopEvent();
						}
					}
				}
			}
		},
		{
			xtype: 'numberfield',
			name: 'numVinetas',
			id: 'numVinetas1',
			fieldLabel: 'N�mero de Vi�etas',
			allowBlank: false,
			allowDecimals: false,
			hidden: false,
			maxLength: 2,	
			width: 50,
			msgTarget: 'side',
			margins: '0 20 0 0',
			listeners:{
				change: function(field, newVal, oldVal){
					var fsAgregadoVinetas = Ext.getCmp('fsAgregadoVinetas1');
					if(newVal==0){
						field.setValue('');
						fsAgregadoVinetas.removeAll();
						fsAgregadoVinetas.doLayout();
					}else{
						if(oldVal != newVal && newVal != ''){
							
							fsAgregadoVinetas.removeAll();
							for(var i=0; i<newVal ; i++){
								var name = 'newVineta'+i;
								var objTextArea = new Ext.form.TextArea({
									name: name,
									id: name,
									fieldLabel: i+1+'',
									allowBlank: false
								});
								fsAgregadoVinetas.add(objTextArea);
							}
							
							fsAgregadoVinetas.doLayout();
						}
					}
				}
			}
		},
		{
			xtype: 'radiogroup', fieldLabel:'Nuevo', id:'servicio', 
			defaults: {xtype: "radio",name: "csNuevo"},
			items:[{
				boxLabel: 'Si',
				inputValue: 'S'
			},{
				boxLabel: 'No',
				inputValue: 'N',
				checked: true
			}]
		},
		{
			// Fieldset in Column 1
			xtype:'fieldset',
			name: 'fsAgregadoVinetas',
			id: 'fsAgregadoVinetas1',
			title: 'Agregar Vi�etas',
			collapsible: true,
			autoHeight:true,
			labelWidth: 50,
			defaults: {
					anchor: '-20' // leave room for error icon
			},
			defaultType: 'textarea' 
		}
	];
	
	var fpAgregarVineta = new Ext.form.FormPanel({
		id: 'fpAgregarVineta1',
		width: 660,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		hidden: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: elementosFormDataVinetas,
		monitorValid: true,
		buttons: [
			{
				text: 'Guardar',
				iconCls: 'icoGuardar',
				id:'btnGuardarNewVineta',
				formBind: true,
				handler: function(){
					
					var fpAgVineta = fpAgregarVineta.getEl();				
					fpAgVineta.mask('Guardando...','x-mask-loading');
					
					Ext.Ajax.request({
						url: '02admincont01ext.data.jsp',
						params: Ext.apply(fpAgregarVineta.getForm().getValues(),{
							informacion: 'AddNewMenuSeccion'
						}),
						callback: procesarSuccessFailureAddNewMenuSeccion
					});
				}
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					fpAgregarVineta.getForm().reset();
					var fsAgregadoVinetas = Ext.getCmp('fsAgregadoVinetas1');
					fsAgregadoVinetas.removeAll();
					fsAgregadoVinetas.doLayout();
				}
				
			}
		]
	});
	
	var gridMenus = new Ext.grid.EditorGridPanel({
			id: 'gridMenus1',
			store: storeDataMenus,
			margins: '20 0 0 0',
			style: 'margin: 0 auto',
			title: ' ',
			clicksToEdit: 1,
			viewConfig: {
				templates: {
					cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
						'</td>'
					)
				}
			},
			columns: [
				{
					header: 'Secci�n',
					tooltip: 'Secci�n',
					dataIndex: 'NOMBRESECCION',
					sortable: true,
					width: 0,
					resizable: true,
					hidden: true,
					renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
				},
				{
					xtype: 'checkcolumn',
					header : '',
					dataIndex : 'SELECCION',
					hideable: false,
					width : 50,
					sortable : false
				},
				{
					header: 'clave',
					tooltip: 'clave',
					dataIndex: 'CVESECCION',
					sortable: true,
					hideable: false,
					width: 50,
					resizable: true,
					hidden: true,
					renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
				},
				{
					header: 'Men�s',
					tooltip: 'Men�s',
					dataIndex: 'DESCOPCION',
					sortable: true,
					width: 255,
					resizable: true,
					hidden: false,
					editor: {
							 xtype: 'textfield'
						},
					renderer:  function (causa, columna, registro){
								columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
								return causa;
					}
				},
				{
					header : 'T�tulo',
					tooltip: 'T�tulo',
					dataIndex : 'TITULO',
					width : 350,
					sortable : true,
					editor: {
							 xtype: 'textfield'
						},
					renderer:  function (causa, columna, registro){
								columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
								return causa;
					}
				},
				{
					header : 'Vi�etas',
					tooltip: 'Vi�etas',
					xtype:	'actioncolumn',
					dataIndex : 'OPCION',
					width:	80,	align: 'center', hidden: false,
					renderer: function(value, metadata, record, rowindex, colindex, store) {
								
					},
					items: [
						{
							getClass: function(value, metadata, record, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Ver';
									return 'modificar';
							},
							handler:	muestraEdicionVineta
						}
					]
				},
				{
					xtype: 'checkcolumn',
					header : 'Nuevo',
					dataIndex : 'ACTIVADO',
					width : 50,
					sortable : false//						return '<div class="x-grid3-check-col'+(csNuevo=='S'?'-on':'')+' x-grid3-cc-'+this.id+'"> </div>';
				},
				{
					header: '',
					tooltip: '',
					dataIndex: 'NUEVO',
					sortable: true,
					width: 50,
					editable: false,
					hideable: false,
					resizable: true,
					hidden: true
				},
				{
					header: 'Fecha de Modificaci�n',
					tooltip: 'Fecha de Modificaci�n', 
					dataIndex: 'FECMODIF',
					sortable: true,
					width: 133,
					resizable: true,
					hidden: false,
					renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
				}
			],
			view: new Ext.grid.GroupingView({
				//forceFit:true,
				groupTextTpl: '{text}'					 
			}),
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 350,
			width: 800,
			frame: true,
			hidden: true,
			bbar: {
				xtype: 'toolbar',
				items: [
					'->',
					'-',
					{
					text: 'Agregar',
					id: 'btnAgregarMenus',
					iconCls: 'aceptar',
					handler: fnAgregarMenus
					},
					'-',
					{
					text: 'Guardar',
					id: 'btnGuardarMenus',
					iconCls: 'icoGuardar',
					handler: fnGuardarMenus
					},
					'-',
					{
					text: 'Eliminar',
					id: 'btnEliminarMenus',
					iconCls: 'borrar',
					handler: fnEliminarMenus
					}
				]
			}
	});
	
	var selectModelVineta = new Ext.grid.CheckboxSelectionModel({
      checkOnly: true
  });

	
	var gridVinetas = new Ext.grid.GridPanel({
		id: 'gridVinetas1',
		store: storeVinetasData,
		margins: '20 0 0 0',
		hideHeaders : false,
		sm: selectModelVineta,
		columns: [
			selectModelVineta,
			{
				header : 'N�mero',
				dataIndex : 'IGORDEN',
				width : 60,
				sortable : true
			},
			{
				header : 'Descripci�n',
				tooltip: 'Descripci�n',
				dataIndex : 'DESCRIPCION',
				width : 500,
				sortable : true,
				renderer:  function (causa, columna, registro){
					columna.attr = "ext:qtip='" + Ext.util.Format.nl2br(causa) + "'";
					return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 600,
		height: 220,
		title: 'Vi�etas',
		frame: true,
		listeners:{
			cellclick: function(grid, rowIndex, columnIndex){
				var record = grid.getStore().getAt(rowIndex);
				var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
				if(fieldName=='IGORDEN'){
					var textoDescripcion = Ext.getCmp('textoDescripcion1');
					var hidCveSeccionDet = Ext.getCmp('hidCveSeccionDet1');
					var tipoEvento = Ext.getCmp('tipoEvento1');
					var data = record.get('DESCRIPCION');
					
					hidCveSeccionDet.setValue(record.get('CEVESECCONTDET'));
					tipoEvento.setValue("G");
					textoDescripcion.setValue(data);
				}
			}
		},
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
				text: 'Eliminar',
				id: 'btnEliminarVinetas',
				iconCls: 'borrar',
				handler: function(){
						var bOk = false
						var indiceSm = 0;
						var registrosEnviar = [];
						
						gridVinetas.getStore().each(function(record) {
							if(selectModelVineta.isSelected(indiceSm)){
								registrosEnviar.push(record.data);
								bOk = true;
							}
							
							indiceSm = indiceSm+1;				
						});
		
						if(bOk){
							Ext.MessageBox.confirm('Confirmaci�n','�Esta seguro de eliminar los registrsos seleccionados?',function(msb){
								if(msb=='yes'){
									Ext.Ajax.request({
										url: '02admincont01ext.data.jsp',
										params: {
											informacion: 'deleteVinetas',
											registros: Ext.encode(registrosEnviar)
										},
										callback: processSuccessFailureDeleteVinetas
									});
								}
							});
						}else{
							Ext.MessageBox.alert('Mensaje','Favor de seleccionar un registro');
						}
					
					}
				}
			]
		}
	});


//MENUS .................................................................................FIN


//LINKS..................................................................................INI

	var procesarDataLinks = function(store, arrRegistros, opts) {
		
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!gridLinks.isVisible()) {
				gridLinks.show();
			}
			
			var el = gridLinks.getGridEl();
			store.rejectChanges();
			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	};
	
	var fnGuardarLink = function(){
		var store = gridLinks.getStore();
		var modificados = store.getModifiedRecords();
		var registrosEnviar = [];
		var bExisteInfo = false;
		
		if(!Ext.isEmpty(modificados)){
			Ext.each(modificados, function(record) {
				if(record.data['NOMBRELINK']=='' || record.data['URLLINK']=='') bExisteInfo = true;
				registrosEnviar.push(record.data);
			});
			
			if(bExisteInfo){
				Ext.MessageBox.alert('Mensaje','Favor de ingresar Descripcion y URL');
			}else{
				var pnlLinks = Ext.getCmp('pnlLinks');
				pnlLinkEl = pnlLinks.getEl();				
				pnlLinkEl.mask('Guardando Modificaciones...','x-mask-loading');
				
				Ext.Ajax.request({
					url: '02admincont01ext.data.jsp',
					params: {
						informacion: 'SaveDataLinks',
						registros: Ext.encode(registrosEnviar)
					},
					callback: processSuccessFailureSaveDataLinks
				});
			}
		}else{
			Ext.MessageBox.alert('Mensaje','No existen registros modificados');
		}
	};
	
	var processSuccessFailureSaveDataLinks =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			var pnlLinks = Ext.getCmp('pnlLinks');
			pnlLinkEl = pnlLinks.getEl();
			pnlLinkEl.unmask();
			
			storeDataLinks.load();
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};

	var storeDataLinks = new Ext.data.JsonStore({
		id:'storeDataLinks1',
		root : 'registros',
		url : '02admincont01ext.data.jsp',
		baseParams: {
			informacion: 'GetDataLinks'
		},
		fields: [
			{name: 'CVELINK'},
			{name: 'NOMBRELINK'},
			{name: 'URLLINK'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDataLinks,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarDataLinks(null, null, null);
				}
			}
		}
	});
	
	
	var gridLinks = new Ext.grid.EditorGridPanel({
		id: 'gridLinks1',
		store: storeDataLinks,
		margins: '20 0 0 0',
		style: 'margin: 0 auto',
		title: 'LINKS',
		clicksToEdit: 1,
		viewConfig: {
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
					'</td>'
				)
			}
		},
		columns: [
			{
				header : 'Descripci�n',
				tooltip: 'Descripci�n',
				dataIndex : 'NOMBRELINK',
				width : 200,
				sortable : true,
				//align: 'center',
				editor: {
						 xtype: 'textfield'
					},
				renderer:  function (causa, columna, registro){
							columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
							return causa;
				}
			},
			{
				header : 'URL',
				tooltip: 'URL',
				dataIndex : 'URLLINK',
				width : 400,
				sortable : true,
				//align: 'center',
				editor: {
						 xtype: 'textfield'
					},
				renderer:  function (causa, columna, registro){
							columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
							return causa;
				}
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		height: 250,
		width: 618,
		frame: true,
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
				text: 'Guardar',
				id: 'btnGuardarLink',
				iconCls: 'icoGuardar',
				handler: fnGuardarLink
				},
				'-',
				{
				text: 'Cancelar',
				id: 'btnCancelarLink',
				iconCls: 'cancelar',
				handler: function(){
						storeDataLinks.rejectChanges();
						storeDataLinks.load();
					}	
				}
			]
		}
	});


//LINKS..................................................................................FIN  

//BANNERS................................................................................INI

	var procesarDataBanners = function(store, arrRegistros, opts) {
		var contenedorPrincipalCmp = Ext.getCmp('contenedorPrincipal');
		if (arrRegistros != null) {
			if (!gridBanners.isVisible()) {
				gridBanners.show();
			}
			var el = gridBanners.getGridEl();

			if(store.getTotalCount() > 0) {
				objBanner.totReg = store.getTotalCount();
				objBanner.regActivos = 0;
				objBanner.regInactivos = 0;
				objBanner.regActivosIni = 0;
				objBanner.regInactivosIni = 0;
				store.each(function(record) {
						if(record.get('ACTIVOBANN')=='S'){
							objBanner.regActivosIni = objBanner.regActivosIni + 1;
						}else{
							objBanner.regInactivosIni = objBanner.regInactivosIni + 1;
						}
				});
				
				
				
				if(objBanner.regActivosIni==4 && objBanner.totReg==4){
					Ext.getCmp('btnEstatusBanner').disable();
				}else{
					Ext.getCmp('btnEstatusBanner').enable();
				};
				
				if(store.getTotalCount()>=6) {
					Ext.getCmp('btnAgregarBanner').disable();
				}else{
					Ext.getCmp('btnAgregarBanner').enable();
				}
				if(store.getTotalCount()<=4) {
					Ext.getCmp('btnEliminarBanner').disable();
				}else{
					Ext.getCmp('btnEliminarBanner').enable();
				}
				el.unmask();
			}else {
				objBanner.totReg = 0;
				objBanner.regActivos = 0;
				objBanner.regInactivos = 0;
				objBanner.regActivosIni = 0;
				objBanner.regInactivosIni = 0;
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	};
	
	var muestraCargaBanner = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var cveBanner = registro.data['CVEBANNER'];
		
		var hidCveBanner = Ext.getCmp('hidCveBanner');
		hidCveBanner.setValue(cveBanner);
		
		var ventana = Ext.getCmp('winCargaBanner');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									modal: true,
									resizable: false,
									layout: 'form',
									x: 300,
									width: 667,
									//height: 310,
									id: 'winCargaBanner',
									closeAction: 'hide',
									items: [fpCargaBanner],
									title: '',
									listeners:{
										beforehide: function(win){
												var cargaArchivo = Ext.getCmp('archivoBanner');
												cargaArchivo.setValue('');
										}
									}
								}).show();
							}
	};
	
	var muestraImagenBanner = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var cveBanner = registro.data['CVEBANNER'];
		
		Ext.Ajax.request({
			url: '02admincont01ext.data.jsp',
			params: {
				informacion: 'GetImageBanner',
				cveBanner: cveBanner
			},
			callback: processSuccessFailureGetImageBanner
		});

	};
	
	var processSuccessFailureGetImageBanner =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			var objMsg = Ext.getCmp('pnlImageBanner1');
			objMsg.setWidth('auto');
			objMsg.body.update('<img src="'+resp.urlImg+'" border="0" alt="">');
			objMsg.show();
			var anchoImg = objMsg.getWidth();
			if(anchoImg>800){
				objMsg.body.update('<img src="'+resp.urlImg+'" width="800" border="0" alt="">');
				objMsg.show();
			}else{
				objMsg.setWidth(800);
				objMsg.show();
			}
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var muestraCargaBannerFondo = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var cveBanner = registro.data['CVEBANNER'];
		
		var hidCveBannerFondo = Ext.getCmp('hidCveBannerF');
		hidCveBannerFondo.setValue(cveBanner);
		
		var ventana = Ext.getCmp('winCargaBannerFondo');
							if (ventana) {
								ventana.show();
							} else {
								new Ext.Window({
									modal: true,
									resizable: false,
									layout: 'form',
									x: 300,
									width: 667,
									//height: 310,
									id: 'winCargaBannerFondo',
									closeAction: 'hide',
									items: [fpCargaBannerFondo],
									title: '',
									listeners:{
										beforehide: function(win){
												var cargaArchivo = Ext.getCmp('archivoBannerF');
												cargaArchivo.setValue('');
										}
									}
								}).show();
							}
	};
	
	var muestraImagenBannerFondo = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var cveBanner = registro.data['CVEBANNER'];
		
		Ext.Ajax.request({
			url: '02admincont01ext.data.jsp',
			params: {
				informacion: 'GetImageBannerFondo',
				cveBanner: cveBanner
			},
			callback: processSuccessFailureGetImageBanner
		});

	};
	
	
	var muestraEditContBanner = function (grid,rowIndex,colIndex,item,event){
		var registro = grid.getStore().getAt(rowIndex);
		var cveBanner = registro.data['CVEBANNER'];
		var hidCveBanner = Ext.getCmp('hidCveBanner1');
		
		hidCveBanner.setValue(cveBanner);
		
		storeDataEditBanner.load({
			params:{
				cveBanner: cveBanner
			}
		});
		

	};
	
	var procesarDataEditBanners = function(store, arrRegistros, opts) {
		
		if (arrRegistros != null) {
			if (!gridEditBanner.isVisible()) {
					gridEditBanner.show();
			}
						
			var hidCveBanner = Ext.getCmp('hidCveBanner1');
			var cveBanner = hidCveBanner.getValue();

			storeCatPosicionData.load({
				params:{
					cveBanner: cveBanner
				}
			});
			
			var ventana = Ext.getCmp('winEditBanner');
			if (ventana) {
				ventana.show();
			} else {
				new Ext.Window({
					modal: true,
					resizable: false,
					layout: 'form',
					x: 300,
					y: 50,
					width: 667,
					//height: 310,
					id: 'winEditBanner',
					closeAction: 'hide',
					items: [fpEdicionBanner,gridEditBanner],
					title: 'Contenido Banner',
					listeners:{
						beforehide: function(win){
								fpEdicionBanner.getForm().reset();
						}
					}
				}).show();
			}
				
			var el = gridEditBanner.getGridEl();


			if(store.getTotalCount() > 0) {
				el.unmask();
			}else {
				el.mask('No se encontr� ning�n registro', 'x-mask');
			}
			
		}
	};
	
	var procesarSuccessFailureAddDetBanner =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			//var pnlMenus = Ext.getCmp('pnlMenus');
			//pnlMenuEl = pnlMenus.getEl();
			//pnlMenuEl.unmask();
			var textTituloDB = Ext.getCmp('textTituloDB1');
			var textTextoDB = Ext.getCmp('textTextoDB1');
			var tipoEvento = Ext.getCmp('tipoEventoBanner1');
			var posicionDetBanner = Ext.getCmp('cboPosicion1');
			
			textTituloDB.setValue('');
			textTextoDB.setValue('');
			posicionDetBanner.setValue('');
					
			tipoEvento.setValue('A');

			var hidCveBanner = Ext.getCmp('hidCveBanner1');
			
			storeDataEditBanner.load({
				params:{
					cveBanner: hidCveBanner.getValue()
				}
			});

			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var processSuccessFailureDeleteDetBanner =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			var textTituloDB = Ext.getCmp('textTituloDB1');
			var textTextoDB = Ext.getCmp('textTextoDB1');
			var posicion = Ext.getCmp('cboPosicion1');
			textTituloDB.setValue('');
			textTextoDB.setValue('');
			posicion.setValue('');

			var hidCveBanner = Ext.getCmp('hidCveBanner1');
			
			storeDataEditBanner.load({
				params:{
					cveBanner: hidCveBanner.getValue()
				}
			});
			
		
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var fnGuardarBanner = function(){
		var store = gridBanners.getStore();
		var modificados = store.getModifiedRecords();
		var registrosEnviar = [];
		var bNoTexto = false;
		if(!Ext.isEmpty(modificados)){
			Ext.each(modificados, function(record) {
				//if(record.data['NOMBREBANN']=='' || record.data['DESCBANN']=='') bNoTexto = true;
				if(record.data['NOMBREBANN']=='' ) bNoTexto = true;
				registrosEnviar.push(record.data);
			});
			
			if(bNoTexto){
				Ext.MessageBox.alert('Mensaje','Favor de ingresar Nombre del Banner');
			}else{
			
				Ext.Ajax.request({
					url: '02admincont01ext.data.jsp',
					params: {
						informacion:  'modifyTextBanners',
						registros: Ext.encode(registrosEnviar)
					},
					callback: processSuccessFailureModifyTextBanners
				});
			}
			
		}else{
			Ext.MessageBox.alert('Mensaje','No existen registros modificados');
		}
	};
	
	var processSuccessFailureModifyTextBanners =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			gridBanners.getStore().rejectChanges();
			storeDataBanners.load({
				params: Ext.apply(fpBannerForm.getForm().getValues())
			});
			
			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var fnEliminarBanner = function(){
		var bOk = false;
		var indiceSm = 0;
		var registrosEnviar = [];
		var regAeliminar = 0;
		
		var regActivos = 0;
		var regInactivos = 0;
		
		gridBanners.getStore().each(function(record) {
			if(record.data['ACTIVOBANN']=='S'){
				regActivos = regActivos +1;
			}else{
				regInactivos = regInactivos +1;
			}
			if(selectModelBanner.isSelected(indiceSm)){
				if(record.data['ACTIVOBANN']=='S'){
					regActivos = regActivos -1;
				}else{
					regInactivos = regInactivos -1;
				}
				registrosEnviar.push(record.data);
				regAeliminar = regAeliminar+1;
				bOk = true;
			}
			
			indiceSm = indiceSm+1;				
		});
		
		var totRegRestante = objBanner.totReg - regAeliminar;
		if(totRegRestante<4 || regActivos<4){
			Ext.MessageBox.alert('Mensaje','No puede eliminar los registros seleccionado, debe existir al menos 4 banners activos');
		}else{
			if(bOk){
				Ext.MessageBox.confirm('Confirmaci�n','�Esta seguro de eliminar los registrsos seleccionados?',function(msb){
					if(msb=='yes'){
						Ext.Ajax.request({
							url: '02admincont01ext.data.jsp',
							params: {
								informacion: 'deleteBanners',
								registros: Ext.encode(registrosEnviar)
							},
							callback: processSuccessFailureDeleteBanner
						});
					}
				});
			}else{
				Ext.MessageBox.alert('Mensaje','Favor de seleccionar un registro');
			}
		}
		
	};
	
	
	var processSuccessFailureDeleteBanner =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			storeCatBannerData.load();
			storeDataBanners.load({
				params: Ext.apply(fpBannerForm.getForm().getValues())
			});

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	
	var fnCambiaEstatusBanner = function(){
		var bOk = false;
		var indiceSm = 0;
		var contActivos = 0;
		var contInactivos = 0;
		var registrosEnviar = [];
		objBanner.Inactivos = 0;
		objBanner.regInactivos = 0;
		
		gridBanners.getStore().each(function(record) {
			
			if(selectModelBanner.isSelected(indiceSm)){
				if(record.data['ACTIVOBANN']=='N'){
				 objBanner.regActivos = objBanner.regActivos+1;
				}
				
				if(record.data['ACTIVOBANN']=='S'){
				 objBanner.regInactivos = objBanner.regInactivos+1;
				}
				
				registrosEnviar.push(record.data);
				bOk = true;
			}
			
			indiceSm = indiceSm+1;				
		});
		
		var totRegAct = objBanner.regActivosIni - objBanner.regInactivos + objBanner.regActivos;
		
		if(totRegAct<4){
			Ext.MessageBox.alert('Mensaje','No pueden existir menos de 4 Banners activos');
		}else{
			if(bOk ){
				Ext.MessageBox.confirm('Confirmaci�n','�Esta seguro de cambiar el estatus de los registrsos seleccionados?',function(msb){
					if(msb=='yes'){
						Ext.Ajax.request({
							url: '02admincont01ext.data.jsp',
							params: {
								informacion: 'invertirEstatusBanners',
								registros: Ext.encode(registrosEnviar)
							},
							callback: processSuccessFailureInvertirEstatusBanner
						});
					}
				});
			}else{
				Ext.MessageBox.alert('Mensaje','Favor de seleccionar un registro');
			}
		}
		
	};
	
	
	var processSuccessFailureInvertirEstatusBanner =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);

			gridBanners.getStore().rejectChanges();
			//storeDataBanners.load();
			storeDataBanners.load({
				params: Ext.apply(fpBannerForm.getForm().getValues())
			});

		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	var fnAgregarBanner = function (){
		fpBannerForm.hide();
		fpAgregarBanner.show();
	};
	
	
	
	var procesarSuccessFailureAddNewDetBanner =  function(opts, success, response) {
		if (success == true && Ext.util.JSON.decode(response.responseText).success == true) {
			var resp = 	Ext.util.JSON.decode(response.responseText);
			
			var textTituloDB = Ext.getCmp('newTextTituloDB1');
			var textTextoDB = Ext.getCmp('newTextTextoDB1');
			var posicionDetBanner = Ext.getCmp('newCboPosicion1');
			
			textTituloDB.setValue('');
			textTextoDB.setValue('');
			posicionDetBanner.setValue('');
			
			storeNewCatPosicionData.load({
				params:{
					cveBanner: Ext.getCmp('newHidCveBanner1').getValue()
				}
			});
			
			var elFpNewDetB = fpNewDetBanner.getEl();
			elFpNewDetB.unmask();

			
		} else {
			NE.util.mostrarConnError(response,opts);
		}
	};
	
	
	var selectModelEditBanner = new Ext.grid.CheckboxSelectionModel({
      checkOnly: true
  });
	
	var selectModelBanner = new Ext.grid.CheckboxSelectionModel({
      checkOnly: true,
		  listeners: {
				rowselect: function(selectModel, rowIndex, record) {
					
					/*if(record.data['ACTIVOBANN']=='S'){
						objBanner.regInactivos = objBanner.regInactivos + 1;
						objBanner.regActivos = objBanner.regActivos - 1;
					}
					if(record.data['ACTIVOBANN']=='N'){
						objBanner.regInactivos = objBanner.regInactivos - 1;
						objBanner.regActivos = objBanner.regActivos + 1;
					}
					
					if(objBanner.regActivos==4 && objBanner.totReg==4){
						Ext.getCmp('btnEstatusBanner').disable();
					}else if(objBanner.regInactivos > (objBanner.totReg-4)){
						Ext.getCmp('btnEstatusBanner').disable();
					}else{
						Ext.getCmp('btnEstatusBanner').enable();
					}
					*/
				},
				rowdeselect: function(selectModel, rowIndex, record) {
					//alert('se');
					/*
					if(record.data['ACTIVOBANN']=='N'){
						objBanner.regInactivos = objBanner.regInactivos + 1;
						objBanner.regActivos = objBanner.regActivos - 1;
					}
					if(objBanner.regActivos==4 && objBanner.totReg==4){
						Ext.getCmp('btnEstatusBanner').disable();
					}else if(objBanner.regInactivos > (objBanner.totReg-4)){
						Ext.getCmp('btnEstatusBanner').disable();
					}else{
						Ext.getCmp('btnEstatusBanner').enable();
					}*/
				},
				beforerowselect: function( selectModel, rowIndex, keepExisting, record ){
					
					/*if(selectModel.isSelected(rowIndex)){
						
						return false;
					}*/
				}
			}
						
  });
	
	
	var storeCatBannerData = new Ext.data.JsonStore({
		id: 'storeCatBannerData1',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '02admincont01ext.data.jsp',
		baseParams: {
			informacion: 'getComboBanners'
		},
		totalProperty : 'total',
		autoLoad: true,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	var storeDataBanners = new Ext.data.JsonStore({
		id:'storeDataBanners1',
		root : 'registros',
		url : '02admincont01ext.data.jsp',
		baseParams: {
			informacion: 'GetDataBanners'
		},
		fields: [
			{name: 'CVEBANNER'},
			{name: 'NOMBREBANN'},
			//{name: 'DESCBANN'},
			{name: 'ACTIVOBANN'},
			{name: 'FECMODBANN'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDataBanners,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarDataBanners(null, null, null);
				}
			}
		}
	});
	
		var storeDataEditBanner = new Ext.data.JsonStore({
		id:'storeDataEditBanner1',
		root : 'registros',
		url : '02admincont01ext.data.jsp',
		baseParams: {
			informacion: 'GetDataDetBanners'
		},
		fields: [
			{name: 'CVEBANNER'},
			{name: 'CVEPOSICION'},
			{name: 'TITULOBANNEDIT'},
			{name: 'TEXTOBANNEDIT'},
			{name: 'POSICIONBANNEDIT'}
		],
		totalProperty : 'total',
		messageProperty: 'msg',
		autoLoad: false,
		listeners: {
			load: procesarDataEditBanners,
			exception: {
				fn: function(proxy, type, action, optionsRequest, response, args) {
					NE.util.mostrarDataProxyError(proxy, type, action, optionsRequest, response, args);
					//LLama procesar consulta, para que desbloquee los componentes.
					procesarDataEditBanners(null, null, null);
				}
			}
		}
	});
	
	var storeCatPosicionData = new Ext.data.JsonStore({
		id: 'storeCatPosicionData1',
		pruneModifiedRecords : true,
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '02admincont01ext.data.jsp',
		baseParams: {
			informacion: 'getComboPosicion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var storeNewCatPosicionData = new Ext.data.JsonStore({
		id: 'storeNewCatPosicionData1',
		root : 'registros',
		fields : ['clave', 'descripcion', 'loadMsg'],
		url : '02admincont01ext.data.jsp',
		baseParams: {
			informacion: 'getComboPosicion'
		},
		totalProperty : 'total',
		autoLoad: false,
		listeners: {
			exception: NE.util.mostrarDataProxyError,
			beforeload: NE.util.initMensajeCargaCombo
		}
	});
	
	
	var elementosFormCargaBanner = [
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			id:'cargaArchivoBanner',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'panel',
					id:		'pnlArchivoBanner',
					columnWidth: 1,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 100,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivoBanner1',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivoBanner',
									width: 400,
									labelWidth: 200,
									emptyText: 'Ruta del Archivo',
									fieldLabel: 'Carga imagen banner',
									name: 'archivoBanner',
									buttonText: 'Examinar...',
									buttonCfg: {
									  iconCls: 'upload-icon'
									},
									//anchor: '95%',
									regex: /^.*\.(jpg|JPG|jpeg|JPEG)$/,
									regexText:'Solo se admiten archivos JPG y JPEG'
								},
								{
									xtype: 'hidden',
									id:	'hidExtensionBanner',
									name:	'hidExtensionBanner',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidCveBanner',
									name:	'hidCveBanner',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidTipoContBanner',
									name:	'hidTipoContBanner',
									value:	''
								},
								{
									xtype: 	'button',
									text: 	'Continuar',
									id: 		'btnContinuarBanner',
									iconCls: 'icoContinuar',
									style: { 
										  marginBottom:  '10px' 
									},
									handler: function(){

										var cargaArchivo = Ext.getCmp('archivoBanner');
										if (!cargaArchivo.isValid()){
											cargaArchivo.focus();
											return;
										}
										if(cargaArchivo.getValue()==''){
											cargaArchivo.markInvalid("Favor de seleccionar una imagen");
											cargaArchivo.focus();
											return;
										}
										var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
										var extArchivo = Ext.getCmp('hidExtensionBanner');
										var tipoContenido = Ext.getCmp('hidTipoContBanner');
										
										if (/^.*\.(jpg|jpeg)$/.test(ifile)){
											extArchivo.setValue(ifile.substring(ifile.indexOf('.')+1));
										}

										fpCargaBanner.el.mask('Procesando...', 'x-mask-loading');
										tipoContenido.setValue('S');
										fpCargaBanner.getForm().submit({
											url: '02admincont01extfile.jsp',
											waitMsg: 'Enviando datos...',
											success: function(form, action) {
												if (action.result.flag){
													Ext.getCmp('archivoBanner').setValue('');
													tipoContenido.setValue('');
													fpCargaBanner.el.unmask();
													var ventana = Ext.getCmp('winCargaBanner');
													ventana.hide();
												
												}else{
													tipoContenido.setValue('');
													fpCargaBanner.el.unmask();
													cargaArchivo.markInvalid(action.result.msgError);
													return;
												}
											},
											failure: NE.util.mostrarSubmitError
										})
											
									}
								}
							]
						}
							
					]
				}
			]
		}
	];
	
	var elementosFormCargaBannerFondo = [
		{
			xtype:	'panel',
			layout:	'column',
			width: 690,
			anchor: '100%',
			id:'cargaArchivoBannerF',
			defaults: {
				bodyStyle:'padding:4px'
			},
			items:	[
				{
					xtype: 'panel',
					id:		'pnlArchivoBannerF',
					columnWidth: 1,
					anchor: '100%',
					layout: 'form',
					fileUpload: true,
					labelWidth: 100,
					defaults: {
						bodyStyle:'padding:5px',
						msgTarget: 'side',
						anchor: '-20'
					},
					items: [
						{
							xtype: 'compositefield',
							fieldLabel: '',
							id: 'cargaArchivoBannerF1',
							combineErrors: false,
							msgTarget: 'side',
							items: [
								{
									xtype: 'fileuploadfield',
									id: 'archivoBannerF',
									width: 400,
									labelWidth: 200,
									emptyText: 'Ruta del Archivo',
									fieldLabel: 'Cargar imagen de fondo',
									name: 'archivoBannerF',
									buttonText: 'Examinar...',
									buttonCfg: {
									  iconCls: 'upload-icon'
									},
									//anchor: '95%',
									regex: /^.*\.(jpg|JPG|jpeg|JPEG)$/,
									regexText:'Solo se admiten archivos JPG y JPEG'
								},
								{
									xtype: 'hidden',
									id:	'hidExtensionBannerF',
									name:	'hidExtensionBannerF',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidCveBannerF',
									name:	'hidCveBannerF',
									value:	''
								},
								{
									xtype: 'hidden',
									id:	'hidTipoContBannerF',
									name:	'hidTipoContBannerF',
									value:	''
								},
								{
									xtype: 	'button',
									text: 	'Continuar',
									id: 		'btnContinuarBannerF',
									iconCls: 'icoContinuar',
									style: { 
										  marginBottom:  '10px' 
									},
									handler: function(){

										var cargaArchivo = Ext.getCmp('archivoBannerF');
										if (!cargaArchivo.isValid()){
											cargaArchivo.focus();
											return;
										}
										if(cargaArchivo.getValue()==''){
											cargaArchivo.markInvalid("Favor de seleccionar una imagen");
											cargaArchivo.focus();
											return;
										}
										var ifile = Ext.util.Format.lowercase(cargaArchivo.getValue());
										var extArchivo = Ext.getCmp('hidExtensionBannerF');
										var tipoContenido = Ext.getCmp('hidTipoContBannerF');
										
										if (/^.*\.(jpg|jpeg)$/.test(ifile)){
											extArchivo.setValue(ifile.substring(ifile.indexOf('.')+1));
										}

										fpCargaBannerFondo.el.mask('Procesando...', 'x-mask-loading');
										tipoContenido.setValue('S');
										fpCargaBannerFondo.getForm().submit({
											url: '02admincont01extfile.jsp',
											waitMsg: 'Enviando datos...',
											success: function(form, action) {
												if (action.result.flag){
													Ext.getCmp('archivoBannerF').setValue('');
													tipoContenido.setValue('');
													fpCargaBannerFondo.el.unmask();
													var ventana = Ext.getCmp('winCargaBannerFondo');
													ventana.hide();
												
												}else{
													tipoContenido.setValue('');
													fpCargaBannerFondo.el.unmask();
													cargaArchivo.markInvalid(action.result.msgError);
													return;
												}
											},
											failure: NE.util.mostrarSubmitError
										})
											
									}
								}
							]
						}
							
					]
				}
			]
		}
	];
	
	
	var fpBannerForm = new Ext.form.FormPanel({
		id: 'fpBannerForm1',
		width: 800,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		hidden: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
				{
					xtype: 'combo',
					name: 'cboBanner',
					id: 'cboBanner1',
					fieldLabel: 'Nombre Banner',
					mode: 'local', 
					displayField : 'descripcion',
					valueField : 'clave',
					hiddenName : 'cboBanner',
					emptyText: 'Seleccione...',
					allowBlank: true,
					width: 400,
					forceSelection : true,
					triggerAction : 'all',
					typeAhead: true,
					minChars : 1,
					store : storeCatBannerData,
					tpl : NE.util.templateMensajeCargaCombo
				}
			],
		monitorValid: false,
		buttons: [
			{
				text: 'Buscar',
				iconCls: 'icoBuscar',
				id:'btnBuscarBanner',
				formBind: true,
				handler: function(){
					storeDataBanners.load({
						params: Ext.apply(fpBannerForm.getForm().getValues())
					});
				}
			}
		]
	});
	
	var fpCargaBanner = new Ext.form.FormPanel({
		id: 'fpCargaBanner',
		width: 700,
		title: 'Agregar Imagen',
		frame: true,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px; text-align:left;',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [elementosFormCargaBanner,
				{
					xtype: 'panel',
					name: 'pnlMsgReqBanner',
					id: 'pnlMsgReqBanner1',
					style: 'margin:0 auto;',
					frame: false,
					hidden: false,
					html: '<div class="formas" align="left" >'+
							'<i>-La imagen debe de tener formato JPEG.</i></br>'+
							'<i>-El tama�o debe ser menor a 12288  bytes.</i></br>'+
							'<i>-Las dimensiones deben ser igual a 198 x 137 pixeles.</i>'+
							'</div>'
				
				}
			],
		monitorValid: true
	});
	
	var fpCargaBannerFondo = new Ext.form.FormPanel({
		id: 'fpCargaBannerFondo',
		width: 700,
		title: 'Agregar Imagen',
		frame: true,
		fileUpload: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px; text-align:left;',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [elementosFormCargaBannerFondo,
				{
					xtype: 'panel',
					name: 'pnlMsgReqBannerF',
					id: 'pnlMsgReqBannerF1',
					style: 'margin:0 auto;',
					frame: false,
					hidden: false,
					html: '<div class="formas" align="left" >'+
							'<i>-La imagen debe de tener formato JPEG.</i></br>'+
							'<i>-El tama�o debe ser menor a 159744  bytes.</i></br>'+
							'<i>-Las dimensiones deben ser igual a 951 x 355 pixeles.</i>'+
							'</div>'
				
				}
			],
		monitorValid: true
	});
	
	var fpAgregarBanner = new Ext.form.FormPanel({
		id: 'fpAgregarBanner',
		width: 800,
		title: '',
		frame: true,
		fileUpload: true,
		hidden: true,
		style: 'margin:0 auto;',
		bodyStyle: 'padding: 6px; text-align:left;',
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [//elementosFormAgregarBanner,
				{
					xtype: 'textfield',
					name: 'newNombreBanner',
					id: 'newNombreBanner1',
					fieldLabel: 'Nombre del Baner',
					allowBlank: false,
					hidden: false,
					maxLength: 100,	
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},
				/*{
					xtype: 'textarea',
					name: 'newDescBanner',
					id: 'newDescBanner1',
					fieldLabel: 'Descripcion del Banner',
					allowBlank: false,
					hidden: false,
					maxLength: 220,	
					width: 100,
					msgTarget: 'side',
					margins: '0 20 0 0'
				},*/
				{
					xtype: 'hidden',
					id:	'hidNewTipoContBanner',
					name:	'hidNewTipoContBanner',
					value:	''
				},
				
				{
					xtype: 'fileuploadfield',
					id: 'newArchivoBanner',
					width: 400,
					labelWidth: 200,
					emptyText: 'Ruta del Archivo',
					fieldLabel: 'Carga imagen banner',
					name: 'newArchivoBanner',
					allowBlank: false,
					buttonText: 'Examinar...',
					buttonCfg: {
						iconCls: 'upload-icon'
					},
					//anchor: '95%',
					regex: /^.*\.(jpg|JPG|jpeg|JPEG)$/,
					regexText:'Solo se admiten archivos JPG y JPEG'
				},
				{
					xtype: 'hidden',
					id:	'hidNewExtensionBanner',
					name:	'hidNewExtensionBanner',
					value:	''
				},
				{
					xtype: 'panel',
					style: 'margin:0 auto;',
					frame: false,
					hidden: false,
					html: '<div class="formas" align="left" >'+
							'<i>-La imagen debe de tener formato JPEG.</i></br>'+
							'<i>-El tama�o debe ser menor a 12288  bytes.</i></br>'+
							'<i>-Las dimensiones deben ser igual a 198 x 137 pixeles.</i>'+
							'</div>'
				
				},
				NE.util.getEspaciador(20),
				{
					xtype: 'fileuploadfield',
					id: 'newArchivoBannerF',
					width: 400,
					labelWidth: 200,
					emptyText: 'Ruta del Archivo',
					fieldLabel: 'Carga imagen de fondo',
					name: 'newArchivoBannerF',
					allowBlank: false,
					buttonText: 'Examinar...',
					buttonCfg: {
						iconCls: 'upload-icon'
					},
					//anchor: '95%',
					regex: /^.*\.(jpg|JPG|jpeg|JPEG)$/,
					regexText:'Solo se admiten archivos JPG y JPEG'
				},
				{
					xtype: 'hidden',
					id:	'hidNewExtensionBannerF',
					name:	'hidNewExtensionBannerF',
					value:	''
				},
				{
					xtype: 'panel',
					style: 'margin:0 auto;',
					frame: false,
					hidden: false,
					html: '<div class="formas" align="left" >'+
							'<i>-La imagen debe de tener formato JPEG.</i></br>'+
							'<i>-El tama�o debe ser menor a 159744  bytes.</i></br>'+
							'<i>-Las dimensiones deben ser igual a 951 x 355 pixeles.</i>'+
							'</div>'
				
				}
			],
		monitorValid: true,
		buttons: [
			{
				text: 'Guardar',
				iconCls: 'icoGuardar',
				id:'btnGuardarAddBanner',
				formBind: true,
				handler: function(){

					var cargaArchivoBanner = Ext.getCmp('newArchivoBanner');
					var cargaArchivoBannerF = Ext.getCmp('newArchivoBannerF');
					if (!cargaArchivoBanner.isValid()){
						cargaArchivoBanner.focus();
						return;
					}
					
					if (!cargaArchivoBannerF.isValid()){
						cargaArchivoBannerF.focus();
						return;
					}
					
					var ifile = Ext.util.Format.lowercase(cargaArchivoBanner.getValue());
					var extArchivo = Ext.getCmp('hidNewExtensionBanner');
					var extArchivoF = Ext.getCmp('hidNewExtensionBannerF');
					var tipoContenido = Ext.getCmp('hidNewTipoContBanner');
					
					if (/^.*\.(jpg|jpeg)$/.test(ifile)){
						extArchivo.setValue(ifile.substring(ifile.indexOf('.')+1));
					}
					
					
					ifile = Ext.util.Format.lowercase(cargaArchivoBannerF.getValue());
					//var tipoContenido = Ext.getCmp('hidTipoContBanner');
					if (/^.*\.(jpg|jpeg)$/.test(ifile)){
						extArchivoF.setValue(ifile.substring(ifile.indexOf('.')+1));
					}

					fpAgregarBanner.el.mask('Procesando...', 'x-mask-loading');
					tipoContenido.setValue('S');
					fpAgregarBanner.getForm().submit({
						url: '02admincont01extfile.jsp',
						waitMsg: 'Enviando datos...',
						success: function(form, action) {
							if (action.result.flag){
								Ext.getCmp('newArchivoBanner').setValue('');
								Ext.getCmp('newArchivoBannerF').setValue('');
								tipoContenido.setValue('');
								
								fpAgregarBanner.el.unmask();
								fpAgregarBanner.hide();
								fpBannerForm.show();
								fpAgregarBanner.getForm().reset();
								
								storeCatBannerData.load();
								storeDataBanners.load();
								
								Ext.getCmp('newHidCveBanner1').setValue(action.result.newCveBanner);
								
								storeNewCatPosicionData.load({
									params:{
										cveBanner: action.result.newCveBanner
									}
								});
								
								var ventana = Ext.getCmp('winNewDetBanner');
								if (ventana) {
									ventana.show();
								} else {
									new Ext.Window({
										modal: true,
										resizable: false,
										layout: 'form',
										x: 300,
										width: 667,
										//height: 310,
										id: 'winNewDetBanner',
										closeAction: 'hide',
										items: [fpNewDetBanner],
										title: 'Contenido Banner',
										listeners:{
										beforehide: function(win){
												fpNewDetBanner.getForm().reset();
										}
									}
									}).show();
								}
							
							}else{
								tipoContenido.setValue('');
								fpAgregarBanner.el.unmask();
								if(action.result.msgError != '')
									cargaArchivoBanner.markInvalid(action.result.msgError);
								if(action.result.msgErrorB != '')
									cargaArchivoBannerF.markInvalid(action.result.msgErrorB);
								return;
							}
						},
						failure: NE.util.mostrarSubmitError
					})
						
				}
			},
			{
				text: 'Cancelar',
				hidden: false,
				iconCls: 'cancelar',
				handler: function() {
					
					fpAgregarBanner.getForm().reset();
					fpAgregarBanner.hide();
					fpBannerForm.show();
				}
				
			}
		]
	});
	
	
	var fpEdicionBanner = new Ext.form.FormPanel({
		id: 'fpEdicionBanner1',
		width: 660,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		hidden: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype: 'hidden',
				name: 'hidCveBanner',
				id: 'hidCveBanner1'
			},
			{
				xtype: 'hidden',
				name: 'tipoEventoBanner',
				id: 'tipoEventoBanner1',
				value: 'A'
			},
			{
				xtype: 'hidden',
				name: 'posicionDetBanner',
				id: 'posicionDetBanner1'
			},
			{
				xtype: 'textfield',
				name: 'textTituloDB',
				id: 'textTituloDB1',
				fieldLabel: 'T�tulo',
				allowBlank: true,
				hidden: false,
				maxLength: 100,	
				width: 100,
				msgTarget: 'side',
				margins: '0 20 0 0'  
			},
			{
				xtype: 'textarea',
				name: 'textTextoDB',
				id: 'textTextoDB1',
				fieldLabel: 'Texto',
				allowBlank: false,
				maxLength: 4000,
				enableKeyEvents: true,
				listeners:{
					keypress: function(field, event){							
						var val = field.getValue();
						if(event.keyCode !=8 && event.keyCode !=46){//ignorar backspace and delete
							if (val.length >= 4000 ){
								event.stopEvent();
							}
						}
					}
				}
			},
			{
				xtype: 'combo',
				name: 'cboPosicion',
				id: 'cboPosicion1',
				fieldLabel: 'Posici�n',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'cboPosicion',
				emptyText: 'Seleccione...',
				allowBlank: false,
				width: 400,
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store : storeCatPosicionData,
				tpl : NE.util.templateMensajeCargaCombo
			}
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Guardar',
				iconCls: 'icoGuardar',
				id:'btnGuardarEdicBann',
				formBind: true,
				handler: function(){
					var tipoEventoBanner = Ext.getCmp('tipoEventoBanner1');
					var valTipoEvento = tipoEventoBanner.getValue();
					var accion = 'AddNewDetBanner';
					if(valTipoEvento=='G')
						accion = 'ModifyDetBanner';
						
					Ext.Ajax.request({
						url: '02admincont01ext.data.jsp',
						params: Ext.apply(fpEdicionBanner.getForm().getValues(),{
							informacion: accion
						}),
						callback: procesarSuccessFailureAddDetBanner
					});
				}
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					
					var tipoEventoBanner = Ext.getCmp('tipoEventoBanner1');
					var hidCveBanner = Ext.getCmp('hidCveBanner1');
					var cveBanner = hidCveBanner.getValue();
					
					tipoEventoBanner.setValue('A');
					fpEdicionBanner.getForm().reset();
					hidCveBanner.setValue(cveBanner);
					storeCatPosicionData.load({
						params:{
							cveBanner: cveBanner
						}
					});
					
					
				}
				
			}
		]
	});
	
	
	var fpNewDetBanner = new Ext.form.FormPanel({
		id: 'fpNewDetBanner1',
		width: 660,
		style: 'margin:0 auto;',
		title: '',
		frame: true,
		collapsible: true,
		titleCollapse: false,
		hidden: false,
		bodyStyle: 'padding: 6px',
		labelWidth: 126,
		defaultType: 'textfield',
		defaults: {
			msgTarget: 'side',
			anchor: '-20'
		},
		items: [
			{
				xtype: 'hidden',
				name: 'newHidCveBanner',
				id: 'newHidCveBanner1'
			},
			{
				xtype: 'textfield',
				name: 'newTextTituloDB',
				id: 'newTextTituloDB1',
				fieldLabel: 'T�tulo',
				allowBlank: true,
				hidden: false,
				maxLength: 100,	
				width: 100,
				msgTarget: 'side',
				margins: '0 20 0 0'  
			},
			{
				xtype: 'textarea',
				name: 'newTextTextoDB',
				id: 'newTextTextoDB1',
				fieldLabel: 'Texto',
				allowBlank: false,
				maxLength: 4000,
				enableKeyEvents: true,
				listeners:{
					keypress: function(field, event){							
						var val = field.getValue();
						if(event.keyCode !=8 && event.keyCode !=46){//ignorar backspace and delete
							if (val.length >= 4000 ){
								event.stopEvent();
							}
						}
					}
				}
			},
			{
				xtype: 'combo',
				name: 'newCboPosicion',
				id: 'newCboPosicion1',
				fieldLabel: 'Posici�n',
				mode: 'local', 
				displayField : 'descripcion',
				valueField : 'clave',
				hiddenName : 'newCboPosicion',
				emptyText: 'Seleccione...',
				allowBlank: false,
				width: 400,
				forceSelection : true,
				triggerAction : 'all',
				typeAhead: true,
				minChars : 1,
				store : storeNewCatPosicionData,
				tpl : NE.util.templateMensajeCargaCombo
			}
		],
		monitorValid: true,
		buttons: [
			{
				text: 'Guardar',
				iconCls: 'icoGuardar',
				id:'btnGuardarNewDetBann',
				formBind: true,
				handler: function(){
					
					var elFpNewDetB = fpNewDetBanner.getEl();
					
					elFpNewDetB.mask('Guardando...','mask-loading');
					
					Ext.Ajax.request({
						url: '02admincont01ext.data.jsp',
						params: Ext.apply(fpNewDetBanner.getForm().getValues(),{
							informacion: 'addDetBannerNew'
						}),
						callback: procesarSuccessFailureAddNewDetBanner
					});
				}
			},
			{
				text: 'Limpiar',
				hidden: false,
				iconCls: 'icoLimpiar',
				handler: function() {
					
					fpNewDetBanner.getForm().reset();
					
				}
				
			},
			{
				text: 'Cerrar',
				hidden: false,
				iconCls: 'icoClose',
				handler: function() {
					
					var ventana = Ext.getCmp('winNewDetBanner');
					ventana.hide();
					
				}
				
			}
		]
	});

	
	var gridBanners = new Ext.grid.EditorGridPanel({
			id: 'gridBanners1',
			store: storeDataBanners,
			margins: '20 0 0 0',
			style: 'margin: 0 auto',
			title: '',
			clicksToEdit: 1,
			viewConfig: {
				templates: {
					cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}"tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}"{attr}>{value}</div>',
						'</td>'
					)
				}
			},
			sm: selectModelBanner,
			columns: [
				selectModelBanner,
				{
					header: 'Nombre del Banner',
					tooltip: 'Nombre del Banner',
					dataIndex: 'NOMBREBANN',
					sortable: true,
					width: 200,
					resizable: true,
					hidden: false,
					editor: {
							 xtype: 'textfield'
						},
					renderer:  function (causa, columna, registro){
								columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
								return causa;
					}
				},
			/*	{
					header : 'Descripcion Banner',
					tooltip: 'Descripcion Banner',
					dataIndex : 'DESCBANN',
					width : 250,
					sortable : true,
					editor: {
							 xtype: 'textfield'
						},
					renderer:  function (causa, columna, registro){
								columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
								return causa;
					}
				},*/
				{
					header : 'Imagen Banner',
					tooltip: 'Imagen Banner',
					xtype:	'actioncolumn',
					dataIndex : 'CVEBANNER',
					width:	80,	align: 'center', hidden: false,
					renderer: function(value, metadata, record, rowindex, colindex, store) {
								
					},
					items: [
						{
							getClass: function(value, metadata, record, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Ver';
									return 'icoImagen';
							},
							handler:	muestraImagenBanner
						}
					]
				},
				{
					header : 'Cargar Banner',
					tooltip: 'Cargar Banner',
					xtype:	'actioncolumn',
					dataIndex : 'CVEBANNER',
					width:	80,	align: 'center', hidden: false,
					renderer: function(value, metadata, record, rowindex, colindex, store) {
								
					},
					items: [
						{
							getClass: function(value, metadata, record, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Carga Imagen';
									return 'upload-icon';
							},
							handler: muestraCargaBanner
						}
					]
				},
				{
					header : 'Imagen Fondo',
					tooltip: 'Imagen Fondo',
					xtype:	'actioncolumn',
					dataIndex : 'CVEBANNER',
					width:	80,	align: 'center', hidden: false,
					renderer: function(value, metadata, record, rowindex, colindex, store) {
								
					},
					items: [
						{
							getClass: function(value, metadata, record, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Ver';
									return 'icoImagen';
							},
							handler:	muestraImagenBannerFondo
						}
					]
				},
				{
					header : 'Cargar Fondo',
					tooltip: 'Cargar Fondo',
					xtype:	'actioncolumn',
					dataIndex : 'CVEBANNER',
					width:	80,	align: 'center', hidden: false,
					renderer: function(value, metadata, record, rowindex, colindex, store) {
								
					},
					items: [
						{
							getClass: function(value, metadata, record, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Carga Imagen';
									return 'upload-icon';
							},
							handler: muestraCargaBannerFondo
						}
					]
				},
				{
					header : 'Contenido Banner',
					tooltip: 'Contenido Banner',
					xtype:	'actioncolumn',
					dataIndex : 'CVEBANNER',
					width:	80,	align: 'center', hidden: false,
					renderer: function(value, metadata, record, rowindex, colindex, store) {
								
					},
					items: [
						{
							getClass: function(value, metadata, record, rowIndex, colIndex, store) {
									this.items[0].tooltip = 'Editar';
									return 'modificar';
							},
							handler: muestraEditContBanner
						}
					]
				},
				{
					header: 'Estatus',
					tooltip: 'Estatus',
					dataIndex: 'ACTIVOBANN',
					sortable: true,
					width: 80,
					resizable: true,
					hidden: false,
					renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						var texto = 'Inactivo';
						if(causa=='S')texto = 'Activo';
						return texto;
					}
				},
				{
					header: 'Fecha Modificaci�n',
					tooltip: 'Fecha Modificaci�n', 
					dataIndex: 'FECMODBANN',
					sortable: true,
					width: 133,
					resizable: true,
					hidden: false,
					renderer:  function (causa, columna, registro){
						columna.attr = 'ext:qtip="' + Ext.util.Format.nl2br(causa) + '"';
						return causa;
					}
				}
			],
			stripeRows: true,
			columnLines : true,
			loadMask: true,
			height: 250,
			width: 800,
			frame: true,
			bbar: {
				xtype: 'toolbar',
				items: [
					'->',
					'-',
					{
					text: 'Agregar',
					id: 'btnAgregarBanner',
					iconCls: 'aceptar',
					handler: fnAgregarBanner
					},
					'-',
					{
					text: 'Guardar',
					id: 'btnGuardarBanner',
					iconCls: 'icoGuardar',
					handler: fnGuardarBanner
					},
					'-',
					{
					text: 'Invertir Estatus',
					id: 'btnEstatusBanner',
					iconCls: 'icoAceptar',
					handler: fnCambiaEstatusBanner
					},
					'-',
					{
					text: 'Eliminar',
					id: 'btnEliminarBanner',
					iconCls: 'borrar',
					handler: fnEliminarBanner
					}
				]
			}
	});
	
	
	var gridEditBanner = new Ext.grid.GridPanel({
		id: 'gridEditBanner1',
		store: storeDataEditBanner,
		margins: '20 0 0 0',
		hideHeaders : false,
		sm: selectModelEditBanner,
		columns: [
			selectModelEditBanner,
			{
				header : 'T�tulo',
				tooltip: 'T�tulo',
				dataIndex : 'TITULOBANNEDIT',
				width : 150,
				sortable : true,
				renderer:  function (causa, columna, registro){
					columna.attr = "ext:qtip='" + Ext.util.Format.nl2br(causa) + "'";
					return causa;
				}
			},
			{
				header : 'Texto',
				tooltip: 'Texto',
				dataIndex : 'TEXTOBANNEDIT',
				width : 398,
				sortable : true,
				renderer:  function (causa, columna, registro){
					columna.attr = "ext:qtip='" + Ext.util.Format.nl2br(causa) + "'";
					return causa;
				}
			},
			{
				header : 'Posici�n',
				tooltip: 'Posici�n',
				dataIndex : 'POSICIONBANNEDIT',
				width : 70,
				sortable : true
				
			}
		],
		stripeRows: true,
		columnLines : true,
		loadMask: true,
		width: 655,
		height: 220,
		title: '',
		frame: true,
		listeners:{
			cellclick: function(grid, rowIndex, columnIndex){
				var record = grid.getStore().getAt(rowIndex);
				var fieldName = grid.getColumnModel().getDataIndex(columnIndex); // Get field name
				if(fieldName=='TITULOBANNEDIT'){
					var textTitulo = Ext.getCmp('textTituloDB1');
					var textTexto = Ext.getCmp('textTextoDB1');
					var posicionDetBanner = Ext.getCmp('posicionDetBanner1');
					var tipoEvento = Ext.getCmp('tipoEventoBanner1');
					var data = record.get(fieldName);
					var reg = Ext.data.Record.create(['clave', 'descripcion','loadMsg']);
					
					if(posicionDetBanner.getValue()!=''){
						
						storeCatPosicionData.removeAt(storeCatPosicionData.findExact('clave',posicionDetBanner.getValue()));
					}
					
					storeCatPosicionData.add(
						new reg({
							clave: record.get('POSICIONBANNEDIT'),
							descripcion: record.get('POSICIONBANNEDIT'),
							loadMsg:''
						})
					);
					
					storeCatPosicionData.sort('clave');
					
					posicionDetBanner.setValue(record.get('POSICIONBANNEDIT'));
					textTitulo.setValue(data);
					textTexto.setValue(record.get('TEXTOBANNEDIT'));
					tipoEvento.setValue("G");
					Ext.getCmp('cboPosicion1').setValue(record.get('POSICIONBANNEDIT'));
					
				}
			}
		},
		bbar: {
			xtype: 'toolbar',
			items: [
				'->',
				'-',
				{
				text: 'Eliminar',
				id: 'btnEliminarEditBanner',
				iconCls: 'borrar',
				handler: function(){
						var bOk = false
						var indiceSm = 0;
						var registrosEnviar = [];
						
						gridEditBanner.getStore().each(function(record) {
							if(selectModelEditBanner.isSelected(indiceSm)){
								registrosEnviar.push(record.data);
								bOk = true;
							}
							
							indiceSm = indiceSm+1;				
						});
		
						if(bOk){
							Ext.MessageBox.confirm('Confirmaci�n','�Esta seguro de eliminar los registrsos seleccionados?',function(msb){
								if(msb=='yes'){
									var hidCveBanner = Ext.getCmp('hidCveBanner1');
									
									Ext.Ajax.request({
										url: '02admincont01ext.data.jsp',
										params: {
											informacion: 'deleteDetBanner',
											registros: Ext.encode(registrosEnviar),
											cveBanner: hidCveBanner.getValue()
										},
										callback: processSuccessFailureDeleteDetBanner
									});
								}
							});
						}else{
							Ext.MessageBox.alert('Mensaje','Favor de seleccionar un registro');
						}
					
					}
				},
				'-',
				{
					text: 'Cancelar',
					id: 'btnCancelar',
					iconCls: 'cancelar',
					handler: function(){
						var winEditBanner = Ext.getCmp('winEditBanner');
						winEditBanner.hide();
					}
				}
			]
		}
	});


//BANNERS ...............................................................................FIN


	var tabTextoLogo = {
		title: 'Texto del Logo',
		id: 'tabTextoLogo1',
		items: [
			{
				xtype: 'panel',
				id: 'pnlTextoLogo',
				bodyBorder: false,
				width: 800,
				//height: 300,
				frame: false,
				hidden: false,
				style: 'margin: 0 auto',
				items:[
					NE.util.getEspaciador(20),
					gridTextoLogo,
					NE.util.getEspaciador(20),
					fpTextLogo
					
				]
			}
		]
	};
	
	var tabSecciones = {
		title: 'Secciones',
		id: 'tabSecciones1',
		items: [
			{
				xtype: 'panel',
				id: 'pnlSecciones',
				width: 800,
				frame: false,
				hidden: false,
				bodyBorder: false,
				style: 'margin: 0 auto',
				items:[
					NE.util.getEspaciador(20),
					gridSecciones,
					NE.util.getEspaciador(20),
					{
					xtype: 'panel',
					name: 'pnlImageSeccion',
					id: 'pnlImageSeccion1',
					width: 710,
					style: 'margin:0 auto;',
					frame: false,
					hidden: true,
					title:'Imagen Cargada'
				
				}
					
				]
			}
		]
	};
	
	var tabMenus = {
		title: 'Men�s',
		id: 'tabMenus1',
		items: [
			{
				xtype: 'panel',
				id: 'pnlMenus',
				width: 800,
				frame: false,
				hidden: false,
				bodyBorder: false,
				style: 'margin: 0 auto',
				items:[
					NE.util.getEspaciador(20),
					fpMenus,
					gridMenus,
					NE.util.getEspaciador(20)
					
				]
			}
		]
	};
	
	var tabBanners = {
		title: 'Banners',
			id: 'tabBanners1',
		items: [
			{
				xtype: 'panel',
				id: 'pnlBanner',
				width: 800,
				frame: false,
				hidden: false,
				bodyBorder: false,
				style: 'margin: 0 auto',
				items:[
					NE.util.getEspaciador(20),
					fpBannerForm,
					fpAgregarBanner,
					gridBanners,
					NE.util.getEspaciador(20),
					{
						xtype: 'panel',
						name: 'pnlImageBanner',
						id: 'pnlImageBanner1',
						//width: 800,
						style: 'margin:0 auto;',
						frame: false,
						hidden: true,
						title:'Imagen Cargada'
					
					}
				]
				
			}
		]
	};
	
	var tabLinks = {
		title: 'Links',
		id: 'tabLinks1',
		items: [
			{
				xtype: 'panel',
				id: 'pnlLinks',
				width: 800,
				frame: false,
				hidden: false,
				style: 'margin: 0 auto',
				bodyBorder: false,
				items:[
					NE.util.getEspaciador(20),
					gridLinks,
					NE.util.getEspaciador(20)
				]
			}
		]
	};

	var tp = new Ext.TabPanel({
		activeTab: 0,
		width:880,
		//height:250,
		plain:false,
		defaults:{autoHeight: true},
		items: [
			tabTextoLogo,
			tabSecciones,
			tabMenus,
			tabBanners,
			tabLinks
		],
		listeners: {
			tabchange: function(tab, panel){
				
				if (panel.getItemId() == 'tabTextoLogo1') {
					fpTextLogo.getForm().reset();
					fpTextLogo.hide();
					Ext.getCmp('btnAgregar').enable();
					Ext.getCmp('btnGuardar').enable();
					Ext.getCmp('btnEliminar').enable();
					storeDataTextLogo.rejectChanges();
					storeDataTextLogo.load();
					Ext.getCmp('pnlImageSeccion1').hide();
				} else if (panel.getItemId() == 'tabSecciones1') {
					storeDataSecciones.rejectChanges();
					storeDataSecciones.load();
					Ext.getCmp('pnlImageSeccion1').hide();
				} else if (panel.getItemId() == 'tabMenus1') {
					fpMenus.getForm().reset();
					storeCatSeccionesData.load();
					gridMenus.hide();
					storeDataMenus.rejectChanges();
				}else if(panel.getItemId()== 'tabLinks1'){
					storeDataLinks.load();
				}else if(panel.getItemId()== 'tabBanners1'){
					
					fpAgregarBanner.show();
					fpAgregarBanner.getForm().reset();
					fpAgregarBanner.hide();
					fpBannerForm.getForm().reset();
					fpBannerForm.show();
					storeDataBanners.rejectChanges();
					storeDataBanners.load();
					Ext.getCmp('pnlImageBanner1').hide();
				}
			}
		}
	});


	var pnl = new Ext.Container({
		id: 'contenedorPrincipal',
		applyTo: 'areaContenido',
		width: 890,
		items: [
			NE.util.getEspaciador(20),
			tp
		]
	});
	
	storeDataTextLogo.load();

});