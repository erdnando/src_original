package netropology.utilerias;
import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/** 
 * Esta clase tiene por objetivo utilizar los paginadores actuales e integrarlos
 * al ExtJS
 * @author gaparicio
 * @deprecated Preferir el uso de CQueryHelperRegExt
 *
 */ 
public class CQueryHelperExtJS {

	private String ids = "_cqueryhelperextjsIds";
	private String totales = "_cqueryhelperextjsTotales";
	
	private IQueryGenerator qryGen;
	private IQueryGeneratorPS qryGenPS;

	private boolean multiplesPaginadoresXPagina = false;
	private PreparedStatement pst = null;

	/**
	 * Id para identificar de manera unica la ejecuci�n de un query y poder
	 * rastrear donde inicia y donde termina.
	 * Util para cuando hay varios queries de paginador ejecutandose
	 * simultaneamente.
	 */
	private final String idQuery = String.valueOf((new java.util.Date()).getTime());

	public CQueryHelperExtJS(){}

	public CQueryHelperExtJS(IQueryGenerator qryGen){
		this.qryGen = qryGen;
		if(qryGen instanceof IQueryGeneratorPS) {
			this.qryGenPS = (IQueryGeneratorPS)qryGen;
		}
	}

	/**
	 * Obtiene la informacion a mostrar en formato JSON.
	 * IMPORTANTE!!!!: Es necesario que los alias de las columnas en la consulta
	 * no se repitan, ya que de lo contrario el objeto resultante eliminar� los
	 * campos repetidos
	 * @deprecated Usar CQueryHelperExtJS
	 * @param request maintains all State for this static invocation
	 * @param con Objeto de conexion, ya debe estar establecida la conexion
	 * @param start Indica el indice del registro a partir del que se obtendra 
	 * 		la informacion
	 * @param limit Especifica el numero maximo de registros que se regresaran
	 * @return Cadena JSON con los registros correspondientes
	 */
	public String getJSONPageResultSet(
			HttpServletRequest request,
			int start,
			int limit) {

		//------------------- Validacion de parametros -------------------------
		try {
			if (start < 0) {
				throw new Exception("El parametro start debe ser mayor a 0");
			}
			if (limit <1) {
				throw new Exception("El parametro limit debe ser mayor a 1");
			}
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		//----------------------------------------------------------------------
		

		JSONArray jsonArr = new JSONArray();
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement pst = null;
		boolean cerrarStatement = false;
		try {
			List nextSetOfIds = this.getNextSetOfIds(request, start, limit);
			if (nextSetOfIds == null || nextSetOfIds.size() == 0) {
				return "{\"success\": true, \"total\": \"" + 
						this.getResultSetSize(request) + "\", \"registros\": " + jsonArr.toString()+"}";
			}
			
			String query =	qryGen.getDocumentSummaryQueryForIds(request,nextSetOfIds);
			
			con.conexionDB();

			if(qryGen instanceof IQueryGeneratorPS){
				pst = con.queryPrecompilado(query);
				int i=1;
				int numList = qryGenPS.getNumList(request);
				for(int j=0;j<numList;j++){
					for (Iterator it = nextSetOfIds.iterator(); it.hasNext();){
						pst.setString(i,it.next().toString());
						i++;
					}
				}
				rs = pst.executeQuery();
			}else{
				cerrarStatement = true;
				rs = con.queryDB(query);
			}

			ResultSetMetaData rsMD = rs.getMetaData();
			int numCols = rsMD.getColumnCount();
					
			while(rs.next()){
				String columna = "";
				String valor = "";
				HashMap registro = new HashMap();
				for (int i = 1; i <= numCols; i++) {
					String nombreColumna = rsMD.getColumnName(i);
					int colType = rsMD.getColumnType(i);
					switch(colType) {
						case Types.NUMERIC:
						case Types.DECIMAL:
						case Types.INTEGER:
							BigDecimal rsNumeric = rs.getBigDecimal(i)==null?new BigDecimal("0"):rs.getBigDecimal(i);
							registro.put(nombreColumna, rsNumeric.toPlainString());
							break;
						case Types.DATE:
						case Types.TIME:
						case Types.TIMESTAMP:
							Timestamp rsFecha = rs.getTimestamp(i);
							registro.put(nombreColumna, rsFecha);
							break;
						default:
							String rsString = rs.getString(i)==null?"":rs.getString(i);
							registro.put(nombreColumna, rsString);
							break;
					}//switch(colType)
				} //fin del for
				jsonArr.add(registro);
			} //fin while rs
			return "{\"success\": true, \"total\": \"" + 
				this.getResultSetSize(request) + "\", \"registros\": " + jsonArr.toString()+"}";
		} catch(Throwable t) {
			throw new AppException("Error al obtener los registros de la paginacion", t);
		} finally {
			if (con.hayConexionAbierta()) {
				try {
					if (rs!=null) {
						rs.close();
					}
					if (pst!=null) {
						pst.close();
					}
					if (cerrarStatement) {
						con.cierraStatement();
					}
					con.terminaTransaccion(true);	//Para las consultas que hagan uso de tablas remotas usando un dblink es requerido un commit.
					con.cierraConexionDB();
				} catch(Exception e) {
					System.out.println("Error al cerrar conexion. " + e);
				}
			}
		}
	}


	/**
	 * get only the Results that will be displayed on the current page
	 * @deprecated Usar CQueryHelperExtJS
	 * @param request maintains all State for this static invocation
	 * @param con is the Connection Object that should be used for the query
	 * @param offset indicates the page that we are currently displaying
	 * @param pageSize gives the number of rows that should be returned here
	 * @return Objeto Registros
	 * @throws Exception
	 */
	public Registros getPageResultSet(
		HttpServletRequest request,
		int offset,
		int pageSize){
		Collection nextSetOfIds = this.getNextSetOfIds(request, offset, pageSize);
		if (nextSetOfIds == null || nextSetOfIds.size() == 0) {
			return new Registros();
		}
		
		String query =	qryGen.getDocumentSummaryQueryForIds(request,nextSetOfIds);
		ResultSet rs = null;
		Registros reg = new Registros();
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			if(qryGen instanceof IQueryGeneratorPS){
				pst = con.queryPrecompilado(query);
				int i=1;
				int numList = qryGenPS.getNumList(request);
				for(int j=0;j<numList;j++){
					for (Iterator it = nextSetOfIds.iterator(); it.hasNext();){
						pst.setString(i,it.next().toString());
						i++;
					}
				}
				rs = pst.executeQuery();
			}else{
				rs = con.queryDB(query);
			}
			return con.getRegistrosFromRS(rs, true);
		} catch(Exception e) {
			throw new AppException("getPageResultSet(Exception)", e);
		} finally {
			if (con.hayConexionAbierta()) {
				try {
					if (pst != null) {
						pst.close();
					}
					if (rs != null) {
						rs.close();
					}
				} catch(Exception e) {
					System.out.println("CQueryHelperExtJS.getPageResultSet()::Error al cerrar statement/resultset");
				}
				con.terminaTransaccion(true); //para las consultas que hacen referencia a tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}
	/**
	 * Obtiene las llaves primarias de la informaci�n a consultar con base
	 * en los parametros (criterios) de busqueda especificados
	 * @deprecated Usar CQueryHelperExtJS
	 *
	 * @param request Objeto HttpServletRequest, de donde se obtienen los
	 * 		parametros de busqueda
	 */
	public void executePKQuery(HttpServletRequest request) {
		HttpSession session = request.getSession();
		
		this.cleanSession(request);
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList al = null;
		AccesoDB con = new AccesoDB();
	
		System.out.println("CQueryHelperExtJS.executePKQuery():: pag= " + qryGen.getClass().getName() + " (" + this.idQuery + ")" );
				
		try {
			con.conexionDB();

			if(qryGen instanceof IQueryGeneratorPS){
					
				ps = con.queryPrecompilado(qryGen.getDocumentQuery(request));
				al = qryGenPS.getConditions(request);
					
				for(int i=0;i<al.size();i++){
					if(al.get(i) instanceof Integer){
					Integer intg = (Integer)al.get(i);
					ps.setInt(i+1, intg.intValue());
					} else if(al.get(i) instanceof String){
						ps.setString(i+1, al.get(i).toString());
					} else if(al.get(i) instanceof Double){
						Double doubg = (Double)al.get(i);
						ps.setDouble(i+1, doubg.doubleValue());
					}
				}
				ps.setMaxRows(1000);	//Se limita el numero m�ximo de registros a 1000 para despliegue
				long tiempoInicial = (new java.util.Date()).getTime();
				rs = ps.executeQuery();
				long tiempoFinal = (new java.util.Date()).getTime();
				System.out.println("CQueryHelperExtJS.executePKQuery():: PKs. t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
				ps.clearParameters();
			}else if (qryGen instanceof IQueryGenerator){
				long tiempoInicial = (new java.util.Date()).getTime();
				rs = con.queryDB(qryGen.getDocumentQuery(request),1000);
				long tiempoFinal = (new java.util.Date()).getTime();
				System.out.println("CQueryHelperExtJS.executePKQuery():: PKs t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
			}
	
			List newIds = new ArrayList();
			List vecTotales = new ArrayList();
			while (rs.next()) {
				newIds.add(rs.getString(1));  //Llenamos el vector de llaves primarias
			}
			if(ps!=null) ps.close();
			
			int count=0;
			int i=0;
			if (newIds.size() > 0) {
				try{
					
					String queryTotales = qryGen.getAggregateCalculationQuery(request);
					
					//Solo si hay query de totales se ejecuta el query
					if (queryTotales != null && !queryTotales.trim().equals("") ) {
					
						if(qryGen instanceof IQueryGeneratorPS){
							al = qryGenPS.getConditions(request);
							ps = con.queryPrecompilado(queryTotales);
							int numList = qryGenPS.getNumList(request);
							 
							for(int j=0;j<numList;j++) {
								for(int x=0;x<al.size();x++){
									if(al.get(x) instanceof Integer){
										Integer intg = (Integer)al.get(x);
										ps.setInt(i+1, intg.intValue());
									} else if(al.get(x) instanceof String){
										ps.setString(i+1, al.get(x).toString());
									} else if(al.get(x) instanceof Double){
										Double doubg = (Double)al.get(x);
										ps.setDouble(i+1, doubg.doubleValue());
									}
									i++;
								}
							}
							long tiempoInicial = (new java.util.Date()).getTime();
							rs = ps.executeQuery();
							long tiempoFinal = (new java.util.Date()).getTime();
							System.out.println("CQueryHelperExtJS.executePKQuery():: Totales t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
							ps.clearParameters();
						} else { //IQueryGenerator
							long tiempoInicial = (new java.util.Date()).getTime();
							rs = con.queryDB(queryTotales);
							long tiempoFinal = (new java.util.Date()).getTime();
							System.out.println("CQueryHelperExtJS.executePKQuery():: Totales t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
						}

						int numCols = rs.getMetaData().getColumnCount();
						while(rs.next()){
							List vecFilas = new ArrayList();
							for(i=1;i<=numCols;i++){
								vecFilas.add((rs.getString(i)==null)?"":rs.getString(i));
							}
							vecTotales.add(vecFilas);
						}
						if(qryGen instanceof IQueryGeneratorPS){
							rs.close();
							if(ps!=null) ps.close();
						} else {
							con.cierraStatement();
						}
					} // fin de Totales
				}catch(Exception e){
					throw new AppException("Error al obtener los totales de la consulta", e);
				}
			}
	
			System.out.println("ResultSetSize:  "+newIds.size());
			System.out.println("Totales "+vecTotales);
			System.out.println("newIds "+newIds);
	
			session.setAttribute(this.ids, newIds);
			session.setAttribute(this.totales, vecTotales);
		} catch(Exception e) {
			throw new AppException("Error al obtener los IDs de la consulta", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //Se incluye por si la consulta involucra tablas remotas via dblink, en cuyo caso se requiere un commit
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * The pager uses this method to determine how many results we
	 * are paging through
	 * @deprecated Usar CQueryHelperExtJS
	 *
	 * @param request maintains the State of this request
	 * @return
	 */
	public int getResultSetSize(HttpServletRequest request){
		List vector = (List) request.getSession().getAttribute(ids);

		if (vector != null) {
			return vector.size();
		} else {
			return 0;
		}
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @deprecated Usar CQueryHelperExtJS
	 * @param request Objeto HttpServletRequest
	 * @param start Indice a partir del cual se toman los registros
	 * @param limit Numero de registros a obtener
	 * @return Lista con los Ids en turno para la paginacion
	 */
	private List getNextSetOfIds(
		HttpServletRequest request,
		int start,
		int limit) {
		List vector = (List) request.getSession().getAttribute(ids);
		if (vector.size()>start+limit){
			return vector.subList(start, start + limit);
		}else{
			return vector.subList(start,vector.size());
		}
	}

	/**
	 * Obtiene una lista con los totales de la paginacion
	 * @deprecated Usar CQueryHelperExtJS
	 */
	public List getResultCount(HttpServletRequest request){
		return (List)request.getSession().getAttribute(totales);
	}

	/**
	 * Obtiene una lista con los totales de la paginacion en formato JSON.
	 * @deprecated Usar CQueryHelperExtJS
	 */
	public String getJSONResultCount(HttpServletRequest request){
		List registros = this.getResultCount(request);
		JSONArray jsonArr = new JSONArray();
		if(registros != null){
			for(int i=0;i<registros.size();i++){
				List registro = (List)registros.get(i);
				JSONObject jsonObj = new JSONObject();
				for (int j = 0; j < registro.size(); j++) {
					jsonObj.put("col" + j, registro.get(j));
				}
				jsonArr.add(jsonObj);
			}
		}
		return "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	}

	/**
	 * Metodo que regresa un resultset, para poder generar un archivo de la consulta.
	 * @deprecated El metodo puede dejar Statements abiertos y generar problemas
	 * 		de rendimiento.
	 * @throws java.lang.Exception
	 * @return 
	 * @param con
	 * @param request
	 */
	public ResultSet getCreateFile(HttpServletRequest request, AccesoDB con) throws Exception {
		HttpSession session = request.getSession();
		System.out.println("getCreateFile(E):: pag= " + qryGen.getClass().getName() + " (" + this.idQuery + ")");
		
		ArrayList al = null;
		ResultSet rs = null;
		
		if(qryGen instanceof IQueryGeneratorPS){
			
			pst = con.queryPrecompilado(qryGen.getDocumentQueryFile(request));
			al = qryGenPS.getConditions(request); //GEAG (Cambio de linea)
			
			int numList = qryGenPS.getNumList(request);
			int x = 0;
			for(int j=0;j<numList;j++) {
				for(int i=0;i<al.size();i++){
					if(al.get(i) instanceof Integer){
						Integer intg = (Integer)al.get(i);
						x++;pst.setInt(x, intg.intValue());
					} else if(al.get(i) instanceof String){
						x++;pst.setString(x, al.get(i).toString());
					} else if(al.get(i) instanceof Double){
						Double doubg = (Double)al.get(i);
						x++;pst.setDouble(x, doubg.doubleValue());
					}
				}
			}
			long tiempoInicial = (new java.util.Date()).getTime();
			rs = pst.executeQuery();
			long tiempoFinal = (new java.util.Date()).getTime();
			System.out.println("CQueryHelperExtJS.getCreateFile():: t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
			pst.clearParameters();
		} else {
			long tiempoInicial = (new java.util.Date()).getTime();
			rs = con.queryDB(qryGen.getDocumentQueryFile(request));
			long tiempoFinal = (new java.util.Date()).getTime();
			System.out.println("CQueryHelperExtJS.getCreateFile():: t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
		}		

		System.out.println("getCreateFile(S)");
		return rs;
	}

	/**
	 * Limpia los datos en sesion referente a la paginacion, para iniciar
	 * una nueva consulta.
	 *
	 * @param request Objeto HttpServletRequest para obtener la sesion
	 */
	private void cleanSession(HttpServletRequest request){
		HttpSession session = request.getSession();

		session.removeAttribute(this.ids);
		session.removeAttribute(this.totales);
	}

	/**
	 * Especifica si hay varios paginadores que se usan en la misma pagina
	 * ya que de ser as� es necesario guardar las informaci�n de las 
	 * consultas con diferentes identificadores en sesion.
	 * Si multiplesPaginadoresXPagina = false (predeterminado) la informaci�n
	 * de la consulta se almacena siempre bajo el mismo identificador en sesi�n.
	 * Este m�todo debe ser llamado inmediatamente despues de invocar el constructor
	 * CQueryHelperRegExtJS(IQueryGeneratorRegExtJS qryGen)
	 * @param multiplesPaginadoresXPagina indicador de si hay multiples paginadores por pagina o no
	 */
	public void setMultiplesPaginadoresXPagina(boolean multiplesPaginadoresXPagina) {
		if (multiplesPaginadoresXPagina) {
			this.ids = this.ids + "_" + qryGen.getClass().getName();
			this.totales = this.totales + "_" + qryGen.getClass().getName();
		}
	}

}
