package netropology.utilerias;

import java.io.InputStream;

import java.net.MalformedURLException;
import java.net.URL;

import java.util.Collections;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.servlet.SessionCookieConfig;
import javax.servlet.SessionTrackingMode;
import javax.servlet.descriptor.JspConfigDescriptor;
import javax.servlet.http.HttpSession;

public class ServletContextFake implements ServletContext {
    @SuppressWarnings("unchecked")
    private HashMap<String, Object> hm = new HashMap();
    
    public ServletContextFake() {
        
    }
    
    public ServletContextFake(ServletContext sc) {
        cloneProperties(sc);
    }
    
    public void cloneProperties(ServletContext sc) {
        
        for (Enumeration e = sc.getAttributeNames(); e.hasMoreElements();) {
            String nombreAtributo = (String)e.nextElement();
            hm.put(nombreAtributo, sc.getAttribute(nombreAtributo));
        }

    }


    @Override
    public String getContextPath() {
        // TODO Implement this method
        return null;
    }

    @Override
    public ServletContext getContext(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public int getMajorVersion() {
        // TODO Implement this method
        return 0;
    }

    @Override
    public int getMinorVersion() {
        // TODO Implement this method
        return 0;
    }

    @Override
    public int getEffectiveMajorVersion() {
        // TODO Implement this method
        return 0;
    }

    @Override
    public int getEffectiveMinorVersion() {
        // TODO Implement this method
        return 0;
    }

    @Override
    public String getMimeType(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public Set<String> getResourcePaths(String string) {
        // TODO Implement this method
        return Collections.emptySet();
    }

    @Override
    public URL getResource(String string) throws MalformedURLException {
        // TODO Implement this method
        return null;
    }

    @Override
    public InputStream getResourceAsStream(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public RequestDispatcher getRequestDispatcher(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public RequestDispatcher getNamedDispatcher(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public Servlet getServlet(String string) throws ServletException {
        // TODO Implement this method
        return null;
    }

    @Override
    public Enumeration<Servlet> getServlets() {
        // TODO Implement this method
        return null;
    }

    @Override
    public Enumeration<String> getServletNames() {
        // TODO Implement this method
        return null;
    }

    @Override
    public void log(String string) {
        // TODO Implement this method
    }

    @Override
    public void log(Exception exception, String string) {
        // TODO Implement this method

    }

    @Override
    public void log(String string, Throwable throwable) {
        // TODO Implement this method

    }

    @Override
    public String getRealPath(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getServerInfo() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getInitParameter(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public Enumeration<String> getInitParameterNames() {
        // TODO Implement this method
        return null;
    }

    @Override
    public boolean setInitParameter(String string, String string2) {
        // TODO Implement this method
        return false;
    }

    @Override
    public Object getAttribute(String atributo) {
        // TODO Implement this method
        return hm.get(atributo)==null?"":hm.get(atributo);
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        // TODO Implement this method
        return null;
    }

    @Override
    public void setAttribute(String string, Object object) {
        // TODO Implement this method

    }

    @Override
    public void removeAttribute(String string) {
        // TODO Implement this method
    }

    @Override
    public String getServletContextName() {
        // TODO Implement this method
        return null;
    }

    @Override
    public ServletRegistration.Dynamic addServlet(String string, String string2) {
        // TODO Implement this method
        return null;
    }

    @Override
    public ServletRegistration.Dynamic addServlet(String string, Servlet servlet) {
        // TODO Implement this method
        return null;
    }

    @Override
    public ServletRegistration.Dynamic addServlet(String string, Class<? extends Servlet> c) {
        // TODO Implement this method
        return null;
    }

    @Override
    public <T extends Servlet> T createServlet(Class<T> c) throws ServletException {
        // TODO Implement this method
        return null;
    }

    @Override
    public ServletRegistration getServletRegistration(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public Map<String, ? extends ServletRegistration> getServletRegistrations() {
        // TODO Implement this method
        return Collections.emptyMap();
    }

    @Override
    public FilterRegistration.Dynamic addFilter(String string, String string2) {
        // TODO Implement this method
        return null;
    }

    @Override
    public FilterRegistration.Dynamic addFilter(String string, Filter filter) {
        // TODO Implement this method
        return null;
    }

    @Override
    public FilterRegistration.Dynamic addFilter(String string, Class<? extends Filter> c) {
        // TODO Implement this method
        return null;
    }

    @Override
    public <T extends Filter> T createFilter(Class<T> c) throws ServletException {
        // TODO Implement this method
        return null;
    }

    @Override
    public FilterRegistration getFilterRegistration(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public Map<String, ? extends FilterRegistration> getFilterRegistrations() {
        // TODO Implement this method
        return Collections.emptyMap();
    }

    @Override
    public SessionCookieConfig getSessionCookieConfig() {
        // TODO Implement this method
        return null;
    }

    @Override
    public void setSessionTrackingModes(Set<SessionTrackingMode> set) {
        // TODO Implement this method
    }

    @Override
    public Set<SessionTrackingMode> getDefaultSessionTrackingModes() {
        // TODO Implement this method
        return Collections.emptySet();
    }

    @Override
    public Set<SessionTrackingMode> getEffectiveSessionTrackingModes() {
        // TODO Implement this method
        return Collections.emptySet();
    }

    @Override
    public void addListener(String string) {
        // TODO Implement this method
    }

    @Override
    public <T extends EventListener> void addListener(T eventListener) {
        // TODO Implement this method
    }

    @Override
    public void addListener(Class<? extends EventListener> c) {
        // TODO Implement this method
    }

    @Override
    public <T extends EventListener> T createListener(Class<T> c) throws ServletException {
        // TODO Implement this method
        return null;
    }

    @Override
    public JspConfigDescriptor getJspConfigDescriptor() {
        // TODO Implement this method
        return null;
    }

    @Override
    public ClassLoader getClassLoader() {
        // TODO Implement this method
        return null;
    }

    @Override
    public void declareRoles(String[] string) {
        // TODO Implement this method
    }
}
