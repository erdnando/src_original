package netropology.utilerias;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DescargaArchivo extends HttpServlet  {
	private static final String CONTENT_TYPE = "application/octet-stream";

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);
		String fullName	= (request.getParameter("nombreArchivo")!=null)?request.getParameter("nombreArchivo"):"";

		if (fullName.equals("")){return;}

		ServletOutputStream out = response.getOutputStream();
		String archivo = getServletContext().getRealPath ( "/" + fullName );
		java.io.File fFile = new java.io.File(archivo);

		if (!fFile.exists()){return;}

		String extF = fFile.getName();
		int point = extF.lastIndexOf(".");
		String extension = (extF.substring(point)).toLowerCase();
		
		System.out.println("extension === "+extension);    
		
		if(extension.equals(".pdf") || extension.equals(".csv") || extension.equals(".txt") || extension.equals(".zip") || 
			extension.equals(".xls") || extension.equals(".xlsx") || extension.equals(".doc") || extension.equals(".docx") || extension.equals(".xml")
		|| extension.equals(".pptx") ){ 

			response.setHeader  ( "Content-Disposition", "attachment; filename=\"" + fFile.getName() + "\"" );
			java.io.FileInputStream fis = null;
	
			try{
				fis = new java.io.FileInputStream (fFile);
				byte [  ]  buf = new byte [ 4 * 1024 ] ; // 4K buffer
				int bytesRead;
				while  (  ( bytesRead = fis.read ( buf )  )  != -1 )
					 out.write ( buf, 0, bytesRead );
			}catch( java.io.FileNotFoundException e )   {
				out.println ( "File not found: " + fFile.getName() );
			}catch( IOException e )   {
				out.println ( "Problem sending file " + fFile.getName() + ": " + e.getMessage (  )  );
			}finally{
				if  ( fis != null ){
					 fis.close();
				}
				if (out != null){
					out.flush();
					out.close();
				}
			}
		}else{
			return;
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}