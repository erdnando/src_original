package netropology.utilerias;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DescargaArchivosBD", urlPatterns = { "/descargaarchivo.do" })
public class DescargaArchivosBD extends HttpServlet {
	private static final String CONTENT_TYPE = "application/octet-stream";

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		try{
			con.conexionDB();
			String nombreArchivo = request.getParameter("nombreArchivo");
		    query = "SELECT bi_archivo " +
		            "  FROM com_archivos_procesos " +
		            " WHERE cg_nombre_archivo = ? ";
		    
		    ps = con.queryPrecompilado(query);
		    ps.setString(1,nombreArchivo);
		    rs = ps.executeQuery();
		    
		    if(rs.next()){
		        response.setHeader  ( "Content-Disposition", "attachment; filename=\"" + nombreArchivo + "\"" );
		        Blob blob = rs.getBlob("bi_archivo");
		        InputStream inStream = blob.getBinaryStream();
		        OutputStream out = response.getOutputStream();
				
		        byte []  buffer = new byte [ 4 * 1024 ] ; // 4K buffer
		        int byteRead = -1;

				while((byteRead = inStream.read(buffer)) != -1){
					out.write ( buffer, 0, byteRead );
				}
				out.flush();
				out.close();
			}else{
				return;
			}
			
		}catch(Exception e ){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				try{rs.close();
					ps.close();
				}catch(Exception e){
					e.printStackTrace();
				}
				con.cierraConexionDB();
			}
		}
	}
}
