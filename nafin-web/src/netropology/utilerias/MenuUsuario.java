package netropology.utilerias;

import com.netro.afiliacion.Afiliacion;
import com.netro.descuento.ISeleccionDocumento;
import com.netro.distribuidores.ParametrosDist;

import java.io.StringReader;

import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;

import org.jdom.CDATA;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.filter.ContentFilter;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;

/**
 * Esta clase esta enfocada a obtener los elementos finales del menu,
 * quitando del menu generico por perfil, los elementos que no apliquen,
 * ya sea porque el usuario no maneja un cierto producto, o por
 * tener alguna restricción de logica de negocio.
 */
public class MenuUsuario  {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(MenuUsuario.class);

	/**
	 * Genera menu a partir del generico que aplica al perfil.
	 * Este metodo ajusta el menu, de acuerdo a la epo con la que va a trabajar
	 * la pyme. Dado que cada Epo puede manejar solo ciertos productos, por lo cual
	 * la pyme no va a tener todas las opciones de menu asociadas al perfil
	 * @param perfil Perfil del usuario
	 * @param clavePyme Clave de la pyme(ic_pyme)
	 * @param claveEpo Clave de la Epo asociada a la pyme (ic_epo)
	 * @param serial Numero del serie del certificado digital (Cuando aplica)
	 * @param appWebContextRoot Ruta de contexto para la aplicación web.
	 * 		Permite ajustar adecuadamente url necesarios en algunos menus.
	 *
	 */
	public static String getMenuPyme(String perfil, String clavePyme,
			String claveEpo, String serial, String appWebContextRoot) {

		log.info("getMenuPyme(E)");

		//**********************************Validación de parametros:*****************************
		int iClavePyme = 0;
		int iClaveEpo = 0;
		try {
			if (perfil == null || clavePyme == null || claveEpo == null || appWebContextRoot == null) {
				throw new Exception("Los parametros son requeridos");
			}
			iClavePyme = Integer.parseInt(clavePyme);
			iClaveEpo = Integer.parseInt(claveEpo);
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu de la PyME", e);
		}
		//****************************************************************************************


		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cg_menu " +
					" FROM seg_perfil " +
					" WHERE cc_perfil = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, perfil);
			ResultSet rs = ps.executeQuery();
			String menuXML = "";
			if (rs.next()) {
				Clob loClob = rs.getClob("cg_menu");
				//loClob.length debe estar entre 0 y 2^31-1 para que la siguiente instrucción sea válida
				menuXML = loClob.getSubString(1, (int)loClob.length());	//
			}
			rs.close();
			ps.close();

			List lMenuEliminar = new ArrayList();
			String pymeValidaDescElec = "";
			CValidacion objValidaProd = new CValidacion();

			strSQL =
					" SELECT ic_tipo_cliente " +
					" FROM comcat_pyme " +
					" WHERE ic_pyme = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, iClavePyme);
			rs = ps.executeQuery();
			int tipoCliente = 0;
			if(rs.next()) {
				tipoCliente = rs.getInt("ic_tipo_cliente");
			}
			rs.close();
			ps.close();
			List mVecDatosSis = null;

			if (tipoCliente == 2) {	//Es solamente Distribuidor
				lMenuEliminar.add("13DESCUENTO");	//Descuento
				lMenuEliminar.add("15SERVICIOS");	//Administración - Servicios
				lMenuEliminar.add("15INFORMACIONAFIL");	//Inf. para Afiliados
				lMenuEliminar.add("15INFORMACIONPAGOS");	//Administración - Información pagos

				lMenuEliminar.add("15ADMPYMEPARAM01");	//Administración - Parametrización - Cuentas Bancarias
				lMenuEliminar.add("15ADMPYMEPARAM02");	//Administración - Parametrización - Selección IF
				lMenuEliminar.add("15ADMPYMEPARAM04");	//Administración - Parametrización - Tasas operativas
				lMenuEliminar.add("15DSCTOAUT");		//Administración - Parametrización - Descuento Automático
				lMenuEliminar.add("15SOLICMANDATO");	//Administración - Parametrización - Instrucción Irrevocable
				lMenuEliminar.add("13PYME13MENSAJE");	//Administración - Parametrización - Solicitud de Afiliación


			} else {
				mVecDatosSis = objValidaProd.prodHabilitado(iClavePyme, iClaveEpo, 0, "NANET");
				if ( mVecDatosSis != null && mVecDatosSis.size() > 0 )	{
					pymeValidaDescElec = objValidaProd.descElecPymeAcceso(iClavePyme, iClaveEpo,"NANET");
					ISeleccionDocumento seleccionDocumentoEJB = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB",ISeleccionDocumento.class);
					if(!seleccionDocumentoEJB.manejaFactoraje(claveEpo,"V")) {
						lMenuEliminar.add("13PYME13FORMA2");
					}
					if(!seleccionDocumentoEJB.manejaFactoraje(claveEpo,"M")) {
						lMenuEliminar.add("13PYME13FORMAMAN");
					}
				} else {
					lMenuEliminar.add("13DESCUENTO");
				}
			}

			if (tipoCliente == 1) {	//Es solamente Proveedor.
				lMenuEliminar.add("24DISTRIBUIDORES");
        
			} else {
				mVecDatosSis = objValidaProd.prodHabilitado(iClavePyme, iClaveEpo, 0, "FDIST");
				if ( mVecDatosSis == null || mVecDatosSis.size() == 0 )	{
					lMenuEliminar.add("24DISTRIBUIDORES");
				}else {				
					lMenuEliminar.add("24FACTORAJEREC");
				}
			}
      
      
      //Se agrega por F043-2014, se quita menu para pymes que no operen con convenio unico o que no tengan cuentas con convenio unico asociadas
      Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB",Afiliacion.class);
      
      String tieneConvUnico = afiliacion.validaPymeParamConvUnico(clavePyme);
      List 	lRegCtas = afiliacion.getCuentasBancariasPyME(clavePyme);
      
      if(!"S".equals(tieneConvUnico) || lRegCtas.size()<=0)
      {
        lMenuEliminar.add("13PYME13MENSAJE");
      }
      //

			mVecDatosSis = objValidaProd.prodHabilitado(iClavePyme, iClaveEpo, 0, "CDER");
			if ( mVecDatosSis == null || mVecDatosSis.size() == 0 )	{
				lMenuEliminar.add("34CESION");
			}
			mVecDatosSis = objValidaProd.prodHabilitado(iClavePyme, iClaveEpo, 0, "FELEC");
			if ( mVecDatosSis == null || mVecDatosSis.size() == 0 )	{
				lMenuEliminar.add("36FIANZA");
			}
			
			//busca Número de N@E de la EPO relacionada
			strSQL = 
					" SELECT ic_nafin_electronico " +
					" FROM comrel_nafin " +
					" WHERE ic_epo_pyme_if = ? " +
					" AND cg_tipo = ? ";
			List params = new ArrayList();
			params.add(new Integer(claveEpo));
			params.add("P");
			Registros reg = con.consultarDB(strSQL, params);
			String ne = null;
			while(reg.next()) {
				ne = reg.getString("ic_nafin_electronico");
			}

			//busca los "menus x epo relacionada" que deben ser excluidos, debido
			//a que no estan parametrizados para la EPO que seleccionó la pyme para trabajar o los que se
			//parametrizaron explicitamente a ser restringidos a los afiliados de la Epo indicada
			strSQL =
					" SELECT DISTINCT mar.cc_menu " +
					"  FROM com_menu_afiliado_restriccion mar, comrel_menu_afiliado_x_perfil maxp " +
					" WHERE mar.ic_tipo_afiliado = maxp.ic_tipo_afiliado " +
					"   AND mar.cc_perfil = maxp.cc_perfil " +
					"   AND mar.cc_menu = maxp.cc_menu " +
					"   AND maxp.cs_activo = ? " +
					"   AND mar.cc_perfil = ? " +
					"   AND mar.cg_tipo_restriccion = ? " +
					"   AND (   (    mar.cs_excluir = ? " +
					"            AND mar.cc_menu NOT IN ( " +
					"                   SELECT cc_menu " +
					"                     FROM com_menu_afiliado_restriccion " +
					"                    WHERE ic_nafin_electronico = ? " +
					"                      AND cg_tipo_restriccion = ? " +
					"                      AND cc_perfil = ? " +
					"                      AND cs_excluir = ? ) " +
					"           ) " +
					"        OR " +
					"           (mar.ic_nafin_electronico = ? AND mar.cs_excluir = ? " +
					"           ) " +
					"       ) ";

			params = new ArrayList();
			params.add("S");
			params.add(perfil);
			params.add("R");	//R= Relacionada (pymes relacionadas con la EPO)
			params.add("N");
			params.add(new Integer(ne));	// N@E de la EPO seleccionada por la pyme con la que va a atrabajar
			params.add("R");	//R= Relacionada (pymes relacionadas con la EPO)
			params.add(perfil);	//perfil de la pyme
			params.add("N");
			params.add(new Integer(ne)); // N@E de la EPO seleccionada por la pyme con la que va a atrabajar
			params.add("S");
			log.debug("getMenuPyme::" + strSQL + "\n" + params);
			reg = con.consultarDB(strSQL, params);
			while(reg.next()) {
				lMenuEliminar.add(reg.getString("cc_menu"));
			}

			ParametrosDist beanParamDist = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class);

			if ( beanParamDist.DesAutomaticoEpo(claveEpo,"PUB_EPO_VENTA_CARTERA").equals("S") ) {
				lMenuEliminar.add("24CAPTURAS");
			}

			lMenuEliminar.addAll(MenuUsuario.getMenusInactivos(perfil, 3)); //3=Pyme
			log.debug("getMenuPyme:: se omiten los sig. menus: " + lMenuEliminar);
			//------------------------- Generacion del XML ajustado ------------------
			SAXBuilder parser = new SAXBuilder();

			Document doc = parser.build(new StringReader(menuXML));

			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null) {
					if (el.getAttribute("id") != null) {
						if ( lMenuEliminar.contains(el.getAttribute("id").getValue()) ) {
							it.remove();
						}
					} else if ( "userdata".equalsIgnoreCase(el.getName()) ) {
						List lContent = el.getContent(new ContentFilter(ContentFilter.CDATA));
						if (!lContent.isEmpty()) {
							CDATA cdata =  (CDATA)lContent.get(0);
							String valueCDATA = cdata.getValue();
							if(valueCDATA != null && valueCDATA.matches(".*[0-9][0-9]pki/.*")) {	//Por convención las rutas que requiere Certificado digital van  en una ruta /XXpki/
								if (serial==null || serial.equals("")) {
									//Hay pantalla PKI pero no se tiene certificado
									//por lo que se ajustan los urls para enviar a la
									//generacion de certificado
									cdata.setText(appWebContextRoot + "/20secure/20generarCertificadoExt.jsp");
								}
							}
						}
					}
				}
			}
			if (!pymeValidaDescElec.equals("S")) {
				//Si la pyme no tiene numero proveedor, elimina los submenus de 13DESCUENTO
				//y cambia la accion del MENU para notificar al usuario
				it = rootElement.getChildren().iterator();
				while (it.hasNext()) {
					Element el  = (Element)it.next();
					if (el != null && el.getAttribute("id") != null) {
						if (el.getAttribute("id").getValue().equals("13DESCUENTO")) {
							el.removeContent();
							Element elHref = new Element("href");
							elHref.setText(appWebContextRoot + "/13descuento/13pyme/13avisoPymeSinNumProvExt.jsp");
							el.setContent(elHref);
							break;
						}
					}
				}
			}

			XMLOutputter output = new XMLOutputter();
			return output.outputString(doc);

		} catch( Exception e) {
			throw new AppException("Error al obtener el menu del usuario", e);
		} finally {
			log.info("getMenuPyme(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Genera menu a partir del generico que aplica al perfil.
	 * Este metodo ajusta el menu, de acuerdo a la epo.
	 * Dado que cada Epo puede manejar solo ciertos productos
	 * no va a tener todas las opciones de menu asociadas al perfil
	 * Tambien se contempla el login del usuario para ajustar el menu
	 * de acuerdo a si ya tiene generado certificado digital o no, en caso de
	 * requerirlo.
	 * @param perfil Perfil del usuario
	 * @param claveEpo Clave de la Epo (ic_epo)
	 * @param serial Numero del serie del certificado digital (Cuando aplica)
	 * @param appWebContextRoot Ruta de contexto para la aplicación web.
	 * 		Permite ajustar adecuadamente url necesarios en algunos menus.
	 *
	 */
	public static String getMenuEpo(String perfil,
			String claveEpo, String serial, String appWebContextRoot) {

		log.info("getMenuEpo(E)");
		
		//**********************************Validación de parametros:*****************************
		int iClaveEpo = 0;
		try {
			if (perfil == null || claveEpo == null || appWebContextRoot == null) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveEpo = Integer.parseInt(claveEpo);
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu de la EPO", e);
		}
		//****************************************************************************************


		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cg_menu " +
					" FROM seg_perfil " +
					" WHERE cc_perfil = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, perfil);
			ResultSet rs = ps.executeQuery();
			String menuXML = "";
			if (rs.next()) {
				Clob loClob = rs.getClob("cg_menu");
				//loClob.length debe estar entre 0 y 2^31-1 para que la siguiente instrucción sea válida
				menuXML = loClob.getSubString(1, (int)loClob.length());	//
			}
			rs.close();
			ps.close();

			List lMenuEliminar = new ArrayList();
			String epoValidaDescElec = "";
			CValidacion objValidaProd = new CValidacion();


			List mVecDatosSis = objValidaProd.prodHabilitado(0, iClaveEpo, 0, "NANET");
			if ( mVecDatosSis == null || mVecDatosSis.size() == 0 )	{
				lMenuEliminar.add("13DESCUENTO");
			}

			mVecDatosSis = objValidaProd.prodHabilitado(0, iClaveEpo, 0, "FDIST");
			if ( mVecDatosSis == null || mVecDatosSis.size() == 0 )	{
				lMenuEliminar.add("24DISTRIBUIDORES");
			}else {				
				lMenuEliminar.add("24FACTORAJEREC");
			}
			

			mVecDatosSis = objValidaProd.prodHabilitado(0, iClaveEpo, 0, "CDER");
			if ( mVecDatosSis == null || mVecDatosSis.size() == 0 )	{
				lMenuEliminar.add("34CESION");
			}

			mVecDatosSis = objValidaProd.prodHabilitado(0, iClaveEpo, 0, "FELEC");
			if ( mVecDatosSis == null || mVecDatosSis.size() == 0 )	{
				lMenuEliminar.add("36FIANZA");
			}

			
			strSQL = 
					" SELECT ic_nafin_electronico " +
					" FROM comrel_nafin " +
					" WHERE ic_epo_pyme_if = ? " +
					" AND cg_tipo = ? ";
			List params = new ArrayList();
			params.add(new Integer(claveEpo));
			params.add("E");
			Registros reg = con.consultarDB(strSQL, params);
			String ne = null;
			while(reg.next()) {
				ne = reg.getString("ic_nafin_electronico");
			}

			//busca los "menus x epo" que deben ser excluidos, debido
			//a que no pertenecen a la EPO que esta entrando o los que se
			//parametrizaron explicitamente a ser restringidos al afiliado
			strSQL =
					" SELECT DISTINCT mar.cc_menu " +
					"  FROM com_menu_afiliado_restriccion mar, comrel_menu_afiliado_x_perfil maxp " +
					" WHERE mar.ic_tipo_afiliado = maxp.ic_tipo_afiliado " +
					"   AND mar.cc_perfil = maxp.cc_perfil " +
					"   AND mar.cc_menu = maxp.cc_menu " +
					"   AND maxp.cs_activo = ? " +
					"   AND mar.cc_perfil = ? " +
					"   AND mar.cg_tipo_restriccion = ? " +
					"   AND (   (    mar.cs_excluir = ? " +
					"            AND mar.cc_menu NOT IN ( " +
					"                   SELECT cc_menu " +
					"                     FROM com_menu_afiliado_restriccion " +
					"                    WHERE ic_nafin_electronico = ? " +
					"                      AND cg_tipo_restriccion = ? " +
					"                      AND cc_perfil = ? " +
					"                      AND cs_excluir = ? ) " +
					"           ) " +
					"        OR " +
					"           (mar.ic_nafin_electronico = ? AND mar.cs_excluir = ? " +
					"           ) " +
					"       ) ";

			params = new ArrayList();
			params.add("S");
			params.add(perfil);
			params.add("D");	//D= Directa
			params.add("N");
			params.add(new Integer(ne));
			params.add("D");	//D= Directa
			params.add(perfil);
			params.add("N");
			params.add(new Integer(ne));
			params.add("S");
			log.debug("getMenuEpo::" + strSQL + "\n" + params);
			reg = con.consultarDB(strSQL, params);
			while(reg.next()) {
				lMenuEliminar.add(reg.getString("cc_menu"));
			}
			
			lMenuEliminar.addAll(MenuUsuario.getMenusInactivos(perfil, 1)); //1=EPO

			log.debug("getMenuEpo:: se omiten los sig. menus: " + lMenuEliminar);
			
			ParametrosDist beanParamDist = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class);

			if(perfil.equals("EPO MAN0 DISTR") ||  perfil.equals("EPO MAN1 DISTR") ) { //Fodea 032-204
				String firmaMancomuanda= beanParamDist.getFirmaMancomunada(claveEpo);
				if ( firmaMancomuanda.equals("N") ) {
					lMenuEliminar.add("24EPOCARGA_MAN");
					lMenuEliminar.add("24EPOCARGA_PMAN"); 
				}	 
			}
			
			//------------------------- Generacion del XML ajustado ------------------
			SAXBuilder parser = new SAXBuilder();

			Document doc = parser.build(new StringReader(menuXML));

			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null) {
					if (el.getAttribute("id") != null) {
						if ( lMenuEliminar.contains(el.getAttribute("id").getValue()) ) {
							it.remove();
						}
					} else if ( "userdata".equalsIgnoreCase(el.getName()) ) {
						List lContent = el.getContent(new ContentFilter(ContentFilter.CDATA));
						if (!lContent.isEmpty()) {
							CDATA cdata =  (CDATA)lContent.get(0);
							String valueCDATA = cdata.getValue();
							if(valueCDATA != null && valueCDATA.matches(".*[0-9][0-9]pki/.*")) {	//Por convención las rutas que requiere Certificado digital van  en una ruta /XXpki/
								if (serial==null || serial.equals("")) {
									//Hay pantalla PKI pero no se tiene certificado
									//por lo que se ajustan los urls para enviar a la
									//generacion de certificado
									cdata.setText(appWebContextRoot + "/20secure/20generarCertificadoExt.jsp");
								}
							}
						}
					}
				}
			}

			XMLOutputter output = new XMLOutputter();
			return output.outputString(doc);

		} catch( Exception e) {
			throw new AppException("Error al obtener el menu del usuario", e);
		} finally {
			log.info("getMenuEpo(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Genera menu a partir del generico que aplica al perfil.
	 * Este metodo ajusta el menu, de acuerdo al IF.
	 * Dado que cada IF puede manejar solo ciertos productos
	 * no va a tener todas las opciones de menu asociadas al perfil
	 * Tambien se contempla el login del usuario para ajustar el menu
	 * de acuerdo a si ya tiene generado certificado digital o no, en caso de
	 * requerirlo.
	 * @param perfil Perfil del usuario
	 * @param claveIF Clave del IF
	 * @param serial Numero del serie del certificado digital (Cuando aplica)
	 * @param appWebContextRoot Ruta de contexto para la aplicación web.
	 * 		Permite ajustar adecuadamente url necesarios en algunos menus.
	 *
	 */
	public static String getMenuIF(String perfil,
			String claveIF, String serial, String appWebContextRoot) {

		log.info("getMenuIF(E)");

		//**********************************Validación de parametros:*****************************
		int iClaveIF = 0;
		try {
			if (perfil == null || claveIF == null || appWebContextRoot == null) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveIF = Integer.parseInt(claveIF);
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu del IF", e);
		}
		//****************************************************************************************


		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cg_menu " +
					" FROM seg_perfil " +
					" WHERE cc_perfil = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, perfil);
			ResultSet rs = ps.executeQuery();
			String menuXML = "";
			if (rs.next()) {
				Clob loClob = rs.getClob("cg_menu");
				//loClob.length debe estar entre 0 y 2^31-1 para que la siguiente instrucción sea válida
				menuXML = loClob.getSubString(1, (int)loClob.length());	//
			}
			rs.close();
			ps.close();

			List lMenuEliminar = new ArrayList();
			String epoValidaDescElec = "";
			CValidacion objValidaProd = new CValidacion();


			List mVecDatosSis = objValidaProd.prodHabilitado(0, 0, iClaveIF, "CDER");
			if ( mVecDatosSis == null || mVecDatosSis.size() == 0 )	{
				lMenuEliminar.add("34CESION");
			}

			mVecDatosSis = objValidaProd.prodHabilitado(0, 0, iClaveIF, "FDIST");
			if ( mVecDatosSis == null || mVecDatosSis.size() == 0 )	{
				lMenuEliminar.add("24DISTRIBUIDORES");
			}

			strSQL = 
					" SELECT ic_nafin_electronico " +
					" FROM comrel_nafin " +
					" WHERE ic_epo_pyme_if = ? " +
					" AND cg_tipo = ? ";
			List params = new ArrayList();
			params.add(new Integer(claveIF));
			params.add("I");
			Registros reg = con.consultarDB(strSQL, params);
			String ne = null;
			while(reg.next()) {
				ne = reg.getString("ic_nafin_electronico");
			}

			//busca los "menus x if" que deben ser excluidos, debido
			//a que no pertenecen a la EPO que esta entrando o los que se
			//parametrizaron explicitamente a ser restringidos al afiliado
			strSQL =
					" SELECT DISTINCT mar.cc_menu " +
					"  FROM com_menu_afiliado_restriccion mar, comrel_menu_afiliado_x_perfil maxp " +
					" WHERE mar.ic_tipo_afiliado = maxp.ic_tipo_afiliado " +
					"   AND mar.cc_perfil = maxp.cc_perfil " +
					"   AND mar.cc_menu = maxp.cc_menu " +
					"   AND maxp.cs_activo = ? " +
					"   AND mar.cc_perfil = ? " +
					"   AND mar.cg_tipo_restriccion = ? " +
					"   AND (   (    mar.cs_excluir = ? " +
					"            AND mar.cc_menu NOT IN ( " +
					"                   SELECT cc_menu " +
					"                     FROM com_menu_afiliado_restriccion " +
					"                    WHERE ic_nafin_electronico = ? " +
					"                      AND cg_tipo_restriccion = ? " +
					"                      AND cc_perfil = ? " +
					"                      AND cs_excluir = ? ) " +
					"           ) " +
					"        OR " +
					"           (mar.ic_nafin_electronico = ? AND mar.cs_excluir = ? " +
					"           ) " +
					"       ) ";

			params = new ArrayList();
			params.add("S");
			params.add(perfil);
			params.add("D");	//D= Directa
			params.add("N");
			params.add(new Integer(ne));
			params.add("D");	//D= Directa
			params.add(perfil);
			params.add("N");
			params.add(new Integer(ne));
			params.add("S");
			log.debug("getMenuEpo::" + strSQL + "\n" + params);
			reg = con.consultarDB(strSQL, params);
			while(reg.next()) {
				lMenuEliminar.add(reg.getString("cc_menu"));
			}
			
			lMenuEliminar.addAll(MenuUsuario.getMenusInactivos(perfil, 2)); //2=IF
			log.debug("getMenuIf:: se omiten los sig. menus: " + lMenuEliminar);
			//------------------------- Generacion del XML ajustado ------------------
			SAXBuilder parser = new SAXBuilder();

			Document doc = parser.build(new StringReader(menuXML));

			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null) {
					if (el.getAttribute("id") != null) {
						if ( lMenuEliminar.contains(el.getAttribute("id").getValue()) ) {
							it.remove();
						}
					} else if ( "userdata".equalsIgnoreCase(el.getName()) ) {
						List lContent = el.getContent(new ContentFilter(ContentFilter.CDATA));
						if (!lContent.isEmpty()) {
							CDATA cdata =  (CDATA)lContent.get(0);
							String valueCDATA = cdata.getValue();
							if(valueCDATA != null && valueCDATA.matches(".*[0-9][0-9]pki/.*")) {	//Por convención las rutas que requiere Certificado digital van  en una ruta /XXpki/
								if (serial==null || serial.equals("")) {
									//Hay pantalla PKI pero no se tiene certificado
									//por lo que se ajustan los urls para enviar a la
									//generacion de certificado
									cdata.setText(appWebContextRoot + "/20secure/20generarCertificadoExt.jsp");
								}
							}
						}
					}
				}
			}

			XMLOutputter output = new XMLOutputter();
			return output.outputString(doc);

		} catch( Exception e) {
			throw new AppException("Error al obtener el menu del usuario", e);
		} finally {
			log.info("getMenuIF(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Genera menu a partir del generico que aplica al perfil.
	 * Tambien se contempla el login del usuario para ajustar el menu
	 * de acuerdo a si ya tiene generado certificado digital o no, en caso de
	 * requerirlo.
	 * @param perfil Perfil del usuario
	 * @param serial Numero del serie del certificado digital (Cuando aplica)
	 * @param appWebContextRoot Ruta de contexto para la aplicación web.
	 * 		Permite ajustar adecuadamente url necesarios en algunos menus.
	 *
	 */
	public static String getMenuNafin(String perfil,
			String serial, String appWebContextRoot) {

		log.info("getMenuNafin(E)");

		//**********************************Validación de parametros:*****************************
		try {
			if (perfil == null || appWebContextRoot == null) {
				throw new Exception("Los parametros son requeridos");
			}
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu Nafin", e);
		}
		//****************************************************************************************


		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cg_menu " +
					" FROM seg_perfil " +
					" WHERE cc_perfil = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, perfil);
			ResultSet rs = ps.executeQuery();
			String menuXML = "";
			if (rs.next()) {
				Clob loClob = rs.getClob("cg_menu");
				//loClob.length debe estar entre 0 y 2^31-1 para que la siguiente instrucción sea válida
				menuXML = loClob.getSubString(1, (int)loClob.length());	//
			}
			rs.close();
			ps.close();

			List lMenuEliminar = new ArrayList();
			
			lMenuEliminar.addAll(MenuUsuario.getMenusInactivos(perfil, 4)); //4=NAFIN
			log.debug("getMenuNafin:: se omiten los sig. menus: " + lMenuEliminar);
			//------------------------- Generacion del XML ajustado ------------------
			SAXBuilder parser = new SAXBuilder();

			Document doc = parser.build(new StringReader(menuXML));

			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null) {
					if (el.getAttribute("id") != null) {
						if ( lMenuEliminar.contains(el.getAttribute("id").getValue()) ) {
							it.remove();
						}
					} else if ( "userdata".equalsIgnoreCase(el.getName()) ) {
						List lContent = el.getContent(new ContentFilter(ContentFilter.CDATA));
						if (!lContent.isEmpty()) {
							CDATA cdata =  (CDATA)lContent.get(0);
							String valueCDATA = cdata.getValue();
							if(valueCDATA != null && valueCDATA.matches(".*[0-9][0-9]pki/.*")) {	//Por convención las rutas que requiere Certificado digital van  en una ruta /XXpki/
								if (serial==null || serial.equals("")) {
									//Hay pantalla PKI pero no se tiene certificado
									//por lo que se ajustan los urls para enviar a la
									//generacion de certificado
									cdata.setText(appWebContextRoot + "/20secure/20generarCertificadoExt.jsp");
								}
							}
						}
					}
				}
			}

			XMLOutputter output = new XMLOutputter();
			return output.outputString(doc);

		} catch( Exception e) {
			throw new AppException("Error al obtener el menu del usuario", e);
		} finally {
			log.info("getMenuNafin(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Genera menu de accesos directos a partir del xml especificado.
	 * No se obtiene directamente de la base de datos, dado que segun el
	 * afiliado y su parametrización de productos,
	 * debe o no mostrar las opciones de menu. Incluso el url en el XML
	 * puede diferir del que está en Base de Datos.
	 * @param xml Xml del usuario
	 * @param perfil Perfil del usuario
	 */
	public static Registros getMenuDirecto(String menuXML, String perfil) {
		log.info("getMenuDirecto(E)");

		//**********************************Validación de parametros:*****************************
		try {
			if (perfil == null || menuXML == null) {
				throw new Exception("Los parametros son requeridos");
			}
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu de acceso directo del afiliado", e);
		}
		//****************************************************************************************


		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cc_menu " +
					" FROM comrel_menu_afiliado_x_perfil " +
					" WHERE cc_perfil = ? " +
					" AND cs_acceso_directo = ? ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, perfil);
			ps.setString(2, "S");
			ResultSet rs = ps.executeQuery();

			List lMenusAccesoDirecto = new ArrayList();
			while (rs.next()) {
				lMenusAccesoDirecto.add(rs.getString("cc_menu"));
			}
			rs.close();
			ps.close();

			//_-----------------------------------------------------------------------------------
			List lMenusAccesoDirectoAMostrar = new ArrayList();

			SAXBuilder parser = new SAXBuilder();
			Document doc = parser.build(new StringReader(menuXML));
			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null && el.getAttribute("id") != null &&
						lMenusAccesoDirecto.contains(el.getAttributeValue("id"))) {

					List children = el.getChildren();
					if (children.size()>0) {
						Iterator itChildren = children.iterator();
						while(itChildren.hasNext()) {
							Element elChildren = (Element)itChildren.next();
							if (elChildren != null && "userdata".equals(elChildren.getName()) && "url".equals(elChildren.getAttributeValue("name")) ) {
								List registro = new ArrayList();
								registro.add(el.getAttributeValue("id"));	//Clave menu
								registro.add(el.getAttributeValue("text"));	//Nombre menu
								String signo = "?";
								if (elChildren.getText().indexOf("?") != -1) {
									signo = "&";
								} else {
									signo = "?";
								}
								registro.add(elChildren.getText() + signo + "idMenu=" + el.getAttributeValue("id"));	//URL
								lMenusAccesoDirectoAMostrar.add(registro);
								break;
							}
						}
					} //if children >0
				}
			}//fin while de elementos del menu

			List lEncabezados = new ArrayList();
			lEncabezados.add("ID");
			lEncabezados.add("TEXT");
			lEncabezados.add("URL");
			Registros regMenusAccesoDirectoAMostrar = new Registros();
			regMenusAccesoDirectoAMostrar.setNombreColumnas(lEncabezados);
			regMenusAccesoDirectoAMostrar.setData(lMenusAccesoDirectoAMostrar);

			return regMenusAccesoDirectoAMostrar;


		} catch( Exception e) {
			throw new AppException("Error al obtener el menu de acceso directo del afiliado", e);
		} finally {
			log.info("getMenuDirecto(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	public static Registros getMenuPymeDirecto(String menuXML, String perfil) {
		return MenuUsuario.getMenuDirecto(menuXML, perfil);
	}

	/**
	 * Determina si el menu del usuario contiene el elemento con el id
	 * especificado.
	 * @param menuXml Xml del menu del usuario
	 * @param id Id del elemento a buscar
	 * @return true si contiene el elemento especificado o false de lo contrario
	 */
	public static boolean menuContieneElemento(String menuXML, String id) {
		log.info("menuContieneElemento(E)");

		//**********************************Validación de parametros:*****************************
		try {
			if (menuXML == null || id == null) {
				throw new Exception("Los parametros son requeridos");
			}
		} catch(Exception e) {
			throw new AppException("Error al determinar la existencia del elemento de menu", e);
		}
		//****************************************************************************************

		try {
			SAXBuilder parser = new SAXBuilder();
			Document doc = parser.build(new StringReader(menuXML));
			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null && el.getAttributeValue("id") != null &&  el.getAttributeValue("id").equals(id)) {
					return true;
				}
			}//fin while de elementos del menu

			return false;

		} catch( Exception e) {
			throw new AppException("Error al determinar la existencia del elemento de menu", e);
		} finally {
			log.info("menuContieneElemento(S)");
		}
	}


	/**
	 * metodo para obtener el Menu  el tipo de Usuario Universidad
	 *
	 * @return 
	 * @param appWebContextRoot
	 * @param serial
	 * @param claveIF
	 * @param perfil
	 */
	public static String getMenuUNI(String perfil,
			String claveIF, String serial, String appWebContextRoot) {

		log.info("getMenuUNI(E)");

		//**********************************Validación de parametros:*****************************
		int iClaveIF = 0;
		try {
			if (perfil == null || claveIF == null || appWebContextRoot == null) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveIF = Integer.parseInt(claveIF);
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu de la Universidad", e);
		}
		//****************************************************************************************


		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cg_menu " +
					" FROM seg_perfil " +
					" WHERE cc_perfil = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, perfil);
			ResultSet rs = ps.executeQuery();
			String menuXML = "";
			if (rs.next()) {
				Clob loClob = rs.getClob("cg_menu");
				//loClob.length debe estar entre 0 y 2^31-1 para que la siguiente instrucción sea válida
				menuXML = loClob.getSubString(1, (int)loClob.length());	//
			}
			rs.close();
			ps.close();

			List lMenuEliminar = new ArrayList();

			lMenuEliminar.addAll(MenuUsuario.getMenusInactivos(perfil, 14)); //14=UNIVERSIDAD
			log.debug("getMenuUNI:: se omiten los sig. menus: " + lMenuEliminar);
			//------------------------- Generacion del XML ajustado ------------------
			SAXBuilder parser = new SAXBuilder();

			Document doc = parser.build(new StringReader(menuXML));

			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null) {
					if (el.getAttribute("id") != null) {
						if ( lMenuEliminar.contains(el.getAttribute("id").getValue()) ) {
							it.remove();
						}
					} else if ( "userdata".equalsIgnoreCase(el.getName()) ) {
						List lContent = el.getContent(new ContentFilter(ContentFilter.CDATA));
						if (!lContent.isEmpty()) {
							CDATA cdata =  (CDATA)lContent.get(0);
							String valueCDATA = cdata.getValue();
							if(valueCDATA != null && valueCDATA.matches(".*[0-9][0-9]pki/.*")) {	//Por convención las rutas que requiere Certificado digital van  en una ruta /XXpki/
								if (serial==null || serial.equals("")) {
									//Hay pantalla PKI pero no se tiene certificado
									//por lo que se ajustan los urls para enviar a la
									//generacion de certificado
									cdata.setText(appWebContextRoot + "/20secure/20generarCertificadoExt.jsp");
								}
							}
						}
					}
				}
			}

			XMLOutputter output = new XMLOutputter();
			return output.outputString(doc);

		} catch( Exception e) {
			throw new AppException("Error al obtener el menu del usuario", e);
		} finally {
			log.info("getMenuIF(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	
	/**
	 * metodo para generar el menu de Afianzadora
	 * @return 
	 * @param appWebContextRoot
	 * @param serial
	 * @param claveIF
	 * @param perfil 
	 */
	public static String getMenuAfianzadora(String perfil,
			String claveAfianzadora, String serial, String appWebContextRoot) {

		log.info("getMenuAfianzadora(E)");

		//**********************************Validación de parametros:*****************************
		int iClaveAfianzadora = 0;
		try {
			if (perfil == null || claveAfianzadora == null || appWebContextRoot == null) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveAfianzadora = Integer.parseInt(claveAfianzadora);
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu de la Afianzadora", e);
		}
		//****************************************************************************************


		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cg_menu " +
					" FROM seg_perfil " +
					" WHERE cc_perfil = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, perfil);
			ResultSet rs = ps.executeQuery();
			String menuXML = "";
			if (rs.next()) {
				Clob loClob = rs.getClob("cg_menu");
				//loClob.length debe estar entre 0 y 2^31-1 para que la siguiente instrucción sea válida
				menuXML = loClob.getSubString(1, (int)loClob.length());	//
			}
			rs.close();
			ps.close();

			List lMenuEliminar = new ArrayList();

			lMenuEliminar.addAll(MenuUsuario.getMenusInactivos(perfil, 12)); //12=AFIANZADORA
			log.debug("getMenuAfianzadora:: se omiten los sig. menus: " + lMenuEliminar);
			//------------------------- Generacion del XML ajustado ------------------
			SAXBuilder parser = new SAXBuilder();

			Document doc = parser.build(new StringReader(menuXML));

			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null) {
					if (el.getAttribute("id") != null) {
						if ( lMenuEliminar.contains(el.getAttribute("id").getValue()) ) {
							it.remove();
						}
					} else if ( "userdata".equalsIgnoreCase(el.getName()) ) {
						List lContent = el.getContent(new ContentFilter(ContentFilter.CDATA));
						if (!lContent.isEmpty()) {
							CDATA cdata =  (CDATA)lContent.get(0);
							String valueCDATA = cdata.getValue();
							if(valueCDATA != null && valueCDATA.matches(".*[0-9][0-9]pki/.*")) {	//Por convención las rutas que requiere Certificado digital van  en una ruta /XXpki/
								if (serial==null || serial.equals("")) {
									//Hay pantalla PKI pero no se tiene certificado
									//por lo que se ajustan los urls para enviar a la
									//generacion de certificado
									cdata.setText(appWebContextRoot + "/20secure/20generarCertificadoExt.jsp");
								}
							}
						}
					}
				}
			}

			XMLOutputter output = new XMLOutputter();
			return output.outputString(doc);

		} catch( Exception e) {
			throw new AppException("Error al obtener el menu del usuario", e);
		} finally {
			log.info("getMenuAfianzadora(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	
		/**
	 * metodo para generar el menu de Afianzadora
	 * @return 
	 * @param appWebContextRoot
	 * @param serial
	 * @param claveIF
	 * @param perfil 
	 */
	public static String getMenuFIADO(String perfil,
			String claveFiado, String serial, String appWebContextRoot) {

		log.info("getMenuFIADO(E)");

		//**********************************Validación de parametros:*****************************
		int iClaveAfianzadora = 0;
		try {
			if (perfil == null || claveFiado == null || appWebContextRoot == null) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveAfianzadora = Integer.parseInt(claveFiado);
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu de  FIADO", e);
		}
		//****************************************************************************************


		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cg_menu " +
					" FROM seg_perfil " +
					" WHERE cc_perfil = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, perfil);
			ResultSet rs = ps.executeQuery();
			String menuXML = "";
			if (rs.next()) {
				Clob loClob = rs.getClob("cg_menu");
				//loClob.length debe estar entre 0 y 2^31-1 para que la siguiente instrucción sea válida
				menuXML = loClob.getSubString(1, (int)loClob.length());	//
			}
			rs.close();
			ps.close();

			List lMenuEliminar = new ArrayList();

			lMenuEliminar.addAll(MenuUsuario.getMenusInactivos(perfil, 13)); //13= FIADO
			log.debug("getMenuFIADO:: se omiten los sig. menus: " + lMenuEliminar);
			//------------------------- Generacion del XML ajustado ------------------
			SAXBuilder parser = new SAXBuilder();

			Document doc = parser.build(new StringReader(menuXML));

			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null) {
					if (el.getAttribute("id") != null) {
						if ( lMenuEliminar.contains(el.getAttribute("id").getValue()) ) {
							it.remove();
						}
					} else if ( "userdata".equalsIgnoreCase(el.getName()) ) {
						List lContent = el.getContent(new ContentFilter(ContentFilter.CDATA));
						if (!lContent.isEmpty()) {
							CDATA cdata =  (CDATA)lContent.get(0);
							String valueCDATA = cdata.getValue();
							if(valueCDATA != null && valueCDATA.matches(".*[0-9][0-9]pki/.*")) {	//Por convención las rutas que requiere Certificado digital van  en una ruta /XXpki/
								if (serial==null || serial.equals("")) {
									//Hay pantalla PKI pero no se tiene certificado
									//por lo que se ajustan los urls para enviar a la
									//generacion de certificado
									cdata.setText(appWebContextRoot + "/20secure/20generarCertificadoExt.jsp");
								}
							}
						}
					}
				} 
			}

			XMLOutputter output = new XMLOutputter();
			return output.outputString(doc);

		} catch( Exception e) {
			throw new AppException("Error al obtener el menu del usuario", e);
		} finally {
			log.info("getMenuFIADO(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}  
	
	/**
	 * Genera menu a partir del generico que aplica al perfil.
	 * Tambien se contempla el login del usuario para ajustar el menu
	 * de acuerdo a si ya tiene generado certificado digital o no, en caso de
	 * requerirlo.
	 * @param perfil Perfil del usuario
	 * @param serial Numero del serie del certificado digital (Cuando aplica)
	 * @param appWebContextRoot Ruta de contexto para la aplicación web.
	 * 		Permite ajustar adecuadamente url necesarios en algunos menus.
	 *
	 */
	public static String getMenuPerfil(String perfil) {

		log.info("getMenuPerfil(E)");

		//**********************************Validación de parametros:*****************************
		try {
			if (perfil == null ) {
				throw new Exception("Los parametros son requeridos");
			}
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu Nafin", e);
		}
		//****************************************************************************************


		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cg_menu " +
					" FROM seg_perfil " +
					" WHERE cc_perfil = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, perfil);
			ResultSet rs = ps.executeQuery();
			String menuXML = "";
			if (rs.next()) {
				Clob loClob = rs.getClob("cg_menu");
				//loClob.length debe estar entre 0 y 2^31-1 para que la siguiente instrucción sea válida
				menuXML = loClob.getSubString(1, (int)loClob.length());	//
			}
			rs.close();
			ps.close();

			List lMenuEliminar = new ArrayList();

			//------------------------- Generacion del XML ajustado ------------------
			SAXBuilder parser = new SAXBuilder();

			Document doc = parser.build(new StringReader(menuXML));

			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null) {
					if (el.getAttribute("id") != null) {
						if ( lMenuEliminar.contains(el.getAttribute("id").getValue()) ) {
							it.remove();
						}
					} else if ( "userdata".equalsIgnoreCase(el.getName()) ) {
						List lContent = el.getContent(new ContentFilter(ContentFilter.CDATA));
						if (!lContent.isEmpty()) {
							CDATA cdata =  (CDATA)lContent.get(0);
							String valueCDATA = cdata.getValue();
							if(valueCDATA != null && valueCDATA.matches(".*[0-9][0-9]pki/.*")) {	//Por convención las rutas que requiere Certificado digital van  en una ruta /XXpki/
								/*if (serial==null || serial.equals("")) {
									//Hay pantalla PKI pero no se tiene certificado
									//por lo que se ajustan los urls para enviar a la
									//generacion de certificado
									cdata.setText(appWebContextRoot + "/20secure/20generarCertificadoExt.jsp");
								}*/
							}
						}
					}
				}
			}

			XMLOutputter output = new XMLOutputter();
			return output.outputString(doc);

		} catch( Exception e) {
			throw new AppException("Error al obtener el menu del usuario", e);
		} finally {
			log.info("getMenuPerfil(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * Obtiene la lista de menus/opciones que estan inactivos,
	 * para que puedan ser eliminados del menu final.
	 * 
	 * Esta funcionalidad debería implementarse en Menu.java, sin embargo
	 * la implementación sería mucho más compleja, por que habría que identificar
	 * por cada nodo si alguno de los antecesores (papa, abuelo, bisabuelo, etc...) 
	 * está marcada como inactivo y así eliminar toda la rama.  Y en esta clase de todas maneras se 
	 * barre el XML para eliminar ramas no deseadas.
	 * @param perfil Perfil del usuario
	 * @param claveTipoAfiliado Clave del tipo de afiliado (1 Epo, 2 IF , 3 PyME   , 4 Nafin, etc...)
	 *
	 */
	private static List getMenusInactivos(String perfil, int claveTipoAfiliado) {

		log.info("getMenusInactivos(E)");

		//**********************************Validación de parametros:*****************************
		try {
			if (perfil == null ) {
				throw new Exception("Los parametros son requeridos");
			}
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu Nafin", e);
		}
		//****************************************************************************************


		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cc_menu " +
					" FROM comcat_menu_afiliado " +
					" WHERE cs_activo = ? AND ic_tipo_afiliado = ? " +
					" UNION ALL " +
					" SELECT cc_menu " +
					" FROM comrel_menu_afiliado_x_perfil " +
					" WHERE cs_activo = ? AND cc_perfil = ? " +
					" AND ic_tipo_afiliado = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, "N");
			ps.setInt(2, claveTipoAfiliado);
			ps.setString(3, "N");
			ps.setString(4, perfil);
			ps.setInt(5, claveTipoAfiliado);
			ResultSet rs = ps.executeQuery();
			List menusInactivos = new ArrayList();
			while (rs.next()) {
				menusInactivos.add(rs.getString("cc_menu"));
			}
			rs.close();
			ps.close();
			
			return menusInactivos;
		} catch( Exception e) {
			throw new AppException("Error al obtener el menu del usuario", e);
		} finally {
			log.info("getMenusInactivos(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	
	public static String getMenuClienteExt(String perfil,
			String claveFiado, String serial, String appWebContextRoot) {

		log.info("getMenuClienteExt(E)");

		//**********************************Validación de parametros:*****************************
		int iClaveAfianzadora = 0;
		try {
			if (perfil == null || claveFiado == null || appWebContextRoot == null) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveAfianzadora = Integer.parseInt(claveFiado);
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu de Cliente Externo", e);
		}
		//****************************************************************************************


		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cg_menu " +
					" FROM seg_perfil " +
					" WHERE cc_perfil = ? ";

			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, perfil);
			ResultSet rs = ps.executeQuery();
			String menuXML = "";
			if (rs.next()) {
				Clob loClob = rs.getClob("cg_menu");
				//loClob.length debe estar entre 0 y 2^31-1 para que la siguiente instrucción sea válida
				menuXML = loClob.getSubString(1, (int)loClob.length());	//
			}
			rs.close();
			ps.close();

			List lMenuEliminar = new ArrayList();

			//lMenuEliminar.addAll(MenuUsuario.getMenusInactivos(perfil, 13));
			log.debug("getMenuClienteExt:: se omiten los sig. menus: " + lMenuEliminar);
			//------------------------- Generacion del XML ajustado ------------------
			SAXBuilder parser = new SAXBuilder();

			Document doc = parser.build(new StringReader(menuXML));

			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null) {
					if (el.getAttribute("id") != null) {
						if ( lMenuEliminar.contains(el.getAttribute("id").getValue()) ) {
							it.remove();
						}
					} else if ( "userdata".equalsIgnoreCase(el.getName()) ) {
						List lContent = el.getContent(new ContentFilter(ContentFilter.CDATA));
						if (!lContent.isEmpty()) {
							CDATA cdata =  (CDATA)lContent.get(0);
							String valueCDATA = cdata.getValue();
							if(valueCDATA != null && valueCDATA.matches(".*[0-9][0-9]pki/.*")) {	//Por convención las rutas que requiere Certificado digital van  en una ruta /XXpki/
								if (serial==null || serial.equals("")) {
									//Hay pantalla PKI pero no se tiene certificado
									//por lo que se ajustan los urls para enviar a la
									//generacion de certificado
									cdata.setText(appWebContextRoot + "/20secure/20generarCertificadoExt.jsp");
								}
							}
						}
					}
				} 
			}

			XMLOutputter output = new XMLOutputter();
			return output.outputString(doc);

		} catch( Exception e) {
			throw new AppException("Error al obtener el menu del usuario", e);
		} finally {
			log.info("getMenuClienteExt(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	/**
	 * Método para obtener el Menu  el tipo de Usuario IFNB CEDI
	 *
	 * @return 
	 * @param appWebContextRoot
	 * @param serial
	 * @param claveIFNB
	 * @param perfil
	 */
	public static String getMenuIFNBCedi(String perfil,
			String claveIFNB, String serial, String appWebContextRoot) {

		log.info("getMenuIFNBCedi(E)");

		//**********************************Validación de parametros:*****************************
		int iClaveIFNB = 0;
		try {
			if (perfil == null || claveIFNB == null || appWebContextRoot == null) {
				throw new Exception("Los parametros son requeridos");
			}
			//iClaveIFNB = Integer.parseInt(claveIFNB);
		} catch(Exception e) {
			throw new AppException("Error al obtener el menu Cedi", e);
		}
		//****************************************************************************************

		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String strSQL =
					" SELECT cg_menu " +
					" FROM seg_perfil " +
					" WHERE cc_perfil = ? ";
		
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, perfil);
			ResultSet rs = ps.executeQuery();
			String menuXML = "";
			if (rs.next()) {
				Clob loClob = rs.getClob("cg_menu");
				//loClob.length debe estar entre 0 y 2^31-1 para que la siguiente instrucción sea válida
				menuXML = loClob.getSubString(1, (int)loClob.length());  //
			}
			rs.close();
			ps.close();
		  
			List lMenuEliminar = new ArrayList();

			lMenuEliminar.addAll(MenuUsuario.getMenusInactivos(perfil, 15)); //14=UNIVERSIDAD
		log.debug("getMenuIFNB:: se omiten los sig. menus: " + lMenuEliminar);
			//------------------------- Generacion del XML ajustado ------------------
			SAXBuilder parser = new SAXBuilder();

			Document doc = parser.build(new StringReader(menuXML));

			Element rootElement = doc.getRootElement();

			Iterator it = rootElement.getDescendants(new org.jdom.filter.ElementFilter());
			while(it.hasNext()) {
				Element el = (Element)it.next();
				if (el != null) {
					if (el.getAttribute("id") != null) {
						if ( lMenuEliminar.contains(el.getAttribute("id").getValue()) ) {
							it.remove();
						}
					} else if ( "userdata".equalsIgnoreCase(el.getName()) ) {
						List lContent = el.getContent(new ContentFilter(ContentFilter.CDATA));
						if (!lContent.isEmpty()) {
							CDATA cdata =  (CDATA)lContent.get(0);
							String valueCDATA = cdata.getValue();
							if(valueCDATA != null && valueCDATA.matches(".*[0-9][0-9]pki/.*")) { //Por convención las rutas que requiere Certificado digital van  en una ruta /XXpki/
								if (serial==null || serial.equals("")) {
									//Hay pantalla PKI pero no se tiene certificado
									//por lo que se ajustan los urls para enviar a la
									//generacion de certificado
									cdata.setText(appWebContextRoot + "/20secure/20generarCertificadoExt.jsp");
								}
							}
						}
					}
				}
			}

			XMLOutputter output = new XMLOutputter();
			return output.outputString(doc);

		} catch( Exception e) {
			throw new AppException("Error al obtener el menu del usuario", e);
		} finally {
			log.info("getMenuIFNB(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	
}
