package netropology.utilerias;

import java.sql.CallableStatement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;

public class OpcionMenuDAO  {

	private final static Log log = ServiceLocator.getInstance().getLog(OpcionMenuDAO.class);

	public OpcionMenuDAO() {
	}

	/**
	 * Realiza la validaci�n de la informaci�n seg�n corresponda a la accion
	 * especificada
	 * @param accion Accion a validar: insertar, actualizar
	 * @param opcionMenu Objeto que contiene la informaci�n de la opci�n del men�
	 *
	 * @return Mapa con los errores presentados de la validaci�n o mapa vac�o de lo contrario
	 */
	public static Map validar(String accion, OpcionMenu opcionMenu) {
		log.info("validar(E)");
		Map errores = new HashMap();
		AccesoDB con = new AccesoDB();

		//**********************************Validacion de parametros:*****************************
		try {
			if (accion == null || accion.equals("")) {
				throw new Exception("Los parametros no pueden ser vacios");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." +
					e.getMessage() + "\n" +	accion);
			throw new AppException("Error en los parametros recibidos.", e);
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			if (accion.equals("insertar")) {
				//Validaciones para insertar
				String strSQL =
						" SELECT COUNT(*) as numReg " +
						" FROM comcat_menu_afiliado " +
						" WHERE cc_menu = UPPER(?) " +
						" AND ic_tipo_afiliado = ? ";
				List params = new ArrayList();
				params.add(opcionMenu.getClaveMenu());
				params.add(new Integer(opcionMenu.getClaveTipoAfiliado()));
				Registros reg = con.consultarDB(strSQL,params);
				reg.next();
				if (!reg.getString("numReg").equals("0")) {
					errores.put("cc_menu", "La clave del menu '" + opcionMenu.getClaveMenu() + "' ya existe");
				}
				if (opcionMenu.getTipo().equals("P") && (opcionMenu.getUrl() == null || "".equals(opcionMenu.getUrl())) ) {
					errores.put("cg_url", "El URL es requerido para las opciones de menu de tipo Pantalla");
				}
			} else if (accion.equals("actualizar")) {
				if (opcionMenu.getTipo().equals("P") && (opcionMenu.getUrl() == null || "".equals(opcionMenu.getUrl())) ) {
					errores.put("cg_url", "El URL es requerido para las opciones de menu de tipo Pantalla");
				}
			} else {
				throw new AppException("Accion no implementada");
			}
			return errores;
		} catch(Exception e) {
			throw new AppException("Error al validar la informaci�n", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("validar(S)");
		}
	}


	/**
	 * Realiza la validaci�n de la informaci�n seg�n corresponda a la accion
	 * especificada
	 * @param accion Accion a validar: eliminar
	 * @param opcionesMenu lista de objetos OpcionMenu que contiene la informaci�n a validar
	 * @return Mapa con los errores presentados de la validaci�n o mapa vac�o de lo contrario
	 */
	public static Map validar(String accion, List opcionesMenu) {
		log.info("validar(E)");
		Map errores = new HashMap();
		AccesoDB con = new AccesoDB();

		//**********************************Validacion de parametros:*****************************
		try {
			if (accion == null || accion.equals("")) {
				throw new Exception("Los parametros no pueden ser vacios");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." +
					e.getMessage() + "\n" +	accion);
			throw new AppException("Error en los parametros recibidos.", e);
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			if (accion.equals("eliminar")) {
				String strSQL =
						" SELECT cc_menu " +
						" FROM comrel_menu_afiliado_x_perfil " +
						" WHERE ic_tipo_afiliado = ? " +
						" AND cc_menu IN (" + Comunes.repetirCadenaConSeparador("?", ",", opcionesMenu.size()) + ")";
				List params = new ArrayList();
				Iterator it = opcionesMenu.iterator();
				int i = 0;
				while (it.hasNext()) {
					OpcionMenu opcion = (OpcionMenu)it.next();
					if (i == 0) {
						//la clave del tipo de afiliado ser�a la misma para todos debido a la estructura de la pantalla.
						//por ello solo se toma un valor.
						params.add(new Integer(opcion.getClaveTipoAfiliado()));
					}
					params.add(opcion.getClaveMenu());
					i++;
				}
				log.debug("validar(strSQL)::" + strSQL + "\n" + params);
				Registros reg = con.consultarDB(strSQL, params);
				StringBuffer clavesRelacionadas = new StringBuffer("");
				while(reg.next()){
					clavesRelacionadas.append(reg.getString("cc_menu") + "\n");
				}
				if (clavesRelacionadas.length() > 0) {
					clavesRelacionadas.deleteCharAt(clavesRelacionadas.length()-1);
					errores.put("cc_menu", "Las siguientes claves aun est�n asignadas \npor lo que no pueden ser eliminadas." +"\n" +
							clavesRelacionadas);
				}

			} else {
				throw new AppException("Accion no implementada");
			}
			return errores;
		} catch(Exception e) {
			throw new AppException("Error al validar la informaci�n", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("validar(S)");
		}
	}

	/**
	 * Realiza la inserci�n de una opcion de men�
	 * @param opcion Bean con la informaci�n de la opci�n de menu
	 */
	public static void insertar(OpcionMenu opcion) {
		log.info("insertar(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;

		//**********************************Validacion de parametros:*****************************
		try {
			if (opcion == null || opcion.getClaveMenu() == null ||
					opcion.getClaveMenuPadre() == null ||
					opcion.getClaveTipoAfiliado() == null ||
					opcion.getDescripcion() == null ||
					opcion.getNombre() == null ||
					opcion.getTipo() == null ||
					opcion.getUrl() == null) {
				throw new Exception("Los parametros/atributos no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros/atributos recibidos." +
					e.getMessage() + "\n" +	opcion);
			throw new AppException("Error en los parametros/atributos recibidos.", e);
		}
		//***************************************************************************************


		try {
			con.conexionDB();
			CallableStatement cs = con.ejecutaSP("SP_REGISTRA_MENU_AFILIADO(?,?,?,?,?,?,?,?)");
			cs.setInt(1, Integer.parseInt(opcion.getClaveTipoAfiliado()));
			cs.setString(2, opcion.getClaveMenu().toUpperCase());
			cs.setString(3, opcion.getNombre());
			cs.setString(4, opcion.getDescripcion());
			cs.setString(5, opcion.getTipo());
			cs.setString(6, opcion.getUrl());
			cs.setString(7,  ( "/".equals(opcion.getClaveMenuPadre())?"":opcion.getClaveMenuPadre() ) );	// Si es / representa el men� ra�z
			cs.setString(8, opcion.getActivo());
			cs.execute();
			cs.close();

			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			throw new AppException("Error al insertar el registro", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("insertar(S)");
		}
	}


	/**
	 * Realiza la actualizaci�n de una opcion de men�
	 * @param opcion Bean con la informaci�n de la opci�n de menu
	 */
	public static void actualizar(OpcionMenu opcion) {
		log.info("actualizar(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;

		//**********************************Validacion de parametros:*****************************
		try {
			if (opcion == null || opcion.getClaveMenu() == null ||
					opcion.getClaveTipoAfiliado() == null ||
					opcion.getDescripcion() == null ||
					opcion.getNombre() == null ||
					opcion.getActivo() == null) {
				throw new Exception("Los parametros/atributos no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros/atributos recibidos." +
					e.getMessage() + "\n" +	opcion);
			throw new AppException("Error en los parametros/atributos recibidos.", e);
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			String strSQL =
					" UPDATE comcat_menu_afiliado " +
					" SET cg_nombre = ?, " +
					" cg_descripcion = ?, " +
					" cg_url = ?, " +
					" cs_activo = ? " +
					" WHERE cc_menu = ? " +
					" AND ic_tipo_afiliado = ? ";
			List params = new ArrayList();
			params.add(opcion.getNombre());
			params.add(opcion.getDescripcion());
			params.add(opcion.getUrl());
			params.add(opcion.getActivo());
			params.add(opcion.getClaveMenu());
			params.add(opcion.getClaveTipoAfiliado());
			con.ejecutaUpdateDB(strSQL, params);

			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			throw new AppException("Error al actualizar el registro", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("actualizar(S)");
		}
	}


	/**
	 * Realiza la eliminaci�n de una o varias opcion de men�
	 * @param opciones Lista de beans (OpcionMenu) con las opciones de menu
	 */
	public static void eliminar(List opciones) {
		log.info("eliminar(E)");

		//**********************************Validacion de parametros:*****************************
		try {
			if (opciones == null || opciones.size() == 0) {
				throw new Exception("Los parametros no pueden ser vacios");
			}
		} catch(Exception e) {
			log.error("Error en los parametros/atributos recibidos." +
					e.getMessage() + "\n" +	opciones);
			throw new AppException("Error en los parametros recibidos.", e);
		}
		//***************************************************************************************

		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		try {
			con.conexionDB();
			Iterator it = opciones.iterator();
			int claveTipoAfiliado = 0;
			List clavesMenu = new ArrayList();
			int i = 0;
			while (it.hasNext()) {
				OpcionMenu opcion = (OpcionMenu)it.next();
				if (i == 0) {
					//la clave del tipo de afiliado ser�a la misma para todos debido a la estructura de la pantalla.
					//por ello solo se toma un valor.
					claveTipoAfiliado = Integer.parseInt(opcion.getClaveTipoAfiliado());
				}
				clavesMenu.add(opcion.getClaveMenu());
				i++;
			}

			String strSQL =
					" DELETE FROM comcat_menu_afiliado " +
					" WHERE ic_tipo_afiliado = ? " +
					" AND cc_menu IN (" + Comunes.repetirCadenaConSeparador("?", ",", clavesMenu.size()) + ")";

			List params = new ArrayList();
			params.add(new Integer(claveTipoAfiliado));
			params.addAll(clavesMenu);

			con.ejecutaUpdateDB(strSQL, params);

			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			throw new AppException("Error al eliminar el registro", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
	}

}