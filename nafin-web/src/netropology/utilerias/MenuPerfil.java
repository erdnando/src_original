package netropology.utilerias;

import java.util.List;

/**
 * Clase que representa la relacion del menu con el perfil (comrel_menu_afiliado_x_perfil)
 */
public class MenuPerfil implements java.io.Serializable {

	private String clavePerfil;
	private String claveTipoAfiliado;
	private String claveMenu;
	private String posicion;
	private String activo;
	private String claveAfiliado;
	private String tipoAfiliado;
	private String claveAfiliadoRelacionado;
	private String tipoAfiliadoRelacionado;
	private String accesoDirecto;
	private String tipoRestriccion;
	private List lstAfilRelacionados;

	public MenuPerfil() {
	}

	/**
	 * Obtiene la clave del perfil
	 * @return Clave del perfil
	 */
	public String getClavePerfil() {
		return clavePerfil;
	}

	/**
	 * Establece la clave del perfil
	 * @param clavePerfil Clave del perfil
	 */
	public void setClavePerfil(String clavePerfil) {
		this.clavePerfil = clavePerfil;
	}

	/**
	 * Obtiene la clave del tipo de afiliado (comcat_tipo_afiliado.ic_tipo_afiliado)
	 * @return Clave del tipo de afiliado 
	 */
	public String getClaveTipoAfiliado() {
		return claveTipoAfiliado;
	}

	/**
	 * Establece la clave del tipo de afiliado (comcat_tipo_afiliado.ic_tipo_afiliado)
	 * @param claveTipoAfiliado Clave del tipo de afiliado
	 */
	public void setClaveTipoAfiliado(String claveTipoAfiliado) {
		this.claveTipoAfiliado = claveTipoAfiliado;
	}

	/**
	 * Obtien la clave del menu
	 * @return  Clave del menu
	 */
	public String getClaveMenu() {
		return claveMenu;
	}

	/**
	 * Establece la clave del menu
	 * @param claveMenu Clave del menu
	 */
	public void setClaveMenu(String claveMenu) {
		this.claveMenu = claveMenu;
	}

	/**
	 * Obtiene la posición del menú, que determina el orden en el que aparecera
	 * en el menu
	 * @return Posicion
	 */
	public String getPosicion() {
		return posicion;
	}

	/**
	 * Establece la posición del menú, que determina el orden en el que aparecera
	 * en el menu
	 * @param posicion Posicion
	 */
	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}

	/**
	 * Obtiene el estatus de activo de la relación
	 * @return true activo, false inactivo
	 */
	public String getActivo() {
		return activo;
	}

	/**
	 * Establece el estatus de activo de la relación
	 * @param activo true activo, false inactivo
	 */
	public void setActivo(String activo) {
		this.activo = activo;
	}

	/**
	 * Obtiene la clave de afiliado en caso de que aplique una restricción
	 * para el menu, en el que solo deba aparecer a un cierto usuario
	 * @return Clave del afiliado de la restricción
	 */
	public String getClaveAfiliado() {
		return claveAfiliado;
	}

	/**
	 * Establece la clave de afiliado en caso de que aplique una restricción
	 * para el menu, en el que solo deba aparecer a un cierto usuario
	 * @param claveAfiliado Clave del afiliado de la restricción
	 */
	public void setClaveAfiliado(String claveAfiliado) {
		this.claveAfiliado = claveAfiliado;
	}

	/**
	 * Obtiene la clave de tipo de afiliado en caso de que aplique una restricción
	 * para el menu, en el que solo deba aparecer a un cierto tipo de usuario
	 * @return Clave del tipo de afiliado de la restricción
	 */
	public String getTipoAfiliado() {
		return tipoAfiliado;
	}

	/**
	 * Establece la clave de tipo de afiliado en caso de que aplique una restricción
	 * para el menu, en el que solo deba aparecer a un cierto tipo de usuario
	 * @param tipoAfiliado Clave del tipo de afiliado de la restricción
	 */
	public void setTipoAfiliado(String tipoAfiliado) {
		this.tipoAfiliado = tipoAfiliado;
	}

	/**
	 * Obtiene la clave de afiliado relacionado en caso de que aplique una restricción
	 * para el menu, en el que solo deba aparecer afiliados del afiliado especificado
	 * @return Clave del afiliado relacionado de la restricción
	 */
	public String getClaveAfiliadoRelacionado() {
		return claveAfiliadoRelacionado;
	}

	/**
	 * Establece la clave de afiliado relacionado en caso de que aplique una restricción
	 * para el menu, en el que solo deba aparecer afiliados del afiliado especificado
	 * @param claveAfiliadoRelacionado Clave del afiliado relacionado de la restricción
	 */
	public void setClaveAfiliadoRelacionado(String claveAfiliadoRelacionado) {
		this.claveAfiliadoRelacionado = claveAfiliadoRelacionado;
	}

	/**
	 * Obtiene la clave de tipo de afiliado relacionado en caso de que aplique una restricción
	 * para el menu, en el que solo deba aparecer afiliados del afiliado especificado
	 * @return Clave de tipo de afiliado relacionado de la restricción
	 */
	public String getTipoAfiliadoRelacionado() {
		return tipoAfiliadoRelacionado;
	}

	/**
	 * Establece la clave de tipo de afiliado relacionado en caso de que aplique una restricción
	 * para el menu, en el que solo deba aparecer afiliados del afiliado especificado
	 * @param tipoAfiliadoRelacionado Clave de tipo de afiliado relacionado de la restricción
	 */
	public void setTipoAfiliadoRelacionado(String tipoAfiliadoRelacionado) {
		this.tipoAfiliadoRelacionado = tipoAfiliadoRelacionado;
	}

	/**
	 * Obtiene si el menu es o no un acceso directo
	 * @return true es acceso directo, false de lo contrario
	 */
	public String getAccesoDirecto() {
		return accesoDirecto;
	}

	/**
	 * Establece si el menu es o no un acceso directo
	 * @param accesoDirecto true es acceso directo, false de lo contrario
	 */
	public void setAccesoDirecto(String accesoDirecto) {
		this.accesoDirecto = accesoDirecto;
	}

	public String toString() {
		return 
				"clavePerfil = " + clavePerfil + "|"+
				"claveTipoAfiliado = " + claveTipoAfiliado + "|"+
				"claveMenu = " + claveMenu + "|"+
				"posicion = " + posicion + "|"+
				"activo = " + activo + "|"+
				"claveAfiliado = " + claveAfiliado + "|"+
				"tipoAfiliado = " + tipoAfiliado + "|"+
				"claveAfiliadoRelacionado = " + claveAfiliadoRelacionado + "|"+
				"tipoAfiliadoRelacionado = " + tipoAfiliadoRelacionado + "|"+
				"accesoDirecto = " + accesoDirecto;
	}


	public void setLstAfilRelacionados(List lstAfilRelacionados) {
		this.lstAfilRelacionados = lstAfilRelacionados;
	}


	public List getLstAfilRelacionados() {
		return lstAfilRelacionados;
	}


	public void setTipoRestriccion(String tipoRestriccion) {
		this.tipoRestriccion = tipoRestriccion;
	}


	public String getTipoRestriccion() {
		return tipoRestriccion;
	}

}