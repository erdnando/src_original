package netropology.utilerias;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;

/**
 * Esta clase tiene por finalidad separar los parámetros correspondientes a
 * campos de una forma, de los campos que corresponden a archivos subidos.
 */
public class ParametrosRequest  {
	
	/**
	 * Constructor
	 * @param items Lista de objetos org.apache.commons.fileupload.FileItem
	 * 	que pueden ser tanto archivos como parametros del request
	 */
	public ParametrosRequest(List items) {
		Iterator iter = items.iterator();
		while (iter.hasNext()) {
			FileItem item = (FileItem) iter.next();
		
			if (item.isFormField()) {
				String name = item.getFieldName();
				String value = item.getString();
				if (parametros.containsKey(name)) {
					Object objValor = parametros.get(name);
					if (objValor instanceof List) {
						((List) objValor).add(value);
					} else {	//Era cadena pero ahora pasa a Lista
						List lista = new ArrayList();
						lista.add(objValor);	//Valor existente
						lista.add(value);	//Valor adicional
						parametros.put(name, lista);
					}
				} else {
					parametros.put(name, value);
				}
			} else {
				files.add(item);
			}
		} //fin de los elementos
	}
	
	/**
	 * Obtiene el valor del parametro especificado del request
	 * @param nombre Nombre del parametro a obtener
	 * @return Cadena con el valor
	 */
	public String getParameter(String nombre) {
		Object objValor = parametros.get(nombre);
		String valorParametro = null;
		if (objValor instanceof List) {
			valorParametro = (String)((List)objValor).get(0);
		} else {
			valorParametro = (String)objValor;
		}
		return valorParametro;
	}

	/**
	 * Obtiene el arreglo de valores de los parametros del request
	 * @param nombre nombre del parametro a obtener
	 * @return arreglo con los valores del parametro especificado
	 */
	public String[] getParameterValues(String nombre) {
		Object objValor = parametros.get(nombre);
		String[] arrValorParametro = null;
		if (objValor instanceof List) {
			List l = (List)objValor;
			arrValorParametro = (String[])(l.toArray(new String[l.size()]));
		} else {
			arrValorParametro = new String[] {(String)objValor};
		}
		return arrValorParametro;
	}
	
	/**
	 * Obtiene una lista de objetos org.apache.commons.fileupload.FileItem
	 * que corresponden unicamente a archivos subidos.
	 * @return 
	 */
	public List getFiles() {
		return files;
	}	
	
	private Map parametros = new HashMap();
	private List files = new ArrayList();



}