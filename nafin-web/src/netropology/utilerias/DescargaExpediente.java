package netropology.utilerias;

import com.netro.afiliacion.Afiliacion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import java.net.URLDecoder;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class DescargaExpediente extends HttpServlet  {
	private static final String CONTENT_TYPE = "application/octet-stream";

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);
		String Expediente = "";
		//System.out.println("Hola mundo!!");
		try {
			Expediente = (request.getParameter("ClaveExpediente")!=null)?request.getParameter("ClaveExpediente"):"";
		} catch(Exception e) {
			e.printStackTrace();
		}

		if (Expediente.equals("")){return;}

		String key = "5d75253j4j91j27c582ji69373y90z24";
      ClaseDESBase64 obj = new ClaseDESBase64(key);

		String decoded = URLDecoder.decode(Expediente, "UTF-8");
		 decoded=decoded.replaceAll(" ","+");
		String RFC = obj.desencriptar(decoded);
		//System.err.println(RFC+"\n\n\n\n\n");
		ServletOutputStream out = response.getOutputStream();
		AccesoDB con         = new AccesoDB();

		try{
	Afiliacion BeanAfiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
	//saca el expediente de efile y lo copia a nafin
	BeanAfiliacion.getExpedientePymeEFile(RFC);

	//consultar el expediente en tabla de nafin
	FileOutputStream fos = null;
	File file            = null;
	String pathname      = "";
	PreparedStatement ps = null;
	ResultSet rs         = null;
	String qryDocto      = "";
	String nombreArchivo ="";
  String ContentType = "application/pdf";
  OutputStream os = response.getOutputStream();
  Blob bin = null;
  InputStream inStream = null;

	boolean existearchivo=false;
  int size = 0;
	con.conexionDB();
	qryDocto = 	" SELECT pym.IC_PYME,  efdoc.BI_DOCUMENTO, efdoc.CG_EXTENSION, 'popupexpediente.jsp'  " +
							" FROM COMTMP_DOC_PYME efdoc, comcat_pyme  pym " +
							" WHERE efdoc.IC_PYME = pym.ic_pyme " +
							" and pym.cg_rfc = '" + RFC + "' ";
	System.out.println("qryDocto : "+qryDocto);

	ps = con.queryPrecompilado(qryDocto);
	rs = ps.executeQuery();
	if(rs.next())
	{
    // SE INCORPORA EL LLAMADO A LA BITACORA DE ACCESOS DE EXPEDIENTES DEL EFILE
    CallableStatement cs = con.ejecutaSP("PROC_BIT_ACCESOS (?, ?, ?, ?)");

    cs.setString(1, RFC);
    cs.setInt(2, 2);
    cs.setString(3, null);
    cs.setString(4, "Nafin Electrónico");

    cs.execute();

    cs.close();
    con.terminaTransaccion(true);

		pathname =  getServletContext().getRealPath ("/00tmp/15cadenas/"+"Efile_"+nombreArchivo+ rs.getString("CG_EXTENSION"));
		System.out.println("pathname : "+pathname);
		file = new File(pathname);
		fos = new FileOutputStream(file);
		bin = rs.getBlob("BI_DOCUMENTO");
		inStream = bin.getBinaryStream();
		size = (int)bin.length();
    int length = 0;
    response.setContentType(ContentType);
	  response.setHeader("Content-Disposition", "attachment;filename=ExpedienteDigital.pdf");
    response.setContentLength(size);

    byte b[] = new byte[1048576]; //1MB de buffer
    while ((length = inStream.read(b)) != -1) {
      os.write(b, 0, length);
    }
    os.flush();
    os.close();
    existearchivo = true;

	 String extF = file.getName();
		int point = extF.lastIndexOf(".");
		String extension = (extF.substring(point)).toLowerCase();

	response.setHeader  ( "Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" );
			java.io.FileInputStream fis = null;

			try{
				fis = new java.io.FileInputStream (file);
				byte [  ]  buf = new byte [ 4 * 1024 ] ; // 4K buffer
				int bytesRead;
				while  (  ( bytesRead = fis.read ( buf )  )  != -1 )
					 out.write ( buf, 0, bytesRead );
			}catch( java.io.FileNotFoundException e )   {
				out.println ( "File not found: " + file.getName() );
			}catch( IOException e )   {
				out.println ( "Problem sending file " + file.getName() + ": " + e.getMessage (  )  );
			}finally{
				if  ( fis != null ){
					 fis.close();
				}
				if (out != null){
					out.flush();
					out.close();
				}
			}


	}else{
		response.setContentType("text/xml; charset=UTF-8");
		//PrintWriter out = response.getWriter();
		//Integer.parseInt("hola");
		out.println("<html>");
		out.println("<head>");
		out.println("<title>Error</title>");
		out.println("</head>");
		out.println("<body bgcolor=\"white\">");
		out.println("Error no se pudo encotrar un Expediente asociado");
		out.println("</body>");
		out.println("</html>");


	}



}catch(Throwable e){
	response.setContentType("text/xml; charset=UTF-8");
  con.terminaTransaccion(false);
  e.printStackTrace();

}
finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
}


	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}