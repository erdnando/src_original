package netropology.utilerias;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

//import netropology.utilerias.Comunes;

public interface IQueryGenerator
{
	/**
	 * James Clark
	 * 
	 * Use parameters in this request to constuct a dynamic query 
	 * for certain documents.  The result set will contain only 
	 * Primary Key data that will be used later to page through
	 * data that summarizes the Documents.
	 */
	public abstract String getAggregateCalculationQuery(HttpServletRequest request);
	/**
	 * James Clark
	 * 
	 * In order to reduce the size of the com_documento result set, we need
	 * a query which returns only results that will actually be displayed to the
	 * user.
	 * 
	 * @param ic_estatus_docto query depends on the estatus
	 * @param ids these are the ids of the Documents we need to display
	 * 
	 */
	public abstract String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids);

	/**
	 * James Clark
	 * 
	 * Use parameters in this request to constuct a dynamic query 
	 * for certain documents.  The result set will contain only 
	 * Primary Key data that will be used later to page through
	 * data that summarizes the Documents.
	 */
	public abstract String getDocumentQuery(HttpServletRequest request);

	/**
	 * RJV
	 * Sirve para regresar el query con todos los datos 
	 * de acuerdo a los criterios de b�squeda.
	 * Para generar el archivo a petici�n del 
	 * usuario.
	 */
	
	public abstract String getDocumentQueryFile(HttpServletRequest request);
}
