package netropology.utilerias;

import java.sql.SQLException;

import java.util.Vector;

import org.apache.commons.logging.Log;

/**
 * Clase que permite realizar las validaciones necesarias para
 * determinar si un afiliado tiene habilitado o no un determinado
 * producto.
 * @author Cindy A. Ramos P�rez
 * @since 27/05/2002
 *
 */ 
public class CValidacion {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CValidacion.class);

	boolean	m_bExito = true;
	/*****************************************************************************
	*		void Constructor 1
	*****************************************************************************/
	public CValidacion(){
	}
	/*****************************************************************************
	*		int obtieneEpo()
	*****************************************************************************/
	public int obtieneEpo(int eiCvePyme)
	{

		Vector lovDatos 	= new Vector();
		Vector lovRegistro 	= null;
		AccesoDB	loConexion = new AccesoDB();
		StringBuffer lsbSQL = new StringBuffer();
		int liCvePYME = 0;
		int liNumCols 		= 1;
		int liCveEpo		= -1;

		log.info("obtieneEpo(E)");
		try {
			loConexion.conectarDB();
		} catch (Exception ex) {
//			throw new SeguException(CConst.COT_ERRC_DB);
			log.error("Error al conectarse()", ex);
		}
		try {

			lsbSQL.append("SELECT ic_epo FROM comrel_pyme_epo");
			lsbSQL.append(" WHERE ic_pyme= " + eiCvePyme);

			loConexion.ejecutarQueryDB(lsbSQL.toString(), liNumCols, lovDatos);
			if( lovDatos != null  ){
				if( lovDatos.size() > 1 )
					liCveEpo = 0;
				else {
				  	lovRegistro = (Vector) lovDatos.elementAt(0);
				if( lovRegistro != null)
					liCveEpo = ((Integer) lovRegistro.elementAt(0)).intValue();
				}
			}
		} catch (Exception error) {
			log.error("obtieneEpo(Error)", error);
		} finally {
			loConexion.cerrarDB(m_bExito);
		}
		return liCveEpo;
	}


	/**
	 * Determina si un producto esta disponible para el afiliado (Epo, Pyme o If)
	 * Agregado 27/12/2002		--CARP
	 * 
	 * @param eiCvePYME Clave de la Pyme
	 * @param eiCveEPO Clave de la Epo (En caso de una Pyme, aqui va la epo a la que esta afiliada)
	 * @param eiCveIF Clave del IF
	 * @param esCveSistema Clave del sistema
	 *
	 * @return Lista con ??? Nombre del Sistema ???
	 * 
	 */
	public Vector prodHabilitado(int eiCvePYME, int eiCveEPO, 
			int eiCveIF, String esCveSistema)	{
 
		log.info("prodHabilitado(E)");  
		AccesoDB	loConexion = new AccesoDB();
		StringBuffer lsbSQL  = new StringBuffer();
		Vector lovDatos 	 = new Vector();
		Vector lovRegistro	 = null;
		int liNumCols		 = 1;
		int liValor 		 = 1;
		String lsValor		 = "";
		String lsTipoUsuario = "";
		boolean lbHabilitado = true;
		boolean lbAntic = false;

		if (esCveSistema.equals("NANET")){
			//USUARIO PYME
			if (eiCvePYME != 0 && eiCveEPO != 0){
				lsTipoUsuario = "PYME";
				lsbSQL.append(
						" SELECT decode(cpe.cg_pyme_epo_interno,null,'S','S')  " +
						" FROM comrel_pyme_epo cpe, comrel_producto_epo pe " +
						" WHERE  " +
						" 	cpe.ic_pyme = " + eiCvePYME +
						" 	AND cpe.ic_epo = " + eiCveEPO +
						" 	AND cpe.ic_epo = pe.ic_epo " +
						" 	AND pe.ic_producto_nafin = 1 " +
						" 	AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) ");
			} else
			//USUARIO EPO
			if (eiCveEPO != 0){
				lsTipoUsuario = "EPO";
				lsbSQL.append("SELECT cs_opera_descuento FROM comcat_epo");
				lsbSQL.append(" WHERE ic_epo= " + eiCveEPO);
			} else
			//USUARIO IF
			if (eiCveIF != 0){
				lsTipoUsuario = "IF";
				lsbSQL.append("SELECT count(*) FROM COMCAT_IF");
				lsbSQL.append(" WHERE ic_if = " + eiCveIF);
			} else {
				lsTipoUsuario = "NAFIN";
			}
		} else if (esCveSistema.equals("CDER")){
			if (eiCvePYME != 0 && eiCveEPO != 0){ //USUARIO PYME
				lsTipoUsuario = "PYME";
				lsbSQL.append(
					" SELECT p.cs_cesion_derechos  " +
					" FROM comcat_pyme p " +
					" WHERE  " +
					" 	p.ic_pyme = " + eiCvePYME  + 
					" 	AND EXISTS (SELECT 1 FROM comrel_pyme_epo pe WHERE pe.ic_pyme = p.ic_pyme AND cs_aceptacion IN ('R','H')) ");
			} else if (eiCveEPO != 0){  		//USUARIO EPO
				lsTipoUsuario = "EPO";
				lsbSQL.append(
						" SELECT pe.cs_habilitado " +
						" FROM comrel_producto_epo pe " +
						" WHERE ic_epo = " + eiCveEPO +
						" AND ic_producto_nafin = 9 "); //9 = Cesion de Derechos
			} else if (eiCveIF != 0){ //USUARIO IF
				lsTipoUsuario = "IF";
				lsbSQL.append(
						" SELECT count(*) " +
						" FROM comrel_producto_if " +
						" WHERE ic_if = " + eiCveIF +
						" AND ic_producto_nafin = 9 "); //9 = Cesion de Derechos
			} else {
				lsTipoUsuario = "NAFIN";
			}
		} else if (esCveSistema.equals("ANTIC")) {
			lbAntic = true;
			//USUARIO PYME
			if (eiCvePYME != 0 && eiCveEPO != 0){
				lsTipoUsuario = "PYME";
				lsbSQL.append(
						" SELECT pep.cs_habilitado  " +
						" FROM comrel_pyme_epo_x_producto pep, comrel_producto_epo cpe  " +
						" WHERE pep.ic_producto_nafin = cpe.ic_producto_nafin  " +
						" 	AND pep.ic_epo = cpe.ic_epo  " +
						" 	AND pep.ic_pyme = " + eiCvePYME+
						" 	AND pep.ic_epo = " + eiCveEPO +
						" 	AND pep.ic_producto_nafin = 2  " +
						" 	AND cpe.cs_habilitado = 'S'  " +
						" 	AND cpe.ic_modalidad = 1  " +
						" 	AND EXISTS ( " +
						" 		SELECT lc.ic_linea_credito " +
						" 		FROM com_linea_credito lc " +
						" 		WHERE lc.ic_epo = pep.ic_epo " +
						" 			AND lc.ic_pyme = pep.ic_pyme " +
						" 			AND lc.ic_producto_nafin = pep.ic_producto_nafin " +
						" 			AND lc.df_vencimiento > TRUNC (SYSDATE) " +
						" 	) ");
			} else
			//USUARIO EPO
			if (eiCveEPO != 0){
				lsTipoUsuario = "EPO";
				lsbSQL.append("SELECT cs_habilitado FROM comrel_producto_epo");
				lsbSQL.append(" WHERE ic_producto_nafin = 2");
				lsbSQL.append(" AND ic_modalidad = 1");
				lsbSQL.append(" AND ic_epo= " + eiCveEPO);
			} else
			//USUARIO IF
			if (eiCveIF != 0){
				lsTipoUsuario = "IF";
				lsbSQL.append(
				" SELECT /*+index(iep)*/ count(1)"   +
				"   FROM comrel_producto_epo cpe, comrel_if_epo_x_producto iep"   +
				"  WHERE cpe.ic_epo = iep.ic_epo"   +
				"    AND cpe.ic_producto_nafin = iep.ic_producto_nafin"   +
				"    AND cpe.ic_producto_nafin = 2"   +
				"    AND cpe.ic_modalidad = 1"   +
				"    AND iep.ic_if = "+eiCveIF+
				"    AND iep.cs_habilitado IN ('S',  'N')");
			} else {
				lsTipoUsuario = "NAFIN";
			}
		} else if (esCveSistema.equals("INVEN")) {
			//USUARIO PYME
			if (eiCvePYME != 0 && eiCveEPO != 0){
				lsTipoUsuario = "PYME";
				lsbSQL.append(
						" SELECT pexp.cs_habilitado  " +
						" FROM comrel_pyme_epo_x_producto pexp " +
						" WHERE pexp.ic_producto_nafin = 5 " +
						" 	AND pexp.ic_pyme= " + eiCvePYME +
						" 	AND pexp.ic_epo= " + eiCveEPO  +
						" 	AND EXISTS ( " +
						" 		SELECT lc.ic_linea_credito " +
						" 		FROM com_linea_credito lc " +
						" 		WHERE lc.ic_epo = pexp.ic_epo " +
						" 			AND lc.ic_pyme = pexp.ic_pyme " +
						" 			AND lc.ic_producto_nafin = pexp.ic_producto_nafin " +
						" 			AND lc.df_vencimiento > TRUNC (SYSDATE) " +
						" 	) ");
						
			} else
			//USUARIO EPO
			if (eiCveEPO != 0){
				lsTipoUsuario = "EPO";
				lsbSQL.append("SELECT cs_habilitado FROM comrel_producto_epo");
				lsbSQL.append(" WHERE ic_producto_nafin = 5");
				lsbSQL.append(" AND ic_epo= " + eiCveEPO);
			} else
			//USUARIO EPO
			if (eiCveIF != 0){
				lsTipoUsuario = "IF";
				lsbSQL.append("SELECT count(*) FROM comrel_if_epo_x_producto");
				lsbSQL.append(" WHERE ic_producto_nafin = 5");
				lsbSQL.append(" AND ic_if = " + eiCveIF);
				lsbSQL.append(" AND cs_habilitado in ('S','H')");
			} else {
				lsTipoUsuario = "NAFIN";
			}
		} else if (esCveSistema.equals("FDIST")) {
			//USUARIO PYME
			if (eiCvePYME != 0 && eiCveEPO != 0){
				lsTipoUsuario = "PYME";
				lsbSQL.append("SELECT CS_DISTRIBUIDORES FROM comrel_pyme_epo");
				lsbSQL.append(" WHERE  ic_pyme= " + eiCvePYME);
				lsbSQL.append(" AND ic_epo= " + eiCveEPO);
			} else
			//USUARIO EPO
			if (eiCveEPO != 0){
				lsTipoUsuario = "EPO";
				lsbSQL.append("SELECT cs_habilitado  FROM comrel_producto_epo");
				lsbSQL.append(" WHERE ic_producto_nafin = 4");
				lsbSQL.append(" AND ic_epo= " + eiCveEPO);
			} else
			//USUARIO EPO
			if (eiCveIF != 0){
				lsTipoUsuario = "IF";
				lsbSQL.append("SELECT count(*) FROM comrel_if_epo_x_producto");
				lsbSQL.append(" WHERE ic_producto_nafin = 4");
				lsbSQL.append(" AND ic_if = " + eiCveIF);
				lsbSQL.append(" AND cs_habilitado in ('S','H')");
			} else {
				lsTipoUsuario = "NAFIN";
			}
		} else if (esCveSistema.equals("FOBRA")) {
			//USUARIO PYME
			if (eiCvePYME != 0 && eiCveEPO != 0){
				lsTipoUsuario = "PYME";
				lsbSQL.append("SELECT 'N' FROM DUAL");
			} else
			//USUARIO EPO
			if (eiCveEPO != 0){
				lsTipoUsuario = "EPO";
				lsbSQL.append("SELECT 'N' FROM DUAL");
			} else
			//USUARIO IF
			if (eiCveIF != 0){
				lsTipoUsuario = "IF";					
				lsbSQL.append(
				" SELECT /*+ use_nl(pi,cb) index(pi)*/ "+
				" COUNT (*) "+
				"  FROM  "+
				" comrel_pyme_if   pi, "+
				" comrel_cuenta_bancaria  cb  "+
				" WHERE  "+
				"  cb.ic_cuenta_bancaria  = pi.ic_cuenta_bancaria "+
				"   AND cb.cs_borrado   = 'N' "+
				"   AND pi.cs_borrado   = 'N' "+
				"   AND pi.cs_vobo_if   = 'S' "+
				"   AND pi.cs_opera_descuento  = 'N' "+
				" AND PI.IC_IF = "+eiCveIF);					
			} else {
				lsTipoUsuario = "NAFIN";
			}
		} else if (esCveSistema.equals("PEQUI")) {
			//USUARIO PYME
			if (eiCvePYME != 0) {
				lsTipoUsuario = "PYME";
				lsbSQL.append("SELECT 'S' FROM DUAL");
			} else
			//USUARIO EPO
			if (eiCveEPO != 0) {
				lsTipoUsuario = "EPO";
				lsbSQL.append("SELECT 'N' FROM DUAL");
			} else
			//USUARIO IF
			if (eiCveIF != 0) {
				lsTipoUsuario = "IF";
				lsbSQL.append(
				" select count(*) " +
				" from comrel_producto_if " +
				" where ic_producto_nafin=7 "  +
				" and ic_if= " + eiCveIF);
			} else {
				lsTipoUsuario = "NAFIN";
			}
		} else if (esCveSistema.equals("PRELI")) {
			lbAntic = true;
			//USUARIO PYME
			if (eiCvePYME != 0 && eiCveEPO != 0){
				lsTipoUsuario = "PYME";
				lsbSQL.append(
				" SELECT pep.cs_habilitado"   +
				"   FROM comrel_pyme_epo_x_producto pep, comrel_producto_epo cpe"   +
				"  WHERE pep.ic_producto_nafin = cpe.ic_producto_nafin"   +
				"    AND pep.ic_epo = cpe.ic_epo"   +
				"    AND cpe.cs_habilitado = 'S'"   +
				"    AND cpe.ic_modalidad = 2"   +
				"    AND pep.ic_producto_nafin = 2"   +
				"    AND pep.ic_pyme = "   +eiCvePYME+
				"    AND pep.ic_epo = "+eiCveEPO);
			} else
			//USUARIO EPO
			if (eiCveEPO != 0){
				lsTipoUsuario = "EPO";
				lsbSQL.append("SELECT cs_habilitado FROM comrel_producto_epo");
				lsbSQL.append(" WHERE ic_producto_nafin = 2");
				lsbSQL.append(" AND ic_modalidad = 2");
				lsbSQL.append(" AND ic_epo= " + eiCveEPO);
			} else
			//USUARIO IF
			if (eiCveIF != 0){
				lsTipoUsuario = "IF";
				lsbSQL.append(
				" SELECT /*+index(iep)*/ count(1)"   +
				"   FROM comrel_producto_epo cpe, comrel_if_epo_x_producto iep"   +
				"  WHERE cpe.ic_epo = iep.ic_epo"   +
				"    AND cpe.ic_producto_nafin = iep.ic_producto_nafin"   +
				"    AND cpe.ic_producto_nafin = 2"   +
				"    AND cpe.ic_modalidad = 2"   +
				"    AND iep.ic_if = "+eiCveIF+
				"    AND iep.cs_habilitado IN ('S',  'N')");
			} else {
				lsTipoUsuario = "NAFIN";
				lbHabilitado = false;
			}
		} else if (esCveSistema.equals("CENAF")) {
			if (eiCvePYME == 0 && eiCveEPO == 0 && eiCveIF==0){
				lsTipoUsuario = "NAFIN";
			}
		} else if (esCveSistema.equals("EXPF")) {
			if (eiCvePYME == 0 && eiCveEPO == 0 && eiCveIF==0){
				lsTipoUsuario = "NAFIN";
			}
		}else if (esCveSistema.equals("EXPOR")) {
			if (eiCvePYME == 0 && eiCveEPO == 0 && eiCveIF==0){
				lsTipoUsuario = "NAFIN";
			}
		}else if (esCveSistema.equals("NEMP")) {
			//USUARIO PYME
			if (eiCvePYME != 0 && eiCveEPO != 0){
				lsTipoUsuario = "PYME";
				lsbSQL.append(
				" SELECT DECODE (cg_pyme_epo_interno, NULL, 'N', 'S')"   +
				"   FROM comrel_pyme_epo cpe, comrel_producto_epo pe"   +
				"  WHERE cpe.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = 8 "   +
				"    AND cpe.ic_pyme = "+eiCvePYME+" "   +
				"    AND cpe.ic_epo = "+eiCveEPO+" "   +
				"    AND pe.ic_modalidad = 2");
			} else
			//USUARIO EPO
			if (eiCveEPO != 0){
				lsTipoUsuario = "EPO";
				lsbSQL.append("SELECT 'N' FROM dual");
				//				lsbSQL.append(
				//					" SELECT epo.cs_opera_descuento"   +
				//					"   FROM comcat_epo epo, comrel_producto_epo cpe"   +
				//					"  WHERE epo.ic_epo = cpe.ic_epo"   +
				//					"    AND epo.ic_epo = "+eiCveEPO+" "   +
				//					"    AND ic_producto_nafin = 1"   +
				//					"    AND cpe.ic_modalidad = 2");
			} else
			//USUARIO IF
			if (eiCveIF != 0){
				lsTipoUsuario = "IF";
			
				lsbSQL.append(
				" SELECT count(*) numEposNafEmpr" +
				" FROM comrel_if_epo_x_producto ie " +
				" WHERE  " +
				" 	ie.ic_if = " + eiCveIF +
				" 	AND ie.ic_producto_nafin = 8 " +
				" 	AND ie.cs_habilitado = 'S' ");
			} else {
				lsTipoUsuario = "NAFIN";
			}
		}
        	
 
 		if (esCveSistema.equals("FELEC")) {
			if (eiCvePYME == 0 && eiCveEPO == 0 && eiCveIF==0){
				lsTipoUsuario = "NAFIN";
			}if (eiCvePYME == 0 && eiCveEPO != 0 && eiCveIF==0){
				lsTipoUsuario = "EPO";
				lsbSQL.append("SELECT 'S' FROM dual");
			} if (eiCvePYME != 0 && eiCveEPO != 0 && eiCveIF==0){
				lsTipoUsuario = "PYME";
				lsbSQL.append(
					" SELECT p.CS_FIANZA_ELECTRONICA   " +
					" FROM comcat_pyme p " +
					" WHERE  " +
					" 	p.ic_pyme = " + eiCvePYME  + 
						" 	AND EXISTS (SELECT 1 FROM comrel_pyme_epo pe WHERE pe.ic_pyme = p.ic_pyme  ) ");
					
			}else{
				lsTipoUsuario = "NAFIN";
			}
			
			}
			

		try {
			loConexion.conectarDB();        	
			//La implementacion esta dise�ada de manera que si es pyme o epo
			//la consulta debe regresar una S o una N
			//Si es IF entonces es un count que debe ser mayor que 0 lo que equivale a S
			
			if ( !lsTipoUsuario.equals("NAFIN")){
			
				if ( lsbSQL != null && !lsbSQL.toString().trim().equals("") ) {
					log.debug("prodHabilitado() lsbSQL=" + lsbSQL);
					loConexion.ejecutarQueryDB(lsbSQL.toString(), liNumCols, lovDatos);
				} else {
					lovDatos = null;
				}
				if (lovDatos == null || lovDatos.size() ==0 ) {
					lbHabilitado = false;
					return null;
				}
				lovRegistro = (Vector) lovDatos.elementAt(0);
				
				if (!lsTipoUsuario.equals("IF")) {
					
					
					//PARA VALIDAR SI LA EPO OPERA  FACTORAJE CON RECUERSO
					if ( (lsTipoUsuario.equals("EPO") ||  lsTipoUsuario.equals("PYME") )   && esCveSistema.equals("FDIST") ) {						
						lsbSQL = new StringBuffer();
						lovRegistro	 = null;
						lsbSQL.append("SELECT cg_tipos_credito  FROM comrel_producto_epo");
						lsbSQL.append(" WHERE ic_producto_nafin = 4");
						lsbSQL.append(" AND ic_epo= " + eiCveEPO);
						
						loConexion.ejecutarQueryDB(lsbSQL.toString(), liNumCols, lovDatos);
						lovRegistro = (Vector) lovDatos.elementAt(1);	
						lsValor = (String) lovRegistro.elementAt(0); // F Factoraje con descuento
						
					}else {
						lsValor = (String) lovRegistro.elementAt(0); //S Si N No						
					}
					
				} else {
					liValor = Integer.parseInt(lovRegistro.elementAt(0).toString()); // >0 Si, =0 No
				}
				if ( lsValor.equals("N") ||  lsValor.equals("F") || liValor == 0 ) {
					lbHabilitado = false;
				}
			}
			
			if (lbHabilitado){
				lsbSQL.delete(0,lsbSQL.length());
				lovDatos.clear();
			
				lsbSQL.append(" SELECT d_descripcion, b_bloqueado, nvl(t_imagen,''),");
				lsbSQL.append(" sg_orden_mnu, sg_sw_accion_click, cg_liga,");
				lsbSQL.append(" d_descripcion_ing ");
				lsbSQL.append(" FROM seg_sistemas");
				lsbSQL.append(" WHERE c_sistema = '"+ esCveSistema +"'");
				lsbSQL.append(" ");
				loConexion.ejecutarQueryDB(lsbSQL.toString(), 6, lovDatos);
			
				if (lovDatos == null ) {
					lbHabilitado = false;
					lovRegistro = null;
					return null;
				}
				lovRegistro = (Vector) lovDatos.elementAt(0);
			} else {
				lovRegistro = null;
			}
			
			
		} catch (SQLException errorSQL) {
			if (lbAntic )
			lbHabilitado = false;
			lovRegistro = null;
			log.error("prodHabilitado(Error)", errorSQL);
		} catch (Exception error) {
			if (lbAntic )
			lbHabilitado = false;
			lovRegistro = null;
			log.error("prodHabilitado(Error)", error);
		} finally {
			loConexion.cerrarDB(m_bExito);
		}
		log.info("prodHabilitado(S)");
		return lovRegistro;
	}
	
	
	public String descElecPymeAcceso(int eiCvePYME, int eiCveEPO, 
			String esCveSistema)	{
		AccesoDB	loConexion = new AccesoDB();
		StringBuffer lsbSQL  = new StringBuffer();
		Vector lovDatos 	 = new Vector();
		Vector lovRegistro	 = null;
		String lsValor		 = "";
		String lsTipoUsuario = "";
		boolean lbHabilitado = true;

		try{
		loConexion.conectarDB();
		
			if (esCveSistema.equals("NANET")){
				//USUARIO PYME
				if (eiCvePYME != 0 && eiCveEPO != 0){
					lsTipoUsuario = "PYME";
					lsbSQL.append(
							" SELECT decode(cpe.cg_pyme_epo_interno,null,'N','S')  " +
							" FROM comrel_pyme_epo cpe, comrel_producto_epo pe " +
							" WHERE  " +
							" 	cpe.ic_pyme = " + eiCvePYME +
							" 	AND cpe.ic_epo = " + eiCveEPO +
							" 	AND cpe.ic_epo = pe.ic_epo " +
							" 	AND pe.ic_producto_nafin = 1 " +
							" 	AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) ");
						
					loConexion.ejecutarQueryDB(lsbSQL.toString(), 6, lovDatos);
					if (lovDatos != null ){
						lovRegistro = (Vector) lovDatos.elementAt(0);
						lsValor=(String)lovRegistro.get(0);
						log.debug("Valor para accesar a DescElec>>>>>>>"+lsValor);
					}//cierre if interno
				}//cierre if interno
			}//cierre if principal			
		}catch(Exception e){
			e.printStackTrace();			
		}finally{
			loConexion.cerrarDB(m_bExito);
		}
		return lsValor;
	}//cierre metodo
}