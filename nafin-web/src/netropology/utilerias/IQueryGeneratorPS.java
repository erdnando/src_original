package netropology.utilerias;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

//import netropology.utilerias.Comunes;

public interface IQueryGeneratorPS extends IQueryGenerator
{
	/**
	 * Cora-G's & Huahua
	 * 
	 * Use parameters in this request to constuct a dynamic query 
	 * for certain documents.  The result set will contain only 
	 * Primary Key data that will be used later to page through
	 * data that summarizes the Documents.
	 */
	public abstract ArrayList getConditions(HttpServletRequest request);	
	public abstract int getNumList(HttpServletRequest request);
}
