package netropology.utilerias;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;

public class MenuPerfilDAO  {

	private final static Log log = ServiceLocator.getInstance().getLog(MenuPerfilDAO.class);
	
	public MenuPerfilDAO() {
	}
	
	
	/**
	 * Realiza la asociaci�n de Menu - Perfil
	 * @param menusPerfil Lista de beans MenuPerfil con la informaci�n de las 
	 * asociaciones a realizar
	 */
	public static void asociarMenuXPerfil(List menusPerfil) {
		log.info("asociarMenuXPerfil(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;

		//**********************************Validacion de parametros:*****************************
		try {
			if (menusPerfil == null || menusPerfil.size() == 0) {
			/*menuPerfil.getClaveMenu() == null || 
					menuPerfil.getClaveTipoAfiliado() == null ||
					menuPerfil.getClavePerfil() == null) {*/
				throw new Exception("Los parametros/atributos no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros/atributos recibidos." + 
					e.getMessage() + "\n" +	menusPerfil);
			throw new AppException("Error en los parametros/atributos recibidos.", e);
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			String strSQL = 
					" INSERT INTO comrel_menu_afiliado_x_perfil (ic_tipo_afiliado, " +
					" cc_perfil, cc_menu, ig_posicion) " +
					" VALUES (?,?,?,?) ";

			String strSQLMenuPadre = 
					" SELECT cc_menu_padre " +
					" FROM comcat_menu_afiliado " +
					" WHERE ic_tipo_afiliado = ? " +
					" AND cc_menu = ? ";
			
			String strSQLMenuOrden = 
					" SELECT NVL (MAX (ig_posicion), 0) + 1 AS orden" +
					" FROM comcat_menu_afiliado ma, comrel_menu_afiliado_x_perfil maxp " +
					" WHERE ma.cc_menu = maxp.cc_menu " +
					" AND ma.ic_tipo_afiliado = maxp.ic_tipo_afiliado " +
					" AND maxp.cc_perfil = ? " +
					" AND ma.cc_menu_padre  = ? ";

			String strSQLMenuRaizOrden = 
					" SELECT NVL (MAX (ig_posicion), 0) + 1 As orden" +
					" FROM comcat_menu_afiliado ma, comrel_menu_afiliado_x_perfil maxp " +
					" WHERE ma.cc_menu = maxp.cc_menu " +
					" AND ma.ic_tipo_afiliado = maxp.ic_tipo_afiliado " +
					" AND maxp.cc_perfil = ? " +
					" AND ma.cc_menu_padre IS NULL ";
			
			PreparedStatement psInsert = con.queryPrecompilado(strSQL);
			PreparedStatement psMenuPadre = con.queryPrecompilado(strSQLMenuPadre);
			PreparedStatement psMenuOrden = con.queryPrecompilado(strSQLMenuOrden);
			PreparedStatement psMenuRaizOrden = con.queryPrecompilado(strSQLMenuRaizOrden);
			
			int orden = 1;
			Iterator it = menusPerfil.iterator();
			while(it.hasNext()){
				psInsert.clearParameters();
				psMenuPadre.clearParameters();
				psMenuOrden.clearParameters();
				psMenuRaizOrden.clearParameters();
				
				MenuPerfil menuPerfil = (MenuPerfil)it.next();

				psMenuPadre.setInt(1, Integer.parseInt(menuPerfil.getClaveTipoAfiliado()));
				psMenuPadre.setString(2, menuPerfil.getClaveMenu());
				
				ResultSet rs = psMenuPadre.executeQuery();
				rs.next();
				String menuPadre = rs.getString("cc_menu_padre");
				rs.close();
				
				if (menuPadre == null || menuPadre.equals("")) {
					psMenuRaizOrden.setString(1, menuPerfil.getClavePerfil());
					rs = psMenuRaizOrden.executeQuery();
					rs.next();
					orden = rs.getInt("orden");
					rs.close();
				} else {
					psMenuOrden.setString(1, menuPerfil.getClavePerfil());
					psMenuOrden.setString(2, menuPadre);
					rs = psMenuOrden.executeQuery();
					rs.next();
					orden = rs.getInt("orden");
					rs.close();
				}
				
				psInsert.setInt(1, Integer.parseInt(menuPerfil.getClaveTipoAfiliado()));
				psInsert.setString(2, menuPerfil.getClavePerfil());
				psInsert.setString(3, menuPerfil.getClaveMenu());
				psInsert.setInt(4, orden);
				psInsert.executeUpdate();
			}
			psInsert.close();
			psMenuPadre.close();
			psMenuOrden.close();
			psMenuRaizOrden.close();
			
			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			throw new AppException("Error al insertar el registro", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("asociarMenuXPerfil(S)");
		}
	}
	
	
	/**
	 * Realiza la actualizaci�n de una menuPerfil de men�
	 * @param menuPerfil Bean con la informaci�n de la opci�n de menu
	 */
	public static void actualizar(MenuPerfil menuPerfil) {
		log.info("actualizar(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;

		//**********************************Validacion de parametros:*****************************
		try {
			if (menuPerfil == null || menuPerfil.getClaveMenu() == null || 
					menuPerfil.getClaveTipoAfiliado() == null ||
					menuPerfil.getClavePerfil() == null ) {
				throw new Exception("Los parametros/atributos no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros/atributos recibidos." + 
					e.getMessage() + "\n" +	menuPerfil);
			throw new AppException("Error en los parametros/atributos recibidos.", e);
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			String strSQL = 
					" UPDATE comrel_menu_afiliado_x_perfil " +
					" SET cs_acceso_directo = ?, " +
					" cs_activo = ? " +
					//" ic_clave_afiliado = ?, " +
					//" cg_tipo_afiliado = ?, " +
					//" ic_clave_afiliado_rel = ?, " +
					//" cg_tipo_afiliado_rel = ? " +
					" WHERE cc_menu = ? " +
					" AND ic_tipo_afiliado = ? " +
					" AND cc_perfil = ? ";
			List params = new ArrayList();

			params.add(menuPerfil.getAccesoDirecto());
			params.add(menuPerfil.getActivo());
			//params.add(menuPerfil.getClaveAfiliado());
			//params.add(menuPerfil.getTipoAfiliado());
			//params.add(menuPerfil.getClaveAfiliadoRelacionado());
			//params.add(menuPerfil.getTipoAfiliadoRelacionado());
			
			params.add(menuPerfil.getClaveMenu());
			params.add(menuPerfil.getClaveTipoAfiliado());
			params.add(menuPerfil.getClavePerfil());

			con.ejecutaUpdateDB(strSQL, params);
			
			
			/*
			strSQL = "DELETE com_menu_afiliado_restriccion " +
						"  WHERE cc_menu = ?  " +
						"    AND ic_tipo_afiliado = ?  " +
						"    AND cc_perfil = ? ";
			
			params = new ArrayList();
			params.add(menuPerfil.getClaveMenu());
			params.add(menuPerfil.getClaveTipoAfiliado());
			params.add(menuPerfil.getClavePerfil());
			con.ejecutaUpdateDB(strSQL, params);

			strSQL = "INSERT INTO com_menu_afiliado_restriccion " +
				"            (ic_menu_afiliado_restriccion, ic_tipo_afiliado, cc_perfil, " +
				"             cc_menu, ic_nafin_electronico, cg_tipo_restriccion " +
				"            ) " +
				"     VALUES ((SELECT MAX(NVL (ic_menu_afiliado_restriccion, 0)) + 1 " +
				"                FROM com_menu_afiliado_restriccion), ?, ?, " +
				"             ?, ?, ? " +
				"            ) ";


			if(menuPerfil.getClaveAfiliado()!=null && !"".equals(menuPerfil.getClaveAfiliado())){
				String claveAfil [] = (menuPerfil.getClaveAfiliado()).split(",");
				for(int x=0; x<claveAfil.length; x++){
						params = new ArrayList();
						params.add(menuPerfil.getClaveTipoAfiliado());
						params.add(menuPerfil.getClavePerfil());
						params.add(menuPerfil.getClaveMenu());
						params.add(claveAfil[x]);
						params.add("D");
						con.ejecutaUpdateDB(strSQL, params);
				}
			}
			
			if(menuPerfil.getClaveAfiliadoRelacionado()!=null && !"".equals(menuPerfil.getClaveAfiliadoRelacionado())){
				String claveAfil [] = (menuPerfil.getClaveAfiliadoRelacionado()).split(",");
				for(int x=0; x<claveAfil.length; x++){
						params = new ArrayList();
						params.add(menuPerfil.getClaveTipoAfiliado());
						params.add(menuPerfil.getClavePerfil());
						params.add(menuPerfil.getClaveMenu());
						params.add(claveAfil[x]);
						params.add("R");
						con.ejecutaUpdateDB(strSQL, params);
				}
			}
			
			*/
			
			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			throw new AppException("Error al actualizar el registro", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("actualizar(S)");
		}
	}
	
	/**
	 * Realiza la actualizaci�n de una menuPerfil de men�
	 * @param menuPerfil Bean con la informaci�n de la opci�n de menu
	 */
	public static void asignaAfiliadosRestringidos(MenuPerfil menuPerfil) {
		log.info("asignaAfiliadosRestringidos(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;

		//**********************************Validacion de parametros:*****************************
		try {
			if (menuPerfil == null || menuPerfil.getClaveMenu() == null || 
					menuPerfil.getClaveTipoAfiliado() == null ||
					menuPerfil.getClavePerfil() == null ) {
				throw new Exception("Los parametros/atributos no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros/atributos recibidos." + 
					e.getMessage() + "\n" +	menuPerfil);
			throw new AppException("Error en los parametros/atributos recibidos.", e);
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			String strSQL = "";
			List params = new ArrayList();

			


			List lstAfiliados = menuPerfil.getLstAfilRelacionados();
			
			if(lstAfiliados!=null && lstAfiliados.size()>0){
				for(int x=0; x<lstAfiliados.size(); x++){
					HashMap mp = (HashMap)lstAfiliados.get(x);
					if(x==0){
						strSQL = "DELETE com_menu_afiliado_restriccion " +
						"  WHERE cc_menu = ?  " +
						"    AND ic_tipo_afiliado = ?  " +
						"    AND cc_perfil = ? " +
						"	  AND cg_tipo_restriccion = ? ";
			
						params = new ArrayList();
						params.add(menuPerfil.getClaveMenu());
						params.add(menuPerfil.getClaveTipoAfiliado());
						params.add(menuPerfil.getClavePerfil());
						params.add((String)mp.get("TPRESTRICCION"));
						con.ejecutaUpdateDB(strSQL, params);
			
						strSQL = "INSERT INTO com_menu_afiliado_restriccion " +
							"            (ic_menu_afiliado_restriccion, ic_tipo_afiliado, cc_perfil, " +
							"             cc_menu, ic_nafin_electronico, cs_excluir, cg_tipo_restriccion  " +
							"            ) " +
							"     VALUES ((SELECT MAX(NVL (ic_menu_afiliado_restriccion, 0)) + 1 " +
							"                FROM com_menu_afiliado_restriccion), ?, ?, " +
							"             ?, ?, ?, ? " +
							"            ) ";
					}
					
					
					
					
					if(lstAfiliados.size()>0){
					
						params = new ArrayList();
						params.add(menuPerfil.getClaveTipoAfiliado());
						params.add(menuPerfil.getClavePerfil());
						params.add(menuPerfil.getClaveMenu());
						params.add((String)mp.get("NAFELECAFIL"));
						params.add((String)mp.get("CSEXCLUIR"));
						params.add((String)mp.get("TPRESTRICCION"));
						con.ejecutaUpdateDB(strSQL, params);
					}
				}
			}else{
				strSQL = "DELETE com_menu_afiliado_restriccion " +
						"  WHERE cc_menu = ?  " +
						"    AND ic_tipo_afiliado = ?  " +
						"    AND cc_perfil = ? " +
						"	  AND cg_tipo_restriccion = ? ";
			
						params = new ArrayList();
						params.add(menuPerfil.getClaveMenu());
						params.add(menuPerfil.getClaveTipoAfiliado());
						params.add(menuPerfil.getClavePerfil());
						params.add(menuPerfil.getTipoRestriccion());
						con.ejecutaUpdateDB(strSQL, params);
			}
			
			/*
			if(menuPerfil.getClaveAfiliado()!=null && !"".equals(menuPerfil.getClaveAfiliado())){
				String claveAfil [] = (menuPerfil.getClaveAfiliado()).split(",");
				for(int x=0; x<claveAfil.length; x++){
						params = new ArrayList();
						params.add(menuPerfil.getClaveTipoAfiliado());
						params.add(menuPerfil.getClavePerfil());
						params.add(menuPerfil.getClaveMenu());
						params.add(claveAfil[x]);
						params.add("D");
						con.ejecutaUpdateDB(strSQL, params);
				}
			}
			
			if(menuPerfil.getClaveAfiliadoRelacionado()!=null && !"".equals(menuPerfil.getClaveAfiliadoRelacionado())){
				String claveAfil [] = (menuPerfil.getClaveAfiliadoRelacionado()).split(",");
				for(int x=0; x<claveAfil.length; x++){
						params = new ArrayList();
						params.add(menuPerfil.getClaveTipoAfiliado());
						params.add(menuPerfil.getClavePerfil());
						params.add(menuPerfil.getClaveMenu());
						params.add(claveAfil[x]);
						params.add("R");
						con.ejecutaUpdateDB(strSQL, params);
				}
			}
			*/
			
			
			
			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			throw new AppException("Error al actualizar el registro", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("asignaAfiliadosRestringidos(S)");
		}
	}
	
	
	/**
	 * Realiza la eliminaci�n de la relaci�n de menu x perfil
	 * @param menuPerfiles Lista de beans (MenuPerfil) de las relaciones a eliminar
	 */
	public static void eliminar(List menuPerfiles) {
		log.info("eliminar(E)");
		
		//**********************************Validacion de parametros:*****************************
		try {
			if (menuPerfiles == null || menuPerfiles.size() == 0) {
				throw new Exception("Los parametros no pueden ser vacios");
			}
		} catch(Exception e) {
			log.error("Error en los parametros/atributos recibidos." + 
					e.getMessage() + "\n" +	menuPerfiles);
			throw new AppException("Error en los parametros recibidos.", e);
		}
		//***************************************************************************************

		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		try {
			con.conexionDB();
			Iterator it = menuPerfiles.iterator();
			int claveTipoAfiliado = 0;
			String clavePerfil = "";
			List clavesMenu = new ArrayList();
			int i = 0;
			while (it.hasNext()) {
				MenuPerfil menuPerfil = (MenuPerfil)it.next();
				if (i == 0) {
					//la clave del tipo de afiliado ser�a la misma para todos debido a la estructura de la pantalla.
					//por ello solo se toma un valor.
					claveTipoAfiliado = Integer.parseInt(menuPerfil.getClaveTipoAfiliado());
					clavePerfil = menuPerfil.getClavePerfil();
				}
				clavesMenu.add(menuPerfil.getClaveMenu());
				i++;
			}
			
			String strSQL = 
					" DELETE FROM comrel_menu_afiliado_x_perfil " +
					" WHERE ic_tipo_afiliado = ? " +
					" AND cc_perfil = ? " +
					" AND cc_menu IN (" + Comunes.repetirCadenaConSeparador("?", ",", clavesMenu.size()) + ")";
			
			List params = new ArrayList();
			params.add(new Integer(claveTipoAfiliado));
			params.add(clavePerfil);
			params.addAll(clavesMenu);
			
			con.ejecutaUpdateDB(strSQL, params);
			
			bOk = true;
		} catch(Throwable t) {
			bOk = false;
			throw new AppException("Error al eliminar la relaci�n", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Realiza la validaci�n de la informaci�n seg�n corresponda a la accion
	 * especificada
	 * @param accion Accion a validar: actualizar
	 * @param menuPerfil Objeto que contiene la informaci�n de la opci�n del men�
	 * 
	 * @return Mapa con los errores presentados de la validaci�n o mapa vac�o de lo contrario
	 */
	public static Map validar(String accion, MenuPerfil menuPerfil) {
		log.info("validar(E)");
		Map errores = new HashMap();

		//**********************************Validacion de parametros:*****************************
		try {
			if (accion == null || accion.equals("") || menuPerfil == null) {
				throw new Exception("Los parametros no pueden ser vacios");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." + 
					e.getMessage() + "\n" +	
					"accion=" + accion + "\n" +
					"menuPerfil=" + menuPerfil + "\n"
					);
			throw new AppException("Error en los parametros recibidos.", e);
		}
		//***************************************************************************************
		
		try {
			if (accion.equals("actualizar")) {
				if (menuPerfil.getClaveAfiliado() != null && !menuPerfil.getClaveAfiliado().equals("") &&
						(menuPerfil.getTipoAfiliado() == null || menuPerfil.getTipoAfiliado().equals("")) ) {
					errores.put("cg_tipo_afiliado", "El tipo es requerido si especifica una clave de afiliado");
				}
				if (menuPerfil.getClaveAfiliadoRelacionado() != null && !menuPerfil.getClaveAfiliadoRelacionado().equals("") &&
						(menuPerfil.getTipoAfiliadoRelacionado() == null || menuPerfil.getTipoAfiliadoRelacionado().equals("")) ) {
					errores.put("cg_tipo_afiliado_rel", "El tipo es requerido si especifica una clave de afiliado relacionado");
				}
				if (menuPerfil.getTipoAfiliado() != null && !menuPerfil.getTipoAfiliado().equals("") &&
						(menuPerfil.getClaveAfiliado() == null || menuPerfil.getClaveAfiliado().equals("")) ) {
					errores.put("ic_clave_afiliado", "La clave es requerida si especifica el tipo de afiliado");
				}
				if (menuPerfil.getTipoAfiliadoRelacionado() != null && !menuPerfil.getTipoAfiliadoRelacionado().equals("") &&
						(menuPerfil.getClaveAfiliadoRelacionado() == null || menuPerfil.getClaveAfiliadoRelacionado().equals("")) ) {
					errores.put("ic_clave_afiliado_rel", "La clave es requerida si especifica el tipo de afiliado relacionado");
				}

			} else {
				throw new AppException("Accion no implementada");
			}
			return errores;
		} catch(Exception e) {
			throw new AppException("Error al validar la informaci�n", e);
		} finally {
			log.info("validar(S)");
		}
	}

}