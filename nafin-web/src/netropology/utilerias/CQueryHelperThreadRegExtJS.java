package netropology.utilerias;

import com.netro.threads.ThreadCreateFiles;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;


/**
 * La clase tiene por finalidad realizar la paginaci�n de informaci�n
 * utilizando variables bind y el objeto Registros para poder accesar
 * a la informaci�n.
 * Tiene mejoras en cuanto a la implementaci�n de las paginaciones
 * ya que contempla el manejo de llaves primarias compuestas
 * @author Gilberto Aparicio
 */
 
public class CQueryHelperThreadRegExtJS {
        /**
         * Variable que se emplea para enviar mensajes al log.
         */
        private final static Log log = ServiceLocator.getInstance().getLog(CQueryHelperThreadRegExtJS.class);
	
	private IQueryGeneratorThreadRegExtJS qryGen;
	private String qrySentencia;
	private Registros registros;
	private Registros newids;
	private Registros totales;

	private String paginadorIds = "_cqhregextjs_ids";
	private String paginadorTotales = "_cqhregextjs_totales";
        
        private int countReg;
        private int totalCountReg;
        private String nombreArchivo;
	
	/**
	 * Establece el maximo numero de registros que regresar� la consulta
	 * De manera predeterminada se establece en 1000
	 */
	private int maximoNumeroRegistros = 1000;

	/**
	 * Id para identificar de manera unica la ejecuci�n de un query y poder
	 * rastrear donde inicia y donde termina.
	 * Util para cuando hay varios queries de paginador ejecutandose
	 * simultaneamente.
	 */
	private final String idQuery = String.valueOf((new java.util.Date()).getTime());

	public CQueryHelperThreadRegExtJS(){}
	
	public CQueryHelperThreadRegExtJS(IQueryGeneratorThreadRegExtJS qryGen){
		this.qryGen = qryGen;
	}

	/**
	 * Obtiene la informacion a mostrar en la paginacion.
	 * IMPORTANTE!!!!: Es necesario que los alias de las columnas en la consulta
	 * no se repitan, ya que de lo contrario el objeto resultante eliminar� los
	 * campos repetidos
	 *
	 * @param request maintains all State for this static invocation
	 * @param con Objeto de conexion, ya debe estar establecida la conexion
	 * @param start Indica el indice del registro a partir del que se obtendra 
	 * 		la informacion
	 * @param limit Especifica el numero maximo de registros que se regresaran
	 * @return Cadena JSON con los registros correspondientes
	 */
	public Registros getPageResultSet(
						HttpServletRequest request,
						int 				start,
						int 				limit){
		AccesoDB con  = new AccesoDB();
		Registros registros = new Registros();

		//------------------- Validacion de parametros -------------------------
		try {
			if (start < 0) {
				throw new Exception("El parametro start debe ser mayor a 0");
			}
			if (limit <1) {
				throw new Exception("El parametro limit debe ser mayor a 1");
			}
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		//----------------------------------------------------------------------

		try {
			con.conexionDB();
			Registros regIds = this.getNextSetOfIds(request, start, limit);
			if (regIds.getData() != null) {
				registros = con.consultarDB(
								this.qryGen.getDocumentSummaryQueryForIds(this.getNextSetOfIds(request, start, limit).getData()), 
								this.qryGen.getConditions(), 
								true);
			}
		} catch(Exception e) {
			throw new AppException("Error al obtener los registros de la paginacion", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //Para consultas a tablas remotas a traves de dblink
				con.cierraConexionDB();
			}
		}
		
		return registros;
	}

	/**
	 * Obtiene una lista con los totales de la paginacion en un objeto
	 * de tipo Registros.
	 * @param request Objeto HttpServletRequest de donde se saca la sesion
	 * @return objeto Registros con los datos de los totales
	 */
	public Registros getResultCount(HttpServletRequest request){
		if("S".equals( (String)request.getSession().getAttribute("_resumenTotales") )) {//Si no se han generado totales
			HttpSession session = request.getSession();
			this.totales = new Registros();
			this.cleanSession(request);
	
			AccesoDB con = new AccesoDB();
			
			log.info("CQueryHelperRegExtJS.getResultCount():: pag= " + this.qryGen.getClass().getName() + " (" + this.idQuery + ")" );
			
			try {
				con.conexionDB();
				qrySentencia = (this.qryGen.getAggregateCalculationQuery()).trim();//TOTALES
				if (qrySentencia != null && !"".equals(qrySentencia)) {
					long tiempoInicial = (new java.util.Date()).getTime();
					totales = con.consultarDB(
								this.qryGen.getAggregateCalculationQuery(), 	
								this.qryGen.getConditions());
					long tiempoFinal = (new java.util.Date()).getTime();
					log.info("CQueryHelperRegExtJS.executePKQuery():: Totales t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
	
					session.setAttribute(this.paginadorTotales, totales);
				}
			} catch (Exception e) {
				throw new AppException("Error al generar la consulta", e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.terminaTransaccion(true); //por las consultas que usen tablas remotas via dblink
					con.cierraConexionDB();
				}
			}
		}
		return (Registros)request.getSession().getAttribute(this.paginadorTotales);
	}

	/**
	 * Obtiene la informacion a mostrar en la paginacion en formato JSON.
	 * IMPORTANTE!!!!: Es necesario que los alias de las columnas en la consulta
	 * no se repitan, ya que de lo contrario el objeto resultante eliminar� los
	 * campos repetidos
	 *
	 * @param request maintains all State for this static invocation
	 * @param con Objeto de conexion, ya debe estar establecida la conexion
	 * @param start Indica el indice del registro a partir del que se obtendra 
	 * 		la informacion
	 * @param limit Especifica el numero maximo de registros que se regresaran
	 * @return Cadena JSON con los registros correspondientes
	 */
	public String getJSONPageResultSet(
			HttpServletRequest request,
			int start,
			int limit) {
		
		Registros registros = this.getPageResultSet(request, start, limit);
		
		return "{\"success\": true, \"total\": \"" + 
				this.getIdsSize() + "\", \"registros\": " + registros.getJSONData()+"}";

	}//getPageResultSet
	

	/**
	 * Obtiene una lista con los totales de la paginacion en formato JSON.
	 * 
	 */
	public String getJSONResultCount(HttpServletRequest request){
		Registros regTotales =  (Registros)request.getSession().getAttribute(this.paginadorTotales);
		
		return "{\"success\": true, \"total\": \"" + 
		regTotales.getNumeroRegistros() + "\", \"registros\": " + regTotales.getJSONData()+"}";
	}

	/**
	 * Obtiene las llaves primarias de la informaci�n a consultar con base
	 * en los parametros (criterios) de busqueda especificados
	 *
	 * @param request Objeto HttpServletRequest, de donde se obtienen los
	 * 		parametros de busqueda
	 */
	public void executePKQuery(HttpServletRequest request) {
		HttpSession session = request.getSession();
		this.newids = new Registros();
		this.totales = new Registros();
		this.cleanSession(request);

		AccesoDB con = new AccesoDB();
		
		log.info("CQueryHelperRegExtJS.executePKQuery():: pag= " + this.qryGen.getClass().getName() + " (" + this.idQuery + ")" );
		
		try {
			con.conexionDB();
			long tiempoInicial = (new java.util.Date()).getTime();
			newids = con.consultarDB(
						this.qryGen.getDocumentQuery(), 	//QUERY DE LLAVES
						this.qryGen.getConditions(),		//VARIABLES BIND
						true,						//NO HACE DISTINCION DE TIPOS DE DATOS EN EL RESULTADO
						this.getMaximoNumeroRegistros());						//NUMERO MAXIMO DE REGISTROS A PRESENTAR EN LA CONSULTA
			long tiempoFinal = (new java.util.Date()).getTime();
			log.info("CQueryHelperRegExtJS.executePKQuery():: PKs. t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );

			session.setAttribute(this.paginadorIds, newids);
			if(newids.getNumeroRegistros()>0) {
				qrySentencia = (this.qryGen.getAggregateCalculationQuery()).trim();//TOTALES
				if (qrySentencia != null && !"".equals(qrySentencia)) {
					tiempoInicial = (new java.util.Date()).getTime();
					totales = con.consultarDB(
								this.qryGen.getAggregateCalculationQuery(), 	
								this.qryGen.getConditions());
					tiempoFinal = (new java.util.Date()).getTime();
					log.info("CQueryHelperRegExtJS.executePKQuery():: Totales t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );

					session.setAttribute(this.paginadorTotales, totales);
				}//if (qrySentencia != null && !"".equals(qrySentencia) )
			}//if(registros.getNumeroRegistros()>0)
		} catch (Exception e) {
			throw new AppException("Error al generar la consulta", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //por las consultas que usen tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}//executePKQuery
	
	//Regresa el numero de ids
	public int getIdsSize() {
		if(this.newids!=null)
			return this.newids.getNumeroRegistros();
		else 
			return 0;
	}//getIdsSize
	
	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @param request Objeto HttpServletRequest
	 * @param start Indice a partir del cual se toman los registros
	 * @param limit Numero de registros a obtener
	 * @return objeto Registros con los Ids en turno para la paginacion
	 */
	private Registros getNextSetOfIds(HttpServletRequest request,
					int 				start,
					int 				limit) {
		this.newids = (Registros)request.getSession().getAttribute(this.paginadorIds);
		
		int indiceMaximo = this.getIdsSize();
		if(this.newids.getNumeroRegistros()>start+limit) {
			indiceMaximo = start + limit;
		}
		registros = new Registros();
		for(int i=start;i<indiceMaximo;i++) {
			registros.addDataRow(newids.getData(i));
		}
		return registros;
	}
	
	/**
	 * Metodo para generar de manera eficiente un archivo.
	 * Los encabezados se obtienen de los encabezados de la consulta, los
	 * guiones bajos los remplaza por espacios.
	 * @param request HttpRequest de donde se obtienen los criterios de busqueda
	 * @path Ruta del archivo a generar
	 * @separador Separador de Campos
	 * @extension Extension del archivo generado
	 * @encabezado Determina si lleva encabezado (true) o no (false)
	 * @return Cadena con el nombre del archivo generado
	 */
	public String getCreateFile(HttpServletRequest request, String path, String separador, String extension, boolean encabezado) {
		CreaArchivo 		archivo 			= new CreaArchivo();
		StringBuffer		contenidoArchivo	= new StringBuffer();
		StringBuffer		contenidoEncabezado	= new StringBuffer();
		PreparedStatement 	ps					= null;
		ResultSet			rs					= null;
		ResultSetMetaData	rsMD				= null;
		OutputStreamWriter 			writer 				= null;
		BufferedWriter 		buffer 				= null;
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			log.info("CQueryHelperRegExtJS.getCreateFile():: pag= " + this.qryGen.getClass().getName() + " (" + this.idQuery + ")" );
			ps = con.queryPrecompilado(this.qryGen.getDocumentQueryFile(), this.qryGen.getConditions());
			long tiempoInicial = (new java.util.Date()).getTime();
			rs = ps.executeQuery();
			long tiempoFinal = (new java.util.Date()).getTime();
			log.info("CQueryHelperRegExtJS.getCreateFile():: t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
			
			ps.clearParameters();
			rsMD = rs.getMetaData();
			int 		numCols 	= rsMD.getColumnCount();
			writer = new OutputStreamWriter(new FileOutputStream(path+archivo.nombreArchivo()+"."+extension,true), "ISO-8859-1");
			buffer = new BufferedWriter(writer);
			while(rs.next()) {
				for(int i=1;i<=numCols;i++) {
					if(encabezado) {
						for(int e=1;e<=numCols;e++) {
							String nombre = rsMD.getColumnName(e).replace('_', ' ').toUpperCase();
							buffer.write(nombre+((e==numCols)?"\r\n":separador));
						}
						encabezado = false;
					}
					String rs_campo = rs.getString(i)==null?"":rs.getString(i).replace(',', ' ').trim();
					buffer.write(rs_campo+((i==numCols)?"\r\n":separador));
				}//for(int i=1;i<=numCols;i++)
			}//while(rs.next())
			buffer.close();
			return archivo.nombre+"."+extension;
		} catch(Exception e) {
			throw new AppException("CQueryHelperRegExtJS::getCreateFile(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				try {
					if (rs != null) {
						rs.close();
					}
					if (ps!=null) {
						ps.close();
					}
				} catch(Exception e) {}
				con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}
	
	public String getCreateFile(HttpServletRequest request, String path, String separador, String extension) {
		return getCreateFile(request, path, ",", "csv",true);
	}
	
	public String getCreateFile(HttpServletRequest request, String path) {
		return getCreateFile(request, path, ",", "csv");
	}


	/**
	 * Metodo para generar un archivo.
	 * @param request HttpRequest de donde se obtienen los criterios de busqueda
	 * @path Ruta del archivo a generar
	 * @tipo tipo de archivo a generar
	 * @return Cadena con el nombre del archivo generado
	 */
	public String getCreateCustomFile(
			HttpServletRequest request, String path, String tipo) {
		PreparedStatement 	ps					= null;
		ResultSet	        rs					= null;
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			log.info("CQueryHelperRegExtJS.getCreateCustomFile():: pag= " + this.qryGen.getClass().getName() + " (" + this.idQuery + ")" );
                    
			ps = con.queryPrecompilado(this.qryGen.getDocumentQueryFile(), this.qryGen.getConditions());
			long tiempoInicial = (new java.util.Date()).getTime();
			rs = ps.executeQuery();
			long tiempoFinal = (new java.util.Date()).getTime();
			log.info("CQueryHelperRegExtJS.getCreateCustomFile():: t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );

			return this.qryGen.crearCustomFile(request, rs, path, tipo);
			
		} catch(Exception e) {
			throw new AppException("CQueryHelperRegExtJS::getCreateCustomFile(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				try {
					if (rs != null) {
						rs.close();
					}
					if (ps!=null) {
						ps.close();
					}
				} catch(Exception e) {}
				con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}
	

	/**
	 * Metodo para generar un archivo con base en los resultados mostrados por p�gina.
	 * @param request HttpRequest de donde se obtienen los criterios de busqueda
	 * @param start Indica el indice del registro a partir del que se obtendra 
	 * 		la informacion
	 * @param limit Especifica el numero maximo de registros que se regresaran
	 * @param path Ruta del archivo a generar
	 * @param tipo tipo de archivo a generar
	 * @return Cadena con el nombre del archivo generado
	 */
	public String getCreatePageCustomFile(
			HttpServletRequest request, int start, int limit, String path, String tipo) {
		try {
			Registros reg = this.getPageResultSet(request, start,limit);
			return this.qryGen.crearPageCustomFile(request, reg, path, tipo);
		} catch(Exception e) {
			throw new AppException("CQueryHelperRegExtJS::getCreatePageCustomFile(Exception) ", e);
		} finally {
		
		}
	}


	/**
	 * Metodo para manejar consultas que no requieren paginacion por ser POCOS registros.
	 * Para mandar a llamar este m�todo, debe existir en el paginador
	 * el query en getDocumentQueryFile().

	 * ATENCI�N: Este m�todo estar� limitado en cuanto a la cantidad de registros
	 * que regresar�, para evitar problemas de memoria. El numero de registros 
	 * maximos a regresar esta definido por el atributo "maximoNumeroRegistros"
	 * 
	 * @return Registros regresados por la consulta
	 */
	public Registros doSearch() {
		Registros registros = new Registros();
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			log.info("CQueryHelperRegExtJS.doSearch():: pag= " + this.qryGen.getClass().getName() + " (" + this.idQuery + ")" );
			long tiempoInicial = (new java.util.Date()).getTime();
			
			registros = con.consultarDB(
						this.qryGen.getDocumentQueryFile(), 	//QUERY DE LLAVES
						this.qryGen.getConditions(),		//VARIABLES BIND
						true,						//NO HACE DISTINCION DE TIPOS DE DATOS EN EL RESULTADO
						this.getMaximoNumeroRegistros());						//NUMERO MAXIMO DE REGISTROS A PRESENTAR EN LA CONSULTA

			long tiempoFinal = (new java.util.Date()).getTime();
			log.info("CQueryHelperRegExtJS.doSearch():: t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );

			return registros;
			
		} catch(Exception e) {
			throw new AppException("CQueryHelperRegExtJS::doSearch(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}
	



	/**
	 * Limpia los datos en sesion referente a la paginacion, para iniciar
	 * una nueva consulta.
	 *
	 * @param request Objeto HttpServletRequest para obtener la sesion
	 */	
	private void cleanSession(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute(this.paginadorIds);
		session.removeAttribute(this.paginadorTotales);
	}
	
	
	/**
	 * Obtiene el n�mero m�ximo de registros a presentar en una consulta 
	 * De manera predeterminada el valor es 1000 a menos que haya sido 
	 * ajustado mediante setMaximoNumeroRegistros(int)
	 * @return numero m�ximo de registros a regresar.
	 */
	public int getMaximoNumeroRegistros() {
		return this.maximoNumeroRegistros;
	}

	/**
	 * Establece el n�mero m�ximo de registros a presentar en una consulta 
	 * De manera predeterminada el valor es 1000 
	 * @param maximoNumeroRegistros M�ximo de registros a regresar.
	 */
	public void setMaximoNumeroRegistros(int maximoNumeroRegistros) {
		this.maximoNumeroRegistros = maximoNumeroRegistros;
	}


	/**
	 * Especifica si hay varios paginadores que se usan en la misma pagina
	 * ya que de ser as� es necesario guardar las informaci�n de las 
	 * consultas con diferentes identificadores en sesion.
	 * Si multiplesPaginadoresXPagina = false (predeterminado) la informaci�n
	 * de la consulta se almacena siempre bajo el mismo identificador en sesi�n.
	 * Este m�todo debe ser llamado inmediatamente despues de invocar el constructor
	 * CQueryHelperRegExtJSWM(IQueryGeneratorRegExtJSWM qryGen)
	 * @param multiplesPaginadoresXPagina indicador de si hay multiples paginadores por pagina o no
	 */
	public void setMultiplesPaginadoresXPagina(boolean multiplesPaginadoresXPagina) {
		if (multiplesPaginadoresXPagina) {
			this.paginadorIds = this.paginadorIds + "_" + qryGen.getClass().getName();
			this.paginadorTotales = this.paginadorTotales + "_" + qryGen.getClass().getName();
		}
	}
        
        
        /**
         * Manda a llamar el metodo para generar los archivos de la consulta realizada a traves de un Hilo.
         * @param request HttpRequest de donde se obtienen los criterios de busqueda
         * @path Ruta del archivo a generar
         * @tipo tipo de archivo a generar
         * @return Cadena con el nombre del archivo generado
         */
        public ThreadCreateFiles getThreadCreateCustomFile(HttpServletRequest request, String path, String tipo){

            try {
                    
                   log.info("CQueryHelperThreadRegExtJS.getThreadCreateCustomFile()::(E) pag= " + this.qryGen.getClass().getName() + " (" + this.idQuery + ")" );


                    RequestFake requestFake = new RequestFake(request);
                    ThreadCreateFiles threadCreateFile = new ThreadCreateFiles();
                    threadCreateFile.setInterface(this.qryGen);
                    threadCreateFile.setQuery(this.qryGen.getDocumentQueryFile());
                    threadCreateFile.setCondiciones(this.qryGen.getConditions());
                    threadCreateFile.setPath(path);
                    threadCreateFile.setTipo(tipo);
                    threadCreateFile.setRequestFake(requestFake);                
                    threadCreateFile.start();
;
                    return threadCreateFile;
                    
            } catch(Exception e) {
                e.printStackTrace();
                throw new AppException("CQueryHelperRegExtJS::getThreadCreateCustomFile(Exception) ", e);
            } finally {
                log.info("CQueryHelperThreadRegExtJS.getThreadCreateCustomFile()::(S)" );
            }
                
        }
        

}//CQueryHelperRegExtJS
