package netropology.utilerias;

import java.io.InputStream;

import java.sql.PreparedStatement;

import org.apache.commons.logging.Log;


public class PDF_correcciones_FianzaElectronica  { 
	//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(PDF_correcciones_FianzaElectronica.class);
	private String cveFianza;
	private int size;


	
	
	/**
	 * metodo que guarda la imagen de manual en la tabla  com_manuales_perfil_epo
	 * @throws netropology.utilerias.AppException
	 * @param is
	 */
	public void guardarArchivo(InputStream is) throws AppException {
		log.info("guardarArchivo(E) ::..");
		AccesoDB con = new AccesoDB();		
		boolean exito = true;
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		int tamanio = 0;
		//**********************************Validación de parametros:*****************************
		try {		
			if (this.cveFianza == null || this.cveFianza.equals("") || 	is == null || this.size == 0 ) {
				throw new Exception("Los parametros son requeridos");	
			}		
			tamanio = this.size;
		} catch(Exception e) {
			log.error("guardarImagen(Error). " +
			"Error en los parametros/atributos establecidos: " + e.getMessage() +
			"this.cveFianza=" + this.cveFianza + "\n" +	
			"this.size=" + this.size + "\n" +
			"is=" + is);
			throw new AppException("Error inesperado. ", e);
		}
		//***************************************************************************************
		try {
			con.conexionDB();

			strSQL.append(" UPDATE fe_fianza");
			strSQL.append(" SET bi_pdf_correcciones = ? ");
			strSQL.append(" WHERE ic_fianza = ? ");
			
			log.debug("..:: strSQL: "+strSQL.toString());

			pst = con.queryPrecompilado(strSQL.toString());
			pst.setBinaryStream(1, is, tamanio);
			pst.setInt(2, Integer.parseInt(cveFianza));
			
			pst.executeUpdate();
			pst.close();
		} catch(Exception e)  {
			exito = false;
			log.error("guardarArchivoPDF_correcciones(ERROR) ::..");
			throw new AppException("Error Inesperado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			log.info("guardarArchivoPDF_correcciones(S) ::..");
		}
	}
	

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}   

	public int getCveFianza() {
		return size;
	}

	public void setCveFianza(String cveFianza) {
		this.cveFianza = cveFianza;
	}   
	
	
	
}