package netropology.utilerias;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;


public class ManualesPerfilEpo  { 
	//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ManualesPerfilEpo.class);
	private String perfil;
	private String noEpo;
	private String nomanual;
	private String ic_manual;
	private int size;

/**
	 * Obtiene la imagen almacenada y genera un 
	 * archivo temporal para consultarla
	 * @param rutaDestino Directorio donde se genera el archivo temporal
	 * @return Cadena con el nombre del archivo generado con la imagen
	 */
	public List consultarImagen(String rutaDestino)  throws AppException {
		log.debug("consultarImagen(E)");
	
		log.debug("rutaDestino  " +rutaDestino);
		StringBuffer  strSQL = new StringBuffer();	
		List documentos = new ArrayList();
		List totaldocumentos = new ArrayList();
		String nombreArchivoTmp = null;						
		
		AccesoDB con = new AccesoDB();	
		//**********************************Validación de parametros:*****************************
		
		try {
			
			if (this.perfil == null || this.perfil.equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
				
		} catch(Exception e) {
			log.error("consultarImagen(Error). " +
					"Error en los parametros/atributos establecidos: " + e.getMessage() +
					"this.perfil=" + this.perfil );
			throw new AppException("Error inesperado. ", e);
		}
		//***************************************************************************************
	
		try {
			con.conexionDB();
			
			strSQL = new StringBuffer();					
			strSQL.append("  SELECT ic_manual, cg_nombre,  bi_documento ");
			strSQL.append("  FROM com_manuales_perfil_epo  ");
			strSQL.append("  where cc_perfil = ? ");
			if(!noEpo.equals("")){					
				strSQL.append( "AND IC_EPO = ?  OR IC_EPO IS NULL "); 
			}
		
			
			log.debug("  strSQL   "+ strSQL.toString());
			log.debug("  perfil   "+ perfil);
			log.debug("  noEpo   "+ noEpo);
			log.debug("  rutaDestino   "+ rutaDestino);
			
			
			PreparedStatement ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1, perfil);
			if(!noEpo.equals("")){		
				ps.setString(2, noEpo);
		  }		
				
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){					
				InputStream inStream = rs.getBinaryStream("bi_documento");
				CreaArchivo creaArchivo = new CreaArchivo();
				if (!creaArchivo.make(inStream, rutaDestino, ".pdf")) {
					throw new AppException("Error al generar el archivo en " + rutaDestino);
				}
				nombreArchivoTmp = creaArchivo.getNombre();
				inStream.close();
				
				documentos = new ArrayList();
				documentos.add(rs.getString(1)==null?"":rs.getString(1));
				documentos.add(rs.getString(2)==null?"":rs.getString(2));	
				documentos.add(nombreArchivoTmp);	
				totaldocumentos.add(documentos);
			}
			rs.close();
			ps.close();
		
			return totaldocumentos;
			
		} catch(Exception e) {
			throw new AppException("Error Inesperado", e);
		} finally {
			log.debug("consultarImagen(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * insertamos el registro del Manual 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param datos
	 */
	public String guardarManual(List datos ) throws AppException {
		log.info("guardarManual(E) ::..");
		AccesoDB con = new AccesoDB();		
		boolean exito = true;
		PreparedStatement pst = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		StringBuffer strSQL = new StringBuffer();
		int seqNEXTVAL =0;

		String perfil0 =datos.get(0).toString();
		String nombre0 =datos.get(1).toString();
		String descripcion0 =datos.get(2).toString();
		String ic_epo =datos.get(3).toString();
		
		System.out.println("perfil0 "+perfil0);
		System.out.println("nombre0 "+nombre0);
		System.out.println("descripcion0 "+descripcion0);
		System.out.println("ic_epo "+ic_epo);
		
		
		try {
			con.conexionDB();

		strSQL = new StringBuffer();	
			strSQL.append(" Select  seq_com_manuales_perfil_epo.nextval  FROM dual ");
			ps = con.queryPrecompilado(strSQL.toString());
			rs = ps.executeQuery();
				while(rs.next()){
				seqNEXTVAL = rs.getInt(1);
			}
			rs.close();
			ps.close();
						
			strSQL = new StringBuffer();			
			strSQL.append(" INSERT INTO  com_manuales_perfil_epo");
			strSQL.append(" ( ic_manual, cc_perfil, cg_nombre, cg_descripcion , ic_epo ) ");
			strSQL.append(" values ( ? , ?, ?, ?, ?) ");
			
			
			log.debug("..:: strSQL: "+strSQL.toString());

			pst = con.queryPrecompilado(strSQL.toString());	
			pst.setInt(1, seqNEXTVAL); 
			pst.setString(2, 	perfil0);
			pst.setString(3, 	nombre0);
			pst.setString(4, 	descripcion0);
			pst.setString(5, 	ic_epo);							
			pst.executeUpdate();
			pst.close();
			
			
		} catch(Exception e)  {
			exito = false;
			log.error("guardarManual(ERROR) ::..");
			throw new AppException("Error Inesperado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			log.info("guardarManual(S) ::..");
		}
		
		return String.valueOf(seqNEXTVAL); 
		
	}
	
	
	/**
	 * metodo que guarda la imagen de manual en la tabla  com_manuales_perfil_epo
	 * @throws netropology.utilerias.AppException
	 * @param is
	 */
	public void guardarArchivo(InputStream is) throws AppException {
		log.info("guardarArchivo(E) ::..");
		AccesoDB con = new AccesoDB();		
		boolean exito = true;
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		int tamanio = 0;
		//**********************************Validación de parametros:*****************************
		try {		
			if (this.ic_manual == null || this.ic_manual.equals("") || 	is == null || this.size == 0 ) {
				throw new Exception("Los parametros son requeridos");	
			}		
			tamanio = this.size;
		} catch(Exception e) {
			log.error("guardarImagen(Error). " +
			"Error en los parametros/atributos establecidos: " + e.getMessage() +
			"this.ic_manual=" + this.ic_manual + "\n" +	
			"this.size=" + this.size + "\n" +
			"is=" + is);
			throw new AppException("Error inesperado. ", e);
		}
		//***************************************************************************************
		try {
			con.conexionDB();

			strSQL.append(" UPDATE com_manuales_perfil_epo");
			strSQL.append(" SET bi_documento = ? ");
			strSQL.append(" WHERE ic_manual = ? ");
			
			log.debug("..:: strSQL: "+strSQL.toString());

			pst = con.queryPrecompilado(strSQL.toString());
			pst.setBinaryStream(1, is, tamanio);
			pst.setInt(2, Integer.parseInt(ic_manual));
			
			pst.executeUpdate();
			pst.close();
		} catch(Exception e)  {
			exito = false;
			log.error("guardarArchivo(ERROR) ::..");
			throw new AppException("Error Inesperado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			log.info("guardarArchivo(S) ::..");
		}
	}
	
/**
	 *metodo para eliminar el manual de la tabla  com_manuales_perfil_epo
	 * @throws netropology.utilerias.AppException
	 * @param manual
	 */

	public void eliminarManual(String  manual ) throws AppException {
		log.info("eliminarManual(E) ::..");
		AccesoDB con = new AccesoDB();		
		boolean exito = true;
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
						
		try {
			con.conexionDB();
			
			strSQL = new StringBuffer();
			strSQL.append(" DELETE FROM com_manuales_perfil_epo");
			strSQL.append(" WHERE  ic_manual =  ? ");
			
			log.debug("..:: strSQL: "+strSQL.toString());

			pst = con.queryPrecompilado(strSQL.toString());	
			pst.setInt(1, Integer.parseInt(manual));	
			pst.executeUpdate();
			pst.close();
			
			
		} catch(Exception e)  {
			exito = false;
			log.error("eliminarManual(ERROR) ::..");
			throw new AppException("Error Inesperado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			log.info("eliminarManual(S) ::..");
		}
				
	}
	
	/**
	 * descarga el manual desde la consulta 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param strDirectorioTemp
	 * @param ic_manual
	 */
	
	public File descargaManual(String ic_manual, String strDirectorioTemp) throws AppException{
		log.info("descargaManual(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		
		try{
			con.conexionDB();

			strSQL.append(" SELECT bi_documento");
			strSQL.append(" FROM com_manuales_perfil_epo");
			strSQL.append(" WHERE ic_manual = ?");
			
			varBind.add(new Long(ic_manual));
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			if(rst.next()){
				String pathname = strDirectorioTemp + Comunes.cadenaAleatoria(8) + ".pdf";
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rst.getBlob("bi_documento");
				InputStream inStream = blob.getBinaryStream();
				int size = (int)blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1){
					fileOutputStream.write(buffer, 0, length);
				}
			}
			rst.close();
			pst.close();
			
		}catch(Exception e){
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("descargaManual(S)");
		}
		return file;
	}
	
	/**
	 * Metodo para generar todos los manuales 
	 * @throws netropology.utilerias.AppException
	 * @param strDirectorioPublicacion
	 */
	public void consultarImagenTodos(String strDirectorioPublicacion)  throws AppException {
		log.debug("consultarImagenTodos(E)");
		
		StringBuffer  strSQL = new StringBuffer();			
		AccesoDB con = new AccesoDB();			
		try {
			con.conexionDB();
			
			strSQL = new StringBuffer();					
			strSQL.append("  SELECT ic_manual as ic_manual,  bi_documento, cc_perfil as perfil ,  IC_EPO as EPO ");
			strSQL.append("  FROM com_manuales_perfil_epo  ");
			
			PreparedStatement ps = con.queryPrecompilado(strSQL.toString());
			ResultSet rs = ps.executeQuery();
			String ruta  = strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/manuales/";				
			Comunes.crearDirectoriosArchivo (ruta);	//Crea Ruta  en caso de no existir 
				
			while(rs.next()){	
			
				InputStream inStream = rs.getBinaryStream("bi_documento");
				CreaArchivo creaArchivo = new CreaArchivo();
				String perfil = rs.getString("perfil")==null?"":rs.getString("perfil");
				String ic_epo = rs.getString("EPO")==null?"":rs.getString("EPO");
				String ic_manual = rs.getString("ic_manual")==null?"":rs.getString("ic_manual");
				String nombreArchivo = perfil.replaceAll(" ","_")+"_"+ic_manual+".pdf";
				String rutaDestino  = strDirectorioPublicacion + "00archivos/15cadenas/15archcadenas/manuales/"+nombreArchivo;				
				
				if (!creaArchivo.make(inStream, rutaDestino)) {
					throw new AppException("Error al generar el archivo en " + rutaDestino);
				}
							
				inStream.close();			
				
			}
			rs.close();
			ps.close();
		
		} catch(Exception e) {
			throw new AppException("Error Inesperado", e);
		} finally {
			log.debug("consultarImagenTodos(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 */
	public List consultarNombres()  throws AppException {
		log.debug("consultarImagen(E)");
	
		
		StringBuffer  strSQL = new StringBuffer();	
		List documentos = new ArrayList();
		List totaldocumentos = new ArrayList();
		List varBind = new ArrayList();
		AccesoDB con = new AccesoDB();	
	
		try {
			con.conexionDB();
			
			strSQL = new StringBuffer();					
			strSQL.append("  SELECT ic_manual as ic_manual , cg_nombre as nombre , cc_perfil as perfil  ");
			strSQL.append("  FROM com_manuales_perfil_epo  ");
			strSQL.append("  where ic_epo IS NULL ");
			if(!noEpo.equals("")){					
				strSQL.append( "OR  IC_EPO = ?   ");
				varBind.add(noEpo);
			}
			if(!perfil.equals("")){	
				strSQL.append("  AND cc_perfil = ? ");
			   varBind.add(perfil); 
			}
			log.debug("strSQL.toString()::::"+strSQL.toString());
			log.debug("varBind::::"+varBind);
					
			PreparedStatement ps = con.queryPrecompilado(strSQL.toString(), varBind);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){					
				documentos = new ArrayList();
				documentos.add(rs.getString("ic_manual")==null?"":rs.getString("ic_manual"));			
				documentos.add(rs.getString("nombre")==null?"":rs.getString("nombre"));
				String perfil = rs.getString("perfil")==null?"":rs.getString("perfil");
				
				documentos.add(perfil.replaceAll(" ","_"));			
				totaldocumentos.add(documentos);
			}
			rs.close();
			ps.close();
		
			return totaldocumentos;
			
		} catch(Exception e) {
			throw new AppException("Error Inesperado", e);
		} finally {
			log.debug("consultarImagen(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	
	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	
	public String getNoEpo() {
		return noEpo;
	}

	public void setNoEpo(String noEpo) {
		this.noEpo = noEpo;
	}

	
	public String getNomanual() {
		return nomanual;
	}

	public void setMomanual(String nomanual) {
		this.nomanual = nomanual;
	}

	public String getIc_manual() {
		return ic_manual;
	}

	public void setIc_manual(String ic_manual) {
		this.ic_manual = ic_manual;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}


	
}