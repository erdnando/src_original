package netropology.utilerias;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class MenuServlet extends HttpServlet  {
	private static final String CONTENT_TYPE = "text/xml; charset=UTF-8";

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		String informacion = (request.getParameter("informacion")!=null)?request.getParameter("informacion"):"";
		
		
		if (informacion.equals("ObtieneMenu")) {
			HttpSession session = request.getSession();
			out.println(session.getAttribute("sesMenu"));
			out.close();
		}
		
		if (informacion.equals("ObtieneMenuPerfil")) {
			HttpSession session = request.getSession();
			out.println(session.getAttribute("sesMenuPerfil"));
			out.close();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	
}