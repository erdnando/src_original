package netropology.utilerias;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

/**
 * @author jclark
 *
 */
public class CQueryHelper {
	
	private IQueryGenerator qryGen;
	private IQueryGeneratorPS qryGenPS;
	public String ids = "fixThisInTheFuture";
	public String totales = "totales";
	private PreparedStatement pst = null;
	/**
	 * Id para identificar de manera unica la ejecuci�n de un query y poder
	 * rastrear donde inicia y donde termina.
	 * Util para cuando hay varios queries de paginador ejecutandose
	 * simultaneamente.
	 */
	private final String idQuery = String.valueOf((new java.util.Date()).getTime());


	public CQueryHelper(IQueryGenerator qryGen){
		this.qryGen = qryGen;
		if(qryGen instanceof IQueryGeneratorPS) {
			this.qryGenPS = (IQueryGeneratorPS)qryGen;
		}
	}

	public CQueryHelper(IQueryGenerator qryGen, String ids1,String totales1){
		this.qryGen = qryGen;
		if(qryGen instanceof IQueryGeneratorPS) {
			this.qryGenPS = (IQueryGeneratorPS)qryGen;
		}
		this.ids = ids1;
		this.totales = totales1;
	}

	public float getSum(HttpServletRequest request){
		Float sum = (Float)request.getSession().getAttribute(documentSum);

		if (sum!=null)
		{
			return sum.floatValue();
		}
		else
		{
			return 0;
		}
	}

//	public static final String ids = "fixThisInTheFuture";
//	public static final String totales = "totales";
	public static final String rowCount = "rowCount";
	public static final String documentSum = "documentSum";
	public static final String discountSum = "discountSum";

	/**
	 * get only the Results that will be displayed on the current page
	 *
	 * @param request maintains all State for this static invocation
	 * @param con is the Connection Object that should be used for the query
	 * @param offset indicates the page that we are currently displaying
	 * @param pageSize gives the number of rows that should be returned here
	 * @return
	 * @throws Exception
	 */
	public ResultSet getPageResultSet(
		HttpServletRequest request,
		AccesoDB con,
		int offset,
		int pageSize)
		throws Exception {
		Collection nextSetOfIds = getNextSetOfIds(request, offset, pageSize);
		String query =	qryGen.getDocumentSummaryQueryForIds(request,nextSetOfIds);
		ResultSet rs = null;
		if(qryGen instanceof IQueryGeneratorPS){
			pst = con.queryPrecompilado(query);
			int i=1;
			int numList = qryGenPS.getNumList(request);
			for(int j=0;j<numList;j++){
				for (Iterator it = nextSetOfIds.iterator(); it.hasNext();){
					pst.setString(i,it.next().toString());
					i++;
				}
			}
			rs = pst.executeQuery();
		}else{
			rs = con.queryDB(query);
		}
		return rs;
	}

	/**
	 * Obtiene la informacion a mostrar en formato JSON.
	 * IMPORTANTE!!!!: Es necesario que los alias de las columnas en la consulta
	 * no se repitan, ya que de lo contrario el objeto resultante eliminar� los
	 * campos repetidos
	 *
	 * @param request maintains all State for this static invocation
	 * @param con Objeto de conexion, ya debe estar establecida la conexion
	 * @param start Indica el indice del registro a partir del que se obtendra 
	 * 		la informacion
	 * @param limit Especifica el numero maximo de registros que se regresaran
	 * @return Cadena JSON con los registros correspondientes
	 */
	public String getJSONPageResultSet(
			HttpServletRequest request,
			AccesoDB con,
			int start,
			int limit) {

		//------------------- Validacion de parametros -------------------------
		try {
			if (start < 0) {
				throw new Exception("El parametro start debe ser mayor a 0");
			}
			if (limit <1) {
				throw new Exception("El parametro limit debe ser mayor a 1");
			}
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		//----------------------------------------------------------------------
		
		JSONArray jsonArr = new JSONArray();
		try {
			ResultSet rs = this.getPageResultSet(request, con, start, limit);
			ResultSetMetaData rsMD = rs.getMetaData();
			int numCols = rsMD.getColumnCount();
					
			while(rs.next()){
				String columna = "";
				String valor = "";
				HashMap registro = new HashMap();
				for (int i = 1; i <= numCols; i++) {
					String nombreColumna = rsMD.getColumnName(i);
					int colType = rsMD.getColumnType(i);
					switch(colType) {
						case Types.NUMERIC:
						case Types.DECIMAL:
						case Types.INTEGER:
							BigDecimal rsNumeric = rs.getBigDecimal(i)==null?new BigDecimal("0"):rs.getBigDecimal(i);
							registro.put(nombreColumna, rsNumeric.toPlainString());
							break;
						case Types.DATE:
						case Types.TIME:
						case Types.TIMESTAMP:
							Timestamp rsFecha = rs.getTimestamp(i);
							registro.put(nombreColumna, rsFecha);
							break;
						default:
							String rsString = rs.getString(i)==null?"":rs.getString(i);
							registro.put(nombreColumna, rsString);
							break;
					}//switch(colType)
				} //fin del for
				jsonArr.add(registro);
			} //fin while rs
			rs.close();
		} catch(Throwable t) {
			throw new AppException("Error al obtener los registros de la paginacion", t);
		}
		return String.valueOf(net.sf.json.JSONObject.fromObject(jsonArr));
		
	}

	/**
	 * query for the Primary Keys of the Documents from a dynamic set of
	 * criteria
	 *
	 * @param request maintains State of this request
	 * @param con is the Connection we should use for this query
	 * @throws Exception
	 */
	public void executePKQueryIfNecessary(
		HttpServletRequest request,
		AccesoDB con)
		throws Exception {
		String offset = request.getParameter("pager.offset");
		HttpSession session = request.getSession();
		String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
		//System.out.println("Paso clase 1");
		/*if(session.getAttribute("operacion")!=null){
			if("Generar".equals(session.getAttribute("operacion").toString()))
				operacion = (String)session.getAttribute("operacion");
		}*/

		if (operacion.equals("Generar") && offset == null) {
			//System.out.println("Paso clase 2");
			session.removeAttribute(ids);
			ResultSet rs = null;
			PreparedStatement ps = null;
			ArrayList al = null;

			System.out.println("CQueryHelper.executePKQueryIfNecessary():: pag= " + qryGen.getClass().getName() + " (" + this.idQuery + ")" );
			
			if(qryGen instanceof IQueryGeneratorPS){
				
				ps = con.queryPrecompilado(qryGen.getDocumentQuery(request));
				al = qryGenPS.getConditions(request);	//GEAG (cambio de linea)
				
				for(int i=0;i<al.size();i++){
					if(al.get(i) instanceof Integer){
					Integer intg = (Integer)al.get(i);
					ps.setInt(i+1, intg.intValue());
					} else if(al.get(i) instanceof String){
						ps.setString(i+1, al.get(i).toString());
					} else if(al.get(i) instanceof Double){
						Double doubg = (Double)al.get(i);
						ps.setDouble(i+1, doubg.doubleValue());
					}
				}
				ps.setMaxRows(1000);	//Se limita el numero m�ximo de registros a 1000 para despliegue
				long tiempoInicial = (new java.util.Date()).getTime();
				rs = ps.executeQuery();
				long tiempoFinal = (new java.util.Date()).getTime();
				System.out.println("CQueryHelper.executePKQueryIfNecessary():: PKs. t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
				ps.clearParameters();
			}else{
				long tiempoInicial = (new java.util.Date()).getTime();
				rs = con.queryDB(qryGen.getDocumentQuery(request),1000);
				long tiempoFinal = (new java.util.Date()).getTime();
				System.out.println("CQueryHelper.executePKQueryIfNecessary():: PKs t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
			}
			//System.out.println("Paso clase 3");
			Vector newIds = new Vector();
			Vector vecTotales = new Vector();
			while (rs.next()) {
				newIds.add(rs.getString(1));  //Llenamos el vector de llaves primarias
			}
			if(ps!=null) ps.close();
			
			int count=0;
			float total=0;
			float discountTotal=0;
			int i=0;
			if (newIds.size() > 0) {
				try{
					
					String queryTotales = qryGen.getAggregateCalculationQuery(request); //GEAG
					
					//Solo si hay query de totales se ejecuta el query //GEAG
					if (queryTotales != null && !queryTotales.trim().equals("") ) { //GEAG
					
						if(qryGen instanceof IQueryGeneratorPS){
							al = qryGenPS.getConditions(request); //GEAG
							ps = con.queryPrecompilado(queryTotales); //GEAG
							int numList = qryGenPS.getNumList(request);
							 
							for(int j=0;j<numList;j++) {
								for(int x=0;x<al.size();x++){
									if(al.get(x) instanceof Integer){
										Integer intg = (Integer)al.get(x);
										ps.setInt(i+1, intg.intValue());
									} else if(al.get(x) instanceof String){
										ps.setString(i+1, al.get(x).toString());
									} else if(al.get(x) instanceof Double){
										Double doubg = (Double)al.get(x);
										ps.setDouble(i+1, doubg.doubleValue());
									}
									i++;
								}
							}
							long tiempoInicial = (new java.util.Date()).getTime();
							rs = ps.executeQuery();
							long tiempoFinal = (new java.util.Date()).getTime();
							System.out.println("CQueryHelper.executePKQueryIfNecessary():: Totales t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
							ps.clearParameters();
						} else {
							long tiempoInicial = (new java.util.Date()).getTime();
							rs = con.queryDB(queryTotales); //GEAG
							long tiempoFinal = (new java.util.Date()).getTime();
							System.out.println("CQueryHelper.executePKQueryIfNecessary():: Totales t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
						}
						//System.out.println("Paso clase 4");
						Vector vecFilas = new Vector();
						int numCols = rs.getMetaData().getColumnCount();
						while(rs.next()){
							vecFilas = new Vector();
							vecFilas = new Vector();
							for(i=1;i<=numCols;i++){
								vecFilas.add((rs.getString(i)==null)?"":rs.getString(i));
							}
							vecTotales.add(vecFilas);
						}
						if(qryGen instanceof IQueryGeneratorPS){
							rs.close();
							if(ps!=null) ps.close();
						} else {
							con.cierraStatement();
						}
					} // fin de Totales
				}catch(Exception e){
					System.out.println(e);
					e.printStackTrace();
				}
			}

			System.out.println("ResultSetSize:  "+newIds.size());
			System.out.println("Totales "+vecTotales);
			System.out.println("newIds "+newIds);

			session.setAttribute(ids, newIds);
			session.setAttribute(totales,vecTotales);
			//System.out.println("Paso clase 5");
		}
	}

	/**
	 * The pager uses this method to determine how many results we
	 * are paging through
	 *
	 * @param request maintains the State of this request
	 * @return
	 */
	public int getResultSetSize(HttpServletRequest request){
		Vector vector = (Vector) request.getSession().getAttribute(ids);

		if (vector != null) {
			return vector.size();
		} else {
			return 0;
		}
	}

	/**
	 * The list of PrimaryKeys is stored in a user Sessions.  We need to page
	 * through this list using an offset provided by the pager taglib.
	 *
	 * @param request
	 * @param offset
	 * @param pageSize
	 * @return
	 */
	private Collection getNextSetOfIds(
		HttpServletRequest request,
		int offset,
		int pageSize) {
		Vector vector = (Vector) request.getSession().getAttribute(ids);
		if (vector.size()>offset+pageSize){
			return vector.subList(offset, offset + pageSize);
		}else{
			return vector.subList(offset,vector.size());
		}
	}

	/**
	 * The pages checks this method for whether or not there any results
	 * to page through
	 *
	 * @param request
	 * @return
	 */
	public boolean readyForPaging(HttpServletRequest request) {
		Vector vector = (Vector) request.getSession().getAttribute(ids);

		if (vector != null) {
			return (vector.size()>0);
		} else {
			return false;
		}
	}

	public Vector getResultCount(HttpServletRequest request){
		return (Vector)request.getSession().getAttribute(totales);
	}

	public float getDiscountSum(HttpServletRequest request){
		Float sum = (Float)request.getSession().getAttribute(discountSum);

		if (sum!=null) {
			return sum.floatValue();
		} else {
			return 0;
		}
	}

	public ResultSet getCreateFile(HttpServletRequest request, AccesoDB con) throws Exception {
		HttpSession session = request.getSession();
		System.out.println("getCreateFile(E):: pag= " + qryGen.getClass().getName() + " (" + this.idQuery + ")");
		
		ArrayList al = null;
		ResultSet rs = null;
		
		if(qryGen instanceof IQueryGeneratorPS){
			
			pst = con.queryPrecompilado(qryGen.getDocumentQueryFile(request));
			al = qryGenPS.getConditions(request); //GEAG (Cambio de linea)
			
			int numList = qryGenPS.getNumList(request);
			int x = 0;
			for(int j=0;j<numList;j++) {
				for(int i=0;i<al.size();i++){
					if(al.get(i) instanceof Integer){
						Integer intg = (Integer)al.get(i);
						x++;pst.setInt(x, intg.intValue());
					} else if(al.get(i) instanceof String){
						x++;pst.setString(x, al.get(i).toString());
					} else if(al.get(i) instanceof Double){
						Double doubg = (Double)al.get(i);
						x++;pst.setDouble(x, doubg.doubleValue());
					}
				}
			}
			long tiempoInicial = (new java.util.Date()).getTime();
			rs = pst.executeQuery();
			long tiempoFinal = (new java.util.Date()).getTime();
			System.out.println("CQueryHelper.getCreateFile():: t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
			pst.clearParameters();
		} else {
			long tiempoInicial = (new java.util.Date()).getTime();
			rs = con.queryDB(qryGen.getDocumentQueryFile(request));
			long tiempoFinal = (new java.util.Date()).getTime();
			System.out.println("CQueryHelper.getCreateFile():: t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
		}		

		System.out.println("getCreateFile(S)");
		return rs;
	}

	/**
	 * clean up the query state in the current User's Session
	 *
	 * @param request
	 */

	public void cleanSession(HttpServletRequest request){
		HttpSession session = request.getSession();

		String strLimpiar = (request.getParameter("txtLimpiar")==null)?"":request.getParameter("txtLimpiar");
		String strOperacion = (request.getParameter("operacion")==null)?"":request.getParameter("operacion");

		if ("Limpiar".equals(strLimpiar) || "Generar".equals(strOperacion) ) {
			session.removeAttribute(discountSum);
			session.removeAttribute(documentSum);
			session.removeAttribute(ids);
			session.removeAttribute(rowCount);
			//System.out.println("CQueryHelper.cleanSession(REMOVER SESIONES IDS) " + ids);
		}

	}

	public void cierraStatement() {
		try{
			if(this.pst !=null){
				this.pst.close();
				this.pst = null;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
