package netropology.utilerias;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.security.Principal;

import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Map;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

public class RequestFake implements HttpServletRequest  {
    private SessionFake sessionFake;

    public void setSessionFake(SessionFake sessionFake) {
        this.sessionFake = sessionFake;
    }

    public SessionFake getSessionFake() {
        return sessionFake;
    }

    public RequestFake(HttpServletRequest r) {
         sessionFake = new SessionFake(r.getSession());
    }

    @Override
    public String getAuthType() {
        // TODO Implement this method
        return null;
    }

    @Override
    public Cookie[] getCookies() {
        // TODO Implement this method
        return new Cookie[0];
    }

    @Override
    public long getDateHeader(String string) {
        // TODO Implement this method
        return 0L;
    }

    @Override
    public String getHeader(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public Enumeration<String> getHeaders(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        // TODO Implement this method
        return null;
    }

    @Override
    public int getIntHeader(String string) {
        // TODO Implement this method
        return 0;
    }

    @Override
    public String getMethod() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getPathInfo() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getPathTranslated() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getContextPath() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getQueryString() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getRemoteUser() {
        // TODO Implement this method
        return null;
    }

    @Override
    public boolean isUserInRole(String string) {
        // TODO Implement this method
        return false;
    }

    @Override
    public Principal getUserPrincipal() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getRequestedSessionId() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getRequestURI() {
        // TODO Implement this method
        return null;
    }

    @Override
    public StringBuffer getRequestURL() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getServletPath() {
        // TODO Implement this method
        return null;
    }

    @Override
    public HttpSession getSession(boolean b) {
        // TODO Implement this method
        return null;
    }

    @Override
    public HttpSession getSession() {
        // TODO Implement this method
        return sessionFake;
    }

    @Override
    public boolean isRequestedSessionIdValid() {
        // TODO Implement this method
        return false;
    }

    @Override
    public boolean isRequestedSessionIdFromCookie() {
        // TODO Implement this method
        return false;
    }

    @Override
    public boolean isRequestedSessionIdFromURL() {
        // TODO Implement this method
        return false;
    }

    @Override
    public boolean isRequestedSessionIdFromUrl() {
        // TODO Implement this method
        return false;
    }

    @Override
    public boolean authenticate(HttpServletResponse httpServletResponse) throws IOException, ServletException {
        // TODO Implement this method
        return false;
    }

    @Override
    public void login(String string, String string2) throws ServletException {
        // TODO Implement this method

    }

    @Override
    public void logout() throws ServletException {
        // TODO Implement this method
    }

    @Override
    public Collection<Part> getParts() throws IOException, ServletException {
        // TODO Implement this method
        return Collections.emptySet();
    }

    @Override
    public Part getPart(String string) throws IOException, ServletException {
        // TODO Implement this method
        return null;
    }

    @Override
    public Object getAttribute(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getCharacterEncoding() {
        // TODO Implement this method
        return null;
    }

    @Override
    public void setCharacterEncoding(String string) throws UnsupportedEncodingException {
        // TODO Implement this method
    }

    @Override
    public int getContentLength() {
        // TODO Implement this method
        return 0;
    }

    @Override
    public String getContentType() {
        // TODO Implement this method
        return null;
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getParameter(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public Enumeration<String> getParameterNames() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String[] getParameterValues(String string) {
        // TODO Implement this method
        return new String[0];
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        // TODO Implement this method
        return Collections.emptyMap();
    }

    @Override
    public String getProtocol() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getScheme() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getServerName() {
        // TODO Implement this method
        return null;
    }

    @Override
    public int getServerPort() {
        // TODO Implement this method
        return 0;
    }

    @Override
    public BufferedReader getReader() throws IOException {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getRemoteAddr() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getRemoteHost() {
        // TODO Implement this method
        return null;
    }

    @Override
    public void setAttribute(String string, Object object) {
        // TODO Implement this method

    }

    @Override
    public void removeAttribute(String string) {
        // TODO Implement this method
    }

    @Override
    public Locale getLocale() {
        // TODO Implement this method
        return null;
    }

    @Override
    public Enumeration<Locale> getLocales() {
        // TODO Implement this method
        return null;
    }

    @Override
    public boolean isSecure() {
        // TODO Implement this method
        return false;
    }

    @Override
    public RequestDispatcher getRequestDispatcher(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getRealPath(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public int getRemotePort() {
        // TODO Implement this method
        return 0;
    }

    @Override
    public String getLocalName() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String getLocalAddr() {
        // TODO Implement this method
        return null;
    }

    @Override
    public int getLocalPort() {
        // TODO Implement this method
        return 0;
    }

    @Override
    public ServletContext getServletContext() {
        // TODO Implement this method
        return null;
    }

    @Override
    public AsyncContext startAsync() throws IllegalStateException {
        // TODO Implement this method
        return null;
    }

    @Override
    public AsyncContext startAsync(ServletRequest servletRequest,
                                   ServletResponse servletResponse) throws IllegalStateException {
        // TODO Implement this method
        return null;
    }

    @Override
    public boolean isAsyncStarted() {
        // TODO Implement this method
        return false;
    }

    @Override
    public boolean isAsyncSupported() {
        // TODO Implement this method
        return false;
    }

    @Override
    public AsyncContext getAsyncContext() {
        // TODO Implement this method
        return null;
    }

    @Override
    public DispatcherType getDispatcherType() {
        // TODO Implement this method
        return null;
    }
}
