package netropology.utilerias;

import com.netro.afiliacion.Afiliacion;
import com.netro.cadenas.ConsBitModificaAfiliado;
import com.netro.descuento.ParametrosDescuento;
import com.netro.fianza.FianzaElectronica;
import com.netro.servicios.Servicios;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;

/**
 * El objetivo de esta clase es tener la l�gica para determinar que pantallas
 * se le deben mostrar al usuario cuando entra a la aplicacion, as� como
 * su orden de aparicion
 * @author Gilberto Aparicio
 */
public class Navegacion implements java.io.Serializable {
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(Navegacion.class);
	private String tipoAfiliado;
	private String claveAfiliado;
	private String claveEpoRelacionada;
	private String menuUsuario;
	private String perfil;
	private String tipoCliente;	//Aplica solo para cuando tipoAfiliado = P. 1 Proveedor 2 Distribuidor
	private String iNoUsuario; 
	/**
	 * Pantallas a invocarse antes de carga_variables
	 * @return Lista de p�ntallas
	 */
	public List getPantallasBasicas() {
		List pantallas = new ArrayList();			
		if (this.tipoAfiliado.equals("P")) {
			pantallas = this.getPantallasBasicasPyme();
		} else if (this.tipoAfiliado.equals("E")) {
			pantallas = this.getPantallasBasicasEpo();
		} else if (this.tipoAfiliado.equals("I")) {
			pantallas = this.getPantallasBasicasIF();
		} else if (this.tipoAfiliado.equals("N")) {
			pantallas = this.getPantallasBasicasNafin();
		} else if (this.tipoAfiliado.equals("A")) {
			pantallas = this.getPantallasBasicasAfianzadora(); 
		} else if (this.tipoAfiliado.equals("F")) {
			pantallas = this.getPantallasBasicasFiado(); 
		} else if (this.tipoAfiliado.equals("C")) {
			pantallas = this.getPantallasBasicasCliente(); 
		} 
		return pantallas;
	}

	/**
	 * Pantallas a invocarse despues de carga_variables
	 * @return Lista de p�ntallas
	 */
	public List getPantallasComplementarias() {
		List pantallas = new ArrayList();
		if (this.tipoAfiliado.equals("P")) {
			pantallas = this.getPantallasComplementariasPyme();
		} else if (this.tipoAfiliado.equals("E")) {
			pantallas = this.getPantallasComplementariasEpo();
		} else if (this.tipoAfiliado.equals("I")) {
			pantallas = this.getPantallasComplementariasIF();
		} else if (this.tipoAfiliado.equals("U")) {
			pantallas = this.getPantallasComplementariasUni();
		} else if (this.tipoAfiliado.equals("N")) {
			pantallas = this.getPantallasComplementariasNafin();
		} else if (this.tipoAfiliado.equals("A")) {
			pantallas = this.getPantallasComplementariasAfianzadora();
		} else if (this.tipoAfiliado.equals("F")) {
			pantallas = this.getPantallasComplementariasFiado();
		} else if (this.tipoAfiliado.equals("C")) {
			pantallas = this.getPantallasComplementariasCliente();
		}else if (this.tipoAfiliado.equals("CE")) {
			pantallas = this.getPantallasComplementariasCedi();
		}

		return pantallas;
	}

	/**
	 * Obtiene las pantallas iniciales basicas que debe ver la pyme.
	 * @return Listado de paginas iniciales.
	 */
	private List getPantallasBasicasPyme() {
		AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();
		
		try {
			con.conexionDB();
		
			//CHECAMOS CON CUANTAS EPOS ESTA RELACIONADA LA PYME
			String qryRelPyemEpo =
					" SELECT COUNT(*) AS numRelacionesPymeEpo " +
					" FROM comrel_pyme_epo pe, comcat_epo e  " +
					" WHERE pe.ic_epo = e.ic_epo  " +
					" 	AND e.cs_habilitado = ? " +
					" 	AND pe.ic_pyme = ? ";
			PreparedStatement pstmt = con.queryPrecompilado(qryRelPyemEpo);
			long numero1=Long.parseLong(this.claveAfiliado);
			pstmt.setString(1,"S");
			pstmt.setLong(2,Long.parseLong(this.claveAfiliado));
			ResultSet rsTraeRelPYMEEPO = pstmt.executeQuery();

			String pymeUnicamenteEnEpoBloqueada = "N";
			if(rsTraeRelPYMEEPO.next()) {
				pymeUnicamenteEnEpoBloqueada = (rsTraeRelPYMEEPO.getInt("numRelacionesPymeEpo") == 0)?"S":"N";
			}
			rsTraeRelPYMEEPO.close();
			pstmt.close();

			/*
			//---------Determina si una pyme es Internacional o no------------
			String qryPymeInternacional =
					" SELECT cs_internacional, '20secure/index.jsp' " +
					" FROM comcat_pyme WHERE ic_pyme = ? ";
			pstmt = con.queryPrecompilado(qryPymeInternacional);
			pstmt.setInt(1, Integer.parseInt(iNoPYME));
			rs1 = pstmt.executeQuery();
			if(rs1.next()){
				session.setAttribute("sesPymeInternacional" ,
						rs1.getString("cs_internacional"));
			}
			rs1.close();
			pstmt.close();
			*/
			
			//Si la pyme solo est� afiliada a una EPO inhabilitada, entonces se le muestra un mensaje y
			//no le permite entrar
			if (pymeUnicamenteEnEpoBloqueada.equals("S")) {
				pantallas.add("/20secure/20epo_bloqueada.jsp");
				return pantallas;
			}

			/*if (session.getAttribute("sesPymeInternacional").equals("S")) {
				bError = true;
				response.sendRedirect("20seleccion_idioma.jsp?bPymeRelacionada=" +  bPymeRelacionadaVariasEpos);
			}*/
			
			pantallas.add("/20secure/eligeEpoExt.jsp");	//Seleccion de Epo para trabajar
			
			
			
			///20secure/carga_variables.jsp siempre debe ser la ultima pantalla en agregarse en la navegacion inicial
			pantallas.add("/20secure/carga_variables.jsp");	//Establece variables de sesi�n que dependen de la relacion pyme-epo
			return pantallas;
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
			
	}

	/**
	 * Obtiene la lista de pantallas complementarias para la navegaci�n inicial
	 * para la Pyme
	 * @return lista de pantallas
	 */
	private List getPantallasComplementariasPyme() {
		//AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();
		
		try {
			Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB",Servicios.class);

			ParametrosDescuento paramDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

  
			List encuesta = servicios.hayEncuestas("PYME",  this.claveAfiliado,  this.claveEpoRelacionada);
			if (encuesta != null && encuesta.size() > 0) { //Hay encuestas por contestar
				pantallas.add("/20secure/encuestasExt.jsp");
			}
			List idsMensajes = servicios.getIdsMensajes(this.claveAfiliado, "PYME", this.claveEpoRelacionada);
			List mensajesAdicionales = paramDscto.getMensajesInicialesAdicionales(this.claveAfiliado, "PYME", this.claveEpoRelacionada);
			
			if (idsMensajes.size() > 0 || mensajesAdicionales.size() > 0) {
				//Hay mensajes a mostrar
				pantallas.add("/20secure/20mensajeExt.jsp"); //Pantalla de mensajes iniciales
			}
			
			if (MenuUsuario.menuContieneElemento(this.menuUsuario, "13DESCUENTO")) {
			
				if (MenuUsuario.menuContieneElemento(this.menuUsuario, "13CAPTURAS")) {
					if(paramDscto.contratoParametrizado(this.claveEpoRelacionada) && 
							!paramDscto.contratoAceptado(this.claveAfiliado, this.claveEpoRelacionada)) {
						pantallas.add("/13descuento/13pyme/contrato_ext.jsp");	//Contrato de Epo parametrizado
					}
					if (paramDscto.contratoParametrizadoIF(this.claveAfiliado) && 
							!paramDscto.contratoAceptadoIF(this.claveAfiliado)) {
						pantallas.add("/13descuento/13pyme/contratoIF_ext.jsp");		//Contrato de If parametrizado
					}
					if (!paramDscto.aceptaPromocion(this.claveAfiliado, this.claveEpoRelacionada)) {
						pantallas.add("/15cadenas/15mantenimiento/15parametrizacion/15pyme/15EnvioPromocionesext.jsp");		//Parametrizacion de Envio de Promociones
					}
				}
				if (servicios.mostrarMensajeCredito(this.claveAfiliado, this.claveEpoRelacionada) ) {
					pantallas.add("/13descuento/13pyme/13creditoext.jsp"); //Mensaje de credito
				}
			}
			
			if (this.tipoCliente != null && this.tipoCliente.equals("2")) {
				//Navegaci�n para cuando seleccionan trabajar como distribuidor.
				pantallas.add("/01principal/01pyme/indexDist.jsp");		//Pagina principal.  Siempre debe ser la ultima de las paginas complementarias
			} else {
				pantallas.add("/01principal/01pyme/index.jsp");		//Pagina principal.  Siempre debe ser la ultima de las paginas complementarias
			}
			
			return pantallas;
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			//if (con.hayConexionAbierta()) {
			//	con.cierraConexionDB();
			//}
		}
		
			
	}
	
	/**
	 * Obtiene la lista de pantallas complementarias para la navegaci�n inicial
	 * para la EPO
	 * @return lista de pantallas
	 */
	private List getPantallasComplementariasEpo() {
		//AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();
		
		try {
			Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB",Servicios.class);

			ParametrosDescuento paramDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);
		

			List encuesta = servicios.hayEncuestas("EPO", this.claveAfiliado, null);	//ADMIN EPO va clavado, ya que el metodo as� trabaja representando a todos los perfiles relacionados con EPO.
			if (encuesta != null && encuesta.size() > 0) { //Hay encuestas por contestar
				pantallas.add("/20secure/encuestasExt.jsp");
			}
			List idsMensajes = servicios.getIdsMensajes(this.claveAfiliado, "EPO", null);
			List mensajesAdicionales = paramDscto.getMensajesInicialesAdicionales(this.claveAfiliado, "EPO", null);
			
			if (idsMensajes.size() > 0 || mensajesAdicionales.size() > 0) {
				//Hay mensajes a mostrar
				pantallas.add("/20secure/20mensajeExt.jsp"); //Pantalla de mensajes iniciales
			}
			
			/*if (MenuUsuario.menuContieneElemento(this.menuUsuario, "13DESCUENTO")) {
			
				if (MenuUsuario.menuContieneElemento(this.menuUsuario, "13CAPTURAS")) {
					if(paramDscto.contratoParametrizado(this.claveEpoRelacionada) && 
							!paramDscto.contratoAceptado(this.claveAfiliado, this.claveEpoRelacionada)) {
						pantallas.add("/13descuento/13pyme/contrato_ext.jsp");	//Contrato de Epo parametrizado
					}
					if (paramDscto.contratoParametrizadoIF(this.claveAfiliado) && 
							!paramDscto.contratoAceptadoIF(this.claveAfiliado)) {
						pantallas.add("/13descuento/13pyme/contratoIF_ext.jsp");		//Contrato de If parametrizado
					}
					if (!paramDscto.aceptaPromocion(this.claveAfiliado, this.claveEpoRelacionada)) {
						pantallas.add("/15cadenas/15mantenimiento/15parametrizacion/15pyme/15EnvioPromocionesext.jsp");		//Parametrizacion de Envio de Promociones
					}
				}
				if (servicios.mostrarMensajeCredito(this.claveAfiliado, this.claveEpoRelacionada) ) {
					pantallas.add("/13descuento/13pyme/13creditoext.jsp"); //Mensaje de credito
				}
			}
		*/		  
			// metodo para  Obtener datos para saber si existen pymes sin numero de proveedor relacionadas a la EPO		
			int numeroPymesSinNumProv = paramDscto.verifPYMEsinNumProv(this.claveAfiliado);
			if(numeroPymesSinNumProv>0){
				pantallas.add("/13descuento/13epo/13pymeSinNumProv_popExt.jsp");
			}
			

			pantallas.add("/01principal/01epo/index.jsp");		//Pagina principal.  Siempre debe ser la ultima de las paginas complementarias
			
			return pantallas;
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			//if (con.hayConexionAbierta()) {
			//	con.cierraConexionDB();
			//}
		}
		
			
	}
	
	/**
	 * Obtiene las pantallas iniciales basicas que debe ver la EPO.
	 * @return Listado de paginas iniciales.
	 */
	private List getPantallasBasicasEpo() {
		AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();
		
		try {
			con.conexionDB();
		
			//if (condicion) {
			//	pantallas.add("/20secure/20epo_bloqueada.jsp");
			//}
			
			if(this.perfil.equals("ADMIN EPO GRUPO")) {
				pantallas.add("/20secure/eligeEpoGrupoExt.jsp"); //Fodea 010-2014	
			}
		
			///20secure/carga_variables.jsp siempre debe ser la utlima pantalla en agregarse en la navegacion inicial
			pantallas.add("/20secure/carga_variables.jsp");	//Epo no debiera pasar por esta pantalla... pasa por fines de compatibilidad.
			return pantallas;
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}



	
	/**
	 * Obtiene las pantallas iniciales basicas que debe ver el IF.
	 * @return Listado de paginas iniciales.
	 */
	private List getPantallasBasicasIF() {
		AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();
		
		try {
			con.conexionDB();
		
			if(this.perfil.equals("IF FONDO JUNIOR")) {
				pantallas.add("/20secure/elige_programaExt.jsp");	//Seleccion de Programa para IF  para trabajar
			}
			///20secure/carga_variables.jsp siempre debe ser la ultima pantalla en agregarse en la navegacion inicial
			pantallas.add("/20secure/carga_variables.jsp");	//IF no debiera pasar por esta pantalla... pasa por fines de compatibilidad.
			return pantallas;
			
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}



	/**
	 * Obtiene la lista de pantallas complementarias para la navegaci�n inicial
	 * para el IF
	 * @return lista de pantallas
	 */
	private List getPantallasComplementariasIF() {
		//AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();
		
		try {
			Servicios servicios = ServiceLocator.getInstance().lookup("ServiciosEJB",Servicios.class);

			ParametrosDescuento paramDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB",ParametrosDescuento.class);

 
			List encuesta = servicios.hayEncuestas("IF", this.claveAfiliado, null);	//ADMIN IF va clavado, ya que el metodo as� trabaja representando a todos los perfiles relacionados con IF.
			if (encuesta != null && encuesta.size() > 0) { //Hay encuestas por contestar
				pantallas.add("/20secure/encuestasExt.jsp");
			}
			List idsMensajes = servicios.getIdsMensajes(this.claveAfiliado, "IF", null);
			List mensajesAdicionales = paramDscto.getMensajesInicialesAdicionales(this.claveAfiliado, "IF", null);
			
			if (idsMensajes.size() > 0 || mensajesAdicionales.size() > 0) {
				//Hay mensajes a mostrar
				pantallas.add("/20secure/20mensajeExt.jsp"); //Pantalla de mensajes iniciales
			}
                        
                        List idsMsjEdoCta=null;
                        if(this.perfil.equals("ADMIN IF")||this.perfil.equals("ADMIN IF PAG")){
                            idsMsjEdoCta = servicios.getIdsMsjEdoCta(this.claveAfiliado);
                        }
                        
                        /* FODEA 002 2017 */
                        if (idsMsjEdoCta != null && idsMsjEdoCta.size() > 0) {
                                //Hay mensajes a mostrar
                                pantallas.add("/20secure/20mensajeEdoCtaExt.jsp"); //Pantalla de mensajes Estado de Cuenta
                        }
                                          
                        if ("ADMIN IF".equals(this.perfil) ) {
                            ConsBitModificaAfiliado  consMod = new ConsBitModificaAfiliado();
                            int hayNotificaciones =  consMod.notificaciones(this.claveAfiliado,this.iNoUsuario );
                            if(hayNotificaciones>0){
                                pantallas.add("/20secure/20BitaModificaAfilado.jsp");    
                            }
                        }
                    
			/*if (MenuUsuario.menuContieneElemento(this.menuUsuario, "13DESCUENTO")) {
			
				if (MenuUsuario.menuContieneElemento(this.menuUsuario, "13CAPTURAS")) {
					if(paramDscto.contratoParametrizado(this.claveEpoRelacionada) && 
							!paramDscto.contratoAceptado(this.claveAfiliado, this.claveEpoRelacionada)) {
						pantallas.add("/13descuento/13pyme/contrato_ext.jsp");	//Contrato de Epo parametrizado
					}
					if (paramDscto.contratoParametrizadoIF(this.claveAfiliado) && 
							!paramDscto.contratoAceptadoIF(this.claveAfiliado)) {
						pantallas.add("/13descuento/13pyme/contratoIF_ext.jsp");		//Contrato de If parametrizado
					}
					if (!paramDscto.aceptaPromocion(this.claveAfiliado, this.claveEpoRelacionada)) {
						pantallas.add("/15cadenas/15mantenimiento/15parametrizacion/15pyme/15EnvioPromocionesext.jsp");		//Parametrizacion de Envio de Promociones
					}
				}
				if (servicios.mostrarMensajeCredito(this.claveAfiliado, this.claveEpoRelacionada) ) {
					pantallas.add("/13descuento/13pyme/13creditoext.jsp"); //Mensaje de credito
				}
			}
			
	
*/			
			pantallas.add("/01principal/01if/index.jsp");		//Pagina principal.  Siempre debe ser la ultima de las paginas complementarias
			
			return pantallas;
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			//if (con.hayConexionAbierta()) {
			//	con.cierraConexionDB();
			//}
		}
		
			
	}

	/**
	 * Metodo para las pantallas del Perfil Universidas
	 * @return 
	 */

	private List getPantallasComplementariasUni() {
		//AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();
		
		try {
			
			pantallas.add("/01principal/01uni/index.jsp");
			
			return pantallas;
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			//if (con.hayConexionAbierta()) {
			//	con.cierraConexionDB();
			//}
		}
		
			
	}

	/**
	 * Obtiene las pantallas iniciales basicas que debe ver NAFIN.
	 * @return Listado de paginas iniciales.
	 */
	private List getPantallasBasicasNafin() {
		AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();
		
		try {
			con.conexionDB();
			
			if(this.perfil.equals("ADMIN FONDO JR") ||  this.perfil.equals("ADMIN GARANT")  ||  this.perfil.equals("ADMIN NAFIN PAG") ) {
				pantallas.add("/20secure/elige_programaExt.jsp");	//Seleccion de Programa para IF  para trabajar
			}
			///20secure/carga_variables.jsp siempre debe ser la ultima pantalla en agregarse en la navegacion inicial
			pantallas.add("/20secure/carga_variables.jsp");	//Nafin no debiera pasar por esta pantalla... pasa por fines de compatibilidad.
			return pantallas;
			
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene la lista de pantallas complementarias para la navegaci�n inicial
	 * para NAFIN
	 * @return lista de pantallas
	 */
	private List getPantallasComplementariasNafin() {
		List pantallas = new ArrayList();
		
		try {

			pantallas.add("/01principal/01nafin/index.jsp");		//Pagina principal.  Siempre debe ser la ultima de las paginas complementarias
			
			return pantallas;
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
		}
		
			
	}
	
	
	/**
	 * Obtiene las pantallas iniciales basicas que debe ver el tipo de usuario AFIANZADORA.
	 * @return Listado de paginas iniciales.
	 */
	private List getPantallasBasicasAfianzadora() {
		AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();
		
		try {
			con.conexionDB(); 
			
			pantallas.add("/20secure/carga_variables.jsp");	//Nafin no debiera pasar por esta pantalla... pasa por fines de compatibilidad.
			return pantallas;
			
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	 
	 	/**
	 * Obtiene las pantallas iniciales basicas que debe ver el tipo de Usuario FIADO.
	 * @return Listado de paginas iniciales.
	 */
	private List getPantallasBasicasFiado() {
		AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();  
		
		try {
			con.conexionDB(); 
			
			pantallas.add("/20secure/carga_variables.jsp");	//Nafin no debiera pasar por esta pantalla... pasa por fines de compatibilidad.

			return pantallas;
			
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * Obtiene la lista de pantallas complementarias para la navegaci�n inicial
	 * para AFIANZADOA
	 * @return 
	 */
	private List getPantallasComplementariasAfianzadora() {
		List pantallas = new ArrayList();
		
		try {

			pantallas.add("/01principal/01afianzadora/index.jsp");		//Pagina principal.  Siempre debe ser la ultima de las paginas complementarias
			
			return pantallas;
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
		}
		
			
	}
	
	
		
	/**
	 * Obtiene la lista de pantallas complementarias para la navegaci�n inicial
	 * para FIADO  
	 * @return 
	 */
	private List getPantallasComplementariasFiado() {
		List pantallas = new ArrayList();
		
		try {

			FianzaElectronica fianza = ServiceLocator.getInstance().lookup("FianzaElectronicaEJB",FianzaElectronica.class);

			String  terminos =fianza.consultaTerminos(claveAfiliado);
					
			if (terminos.equals("N") ) { 
				pantallas.add("/36fianza/36TerCondiciones.jsp"); 
			}
			
			pantallas.add("/01principal/01fiado/index.jsp");		//Pagina principal.  Siempre debe ser la ultima de las paginas complementarias
		
			
			
			return pantallas;
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
		}
		
			
	}
	
	
	
	 	/**
	 * Obtiene las pantallas iniciales basicas que debe ver el tipo de Usuario CLIENTE EXTERNO
	 * @return Listado de paginas iniciales.
	 */
	private List getPantallasBasicasCliente() { 
		AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();  
		
		try {
			con.conexionDB(); 
			
			pantallas.add("/20secure/carga_variables.jsp");	//Nafin no debiera pasar por esta pantalla... pasa por fines de compatibilidad.

			return pantallas;
			
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * Obtiene la lista de pantallas complementarias para la navegaci�n inicial
	 * para Cliente Externo    
	 * @return 
	 */
	private List getPantallasComplementariasCliente() {
		List pantallas = new ArrayList();
		
		try { 
			Afiliacion afiliacion = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
 			
			String bloqueo = afiliacion.getBleoqueCliExt(this.claveAfiliado);
			if (bloqueo.equals("N")) {
				pantallas.add("/20secure/mensajeClienteExt.jsp");
				return pantallas;
			}
			pantallas.add("/01principal/01cliente/index.jsp");		//Pagina principal.  Siempre debe ser la ultima de las paginas complementarias
		
			
			
			return pantallas;
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
		}
		
			
	}
	/**
	 *F13-2015
	 * Obtiene la lista de pantallas complementarias para la navegaci�n inicial
	 * para IFNB CEDI  
	 * @return
	 */
	private List getPantallasComplementariasCedi() {
		//AccesoDB con = new AccesoDB();
		List pantallas = new ArrayList();
		
		try {
			
			pantallas.add("/01principal/01cedi/index.jsp");
			
			return pantallas;
		} catch(Exception e) {
			throw new AppException("Error al obtener las pantallas de iniciales de la aplicacion", e);
		} finally {
			//if (con.hayConexionAbierta()) {
			// con.cierraConexionDB();
			//}
		}
		
			
	}
	
	public String getTipoAfiliado() {
		return tipoAfiliado;
	}

	/**
	 * Establece el tipo de afiliado
	 * @param tipoAfiliado 	 Tipo de afiliado E Epo, I If, P Pyme, N Nafin 
	 */
	public void setTipoAfiliado(String tipoAfiliado) {
		this.tipoAfiliado = tipoAfiliado;
	}

	public String getClaveAfiliado() {
		return claveAfiliado;
	}

	/**
	 * Establece la clave del afiliado
	 * @param claveAfiliado Clave del afiliado (ic_pyme, ic_epo, etc...)
	 */
	public void setClaveAfiliado(String claveAfiliado) {
		this.claveAfiliado = claveAfiliado;
	}


	/**
	 * Establece la clave de la epo relacionada con la pyme
	 * Solo aplica cuando tipoAfiliado = P
	 * @param claveEpoRelacionada Clave de la epo relacionada (ic_epo de comrel_pyme_epo)
	 */
	public void setClaveEpoRelacionada(String claveEpoRelacionada) {
		this.claveEpoRelacionada = claveEpoRelacionada;
	}


	public String getClaveEpoRelacionada() {
		return claveEpoRelacionada;
	}


	/**
	 * Establece el xml que contiene los elementos del menu
	 * @param menuUsuario XML del usuario
	 */
	public void setMenuUsuario(String menuUsuario) {
		this.menuUsuario = menuUsuario;
	}


	public String getMenuUsuario() {
		return menuUsuario;
	}

	/**
	 * Establece el perfil del usuario
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}


	public String getPerfil() {
		return perfil;
	}


	/**
	 * Establece el tipo de cliente para afectar la navegaci�n acorde a lo seleccionado.
	 * @param tipoCliente Tipo de cliente 1 Proveedor 2 Distribuidor
	 */
	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}


	public String getTipoCliente() {
		return tipoCliente;
	}


        public void setINoUsuario(String iNoUsuario) {
            this.iNoUsuario = iNoUsuario;
        }
    
        public String getINoUsuario() {
            return iNoUsuario;
        }


}
