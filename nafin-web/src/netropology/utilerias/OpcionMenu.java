package netropology.utilerias;


public class OpcionMenu implements java.io.Serializable {
	private String claveMenuPadre;
	private String claveMenu;
	private String nombre;
	private String descripcion;
	private String url;
	private String activo;
	private String tipo;
	private String claveTipoAfiliado;

	public OpcionMenu() {
	}

	/**
	 * Obtiene la clave del menu padre
	 * @return Clave del menu padre
	 */
	public String getClaveMenuPadre() {
		return claveMenuPadre;
	}

	/**
	 * Establece la clave del menu padre
	 * @param claveMenuPadre Clave del menu padre
	 */
	public void setClaveMenuPadre(String claveMenuPadre) {
		this.claveMenuPadre = claveMenuPadre;
	}

	/**
	 * Obtiene la clave del menu
	 * @return Clave del menu
	 */
	public String getClaveMenu() {
		return claveMenu;
	}

	/**
	 * Establece la clave del menu
	 * @param claveMenu Clave del menu
	 */
	public void setClaveMenu(String claveMenu) {
		this.claveMenu = claveMenu;
	}

	/**
	 * Obtiene el nombre del menu
	 * @return Nombre del menu
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Establece el nombre del menu
	 * @param nombre Nombre del menu
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Obtiene la descripcion del menu
	 * @return Descripcion del menu
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Establece la descripci�n del menu
	 * @param descripcion Descripci�n del menu
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Obtiene el url del menu
	 * @return Url del menu
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Establece el url del menu
	 * @param url Url del menu
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Obtiene el estatus de activo del menu
	 * @return true si el menu est� activo o false de lo contrario
	 */
	public String getActivo() {
		return activo;
	}

	/**
	 * Establece el estatus de activo del menu
	 * @param activo true.- activo, false.- inactivo
	 */
	public void setActivo(String activo) {
		this.activo = activo;
	}

	/**
	 * Obtiene el tipo de menu
	 * @return P Pantalla, M Menu
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * Establece el tipo de menu
	 * @param tipo P Pantalla, M Menu
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * Obtiene la clave del tipo de afiliado (comcat_tipo_afiliado)
	 * @return Clave del tipo de afiliado
	 */
	public String getClaveTipoAfiliado() {
		return claveTipoAfiliado;
	}

	/**
	 * Establece la clavce del tipo de afiliado (comcat_tipo_afiliado)
	 * @param claveTipoAfiliado Clave del tipo de afiliado
	 */
	public void setClaveTipoAfiliado(String claveTipoAfiliado) {
		this.claveTipoAfiliado = claveTipoAfiliado;
	}
	
	public String toString() {
		return 
				"claveMenuPadre = " + claveMenuPadre + "|"+
				"claveMenu = " + claveMenu + "|"+
				"nombre = " + nombre + "|"+
				"descripcion = " + descripcion + "|"+
				"url = " + url + "|"+
				"activo = " + activo + "|"+
				"tipo = " + tipo + "|"+
				"claveTipoAfiliado = " + claveTipoAfiliado + "|";
	}
}