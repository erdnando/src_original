package netropology.utilerias.negocio;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import netropology.utilerias.AccesoDB;

public class FuncionesFinancieras{
	private AccesoDB bd=null;

	public void setConexion(AccesoDB bd){
		this.bd=bd;
	}

	private double anualidad(int n, double i){
		if ( i > 0){ return (1 - Math.pow((1 + i), -n))/i; }
		else{  return (double)n; }
	}

	public double tre(double tasa1, int plazo1, int plazo2, int base){
		double i=0d;
		double j=0d;
		double resultado=0d;
		double pow=0d;
		double p1=(double)plazo1;
		double p2=(double)plazo2;
		double b=(double)base;
		double h=0d;

		if ((plazo1 > 0) && (plazo2 > 0)){
			i=(double)(1 + (tasa1 * plazo1) / base);
			j=(double)(p2 / p1);
			pow=(double)(Math.pow(i, j) - 1);
			h=(double)(b / p2);
			resultado= (double)pow * (double)h;
			System.out.println("TASA TRE: \n resultado:" + resultado);
			return resultado;
		}
		else{ return 0; }
	}

	public double tre(double tasa1, int plazo1, int plazo2){
		double base=0d;
		double i=0d;
		double j=0d;
		double resultado=0d;
		double p1=(double)plazo1;
		double p2=(double)plazo2;
		double pow=0d;
		double h=0d;
		if ((plazo1 > 0) && (plazo2 > 0)){
			base=(double)360;
			i=(double)(1 + (tasa1 * plazo1) / base);
			j=(double)(p2 / p1);
			pow=(double)(Math.pow(i, j) - 1);
			h=(double)(base / p2);
			resultado=(double)(pow * h);
			System.out.println("TASA TRE: \n resultado:" + resultado);
			return resultado;
		}
		else{ return 0; }
	}

	public double tasaForward(double tasaCorta, int plazoCorto, double tasaLarga, int plazoLargo, int base)  {
		double mayor=0d;
		double menor=0d;
		double resultado=0d;

		if (plazoLargo > plazoCorto){
			mayor=(1 + tasaLarga * plazoLargo / base);
			System.out.println("TASA FORWARD\nMayor: " + mayor);
			menor=(1 + tasaCorta * plazoCorto / base);
			System.out.println("Menor: " + menor);
			resultado= ((mayor/menor)-1) * base / (plazoLargo - plazoCorto);
			System.out.println("Resultado: " + resultado);
			return resultado;
		}
		else{  return 0; }
	}

	private double frac(double x){
		return x - Math.floor(x);
	}

	private double bono(double valorNominal, int diasVencer, double tasaRendimiento,
		double sobreTasa, double tasaCupon, int plazoCupon, boolean limpio, int base){
		int n = (int)Math.floor(diasVencer / plazoCupon);
		double m = frac(diasVencer / plazoCupon);
		double cuponFuturo = (valorNominal * tasaRendimiento * plazoCupon / base);
		double cuponVigente = (valorNominal * tasaCupon * plazoCupon / base);
		double TR = ((tasaRendimiento + sobreTasa) * plazoCupon / base);
		double vpVN = (valorNominal / (Math.pow((1 + TR), n)));
		double vpCupones = (cuponFuturo * anualidad(n, TR));
		double precioSucio = ((vpVN + vpCupones + cuponVigente) / (Math.pow((1 + TR), m)));

		if (limpio){ return (precioSucio - ((1 - m) * cuponVigente)); }
		else{  return precioSucio; }
	}

/*	public double tasaSpotVector(int plazo, String strNombreCurva, String Fecha, String tipoCurva,  String modoCalculo, int base) throws CotizadorException{
		System.out.println("\nInicia metodo tasaSpotVector() "+new java.util.Date());
		ResultSet rs=null;
		PreparedStatement ps=null;
		int iPlazoMayor=0;
		int iPlazoMenor=0;
		double dtasaMayor=0d;
		double dtasaMenor=0d;
		double resultado=0d;
		boolean tieneMayores=false;
		boolean tieneMenores=false;
		String query="";
		String queryStr="";

		try{
			if (plazo == 0){ resultado = 0; }
			else{
				//Selecciona los registros que sean igual al plazo
				queryStr=*/
					//" SELECT /*+ use_nl(c,d) index(d) rowid(c)*/"   +
			/*		" d.tasa, d.Plazo "   +
					" FROM curva c,datoscurva d"   +
					" WHERE d.idcurva=c.idcurva "   +
					" AND c.Nombre='" + strNombreCurva + "' " +
					" and Fecha='" + Fecha + "' and D.Plazo=" + plazo;
				query=*/
					//" SELECT /*+ use_nl(c,d) index(d) rowid(c)*/"   +
				/*	" d.tasa, d.Plazo "   +
					" FROM curva c,datoscurva d"   +
					" WHERE d.idcurva=c.idcurva "   +
					" AND c.Nombre=? " +
					" and Fecha=? and D.Plazo=?";
				ps=bd.queryPrecompilado(query);
        ps.setString(1,strNombreCurva);
        ps.setString(2,Fecha);
        ps.setInt(3,plazo);
        rs=ps.executeQuery();
				System.out.println(queryStr);

				//SI no hay ningun registro con un plazo Igual
				if (rs.next()){
					resultado=(double)rs.getDouble("Tasa");
          ps.close();
				}else{
          ps.close();
					//Selecciona los registros que sean mayor al plazo
		queryStr = */
				//" SELECT /*+ use_nl(c,d) index(d) rowid(c)*/ "   +
			/*	" d.tasa, d.Plazo "   +
				" FROM curva c,datoscurva d"   +
				" WHERE d.idcurva=c.idcurva "   +
				" AND c.Nombre='" + strNombreCurva + "' " +
				" AND Fecha='" + Fecha + "' " +
				" AND d.Plazo > " + plazo + 
				" ORDER BY Plazo ASC" ;
        query = */
				//" SELECT /*+ use_nl(c,d) index(d) rowid(c)*/ "   +
			/*	" d.tasa, d.Plazo "   +
				" FROM curva c,datoscurva d"   +
				" WHERE d.idcurva=c.idcurva "   +
				" AND c.Nombre=?" +
				" AND Fecha=?" +
				" AND d.Plazo > ?" +
				" ORDER BY Plazo ASC" ;
          ps=bd.queryPrecompilado(query);
          ps.setString(1,strNombreCurva);
          ps.setString(2,Fecha);
          ps.setInt(3,plazo);
          rs=ps.executeQuery();
          System.out.println(queryStr);

					if (rs.next() ) {
						tieneMayores=true;
						iPlazoMayor=rs.getInt("Plazo");
						dtasaMayor=rs.getDouble("Tasa");
						System.out.println("dtasaMayor: "+iPlazoMayor+" dtasaMayor: "+dtasaMayor);
					}
					ps.close();

					//Selecciona los registros que sean menores al plazo
		  queryStr = */
			//	" SELECT /*+ use_nl(c,d) index(d) rowid(c)*/ "   +
			/*	" d.tasa, d.Plazo "   +
				" FROM curva c,datoscurva d"   +
				" WHERE d.idcurva=c.idcurva  "   +
				" AND c.Nombre='" + strNombreCurva + "' " +
				" AND Fecha='" + Fecha + "' " +
				" AND d.Plazo < " + plazo + " ORDER BY Plazo Desc";

          query = */
			//	" SELECT /*+ use_nl(c,d) index(d) rowid(c)*/ "   +
		/*		" d.tasa, d.Plazo "   +
				" FROM curva c,datoscurva d"   +
				" WHERE d.idcurva=c.idcurva  "   +
				" AND c.Nombre=?" +
				" AND Fecha=?" +
				" AND d.Plazo < ? ORDER BY Plazo Desc";		  

          ps=bd.queryPrecompilado(query);
          ps.setString(1,strNombreCurva);
          ps.setString(2,Fecha);
          ps.setInt(3,plazo);
          rs=ps.executeQuery();
          System.out.println(queryStr);

					if (rs.next() ) {
						tieneMenores=true;
						iPlazoMenor=rs.getInt("Plazo");
						dtasaMenor=rs.getDouble("Tasa");
						System.out.println("iPlazoMenor: "+iPlazoMenor+" dtasaMenor: "+dtasaMenor);
					}
					ps.close();

					//Se encuentra en medio del vector
					if (tieneMenores && tieneMayores ){
						if (tipoCurva.equals("DIARIA")){
							System.out.println("Entre en DIARIA");
							resultado = 0;
						}else if (tipoCurva.equals("MUESTRA")){
							if (modoCalculo.equals("ALAMBRADA")){
								double tasa1 = tasaForward(dtasaMenor,iPlazoMenor,dtasaMayor,iPlazoMayor,base);
								double tasa2 = tre(tasa1,(iPlazoMayor - iPlazoMenor), (plazo - iPlazoMenor), base);
								resultado = (double)((1 + dtasaMenor * iPlazoMenor / base) * (1 + tasa2 * (plazo - iPlazoMenor) / base) - 1) * (double)base / plazo;
								System.out.println("Resultado alambrada: " + resultado);
							}else if (modoCalculo.equals("INTERPOLACION")) {
								resultado = (dtasaMayor-dtasaMenor)/(iPlazoMayor-iPlazoMenor)*(plazo-iPlazoMenor)+dtasaMenor;
								System.out.println("Resultado interpolacion: " + resultado);
							}
						}
					}
					//El plazo es mayor a a todos los plazos del vector
					else if (tieneMenores){
						System.out.println("Solo tiene menores");
						resultado = tre(dtasaMenor, iPlazoMenor, plazo, base);
						System.out.println("Resultado solo menores: " + resultado);
					}
					//El plazo es menor a a todos los plazos del vector
					else{
						System.out.println("Solo tiene mayores");
						resultado = tre(dtasaMayor, iPlazoMayor, plazo, base);
						System.out.println("Resultado solo mayores: " + resultado);
					}
				}
			}
		}catch(SQLException e){
			System.out.println("-----------------------");
			System.out.println("FuncionesFinancieras.tasaSpotVector()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("FuncionesFinancieras.tasaSpotVector(): " + e.getMessage());
		}
  System.out.println("\nTermina metodo tasaSpotVector() "+new java.util.Date());
	return resultado;
	}*/

	public ArrayList getPlazosTasaCurva(int plazo, String strNombreCurva, String Fecha, String tipoCurva) throws CotizadorException{
		System.out.println("\nInicia metodo getPlazosTasaCurva() "+new java.util.Date());
		ResultSet rs=null;
		PreparedStatement ps=null;
		String query="";

		double tasaAct		= 0;
		int 	 plazoAct		= 0;
		PlazosTasaCurva	ptc = new PlazosTasaCurva();

		ArrayList	alFilas = new ArrayList();
		try{
				//Selecciona los registros que sean igual al plazo
				query = 
				" SELECT /*+ use_nl(c,d) index(d) rowid(c)*/ d.tasa, d.Plazo"   +
				" FROM curva c,datoscurva d "   +
				" WHERE d.idcurva=c.idcurva  "   +
				" AND c.Nombre=?  "   +
				" AND d.Fecha=? "   +
				" AND D.Plazo<?"+
				" ORDER BY d.plazo";
				
				ps=bd.queryPrecompilado(query);
				ps.setString(1,strNombreCurva);
		      ps.setString(2,Fecha);
	         ps.setInt(3,plazo);
	         rs=ps.executeQuery();
				//SI no hay ningun registro con un plazo Igual
				while (rs.next()){
					plazoAct	= rs.getInt("plazo");
					tasaAct	= rs.getDouble("tasa");

					ptc.setPlazo(plazoAct);
					ptc.setTasa(tasaAct);
					alFilas.add(ptc);
				}
				ps.close();

		}catch(SQLException e){
			System.out.println("-----------------------");
			System.out.println("FuncionesFinancieras.getPlazosTasaCurva()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("FuncionesFinancieras.getPlazosTasaCurva(): " + e.getMessage());
		}
	  System.out.println("\ngetPlazosTasaCurva() "+new java.util.Date());
	return alFilas;
	}



	public double tasaSpotVector(int plazo, String strNombreCurva, String Fecha, String tipoCurva,  String modoCalculo, int base) throws CotizadorException{
		System.out.println("\nInicia metodo tasaSpotVector() "+new java.util.Date());
		ResultSet rs=null;
		PreparedStatement ps=null;
		int iPlazoMayor=0;
		int iPlazoMenor=0;
		double dtasaMayor=0d;
		double dtasaMenor=0d;
		double resultado=0d;
		boolean tieneMayores=false;
		boolean tieneMenores=false;
		String query="";
		String queryStr="";

		try{
			if (plazo == 0){ resultado = 0; }
			else{
				//Selecciona los registros que sean igual al plazo
				query = 
				" SELECT /*+ use_nl(c,d) index(d) rowid(c)*/ d.tasa, d.Plazo,'igual'  "   +
				" FROM curva c,datoscurva d "   +
				" WHERE d.idcurva=c.idcurva  "   +
				" AND c.Nombre=?  "   +
				" AND d.Fecha=? "   +
				" AND D.Plazo=?"   +
				" UNION ALL"   +
				" SELECT * FROM ("   +
				" 	   SELECT /*+ use_nl(c,d) index(d) rowid(c)*/  d.tasa, d.Plazo,'mayor'  "   +
				" 	   FROM curva c,datoscurva d "   +
				" 	   WHERE d.idcurva=c.idcurva  "   +
				" 	   AND c.Nombre=?  "   +
				" 	   AND Fecha=?  "   +
				" 	   AND d.Plazo > ? "   +
				" 	   ORDER BY Plazo ASC"   +
				" ) WHERE ROWNUM = 1"   +
				" UNION ALL"   +
				" SELECT * FROM ("   +
				"        SELECT /*+ use_nl(c,d) index(d) rowid(c)*/  d.tasa, d.Plazo,'menor'  "   +
				" 	   FROM curva c,datoscurva d "   +
				" 	   WHERE d.idcurva=c.idcurva   "   +
				" 	   AND c.Nombre=?  "   +
				" 	   AND Fecha=?  "   +
				" 	   AND d.Plazo < ?  "   +
				" 	   ORDER BY Plazo DESC"   +
				" ) WHERE ROWNUM = 1"  ;
				
				ps=bd.queryPrecompilado(query);
				ps.setString(1,strNombreCurva);
		      ps.setString(2,Fecha);
	         ps.setInt(3,plazo);
				ps.setString(4,strNombreCurva);
		      ps.setString(5,Fecha);
	         ps.setInt(6,plazo);
				ps.setString(7,strNombreCurva);
		      ps.setString(8,Fecha);
	         ps.setInt(9,plazo);
	         rs=ps.executeQuery();
				System.out.println(queryStr);

				//SI no hay ningun registro con un plazo Igual
				while (rs.next()){
					if("igual".equals(rs.getString(3))){
						resultado=(double)rs.getDouble("Tasa");
						break;
					}
					else if("mayor".equals(rs.getString(3))){
						tieneMayores=true;
						iPlazoMayor=rs.getInt("Plazo");
						dtasaMayor=rs.getDouble("Tasa");
						System.out.println("dtasaMayor: "+iPlazoMayor+" dtasaMayor: "+dtasaMayor);					
					}else if("menor".equals(rs.getString(3))){
						tieneMenores=true;
						iPlazoMenor=rs.getInt("Plazo");
						dtasaMenor=rs.getDouble("Tasa");
						System.out.println("iPlazoMenor: "+iPlazoMenor+" dtasaMenor: "+dtasaMenor);
					}
				}
				ps.close();

				//Se encuentra en medio del vector
				if (tieneMenores && tieneMayores ){
						if (tipoCurva.equals("DIARIA")){
							System.out.println("Entre en DIARIA");
							resultado = 0;
						}else if (tipoCurva.equals("MUESTRA")){
							if (modoCalculo.equals("ALAMBRADA")){
								double tasa1 = tasaForward(dtasaMenor,iPlazoMenor,dtasaMayor,iPlazoMayor,base);
								double tasa2 = tre(tasa1,(iPlazoMayor - iPlazoMenor), (plazo - iPlazoMenor), base);
								resultado = (double)((1 + dtasaMenor * iPlazoMenor / base) * (1 + tasa2 * (plazo - iPlazoMenor) / base) - 1) * (double)base / plazo;
								System.out.println("Resultado alambrada: " + resultado);
							}else if (modoCalculo.equals("INTERPOLACION")) {
								resultado = (dtasaMayor-dtasaMenor)/(iPlazoMayor-iPlazoMenor)*(plazo-iPlazoMenor)+dtasaMenor;
								System.out.println("Resultado interpolacion: " + resultado);
							}
						}
					}
					//El plazo es mayor a a todos los plazos del vector
					else if (tieneMenores){
						System.out.println("Solo tiene menores");
						resultado = tre(dtasaMenor, iPlazoMenor, plazo, base);
						System.out.println("Resultado solo menores: " + resultado);
					}
					//El plazo es menor a a todos los plazos del vector
					else if(tieneMayores){
						System.out.println("Solo tiene mayores");
						resultado = tre(dtasaMayor, iPlazoMayor, plazo, base);
						System.out.println("Resultado solo mayores: " + resultado);
					}
			}
		}catch(SQLException e){
			System.out.println("-----------------------");
			System.out.println("FuncionesFinancieras.tasaSpotVector()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("FuncionesFinancieras.tasaSpotVector(): " + e.getMessage());
		}
  System.out.println("\nTermina metodo tasaSpotVector() "+new java.util.Date());
	return resultado;
	}

	public double tasaSpotVector(ArrayList plazosCurva,int plazo, String strNombreCurva, String Fecha, String tipoCurva,  String modoCalculo, int base) throws CotizadorException{
		System.out.println("\nInicia metodo tasaSpotVector() "+new java.util.Date());

		int iPlazoMayor=0;
		int iPlazoMenor=0;
		double dtasaMayor=0d;
		double dtasaMenor=0d;
		double resultado=0d;
		boolean tieneMayores=false;
		boolean tieneMenores=false;
		String query="";
		String queryStr="";

		try{
			if (plazo == 0){ resultado = 0; }
			else{

				for (int i=0;i<plazosCurva.size();i++){
					PlazosTasaCurva ptc = (PlazosTasaCurva)plazosCurva.get(i);
					if(ptc.getPlazo()==plazo){
						resultado=ptc.getTasa();
						break;
					}
					else if(ptc.getPlazo()>plazo&&!tieneMayores){
						tieneMayores=true;
						iPlazoMayor=ptc.getPlazo();
						dtasaMayor=ptc.getTasa();
						System.out.println("dtasaMayor: "+iPlazoMayor+" dtasaMayor: "+dtasaMayor);					
					}else if(ptc.getPlazo()<plazo){
						tieneMenores=true;
						iPlazoMenor=ptc.getPlazo();
						dtasaMenor=ptc.getTasa();
						System.out.println("iPlazoMenor: "+iPlazoMenor+" dtasaMenor: "+dtasaMenor);
					}
				}

				//Se encuentra en medio del vector
				if (tieneMenores && tieneMayores ){
						if (tipoCurva.equals("DIARIA")){
							System.out.println("Entre en DIARIA");
							resultado = 0;
						}else if (tipoCurva.equals("MUESTRA")){
							if (modoCalculo.equals("ALAMBRADA")){
								double tasa1 = tasaForward(dtasaMenor,iPlazoMenor,dtasaMayor,iPlazoMayor,base);
								double tasa2 = tre(tasa1,(iPlazoMayor - iPlazoMenor), (plazo - iPlazoMenor), base);
								resultado = (double)((1 + dtasaMenor * iPlazoMenor / base) * (1 + tasa2 * (plazo - iPlazoMenor) / base) - 1) * (double)base / plazo;
								System.out.println("Resultado alambrada: " + resultado);
							}else if (modoCalculo.equals("INTERPOLACION")) {
								resultado = (dtasaMayor-dtasaMenor)/(iPlazoMayor-iPlazoMenor)*(plazo-iPlazoMenor)+dtasaMenor;
								System.out.println("Resultado interpolacion: " + resultado);
							}
						}
					}
					//El plazo es mayor a a todos los plazos del vector
					else if (tieneMenores){
						System.out.println("Solo tiene menores");
						resultado = tre(dtasaMenor, iPlazoMenor, plazo, base);
						System.out.println("Resultado solo menores: " + resultado);
					}
					//El plazo es menor a a todos los plazos del vector
					else if(tieneMayores){
						System.out.println("Solo tiene mayores");
						resultado = tre(dtasaMayor, iPlazoMayor, plazo, base);
						System.out.println("Resultado solo mayores: " + resultado);
					}
			}
		}catch(Exception e){
			System.out.println("-----------------------");
			System.out.println("FuncionesFinancieras.tasaSpotVector()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("FuncionesFinancieras.tasaSpotVector(): " + e.getMessage());
		}
  System.out.println("\nTermina metodo tasaSpotVector() "+new java.util.Date());
	return resultado;
	}
	


	public double duracionCreditoTasaFijaTipo2(boolean modificada, int plazoCredito, int UTB,
										  double tasaCredito, int numPeriodosInt, double UTBPeriodoInt,
										  double UTBGraciaInt, int numPeriodosCapital, double UTBPeriodoCapital,
										  double UTBGraciaCapital, String UTBSalida, String strNombreCurva,
										  String Fecha, String tipoCurva, String modoCalculo,
										  int base) throws CotizadorException{
	System.out.println("\nInicia metodo duracionCreditoTasaFijaTipo2() "+new java.util.Date());
	double resultado=0;
	if ((plazoCredito <= 0) || (numPeriodosCapital <= 0) || (numPeriodosInt <= 0)){
		return -1;
	}
	if((UTBPeriodoInt<=0)||(UTBPeriodoCapital<=0)||(UTBGraciaInt<0)||(UTBGraciaCapital<0)){
		return -1;
	}
	double UTBsInt = numPeriodosInt * UTBPeriodoInt +UTBGraciaInt;
	if (UTBsInt != (double)plazoCredito){
		return -1;
	}
	double UTBsCapital = numPeriodosCapital * UTBPeriodoCapital + UTBGraciaCapital;
	if (UTBsCapital != (double)plazoCredito){
		return -1;
	}
	int plazoDiasUTB = UTB;

	/* Vector de Capital amortizado por periodo */
	double capitalInicial = 1000000;
	double pagoCapital = (capitalInicial / numPeriodosCapital);
	double capitalAmortizado[] = new double[plazoCredito+1];
	for (int n=0; n <= plazoCredito; n++){
		capitalAmortizado[n] = 0;
	}
	int a = (int)UTBGraciaCapital +(int) UTBPeriodoCapital;
	while (a <= plazoCredito){
		capitalAmortizado[a] = pagoCapital;
		a += UTBPeriodoCapital;
	}
	/* Vector de Saldos insolutos y Vector de Flujos de Efectivo por periodo*/
	double saldoInsoluto[] = new double[plazoCredito+1];
	double VPFlujoEfectivo[] = new double[plazoCredito+1];
	double intAcumulados = 0;
	double intAcumGracia = 0;
	double totalVPFlujos = 0;
	double sumaProdDuracion = 0;
	double interesesPeriodo = 0;
	double amortIntGracia = 0;
	double intAmortizados = 0;
	boolean primeraVez = true;
	double j = UTBGraciaInt + UTBPeriodoInt;
	int plazoControl = 0;
	double factorDescuento = 0;
	double plazoDuracion = 0;
	int plazoDiasUnidadSalida = 0;
	saldoInsoluto[0] = capitalInicial;
  
  System.out.println("\nboolean modificada: "+modificada);
  System.out.println("\nint plazoCredito: "+plazoCredito);
  System.out.println("\nint UTB: "+UTB);
  System.out.println("\ndouble tasaCredito: "+tasaCredito);
  System.out.println("\nint numPeriodosInt: "+numPeriodosInt);
  System.out.println("\ndouble UTBPeriodoInt: "+UTBPeriodoInt);
  System.out.println("\ndouble UTBGraciaInt: "+UTBGraciaInt);
  System.out.println("\nint numPeriodosCapital: "+numPeriodosCapital);
  System.out.println("\ndouble UTBPeriodoCapital: "+UTBPeriodoCapital);
  System.out.println("\ndouble UTBGraciaCapital: "+UTBGraciaCapital);
  System.out.println("\nString UTBSalida: "+UTBSalida);
  System.out.println("\nString strNombreCurva: "+strNombreCurva);
  System.out.println("\nString Fecha: "+Fecha);
  System.out.println("\nString tipoCurva: "+tipoCurva);
  System.out.println("\nString modoCalculo: "+modoCalculo);
  System.out.println("\nint base: "+base);
  System.out.println("\nint plazoDiasUTB: "+plazoDiasUTB);

	ArrayList plazoscurva = getPlazosTasaCurva(plazoCredito*plazoDiasUTB,strNombreCurva, Fecha, tipoCurva);
  
	for (int i = 1; i <= plazoCredito; i++){
		interesesPeriodo = saldoInsoluto[i-1] * tasaCredito * plazoDiasUTB / base;
		if (i <= UTBGraciaInt){
			intAcumGracia += interesesPeriodo;
		}else{
			if (primeraVez){
				amortIntGracia = intAcumGracia / numPeriodosInt;
				primeraVez = false;
			}
			intAcumulados += interesesPeriodo;
		}
		if (i == j){
			intAmortizados = intAcumulados + amortIntGracia;
			intAcumulados = 0;
			j += UTBPeriodoInt;
		}else{
			intAmortizados = 0;
		}
		saldoInsoluto[i]=saldoInsoluto[i-1]*(1+tasaCredito*plazoDiasUTB/base)-capitalAmortizado[i]-intAmortizados;
		System.out.println("i : " + i);
		plazoControl = i * plazoDiasUTB;
		factorDescuento = (1 + tasaSpotVector(plazoscurva,plazoControl, strNombreCurva, Fecha, tipoCurva, modoCalculo, base) * plazoControl / base);

		VPFlujoEfectivo[i] = (capitalAmortizado[i] + intAmortizados) / factorDescuento;

		System.out.println("intAmortizados : " + intAmortizados);
		System.out.println("capitalAmortizado : " + capitalAmortizado[i]);
		totalVPFlujos += VPFlujoEfectivo[i];
		if (modificada){
			plazoDuracion = plazoControl / factorDescuento;
		}else{
			plazoDuracion = plazoControl;
		}
		sumaProdDuracion += (plazoDuracion * VPFlujoEfectivo[i]);
	}
	if (UTBSalida.equals("")){
		plazoDiasUnidadSalida = plazoDiasUTB;
	}else{
		plazoDiasUnidadSalida = diasEquivalentesUnidadTiempo(UTBSalida);
	}
	resultado = (sumaProdDuracion / totalVPFlujos) / plazoDiasUnidadSalida;
	System.out.println("sumaProdDuracion : " + sumaProdDuracion);
	System.out.println("totalVPFlujos : " + totalVPFlujos);
	System.out.println("plazoDiasUnidadSalida : " + plazoDiasUnidadSalida);

  System.out.println("\nTermina metodo duracionCreditoTasaFijaTipo2() "+new java.util.Date());
	return resultado;
	}

	public double duracionCreditoTasaFijaTipo3(boolean modificada, int plazoCredito,
										  int UTB, double tasaCredito, int UTBGracia, String UTBSalida,
										  String strNombreCurva, String Fecha, String tipoCurva,
										  String modoCalculo, int base) throws CotizadorException{
		/* Verificacion de parametros */
    System.out.println("\nInicia metodo duracionCreditoTasaFijaTipo3() "+new java.util.Date());
		if ((plazoCredito <= 0 )||(UTBGracia < 0)||(UTBGracia >= plazoCredito)){
			return -1;
		}
		/* C�lculo de la duraci�n del Cr�dito */
		double VPFlujoEfectivo[] = new double[plazoCredito+1];
		double plazoDiasUnidadSalida = 0;

		int plazoDiasUTB = UTB;
		double capitalInicial = 1000000;
		double anuDiferida = 0;
		double anuTotal = anualidad(plazoCredito, (tasaCredito*plazoDiasUTB/base));
		if (UTBGracia > 0){
			anuDiferida = anualidad(UTBGracia, (tasaCredito*plazoDiasUTB/base));
		}else{
			anuDiferida = 0;
		}
		double pago = capitalInicial/(anuTotal - anuDiferida);
		double totalVPFlujos = 0;
		double sumaProductoDuracion = 0;
		int plazoControl = 0;
		double factorDescuento = 0;
		double plazoDuracion = 0;

		ArrayList plazoscurva = getPlazosTasaCurva(plazoCredito*plazoDiasUTB,strNombreCurva, Fecha, tipoCurva);

		for (int i=UTBGracia+1; i<=plazoCredito; i++){
			plazoControl = i * plazoDiasUTB;
			factorDescuento=(1+tasaSpotVector(plazoscurva,plazoControl, strNombreCurva, Fecha,tipoCurva,modoCalculo,base)*plazoControl/base);

			VPFlujoEfectivo[i] = pago/factorDescuento;
			totalVPFlujos += VPFlujoEfectivo[i];
			if (modificada){
				plazoDuracion = plazoControl / factorDescuento;
			}else{
				plazoDuracion = plazoControl;
			}
			sumaProductoDuracion += (plazoDuracion * VPFlujoEfectivo[i]);

			System.out.println("i : " + i);
			System.out.println("plazoDuracion : " + plazoDuracion);
			System.out.println("VPFlujoEfectivo : " + VPFlujoEfectivo[i]);
			System.out.println("sumaProductoDuracion : " + sumaProductoDuracion);
		}
		if (UTBSalida.equals("")){
			plazoDiasUnidadSalida = plazoDiasUTB;
		}else{
			plazoDiasUnidadSalida = diasEquivalentesUnidadTiempo(UTBSalida);
		}

		System.out.println("sumaProductoDuracion : " + sumaProductoDuracion);
		System.out.println("totalVPFlujos : " + totalVPFlujos);
		System.out.println("plazoDiasUnidadSalida : " + plazoDiasUnidadSalida);

		//Double Duracion= new Double((sumaProductoDuracion/totalVPFlujos)/plazoDiasUnidadSalida);
		//int resultado=Duracion.intValue();
		double resultado = (sumaProductoDuracion/totalVPFlujos)/plazoDiasUnidadSalida;

		System.out.println("resultado : " + resultado);
    System.out.println("\nTermina metodo duracionCreditoTasaFijaTipo3() "+new java.util.Date());
	return resultado;
	}

	public int diasEquivalentesUnidadTiempo(String unidadTiempo){
		int dias = 0;
		if (unidadTiempo.equals("dias")) dias = 1;
		if (unidadTiempo.equals("semanas"))dias = 7;
		if (unidadTiempo.equals("quincenas")) dias = 15;
		if (unidadTiempo.equals("meses")) dias = 30;
		if (unidadTiempo.equals("bimestres")) dias = 60;
		if (unidadTiempo.equals("trimestres")) dias = 90;
		if (unidadTiempo.equals("cuatrimestres")) dias = 120;
		if (unidadTiempo.equals("semestres")) dias = 180;
		if (unidadTiempo.equals("a�os")) dias = 360;

	return dias;
	}

	public double obtenFactorRiesgo(String pstrTipoIntermediario, int piPlazo) throws CotizadorException{
	ResultSet rsFactoresAplicables=null;
	double factorRiesgo=0d;
	String query="";

	try{
		//Busca el registro con un plazo mayor o igual al que se pas� de par�metro//
		query = "SELECT plazo, riesgo " +
				"FROM   factoresriesgo a, tipointermediario b " +
				"WHERE  a.idTipoIntermediario = b.idTipoIntermediario " +
				"AND    b.descripcion LIKE \'" + pstrTipoIntermediario + "\' " +
				"AND    a.plazo>=" + piPlazo + " " +
				"ORDER BY a.plazo";
		rsFactoresAplicables=bd.queryDB(query);
		//Si no hay un registro con un plazo mayor o igual al par�metro
		if (!rsFactoresAplicables.next()){
			//Busca el registro con un plazo menor al que se pas� como par�metro
			query = "SELECT plazo, riesgo " +
					"FROM   factoresriesgo a, tipointermediario b " +
					"WHERE  a.idTipoIntermediario = b.idTipoIntermediario " +
					"AND    b.descripcion LIKE \'" + pstrTipoIntermediario + "\' " +
					"AND    a.plazo<" + piPlazo + " " +
					"ORDER BY a.plazo Desc";
			bd.cierraStatement();
			rsFactoresAplicables=bd.queryDB(query);
			if (!rsFactoresAplicables.next())
				throw new CotizadorException("El cat�logo de factores de riesgo est� vac�o");
		}
		factorRiesgo=rsFactoresAplicables.getDouble("riesgo");
		bd.cierraStatement();
	}catch(SQLException e){
		System.out.println("-----------------------");
		System.out.println("FuncionesFinancieras.obtenFactorResigo()");
		e.printStackTrace();
		System.out.println("-----------------------");
		throw new CotizadorException("FuncionesFinancieras.obtenFactorResigo(): " + e.getMessage());
	}
	return factorRiesgo;
	}
} // fin de la clase.							"AND d.Plazo < " + plazo + " ORDER BY Plazo Desc";


class PlazosTasaCurva{

	private int plazo			= 0;
	private double tasa		= 0;

	public void setPlazo(int iplazo){
		this.plazo = iplazo;
	}
	public void setTasa(double dtasa ){
		this.tasa = dtasa;
	}

	public int getPlazo(){
		return this.plazo;
	}
	public double getTasa(){
		return this.tasa;
	}
}