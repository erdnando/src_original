package netropology.utilerias.negocio;

import java.util.Hashtable;

public class ValidaParamBean{
	private String strHoraIniConsultaHr="";
	private String strHoraIniConsultaMin="";
	private String strHoraFinConsultaHr="";
	private String strHoraFinConsultaMin="";
	private String strHoraIniConfirmacionHr="";
	private String strHoraIniConfirmacionMin="";
	private String strHoraFinConfirmacionHr="";
	private String strHoraFinConfirmacionMin="";
	private String strMontoLimiteExternos="";
	private String strMontoLimiteExternosDl="";
	private String strPlazoLimiteExternos="";
	private String strPlazoLimiteExternosDl="";
	private String strEstaBloqueado="";
	private String strEstaBloqueadoDl="";	
	private Hashtable error=new Hashtable();
	private String strIg_plazo_max_oper=""; //foda 10-2005-smj
	private String strIg_plazo_max_oper_dl="";
	private String strIg_num_max_disp="";
	private String strIg_num_max_disp_dl="";
	private String strIg_tasa_impuesto="";
	private String strIg_tasa_impuesto_dl="";
	private String strIg_decimales=""; 
	private String strIg_dias_solic_rec="";
	private String strIg_dias_solic_rec_dl="";

	private String strCg_metodo_inter="";
	private String strCg_metodo_inter_dl="";
	private String strCg_metodo_calc="";
	private String strCg_metodo_calc_dl="";

	private double 	ig_techo_tasa_p;
	private int 	ig_plazo_max_cot; 
	private int 	ig_num_max_disp_cot; 
	private double 	ig_monto_max_cot;
	private int 	ig_plazo_max_cupon; 
	
	private int 	ig_plazo_max_cot_dl; 
	private int 	ig_num_max_disp_cot_dl; 
	private double 	ig_monto_max_cot_dl;
	private int 	ig_plazo_max_cupon_dl; 
	
	private String 	cg_correo; 
	private String 	cg_correo_dl; 
	
	private String 	cg_tasa_f		= "N"; 
	private String 	cg_tasa_f_dl	= "N"; 
	private String 	cg_tasa_v		= "N"; 
	private String 	cg_tasa_v_dl	= "N"; 

	public boolean valida(){
		boolean datosCorrectos=true;
    
		if (strHoraIniConsultaHr.equals("") || strHoraIniConsultaMin.equals("") ){
			error.put("horaIniConsultaHr", "Proporcione una hora para el inicio de la consulta, por favor");
			//error.put("horaIniConsultaMin", "Proporcione una hora para el inicio de la consulta, por favor");      
			strHoraIniConsultaHr="0";
			strHoraIniConsultaMin="0";
			datosCorrectos=false;
		}else{
			try{
				new Integer(strHoraIniConsultaHr).intValue(); 
				new Integer(strHoraIniConsultaMin).intValue(); 
			}
			catch(Exception e){
				error.put("horaIniConsultaHr", "Proporcione la hora correcta, por favor");
				strHoraIniConsultaHr="0";
				strHoraIniConsultaMin="0";
				datosCorrectos=false;
			}
		}
		if (strHoraFinConsultaHr.equals("") || strHoraFinConsultaMin.equals("") ){
			error.put("horaFinConsultaHr", "Proporcione una hora para el t�rmino de la consulta, por favor");
			//error.put("horaFinConsultaMin", "Proporcione una hora para el t�rmino de la consulta, por favor");      
			strHoraFinConsultaHr="0";
			strHoraFinConsultaMin="0";
			datosCorrectos=false;
		}else{
			try{ 
				new Integer(strHoraFinConsultaHr).intValue(); 
				new Integer(strHoraFinConsultaMin).intValue(); 
			}
			catch(Exception e){
				error.put("horaFinConsultaHr", "Proporcione la hora correcta, por favor");
				strHoraFinConsultaHr="0";
				strHoraFinConsultaMin="0";
				datosCorrectos=false;
			}
		}
		if (strHoraIniConfirmacionHr.equals("") || strHoraIniConfirmacionMin.equals("") ){
			error.put("horaIniConfirmacionHr", "Proporcione una hora para el inicio de la confirmaci�n, por favor");
			//error.put("horaIniConfirmacionMin", "Proporcione l para el inicio de la consulta, por favor");
			strHoraIniConfirmacionHr="0";
			strHoraIniConfirmacionMin="0";
			datosCorrectos=false;
		}else{
			try{ 
				new Integer(strHoraIniConfirmacionHr).intValue(); 
				new Integer(strHoraIniConfirmacionMin).intValue(); 
			}
			catch(Exception e){
				error.put("strHoraIniConfirmacionHr", "Proporcione la hora correcta, por favor");
				strHoraIniConfirmacionHr="0";
				strHoraIniConfirmacionMin="0";
				datosCorrectos=false;
			}
		}
		if (strHoraFinConfirmacionHr.equals("") || strHoraFinConfirmacionMin.equals("") ){
			error.put("horaFinConfirmacionHr", "Proporcione una hora para el t�rmino de la confirmaci�n, por favor");
			//error.put("horaFinConfirmacionMin", "Proporcione una hora para el t�rmino de la consulta, por favor");
			strHoraFinConfirmacionHr="0";
			strHoraFinConfirmacionMin="0";
			datosCorrectos=false;
		}else{
			try{ 
				new Integer(strHoraFinConfirmacionHr).intValue(); 
				new Integer(strHoraFinConfirmacionMin).intValue(); 
			}
			catch(Exception e){
				error.put("strHoraIniConfirmacionHr", "Proporcione la hora correcta, por favor");
				strHoraFinConfirmacionHr="0";
				strHoraFinConfirmacionMin="0";
				datosCorrectos=false;
			}
		}
  
   
		if (strMontoLimiteExternos.equals("")){
			error.put("montoLimite", "Proporcione el monto l�mite para externos en Moneda Nacional, por favor");
			strMontoLimiteExternos="0";
			datosCorrectos=false;
		}else{

    
			try{ double m= new Double(strMontoLimiteExternos).doubleValue(); }
			catch(Exception e){
				error.put("montoLimite", "Proporcione una cantidad v�lida para el monto en Moneda Nacional, por favor");
				strMontoLimiteExternos="0";
				datosCorrectos=false;

  
			}
		}

		if (strMontoLimiteExternosDl.equals("")){
			error.put("montoLimite", "Proporcione el monto l�mite para externos en Dolares, por favor");
			strMontoLimiteExternosDl="0";
			datosCorrectos=false;
		}else{

    
			try{ double m= new Double(strMontoLimiteExternosDl).doubleValue(); }
			catch(Exception e){
				error.put("montoLimite", "Proporcione una cantidad v�lida para el monto en Dolares, por favor");
				strMontoLimiteExternosDl="0";
				datosCorrectos=false;

  
			}
		}		

    
		if (strPlazoLimiteExternos.equals("")){
			error.put("plazoLimite", "Proporcione un plazo l�mite para externos en Moneda Nacional, por favor");
			strPlazoLimiteExternos="0";
			datosCorrectos=false;
		}else{
			try{ int i = new Integer(strPlazoLimiteExternos).intValue(); }
			catch(Exception e){
				error.put("plazoLimite", "Proporcione una cantidad v�lida para el plazo l�mite, por favor");
				strPlazoLimiteExternos="0";
				datosCorrectos=false;
			}
		}

		if (strPlazoLimiteExternosDl.equals("")){
			error.put("plazoLimite", "Proporcione un plazo l�mite para externos en dolares, por favor");
			strPlazoLimiteExternosDl="0";
			datosCorrectos=false;
		}else{
			try{ int i = new Integer(strPlazoLimiteExternosDl).intValue(); }
			catch(Exception e){
				error.put("plazoLimite", "Proporcione una cantidad v�lida para el plazo l�mite para externos en dolares, por favor");
				strPlazoLimiteExternosDl="0";
				datosCorrectos=false;
			}
		}		
		
		 //foda 10-2005-smj
		if (strIg_plazo_max_oper.equals("")){
			error.put("ig_plazo_max_oper", "Proporcione un plazo m�ximo en  moneda nacional, por favor");
			strIg_plazo_max_oper="0";
			datosCorrectos=false;
		}else{
    
			try{ 
          int i = new Integer(strIg_plazo_max_oper).intValue()  ; 
          if(i<0)
          {
            error.put("ig_plazo_max_oper", "El n�mero no debe ser negativo");
            datosCorrectos=false;
          }
          
      }
			catch(Exception e){
				error.put("ig_plazo_max_oper", "Proporcione una cantidad v�lida para el plazo m�ximo en moneda nacional, por favor");
				strIg_plazo_max_oper="0";
				datosCorrectos=false;
			}
		}

		if (strIg_plazo_max_oper_dl.equals("")){
			error.put("ig_plazo_max_oper_dl", "Proporcione un plazo m�ximo en dolares, por favor");
			strIg_plazo_max_oper_dl="0";
			datosCorrectos=false;
		}else{
    
			try{ 
          int i = new Integer(strIg_plazo_max_oper_dl).intValue()  ; 
          if(i<0)
          {
            error.put("ig_plazo_max_oper_dl", "El n�mero no debe ser negativo");
            datosCorrectos=false;
          }
          
      }
			catch(Exception e){
				error.put("ig_plazo_max_oper_dl", "Proporcione una cantidad v�lida para el plazo m�ximo en dolares, por favor");
				strIg_plazo_max_oper_dl="0";
				datosCorrectos=false;
			}
		}
		
		if (strIg_num_max_disp.equals("")){
			error.put("ig_num_max_disp", "Proporcione un n�mero m�ximo de disposiciones en moneda nacional, por favor");
			strIg_num_max_disp="0";
			datosCorrectos=false;
		}else{
			try{ 
	          int i = new Integer(strIg_num_max_disp).intValue();
	          if(i<0)
	          {
	            error.put("ig_num_max_disp", "El n�mero no debe ser negativo");
	            datosCorrectos=false;
	          }
      		} catch(Exception e){
				error.put("ig_num_max_disp", "Proporcione una cantidad v�lida para el n�mero m�ximo, por favor");
				strIg_num_max_disp="0";
				datosCorrectos=false;
			}
		}

		if (strIg_num_max_disp_dl.equals("")){
			error.put("ig_num_max_disp_dl", "Proporcione un n�mero m�ximo de disposiciones en dolares, por favor");
			strIg_num_max_disp_dl="0";
			datosCorrectos=false;
		}else{
			try{ 
	          int i = new Integer(strIg_num_max_disp_dl).intValue();
	          if(i<0)
	          {
	            error.put("ig_num_max_disp_dl", "El n�mero no debe ser negativo");
	            datosCorrectos=false;
	          }
      		} catch(Exception e){
				error.put("ig_num_max_disp_dl", "Proporcione una cantidad v�lida para el n�mero m�ximo de disposiciones en dolares, por favor");
				strIg_num_max_disp_dl="0";
				datosCorrectos=false;
			}
		}		

		if (!strIg_tasa_impuesto.equals("")){
			try{ 
				double t= new Double(strIg_tasa_impuesto).doubleValue();
				if(t<0){
		            error.put("ig_tasa_impuesto", "El n�mero no debe ser negativo");
		            datosCorrectos=false;				
				}
			} catch(Exception e){
				error.put("ig_tasa_impuesto", "Proporcione una cantidad v�lida para la tasa de impuesto, por favor");
				strIg_tasa_impuesto="0";
				datosCorrectos=false;
			}
			
			if(datosCorrectos){
				if(strIg_tasa_impuesto.indexOf(".")<0){
					if(strIg_tasa_impuesto.length() > 2){
						error.put("ig_tasa_impuesto", "La tasa debe tener m�ximo 2 enteros");
						strIg_tasa_impuesto="0";
						datosCorrectos=false;
					}
				} else {
					int indice = strIg_tasa_impuesto.indexOf(".");
					String tasaInt = strIg_tasa_impuesto.substring(0,indice);
					String tasaDec = strIg_tasa_impuesto.substring(indice+1,strIg_tasa_impuesto.length());
					if(tasaInt.length() > 2){
						error.put("ig_tasa_impuesto", "La tasa debe tener m�ximo 2 enteros");
						strIg_tasa_impuesto="0.0";
						datosCorrectos=false;
					} else if(tasaDec.length() > 4){
						error.put("ig_tasa_impuesto", "La tasa debe tener m�ximo 4 decimales");
						strIg_tasa_impuesto="0.0";
						datosCorrectos=false;
					}
				}
			}
		}

		if (!strIg_tasa_impuesto_dl.equals("")){
			try{ 
				double t= new Double(strIg_tasa_impuesto_dl).doubleValue();
				if(t<0){
		            error.put("ig_tasa_impuesto_dl", "El n�mero no debe ser negativo");
		            datosCorrectos=false;				
				}
			} catch(Exception e){
				error.put("ig_tasa_impuesto_dl", "Proporcione una cantidad v�lida para la tasa de impuesto en d�lares, por favor");
				strIg_tasa_impuesto_dl="0";
				datosCorrectos=false;
			}
			
			if(datosCorrectos){
				if(strIg_tasa_impuesto_dl.indexOf(".")<0){
					if(strIg_tasa_impuesto_dl.length() > 2){
						error.put("ig_tasa_impuesto_dl", "La tasa debe tener m�ximo 2 enteros");
						strIg_tasa_impuesto_dl="0";
						datosCorrectos=false;
					}
				} else {
					int indice = strIg_tasa_impuesto_dl.indexOf(".");
					String tasaInt = strIg_tasa_impuesto_dl.substring(0,indice);
					String tasaDec = strIg_tasa_impuesto_dl.substring(indice+1,strIg_tasa_impuesto_dl.length());
					if(tasaInt.length() > 2){
						error.put("ig_tasa_impuesto_dl", "La tasa debe tener m�ximo 2 enteros");
						strIg_tasa_impuesto_dl="0.0";
						datosCorrectos=false;
					} else if(tasaDec.length() > 4){
						error.put("ig_tasa_impuesto_dl", "La tasa debe tener m�ximo 4 decimales");
						strIg_tasa_impuesto_dl="0.0";
						datosCorrectos=false;
					}
				}
			}
		}		

    
	  /*if (!strIg_decimales.equals("")){
			try{ 
				int i = new Integer(strIg_decimales).intValue(); 
				if(i<0){
		            error.put("ig_decimales", "El n�mero no debe ser negativo");
		            datosCorrectos=false;				
				}
			} catch(Exception e){
				error.put("ig_decimales", "Proporcione una cantidad v�lida para el n�mero de decimales, por favor");
				strIg_decimales="0";
				datosCorrectos=false;
			}
		}*/

		if (strIg_dias_solic_rec.equals("")){
			error.put("ig_dias_solic_rec", "Proporcione un n�mero m�ximo de dias habiles en moneda nacional, por favor");
			strIg_dias_solic_rec="0";
			datosCorrectos=false;
		}else{
			try{ 
	          int i = new Integer(strIg_dias_solic_rec).intValue();
	          if(i<0)
	          {
	            error.put("ig_dias_solic_rec", "El n�mero no debe ser negativo");
	            datosCorrectos=false;
	          }
      		} catch(Exception e){
				error.put("strIg_dias_solic_rec", "Proporcione una cantidad v�lida para el n�mero m�ximode dias h�biles en moneda nacional, por favor");
				strIg_dias_solic_rec="0";
				datosCorrectos=false;
			}
		}

		if (strIg_dias_solic_rec_dl.equals("")){
			error.put("ig_dias_solic_rec_dl", "Proporcione un n�mero m�ximo de dias habiles en dolares, por favor");
			strIg_dias_solic_rec_dl="0";
			datosCorrectos=false;
		}else{
			try{ 
	          int i = new Integer(strIg_dias_solic_rec_dl).intValue();
	          if(i<0)
	          {
	            error.put("ig_dias_solic_rec_dl", "El n�mero no debe ser negativo");
	            datosCorrectos=false;
	          }
      		} catch(Exception e){
				error.put("strIg_dias_solic_rec_dl", "Proporcione una cantidad v�lida para el n�mero m�ximode dias h�biles en dolares, por favor");
				strIg_dias_solic_rec_dl="0";
				datosCorrectos=false;
			}
		}		

	System.out.println(datosCorrectos);		
	return datosCorrectos;
  
	}

	public String getMensajeError(String s) {
  		
		String mensajeError =(String)error.get(s.trim());
    
		return (mensajeError == null) ? "":mensajeError;
   
	}
	public String getHoraIniConsultaHr(){
  System.out.println("Entra al getHora");	
		return strHoraIniConsultaHr;
	}
	public String getHoraIniConsultaMin(){
		return strHoraIniConsultaMin;
	}
	public String getHoraFinConsultaHr(){
		return strHoraFinConsultaHr;
	}
	public String getHoraFinConsultaMin(){
		return strHoraFinConsultaMin;
	}
	public String getHoraIniConfirmacionHr(){
		return strHoraIniConfirmacionHr;
	}
	public String getHoraIniConfirmacionMin(){
		return strHoraIniConfirmacionMin;
	}
	public String getHoraFinConfirmacionHr(){
		return strHoraFinConfirmacionHr;
	}
	public String getHoraFinConfirmacionMin(){
		return strHoraFinConfirmacionMin;
	}
	public String getMontoLimite(){
  		return strMontoLimiteExternos;  
	}
	public String getMontoLimiteDl(){
  		return strMontoLimiteExternosDl;  
	}	
	public String getPlazoLimite(){
		return strPlazoLimiteExternos;
	}
	public String getPlazoLimiteDl(){
		return strPlazoLimiteExternosDl;
	}	
	public String getStatusSistema(){
		return strEstaBloqueado;
	}
	public String getStatusSistemaDl(){
		return strEstaBloqueadoDl;
	}
	
	//foda 10-smj
	
	public String getIg_plazo_max_oper(){
		return strIg_plazo_max_oper;
	}

	public String getIg_plazo_max_oper_dl(){
		return strIg_plazo_max_oper_dl;
	}	
	
	public String getIg_num_max_disp(){
		return strIg_num_max_disp;
	}

	public String getIg_num_max_disp_dl(){
		return strIg_num_max_disp_dl;
	}	

	public String getIg_tasa_impuesto(){
    	return strIg_tasa_impuesto;
 	}	

	public String getIg_tasa_impuesto_dl(){
    	return strIg_tasa_impuesto_dl;
 	}	
	
	public String getIg_decimales(){
		return strIg_decimales;
	}

 	public String getIg_dias_solic_rec(){
    	return strIg_dias_solic_rec;
 	}		
 	public String getIg_dias_solic_rec_dl(){
    	return strIg_dias_solic_rec_dl;
 	}			
	
//Comienzan SET	
	
	
	public void setHoraIniConsultaHr(String s){
		strHoraIniConsultaHr=s;
	}
	public void setHoraIniConsultaMin(String s){
		strHoraIniConsultaMin=s;
	}
	public void setHoraFinConsultaHr(String s){
		strHoraFinConsultaHr=s;
	}
	public void setHoraFinConsultaMin(String s){
		strHoraFinConsultaMin=s;
	}
	public void setHoraIniConfirmacionHr(String s){
		strHoraIniConfirmacionHr=s;
	}
	public void setHoraIniConfirmacionMin(String s){
		strHoraIniConfirmacionMin=s;
	}
	public void setHoraFinConfirmacionHr(String s){
		strHoraFinConfirmacionHr=s;
	}
	public void setHoraFinConfirmacionMin(String s){
		strHoraFinConfirmacionMin=s;
	}
  public void setMontoLimite(String s){
		strMontoLimiteExternos=s; 
	}
  public void setMontoLimiteDl(String s){
		strMontoLimiteExternosDl=s; 
	}	
	public void setPlazoLimite(String s){
		strPlazoLimiteExternos=s;
	}
	public void setPlazoLimiteDl(String s){
		strPlazoLimiteExternosDl=s;
	}	
	public void setStatusSistema(String s){
		strEstaBloqueado=s;
	}
	public void setStatusSistemaDl(String s){
		strEstaBloqueadoDl=s;
	}
	
	public void setError(String llave, String mensaje) {
		error.put(llave,mensaje);
	}  
	
	//foda 10-smj
	public void setIg_plazo_max_oper(String s){
		strIg_plazo_max_oper=s;
	}

	public void setIg_plazo_max_oper_dl(String s){
		strIg_plazo_max_oper_dl=s;
	}
	
	public void setIg_num_max_disp(String s){
		strIg_num_max_disp=s; 	
	}

	public void setIg_num_max_disp_dl(String s){
		strIg_num_max_disp_dl=s; 	
	}	
  
	public void setIg_tasa_impuesto_dl(String s){
		strIg_tasa_impuesto_dl = s;
	}

	public void setIg_tasa_impuesto(String s){
		strIg_tasa_impuesto = s;
	}
	
 	public void setIg_decimales(String s){
    	strIg_decimales = s;
 	}	

 	public void setIg_dias_solic_rec(String s){
    	strIg_dias_solic_rec = s;
 	}		
 	public void setIg_dias_solic_rec_dl(String s){
    	strIg_dias_solic_rec_dl = s;
 	}		
	
	public void setCg_metodo_calc(String val){
		strCg_metodo_calc = val;
	}
	public void setCg_metodo_calc_dl(String val){
		strCg_metodo_calc_dl = val;
	}

	public String getCg_metodo_calc(){
		return strCg_metodo_calc;
	}
	public String getCg_metodo_calc_dl(){
		return strCg_metodo_calc_dl;
	}

	public void setCg_metodo_inter(String val){
		strCg_metodo_inter = val;
	}
	public void setCg_metodo_inter_dl(String val){
		strCg_metodo_inter_dl = val;
	}

	public String getCg_metodo_inter(){
		return strCg_metodo_inter;
	}
	public String getCg_metodo_inter_dl(){
		return strCg_metodo_inter_dl;
	}

	//SET
	public void setIg_techo_tasa_p(double ig_techo_tasa_p){
		this.ig_techo_tasa_p = ig_techo_tasa_p;
	}
	public void setIg_plazo_max_cot(int ig_plazo_max_cot){
		this.ig_plazo_max_cot = ig_plazo_max_cot;
	}
	public void setIg_num_max_disp_cot(int ig_num_max_disp_cot){
		this.ig_num_max_disp_cot = ig_num_max_disp_cot;
	}
	public void setIg_monto_max_cot(double ig_monto_max_cot){
		this.ig_monto_max_cot = ig_monto_max_cot;
	}
	public void setig_plazo_max_cupon(int ig_plazo_max_cupon){
		this.ig_plazo_max_cupon = ig_plazo_max_cupon;
	}
	
	public void setIg_plazo_max_cot_dl(int ig_plazo_max_cot_dl){
		this.ig_plazo_max_cot_dl = ig_plazo_max_cot_dl;
	}
	public void setIg_num_max_disp_cot_dl(int ig_num_max_disp_cot_dl){
		this.ig_num_max_disp_cot_dl = ig_num_max_disp_cot_dl;
	}
	public void setIg_monto_max_cot_dl(double ig_monto_max_cot_dl){
		this.ig_monto_max_cot_dl = ig_monto_max_cot_dl;
	}
	public void setig_plazo_max_cupon_dl(int ig_plazo_max_cupon_dl){
		this.ig_plazo_max_cupon_dl = ig_plazo_max_cupon_dl;
	}
	
	public void setCg_correo(String cg_correo){
		this.cg_correo = cg_correo;
	}
	public void setCg_correo_dl(String cg_correo_dl){
		this.cg_correo_dl = cg_correo_dl;
	}
	
	public void setCg_tasa_f(String cg_tasa_f){
		this.cg_tasa_f = cg_tasa_f;
	}
	
	public void setCg_tasa_f_dl(String cg_tasa_f_dl){
		this.cg_tasa_f_dl = cg_tasa_f_dl;
	}
	
	public void setCg_tasa_v(String cg_tasa_v){
		this.cg_tasa_v = cg_tasa_v;
	}
	
	public void setCg_tasa_v_dl(String cg_tasa_v_dl){
		this.cg_tasa_v_dl = cg_tasa_v_dl;
	}
	
	//GET
	public double getIg_techo_tasa_p() {
		return this.ig_techo_tasa_p;
	}
	public int getIg_plazo_max_cot() {
		return this.ig_plazo_max_cot;
	}
	public int getIg_num_max_disp_cot() {
		return this.ig_num_max_disp_cot;
	}
	public double getIg_monto_max_cot() {
		return this.ig_monto_max_cot;
	}
	public int getIg_plazo_max_cupon() {
		return this.ig_plazo_max_cupon;
	}
	
	public int getIg_plazo_max_cot_dl() {
		return this.ig_plazo_max_cot_dl;
	}
	public int getIg_num_max_disp_cot_dl() {
		return this.ig_num_max_disp_cot_dl;
	}
	public double getIg_monto_max_cot_dl() {
		return this.ig_monto_max_cot_dl;
	}
	public int getIg_plazo_max_cupon_dl() {
		return this.ig_plazo_max_cupon_dl;
	}
	
	public String getCg_correo() {
		return this.cg_correo;
	}
	public String getCg_correo_dl() {
		return this.cg_correo_dl;
	}
	
	public String getCg_tasa_f(){
		return this.cg_tasa_f;
	}
	
	public String getCg_tasa_f_dl(){
		return this.cg_tasa_f_dl;
	}
	
	public String getCg_tasa_v(){
		return this.cg_tasa_v;
	}
	
	public String getCg_tasa_v_dl(){
		return this.cg_tasa_v_dl;
	}
	
}