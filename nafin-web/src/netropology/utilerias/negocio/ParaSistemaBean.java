package netropology.utilerias.negocio;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ParaSistemaBean{
    
    private static final Log LOG = ServiceLocator.getInstance().getLog(ParaSistemaBean.class);
    
	private AccesoDB cbd = new AccesoDB();
	private ResultSet cRS;
	private java.util.Calendar cdtmHoraInicioConsulta 		=  Calendar.getInstance();
	private java.util.Calendar cdtmHoraTerminoConsulta 		=  Calendar.getInstance();
	private java.util.Calendar cdtmHoraInicioConfirmacion 	=  Calendar.getInstance();
	private java.util.Calendar cdtmHoraTerminoConfirmacion 	=  Calendar.getInstance();
	private double  cdblMontoLimiteExternos;
	private double  cdblMontoLimiteExternosDl;
	private int cintPlazoLimiteExternos;
	private int cintPlazoLimiteExternosDl;
	private boolean cbolSistemaBloqueado;
	private boolean cbolSistemaBloqueadoDl;
	private boolean existeRegistro; 

	//Foda 10-2005-SMJ
	private int ig_plazo_max_oper; 
	private int ig_num_max_disp;
	private double ig_tasa_impuesto_dl;
	private int ig_decimales;	
	
	private double 	ig_techo_tasa_p;
	private int 	ig_plazo_max_cot; 
	private int 	ig_num_max_disp_cot; 
	private double 	ig_monto_max_cot;
	private int 	ig_plazo_max_cupon; 
	
	private int 	ig_plazo_max_cot_dl; 
	private int 	ig_num_max_disp_cot_dl; 
	private double 	ig_monto_max_cot_dl;
	private int 	ig_plazo_max_cupon_dl; 
	
	private String 	cg_correo; 
	private String 	cg_correo_dl; 
	private String 	cg_tasa_f		= "N"; 
	private String 	cg_tasa_f_dl	= "N"; 
	private String 	cg_tasa_v		= "N"; 
	private String 	cg_tasa_v_dl	= "N"; 
	
	//Foda 08-2006
	private int ig_plazo_max_oper_dl; 
	private int ig_num_max_disp_dl;
	private double ig_tasa_impuesto;
	private int ig_dias_solic_rec;
	private int ig_dias_solic_rec_dl;
	private String cg_metodo_inter;
	private String cg_metodo_inter_dl;
	private String cg_metodo_calc;
	private String cg_metodo_calc_dl;
	private List	 lEsquemasRecup;
	private String cs_disponible_if_mn[];
	private String cs_disponible_if_dl[];
  	
	private String 	correopelecMn; 
	private String  correopelecUsd; 
	private String 	correoopeMn; 
	private String 	correoopeUsd; 
	
	 
	public boolean load() throws Exception{
	boolean okCon = true;
		try { this.cbd.conexionDB(); } 
		catch(SQLException sqle) { okCon = false; }
	return okCon;
	}
  
	public boolean loadParametros(){
		System.out.println("ParaSistemaBean::loadParametros(E)");

		boolean ok=true;
		boolean mn = false;
		boolean dl = false;		
		StringBuilder strQuery = new StringBuilder();
		try {
			strQuery = new StringBuilder();
			strQuery.append(
				"SELECT substr(lpad(to_char(horarioiniconsulta), 4, '0'),1,2) IniConsultaHr, " +
				"       substr(lpad(to_char(horarioiniconsulta), 4, '0'),3,4) IniConsultaMi, " +
				"       substr(lpad(to_char(horariofinconsulta), 4, '0'), 1, 2) FinConsultaHr, " +
				"       substr(lpad(to_char(horariofinconsulta), 4, '0'), 3, 4) FinConsultaMi, " +
				"       substr(lpad(to_char(horarioiniconfirmacion), 4, '0'), 1, 2) IniConfHr, " +
				"       substr(lpad(to_char(horarioiniconfirmacion), 4, '0'), 3, 4) IniConfMi, " +
				"       substr(lpad(to_char(horariofinconfirmacion), 4, '0'), 1, 2) FinConfHr, " +
				"       substr(lpad(to_char(horariofinconfirmacion), 4, '0'), 3, 4) FinConfMi, " +
				"       montomaximo, " +
				"       plazomaximo, " +
				"       estatus, " +
				"       ig_plazo_max_oper, " +
				"       ig_num_max_disp, " +
				"		ig_tasa_impuesto_dl, " +
				"       ig_decimales, " +
				"       ic_moneda, " +
				"       ig_dias_solic_rec, " +
				"       cg_metodo_inter, " +
				"       cg_metodo_calc, " +
				"       ig_techo_tasa_p, " +
				"       ig_plazo_max_cot, " +
				"       ig_num_max_disp_cot, " +
				"       ig_monto_max_cot, " +
				"       ig_plazo_max_cupon, " +
				"       cg_correo, " +
				"       cg_tasa_f, " +
				"       cg_tasa_v , " +
				"       CG_CORREOPELEC , " +
				"       CG_CORREOOPE  " +				
				"  FROM parametrossistema ");
		    
		    LOG.debug("query  --->"+strQuery.toString()  );
		    
			this.cRS=this.cbd.queryDB(strQuery.toString() );
			while (this.cRS.next()){
				if(this.cRS.getInt("ic_moneda")==1){
					cdtmHoraInicioConsulta.set(2000,0,1,Integer.parseInt(this.cRS.getString("IniConsultaHr")),Integer.parseInt(cRS.getString("IniConsultaMi")));
					cdtmHoraTerminoConsulta.set(2000,0,1,Integer.parseInt(this.cRS.getString("FinConsultaHr")),Integer.parseInt(cRS.getString("FinConsultaMi")));
					cdtmHoraInicioConfirmacion.set(2000,0,1,Integer.parseInt(this.cRS.getString("IniConfHr")),Integer.parseInt(cRS.getString("IniConfMi")));
					cdtmHoraTerminoConfirmacion.set(2000,0,1,Integer.parseInt(this.cRS.getString("FinConfHr")),Integer.parseInt(cRS.getString("FinConfMi")));
					cdblMontoLimiteExternos=this.cRS.getDouble("montomaximo");
					cintPlazoLimiteExternos=this.cRS.getInt("plazomaximo");
					if (this.cRS.getString("estatus").equals("1")){
						//Sistema no bloqueado
						cbolSistemaBloqueado=true;
					}else{
						//Sistema bloqueado
						cbolSistemaBloqueado=false;
					}

					//Foda 10-2005-SMJ
					ig_plazo_max_oper 	= this.cRS.getInt("ig_plazo_max_oper");
					ig_num_max_disp 	= this.cRS.getInt("ig_num_max_disp");
					ig_dias_solic_rec 	= this.cRS.getInt("ig_dias_solic_rec");
					
					ig_tasa_impuesto 	= this.cRS.getDouble("ig_tasa_impuesto_dl");
					ig_decimales 		= this.cRS.getInt("ig_decimales");
					cg_metodo_inter 	= (this.cRS.getString("cg_metodo_inter")==null)?"A":this.cRS.getString("cg_metodo_inter");
					cg_metodo_calc 		= (this.cRS.getString("cg_metodo_calc")==null)?"D":this.cRS.getString("cg_metodo_calc");
					
					ig_techo_tasa_p 	= this.cRS.getDouble("ig_techo_tasa_p");
					ig_plazo_max_cot 	= this.cRS.getInt("ig_plazo_max_cot");
					ig_num_max_disp_cot = this.cRS.getInt("ig_num_max_disp_cot");
					ig_monto_max_cot 	= this.cRS.getDouble("ig_monto_max_cot");
					ig_plazo_max_cupon 	= this.cRS.getInt("ig_plazo_max_cupon");
					cg_correo			= this.cRS.getString("cg_correo");
					cg_tasa_f			= this.cRS.getString("cg_tasa_f");
					cg_tasa_v			= this.cRS.getString("cg_tasa_v");
					
					correopelecMn =  this.cRS.getString("CG_CORREOPELEC");
					correoopeMn =  this.cRS.getString("CG_CORREOOPE");
					
					mn = true;
				}else{ //dolares
					cdtmHoraInicioConsulta.set(2000,0,1,Integer.parseInt(this.cRS.getString("IniConsultaHr")),Integer.parseInt(cRS.getString("IniConsultaMi")));
					cdtmHoraTerminoConsulta.set(2000,0,1,Integer.parseInt(this.cRS.getString("FinConsultaHr")),Integer.parseInt(cRS.getString("FinConsultaMi")));
					cdtmHoraInicioConfirmacion.set(2000,0,1,Integer.parseInt(this.cRS.getString("IniConfHr")),Integer.parseInt(cRS.getString("IniConfMi")));
					cdtmHoraTerminoConfirmacion.set(2000,0,1,Integer.parseInt(this.cRS.getString("FinConfHr")),Integer.parseInt(cRS.getString("FinConfMi")));
					cdblMontoLimiteExternosDl=this.cRS.getDouble("montomaximo");
					cintPlazoLimiteExternosDl=this.cRS.getInt("plazomaximo");
					if (this.cRS.getString("estatus").equals("1")){
						//Sistema no bloqueado
						cbolSistemaBloqueadoDl=true;
					}else{
						//Sistema bloqueado
						cbolSistemaBloqueadoDl=false;
					}
					
					ig_plazo_max_oper_dl 	= this.cRS.getInt("ig_plazo_max_oper");
					ig_num_max_disp_dl 		= this.cRS.getInt("ig_num_max_disp");
					ig_dias_solic_rec_dl 	= this.cRS.getInt("ig_dias_solic_rec");
					
					ig_tasa_impuesto_dl 	= this.cRS.getDouble("ig_tasa_impuesto_dl");
					ig_decimales 			= this.cRS.getInt("ig_decimales");
					cg_metodo_inter_dl  	= (this.cRS.getString("cg_metodo_inter")==null)?"L":this.cRS.getString("cg_metodo_inter");
					cg_metodo_calc_dl 		= (this.cRS.getString("cg_metodo_calc")==null)?"P":this.cRS.getString("cg_metodo_calc");
					
					ig_plazo_max_cot_dl 	= this.cRS.getInt("ig_plazo_max_cot");
					ig_num_max_disp_cot_dl 	= this.cRS.getInt("ig_num_max_disp_cot");
					ig_monto_max_cot_dl 	= this.cRS.getDouble("ig_monto_max_cot");
					ig_plazo_max_cupon_dl 	= this.cRS.getInt("ig_plazo_max_cupon");
					cg_correo_dl			= this.cRS.getString("cg_correo");
					cg_tasa_f_dl			= this.cRS.getString("cg_tasa_f");
					cg_tasa_v_dl			= this.cRS.getString("cg_tasa_v");
				    
					correopelecUsd =  this.cRS.getString("CG_CORREOPELEC");
					correoopeUsd =  this.cRS.getString("CG_CORREOOPE");
					

					dl = true;
				}	//si es dolares
			}//while (this.cRS.next())
      		cbd.cierraStatement();			

			if(!mn&&!dl) {
				cdtmHoraInicioConsulta.set(2000,0,1,9,0);
				cdtmHoraTerminoConsulta.set(2000,0,1,18,30);
				cdtmHoraInicioConfirmacion.set(2000,0,1,10,00);
				cdtmHoraTerminoConfirmacion.set(2000,0,1,17,15);
				ig_decimales=1;
			}
			if(!mn){
				cdblMontoLimiteExternos=100000000;
				cintPlazoLimiteExternos=600;
				cbolSistemaBloqueado=false;
				ig_plazo_max_oper=5400;
				ig_num_max_disp=100;
				ig_tasa_impuesto=0.00;
				ig_dias_solic_rec =1;
				cg_metodo_inter="A";
				cg_metodo_calc="D";
			}
			if(!dl){
				cdblMontoLimiteExternosDl=100000000;
				cintPlazoLimiteExternosDl=600;
				cbolSistemaBloqueadoDl=false;
				ig_plazo_max_oper_dl=5400;
				ig_num_max_disp_dl=100;
				ig_tasa_impuesto_dl=5.152;
				ig_dias_solic_rec_dl =2;
				cg_metodo_inter_dl="L";
				cg_metodo_calc_dl="P";
			}
			lEsquemasRecup = new ArrayList();
			strQuery = new StringBuilder();
			strQuery.append(
				" select ic_esquema_recup"   +
				"  , cg_descripcion"   +
				"  , cs_disponible_if_mn"   +
				"  , cs_disponible_if_dl"   +
				" from cotcat_esquema_recup")  ;
			PreparedStatement ps = cbd.queryPrecompilado(strQuery.toString());
			this.cRS = ps.executeQuery();
			while(cRS.next()){
				Map mColumnas = new HashMap();
				mColumnas.put("ic_esquema_recup",cRS.getString("ic_esquema_recup"));
				mColumnas.put("cg_descripcion",cRS.getString("cg_descripcion"));
				mColumnas.put("cs_disponible_if_mn",cRS.getString("cs_disponible_if_mn"));
				mColumnas.put("cs_disponible_if_dl",cRS.getString("cs_disponible_if_dl"));
				lEsquemasRecup.add(mColumnas);
			}
			this.cRS.close();
			ps.close();
		} catch(SQLException sqle) { 
			ok=false; 
			System.out.println("ParaSistemaBean::loadParametros(Exception: )"+sqle);
		} finally {
			System.out.println("ParaSistemaBean::loadParametros(S)");
		}
		ok=true;
    
		return ok;
	}

	private void validaHorarios() throws CotizadorException{
		if (cdtmHoraInicioConsulta.getTime().compareTo(cdtmHoraTerminoConsulta.getTime())> 0){
			throw new CotizadorException("El horario de inicio de consulta debe ser menor que el horario final de consulta");
		}
		if (cdtmHoraInicioConfirmacion.getTime().compareTo(cdtmHoraTerminoConfirmacion.getTime())> 0){
			throw new CotizadorException("El horario de inicio de confirmación debe ser menor que el horario final de confirmación");
		}
	}

	public boolean guardarCambios() throws CotizadorException{
	
		boolean ok=true;
		StringBuilder strGuardar = new StringBuilder();
		String h="";
		String hdl="";
		PreparedStatement ps = null;
		validaHorarios();
   
		if (cbolSistemaBloqueado) h="1";
		else h="0";

		if (cbolSistemaBloqueadoDl) hdl="1";
		else hdl="0";
		
		strGuardar = new StringBuilder();
		strGuardar.append("SELECT COUNT(1)as numReg,ic_moneda FROM parametrossistema group by IC_MONEDA");	

    
		try{

			boolean regmn = false;
			boolean regdl = false;
			this.cRS=this.cbd.queryDB(strGuardar.toString());
			while (this.cRS.next()){
				if(this.cRS.getInt("ic_moneda")==1&&this.cRS.getInt("numReg")>0){
					regmn = true;
				}
				if(this.cRS.getInt("ic_moneda")==54&&this.cRS.getInt("numReg")>0){
					regdl = true;
				}
			}
			this.cbd.cierraStatement();
			
			if (regmn){
				System.out.println("Actualiza Pesos");   
				strGuardar = new StringBuilder();
				strGuardar.append("UPDATE parametrossistema " +
					"   SET horarioiniconsulta = " + cdtmHoraInicioConsulta.get(Calendar.HOUR_OF_DAY)+""+InformacionGeneralBean.formatoNumeros(cdtmHoraInicioConsulta.get(Calendar.MINUTE), "00") + ", " +
					"       horariofinconsulta = " + cdtmHoraTerminoConsulta.get(Calendar.HOUR_OF_DAY)+""+InformacionGeneralBean.formatoNumeros(cdtmHoraTerminoConsulta.get(Calendar.MINUTE), "00") + ", " +
					"       horarioiniconfirmacion = " + cdtmHoraInicioConfirmacion.get(Calendar.HOUR_OF_DAY)+""+InformacionGeneralBean.formatoNumeros(cdtmHoraInicioConfirmacion.get(Calendar.MINUTE), "00") + ", " +
					"       horariofinconfirmacion = " + cdtmHoraTerminoConfirmacion.get(Calendar.HOUR_OF_DAY)+""+InformacionGeneralBean.formatoNumeros(cdtmHoraTerminoConfirmacion.get(Calendar.MINUTE), "00") + ", " +
					"       montomaximo = " + cdblMontoLimiteExternos + ", " +
					"       plazomaximo = " + cintPlazoLimiteExternos + ", " +
					"       estatus = '" + h + "'" +
					"		,ig_plazo_max_oper = "+ ig_plazo_max_oper +   //Foda 10-2005-SMJ
					"		,ig_num_max_disp = "+	ig_num_max_disp + 
					"		,ig_tasa_impuesto_dl = "+	ig_tasa_impuesto + 
					"		,ig_decimales = "+	ig_decimales+ 
					"		,ig_dias_solic_rec = "+ig_dias_solic_rec+
					"		,cg_metodo_calc = '"+cg_metodo_calc+"'"+
					"		,cg_metodo_inter = '"+cg_metodo_inter+"'"+
					"		,ig_techo_tasa_p = "+	ig_techo_tasa_p+ 
					"		,ig_plazo_max_cot = "+	ig_plazo_max_cot+ 
					"		,ig_num_max_disp_cot = "+	ig_num_max_disp_cot+ 
					"		,ig_monto_max_cot = "+	ig_monto_max_cot+ 
					"		,ig_plazo_max_cupon = "+	ig_plazo_max_cupon+ 
					"		,cg_correo = '"+cg_correo+"'"+
					"		,cg_tasa_f = '"+cg_tasa_f+"'"+
					"		,cg_tasa_v = '"+cg_tasa_v+"'"+					
					"		,CG_CORREOPELEC = '"+correopelecMn+"'"+
					"		,CG_CORREOOPE = '"+correoopeMn+"'"+				
					
					" WHERE IC_MONEDA = 1");
					LOG.debug(strGuardar.toString());   
					this.cbd.ejecutaSQL(strGuardar.toString());
			}else{
				LOG.debug("Inserta Pesos");  
				strGuardar = new StringBuilder();
				strGuardar.append(
					" INSERT INTO parametrossistema ("   +
					"              idparametro, ic_moneda, horarioiniconsulta, horariofinconsulta,"   +
					"              horarioiniconfirmacion, horariofinconfirmacion, montomaximo,"   +
					"              plazomaximo, estatus, ig_plazo_max_oper, ig_num_max_disp,"   +
					"              ig_tasa_impuesto_dl, ig_decimales, ig_dias_solic_rec,"   +
					"              cg_metodo_calc, cg_metodo_inter, ig_techo_tasa_p,"   +
					"              ig_plazo_max_cot, ig_num_max_disp_cot, ig_monto_max_cot,"   +
					"              ig_plazo_max_cupon, cg_correo, cg_tasa_f, cg_tasa_v, CG_CORREOPELEC, CG_CORREOOPE  )"  +
					"      VALUES (1, 1, "+
					cdtmHoraInicioConsulta.get(Calendar.HOUR_OF_DAY)+""+cdtmHoraInicioConsulta.get(Calendar.MINUTE) + ", " +
					cdtmHoraTerminoConsulta.get(Calendar.HOUR_OF_DAY)+""+cdtmHoraTerminoConsulta.get(Calendar.MINUTE) + ", " +
					cdtmHoraInicioConfirmacion.get(Calendar.HOUR_OF_DAY)+""+cdtmHoraInicioConfirmacion.get(Calendar.MINUTE) + ", " +
					cdtmHoraTerminoConfirmacion.get(Calendar.HOUR_OF_DAY)+""+cdtmHoraTerminoConfirmacion.get(Calendar.MINUTE) + ", " +
					cdblMontoLimiteExternos + ", " + 
					cintPlazoLimiteExternos + ", " +
					"'" + h + "'," + 
					ig_plazo_max_oper + ", " +
					ig_num_max_disp+ "," + //Foda 10-2005-SMJ
					ig_tasa_impuesto + ", " +
					ig_decimales+","+
					ig_dias_solic_rec+" , "+
					"'"+cg_metodo_calc+"',"+
					"'"+cg_metodo_inter+"',"+
					ig_techo_tasa_p+" , "+
					ig_plazo_max_cot+" , "+
					ig_num_max_disp_cot+" , "+
					ig_monto_max_cot+" , "+
					ig_plazo_max_cupon+",  "+
					"'"+cg_correo+"',  "+
					"'"+cg_tasa_f+"',  "+
					"'"+cg_tasa_v+"',  "+
					"'"+correopelecMn+"',  "+
					"'"+correoopeMn+"' "+					
					")"); 
					LOG.debug(strGuardar);
					this.cbd.ejecutaSQL(strGuardar.toString());
			}

			if (regdl){
				LOG.debug("Actualiza Dolares");  
				strGuardar = new StringBuilder();
				strGuardar.append( 
					"UPDATE parametrossistema " +
					"   SET horarioiniconsulta=" + cdtmHoraInicioConsulta.get(Calendar.HOUR_OF_DAY)+""+InformacionGeneralBean.formatoNumeros(cdtmHoraInicioConsulta.get(Calendar.MINUTE), "00") + ", " +
					"       horariofinconsulta=" + cdtmHoraTerminoConsulta.get(Calendar.HOUR_OF_DAY)+""+InformacionGeneralBean.formatoNumeros(cdtmHoraTerminoConsulta.get(Calendar.MINUTE), "00") + ", " +
					"       horarioiniconfirmacion=" + cdtmHoraInicioConfirmacion.get(Calendar.HOUR_OF_DAY)+""+InformacionGeneralBean.formatoNumeros(cdtmHoraInicioConfirmacion.get(Calendar.MINUTE), "00") + ", " +
					"       horariofinconfirmacion=" + cdtmHoraTerminoConfirmacion.get(Calendar.HOUR_OF_DAY)+""+InformacionGeneralBean.formatoNumeros(cdtmHoraTerminoConfirmacion.get(Calendar.MINUTE), "00") + ", " +
					"       montomaximo=" + cdblMontoLimiteExternosDl + ", " +
					"       plazomaximo=" + cintPlazoLimiteExternosDl + ", " +
					"       estatus='" + hdl + "'" +
					"		,ig_plazo_max_oper="+ ig_plazo_max_oper_dl +   //Foda 10-2005-SMJ
					"		,ig_num_max_disp="+	ig_num_max_disp_dl + 
					"		,ig_tasa_impuesto_dl="+	ig_tasa_impuesto_dl + 
					"		,ig_decimales="+	ig_decimales+ 
					"		,ig_dias_solic_rec="+ig_dias_solic_rec_dl+
					"		,cg_metodo_calc='"+cg_metodo_calc_dl+"'"+
					"		,cg_metodo_inter='"+cg_metodo_inter_dl+"'"+
					"		,ig_plazo_max_cot = "+	ig_plazo_max_cot_dl+ 
					"		,ig_num_max_disp_cot = "+	ig_num_max_disp_cot_dl+ 
					"		,ig_monto_max_cot = "+	ig_monto_max_cot_dl+ 
					"		,ig_plazo_max_cupon = "+	ig_plazo_max_cupon_dl+ 
					"		,cg_correo = '"+cg_correo_dl+"'"+
					"		,cg_tasa_f = '"+cg_tasa_f_dl+"'"+
					"		,cg_tasa_v = '"+cg_tasa_v_dl+"'"+
					"		,CG_CORREOPELEC = '"+correopelecUsd+"'"+
					"		,CG_CORREOOPE = '"+correoopeUsd+"'"+					
					" WHERE IC_MONEDA = 54");
					LOG.debug(strGuardar.toString());   
					this.cbd.ejecutaSQL(strGuardar.toString());
			}else{
				LOG.debug("Inserta Dolares");  
				strGuardar = new StringBuilder();
				strGuardar.append(
					" INSERT INTO parametrossistema ("   +
					"              idparametro, ic_moneda, horarioiniconsulta, horariofinconsulta,"   +
					"              horarioiniconfirmacion, horariofinconfirmacion, montomaximo,"   +
					"              plazomaximo, estatus, ig_plazo_max_oper, ig_num_max_disp,"   +
					"              ig_tasa_impuesto_dl, ig_decimales, ig_dias_solic_rec,"   +
					"              cg_metodo_calc, cg_metodo_inter, ig_plazo_max_cot,"   +
					"              ig_num_max_disp_cot, ig_monto_max_cot, ig_plazo_max_cupon, cg_correo,"   +
					"              cg_tasa_f, cg_tasa_v,CG_CORREOPELEC , CG_CORREOOPE  )"  +
					"VALUES (2,54," +
					cdtmHoraInicioConsulta.get(Calendar.HOUR_OF_DAY)+""+cdtmHoraInicioConsulta.get(Calendar.MINUTE) + ", " +
					cdtmHoraTerminoConsulta.get(Calendar.HOUR_OF_DAY)+""+cdtmHoraTerminoConsulta.get(Calendar.MINUTE) + ", " +
					cdtmHoraInicioConfirmacion.get(Calendar.HOUR_OF_DAY)+""+cdtmHoraInicioConfirmacion.get(Calendar.MINUTE) + ", " +
					cdtmHoraTerminoConfirmacion.get(Calendar.HOUR_OF_DAY)+""+cdtmHoraTerminoConfirmacion.get(Calendar.MINUTE) + ", " +
					cdblMontoLimiteExternosDl + ", " + 
					cintPlazoLimiteExternosDl + ", "+
					"'" + hdl + "'," + 
					ig_plazo_max_oper_dl + ", " +
					ig_num_max_disp_dl+ "," + //Foda 10-2005-SMJ
					ig_tasa_impuesto_dl + ", " +
					ig_decimales+","+
					ig_dias_solic_rec_dl+","+
					"'"+cg_metodo_calc_dl+"',"+
					"'"+cg_metodo_inter_dl+"',"+
					ig_plazo_max_cot_dl+" , "+
					ig_num_max_disp_cot_dl+" , "+
					ig_monto_max_cot_dl+" , "+
					ig_plazo_max_cupon_dl+" , "+
					"'"+cg_correo_dl+"', "+
					"'"+cg_tasa_f+"', "+
					"'"+cg_tasa_v+"', "+
					"'"+correopelecUsd+"', "+
					"'"+correoopeUsd+"'"+					
					")"); 
					LOG.debug(strGuardar.toString());
					this.cbd.ejecutaSQL(strGuardar.toString());
			}
			strGuardar = new StringBuilder();
			strGuardar.append(
				" update cotcat_esquema_recup"+
				" set cs_disponible_if_mn = 'N'"+
				"	,cs_disponible_if_dl = 'N'");
			ps = this.cbd.queryPrecompilado(strGuardar.toString());
			ps.execute();
			ps.close();

			if(this.cs_disponible_if_mn!=null&&this.cs_disponible_if_mn.length>0){
				strGuardar = new StringBuilder();
				strGuardar.append(
					" update cotcat_esquema_recup"+
					" set cs_disponible_if_mn = 'S'"+
					"	where ic_esquema_recup = ?");
				
				ps = this.cbd.queryPrecompilado(strGuardar.toString());
				for(int i=0;i<this.cs_disponible_if_mn.length;i++){
					ps.setString(1,cs_disponible_if_mn[i]);
					ps.execute();
				}
			}
			ps.close();

			if(this.cs_disponible_if_dl!=null&&this.cs_disponible_if_dl.length>0){
				strGuardar = new StringBuilder();
				strGuardar .append( 
					" update cotcat_esquema_recup"+
					" set cs_disponible_if_dl = 'S'"+
					"	where ic_esquema_recup = ?");
				ps = this.cbd.queryPrecompilado(strGuardar.toString());
				for(int i=0;i<this.cs_disponible_if_dl.length;i++){
					ps.setString(1,cs_disponible_if_dl[i]);
					ps.execute();
				}
			}
			ps.close();			
			
		}catch(SQLException e){
			ok = false;
			e.printStackTrace();
			throw new CotizadorException("ParaSistemaBean.guardarCambios():: " + e.getMessage());
		} finally {
			this.cbd.terminaTransaccion(ok);
   	}
	return ok;
	}

	public void release(){
		try {
			this.cbd.cierraConexionDB();
		}catch(Exception e) { System.out.println(e.getMessage()); }
	}

	public java.util.Calendar getHoraInicioConsulta(){
		return cdtmHoraInicioConsulta;
	}

	public void setHoraInicioConsulta(int hora, int min){
		cdtmHoraInicioConsulta.set(2000,0,1,hora,min);
	}

	public java.util.Calendar getHoraTerminoConsulta(){
		return cdtmHoraTerminoConsulta;
	}

	public void setHoraTerminoConsulta(int hora, int min){
		cdtmHoraTerminoConsulta.set(2000,0,1,hora,min);
	}
  
	public java.util.Calendar getHoraInicioConfirmacion(){
		return cdtmHoraInicioConfirmacion;
	}
	
	public void setHoraInicioConfirmacion(int hora, int min){
		cdtmHoraInicioConfirmacion.set(2000,0,1,hora,min);
	}
	
	public java.util.Calendar getHoraTerminoConfirmacion(){
		return cdtmHoraTerminoConfirmacion;
	}  
	
	public void setHoraTerminoConfirmacion(int hora, int min){
		cdtmHoraTerminoConfirmacion.set(2000,0,1,hora,min);
	}

         
	public double getMontoLimiteExternos(){
		return cdblMontoLimiteExternos;
	} 
	public double getMontoLimiteExternosDl(){
		return cdblMontoLimiteExternosDl;
	} 	

  
	public void setMontoLimiteExternos(double var){
		cdblMontoLimiteExternos=var;
	}
	public void setMontoLimiteExternosDl(double var){
		cdblMontoLimiteExternosDl=var;
	}	
	public int getPlazoLimiteExternos(){
		return cintPlazoLimiteExternos;
	}
	public int getPlazoLimiteExternosDl(){
		return cintPlazoLimiteExternosDl;
	}	
	
	public void setPlazoLimiteExternos(int var){
		cintPlazoLimiteExternos=var;
	}
	public void setPlazoLimiteExternosDl(int var){
		cintPlazoLimiteExternosDl=var;
	}	
	
	public boolean getEstatus(){
		return cbolSistemaBloqueado;
	} 
	public boolean getEstatusDl(){
		return cbolSistemaBloqueadoDl;
	} 
	
	public void setEstatus(boolean var){
		cbolSistemaBloqueado=var;
	}

	public void setEstatusDl(boolean var){
		cbolSistemaBloqueadoDl=var;
	}

	//Foda 10-2005-SMJ
	
	public int getIg_plazo_max_oper(){
		return ig_plazo_max_oper;
	}
	public int getIg_plazo_max_oper_dl(){
		return ig_plazo_max_oper_dl;
	}	
	
	public void setIg_plazo_max_oper(int var){
		ig_plazo_max_oper=var;
	}

	public void setIg_plazo_max_oper_dl(int var){
		ig_plazo_max_oper_dl=var;
	}
	
	public int getIg_num_max_disp(){
		return ig_num_max_disp;
	}
	public int getIg_num_max_disp_dl(){
		return ig_num_max_disp_dl;
	}	
	
	public void setIg_num_max_disp(int var){
		ig_num_max_disp=var;
    
	}
	public void setIg_num_max_disp_dl(int var){
		ig_num_max_disp_dl=var;
    
	}
	

	public double getIg_tasa_impuesto_dl(){
		return ig_tasa_impuesto_dl;
	}

	public double getIg_tasa_impuesto(){
		return ig_tasa_impuesto;
	}	
	
	public void setIg_tasa_impuesto_dl(double newIg_tasa_impuesto_dl){
		ig_tasa_impuesto_dl = newIg_tasa_impuesto_dl;
	}

	public void setIg_tasa_impuesto(double newIg_tasa_impuesto){
		ig_tasa_impuesto = newIg_tasa_impuesto;
	}	
	
	
	public int getIg_decimales(){
		return ig_decimales;
	}
	
	public void setIg_decimales(int newIg_decimales){
		ig_decimales = newIg_decimales;
	}  

	public int getIg_dias_solic_rec(){
		return ig_dias_solic_rec;
	}		

	public int getIg_dias_solic_rec_dl(){
		return ig_dias_solic_rec_dl;
	}			
	
	public void setIg_dias_solic_rec(int val){
		ig_dias_solic_rec = val;
	}
	public void setIg_dias_solic_rec_dl(int val){
		ig_dias_solic_rec_dl = val;
	}		

	public void setCg_metodo_calc(String val){
		cg_metodo_calc = val;
	}
	public void setCg_metodo_calc_dl(String val){
		cg_metodo_calc_dl = val;
	}

	public String getCg_metodo_calc(){
		return cg_metodo_calc;
	}
	public String getCg_metodo_calc_dl(){
		return cg_metodo_calc_dl;
	}

	public void setCg_metodo_inter(String val){
		cg_metodo_inter = val;
	}
	public void setCg_metodo_inter_dl(String val){
		cg_metodo_inter_dl = val;
	}

	public String getCg_metodo_inter(){
		return cg_metodo_inter;
	}
	public String getCg_metodo_inter_dl(){
		return cg_metodo_inter_dl;
	}	

	public List getEsquemasRecup(){
		return lEsquemasRecup;
	}

	public void setCs_disponible_if_mn(String val[]){
		this.cs_disponible_if_mn = val;
	}

	public void setCs_disponible_if_dl(String val[]){
		this.cs_disponible_if_dl = val;
	}	
	
	//SET
	public void setIg_techo_tasa_p(double ig_techo_tasa_p){
		this.ig_techo_tasa_p = ig_techo_tasa_p;
	}
	public void setIg_plazo_max_cot(int ig_plazo_max_cot){
		this.ig_plazo_max_cot = ig_plazo_max_cot;
	}
	public void setIg_num_max_disp_cot(int ig_num_max_disp_cot){
		this.ig_num_max_disp_cot = ig_num_max_disp_cot;
	}
	public void setIg_monto_max_cot(double ig_monto_max_cot){
		this.ig_monto_max_cot = ig_monto_max_cot;
	}
	public void setIg_plazo_max_cupon(int ig_plazo_max_cupon){
		this.ig_plazo_max_cupon = ig_plazo_max_cupon;
	}
	
	public void setIg_plazo_max_cot_dl(int ig_plazo_max_cot_dl){
		this.ig_plazo_max_cot_dl = ig_plazo_max_cot_dl;
	}
	public void setIg_num_max_disp_cot_dl(int ig_num_max_disp_cot_dl){
		this.ig_num_max_disp_cot_dl = ig_num_max_disp_cot_dl;
	}
	public void setIg_monto_max_cot_dl(double ig_monto_max_cot_dl){
		this.ig_monto_max_cot_dl = ig_monto_max_cot_dl;
	}
	public void setIg_plazo_max_cupon_dl(int ig_plazo_max_cupon_dl){
		this.ig_plazo_max_cupon_dl = ig_plazo_max_cupon_dl;
	}
	
	public void setCg_correo(String cg_correo){
		this.cg_correo = cg_correo;
	}
	public void setCg_correo_dl(String cg_correo_dl){
		this.cg_correo_dl = cg_correo_dl;
	}
	
	public void setCg_tasa_f(String cg_tasa_f){
		this.cg_tasa_f = cg_tasa_f;
	}
	
	public void setCg_tasa_f_dl(String cg_tasa_f_dl){
		this.cg_tasa_f_dl = cg_tasa_f_dl;
	}
	
	public void setCg_tasa_v(String cg_tasa_v){
		this.cg_tasa_v = cg_tasa_v;
	}
	
	public void setCg_tasa_v_dl(String cg_tasa_v_dl){
		this.cg_tasa_v_dl = cg_tasa_v_dl;
	}
	
	//GET
	public double getIg_techo_tasa_p() {
		return this.ig_techo_tasa_p;
	}
	public int getIg_plazo_max_cot() {
		return this.ig_plazo_max_cot;
	}
	public int getIg_num_max_disp_cot() {
		return this.ig_num_max_disp_cot;
	}
	public double getIg_monto_max_cot() {
		return this.ig_monto_max_cot;
	}
	public int getIg_plazo_max_cupon() {
		return this.ig_plazo_max_cupon;
	}
	
	public int getIg_plazo_max_cot_dl() {
		return this.ig_plazo_max_cot_dl;
	}
	public int getIg_num_max_disp_cot_dl() {
		return this.ig_num_max_disp_cot_dl;
	}
	public double getIg_monto_max_cot_dl() {
		return this.ig_monto_max_cot_dl;
	}
	public int getIg_plazo_max_cupon_dl() {
		return this.ig_plazo_max_cupon_dl;
	}
	
	public String getCg_correo() {
		return this.cg_correo;
	}
	public String getCg_correo_dl() {
		return this.cg_correo_dl;
	}
	
	public String getCg_tasa_f(){
		return this.cg_tasa_f;
	}
	
	public String getCg_tasa_f_dl(){
		return this.cg_tasa_f_dl;
	}
	
	public String getCg_tasa_v(){
		return this.cg_tasa_v;
	}
	
	public String getCg_tasa_v_dl(){
		return this.cg_tasa_v_dl;
	}
	
	
	
	
	
	//2017 QC	
	public void setCorreopelecMn(String correopelecMn){
	    this.correopelecMn = correopelecMn;
	}
	
	
	public String getCorreopelecMn(){
	    return this.correopelecMn;
	}
	
	
	public void setCorreopelecUsd(String correopelecUsd){
	    this.correopelecUsd = correopelecUsd;
	}
	
	
	public String getCorreopelecUsd(){
	    return this.correopelecUsd;
	}
	
	
	public void setCorreoopeMn(String correoopeMn){
	    this.correoopeMn = correoopeMn;
	}
	
	
	public String getCorreoopeMn(){
	    return this.correoopeMn;
	}
	
	
	public void setCorreoopeUsd(String correoopeUsd){
	    this.correoopeUsd = correoopeUsd;
	}
	
	
	public String getCorreoopeUsd(){
	    return this.correoopeUsd;
	}	
	
}