package netropology.utilerias.negocio;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class ResultadoPTRBean {
	private String acuseRecibo;
	private long lNoReferencia;
	private String strAreaResponsable;
	private String strIntermediario;
	private String strTipoIntermediario;
	private int iIdTipoIntermediario;
	private String strAcreditado;
	private String strFechaCotizacion;
	private String strFechaDisposicion;
	private double dMontoOperacion;
	private int iUnidadTiempo;
	private int iIdUnidadTiempo;
	private int iPlazoCredito;
	private String strCurvaCotizacion;
	private int iIdCurvaCotizacion;
	private String strCurvaDuracion;
	private int iIdCurvaDuracion;
	private String usuario;
	private String strGrupoUsuario;
	private int iDuracionCreditoDias;
	private int iDiasUTB;
	private double dTasaCreditoDuracion;
	private double dFactorRiesgo;
	private double dTasaNeta;
	private double dTasaNeta1Mes;
	private double dTasaNetaConVigencia;
	private double dTasaNetaConVigencia1Mes;
	private double dSobretasa;
	private int iPlazoCreditoDias;
	private int iPlazoVigencia;
	private int iPeriodosGracia;
	private int iNumeroPagosIntereses;
	private int iPeriodosGraciaIntereses;
	private int iNumeroPagosCapital;
	private int iPeriodosGraciaCapital;
	private double dTasaCreditoUTB;
	private String strIdStatus;

	public boolean confirmarCotizacion() throws Exception{
		PersCotizacion confirma=new PersCotizacion("PTR");
		String strUpdate="";
		boolean confirmado=false;

		if (confirma.load()){
			strIdStatus="2";//status=2 confirmado

			strUpdate="UPDATE credito "+
						"SET  idStatus=" + strIdStatus + ", " +
						"     acuseRecibo='" + acuseRecibo + "' " +
						"WHERE noReferencia=" + lNoReferencia;
			System.out.println(strUpdate);

			if (confirma.guardar(strUpdate)) confirmado=true;
			else confirmado=false;

			confirma.release();
		}
	return confirmado;
	}

	public String formatoDec(double num){
			String formato ="#.#######";
			DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
			simbolo.setDecimalSeparator('.');
			DecimalFormat decFormato = new DecimalFormat(formato, simbolo);
			return decFormato.format(num);

	}
	public void guardarCotizacion() throws CotizadorException{
		PersCotizacion datos=new PersCotizacion("PPR");
		String strInsert="";
		String strFechaCot="";
		String strFechaDisp="";
		boolean guardado=false;
		try{
			if (datos.load()){
				lNoReferencia=datos.getNumReferencia();
				iIdUnidadTiempo=datos.getIDUnidadTiempo(iUnidadTiempo);
				iIdCurvaDuracion=datos.getIDCurva(strCurvaDuracion);
				iIdCurvaCotizacion=datos.getIDCurva(strCurvaCotizacion);
				strFechaCot=strFechaCotizacion.substring(6,8)+"/"+strFechaCotizacion.substring(4,6)+"/"+strFechaCotizacion.substring(0,4);
				strFechaDisp=strFechaDisposicion.substring(6,8)+"/"+strFechaDisposicion.substring(4,6)+"/"+strFechaDisposicion.substring(0,4);
				strIdStatus="1"; //status 1=cotizado

				strInsert="INSERT INTO credito " +
						"(NoReferencia, idStatus, idUnidadTiempo, usuario, idCurvaDuracion, " +
						"idCurvaCotizacion, areaResponsable, intermediario, idTipoIntermediario, " +
						"acreditado, fechaCotizacion, fechaDisposicion, monto, plazo, " +
						"duracionCreditoDias, plazoVigencia, periodosGracia, numeroPagosIntereses, " +
						"periodoGraciaIntereses, numeroPagosCapital, periodoGraciaCapital, tasaNetaConVigencia, " +
						"tasaNetaConVigencia1Mes, sobretasa, tasaNeta, tasaNeta1mes, tasaCreditoUTB, diasUTB) " +
						"VALUES(" + lNoReferencia + ", " + strIdStatus + ", " + iIdUnidadTiempo + ", '" +
						usuario + "', " + iIdCurvaDuracion + ", " + iIdCurvaCotizacion + ", '" +
						strAreaResponsable + "', '" + strIntermediario + "', " + iIdTipoIntermediario + ",'" +
						strAcreditado + "', to_date('" + strFechaCot + "', 'dd/mm/yyyy'), " +
						"to_date('" + strFechaDisp + "', 'dd/mm/yyyy'), " + dMontoOperacion + ", " +
						iPlazoCredito + ", " + iDuracionCreditoDias + ", " + iPlazoVigencia + ", " +
						iPeriodosGracia + ", " + iNumeroPagosIntereses + ", " + iPeriodosGraciaIntereses + ", " +
						iNumeroPagosCapital + ", " + iPeriodosGraciaCapital + ", " + formatoDec(dTasaNetaConVigencia) + ", " +
						formatoDec(dTasaNetaConVigencia1Mes) + ", " +
						formatoDec(dSobretasa) + ", " + dTasaNeta + ", " + formatoDec(dTasaNeta1Mes) + ", " + formatoDec(dTasaCreditoUTB) + ", " + iDiasUTB +")";
				System.out.println(strInsert);

				if (datos.guardar(strInsert))
					guardado=true;
				else
					throw new CotizadorException("No se guardaron los datos de la cotización");
			}else
				throw new CotizadorException("No se guardaron los datos de la cotización. No hay conexión a la base de datos.");
		}catch(Exception e){
			System.out.println("---------------------");
			System.out.println("Error en CotizadorPTRBean.guardarCotizacion()");
			e.printStackTrace();
			System.out.println("---------------------");
			throw new CotizadorException("Error al guardar los datos de la cotización. " + "<br>" + e.getMessage());
		}finally{
			datos.release();
		}
	}

	public void setNoReferencia(long var){
		lNoReferencia=var;
	}
	public long getNoReferencia(){
		return lNoReferencia;
	}

	public String getAcuseRecibo(){
		return acuseRecibo;
	}
	public void setAcuseRecibo(String var){
		acuseRecibo=var;
	}

	public void setAreaResponsable(String var){
		strAreaResponsable=var;
	}
	public String getAreaResponsable(){
		return strAreaResponsable;
	}

	public void setIntermediario(String var){
		strIntermediario=var;
	}
	public String getIntermediario(){
		return strIntermediario;
	}

	public void setTipoIntermediario(String var){
		strTipoIntermediario=var;
	}
	public String getTipoIntermediario(){
		return strTipoIntermediario;
	}

	public void setIdTipoIntermediario(int var){
		iIdTipoIntermediario=var;
	}
	public int getIdTipoIntermediario(){
		return iIdTipoIntermediario;
	}
	public void setAcreditado(String var){
		strAcreditado=var;
	}
	public String getAcreditado(){
		return strAcreditado;
	}

	public void setFechaCotizacion(String var){
		strFechaCotizacion=var;
	}
	public String getFechaCotizacion(){
		return strFechaCotizacion;
	}

	public void setFechaDisposicion(String var){
		strFechaDisposicion=var;
	}
	public String getFechaDisposicion(){
		return strFechaDisposicion;
	}

	public void setMontoOperacion(double var){
		dMontoOperacion=var;
	}
	public double getMontoOperacion(){
		return dMontoOperacion;
	}

	public void setUnidadTiempo(int var){
		iUnidadTiempo=var;
	}
	public int getUnidadTiempo(){
		return iUnidadTiempo;
	}

	public void setIdUnidadTiempo(int var){
		iIdUnidadTiempo=var;
	}
	public int getIdUnidadTiempo(){
		return iIdUnidadTiempo;
	}

	public void setPlazoCredito(int var){
		iPlazoCredito=var;
	}
	public int getPlazoCredito(){
		return iPlazoCredito;
	}

	public void setCurvaCotizacion(String var){
		strCurvaCotizacion=var;
	}
	public String getCurvaCotizacion(){
		return strCurvaCotizacion;
	}

	public void setCurvaDuracion(String var){
		strCurvaDuracion=var;
	}
	public String getCurvaDuracion(){
		return strCurvaDuracion;
	}

	public void setUsuario(String var){
		usuario=var;
	}
	public String getUsuario(){
		return usuario;
	}

	public void setGrupoUsuario(String var){
		strGrupoUsuario=var;
	}
	public String getGrupoUsuario(){
		return strGrupoUsuario;
	}

	public void setDuracionCreditoDias(int var){
		iDuracionCreditoDias=var;
	}
	public int getDuracionCreditoDias(){
		return iDuracionCreditoDias;
	}

	public void setDiasUTB(int var){
		iDiasUTB=var;
	}
	public int getDiasUTB(){
		return iDiasUTB;
	}

	public void setTasaCreditoDuracion(double var){
		dTasaCreditoDuracion=var;
	}
	public double getTasaCreditoDuracion(){
		return dTasaCreditoDuracion;
	}

	public void setFactorRiesgo(double var){
		dFactorRiesgo=var;
	}
	public double getFactorRiesgo(){
		return dFactorRiesgo;
	}

	public void setTasaNeta(double var){
		dTasaNeta=var;
	}
	public double getTasaNeta(){
		return dTasaNeta;
	}

	public void setTasaNeta1Mes(double var){
		dTasaNeta1Mes=var;
	}
	public double getTasaNeta1Mes(){
		return dTasaNeta1Mes;
	}

	public void setTasaNetaConVigencia(double var){
		dTasaNetaConVigencia=var;
	}
	public double getTasaNetaConVigencia(){
		return dTasaNetaConVigencia;
	}

	public void setTasaNetaConVigencia1Mes(double var){
		dTasaNetaConVigencia1Mes=var;
	}
	public double getTasaNetaConVigencia1Mes(){
		return dTasaNetaConVigencia1Mes;
	}

	public void setSobretasa(double var){
		dSobretasa=var;
	}
	public double getSobretasa(){
		return dSobretasa;
	}

	public void setPlazoCreditoDias(int var){
		iPlazoCreditoDias=var;
	}
	public int getPlazoCreditoDias(){
		return iPlazoCreditoDias;
	}

	public void setPlazoVigencia(int var){
		iPlazoVigencia=var;
	}
	public int getPlazoVigencia(){
		return iPlazoVigencia;
	}

	public void setTasaCreditoUTB(double var){
		dTasaCreditoUTB=var;
	}
	public double getTasaCreditoUTB(){
		return dTasaCreditoUTB;
	}

	public void setPeriodosGracia(int var){
		iPeriodosGracia=var;
	}
	public int getPeriodosGracia(){
		return iPeriodosGracia;
	}
	public void setNumeroPagosIntereses(int var){
		iNumeroPagosIntereses=var;
	}
	public int getNumeroPagosIntereses(){
		return iNumeroPagosIntereses;
	}
	public void setPeriodosGraciaIntereses(int var){
		iPeriodosGraciaIntereses=var;
	}
	public int getPeriodosGraciaIntereses(){
		return iPeriodosGraciaIntereses;
	}
	public void setNumeroPagosCapital(int var){
		iNumeroPagosCapital=var;
	}
	public int getNumeroPagosCapital(){
		return iNumeroPagosCapital;
	}
	public void setPeriodosGraciaCapital(int var){
		iPeriodosGraciaCapital=var;
	}
	public int getPeriodosGraciaCapital(){
		return iPeriodosGraciaCapital;
	}
 /* public void setSobretasaMasCreditoDuracion(double var){
    dTasaNeta=var;
  }
  public double getSobretasaMasCreditoDuracion(){
    return dTasaNeta;
  }*/


}
