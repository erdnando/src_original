package netropology.utilerias.negocio;


public class ArchivoCurvasIncorrectoException extends Exception{
	public ArchivoCurvasIncorrectoException(String mensaje){
		super(mensaje);
	}
}