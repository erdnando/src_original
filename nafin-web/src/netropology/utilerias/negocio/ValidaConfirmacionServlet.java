package netropology.utilerias.negocio;

import java.io.IOException;

import java.sql.ResultSet;

import java.util.Calendar;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;

public class ValidaConfirmacionServlet extends HttpServlet{
	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  
	public void init(ServletConfig config) throws ServletException{
		super.init(config);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		AccesoDB db=new AccesoDB();
		ResultSet rs=null;
		String strQuery="";
		String strMensajeError="";
		String strIniConfirmacion="";
		String strFinConfirmacion="";
		Calendar calHoraConfirmacion=Calendar.getInstance();
		Calendar calIniConfirmacion=Calendar.getInstance();
		Calendar calFinConfirmacion=Calendar.getInstance();
		boolean excede=false;
		
		response.setContentType(CONTENT_TYPE);
		//PrintWriter out = response.getWriter();
		
		ResultadoCIVBean civ=null;
		ResultadoPPRBean ppr=null;
		ResultadoPTRBean ptr=null;
		HttpSession session= request.getSession();
		String tipoCredito=request.getParameter("tipoCredito");
		String strGrupoUsuario="";
		String titulo="";//t�tulo para la p�gina de error

		try{
			db.conexionDB();
			if (tipoCredito.equals("CIV")){
				titulo="Cotizaci�n de Cr�ditos a Tasa Fija en Moneda Nacional<br>(Pago de Capital e Intereses al Vencimiento: Bono Cup�n Cero)";    
				civ=(ResultadoCIVBean)session.getAttribute("res");
				session.removeAttribute("res");
				request.setAttribute("res", civ);
				strGrupoUsuario=civ.getGrupoUsuario();
			}
			if (tipoCredito.equals("PPR")){
				titulo="Cotizaci�n de Cr�ditos a Tasa Fija en Moneda Nacional <BR>(Pagos Peri�dicos Regulares)";    
				ppr=(ResultadoPPRBean)session.getAttribute("res");
				session.removeAttribute("res");
				request.setAttribute("res", ppr);
				strGrupoUsuario=ppr.getGrupoUsuario();
			}
			if (tipoCredito.equals("PTR")){
				titulo="Cotizaci�n de Cr�ditos a Tasa Fija en Moneda Nacional <BR>(Pagos tipo Renta)";
				ptr=(ResultadoPTRBean)session.getAttribute("res");
				session.removeAttribute("res");
				request.setAttribute("res", ptr);
				strGrupoUsuario=ptr.getGrupoUsuario();
			}

			if (strGrupoUsuario.equalsIgnoreCase("tesoreria")||strGrupoUsuario.equalsIgnoreCase("admin tesoreria")){
				excede=false;
			}else{
				strQuery ="SELECT horarioiniconfirmacion, horariofinconfirmacion " +
							"FROM parametrossistema";    
				rs=db.queryDB(strQuery);
				if (rs.next()){
					strIniConfirmacion=String.valueOf(InformacionGeneralBean.formatoNumeros(rs.getInt("horarioiniconfirmacion"), "0000"));
					strFinConfirmacion=String.valueOf(InformacionGeneralBean.formatoNumeros(rs.getInt("horariofinconfirmacion"), "0000"));
				}else{
					throw new ParamSistemaNoEncontradosException("No se encontraron los par�metros de configuraci�n del sistema en la base de datos");
				}
				db.cierraStatement();

				calIniConfirmacion.set(2002, 0, 1, Integer.parseInt(strIniConfirmacion.substring(0,2)), Integer.parseInt(strIniConfirmacion.substring(2,4)) );
				calFinConfirmacion.set(2002, 0, 1, Integer.parseInt(strFinConfirmacion.substring(0,2)), Integer.parseInt(strFinConfirmacion.substring(2,4)));
				calHoraConfirmacion.set(2002, 0, 1);
        
				if (calIniConfirmacion.getTime().compareTo(calHoraConfirmacion.getTime())> 0){
					excede=true;
					strMensajeError="La operaci�n no puede ser confirmada. El horario de confirmaci�n es de las " +
									InformacionGeneralBean.formatoNumeros(calIniConfirmacion.get(Calendar.HOUR_OF_DAY), "00") + ":" +
									InformacionGeneralBean.formatoNumeros(calIniConfirmacion.get(Calendar.MINUTE), "00") + " hrs. a las " +
									InformacionGeneralBean.formatoNumeros(calFinConfirmacion.get(Calendar.HOUR_OF_DAY), "00") + ":" +
									InformacionGeneralBean.formatoNumeros(calFinConfirmacion.get(Calendar.MINUTE), "00") + " hrs.";
				}else if (calFinConfirmacion.getTime().compareTo(calHoraConfirmacion.getTime())<0){
					excede=true;
					strMensajeError="La operaci�n no puede ser confirmada. El horario de confirmaci�n es de las " +
									InformacionGeneralBean.formatoNumeros(calIniConfirmacion.get(Calendar.HOUR_OF_DAY), "00") + ":" +
									InformacionGeneralBean.formatoNumeros(calIniConfirmacion.get(Calendar.MINUTE), "00") + " hrs. a las " +
									InformacionGeneralBean.formatoNumeros(calFinConfirmacion.get(Calendar.HOUR_OF_DAY), "00") + ":" +
									InformacionGeneralBean.formatoNumeros(calFinConfirmacion.get(Calendar.MINUTE), "00") + " hrs.";
				}
			}

			if (excede){
				//getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/22MuestraErrores.jsp?titulo="+ titulo + "&mensajeError=" + strMensajeError).forward(request, response);
				getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/22MuestraErrores.jsp").forward(request, response);
			}else{
				getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/credito/Confirmacion/ConfirmaCredito1.jsp").forward(request, response);
			}
		}catch (NullPointerException e){
			System.out.println("------------------------");
			System.out.println("ValidaConfirmacionServlet");
			System.out.println("La cotizaci�n no puede confirmarse dos veces");
			e.printStackTrace();
			System.out.println("------------------------");
			String strMensaje="La cotizaci�n ya ha sido confirmada";
			getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/22MuestraErrores.jsp").forward(request, response);
		}catch (Exception e){ 
			System.out.println("------------------------");
			System.out.println("ValidaConfirmacionServlet");
			e.printStackTrace();
			System.out.println("------------------------");
			getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/22MuestraErrores.jsp?").forward(request, response);
		}finally{
			if(db!=null) db.cierraConexionDB();
		}
    //out.close();    
	}
	
} // Fin del Servlet.
