package netropology.utilerias.negocio;


public class ParamSistemaNoEncontradosException extends Exception{
	public ParamSistemaNoEncontradosException(String mensaje){
		super(mensaje);
	}
}