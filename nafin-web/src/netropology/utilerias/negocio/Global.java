package netropology.utilerias.negocio;


public class Global{
	private static String strRutaFisicaArchivosCurvas="";
	private static String strRutaFisicaArchivoFactoresRiesgo=""; 
	private static String strRutaFisicaArchivoRompFondeo="";
	private static String strNombreArchivoRompFondeo="comisionesrompimiento.csv";
	public static String strCREDITOCONFIRMADO="2";
	public static String strCREDITOCOTIZADO="1";

	public static void setRutaFisicaArchivosCurvas(String ruta){
		strRutaFisicaArchivosCurvas=ruta;
	}
	public static void setRutaFisicaArchivoFactoresRiesgo(String ruta){
		strRutaFisicaArchivoFactoresRiesgo=ruta;
	}
	public static void setRutaFisicaArchivoRompFondeo(String ruta){
		strRutaFisicaArchivoRompFondeo=ruta;
	}
	public static String getRutaFisicaArchivosCurvas(){
		return strRutaFisicaArchivosCurvas;
	}
	public static String getRutaFisicaArchivoFactoresRiesgo(){
		return strRutaFisicaArchivoFactoresRiesgo;
	}
	public static String getRutaFisicaArchivoRompFondeo(){
		return strRutaFisicaArchivoRompFondeo;
	}
	public static void setNonbreArchivoRompFondeo(String nombre){
		strNombreArchivoRompFondeo=nombre;
	}
	public static String getNombreArchivoRompFondeo(){
		return strNombreArchivoRompFondeo;
	}

}