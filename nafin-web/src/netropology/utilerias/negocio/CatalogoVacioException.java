package netropology.utilerias.negocio;


public class CatalogoVacioException extends Exception{
	public CatalogoVacioException(String mensaje){
		super(mensaje);
	}
}