package netropology.utilerias.negocio;


public class CotizadorException extends Exception{
	public CotizadorException(String mensaje){
		super(mensaje);
	}
}