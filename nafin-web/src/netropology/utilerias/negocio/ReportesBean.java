/*---------------------------------------------------------
  Autor:          Laura Araceli Cobos Castillo
  Fecha Creaci�n: 13 de abril de 2002
  Nombre clase:   ReportesBean
  Descripci�n:    Proporciona los datos para armar los
                  reportes
-------------------------------------------------------------*/
package netropology.utilerias.negocio;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;

import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ReportesBean{
	private AccesoDB bd = new AccesoDB();
	private ResultSet rs=null;
	private String strTipoReporte;
	private String strGrupoUsuario;
	private String strUsuarioQueConsulta;
	//Factores de Riesgo
	private String strPlazo;
	private String strRiesgo;
	private String strTipoIntermediario;
	private int iNumPlazos;
	//Rompimiento de Fondeo
	private String fondeo[]=new String[20];
	//Cotizaciones
	private String strFechaCotizacion;
	private String strFechaCotizacionA;
	private String strFechaDisposicion;
	private String strFechaDisposicionA;
	private String strNoReferencia;
	private String strUsuario;
	private String strAreaResponsable;
	private String strIntermediario;
	//private String strTipoIntermediario;
	private String strAcreditado;
	private String strMontoOperacion;
	private String strPlazoCredito;
	private String strUnidadTiempo;
	private String strTasaCredito;
	private String strTasaCreditoCurvaMensual;
	private String strPlazoVigencia;
	private String strNumeroPagosIntereses;
	private String strPeriodosGraciaIntereses;
	private String strNumeroPagosCapital;
	private String strPeriodosGraciaCapital;
	private String strDuracionCreditoDias;
	private String strCurvaCotizacion;
	private String strCurvaDuracion;

	//INFOTEC - ARS cambios para agregar una variable para el estatus
	private String strStatusDesc;
	private String strWhere;
  
	/*------------------------------------------------------------------
	  Autor:          Laura Araceli Cobos Castillo
	  Fecha Creaci�n: 13 de abril de 2002
	  Descripci�n:    Realiza la conexi�n a la base de datos y obtiene 
	                  los datos que correspondan dependiendo del reporte
	  Out:            boolean, indica si se complet� la transacci�n 
	                  con �xito o no 
	--------------------------------------------------------------------*/
	public boolean load(String pstrTipoReporte, String pstrGrupoUsuario, String pstrUsuario) throws CotizadorException{
    boolean ok=true;
    this.strTipoReporte=pstrTipoReporte;
	this.strGrupoUsuario=pstrGrupoUsuario;
	this.strUsuarioQueConsulta=pstrUsuario;
	try {
    	this.bd.conexionDB();
		//Reporte: Factores de Riesgo	
		if (strTipoReporte.equalsIgnoreCase("factoresriesgo1")){
			try{
				ok=getFactoresRiesgo(1);
			}catch(Exception e){
				System.out.println("-----------------------------");
				System.out.println("ERROR-ReportesBean.load()::Factores de Riesgo");
				e.printStackTrace();
				System.out.println("-----------------------------");
				throw new CotizadorException("ReportesBean.load()::Factores de Riesgo. "+e.getMessage());
			}
		}
		if (strTipoReporte.equalsIgnoreCase("factoresriesgo54")){
			try{
				ok=getFactoresRiesgo(54);
			}catch(Exception e){
				System.out.println("-----------------------------");
				System.out.println("ERROR-ReportesBean.load()::Factores de Riesgo");
				e.printStackTrace();
				System.out.println("-----------------------------");
				throw new CotizadorException("ReportesBean.load()::Factores de Riesgo. "+e.getMessage());
			}
		}
    
		//Reporte: Rompimiento de Fondeo
		if (strTipoReporte.equalsIgnoreCase("rompimientofondeo")){
			ok=true;
		}
    
		//Reporte: Cotizaciones
		if (strTipoReporte.equalsIgnoreCase("creditosconfirmados")){
			try{
     			if (strGrupoUsuario.equals("tesoreria")){
					if (strWhere==null || strWhere.trim().equals("")){
						System.out.println("ReportesBean.load()::Cotizaciones Tesorer�a. No se estableci� el status del cr�dito");
						throw new CotizadorException("ReportesBean.load()::Listado de Cotizaciones para Tesorer�a. No est� especificado la condici�n de consulta (cr�ditos confirmados o no confirmados)");
					}
				}
				ok=getCreditos(strWhere);
			}catch(Exception e){
				System.out.println("-----------------------------");
				System.out.println("ERROR-ReportesBean.load()::Listado de Cotizaciones");
				e.printStackTrace();
				System.out.println("-----------------------------");
				throw new CotizadorException("ReportesBean.load()::Listado de Cotizaciones. " + e.getMessage());
			}
		}
	}catch(Exception e) { ok=false; }
	return ok;
	}
  
	public boolean eof() throws CotizadorException{
		boolean ok=true;
    	//Reporte: Factores de Riesgo
		try{
			if (strTipoReporte.equals("factoresriesgo1")||strTipoReporte.equals("factoresriesgo54")){
				if (this.rs.next()){
					setPlazo(String.valueOf(this.rs.getInt("plazo")));
					setRiesgo(String.valueOf(this.rs.getDouble("riesgo")));
					setTipoIntermediario(this.rs.getString("descripcion"));
					ok=true;
				}else ok=false;
			}
		}catch(Exception e){
			System.out.println("-----------------------------");
			System.out.println("ERROR-ReportesBean.eof()::Factores de Riesgo");
			e.printStackTrace();
			System.out.println("-----------------------------");
			throw new CotizadorException("ReportesBean.eof()::Factores de Riesgo. " + e.getMessage());
		}
		
		//Reporte: Rompimiento de Fondeo
		if (strTipoReporte.equals("rompimientofondeo")){
			ok=true;
		}
		
		//Reporte: Lista de Cotizaciones
		try{
			if (strTipoReporte.equals("creditosconfirmados")){
				if (this.rs.next()){
					setFechaCotizacion(this.rs.getString("fechaCotizacion"));
					setFechaDisposicion(this.rs.getString("fechaDisposicion"));
					setNoReferencia(String.valueOf(this.rs.getLong("NoReferencia")));
					setUsuario(this.rs.getString("Usuario"));
					setAreaResponsable(this.rs.getString("AreaResponsable"));
					setIntermediario(this.rs.getString("Intermediario"));
					setTipoIntermediario(this.rs.getString("TipoIntermediario"));
					setAcreditado(this.rs.getString("Acreditado"));
					setMontoOperacion(String.valueOf(this.rs.getDouble("MontoOperacion")));
					setPlazoCredito(String.valueOf(this.rs.getInt("PlazoCredito")));
					setUnidadTiempo(this.rs.getString("UnidadTiempo"));
					setTasaCredito(String.valueOf(this.rs.getDouble("TasaCredito")));
					setTasaCreditoCurvaMensual(String.valueOf(this.rs.getDouble("TasaCreditoCurvaMensual")));
					setPlazoVigencia(String.valueOf(this.rs.getInt("PlazoVigencia")));
					setNumeroPagosIntereses(String.valueOf(this.rs.getInt("NumeroPagosIntereses")));
					setPeriodosGraciaIntereses(String.valueOf(this.rs.getInt("PeriodosGraciaIntereses")));
					setNumeroPagosCapital(String.valueOf(this.rs.getInt("NumeroPagosCapital")));
					setPeriodosGraciaCapital(String.valueOf(this.rs.getInt("PeriodosGraciaCapital")));
					setDuracionCreditoDias(String.valueOf(this.rs.getInt("duracionCreditoDias")));
					setCurvaCotizacion(this.rs.getString("CurvaCotizacion"));
					setCurvaDuracion(this.rs.getString("CurvaDuracion"));
					setStatusDesc(this.rs.getString("StatusDesc"));  /* INFOTEC - ARS cambios*/
					
					ok=true;
				}else ok=false;
			}
		}catch(Exception e){
			System.out.println("-----------------------------");
			System.out.println("ERROR-ReportesBean.eof()::Listado de Cotizaciones");
			e.printStackTrace();
			System.out.println("-----------------------------");
			throw new CotizadorException("ReportesBean.eof()::Listado de Cotizaciones. " + e.getMessage());
		}
	return ok;
	}

	public void release(){
		try {
			this.bd.cierraStatement();  
			if(this.bd!=null) this.bd.cierraConexionDB();
		} catch(SQLException sqle) { System.out.println(sqle.getMessage()); }
	}

	/*------------------------------------------------------------------
	  Autor:          Laura Araceli Cobos Castillo
	  Fecha Creaci�n: 13 de abril de 2002
	  Descripci�n:    Obtiene los cr�ditos cotizados o confirmados
	  Out:            boolean, indica si se complet� la transacci�n 
	                  con �xito o no 
	--------------------------------------------------------------------*/
	private boolean getCreditos(String pstrWhere) throws CotizadorException{
	String strQuery="";
	String condicion="";
	boolean regresa=true;
	String estatus="";

/* INFOTEC - ARS antigua query

	strQuery= "SELECT TO_CHAR(cred.fechacotizacion, 'yyyymmdd') fechaCotizacion, " +
              "       TO_CHAR(fechadisposicion, 'yyyymmdd') fechaDisposicion, " +
              "       cred.noreferencia NoReferencia, cred.usuario Usuario, " +
              "       cred.arearesponsable AreaResponsable, cred.intermediario Intermediario, " +
              "       inter.descripcion TipoIntermediario, cred.acreditado Acreditado, " +
              "       cred.monto MontoOperacion, cred.plazo PlazoCredito, ut.descripcion UnidadTiempo, " +
              "       cred.tasanetaconvigencia TasaCredito, cred.tasanetaconvigencia1mes TasaCreditoCurvaMensual, " +
              "       cred.plazovigencia PlazoVigencia, cred.numeropagosintereses NumeroPagosIntereses, " +
              "       cred.periodograciaintereses PeriodosGraciaIntereses, cred.numeropagoscapital NumeroPagosCapital, " +
              "       cred.periodograciacapital PeriodosGraciaCapital, cred.duracionCreditoDias, " +
              "       cur.nombre CurvaCotizacion, cur.nombre CurvaDuracion " +
              "FROM   credito cred, tipointermediario inter, unidadtiempo ut, curva cur " +
              "WHERE  cred.idcurvaduracion = cur.idcurva " +
              "AND    cred.idcurvacotizacion=cur.idcurva " +
              "AND    cred.idunidadtiempo=ut.idunidadtiempo " +
              "AND    cred.idtipointermediario=inter.idtipointermediario ";
INFOTEC -  */

	strQuery= "SELECT TO_CHAR(cred.fechacotizacion, 'yyyymmdd') fechaCotizacion, " +
			"       TO_CHAR(fechadisposicion, 'yyyymmdd') fechaDisposicion, " +
			"       cred.noreferencia NoReferencia, cred.usuario Usuario, " +
			"       cred.arearesponsable AreaResponsable, cred.intermediario Intermediario, " +
			"       inter.descripcion TipoIntermediario, cred.acreditado Acreditado, " +
			"       cred.monto MontoOperacion, cred.plazo PlazoCredito, ut.descripcion UnidadTiempo, " +
			"       cred.tasanetaconvigencia TasaCredito, cred.tasanetaconvigencia1mes TasaCreditoCurvaMensual, " +
			"       cred.plazovigencia PlazoVigencia, cred.numeropagosintereses NumeroPagosIntereses, " +
			"       cred.periodograciaintereses PeriodosGraciaIntereses, cred.numeropagoscapital NumeroPagosCapital, " +
			"       cred.periodograciacapital PeriodosGraciaCapital, cred.duracionCreditoDias, " +
			"       cur.nombre CurvaCotizacion, cur.nombre CurvaDuracion"+
			"		 , st.descripcion StatusDesc "+
		   " FROM   credito cred, tipointermediario inter, unidadtiempo ut, curva cur, StatusCotizacion st " +
			" WHERE  cred.idcurvaduracion = cur.idcurva " +
			" AND    cred.idcurvacotizacion=cur.idcurva " +
			" AND    cred.idunidadtiempo=ut.idunidadtiempo " +
			" AND    cred.idtipointermediario=inter.idtipointermediario "+
			" AND		cred.idstatus=st.idstatus " ;
	if (!strGrupoUsuario.equalsIgnoreCase("tesoreria")&&!strGrupoUsuario.equalsIgnoreCase("admin tesoreria")){
		strQuery+="AND cred.usuario LIKE '" + strUsuarioQueConsulta + "' " ; 
	}

	if(!"".equals(pstrWhere)&&pstrWhere!=null)
		condicion+=" AND cred.idstatus=" + pstrWhere + " " ;
	if(!"".equals(strNoReferencia)&&strNoReferencia!=null)
		condicion += " AND cred.noreferencia="+strNoReferencia;
	if(!"".equals(strIntermediario)&&strIntermediario!=null)
		condicion += " AND cred.intermediario like '%"+strIntermediario+"%'";
	if(!"".equals(strUsuario)&&strUsuario!=null)
		condicion += " AND cred.usuario = '"+strUsuario+"'";
	if(!"".equals(strMontoOperacion)&&strMontoOperacion!=null)
		condicion += " AND cred.monto = "+strMontoOperacion;
	if(!"".equals(strFechaCotizacion)&&strFechaCotizacion!=null&&!"".equals(strFechaCotizacionA)&&strFechaCotizacionA!=null)
		condicion += " and trunc(fechacotizacion) between to_date('"+strFechaCotizacion+"','dd/mm/yyyy') and to_date('"+strFechaCotizacionA+"','dd/mm/yyyy')";
	if(!"".equals(strFechaDisposicion)&&strFechaDisposicion!=null&&!"".equals(strFechaDisposicionA)&&strFechaDisposicionA!=null)
		condicion += " and trunc(fechadisposicion) between to_date('"+strFechaDisposicion+"','dd/mm/yyyy') and to_date('"+strFechaDisposicionA+"','dd/mm/yyyy')";
	if("".equals(condicion))
		condicion += " and trunc(fechadisposicion) = trunc(sysdate)";
	strQuery += condicion;


	strQuery+=" ORDER BY cred.fechacotizacion desc";

	try {
		this.rs=this.bd.queryDB(strQuery);
	} catch(SQLException sqle) { System.out.println(sqle.getMessage()); }
	if (this.rs!=null){
		regresa=true;
	}else 
		throw new CotizadorException("No se obtuvieron cr�ditos para el usuario: " + strUsuarioQueConsulta);;
    
	return regresa;  
	}
  
  /*------------------------------------------------------------------
    Autor:          Laura Araceli Cobos Castillo
    Fecha Creaci�n: 13 de abril de 2002
    Descripci�n:    Obtiene los nombre de los factores de riesgo
    Out:            Vector cuyos elementos son el nombre 
                    de los tipos de factores de riesgo 
  --------------------------------------------------------------------*/
	public Vector getHeaderFactoresRiesgo(String moneda) throws CotizadorException{
	boolean regresa=true;
	String strQuery="";
	ResultSet rsHeaders=null;
	AccesoDB con = new AccesoDB();
	Vector encabezados=new Vector(3,1);

	strQuery = "SELECT DISTINCT descripcion,b.idtipointermediario " +
				" FROM  factoresriesgo a, tipointermediario b " +
				" WHERE a.idtipointermediario = b.idtipointermediario"+
				" AND a.ic_moneda = " + moneda +
				" order by b.idtipointermediario";
	try{
		con.conexionDB();
		rsHeaders=con.queryDB(strQuery);
		while(rsHeaders.next()){
			encabezados.add(rsHeaders.getString("descripcion"));
		}
		con.cierraStatement();
	}catch(Exception e){
		System.out.println("-----------------------------");
		System.out.println("ERROR-ReportesBean.getHeaderFactoresRiesgo()");
		e.printStackTrace();
		System.out.println("-----------------------------");
		throw new CotizadorException("ERROR-ReportesBean.getHeaderFactoresRiesgo(): " + e.getMessage());
	}finally{ if(con!=null) con.cierraConexionDB(); }

	return encabezados;
	}
	
	/*------------------------------------------------------------------
	  Autor:          Laura Araceli Cobos Castillo
	  Fecha Creaci�n: 13 de abril de 2002
	  Descripci�n:    Obtiene los factores de riesgo de la bd
	  Out:            boolean, indica si se complet� la transacci�n 
	                  con �xito o no 
	--------------------------------------------------------------------*/
	private boolean getFactoresRiesgo(int moneda) throws CotizadorException{
		String strQuery="";
		boolean regresa=true;
	
		strQuery = "SELECT COUNT(DISTINCT plazo) as num FROM factoresriesgo "+
				   "WHERE ic_moneda = "+moneda;
		try{
			this.rs=this.bd.queryDB(strQuery);
			if (this.rs.next()) setNumPlazos(this.rs.getInt("num"));
			else regresa=false;
			this.bd.cierraStatement();
	    
			strQuery = "SELECT a.plazo, a.riesgo, b.descripcion "+
						"FROM factoresriesgo a, tipointermediario b " +
						"WHERE a.idtipointermediario=b.idtipointermediario "+
						"AND ic_moneda = " + moneda + 
						"order by a.plazo,a.idtipointermediario";
			System.out.println(strQuery);
			this.rs=this.bd.queryDB(strQuery);
		}catch(Exception e){
			System.out.println("-----------------------------");
			System.out.println("ERROR-ReportesBean.getFactoresRiesgo()");
			throw new CotizadorException(e.getMessage());
		}  
    return regresa;  
	}
	

  /*------------------------------------------------------------------
    Autor:          Laura Araceli Cobos Castillo
    Fecha Creaci�n: 13 de abril de 2002
    Descripci�n:    Lee el archivo comisionesrompimiento.csv
    Out:            Arreglo de cadenas. 
                    Cada elemento del arreglo es una l�nea del archivo
  --------------------------------------------------------------------*/
	public String[] getRompimientoFondeo() throws CotizadorException{
	String renglon="";
	String strNombreArchivo=Global.getRutaFisicaArchivoRompFondeo()+Global.getNombreArchivoRompFondeo();
	int i=0;

		//Verificamos que exista el archivo
		try{
			File archivoALeer = new File(strNombreArchivo);
			if (archivoALeer.exists()){
				//FileReader fDatos=new FileReader(strNombreArchivo);
				//BufferedReader bDatos=new BufferedReader(fDatos);
				BufferedReader bDatos = new BufferedReader (new InputStreamReader(new FileInputStream(archivoALeer), "ISO-8859-1"));
				while((renglon=bDatos.readLine())!=null){
					fondeo[i]=renglon;
					i++;
				}  
			}else{ 
				System.out.println("Error. ReportesBean.getRompimientoFondeo()::El archivo " + archivoALeer.getName() + " no ha sido encontrado");
				throw new CotizadorException("El archivo " + archivoALeer.getName() + " no ha sido encontrado en la ruta " + Global.getRutaFisicaArchivoRompFondeo()+ ".");
			}
			int j=0;
			for (int k=0; k<fondeo.length; k++){
				if (fondeo[k] != null){
					if( !(fondeo[k].trim().equals(""))) j++;
				}
			}
			String fondeo2[]=new String[j];
			for (int k=0; k<fondeo.length; k++){
				if (fondeo[k] != null){
					if (!(fondeo[k].trim().equals(""))) fondeo2[k]=fondeo[k];
				}
			}
			return fondeo2;      
		}catch(Exception e){
			System.out.println("-------------------");
			System.out.println("ReportesBean.getRompimientoFondeo()");
			e.printStackTrace();
			System.out.println("-------------------");
			throw new CotizadorException("Error al generar el reporte: " + e.getMessage());
		}
	}

	//Atributos del reporte Factores de Riesgo por Tipo de Intermediario
	public int getNumPlazos(){
		return iNumPlazos;
	}
  
	private void setNumPlazos(int piNumPlazos){
		iNumPlazos=piNumPlazos;
	}
	
	public String getPlazo(){
		return strPlazo;
	}
	
	private void setPlazo(String pstrPlazo){
		strPlazo=pstrPlazo;
	}
	
	public String getRiesgo(){
		return strRiesgo;
	}
	
	private void setRiesgo(String pstrRiesgo){
		strRiesgo=pstrRiesgo;
	}
	
	public String getTipoIntermediario(){
		return strTipoIntermediario;
	}
	
	private void setTipoIntermediario(String pstrTipoIntermediario){
		strTipoIntermediario=pstrTipoIntermediario;
	}
	//Terminan atributos del reporte Factores de Riesgo por Tipo de Intermediario
	//Comienzan atributos del reporte Cr�ditos Confirmados y Cotizados
	public void setWhere(String var){
		strWhere=var;
	}
	public String getFechaCotizacion(){
		return strFechaCotizacion;
	}
	public void setFechaCotizacion(String var){
		strFechaCotizacion=var;
	}  
	public void setFechaCotizacionA(String var){
		strFechaCotizacionA=var;
	}  	
	public String getFechaDisposicion(){
		return strFechaDisposicion;
	}
	public void setFechaDisposicion(String var){
		strFechaDisposicion=var;
	}
	public void setFechaDisposicionA(String var){
		strFechaDisposicionA=var;
	}	
	public String getNoReferencia(){
		return strNoReferencia;
	}
	public void setNoReferencia(String var){
		strNoReferencia=var;
	}
	public String getUsuario(){
		return strUsuario;
	}
	public void setUsuario(String var){
		strUsuario=var;
	}
	public String getAreaResponsable(){
		return strAreaResponsable;
	}
	public void setAreaResponsable(String var){
		strAreaResponsable=var;
	}
	public String getIntermediario(){
		return strIntermediario;
	}
	public void setIntermediario(String var){
		strIntermediario=var;
	}
/*  public String getTipoIntermediario(){
    return strTipoIntermediario;
  }
  public void setTipoIntermediario(String var){
    strTipoIntermediario=var;
  }*/
	public String getAcreditado(){
		return strAcreditado;
	}
	public void setAcreditado(String var){
		strAcreditado=var;
	}
	public String getMontoOperacion(){
		return strMontoOperacion;
	}
	public void setMontoOperacion(String var){
		strMontoOperacion=var;
	}
	public String getPlazoCredito(){
		return strPlazoCredito;
	}
	public void setPlazoCredito(String var){
		strPlazoCredito=var;
	}
	public String getUnidadTiempo(){
		return strUnidadTiempo;
	}
	public void setUnidadTiempo(String var){
		strUnidadTiempo=var;
	}
	public String getTasaCredito(){
		return strTasaCredito;
	}
	public void setTasaCredito(String var){
		strTasaCredito=var;
	}
	public String getTasaCreditoCurvaMensual(){
		return strTasaCreditoCurvaMensual;
	}
	public void setTasaCreditoCurvaMensual(String var){
		strTasaCreditoCurvaMensual=var;
	}
	public String getPlazoVigencia(){
		return strPlazoVigencia;
	}
	public void setPlazoVigencia(String var){
		strPlazoVigencia=var;
	}
	public String getNumeroPagosIntereses(){
		return strNumeroPagosIntereses;
	}
	public void setNumeroPagosIntereses(String var){
		strNumeroPagosIntereses=var;
	}
	public String getPeriodosGraciaIntereses(){
		return strPeriodosGraciaIntereses;
	}
	public void setPeriodosGraciaIntereses(String var){
		strPeriodosGraciaIntereses=var;
	}
	public String getNumeroPagosCapital(){
		return strNumeroPagosCapital;
	}
	public void setNumeroPagosCapital(String var){
		strNumeroPagosCapital=var;
	}
	public String getPeriodosGraciaCapital(){
		return strPeriodosGraciaCapital;
	}
	public void setPeriodosGraciaCapital(String var){
		strPeriodosGraciaCapital=var;
	}
	public String getDuracionCreditoDias(){
		return strDuracionCreditoDias;
	}
	public void setDuracionCreditoDias(String var){
		strDuracionCreditoDias=var;
	}
	public String getCurvaCotizacion(){
		return strCurvaCotizacion;
	}
	public void setCurvaCotizacion(String var){
		strCurvaCotizacion=var;
	}
	public String getCurvaDuracion(){
		return strCurvaDuracion;
	}
	public void setCurvaDuracion(String var){
		strCurvaDuracion=var;
	}

/*INFOTEC - ARS cambios para agregar los m�todos propios del estatus*/
	public String getStatusDesc(){
		return strStatusDesc;
	}
	public void setStatusDesc(String var){
		strStatusDesc=var;
	}
/*ARS fin */

 //Terminan atributos del reporte Cr�ditos Confirmados y Cotizados
 
 
 ////--------------------------MIGRACION----------------------------------------
 ///---------METODOS NUEVOS------
 
 //Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ReportesBean.class);
	
	StringBuffer qrySentencia = new StringBuffer("");
	
	private   List 	conditions;
	private 	String	moneda;
	private String formato;
 public Registros getEncabezados(){
		log.info("getEncabezados (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;
		PreparedStatement ps = null;
		HashMap datos =  new HashMap();
		Registros registros  = new Registros();
		Registros reg =new  Registros();
		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
 
		try{
			con.conexionDB();
			conditions.add(moneda);
			qrySentencia.append("	SELECT DISTINCT descripcion,b.idtipointermediario   \n"+
										"	FROM  factoresriesgo a, tipointermediario b  \n"+
										"	WHERE a.idtipointermediario = b.idtipointermediario  \n"+
										" 	AND a.ic_moneda = ? \n" + 
										"	order by b.idtipointermediario  \n");
			registros=con.consultarDB(qrySentencia.toString(),conditions);
		}catch(Exception e){
			log.error("getEncabezados  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getEncabezados (S) ");
		return registros;
	}
 public Registros getFactoresRiesgo(){
		log.info("getFactoresRiesgo (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;
		PreparedStatement ps = null;
		HashMap datos =  new HashMap();
		Registros registros  = new Registros();
		Registros reg =new  Registros();
		int iEncabezados = this.getEncabezados().getNumeroRegistros();
		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
		
		
		try{
			con.conexionDB();
			conditions.add(moneda);
			qrySentencia.append("	SELECT   a.plazo, a.riesgo, b.descripcion  \n"+
										"	FROM factoresriesgo a, tipointermediario b  \n"+
										"	WHERE a.idtipointermediario = b.idtipointermediario AND ic_moneda = ?  \n"+
										"	ORDER BY a.plazo, a.idtipointermediario  \n");
			registros=con.consultarDB(qrySentencia.toString(),conditions);
			
			List colNames = new ArrayList();
			colNames.add("PLAZO");
			List data = new ArrayList();
			String plazoAux = "";
			boolean primeraIteracion = true;
			int iteracion = 1;
			List dataRow = new ArrayList();
			String plazo="",riesgo="",descripcion="";
			
			while(registros.next()){
				plazo = registros.getString("plazo");
				riesgo = registros.getString("riesgo");
				riesgo = InformacionGeneralBean.formatoNumeros(Double.parseDouble(riesgo)*100, formato)+"%";
				descripcion = registros.getString("descripcion");
				if(primeraIteracion){
					plazoAux=plazo;
					if(iteracion<iEncabezados){
						colNames.add(descripcion.replaceAll("\\s","_").trim());
						iteracion++;
					}
					if(iteracion==2){
						dataRow.add(plazo);
					}
					dataRow.add(riesgo);
					primeraIteracion= false;
				}else{
					if(plazoAux.equals(plazo)){
						if(iteracion<iEncabezados){
							colNames.add(descripcion.replaceAll("\\s","_").trim());
							iteracion++;
						}
						//dataRow.add(plazo);
						dataRow.add(riesgo);	
					}else{
						//dataRow.add(plazo);
						//dataRow.add(riesgo);
						data.add(dataRow);
						dataRow = new ArrayList();
						primeraIteracion = true;
						if(iteracion<iEncabezados){
							colNames.add(descripcion.replaceAll("\\s","_").trim());
							iteracion++;
						}
						
						dataRow.add(plazo);
						dataRow.add(riesgo);	
					}
				}
			}
			dataRow.add(plazo);
			dataRow.add(riesgo);
			dataRow.add(descripcion);
			colNames.add(descripcion.replaceAll("\\s","_").trim());
			data.add(dataRow);
			reg.setNombreColumnas(colNames);	
			reg.setData(data);
			
		}catch(Exception e){
			log.error("getFactoresRiesgo  Error: " + e);
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getFactoresRiesgo (S) ");
		return reg;
	}

public String generarPDFCSV(HttpServletRequest request,String tipo, Registros titulos, Registros data,String path){
	log.info("generarPDFCSV(E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int cols = titulos.getData().size();
	if("CSV".equals(tipo)){
		try {	
				String header = "";
				contenidoArchivo.append("Plazo,");
				while(titulos.next()){
					String descripcion = titulos.getString("descripcion");
					contenidoArchivo.append(descripcion+","	);						
				}
				contenidoArchivo.append("\n");
			while (data.next())	{
				for(int i = 0; i<(cols+1);i++){
						contenidoArchivo.append(data.getString((i+1))+",");		
				}
				contenidoArchivo.append("\n");
			}
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
		} catch(Exception e) {
			e.printStackTrace();
			
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			log.debug("crearCustomFile (S)");
		}
	
	}else {
		try{
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			//int cols = titulos.getData().size();
			pdfDoc.setTable((cols+1), 100);
			pdfDoc.setCell("Plazo", "celda01", ComunesPDF.CENTER);
			while(titulos.next()){
				pdfDoc.setCell(titulos.getString("descripcion"),"celda01",ComunesPDF.CENTER);
			}
				while (data.next())	{
					for(int i = 0; i<(cols+1);i++){
						pdfDoc.setCell(data.getString((i+1)),"formas",ComunesPDF.CENTER);		
					}
				}
			pdfDoc.addTable();
			pdfDoc.endDocument();
		}catch(Exception e){
			throw new AppException("Error al generar el archivo ", e);
		}
	
	}
	
	return nombreArchivo;
}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setFormato(String formato) {
		this.formato = formato;
	}


	public String getFormato() {
		return formato;
	}
 
 
 
 
}