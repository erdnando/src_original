/*---------------------------------------------------------
  Autor:          Laura Araceli Cobos Castillo
  Fecha Creaci�n: 25 de marzo de 2002
  Empresa:        Air-Go Technologies
  Nombre clase:   InformacionGeneralBean
  Descripci�n:    Proporciona la funcionalidad necesaria 
                  para los jsp's
  �ltima modificaci�n: 10 de abril de 2002
-------------------------------------------------------------*/
package netropology.utilerias.negocio;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import netropology.utilerias.AccesoDB;

public class InformacionGeneralBean{
	private AccesoDB bd = new AccesoDB();
	private ResultSet rs;
	private String intermediario;
	private String tipoIntermediario;
	private String idTipoIntermediario;
	private Hashtable hashIdTipoIntermediario;
	private Hashtable hashFechasCurvas;
	private static String strPasswdUsuario;

	public boolean load() throws CotizadorException{
	boolean ok = true;
		try{
			bd.conexionDB();
		}catch(Exception e){
			ok = false;
			System.out.println("-----------------------");
			System.out.println("InformacionGeneralBean.load()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("InformacionGeneralBean.load(): " + e.getMessage());
		}
	return ok;
    }

	public void release(){
		bd.cierraConexionDB();
	}

		public boolean loadDatosUsuario(String iNoCliente) throws UsuarioNoEncontradoException, CotizadorException{
		String query="";
		boolean ok=false;		
		AccesoDB con = new AccesoDB();
		ResultSet rs = null; 
	
	    try{
		 		 	
			con.conexionDB();
		
			query = "SELECT a.cg_razon_social, a.idtipointermediario, b.descripcion " +
					"FROM comcat_if a, tipointermediario b " +
					"WHERE  a.idtipointermediario = b.idtipointermediario "+
					"AND A.IC_IF =" + iNoCliente;
				System.out.println("--query--------------"+query);  			
			rs = con.queryDB(query);
			if (rs.next()){
				intermediario			=rs.getString("cg_razon_social");
				tipoIntermediario		=rs.getString("descripcion");
				idTipoIntermediario	=rs.getString("idtipointermediario");
				ok=true;
			}else{				
				ok = false;
			}
			rs.close();
			 
			if(!ok)
				throw new UsuarioNoEncontradoException("El intermediario no existe en la BD del Cotizador de Cr�ditos");
		}catch(Exception e){
			e.printStackTrace();			
			throw new CotizadorException("InformacionGeneralBean.loadDatosUsuarios(): " + e.getMessage());
	    } finally{
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		 
	return ok;
	}



	public String getIntermediario(){
		return intermediario;
	}
	
	public String getTipoIntermediario(){
		return tipoIntermediario;
	}
	
	public String getIdTipoIntermediario(){
		return idTipoIntermediario;
	}

	//Regresa c�digo html con el combo de d�as para las fechas
	public String getDias(String pstrNombreCampo, String pstrDiaActual){
		String strHTML="";
		strHTML= "  <SELECT NAME=\"" + pstrNombreCampo + "\"> \n ";
		for (int intI=1; intI<=31; intI++){
			String dia=new String(""+intI);
			if (dia.equals(pstrDiaActual))
				strHTML=strHTML + "<OPTION VALUE=" + intI + " SELECTED>" + intI + "</OPTION> \n";
			else
				strHTML=strHTML + "<OPTION VALUE=" + intI + ">" + intI + "</OPTION> \n";
		}
		strHTML=strHTML + "</SELECT>\n";
	return strHTML;
	}

	//Regresa c�digo html con el combo de meses para las fechas
	public String getMeses(String pstrNombreCampo, String pstrMesActual){
		String strHTML="";
		String meses[]={"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		strHTML="  <SELECT NAME=\"" + pstrNombreCampo + "\"> \n ";
		for (int intI=0; intI<meses.length; intI++){
			String mes = new String(""+intI);
			if (mes.equals(pstrMesActual))
				strHTML= strHTML + "<OPTION VALUE=" + (intI + 1) + " SELECTED>" + meses[intI] +"</OPTION>\n";
			else
				strHTML= strHTML + "<OPTION VALUE=" + (intI + 1) + ">" + meses[intI] + "</OPTION>\n";
		}
		strHTML= strHTML + "</SELECT>\n";
	return strHTML;
	}
  
	//Regresa c�digo html con el combo de a�os para las fechas  
	public String getAnios(String pstrNombreCampo, String pstrAnioActual){  
		String strHTML="";
		strHTML= "  <SELECT NAME=\"" + pstrNombreCampo + "\"> \n ";
		for (int intI=2000; intI<=2010; intI++){
			String anio = new String(""+intI);
			if (anio.equals(pstrAnioActual))
				strHTML=strHTML + "<OPTION VALUE=" + intI + " SELECTED>" + intI +"</OPTION>\n";
			else
				strHTML=strHTML + "<OPTION VALUE=" + intI + ">" + intI + "</OPTION>\n";
		}
		strHTML=strHTML + "</SELECT>\n";
	return strHTML;
	}

	//Regresa c�digo html con el combo de unidades de tiempo  
	public String getUnidadTiempo(String pstrNombreCampo, String pstrSeleccionado) throws CatalogoVacioException, CotizadorException{
	String strHTML="";
	int intI=0;
    
	strHTML= "  <SELECT NAME=\"" + pstrNombreCampo + "\"> \n ";
		try{
			rs=bd.queryDB("SELECT descripcion, duraciondias FROM unidadtiempo");
			while (rs.next()){
				intI++;
				if (rs.getString("duraciondias").equalsIgnoreCase(pstrSeleccionado))
					strHTML+= "<OPTION VALUE=" + rs.getString("duraciondias") + " SELECTED>" + rs.getString("descripcion") +"</OPTION>\n";
				else
					strHTML+= "<OPTION VALUE=" + rs.getString("duraciondias") + ">" + rs.getString("descripcion") +"</OPTION>\n";
			}
			bd.cierraStatement();
			if (intI<1){
				throw new CatalogoVacioException("Tabla \"UNIDADTIEMPO\" vac�a");
			}
		}catch(SQLException e){
			System.out.println("-----------------------");
			System.out.println("InformacionGeneralBean.getUnidadTiempo()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("InformacionGeneralBean.getUnidadTiempo(): " + e.getMessage());
		}
	strHTML+="</SELECT>\n";
	return strHTML;
	}

	//Regresa c�digo html con el combo curvas
	public String getCurva(boolean pblnComboCurvas) throws SQLException, CatalogoVacioException, CotizadorException{
	String strHTML="";
	String strCurvas="";
	int intI=0;
	try{
		if (pblnComboCurvas) {
			rs= bd.queryDB("SELECT nombre FROM curva where ic_moneda = 1");
			while (rs.next()){
				intI++;
				strCurvas= strCurvas + "<OPTION VALUE=" + rs.getString("nombre") + ">" + rs.getString("nombre") +"</OPTION>\n";
			}
			strHTML=strHTML + strCurvas;
			if (intI<1){
				throw new CatalogoVacioException("Tabla \"CURVA\" vac�a");
			}
		}else{
			rs = bd.queryDB("SELECT nombre FROM curva WHERE curvadefault=1 and ic_moneda = 1");
			if (rs.next()){
				strHTML=rs.getString("nombre");
			}else{
				throw new CatalogoVacioException("Tabla \"CURVA\" vac�a");
			}
		}
	}catch(SQLException e){
		System.out.println("-----------------------");
		System.out.println("InformacionGeneralBean.getCurva()");
		e.printStackTrace();
		System.out.println("-----------------------");
		throw new CotizadorException("InformacionGeneralBean.getCurva(): " + e.getMessage());
	}finally{ bd.cierraStatement(); }
      
	return strHTML;
	}

	//Regresa c�digo html con el combo curvas
	//pblnComboCurvas: indica si se debe generar el combo de curvas, 
	//                 o s�lo debe regresar la curva por default
	public String getCurva(boolean pblnComboCurvas, String pstrCurvaSel) throws SQLException, CatalogoVacioException, CotizadorException{
		String strHTML="";
		String strCurvas="";
		int intI=0;
		try{
			if (pblnComboCurvas) {
				rs= bd.queryDB("SELECT nombre FROM curva where ic_moneda = 1");
				while (rs.next()){
					intI++;
					if (pstrCurvaSel.equals(rs.getString("nombre")))
						strCurvas= strCurvas + "<OPTION VALUE=" + rs.getString("nombre") + " SELECTED>" + rs.getString("nombre") +"</OPTION>\n";
					else
						strCurvas= strCurvas + "<OPTION VALUE=" + rs.getString("nombre") + ">" + rs.getString("nombre") +"</OPTION>\n";
				}
				strHTML=strHTML + strCurvas;
				if (intI<1){
					throw new CatalogoVacioException("Tabla \"CURVA\" vac�a");
				}
			}else{
				rs=bd.queryDB("SELECT nombre FROM curva WHERE curvadefault=1 and ic_moneda = 1");
				if (rs.next()){
					strHTML=rs.getString("nombre");
				}else{
					throw new CatalogoVacioException("Tabla \"CURVA\" vac�a");
				}
			}
		}catch(SQLException e){
			System.out.println("-----------------------");
			System.out.println("InformacionGeneralBean.getCurva()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("InformacionGeneralBean.getCurva(): " + e.getMessage());
		} finally{
			bd.cierraStatement();
		}      
	return strHTML;
	}


	//Regresa c�digo html con el combo Tipos de Intermediarios
	public String getComboTipoIntermediario(String strSeleccionado) throws CotizadorException{
		String strHTML="";
		String strDesc="";
		int intI=0;

		try{
			Enumeration desc=hashIdTipoIntermediario.elements();
			while (desc.hasMoreElements()){
				strDesc=(String)desc.nextElement();
				intI++;
				if (strSeleccionado.equals(strDesc)){
					strHTML= strHTML + "<OPTION VALUE=\"" + strDesc + "\" SELECTED>" + strDesc +"</OPTION>\n";
				}else{
					if (intI==1)
						strHTML= strHTML + "<OPTION VALUE=\"" + strDesc + "\" SELECTED>" + strDesc +"</OPTION>\n";
					else
						strHTML= strHTML + "<OPTION VALUE=\"" + strDesc + "\">" + strDesc +"</OPTION>\n";
				}
			}
		}catch(Exception e){
			System.out.println("-----------------------");
			System.out.println("InformacionGeneralBean.getComboTipoIntermediario()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("InformacionGeneralBean.getComboTipoIntermediario(): " + e.getMessage());
		}
	return strHTML;
	}
	
	public void setTablaIdTipoInter(Hashtable var){
		hashIdTipoIntermediario=var;
	}
	
	public Hashtable getTablaIdTipoInter() throws CatalogoVacioException, CotizadorException{
		String strQuery="";
		hashIdTipoIntermediario=new Hashtable();
		int intI=0;
		try{
			strQuery= "SELECT idtipointermediario, descripcion " +
					"FROM tipointermediario ORDER BY idtipointermediario";
			rs= bd.queryDB(strQuery);
			while (rs.next()){
				intI++;
				hashIdTipoIntermediario.put(String.valueOf(rs.getInt("idtipointermediario")),rs.getString("descripcion"));
			}
			bd.cierraStatement();
			if (intI<1){
				throw new CatalogoVacioException("Tabla \"TIPOINTERMEDIARIO\" vac�a");
			}
		}catch(SQLException e){
			System.out.println("-----------------------");
			System.out.println("InformacionGeneralBean.getTablaIdTipoInter()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("InformacionGeneralBean.getTablaIdTipoInter(): " + e.getMessage());
		}
	return hashIdTipoIntermediario;
	}

	//Regresa c�digo html para el combo nombre de la curva
	//Se usa en el m�dulo Costos de Transferencia
	public String getNombreCurvas(String moneda, String nombre) throws CotizadorException, CatalogoVacioException{
		String strHTML="";
		String sel="";
		int curva=0;
		int intI=1;
		hashFechasCurvas=new Hashtable();
		Vector fechas=null;
	    try{
			rs=bd.queryDB("SELECT nombre FROM curva where ic_moneda = "+moneda);
			//Combo con el nombre de las curvas
			while (rs.next()){
        sel="";
				intI++;
        if(nombre.equals(rs.getString("nombre")))
          sel = "selected";
				strHTML= strHTML + "<OPTION VALUE=" + rs.getString("nombre") + " " + sel + ">" + rs.getString("nombre") +"</OPTION>\n";
			}
			bd.cierraStatement();
			if (intI<1){
				throw new CatalogoVacioException("Tabla \"CURVA\" vac�a");
			}
	
	      //Tablas con las correspondencia de curvas y fechas
	/*      bd.cierraStatement();
	      rs=bd.queryDB("SELECT DISTINCT idcurva, fecha FROM datoscurva ORDER BY fecha DESC"); 
	      while(rs.next()){
	        intI++;
	        if (curva==rs.getInt("")){
	
	        hashFechasCurvas.put(String.valueOf(rs.getInt("idcurva")), rs.getString("fecha"));
	                curva=rs.getInt("idcurva");
	      }
	      if (intI<1){
	        throw new CatalogoVacioException("Tabla \"DATOSCURVA\" vac�a");
	      }      */
		}catch(SQLException e){
			System.out.println("-----------------------");
			System.out.println("InformacionGeneralBean.getNombreCurvas()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("InformacionGeneralBean.getNombreCurvas(): " + e.getMessage());
		}
	return strHTML;
	}

	//Regresa c�digo html para el combo nombre de la curva
	//Se usa en el m�dulo Costos de Transferencia
	public String getFechasCurvas() throws CotizadorException{
		String strHTML="";
		String strCurvas="";
		String strFecha="";
		int intI=0;
		try{
			rs=bd.queryDB("SELECT DISTINCT fecha FROM datoscurva ORDER BY fecha DESC"); 
			while (rs.next()){
				intI++;
				strFecha=rs.getString("fecha").substring(6,8)+" de "+InformacionGeneralBean.equivalenteMes(rs.getString("fecha").substring(4,6))+" de "+rs.getString("fecha").substring(0,4);
				strHTML= strHTML + "<OPTION VALUE=" + rs.getString("fecha") + ">" + strFecha +"</OPTION>\n";
			}
			bd.cierraStatement();
			if (intI<1){
				throw new CotizadorException("No existen datos de curvas dados de alta en el sistema");
			}
		}catch(SQLException e){
			System.out.println("-----------------------");
			System.out.println("InformacionGeneralBean.getFechasCurvas()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("InformacionGeneralBean.getFechasCurvas(): " + e.getMessage());
		}
	return strHTML;
	}


	// se usa en las calculadoras de precios
	// obtiene la fecha minima de datos cuva

	public String getMinFechasCurvas() throws CotizadorException{
		String strFecha="";
		String qrySentencia = "";
		int intI=0;
		try{
			PreparedStatement ps = null;
			qrySentencia = 	
				" select /*+ index(d in_datos_curva_03_nuk)*/"+
				" to_char(min(to_date(FECHA,'YYYYmmdd')),'dd/mm/yyyy') as fechamin from datoscurva d"+
				" where to_date(fecha,'YYYYmmdd') >= sysdate - 15";
			ps = bd.queryPrecompilado(qrySentencia);
			rs=ps.executeQuery();
			if (rs.next()){
				strFecha = rs.getString(1);
				intI ++;	
			}
			ps.close();
			if (intI==0){
				throw new CotizadorException("No existen datos de curvas dados de alta en el sistema");
			}
		}catch(SQLException e){
			System.out.println("-----------------------");
			System.out.println("InformacionGeneralBean.getFechasCurvas()");
			e.printStackTrace();
			System.out.println("-----------------------");
			throw new CotizadorException("InformacionGeneralBean.getFechasCurvas(): " + e.getMessage());
		}
	return strFecha;
	}
	

	public static String formatoNumeros(double dMonto, String pstrFormato){
		String numeroFormateado="";
		Locale formatoMX= new Locale("es", "MX");
	    
		NumberFormat g = NumberFormat.getInstance(formatoMX);
		DecimalFormat n = (DecimalFormat)g;
	
		n.applyLocalizedPattern(pstrFormato);
		numeroFormateado=n.format(dMonto);

	return numeroFormateado;   
	}

	//M�todo para formatos de n�meros
	public static String formatoMontos(double dMonto){
		String numeroFormateado="";    
		Locale formatoMX= new Locale("es", "MX");
    
		NumberFormat g = NumberFormat.getInstance(formatoMX);
		DecimalFormat n = (DecimalFormat)g;
    
		n.applyLocalizedPattern("##,##0.00");
		numeroFormateado = n.format(dMonto);
    
	return numeroFormateado;
	}

	//M�todo para formato de porcentajes
	public static String formatoPorcentajes(double dPorcentaje){
		String numeroFormateado="";
		Locale formatoMX= new Locale("es", "MX");
		
		NumberFormat g = NumberFormat.getInstance(formatoMX);
		DecimalFormat n = (DecimalFormat)g;
		
		n.applyLocalizedPattern("##0.00");
		numeroFormateado = n.format(dPorcentaje);
	
	return numeroFormateado;
	}

	public static String equivalenteMes(int piMes){
		String mes="";
		if (piMes == 1) mes="enero";
		if (piMes == 2) mes="febrero";
		if (piMes == 3) mes="marzo";
		if (piMes == 4) mes="abril";
		if (piMes == 5) mes="mayo";
		if (piMes == 6) mes="junio";
		if (piMes == 7) mes="julio";
		if (piMes == 8) mes="agosto";
		if (piMes == 9) mes="septiembre";
		if (piMes == 10) mes="octubre";
		if (piMes == 11) mes="noviembre";
		if (piMes == 12) mes="diciembre";
		return mes;
	}
  
	public static String equivalenteMes(String pstrMes){
		String mes= equivalenteMes(Integer.parseInt(pstrMes));
		return mes;
	}

	//Regresa la cadena con la palabra equivalente a los d�as.
	public static String equivalenteUnidadTiempo(int unidadTiempo){
		String tiempo = "";
		if (unidadTiempo == 1) tiempo = "dias";
		if (unidadTiempo == 7)tiempo = "semanas";
		if (unidadTiempo == 15) tiempo = "quincenas";
		if (unidadTiempo == 30) tiempo = "meses";
		if (unidadTiempo == 60) tiempo = "bimestres";
		if (unidadTiempo == 90) tiempo = "trimestres";
		if (unidadTiempo == 120) tiempo = "cuatrimestres";
		if (unidadTiempo == 180) tiempo = "semestres";
		if (unidadTiempo == 360) tiempo = "a�os";
	
	return tiempo;
	}

	//Convierte un objeto Date en String con el formato yyyymmdd
	public static String obtenFechaEnString(java.util.Date pdtFechaAConvertir){
		String strFechaRetorno = "";
		int iMes=1;
		int iDia=1;
		Calendar objCalendar = Calendar.getInstance();
		objCalendar.setTime(pdtFechaAConvertir);
		strFechaRetorno=String.valueOf(objCalendar.get(Calendar.YEAR)); 
		iMes=objCalendar.get(Calendar.MONTH)+1;
		if (iMes<10)
			strFechaRetorno=strFechaRetorno+ "0" +  String.valueOf(iMes); 
		else
			strFechaRetorno=strFechaRetorno+  String.valueOf(iMes);
		
		iDia=objCalendar.get(Calendar.DATE);
		if (iDia<10)
			strFechaRetorno=strFechaRetorno+ "0" + String.valueOf(iDia); 
		else
			strFechaRetorno=strFechaRetorno+  String.valueOf(iDia); 
	
	return strFechaRetorno;
	}


	//Me regresa el dia, mes o anio actual, y lo uso en el jsp para 
	//mostrar en los combos de fechas la fecha actual por default.
	public String obtenFechaEnString(java.util.Date pdtFechaAConvertir, String pstrSeleccion){	
		String strFechaRetorno="";
		Calendar objCalendar = Calendar.getInstance();
		objCalendar.setTime(pdtFechaAConvertir);
		
		if (pstrSeleccion.equals("dia")){
			strFechaRetorno = String.valueOf(objCalendar.get(Calendar.DATE)); 
		}
		else if (pstrSeleccion.equals("mes")){
			strFechaRetorno = String.valueOf(objCalendar.get(Calendar.MONTH));
		}
		else if (pstrSeleccion.equals("anio")){
			strFechaRetorno = String.valueOf(objCalendar.get(Calendar.YEAR)); 
		}
	
	return strFechaRetorno;
	}

	public Hashtable getParametrosSistema() throws SQLException,ParamSistemaNoEncontradosException{
		Hashtable param=new Hashtable();
		rs= bd.queryDB("SELECT montomaximo, plazomaximo FROM parametrossistema");
		try{
			if (rs.next()){
				param.put("montomaximo", rs.getString("montomaximo"));
				param.put("plazomaximo", rs.getString("plazomaximo"));
			}else
				throw new ParamSistemaNoEncontradosException("No se encontraron los par�metros de configuraci�n del sistema en la base de datos");
			bd.cierraStatement();
		}catch(SQLException e){
			System.out.println("--------------------------");
			System.out.println("InformacionGeneralBean.getParametrosSistema()");
			e.printStackTrace();
			System.out.println("--------------------------");
			throw new ParamSistemaNoEncontradosException("InformacionGeneralBean.getParametrosSistema(): " + e.getMessage());
		}
	return param;
	}

	//M�todo que obtiene el password del usuario, con el objetivo de pas�rselo a un cgi que
	//genera certificador digitales cuando dicho usuario no tiene generado un certificado digital.
	static public boolean loadPasswdUsuario(String strLogin) throws CotizadorException{
		strPasswdUsuario = "";
		return true;
	}
  
	static public String getPasswdUsuario(){
		return strPasswdUsuario;
	}
} // fin de la clase.
