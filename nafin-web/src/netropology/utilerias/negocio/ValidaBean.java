package netropology.utilerias.negocio;

import java.util.Calendar;
import java.util.Hashtable;

public class ValidaBean{
	private String areaResponsable="";
	private String intermediario="";
	private String tipoIntermediario="";
	private String idTipoIntermediario="";
	private String usuario="";
	private String grupoUsuario="";
	private String acreditado="";
	private String diasCotizacion="";
	private String mesesCotizacion="";
	private String aniosCotizacion="";
	private String diasDisposicion="";
	private String mesesDisposicion="";
	private String aniosDisposicion="";
	private String montoOperacion="";
	private String unidadTiempo="";
	private String plazoCredito="";
	private String curvaCotizacion="";
	private String curvaDuracion="";
	private String tipoCredito="";
	//Exclusivas de credito2
	private String periodosGraciaIntereses="";
	private String numeroPagosIntereses="";
	private String periodosGraciaCapital="";
	private String numeroPagosCapital="";
	//Exclusiva de credito3
	private String periodosGracia="";
	private Hashtable error=new Hashtable();

	public boolean valida(){
		boolean datosCorrectos=true;
		Calendar calFechaCotizacion=Calendar.getInstance();
		Calendar calFechaDisposicion=Calendar.getInstance();
		calFechaCotizacion.set(Integer.parseInt(aniosCotizacion), Integer.parseInt(mesesCotizacion)-1, Integer.parseInt(diasCotizacion));
		calFechaDisposicion.set(Integer.parseInt(aniosDisposicion), Integer.parseInt(mesesDisposicion)-1, Integer.parseInt(diasDisposicion));
   
		if (calFechaDisposicion.getTime().compareTo(calFechaCotizacion.getTime())<0){
			error.put("fechaDisposicion", "La fecha de disposici�n debe ser mayor o igual a la fecha de cotizaci�n");
			diasDisposicion=diasCotizacion;
			mesesDisposicion=mesesCotizacion;
			aniosDisposicion=aniosCotizacion;
			datosCorrectos=false;
		}
		if ((calFechaDisposicion.get(Calendar.DAY_OF_WEEK) == 7) || (calFechaDisposicion.get(Calendar.DAY_OF_WEEK) == 1)){
			error.put("fechaDisposicion", "La fecha de disposici�n elegida es un d�a inh�bil");
			diasDisposicion=diasDisposicion;
			mesesDisposicion=mesesDisposicion;
			aniosDisposicion=aniosDisposicion;
			datosCorrectos=false;
		}
		if (grupoUsuario.equalsIgnoreCase("credito")||grupoUsuario.equalsIgnoreCase("tesoreria")||grupoUsuario.equalsIgnoreCase("riesgo")||grupoUsuario.equalsIgnoreCase("admin tesoreria")){
			if (areaResponsable.trim().equals("")){
				error.put("areaResponsable", "Proporcione el �rea responsable, por favor");
				areaResponsable="";
				datosCorrectos=false;
			}
		}
		if (grupoUsuario.equalsIgnoreCase("credito")||grupoUsuario.equalsIgnoreCase("tesoreria")||grupoUsuario.equalsIgnoreCase("riesgo")||grupoUsuario.equalsIgnoreCase("admin tesoreria")){
			if (intermediario.trim().equals("")){
				error.put("intermediario", "Proporcione el intermediario, por favor");
				intermediario="";
				datosCorrectos=false;
			}
		}
		if (acreditado.trim().equals("")){
			error.put("acreditado", "Proporcione el acreditado, por favor");
			acreditado="";
			datosCorrectos=false;
		}
		if (montoOperacion.equals("")){
			error.put("montoOperacion", "Proporcione un monto, por favor");
			montoOperacion="";
			datosCorrectos=false;
		}else{
			try{ 
				double dMontoOperacion= new Double(montoOperacion).doubleValue(); 
				if (dMontoOperacion < 0){
					error.put("montoOperacion", "El monto del cr�dito debe ser un n�mero positivo");
					montoOperacion="";
					datosCorrectos=false;
				}
				if (dMontoOperacion == 0){
					error.put("montoOperacion", "El monto del cr�dito debe ser mayor a cero");
					montoOperacion="";
					datosCorrectos=false;
				}
			}catch(Exception e){
				error.put("montoOperacion", "Proporcione una cantidad v�lida para el monto, por favor");
				montoOperacion="";
				datosCorrectos=false;
			}
		} 
		if (plazoCredito.equals("")){
			error.put("plazoCredito", "Proporcione un plazo para el cr�dito, por favor");
			plazoCredito="";
			datosCorrectos=false;
		}else{
			try{ 
				int iPlazoCredito=new Integer(plazoCredito).intValue(); 
				if (iPlazoCredito < 0){
					error.put("plazoCredito", "El plazo del cr�dito debe ser un n�mero positivo");
					plazoCredito="";
					datosCorrectos=false;
				}
				if (iPlazoCredito == 0){
					error.put("plazoCredito", "El plazo del cr�dito debe ser mayor a cero");
					plazoCredito="";
					datosCorrectos=false;
				}
			}catch(Exception e){
				error.put("plazoCredito", "Proporcione una cantidad v�lida para el plazo del cr�dito, por favor");
				plazoCredito="";
				datosCorrectos=false;
			}
		}
		//Validaciones para el cotizador 2
		if (tipoCredito.equals("PPR")){
			//INTERESES
			if (numeroPagosIntereses.equals("")){
				error.put("numeroPagosIntereses", "Proporcione el n�mero de pagos para los intereses, por favor");
				numeroPagosIntereses="";
				datosCorrectos=false;
			}else{
				try{ 
					int iNumeroPagosIntereses=new Integer(numeroPagosIntereses).intValue();
					if (iNumeroPagosIntereses < 0){
						error.put("numeroPagosIntereses", "El n�mero de Pagos de Intereses debe ser positivo");
						numeroPagosIntereses="";
						datosCorrectos=false;
					}
				}catch(Exception e){
					error.put("numeroPagosIntereses", "Proporcione una cantidad v�lida para el n�mero de Pagos de Intereses, por favor");
					numeroPagosIntereses="";
					datosCorrectos=false;
				}
			}
			
			if (periodosGraciaIntereses.equals("")){
				error.put("periodosGraciaIntereses", "Proporcione el n�mero de per�odos de gracia para los intereses, por favor");
				periodosGraciaIntereses="";
				datosCorrectos=false;
			}else{
				try{ 
					int iPeriodosGraciaIntereses=new Integer(periodosGraciaIntereses).intValue();
					int iNumeroPagosIntereses=new Integer(numeroPagosIntereses).intValue();
					int iPlazoCredito=new Integer(plazoCredito).intValue();
					if ((iPlazoCredito - iPeriodosGraciaIntereses)<=0){
						error.put("periodosGraciaIntereses", "El n�mero de Per�odos de Gracia de Intereses debe ser menor al plazo del cr�dito.");
						periodosGraciaIntereses="";
						datosCorrectos=false;
					}
					if (iPeriodosGraciaIntereses < 0){
						error.put("periodosGraciaIntereses", "El n�mero de Per�odos de Gracia de Intereses debe ser positivo");
						periodosGraciaIntereses="";
						datosCorrectos=false;
					}
					if (!numeroPagosIntereses.equals("")){
						if ((double)((double)(iPlazoCredito-iPeriodosGraciaIntereses)/(double)iNumeroPagosIntereses) != (Math.floor((iPlazoCredito-iPeriodosGraciaIntereses)/iNumeroPagosIntereses)) ){
							if (grupoUsuario.equalsIgnoreCase("tesoreria")|| grupoUsuario.equalsIgnoreCase("admin tesoreria") || grupoUsuario.equalsIgnoreCase("riesgo")){
								error.put("periodosGraciaIntereses", "El n�mero de Pagos y de Per�odos de Gracia de Intereses NO son consistentes con el plazo del cr�dito.");
								periodosGraciaIntereses="";
								numeroPagosIntereses="";
							}else{
								error.put("numeroPagosIntereses", "El n�mero de Pagos NO es consistente con el plazo del cr�dito.");
								periodosGraciaIntereses="0";
								numeroPagosIntereses="";
							}
							datosCorrectos=false;
						}
					}
				}catch(Exception e){
					error.put("periodosGraciaIntereses", "Proporcione una cantidad v�lida de per�odos para los intereses, por favor");
					periodosGraciaIntereses="";
					datosCorrectos=false;
				}
			}
			
			//CAPITAL
			if (numeroPagosCapital.equals("")){
				error.put("numeroPagosCapital", "Proporcione el n�mero de Pagos de Capital, por favor");
				numeroPagosCapital="";
				datosCorrectos=false;
			}else{
				try{ 
					int iNumeroPagosCapital=new Integer(numeroPagosCapital).intValue();
				
					if (iNumeroPagosCapital < 0){
						error.put("numeroPagosCapital", "El n�mero de Pagos de Capital debe ser positivo");
						numeroPagosCapital="";
						datosCorrectos=false;
					}
					if (iNumeroPagosCapital == 0){
						error.put("numeroPagosCapital", "El n�mero de Pagos de Capital debe ser mayor a cero");
						numeroPagosCapital="";
						datosCorrectos=false;
					}
				}catch(Exception e){
					error.put("numeroPagosCapital", "Proporcione una cantidad v�lida para el n�mero de Per�odos de Gracia de Capital, por favor");
					numeroPagosCapital="";
					datosCorrectos=false;
				}
			}
			
			if (periodosGraciaCapital.equals("")){
				error.put("periodosGraciaCapital", "Proporcione el n�mero de Per�odos de Gracia de Capital, por favor");
				periodosGraciaCapital="";
				datosCorrectos=false;
			}else{
				try{
					int iPeriodosGraciaCapital=new Integer(periodosGraciaCapital).intValue();
					int iNumeroPagosCapital=new Integer(numeroPagosCapital).intValue();
					int iPlazoCredito=new Integer(plazoCredito).intValue();
				
					if ((iPlazoCredito - iPeriodosGraciaCapital)<=0){
						error.put("periodosGraciaCapital", "El n�mero de Per�odos de Gracia de Capital debe ser menor al plazo del cr�dito.");
						periodosGraciaCapital="";
						datosCorrectos=false;
					}
					if (iPeriodosGraciaCapital < 0){
						error.put("periodosGraciaCapital", "El n�mero de Per�odos de Gracia de Capital debe ser positivo");
						periodosGraciaCapital="";
						datosCorrectos=false;
					}
					if (!numeroPagosCapital.equals("")){
						if ((double)((double)(iPlazoCredito-iPeriodosGraciaCapital)/(double)iNumeroPagosCapital) != Math.floor((iPlazoCredito-iPeriodosGraciaCapital)/iNumeroPagosCapital)){
							error.put("periodosGraciaCapital", "El n�mero de Pagos y de Per�odos de Gracia de Capital NO son consistentes con el plazo del cr�dito.");
							periodosGraciaCapital="";
							numeroPagosCapital="";
							datosCorrectos=false;
						}
					}
				}catch(Exception e){
					error.put("periodosGraciaCapital", "Proporcione una cantidad v�lida para el n�mero de Per�odos de Capital, por favor");
					periodosGraciaCapital="";
					datosCorrectos=false;
				}
			}
    	}
		
		//Validaciones para el cotizador 3
		if (tipoCredito.equals("PTR")){
			if (periodosGracia.equals("")){
				error.put("periodosGracia", "Proporcione el n�mero de Per�odos de Gracia, por favor");
				periodosGracia="";
				datosCorrectos=false;
			}else{
				try{
					int iPeriodosGracia=new Integer(periodosGracia).intValue();
					int iPlazoCredito=new Integer(plazoCredito).intValue();
					if ((Integer.parseInt(plazoCredito) - iPeriodosGracia)<=0){
						error.put("periodosGracia", "El n�mero de Per�odos de Gracia debe ser menor al plazo del cr�dito.");
						periodosGracia="";
						datosCorrectos=false;
					}
					if (iPeriodosGracia < 0){
						error.put("periodosGracia", "El n�mero de Per�odos de Gracia debe ser positivo");
						periodosGracia="";
						datosCorrectos=false;
					}
					if ((iPlazoCredito-iPeriodosGracia)<=0) {
						error.put("periodosGracia","El n�mero de Per�odos de Gracia debe ser menor al plazo del cr�dito."); 	
						datosCorrectos=false;
					}   
				}catch(Exception e){
					error.put("periodosGracia", "Proporcione un n�mero v�lido para los Per�odos de Gracia, por favor");
					periodosGracia="";
					datosCorrectos=false;
				}
			}
		}
   
	return datosCorrectos;
	}

	public String getMensajeError(String s) {
		String mensajeError =(String)error.get(s.trim());
		return (mensajeError == null) ? "":mensajeError;
	}

	public String getAreaResponsable(){
		return areaResponsable;
	}
	public String getIntermediario(){
		return intermediario;
	}
	public String getTipoIntermediario(){
		return tipoIntermediario;
	}
	public String getIdTipoIntermediario(){
		return idTipoIntermediario;
	}
	public String getUsuario(){
		return usuario;
	}
	public String getGrupoUsuario(){
		return grupoUsuario;
	}
	public String getAcreditado(){
		return acreditado;
	}
	public String getDiasCotizacion(){
		return diasCotizacion;
	}
	public String getMesesCotizacion(){
		return mesesCotizacion;
	}
	public String getAniosCotizacion(){
		return aniosCotizacion;
	}
	public String getDiasDisposicion(){
		return diasDisposicion;
	}
	public String getMesesDisposicion(){
		return mesesDisposicion;
	}
	public String getAniosDisposicion(){
		return aniosDisposicion;
	}
	public String getMontoOperacion(){
		return montoOperacion;
	}
	public String getUnidadTiempo(){
		return unidadTiempo;
	}
	public String getPlazoCredito(){
		return plazoCredito;
	}
	public String getCurvaCotizacion(){
		return curvaCotizacion;
	}
	public String getCurvaDuracion(){
		return curvaDuracion;
	}
	public String getTipoCredito(){
		return tipoCredito;
	}
	public String getPeriodosGraciaIntereses(){
		return periodosGraciaIntereses;
	}
	public String getNumeroPagosIntereses(){
		return numeroPagosIntereses;
	}
	public String getPeriodosGraciaCapital(){
		return periodosGraciaCapital;
	}
	public String getNumeroPagosCapital(){
		return numeroPagosCapital;
	}
	public String getPeriodosGracia(){
		return periodosGracia;
	}
	
	public void setAreaResponsable(String s){
		areaResponsable=s;
	}
	public void setIntermediario(String s){
		intermediario=s;
	}
	public void setTipoIntermediario(String s){
		tipoIntermediario=s;
	}
	public void setIdTipoIntermediario(String s){
		idTipoIntermediario=s;
	}
	public void setUsuario(String s){
		usuario=s;
	}
	public void setGrupoUsuario(String s){
		grupoUsuario=s;
	}
	public void setAcreditado(String s){
		acreditado=s;
	}
	public void setDiasCotizacion(String s){
		diasCotizacion=s;
	}
	public void setMesesCotizacion(String s){
		mesesCotizacion=s;
	}
	public void setAniosCotizacion(String s){
		aniosCotizacion=s;
	}
	public void setDiasDisposicion(String s){
		diasDisposicion=s;
	}
	public void setMesesDisposicion(String s){
		mesesDisposicion=s;
	}
	public void setAniosDisposicion(String s){
		aniosDisposicion=s;
	}
	public void setMontoOperacion(String s){
		montoOperacion=s;
	}
	public void setUnidadTiempo(String s){
		unidadTiempo=s;
	}
	public void setPlazoCredito(String s){
		plazoCredito=s;
	}
	public void setCurvaCotizacion(String s){
		curvaCotizacion=s;
	}
	public void setCurvaDuracion(String s){
		curvaDuracion=s;
	}
	public void setTipoCredito(String s){
		tipoCredito=s;
	}
	
	public void setPeriodosGraciaIntereses(String s){
		periodosGraciaIntereses=s;
	}
	public void setNumeroPagosIntereses(String s){
		numeroPagosIntereses=s;
	}
	public void setPeriodosGraciaCapital(String s){
		periodosGraciaCapital=s;
	}
	public void setNumeroPagosCapital(String s){
		numeroPagosCapital=s;
	}
	public void setPeriodosGracia(String s){
		periodosGracia=s;
	}
	public void setError(String llave, String mensaje) {
		error.put(llave,mensaje);
	}  
}