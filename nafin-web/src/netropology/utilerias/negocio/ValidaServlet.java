package netropology.utilerias.negocio;

import java.io.IOException;

import java.sql.ResultSet;

import java.util.Calendar;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import netropology.utilerias.AccesoDB;

public class ValidaServlet extends HttpServlet{
	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

	public void init(ServletConfig config) throws ServletException{
		super.init(config);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

		ValidaBean f = null; 

		if(request.getAttribute("formHandler") == null){
			System.out.println("formHandler nulo");
			return;
		}
		else{
			System.out.println("formHandler NO nulo");
			f = (ValidaBean) request.getAttribute("formHandler");
		}
		
		System.out.println(f);
		AccesoDB db=new AccesoDB();
		ResultSet rs=null;
		String strQuery="";
		double montomaximo=0d;
		int plazomaximo=0;
		double montoOperacion=0d;
		int plazoCredito=0;
		int unidadTiempo=0;
		boolean excede=false;
		String inicioConsulta="";
		String finConsulta="";
		Calendar horaConsulta=Calendar.getInstance();
		Calendar calIniConsulta=Calendar.getInstance();
		Calendar calFinConsulta=Calendar.getInstance();
		boolean status=false;

		String archSolicitudCotEsp = null;
		String titulo="";//t�tulo para la p�gina de error
		if(request.getAttribute("archSolicitudCotEsp") != null){
			archSolicitudCotEsp=(String)request.getAttribute("archSolicitudCotEsp");
			System.out.println(archSolicitudCotEsp);
			System.out.println("archSolicitudCotEsp NO nulo");
		}
		else{
			System.out.println("archSolicitudCotEsp nulo");
			return;
		}

		if (f.getTipoCredito().equals("CIV")){
			titulo="Cotizaci�n de Cr�ditos a Tasa Fija en Moneda Nacional<br>(Pago de Capital e Intereses al Vencimiento: Bono Cup�n Cero)";    
		}
		if (f.getTipoCredito().equals("PPR")){
			titulo="Cotizaci�n de Cr�ditos a Tasa Fija en Moneda Nacional <BR>(Pagos Peri�dicos Regulares)";    
		}
		if (f.getTipoCredito().equals("PTR")){
			titulo="Cotizaci�n de Cr�ditos a Tasa Fija en Moneda Nacional <BR>(Pagos tipo Renta)";
		}
      
		try{
			if (f.getGrupoUsuario().equalsIgnoreCase("tesoreria")||f.getGrupoUsuario().equalsIgnoreCase("admin tesoreria")){
				excede=false;
			}else{
				db.conexionDB();
				strQuery =" SELECT montomaximo, plazomaximo, " +
							" horarioiniconsulta, horariofinconsulta, estatus " +
							" FROM parametrossistema";
				rs=db.queryDB(strQuery);
				if (rs.next()){
					montomaximo=rs.getDouble("montomaximo");
					plazomaximo=rs.getInt("plazomaximo");
					inicioConsulta=String.valueOf(InformacionGeneralBean.formatoNumeros(rs.getInt("horarioiniconsulta"), "0000"));
					finConsulta=String.valueOf(InformacionGeneralBean.formatoNumeros(rs.getInt("horariofinconsulta"), "0000"));
					if (rs.getString("estatus").equals("1")) status=true;
					else status=false;
				}else{
					throw new ParamSistemaNoEncontradosException("No se encontraron los par�metros de configuraci�n del sistema en la base de datos");
				}
				db.cierraStatement();
				
				//Valido que el sistema no est� bloqueado y que sea la hora correcta de consulta
				calIniConsulta.set(2002, 0, 1, Integer.parseInt(inicioConsulta.substring(0,2)), Integer.parseInt(inicioConsulta.substring(2,4)) );
				calFinConsulta.set(2002, 0, 1, Integer.parseInt(finConsulta.substring(0,2)), Integer.parseInt(finConsulta.substring(2,4)));
				horaConsulta.set(2002, 0, 1);
				if (calIniConsulta.getTime().compareTo(horaConsulta.getTime())> 0){
					excede=true;
					String mensaje= "La operaci�n no se puede cotizar. El horario de consulta es de las " +
									InformacionGeneralBean.formatoNumeros(calIniConsulta.get(Calendar.HOUR_OF_DAY), "00") + ":" +
									InformacionGeneralBean.formatoNumeros(calIniConsulta.get(Calendar.MINUTE), "00") + " hrs. a las " +
									InformacionGeneralBean.formatoNumeros(calFinConsulta.get(Calendar.HOUR_OF_DAY), "00") + ":" +
									InformacionGeneralBean.formatoNumeros(calFinConsulta.get(Calendar.MINUTE), "00") + " hrs.";
					f.setError("horaConsulta", mensaje);
				}else if (calFinConsulta.getTime().compareTo(horaConsulta.getTime())<0){
					excede=true;
					String mensaje= "La operaci�n no se puede cotizar. El horario de consulta es de las " +
									InformacionGeneralBean.formatoNumeros(calIniConsulta.get(Calendar.HOUR_OF_DAY), "00") + ":" +
									InformacionGeneralBean.formatoNumeros(calIniConsulta.get(Calendar.MINUTE), "00") + " hrs. a las " +
									InformacionGeneralBean.formatoNumeros(calFinConsulta.get(Calendar.HOUR_OF_DAY), "00") + ":" +
									InformacionGeneralBean.formatoNumeros(calFinConsulta.get(Calendar.MINUTE), "00") + " hrs.";
					f.setError("horaConsulta", mensaje);
				}
  
				if (status){
					excede=true;
					f.setError("statusSistema", "El Sistema se encuentra temporalmente <B>bloqueado</B>. <BR>Si necesita una cotizaci�n por favor llene el formato <A HREF=\"" + archSolicitudCotEsp + "\">\"Solicitud de Cotizaci�n Especial de Tasa de Inter�s\"</A> y env�elo electr�nicamente a alg�n funcionario de la Direcci�n de Productos de Financiamiento de Nacional Financiera, o en atenci�n de alguna de las personas que se indican en el formato.");
				}
        
				//Valido el monto y el plazo del cr�dito
				montoOperacion = new Double(f.getMontoOperacion()).doubleValue();
				plazoCredito = new Integer(f.getPlazoCredito()).intValue();
				unidadTiempo = new Integer(f.getUnidadTiempo()).intValue();
				plazoCredito = plazoCredito*unidadTiempo;

				if (montoOperacion > montomaximo){
					excede=true;
					f.setError("montoOperacion","El Monto de Operaci�n debe ser menor a " + InformacionGeneralBean.formatoMontos(montomaximo) + " de pesos.<BR>Si necesita un cr�dito mayor a este monto comun�quese directamente a la Direcci�n de Tesorer�a");
				}
				if (plazoCredito > plazomaximo){
					excede=true;
					f.setError("plazoCredito", "El plazo del cr�dito excede del l�mite permitido");
				}
			}
			
			if (excede){
				getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/credito/creditoError.jsp").forward(request, response);
			}else{
				getServletConfig().getServletContext().getRequestDispatcher("/nafin/CotizadorCreditoSERVLET").forward(request, response);
			}
		}catch (Exception e){ 
			System.out.println("------------------------");
			System.out.println("ValidaServlet");
			e.printStackTrace();
			System.out.println("------------------------");
			getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/22MuestraErrores.jsp").forward(request, response);
		}finally{
			if(db!=null) db.cierraConexionDB();
		}
	}
} // Fin del Servlet.
