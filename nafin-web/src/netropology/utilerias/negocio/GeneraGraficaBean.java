package netropology.utilerias.negocio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GeneraGraficaBean{
	private String strArchivoCurva="";
	private String strArchivoGrafica="";
	private String strArchivoScript="";

	public boolean load(String pstrCurva, String pstrRutaGrafica, String pstrRutaCurvas, String pstrRutaGNUPLOT) throws CotizadorException{
		String strTituloGrafica=getTitulo(pstrCurva);
		Runtime ejecuta=null;
		Process proceso=null;
		boolean regreso=false;

		strArchivoCurva=pstrRutaCurvas+pstrCurva+".csv";
		strArchivoGrafica=pstrRutaGrafica+pstrCurva+".gif";
		strArchivoScript=pstrRutaGrafica+pstrCurva+".plt";

		try{
			FileWriter fDatos=new FileWriter(strArchivoScript);
			BufferedWriter bDatosCurva=new BufferedWriter(fDatos);
			bDatosCurva.write("set terminal gif size 500,300\r\n");
			bDatosCurva.write("set output \"" + strArchivoGrafica + "\" \r\n");
			bDatosCurva.write("set data style linespoints \r\n");
			bDatosCurva.write("set title \"" + strTituloGrafica + "\" \r\n");
			bDatosCurva.write("set xlabel \"Plazo (dias)\" \r\n");
			bDatosCurva.write("set xrange [0:3640] \r\n");
			bDatosCurva.write("set xtics 364 \r\n");
			bDatosCurva.write("set ylabel \"Tasa\" \r\n");
			bDatosCurva.write("set format y \"%.2f%%\" \r\n");
			bDatosCurva.write("set grid \r\n");
			bDatosCurva.write("plot \"" + pstrRutaCurvas+pstrCurva+".csv" + "\" using 1:($2*100) '%lf,%lf\' title '" + pstrCurva + "' \r\n");
			bDatosCurva.write("exit \r\n");
			bDatosCurva.close();
      
			ejecuta=Runtime.getRuntime();
			proceso=ejecuta.exec(pstrRutaGNUPLOT + " " + strArchivoScript);
			proceso.waitFor();

			if (proceso.exitValue()!=0){
				regreso=false;
			}else{
				File fGrafica=new File(strArchivoGrafica);
				regreso=fGrafica.exists();
			}
      
		}catch(IOException e){
			throw new CotizadorException("Error en GeneraGraficaBean.load()::" + e.getMessage());
		}catch(InterruptedException e){
			System.out.println("-----------------------------------");
			System.out.println("GeneraGraficaBean.load()");
			e.printStackTrace();
			System.out.println("-----------------------------------");
			throw new CotizadorException("Error en GeneraGraficaBean.load(). El programa GNUPLOT no se ejecut� exitosamente. " + e.getMessage());
		}catch(Exception e){
			throw new CotizadorException("Error en GeneraGraficaBean.load()::" + e.getMessage());
		}
	return regreso;
	}

	public void release(){
		File fPLT=new File(strArchivoScript);
		fPLT.delete();
	}
  
	private String getTitulo(String pstrCurva){
		String strNombreCurva="";
		String strFechaCurva="";
		int iTamano=0;
		
		iTamano=pstrCurva.length();
		strNombreCurva= pstrCurva.substring(8,iTamano);
		strFechaCurva= pstrCurva.substring(0,8);
		
		return strNombreCurva + " del " + strFechaCurva.substring(6,8)+"/"+InformacionGeneralBean.equivalenteMes(strFechaCurva.substring(4,6))+"/"+strFechaCurva.substring(0,4);
	}
	
} // fin de clase.