package netropology.utilerias.negocio;

import java.sql.ResultSet;
import java.sql.SQLException;

import netropology.utilerias.AccesoDB;

public class PersCotizacion{
	String strTipoCotizacion="";
	AccesoDB bd=new AccesoDB();
	ResultSet rs=null;
  
	public PersCotizacion(String pstrTipoCotizacion){
		strTipoCotizacion=pstrTipoCotizacion;
	}
  
	public boolean load() throws Exception{
		boolean ok = true;
		try {
			bd.conexionDB();
		}catch(SQLException sqle) { ok = false; }
	return ok;
	}

	public void release(){
		bd.cierraConexionDB();
	}

	public boolean guardar(String strInsert){
		boolean guardado=true;
		
		try {
			bd.ejecutaSQL(strInsert);
		} catch(Exception e) { guardado=false; }
		bd.terminaTransaccion(guardado);
		
	return guardado;
	}

	public long getNumReferencia() throws CotizadorException, SQLException{
		String strQuery="";
		long inumReferencia=0l;
		
		strQuery="SELECT seq_referencia.nextval FROM dual";
		rs=bd.queryDB(strQuery);
		if (rs.next()){
			inumReferencia=rs.getLong(1);
		}else{
			System.out.println("Error. Resultado" + strTipoCotizacion + "Bean.guardaCotizacion::No se pudo obtener el n�mero de referencia");
			throw new CotizadorException("Resultado" + strTipoCotizacion + "Bean.guardaCotizacion::No se pudo obtener el n�mero de referencia");
		}
		bd.cierraStatement();
		rs=null;
	
	return inumReferencia;
	}

	public int getIDUnidadTiempo(int piUnidadTiempo) throws CotizadorException, SQLException{
		String strQuery="";
		int iIDUnidadTiempo=0;

		strQuery="SELECT idunidadtiempo FROM unidadtiempo WHERE duraciondias=" + piUnidadTiempo;
		rs=bd.queryDB(strQuery);
		if (rs.next()){
			iIDUnidadTiempo=rs.getInt("idunidadtiempo");
		}else{
			System.out.println("Error. Resultado" + strTipoCotizacion + "Bean.guardaCotizacion::No se pudo encontrar el id de la unidad de tiempo");
			throw new CotizadorException("No existe la unidad de tiempo para los d�as " + piUnidadTiempo);
		}

		bd.cierraStatement();
		rs=null;

	return iIDUnidadTiempo;
	}
  
	public int getIDCurva(String pstrCurva) throws CotizadorException, SQLException{
		int iIdCurva=0;

		String strQuery="SELECT idcurva FROM curva WHERE nombre='" + pstrCurva +"'";
		rs=bd.queryDB(strQuery);
		if (rs.next()){
			iIdCurva=rs.getInt("idcurva");
		}else{
			System.out.println("Error. Resultado" + strTipoCotizacion + "Bean.guardaCotizacion::No se pudo encontrar el id de la curva");
			throw new CotizadorException("No existe en la base de datos la curva " + pstrCurva);
		}
		bd.cierraStatement();
		rs=null;

	return iIdCurva;
	}
	
} // fin de la clase.