package netropology.utilerias.negocio;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import netropology.utilerias.AccesoDB;

public class CotizacionCreditosBean{
	//Datos que vienen de los jsp's
	public String areaResponsable;
	public String intermediario;
	public String tipoIntermediario;
	public int idTipoIntermediario;
	public String grupoUsuario;
	public String usuario;
	public String acreditado;
	public java.util.Date fechaCotizacion;
	public java.util.Date fechaDisposicion;
	public double montoOperacion;
	public int unidadTiempo;
	public int plazoCredito;
	public int numeroPagosIntereses;
	public int periodosGraciaIntereses;
	public int numeroPagosCapital;
	public int periodosGraciaCapital;
	public int periodosGracia;  
	public String curvaCotizacion;
	public String curvaDuracion;
	public boolean getFromHistorico = false;

	 //Variables de c�lculo
	 private int iplazoVigencia;
	 private int idiasUTB;
	 private int iplazoCreditoDias;
	 private String strNombreCurvaDuracion;
	 private String strTipoCurvaDuracion;
	 private String strModoCurvaDuracion;
	 private int ibaseCurvaDuracion;
	 private String strNombreCurvaCotizacion;
	 private String strTipoCurvaCotizacion;
	 private String strModoCurvaCotizacion;
	 private int ibaseCurvaCotizacion;
	 private int iplazoOvernight;
	 private double dtasaOvernight;
	 private int iduracionCreditoDias;
	 private double dtasaCreditoDuracion;
	 private double dtasaVigenciaCotizacion;
	 private double dtasaVigenciaOvernight;
	 private double dtasaVigencia;
	 private int iplazoDuracionMasVigencia;
	 private double dtasaDuracionMasVigencia;
	 private double dtasaFwdCredito;
	 private double dsobretasa;
	 private double dfactorRiesgo;
	 private double dtasaNeta;
	 private double dtasaNeta1Mes;
	 private double dtasaNetaConVigencia;
	 private double dtasaNetaConVigencia1Mes;
	 //Exclusivas de Cotizaci�n Por Pagos Peri�dicos Regulares  
	 private double dUTBsPorPeriodoIntereses;
	 private double dUTBsPorPeriodoCapital;
	 private double dTasaCreditoUTB;
	 private double dtasaFijaDuracion;
	 private String strUTBSalida;
	 private double dtasaFwdCreditoUTB;
	 private double dsobretasaMasCreditoDuracion;
	
	 //Objetos de acceso a datos
	 private AccesoDB bd=new AccesoDB();
	 private ResultSet rsCurvaCotizacion=null;
	 private ResultSet rsCurvaDuracion=null;
	 private ResultSet rsDatosCurva=null;

	 private PreparedStatement ps = null;
	 
	 private String query="";

	/*---------------------------------------------------------
	Autor:          Laura Araceli Cobos Castillo
	Fecha Creaci�n: 26 de marzo de 2002
	Nombre m�todo:  getCapitalIntereses()
	Descripci�n:    Calcula la cotizaci�n para los cr�ditos
	                a Tasa Fija en Moneda Nacional (Pago de 
	                Capital e Intereses al Vencimiento: 
	                Bono Cup�n Cero)
	Retorna:        Objeto con los resultados de la cotizaci�n
	-----------------------------------------------------------*/
	public ResultadoCIVBean getCapitalIntereses() throws CotizadorException{
		ResultadoCIVBean civ = new ResultadoCIVBean();
		FuncionesFinancieras funciones = new FuncionesFinancieras();

		try{
			bd.conexionDB();
      
			funciones.setConexion(bd);
			//getQueries();
         getQueryCurvaCot();
			if (rsCurvaCotizacion.next()){
				strNombreCurvaCotizacion=rsCurvaCotizacion.getString("nombre");
				strTipoCurvaCotizacion=rsCurvaCotizacion.getString("tipo");
				strModoCurvaCotizacion=rsCurvaCotizacion.getString("modocalculo");
				ibaseCurvaCotizacion=rsCurvaCotizacion.getInt("base");
			}else{
				throw new CotizadorException("No existe la curva de cotizaci�n " + curvaCotizacion);
			}
			rsCurvaCotizacion.close();
			ps.close();

			getQueryCurvaDur();
        	if (rsCurvaDuracion.next()){
				strNombreCurvaDuracion=rsCurvaDuracion.getString("nombre");
				strTipoCurvaDuracion=rsCurvaDuracion.getString("tipo");
				strModoCurvaDuracion=rsCurvaDuracion.getString("modocalculo");
				ibaseCurvaDuracion=rsCurvaDuracion.getInt("base");
			}else{
				throw new CotizadorException("No existe la curva de duraci�n " + curvaDuracion);
			}
         rsCurvaDuracion.close();
			ps.close();

			getQueryDatosCurva();
			if (rsDatosCurva.next()){
				iplazoOvernight=rsDatosCurva.getInt("plazo");
				dtasaOvernight=rsDatosCurva.getDouble("tasa");
			}else{
				String fecha=InformacionGeneralBean.obtenFechaEnString(fechaCotizacion);
				throw new CotizadorException("No existen datos actualizados de la curva para la fecha de cotizaci�n " + fecha.substring(6,8)+"/"+InformacionGeneralBean.equivalenteMes(fecha.substring(4,6))+"/"+fecha.substring(0,4) );
			}
         rsDatosCurva.close();
			ps.close();
			double milisegundos=(double)(fechaDisposicion.getTime()-fechaCotizacion.getTime())/86400000;
			iplazoVigencia=(int)Math.round(milisegundos);
			idiasUTB=unidadTiempo;
			iplazoCreditoDias=plazoCredito*idiasUTB;

			dfactorRiesgo=funciones.obtenFactorRiesgo(tipoIntermediario,iplazoCreditoDias);
        
			//Calculo la duraci�n del cr�dito
			iduracionCreditoDias=iplazoCreditoDias;

			//Calcula tasa del cr�dito en funci�n de la duraci�n
			String fechaCotCadena=InformacionGeneralBean.obtenFechaEnString(fechaCotizacion);
			dtasaCreditoDuracion= funciones.tasaSpotVector(iduracionCreditoDias, strNombreCurvaCotizacion.trim(),fechaCotCadena, strTipoCurvaCotizacion.trim(), strModoCurvaCotizacion.trim(), ibaseCurvaCotizacion);

			//Calcula la tasa asociada a la vigencia
			dtasaVigenciaCotizacion=funciones.tasaSpotVector(iplazoVigencia,  strNombreCurvaCotizacion.trim(), InformacionGeneralBean.obtenFechaEnString(fechaCotizacion), strTipoCurvaCotizacion.trim(), strModoCurvaCotizacion.trim(), ibaseCurvaCotizacion);
			dtasaVigenciaOvernight=funciones.tre(dtasaOvernight, iplazoOvernight, iplazoVigencia, ibaseCurvaCotizacion);
			if (dtasaVigenciaCotizacion < dtasaVigenciaOvernight) dtasaVigencia=dtasaVigenciaCotizacion;
			else dtasaVigencia=dtasaVigenciaOvernight;

			//Calcula la tasa forward
			iplazoDuracionMasVigencia=iduracionCreditoDias+iplazoVigencia;
			dtasaDuracionMasVigencia=funciones.tasaSpotVector(iplazoDuracionMasVigencia, strNombreCurvaCotizacion.trim(), InformacionGeneralBean.obtenFechaEnString(fechaCotizacion), strTipoCurvaCotizacion.trim(), strModoCurvaCotizacion.trim(), ibaseCurvaCotizacion);
			dtasaFwdCredito=funciones.tasaForward(dtasaVigencia, iplazoVigencia, dtasaDuracionMasVigencia, iplazoDuracionMasVigencia, ibaseCurvaCotizacion);
			if ((dtasaFwdCredito-dtasaCreditoDuracion)>0) dsobretasa=dtasaFwdCredito-dtasaCreditoDuracion;
			else dsobretasa=0;

			//Calcula tasas netas
			dtasaNeta=dtasaCreditoDuracion + dfactorRiesgo;
			dtasaNeta1Mes=funciones.tre(dtasaNeta,iduracionCreditoDias, 30);
			dtasaNetaConVigencia=dtasaCreditoDuracion + dfactorRiesgo + dsobretasa;
			dtasaNetaConVigencia1Mes = funciones.tre(dtasaNetaConVigencia,iduracionCreditoDias , 30);
		}catch(SQLException sqle) {
			throw new CotizadorException("Error al generar la cotizaci�n.");
		}catch(Exception e){ 
			System.out.println("-------------------");
			System.out.println("CotizacionCreditosBean.getCapitalIntereses()");
			e.printStackTrace();
			System.out.println("-------------------");
			throw new CotizadorException("Error al generar la cotizaci�n: " + e.getMessage());
		}finally { 
			if(bd!=null) bd.cierraConexionDB(); 
		}

		//Asigno valores al objeto de tipo ResultadoCIVBean 
		civ.setAreaResponsable(areaResponsable);
		civ.setIntermediario(intermediario);
		civ.setTipoIntermediario(tipoIntermediario);
		civ.setIdTipoIntermediario(idTipoIntermediario);        
		civ.setGrupoUsuario(grupoUsuario);
		civ.setUsuario(usuario);        
		civ.setAcreditado(acreditado);
		civ.setFechaCotizacion(InformacionGeneralBean.obtenFechaEnString(fechaCotizacion));
		civ.setFechaDisposicion(InformacionGeneralBean.obtenFechaEnString(fechaDisposicion));
		civ.setMontoOperacion(montoOperacion);
		civ.setUnidadTiempo(unidadTiempo);
		civ.setPlazoCredito(plazoCredito);
		civ.setPlazoCreditoDias(iplazoCreditoDias);
		civ.setCurvaCotizacion(curvaCotizacion);
		civ.setCurvaDuracion(curvaDuracion);
		civ.setDuracionCreditoDias(iduracionCreditoDias);
		civ.setDiasUTB(idiasUTB);
		civ.setTasaCreditoDuracion(dtasaCreditoDuracion);
		civ.setFactorRiesgo(dfactorRiesgo);
		civ.setTasaNeta(dtasaNeta);
		civ.setTasaNeta1Mes(dtasaNeta1Mes);
		civ.setTasaNetaConVigencia(dtasaNetaConVigencia);
		civ.setTasaNetaConVigencia1Mes(dtasaNetaConVigencia1Mes);
		civ.setSobretasa(dsobretasa);
		civ.setPlazoVigencia(iplazoVigencia);

	return civ;    
	}

	/*---------------------------------------------------------
	Autor:          Ferm�n C�rdenaz
	Fecha Creaci�n: 1 de abril de 2002
	Nombre m�todo:  getPagosPeriodicosRegulares()
	Descripci�n:    Calcula la cotizaci�n para los cr�ditos
	                a Tasa Fija en Moneda Nacional 
	                (Pagos Peri�dicos Regulares) 
	Retorna:        Objeto con los resultados de la cotizaci�n
	Modifcada por:  Laura Araceli Cobos Castillo
	Fecha mod.:     4 de abril de 2002
	-----------------------------------------------------------*/
	public ResultadoPPRBean getPagosPeriodicosRegulares() throws CotizadorException{
	ResultadoPPRBean PPR = new ResultadoPPRBean();
	FuncionesFinancieras funciones=new FuncionesFinancieras();
    try{
		bd.conexionDB();

		funciones.setConexion(bd);
		//getQueries();
      getQueryCurvaCot();
		if (rsCurvaCotizacion.next()){
			strNombreCurvaCotizacion=rsCurvaCotizacion.getString("nombre");
			strTipoCurvaCotizacion=rsCurvaCotizacion.getString("tipo");
			strModoCurvaCotizacion=rsCurvaCotizacion.getString("modocalculo");
			ibaseCurvaCotizacion=rsCurvaCotizacion.getInt("base");
		}else{
			throw new CotizadorException("No existe la curva de cotizaci�n " + curvaCotizacion);
		}
		rsCurvaCotizacion.close();
		ps.close();

		getQueryCurvaDur();     
		if (rsCurvaDuracion.next()){
			strNombreCurvaDuracion=rsCurvaDuracion.getString("nombre");
			strTipoCurvaDuracion=rsCurvaDuracion.getString("tipo");
			strModoCurvaDuracion=rsCurvaDuracion.getString("modocalculo");
			ibaseCurvaDuracion=rsCurvaDuracion.getInt("base");
		}else{
			throw new CotizadorException("No existe la curva de duraci�n " + curvaDuracion);
		}
		rsCurvaDuracion.close();
		ps.close();
		
      getQueryDatosCurva();  
		if (rsDatosCurva.next()){
			iplazoOvernight=rsDatosCurva.getInt("plazo");
			dtasaOvernight=rsDatosCurva.getDouble("tasa");
		}else{
			String fecha=InformacionGeneralBean.obtenFechaEnString(fechaCotizacion);
			throw new CotizadorException("No existen datos actualizados de la curva para la fecha de cotizaci�n " + fecha.substring(6,8)+"/"+InformacionGeneralBean.equivalenteMes(fecha.substring(4,6))+"/"+fecha.substring(0,4) );
		}
		rsDatosCurva.close();
		ps.close();


		double milisegundos=(double)(fechaDisposicion.getTime()-fechaCotizacion.getTime())/86400000;
		iplazoVigencia=(int)Math.round(milisegundos);


		dUTBsPorPeriodoIntereses=(plazoCredito-periodosGraciaIntereses)/numeroPagosIntereses;
		dUTBsPorPeriodoCapital=(plazoCredito-periodosGraciaCapital)/numeroPagosCapital;
		idiasUTB=unidadTiempo;
		iplazoCreditoDias=plazoCredito*idiasUTB;


		//Obtengo el factor de riesgo
		dfactorRiesgo=funciones.obtenFactorRiesgo(tipoIntermediario,iplazoCreditoDias);

        
		//Calculo la duraci�n del cr�dito
		dtasaFijaDuracion = funciones.tasaSpotVector(idiasUTB,strNombreCurvaDuracion.trim(),InformacionGeneralBean.obtenFechaEnString(fechaCotizacion), strTipoCurvaDuracion.trim() , strModoCurvaDuracion.trim() , ibaseCurvaDuracion);
		strUTBSalida="dias";
		iduracionCreditoDias=(int)Math.round(funciones.duracionCreditoTasaFijaTipo2(false, plazoCredito, unidadTiempo, dtasaFijaDuracion, numeroPagosIntereses, dUTBsPorPeriodoIntereses, (double)periodosGraciaIntereses, numeroPagosCapital, dUTBsPorPeriodoCapital, (double)periodosGraciaCapital,strUTBSalida,strNombreCurvaDuracion.trim(), InformacionGeneralBean.obtenFechaEnString(fechaCotizacion),strTipoCurvaDuracion.trim(), strModoCurvaDuracion.trim(), ibaseCurvaDuracion));


		//Calcula tasa del cr�dito en funci�n de la duraci�n
		dtasaCreditoDuracion= funciones.tasaSpotVector((int)iduracionCreditoDias, strNombreCurvaCotizacion.trim(), InformacionGeneralBean.obtenFechaEnString(fechaCotizacion), strTipoCurvaCotizacion.trim(), strModoCurvaCotizacion.trim(), ibaseCurvaCotizacion);
		dTasaCreditoUTB=funciones.tre(dtasaCreditoDuracion,(int)iduracionCreditoDias,idiasUTB,ibaseCurvaCotizacion);


		//Calcula la tasa asociada a la vigencia
		dtasaVigenciaCotizacion=funciones.tasaSpotVector(iplazoVigencia, strNombreCurvaCotizacion.trim(), InformacionGeneralBean.obtenFechaEnString(fechaCotizacion), strTipoCurvaCotizacion.trim(), strModoCurvaCotizacion.trim(), ibaseCurvaCotizacion);
		dtasaVigenciaOvernight=funciones.tre(dtasaOvernight, iplazoOvernight, iplazoVigencia, ibaseCurvaCotizacion);
		if (dtasaVigenciaCotizacion < dtasaVigenciaOvernight) dtasaVigencia=dtasaVigenciaCotizacion;
		else dtasaVigencia=dtasaVigenciaOvernight;


		//Calcula la tasa forward
		iplazoDuracionMasVigencia=iduracionCreditoDias+iplazoVigencia;
		dtasaDuracionMasVigencia=funciones.tasaSpotVector((int)iplazoDuracionMasVigencia, strNombreCurvaCotizacion.trim(), InformacionGeneralBean.obtenFechaEnString(fechaCotizacion), strTipoCurvaCotizacion.trim(), strModoCurvaCotizacion.trim(), ibaseCurvaCotizacion);
		dtasaFwdCredito=funciones.tasaForward(dtasaVigencia, (int)iplazoVigencia, dtasaDuracionMasVigencia, (int)iplazoDuracionMasVigencia, ibaseCurvaCotizacion);
		dtasaFwdCreditoUTB = funciones.tre (dtasaFwdCredito,(int)iduracionCreditoDias,idiasUTB,ibaseCurvaCotizacion); 
		if ((dtasaFwdCredito-dtasaCreditoDuracion)>0) dsobretasa=dtasaFwdCreditoUTB-dTasaCreditoUTB;
		else dsobretasa=0;


		//Calcula tasas netas
		dtasaNeta=dTasaCreditoUTB + dfactorRiesgo;
		dtasaNeta1Mes=funciones.tre(dtasaNeta,idiasUTB, 30);
		dtasaNetaConVigencia=dTasaCreditoUTB + dfactorRiesgo + dsobretasa;
		dtasaNetaConVigencia1Mes = funciones.tre(dtasaNetaConVigencia,idiasUTB , 30);


		dsobretasaMasCreditoDuracion=(double)(dsobretasa+dTasaCreditoUTB);


	}catch(SQLException sqle) {
          throw new CotizadorException("Error al generar la cotizaci�n. "+sqle.getMessage());
    }catch(Exception e){ 
		System.out.println("-------------------");
		System.out.println("CotizacionCreditosBean.getPagosPeriodicosRegulares()");
		e.printStackTrace();
		System.out.println("-------------------");
		throw new CotizadorException("Error al generar la cotizaci�n: " + e.getMessage());
	}finally{
		if(bd!=null) bd.cierraConexionDB(); 
	}

	PPR.setAreaResponsable(areaResponsable);
	PPR.setIntermediario(intermediario);
	PPR.setTipoIntermediario(tipoIntermediario);
	PPR.setIdTipoIntermediario(idTipoIntermediario);        
	PPR.setGrupoUsuario(grupoUsuario);
	PPR.setUsuario(usuario);        
	PPR.setAcreditado(acreditado);
	PPR.setFechaCotizacion(InformacionGeneralBean.obtenFechaEnString(fechaCotizacion));
	PPR.setFechaDisposicion(InformacionGeneralBean.obtenFechaEnString(fechaDisposicion));
	PPR.setMontoOperacion(montoOperacion);
	PPR.setUnidadTiempo(unidadTiempo);
	PPR.setPlazoCredito(plazoCredito);
	PPR.setPlazoCreditoDias(iplazoCreditoDias);
	PPR.setPlazoVigencia(iplazoVigencia);
	PPR.setCurvaCotizacion(curvaCotizacion);
	PPR.setCurvaDuracion(curvaDuracion);
	PPR.setDuracionCreditoDias((int)iduracionCreditoDias);
	PPR.setDiasUTB(idiasUTB);
	PPR.setTasaCreditoDuracion(dtasaCreditoDuracion);
	PPR.setFactorRiesgo(dfactorRiesgo);
	PPR.setTasaNeta(dtasaNeta);
	PPR.setTasaNeta1Mes(dtasaNeta1Mes);
	PPR.setTasaNetaConVigencia(dtasaNetaConVigencia);
	PPR.setTasaNetaConVigencia1Mes(dtasaNetaConVigencia1Mes);
	PPR.setSobretasa(dsobretasa); 
	PPR.setUTBsPorPeriodoIntereses(dUTBsPorPeriodoIntereses ); 
	PPR.setUTBsPorPeriodoCapital(dUTBsPorPeriodoCapital);
	PPR.setNumeroPagosIntereses(numeroPagosIntereses);
	PPR.setPeriodosGraciaIntereses(periodosGraciaIntereses);
	PPR.setNumeroPagosCapital(numeroPagosCapital);
	PPR.setPeriodosGraciaCapital(periodosGraciaCapital);
	PPR.setTasaCreditoUTB(dTasaCreditoUTB);
	/*PPR.setSobretasaMasCreditoDuracion(dsobretasaMasCreditoDuracion);*/
    
	return PPR;    
	}

	/*---------------------------------------------------------
	Autor:          Ferm�n C�rdenaz
	Fecha Creaci�n: 1 de abril de 2002
	Nombre m�todo:  getPagosTipoRenta()
	Descripci�n:    Calcula la cotizaci�n para los cr�ditos
	                a Tasa Fija en Moneda Nacional 
	                (Pagos tipo Renta) 
	Retorna:        Objeto con los resultados de la cotizaci�n
	Modifcada por:  Laura Araceli Cobos Castillo
	Fecha mod.:     4 de abril de 2002
	-----------------------------------------------------------*/
	public ResultadoPTRBean getPagosTipoRenta() throws CotizadorException{
	ResultadoPTRBean PTR = new ResultadoPTRBean();
	FuncionesFinancieras funciones=new FuncionesFinancieras();

	try{
		bd.conexionDB();
		funciones.setConexion(bd);
		//getQueries();

      getQueryCurvaCot();
		if (rsCurvaCotizacion.next()){
			strNombreCurvaCotizacion=rsCurvaCotizacion.getString("nombre");
			strTipoCurvaCotizacion=rsCurvaCotizacion.getString("tipo");
			strModoCurvaCotizacion=rsCurvaCotizacion.getString("modocalculo");
			ibaseCurvaCotizacion=rsCurvaCotizacion.getInt("base");
		}else{
			throw new CotizadorException("No existe la curva de cotizaci�n " + curvaCotizacion);
		}
		rsCurvaCotizacion.close();
		ps.close();
		
		getQueryCurvaDur();
		if (rsCurvaDuracion.next()){
			strNombreCurvaDuracion=rsCurvaDuracion.getString("nombre");
			strTipoCurvaDuracion=rsCurvaDuracion.getString("tipo");
			strModoCurvaDuracion=rsCurvaDuracion.getString("modocalculo");
			ibaseCurvaDuracion=rsCurvaDuracion.getInt("base");
		}else{
			throw new CotizadorException("No existe la curva de duraci�n " + curvaDuracion);
		}
		rsCurvaDuracion.close();
		ps.close();

		getQueryDatosCurva();
		if (rsDatosCurva.next()){
			iplazoOvernight=rsDatosCurva.getInt("plazo");
			dtasaOvernight=rsDatosCurva.getDouble("tasa");
		}else{
			String fecha=InformacionGeneralBean.obtenFechaEnString(fechaCotizacion);
			throw new CotizadorException("No existen datos actualizados de la curva para la fecha de cotizaci�n " + fecha.substring(6,8)+"/"+InformacionGeneralBean.equivalenteMes(fecha.substring(4,6))+"/"+fecha.substring(0,4) );
		}
		rsDatosCurva.close();
		ps.close();

		double milisegundos=(double)(fechaDisposicion.getTime()-fechaCotizacion.getTime())/86400000;
		iplazoVigencia=(int)Math.round(milisegundos);
		idiasUTB=unidadTiempo;
		iplazoCreditoDias=plazoCredito*idiasUTB;

		dfactorRiesgo=funciones.obtenFactorRiesgo(tipoIntermediario,iplazoCreditoDias);

		System.out.println("PLAZO VIGENCIA:" + iplazoVigencia);
		System.out.println("DIAS UTB:" + idiasUTB);
		System.out.println("PLAZO CREDITO DIAS:" + iplazoCreditoDias);
		System.out.println("FACTOR DE RIESGO:" + dfactorRiesgo);
		//Calculo la duracion del credito
		strUTBSalida="dias";
		dtasaFijaDuracion=funciones.tasaSpotVector(idiasUTB,strNombreCurvaDuracion.trim(), InformacionGeneralBean.obtenFechaEnString(fechaCotizacion),strTipoCurvaDuracion.trim(),strModoCurvaDuracion.trim(),ibaseCurvaDuracion);
		iduracionCreditoDias=new Long(Math.round(funciones.duracionCreditoTasaFijaTipo3(false, plazoCredito, unidadTiempo, dtasaFijaDuracion, periodosGracia, strUTBSalida, strNombreCurvaDuracion.trim(), InformacionGeneralBean.obtenFechaEnString(fechaCotizacion), strTipoCurvaDuracion.trim(), strModoCurvaDuracion.trim(), ibaseCurvaDuracion))).intValue();

		System.out.println("TASA FIJA DURACION:" + dtasaFijaDuracion);
		System.out.println("DURACION CREDITO DIAS:" + iduracionCreditoDias);

		//Calcula tasa del cr�dito en funci�n de la duraci�n
		dtasaCreditoDuracion= funciones.tasaSpotVector(iduracionCreditoDias, strNombreCurvaCotizacion.trim(), InformacionGeneralBean.obtenFechaEnString(fechaCotizacion), strTipoCurvaCotizacion.trim(), strModoCurvaCotizacion.trim(), ibaseCurvaCotizacion);
		dTasaCreditoUTB=funciones.tre(dtasaCreditoDuracion,iduracionCreditoDias,idiasUTB,ibaseCurvaCotizacion);

		System.out.println("TASA CREDITO DURACION:" + dtasaCreditoDuracion);
		System.out.println("TASA CREDITO UTB:" + dTasaCreditoUTB);

		//Calcula la tasa asociada a la vigencia
		dtasaVigenciaCotizacion=funciones.tasaSpotVector(iplazoVigencia, strNombreCurvaCotizacion.trim() , InformacionGeneralBean.obtenFechaEnString(fechaCotizacion), strTipoCurvaCotizacion.trim(), strModoCurvaCotizacion.trim(), ibaseCurvaCotizacion);
		dtasaVigenciaOvernight=funciones.tre(dtasaOvernight, iplazoOvernight, iplazoVigencia, ibaseCurvaCotizacion);
		if (dtasaVigenciaCotizacion < dtasaVigenciaOvernight) dtasaVigencia=dtasaVigenciaCotizacion;
		else dtasaVigencia=dtasaVigenciaOvernight;

		System.out.println("TASA VIGENCIA COTIZACION:" + dtasaVigenciaCotizacion);
		System.out.println("TASA VIGENCIA OVERNIGHT:" + dtasaVigenciaOvernight);
		System.out.println("TASA VIGENCIA:" + dtasaVigencia);
        
		//Calcula la tasa forward
		iplazoDuracionMasVigencia=iduracionCreditoDias+iplazoVigencia;
		dtasaDuracionMasVigencia=funciones.tasaSpotVector(iplazoDuracionMasVigencia, strNombreCurvaCotizacion.trim(), InformacionGeneralBean.obtenFechaEnString(fechaCotizacion), strTipoCurvaCotizacion.trim(), strModoCurvaCotizacion.trim(), ibaseCurvaCotizacion);
		dtasaFwdCredito=funciones.tasaForward(dtasaVigencia, iplazoVigencia, dtasaDuracionMasVigencia, iplazoDuracionMasVigencia, ibaseCurvaCotizacion);
		dtasaFwdCreditoUTB = funciones.tre(dtasaFwdCredito,iduracionCreditoDias,idiasUTB,ibaseCurvaCotizacion); 
		if ((dtasaFwdCreditoUTB-dTasaCreditoUTB)>0) dsobretasa=dtasaFwdCreditoUTB-dTasaCreditoUTB;
		else dsobretasa=0;

		System.out.println("PLAZO DURACION MAS VIGENCIA:" + iplazoDuracionMasVigencia);
		System.out.println("TASA DURACION MAS VIGENCIA:" + dtasaDuracionMasVigencia);
		System.out.println("TASA FORWARD CREDITO:" + dtasaFwdCredito);
		System.out.println("TASA FORWARD CREDITO UTB:" + dtasaFwdCreditoUTB);
		System.out.println("SOBRETASA:" + dsobretasa);
        
		//Calcula tasas netas
		dtasaNeta=dTasaCreditoUTB + dfactorRiesgo;
		dtasaNeta1Mes=funciones.tre(dtasaNeta,idiasUTB, 30);
		dtasaNetaConVigencia=dTasaCreditoUTB + dfactorRiesgo + dsobretasa;
		dtasaNetaConVigencia1Mes = funciones.tre(dtasaNetaConVigencia,idiasUTB , 30);

		//dsobretasaMasCreditoDuracion=(double)(dsobretasa+dTasaCreditoUTB);

		System.out.println("TASA NETA:" + dtasaNeta);
		System.out.println("TASA NETA A 1 MES:" + dtasaNeta1Mes);
		System.out.println("TASA NETA CON VIGENCIA:" + dtasaNetaConVigencia);
		System.out.println("TASA NETA CON VIGENCIA A 1 MES:" + dtasaNetaConVigencia1Mes);

	}catch(SQLException sqle) {
		throw new CotizadorException("Error al generar la cotizaci�n. "+sqle.getMessage());
    }catch(Exception e){ 
		System.out.println("-------------------");
		System.out.println("CotizacionCreditosBean.getPagosTipoRenta()");
		e.printStackTrace();
		System.out.println("-------------------");
		throw new CotizadorException("Error al generar la cotizaci�n: " + e.getMessage());
	}finally{
		if(bd!=null) bd.cierraConexionDB();
	}

	PTR.setAcreditado(acreditado);
	PTR.setAreaResponsable(areaResponsable);
	PTR.setCurvaCotizacion(curvaCotizacion);
	PTR.setCurvaDuracion(curvaDuracion);
	PTR.setDiasUTB(idiasUTB);
	PTR.setDuracionCreditoDias(iduracionCreditoDias);
	PTR.setFactorRiesgo(dfactorRiesgo);
	PTR.setFechaCotizacion(InformacionGeneralBean.obtenFechaEnString(fechaCotizacion));
	PTR.setFechaDisposicion(InformacionGeneralBean.obtenFechaEnString(fechaDisposicion));
	PTR.setIntermediario(intermediario);
	PTR.setIdTipoIntermediario(idTipoIntermediario);        
	PTR.setGrupoUsuario(grupoUsuario);
	PTR.setUsuario(usuario);        
	PTR.setMontoOperacion(montoOperacion);
	PTR.setPeriodosGracia(periodosGracia);
	PTR.setNumeroPagosIntereses(numeroPagosIntereses);
	PTR.setPeriodosGraciaIntereses(periodosGraciaIntereses);
	PTR.setNumeroPagosCapital(numeroPagosCapital);
	PTR.setPeriodosGraciaCapital(periodosGraciaCapital);
	PTR.setPlazoCredito(plazoCredito);
	PTR.setPlazoCreditoDias(iplazoCreditoDias);
	PTR.setPlazoVigencia(iplazoVigencia);
	PTR.setSobretasa(dsobretasa);
	PTR.setTasaCreditoDuracion(dtasaCreditoDuracion);
	PTR.setTasaCreditoUTB(dTasaCreditoUTB);
	PTR.setTasaNeta(dtasaNeta);
	PTR.setTasaNeta1Mes(dtasaNeta1Mes);
	PTR.setTasaNetaConVigencia(dtasaNetaConVigencia);
	PTR.setTasaNetaConVigencia1Mes(dtasaNetaConVigencia1Mes);
	PTR.setTipoIntermediario(tipoIntermediario);
	PTR.setUnidadTiempo(unidadTiempo);
	//PTR.setSobretasaMasCreditoDuracion(dsobretasaMasCreditoDuracion);

	return PTR;
	}

	/*---------------------------------------------------------
	Autor:          Laura Araceli Cobos Castillo
	Fecha Creaci�n: 05 de abril de 2002
	Nombre m�todo:  getQueries()
	Descripci�n:    Contiene los queries que traen datos de la 
	                BD para hacer los c�lculos correspondientes.
	                Asigna a recordsets de clase los registros 
	                encontrados, para ser manipulados directamente 
	                por los m�todos que la mandan llamar
	Retorna:        Nada
	-----------------------------------------------------------*/
	/*private void getQueries() throws SQLException{
		if(getFromHistorico){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			CallableStatement cs = bd.ejecutaSP("SP_DEPURA_DATOSCURVA(?,?,?,?)");
			cs.setString(1,"RESP_DATOSCURVA");
			cs.setString(2,"DATOSCURVA");
			cs.setString(3,sdf.format(fechaCotizacion));
			cs.setInt(4,1);
			cs.execute();
		}

	   query = " SELECT b.nombre, b.tipo, b.modocalculo, b.base " +
				" FROM   Curva b " +
				" WHERE  nombre LIKE ?";
		ps = bd.queryPrecompilado(query);
		ps.setString(1,curvaCotizacion);
		rsCurvaCotizacion = ps.executeQuery();
      
		query=" SELECT b.nombre, b.tipo, b.modocalculo, b.base " +
	          " FROM   Curva b " +
	          " WHERE  nombre LIKE ? ";
		ps = bd.queryPrecompilado(query);
		ps.setString(1,curvaDuracion);
		rsCurvaDuracion=ps.executeQuery();*/

		//query = "SELECT /*+ use_nl(b,a) index(a) rowid(b)*/ "+
/*				" a.plazo, a.tasa, a.fecha " +
				" FROM   curva b, DatosCurva a" +
				" WHERE  a.fecha = ?" +
				" AND    a.idcurva=b.idcurva " + 
				" AND    b.nombre LIKE ? ";
		ps = bd.queryPrecompilado(query);
		ps.setString(1,InformacionGeneralBean.obtenFechaEnString(fechaCotizacion));
		ps.setString(2,curvaCotizacion);
		rsDatosCurva=ps.executeQuery();
	}*/


	private void getQueryCurvaCot() throws SQLException{
		if(getFromHistorico){
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			CallableStatement cs = bd.ejecutaSP("SP_DEPURA_DATOSCURVA(?,?,?,?)");
			cs.setString(1,"RESP_DATOSCURVA");
			cs.setString(2,"DATOSCURVA");
			cs.setString(3,sdf.format(fechaCotizacion));
			cs.setInt(4,1);
			cs.execute();
			cs.close();
		}
	   query = " SELECT b.nombre, b.tipo, b.modocalculo, b.base " +
				" FROM   Curva b " +
				" WHERE  nombre LIKE ?";
		ps = bd.queryPrecompilado(query);
		ps.setString(1,curvaCotizacion);
		rsCurvaCotizacion = ps.executeQuery();
      
	}

	private void getQueryCurvaDur() throws SQLException{
      
		query=" SELECT b.nombre, b.tipo, b.modocalculo, b.base " +
	          " FROM   Curva b " +
	          " WHERE  nombre LIKE ? ";
		ps = bd.queryPrecompilado(query);
		ps.setString(1,curvaDuracion);
		rsCurvaDuracion=ps.executeQuery();

		query = "SELECT /*+ use_nl(b,a) index(a) rowid(b)*/ "+
				" a.plazo, a.tasa, a.fecha " +
				" FROM   curva b, DatosCurva a" +
				" WHERE  a.fecha = ?" +
				" AND    a.idcurva=b.idcurva " + 
				" AND    b.nombre LIKE ? ";
		ps = bd.queryPrecompilado(query);
		ps.setString(1,InformacionGeneralBean.obtenFechaEnString(fechaCotizacion));
		ps.setString(2,curvaCotizacion);
		rsDatosCurva=ps.executeQuery();
	}

	private void getQueryDatosCurva() throws SQLException{
      query = "SELECT /*+ use_nl(b,a) index(a) rowid(b)*/ "+
				" a.plazo, a.tasa, a.fecha " +
				" FROM   curva b, DatosCurva a" +
				" WHERE  a.fecha = ?" +
				" AND    a.idcurva=b.idcurva " + 
				" AND    b.nombre LIKE ? ";
		ps = bd.queryPrecompilado(query);
		ps.setString(1,InformacionGeneralBean.obtenFechaEnString(fechaCotizacion));
		ps.setString(2,curvaCotizacion);
		rsDatosCurva=ps.executeQuery();
	}
	
	
	

} // fin de clase.
