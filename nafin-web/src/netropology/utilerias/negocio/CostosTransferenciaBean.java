   package netropology.utilerias.negocio;

import java.sql.ResultSet;
import java.sql.SQLException;

import netropology.utilerias.AccesoDB;

public class CostosTransferenciaBean{
      private AccesoDB bd=new AccesoDB();
      private ResultSet rsCurva;
      private String strFechaCurva;
      private String strNombreCurva;
      private String strIdCurva;
      private String strPlazo;
      private String strTasa;
   
      public boolean load(String pstrNombreCurva, String pstrFechaCurva,String pstrFechaCurvaA,String mascara) throws CotizadorException{
         setFechaCurva(pstrFechaCurva);
         setNombreCurva(pstrNombreCurva);
         boolean regresa=false;
      
         try{
            this.bd.conexionDB();
            regresa=getCurva(pstrNombreCurva, pstrFechaCurva,pstrFechaCurvaA,mascara);
         }
            catch(Exception e){
               System.out.println("CostosTransferenciaBean.load()::No se pudo realizar la conexi�n a la BD");
               throw new CotizadorException("Error en CostosTransferencia.load(): " + e.getMessage());
            }
         return regresa;
      }		
   
      /*public String ultimaFecha(String nomCurva)  throws CotizadorException{
      
         boolean noReg=false;
         AccesoDB dbCom = new AccesoDB();
         ResultSet rsCom;
         String fecha="";
         String query="SELECT max(fecha)FROM datoscurva a, curva b WHERE a.idcurva=b.idcurva";
         try{
            rsCom=dbCom.queryDB(query);
         
            if(rsCom!=null && rsCom.next()){
            
               fecha=rsCom.getString(1);
            }
            else{
            
               noReg=true;
               fecha="";
               throw new CotizadorException("Error en CostosTransferencia.ultimaFecha(String nomCurva). No hay datos para la curva " + nomCurva);
            }
         
            dbCom.cierraStatement();
            dbCom.cierraConexionDB();
         
         }
            catch(SQLException sqle) {
            
               System.out.println(sqle.getMessage());
            }
         if (noReg)
            throw new CotizadorException("Error en CostosTransferencia.ultimaFecha(String nomCurva). No hay datos para la curva " + nomCurva);
         return fecha;
      }*/
      public boolean eof() throws CotizadorException{
         int intI=0;
         boolean regresa=true;
         try{
            if(this.rsCurva.next()){
               intI++;
               setPlazo(this.rsCurva.getString("plazo"));
               setTasa(this.rsCurva.getString("tasa"));
					setFechaCurva(this.rsCurva.getString("fecha"));
               regresa=true;
            }
            else
               regresa=false;
         }
            catch(SQLException e){
               System.out.println("----------------------------------");
               System.out.println("Error en CostosTransferencia.eof()");
               e.printStackTrace();
               System.out.println("----------------------------------");
               throw new CotizadorException("Error en CostosTransferencia.eof()");
            }
         return regresa;
      }
   
   
      public void release(){
         try {
            this.bd.cierraStatement();
         } 
            catch(Exception e) { 
               System.out.println(e.getMessage());
			}  finally{          
					if(this.bd!=null&&this.bd.hayConexionAbierta()) 
						this.bd.cierraConexionDB();
			}
      }
   
      private boolean getCurva(String pstrNombreCurva, String pstrFechaCurva,String pstrFechaCurvaA,String mascara) throws CotizadorException{
         String strQuery="";
         boolean regresa=true;

         try {
				if(pstrFechaCurva==null||pstrFechaCurva.equals("")||pstrFechaCurvaA==null||pstrFechaCurvaA.equals("")){
					strQuery = "SELECT to_char(max(to_date(fecha,'yyyymmdd')),'"+mascara+"') FROM datoscurva a, curva b WHERE b.nombre='" + pstrNombreCurva + "' and a.idcurva=b.idcurva";
					this.rsCurva = this.bd.queryDB(strQuery);
					if(this.rsCurva.next()){
						pstrFechaCurva = pstrFechaCurvaA = this.rsCurva.getString(1);
					} 
					if(pstrFechaCurva==null){
						regresa = false;
						return regresa;
					}
					this.bd.cierraStatement();
					setFechaCurva(pstrFechaCurva);
				}
      
         strQuery = "SELECT distinct a.plazo, a.tasa, b.nombre,fecha FROM datoscurva a, curva b " +
            "WHERE b.nombre='" + pstrNombreCurva + "'"+
				" AND fecha='" + pstrFechaCurva + "'" +
				//" AND to_date(fecha,'"+mascara+"')<=to_date('" + pstrFechaCurvaA + "','"+mascara+"')" +
            " AND a.idcurva=b.idcurva " +
            " ORDER BY fecha,a.plazo ASC";

            this.rsCurva=this.bd.queryDB(strQuery);
         } catch(SQLException sqle) { 
					System.out.println(sqle.getMessage()); 
			}
			regresa=this.rsCurva!=null;
			if (!regresa)
					throw new CotizadorException("Error en CostosTransferencia.getCurva(). No hay datos para la curva " + pstrNombreCurva);
      
         return regresa;
      }

      public String ultimaFecha (String curva) throws CotizadorException{
         AccesoDB dbCom = new AccesoDB();
         String consulta="";
         String fecha="";
         ResultSet rsFecha=null;
         boolean registros=true;
         consulta = "SELECT max(fecha)FROM datoscurva a, curva b WHERE b.nombre='"+curva+"' and a.idcurva=b.idcurva";
         try{
            dbCom.conexionDB();
            rsFecha=dbCom.queryDB(consulta);
         }
            catch(SQLException sqle){
               System.out.println(sqle.getMessage());
            }
            catch(Exception e){
               System.out.println("CostosTransferenciaBean.ultimaFecha()::No se pudo realizar la conexi�n a la BD");
               throw new CotizadorException("Error en CostosTransferencia.ultimaFecha(): " + e.getMessage());
            }
            finally{
               try {
                  registros=rsFecha!=null;
                  if (!registros){
                     dbCom.cierraStatement();
                     if(dbCom!=null)
                        dbCom.cierraConexionDB();
                  }
               } 
                  catch(SQLException sqle) { 
                     System.out.println(sqle.getMessage()); }
            }
      if (!registros)
            throw new CotizadorException("Error en CostosTransferencia.ultimaFecha(). No hay datos para la curva "+curva);
         try{
            while(rsFecha.next())
               fecha=rsFecha.getString(1);
         
         }
            catch(SQLException sqle){
               System.out.println(sqle.getMessage());
            }
            finally{
               try {
                  dbCom.cierraStatement();
                  if(dbCom!=null)dbCom.cierraConexionDB();
               } 
               
                  catch(SQLException sqle) { 
                  
                     System.out.println(sqle.getMessage()); }
            }
         return fecha;
      }
   
      public String getPlazo(){
         return strPlazo;
      }
   
      private void setPlazo(String pstrPlazo){
         strPlazo=pstrPlazo;
      }
   
      public String getTasa(){
         return strTasa;
      }
   
      private void setTasa(String pstrTasa){
         strTasa=pstrTasa;
      }
   
      public String getNombreCurva(){
         return strNombreCurva;
      }
   
      private void setNombreCurva(String pstrNombreCurva){
         strNombreCurva=pstrNombreCurva;
      }
   
      public String getFechaCurva(){
         return strFechaCurva;
      }
   
      private void setFechaCurva(String pstrFechaCurva){
         strFechaCurva=pstrFechaCurva;
      }
   
   }