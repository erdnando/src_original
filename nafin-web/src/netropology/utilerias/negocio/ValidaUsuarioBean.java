package netropology.utilerias.negocio;

import java.util.Hashtable;

public class ValidaUsuarioBean{
	private String strUsuario="";
	private String strIntermediario="";
	private String strTipoIntermediario="";
	private String strAceptar="";
	private boolean blnExisteUsuario;
	private Hashtable error=new Hashtable();
  
	public boolean valida(){
		boolean datosCorrectos=true;

		if (!strAceptar.equalsIgnoreCase("Buscar")){
			if (strUsuario.equals("")){
				error.put("usuario", "Proporcione la clave del usuario, por favor");
				strUsuario="";
				datosCorrectos=false;
			}
			if (strIntermediario.equals("")){
				error.put("intermediario", "Proporcione el intermediario, por favor");
				strIntermediario="";
				datosCorrectos=false;
			}
		}
	return datosCorrectos;
	}

	public String getMensajeError(String s) {
		String mensajeError =(String)error.get(s.trim());
		return (mensajeError == null) ? "":mensajeError;
	}

	public void setError(String llave, String mensaje) {
		error.put(llave,mensaje);
	}  
	
	public String getUsuario(){
		return strUsuario;
	}
	
	public String getIntermediario(){
		return strIntermediario;
	}
	
	public String getTipoIntermediario(){
		return strTipoIntermediario;
	}
	
	public String getAceptar(){
		return strAceptar;
	}
	
	public void setUsuario(String usuario){
		strUsuario=usuario;
	}
	
	public void setIntermediario(String intermediario){
		strIntermediario=intermediario;
	}
	
	public void setTipoIntermediario(String tipoIntermediario){
		strTipoIntermediario=tipoIntermediario;
	}
	
	public void setAceptar(String aceptar){
		strAceptar=aceptar;
	}
	
	public void setExisteUsuario(boolean pblnExisteUsuario){
		blnExisteUsuario=pblnExisteUsuario;
	}
	public boolean getExisteUsuario(){
		return blnExisteUsuario;
	}
}