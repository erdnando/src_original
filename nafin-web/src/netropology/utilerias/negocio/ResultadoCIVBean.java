package netropology.utilerias.negocio;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class ResultadoCIVBean{
	private String acuseRecibo;
	private long noReferencia;
	private String areaResponsable;
	private String intermediario;
	private String tipoIntermediario;
	private int idTipoIntermediario;
	private String grupoUsuario;
	private String usuario;
	private String acreditado;
	private String fechaCotizacion;
	private String fechaDisposicion;
	private double montoOperacion;
	private int unidadTiempo;
	private int idUnidadTiempo;
	private int plazoCredito;
	private String curvaCotizacion;
	private int idCurvaCotizacion;
	private String curvaDuracion;
	private int idCurvaDuracion;
	private int duracionCreditoDias;
	private int diasUTB;
	private double tasaCreditoDuracion;
	private double factorRiesgo;
	private double tasaNeta;
	private double tasaNeta1Mes;
	private double tasaNetaConVigencia;
	private double tasaNetaConVigencia1Mes;
	private double sobretasa;
	private int plazoCreditoDias;
	private int plazoVigencia;
	private String idStatus;

	public boolean confirmarCotizacion() throws Exception{
		PersCotizacion confirma=new PersCotizacion("CIV");
		String strUpdate="";
		boolean confirmado=false;
		
		if (confirma.load()){
			idStatus="2";//status 2=confirmado
			
			strUpdate="UPDATE credito "+
						"SET idStatus=" + idStatus + ", " +
						"    acuseRecibo='" + acuseRecibo + "' " +
						"WHERE noReferencia=" + noReferencia;
			System.out.println(strUpdate);
			confirmado=confirma.guardar(strUpdate);
			confirma.release();
		}
	return confirmado;
	}
  public String formatoDec(double num){
			String formato ="#.#######";
			DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
			simbolo.setDecimalSeparator('.');
			DecimalFormat decFormato = new DecimalFormat(formato, simbolo);
			return decFormato.format(num);

	}

	public void guardarCotizacion() throws CotizadorException{
		PersCotizacion datos=new PersCotizacion("CIV");
		String strInsert="";
		String fechaCot="";
		String fechaDisp="";
		boolean guardado=false;
		try{
			if (datos.load()){
				noReferencia=datos.getNumReferencia();
				idUnidadTiempo=datos.getIDUnidadTiempo(unidadTiempo);
				idCurvaDuracion=datos.getIDCurva(curvaDuracion);
				idCurvaCotizacion=datos.getIDCurva(curvaCotizacion);
				fechaCot=fechaCotizacion.substring(6,8)+"/"+fechaCotizacion.substring(4,6)+"/"+fechaCotizacion.substring(0,4);
				fechaDisp=fechaDisposicion.substring(6,8)+"/"+fechaDisposicion.substring(4,6)+"/"+fechaDisposicion.substring(0,4);
				idStatus="1"; //status 1=cotizado
      
				strInsert="INSERT INTO credito " +
						"(NoReferencia, idStatus, idUnidadTiempo, usuario, idCurvaDuracion, " +
						"idCurvaCotizacion, areaResponsable, intermediario, idTipoIntermediario, " +
						"acreditado, fechaCotizacion, fechaDisposicion, monto, plazo, " +
						"duracionCreditoDias, plazoVigencia, tasaNetaConVigencia, " +
						"tasaNetaConVigencia1Mes, sobretasa, tasaNeta, tasaNeta1mes, diasUTB) " +
						"VALUES(" + noReferencia + ", " + idStatus + ", " + idUnidadTiempo + ", '" + 
						usuario + "', " + idCurvaDuracion + ", " + idCurvaCotizacion + ", '" + 
						areaResponsable + "', '" + intermediario + "', " + idTipoIntermediario + ", '" + 
						acreditado + "', to_date('" + fechaCot + "', 'dd/mm/yyyy'), " +  
						"to_date('" + fechaDisp + "', 'dd/mm/yyyy'), " + montoOperacion + ", " + 
						plazoCredito + ", " + duracionCreditoDias + ", " + plazoVigencia + ", " +
						formatoDec(tasaNetaConVigencia) + ", " + formatoDec(tasaNetaConVigencia1Mes) + ", " + formatoDec(sobretasa) + ", " + 
						formatoDec(tasaNeta) + ", " + formatoDec(tasaNeta1Mes) + ", " + diasUTB + ")";
				guardado=datos.guardar(strInsert);
				datos.release();
			}else
				throw new CotizadorException("No hay conexión a la base de datos.");
		}catch(Exception e){
			datos.release();
			System.out.println("---------------------");
			System.out.println("Error en CotizadorCIVBean.guardarCotizacion()");
			e.printStackTrace();
			System.out.println("---------------------");
			throw new CotizadorException("Error al guardar los datos de la cotización. " + "<br>" + e.getMessage());
		}
	}

	public long getNoReferencia(){
		return noReferencia;
	}
	
	public String getAcuseRecibo(){
		return acuseRecibo;
	}
	
	public void setAcuseRecibo(String var){
		acuseRecibo=var;
	}
	
	public void setAreaResponsable(String var){
		areaResponsable=var;
	}
	
	public String getAreaResponsable(){
		return areaResponsable;
	}
	
	public void setIntermediario(String var){
		intermediario=var;
	}
	
	public String getIntermediario(){
		return intermediario;
	}
	
	public void setTipoIntermediario(String var){
		tipoIntermediario=var;
	}
	
	public String getTipoIntermediario(){
		return tipoIntermediario;
	}

	public void setIdTipoIntermediario(int var){
		idTipoIntermediario=var;
	}
	
	public int getIdTipoIntermediario(){
		return idTipoIntermediario;
	}
	
	public void setGrupoUsuario(String var){
		grupoUsuario=var;
	}
	public String getGrupoUsuario(){
		return grupoUsuario;
	}
	
	public void setUsuario(String var){
		usuario=var;
	}
	
	public String getUsuario(){
		return usuario;
	}
	
	public void setAcreditado(String var){
		acreditado=var;
	}
	
	public String getAcreditado(){
		return acreditado;
	}
	
	public void setFechaCotizacion(String var){
		fechaCotizacion=var;
	}
	
	public String getFechaCotizacion(){
		return fechaCotizacion;
	}
	
	public void setFechaDisposicion(String var){
		fechaDisposicion=var;
	}
	
	public String getFechaDisposicion(){
		return fechaDisposicion;
	}
	
	public void setMontoOperacion(double var){
		montoOperacion=var;
	}
	public double getMontoOperacion(){
		return montoOperacion;
	}
	
	public void setUnidadTiempo(int var){
		unidadTiempo=var;
	}
	public int getUnidadTiempo(){
		return unidadTiempo;
	}
	
	public void setPlazoCredito(int var){
		plazoCredito=var;
	}
	public int getPlazoCredito(){
		return plazoCredito;
	}

	public void setCurvaCotizacion(String var){
		curvaCotizacion=var;
	}
	public String getCurvaCotizacion(){
		return curvaCotizacion;
	}
	
	public void setCurvaDuracion(String var){
		curvaDuracion=var;
	}
	public String getCurvaDuracion(){
		return curvaDuracion;
	}
	
	public void setDuracionCreditoDias(int var){
		duracionCreditoDias=var;
	}
	public int getDuracionCreditoDias(){
		return duracionCreditoDias;
	}
	
	public void setDiasUTB(int var){
		diasUTB=var;
	}
	public int getDiasUTB(){
		return diasUTB;
	}
	
	public void setTasaCreditoDuracion(double var){
		tasaCreditoDuracion=var;
	}
	public double getTasaCreditoDuracion(){
		return tasaCreditoDuracion;
	}
	
	public void setFactorRiesgo(double var){
		factorRiesgo=var;
	}
	public double getFactorRiesgo(){
		return factorRiesgo;
	}
	
	public void setTasaNeta(double var){
		tasaNeta=var;
	}
	public double getTasaNeta(){
		return tasaNeta;
	}
	
	public void setTasaNeta1Mes(double var){
		tasaNeta1Mes=var;
	}
	
	public double getTasaNeta1Mes(){
		return tasaNeta1Mes;
	}
	
	public void setTasaNetaConVigencia(double var){
		tasaNetaConVigencia=var;
	}
	
	public double getTasaNetaConVigencia(){
		return tasaNetaConVigencia;
	}
	
	public void setTasaNetaConVigencia1Mes(double var){
		tasaNetaConVigencia1Mes=var;
	}
	
	public double getTasaNetaConVigencia1Mes(){
		return tasaNetaConVigencia1Mes;
	}
	
	public void setSobretasa(double var){
		sobretasa=var;
	}
	
	public double getSobretasa(){
		return sobretasa;
	}
	
	public void setPlazoCreditoDias(int var){
		plazoCreditoDias=var;
	}
	
	public int getPlazoCreditoDias(){
		return plazoCreditoDias;
	}
	
	public void setPlazoVigencia(int var){
		plazoVigencia=var;
	}
	
	public int getPlazoVigencia(){
		return plazoVigencia;
	}

}