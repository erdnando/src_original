package netropology.utilerias.negocio;

import java.io.IOException;

import java.sql.ResultSet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import netropology.utilerias.AccesoDB;

public class ValidaUsuarioServlet extends HttpServlet{
	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

	public void init(ServletConfig config) throws ServletException{
		super.init(config);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
	ValidaUsuarioBean f = (ValidaUsuarioBean) request.getAttribute("usuarioHandler"); 
	AccesoDB db=new AccesoDB();
	ResultSet rs=null;
	String strQuery="";
	String usuario="";
	boolean existe=false;
	boolean error=false;

	String titulo="Asignaci&oacute;n de Tipo de Riesgo e Intermediario";//t�tulo para la p�gina de error

		try{
	        db.conexionDB();
			usuario=f.getUsuario();
	
			strQuery="SELECT usuario FROM usuario WHERE usuario LIKE '" + usuario + "'";
			rs=db.queryDB(strQuery);
			if (rs.next()){
				existe=true;
				error=false;
			}
			else{
				existe=false;
				if (f.getAceptar().equals("eliminar")){
					error=true;
					f.setError("usuario", "El usuario no puede ser eliminado porque no existe.");
				}
			}

			f.setExisteUsuario(existe);
			request.setAttribute("datosUsuario", f);
			if (error){
				getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/Administracion/AdmonUsuariosError.jsp").forward(request, response);
			}else{
				getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/Administracion/AdmonUsuarios.jsp").forward(request, response);
			}
		}catch(Exception e){
			System.out.println("------------------------");
			System.out.println("ValidaUsuarioServlet");
			e.printStackTrace();
			System.out.println("------------------------");
			getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/22MuestraErrores.jsp?titulo="+ titulo + "&mensajeError=" + e.getMessage()).forward(request, response);
		}finally{
			if(db!=null) db.cierraConexionDB();
		}

	}
} // fin del Servlet.
