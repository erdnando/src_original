package netropology.utilerias.negocio;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import netropology.utilerias.Comunes;

public class CotizadorCreditoSERVLET extends HttpServlet{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {
    CotizacionCreditosBean cotizador=new CotizacionCreditosBean();
    ResultadoCIVBean resCIV;
    ResultadoPPRBean resPPR;
    ResultadoPTRBean resPTR;

    String grupoUsuario="";
    String usuario="";
    String tipoCredito="";  
    String areaResponsable = "";
    String intermediario = "";
    String tipoIntermediario = "";
    int idTipoIntermediario;
    String acreditado = "";
    int diasCotizacion=0;
    int mesesCotizacion=0;
    int aniosCotizacion=0;
    Date fechaCotizacion;
    int diasDisposicion=0;
    int mesesDisposicion=0;
    int aniosDisposicion=0;
    Date fechaDisposicion;
    double montoOperacion=0d;
    int unidadTiempo=0;
    int plazoCredito=0;
    int periodosGracia=0;
    int numeroPagosIntereses=0;
    int periodosGraciaIntereses=0;
    int numeroPagosCapital=0;
    int periodosGraciaCapital=0;
    String curvaCotizacion = "";
    String curvaDuracion = "";

	 Date fechaMinCurva	= null;

    response.setContentType(CONTENT_TYPE);
    PrintWriter out = response.getWriter();
    String titulo="";    

    usuario=request.getParameter("usuario");
    grupoUsuario=request.getParameter("grupoUsuario");
    tipoCredito=request.getParameter("tipoCredito");
    areaResponsable = request.getParameter("areaResponsable");
    intermediario = request.getParameter("intermediario");
    tipoIntermediario = request.getParameter("tipoIntermediario");
    idTipoIntermediario = Integer.parseInt(request.getParameter("idTipoIntermediario"));
    acreditado = request.getParameter("acreditado");
    diasCotizacion = Integer.parseInt(request.getParameter("diasCotizacion"));
    mesesCotizacion = Integer.parseInt(request.getParameter("mesesCotizacion"));
    aniosCotizacion = Integer.parseInt(request.getParameter("aniosCotizacion"));
    diasDisposicion = Integer.parseInt(request.getParameter("diasDisposicion"));
    mesesDisposicion = Integer.parseInt(request.getParameter("mesesDisposicion"));
    aniosDisposicion = Integer.parseInt(request.getParameter("aniosDisposicion"));
    montoOperacion = Math.abs(Double.valueOf(request.getParameter("montoOperacion")).doubleValue());
    unidadTiempo=Integer.parseInt(request.getParameter("unidadTiempo"));
    plazoCredito=Math.abs(Integer.parseInt(request.getParameter("plazoCredito")));

	//Netropology JRFH 20/01/2005		
	 try{
		 fechaMinCurva		= Comunes.parseDate(request.getParameter("fechaMinCurva"));
		 fechaCotizacion	= Comunes.parseDate(diasCotizacion+"/"+mesesCotizacion+"/"+aniosCotizacion);

		System.out.println("OBTENIENDO SI HAY QUE CORRER EL STORED ");
		System.out.println("LA FECHA MIN CURVA = "+fechaMinCurva.toString());
		System.out.println("LA FECHA DE COTIZACION = "+fechaCotizacion.toString());
		System.out.println("LA COMPARACION = "+fechaMinCurva.compareTo(fechaCotizacion));		

		 if(fechaMinCurva.compareTo(fechaCotizacion)>0){
			cotizador.getFromHistorico = true;
		 }else{
			cotizador.getFromHistorico = false;
		 }

		 System.out.println("EL RESULTADO = "+cotizador.getFromHistorico);
	 }catch(Exception e){
		System.out.println("CotizadorCreditoSERVLET Exception: ERROR AL TRATAR DE OBTENER DATOS DEL HISTORICO");
	 }



    if (tipoCredito.equals("PPR")){
      numeroPagosIntereses=Math.abs(Integer.parseInt(request.getParameter("numeroPagosIntereses")));
      periodosGraciaIntereses=Math.abs(Integer.parseInt(request.getParameter("periodosGraciaIntereses")));
      numeroPagosCapital=Math.abs(Integer.parseInt(request.getParameter("numeroPagosCapital")));
      periodosGraciaCapital=Math.abs(Integer.parseInt(request.getParameter("periodosGraciaCapital")));
    }
    if (tipoCredito.equals("PTR")){
      periodosGracia=Math.abs(Integer.parseInt(request.getParameter("periodosGracia")));
    }
    curvaCotizacion = request.getParameter("curvaCotizacion");
    curvaDuracion = request.getParameter("curvaDuracion");
    //Inicializo el cotizador
    cotizador.grupoUsuario=grupoUsuario.trim();
    cotizador.usuario=usuario;
    cotizador.areaResponsable=areaResponsable.trim();
    cotizador.intermediario=intermediario.trim();
    cotizador.tipoIntermediario=tipoIntermediario.trim();
    cotizador.idTipoIntermediario=idTipoIntermediario;
    cotizador.acreditado=acreditado.trim();
    fechaCotizacion=new Date(aniosCotizacion-1900,mesesCotizacion-1, diasCotizacion);
    cotizador.fechaCotizacion=fechaCotizacion;
    fechaDisposicion=new Date(aniosDisposicion-1900,mesesDisposicion-1, diasDisposicion);
    cotizador.fechaDisposicion=fechaDisposicion;
    cotizador.montoOperacion=montoOperacion;
    cotizador.unidadTiempo=unidadTiempo;
    cotizador.plazoCredito=plazoCredito;
    cotizador.curvaCotizacion=curvaCotizacion.trim();
    cotizador.curvaDuracion=curvaDuracion.trim();
    if (tipoCredito.equals("PPR")){
      cotizador.numeroPagosIntereses=numeroPagosIntereses;
      cotizador.periodosGraciaIntereses=periodosGraciaIntereses;
      cotizador.numeroPagosCapital=numeroPagosCapital;
      cotizador.periodosGraciaCapital=periodosGraciaCapital;
    }
    if (tipoCredito.equals("PTR")){
      cotizador.periodosGracia=periodosGracia;
      cotizador.numeroPagosIntereses=plazoCredito;
      cotizador.periodosGraciaIntereses=periodosGracia;
      cotizador.numeroPagosCapital=plazoCredito;
      cotizador.periodosGraciaCapital=periodosGracia;
    }
      
    try{
      if (tipoCredito.equals("CIV") ){
        titulo="Cotizaci�n de Cr�ditos a Tasa Fija en Moneda Nacional<br>(Pago de Capital e Intereses al Vencimiento: Bono Cup�n Cero)";      
        resCIV = cotizador.getCapitalIntereses();
        resCIV.guardarCotizacion();
        request.setAttribute("res", resCIV);
        //getServletConfig().getServletContext().getRequestDispatcher("/nafin/22cotizador/credito/ResultadoCotizacion.jsp").forward(request, response);
		  getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/credito/ResultadoCotizacion.jsp").forward(request, response);
      }
      if (tipoCredito.equals("PPR") ){
        titulo="Cotizaci�n de Cr�ditos a Tasa Fija en Moneda Nacional <BR>(Pagos Peri�dicos Regulares)";
        resPPR = cotizador.getPagosPeriodicosRegulares();
        resPPR.guardarCotizacion();
        request.setAttribute("res", resPPR);
        //getServletConfig().getServletContext().getRequestDispatcher("/nafin/22cotizador/credito/ResultadoCotizacion.jsp").forward(request, response);
        getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/credito/ResultadoCotizacion.jsp").forward(request, response);
      }
      if (tipoCredito.equals("PTR") ){
        titulo="Cotizaci�n de Cr�ditos a Tasa Fija en Moneda Nacional <BR>(Pagos tipo Renta)";
        resPTR = cotizador.getPagosTipoRenta();
        resPTR.guardarCotizacion();
        request.setAttribute("res", resPTR);
        //getServletConfig().getServletContext().getRequestDispatcher("/nafin/22cotizador/credito/ResultadoCotizacion.jsp").forward(request, response);
        getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/credito/ResultadoCotizacion.jsp").forward(request, response);
      }  
    }catch(CotizadorException e){
      System.out.println("-----------------------");
      System.out.println("CotizadorCreditoSERVLET");
      e.printStackTrace();
      System.out.println("-----------------------");
      getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/22MuestraErrores.jsp").forward(request, response);
    }catch(Exception e){
      System.out.println("-----------------------");
      System.out.println("CotizadorCreditoSERVLET");
      e.printStackTrace();
      System.out.println("-----------------------");
      getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/22MuestraErrores.jsp").forward(request, response);
    }
    
    out.close();

  }
}