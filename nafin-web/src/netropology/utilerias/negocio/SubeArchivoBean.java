package netropology.utilerias.negocio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import netropology.utilerias.AccesoDB;

public class SubeArchivoBean{
	private String nombreCurva;
	private AccesoDB ad=new AccesoDB();
	private ResultSet rs=null;

	public boolean load() throws Exception{
		boolean okC = true;
		try {
		    ad.conexionDB(); 
		} catch(SQLException sqle) { okC = false; }
	return okC;
	}

	public void release() throws Exception{
		if(ad!=null) ad.cierraConexionDB();
	}
  
	public boolean leeDatosCurva(String pstrNombreArchivo,String strNombreUsuario) throws CatalogoVacioException, FileNotFoundException, CotizadorException, ArchivoCurvasIncorrectoException{
		String renglon="";
		String strQuery="";
		String strInsert="";
		String strFechaCurva="";
		String strNombreCurva="";
		String strPlazo="";
		String strTasa="";
		String idDatosCurva="";
		int idCurva=0; 
		boolean regreso=true;
		Hashtable curvaExistente=new Hashtable();
    
		//Verificamos que exista el archivo
		File archivoALeer = new File(pstrNombreArchivo);
		if (archivoALeer.exists()){
    
			try{
				//primeros 8 caracteres corresponden a la fecha
				strFechaCurva=archivoALeer.getName().substring(0,8);
				//siguientes caracteres hasta antes del punto corresponden al tipo de curva
				strNombreCurva=archivoALeer.getName().substring(8,archivoALeer.getName().indexOf(".", 9));

				//Obtengo el id de la curva
				strQuery = "SELECT idCurva FROM Curva WHERE nombre=\'" + strNombreCurva + "\'" ;
				rs=ad.queryDB(strQuery);
				if (rs.next()){
					idCurva=rs.getInt("idCurva");
					ad.cierraStatement();
				}
				else{
					ad.cierraStatement();
					throw new CatalogoVacioException("No existe una curva con el nombre: " + archivoALeer.getName() + ". Verifique que el nombre del archivo tenga el formato: \"yyyymmddNOMBRECURVA.extension\"<br>En caso de ser un documento, este ya se proceso, favor de verificarlo");
					
				}

				//Valido que los datos de esa curva para esa fecha no existan
				//Si existen, deben actualizarse con los nuevos datos
				strQuery= "SELECT iddatoscurva, plazo " +
							"FROM   datoscurva " +
							"WHERE  idcurva=" + idCurva + " " +
							"AND    fecha='" + strFechaCurva + "'";
				rs=ad.queryDB(strQuery);
				while(rs.next()){
					curvaExistente.put(rs.getString("plazo"), rs.getString("iddatoscurva"));
				}
				ad.cierraStatement();
        
				FileReader fDatos=new FileReader(pstrNombreArchivo);
				BufferedReader bDatos=new BufferedReader(fDatos);
				while((renglon=bDatos.readLine())!=null){
					StringTokenizer datos=new StringTokenizer(renglon, ",");
					while(datos.hasMoreElements()){
						strPlazo=datos.nextToken();
						strTasa=datos.nextToken();
						if (curvaExistente.size()==0){
							//Los datos de la curva son nuevos
							regreso=subeDatosCurva(false,"",""+idCurva, strFechaCurva, strPlazo, strTasa, strNombreUsuario );
						}else{
							//Los datos de la curva ya existen
							idDatosCurva=(String)curvaExistente.get(strPlazo);
							if (idDatosCurva==null || idDatosCurva.equals("") || idDatosCurva.length()==0)
								regreso=subeDatosCurva(false,"",""+idCurva, strFechaCurva, strPlazo, strTasa, strNombreUsuario);
							else
								regreso=subeDatosCurva(true,idDatosCurva,""+idCurva, strFechaCurva, strPlazo, strTasa, strNombreUsuario);
						}
					}
				}
				// Insert de la Bitacora
				
				strInsert =	" INSERT INTO BIT_ALTA_CURVA( " +
							   " IDCURVA,DC_ALTA,CG_NOMBRE_USUARIO)" + 
							   " VALUES( " + idCurva + ", Sysdate , '"+ strNombreUsuario+"')";
									
							    ad.ejecutaSQL(strInsert);
							    System.out.println("Query INSERTA HISTORICO:::"+strInsert);	
						
				
			}catch(IOException e){
				System.out.println("----------------------------");
				System.out.println("SubeArchivosBean.leeDatosCurvas()");
				e.printStackTrace();
				System.out.println("----------------------------");
				throw new CotizadorException("SubeArchivoBean.leeDatosCurva(): IOException::" + e.getMessage());
			}catch(SQLException e){
				System.out.println("----------------------------");
				System.out.println("SubeArchivosBean.leeDatosCurvas()");
				e.printStackTrace();
				System.out.println("----------------------------");
				throw new CotizadorException("SubeArchivoBean.leeDatosCurva(): SQLException::" + e.getMessage());
			}catch(NoSuchElementException e){
				throw new ArchivoCurvasIncorrectoException("El archivo que se especific� no contiene los datos de las curvas o se encuentra en un formato diferente. El formato para los datos de las curvas es: \"plazo, tasa\" ");
			}catch(StringIndexOutOfBoundsException e){
				throw new CatalogoVacioException("No existe una curva con el nombre: " + archivoALeer.getName() + ". Verifique que el nombre del archivo tenga el formato: \"yyyymmddNOMBRECURVA.extension\"<br>En caso de ser un documento, este ya se proceso, favor de verificarlo");
			}  
		}else{
			throw new FileNotFoundException("SubeArchivoBean.leeDatosCurva(): El archivo" + pstrNombreArchivo + " no ha sido encontrado. Posiblemente no se subi� correctamente");
		}

		ad.terminaTransaccion(regreso); 
	return regreso;
	}
  
	private boolean subeDatosCurva(boolean pblnExiste, String pstrIdDatosCurva, String pstrIdCurva, String pstrFechaCurva, String pstrPlazo, String pstrTasa,String strNombreUsuario) throws CotizadorException{
	String strInsert;
	String strInsert2;
	boolean salida=true;

	if (pblnExiste){
		//Actualizo el dato que existe, con el nuevo dato
		strInsert="UPDATE DatosCurva " +
					"SET    plazo=" + pstrPlazo + ", " +
					"       tasa=" + pstrTasa + " " +
					"WHERE  idDatosCurva = " + pstrIdDatosCurva;
		try {
			ad.ejecutaSQL(strInsert);							
						
		} catch(SQLException sqle) { salida=false; }
	}else{
		//Inserto los datos a la tabla DatosCurva
		strInsert = "INSERT INTO DatosCurva VALUES(seq_DatosCurva.nextval, " + pstrIdCurva + ", "; 
		strInsert += pstrPlazo + ", " + pstrTasa + ", '" + pstrFechaCurva + "')";
		try {
			ad.ejecutaSQL(strInsert);
		}catch(SQLException sqle) { salida=false; }
	}//el dato de la curva no exist�a
	
	
	
	return salida;
	}

	public boolean leeFactoresRiesgo(String pstrNombreArchivo,String moneda) throws Exception{
	String renglon="";
	String strDelete="";
	String strQuery="";
	String dPlazo="";
	int iTipoIntermediario=0; //Toma valores 1, 2, 3
	boolean regreso=true;
    
		//Verificamos que exista el archivo
		File archivoALeer = new File(pstrNombreArchivo);
		if (archivoALeer.exists()){
			//Borro los valores actuales de la tabla factoresriesgo
			strDelete = "DELETE FROM factoresriesgo where ic_moneda = " + moneda;
			try {
				ad.ejecutaSQL(strDelete);
				System.out.println("Datos actuales de factores de riesgo borrados");
			}catch(SQLException sqle) {
				System.out.println("No fue posible borrar los datos actuales de factores de riesgo");
				throw new CotizadorException("No fue posible borrar los datos actuales de factores de riesgo");
			}
	      
			FileReader fDatos=new FileReader(pstrNombreArchivo);
			BufferedReader bDatos=new BufferedReader(fDatos);
      iTipoIntermediario=1;
      ArrayList aListRegFacRiesgo= new ArrayList();
			while((renglon=bDatos.readLine())!=null){
        //Carga los id's de los riesgos
        if(iTipoIntermediario==1){
          StringTokenizer sTokRiesgos=new StringTokenizer(renglon, ",");
          while(sTokRiesgos.hasMoreElements()){
            aListRegFacRiesgo.add(sTokRiesgos.nextElement());
          }
        }else{
				
        StringTokenizer datos=new StringTokenizer(renglon, ",");
				if (datos.hasMoreElements()){
					dPlazo=datos.nextToken();
				}
				while(datos.hasMoreElements()){
					if (subeFactoresRiesgo(dPlazo, datos.nextToken(), ""+aListRegFacRiesgo.get(iTipoIntermediario-2), moneda))
						regreso=true;
					else regreso=false;
          iTipoIntermediario++;
				}
			}
      iTipoIntermediario=2;
    }
		}else{ 
			System.out.println("Error. SubeArchivoBean.leeFactoresRiesgo::El archivo " + pstrNombreArchivo + " no ha sido encontrado");
			throw new CotizadorException("El archivo " + pstrNombreArchivo + " no ha sido encontrado");
		}
		ad.terminaTransaccion(regreso); 
	return regreso;
	}

	private boolean subeFactoresRiesgo(String pstrPlazo, String pstrRiesgo, String pstrTipoIntermediario, String moneda) throws Exception{
		String strInsert="";
		boolean salida=true;

		//Inserto los datos a la tabla FactoresRiesgo
		strInsert = "INSERT INTO factoresriesgo VALUES(seq_FactoresRiesgo.nextval, " + pstrTipoIntermediario + ", "; 
		strInsert += pstrPlazo + ", " + pstrRiesgo + ", " + moneda + ")";
		System.out.println(strInsert);
		try {
			ad.ejecutaSQL(strInsert);
			System.out.println("Se insert� el riesgo:" + pstrPlazo + "-" + pstrRiesgo + " para el intermediario " + pstrTipoIntermediario);
			salida=true;
		} catch(SQLException sqle) {
			System.out.println("SubeArchivoBean: Error en la inserci�n de datos:: factores riesgo");
			salida=false;
		}		
	return salida;
	}
	
}