package netropology.utilerias.negocio;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ConfirmaCreditoServlet extends HttpServlet{
	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

	public void init(ServletConfig config) throws ServletException{
		super.init(config);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		ResultadoCIVBean civ=null;
		ResultadoPPRBean ppr=null;
		ResultadoPTRBean ptr=null;
    
		response.setContentType(CONTENT_TYPE);
		//PrintWriter out = response.getWriter();
		try{
			String tipoCredito=(request.getParameter("tipoCredito")==null)?"":request.getParameter("tipoCredito");
			
			if (tipoCredito.equals("CIV")){
				civ = (ResultadoCIVBean) request.getAttribute("res");
				if (civ.confirmarCotizacion())
					getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/credito/Confirmacion/creditoConfirmado.jsp").forward(request, response);
				else
					throw new Exception("La operacion no pudo ser confirmada");
			}
			
			if (tipoCredito.equals("PPR")){
				ppr = (ResultadoPPRBean) request.getAttribute("res");
				if (ppr.confirmarCotizacion())
					getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/credito/Confirmacion/creditoConfirmado.jsp").forward(request, response);
				else
					throw new Exception("La operacion no pudo ser confirmada");
			}
			
			if (tipoCredito.equals("PTR")){
				ptr = (ResultadoPTRBean) request.getAttribute("res");
				if (ptr.confirmarCotizacion())
					getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/credito/Confirmacion/creditoConfirmado.jsp").forward(request, response);
				else
					throw new Exception("La operacion no pudo ser confirmada");
			}
		}catch(Exception e){
			System.out.println("------------------------");
			System.out.println("ConfirmaCreditoServlet");
			e.printStackTrace();
			System.out.println("------------------------");
			getServletConfig().getServletContext().getRequestDispatcher("/22cotizador/22MuestraErrores.jsp").forward(request, response);
		}
		//out.close();
	}
	
} // Fin del Servlet.
