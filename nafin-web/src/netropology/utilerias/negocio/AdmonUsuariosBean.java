package netropology.utilerias.negocio;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.Hashtable;
import java.util.Vector;

import netropology.utilerias.AccesoDB;

public class AdmonUsuariosBean{
	private AccesoDB db = new AccesoDB();
	private ResultSet cRS;
	private String strUsuario="";
	private String strIntermediario="";
	private String strTipoIntermediario="";
	public String strMensajeError="";

	public boolean load() throws Exception{
		boolean ok=true;
		try {
			this.db.conexionDB();
		} catch(Exception e) { ok=false; }
		
		return ok;
	}

	public void release() throws Exception{
		this.db.cierraConexionDB();
	}

	public Vector buscar(String pstrUsuario) throws CotizadorException{
		Vector usuarios=new Vector();
		
		try {
			String strBuscar="SELECT a.usuario, a.intermediario, b.descripcion tipoIntermediario, "+
							"a.idtipointermediario idTipoIntermediario " +
							"FROM usuario a, tipointermediario b " +
							"WHERE a.idtipointermediario = b.idtipointermediario " +
							"AND a.usuario LIKE '" + pstrUsuario + "%'";
			this.cRS=this.db.queryDB(strBuscar);
			while (this.cRS.next()){
				Hashtable datos=new Hashtable(3);
				datos.put("usuario", this.cRS.getString("usuario"));
				datos.put("intermediario", this.cRS.getString("intermediario"));
				datos.put("tipoIntermediario", this.cRS.getString("tipoIntermediario"));
				datos.put("idTipoIntermediario", this.cRS.getString("idTipoIntermediario"));
				usuarios.add(datos);
			}
			this.db.cierraStatement();
    	} catch(SQLException e){
			System.out.println("-----------------------------");
			System.out.println("Error:AdmonUsuariosBean");
			e.printStackTrace();
			System.out.println("-----------------------------");
			throw new CotizadorException("Error: " + e.getMessage());
		}
	return usuarios;
	}
  
	public boolean borrar() throws Exception{
		boolean ok=true;	
		String strEliminar="DELETE FROM usuario WHERE usuario like '" + this.strUsuario + "'";
		try { 
			this.db.ejecutaSQL(strEliminar);
		} catch(Exception e) { ok=false; }
		finally { this.db.terminaTransaccion(ok); }
		
	return ok;
	}
  
	public boolean guardar(boolean existeUsuario) throws Exception{
	boolean ok=true;
	String strGuardar="";
	try {
		if (existeUsuario){
			strGuardar = "UPDATE usuario " +    
						" SET idtipointermediario=" + strTipoIntermediario + ", " +
						"     usuario='" + strUsuario + "', " +
						"     intermediario='" + strIntermediario + "' " +
						" WHERE usuario LIKE '" + strUsuario + "'" ;
			this.db.ejecutaSQL(strGuardar);
			
	    } else {
			strGuardar= "INSERT INTO usuario VALUES(" +
						"seq_Usuario.nextVal, " + strTipoIntermediario + ", " +
						"'" + strUsuario + "', '" + strIntermediario + "')";
			this.db.ejecutaSQL(strGuardar);
	    }
	} catch(Exception e) { ok=false; }
	finally { this.db.terminaTransaccion(ok); }
            
    return ok;
	}

	public void setUsuario(String pstrUsuario){
		strUsuario=pstrUsuario;
	}

	public void setIntermediario(String pstrIntermediario){
		strIntermediario=pstrIntermediario;
	}

	public void setTipoIntermediario(String pstrTipoIntermediario){
		strTipoIntermediario=pstrTipoIntermediario;
	}
}