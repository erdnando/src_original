package netropology.utilerias;

import java.util.List;

public interface IQueryGeneratorReg {
	public abstract String 		getDocumentQuery();
	public abstract String 		getDocumentSummaryQueryForIds(List ids);
	public abstract String 		getAggregateCalculationQuery();
	public abstract List 		getConditions();
	public abstract String 		getDocumentQueryFile();
	public abstract void 		setPaginaNo(String newPaginaNo);
	public abstract void 		setPaginaOffset(String newPaginaOffset);
	public abstract String 		getPaginaNo();
	public abstract	String 		getPaginaOffset();	
}
