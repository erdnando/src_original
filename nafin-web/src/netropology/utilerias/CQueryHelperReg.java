package netropology.utilerias;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * La clase tiene por finalidad realizar la paginaci�n de informaci�n
 * utilizando variables bind y el objeto Registros para poder accesar
 * a la informaci�n.
 * Tiene mejoras en cuanto a la implementaci�n de las paginaciones
 * ya que contempla el manejo de llaves primarias compuestas
 * @author ihj
 */
 
public class CQueryHelperReg {
	
	private 	IQueryGeneratorReg 	qryGen;
	private		String				qrySentencia;
	private		List 				lVarBind;
	private		Registros 			registros;
	private		Registros 			newids;
	private		Registros 			totales;
	private 	int 				pageSize;
	public 		String 				sIds 			= "sesids";
	public 		String 				sTotales 		= "totales";
	/**
	 * Establece el maximo numero de registros que regresar� la consulta
	 * De manera predeterminada se establece en 1000
	 */
	private int maximoNumeroRegistros = 1000;

	/**
	 * Id para identificar de manera unica la ejecuci�n de un query y poder
	 * rastrear donde inicia y donde termina.
	 * Util para cuando hay varios queries de paginador ejecutandose
	 * simultaneamente.
	 */
	private final String idQuery = String.valueOf((new java.util.Date()).getTime());

	
	public CQueryHelperReg(IQueryGeneratorReg qryGen){
		this.qryGen = (IQueryGeneratorReg)qryGen;
	}//CQueryHelperReg

	public CQueryHelperReg(IQueryGeneratorReg qryGen, String sIds, String sTotales){
		this.qryGen 	= (IQueryGeneratorReg)qryGen;
		this.sIds		= sIds;
		this.sTotales	= sTotales;
	}//CQueryHelperReg
	
	//Regresa una pagina de la consulta
	public Registros getPageResultSet(
						AccesoDB 			con,
						int 				offset,
						int 				pageSize) throws Exception {
		registros = con.consultarDB(
							qryGen.getDocumentSummaryQueryForIds(getNextSetOfIds(offset, pageSize).getData()), 
							qryGen.getConditions(), 
							true);
		return registros;
	}//getPageResultSet
	

	/**
	 * Regresa los totales de la consulta.
	 * @param request Objeto Request
	 */
	public Registros getResultCount(HttpServletRequest request){
		return (Registros)request.getSession().getAttribute(sTotales);
	}

	//Establece las llaves de la consulta
	public void executePKQueryIfNecessary (
					HttpServletRequest 	request,
					AccesoDB 			con) throws Exception {
		HttpSession 	session 	= request.getSession();
						newids		= new Registros();
						totales		= new Registros();
		cleanSession(request);
		qryGen.setPaginaNo("0");
		qryGen.setPaginaOffset("0");
		
		System.out.println("CQueryHelperReg.executePKQueryIfNecessary():: pag= " + qryGen.getClass().getName() + " (" + this.idQuery + ")" );
		long tiempoInicial = (new java.util.Date()).getTime();

		newids 	= con.consultarDB(
						qryGen.getDocumentQuery(), 	//QUERY DE LLAVES
						qryGen.getConditions(),		//VARIABLES BIND
						true,						//NO HACE DISTINCION DE TIPOS DE DATOS EN EL RESULTADO
						this.getMaximoNumeroRegistros());						//NUMERO MAXIMO DE REGISTROS A PRESENTAR EN LA CONSULTA
		long tiempoFinal = (new java.util.Date()).getTime();
		System.out.println("CQueryHelperReg.executePKQueryIfNecessary():: PKs. t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );

		if(newids.getNumeroRegistros()>0) {
			session.setAttribute(sIds, newids);
			qrySentencia = (qryGen.getAggregateCalculationQuery()).trim();//TOTALES
			if (qrySentencia != null && !"".equals(qrySentencia)) {
				tiempoInicial = (new java.util.Date()).getTime();
				totales = con.consultarDB(
								qryGen.getAggregateCalculationQuery(), 	
								qryGen.getConditions());
				tiempoFinal = (new java.util.Date()).getTime();
				System.out.println("CQueryHelperReg.executePKQueryIfNecessary():: Totales t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );

				session.setAttribute(sTotales, totales);
			}//if (qrySentencia != null && !"".equals(qrySentencia) )
		}//if(registros.getNumeroRegistros()>0)
	}//executePKQueryIfNecessary
	
	//Regresa el numero de ids
	public int getIdsSize() {
		if(newids!=null)
			return newids.getNumeroRegistros();
		else 
			return 0;
	}//getIdsSize
	
	public boolean readyForPaging(HttpServletRequest request) {
		newids = (Registros) request.getSession().getAttribute(sIds);
		if (newids!=null) {
			return true;
		} else {
			return false;
		}
	}

	//Regresa el siguiente paquete de ids
	private Registros getNextSetOfIds(
					int 				offset,
					int 				pageSize) {
		int indiceMaximo = getIdsSize();
		if(newids.getNumeroRegistros()>offset+pageSize) {
			indiceMaximo = offset + pageSize;
		}
		registros = new Registros();
		for(int i=offset;i<indiceMaximo;i++) {
			registros.addDataRow(newids.getData(i));
		}
		return registros;
	}
	
	//Arma archivo con todos los registros de la consulta
	
	public String getCreateFile(HttpServletRequest request, String path, AccesoDB con, String separador, String extension, boolean encabezado) {
		CreaArchivo 		archivo 			= new CreaArchivo();
		StringBuffer		contenidoArchivo	= new StringBuffer();
		StringBuffer		contenidoEncabezado	= new StringBuffer();
		PreparedStatement 	ps					= null;
		ResultSet			rs					= null;
		ResultSetMetaData	rsMD				= null;
		OutputStreamWriter 			writer 				= null;
		BufferedWriter 		buffer 				= null;
		try {
			System.out.println("CQueryHelperReg.getCreateFile():: pag= " + qryGen.getClass().getName() + " (" + this.idQuery + ")" );
			ps = con.queryPrecompilado(qryGen.getDocumentQueryFile(), qryGen.getConditions());
			long tiempoInicial = (new java.util.Date()).getTime();
			rs = ps.executeQuery();
			long tiempoFinal = (new java.util.Date()).getTime();
			System.out.println("CQueryHelperReg.getCreateFile():: t=" + String.valueOf(tiempoFinal-tiempoInicial) + " ms. idQuery=(" + this.idQuery + ")" );
			
			ps.clearParameters();
			rsMD = rs.getMetaData();
			int 		numCols 	= rsMD.getColumnCount();
			writer = new OutputStreamWriter(new FileOutputStream(path+archivo.nombreArchivo()+"."+extension,true), "ISO-8859-1");
			buffer = new BufferedWriter(writer);
			while(rs.next()) {
				for(int i=1;i<=numCols;i++) {
					if(encabezado) {
						for(int e=1;e<=numCols;e++) {
							String nombre = rsMD.getColumnName(e).replace('_', ' ').toUpperCase();
							buffer.write(nombre+((e==numCols)?"\r\n":separador));
						}
						encabezado = false;
					}
					String rs_campo = rs.getString(i)==null?"":rs.getString(i).replace(',', ' ').trim();
					buffer.write(rs_campo+((i==numCols)?"\r\n":separador));
				}//for(int i=1;i<=numCols;i++)
			}//while(rs.next())
			buffer.close();
			rs.close();
			if(ps!=null) ps.close();
		} catch(Exception e) {
			System.out.println("CQueryHelperReg::getCreateFile(Exception) "+e);
			e.printStackTrace();
		}
		return archivo.nombre+"."+extension;
	}
	
	public String getCreateFile(HttpServletRequest request, String path, AccesoDB con, String separador, String extension) {
		return getCreateFile(request, path, con, ",", "csv",true);
	}
	
	public String getCreateFile(HttpServletRequest request, String path, AccesoDB con) {
		return getCreateFile(request, path, con, ",", "csv");
	}
	
	public void cleanSession(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute(sIds);
		session.removeAttribute(sTotales);
	}
	
	public Registros setPaginacion(HttpServletRequest request, int pageSize){
		Registros registros = new Registros();
		this.pageSize = pageSize;
		AccesoDB con = new AccesoDB();
    boolean resultado = true;
		try {
			con.conexionDB();
			if (!readyForPaging(request)) {
				executePKQueryIfNecessary(request, con);
			}
			if (readyForPaging(request)) {
				registros = getPageResultSet(con, Integer.parseInt(qryGen.getPaginaOffset()), this.pageSize);
			}
		} catch(Exception e) {
			e.printStackTrace();
      resultado = false;
		} finally {
			if(con.hayConexionAbierta()) 
      con.terminaTransaccion(resultado);
				con.cierraConexionDB();
		}
		return registros;
	}
	
	public List getSelectOptions() {
		List list = new ArrayList();
		if(getIdsSize()>0) {
			int pageOffset = Integer.parseInt(qryGen.getPaginaOffset());
			int numPaginas 	= getIdsSize()/this.pageSize;
			if(getIdsSize()%this.pageSize>0) {
				numPaginas++;
			}
			int seleccionado = (pageOffset/this.pageSize)+1;
			qryGen.setPaginaNo(String.valueOf(seleccionado));
			for(int i=0;i<numPaginas;i++) {
				int offset = (i*this.pageSize);
				int pagina = i+1;
				List options = new ArrayList();
				options.add(String.valueOf(offset));
				options.add("Pagina "+pagina+" de "+numPaginas);
				list.add(options);
			}//for(int i=1;i<=numPaginas;i++)
		}
		return list;
	}
	
	public String getComboPaginacion(int pageSize, int pageOffset) {
		StringBuffer sb = new StringBuffer();
		int numPaginas 	= getIdsSize()/pageSize;
		if(getIdsSize()%pageSize>0) {
			numPaginas++;
		}
		int seleccionado = (pageOffset/pageSize)+1;
		for(int i=0;i<numPaginas;i++) {
			int offset = (i*pageSize);
			int pagina = i+1;
			String aux = (pagina==seleccionado)?"selected":"";
			sb.append("<option value='"+offset+"' "+aux+">Pagina "+pagina+" de "+numPaginas+"</option>\n");
		}//for(int i=1;i<=numPaginas;i++)
		return sb.toString();
	}

	/**
	 * Obtiene el n�mero m�ximo de registros a presentar en una consulta 
	 * De manera predeterminada el valor es 1000 a menos que haya sido 
	 * ajustado mediante setMaximoNumeroRegistros(int)
	 * @return numero m�ximo de registros a regresar.
	 */
	public int getMaximoNumeroRegistros() {
		return this.maximoNumeroRegistros;
	}

	/**
	 * Establece el n�mero m�ximo de registros a presentar en una consulta 
	 * De manera predeterminada el valor es 1000 
	 * @param maximoNumeroRegistros M�ximo de registros a regresar.
	 */
	public void setMaximoNumeroRegistros(int maximoNumeroRegistros) {
		this.maximoNumeroRegistros = maximoNumeroRegistros;
	}
	
}//CQueryHelperReg
