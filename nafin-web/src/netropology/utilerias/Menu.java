package netropology.utilerias;

import java.io.BufferedWriter;
import java.io.FileWriter;

import java.math.BigDecimal;

import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;

/**
 * Esta clase esta enfocada a producir los elementos del menu, segun lo requiera el
 * componente a utilizar.
 */
public class Menu  {
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(Menu.class);
	
	/**
	 * Genera menu por perfil. Para evitar ser llamado tantas veces como ISLANDS
	 * se levanten en el Oracle iAS, se adiciona l�gica para que solamente se
	 * ejecute una sola vez. 
	 * @param forzarRegenerar Forzar regeneracion de los menus. true si, false no
	 */
	public static synchronized void generarMenus(boolean forzarRegenerar) {
		
		AccesoDB con = new AccesoDB();
		
		try {
			boolean generarMenus = false;
			if (forzarRegenerar) {
				generarMenus = true;
			} else {
				String sql = 
						" SELECT NVL( (SYSDATE - MAX(df_generacion_menu))*24*60, -1) AS minutosTranscurridos" +
						" FROM seg_perfil ";
				con.conexionDB();
				Registros reg = con.consultarDB(sql, null, true);
				float minutosTranscurridos = -1;
				if (reg.next()) {
					minutosTranscurridos = ((BigDecimal)reg.getObject("minutosTranscurridos")).floatValue();
					if( minutosTranscurridos == -1 || minutosTranscurridos >= 15) {
						//-1 .- No se ha generado nunca los menus, por lo cual es necesario generarlos.
						generarMenus = true;
					} else {
						generarMenus = false;
					}
				}
			}
			
			if (generarMenus) {
				Menu.generarMenusXPerfil();
			}
		} catch( Throwable e) {
			throw new AppException("Error al generar los menus", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Regenera menus por perfil. 
	 *
	 */
	public static synchronized void regenerarMenus() {
		try {
			Menu.generarMenus(true);
		} catch( Throwable e) {
			throw new AppException("Error al regenerar los menus", e);
		}
	}


	/**
	 * Genera menus por perfil
	 */
	private static synchronized void generarMenusXPerfil() {
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		try {
			con.conexionDB();
			String strSQL = 
						" UPDATE seg_perfil " +
						" SET cg_menu = empty_clob(), " +
						" df_generacion_menu = SYSDATE ";
			con.ejecutaUpdateDB(strSQL);

			strSQL = 
					" SELECT cc_perfil, sc_tipo_usuario " +
					" FROM seg_perfil ";
			Registros reg = con.consultarDB(strSQL);

			strSQL = 
					" SELECT cg_menu " +
					" FROM seg_perfil " +
					" WHERE cc_perfil = ?  " +
					" FOR UPDATE ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);

			while(reg.next()) {
				ps.clearParameters();
				String perfil = reg.getString("cc_perfil");
				int claveTipoUsuario = Integer.parseInt(reg.getString("sc_tipo_usuario"));
				ps.setString(1, perfil);
				ResultSet rs = ps.executeQuery();
				rs.next();
				Clob clob = rs.getClob("cg_menu");
				clob.setString(1, Menu.generarMenuXPerfil(perfil, claveTipoUsuario));
				rs.close();
			}
			ps.close();
			bOk = true;
		} catch( Exception e) {
			bOk = false;
			throw new AppException("Error al generar los menus x perfil: ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
	}



	/**
	 * Genera el archivo XML que contiene la informaci�n necesaria para 
	 * generar el menu.
	 * @param perfil Perfil del usuario
	 */
	public static synchronized String generarMenuXPerfil(String perfil, int claveTipoUsuario) {
		AccesoDB con = new AccesoDB();
		StringBuffer sbMenu = new StringBuffer(8192);
		
		try {
			con.conexionDB();
			List params = null;
			//El funcionamiento de la logica para generar el menu, se basa
			//En el correcto ordenamiento, por lo que no debe ser
			//afectado el ORDER BY del siguiente query.
			String strSQL = 
				"SELECT  m.cc_menu, m.cg_nombre, m.ig_nivel, mxp.ig_posicion, " +
				" 		m.cc_menu_padre, m.cg_tipo, m.cg_url " +
				" FROM comcat_menu_afiliado m, comrel_menu_afiliado_x_perfil mxp " +
				" WHERE m.cc_menu = mxp.cc_menu " +
				"     AND m.ic_tipo_afiliado = mxp.ic_tipo_afiliado " +
				"     AND mxp.cc_perfil = ? " +
				"     AND mxp.ic_tipo_afiliado = ? " +
				//"     AND m.cs_activo = 'S' " +
				//"     AND mxp.cs_activo = 'S' " +
				"ORDER BY m.ig_nivel, m.cc_menu_padre, mxp.ig_posicion ";	//NO cambiar ORDER BY
			params = new ArrayList();
			params.add(perfil);
			params.add(new Integer(claveTipoUsuario));
			Registros reg = con.consultarDB(strSQL, params);
			int nivel = 0;
				
			sbMenu.append(
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
					"	<menu>");
			boolean tieneHijosXProcesar = false;
			boolean menuNoProcesado = true;
			List indicesSistemaPadre = new ArrayList();
			if(!reg.next()) {
				return "";
			}
			principal:
			while(reg.getNumeroRegistros() > 0) {
				String menuEnTurno = reg.getString("cc_menu");
				String menuPadre = reg.getString("cc_menu_padre");
				nivel = Integer.parseInt(reg.getString("ig_nivel"));
				String tipoItem = reg.getString("cg_tipo");
				
				if (menuNoProcesado) {	//Si este menu, no ha sido previamente procesado
					sbMenu.append(
							"<item id=\"" + reg.getString("cc_menu") + "\" text=\"" + reg.getString("cg_nombre") + "\">");
					if (reg.getString("cg_url") != null && !reg.getString("cg_url").trim().equals("")) {
						sbMenu.append(
								"<userdata name=\"url\"><![CDATA[" + reg.getString("cg_url") + "]]></userdata>");
								/*"<href><![CDATA[" + reg.getString("cg_url") + "]]></href>");*/
					}
				}
				
				//Busca Hijos
				tieneHijosXProcesar = false;
				if (tipoItem.equals("M")) {	//Menu
					if(menuNoProcesado) {
						indicesSistemaPadre.add(new Integer(reg.getNumRegistro()));
					}
					while(reg.next()) {
						int nivelHijo = Integer.parseInt(reg.getString("ig_nivel"));

						//Para evitar que recorra todos los registros en busca de hijos
						//solo se busca en el nivel siguiente (nivel + 1) al del menu en turno
						//que es en donde unicamente pueden estar los hijos.
						if (nivelHijo < nivel + 1) {
							//Estamos en un nivel anterior a donde necesitamos buscar a los hijos
							continue;  //Se va al siguiente
						} else if (nivelHijo > nivel + 1) {
							//Estamos en un nivel mayor. En este ya no es posible encontrar a mas hijos
							break; // No hay mas hijos
						}
						if (reg.getString("cc_menu_padre").equals(menuEnTurno)) {
							tieneHijosXProcesar = true;
							menuNoProcesado = true; //El menu hijo aun no ha sido procesado
							continue principal;
						}
					}
					//Ya no hay mas hijos por procesar
					if (!tieneHijosXProcesar) {
							//sbMenu.deleteCharAt(sbMenu.length()-1); //borra ultima coma...
							sbMenu.append(
								"</item>"
							);
							reg.remove(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue()); ////Elimino el padre ya procesado completamente
							indicesSistemaPadre.remove(indicesSistemaPadre.size()-1);	//Elimino el indice del elemento padre ya procesado completamente
							if (indicesSistemaPadre.size() == 0) {
								reg.setNumRegistro(0);	//Se regresa al nuevo registro 0 si es que hay, si no hay la evaluaci�n del while detendra la ejecuci�n
								menuNoProcesado = true; //El menu padre (si existe) aun no ha sido procesado
								continue principal;
							}
							reg.setNumRegistro(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue());  //Se regresa al padre inmediato
							menuNoProcesado = false;	//Porque el sistema padre ya fue procesado anteriormente

					}
				} else if (tipoItem.equals("P")) {	//Pantalla
					reg.remove(); //Elimina el registro en se�al que ya fue procesado
					reg.setNumRegistro(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue()); //regresa el "apuntador" al sistema padre
					menuNoProcesado = false;  //Para indicar que el sistema padre ya fue procesado anteriormente
					sbMenu.append(
							"</item>"
					);
				}
				
			} //fin del ciclo
			//sbMenu.deleteCharAt(sbMenu.length()-1); //borra ultima coma...
			sbMenu.append(
				"	</menu>" + "\n"
			);

			return sbMenu.toString();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo de menu para el perfil:" + perfil, e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Genera el archivo JavaScript que contiene la informaci�n necesaria para 
	 * generar el menu.
	 * @param perfil Perfil del usuario
	 * @param rutaFisica Ruta fisica donde se genera el JS.
	 */
	private synchronized void generarMenuXPerfilExtJS(String perfil, String rutaFisica) {
		AccesoDB con = new AccesoDB();
		StringBuffer sbMenu = new StringBuffer(1024);
		
		try {
			con.conexionDB();
			List params = null;
			//El funcionamiento de la logica para generar el menu, se basa
			//En el correcto ordenamiento, por lo que no debe ser
			//afectado el ORDER BY del siguiente query.
			String strSQL = 
				"SELECT  m.cc_menu, m.cg_nombre, m.ig_nivel, mxp.ig_posicion, " +
				" 		m.cc_menu_padre, m.cg_tipo, m.cg_url " +
				" FROM comcat_menu m, comrel_menu_x_perfil mxp " +
				" WHERE m.cc_menu = mxp.cc_menu " +
				"     AND cc_perfil = ? " +
				"     AND m.cs_activo = 'S' " +
				"     AND mxp.cs_activo = 'S' " +
				"ORDER BY m.ig_nivel, m.cc_menu_padre, mxp.ig_posicion ";	//NO cambiar ORDER BY
			params = new ArrayList();
			params.add(perfil);
			Registros reg = con.consultarDB(strSQL, params);
			int nivel = 0;
				
			sbMenu.append(
					"Ext.onReady(function() { " + "\n" +
					"	var _menu = new Ext.Toolbar({" + "\n" +
					"	renderTo: '_menuApp'," + "\n" +
					"	items: [");
			boolean tieneHijosXProcesar = false;
			boolean menuNoProcesado = true;
			List indicesSistemaPadre = new ArrayList();
			if(!reg.next()) {
				return;
			}
			principal:
			while(reg.getNumeroRegistros() > 0) {
				String menuEnTurno = reg.getString("cc_menu");
				String menuPadre = reg.getString("cc_menu_padre");
				nivel = Integer.parseInt(reg.getString("ig_nivel"));
				String tipoItem = reg.getString("cg_tipo");
				
				if (menuNoProcesado) {	//Si este menu, no ha sido previamente procesado
					sbMenu.append(
							"{" +
							" text: '" + reg.getString("cg_nombre") + "'");
					if (reg.getString("cg_url") != null && !reg.getString("cg_url").trim().equals("")) {
						sbMenu.append(
								" ,handler: function(b, ev) { window.location='" + reg.getString("cg_url") + "'; }");
					}
				}
				
				//Busca Hijos
				tieneHijosXProcesar = false;
				if (tipoItem.equals("M")) {	//Menu
					if(menuNoProcesado) {
						sbMenu.append(
								" ,menu: { " +
								" items: [ ");
						indicesSistemaPadre.add(new Integer(reg.getNumRegistro()));
					}
					while(reg.next()) {
						int nivelHijo = Integer.parseInt(reg.getString("ig_nivel"));

						//Para evitar que recorra todos los registros en busca de hijos
						//solo se busca en el nivel siguiente (nivel + 1) al del menu en turno
						//que es en donde unicamente pueden estar los hijos.
						if (nivelHijo < nivel + 1) {
							//Estamos en un nivel anterior a donde necesitamos buscar a los hijos
							continue;  //Se va al siguiente
						} else if (nivelHijo > nivel + 1) {
							//Estamos en un nivel mayor. En este ya no es posible encontrar a mas hijos
							break; // No hay mas hijos
						}
						////////////////////// aqui se podria poner que si cambio de nivel ya no busque...
						if (reg.getString("cc_menu_padre").equals(menuEnTurno)) {
							tieneHijosXProcesar = true;
							menuNoProcesado = true; //El menu hijo aun no ha sido procesado
							continue principal;
						}
					}
					//Ya no hay mas hijos por procesar
					if (!tieneHijosXProcesar) {
							sbMenu.deleteCharAt(sbMenu.length()-1); //borra ultima coma...
							sbMenu.append(
								"]" +
								"}},"
							);
							reg.remove(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue()); ////Elimino el padre ya procesado completamente
							indicesSistemaPadre.remove(indicesSistemaPadre.size()-1);	//Elimino el indice del elemento padre ya procesado completamente
							if (indicesSistemaPadre.size() == 0) {
								reg.setNumRegistro(0);	//Se regresa al nuevo registro 0 si es que hay, si no hay la evaluaci�n del while detendra la ejecuci�n
								menuNoProcesado = true; //El menu padre (si existe) aun no ha sido procesado
								continue principal;
							}
							reg.setNumRegistro(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue());  //Se regresa al padre inmediato
							menuNoProcesado = false;	//Porque el sistema padre ya fue procesado anteriormente

					}
				} else if (tipoItem.equals("P")) {	//Pantalla
					reg.remove(); //Elimina el registro en se�al que ya fue procesado
					reg.setNumRegistro(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue()); //regresa el "apuntador" al sistema padre
					menuNoProcesado = false;  //Para indicar que el sistema padre ya fue procesado anteriormente
					sbMenu.append(
							"},"
					);
				}
				
			} //fin del ciclo
			sbMenu.deleteCharAt(sbMenu.length()-1); //borra ultima coma...
			sbMenu.append(
				"	]" + "\n" +
				"	}).show();"  + "\n" +
				"});");

			BufferedWriter out = new BufferedWriter(
					new FileWriter(rutaFisica + perfil.replace(' ','_') + ".js") );
			String outText = sbMenu.toString();
			out.write(outText);
			out.close();
		} catch(Exception e) {
			throw new AppException("Error al generar el archivo de menu", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Genera el Arbol en JSON para el perfil especificado
	 * @param tipoAfiliado Clave del tipo de afiliado. (comcat_tipo_afiliado.ic_tipo_afiliado)
 	 * @return Cadena json con la estructura del arbol
	 */
	public static String generarArbolMenuJSON(int tipoAfiliado) {
		AccesoDB con = new AccesoDB();
		StringBuffer sbMenu = new StringBuffer(8192);

		//**********************************Validaci�n de parametros:*****************************
/*		try {
			if (tipoAfiliado == 0) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" claveUsuario=" + claveUsuario);
			throw new AppException("Error en lo parametros recibidos");
		}
*/
		//***************************************************************************************

		
		try {
			con.conexionDB();
			List params = null;
			//El funcionamiento de la logica para generar el menu, se basa
			//En el correcto ordenamiento, por lo que no debe ser
			//afectado el ORDER BY del siguiente query.
			String strSQL = 
				" SELECT  m.cc_menu, m.cg_nombre, m.cg_descripcion, m.ig_nivel, " +
				" 		m.cc_menu_padre, m.cg_tipo, m.cg_url, TO_CHAR(m.df_alta,'dd/mm/yyyy hh24:mi') as df_alta, " +
				"     m.cs_activo " +
				" FROM comcat_menu_afiliado m " +
				" WHERE m.ic_tipo_afiliado = ? " +
				" ORDER BY m.ig_nivel, m.cc_menu_padre, m.cg_tipo, m.cc_menu ";	//NO cambiar ORDER BY
			params = new ArrayList();
			params.add(new Integer(tipoAfiliado));
                    
                        log.debug("strSQL: "+strSQL);
                        log.debug("params: "+params);
                    
			Registros reg = con.consultarDB(strSQL, params);
			int nivel = 0;
			
			sbMenu.append(
					" [ {\"cc_menu\":\"/\",  \"expanded\": true, \"children\": [ " );
					//" [ " );
			boolean tieneHijosXProcesar = false;
			boolean menuNoProcesado = true;
			List indicesSistemaPadre = new ArrayList();
			boolean primerHijo = true;
			if(!reg.next()) {
				return "";
			}
			principal:
			while(reg.getNumeroRegistros() > 0) {
				String menuEnTurno = reg.getString("cc_menu");
				String menuPadre = reg.getString("cc_menu_padre");
				nivel = Integer.parseInt(reg.getString("ig_nivel"));
				String tipoItem = reg.getString("cg_tipo");
				
				if (menuNoProcesado) {	//Si este menu, no ha sido previamente procesado
					sbMenu.append(
							(primerHijo?"":",") +
							" { " +
							"     \"id\":\"" + menuEnTurno + "\", " + "\n" +
							"     \"cg_tipo\":\"" + reg.getString("cg_tipo") + "\", " + "\n" +
							"     \"cc_menu\":\"" + menuEnTurno + "\", " + "\n" +
							"     \"cg_nombre\":\"" + reg.getString("cg_nombre") + "\", " + "\n" +
							"     \"cg_descripcion\":\"" + reg.getString("cg_descripcion") + "\", " + "\n" +
							"     \"cg_url\":\"" + reg.getString("cg_url") + "\", " + "\n" +
							"     \"df_alta\":\"" + reg.getString("df_alta") + "\", " + "\n" +
							"     \"cg_tipo\":\"" + reg.getString("cg_tipo") + "\", " + "\n" +
							"     \"cc_menu_padre\":\"" + ( (menuPadre == null || menuPadre.equals(""))?"/":menuPadre) + "\", " + "\n" +
							"     \"cs_activo\":\"" + reg.getString("cs_activo") + "\", " + "\n"
							);
					primerHijo = false;
				}
				//Busca Hijos
				tieneHijosXProcesar = false;
				if (tipoItem.equals("M")) {	//Menu
					if(menuNoProcesado) {
						indicesSistemaPadre.add(new Integer(reg.getNumRegistro()));
						sbMenu.append( "     \"expanded\": true, \n \"children\" : [ "  + "\n" );
						primerHijo = true;
					}
					while(reg.next()) {
						int nivelHijo = Integer.parseInt(reg.getString("ig_nivel"));

						//Para evitar que recorra todos los registros en busca de hijos
						//solo se busca en el nivel siguiente (nivel + 1) al del menu en turno
						//que es en donde unicamente pueden estar los hijos.
						if (nivelHijo < nivel + 1) {
							//Estamos en un nivel anterior a donde necesitamos buscar a los hijos
							continue;  //Se va al siguiente
						} else if (nivelHijo > nivel + 1) {
							//Estamos en un nivel mayor. En este ya no es posible encontrar a mas hijos
							primerHijo = false;
							break; // No hay mas hijos
						}
						
						if (reg.getString("cc_menu_padre").equals(menuEnTurno)) {
							tieneHijosXProcesar = true;
							menuNoProcesado = true; //El menu hijo aun no ha sido procesado
							continue principal;
						}
					}
					//Ya no hay mas hijos por procesar
					if (!tieneHijosXProcesar) {
						primerHijo = false;
						sbMenu.append(
							"]"  + "\n" + "}"
						);
						
						reg.remove(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue()); ////Elimino el padre ya procesado completamente
						indicesSistemaPadre.remove(indicesSistemaPadre.size()-1);	//Elimino el indice del elemento padre ya procesado completamente
						if (indicesSistemaPadre.size() == 0) {
							reg.setNumRegistro(0);	//Se regresa al nuevo registro 0 si es que hay, si no hay la evaluaci�n del while detendra la ejecuci�n
							menuNoProcesado = true; //El menu padre (si existe) aun no ha sido procesado
							continue principal;
						}
						reg.setNumRegistro(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue());  //Se regresa al padre inmediato
						menuNoProcesado = false;	//Porque el sistema padre ya fue procesado anteriormente

					}
				} else if (tipoItem.equals("P")) {	//Pantalla
					reg.remove(); //Elimina el registro en se�al que ya fue procesado
					reg.setNumRegistro(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue()); //regresa el "apuntador" al sistema padre
					menuNoProcesado = false;  //Para indicar que el sistema padre ya fue procesado anteriormente
					sbMenu.append(
							" \"leaf\":true \n}" + "\n"
					);
				}
				
			} //fin del ciclo
			//sbMenu.deleteCharAt(sbMenu.length()-1); //borra ultima coma...
			sbMenu.append(
				"	] } ] " + "\n"
				//"	] " + "\n"
			);

			return sbMenu.toString();
		} catch(Throwable e) {
			throw new AppException("Error al obtener los datos de los menus:", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Genera el Arbol en JSON de todos los menus para su asignacion a un perfil
	 * @param tipoAfiliado Clave del tipo de afiliado (comcat_tipo_afiliado.ic_tipo_afiliado)
	 * @param perfil Clave del perfil (seg_perfil.cc_perfil)
	 * @return Cadena json con la estructura del arbol
	 */
	public static String generarArbolMenuPerfilJSON(int tipoAfiliado, String perfil) {
		AccesoDB con = new AccesoDB();
		StringBuffer sbMenu = new StringBuffer(8192);

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (perfil == null || perfil.equals("")) {
				throw new Exception("Los parametros no pueden ser nulos ni vac�os");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" perfil=" + perfil);
			throw new AppException("Error en lo parametros recibidos");
		}
		//***************************************************************************************

		
		try {
			con.conexionDB();
			
			List params = null;
			//El funcionamiento de la logica para generar el menu, se basa
			//En el correcto ordenamiento, por lo que no debe ser
			//afectado el ORDER BY del siguiente query.
			String strSQL = 
					" SELECT  m.cc_menu, m.cg_nombre, m.cg_descripcion, m.ig_nivel, m.cc_menu_padre, m.cg_tipo, " +
					"     CASE WHEN maxp.cc_menu IS NULL THEN 'N' ELSE 'S' END AS relacionada, maxp.ig_posicion, " +
					"     maxp.cs_activo, maxp.cs_acceso_directo,  " +
					"     TO_CHAR(maxp.df_alta,'dd/mm/yyyy hh24:mi') as df_alta, " +
					"		(SELECT count(1) from com_menu_afiliado_restriccion " +
					"            WHERE cc_menu = maxp.cc_menu " +
					"            AND ic_tipo_afiliado = maxp.ic_tipo_afiliado " +
					"            AND cc_perfil = maxp.cc_perfil " +
					"            AND cg_tipo_restriccion = 'D') contDirecto, " +
					"         (SELECT count(1) from com_menu_afiliado_restriccion " +
					"            WHERE cc_menu = maxp.cc_menu " +
					"            AND ic_tipo_afiliado = maxp.ic_tipo_afiliado " +
					"            AND cc_perfil = maxp.cc_perfil " +
					"            AND cg_tipo_restriccion = 'R') contRel " +
					" FROM comcat_menu_afiliado m, comrel_menu_afiliado_x_perfil maxp " +
					" WHERE m.ic_tipo_afiliado = ? " +
					" AND m.ic_tipo_afiliado = maxp.ic_tipo_afiliado (+) " +
					" AND m.cc_menu = maxp.cc_menu (+) " +
					" AND ? = maxp.cc_perfil (+) " +
					" AND m.cs_activo = ?  " +
					" ORDER BY m.ig_nivel, m.cc_menu_padre, m.cg_tipo, maxp.ig_posicion, m.cc_menu  ";	//NO cambiar ORDER BY
			params = new ArrayList();
			params.add(new Integer(tipoAfiliado));
			params.add(perfil);
			params.add("S");	//cs_activo = S
			Registros reg = con.consultarDB(strSQL, params);
			int nivel = 0;
			
			sbMenu.append(
					" [ {\"cc_menu\":\"/\", \"relacionada\":true, \"expanded\": true, \"children\": [ " );
			boolean tieneHijosXProcesar = false;
			boolean menuNoProcesado = true;
			List indicesSistemaPadre = new ArrayList();
			boolean primerHijo = true;
			if(!reg.next()) {
				return "";
			}
			principal:
			while(reg.getNumeroRegistros() > 0) {
				String menuEnTurno = reg.getString("cc_menu");
				String menuPadre = reg.getString("cc_menu_padre");
				nivel = Integer.parseInt(reg.getString("ig_nivel"));
				String tipoItem = reg.getString("cg_tipo");
				
				if (menuNoProcesado) {	//Si este menu, no ha sido previamente procesado
					String contDirecto = reg.getString("contDirecto");
					String contRel = reg.getString("contRel");

					sbMenu.append(
							(primerHijo?"":",") +
							" { " +
							"     \"id\":\"" + menuEnTurno + "\", " + "\n" +
							"     \"cc_menu\":\"" + menuEnTurno + "\", " + "\n" +
							"     \"cg_tipo\":\"" + reg.getString("cg_tipo") + "\", " + "\n" +
							"     \"cg_nombre\":\"" + reg.getString("cg_nombre") + "\", " + "\n" +
							"     \"cg_descripcion\":\"" + reg.getString("cg_descripcion") + "\", " + "\n" +
							"     \"relacionada\":" + ("S".equals(reg.getString("relacionada"))?"true":"false") + ", " + "\n" +
							"     \"ig_posicion\":\"" + reg.getString("ig_posicion") + "\", " + "\n" +
							"     \"df_alta\":\"" + reg.getString("df_alta") + "\", " + "\n" +
							"     \"cs_acceso_directo\":\"" + reg.getString("cs_acceso_directo") + "\", " + "\n" +
							"     \"cg_conteo_d\":\"" + reg.getString("contDirecto") + "\", " + "\n" +
							"     \"cg_conteo_r\":\"" + reg.getString("contRel") + "\", " + "\n" +
							"     \"cs_activo\":\"" + reg.getString("cs_activo") + "\", " + "\n"
							);
					primerHijo = false;
				}
				//Busca Hijos
				tieneHijosXProcesar = false;
				if (tipoItem.equals("M")) {	//Menu
					if(menuNoProcesado) {
						indicesSistemaPadre.add(new Integer(reg.getNumRegistro()));
						sbMenu.append( "     \"expanded\": false, \n \"children\" : [ "  + "\n" );
						primerHijo = true;
					}
					while(reg.next()) {
						int nivelHijo = Integer.parseInt(reg.getString("ig_nivel"));

						//Para evitar que recorra todos los registros en busca de hijos
						//solo se busca en el nivel siguiente (nivel + 1) al del menu en turno
						//que es en donde unicamente pueden estar los hijos.
						if (nivelHijo < nivel + 1) {
							//Estamos en un nivel anterior a donde necesitamos buscar a los hijos
							continue;  //Se va al siguiente
						} else if (nivelHijo > nivel + 1) {
							//Estamos en un nivel mayor. En este ya no es posible encontrar a mas hijos
							primerHijo = false;
							break; // No hay mas hijos
						}
						
						if (reg.getString("cc_menu_padre").equals(menuEnTurno)) {
							tieneHijosXProcesar = true;
							menuNoProcesado = true; //El menu hijo aun no ha sido procesado
							continue principal;
						}
					}
					//Ya no hay mas hijos por procesar
					if (!tieneHijosXProcesar) {
						primerHijo = false;
						sbMenu.append(
							"]"  + "\n" + "}"
						);
						
						reg.remove(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue()); ////Elimino el padre ya procesado completamente
						indicesSistemaPadre.remove(indicesSistemaPadre.size()-1);	//Elimino el indice del elemento padre ya procesado completamente
						if (indicesSistemaPadre.size() == 0) {
							reg.setNumRegistro(0);	//Se regresa al nuevo registro 0 si es que hay, si no hay la evaluaci�n del while detendra la ejecuci�n
							menuNoProcesado = true; //El menu padre (si existe) aun no ha sido procesado
							continue principal;
						}
						reg.setNumRegistro(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue());  //Se regresa al padre inmediato
						menuNoProcesado = false;	//Porque el sistema padre ya fue procesado anteriormente

					}
				} else if (tipoItem.equals("P")) {	//Pantalla
					reg.remove(); //Elimina el registro en se�al que ya fue procesado
					reg.setNumRegistro(((Integer)indicesSistemaPadre.get(indicesSistemaPadre.size()-1)).intValue()); //regresa el "apuntador" al sistema padre
					menuNoProcesado = false;  //Para indicar que el sistema padre ya fue procesado anteriormente
					sbMenu.append(
							" \"leaf\":true \n}" + "\n"
					);
				}
				
			} //fin del ciclo
			sbMenu.append(
				"	] } ] " + "\n"
			);

			return sbMenu.toString();
		} catch(Throwable e) {
			throw new AppException("Error al obtener los datos de los menus:", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

}
