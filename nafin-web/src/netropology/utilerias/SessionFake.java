package netropology.utilerias;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

public class SessionFake implements HttpSession {
    private HashMap<String, Object> hm = new HashMap();
    private ServletContextFake servletContextFake;

    public SessionFake() {
    }

    public SessionFake(HttpSession session) {
        servletContextFake = new ServletContextFake(session.getServletContext());
        cloneProperties(session);
    }

    public void cloneProperties(HttpSession session) {
        
        Enumeration nombres = session.getAttributeNames();
        for (Enumeration e = session.getAttributeNames(); e.hasMoreElements();) {
            String nombreAtributo = (String)e.nextElement();
            hm.put(nombreAtributo, session.getAttribute(nombreAtributo));
        }
        
        hm.put("strDirectorioPublicacion", session.getServletContext().getAttribute("strDirectorioPublicacion"));

    }

    public Object getAttribute(String atributo) {
        return hm.get(atributo)==null?"":hm.get(atributo);
    }

    @Override
    public long getCreationTime() {
        // TODO Implement this method
        return 0L;
    }

    @Override
    public String getId() {
        // TODO Implement this method
        return null;
    }

    @Override
    public long getLastAccessedTime() {
        // TODO Implement this method
        return 0L;
    }

    @Override
    public ServletContext getServletContext() {
        // TODO Implement this method
        return servletContextFake;
    }

    @Override
    public void setMaxInactiveInterval(int i) {
        // TODO Implement this method
    }

    @Override
    public int getMaxInactiveInterval() {
        // TODO Implement this method
        return 0;
    }

    @Override
    public HttpSessionContext getSessionContext() {
        // TODO Implement this method
        return null;
    }

    @Override
    public Object getValue(String string) {
        // TODO Implement this method
        return null;
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        // TODO Implement this method
        return null;
    }

    @Override
    public String[] getValueNames() {
        // TODO Implement this method
        return new String[0];
    }

    @Override
    public void setAttribute(String string, Object object) {
        // TODO Implement this method

    }

    @Override
    public void putValue(String string, Object object) {
        // TODO Implement this method

    }

    @Override
    public void removeAttribute(String string) {
        // TODO Implement this method
    }

    @Override
    public void removeValue(String string) {
        // TODO Implement this method
    }

    @Override
    public void invalidate() {
        // TODO Implement this method
    }

    @Override
    public boolean isNew() {
        // TODO Implement this method
        return false;
    }
}
