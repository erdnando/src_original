package com.nafin.nafinetmovil.transaccional;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class TransaccionalFiltro implements Filter
{
  private FilterConfig _filterConfig = null;

  public void init(FilterConfig filterConfig) throws ServletException
  {
    _filterConfig = filterConfig;
  }

  public void destroy()
  {
    _filterConfig = null;
  }

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
  {
    String requestedUrl=null;
    //String context=null;
    String urlClean=null;
    try{
      HttpServletRequest httpRequest = (HttpServletRequest) request;
      //HttpServletResponse httpResponse = (HttpServletResponse) response;
      requestedUrl =httpRequest.getRequestURL().toString();
      System.out.println("Filtro.requestedUrl: "+requestedUrl);
      //context =httpRequest.getContextPath();
      //System.out.println("Filtro.context: "+context);
      urlClean = requestedUrl.substring(requestedUrl.indexOf("nafinetmovil")+1+12);	//12="nafinetmovil".length;
      System.out.println("Filtro.urlClean: "+urlClean);
      httpRequest.setAttribute("url",urlClean);
      RequestDispatcher dispatcher =httpRequest.getRequestDispatcher("/nafinetmovil/transaccional/TransaccionalDespachador");
			dispatcher.forward(request, response);
     }catch(Throwable e){
        e.printStackTrace();
     }
  }
}