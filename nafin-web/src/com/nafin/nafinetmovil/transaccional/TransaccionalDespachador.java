package com.nafin.nafinetmovil.transaccional;

import com.nafin.nafinetmovil.utils.Constantes;
import com.nafin.nafinetmovil.utils.FileManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TransaccionalDespachador extends HttpServlet
{
  private static final String CONTENT_TYPE = "application/xhtml+xml";

  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request,response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    String urlClean=null;
    String path=null;
    ServletContext contextp=null;
    String realPath=null;
    //JerarquiaSeccion seccion=null;
    RequestDispatcher rdisp=null;
      
    try{
      response.setContentType(CONTENT_TYPE);
      urlClean=(String)request.getAttribute("url");
      System.out.println("urlClean: "+urlClean);
      path=urlClean;
      contextp = request.getSession().getServletContext();
      System.out.println("contextp: "+contextp);
      realPath = contextp.getRealPath(path);
      System.out.println("realPath: "+realPath);
      if(FileManager.existeArchivo(realPath)){ //xml estaticos en el servidor
        despachaArchivo(response,realPath);
      }
      if("transaccional/menu.xhtml".equals(urlClean)){
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/menu.jsp");
      }
      else if("transaccional/select_doctos.xhtml".equals(urlClean)){
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/select_doctos.jsp");
      }
      else if("transaccional/logout.xhtml".equals(urlClean)){
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/salir.jsp");
      }
      else if("transaccional/consultaif.xhtml".equals(urlClean)){
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/consultaIF.jsp");
      }
      else if("transaccional/consultaepo.xhtml".equals(urlClean)){
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/consultaEPO.jsp");
      }
      else if("transaccional/menuif.xhtml".equals(urlClean)){
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/menuIF.jsp");
      }
      else if("transaccional/menuepo.xhtml".equals(urlClean)){
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/menuEPO.jsp");
      }
      else{ 
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/login.jsp");
      }

    }catch(Exception e){
      System.out.println("*********TransaccionalDespachador***************");
      e.printStackTrace();
      rdisp=request.getRequestDispatcher(Constantes.RUTA_ERROR_PAGE);
    }finally{
      if(rdisp!=null){
        try {
		    rdisp.forward(request,response);
		  } catch(Throwable t) {
			  t.printStackTrace();
			  throw t;
		  }
      }
    }
  }

  private void despachaArchivo(HttpServletResponse response,String realPath) throws Exception{
    InputStream inputStream = null;
		OutputStream outputStream = null;
    inputStream = FileManager.getStream(realPath);
    outputStream = response.getOutputStream();
    FileManager.write(inputStream, outputStream);
    inputStream.close();
    outputStream.flush();
    outputStream.close();
  }

}