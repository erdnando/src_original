package com.nafin.nafinetmovil.transaccional;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AcuseS extends HttpServlet 
{
  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    RequestDispatcher rdisp=null;
    try {
  
      String icPyme=request.getParameter("txtPyme");
      String icEPO=request.getParameter("txtEPO");
      String icIF=request.getParameter("cboIF");
      String icMoneda=request.getParameter("cboMoneda");
      String [] strParameterValuesChk = request.getParameterValues("chkP");
      String txtPos1=request.getParameter("txtPos1");
      String txtPos2=request.getParameter("txtPos2");
      String txtPos3=request.getParameter("txtPos3");
      String txtPos4=request.getParameter("txtPos4");
      String txtPos5=request.getParameter("txtPos5");
      String txtPos6=request.getParameter("txtPos6");
      String txtPos7=request.getParameter("txtPos7");
      String txtPos8=request.getParameter("txtPos8");
      String strFechaSigDia=request.getParameter("txtFechaSigDia");

      /*System.out.println("AcuseS:icPyme: "+icPyme);
      System.out.println("AcuseS:icEPO: "+icEPO);
      System.out.println("AcuseS:icIF: "+icIF);
      System.out.println("AcuseS:icMoneda: "+icMoneda);
      System.out.println("AcuseS:txtPos1: "+txtPos1);
      System.out.println("AcuseS:txtPos2: "+txtPos2);
      System.out.println("AcuseS:txtPos3: "+txtPos3);
      System.out.println("AcuseS:txtPos4: "+txtPos4);
      System.out.println("AcuseS:txtPos5: "+txtPos5);
      System.out.println("AcuseS:txtPos6: "+txtPos6);
      System.out.println("AcuseS:txtPos7: "+txtPos7);
      System.out.println("AcuseS:txtPos8: "+txtPos8);*/

      request.setAttribute("txtPyme",icPyme);
      request.setAttribute("txtEPO",icEPO);
      request.setAttribute("cboIF",icIF);
      request.setAttribute("cboMoneda",icMoneda);
      request.setAttribute("txtPos1",txtPos1);
      request.setAttribute("txtPos2",txtPos2);
      request.setAttribute("txtPos3",txtPos3);
      request.setAttribute("txtPos4",txtPos4);
      request.setAttribute("txtPos5",txtPos5);
      request.setAttribute("txtPos6",txtPos6);
      request.setAttribute("txtPos7",txtPos7);
      request.setAttribute("txtPos8",txtPos8);
      request.setAttribute("strParameterValuesChk",strParameterValuesChk);
      request.setAttribute("txtFechaSigDia",strFechaSigDia);

      rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/acuse.jsp");
    }catch(Exception e){
      System.out.println("*********AcuseS***************");
      e.printStackTrace();
      rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/acuse.jsp");
    }finally{
      if(rdisp!=null){
        rdisp.forward(request,response);
      }
    } 
  }
}
