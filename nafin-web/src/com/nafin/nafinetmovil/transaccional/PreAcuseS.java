package com.nafin.nafinetmovil.transaccional;

import com.nafin.nafinetmovil.utils.ServiceLocator;
import com.nafin.nafinetmovil.utils.UsuarioBasico;

import com.netro.descuento.movil.SeleccionMovilDoctos;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class PreAcuseS extends HttpServlet 
{
  //private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  HttpSession session;
  
  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    RequestDispatcher rdisp = null;
    String [] strParameterValuesChk;
    //SeleccionMovilDoctos selecDoctosMovilEJB = null;
    HashMap respuesta = null;
    Boolean bHayCveSession = new Boolean(false);
    String user = null;
    UsuarioBasico userBasico = null;
    
    try {
      session = request.getSession(false);
    
      if(session.getAttribute("user")!=null){
        userBasico = (UsuarioBasico)session.getAttribute("user");
        user = userBasico.getCn();
      }

      strParameterValuesChk = request.getParameterValues("chkP");

      respuesta = this.getSelecDoctosMovil().getParametrosClaveConfirmacion(user);
      bHayCveSession = new Boolean((String)respuesta.get("hayClaveCesion"));
      
      //for(int i=0;i<strParameterValuesChk.length;i++)
      //  System.out.println("strParameterValuesChk: "+strParameterValuesChk[i]);

  
      String icPyme=request.getParameter("txtPyme");
      String icEPO=request.getParameter("txtEPO");
      String icIF=request.getParameter("cboIF");
      String icMoneda=request.getParameter("cboMoneda");
      String strFechaSigDia=request.getParameter("txtFechaSigDia");
      
      //System.out.println("icPyme: "+icPyme);
      //System.out.println("icEPO: "+icEPO);
      //System.out.println("icIF: "+icIF);
      request.setAttribute("txtPyme",icPyme);
      request.setAttribute("txtEPO",icEPO);
      request.setAttribute("cboIF",icIF);
      request.setAttribute("cboMoneda",icMoneda);
      request.setAttribute("strParameterValuesChk",strParameterValuesChk);
      request.setAttribute("txtFechaSigDia",strFechaSigDia);
      
      if(!bHayCveSession.booleanValue())
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/cve_confirmacion.jsp");
      else
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/pre_acuse.jsp");
    }catch(Exception e){
      System.out.println("*********PreAcuseS***************");
      e.printStackTrace();
      rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/pre_acuse.jsp");
    }finally{
      if(rdisp!=null){
        rdisp.forward(request,response);
      }
    } 
  }
  
  private SeleccionMovilDoctos getSelecDoctosMovil() throws Exception
  {
      SeleccionMovilDoctos seleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionMovilDoctosEJB", SeleccionMovilDoctos.class);
      return seleccionDocumento;
  } 
  
}
