package com.nafin.nafinetmovil.transaccional;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class SelectDoctosS extends HttpServlet 
{
  //private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    RequestDispatcher rdisp=null;
    try {
  
      String icPyme=request.getParameter("txtPyme");
      String icEPO=request.getParameter("txtEPO");
      String icIF=request.getParameter("cboIF");
      String icMoneda=request.getParameter("cboMoneda");
		
      System.out.println("SelectDoctosS::icPyme: "+icPyme);
      System.out.println("SelectDoctosS::icEPO: "+icEPO);
      System.out.println("SelectDoctosS::icIF: "+icIF);
      System.out.println("SelectDoctosS::icMoneda: "+icMoneda);
		
      request.setAttribute("txtPyme",icPyme);
      request.setAttribute("txtEPO",icEPO);
      request.setAttribute("cboIF",icIF);
      request.setAttribute("cboMoneda",icMoneda);
      rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/select_doctos.jsp");
    }catch(Exception e){
      System.out.println("*********SelectDoctosS***************");
      e.printStackTrace();
      rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/select_doctos.jsp");
    }finally{
      if(rdisp!=null){
        rdisp.forward(request,response);
      }
    } 
  }
}
