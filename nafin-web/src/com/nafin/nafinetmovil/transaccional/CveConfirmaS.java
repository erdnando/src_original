package com.nafin.nafinetmovil.transaccional;

import com.nafin.nafinetmovil.utils.ServiceLocator;
import com.nafin.nafinetmovil.utils.UsuarioBasico;

import com.netro.descuento.movil.SeleccionMovilDoctos;

import java.io.IOException;

import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class CveConfirmaS extends HttpServlet 
{
  //private static final String CONTENT_TYPE = "text/html; charset=windows-1252";
  HttpSession session;
  
  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doPost(request, response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    RequestDispatcher rdisp = null;
    String [] strParameterValuesChk;
    SeleccionMovilDoctos selecDoctosMovilEJB = null;
    HashMap respuesta = null;
    String user = null;
    UsuarioBasico userBasico = null;

    Boolean bHayMensaje = new Boolean(false);
    Boolean bExito = new Boolean(false);
    Boolean bHayError = new Boolean(false);
		String strMensaje = null;
    
    try {
      session = request.getSession(false);

      if(session.getAttribute("user")!=null){
        userBasico = (UsuarioBasico)session.getAttribute("user");
        user = userBasico.getCn();
      }

      strParameterValuesChk = request.getParameterValues("chkP");
      String icPyme=request.getParameter("txtPyme");
      String icEPO=request.getParameter("txtEPO");
      String icIF=request.getParameter("cboIF");
      String icMoneda=request.getParameter("cboMoneda");
      String strCveConfirma=request.getParameter("txtClave1");
      String strFechaSigDia=request.getParameter("txtFechaSigDia");
      

      /*System.out.println("CveConfirmaS:strParameterValuesChk: "+strParameterValuesChk);
      System.out.println("CveConfirmaS:icPyme: "+icPyme);
      System.out.println("CveConfirmaS:icEPO: "+icEPO);
      System.out.println("CveConfirmaS:icIF: "+icIF);
      System.out.println("CveConfirmaS:icMoneda: "+icMoneda);
      System.out.println("CveConfirmaS:strCveConfirma: "+strCveConfirma);*/
      
      respuesta 	= this.getSelecDoctosMovil().guardarClaveConfirmacion(user, strCveConfirma);
      bExito = new Boolean((String)respuesta.get("exito"));
      bHayError = new Boolean((String)respuesta.get("hayError"));
      bHayMensaje = new Boolean((String)respuesta.get("hayMensaje"));
      strMensaje = (String)respuesta.get("mensaje");
      
      request.setAttribute("txtPyme",icPyme);
      request.setAttribute("txtEPO",icEPO);
      request.setAttribute("cboIF",icIF);
      request.setAttribute("cboMoneda",icMoneda);
      request.setAttribute("strParameterValuesChk",strParameterValuesChk);
      request.setAttribute("txtClave1",strCveConfirma);
      request.setAttribute("txtFechaSigDia",strFechaSigDia);

      request.setAttribute("bHayMensaje",bHayMensaje.toString());
      request.setAttribute("bHayError",bHayError.toString());
      request.setAttribute("strMensaje",strMensaje);
      
      if(bExito.booleanValue())
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/pre_acuse.jsp");
      else
        rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/cve_confirma.jsp");

    }catch(Exception e){
      System.out.println("*********CveConfirmaS***************");
      e.printStackTrace();
      rdisp=request.getRequestDispatcher("/nafinetmovil/transaccional/cve_confirma.jsp");
    }finally{
      if(rdisp!=null){
        rdisp.forward(request,response);
      }
    } 
  }
  
  private SeleccionMovilDoctos getSelecDoctosMovil() throws Exception
  {
      SeleccionMovilDoctos seleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionMovilDoctosEJB", SeleccionMovilDoctos.class);
      return seleccionDocumento;
  } 
  
}
