package com.nafin.nafinetmovil.admin;

import com.nafin.nafinetmovil.ejbs.Seguridad;
import com.nafin.nafinetmovil.utils.ServiceLocator;
import com.nafin.nafinetmovil.utils.UsuarioBasico;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SeguridadOIDS extends HttpServlet{

  public void init(ServletConfig config) throws ServletException{
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
    doPost(request,response);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

    String user=request.getParameter("movilUser");
    String password=request.getParameter("movilPassword");
    String ruta="login.jsp";
    RequestDispatcher rdisp=null;
    Seguridad seguridad=null;
    UsuarioBasico usrBasico=null;
    char tipoAfiliado;
    
    try{
      if(user!=null && password!=null){
        seguridad=this.getSeguridad();
        boolean bIsValidUser = seguridad.isValidUser(user,password);
        //boolean bIsValidUser=true;
		  System.out.println("SeguridadOIDS::doPost::bIsValidUser:"+bIsValidUser);
		 
        if(bIsValidUser){
          usrBasico = seguridad.getUsuario(user);
          tipoAfiliado=usrBasico.getTipoAfiliado().charAt(0);
          /*System.out.println("Los valores que se obtienen del UsuarioBasico: ");
          System.out.println("El login: "+usrBasico.getCn());
          System.out.println("El email: "+usrBasico.getMail());
          System.out.println("El apellido paterno: "+usrBasico.getApellidoPaterno());
          System.out.println("El apellido materno: "+usrBasico.getApellidoMaterno());
          System.out.println("El nombre: "+usrBasico.getNombre());
          System.out.println("El CP: "+usrBasico.getCP());
          System.out.println("La Ciudad: "+usrBasico.getCiudad());
          System.out.println("El Estado: "+usrBasico.getEstado());
          System.out.println("La Calle: "+usrBasico.getCalle());
          System.out.println("Tipo Afiliado: "+usrBasico.getTipoAfiliado());
          System.out.println("Cve Afiliado: "+usrBasico.getCveAfiliado());
          System.out.println("Perfil: "+usrBasico.getPerfil());*/
          
          switch (tipoAfiliado){
            case 'E': ruta="menuEPO.jsp"; break;
            case 'P': ruta="menu.jsp"; break;
            case 'I': ruta="menuIF.jsp"; break;
            default: 
                    ruta="login.jsp";
                    request.setAttribute("mensaje","No tiene acceso, su perfil no est&aacute; habilitado para esta aplicaci&oacute;n");
                    break;
          };
          
          HttpSession sesion=request.getSession();
          sesion.setAttribute("user",usrBasico);
          //ruta="menu.jsp";
        }else{
          //Aqui se tiene que ver si se da de alta
          
            request.setAttribute("mensaje","No tiene acceso, verifique su usuario y contrase&ntilde;a");
        }
         
      }else{
        request.setAttribute("mensaje","No tiene acceso, verifique su usuario y Contrase&ntilde;a");
      } 
    }catch(Exception e){
      e.printStackTrace();
      request.setAttribute("mensaje","Error:"+e.getMessage());
      ruta="login.xhtml";
    }finally{
      rdisp=request.getRequestDispatcher(ruta);
      System.out.println("ruta: "+ruta);
      rdisp.forward(request,response);
    }
  }
  
  private Seguridad getSeguridad() throws Exception
  {
      Seguridad seguridad = ServiceLocator.getInstance().lookup("SeguridadMovilEJB", Seguridad.class);
      return seguridad;
  } 
  
  
}