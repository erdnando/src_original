package com.nafin.nafinetmovil.utils;
import java.util.*;
import java.util.regex.*;
import javax.servlet.http.HttpServletRequest;

public class GridFilter 
{
    public static HashMap getParameters(HttpServletRequest request){
    HashMap parametros=new HashMap();
    try{
        String dummy="";
        Enumeration x=request.getParameterNames(); 
        while (x.hasMoreElements())
        {
          dummy=(String)x.nextElement();
          if(dummy.indexOf("filter")!=-1)
          {
            parametros.put(dummy,request.getParameter(dummy));
          }
        } 
      }catch(Exception e)
      {
      
      }
      return parametros;
    }
    
    
    public static String creaFiltro(HashMap parametros)throws Exception
    {
        String filtro="  "; 
        try
        {
            String type;
            String field;
            String value;
            for(int i=0;i<parametros.size();i++)
            {
                if(parametros.containsKey("filter["+i+"][data][type]"))
                {
                    type=(String)parametros.get("filter["+i+"][data][type]");
                    if(type.equals("string"))
                    {
                        if(parametros.containsKey("filter["+i+"][field]") && parametros.containsKey("filter["+i+"][data][value]"))
                        {
                            String vlsAux=((String)parametros.get("filter["+i+"][data][value]")).toUpperCase();
                            Pattern patron = Pattern.compile("\\*");
                            Matcher encaja = patron.matcher(vlsAux);
                            String resultado = encaja.replaceAll("%");
                            if(i>0)                            
                                filtro+=" AND";
                            filtro+=" UPPER("+(String)parametros.get("filter["+i+"][field]")+")";
                            filtro+=" LIKE '"+resultado+"'";
                        }        
                    }
                    else if(type.equals("list"))
                    {
                        if(parametros.containsKey("filter["+i+"][field]") && parametros.containsKey("filter["+i+"][data][value]"))
                        {
                            String tokens[]=((String)parametros.get("filter["+i+"][data][value]")).split(",");
                            String list="";
                            String comma="";
                            for(int k=0;k<tokens.length;k++)
                            {
                                list+=comma+"'"+tokens[k]+"'";
                                comma=",";
                            }
                            filtro+=" and "+(String)parametros.get("filter["+i+"][field]")+" IN ("+list+")";;               
                        }       
                    }
                    else if(type.equals("boolean"))
                    {
                        if(parametros.containsKey("filter["+i+"][field]") && parametros.containsKey("filter["+i+"][data][value]"))
                        {
                            filtro+=" and "+(String)parametros.get("filter["+i+"][field]")+"=";
                            filtro+=(String)parametros.get("filter["+i+"][data][value]");
                        }     
                    }
                    else if(type.equals("numeric"))
                    {
                        if(parametros.containsKey("filter["+i+"][field]") && parametros.containsKey("filter["+i+"][data][value]") && parametros.containsKey("filter["+i+"][data][comparison]"))
                        {
                            if(i>0)                            
                                filtro+=" AND ";
                            if(((String)parametros.get("filter["+i+"][data][comparison]")).equals("eq"))
                            {
                                filtro+=(String)parametros.get("filter["+i+"][field]")+"=";
                                filtro+=(String)parametros.get("filter["+i+"][data][value]");
                            }
                            else if(((String)parametros.get("filter["+i+"][data][comparison]")).equals("lt"))
                            {
                                filtro+=(String)parametros.get("filter["+i+"][field]")+"<";
                                filtro+=(String)parametros.get("filter["+i+"][data][value]");
                            }
                            else if(((String)parametros.get("filter["+i+"][data][comparison]")).equals("gt"))
                            {
                                filtro+=(String)parametros.get("filter["+i+"][field]")+">";
                                filtro+=(String)parametros.get("filter["+i+"][data][value]");
                            }
                        }  
                    }
                    else if(type.equals("date"))
                    {
                        if(parametros.containsKey("filter["+i+"][field]") && parametros.containsKey("filter["+i+"][data][value]") && parametros.containsKey("filter["+i+"][data][comparison]"))
                        {
                            if(i>0)                            
                                filtro+=" AND ";
                            if(((String)parametros.get("filter["+i+"][data][comparison]")).equals("eq"))
                            {
                                filtro+=(String)parametros.get("filter["+i+"][field]")+" = ";
                                filtro+="TO_DATE('"+(String)parametros.get("filter["+i+"][data][value]")+"','dd/MM/YYYY')";
                            }
                            else if(((String)parametros.get("filter["+i+"][data][comparison]")).equals("lt"))
                            {
                                filtro+=(String)parametros.get("filter["+i+"][field]")+" < ";
                                filtro+="TO_DATE('"+(String)parametros.get("filter["+i+"][data][value]")+"','dd/MM/YYYY')";
                            }
                            else if(((String)parametros.get("filter["+i+"][data][comparison]")).equals("gt"))
                            {
                                filtro+=(String)parametros.get("filter["+i+"][field]")+" > ";
                                filtro+="TO_DATE('"+(String)parametros.get("filter["+i+"][data][value]")+"','dd/MM/YYYY')";
                            }
                        }  
                    }
                }
            }
        }
        catch(Exception e)
        {
            throw new Exception("GridFilter,Error:"+e.getMessage());
        }
        return " WHERE "+ filtro;
    }
}