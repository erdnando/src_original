package com.nafin.nafinetmovil.utils;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EnviaCorreoUtils 
{
  public EnviaCorreoUtils()
  {
  }
  
  /**
   * 
   * @throws java.lang.Exception
   * @return 
   * @param mensaje
   * @param paraParam
   * @param fromParam
   * @param path
   */
  public String construyeMensaje(String path, String fromParam, String paraParam, String mensaje, String rutaImagen) throws Exception {
    StringBuffer sb = new StringBuffer();
    String resultado = "";
    try {
      String line = null; 
      BufferedReader br = new BufferedReader(new FileReader(path)); 
      while ((line = br.readLine()) != null) {
        if(line.indexOf("<tagFrom>") != -1) {
          sb.append(line);
          sb.append(fromParam);
        } else if(line.indexOf("<tagPara>") != -1) {
          sb.append(line);
          sb.append(paraParam);
        } else if(line.indexOf("<tagCuerpo>") != -1) {
          sb.append(line);
          sb.append(mensaje);
        } else if(line.indexOf("<tagImgURL>") != -1) {
          sb.append(line);
          if(rutaImagen!=null)
          sb.append("<img src=\"" + rutaImagen + "\">");
        } else {
          sb.append(line);
        }
      }
      br.close();
      resultado = sb.toString();
    } catch(Exception e) {
      throw new Exception();
    }
    return resultado;
  }
  
  /**
   * 
   * @throws java.lang.Exception
   * @param mensaje
   * @param asunto
   * @param ccAddr
   * @param toAddr
   * @param fromAddr
   * @param host
   */
  public void enviaCorreo(String host, String fromAddr, String toAddr, List ccAddr, String asunto, String mensaje) throws Exception {
    try {
      //Invoca el host del servidor SMTP
      Properties properties =  System.getProperties();
	    properties.put("mail.smtp.host",host);
	    Session session = Session.getInstance(properties,null);
      System.out.println("      ++ Establece el host ++     ");
	       
      //Construye la sesi�n del servidor SMTP
	    MimeMessage msg = new MimeMessage(session);
	    msg.setFrom(new InternetAddress(fromAddr));
      System.out.println("      ++ Establece el Emisor ++     ");
	        
      //Obtiene la direcci�n de la cuenta a la que se le env�a
	    InternetAddress [] address1 = { new InternetAddress(toAddr)};
	    msg.setRecipients(Message.RecipientType.TO, address1);
      System.out.println("      ++ Establece el destinatario ++     ");
			
      //Agrega los correos con copia
      if(ccAddr.size() > 0) {
        for(int i = 0; i < ccAddr.size(); i++) {
          if(ccAddr.get(i).toString() != null && !ccAddr.get(i).toString().trim().equals("")) {
            msg.addRecipient(Message.RecipientType.CC, new InternetAddress((String)ccAddr.get(i)));
          }
        }
      }
      System.out.println("      ++ Establece correos CC ++     ");
	    
      //Agrega el asunto del mensaje
	    msg.setSubject(asunto);
      msg.setSentDate(new Date());
      
      //Agrega el cuerpo del mensaje
      DataHandler data = new DataHandler(mensaje, "text/html");
      msg.setDataHandler(data);
      System.out.println("      ++ Establece el cuerpo del mensaje ++     ");

      //Env�a el correo
      Transport.send(msg);
      System.out.println("      ++ Envia el correo ++     ");
    } catch(MessagingException messagingException) {
      messagingException.printStackTrace();
      System.out.println("      ++ Error en envio de Correos  ++     ");
      throw new Exception();
    }
  }
}