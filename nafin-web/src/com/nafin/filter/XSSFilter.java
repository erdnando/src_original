package com.nafin.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class XSSFilter implements Filter {
    
    private FilterConfig _filterConfig = null;
    
    public void init(FilterConfig filterConfig) throws ServletException {
        _filterConfig = filterConfig;
    }
    
    public void destroy() {
        _filterConfig = null;
    }
    
    /* private String [] notApplyHtmlFilter = {
        "/07proyecto/07seguimiento/07MatrizObligacionesContractuales01ext.data.jsp",
        "/07proyecto/07disposicion/07CartasBancos01ext.data.jsp",
        "\07proyecto\07disposicion\07ClausulasVencer01ext.js",
    }; */
    
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,ServletException {
        boolean httpTagsFilter = true;
        ((HttpServletResponse)response).setHeader("X-XSS-Protection", "1; mode=block");
        ((HttpServletResponse)response).addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
        //((HttpServletResponse)response).addHeader("X-Content-Type-Options", "nosniff");
        String informacion = ((HttpServletRequest) request).getParameter("informacion");
        if(informacion!=null && informacion.equals("EnviaCorreo")){
            httpTagsFilter = false;
        }
        chain.doFilter(new XSSRequestWrapper((HttpServletRequest) request,httpTagsFilter), response);
    }
    
}//XSSFilter
