package com.nafin.filter;

import java.util.regex.Pattern;

public class UtilXSSSanatizer {
    
    public static Pattern[] SRC_ATTRIBUTES_PATTERNS = new Pattern[]{
        // src='...'
        Pattern.compile("src[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("src[\r\n]*=[\r\n]*(\\\\)*\\\"(.*?)(\\\\)*\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("onerror[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("onerror[\r\n]*=[\r\n]*(\\\\)*\\\"(.*?)(\\\\)*\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("onload[\r\n]*=[\r\n]*\\\'(.*?)\\\'", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
        Pattern.compile("onload[\r\n]*=[\r\n]*(\\\\)*\\\"(.*?)(\\\\)*\\\"", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL)   
    };
    
    public  static Pattern[] XSS_PATTERNS = new Pattern[]{
           // Script fragments
           Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE),
           
           // lonely script tags
           Pattern.compile("</script>", Pattern.CASE_INSENSITIVE),
           Pattern.compile("<script(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
           // eval(...)
           Pattern.compile("eval\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
           // expression(...)
           Pattern.compile("expression\\((.*?)\\)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL),
           // javascript:...
           Pattern.compile("javascript:", Pattern.CASE_INSENSITIVE),
           // vbscript:...
           Pattern.compile("vbscript:", Pattern.CASE_INSENSITIVE),
           // onload(...)=...
           Pattern.compile("onload(.*?)=", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL)
       };
    
    public static String replace(String value, Pattern[]patterns){
        for (Pattern scriptPattern : patterns){
            value = scriptPattern.matcher(value).replaceAll("");
        }
        return value;
    }
    
    public static String sanatize(String value){
        String resultado = value;
        resultado = replace(value, XSS_PATTERNS);
        resultado = replace(resultado, SRC_ATTRIBUTES_PATTERNS);
        resultado = HTMLEntityEncode(resultado);
        return resultado;
    }
    
    public static String HTMLEntityEncode(String input) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < input.length(); ++i) {
            char ch = input.charAt(i);

            if (ch == '<') {
                sb.append("&lt;");
            } else if (ch == '>') {
                sb.append("&gt;");
            } else {
                sb.append(ch);
            }
        }

        return sb.toString();
    } 
    
    public static String HTMLFullEncode(String input) {
        StringBuffer sb = new StringBuffer();
        if(input!=null){
            for (int i = 0; i < input.length(); ++i) {
                char ch = input.charAt(i);
    
                if (ch == '<') {
                    sb.append("&lt;");
                } else if (ch == '>') {
                    sb.append("&gt;");
                } else if (ch == '&') {
                    sb.append("&amp;");
                } else if (ch == '"') {
                    sb.append("&quot;");
                } else if (ch == '\'') {
                    sb.append("&#x27;");
                } else if (ch == '/') {
                    sb.append("&#x2F;");
                }  else {
                    sb.append(ch);
                }
            }
        }
        return sb.toString();
    } 
    
    public UtilXSSSanatizer() {
        super();
    }
}
