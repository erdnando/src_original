package com.nafin.filter;

import java.text.Normalizer;
import java.text.Normalizer.Form;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class XSSRequestWrapper extends HttpServletRequestWrapper {
    static final Logger log = LogManager.getLogger(XSSRequestWrapper.class.getName());
    
    private boolean htmlFilter = true;
       public XSSRequestWrapper(HttpServletRequest servletRequest, boolean htmlFilter) {
           super(servletRequest);
           this.htmlFilter = htmlFilter;
       }

       @Override
       public String[] getParameterValues(String parameter) {
           String[] values = super.getParameterValues(parameter);

           if (values == null) {
               return null;
           }

           int count = values.length;
           String[] encodedValues = new String[count];
           for (int i = 0; i < count; i++) {
               encodedValues[i] = stripXSS(values[i]);
           }

           return encodedValues;
       }

       @Override
       public String getParameter(String parameter) {
           String value = super.getParameter(parameter);

           return stripXSS(value);
       }

       @Override
       public String getHeader(String name) {
           //System.out.println("\n<<<name>>>:"+name);
           String value = super.getHeader(name);
           return stripXSS(value);
       }
        
        
        
       private String stripXSS(String rawValue) {
           //System.out.println("\n<<<rawValue>>>:"+rawValue);
           String value = null;
           if (rawValue != null) {
               // NOTE: It's highly recommended to use the ESAPI library and uncomment the following line to
               // avoid encoded attacks.
               // value = ESAPI.encoder().canonicalize(value);
               
               //value = Normalizer.normalize(rawValue, Form.NFD);
               
               // Avoid null characters
               value = rawValue.replaceAll("\0", "");

               // Remove all sections that match a pattern
               value = UtilXSSSanatizer.replace(value, UtilXSSSanatizer.XSS_PATTERNS);
               if(htmlFilter){
                   value = UtilXSSSanatizer.replace(value, UtilXSSSanatizer.SRC_ATTRIBUTES_PATTERNS); 
                   value = UtilXSSSanatizer.HTMLEntityEncode(value);
               }
               log.trace("RAW VALUE:"+rawValue +", NORMALIZE VALUE:"+ Normalizer.normalize(rawValue, Form.NFD)+", final value:"+value);
           }
           return value;
       }
}
