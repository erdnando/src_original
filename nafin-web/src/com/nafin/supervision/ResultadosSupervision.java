package com.nafin.supervision;

import com.netro.pdf.ComunesPDF;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

/**
 * Clase para generar archivos para la pantalla Proceso de Supervisi�n
 */
public class ResultadosSupervision implements IQueryGeneratorRegExtJS {

	private static final Log LOGGER = ServiceLocator.getInstance().getLog(ResultadosSupervision.class);
	StringBuilder qrySentencia = new StringBuilder();
	private List conditions;
	private String folioOperacion;

	public ResultadosSupervision() {
	}

	@Override
	public String getDocumentQuery() {
		LOGGER.info("getDocumentQuery(E)");
		qrySentencia = new StringBuilder();
		conditions   = new ArrayList();
		LOGGER.debug("qrySentencia---------------> "+qrySentencia);
		LOGGER.debug("conditions----------------->"+conditions);
		LOGGER.info("getDocumentQuery(S)");
		return qrySentencia.toString(); 
	}

	@Override
	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	@Override
	public String getAggregateCalculationQuery() {
		return null;
	}

	@Override
	public List getConditions() {
		return Collections.emptyList();
	}

	@Override
	public String getDocumentQueryFile() {
		LOGGER.info("getDocumentQueryFile(E)");

		qrySentencia = new StringBuilder();
		conditions   = new ArrayList();

		qrySentencia.append("SELECT DISTINCT A.CC_ACUSE AS FOLIO_OPERACION, \n" + 
		"TO_CHAR(B.AC_FECHA_ACUSE,'DD/MM/YYYY') AS FECHA_CARGA, \n" + 
		"A.ARC_NO_ARCHIVOS AS NO_ARCHIVOS_CARGADOS \n" + 
		"FROM SUP_ARCHIVOS_CARGA_IF A, SUP_ACUSE B \n" + 
		"WHERE A.CC_ACUSE = B.CC_ACUSE \n" + 
		"AND A.CC_ACUSE = '" + folioOperacion + "'");

		LOGGER.info("qrySentencia:: " + qrySentencia);
		LOGGER.info("conditions:: " + conditions);
		LOGGER.info("getDocumentQueryFile(S)");

		return qrySentencia.toString();
	}

	@Override
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		LOGGER.debug("crearCustomFile (E)");

		String nombreArchivo = "";
		String relGarantias  = "";
		HttpSession session  = request.getSession();
		ComunesPDF pdfDoc    = new ComunesPDF();
		String usuario       = (String)session.getAttribute("strNombre") + " - " +(String)session.getAttribute("strNombreUsuario");

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			String[] meses     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual   = fechaActual.substring(0,2);
			String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual  = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Resumen de carga de documentos para la supervisi�n de NAFIN","formas",ComunesPDF.CENTER);
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

			pdfDoc.setLTable(5, 85);
			pdfDoc.setLCell("Folio de la operaci�n",         "celda02",ComunesPDF.CENTER);
			pdfDoc.setLCell("Fecha de carga",                "celda02",ComunesPDF.CENTER);
			pdfDoc.setLCell("Usuario",                       "celda02",ComunesPDF.CENTER);
			pdfDoc.setLCell("N�mero de archivos cargados",   "celda02",ComunesPDF.CENTER);
			pdfDoc.setLCell("Relaci�n de archivos cargados", "celda02",ComunesPDF.CENTER);

			//Obtengo la relaci�n de garant�as aparte porque solo necesito un registro
			relGarantias = obtieneRelacionDeGArantias();

			while (rs.next()) {
				pdfDoc.setLCell((rs.getString("FOLIO_OPERACION") == null) ? "" : rs.getString("FOLIO_OPERACION"), "formas", ComunesPDF.CENTER);
				pdfDoc.setLCell((rs.getString("FECHA_CARGA") == null) ? "" : rs.getString("FECHA_CARGA"), "formas", ComunesPDF.CENTER);
				pdfDoc.setLCell(usuario, "formas", ComunesPDF.CENTER);
				pdfDoc.setLCell((rs.getString("NO_ARCHIVOS_CARGADOS") == null) ? "" : rs.getString("NO_ARCHIVOS_CARGADOS"), "formas", ComunesPDF.CENTER);
				pdfDoc.setLCell(relGarantias.replace(",", "\n"), "formas", ComunesPDF.LEFT);
			}

			pdfDoc.addLTable();
			pdfDoc.endDocument();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			AccesoDB.cerrarResultSet(rs);
		}
		LOGGER.debug("crearCustomFile (S)");
		return nombreArchivo;
	}

	@Override
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}

	public String getFolioOperacion() {
		return folioOperacion;
	}

	public void setFolioOperacion(String folioOperacion) {
		this.folioOperacion = folioOperacion;
	}

	/**
	 * Obtiene la relaci�n de garant�as por folio, para poder concatenarlas en una sola celda del archivo
	 * @return variable con las relaciones de garant�as concatenadas
	 */
	private String obtieneRelacionDeGArantias(){
		StringBuilder relGarant = new StringBuilder();
		String        consulta  = "SELECT REL_GARANTIAS FROM SUP_ARCHIVOS_CARGA_IF WHERE CC_ACUSE = " + folioOperacion;
		AccesoDB      con       = new AccesoDB();
		ResultSet     rs        = null;
		int           cont      = 0;
		LOGGER.info("obtieneRelacionDeGArantias (E)");
		try{
			con.conexionDB();
			rs = con.queryDB(consulta);
			while(rs.next()) {
				if(cont>0){
					relGarant.append(",");
				}	
				relGarant.append((rs.getString("REL_GARANTIAS") == null)?"": (rs.getString("REL_GARANTIAS")));
				cont++;
			}
			rs.close();
			con.cierraStatement();
		} catch(Exception e) {
			LOGGER.error("obtieneRelacionDeGArantias (Exception)", e);
		} finally {
			con.cierraConexionDB();
			LOGGER.info("obtieneRelacionDeGArantias (S)");
		}
		return relGarant.toString();
	}
}
