package com.nafin.supervision;

import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

/**
 * Clase para la pantalla Bit�cora, del m�dulo de Supervisi�n.
 * Obtiene los datos para generar el grid y los archivos a descargar (PDF y XLS)
 */
public class BitacoraSupervision implements IQueryGeneratorRegExtJS {

	private static final Log LOGGER = ServiceLocator.getInstance().getLog(BitacoraSupervision.class);
	StringBuilder strQuery = new StringBuilder();
	private Integer interFin;
	private String ejecutivo;
	private Integer anio;
	private Integer mes;
	private List conditions;
        private Supervision supervision; 

	/**
	 * Constructor por default
	 */
	public BitacoraSupervision() {
	    this.supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);
	}

	public Integer getInterFin() {
		return interFin;
	}

	public void setInterFin(Integer interFin) {
		this.interFin = interFin;
	}

	public String getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	@Override
	public List getConditions() {
		return conditions;
	}

	/**
	 * Query para obtener las llaves primarias
	 * @return Cadena con la consulta de SQL para obtener llaves primarias
	 */
	@Override
	public String getDocumentQuery() {
		LOGGER.info("getDocumentQuery(E)");

		strQuery = new StringBuilder();
		conditions = new ArrayList();

		strQuery.append("SELECT A.BIT_ID \n" + 
		"FROM BIT_SUPERVISION A \n" + 
		"LEFT JOIN COMCAT_IF B ON A.IC_IF = B.IC_IF \n" + 
		"WHERE 1 = 1 \n");
		if(null != interFin && interFin > 0) {
			strQuery.append("AND A.IC_IF IN (SELECT IC_IF FROM SUP_DATOS_IF WHERE IC_CVE_SIAG = ?) \n");
			conditions.add(interFin);
		}
		if(!"".equals(ejecutivo)) {
			strQuery.append("AND A.BIT_EJECUTIVO_LOGIN = ? \n");
			conditions.add(ejecutivo);
		}
		if(null != anio && anio > 0) {
			strQuery.append("AND A.CAL_ANIO = ? \n");
			conditions.add(anio);
		}
		if(null != mes && mes > 0) {
			strQuery.append("AND A.CAL_MES = ?");
			conditions.add(mes);
		}

		LOGGER.debug("getDocumentQuery(strQuery): " + strQuery.toString());
		LOGGER.debug("getDocumentQuery(conditions): " + conditions);
		LOGGER.info("getDocumentQuery(S)");
		return strQuery.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @param ids
	 * @return Cadena con la consulta de SQL para obtener la informaci�n
	 * completa de los registros con las llaves especificadas
	 */
	@Override
	public String getDocumentSummaryQueryForIds(List ids) {
		LOGGER.info("getDocumentSummaryQueryForIds(E)");

		strQuery = new StringBuilder();
		conditions = new ArrayList();
		strQuery.append("SELECT " +
                "'666' as CONDA, " + 
                "A.BIT_ID, \n" + 
		"A.IC_IF, \n" + 
		"B.CG_RAZON_SOCIAL AS NOMBRE_IF, \n" + 
		"A.BIT_EJECUTIVO_NOMBRE, \n" + 
		"TO_CHAR(A.BIT_FEC_PUB_CAL,'DD/MM/YYYY') BIT_FEC_PUB_CAL, \n" + 
		
                "TO_CHAR(A.BIT_FEC_PUB_AVI,'DD/MM/YYYY') BIT_FEC_PUB_AVI, \n" + 
		"TO_CHAR(A.BIT_FEC_CON_AVI,'DD/MM/YYYY') BIT_FEC_CON_AVI, \n" + 
		"TO_CHAR(A.BIT_FEC_LIM_ENT_EXP,'DD/MM/YYYY') BIT_FEC_LIM_ENT_EXP, \n" + 
		"TO_CHAR(A.BIT_FEC_ENT_EXP,'DD/MM/YYYY') BIT_FEC_ENT_EXP, \n" + 
                "null as NGR, \n"+               
		
                "TO_CHAR(A.BIT_FEC_PUB_RES_SUP,'DD/MM/YYYY') BIT_FEC_PUB_RES_SUP, \n" + 
		"TO_CHAR(A.BIT_FEC_CON_RES_SUP,'DD/MM/YYYY') BIT_FEC_CON_RES_SUP, \n" + 
		"CASE WHEN A.BIT_FOLIO_CON_RES IS NULL THEN '' ELSE ''||A.BIT_FOLIO_CON_RES END AS BIT_FOLIO_CON_RES, \n" + 
		"TO_CHAR(A.BIT_FEC_LIM_ENT_INF_ACL,'DD/MM/YYYY') BIT_FEC_LIM_ENT_INF_ACL, \n" + 
                "TO_CHAR(A.BIT_FEC_ENT_INF_ACL,'DD/MM/YYYY') BIT_FEC_ENT_INF_ACL, \n" +                 
		
                "TO_CHAR(A.BIT_FEC_PUB_DIC,'DD/MM/YYYY') BIT_FEC_PUB_DIC, \n" + 
		"TO_CHAR(A.BIT_FEC_CON_RES_DIC,'DD/MM/YYYY') BIT_FEC_CON_RES_DIC, \n" + 
		"CASE WHEN A.BIT_FOLIO_CON_DIC IS NULL THEN '' ELSE ''||A.BIT_FOLIO_CON_DIC END AS BIT_FOLIO_CON_DIC, \n" + 
                "A.CAL_ANIO AS ANIO, \n"+                    
                "A.CAL_MES AS MES, \n"+
                "A.BIT_TIPO, \n"+
                                
                "B.IC_IF_SIAG AS IC_SIAG, \n"+   
                "'999' as CONDB " + 
                "FROM BIT_SUPERVISION A \n" + 
		"LEFT JOIN COMCAT_IF B ON A.IC_IF = B.IC_IF \n" +   
		"WHERE 1 = 1 \n");
		strQuery.append("AND (");
		for (int i = 0; i < ids.size(); i++) {
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				strQuery.append(" OR ");
			}
			strQuery.append("A.BIT_ID = ?");
			conditions.add(new Long(lItem.get(0).toString()));
		}
		strQuery.append(") \n");
		strQuery.append("ORDER BY A.IC_IF, B.CG_RAZON_SOCIAL");

		LOGGER.debug("getDocumentSummaryQueryForIds(strQuery): " + strQuery.toString());
		LOGGER.debug("getDocumentSummaryQueryForIds(conditions): " + conditions);
		LOGGER.info("getDocumentSummaryQueryForIds(S)");
		return strQuery.toString();
	}

	@Override
	public String getAggregateCalculationQuery() {
		LOGGER.info("getAggregateCalculationQuery(E)");
		LOGGER.info("getAggregateCalculationQuery(S)");
		return "";
	}

	/**
	 * Obtiene la consulta para generar el archivo sin paginaci�n
	 * @return
	 */
	@Override
	public String getDocumentQueryFile() {
		LOGGER.info("getDocumentQueryFile(E)");

		strQuery = new StringBuilder();
		conditions = new ArrayList();

		strQuery.append("SELECT " +
                "A.BIT_ID, \n" + 
		"A.IC_IF, \n" + 
		"B.CG_RAZON_SOCIAL AS NOMBRE_IF, \n" + 
		"A.BIT_EJECUTIVO_NOMBRE, \n" + 		
                "TO_CHAR(A.BIT_FEC_PUB_CAL,'DD/MM/YYYY') BIT_FEC_PUB_CAL, \n" + 
                                
		"TO_CHAR(A.BIT_FEC_PUB_AVI,'DD/MM/YYYY') BIT_FEC_PUB_AVI, \n" + 
		"TO_CHAR(A.BIT_FEC_CON_AVI,'DD/MM/YYYY') BIT_FEC_CON_AVI, \n" + 
		"TO_CHAR(A.BIT_FEC_LIM_ENT_EXP,'DD/MM/YYYY') BIT_FEC_LIM_ENT_EXP, \n" + 
		"TO_CHAR(A.BIT_FEC_ENT_EXP,'DD/MM/YYYY') BIT_FEC_ENT_EXP, \n" + 
                " null as NGR, \n"+ 
		
                "TO_CHAR(A.BIT_FEC_PUB_RES_SUP,'DD/MM/YYYY') BIT_FEC_PUB_RES_SUP, \n" + 
		"TO_CHAR(A.BIT_FEC_CON_RES_SUP,'DD/MM/YYYY') BIT_FEC_CON_RES_SUP, \n" + 
		"A.BIT_FOLIO_CON_RES, \n" + 
		"TO_CHAR(A.BIT_FEC_LIM_ENT_INF_ACL,'DD/MM/YYYY') BIT_FEC_LIM_ENT_INF_ACL, \n" + 
                "TO_CHAR(A.BIT_FEC_ENT_INF_ACL,'DD/MM/YYYY') BIT_FEC_ENT_INF_ACL, \n" +                 
		
                "TO_CHAR(A.BIT_FEC_PUB_DIC,'DD/MM/YYYY') BIT_FEC_PUB_DIC, \n" + 
		"TO_CHAR(A.BIT_FEC_CON_RES_DIC,'DD/MM/YYYY') BIT_FEC_CON_RES_DIC, \n" + 
		"A.BIT_FOLIO_CON_DIC, \n" + 
                "A.CAL_ANIO AS ANIO, \n"+                    
                "A.CAL_MES AS MES, \n"+
                "A.BIT_TIPO, \n"+
	    
                "B.IC_IF_SIAG AS IC_SIAG \n"+   
		
                "FROM BIT_SUPERVISION A \n" + 
		"LEFT JOIN COMCAT_IF B ON A.IC_IF = B.IC_IF \n" + 
		"WHERE 1 = 1 \n");
		if(null != interFin && interFin > 0) {
			strQuery.append("AND A.IC_IF IN (SELECT IC_IF FROM SUP_DATOS_IF WHERE IC_CVE_SIAG = ?) \n");
			conditions.add(interFin);
		}
		if(!"".equals(ejecutivo)) {
			strQuery.append("AND A.BIT_EJECUTIVO_LOGIN = ? \n");
			conditions.add(ejecutivo);
		}
		if(null != anio && anio > 0) {
			strQuery.append("AND A.CAL_ANIO = ? \n");
			conditions.add(anio);
		}
		if(null != mes && mes > 0) {
			strQuery.append("AND A.CAL_MES = ? \n");
			conditions.add(mes);
		}
		strQuery.append("ORDER BY A.IC_IF, B.CG_RAZON_SOCIAL");

		LOGGER.debug("getDocumentQueryFile(strQuery): " + strQuery.toString());
		LOGGER.debug("getDocumentQueryFile(conditions): " + conditions);
		LOGGER.info("getDocumentQueryFile(S)");
		return strQuery.toString();
	}

	/**
	 * M�todo para generar los archivos PDF y XLS
	 * @param request
	 * @param rs
	 * @param path ruta donde se genera el archivo
	 * @param tipo PDF o XLS
	 * @return nombre del archivo
	 */
	@Override
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {
		LOGGER.info("crearCustomFile(E)");
		String nombreArchivo = "";
                

		try {                   
			if("PDF".equals(tipo)) {

				ComunesPDF pdfDoc = new ComunesPDF();
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String[] meses     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("Ciudad de M�xico a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" Bit�cora ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				pdfDoc.setTable(17, 100);
				pdfDoc.setCell("Clave del IF",                                        "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre del IF",                                       "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre del ejecutivo",                                "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Publicaci�n \n del Calendario",              "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Notificaci�n \n de Avisos de Supervisi�n",   "celda01",ComunesPDF.CENTER);
				
                                pdfDoc.setCell("Fecha de Consulta IF \n de Avisos de Supervisi�n",    "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha L�mite de Entrega \n de Expediente",            "celda01",ComunesPDF.CENTER);				
                                pdfDoc.setCell("Fecha de Entrega \n de Expediente",                   "celda01",ComunesPDF.CENTER);
                                pdfDoc.setCell("N�m de garant�as \n recibidas",                       "celda01",ComunesPDF.CENTER);				
                                pdfDoc.setCell("Fecha de Publicaci�n de \n Resultado de Supervisi�n", "celda01",ComunesPDF.CENTER);
                                
				pdfDoc.setCell("Fecha de Consulta IF de \n Resultado de Supervisi�n", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Folio Consulta Resultado \n de Supervisi�n",          "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha L�mite de Entrega \n de Informaci�n Aclaraci�n","celda01",ComunesPDF.CENTER);                                
                                pdfDoc.setCell("Fecha de expediente de \n aclaraciones",              "celda01",ComunesPDF.CENTER);
                                pdfDoc.setCell("Fecha de Publicaci�n \n de Dictamen",                 "celda01",ComunesPDF.CENTER);
                                
				pdfDoc.setCell("Fecha de Consulta IF \n del Dictamen Supervisi�n",    "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Folio Consulta Dictamen \n de Supervisi�n",           "celda01",ComunesPDF.CENTER);
                                                                

				while (rs.next()) {
					pdfDoc.setCell((rs.getString("IC_IF")                   ==null) ?" ":rs.getString("IC_IF"),                  "formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("NOMBRE_IF")               ==null) ?" ":rs.getString("NOMBRE_IF"),              "formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("BIT_EJECUTIVO_NOMBRE")    ==null) ?" ":rs.getString("BIT_EJECUTIVO_NOMBRE"),   "formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("BIT_FEC_PUB_CAL")         ==null) ?" ":rs.getString("BIT_FEC_PUB_CAL"),        "formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("BIT_FEC_PUB_AVI")         ==null) ?" ":rs.getString("BIT_FEC_PUB_AVI"),        "formas",ComunesPDF.CENTER);
					
                                        pdfDoc.setCell((rs.getString("BIT_FEC_CON_AVI")         ==null) ?" ":rs.getString("BIT_FEC_CON_AVI"),        "formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("BIT_FEC_LIM_ENT_EXP")     ==null) ?" ":rs.getString("BIT_FEC_LIM_ENT_EXP"),    "formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("BIT_FEC_ENT_EXP")         ==null) ?" ":rs.getString("BIT_FEC_ENT_EXP"),        "formas",ComunesPDF.CENTER);
                                                                          
                                        //String numgt =  supervision.obtenerNumGarantias (mes, anio, claveSiag, tipo2);
                                        String numG = supervision.obtenerNumGarantias(rs.getString("MES"), rs.getString("ANIO"), rs.getString("IC_SIAG"), rs.getString("BIT_TIPO"));                                        
                                        
                                        pdfDoc.setCell(numG,"formas", ComunesPDF.CENTER);
                                        pdfDoc.setCell((rs.getString("BIT_FEC_PUB_RES_SUP")     ==null) ?" ":rs.getString("BIT_FEC_PUB_RES_SUP"),    "formas",ComunesPDF.CENTER);
					
                                        
                                        pdfDoc.setCell((rs.getString("BIT_FEC_CON_RES_SUP")     ==null) ?" ":rs.getString("BIT_FEC_CON_RES_SUP"),    "formas",ComunesPDF.CENTER);					
                                        pdfDoc.setCell((rs.getString("BIT_FOLIO_CON_RES")       ==null) ?" ":rs.getString("BIT_FOLIO_CON_RES"),      "formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("BIT_FEC_LIM_ENT_INF_ACL") ==null) ?" ":rs.getString("BIT_FEC_LIM_ENT_INF_ACL"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("BIT_FEC_ENT_INF_ACL")     ==null) ?" ":rs.getString("BIT_FEC_ENT_INF_ACL"),        "formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("BIT_FEC_PUB_DIC")         ==null) ?" ":rs.getString("BIT_FEC_PUB_DIC"),    "formas",ComunesPDF.CENTER);
                    
                                        pdfDoc.setCell((rs.getString("BIT_FEC_CON_RES_DIC")     == null)?" ":rs.getString("BIT_FEC_CON_RES_DIC"),    "formas",ComunesPDF.CENTER);					
                                        pdfDoc.setCell((rs.getString("BIT_FOLIO_CON_DIC")       ==null) ?" ":rs.getString("BIT_FOLIO_CON_DIC"),      "formas",ComunesPDF.CENTER);
//                                        
                                        
                                    
				    
				}

				pdfDoc.addTable();
				pdfDoc.endDocument();

			} else if("XLS".equals(tipo)) {

				CreaArchivo archivo = new CreaArchivo();
				nombreArchivo = archivo.nombreArchivo()+".xls";
				ComunesXLS xlsDoc = new ComunesXLS(path + nombreArchivo, "Bitacora");

				// Encabezado de la tabla
				xlsDoc.setTabla(17);
				xlsDoc.setCelda("Clave del IF",                                     "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Nombre del IF",                                    "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Nombre del ejecutivo",                             "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Fecha de Publicaci�n del Calendario",              "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Fecha de Notificaci�n de Avisos de Supervisi�n",   "formasb", ComunesXLS.CENTER);
				
                                xlsDoc.setCelda("Fecha de Consulta IF de Avisos de Supervisi�n",    "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Fecha L�mite de Entrega de Expediente",            "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Fecha de Entrega de Expediente",                   "formasb", ComunesXLS.CENTER);
                                xlsDoc.setCelda("N�m de garant�as recibidas",                       "formasb", ComunesXLS.CENTER);
                                xlsDoc.setCelda("Fecha de Publicaci�n de Resultado de Supervisi�n", "formasb", ComunesXLS.CENTER);
				
                                xlsDoc.setCelda("Fecha de Consulta IF de Resultado de Supervisi�n", "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Folio Consulta Resultado de Supervisi�n",          "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Fecha L�mite de Entrega de Informaci�n Aclaraci�n","formasb", ComunesXLS.CENTER);
                                xlsDoc.setCelda("Fecha de expediente de aclaraciones",              "formasb", ComunesXLS.CENTER);
                                xlsDoc.setCelda("Fecha de Publicaci�n de Dictamen",                 "formasb", ComunesXLS.CENTER);
				
                                xlsDoc.setCelda("Fecha de Consulta IF del Dictamen Supervisi�n",    "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Folio Consulta Dictamen de Supervisi�n",           "formasb", ComunesXLS.CENTER);

				//Contenido de la tabla
				while (rs.next()) {
					xlsDoc.setCelda((rs.getString("IC_IF")                   ==null) ?" ":rs.getString("IC_IF")                  );
					xlsDoc.setCelda((rs.getString("NOMBRE_IF")               ==null) ?" ":rs.getString("NOMBRE_IF")              );
					xlsDoc.setCelda((rs.getString("BIT_EJECUTIVO_NOMBRE")    ==null) ?" ":rs.getString("BIT_EJECUTIVO_NOMBRE")   );
					xlsDoc.setCelda((rs.getString("BIT_FEC_PUB_CAL")         ==null) ?" ":rs.getString("BIT_FEC_PUB_CAL")        );
					xlsDoc.setCelda((rs.getString("BIT_FEC_PUB_AVI")         ==null) ?" ":rs.getString("BIT_FEC_PUB_AVI")        );
					
                                        xlsDoc.setCelda((rs.getString("BIT_FEC_CON_AVI")         ==null) ?" ":rs.getString("BIT_FEC_CON_AVI")        );
					xlsDoc.setCelda((rs.getString("BIT_FEC_LIM_ENT_EXP")     ==null) ?" ":rs.getString("BIT_FEC_LIM_ENT_EXP")    );
					xlsDoc.setCelda((rs.getString("BIT_FEC_ENT_EXP")         ==null) ?" ":rs.getString("BIT_FEC_ENT_EXP")        );
                                        //xlsDoc.setCelda((rs.getString("NGR")                     ==null) ?" ":rs.getString("NGR")                    );
                                        String numG = supervision.obtenerNumGarantias(rs.getString("MES"), rs.getString("ANIO"), rs.getString("IC_SIAG"), rs.getString("BIT_TIPO"));                                        
                                        xlsDoc.setCelda(numG);
                                        
                                        xlsDoc.setCelda((rs.getString("BIT_FEC_PUB_RES_SUP")     ==null) ?" ":rs.getString("BIT_FEC_PUB_RES_SUP")    );
					
                                        xlsDoc.setCelda((rs.getString("BIT_FEC_CON_RES_SUP")     ==null) ?" ":rs.getString("BIT_FEC_CON_RES_SUP")    );					
                                        xlsDoc.setCelda((rs.getString("BIT_FOLIO_CON_RES")       ==null) ?" ":rs.getString("BIT_FOLIO_CON_RES")      );
					xlsDoc.setCelda((rs.getString("BIT_FEC_LIM_ENT_INF_ACL") ==null) ?" ":rs.getString("BIT_FEC_LIM_ENT_INF_ACL"));
					xlsDoc.setCelda((rs.getString("BIT_FEC_ENT_INF_ACL")         ==null) ?" ":rs.getString("BIT_FEC_ENT_INF_ACL")        );
					xlsDoc.setCelda((rs.getString("BIT_FEC_PUB_DIC")         ==null) ? " ":rs.getString("BIT_FEC_PUB_DIC")        );
                                        
                                        xlsDoc.setCelda((rs.getString("BIT_FEC_CON_RES_DIC")     ==null) ?" ":rs.getString("BIT_FEC_CON_RES_DIC")    );					
                                        xlsDoc.setCelda((rs.getString("BIT_FOLIO_CON_DIC")       ==null) ?" ":rs.getString("BIT_FOLIO_CON_DIC")      );
				}

				xlsDoc.cierraTabla();
				xlsDoc.cierraXLS();

			}

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		}
		LOGGER.info("crearCustomFile(S)");
		return nombreArchivo; 
	}

	@Override
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		LOGGER.info("crearPageCustomFile(E)");
		LOGGER.info("crearPageCustomFile(S)");
		return null;
	}

}