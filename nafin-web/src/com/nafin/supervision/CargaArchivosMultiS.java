package com.nafin.supervision;



import com.nafin.nafinetmovil.utils.ServiceLocator;

import java.io.IOException;
import java.io.PrintWriter;



import javax.servlet.*;

import javax.servlet.http.*;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.nafin.supervision.*;

//import org.json.JSONArray;
//import org.json.JSONObject;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import java.util.HashMap;

import net.sf.json.*;

public class CargaArchivosMultiS extends HttpServlet {
    private static final String DOCUMENTS_FOLDER  = "documentsUpload";
       //private Logger logger = LoggerFactory.getLogger(CargaArchivosMultiS.class);
       
       public void init(ServletConfig config) throws ServletException
       {
         super.init(config);
       }
       
       /**
           * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
           * 
           */
       /*@Override
       protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

           
           if (request.getParameter("getfile") != null && !request.getParameter("getfile").isEmpty()) {
               File file = new File(request.getServletContext().getRealPath("/")+"imgs/"+request.getParameter("getfile"));
               if (file.exists()) {
                   int bytes = 0;
                   ServletOutputStream op = response.getOutputStream();

                   response.setContentType(getMimeType(file));
                   response.setContentLength((int) file.length());
                   response.setHeader( "Content-Disposition", "inline; filename=\"" + file.getName() + "\"" );

                   byte[] bbuf = new byte[1024];
                   DataInputStream in = new DataInputStream(new FileInputStream(file));

                   while ((in != null) && ((bytes = in.read(bbuf)) != -1)) {
                       op.write(bbuf, 0, bytes);
                   }

                   in.close();
                   op.flush();
                   op.close();
               }
           } else if (request.getParameter("delfile") != null && !request.getParameter("delfile").isEmpty()) {
               File file = new File(request.getServletContext().getRealPath("/")+"imgs/"+ request.getParameter("delfile"));
               if (file.exists()) {
                   file.delete(); // TODO:check and report success
               } 
           }
           else if (request.getParameter("getthumb") != null && !request.getParameter("getthumb").isEmpty()) {
               File file = new File(request.getServletContext().getRealPath("/")+"imgs/"+request.getParameter("getthumb"));
                   if (file.exists()) {
                       logger.info(file.getAbsolutePath());
                       String mimetype = getMimeType(file);
                       if (mimetype.endsWith("png") || mimetype.endsWith("jpeg")|| mimetype.endsWith("jpg") || mimetype.endsWith("gif")) {
                           BufferedImage im = ImageIO.read(file);
                           if (im != null) {
                               BufferedImage thumb = Scalr.resize(im, 75); 
                               ByteArrayOutputStream os = new ByteArrayOutputStream();
                               if (mimetype.endsWith("png")) {
                                   ImageIO.write(thumb, "PNG" , os);
                                   response.setContentType("image/png");
                               } else if (mimetype.endsWith("jpeg")) {
                                   ImageIO.write(thumb, "jpg" , os);
                                   response.setContentType("image/jpeg");
                               } else if (mimetype.endsWith("jpg")) {
                                   ImageIO.write(thumb, "jpg" , os);
                                   response.setContentType("image/jpeg");
                               } else {
                                   ImageIO.write(thumb, "GIF" , os);
                                   response.setContentType("image/gif");
                               }
                               ServletOutputStream srvos = response.getOutputStream();
                               response.setContentLength(os.size());
                               response.setHeader( "Content-Disposition", "inline; filename=\"" + file.getName() + "\"" );
                               os.writeTo(srvos);
                               srvos.flush();
                               srvos.close();
                           }
                       }
               } // TODO: check and report success
           }
           else {
               PrintWriter writer = response.getWriter();
               writer.write("call POST with multipart form data");
           }
       }*/
       
       /**
           * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
           * 
           */
       @SuppressWarnings("unchecked")
       @Override
       protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

           if (!ServletFileUpload.isMultipartContent(request)) {
               throw new IllegalArgumentException("Request is not multipart, please 'multipart/form-data' enctype for your form.");
           }

           ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
           PrintWriter writer = response.getWriter();
           response.setContentType("text/plain");
           JSONObject json = new JSONObject();
           JSONArray jsonFiles = new JSONArray();
           String message ="";
           String error  = "";
           boolean success = true; 
           String userName = (String)request.getSession().getAttribute("iNoUsuario");
           String ic_if    = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
           System.out.println("usuario "+ userName);
           System.out.println("Inter financiero "+ ic_if);
           String mes = "";
           String anio = "";
           String tipoInformacion = "";
           String noArchivos = "";
           String primerA = "0";
           String acuerdo = "";
               
           List<File> archivos = new ArrayList<File>();
           JSONObject dummy = null;
           int archivosCargados = 0;
           try {
               //Cargar file system del servidor
               request.setCharacterEncoding("UTF-8");
               List<FileItem> items = uploadHandler.parseRequest(request);
               String filePath = null;
               String filePathB = null;
               Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);
               //UtilWeb util = new UtilWeb(request);
               //String  = getServletConfig().getServletContext().getRealPath("/");
               String rootPath = getServletConfig().getServletContext().getRealPath("/");
          
               for (FileItem item : items) {
                   if (!item.isFormField()) {
                       if(primerA.equals("0")){
                           ArchivosCargaValue archivosCargaA = new ArchivosCargaValue();
                           archivosCargaA.setUsuario(userName);
                           archivosCargaA.setInterFinan(ic_if);
                           List archivosB =  supervision.obtenArchivosUsuario(archivosCargaA);                           
                           for(int i = 0; i<archivosB.size();i++){
                               filePathB = rootPath+File.separator+CargaArchivosMultiS.DOCUMENTS_FOLDER+File.separator+(String)archivosB.get(i);
                               System.out.println(filePathB);//filePathB += (String)archivosB.get(i);
                               File fileB = new File(filePathB);
                               this.borrarArchivo(fileB);                             
                           }
                         
                       }
                           
                           
                           filePath = rootPath+File.separator+CargaArchivosMultiS.DOCUMENTS_FOLDER+File.separator+item.getName();
                           File file = new File(filePath);
                           
                           file.setReadable(true);
                           
                           if(file.exists()){
                               file.delete();
                           }
                               
                           item.write(file);
                           JSONObject jsono = new JSONObject();
                           jsono.put("name", item.getName());
                           jsono.put("size", item.getSize());
                           jsonFiles.add(jsono);   
                           archivos.add(file);                            
                   }else{
                       String fieldName = item.getFieldName();
                       String fieldValue = item.getString();
                       if(fieldName.equals("mes")){
                           mes = fieldValue;                          
                       } else if(fieldName.equals("anio")){
                           anio = fieldValue;                          
                       } else if(fieldName.equals("noArchivos")){
                           noArchivos = fieldValue;                          
                       } else if(fieldName.equals("tipoInformacion")){
                           tipoInformacion = fieldValue;                          
                       }  else if(fieldName.equals("primerA")){
                           //System.out.println("ya entro "+fieldValue);
                           primerA = fieldValue; 
                       } else if(fieldName.equals("noAcuerdo")){
                           acuerdo = fieldValue; 
                       }
                   }
               }
               
               int existeUsuario = supervision.validaUsuario(userName);
               
               if(existeUsuario > 0) {
                   
                   //Guardando en la base de datos.
                   HashMap<String,Object> regresoVal = null;
                   if(archivos.size()>0) {
                       ArchivosCargaValue archivosCarga = new ArchivosCargaValue();
                       archivosCarga.setMes(mes);
                       archivosCarga.setAnio(anio);
                       archivosCarga.setNoArchivos(noArchivos);
                       archivosCarga.setTipoCarga(tipoInformacion);
                       archivosCarga.setUsuario(userName);
                       archivosCarga.setInterFinan(ic_if);
                       archivosCarga.setAcuerdo(acuerdo);
                       
                       if(supervision.validaExtemporaneo(archivosCarga)) {
                           //PersistExpediente bean = new PersistExpediente();
                           if(primerA.equals("0"))
                                supervision.deleteArchivosTemps(archivosCarga);
                           for(int i=0;i<archivos.size();i++) {
                               File f = archivos.get(i);
                               try {
                                   //expediente = bean.save(expediente,f);
                                   
                                   regresoVal = supervision.insertaArchivosTemps(archivosCarga, f);
                                   
                                   if(!regresoVal.get("mensaje").toString().equals("OK")){
                                       success = false;
                                       switch((Integer)regresoVal.get("validaRegreso")){
                                       case 1:  message="La garantia no existe dentro del calendario.";
                                                break;
                                       case 2:  message="La fecha de carga de información excede el límite de fecha programada o el calendario no fue cargado correctamente";
                                                break;
                                       case 3:  message="El archivo ya se cargo previamente";
                                                break;
                                       case 4:  message="El archivo ZIP no contiene archivos PDF";
                                                break;
                                       case 5:  message="El archivo ZIP contiene demasiados archivos, intente con un archivo zip de entre " + regresoVal.get("margen") + " y " + regresoVal.get("maximo") + " archivos";
                                                break;
                                       default: message="Error interno del sistema vuelva a reprocesar";
                                                break;
                                       }
                                       this.borrarArchivo(f); 
                                   } else {
                                            success = true; 
                                            message="Carga exitosa.";
                                   }                                 
                               } catch(Exception e) {
                                   success = false;
                                   message = e.getMessage();
                               }
                               dummy = (JSONObject)jsonFiles.get(i);
                               dummy.put("message",message); 
                               dummy.put("success",success);
                           }//for(int i=0;i<archivos.size();i++)                           
                       } else {
                           message="El Numero de Acuerdo capturado no es valido.";
                           success = false;
                           dummy = (JSONObject)jsonFiles.get(0);
                           dummy.put("message",message); 
                           dummy.put("success",success);
                       }//if(supervision.validaExtemporaneo(userName))                       
                   } else {
                       message="No se encontraron archivos a cargar.";
                       success = false;
                   }
               } else {
                    success = false;
                    message="El usuario no tiene permisos para cargar informacion.";
                    error = "El usuario no tiene permisos para cargar informacion.";
                    dummy = (JSONObject)jsonFiles.get(0);
                    dummy.put("message",message); 
                    dummy.put("success",success);
               }
               
           } catch (Exception e) {
               e.printStackTrace();
               success = false;
               error=e.getMessage();
               try{
                   for(int i=0; i<jsonFiles.size();i++){
                       dummy = (JSONObject)jsonFiles.get(i);
                       dummy.put("error","-------");
                   }
               }catch(Exception ignore){}
               
           } finally {
               json.put("success",success);
               json.put("error",error);
               json.put("files",jsonFiles);
               
               writer.write(json.toString());
               writer.close();
           }

       }

       private String getMimeType(File file) {
           String mimetype = "";
           if (file.exists()) {
               if (getSuffix(file.getName()).equalsIgnoreCase("png")) {
                   mimetype = "image/png";
               }else if(getSuffix(file.getName()).equalsIgnoreCase("jpg")){
                   mimetype = "image/jpg";
               }else if(getSuffix(file.getName()).equalsIgnoreCase("jpeg")){
                   mimetype = "image/jpeg";
               }else if(getSuffix(file.getName()).equalsIgnoreCase("gif")){
                   mimetype = "image/gif";
               }else {
                   javax.activation.MimetypesFileTypeMap mtMap = new javax.activation.MimetypesFileTypeMap();
                   mimetype  = mtMap.getContentType(file);
               }
           }
           return mimetype;
       }



       private String getSuffix(String filename) {
           String suffix = "";
           int pos = filename.lastIndexOf('.');
           if (pos > 0 && pos < filename.length() - 1) {
               suffix = filename.substring(pos + 1);
           }
           return suffix;
       }
       
    private String getFileName(final Part part) {
        try{
            final String partHeader = part.getHeader("content-disposition");
            for (String content : part.getHeader("content-disposition").split(";")) {
                if (content.trim().startsWith("filename")) {
                    return content.substring(
                            content.indexOf('=') + 1).trim().replace("\"", "");
                }
            }
        
        }catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    private boolean borrarArchivo(File fileB){
        boolean borro = true;
        if(fileB.exists()){
            try {
                fileB.setWritable(true);
                if(fileB.delete())
                     System.out.println("se borro archivo");
                else{
                    borro = false;
                     System.out.println("no se borro archivo");
                }     
            } catch (SecurityException x) {
                x.printStackTrace();
                borro = false;
                //logger.info("No se pudo eliminar el archivo "+f.getName());
            } catch (Exception e) {
                e.printStackTrace();
                borro = false;
                //logger.info("No se pudo eliminar el archivo "+f.getName());
            } 
        }
     return borro;
    }
    
}
