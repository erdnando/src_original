package com.nafin.supervision;

import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Clase para la pantalla Bit�cora, del m�dulo de Supervisi�n.
 * Obtiene los datos para generar el grid y los archivos a descargar (PDF y XLS)
 */
/**
 * Clase para la pantalla Monitor, del m�dulo de Supervisi�n.
 * Obtiene los datos para generar el grid y los archivos a descargar (PDF y XLS)
 */
public class BitacoraSupervisionM implements IQueryGeneratorRegExtJS {
        
        private Supervision supervision; 
	private static final Log LOGGER = ServiceLocator.getInstance().getLog(BitacoraSupervisionM.class);
	StringBuilder strQuery = new StringBuilder();
	private Integer claveSIAG;
	private Integer interFin;
	private String ejecutivo;
	private Integer anio;
	private Integer mes;
	private String tipoDocumento;
	private List conditions;
        
    /**
     * Constructor por default
     */
    public BitacoraSupervisionM() {
        this.supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }
   
    

	

    public void setClaveSIAG(Integer claveSIAG) {
        this.claveSIAG = claveSIAG;
    }

    public Integer getClaveSIAG() {
        return claveSIAG;
    }

    public Integer getInterFin() {
		return interFin;
	}

	public void setInterFin(Integer interFin) {
		this.interFin = interFin;
	}

	public String getEjecutivo() {
		return ejecutivo;
	}

	public void setEjecutivo(String ejecutivo) {
		this.ejecutivo = ejecutivo;
	}

	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	@Override
	public List getConditions() {
		return conditions;
	}

	/**
	 * Query para obtener las llaves primarias
	 * @return Cadena con la consulta de SQL para obtener llaves primarias
	 */
	@Override
	public String getDocumentQuery() {
		LOGGER.info("getDocumentQuery(E)");                

		strQuery = new StringBuilder();
		conditions = new ArrayList();

		strQuery.append("SELECT        \n" + 
		"                   A.BIT_ID \n" + 		
		"                    FROM BIT_SUPERVISION A, COMCAT_IF B, sup_calendario_if SCI \n" + 
		"                        WHERE A.IC_IF = B.IC_IF \n" + 
		"                            AND b.ic_if_siag = SCI.ic_cve_siag \n" +
                "                           AND A.cal_mes = SCI.cal_mes \n" + 
                "		                            AND A.cal_anio = SCI.cal_anio \n" + 
                "		                            AND A.bit_tipo = SCI.cal_tipo \n" +
					"		                            AND SCI.cal_fec_lim_ent_r is not null \n"  );
		
                if(claveSIAG != null && claveSIAG > 0 ) {
	            strQuery.append(" AND b.ic_if_siag = ? \n");
	            conditions.add(claveSIAG);
                }
		if(null != interFin && interFin > 0) {
			strQuery.append(" AND A.IC_IF IN (SELECT IC_IF FROM SUP_DATOS_IF WHERE IC_CVE_SIAG = ?) \n");
			conditions.add(interFin);
		}if(null != anio && anio > 0) {
			strQuery.append(" AND A.CAL_ANIO = ? \n");
			conditions.add(anio);
		}if(null != mes && mes > 0) {
			strQuery.append(" AND A.CAL_MES = ?");
			conditions.add(mes);
		}                
		if(!"".equals(ejecutivo)) {
			strQuery.append(" AND A.BIT_EJECUTIVO_LOGIN = ? \n");
			conditions.add(ejecutivo);
		}
                if(tipoDocumento != null && !tipoDocumento.isEmpty()){
                    if("R".equals(tipoDocumento)){
                        strQuery.append(" AND SCI.cal_doc_solicitud IS NOT NULL" );
                    }else if("S".equals(tipoDocumento)){
                        strQuery.append(" AND SCI.cal_doc_dictamen = IS NOT NULL");
                        }
                    
                }		

		LOGGER.debug("getDocumentQuery(strQuery): " + strQuery.toString());
		LOGGER.debug("getDocumentQuery(conditions): " + conditions);
		LOGGER.info("getDocumentQuery(S)");
                
		return strQuery.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @param ids
	 * @return Cadena con la consulta de SQL para obtener la informaci�n
	 * completa de los registros con las llaves especificadas
	 */
	@Override
	public String getDocumentSummaryQueryForIds(List ids) {
		LOGGER.info("getDocumentSummaryQueryForIds(E)");

		strQuery = new StringBuilder();
		conditions = new ArrayList();
		strQuery.append("SELECT        \n" + 
		"        A.BIT_ID, \n" + 
		"		A.IC_IF,\n" + 
		"		B.CG_RAZON_SOCIAL AS NOMBRE_IF,\n" + 
		"		A.BIT_EJECUTIVO_NOMBRE,		\n" + 
		"        '' AS SEMAFORO_RES, '' AS SEMAFORO_DIC, null as NGR, \n" + 
		"        TO_CHAR(A.BIT_FEC_PUB_RES_SUP,'DD/MM/YYYY') BIT_FEC_PUB_RES_SUP,\n" + 
		"        TO_CHAR(A.BIT_FEC_PUB_DIC,'DD/MM/YYYY') BIT_FEC_PUB_DIC,	\n" + 
		"        A.CAL_ANIO AS ANIO,\n" + 
		"        A.CAL_MES AS MES,\n" + 
		"        concat(A.CAL_MES, concat(' - ', A.CAL_ANIO)) as MESANIOG,              \n" + 
		"        DECODE(A.BIT_TIPO, 'O', 'Ordinario', 'E', 'Extempor�neo') AS bit,                 \n" + 
		"        A.BIT_TIPO AS BIT_TIPO,\n" + 
		"        B.IC_IF_SIAG AS IC_SIAG,\n" + 
		"        TO_CHAR(sci.cal_fec_lim_ent_r,'DD/MM/YYYY') cal_fec_lim_ent_r,\n" + 
		"        TO_CHAR(sci.cal_fec_lim_ent_d,'DD/MM/YYYY') cal_fec_lim_ent_d, \n" + 
		"        TO_CHAR(SCI.CAL_FECHA_INSER,'DD/MM/YYYY') CAL_FECHA_INSER, null as tipoCal \n" + 
		"                    FROM BIT_SUPERVISION A, COMCAT_IF B, sup_calendario_if SCI \n" + 
		"                        WHERE A.IC_IF = B.IC_IF \n" + 
		"                            AND b.ic_if_siag = SCI.ic_cve_siag \n " +
                "                  AND A.cal_mes = SCI.cal_mes \n" + 
                "		              AND A.cal_anio = SCI.cal_anio \n" + 
                "		              AND A.bit_tipo = SCI.cal_tipo \n"+
					 "		              AND SCI.cal_fec_lim_ent_r is not null \n");
		
		
		
		strQuery.append("AND (");
		for (int i = 0; i < ids.size(); i++) {
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				strQuery.append(" OR ");
			}
			strQuery.append("A.BIT_ID = ?");
			conditions.add(new Long(lItem.get(0).toString()));
		}
		strQuery.append(") \n");

		LOGGER.debug("getDocumentSummaryQueryForIds(strQuery): " + strQuery.toString());
		LOGGER.debug("getDocumentSummaryQueryForIds(conditions): " + conditions);
		LOGGER.info("getDocumentSummaryQueryForIds(S)");
		return strQuery.toString();
	}

	@Override
	public String getAggregateCalculationQuery() {
		LOGGER.info("getAggregateCalculationQuery(E)");
		LOGGER.info("getAggregateCalculationQuery(S)");
		return "";
	}

	/**
	 * Obtiene la consulta para generar el archivo sin paginaci�n
	 * @return
	 */
	@Override
	public String getDocumentQueryFile() {
		LOGGER.info("getDocumentQueryFile(E)");

		strQuery = new StringBuilder();
		conditions = new ArrayList();

		strQuery.append("SELECT        \n" + 
		"        A.BIT_ID, \n" + 
		"		A.IC_IF,\n" + 
		"		B.CG_RAZON_SOCIAL AS NOMBRE_IF,\n" + 
		"		A.BIT_EJECUTIVO_NOMBRE,		\n" + 
		"        '' AS SEMAFORO_RES, '' AS SEMAFORO_DIC, null as NGR, \n" + 
		"        TO_CHAR(A.BIT_FEC_PUB_RES_SUP,'DD/MM/YYYY') BIT_FEC_PUB_RES_SUP,\n" + 
		"        TO_CHAR(A.BIT_FEC_PUB_DIC,'DD/MM/YYYY') BIT_FEC_PUB_DIC,	\n" + 
		"        A.CAL_ANIO AS ANIO,\n" + 
		"        A.CAL_MES AS MES,\n" + 
		"        concat(A.CAL_MES, concat(' - ', A.CAL_ANIO)) as MESANIOG,              \n" + 
		"        DECODE(A.BIT_TIPO, 'O', 'Ordinario', 'E', 'Extempor�neo') AS bit,                 \n" + 
		"        A.BIT_TIPO AS BIT_TIPO,\n" + 
		"        B.IC_IF_SIAG AS IC_SIAG,\n" + 
		"        TO_CHAR(sci.cal_fec_lim_ent_r,'DD/MM/YYYY') cal_fec_lim_ent_r,\n" + 
		"        TO_CHAR(sci.cal_fec_lim_ent_d,'DD/MM/YYYY') cal_fec_lim_ent_d, \n" + 
		"        TO_CHAR(SCI.CAL_FECHA_INSER,'DD/MM/YYYY') CAL_FECHA_INSER, null as tipoCal \n" + 
		"                    FROM BIT_SUPERVISION A, COMCAT_IF B, sup_calendario_if SCI \n" + 
		"                        WHERE A.IC_IF = B.IC_IF \n" + 
		"                            AND b.ic_if_siag = SCI.ic_cve_siag \n" +
                "                            AND A.cal_mes = SCI.cal_mes \n" + 
                "		                            AND A.cal_anio = SCI.cal_anio \n" + 
                "		                            AND A.bit_tipo = SCI.cal_tipo \n"+
					 "		                            AND SCI.cal_fec_lim_ent_r is not null \n"  );
                                        	
		
		    if( claveSIAG != null && claveSIAG > 0) {
		        strQuery.append(" AND b.ic_if_siag = ? \n");
		        conditions.add(claveSIAG);
		    }
		    if(null != interFin && interFin > 0) {
		            strQuery.append(" AND A.IC_IF IN (SELECT IC_IF FROM SUP_DATOS_IF WHERE IC_CVE_SIAG = ?) \n");
		            conditions.add(interFin);
		    }if(null != anio && anio > 0) {
		            strQuery.append(" AND A.CAL_ANIO = ? \n");
		            conditions.add(anio);
		    }if(null != mes && mes > 0) {
		            strQuery.append(" AND A.CAL_MES = ?");
		            conditions.add(mes);
		    }                
		    if(!"".equals(ejecutivo)) {
		            strQuery.append(" AND A.BIT_EJECUTIVO_LOGIN = ? \n");
		            conditions.add(ejecutivo);
		    }
                    if(tipoDocumento != null && !tipoDocumento.isEmpty()){
                        if("R".equals(tipoDocumento)){
                            strQuery.append(" AND SCI.cal_doc_solicitud IS NOT NULL" );
                        }else if("S".equals(tipoDocumento)){
                            strQuery.append(" AND SCI.cal_doc_dictamen = IS NOT NULL");
                            }
		        
		    }
		strQuery.append("ORDER BY A.IC_IF, B.CG_RAZON_SOCIAL");

		LOGGER.debug("getDocumentQueryFile(strQuery): " + strQuery.toString());                
		LOGGER.debug("getDocumentQueryFile(conditions): " + conditions);
		LOGGER.info("getDocumentQueryFile(S)");
		return strQuery.toString();
	}

	/**
	 * M�todo para generar los archivos PDF y XLS
	 * @param request
	 * @param rs
	 * @param path ruta donde se genera el archivo
	 * @param tipo PDF o XLS
	 * @return nombre del archivo
	 */
	@Override
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {
		LOGGER.info("crearCustomFile(E)");
		String nombreArchivo = "";

		try {

			if("PDF".equals(tipo)) {

				ComunesPDF pdfDoc = new ComunesPDF();
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String[] meses     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("Ciudad de M�xico a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" Monitor ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				pdfDoc.setTable(12, 100);
                                
                                pdfDoc.setCell("Mes y A�o de Garant�as Pagadas",                            "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Calendario",                                        "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Clave SIAG",                                                "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre del Intermediario",                                  "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Ejecutivo Responsable",                                     "celda01",ComunesPDF.CENTER);
				
                                pdfDoc.setCell("N�mero de Garant�as",                                       "celda01",ComunesPDF.CENTER);                               
                                pdfDoc.setCell("Fecha Programada del Resultado de Supervisi�n",             "celda01",ComunesPDF.CENTER);				
                                pdfDoc.setCell("Fecha Real del Resultado de Supervisi�n",                    "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Avance de Cumplimiento del Resultado de Supervisi�n",        "celda01",ComunesPDF.CENTER);				
                                pdfDoc.setCell("Fecha Programada del Dictamen de Supervisi�n",               "celda01",ComunesPDF.CENTER);
                                
                                pdfDoc.setCell("Fecha Real del Dictamen de Supervisi�n",                     "celda01",ComunesPDF.CENTER);                                
                                pdfDoc.setCell("Avance de Cumplimiento del Dictamen de Supervisi�n",         "celda01",ComunesPDF.CENTER);                                                                

				while (rs.next()) {
                                        pdfDoc.setCell((rs.getString("MESANIOG")    ==null) ?" ":rs.getString("MESANIOG"),   "formas",ComunesPDF.CENTER);                                        
                                        pdfDoc.setCell((rs.getString("bit")    ==null) ?" ":rs.getString("bit"),   "formas",ComunesPDF.CENTER);                                        
                                       
                                        					
					pdfDoc.setCell((rs.getString("IC_SIAG")               ==null) ?" ":rs.getString("IC_SIAG"),              "formas",ComunesPDF.CENTER);					
                                        pdfDoc.setCell((rs.getString("NOMBRE_IF")               ==null) ?" ":rs.getString("NOMBRE_IF"),              "formas",ComunesPDF.CENTER);                                        
                                        pdfDoc.setCell((rs.getString("BIT_EJECUTIVO_NOMBRE")    ==null) ?" ":rs.getString("BIT_EJECUTIVO_NOMBRE"),   "formas",ComunesPDF.CENTER);					
                                        
                                        String numt = supervision.obtenerTotalGarantias(rs.getString("MES"), rs.getString("ANIO"), rs.getString("IC_SIAG"), rs.getString("BIT_TIPO"));
                                        pdfDoc.setCell( numt,"formas",ComunesPDF.CENTER);                                        
                                       
                                        //Obtengo el color del semaforo
                                        String semaforoRes = supervision.obtieneSemaforoMonitordeSupervision(rs.getString("BIT_FEC_PUB_RES_SUP"), rs.getString("CAL_FEC_LIM_ENT_R"), "RESOLUCION");
                                        String semaforoDic = supervision.obtieneSemaforoMonitordeSupervision(rs.getString("BIT_FEC_PUB_DIC"), rs.getString("CAL_FEC_LIM_ENT_D"), "DICTAMEN");
                                    
                                        pdfDoc.setCell((rs.getString("CAL_FEC_LIM_ENT_R")        ==null) ?" ":rs.getString("CAL_FEC_LIM_ENT_R"),      "formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("BIT_FEC_PUB_RES_SUP")      ==null) ?" ":rs.getString("BIT_FEC_PUB_RES_SUP"),    "formas",ComunesPDF.CENTER);
					pdfDoc.setCell(semaforoRes, "formas",ComunesPDF.CENTER);                                        
                                        pdfDoc.setCell((rs.getString("CAL_FEC_LIM_ENT_D")        == null)?" ":rs.getString("CAL_FEC_LIM_ENT_D"),      "formas", ComunesPDF.CENTER);                                        
                                        
                                        pdfDoc.setCell((rs.getString("BIT_FEC_PUB_DIC")          ==null) ?" ":rs.getString("BIT_FEC_PUB_DIC"),       "formas",ComunesPDF.CENTER);                                        
                                        pdfDoc.setCell(semaforoDic,                       "formas",ComunesPDF.CENTER);
				    
				}

				pdfDoc.addTable();
				pdfDoc.endDocument();

			} else if("XLS".equals(tipo)) {

				CreaArchivo archivo = new CreaArchivo();
				nombreArchivo = archivo.nombreArchivo()+".xls";
				ComunesXLS xlsDoc = new ComunesXLS(path + nombreArchivo, "Monitor");

				// Encabezado de la tabla
				xlsDoc.setTabla(12);
				
                                xlsDoc.setCelda("Mes y A�o de Garant�as Pagadas",                                     "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Tipo de Calendario",                                    "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Clave SIAG",                             "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Nombre del Intermediario",              "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Ejecutivo Responsable",   "formasb", ComunesXLS.CENTER);
                                
                                xlsDoc.setCelda("N�mero de Garant�as",                       "formasb", ComunesXLS.CENTER);
                                xlsDoc.setCelda("Fecha Programada del Resultado de Supervisi�n",                       "formasb", ComunesXLS.CENTER);
                                xlsDoc.setCelda("Fecha Real del Resultado de Supervisi�n",                       "formasb", ComunesXLS.CENTER);
                                xlsDoc.setCelda("Avance de Cumplimiento del Resultado de Supervisi�n",    "formasb", ComunesXLS.CENTER);
				xlsDoc.setCelda("Fecha Programada del Dictamen de Supervisi�n",            "formasb", ComunesXLS.CENTER);
				                                
                                xlsDoc.setCelda("Fecha Real del Dictamen de Supervisi�n",                       "formasb", ComunesXLS.CENTER);                                
                                xlsDoc.setCelda("Avance de Cumplimiento del Dictamen de Supervisi�n", "formasb", ComunesXLS.CENTER);
			
				//Contenido de la tabla
				while (rs.next()) {
                                        xlsDoc.setCelda((rs.getString("MESANIOG")         ==null) ?" ":rs.getString("MESANIOG")        );
                                        xlsDoc.setCelda((rs.getString("bit")         ==null) ?" ":rs.getString("bit")        );
                                         
                                    
                                        xlsDoc.setCelda((rs.getString("IC_SIAG")    ==null) ?" ":rs.getString("IC_SIAG")   );
                                        xlsDoc.setCelda((rs.getString("NOMBRE_IF")         ==null) ?" ":rs.getString("NOMBRE_IF")        );                                        
                                        xlsDoc.setCelda((rs.getString("BIT_EJECUTIVO_NOMBRE")         ==null) ?" ":rs.getString("BIT_EJECUTIVO_NOMBRE")        );
                                        
                                        String numG = supervision.obtenerTotalGarantias(rs.getString("MES"), rs.getString("ANIO"), rs.getString("IC_SIAG"), rs.getString("BIT_TIPO"));                                        
                                        xlsDoc.setCelda(numG);					
				    
                                        //Obtengo el color del semaforo
                                        String semaforoRes = supervision.obtieneSemaforoMonitordeSupervision(rs.getString("BIT_FEC_PUB_RES_SUP"), rs.getString("CAL_FEC_LIM_ENT_R"), "RESOLUCION");
                                        String semaforoDic = supervision.obtieneSemaforoMonitordeSupervision(rs.getString("BIT_FEC_PUB_DIC"), rs.getString("CAL_FEC_LIM_ENT_D"), "DICTAMEN");
				    
                                        xlsDoc.setCelda((rs.getString("CAL_FEC_LIM_ENT_R")         ==null) ?" ":rs.getString("CAL_FEC_LIM_ENT_R")        );
					xlsDoc.setCelda((rs.getString("BIT_FEC_PUB_RES_SUP")     ==null) ?" ":rs.getString("BIT_FEC_PUB_RES_SUP")    );
					xlsDoc.setCelda(semaforoRes);
                                        xlsDoc.setCelda((rs.getString("CAL_FEC_LIM_ENT_D")                     ==null) ?" ":rs.getString("CAL_FEC_LIM_ENT_D")                    );
                                        
                                        xlsDoc.setCelda((rs.getString("BIT_FEC_PUB_DIC")     ==null) ?" ":rs.getString("BIT_FEC_PUB_DIC"));
					xlsDoc.setCelda(semaforoDic);
                                        
				}

				xlsDoc.cierraTabla();
				xlsDoc.cierraXLS();

			}

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		}
		LOGGER.info("crearCustomFile(S)");
		return nombreArchivo; 
	}

	@Override
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		LOGGER.info("crearPageCustomFile(E)");
		LOGGER.info("crearPageCustomFile(S)");
		return null;
	}

}