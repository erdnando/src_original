package com.nafin.supervision;

import java.io.FileOutputStream;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Clase para generar el archivo de avisos de supervision
 */
public class AvisoSupervision implements IQueryGeneratorRegExtJS {

	private static final Log LOGGER = ServiceLocator.getInstance().getLog(AvisoSupervision.class);
	StringBuilder strQuery = new StringBuilder();
	private Integer interFin;
	private Integer anio;
	private Integer mes;
	private String relGarant;
	private List conditions;

	/**
	 * Constructor por default
	 */
	public AvisoSupervision() {
	}

	public Integer getInterFin() {
		return interFin;
	}

	public void setInterFin(Integer interFin) {
		this.interFin = interFin;
	}

	public Integer getAnio() {
		return anio;
	}

	public void setAnio(Integer anio) {
		this.anio = anio;
	}

	public Integer getMes() {
		return mes;
	}

	public void setMes(Integer mes) {
		this.mes = mes;
	}

	public String getRelGarant() {
		return relGarant;
	}

	public void setRelGarant(String relGarant) {
		this.relGarant = relGarant;
	}

	@Override
	public String getDocumentQuery() {
		return null;
	}

	@Override
	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	@Override
	public String getAggregateCalculationQuery() {
		return "";
	}

	@Override
	public List getConditions() {
		return conditions;
	}

	/**
	 * Obtiene la consulta para generar el archivo sin paginaci�n
	 * @return cadena de texto con la consulta a ejecutar
	 */
	@Override
	public String getDocumentQueryFile() {
		LOGGER.info("getDocumentQueryFile(E)");

		strQuery = new StringBuilder();
		conditions = new ArrayList();

		strQuery.append("SELECT A.IC_CVE_SIAG \n" + 
		", decode(A.CAL_MES, 13, '1erSem', 14, '2doSem', A.cal_mes) AS CAL_MES \n" + 
		", A.CAL_ANIO \n" + 
		", TO_CHAR(A.CAL_FEC_LIM_ENT_E,'DD/MM/YYYY') AS FECHA_EXP \n" + 
		", TO_CHAR(A.CAL_FEC_LIM_ENT_A,'DD/MM/YYYY') AS FECHA_ACL \n" + 
		", TO_CHAR(A.CAL_FEC_LIM_ENT_D,'DD/MM/YYYY') AS FECHA_DIC \n" + 
		", DECODE(A.CAL_TIPO, 'O', 'Ordinario', 'E','Extempor�neo') CAL_TIPO \n" + 
		", A.CAL_REL_GARANTIAS AS REL_GARANTIAS \n" + 
		"FROM SUP_CALENDARIO_IF A \n" + 
		"WHERE 1 = 1 \n");
		if(null != interFin && interFin > 0) {
			strQuery.append("AND A.IC_CVE_SIAG = ? \n");
			conditions.add(interFin);
		}
		if(null != anio && anio > 0) {
			strQuery.append("AND A.CAL_ANIO = ? \n");
			conditions.add(anio);
		}
		if(null != mes && mes > 0) {
			strQuery.append("AND A.CAL_MES = ?");
			conditions.add(mes);
		}

		LOGGER.debug("getDocumentQueryFile(strQuery): " + strQuery.toString());
		LOGGER.debug("getDocumentQueryFile(conditions): " + conditions);
		LOGGER.info("getDocumentQueryFile(S)");
		return strQuery.toString();
	}

	/**
	 * M�todo para generar EL archivo PDF
	 * @param request
	 * @param rs
	 * @param path
	 * @param tipo
	 * @return nombre del archivo
	 */
	@Override
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {
		LOGGER.info("crearCustomFile(E)");
		String nombreArchivo = "";

		if("XLS".equals(tipo)) {
			try {

				//Nombre del archivo
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".xlsx";

				//Nombre de la hoja 1
				String nombreHoja1 = "Hoja 1";

				XSSFWorkbook libroTrabajo = new XSSFWorkbook();
				XSSFSheet hoja1 = libroTrabajo.createSheet(nombreHoja1) ;

				//Agrego el encabezado
				CellStyle cellStyle = libroTrabajo.createCellStyle();
				XSSFRow row = hoja1.createRow(0);
				XSSFCell cell = row.createCell(0);
				cell.setCellValue("Clave SIAG");
				cell = row.createCell(1);
				cell.setCellValue("Mes de Pago del Calendario");
				cell = row.createCell(2);
				cell.setCellValue("A�o de Pago del Calendario");
				cell = row.createCell(3);
				cell.setCellValue("Fecha L�mite de Entrega de Expediente");
				cell = row.createCell(4);
				cell.setCellValue("Fecha L�mite de Entrega de Aclaraciones");
				cell = row.createCell(5);
				cell.setCellValue("Fecha L�mite de Entrega de Dictamen");
				cell = row.createCell(6);
				cell.setCellValue("Tipo de Calendario");
				cell = row.createCell(7);
				cell.setCellValue("Relaci�n de Garant�as");

				//Agrego contenido dinamico
				int r=1;
				while (rs.next()) {
					row = hoja1.createRow(r++);

					if(null != relGarant && !"".equals(relGarant)) {
						relGarant = relGarant.replace(",", "\n");
					} else {
						relGarant = "";
					}
					cell = row.createCell(0);
					cell.setCellValue((rs.getString("IC_CVE_SIAG")     ==null) ?" ":rs.getString("IC_CVE_SIAG"));
					cell = row.createCell(1);
					cell.setCellValue((rs.getString("CAL_MES")         ==null) ?" ":rs.getString("CAL_MES"));
					cell = row.createCell(2);
					cell.setCellValue((rs.getString("CAL_ANIO")        ==null) ?" ":rs.getString("CAL_ANIO"));
					cell = row.createCell(3);
					cell.setCellValue((rs.getString("FECHA_EXP")       ==null) ?" ":rs.getString("FECHA_EXP"));
					cell = row.createCell(4);
					cell.setCellValue((rs.getString("FECHA_ACL")       ==null) ?" ":rs.getString("FECHA_ACL"));
					cell = row.createCell(5);
					cell.setCellValue((rs.getString("FECHA_DIC")       ==null) ?" ":rs.getString("FECHA_DIC"));
					cell = row.createCell(6);
					cell.setCellValue((rs.getString("CAL_TIPO")        ==null) ?" ":rs.getString("CAL_TIPO"));
					cell = row.createCell(7);
					cell.setCellValue(relGarant);
					cellStyle.setWrapText(true);
					cell.setCellStyle(cellStyle);
				}

				// Definimos el tama�o de las celdas
				Sheet ssheet = libroTrabajo.getSheetAt(0);
				ssheet.autoSizeColumn(0);
				ssheet.autoSizeColumn(1);
				ssheet.autoSizeColumn(2);
				ssheet.autoSizeColumn(3);
				ssheet.autoSizeColumn(4);
				ssheet.autoSizeColumn(5);
				ssheet.autoSizeColumn(6);
				ssheet.autoSizeColumn(7);

				//escribir este libro en un OutputStream.
				try (FileOutputStream fileOut = new FileOutputStream(path + nombreArchivo)) {
					libroTrabajo.write(fileOut);
					fileOut.flush();
				}
			} catch(Exception e){
				throw new AppException("Error al generar el archivo", e);
			}

		}

		LOGGER.info("crearCustomFile(S)");
		return nombreArchivo;
	}

	@Override
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}

}