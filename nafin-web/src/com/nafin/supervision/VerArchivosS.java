package com.nafin.supervision;

import com.nafin.nafinetmovil.utils.ServiceLocator;
import java.io.IOException;
import java.io.OutputStream;
import javax.naming.Context;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(name = "VerArchivosS", urlPatterns = { "/VerArchivosS" })
public class VerArchivosS extends HttpServlet {
	private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

	private static Context context;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean success        = true;
		String  usuario        = null;
		String  tipoPerfil     = null;
		String  idArchivo      = null;
		String  nombreArchivo  = null;
		String  acuse          = null;
		String  recibo         = null;
		String  interFin       = null;
		String  anio           = null;
		String  mes            = null;
		String  tipoCalendario = null;
		String  error          = "";
		try {

			nombreArchivo  = request.getParameter("nombreArchivo");
			acuse          = request.getParameter("acuse");
			interFin       = request.getParameter("interFin");
			anio           = request.getParameter("calAnio");
			mes            = request.getParameter("mes");
			tipoPerfil     = request.getParameter("tipoPerfil");
			idArchivo      = request.getParameter("idArchivo");
			recibo         = request.getParameter("recibo");
			tipoCalendario = request.getParameter("tipoCalendario");

			Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);

			/*
			Strng fileType = DOC_DOCUMENTO.substring(DOC_DOCUMENTO.lastIndexOf(".")+1,DOC_DOCUMENTO.length());
			if (fileType.trim().equalsIgnoreCase("txt")) {
				response.setContentType( "text/plain" );
			} else if (fileType.trim().equalsIgnoreCase("doc")) {
				response.setContentType( "application/msword" );
			} else if (fileType.trim().equalsIgnoreCase("xls")) {
				response.setContentType( "application/vnd.ms-excel" );
			} else if (fileType.trim().equalsIgnoreCase("pdf")) {
				response.setContentType( "application/pdf" );
			} else if (fileType.trim().equalsIgnoreCase("docx")) {
				response.setContentType( "application/msword" );
			} else if (fileType.trim().equalsIgnoreCase("xlsx")) {
				response.setContentType( "application/vnd.ms-excel" );
			} else {
				response.setContentType( "application/octet-stream" );
			}
			*/
			response.setContentType("application/octet-stream");
			//response.setContentType( "application/msword" );
			//String disposition = "attachment;
			//fileName="+DOC_DOCUMENTO;
			//response.setHeader("Content-Disposition", disposition);

			ArchivosCargaValue archivosValue = null;
			if(null == idArchivo || "".equals(idArchivo)) {
				archivosValue = supervision.getArchivo(nombreArchivo, acuse);
			} else {
				archivosValue = supervision.getArchivo(tipoPerfil, idArchivo, nombreArchivo, interFin, anio, mes, recibo, tipoCalendario);
			}
			byte[] x = archivosValue.getARCHIVO_ESP();
			response.setHeader("Content-Disposition",  "attachment; fileName=\""+nombreArchivo+"\"");
			OutputStream out = response.getOutputStream();
			//response.setContentType("application/pdf");
			for (int i = 0; i < x.length; i++) {
				out.write(x[i]);
			}
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
			success = false;
			error=ex.getMessage();
		}
	}
}