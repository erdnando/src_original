package com.nafin.supervision;

import com.nafin.nafinetmovil.utils.ServiceLocator;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import net.sf.json.JSONObject;

public class ArchivosMultS extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
      doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        RequestDispatcher rdisp=null;
        try {
        String userName = (String)request.getSession().getAttribute("iNoUsuario");
        ArchivosCargaValue archivosCarga = new ArchivosCargaValue();
        archivosCarga.setUsuario(userName);
            Supervision supervision = ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);
            supervision.deleteArchivosTemps(archivosCarga);
            rdisp=request.getRequestDispatcher("/40supervision/40CargaMasiva/40CargaMasivaExt.jsp?idMenu=40MASIVASUPERV");
        }catch (Exception e) {
                       e.printStackTrace();
                       rdisp=request.getRequestDispatcher("/40supervision/40CargaMasiva/40CargaMasivaExt.jsp?idMenu=40MASIVASUPERV");
        } finally {
                       if(rdisp!=null){
                         rdisp.forward(request,response);
             }  
        }
        
    }
}
