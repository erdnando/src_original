package com.nafin.supervision;

import com.netro.descuento.ConsAutorizaSolic;

import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class SupervisionConsulta implements IQueryGeneratorRegExtJS  {
    private final static Log log = ServiceLocator.getInstance().getLog(ConsAutorizaSolic.class);
    private String  paginaOffset;
    private String  paginaNo;
    private List            conditions;
    StringBuffer qrySentencia = new StringBuffer("");
    private String ic_if;
    private String ic_epo;
    private String ic_folio;
    private String ic_moneda;
    private String ic_estatus_solic;
    private String cs_tipo_solicitud;
    private String pantalla;
    private String acuse;
    private String noSolicitudes;
    
    
    public SupervisionConsulta() {
        super();
    }

    @Override
    public String getDocumentQuery() {
            
            log.info("getDocumentQuery(E)");
            qrySentencia    = new StringBuffer();
            conditions              = new ArrayList();
            

    
            log.debug("qrySentencia---------------> "+qrySentencia);
            log.debug("conditions----------------->"+conditions);
            
            log.info("getDocumentQuery(S)");
            return qrySentencia.toString(); 
    }

    @Override
    public String getDocumentSummaryQueryForIds(List pageIds) {
    
            log.info("getDocumentSummaryQueryForIds(E)");
            qrySentencia    = new StringBuffer();
            conditions              = new ArrayList();
            StringBuffer clavesDocumentos   = new StringBuffer();           

            
            log.info("qrySentencia  "+qrySentencia);
            log.info("conditions "+conditions);
            log.info("getDocumentSummaryQueryForIds(S)");
            return qrySentencia.toString(); 
    }

    @Override
    public String getAggregateCalculationQuery() {
            log.info("getAggregateCalculationQuery(E)");
            qrySentencia    = new StringBuffer();
            conditions              = new ArrayList();
            
            
            
    
            
            
            log.info("qrySentencia  "+qrySentencia);
            log.info("conditions "+conditions);
            log.info("getAggregateCalculationQuery(S)");
            return qrySentencia.toString(); 
    }
    

    @Override
    public List getConditions() {
        // TODO Implement this method
        return Collections.emptyList();
    }

    @Override
    public String getDocumentQueryFile() {
        log.info("getDocumentQueryFile(E)");
        qrySentencia    = new StringBuffer();
        conditions              = new ArrayList();
        
        StringBuffer qrySentenciaC      = new StringBuffer();
        StringBuffer qrySentenciaE      = new StringBuffer();
        
        if(pantalla.equals("Acuse"))  {
            qrySentencia.append(" select t.cc_acuse cc_acuse,\n" + 
            "       t.ic_usuario ic_usuario,\n" + 
            "       t.ac_mes_pago ac_mes_pago,\n" + 
            "       t.ac_anio_pago ac_anio_pago,\n" + 
            //"       obtenArchivos(t.cc_acuse) ac_rel_garantias,\n"+            
            //"       t.ac_rel_garantias ac_rel_garantias,\n" + 
            "       t.ac_no_archivos ac_no_archivos,\n" + 
            "       DECODE(t.ac_tipo_carga,\n" + 
            "              'P',\n" + 
            "              'Carga Parcial',\n" + 
            "              'F',\n" + 
            "              'Carga Final',\n" + 
            "              'A',\n" + 
            "              'Carga Aclaraciones',\n" + 
            "              'E',\n" + 
            "              'Carga Extemporanea') ac_tipo_carga,\n" + 
            "      to_char(t.ac_fecha_acuse, 'dd/mm/yyyy') ac_fecha_acuse  \n" + 
            "  from sup_acuse t\n" + 
            " where t.cc_acuse = "+acuse);    
        
        
        }
        
        log.info("qrySentencia  ------"+cs_tipo_solicitud+"----"+pantalla+"-----"+qrySentencia);
        log.info("conditions "+conditions);
        log.info("getDocumentQueryFile(S)");
        return qrySentencia.toString(); 
        
    }

    @Override
    public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {
        log.debug("crearCustomFile (E)");
        
        String nombreArchivo = "";
        HttpSession session = request.getSession();
        ComunesPDF pdfDoc = new ComunesPDF();
        CreaArchivo creaArchivo = new CreaArchivo();
        StringBuffer contenidoArchivo = new StringBuffer();
        int numRegistrosMN =0, numRegistrosDL=0;
        
        String tipoCarga = "";
        String noArchivos = "";
        String relGarantia = "";
        String acuseF = "";
        String Fecha_hora = "";
        String usuario = "";
      
        
        try {

                nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
                pdfDoc = new ComunesPDF(2, path + nombreArchivo);
                String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
                String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
                String diaActual    = fechaActual.substring(0,2);
                String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
                String anioActual   = fechaActual.substring(6,10);
                String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
        
                pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                        ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
                        (String)session.getAttribute("sesExterno"),
                        (String) session.getAttribute("strNombre"),
                        (String) session.getAttribute("strNombreUsuario"),
                        (String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));  
                        
                        pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
                
                                
                        pdfDoc.setTable(2, 85);
                        /*pdfDoc.setCell("Tipo de Carga","celda02",ComunesPDF.CENTER);
                        pdfDoc.setCell("No total de Archivos Transmitidos","celda02",ComunesPDF.CENTER);
                        pdfDoc.setCell("Numero de Credito de Garantia","celda02",ComunesPDF.CENTER);
                        pdfDoc.setCell("Folio de la Operacion","celda02",ComunesPDF.CENTER);
                        pdfDoc.setCell("Fecha y Hora de Carga","celda02",ComunesPDF.CENTER);                           
                        pdfDoc.setCell("Usuario ","celda02",ComunesPDF.CENTER);*/
                       
                        Supervision supervision = com.nafin.nafinetmovil.utils.ServiceLocator.getInstance().lookup("SupervisionEJB", Supervision.class);
                        
                        while (rs.next())       {               
                                 tipoCarga = (rs.getString("ac_tipo_carga") == null) ? "" : rs.getString("ac_tipo_carga");
                                 noArchivos = (rs.getString("ac_no_archivos") == null) ? "" : rs.getString("ac_no_archivos");
                                 acuseF = (rs.getString("cc_acuse") == null) ? "" : rs.getString("cc_acuse");
                                 relGarantia = supervision.obtenGarantias(acuseF);
                                 //relGarantia = (rs.getString("ac_rel_garantias") == null) ? "" : rs.getString("ac_rel_garantias");
                                 Fecha_hora = (rs.getString("ac_fecha_acuse") == null) ? "" : rs.getString("ac_fecha_acuse");
                                 usuario = (rs.getString("ic_usuario") == null) ? "" : rs.getString("ic_usuario");
                            
                            /*pdfDoc.setCell(tipoCarga,"formas",ComunesPDF.LEFT);
                            pdfDoc.setCell(noArchivos,"formas",ComunesPDF.LEFT);
                            pdfDoc.setCell(relGarantia,"formas",ComunesPDF.LEFT);
                            pdfDoc.setCell(acuseF,"formas",ComunesPDF.CENTER);
                            pdfDoc.setCell(Fecha_hora,"formas",ComunesPDF.CENTER);
                            pdfDoc.setCell(usuario,"formas",ComunesPDF.CENTER);*/
                        }               
                        
            pdfDoc.setCell("Tipo de Carga","celda02",ComunesPDF.CENTER);
            pdfDoc.setCell(tipoCarga,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("No total de Archivos Transmitidos","celda02",ComunesPDF.CENTER);
            pdfDoc.setCell(noArchivos,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("Numero de Credito de Garantia","celda02",ComunesPDF.CENTER);
            pdfDoc.setCell(relGarantia,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("Folio de la Operacion","celda02",ComunesPDF.CENTER);
            pdfDoc.setCell(acuseF,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("Fecha y Hora de Carga","celda02",ComunesPDF.CENTER);  
            pdfDoc.setCell(Fecha_hora,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("Usuario ","celda02",ComunesPDF.CENTER);
            pdfDoc.setCell(usuario,"formas",ComunesPDF.CENTER);
                                                        
                        pdfDoc.addTable();
                        pdfDoc.endDocument();
                                                
        
        } catch(Throwable e) {
                throw new AppException("Error al generar el archivo ", e);
        } finally {
                try {
                        //rs.close();
                } catch(Exception e) {}
          
        }
        log.debug("crearCustomFile (S)");
        return nombreArchivo;
    }

    @Override
    public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
        String nombreArchivo = "";
        HttpSession session = request.getSession();     
        ComunesPDF pdfDoc = new ComunesPDF();
        CreaArchivo creaArchivo = new CreaArchivo();
        StringBuffer contenidoArchivo = new StringBuffer();
        int i=0;
        try {

        
        
        } catch(Throwable e) {
                throw new AppException("Error al generar el archivo ", e);
        } finally {
                try {
                        //rs.close();
                } catch(Exception e) {}
          
        }
        return nombreArchivo;
        }

    

    public void setPaginaOffset(String paginaOffset) {
        this.paginaOffset = paginaOffset;
    }

    public String getPaginaOffset() {
        return paginaOffset;
    }

    public void setPaginaNo(String paginaNo) {
        this.paginaNo = paginaNo;
    }

    public String getPaginaNo() {
        return paginaNo;
    }

    public void setConditions(List conditions) {
        this.conditions = conditions;
    }

    public List getConditions1() {
        return conditions;
    }

    public void setQrySentencia(StringBuffer qrySentencia) {
        this.qrySentencia = qrySentencia;
    }

    public StringBuffer getQrySentencia() {
        return qrySentencia;
    }

    public void setIc_if(String ic_if) {
        this.ic_if = ic_if;
    }

    public String getIc_if() {
        return ic_if;
    }

    public void setIc_epo(String ic_epo) {
        this.ic_epo = ic_epo;
    }

    public String getIc_epo() {
        return ic_epo;
    }

    public void setIc_folio(String ic_folio) {
        this.ic_folio = ic_folio;
    }

    public String getIc_folio() {
        return ic_folio;
    }

    public void setIc_moneda(String ic_moneda) {
        this.ic_moneda = ic_moneda;
    }

    public String getIc_moneda() {
        return ic_moneda;
    }

    public void setIc_estatus_solic(String ic_estatus_solic) {
        this.ic_estatus_solic = ic_estatus_solic;
    }

    public String getIc_estatus_solic() {
        return ic_estatus_solic;
    }

    public void setCs_tipo_solicitud(String cs_tipo_solicitud) {
        this.cs_tipo_solicitud = cs_tipo_solicitud;
    }

    public String getCs_tipo_solicitud() {
        return cs_tipo_solicitud;
    }

    public void setPantalla(String pantalla) {
        this.pantalla = pantalla;
    }

    public String getPantalla() {
        return pantalla;
    }

    public void setNoSolicitudes(String noSolicitudes) {
        this.noSolicitudes = noSolicitudes;
    }

    public String getNoSolicitudes() {
        return noSolicitudes;
    }

    public void setAcuse(String acuse) {
        this.acuse = acuse;
    }

    public String getAcuse() {
        return acuse;
    }
}
