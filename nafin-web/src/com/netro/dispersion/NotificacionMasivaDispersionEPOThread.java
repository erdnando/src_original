package com.netro.dispersion;

import java.io.Serializable;

import java.util.List;

import javax.servlet.http.HttpSession;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class NotificacionMasivaDispersionEPOThread implements Runnable, Serializable {

	private static final long 		serialVersionUID 			= 20130529L;
	//Variable para enviar mensajes al log.
	private static final Log 		log 							= ServiceLocator.getInstance().getLog(NotificacionMasivaDispersionEPOThread.class);
	
   private volatile boolean 		running           		= false;
   private volatile boolean 		completed         		= false;
   private volatile boolean 		error             		= false;
	private volatile boolean 		requestStop		 			= false;
	
	// Porcentaje de Avance
	
	private volatile int   			numberOfRegisters 		= 0;
	private volatile float 			percent 		   			= 0;
	private volatile int  			processedRegisters 		= 0; 
   
	// Variables especificas
	private List						eposSeleccionadas			= null; 
	private List 						reportesEPO					= null;
   private String						loginDelUsuario			= null;
   private String						nombreDelUsuario			= null;
   private String						correoUsuario				= null;
   private String						fechaDeVencimiento		= null;
   private HttpSession 				session						= null;
   private String						directorioTemporal		= null;
   private String						directorioPublicacion	= null;			
   private int 						totalNotificaciones 		= 0;			
   private DispersionBean   		dispersionBean   			= null;
   private NotificaDispersionEPO	notificaDispersionEPO	= null;
   private StringBuffer				mensajes						= null;
   
   /* 
   	Nota: totalNotificaciones
   	Se deber� calcular el n�mero total de notificaciones a partir de enviarACliente,
		este n�mero representar� nuestro porcentaje de avance. enviarACliente.length
	*/
 
   public NotificacionMasivaDispersionEPOThread(){
   
   	this.running    		= false;
   	this.completed  		= false;
   	this.error      		= false;
   	this.requestStop 		= false;
   	
   }
   
   public void run() {
 
   	 // Si no hay ningun registro v�lido, finalizar la operacion
   	 if( this.totalNotificaciones == 0 || this.reportesEPO == null ){
   	 	 
   	 	 log.info("run: Something wrong happened here :(");
   	 	 log.info("run.totalNotificaciones = <" + this.totalNotificaciones +">");
   	 	 log.info("run.reportesEPO         = <" + this.reportesEPO         +">");
   	 	 
		 	 setRunning(false);
		 	 setCompleted(true);
		 	 setError(false);
		 	 
		 	 setNumberOfRegisters(this.totalNotificaciones);
		 	 setPercent(100);
		 	 setProcessedRegisters(0);
		 	 return;
		 	 
		 }
		 
		 // Inicializar variables de ejecuci�n
		 setRunning(true);
		 setCompleted(false);
		 setError(false);
		 
		 // Inicializar variables de c�lculo del porcentaje
		 setNumberOfRegisters(this.totalNotificaciones);
		 setPercent(0);
		 setProcessedRegisters(0);
	
       try {
			 
       	 // Crear un buffer donde se guardar�n los mensajes de error
       	 this.mensajes		= new StringBuffer(512);
		 
          // Para cada EPO en lista de reportes:
          int cuentaNotificaciones =  0;
          int indiceReporte		  =  -1;
			 while( this.isRunning() && !this.isCompleted() ){
 
			 	// Revisar si se le ha dado la orden al thread de detenerse
			 	if(  this.getRequestStop() ){
					break;
				} 
				
				// Revisar que no se hayan leido todos los registros
				indiceReporte++;
				if( indiceReporte >= reportesEPO.size() ){
					break;
				}
				
			 	// Obtener Clave de la EPO 
			 	RepDispEpo 	reporteEPO 	= (RepDispEpo) reportesEPO.get(indiceReporte);
			 	String 		claveEPO 	= reporteEPO == null?"-1":reporteEPO.getIc_epo();
			 	
			 	// Si la EPO de reportes no se encuentra la lista de EPOS seleccionadas, pasar a la EPO siguiente.
			 	if( !eposSeleccionadas.contains(claveEPO) ){
			 		log.info("run.claveEPO(Skipped) = <" + claveEPO   + ">");
			 		log.info("run.reporteEPO        = <" + reporteEPO + ">");
			 		continue;
			 	}
			 	
			 	// Notificar a la EPO y al personal de NAFIN; enlistar a la EPO para su dispersion
			 	notificaDispersionEPO.enlistarDispersionEPO(
			 		claveEPO,
					reporteEPO,
					loginDelUsuario,
					nombreDelUsuario,
					correoUsuario,
					fechaDeVencimiento,
					session,
					directorioTemporal,
					directorioPublicacion, 
					dispersionBean,
					this.mensajes
				);
			 	System.err.println("---> this.mensajes = <" + this.mensajes.toString() + ">");
			 	// Incrementar contador de EPO notificadas
			 	cuentaNotificaciones++;
			 	
			 	// Actualizar porcentaje de avance
			 	setProcessedRegisters(cuentaNotificaciones);
			 	setPercent((cuentaNotificaciones/(float)this.totalNotificaciones)*100);
 
			 }
			 
       } catch(Exception e) {
       	 
			setError(true);
			this.mensajes.setLength(0);
			this.mensajes.append("Ocurri� un error inesperado al realizar la operaci�n: " + e.getCause() + "; se aborta el proceso.");
			
			log.error("run(Exception)");
			log.error("run.eposSeleccionadas     = <" + eposSeleccionadas     + ">");
			log.error("run.reportesEPO           = <" + reportesEPO           + ">");
			log.error("run.loginDelUsuario       = <" + loginDelUsuario       + ">");
			log.error("run.nombreDelUsuario      = <" + nombreDelUsuario      + ">");
			log.error("run.correoUsuario         = <" + correoUsuario         + ">");
			log.error("run.fechaDeVencimiento    = <" + fechaDeVencimiento    + ">");
			log.error("run.session               = <" + session               + ">");
			log.error("run.directorioTemporal    = <" + directorioTemporal    + ">");
			log.error("run.directorioPublicacion = <" + directorioPublicacion + ">");
			log.error("run.totalNotificaciones   = <" + totalNotificaciones   + ">");
			log.error("run.dispersionBean        = <" + dispersionBean        + ">");
			log.error("run.notificaDispersionEPO  = <" + notificaDispersionEPO  + ">");
			log.error("run.mensajes              = <" + mensajes.toString()   + ">");

         e.printStackTrace();
          
       } finally {
       	 
			 setRunning(false);
			 setCompleted(true);
			 setProcessedRegisters(this.totalNotificaciones);
			 setPercent(100.0f);
			 
		 }
         
   }
   
	public synchronized boolean isRunning(){
		return this.running;
	}

	public synchronized void setRunning(boolean running){
		this.running = running;
	}

	public synchronized boolean isCompleted(){
		return this.completed;
	}

	public synchronized void setCompleted(boolean completed){
		this.completed = completed;
	}

	public synchronized boolean hasError(){
		return this.error;
	}

	public synchronized void setError(boolean error){
		this.error = error;
	}
 
	public synchronized void setRequestStop(boolean requestStop){
		// En caso de que el thread no haya sido lanzado...
		if( !this.running && !this.completed ){
			 setRunning(false);
			 setCompleted(true);
		}
		this.requestStop = requestStop;
	}
	
	public synchronized boolean getRequestStop(){
		return this.requestStop;
	}
	
	// Porcentaje de Avance
	
	public synchronized int getNumberOfRegisters() {		
		return numberOfRegisters;	
	}
 
	public synchronized void setNumberOfRegisters(int numberOfRegisters) {
		this.numberOfRegisters = numberOfRegisters;	
	}

 
	public synchronized float getPercent() {		
		return percent;	
	}

 
	public synchronized void setPercent(float percent) {		
		this.percent = percent;	
	}

 
	public synchronized int getProcessedRegisters() {		
		return processedRegisters;	
	}


 
	public synchronized void setProcessedRegisters(int processedRegisters) {		
		this.processedRegisters = processedRegisters;	
	}

 
	// Variables espec�ficas

	public List getEposSeleccionadas() {
		return eposSeleccionadas;	
	}


	public void setEposSeleccionadas(List eposSeleccionadas) {
		this.eposSeleccionadas = eposSeleccionadas;	
	}

 
	public List getReportesEPO() {
		return reportesEPO;	
	}

 
	public void setReportesEPO(List reportesEPO) {
		this.reportesEPO = reportesEPO;	
	}

 
	public String getLoginDelUsuario() {
		return loginDelUsuario;	
	}


 
	public void setLoginDelUsuario(String loginDelUsuario) {		
		this.loginDelUsuario = loginDelUsuario;	
	}

 
	public String getNombreDelUsuario() {		
		return nombreDelUsuario;	
	}


 
	public void setNombreDelUsuario(String nombreDelUsuario) {		
		this.nombreDelUsuario = nombreDelUsuario;	
	}


 
	public String getCorreoUsuario() {		
		return correoUsuario;	
	}

 
	public void setCorreoUsuario(String correoUsuario) {		
		this.correoUsuario = correoUsuario;	
	}

 
	public String getFechaDeVencimiento() {		
		return fechaDeVencimiento;	
	}


 
	public void setFechaDeVencimiento(String fechaDeVencimiento) {		
		this.fechaDeVencimiento = fechaDeVencimiento;	
	}

 
	public HttpSession getSession() {		
		return session;	
	}

 
	public void setSession(HttpSession session) {		
		this.session = session;	
	}

 
	public String getDirectorioTemporal() {		
		return directorioTemporal;	
	}


 
	public void setDirectorioTemporal(String directorioTemporal) {		
		this.directorioTemporal = directorioTemporal;	
	}

 
	public String getDirectorioPublicacion() {		
		return directorioPublicacion;	
	}


 
	public void setDirectorioPublicacion(String directorioPublicacion) {		
		this.directorioPublicacion = directorioPublicacion;	
	}

 
	public int getTotalNotificaciones() {		
		return totalNotificaciones;	
	}

 
	public void setTotalNotificaciones(int totalNotificaciones) {		
		this.totalNotificaciones = totalNotificaciones;	
	}


 
	public DispersionBean getDispersionBean() {		
		return dispersionBean;	
	}

 
	public void setDispersionBean(DispersionBean dispersionBean) {		
		this.dispersionBean = dispersionBean;	
	}

 
	public NotificaDispersionEPO getNotificaDispersionEPO() {		
		return this.notificaDispersionEPO;	
	}


 
	public void setNotificaDispersionEPO( NotificaDispersionEPO notificaDispersionEPO) {		
		this.notificaDispersionEPO = notificaDispersionEPO;	
	}


 
	public String getMensajes(){
		return this.mensajes.toString();
	}
	
}