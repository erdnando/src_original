package com.netro.dispersion;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashMap;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class EnvioFlujoFondosEPOThread implements Runnable, Serializable {

	private static final long 		serialVersionUID 			= 20130604L;
	//Variable para enviar mensajes al log.
	private static final Log 		log 							= ServiceLocator.getInstance().getLog(EnvioFlujoFondosEPOThread.class);
	
   private volatile boolean 		running           		= false;
   private volatile boolean 		completed         		= false;
   private volatile boolean 		error             		= false;
	private volatile boolean 		requestStop		 			= false;
	
	// Porcentaje de Avance
	
	private volatile int   			numberOfRegisters 		= 0;
	private volatile float 			percent 		   			= 0;
	private volatile int  			processedRegisters 		= 0; 
   
	// Variables especificas

   private String						loginDelUsuario			= null;
   private String						nombreDelUsuario			= null;		
   private Dispersion   			dispersion      			= null;
   private JSONArray					registrosDispersionEPO	= null;
   private int							numeroTotalEnviosFFON	= 0;
   private ArrayList					resultados					= null;
   private StringBuffer				mensajes						= null;
  
   public EnvioFlujoFondosEPOThread(){
   
   	this.running    		= false;
   	this.completed  		= false;
   	this.error      		= false;
   	this.requestStop 		= false;
   	
   }
   
   public void run() {
 
   	 // Si no hay ningun registro v�lido, finalizar la operacion
   	 if( this.numeroTotalEnviosFFON == 0 || this.registrosDispersionEPO == null ){
   	 	 
   	 	 log.info("run: Something wrong happened here :(");
   	 	 log.info("run.numeroTotalEnviosFFON  = <" + this.numeroTotalEnviosFFON  +">");
   	 	 log.info("run.registrosDispersionEPO = <" + this.registrosDispersionEPO +">");
   	 	 
		 	 setRunning(false);
		 	 setCompleted(true);
		 	 setError(false);
		 	 
		 	 setNumberOfRegisters(this.numeroTotalEnviosFFON);
		 	 setPercent(100);
		 	 setProcessedRegisters(0);
		 	 return;
		 	 
		 }
		 
		 // Inicializar variables de ejecuci�n
		 setRunning(true);
		 setCompleted(false);
		 setError(false);
		 
		 // Inicializar variables de c�lculo del porcentaje
		 setNumberOfRegisters(this.numeroTotalEnviosFFON);
		 setPercent(0);
		 setProcessedRegisters(0);
	
       try {
			 
       	 // Crear ArrayList donde se guardar�n los resultados de la operaci�n
       	 this.resultados = new ArrayList();
       	 
       	 // Crear un buffer donde se guardar�n los mensajes de error
       	 this.mensajes   = new StringBuffer(512);
		 
          // Para cada EPO en lista de reportes:
          int cuentaEnviosFFON	=  0;
          int indiceEnvioFFON		=  -1;
			 while( this.isRunning() && !this.isCompleted() ){
 
			 	// Revisar si se le ha dado la orden al thread de detenerse
			 	if(  this.getRequestStop() ){
					break;
				} 
				
				// Revisar que no se hayan leido todos los registros
				indiceEnvioFFON++;
				if( indiceEnvioFFON >= registrosDispersionEPO.size() ){
					break;
				}
				
			 	// Obtener Clave de la EPO 
			 	JSONObject 	registroDispersionEPO 	= (JSONObject) registrosDispersionEPO.getJSONObject(indiceEnvioFFON);
			 	String 		recordID 					= (registroDispersionEPO.getString("RECORD_ID")				== null)?"":registroDispersionEPO.getString("RECORD_ID");
			 	String 		icEPO 						= (registroDispersionEPO.getString("IC_EPO")					== null)?"":registroDispersionEPO.getString("IC_EPO");
			 	String 		icMoneda 					= (registroDispersionEPO.getString("IC_MONEDA")				== null)?"":registroDispersionEPO.getString("IC_MONEDA");
			 	String 		dcVencimiento 				= (registroDispersionEPO.getString("DC_VENCIMIENTO")		== null)?"":registroDispersionEPO.getString("DC_VENCIMIENTO");
			 	String 		tipoDocumento 				= (registroDispersionEPO.getString("TIPO_DOCUMENTO")		== null)?"":registroDispersionEPO.getString("TIPO_DOCUMENTO");
			 	String 		totalDocumentos 			= (registroDispersionEPO.getString("TOTAL_DOCUMENTOS")	== null)?"":registroDispersionEPO.getString("TOTAL_DOCUMENTOS");
			 	String 		monto 						= (registroDispersionEPO.getString("MONTO")					== null)?"":registroDispersionEPO.getString("MONTO");
 			 	
			 	System.err.println("this.resultados = <" + this.resultados+ ">");
			 	
			 	// Enviar a Flujo de Fondos
			 	HashMap resultado = dispersion.enviarFlujoFondosDocumentosEPO(
			 		recordID,
					icEPO,
					icMoneda,
					dcVencimiento,
					tipoDocumento,
					totalDocumentos,
					monto					
				);
				this.resultados.add(resultado);
			 	
			 	// Incrementar contador de EPO notificadas
			 	cuentaEnviosFFON++;
			 	
			 	// Actualizar porcentaje de avance
			 	setProcessedRegisters(cuentaEnviosFFON);
			 	setPercent((cuentaEnviosFFON/(float)this.numeroTotalEnviosFFON)*100);
 
			 }
			 
       } catch(Exception e) {
       	 
			setError(true);
			this.mensajes.setLength(0);
			this.mensajes.append("Ocurri� un error inesperado al realizar la operaci�n: " + e.getCause() + "; se aborta el proceso.");
			
			log.error("run(Exception)");	
			log.error("run.loginDelUsuario 			= <" + loginDelUsuario			+ ">");
			log.error("run.nombreDelUsuario 			= <" + nombreDelUsuario			+ ">");
			log.error("run.dispersion 					= <" + dispersion					+ ">");
			log.error("run.registrosDispersionEPO 	= <" + registrosDispersionEPO	+ ">");
			log.error("run.numeroTotalEnviosFFON 	= <" + numeroTotalEnviosFFON	+ ">");
			log.error("run.resultados 					= <" + resultados					+ ">");
			log.error("run.mensajes 					= <" + mensajes.toString() 	+ ">");

         e.printStackTrace();
          
       } finally {
       	 
			 setRunning(false);
			 setCompleted(true);
			 setProcessedRegisters(this.numeroTotalEnviosFFON);
			 setPercent(100.0f);
			 
		 }
         
   }
   
	public synchronized boolean isRunning(){
		return this.running;
	}

	public synchronized void setRunning(boolean running){
		this.running = running;
	}

	public synchronized boolean isCompleted(){
		return this.completed;
	}

	public synchronized void setCompleted(boolean completed){
		this.completed = completed;
	}

	public synchronized boolean hasError(){
		return this.error;
	}

	public synchronized void setError(boolean error){
		this.error = error;
	}
 
	public synchronized void setRequestStop(boolean requestStop){
		// En caso de que el thread no haya sido lanzado...
		if( !this.running && !this.completed ){
			 setRunning(false);
			 setCompleted(true);
		}
		this.requestStop = requestStop;
	}
	
	public synchronized boolean getRequestStop(){
		return this.requestStop;
	}
	
	// Porcentaje de Avance
	
	public synchronized int getNumberOfRegisters() {		
		return numberOfRegisters;	
	}
 
	public synchronized void setNumberOfRegisters(int numberOfRegisters) {
		this.numberOfRegisters = numberOfRegisters;	
	}

 
	public synchronized float getPercent() {		
		return percent;	
	}

 
	public synchronized void setPercent(float percent) {		
		this.percent = percent;	
	}

 
	public synchronized int getProcessedRegisters() {		
		return processedRegisters;	
	}


 
	public synchronized void setProcessedRegisters(int processedRegisters) {		
		this.processedRegisters = processedRegisters;	
	}

 
	// Variables espec�ficas
 
	public String getLoginDelUsuario() {
		return loginDelUsuario;	
	}
 
	public void setLoginDelUsuario(String loginDelUsuario) {		
		this.loginDelUsuario = loginDelUsuario;	
	}

 
	public String getNombreDelUsuario() {		
		return nombreDelUsuario;	
	}
 
	public void setNombreDelUsuario(String nombreDelUsuario) {		
		this.nombreDelUsuario = nombreDelUsuario;	
	}
 
	public Dispersion getDispersion() {		
		return dispersion;	
	}

 
	public void setDispersion(Dispersion dispersion) {		
		this.dispersion = dispersion;	
	}
	
	public JSONArray getRegistrosDispersionEPO() {		
		return this.registrosDispersionEPO;	
	}


 
	public void setRegistrosDispersionEPO(JSONArray registrosDispersionEPO) {		
		this.registrosDispersionEPO = registrosDispersionEPO;	
	}

 
	public int getNumeroTotalEnviosFFON() {		
		return this.numeroTotalEnviosFFON;	
	}

 
	public void setNumeroTotalEnviosFFON( int numeroTotalEnviosFFON ) {		
		this.numeroTotalEnviosFFON = numeroTotalEnviosFFON;	
	}

	public ArrayList getResultados(){
		return this.resultados;
	}
 
	public String getMensajes(){
		return this.mensajes.toString();
	}
	
}