package com.netro.dispersion;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.HashMap;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**

	Esta clase se encarga de generar los archivos de la consulta dentro del monitor de flujo de fondos, en la pantalla:
		PANTALLA: ADMIN NAFIN - ADMINISTRACION - FLUJO DE FONDOS - MONITOR
	
	@author jshernandez
	@since  F006-2013 -- ADMIN NAFIN - FFON Monitor; 06/09/2013 03:54:16 p.m.

 */
public class ConsultaDetalleMonitoreoIfFFON {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaDetalleMonitoreoIfFFON.class);
	
	/**
	 *	Crea un archivo CSV con el detalle de la Consulta del Monitor Flujo de Fondos.
	 *	@throws AppException   
	 *	
	 *	@param directorio <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 *	@param folio <tt>String</tt> con el numero de folio de la operacion.
	 * @param tablaDetalle <tt>String</tt> con un sufijo que indica la tabla de donde se tomar&aacute; el detalle de la informaci&oacute;n.
	 *
	 *	@return <tt>String</tt> con el nombre del archivo generado.                               
	 *
	 */	
	public static String generaArchivoCSV( String directorio, String folio, String tablaDetalle )
		throws AppException {

		log.info("generaArchivoCSV(E)");
		
		FileOutputStream		out 				= null;
		BufferedWriter 		csv 				= null;
		CreaArchivo 			archivo 			= null;
		String					nombreArchivo 	= null;
 
		// 2. Obtener instancia de EJB de Dispersion
		Dispersion 	dispersion 			= null;
		try {
							
			dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
							
		} catch(Exception e) {
			 
			String msg	= "Ocurri� un error al obtener instancia del EJB de Dispersi�n";
			log.error(msg);
			e.printStackTrace();
					
			throw new AppException(msg);
			 
		}
	
		try {
 
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".csv";
			out 				= new FileOutputStream(directorio+nombreArchivo);
			csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
 
			// Agregar detalle de la Operaci�n
			csv.write("Detalle de Operaci�n: ");
			csv.write(folio);
			
			// Agregar descripcion
			HashMap detalle = dispersion.getDetalleOperacion(folio, tablaDetalle);

			csv.write("\nNum. de Campo,Descripci�n,Valor");
			csv.write("\n1,Fecha de captura,");
			csv.write( (String) detalle.get("1"));
			csv.write("\n2,ID sistema,");
			csv.write( (String) detalle.get("2"));
			csv.write("\n3,Area que captura,");
			csv.write( (String) detalle.get("3"));
			csv.write("\n4,Folio,");
			csv.write( (String) detalle.get("4"));
			csv.write("\n5,Tipo de operaci�n,");
			csv.write( (String) detalle.get("5"));
			csv.write("\n6,Referncia del movimiento,");
			csv.write( (String) detalle.get("6"));
			csv.write("\n,Cliente  ordenante");
			csv.write("\n8,Clave ordenante,");
			csv.write( (String) detalle.get("8"));
			csv.write("\n9,RFC,");
			csv.write( (String) detalle.get("9"));
			csv.write("\n10,Nombre,");
			csv.write( detalle.get("10").toString().replace(',',' '));
			csv.write("\n11,Domicilio,");
			csv.write( detalle.get("11").toString().replace(',',' '));
			csv.write("\n12,Banco,");
			csv.write( (String) detalle.get("12"));
			csv.write("\n13,Plaza,");
			csv.write( (String) detalle.get("13"));
			csv.write("\n14,Sucursal,");
			csv.write( (String) detalle.get("14"));
			csv.write("\n15,No. Cuenta,");
			csv.write( (String) detalle.get("15"));
			csv.write("\n16,Tipo de Cuenta,");
			csv.write( (String) detalle.get("16"));
			csv.write("\n17,Tipo de Divisa,");
			csv.write( (String) detalle.get("17"));
			csv.write("\n,Cliente Beneficiario");
			csv.write("\n19,Clave del Beneficiario,");
			csv.write( (String) detalle.get("19"));
			csv.write("\n20,RFC,");
			csv.write( (String) detalle.get("20"));
			csv.write("\n21,Nombre,");
			csv.write( detalle.get("21").toString().replace(',',' '));
			csv.write("\n22,Domicilio,");
			csv.write( detalle.get("22").toString().replace(',',' '));
			csv.write("\n23,Banco,");
			csv.write( (String) detalle.get("23"));
			csv.write("\n24,Plaza,");
			csv.write( (String) detalle.get("24"));
			csv.write("\n25,,");
			csv.write( (String) detalle.get("25"));
			csv.write("\n26,No. Cuenta Clabe,");
			csv.write( (String) detalle.get("26"));
			csv.write("\n27,Tipo de cuenta,");
			csv.write( (String) detalle.get("27"));
			csv.write("\n28,Tipo de Divisa,");
			csv.write( (String) detalle.get("28"));
			csv.write("\n29,Fecha Valor,");
			csv.write( (String) detalle.get("29"));
			csv.write("\n30,Importe,");
			csv.write( (String) detalle.get("30"));
			csv.write("\n31,Descripci�n de la instrucci�n,");
			csv.write( detalle.get("31").toString().replace(',',' '));
			csv.write("\n34,Concepto,");
			csv.write( detalle.get("34").toString().replace(',',' '));
			csv.write("\n35,Tipo de operaci�n (movto.),");
			csv.write( (String) detalle.get("35"));
			csv.write("\n36,Clave rastreo,");
			csv.write( (String) detalle.get("36"));
			csv.write("\n38,Digito verificador de la cuenta ordenante,");
			csv.write( (String) detalle.get("38"));
			csv.write("\n39,Digito verificador del beneficiario,");
			csv.write( (String) detalle.get("39"));
			csv.write("\n74,Leyenda de rastreo,");
			csv.write( detalle.get("74").toString().replace(',',' '));
 
		} catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoCSV(OutOfMemoryError)");
			log.error("generaArchivoCSV.message      = <" + om.getMessage() + ">");
			log.error("generaArchivoCSV.freeMemory   = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error("generaArchivoCSV.directorio   = <" + directorio      + ">");
			log.error("generaArchivoCSV.folio        = <" + folio           + ">");
			log.error("generaArchivoCSV.tablaDetalle = <" + tablaDetalle    + ">");
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoCSV(Exception)");
			log.error("generaArchivoCSV.message      = <" + e.getMessage() + ">");
			log.error("generaArchivoCSV.directorio   = <" + directorio     + ">");
			log.error("generaArchivoCSV.folio        = <" + folio          + ">");
			log.error("generaArchivoCSV.tablaDetalle = <" + tablaDetalle   + ">");
			e.printStackTrace(); 
			
			throw new AppException("Ocurri� un error al generar el CSV con la informaci�n detallada.");
			
		} finally {
 
			// Cerrar archivo
			if(csv 	!= null )	try{csv.close();}catch(Exception e){};
			
			log.info("generaArchivoCSV(S)");
			
		}
		
		return nombreArchivo;
		
	}
 	
}