package com.netro.dispersion;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/******************************************************************************************
 * Fodea			11-2014																							*
 * Descripci�n: Migraci�n de la pantalla Administraci�n-Dispersion-Resumen IFs				*
 *					a ExtJS. Realiza la consulta para llenar el grid y generar los archivos.	*
 * Elabor�:		Agust�n Bautista Ruiz																		*
 * Fecha:		06/08/2014																						*
 ******************************************************************************************/
public class ConsultaDispersion implements IQueryGeneratorRegExtJS   {

	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaDispersion.class);
	private int 	icIf;
	private int 	icEpo;
	private int 	icMoneda;
	private int 	numeroTot;
	private int 	proveedorTot;
	private int 	docTot;
	private double montoTot;
	private double montoDispTot;
	private String fechaInicio;
	private String fechaFin;
	private String usuario;
	private String nombreIf;
	private String nombreEpo;
	private String fechaEmision;
	private String titulo;
	private List 	conditions;

	public ConsultaDispersion() {}
	public String getDocumentQuery(){ return null; }
	public String getAggregateCalculationQuery(){ return null; }
	public String getDocumentSummaryQueryForIds(List ids){ return null; }
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){ return null; }
	
	/**
	 * La consulta no requiere paginaci�n
	 * @return qrySentencia
	 */
	public String getDocumentQueryFile(){ 
	
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		log.info("getDocumentQueryFile (E)");	
		try{
			qrySentencia.append(
				"SELECT	FAC.DF_REGISTRO AS FECHA, "																					+
				"		SUM(TOTALOPER) AS NUMERO, "	 			  																		+
				"		SUM(MONTOOPER) AS MONTO_OPERADOS, "																				+
				"		COUNT(FAC.PYME) AS NUMERO_PROVEEDOR, "																			+
				"		SUM(IG_TOTAL_DOCUMENTOS) AS NUMERO_DOC, "																		+
				"		SUM(FN_IMPORTE) AS MONTO_DISPERSADOS "																			+
				"FROM	( SELECT	TO_CHAR(OPER.DF_REGISTRO,'DD/MM/YYYY') AS DF_REGISTRO, " 									+
				"					OPER.TOTALOPER, " 																						+
				"					OPER.MONTOOPER, " 																						+
				"					FF.IG_TOTAL_DOCUMENTOS, " 																				+
				"					FF.FN_IMPORTE, " 																							+
				"					DECODE(FF.IG_TOTAL_DOCUMENTOS,NULL,NULL,OPER.IC_PYME) AS PYME " 							+
				"			FROM	INT_FLUJO_FONDOS FF, " 																					+
				"					(SELECT	TRUNC(S.DF_FECHA_SOLICITUD) AS DF_REGISTRO, " 										+
				"							COUNT(DS.IC_DOCUMENTO) AS TOTALOPER, "														+
				"							SUM(DS.IN_IMPORTE_RECIBIR) AS MONTOOPER, " 												+
				"							D.IC_EPO, " 																						+
				"							D.IC_IF, " 																							+
				"							D.IC_PYME " 																						+
				"					FROM	COM_DOCUMENTO D, COM_DOCTO_SELECCIONADO DS, COM_SOLICITUD S, COMCAT_PYME P "	+
				"					WHERE	DS.IC_DOCUMENTO = D.IC_DOCUMENTO " 															+
				"							AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " 													+
				"							AND D.IC_EPO = DS.IC_EPO " 																	+
				"							AND D.IC_PYME = P.IC_PYME " 																	+
				"							AND DS.IC_IF = ? " 																				+
				"							AND DS.IC_EPO = ? " 																				+
				"							AND D.IC_MONEDA = ? " 																			+
				"							AND D.IC_ESTATUS_DOCTO IN (4, 11) " 														+
				"							AND TRUNC(S.DF_FECHA_SOLICITUD) BETWEEN TO_DATE(?,'DD/MM/YYYY') "					+
				"							AND TO_DATE(?,'DD/MM/YYYY') " 																+
				"				GROUP BY	TRUNC(S.DF_FECHA_SOLICITUD), D.IC_EPO, D.IC_IF, D.IC_PYME ORDER BY 1) OPER "	+
				"			WHERE	FF.IC_EPO(+) = OPER.IC_EPO "																			+
				"					AND FF.IC_IF(+) = OPER.IC_IF " 																		+
				"					AND FF.IC_PYME(+) = OPER.IC_PYME " 																	+
				"					AND TRUNC(FF.DF_REGISTRO(+)) = TRUNC(OPER.DF_REGISTRO) " 									+
				"					AND FF.IC_MONEDA(+) = ? " 																				+
				"		ORDER BY	OPER.DF_REGISTRO) FAC " 																				+
				"GROUP BY FAC.DF_REGISTRO "
			);
			
			conditions.add(	new Integer(this.icIf)			);
			conditions.add(	new Integer(this.icEpo)			);
			conditions.add(	new Integer(this.icMoneda)		);
			conditions.add(	new String(this.fechaInicio)	);
			conditions.add(	new String(this.fechaFin)		);
			conditions.add(	new Integer(this.icMoneda)		);
			
			log.info("Sentencia: "   + qrySentencia.toString());		
			log.info("Condiciones: " + conditions.toString());	
			
		}catch(Exception e){
			log.warn("getDocumentQueryFileException "+e);
		}
		log.info("getDocumentQueryFile (S)");
		
		return qrySentencia.toString(); 
	}

	/**
	 * Genera el archivo PDF
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 * @return 
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){ 
	
		log.info("crearCustomFile (E)");
		
		String nombreArchivo = "";
		String fecha			= "";
		int numero				= 0;
		float monto				= 0;
		int numProv				= 0;
		int numDoc				= 0;
		float montoDisp		= 0;		
		
		if(tipo.equals("PDF")){
		
			HttpSession session  			= request.getSession();
			ComunesPDF pdfDoc    			= new ComunesPDF();
			StringBuffer contenidoArchivo = new StringBuffer();
			CreaArchivo creaArchivo 		= new CreaArchivo();
			OutputStreamWriter writer		= null;
			BufferedWriter buffer 			= null;
	
			try {
				Comunes comunes		= new Comunes();
				nombreArchivo 			= Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc 					= new ComunesPDF(2, path + nombreArchivo);
			
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    	= fechaActual.substring(0,2);
				String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   	= fechaActual.substring(6,10);
				String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText( "	","formas",ComunesPDF.RIGHT);
				
				pdfDoc.addText( "Fecha de Emisi�n: " + this.fechaEmision , "formas", ComunesPDF.RIGHT);
				pdfDoc.addText( "Nombre IF: " 		 + this.nombreIf , 	  "formas", ComunesPDF.LEFT);
				pdfDoc.addText( "Nombre EPO: " 		 + this.nombreEpo , 	  "formas", ComunesPDF.LEFT);
				pdfDoc.addText( "Fecha de incio: " 	 + this.fechaInicio + "          Fecha de t�rmino: " 	+ this.fechaFin , "formas", ComunesPDF.LEFT);
				
				pdfDoc.addText( this.titulo, "formas", ComunesPDF.CENTER);
				
				pdfDoc.setTable(6,80);
				pdfDoc.setCell("Fecha", 						"celda01", ComunesPDF.CENTER, 1, 2);
				pdfDoc.setCell("Documentos Operados",		"celda01", ComunesPDF.CENTER, 2, 1);
				pdfDoc.setCell("Proveedores Dispersados", "celda01", ComunesPDF.CENTER, 3, 1);
				pdfDoc.setCell("N�mero",						"celda01",	ComunesPDF.CENTER);
				pdfDoc.setCell("Monto",							"celda01",	ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Prov.",					"celda01",	ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Doc",						"celda01",	ComunesPDF.CENTER);
				pdfDoc.setCell("Monto",							"celda01",	ComunesPDF.CENTER);
				
				while (rs.next())	{		
					fecha = 		(rs.getString("FECHA") 				== null) ? ""	: rs.getString("FECHA");	
					numero =		(rs.getInt("NUMERO") 				== 0) 	? 0 	: rs.getInt("NUMERO");
					monto =		(rs.getFloat("MONTO_OPERADOS")	== 0) 	? 0 	: rs.getFloat("MONTO_OPERADOS");
					numProv =	(rs.getInt("NUMERO_PROVEEDOR") 	== 0) 	? 0 	: rs.getInt("NUMERO_PROVEEDOR");
					numDoc =		(rs.getInt("NUMERO_DOC") 			== 0) 	? 0 	: rs.getInt("NUMERO_DOC");
					montoDisp = (rs.getFloat("MONTO_DISPERSADOS")== 0) 	? 0 	: rs.getFloat("MONTO_DISPERSADOS");
					
					pdfDoc.setCell(fecha,												"formas",	ComunesPDF.CENTER);			
					pdfDoc.setCell(""+numero,											"formas",	ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),		"formas",	ComunesPDF.RIGHT);
					pdfDoc.setCell(""+numProv,											"formas",	ComunesPDF.CENTER);
					pdfDoc.setCell(""+numDoc,											"formas",	ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDisp,2),	"formas",	ComunesPDF.RIGHT);
					
				}
				pdfDoc.setCell("Total",														"formas",	ComunesPDF.CENTER);			
				pdfDoc.setCell("" + this.numeroTot,										"formas",	ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(this.montoTot,2),		"formas",	ComunesPDF.RIGHT);
				pdfDoc.setCell("" + this.proveedorTot,									"formas",	ComunesPDF.CENTER);
				pdfDoc.setCell("" + this.docTot,											"formas",	ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(this.montoDispTot,2),"formas",	ComunesPDF.RIGHT);
					
				pdfDoc.addTable();	
					
				pdfDoc.addText( "Elabor�: " + this.usuario,	"formas",	ComunesPDF.CENTER);
					
				pdfDoc.endDocument();	
				
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally { try { /*rs.close();*/ } catch(Exception e) {} }		
		
		} else if(tipo.equals("CSV")){

			StringBuffer contenidoArchivo = new StringBuffer();
			CreaArchivo  archivo 			= new CreaArchivo();		
			
			try {
				
				contenidoArchivo.append(" , , , , ,Fecha de emisi�n: "+ this.fechaEmision +" \n");
				contenidoArchivo.append("Nombre IF: "+ this.nombreIf.replace(',',' ') +" \n");
				contenidoArchivo.append("Nombre Epo: "+ this.nombreEpo.replace(',',' ') +" \n");
				contenidoArchivo.append("Fecha de inicio: , , , Fecha de t�rmino:\n");
				contenidoArchivo.append(this.fechaInicio.replace(',',' ') +" , , , "+ this.fechaFin +"  \n");
				contenidoArchivo.append(" ,  \n");
				contenidoArchivo.append(this.titulo.replace(',',' ') + " \n");	
				contenidoArchivo.append(", Documentos, Operados, Proveedores, Dispersados,\n");
				contenidoArchivo.append("Fecha, N�mero, Monto, Num. Prov., Num. Doc, Monto \n");
					
				while(rs.next()){
				
					fecha = 		(rs.getString("FECHA") 				== null) ? ""	: rs.getString("FECHA");	
					numero =		(rs.getInt("NUMERO") 				== 0) 	? 0 	: rs.getInt("NUMERO");
					monto =		(rs.getFloat("MONTO_OPERADOS")	== 0) 	? 0 	: rs.getFloat("MONTO_OPERADOS");
					numProv =	(rs.getInt("NUMERO_PROVEEDOR") 	== 0) 	? 0 	: rs.getInt("NUMERO_PROVEEDOR");
					numDoc =		(rs.getInt("NUMERO_DOC") 			== 0) 	? 0 	: rs.getInt("NUMERO_DOC");
					montoDisp = (rs.getFloat("MONTO_DISPERSADOS")== 0) 	? 0 	: rs.getFloat("MONTO_DISPERSADOS");
				
					contenidoArchivo.append(fecha.replace(',',' ') +",");
					contenidoArchivo.append((""+numero).replace(',',' ') +",");
					contenidoArchivo.append((""+monto).replace(',',' ') +",");
					contenidoArchivo.append((""+numProv).replace(',',' ') +",");
					contenidoArchivo.append((""+numDoc).replace(',',' ') +",");
					contenidoArchivo.append((""+montoDisp).replace(',',' ') +",");			
					contenidoArchivo.append("\n");
				}
				contenidoArchivo.append("Total" +",");
				contenidoArchivo.append((""+this.numeroTot).replace(',',' ') +",");
				contenidoArchivo.append(Comunes.formatoDecimal(this.montoTot,2).replace(',',' ') +",");
				contenidoArchivo.append((""+this.proveedorTot).replace(',',' ') +",");
				contenidoArchivo.append((""+this.docTot).replace(',',' ') +",");
				contenidoArchivo.append(Comunes.formatoDecimal(this.montoDispTot,2).replace(',',' ') +",");			
				contenidoArchivo.append("\n");
				contenidoArchivo.append(" , \n");
				contenidoArchivo.append(" ,Elabor�: "+ this.usuario +",  \n");
				if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
					nombreArchivo="ERROR";
				}	else{
					nombreArchivo = archivo.nombre;
				}
				
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally { try { rs.close(); } catch(Exception e) {} }		
		
		}	

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}
			
/************************************************************
 *								GETTERS ANS SETTERS						*
 ************************************************************/	
	public int getIcIf() {
		return icIf;
	}

	public void setIcIf(int icIf) {
		this.icIf = icIf;
	}

	public int getIcEpo() {
		return icEpo;
	}

	public void setIcEpo(int icEpo) {
		this.icEpo = icEpo;
	}
	
	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public int getIcMoneda() {
		return icMoneda;
	}

	public void setIcMoneda(int icMoneda) {
		this.icMoneda = icMoneda;
	}

	public List getConditions(){
		return conditions; 
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public int getNumeroTot() {
		return numeroTot;
	}

	public void setNumeroTot(int numeroTot) {
		this.numeroTot = numeroTot;
	}

	public int getProveedorTot() {
		return proveedorTot;
	}

	public void setProveedorTot(int proveedorTot) {
		this.proveedorTot = proveedorTot;
	}

	public int getDocTot() {
		return docTot;
	}

	public void setDocTot(int docTot) {
		this.docTot = docTot;
	}

	public double getMontoTot() {
		return montoTot;
	}

	public void setMontoTot(double montoTot) {
		this.montoTot = montoTot;
	}

	public double getMontoDispTot() {
		return montoDispTot;
	}

	public void setMontoDispTot(double montoDispTot) {
		this.montoDispTot = montoDispTot;
	}

	public String getNombreIf() {
		return nombreIf;
	}

	public void setNombreIf(String nombreIf) {
		this.nombreIf = nombreIf;
	}

	public String getNombreEpo() {
		return nombreEpo;
	}

	public void setNombreEpo(String nombreEpo) {
		this.nombreEpo = nombreEpo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
}