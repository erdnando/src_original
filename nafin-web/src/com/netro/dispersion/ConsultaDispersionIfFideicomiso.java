package com.netro.dispersion;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 *	Esta clase se encarga de generar los archivos de la consulta de Dispersion IFs FIDEICOMISO.
 *		PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - IFs FIDEICOMISO
 *	
 *	@author jshernandez
 *	@since  F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 13/09/2013 05:00:09 p.m.
 *	
 */
public class ConsultaDispersionIfFideicomiso {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaDispersionIfFideicomiso.class);
	
	/**
	 *	Crea un archivo CSV con el detalle de los documentos que no se pueden dispersar.
	 *	@throws AppException   
	 *	
	 *	@param directorio             <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 * @param directorioPublicacion 	<tt>String</tt> con la ruta WEB del directorio de publicacion.
	 * @param claveIfFideicomiso     <tt>String</tt> con la clave del IF (COMCAT_IF.IC_IF) que opera 
	 *                               fideicomiso para el desarrollo de proveedores  (COMCAT_IF.CS_OPERA_FIDEICOMSO). 
	 * @param claveEPO				   <tt>String</tt> con la clave de la EPO, como aparece en la tabla (<tt>COMCAT_EPO.IC_EPO</tt>).
	 * @param claveIF					   <tt>String</tt> con la clave del Intermediario Financiero, como aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>).
	 * @param claveMoneda			   <tt>String</tt> con la clave de la Moneda, como aparece en la tabla (<tt>COMCAT_MONEDA.IC_MONEDA</tt>).
	 * @param fechaRegistro 		   <tt>String</tt> con la fecha de registro, en formato: DD/MM/YYYY.
	 * @param session                Objeto <tt>HttpSession</tt> con la sesi�n del usuario.
	 *
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 *
	 */	
	public static String generaArchivoCSVErrores( 
		String 		directorio, 
		String 		directorioPublicacion, 
		String      claveIfFideicomiso,
		String 		claveEPO,
		String 		claveIF, 
		String 		claveMoneda,
		String 		fechaRegistro, 
		HttpSession session  
	) throws AppException {

		log.info("generaArchivoCSVErrores(E)");
		
		FileOutputStream		out 				= null;
		BufferedWriter 		csv 				= null;
		CreaArchivo 			archivo 			= null;
		String					nombreArchivo 	= null;
		
		AccesoDB 				con 				= new AccesoDB();
		String 					querySentencia	= null;
		List						parametros		= null;
		HashMap					query				= null;
		ResultSet 				rs 				= null;
		PreparedStatement 	ps 				= null;
 
		// 1. Obtener instancia de EJB de Dispersion
		Dispersion dispersion = null;
		try {
					
			dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
					
		} catch(Exception e) {
	 
			String msg = "Ocurri� un error al obtener instancia del EJB de Dispersi�n";
			log.error(msg);
			e.printStackTrace();
			
			throw new AppException(msg);
	 
		}
	
		try {
 
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".csv";
			out 				= new FileOutputStream(directorio+nombreArchivo);
			csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
 
			// Conectarse a la Base de Datos
			con.conexionDB();
			
			// Obtener query 
			query				= dispersion.getQueryDetalleErrorDispersionIfFideicomiso( claveIfFideicomiso, claveEPO, claveIF, claveMoneda, fechaRegistro );
			querySentencia	= (String) query.get("text");
			// Preparar Query
			ps 				= con.queryPrecompilado(querySentencia);
			// Definir los parametros
			int indice  	= 1;
			parametros		= (List)	query.get("parameters");	
			for(int i=0;i<parametros.size();i++){
				Object o = (Object) parametros.get(i);
				if(        o instanceof String   ){
					ps.setString(indice++, (String)   o );
				} else if( o instanceof Long     ){
					ps.setLong(indice++,   ((Long)    o ).longValue() );
				} else if( o instanceof Integer  ){
					ps.setInt(indice++,    ((Integer) o ).intValue() );
				}
			}
			// Realizar consulta
			rs = ps.executeQuery();

			// Variables asociadas a la consulta
			String 		rfc 					= null; // RFC  
			String 		nombreProveedor	= null; // Nombre del Proveedor 
			String 		numeroDocumento	= null; // N�mero de Documento
			String 		importeDocumento	= null; // Importe del Documento 
			String		banco 				= null; // Clave del banco de la PYME
			String 		tipoCuenta 			= null; // Tipo de Cuenta 
			String 		cuentaClabeSwift	= null; // Cuenta CLABE / SWIFT
			String 		estatusCecoban 	= null; // Estatus Cecoban
			
			// Agregar t�tulo	
			csv.write("RFC,Nombre del Proveedor,N�mero de Documento,Importe del Documento,Banco,Tipo de Cuenta,Cuenta CLABE / SWIFT\n");

			// Agregar registros
			boolean hayRegistrosConError = false;
			int 	  ctaRegistros 		  = 0;
			while(rs.next()){

				// Leer parametros del registro
				rfc 					= (rs.getString("cg_rfc")					== null)?"":rs.getString("cg_rfc");
				nombreProveedor 	= (rs.getString("cg_razon_social")		== null)?"":rs.getString("cg_razon_social");
				numeroDocumento 	= (rs.getString("ig_numero_docto")		== null)?"":rs.getString("ig_numero_docto");
				importeDocumento 	= (rs.getString("in_importe_recibir")	== null)?"":rs.getString("in_importe_recibir");
				banco 				= (rs.getString("ic_bancos_tef")			== null)?"":rs.getString("ic_bancos_tef");
				tipoCuenta 			= (rs.getString("ic_tipo_cuenta")		== null)?"":rs.getString("ic_tipo_cuenta");
				cuentaClabeSwift 	= (rs.getString("cg_cuenta")				== null)?"":rs.getString("cg_cuenta");
				estatusCecoban 	= (rs.getString("ic_estatus_cecoban")	== null)?"":rs.getString("ic_estatus_cecoban");

				// Solo mostrar el detalle de los documentos que no se pueden dispersar
				if( "99".equals( estatusCecoban ) ){
					continue;
				}
				
				// Actualizar bandera de registros con error
				hayRegistrosConError = true;

				// Actualizar conteo de registros
				++ctaRegistros;
				
				csv.write("\"");	csv.write(rfc.replaceAll("\"","\"\""));					csv.write("\",");
				csv.write("\"");	csv.write(nombreProveedor.replaceAll("\"","\"\""));	csv.write("\",");
				csv.write("\"");	csv.write(numeroDocumento.replaceAll("\"","\"\""));	csv.write("\",");
				csv.write("\"");	csv.write(importeDocumento.replaceAll("\"","\"\""));	csv.write("\",");
				csv.write("\"");	csv.write(banco.replaceAll("\"","\"\""));					csv.write("\",");
				csv.write("\"");	csv.write(tipoCuenta.replaceAll("\"","\"\""));			csv.write("\",");
				csv.write("\"");	csv.write(cuentaClabeSwift.replaceAll("\"","\"\""));	csv.write("\",");
				csv.write("\n");
				 
				// Guardar en disco cada 256 registros
				if( ctaRegistros == 256 ){ 
					csv.flush(); 
					ctaRegistros = 0;
				}
				
			}

			// Agregar mensaje en caso no se haya encontrado ning�n registro
			if( !hayRegistrosConError ){
				csv.write( "No se encontr� ning�n registro." );
				csv.write("\n");
			}
 
		} catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoCSVErrores(OutOfMemoryError)");
			log.error("generaArchivoCSVErrores.message                = <" + om.getMessage()        + ">");
			log.error("generaArchivoCSVErrores.freeMemory             = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error("generaArchivoCSVErrores.directorio             = <" + directorio             + ">");
			log.error("generaArchivoCSVErrores.directorioPublicacion  = <" + directorioPublicacion  + ">");
			log.error("generaArchivoCSVErrores.claveIfFideicomiso 	 = <" + claveIfFideicomiso     + ">");
			log.error("generaArchivoCSVErrores.claveEPO               = <" + claveEPO               + ">");
			log.error("generaArchivoCSVErrores.claveIF                = <" + claveIF                + ">");
			log.error("generaArchivoCSVErrores.claveMoneda            = <" + claveMoneda            + ">");
			log.error("generaArchivoCSVErrores.fechaRegistro          = <" + fechaRegistro          + ">");
			log.error("generaArchivoCSVErrores.session                = <" + session                + ">"); 
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoCSVErrores(Exception)");
			log.error("generaArchivoCSVErrores.message                = <" + e.getMessage()         + ">");
			log.error("generaArchivoCSVErrores.directorio             = <" + directorio             + ">");
			log.error("generaArchivoCSVErrores.directorioPublicacion  = <" + directorioPublicacion  + ">");
			log.error("generaArchivoCSVErrores.claveIfFideicomiso 	 = <" + claveIfFideicomiso     + ">");
			log.error("generaArchivoCSVErrores.claveEPO               = <" + claveEPO               + ">");
			log.error("generaArchivoCSVErrores.claveIF                = <" + claveIF                + ">");
			log.error("generaArchivoCSVErrores.claveMoneda            = <" + claveMoneda            + ">");
			log.error("generaArchivoCSVErrores.fechaRegistro          = <" + fechaRegistro          + ">");
			log.error("generaArchivoCSVErrores.session                = <" + session                + ">");
			e.printStackTrace(); 
			
			throw new AppException("Ocurri� un error al generar el CSV con el detallade de los errores.");
			
		} finally {
			
			if(rs  != null ){ try { rs.close(); }catch(Exception e){} }
			if(ps  != null ){ try { ps.close(); }catch(Exception e){} }
			
			// Cerrar archivo
			if(csv != null ){ try{ csv.close(); }catch(Exception e){} }
			
			// Cerrar conexion con la base de datos
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}

			log.info("generaArchivoCSVErrores(S)");
			
		}
		
		return nombreArchivo;
		
	}
	
	/**
	 *	Crea un archivo PDF con el detalle de los documentos que se van a dispersar, como aquellos que no.
	 *	@throws AppException   
	 *	
	 *	@param directorio             <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 * @param directorioPublicacion 	<tt>String</tt> con la ruta WEB del directorio de publicacion.
	 * @param claveIfFideicomiso     <tt>String</tt> con la clave del IF (COMCAT_IF.IC_IF) que opera 
	 *                               fideicomiso para el desarrollo de proveedores  (COMCAT_IF.CS_OPERA_FIDEICOMSO). 
	 * @param claveEPO				   <tt>String</tt> con la clave de la EPO, como aparece en la tabla (<tt>COMCAT_EPO.IC_EPO</tt>).
	 * @param claveIF					   <tt>String</tt> con la clave del Intermediario Financiero, como aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>).
	 * @param claveMoneda			   <tt>String</tt> con la clave de la Moneda, como aparece en la tabla (<tt>COMCAT_MONEDA.IC_MONEDA</tt>).
	 * @param fechaRegistro 		   <tt>String</tt> con la fecha de registro, en formato: DD/MM/YYYY.
	 * @param session                Objeto <tt>HttpSession</tt> con la sesi�n del usuario.
	 *
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 *
	 */	
	public static String generaArchivoPDF( 
		String 		directorio, 
		String 		directorioPublicacion, 
		String      claveIfFideicomiso,
		String 		claveEPO,
		String 		claveIF,
		String 		claveMoneda,
		String 		fechaRegistro, 
		HttpSession session  
	) throws AppException {

		log.info("generaArchivoPDF(E)");
		
		CreaArchivo	archivo 			= null;
		String		nombreArchivo	= null;
		ComunesPDF	pdf 				= null;
 
		// 1. Obtener instancia de EJB de Dispersion
		Dispersion dispersion = null;
		try {
					
			dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
					
		} catch(Exception e) {
	 
			String msg = "Ocurri� un error al obtener instancia del EJB de Dispersi�n";
			log.error(msg);
			e.printStackTrace();
			
			throw new AppException(msg);
	 
		}
	
		try {
 
			// Realizar consulta
			HashMap	consulta 			= dispersion.consultaDispersionIfFideicomiso( claveIfFideicomiso, claveEPO, claveIF, claveMoneda, fechaRegistro);
			List 		registros			= (List) 	consulta.get("registros");
			List		registrosTotales	= (List) 	consulta.get("registrosTotales");
			HashMap	detalleTotales		= (HashMap) consulta.get("detalleTotales");
	
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".pdf";
 
			// Sacando la fecha para encabezado de Excel
			String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaActual.substring(0,2);
			String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   	= fechaActual.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			String strEncabezado = "M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + 
										  " ----------------------------- " + horaActual;
 
			// Crear Documento PDF
			pdf = new ComunesPDF(1, directorio+nombreArchivo, "P�gina " );
			pdf.encabezadoConImagenes(
				pdf,
				(String)session.getAttribute("strPais"),
				(String)session.getAttribute("iNoNafinElectronico"),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				directorioPublicacion
			);
 
			pdf.addText(strEncabezado, "formas", ComunesPDF.RIGHT );
			pdf.addText(" ",           "formasG",ComunesPDF.CENTER);
			
			// Variables asociadas a la consulta
			String 		rfc 					= null; // RFC  
			String 		nombreProveedor	= null; // Nombre del Proveedor 
			String 		totalDocumentos	= null; // Total de Documentos
			String 		moneda 				= null; // Moneda 
			String 		monto 				= null; // Monto 
			String		banco 				= null; // Banco 
			String 		tipoCuenta 			= null; // Tipo de Cuenta 
			String 		numeroCuenta 		= null; // N�mero de Cuenta
			String 		error					= null; // Error de los Documentos
 
			List titulosReporte = new ArrayList();
			titulosReporte.add("RFC");
			titulosReporte.add("Nombre del Proveedor");
			titulosReporte.add("Total de Documentos");
			titulosReporte.add("Moneda");
			titulosReporte.add("Monto");
			titulosReporte.add("Banco");
			titulosReporte.add("Tipo de Cuenta");
			titulosReporte.add("N�mero de Cuenta");
			titulosReporte.add("Error");
 
			// Agregar t�tulo
			pdf.setTable(titulosReporte, "celda01", 90, new float[]{8.33f,15.95f,9.52f,8.81f,17.86f,5.95f,5.95f,17.86f,10.48f});
			
			// Agregar registros
			for(int i=0,ctaRegistros = 0;i<registros.size();i++,ctaRegistros++){
				
				HashMap  registro = (HashMap) registros.get(i);
	
				rfc 					= (String) registro.get("RFC");
				nombreProveedor 	= (String) registro.get("NOMBRE_PROVEEDOR");
				totalDocumentos 	= (String) registro.get("TOTAL_DOCUMENTOS");
				moneda 				= (String) registro.get("MONEDA");
				monto 				= (String) registro.get("MONTO");
				banco 				= (String) registro.get("BANCO");
				tipoCuenta 			= (String) registro.get("TIPO_CUENTA");
				numeroCuenta 		= (String) registro.get("NUMERO_CUENTA");
				error 				= (String) registro.get("ERROR");
				
				rfc 					= rfc 				== null?"":rfc;
				nombreProveedor 	= nombreProveedor == null?"":nombreProveedor;
				totalDocumentos 	= totalDocumentos == null?"":totalDocumentos;
				moneda 				= moneda 			== null?"":moneda;
				monto 				= monto 				== null?"":monto;
				banco 				= banco 				== null?"":banco;
				tipoCuenta 			= tipoCuenta 		== null?"":tipoCuenta;
				numeroCuenta 		= numeroCuenta 	== null?"":numeroCuenta;
				error 				= error 				== null?"":error;
				
				// Agregar contenido del registro
				pdf.setCell( rfc, 				"formas", ComunesPDF.CENTER );
				pdf.setCell( nombreProveedor, "formas", ComunesPDF.CENTER );
				pdf.setCell( totalDocumentos, "formas", ComunesPDF.CENTER );
				pdf.setCell( moneda, 			"formas", ComunesPDF.CENTER );
				pdf.setCell( monto, 				"formas", ComunesPDF.RIGHT  );
				pdf.setCell( banco, 				"formas", ComunesPDF.CENTER );
				pdf.setCell( tipoCuenta, 		"formas", ComunesPDF.CENTER );
				pdf.setCell( numeroCuenta, 	"formas", ComunesPDF.CENTER );
				pdf.setCell( error, 				"formas", ComunesPDF.CENTER );
 
			}
			
			// Agregar registros totales
			for(int i=0,ctaRegistros = 0;i<registrosTotales.size();i++,ctaRegistros++){
				
				HashMap registro = (HashMap) registrosTotales.get(i);
				
				rfc 					= (String) registro.get("RFC");
				nombreProveedor 	= (String) registro.get("NOMBRE_PROVEEDOR");
				totalDocumentos 	= (String) registro.get("TOTAL_DOCUMENTOS");
				moneda 				= (String) registro.get("MONEDA");
				monto 				= (String) registro.get("MONTO");
				banco 				= (String) registro.get("BANCO");
				tipoCuenta 			= (String) registro.get("TIPO_CUENTA");
				numeroCuenta 		= (String) registro.get("NUMERO_CUENTA");
				error 				= (String) registro.get("ERROR");
				
				rfc 					= rfc 				== null?"":rfc;
				nombreProveedor 	= nombreProveedor == null?"":nombreProveedor;
				totalDocumentos 	= totalDocumentos == null?"":totalDocumentos;
				moneda 				= moneda 			== null?"":moneda;
				monto 				= monto 				== null?"":monto;
				banco 				= banco 				== null?"":banco;
				tipoCuenta 			= tipoCuenta 		== null?"":tipoCuenta;
				numeroCuenta 		= numeroCuenta 	== null?"":numeroCuenta;
				error 				= error 				== null?"":error;
				
				// Agregar contenido del registro
				pdf.setCell( rfc, 			   "celda01",  ComunesPDF.RIGHT,  2 );
				// pdf.setCell( nombreProveedor, "formas", ComunesPDF.CENTER );
				pdf.setCell( totalDocumentos, "formas", 	ComunesPDF.CENTER    );
				pdf.setCell( "", 					"celda01", 	ComunesPDF.CENTER    ); 
				// pdf.setCell( moneda, 			"formas", 	ComunesPDF.CENTER ); 
				pdf.setCell( monto, 				"formas", 	ComunesPDF.RIGHT     );
				pdf.setCell( "", 					"celda01", 	ComunesPDF.CENTER, 4 );
				// pdf.setCell( banco, 				"formas", 	ComunesPDF.CENTER );
				// pdf.setCell( tipoCuenta, 		"formas", 	ComunesPDF.CENTER );
				// pdf.setCell( numeroCuenta, 	"formas", 	ComunesPDF.CENTER );
				// pdf.setCell( error, 				"formas", 	ComunesPDF.CENTER );
				
			}
			
			// Agregar totales
			if( registros.size() == 0 ){
				pdf.setCell( "No se encontr� ning�n registro", 	"celda01", ComunesPDF.CENTER, 9 );
			}
 	
			pdf.addTable();
					
			pdf.endDocument();
 
		} catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoPDF(OutOfMemoryError)");
			log.error("generaArchivoPDF.message                = <" + om.getMessage()        + ">");
			log.error("generaArchivoPDF.freeMemory             = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error("generaArchivoPDF.directorio             = <" + directorio             + ">");
			log.error("generaArchivoPDF.directorioPublicacion  = <" + directorioPublicacion  + ">");
			log.error("generaArchivoPDF.claveIfFideicomiso     = <" + claveIfFideicomiso     + ">"); 
			log.error("generaArchivoPDF.claveEPO               = <" + claveEPO               + ">");
			log.error("generaArchivoPDF.claveIF                = <" + claveIF						+ ">");
			log.error("generaArchivoPDF.claveMoneda            = <" + claveMoneda            + ">");
			log.error("generaArchivoPDF.fechaRegistro          = <" + fechaRegistro          + ">"); 
			log.error("generaArchivoPDF.session                = <" + session                + ">"); 
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoPDF(Exception)");
			log.error("generaArchivoPDF.message                = <" + e.getMessage()         + ">");
			log.error("generaArchivoPDF.directorio             = <" + directorio             + ">");
			log.error("generaArchivoPDF.directorioPublicacion  = <" + directorioPublicacion  + ">");
			log.error("generaArchivoPDF.claveIfFideicomiso     = <" + claveIfFideicomiso     + ">");
			log.error("generaArchivoPDF.claveEPO               = <" + claveEPO               + ">");
			log.error("generaArchivoPDF.claveIF                = <" + claveIF						+ ">"); 
			log.error("generaArchivoPDF.claveMoneda            = <" + claveMoneda            + ">");
			log.error("generaArchivoPDF.fechaRegistro          = <" + fechaRegistro          + ">"); 
			log.error("generaArchivoPDF.session                = <" + session                + ">");
			e.printStackTrace(); 
			
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n detallada.");
			
		} finally {
 
			log.info("generaArchivoPDF(S)");
			
		}
		
		return nombreArchivo;
		
	}
	
}