package com.netro.dispersion;

import com.netro.descuento.CargaDocumentoBean;
import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;

import java.io.File;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class NotificaDispersionEPO {
 
	//Variable para enviar mensajes al log.
	private static final Log log  = ServiceLocator.getInstance().getLog(NotificaDispersionEPO.class);
	
	/** 
	 *
	 * Construye el mensaje con el resumen de dispersion a ser enviado por correo.
	 * @throws AppException
	 *
	 * @param claveEPO              <tt>String</tt> con la clave de la EPO ( COMCAT_EPO.IC_EPO )
	 * @param fechaReporte          <tt>String</tt> con la fecha del reporte, en formato: DD/MM/YYYY.
	 * @param session               Objeto <tt>HttpSession</tt> con la sesi�n del usuario.
	 * @param reporteEPO            Objeto <tt>RepDispEpo</tt> con el detalle de los documentos cuyos totales ser�n notificados y/o enlistados.
	 * @param directorioPublicacion <tt>String</tt> con el directorio donde se encuentran los recursos web.
	 * @param directorioTemporal    <tt>String</tt> con el directorio temporal donde se crear�n los archivos que seran enviados por correo.
	 * @param nombreDelUsuario      <tt>String</tt> con el nombre del usuario.
	 * @param correoUsuario         <tt>String</tt> con el correo del usuario que realiza las notificaciones.
	 *
	 * @return <tt>HashMap</tt> con el detalle del mensaje.
	 *
	 * @author  jshernandez
	 * @since   F012 - 2013; 03/06/2013 11:09:41 a.m.
	 *
	 */
	public HashMap construyeMensaje( 
		String     	claveEPO, 
		String     	fechaReporte, 
		HttpSession	session,
		RepDispEpo 	reporteEPO, 
		String     	directorioPublicacion, 
		String     	directorioTemporal,
		String     	nombreDelUsuario,
		String     	correoUsuario
	) throws AppException {	
	  
		log.info("construyeMensaje(E)");
		
		HashMap mensaje = new HashMap();
		
		try {
			DispersionBean			dispersionBean			= new DispersionBean();
			CargaDocumentoBean	CargaDocumentos 		= new CargaDocumentoBean();
			
			// Obtener Nombre de la EPO
			String 					razonSocialEPO			= dispersionBean.getNombreEPO(claveEPO);

			// PREPARAR MENSAJE
			
			// Obtener mensaje de dispersion
			String 					mensaje_correo			= dispersionBean.getMensajeDedispersion();
			if(mensaje_correo.length() > 3072){
				mensaje_correo = mensaje_correo.substring(0,3072);
			}
		
			// Obtener dias inhabiles adicionales
			String 		diasInhabilesPorEPO 						= CargaDocumentos.getDiasInhabiles(claveEPO);
			String 		numFechaADepositar 						= Fecha.getDiaHabilAnterior(fechaReporte,"dd/MM/yyyy",diasInhabilesPorEPO);
		
			// Obtener parametros del cuerpo del mensaje
			HashMap		datosIntermediarios						= getListaDeIntermediariosConOperacion(reporteEPO);
			
			String 		montoDocumentosNoOperados				= getMontoDocumentosNoOperados(reporteEPO);
			String 		fechaDeVencimiento						= Fecha.getFechaConNombreDelMes(fechaReporte);
			String 		fechaADepositar							= Fecha.getFechaConNombreDelMes(numFechaADepositar);
			String 		totalNoDispersado							= getTotalNoDispersado(reporteEPO);
			ArrayList	listaDeIntermediarios 					= (ArrayList) datosIntermediarios.get("LISTA_BANCOS");
			String 		montoTotalADispersar						= "0.00";
			
			ArrayList	listaDeIntermediariosUSD 				= (ArrayList) datosIntermediarios.get("LISTA_BANCOS_USD");
			String 		montoDocumentosNoOperadosUSD			= getMontoDocumentosNoOperadosUSD(reporteEPO);
			String 		montoTotalADispersarUSD					= "0.00"; 
			String 		totalNoDispersadoUSD						= getTotalNoDispersadoUSD(reporteEPO); 
			
			String 		totalIntermediariosConOperacion		= "0.00";
			String 		totalIntermediariosConOperacionUSD  = "0.00";
			
			// Moneda Nacional
			BigDecimal 	monto	= new BigDecimal((String) datosIntermediarios.get("MONTO_TOTAL"));
			monto					= monto.add(new BigDecimal(montoDocumentosNoOperados));
			
			montoTotalADispersar 			= "$ " + Comunes.formatoDecimal(monto.toPlainString(),2,true); 
			montoDocumentosNoOperados		= "$ " + Comunes.formatoDecimal(montoDocumentosNoOperados,2,true);
			totalNoDispersado					= "$ " + Comunes.formatoDecimal(totalNoDispersado,2,true);
			
			// Dolares Americanos
			BigDecimal 	montoUSD	= new BigDecimal((String) datosIntermediarios.get("MONTO_TOTAL_USD"));
			montoUSD					= montoUSD.add(new BigDecimal(montoDocumentosNoOperadosUSD));
			
			montoTotalADispersarUSD 		= "$ " + Comunes.formatoDecimal(montoUSD.toPlainString(),2,true);
			montoDocumentosNoOperadosUSD	= "$ " + Comunes.formatoDecimal(montoDocumentosNoOperadosUSD,2,true);
			totalNoDispersadoUSD				= "$ " + Comunes.formatoDecimal(totalNoDispersadoUSD,2,true);
			
			// Cacular totales de las listas de intermediarios con operacion
			totalIntermediariosConOperacion		=  "$ " + Comunes.formatoDecimal((String) datosIntermediarios.get("MONTO_TOTAL"),2,true);
			totalIntermediariosConOperacionUSD  =  "$ " + Comunes.formatoDecimal((String) datosIntermediarios.get("MONTO_TOTAL_USD"),2,true);
			
			// PREPARAR ARCHIVOS DE DISPERSION
 
			String		archivoPDF			= "";
			String		archivoXLS			= "";
			
			// Nuevo nombre del archivo
			String 		nombreArchivo		= "DISPERSION " + razonSocialEPO + " " + fechaReporte;
			nombreArchivo						= nombreArchivo.replaceAll(" ","_");
			nombreArchivo						= escapaCaracteresEspeciales(nombreArchivo);
			// Filtra caracteres que no son permitidos en windows: \ / : * ? " < > |
			nombreArchivo						= nombreArchivo.replaceAll("[\\\\/:\\*\\?\"<>\\|]","");
			
			String 		tamanoArchivoPDF 	= "0 KB";
			String 		tamanoArchivoXLS 	= "0 KB";
			String 		nombreArchivoPDF 	= "";
			String 		nombreArchivoXLS 	= "";
		
			// Generar Archivo
		
			String tipoArchivo = "A";
			//Determinar tipos de archivo a enviar
			if(claveEPO != null && !claveEPO.trim().equals("")){
				tipoArchivo = dispersionBean.getTipoArchivoAEnviar(claveEPO);
			}

			List listaReportesEPO = new ArrayList();
			listaReportesEPO.add(reporteEPO);
			if(tipoArchivo.equals("P") || tipoArchivo.equals("A")){
				generaArchivoAdjuntoPDF(listaReportesEPO, session, directorioTemporal, directorioPublicacion, nombreArchivo, dispersionBean );
				nombreArchivoPDF = nombreArchivo + ".pdf";
				archivoPDF = directorioTemporal + nombreArchivoPDF;
				tamanoArchivoPDF = getFileSize(archivoPDF) + " KB";
			}
			if(tipoArchivo.equals("E") || tipoArchivo.equals("A")){
				generaArchivoAdjuntoXLS(listaReportesEPO, session, directorioTemporal, directorioPublicacion, nombreArchivo, dispersionBean );
				nombreArchivoXLS = nombreArchivo + ".xls";
				archivoXLS = directorioTemporal + nombreArchivoXLS;
				tamanoArchivoXLS = getFileSize(archivoXLS) + " KB";		
			}
			
		
			// Configurar destinatarios 
		
			String nombreRemitente 			= nombreDelUsuario;
			String proveedor		  			= "";
			String listaDeCuentasNAFIN		= "";
			String listaDeCuentasEPO		= "";
			String []cuentasDeCorreoNAFIN	= null;
			String []cuentasDeCorreoEPO	= null;
				
			String asunto						= "DISPERSION " +  razonSocialEPO + " " + fechaReporte ;
			if(claveEPO != null && !claveEPO.equals("")){
				
				HashMap parametros 	= dispersionBean.getParametrosDeDispersion(claveEPO);
				
				proveedor 					= (String)    parametros.get("CLAVE_PYME");
				cuentasDeCorreoNAFIN		= (String []) parametros.get("CUENTAS_NAFIN");
				cuentasDeCorreoEPO		= (String []) parametros.get("CUENTAS_EPO");
				
				listaDeCuentasNAFIN	= getListaDeDirecciones(cuentasDeCorreoNAFIN);
				listaDeCuentasEPO		= getListaDeDirecciones(cuentasDeCorreoEPO);
			}
		
			// Convertir Texto a Codigo HTML 
		
			String 				mensajeWeb 	= "";
			TraductorDeCorreo traductor 	= new TraductorDeCorreo();
			
			traductor.setTotalIntermediariosConOperacion(totalIntermediariosConOperacion); 
			traductor.setTotalIntermediariosConOperacionUSD(totalIntermediariosConOperacionUSD);
			
			traductor.setRazonSocialEPO(razonSocialEPO);
			traductor.setListaDeIntermediariosConOperacion(listaDeIntermediarios);
			traductor.setMontoDocumentosNoOperados(montoDocumentosNoOperados);
			traductor.setTotalADispersar(montoTotalADispersar);
			traductor.setFechaDeVencimiento(fechaDeVencimiento);
			traductor.setFechaADepositar(fechaADepositar);
			traductor.setTotalNoDispersado(totalNoDispersado);
			
			traductor.setListaDeIntermediariosConOperacionUSD(listaDeIntermediariosUSD);
			traductor.setMontoDocumentosNoOperadosUSD(montoDocumentosNoOperadosUSD);
			traductor.setTotalADispersarUSD(montoTotalADispersarUSD);
			traductor.setTotalNoDispersadoUSD(totalNoDispersadoUSD);
			
			mensajeWeb 	= traductor.convierteEnCodigoHTML(mensaje_correo);
			
		  // Generar el Codigo HTML que se utilizara para enviar el correo --%>
		
			// Agregar remitente
			String remitente 			= correoUsuario;
			
			// Agregar destinarios
			String destinatarios 	= traductor.arrayToString(cuentasDeCorreoEPO,",");
			
			// Agregar copia de carbon para los siguientes destinatarios
			String ccDestinatarios	= traductor.arrayToString(cuentasDeCorreoNAFIN,",");
			
			// Agregar titulo del mensaje
			String tituloMensaje		= asunto ;
			
			// Agregar mensaje
			traductor.setLogo("<img src=\"cid:nafin_id\" height=\"65\" width=\"175\" alt=\"\" border=\"0\">");
			String mensajeCorreo 	= 	"<html>															"  +
												"       <head>													"  +
												"               <title>"+tituloMensaje+"</title>	"  +
												"       </head>												"  +
												"       <body>													"	+
												traductor.convierteEnCodigoHTML(mensaje_correo) 		+
												"	     </body>												" 	+
												"</html>															";
		
			// Agregar Archivos de Imagenes 										
			ArrayList 		listaDeImagenes	= new ArrayList();
		
			HashMap 			imagenNafin 		= new HashMap();
			imagenNafin.put("FILE_FULL_PATH",	directorioPublicacion + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif");
			imagenNafin.put("FILE_ID","nafin_id");
		
			listaDeImagenes.add(imagenNafin);
			
			// Agregar archivo adjunto
			ArrayList 		listaDeArchivos		 = new ArrayList();
		
			HashMap 			archivoAdjunto 		 = null;
			if(tipoArchivo.equals("A") || tipoArchivo.equals("P")){
				archivoAdjunto 		 = new HashMap();
				archivoAdjunto.put("FILE_FULL_PATH", archivoPDF );//applicationPath + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif");
				listaDeArchivos.add(archivoAdjunto);
			}
			if(tipoArchivo.equals("A") || tipoArchivo.equals("E")){
				archivoAdjunto 		 = new HashMap();
				archivoAdjunto.put("FILE_FULL_PATH", archivoXLS );
				listaDeArchivos.add(archivoAdjunto);		
			}
			
			// Enviar resultados
			mensaje.put("remitente",			remitente		 );
			mensaje.put("destinatarios",		destinatarios	 );
			mensaje.put("ccDestinatarios",	ccDestinatarios );
			mensaje.put("tituloMensaje",		tituloMensaje	 );
			mensaje.put("mensajeCorreo",		mensajeCorreo	 );
			mensaje.put("listaDeImagenes",	listaDeImagenes );
			mensaje.put("listaDeArchivos",	listaDeArchivos );
			// Parametros adicionales para su vista en Web
			mensaje.put("mensajeWeb",			mensajeWeb		 );
			mensaje.put("tipoArchivo",			tipoArchivo		 );
			mensaje.put("listaDeCuentasNAFIN", listaDeCuentasNAFIN );
			mensaje.put("listaDeCuentasEPO",   listaDeCuentasEPO   );
			if(tipoArchivo.equals("P") || tipoArchivo.equals("A")){
				mensaje.put("nombreArchivoPDF",nombreArchivoPDF);
				mensaje.put("tamanoArchivoPDF",tamanoArchivoPDF);
			}
			if(tipoArchivo.equals("E") || tipoArchivo.equals("A")){
				mensaje.put("nombreArchivoXLS",nombreArchivoXLS);
				mensaje.put("tamanoArchivoXLS",tamanoArchivoXLS);
			}
			
		} catch(Exception e){
 
			log.error("construyeMensaje(Exception)");
			log.error("construyeMensaje.claveEPO              = <" + claveEPO              + ">"); 
			log.error("construyeMensaje.fechaReporte          = <" + fechaReporte          + ">"); 
			log.error("construyeMensaje.session               = <" + session               + ">");
			log.error("construyeMensaje.reporteEPO            = <" + reporteEPO            + ">"); 
			log.error("construyeMensaje.directorioPublicacion = <" + directorioPublicacion + ">"); 
			log.error("construyeMensaje.directorioTemporal    = <" + directorioTemporal    + ">");
			log.error("construyeMensaje.nombreDelUsuario      = <" + nombreDelUsuario      + ">");
			log.error("construyeMensaje.correoUsuario         = <" + correoUsuario         + ">"); 
			e.printStackTrace();
			
			throw new AppException("Ocurri� un error al preparar mensaje de notificaci�n.");
 
		} finally {
			log.info("construyeMensaje(S)");
		}
		
		return mensaje;
		
	}
 
	 public String getFileSize(String file){
		 
	 	 log.info("getFileSize(E)");
		 File 		f				= new File(file);
		 BigDecimal size			= new BigDecimal(f.length());
		 BigDecimal resultado	= size.divide(new BigDecimal("1024"),0,BigDecimal.ROUND_HALF_UP);
		 
		 log.info("getFileSize(S)");
		 return resultado.toPlainString();
		 
	 }
	 
	 public String getListaDeDirecciones(String []lineas){
		StringBuffer buffer = new StringBuffer();
		
		if(lineas 		== null) return null;
		
		for(int i=0;i<lineas.length;i++){
			buffer.append("'" + lineas[i] + "'");
			if( i < (lineas.length-1) ) { buffer.append("; ");}
		}
		
		return buffer.toString();
	}
	
	 public void renameFile( String directorioTemporal, String fileName, String newName ) 
	 	throws Exception {
		 
	 	 log.info("renameFile(E)");
	 	 
		 File oldFile = null;
		 
		 try{
			 
			 oldFile = new File(directorioTemporal,fileName); 
			 oldFile.renameTo(new File(directorioTemporal,newName));
			 
		 }catch(Exception e){
			 
			 log.error("renameFile(Exception)");
			 log.error("renameFile.directorioTemporal = <" + directorioTemporal + ">");
			 log.error("renameFile.fileName      = <" + fileName      + ">");
			 log.error("renameFile.newName       = <" + newName       + ">");
			 e.printStackTrace();
			 
			 throw e;
			 
		 } finally{
		 	 
		 	 log.info("renameFile(S)");
		 	 
		 }
	
	 }
 
 	public String getMontoDocumentosNoOperados(RepDispEpo repDispEpo){
		
		log.info("getMontoDocumentosNoOperados(E)");
		
		String 	montoTotal 	= "0.00";
		String   claveMoneda = "";
			
		if( repDispEpo != null ){
 				
			Registros 	reporte 		= repDispEpo.getRepPymesConDisp();
			BigDecimal	rs_monto		= new BigDecimal(0);
			BigDecimal	totMonto 	= new BigDecimal(0);
				
			while(reporte.next()) {
				
				claveMoneda = reporte.getString("rs_ic_moneda");
				if(claveMoneda == null || !claveMoneda.equals("1")){	continue;}
				
				rs_monto 	= "".equals(reporte.getString("Monto Total"))?
									new BigDecimal(0):new BigDecimal(reporte.getString("Monto Total"));
				totMonto		= totMonto.add(rs_monto);
				
			}
				
			montoTotal = Comunes.formatoDecimal(totMonto.toPlainString(), 2,false);
			
		}
		
		log.info("getMontoDocumentosNoOperados(S)");	
		return montoTotal;
		
	}
	
	 public String getMontoDocumentosNoOperadosUSD(RepDispEpo repDispEpo){
		
		log.info("getMontoDocumentosNoOperadosUSD(E)");

		String 	montoTotal 	= "0.00";
		String   claveMoneda = "";
			
		if(repDispEpo != null) {
			
			Registros 	reporte 		= repDispEpo.getRepPymesConDisp();
			BigDecimal	rs_monto		= new BigDecimal(0);
			BigDecimal	totMonto 	= new BigDecimal(0);
				
			while(reporte.next()) {
				
				claveMoneda = reporte.getString("rs_ic_moneda");
				if(claveMoneda == null || !claveMoneda.equals("54")){	continue;}
				
				rs_monto 	= "".equals(reporte.getString("Monto Total"))?
									new BigDecimal(0):new BigDecimal(reporte.getString("Monto Total"));
				totMonto		= totMonto.add(rs_monto);
				
			}
				
			montoTotal = Comunes.formatoDecimal(totMonto.toPlainString(), 2,false);
			
		}
		
		log.info("getMontoDocumentosNoOperadosUSD(S)");	
		return montoTotal;
		
	}

	public String getTotalNoDispersado(RepDispEpo repDispEpo){
		
		log.info("getTotalNoDispersado(E)");
 
		String 	montoTotal 	= "0.00";
		String 	claveMoneda = "";
			
		if( repDispEpo != null ){
 				
			Registros 	reporte 		= repDispEpo.getRepPymesSinDisp();
			BigDecimal	rs_monto		= new BigDecimal(0);
			BigDecimal	totMonto 	= new BigDecimal(0);
				
			while(reporte.next()) {
				claveMoneda = reporte.getString("rs_ic_moneda");
				if(claveMoneda == null || !claveMoneda.equals("1")){	continue;}
				
				rs_monto 	= "".equals(reporte.getString("Monto Total"))?
									new BigDecimal(0):new BigDecimal(reporte.getString("Monto Total"));
				totMonto		= totMonto.add(rs_monto);
			}
							
			montoTotal = Comunes.formatoDecimal(totMonto.toPlainString(), 2,false);
		}
			
		log.info("getTotalNoDispersado(S)");
		
		return montoTotal;
	}

	public String getTotalNoDispersadoUSD(RepDispEpo repDispEpo){
		
		log.info("getTotalNoDispersadoUSD(E)");
 
		String 	montoTotal 	= "0.00";
		String   claveMoneda	= "";
			
		if(repDispEpo != null) {
 				
			Registros 	reporte 		= repDispEpo.getRepPymesSinDisp();
			BigDecimal	rs_monto		= new BigDecimal(0);
			BigDecimal	totMonto 	= new BigDecimal(0);
				
			while(reporte.next()) {
				claveMoneda = reporte.getString("rs_ic_moneda");
				if(claveMoneda == null || !claveMoneda.equals("54")){	continue;}
				
				rs_monto 	= "".equals(reporte.getString("Monto Total"))?
									new BigDecimal(0):new BigDecimal(reporte.getString("Monto Total"));
				totMonto 	= totMonto.add(rs_monto);
			}
							
			montoTotal = Comunes.formatoDecimal(totMonto.toPlainString(), 2,false);
		}
			
		log.info("getTotalNoDispersadoUSD(S)");
		
		return montoTotal;
	}

	public HashMap getListaDeIntermediariosConOperacion(RepDispEpo repDispEpo){
		
		log.info("getListaDeIntermediariosConOperacion(E)");
		
		String 		montoTotal 			= "0.00";
		HashMap		datos					= new HashMap();
		ArrayList	registros			= new ArrayList();
		HashMap		registro				= null;
		
		String 		montoTotalUSD 		= "0.00";
		ArrayList	registrosUSD		= new ArrayList();
	
		if(repDispEpo != null) {
 			
			Registros 	reporte 			= repDispEpo.getRepPagoIf();
			String 		intermediario	= "";	
			String 		claveMoneda		= "";
			BigDecimal	rs_monto			= new BigDecimal(0);
			BigDecimal	totMonto 		= new BigDecimal(0);
			BigDecimal	totMontoUSD		= new BigDecimal(0);
				
			while(reporte.next()) {
				registro			= new HashMap();
			
				intermediario 	= 	reporte.getString("IF");
				claveMoneda 	= 	reporte.getString("rs_ic_moneda");
				rs_monto 		= 	"".equals(reporte.getString("Monto Total"))?
											new BigDecimal(0):new BigDecimal(reporte.getString("Monto Total"));
				
				if(claveMoneda.equals("1")){
					totMonto 	= totMonto.add(rs_monto);
					registro.put("INTERMEDIARIO",	intermediario );
					registro.put("MONTO",			"$ " + Comunes.formatoDecimal(rs_monto.toPlainString(), 2));
					registros.add(registro);
				}
				
				if(claveMoneda.equals("54")){
					totMontoUSD = totMontoUSD.add(rs_monto);
					registro.put("INTERMEDIARIO",	intermediario );
					registro.put("MONTO",			"$ " + Comunes.formatoDecimal(rs_monto.toPlainString(), 2));
					registrosUSD.add(registro);
				}
			}
				
			datos.put("LISTA_BANCOS",registros);	
			montoTotal = Comunes.formatoDecimal(totMonto.toPlainString(), 2,false);
			log.info("MONTO_TOTAL = " + montoTotal);	
			datos.put("MONTO_TOTAL",montoTotal);
			
			datos.put("LISTA_BANCOS_USD",registrosUSD);				
			montoTotalUSD = Comunes.formatoDecimal(totMontoUSD.toPlainString(), 2,false);
			log.info("MONTO_TOTAL_USD = " + montoTotalUSD);	
			datos.put("MONTO_TOTAL_USD",montoTotalUSD);
		}
		
		log.info("getListaDeIntermediariosConOperacion(S)");
		
		return datos;
	}
	
	public void generaArchivoAdjuntoXLS( 
		List				lReportes, 
		HttpSession 	session, 
		String 			strDirectorioTemp, 
		String 			strDirectorioPublicacion, 
		String 			fileName, 
		DispersionBean dispersionBean
	) throws AppException {	
		
		log.info("generaArchivoAdjuntoXLS(E)");
		
	 	AccesoDB con =	null;
	 	
		try {
			
			con = new AccesoDB();
			con.conexionDB();
 
			CreaArchivo 	archivo 				= new CreaArchivo();
			StringBuffer 	contenidoArchivo 	= new StringBuffer("");
			String 			nombreArchivo2 	= fileName+".xls";
			boolean 			bOK 					= true;
			
			if( lReportes == null || lReportes.size() == 0 ) {
				bOK = false;
			} else {

				// Obtener la descripcion de las monedas
				String descripcionMonedaNacional 	= dispersionBean.getDescripcionMoneda("1");
				String descripcionDolaresAmericanos = dispersionBean.getDescripcionMoneda("54");
	
				//Sacando la fecha para encabezado de Excel
				String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    	= fechaActual.substring(0,2);
				String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   	= fechaActual.substring(6,10);
				String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
				String strEncabezado = 	"M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
												" ----------------------------- " + horaActual;
				//Fin encabezado
		
				// Crear Documento XLS
				String 		nom_hoja_xls 	= "";
				ComunesXLS 	xlsDoc 			= null;
 
				for(int i=0;i<lReportes.size();i++) {
			
					// Leer registro
					RepDispEpo 	repDispEpo 		= (RepDispEpo)lReportes.get(i);
					String 		ic_epo 			= repDispEpo.getIc_epo();
					String 		nom_epo 			= repDispEpo.getNom_epo();
					String 		df_fecha_venc 	= repDispEpo.getDf_fecha_venc();
					String 		rs_cecoban 		= repDispEpo.getRs_cecoban();
					String 		rs_ic_moneda	= "";
					String 		nombreMoneda	= "";
					
					log.debug(" ---> nom_epo    = <"+nom_epo+">"); 		
					log.debug(" ---> rs_cecoban = <"+rs_cecoban+">"); 	

					List 		lTitulos;
					List 		lPymesConDisp 	= new ArrayList();
					List 		lPymesSinDisp 	= new ArrayList();
		
					Registros 	reporte 		= new Registros();
					int			rs_doctos;
					BigDecimal	rs_monto		= new BigDecimal(0);
					int			totDoctos;
					BigDecimal	totMonto		= new BigDecimal(0);
					int			totDoctosUSD;
					BigDecimal	totMontoUSD = new BigDecimal(0);
					
		
					for(int j=0;j<3;j++){
						
						lTitulos = new ArrayList();
						lTitulos.add("Num. Total Doctos");
						lTitulos.add("Moneda");
						lTitulos.add("Monto Total");
						lTitulos.add("ic_pyme");
						
						totDoctos 		= 0;
						totMonto 		= new BigDecimal(0);
						totDoctosUSD 	= 0;
						totMontoUSD 	= new BigDecimal(0);
						
						switch ( j ) {
							case 0:
								lTitulos.add(0, "PYME");
								reporte = repDispEpo.getRepPymesConDisp();
		
								nom_hoja_xls = (nom_epo.length() > 14)?nom_epo.substring(0,14):nom_epo;
		
								if(i==0)
									xlsDoc = new ComunesXLS(strDirectorioTemp+nombreArchivo2, nom_hoja_xls+"PYMES DISP_"+i);
								if(i>0)
									xlsDoc.creaHoja(nom_hoja_xls+"PYMES DISP_"+i);
								
								// Poner Fecha en la parte superior derecha de la hoja
								xlsDoc.setCelda(strEncabezado, "formas", ComunesXLS.RIGHT, 12);
		
								xlsDoc.setTabla(10);
								//Agregar linea en Blanco
								if(i>0){
									xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("",  "formas", ComunesXLS.CENTER, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("",  "formas", ComunesXLS.CENTER, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("",  "formas", ComunesXLS.CENTER, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("",  "formas", ComunesXLS.CENTER, 1);
								}
								// Agregar nombre de la EPO
								xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);
								xlsDoc.setCelda("EPO:"+nom_epo,  "titulo", ComunesXLS.CENTER, 8);
								// Agregar detalle de la Fecha de vencimiento
								xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);
								xlsDoc.setCelda("FECHA DE VENCIMIENTO:"+ df_fecha_venc,  "titulo", ComunesXLS.CENTER, 8);
								// Agregar titulo PYMES CON DISPERSION
								xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);
								xlsDoc.setCelda("PYMES CON DISPERSION",  "titulo", ComunesXLS.CENTER, 8);
								xlsDoc.cierraTabla();
								
							break;
		
							case 1:
								lTitulos.add(0, "PYME");
								reporte = repDispEpo.getRepPymesSinDisp();
		
								xlsDoc.creaHoja(nom_hoja_xls+"PYMES SN DISP_"+i);
								// Poner Fecha en la parte superior derecha de la hoja
								xlsDoc.setCelda(strEncabezado, "formas", ComunesXLS.RIGHT, 12);
								
								xlsDoc.setTabla(10);
								// Agregar nombre de la EPO
								xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);
								xlsDoc.setCelda("EPO:"+nom_epo,  "titulo", ComunesXLS.CENTER, 8);
								// Agregar detalle de la Fecha de vencimiento
								xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);
								xlsDoc.setCelda("FECHA DE VENCIMIENTO:"+ df_fecha_venc,  "titulo", ComunesXLS.CENTER, 8);
								// Agregar titulo PYMES CON DISPERSION
								xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);
								xlsDoc.setCelda("PYMES SIN DISPERSION",  "titulo", ComunesXLS.CENTER, 8);
								xlsDoc.cierraTabla();
		
							break;
		
							case 2:
								lTitulos.add(0, "IF");
								reporte = repDispEpo.getRepPagoIf();
		
								xlsDoc.creaHoja(nom_hoja_xls+"PAGO IF (OP)_"+i);
								// Poner Fecha en la parte superior derecha de la hoja
								xlsDoc.setCelda(strEncabezado, "formas", ComunesXLS.RIGHT, 12);
								
								xlsDoc.setTabla(10);
								// Agregar nombre de la EPO
								xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);
								xlsDoc.setCelda("EPO:"+nom_epo,  "titulo", ComunesXLS.CENTER, 8);
								// Agregar detalle de la Fecha de vencimiento
								xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);
								xlsDoc.setCelda("FECHA DE VENCIMIENTO:"+ df_fecha_venc,  "titulo", ComunesXLS.CENTER, 8);
								// Agregar titulo PYMES CON DISPERSION
								xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);xlsDoc.setCelda("", "formas", ComunesXLS.RIGHT, 1);
								xlsDoc.setCelda("PAGO A IF (OPERADO)",  "titulo", ComunesXLS.CENTER, 8);
								xlsDoc.cierraTabla();
		
							break;
						}//switch ( j )
		
						lTitulos.remove(lTitulos.size()-1);
		
						xlsDoc.setTabla(10);
						// Agregar Titulos de la Tabla
						xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 2);
						xlsDoc.setCelda((String)lTitulos.get(0), "celda01", ComunesXLS.CENTER, 2);
						xlsDoc.setCelda((String)lTitulos.get(1), "celda01", ComunesXLS.CENTER, 2);
						xlsDoc.setCelda((String)lTitulos.get(2), "celda01", ComunesXLS.CENTER, 2);
						xlsDoc.setCelda((String)lTitulos.get(3), "celda01", ComunesXLS.CENTER, 2);
		 
						while(reporte.next()) {
			
							rs_ic_moneda	= reporte.getString("rs_ic_moneda");
							nombreMoneda	= rs_ic_moneda.equals("1")?descripcionMonedaNacional:(rs_ic_moneda.equals("54")?descripcionDolaresAmericanos:"");
			
							rs_doctos 		= 	"".equals(reporte.getString("Num. Total Doctos"))?0:Integer.parseInt(reporte.getString("Num. Total Doctos"));
							rs_monto 		= 	"".equals(reporte.getString("Monto Total"))?
													new BigDecimal(0):new BigDecimal(reporte.getString("Monto Total"));
							
							if(rs_ic_moneda.equals("1")){
								totDoctos		+= rs_doctos;
								totMonto			=  totMonto.add(rs_monto);
							}
							
							if(rs_ic_moneda.equals("54")){
								totDoctosUSD	+= rs_doctos;
								totMontoUSD		=  totMontoUSD.add(rs_monto);
							}	
							
							// XLS - AGREGAR RESUMEN DE LOS DOCUMENTOS
							xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda(reporte.getString(lTitulos.get(0).toString()), "formas", ComunesXLS.LEFT, 2);
							xlsDoc.setCelda(String.valueOf(rs_doctos), "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda(nombreMoneda, "formas", ComunesXLS.CENTER, 2); // Moneda
							xlsDoc.setCelda("$ "+Comunes.formatoDecimal(rs_monto.toPlainString(), 2), "formas", ComunesXLS.RIGHT, 2);
							
							if(j==0) {
								lPymesConDisp.add(reporte.getString("ic_pyme"));
							} else if (j==1) {
								lPymesSinDisp.add(reporte.getString("ic_pyme"));
							}
						}//while(reporte.next())
						// Mostrar Totales del Resumen de los Documentos
						if( totDoctos>0 || totDoctosUSD>0 ) {
									
							// XLS - Total M.N.
							xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda("TOTAL M.N.", "celda01", ComunesXLS.RIGHT, 2);
							xlsDoc.setCelda(String.valueOf(totDoctos), "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda("", "celda01", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda("$ "+Comunes.formatoDecimal(totMonto.toPlainString(), 2), "formas", ComunesXLS.RIGHT, 2);
							
							// XLS - Total USD
							xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda("TOTAL USD", "celda01", ComunesXLS.RIGHT, 2);
							xlsDoc.setCelda(String.valueOf(totDoctosUSD), "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda("", "celda01", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda("$ "+Comunes.formatoDecimal(totMontoUSD.toPlainString(), 2), "formas", ComunesXLS.RIGHT, 2);
		 
						} else {
								
							// XLS - NO SE ENCONTRO NINGUN REGISTRO
							xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda("NO EXISTEN REGISTROS", "celda01", ComunesXLS.CENTER, 8);
							
						}
						//cierra tabla
						xlsDoc.cierraTabla();
		
					}//for(int j=0;j<3;j++)
		
					lTitulos = new ArrayList();
					lTitulos.add("PYME");
					lTitulos.add("Num. Documento");
					lTitulos.add("Fecha de emisi�n");
					lTitulos.add("Fecha de Vencimiento");
					lTitulos.add("Moneda");
					lTitulos.add("Monto");
					List lPymes = new ArrayList();
					for(int j=0;j<2;j++) {
						totDoctos 		= 0;
						totMonto 		= new BigDecimal(0);
						totDoctosUSD 	= 0;
						totMontoUSD 	= new BigDecimal(0);
						switch ( j ) {
							case 0:
								lPymes = lPymesConDisp;
		
								xlsDoc.creaHoja(nom_hoja_xls+"DT DCT P DSP_"+i);
								xlsDoc.setCelda(strEncabezado, "formas", ComunesXLS.RIGHT, 12);
								xlsDoc.agregaTitulo("EPO:"+nom_epo,12);
								xlsDoc.agregaTitulo("DETALLE DE DOCUMENTOS DE PROVEEDORES QUE SE DISPERSARAN",12);
		
							break;
		
							case 1:
								lPymes = lPymesSinDisp;
		
								xlsDoc.creaHoja(nom_hoja_xls+"DT DCT P N DSP_"+i);
								xlsDoc.setCelda(strEncabezado, "formas", ComunesXLS.RIGHT, 12);
								xlsDoc.agregaTitulo("EPO:"+nom_epo,14);
								xlsDoc.agregaTitulo("DETALLE DE DOCUMENTOS DE PROVEEDORES QUE NO SE DISPERSARAN",14);
		
							break;
						}//switch ( j )
		
		
		
						List lVarBind = new ArrayList();
						StringBuffer qrySentencia = new StringBuffer ();
		
						if(lPymes.size()>0) {
							
							qrySentencia.append(dispersionBean.getQueryDetalleDocumentosADispersar(lPymes.size()));
							
							lVarBind.add("D");
							lVarBind.add("I");
							lVarBind.add(new Integer(0));
							lVarBind.add("N");
							lVarBind.add("V");
							lVarBind.add("M");
							lVarBind.add(new Integer(ic_epo));
							lVarBind.add(new Integer(1));
							lVarBind.add(new Integer(2));
							lVarBind.add(new Integer(1));  // ic_moneda M.N.
							lVarBind.add(new Integer(54)); // ic_moneda USD
							lVarBind.add(df_fecha_venc);
							lVarBind.add(df_fecha_venc);
							lVarBind.add(new Integer(1));
							
							for(int p=0;p<lPymes.size();p++){
								lVarBind.add(lPymes.get(p).toString());
							}
		
							lVarBind.add("D");
							lVarBind.add("I");
							lVarBind.add(new Integer(0));
							lVarBind.add("N");
							lVarBind.add("V");
							lVarBind.add("C");
							lVarBind.add("M");
							lVarBind.add(new Integer(ic_epo));
							lVarBind.add(new Integer(9));
							lVarBind.add(new Integer(1));  // ic_moneda M.N.
							lVarBind.add(new Integer(54)); // ic_moneda USD
							lVarBind.add(df_fecha_venc);
							lVarBind.add(df_fecha_venc);
							lVarBind.add(new Integer(1));
		
							for(int p=0;p<lPymes.size();p++){
								lVarBind.add(lPymes.get(p).toString());
							}
							
						}//if(lPymes.size()>0)
		
						if(qrySentencia.length()>0)
							reporte = con.consultarDB(qrySentencia.toString(), lVarBind, false);
						else
							reporte = new Registros();
 
						if(j==1) lTitulos.add("Estatus");
		
						String 	strCuenta 		= "";
						int 	iregistrosTabla = 12;
		
		//poniendo la tabla
						if(j==1)
							iregistrosTabla = 14;
						xlsDoc.setTabla(iregistrosTabla);
		//poniendo subtitulos a la tabla
						xlsDoc.setCelda((String)lTitulos.get(0), "celda01", ComunesXLS.CENTER, 2);
						xlsDoc.setCelda((String)lTitulos.get(1), "celda01", ComunesXLS.CENTER, 2);
						xlsDoc.setCelda((String)lTitulos.get(2), "celda01", ComunesXLS.CENTER, 2);
						xlsDoc.setCelda((String)lTitulos.get(3), "celda01", ComunesXLS.CENTER, 2);
						xlsDoc.setCelda((String)lTitulos.get(4), "celda01", ComunesXLS.CENTER, 2);// Moneda
						xlsDoc.setCelda((String)lTitulos.get(5), "celda01", ComunesXLS.CENTER, 2);
						if(j==1)
							xlsDoc.setCelda("Estatus", "celda01", ComunesXLS.CENTER, 2);
		
							if(rs_cecoban.equals("99"))
								strCuenta = "cuenta correcta";
							else if(rs_cecoban.equals("0"))
								strCuenta = "sin cuenta";
							else strCuenta = "cuenta no v�lida";
		
						while(reporte.next()) {
							rs_monto 		= "".equals(reporte.getString("fn_monto"))?
													new BigDecimal(0):new BigDecimal(reporte.getString("fn_monto"));
							rs_ic_moneda	= reporte.getString("ic_moneda");
							nombreMoneda	= rs_ic_moneda.equals("1")?descripcionMonedaNacional:(rs_ic_moneda.equals("54")?descripcionDolaresAmericanos:"");
		
		//datos
							xlsDoc.setCelda(reporte.getString("cg_razon_social"), "formas", ComunesXLS.LEFT, 2);
							xlsDoc.setCelda(reporte.getString("ig_numero_docto"), "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda(reporte.getString("df_fecha_docto"), "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda(reporte.getString("df_fecha_venc"), "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda(nombreMoneda, "formas", ComunesXLS.CENTER, 2);// Moneda
							xlsDoc.setCelda("$ "+Comunes.formatoDecimal(rs_monto.toPlainString(), 2), "formas", ComunesXLS.RIGHT, 2);
							if(j==1)
								xlsDoc.setCelda(strCuenta, "formas", ComunesXLS.RIGHT, 2);
							
							if(rs_ic_moneda.equals("1")){
								totDoctos	++;
								totMonto 	= totMonto.add(rs_monto);
							}
							if(rs_ic_moneda.equals("54")){
								totDoctosUSD	++;
								totMontoUSD = totMontoUSD.add(rs_monto);
							}
						}//while(reporte.next())
						if(totDoctos>0) {
									
							// XLS - TOTALES DETALLE
							xlsDoc.setCelda("TOTAL M.N.", "celda01", ComunesXLS.RIGHT, 2);
							xlsDoc.setCelda(String.valueOf(totDoctos), "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda("", "celda01", ComunesXLS.RIGHT, 6);
							xlsDoc.setCelda("$ "+Comunes.formatoDecimal(totMonto.toPlainString(), 2), "formas", ComunesXLS.RIGHT, 2);
							if(j==1){xlsDoc.setCelda("", "celda01", ComunesXLS.CENTER, 2);}
							
							xlsDoc.setCelda("TOTAL USD", "celda01", ComunesXLS.RIGHT, 2);
							xlsDoc.setCelda(String.valueOf(totDoctosUSD), "formas", ComunesXLS.CENTER, 2);
							xlsDoc.setCelda("", "celda01", ComunesXLS.RIGHT, 6);
							xlsDoc.setCelda("$ "+Comunes.formatoDecimal(totMontoUSD.toPlainString(), 2), "formas", ComunesXLS.RIGHT, 2);
							if(j==1){xlsDoc.setCelda("", "celda01", ComunesXLS.CENTER, 2);}
		
						} else {
		
							//NO HAY DATOS
							xlsDoc.setCelda("NO EXISTEN REGISTROS", "celda01", ComunesXLS.CENTER, iregistrosTabla);
		
						}
		
					}//for(int j=0;j<2;j++)
				
				}//for(int i=0;i<lReportes.size();i++)
				xlsDoc.cierraXLS();
					
			}
			
		} catch(Exception e) {
			
			log.error("generaArchivoAdjuntoXLS(Exception)");
			log.error("generaArchivoAdjuntoXLS.lReportes                = <" + lReportes                + ">");
			log.error("generaArchivoAdjuntoXLS.session                  = <" + session                  + ">");
			log.error("generaArchivoAdjuntoXLS.strDirectorioTemp        = <" + strDirectorioTemp        + ">");
			log.error("generaArchivoAdjuntoXLS.strDirectorioPublicacion = <" + strDirectorioPublicacion + ">");
			log.error("generaArchivoAdjuntoXLS.fileName                 = <" + fileName                 + ">");
			log.error("generaArchivoAdjuntoXLS.dispersionBean           = <" + dispersionBean           + ">");
			e.printStackTrace();
			
			throw new AppException("Ocurri� un error al generar archivo XLS adjunto: " + e.getMessage() + ".");
			
		} finally {
			
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("generaArchivoAdjuntoXLS(S)");
			
		}
		
	}
	
	public void generaArchivoAdjuntoPDF( 
		List 				lReportes, 
		HttpSession 	session, 
		String 			strDirectorioTemp, 
		String 			strDirectorioPublicacion, 
		String 			fileName, 
		DispersionBean dispersionBean
	) throws AppException {	
		
		log.info("generaArchivoAdjuntoPDF(E)");
		
	 	AccesoDB con =	null;
	 	
		try {
			
			con = new AccesoDB();
			con.conexionDB();
 
			CreaArchivo 	archivo 				= new CreaArchivo();
			StringBuffer 	contenidoArchivo 	= new StringBuffer("");
			String 			nombreArchivo 		= fileName+".pdf";
			boolean 			bOK 					= true;
			
			if( lReportes == null || lReportes.size() == 0 ) {
				bOK = false;
			} else {

				// Obtener la descripcion de las monedas
				String descripcionMonedaNacional 	= dispersionBean.getDescripcionMoneda("1");
				String descripcionDolaresAmericanos = dispersionBean.getDescripcionMoneda("54");
	
				//Sacando la fecha para encabezado de Excel
				String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    	= fechaActual.substring(0,2);
				String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   	= fechaActual.substring(6,10);
				String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
				String strEncabezado = 	"M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
												" ----------------------------- " + horaActual;
				//Fin encabezado

				// Crear Documento PDF
				ComunesPDF pdfDoc 	= new ComunesPDF(2, strDirectorioTemp+nombreArchivo, "", false, true, true);
				pdfDoc.setEncabezado(
						(String)session.getAttribute("strPais"),
						(String)session.getAttribute("iNoNafinElectronico"),
						(String)session.getAttribute("sesExterno"),
						(String)session.getAttribute("strNombre"),
						(String)session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),
						strDirectorioPublicacion, "");
		 
				for(int i=0;i<lReportes.size();i++) {
			
					// Leer registro
					RepDispEpo 	repDispEpo 		= (RepDispEpo)lReportes.get(i);
					String 		ic_epo 			= repDispEpo.getIc_epo();
					String 		nom_epo 			= repDispEpo.getNom_epo();
					String 		df_fecha_venc 	= repDispEpo.getDf_fecha_venc();
					String 		rs_cecoban 		= repDispEpo.getRs_cecoban();
					String 		rs_ic_moneda	= "";
					String 		nombreMoneda	= "";
					
					log.debug(" ---> nom_epo    = <"+nom_epo+">"); 		
					log.debug(" ---> rs_cecoban = <"+rs_cecoban+">"); 	
		
					String 	tituloReporte 		= "";
					String 	tituloAdicional 	= "";
					List 		lTitulos;
					List 		lPymesConDisp 	= new ArrayList();
					List 		lPymesSinDisp 	= new ArrayList();
		
					Registros 	reporte 		= new Registros();
					int			rs_doctos;
					BigDecimal	rs_monto		= new BigDecimal(0);
					int			totDoctos;
					BigDecimal	totMonto		= new BigDecimal(0);
					int			totDoctosUSD;
					BigDecimal	totMontoUSD = new BigDecimal(0);
					
		
					for(int j=0;j<3;j++){
						
						lTitulos = new ArrayList();
						lTitulos.add("Num. Total Doctos");
						lTitulos.add("Moneda");
						lTitulos.add("Monto Total");
						lTitulos.add("ic_pyme");
						
						totDoctos 		= 0;
						totMonto 		= new BigDecimal(0);
						totDoctosUSD 	= 0;
						totMontoUSD 	= new BigDecimal(0);
						
						switch ( j ) {
							
							case 0:
								tituloReporte = "EPO:"+nom_epo+"\nFECHA DE VENCIMIENTO:"+df_fecha_venc;
								tituloAdicional = "PYMES CON DISPERSION";
								lTitulos.add(0, "PYME");
								reporte = repDispEpo.getRepPymesConDisp();
							break;
		
							case 1:
								tituloReporte = "EPO:"+nom_epo+"\nFECHA DE VENCIMIENTO:"+df_fecha_venc;
								tituloAdicional = "PYMES SIN DISPERSION";
								lTitulos.add(0, "PYME");
								reporte = repDispEpo.getRepPymesSinDisp();
							break;
		
							case 2:
								tituloReporte = "EPO:"+nom_epo+"\nFECHA DE VENCIMIENTO:"+df_fecha_venc;
								tituloAdicional = "PAGO A IF (OPERADO)";
								lTitulos.add(0, "IF");
								reporte = repDispEpo.getRepPagoIf();
							break;
							
						}//switch ( j )
		
						lTitulos.remove(lTitulos.size()-1);
						pdfDoc.setStrTitulo(tituloReporte);
						pdfDoc.addText(tituloAdicional, "formasB", ComunesPDF.CENTER);
						pdfDoc.setTable(lTitulos, "celda01", 50);
		
						while(reporte.next()) {
			
							rs_ic_moneda	= reporte.getString("rs_ic_moneda");
							nombreMoneda	= rs_ic_moneda.equals("1")?descripcionMonedaNacional:(rs_ic_moneda.equals("54")?descripcionDolaresAmericanos:"");
			
							rs_doctos 		= 	"".equals(reporte.getString("Num. Total Doctos"))?0:Integer.parseInt(reporte.getString("Num. Total Doctos"));
							rs_monto 		= 	"".equals(reporte.getString("Monto Total"))?
													new BigDecimal(0):new BigDecimal(reporte.getString("Monto Total"));
							
							if(rs_ic_moneda.equals("1")){
								totDoctos		+= rs_doctos;
								totMonto			=  totMonto.add(rs_monto);
							}
							
							if(rs_ic_moneda.equals("54")){
								totDoctosUSD	+= rs_doctos;
								totMontoUSD		=  totMontoUSD.add(rs_monto);
							}	
							
							pdfDoc.setCell(reporte.getString(lTitulos.get(0).toString()), "formas", ComunesPDF.LEFT);
							pdfDoc.setCell(String.valueOf(rs_doctos), "formas", ComunesPDF.CENTER);
							pdfDoc.setCell(nombreMoneda, "formas", ComunesPDF.LEFT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(rs_monto.toPlainString(), 2), "formas", ComunesPDF.RIGHT);
		
							if(j==0) {
								lPymesConDisp.add(reporte.getString("ic_pyme"));
							} else if (j==1) {
								lPymesSinDisp.add(reporte.getString("ic_pyme"));
							}
							
						}//while(reporte.next())
						// Mostrar Totales del Resumen de los Documentos
						if( totDoctos>0 || totDoctosUSD>0 ) {
							
							// PDF - Total M.N.
							pdfDoc.setCell("TOTAL M.N.", "celda01", ComunesPDF.RIGHT);
							pdfDoc.setCell(String.valueOf(totDoctos), "formas", ComunesPDF.CENTER);
							pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMonto.toPlainString(), 2), "formas", ComunesPDF.RIGHT);
							
							// PDF - Total USD
							pdfDoc.setCell("TOTAL USD", "celda01", ComunesPDF.RIGHT);
							pdfDoc.setCell(String.valueOf(totDoctosUSD), "formas", ComunesPDF.CENTER);
							pdfDoc.setCell("", "celda01", ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMontoUSD.toPlainString(), 2), "formas", ComunesPDF.RIGHT);

						} else {
							
							pdfDoc.setCell("NO EXISTEN REGISTROS", "celda01", ComunesPDF.CENTER, 4);
							
						}
						
						pdfDoc.addTable();
						pdfDoc.newPage();
 
					}//for(int j=0;j<3;j++)
		
					lTitulos = new ArrayList();
					lTitulos.add("PYME");
					lTitulos.add("Num. Documento");
					lTitulos.add("Fecha de emisi�n");
					lTitulos.add("Fecha de Vencimiento");
					lTitulos.add("Moneda");
					lTitulos.add("Monto");
					List lPymes = new ArrayList();
					for(int j=0;j<2;j++) {
						totDoctos 		= 0;
						totMonto 		= new BigDecimal(0);
						totDoctosUSD 	= 0;
						totMontoUSD 	= new BigDecimal(0);
						switch ( j ) {
							case 0:
								tituloReporte = "EPO:"+nom_epo+"\nDETALLE DE DOCUMENTOS DE PROVEEDORES QUE SE DISPERSARAN";
								lPymes = lPymesConDisp;		
							break;
		
							case 1:
								tituloReporte = "EPO:"+nom_epo+"\nDETALLE DE DOCUMENTOS DE PROVEEDORES QUE NO SE DISPERSARAN";
								lPymes = lPymesSinDisp;
							break;
						}//switch ( j )
		
		
		
						List lVarBind = new ArrayList();
						StringBuffer qrySentencia = new StringBuffer ();
		
						if(lPymes.size()>0) {
							
							qrySentencia.append(dispersionBean.getQueryDetalleDocumentosADispersar(lPymes.size()));
							
							lVarBind.add("D");
							lVarBind.add("I");
							lVarBind.add(new Integer(0));
							lVarBind.add("N");
							lVarBind.add("V");
							lVarBind.add("M");
							lVarBind.add(new Integer(ic_epo));
							lVarBind.add(new Integer(1));
							lVarBind.add(new Integer(2));
							lVarBind.add(new Integer(1));  // ic_moneda M.N.
							lVarBind.add(new Integer(54)); // ic_moneda USD
							lVarBind.add(df_fecha_venc);
							lVarBind.add(df_fecha_venc);
							lVarBind.add(new Integer(1));
							
							for(int p=0;p<lPymes.size();p++){
								lVarBind.add(lPymes.get(p).toString());
							}
		
							lVarBind.add("D");
							lVarBind.add("I");
							lVarBind.add(new Integer(0));
							lVarBind.add("N");
							lVarBind.add("V");
							lVarBind.add("C");
							lVarBind.add("M");
							lVarBind.add(new Integer(ic_epo));
							lVarBind.add(new Integer(9));
							lVarBind.add(new Integer(1));  // ic_moneda M.N.
							lVarBind.add(new Integer(54)); // ic_moneda USD
							lVarBind.add(df_fecha_venc);
							lVarBind.add(df_fecha_venc);
							lVarBind.add(new Integer(1));
		
							for(int p=0;p<lPymes.size();p++){
								lVarBind.add(lPymes.get(p).toString());
							}
							
						}//if(lPymes.size()>0)
		
						if(qrySentencia.length()>0)
							reporte = con.consultarDB(qrySentencia.toString(), lVarBind, false);
						else
							reporte = new Registros();
		
						pdfDoc.setStrTitulo(tituloReporte);
		
						if(j==1) lTitulos.add("Estatus");
							pdfDoc.setTable(lTitulos, "celda01", 50);
		
						String 	strCuenta 		= "";
						int 	iregistrosTabla = 12;
		
		//poniendo la tabla
						if(j==1)
							iregistrosTabla = 14;
								
							if(rs_cecoban.equals("99"))
								strCuenta = "cuenta correcta";
							else if(rs_cecoban.equals("0"))
								strCuenta = "sin cuenta";
							else strCuenta = "cuenta no v�lida";
		
						while(reporte.next()) {
							rs_monto 		= "".equals(reporte.getString("fn_monto"))?
													new BigDecimal(0):new BigDecimal(reporte.getString("fn_monto"));
							rs_ic_moneda	= reporte.getString("ic_moneda");
							nombreMoneda	= rs_ic_moneda.equals("1")?descripcionMonedaNacional:(rs_ic_moneda.equals("54")?descripcionDolaresAmericanos:"");
		
							pdfDoc.setCell(reporte.getString("cg_razon_social"), "formas", ComunesPDF.LEFT);
							pdfDoc.setCell(reporte.getString("ig_numero_docto"), "formas", ComunesPDF.CENTER);
							pdfDoc.setCell(reporte.getString("df_fecha_docto"), "formas", ComunesPDF.CENTER);
							pdfDoc.setCell(reporte.getString("df_fecha_venc"), "formas", ComunesPDF.CENTER);
							pdfDoc.setCell(nombreMoneda, "formas", ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(rs_monto.toPlainString(), 2), "formas", ComunesPDF.RIGHT);
							if(j==1)
								pdfDoc.setCell(strCuenta, "formas", ComunesXLS.RIGHT);
		//datos
		
							if(rs_ic_moneda.equals("1")){
								totDoctos	++;
								totMonto 	= totMonto.add(rs_monto);
							}
							if(rs_ic_moneda.equals("54")){
								totDoctosUSD	++;
								totMontoUSD = totMontoUSD.add(rs_monto);
							}
							
						}//while(reporte.next())
						
						if(totDoctos>0) {
		
							//PDF - TOTALES DETALLE
							pdfDoc.setCell("TOTAL M.N.", "celda01", ComunesPDF.RIGHT);
							pdfDoc.setCell(String.valueOf(totDoctos), "formas", ComunesPDF.CENTER);
							pdfDoc.setCell("", "celda01", ComunesPDF.CENTER, 3);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMonto.toPlainString(), 2), "formas", ComunesPDF.RIGHT);
							if(j==1){pdfDoc.setCell("", "celda01", ComunesPDF.CENTER);}
							
							pdfDoc.setCell("TOTAL USD", "celda01", ComunesPDF.RIGHT);
							pdfDoc.setCell(String.valueOf(totDoctosUSD), "formas", ComunesPDF.CENTER);
							pdfDoc.setCell("", "celda01", ComunesPDF.CENTER, 3);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(totMontoUSD.toPlainString(), 2), "formas", ComunesPDF.RIGHT);
							if(j==1){pdfDoc.setCell("", "celda01", ComunesPDF.CENTER);}
									
						} else {
		
							//NO HAY DATOS
							pdfDoc.setCell("NO EXISTEN REGISTROS", "celda01", ComunesPDF.CENTER, iregistrosTabla/2);
							//pdfDoc.setCell("NO EXISTEN REGISTROS", "celda01", ComunesPDF.CENTER, 5);
		
						}
						pdfDoc.addTable();
						pdfDoc.newPage();
		
					}//for(int j=0;j<2;j++)
 
				}//for(int i=0;i<lReportes.size();i++)
				pdfDoc.endDocument();
				
			}
			
		} catch(Exception e) {
			
			log.error("generaArchivoAdjuntoPDF(Exception)");
			log.error("generaArchivoAdjuntoPDF.lReportes               	= <" + lReportes               + ">");
			log.error("generaArchivoAdjuntoPDF.session                  = <" + session                  + ">");
			log.error("generaArchivoAdjuntoPDF.strDirectorioTemp        = <" + strDirectorioTemp        + ">");
			log.error("generaArchivoAdjuntoPDF.strDirectorioPublicacion = <" + strDirectorioPublicacion + ">");
			log.error("generaArchivoAdjuntoPDF.fileName                 = <" + fileName                 + ">");
			log.error("generaArchivoAdjuntoPDF.dispersionBean           = <" + dispersionBean           + ">");
			e.printStackTrace();
			
			throw new AppException("Ocurri� un error al generar archivo PDF adjunto: " + e.getMessage() + ".");
			
		} finally {
			
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("generaArchivoAdjuntoPDF(S)");
			
		}
		
	}
	
	private String escapaCaracteresEspeciales(String text){
	
		log.info("escapaCaracteresEspeciales(E)");
		
		if( text == null ){
			return text;
		}
		
		StringBuffer buffer = new StringBuffer(128);
		for(int i=0;i<text.length();i++){
					
			char c = text.charAt(i);
			if(c == '�'){
				buffer.append("a");
			}else if(c == '�'){
				buffer.append("e");
			}else if(c == '�'){
				buffer.append("i");
			}else if(c == '�'){
				buffer.append("o");
			}else if(c == '�'){
				buffer.append("u");
			}else if(c == '�'){
				buffer.append("A");
			}else if(c == '�'){
				buffer.append("E");
			}else if(c == '�'){
				buffer.append("I");
			}else if(c == '�'){
				buffer.append("O");
			}else if(c == '�'){
				buffer.append("U");
			}else if(c == '�'){
				buffer.append("n");
			}else if(c == '�'){
				buffer.append("N");
			}else if(c == '�'){
				buffer.append("a");
			}else if(c == '�'){
				buffer.append("e");
			}else if(c == '�'){
				buffer.append("i");
			}else if(c == '�'){
				buffer.append("o");
			}else if(c == '�'){
				buffer.append("u");
			}else if(c == '�'){
				buffer.append("A");
			}else if(c == '�'){
				buffer.append("E");
			}else if(c == '�'){
				buffer.append("I");
			}else if(c == '�'){
				buffer.append("O");
			}else if(c == '�'){
				buffer.append("U");
			}else if(c == '�'){
				buffer.append("a");
			}else if(c == '�'){
				buffer.append("e");
			}else if(c == '�'){
				buffer.append("i");
			}else if(c == '�'){
				buffer.append("o");
			}else if(c == '�'){
				buffer.append("u");
			}else if(c == '�'){
				buffer.append("A");
			}else if(c == '�'){
				buffer.append("E");
			}else if(c == '�'){
				buffer.append("I");
			}else if(c == '�'){
				buffer.append("O");
			}else if(c == '�'){
				buffer.append("U");
			}else{
				buffer.append(c);
			}
			
		}
		
		log.info("escapaCaracteresEspeciales(S)");
		return buffer.toString();
		
	}
	
	
	/**
	 *
	 * Enlista los tipos de documentos que ser�n enviados a flujo a fondos para una EPO en especifico,
	 * adem�s de notificar por correo los detalles de estos grupos de documentos.
	 *
	 * @param reporteEPO            Objeto <tt>RepDispEpo</tt> con el detalle de los documentos cuyos totales ser�n notificados y/o enlistados.
	 * @param loginDelUsuario       <tt>String</tt> con el login del usuario.
	 * @param nombreDelUsuario      <tt>String</tt> con el nombre del usuario.
	 * @param correoUsuario         <tt>String</tt> con el correo del usuario que realiza las notificaciones.
	 * @param fechaDeVencimiento    <tt>String</tt> con la fecha de vencimiento de los documentos.
	 * @param session               Objeto <tt>HttpSession</tt> con la sesi�n del usuario.
	 * @param directorioTemporal    <tt>String</tt> con el directorio temporal donde se crear�n los archivos que seran enviados por correo.
	 * @param directorioPublicacion <tt>String</tt> con el directorio publicacion como aparece en web.
	 * @param dispersion		  Bean del EJB de dispersi�n.
	 * @param mensajes              <tt>StringBuffer</tt> con los mensajes de error que pudieran presentarse.
	 *	
	 * @return <tt>true</tt> si al menos actualiz�/insert� alg�n grupo de documentos; retorna <tt>false</tt>
	 *         en caso contrario.
	 *
	 * @author jshernandez
	 * @mod F012-2013; 29/05/2013 06:12:27 p.m.
	 *
	 */
	public boolean enlistarDispersionEPO(
		String 			claveEPO,
		RepDispEpo		reporteEPO,
		String 			loginDelUsuario,
		String 			nombreDelUsuario,
		String 			correoUsuario,
		String 			fechaDeVencimiento,
		HttpSession 	session,
		String 			directorioTemporal,
		String 			directorioPublicacion, 
		DispersionBean	dispersionBean,
		StringBuffer 	mensajes
	) {
 		
		log.info("enlistarDispersionEPO(E)");
		
		StringBuffer mensaje 		       		= new StringBuffer(256);
		String 		 razonSocialEPO       		= claveEPO;
		AccesoDB 	 con 				  		 		= null;	
		// Por default se asume que la operaci�n es exitosa
		boolean  	 exito 			  		 		= true;
		// Inicializar bandera de registrosActualizados
		boolean		 registrosActualizados 		= false;	
		// Constantes de moneda
		final String PESOS					 		= "1";
		final String DOLARES					 		= "54";
 
		try {
 	
			// 1.1 Leer datos de la consulta de la EPO: RepDispEpo
			razonSocialEPO  = reporteEPO.getNom_epo();
		
			// 1.3 Consultar parametrizaci�n de propiedades de dispersion de la EPO. Si no est� parametrizada,
			// se cancela la operaci�n.
			if(!dispersionBean.existenParametrosDeDispersion(claveEPO)){
			
				mensaje.append("Falta parametrizar el proveedor y/o las cuentas de correo para el servicio de dispersion de la EPO: ");
				mensaje.append(razonSocialEPO);
				mensaje.append(".\n");
						
				return registrosActualizados; // Retorna false.
			
			}
		
			//	1.4 Calcular total de documentos y total de montos para cada uno de los grupos de documentos
			// de cada una de las monedas	[ 1, 54 ]
			String[] monedas 			   = { PESOS, DOLARES };
			HashMap  totalesPorMoneda  = new HashMap();
			for(int i=0;i<monedas.length;i++){
				
				// Obtener la clave de la moneda a procesar
				String  claveMoneda		 					= monedas[i];
	  
				//
				// 1.4.1 Calcular total monto y total doctos de la dispersion para PYMES
				// 
				HashMap totalesPYME = dispersionBean.calcularDoctosYMontosTotalesDispersionEPO(reporteEPO.getRepPymesConDisp(), claveMoneda, fechaDeVencimiento);
				
				// Variables para el grupo 'OPR'
				int		 	totalDocumentosOprPYME		= ((Integer) 	totalesPYME.get("totalDocumentosOpr")).intValue();
				BigDecimal	totalMontoOprPYME				= (BigDecimal) totalesPYME.get("totalMontoOpr");
				
				// Variables para el grupo 'SNOPR'
				int 		 	totalDocumentosSnOprPYME 	= ((Integer) 	totalesPYME.get("totalDocumentosSnOpr")).intValue();
				BigDecimal  totalMontoSnOprPYME			= (BigDecimal) totalesPYME.get("totalMontoSnOpr");
	 
				//
				// 1.4.2 Calcular total monto y total doctos de la dispersion para IFs
				// 
				HashMap totalesIF = dispersionBean.calcularDoctosYMontosTotalesDispersionEPO(reporteEPO.getRepPagoIf(), claveMoneda, fechaDeVencimiento);
				
				// Variables para el grupo 'OPR'
				int		 	totalDocumentosOprIF			= ((Integer) 	totalesIF.get("totalDocumentosOpr")).intValue();
				BigDecimal 	totalMontoOprIF				= (BigDecimal) totalesIF.get("totalMontoOpr");
				
				// Variables para el grupo 'SNOPR'
				int 		 	totalDocumentosSnOprIF 		= ((Integer) 	totalesIF.get("totalDocumentosSnOpr")).intValue();
				BigDecimal	totalMontoSnOprIF				= (BigDecimal) totalesIF.get("totalMontoSnOpr");
	
				//
				//	1.4.3 Sumar subtotales para cada uno de los estatus y moneda especificada:
				// 		
	
				// Variables para el grupo 'OPR'
				int			totalDocumentosOpr	= totalDocumentosOprPYME   + totalDocumentosOprIF;
				BigDecimal 	totalMontoOpr 		 	= totalMontoOprPYME.add(totalMontoOprIF);
				// Variables para el grupo 'SNOPR'
				int			totalDocumentosSnOpr = totalDocumentosSnOprPYME + totalDocumentosSnOprIF;
				BigDecimal 	totalMontoSnOpr 		= totalMontoSnOprPYME.add(totalMontoSnOprIF);
	 
				//
				//	1.4.4 Agregar totales por moneda
				// 
				HashMap  totalPorMoneda  = new HashMap();
				
				// Totales para el grupo 'OPR'
				totalPorMoneda.put("totalDocumentosOpr",	new Integer(totalDocumentosOpr)	);
				totalPorMoneda.put("totalMontoOpr",			totalMontoOpr		);
				
				// Totales para el grupo 'SNOPR'
				totalPorMoneda.put("totalDocumentosSnOpr",new Integer(totalDocumentosSnOpr));
				totalPorMoneda.put("totalMontoSnOpr",		totalMontoSnOpr	);
				
				totalesPorMoneda.put(claveMoneda,totalPorMoneda);
				
			}
		
			// 1.5 Preparar mensaje y archivos que ser�n enviados por correo
			HashMap parametrosMensaje = construyeMensaje( 
				claveEPO, 
				fechaDeVencimiento, 
				session,
				reporteEPO, 
				directorioPublicacion, 
				directorioTemporal,
				nombreDelUsuario,
				correoUsuario
			);
			
			String      remitente       = (String)    parametrosMensaje.get("remitente");
			String      destinatarios   = (String)    parametrosMensaje.get("destinatarios");
			String      ccDestinatarios = (String)    parametrosMensaje.get("ccDestinatarios");
			String      tituloMensaje   = (String)    parametrosMensaje.get("tituloMensaje");
			String      mensajeCorreo   = (String)    parametrosMensaje.get("mensajeCorreo");
			ArrayList   listaDeImagenes = (ArrayList) parametrosMensaje.get("listaDeImagenes");
			ArrayList   listaDeArchivos = (ArrayList) parametrosMensaje.get("listaDeArchivos");
 
			// 1.6 Realizar el proceso de notificaci�n

			// 1.6.1 Crear instancia de AccesoDB
			con = new AccesoDB();
			
			// 1.6.2 Abrir conexi�n a la Base de Datos, iniciar transacci�n.		
			con.conexionDB();
				
			// 1.6.3 Realizar actualizaci�n en base de datos
			for(int i=0;i<monedas.length;i++){
	 
				// Obtener la clave de la moneda a procesar
				String  claveMoneda		 					= monedas[i];
				// Resetear en false bandera de registros registrosActualizadosPorMoneda
				boolean registrosActualizadosPorMoneda = false;
				
				// 1.6.3.1 Si no hay registro previo para IC_EPO, IC_MONEDA, DC_VENCIMIENTO se deber�n 
				// insertar en la tabla COM_DISPERSION_ENLISTADA_X_EPO todos los campos siguientes:
				boolean hayDispersionEnlistada = dispersionBean.existeDispersionEnlistadaPorEPO( claveEPO, claveMoneda, fechaDeVencimiento, con );
				if( !hayDispersionEnlistada ){
					dispersionBean.registrarNuevaDispersionEnlistadaPorEPO(
						claveEPO,
						claveMoneda,							
						fechaDeVencimiento,
						loginDelUsuario,
						nombreDelUsuario,
						con
					);
				}
				
				// 1.6.3.4 Obtener los totales por moneda
				HashMap 	 	totalPorMoneda 			= (HashMap) totalesPorMoneda.get(claveMoneda);
				// Variables para el grupo 'OPR'
				int		 	totalDocumentosOpr		= ((Integer) 	totalPorMoneda.get("totalDocumentosOpr")).intValue();
				BigDecimal	totalMontoOpr				= (BigDecimal)	totalPorMoneda.get("totalMontoOpr");
				
				// Variables para el grupo 'SNOPR'
				int 		 	totalDocumentosSnOpr 	= ((Integer) 	totalPorMoneda.get("totalDocumentosSnOpr")).intValue();
				BigDecimal	totalMontoSnOpr			= (BigDecimal) totalPorMoneda.get("totalMontoSnOpr");
				
				// 1.6.3.2 Actualizar detalle de cada uno de los 2 grupos de documentos: [ _OPR, _SNOPR ], 
				// Si al menos una de las operaciones fue exitosa, activar bandera de registrosActualizados, y tambien 
				boolean exitoEnlistarOPR   = dispersionBean.enlistarDispersionEPOPorTipoDocumento(
					String.valueOf(totalDocumentosOpr),
					totalMontoOpr.toPlainString(),
					"OPR",
					claveEPO,
					claveMoneda,							
					fechaDeVencimiento,
					loginDelUsuario,
					nombreDelUsuario,
					con
				);
				boolean exitoEnlistarSNOPR = dispersionBean.enlistarDispersionEPOPorTipoDocumento(
					String.valueOf(totalDocumentosSnOpr),
					totalMontoSnOpr.toPlainString(),
					"SNOPR",
					claveEPO,
					claveMoneda,							
					fechaDeVencimiento,
					loginDelUsuario,
					nombreDelUsuario,
					con
				);
				
				// 1.6.3.3 activar bandera registrosActualizadosPorMoneda.
				registrosActualizadosPorMoneda = exitoEnlistarOPR || exitoEnlistarSNOPR?true:false;
				
				// 1.6.3.5 Si registrosActualizadosPorMoneda == true y IG_TOTAL_DOCUMENTOS_OPR == 0 y IG_TOTAL_DOCUMENTOS_SNOPR == 0, 
				//	borrar registro de la tabla:
				if( registrosActualizadosPorMoneda && totalDocumentosOpr == 0 && totalDocumentosSnOpr == 0 ){
					dispersionBean.borraDispersionEnlistadaPorEPO(
						claveEPO,
						claveMoneda,							
						fechaDeVencimiento,
						con
					);
				}
				// 1.6.3.6 Activar bandera registrosActualizados en caso de que haya registros actualizados por moneda
				if( registrosActualizadosPorMoneda ){
					registrosActualizados = true;
				} 
					
			}
			if( !registrosActualizados ){	
				exito = false; // Se deshar�n los cambios inexistentes a la base de datos, solo por se consistente con la logica :D
			}
			
			// 1.6.4 Si no se actualiz� ning�n registro, cancelar la transaccion, agregar al buffer el siguiente 
			// mensaje de error:
			if( !registrosActualizados ){	
						
				mensaje.append("Se aborta el env�o del correo debido a que todos los documentos ya fueron enviados a flujo fondos. EPO: ");
				mensaje.append(razonSocialEPO);
				mensaje.append(".\n");
								
				return registrosActualizados; // Retorna false.
			
			}	

			// 1.6.5 Registrar que se enviar� exitosamente el correo
			dispersionBean.registrarEnvioCorreoDispersionEPO(
				claveEPO, 
				fechaDeVencimiento, 
				loginDelUsuario, 
				nombreDelUsuario,
				con 
			);
 
			// 1.6.6 Enviar correo de notificaci�n.
			// Se realiza el env�o del correo de notificaciones, aunque no se haya actualizado ning�n registro y
			//	aunque no* se hayan insertado en la base de datos. (*) Solo hay un caso en el que no se podr�an insertar,
			// pero la probabilidad es muy alta de que no se de este caso.
			String  causaFalloEnvioCorreo = null;
			boolean correoEnviado			= false;
			try {
					
				Correo correo = new Correo();
				correo.enviaCorreoConDatosAdjuntos(remitente,destinatarios,ccDestinatarios,tituloMensaje,mensajeCorreo,listaDeImagenes,listaDeArchivos);
				// Actualizar estatus de env�o de correo
				correoEnviado = true;
					
			} catch(Exception e) {
					
				correoEnviado 			 = false;
 
				registrosActualizados = false; 
				exito 					 = false; 
				
				log.error("enlistarDispersionEPO(Exception):correo.enviaCorreoConDatosAdjuntos");
				log.error("enlistarDispersionEPO.remitente       = <" + remitente       + ">");
				log.error("enlistarDispersionEPO.destinatarios   = <" + destinatarios   + ">");
				log.error("enlistarDispersionEPO.ccDestinatarios = <" + ccDestinatarios + ">");
				log.error("enlistarDispersionEPO.tituloMensaje   = <" + tituloMensaje   + ">");
				log.error("enlistarDispersionEPO.mensajeCorreo   = <" + mensajeCorreo   + ">");
				log.error("enlistarDispersionEPO.listaDeImagenes = <" + listaDeImagenes + ">");
				log.error("enlistarDispersionEPO.listaDeArchivos = <" + listaDeArchivos + ">");
				log.error("enlistarDispersionEPO.claveEPO        = <" + claveEPO        + ">");
				e.printStackTrace();
		
				causaFalloEnvioCorreo = e.getMessage();
				
			}
			
			// 1.6.7 Si falla el envio del correo mostrar mensaje de error
			if( !correoEnviado ){
				
				mensaje.append("Ocurri� un error al enviar el correo de notificaci�n: ");
				mensaje.append(causaFalloEnvioCorreo);
				mensaje.append(". EPO: ");
				mensaje.append(razonSocialEPO);
				mensaje.append(".\n");
					
				return registrosActualizados;
				
			}
			
 		} catch(AppException a){
 
 			registrosActualizados 	= false; 
			exito 						= false; 

			log.error("enlistarDispersionEPO(Exception)");
			log.error("enlistarDispersionEPO.claveEPO              = <" + claveEPO              + ">");
			log.error("enlistarDispersionEPO.reporteEPO            = <" + reporteEPO            + ">");
			log.error("enlistarDispersionEPO.loginDelUsuario       = <" + loginDelUsuario       + ">");
			log.error("enlistarDispersionEPO.nombreDelUsuario      = <" + nombreDelUsuario      + ">");
			log.error("enlistarDispersionEPO.correoUsuario         = <" + correoUsuario         + ">");
			log.error("enlistarDispersionEPO.fechaDeVencimiento    = <" + fechaDeVencimiento    + ">");
			log.error("enlistarDispersionEPO.session               = <" + session               + ">");
			log.error("enlistarDispersionEPO.directorioTemporal    = <" + directorioTemporal    + ">");
			log.error("enlistarDispersionEPO.directorioPublicacion = <" + directorioPublicacion + ">"); 
			log.error("enlistarDispersionEPO.mensajes is null      = <" + ( mensajes == null )  + ">");
			a.printStackTrace();
			
			mensaje.setLength(0);
			mensaje.append( a.getMessage() + ". EPO: " + razonSocialEPO + ".\n" );
			
		} catch(Exception e){
			
			registrosActualizados 	= false;
			exito 						= false;
			
			log.error("enlistarDispersionEPO(Exception)");
			log.error("enlistarDispersionEPO.claveEPO              = <" + claveEPO              + ">");
			log.error("enlistarDispersionEPO.reporteEPO            = <" + reporteEPO            + ">");
			log.error("enlistarDispersionEPO.loginDelUsuario       = <" + loginDelUsuario       + ">");
			log.error("enlistarDispersionEPO.nombreDelUsuario      = <" + nombreDelUsuario      + ">");
			log.error("enlistarDispersionEPO.correoUsuario         = <" + correoUsuario         + ">");
			log.error("enlistarDispersionEPO.fechaDeVencimiento    = <" + fechaDeVencimiento    + ">");
			log.error("enlistarDispersionEPO.session               = <" + session               + ">");
			log.error("enlistarDispersionEPO.directorioTemporal    = <" + directorioTemporal    + ">");
			log.error("enlistarDispersionEPO.directorioPublicacion = <" + directorioPublicacion + ">"); 
			log.error("enlistarDispersionEPO.mensajes is null      = <" + ( mensajes == null )  + ">");
			e.printStackTrace();
		
			mensaje.setLength(0);
			mensaje.append("Ocurri� un error inesperado: " + e.getMessage() + ". EPO: "+ razonSocialEPO +".\n");
 
		} finally {
			
			if( con != null && con.hayConexionAbierta() ){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			
			if( mensaje.length() > 0 ){
				mensajes.append(mensaje);
			}
			
		}
		
		return registrosActualizados;
		
	}
	
}