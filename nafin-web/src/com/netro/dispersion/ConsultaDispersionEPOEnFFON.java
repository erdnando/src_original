package com.netro.dispersion;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaDispersionEPOEnFFON implements IQueryGeneratorRegExtJS {
 
private final static Log log = ServiceLocator.getInstance().getLog(ConsultaDispersionEPOEnFFON.class);
 
	StringBuffer 		query;
	private List 		conditions;
 
	//Constructor
	public ConsultaDispersionEPOEnFFON(){}
	
	/**
	 * M�todo para obtener las llaves primarias
	 * @return 
	 */
	public String getDocumentQuery(){
		
		query 							= new StringBuffer();
		conditions 						= new ArrayList();
		
		boolean hayEnvioFlujoFondosOpr	= fechaEnvioFlujoFondosOpr   == null || fechaEnvioFlujoFondosOpr.matches("\\s*")  ?false:true;
		boolean hayEnvioFlujoFondosSnOpr = fechaEnvioFlujoFondosSnOpr == null || fechaEnvioFlujoFondosSnOpr.matches("\\s*")?false:true;
		
		if( hayEnvioFlujoFondosOpr ){
			query.append(
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff btt)*/    "  +
				"        ff.ic_flujo_fondos                                            "  +
				"   FROM                                                               "  +
				"        int_flujo_fondos  ff,                                         "  +
				"        cfe_m_encabezado  me,                                         "  +
				"        comrel_nafin      crn,                                        "  +
				"        comcat_epo        e,                                          "  +
				"        comcat_if         i,                                          "  +
				"        comcat_pyme       p,                                          "  +
				"        com_cuentas       c,                                          "  +
				"        comcat_estatus_ff eff,                                        "  +
				"        comcat_bancos_tef btef                                        "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos   =  me.idoperacion_cen_i                  "  +
				"    AND ff.ic_epo            =  e.ic_epo                              "  +
				"    AND ff.ic_if             =  i.ic_if(+)                            "  +
				"    AND ff.ic_pyme           =  p.ic_pyme(+)                          "  +
				"    AND ff.ic_cuenta         =  c.ic_cuenta                           "  +
				"    AND c.ic_bancos_tef      =  btef.ic_bancos_tef                    "  +
				"    AND me.status_cen_i      =  eff.ic_estatus_ff(+)                  "  +
				"    AND ff.cs_reproceso      =  'N'                                   "  +
				"    AND ff.cs_concluido      =  'N'                                   "  +
				"    AND crn.ic_epo_pyme_if   =  NVL (ff.ic_pyme, ff.ic_if)            "  +
   			"    AND crn.cg_tipo          =  DECODE (ff.ic_pyme, NULL, 'I', 'P')   "  +
   			"    AND c.ic_producto_nafin  =  1                                     "  +
   			"    AND (FF.df_fecha_venc is not null or FF.df_operacion is not null) "  +   
   			"    and ff.ic_estatus_docto  in ( 4, 11 )                             "  +
   			"    AND FF.df_registro       >= TO_DATE(?,'dd/mm/yyyy')               "  + // DF_ENVIO_FLUJO_FONDOS_OPR
				"    AND FF.df_registro       <  (TO_DATE(?,'dd/mm/yyyy')+1)           "  + // DF_ENVIO_FLUJO_FONDOS_OPR
				"    AND FF.ic_epo            = ?                                      "  + // IC_EPO
				"    AND FF.ic_moneda         = ?                                      "  + // IC_MONEDA
				"    AND FF.df_fecha_venc     >= TO_DATE(?,'dd/mm/yyyy')               "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc     <  TO_DATE(?,'dd/mm/yyyy') + 1           "  + // DC_VENCIMIENTO
				" UNION ALL                                                            "  +
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/        "  +
				"        ff.ic_flujo_fondos                                            "  +
				"   FROM                                                               "  +
				"        int_flujo_fondos_err ff,                                      "  +
				"        cfe_m_encabezado_err me,                                      "  +
				"        comrel_nafin         crn,                                     "  +
				"        comcat_epo           e,                                       "  +
				"        comcat_if            i,                                       "  +
				"        comcat_pyme          p,                                       "  +
				"        com_cuentas          c,                                       "  +
				"        comcat_estatus_ff   eff                                       "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos      = me.idoperacion_cen_i                "  +
				"    AND ff.ic_epo               = e.ic_epo                            "  +
				"    AND ff.ic_if                = i.ic_if(+)                          "  +
				"    AND ff.ic_pyme              = p.ic_pyme(+)                        "  +
				"    AND ff.ic_cuenta            = c.ic_cuenta(+)                      "  +
				"    AND me.status_cen_i         = eff.ic_estatus_ff(+)                "  +
				"    AND ff.cs_reproceso         = 'N'                                 "  +
				"    AND ff.cs_concluido         = 'N'                                 "  +
				"    AND crn.ic_epo_pyme_if      = NVL (ff.ic_pyme, ff.ic_if)          "  +
   			"    AND crn.cg_tipo             = DECODE (ff.ic_pyme, NULL, 'I', 'P') "  +
   			"    AND c.ic_tipo_cuenta (+)    = 40                                  "  +
   			"    AND c.ic_producto_nafin (+) = 1                                   "  +
   			"    AND c.ic_moneda (+)         = 1                                   "  +
   			"    AND (FF.df_fecha_venc is not null or FF.df_operacion is not null) "  +
   			"    AND ff.ic_estatus_docto     in ( 4, 11 )                          "  +
				"    AND FF.df_registro          >= TO_DATE(?,'dd/mm/yyyy')            "  + // DF_ENVIO_FLUJO_FONDOS_OPR 
				"    AND FF.df_registro          < (TO_DATE(?,'dd/mm/yyyy')+1)         "  + // DF_ENVIO_FLUJO_FONDOS_OPR
				"    AND FF.ic_epo               = ?                                   "  + // IC_EPO
				"    AND FF.ic_moneda            = ?                                   "  + // IC_MONEDA
				"    AND FF.df_fecha_venc        >= TO_DATE(?,'dd/mm/yyyy')            "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc        <  TO_DATE(?,'dd/mm/yyyy') + 1        "  + // DC_VENCIMIENTO
				" UNION ALL                                                            "  +
				" SELECT /*+index(ff) use_nl(ff me e i p c eff)*/                      "  +
				"        ff.ic_flujo_fondos                                            "  +
				"   FROM                                                               "  +
				"      int_flujo_fondos  ff,                                           "  +
				" 		 cfe_m_encabezado  me,                                           "  +
				" 		 comcat_epo        e,                                            "  +
				" 		 comcat_if         i,                                            "  +
				" 		 comcat_pyme       p,                                            "  +
				" 		 com_cuentas       c,                                            "  +
				" 		 comcat_estatus_ff eff                                           "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos  = me.idoperacion_cen_i                    "  +
				"    AND ff.ic_epo           = e.ic_epo                                "  +
				"    AND ff.ic_if            = i.ic_if(+)                              "  +
				"    AND ff.ic_pyme          = p.ic_pyme(+)                            "  +
				"    AND ff.ic_cuenta        = c.ic_cuenta(+)                          "  +
				"    AND me.status_cen_i     = eff.ic_estatus_ff(+)                    "  +
				"    AND ff.cs_reproceso     IN ('N', 'D')                             "  +
				"    AND ff.cs_concluido     = 'S'                                     "  +
				"    AND (ff.df_fecha_venc   IS NOT NULL OR ff.df_operacion IS NOT NULL) "  +
				"    AND ff.ic_estatus_docto IN ( 4, 11 )                              "  +
				"    AND FF.df_registro      >= TO_DATE(?,'dd/mm/yyyy')                "  + // DF_ENVIO_FLUJO_FONDOS_OPR
				"    AND FF.df_registro      < (TO_DATE(?,'dd/mm/yyyy') + 1)           "  + // DF_ENVIO_FLUJO_FONDOS_OPR
				"    AND FF.ic_epo           = ?                                       "  + // IC_EPO
				"    AND FF.ic_moneda        = ?                                       "  + // IC_MONEDA
				"    AND FF.df_fecha_venc    >= TO_DATE(?,'dd/mm/yyyy')                "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc    <  TO_DATE(?,'dd/mm/yyyy') + 1            "    // DC_VENCIMIENTO
			);
		}
		
		if( hayEnvioFlujoFondosOpr && hayEnvioFlujoFondosSnOpr ){
			query.append(
				" UNION ALL                                                            "
			);
		}
		
		if( hayEnvioFlujoFondosSnOpr ){
			query.append(
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff btt)*/    "  +
				"        ff.ic_flujo_fondos                                            "  +
				"   FROM                                                               "  +
				"        int_flujo_fondos  ff,                                         "  +
				"        cfe_m_encabezado  me,                                         "  +
				"        comrel_nafin      crn,                                        "  +
				"        comcat_epo        e,                                          "  +
				"        comcat_if         i,                                          "  +
				"        comcat_pyme       p,                                          "  +
				"        com_cuentas       c,                                          "  +
				"        comcat_estatus_ff eff,                                        "  +
				"        comcat_bancos_tef btef                                        "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos   =  me.idoperacion_cen_i                  "  +
				"    AND ff.ic_epo            =  e.ic_epo                              "  +
				"    AND ff.ic_if             =  i.ic_if(+)                            "  +
				"    AND ff.ic_pyme           =  p.ic_pyme(+)                          "  +
				"    AND ff.ic_cuenta         =  c.ic_cuenta                           "  +
				"    AND c.ic_bancos_tef      =  btef.ic_bancos_tef                    "  +
				"    AND me.status_cen_i      =  eff.ic_estatus_ff(+)                  "  +
				"    AND ff.cs_reproceso      =  'N'                                   "  +
				"    AND ff.cs_concluido      =  'N'                                   "  +
				"    AND crn.ic_epo_pyme_if   =  NVL (ff.ic_pyme, ff.ic_if)            "  +
   			"    AND crn.cg_tipo          =  DECODE (ff.ic_pyme, NULL, 'I', 'P')   "  +
   			"    AND c.ic_producto_nafin  =  1                                     "  +
   			"    AND (FF.df_fecha_venc is not null or FF.df_operacion is not null) "  +   
   			"    and ff.ic_estatus_docto  in ( 9, 10 )                             "  +
   			"    AND FF.df_registro       >= TO_DATE(?,'dd/mm/yyyy')               "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
				"    AND FF.df_registro       <  (TO_DATE(?,'dd/mm/yyyy')+1)           "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
				"    AND FF.ic_epo            = ?                                      "  + // IC_EPO
				"    AND FF.ic_moneda         = ?                                      "  + // IC_MONEDA
				"    AND FF.df_fecha_venc     >= TO_DATE(?,'dd/mm/yyyy')               "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc     <  TO_DATE(?,'dd/mm/yyyy') + 1           "  + // DC_VENCIMIENTO
				" UNION ALL                                                            "  +
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/        "  +
				"        ff.ic_flujo_fondos                                            "  +
				"   FROM                                                               "  +
				"        int_flujo_fondos_err ff,                                      "  +
				"        cfe_m_encabezado_err me,                                      "  +
				"        comrel_nafin         crn,                                     "  +
				"        comcat_epo           e,                                       "  +
				"        comcat_if            i,                                       "  +
				"        comcat_pyme          p,                                       "  +
				"        com_cuentas          c,                                       "  +
				"        comcat_estatus_ff   eff                                       "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos      = me.idoperacion_cen_i                "  +
				"    AND ff.ic_epo               = e.ic_epo                            "  +
				"    AND ff.ic_if                = i.ic_if(+)                          "  +
				"    AND ff.ic_pyme              = p.ic_pyme(+)                        "  +
				"    AND ff.ic_cuenta            = c.ic_cuenta(+)                      "  +
				"    AND me.status_cen_i         = eff.ic_estatus_ff(+)                "  +
				"    AND ff.cs_reproceso         = 'N'                                 "  +
				"    AND ff.cs_concluido         = 'N'                                 "  +
				"    AND crn.ic_epo_pyme_if      = NVL (ff.ic_pyme, ff.ic_if)          "  +
   			"    AND crn.cg_tipo             = DECODE (ff.ic_pyme, NULL, 'I', 'P') "  +
   			"    AND c.ic_tipo_cuenta (+)    = 40                                  "  +
   			"    AND c.ic_producto_nafin (+) = 1                                   "  +
   			"    AND c.ic_moneda (+)         = 1                                   "  +
   			"    AND (FF.df_fecha_venc is not null or FF.df_operacion is not null) "  +
   			"    AND ff.ic_estatus_docto     in ( 9, 10 )                          "  +
				"    AND FF.df_registro          >= TO_DATE(?,'dd/mm/yyyy')            "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
				"    AND FF.df_registro          < (TO_DATE(?,'dd/mm/yyyy')+1)         "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
				"    AND FF.ic_epo               = ?                                   "  + // IC_EPO
				"    AND FF.ic_moneda            = ?                                   "  + // IC_MONEDA
				"    AND FF.df_fecha_venc        >= TO_DATE(?,'dd/mm/yyyy')            "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc        <  TO_DATE(?,'dd/mm/yyyy') + 1        "  + // DC_VENCIMIENTO
				" UNION ALL                                                            "  +
				" SELECT /*+index(ff) use_nl(ff me e i p c eff)*/                      "  +
				"        ff.ic_flujo_fondos                                            "  +
				"   FROM                                                               "  +
				"      int_flujo_fondos  ff,                                           "  +
				" 		 cfe_m_encabezado  me,                                           "  +
				" 		 comcat_epo        e,                                            "  +
				" 		 comcat_if         i,                                            "  +
				" 		 comcat_pyme       p,                                            "  +
				" 		 com_cuentas       c,                                            "  +
				" 		 comcat_estatus_ff eff                                           "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos  = me.idoperacion_cen_i                    "  +
				"    AND ff.ic_epo           = e.ic_epo                                "  +
				"    AND ff.ic_if            = i.ic_if(+)                              "  +
				"    AND ff.ic_pyme          = p.ic_pyme(+)                            "  +
				"    AND ff.ic_cuenta        = c.ic_cuenta(+)                          "  +
				"    AND me.status_cen_i     = eff.ic_estatus_ff(+)                    "  +
				"    AND ff.cs_reproceso     IN ('N', 'D')                             "  +
				"    AND ff.cs_concluido     = 'S'                                     "  +
				"    AND (ff.df_fecha_venc   IS NOT NULL OR ff.df_operacion IS NOT NULL) "  +
				"    AND ff.ic_estatus_docto IN ( 9, 10 )                              "  +
				"    AND FF.df_registro      >= TO_DATE(?,'dd/mm/yyyy')                "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
				"    AND FF.df_registro      < (TO_DATE(?,'dd/mm/yyyy') + 1)           "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
				"    AND FF.ic_epo           = ?                                       "  + // IC_EPO
				"    AND FF.ic_moneda        = ?                                       "  + // IC_MONEDA
				"    AND FF.df_fecha_venc    >= TO_DATE(?,'dd/mm/yyyy')                "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc    <  TO_DATE(?,'dd/mm/yyyy') + 1            "	 // DC_VENCIMIENTO		
			);
		}
		
		if( hayEnvioFlujoFondosOpr ){
			
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
			
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
			
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
		
		}
		
		if( hayEnvioFlujoFondosSnOpr ){
			
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
			
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
			
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
		
		}
		
		log.debug("getDocumentQuery.fechaEnvioFlujoFondosOpr    	= <" + this.fechaEnvioFlujoFondosOpr  	+ ">");
		log.debug("getDocumentQuery.fechaEnvioFlujoFondosSnOpr  	= <" + this.fechaEnvioFlujoFondosSnOpr	+ ">");
		log.debug("getDocumentQuery.claveEpo                		= <" + this.claveEpo          		 	+ ">");
		log.debug("getDocumentQuery.claveMoneda                	= <" + this.claveMoneda          		+ ">");
		log.debug("getDocumentQuery.fechaVencimiento           	= <" + this.fechaVencimiento          	+ ">");
				
		log.debug("getDocumentQuery.query                      	= <" + query.toString()     			   + ">");
		log.debug("getDocumentQuery.conditions                 	= <" + conditions                      + ">");
		
		return query.toString();
		
	}//getDocumentQuery
	
	/**
	 * M�todo para obtener el los registros por p�gina 
	 * @return 
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		
		query 							= new StringBuffer();
		conditions 						= new ArrayList();
 
		// Preparar variables bind
		StringBuffer variablesBind	= new StringBuffer();
		for(int i=0;i<pageIds.size();i++) {
			if(i>0){
				variablesBind.append(",");
			}
			variablesBind.append("?");
		};
		
		// Construir query
		query.append(
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff btt)*/     "  +
				"    i.cg_razon_social                            as NOMBRE_IF,         "  +
				"    p.cg_razon_social                            as NOMBRE_PYME,       "  +
				"    p.cg_rfc                                     as RFC_PYME,          "  +
				"    c.ic_bancos_tef                              as CLAVE_BANCO,       "  +
				"    c.ic_tipo_cuenta                             as TIPO_CUENTA,       "  +
				"    c.cg_cuenta                                  as CUENTA_CLABE_SWIFT,      "  +
				"    ff.fn_importe                                as IMPORTE_TOTAL_DESCUENTO, "  +
				"    ff.ic_flujo_fondos                           as FOLIO,             "  +
				"    me.status_cen_i || ' ' || eff.cd_descripcion as ESTATUS_FFON,      "  +
				"    TO_CHAR (ff.df_registro, 'dd/mm/yyyy')       as FECHA_REGISTRO,    "  +
				"    ff.cg_via_liquidacion                        as VIA_LIQUIDACION,   "  +
				"	  me.status_cen_i										  as CLAVE_ESTATUS_FFON,"  +
				"	  c.ic_estatus_cecoban								  as ESTATUS_CECOBAN,	"  +
				"    me.error_cen_s										  as CAUSA_RECHAZO_FFON "  +
				"   FROM                                                                "  +
				"        int_flujo_fondos  ff,                                          "  +
				"        cfe_m_encabezado  me,                                          "  +
				"        comrel_nafin      crn,                                         "  +
				"        comcat_epo        e,                                           "  +
				"        comcat_if         i,                                           "  +
				"        comcat_pyme       p,                                           "  +
				"        com_cuentas       c,                                           "  +
				"        comcat_estatus_ff eff,                                         "  +
				"        comcat_bancos_tef btef                                         "  +
				"  WHERE                                                                "  +
				"        ff.ic_flujo_fondos   =  me.idoperacion_cen_i                   "  +
				"    AND ff.ic_epo            =  e.ic_epo                               "  +
				"    AND ff.ic_if             =  i.ic_if(+)                             "  +
				"    AND ff.ic_pyme           =  p.ic_pyme(+)                           "  +
				"    AND ff.ic_cuenta         =  c.ic_cuenta                            "  +
				"    AND c.ic_bancos_tef      =  btef.ic_bancos_tef                     "  +
				"    AND me.status_cen_i      =  eff.ic_estatus_ff(+)                   "  +
				"    AND ff.cs_reproceso      =  'N'                                    "  +
				"    AND ff.cs_concluido      =  'N'                                    "  +
				"    AND crn.ic_epo_pyme_if   =  NVL (ff.ic_pyme, ff.ic_if)             "  +
   			"    AND crn.cg_tipo          =  DECODE (ff.ic_pyme, NULL, 'I', 'P')    "  +
   			"    AND c.ic_producto_nafin  =  1                                      "  +
   			"    AND (FF.df_fecha_venc is not null or FF.df_operacion is not null)  "  +   
   			"    AND ff.ic_estatus_docto  in ( 4, 9, 10, 11 )                       "
   	);
   	query.append("    AND ff.ic_flujo_fondos   in (");
   	query.append(variablesBind);
   	query.append(") ");
   	query.append(
				" UNION ALL                                                                 	"  +
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/             	"  +
				"        i.cg_razon_social                             as NOMBRE_IF,        	"  +
				"        p.cg_razon_social                             as NOMBRE_PYME,      	"  +
				"        p.cg_rfc                                      as RFC_PYME,         	"  +
				"        c.ic_bancos_tef                               as CLAVE_BANCO,      	"  +
				"        c.ic_tipo_cuenta                              as TIPO_CUENTA,      	"  +
				"        c.cg_cuenta                                   as CUENTA_CLABE_SWIFT,      "  +
				"        ff.fn_importe                                 as IMPORTE_TOTAL_DESCUENTO, "  +
				"        ff.ic_flujo_fondos                            as FOLIO,            	"  +
				"        me.status_cen_i || ' ' || eff.cd_descripcion  as ESTATUS_FFON,     	"  +
				"        TO_CHAR (ff.df_registro, 'dd/mm/yyyy')        as FECHA_REGISTRO,   	"  +
				"        ff.cg_via_liquidacion                         as VIA_LIQUIDACION,   	"  +
				"	  		me.status_cen_i										 as CLAVE_ESTATUS_FFON, "  +
				"	  		c.ic_estatus_cecoban								    as ESTATUS_CECOBAN,	   "  +
				"        me.error_cen_s										    as CAUSA_RECHAZO_FFON  "  +
				"   FROM                                                               "  +
				"        int_flujo_fondos_err ff,                                      "  +
				"        cfe_m_encabezado_err me,                                      "  +
				"        comrel_nafin         crn,                                     "  +
				"        comcat_epo           e,                                       "  +
				"        comcat_if            i,                                       "  +
				"        comcat_pyme          p,                                       "  +
				"        com_cuentas          c,                                       "  +
				"        comcat_estatus_ff   eff                                       "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos      = me.idoperacion_cen_i                "  +
				"    AND ff.ic_epo               = e.ic_epo                            "  +
				"    AND ff.ic_if                = i.ic_if(+)                          "  +
				"    AND ff.ic_pyme              = p.ic_pyme(+)                        "  +
				"    AND ff.ic_cuenta            = c.ic_cuenta(+)                      "  +
				"    AND me.status_cen_i         = eff.ic_estatus_ff(+)                "  +
				"    AND ff.cs_reproceso         = 'N'                                 "  +
				"    AND ff.cs_concluido         = 'N'                                 "  +
				"    AND crn.ic_epo_pyme_if      = NVL (ff.ic_pyme, ff.ic_if)          "  +
   			"    AND crn.cg_tipo             = DECODE (ff.ic_pyme, NULL, 'I', 'P') "  +
   			"    AND c.ic_tipo_cuenta (+)    = 40                                  "  +
   			"    AND c.ic_producto_nafin (+) = 1                                   "  +
   			"    AND c.ic_moneda (+)         = 1                                   "  +
   			"    AND (FF.df_fecha_venc is not null or FF.df_operacion is not null) "  +
   			"    AND ff.ic_estatus_docto     IN ( 4, 9, 10, 11 )                   "
   		);
   		query.append("    AND ff.ic_flujo_fondos   in (");
   		query.append(variablesBind);
   		query.append(") ");
   		query.append(
				" UNION ALL                                                              		"  +
				" SELECT /*+index(ff) use_nl(ff me e i p c eff)*/                        		"  +
				"		  i.cg_razon_social                             as NOMBRE_IF,      		"  + 
				"		  p.cg_razon_social                             as NOMBRE_PYME,    		"  + 
				"		  p.cg_rfc                                      as RFC_PYME,       		"  +
				" 		  c.ic_bancos_tef                               as CLAVE_BANCO,    		"  +
				"		  c.ic_tipo_cuenta                              as TIPO_CUENTA,    		"  + 
				"		  c.cg_cuenta                                   as CUENTA_CLABE_SWIFT,      "  +
				"		  ff.fn_importe                                 as IMPORTE_TOTAL_DESCUENTO, "  +
				" 		  ff.ic_flujo_fondos                            as FOLIO,          		"  + 
				"		  me.status_cen_i || ' ' || eff.cd_descripcion  as ESTATUS_FFON,   		"  +
				" 		  TO_CHAR (ff.df_registro, 'dd/mm/yyyy')        as FECHA_REGISTRO, 		"  + 
				"		  ff.cg_via_liquidacion                         as VIA_LIQUIDACION,   	"  +
				"	  	  me.status_cen_i										   as CLAVE_ESTATUS_FFON,  "  +
				"	  	  c.ic_estatus_cecoban								   as ESTATUS_CECOBAN,	   "  +
				"       NULL								   				   as CAUSA_RECHAZO_FFON   "  + 
				"   FROM                                                               "  +
				"      int_flujo_fondos  ff,                                           "  +
				" 		 cfe_m_encabezado  me,                                           "  +
				" 		 comcat_epo        e,                                            "  +
				" 		 comcat_if         i,                                            "  +
				" 		 comcat_pyme       p,                                            "  +
				" 		 com_cuentas       c,                                            "  +
				" 		 comcat_estatus_ff eff                                           "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos  = me.idoperacion_cen_i                    "  +
				"    AND ff.ic_epo           = e.ic_epo                                "  +
				"    AND ff.ic_if            = i.ic_if(+)                              "  +
				"    AND ff.ic_pyme          = p.ic_pyme(+)                            "  +
				"    AND ff.ic_cuenta        = c.ic_cuenta(+)                          "  +
				"    AND me.status_cen_i     = eff.ic_estatus_ff(+)                    "  +
				"    AND ff.cs_reproceso     IN ('N', 'D')                             "  +
				"    AND ff.cs_concluido     = 'S'                                     "  +
				"    AND (ff.df_fecha_venc   IS NOT NULL OR ff.df_operacion IS NOT NULL) "  +
				"    AND ff.ic_estatus_docto IN ( 4, 9, 10, 11 )                       "  
		);
   	query.append("    AND ff.ic_flujo_fondos   in (");
   	query.append(variablesBind);
   	query.append(") ");
 
		// Agregar condiciones
		for(int i=0;i<pageIds.size();i++) {
			List item = (List)pageIds.get(i);
			conditions.add(item.get(0).toString());	
		}
		
		for(int i=0;i<pageIds.size();i++) {
			List item = (List)pageIds.get(i);
			conditions.add(item.get(0).toString());	
		}
		
		for(int i=0;i<pageIds.size();i++) {
			List item = (List)pageIds.get(i);
			conditions.add(item.get(0).toString());	
		}
		
		for(int i=0;i<pageIds.size();i++) {
			List item = (List)pageIds.get(i);
			log.debug("getDocumentSummaryQueryForIds.pageIds("+i+") = <" + item.get(0).toString() +">");
		}
		
		log.debug("getDocumentSummaryQueryForIds.query                   = <" + query.toString()     			+">");
		log.debug("getDocumentSummaryQueryForIds.conditions              = <" + conditions                   	+">");
		
		return query.toString();
		
	}//getDocumentSummaryQueryForIds
	
	/**
	 * M�todo para calcular los totales 
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		
		query 							= new StringBuffer();
		conditions 						= new ArrayList();
		
		boolean hayEnvioFlujoFondosOpr	= fechaEnvioFlujoFondosOpr   == null || fechaEnvioFlujoFondosOpr.matches("\\s*")  ?false:true;
		boolean hayEnvioFlujoFondosSnOpr = fechaEnvioFlujoFondosSnOpr == null || fechaEnvioFlujoFondosSnOpr.matches("\\s*")?false:true;
		
		query.append(
			" SELECT                                                               "  +
			"        COUNT(1)          AS TOTAL_OPERACIONES,                       "  +
			"        SUM(A.FN_IMPORTE) AS IMPORTE_TOTAL_DESCUENTO                  "  +
			" FROM (                                                               "
		);
		
		if( hayEnvioFlujoFondosOpr ){
			query.append(
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff btt)*/    "  +
				"        ff.fn_importe                                                 "  +
				"   FROM                                                               "  +
				"        int_flujo_fondos  ff,                                         "  +
				"        cfe_m_encabezado  me,                                         "  +
				"        comrel_nafin      crn,                                        "  +
				"        comcat_epo        e,                                          "  +
				"        comcat_if         i,                                          "  +
				"        comcat_pyme       p,                                          "  +
				"        com_cuentas       c,                                          "  +
				"        comcat_estatus_ff eff,                                        "  +
				"        comcat_bancos_tef btef                                        "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos   =  me.idoperacion_cen_i                  "  +
				"    AND ff.ic_epo            =  e.ic_epo                              "  +
				"    AND ff.ic_if             =  i.ic_if(+)                            "  +
				"    AND ff.ic_pyme           =  p.ic_pyme(+)                          "  +
				"    AND ff.ic_cuenta         =  c.ic_cuenta                           "  +
				"    AND c.ic_bancos_tef      =  btef.ic_bancos_tef                    "  +
				"    AND me.status_cen_i      =  eff.ic_estatus_ff(+)                  "  +
				"    AND ff.cs_reproceso      =  'N'                                   "  +
				"    AND ff.cs_concluido      =  'N'                                   "  +
				"    AND crn.ic_epo_pyme_if   =  NVL (ff.ic_pyme, ff.ic_if)            "  +
   			"    AND crn.cg_tipo          =  DECODE (ff.ic_pyme, NULL, 'I', 'P')   "  +
   			"    AND c.ic_producto_nafin  =  1                                     "  +
   			"    AND (FF.df_fecha_venc is not null or FF.df_operacion is not null) "  +   
   			"    AND ff.ic_estatus_docto  in ( 4, 11 )                             "  +
   			"    AND FF.df_registro       >= TO_DATE(?,'dd/mm/yyyy')               "  + // DF_ENVIO_FLUJO_FONDOS_OPR
				"    AND FF.df_registro       <  (TO_DATE(?,'dd/mm/yyyy')+1)           "  + // DF_ENVIO_FLUJO_FONDOS_OPR
				"    AND FF.ic_epo            =  ?                                     "  + // IC_EPO
				"    AND FF.ic_moneda         =  ?                                     "  + // IC_MONEDA
				"    AND FF.df_fecha_venc     >= TO_DATE(?,'dd/mm/yyyy')               "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc     <  TO_DATE(?,'dd/mm/yyyy') + 1           "  + // DC_VENCIMIENTO
				" UNION ALL                                                            "  +
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/        "  +
				"        ff.fn_importe                                                 "  +
				"   FROM                                                               "  +
				"        int_flujo_fondos_err ff,                                      "  +
				"        cfe_m_encabezado_err me,                                      "  +
				"        comrel_nafin         crn,                                     "  +
				"        comcat_epo           e,                                       "  +
				"        comcat_if            i,                                       "  +
				"        comcat_pyme          p,                                       "  +
				"        com_cuentas          c,                                       "  +
				"        comcat_estatus_ff   eff                                       "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos      = me.idoperacion_cen_i                "  +
				"    AND ff.ic_epo               = e.ic_epo                            "  +
				"    AND ff.ic_if                = i.ic_if(+)                          "  +
				"    AND ff.ic_pyme              = p.ic_pyme(+)                        "  +
				"    AND ff.ic_cuenta            = c.ic_cuenta(+)                      "  +
				"    AND me.status_cen_i         = eff.ic_estatus_ff(+)                "  +
				"    AND ff.cs_reproceso         = 'N'                                 "  +
				"    AND ff.cs_concluido         = 'N'                                 "  +
				"    AND crn.ic_epo_pyme_if      = NVL (ff.ic_pyme, ff.ic_if)          "  +
   			"    AND crn.cg_tipo             = DECODE (ff.ic_pyme, NULL, 'I', 'P') "  +
   			"    AND c.ic_tipo_cuenta (+)    = 40                                  "  +
   			"    AND c.ic_producto_nafin (+) = 1                                   "  +
   			"    AND c.ic_moneda (+)         = 1                                   "  +
   			"    AND (FF.df_fecha_venc is not null or FF.df_operacion is not null) "  +
   			"    AND ff.ic_estatus_docto     in ( 4, 11 )                          "  +
				"    AND FF.df_registro          >= TO_DATE(?,'dd/mm/yyyy')            "  + // DF_ENVIO_FLUJO_FONDOS_OPR
				"    AND FF.df_registro          < (TO_DATE(?,'dd/mm/yyyy')+1)         "  + // DF_ENVIO_FLUJO_FONDOS_OPR
				"    AND FF.ic_epo               =  ?                                  "  + // IC_EPO
				"    AND FF.ic_moneda            =  ?                                  "  + // IC_MONEDA
				"    AND FF.df_fecha_venc        >= TO_DATE(?,'dd/mm/yyyy')            "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc        <  TO_DATE(?,'dd/mm/yyyy') + 1        "  + // DC_VENCIMIENTO
				" UNION ALL                                                            "  +
				" SELECT /*+index(ff) use_nl(ff me e i p c eff)*/                      "  +
				"        ff.fn_importe                                                 "  +
				"   FROM                                                               "  +
				"      int_flujo_fondos  ff,                                           "  +
				" 		 cfe_m_encabezado  me,                                           "  +
				" 		 comcat_epo        e,                                            "  +
				" 		 comcat_if         i,                                            "  +
				" 		 comcat_pyme       p,                                            "  +
				" 		 com_cuentas       c,                                            "  +
				" 		 comcat_estatus_ff eff                                           "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos  = me.idoperacion_cen_i                    "  +
				"    AND ff.ic_epo           = e.ic_epo                                "  +
				"    AND ff.ic_if            = i.ic_if(+)                              "  +
				"    AND ff.ic_pyme          = p.ic_pyme(+)                            "  +
				"    AND ff.ic_cuenta        = c.ic_cuenta(+)                          "  +
				"    AND me.status_cen_i     = eff.ic_estatus_ff(+)                    "  +
				"    AND ff.cs_reproceso     IN ('N', 'D')                             "  +
				"    AND ff.cs_concluido     = 'S'                                     "  +
				"    AND (ff.df_fecha_venc   IS NOT NULL OR ff.df_operacion IS NOT NULL) "  +
				"    AND ff.ic_estatus_docto IN ( 4, 11 )                              "  +
				"    AND FF.df_registro      >= TO_DATE(?,'dd/mm/yyyy')                "  + // DF_ENVIO_FLUJO_FONDOS_OPR
				"    AND FF.df_registro      < (TO_DATE(?,'dd/mm/yyyy') + 1)           "  + // DF_ENVIO_FLUJO_FONDOS_OPR
				"    AND FF.ic_epo           =  ?                                      "  + // IC_EPO
				"    AND FF.ic_moneda        =  ?                                      "  + // IC_MONEDA
				"    AND FF.df_fecha_venc    >= TO_DATE(?,'dd/mm/yyyy')                "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc    <  TO_DATE(?,'dd/mm/yyyy') + 1            "    // DC_VENCIMIENTO
			);
		}
		
		if( hayEnvioFlujoFondosOpr && hayEnvioFlujoFondosSnOpr ){
			query.append(
				" UNION ALL                                                            "
			);
		}
		
		if( hayEnvioFlujoFondosSnOpr ){
			query.append(
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff btt)*/    "  +
				"        ff.fn_importe                                                 "  +
				"   FROM                                                               "  +
				"        int_flujo_fondos  ff,                                         "  +
				"        cfe_m_encabezado  me,                                         "  +
				"        comrel_nafin      crn,                                        "  +
				"        comcat_epo        e,                                          "  +
				"        comcat_if         i,                                          "  +
				"        comcat_pyme       p,                                          "  +
				"        com_cuentas       c,                                          "  +
				"        comcat_estatus_ff eff,                                        "  +
				"        comcat_bancos_tef btef                                        "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos   =  me.idoperacion_cen_i                  "  +
				"    AND ff.ic_epo            =  e.ic_epo                              "  +
				"    AND ff.ic_if             =  i.ic_if(+)                            "  +
				"    AND ff.ic_pyme           =  p.ic_pyme(+)                          "  +
				"    AND ff.ic_cuenta         =  c.ic_cuenta                           "  +
				"    AND c.ic_bancos_tef      =  btef.ic_bancos_tef                    "  +
				"    AND me.status_cen_i      =  eff.ic_estatus_ff(+)                  "  +
				"    AND ff.cs_reproceso      =  'N'                                   "  +
				"    AND ff.cs_concluido      =  'N'                                   "  +
				"    AND crn.ic_epo_pyme_if   =  NVL (ff.ic_pyme, ff.ic_if)            "  +
   			"    AND crn.cg_tipo          =  DECODE (ff.ic_pyme, NULL, 'I', 'P')   "  +
   			"    AND c.ic_producto_nafin  =  1                                     "  +
   			"    AND (FF.df_fecha_venc is not null or FF.df_operacion is not null) "  +   
   			"    and ff.ic_estatus_docto  in ( 9, 10 )                             "  +
   			"    AND FF.df_registro       >= TO_DATE(?,'dd/mm/yyyy')               "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
				"    AND FF.df_registro       <  (TO_DATE(?,'dd/mm/yyyy')+1)           "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
				"    AND FF.ic_epo            =  ?                                     "  + // IC_EPO
				"    AND FF.ic_moneda         =  ?                                     "  + // IC_MONEDA
				"    AND FF.df_fecha_venc     >= TO_DATE(?,'dd/mm/yyyy')               "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc     <  TO_DATE(?,'dd/mm/yyyy') + 1           "  + // DC_VENCIMIENTO
				" UNION ALL                                                            "  +
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/        "  +
				"        ff.fn_importe                                                 "  +
				"   FROM                                                               "  +
				"        int_flujo_fondos_err ff,                                      "  +
				"        cfe_m_encabezado_err me,                                      "  +
				"        comrel_nafin         crn,                                     "  +
				"        comcat_epo           e,                                       "  +
				"        comcat_if            i,                                       "  +
				"        comcat_pyme          p,                                       "  +
				"        com_cuentas          c,                                       "  +
				"        comcat_estatus_ff   eff                                       "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos      = me.idoperacion_cen_i                "  +
				"    AND ff.ic_epo               = e.ic_epo                            "  +
				"    AND ff.ic_if                = i.ic_if(+)                          "  +
				"    AND ff.ic_pyme              = p.ic_pyme(+)                        "  +
				"    AND ff.ic_cuenta            = c.ic_cuenta(+)                      "  +
				"    AND me.status_cen_i         = eff.ic_estatus_ff(+)                "  +
				"    AND ff.cs_reproceso         = 'N'                                 "  +
				"    AND ff.cs_concluido         = 'N'                                 "  +
				"    AND crn.ic_epo_pyme_if      = NVL (ff.ic_pyme, ff.ic_if)          "  +
   			"    AND crn.cg_tipo             = DECODE (ff.ic_pyme, NULL, 'I', 'P') "  +
   			"    AND c.ic_tipo_cuenta (+)    = 40                                  "  +
   			"    AND c.ic_producto_nafin (+) = 1                                   "  +
   			"    AND c.ic_moneda (+)         = 1                                   "  +
   			"    AND (FF.df_fecha_venc is not null or FF.df_operacion is not null) "  +
   			"    AND ff.ic_estatus_docto     in ( 9, 10 )                          "  +
				"    AND FF.df_registro          >= TO_DATE(?,'dd/mm/yyyy')            "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR 
				"    AND FF.df_registro          < (TO_DATE(?,'dd/mm/yyyy')+1)         "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
				"    AND FF.ic_epo               =  ?                                  "  + // IC_EPO
				"    AND FF.ic_moneda            =  ?                                  "  + // IC_MONEDA
				"    AND FF.df_fecha_venc        >= TO_DATE(?,'dd/mm/yyyy')            "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc        <  TO_DATE(?,'dd/mm/yyyy') + 1        "  + // DC_VENCIMIENTO
				" UNION ALL                                                            "  +
				" SELECT /*+index(ff) use_nl(ff me e i p c eff)*/                      "  +
				"        ff.fn_importe                                                 "  +
				"   FROM                                                               "  +
				"      int_flujo_fondos  ff,                                           "  +
				" 		 cfe_m_encabezado  me,                                           "  +
				" 		 comcat_epo        e,                                            "  +
				" 		 comcat_if         i,                                            "  +
				" 		 comcat_pyme       p,                                            "  +
				" 		 com_cuentas       c,                                            "  +
				" 		 comcat_estatus_ff eff                                           "  +
				"  WHERE                                                               "  +
				"        ff.ic_flujo_fondos  = me.idoperacion_cen_i                    "  +
				"    AND ff.ic_epo           = e.ic_epo                                "  +
				"    AND ff.ic_if            = i.ic_if(+)                              "  +
				"    AND ff.ic_pyme          = p.ic_pyme(+)                            "  +
				"    AND ff.ic_cuenta        = c.ic_cuenta(+)                          "  +
				"    AND me.status_cen_i     = eff.ic_estatus_ff(+)                    "  +
				"    AND ff.cs_reproceso     IN ('N', 'D')                             "  +
				"    AND ff.cs_concluido     = 'S'                                     "  +
				"    AND (ff.df_fecha_venc   IS NOT NULL OR ff.df_operacion IS NOT NULL) "  +
				"    AND ff.ic_estatus_docto IN ( 9, 10 )                              "  +
				"    and FF.df_registro      >= TO_DATE(?,'dd/mm/yyyy')                "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
				"    and FF.df_registro      < (TO_DATE(?,'dd/mm/yyyy') + 1)           "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
				"    AND FF.ic_epo           =  ?                                      "  + // IC_EPO
				"    AND FF.ic_moneda        =  ?                                      "  + // IC_MONEDA
				"    AND FF.df_fecha_venc    >= TO_DATE(?,'dd/mm/yyyy')                "  + // DC_VENCIMIENTO
				"    AND FF.df_fecha_venc    <  TO_DATE(?,'dd/mm/yyyy') + 1            "    // DC_VENCIMIENTO
			);
		}
		query.append(
				" ) A "
		);
		
		if( hayEnvioFlujoFondosOpr ){
			
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
			
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
			
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(fechaEnvioFlujoFondosOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
		
		}
		
		if( hayEnvioFlujoFondosSnOpr ){
			
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
			
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
			
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(fechaEnvioFlujoFondosSnOpr);
			conditions.add(claveEpo);
			conditions.add(claveMoneda);
			conditions.add(fechaVencimiento);
			conditions.add(fechaVencimiento);
		
		}
		
		log.debug("getAggregateCalculationQuery.fechaEnvioFlujoFondosOpr    	= <" + this.fechaEnvioFlujoFondosOpr  	+">");
		log.debug("getAggregateCalculationQuery.fechaEnvioFlujoFondosSnOpr  	= <" + this.fechaEnvioFlujoFondosSnOpr	+">");
		log.debug("getAggregateCalculationQuery.claveEpo                		= <" + this.claveEpo          		 	+">");
		log.debug("getAggregateCalculationQuery.claveMoneda                	= <" + this.claveMoneda          		+">");
		log.debug("getAggregateCalculationQuery.fechaVencimiento           	= <" + this.fechaVencimiento          	+">");
 
		log.debug("getAggregateCalculationQuery.query                      	= <" + query.toString()     			  	+">");
		log.debug("getAggregateCalculationQuery.conditions                 	= <" + conditions                     	+">");
		
		return query.toString();
		
	}//getAggregateCalculationQuery
	
	
	public List getConditions() {
		return conditions;
	}
 
	/**
	 * M�todo para generar el Archivo  PDF o CSV con todos los registos generados por la consulta  
	 * @return 
	 */
	public String getDocumentQueryFile(){
		return "";
	}//getDocumentQueryFile
 
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return "";
	}
 						
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {
		return "";
	}
 
	// ATRIBUTOS DE LA CLASE
	
	private String fechaEnvioFlujoFondosOpr;
	private String fechaEnvioFlujoFondosSnOpr;
	private String claveEpo;
	private String claveMoneda;
	private String fechaVencimiento;
 
	
	public String getFechaEnvioFlujoFondosOpr() {
		return fechaEnvioFlujoFondosOpr;
	}
 
	public void setFechaEnvioFlujoFondosOpr(String fechaEnvioFlujoFondosOpr) {
		this.fechaEnvioFlujoFondosOpr = fechaEnvioFlujoFondosOpr;
	}
 
	public String getFechaEnvioFlujoFondosSnOpr() {
		return fechaEnvioFlujoFondosSnOpr;
	}

	public void setFechaEnvioFlujoFondosSnOpr(String fechaEnvioFlujoFondosSnOpr) {
		this.fechaEnvioFlujoFondosSnOpr = fechaEnvioFlujoFondosSnOpr;
	}

	public String getClaveEpo() {
		return claveEpo;
	}

	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	public String getClaveMoneda() {
		return claveMoneda;
	}

	public void setClaveMoneda(String claveMoneda) {
		this.claveMoneda = claveMoneda;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

 
}//ConsultaDispersionEPOEnFFON