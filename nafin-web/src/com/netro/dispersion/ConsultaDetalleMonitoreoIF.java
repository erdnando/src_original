package com.netro.dispersion;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**

	Esta clase se encarga de generar los archivos de la consulta dentro del monitor de dispersion IF, en la pantalla:
		PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - MONITOR ( Tipo de Monitoreo: IF's)
	
	@author jshernandez
	@since  F### - 2013 -- ADMIN NAFIN - Migracion Monitor
	@date	  11/07/2013 10:52:52 a.m.
	
 */
public class ConsultaDetalleMonitoreoIF {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaDetalleMonitoreoIF.class);
	
	/**
	 *	Crea un archivo CSV con el detalle de la Consulta del Monitor IF.
	 *	@throws AppException   
	 *	
	 *	@param directorio <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 *	@param registros <tt>JSONArray</tt> con el detalle de la consulta de IF.
	 * @param summaryData <tt>JSONObject</tt> con los totales agrupados por IF.
	 *
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 *
	 */	
	public static String generaArchivoCSV( String directorio, JSONArray registros, JSONObject summaryData )
		throws AppException {

		log.info("generaArchivoCSV(E)");
		
		FileOutputStream		out 				= null;
		BufferedWriter 		csv 				= null;
		CreaArchivo 			archivo 			= null;
		String					nombreArchivo 	= null;
 
		try {
 
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".csv";
			out 				= new FileOutputStream(directorio+nombreArchivo);
			csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
 
			// Variables asociadas a la consulta
			String 		groupId 						= null;
			String 		icIf 							= null;
			String 		nombreIf						= null;
			String 		nombreEpo 					= null;
			String 		nombrePyme 					= null;
			String 		numeroNafinElectronico	= null;
			String 		habilitada 					= null;
			String 		numeroTotalDocumentos 	= null;
			String 		nombreMoneda 				= null;
			String 		montoTotal 					= null;
			String 		codigoBancoCuentaClabe	= null;
			String 		cuentaClabeSwift 			= null;
			String 		descripcionCecoban 		= null;
			String 		descripcionDolares 		= null;
 
			JSONArray  	summaryDataRegisters 	= null;
			JSONObject 	summaryDataRegister  	= null;
			String     	descripcionTotal01 		= null;
			String 		numeroTotalDocumentos01	= null;
			String 		montoTotal01 				= null;
			
			String   	groupIdAnterior 			= "";
			boolean 		esGrupoNuevo 				= false;
			boolean		agregarTotales				= false;
			boolean  	esUltimoRegistro			= false;
			int 			indiceUltimoRegistro		= registros.size()-1;
			
			for(int i=0,ctaRegistros = 0;i<=indiceUltimoRegistro;i++,ctaRegistros++){
				
				JSONObject  registro = registros.getJSONObject(i);
				groupId  				= registro.getString("IC_IF"); // GROUP_ID
				icIf						= registro.getString("IC_IF");
				nombreIf 				= registro.getString("NOMBRE_IF");
				
				nombreIf 				= nombreIf == null?"":nombreIf.replace(',',' ');
				
				esGrupoNuevo 			= !groupIdAnterior.equals(groupId)?true:false;
				esUltimoRegistro		= i == indiceUltimoRegistro?true:false;
				agregarTotales			= i>0 && esGrupoNuevo?true:false;
 
				// 1. Agregar totales del grupo anterior
				if( agregarTotales ){
					
					summaryDataRegisters 	= summaryData.getJSONArray(groupIdAnterior);
					
					summaryDataRegister 		= summaryDataRegisters.getJSONObject(0); // Total M.N.
					descripcionTotal01 		= summaryDataRegister.getString("NOMBRE_EPO");
					numeroTotalDocumentos01	= summaryDataRegister.getString("NUMERO_TOTAL_DOCUMENTOS");
					montoTotal01 				= summaryDataRegister.getString("MONTO_TOTAL");
					
					descripcionTotal01		= descripcionTotal01 		== null?"":descripcionTotal01.replace(',',' ');
					numeroTotalDocumentos01	= numeroTotalDocumentos01	== null?"":numeroTotalDocumentos01;
					montoTotal01				= montoTotal01					== null?"":montoTotal01.replaceAll("[$,]","");
					
					csv.write("\n");
					csv.write(descripcionTotal01);
					csv.write(",,,,");
					csv.write(numeroTotalDocumentos01);
					csv.write(",,");
					csv.write(montoTotal01);
					csv.write("\n");
					
					summaryDataRegister 		= summaryDataRegisters.getJSONObject(1); // Total USD
					descripcionTotal01 		= summaryDataRegister.getString("NOMBRE_EPO");
					numeroTotalDocumentos01	= summaryDataRegister.getString("NUMERO_TOTAL_DOCUMENTOS");
					montoTotal01 				= summaryDataRegister.getString("MONTO_TOTAL");
					
					descripcionTotal01		= descripcionTotal01 		== null?"":descripcionTotal01.replace(',',' ');
					numeroTotalDocumentos01	= numeroTotalDocumentos01	== null?"":numeroTotalDocumentos01;
					montoTotal01				= montoTotal01					== null?"":montoTotal01.replaceAll("[$,]","");
 
					csv.write(descripcionTotal01);
					csv.write(",,,,");
					csv.write(numeroTotalDocumentos01);
					csv.write(",,");
					csv.write(montoTotal01);
					csv.write("\n");
					csv.write("\n");
					
				}
				
				// 2. Agregar Nombre del Intermediario Financiero
				if( esGrupoNuevo ){
					csv.write("\n");
					csv.write(nombreIf);
				}
				
				// 3. Agregar cabecera
				if( esGrupoNuevo ){
					csv.write("\nEPO,PYME,Num. Nafin Electronico,Habilitada,Num. Total de Doctos.,Moneda,Monto Total,C�digo Banco Cuenta Clabe,Cuenta CLABE/SWIFT,Descripci�n CECOBAN,Descripci�n D�LARES");
				}
				
				nombreEpo 					= registro.getString("NOMBRE_EPO");
				nombrePyme 					= registro.getString("NOMBRE_PYME");
				numeroNafinElectronico 	= registro.getString("NUMERO_NAFIN_ELECTRONICO");
				habilitada 					= registro.getString("HABILITADA");
				numeroTotalDocumentos 	= registro.getString("NUMERO_TOTAL_DOCUMENTOS");
				nombreMoneda 				= registro.getString("NOMBRE_MONEDA");
				montoTotal 					= registro.getString("MONTO_TOTAL");
				codigoBancoCuentaClabe 	= registro.getString("CODIGO_BANCO_CUENTA_CLABE");
				cuentaClabeSwift 			= registro.getString("CUENTA_CLABE_SWIFT");
				descripcionCecoban 		= registro.getString("DESCRIPCION_CECOBAN");
				descripcionDolares 		= registro.getString("DESCRIPCION_DOLARES");
 
				nombreEpo 					= nombreEpo 					== null?"":nombreEpo.replace(',',' ');
				nombrePyme 					= nombrePyme 					== null?"":nombrePyme.replace(',',' ');
				numeroNafinElectronico 	= numeroNafinElectronico 	== null?"":numeroNafinElectronico;
				habilitada 					= habilitada 					== null?"":habilitada.replace(',',' ');
				numeroTotalDocumentos 	= numeroTotalDocumentos 	== null?"":numeroTotalDocumentos;
				nombreMoneda 				= nombreMoneda 				== null?"":nombreMoneda.replace(',',' ');
				montoTotal 					= montoTotal 					== null?"":montoTotal.replaceAll("[$,]","");
				codigoBancoCuentaClabe 	= codigoBancoCuentaClabe 	== null?"":codigoBancoCuentaClabe;
				cuentaClabeSwift 			= cuentaClabeSwift 			== null?"":cuentaClabeSwift;
				descripcionCecoban 		= descripcionCecoban 		== null?"":descripcionCecoban.replace(',',' ');
				descripcionDolares 		= descripcionDolares 		== null?"":descripcionDolares.replace(',',' ');

				// 4. Agregar contenido del registro
				csv.write("\n");
				csv.write(nombreEpo);					csv.write(", ");
				csv.write(nombrePyme);					csv.write(", ");
				csv.write(numeroNafinElectronico);	csv.write(",");
				csv.write(habilitada);					csv.write(", ");
				csv.write(numeroTotalDocumentos);	csv.write(", ");
				csv.write(nombreMoneda);				csv.write(", ");
				csv.write(montoTotal);					csv.write(", ");
				csv.write(codigoBancoCuentaClabe);	csv.write(", ");
				csv.write(cuentaClabeSwift);			csv.write(", ");
				csv.write(descripcionCecoban);		csv.write(", ");
				csv.write(descripcionDolares);		csv.write(", ");
 
				// 5. Agregar totales del grupo actual
				if( esUltimoRegistro ){
					
					summaryDataRegisters 	= summaryData.getJSONArray(groupId);
					
					summaryDataRegister 		= summaryDataRegisters.getJSONObject(0); // Total M.N.
					descripcionTotal01 		= summaryDataRegister.getString("NOMBRE_EPO");
					numeroTotalDocumentos01	= summaryDataRegister.getString("NUMERO_TOTAL_DOCUMENTOS");
					montoTotal01 				= summaryDataRegister.getString("MONTO_TOTAL");
					
					descripcionTotal01		= descripcionTotal01 		== null?"":descripcionTotal01.replace(',',' ');
					numeroTotalDocumentos01	= numeroTotalDocumentos01	== null?"":numeroTotalDocumentos01;
					montoTotal01				= montoTotal01					== null?"":montoTotal01.replaceAll("[$,]","");
					
					csv.write("\n");
					csv.write(descripcionTotal01);
					csv.write(",,,,");
					csv.write(numeroTotalDocumentos01);
					csv.write(",,");
					csv.write(montoTotal01);
					csv.write("\n");
					
					summaryDataRegister 		= summaryDataRegisters.getJSONObject(1); // Total USD
					descripcionTotal01 		= summaryDataRegister.getString("NOMBRE_EPO");
					numeroTotalDocumentos01	= summaryDataRegister.getString("NUMERO_TOTAL_DOCUMENTOS");
					montoTotal01 				= summaryDataRegister.getString("MONTO_TOTAL");
					
					descripcionTotal01		= descripcionTotal01 		== null?"":descripcionTotal01.replace(',',' ');
					numeroTotalDocumentos01	= numeroTotalDocumentos01	== null?"":numeroTotalDocumentos01;
					montoTotal01				= montoTotal01					== null?"":montoTotal01.replaceAll("[$,]","");
 
					csv.write(descripcionTotal01);
					csv.write(",,,,");
					csv.write(numeroTotalDocumentos01);
					csv.write(",,");
					csv.write(montoTotal01);
					csv.write("\n");
					csv.write("\n");
					
				}
				
				// 6. Actualizar group id anterior
				groupIdAnterior = groupId;
				
				// 7. Guardar en disco cada 100 registros
				if( ctaRegistros == 100 ){ 
					csv.flush(); 
					ctaRegistros = 0;
				}

			}
 
		} catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoCSV(OutOfMemoryError)");
			log.error("generaArchivoCSV.message     = <" + om.getMessage() + ">");
			log.error("generaArchivoCSV.freeMemory  = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error("generaArchivoCSV.directorio  = <" + directorio  + ">");
			log.error("generaArchivoCSV.registros   = <" + registros   + ">");
			log.error("generaArchivoCSV.summaryData = <" + summaryData + ">");
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoCSV(Exception)");
			log.error("generaArchivoCSV.message     = <" + e.getMessage() + ">");
			log.error("generaArchivoCSV.directorio  = <" + directorio     + ">");
			log.error("generaArchivoCSV.registros   = <" + registros      + ">");
			log.error("generaArchivoCSV.summaryData = <" + summaryData    + ">");
			e.printStackTrace(); 
			
			throw new AppException("Ocurri� un error al generar el CSV con la informaci�n detallada.");
			
		} finally {
 
			// Cerrar archivo
			if(csv 	!= null )	try{csv.close();}catch(Exception e){};
			
			log.info("generaArchivoCSV(S)");
			
		}
		
		return nombreArchivo;
		
	}
	
	/**
	 *	Crea un archivo PDF con el detalle de la Consulta del Monitor IF.
	 *	@throws AppException   
	 *	
	 *	@param directorio             <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 * @param directorioPublicacion 	<tt>String</tt> con la ruta WEB del directorio de publicacion.
	 *	@param registros              <tt>JSONArray</tt> con el detalle de la consulta de IF.
	 * @param summaryData            <tt>JSONObject</tt> con los totales agrupados por IF.
	 *
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 *
	 */	
	public static String generaArchivoPDF( String directorio, String directorioPublicacion, JSONArray registros, JSONObject summaryData, HttpSession session  )
		throws AppException {

		log.info("generaArchivoPDF(E)");
		
		CreaArchivo	archivo 			= null;
		String		nombreArchivo	= null;
		ComunesPDF	pdf 				= null;
 
		try {
 
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".pdf";
 
			// Sacando la fecha para encabezado de Excel
			String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaActual.substring(0,2);
			String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   	= fechaActual.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			String strEncabezado = "M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
										  " ----------------------------- " + horaActual;

			// Crear Documento PDF
			pdf = new ComunesPDF(2, directorio+nombreArchivo, "", false, true, true);
			pdf.setEncabezado(
				(String)session.getAttribute("strPais"),
				(String)session.getAttribute("iNoNafinElectronico"),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				directorioPublicacion, 
				""
			);
 
			// Variables asociadas a la consulta
			String 		groupId 						= null;
			String 		icIf 							= null;
			String 		nombreIf 					= null;
			String 		nombreEpo 					= null;
			String 		nombrePyme 					= null;
			String 		numeroNafinElectronico	= null;
			String 		habilitada 					= null;
			String 		numeroTotalDocumentos 	= null;
			String 		nombreMoneda 				= null;
			String 		montoTotal 					= null;
			String 		codigoBancoCuentaClabe	= null;
			String 		cuentaClabeSwift 			= null;
			String 		descripcionCecoban 		= null;
			String 		descripcionDolares 		= null;
			
			JSONArray  	summaryDataRegisters 	= null;
			JSONObject 	summaryDataRegister  	= null;
			String     	descripcionTotal01 		= null;
			String 		numeroTotalDocumentos01	= null;
			String 		montoTotal01 				= null;
			
			List titulosReporte = new ArrayList();
			titulosReporte.add("EPO");
			titulosReporte.add("PYME");
			titulosReporte.add("Num. Nafin Electr�nico");
			titulosReporte.add("Habilitada");
			titulosReporte.add("Num. Total de Doctos.");
			titulosReporte.add("Moneda");
			titulosReporte.add("Monto Total");
			titulosReporte.add("C�digo Banco Cuenta Clabe");
			titulosReporte.add("Cuenta CLABE/SWIFT");
			titulosReporte.add("Descripci�n CECOBAN");
			titulosReporte.add("Descripci�n D�LARES");
 	
			String   groupIdAnterior 		= "";
			boolean 	esGrupoNuevo 			= false;
			boolean	agregarTotales			= false;
			boolean  esUltimoRegistro		= false;
			int 		indiceUltimoRegistro	= registros.size()-1;
			
			for(int i=0,ctaRegistros = 0;i<=indiceUltimoRegistro;i++,ctaRegistros++){
				
				JSONObject  registro = registros.getJSONObject(i);
				groupId  				= registro.getString("IC_IF"); // GROUP_ID
				icIf						= registro.getString("IC_IF");
				nombreIf 				= registro.getString("NOMBRE_IF");
				
				nombreIf 				= nombreIf == null?"":nombreIf;
				
				esGrupoNuevo 			= !groupIdAnterior.equals(groupId)?true:false;
				esUltimoRegistro		= i == indiceUltimoRegistro?true:false;
				agregarTotales			= i>0 && esGrupoNuevo?true:false;
 
				// 1. Agregar totales del grupo anterior
				if( agregarTotales ){
					
					summaryDataRegisters 	= summaryData.getJSONArray(groupIdAnterior);
					
					summaryDataRegister 		= summaryDataRegisters.getJSONObject(0); // Total M.N.
					descripcionTotal01 		= summaryDataRegister.getString("NOMBRE_EPO");
					numeroTotalDocumentos01	= summaryDataRegister.getString("NUMERO_TOTAL_DOCUMENTOS");
					montoTotal01 				= summaryDataRegister.getString("MONTO_TOTAL");
					
					descripcionTotal01		= descripcionTotal01 		== null?"":descripcionTotal01;
					numeroTotalDocumentos01	= numeroTotalDocumentos01	== null?"":numeroTotalDocumentos01;
					montoTotal01				= montoTotal01					== null?"":montoTotal01;
 
					pdf.setCell( descripcionTotal01, 		"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( numeroTotalDocumentos01, 	"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( montoTotal01, 				"celda01", ComunesPDF.RIGHT);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					
					summaryDataRegister 		= summaryDataRegisters.getJSONObject(1); // Total USD
					descripcionTotal01 		= summaryDataRegister.getString("NOMBRE_EPO");
					numeroTotalDocumentos01	= summaryDataRegister.getString("NUMERO_TOTAL_DOCUMENTOS");
					montoTotal01 				= summaryDataRegister.getString("MONTO_TOTAL");
					
					descripcionTotal01		= descripcionTotal01 		== null?"":descripcionTotal01;
					numeroTotalDocumentos01	= numeroTotalDocumentos01	== null?"":numeroTotalDocumentos01;
					montoTotal01				= montoTotal01					== null?"":montoTotal01;
 
					pdf.setCell( descripcionTotal01, 		"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( numeroTotalDocumentos01, 	"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( montoTotal01, 				"celda01", ComunesPDF.RIGHT);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					
					pdf.addTable();
					pdf.addText(" ","formas", ComunesPDF.CENTER );
					
				}
				
				// 2. Agregar Nombre del Intermediario Financiero
				if( esGrupoNuevo ){
					pdf.addText(nombreIf, "formasB", ComunesPDF.CENTER);
				}
				
				// 3. Agregar cabecera
				if( esGrupoNuevo ){
					pdf.setTable(titulosReporte, "celda01", 90);
				}
				
				nombreEpo 					= registro.getString("NOMBRE_EPO");
				nombrePyme 					= registro.getString("NOMBRE_PYME");
				numeroNafinElectronico 	= registro.getString("NUMERO_NAFIN_ELECTRONICO");
				habilitada 					= registro.getString("HABILITADA");
				numeroTotalDocumentos 	= registro.getString("NUMERO_TOTAL_DOCUMENTOS");
				nombreMoneda 				= registro.getString("NOMBRE_MONEDA");
				montoTotal 					= registro.getString("MONTO_TOTAL");
				codigoBancoCuentaClabe 	= registro.getString("CODIGO_BANCO_CUENTA_CLABE");
				cuentaClabeSwift 			= registro.getString("CUENTA_CLABE_SWIFT");
				descripcionCecoban 		= registro.getString("DESCRIPCION_CECOBAN");
				descripcionDolares 		= registro.getString("DESCRIPCION_DOLARES");
 
				nombreEpo 					= nombreEpo 					== null?"":nombreEpo;
				nombrePyme 					= nombrePyme 					== null?"":nombrePyme;
				numeroNafinElectronico 	= numeroNafinElectronico 	== null?"":numeroNafinElectronico;
				habilitada 					= habilitada 					== null?"":habilitada;
				numeroTotalDocumentos 	= numeroTotalDocumentos 	== null?"":numeroTotalDocumentos;
				nombreMoneda 				= nombreMoneda 				== null?"":nombreMoneda;
				montoTotal 					= montoTotal 					== null?"":montoTotal;
				codigoBancoCuentaClabe 	= codigoBancoCuentaClabe 	== null?"":codigoBancoCuentaClabe;
				cuentaClabeSwift 			= cuentaClabeSwift 			== null?"":cuentaClabeSwift;
				descripcionCecoban 		= descripcionCecoban 		== null?"":descripcionCecoban;
				descripcionDolares 		= descripcionDolares 		== null?"":descripcionDolares;

				// 4. Agregar contenido del registro
				pdf.setCell( nombreEpo, 					"formas", ComunesPDF.CENTER);
				pdf.setCell( nombrePyme, 					"formas", ComunesPDF.CENTER);
				pdf.setCell( numeroNafinElectronico,	"formas", ComunesPDF.CENTER);
				pdf.setCell( habilitada, 					"formas", ComunesPDF.CENTER);
				pdf.setCell( numeroTotalDocumentos, 	"formas", ComunesPDF.CENTER);
				pdf.setCell( nombreMoneda, 				"formas", ComunesPDF.CENTER);
				pdf.setCell( montoTotal, 					"formas", ComunesPDF.RIGHT);
				pdf.setCell( codigoBancoCuentaClabe,	"formas", ComunesPDF.CENTER);
				pdf.setCell( cuentaClabeSwift,			"formas", ComunesPDF.CENTER);
				pdf.setCell( descripcionCecoban,			"formas", ComunesPDF.CENTER);
				pdf.setCell( descripcionDolares,			"formas", ComunesPDF.CENTER);

				// 5. Agregar totales del grupo actual
				if( esUltimoRegistro ){
					
					summaryDataRegisters 	= summaryData.getJSONArray(groupId);
					
					summaryDataRegister 		= summaryDataRegisters.getJSONObject(0); // Total M.N.
					descripcionTotal01 		= summaryDataRegister.getString("NOMBRE_EPO");
					numeroTotalDocumentos01	= summaryDataRegister.getString("NUMERO_TOTAL_DOCUMENTOS");
					montoTotal01 				= summaryDataRegister.getString("MONTO_TOTAL");
					
					descripcionTotal01		= descripcionTotal01 		== null?"":descripcionTotal01;
					numeroTotalDocumentos01	= numeroTotalDocumentos01	== null?"":numeroTotalDocumentos01;
					montoTotal01				= montoTotal01					== null?"":montoTotal01;
 
					pdf.setCell( descripcionTotal01, 		"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( numeroTotalDocumentos01, 	"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( montoTotal01, 				"celda01", ComunesPDF.RIGHT);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					
					summaryDataRegister 		= summaryDataRegisters.getJSONObject(1); // Total USD
					descripcionTotal01 		= summaryDataRegister.getString("NOMBRE_EPO");
					numeroTotalDocumentos01	= summaryDataRegister.getString("NUMERO_TOTAL_DOCUMENTOS");
					montoTotal01 				= summaryDataRegister.getString("MONTO_TOTAL");
					
					descripcionTotal01		= descripcionTotal01 		== null?"":descripcionTotal01;
					numeroTotalDocumentos01	= numeroTotalDocumentos01	== null?"":numeroTotalDocumentos01;
					montoTotal01				= montoTotal01					== null?"":montoTotal01;
 
					pdf.setCell( descripcionTotal01, 		"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( numeroTotalDocumentos01, 	"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( montoTotal01, 				"celda01", ComunesPDF.RIGHT);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					pdf.setCell( "", 								"celda01", ComunesPDF.CENTER);
					
					pdf.addTable();
					pdf.addText(" ","formas", ComunesPDF.CENTER );
					
				}
				
				// 5. Actualizar group id anterior
				groupIdAnterior = groupId;
				
			}
			
			pdf.endDocument();
 
		} catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoPDF(OutOfMemoryError)");
			log.error("generaArchivoPDF.message                = <" + om.getMessage()        + ">");
			log.error("generaArchivoPDF.freeMemory             = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error("generaArchivoPDF.directorio             = <" + directorio             + ">");
			log.error("generaArchivoPDF.directorioPublicacion  = <" + directorioPublicacion  + ">");
			log.error("generaArchivoPDF.registros              = <" + registros              + ">");
			log.error("generaArchivoPDF.summaryData            = <" + summaryData            + ">");
			log.error("generaArchivoPDF.session                = <" + session                + ">");
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoPDF(Exception)");
			log.error("generaArchivoPDF.message                = <" + e.getMessage()         + ">");
			log.error("generaArchivoPDF.directorio             = <" + directorio             + ">");
			log.error("generaArchivoPDF.directorioPublicacion  = <" + directorioPublicacion  + ">");
			log.error("generaArchivoPDF.registros              = <" + registros              + ">");
			log.error("generaArchivoPDF.summaryData            = <" + summaryData            + ">");
			log.error("generaArchivoPDF.session                = <" + session                + ">");
			e.printStackTrace(); 
			
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n detallada.");
			
		} finally {
 
			log.info("generaArchivoPDF(S)");
			
		}
		
		return nombreArchivo;
		
	}
		
}