package com.netro.dispersion;

import com.netro.pdf.ComunesPDF;

//import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
//import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaDispersionFactura  {

	private String claveEpo;
	private String fechaRegIni;
	private String fechaRegFin;
	private String nombreEPO;
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaDispersionFactura.class);
	
	public ConsultaDispersionFactura() {
	}
	
	public String generarArchivoDetalle(String path){   
		log.info("generarArchivoDetalle(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer qrySentencia = new StringBuffer();
		String					nombreArchivo 	= null;
		StringBuffer info = new StringBuffer("");
		
		try{ 
			con.conexionDB();
			
			Dispersion  dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
	
			String txt_dias1 ="",  txt_dias2="", txt_dias1USD ="", txt_dias2USD ="";
						
			List lTarifa 		= new ArrayList();
			List  lTarifaUSD = new ArrayList();
			
			if(!"".equals(claveEpo)) {
				lTarifa = dispersion.getTarifaEpo(claveEpo);
				for(int i=0;i<lTarifa.size();i++) {
					List lDatos = (ArrayList)lTarifa.get(i);
					String rs_dia 		= lDatos.get(0).toString();
					//double rs_tarifa 	= Double.parseDouble(lDatos.get(1).toString());
					switch(i) {
						case 0:
							txt_dias1		= rs_dia;								
						break;
						case 1:
							txt_dias2	= rs_dia;								
						break;							
					}//switch(i)
				}//for(int i=0;i<lRegistro.size();i++)
				
				 lTarifaUSD = dispersion.getTarifaEpoUSD(claveEpo);
				for(int i=0;i<lTarifaUSD.size();i++) {
					List lDatos = (ArrayList)lTarifaUSD.get(i);
					String rs_dia 		= lDatos.get(0).toString();
					//double rs_tarifa 	= Double.parseDouble(lDatos.get(1).toString());
					switch(i) {
						case 0:
							txt_dias1USD		= rs_dia;							
							break;
						case 1:
							txt_dias2USD		= rs_dia;							
							break;
					}//switch(i)
				}//for(int i=0;i<lRegistro.size();i++)				
			}
	 	
				StringBuffer info15 = new StringBuffer("");  
				StringBuffer info16 = new StringBuffer("");
				StringBuffer info15DL = new StringBuffer("");
				StringBuffer info16DL = new StringBuffer("");		

				String	infoEPO 		= "",  infoIF 	= "", infoProvedor	= "",  infoNumerDoc	= "",
						infoAlta 	= "",  infoVence 	= "",  infoDias = "",  infoMoneda = "", infoMonto	= "",  infoEstatus	= "",
						infoGrupo     = "", infoIc_moneda ="";				
				int 	 totalDoc15 	= 0,  totalDoc16 	= 0,  totalDoc15DL = 0,  totalDoc16DL 	= 0;				
				double monto= 0,  totalMonto15	= 0,  totalMonto16	= 0, 	totalMonto15DL	= 0,  totalMonto16DL	= 0;
				int 	 dias	= 16,  diasCompara  = 0,maximo	= 0;
			
			qrySentencia.append(	" SELECT * from  ( ");
			
				if(lTarifa.size()>0) {
			
				qrySentencia.append(	"  SELECT e.cg_razon_social as nombreEpo, p.cg_razon_social as nombrePyme, d.ig_numero_docto, TO_CHAR(d.df_alta,'dd/mm/yyyy') as alta, TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as vence, d.df_fecha_venc - d.df_alta AS dias, "+
					" m.cd_nombre as moneda, d.fn_monto, ed.cd_descripcion as estatusDocto,  "+
					" CASE WHEN (d.df_fecha_venc - d.df_alta) < " +(Integer.parseInt(txt_dias1)+1) +"  THEN 1 "+
					" WHEN (d.df_fecha_venc - d.df_alta) >=" +(Integer.parseInt(txt_dias1)+1) +"  AND (d.df_fecha_venc - d.df_alta) < "+txt_dias2+" THEN 2 "+
					" ELSE 3 END grupo ,  "+
					" m.ic_moneda FROM com_documento d, int_flujo_fondos ff, comrel_nafin n, com_cuentas c, cfe_m_encabezado cfe, comcat_epo e, comcat_pyme p, comcat_moneda m, comcat_estatus_docto ed "+
					" WHERE d.ic_moneda = 1 AND d.ic_epo ="+claveEpo+" AND d.ic_estatus_docto IN (9, 10) AND d.df_fecha_venc >= TO_DATE ('"+fechaRegIni+"', 'dd/mm/yyyy') AND d.df_fecha_venc < (TO_DATE ('"+fechaRegFin+"', 'dd/mm/yyyy') + 1) "+
					" AND (   (    d.cs_dscto_especial IN ('D', 'I') AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 ) OR d.cs_dscto_especial IN ('N', 'V', 'M', 'C') ) "+
					" AND d.ic_pyme = n.ic_epo_pyme_if AND d.ic_epo = ff.ic_epo AND d.ic_moneda = ff.ic_moneda AND d.ic_epo = e.ic_epo AND d.ic_pyme = p.ic_pyme AND d.ic_moneda = m.ic_moneda "+
					" AND d.ic_estatus_docto = ed.ic_estatus_docto AND n.ic_nafin_electronico = c.ic_nafin_electronico AND ff.df_fecha_venc >= d.df_fecha_venc AND ff.df_fecha_venc < (d.df_fecha_venc + 1) "+
					" AND ff.ic_flujo_fondos = cfe.idoperacion_cen_i AND c.ic_cuenta = ff.ic_cuenta AND n.cg_tipo = 'P' AND c.ic_tipo_cuenta = 40 AND c.ic_producto_nafin = 1 AND cfe.status_cen_i > 0  "+
					" UNION ALL "+
					" SELECT e.cg_razon_social as nombreEpo, p.cg_razon_social as nombrePyme, d.ig_numero_docto, TO_CHAR(d.df_alta,'dd/mm/yyyy') as alta, TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as vence, d.df_fecha_venc - d.df_alta AS dias, m.cd_nombre as moneda, d.fn_monto, ed.cd_descripcion as estatusDocto, "+
					" CASE WHEN (d.df_fecha_venc - d.df_alta) < " +(Integer.parseInt(txt_dias1)+1) +"   THEN 1 "+ 
					" WHEN (d.df_fecha_venc - d.df_alta) >= " +(Integer.parseInt(txt_dias1)+1) +"  AND (d.df_fecha_venc - d.df_alta) <  "+txt_dias2+" THEN 2 "+
					" ELSE 3 END grupo  , "+
					" m.ic_moneda  "+
					" FROM com_documento d, int_flujo_fondos ff, comrel_nafin n, com_cuentas c, cfe_m_encabezado cfe, comcat_epo e, comcat_pyme p, comcat_moneda m, comcat_estatus_docto ed "+
					" WHERE d.ic_moneda = 1 AND d.ic_epo ="+claveEpo+" AND d.ic_estatus_docto IN (9, 10) AND d.df_fecha_venc >= TO_DATE ('"+fechaRegIni+"', 'dd/mm/yyyy') AND d.df_fecha_venc < (TO_DATE ('"+fechaRegFin+"', 'dd/mm/yyyy') + 1) "+
					" AND d.cs_dscto_especial IN ('D', 'I') AND (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 AND d.ic_beneficiario = n.ic_epo_pyme_if AND d.ic_epo = ff.ic_epo AND d.ic_moneda = ff.ic_moneda "+
					" AND d.ic_epo = e.ic_epo AND d.ic_pyme = p.ic_pyme AND d.ic_moneda = m.ic_moneda AND d.ic_estatus_docto = ed.ic_estatus_docto AND n.ic_nafin_electronico = c.ic_nafin_electronico "+
					" AND ff.df_fecha_venc >= d.df_fecha_venc AND ff.df_fecha_venc < (d.df_fecha_venc + 1) AND ff.ic_flujo_fondos = cfe.idoperacion_cen_i AND c.ic_cuenta = ff.ic_cuenta  AND n.cg_tipo = 'I'"+
					" AND c.ic_tipo_cuenta = 40 AND c.ic_producto_nafin = 1 AND cfe.status_cen_i > 0   ");
				
				}	
				if(lTarifaUSD.size()>0) {
				
				qrySentencia.append( "  UNION ALL "+	
					
					//****************Dolares*****************+
					" SELECT e.cg_razon_social as nombreEpo, p.cg_razon_social as nombrePyme, d.ig_numero_docto, TO_CHAR(d.df_alta,'dd/mm/yyyy') as alta, TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as vence, d.df_fecha_venc - d.df_alta AS dias, "+
					" m.cd_nombre as moneda, d.fn_monto, ed.cd_descripcion as estatusDocto, "+
					" CASE WHEN (d.df_fecha_venc - d.df_alta) < " +(Integer.parseInt(txt_dias1USD)+1) +"  THEN 1"+
					" WHEN (d.df_fecha_venc - d.df_alta) >= "+(Integer.parseInt(txt_dias1USD)+1) +"  AND (d.df_fecha_venc - d.df_alta) < "+txt_dias2USD+"  THEN 2 "+
					" ELSE 3 END grupo ,  "+
					" m.ic_moneda  FROM com_documento d, int_flujo_fondos ff, comrel_nafin n, com_cuentas c, cfe_m_encabezado cfe, comcat_epo e, comcat_pyme p, comcat_moneda m, comcat_estatus_docto ed "+
					" WHERE d.ic_moneda = 54 AND d.ic_epo ="+claveEpo+" AND d.ic_estatus_docto IN (9, 10) AND d.df_fecha_venc >= TO_DATE ('"+fechaRegIni+"', 'dd/mm/yyyy') AND d.df_fecha_venc < (TO_DATE ('"+fechaRegFin+"', 'dd/mm/yyyy') + 1) "+
					" AND (   (    d.cs_dscto_especial IN ('D', 'I') AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 ) OR d.cs_dscto_especial IN ('N', 'V', 'M', 'C') ) "+
					" AND d.ic_pyme = n.ic_epo_pyme_if AND d.ic_epo = ff.ic_epo AND d.ic_moneda = ff.ic_moneda AND d.ic_epo = e.ic_epo AND d.ic_pyme = p.ic_pyme AND d.ic_moneda = m.ic_moneda "+
					" AND d.ic_estatus_docto = ed.ic_estatus_docto AND n.ic_nafin_electronico = c.ic_nafin_electronico AND ff.df_fecha_venc >= d.df_fecha_venc AND ff.df_fecha_venc < (d.df_fecha_venc + 1) "+
					" AND ff.ic_flujo_fondos = cfe.idoperacion_cen_i AND c.ic_cuenta = ff.ic_cuenta AND n.cg_tipo = 'P' AND c.ic_tipo_cuenta = 50 AND c.ic_producto_nafin = 1 AND cfe.status_cen_i > 0  "+
					" UNION ALL "+
					" SELECT e.cg_razon_social as nombreEpo, p.cg_razon_social as nombrePyme, d.ig_numero_docto, TO_CHAR(d.df_alta,'dd/mm/yyyy') as alta, TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as vence, d.df_fecha_venc - d.df_alta AS dias, m.cd_nombre as moneda, d.fn_monto, ed.cd_descripcion as estatusDocto, "+
					" CASE WHEN (d.df_fecha_venc - d.df_alta) < " +(Integer.parseInt(txt_dias1USD)+1) +"  THEN 1 "+
					" WHEN (d.df_fecha_venc - d.df_alta) >= " +(Integer.parseInt(txt_dias1USD)+1) +"  AND (d.df_fecha_venc - d.df_alta) < "+txt_dias2USD+" THEN 2 "+
					" ELSE 3 END grupo  ,  "+
					" m.ic_moneda "+ 
					" FROM com_documento d, int_flujo_fondos ff, comrel_nafin n, com_cuentas c, cfe_m_encabezado cfe, comcat_epo e, comcat_pyme p, comcat_moneda m, comcat_estatus_docto ed "+
					" WHERE d.ic_moneda = 54 AND d.ic_epo ="+claveEpo+" AND d.ic_estatus_docto IN (9, 10) AND d.df_fecha_venc >= TO_DATE ('"+fechaRegIni+"', 'dd/mm/yyyy') AND d.df_fecha_venc < (TO_DATE ('"+fechaRegFin+"', 'dd/mm/yyyy') + 1) "+
					" AND d.cs_dscto_especial IN ('D', 'I') AND (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 AND d.ic_beneficiario = n.ic_epo_pyme_if AND d.ic_epo = ff.ic_epo AND d.ic_moneda = ff.ic_moneda "+
					" AND d.ic_epo = e.ic_epo AND d.ic_pyme = p.ic_pyme AND d.ic_moneda = m.ic_moneda AND d.ic_estatus_docto = ed.ic_estatus_docto AND n.ic_nafin_electronico = c.ic_nafin_electronico "+
					" AND ff.df_fecha_venc >= d.df_fecha_venc AND ff.df_fecha_venc < (d.df_fecha_venc + 1) AND ff.ic_flujo_fondos = cfe.idoperacion_cen_i AND c.ic_cuenta = ff.ic_cuenta  AND n.cg_tipo = 'I'"+
					" AND c.ic_tipo_cuenta = 50 AND c.ic_producto_nafin = 1 AND cfe.status_cen_i > 0 " );
				}
				
				qrySentencia.append( "  ) b  "+
					" where   grupo < = 2   " +
					" ORDER BY grupo, nombrepyme, ig_numero_docto ");
				
					
			log.info("Consulta Generar Detalle ==== :: "+qrySentencia.toString());			
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			
			while(rs.next() && maximo<1000){ maximo++;
				infoEPO 			= rs.getString(1);
				infoIF 			= "";
				infoProvedor	= rs.getString(2);
				infoNumerDoc	= rs.getString(3);
				infoAlta 		= rs.getString(4); 
				infoVence 		= rs.getString(5); 
				infoDias 		= rs.getString(6);
				infoMoneda		= rs.getString(7);
				infoMonto		= rs.getString(8); 
				infoEstatus		= rs.getString(9);
				infoGrupo		= rs.getString(10);
				infoIc_moneda = rs.getString(11);
				
				diasCompara 	= Integer.parseInt(infoDias);
				monto = Double.parseDouble(infoMonto);
				
				if( infoIc_moneda.equals("1"))  {
					if(diasCompara < dias ){ //dias=16
						totalDoc15++;
						totalMonto15 = totalMonto15 + monto;
						info15.append(infoEPO.replace(',',' ')+","+infoProvedor.replace(',',' ')+","+infoNumerDoc+","+infoAlta+","+infoVence+","+infoDias+","+infoMoneda+",$"+infoMonto+","+infoEstatus+"\n");
					} else if(diasCompara >= dias && diasCompara < 30){ //dias=16
						totalDoc16++;
						totalMonto16 = totalMonto16 + monto;
						info16.append(infoEPO.replace(',',' ')+","+infoProvedor.replace(',',' ')+","+infoNumerDoc+","+infoAlta+","+infoVence+","+infoDias+","+infoMoneda+",$"+infoMonto+","+infoEstatus+"\n");
					}
				}else {
						
					if(diasCompara < dias ){ //dias=16
						totalDoc15DL++;
						totalMonto15DL = totalMonto15DL + monto;
						info15DL.append(infoEPO.replace(',',' ')+","+infoProvedor.replace(',',' ')+","+infoNumerDoc+","+infoAlta+","+infoVence+","+infoDias+","+infoMoneda+",$"+infoMonto+","+infoEstatus+"\n");
					} else if(diasCompara >= dias && diasCompara < 30){ //dias=16
						totalDoc16DL++;
						totalMonto16DL = totalMonto16DL+ monto;
						info16DL.append(infoEPO.replace(',',' ')+","+infoProvedor.replace(',',' ')+","+infoNumerDoc+","+infoAlta+","+infoVence+","+infoDias+","+infoMoneda+",$"+infoMonto+","+infoEstatus+"\n");
					}
				}
			}
			
			info.append(",\n ,Fecha de Inicio: "+fechaRegIni+",,,Fecha de T�rmino: "+fechaRegFin+", \n");
			//************************* MONEDA NACIONAL*****************
			
			if(lTarifa.size()>0) {		
				info.append(", \n Hasta "+txt_dias1+" D�as Inclusive \n");
				info.append("Nombre EPO,Nombre Proveedor,N�mero de Documento,Fecha de Alta,Fecha de Vencimiento,D�as,Moneda,Monto,Estatus \n");
				info.append(info15.toString());
				info.append(",\n Total,,"+totalDoc15+",,,,,$"+totalMonto15+"\n");
						
				info.append(" ,\n De los "+(Integer.parseInt(txt_dias1)+1) +" a los "+txt_dias2+" D�as \n");
				info.append("Nombre EPO,Nombre Proveedor,N�mero de Documento,Fecha de Alta,Fecha de Vencimiento,D�as,Moneda,Monto,Estatus \n");
				info.append(info16.toString());
				info.append(",\n Total,,"+totalDoc16+",,,,,$"+totalMonto16);
			}		
				
			//*************************DOLARES ***************** 
			
			if(lTarifaUSD.size()>0) {
						info.append(", \n Hasta "+txt_dias1USD+" D�as Inclusive \n");
				info.append("Nombre EPO,Nombre Proveedor,N�mero de Documento,Fecha de Alta,Fecha de Vencimiento,D�as,Moneda,Monto,Estatus \n");
				info.append(info15DL.toString());
				info.append(",\n Total,,"+totalDoc15DL+",,,,,$"+totalMonto15DL+"\n");
						
				info.append(" ,\n De los "+(Integer.parseInt(txt_dias1USD)+1)+" a los "+txt_dias2USD+" D�as \n");
				info.append("Nombre EPO,Nombre Proveedor,N�mero de Documento,Fecha de Alta,Fecha de Vencimiento,D�as,Moneda,Monto,Estatus \n");
				info.append(info16DL.toString());
				info.append(",\n Total,,"+totalDoc16DL+",,,,,$"+totalMonto16DL);				
			}
			
			// Crear Archivo
			CreaArchivo archivo = new CreaArchivo();
			if (archivo.make(info.toString(), path, ".csv"))
				nombreArchivo = archivo.nombre;	
			
		}catch(Exception e){
			log.error("generarArchivoDetalle(Error):: "+e.getMessage());
			e.printStackTrace();
		}finally{ 
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
		return nombreArchivo;
	}
	
	public String generarArchivo(String path,String strNombreUsuario, String txt_dias1, String txt_dias2, String txt_dias1USD, String txt_dias2USD, boolean esTarifaEnMonedaNacionalValida, boolean esTarifaEnDolaresAmericanosValida, String detalle, String detalleMN, String detalleUSD,String tipo,HttpServletRequest request){
			
			ComunesPDF pdfDoc = new ComunesPDF();
			HttpSession session = request.getSession();
			
			/*HashMap  registro 				= null;
			String 	rowSpan 					= "1";
			String 	hayDocumentosMN 		= "";
			String 	hayDocumentosUSD 		= ""; 
			String 	nombre_intermediario	= "";
			String   monto_mn 				= "";
			String   numero_mn 				= "";
			String 	monto_usd 				= "";
			String   numero_usd 				= "";	
			int iTotOper 				= 0;
			BigDecimal bMtoOper 		= new BigDecimal(0);
			
			int iTotOperUSD 			= 0;
			BigDecimal bMtoOperUSD 	= new BigDecimal(0);*/
			String nombreArchivo = null;							  
			StringBuffer sbContenidoF = new StringBuffer("");
		try{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if(tipo.equals("CSV")){
			
			sbContenidoF = new StringBuffer(",,,,,,,,RESUMEN DISPERSI�N EPO\r\n");
			sbContenidoF.append(",,,,,,,,Fecha de Emisi�n:"+sdf.format(new java.util.Date())+"\r\n");
			sbContenidoF.append("Nombre EPO:"+nombreEPO.replaceAll(",","")+",,,,,,,,\r\n");
			sbContenidoF.append(",,Fecha de Inicio,,,Fecha de T�rmino,,,\r\n");
			sbContenidoF.append(",,"+fechaRegIni+",,,"+fechaRegFin+",,,\r\n");
			sbContenidoF.append("\r\n");
			sbContenidoF.append("Detalle Documentos Operados por Intermediario Financiero,,,,,,,,\r\n");
			sbContenidoF.append("Intermediario Financiero,Moneda,N�mero,Monto,,,,,,\r\n");
			
			String [] filas = detalle.split("\r\n");
	
			for(int i = 0; i<filas.length;i++){				
				String []reg = filas[i].split(";");					
				for(int j = 0; j < reg.length; j++ ){
					if(j == 3){							
						sbContenidoF.append(reg[j].replaceAll("\"","").replaceAll(",","")  +", ");
					}else{
						sbContenidoF.append(reg[j].replaceAll("\"","").replaceAll(",","") +", ");							
					}						
				}
				sbContenidoF.append("\n"); 
			}
			
			if(esTarifaEnMonedaNacionalValida){
				sbContenidoF.append("\r\n");
				sbContenidoF.append("Detalle Documentos no Operados M.N. por periodo de Vigencia*,,,,,,,,\r\n");
				sbContenidoF.append(",Hasta "+txt_dias1+" D�as Inclusive,,De los "+(Integer.parseInt(txt_dias1)+1)+" a los "+(Integer.parseInt(txt_dias2))+" D�as,,Total No Operados,\r\n");
				sbContenidoF.append("Fecha de Vencimiento,N�mero de Proveedores,Monto,N�mero de Proveedores,Monto,N�mero de Proveedores,Monto\r\n");
			
				sbContenidoF.append(detalleMN);
			}
			// Dolar Americano
			if(esTarifaEnDolaresAmericanosValida){
				sbContenidoF.append("\r\n");
				sbContenidoF.append("Detalle Documentos no Operados USD por periodo de Vigencia*,,,,,,,,\r\n");
				sbContenidoF.append(",Hasta "+txt_dias1USD+" D�as Inclusive,,De los "+(Integer.parseInt(txt_dias1USD)+1)+" a los "+(Integer.parseInt(txt_dias2USD))+" D�as,,Total No Operados,\r\n");
				sbContenidoF.append("Fecha de Vencimiento,N�mero de Proveedores,Monto,N�mero de Proveedores,Monto,N�mero de Proveedores,Monto \r\n");
				sbContenidoF.append(detalleUSD);
			}
			sbContenidoF.append("\r\n");
			sbContenidoF.append("\"* Para determinar el per�odo de vigencia de los documentos se calcular� la diferiencia en d�as entre la fecha de vencimiento\"\r\n");
			sbContenidoF.append("\"y la fecha de alta de los mismos en Cadenas Productivas. En el caso de existir m�s de un documento con la misma fecha de\"\r\n");
			sbContenidoF.append("\"vencimiento para el mismo proveedor, se tomar� como periodo de vigencia el m�nimo del grupo de documentos.\",,,,,,,,\r\n");
			sbContenidoF.append(",,,Elabor�:"+strNombreUsuario+",,,,,\r\n");
		}
			if(tipo.equals("PDF") )   {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
						
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
						(String)session.getAttribute("sesExterno"),
						(String) session.getAttribute("strNombre"),
						(String) session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" RESUMEN DISPERSI�N EPO  ","formasB",ComunesPDF.RIGHT);
				
				//float anchoCeldaAA[] = {5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f};
				pdfDoc.setTable(6,50);//,anchoCeldaAA,2);				
				pdfDoc.setCell("Fecha de Emisi�n:"+sdf.format(new java.util.Date()),"formasB",ComunesPDF.RIGHT,6);
				
				pdfDoc.setCell("Nombre EPO:"+nombreEPO,"formasB",ComunesPDF.LEFT,6);				
				pdfDoc.setCell("Fecha de Inicio","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell("Fecha de T�rmino","formas",ComunesPDF.CENTER,3);
				pdfDoc.setCell(fechaRegIni,"formasB",ComunesPDF.CENTER,3);
				pdfDoc.setCell(fechaRegFin,"formasB",ComunesPDF.CENTER,3);
				pdfDoc.addTable();				
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
//------------------------------------------------------------------------------
//				ALINEAR A LA IZQUIERDA
//------------------------------------------------------------------------------
				float anchoCeldaA[] = {5.88f, 5.88f, 5.88f, 5.88f};
				pdfDoc.setTable(4,30,anchoCeldaA);
				pdfDoc.addText(" Detalle Documentos Operados por Intermediario Financiero ","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(" Intermediario \nFinanciero  " , "celda02",ComunesPDF.CENTER, 1,2);
				pdfDoc.setCell(" Documentos Operados  " , "celda02",ComunesPDF.CENTER, 3);
				pdfDoc.setCell(" Moneda  " , "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero  " , "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto  " , "celda02",ComunesPDF.CENTER);
				
				String [] filas = detalle.split("\r\n");
				System.err.println(filas.length);
				for(int i = 0; i<filas.length;i++){				
					String []reg = filas[i].split(";");					
					for(int j = 0; j < reg.length; j++ ){
						if(j == 3){								
							pdfDoc.setCell("$"+Comunes.formatoDecimal(reg[j].replaceAll("\"",""),2) , "formas",ComunesPDF.RIGHT);
						}else{						
							pdfDoc.setCell(reg[j].replaceAll("\"","") , "formas",ComunesPDF.CENTER);	
						}
					
					}	
				}
				pdfDoc.addTable();;
//------------------------------------------------------------------------------				
				
				if(esTarifaEnMonedaNacionalValida){
					pdfDoc.setTable(7);
					pdfDoc.addText(" \n\nDetalle Documentos no Operados M.N. por periodo de Vigencia* ","formas",ComunesPDF.LEFT);
					
					pdfDoc.setCell("Fecha de Vencimiento","celda02",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("Hasta "+txt_dias1+" D�as Inclusive","celda02",ComunesPDF.CENTER,2);
					pdfDoc.setCell("De los "+(Integer.parseInt(txt_dias1)+1)+" a los "+(Integer.parseInt(txt_dias2))+" D�as","celda02",ComunesPDF.CENTER,2);
					pdfDoc.setCell("Total No Operados","celda02",ComunesPDF.CENTER,2);
					pdfDoc.setCell("N�mero de Proveedores","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Proveedores","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Proveedores","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto","celda02",ComunesPDF.CENTER);
					
					
					String []filas1 = detalleMN.split("\n");
					if(filas1.length>1){
						for(int i = 0; i<filas1.length;i++){
							String []reg = filas1[i].split(",");
							for(int j = 0; j < reg.length; j++ ){
								String clase = "formas";
								if( reg[j].equals("Total:")|| reg[j].equals("Tarifa:")|| reg[j].equals("Monto Total de Comisi�n:")){
									clase = "celda02";  
								}								
								 
								if(((j==2)||(j==4)||(j==6)) & !reg[j].trim().equals("")){
									pdfDoc.setCell("$"+Comunes.formatoDecimal(reg[j].replaceAll("\"",""),2) , clase,ComunesPDF.RIGHT);	
								}else{
									if( !reg[j].trim().equals("")){	  							
										if((i == filas1.length-1) && j ==1){
											pdfDoc.setCell("$"+Comunes.formatoDecimal(reg[j].replaceAll("\"",""),2) , clase,ComunesPDF.RIGHT);		
										}else if((i == filas1.length-2) &&(j==1 || j==3)){
											pdfDoc.setCell("$"+Comunes.formatoDecimal(reg[j].replaceAll("\"",""),2) , clase,ComunesPDF.RIGHT);	
										}else{										  
											pdfDoc.setCell(reg[j].replaceAll("\"","").replaceAll("-","") , clase,ComunesPDF.CENTER);	
										}
									}
								}
							}		
						} pdfDoc.setCell("" , "formas",ComunesPDF.CENTER, 2);
					}else{
						pdfDoc.setCell("No se encontraron registros","formasB",ComunesPDF.CENTER,7);	
					}
					pdfDoc.addTable();
				}
 
//------------------------------------------------------------------------------				

				if(esTarifaEnDolaresAmericanosValida){
					pdfDoc.setTable(7);
					pdfDoc.addText(" \n\nDetalle Documentos no Operados USD por periodo de Vigencia* ","formas",ComunesPDF.LEFT);
					
					pdfDoc.setCell("Fecha de Vencimiento","celda02",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("Hasta "+txt_dias1USD+" D�as Inclusive","celda02",ComunesPDF.CENTER,2);
					pdfDoc.setCell("De los "+(Integer.parseInt(txt_dias1USD)+1)+" a los "+(Integer.parseInt(txt_dias2USD))+" D�as","celda02",ComunesPDF.CENTER,2);
					pdfDoc.setCell("Total No Operados","celda02",ComunesPDF.CENTER,2);
					pdfDoc.setCell("N�mero de Proveedores","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Proveedores","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Proveedores","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto","celda02",ComunesPDF.CENTER);

					String []filas2 = detalleUSD.split(",");
					if(filas2.length>1){
						for(int i = 0; i<filas2.length;i++){
							String []reg = filas2[i].split(",");
							
							for(int j = 0; j < reg.length; j++ ){
								String clase = "formas";
								if( reg[j].trim().equals("Total:") || reg[j].trim().equals("Tarifa:")|| reg[j].trim().equals("Monto Total de Comisi�n:")){
									clase = "celda02"; 										
								}	
								
								if(((j==2)||(j==4)||(j==6)) & !reg[j].trim().equals("")){
									pdfDoc.setCell("$"+Comunes.formatoDecimal(reg[j].replaceAll("\"",""),2) , clase,ComunesPDF.RIGHT);	
								}else{
									if( !reg[j].trim().equals("")){	  
										if((i == filas2.length-1) && j ==1){
											pdfDoc.setCell("$"+Comunes.formatoDecimal(reg[j].replaceAll("\"",""),2) , clase,ComunesPDF.RIGHT);		
										}else if((i == filas2.length-2) &&(j==1 || j==3)){
											pdfDoc.setCell("$"+Comunes.formatoDecimal(reg[j].replaceAll("\"",""),2) , clase,ComunesPDF.RIGHT);	
										}else{   
										
											String pos = reg[j].toString();
											if(pos.indexOf(".")>=1 )  { 
												pdfDoc.setCell("$"+Comunes.formatoDecimal(reg[j].replaceAll("\"","").replaceAll("-",""),2) , clase,ComunesPDF.RIGHT);	
											}else  {
												pdfDoc.setCell(reg[j].replaceAll("\"","").replaceAll("-","") , clase,ComunesPDF.CENTER);	
												
											}
										
										}
									}
								}
							}	
						} pdfDoc.setCell("" , "formas",ComunesPDF.CENTER, 2);	
					}else{
						pdfDoc.setCell("No se encontraron registros","formasB",ComunesPDF.CENTER,7);	
					}
					pdfDoc.addTable();
				}
				
				///Mensaje final 
				//pdfDoc.setTable(1,50);
					pdfDoc.addText("\n\n* Para determinar el per�odo de vigencia de los documentos, se calcular� la diferiencia en d�as entre la\nfecha de vencimiento y la fecha de alta de los mismos en Cadenas Productivas. En el caso de existir m�s\nde un documento con la misma fecha de vencimiento para el mismo proveedor, se tomar� como periodo\nde vigencia el m�nimo del grupo de documentos. ","formas",ComunesPDF.CENTER);
					pdfDoc.addText("\n\nElabor�:"+strNombreUsuario,"formasB",ComunesPDF.CENTER);
					
				//pdfDoc.addTable();
				pdfDoc.endDocument();
			}
			
			// Crear Archivo
			if(tipo.equals("CSV")){
				log.info("Se creara el archivo de tipo cvs");
				CreaArchivo archivo = new CreaArchivo();
				if (archivo.make(sbContenidoF.toString(), path, ".csv"))
					nombreArchivo = archivo.nombre;	
			}
			
			
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return nombreArchivo;
	}
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setFechaRegIni(String fechaRegIni) {
		this.fechaRegIni = fechaRegIni;
	}


	public String getFechaRegIni() {
		return fechaRegIni;
	}


	public void setFechaRegFin(String fechaRegFin) {
		this.fechaRegFin = fechaRegFin;
	}


	public String getFechaRegFin() {
		return fechaRegFin;
	}


	public void setNombreEPO(String nombreEPO) {
		this.nombreEPO = nombreEPO;
	}


	public String getNombreEPO() {
		return nombreEPO;
	}
}