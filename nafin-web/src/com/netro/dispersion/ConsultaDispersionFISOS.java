package com.netro.dispersion;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**

	Esta clase se encarga de generar los archivos de la consulta dentro del monitor de Dispersion IF, en la pantalla:
		PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - IF's
	
	@author jshernandez
	@since  F006 - 2013 -- ADMIN NAFIN - Dispersion IF
	@date	  29/07/2013 07:03:49 p.m.
	
 */

public class ConsultaDispersionFISOS {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaDispersionFISOS.class);
	
	/**
	 *	Crea un archivo PDF con el detalle de la Consulta de Dispersion del Monitor IF.
	 *	@throws AppException   
	 *	
	 *	@param directorio             <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 * @param directorioPublicacion 	<tt>String</tt> con la ruta WEB del directorio de publicacion.
	 * @param claveIF					   <tt>String</tt> con la clave del Intermediario Financiero, como aparece en la tabla (<tt>COMCAT_IF.IC_IF</tt>).
	 * @param claveEPO				   <tt>String</tt> con la clave de la EPO, como aparece en la tabla (<tt>COMCAT_EPO.IC_EPO</tt>).
	 * @param claveMoneda			   <tt>String</tt> con la clave de la Moneda, como aparece en la tabla (<tt>COMCAT_MONEDA.IC_MONEDA</tt>).
	 * @param fechaRegistro 		   <tt>String</tt> con la fecha de registro, en formato: DD/MM/YYYY.
	 * @param session                Objeto <tt>HttpSession</tt> con la sesi�n del usuario.
	 *
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 *
	 */	
	public static String generaArchivoPDF( 
		String 		directorio, 
		String 		directorioPublicacion, 
		String 		claveIF, 
		String 		claveEPO,
		String 		claveMoneda,
		String 		fechaRegistro, 
		HttpSession session  
	) throws AppException {

		log.info("generaArchivoPDF(E)");
		
		CreaArchivo	archivo 			= null;
		String		nombreArchivo	= null;
		ComunesPDF	pdf 				= null;
 
		// 1. Obtener instancia de EJB de Dispersion
		Dispersion dispersion = null;
		try {
					
			dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
					
		} catch(Exception e) {
	 
			String msg = "Ocurri� un error al obtener instancia del EJB de Dispersi�n";
			log.error(msg);
			e.printStackTrace();
			
			throw new AppException(msg);
	 
		}
	
		try {
 
			// Realizar consulta
			HashMap	consulta 			= dispersion.consultaDispersionFISOS( claveEPO, claveMoneda, claveIF, fechaRegistro );
			List 		registros			= (List) 	consulta.get("registros");
			List		registrosTotales	= (List) 	consulta.get("registrosTotales");
			HashMap	detalleTotales		= (HashMap) consulta.get("detalleTotales");
	
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".pdf";
 
			// Sacando la fecha para encabezado de Excel
			String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaActual.substring(0,2);
			String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   	= fechaActual.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			String strEncabezado = "M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + 
										  " ----------------------------- " + horaActual;
 
			// Crear Documento PDF
			pdf = new ComunesPDF(1, directorio+nombreArchivo, "P�gina " );
			pdf.encabezadoConImagenes(
				pdf,
				(String)session.getAttribute("strPais"),
				(String)session.getAttribute("iNoNafinElectronico"),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				directorioPublicacion
			);
 
			pdf.addText(strEncabezado, "formas", ComunesPDF.RIGHT );
			pdf.addText(" ",           "formasG",ComunesPDF.CENTER);
			
			// Variables asociadas a la consulta
			String 		rfc 					= null; // RFC  
			String 		nombreProveedor	= null; // Nombre del Proveedor 
			String 		totalDocumentos	= null; // Total de Documentos
			String 		moneda 				= null; // Moneda 
			String 		monto 				= null; // Monto 
			String		banco 				= null; // Banco 
			String 		tipoCuenta 			= null; // Tipo de Cuenta 
			String 		numeroCuenta 		= null; // N�mero de Cuenta
 
			List titulosReporte = new ArrayList();
			titulosReporte.add("RFC");
			titulosReporte.add("Nombre del Proveedor");
			titulosReporte.add("Total de Documentos");
			titulosReporte.add("Moneda");
			titulosReporte.add("Monto");
			titulosReporte.add("Banco");
			titulosReporte.add("Tipo de Cuenta");
			titulosReporte.add("N�mero de Cuenta");
 
			// Agregar t�tulo
			pdf.setTable(titulosReporte, "celda01", 90, new float[]{6.8f,13.4f,8.0f,7.4f,15.0f,5.0f,5.0f,15.0f});
			
			// Agregar registros
			for(int i=0,ctaRegistros = 0;i<registros.size();i++,ctaRegistros++){
				
				HashMap  registro = (HashMap) registros.get(i);
	
				rfc 					= (String) registro.get("RFC");
				nombreProveedor 	= (String) registro.get("NOMBRE_PROVEEDOR");
				totalDocumentos 	= (String) registro.get("TOTAL_DOCUMENTOS");
				moneda 				= (String) registro.get("MONEDA");
				monto 				= (String) registro.get("MONTO");
				banco 				= (String) registro.get("BANCO");
				tipoCuenta 			= (String) registro.get("TIPO_CUENTA");
				numeroCuenta 		= (String) registro.get("NUMERO_CUENTA");
 
				rfc 					= rfc 				== null?"":rfc;
				nombreProveedor 	= nombreProveedor == null?"":nombreProveedor;
				totalDocumentos 	= totalDocumentos == null?"":totalDocumentos;
				moneda 				= moneda 			== null?"":moneda;
				monto 				= monto 				== null?"":monto;
				banco 				= banco 				== null?"":banco;
				tipoCuenta 			= tipoCuenta 		== null?"":tipoCuenta;
				numeroCuenta 		= numeroCuenta 	== null?"":numeroCuenta;
				
				// Agregar contenido del registro
				pdf.setCell( rfc, 				"formas", ComunesPDF.CENTER );
				pdf.setCell( nombreProveedor, "formas", ComunesPDF.CENTER );
				pdf.setCell( totalDocumentos, "formas", ComunesPDF.CENTER );
				pdf.setCell( moneda, 			"formas", ComunesPDF.CENTER );
				pdf.setCell( monto, 				"formas", ComunesPDF.RIGHT  );
				pdf.setCell( banco, 				"formas", ComunesPDF.CENTER );
				pdf.setCell( tipoCuenta, 		"formas", ComunesPDF.CENTER );
				pdf.setCell( numeroCuenta, 	"formas", ComunesPDF.CENTER );
 
			}
			
			// Agregar registros totales
			for(int i=0,ctaRegistros = 0;i<registrosTotales.size();i++,ctaRegistros++){
				
				HashMap registro = (HashMap) registrosTotales.get(i);
				
				rfc 					= (String) registro.get("RFC");
				nombreProveedor 	= (String) registro.get("NOMBRE_PROVEEDOR");
				totalDocumentos 	= (String) registro.get("TOTAL_DOCUMENTOS");
				moneda 				= (String) registro.get("MONEDA");
				monto 				= (String) registro.get("MONTO");
				banco 				= (String) registro.get("BANCO");
				tipoCuenta 			= (String) registro.get("TIPO_CUENTA");
				numeroCuenta 		= (String) registro.get("NUMERO_CUENTA");
 
				rfc 					= rfc 				== null?"":rfc;
				nombreProveedor 	= nombreProveedor == null?"":nombreProveedor;
				totalDocumentos 	= totalDocumentos == null?"":totalDocumentos;
				moneda 				= moneda 			== null?"":moneda;
				monto 				= monto 				== null?"":monto;
				banco 				= banco 				== null?"":banco;
				tipoCuenta 			= tipoCuenta 		== null?"":tipoCuenta;
				numeroCuenta 		= numeroCuenta 	== null?"":numeroCuenta;
				
				// Agregar contenido del registro
				pdf.setCell( rfc, 			   "celda01",  ComunesPDF.RIGHT,  2 );
				// pdf.setCell( nombreProveedor, "formas", ComunesPDF.CENTER );
				pdf.setCell( totalDocumentos, "formas", 	ComunesPDF.CENTER    );
				pdf.setCell( "", 					"celda01", 	ComunesPDF.CENTER    ); 
				// pdf.setCell( moneda, 			"formas", 	ComunesPDF.CENTER ); 
				pdf.setCell( monto, 				"formas", 	ComunesPDF.RIGHT     );
				pdf.setCell( "", 					"celda01", 	ComunesPDF.CENTER, 3 );
				// pdf.setCell( banco, 				"formas", 	ComunesPDF.CENTER );
				// pdf.setCell( tipoCuenta, 		"formas", 	ComunesPDF.CENTER );
				// pdf.setCell( numeroCuenta, 	"formas", 	ComunesPDF.CENTER );
				
			}
			
			// Agregar totales
			if( registros.size() == 0 ){
				pdf.setCell( "No se encontr� ning�n registro", 	"celda01", ComunesPDF.CENTER, 8 );
			}
 	
			pdf.addTable();
			pdf.addText(" ","formas", ComunesPDF.CENTER );
					
			pdf.endDocument();
 
		} catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoPDF(OutOfMemoryError)");
			log.error("generaArchivoPDF.message                = <" + om.getMessage()        + ">");
			log.error("generaArchivoPDF.freeMemory             = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error("generaArchivoPDF.directorio             = <" + directorio             + ">");
			log.error("generaArchivoPDF.directorioPublicacion  = <" + directorioPublicacion  + ">");
			log.error("generaArchivoPDF.claveIF                = <" + claveIF						+ ">"); 
			log.error("generaArchivoPDF.claveEPO               = <" + claveEPO               + ">");
			log.error("generaArchivoPDF.claveMoneda            = <" + claveMoneda            + ">");
			log.error("generaArchivoPDF.fechaRegistro          = <" + fechaRegistro          + ">"); 
			log.error("generaArchivoPDF.session                = <" + session                + ">"); 
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoPDF(Exception)");
			log.error("generaArchivoPDF.message                = <" + e.getMessage()         + ">");
			log.error("generaArchivoPDF.directorio             = <" + directorio             + ">");
			log.error("generaArchivoPDF.directorioPublicacion  = <" + directorioPublicacion  + ">");
			log.error("generaArchivoPDF.claveIF                = <" + claveIF						+ ">"); 
			log.error("generaArchivoPDF.claveEPO               = <" + claveEPO               + ">");
			log.error("generaArchivoPDF.claveMoneda            = <" + claveMoneda            + ">");
			log.error("generaArchivoPDF.fechaRegistro          = <" + fechaRegistro          + ">"); 
			log.error("generaArchivoPDF.session                = <" + session                + ">");
			e.printStackTrace(); 
			
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n detallada.");
			
		} finally {
 
			log.info("generaArchivoPDF(S)");
			
		}
		
		return nombreArchivo;
		
	}
	
}