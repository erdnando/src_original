package com.netro.dispersion;


import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Fecha;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**

	Esta clase se utilza para consultar el resumen de la operacion, el cual se muestra en la pantalla:
		PANTALLA: ADMIN NAFIN - ADMINISTRACION - FLUJO DE FONDOS - MONITOR
		PANTALLA: ADMIN_NAFIN - ADMINISTRACI�N � FLUJO DE FONDOS � CONSULTAS
		
		Implementa la consulta realizada en el m�todo:
		
			<tt>			
				getResumOperacErrorFF(
					String sTipo,
					String sTipoDispersion,
					String sFechaRegIni,
					String sFechaRegFin,
					String sViaLiq,
					String ic_epo,
					String ic_if,
					String sEstatus,
					String sEstatusFF
				)
			</tt>
			
		De la clase:
		
			<tt>com.netro.dispersion.DispersionBean</tt>
		
	@author jshernandez
	@since  F006 - 2013 -- ADMIN NAFIN - FFON Monitor
	@date	  14/08/2013 01:10:31 p.m.
	
	@note:  Los cambios realizados al query de este m�todo deber�n verse reflejados en el m�todo:
	
					getResumOperacErrorFF(
						String sTipo,
						String sTipoDispersion,
						String sFechaRegIni,
						String sFechaRegFin,
						String sViaLiq,
						String ic_epo,
						String ic_if,
						String sEstatus,
						String sEstatusFF
					)
											
			  de la clase:
			
               ejb/DispersionEJB/src/com/netro/dispersion/DispersionBean.java

           que contiene la versi�n original de esta consulta.

   @since  F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 25/09/2013 04:51:48 p.m.
	@author jshernandez

 */
public class ConsultaResumenOperacionErrorFFON implements IQueryGeneratorRegExtJS {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaResumenOperacionErrorFFON.class);
	
	public ConsultaResumenOperacionErrorFFON() {	}
	
	private List 			conditions;
	private StringBuffer query;
 
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		
		this.query			= new StringBuffer();
		this.conditions	= new ArrayList();
		
		StringBuffer cond1 				= new StringBuffer(64);
		List			 cond1Conditions	= new ArrayList();
			
		if(			this.tipoDispersion.equals("E") 												) {
				
			cond1.append(
				"    AND (ff.df_fecha_venc IS NOT NULL OR ff.df_operacion IS NOT NULL)"   +
				"    AND ff.ic_estatus_docto IN (1, 4, 9, 10, 11)"
			);
				
		} else if(	this.tipoDispersion.equals("F") 											) {
				
			cond1.append(
				"    AND (ff.ic_if IS NOT NULL AND ff.df_fecha_venc IS NULL)"  
			);
				
		} else if(	this.tipoDispersion.equals("P") && "2".equals(this.estatus)		) {
				
			cond1.append(
				"    AND (    ff.ic_if IS NULL"   +
				"         AND ff.df_fecha_venc IS NOT NULL"   +
				"         AND ff.ic_estatus_docto = 2"   +
				"        )"  
			);
				
		} else if(	this.tipoDispersion.equals("P") && "PA".equals(this.estatus)	) {
				
			cond1.append(
				"    AND (    ff.ic_if IS NULL"   +
				"         AND ff.df_operacion IS NOT NULL"   +
				"         AND ff.ic_estatus_docto IS NULL"   +
				"        )"  
			);
				
		}

		if( !this.fechaRegistroInicial.equals("") && !this.fechaRegistroFinal.equals("") ){
				
			cond1.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy')      "  + // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy') + 1) "    // this.fechaRegistroFinal
			);
			cond1Conditions.add(this.fechaRegistroInicial);
			cond1Conditions.add(this.fechaRegistroFinal);
				
		}
		
/*
			String cond = (sTipoDispersion.equals("E")?"and (FF.df_fecha_venc is not null or FF.df_operacion is not null)":(sTipoDispersion.equals("F")?"and (FF.ic_if is not null and FF.df_fecha_venc is null) ":""))+
							((!sFechaRegIni.equals("") && !sFechaRegFin.equals(""))?"and trunc(FF.df_registro) between TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') and TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy') ":"");
*/

		String epoEspecifica 				= "";
		List	 epoEspecificaConditions 	= new ArrayList();
		String ifEspecifica  				= "";
		List	 ifEspecificaConditions  	= new ArrayList();

		if(        !"".equals(this.claveEPO) ){
			epoEspecifica = " AND FF.ic_epo = ? "; // this.claveEPO
			epoEspecificaConditions.add( this.claveEPO );
		} else if( !"".equals(this.claveIF)  ){
			ifEspecifica  = " AND FF.ic_if  = ? "; // this.claveIF
			ifEspecificaConditions.add(  this.claveIF  );
		}

		String condEPOs =
			"AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
   		"AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') "+
   		"AND c.ic_producto_nafin  = 1 ";
   				
		String condEPOsErr =
   		"AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
   		"AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 1 "+
   		"AND c.ic_moneda (+) = 1 ";

		String condIFs =
   		"AND crn.ic_nafin_electronico = c.ic_nafin_electronico "+
//   		"AND crn.ic_epo_pyme_if = ff.ic_pyme "+
   		"AND c.ic_producto_nafin  = 1 ";
   				
		String condIFsErr =
   		"AND crn.ic_nafin_electronico(+) = c.ic_nafin_electronico "+
//   		"AND crn.ic_epo_pyme_if = ff.ic_pyme "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 1 "+
   		"AND c.ic_moneda (+) = 1 ";

		String condPEMEXs =
   		"AND crn.ic_epo_pyme_if = ff.ic_epo "+
   		"AND crn.cg_tipo = 'E' "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 4 "+
   		"AND c.ic_moneda (+) = 1 ";
 	
		StringBuffer cond2 	 				= new StringBuffer(64);
		List			 cond2Conditions 		= new ArrayList();
		StringBuffer cond2Err 				= new StringBuffer(64);
		List			 cond2ErrConditions	= new ArrayList();
			
		if(			this.tipoDispersion.equals("E")	){
				
			cond2.append(" ");
			cond2.append(condEPOs);
			cond2.append(
				" and (FF.df_fecha_venc is not null or FF.df_operacion is not null) and ff.ic_estatus_docto in (1, 4, 9,10, 11) "
			);
				
			cond2Err.append(" ");
			cond2Err.append(condEPOsErr);
			cond2Err.append(
				" and (FF.df_fecha_venc is not null or FF.df_operacion is not null) and ff.ic_estatus_docto in (1, 4, 9,10, 11) "
			);
				
		} else if(	this.tipoDispersion.equals("F")	){
				
			cond2.append(" ");
			cond2.append(condIFs);
			cond2.append(
				" and (FF.ic_if is not null and FF.df_fecha_venc is null) "
			);
				
			cond2Err.append(" ");
			cond2Err.append(condIFsErr);
			cond2Err.append(
				" and (FF.ic_if is not null and FF.df_fecha_venc is null) "
			);
				
		} else if(	this.tipoDispersion.equals("P") && "2".equals(this.estatus ) ){
				
			cond2.append(" ");
			cond2.append(condPEMEXs);
			cond2.append(
				" AND (ff.ic_if IS NULL AND ff.df_fecha_venc IS NOT NULL AND ff.ic_estatus_docto = 2) "
			);
				
		} else if(  this.tipoDispersion.equals("P") && "PA".equals(this.estatus) ){
				
			cond2.append(" ");
			cond2.append(condPEMEXs);
			cond2.append(
				" AND (ff.ic_if IS NULL AND ff.df_operacion IS NOT NULL AND ff.ic_estatus_docto IS NULL) "
			);
				
		}
			
		if( !this.fechaRegistroInicial.equals("") && !this.fechaRegistroFinal.equals("") ) {
				
			cond2.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy')    "  + // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy')+1) "    // this.fechaRegistroFinal
			);
			cond2Conditions.add(this.fechaRegistroInicial);
			cond2Conditions.add(this.fechaRegistroFinal);
				
			cond2Err.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy') "+ // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy')+1) " // this.fechaRegistroFinal
			);
			cond2ErrConditions.add(this.fechaRegistroInicial);
			cond2ErrConditions.add(this.fechaRegistroFinal);
				
		}
/*
		String cond2 = (sTipoDispersion.equals("E")?condEPOs+" and (FF.df_fecha_venc is not null or FF.df_operacion is not null)":(sTipoDispersion.equals("F")?condIFs+" and (FF.ic_if is not null and FF.df_fecha_venc is null) ":""))+
						((!sFechaRegIni.equals("") && !sFechaRegFin.equals(""))?"and trunc(FF.df_registro) between TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') and TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy') ":"");

*/

		String condvia = "";
		if( !"".equals(this.viaLiquidacion) ) {
				 
			if(        "T".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'TEF'   ";
			} else if( "S".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'SPEUA' ";
			} else if( "I".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'SPEI'  ";
			}
				
		}
 			
		String condMonitor					= "";
		String condConsultaEpo				= "";
		List   condConsultaEpoConditions = new ArrayList();
			
		if( this.tipo.equals("Monitor") ) {
			condMonitor 		= "    AND ME.status_cen_i IN (-20, -1, 42, 44, 60, 9999) ";
		}
		if ( !"".equals(this.estatusFF)  ) {
			condConsultaEpo 	= "    AND ME.status_cen_i >= ? "; // this.estatusFF
			condConsultaEpoConditions.add(this.estatusFF);
		}
			
		this.query.append(
			" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff btt)*/ "   +
			"        ff.ic_flujo_fondos "	+
			"   FROM int_flujo_fondos ff,"   +
			"        cfe_m_encabezado me,"   +
			"        comrel_nafin crn,"   +
			"        comcat_epo e,"   +
			"        comcat_if i,"   +
			"        comcat_pyme p,"   +
			"        com_cuentas c,"   +
			"        comcat_estatus_ff eff,"   +
			"        comcat_bancos_tef btef"   +
			"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
			"    AND ff.ic_epo = e.ic_epo"   +
			"    AND ff.ic_if = i.ic_if(+)"   +
			"    AND ff.ic_pyme = p.ic_pyme(+)"   +
			"    AND ff.ic_cuenta = c.ic_cuenta"   +
			"    AND c.ic_bancos_tef = btef.ic_bancos_tef"   +
			"    AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
			//"    AND ff.cs_reproceso IN ('N', 'D')"   +
			"    AND ff.cs_reproceso ='N' "+
			"    AND ff.cs_concluido = 'N'"
		);
		this.query.append(
			cond2 
		);
		this.query.append(
			condvia 
		);
		this.query.append(
			epoEspecifica 
		);
		this.query.append(
			ifEspecifica 
		);
		this.query.append(
			condMonitor 
		);
		this.query.append(
			condConsultaEpo 
		);
		this.query.append(
			" UNION ALL "+
			" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/"   +
			"        ff.ic_flujo_fondos "	+
			"   FROM int_flujo_fondos_err ff,"   +
			"        cfe_m_encabezado_err me,"   +
			"        comrel_nafin crn,"   +
			"        comcat_epo e,"   +
			"        comcat_if i,"   +
			"        comcat_pyme p,"   +
			"        com_cuentas c,"   +
			"        comcat_estatus_ff eff"   +
			"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
			"    AND ff.ic_epo = e.ic_epo"   +
			"    AND ff.ic_if = i.ic_if(+)"   +
			"    AND ff.ic_pyme = p.ic_pyme(+)"   +
			"    AND ff.ic_cuenta = c.ic_cuenta(+)"   +
			"    AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
			//"    AND ff.cs_reproceso IN ('N', 'D')"   +
			"    AND ff.cs_reproceso ='N' "+
			"    AND ff.cs_concluido = 'N'"
		);
		this.query.append(
			cond2Err 
		);
		this.query.append(
			condvia 
		);
		this.query.append(
			epoEspecifica 
		);
		this.query.append(
			ifEspecifica 
		);
		this.query.append(
			condMonitor 
		);
		this.query.append(
			condConsultaEpo 
		);
			
		// AGREGAR CONDICIONES
			
		this.conditions.addAll(cond2Conditions); 
		// this.conditions.addAll(condviaConditions);
		this.conditions.addAll(epoEspecificaConditions);
		this.conditions.addAll(ifEspecificaConditions);
		// this.conditions.addAll(condMonitorConditions);
		this.conditions.addAll(condConsultaEpoConditions);
 
		this.conditions.addAll(cond2ErrConditions); 
		// this.conditions.addAll(condviaConditions);
		this.conditions.addAll(epoEspecificaConditions);
		this.conditions.addAll(ifEspecificaConditions); 
		// this.conditions.addAll(condMonitorConditions);
		this.conditions.addAll(condConsultaEpoConditions);
 
		if( this.tipo.equals("Consulta") ) {
				
			this.query.append(
				" UNION ALL"   +
				" SELECT /*+index(ff) use_nl(ff me e i p c eff)*/"   +
				"      ff.ic_flujo_fondos "	+
				"   FROM int_flujo_fondos ff,"   +
				" 		 cfe_m_encabezado me,"   +
				" 		 comcat_epo e,"   +
				" 		 comcat_if i,"   +
				" 		 comcat_pyme p,"   +
				" 		 com_cuentas c,"   +
				" 		 comcat_estatus_ff eff"   +
				"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
				" 	AND ff.ic_epo = e.ic_epo"   +
				" 	AND ff.ic_if = i.ic_if(+)"   +
				" 	AND ff.ic_pyme = p.ic_pyme(+)"   +
				" 	AND ff.ic_cuenta = c.ic_cuenta(+)"   +
				" 	AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
				" 	AND ff.cs_reproceso IN ('N', 'D')"   +
				" 	AND ff.cs_concluido = 'S'"
			);
			this.query.append(
				cond1 
			);
			this.query.append(
				condvia 
			);
			this.query.append(
				epoEspecifica 
			);
			this.query.append(
				ifEspecifica 
			);
			this.query.append(
				condConsultaEpo 
			);
 
			this.conditions.addAll(cond1Conditions);
			// this.conditions.addAll(condviaConditions); 
			this.conditions.addAll(epoEspecificaConditions);
			this.conditions.addAll(ifEspecificaConditions);
			this.conditions.addAll(condConsultaEpoConditions);

		}

		log.debug("getDocumentQuery.tipo                 = <" + this.tipo                 + ">");
		log.debug("getDocumentQuery.tipoDispersion       = <" + this.tipoDispersion       + ">");
		log.debug("getDocumentQuery.fechaRegistroInicial = <" + this.fechaRegistroInicial + ">");
		log.debug("getDocumentQuery.fechaRegistroFinal   = <" + this.fechaRegistroFinal   + ">");
		log.debug("getDocumentQuery.viaLiquidacion       = <" + this.viaLiquidacion       + ">");
		log.debug("getDocumentQuery.claveEPO             = <" + this.claveEPO             + ">");
		log.debug("getDocumentQuery.claveIF              = <" + this.claveIF              + ">");
		log.debug("getDocumentQuery.estatus              = <" + this.estatus              + ">");
		log.debug("getDocumentQuery.estatusFF            = <" + this.estatusFF            + ">");

		log.debug("getDocumentQuery.query                = <" + this.query                + ">");
		log.debug("getDocumentQuery.conditions           = <" + this.conditions           + ">");
		
		log.info("getDocumentQuery(S)");
 
		return this.query.toString();

	}
	
	/**
	 * Obtiene el query para obtener el detalle de los registros por p�gina.
	 * @return Cadena con la consulta de SQL, para obtener el detalle por p�gina.
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		
		log.info("getDocumentSummaryQueryForIds(E)");
		
		this.query			= new StringBuffer();
		this.conditions	= new ArrayList();
		
		StringBuffer cond1 				= new StringBuffer(64);
		List			 cond1Conditions	= new ArrayList();
			
		if(			this.tipoDispersion.equals("E") 												) {
				
			cond1.append(
				"    AND (ff.df_fecha_venc IS NOT NULL OR ff.df_operacion IS NOT NULL)"   +
				"    AND ff.ic_estatus_docto IN (1, 4, 9, 10, 11)"
			);
				
		} else if(	this.tipoDispersion.equals("F") 											) {
				
			cond1.append(
				"    AND (ff.ic_if IS NOT NULL AND ff.df_fecha_venc IS NULL)"  
			);
				
		} else if(	this.tipoDispersion.equals("P") && "2".equals(this.estatus)		) {
				
			cond1.append(
				"    AND (    ff.ic_if IS NULL"   +
				"         AND ff.df_fecha_venc IS NOT NULL"   +
				"         AND ff.ic_estatus_docto = 2"   +
				"        )"  
			);
				
		} else if(	this.tipoDispersion.equals("P") && "PA".equals(this.estatus)	) {
				
			cond1.append(
				"    AND (    ff.ic_if IS NULL"   +
				"         AND ff.df_operacion IS NOT NULL"   +
				"         AND ff.ic_estatus_docto IS NULL"   +
				"        )"  
			);
				
		}

		if( !this.fechaRegistroInicial.equals("") && !this.fechaRegistroFinal.equals("") ){
				
			cond1.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy')      "  + // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy') + 1) "    // this.fechaRegistroFinal
			);
			cond1Conditions.add(this.fechaRegistroInicial);
			cond1Conditions.add(this.fechaRegistroFinal);
				
		}
		
/*
			String cond = (sTipoDispersion.equals("E")?"and (FF.df_fecha_venc is not null or FF.df_operacion is not null)":(sTipoDispersion.equals("F")?"and (FF.ic_if is not null and FF.df_fecha_venc is null) ":""))+
							((!sFechaRegIni.equals("") && !sFechaRegFin.equals(""))?"and trunc(FF.df_registro) between TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') and TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy') ":"");
*/

		String epoEspecifica 				= "";
		List	 epoEspecificaConditions 	= new ArrayList();
		String ifEspecifica  				= "";
		List	 ifEspecificaConditions  	= new ArrayList();

		if(        !"".equals(this.claveEPO) ){
			epoEspecifica = " AND FF.ic_epo = ? "; // this.claveEPO
			epoEspecificaConditions.add( this.claveEPO );
		} else if( !"".equals(this.claveIF)  ){
			ifEspecifica  = " AND FF.ic_if  = ? "; // this.claveIF
			ifEspecificaConditions.add(  this.claveIF  );
		}

		String condEPOs =
			"AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
   		"AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') "+
   		"AND c.ic_producto_nafin  = 1 ";
   				
		String condEPOsErr =
   		"AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
   		"AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 1 "+
   		"AND c.ic_moneda (+) = 1 ";

		String condIFs =
   		"AND crn.ic_nafin_electronico = c.ic_nafin_electronico "+
//   		"AND crn.ic_epo_pyme_if = ff.ic_pyme "+
   		"AND c.ic_producto_nafin  = 1 ";
   				
		String condIFsErr =
   		"AND crn.ic_nafin_electronico(+) = c.ic_nafin_electronico "+
//   		"AND crn.ic_epo_pyme_if = ff.ic_pyme "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 1 "+
   		"AND c.ic_moneda (+) = 1 ";

		String condPEMEXs =
   		"AND crn.ic_epo_pyme_if = ff.ic_epo "+
   		"AND crn.cg_tipo = 'E' "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 4 "+
   		"AND c.ic_moneda (+) = 1 ";
 	
		StringBuffer cond2 	 				= new StringBuffer(64);
		List			 cond2Conditions 		= new ArrayList();
		StringBuffer cond2Err 				= new StringBuffer(64);
		List			 cond2ErrConditions	= new ArrayList();
			
		if(			this.tipoDispersion.equals("E")	){
				
			cond2.append(" ");
			cond2.append(condEPOs);
			cond2.append(
				" and (FF.df_fecha_venc is not null or FF.df_operacion is not null) and ff.ic_estatus_docto in (1, 4, 9,10, 11) "
			);
				
			cond2Err.append(" ");
			cond2Err.append(condEPOsErr);
			cond2Err.append(
				" and (FF.df_fecha_venc is not null or FF.df_operacion is not null) and ff.ic_estatus_docto in (1, 4, 9,10, 11) "
			);
				
		} else if(	this.tipoDispersion.equals("F")	){
				
			cond2.append(" ");
			cond2.append(condIFs);
			cond2.append(
				" and (FF.ic_if is not null and FF.df_fecha_venc is null) "
			);
				
			cond2Err.append(" ");
			cond2Err.append(condIFsErr);
			cond2Err.append(
				" and (FF.ic_if is not null and FF.df_fecha_venc is null) "
			);
				
		} else if(	this.tipoDispersion.equals("P") && "2".equals(this.estatus ) ){
				
			cond2.append(" ");
			cond2.append(condPEMEXs);
			cond2.append(
				" AND (ff.ic_if IS NULL AND ff.df_fecha_venc IS NOT NULL AND ff.ic_estatus_docto = 2) "
			);
				
		} else if(  this.tipoDispersion.equals("P") && "PA".equals(this.estatus) ){
				
			cond2.append(" ");
			cond2.append(condPEMEXs);
			cond2.append(
				" AND (ff.ic_if IS NULL AND ff.df_operacion IS NOT NULL AND ff.ic_estatus_docto IS NULL) "
			);
				
		}
			
		if( !this.fechaRegistroInicial.equals("") && !this.fechaRegistroFinal.equals("") ) {
				
			cond2.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy')    "  + // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy')+1) "    // this.fechaRegistroFinal
			);
			cond2Conditions.add(this.fechaRegistroInicial);
			cond2Conditions.add(this.fechaRegistroFinal);
				
			cond2Err.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy') "+ // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy')+1) " // this.fechaRegistroFinal
			);
			cond2ErrConditions.add(this.fechaRegistroInicial);
			cond2ErrConditions.add(this.fechaRegistroFinal);
				
		}
/*
		String cond2 = (sTipoDispersion.equals("E")?condEPOs+" and (FF.df_fecha_venc is not null or FF.df_operacion is not null)":(sTipoDispersion.equals("F")?condIFs+" and (FF.ic_if is not null and FF.df_fecha_venc is null) ":""))+
						((!sFechaRegIni.equals("") && !sFechaRegFin.equals(""))?"and trunc(FF.df_registro) between TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') and TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy') ":"");

*/

		String condvia = "";
		if( !"".equals(this.viaLiquidacion) ) {
				 
			if(        "T".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'TEF'   ";
			} else if( "S".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'SPEUA' ";
			} else if( "I".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'SPEI'  ";
			}
				
		}
 			
		String condMonitor					= "";
		String condConsultaEpo				= "";
		List   condConsultaEpoConditions = new ArrayList();
			
		if( this.tipo.equals("Monitor") ) {
			condMonitor 		= "    AND ME.status_cen_i IN (-20, -1, 42, 44, 60, 9999) ";
		}
		if ( !"".equals(this.estatusFF)  ) {
			condConsultaEpo 	= "    AND ME.status_cen_i >= ? "; // this.estatusFF
			condConsultaEpoConditions.add(this.estatusFF);
		}

		// Preparadar condiciones in de los pageids...
		StringBuffer condPageIds = new StringBuffer(64);
		condPageIds.append("		 AND ff.ic_flujo_fondos IN ( ");
		for(int i=0;i<pageIds.size();i++) {
			if(i>0){
				condPageIds.append(",");
			}
			condPageIds.append("?");
		};
		condPageIds.append("		 )      ");
			
		this.query.append(
			" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff btt)*/"   +
			"         e.cg_razon_social as razon_social_epo,"   +
			"         i.cg_razon_social as razon_social_if,"   +
			"         p.cg_razon_social as razon_social_pyme,"   +
			"         p.cg_rfc,"   +
			"         to_char(c.ic_bancos_tef) as ic_bancos_tef,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			"         to_char(c.ic_tipo_cuenta) as ic_tipo_cuenta,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			"         c.cg_cuenta,"   +
			"         ff.fn_importe,"   +
			"         me.error_cen_s,"   +
			"         ff.ic_flujo_fondos,"   +
			"         me.status_cen_i || ' ' || eff.cd_descripcion as estatus_descripcion,"   +
			"         TO_CHAR (ff.df_registro, 'dd/mm/yyyy') as df_registro,"   +
			"         ff.cg_via_liquidacion,"   +
			"         c.ic_estatus_cecoban,"   +
			"         c.ic_cuenta,"   +
			"         ff.cs_reproceso,"   +
			"         e.ic_epo,"   +
			"         ff.ic_pyme,"   +
			"         ff.ic_if,"   +
			"         ff.ic_estatus_docto,"   +
			" 		    TO_CHAR(ff.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc, "   + // Para que en la paginacion no truene
			" 		    TO_CHAR(ff.df_operacion, 'dd/mm/yyyy') as df_operacion, "   + // Para que en la paginacion no truene
			"         'FFON' origen,"   +
			"         me.status_cen_i, "   +
			"			 btef.cc_clave_dcat_s || ' - ' || btef.cd_descripcion as inst_cuenta_benef, "	+
			"         nvl(iff.cg_razon_social,'N/A') as razon_social_iffon "   +
			"   FROM int_flujo_fondos ff,"   +
			"        cfe_m_encabezado me,"   +
			"        comrel_nafin crn,"   +
			"        comcat_epo e,"   +
			"        comcat_if i,"   +
			"        comcat_if iff,"   +
			"        comcat_pyme p,"   +
			"        com_cuentas c,"   +
			"        comcat_estatus_ff eff,"   +
			"        comcat_bancos_tef btef"   +
			"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
			"    AND ff.ic_epo = e.ic_epo"   +
			"    AND ff.ic_if = i.ic_if(+)"   +
			"    AND ff.ic_if_fondeo = iff.ic_if(+)"   +
			"    AND ff.ic_pyme = p.ic_pyme(+)"   +
			"    AND ff.ic_cuenta = c.ic_cuenta"   +
			"    AND c.ic_bancos_tef = btef.ic_bancos_tef"   +
			"    AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
			//"    AND ff.cs_reproceso IN ('N', 'D')"   +
			"    AND ff.cs_reproceso ='N' "+
			"    AND ff.cs_concluido = 'N'"
		);
		this.query.append(
			cond2 
		);
		this.query.append(
			condvia 
		);
		this.query.append(
			epoEspecifica 
		);
		this.query.append(
			ifEspecifica 
		);
		this.query.append(
			condMonitor 
		);
		this.query.append(
			condConsultaEpo 
		);
		this.query.append(
			condPageIds 
		);
		this.query.append(
			" UNION ALL "+
			" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/"   +
			"         e.cg_razon_social as razon_social_epo,"   +
			"         i.cg_razon_social as razon_social_if,"   +
			"         p.cg_razon_social as razon_social_pyme,"   +
			"         p.cg_rfc,"   +
			"         to_char(c.ic_bancos_tef) as ic_bancos_tef,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			"         to_char(c.ic_tipo_cuenta) as ic_tipo_cuenta,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			"         c.cg_cuenta,"   +
			"         ff.fn_importe,"   +
			"         me.error_cen_s,"   +
			"         ff.ic_flujo_fondos,"   +
			"         me.status_cen_i || ' ' || eff.cd_descripcion as estatus_descripcion,"   +
			"         TO_CHAR (ff.df_registro, 'dd/mm/yyyy') as df_registro,"   +
			"         ff.cg_via_liquidacion,"   +
			"         c.ic_estatus_cecoban,"   +
			"         c.ic_cuenta,"   +
			"         ff.cs_reproceso,"   +
			"         e.ic_epo,"   +
			"         ff.ic_pyme,"   +
			"         ff.ic_if,"   +
			"         ff.ic_estatus_docto,"   +
			" 		    TO_CHAR(ff.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc, "   + // Para que en la paginacion no truene
			" 		    TO_CHAR(ff.df_operacion, 'dd/mm/yyyy') as df_operacion, "   + // Para que en la paginacion no truene
			"         'ERR' origen,"   +
			"         me.status_cen_i, "   +
			"			 ' '  as inst_cuenta_benef, "	+
			"         nvl(iff.cg_razon_social,'N/A') as razon_social_iffon "   +
			"   FROM int_flujo_fondos_err ff,"   +
			"        cfe_m_encabezado_err me,"   +
			"        comrel_nafin crn,"   +
			"        comcat_epo e,"   +
			"        comcat_if i,"   +
			"        comcat_if iff,"   +
			"        comcat_pyme p,"   +
			"        com_cuentas c,"   +
			"        comcat_estatus_ff eff"   +
			"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
			"    AND ff.ic_epo = e.ic_epo"   +
			"    AND ff.ic_if = i.ic_if(+)"   +
			"    AND ff.ic_if_fondeo = iff.ic_if(+)"   +
			"    AND ff.ic_pyme = p.ic_pyme(+)"   +
			"    AND ff.ic_cuenta = c.ic_cuenta(+)"   +
			"    AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
			//"    AND ff.cs_reproceso IN ('N', 'D')"   +
			"    AND ff.cs_reproceso ='N' "+
			"    AND ff.cs_concluido = 'N'"
		);
		this.query.append(
			cond2Err 
		);
		this.query.append(
			condvia 
		);
		this.query.append(
			epoEspecifica 
		);
		this.query.append(
			ifEspecifica 
		);
		this.query.append(
			condMonitor 
		);
		this.query.append(
			condConsultaEpo 
		);
		this.query.append(
			condPageIds
		);
			
		// AGREGAR CONDICIONES
			
		this.conditions.addAll(cond2Conditions); 
		// this.conditions.addAll(condviaConditions);
		this.conditions.addAll(epoEspecificaConditions);
		this.conditions.addAll(ifEspecificaConditions);
		// this.conditions.addAll(condMonitorConditions);
		this.conditions.addAll(condConsultaEpoConditions);
			
		for(int i=0;i<pageIds.size();i++) {
			List item = (List)pageIds.get(i);
			this.conditions.add(item.get(0).toString());	
		}
		
		this.conditions.addAll(cond2ErrConditions); 
		// this.conditions.addAll(condviaConditions);
		this.conditions.addAll(epoEspecificaConditions);
		this.conditions.addAll(ifEspecificaConditions); 
		// this.conditions.addAll(condMonitorConditions);
		this.conditions.addAll(condConsultaEpoConditions);
			
		for(int i=0;i<pageIds.size();i++) {
			List item = (List)pageIds.get(i);
			this.conditions.add(item.get(0).toString());	
		}

		if( this.tipo.equals("Consulta") ) {
				
			this.query.append(
				" UNION ALL"   +
				" SELECT /*+index(ff) use_nl(ff me e i p c eff)*/"   +
				" 		    e.cg_razon_social as razon_social_epo,"   +
			   "         i.cg_razon_social as razon_social_if,"   +
			   "         p.cg_razon_social as razon_social_pyme,"   +
			   "         p.cg_rfc,"   +
				"         to_char(c.ic_bancos_tef) as ic_bancos_tef,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			   "         to_char(c.ic_tipo_cuenta) as ic_tipo_cuenta,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			   "         c.cg_cuenta,"   +
			   "         ff.fn_importe,"   +
			   "         NULL as error_cen_s,"   +
				" 		    ff.ic_flujo_fondos,"   +
			   "         me.status_cen_i || ' ' || eff.cd_descripcion as estatus_descripcion,"   +
				" 		    TO_CHAR (ff.df_registro, 'dd/mm/yyyy') as df_registro,"   +
			   "         ff.cg_via_liquidacion,"   +
				" 		    c.ic_estatus_cecoban,"   +
			   "         c.ic_cuenta,"   +
			   "         ff.cs_reproceso,"   +
			   "         e.ic_epo,"   +
				" 		    ff.ic_pyme,"   +
			   "         ff.ic_if,"   +
			   "         ff.ic_estatus_docto,"   +
			   " 		    TO_CHAR(ff.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc, "   + // Para que en la paginacion no truene
			   " 		    TO_CHAR(ff.df_operacion, 'dd/mm/yyyy') as df_operacion, "   + // Para que en la paginacion no truene
			   "         'FFON' origen,"   +
			   "         me.status_cen_i, "   +
				"			 ' '  as inst_cuenta_benef, "	+
				"         nvl(iff.cg_razon_social,'N/A') as razon_social_iffon "   +
				"   FROM int_flujo_fondos ff,"   +
				" 		 cfe_m_encabezado me,"   +
				" 		 comcat_epo e,"   +
				" 		 comcat_if i,"   +
				" 		 comcat_if iff,"   +
				" 		 comcat_pyme p,"   +
				" 		 com_cuentas c,"   +
				" 		 comcat_estatus_ff eff"   +
				"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
				" 	AND ff.ic_epo = e.ic_epo"   +
				" 	AND ff.ic_if = i.ic_if(+)"   +
				" 	AND ff.ic_if_fondeo = iff.ic_if(+)"   +
				" 	AND ff.ic_pyme = p.ic_pyme(+)"   +
				" 	AND ff.ic_cuenta = c.ic_cuenta(+)"   +
				" 	AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
				" 	AND ff.cs_reproceso IN ('N', 'D')"   +
				" 	AND ff.cs_concluido = 'S'"
			);
			this.query.append(
				cond1 
			);
			this.query.append(
				condvia 
			);
			this.query.append(
				epoEspecifica 
			);
			this.query.append(
				ifEspecifica 
			);
			this.query.append(
				condConsultaEpo 
			);
			this.query.append(
				condPageIds
			);
 
			this.conditions.addAll(cond1Conditions);
			// this.conditions.addAll(condviaConditions); 
			this.conditions.addAll(epoEspecificaConditions);
			this.conditions.addAll(ifEspecificaConditions);
			this.conditions.addAll(condConsultaEpoConditions);
				
			for(int i=0;i<pageIds.size();i++) {
				List item = (List)pageIds.get(i);
				this.conditions.add(item.get(0).toString());	
			}
				
		}

		// EQUIVALENCIAS
		
		// 00 razon_social_epo
		// 01 razon_social_if
		// 02 razon_social_pyme
		// 03 cg_rfc
		// 04 ic_bancos_tef
		// 05 ic_tipo_cuenta
		// 06 cg_cuenta
		// 07 fn_importe
		// 08 error_cen_s
		// 09 ic_flujo_fondos
		// 10 estatus_descripcion
		// 11 df_registro
		// 12 cg_via_liquidacion
		// 13 ic_estatus_cecoban
		// 14 ic_cuenta
		// 15 cs_reproceso
		// 16 ic_epo
		// 17 ic_pyme
		// 18 ic_if
		// 19 ic_estatus_docto
		// 20 df_fecha_venc
		// 21 df_operacion
		// 22 origen
		// 23 status_cen_i
		// 24 inst_cuenta_benef
		// 25 razon_social_iffon

		log.debug("getDocumentSummaryQueryForIds.tipo                 = <" + this.tipo                 + ">");
		log.debug("getDocumentSummaryQueryForIds.tipoDispersion       = <" + this.tipoDispersion       + ">");
		log.debug("getDocumentSummaryQueryForIds.fechaRegistroInicial = <" + this.fechaRegistroInicial + ">");
		log.debug("getDocumentSummaryQueryForIds.fechaRegistroFinal   = <" + this.fechaRegistroFinal   + ">");
		log.debug("getDocumentSummaryQueryForIds.viaLiquidacion       = <" + this.viaLiquidacion       + ">");
		log.debug("getDocumentSummaryQueryForIds.claveEPO             = <" + this.claveEPO             + ">");
		log.debug("getDocumentSummaryQueryForIds.claveIF              = <" + this.claveIF              + ">");
		log.debug("getDocumentSummaryQueryForIds.estatus              = <" + this.estatus              + ">");
		log.debug("getDocumentSummaryQueryForIds.estatusFF            = <" + this.estatusFF            + ">");
		log.debug("getDocumentSummaryQueryForIds.pageIds              = <" + pageIds                   + ">");

		log.debug("getDocumentSummaryQueryForIds.query                = <" + this.query                + ">");
		log.debug("getDocumentSummaryQueryForIds.conditions           = <" + this.conditions           + ">");
		
		log.info("getDocumentSummaryQueryForIds(S)");
 
		return this.query.toString();
					
	}
	
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		
		log.info("getAggregateCalculationQuery(E)");
		
		this.query			= new StringBuffer();
		this.conditions	= new ArrayList();
		
		StringBuffer cond1 				= new StringBuffer(64);
		List			 cond1Conditions 	= new ArrayList();
		
		if(			this.tipoDispersion.equals("E") 								) {
				
			cond1.append(
				"    AND (ff.df_fecha_venc IS NOT NULL OR ff.df_operacion IS NOT NULL)"   +
				"    AND ff.ic_estatus_docto IN (1, 4, 9, 10, 11)"
			);
				
		} else if(	this.tipoDispersion.equals("F") 											) {
				
			cond1.append(
				"    AND (ff.ic_if IS NOT NULL AND ff.df_fecha_venc IS NULL)"  
			);
				
		} else if(	this.tipoDispersion.equals("P") && "2".equals(this.estatus)		) {
				
			cond1.append(
				"    AND (    ff.ic_if IS NULL"   +
				"         AND ff.df_fecha_venc IS NOT NULL"   +
				"         AND ff.ic_estatus_docto = 2"   +
				"        )"  
			);
				
		} else if(	this.tipoDispersion.equals("P") && "PA".equals(this.estatus)	) {
				
			cond1.append(
				"    AND (    ff.ic_if IS NULL"   +
				"         AND ff.df_operacion IS NOT NULL"   +
				"         AND ff.ic_estatus_docto IS NULL"   +
				"        )"  
			);
				
		}

		if( !this.fechaRegistroInicial.equals("") && !this.fechaRegistroFinal.equals("") ){
				
			cond1.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy')      "  + // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy') + 1) "    // this.fechaRegistroFinal
			);
			cond1Conditions.add( this.fechaRegistroInicial );
			cond1Conditions.add( this.fechaRegistroFinal   );
			
		}
/*
			String cond = (sTipoDispersion.equals("E")?"and (FF.df_fecha_venc is not null or FF.df_operacion is not null)":(sTipoDispersion.equals("F")?"and (FF.ic_if is not null and FF.df_fecha_venc is null) ":""))+
							((!sFechaRegIni.equals("") && !sFechaRegFin.equals(""))?"and trunc(FF.df_registro) between TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') and TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy') ":"");
*/

		String epoEspecifica 				= "";
		List	 epoEspecificaConditions	= new ArrayList();
		String ifEspecifica  				= "";
		List	 ifEspecificaConditions		= new ArrayList();
 
		if(        !"".equals(this.claveEPO) ){
			epoEspecifica = " AND FF.ic_epo = ? "; // this.claveEPO
			epoEspecificaConditions.add(   this.claveEPO );
		} else if( !"".equals(this.claveIF)  ){
			ifEspecifica  = " AND FF.ic_if  = ? "; // this.claveIF
			ifEspecificaConditions.add( this.claveIF  );
		}

		String condEPOs =
   		"AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
   		"AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') "+
   		"AND c.ic_producto_nafin  = 1 ";
   				
		String condEPOsErr =
   		"AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
   		"AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 1 "+
   		"AND c.ic_moneda (+) = 1 ";

		String condIFs =
   		"AND crn.ic_nafin_electronico = c.ic_nafin_electronico "+
//   		"AND crn.ic_epo_pyme_if = ff.ic_pyme "+
   		"AND c.ic_producto_nafin  = 1 ";
   				
		String condIFsErr =
   		"AND crn.ic_nafin_electronico(+) = c.ic_nafin_electronico "+
//   		"AND crn.ic_epo_pyme_if = ff.ic_pyme "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 1 "+
   		"AND c.ic_moneda (+) = 1 ";

		String condPEMEXs =
   		"AND crn.ic_epo_pyme_if = ff.ic_epo "+
   		"AND crn.cg_tipo = 'E' "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 4 "+
   		"AND c.ic_moneda (+) = 1 ";
 	
		StringBuffer cond2 	 				= new StringBuffer(64);
		List			 cond2Conditions 		= new ArrayList();
		StringBuffer cond2Err 				= new StringBuffer(64);
		List			 cond2ErrConditions	= new ArrayList();
			
		if(			this.tipoDispersion.equals("E")	){
				
			cond2.append(" ");
			cond2.append(condEPOs);
			cond2.append(
				" and (FF.df_fecha_venc is not null or FF.df_operacion is not null) and ff.ic_estatus_docto in (1, 4, 9,10, 11) "
			);
			
			cond2Err.append(" ");
			cond2Err.append(condEPOsErr);
			cond2Err.append(
				" and (FF.df_fecha_venc is not null or FF.df_operacion is not null) and ff.ic_estatus_docto in (1, 4, 9,10, 11) "
			);
				
		} else if(	this.tipoDispersion.equals("F")	){
				
			cond2.append(" ");
			cond2.append(condIFs);
			cond2.append(
				" and (FF.ic_if is not null and FF.df_fecha_venc is null) "
			);
			
			cond2Err.append(" ");
			cond2Err.append(condIFsErr);
			cond2Err.append(
				" and (FF.ic_if is not null and FF.df_fecha_venc is null) "
			);
				
		} else if(	this.tipoDispersion.equals("P") && "2".equals(this.estatus ) ){
				
			cond2.append(" ");
			cond2.append(condPEMEXs);
			cond2.append(
				" AND (ff.ic_if IS NULL AND ff.df_fecha_venc IS NOT NULL AND ff.ic_estatus_docto = 2) "
			);
				
		} else if(  this.tipoDispersion.equals("P") && "PA".equals(this.estatus) ){
				
			cond2.append(" ");
			cond2.append(condPEMEXs);
			cond2.append(
				" AND (ff.ic_if IS NULL AND ff.df_operacion IS NOT NULL AND ff.ic_estatus_docto IS NULL) "
			);
				
		}
			
		if( !this.fechaRegistroInicial.equals("") && !this.fechaRegistroFinal.equals("") ) {
				
			cond2.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy')    "  + // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy')+1) "    // this.fechaRegistroFinal
			);
			cond2Conditions.add(this.fechaRegistroInicial);
			cond2Conditions.add(this.fechaRegistroFinal);
			
			cond2Err.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy')    "  + // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy')+1) "    // this.fechaRegistroFinal
			);
			cond2ErrConditions.add(this.fechaRegistroInicial);
			cond2ErrConditions.add(this.fechaRegistroFinal);
				
		}
			
/*
			String cond2 = (sTipoDispersion.equals("E")?condEPOs+" and (FF.df_fecha_venc is not null or FF.df_operacion is not null)":(sTipoDispersion.equals("F")?condIFs+" and (FF.ic_if is not null and FF.df_fecha_venc is null) ":""))+
							((!sFechaRegIni.equals("") && !sFechaRegFin.equals(""))?"and trunc(FF.df_registro) between TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') and TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy') ":"");

*/

		String condvia = "";
		if(!"".equals(this.viaLiquidacion)) {
				 
			if(        "T".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'TEF'   ";
			} else if( "S".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'SPEUA' ";
			} else if( "I".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'SPEI'  ";
			}
				
		}
 
		if( this.tipo.equals("Monitor") ) {
				
			this.query.append(
				" SELECT ffa.enproceso as EN_PROCESO, ffb.aceptados + ffb2.aceptados as ACEPTADOS, ffc.conerror as CON_ERROR "   +
				"   FROM (SELECT COUNT (1) AS enproceso"   +
				"           FROM int_flujo_fondos ff, cfe_m_encabezado e"   +
				"          WHERE ff.ic_flujo_fondos = e.idoperacion_cen_i"   +
				"            AND e.status_cen_i = 0"   +
				"            AND ff.cs_reproceso = 'N' "
			);
			this.query.append(
				cond1
			);
			this.query.append(
				epoEspecifica
			);
			this.query.append(
				ifEspecifica
			);
			this.query.append(
				" ) ffa,"   +
				"        (SELECT COUNT (1) AS aceptados"   +
				"           FROM int_flujo_fondos ff, cfe_m_encabezado e"   +
				"          WHERE ff.ic_flujo_fondos = e.idoperacion_cen_i"   +
				"            AND e.status_cen_i >= 1"   +
				"            AND ff.cs_reproceso = 'N' "
			);
			this.query.append(
				cond1
			);
			this.query.append(
				epoEspecifica
			);
			this.query.append(
				ifEspecifica
			);
			this.query.append(
				" ) ffb,"   +
				"        (SELECT COUNT (1) AS aceptados"   +
				"           FROM int_flujo_fondos ff"   +
				"          WHERE ff.cs_reproceso = 'N'"   +
				"            AND ff.cs_concluido = 'S' " 
			);
			this.query.append(
				cond1
			);
			this.query.append(
				epoEspecifica
			);
			this.query.append(
				ifEspecifica
			);
			this.query.append(
				" ) ffb2,"   +
				"        (SELECT SUM (conerror) conerror"   +
				"           FROM (SELECT COUNT (1) AS conerror"   +
				"                   FROM int_flujo_fondos ff, cfe_m_encabezado e"   +
				"                  WHERE ff.ic_flujo_fondos = e.idoperacion_cen_i"   +
				"                    AND e.status_cen_i = -1"   +
				"                    AND ff.cs_reproceso = 'N' "
			);
			this.query.append(
				cond1
			);
			this.query.append(
				epoEspecifica
			);
			this.query.append(
				ifEspecifica
			);
			this.query.append(
				" "   +
				"                 UNION ALL"   +
				"                 SELECT COUNT (1) AS conerror"   +
				"                   FROM int_flujo_fondos_err ff, cfe_m_encabezado_err e"   +
				"                  WHERE ff.ic_flujo_fondos = e.idoperacion_cen_i"   +
				"                    AND e.status_cen_i IN (-20, -1, 42, 44) "   +
				"                    AND ff.cs_reproceso = 'N' "
			);
			this.query.append(
				cond1
			);
			this.query.append(
				epoEspecifica
			);
			this.query.append(
				ifEspecifica
			);
			this.query.append(
				" )) ffc"  
			);
					
			for(int i=0;i<5;i++){
				this.conditions.addAll( cond1Conditions         );
				this.conditions.addAll( epoEspecificaConditions );
				this.conditions.addAll( ifEspecificaConditions  );
			}
			
		} else if( this.tipo.equals("Consulta") ){
			
			String condMonitor					= "";
			String condConsultaEpo				= "";
			List   condConsultaEpoConditions = new ArrayList();
		
			if( this.tipo.equals("Monitor") ) { // Nota: Obviamente no se usa, se deja para seguir el flujo de la logica de getDocumentSummaryQueryForIds(List pageIds)
				condMonitor 		= "    AND ME.status_cen_i IN (-20, -1, 42, 44, 60, 9999) ";
			}
			if ( !"".equals(this.estatusFF)  ) {
				condConsultaEpo 	= "    AND ME.status_cen_i >= ? "; // this.estatusFF
				condConsultaEpoConditions.add(this.estatusFF);
			}

			this.query.append(
				"SELECT                                 "  +
				"  SUM(FN_IMPORTE) AS TOTAL_IMPORTE,    "  +
				"  COUNT(1)        AS TOTAL_OPERACIONES "  +
				"FROM                                   "  +
				"(                                      "
			);
	
			this.query.append(
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff btt)*/"   +
				"         ff.fn_importe as fn_importe "   +
				"   FROM int_flujo_fondos ff,"   +
				"        cfe_m_encabezado me,"   +
				"        comrel_nafin crn,"   +
				"        comcat_epo e,"   +
				"        comcat_if i,"   +
				"        comcat_if iff,"   +
				"        comcat_pyme p,"   +
				"        com_cuentas c,"   +
				"        comcat_estatus_ff eff,"   +
				"        comcat_bancos_tef btef"   +
				"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
				"    AND ff.ic_epo = e.ic_epo"   +
				"    AND ff.ic_if = i.ic_if(+)"   +
				"    AND ff.ic_if_fondeo = iff.ic_if(+)"   +
				"    AND ff.ic_pyme = p.ic_pyme(+)"   +
				"    AND ff.ic_cuenta = c.ic_cuenta"   +
				"    AND c.ic_bancos_tef = btef.ic_bancos_tef"   +
				"    AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
				//"    AND ff.cs_reproceso IN ('N', 'D')"   +
				"    AND ff.cs_reproceso ='N' "+
				"    AND ff.cs_concluido = 'N'"
			);
			this.query.append(
				cond2 
			);
			this.query.append(
				condvia 
			);
			this.query.append(
				epoEspecifica 
			);
			this.query.append(
				ifEspecifica 
			);
			this.query.append(
				condMonitor 
			);
			this.query.append(
				condConsultaEpo 
			);
			this.query.append(
				" UNION ALL "+
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/"   +
				"         ff.fn_importe as fn_importe "   +
				"   FROM int_flujo_fondos_err ff,"   +
				"        cfe_m_encabezado_err me,"   +
				"        comrel_nafin crn,"   +
				"        comcat_epo e,"   +
				"        comcat_if i,"   +
				"        comcat_if iff,"   +
				"        comcat_pyme p,"   +
				"        com_cuentas c,"   +
				"        comcat_estatus_ff eff"   +
				"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
				"    AND ff.ic_epo = e.ic_epo"   +
				"    AND ff.ic_if = i.ic_if(+)"   +
				"    AND ff.ic_if_fondeo = iff.ic_if(+)"   +
				"    AND ff.ic_pyme = p.ic_pyme(+)"   +
				"    AND ff.ic_cuenta = c.ic_cuenta(+)"   +
				"    AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
				//"    AND ff.cs_reproceso IN ('N', 'D')"   +
				"    AND ff.cs_reproceso ='N' "+
				"    AND ff.cs_concluido = 'N'"
			);
			this.query.append(
				cond2Err 
			);
			this.query.append(
				condvia 
			);
			this.query.append(
				epoEspecifica 
			);
			this.query.append(
				ifEspecifica 
			);
			this.query.append(
				condMonitor 
			);
			this.query.append(
				condConsultaEpo 
			);
				
			// AGREGAR CONDICIONES
				
			this.conditions.addAll(cond2Conditions); 
			// this.conditions.addAll(condviaConditions);
			this.conditions.addAll(epoEspecificaConditions);
			this.conditions.addAll(ifEspecificaConditions);
			// this.conditions.addAll(condMonitorConditions);
			this.conditions.addAll(condConsultaEpoConditions);
				
			this.conditions.addAll(cond2ErrConditions); 
			// this.conditions.addAll(condviaConditions);
			this.conditions.addAll(epoEspecificaConditions);
			this.conditions.addAll(ifEspecificaConditions); 
			// this.conditions.addAll(condMonitorConditions);
			this.conditions.addAll(condConsultaEpoConditions);
				
			if( this.tipo.equals("Consulta") ) {
					
				this.query.append(
					" UNION ALL"   +
					" SELECT /*+index(ff) use_nl(ff me e i p c eff)*/"   +
					"         ff.fn_importe as fn_importe "   +
					"   FROM int_flujo_fondos ff,"   +
					" 		 cfe_m_encabezado me,"   +
					" 		 comcat_epo e,"   +
					" 		 comcat_if i,"   +
					" 		 comcat_if iff,"   +
					" 		 comcat_pyme p,"   +
					" 		 com_cuentas c,"   +
					" 		 comcat_estatus_ff eff"   +
					"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
					" 	AND ff.ic_epo = e.ic_epo"   +
					" 	AND ff.ic_if = i.ic_if(+)"   +
					" 	AND ff.ic_if_fondeo = iff.ic_if(+)"   +
					" 	AND ff.ic_pyme = p.ic_pyme(+)"   +
					" 	AND ff.ic_cuenta = c.ic_cuenta(+)"   +
					" 	AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
					" 	AND ff.cs_reproceso IN ('N', 'D')"   +
					" 	AND ff.cs_concluido = 'S'"
				);
				this.query.append(
					cond1 
				);
				this.query.append(
					condvia 
				);
				this.query.append(
					epoEspecifica 
				);
				this.query.append(
					ifEspecifica 
				);
				this.query.append(
					condConsultaEpo 
				);
	 
				this.conditions.addAll(cond1Conditions);
				// this.conditions.addAll(condviaConditions); 
				this.conditions.addAll(epoEspecificaConditions);
				this.conditions.addAll(ifEspecificaConditions);
				this.conditions.addAll(condConsultaEpoConditions);
									
			}
			
			this.query.append(
				")                                     "
			);
		
		}
			
		log.debug("getAggregateCalculationQuery.tipo                 = <" + this.tipo                 + ">");
		log.debug("getAggregateCalculationQuery.tipoDispersion       = <" + this.tipoDispersion       + ">");
		log.debug("getAggregateCalculationQuery.fechaRegistroInicial = <" + this.fechaRegistroInicial + ">");
		log.debug("getAggregateCalculationQuery.fechaRegistroFinal   = <" + this.fechaRegistroFinal   + ">");
		log.debug("getAggregateCalculationQuery.viaLiquidacion       = <" + this.viaLiquidacion       + ">");
		log.debug("getAggregateCalculationQuery.claveEPO             = <" + this.claveEPO             + ">");
		log.debug("getAggregateCalculationQuery.claveIF              = <" + this.claveIF              + ">");
		log.debug("getAggregateCalculationQuery.estatus              = <" + this.estatus              + ">");
		log.debug("getAggregateCalculationQuery.estatusFF            = <" + this.estatusFF            + ">");

		log.debug("getAggregateCalculationQuery.query                = <" + this.query                + ">");
		log.debug("getAggregateCalculationQuery.conditions           = <" + this.conditions           + ">");
		
		log.info("getAggregateCalculationQuery(S)");
 
		return this.query.toString();
			
	}
 
	/**
	 * Este m�todo regresa una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
	public List getConditions(){
		return conditions;
	}
	 
	/**
	 * Este m�todo regresa el query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQueryFile(){
		
		log.info("getDocumentQueryFile(E)");
		
		this.query			= new StringBuffer();
		this.conditions	= new ArrayList();
		
		StringBuffer cond1 				= new StringBuffer(64);
		List			 cond1Conditions	= new ArrayList();
			
		if(			this.tipoDispersion.equals("E") 												) {
				
			cond1.append(
				"    AND (ff.df_fecha_venc IS NOT NULL OR ff.df_operacion IS NOT NULL)"   +
				"    AND ff.ic_estatus_docto IN (1, 4, 9, 10, 11)"
			);
				
		} else if(	this.tipoDispersion.equals("F") 											) {
				
			cond1.append(
				"    AND (ff.ic_if IS NOT NULL AND ff.df_fecha_venc IS NULL)"  
			);
				
		} else if(	this.tipoDispersion.equals("P") && "2".equals(this.estatus)		) {
				
			cond1.append(
				"    AND (    ff.ic_if IS NULL"   +
				"         AND ff.df_fecha_venc IS NOT NULL"   +
				"         AND ff.ic_estatus_docto = 2"   +
				"        )"  
			);
				
		} else if(	this.tipoDispersion.equals("P") && "PA".equals(this.estatus)	) {
				
			cond1.append(
				"    AND (    ff.ic_if IS NULL"   +
				"         AND ff.df_operacion IS NOT NULL"   +
				"         AND ff.ic_estatus_docto IS NULL"   +
				"        )"  
			);
				
		}

		if( !this.fechaRegistroInicial.equals("") && !this.fechaRegistroFinal.equals("") ){
				
			cond1.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy')      "  + // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy') + 1) "    // this.fechaRegistroFinal
			);
			cond1Conditions.add(this.fechaRegistroInicial);
			cond1Conditions.add(this.fechaRegistroFinal);
				
		}
		
/*
			String cond = (sTipoDispersion.equals("E")?"and (FF.df_fecha_venc is not null or FF.df_operacion is not null)":(sTipoDispersion.equals("F")?"and (FF.ic_if is not null and FF.df_fecha_venc is null) ":""))+
							((!sFechaRegIni.equals("") && !sFechaRegFin.equals(""))?"and trunc(FF.df_registro) between TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') and TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy') ":"");
*/

		String epoEspecifica 				= "";
		List	 epoEspecificaConditions 	= new ArrayList();
		String ifEspecifica  				= "";
		List	 ifEspecificaConditions  	= new ArrayList();

		if(        !"".equals(this.claveEPO) ){
			epoEspecifica = " AND FF.ic_epo = ? "; // this.claveEPO
			epoEspecificaConditions.add( this.claveEPO );
		} else if( !"".equals(this.claveIF)  ){
			ifEspecifica  = " AND FF.ic_if  = ? "; // this.claveIF
			ifEspecificaConditions.add(  this.claveIF  );
		}

		String condEPOs =
			"AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
   		"AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') "+
   		"AND c.ic_producto_nafin  = 1 ";
   				
		String condEPOsErr =
   		"AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
   		"AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 1 "+
   		"AND c.ic_moneda (+) = 1 ";

		String condIFs =
   		"AND crn.ic_nafin_electronico = c.ic_nafin_electronico "+
//   		"AND crn.ic_epo_pyme_if = ff.ic_pyme "+
   		"AND c.ic_producto_nafin  = 1 ";
   				
		String condIFsErr =
   		"AND crn.ic_nafin_electronico(+) = c.ic_nafin_electronico "+
//   		"AND crn.ic_epo_pyme_if = ff.ic_pyme "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 1 "+
   		"AND c.ic_moneda (+) = 1 ";

		String condPEMEXs =
   		"AND crn.ic_epo_pyme_if = ff.ic_epo "+
   		"AND crn.cg_tipo = 'E' "+
   		"AND c.ic_tipo_cuenta (+) = 40 "+
   		"AND c.ic_producto_nafin (+) = 4 "+
   		"AND c.ic_moneda (+) = 1 ";
 	
		StringBuffer cond2 	 				= new StringBuffer(64);
		List			 cond2Conditions 		= new ArrayList();
		StringBuffer cond2Err 				= new StringBuffer(64);
		List			 cond2ErrConditions	= new ArrayList();
			
		if(			this.tipoDispersion.equals("E")	){
				
			cond2.append(" ");
			cond2.append(condEPOs);
			cond2.append(
				" and (FF.df_fecha_venc is not null or FF.df_operacion is not null) and ff.ic_estatus_docto in (1, 4, 9,10, 11) "
			);
				
			cond2Err.append(" ");
			cond2Err.append(condEPOsErr);
			cond2Err.append(
				" and (FF.df_fecha_venc is not null or FF.df_operacion is not null) and ff.ic_estatus_docto in (1, 4, 9,10, 11) "
			);
				
		} else if(	this.tipoDispersion.equals("F")	){
				
			cond2.append(" ");
			cond2.append(condIFs);
			cond2.append(
				" and (FF.ic_if is not null and FF.df_fecha_venc is null) "
			);
				
			cond2Err.append(" ");
			cond2Err.append(condIFsErr);
			cond2Err.append(
				" and (FF.ic_if is not null and FF.df_fecha_venc is null) "
			);
				
		} else if(	this.tipoDispersion.equals("P") && "2".equals(this.estatus ) ){
				
			cond2.append(" ");
			cond2.append(condPEMEXs);
			cond2.append(
				" AND (ff.ic_if IS NULL AND ff.df_fecha_venc IS NOT NULL AND ff.ic_estatus_docto = 2) "
			);
				
		} else if(  this.tipoDispersion.equals("P") && "PA".equals(this.estatus) ){
				
			cond2.append(" ");
			cond2.append(condPEMEXs);
			cond2.append(
				" AND (ff.ic_if IS NULL AND ff.df_operacion IS NOT NULL AND ff.ic_estatus_docto IS NULL) "
			);
				
		}
			
		if( !this.fechaRegistroInicial.equals("") && !this.fechaRegistroFinal.equals("") ) {
				
			cond2.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy')    "  + // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy')+1) "    // this.fechaRegistroFinal
			);
			cond2Conditions.add(this.fechaRegistroInicial);
			cond2Conditions.add(this.fechaRegistroFinal);
				
			cond2Err.append(
				" and FF.df_registro >= TO_DATE(?,'dd/mm/yyyy') "+ // this.fechaRegistroInicial
				" and FF.df_registro < (TO_DATE(?,'dd/mm/yyyy')+1) " // this.fechaRegistroFinal
			);
			cond2ErrConditions.add(this.fechaRegistroInicial);
			cond2ErrConditions.add(this.fechaRegistroFinal);
				
		}
/*
		String cond2 = (sTipoDispersion.equals("E")?condEPOs+" and (FF.df_fecha_venc is not null or FF.df_operacion is not null)":(sTipoDispersion.equals("F")?condIFs+" and (FF.ic_if is not null and FF.df_fecha_venc is null) ":""))+
						((!sFechaRegIni.equals("") && !sFechaRegFin.equals(""))?"and trunc(FF.df_registro) between TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') and TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy') ":"");

*/

		String condvia = "";
		if( !"".equals(this.viaLiquidacion) ) {
				 
			if(        "T".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'TEF'   ";
			} else if( "S".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'SPEUA' ";
			} else if( "I".equals(this.viaLiquidacion)  ) {
				condvia = " and FF.cg_via_liquidacion = 'SPEI'  ";
			}
				
		}
 			
		String condMonitor					= "";
		String condConsultaEpo				= "";
		List   condConsultaEpoConditions = new ArrayList();
			
		if( this.tipo.equals("Monitor") ) {
			condMonitor 		= "    AND ME.status_cen_i IN (-20, -1, 42, 44, 60, 9999) ";
		}
		if ( !"".equals(this.estatusFF)  ) {
			condConsultaEpo 	= "    AND ME.status_cen_i >= ? "; // this.estatusFF
			condConsultaEpoConditions.add(this.estatusFF);
		}

		this.query.append(
			" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff btt)*/"   +
			"         e.cg_razon_social as razon_social_epo,"   +
			"         i.cg_razon_social as razon_social_if,"   +
			"         p.cg_razon_social as razon_social_pyme,"   +
			"         p.cg_rfc,"   +
			"         to_char(c.ic_bancos_tef) as ic_bancos_tef,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			"         to_char(c.ic_tipo_cuenta) as ic_tipo_cuenta,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			"         c.cg_cuenta,"   +
			"         ff.fn_importe,"   +
			"         me.error_cen_s,"   +
			"         ff.ic_flujo_fondos,"   +
			"         me.status_cen_i || ' ' || eff.cd_descripcion as estatus_descripcion,"   +
			"         TO_CHAR (ff.df_registro, 'dd/mm/yyyy') as df_registro,"   +
			"         ff.cg_via_liquidacion,"   +
			"         c.ic_estatus_cecoban,"   +
			"         c.ic_cuenta,"   +
			"         ff.cs_reproceso,"   +
			"         e.ic_epo,"   +
			"         ff.ic_pyme,"   +
			"         ff.ic_if,"   +
			"         ff.ic_estatus_docto,"   +
			" 		    TO_CHAR(ff.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc, "   + // Para que en la paginacion no truene
			" 		    TO_CHAR(ff.df_operacion, 'dd/mm/yyyy') as df_operacion, "   + // Para que en la paginacion no truene
			"         'FFON' origen,"   +
			"         me.status_cen_i, "   +
			"			 btef.cc_clave_dcat_s || ' - ' || btef.cd_descripcion as inst_cuenta_benef, "	+
			"         nvl(iff.cg_razon_social,'N/A') as razon_social_iffon "   +
			"   FROM int_flujo_fondos ff,"   +
			"        cfe_m_encabezado me,"   +
			"        comrel_nafin crn,"   +
			"        comcat_epo e,"   +
			"        comcat_if i,"   +
			"        comcat_if iff,"   +
			"        comcat_pyme p,"   +
			"        com_cuentas c,"   +
			"        comcat_estatus_ff eff,"   +
			"        comcat_bancos_tef btef"   +
			"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
			"    AND ff.ic_epo = e.ic_epo"   +
			"    AND ff.ic_if = i.ic_if(+)"   +
			"    AND ff.ic_if_fondeo = iff.ic_if(+)"   +
			"    AND ff.ic_pyme = p.ic_pyme(+)"   +
			"    AND ff.ic_cuenta = c.ic_cuenta"   +
			"    AND c.ic_bancos_tef = btef.ic_bancos_tef"   +
			"    AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
			//"    AND ff.cs_reproceso IN ('N', 'D')"   +
			"    AND ff.cs_reproceso ='N' "+
			"    AND ff.cs_concluido = 'N'"
		);
		this.query.append(
			cond2 
		);
		this.query.append(
			condvia 
		);
		this.query.append(
			epoEspecifica 
		);
		this.query.append(
			ifEspecifica 
		);
		this.query.append(
			condMonitor 
		);
		this.query.append(
			condConsultaEpo 
		);
		this.query.append(
			" UNION ALL "+
			" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/"   +
			"         e.cg_razon_social as razon_social_epo,"   +
			"         i.cg_razon_social as razon_social_if,"   +
			"         p.cg_razon_social as razon_social_pyme,"   +
			"         p.cg_rfc,"   +
			"         to_char(c.ic_bancos_tef) as ic_bancos_tef,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			"         to_char(c.ic_tipo_cuenta) as ic_tipo_cuenta,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			"         c.cg_cuenta,"   +
			"         ff.fn_importe,"   +
			"         me.error_cen_s,"   +
			"         ff.ic_flujo_fondos,"   +
			"         me.status_cen_i || ' ' || eff.cd_descripcion as estatus_descripcion,"   +
			"         TO_CHAR (ff.df_registro, 'dd/mm/yyyy') as df_registro,"   +
			"         ff.cg_via_liquidacion,"   +
			"         c.ic_estatus_cecoban,"   +
			"         c.ic_cuenta,"   +
			"         ff.cs_reproceso,"   +
			"         e.ic_epo,"   +
			"         ff.ic_pyme,"   +
			"         ff.ic_if,"   +
			"         ff.ic_estatus_docto,"   +
			" 		    TO_CHAR(ff.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc, "   + // Para que en la paginacion no truene
			" 		    TO_CHAR(ff.df_operacion, 'dd/mm/yyyy') as df_operacion, "   + // Para que en la paginacion no truene
			"         'ERR' origen,"   +
			"         me.status_cen_i, "   +
			"			 ' '  as inst_cuenta_benef, "	+
			"         nvl(iff.cg_razon_social,'N/A') as razon_social_iffon "   +
			"   FROM int_flujo_fondos_err ff,"   +
			"        cfe_m_encabezado_err me,"   +
			"        comrel_nafin crn,"   +
			"        comcat_epo e,"   +
			"        comcat_if i,"   +
			"        comcat_if iff,"   +
			"        comcat_pyme p,"   +
			"        com_cuentas c,"   +
			"        comcat_estatus_ff eff"   +
			"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
			"    AND ff.ic_epo = e.ic_epo"   +
			"    AND ff.ic_if = i.ic_if(+)"   +
			"    AND ff.ic_if_fondeo = iff.ic_if(+)"   +
			"    AND ff.ic_pyme = p.ic_pyme(+)"   +
			"    AND ff.ic_cuenta = c.ic_cuenta(+)"   +
			"    AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
			//"    AND ff.cs_reproceso IN ('N', 'D')"   +
			"    AND ff.cs_reproceso ='N' "+
			"    AND ff.cs_concluido = 'N'"
		);
		this.query.append(
			cond2Err 
		);
		this.query.append(
			condvia 
		);
		this.query.append(
			epoEspecifica 
		);
		this.query.append(
			ifEspecifica 
		);
		this.query.append(
			condMonitor 
		);
		this.query.append(
			condConsultaEpo 
		);
			
		// AGREGAR CONDICIONES
			
		this.conditions.addAll(cond2Conditions); 
		// this.conditions.addAll(condviaConditions);
		this.conditions.addAll(epoEspecificaConditions);
		this.conditions.addAll(ifEspecificaConditions);
		// this.conditions.addAll(condMonitorConditions);
		this.conditions.addAll(condConsultaEpoConditions);
		
		this.conditions.addAll(cond2ErrConditions); 
		// this.conditions.addAll(condviaConditions);
		this.conditions.addAll(epoEspecificaConditions);
		this.conditions.addAll(ifEspecificaConditions); 
		// this.conditions.addAll(condMonitorConditions);
		this.conditions.addAll(condConsultaEpoConditions);

		if( this.tipo.equals("Consulta") ) {
				
			this.query.append(
				" UNION ALL"   +
				" SELECT /*+index(ff) use_nl(ff me e i p c eff)*/"   +
				" 		    e.cg_razon_social as razon_social_epo,"   +
			   "         i.cg_razon_social as razon_social_if,"   +
			   "         p.cg_razon_social as razon_social_pyme,"   +
			   "         p.cg_rfc,"   +
				"         to_char(c.ic_bancos_tef) as ic_bancos_tef,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			   "         to_char(c.ic_tipo_cuenta) as ic_tipo_cuenta,"   + // Para que en la paginacion se muestre vacio en vez de un cero
			   "         c.cg_cuenta,"   +
			   "         ff.fn_importe,"   +
			   "         NULL as error_cen_s,"   +
				" 		    ff.ic_flujo_fondos,"   +
			   "         me.status_cen_i || ' ' || eff.cd_descripcion as estatus_descripcion,"   +
				" 		    TO_CHAR (ff.df_registro, 'dd/mm/yyyy') as df_registro,"   +
			   "         ff.cg_via_liquidacion,"   +
				" 		    c.ic_estatus_cecoban,"   +
			   "         c.ic_cuenta,"   +
			   "         ff.cs_reproceso,"   +
			   "         e.ic_epo,"   +
				" 		    ff.ic_pyme,"   +
			   "         ff.ic_if,"   +
			   "         ff.ic_estatus_docto,"   +
			   " 		    TO_CHAR(ff.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc, "   + // Para que en la paginacion no truene
			   " 		    TO_CHAR(ff.df_operacion, 'dd/mm/yyyy') as df_operacion, "   + // Para que en la paginacion no truene
			   "         'FFON' origen,"   +
			   "         me.status_cen_i, "   +
				"			 ' '  as inst_cuenta_benef, "	+
				"         nvl(iff.cg_razon_social,'N/A') as razon_social_iffon "   +
				"   FROM int_flujo_fondos ff,"   +
				" 		 cfe_m_encabezado me,"   +
				" 		 comcat_epo e,"   +
				" 		 comcat_if i,"   +
				" 		 comcat_if iff,"   +
				" 		 comcat_pyme p,"   +
				" 		 com_cuentas c,"   +
				" 		 comcat_estatus_ff eff"   +
				"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
				" 	AND ff.ic_epo = e.ic_epo"   +
				" 	AND ff.ic_if = i.ic_if(+)"   +
				" 	AND ff.ic_if_fondeo = iff.ic_if(+)"   +
				" 	AND ff.ic_pyme = p.ic_pyme(+)"   +
				" 	AND ff.ic_cuenta = c.ic_cuenta(+)"   +
				" 	AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
				" 	AND ff.cs_reproceso IN ('N', 'D')"   +
				" 	AND ff.cs_concluido = 'S'"
			);
			this.query.append(
				cond1 
			);
			this.query.append(
				condvia 
			);
			this.query.append(
				epoEspecifica 
			);
			this.query.append(
				ifEspecifica 
			);
			this.query.append(
				condConsultaEpo 
			);
 
			this.conditions.addAll(cond1Conditions);
			// this.conditions.addAll(condviaConditions); 
			this.conditions.addAll(epoEspecificaConditions);
			this.conditions.addAll(ifEspecificaConditions);
			this.conditions.addAll(condConsultaEpoConditions);
								
		}

		// EQUIVALENCIAS
		
		// 00 razon_social_epo
		// 01 razon_social_if
		// 02 razon_social_pyme
		// 03 cg_rfc
		// 04 ic_bancos_tef
		// 05 ic_tipo_cuenta
		// 06 cg_cuenta
		// 07 fn_importe
		// 08 error_cen_s
		// 09 ic_flujo_fondos
		// 10 estatus_descripcion
		// 11 df_registro
		// 12 cg_via_liquidacion
		// 13 ic_estatus_cecoban
		// 14 ic_cuenta
		// 15 cs_reproceso
		// 16 ic_epo
		// 17 ic_pyme
		// 18 ic_if
		// 19 ic_estatus_docto
		// 20 df_fecha_venc
		// 21 df_operacion
		// 22 origen
		// 23 status_cen_i
		// 24 inst_cuenta_benef
		// 25 razon_social_iffon

		log.debug("getDocumentQueryFile.tipo                 = <" + this.tipo                 + ">");
		log.debug("getDocumentQueryFile.tipoDispersion       = <" + this.tipoDispersion       + ">");
		log.debug("getDocumentQueryFile.fechaRegistroInicial = <" + this.fechaRegistroInicial + ">");
		log.debug("getDocumentQueryFile.fechaRegistroFinal   = <" + this.fechaRegistroFinal   + ">");
		log.debug("getDocumentQueryFile.viaLiquidacion       = <" + this.viaLiquidacion       + ">");
		log.debug("getDocumentQueryFile.claveEPO             = <" + this.claveEPO             + ">");
		log.debug("getDocumentQueryFile.claveIF              = <" + this.claveIF              + ">");
		log.debug("getDocumentQueryFile.estatus              = <" + this.estatus              + ">");
		log.debug("getDocumentQueryFile.estatusFF            = <" + this.estatusFF            + ">");

		log.debug("getDocumentQueryFile.query                = <" + this.query                + ">");
		log.debug("getDocumentQueryFile.conditions           = <" + this.conditions           + ">");
		
		log.info("getDocumentQueryFile(S)");
 
		return this.query.toString();
					
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
	 * con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
	 * de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param directorio Ruta f�sica donde se generar� el archivo
	 * @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String directorio, String tipo) {
		
	 	log.info("crearCustomFile(E)");
		
		FileOutputStream		out 						= null;
		BufferedWriter 		csv 						= null;
		CreaArchivo 			archivo 					= null;
		String					nombreArchivo 			= null;
 			
		try {
		
			// Ocultar/Mostrar columna IF Fondeo dependiendo del tipo de dispersion seleccionada
			boolean 		esTipoDispersionIF	= "F".equals(this.getTipoDispersion())?true:false;
			
			// Crear Archivo
			archivo 					= new CreaArchivo();
			nombreArchivo			= Comunes.cadenaAleatoria(16)+".csv";
			out 						= new FileOutputStream(directorio+nombreArchivo);
			csv 						= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
 			
			// Agregar titulo
			csv.write("EPO,");
			csv.write("IF,");
			csv.write("PYME,");
			csv.write("RFC,");
			if( esTipoDispersionIF ){
				csv.write("Intermediario Financiero Fondeo,");
			}
			csv.write("Banco,");
			csv.write("Tipo de Cuenta,");
			csv.write("No. de Cuenta,");
			csv.write("Importe Total del Descuento,");
			csv.write("Estatus,");
			csv.write("V�a de Liquidaci�n,");
			csv.write("Folio,");
			csv.write("Fecha de Registro");
			csv.write("\n");
		
			boolean hayRegistros = false;
			int 	  ctaRegistros = 0;
			while(rs != null && rs.next()){
				
				ctaRegistros++;
				
				hayRegistros = true;
			
				String razonSocialEpo 	= (rs.getString("razon_social_epo")		== null)?"":rs.getString("razon_social_epo");
				String razonSocialIf 	= (rs.getString("razon_social_if")		== null)?"":rs.getString("razon_social_if");
				String razonSocialPyme 	= (rs.getString("razon_social_pyme")	== null)?"":rs.getString("razon_social_pyme");
				String cgRfc 				= (rs.getString("cg_rfc")					== null)?"":rs.getString("cg_rfc");
				String razonSocialIffon = (rs.getString("razon_social_iffon")	== null)?"":rs.getString("razon_social_iffon");
				String icBancosTef 		= (rs.getString("ic_bancos_tef")			== null)?"":rs.getString("ic_bancos_tef");
				String icTipoCuenta 		= (rs.getString("ic_tipo_cuenta")		== null)?"":rs.getString("ic_tipo_cuenta");
				String cgCuenta 			= (rs.getString("cg_cuenta")				== null)?"":rs.getString("cg_cuenta");
				String fnImporte 			= (rs.getString("fn_importe")				== null)?"0":rs.getString("fn_importe");
				String estatus 			= (rs.getString("estatus_descripcion") == null)?"":rs.getString("estatus_descripcion");
				String cgViaLiquidacion = (rs.getString("cg_via_liquidacion")	== null)?"":rs.getString("cg_via_liquidacion");
				String icFlujoFondos 	= (rs.getString("ic_flujo_fondos")		== null)?"":rs.getString("ic_flujo_fondos");
				String dfRegistro 		= (rs.getString("df_registro")			== null)?"":rs.getString("df_registro");
				
				String causaRechazoFFON	= (rs.getString("error_cen_s")		   == null)?"":rs.getString("error_cen_s");
				String estatusCECOBAN 	= (rs.getString("ic_estatus_cecoban")  == null)?"":rs.getString("ic_estatus_cecoban");
				String claveEstatusFFON = (rs.getString("status_cen_i")		   == null)?"":rs.getString("status_cen_i");		
											
				if( !"99".equals(estatusCECOBAN) && "-1".equals(claveEstatusFFON) ){
					estatus = "Error en la informaci�n de la cuenta";
				} else {
					estatus = estatus+": "+causaRechazoFFON;
				}
				
				fnImporte = Comunes.formatoDecimal(fnImporte,2,false);

				// Formatear Campos
				
				razonSocialEpo 	= razonSocialEpo.replaceAll(","," ");
				razonSocialIf 		= razonSocialIf.replaceAll(","," ");
				razonSocialPyme 	= razonSocialPyme.replaceAll(","," ");
				//cgRfc 				= cgRfc.replaceAll(","," ");
				razonSocialIffon 	= razonSocialIffon.replaceAll(","," ");
				//icBancosTef 		= icBancosTef.replaceAll(","," ");
				//icTipoCuenta 		= icTipoCuenta.replaceAll(","," ");
				//cgCuenta 			= cgCuenta.replaceAll(","," ");
				//fnImporte 			= fnImporte.replaceAll(","," ");
				estatus 				= estatus.replaceAll(","," ");
				//cgViaLiquidacion 	= cgViaLiquidacion.replaceAll(","," ");
				//icFlujoFondos 		= icFlujoFondos.replaceAll(","," ");
				//dfRegistro 			= dfRegistro.replaceAll(","," ");
					
				// CAMPO: EPO
				csv.write(razonSocialEpo);csv.write(",");
				// CAMPO: IF
				csv.write(razonSocialIf);csv.write(",");
				// CAMPO: PYME
				csv.write(razonSocialPyme);csv.write(",");
				// CAMPO: RFC
				csv.write(cgRfc);csv.write(",");
				// CAMPO: Intermediario Financiero Fondeo
				if( esTipoDispersionIF ){
					csv.write(razonSocialIffon);csv.write(",");
				}
				// CAMPO: Banco
				csv.write(icBancosTef);csv.write(",");
				// CAMPO: Tipo de Cuenta
				csv.write(icTipoCuenta);csv.write(",");
				// CAMPO: No. de Cuenta
				csv.write(cgCuenta);csv.write(",");
				// CAMPO: Importe Total del Descuento
				csv.write(fnImporte);csv.write(",");
				// CAMPO: Estatus
				csv.write(estatus);csv.write(",");
				// CAMPO: V�a de Liquidaci�n
				csv.write(cgViaLiquidacion);csv.write(",");
				// CAMPO: Folio
				csv.write(icFlujoFondos);csv.write(",");
				// CAMPO: Fecha de Registro
				csv.write(dfRegistro);csv.write(",");	
					
				csv.write("\n");

				// Guardar en disco cada 100 registros
				if( ctaRegistros == 100 ){ 
					csv.flush(); 
					ctaRegistros = 0;
				}
				
			}
			
			// Agregar mensaje indicando que no hay registros
			if( !hayRegistros ){
				csv.write("No existe alguna operaci�n por el momento Aceptada, En Proceso o Rechazada.\n");	
			}
				
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("crearCustomFile(Exception)"); 
			log.error("crearCustomFile.request    = <" + request    + ">");
			log.error("crearCustomFile.rs         = <" + rs         + ">");
			log.error("crearCustomFile.directorio = <" + directorio + ">");
			log.error("crearCustomFile.tipo       = <" + tipo       + ">");
			e.printStackTrace(); 
			
			throw new AppException(e.getMessage());
			
		} finally {
 
			// Cerrar archivo
			if(csv 	!= null )	try{ csv.close(); }catch(Exception e){};
			
			log.info("crearCustomFile(S)");
			
		}
		
		return nombreArchivo;
		
	}
 
	/** 
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param consulta Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros consulta, String path, String tipo){
		
		log.info("crearPageCustomFile(E)");
		
		String nombreArchivo		= "";
		
		String totalImporte     = (request.getParameter("totalImporte")		== null)?"":request.getParameter("totalImporte");
		String totalOperaciones = (request.getParameter("totalOperaciones")	== null)?"":request.getParameter("totalOperaciones");

		if ("PDF".equals(tipo)) {
			
			try {
				
				// Ocultar/Mostrar columna IF Fondeo dependiendo del tipo de dispersion seleccionada
				boolean 		esTipoDispersionIF	= "F".equals(this.getTipoDispersion())?true:false;
			
				HttpSession session 					= request.getSession();

				nombreArchivo 							= Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF 	pdfDoc 					= new ComunesPDF(2, path + nombreArchivo);
				
				String 		meses[] 			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String 		fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String 		diaActual    	= fechaActual.substring(0,2);
				String 		mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String 		anioActual   	= fechaActual.substring(6,10);
				String 		horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico") == null ? "" : session.getAttribute("iNoNafinElectronico").toString(),
				(String) session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String) session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				pdfDoc.setTable( esTipoDispersionIF?13:12, 100 );
				pdfDoc.setCell("EPO",										"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF",											"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("PYME",										"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC",										"celda01",ComunesPDF.CENTER);
				if( esTipoDispersionIF ){
					pdfDoc.setCell("Intermediario Financiero Fondeo",	"celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setCell("Banco",										"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Cuenta",							"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. de Cuenta",							"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe Total del Descuento",		"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus",									"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("V�a de Liquidaci�n",					"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Folio",										"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Registro",						"celda01",ComunesPDF.CENTER);
									
				boolean hayRegistros = false;
				while(consulta != null && consulta.next()){
					
					hayRegistros = true;
					
					String causaRechazoFFON		= consulta.getString("error_cen_s");
					String estatus 				= consulta.getString("estatus_descripcion");
					String estatusCECOBAN 		= consulta.getString("ic_estatus_cecoban");
					String claveEstatusFFON 	= consulta.getString("status_cen_i");				
											
					if( !"99".equals(estatusCECOBAN) && "-1".equals(claveEstatusFFON) ){
						estatus = "Error en la informaci�n de la cuenta";
					} else {
						estatus = estatus+": "+causaRechazoFFON;
					}
								
					// CAMPO: EPO
					pdfDoc.setCell(consulta.getString("razon_social_epo"),	"formas",ComunesPDF.CENTER);
					// CAMPO: IF
					pdfDoc.setCell(consulta.getString("razon_social_if"),		"formas",ComunesPDF.CENTER);
					// CAMPO: PYME
					pdfDoc.setCell(consulta.getString("razon_social_pyme"),	"formas",ComunesPDF.CENTER);
					// CAMPO: RFC
					pdfDoc.setCell(consulta.getString("cg_rfc"),					"formas",ComunesPDF.CENTER);			
					// CAMPO: Intermediario Financiero Fondeo
					if( esTipoDispersionIF ){
						pdfDoc.setCell(consulta.getString("razon_social_iffon"),	"formas",ComunesPDF.CENTER);
					}
					// CAMPO: Banco
					pdfDoc.setCell(consulta.getString("ic_bancos_tef"),		"formas",ComunesPDF.CENTER);	
					// CAMPO: Tipo de Cuenta
					pdfDoc.setCell(consulta.getString("ic_tipo_cuenta"),		"formas",ComunesPDF.CENTER);
					// CAMPO: No. de Cuenta
					pdfDoc.setCell(consulta.getString("cg_cuenta"),				"formas",ComunesPDF.CENTER);		
					// CAMPO: Importe Total del Descuento
					pdfDoc.setCell(Comunes.formatoMN(consulta.getString("fn_importe")),"formas",ComunesPDF.RIGHT);
					// CAMPO: Estatus
					pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);										
					// CAMPO: V�a de Liquidaci�n
					pdfDoc.setCell(consulta.getString("cg_via_liquidacion"),	"formas",ComunesPDF.CENTER);
					// CAMPO: Folio
					pdfDoc.setCell(consulta.getString("ic_flujo_fondos"),		"formas",ComunesPDF.CENTER);
					// CAMPO: Fecha de Registro
					pdfDoc.setCell(consulta.getString("df_registro"),			"formas",ComunesPDF.CENTER);	
					
					
				}
				
				// Agregar mensaje indicando que no hay registros
				if( !hayRegistros ){
					pdfDoc.setCell("No existe alguna operaci�n por el momento Aceptada, En Proceso o Rechazada.", "celda01", ComunesPDF.CENTER, esTipoDispersionIF?13:12 );
				// En caso contrario mostrar los totales	
				} else {
					
					// CAMPO: EPO
					pdfDoc.setCell("Total de Operaciones",		"celda01", ComunesPDF.CENTER); 
					// CAMPO: IF
					pdfDoc.setCell(totalOperaciones,				"formas",  ComunesPDF.CENTER);
					// CAMPOS
					pdfDoc.setCell("",								"formas",  ComunesPDF.CENTER, esTipoDispersionIF?6:5);		
					// CAMPO: Importe Total del Descuento
					pdfDoc.setCell(totalImporte,					"formas",  ComunesPDF.RIGHT);
					// CAMPOS
					pdfDoc.setCell("",								"formas",  ComunesPDF.CENTER,4);	
					
				}
				
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally {
				log.info("crearPageCustomFile(S)");
			}
			
		}
		
		return nombreArchivo;
		
		
	}
	 
	// METODOS "INVITADOS"
	
	/** 
	 *
	 * Este m�todo se utiliza para generar el comprobante de transferencia. Se usa en la pantalla:
	 *    PANTALLA: ADMIN_NAFIN - ADMINISTRACI�N � FLUJO DE FONDOS � CONSULTAS
	 * @throws Exception
	 *
	 * @param request <tt>HttpRequest</tt> empleado principalmente para obtener el objeto <tt>session</tt>.
	 * @param directorioTemporal <tt>String</tt> con la ruta donde se generar� el archivo.
	 * @param directorioPublicacion <tt>String</tt> con el directorio publicacion como aparece en web.
	 * @param tipo <tt>String</tt> que identifica el tipo o variante de archivo que va a generar.
	 * @param nombreUsuario <tt>String</tt> con el nombre del usuario firmado en el sistema.
	 *
	 * @return Cadena con la ruta del archivo generado
	 *
	 * @since  F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 27/09/2013 01:17:27 p.m.
	 * @author jshernandez
	 *
	 */
	public String generaComprobanteTransferencia( HttpServletRequest request, String directorioTemporal, String directorioPublicacion, String tipo, String nombreUsuario)
		throws Exception {
			
		log.info("generaComprobanteTransferencia(E)");

		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		
		// 1. Obtener instancia de EJB de Dispersion
		Dispersion dispersion = null;
		try {
	
			dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
			
		} catch(Exception e) {
	 
			String msg = "Ocurri� un error al obtener instancia del EJB de Dispersi�n";
			
			log.error("generaComprobanteTransferencia(Exception)");
			log.error(msg);
			e.printStackTrace();
			
			throw new AppException(msg);
			
		} finally {
			log.info("generaComprobanteTransferencia(S)");
		}
	
		try {

			// Obtener registros 
			Hashtable 	consulta = dispersion.getResumOperacErrorFF(
				this.getTipo(), // "Consulta" 
				this.getTipoDispersion(), 
				this.getFechaRegistroInicial(), 
				this.getFechaRegistroFinal(),
				this.getViaLiquidacion(),
				this.getClaveEPO(),
				this.getClaveIF(),
				this.getEstatus()
			); 	
			List 		lista		= ((consulta == null)?new Vector():(Vector)consulta.get("OperError"));
			
			// Crear nombre del archivo
			archivo 			= new CreaArchivo();
			nombreArchivo 	= archivo.nombreArchivo()+".pdf";
			
			// Fuentes
			Font fuenteTitulo 			= FontFactory.getFont("Arial", 12, Font.BOLD);
			Font fuenteSubTitulo 		= FontFactory.getFont("Arial", 10, Font.BOLD);
			Font fuenteSubTituloNormal = FontFactory.getFont("Arial", 10);
			Font fuenteSubTituloBlanco = FontFactory.getFont("Arial", 10, Font.BOLD, BaseColor.WHITE);
			Font fuentePequena 			= FontFactory.getFont("Arial", 5, Font.BOLD);
			Font fuenteEnorme 			= FontFactory.getFont("Arial", 14, Font.BOLD);
			
			// Definir la imagen que se presentara en el logo
			String strLogo = directorioPublicacion +	"/00archivos/15cadenas/15archcadenas/logos/" + "nafinsa_ff.gif";
			Image image = Image.getInstance(strLogo);
			image.setAlignment(Element.ALIGN_LEFT);
			image.scalePercent(82);
			
			// Crear documento PDF
			Document document = new Document(PageSize.A4, 22, 22, 30, 30);
			PdfWriter.getInstance(document,new FileOutputStream(directorioTemporal+nombreArchivo));
			document.open();
			
			Vector 	registro 				= null;
			String	fechaEmision 			= "";
			String 	acuse 					= "";
			String	beneficiario			= "";
			String	movimientos				= "";
			String	operacionRealizada 	= "";
			String	claveRastreo			= "";
			String	tipoCuenta				= "";
			String	numeroDeCuenta			= "";
			String	importe					= "";
			String	institucionCuenta 	= "";
			String   nombreIF          	= "";
			String   nombrePYME        	= "";
			// Fecha de Emision del Reporte
			String 	fecha 					= Fecha.getFechaActual() + " - " + Fecha.getHoraActual("HH:MI AM");
			String 	TipoDispersion			= this.getTipoDispersion();
			
			for(int i=0;i<lista.size();i++){
				registro = (Vector) lista.get(i);
				registro = (registro == null)?new Vector():registro;
					
				fechaEmision 			= fecha;
				acuse 					= dispersion.getNumeroAcusePdfFlujoFondos();
				nombreIF					= registro.get(1)		== null?"":registro.get(1).toString();
				nombrePYME				= registro.get(2)		== null?"":registro.get(2).toString();
				if(TipoDispersion.equals("F")){ //cuando es If se muestran siempre las Pymes como Beneficiarios
					beneficiario			= nombrePYME;					
				}else{
				beneficiario			= nombreIF.trim().equals("")?nombrePYME:nombreIF;
				}
				movimientos				= registro.get(12)	== null?"":registro.get(12).toString();
				operacionRealizada 	= registro.get(11)	== null?"":registro.get(11).toString();
				claveRastreo			= registro.get(9) 	== null?"":"OPE-200-"+registro.get(9).toString();
				tipoCuenta				= "CH - CUENTA DE CHEQ";
				numeroDeCuenta			= registro.get(6)		== null?"":registro.get(6).toString();
				importe					= registro.get(7)		== null?"0.00":Comunes.formatoDecimal(registro.get(7).toString(),2,true);
				institucionCuenta 	= registro.get(24)	== null?"":registro.get(24).toString();
				
				claveRastreo 	= 	formateaClaveRastreo(claveRastreo);
				importe			=	"$ " + importe;
				
				// Agregar pagina nueva
				if(i>0){
					document.add(Chunk.NEWLINE);
					document.newPage();
				}
				
				// A�adir logo 
				PdfPCell celdaLogo =  new PdfPCell(image);
				celdaLogo.setBorder(0);
				celdaLogo.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				// Crear Titulo del Documento
				PdfPCell celdaTitulo =  new PdfPCell(new Paragraph("COMPROBANTE DE TRANSFERENCIA ELECTR�NICA",fuenteTitulo));
				celdaTitulo.setBorder(0);
				celdaTitulo.setHorizontalAlignment(Element.ALIGN_CENTER);
				celdaTitulo.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 
				// 1. CREAR TABLA DEL ENCABEZADO
				float[] colsWidth = {2.8f, 7.2f}; 
				PdfPTable table = new PdfPTable(colsWidth); 
				table.setWidthPercentage(100); 
				// 1.1 Agregar celdas
				table.addCell(celdaLogo);
				table.addCell(celdaTitulo);
				// 1.2 Agregar tabla
				document.add(table);
		
				// 2. AGREGAR LINEA EN BLANCO
				document.add(new Paragraph(" ",fuentePequena));
					
				// 3. AGREGAR LINEA INDICANDO LA FECHA DE EMISION
				document.add(new Paragraph("Fecha de Emisi�n",fuenteSubTitulo));
		 
			  // 4. AGREGAR TABLA DEL BENEFICIARIO
			   float 		padding 	= fuenteSubTitulo.getSize();
				colsWidth 				= new float[]{2.5f, 7.2f}; 
				PdfPTable 	t 			= new PdfPTable(2); 
				t.setWidthPercentage(100f);
				t.setSpacingBefore(0f);
				t.setWidths(colsWidth);
				// 4.1 Agregar Fecha de Emision
				PdfPCell cell =  new PdfPCell(new Phrase(fechaEmision,fuenteSubTitulo)); // "FECHA_DE_EMISION"
				 cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				 cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				 cell.setBorderWidth(0.5f);
				 cell.setPadding(padding);
				 cell.setBorder(Rectangle.LEFT | Rectangle.TOP | Rectangle.RIGHT );
				 t.addCell(cell);
				// 4.2 Agregar Celda con el titulo del beneficiario
				cell =  new PdfPCell(new Phrase("BENEFICIARIO",fuenteSubTituloBlanco));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setBorderWidth(0.5f);
				cell.setPadding(padding);
				cell.setBackgroundColor(BaseColor.GRAY);
				t.addCell(cell);
				// 4.3 Agregar Fecha de Emision
				cell =  new PdfPCell(new Phrase(acuse,fuenteSubTitulo)); // ACUSE 
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setBorderWidth(0.5f);
				cell.setPadding(padding);
				cell.setBorder(Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT );
				t.addCell(cell);
				// 4.4 Agregar Celda con el titulo del beneficiario
				cell =  new PdfPCell(new Phrase(beneficiario,fuenteSubTitulo));// BENEFICIARIO
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setBorderWidth(0.5f);
				cell.setPadding(padding);
				t.addCell(cell);
				document.add(t);
				
				// 5. AGREGAR LINEA EN BLANCO
				document.add(new Paragraph(" ",fuentePequena));
				
				// 6. AGREGAR RENGLON DE MOVIMIENTOS
				document.add(new Paragraph("Movimientos: "+movimientos,fuenteEnorme)); // MOVIMIENTOS
				// 7. AGREGAR RENGLON CON FECHA DE LA OPERACION REALIZADA
				document.add(new Paragraph("Operaci�n realizada: "+operacionRealizada,fuenteSubTitulo)); // OPERACION_REALIZADA
				
				// 8. AGREGAR TABLA DE MOVIMIENTOS
				float colsWidth4[] = new float[]{1f,1f,1f,1f}; 
				table = new PdfPTable(colsWidth4); 
				table.setWidthPercentage(100); 
				// 8.1 Agregar Titulo: Clave de Rastreo
				PdfPCell celda =  new PdfPCell(new Paragraph("Clave de Rastreo",fuenteSubTituloNormal));
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				celda.setFixedHeight(26f) ;
				table.addCell(celda);
				// 8.2 Agregar Titulo:  Tipo de Cuenta
				celda =  new PdfPCell(new Paragraph("Tipo de Cuenta",fuenteSubTituloNormal));
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
				// 8.3 Agregar Titulo:  N�mero de Cuenta
				celda =  new PdfPCell(new Paragraph("N�mero de Cuenta",fuenteSubTituloNormal));
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
				// 8.4 Agregar Titulo:  Importe
				celda =  new PdfPCell(new Paragraph("Importe",fuenteSubTituloNormal));
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
				
				// 8.5 Agregar Contenido: Clave de Rastreo
				celda =  new PdfPCell(new Paragraph(claveRastreo,fuenteSubTitulo)); // CLAVE DE RASTREO
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				celda.setFixedHeight(26f);
				table.addCell(celda);
				// 8.6 Agregar Contenido:  Tipo de Cuenta
				celda =  new PdfPCell(new Paragraph(tipoCuenta,fuenteSubTitulo)); // TIPO DE CUENTA
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
				// 8.7 Agregar Contenido:  N�mero de Cuenta
				celda =  new PdfPCell(new Paragraph(numeroDeCuenta,fuenteSubTitulo)); // NUMERO DE CUENTA
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
				// 8.8 Agregar Contenido:  Importe
				celda =  new PdfPCell(new Paragraph(importe,fuenteSubTitulo)); // IMPORTE
				celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
		 
				// 8.8 Agregar Titulo:  Instituci�n de la Cuenta del Beneficiario
				celda =  new PdfPCell(new Paragraph("Instituci�n de la Cuenta del Beneficiario",fuenteSubTituloNormal));
				celda.setHorizontalAlignment(Element.ALIGN_LEFT);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				celda.setIndent(12f);
				celda.setFixedHeight(26f);
				celda.setColspan(4);
				table.addCell(celda);
				// 8.9 Agregar Contenido:  Instituci�n de la Cuenta del Beneficiario
				celda =  new PdfPCell(new Paragraph(institucionCuenta,fuenteSubTitulo)); // INSTITUCION CUENTA
				celda.setHorizontalAlignment(Element.ALIGN_LEFT);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				celda.setIndent(5f);
				celda.setFixedHeight(26f);
				celda.setColspan(4);
				table.addCell(celda);
				
				document.add(table);
				
				document.add(new Paragraph("Gener�: "+nombreUsuario,fuenteTitulo));
			}
			
			// Fin de Documento
			if(lista == null || lista.size() == 0){
				document.add(new Paragraph(" No se encontraron registros",fuenteTitulo));
			}
			document.close();
			
		} catch(Exception e) {
			
			log.error("generaComprobanteTransferencia(Exception)");
			log.error("generaComprobanteTransferencia.request               = <" + request               + ">");
			log.error("generaComprobanteTransferencia.directorioTemporal    = <" + directorioTemporal    + ">");
			log.error("generaComprobanteTransferencia.directorioPublicacion = <" + directorioPublicacion + ">");
			log.error("generaComprobanteTransferencia.tipo                  = <" + tipo                  + ">");
			log.error("generaComprobanteTransferencia.nombreUsuario         = <" + nombreUsuario         + ">");
			e.printStackTrace();

			throw e;
			
		} finally {
			
			log.info("generaComprobanteTransferencia(S)");
			
		}
		
		return nombreArchivo;
		
	}
	
	/**
	 * Formatea la clave de rastreo agreg�ndole guiones.
	 *
	 * @param claveRastreo String con la clave de rastreo original.
	 *
	 * @return String con la clave de rastreo formateada.
	 *
	 * @since  F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 27/09/2013 01:20:23 p.m.
	 * @author jshernandez
	 *
	 */
	private String formateaClaveRastreo(String claveRastreo){
		
		StringBuffer buffer = new StringBuffer();
		
		for(int i=0;i<claveRastreo.length();i++){
			buffer.append(claveRastreo.charAt(i));
			if(i == 11 || i == 13 || i == 15) buffer.append("-");
		}
		
		return buffer.toString();
		
	}
	
	/**
	 *	Crea un archivo CSV con el detalle de los individual de los documentos. Se usa en la pantalla
	 *   PANTALLA: ADMIN_NAFIN - ADMINISTRACI�N � FLUJO DE FONDOS � CONSULTAS  ( Ver Detalle, Generar Archivo )
	 *	@throws Exception   
	 *	
	 *	@param directorio <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 *	@param parametros <tt>HashMap</tt> con los parametros (tipo <tt>String</tt>) de la consulta: icFlujoFondos, 
	 *                   claveEpo, clavePyme, claveIf, estatusDocumento, claveTipoDispersion, fechaVencimiento, 
	 *                   fechaOperacion, claveEstatus.
	 *
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 *
	 * @since  F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 27/09/2013 08:15:50 p.m.
	 * @author jshernandez
	 *
	 */	
	public String generaArchivoCSVDetalleDocumentos( String directorio, HashMap parametros )
		throws Exception {
			
		log.info("generaArchivoCSVDetalleDocumentos(E)");
		
		FileOutputStream		out 						= null;
		BufferedWriter 		csv 						= null;
		CreaArchivo 			archivo 					= null;
		String					nombreArchivo 			= null;
 
		String 					icFlujoFondos 			= null;
		String 					claveEpo 				= null;
		String 					clavePyme 				= null;
		String 					claveIf 					= null;
		String 					estatusDocumento 		= null;
		String 					claveTipoDispersion 	= null;
		String 					fechaVencimiento 		= null;
		String 					fechaOperacion 		= null;
		String 					claveEstatus 			= null;

		// 1. Obtener instancia de EJB de Dispersion
		Dispersion dispersion = null;
		try {
		
			dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
				
		}catch(Exception e){
		 
			String msg = "Ocurri� un error al obtener instancia del EJB de Dispersi�n";
			log.error(msg);
			e.printStackTrace();
				
			throw new AppException(msg);
				
		}finally{
			
			log.info("generaArchivoCSVDetalleDocumentos(S)");
			
		}
			
		try {
		
			// Leer Parametros
			icFlujoFondos 			= (String) parametros.get("icFlujoFondos");
			claveEpo 				= (String) parametros.get("claveEpo");
			clavePyme 				= (String) parametros.get("clavePyme");
			claveIf 					= (String) parametros.get("claveIf");
			estatusDocumento 		= (String) parametros.get("estatusDocumento");
			claveTipoDispersion 	= (String) parametros.get("claveTipoDispersion");
			fechaVencimiento 		= (String) parametros.get("fechaVencimiento");
			fechaOperacion 		= (String) parametros.get("fechaOperacion");
			claveEstatus 			= (String) parametros.get("claveEstatus");
 
			// Crear Archivo
			archivo 					= new CreaArchivo();
			nombreArchivo			= archivo.nombreArchivo()+".csv";
			out 						= new FileOutputStream(directorio+nombreArchivo);
			csv 						= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
 
			// Realizar consulta
			Vector datos = dispersion.getDetalleDoctos( icFlujoFondos, claveTipoDispersion, claveEstatus, claveEpo, clavePyme, claveIf, estatusDocumento, fechaVencimiento, fechaOperacion );
				
			// Variables Auxiliares
			int 			docsMN 			= 0;
			BigDecimal 	mtoMN 			= new BigDecimal("0");
			BigDecimal 	impMN 			= new BigDecimal("0");
			int 			docsUSD 			= 0;
			BigDecimal 	mtoUSD 			= new BigDecimal("0");
			BigDecimal 	impUSD 			= new BigDecimal("0");
			boolean		bNotaCred 		= false;
			
			// Enviar detalle de la consulta
			JSONArray 	registros 	= new JSONArray();
			JSONObject 	registro  	= null;
			
			if(        "PA".equals(claveEstatus)        ){
				csv.write("Num. de Documento, PyME, Importe Documento");
			} else if( !"P".equals(claveTipoDispersion) ){
				csv.write("Num. de Documento, PyME, Importe Documento,Importe Descuento");
			} else {
				csv.write("Num. de Documento, PyME, Importe Documento");
			}
			csv.write("\n");
		
			for(int i = 0,ctaRegistros = 0;i<datos.size();i++,ctaRegistros++) {
					
				Vector renglon 				= (Vector)datos.get(i);
				
				String rs_documento 			= renglon.get(1).toString();
				String rs_pyme 				= renglon.get(2).toString();
				String rs_monto 				= renglon.get(3).toString();
				String rs_importe 			= renglon.get(4).toString();
				String rs_moneda 				= renglon.get(5).toString();
				String rs_dscto_especial	= renglon.get(6).toString();
				String auxNota 				= "";
				
				if("C".equals(rs_dscto_especial)) {
					if(!bNotaCred)
						bNotaCred = true; 
					auxNota 		= "*";
					rs_monto 	= "-"+rs_monto;
					rs_importe 	= "-"+rs_importe;
				}
				if("1".equals(rs_moneda)) {
					if(!"C".equals(rs_dscto_especial))
						docsMN++;
					mtoMN = mtoMN.add( new BigDecimal(rs_monto));
					impMN = impMN.add( new BigDecimal(rs_importe));
				} else if("54".equals(rs_moneda)) {
					if(!"C".equals(rs_dscto_especial))
						docsUSD++;
					mtoUSD = mtoUSD.add( new BigDecimal(rs_monto));
					impUSD = impUSD.add( new BigDecimal(rs_importe));
				}
							
				if(        "PA".equals(claveEstatus)      ){
					csv.write(auxNota+rs_documento+","+rs_pyme+","+rs_monto);
				} else if( !"P".equals(claveTipoDispersion) ){
					csv.write(auxNota+rs_documento+","+rs_pyme+","+rs_monto+","+rs_importe);
				} else {
					csv.write(auxNota+rs_documento+","+rs_pyme+","+rs_monto);
				}
				csv.write("\n");

				// 7. Guardar en disco cada 100 registros
				if( ctaRegistros == 100 ){ 
					csv.flush(); 
					ctaRegistros = 0;
				}
				
			}

			if(bNotaCred) {
				csv.write("\n(*) Nota de Cr�dito");
			}
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoCSVDetalleDocumentos(Exception)");
			log.error("generaArchivoCSVDetalleDocumentos.directorio  = <" + directorio     + ">");
			log.error("generaArchivoCSVDetalleDocumentos.parametros  = <" + parametros     + ">");
			e.printStackTrace(); 
			
			throw e;
			
		} finally {
 
			// Cerrar archivo
			if(csv 	!= null )	try{csv.close();}catch(Exception e){};
			
			log.info("generaArchivoCSVDetalleDocumentos(S)");
			
		}
		
		return nombreArchivo;
		
	}

	// VARIABLES PROPIAS DE LA CLASE
	
	private String 		tipo 						= ""; // sTipo
	private String 		tipoDispersion 		= ""; // sTipoDispersion
	private String 		fechaRegistroInicial = ""; // sFechaRegIni
	private String 		fechaRegistroFinal 	= ""; // sFechaRegFin
	private String 		viaLiquidacion 		= ""; // sViaLiq
	private String 		claveEPO 				= ""; // ic_epo
	private String 		claveIF 					= ""; // ic_if
	private String 		estatus 					= ""; // sEstatus
	private String 		estatusFF 				= ""; // sEstatusFF
	
	public String getTipo() {
		return tipo;
	}
 
	public void setTipo(String tipo) {
		this.tipo = tipo == null || tipo.matches("\\s*")?"":tipo;
	}

	public String getTipoDispersion() {
		return tipoDispersion;
	}
 
	public void setTipoDispersion(String tipoDispersion) {
		this.tipoDispersion = tipoDispersion == null || tipoDispersion.matches("\\s*")?"":tipoDispersion;
	}

	public String getFechaRegistroInicial() {
		return fechaRegistroInicial;
	}

	public void setFechaRegistroInicial(String fechaRegistroInicial) {
		this.fechaRegistroInicial = fechaRegistroInicial == null || fechaRegistroInicial.matches("\\s*")?"":fechaRegistroInicial;
	}
 
	public String getFechaRegistroFinal() {
		return fechaRegistroFinal;
	}
 
	public void setFechaRegistroFinal(String fechaRegistroFinal) {
		this.fechaRegistroFinal = fechaRegistroFinal == null || fechaRegistroFinal.matches("\\s*")?"":fechaRegistroFinal;
	}
 
	public String getViaLiquidacion() {
		return viaLiquidacion;
	}
 
	public void setViaLiquidacion(String viaLiquidacion) {
		this.viaLiquidacion = viaLiquidacion == null || viaLiquidacion.matches("\\s*")?"":viaLiquidacion;
	}
 
	public String getClaveEPO() {
		return claveEPO;
	}

	public void setClaveEPO(String claveEPO) {
		this.claveEPO = claveEPO == null || claveEPO.matches("\\s*")?"":claveEPO;
	}
 
	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF == null || claveIF.matches("\\s*")?"":claveIF;
	}
 
	public String getEstatus() {
		return estatus;
	}
 
	public void setEstatus(String estatus) {
		this.estatus = estatus == null || estatus.matches("\\s*")?"":estatus;
	}

	public String getEstatusFF() {
		return estatusFF;
	}

	public void setEstatusFF(String estatusFF) {
		this.estatusFF = estatusFF == null || estatusFF.matches("\\s*")?"":estatusFF;
	}
	
}
