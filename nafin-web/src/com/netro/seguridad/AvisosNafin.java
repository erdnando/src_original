package com.netro.seguridad;

import java.math.BigDecimal;
import netropology.utilerias.*;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import java.util.*;     
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.netro.exception.*;
import java.io.*;
import com.netro.xlsx.*;   
   
public class AvisosNafin implements IQueryGeneratorRegExtJS {


        //Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(AvisosNafin.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List    conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String tipoConsulta;
	private String no_operacion;
	private String nombreProducto;
 	private String directorioPlantilla;
	private String if_fisos;
	private String ic_bitacora;
	private String actualizarCorte;
	private String id_producto;
	

	public AvisosNafin() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 	= new ArrayList();
				
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 	= new ArrayList();
	
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 	= new ArrayList();

		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 	= new ArrayList();  

		if(tipoConsulta.equals("ConsultarServicios")   ) { 
			
			qrySentencia.append("SELECT ic_nombre as PRODUCTO ,"+
				" cs_cierre_servicio,  cs_cierre_servicio_if, cs_cierre_servicio_nafin , "+
				" 'Servicio Abierto' as  SERVICIO "+
				"  FROM comcat_producto_nafin  "+
				" WHERE ic_producto_nafin IN (1,  4) "+
				"	AND TO_DATE(to_char(SYSDATE, 'hh24:mi'), 'hh24:mi') < TO_DATE(cg_horario_fin, 'hh24:mi')  ");

		}else if(tipoConsulta.equals("DescuentoElectronico") ) {
			
			qrySentencia.append("SELECT docum.ic_moneda  as  IC_MONEDA,  m.CD_NOMBRE as MONEDA , "+
				"	 catif.cg_razon_social         as  INTERMEDIARIO,  "+
				"	 COUNT(solic.ic_folio)          as  OPERACIONES,  "+
				"	 SUM(docsel.in_importe_recibir) as  MONTO  "+
				" FROM comcat_if               catif,  "+
				"	  com_documento           docum,  "+
				"	  com_docto_seleccionado docsel,  "+
				"	  com_solicitud           solic,   "+
				"    comcat_moneda m   "+
				" WHERE docum.ic_documento = docsel.ic_documento  "+
				" AND   solic.ic_documento = docum.ic_documento  "+ 
				" and   docsel.ic_if = catif.ic_if  "+
				" and docum.ic_moneda = m.ic_moneda  "+				
				
				" AND (   (    solic.df_fecha_solicitud >= TRUNC (SYSDATE)  "+
				" AND solic.df_fecha_solicitud < TRUNC (SYSDATE + 1) "+
				" AND solic.ic_estatus_solic = 3     )  "+
			
				" OR (Solic.df_fecha_solicitud >= Trunc(Sysdate)   "+
				" And Solic.df_fecha_solicitud < Trunc(Sysdate+1)  "+
				" And Solic.df_operacion >= Trunc(Sysdate)  "+
				" And Solic.df_operacion < Trunc(Sysdate+1) "+
				" And docum.Ic_Moneda = 1  "+
				" AND solic.ic_estatus_solic = 3 )  "+
				
				"  Or  (Solic.df_fecha_solicitud >= Trunc(Sysdate)  "+
				" And Solic.df_fecha_solicitud < Trunc(Sysdate+1)  "+ 
				" And Solic.df_operacion >= trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1))  "+
				" And Solic.df_operacion < trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1)+1) "+ 
				" And docum.Ic_Moneda = 54 "+
				" AND solic.ic_estatus_solic = 3 )  "+
				
				" OR  (Solic.df_fecha_solicitud < Trunc(Sysdate)  "+
				" And Solic.df_operacion >= Trunc(Sysdate)  "+
				" And Solic.df_operacion < Trunc(Sysdate+1) "+
				" And docum.Ic_Moneda = 1 "+
				" AND solic.ic_estatus_solic = 3  ) "+
				
				" Or  ( Solic.df_fecha_solicitud < Trunc(Sysdate)  "+
				" And Solic.Df_Operacion >= trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1))  "+
				" And Solic.Df_Operacion < trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1)+1) "+
				" And docum.Ic_Moneda = 54  " +
				"  AND solic.ic_estatus_solic = 3  ) ) "+
				" GROUP BY docum.ic_moneda, m.CD_NOMBRE ,  catif.cg_razon_social "+
				" order by  docum.ic_moneda asc ");
				
					
		}else if(tipoConsulta.equals("Distribuidores") ) {
		
		   qrySentencia.append(" SELECT docum.ic_moneda  as  IC_MONEDA,  m.CD_NOMBRE as MONEDA ,  "+
		            " catif.cg_razon_social   as        INTERMEDIARIO,   "+
		            " COUNT(solic.ic_documento)     as      Operaciones,   "+
		            " SUM(docsel.FN_IMPORTE_RECIBIR) as MONTO   "+
		            " FROM comcat_if               catif,   "+
		            " dis_documento           docum,   "+
		            " dis_docto_seleccionado docsel,   "+
		            " dis_solicitud           solic,    "+
		            "  comcat_moneda m    "+
		            " WHERE docum.ic_documento = docsel.ic_documento   "+
		            " AND   solic.ic_documento = docum.ic_documento   "+
		            " and   solic.ic_if = catif.ic_if   "+
		            " and docum.ic_moneda = m.ic_moneda   "+
		            " AND (  (    solic.df_fecha_solicitud >= TRUNC (SYSDATE)   "+
		            " AND solic.df_fecha_solicitud < TRUNC (SYSDATE + 1)  "+
		            " AND solic.ic_estatus_solic = 3     )   "+
		            
		            " OR (Solic.df_fecha_solicitud >= Trunc(Sysdate)    "+
		            " And Solic.df_fecha_solicitud < Trunc(Sysdate+1)  "+
		            " And Solic.df_operacion >= Trunc(Sysdate)  "+
		            " And Solic.df_operacion < Trunc(Sysdate+1) "+
		            " And docum.Ic_Moneda = 1   "+
		            " AND solic.ic_estatus_solic = 3  )  "+
		            "  Or  (Solic.df_fecha_solicitud >= Trunc(Sysdate)  "+
		            " And Solic.df_fecha_solicitud < Trunc(Sysdate+1)  "+
		            " And Solic.df_operacion >= trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1))  "+
		            " And Solic.df_operacion < trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1)+1) "+
		            " And docum.Ic_Moneda = 54 "+
		            " AND solic.ic_estatus_solic = 3  )  "+
		            
		            " OR  (Solic.df_fecha_solicitud < Trunc(Sysdate)  "+
		            " And Solic.df_operacion >= Trunc(Sysdate)  "+
		            " And Solic.df_operacion < Trunc(Sysdate+1) "+
		            " And docum.Ic_Moneda = 1   "+
		            " AND solic.ic_estatus_solic = 3  ) "+
		            
		            " Or  ( Solic.df_fecha_solicitud < Trunc(Sysdate)  "+
		            " And Solic.Df_Operacion >= trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1))  "+
		            " And Solic.Df_Operacion < trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1)+1) "+
		            " And docum.Ic_Moneda = 54   "+
		            " AND solic.ic_estatus_solic = 3  ) ) "+
		            
		            " GROUP BY  docum.ic_moneda,  m.CD_NOMBRE ,  catif.cg_razon_social "+
		            " order by  docum.ic_moneda asc "); 
				
		}else if(tipoConsulta.equals("CreditoElectronico")  ) {
		
			qrySentencia.append("	SELECT solic.ic_moneda AS IC_MONEDA , m.CD_NOMBRE as MONEDA ,   "+
				" catif.cg_razon_social        as   INTERMEDIARIO,  "+
				" COUNT(solic.ic_solic_portal)    as       OPERACIONES,  "+
				" SUM(solic.FN_IMPORTE_DSCTO) as MONTO  "+
			
				" FROM comcat_if               catif,  "+
				" com_solic_portal           solic,   "+
				" comcat_moneda m   "+
				" WHERE solic.ic_if = catif.ic_if  "+
			" AND  (  (SOLIC.DF_CARGA >= trunc(sysdate)  "+
				" AND   SOLIC.DF_CARGA < trunc(sysdate+1)  "+
			" AND SOLIC.DF_OPERACION >= trunc(sysdate) "+
			" AND SOLIC.DF_OPERACION < trunc(sysdate+1)"+
			" AND solic.ic_moneda = 1  " +
			" AND solic.ic_estatus_solic = 3   ) "+
			
			" or  (SOLIC.DF_CARGA >= trunc(sysdate)  "+
			" AND SOLIC.DF_CARGA < trunc(sysdate+1) "+
			" AND SOLIC.DF_OPERACION >= trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1)) "+
			" AND SOLIC.DF_OPERACION < trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1)+1) "+  
			" AND solic.ic_moneda = 54  "+
			" AND solic.ic_estatus_solic = 3  )  "+
			
			" OR ( SOLIC.DF_CARGA < trunc(sysdate) "+
			" AND SOLIC.DF_OPERACION >= trunc(sysdate) "+
			" AND SOLIC.DF_OPERACION < trunc(sysdate+1)"+ 
			" AND solic.ic_moneda = 1  "+
			" AND solic.ic_estatus_solic = 3 ) "+
			
			" OR ( SOLIC.DF_CARGA < trunc(sysdate) "+
			" AND SOLIC.DF_OPERACION >= trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1)) "+ 
			" AND SOLIC.DF_OPERACION < trunc(SIGDIAHABIL2(trunc(sysdate), 1, 1)+1) "+
			" AND solic.ic_moneda = 54  " +
			" AND solic.ic_estatus_solic = 3  )   )"+
			" and solic.ic_moneda = m.ic_moneda  "+					 
			" GROUP BY solic.ic_moneda, m.CD_NOMBRE , catif.cg_razon_social  "+
			" order by   solic.ic_moneda asc   "); 
		

		}else if(tipoConsulta.equals("ConsCortes") ) {
		
			qrySentencia.append( queryGenerarCorte(if_fisos ) );  
				
					
		}else if(tipoConsulta.equals("DatosCorreo")) { 
		
			qrySentencia.append("  SELECT  IC_OPERACIONES,  CG_DESTINATARIO, CG_CCDESTINATARIO , CG_ASUNTO, CG_CUERPO_CORREO   "+
				" FROM COM_PLANTILLA_CORREO  "+
				" WHERE IC_OPERACIONES =  ? ");			
				conditions.add(no_operacion);		 
		
		}

		log.info("tipoConsulta  "+tipoConsulta); 
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		return "";
		
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
	
		String nombreArchivo =  "";
		ComunesXLSX				documentoXLSX 	= null;
		Writer					writer			= null;
		
		// Estilos en la plantilla xlsx
		HashMap 					estilos			= new HashMap();
		estilos.put("PERCENT",			"1");
		estilos.put("COEFF",				"2");
		estilos.put("CURRENCY",			"3");
		estilos.put("DATE",				"4");
		estilos.put("HEADER",			"5");
		estilos.put("CSTRING",			"6");
		estilos.put("TITLE",				"7");
		estilos.put("SUBTITLE",			"8");
		estilos.put("RIGHTCSTRING",	"9");
		estilos.put("CENTERCSTRING",	"10");
		estilos.put("REDCURRENCY",					"11");
		estilos.put("CENTERCSTRING_ESMERALDA",	"12");
		estilos.put("BOLD_CSTRING",				"13");
		estilos.put("BOLD_CENTERCSTRING",		"14");
		estilos.put("BOLD_CURRENCY",				"15");
						
		try {
			
			// Crear archivo XLSX
			documentoXLSX 	= new ComunesXLSX( path, estilos );
			
			// Crear archivo
			writer 			= documentoXLSX.creaHoja();
			documentoXLSX.setLongitudesColumna(new String[]{"63.42578125","25.7109375","35.140625","23.7109375","15.7109375","23.7109375","23.7109375","23.7109375","22.7109375","23.7109375"});
		
			short	indiceRenglon 	= 0;
 			short	indiceCelda		= 0;
 			
			log.info("no_operacion" +no_operacion);
			
			//Descuento Electr�nico", Financiamiento a Distribuidores, Cr�dito Electr�nico	
			if(no_operacion.equals("1") ||  no_operacion.equals("2")  ||  no_operacion.equals("3")   ) { 
			
				// Definir el Encabezado PROGRAMA
				documentoXLSX.agregaRenglon(indiceRenglon++); 
				indiceCelda		= 0;
				documentoXLSX.agregaCelda(indiceCelda++, (String) "Intermediario","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String) "Operaciones","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String) "Monto","HEADER" );			
				documentoXLSX.finalizaRenglon();
			
				BigDecimal totalMN = new BigDecimal("0.0");
				BigDecimal totalDL = new BigDecimal("0.0");
				int totalRegMN =0,  totalRegDL =0;			
				int mMN = 0 , mDL =0, reg =0;
				
				String ic_monedaAnterior ="1", intermediario = "", ic_moneda ="",  moneda ="", operaciones = "",  monto = "";
				
				while (rs.next())	{	
					reg ++;				
					intermediario = (rs.getString("INTERMEDIARIO") == null) ? "" : rs.getString("INTERMEDIARIO");	
					ic_moneda = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");
					moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");		
					operaciones = (rs.getString("OPERACIONES") == null) ? "0" : rs.getString("OPERACIONES");
					monto = (rs.getString("MONTO") == null) ? "0" : rs.getString("MONTO");
					
					if(ic_moneda.equals("1"))  {
						totalMN = totalMN.add(new BigDecimal((String)monto) );
						totalRegMN +=  Integer.parseInt(operaciones);	
						mMN++;
					}else  if(ic_moneda.equals("54"))  {
						totalDL = totalDL.add(new BigDecimal((String)monto) );
						totalRegDL +=  Integer.parseInt(operaciones);
						mDL++;
					}	
					
					// Descripcion de la Moneda		
					if(reg==1  )  {						
						documentoXLSX.agregaRenglon(indiceRenglon++); 
						indiceCelda					= 0;
						documentoXLSX.agregaCelda(indiceCelda++,moneda,"CENTERCSTRING_ESMERALDA" );				
						documentoXLSX.finalizaRenglon();
					}
									
					///Total de  Moneda Nacional 
					if(!ic_moneda.equals(ic_monedaAnterior) &&  mMN>0  )  {	
						documentoXLSX.agregaRenglon(indiceRenglon++); 
						indiceCelda					= 0;
						documentoXLSX.agregaCelda(indiceCelda++,"Total",	"BOLD_CSTRING");
						documentoXLSX.agregaCelda(indiceCelda++,String.valueOf(totalRegMN) ,	"BOLD_CENTERCSTRING");				
						documentoXLSX.agregaCelda(indiceCelda++,"$"+Comunes.formatoDecimal(totalMN,2),	"BOLD_CURRENCY");
						
						
						documentoXLSX.finalizaRenglon();	
						
						// Agregar renglon en blanco
						documentoXLSX.agregaRenglon(indiceRenglon++); 
						indiceCelda					= 0;
						documentoXLSX.agregaCelda(indiceCelda++,"","CSTRING" ); 				
						documentoXLSX.finalizaRenglon();
						
						// Descripcion de la  Moneda Dolar
						documentoXLSX.agregaRenglon(indiceRenglon++); 
						indiceCelda					= 0;
						documentoXLSX.agregaCelda(indiceCelda++,moneda,"CENTERCSTRING_ESMERALDA" ); 				
						documentoXLSX.finalizaRenglon();
						mMN=0; // se inicializa en cero  
						
					}
					
					// Datos de la consulta 
					documentoXLSX.agregaRenglon(indiceRenglon++); 
					indiceCelda					= 0;
					documentoXLSX.agregaCelda(indiceCelda++,intermediario,"CSTRING" );
					documentoXLSX.agregaCelda(indiceCelda++,operaciones,	"CENTERCSTRING" );				
					documentoXLSX.agregaCelda(indiceCelda++,"$"+Comunes.formatoDecimal(monto,2),	"CURRENCY"); 
					
					documentoXLSX.finalizaRenglon();
				
					ic_monedaAnterior = ic_moneda;
					
				}	//while 
				
				if( mDL>0  )  {		
					// Moneda Dolar 
					documentoXLSX.agregaRenglon(indiceRenglon++); 
					indiceCelda					= 0;
					documentoXLSX.agregaCelda(indiceCelda++,"Total",	"BOLD_CSTRING"); 
					documentoXLSX.agregaCelda(indiceCelda++,String.valueOf(totalRegDL) ,	"BOLD_CENTERCSTRING"); 				
					documentoXLSX.agregaCelda(indiceCelda++,"$"+Comunes.formatoDecimal(totalDL.toString(),2),	"BOLD_CURRENCY"); 
					documentoXLSX.finalizaRenglon();
				}
				
				if( mMN>0 && mDL==0  )  {				
				 //  Moneda Nacional 
					documentoXLSX.agregaRenglon(indiceRenglon++); 
					indiceCelda	= 0;
					documentoXLSX.agregaCelda(indiceCelda++,"Total",	"BOLD_CSTRING");
					documentoXLSX.agregaCelda(indiceCelda++,String.valueOf(totalRegMN) ,	"BOLD_CENTERCSTRING");					
					documentoXLSX.agregaCelda(indiceCelda++,"$"+Comunes.formatoDecimal(totalMN.toString(),2),	"BOLD_CURRENCY");
					documentoXLSX.finalizaRenglon();
				
				}		
			
			}else  {
			
				// Definir el Encabezado PROGRAMA
				documentoXLSX.agregaRenglon(indiceRenglon++); 
				indiceCelda		= 0;
				documentoXLSX.agregaCelda(indiceCelda++, (String) "FISOS","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String) "Cantidad","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String) "Monto","HEADER" );			
				documentoXLSX.finalizaRenglon();
				
				String nombreFisos = "", cantidad = "",  monto = "";
				while (rs.next())	{	
					nombreFisos = (rs.getString("NOMBRE_FISOS") == null) ? "" : rs.getString("NOMBRE_FISOS");	
					cantidad = (rs.getString("CANTIDAD_SIRAC") == null) ? "" : rs.getString("CANTIDAD_SIRAC");
					monto = (rs.getString("MONTO_SIRAC") == null) ? "" : rs.getString("MONTO_SIRAC");
									
					// Datos de la consulta 
					documentoXLSX.agregaRenglon(indiceRenglon++); 
					indiceCelda					= 0;
					documentoXLSX.agregaCelda(indiceCelda++,nombreFisos,"CSTRING" );
					documentoXLSX.agregaCelda(indiceCelda++,cantidad,	"CENTERCSTRING" );					
					documentoXLSX.agregaCelda(indiceCelda++,"$"+Comunes.formatoDecimal(monto,2),	"CURRENCY");
					documentoXLSX.finalizaRenglon();					
					
				}//while
				 
			}
			documentoXLSX.finalizaHoja(); 			
			
			nombreArchivo = documentoXLSX.finalizaDocumento( directorioPlantilla, nombreProducto ); 
 
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	/**
	 * Dispersiones
	 * @return 
	 */
	public String  getDispersiones (){
	
		log.info("getDispersiones  (E) " );
		
		StringBuffer	qrySentencia 	= new StringBuffer();
		List	conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 		
		String totalReg="0";
		
		try{
		 
			con.conexionDB();
			
			qrySentencia.append(	"SELECT  COUNT(1) AS NUMERO_CADENAS     "+
			" FROM (    " +
			" SELECT  COUNT(1)   "+                                                    
			" FROM  COM_DISPERSION_ENLISTADA_X_EPO   "+                              
			" WHERE ( "+
			"     ( DF_ENVIO_FLUJO_FONDOS_OPR IS NULL  AND IG_TOTAL_DOCUMENTOS_OPR  > 0 )    "+                                                      
			"     OR ( DF_ENVIO_FLUJO_FONDOS_SNOPR IS NULL AND IG_TOTAL_DOCUMENTOS_SNOPR  > 0  ) "+                                                         
			"  )   "+                                           
			" AND DC_VENCIMIENTO >= trunc(Sysdate)   "+     
			" AND DC_VENCIMIENTO <  trunc(Sysdate+1)   "+
			" GROUP BY IC_EPO     "+                                                      
			" ) EPOS  ") ; 					
		
			PreparedStatement ps = con.queryPrecompilado(qrySentencia.toString());
			ResultSet rs = ps.executeQuery();
			
			if( rs.next()){
				totalReg  = rs.getString("NUMERO_CADENAS")==null?"0":rs.getString("NUMERO_CADENAS");
				
			}
			rs.close();
			ps.close();
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDispersiones  Error: " + e);
                        e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getDispersiones  (S) " );
		} 		
		return totalReg;
	}
	
	/**
	 * 	Reducci�n de Tasas
	 *   Los datos a mostrar solo son exclusivamente  los siguientes if y epos  
	 *  IC_EPO = 568  ( COMPA�IA NACIONAL ALMACENADORA, S.A. DE C.V.	 )
	 *  IC_EPO = 1137 (  CASA MARZAM S.A. DE C.V. ) 
	 *    
	 * @return 
	 */
	public HashMap  getReduccionTasas (){
		
		log.info("getReduccionTasas  (E) " );
		StringBuffer	qrySentencia 	= new StringBuffer();
		AccesoDB con = new AccesoDB(); 	
		HashMap	registros = new HashMap();
	
	try{
		con.conexionDB();
		
		qrySentencia.append(	" SELECT   /*+ leading (s) using_index(IN_COM_SOLICITUD_05_NUK)*/  "+
		" e.cg_razon_social as NOMBRE_EPO ,   count(s.ic_documento) TOTAL  "+
		" FROM com_solicitud s,   com_documento d,  comcat_epo e "+
		" WHERE s.ic_documento = d.ic_documento  "+
		" AND d.ic_epo = e.ic_epo    "+
		" AND d.ic_epo  in (1137,568)   "+
		" and s.DF_OPERACION  >=  trunc(Sysdate)   "+
		" and s.DF_OPERACION  <  trunc(Sysdate+1) "+
		" group by e.cg_razon_social ");  
								
			PreparedStatement ps = con.queryPrecompilado(qrySentencia.toString());
			ResultSet rs = ps.executeQuery();
			
			StringBuffer epos1  = new StringBuffer("");
			StringBuffer epos2  = new StringBuffer("");
			int x=0;
			while( rs.next() ){
				x++;				
				if(rs.getInt("TOTAL")>0){
					epos1.append(rs.getString("NOMBRE_EPO")==null?"":rs.getString("NOMBRE_EPO")+"<br>");					
				}			
			}
			rs.close();
			ps.close();
			
			if(x==0)  {
				
				qrySentencia 	= new StringBuffer();
				qrySentencia.append(	" SELECT  e.cg_razon_social as NOMBRE_EPO  "+
				"   FROM comcat_epo e "+
				" WHERE e.ic_epo  in (1137,568)  ");  
								

				PreparedStatement ps1 = con.queryPrecompilado(qrySentencia.toString());
				ResultSet rs1 = ps1.executeQuery();
			
				x=0;
				while( rs1.next() ){
						x++;
						if(x==2){  		epos2.append( " y ");  	}
						epos2.append(rs1.getString("NOMBRE_EPO")==null?"":rs1.getString("NOMBRE_EPO"));				
					}
					rs1.close();
					ps1.close();
				}
			
				registros.put("EPOS_1", epos1.toString());
				registros.put("EPOS_2", epos2.toString());
			
			

	
	} catch (Exception e) {
		log.error("getReduccionTasas  Error: " + e);
                e.printStackTrace();
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
		log.info("getReduccionTasas  (S) " );
	} 		
	return registros;
}

	/**
	 * Operaci�n Entrega Continua 
	 * Los datos a mostrar solo son exclusivamente  los siguientes if y epos
	 * IC_IF = 12    (NACIONAL FINANCIERA S.N.C. )
	 * IC_EPO = 396  ( ENTREGA CONTINUA DE VIVIENDA (INFONAVIT)) 
	 * ic_estatus_docto = 3 ( Seleccionada Pyme )
	 * 
	 * @return 
	 */
	public HashMap getEntregaContinua(){
	
		StringBuffer	qrySentencia 	= new StringBuffer();
		AccesoDB con = new AccesoDB(); 				
		HashMap	registros = new HashMap();	
		
		log.info("getEntregaContinua  (E) " );
		
		try{
			con.conexionDB();
			qrySentencia.append(
					"SELECT   /*+ leading (ds) */ " +
					"   e.cg_razon_social AS nombre_epo, COUNT (ds.ic_documento) total, " +
					"   SUM (d.fn_monto) AS monto " +
					"FROM com_docto_seleccionado ds, com_documento d, comcat_epo e " +
					"WHERE d.ic_documento = ds.ic_documento " +
					"   AND d.ic_epo = e.ic_epo " +
					"   AND d.ic_epo IN (396) " +
					"   AND d.ic_estatus_docto IN (3, 4) " +
					"   AND d.ic_if = 12 " +
					"   AND ds.df_fecha_seleccion >= TRUNC (SYSDATE) " +
					"   AND ds.df_fecha_seleccion < TRUNC (SYSDATE + 1) " +
					"GROUP BY e.cg_razon_social ");
				PreparedStatement ps = con.queryPrecompilado(qrySentencia.toString());
				ResultSet rs = ps.executeQuery();
				
				StringBuffer epos1  = new StringBuffer("");
				StringBuffer epos2  = new StringBuffer("");
				int x=0;			
				while( rs.next() ){
					x++;				
					registros.put("NOMBRE_EPO", rs.getString("NOMBRE_EPO")==null?"":rs.getString("NOMBRE_EPO"));
					registros.put("TOTAL", rs.getString("TOTAL")==null?"":rs.getString("TOTAL"));
					registros.put("MONTO", rs.getString("MONTO")==null?"":rs.getString("MONTO"));
				}
				rs.close();
				ps.close();				
				
				if(x==0)  {
					
					registros.put("TOTAL","0");
					registros.put("MONTO","0");
					
					qrySentencia 	= new StringBuffer();
					qrySentencia.append(
						" SELECT  e.cg_razon_social as NOMBRE_EPO  "+
					"   FROM comcat_epo e "+
					" WHERE e.ic_epo =  396  ");  
									
	
					PreparedStatement ps1 = con.queryPrecompilado(qrySentencia.toString());
					ResultSet rs1 = ps1.executeQuery();
					if( rs1.next() ){					
						registros.put("NOMBRE_EPO", rs1.getString("NOMBRE_EPO")==null?"":rs1.getString("NOMBRE_EPO"));
					}
					rs1.close();
					ps1.close();
				}	
		} catch (Exception e) {
			log.error("getEntregaContinua  Error: ", e);
                        e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getEntregaContinua  (S) " );
		} 		
	 
	return registros;
	}


	/**
	 * Operaci�n Cr�dito Electr�nico (Dls): 
	 * @return 
	 */
	public String  getCreditoElectronico (String moneda ){
		
		log.info("getCreditoElectronico  (E) " );
		
		StringBuffer	qrySentencia 	= new StringBuffer();
		List	conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 		
		String totalReg="0";
	
		try{
		
			con.conexionDB();
			
			qrySentencia.append(	"SELECT  COUNT (*)  as TOTAL   "+
			" FROM com_solic_portal   "+
			"	where ic_estatus_solic IN ( 3)   "+
			"  AND ic_moneda =  "+moneda+
			"  AND df_carga >= TRUNC (SYSDATE)  "+ 
			" AND df_carga < TRUNC (SYSDATE + 1) ") ;  					
		
			PreparedStatement ps = con.queryPrecompilado(qrySentencia.toString());
			ResultSet rs = ps.executeQuery();
			
			if( rs.next()){
				totalReg  = rs.getString("TOTAL")==null?"0":rs.getString("TOTAL");
				
			}
			rs.close();
			ps.close();
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getCreditoElectronico  Error: " + e);
                        e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getCreditoElectronico  (S) " );
		} 		
	return totalReg;
}


	/**
		 *
		 * Fodea 05-2014
		 * @throws netropology.utilerias.AppException
		 * @param claveEpos
		 * @param ruta
		 */
		public String  procesoEnviodeCorreo (List parametros)	throws NafinException	{
			log.info("procesoEnviodeCorreo (E)");
			
		   StringBuffer mensajeCorreo = new StringBuffer();
			HashMap 	archivoAdjunto 		 = new HashMap();		
			ArrayList listaDeArchivos = new ArrayList();
			ArrayList listaDeImagenes = new ArrayList(); 
						
			String remitente=  parametros.get(0).toString();
			String destinatario=  parametros.get(1).toString();
			String ccdestinatario=  parametros.get(2).toString();
			String asunto=  parametros.get(3).toString();
			String cuerpoCorreo=  parametros.get(4).toString();		
			String nombreArchivo=  parametros.get(5).toString();		
			String respuesta ="Fallo";  

			try {
			
				mensajeCorreo = new StringBuffer();
				mensajeCorreo.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">");				
				mensajeCorreo.append("<br>"+cuerpoCorreo+"<br>"); 
							
				mensajeCorreo.append(
					" <br>  ");    
				
				// para adjuntar el archivo
				archivoAdjunto = new HashMap();
				archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo );
				listaDeArchivos.add(archivoAdjunto);
						
				try { //Si el env�o del correo falla se ignora.  
					Correo correo = new Correo(); 
				   correo.setCodificacion("charset=UTF-8");    
					correo.enviaCorreoConDatosAdjuntos(remitente,destinatario,ccdestinatario,asunto,mensajeCorreo.toString(),listaDeImagenes,listaDeArchivos);
				} catch(Throwable t) {
					log.error("Error al enviar correo " + t.getMessage());
                                        t.printStackTrace();
				}
				
		}catch(Exception e){				
				log.error("procesoEnviodeCorreo (Exception) " + e);
				throw new AppException("procesoEnviodeCorreo(Exception) ", e);
			}finally{
				respuesta= "Correo enviado con �xito";
			}
			
			log.info("procesoEnviodeCorreo (S)");
			
			return respuesta;
			
		}
		
	/**
	 * metodo que registra la generaci�n del Corte 
	 * @param reg
	 */
	public List  getGenerarCorte (){
		log.info("getGenerarCorte  (E) " );
		
		StringBuffer	qrySentencia 	= new StringBuffer();		
		List  lVarBind		= new ArrayList();
		PreparedStatement ps	= null;
		ResultSet  rs = null;
		AccesoDB con = new AccesoDB(); 		
		boolean exito = true;
		HashMap datos = new HashMap();	
		List registros = new ArrayList();
		try{
		
			con.conexionDB();
	 		
			
			qrySentencia.append(queryGenerarCorte("") );
			
			ps = con.queryPrecompilado(qrySentencia.toString()); 
			log.debug("qrySentencia  "+qrySentencia ); 
			
			rs = ps.executeQuery();		
			
			
			
			while (rs.next())	{				
				String  ic_if  = (rs.getString("IC_IF") == null) ? "" : rs.getString("IC_IF");	
				String  nombre_fisos  = (rs.getString("NOMBRE_FISOS") == null) ? "" : rs.getString("NOMBRE_FISOS");	
				String cantidad_if  = (rs.getString("CANTIDAD_IF") == null) ? "" : rs.getString("CANTIDAD_IF");	
				String monto_if  = (rs.getString("MONTO_IF") == null) ? "" : rs.getString("MONTO_IF");	
				String cantidad_sirac  = (rs.getString("CANTIDAD_SIRAC") == null) ? "" : rs.getString("CANTIDAD_SIRAC");	
				String monto_sirac  = (rs.getString("MONTO_SIRAC") == null) ? "" : rs.getString("MONTO_SIRAC");	
			   String fechaCorte =  getUltimaFechaCorte (ic_if ); 
				
				datos = new HashMap();
				datos.put("IC_IF",ic_if);
				datos.put("NOMBRE_FISOS",nombre_fisos);
				datos.put("CANTIDAD_IF",cantidad_if);
				datos.put("MONTO_IF",monto_if);
				datos.put("CANTIDAD_SIRAC",cantidad_sirac);
				datos.put("MONTO_SIRAC",monto_sirac);
				datos.put("HORA_ULTIMO_CORTE",fechaCorte); 
				datos.put("SELECCION","");
				registros.add(datos);
			}
			rs.close();
			ps.close();
			
			
			
		} catch (Exception e) {
			exito = false;
			e.printStackTrace();
			log.error(" getGenerarCorte  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("   getGenerarCorte (S) ");
			}
		} 	
		return registros;
	}
	     
	/*
	 	Nota by jshernandez (22/04/2015 05:23:10 p.m.): A solicitud de Irving Sotuyo solo se considera en los 
	 	cortes moneda nacional.
	*/
	public String   queryGenerarCorte (String if_fisos  ){
	
		log.info("  id_producto ===== >>>>>>" +id_producto);
		log.info("  if_fisos ===== >>>>>>" +if_fisos);
		
		
		StringBuffer	qrySentencia 	= new StringBuffer();	
		
		if(id_producto.equals("1")) { // Descuento Electr�nico 
			
			
		qrySentencia.append("	SELECT   a.ic_if AS ic_if, a.nombre_fisos AS NOMBRE_FISOS , "+
			" a.CANTIDAD_IF AS  CANTIDAD_IF, a.MONTO_IF AS MONTO_IF,  "+
			" b.CANTIDAD_SIRAC  AS CANTIDAD_SIRAC ,   b.MONTO_SIRAC  AS MONTO_SIRAC " + 
			"  FROM   " +
			"  (  SELECT  i.ic_if as  IC_IF ,   "+
			"     i.cg_razon_social  AS NOMBRE_FISOS,  "+
			"     COUNT(s.ic_folio) AS  CANTIDAD_IF, "+
			"     SUM(ds.in_importe_recibir)  AS  MONTO_IF "+
			"     FROM comcat_if i,     "+
			"     com_documento  d,   "+
			"     com_docto_seleccionado ds, "+
			"     com_solicitud s,  "+
			"   com_parametrizacion_if p "+
			"     WHERE d.ic_documento = ds.ic_documento  "+
			"     AND d.ic_moneda = 1  " +
			"     AND   s.ic_documento = d.ic_documento  "+ 
			"     AND   i.ic_if         = ds.ic_if   "+ 
			"     AND s.DF_FECHA_SOLICITUD >= trunc (SYSDATE)  "+
			"     AND s.DF_FECHA_SOLICITUD  < trunc (SYSDATE+1)  "+					 
			"  	AND s.DF_FECHA_SOLICITUD > (select nvl(   max(DF_DE_CORTE_HORA), to_date ('01/01/1980', 'dd/mm/yyyy') )  from BIT_AVISOS_CORTES  b   where b.ic_if =ds.ic_if)  "+ //  esta forma date '1980-01-01'  la coloco para los registros que un no tiene fecha de corte, recomendaci�n de Gil
			"     AND s.IC_BLOQUEO != 1 " +
			"    and p.ic_if = ds.ic_if   "+
		   "    AND p.cc_parametro_if = 'FISO_CORTES'  "+
		   "    and p.cg_valor = 'S'    ");
			if(!if_fisos.equals("")){
				qrySentencia.append(" and ds.ic_if  in ( "+	if_fisos +") ");				
			}
			
			qrySentencia.append("    GROUP BY i.ic_if,  i.cg_razon_social  "+
			"    ORDER BY i.ic_if )   a ,   "+			
			"  ( SELECT       "+
			"   i.ic_if AS IC_IF,  i.cg_razon_social AS NOMBRE_FISOS,  "+
			"   COUNT(s.ic_folio)     AS      CANTIDAD_SIRAC,  "+
			"   SUM(ds.in_importe_recibir)  AS MONTO_SIRAC  "+
			"   FROM comcat_if  i,     "+
			"   com_documento   d,  "+
			"   com_docto_seleccionado ds,  "+
			"   com_solicitud s,  "+
			"   com_parametrizacion_if p "+
			"   WHERE d.ic_documento = ds.ic_documento  "+
			"   AND d.ic_moneda = 1  " +
			"   AND   s.ic_documento = d.ic_documento  "+
			"   AND   i.ic_if         = ds.ic_if "+
			"   AND s.DF_OPERACION >= trunc (SYSDATE) "+ 
			"   AND s.DF_OPERACION  < trunc (SYSDATE+1) "+			 
		   "	 AND s.DF_FECHA_SOLICITUD > (select nvl(   max(DF_DE_CORTE_HORA), to_date ('01/01/1980', 'dd/mm/yyyy') )  from BIT_AVISOS_CORTES  b   where b.ic_if =ds.ic_if)  "+	//  esta forma date '1980-01-01'  la coloco para los registros que un no tiene fecha de corte, recomendaci�n de Gil		
			"   AND  s.ic_estatus_solic = 3 "+
			"    and p.ic_if = ds.ic_if   "+
		   "    AND p.cc_parametro_if = 'FISO_CORTES'  "+
		   "    and p.cg_valor = 'S'    ");		
			if(!if_fisos.equals("")){
				qrySentencia.append(" and ds.ic_if  in ( "+	if_fisos +") ");		   		
			}
			qrySentencia.append("   GROUP BY i.ic_if,  i.cg_razon_social  "+     
			"   ORDER BY i.ic_if )  b  "+
			" where  a.ic_if = b.ic_if(+)  " );    
	
		}else  	if(id_producto.equals("0")) { //Cr�dito Electr�nico 			
			  
			 qrySentencia.append("  SELECT a.ic_if AS ic_if, a.nombre_fisos AS nombre_fisos,  "+
			 " a.cantidad_if AS cantidad_if, a.monto_if AS monto_if,  "+
			 " b.cantidad_sirac AS cantidad_sirac, b.monto_sirac AS monto_sirac  "+
			 " FROM (   SELECT   i.ic_if AS ic_if, i.cg_razon_social AS nombre_fisos,  "+
			 " COUNT (s.IC_SOLIC_PORTAL) AS cantidad_if,  "+
			 " SUM (s.FN_IMPORTE_DSCTO) AS monto_if  "+ 
			 " FROM comcat_if i, "+
			 "  com_solic_portal s,  "+
			 "  com_parametrizacion_if p "+
			 "  WHERE s.ic_moneda = 1  "+
			 "  AND i.ic_if = s.ic_if  "+
			 "  AND p.ic_if = s.ic_if  "+
			 "  AND s.df_carga  >= TRUNC (SYSDATE) "+  
          "  AND s.df_carga < TRUNC (SYSDATE + 1)  "+
			 "  AND s.df_carga >  (SELECT NVL (MAX (df_de_corte_hora), TO_DATE ('01/01/1980', 'dd/mm/yyyy') )  FROM bit_avisos_cortes b     WHERE b.ic_if = s.ic_if) "+
			 "  AND p.ic_if = s.ic_if "+
			 "  AND p.cc_parametro_if = 'FISO_CORTES_CREDELEC' "+
			 "  AND p.cg_valor = 'S'  ");
			 if(!if_fisos.equals("")){
					qrySentencia.append(" and s.ic_if  in ( "+	if_fisos +") ");
				}
				
			qrySentencia.append( "  GROUP BY i.ic_if, i.cg_razon_social  "+
			 "   ORDER BY i.ic_if    ) a,  "+
        
			 " (  SELECT   i.ic_if AS ic_if, i.cg_razon_social AS nombre_fisos,  "+
			 "    COUNT (s.IC_SOLIC_PORTAL) AS cantidad_sirac,  "+
			 "    SUM (s.FN_IMPORTE_DSCTO) AS monto_sirac "+
			 "    FROM comcat_if i,  "+
			 "    com_solic_portal s,  "+
			 "    com_parametrizacion_if p  "+
			 "    WHERE s.ic_moneda = 1  "+
          "    AND i.ic_if = s.ic_if  "+ 
			 "    AND p.ic_if = s.ic_if  "+
			 "    AND  s.df_carga >= TRUNC (SYSDATE)  "+
			 "    AND  s.df_carga < TRUNC (SYSDATE + 1)  "+
          "    AND  s.df_carga > (SELECT NVL (MAX (df_de_corte_hora), TO_DATE ('01/01/1980', 'dd/mm/yyyy')  )   FROM bit_avisos_cortes b   WHERE b.ic_if = s.ic_if) "+
			 "    AND  s.ic_estatus_solic = 3  "+
			 "    AND p.cc_parametro_if = 'FISO_CORTES_CREDELEC' "+
			 "    AND p.cg_valor = 'S'  ");
			 
			  if(!if_fisos.equals("")){
					qrySentencia.append(" and s.ic_if  in ( "+	if_fisos +") ");
				}
				
			qrySentencia.append("   GROUP BY i.ic_if, i.cg_razon_social  "+
			 "    ORDER BY i.ic_if   ) b "+
			 "    WHERE a.ic_if = b.ic_if(+)  ");
 
 
 
			
			
			
		}
		
		return qrySentencia.toString(); 
	}
	
	 /**
	 * se obtiene la ultima fecha de corte  
	 * @return 
	 *
	 * Nota by jshernandez (27/04/2015 03:31:09 p.m.): Se restringe la fecha de corte
	 * consultada para que s�lo traiga las del d�a.
	 *
	 */
	public String  getUltimaFechaCorte (String ic_if  ){ 
		log.info("getUltimaFechaCorte  (E) " );
		
		StringBuffer	qrySentencia 	= new StringBuffer();					
		PreparedStatement ps	= null;
		ResultSet  rs = null;
		AccesoDB con = new AccesoDB(); 		
		boolean exito = true;	
		String fechaCorte ="";
		
		try{ 
		
			con.conexionDB();
				
			qrySentencia 	= new StringBuffer();
			qrySentencia.append(
				"SELECT                                                                     "  +
				"  MAX( TO_CHAR(DF_DE_CORTE_HORA, 'dd/mm/yyyy HH24:mi:ss') ) AS FECHA_CORTE "  +
				"FROM                                                                       "  +
				"  BIT_AVISOS_CORTES                                                        "  +
				"WHERE                                                                      "  +
				"  DF_DE_CORTE_HORA  >= TRUNC(sysdate)     AND                              "  +
				"  DF_DE_CORTE_HORA  <  TRUNC(sysdate) + 1                                  "
			); 
			
			if(!ic_if.equals("")) {
				qrySentencia.append(" and  ic_if = "+ic_if);    
			}
			 
			
			ps = con.queryPrecompilado(qrySentencia.toString()); 
			rs = ps.executeQuery();		
			
			log.debug("qrySentencia  "+qrySentencia ); 
			
			if (rs.next())	{				
				fechaCorte  = (rs.getString("FECHA_CORTE") == null) ? "" : rs.getString("FECHA_CORTE");	
						
			}		
			rs.close();
			ps.close();		  	 
			
			
		} catch (Exception e) {
			exito = false;
			e.printStackTrace();
			log.error("getUltimaFechaCorte  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  getReduccionTasas (S) ");
			}	
		}
		return fechaCorte;
		
	}
	
	/**
	 * metodo que registra la fecha y hora en la que se envio el correo 
	 * de los cortes 
	 * @param ic_bitacora  
	 */
	public void setEnvioCortes ( String[]  ic_if,  String[]  cantidad_if  , String[]  monto_if, String[]  cantidad_sirac ,  String[] monto_sirac , String fechaCorte ){
		log.info("setEnvioCortes  (E) " );
		
		StringBuffer	qrySentencia 	= new StringBuffer();					
		PreparedStatement ps	= null;
		AccesoDB con = new AccesoDB(); 		
		boolean exito = true;	
		List  lVarBind		= new ArrayList();
		 
		try{
		
			con.conexionDB(); 
			
			for(int x=0; x<ic_if.length;x++){
				// Inserto a Cortes  
				qrySentencia 	= new StringBuffer();
				qrySentencia.append(" INSERT INTO BIT_AVISOS_CORTES " +
					" (IC_BITACORA ,  IC_IF ,  IC_CANTIDAD_IF  , FN_MONTO_IF , IC_CANTIDAD_SIRAC , FN_MONTO_SIRAC , DF_DE_CORTE_HORA, DF_DE_ENVIO_CORREO   )  "  +
					" values  ( SEQ_BIT_AVISOS_CORTES.NEXTVAL, ? ,  ? , ? , ?, ?, to_date(?, 'dd mm rrrr HH24:MI:SS') , Sysdate   ) "); 	  	 			
						
				lVarBind		= new ArrayList();
				lVarBind.add(ic_if[x]);	
				lVarBind.add(cantidad_if[x]);	
				lVarBind.add(monto_if[x]);	
				lVarBind.add(cantidad_sirac[x]);	
				lVarBind.add(monto_sirac[x]);	
				lVarBind.add(fechaCorte);	 
						
				log.debug("qrySentencia  "+qrySentencia );
				log.debug("lVarBind  "+lVarBind );
						
				ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();	
			}
			
		} catch (Exception e) {
			exito = false;
			e.printStackTrace();
			log.error("setEnvioCortes  Error: " + e);
			log.error("setEnvioCortes  Error: qrySentencia  "+qrySentencia );
			log.error("setEnvioCortes  Error: lVarBind      "+lVarBind );
		} finally {
			if( ps != null ){ try { ps.close(); }catch(Exception e){} }
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  setEnvioCortes (S) ");
			}	
		} 				
	}
	

	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getNo_operacion() {
		return no_operacion;
	}

	public void setNo_operacion(String no_operacion) {
		this.no_operacion = no_operacion;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public String getDirectorioPlantilla() {
		return directorioPlantilla;
	} 

	public void setDirectorioPlantilla(String directorioPlantilla) {
		this.directorioPlantilla = directorioPlantilla;
	}

	public String getIf_fisos() {
		return if_fisos;
	}

	public void setIf_fisos(String if_fisos) {
		this.if_fisos = if_fisos;
	}

	public String getIc_bitacora() {
		return ic_bitacora;
	}

	public void setIc_bitacora(String ic_bitacora) {
		this.ic_bitacora = ic_bitacora;
	}

	public String getActualizarCorte() {
		return actualizarCorte;
	}
 
	public void setActualizarCorte(String actualizarCorte) {
		this.actualizarCorte = actualizarCorte;
	}
 
	public String getId_producto() {
		return id_producto;
	}

	public void setId_producto(String id_producto) {
		this.id_producto = id_producto;
	}


		

}