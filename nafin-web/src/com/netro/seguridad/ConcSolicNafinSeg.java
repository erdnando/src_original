package com.netro.seguridad;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/*ultimo*/
public class ConcSolicNafinSeg implements IQueryGeneratorPS,  IQueryGeneratorRegExtJS {	
	private int numList = 1;
	
	public ConcSolicNafinSeg() {}
  
	public String getAggregateCalculationQuery(HttpServletRequest request){
		String first_name		= (request.getParameter("first_name")==null)?"":request.getParameter("first_name");
		String middle_name	= (request.getParameter("middle_name")==null)?"":request.getParameter("middle_name");
		String last_name		= (request.getParameter("last_name")==null)?"":request.getParameter("last_name");
		String email			= (request.getParameter("email")==null)?"":request.getParameter("email");
		String status			= (request.getParameter("status")==null)?"":request.getParameter("status");

	  	StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append(
			" select count(1)"+
			" from ag_person ap "+
			" ,ag_application_rol ar "+
			" where ap.id_person = ar.id_person");

		if(!first_name.equals(""))
			qrySentencia.append(" and upper(ap.first_name) like ? ");
		if(!middle_name.equals(""))
			qrySentencia.append(" and upper(ap.middle_name) like ?");
		if(!last_name.equals(""))
			qrySentencia.append(" and upper(ap.last_name) like ? ");
		if(!email.equals(""))
			qrySentencia.append(" and email = ? ");
		if(!status.equals(""))
			qrySentencia.append(" and ar.status = ? ");

	  return qrySentencia.toString();
	}

	public int getNumList(HttpServletRequest request){
		return numList;	
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
		StringBuffer query = new StringBuffer();
	    StringBuffer lstdoctos = new StringBuffer();

		for (Iterator it = ids.iterator(); it.hasNext();){
				it.next();
				lstdoctos.append("?,");
		}
		lstdoctos = lstdoctos.delete(lstdoctos.length()-1,lstdoctos.length());
		query.append(
			" select ap.first_name "+
			" ,ap.middle_name "+
			" ,ap.last_name "+
			" ,ap.email "+
			" ,decode(ar.status,1,'Solicitud en proceso',2,'Solicitud aprobada',3,'Solicitud rechazada',4,'Solicitud eliminada o dada de baja') "+
			" ,nvl(request_error,'') as request_error "+
			" from ag_person ap "+
			" ,ag_application_rol ar "+
			" where ap.id_person = ar.id_person"+
			" and ap.id_person in("+lstdoctos.toString()+")"
		);

		return query.toString();

  }

// Devuelve el query de llaves primarias
	public String getDocumentQuery(HttpServletRequest request){
		String first_name		= (request.getParameter("first_name")==null)?"":request.getParameter("first_name");
		String middle_name	= (request.getParameter("middle_name")==null)?"":request.getParameter("middle_name");
		String last_name		= (request.getParameter("last_name")==null)?"":request.getParameter("last_name");
		String email			= (request.getParameter("email")==null)?"":request.getParameter("email");
		String status			= (request.getParameter("status")==null)?"":request.getParameter("status");

	  	StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append(
			" select ap.id_person "+
			" from ag_person ap "+
			" ,ag_application_rol ar "+
			" where ap.id_person = ar.id_person");

		if(!first_name.equals(""))
			qrySentencia.append(" and upper(ap.first_name) like ? ");
		if(!middle_name.equals(""))
			qrySentencia.append(" and upper(ap.middle_name) like ?");
		if(!last_name.equals(""))
			qrySentencia.append(" and upper(ap.last_name) like ? ");
		if(!email.equals(""))
			qrySentencia.append(" and email = ? ");
		if(!status.equals(""))
			qrySentencia.append(" and ar.status = ? ");			
	  return qrySentencia.toString();
  }

	public String getDocumentQueryFile(HttpServletRequest request){
		String first_name		= (request.getParameter("first_name")==null)?"":request.getParameter("first_name");
		String middle_name	= (request.getParameter("middle_name")==null)?"":request.getParameter("middle_name");
		String last_name		= (request.getParameter("last_name")==null)?"":request.getParameter("last_name");
		String email			= (request.getParameter("email")==null)?"":request.getParameter("email");
		String status			= (request.getParameter("status")==null)?"":request.getParameter("status");

	  	StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append(
			" select ap.first_name "+
			" ,ap.middle_name "+
			" ,ap.last_name "+
			" ,ap.email "+
			" ,decode(ar.status,1,'Solicitud en proceso',2,'Solicitud aprobada',3,'Solicitud rechazada',4,'Solicitud eliminada o dada de baja') "+
			" ,nvl(request_error,'') as request_error "+
			" from ag_person ap "+
			" ,ag_application_rol ar "+
			" where ap.id_person = ar.id_person");

		if(!first_name.equals(""))
			qrySentencia.append(" and upper(ap.first_name) like ? ");
		if(!middle_name.equals(""))
			qrySentencia.append(" and upper(ap.middle_name) like ?");
		if(!last_name.equals(""))
			qrySentencia.append(" and upper(ap.last_name) like ? ");
		if(!email.equals(""))
			qrySentencia.append(" and email = ? ");
		if(!status.equals(""))
			qrySentencia.append(" and ar.status = ? ");
			
	  return qrySentencia.toString();
  }

	public ArrayList getConditions(HttpServletRequest request){

		ArrayList condiciones = new ArrayList();
		String first_name		= (request.getParameter("first_name")==null)?"":request.getParameter("first_name");
		String middle_name	= (request.getParameter("middle_name")==null)?"":request.getParameter("middle_name");
		String last_name		= (request.getParameter("last_name")==null)?"":request.getParameter("last_name");
		String email			= (request.getParameter("email")==null)?"":request.getParameter("email");
		String status			= (request.getParameter("status")==null)?"":request.getParameter("status");
		
		if(!first_name.equals(""))
			condiciones.add("%"+first_name.toUpperCase()+"%");
		if(!middle_name.equals(""))
			condiciones.add("%"+middle_name.toUpperCase()+"%");
		if(!last_name.equals(""))
			condiciones.add("%"+last_name.toUpperCase()+"%");
		if(!email.equals(""))
			condiciones.add(email);
		if(!status.equals(""))
			condiciones.add(new Integer(status));

	  return condiciones;
  }
  
  
  //*****************+Migraci�n Nafin ************************
  
	private final static Log log = ServiceLocator.getInstance().getLog(ConcSolicNafinSeg.class);
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String 	paginaOffset;
	private String 	paginaNo;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String email;
	private String estatus;
	
  
  public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		qrySentencia.append(
			" select ap.id_person "+
			" from ag_person ap "+
			" ,ag_application_rol ar "+
			" where ap.id_person = ar.id_person");

		if(!nombre.equals("")){
			qrySentencia.append(" and upper(ap.first_name) like ? ");
			conditions.add("%"+nombre.toUpperCase()+"%");
		}
		if(!apellidoPaterno.equals("")){
			qrySentencia.append(" and upper(ap.middle_name) like ?");
			conditions.add("%"+apellidoPaterno.toUpperCase()+"%");
		}
		if(!apellidoMaterno.equals("")){
			qrySentencia.append(" and upper(ap.last_name) like ? ");
			conditions.add("%"+apellidoMaterno.toUpperCase()+"%");
		}
		if(!email.equals("")){
			qrySentencia.append(" and email = ? ");
			conditions.add(email);
		}
		if(!estatus.equals("")) {
			qrySentencia.append(" and ar.status = ? ");			
			conditions.add(estatus);
		}
	   log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
				
			qrySentencia.append(
			" select ap.first_name as NOMBRE   "+
			" ,ap.middle_name  as APELLIDO_PATERNO "+
			" ,ap.last_name as APELLIDO_MATERNO  "+
			" ,ap.email as CORREO_ELECTRONICO  "+
			" ,decode(ar.status,1,'Solicitud en proceso',2,'Solicitud aprobada',3,'Solicitud rechazada',4,'Solicitud eliminada o dada de baja') as ESTATUS "+
			" ,nvl(request_error,'') as CAUSA "+
			" from ag_person ap "+
			" ,ag_application_rol ar "+
			" where ap.id_person = ar.id_person");
		
		
			qrySentencia.append(" AND (");
			
			for (int i = 0; i < pageIds.size(); i++) { 
				List lItem = (ArrayList)pageIds.get(i);
				
				if(i > 0){qrySentencia.append("  OR  ");}
				
				qrySentencia.append("ap.id_person = ?");
				conditions.add(new Long(lItem.get(0).toString()));
			}
			
			qrySentencia.append(" ) ");
			
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
				
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			}
				
			pdfDoc.setTable(6,100);
			pdfDoc.setCell("Nombre","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Apellido Paterno","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Apellido Materno","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Correo Electr�nico","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Causa","celda01",ComunesPDF.CENTER);
			
			while (rs.next())	{					
				String nombre = (rs.getString("NOMBRE") == null) ? "" : rs.getString("NOMBRE");
				String apellidoPaterno= (rs.getString("APELLIDO_PATERNO") == null) ? "" : rs.getString("APELLIDO_PATERNO");
				String apellidoMaterno = (rs.getString("APELLIDO_MATERNO") == null) ? "" : rs.getString("APELLIDO_MATERNO");
				String email = (rs.getString("CORREO_ELECTRONICO") == null) ? "" : rs.getString("CORREO_ELECTRONICO");
				String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
				String causa = (rs.getString("CAUSA") == null) ? "" : rs.getString("CAUSA");
				
				pdfDoc.setCell(nombre,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(apellidoPaterno,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(apellidoMaterno,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(email,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(estatus,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(causa,"formas",ComunesPDF.LEFT);
				
			}
				
			if(tipo.equals("PDF") ) {
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			}
		
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo = "";
			
		try {
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}


		/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	
}