package com.netro.seguridad;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;


public class ConsUsuariosNafinSeg  {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsUsuariosNafinSeg.class);
	private String tipoUsuario;
	private String nombreEmpresa;
	private String numeroElectronico;
			 
	public ConsUsuariosNafinSeg() { }
	
	/**
	 * 
	 * @return 
	 * @param tipoUsuario
	 */
	public List  getCatalogoPerfil(String tipoUsuario  ){
		
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
			
			String SQL =  "SELECT CC_PERFIL FROM SEG_PERFIL WHERE SC_TIPO_USUARIO = " + tipoUsuario + "ORDER BY 1" ;
											
			rs = con.queryDB(SQL);			
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString(1) );
				datos.put("descripcion", rs.getString(1));
				registros.add(datos);
			}
			rs.close();
			con.cierraStatement();
		
		} catch (Exception e) {
			System.out.println("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		
		return registros;
	}
	
		/**
	 * Obtiene los datos del afiliado.
	 * @param tipoAfiliado E epo I If P Pyme
	 * @param claveAfiliado Clave del afiliado (ic_epo, ic_if o ic_pyme)
	 * @return Lista con los datos del afiliado:
	 * 		0.- Razon social del afiliado
	 * 		1.- Numero de Nafin Electr�nico
	 */
	public List getDatosAfiliado(String tipoAfiliado, String claveAfiliado)  throws Exception {
		AccesoDB con = new AccesoDB();
		String tabla = "";
		String campo = "";
		if (tipoAfiliado.equals("E")) {
			tabla = "comcat_epo";
			campo = "ic_epo";
		} else if (tipoAfiliado.equals("I")) {
			tabla = "comcat_if";
			campo = "ic_if";
		} else {
			tabla = "comcat_pyme";
			campo = "ic_pyme";
		}
		try {
			con.conexionDB();
			
			String strSQL = 
						"SELECT afiliado.cg_razon_social, n.ic_nafin_electronico " +
						" FROM " + tabla + " afiliado, comrel_nafin n " +
						" WHERE afiliado." + campo + " = n.ic_epo_pyme_if " +
						" AND n.cg_tipo = ? " +
						" AND n.ic_epo_pyme_if = ? ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, tipoAfiliado);
			ps.setInt(2, Integer.parseInt(claveAfiliado));
			ResultSet rs = ps.executeQuery();
			List datos = new ArrayList();
			if (rs.next()) {
				datos.add(rs.getString("cg_razon_social"));
				datos.add(rs.getString("ic_nafin_electronico"));
			}
			rs.close();
			ps.close();
			return datos;
		} finally {
			if (con.hayConexionAbierta()) {			
				con.cierraConexionDB();
			}
		}
	}

    /**
    * 
    * @throws java.lang.Exception
    * @return 
    * @param perfil
    * @param claveAfiliado
    * @param tipoAfiliado
    */
    private ArrayList getListaUsuarios(String tipoAfiliado, String claveAfiliado, String perfil) throws Exception {
        
        ArrayList   listaUsuarios   = new ArrayList();
        UtilUsr     oUtilUsr        = new UtilUsr();
        UtilUsr     utilUsr         = new UtilUsr();
        
        List cuentas = utilUsr.getUsuariosxAfiliado( claveAfiliado, tipoAfiliado);
        Iterator itCuentas = cuentas.iterator();
        while (itCuentas.hasNext()) {
            netropology.utilerias.usuarios.Usuario oUsuario = null;
            String login = (String) itCuentas.next();
            String perfil_usr=utilUsr.getPerfilUsuario(login);
            if( "".equals(perfil) || perfil.equals(perfil_usr)) {
                oUsuario = oUtilUsr.getUsuario(login);
                listaUsuarios.add(oUsuario);
            }
        }        
        Collections.sort(listaUsuarios);
        
        return listaUsuarios;
        
    }
    

    public String getPerfilByUsuario(String usuario) throws Exception {
        UtilUsr     oUtilUsr        = new UtilUsr();       
        return oUtilUsr.getUsuario(usuario).getPerfil();
    }
    
    public Usuario getUsuarioById(String usuario) throws Exception {
        UtilUsr     oUtilUsr        = new UtilUsr();   
        return oUtilUsr.getUsuario(usuario);
    }
    
	/**
	 * 
	 * @throws java.lang.Exception
	 * @return 
	 * @param perfil
	 * @param empresa
	 * @param tipoUsuario
	 */
	public List getConsultar(String tipoUsuario, String empresa, String perfil) throws Exception {
		AccesoDB con = new AccesoDB();	
		 List informacion = new  ArrayList();		 
		 HashMap registros = new HashMap();
		 List listaUsuarios = new  ArrayList();
		 PreparedStatement ps = null;
		 ResultSet rs =null;
		 String datos = "";
		 
		try {
			con.conexionDB();
			
			if (tipoUsuario.equals("4") || tipoUsuario.equals("8")) {
				listaUsuarios = getListaUsuarios("N", "", perfil);
			} else {
				listaUsuarios = getListaUsuarios((tipoUsuario.equals("1")?"E":"I"), empresa, perfil);
			}			
			if( listaUsuarios.size() > 0 ){
				if(tipoUsuario.equals("8")){
					for (int indice=0; indice < listaUsuarios.size(); indice++){ 
						netropology.utilerias.usuarios.Usuario oUsuario = (Usuario)listaUsuarios.get(indice); 
						int num=0;
						String SQLConsulta =	"SELECT count(CC_PERFIL) FROM SEG_PERFIL WHERE SC_TIPO_USUARIO = " + tipoUsuario + "AND CC_PERFIL='"+oUsuario.getPerfil()+"'";
						ps = con.queryPrecompilado(SQLConsulta);
						rs = ps.executeQuery();
						ps.clearParameters();
						if(rs.next()) {
							num = rs.getInt(1);
						}
						ps.close();
						rs.close();
						if(num!=0){							
							datos  = oUsuario.getPerfil()+","+oUsuario.getLogin()+","+oUsuario.getNombre()+","+oUsuario.getApellidoPaterno()+","+oUsuario.getApellidoMaterno()+","+oUsuario.getEmail();
                  	 registros = new HashMap();
							 registros.put("ID_REGISTRO", datos);
							 registros.put("PERFIL", oUsuario.getPerfil());
							 registros.put("LOGIN", oUsuario.getLogin());
							 registros.put("NOMBRE", oUsuario.getNombre());
							 registros.put("APELLIDO_PATERMO", oUsuario.getApellidoPaterno());
							 registros.put("APELLIDO_MATERNO", oUsuario.getApellidoMaterno());
							 registros.put("CORREO", oUsuario.getEmail());
							 informacion.add(registros);
						}
		         }
					
				} 	else if(tipoUsuario.equals("4")){
				
					for (int indice=0; indice < listaUsuarios.size(); indice++){ 
						netropology.utilerias.usuarios.Usuario oUsuario = (Usuario)listaUsuarios.get(indice); 
						int num=0;
						String SQLConsulta =	"SELECT count(CC_PERFIL) FROM SEG_PERFIL WHERE SC_TIPO_USUARIO = " + tipoUsuario + " AND CC_PERFIL='"+oUsuario.getPerfil()+"'";
						ps = con.queryPrecompilado(SQLConsulta);
						rs = ps.executeQuery();
						ps.clearParameters();
						if(rs.next()) {
							num = rs.getInt(1);
						}
						ps.close();
						rs.close();	
						if(num!=0){		
							 datos  = oUsuario.getPerfil()+","+oUsuario.getLogin()+","+oUsuario.getNombre()+","+oUsuario.getApellidoPaterno()+","+oUsuario.getApellidoMaterno()+","+oUsuario.getEmail();      							
							 registros = new HashMap();
							 registros.put("ID_REGISTRO", datos);
							 registros.put("PERFIL", oUsuario.getPerfil());
							 registros.put("LOGIN", oUsuario.getLogin());
							 registros.put("NOMBRE", oUsuario.getNombre());
							 registros.put("APELLIDO_PATERMO", oUsuario.getApellidoPaterno());
							 registros.put("APELLIDO_MATERNO", oUsuario.getApellidoMaterno());
							 registros.put("CORREO", oUsuario.getEmail());
							 informacion.add(registros);							 
						}					
					}
				} else {
					for (int indice=0; indice < listaUsuarios.size(); indice++){ 
						netropology.utilerias.usuarios.Usuario oUsuario = (Usuario)listaUsuarios.get(indice); 
						datos  = oUsuario.getPerfil()+","+oUsuario.getLogin()+","+oUsuario.getNombre()+","+oUsuario.getApellidoPaterno()+","+oUsuario.getApellidoMaterno()+","+oUsuario.getEmail();
						registros = new HashMap();
						registros.put("ID_REGISTRO", datos);
						registros.put("PERFIL", oUsuario.getPerfil());
						registros.put("LOGIN", oUsuario.getLogin());
						registros.put("NOMBRE", oUsuario.getNombre());
						registros.put("APELLIDO_PATERMO", oUsuario.getApellidoPaterno());
						registros.put("APELLIDO_MATERNO", oUsuario.getApellidoMaterno());
						registros.put("CORREO", oUsuario.getEmail());
						informacion.add(registros);		
							 
					}
				}
			}
						
			
			return informacion;
		} finally {
			if (con.hayConexionAbierta()) {			
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * 
	 * @throws java.lang.Exception
	 * @return 
	 * @param path
	 * @param tipoArchivo
	 * @param registros
	 * @param request
	 */
	
	public String generarArchivos(HttpServletRequest request,List registros, String tipoArchivo, String path) throws Exception {
	
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		try  {
			
			if(tipoArchivo.equals("PDF"))  {
		
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
				 pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			
				pdfDoc.setTable(6,100);
				if( "1".equals(tipoUsuario) ||  "2".equals(tipoUsuario)   ){
					pdfDoc.setCell("Empresa: ", "celda02",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreEmpresa, "formas",ComunesPDF.LEFT, 5);
					pdfDoc.setCell("N�mero N@E : ", "celda02",ComunesPDF.CENTER);
					pdfDoc.setCell(numeroElectronico, "formas",ComunesPDF.LEFT, 5);				
				}
				pdfDoc.setCell("Perfil", "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Login", "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre", "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Apellido Paterno", "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Apellido Materno", "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Correo Electr�nico", "celda02",ComunesPDF.CENTER);
				
			}
			if(tipoArchivo.equals("CSV"))  {
			
				if( "1".equals(tipoUsuario) ||  "2".equals(tipoUsuario)   ){				
					contenidoArchivo.append("\n");
					contenidoArchivo.append(" Empresa: ," +nombreEmpresa.replaceAll(",","")+"\n");
					contenidoArchivo.append(" N�mero N@E : ," +numeroElectronico.replaceAll(",","")+"\n");
					contenidoArchivo.append("\n");
				}			
				contenidoArchivo.append(" Perfil, Login,  Nombre, Apellido Paterno , Apellido Materno, Correo Electr�nico \n ");
			}
		
			for (int i = 0; i <registros.size(); i++) {
				HashMap solicitud = (HashMap)registros.get(i);			
				String perfil  = solicitud.get("PERFIL").toString();
				String login  = solicitud.get("LOGIN").toString();
				String nombre  = solicitud.get("NOMBRE").toString();
				String apellidoPaterno  = solicitud.get("APELLIDO_PATERMO").toString();
				String apellidoMaterno  = solicitud.get("APELLIDO_MATERNO").toString();
				String correo  = solicitud.get("CORREO").toString();
				
				if(tipoArchivo.equals("PDF"))  {	
					pdfDoc.setCell(perfil,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(login,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(nombre,"formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(apellidoPaterno,"formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(apellidoMaterno,"formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(correo,"formas",ComunesPDF.LEFT);	
				}
				if(tipoArchivo.equals("CSV") ) {
					contenidoArchivo.append(perfil.replaceAll(",","")+","+
													login.replaceAll(",","")+","+
													nombre.replaceAll(",","")+","+
													apellidoPaterno.replaceAll(",","")+","+
													apellidoMaterno.replaceAll(",","")+","+
													correo.replaceAll(",","")+"\n");				
				}				
			}	
			
			if(tipoArchivo.equals("PDF"))  {
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}
			
			if(tipoArchivo.equals("CSV") ) {
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}

		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getNumeroElectronico() {
		return numeroElectronico;
	}

	public void setNumeroElectronico(String numeroElectronico) {
		this.numeroElectronico = numeroElectronico;
	}
	

}