package com.netro.seguridad;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Bitacora;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;


public class CambioPerfil  {  

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CambioPerfil.class);
	
			 
	public CambioPerfil() { }
	

  	public HashMap getConsultaUsuario(String txtLogin, String afiliados   )  throws Exception {
		AccesoDB con = new AccesoDB();
		UtilUsr 		oUtilUsr 		= new UtilUsr();
		Usuario oUsuario = new Usuario();
		
		String sNombreEmpresa ="",  internacional ="", sTipoAfiliado ="";
		int iTipoPerfil = 0, iNoElectronico= 0;
		List listaPerfiles = new ArrayList();
		HashMap informacion = new HashMap();
		
		String mensaje ="";
		
		try {
			con.conexionDB();			
			
			oUtilUsr = new UtilUsr();
			oUsuario = oUtilUsr.getUsuario(txtLogin);
			if (!"".equals(oUsuario.getTipoAfiliado())) {
				String sCampos = null;
				String sTabla = null;
				String sQuery = null;
				con.conexionDB();
				sTipoAfiliado = oUsuario.getTipoAfiliado();
				String sClaveAfiliado = "";
				
				if ("N".equals(sTipoAfiliado)) {
					sClaveAfiliado = "0";	//Para usuarios Nafin la clave de afiliado no aplica. se establece un 0
				} else {
					sClaveAfiliado = oUsuario.getClaveAfiliado();
				}
				
				if ("E".equals(sTipoAfiliado) && !"".equals(sClaveAfiliado)) {
					sQuery = " SELECT ic_nafin_electronico, datos.cg_Razon_social, 1 as tipoPerfil, DECODE(CS_INTERNACIONAL,'S','I','') as internacional" +
						" FROM comcat_epo datos, comrel_nafin n"+
						" WHERE datos.ic_epo = n.ic_epo_pyme_if "+
						" AND ic_epo_pyme_if = ?"+
						" AND cg_tipo = ? ";
					
				} else if ("I".equals(sTipoAfiliado) && !"".equals(sClaveAfiliado)) {
					sQuery = " SELECT ic_nafin_electronico, datos.cg_Razon_social, decode(NVL(datos.IC_CONTRAGARANTE, 0), 0, 2, 6) as tipoPerfil  " +
						" FROM comcat_if datos, comrel_nafin n "+
						" WHERE datos.ic_if = n.ic_epo_pyme_if "+
						" AND ic_epo_pyme_if = ?"+
						" AND cg_tipo = ? ";
				} else if ("P".equals(sTipoAfiliado) && !"".equals(sClaveAfiliado)) {
					sQuery = " SELECT ic_nafin_electronico, datos.cg_Razon_social, 3 as tipoPerfil  " +
						" FROM comcat_pyme datos, comrel_nafin n "+
						" WHERE datos.ic_pyme = n.ic_epo_pyme_if "+
						" AND ic_epo_pyme_if = ?"+
						" AND cg_tipo = ? ";
				} else if ("D".equals(sTipoAfiliado) && !"".equals(sClaveAfiliado)) {
					sQuery = " SELECT ic_nafin_electronico, datos.cg_Razon_social, 5 as tipoPerfil  " +
						" FROM foncat_distribuidor datos, comrel_nafin n "+
						" WHERE datos.ic_distribuidor = n.ic_epo_pyme_if "+
						" AND ic_epo_pyme_if = ?"+
						" AND cg_tipo = ? ";
				} else if ("C".equals(sTipoAfiliado) && !"".equals(sClaveAfiliado)) {
					sQuery = " SELECT  datos.IC_NAFIN_ELECTRONICO, DECODE (CS_TIPO_PERSONA,  'F', CG_NOMBRE || ' ' || CG_APPAT || ' ' || CG_APMAT ,    CG_RAZON_SOCIAL  ) as CG_RAZON_SOCIAL , 10 as tipoPerfil  " +
						" FROM COMCAT_CLI_EXTERNO datos , comrel_nafin n "+
						" WHERE datos.IC_NAFIN_ELECTRONICO = n.ic_epo_pyme_if "+
						" AND datos.IC_NAFIN_ELECTRONICO = ? "+ 
						" AND cg_tipo = ? ";  
						
				} else if ("M".equals(sTipoAfiliado) && !"".equals(sClaveAfiliado)) {
				
				sQuery =" SELECT ic_nafin_electronico, datos.cg_Razon_social, 11 as tipoPerfil "+
				" FROM COMCAT_MANDANTE datos, comrel_nafin n " +
				" WHERE datos.IC_MANDANTE = n.ic_epo_pyme_if "+
				" AND n.ic_epo_pyme_if = ? "+
				" AND cg_tipo = ? ";
				
				} else if ("A".equals(sTipoAfiliado) && !"".equals(sClaveAfiliado)) {
					sQuery = "  SELECT n.ic_nafin_electronico, datos.cg_Razon_social, 1 as tipoPerfil "+
					"  FROM fe_afianzadora datos, comrel_nafin n "+
					"  WHERE datos.IC_AFIANZADORA = n.ic_epo_pyme_if "+
					"  AND ic_epo_pyme_if = ? "+
					"  AND cg_tipo = ? "; 				
							
				} else if ("F".equals(sTipoAfiliado) && !"".equals(sClaveAfiliado)) {
					sQuery = "  SELECT n.ic_nafin_electronico, datos.cg_nombre cg_razon_social, 13 as tipoPerfil "+
					"  FROM fe_fiado datos, comrel_nafin n "+
					"  WHERE datos.ic_fiado = n.ic_epo_pyme_if "+
					"  AND ic_epo_pyme_if = ? "+
					"  AND cg_tipo = ? "; 				
							
				}else if ("N".equals(sTipoAfiliado)) {
				
				} else {
					sQuery = "  SELECT 0 as ic_nafin_electronico, 'Tipo de afiliado:" + sTipoAfiliado + " no implementado' as cg_razon_social, 0 as tipoPerfil "+
							" FROM dual";
				}
		
				System.out.println("sQuery  "+sQuery); 
				System.out.println("oUsuario.getClaveAfiliado()  "+oUsuario.getClaveAfiliado()); 
				System.out.println("sTipoAfiliado  "+sTipoAfiliado);  
				
				
		
				if (!"N".equals(sTipoAfiliado)) {				
					PreparedStatement pst = con.queryPrecompilado(sQuery);					
					pst.setString(1, oUsuario.getClaveAfiliado());
					pst.setString(2, sTipoAfiliado);
					ResultSet rs = pst.executeQuery();
					
					if (rs.next()) {
						sNombreEmpresa = rs.getString("CG_RAZON_SOCIAL");
						iNoElectronico = rs.getInt("IC_NAFIN_ELECTRONICO");
						iTipoPerfil = rs.getInt("TIPOPERFIL");
						if ("E".equals(sTipoAfiliado)) {
							internacional = (rs.getString("INTERNACIONAL")== null)?"":rs.getString("INTERNACIONAL");
						}
					}
					pst.close();
				
				} 
					
				if ("N".equals(sTipoAfiliado) || "A".equals(sTipoAfiliado)) {
				
					sQuery = " SELECT SC_TIPO_USUARIO " +
						" FROM seg_perfil "+
						" WHERE cc_perfil = ? ";

					PreparedStatement pst = con.queryPrecompilado(sQuery);
					pst.setString(1, oUsuario.getPerfil());
					ResultSet rs = pst.executeQuery();
					if (rs.next()) {
						iTipoPerfil = rs.getInt("SC_TIPO_USUARIO");
					}
					pst.close();
					}
				if ("N".equals(sTipoAfiliado)) {
					sNombreEmpresa = (iTipoPerfil == 4)?"NACIONAL FINANCIERA":"BANCOMEXT";
					iNoElectronico = 0;
				}
			
				com.netro.seguridadbean.Seguridad SeguridadBean = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
				System.out.println("iTipoPerfil  "+iTipoPerfil);
	
				listaPerfiles = SeguridadBean.consultarPerfiles(iTipoPerfil);
				
				informacion.put("EMPRESA",sNombreEmpresa);
				informacion.put("INTERNACIONAL",internacional);
				informacion.put("TIPOAFILIADO",sTipoAfiliado);
				informacion.put("NAELECTRONICO",String.valueOf(iNoElectronico));
				informacion.put("CLAVEAFILIADO",oUsuario.getClaveAfiliado());	
				informacion.put("NOMBRE_USUARIO",oUsuario.getNombre());
				informacion.put("APELLIDO_PATERNO",oUsuario.getApellidoPaterno());
				informacion.put("APELLIDO_MATERNO",oUsuario.getApellidoMaterno());
				informacion.put("EMAIL",oUsuario.getEmail());	
				informacion.put("PERFIL_ACTUAL",oUsuario.getPerfil());	
				informacion.put("LOGIN",txtLogin);
				informacion.put("PERFILES",listaPerfiles);
				
			}else  {
			 	mensaje =  "No se encontr� el usuario con la clave especificada, por favor vuelva a intentarlo.";
	    	
			}
			
			
			return informacion;					
		} catch(Exception e) {			
			e.printStackTrace();
			throw new Exception("SIST0001");
			
		} finally {
			if (con.hayConexionAbierta()) {			
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * 
	 * @throws java.lang.Exception
	 * @return 
	 * @param clave_usuario
	 * @param txtTipoAfiliado
	 * @param internacional
	 * @param txtNafinElectronico
	 * @param txtPerfilAnt
	 * @param txtPerfil
	 * @param txtLogin
	 */
	public String getConsultaUsuario(String txtLogin, String txtPerfil, 
				String txtPerfilAnt, String txtNafinElectronico , 
				String internacional, String txtTipoAfiliado, 			
				String clave_usuario )  throws Exception {
				
		AccesoDB con = new AccesoDB();
		UtilUsr 		oUtilUsr 		= new UtilUsr();
		Usuario oUsuario = new Usuario();
		boolean exito = true;
		
		String mensaje ="";
		List datosAnteriores = new ArrayList();
		List datosActuales = new ArrayList();
		StringBuffer valoresAnteriores = new StringBuffer();
		StringBuffer valoresActuales = new StringBuffer();
		String[] nombres = { "Perfil"};

		try {
			con.conexionDB();
			
			oUtilUsr = new UtilUsr();
			oUtilUsr.setPerfilUsuario(txtLogin, txtPerfil);

			datosAnteriores.add(0, txtLogin + " - " + txtPerfilAnt);
			datosActuales.add(0, txtLogin + " - " + txtPerfil);
			List cambios = Bitacora.getCambiosBitacora(datosAnteriores, datosActuales, nombres);
			valoresAnteriores.append(cambios.get(0));
			valoresActuales.append(cambios.get(1));
			
			Bitacora.grabarEnBitacora(con, "SEGCAMBIOPERFIL", txtTipoAfiliado+internacional,
					txtNafinElectronico, clave_usuario,
					valoresAnteriores.toString(), valoresActuales.toString());
					
			mensaje = "El cambio de perfil fue realizado con �xito.";	    
			
					
		} catch(Exception e) {
			exito = false;
			mensaje=e.toString();
			throw e;			
		} finally {
			if (con.hayConexionAbierta() == true)
			con.terminaTransaccion(exito);
			con.cierraConexionDB();
		}
		return mensaje;
	}
	
}