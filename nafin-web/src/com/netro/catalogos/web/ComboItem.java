package com.netro.catalogos.web;

import java.io.Serializable;

public class ComboItem implements Serializable
{

  private Object comboPK;
  private String comboValue;
  
  public ComboItem(Object comboPK, String comboValue)
  {
    this.comboPK = comboPK;
    this.comboValue = comboValue;
  }

  public String getValue()
  {
    return comboValue;
  }

  public void setValue(String comboValue)
  {
    this.comboValue = comboValue;
  }

  public Object getPK()
  {
    return comboPK;
  }

  public void setPK(Object comboPK)
  {
    this.comboPK = comboPK;
  }

  public String toString()
  {
    return "<option value=\"" + comboPK + "\">" + comboValue + "</option>";
  }

  public String toString(boolean selected)
  {
    if(selected)
      return "<option value=\"" + comboPK + "\" selected>" + comboValue + "</option>";
    else
      return "<option value=\"" + comboPK + "\">" + comboValue + "</option>";
  }  
}