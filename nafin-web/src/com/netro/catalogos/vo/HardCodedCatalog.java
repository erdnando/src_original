package com.netro.catalogos.vo;


public class HardCodedCatalog {
	
	private String action; 
 
	public String getAction() {
		return action;
	}
 
	public void setAction(String action) {
		this.action = action;
	}
 
}