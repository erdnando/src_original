package com.netro.catalogos.vo;

import java.io.Serializable;
import java.util.ArrayList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
	
public class DataSet implements Serializable {
  private ArrayList arDatos;
  private String strOrderByFields;
  
  public DataSet() {
    arDatos = new ArrayList();
  }

  public int getRowCount() {
    return arDatos.size();
  }

  public void addRow(DataRow row) {
    arDatos.add(row);
  }

  public DataRow getRow(int id) {
    return (DataRow)arDatos.get(id);
  }

  public String getOrderByFields() {
    return strOrderByFields;
  }

  public void setOrderByFields(String strOrderByFields) {
    this.strOrderByFields = strOrderByFields;
  }
 
  public JSONArray toJSON(){
	  
	 JSONArray 		result 		= new JSONArray();
	 JSONObject 	register 	= null;
    DataRow 		row 			= null;
    ArrayList 		fieldNames 	= null;
	 
	 if(arDatos.size() > 0) {
		 
		 for(int i = 0; i < arDatos.size(); i++) {
        row = (DataRow)arDatos.get(i);
        if(fieldNames == null) {
          fieldNames = row.getFieldNames();
        }
        
        register = new JSONObject();
        for(int j = 0; j < fieldNames.size(); j++) {
            register.put(fieldNames.get(j),row.getData((String)fieldNames.get(j)));
        }
        
        register.put("ROWNUM",String.valueOf(i));
        result.add(register);
      }
	 }
	 
	 return result;
	 
  }
  
  /** TODO: No sirve para muchos registros, todos los datos se presentan como String **/
  public String toJSONString() {
    String result = "";
    DataRow row = null;
    ArrayList fieldNames = null;
    
    if(arDatos.size() <= 0) {
      result = "[]";  
    } else {
      result = "[";
      for(int i = 0; i < arDatos.size(); i++) {
        row = (DataRow)arDatos.get(i);
        if(fieldNames == null) {
          fieldNames = row.getFieldNames();
        }
        
        result += "{";
        
        for(int j = 0; j < fieldNames.size(); j++) {
          result += "\""+fieldNames.get(j)+"\":\""+row.getData((String)fieldNames.get(j))+"\"";
          if(j != fieldNames.size()-1) result += ",";
        }
        
        result += ",\"ROWNUM\":"+i+"}";
        if(i != arDatos.size()-1) result += ",";
      }
      result += "]";
    }
    
    return result;
  }
  
  public void close() {
    for(int i = 0; i < arDatos.size(); i++) {
      ((DataRow)arDatos.get(i)).close();
    }
    
    arDatos.clear();
    arDatos = null;
  }
}