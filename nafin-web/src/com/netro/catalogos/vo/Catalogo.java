package com.netro.catalogos.vo;

import java.math.BigDecimal;

import java.util.ArrayList;

public class Catalogo {
	
  private   BigDecimal  tableId;
  private   String      primaryKey;
  private   String      sequenceName;
  private   String      tableName         = "";
  private   ArrayList   fields            = new ArrayList();
  private   boolean     useSequence;  
  private   String      defaultOrderBy;
  private   int         registros_pagina;
  private   int         nivelFK;
  public    boolean     haveForeignKeys   = false;
  private   String      descripcion;
  private   String      orderType         = "ASC";
  private   String      fieldAlias;
  private   String      selectBody;
  private   String      insertBody;
  private   String      updateBody;
  private   String      deleteBody;
  private 	boolean 		incrementKey 						= false;
  private	boolean 		hasUserModificationField 		= false;
  private	boolean 		hasUserModificationDateField 	= false;
  private   ArrayList   queryConditions        			= new ArrayList();
  private   String      perfilCatalogo						= null; // IMPORTANTE: Este parametro nunca se debe Especificar en el XML
  
  public Catalogo(BigDecimal tableId, String tableName, String descripcion){ // tableId en realidad es el id del catalogo
    this.tableId        = tableId;
    this.tableName      = tableName;
    this.descripcion    = descripcion;
  }

  public String toString(){
    return tableName;
  }
  
  public int getFKLevel(){
    return nivelFK;  
  }
  
  public void setFKLevel(int nivelFK){
    this.nivelFK = nivelFK;
  }
  
  public BigDecimal getTableId(){
    return tableId;  
  }

  public void setTableId(BigDecimal tableId){
    this.tableId = tableId;
  }
  
  public String getTableName()
  {
    return tableName;
  }

  public void setTableName(String tableName){
    this.tableName = tableName;
  }

  public ArrayList getFields(){
    return fields;
  }

  public void setFields(ArrayList fields){
    this.fields = fields;
  }

  public Campo getField(int item){
    return (Campo)fields.get(item);
  }

  public Campo getField(String name){
    Campo campo = null;
    
    for(int i = 0; i < fields.size(); i++)
    {
      if(((Campo)fields.get(i)).getFieldName().equals(name)) {
        campo = (Campo)fields.get(i);
      }
    }
    
    return campo;
  }
  
  public String getPrimaryKeyName(){
    return primaryKey;
  }

  public void setPrimaryKeyName(String primaryKey){
    this.primaryKey = primaryKey;
  }

  public String getSequenceName(){
    return sequenceName;
  }

  public void setSequenceName(String sequenceName){
    this.sequenceName = sequenceName;
  }
 
  public boolean getIncrementKey() {
  	  return this.incrementKey;
  }
 
  public void setIncrementKey(boolean incrementKey){
  	  this.incrementKey = incrementKey;
  }

  public boolean useSequence(){
    return !sequenceName.trim().equals("");
  }

  public void addField(Campo campo){
    fields.add(campo);
  }

  public int getFieldCount(){
    return fields.size();
  }

  public String getDefaultOrderBy(){
    return defaultOrderBy;
  }

  public void setDefaultOrderBy(String defaultOrderBy){
    this.defaultOrderBy = defaultOrderBy;
  }

  public int getRegistrosXpagina(){
    return registros_pagina;
  }

  public void setRegistrosXpagina(int registros_pagina){
    this.registros_pagina = registros_pagina;
  }

  public String getDescripcion(){
    return descripcion;
  }

  public void setDescripcion(String descripcion){
    this.descripcion = descripcion;
  }

  public String getOrderByType(){
    return orderType;
  }

  public void setOrderByType(String orderType){
    this.orderType = orderType;
  }

  public String getSelectBody(){
    return selectBody;
  }

  public void setSelectBody(String selectBody){
    this.selectBody = selectBody;
  }

  public String getInsertBody(){
    return insertBody;
  }

  public void setInsertBody(String insertBody){
    this.insertBody = insertBody;
  }

  public String getUpdateBody(){
    return updateBody;
  }

  public void setUpdateBody(String updateBody){
    this.updateBody = updateBody;
  }

  public String getDeleteBody(){
    return deleteBody;
  }

  public void setDeleteBody(String deleteBody){
    this.deleteBody = deleteBody;
  }

  public String getFieldAlias(){
    return fieldAlias;
  }

  public void setFieldAlias(String fieldAlias) {
    this.fieldAlias = fieldAlias;
  }
 
  public  boolean getHasUserModificationField() {
		return hasUserModificationField;
  }
 
  public void setHasUserModificationField( boolean hasUserModificationField) {
  	  this.hasUserModificationField = hasUserModificationField;
  }

  public boolean getHasUserModificationDateField() {
  	  return hasUserModificationDateField;
  }

  public void setHasUserModificationDateField( boolean hasUserModificationDateField) {
  	  this.hasUserModificationDateField = hasUserModificationDateField;
  }

  public ArrayList getQueryConditions(){
    return queryConditions;
  }

  public void setQueryConditions(ArrayList queryConditions){
    this.queryConditions = queryConditions;
  }

  public QueryCondition getQueryCondition(int item){
    return (QueryCondition)queryConditions.get(item);
  }

  public QueryCondition getQueryCondition(String name){
    QueryCondition condition = null;
    
    for(int i = 0; i < queryConditions.size(); i++){
      if(((QueryCondition)queryConditions.get(i)).getName().equalsIgnoreCase(name)) {
        condition = (QueryCondition)queryConditions.get(i);
        break;
      }
    }
    
    return condition;
  }
  
  public void addQueryCondition(QueryCondition condition){
    queryConditions.add(condition);
  }

  public int getQueryConditionCount(){
    return queryConditions.size();
  }
  
  /* IMPORTANTE: Este parametro nunca se debe especificar en el xml */
  public String getPerfilCatalogo() {
  	  return perfilCatalogo;
  }
 
  /* IMPORTANTE: Este parametro nunca se debe especificar en el xml */
  public void setPerfilCatalogo(String perfilCatalogo) {
  	  this.perfilCatalogo = perfilCatalogo;
  }
  
  public boolean containsQueryConditionColumn( String columnName ){
  	  
  	  boolean found 					= false;
  	  
  	  for(int m = 0; m < this.getQueryConditionCount(); m++){
    	 
		  QueryCondition queryCondition 	= this.getQueryCondition(m);
		  
		  // Determinar si el perfil del usuario tiene permiso de usar esta condición del query
		  if( queryCondition.skipCondition( this.getPerfilCatalogo() ) ){
		  	  continue;
        }
			
		  // Determinar si la columna del query codition coincide con la columna especificada a buscar
		  if( queryCondition.getColumnName().equalsIgnoreCase( columnName ) ){
				found = true;
				break;
		  }
			
     }
     
     return found;
     
  }

  
}