package com.netro.catalogos.vo;

import java.math.BigDecimal;

import java.util.ArrayList;

public class Campo
{
  private String fieldName;
  public boolean isVisible = false;
  private String friendlyName;
  private Object type;
  private Integer maxLength;
  public boolean isNullable = false;
  public boolean isForeignKey = false;
  public boolean isHardCodedCatalog = false;
  public boolean canSearch = false;
  public boolean isReadOnly = false;
  private Object defaulValue;
  private ForeignKey foreignKey;
  private HardCodedCatalog hardCodedCatalog;
  private ArrayList allowedValues = new ArrayList();
  private String searchType = "";
  private String format = "";
  public boolean isFile = false;
  private String rutaFile;
  public boolean displayAsLink = false;
  public boolean showLink = false;
  private String width = "";
  private String height = "";
  private String alias = "";
  public boolean toUpperCase = false;
  public boolean showEdit = true;
  public boolean useOnUpdate = true;
  private String requiredMessage = "";
  public boolean isAutoExpand = false;
  private String valitadionType = "";
  private String 		numberFormat  	= "0";
  private BigDecimal	numberMinValue = null;
  private BigDecimal numberMaxValue = null;
  private Integer		gridWidth 		= null;
  private String 		dateFormat 		= null; 
  
  public Campo(String fieldName)
  {
    this.fieldName = fieldName;
  }

  public Campo(String fieldName, boolean isVisible, String friendlyName, Object type, int maxLength, boolean isNullable, boolean canSearch, Object defaulValue)
  {
    this.fieldName = fieldName;
    this.isVisible = isVisible;
    this.friendlyName = friendlyName;
    this.type = type;
    this.maxLength = new Integer(maxLength);
    this.isNullable = isNullable;
    this.canSearch = canSearch;
    this.defaulValue = defaulValue;
  }  

  public String getFieldName()
  {
    return fieldName;
  }

  public void setFieldName(String fieldName)
  {
    this.fieldName = fieldName;
  }

  public String getFriendlyName()
  {
    return friendlyName;
  }

  public void setFriendlyName(String friendlyName)
  {
    this.friendlyName = friendlyName;
  }

  public Object getType()
  {
    return type;
  }

  public void setType(String dbType)
  {
    if(dbType.trim().equalsIgnoreCase("VARCHAR2")) {
      type = new String("");
    } 

    if(dbType.trim().equalsIgnoreCase("CHAR")) {
      type = new String("");
    }

    if(dbType.trim().equalsIgnoreCase("NUMBER")) {
      type = new BigDecimal(0);
    }

    if(dbType.trim().equalsIgnoreCase("DATE")) {
      type = new java.sql.Date(System.currentTimeMillis());
    }    
  }

  public Integer getMaxLength()
  {
    return maxLength;
  }

  public void setMaxLength(Integer maxLength)
  {
    this.maxLength = maxLength;
  }

  public Object getDefaulValue()
  {
    return defaulValue;
  }

  public void setDefaultValue(String strDefaultValue)
  {
    try {
      if(type instanceof String) {
        defaulValue = strDefaultValue;
      } else if(type instanceof BigDecimal) {
        defaulValue = new BigDecimal(strDefaultValue);
      } else if(type instanceof java.sql.Date) {
        defaulValue = strDefaultValue;
      } else {
        defaulValue = strDefaultValue;
      }
    } catch(Exception e) {}
  }

  public ForeignKey getForeignKey()
  {
    return foreignKey;
  }

  public void setForeignKey(ForeignKey foreignKey)
  {
    this.foreignKey = foreignKey;
  }

  public ArrayList getAllowedValues()
  {
    return allowedValues;
  }

  public void setAllowedValues(ArrayList allowedValues)
  {
    this.allowedValues = allowedValues;
  }

  public void addAllowedValue(AllowedValue newAllowedValue)
  {
    allowedValues.add(newAllowedValue);
  }

  public AllowedValue getAllowedValue(int item)
  {
    return (AllowedValue)allowedValues.get(item);
  }
  
  public void setCanSearch(String strCansearch)
  {
    try {
      if(strCansearch.indexOf(":") > -1)
      {
        canSearch = (new Boolean(strCansearch.substring(0, strCansearch.indexOf(":")))).booleanValue();
        searchType = strCansearch.substring(strCansearch.indexOf(":") + 1, strCansearch.length());
      }
      else
      {
        canSearch = (new Boolean(strCansearch)).booleanValue();
        searchType = "=";
      }
    } catch(Exception e) {}
  }

  public String getSearchType()
  {
    return searchType;
  }

  public String getFormat()
  {
    return format;
  }

  public void setFormat(String format)
  {
    this.format = format;
  }

  public String getRuta()
  {
    return rutaFile;
  }

  public void setRuta(String rutaFile)
  {
    isFile = !rutaFile.trim().equals("");
    this.rutaFile = rutaFile;
  }

  public String getFileWidth()
  {
    return width;
  }

  public void setFileWidth(String width)
  {
    this.width = width;
  }

  public String getFileHeight()
  {
    return height;
  }

  public void setFileHeight(String height)
  {
    this.height = height;
  }

  public void setDimension(String dimension)
  {
    int firstComma = dimension.indexOf(",");
    if(firstComma == -1) firstComma = dimension.length();
    
    try { width = dimension.substring(0, firstComma); } catch(Exception e) {}
    try { height = dimension.substring(firstComma + 1); } catch(Exception e) {}    
  }

  public String getAlias()
  {
    return alias;
  }

  public void setAlias(String alias)
  {
    this.alias = alias;
  }
  
  public String getRequiredMessage(){
	  return requiredMessage;
  }
  
  public void setRequiredMessage(String requiredMessage){
	  this.requiredMessage = requiredMessage;
  }
 
  public String getValitadionType(){
		return this.valitadionType;
  }

  public void setValitadionType(String valitadionType){
		this.valitadionType = valitadionType;
  }
 
  public String getNumberFormat() {

  	  return numberFormat;

  }

 
  public void setNumberFormat(String numberFormat) {

  	  this.numberFormat = numberFormat;

  }

 
  public BigDecimal getNumberMinValue() {

  	  return this.numberMinValue;

  }

 
  public void setNumberMinValue( String numberMinValue) {

  	  if ( numberMinValue != null && !numberMinValue.trim().equals("")){
  	  	  this.numberMinValue = new BigDecimal(numberMinValue);

  	  } else {
  	  	  this.numberMinValue = null;  
  	  }
  }

 
	public BigDecimal getNumberMaxValue() {

		return this.numberMaxValue;

	}

 
	public void setNumberMaxValue(String numberMaxValue) {

		if( numberMaxValue != null && !numberMaxValue.trim().equals("") ){
			this.numberMaxValue = new BigDecimal(numberMaxValue);

		} else {
			this.numberMaxValue = null;

		}
	}
 
	public Integer getGridWidth() {
		return this.gridWidth;
	}
 
	public void setGridWidth( Integer gridWidth ) {
		this.gridWidth = gridWidth;
	}
 
	public HardCodedCatalog getHardCodedCatalog(){
		return hardCodedCatalog;
	}
 
	public void setHardCodedCatalog(HardCodedCatalog hardCodedCatalog) {
		this.hardCodedCatalog = hardCodedCatalog;
	}
 
	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
 
}