package com.netro.catalogos.vo;


public class AllowedValue 
{
  private String displayName;
  private String value;
  
  public AllowedValue(String displayName, String value)
  {
    this.displayName = displayName;
    this.value = value;
  }

  public String getValue()
  {
    return value;
  }

  public void setValue(String value)
  {
    this.value = value;
  }

  public String getName()
  {
    return displayName;
  }

  public void setName(String displayName)
  {
    this.displayName = displayName;
  }

  public String toString()
  {
    return displayName;
  }
}