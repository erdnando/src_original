package com.netro.catalogos.vo;

import java.util.ArrayList;

import netropology.utilerias.AppException;

public class QueryCondition {
	
	public  static final int NO_VALUE_TYPE 	= 100;
	public  static final int SESSION 			= 101; 
	public  static final int REQUEST 			= 102; // NO SOPORTADO ACTUALMENTE
	public  static final int LOGICAL 			= 103; // NO SOPORTADO ACTUALMENTE
	public  static final int VALUE 				= 104; // NO SOPORTADO ACTUALMENTE
	
	public  static final int NO_PROFILE_MODE 	= 200;
	public  static final int INCLUDE 			= 201;
	public  static final int EXCLUDE 			= 202; // NO SOPORTADO ACTUALMENTE
 
	private String 	name							= null;
	private int 		valueType					= QueryCondition.NO_VALUE_TYPE;
	private String 	value							= null;
	private String    tableName     				= null;
	private int 		profileMode					= QueryCondition.NO_PROFILE_MODE;
	private ArrayList catalogProfiles			= new ArrayList();
	private String    columnName					= null;
	
	public String getName() {
		return name;
	}
 
	public void setName(String name) {
		this.name = name;
	}
 
	public int getValueType() {
		return valueType;
	}
 
	public void setValueType(String valueType) {
 
		if(			"SESSION".equalsIgnoreCase(valueType)	){
			this.valueType = QueryCondition.SESSION;
		} else if(	"REQUEST".equalsIgnoreCase(valueType)	){
			this.valueType = QueryCondition.REQUEST;
			throw new AppException("Caracteristica no soportada en esta versi�n");
		} else if(	"LOGICAL".equalsIgnoreCase(valueType)	){
			this.valueType = QueryCondition.LOGICAL;
			throw new AppException("Caracteristica no soportada en esta versi�n");
		} else if(	"VALUE".equalsIgnoreCase(valueType)	){
			this.valueType = QueryCondition.VALUE;
			throw new AppException("Caracteristica no soportada en esta versi�n");
		} else {
			this.valueType = QueryCondition.NO_VALUE_TYPE;
		}

	}
 
	public void setValueType(int valueType){
 
		if(			QueryCondition.SESSION == valueType	){
			this.valueType = QueryCondition.SESSION;
		} else if(	QueryCondition.REQUEST == valueType	){
			this.valueType = QueryCondition.REQUEST;
			throw new AppException("Caracteristica no soportada en esta versi�n");
		} else if(	QueryCondition.LOGICAL == valueType	){
			this.valueType = QueryCondition.LOGICAL;
			throw new AppException("Caracteristica no soportada en esta versi�n");
		} else if(	QueryCondition.VALUE   == valueType	){
			this.valueType = QueryCondition.VALUE;
			throw new AppException("Caracteristica no soportada en esta versi�n");
		} else {
			this.valueType = QueryCondition.NO_VALUE_TYPE;
		}
		
	}
 
	public String getValue(){
		return value;
	}
 
	public void setValue(String value){
		this.value = value;
	}
 
	public String getTableName(){
		return tableName;
	}
 
	public void setTableName(String tableName){
		this.tableName = tableName;
	}
 
	public int getProfileMode() {

		return profileMode;

	}

 
	public void setProfileMode(String profileMode){

		
		if( "INCLUDE".equalsIgnoreCase(profileMode) ) {
			this.profileMode = QueryCondition.INCLUDE;

		} else if ( "EXCLUDE".equalsIgnoreCase(profileMode) ) {
			this.profileMode = QueryCondition.EXCLUDE;

			throw new AppException("Caracteristica no soportada en esta versi�n");
		} else {
			this.profileMode = QueryCondition.NO_PROFILE_MODE;

		}
		
	}

	
	public void setProfileMode(int profileMode) {

		
		if( 			QueryCondition.INCLUDE == profileMode ) {
			this.profileMode = QueryCondition.INCLUDE;

		} else if ( QueryCondition.EXCLUDE == profileMode ) {
			this.profileMode = QueryCondition.EXCLUDE;

			throw new AppException("Caracteristica no soportada en esta versi�n");
		} else {
			this.profileMode = QueryCondition.NO_PROFILE_MODE;

		}
		
	}

	
	public ArrayList getCatalogProfiles(){
		return catalogProfiles;
	}

	public void setCatalogProfiles(ArrayList catalogProfiles){
		this.catalogProfiles = catalogProfiles;
	}

	public void addCatalogProfile(String catalogProfileName){
		catalogProfiles.add(catalogProfileName);
	}

	public String getCatalogProfile(int item){
		return (String)catalogProfiles.get(item);
	}
  
	public int getCatalogProfilesCount(){
		return catalogProfiles.size();
	}
 
	public String getColumnName() {
		return columnName;
	}
 
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
 
	public boolean searchCatalogProfile(String catalogProfileName){
		boolean found = false;
		for(int i=0;i<catalogProfiles.size();i++){
			String profileName = (String) catalogProfiles.get(i);
			if( profileName.equals(catalogProfileName) ){
				found = true;
				break;
			}
		}
		return found;
	}
	
	public boolean skipCondition(String perfilCatalogo){
		
		boolean skipQueryCondition						= false;
		  
		// Determinar si el perfil del usuario tiene permiso de usar esta condici�n del query
		if( QueryCondition.INCLUDE 					== this.getProfileMode() ){
 
		   // Si no se especifico ningun perfil en especifico, significa que esta 
		   // condicion aplica para todos los perfiles
		   if( this.getCatalogProfilesCount() == 0 ){
		  	   	
		   	skipQueryCondition 		= false;
		  	   
		   // Como se especificaron perfiles, esta condicion se debe aplicar SOLO para los 
		   // perfiles especificados
		   } else {
		  	   	
		   	boolean includeCondition = this.searchCatalogProfile( perfilCatalogo );
				if( !includeCondition){
					skipQueryCondition 	= true;
				}
					
			}
 
		} else if( QueryCondition.EXCLUDE 			== this.getProfileMode() ){
				
			throw new AppException("Actualmente esta opci�n no se encunetra soportada");
				
		}
		
		return skipQueryCondition;
			
	}
 
}