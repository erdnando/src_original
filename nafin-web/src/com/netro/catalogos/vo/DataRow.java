package com.netro.catalogos.vo;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

public class DataRow implements Serializable {
  private Map dataRow;
  private ArrayList fields;
  
  public DataRow() {
    dataRow = Collections.synchronizedMap(new HashMap());
    fields = new ArrayList();
  }

  public void addData(String key, Object value) {
    dataRow.put(key, value);
    fields.add(key);
  }

  public Object getData(String key) {
    return dataRow.get(key);
  }

  public String getString(String key) {
    return (String)dataRow.get(key);
  }

  public int getFieldCount() {
    return fields.size();
  }

  public ArrayList getFieldNames() {
    return fields;
  }
  
  public void close() {
    if(dataRow != null) dataRow.clear();
    if(fields != null) fields.clear();
    
    dataRow = null;
    fields = null;
  }
  
  public JSONObject toJSON(){
  
     JSONObject respuesta = new JSONObject();
     
     String key   = null;
     Object value = null;
     
     for(int i=0;i<fields.size();i++){
     
         key   = (String) fields.get(i);
         value = dataRow.get(key);
         
         respuesta.put(key,value == null?"":value.toString());
     }
     
     return respuesta;
     
  }
  
}