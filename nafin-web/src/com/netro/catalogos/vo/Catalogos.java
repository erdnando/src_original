package com.netro.catalogos.vo;

import java.io.Serializable;

import java.math.BigDecimal;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Catalogos implements Serializable
{

  private Map tablas = null;
  //private Map tablasId = null;    
  
  public Catalogos()
  {
    tablas = Collections.synchronizedMap(new HashMap());
    //tablasId = Collections.synchronizedMap(new HashMap());
  }

  public void addTable(Catalogo tabla)
  {
    tablas.put(tabla.getTableName(), tabla);
    //tablasId.put(tabla.getTableId(), tabla);
  }

  public Catalogo getTable(String key)
  {
    return (Catalogo)tablas.get(key);
  }

  public Catalogo getTable(BigDecimal id)
  {
    //return (Tabla)tablasId.get(id);
    Iterator it = tablas.values().iterator();

    while(it.hasNext())
    {
      Catalogo tabla = (Catalogo)it.next();
      if(tabla.getTableId().equals(id.toPlainString()))
      {
        return tabla;
      }
    }

    return null;
  }
  
  public int getTableCount()
  {
    return tablas.size();
  }
}