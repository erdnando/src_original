package com.netro.catalogos.vo;


public class ForeignKey 
{
  private String foreignTable;
  private String keyName;
  private String selectField;
  private String fieldAlias;
  private String in;
  
  public ForeignKey(String foreignTable, String keyName, String selectField, String fieldAlias ) {
    this( foreignTable, keyName, selectField, fieldAlias, "");
  }
  
  public ForeignKey(String foreignTable, String keyName, String selectField, String fieldAlias, String in) {
    this.foreignTable = foreignTable;
    this.keyName      = keyName;
    this.selectField  = selectField;
    this.fieldAlias   = fieldAlias;
    this.in           = in;
  }

  public String getTable()
  {
    return foreignTable;
  }

  public void setForeignTable(String foreignTable)
  {
    this.foreignTable = foreignTable;
  }

  public String getFk()
  {
    return keyName;
  }

  public void setFK(String keyName)
  {
    this.keyName = keyName;
  }

  public String getShowField()
  {
    return selectField;
  }

  public void setShowField(String selectField)
  {
    this.selectField = selectField;
  }


  public void setFieldAlias(String fieldAlias) {
    this.fieldAlias = fieldAlias;
  }


  public String getFieldAlias() {
    return fieldAlias;
  }
 
  public String getIn() {

    return in;

  }


 
  public void setIn(String in) {

    this.in = in;

  }

 
}