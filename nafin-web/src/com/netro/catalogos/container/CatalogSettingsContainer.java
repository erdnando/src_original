package com.netro.catalogos.container;

import com.netro.catalogos.vo.AllowedValue;
import com.netro.catalogos.vo.Campo;
import com.netro.catalogos.vo.Catalogo;
import com.netro.catalogos.vo.ForeignKey;
import com.netro.catalogos.vo.HardCodedCatalog;
import com.netro.catalogos.vo.QueryCondition;

import java.io.File;
import java.io.IOException;

import java.math.BigDecimal;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;

public class CatalogSettingsContainer {
	
  private final static Log log 						= ServiceLocator.getInstance().getLog(CatalogSettingsContainer.class);
  private String 				contextRootRealPath	= null;
  
  public CatalogSettingsContainer(String contextRootRealPath){
  	  this.contextRootRealPath = contextRootRealPath;
  }
 
  public Catalogo getCatalogo(String claveCatalogo, String nombreArchivo) {
 
  	 log.info("getCatalogo(E)");
  	 
    DocumentBuilder 	builder 	= null;
    Document 			document = null;
    boolean				exito		= true;
    Catalogo         catalogo = null;
    
    log.debug("Cargando configuraci�n del cat�logo: " + nombreArchivo );
    
    try {
    	 
    	// Leer archivo de configuracion del catalogo
    	File catalogConfigFile	= new java.io.File( this.contextRootRealPath + "/WEB-INF/config/CatalogosParametrizacion/" + nombreArchivo );
    
    	// Parsear contenido del catalogo
      builder 						= DocumentBuilderFactory.newInstance().newDocumentBuilder();
      document 					= builder.parse(catalogConfigFile);
      NodeList list 				= document.getElementsByTagName("catalogo");

      if( list.getLength() > 0 ){
      	
        // LEER CATALOGO
        Node nodo = list.item(0);  
        catalogo  = new Catalogo(
        	 new BigDecimal(
        	   getValue(nodo, "id", 				"0")	// Como aparece en COMCAT_CATALOGOS_PARAM.IC_CATALOGOS_PARAM
        	 ),
        	  	getValue(nodo, "table", 			""	), // Nombre de la tabla asociada al catalogo
        	  	getValue(nodo, "descripcion", 	""	)	// Comentarios sobre el catalogo 
        );
 
        // Validar que la clave del catalogo configurada en el xml coincida con la registrada en la base de datos
        if( !claveCatalogo.equals( catalogo.getTableId().toString() )){
        	  throw new AppException("El \"id\" del cat�logo no coincide con el registrado en la tabla: COMCAT_CATALOGOS_PARAM. Se aborta la lectura del cat�logo.");
        }
        
        // 1. LEER PROPIEDADES DEL CATALOGO
        catalogo.setFieldAlias(getValue(nodo, 	"field-alias", ""));
        // Leer el nivel Foreign Key
        catalogo.setFKLevel( Integer.parseInt(getValue(nodo, "fk-level", "1")) );
        
        // 2. LEER LLAVE PRIMARIA
        Node pk = getNodeByTagName(nodo.getChildNodes(),"primary-key");
        if( pk == null ){
           throw new AppException("El atributo \"primary-key\", no se encuentra definido. Se aborta la lectura del cat�logo.");
        }
        
        catalogo.setPrimaryKeyName(		getValue(pk, "name", 			"")		);
        if( catalogo.getPrimaryKeyName().indexOf(",") != -1 ){
        	  throw new AppException("El atributo \"primary-key\", no puede tener mas de una llave. Se aborta la lectura del cat�logo.");
        }
        
        catalogo.setSequenceName(		getValue(pk, "sequence", 		"")		);
		  catalogo.setIncrementKey( 		getValue(pk, "increment-key", false)	);
 
		  if (  !"".equals ( catalogo.getSequenceName().trim() ) && catalogo.getIncrementKey() ){
		  	  throw new AppException("El atributo \"primary-key\", tiene definidas ambas propiedades: sequence y increment-key, pero s�lo se permite una. Se aborta la lectura del cat�logo.");
		  } 
		  
		  if (  "".equals ( catalogo.getSequenceName().trim() )  && !catalogo.getIncrementKey() ){
		  	  throw new AppException("El atributo \"primary-key\", requiere que se tenga definida s�lo una de las siguientes propiedades: sequence � increment-key. Se aborta la lectura del cat�logo.");
		  }
		  	  
        // 3. LEER CAMPOS ASOCIADOS AL CATALOGO
        Node     fields = getNodeByTagName(nodo.getChildNodes(),"fields");
        if( fields == null ){
           throw new AppException("El atributo \"fields\", no se encuentra definido. Se aborta la lectura del cat�logo.");
        }
        NodeList campos = fields.getChildNodes();
        
        for(int j = 0,ctaCampos = 0; j < campos.getLength(); j++){
 
          if( j % 2 == 0 ) continue;
          ctaCampos++;
          
        	 String strName = null;
        	 
          try {

            strName = getValue(campos, j, "name", "");
            
            if("DULTIMA_MODIFICACION".equalsIgnoreCase(strName)){
            	catalogo.setHasUserModificationDateField(true);
            }
            
            if("VUSUARIO_ULT_MODIF".equalsIgnoreCase(strName)){
            	catalogo.setHasUserModificationField(true);
            }
            
            if(!strName.trim().equals("")){
            	
              Campo campo = new Campo(strName);

              // 1. Leer propiedad que indica si el campo es visible
              campo.isVisible 		= (new Boolean(
              	  								getValue(campos, 	j, "visible", 				"true")	)
              ).booleanValue();
              
              // 2. Obtener nombre amigable del campo
              campo.setFriendlyName(	getValue(campos, 	j, "display-name", 		"")		);
              
              // 3. Leer tipo de dato que almacena el campo
              campo.setType(				getValue(campos, 	j, "type", 					"")		);
              
              if(          campo.getType() instanceof java.math.BigDecimal    ){
              	  campo.setNumberFormat(			getValue(campos, 	j, "number-format", 		"0")			);
              	  campo.setNumberMinValue(			getValue(campos, 	j, "number-min-value", 	"" )			);
              	  campo.setNumberMaxValue(			getValue(campos, 	j, "number-max-value", 	"" )			);
              } else if (  campo.getType() instanceof java.sql.Date ){
              	  campo.setDateFormat(				getValue(campos, 	j, "date-format", 		"d/m/Y")		);
              }
              // 4. Leer maxima longitud del campo
              campo.setMaxLength(new Integer(
              	  								getValue(campos,  j, "max-length", 			"0")		)
              );
              
              // 5. Leer propiedad que indica si el campo puede aceptar valores nulos
              campo.isNullable 		= (new Boolean(
              	  								getValue(campos, 	j, "accept-null", 		"false")	)
              ).booleanValue();
              
              // 6. Leer propiedad que indica si el campo pertenece a una llave for�nea
              campo.isForeignKey 	= (new Boolean(
              	  								getValue(campos, 	j, "foreign-key", 		"false")	)
              ).booleanValue();
              
              // 7. Leer propiedad que indica si el campo puede utilizarse como como campo de busqueda
              campo.setCanSearch(		getValue(campos, 	j, "can-search", 			"")		);
              
              // 8. Leer propiedad que indica si el campo es de solo lectura
              campo.isReadOnly 		= (new Boolean(
              	  								getValue(campos, 	j, "read-only", 			"false")	)
              ).booleanValue();
              
              // 9. Leer propiedad de formato del campo...este generalmente es ocupado para la base de datos
              campo.setFormat(			getValue(campos, 	j, "format", 				"")		);
              
              // 10. Leer valor default del campo
              campo.setDefaultValue(getValue(campos, 		j, "default", 				"")		);
              
              // 11. Leer ruta de archivo... 
              // Nota: En esta version, esta caracteristica no se encuentra soportada
              campo.setRuta(				getValue(campos, 	j, "file-path", 			"")		);
              
              // 12. Mostrar campo como si fuera una liga
              // Nota: En esta version, esta caracteristica no se encuentra soportada
              campo.displayAsLink 	= (new Boolean(
              	  								getValue(campos, j, "displayAsLink", 		"false")	)
              ).booleanValue();
              
              // 13. Mostrar liga
              // Nota: En esta version, esta caracteristica no se encuentra soportada
              campo.showLink 			= (new Boolean(
              	  								getValue(campos, j, "showLink", 				"false")	)
              ).booleanValue();
              
              // 14. Leer tama�o en pantalla ... para esta versi�n, esta opci�n no se encuentra soportada
              campo.setDimension(		getValue(campos, j, "display-size", 		"")		);
              
              // 15. Leer alias del campo
              campo.setAlias(				getValue(campos, j, "field-alias", 			"")		);
              
              // 16. Indicar si el contenido capturado en el campo, siempre ser� convertido a may�sculas
              campo.toUpperCase 		= (new Boolean(
              	  								getValue(campos, j, "to-upper-case",		"false")	)
              ).booleanValue();
              
              // 17. Este campo sirve para mostrar el RowEditor ... siempre debe ir en true
              // Si no se desea se muestre el row editor poner la opcion read-only en true.
              campo.showEdit 			= (new Boolean(
              	  								getValue(campos, j, "showEdit",				"true")	)
              ).booleanValue();
              
              // 18. Usar este campo cuando se actualice el campo
              campo.useOnUpdate 		= (new Boolean(
              	  								getValue(campos, j, "useOnUpdate",			"true")	)
              ).booleanValue();
              
              // 19. Mensaje a mostrar cuando el campo sea requerido
              campo.setRequiredMessage(getValue(campos, j, "required_message", 	"")  	  	);
              
              // 20. Leer propiedad que indica en el grid si el campo se autoexpandira para cubrir todo el 
              // espacio restante de la columnas
				  campo.isAutoExpand 	= (new Boolean(
				  	  								getValue(campos, j, "auto-expand",			"false")	)
				  ).booleanValue();
				  
				  // 21. Leer longitud del campo en el grid-panel
				  campo.setGridWidth( new Integer(
				  	  								getValue(campos,  j, "grid-width", 			"100")	)
				  );
				  
				  // 22. Leer tipo de validacion que se aplicara al campo cuando este se edite en el grid panel
				  campo.setValitadionType(	getValue(campos, j, "validation-type", 	"")		);
				  
				  // 23. Si el campo corresponde a una llave foranea
              if(campo.isForeignKey){
              	  
                catalogo.haveForeignKeys = true;
                Node fk = campos.item(j).getChildNodes().item(1);
                campo.setForeignKey(new ForeignKey(
                  getValue(fk, "name", 			""), 	// Leer nombre de la tabla a la que hace referencia este campo
                  getValue(fk, "field-name", 	""), 	// Leer nombre del campo en la tabla foranea
                  getValue(fk, "select-field", 	""), 	// Leer nombre del campo que describe a la llave foranea utilizada
                  getValue(fk, "field-alias", 	""),	// Leer alias del campo foraneo
                  getValue(fk, "in", 				"")   // Lista separada por comas, con las claves permitidas del catalogo
                ));
                
              }
          
              // 24. Leer valores restringidos para el campo
              if(campos.item(j).hasChildNodes()){ 
              
              	 // Leer lista de valores restringidos
                NodeList allowedValues = campos.item(j).getChildNodes();
                int      numeroNodo    = 0;
                for(int k = 0; k < allowedValues.getLength(); k++){
                	 
                  String nodeName = allowedValues.item(k).getNodeName();
                  
                  if( !"allowed".equals(nodeName) ){
                     continue;
                  } else {
                     numeroNodo++;
                  }
                  
                  try {
                  
                  	// Leer descripcion del valor permitido	
                  	String strDisplay 	= getValue(allowedValues, k, "display", 	"");
                  	// Leer valor permitido
                   	String strValue 		= getValue(allowedValues, k, "value", 		"");

                    if(!strDisplay.trim().equals("") && !strValue.trim().equals("")) {
                      AllowedValue allowedValue = new AllowedValue(strDisplay, strValue);
                      campo.addAllowedValue(allowedValue);
                    } else {
                    	 log.debug("Campo "+strName+": El registro \"allowed\" numero "+ numeroNodo +" ha sido descartado por tener elementos vac�os.");  
                    }
                    
                  } catch(Exception ex) {}
                  
                }
                
              }
              
              // 25. Determinar si es un catalogo harcodeado
              campo.isHardCodedCatalog 	= (new Boolean(
              	  								getValue(campos, 	j, "hard-coded-catalog", 		"false")	)
              ).booleanValue();
              
              if( campo.isHardCodedCatalog  ){
              	  
              	  Node hardcodedCatalog = campos.item(j).getChildNodes().item(1);
              	  
              	  HardCodedCatalog  hardCodedCatalog = new HardCodedCatalog();
              	  hardCodedCatalog.setAction(
              	  	  getValue(hardcodedCatalog, "action", "")
              	  );
              	  
              	  campo.setHardCodedCatalog(
              	  	  hardCodedCatalog
              	  );
              
              }
              // Nota: ser�a �til agregar validaci�n excluyente para los par�metros de tipo
              // hard-coded-catalog, foreign-key y allowed
              
              // 26. Agregar campo al catalogo
              catalogo.addField(campo);
              
            } else {
               
               log.debug("El Campo No. \"" + ctaCampos + "\", ha sido descartado por no tener el atributo name.");
               
            }
            
          } catch(Exception ex) {
          	 
          	 log.debug("El campo \"" + strName + "\" con No. \"" + ctaCampos + "\", ha sido descartado debido a que se presentaron problemas en su lectura.");
          	 ex.printStackTrace();
          	 
          }
          
        }
 
        // 4. LEER NUMERO DE REGISTROS POR PAGINA
        try
        {
        	 catalogo.setRegistrosXpagina(15);
          //Obtener registros por pagina
          Node     registrosPagina =  getNodeByTagName(nodo.getChildNodes(),"registros-pagina");
          if( registrosPagina == null ){
           throw new AppException("El atributo \"registros-pagina\", no se encuentra definido. Se aborta la lectura del catalogo.");
          }
          NodeList regsXpage = registrosPagina.getChildNodes();
          catalogo.setRegistrosXpagina(Integer.parseInt( (String) regsXpage.item(0).getNodeValue() ));
        } catch(Exception e) { 
        	  log.info("No fue posible obtener la cantidad de registros por pagina para \"" + catalogo.getTableName() + "\" se asignara el valor default de 15."); 
        }

        // 5. LEER ORDERNAMIENTO DEL CATALOGO
        try
        { 
        	  catalogo.setDefaultOrderBy("");
        	  //Obtener default order by
           Node     defaultOrder =  getNodeByTagName(nodo.getChildNodes(),"default-order");
           if( defaultOrder == null ){
               throw new AppException("El atributo \"default-order\", no se encuentra definido. Se aborta la lectura del catalogo.");
           }
        	  NodeList defaultOrderList = defaultOrder.getChildNodes();
        	  catalogo.setDefaultOrderBy(defaultOrderList.item(0).getNodeValue());          
        } catch(Exception e) { 
        		log.info("No fue posible obtener la clausula de ordenamiento para \"" + catalogo.getTableName() + "\" se asignara el valor default."); 
        }
        
        // 6. REVIRSAR EL NUMERO DE CAMPOS CONFIGURADOS AL CATALOGO
        if(catalogo.getFieldCount() == 0){
        	  throw new AppException("El Cat�logo no tiene configurado ning�n campo");
        }
       
        // 7. LEER CONDICIONES ESPECIALES DEL QUERY
        Node     queryConditions 		= getNodeByTagName(nodo.getChildNodes(),"query-conditions");
        
        if( queryConditions != null ){
        	  
			  NodeList queryConditionsNodes 	= queryConditions.getChildNodes();
			  
			  for(int k = 0,ctaCondiciones = 0; k < queryConditionsNodes.getLength(); k++){
	 
				  if( k % 2 == 0 ){
				  	  continue;
				  } else if ( !"query-condition".equals( queryConditionsNodes.item(k).getNodeName() ) ) {
				  	  continue;
			  	  }
				  ctaCondiciones++;
				  String strName = null;
	 
				  try {
	
					  strName = getValue(queryConditionsNodes, k, "name", "");
					
					  if(!strName.trim().equals("")){
						  
					  	  QueryCondition queryCondition = new QueryCondition();
					  	  
					  	  // 1. Leer nombre de la condicion ... este campo es opcional
					  	  queryCondition.setName(
					  	  	  strName
					  	  );
					  	  // 2. Leer nombre de la tabla ... este campo es requerido
					  	  queryCondition.setTableName(
					  	  	  getValue(queryConditionsNodes, k, "table-name", 		"")
					  	  );
					  	  // 3. Leer nombre de la columna... este campo es requerido
					  	  queryCondition.setColumnName(
					  	  	  getValue(queryConditionsNodes, k, "column-name", 	"")
					  	  );
					  	  // 4. Leer tipo de valor ... este campo es requerido
					  	  queryCondition.setValueType(
					  	  	  getValue(queryConditionsNodes, k, "value-type", 		"")
					  	  );
					  	  // 5. Leer valor ( que tambien podria ser el nombre de un atributo ) ... este campo podr�a ser opcional en
					  	  // caso se agregara el value type NULL... pero por el momento es requerido
					  	  queryCondition.setValue(
					  	  	  getValue(queryConditionsNodes, k, "value", 			"")
					  	  );
					  	  // 6. Definir el modo de acceso que van a tener los perfiles... este campo es opcional
					  	  // ... solo es requerido si se especifican los modos de perfil INCLUDE � EXCLUDE
					  	  queryCondition.setProfileMode(
					  	  	  getValue(queryConditionsNodes, k, "profile-mode", 	"")
					  	  );
					  	  
					  	  // 7. Si hay condiciones de perfil ... leerlas
					  	  if(queryConditionsNodes.item(k).hasChildNodes()){ 
              
							 // Leer lista de valores restringidos
							 NodeList queryConditionNodes = queryConditionsNodes.item(k).getChildNodes();
							 int      numeroNodo    		= 0;
							 for(int m = 0; m < queryConditionNodes.getLength(); m++){
								 
								String nodeName = queryConditionNodes.item(m).getNodeName();
								
								if( !"catalog-profile".equals(nodeName) ){
									continue;
								} else {
									numeroNodo++;
								}
								
								try {
 
									// Leer valor permitido
									String strProfileName 		= getValue(queryConditionNodes, m, "profile-name", 		"");
		
									if( !"".equals( strProfileName.trim() ) ){
										queryCondition.addCatalogProfile(strProfileName);
									} else {
										log.debug("El registro \"catalog-profile\" con n�mero "+ numeroNodo +", ha sido descartado por venir vac�o.");
									}
								  
								} catch(Exception ex) {
									log.debug("El registro \"catalog-profile\" con n�mero "+ numeroNodo +", ha sido descartado por presentar problemas en su lectura.");
									ex.printStackTrace();
								}
								
							 }
							 
						  }
              
					  	  if( "".equals( queryCondition.getTableName().trim() ) ){
					  	  	  throw new AppException("El atributo query-condition("+strName+").table-name es requerido. Se excluye esta condicion del cat�logo.");
					  	  }
					  	  if( "".equals( queryCondition.getColumnName().trim() ) ){
					  	  	  throw new AppException("El atributo query-condition("+strName+").column-name es requerido. Se excluye esta condicion del cat�logo.");
					  	  }
					  	  if( QueryCondition.NO_VALUE_TYPE == queryCondition.getValueType() ){
					  	  	  throw new AppException("El atributo query-condition("+strName+").value-type es requerido. Se excluye esta condicion del cat�logo.");
					  	  }
					  	  if( "".equals( queryCondition.getValue().trim() ) ){
					  	  	  throw new AppException("El atributo query-condition("+strName+").value es requerido. Se excluye esta condicion del cat�logo.");
					  	  }
					  	  if( queryCondition.getCatalogProfilesCount() > 0 &&  queryCondition.getProfileMode() == QueryCondition.NO_PROFILE_MODE ){
					  	  	  log.info(
					  	  	  	  "El atributo query-condition("+strName+") tiene elementos de catalogo perfil ( catalog-profile )"+
					  	  	  	  " pero no ha especificado correctamente el modo de perfil (profile-mode), por lo que estos elementos"+
					  	  	  	  " del cat�logo perfil no ser�n considerados."
					  	  	  );
					  	  }
					  	  
					  	  catalogo.addQueryCondition( queryCondition );
					  	  
					  } else {
						  log.debug("La Condici�n de Query No. \"" + ( ctaCondiciones ) + "\", ha sido descartada por no tener el atributo name.");
					  }
					
				  } catch(Exception e){
					  
					  log.debug("La Condici�n de Query \"" + strName + "\" con No. \"" + ctaCondiciones + "\", ha sido descartada debido a que se presentaron problemas en su lectura.");
					  e.printStackTrace();
					  
				  }
				 
			  }
			  
        }
        
      }
 
      list = null;
 
    } catch(ParserConfigurationException e) {
    	 
    	exito = false;
    	log.error("getCatalogo(ParserConfigurationException)");
    	log.error("getCatalogo.nombreArchivo = <" + nombreArchivo + ">"); 
      e.printStackTrace();
      
    } catch(IOException e) {
    	
    	exito = false;
    	log.error("getCatalogo(IOException)");
    	log.error("getCatalogo.nombreArchivo = <" + nombreArchivo + ">"); 
      e.printStackTrace();
      
    } catch(SAXException e) {
    	
    	exito = false;
    	log.error("getCatalogo(SAXException)");
    	log.error("getCatalogo.nombreArchivo = <" + nombreArchivo + ">"); 
      e.printStackTrace();
      
    } catch(Exception e) {
    	
    	exito = false;
    	log.error("getCatalogo(Exception)");
    	log.error("getCatalogo.nombreArchivo = <" + nombreArchivo + ">"); 
      e.printStackTrace();
      
    } finally {
    	 
    	catalogo = !exito ? null :catalogo;
    	
      document = null;
      builder 	= null;
 
      log.debug("Se finaliz� la lectura del cat�logo");
      log.info("getCatalogo(S)");
      
    }
 
    return catalogo;
    
  }

  private String getValue(NodeList nodeList, int id, String key, String strDefault){
  	  
    try{
    	 
      NamedNodeMap attr = nodeList.item(id).getAttributes();
      if(attr == null || attr.getNamedItem(key) == null)
        return strDefault;
      else
        return attr.getNamedItem(key).getNodeValue();
     
    } catch(Exception e) {
    	 
    	log.error("getValue(Exception)");
      log.error("Error al leer el Atributo: \"" + key + "\"");
      log.error("getValue.nodeList   = <" + nodeList + ">");
      log.error("getValue.key        = <" + key + ">");
      log.error("getValue.strDefault = <" + strDefault + ">");
      
    }

    return strDefault;
    
  }

  private String getValue(Node node, String key, String strDefault) {
  	  
    try {
    	 
      if(node.getAttributes() != null && node.getAttributes().getNamedItem(key) == null)
        return strDefault;
      else
        return node.getAttributes().getNamedItem(key).getNodeValue();
     
    } catch(Exception e) {
    	 
    	log.error("getValue(Exception)");
      log.error("Error al leer el Atributo: \"" + key + "\"");
      log.error("getValue.node       = <" + node + ">");
      log.error("getValue.key        = <" + key + ">");
      log.error("getValue.strDefault = <" + strDefault + ">");
      
      e.printStackTrace();
      
    }

    return strDefault;
    
  }
  
  private boolean getValue(Node node, String key, boolean defaultValue) {
  	  
    try {
    	 
      if(node.getAttributes() != null && node.getAttributes().getNamedItem(key) == null)
        return defaultValue;
      else
        return "true".equals( node.getAttributes().getNamedItem(key).getNodeValue() );
     
    } catch(Exception e) {
    	 
    	log.error("getValue(Exception)");
      log.error("Error al leer el Atributo: \"" + key + "\"");
      log.error("getValue.node         = <" + node + ">");
      log.error("getValue.key          = <" + key + ">");
      log.error("getValue.defaultValue = <" + defaultValue + ">");
      
      e.printStackTrace();
      
    }

    return defaultValue;
    
  }
  
  private Node getNodeByTagName(NodeList nodeList, String tagName ){
  	  
    Node node = null;
    if ( nodeList != null ){
    
      for(int i=0;i<nodeList.getLength();i++){
         Node     aux      = nodeList.item(i);
         String   nodeName = aux.getNodeName();
         if( nodeName != null && nodeName.equals( tagName )){
            node = aux;
            break;
         }
      }
      
    }
    
    return node;
    
  }
 
}
