package com.netro.catalogos.dao;

import com.netro.catalogos.vo.DataRow;
import com.netro.catalogos.vo.DataSet;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.HashMap;
import java.util.Iterator;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class GenericDML 
{

  /**
   * Variable que se emplea para enviar mensajes al log.
   */
  private final static Log log = ServiceLocator.getInstance().getLog(GenericDML.class);

  public static boolean executeUpdate(String strSQL)
  {
    return executeUpdate(strSQL, null);
  }
  
  public static DataRow getDataRow(String strSQL) {
	  return getDataRow(strSQL, null);
  }
  
  public static DataRow getDataRow(String strSQL, Object[] values) {
  
	 DataRow 				row 		= new DataRow();
	 
    AccesoDB 				con 		= null;
	 PreparedStatement 	ps 		= null;
	 ResultSet 				rs 		= null;
    ResultSetMetaData 	mdata 	= null;
	 
    try{
    
      con = new AccesoDB();
      con.conexionDB();
      ps = con.queryPrecompilado(strSQL);
 
      if(values != null){
			
        for(int i = 1; i <= values.length; i++){
			  
          int k = i-1;

          if(values[k] == null){
				 
            log.debug("Setting parameter " + i + " to null.");
            ps.setNull(i, Types.VARCHAR);
				
          }else if(values[k] instanceof BigDecimal){
				 
            log.debug("Setting parameter " + i + " to " + values[k] + " as BigDecimal.");
            ps.setBigDecimal(i, (BigDecimal)values[k]);
				
          }else if(values[k] instanceof Integer){
				 
            log.debug("Setting parameter " + i + " to " + values[k] + " as BigDecimal.");
            ps.setInt(i, ((Integer)values[k]).intValue());
				
          }else if(values[k] instanceof Long){
				 
            log.debug("Setting parameter " + i + " to " + values[k] + " as BigDecimal.");
            ps.setLong(i, ((Long)values[k]).longValue());
				
          }else{
				 
            log.debug("Setting parameter " + i + " to \"" + values[k] + "\" as String.");
            ps.setString(i, (String)values[k]);
				
          }
			 
        }
		  
      }      
      
      //return getDataRow(ps);
		
		rs = ps.executeQuery();
      
		if(rs.next()){
			
        mdata = rs.getMetaData();      
       
        for(int i = 1; i <= mdata.getColumnCount(); i++){
          Object obj = rs.getObject(i);

          if(obj instanceof java.util.Date || obj instanceof java.sql.Date)
          {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            obj = formatter.format((java.util.Date)obj);            
          }
          
          row.addData(mdata.getColumnLabel(i), obj); 
        }
		  
      }
		
    }catch(Exception e){
		 
		row = new DataRow();
      e.printStackTrace();
		
    }finally{
		 
		if(rs != null) try { rs.close(); } catch(SQLException e) {}
      if(ps != null) try { ps.close(); } catch(SQLException e) {}
      if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
		
	 }

    return row;  
     
  }

  public static DataSet getDataSet(String strSQL) {
     return getDataSet(strSQL,null);
  }
  
  public static DataSet getDataSet(String strSQL, Object[] values) {
  
	  DataSet 				datos 	= new DataSet();
	 
	  AccesoDB 				con 		= null;
	  PreparedStatement 	ps 		= null;
	  ResultSet 			rs 		= null;
     ResultSetMetaData 	mdata 	= null;
	 
     try {
		  
		  con = new AccesoDB();
		  con.conexionDB();
		 
		  ps = con.queryPrecompilado(strSQL);
            
		  if(values != null){
			  
			  for(int i = 1; i <= values.length; i++){
				  int k = i-1;

				  if(values[k] == null){
					  
					  log.debug("Setting parameter " + i + " to null.");
					  ps.setNull(i, Types.VARCHAR);
					  
				  }else if(values[k] instanceof BigDecimal){
					  
					  log.debug("Setting parameter " + i + " to " + values[k] + " as BigDecimal.");
					  ps.setBigDecimal(i, (BigDecimal)values[k]);
					  
				  }else{
					  
					  log.debug("Setting parameter " + i + " to \"" + values[k] + "\" as String.");
					  ps.setString(i, (String)values[k]);
				  }
			  }
		  }      
      
      //return getDataSet(ps);
		rs = ps.executeQuery();
		
		boolean getHeader = true;
		
		while(rs.next()){
			
        if(getHeader){
          mdata = rs.getMetaData();      
          getHeader = false;  
        }
        
        DataRow row = new DataRow();

        for(int i = 1; i <= mdata.getColumnCount(); i++){
          Object obj = rs.getObject(i);

          if(obj instanceof java.util.Date || obj instanceof java.sql.Date){
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            obj = formatter.format((java.util.Date)obj);            
          }
          
          row.addData(mdata.getColumnLabel(i), obj); 
        }

        datos.addRow(row);
      }
		
    }catch(Exception e){
		 
      e.printStackTrace();
		datos = new DataSet();
		
    }finally{
		 
      if(rs != null) try { rs.close(); } catch(SQLException e) {}
      if(ps != null) try { ps.close(); } catch(SQLException e) {}
      if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
		
    }
    
    return datos;
	 
  }
  
  /*
  public static DataRow getDataRow(String data_source, String strSQL)
  {
    return getDataRow(data_source, strSQL, null);
  }
  */
  
  /*
  public static DataRow getDataRow(String data_source, String strSQL, Object[] values)
  {
    try
    {
      Connection cn = ServiceLocator.getInstance().getDatasource(data_source).getConnection();
      return getDataRow(cn, strSQL, values);
    }
    catch(SQLException e)
    {
      e.printStackTrace();
    }

    return new DataRow();    
  }*/

  /*
  public static DataRow getDataRow(Connection cn, String strSQL)
  {
    return getDataRow(cn, strSQL, null);
  }
  
  public static DataRow getDataRow(Connection cn, String strSQL, Object[] values)
  {
    try
    {
      PreparedStatement ps = cn.prepareStatement(strSQL);
 
      if(values != null)
      {
        for(int i = 1; i <= values.length; i++)
        {
          int k = i-1;

          if(values[k] == null)
          {
            log.debug("Setting parameter " + i + " to null.");
            ps.setNull(i, Types.VARCHAR);
          }
          else if(values[k] instanceof BigDecimal)
          {
            log.debug("Setting parameter " + i + " to " + values[k] + " as BigDecimal.");
            ps.setBigDecimal(i, (BigDecimal)values[k]);
          }
          else if(values[k] instanceof Integer)
          {
            log.debug("Setting parameter " + i + " to " + values[k] + " as BigDecimal.");
            ps.setInt(i, ((Integer)values[k]).intValue());
          }
          else if(values[k] instanceof Long)
          {
            log.debug("Setting parameter " + i + " to " + values[k] + " as BigDecimal.");
            ps.setLong(i, ((Long)values[k]).longValue());
          }
          else
          {
            log.debug("Setting parameter " + i + " to \"" + values[k] + "\" as String.");
            ps.setString(i, (String)values[k]);
          }
        }
      }      
      
      return getDataRow(ps);
    }
    catch(SQLException e)
    {
      e.printStackTrace();
    }

    return new DataRow();    
  }
  
  public static DataRow getDataRow(PreparedStatement ps)
  {
    DataRow row = new DataRow();

    Connection cn = null;
    ResultSet rs = null;
    ResultSetMetaData mdata = null;
    
    try
    {
      cn = ps.getConnection();
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        mdata = rs.getMetaData();      
       
        for(int i = 1; i <= mdata.getColumnCount(); i++)
        {
          Object obj = rs.getObject(i);

          if(obj instanceof java.util.Date || obj instanceof java.sql.Date)
          {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            obj = formatter.format((java.util.Date)obj);            
          }
          
          row.addData(mdata.getColumnLabel(i), obj); 
        }
      }
    }
    catch(SQLException e)
    {
      e.printStackTrace(); 
    }
    finally
    {
      if(rs != null) try { rs.close(); } catch(SQLException e) {}
      if(ps != null) try { ps.close(); } catch(SQLException e) {}
      if(cn != null) try { cn.close(); } catch(SQLException e) {}

      mdata = null;
      rs = null;
      ps = null;      
      cn = null;
    }
    
    return row;
  }
  */
  /*
  public static DataSet getDataSet(Connection cn, String strSQL)
  {
    return getDataSet(cn, strSQL, null);
  }
  
  public static DataSet getDataSet(Connection cn, String strSQL, Object[] values)
  {
    try
    {
      PreparedStatement ps = cn.prepareStatement(strSQL);
      //System.out.println("QueryMPCS:"+strSQL+":");
            
      if(values != null)
      {
        for(int i = 1; i <= values.length; i++)
        {
          int k = i-1;

          if(values[k] == null)
          {
            log.debug("Setting parameter " + i + " to null.");
            ps.setNull(i, Types.VARCHAR);
          }
          else if(values[k] instanceof BigDecimal)
          {
            log.debug("Setting parameter " + i + " to " + values[k] + " as BigDecimal.");
            ps.setBigDecimal(i, (BigDecimal)values[k]);
          }
          else
          {
            log.debug("Setting parameter " + i + " to \"" + values[k] + "\" as String.");
            ps.setString(i, (String)values[k]);
          }
        }
      }      
      
      return getDataSet(ps);
    }
    catch(SQLException e)
    {
      e.printStackTrace();
    }
    finally
    {
      if(cn != null) try { cn.close(); } catch(SQLException e) {}
      cn = null;
    }
    
    return new DataSet();
  }

  public static DataSet getDataSet(String data_source, String strSQL)
  {
    return getDataSet(data_source, strSQL, null);
  }
  
  public static DataSet getDataSet(String data_source, String strSQL, Object[] values)
  {
    Connection cn = null;
    
    try
    {
      cn = ServiceLocator.getInstance().getDatasource(data_source).getConnection();
      return getDataSet(cn, strSQL, values);
    }
    catch(SQLException e)
    {
      System.out.println("GenericDML Command: " + strSQL);
      e.printStackTrace();
    }
    finally
    {
      if(cn != null) try { cn.close(); } catch(SQLException e) {}
      cn = null;
    }

    return new DataSet();
  }

  public static DataSet getDataSet(PreparedStatement ps)
  {
    DataSet datos = new DataSet();

    Connection cn = null;
    ResultSet rs = null;
    ResultSetMetaData mdata = null;
    
    try
    {
      cn = ps.getConnection();
      rs = ps.executeQuery();

      boolean getHeader = true;
      
      while(rs.next())
      {
        if(getHeader)
        {
          mdata = rs.getMetaData();      
          getHeader = false;  
        }
        
        DataRow row = new DataRow();

        for(int i = 1; i <= mdata.getColumnCount(); i++)
        {
          Object obj = rs.getObject(i);

          if(obj instanceof java.util.Date || obj instanceof java.sql.Date)
          {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            obj = formatter.format((java.util.Date)obj);            
          }
          
          row.addData(mdata.getColumnLabel(i), obj); 
        }

        datos.addRow(row);
      }
    }
    catch(SQLException e)
    {
      System.out.println("Error en la consulta: " + e.getSQLState());
      e.printStackTrace(); 
    }
    finally
    {
      if(rs != null) try { rs.close(); } catch(SQLException e) {}
      if(ps != null) try { ps.close(); } catch(SQLException e) {}
      if(cn != null) try { cn.close(); } catch(SQLException e) {}

      mdata = null;
      rs = null;
      cn = null;
    }
    
    return datos;    
  }  
  */
  
  public static HashMap executeCall(String strSQL, Object[] values, HashMap outKeys){
	  
    HashMap 				result 	= new HashMap();

    AccesoDB 				con		= null;
    CallableStatement 	call 		= null;
	 boolean					bOk		= true;
	  
    log.debug("strSQL: " + strSQL);
    
    try {
		 
		 con = new AccesoDB();
		 con.conexionDB();
		       
       call = con.getConnection().prepareCall(strSQL);

		 if(values != null) {
			 
			 for(int i = 1; i <= values.length; i++) {
				 int k = i-1;
          
				 if(outKeys.containsKey(new Long(i))) {
					 
					 if(outKeys.get(new Long(i)) instanceof BigDecimal) {
						 call.registerOutParameter(i, Types.NUMERIC);
						 log.debug("Registering output parameter " + i + " as numeric (BigDecimal).");
					 } else if(outKeys.get(new Long(i)) instanceof Long) {
						 call.registerOutParameter(i, Types.NUMERIC);
						 log.debug("Registering output parameter " + i + " as numeric. (Long)");
					 } else if(outKeys.get(new Long(i)) instanceof Integer) {
						 call.registerOutParameter(i, Types.NUMERIC);
						 log.debug("Registering output parameter " + i + " as numeric. (Integer)");
					 } else if(outKeys.get(new Long(i)) instanceof Date) {
						 call.registerOutParameter(i, Types.DATE);
						 log.debug("Registering output parameter " + i + " as date.");
					 } else {
						 call.registerOutParameter(i, Types.VARCHAR);
						 log.debug("Registering output parameter " + i + " as String.");
					 }
					 
				 } else {
					 
					 if(values[k] == null) {
						 call.setNull(i, Types.VARCHAR);
						 log.debug("Setting parameter " + i + " to null.");
					 } else if(values[k] instanceof BigDecimal) {
						 log.debug("Setting parameter " + i + " to " + values[k] + " as BigDecimal.");
						 call.setBigDecimal(i, (BigDecimal)values[k]);
					 } else if(values[k] instanceof Long) {
						 log.debug("Setting parameter " + i + " to " + values[k] + " as Long.");
						 call.setLong(i, ((Long)values[k]).longValue());
					 } else if(values[k] instanceof Integer) {
						 log.debug("Setting parameter " + i + " to " + values[k] + " as Integer.");
						 call.setInt(i, ((Integer)values[k]).intValue());
					 } else if(values[k] instanceof Date) {
						 log.debug("Setting parameter " + i + " to " + values[k] + " as Date.");
						 call.setDate(i, new java.sql.Date( ((Date)values[k]).getTime()) );
					 } else {
						 log.debug("Setting parameter " + i + " to \"" + values[k] + "\" as String.");
						 call.setString(i, (String)values[k]);
					 }
					 
				 }
				 
			 }
			 
      }      
      
      call.executeUpdate();
      
      Iterator itOutKeys = outKeys.keySet().iterator();
      
      while(itOutKeys.hasNext()){
			
        Object key 			= itOutKeys.next();
        Object fieldValue 	= null;
        
        int 	fieldId 		= ((Long)key).intValue();
        
        if(outKeys.get(key) instanceof BigDecimal)
          result.put(key, call.getBigDecimal(fieldId));
        else if(outKeys.get(key) instanceof Long)
          result.put(key, new Long(call.getLong(fieldId)));
        else if(outKeys.get(key) instanceof Integer)
          result.put(key, new Integer(call.getInt(fieldId)));
        else if(outKeys.get(key) instanceof Date)
          result.put(key, call.getDate(fieldId));
        else
          result.put(key, call.getString(fieldId));
		 
      }
    
    } catch(Exception e) {
    
		bOk = false;
		
      log.debug("strSQL: " + strSQL);
      result.put("SQL_EXCEPTION", e);
      log.error("executeCall(Exception)"+e);
      if(e instanceof SQLException){
         log.error("executeCall(SQLException):"+((SQLException)e).getErrorCode()); 
      }
      log.error("executeCall(SQLException):"+e.getMessage()); 
      e.printStackTrace();
      
    } finally {
		 
		con.terminaTransaccion(bOk);
		
      if(call != null) try { call.close(); } catch(Exception e) {}      
      if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
		
    }
    
    return result;   
	 
  }
  
  public static boolean executeUpdate(String strSQL, Object[] values){
	  
	 log.info("executeUpdate(E)");
	 
	 AccesoDB 				con 		= new AccesoDB();
	 PreparedStatement 	ps 		= null;
	 boolean					bOk		= true;
	 
    log.debug("strSQL: " + strSQL);
    log.debug("VALUES=+" + values.toString());
	 
    try{
		 
		con.conexionDB();
      ps = con.queryPrecompilado(strSQL);

      if(values != null){
			
        for(int i = 1; i <= values.length; i++){
			  
			 int k = i-1;
			 log.debug("VALUES("+k+")=+"+values[k]);
          
          if(values[k] == null){
				 
            log.debug("Setting parameter " + i + " to null.");
            ps.setNull(i, Types.VARCHAR);
				
          }else if(values[k] instanceof BigDecimal){
				 
            log.debug("Setting parameter " + i + " to " + values[k] + " as BigDecimal.");
            ps.setBigDecimal(i, (BigDecimal)values[k]);
				
          }else if(values[k] instanceof Long){
				 
            log.debug("Setting parameter " + i + " to " + values[k] + " as Long.");
            ps.setLong(i, ((Long)values[k]).longValue());
				
          }else if(values[k] instanceof Integer){
				 
            log.debug("Setting parameter " + i + " to " + values[k] + " as Integer.");
            ps.setInt(i, ((Integer)values[k]).intValue());
				
          }else{
				 
            log.debug("Setting parameter " + i + " to \"" + values[k] + "\" as String.");
            ps.setString(i, (String)values[k]);
				
          }
			 
        }
		  
      }      
      
      ps.executeUpdate();
 
    }catch(Exception e){
		 
		bOk = false;
		
		log.error("executeUpdate(Exception)");
      log.error("strSQL: " + strSQL);
      e.printStackTrace();
		
    }finally{
		
		con.terminaTransaccion(bOk);
		
		if(ps != null) try { ps.close(); } catch(Exception e) {}
      if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
		
		log.info("executeUpdate(S)");
			
    }
    
    return bOk;
    
  }
  
}