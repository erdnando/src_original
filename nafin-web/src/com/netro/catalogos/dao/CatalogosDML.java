package com.netro.catalogos.dao;

import com.netro.catalogos.vo.Campo;
import com.netro.catalogos.vo.Catalogo;
import com.netro.catalogos.vo.DataRow;
import com.netro.catalogos.vo.QueryCondition;

import java.math.BigDecimal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CatalogosDML {

  /**
   * Variable que se emplea para enviar mensajes al log.
   */
  private final static Log log = ServiceLocator.getInstance().getLog(CatalogosDML.class);

  private HttpServletRequest 	request 					= null;
  private String 					strSQL 					= "";  
  private HashMap 				parameters 				= null;
  private HashMap 				modifiedParameters 	= null; // En realidad aqui se guardan los valores orginales
  private String 					usuario;
  private boolean 				isDataArray 			= false;

  /**
    * Nota: este metodo solo es util cuando los parametros pasados
    * en el request no vienen en forma de array: isDataArray == false
    */
  public String obtenerValor(String strCampo, String strDefault){
    String strValor = (String)parameters.get(strCampo);
    if(strValor != null && strValor.equals("-1")) strValor = null;
    return strValor == null?strDefault:strValor;
  }

  /**
    * Nota: este metodo solo es util cuando los parametros pasados
    * en el request no vienen en forma de array: isDataArray == false
    */
  public String obtenerValorOriginal(String strCampo, String strDefault){
    String strValor = (String)modifiedParameters.get(strCampo);
    if(strValor != null && strValor.equals("-1")) strValor = null;
    return strValor == null?strDefault:strValor;
  }
  
  /**
    * Nota: este metodo solo es util cuando los parametros pasados
    * en el request vienen en forma de array: isDataArray == true
    */
  public BigDecimal[] obtenerValores(String strCampo, String strDefault){
  	 BigDecimal[] lista = new BigDecimal[parameters.size()]; 
  	 
  	 for(int i=0;i<parameters.size();i++){
  	 	 
  	 	JSONObject 	elemento = (JSONObject) parameters.get(String.valueOf(i));
  	 	
  	 	String 		strValor = elemento.getString(strCampo);
  	 	if(strValor != null && strValor.equals("-1")) strValor = null;
  	 	 lista[i] = new BigDecimal(strValor == null?strDefault:strValor);
  	 }
	
	 return lista;  	 

  }
  
  private void parseRequest(){
	  
     // 1. Leer campos actuales
	  
	  isDataArray = "true".equals((String) request.getParameter("dataarray"))?true:false;
	  
	  String source = request.getParameter("data");
	  source = source == null?(isDataArray?"[]":"{}"):source.trim();
 
	  if(isDataArray){
		  JSONArray requestDataArray = JSONArray.fromObject(source);
		  for (int i = 0; i < requestDataArray.size(); i++) {
			  JSONObject elemento = requestDataArray.getJSONObject(i);
			  parameters.put(String.valueOf(i), elemento );
		  }
	  }else{
		  JSONObject requestData = JSONObject.fromObject(source);
		  for(Iterator keys = requestData.keys(); keys.hasNext(); ){
				String name = (String) keys.next();
				parameters.put(name, requestData.getString(name));
		  }
	  }	
	  
	  // 2. Leer campos modificados
     source = request.getParameter("modified");
	  source = source == null?"{}":source.trim();
      
     JSONObject requestModified = JSONObject.fromObject(source);
     for(Iterator keys = requestModified.keys(); keys.hasNext(); ){
		  String name = (String) keys.next();
		  modifiedParameters.put(name, requestModified.getString(name));
	  }
     
	  /* 
     Enumeration en = request.getParameterNames();
     while(en.hasMoreElements())
     {
       String name = (String)en.nextElement();
       parameters.put(name, request.getParameter(name));
     }
	  */
  }

  private boolean existsOnRequest(String strCampo){
    return modifiedParameters.containsKey(strCampo);
  }
  
  public boolean existsOnModifiedParameterList(String strCampo){
    return modifiedParameters.containsKey(strCampo);
  }
 
  public CatalogosDML(HttpServletRequest request){
  	  
    this.request 					= request;
    this.parameters 				= new HashMap();
    this.modifiedParameters 	= new HashMap();
    parseRequest();
    
  }
 
  public CatalogosDML(HashMap parameters){
    this.parameters = parameters;
  }
 
  public DataRow getDatos(Catalogo catalogo, BigDecimal pk)
  {
    log.debug("Datos Obtenidos! X");
    return getDatos(catalogo, pk, new BigDecimal(0));
  }

  public DataRow getDatos(Catalogo catalogo, BigDecimal pk, BigDecimal pkComp){
  	  
    DataRow data = new DataRow();

    if(pkComp.intValue() != 0){
    	
      String[] pks = getPk(catalogo.getPrimaryKeyName());
      strSQL = "SELECT * FROM " + catalogo.getTableName() + " WHERE " + pks[0] + " = " + pk + " AND " + pks[1] + " = " + pkComp;
      throw new AppException("Unsupported compound keys");
      
    } else {
    	 
      strSQL = "SELECT * FROM " + catalogo.getTableName() + " WHERE " + catalogo.getPrimaryKeyName() + " = " + pk;
      
    }

    // Si se tienen definidas condiciones especiales para este query, agregarlas:
    ArrayList queryConditionsValuesList = new ArrayList();
    for(int m = 0; m < catalogo.getQueryConditionCount(); m++){
    	 
    	 QueryCondition queryCondition = catalogo.getQueryCondition(m);
    	 
    	 // Nota: por simplicidad no se verifica que la tabla del query condition coincida con la
    	 // configurada en el tag del catalogo, una futura version con mas caracteristicas podr�a
    	 // requerirla
    	 
    	// Determinar si el perfil del usuario tiene permiso de usar esta condici�n del query
      if( queryCondition.skipCondition( catalogo.getPerfilCatalogo() ) ){
      	continue;
      }
      
      // Obtener valor asociado
      String valor = null;
      if( 			QueryCondition.SESSION	== queryCondition.getValueType() ){
      	String attributeName = queryCondition.getValue();
      	valor 					= (String) request.getSession().getAttribute(attributeName);
      } else if(	QueryCondition.REQUEST	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
      	
      } else if(	QueryCondition.LOGICAL	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
      	
      } else if(	QueryCondition.VALUE 	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
      	
      }
    	
      // Agregar condiciones adicionales
      if( valor == null ){
      	
      	// La condicion adicional es NULL
      	strSQL += " AND " +  queryCondition.getColumnName() + " IS NULL ";
      	
      } else {
      	
      	// Agregar condicion null adicional
			valor = valor.replaceAll("'","''");
			strSQL += " AND " +  queryCondition.getColumnName() + " = '"+valor+"' ";
			
      }
      
    }
    
    data = GenericDML.getDataRow(strSQL);
    
    return data;
  }
 
  public HashMap removeRecords(Catalogo catalogo, Object[] arrayPKs){
 
    HashMap deleteOK = null;

    /*
    if(catalogo.getPrimaryKeyName().indexOf(",") != -1)
    {
      String[] pks = getPk(catalogo.getPrimaryKeyName());
      strSQL = "DELETE FROM " + catalogo.getTableName() + " WHERE " + pks[0] + " = ? AND " + pks[1] + " = ?";
      deleteOK = executeUpdate(strSQL, new Object[]{pk, pk});
    }
    else
    {
      strSQL = "DELETE FROM " + catalogo.getTableName() + " WHERE " + catalogo.getPrimaryKeyName() + " = ? ";
      deleteOK = executeUpdate(strSQL, new Object[]{pk});
    }
    */
    
    StringBuffer variablesBind = new StringBuffer();
    for(int i=0;i<arrayPKs.length;i++){
    	 if(i>0) variablesBind.append(",");
    	 variablesBind.append("?");
    }
    
    strSQL = "DELETE FROM " + catalogo.getTableName() + " WHERE " + catalogo.getPrimaryKeyName() + " IN ("+variablesBind.toString()+") ";
    
    // Si se tienen definidas condiciones especiales para este query, agregarlas:
    ArrayList queryConditionsValuesList = new ArrayList();
    for(int m = 0; m < catalogo.getQueryConditionCount(); m++){
    	 
    	 QueryCondition queryCondition = catalogo.getQueryCondition(m);
    	 
    	 // Nota: por simplicidad no se verifica que la tabla del query condition coincida con la
    	 // configurada en el tag del catalogo, una futura version con mas caracteristicas podr�a
    	 // requerirla
    	 
    	// Determinar si el perfil del usuario tiene permiso de usar esta condici�n del query
      if( queryCondition.skipCondition( catalogo.getPerfilCatalogo() ) ){
      	continue;
      }
      
      // Obtener valor asociado
      String valor = null;
      if( 			QueryCondition.SESSION	== queryCondition.getValueType() ){
      	String attributeName = queryCondition.getValue();
      	valor 					= (String) request.getSession().getAttribute(attributeName);
      } else if(	QueryCondition.REQUEST	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
      	
      } else if(	QueryCondition.LOGICAL	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
      	
      } else if(	QueryCondition.VALUE 	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
      	
      }
    	 
      // Agregar condiciones adicionales
      strSQL += " AND " +  queryCondition.getColumnName() + " = ? ";
      queryConditionsValuesList.add(valor);
              
    }
    
    // Si se agregaron condiciones adicionales al query, crear nuevo array de valores
    Object[] arrayPKsActualizado = null;
    if(  queryConditionsValuesList.size() > 0 ){
    	 
    	 arrayPKsActualizado = new Object[ arrayPKs.length + queryConditionsValuesList.size() ];
    	 
    	 int k = 0;
    	 // Copiar los elementos del array original
    	 for( k = 0; k < arrayPKs.length; k++){
    	 	 arrayPKsActualizado[k] = arrayPKs[k];
    	 }
    	 // Agregar los valores de la Query Conditions
    	 for( int n = 0; n < queryConditionsValuesList.size(); n++,k++ ){
    	 	  arrayPKsActualizado[k] = queryConditionsValuesList.get(n);
    	 }
    	 // Referencia al nuevo array
    	 arrayPKs = arrayPKsActualizado;
    	 
    }
    
    deleteOK = executeUpdate(strSQL, (Object[])arrayPKs);
 
    return deleteOK;
    
  }
  
  public HashMap updateCatalogo(Catalogo catalogo, BigDecimal pk){
    return updateCatalogo(catalogo, pk, "");
  }

  public HashMap updateCatalogo(Catalogo catalogo, BigDecimal pk, String fieldAlias) {
    return updateCatalogo(catalogo, pk, fieldAlias, false);
  }

  public HashMap updateCatalogo(Catalogo catalogo, BigDecimal pk, String fieldAlias, boolean onlyFromRequest) {
    return updateCatalogo(catalogo, pk, fieldAlias, onlyFromRequest, false);
  }

  public HashMap updateCatalogo(Catalogo catalogo, BigDecimal pk, String fieldAlias, boolean onlyFromRequest, boolean useTablePrefix) {
 
    HashMap 	retValue = null;
    ArrayList 	arValues = new ArrayList();

    /*
     String usuario = obtenerValor("NafinUserName", "SISTEMA");
     if(usuario == null || usuario.trim().equals("")) usuario = this.usuario;
    */
   
    strSQL = "UPDATE " + catalogo.getTableName() + " SET ";// Debug info: depuracion de errores

    boolean takeField 		= true;

    String strUpdateFields = "";
    String valor 				= "";

    int ctaCamposUpdate = 0;
    for(int i = 0; i < catalogo.getFieldCount(); i++){
    	 
      Campo campo = catalogo.getField(i);
      takeField = true;

      //if(onlyFromRequest & !existsOnRequest(fieldAlias + campo.getFieldName()))
      if(onlyFromRequest & !existsOnRequest((useTablePrefix ? catalogo.getTableName() + "." :fieldAlias)  + campo.getFieldName()) )
      takeField = false;

      if(!campo.useOnUpdate)
      {
        takeField = false;
        strUpdateFields += campo.getFieldName() + ",";
      }

      if(takeField)
      {
        if(!campo.isFile)
        {
          if(!campo.isReadOnly)
          {
            if(campo.getDefaulValue() == null)
              valor = obtenerValor((useTablePrefix ? catalogo.getTableName() + "." : "") + fieldAlias + campo.getFieldName(), "");
            else
              valor = obtenerValor((useTablePrefix ? catalogo.getTableName() + "." : "") + fieldAlias + campo.getFieldName(), campo.getDefaulValue().toString());

            if(valor.trim().equals("-1")) valor = "NULL";
            if(valor.trim().equals("") && campo.isNullable) valor = "NULL";

           log.debug("[CatalogosDML:331] Catalog: " + catalogo.getTableName() + 
                                 " Field " + campo.getFieldName() + " Value: '" + valor + "'" + 
                                 " Nullable: " + campo.isNullable);

            if(campo.getType() instanceof String)
            {
              if(!valor.trim().toUpperCase().equals("NULL")) {
                if(valor.trim().equals("") && campo.isNullable) {
                	strSQL += ctaCamposUpdate > 0?",":"";
                  strSQL += campo.getFieldName() + " = NULL ";
                  ctaCamposUpdate++;
                  //arValues.add(null);
                } else {
                	strSQL += ctaCamposUpdate > 0?",":"";
                  strSQL += campo.getFieldName() + " = ? ";
                  ctaCamposUpdate++;
                  arValues.add(remplazarNoValidos(valor));
                }
              } else {
              	 strSQL += ctaCamposUpdate > 0?",":"";
                strSQL += campo.getFieldName() + " = NULL ";
                ctaCamposUpdate++;
                //arValues.add(null);
              }
            }
            else if(campo.getType() instanceof Date)
            {
              if(valor.equalsIgnoreCase("SYSDATE"))
              {
              	 strSQL += ctaCamposUpdate > 0?",":"";
                strSQL += campo.getFieldName() + " = SYSDATE ";
                ctaCamposUpdate++;
              }
              else
              {
                if(valor != null && !valor.trim().toUpperCase().equals("NULL")) {
                	strSQL += ctaCamposUpdate > 0?",":"";
                  strSQL += campo.getFieldName() + " = TO_DATE(?, '" + campo.getFormat() + "') ";
                  ctaCamposUpdate++;
                  arValues.add(remplazarNoValidos(valor));
                } else {
                	strSQL += ctaCamposUpdate > 0?",":"";
                  strSQL += campo.getFieldName() + " = NULL ";
                  ctaCamposUpdate++;
                }
              }
            }
            else if(campo.getType() instanceof BigDecimal)
            {
              if(valor != null && !valor.trim().toUpperCase().equals("NULL")) {
              	 strSQL += ctaCamposUpdate > 0?",":"";
                strSQL += campo.getFieldName() + " = ? ";
                ctaCamposUpdate++;
                arValues.add(new BigDecimal(valor));
              } else {
              	 strSQL += ctaCamposUpdate > 0?",":"";
                strSQL += campo.getFieldName() + " = NULL ";
                ctaCamposUpdate++;
              }
            }
            else
            {
              if(valor != null && !valor.trim().toUpperCase().equals("NULL")) {
              	 strSQL += ctaCamposUpdate > 0?",":"";
                strSQL += campo.getFieldName() + " = ? ";
                ctaCamposUpdate++;
                arValues.add(valor);
              } else {
              	 strSQL += ctaCamposUpdate > 0?",":"";
                strSQL += campo.getFieldName() + " = NULL ";
                ctaCamposUpdate++;
              }
            }
          }
        }
      }
    }
 
    // Determinar si se guardar� la fecha en la que se realiz� la modificaci�n del registro
    if( catalogo.getHasUserModificationDateField() ){
    	 strSQL += ctaCamposUpdate > 0?",":"";
    	 strSQL += " DULTIMA_MODIFICACION = SYSDATE ";
    	 ctaCamposUpdate++;
    }
    
    // Determinar si se registrara el login del usuario que realiz� la modificaci�n al cat�logo
    if ( catalogo.getHasUserModificationField()    ){
    	 strSQL += ctaCamposUpdate > 0?",":"";
    	 strSQL += " VUSUARIO_ULT_MODIF = ? ";
    	 ctaCamposUpdate++;
    	 arValues.add(this.usuario);
    }
    
    // Agregar clausula where
	 strSQL += " WHERE ";

    if(catalogo.getPrimaryKeyName().indexOf(",") == -1){
      
		boolean addAnd = false;
		
    	// Nota: Esta opci�n ha quedadado pendiente de usar, ser�a en caso
    	// de que la tabla no tenga un campo indice( ni por secuencia, ni por max + 1)
    	if(catalogo.getPrimaryKeyName().equals("")){
    		
        StringTokenizer stUpFields = new StringTokenizer(strUpdateFields, ",");
 
        while(stUpFields.hasMoreTokens())
        {
          String strFieldName = stUpFields.nextToken();

          if(!addAnd)
          {
            strSQL += strFieldName + " = ? ";
            addAnd = true;
          }
          else
            strSQL += " AND " + strFieldName + " = ? ";

          valor = obtenerValor((useTablePrefix ? catalogo.getTableName() + "." : "") + fieldAlias + strFieldName, "");

          Campo c = catalogo.getField(strFieldName);

          if(c.getType() instanceof Date)
            arValues.add(remplazarNoValidos(valor));
          else if(c.getType() instanceof BigDecimal)
            arValues.add(new BigDecimal(valor));
          else
            arValues.add(valor);
        }
        
      // Agregar condicion correspondiente a la llave primaria
      } else {
      	
        strSQL += catalogo.getPrimaryKeyName() + " = ? ";
        arValues.add(pk);
        addAnd = true;
        
      }
      
      // Agregar condiciones del query
      for(int m = 0; m < catalogo.getQueryConditionCount(); m++){
      	
      	QueryCondition queryCondition = catalogo.getQueryCondition(m);
      	
      	// Nota: por simplicidad no se verifica que la tabla del query condition coincida con la
      	// configurada en el tag del catalogo, una futura version con mas caracteristicas podr�a
    	 	// requerirla
    	 
    	 	// Determinar si el perfil del usuario tiene permiso de usar esta condici�n del query
     		if( queryCondition.skipCondition( catalogo.getPerfilCatalogo() ) ){
      		continue;
      	}
      	
      	// Obtener valor asociado
			valor = null;
			if( 			QueryCondition.SESSION	== queryCondition.getValueType() ){
				String attributeName = queryCondition.getValue();
				valor 					= (String) request.getSession().getAttribute(attributeName);
			} else if(	QueryCondition.REQUEST	== queryCondition.getValueType() ){
				
				throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
				
			} else if(	QueryCondition.LOGICAL	== queryCondition.getValueType() ){
				
				throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
				
			} else if(	QueryCondition.VALUE 	== queryCondition.getValueType() ){
				
				throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
				
			}
			
			// Agregar las condiciones a la clausula where
			if( !addAnd ){
				
				strSQL += queryCondition.getColumnName() + " = ? ";
				arValues.add(valor);
				addAnd = true;
				
			} else {
				
				strSQL += " AND " + queryCondition.getColumnName() + " = ? ";
				arValues.add(valor);
				
			}
        
      }
      
    // NOTA: ACTUALMENTE ESTA SECCION HA QUEDADO DEPRECADA 
    } else {

      log.debug("Llave Compuesta: " + catalogo.getPrimaryKeyName());

      String aux = catalogo.getPrimaryKeyName();
      int indx = 0;
      boolean isFirst = true;
      String auxFieldName = "";

      while((indx = aux.indexOf(",")) != -1)
      {
        if(isFirst)
        {
          strSQL += aux.substring(0, indx) + " = ? ";
          arValues.add(pk);
          isFirst = false;

          aux = aux.substring(indx + 1, aux.length());

          if(aux.indexOf(",") != -1) {
            indx = aux.indexOf(",");
            String temp = aux.substring(indx + 1);
            auxFieldName = aux.substring(indx + 1, temp.indexOf(",") + (indx - 1));
          } else {
            auxFieldName = aux.trim();
          }

          auxFieldName = auxFieldName.trim();

          log.debug("auxFieldName: " + auxFieldName);
          strSQL += " AND " + auxFieldName + " = ? ";
          arValues.add(new BigDecimal(obtenerValor((useTablePrefix ? catalogo.getTableName() + "." : "") + auxFieldName, "0")));
        }
        else
        {
          auxFieldName = "";

          if(aux.substring(indx + 1).indexOf(",") != -1) {
            String temp = aux.substring(indx + 1);
            auxFieldName = aux.substring(indx + 1, temp.indexOf(",") + (indx - 1));
          } else {
            auxFieldName = aux.substring(indx + 1, aux.length());
          }

          auxFieldName = auxFieldName.trim();

          log.debug("auxFieldName: " + auxFieldName);
          strSQL += " AND " + auxFieldName + " = ? ";
          arValues.add(new BigDecimal(obtenerValor((useTablePrefix ? catalogo.getTableName() + "." : "") + auxFieldName, "0")));

          aux = aux.substring(indx + 1, aux.length());
        }
      }
    }

    log.debug("strSQL: " + strSQL);

    retValue = executeUpdate(strSQL, arValues.toArray());

    return retValue;
    
  }

  public HashMap insertCatalogo(Catalogo catalogo){
    return insertCatalogo(catalogo, false);
  }

  public HashMap insertCatalogo(Catalogo catalogo, boolean useTablePrefix){
 
    HashMap 	retValue 	= null;
    ArrayList 	arValues 	= new ArrayList();

    String 		usuario 		= obtenerValor("NafinUserName", "SISTEMA");

    String 		strFields 	= "";
    String 		strValues 	= "";
    String 		pkValue   	= "";

    // Nota: Esta seccion ha sido deprecada ... solo se permite una llave primaria por etiqueta de primary key
    if(catalogo.getPrimaryKeyName().indexOf(",") != -1){
    	 
      strFields = catalogo.getPrimaryKeyName().substring(0, catalogo.getPrimaryKeyName().indexOf(",")).trim() + ", ";

	  String pkName = catalogo.getPrimaryKeyName().substring(catalogo.getPrimaryKeyName().indexOf(",") + 1).trim();
	  if(catalogo.getTableName().toString().equals("ECON_CAT_CIUDADES")||catalogo.getTableName().toString().equals("ECON_CAT_MUNICIPIOS"))
	  {
	  strSQL = "SELECT NVL((SELECT MAX(" + catalogo.getPrimaryKeyName().substring(0, catalogo.getPrimaryKeyName().indexOf(",")).trim() +
			   ") FROM " + catalogo.getTableName() + " WHERE " + pkName + " = " + obtenerValor(pkName, "0") +
			   " AND " + catalogo.getPrimaryKeyName().substring(0, catalogo.getPrimaryKeyName().indexOf(",")).trim() + "<1000000), 0) + 1 NEXTPK FROM DUAL";
	  }
	  else
	  {
	  strSQL = "SELECT NVL((SELECT MAX(" + catalogo.getPrimaryKeyName().substring(0, catalogo.getPrimaryKeyName().indexOf(",")).trim() +
			   ") FROM " + catalogo.getTableName() + " WHERE " + pkName + " = " + obtenerValor(pkName, "0") + "), 0) + 1 NEXTPK FROM DUAL";
	  }
	   log.debug("strSQL:"+strSQL);
      pkValue    = GenericDML.getDataRow(strSQL).getData("NEXTPK").toString();
      strValues += "" + pkValue + ", ";
      
    } else {
    	 
    	// I. Obtener valor de la llave primaria y agregarlo string del insert
    	
    	// IMPLEMENTACION "TEMPORAL"
    	// IMPORTANTE: EN UN DESARROLLO FUTURO, CUANDO ESTA PARTE SE VUELVA MAS GENERICA
    	// MIGRAR ESTA OPCION AL XML ... SE SUGIERE UTILIZAR EL TAG primary-key con el atributo dinamic-sequence
    	if( 
    			"1".equals( catalogo.getTableId().toString() ) 
    				&& 
    			"COMREL_CALIF_EPO".equalsIgnoreCase( catalogo.getTableName() ) 
    			
    		){ 
 
    		String perfilUsuario 	= (String) request.getSession().getAttribute("sesPerfil");
    		if( !"ADMIN EPO".equals(perfilUsuario) ){
    			throw new AppException("No cumple con los requisitos sufientes para agregar un nuevo registro.");
    		}
    		
    		String valorPK 	= obtenerValor((useTablePrefix ? catalogo.getTableName() + "." : "") + catalogo.getPrimaryKeyName(), "0");
    		
    		strFields 			= catalogo.getPrimaryKeyName() + ", ";
 
    		if( valorPK.equals("") || valorPK.equals("0") ){
    			
    			String cadenasEpos 			=  (String) request.getSession().getAttribute("iNoEPO");
    			int 	 totalRegistros  		= 0;
    			int 	 valorNuevaClavePk 	= 0;
				
    			// Obtener clave siguiente
    			strSQL     				=  "SELECT nvl(max(T1.IC_CALIFICACION),0)+1 AS CVEREGSIG FROM COMCAT_CALIFICACION T1,COMREL_CALIF_EPO T2 WHERE T2.IC_EPO=" + cadenasEpos + " AND T1.IC_CALIFICACION=T2.IC_CALIFICACION ORDER BY T2.IC_CALIFICACION";
    			pkValue    				=  GenericDML.getDataRow(strSQL).getData("CVEREGSIG").toString();
    			strValues  				+= pkValue + ", ";
 
    			// Verificar si la clave ya esta siendo usada
    			strSQL     				= "SELECT COUNT(IC_CALIFICACION) AS TOTAL FROM COMCAT_CALIFICACION";
				totalRegistros 		= Integer.parseInt( GenericDML.getDataRow(strSQL).getData("TOTAL").toString() );
				valorNuevaClavePk		= Integer.parseInt(pkValue);
 
				// En caso de que la clave siguiente ya se este utilizando, abortar insercion del nuevo registro
				if( valorNuevaClavePk > totalRegistros ){
					throw new AppException("Se agotaron las claves disponibles, se aborta la inserci�n del nuevo registro.");
    			}
 
    		} else {
    			
    			pkValue    = valorPK;
    			strValues += valorPK+", ";
 
    		}
    		
    	// 1. El Catalogo usa secuencia
      } else if(!catalogo.getSequenceName().equals("")) {
      	
        String valorPK = obtenerValor((useTablePrefix ? catalogo.getTableName() + "." : "") + catalogo.getPrimaryKeyName(), "0");

        strFields = catalogo.getPrimaryKeyName() + ", ";

        // Obtener nuevo valor de llave primaria
        if(valorPK.equals("") || valorPK.equals("0")){
          strSQL     =  "SELECT "+catalogo.getSequenceName() + ".NEXTVAL AS NEXTPK"+" FROM DUAL";
          pkValue    =  GenericDML.getDataRow(strSQL).getData("NEXTPK").toString();
          strValues  += pkValue + ", ";
        // Usar el valor de llave primaria especificado por el usuario
        } else {                                                         
          pkValue    = valorPK;
          strValues += valorPK+", ";
        }
        
      // 2. El Catalogo usa el esquema MAX VALUE + 1
      } else if ( catalogo.getIncrementKey() ){
      	
      	String valorPK = obtenerValor((useTablePrefix ? catalogo.getTableName() + "." : "") + catalogo.getPrimaryKeyName(), "0");
 
      	strFields  = catalogo.getPrimaryKeyName() + ", ";
      	
      	// Obtener nuevo valor de llave primaria
      	if(valorPK.equals("") || valorPK.equals("0")){ 
      		String pkColumn 	= (useTablePrefix ? catalogo.getTableName() + "." : "") + catalogo.getPrimaryKeyName();
      		strSQL     			=  "SELECT NVL(MAX(" + pkColumn + "),0)+1 AS NEXTPK FROM " + catalogo.getTableName();
         	pkValue    			=  GenericDML.getDataRow(strSQL).getData("NEXTPK").toString();
         	strValues  			+= pkValue + ", ";
         // Usar el valor de llave primaria especificado por el usuario
         } else {                                                         
         	pkValue    			= valorPK;
         	strValues 			+= valorPK + ", ";
      	}
      	
      	// Nota: A la clase QueryCondition podr�a agregarsele un flag para que sus condiciones se utilicen en este tipo de
      	// generaci�n de la llave primaria, por el momento se precinde de su implementaci�n
      	
      }
      
    }

    // II. Agregar al insert las QueryCondition que correspondan al perfil
    for(int m = 0; m < catalogo.getQueryConditionCount(); m++){
    	 
    	 QueryCondition queryCondition = catalogo.getQueryCondition(m);
    	 
    	 // Nota: por simplicidad no se verifica que la tabla del query condition coincida con la
    	 // configurada en el tag del catalogo, una futura version con mas caracteristicas podr�a
    	 // requerirla
    	 
    	// Determinar si el perfil del usuario tiene permiso de usar esta condici�n del query
      if( queryCondition.skipCondition( catalogo.getPerfilCatalogo() ) ){
      	continue;
      }
      
      // Obtener valor asociado
      String valor = null;
      if( 			QueryCondition.SESSION	== queryCondition.getValueType() ){
      	String attributeName = queryCondition.getValue();
      	valor 					= (String) request.getSession().getAttribute(attributeName);
      } else if(	QueryCondition.REQUEST	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
      	
      } else if(	QueryCondition.LOGICAL	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
      	
      } else if(	QueryCondition.VALUE 	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada.");
      	
      }
    	 
      // Agregar campos al insert
      strFields += queryCondition.getColumnName() + ", ";
      strValues += "?, ";
      arValues.add(valor);
              
    }
    
    // III. Agregar los campos especificados en el grid panel asociado al catalogo
    strSQL = "INSERT INTO " + catalogo.getTableName();

    for(int i = 0; i < catalogo.getFieldCount(); i++){
    	 
      Campo campo = catalogo.getField(i);

      // Si el campo con la llave primaria tambien se muestra en el grid panel, no volver considerar valor 
      // que pudiera tener el campo en esta lista ( el valor del campo ya fue leido en la seccion anterior )
      if( catalogo.getPrimaryKeyName().equalsIgnoreCase( campo.getFieldName() ) ){
         continue; 
      }
      
      // Excluir de esta lista aquellos campos que coincidan con los QueryConditions asociados al perfil
      if( catalogo.containsQueryConditionColumn( campo.getFieldName() ) ){
      	continue;
      }
      
      if(!campo.isFile){
      	
        if(!campo.isReadOnly||campo.getFieldName().toString().equals("VUSUARIO_ULT_MODIF")){
        	  
          strFields += campo.getFieldName() + ", ";

          String strDefault = "";

          Object defaultVal = campo.getDefaulValue();

          if(defaultVal != null)
            strDefault = defaultVal.toString().trim();
          else
            strDefault = null;

          String valor = obtenerValor((useTablePrefix ? catalogo.getTableName() + "." : "") + campo.getFieldName(), strDefault);

          if(valor != null)
            valor = valor.trim();
          else
            valor = "";

          if(valor.trim().toUpperCase().indexOf("NULL") > -1) valor = "";

          if(valor.equals("") && campo.isNullable)
          {
            strValues += "NULL, ";
          }
          else
          {
            if(campo.getType() instanceof String) {
              strValues += "?, ";
              arValues.add(valor);
            }
            else if(campo.getType() instanceof Date)
            {
              if(valor.equalsIgnoreCase("SYSDATE"))
              {
                strValues += "SYSDATE, ";
              }
              else
              {
                strValues += "TO_DATE(?, '" + campo.getFormat() + "'), ";
                arValues.add(valor);
              }
            }
            else
            {
              strValues += "?, ";
              try { arValues.add(new BigDecimal(valor)); } catch(Exception e) { arValues.add(valor); }
            }
          }
        }
      }
    }

    strFields = strFields.trim().substring(0, strFields.length() - 2);
    strValues = strValues.trim().substring(0, strValues.length() - 2);
    //Integer.parseInt("a");
    strSQL += " (" + strFields + ") VALUES (" + strValues + ")";
    retValue = executeUpdate(strSQL, arValues.toArray());
 
    retValue.put("PK_VALUE",pkValue);
    
    return retValue;
    
  }

  private String remplazarNoValidos(String cadena){
  	  
    char[] salida = cadena.toCharArray();
    StringBuffer retValue = new StringBuffer();

    for(int i = 0; i < salida.length; i++){
    	 
      if(salida[i] == '\''){
        retValue.append("'"); //retValue.append("&#39;");
      } else {
        retValue.append(salida[i]);
      }
      
    }
    return retValue.toString();
    
  }

  private String[] getPk(String pkName){
  	  
    String[] result = new String[] {"", ""};

    if(pkName.indexOf(",") == -1)
      result[0] = pkName;
    else
    {
      result[0] = pkName.substring(0, pkName.indexOf(",")).trim();
      result[1] = pkName.substring(pkName.indexOf(",") + 1).trim();
    }

    return result;
    
  }


  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }


  public String getUsuario() {
    return usuario;
  }
  
  public HashMap executeUpdate(String strSQL, Object[] values) {
 
	 log.info("executeUpdate(E)");
	 
	 AccesoDB 				con 			= null;
	 PreparedStatement 	ps 			= null;
	 boolean					bOk			= true;
	 HashMap 				respuesta 	= new HashMap();
	 
    log.debug("strSQL: " + strSQL);
    log.debug("VALUES=+" + values.toString());
	 
    try{
    
		con 		= new AccesoDB(); 
		con.conexionDB();
      ps = con.queryPrecompilado(strSQL);

      if(values != null){
			
        for(int i = 1; i <= values.length; i++){
			  
			 int k = i-1;
			 log.debug("VALUES("+k+")=+"+values[k]);
          
          if(values[k] == null){
				 
            log.debug("Setting parameter " + i + " to null.");
            ps.setNull(i, Types.VARCHAR);
				
          }else if(values[k] instanceof BigDecimal){
				 
            log.debug("Setting parameter " + i + " to " + values[k] + " as BigDecimal.");
            ps.setBigDecimal(i, (BigDecimal)values[k]);
				
          }else if(values[k] instanceof Long){
				 
            log.debug("Setting parameter " + i + " to " + values[k] + " as Long.");
            ps.setLong(i, ((Long)values[k]).longValue());
				
          }else if(values[k] instanceof Integer){
				 
            log.debug("Setting parameter " + i + " to " + values[k] + " as Integer.");
            ps.setInt(i, ((Integer)values[k]).intValue());
				
          }else{
				 
            log.debug("Setting parameter " + i + " to \"" + values[k] + "\" as String.");
            ps.setString(i, (String)values[k]);
				
          }
			 
        }
		  
      }      
      
      ps.executeUpdate();
 
    }catch(Exception e){
		 
		bOk = false;
		
		log.error("executeUpdate(Exception)");
      log.error("strSQL: " + strSQL);
      e.printStackTrace();
		
      // Enviar descripcion del error
      if( e instanceof SQLException ){
      	
      	SQLException 	sqlException = (SQLException) e;
      	int 				sqlErrorCode = sqlException.getErrorCode();
      	respuesta.put("SQL_ERROR_CODE", new Integer(sqlErrorCode) );
      	
      }
      respuesta.put("ERROR_MESSAGE", e.getMessage() );
      
    }finally{
    
		con.terminaTransaccion(bOk);
      
		if(ps != null) try { ps.close(); } catch(Exception e) {}	
      if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
		
		log.info("executeUpdate(S)");
			
    }
 
    respuesta.put("success", new Boolean(bOk));
    return respuesta;
    
  }
 
}