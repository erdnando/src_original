package com.netro.catalogos.dao;

import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;

import com.netro.catalogos.vo.Campo;
import com.netro.catalogos.vo.DataSet;
import com.netro.catalogos.vo.Catalogo;
import com.netro.catalogos.vo.QueryCondition;

import netropology.utilerias.ServiceLocator;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;

import org.apache.commons.logging.Log;
 
public class QueryCatalog 
{
  /**
   * Variable que se emplea para enviar mensajes al log.
   */
  private final static Log log = ServiceLocator.getInstance().getLog(QueryCatalog.class);
 
  //private Connection cn = null;
  //private String dataSource = "jdbc/EcontractDS";
  private HttpServletRequest 	request 			= null;
  
  private String obtenerValor(String strCampo, String strDefault)
  {
    String strValor = request.getParameter(strCampo);
    if(strValor != null && strValor.equals("-1")) strValor = null;
    return strValor == null?strDefault:strValor;
  }  
  
  /*
  public QueryCatalog(Connection cn, HttpServletRequest request)
  {
    this.cn = cn;
    this.request = request;
  }

  public QueryCatalog(String dataSource, HttpServletRequest request)
  {
    this.dataSource = dataSource;
    this.request = request;
  }
  */
  
  public QueryCatalog(HttpServletRequest request){
    this.request = request;
  }
  
  public DataSet getCatalogData(Catalogo catalogo, String orderBy)
  {
    return getCatalogData(catalogo, orderBy, false);
  }
  
  public DataSet getCatalogData(Catalogo catalogo, String orderBy, boolean overrideBody)
  {
    DataSet set = new DataSet();

    String strSQL = "SELECT ";
    int i = 0;
    boolean whereClause = false;      
    String strComodin = "";
		String strLastForeignTable = "";//FODEA EC-003-2011 ACF
    int fkTableId = 0;
    
    if(overrideBody) 
      catalogo.setSelectBody(null);

    log.debug("catalogo.getSelectBody()="+catalogo.getSelectBody()+"--");
          
    if(catalogo.getSelectBody() == null || catalogo.getSelectBody().trim().equals(""))
    {
      String strForeignKeys = "";
      String strForeignTables = "";

      log.debug("catalogo.haveForeignKeys="+catalogo.haveForeignKeys+"--");

      boolean useFk = false;
    
      if(catalogo.haveForeignKeys)
        strComodin = catalogo.getTableName() + ".";

      strSQL += strComodin + catalogo.getPrimaryKeyName() + ", ";
    
      for(i = 0; i < catalogo.getFieldCount(); i++)
      {
        Campo campo = catalogo.getField(i);
        
        if(campo.getFormat().trim().equals("")) 
        {

          if(campo.isForeignKey)
          {
            String fkTable = campo.getForeignKey().getTable().trim().toUpperCase();
            
            useFk = true;

            if(fkTable.equals(catalogo.getTableName().trim().toUpperCase()))
            {
              strForeignTables += fkTable + " TABLA_" + fkTableId + ",";
              fkTable = "TABLA_" + fkTableId + ",";
              fkTableId++;
            }
            else
            {
							//---------------------------------------------------------------- FODEA EC-003-2011 ACF
							if (!strLastForeignTable.equals(fkTable)) {
								strForeignTables += fkTable + ",";
							}
							//----------------------------------------------------------------
              //strForeignTables += fkTable + ",";
            }            
            
            strForeignKeys += strComodin + campo.getFieldName() + " = " + fkTable + "." + campo.getForeignKey().getFk();

            if(campo.isNullable)
            {
              strForeignKeys += "(+)";
            }

            strForeignKeys += " AND ";            
            strSQL += strComodin + campo.getFieldName() + ", " + fkTable + "." + campo.getForeignKey().getShowField() + " " + campo.getAlias() + ", ";
						strLastForeignTable = fkTable;//FODEA EC-003-2011 ACF
          }
          else
          {
            if(campo.getAllowedValues().size() > 0 && !campo.isForeignKey)
            {
              strSQL += "DECODE(" + strComodin + campo.getFieldName() + ", ";
          
              for(int j = 0; j < campo.getAllowedValues().size(); j++)
                strSQL += "'" + campo.getAllowedValue(j).getValue() + "', '" + campo.getAllowedValue(j).getName() + "', ";

              strSQL += "NULL) " + campo.getFieldName() + ", ";
            }
            else
            {
              strSQL += strComodin + campo.getFieldName() + ", ";
            }
          }
        }
        else
        {
          if(campo.getType() instanceof java.sql.Date)
          {
            strSQL += "TO_CHAR(" + strComodin + campo.getFieldName() + ", '" + campo.getFormat() + "') " + campo.getFieldName() + ", ";
          }
        }
      }
    
      strSQL = strSQL.trim();
      strSQL = strSQL.substring(0, strSQL.length() - 1);

      if(!strForeignTables.equals(""))
      {
        strForeignTables = strForeignTables.trim();
        strForeignTables = strForeignTables.substring(0, strForeignTables.length() - 1);      
      }

      if(!strForeignKeys.equals(""))
      {
        strForeignKeys = strForeignKeys.trim();
        strForeignKeys = strForeignKeys.substring(0, strForeignKeys.length() - 3).trim();      
      }    

      if(useFk)
      {
        whereClause = true;
        strSQL += " FROM " + catalogo.getTableName() + ", " + strForeignTables;
        strSQL += " WHERE " + strForeignKeys;
      }
      else
        strSQL += " FROM " + catalogo.getTableName();
    }
    
    for(i = 0; i < catalogo.getFieldCount(); i++)
    {
      Campo campo = catalogo.getField(i);
      
      if(campo.canSearch)
      {
        if(!obtenerValor(campo.getFieldName(), "").equals("") && !obtenerValor(campo.getFieldName(), "").equals("-1"))
        { 
          String valor = obtenerValor(campo.getFieldName(), "");
          String nombreCampo = strComodin + campo.getFieldName();
          
          if(campo.getType() instanceof BigDecimal)
          {
            log.debug("Es BigDecimal!");
          }
          else if(campo.getType() instanceof Date)
          {
            nombreCampo = "TRUNC(" + nombreCampo + ")";
            valor = "TO_DATE('" + valor.trim().toUpperCase() + "', '" + campo.getFormat() + "')";
          }
          else
          {
            //if(strComodin.trim().equals(""))
            //{
              nombreCampo = "UPPER(" + nombreCampo + ")";
            //}
            
            if(campo.getSearchType().trim().equals("like"))
              valor = "'%" + valor.trim().toUpperCase() + "%'";
            else
              valor = "'" + valor.trim().toUpperCase() + "'";
          }
          
          if(!whereClause) {
            strSQL += " WHERE " + nombreCampo + " " + campo.getSearchType().trim() + " " + valor;
            whereClause = true;
          }
          else
            strSQL += " AND " + nombreCampo + " " + campo.getSearchType().trim() + " " + valor;
        }
      }
    }

    // Agregar condiciones adicionales a la clausula WHERE si estas fueron especificadas
    for(i = 0; i < catalogo.getQueryConditionCount(); i++){
    	 
      QueryCondition queryCondition = catalogo.getQueryCondition(i);
 
      // Determinar si el perfil del usuario tiene permiso de usar esta condici�n del query
      if( queryCondition.skipCondition( catalogo.getPerfilCatalogo() ) ){
      	continue;
      }
 
      // Obtener valor asociado
      String valor = null;
      if( 			QueryCondition.SESSION	== queryCondition.getValueType() ){
      	String attributeName = queryCondition.getValue();
      	valor 					= (String) request.getSession().getAttribute(attributeName);
      } else if(	QueryCondition.REQUEST	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada");
      	
      } else if(	QueryCondition.LOGICAL	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada");
      	
      } else if(	QueryCondition.VALUE 	== queryCondition.getValueType() ){
      	
      	throw new AppException("Actualmente esta opci�n no se encunetra soportada");
      	
      }
      valor = valor == null?"":valor.replaceAll("'","''");
      
      // Agregar condicion a la clausula WHERE
      if(!whereClause) {
      	strSQL += " WHERE " + queryCondition.getColumnName() + " = '" + valor + "' ";
      	whereClause = true;
      } else {
      	strSQL += " AND "   + queryCondition.getColumnName() + " = '" + valor + "' ";
      }
 
    }
    
    if(orderBy.trim().equals(""))
    {
      if(!catalogo.getDefaultOrderBy().trim().equals(""))
        strSQL += " ORDER BY " + catalogo.getDefaultOrderBy() + " " + catalogo.getOrderByType();
    }
    else
    {
      strSQL += " ORDER BY " + orderBy;  
    }

    log.debug("strSQL: " + strSQL);
    
    set = GenericDML.getDataSet(strSQL);
    
    return set;
  }
 
}