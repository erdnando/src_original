package com.netro.catalogos.dao.util;

import com.netro.catalogos.web.ComboItem;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;

import netropology.utilerias.AccesoDB;

public class CatalogFkInfo 
{
  public static ArrayList getCatalogFkInfo(String table, String fkField, String showField) {
  	  return getCatalogFkInfo( table, fkField, showField, "" );
  }
  
  public static ArrayList getCatalogFkInfo(String table, String fkField, String showField, String in ) {
    ArrayList arResult = new ArrayList();
    
    //String strSQL = "SELECT " + fkField + ", " + showField + " FROM " + table + " WHERE CREGISTRO_ACTIVO_CK = 'S' ORDER BY 2";
	 String strSQL = "";
	 if( in == null || in.matches("\\s*") ){
	 	 strSQL = "SELECT " + fkField + ", " + showField + " FROM " + table + " ORDER BY 2";
	 } else {
	 	 strSQL = "SELECT " + fkField + ", " + showField + " FROM " + table + " WHERE " + fkField + " IN (" + in + ")" + " ORDER BY 2";
	 }

    AccesoDB            con   = null;
    PreparedStatement   ps    = null;
    ResultSet           rs    = null;
    
    try {
    
      con = new AccesoDB();
      con.conectarDB();
      ps = con.queryPrecompilado(strSQL);
      rs = ps.executeQuery();

      //arResult.add(new ComboItem(new BigDecimal("-1"), "-- Seleccione --"));
      
      while(rs.next())
      {
        ComboItem comboItem = new ComboItem((rs.getBigDecimal(1)).toPlainString(), rs.getString(2));
        arResult.add(comboItem);
      }
      
    }catch(Exception e){
    
      e.printStackTrace();
      
    }finally{
    
      if(rs != null) try { rs.close(); } catch(SQLException e) {}  
      if(ps != null) try { ps.close(); } catch(SQLException e) {}  
      
      if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
      
    }

    return arResult;
  }
  
}