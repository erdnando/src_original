package com.netro.descuento;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsDsctoAutomaticoEPO implements IQueryGeneratorRegExtJS {//DBM

	
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsDsctoAutomaticoEPO.class);

	StringBuffer qrySentencia = new StringBuffer("");
	StringBuffer	condicion  =new StringBuffer(); 		
	private List   conditions;

	private String tipoDescuento="";
	private String modalidad="";
	private String proceso = "";
	private String detalle = "N";
	private String noEpo = "";
		
	/*Constructor*/
	public ConsDsctoAutomaticoEPO() { }
	
  
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery() { 
		return "";
	}
	
	
	/**
	* Obtiene el query para la CONSULTA
	* @return Cadena con la consulta de SQL
	*/
	public String getDocumentQuery() { 
	// las  LLAVES  ::.......... Solo para la consulta del Detalle de Documentos de la EPO seleccionada
		log.info("getDocumentQuery(E) :::...");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
			
			qrySentencia.append( " SELECT DISTINCT p.ic_pyme " +
										" FROM com_docto_seleccionado DS, " +
										"    comcat_epo E, " +
										"    comcat_pyme p, " +
										"    comrel_nafin n,  " +
										"    comcat_if i,  " +
										"    com_documento d, " +
										"    com_acuse2 A " +
										" WHERE a.cc_acuse = ds.cc_acuse " +
										"    AND p.ic_pyme = d.ic_pyme " +
										"	  AND	 n.ic_epo_pyme_if = d.ic_pyme " +
										"    AND I.ic_if = DS.ic_if " +
										"    AND E.ic_epo = DS.ic_epo " +
										"    AND D.ic_documento = DS.ic_documento " +	
										"		and A.ic_proceso = ? " +
										"	  GROUP BY p.ic_pyme ");
 
			conditions.add(proceso);
		log.info("getDocumentQuery :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	* Obtiene el query para obtener la consulta
	* @return Cadena con la consulta de SQL, para obtener la consulta
	*/
	public String getDocumentSummaryQueryForIds(List pageIds) // Solo para la consulta del Detalle de Documentos de la EPO seleccionada
	{		
		log.info("getDocumentSummaryQueryForIds(E) :::...");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
		qrySentencia.append( " SELECT  E.cg_razon_social as NOMBRE_EPO, " +
									"		n.ic_nafin_electronico as NAFIN_ELECTRO, " + 
									"		p.cg_razon_social as  NOMBRE_PYME, " +
									"		p.cg_rfc  as RFCPYME,   " +
									"		COUNT (DECODE (d.ic_moneda, 1, d.ic_documento)) AS  NUMDOCTOSMN, " +
									"  	COUNT (DECODE (d.ic_moneda, 54, d.ic_documento)) AS NUMDOCTOSDL, " +
									"		NVL( SUM(CASE WHEN d.ic_moneda = 1 THEN fn_monto ELSE 0 END) , 0) AS MONTOMN,  " +
									"		NVL( SUM(CASE WHEN d.ic_moneda = 54 THEN fn_monto ELSE 0 END) , 0) AS MONTODL " +
									" FROM com_docto_seleccionado DS, " +
									"    comcat_epo E, " +
									"    comcat_pyme p, " +
									"    comrel_nafin n, " +
									"    comcat_if I,   " +
									"    com_documento D, " +
									"    com_acuse2 A " +
									" WHERE a.cc_acuse = ds.cc_acuse " +
									"    AND p.ic_pyme = d.ic_pyme  " +
									"    AND n.ic_epo_pyme_if = d.ic_pyme  " +
									"    AND I.ic_if = DS.ic_if " +
									"    AND E.ic_epo = DS.ic_epo " +
									"    AND D.ic_documento = DS.ic_documento " +
									//"    AND d.ic_epo = ? " + //    -- no de la EPO ---> por si las F
									//"	  AND a.cc_acuse = ? " +
									//"    AND d.ic_estatus_docto = 3 " );
									"		and A.ic_proceso = ? " );
			conditions.add(proceso);
			
			qrySentencia.append(" AND (");
			for(int i = 0; i < pageIds.size(); i++){
				List lItem = (ArrayList)pageIds.get(i);					
				if(i > 0){ qrySentencia.append("OR"); }
					qrySentencia.append("( p.ic_pyme = ? )");
				conditions.add(new Integer(lItem.get(0).toString()));					
			}		
			qrySentencia.append(" ) " +
			" GROUP BY E.cg_razon_social , n.ic_nafin_electronico,  p.cg_razon_social,  p.cg_rfc ");
		
		log.info("getDocumentSummaryQueryForIds :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
	// Consulta del Grid Principal en estatus 2
		log.info("getDocumentQueryFile(E) :::...");
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
		
			/* Tipo Descuento Normal */
			if (tipoDescuento.equals("N")  ) { 
				
				
				qrySentencia.append(	" SELECT   /*+index(d IN_COM_DOCUMENTO_04_NUK) use_nl(d b e )*/ "+
											"		b.ic_proceso, e.ic_epo, e.cg_razon_social, "+
											"		COUNT (DECODE (d.ic_moneda, 1, d.ic_documento)) AS numdoctosmn, "+
											"		COUNT (DECODE (d.ic_moneda, 54, d.ic_documento)) AS numdoctosdl, "+
											" 		TO_CHAR (b.df_fecha_hora_ini, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_INI, " + 
											" 		TO_CHAR (b.df_fecha_hora_fin, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_FIN, " + 
											"		b.cg_estatus " +" FROM  com_documento d, "+
											"		bit_proceso_automatico b, "+
											"		comcat_epo e "+
											" WHERE d.ic_epo = e.ic_epo "+
											"		AND d.cs_dscto_especial = 'N'   "+
											"		AND d.ic_estatus_docto = 2 "+
											"		AND d.ic_epo = b.ic_epo(+) "+
											"		AND b.cg_tipo_descuento(+) = 'N'   "+
											"		AND EXISTS ( "+
											"			SELECT /*+ index(a COMREL_DSCTO_AUT_PYME_01_NUK) */  1 "+
											"			FROM comrel_dscto_aut_pyme a "+
											"			WHERE d.ic_pyme = a.ic_pyme "+
											"			AND d.ic_epo = a.ic_epo "+
											"			AND d.ic_moneda = a.ic_moneda "+
											"			AND a.cs_dscto_automatico_dia = ? " +
											"			AND a.cc_tipo_factoraje = 'N'  "+
											"        AND a.cs_dscto_automatico_proc = 'S' ) "+
											"	GROUP BY b.ic_proceso, e.ic_epo, e.cg_razon_social, b.df_fecha_hora_ini, b.df_fecha_hora_fin, b.cg_estatus "+
											"	ORDER BY cg_razon_social");
		
				conditions.add(modalidad);
				
			}
			
			/* Tipo Descuento Vencido */
			else if (tipoDescuento.equals("V") ) { 
				
				qrySentencia.append(	" SELECT   /*+index(a COMREL_DSCTO_AUT_PYME_01_NUK, d IN_COM_DOCUMENTO_04_NUK) use_nl(d b e a )*/ " +
											" 		b.ic_proceso, e.ic_epo, e.cg_razon_social, " +
											"		COUNT (DECODE (d.ic_moneda, 1, d.ic_documento)) AS numdoctosmn, " +
											"		COUNT (DECODE (d.ic_moneda, 54, d.ic_documento)) AS numdoctosdl, " +
											" 		TO_CHAR (b.df_fecha_hora_ini, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_INI, " + 
											" 		TO_CHAR (b.df_fecha_hora_fin, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_FIN, " + 
											"		b.cg_estatus " +
											" FROM comrel_dscto_aut_pyme a, " +
											"		com_documento d, " +
											"		bit_proceso_automatico b, " +
											"		comcat_epo e " +
											" WHERE d.ic_epo = e.ic_epo " +
											"		AND d.cs_dscto_especial = 'V'   " +
											"		AND d.ic_epo = a.ic_epo " +
											"		AND d.ic_pyme = a.ic_pyme " +
											" 		AND d.ic_moneda = a.ic_moneda " +
											"		AND a.cc_tipo_factoraje = 'V' " +
											"		AND a.cs_dscto_automatico_dia = ?   " + //------------>
											"		AND d.ic_estatus_docto = 2  " +
											"		AND d.ic_epo = b.ic_epo(+) " +
											"		AND b.cg_tipo_descuento(+) = 'V'   " +
											"		and a.ic_if = d.ic_if " +
											"     AND a.cs_dscto_automatico_proc = 'S'  "+
											"	GROUP BY b.ic_proceso, e.ic_epo, e.cg_razon_social, b.df_fecha_hora_ini,  b.df_fecha_hora_fin, b.cg_estatus " +
											"	ORDER BY cg_razon_social");
	
	
				conditions.add(modalidad);				
			}
			
			/* Tipo Descuento Infonavit,	Automatico(IF), 	Mandato , Y   Distribuido  */
			else if (tipoDescuento.equals("I") || tipoDescuento.equals("A") || tipoDescuento.equals("M") || tipoDescuento.equals("D")) {
				
				qrySentencia.append( " SELECT    /*+index(doc IN_COM_DOCUMENTO_04_NUK) */ " +
											"		distinct  b.ic_proceso, e.ic_epo, e.cg_razon_social ,  " +
											"     COUNT (DECODE (doc.ic_moneda, 1, doc.ic_documento)) AS numdoctosmn, " +
											"     COUNT (DECODE (doc.ic_moneda, 54, doc.ic_documento)) AS numdoctosdl,  " +       
											" 		TO_CHAR (b.df_fecha_hora_ini, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_INI, " + 
											" 		TO_CHAR (b.df_fecha_hora_fin, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_FIN, " + 
											"		b.cg_estatus " +
											" FROM  com_documento doc, " +
											"     comcat_epo e, " +
											"     BIT_PROCESO_AUTOMATICO b " +
											" WHERE doc.cs_dscto_especial = ? " + /*---- ESTE VALOR CAMBIO CON RESPECTO COMBO DE   TIPO DESCUENTO */
											"     AND doc.ic_estatus_docto = 2 " +
											"     and e.ic_epo = doc.ic_epo " +
											"     and b.ic_epo(+) = doc.ic_epo " +
											"      AND b.cg_tipo_descuento(+) = ? " + /*---- ESTE VALOR CAMBIO CON RESPECTO COMBO DE   TIPO DESCUENTO */
											" GROUP BY e.ic_epo, e.cg_razon_social, b.DF_FECHA_HORA_INI , b.DF_FECHA_HORA_FIN  , b.CG_ESTATUS,  b.ic_proceso " +
											" ORDER BY ic_epo desc");	
				
				conditions.add(tipoDescuento);
				conditions.add(tipoDescuento);
				
			}
			
			
			/* Tipo Descuento Instrucción Irrevocable */
			else if (tipoDescuento.equals("II")) {
				qrySentencia.append( " SELECT   /*+index(doc IN_COM_DOCUMENTO_04_NUK) index(csm CP_COM_SOLIC_MAND_DOC_PK) use_nl(csm  doc )*/ " +
											"		distinct  b.ic_proceso, e.ic_epo, e.cg_razon_social ,  " +
											"     COUNT (DECODE (doc.ic_moneda, 1, doc.ic_documento)) AS numdoctosmn, " +
											"     COUNT (DECODE (doc.ic_moneda, 54, doc.ic_documento)) AS numdoctosdl, " +     
											" 		TO_CHAR (b.df_fecha_hora_ini, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_INI, " + 
											" 		TO_CHAR (b.df_fecha_hora_fin, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_FIN, " + 
											"		b.cg_estatus " +
											" FROM com_solic_mand_doc csm, comrel_producto_epo cpe, com_documento doc, " +
											" 		comcat_epo e, " +
											"     BIT_PROCESO_AUTOMATICO b " +
											" WHERE doc.ic_epo = csm.ic_epo " +
											"     AND doc.ic_pyme = csm.ic_pyme " +
											"     AND csm.ic_epo = cpe.ic_epo " +
											"     AND cpe.ic_producto_nafin = 1 " +
											"     AND doc.cs_dscto_especial IN ('N') " +
											"     AND doc.ic_estatus_docto = 2 " +
											"     AND csm.ic_estatus_man_doc = 2 " +
											"     AND csm.cs_dscto_aut_irrev IN ('S', 'N') " +
											"     AND e.ic_epo = doc.ic_epo " +
											"		AND b.ic_epo(+) = doc.ic_epo " +
											"		AND b.cg_tipo_descuento(+) = 'II' " +
											" GROUP BY e.ic_epo, e.cg_razon_social,  b.DF_FECHA_HORA_INI , b.DF_FECHA_HORA_FIN  , b.CG_ESTATUS,  b.ic_proceso " +
											" ORDER BY ic_epo desc");	
			}
		
			/* Tipo Descuento Factoraje Vencido EPO  */
			else if (tipoDescuento.equals("VE")) {
				qrySentencia.append( " SELECT   /*+index(d IN_COM_DOCUMENTO_04_NUK) */ " +
											"		distinct  b.ic_proceso, e.ic_epo, e.cg_razon_social ,  " +
											"     COUNT (DECODE (d.ic_moneda, 1, d.ic_documento)) AS numdoctosmn, " +
											"     COUNT (DECODE (d.ic_moneda, 54, d.ic_documento)) AS numdoctosdl, " +      
											" 		TO_CHAR (b.df_fecha_hora_ini, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_INI, " + 
											" 		TO_CHAR (b.df_fecha_hora_fin, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_FIN, " + 
											"		b.cg_estatus " +
											" FROM com_documento d, " +
											"     comcat_epo e, " +
											"     com_parametrizacion_epo pa, " +      
											"     BIT_PROCESO_AUTOMATICO b " +       
											" WHERE d.cs_dscto_especial IN ('V') " +
											"     AND d.ic_estatus_docto = 2 " +
											"     AND pa.ic_epo = e.ic_epo " +
											"     AND pa.ic_epo = d.ic_epo " +
											"     AND pa.cc_parametro_epo = 'PUB_EPO_DESCAUTO_FACTVENCIDO' " +
											"     AND pa.cg_valor = 'S'  " +    
											"     AND pa.ic_epo = e.ic_epo " +
											"     and e.ic_epo = d.ic_epo " +
											"     and b.ic_epo(+) = d.ic_epo " +  
											"     AND b.cg_tipo_descuento(+) = 'VE' " +    
											" GROUP BY e.ic_epo, e.cg_razon_social,  b.DF_FECHA_HORA_INI , b.DF_FECHA_HORA_FIN  , b.CG_ESTATUS,  b.ic_proceso " +
											" ORDER BY ic_epo desc");	
			}
	
			/* Tipo Descuento Automático EPO */
			else if (tipoDescuento.equals("N19")) {
				
				qrySentencia.append( " SELECT   /*+index(d IN_COM_DOCUMENTO_04_NUK) */ " +  
											"		distinct  b.ic_proceso, e.ic_epo, e.cg_razon_social , " +   
											"     COUNT (DECODE (d.ic_moneda, 1, d.ic_documento)) AS numdoctosmn, " +  
											"     COUNT (DECODE (d.ic_moneda, 54, d.ic_documento)) AS numdoctosdl, " +          
											" 		TO_CHAR (b.df_fecha_hora_ini, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_INI, " + 
											" 		TO_CHAR (b.df_fecha_hora_fin, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_FIN, " + 
											"		b.cg_estatus " +
											" FROM com_documento d,  " +          
											" 		comcat_epo e, " +           
											"     com_parametrizacion_epo pa, " +  
											"     BIT_PROCESO_AUTOMATICO b   " +         
											" WHERE d.cs_dscto_especial IN ('N') " +      
											"     AND d.ic_estatus_docto = 2 " +  
											"     AND pa.ic_epo = d.ic_epo " +  
											"     AND pa.cc_parametro_epo = 'DESC_AUT_ULTIMO_DIA' " +  
											"     AND pa.cg_valor = 'S' " +  
											"     and e.ic_epo = d.ic_epo " +  
											"     and b.ic_epo(+) = d.ic_epo " +  
											 "		AND b.cg_tipo_descuento(+) = 'N19' " +
											" GROUP BY e.ic_epo, e.cg_razon_social,  b.DF_FECHA_HORA_INI , b.DF_FECHA_HORA_FIN  , b.CG_ESTATUS,  b.ic_proceso " +  
											" ORDER BY ic_epo");	
			}
			
			
			//Distribuido 2do piso		 
			if ("D2".equals(tipoDescuento)  ) {	
				
			    qrySentencia.append(" SELECT   /*+index(d IN_COM_DOCUMENTO_04_NUK) use_nl(d b e )*/ "+
						" b.ic_proceso, e.ic_epo, e.cg_razon_social, "+
						" COUNT (DECODE (d.ic_moneda, 1, d.ic_documento)) AS numdoctosmn, "+
						" COUNT (DECODE (d.ic_moneda, 54, d.ic_documento)) AS numdoctosdl, "+
						" TO_CHAR (b.df_fecha_hora_ini, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_INI, " + 
						" TO_CHAR (b.df_fecha_hora_fin, 'dd/mm/yyyy HH:MI:SS') AS DF_FECHA_HORA_FIN, " + 
						" b.cg_estatus " +" FROM  com_documento d, "+
						" bit_proceso_automatico b, "+
						" comcat_epo e "+
						" WHERE d.ic_epo = e.ic_epo "+
						" AND d.cs_dscto_especial = 'D'   "+
						" AND d.ic_estatus_docto = 2 "+
						" AND d.ic_epo = b.ic_epo(+) "+
						" AND b.cg_tipo_descuento(+) = 'D2'   "+
						" AND EXISTS ( "+
						"	SELECT /*+ index(a COMREL_DSCTO_AUT_PYME_01_NUK) */  1 "+
						"		FROM comrel_dscto_aut_pyme a "+
						"		WHERE d.ic_pyme = a.ic_pyme "+
						"		AND d.ic_epo = a.ic_epo "+
						"		AND d.ic_moneda = a.ic_moneda "+
						"		AND a.cs_dscto_automatico_dia = ? " +
						"		AND a.cc_tipo_factoraje = 'D'  "+
						"        	AND a.cs_dscto_automatico_proc = 'S' ) "+
						" GROUP BY b.ic_proceso, e.ic_epo, e.cg_razon_social, b.df_fecha_hora_ini, b.df_fecha_hora_fin, b.cg_estatus "+
						" ORDER BY cg_razon_social");		
				conditions.add(modalidad);
				
			}
				
		//}
		log.info("getDocumentQueryFile :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQueryFile(S)"); 
		return qrySentencia.toString();	
	} 
	  
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) 
	{	
		return  "";
	}
	
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) CON paginador*/
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) 
	{
		String nombreArchivo = "";
		if("CSV".equals(tipo))
		{
				String linea = "";
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				StringBuffer 	contenidoArchivo 	= new StringBuffer();
				CreaArchivo 	archivo 			= new CreaArchivo();
				String temp = "", temp1 = "";
					
				try {
				
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
					writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
					buffer = new BufferedWriter(writer);
					buffer.write(linea);
					
					linea="EPO,N@E,Pyme,RFC,Documentos Moneda Nacional,Documentos Doláres, Monto Total Moneda Nacional, Monto Total Doláres" + "\n";					
					buffer.write(linea);
					
					while (reg.next()) 
					{
						String sEpo 			= (reg.getString("NOMBRE_EPO") == null ? "" : reg.getString("NOMBRE_EPO"));
						String noNafin			= (reg.getString("NAFIN_ELECTRO") == null ? "" : reg.getString("NAFIN_ELECTRO"));
						String sPyme			= (reg.getString("NOMBRE_PYME")== null) ? "": reg.getString("NOMBRE_PYME");
						String rfcPyme    	= (reg.getString("RFCPYME") == null ? "" : reg.getString("RFCPYME"));						
						String doctosMN 		= (reg.getString("NUMDOCTOSMN") == null ? "" : reg.getString("NUMDOCTOSMN"));
						String doctosDL 		= (reg.getString("NUMDOCTOSDL") == null ? "" : reg.getString("NUMDOCTOSDL"));
						String montoMN 		= (reg.getString("MONTOMN") == null ? "" : reg.getString("MONTOMN"));
						String montoDL		 	= (reg.getString("MONTODL") == null ? "" : reg.getString("MONTODL"));
						
						linea =	sEpo.replaceAll(",", "")+","+ noNafin+","+ sPyme.replaceAll(",", "")+","+ rfcPyme+","+doctosMN+","+doctosDL+","+Comunes.formatoDecimal(montoMN,2,false)+","+Comunes.formatoDecimal(montoDL,2,false)+"\n";
						buffer.write(linea);
					}				
						
					buffer.close();
				} catch (Throwable e) 
				{
					throw new AppException("Error al generar el archivo ", e);
				} 
		}
		return  nombreArchivo;
	}
	
	 
	
	/*****************************************************
									SETTERS         
	*******************************************************/
	
	
	public void setTipoDescuento (String tipoDescuento) { this.tipoDescuento = tipoDescuento; }
	
	public void setModalidad (String modalidad) { this.modalidad = modalidad; }
	
	public void setProceso (String proceso) { this.proceso = proceso; }
	
	public void setConsultaDetalle (String detalle) { this.detalle = detalle; }
	
	public void setNoEpo (String noEpo) { this.noEpo = noEpo; }
	/*****************************************************
								GETTERS
	*******************************************************/
	
	
	public String getTipoDescuento() { return tipoDescuento; }
	
	public String getModalidad() { return modalidad ; }
	
	public String getProceso() { return proceso ; }
	
	public String getNoEpo() { return noEpo ; }
	
	public List getConditions() {  return conditions;  }  
  
	

	

}