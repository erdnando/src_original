/**
 * Correcci�n: 26/11/2012 - getDocumentQueryFile() :: 
 * Se estaba generando informaci�n en base al ic_if=3, reemplazando el valor por el seleccionado en tiempo de ejecuci�n
 */

package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;

public class VencIfDE implements IQueryGenerator, IQueryGeneratorRegExtJS{
  public VencIfDE()
  {
  }  
  
 
	public String getAggregateCalculationQuery(HttpServletRequest request)
  {
    String condicion = "";
    StringBuffer qrySentencia = new StringBuffer();
    
	String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
	String cliente = (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
	String prestamo = ( request.getParameter("prestamo") == null) ? "" :  request.getParameter("prestamo");
	String FechaCorte = (request.getParameter("FechaCorte") == null) ? "" : request.getParameter("FechaCorte");
	String moneda = (request.getParameter("moneda") == null) ? "" : request.getParameter("moneda");
//	String FechaInicio = (request.getParameter("FechaInicio") == null) ? "" : request.getParameter("FechaInicio");
//	String FechaFin = (request.getParameter("FechaFin") == null) ? "" : request.getParameter("FechaFin");
//	String FechaFin = (request.getParameter("FechaFin") == null) ? "" : request.getParameter("FechaFin");
    try{
			qrySentencia.append(
				"select " +
				"    /*+ " +
				"        INDEX (V IN_COM_VENCIMIENTO_01_NUK) " +
				"    */  " +
				"        count(1) , " +
				"        sum(fg_sdoinsoluto), " +
				"        sum(fg_amortizacion), " +
				"        sum(fg_interes), " +
				"        sum(fg_totdescfop), " +
				"        sum(fg_totdescfinape), " +
				"        sum(fg_totalvencimiento), " +
				"        sum(fg_adeudototal), " +
				"        sum(fg_sdoinsnvo), " +
				"        M.cd_nombre, " +
				"			sum(fg_totalexigible),"+
				"    	 'VencIfDE::getAggregateCalculationQuery' " +
				"from  " +
				"        COM_VENCIMIENTO V, " +
				"        COMCAT_MONEDA M " +
				"where   V.ic_moneda = M.ic_moneda " +
				"    and V.ic_if = " + ic_if ); 

         if(!cliente.equals(""))
         	condicion += " and v.ig_cliente="+cliente;
         if(!prestamo.equals(""))
         	condicion += " and v.ig_prestamo="+prestamo;
         	
         if(!FechaCorte.equals(""))
            condicion += " AND com_fechaprobablepago >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND com_fechaprobablepago < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";

				 if(!moneda.equals(""))
						condicion += " and V.ic_moneda = "+moneda;
         	
        /* if(!FechaInicio.equals(""))
            condicion += " and com_fechaprobablepago between to_date('"+FechaInicio+"', 'dd/mm/yyyy') " +
               " and to_date('"+ FechaFin +"', 'dd/mm/yyyy') ";
         else
            condicion += " and to_char(com_fechaprobablepago, 'dd/mm/yyyy') = to_char(sysdate, 'dd/mm/yyyy')";*/
 
        qrySentencia.append(" " + condicion);
     	qrySentencia.append(" group by v.ic_moneda,m.cd_nombre");
      	qrySentencia.append(" order by v.ic_moneda");
	       
    	System.out.println("EL query del AggregateCalculationQuery es: "+qrySentencia.toString());
    
    }catch(Exception e){
      System.out.println("VencIfDE::getAggregateCalculationQuery "+e);
    }
    return qrySentencia.toString();
  }

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids)
  {
  	
 	int i=0;
    StringBuffer qrySentencia = new StringBuffer();
 	
		qrySentencia.append(
			"select " +
			"    /*+ " +
			"        INDEX (V CP_COM_VENCIMIENTO_PK) " +
			"        INDEX (I CP_COMCAT_IF_PK) " +
			"        INDEX (M CP_COMCAT_MONEDA_PK) " +
			"    */  " +
			"        ig_cliente, cg_nombrecliente, to_char(com_fechaprobablepago, 'dd/mm/yyyy'),  " +
			"        ig_prestamo, ig_numelectronico, cg_esquematasas, fg_margen, fg_sdoinsoluto, fg_amortizacion,  " +
			"        fg_interes, fg_totdescfop, fg_totdescfinape, fg_totalvencimiento, fg_adeudototal,  " +
			"        fg_sdoinsnvo, V.ic_moneda, M.cd_nombre,  " +
			"        decode(I.ig_tipo_piso,1,'',2,ic_financiera) as ic_financiera,  " +
			"        cg_modalidadpago, cg_razon_social, ig_baseoperacion, cg_descbaseoper,  " +
			"        ig_sucursal, ig_proyecto, ig_contrato, cg_bursatilizado, ig_disposicion, ig_subaplicacion,  " +
			"        ig_cuotasemit, ig_cuotasporvenc, ig_totalcuotas, cg_aniomodalidad, ig_tipoestrato, to_char(df_fechaopera, 'dd/mm/yyyy'),  " +
			"        ig_sancionado, ig_frecuenciacapital, ig_frecuenciainteres, ig_tasabase, cg_relmat_1,  " +
			"        fg_spread_1, cg_relmat_2, fg_spread_2, cg_relmat_3, fg_spread_3, cg_relmat_4,  " +
			"        ig_tipogarantia, fg_porcdescfop, fg_porcdescfinape,  " +
			"        fg_fcrecimiento, to_char(df_periodoinic, 'dd/mm/yyyy'), to_char(df_periodofin, 'dd/mm/yyyy'), ig_dias, fg_comision, fg_porcgarantia,  " +
			"        fg_porccomision, fg_pagotrad,  " +
			"        fg_subsidio, fg_totalexigible, fg_capitalvencido, fg_interesvencido, fg_totalcarven,  " +
			"        fg_finadicotorgado, fg_finadicrecup, fg_sdoinsbase, fg_intcobradoxanticip," +
			" 			 I.ic_financiera as financiera,"+
			" 			 v.ig_numero_docto as numero_documento,"+//FODEA 015 - 2009
      
			"    	 'VencIfDE::getDocumentSummaryQueryFile' " +
			"from  " +
			"        COM_VENCIMIENTO V,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   V.ic_moneda = M.ic_moneda  " +
			"    and V.ic_if = I.ic_if " +
	        "	 and V.ic_vencimiento in ( ");
	
	for (Iterator it = ids.iterator(); it.hasNext();i++){
    	if(i>0)
      		qrySentencia.append(",");
		qrySentencia.append("'"+it.next().toString()+"'");
	}
	
	qrySentencia.append(") " +
		" order by ic_moneda, ig_cliente, to_char(com_fechaprobablepago, 'dd/mm/yyyy')");
	
    System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
	
	return qrySentencia.toString();

  }
	
	public String getDocumentQuery(HttpServletRequest request)
	{
    String condicion = "";
    StringBuffer qrySentencia = new StringBuffer();
    
	String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
	String cliente = (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
	String prestamo = ( request.getParameter("prestamo") == null) ? "" :  request.getParameter("prestamo");
	String FechaCorte = (request.getParameter("FechaCorte") == null) ? "" : request.getParameter("FechaCorte");
	String moneda = (request.getParameter("moneda") == null) ? "" : request.getParameter("moneda");
//	String FechaInicio = (request.getParameter("FechaInicio") == null) ? "" : request.getParameter("FechaInicio");
//	String FechaFin = (request.getParameter("FechaFin") == null) ? "" : request.getParameter("FechaFin");

    try{
			qrySentencia.append(
				"select " +
				"    /*+ " +
				"        INDEX (V IN_COM_VENCIMIENTO_01_NUK) " +
				"    */  " +
				"        IC_VENCIMIENTO, " +
				"    	 'VencIfDE::getDocumentQuery' " +
				"from  " +
				"        COM_VENCIMIENTO " +
				"WHERE   ic_if = " + ic_if); 

        if(!cliente.equals(""))
         	condicion += " and ig_cliente="+cliente;
        if(!prestamo.equals(""))
         	condicion += " and ig_prestamo="+prestamo;
        if(!FechaCorte.equals(""))
            condicion += " AND com_fechaprobablepago >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND com_fechaprobablepago < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
 			  if(!moneda.equals(""))
						condicion += " and ic_moneda = "+moneda;
         	
 /*        if(!FechaInicio.equals(""))
            condicion += " and com_fechaprobablepago between to_date('"+FechaInicio+"', 'dd/mm/yyyy') " +
               " and to_date('"+ FechaFin +"', 'dd/mm/yyyy') ";
         else
            condicion += " and to_char(com_fechaprobablepago, 'dd/mm/yyyy') = to_char(sysdate, 'dd/mm/yyyy')";*/
 
	     qrySentencia.append(" " + condicion +
	       	" order by ic_moneda, ig_cliente, to_char(com_fechaprobablepago, 'dd/mm/yyyy') ");
				       
    	 System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
    	 
    }catch(Exception e){
      System.out.println("VencIfDE::getDocumentQueryException "+e);
    }
    return qrySentencia.toString();
  }
	
	public String getDocumentQueryFile(HttpServletRequest request)
  {
    //String condicion = "";
    //StringBuffer qrySentencia = new StringBuffer();
    
	String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
	String cliente = (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
	String prestamo = ( request.getParameter("prestamo") == null) ? "" :  request.getParameter("prestamo");
	String FechaCorte = (request.getParameter("FechaCorte") == null) ? "" : request.getParameter("FechaCorte");
	String moneda = (request.getParameter("moneda") == null) ? "" : request.getParameter("moneda");
//	String FechaInicio = (request.getParameter("FechaInicio") == null) ? "" : request.getParameter("FechaInicio");
//	String FechaFin = (request.getParameter("FechaFin") == null) ? "" : request.getParameter("FechaFin");
	String qry = "";

    try{
			ArmaQueryFile queryf = new ArmaQueryFile();
    		qry = queryf.regresaQueryVenc(ic_if,cliente,prestamo,FechaCorte,moneda);   
			/*
			qrySentencia.append(
        
			"select " +
			"    /*+ " +
			"        INDEX (V IN_COM_VENCIMIENTO_01_NUK) " +
			"    */ /* " +
			" to_char(v.com_fechaprobablepago,'dd/mm/yyyy') as FechaProbablePago   "   +
			" ,v.CG_MODALIDADPAGO as ModalidadPago       "   +
			" ,v.IC_MONEDA as CodMoneda           "   +
			" ,m.cd_nombre as DescMoneda          "   +
			" ,v.ic_if as Intermediario       "   +
			" ,i.cg_razon_social as DescIntermediario   "   +
			" ,v.IG_BASEOPERACION as BaseOperacion       "   +
			" ,v.cg_descbaseoper as DescBaseOper        "   +
			" ,v.ig_cliente as Cliente             "   +
			" ,v.cg_nombrecliente as Nombre              "   +
			" ,v.ig_sucursal as Sucursal            "   +
			" ,v.ig_proyecto as Proyecto            "   +
			" ,v.IG_CONTRATO as Contrato            "   +
			" ,v.IG_PRESTAMO as Prestamo            "   +
			" ,v.IG_NUMELECTRONICO as NumElectronico      "   +
			" ,v.CG_BURSATILIZADO as Bursatilizado       "   +
			" ,v.IG_DISPOSICION as Disposicion         "   +
			" ,v.IG_SUBAPLICACION as SubAplicacion       "   +
			" ,v.IG_CUOTASEMIT as CuotasEmit          "   +
			" ,v.IG_CUOTASPORVENC as CuotasPorVenc       "   +
			" ,v.IG_TOTALCUOTAS as TotalCuotas         "   +
			" ,v.CG_ANIOMODALIDAD as AnioModalidad       "   +
			" ,v.IG_TIPOESTRATO as TipoEstrato         "   +
			" ,to_char(v.DF_FECHAOPERA,'dd/mm/yyyy') as FechaOpera          "   +
			" ,v.IG_SANCIONADO as Sancionado          "   +
			" ,v.IG_FRECUENCIACAPITAL as FrecuenciaCapital   "   +
			" ,v.IG_FRECUENCIAINTERES as FrecuenciaInteres   "   +
			" ,v.IG_TASABASE as TasaBase            "   +
			" ,v.CG_ESQUEMATASAS as EsquemaTasas        "   +
			" ,v.CG_RELMAT_1 as RelMat_1            "   +
			" ,v.FG_SPREAD_1 as Spread_1            "   +
			" ,v.CG_RELMAT_2 as RelMat_2            "   +
			" ,v.FG_SPREAD_2 as Spread_2            "   +
			" ,v.CG_RELMAT_3 as RelMat_3            "   +
			" ,v.FG_SPREAD_3 as Spread_3            "   +
			" ,v.CG_RELMAT_4 as RelMat_4            "   +
			" ,v.FG_MARGEN as Margen              "   +
			" ,v.IG_TIPOGARANTIA as TipoGarantia        "   +
			" ,v.FG_PORCDESCFOP as PorcDescFop         "   +
			" ,v.FG_TOTDESCFOP as TotDescFop          "   +
			" ,v.FG_PORCDESCFINAPE as PorcDescFinape      "   +
			" ,v.FG_TOTDESCFINAPE as TotDescFinape       "   +
			" ,v.FG_FCRECIMIENTO as FCrecimiento        "   +
			" ,to_char(v.DF_PERIODOINIC,'dd/mm/yyyy') as PeriodoInic         "   +
			" ,to_char(v.DF_PERIODOFIN,'dd/mm/yyyy') as PeriodoFin          "   +
			" ,v.IG_DIAS as Dias                "   +
			" ,v.FG_COMISION as Comision            "   +
			" ,v.FG_PORCGARANTIA as PorcGarantia        "   +
			" ,v.FG_PORCCOMISION as PorcComision        "   +
			" ,v.FG_SDOINSOLUTO as SdoInsoluto         "   +
			" ,v.FG_AMORTIZACION as Amortizacion        "   +
			" ,v.FG_INTERES as Interes             "   +
			" ,v.FG_INTCOBRADOXANTICIP as IntCobAnt           "   +
			" ,v.FG_TOTALVENCIMIENTO as TotalVencimiento    "   +
			" ,v.FG_PAGOTRAD as PagoTrad            "   +
			" ,v.FG_SUBSIDIO as Subsidio            "   +
			" ,v.FG_TOTALEXIGIBLE as TotalExigible       "   +
			" ,v.FG_CAPITALVENCIDO as CapitalVencido      "   +
			" ,v.FG_INTERESVENCIDO as InteresVencido      "   +
			" ,v.FG_TOTALCARVEN as TotalCarVen         "   +
			" ,v.FG_ADEUDOTOTAL as AdeudoTotal         "   +
			" ,v.FG_FINADICOTORGADO as FinAdicOtorgado     "   +
			" ,v.FG_FINADICRECUP as FinAdicRecup        "   +
			" ,v.FG_SDOINSNVO as SdoInsNvo           "   +
			" ,v.FG_SDOINSBASE as SdoInsBase"  +
			" ,I.ic_financiera as financiera"  +
			" ,'VencIfDE::getDocumentQueryFile' " +
			"from  " +
			"        COM_VENCIMIENTO V,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   V.ic_moneda = M.ic_moneda  " +
			"    and V.ic_if = I.ic_if " +
			"	 and V.ic_if = " + ic_if);

        if(!cliente.equals(""))
         	condicion += " and v.ig_cliente="+cliente;
         if(!prestamo.equals(""))
         	condicion += " and v.ig_prestamo="+prestamo;
         if(!FechaCorte.equals(""))
            condicion += " AND com_fechaprobablepago >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND com_fechaprobablepago < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
				 if(!moneda.equals(""))
						condicion += " and V.ic_moneda = "+moneda;*/
         	
         	
        /* if(!FechaInicio.equals(""))
            condicion += " and com_fechaprobablepago between to_date('"+FechaInicio+"', 'dd/mm/yyyy') " +
               " and to_date('"+ FechaFin +"', 'dd/mm/yyyy') ";
         else
            condicion += " and to_char(com_fechaprobablepago, 'dd/mm/yyyy') = to_char(sysdate, 'dd/mm/yyyy')";*/
 
	  /*   qrySentencia.append(" " + condicion +
	       	" order by v.ic_moneda, ig_cliente, to_char(com_fechaprobablepago, 'dd/mm/yyyy') ");*/

	    System.out.println("EL query de DocumentQueryFile es: "+qry);//qrySentencia.toString());
      
    }catch(Exception e){
			e.printStackTrace();
      System.out.println("VencIfDE::getDocumentQueryFileException "+e);
    }
    return qry;//qrySentencia.toString();
  }
   /*****************************************************************************
	*										MIGRACION												 *
	*										garellano												 *
	*                         	 19/junio/2012										 		 *  
	****************************************************************************/
	public  StringBuffer strQuery;
	private List conditions = new ArrayList(); 
	private String paramNumDocto;
	private String parametro;
	private String moneda;
	private String cliente;
	private String prestamo;
	private String FechaCorte;
  private String tipoLinea;
	private String ic_if;
	private Registros vecTotales;
	
	/**     
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() { // Obtiene los totales 
		String condicion = "";
		StringBuffer qrySentencia = new StringBuffer();

		qrySentencia.append(
			"select " +
			"    /*+ " +
			"        INDEX (V IN_COM_VENCIMIENTO_01_NUK) " +
			"    */  " +
			"        count(1) , " +
			"        sum(fg_sdoinsoluto), " +
			"        sum(fg_amortizacion), " +
			"        sum(fg_interes), " +
			"        sum(fg_totdescfop), " +
			"        sum(fg_totdescfinape), " +
			"        sum(fg_totalvencimiento), " +
			"        sum(fg_adeudototal), " +
			"        sum(fg_sdoinsnvo), " +
			"        M.cd_nombre, " +
			"			sum(fg_totalexigible),"+
			"    	 'VencIfDE::getAggregateCalculationQuery' " +
			"from  " +
			"        COM_VENCIMIENTO V, " +
			"        COMCAT_MONEDA M " +
			"where   V.ic_moneda = M.ic_moneda " +
            "    and V.ic_if = " + ic_if ); 
	
		if(!cliente.equals(""))   
			condicion += " and v.ig_cliente="+cliente;
		if(!prestamo.equals(""))
			condicion += " and v.ig_prestamo="+prestamo;
		
		if(!FechaCorte.equals(""))
			condicion += " AND com_fechaprobablepago >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND com_fechaprobablepago < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
		
		if(!moneda.equals(""))
			condicion += " and V.ic_moneda = "+moneda;
		
		qrySentencia.append(" " + condicion);
		qrySentencia.append(" group by v.ic_moneda,m.cd_nombre");
		qrySentencia.append(" order by v.ic_moneda");
		
		System.out.println("EL query del AggregateCalculationQuery es: "+qrySentencia.toString());

		return qrySentencia.toString();
 	}  
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */	 
	public String getDocumentQuery(){  // para las llaves primarias		
		String condicion = "";
		StringBuffer qrySentencia = new StringBuffer();

		qrySentencia.append(
			"select " +
			"    /*+ " +
			"        INDEX (V IN_COM_VENCIMIENTO_01_NUK) " +
			"    */  " +
			"        IC_VENCIMIENTO, " +
			"    	 'VencIfDE::getDocumentQuery' " +
			"from  " +
			"        COM_VENCIMIENTO " +
			"WHERE   ic_if = " + ic_if); 
	
		if(!cliente.equals(""))
			condicion += " and ig_cliente="+cliente;
		if(!prestamo.equals(""))
			condicion += " and ig_prestamo="+prestamo;
		if(!FechaCorte.equals(""))
			condicion += " AND com_fechaprobablepago >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND com_fechaprobablepago < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
		if(!moneda.equals(""))
			condicion += " and ic_moneda = "+moneda;

		qrySentencia.append(" " + condicion +
		" order by ic_moneda, ig_cliente, to_char(com_fechaprobablepago, 'dd/mm/yyyy') ");
		
		System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		return qrySentencia.toString();
 	}  
	   
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */	  		
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 			
		StringBuffer qrySentencia = new StringBuffer();
		
		qrySentencia.append(
			"select " +
			"    /*+ " +
			"        INDEX (V CP_COM_VENCIMIENTO_PK) " +
			"        INDEX (I CP_COMCAT_IF_PK) " +
			"        INDEX (M CP_COMCAT_MONEDA_PK) " +
			"    */  " +
			"        ig_cliente, cg_nombrecliente, to_char(com_fechaprobablepago, 'dd/mm/yyyy'),  " +
			"        ig_prestamo, ig_numelectronico, cg_esquematasas, fg_margen, fg_sdoinsoluto, fg_amortizacion,  " +
			"        fg_interes, fg_totdescfop, fg_totdescfinape, fg_totalvencimiento, fg_adeudototal,  " +
			"        fg_sdoinsnvo, V.ic_moneda, M.cd_nombre,  " +
			"        decode(I.ig_tipo_piso,1,'',2,ic_financiera) as ic_financiera,  " +
			"        cg_modalidadpago, cg_razon_social, ig_baseoperacion, cg_descbaseoper,  " +
			"        ig_sucursal, ig_proyecto, ig_contrato, cg_bursatilizado, ig_disposicion, ig_subaplicacion,  " +
			"        ig_cuotasemit, ig_cuotasporvenc, ig_totalcuotas, cg_aniomodalidad, ig_tipoestrato, to_char(df_fechaopera, 'dd/mm/yyyy'),  " +
			"        ig_sancionado, ig_frecuenciacapital, ig_frecuenciainteres, ig_tasabase, cg_relmat_1,  " +
			"        fg_spread_1, cg_relmat_2, fg_spread_2, cg_relmat_3, fg_spread_3, cg_relmat_4,  " +
			"        ig_tipogarantia, fg_porcdescfop, fg_porcdescfinape,  " +
			"        fg_fcrecimiento, to_char(df_periodoinic, 'dd/mm/yyyy'), to_char(df_periodofin, 'dd/mm/yyyy'), ig_dias, fg_comision, fg_porcgarantia,  " +
			"        fg_porccomision, fg_pagotrad,  " +
			"        fg_subsidio, fg_totalexigible, fg_capitalvencido, fg_interesvencido, fg_totalcarven,  " +
			"        fg_finadicotorgado, fg_finadicrecup, fg_sdoinsbase, fg_intcobradoxanticip," +
			" 			 I.ic_financiera as financiera,"+
			" 			 v.ig_numero_docto as numero_documento,"+//FODEA 015 - 2009
			
			"    	 'VencIfDE::getDocumentSummaryQueryFile' " +
			"from  " +
			"        COM_VENCIMIENTO V,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   V.ic_moneda = M.ic_moneda  " +
			"    and V.ic_if = I.ic_if " +
			"	 and V.ic_vencimiento in ( ");
		   
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0){ qrySentencia.append(","); }
			qrySentencia.append("'"+lItem.get(0).toString()+"'");
		}
		
		qrySentencia.append(") " +
			" order by ic_moneda, ig_cliente, to_char(com_fechaprobablepago, 'dd/mm/yyyy')");
		
		System.out.println("\nQuery de getDocumentSummaryQueryForIds: "+qrySentencia.toString());

		return qrySentencia.toString();
 	}  
	
	public String getDocumentQueryFile(){ // para todos los registros				
		String qry = "";
		ArmaQueryFile queryf = new ArmaQueryFile();
		qry = queryf.regresaQueryVenc((("C".equals(tipoLinea))?"12":ic_if),cliente,prestamo,FechaCorte,moneda);
		System.out.println("EL query de DocumentQueryFile es: "+qry);//qrySentencia.toString());
		return qry;//qrySentencia.toString();
 	}     
	         
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";

		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		
		if ("TXT".equals(tipo) || "ZIP".equals(tipo) ) {//AQUI COMIENZA ...
			try {
				int registros = 0;	
				if("ZIP".equals(tipo)) {
          			FechaCorte = FechaCorte.replace('/','_');	
          			nombreArchivo = "archvenc_"+FechaCorte+"_"+ic_if+"_S.txt";
        		} else {
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
        		}
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, false),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
				
				contenidoArchivo = new StringBuffer();	
				contenidoArchivo.append("FechaProbablePago;ModalidadPago;CodMoneda;DescMoneda;Intermediario;DescIntermediario;BaseOperacion;DescBaseOper;"+
                                "Cliente;Nombre;Sucursal;Proyecto;Contrato;Prestamo;NumElectronico;Bursatilizado;Disposicion;SubAplicacion;CuotasEmit;"+
                                "CuotasPorVenc;TotalCuotas;AnioModalidad;TipoEstrato;FechaOpera;Sancionado;FrecuenciaCapital;FrecuenciaInteres;TasaBase;"+
                                "EsquemaTasas;RelMat_1;Spread_1;RelMat_2;Spread_2;RelMat_3;Spread_3;RelMat_4;Margen;TipoGarantia;"+
                                //"PorcDescFop;TotDescFop; PorcDescFinape;TotDescFinape;"+
                                "FCrecimiento;PeriodoInic;PeriodoFin;Dias;Comision;PorcGarantia;PorcComision;SdoInsoluto;"+
                                "Amortizacion;Interes;IntCobAnt;TotalVencimiento;PagoTrad;Subsidio;TotalExigible;CapitalVencido;InteresVencido;TotalCarVen;"+
                                "AdeudoTotal;FinAdicOtorgado;FinAdicRecup;SdoInsNvo;SdoInsBase;CuentaCLABE");
				
				while (rs.next()) {
					String fpp = rs.getString("FechaProbablePago")==null?"":rs.getString("FechaProbablePago");
					String mp = rs.getString("ModalidadPago")==null?"":rs.getString("ModalidadPago");				
					String cm = rs.getString("CodMoneda")==null?"":rs.getString("CodMoneda");
					String dm = rs.getString("DescMoneda")==null?"":rs.getString("DescMoneda");
					String inter = rs.getString("Intermediario")==null?"":rs.getString("Intermediario");
					String di = rs.getString("DescIntermediario")==null?"":rs.getString("DescIntermediario");				
					String bo = rs.getString("BaseOperacion")==null?"":rs.getString("BaseOperacion");
					String dbo = rs.getString("DescBaseOper")==null?"":rs.getString("DescBaseOper");
					String cli = rs.getString("Cliente")==null?"":rs.getString("Cliente");
					String nom = rs.getString("Nombre")==null?"":rs.getString("Nombre");				
					String suc = rs.getString("Sucursal")==null?"":rs.getString("Sucursal");
					String pro = rs.getString("Proyecto")==null?"":rs.getString("Proyecto");
					String con = rs.getString("Contrato")==null?"":rs.getString("Contrato");
					String pre = rs.getString("Prestamo")==null?"":rs.getString("Prestamo");				
					String ne = rs.getString("NumElectronico")==null?"":rs.getString("NumElectronico");
					String bur = rs.getString("Bursatilizado")==null?"":rs.getString("Bursatilizado");
					String dis = rs.getString("Disposicion")==null?"":rs.getString("Disposicion");
					String suba = rs.getString("SubAplicacion")==null?"":rs.getString("SubAplicacion");				
					String ce = rs.getString("CuotasEmit")==null?"":rs.getString("CuotasEmit");
					String cpv = rs.getString("CuotasPorVenc")==null?"":rs.getString("CuotasPorVenc");
					String tc = rs.getString("TotalCuotas")==null?"":rs.getString("TotalCuotas");
					String am = rs.getString("AnioModalidad")==null?"":rs.getString("AnioModalidad");				
					String te = rs.getString("TipoEstrato")==null?"":rs.getString("TipoEstrato");
					String fo = rs.getString("FechaOpera")==null?"":rs.getString("FechaOpera");
					String san = rs.getString("Sancionado")==null?"":rs.getString("Sancionado");
					String fc = rs.getString("FrecuenciaCapital")==null?"":rs.getString("FrecuenciaCapital");
					String fi = rs.getString("FrecuenciaInteres")==null?"":rs.getString("FrecuenciaInteres");
					String tb = rs.getString("TasaBase")==null?"":rs.getString("TasaBase");				
					String et = rs.getString("EsquemaTasas")==null?"":rs.getString("EsquemaTasas");
					String rm1 = rs.getString("RelMat_1")==null?"":rs.getString("RelMat_1");
					String sp1 = rs.getString("Spread_1")==null?"":rs.getString("Spread_1");
					String rm2 = rs.getString("RelMat_2")==null?"":rs.getString("RelMat_2");
					String sp2 = rs.getString("Spread_2")==null?"":rs.getString("Spread_2");
					String rm3 = rs.getString("RelMat_3")==null?"":rs.getString("RelMat_3");
					String sp3 = rs.getString("Spread_3")==null?"":rs.getString("Spread_3");
					String rm4 = rs.getString("RelMat_4")==null?"":rs.getString("RelMat_4");
					String mar = rs.getString("Margen")==null?"":rs.getString("Margen");				
					String tg = rs.getString("TipoGarantia")==null?"":rs.getString("TipoGarantia");
					String pdf = rs.getString("PorcDescFop")==null?"":rs.getString("PorcDescFop");			
					String tdf = rs.getString("TotDescFop")==null?"":rs.getString("TotDescFop");
					String pdfi = rs.getString("PorcDescFinape")==null?"":rs.getString("PorcDescFinape");
					String tdfi = rs.getString("TotDescFinape")==null?"":rs.getString("TotDescFinape");
					String fcc = rs.getString("FCrecimiento")==null?"":rs.getString("FCrecimiento");
					String pini = rs.getString("PeriodoInic")==null?"":rs.getString("PeriodoInic");
					String pf = rs.getString("PeriodoFin")==null?"":rs.getString("PeriodoFin");
					String dias = rs.getString("Dias")==null?"":rs.getString("Dias");
					String com = rs.getString("Comision")==null?"":rs.getString("Comision");
					String pg = rs.getString("PorcGarantia")==null?"":rs.getString("PorcGarantia");
					String pc = rs.getString("PorcComision")==null?"":rs.getString("PorcComision");
					String sins = rs.getString("SdoInsoluto")==null?"":rs.getString("SdoInsoluto");
					String amor = rs.getString("Amortizacion")==null?"":rs.getString("Amortizacion");
					String interes = rs.getString("Interes")==null?"":rs.getString("Interes");
					String intc = rs.getString("IntCobAnt")==null?"":rs.getString("IntCobAnt");
					String tv = rs.getString("TotalVencimiento")==null?"":rs.getString("TotalVencimiento");
					String pt = rs.getString("PagoTrad")==null?"":rs.getString("PagoTrad");
					String sub = rs.getString("Subsidio")==null?"":rs.getString("Subsidio");
					String tex = rs.getString("TotalExigible")==null?"":rs.getString("TotalExigible");
					String cv = rs.getString("CapitalVencido")==null?"":rs.getString("CapitalVencido");
					String iv = rs.getString("InteresVencido")==null?"":rs.getString("InteresVencido");
					String tcv = rs.getString("TotalCarVen")==null?"":rs.getString("TotalCarVen");
					String at = rs.getString("AdeudoTotal")==null?"":rs.getString("AdeudoTotal");
					String fa = rs.getString("FinAdicOtorgado")==null?"":rs.getString("FinAdicOtorgado");
					String far = rs.getString("FinAdicRecup")==null?"":rs.getString("FinAdicRecup");
					String sin = rs.getString("SdoInsNvo")==null?"":rs.getString("SdoInsNvo");
					String sib = rs.getString("SdoInsBase")==null?"":rs.getString("SdoInsBase");
					String ccb = rs.getString("CuentaCLABE")==null?"":rs.getString("CuentaCLABE");
				
					contenidoArchivo.append("\n"+
						fpp.replaceAll(","," ") +";" + mp.replaceAll(","," ") + ";" + cm.replaceAll(","," ") +";"+ dm.replaceAll(","," ") +";" +
						inter.replaceAll(","," ") + ";" + di.replaceAll(","," ") + ";" + bo.replaceAll(","," ") + ";" + dbo.replaceAll(","," ")+ ";" +
						cli.replaceAll(","," ") + ";" + nom.replaceAll(","," ") + ";" + suc.replaceAll(","," ") + ";" + pro.replaceAll(","," ") + ";" + 
						con.replaceAll(","," ") + ";" + pre.replaceAll(","," ") + ";" + ne.replaceAll(","," ") + ";" + bur.replaceAll(","," ") + ";" + 
						dis.replaceAll(","," ") + ";" + suba.replaceAll(","," ") + ";" + ce.replaceAll(","," ") + ";" + cpv.replaceAll(","," ") + ";" + 
						tc.replaceAll(","," ") + ";" + am.replaceAll(","," ") + ";" + te.replaceAll(","," ") + ";" + fo.replaceAll(","," ") + ";" + 
						san.replaceAll(","," ") + ";" + fc.replaceAll(","," ") + ";" + fi.replaceAll(","," ") + ";" + tb.replaceAll(","," ") + ";" + 
						et.replaceAll(","," ") + ";" + rm1.replaceAll(","," ") + ";" + sp1.replaceAll(","," ") + ";" + rm2.replaceAll(","," ") + ";" + 
						sp2.replaceAll(","," ") + ";" + rm3.replaceAll(","," ") + ";" + sp3.replaceAll(","," ") + ";" + rm4.replaceAll(","," ") + ";" +
						mar.replaceAll(","," ") + ";" + tg.replaceAll(","," ") + ";" + 
						//pdf.replaceAll(","," ") + ";" + tdf.replaceAll(","," ") + ";" + pdfi.replaceAll(","," ") + ";" + tdfi.replaceAll(","," ") + ";" + 
						fcc.replaceAll(","," ") + ";" + pini.replaceAll(","," ") + ";" + 
						pf.replaceAll(","," ") + ";" + dias.replaceAll(","," ") + ";" + com.replaceAll(","," ") + ";" + pg.replaceAll(","," ") + ";" + 
						pc.replaceAll(","," ") + ";" + sins.replaceAll(","," ") + ";" + amor.replaceAll(","," ") + ";" + interes.replaceAll(","," ") + ";" +
						intc.replaceAll(","," ") + ";" + tv.replaceAll(","," ") + ";" + pt.replaceAll(","," ") + ";" + sub.replaceAll(","," ") + ";" + 
						tex.replaceAll(","," ") + ";" + cv.replaceAll(","," ") + ";" + iv.replaceAll(","," ") + ";" + tcv.replaceAll(","," ")  + ";" + 
						at.replaceAll(","," ")  + ";" + fa.replaceAll(","," ") + ";" + far.replaceAll(","," ") + ";" + sin.replaceAll(","," ") + ";" + 
						sib.replaceAll(","," ") + ";" + ccb.replaceAll(","," ") );
				}
        
        		String fechaCorte = FechaCorte.replace('/','_');	
        		String nomArc = "archvenc_"+fechaCorte+"_"+ic_if+"_S";
        		archivo.make(contenidoArchivo.toString(), path, nomArc,".txt");
				nombreArchivo = archivo.getNombre();
			} catch (Throwable e) {
				throw new AppException("Error al generar el archivo txt", e);
			} finally {
				try {
          			System.out.println("Cerrando archivos");
					rs.close();
          			writer.close();
          			buffer.close();
				} catch(Exception e) {
          			System.err.println("Error al cerrar los archivos: "+e);
				}
			}
		}			//AQUI TERMINA....

		if ("XLS".equals(tipo)) {
				try {
					int registros = 0;	
					while(rs.next()){
						if(registros==0) { 
							contenidoArchivo.append("Nombre del proveedor, Numero del proveedor, Numero de Transferencia, Fecha de pago, Montonto total de la transferencia, Moneda, Fecha de la factura, Monto de la factura, Fecha de operacion");
						}
						registros++;
						String nomproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
							
						contenidoArchivo.append("\n"+
							nomproveedor.replaceAll(",","") + "," +
							numproveedor + "," +
							numtrans + "," +
							fechapago.replaceAll("-","/") + "," +
							"$ " +  Comunes.formatoDecimal(montocheque, 2, false) + "," +
							moneda + "," +
							fechafactura.replaceAll("-","/") + "," +
							"$ " + Comunes.formatoDecimal(montofactura, 2, false) + "," +
							fechaoperacion.replaceAll("-","/") + ",");
					}
					
					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}	 
				} catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}else if ("PDF".equals(tipo)) {
				try {
					HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
							
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.setTable(9, 80);
					pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Numero del Proveedor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Numero de Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Pagado","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Total de la Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operacion","celda01",ComunesPDF.CENTER);
													
					while (rs.next()) {
						String nombreproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numeroproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
						 
						pdfDoc.setCell(nombreproveedor,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(numeroproveedor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numtrans,"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(fechapago,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechafactura,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(fechaoperacion,"formas",ComunesPDF.CENTER);				
					}
					pdfDoc.addTable();
					pdfDoc.endDocument();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}
		return nombreArchivo;	
	}	

	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo 	= new CreaArchivo();
		
		
		
		if ("PDF".equals(tipo)) {   
			int TOTAL_CAMPOS = 18;
			int nRow = 0, numMon = 0, numDL = 0;
			double totSaldoInsMon = 0.0;
			double totAmortizMon = 0.0;
			double totInteresMon = 0.0;
			double totDescFopMon = 0.0;
			double totDescFinapeMon	= 0.0;
			double totVentoMon = 0.0;
			double totAdeudoTotMon = 0.0;
			double totSaldoInsNvoMon = 0.0;
			double totExigibleMon = 0.0;
			double totSaldoInsDL = 0.0;
			double totAmortizDL = 0.0;
			double totInteresDL = 0.0;
			double totDescFopDL = 0.0;
			double totDescFinapeDL	= 0.0;
			double totVentoDL = 0.0;
			double totAdeudoTotDL = 0.0;
			double totSaldoInsNvoDL = 0.0;
			double totExigibleDL = 0.0;
			int numCols = 0;
			float anchos[] = null;
			
			if (parametro.equals("S")){
			  if(paramNumDocto.equals("S")){
				 //float aux[]={5,8,5.3f,6,3,4,5,6,6,6,5,5,6,6,6,6.7f,6};
         float aux[]={5,8,5.3f,6,3,4,5,6,6,6,6,6,6,6.7f,6};
				 anchos = aux;
				 //numCols = 17;
         numCols = 15;
			  }else{
				 //float aux[]={5,9,5.3f,6,3,4,5,7,7,6,5,5,7,7,7,6.7f};
         float aux[]={5,9,5.3f,6,3,4,5,7,7,6,7,7,7,6.7f};
				 anchos = aux;
				 //numCols = 16;
         numCols = 14;
			  }
			}else{
			  if(paramNumDocto.equals("S")){
				 //float aux[]={5,8,6,4,5,6,6,6,5,5,6,6,6.7f,5};
				 float aux[]={5,8,6,4,5,6,6,6,6,6,6.7f,5};
				 anchos = aux;
				 //numCols = 14;
         numCols = 12;
			  }else{
				 //float aux[]={5,9,6,4,5,7,7,6,5,5,7,7,6.7f};
         float aux[]={5,9,6,4,5,7,7,6,7,7,6.7f};
				 anchos = aux;
				 //numCols = 13;
         numCols = 11;
			  }
			}
			
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
			/*	pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			*/			
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				if (parametro.equals("S")){
					pdfDoc.setTable(numCols+1, 100);
					pdfDoc.setCell("Cliente","formasmenB",ComunesPDF.CENTER);		
					pdfDoc.setCell("Nombre del Cliente","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Pr�ximo Pago","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Pr�stamo","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("No. Elec.","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Esquema de tasas","formasmenB",ComunesPDF.CENTER);
          pdfDoc.setCell("Moneda","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Margen","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Saldo insoluto","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Amortizaci�n","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Intereses","formasmenB",ComunesPDF.CENTER);
					//pdfDoc.setCell("Total desc. FOPYME","formasmenB",ComunesPDF.CENTER);
					//pdfDoc.setCell("Total desc. FINAPE","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Total vencimiento","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Total exigible","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Adeudo Total","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Saldo insoluto nuevo","formasmenB",ComunesPDF.CENTER);
					if(paramNumDocto.equals("S")){
					  pdfDoc.setCell("N�mero de Documento","formasmenB",ComunesPDF.CENTER);//FODEA 015 - 2009 ACF
					}
				}else {
					pdfDoc.setTable(numCols,100,anchos);//FODEA 015 - 2009 ACF
					pdfDoc.setCell("Cliente","formasmenB",ComunesPDF.CENTER);		
					pdfDoc.setCell("Nombre del Cliente","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Pr�stamo","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Esque-\nma de tasas","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Margen","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Saldo insoluto","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Amortizaci�n","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Intereses","formasmenB",ComunesPDF.CENTER);
					//pdfDoc.setCell("Total desc. FOPYME","formasmenB",ComunesPDF.CENTER);
					//pdfDoc.setCell("Total desc. FINAPE","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Total vencimiento","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Total exigible","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Saldo insoluto nuevo","formasmenB",ComunesPDF.CENTER);
					if(paramNumDocto.equals("S")){
						pdfDoc.setCell("N�mero de Documento","formasmenB",ComunesPDF.CENTER);//FODEA 015 - 2009 ACF
					}
				}
				//variabble para almacenar los totales en dolares
					BigDecimal saldoInsolitoD = new BigDecimal(0);
					BigDecimal amortizacionD = new BigDecimal("0.0");
					BigDecimal interecesD = new BigDecimal("0.0");
					BigDecimal fopymeD = new BigDecimal("0.0");
					BigDecimal finapeD = new BigDecimal("0.0");
					BigDecimal vencimientoD = new BigDecimal("0.0");
					BigDecimal exigibleD = new BigDecimal("0.0");
					BigDecimal adeudoTotalD = new BigDecimal("0.0");
					BigDecimal nuevoSaldoInsolitoD = new BigDecimal("0.0");
					//variabble para almacenar los totales en moneda nacional
					BigDecimal saldoInsolitoMN = new BigDecimal(0);
					BigDecimal amortizacionMN = new BigDecimal("0.0");
					BigDecimal interecesMN = new BigDecimal("0.0");
					BigDecimal fopymeMN = new BigDecimal("0.0");
					BigDecimal finapeMN = new BigDecimal("0.0");
					BigDecimal vencimientoMN = new BigDecimal("0.0");
					BigDecimal exigibleMN = new BigDecimal("0.0");
					BigDecimal adeudoTotalMN = new BigDecimal("0.0");
					BigDecimal nuevoSaldoInsolitoMN = new BigDecimal("0.0");


				while (rs.next()) {
					Vector vecDatos = new Vector();	   
					for(int i=1; i<=TOTAL_CAMPOS; i++) {
						vecDatos.add(rs.getString(i));
					}
					
					String numeroDocumento = rs.getString("numero_documento")==null?"":rs.getString("numero_documento");//FODEA 015 - 2009 ACF
					if(rs.getString(3).equals("54")){
						numDL++;
						totSaldoInsDL		+= Double.valueOf(rs.getString(8)).doubleValue();
						totAmortizDL		+= Double.valueOf(rs.getString(9)).doubleValue();
						totInteresDL		+= Double.valueOf(rs.getString(10)).doubleValue();
						totDescFopDL		+= Double.valueOf(rs.getString(11)).doubleValue();
						totDescFinapeDL	+= Double.valueOf(rs.getString(12)).doubleValue();
						totVentoDL			+= Double.valueOf(rs.getString(13)).doubleValue();
						totAdeudoTotDL		+= Double.valueOf(rs.getString(14)).doubleValue();
						totSaldoInsNvoDL	+= Double.valueOf(rs.getString(15)).doubleValue();
						totExigibleDL		+= Double.valueOf(rs.getString("fg_totalexigible")).doubleValue();
						//variabble para almacenar los totales en moneda nacional
						 saldoInsolitoD =saldoInsolitoD.add( new BigDecimal(rs.getString(8)));
						 amortizacionD =amortizacionD .add( new BigDecimal(rs.getString(9)));
						 interecesD = interecesD.add( new BigDecimal(rs.getString(10)));
						 fopymeD = fopymeD.add( new BigDecimal(rs.getString(11)));
						 finapeD = finapeD.add( new BigDecimal(rs.getString(12)));
						 vencimientoD = vencimientoD.add( new BigDecimal(rs.getString(13)));
						 exigibleD = exigibleD.add( new BigDecimal(rs.getString("fg_totalexigible")));
						 adeudoTotalD = adeudoTotalD.add( new BigDecimal(rs.getString(14)));
						 nuevoSaldoInsolitoD = nuevoSaldoInsolitoD.add( new BigDecimal(rs.getString(15)));
					} else {
						numMon++;
						totSaldoInsMon		+= Double.valueOf(rs.getString(8)).doubleValue();
						totAmortizMon		+= Double.valueOf(rs.getString(9)).doubleValue();
						totInteresMon		+= Double.valueOf(rs.getString(10)).doubleValue();
						totDescFopMon		+= Double.valueOf(rs.getString(11)).doubleValue();
						totDescFinapeMon	+= Double.valueOf(rs.getString(12)).doubleValue();
						totVentoMon			+= Double.valueOf(rs.getString(13)).doubleValue();
						totAdeudoTotMon	+= Double.valueOf(rs.getString(14)).doubleValue();
						totSaldoInsNvoMon	+= Double.valueOf(rs.getString(15)).doubleValue();
						totExigibleMon		+= Double.valueOf(rs.getString("fg_totalexigible")).doubleValue();
						
						//variabble para almacenar los totales en moneda nacional
						 saldoInsolitoMN =saldoInsolitoMN.add( new BigDecimal(rs.getString(8)));
						 amortizacionMN =amortizacionMN .add( new BigDecimal(rs.getString(9)));
						 interecesMN = interecesMN.add( new BigDecimal(rs.getString(10)));
						 fopymeMN = fopymeMN.add( new BigDecimal(rs.getString(11)));
						 finapeMN = finapeMN.add( new BigDecimal(rs.getString(12)));
						 vencimientoMN = vencimientoMN.add( new BigDecimal(rs.getString(13)));
						 exigibleMN = exigibleMN.add( new BigDecimal(rs.getString("fg_totalexigible")));
						 adeudoTotalMN = adeudoTotalMN.add( new BigDecimal(rs.getString(14)));
						 nuevoSaldoInsolitoMN = nuevoSaldoInsolitoMN.add( new BigDecimal(rs.getString(15)));
					}
					
					if(parametro.equals("S")) { 
						pdfDoc.setCell(vecDatos.get(0).toString().trim(),"formasmen",ComunesPDF.LEFT);		
						pdfDoc.setCell(vecDatos.get(1).toString().trim(),"formasmen",ComunesPDF.CENTER);
						pdfDoc.setCell(vecDatos.get(2).toString().trim(),"formasmen",ComunesPDF.CENTER);
						pdfDoc.setCell(vecDatos.get(3).toString().trim(),"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(vecDatos.get(4).toString().trim(),"formasmen",ComunesPDF.CENTER);
						pdfDoc.setCell(vecDatos.get(5).toString().trim(),"formasmen",ComunesPDF.CENTER);
            pdfDoc.setCell(""+rs.getString("CD_NOMBRE"),"formasmen",ComunesPDF.CENTER);
						//pdfDoc.setCell(vecDatos.get(16).toString().trim(),"formasmen",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(vecDatos.get(6).toString().trim(), 2, false),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(7).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(8).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(9).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						//pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(10).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						//pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(11).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(12).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(rs.getString("fg_totalexigible"), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(13).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(14).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						//FODEA 015 - 2009 ACF (I)
						if(paramNumDocto.equals("S")){
							pdfDoc.setCell(numeroDocumento,"formasmen",ComunesPDF.LEFT);
						}
						//FODEA 015 - 2009 ACF (F)
					} else {
						pdfDoc.setCell(vecDatos.get(0).toString().trim(),"formasmen",ComunesPDF.LEFT);		
						pdfDoc.setCell(vecDatos.get(1).toString().trim(),"formasmen",ComunesPDF.CENTER);
						pdfDoc.setCell(vecDatos.get(3).toString().trim(),"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(vecDatos.get(5).toString().trim(),"formasmen",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(vecDatos.get(6).toString().trim(), 2, false),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(7).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(8).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(9).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						//pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(10).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						//pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(11).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(12).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(rs.getString("fg_totalexigible"), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(14).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
						//FODEA 015 - 2009 ACF (I)
						if(paramNumDocto.equals("S")){  
							pdfDoc.setCell(numeroDocumento,"formasmen",ComunesPDF.LEFT);    
						}
						//FODEA 015 - 2009 ACF (F)
					}
					nRow++;
				}
				/*
				if(nRow > 0) { 
					if(numMon > 0){
					  if(parametro.equals("S")) {
						pdfDoc.setCell("Total Parcial Moneda Nacional","formasrep",ComunesPDF.LEFT,7);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totAmortizMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totDescFopMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totDescFinapeMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totVentoMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totExigibleMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totAdeudoTotMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsNvoMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
					//FODEA 015 - 2009 ACF (I)
					if(paramNumDocto.equals("S")){
					  pdfDoc.setCell(" ","formasmen",ComunesPDF.LEFT);
					}
					//FODEA 015 - 2009 ACF (F)
					  } else {
						 pdfDoc.setCell("Total Parcial Moneda Nacional","formasrep",ComunesPDF.LEFT,5);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totAmortizMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totDescFopMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totDescFinapeMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totVentoMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totExigibleMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsNvoMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
					//FODEA 015 - 2009 ACF (I)
					if(paramNumDocto.equals("S")){
					  pdfDoc.setCell(" ","formasmen",ComunesPDF.LEFT);
					}
					//FODEA 015 - 2009 ACF (F)
					  }
					}          
					if(numDL > 0){
						 if(parametro.equals("S")) {	
							pdfDoc.setCell("Total Parcial D�lares","formasrep",ComunesPDF.LEFT,7);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totAmortizDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totDescFopDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totDescFinapeDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totVentoDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totExigibleDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totAdeudoTotDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsNvoDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							//FODEA 015 - 2009 ACF (I)
							if(paramNumDocto.equals("S")){
								pdfDoc.setCell(" ","formasmen",ComunesPDF.LEFT);
							}
							//FODEA 015 - 2009 ACF (F)
						} else {
							pdfDoc.setCell("Total Parcial D�lares","formasrep",ComunesPDF.LEFT,5);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totAmortizDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totDescFopDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totDescFinapeDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totVentoDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totExigibleDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsNvoDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							//FODEA 015 - 2009 ACF (I)
							if(paramNumDocto.equals("S")){
								pdfDoc.setCell(" ","formasmen",ComunesPDF.LEFT);
							}
							//FODEA 015 - 2009 ACF (F)
						}
					}
					      
					//Registros vecTotales = queryHelperRegExtJS.getResultCount(request);
					
					if(vecTotales.getNumeroRegistros()>0){
						int j = 0;
						List vecColumnas = null;
						for(j=0;j<vecTotales.getNumeroRegistros();j++){
							vecColumnas = vecTotales.getData(j);
							if(parametro.equals("S")) {	 	
								pdfDoc.setCell("TOTAL "+vecColumnas.get(9).toString(),"formasmen",ComunesPDF.LEFT,7);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(1).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(2).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(3).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(4).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(5).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(6).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(10).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(7).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(8).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								//FODEA 015 - 2009 ACF (I)
								if(paramNumDocto.equals("S")){
									pdfDoc.setCell(" ","formasmen",ComunesPDF.LEFT);
								}
								//FODEA 015 - 2009 ACF (F)
							} else {
								pdfDoc.setCell("TOTAL "+vecColumnas.get(9).toString(),"formasmen",ComunesPDF.LEFT,5);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(1).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(2).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(3).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(4).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(5).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(6).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(10).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(8).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								//FODEA 015 - 2009 ACF (I)
								if(paramNumDocto.equals("S")){
									pdfDoc.setCell(" ","formasmen",ComunesPDF.LEFT);
								}
								//FODEA 015 - 2009 ACF (F)
							}
						}	
					}
				}
            */
				pdfDoc.addTable();
				
				pdfDoc.setTable(numCols+1, 100);
				Registros parciales = getTotales();
				parciales.rewind();
				String tp="Total Parcial ";
				while(parciales.next()){
				String tpm = (parciales.getString("cd_nombre") == null) ? "" : parciales.getString("cd_nombre");
				tp=tp+tpm;
				String si = (parciales.getString("SALDO_INSOLUTO") == null) ? "" : parciales.getString("SALDO_INSOLUTO");
				String a = (parciales.getString("AMORTIZACION") == null) ? "" : parciales.getString("AMORTIZACION");
				String i = (parciales.getString("INTERESES") == null) ? "" : parciales.getString("INTERESES");
				String fo = (parciales.getString("FOPYME") == null) ? "" : parciales.getString("FOPYME");
				String fi = (parciales.getString("FINAPE") == null) ? "" : parciales.getString("FINAPE");
				String tv = (parciales.getString("T_VENC") == null) ? "" : parciales.getString("T_VENC");
				String te = (parciales.getString("T_EXIG") == null) ? "" : parciales.getString("T_EXIG");
				String at = (parciales.getString("ADEUDO") == null) ? "" : parciales.getString("ADEUDO");
				String sin = (parciales.getString("SALDO_IN") == null) ? "" : parciales.getString("SALDO_IN");
				/*
				pdfDoc.setCell(tp,"celda01",ComunesPDF.LEFT,8);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( si,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( a,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( i,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( fo,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( fi,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( tv,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( te,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( at,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( sin,2,true),"formas",ComunesPDF.RIGHT);
				*/
				
					if("MONEDA NACIONAL".equals(tpm)){
						pdfDoc.setCell(tp,"celda01",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( saldoInsolitoMN.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( amortizacionMN.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( interecesMN.toString(),2,true),"formas",ComunesPDF.RIGHT);
						//pdfDoc.setCell("$"+Comunes.formatoDecimal( fopymeMN.toString(),2,true),"formas",ComunesPDF.RIGHT);
						//pdfDoc.setCell("$"+Comunes.formatoDecimal( finapeMN.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( vencimientoMN.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( exigibleMN.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( adeudoTotalMN.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( nuevoSaldoInsolitoMN.toString(),2,true),"formas",ComunesPDF.RIGHT);
					}else{
						pdfDoc.setCell(tp,"celda01",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( saldoInsolitoD.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( amortizacionD.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( interecesD.toString(),2,true),"formas",ComunesPDF.RIGHT);
						//pdfDoc.setCell("$"+Comunes.formatoDecimal( fopymeD.toString(),2,true),"formas",ComunesPDF.RIGHT);
						//pdfDoc.setCell("$"+Comunes.formatoDecimal( finapeD.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( vencimientoD.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( exigibleD.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( adeudoTotalD.toString(),2,true),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal( nuevoSaldoInsolitoD.toString(),2,true),"formas",ComunesPDF.RIGHT);
					}
				}				
				pdfDoc.addTable();
				
				pdfDoc.setTable(numCols+1, 100);
				parciales = getTotales();
				parciales.rewind();
				tp="Total  ";
				String r="N�mero de registros: ";
				while(parciales.next()){
				String tpm = (parciales.getString("cd_nombre") == null) ? "" : parciales.getString("cd_nombre");
				tp=tp+tpm;
				String nr = (parciales.getString("REGISTROS") == null) ? "" : parciales.getString("REGISTROS");
				r=r+nr;
				String si = (parciales.getString("SALDO_INSOLUTO") == null) ? "" : parciales.getString("SALDO_INSOLUTO");
				String a = (parciales.getString("AMORTIZACION") == null) ? "" : parciales.getString("AMORTIZACION");
				String i = (parciales.getString("INTERESES") == null) ? "" : parciales.getString("INTERESES");
				String fo = (parciales.getString("FOPYME") == null) ? "" : parciales.getString("FOPYME");
				String fi = (parciales.getString("FINAPE") == null) ? "" : parciales.getString("FINAPE");
				String tv = (parciales.getString("T_VENC") == null) ? "" : parciales.getString("T_VENC");
				String te = (parciales.getString("T_EXIG") == null) ? "" : parciales.getString("T_EXIG");
				String at = (parciales.getString("ADEUDO") == null) ? "" : parciales.getString("ADEUDO");
				String sin = (parciales.getString("SALDO_IN") == null) ? "" : parciales.getString("SALDO_IN");
				
				pdfDoc.setCell(tp,"celda01",ComunesPDF.LEFT,5);
				pdfDoc.setCell(r,"celda01",ComunesPDF.LEFT,3);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( si,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( a,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( i,2,true),"formas",ComunesPDF.RIGHT);
				//pdfDoc.setCell("$"+Comunes.formatoDecimal( fo,2,true),"formas",ComunesPDF.RIGHT);
				//pdfDoc.setCell("$"+Comunes.formatoDecimal( fi,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( tv,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( te,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( at,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( sin,2,true),"formas",ComunesPDF.RIGHT);
				}				
				pdfDoc.addTable();
				
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
		}		
		return nombreArchivo;
	}
	
	/**
	 * 
	 * @return objeto de tipo registros.
	 * Calcula los totales
	 */
	public Registros getTotales(){
	StringBuffer qrySentencia = new StringBuffer("");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
				qrySentencia.append(
" SELECT   /*+         INDEX (V IN_COM_VENCIMIENTO_01_NUK)     */     COUNT (1) AS REGISTROS,	"+
"  SUM (fg_sdoinsoluto) AS SALDO_INSOLUTO, SUM (fg_amortizacion) AS AMORTIZACION, "+
"  SUM (fg_interes) AS INTERESES, SUM (fg_totdescfop) AS FOPYME, SUM (fg_totdescfinape) AS FINAPE, "+
"  SUM (fg_totalvencimiento) AS T_VENC, SUM (fg_adeudototal) AS ADEUDO, SUM (fg_sdoinsnvo) AS SALDO_IN, "+
"  m.cd_nombre, SUM (fg_totalexigible) AS T_EXIG "+
"    FROM com_vencimiento v, comcat_moneda m "+
"   WHERE v.ic_moneda = m.ic_moneda "+
"     AND v.ic_if = ? "+
"     AND com_fechaprobablepago >= TO_DATE ('05/05/2014', 'dd/mm/yyyy') "+
"     AND com_fechaprobablepago < TO_DATE ('05/05/2014', 'dd/mm/yyyy') + 1 "+
" GROUP BY v.ic_moneda, m.cd_nombre "+
" ORDER BY v.ic_moneda ");								
			conditions.add(ic_if);
			System.err.println("conditions - -- - - - - - - -  -\n: "+conditions);			
			System.err.println("qrySentencia - -- - - - - - - - \n: "+qrySentencia);
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
		
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	return registros;
	}
	
	
	public List getConditions() {
		return conditions;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public void setCliente(String cliente) {
		this.cliente = cliente;
	}


	public void setPrestamo(String prestamo) {
		this.prestamo = prestamo;
	}


	public void setFechaCorte(String FechaCorte) {
		this.FechaCorte = FechaCorte;
	}
    

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public void setParamNumDocto(String paramNumDocto) {
		this.paramNumDocto = paramNumDocto;
	}


	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	public void setVecTotales(Registros vecTotales) {
		this.vecTotales = vecTotales;
	}


  public void setTipoLinea(String tipoLinea)
  {
    this.tipoLinea = tipoLinea;
  }


  public String getTipoLinea()
  {
    return tipoLinea;
  }
}