package com.netro.descuento;

import java.io.Serializable;

import java.util.ArrayList;

public class ResultadosValidacionCatalogo implements Serializable{

	private static final long serialVersionUID = 82010L;
	
	private StringBuffer errorGeneral 	= null;
	private int 			icProceso		= -1;
	private StringBuffer errores			= null;
	private boolean		errorLinea		= false;
	private int 			numRegErr   	= 0; 
	private int 			numRegOk			= 0;
	private ArrayList		registros		= new ArrayList();
			
	public void appendErrores(String error){
			if(errorGeneral == null) errorGeneral = new StringBuffer();
			errorGeneral.append(error);
	}
	
	public StringBuffer getErrores(){
		return errorGeneral;
	}
 
	public void setIcProceso(int icProceso){
		this.icProceso = icProceso;
	}
	public int  getIcProceso(){
		return icProceso;
	}
 
	public void setErrorLinea(boolean errorLinea){
		this.errorLinea = errorLinea;
	}
	public boolean getErrorLinea(){
		return this.errorLinea;
	}
 
	public void appendErrores(int numeroLinea,String numeroCampo,String descripcionError){
		ValidacionCatalogo v = (ValidacionCatalogo) registros.get(numeroLinea-1);
		v.appendError(numeroCampo,descripcionError);
	}
	
	public void incNumRegErr(){
		this.numRegErr++;
	}
	public void decNumRegErr(){
		this.numRegErr--;
		if(this.numRegErr < 0) this.numRegErr = 0;
	}
	
	public void incNumRegOk(){
		this.numRegOk++;	
	}
	public void decNumRegOk(){
		this.numRegOk--;
		if(this.numRegOk < 0) this.numRegOk = 0;
	}
	
	public void appendErrorClaveRepetida(String clave,String mensajeError){
		if(clave == null) return;
		for(int i=0;i<registros.size();i++){
			ValidacionCatalogo v = (ValidacionCatalogo) registros.get(i);
			if(clave.equals(v.getClave())){
				if(!v.hayError()){
					decNumRegOk();
					incNumRegErr();
				}
				v.appendError("1",mensajeError);
			}
		}
	}
	
	public void appendErrorDescripcionRepetida(String clave,String mensajeError){
		if(clave == null) return;
		for(int i=0;i<registros.size();i++){
			ValidacionCatalogo v = (ValidacionCatalogo) registros.get(i);
			if(clave.equals(v.getClave())){
				if(!v.hayError()){
					decNumRegOk();
					incNumRegErr();
				}
				v.appendError("2",mensajeError);
			}
		}
	}
	
	public void agregaRegistro(int numeroLinea,String rfc){
		registros.add(new ValidacionCatalogo(rfc,numeroLinea));
	}

	public ArrayList getListaDeRegistrosExitosos(){
		
		ArrayList lista = new ArrayList();
		
		for(int i=0;i<registros.size();i++){
			ValidacionCatalogo v = (ValidacionCatalogo) registros.get(i);
			if(!v.hayError()){
				lista.add(String.valueOf(v.getNumeroLinea()));
			}
		}		
		return lista;

	}
	
	public ArrayList getRegistros(){
		return this.registros;	
	}
	
	public int getNumRegErr(){	
		return this.numRegErr;
	}
	
	public int getNumRegOk(){
		return this.numRegOk;	
	}
	
}
