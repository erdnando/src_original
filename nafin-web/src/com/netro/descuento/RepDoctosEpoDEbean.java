package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class RepDoctosEpoDEbean implements IQueryGeneratorRegExtJS{
	StringBuffer qrySentencia;
	private List conditions;
	
	private String iNoEstatusDocto;
	private String iNoEpo; // IMPORTANTE: EL CAMPO iNoEpo ES OBLIGATORIO
	private String sOperaNotas;
	private String sesIdiomaUsuario;
	private String sFechaVencPyme;
	
	//Migraci�n EPO- 2012
	private String porcentaje;
	private String porcentajeDL;
	
	private boolean bOperaFactorajeVencido;
	private boolean bOperaFactorajeDistribuido;
	private boolean bOperaFactorajeNotaDeCredito;
	private boolean bOperaFactConMandato;
	private boolean bOperaFactVencimientoInfonavit;
	private boolean bTipoFactoraje;
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(RepDoctosEpoDEbean.class);
	
	//Constructor
	public RepDoctosEpoDEbean() { }
	
	public String getAggregateCalculationQuery(){
		qrySentencia	= new StringBuffer();
		conditions		= new ArrayList();
		
		StringBuffer sbQuery = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		StringBuffer campos = new StringBuffer();
		StringBuffer tablas = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		StringBuffer order = new StringBuffer();
		String sReferencia = ",'RepDoctosEpoDEbean:getQueryPorEstatus("+iNoEstatusDocto+")' as PANTALLA";
    	AccesoDB con = new AccesoDB();
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
		try {
			con.conexionDB();
			String fechaSigDol = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			ps.executeUpdate();
			ps.close();

			qrySentencia =
				" SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha"   +
				"   FROM dual"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(iNoEpo));
			ResultSet rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
			}
			rs.close();ps.close();


			hint.append("/*+ index(ac2)*/");
			/*
			 *CAMPOS
			 */
			campos.append("mon.ic_moneda");
			campos.append(",mon.cd_nombre AS nombre_moneda");
			campos.append(",COUNT(mon.cd_nombre) AS num_registros");
			campos.append(",SUM(doc.fn_monto) AS total_monto");
			campos.append(",'' AS total_monto_descontar");
			if(!("2".equals(iNoEstatusDocto))&&!("11".equals(iNoEstatusDocto))){
				campos.append(",SUM(ds.in_importe_interes) AS total_importe_interes");
				campos.append(",SUM(ds.in_importe_recibir) AS total_importe_recibir");
			}
			
			/*
			 *TABLAS
			 */
			tablas.append(
				" FROM com_documento doc"   +
				" ,comcat_if cif "   +
				" ,comcat_pyme pym "   +
				" ,COMCAT_MANDANTE ma "+  // Fodea 023 2009
				" ,COMCAT_TIPO_FACTORAJE tf "+  // Fodea 023 2009
				" ,comrel_pyme_epo cpe ");

			if("2".equals(iNoEstatusDocto))
				tablas.append(" ,com_acuse1 ac1 ");

			if("3".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto))
				tablas.append(" ,com_acuse2 ac2");

			if("3".equals(iNoEstatusDocto) || "4,16".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto)  )
				tablas.append(" ,com_docto_seleccionado ds");

			if("4,16".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto))
				tablas.append(" ,com_solicitud sol");

			tablas.append(
				" ,comcat_moneda mon"+
				" ,comcat_if cif2");
			/*
			 *CONDICION
			 */
			condicion.append(
				" WHERE doc.ic_pyme = pym.ic_pyme");
				condicion.append("  AND ma.IC_MANDANTE(+) = doc.IC_MANDANTE"); // Fodea 023 2009
				condicion.append("  AND doc.cs_dscto_especial = tf.cc_tipo_factoraje "); // Fodea 023 2009
				
			if("3".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto) ) {
				condicion.append(
					" AND ac2.cc_acuse = ds.cc_acuse"+										
					" AND ac2.df_fecha_hora >= TRUNC(SYSDATE)  AND ac2.df_fecha_hora < TRUNC(SYSDATE) + 1 "+
					" AND ac2.ic_producto_nafin = 1 ");
			}
			// Fodea 000 - 2012. Fodea 000 - 2012. ADMIN - Proceso de Aut IF
			if( "24".equals(iNoEstatusDocto) ) {
				condicion.append(
					" AND ac2.cc_acuse = ds.cc_acuse"+										
					" AND ac2.ic_producto_nafin = 1 "
				);
			}
			if("2".equals(iNoEstatusDocto)) {
				condicion.append(
					" AND doc.cc_acuse = ac1.cc_acuse"+
					" AND doc.ic_if = cif.ic_if (+)"   +					
					" AND ac1.df_fechahora_carga >= TRUNC(SYSDATE)  AND ac1.df_fechahora_carga < TRUNC(SYSDATE) + 1 "+
					" AND ac1.ic_producto_nafin = 1" );
			} else if("4,16".equals(iNoEstatusDocto)) {
				condicion.append(				
					" AND sol.df_fecha_solicitud >= TRUNC(SYSDATE)  AND sol.df_fecha_solicitud < TRUNC(SYSDATE) + 1 ");					
			} else if("11".equals(iNoEstatusDocto)) {
				condicion.append(
					" AND TO_CHAR (doc.df_fecha_venc"+sFechaVencPyme+", 'DD/MM/YYYY') = TO_CHAR (SYSDATE, 'DD/MM/YYYY')");

			}
			if("3".equals(iNoEstatusDocto) || "4,16".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto)) {
				condicion.append(
					" AND doc.ic_if = cif.ic_if"   +
					" AND doc.ic_documento = ds.ic_documento");
			}
			if("11".equals(iNoEstatusDocto))
				condicion.append(  " AND ds.ic_documento = sol.ic_documento");

			condicion.append(
				" AND pym.ic_pyme = cpe.ic_pyme"   +
				" AND doc.ic_beneficiario = cif2.ic_if (+)"   +
				" AND doc.ic_moneda = mon.ic_moneda" +
				" AND cpe.ic_epo = ?"   +
				" AND doc.ic_epo = ?"   +
				" AND doc.ic_estatus_docto in ("+iNoEstatusDocto+") ");
				conditions.add(this.iNoEpo);
				conditions.add(this.iNoEpo);

			if("4,16".equals(iNoEstatusDocto)) {
				hint.append("/*+ use_nl(doc,cif,pym,cpe,ds,sol,mon ,cif2) index(doc)*/");
				sbQuery.append(
					" SELECT * FROM ("+
					" 	SELECT "+hint.toString()+" "+campos.toString()+" "+tablas.toString()+" "+condicion.toString()+" AND ds.ic_documento = sol.ic_documento "
				);
				sbQuery.append(" GROUP BY mon.ic_moneda, mon.cd_nombre");

				if("S".equals(sOperaNotas)) {
					sbQuery.append(
						" 	UNION ALL "+
						" 	SELECT "+hint+" "+campos.toString()+" "+tablas.toString()+" "+condicion.toString()+" AND doc.ic_docto_asociado = sol.ic_documento "
					);
					sbQuery.append(" GROUP BY mon.ic_moneda, mon.cd_nombre");
					conditions.add(this.iNoEpo);
					conditions.add(this.iNoEpo);
				}

				sbQuery.append(
					" ) "
				);

			} else{
				sbQuery.append(" SELECT "+hint+" "+campos.toString()+" "+tablas.toString()+" "+condicion.toString());
				sbQuery.append(" GROUP BY mon.ic_moneda, mon.cd_nombre");
			}
			
			log.debug("RepDoctosEpoDEbean :::qrySentencia  :: "+qrySentencia);	
			log.debug("RepDoctosEpoDEbean :::condiciones  :: "+conditions.toString());
			log.debug("RepDoctosEpoDEbean :::sbQuery :::: "+iNoEstatusDocto+":: "+sbQuery);
			
		} catch(Exception e) {
			log.error("RepDoctosEpoDEbean::getQueryPorEstatus Exception "+e);
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
		return sbQuery.toString();		
	}
	public String getDocumentQuery(){
		qrySentencia	= new StringBuffer();
		conditions		= new ArrayList();
		log.debug("querySentencia: " + qrySentencia);
		log.debug("conditions:" + conditions);
		return qrySentencia.toString();
	}
	public String getDocumentQueryFile(){
		qrySentencia	= new StringBuffer();
		//conditions		= new ArrayList();
			qrySentencia.append(getQueryPorEstatus());
		return qrySentencia.toString();
	}
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia	= new StringBuffer();
		conditions		= new ArrayList();
		return qrySentencia.toString();
	}
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		String nombreArchivo = "";
		int totalDoctosMN = 0;
		int totalDoctosUSD = 0;
		double	totalMontoMN = 0;
		double	totalMontoUSD = 0;
		double	totalMontoDescontarMN = 0;
		double	totalMontoDescontarUSD = 0;
		double	totalMontoIntMN	= 0;
		double	totalMontoIntUSD = 0;
		double	totalImporteRecibirMN	= 0;
		double	totalImporteRecibirUSD = 0;
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[]		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				String estatus = "";
				if("2".equals(this.iNoEstatusDocto)){
					estatus = "Negociable" ;
				}else if ("3".equals(this.iNoEstatusDocto)){
					estatus = "Seleccionada Pyme";
				}else if("4".equals(this.iNoEstatusDocto)){
					estatus = "Operada";
				}else if("11".equals(this.iNoEstatusDocto)){
					estatus = "Operada Pagada";
				}else if("24".equals(this.iNoEstatusDocto)){
					estatus = "En Proceso de Autorizaci�n IF";
				}else if("26".equals(this.iNoEstatusDocto)){
					estatus = "Programado Pyme";
				}
				pdfDoc.addText("Estatus: " + estatus,"celda01");
				
				pdfDoc.setTable(12,100);
				//Estatus Negociable
				if("2".equals(this.iNoEstatusDocto)){
					pdfDoc.setCell("A","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre de Proveedor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
					if(!"".equals(this.sFechaVencPyme)){
						pdfDoc.setCell("Fecha de Vencimiento PyME","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					if(this.bOperaFactorajeVencido || this.bOperaFactorajeDistribuido 
					|| this.bOperaFactorajeNotaDeCredito || this.bOperaFactVencimientoInfonavit) { /* Desplegar solo si opera "Factoraje Vencido" */
						pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero Acuse","celda01",ComunesPDF.CENTER);
					
					if("".equals(this.sFechaVencPyme)){
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					if(!this.bOperaFactorajeVencido && !this.bOperaFactorajeDistribuido 
					&& !this.bOperaFactorajeNotaDeCredito && !this.bOperaFactVencimientoInfonavit) {
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("B","celda02",ComunesPDF.CENTER);
					if(this.bOperaFactorajeVencido || this.bOperaFactConMandato || this.bOperaFactVencimientoInfonavit ) { /* Desplegar solo si opera "Factoraje Vencido" */
						pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					}
					if(this.bOperaFactConMandato) {
						pdfDoc.setCell("Mandatario","celda01",ComunesPDF.CENTER);
					}
					if(this.bOperaFactorajeDistribuido) {
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Porcentaje Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					}
					if(!this.bOperaFactorajeVencido && !this.bOperaFactConMandato && !this.bOperaFactVencimientoInfonavit ) {
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					if(!this.bOperaFactConMandato) {
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					if(!this.bOperaFactorajeDistribuido) {
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				}
				else if("3".equals(this.iNoEstatusDocto)
				||"24".equals(this.iNoEstatusDocto)
				||"26".equals(this.iNoEstatusDocto)){
					pdfDoc.setCell("A","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre PyME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
					if(!"".equals(this.sFechaVencPyme)){
						pdfDoc.setCell("Fecha de Vencimiento PyME","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					if(this.bTipoFactoraje) { /* Desplegar solo si opera "Factoraje Vencido" */
						pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("Monto de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
					
					if("".equals(this.sFechaVencPyme)){
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					if(!this.bTipoFactoraje) {
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("B","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo en D�as","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Int","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero Acuse","celda01",ComunesPDF.CENTER);
					if(this.bOperaFactConMandato) {
						pdfDoc.setCell("Mandatario","celda01",ComunesPDF.CENTER);
					}
					if(("24".equals(this.iNoEstatusDocto)&&(this.bOperaFactorajeDistribuido || this.bOperaFactVencimientoInfonavit))
					||(!("24".equals(this.iNoEstatusDocto))&&this.bOperaFactorajeDistribuido)){
						pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Porcentaje Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					}else{
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					if(!this.bOperaFactConMandato) {
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
				}
				else if("4,16".equals(this.iNoEstatusDocto)){
					pdfDoc.setCell("A","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre PyME","celda01",ComunesPDF.CENTER);
					if(this.bOperaFactVencimientoInfonavit){
						pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
					if(!"".equals(this.sFechaVencPyme)){
						pdfDoc.setCell("Fecha de Vencimiento PyME","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					if(this.bTipoFactoraje) { /* Desplegar solo si opera "Factoraje Vencido" */
						pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("Monto de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					if(!this.bOperaFactVencimientoInfonavit){
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					if("".equals(this.sFechaVencPyme)){
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					if(!this.bTipoFactoraje) {
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("B","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
					if(this.bOperaFactConMandato) {
						pdfDoc.setCell("Mandatario","celda01",ComunesPDF.CENTER);
					}
					if(this.bOperaFactorajeDistribuido||this.bOperaFactVencimientoInfonavit) {
						pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Porcentaje Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					}else{
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					if(!this.bOperaFactConMandato) {
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				}
				else if("11".equals(this.iNoEstatusDocto)){
					pdfDoc.setCell("A","celda02",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre PyME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Solicitud","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					if(this.bTipoFactoraje) { /* Desplegar solo si opera "Factoraje Vencido" */
						pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					}	
					pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
					if(!this.bTipoFactoraje) {
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("B","celda02",ComunesPDF.CENTER);
					if(!"".equals(this.sFechaVencPyme)){
						pdfDoc.setCell("Fecha de Vencimiento PyME","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
					if(this.bOperaFactConMandato) {
						pdfDoc.setCell("Mandatario","celda01",ComunesPDF.CENTER);
					}
					if(this.bOperaFactorajeDistribuido||this.bOperaFactVencimientoInfonavit) {
						pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Porcentaje Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					}else{
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					if("".equals(this.sFechaVencPyme)){
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					if(!this.bOperaFactConMandato) {
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				}
				while(rs.next()){
					String tasa = "";
					String tasaOperada = "";
					String numSolicitud = "";
					double montoInt = 0;
					String plazo = "";
					double montoOper = 0;
					String referencia = "";
					String netoRecibir = "";
					String nombreIf = "";
					String nombreBeneficiario = "";
					String porcBeneficiario = "";
					String montoBeneficiario = "";
					String acuse = "";
					String fechaDocto = "";
					double dblPorcentaje = 0;
					double monto = 0;
					int moneda = 0;
					String tipoFactoraje = "";
					BigDecimal mtoDesc = new BigDecimal("0.0");
					
					//Validaciones previas
					moneda = Integer.parseInt(rs.getString("IC_MONEDA").trim());
					monto = Double.parseDouble(rs.getString("FN_MONTO").trim());
					moneda = rs.getInt("ic_moneda");
					dblPorcentaje = rs.getDouble("FN_PORC_ANTICIPO");
					mtoDesc = new BigDecimal(rs.getString("FN_MONTO_DSCTO"));
					nombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
					if(this.iNoEstatusDocto.equals("2")){//Negociable
						if(moneda == 1){
							dblPorcentaje = Double.parseDouble(porcentaje)*100;
						}else if(moneda == 54){
							dblPorcentaje = Double.parseDouble(porcentajeDL)*100;
						}
						tipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
						nombreIf = "";
						if(this.bOperaFactorajeVencido){//Para factoraje vencido
							if(tipoFactoraje.equals("V")||tipoFactoraje.equals("M")||this.bOperaFactConMandato||this.bOperaFactVencimientoInfonavit){
								dblPorcentaje = 100;
								mtoDesc = new BigDecimal(monto);
								nombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
							}
						}
						if(this.bOperaFactVencimientoInfonavit){// para Factoraje Vencimiento Infonavit FODEA 042 - Agosto/2009
							nombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
						}
						if(this.bOperaFactorajeDistribuido || this.bOperaFactVencimientoInfonavit){//Para Factoraje Distribuido
							if(tipoFactoraje.equals("D")||tipoFactoraje.equals("I")){
								dblPorcentaje = 100;
								mtoDesc = new BigDecimal(monto);
								nombreBeneficiario = (rs.getString("NOMBRE_BENEFICIARIO")==null)?"":rs.getString("NOMBRE_BENEFICIARIO");
								porcBeneficiario = (rs.getString("FN_PORC_BENEFICIARIO")==null)?"":rs.getString("FN_PORC_BENEFICIARIO");
								montoBeneficiario = (rs.getString("MONTO_BENEFICIARIO")==null)?"":rs.getString("MONTO_BENEFICIARIO");							
							}
						}
						if(this.bOperaFactorajeNotaDeCredito){// aqui new para factoraje Nota de Credito
							if(tipoFactoraje.equals("C")){
								dblPorcentaje = 100;
								mtoDesc = new BigDecimal(monto);
							}
						}
					}else if(this.iNoEstatusDocto.equals("3")||this.iNoEstatusDocto.equals("26")){//Seleccionada Pyme
						if(this.bOperaFactorajeVencido || this.bOperaFactConMandato){
							if(tipoFactoraje.equals("V")||tipoFactoraje.equals("A") || this.bOperaFactConMandato || this.bOperaFactVencimientoInfonavit){
								dblPorcentaje =  100;
								mtoDesc = new BigDecimal(monto);
							}
						}
						if(this.bOperaFactorajeDistribuido || this.bOperaFactVencimientoInfonavit) { // Para Factoraje Distribuido
							if(tipoFactoraje.equals("D") || tipoFactoraje.equals("I")) {
								dblPorcentaje =  100;
								mtoDesc = new BigDecimal(monto);
								netoRecibir = (rs.getString("NETO_RECIBIR")==null)?"":rs.getString("NETO_RECIBIR");
								nombreBeneficiario = (rs.getString("NOMBRE_BENEFICIARIO")==null)?"":rs.getString("NOMBRE_BENEFICIARIO");
								porcBeneficiario = (rs.getString("FN_PORC_BENEFICIARIO")==null)?"":rs.getString("FN_PORC_BENEFICIARIO");
								montoBeneficiario = (rs.getString("MONTO_BENEFICIARIO")==null)?"":rs.getString("MONTO_BENEFICIARIO");
							}
						}
						if(this.bOperaFactorajeNotaDeCredito){ // aqui new para factoraje Nota de Credito
							if(tipoFactoraje.equals("C")){
								dblPorcentaje =  100;
								mtoDesc = new BigDecimal(monto);
							}
						}
					}else if(this.iNoEstatusDocto.equals("24")||this.iNoEstatusDocto.equals("11")||this.iNoEstatusDocto.equals("4,16")){//Proceso de Autorizacion IF
						if(this.bOperaFactorajeVencido){// Para Factoraje Vencido
							if(tipoFactoraje.equals("V")){
								dblPorcentaje = 100;
								mtoDesc = new BigDecimal(monto);
							}
						}
						if(this.bOperaFactorajeDistribuido || this.bOperaFactVencimientoInfonavit) { // Para Factoraje Distribuido
							if(tipoFactoraje.equals("D") || tipoFactoraje.equals("I")) {
								dblPorcentaje = 100;
								mtoDesc = new BigDecimal(monto);
								netoRecibir = (rs.getString("NETO_RECIBIR")==null)?"":rs.getString("NETO_RECIBIR");
								nombreBeneficiario = (rs.getString("NOMBRE_BENEFICIARIO")==null)?"":rs.getString("NOMBRE_BENEFICIARIO");
								porcBeneficiario = (rs.getString("FN_PORC_BENEFICIARIO")==null)?"":rs.getString("FN_PORC_BENEFICIARIO");
								montoBeneficiario = (rs.getString("MONTO_BENEFICIARIO")==null)?"":rs.getString("MONTO_BENEFICIARIO");							
							}
						}
						if(this.bOperaFactorajeNotaDeCredito){ // aqui new para factoraje Nota de Credito
							if(tipoFactoraje.equals("C")){
								dblPorcentaje =  100;
								mtoDesc = new BigDecimal(monto);							
							}
						}
					}		
					String proveedor	= (rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME");
					String numeroDocto	= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					String fechaVto	= (rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC");
					String fechaVencPyme	= (rs.getString("FECHA_VENC_PYME")==null)?"":rs.getString("FECHA_VENC_PYME");
					String nombreMoneda	= (rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA");
					String tipoFactorajeDesc	= (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE");
					String mandatario	= (rs.getString("MANDANTE")==null)?"":rs.getString("MANDANTE");
					if(!("4,16".equals(this.iNoEstatusDocto))&&!("11".equals(this.iNoEstatusDocto))){
						acuse	= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					}
					if(!("2".equals(this.iNoEstatusDocto))){
						tasa = (rs.getString("IN_TASA_ACEPTADA")==null)?"":rs.getString("IN_TASA_ACEPTADA");
						netoRecibir = (rs.getString("NETO_RECIBIR")==null)?"":rs.getString("NETO_RECIBIR");
					}
					if("11".equals(this.iNoEstatusDocto)){
						tasaOperada = (rs.getString("FN_PORC_ANTICIPO")==null)?"":rs.getString("FN_PORC_ANTICIPO");
						numSolicitud = (rs.getString("IC_FOLIO")==null)?"":rs.getString("IC_FOLIO");
					}else{
						fechaDocto	= (rs.getString("FECHA_DOCTO")==null)?"":rs.getString("FECHA_DOCTO");
					}
					if("3".equals(this.iNoEstatusDocto)||"24".equals(this.iNoEstatusDocto)||"26".equals(this.iNoEstatusDocto)){
						plazo = (rs.getString("PLAZO")==null)?"":rs.getString("PLAZO");
						montoInt = rs.getDouble("IN_IMPORTE_INTERES");
					}
					if(!("2".equals(this.iNoEstatusDocto))&&!("11".equals(this.iNoEstatusDocto))){
						montoOper = rs.getDouble("IN_IMPORTE_RECIBIR");
						referencia = (rs.getString("CT_REFERENCIA")==null)?"":rs.getString("CT_REFERENCIA");
					}
					if("2".equals(this.iNoEstatusDocto)){
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(proveedor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numeroDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVto,"formas",ComunesPDF.CENTER);
						if(!"".equals(this.sFechaVencPyme)){
							pdfDoc.setCell(fechaVencPyme,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell(nombreMoneda,"formas",ComunesPDF.CENTER);
						if(this.bOperaFactorajeVencido || this.bOperaFactorajeDistribuido 
						|| this.bOperaFactorajeNotaDeCredito || this.bOperaFactVencimientoInfonavit) { /* Desplegar solo si opera "Factoraje Vencido" */
							pdfDoc.setCell(tipoFactorajeDesc,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(monto), false),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(mtoDesc.toString(), false),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER);
						
						if("".equals(this.sFechaVencPyme)){
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						if(!this.bOperaFactorajeVencido && !this.bOperaFactorajeDistribuido 
						&& !this.bOperaFactorajeNotaDeCredito && !this.bOperaFactVencimientoInfonavit) {
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
						if(this.bOperaFactorajeVencido || this.bOperaFactConMandato || this.bOperaFactVencimientoInfonavit ) { /* Desplegar solo si opera "Factoraje Vencido" */
							pdfDoc.setCell( nombreIf,"formas",ComunesPDF.CENTER);
						}
						if(this.bOperaFactConMandato) {
							pdfDoc.setCell(mandatario,"formas",ComunesPDF.CENTER);
						}
						if(this.bOperaFactorajeDistribuido) {
							pdfDoc.setCell(nombreBeneficiario,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2)+" %","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(montoBeneficiario, false),"formas",ComunesPDF.CENTER);
						}
						if(!this.bOperaFactorajeVencido && !this.bOperaFactConMandato && !this.bOperaFactVencimientoInfonavit ) {
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						if(!this.bOperaFactConMandato) {
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						if(!this.bOperaFactorajeDistribuido) {
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					else if("3".equals(this.iNoEstatusDocto)
					||"24".equals(this.iNoEstatusDocto)
					||"26".equals(this.iNoEstatusDocto)){
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(proveedor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numeroDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVto,"formas",ComunesPDF.CENTER);
						if(!"".equals(this.sFechaVencPyme)){
							pdfDoc.setCell(fechaVencPyme,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell(nombreMoneda,"formas",ComunesPDF.CENTER);
						if(this.bTipoFactoraje) { /* Desplegar solo si opera "Factoraje Vencido" */
							pdfDoc.setCell(tipoFactorajeDesc,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(monto),false),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(mtoDesc),false),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);
						
						if("".equals(this.sFechaVencPyme)){
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						if(!this.bTipoFactoraje) {
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(plazo,0),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(montoInt),false),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(montoOper),false),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER);
						if(this.bOperaFactConMandato) {
							pdfDoc.setCell(mandatario,"formas",ComunesPDF.CENTER);
						}
						if(("24".equals(this.iNoEstatusDocto)&&(this.bOperaFactorajeDistribuido || this.bOperaFactVencimientoInfonavit))
						||(!("24".equals(this.iNoEstatusDocto))&&this.bOperaFactorajeDistribuido)){
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(netoRecibir, false),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(nombreBeneficiario,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2)+" %","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(montoBeneficiario, false),"formas",ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						if(!this.bOperaFactConMandato) {
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
					}
					else if("4,16".equals(this.iNoEstatusDocto)){
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(proveedor,"formas",ComunesPDF.CENTER);
						if(this.bOperaFactVencimientoInfonavit){
							pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell(numeroDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVto,"formas",ComunesPDF.CENTER);
						if(!"".equals(this.sFechaVencPyme)){
							pdfDoc.setCell(fechaVencPyme,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell(nombreMoneda,"formas",ComunesPDF.CENTER);
						if(this.bTipoFactoraje) { /* Desplegar solo si opera "Factoraje Vencido" */
							pdfDoc.setCell(tipoFactorajeDesc,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(monto),false),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(mtoDesc),false),"formas",ComunesPDF.CENTER);
						if(!this.bOperaFactVencimientoInfonavit){
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						if("".equals(this.sFechaVencPyme)){
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						if(!this.bTipoFactoraje) {
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(montoOper),false),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
						if(this.bOperaFactConMandato) {
							pdfDoc.setCell(mandatario,"formas",ComunesPDF.CENTER);
						}
						if(this.bOperaFactorajeDistribuido||this.bOperaFactVencimientoInfonavit) {
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(netoRecibir, false),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(nombreBeneficiario,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2)+" %","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(montoBeneficiario, false),"formas",ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						if(!this.bOperaFactConMandato) {
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					else if("11".equals(this.iNoEstatusDocto)){
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(proveedor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numSolicitud,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numeroDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreMoneda,"formas",ComunesPDF.CENTER);
						if(this.bTipoFactoraje) { /* Desplegar solo si opera "Factoraje Vencido" */
							pdfDoc.setCell(tipoFactorajeDesc,"formas",ComunesPDF.CENTER);
						}	
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(monto),false),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(tasaOperada),0,false)+" %","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(mtoDesc),false),"formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(fechaVto,"formas",ComunesPDF.CENTER);
						if(!this.bTipoFactoraje) {
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
						if(!"".equals(this.sFechaVencPyme)){
							pdfDoc.setCell(fechaVencPyme,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell(fechaVto,"formas",ComunesPDF.CENTER);
						if(this.bOperaFactConMandato) {
							pdfDoc.setCell(mandatario,"formas",ComunesPDF.CENTER);
						}
						if(this.bOperaFactorajeDistribuido||this.bOperaFactVencimientoInfonavit) {
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(netoRecibir, false),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(nombreBeneficiario,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(Comunes.formatoDecimal(porcBeneficiario,2)+" %","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(montoBeneficiario, false),"formas",ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						if("".equals(this.sFechaVencPyme)){
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						if(!this.bOperaFactConMandato) {
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					//Totales
					if(!tipoFactoraje.equals("C")) {
						if (moneda==1) {
							totalMontoMN = totalMontoMN + monto;
							totalMontoDescontarMN = totalMontoDescontarMN + mtoDesc.doubleValue();
							if(!("2".equals(this.iNoEstatusDocto))&&!("11".equals(this.iNoEstatusDocto))){
								if(!("4,16".equals(this.iNoEstatusDocto))){
									totalMontoIntMN = totalMontoIntMN + montoInt;
								}
								totalImporteRecibirMN = totalImporteRecibirMN + montoOper;
							}
							totalDoctosMN++;
						} else {
							totalMontoUSD = totalMontoUSD + monto;
							totalMontoDescontarUSD = totalMontoDescontarUSD + mtoDesc.doubleValue();
							if(!("2".equals(this.iNoEstatusDocto))&&!("11".equals(this.iNoEstatusDocto))){
								if(!("4,16".equals(this.iNoEstatusDocto))){
									totalMontoIntUSD = totalMontoIntUSD + montoInt;
								}
								totalImporteRecibirUSD = totalImporteRecibirUSD + montoOper;
							}
							totalDoctosUSD++;
						}
					}						
				}
				pdfDoc.addTable();
				pdfDoc.setTable(2,100);
				if(totalDoctosMN>0){
					pdfDoc.setCell("Total Documentos en M.N.","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell(Integer.toString(totalDoctosMN),"celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Total Monto M.N.","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMontoMN,2),"celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Total Monto Descontar M.N.","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMontoDescontarMN,2),"celda01",ComunesPDF.LEFT);
					if(!("2".equals(this.iNoEstatusDocto))&&!("11".equals(this.iNoEstatusDocto))){
						if(!("4,16".equals(this.iNoEstatusDocto))){
							pdfDoc.setCell("Total Monto Int M.N.","celda01",ComunesPDF.RIGHT);
							pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMontoIntMN,2),"celda01",ComunesPDF.LEFT);
						}
						pdfDoc.setCell("Total Importe a Recibir M.N.","celda01",ComunesPDF.RIGHT);
						pdfDoc.setCell("$" + Comunes.formatoDecimal(totalImporteRecibirMN,2),"celda01",ComunesPDF.LEFT);
					}
				}
				if(totalDoctosUSD>0){
					pdfDoc.setCell("Total Documentos en D�lares","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell(Integer.toString(totalDoctosUSD),"celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Total Monto D�lares","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMontoUSD,2),"celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Total Monto Descontar D�lares","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMontoDescontarUSD,2),"celda01",ComunesPDF.LEFT);
					if(!("2".equals(this.iNoEstatusDocto))&&!("11".equals(this.iNoEstatusDocto))){
						if(!("4,16".equals(this.iNoEstatusDocto))){
							pdfDoc.setCell("Total Monto Int D�lares","celda01",ComunesPDF.RIGHT);
							pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMontoIntUSD,2),"celda01",ComunesPDF.LEFT);
						}
						pdfDoc.setCell("Total Importe a Recibir D�lares","celda01",ComunesPDF.RIGHT);
						pdfDoc.setCell("$" + Comunes.formatoDecimal(totalImporteRecibirUSD,2),"celda01",ComunesPDF.LEFT);
					}
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException ("Error al generar el archivo", e);
			}
		}
		return nombreArchivo;
	}
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path,String tipo) {
		String nombreArchivo = "";
		return nombreArchivo;
	}
	public String getQueryPorEstatus() {
		conditions		= new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		StringBuffer campos = new StringBuffer();
		StringBuffer tablas = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		StringBuffer order = new StringBuffer();
		String sReferencia = ",'RepDoctosEpoDEbean:getQueryPorEstatus("+iNoEstatusDocto+")' as PANTALLA";
		
    	AccesoDB 				con 	= new AccesoDB();
    	PreparedStatement 	ps 	= null;
    	ResultSet 				rs 	= null;
    	
		System.out.println("La clave del estatus es: "+iNoEstatusDocto);
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
		try {
			


			hint.append("/*+ index(ac2)*/");
			/*
			 *CAMPOS
			 */
			campos.append(
				" doc.ic_pyme"+
				" ,'' AS dbl_Porcentaje"+
				" ,(DECODE (cpe.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || pym.cg_razon_social) as nombrePyme"   +
				" ,doc.ig_numero_docto"+
				" ,mon.cd_nombre"+idioma+" as nombreMoneda"   +
				" ,doc.ic_moneda"   +
				" ,doc.fn_monto"   +
				" ,doc.cs_dscto_especial"   +
				" ,(DECODE (cif.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || cif.cg_razon_social) AS nombreif"   +
				" ,cif2.cg_razon_social AS nombre_beneficiario"   +
				" ,doc.fn_porc_beneficiario");

			if("2".equals(iNoEstatusDocto)) {
				campos.append(
					" ,doc.cc_acuse"+
					" ,DECODE (doc.ic_moneda, 1, ?, 54, ?) AS fn_porc_anticipo"   +
					" ,doc.fn_monto * (DECODE (doc.ic_moneda, 1, ?, 54, ?)) AS fn_monto_dscto"   +
					" ,(doc.fn_porc_beneficiario / 100) * doc.fn_monto AS monto_beneficiario");
				conditions.add(this.porcentaje);
				conditions.add(this.porcentajeDL);
				conditions.add(this.porcentaje);
				conditions.add(this.porcentajeDL);
			}
			if("2".equals(iNoEstatusDocto) || "3".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto) || "4,16".equals(iNoEstatusDocto))
				campos.append(" ,TO_CHAR (doc.df_fecha_docto, 'DD/MM/YYYY') fecha_docto");

			if("2".equals(iNoEstatusDocto) || "3".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto) || "4,16".equals(iNoEstatusDocto))
				campos.append(" ,TO_CHAR (doc.df_fecha_venc, 'DD/MM/YYYY') fecha_venc");
				campos.append(" ,TO_CHAR (doc.df_fecha_venc_pyme, 'DD/MM/YYYY') fecha_venc_pyme");

			//if("3".equals(iNoEstatusDocto) || "4,16".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto)) {
			if("26".equals(iNoEstatusDocto)) {
				campos.append(
					" ,ds.in_tasa_aceptada"   +
					" ,doc.fn_porc_anticipo"   +
					" ,doc.fn_monto_dscto"   +
					" ,ds.in_importe_recibir - (ds.fn_importe_recibir_benef) AS neto_recibir"   +
					" ,ds.fn_importe_recibir_benef AS monto_beneficiario");
			}
			//Agregado Fodea 17
			if("3".equals(iNoEstatusDocto) || "4,16".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto)) {
				campos.append(
					//" ,ds.in_tasa_aceptada"   + //decode?
					" ,NVL(ds.in_tasa_aceptada_fondeo,ds.in_tasa_aceptada) as in_tasa_aceptada"+
					" ,doc.fn_porc_anticipo"   +
					" ,doc.fn_monto_dscto"   +
					" ,ds.in_importe_recibir - (ds.fn_importe_recibir_benef) AS neto_recibir"   +
					" ,ds.fn_importe_recibir_benef AS monto_beneficiario");
			}
			
			//if("3".equals(iNoEstatusDocto) || "4,16".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto)) {
			if("26".equals(iNoEstatusDocto)) {
				campos.append(
					" ,ds.in_importe_recibir"   +
					" ,doc.ct_referencia");
			}
			
		//	Agregado Fodea 17
			
			if("3".equals(iNoEstatusDocto) || "4,16".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto)) {
				campos.append(
					//" ,ds.in_importe_recibir"   +
					", NVL(ds.in_importe_recibir_fondeo,ds.in_importe_recibir) as in_importe_recibir"+
					" ,doc.ct_referencia");
			}
			
		
			// Fodea 048 - 2012. Determinar el Plazo
			// " ,(doc.df_fecha_venc"+sFechaVencPyme+" - DECODE(doc.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaSigDol+"', 'dd/mm/yyyy'))) PLAZO "+
			// Obtener Fecha del siguiente d�a h�bil
			String fechaHabilSiguienteXEpo = null; // new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			if("3".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto)) {
				
				con.conexionDB();
 
				String qrySentencia 	= "ALTER SESSION SET NLS_TERRITORY = MEXICO";
				ps 						= con.queryPrecompilado(qrySentencia);
				ps.executeUpdate();
				ps.close();
	
				qrySentencia 	=
					" SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha"   +
					"   FROM dual"  ;
				ps 				= con.queryPrecompilado(qrySentencia);
				ps.setInt(1, Integer.parseInt(iNoEpo));
				rs 				= ps.executeQuery();
				ps.clearParameters();
				if(rs.next()) {
					fechaHabilSiguienteXEpo = rs.getString(1);
				}
				// Este error NUNCA DEBER�A DE OCRRIR se deja para avisar al usuario que se presento un problema con la
				// funcion sigfechahabilxepo y de esta manera evitar que se le muestre un plazo erroneo
				if( fechaHabilSiguienteXEpo == null ){
					throw new AppException("No se pudo determinar la fecha del d�a h�bil siguiente por EPO");
				}
				rs.close();
				ps.close();
				
			}
			// Agregar condiciones para calcular el plazo
			if(
				"3".equals(iNoEstatusDocto)  //  "3": Seleccionada Pyme
					|| 
				"24".equals(iNoEstatusDocto) // "24": En Proceso de Autorizacion IF
			){
				
				// Agregar las condiciones para la consulta del plazo
				campos.append(
					", ( " +
						" doc.df_fecha_venc"+sFechaVencPyme + " - " +
						" 	(CASE  " +
						" 	WHEN doc.ic_moneda = 1 OR (doc.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
						"  	TRUNC(SYSDATE) " +
						" 	WHEN doc.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
						"  	TO_DATE('"+fechaHabilSiguienteXEpo+"', 'dd/mm/yyyy')  "  +
						//"    sigfechahabilxepo(doc.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	END) " + 
					" ) " + 	
					" AS PLAZO "
				);
				
			} else if( 
				"26".equals(iNoEstatusDocto) // "26": Programado Pyme 
			){
			
				// Nota: como funciona el siguiente dia habil cuando se consulte el documento en su d�a programado 
				// Con el propsito de agilizar la consulta, obtener la fecha del segundo d�a h�bil sguiente por EPO
				String segundaFechaHabilSiguienteXEpo = null;
				
				String qrySentencia 	=
					" SELECT TO_CHAR(sigfechahabilxepo ( ?, TO_DATE(?,'DD/MM/YYYY'), 1, '+'), 'dd/mm/yyyy') fecha"   +
					"   FROM dual"  ;
				ps 				= con.queryPrecompilado(qrySentencia);
				ps.setInt(1, 		Integer.parseInt(iNoEpo));
				ps.setString(2, 	fechaHabilSiguienteXEpo);
				rs 				= ps.executeQuery();
				ps.clearParameters();
				if(rs.next()) {
					segundaFechaHabilSiguienteXEpo = rs.getString(1);
				}
				// Este error NUNCA DEBER�A DE OCRRIR se deja para avisar al usuario que se presento un problema con la
				// funcion sigfechahabilxepo y de esta manera evitar que se le muestre un plazo erroneo
				if( segundaFechaHabilSiguienteXEpo == null ){
					throw new AppException("No se pudo determinar la fecha del segundo d�a h�bil siguiente por EPO");
				}
				
				// Agregar las condiciones para la consulta del plazo
				campos.append(
					", ( "  +
						" doc.df_fecha_venc"+sFechaVencPyme + " - "  +
						" 	(CASE  "  +
						" 	WHEN doc.ic_moneda = 1 OR (doc.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN "  +
						// "    TRUNC(SYSDATE) " +
						"    TO_DATE ('"+fechaHabilSiguienteXEpo+"',        'DD/MM/YYYY' ) "  +
						" 	WHEN doc.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN "  +
						//"    sigfechahabilxepo(doc.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						"    TO_DATE ('"+segundaFechaHabilSiguienteXEpo+"', 'DD/MM/YYYY' ) "  +
						" 	END) "  + 
					" ) "  + 	
					" AS PLAZO "
				);
						
			}
			
			if("26".equals(iNoEstatusDocto)) {
				campos.append(
					" ,ac2.cc_acuse"   +
					" ,ds.in_importe_interes");
			}
			
			if("3".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto)) {
				campos.append(
					" ,ac2.cc_acuse"   +
					" , nvl(ds.IN_IMPORTE_INTERES_FONDEO, ds.in_importe_interes) as in_importe_interes ");
			}
			
			if("4,16".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto))
				campos.append(" ,SUBSTR (sol.ic_folio, 1, 10) || '-' || SUBSTR (sol.ic_folio, 11, 1) as ic_folio");

			if("26".equals(iNoEstatusDocto) || "3".equals(iNoEstatusDocto) )
				campos.append(" ,TO_CHAR (ds.df_programacion, 'DD/MM/YYYY') df_programacion"); // Fodea 005-2009 24 Hrs

			campos.append(" ,ma.CG_RAZON_SOCIAL as mandante"+ 
						" ,tf.cg_nombre as TIPO_FACTORAJE"); // Fodea 023 2009
			/*
			 *TABLAS
			 */
			tablas.append(
				" FROM com_documento doc"   +
				" ,comcat_if cif "   +
				" ,comcat_pyme pym "   +
				" ,COMCAT_MANDANTE ma "+  // Fodea 023 2009
				" ,COMCAT_TIPO_FACTORAJE tf "+  // Fodea 023 2009
				" ,comrel_pyme_epo cpe ");

			if("2".equals(iNoEstatusDocto))
				tablas.append(" ,com_acuse1 ac1 ");

			if("3".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto))
				tablas.append(" ,com_acuse2 ac2");

			if("3".equals(iNoEstatusDocto) || "4,16".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto)  )
				tablas.append(" ,com_docto_seleccionado ds");

			if("4,16".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto))
				tablas.append(" ,com_solicitud sol");

			tablas.append(
				" ,comcat_moneda mon"+
				" ,comcat_if cif2");
 
			// Fodea 048 - 2012
			if( 
					"3".equals(iNoEstatusDocto)  //  "3": Seleccionada Pyme
						|| 
					"24".equals(iNoEstatusDocto) // "24": En Proceso de Autorizacion IF
						|| 
					"26".equals(iNoEstatusDocto) // "26": Programado Pyme
			) {
				tablas.append(
					" , comrel_if_epo_x_producto iexp "
				);
			}
 
			/*
			 *CONDICION
			 */
			condicion.append(
				" WHERE doc.ic_pyme = pym.ic_pyme");
				condicion.append("  AND ma.IC_MANDANTE(+) = doc.IC_MANDANTE"); // Fodea 023 2009
				condicion.append("  AND doc.cs_dscto_especial = tf.cc_tipo_factoraje "); // Fodea 023 2009
				
			if("3".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto) ) {
				condicion.append(
					" AND ac2.cc_acuse = ds.cc_acuse"+										
					" AND ac2.df_fecha_hora >= TRUNC(SYSDATE)  AND ac2.df_fecha_hora < TRUNC(SYSDATE) + 1 "+
					" AND ac2.ic_producto_nafin = 1 ");
			}
			// Fodea 000 - 2012. Fodea 000 - 2012. ADMIN - Proceso de Aut IF	
			if( "24".equals(iNoEstatusDocto) ) {
				condicion.append(
					" AND ac2.cc_acuse = ds.cc_acuse"+
					" AND ac2.ic_producto_nafin = 1 ");
			}
			if("2".equals(iNoEstatusDocto)) {
				condicion.append(
					" AND doc.cc_acuse = ac1.cc_acuse"+
					" AND doc.ic_if = cif.ic_if (+)"   +					
					" AND ac1.df_fechahora_carga >= TRUNC(SYSDATE)  AND ac1.df_fechahora_carga < TRUNC(SYSDATE) + 1 "+
					" AND ac1.ic_producto_nafin = 1" );
			} else if("4,16".equals(iNoEstatusDocto)) {
				condicion.append(				
					" AND sol.df_fecha_solicitud >= TRUNC(SYSDATE)  AND sol.df_fecha_solicitud < TRUNC(SYSDATE) + 1 ");					
			} else if("11".equals(iNoEstatusDocto)) {
				condicion.append(
					" AND TO_CHAR (doc.df_fecha_venc"+sFechaVencPyme+", 'DD/MM/YYYY') = TO_CHAR (SYSDATE, 'DD/MM/YYYY')");

			}
			if("3".equals(iNoEstatusDocto) || "4,16".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto) || "24".equals(iNoEstatusDocto) || "26".equals(iNoEstatusDocto)) {
				condicion.append(
					" AND doc.ic_if = cif.ic_if"   +
					" AND doc.ic_documento = ds.ic_documento");
			}
			if("11".equals(iNoEstatusDocto))
				condicion.append(  " AND ds.ic_documento = sol.ic_documento");

			condicion.append(
				" AND pym.ic_pyme = cpe.ic_pyme"   +
				" AND doc.ic_beneficiario = cif2.ic_if (+)"   +
				" AND doc.ic_moneda = mon.ic_moneda" +
				" AND cpe.ic_epo = ?"   +
				" AND doc.ic_epo = ?"   +
				" AND doc.ic_estatus_docto in ("+iNoEstatusDocto+") ");
				conditions.add(this.iNoEpo);
				conditions.add(this.iNoEpo);
			
			// Fodea 048 - 2012
			if( 
					"3".equals(iNoEstatusDocto)  //  "3": Seleccionada Pyme
						|| 
					"24".equals(iNoEstatusDocto) // "24": En Proceso de Autorizacion IF
						|| 
					"26".equals(iNoEstatusDocto) // "26": Programado Pyme
			){
				condicion.append(
					" 	AND doc.ic_if              = iexp.ic_if  " +
					" 	AND doc.ic_epo             = iexp.ic_epo " +
					" 	AND iexp.ic_producto_nafin = 1 "
				);
			}
						
			order.append(
				" ORDER BY nombrePyme "+
				"		   ,doc.ig_numero_docto ");

			if("2".equals(iNoEstatusDocto))
				order.append( "		   ,fecha_docto ");

			else if("2".equals(iNoEstatusDocto) || "11".equals(iNoEstatusDocto))
				order.append( " 	       ,fecha_venc"+sFechaVencPyme+" ");

			order.append(
				" 	       ,nombreMoneda "+
				" 	       ,doc.fn_monto "+
				" 	       ,doc.fn_monto_dscto");
			if("11".equals(iNoEstatusDocto)) {
				order.append(
					" 	       ,nombreif "+
					" 	       ,ds.in_tasa_aceptada ");
			}

			if("4,16".equals(iNoEstatusDocto)) {
				hint.append("/*+ use_nl(doc,cif,pym,cpe,ds,sol,mon ,cif2) index(doc)*/");
				sbQuery.append(
					" SELECT * FROM ("+
					" 	SELECT "+hint.toString()+" "+campos.toString()+" "+tablas.toString()+" "+condicion.toString()+" AND ds.ic_documento = sol.ic_documento "
				);

				if("S".equals(sOperaNotas)) {
					sbQuery.append(
						" 	UNION ALL "+
						" 	SELECT "+hint+" "+campos.toString()+" "+tablas.toString()+" "+condicion.toString()+" AND doc.ic_docto_asociado = sol.ic_documento "
					);
					conditions.add(this.iNoEpo);
					conditions.add(this.iNoEpo);
				}

				sbQuery.append(
					" ) "
				);

			} else
				sbQuery.append(" SELECT "+hint+" "+campos.toString()+" "+tablas.toString()+" "+condicion.toString()+order.toString());
			
			log.debug("RepDoctosEpoDEbean :::qrySentencia  :: "+qrySentencia);	
			log.debug("RepDoctosEpoDEbean :::condiciones  :: "+conditions.toString());
			log.debug("RepDoctosEpoDEbean :::sbQuery :::: "+iNoEstatusDocto+":: "+sbQuery);
			
		} catch(Exception e) {
			log.error("RepDoctosEpoDEbean::getQueryPorEstatus Exception "+e);
			e.printStackTrace();
		} finally {
			if( rs != null){ try{ rs.close(); }catch(Exception e){} }
			if( ps != null){ try{ ps.close(); }catch(Exception e){} } 
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			
		}
		return sbQuery.toString();
	} // getQueryPorCambiosEstatus()
	public List getConditions(){
		return conditions;
	}
	public void setiNoEstatusDocto(String iNoEstatusDocto) {
		this.iNoEstatusDocto = iNoEstatusDocto;
	}
	public void setiNoEpo(String iNoEpo) {
		this.iNoEpo = iNoEpo;
	}
	public void setsOperaNotas(String sOperaNotas) {
		this.sOperaNotas = sOperaNotas;
	}
	public void setsIdiomaUsuario(String sesIdiomaUsuario) {
		this.sesIdiomaUsuario = sesIdiomaUsuario;
	}
	public void setsFechaVencPyme(String sFechaVencPyme) {
		this.sFechaVencPyme= sFechaVencPyme;
	}
	public void setPorcentaje(String porcentaje) {
		this.porcentaje = porcentaje;
	}
	public String getPorcentaje() {
		return porcentaje;
	}
	public void setPorcentajeDL(String porcentajeDL) {
		this.porcentajeDL = porcentajeDL;
	}
	public String getPorcentajeDL() {
		return porcentajeDL;
	}
	public void setBOperaFactorajeVencido(boolean bOperaFactorajeVencido) {
		this.bOperaFactorajeVencido = bOperaFactorajeVencido;
	}
	public boolean isBOperaFactorajeVencido() {
		return bOperaFactorajeVencido;
	}
	public void setBOperaFactorajeDistribuido(boolean bOperaFactorajeDistribuido) {
		this.bOperaFactorajeDistribuido = bOperaFactorajeDistribuido;
	}
	public boolean isBOperaFactorajeDistribuido() {
		return bOperaFactorajeDistribuido;
	}
	public void setBOperaFactorajeNotaDeCredito(boolean bOperaFactorajeNotaDeCredito) {
		this.bOperaFactorajeNotaDeCredito = bOperaFactorajeNotaDeCredito;
	}
	public boolean isBOperaFactorajeNotaDeCredito() {
		return bOperaFactorajeNotaDeCredito;
	}
	public void setBOperaFactConMandato(boolean bOperaFactConMandato) {
		this.bOperaFactConMandato = bOperaFactConMandato;
	}
	public boolean isBOperaFactConMandato() {
		return bOperaFactConMandato;
	}
	public void setBOperaFactVencimientoInfonavit(boolean bOperaFactVencimientoInfonavit) {
		this.bOperaFactVencimientoInfonavit = bOperaFactVencimientoInfonavit;
	}
	public boolean isBOperaFactVencimientoInfonavit() {
		return bOperaFactVencimientoInfonavit;
	}
	public void setBTipoFactoraje(boolean bTipoFactoraje) {
		this.bTipoFactoraje = bTipoFactoraje;
	}
	public boolean isBTipoFactoraje() {
		return bTipoFactoraje;
	}
} // Fin de la clase.
