package com.netro.descuento;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCtaBancariasDist implements IQueryGeneratorRegExtJS {
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsCtaBancariasDist.class);
	private String paginaOffset;
	private String paginaNo;
	private List 	conditions;
	StringBuffer	qrySentencia	= new StringBuffer("");
	private String ic_epo;
	private String txtNE;
	private String ic_if;
	private String txtBanco;
	private String txtSucursal;
	private String cboMoneda;
	private String txtCuenta;
	private String cboPlaza;
	private String CtasCapturadas;

	public ConsCtaBancariasDist() {
	}

	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentQuery() {

		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(
			" SELECT cbp.ic_cuenta_bancaria "   +
			" FROM comrel_cuenta_bancaria_x_prod cbp, comcat_epo epo, comcat_if cif, "   +
			" comcat_financiera fin, comcat_pyme pym, comcat_moneda mon, comcat_plaza plz, "   +
			" comrel_nafin crn "   +
			" WHERE cbp.ic_epo = epo.ic_epo "   +
			" AND cbp.ic_if = cif.ic_if (+) "   +
			" AND cbp.ic_financiera = fin.ic_financiera (+) "   +
			" AND cbp.ic_pyme = pym.ic_pyme "   +
			" AND cbp.ic_moneda = mon.ic_moneda "   +
			" AND cbp.ic_plaza = plz.ic_plaza "   +
			" AND crn.ic_epo_pyme_if = pym.ic_pyme "   +
			" AND crn.cg_tipo = ? "   +
			" AND ic_producto_nafin = ? "
		);
		conditions.add("P");
		conditions.add(new Integer(4));

		if( ic_epo != null &&	!"".equals(ic_epo)	){
			qrySentencia.append(" AND cbp.ic_epo = ? ");
			conditions.add(ic_epo);
		}

		if(	txtNE != null &&	!"".equals(txtNE)	){
			qrySentencia.append(" AND crn.ic_nafin_electronico = ? ");
			conditions.add(txtNE);
		}

		if(	ic_if != null &&	!"".equals(ic_if)	){
			qrySentencia.append(" AND cbp.ic_if = ? ");
			conditions.add(ic_if);
		}

		if(	txtBanco != null &&	!"".equals(txtBanco)	){
			qrySentencia.append(" AND cbp.ic_financiera = ? ");
			conditions.add(txtBanco);
		}

		if(	txtSucursal != null &&	!"".equals(txtSucursal)	){
			qrySentencia.append(" AND cbp.cg_sucursal = ? ");
			conditions.add(txtSucursal);
		}

		if(	cboMoneda != null &&	!"".equals(cboMoneda)	){
			qrySentencia.append(" AND cbp.ic_moneda = ? ");
			conditions.add(cboMoneda);
		}

		if(	txtCuenta != null &&	!"".equals(txtCuenta)	){
			qrySentencia.append(" AND cbp.cg_numero_cuenta = ? ");
			conditions.add(txtCuenta);
		}

		if(	cboPlaza != null &&	!"".equals(cboPlaza)		){
			qrySentencia.append(" AND cbp.ic_plaza = ? ");
			conditions.add(cboPlaza);
		}

		if(	CtasCapturadas != null &&	!"".equals(CtasCapturadas)	){
			qrySentencia.append(" AND cbp.ic_cuenta_bancaria IN (" +CtasCapturadas +") ");			
		}

		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		qrySentencia.append(
			" SELECT epo.cg_razon_social AS nomepo,"   +
			"     cif.cg_razon_social AS nomif,"   +
			"     fin.cd_nombre AS nomfin,"   +
			"     pym.cg_razon_social AS nompyme,"   +
			"     pym.cg_rfc AS rfc,"   +
			"     cbp.cg_sucursal AS sucursal,"   +
			"     mon.cd_nombre AS moneda,"   +
			"     cbp.cg_numero_cuenta AS nocta,"   +
			"     decode (plz.cd_descripcion, NULL, ' ', plz.cd_descripcion||', '||plz.cg_estado) AS plaza "   +
			" FROM comrel_cuenta_bancaria_x_prod cbp, comcat_epo epo, comcat_if cif, "   +
			" comcat_financiera fin, comcat_pyme pym, comcat_moneda mon, comcat_plaza plz, "   +
			" comrel_nafin crn "   +
			" WHERE cbp.ic_epo = epo.ic_epo "   +
			" AND cbp.ic_if = cif.ic_if (+) "   +
			" AND cbp.ic_financiera = fin.ic_financiera (+) "   +
			" AND cbp.ic_pyme = pym.ic_pyme "   +
			" AND cbp.ic_moneda = mon.ic_moneda "   +
			" AND cbp.ic_plaza = plz.ic_plaza "   +
			" AND crn.ic_epo_pyme_if = pym.ic_pyme "   +
			" AND crn.cg_tipo = ? "   +
			" AND ic_producto_nafin = ? "
		);
		conditions.add("P");
		conditions.add(new Integer(4));

		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i==0) {
				qrySentencia.append("  AND  ( "); 
			} else {
				qrySentencia.append("  OR  "); 
			}
			qrySentencia.append(" ( cbp.ic_cuenta_bancaria = ? ) "); 
			conditions.add(new Long(lItem.get(0).toString()));
		}//for(int i=0;i<ids.size();i++)
		qrySentencia.append("  ) "); 
		
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		return "";
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {		
		return "";
	}

	public void setPaginaOffset(String paginaOffset) {
		this.paginaOffset = paginaOffset;
	}

	public String getPaginaOffset() {
		return paginaOffset;
	}

	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}

	public String getPaginaNo() {
		return paginaNo;
	}

	public void setClaveEpo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getClaveEpo() {
		return ic_epo;
	}

	public void setNEPyme(String txtNE) {
		this.txtNE = txtNE;
	}

	public String getNEPyme() {
		return txtNE;
	}

	public void setClaveIf(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getClaveIf() {
		return ic_if;
	}

	public void setBanco(String txtBanco) {
		this.txtBanco = txtBanco;
	}

	public String getBanco() {
		return txtBanco;
	}

	public void setSucursal(String txtSucursal){
		this.txtSucursal = txtSucursal;
	}

	public String getSucursal(){
		return txtSucursal;
	}

	public void setMoneda(String cboMoneda){
		this.cboMoneda = cboMoneda;
	}

	public String getMoneda(){
		return cboMoneda;
	}

	public void setCuenta(String txtCuenta){
		this.txtCuenta = txtCuenta;
	}

	public String getCuenta(){
		return txtCuenta;
	}

	public void setPlaza(String cboPlaza){
		this.cboPlaza = cboPlaza;
	}

	public String getPlaza(){
		return cboPlaza;
	}

	public void setCuentasCaptura(String CtasCapturadas){
		this.CtasCapturadas = CtasCapturadas;
	}

	public String getCuentasCaptura(){
		return CtasCapturadas;
	}

	public List getConditions(){
		return this.conditions;
	}

}