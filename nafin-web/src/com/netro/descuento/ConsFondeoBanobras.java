package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsFondeoBanobras implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsFondeoBanobras.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String df_vencimiento_inicio;
	private String df_vencimiento_fin;
	private String df_operacion_inicio;
	private String df_operacion_fin;
	private String ic_moneda;
	
	 
	public ConsFondeoBanobras() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		
		 qrySentencia.append(" Select    /*+ ordered  use_nl(s ds d m)*/   "+
			" TO_CHAR(s.df_operacion, 'DD/MM/YYYY') as FECHA_OPERACION ,  "+
			" TO_CHAR(s.df_v_descuento, 'DD/MM/YYYY') as FECHA_VENCIMIENTO ,  "+
			" s.ig_plazo as PLAZO,  "+
			" m.cd_nombre as MONEDA,   "+
			" count(1) as TOTAL_DOCUMENTOS,  "+
			" TO_CHAR(sum(ds.in_importe_recibir),'FM999999999.99') as MONTO_FONDEO "+
			" from com_solicitud s  "+
			" ,com_docto_seleccionado ds  "+
			" ,com_documento d  "+
			" ,comcat_moneda m  "+
			" where d.ic_documento = ds.ic_documento  "+
			" and ds.ic_documento = s.ic_documento  "+
			" and d.ic_moneda = m.ic_moneda  "+
			" and s.cs_obra_publica = 'S'  "+
			" and s.ic_estatus_solic  IN (3,5,6)   ");
			
			
			if (!df_operacion_inicio.equals("") && !df_operacion_fin.equals("")) {
				qrySentencia.append(" and s.df_operacion >= to_date(?, 'dd/mm/yyyy') "+
										  " and s.df_operacion < to_date(?, 'dd/mm/yyyy')+1 ");
				conditions.add(df_operacion_inicio);
				conditions.add(df_operacion_fin);
			}
			if (!df_vencimiento_inicio.equals("") && !df_vencimiento_fin.equals("")) {			
				qrySentencia.append(" and s.df_v_descuento >= to_date(?, 'dd/mm/yyyy') " +
				      				  " and s.df_v_descuento < to_date(?, 'dd/mm/yyyy')+1 ");
				conditions.add(df_vencimiento_inicio);
				conditions.add(df_vencimiento_fin);
			}
			if (!ic_moneda.equals("")) {
				qrySentencia.append("and d.ic_moneda = ? ");
				conditions.add(ic_moneda);
			}
			
			qrySentencia.append(" Group by s.df_operacion, s.df_v_descuento, s.ig_plazo, m.cd_nombre  order by s.df_operacion ");
					
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		int numRegistrosMN =0, numRegistrosDL=0;
		BigDecimal montoTotal = new BigDecimal("0.00");				
		int totalDoc= 0;
			
		try {
		
			if(tipo.equals("PDF") ) {
				
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				
				pdfDoc.setTable(6,100);										
				pdfDoc.setCell("Fecha Operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Total de Documentos","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Monto de Fondeo","celda01",ComunesPDF.CENTER); 			
			}
			
			
			contenidoArchivo.append("Fecha Operaci�n,  Fecha Vencimiento, Plazo, Moneda ,Total de Documentos, Monto de Fondeo \n");

			while (rs.next())	{
				String fechaOperacion = (rs.getString("FECHA_OPERACION") == null) ? "" : rs.getString("FECHA_OPERACION");
				String fechaVencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
				String plazo = (rs.getString("PLAZO") == null) ? "" : rs.getString("PLAZO");
				String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
				String total_documentos = (rs.getString("TOTAL_DOCUMENTOS") == null) ? "" : rs.getString("TOTAL_DOCUMENTOS");
				String monto_fondeo = (rs.getString("MONTO_FONDEO") == null) ? "" : rs.getString("MONTO_FONDEO");
			
				montoTotal = montoTotal.add(new BigDecimal(monto_fondeo));
				totalDoc +=Integer.parseInt(total_documentos);			
			
		
				if(tipo.equals("PDF") ) {
					pdfDoc.setCell(fechaOperacion,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(total_documentos,"formas",ComunesPDF.CENTER);						
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_fondeo,2),"formas",ComunesPDF.RIGHT);
				}
				
				if(tipo.equals("CSV") ) {
				
					contenidoArchivo.append(fechaOperacion.replaceAll(",","")+","+
													fechaVencimiento.replaceAll(",","")+","+
													plazo.replaceAll(",","")+","+
													moneda.replaceAll(",","")+","+
													total_documentos.replaceAll(",","")+","+
													monto_fondeo.replaceAll(",","")+"\n");				
				}
			}
			
			if(tipo.equals("PDF") ) {
			
			pdfDoc.setCell("TOTAL","formas",ComunesPDF.RIGHT,4);	
			pdfDoc.setCell(String.valueOf(totalDoc),"formas",ComunesPDF.CENTER);	
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotal.toPlainString(),2),"formas",ComunesPDF.RIGHT);
			
			
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			}
			if(tipo.equals("CSV") ) {
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getDf_vencimiento_inicio() {
		return df_vencimiento_inicio;
	}

	public void setDf_vencimiento_inicio(String df_vencimiento_inicio) {
		this.df_vencimiento_inicio = df_vencimiento_inicio;
	}

	public String getDf_vencimiento_fin() {
		return df_vencimiento_fin;
	}

	public void setDf_vencimiento_fin(String df_vencimiento_fin) {
		this.df_vencimiento_fin = df_vencimiento_fin;
	}

	public String getDf_operacion_inicio() {
		return df_operacion_inicio;
	}

	public void setDf_operacion_inicio(String df_operacion_inicio) {
		this.df_operacion_inicio = df_operacion_inicio;
	}

	public String getDf_operacion_fin() {
		return df_operacion_fin;
	}

	public void setDf_operacion_fin(String df_operacion_fin) {
		this.df_operacion_fin = df_operacion_fin;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}




	

}