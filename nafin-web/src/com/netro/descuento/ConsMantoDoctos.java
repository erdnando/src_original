package com.netro.descuento;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsMantoDoctos implements IQueryGeneratorRegExtJS {

	private int numList = 1;
	private ArrayList conditions = null;
	private final static Log log = ServiceLocator.getInstance().getLog(ConsMantoDoctos.class);
	
	
	private String esClavePyme = "";
	private String esNumeroDocto = "";
	private String esFechaDocumento = "";
	private String esFechaVencimiento = "";
	private String esClaveMoneda = "";
	private String esMontoMinimo = "";
	private String esMontoMaximo = "";
	private String esEstatus = "";
	private String esAforo = "";
	private String esClaveEpo = "";
	private String esAforoDL = "";
	private String tipoConsulta;
	private String montoDocto;
	

	public ConsMantoDoctos() {	}
	
	public int getNumList(){
		return numList;
	}
	
  
  /**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  

  
  /**
	 * Metodo que genera el query para obtener las llaves primarias para
	 * la paginacion en la consulta.
	 * Cualquier modificacion a este m�todo debe ser evaluada, para determinar que
	 * optimizaci�n aplica m�s, entrar por com_acuse3 o por com_documento, para
	 * que de esta manera se agregue el c�digo donde corresponda.
	 * En caso que ninguna de las dos aplique, ser� necesario crear una secci�n
	 * para el caso en particular.
	 * @param request Objeto HTTP Request
	 * @return Cadena con la consulta del query para generacion de claves.
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E) ::..");
		this.conditions = new ArrayList();
		StringBuffer queryCons = new StringBuffer();

		boolean ebOperaFactorajeVencido = true;//(request.getParameter("ebOperaFactorajeVencido") == null)?"":request.getParameter("ebOperaFactorajeVencido");
		boolean ebInicial = true;//(request.getParameter("ebInicial") == null)?"":request.getParameter("ebInicial");
			
		queryCons.append("SELECT d.ic_documento    " );
		queryCons.append("  FROM com_documento d, " );
		queryCons.append("       comcat_pyme py, " );
		queryCons.append("       comrel_pyme_epo pe " );
		queryCons.append(" WHERE  " );
		queryCons.append("     py.ic_pyme = pe.ic_pyme " );
		queryCons.append("    AND d.ic_pyme = py.ic_pyme " );
		queryCons.append("    and d.ic_epo = ? " );
		queryCons.append("   AND d.cs_dscto_especial != 'C' " );
		queryCons.append("   AND pe.ic_epo = ? " );
		queryCons.append("   AND pe.cs_habilitado = 'S' ");
		conditions.add(new Integer(esClaveEpo));
		conditions.add(new Integer(esClaveEpo));
				
		if (esClavePyme!=null && !esClavePyme.equals("")){
			queryCons.append(" AND d.ic_pyme = ? ");
			conditions.add(new Integer(esClavePyme));
		}
		if (esNumeroDocto!=null && !esNumeroDocto.equals(""))
			queryCons.append(" AND d.ig_numero_docto = ? ");
			conditions.add(esNumeroDocto);
		if (esFechaDocumento!=null && !esFechaDocumento.equals(""))
			queryCons.append(" AND d.df_fecha_docto=TO_DATE(?,'dd/mm/yyyy') ");
			conditions.add(esFechaDocumento);
		if (!esFechaVencimiento.equals(""))
			queryCons.append(" AND d.df_fecha_venc=TO_DATE(?,'dd/mm/yyyy') ");
			conditions.add(esFechaVencimiento);
		if (!esClaveMoneda.equals(""))
			queryCons.append(" AND d.ic_moneda = ? ");
			conditions.add(new Integer(esClaveMoneda));
		if (!esMontoMinimo.equals(""))
		{
			if (!esMontoMaximo.equals("")){
				queryCons.append(" AND d.fn_monto BETWEEN ? AND ? ");
				conditions.add(new Double(esMontoMinimo));
				conditions.add(new Double(esMontoMaximo));
			}else{
				queryCons.append(" AND d.fn_monto = ? ");
				conditions.add(new Double(esMontoMaximo));
			}
		}
		if (!esEstatus.equals("")){
			queryCons.append(" AND d.ic_estatus_docto = ? ");
			conditions.add(new Integer(esEstatus));
		}else{
			queryCons.append(" AND d.ic_estatus_docto in (?,?,?) ");
			conditions.add(new Integer("1"));
			conditions.add(new Integer("2"));
			conditions.add(new Integer("21"));
		}
				
		//if(!"1".equals(lsCondEstatus)) {
		queryCons.append("UNION " );
		queryCons.append("SELECT d.ic_documento " );
		queryCons.append(" FROM com_documento d, comcat_pyme py, " );
		queryCons.append("     comrel_pyme_epo pe " );
		queryCons.append(" WHERE py.ic_pyme = pe.ic_pyme " );
		queryCons.append("   AND d.ic_pyme=py.ic_pyme " );
		queryCons.append("   AND d.CS_DSCTO_ESPECIAL='C'  " );
		queryCons.append("   AND d.ic_epo= ? " );
		queryCons.append("   AND pe.ic_epo = ? " );
		queryCons.append("   AND pe.cs_habilitado='S' ");
		conditions.add(new Integer(esClaveEpo));
		conditions.add(new Integer(esClaveEpo));
				
		if (esClavePyme!=null && !esClavePyme.equals("")){
			queryCons.append(" AND d.ic_pyme = ? ");
			conditions.add(new Integer(esClavePyme));
		}
		if (esNumeroDocto!=null && !esNumeroDocto.equals(""))
			queryCons.append(" AND d.ig_numero_docto = ? ");
			conditions.add(esNumeroDocto);
		if (esFechaDocumento!=null && !esFechaDocumento.equals(""))
			queryCons.append(" AND d.df_fecha_docto=TO_DATE(?,'dd/mm/yyyy') ");
			conditions.add(esFechaDocumento);
		if (!esFechaVencimiento.equals(""))
			queryCons.append(" AND d.df_fecha_venc=TO_DATE(?,'dd/mm/yyyy') ");
			conditions.add(esFechaVencimiento);
		if (!esClaveMoneda.equals(""))
			queryCons.append(" AND d.ic_moneda = ? ");
			conditions.add(new Integer(esClaveMoneda));
		if (!esMontoMinimo.equals(""))
		{
			if (!esMontoMaximo.equals("")){
				queryCons.append(" AND d.fn_monto BETWEEN ? AND ? ");
				conditions.add(new Double(esMontoMinimo));
				conditions.add(new Double(esMontoMaximo));
			}else{
				queryCons.append(" AND d.fn_monto = ? ");
				conditions.add(new Double(esMontoMaximo));
			}
		}
		if ("2".equals(esEstatus)||"21".equals(esEstatus)||"1".equals(esEstatus) ||"28".equals(esEstatus) ||"33".equals(esEstatus)) {
			queryCons.append(" AND d.ic_estatus_docto = ? ");
			conditions.add(new Integer(esEstatus));
		}else{
			queryCons.append(" AND d.ic_estatus_docto in (?,?,?, ? ) ");
			conditions.add(new Integer("1"));
			conditions.add(new Integer("2"));
			conditions.add(new Integer("21"));
			conditions.add(new Integer("33"));
		}
				
			

		String lsQrySentencia = "select ic_documento from ("+queryCons.toString()+") ORDER BY ic_documento ";


		log.debug("lsQrySentencia: "+lsQrySentencia);
		log.debug("conditions::"+this.conditions);
		log.info("getDocumentQuery(S) ::..");
		return lsQrySentencia;
	}

  
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		StringBuffer queryCons 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		queryCons.append(" SELECT ic_documento, ig_numero_docto, df_fecha_docto, df_fecha_venc, fn_monto, ");
		queryCons.append(" cg_razon_social, cd_descripcion, ic_moneda, cd_nombre , ");
		queryCons.append(" montoDescuento, CS_DSCTO_ESPECIAL, NombreIF, ic_estatus_docto, ");
		queryCons.append(" df_fecha_venc_pyme, mandante, TIPO_FACTORAJE ");
		queryCons.append(" FROM ( ");
		queryCons.append("SELECT d.ic_documento, d.ig_numero_docto, ");
		queryCons.append("   TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, ");
		queryCons.append("   TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, ");
		queryCons.append("   d.fn_monto, ");
		queryCons.append("   decode (py.cs_habilitado,'N','* ','')|| py.cg_razon_social as cg_razon_social, ");
		queryCons.append("   ed.cd_descripcion as cd_descripcion , " );
		queryCons.append("    m.ic_moneda, ");
		queryCons.append("   m.cd_nombre as cd_nombre, " );
		queryCons.append("   decode(d.ic_moneda,1, ?, 54, ?)*d.fn_monto as montoDescuento, ");
		conditions.add(new Double(esAforo));
		conditions.add(new Double(esAforoDL));
		queryCons.append("   CS_DSCTO_ESPECIAL, ");
		queryCons.append("   decode (I.cs_habilitado,'N','* ', ' ')||I.cg_razon_social as NombreIF ");
		queryCons.append("   ,d.ic_estatus_docto");
		queryCons.append("   ,TO_CHAR(d.df_fecha_venc_pyme,'dd/mm/yyyy') as df_fecha_venc_pyme ");
		queryCons.append("   ,d.ic_pyme as clave_pyme, ma.CG_RAZON_SOCIAL as mandante ");
		queryCons.append("   , tf.cg_nombre as TIPO_FACTORAJE ");
		queryCons.append(" FROM com_documento d, comcat_pyme py, comcat_estatus_docto ed, ");
		queryCons.append("   comcat_moneda m, comcat_if I, comrel_pyme_epo pe, COMCAT_MANDANTE ma, COMCAT_TIPO_FACTORAJE tf ");
		queryCons.append(" WHERE d.ic_epo = ? ");
		conditions.add(new Integer(esClaveEpo));
		queryCons.append("   AND d.ic_pyme = py.ic_pyme");
		queryCons.append("   AND d.CS_DSCTO_ESPECIAL != 'C' ");
		queryCons.append("   AND py.ic_pyme = pe.ic_pyme");
		queryCons.append("   AND pe.ic_epo = ? ");
		conditions.add(new Integer(esClaveEpo));
		queryCons.append("   AND pe.cs_habilitado = 'S'");
		queryCons.append("   AND d.ic_moneda=m.ic_moneda");
		queryCons.append("  AND ma.IC_MANDANTE(+) = d.IC_MANDANTE ");
		queryCons.append("   AND d.ic_estatus_docto = ed.ic_estatus_docto ");
		queryCons.append("   AND d.cs_dscto_especial = tf.cc_tipo_factoraje  ");
		queryCons.append("   AND d.ic_if = I.ic_if(+) ");
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (List)pageIds.get(i);
			if(i==0) {
				queryCons.append(" AND d.ic_documento in  ( ");
			}
			queryCons.append("?");
			conditions.add(new Long(lItem.get(0).toString()));					
			if(i!=(pageIds.size()-1)) {
				queryCons.append(",");
			} else {
				queryCons.append(" ) ");
			}			
		}
		
		queryCons.append(" UNION ");
		queryCons.append("SELECT d.ic_documento, d.ig_numero_docto, ");
		queryCons.append("   TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, ");
		queryCons.append("   TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, ");
		queryCons.append("   d.fn_monto, ");
		queryCons.append("   decode (py.cs_habilitado,'N','* ','')|| py.cg_razon_social as cg_razon_social , ");
		queryCons.append("   ed.cd_descripcion as cd_descripcion , " );
		queryCons.append("   m.ic_moneda, ");
		queryCons.append("   m.cd_nombre as cd_nombre, " );
		queryCons.append("   decode(d.ic_moneda,1, ?, 54, ?)*d.fn_monto as montoDescuento, ");
		conditions.add(new Double(esAforo));
		conditions.add(new Double(esAforoDL));
		queryCons.append("   CS_DSCTO_ESPECIAL, ");
		queryCons.append("   decode (I.cs_habilitado,'N','* ', ' ')||I.cg_razon_social as NombreIF ");
		queryCons.append("   ,d.ic_estatus_docto");
		queryCons.append("   ,TO_CHAR(d.df_fecha_venc_pyme,'dd/mm/yyyy') as df_fecha_venc_pyme ");
		queryCons.append("   ,d.ic_pyme as clave_pyme, ma.CG_RAZON_SOCIAL as mandante, ");//<<================================= FODEA 050 - 2008
		queryCons.append("   tf.cg_nombre as TIPO_FACTORAJE ");
		queryCons.append(" FROM com_documento d, comcat_pyme py, comcat_estatus_docto ed, ");
		queryCons.append("   comcat_moneda m, comcat_if I, comrel_pyme_epo pe, COMCAT_MANDANTE ma, COMCAT_TIPO_FACTORAJE tf ");
		queryCons.append(" WHERE d.ic_epo= ? ");
		conditions.add(new Integer(esClaveEpo));
		queryCons.append("   AND d.ic_pyme=py.ic_pyme");
		queryCons.append("   AND d.CS_DSCTO_ESPECIAL='C' ");
		queryCons.append("   AND py.ic_pyme = pe.ic_pyme");
		queryCons.append("   AND pe.ic_epo = ? ");
		conditions.add(new Integer(esClaveEpo));
		queryCons.append("   AND pe.cs_habilitado='S'");
		queryCons.append("   AND d.ic_moneda=m.ic_moneda");
		queryCons.append("   AND ma.IC_MANDANTE(+) = d.IC_MANDANTE ");
		queryCons.append("   AND d.cs_dscto_especial = tf.cc_tipo_factoraje  ");
		queryCons.append("   AND d.ic_estatus_docto=ed.ic_estatus_docto ");
		queryCons.append("   AND d.ic_if=I.ic_if(+) ");
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (List)pageIds.get(i);
			if(i==0) {
				queryCons.append(" AND d.ic_documento in  ( ");
			}
			queryCons.append("?");
			conditions.add(new Long(lItem.get(0).toString()));					
			if(i!=(pageIds.size()-1)) {
				queryCons.append(",");
			} else {
				queryCons.append(" ) ");
			}			
		}
		
		queryCons.append(") ORDER BY cg_razon_social, ic_moneda, df_fecha_docto, ig_numero_docto ");
		
		
		log.debug("..:: qrySentencia "+queryCons.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return queryCons.toString();
	}
	
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		StringBuffer queryCons 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		log.debug("..:: tipoConsulta "+tipoConsulta  );
		
		if (tipoConsulta.equals("Consulta")){ 
		
			queryCons.append(" SELECT IC_DOCUMENTO, IG_NUMERO_DOCTO, DF_FECHA_DOCTO, DF_FECHA_VENC, FN_MONTO, ");
			queryCons.append(" CLAVE_PYME, CG_RAZON_SOCIAL, CD_DESCRIPCION, IC_MONEDA, CD_NOMBRE , ");
			queryCons.append(" MONTODESCUENTO, CS_DSCTO_ESPECIAL, NOMBREIF, IC_ESTATUS_DOCTO, ");
			queryCons.append(" DF_FECHA_VENC_PYME, MANDANTE, TIPO_FACTORAJE, ");
			queryCons.append(" '' MONTOPORCENTAJE, '' FECRECEP, '' TIPOCOMPRA, '' CLASIFICADOR, '' PLAZOMAX  ");
			queryCons.append(" FROM ( ");
			queryCons.append("SELECT d.ic_documento, d.ig_numero_docto, ");
			queryCons.append("   TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, ");
			queryCons.append("   TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, ");
			queryCons.append("   d.fn_monto, ");
			queryCons.append("   decode (py.cs_habilitado,'N','* ','')|| py.cg_razon_social as cg_razon_social, ");
			queryCons.append("   ed.cd_descripcion as cd_descripcion , " );
			queryCons.append("    m.ic_moneda, ");
			queryCons.append("   m.cd_nombre as cd_nombre, " );
			queryCons.append("   decode(d.ic_moneda,1, ?, 54, ?)*d.fn_monto as montoDescuento, ");
			conditions.add(new Double(esAforo));
			conditions.add(new Double(esAforoDL));
			queryCons.append("   CS_DSCTO_ESPECIAL, ");
			queryCons.append("   decode (I.cs_habilitado,'N','* ', ' ')||I.cg_razon_social as NombreIF ");
			queryCons.append("   ,d.ic_estatus_docto");
			queryCons.append("   ,TO_CHAR(d.df_fecha_venc_pyme,'dd/mm/yyyy') as df_fecha_venc_pyme ");
			queryCons.append("   ,d.ic_pyme as clave_pyme, ma.CG_RAZON_SOCIAL as mandante ");
			queryCons.append("   , tf.cg_nombre as TIPO_FACTORAJE ");
			queryCons.append(" FROM com_documento d, comcat_pyme py, comcat_estatus_docto ed, ");
			queryCons.append("   comcat_moneda m, comcat_if I, comrel_pyme_epo pe, COMCAT_MANDANTE ma, COMCAT_TIPO_FACTORAJE tf ");
			queryCons.append(" WHERE d.ic_epo = ? ");
			conditions.add(new Integer(esClaveEpo));
			queryCons.append("   AND d.ic_pyme = py.ic_pyme");
			queryCons.append("   AND d.CS_DSCTO_ESPECIAL != 'C' ");
			queryCons.append("   AND py.ic_pyme = pe.ic_pyme");
			queryCons.append("   AND pe.ic_epo = ? ");
			conditions.add(new Integer(esClaveEpo));
			queryCons.append("   AND pe.cs_habilitado = 'S'");
			queryCons.append("   AND d.ic_moneda=m.ic_moneda");
			queryCons.append("  AND ma.IC_MANDANTE(+) = d.IC_MANDANTE ");
			queryCons.append("   AND d.ic_estatus_docto = ed.ic_estatus_docto ");
			queryCons.append("   AND d.cs_dscto_especial = tf.cc_tipo_factoraje  ");
			queryCons.append("   AND d.ic_if = I.ic_if(+) ");
			
			if (esClavePyme!=null && !esClavePyme.equals("")){
				queryCons.append(" AND d.ic_pyme = ? ");
				conditions.add(new Integer(esClavePyme));
			}
			if (esNumeroDocto!=null && !esNumeroDocto.equals("")){
				queryCons.append(" AND d.ig_numero_docto = ? ");
				conditions.add(esNumeroDocto);
			}
			if (esFechaDocumento!=null && !esFechaDocumento.equals("")){
				queryCons.append(" AND d.df_fecha_docto=TO_DATE(?,'dd/mm/yyyy') ");
				conditions.add(esFechaDocumento);
			}
			if (esFechaVencimiento!= null && !esFechaVencimiento.equals("")){
				queryCons.append(" AND d.df_fecha_venc=TO_DATE(?,'dd/mm/yyyy') ");
				conditions.add(esFechaVencimiento);
			}
			if (esClaveMoneda!=null && !esClaveMoneda.equals("")){
				queryCons.append(" AND d.ic_moneda = ? ");
				conditions.add(new Integer(esClaveMoneda));
			}
			if (esMontoMinimo!=null && !esMontoMinimo.equals(""))
			{
				if (esMontoMaximo!=null && !esMontoMaximo.equals("")){
					queryCons.append(" AND d.fn_monto BETWEEN ? AND ? ");
					conditions.add(new Double(esMontoMinimo));
					conditions.add(new Double(esMontoMaximo));
				}else{
					queryCons.append(" AND d.fn_monto = ? ");
					conditions.add(new Double(esMontoMaximo));
				}
			}
			if (esEstatus!=null && !esEstatus.equals("")){
				queryCons.append(" AND d.ic_estatus_docto = ? ");
				conditions.add(new Integer(esEstatus));
			}else{
				queryCons.append(" AND d.ic_estatus_docto in (?,?,?) ");
				conditions.add(new Integer("1"));
				conditions.add(new Integer("2"));
				conditions.add(new Integer("21"));
			}
			
			queryCons.append(" UNION ");
			queryCons.append("SELECT d.ic_documento, d.ig_numero_docto, ");
			queryCons.append("   TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, ");
			queryCons.append("   TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, ");
			queryCons.append("   d.fn_monto, ");
			queryCons.append("   decode (py.cs_habilitado,'N','* ','')|| py.cg_razon_social as cg_razon_social , ");
			queryCons.append("   ed.cd_descripcion as cd_descripcion , " );
			queryCons.append("   m.ic_moneda, ");
			queryCons.append("   m.cd_nombre as cd_nombre, " );
			queryCons.append("   decode(d.ic_moneda,1, ?, 54, ?)*d.fn_monto as montoDescuento, ");
			conditions.add(new Double(esAforo));
			conditions.add(new Double(esAforoDL));
			queryCons.append("   CS_DSCTO_ESPECIAL, ");
			queryCons.append("   decode (I.cs_habilitado,'N','* ', ' ')||I.cg_razon_social as NombreIF ");
			queryCons.append("   ,d.ic_estatus_docto");
			queryCons.append("   ,TO_CHAR(d.df_fecha_venc_pyme,'dd/mm/yyyy') as df_fecha_venc_pyme ");
			queryCons.append("   ,d.ic_pyme as clave_pyme, ma.CG_RAZON_SOCIAL as mandante, ");//<<================================= FODEA 050 - 2008
			queryCons.append("   tf.cg_nombre as TIPO_FACTORAJE ");
			queryCons.append(" FROM com_documento d, comcat_pyme py, comcat_estatus_docto ed, ");
			queryCons.append("   comcat_moneda m, comcat_if I, comrel_pyme_epo pe, COMCAT_MANDANTE ma, COMCAT_TIPO_FACTORAJE tf ");
			queryCons.append(" WHERE d.ic_epo= ? ");
			conditions.add(new Integer(esClaveEpo));
			queryCons.append("   AND d.ic_pyme=py.ic_pyme");
			queryCons.append("   AND d.CS_DSCTO_ESPECIAL='C' ");
			queryCons.append("   AND py.ic_pyme = pe.ic_pyme");
			queryCons.append("   AND pe.ic_epo = ? ");
			conditions.add(new Integer(esClaveEpo));
			queryCons.append("   AND pe.cs_habilitado='S'");
			queryCons.append("   AND d.ic_moneda=m.ic_moneda");
			queryCons.append("   AND ma.IC_MANDANTE(+) = d.IC_MANDANTE ");
			queryCons.append("   AND d.cs_dscto_especial = tf.cc_tipo_factoraje  ");
			queryCons.append("   AND d.ic_estatus_docto=ed.ic_estatus_docto ");
			queryCons.append("   AND d.ic_if=I.ic_if(+) ");
			if (esClavePyme!=null && !esClavePyme.equals("")){
				queryCons.append(" AND d.ic_pyme = ? ");
				conditions.add(new Integer(esClavePyme));
			}
			if (esNumeroDocto!=null && !esNumeroDocto.equals("")){
				queryCons.append(" AND d.ig_numero_docto = ? ");
				conditions.add(esNumeroDocto);
			}
			if (esFechaDocumento!=null && !esFechaDocumento.equals("")){
				queryCons.append(" AND d.df_fecha_docto=TO_DATE(?,'dd/mm/yyyy') ");
				conditions.add(esFechaDocumento);
			}
			if (esFechaVencimiento!=null && !esFechaVencimiento.equals("")){
				queryCons.append(" AND d.df_fecha_venc=TO_DATE(?,'dd/mm/yyyy') ");
				conditions.add(esFechaVencimiento);
			}
			if (esClaveMoneda!=null && !esClaveMoneda.equals("")){
				queryCons.append(" AND d.ic_moneda = ? ");
				conditions.add(new Integer(esClaveMoneda));
			}
			if (esMontoMinimo!=null && !esMontoMinimo.equals(""))
			{
				if (esMontoMaximo!=null && !esMontoMaximo.equals("")){
					queryCons.append(" AND d.fn_monto BETWEEN ? AND ? ");
					conditions.add(new Double(esMontoMinimo));
					conditions.add(new Double(esMontoMaximo));
				}else{
					queryCons.append(" AND d.fn_monto = ? ");
					conditions.add(new Double(esMontoMaximo));
				}
			}
			if ("2".equals(esEstatus)||"21".equals(esEstatus)||"1".equals(esEstatus) ||"28".equals(esEstatus) || "33".equals(esEstatus)) {
				queryCons.append(" AND d.ic_estatus_docto = ? ");
				conditions.add(new Integer(esEstatus));
			}else{
				queryCons.append(" AND d.ic_estatus_docto in (?,?,?, ? ) ");
				conditions.add(new Integer("1"));
				conditions.add(new Integer("2"));
				conditions.add(new Integer("21"));
				conditions.add(new Integer("33"));
			}
			
			queryCons.append(") ORDER BY cg_razon_social, ic_moneda, df_fecha_docto, ig_numero_docto ");
		
		}else  if (tipoConsulta.equals("Detalle")){	
		
			queryCons.append(" select e.CG_RAZON_SOCIAL as NOMBRE_EPO ,  "+
								  " p.CG_RAZON_SOCIAL AS  NOMBRE_PYME ,   "+
								  " d.IG_NUMERO_DOCTO AS NUM_DOCUMENTO, "+
								  " es.CD_DESCRIPCION as ESTATUS,  "+
								  " TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy')  as FECHA_EMISION, "+
								  " m.CD_NOMBRE  as MONEDA, "+
								  " d.fn_monto as MONTO "+
								  "  from com_documento d,   "+
								  " comcat_epo e,  "+
								  " comcat_pyme p , "+
								  " comcat_moneda m ,   "+
								  " comcat_estatus_docto es  "+
								  " where d.ic_epo = e.ic_epo "+
								  " and d.ic_pyme = p.ic_pyme  "+
								  " and d.ic_moneda = m.ic_moneda "+
								  " and d.ic_estatus_docto = es.ic_estatus_docto "+
								  " and d.ic_estatus_docto not in (  ?, ?, ? ) "+
								  " and d.ic_epo  = ?     "+
								  " and to_char(d.df_fecha_docto,'dd/mm/yyyy') = to_char(to_date( ? ,'dd/mm/yyyy'),'dd/mm/yyyy')   "+
								  " and d.ic_moneda =  ? "+
								  " and d.ic_pyme  =   ?  "+
								  " and d.fn_monto =  ?     ");				  
		
			conditions.add(new Integer("1"));
			conditions.add(new Integer("5"));
			conditions.add(new Integer("13"));
			conditions.add(new Integer(esClaveEpo));
			conditions.add(esFechaDocumento);
			conditions.add(new Integer(esClaveMoneda));
			conditions.add(new Integer(esClavePyme));
			conditions.add(new Double(montoDocto));
		
		}
		
		
		log.debug("..:: qrySentencia "+queryCons.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQueryFile(S)");
		return queryCons.toString();
	}
	
	
	public String getAggregateCalculationQuery() {
		return "";
	}
  
  /**
	 * metodo que genera archivo por pagina
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		return "";
	}
  
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipoArchivo) {
		return "";
	}
	


	public void setEsClavePyme(String esClavePyme) {
		this.esClavePyme = esClavePyme;
	}


	public String getEsClavePyme() {
		return esClavePyme;
	}


	public void setEsNumeroDocto(String esNumeroDocto) {
		this.esNumeroDocto = esNumeroDocto;
	}


	public String getEsNumeroDocto() {
		return esNumeroDocto;
	}


	public void setEsFechaDocumento(String esFechaDocumento) {
		this.esFechaDocumento = esFechaDocumento;
	}


	public String getEsFechaDocumento() {
		return esFechaDocumento;
	}


	public void setEsFechaVencimiento(String esFechaVencimiento) {
		this.esFechaVencimiento = esFechaVencimiento;
	}


	public String getEsFechaVencimiento() {
		return esFechaVencimiento;
	}


	public void setEsClaveMoneda(String esClaveMoneda) {
		this.esClaveMoneda = esClaveMoneda;
	}


	public String getEsClaveMoneda() {
		return esClaveMoneda;
	}


	public void setEsMontoMinimo(String esMontoMinimo) {
		this.esMontoMinimo = esMontoMinimo;
	}


	public String getEsMontoMinimo() {
		return esMontoMinimo;
	}


	public void setEsMontoMaximo(String esMontoMaximo) {
		this.esMontoMaximo = esMontoMaximo;
	}


	public String getEsMontoMaximo() {
		return esMontoMaximo;
	}


	public void setEsEstatus(String esEstatus) {
		this.esEstatus = esEstatus;
	}


	public String getEsEstatus() {
		return esEstatus;
	}


	public void setEsAforo(String esAforo) {
		this.esAforo = esAforo;
	}


	public String getEsAforo() {
		return esAforo;
	}


	public void setEsClaveEpo(String esClaveEpo) {
		this.esClaveEpo = esClaveEpo;
	}


	public String getEsClaveEpo() {
		return esClaveEpo;
	}


	public void setEsAforoDL(String esAforoDL) {
		this.esAforoDL = esAforoDL;
	}


	public String getEsAforoDL() {
		return esAforoDL;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getMontoDocto() {
		return montoDocto;
	}

	public void setMontoDocto(String montoDocto) {
		this.montoDocto = montoDocto;
	}

	
	
	
	
}