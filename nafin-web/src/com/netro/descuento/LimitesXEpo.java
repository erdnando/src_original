package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class LimitesXEpo implements IQueryGeneratorRegExtJS {

	public LimitesXEpo() {  }

	private List conditions;
	StringBuffer strQuery;

	private String claveEpo;
	private String montoLimite;
	private String claveIf;
	
	private static final Log log = ServiceLocator.getInstance().getLog(LimitesXEpo.class);//Variable para enviar mensajes al log.
	private String montoLimite_dl;

	public String getAggregateCalculationQuery() {
		return "";
 	}
		

	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer();
			
		strQuery.append(
				" SELECT "+
				" e.ic_epo as cveEpo  "+
				" FROM comcat_epo e, comrel_if_epo ie, comrel_producto_epo cpe  "+
				" WHERE ie.ic_epo=e.ic_epo and UPPER(ie.cs_vobo_nafin) = ? "+
				" AND (  ie.in_limite_epo <> ?  or    ie.in_limite_epo_dl <>  ?  ) " +
				" AND e.cs_habilitado = ? " +
				" AND cpe.ic_epo=e.ic_epo "+
				" AND cpe.ic_epo=ie.ic_epo "+
				" AND cpe.ic_producto_nafin=? "+
				" AND cpe.cs_limite_pyme!=? ");
		conditions.add("S" );
		conditions.add(new Integer(0) );
		conditions.add(new Integer(0) );
		conditions.add("S" );
		conditions.add(new Integer(1) );
		conditions.add("S" );
		
		strQuery.append( " AND ie.ic_if = ?" );
		conditions.add(new Integer(claveIf) );

		if (claveEpo != null && !claveEpo.equals("")) {
			strQuery.append( " AND ie.ic_epo = ?" );
			conditions.add(new Integer(claveEpo) );
		}
		if (montoLimite != null && !montoLimite.equals("")) {
			strQuery.append( " AND ie.in_limite_epo <= ?" );
			conditions.add(new BigDecimal(montoLimite).toPlainString());
		}
		if (montoLimite_dl != null && !montoLimite_dl.equals("")) {
			strQuery.append( " AND ie.in_limite_epo_dl <= ?" );
			conditions.add(new BigDecimal(montoLimite_dl).toPlainString());
		}
		
		strQuery.append( " ORDER BY e.cg_razon_social " );
		
		log.debug("getDocumentQuery)"+strQuery.toString());
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery 		= new StringBuffer();
		
		log.debug("pageIds)"+pageIds);
			
		strQuery.append(
				"	SELECT  /*+index(cpe) index(ie)*/  " +
				" e.ic_epo as cveEpo,  "+
				" (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ')||' '|| e.cg_razon_social) AS nombre_epo, " +
				"  ie.in_limite_epo AS limite, " +
				 " CASE "+
				"	WHEN ie.fn_limite_utilizado <>0 and  ie.in_limite_epo <>0   "+
				"		THEN ROUND (((ie.fn_limite_utilizado / ie.in_limite_epo) * 100),5)  " +
				"		ELSE 0  "+
				"  END AS porcentaje,  "+		
				" (ie.in_limite_epo - ie.fn_limite_utilizado) disponible, " +
				" decode( ie.cs_activa_limite,'S','Activa', 'N', 'Inactiva')   AS estatus, "+			
				" NVL(ie.fn_monto_comprometido,0) AS monto_comprometido,   " +
				" NVL(ie.fn_monto_comprometido_dl,0) AS monto_comprometido_dl " +
				" ,TO_char(ie.df_venc_linea, 'dd/mm/yyyy')	AS fechalinea  "+
				" ,TO_char(ie.df_cambio_admon, 'dd/mm/yyyy')	AS fechacambio  "+				
				" ,decode(ie.CS_FECHA_LIMITE,'S','SI', 'N', 'NO') as csFechalimite  "+
				" , NVL (ie.in_limite_epo, 0)  -   (  NVL (ie.fn_limite_utilizado, 0) + NVL (ie.fn_monto_comprometido, 0)  ) as MontoDisponible  "+
				"  , ie.in_limite_epo_dl AS limite_dl " +
				" , CASE "+
				"	WHEN ie.fn_limite_utilizado_dl<>0 and  ie.in_limite_epo_dl <>0   "+
				"		THEN ROUND (((ie.fn_limite_utilizado_dl / ie.in_limite_epo_dl) * 100), 5     )  "+
				"	 ELSE 0  "+
				"  END AS porcentaje_dl "+		
				" , (ie.in_limite_epo_dl - ie.fn_limite_utilizado_dl) disponible_dl " +
				" , NVL (ie.in_limite_epo_dl, 0)  -   (  NVL (ie.fn_limite_utilizado_dl, 0) + NVL (ie.fn_monto_comprometido_dl, 0)  )  as MontoDisponible_dl ,  "+
				
				 " decode(ie.CS_ACTIVA_LIMITE , 'S', 'Activa' ,'N' , 'Inactiva') as DESCRIPCION_ESTATUS,   "+        
				 "  TO_CHAR (ie.DF_BLOQ_DES, 'dd/mm/yyyy') as FECHA_BLOQ_DESB,   "+
				 " ie.CG_USUARIO_BLOQ_DES as USUARIO_BLOQ_DESB,   "+
				 " ie.CG_DEPENDENCIA_BLOQ_DES as DEPENDENCIA_BLOQ_DESB   "+  
				 
				" FROM comcat_epo e, comrel_if_epo ie, comrel_producto_epo cpe  "+
				" WHERE ie.ic_epo=e.ic_epo and UPPER(ie.cs_vobo_nafin) = ? "+
				" AND (  ie.in_limite_epo <> ?  or    ie.in_limite_epo_dl <>  ?  ) " +				
				" AND e.cs_habilitado = ? " +
				" AND cpe.ic_epo=e.ic_epo "+
				" AND cpe.ic_epo=ie.ic_epo "+
				" AND cpe.ic_producto_nafin=? "+
				" AND cpe.cs_limite_pyme!=? ");

		conditions.add("S" );
		conditions.add(new Integer(0) );
		conditions.add(new Integer(0) );
		conditions.add("S" );
		conditions.add(new Integer(1) );
		conditions.add("S" );

		
		strQuery.append( " AND ie.ic_if = ?" );
		conditions.add(claveIf );
						
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (List)pageIds.get(i);
			if(i==0) {
				strQuery.append(" AND ie.ic_epo  IN ( ");
			}
			strQuery.append("?");
			conditions.add(new Long(lItem.get(0).toString()));					
			if(i!=(pageIds.size()-1)) {
				strQuery.append(",");
			} else {
				strQuery.append(" ) ");
			}			
		}
		
		strQuery.append( " ORDER BY e.cg_razon_social" );
	
	
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		
						
		return strQuery.toString();
 	}
		
			
	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		strQuery = new StringBuffer();
		
		strQuery.append(
				"	SELECT  /*+index(cpe) index(ie)*/  " +
				" e.ic_epo as cveEpo,  "+
				" (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ')||' '|| e.cg_razon_social) AS nombre_epo, " +
				" ie.in_limite_epo AS limite, " +
				 " CASE "+
				"	WHEN ie.fn_limite_utilizado <>0 and  ie.in_limite_epo <>0   "+
				"		THEN ROUND (((ie.fn_limite_utilizado / ie.in_limite_epo) * 100),5)  " +
				"		ELSE 0  "+
				"  END AS porcentaje,  "+	
				" (ie.in_limite_epo - ie.fn_limite_utilizado) disponible, " +
				" decode( ie.cs_activa_limite,'S','Activa', 'N', 'Inactiva')   AS estatus, "+			
				" NVL(ie.fn_monto_comprometido,0) AS monto_comprometido,   " +
				" NVL(ie.fn_monto_comprometido_dl,0) AS monto_comprometido_dl  " +
				" ,TO_char(ie.df_venc_linea, 'dd/mm/yyyy')	AS fechalinea  "+
				" ,TO_char(ie.df_cambio_admon, 'dd/mm/yyyy')	AS fechacambio  "+				
				" ,decode(ie.CS_FECHA_LIMITE,'S','SI', 'N', 'NO') as csFechalimite  "+
				" , (NVL (ie.in_limite_epo, 0) - NVL (ie.fn_limite_utilizado, 0)    )   + NVL (ie.fn_monto_comprometido, 0) as MontoDisponible  "+
				"  , ie.in_limite_epo_dl AS limite_dl " +
				" , CASE "+
				"	WHEN ie.fn_limite_utilizado_dl<>0 and  ie.in_limite_epo_dl <>0   "+
				"		THEN ROUND (((ie.fn_limite_utilizado_dl / ie.in_limite_epo_dl) * 100), 5     )  "+
				"	 ELSE 0  "+
				"  END AS porcentaje_dl "+		
				" , (ie.in_limite_epo_dl - ie.fn_limite_utilizado_dl) disponible_dl " +
				" , NVL (ie.in_limite_epo_dl, 0)  -   (  NVL (ie.fn_limite_utilizado_dl, 0) + NVL (ie.fn_monto_comprometido_dl, 0)  )  as MontoDisponible_dl ,  "+
				
				 " decode(ie.CS_ACTIVA_LIMITE , 'S', 'Activa' ,'N' , 'Inactiva') as DESCRIPCION_ESTATUS,   "+        
				 "  TO_CHAR (ie.DF_BLOQ_DES, 'dd/mm/yyyy') as FECHA_BLOQ_DESB,   "+
				 " ie.CG_USUARIO_BLOQ_DES as USUARIO_BLOQ_DESB,   "+
				 " ie.CG_DEPENDENCIA_BLOQ_DES as DEPENDENCIA_BLOQ_DESB   "+  
				 
				 									
				" FROM comcat_epo e, comrel_if_epo ie, comrel_producto_epo cpe  "+
				" WHERE ie.ic_epo=e.ic_epo and UPPER(ie.cs_vobo_nafin) = ? "+
				" AND (  ie.in_limite_epo <> ?  or    ie.in_limite_epo_dl <>  ?  ) " +	
				" AND e.cs_habilitado = ? " +
				" AND cpe.ic_epo=e.ic_epo "+
				" AND cpe.ic_epo=ie.ic_epo "+
				" AND cpe.ic_producto_nafin=? "+
				" AND cpe.cs_limite_pyme!=? ");
		conditions.add("S" );
		conditions.add(new Integer(0) );
		conditions.add(new Integer(0) );
		conditions.add("S" );
		conditions.add(new Integer(1) );
		conditions.add("S" );

		strQuery.append( " AND ie.ic_if = ?" );
		conditions.add(new Integer(claveIf) );

		if (!claveEpo.equals("")) {
			strQuery.append( " AND ie.ic_epo = ?" );
			conditions.add(new Integer(claveEpo) );
		}
		if (!montoLimite.equals("")) {
			strQuery.append( " AND ie.in_limite_epo <= ?" );
			conditions.add(new BigDecimal(montoLimite).toPlainString());
		}
		
		if (montoLimite_dl != null && !montoLimite_dl.equals("")) {
			strQuery.append( " AND ie.in_limite_epo_dl <= ?" );
			conditions.add(new BigDecimal(montoLimite_dl).toPlainString());
		}
		
					
		strQuery.append( " ORDER BY e.cg_razon_social" );
		
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
	
		return strQuery.toString();
 	} 		
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";

		if ("CSV".equals(tipo)) {
			linea = "Epo,Monto Limite Moneda Nacional ,Porcentaje de utilizacion Moneda Nacional , Monto Disponible Moneda Nacional, "+
			" Monto Limite D�lar Americano ,Porcentaje de utilizacion D�lar Americano , Monto Disponible D�lar Americano , "+			
			"  Monto Suceptible a Operarse En Factoraje 24 Horas Moneda Nacional , Monto Suceptible a Operarse En Factoraje 24 Horas  D�lar Americano,  Monto Disponible despu�s de Comprometido Moneda Nacional , "+
			" Monto Disponible despu�s de Comprometido D�lar Americano , Fecha Vencimiento L�nea de Cr�dito, Fecha Cambio de Administraci�n , Validar Fechas L�mite ,  " +
			" Estatus ,  Fecha de Bloqueo/Desbloqueo,  Usuario de Bloqueo/Desbloqueo , Dependencia que Bloqueo/Desbloqueo 	\n";
			
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);

				while (rs.next()) {
					String nombreEpo = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					String limite  = (rs.getString("LIMITE") == null) ? "" : rs.getString("LIMITE");
					String porcentaje = (rs.getString("PORCENTAJE") == null) ? "" : rs.getString("PORCENTAJE");
					String disponible  = (rs.getString("DISPONIBLE") == null) ? "" : rs.getString("DISPONIBLE");
					String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
					String montoSuceptible = (rs.getString("MONTO_COMPROMETIDO") == null) ? "" : rs.getString("MONTO_COMPROMETIDO");
					String montoDisponible = (rs.getString("MONTODISPONIBLE") == null) ? "" : rs.getString("MONTODISPONIBLE");
					String fechaLinea = (rs.getString("FECHALINEA") == null) ? "" : rs.getString("FECHALINEA");
					String fechaCambio = (rs.getString("FECHACAMBIO") == null) ? "" : rs.getString("FECHACAMBIO");
					String validaFechaLimite = (rs.getString("CSFECHALIMITE") == null) ? "" : rs.getString("CSFECHALIMITE");
					
					String limite_dl  = (rs.getString("LIMITE_DL") == null) ? "" : rs.getString("LIMITE_DL");
					String porcentaje_dl = (rs.getString("PORCENTAJE_DL") == null) ? "" : rs.getString("PORCENTAJE_DL");
					String disponible_dl  = (rs.getString("DISPONIBLE_DL") == null) ? "" : rs.getString("DISPONIBLE_DL");
					String montoDisponible_dl = (rs.getString("MONTODISPONIBLE_DL") == null) ? "" : rs.getString("MONTODISPONIBLE_DL");
					String montoSuceptible_dl = (rs.getString("MONTO_COMPROMETIDO_DL") == null) ? "" : rs.getString("MONTO_COMPROMETIDO_DL");
					
					String descripcionEstatus = (rs.getString("DESCRIPCION_ESTATUS") == null) ? "" : rs.getString("DESCRIPCION_ESTATUS");
					String fecha_bloq_des = (rs.getString("FECHA_BLOQ_DESB") == null) ? "" : rs.getString("FECHA_BLOQ_DESB");
					String usuario_bloq_des = (rs.getString("USUARIO_BLOQ_DESB") == null) ? "" : rs.getString("USUARIO_BLOQ_DESB");
					String dependencia_bloq_des = (rs.getString("DEPENDENCIA_BLOQ_DESB") == null) ? "" : rs.getString("DEPENDENCIA_BLOQ_DESB");
			
				
					linea = nombreEpo.replace(',',' ')+", " +
							limite.replace(',',' ')+", " +
							porcentaje.replace(',',' ')+"%"+", " +
							"$"+disponible.replace(',',' ')+", " +							
							limite_dl.replace(',',' ')+", " +
							porcentaje_dl.replace(',',' ')+"%"+", " +
							"$"+disponible_dl.replace(',',' ')+", " +						
							"$"+montoSuceptible.replace(',',' ')+", " +
							"$"+montoSuceptible_dl.replace(',',' ')+", " +
							"$"+montoDisponible.replace(',',' ')+", " +
							"$"+montoDisponible_dl.replace(',',' ')+", " +
							fechaLinea.replace(',',' ')+", " +
							fechaCambio.replace(',',' ')+", " +
							validaFechaLimite.replace(',',' ')+", " +   
							
							descripcionEstatus.replace(',',' ')+", " +
							fecha_bloq_des.replace(',',' ')+", " +
							usuario_bloq_des.replace(',',' ')+", " +
							dependencia_bloq_des.replace(',',' ')+"\n";
							
							
					buffer.write(linea);
				}
				buffer.close();
			} catch (Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		} else if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(18, 100);
				pdfDoc.setCell("Epo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Limite Moneda Nacional","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de utilizaci�n  Moneda Nacional","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Disponible Moneda Nacional ","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("Monto Limite D�lar Americano ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de utilizaci�n  D�lar Americano ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Disponible D�lar Americano","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("Monto Suceptible a Operarse En Factoraje 24 Horas Moneda Nacional ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Suceptible a Operarse En Factoraje 24 Horas  D�lar Americano ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Disponible despu�s de Comprometido Moneda Nacional ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Disponible despu�s de Comprometido D�lar Americano","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("Fecha Vencimiento L�nea de Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Cambio de Administraci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Validar Fechas L�mite","celda01",ComunesPDF.CENTER);
			
			  pdfDoc.setCell(" Estatus ", "celda01",ComunesPDF.CENTER);
			  pdfDoc.setCell(" Fecha de Bloqueo/Desbloqueo ", "celda01",ComunesPDF.CENTER);
			  pdfDoc.setCell(" Usuario de Bloqueo/Desbloqueo   ", "celda01",ComunesPDF.CENTER);
			  pdfDoc.setCell( "Dependencia que Bloqueo/Desbloqueo ", "celda01",ComunesPDF.CENTER);
			  
		  
				while (rs.next()) {
					String nombreEpo = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					String limite  = (rs.getString("LIMITE") == null) ? "" : rs.getString("LIMITE");
					String porcentaje = (rs.getString("PORCENTAJE") == null) ? "" : rs.getString("PORCENTAJE");
					String disponible  = (rs.getString("DISPONIBLE") == null) ? "" : rs.getString("DISPONIBLE");
					String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
					String montoSuceptible = (rs.getString("MONTO_COMPROMETIDO") == null) ? "" : rs.getString("MONTO_COMPROMETIDO");
					String montoDisponible = (rs.getString("MONTODISPONIBLE") == null) ? "" : rs.getString("MONTODISPONIBLE");
					String fechaLinea = (rs.getString("FECHALINEA") == null) ? "" : rs.getString("FECHALINEA");
					String fechaCambio = (rs.getString("FECHACAMBIO") == null) ? "" : rs.getString("FECHACAMBIO");
					String validaFechaLimite = (rs.getString("CSFECHALIMITE") == null) ? "" : rs.getString("CSFECHALIMITE");
		
					String limite_dl  = (rs.getString("LIMITE_DL") == null) ? "" : rs.getString("LIMITE_DL");
					String porcentaje_dl = (rs.getString("PORCENTAJE_DL") == null) ? "" : rs.getString("PORCENTAJE_DL");
					String disponible_dl  = (rs.getString("DISPONIBLE_DL") == null) ? "" : rs.getString("DISPONIBLE_DL");
					String montoDisponible_dl = (rs.getString("MONTODISPONIBLE_DL") == null) ? "" : rs.getString("MONTODISPONIBLE_DL");
					String montoSuceptible_dl = (rs.getString("MONTO_COMPROMETIDO_DL") == null) ? "" : rs.getString("MONTO_COMPROMETIDO_DL");
				
					String descripcionEstatus = (rs.getString("DESCRIPCION_ESTATUS") == null) ? "" : rs.getString("DESCRIPCION_ESTATUS");
					String fecha_bloq_des = (rs.getString("FECHA_BLOQ_DESB") == null) ? "" : rs.getString("FECHA_BLOQ_DESB");
					String usuario_bloq_des = (rs.getString("USUARIO_BLOQ_DESB") == null) ? "" : rs.getString("USUARIO_BLOQ_DESB");
					String dependencia_bloq_des = (rs.getString("DEPENDENCIA_BLOQ_DESB") == null) ? "" : rs.getString("DEPENDENCIA_BLOQ_DESB");
												
					pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(limite,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(porcentaje+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(disponible,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(limite_dl,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(porcentaje_dl+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(disponible_dl,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoSuceptible,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoSuceptible_dl,2),"formas",ComunesPDF.RIGHT);
					
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDisponible,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDisponible_dl,2),"formas",ComunesPDF.RIGHT);					
					pdfDoc.setCell(fechaLinea,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(validaFechaLimite,"formas",ComunesPDF.CENTER);

					pdfDoc.setCell(descripcionEstatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_bloq_des,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(usuario_bloq_des,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(dependencia_bloq_des,"formas",ComunesPDF.LEFT);
					
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}

		return nombreArchivo;
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	
	public List getConditions() {
		return conditions;
	}


	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setMontoLimite(String montoLimite) {
		this.montoLimite = montoLimite;
	}


	public String getMontoLimite() {
		return montoLimite;
	}


	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}


	public String getClaveIf() {
		return claveIf;
	}

	public String getMontoLimite_dl() {
		return montoLimite_dl;
	}

	public void setMontoLimite_dl(String montoLimite_dl) {
		this.montoLimite_dl = montoLimite_dl;
	}


}