package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsDoctosBenefDE implements IQueryGeneratorPS, IQueryGeneratorRegExtJS {	

	private int numList = 1;
	
	public ConsDoctosBenefDE() {}

	public String getAggregateCalculationQuery(HttpServletRequest request){
		String no_docto = (request.getParameter("no_docto")==null)?"":request.getParameter("no_docto");
		String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo").trim();
		String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme").trim();
		String ic_estatus_docto = (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto").trim();
		String df_fecha_docto_de = (request.getParameter("df_fecha_docto_de")==null)?"":request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a = (request.getParameter("df_fecha_docto_a")==null)?"":request.getParameter("df_fecha_docto_a");
		String df_fecha_venc_de = (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a = (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
		String fn_monto_de = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String ic_beneficiario = (request.getParameter("ic_beneficiario")==null)?"":request.getParameter("ic_beneficiario").trim();
		String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");

		StringBuffer strSQL = new StringBuffer();
		StringBuffer condicion = new StringBuffer();

		if (!ic_estatus_docto.equals("")) {
			condicion.append(" AND d.ic_estatus_docto = ?  ");
		} else {
			condicion.append(" AND d.ic_estatus_docto in (2,9,10)  ");
		}
		if (!no_docto.equals("")) {
			condicion.append(" AND d.ig_numero_docto = ?  ");
		}
		if (!ic_epo.equals("")) {
			condicion.append(" AND d.ic_epo = ?  ");
		}
		if (!ic_pyme.equals("")) {
			condicion.append(" AND d.ic_pyme = ?  ");
		}
		if (!df_fecha_docto_de.equals("")) {
			condicion.append(" AND d.df_fecha_docto >= TO_DATE(?,'dd/mm/yyyy') ");
		}
		if (!df_fecha_docto_a.equals("")) {
			condicion.append(" AND d.df_fecha_docto < TO_DATE(?,'dd/mm/yyyy') + 1 ");
		}
		if (!df_fecha_venc_de.equals("")) {
			condicion.append(" AND d.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') ");
		}
		if (!df_fecha_venc_a.equals("")) {
			condicion.append(" AND d.df_fecha_venc < TO_DATE(?,'dd/mm/yyyy') + 1 ");
		}
		if (!fn_monto_de.equals("")) {
			condicion.append(" AND d.fn_monto >= ? ");
		}
		if (!fn_monto_a.equals("")) {
			condicion.append(" AND d.fn_monto <= ? ");
		}
		if (!ic_beneficiario.equals("")) {
			condicion.append(" AND d.ic_beneficiario = ?  ");
		}
		if (!ic_moneda.equals("")) {
			condicion.append(" AND d.ic_moneda = ?  ");
		}

		strSQL.append (
				" SELECT /*+ index (d IN_COM_DOCUMENTO_04_NUK) */ " +
				" 	COUNT(ic_documento) numRegistros, " +
				" 	SUM(fn_monto) AS fn_monto, " +
				" 	SUM(fn_monto_dscto) AS fn_monto_dscto, " +
				" 	SUM(CASE WHEN d.ic_estatus_docto = 2 THEN d.fn_monto ELSE 0 END) " +
				" 		AS montoNegociable, " +
				" 	M.cd_nombre AS moneda, D.ic_moneda " +
				" FROM com_documento D,comcat_moneda M " +
				" WHERE  " +
				" 	d.ic_moneda = m.ic_moneda " +
				condicion +
				" 	AND d.cs_dscto_especial = 'D' " +
				" GROUP BY D.IC_MONEDA, M.CD_NOMBRE "); 

		System.out.println(strSQL.toString());
		return strSQL.toString();
	}

	public int getNumList(HttpServletRequest request){
		return numList;	
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
		StringBuffer strSQL  = new StringBuffer();
		StringBuffer lstdoctos  = new StringBuffer();

		for (Iterator it = ids.iterator(); it.hasNext();){
			it.next();
			if(lstdoctos.length() > 0) {
				lstdoctos.append(",");
			}
			lstdoctos.append("?");
		}

		strSQL.append(
				" SELECT " +
				" 	d.ic_documento, " +
				" 	e.cg_razon_social as nombreEpo, " +
				" 	p.cg_razon_social as nombrePyme, d.ig_numero_docto,  " +
				" 	to_char(df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, " +
				" 	to_char(df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, " +
				" 	m.cd_nombre, 'Distribuido' as tipoFactoraje, " +
				" 	d.fn_monto,  100 as fn_porc_anticipo, d.fn_monto as fn_monto_dscto, " +
				" 	ed.cd_descripcion, d.ct_referencia, " +
				" 	i.cg_razon_social as beneficiario, " +
				" 	d.fn_porc_beneficiario,  " +
				" 	d.fn_monto*(d.fn_porc_beneficiario/100) as  fn_importe_recibir_benef, " +
				" 	d.cs_detalle, d.cs_cambio_importe, d.ic_epo " +
				" FROM com_documento d, comcat_pyme p, comcat_if i,  " +
				" 	comcat_estatus_docto ed, comcat_epo e,  " +
				" 	comcat_moneda m " +
				" WHERE " +
				" 	d.ic_pyme = p.ic_pyme " +
				" 	AND d.ic_beneficiario = i.ic_if " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto  " +
				" 	AND d.ic_documento in (" +	lstdoctos + ")");

		System.out.println(strSQL.toString());

		return strSQL.toString();
	}

// Devuelve el query de llaves primarias
	public String getDocumentQuery(HttpServletRequest request){

		String no_docto = (request.getParameter("no_docto")==null)?"":request.getParameter("no_docto");
		String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo").trim();
		String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme").trim();
		String ic_estatus_docto = (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto").trim();
		String df_fecha_docto_de = (request.getParameter("df_fecha_docto_de")==null)?"":request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a = (request.getParameter("df_fecha_docto_a")==null)?"":request.getParameter("df_fecha_docto_a");
		String df_fecha_venc_de = (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a = (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
		String fn_monto_de = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String ic_beneficiario = (request.getParameter("ic_beneficiario")==null)?"":request.getParameter("ic_beneficiario").trim();
		String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");

		StringBuffer strSQL = new StringBuffer();
		StringBuffer condicion = new StringBuffer();

		if (!ic_estatus_docto.equals("")) {
			condicion.append(" AND d.ic_estatus_docto = ?  ");
		} else {
			condicion.append(" AND d.ic_estatus_docto in (2,9,10)  ");
		}
		if (!no_docto.equals("")) {
			condicion.append(" AND d.ig_numero_docto = ?  ");
		}
		if (!ic_epo.equals("")) {
			condicion.append(" AND d.ic_epo = ?  ");
		}
		if (!ic_pyme.equals("")) {
			condicion.append(" AND d.ic_pyme = ?  ");
		}
		if (!df_fecha_docto_de.equals("")) {
			condicion.append(" AND d.df_fecha_docto >= TO_DATE(?,'dd/mm/yyyy') ");
		}
		if (!df_fecha_docto_a.equals("")) {
			condicion.append(" AND d.df_fecha_docto < TO_DATE(?,'dd/mm/yyyy') + 1 ");
		}
		if (!df_fecha_venc_de.equals("")) {
			condicion.append(" AND d.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') ");
		}
		if (!df_fecha_venc_a.equals("")) {
			condicion.append(" AND d.df_fecha_venc < TO_DATE(?,'dd/mm/yyyy') + 1 ");
		}
		if (!fn_monto_de.equals("")) {
			condicion.append(" AND d.fn_monto >= ? ");
		}
		if (!fn_monto_a.equals("")) {
			condicion.append(" AND d.fn_monto <= ? ");
		}
		if (!ic_beneficiario.equals("")) {
			condicion.append(" AND d.ic_beneficiario = ?  ");
		}
		if (!ic_moneda.equals("")) {
			condicion.append(" AND d.ic_moneda = ?  ");
		}

		strSQL.append(
				" SELECT /*+ index (d IN_COM_DOCUMENTO_04_NUK) */ " +
				" 	ic_documento  " +
				" 	,'ConsDoctosBenefDE::getDocumentQuery'"+
				" FROM com_documento d " +
				" WHERE " + 
				" 	d.cs_dscto_especial = 'D' " +
				condicion );


		System.out.println(strSQL.toString());


		return strSQL.toString();
	}

	public String getDocumentQueryFile(HttpServletRequest request) {
		String no_docto = (request.getParameter("no_docto")==null)?"":request.getParameter("no_docto");
		String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo").trim();
		String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme").trim();
		String ic_estatus_docto = (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto").trim();
		String df_fecha_docto_de = (request.getParameter("df_fecha_docto_de")==null)?"":request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a = (request.getParameter("df_fecha_docto_a")==null)?"":request.getParameter("df_fecha_docto_a");
		String df_fecha_venc_de = (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a = (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
		String fn_monto_de = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String ic_beneficiario = (request.getParameter("ic_beneficiario")==null)?"":request.getParameter("ic_beneficiario").trim();
		String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");

		StringBuffer strSQL = new StringBuffer();
		StringBuffer condicion = new StringBuffer();

		if (!ic_estatus_docto.equals("")) {
			condicion.append(" AND d.ic_estatus_docto = ?  ");
		} else {
			condicion.append(" AND d.ic_estatus_docto in (2,9,10)  ");
		}
		if (!no_docto.equals("")) {
			condicion.append(" AND d.ig_numero_docto = ?  ");
		}
		if (!ic_epo.equals("")) {
			condicion.append(" AND d.ic_epo = ?  ");
		}
		if (!ic_pyme.equals("")) {
			condicion.append(" AND d.ic_pyme = ?  ");
		}
		if (!df_fecha_docto_de.equals("")) {
			condicion.append(" AND d.df_fecha_docto >= TO_DATE(?,'dd/mm/yyyy') ");
		}
		if (!df_fecha_docto_a.equals("")) {
			condicion.append(" AND d.df_fecha_docto < TO_DATE(?,'dd/mm/yyyy') + 1 ");
		}
		if (!df_fecha_venc_de.equals("")) {
			condicion.append(" AND d.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') ");
		}
		if (!df_fecha_venc_a.equals("")) {
			condicion.append(" AND d.df_fecha_venc < TO_DATE(?,'dd/mm/yyyy') + 1 ");
		}
		if (!fn_monto_de.equals("")) {
			condicion.append(" AND d.fn_monto >= ? ");
		}
		if (!fn_monto_a.equals("")) {
			condicion.append(" AND d.fn_monto <= ? ");
		}
		if (!ic_beneficiario.equals("")) {
			condicion.append(" AND d.ic_beneficiario = ?  ");
		}
		if (!ic_moneda.equals("")) {
			condicion.append(" AND d.ic_moneda = ?  ");
		}

		strSQL.append(
				" SELECT /*+ index (d IN_COM_DOCUMENTO_04_NUK) use_nl (d e) */ " +
				" 	e.cg_razon_social as nombreEpo, " +
				" 	p.cg_razon_social as nombrePyme, d.ig_numero_docto, " +
				" 	to_char(df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, " +
				" 	to_char(df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, " +
				" 	m.cd_nombre, 'Distribuido' as tipoFactoraje, " +
				" 	d.fn_monto, 100 as fn_porc_anticipo, d.fn_monto as fn_monto_dscto, " +
				" 	ed.cd_descripcion, d.ct_referencia, " +
				" 	i.cg_razon_social as beneficiario, " +
				" 	d.fn_porc_beneficiario,  " +
				" 	d.fn_monto*(d.fn_porc_beneficiario/100) as  fn_importe_recibir_benef " +
				" FROM com_documento d, comcat_if i,  comcat_pyme p, " +
				" 	comcat_estatus_docto ed, comcat_epo e,  " +
				" 	comcat_moneda m " +
				" WHERE " +
				" 	d.ic_pyme = p.ic_pyme " +
				" 	AND d.ic_beneficiario = i.ic_if " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto  " + condicion );

		System.out.println(strSQL.toString());
		
		return strSQL.toString();
	}

	public ArrayList getConditions(HttpServletRequest request){

		ArrayList condiciones = new ArrayList();
		String no_docto = (request.getParameter("no_docto")==null)?"":request.getParameter("no_docto");
		String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo").trim();
		String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme").trim();
		String ic_estatus_docto = (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto").trim();
		String df_fecha_docto_de = (request.getParameter("df_fecha_docto_de")==null)?"":request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a = (request.getParameter("df_fecha_docto_a")==null)?"":request.getParameter("df_fecha_docto_a");
		String df_fecha_venc_de = (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a = (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
		String fn_monto_de = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String ic_beneficiario = (request.getParameter("ic_beneficiario")==null)?"":request.getParameter("ic_beneficiario").trim();
		String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");

		StringBuffer strSQL = new StringBuffer();

		if (!ic_estatus_docto.equals("")) {
			condiciones.add(ic_estatus_docto);
		} 
		if (!no_docto.equals("")) {
			condiciones.add(no_docto);
		}
		if (!ic_epo.equals("")) {
			condiciones.add(ic_epo);
		}
		if (!ic_pyme.equals("")) {
			condiciones.add(ic_pyme);
		}
		if (!df_fecha_docto_de.equals("")) {
			condiciones.add(df_fecha_docto_de);
		}
		if (!df_fecha_docto_a.equals("")) {
			condiciones.add(df_fecha_docto_a);
		}
		if (!df_fecha_venc_de.equals("")) {
			condiciones.add(df_fecha_venc_de);
		}
		if (!df_fecha_venc_a.equals("")) {
			condiciones.add(df_fecha_venc_a);
		}
		if (!fn_monto_de.equals("")) {
			condiciones.add(fn_monto_de);
		}
		if (!fn_monto_a.equals("")) {
			condiciones.add(fn_monto_a);
		}
		if (!ic_beneficiario.equals("")) {
			condiciones.add(ic_beneficiario);
		}
		if (!ic_moneda.equals("")) {
			condiciones.add(ic_moneda);
		}
		System.out.println("COndiciones:" + condiciones);
		return condiciones;
	}
	
	/***************************************************************************
	 * 									MIGRACION											   *
	 *                         Gustavo Arellano											*
	 **************************************************************************/
	private List conditions;
	private StringBuffer strQuery;
	private String no_docto;
	private String ic_epo;
	private String ic_pyme;
	private String ic_estatus_docto;
	private String df_fecha_docto_de;
	private String df_fecha_docto_a;
	private String df_fecha_venc_de;
	private String df_fecha_venc_a;
	private String fn_monto_de;
	private String fn_monto_a;
	private String ic_beneficiario;
	private String ic_moneda;  
		
	private static final Log log = ServiceLocator.getInstance().getLog(ConsDoctosBenefDE.class);
	
	public String getAggregateCalculationQuery() {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		log.debug("getAggregateCalculationQuery");

		strQuery.append (
				" SELECT /*+ index (d IN_COM_DOCUMENTO_04_NUK) */ " +
				" 	COUNT(ic_documento) numRegistros, " +
				" 	SUM(fn_monto) AS fn_monto, " +
				" 	SUM(fn_monto_dscto) AS fn_monto_dscto, " +
				" 	SUM(CASE WHEN d.ic_estatus_docto = 2 THEN d.fn_monto ELSE 0 END) " +
				" 		AS montoNegociable, " +
				" 	M.cd_nombre AS moneda, D.ic_moneda " +
				" FROM com_documento D,comcat_moneda M " +
				" WHERE  " +
				" 	d.ic_moneda = m.ic_moneda ");
				
		if (!ic_estatus_docto.equals("")) {
			strQuery.append(" AND d.ic_estatus_docto = ?  ");
			conditions.add(ic_estatus_docto);
		} else {
			strQuery.append(" AND d.ic_estatus_docto in (2,9,10)  ");
		}
		if (!no_docto.equals("")) {
			strQuery.append(" AND d.ig_numero_docto = ?  ");
			conditions.add(no_docto);
		}   
		if (!ic_epo.equals("")) {
			strQuery.append(" AND d.ic_epo = ?  ");
			conditions.add(ic_epo);
		}
		if (!ic_pyme.equals("")) {
			strQuery.append(" AND d.ic_pyme = ?  ");
			conditions.add(ic_pyme);
		}
		if (!df_fecha_docto_de.equals("")) {
			strQuery.append(" AND d.df_fecha_docto >= TO_DATE(?,'dd/mm/yyyy') ");
			conditions.add(df_fecha_docto_de);
		}
		if (!df_fecha_docto_a.equals("")) {
			strQuery.append(" AND d.df_fecha_docto < TO_DATE(?,'dd/mm/yyyy') + 1 ");
			conditions.add(df_fecha_docto_a);
		}
		if (!df_fecha_venc_de.equals("")) {
			strQuery.append(" AND d.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') ");
			conditions.add(df_fecha_venc_de);
		}
		if (!df_fecha_venc_a.equals("")) {
			strQuery.append(" AND d.df_fecha_venc < TO_DATE(?,'dd/mm/yyyy') + 1 ");
			conditions.add(df_fecha_venc_a);
		}
		if (!fn_monto_de.equals("")) {
			strQuery.append(" AND d.fn_monto >= ? ");
			conditions.add(fn_monto_de);
		}
		if (!fn_monto_a.equals("")) {
			strQuery.append(" AND d.fn_monto <= ? ");
			conditions.add(fn_monto_a);
		}
		if (!ic_beneficiario.equals("")) {
			strQuery.append(" AND d.ic_beneficiario = ?  ");
			conditions.add(ic_beneficiario);
		}
      System.err.println("ic_beneficiario: " + ic_beneficiario);
		if (!ic_moneda.equals("")) {
			strQuery.append(" AND d.ic_moneda = ?  ");   
			conditions.add(ic_moneda);
		}
		
		strQuery.append(
				" 	AND d.cs_dscto_especial = 'D' " + 
				" GROUP BY D.IC_MONEDA, M.CD_NOMBRE ");
				
		System.out.println("getAggregateCalculationQuery:::: " + strQuery.toString());
				
		return strQuery.toString();
	}
	
	public String getDocumentQuery() {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		log.debug("getDocumentQuery");

		strQuery.append(
				" SELECT /*+ index (d IN_COM_DOCUMENTO_04_NUK) */ " +
				" 	ic_documento  " +
				" 	,'ConsDoctosBenefDE::getDocumentQuery'"+
				" FROM com_documento " +
				" WHERE " + 
				" 	cs_dscto_especial = 'D' ");
		
		if (!ic_estatus_docto.equals("")) {
			strQuery.append(" AND ic_estatus_docto = ?  ");
			conditions.add(ic_estatus_docto);
		} else {
			strQuery.append(" AND ic_estatus_docto in (2,9,10)  ");
		}
		if (!no_docto.equals("")) {
			strQuery.append(" AND ig_numero_docto = ?  ");
			conditions.add(no_docto);
		}
		if (!ic_epo.equals("")) {
			strQuery.append(" AND ic_epo = ?  ");
			conditions.add(ic_epo);
		}
		if (!ic_pyme.equals("")) {
			strQuery.append(" AND ic_pyme = ?  ");
			conditions.add(ic_pyme);
		}
		if (!df_fecha_docto_de.equals("")) {
			strQuery.append(" AND df_fecha_docto >= TO_DATE(?,'dd/mm/yyyy') ");
			conditions.add(df_fecha_docto_de);
		}
		if (!df_fecha_docto_a.equals("")) {
			strQuery.append(" AND df_fecha_docto < TO_DATE(?,'dd/mm/yyyy') + 1 ");
			conditions.add(df_fecha_docto_a);
		}
		if (!df_fecha_venc_de.equals("")) {
			strQuery.append(" AND df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') ");
			conditions.add(df_fecha_venc_de);
		}
		if (!df_fecha_venc_a.equals("")) {
			strQuery.append(" AND df_fecha_venc < TO_DATE(?,'dd/mm/yyyy') + 1 ");
			conditions.add(df_fecha_venc_a);
		}
		if (!fn_monto_de.equals("")) {
			strQuery.append(" AND fn_monto >= ? ");
			conditions.add(fn_monto_de);
		}
		if (!fn_monto_a.equals("")) {
			strQuery.append(" AND fn_monto <= ? ");
			conditions.add(fn_monto_a);
		}
		if (!ic_beneficiario.equals("")) {
			strQuery.append(" AND ic_beneficiario = ?  ");
			conditions.add(ic_beneficiario);
		}
		if (!ic_moneda.equals("")) {
			strQuery.append(" AND ic_moneda = ?  ");
			conditions.add(ic_moneda);
		}
		log.debug("getDocumentQuery " + strQuery.toString());
		System.out.println(strQuery.toString());

		return strQuery.toString();
	}  
	public String getDocumentQueryFile(){
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		log.debug("getDocumentQueryFile");
		
		strQuery.append(
				" SELECT /*+ index (d IN_COM_DOCUMENTO_04_NUK) use_nl (d e) */ " +
				" 	e.cg_razon_social as nombreEpo, " +
				" 	p.cg_razon_social as nombrePyme, d.ig_numero_docto, " +
				" 	to_char(df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, " +
				" 	to_char(df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, " +
				" 	m.cd_nombre, 'Distribuido' as tipoFactoraje, " +
				" 	d.fn_monto, 100 as fn_porc_anticipo, d.fn_monto as fn_monto_dscto, " +
				" 	ed.cd_descripcion, d.ct_referencia, " +
				" 	i.cg_razon_social as beneficiario, " +
				" 	d.fn_porc_beneficiario,  " +
				" 	d.fn_monto*(d.fn_porc_beneficiario/100) as  fn_importe_recibir_benef " +
				" FROM com_documento d, comcat_if i,  comcat_pyme p, " +
				" 	comcat_estatus_docto ed, comcat_epo e,  " +
				" 	comcat_moneda m " +
				" WHERE " +
				" 	d.ic_pyme = p.ic_pyme " +
				" 	AND d.ic_beneficiario = i.ic_if " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto  ");
				
		if (!ic_estatus_docto.equals("")) {
			strQuery.append(" AND d.ic_estatus_docto = ?  ");
			conditions.add(ic_estatus_docto);
		} else {
			strQuery.append(" AND d.ic_estatus_docto in (2,9,10)  ");
		}
		if (!no_docto.equals("")) {
			strQuery.append(" AND d.ig_numero_docto = ?  ");
			conditions.add(no_docto);
		}
		if (!ic_epo.equals("")) {
			strQuery.append(" AND d.ic_epo = ?  ");
			conditions.add(ic_epo);
		}
		if (!ic_pyme.equals("")) {
			strQuery.append(" AND d.ic_pyme = ?  ");
			conditions.add(ic_pyme);
		}
		if (!df_fecha_docto_de.equals("")) {
			strQuery.append(" AND d.df_fecha_docto >= TO_DATE(?,'dd/mm/yyyy') ");
			conditions.add(df_fecha_docto_de);
		}
		if (!df_fecha_docto_a.equals("")) {
			strQuery.append(" AND d.df_fecha_docto < TO_DATE(?,'dd/mm/yyyy') + 1 ");
			conditions.add(df_fecha_docto_a);
		}
		if (!df_fecha_venc_de.equals("")) {
			strQuery.append(" AND d.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') ");
			conditions.add(df_fecha_venc_de);
		}
		if (!df_fecha_venc_a.equals("")) {
			strQuery.append(" AND d.df_fecha_venc < TO_DATE(?,'dd/mm/yyyy') + 1 ");
			conditions.add(df_fecha_venc_a);
		}
		if (!fn_monto_de.equals("")) {
			strQuery.append(" AND d.fn_monto >= ? ");
			conditions.add(fn_monto_de);
		}
		if (!fn_monto_a.equals("")) {
			strQuery.append(" AND d.fn_monto <= ? ");
			conditions.add(fn_monto_a);
		}
		if (!ic_beneficiario.equals("")) {
			strQuery.append(" AND d.ic_beneficiario = ?  ");
			conditions.add(ic_beneficiario);
		}
		if (!ic_moneda.equals("")) {
			strQuery.append(" AND d.ic_moneda = ?  ");
			conditions.add(ic_moneda);
		}
		
		System.out.println("getDocumentQueryFile::."  + strQuery.toString());
		
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		StringBuffer clavesCombinaciones = new StringBuffer("");
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		log.debug("getDocumentSummaryQueryForIds ");
		
		for (int i = 0; i < pageIds.size(); i++) {
			List lItem = (ArrayList)pageIds.get(i);
			clavesCombinaciones.append("'"+lItem.get(0).toString()+"'"+",");
		}
		
		clavesCombinaciones.deleteCharAt(clavesCombinaciones.length()-1);
		/*
		for (Iterator it = pageIds.iterator(); it.hasNext();){
			it.next();
			if(lstdoctos.length() > 0) {
				lstdoctos.append(",");
			}
			lstdoctos.append("?");
		}
*/
		strQuery.append(
				" SELECT " +
				" 	d.ic_documento, " +
				" 	e.cg_razon_social as nombreEpo, " +
				" 	p.cg_razon_social as nombrePyme, d.ig_numero_docto,  " +
				" 	to_char(df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto, " +
				" 	to_char(df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc, " +
				" 	m.cd_nombre, 'Distribuido' as tipoFactoraje, " +
				" 	d.fn_monto,  100 as fn_porc_anticipo, d.fn_monto as fn_monto_dscto, " +
				" 	ed.cd_descripcion, d.ct_referencia, " +
				" 	i.cg_razon_social as beneficiario, " +
				" 	d.fn_porc_beneficiario,  " +
				" 	d.fn_monto*(d.fn_porc_beneficiario/100) as  fn_importe_recibir_benef, " +
				" 	d.cs_detalle, d.cs_cambio_importe, d.ic_epo " +
				" FROM com_documento d, comcat_pyme p, comcat_if i,  " +
				" 	comcat_estatus_docto ed, comcat_epo e,  " +
				" 	comcat_moneda m " +
				" WHERE " +
				" 	d.ic_pyme = p.ic_pyme " +
				" 	AND d.ic_beneficiario = i.ic_if " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto  " +
				" 	AND d.ic_documento in (" +	clavesCombinaciones + ")");

		System.out.println(strQuery.toString());
		
		return strQuery.toString();
	}
	
		/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)  {
		OutputStreamWriter writer 		= null;
		BufferedWriter buffer 			= null;
		StringBuffer contenidoArchivo	= new StringBuffer();
		CreaArchivo archivo 				= new CreaArchivo();
		String nombreArchivo 			= "";
		String linea 						= "";
		
		if ("XLS".equals(tipo)) {  
			try {
				int registros = 0;
				
				System.out.println("\n\n\n@@@@@@ XLS 1 @@@@@@\n\n\n");
				
				while(rs.next()){
					if(registros==0) {
						contenidoArchivo.append("Nombre de la EPO,Nombre del Proveedor" + 
							",Numero de Documento,Fecha de Emision,Fecha de Vencimiento,Moneda,Tipo Factoraje,Monto,Porcentaje de Descuento" +
							",Monto a Descontar,Estatus,Referencia,Beneficiario,% Beneficiario,Importe a Recibir Beneficiario");
					}
					registros++;				
					
					String nomEpo 				= rs.getString(1)==null?"":rs.getString(1);
					String nomPro 				= rs.getString(2)==null?"":rs.getString(2);
					String numDoc 				= rs.getString(3)==null?"":rs.getString(3);
					String fecEmi 				= rs.getString(4)==null?"":rs.getString(4);
					String fecVen 				= rs.getString(5)==null?"":rs.getString(5);
					String moneda 				= rs.getString(6)==null?"":rs.getString(6);
					String tFacto 				= rs.getString(7)==null?"":rs.getString(7);
					String monto 				= rs.getString(8)==null?"":rs.getString(8);
					String porDes 				= rs.getString(9)==null?"":rs.getString(9);
					String monDes 				= rs.getString(10)==null?"":rs.getString(10);
					String status 				= rs.getString(11)==null?"":rs.getString(11);
					String refere 				= rs.getString(12)==null?"":rs.getString(12);
					String benefi 				= rs.getString("beneficiario")==null?"":rs.getString("beneficiario");
					String porBen 				= rs.getString("fn_porc_beneficiario")==null?"":rs.getString("fn_porc_beneficiario");
					String impRBe 				= rs.getString("fn_importe_recibir_benef")==null?"":rs.getString("fn_importe_recibir_benef");
						System.out.println("\n\n\n ENTRA AQUIIIII ::::::: "); 
					contenidoArchivo.append("\n"+
						nomEpo.replaceAll(",","") +","+
						nomPro.replaceAll(",","") +","+
						numDoc.replaceAll(",","") +","+
						fecEmi.replaceAll(",","") +","+
						fecVen.replaceAll(",","") +","+
						moneda.replaceAll(",","") +","+
						tFacto.replaceAll(",","") +","+
						monto.replaceAll(",","")  +","+
						porDes.replaceAll(",","") +","+
						monDes.replaceAll(",","") +","+
						status.replaceAll(",","") +","+
						refere.replaceAll(",","") +","+
						benefi.replaceAll(",","") +","+
						porBen.replaceAll(",","") +","+
						impRBe.replaceAll(",","") +","); 
				}
						System.out.println("\n\n\n ENTRA AQUIIIII ::::::: ");
				if(registros >= 1){
					if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
						nombreArchivo="ERROR";
					}else{
						nombreArchivo = archivo.nombre;
					}
				}	
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}else if("PDF".equals(tipo)) { 
			
			System.out.println("\n\n\n@@@@@@ PDF ENTRO .:: 1 ::.@@@@@\n\n\n");
			
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[]	=	{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual	=	fechaActual.substring(0,2);
				String mesActual	=	meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual	=	fechaActual.substring(6,10);
				String horaActual	=	new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
				horaActual, "formas", ComunesPDF.RIGHT);    
				pdfDoc.setTable(15, 100);              
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Numero de Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Emision","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
								
				while (rs.next()) {
					String nomEpo 				= rs.getString(1)==null?"":rs.getString(1);
					String nomPro 				= rs.getString(2)==null?"":rs.getString(2);
					String numDoc 				= rs.getString(3)==null?"":rs.getString(3);
					String fecEmi 				= rs.getString(4)==null?"":rs.getString(4);
					String fecVen 				= rs.getString(5)==null?"":rs.getString(5);
					String moneda 				= rs.getString(6)==null?"":rs.getString(6);
					String tFacto 				= rs.getString(7)==null?"":rs.getString(7);
					String monto 				= rs.getString(8)==null?"":rs.getString(8);
					String porDes 				= rs.getString(9)==null?"":rs.getString(9);
					String monDes 				= rs.getString(10)==null?"":rs.getString(10);
					String status 				= rs.getString(11)==null?"":rs.getString(11);
					String refere 				= rs.getString(12)==null?"":rs.getString(12);
					String benefi 				= rs.getString("beneficiario")==null?"":rs.getString("beneficiario");
					String porBen 				= rs.getString("fn_porc_beneficiario")==null?"":rs.getString("fn_porc_beneficiario");
					String impRBe 				= rs.getString("fn_importe_recibir_benef")==null?"":rs.getString("fn_importe_recibir_benef");
					
					pdfDoc.setCell(nomEpo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nomPro,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numDoc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecEmi,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecVen,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tFacto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(monto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(porDes,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(monDes,"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(status,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(refere,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(benefi,"formas",ComunesPDF.CENTER);  
					pdfDoc.setCell(porBen,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(impRBe,"formas",ComunesPDF.CENTER);									
				}	
				pdfDoc.addTable(); 
				pdfDoc.endDocument();
				
			}catch (Throwable e) {
				throw new AppException("Error al generar el archivo",e);
			}finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}
		return nombreArchivo;
	}
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo 	= new CreaArchivo();
		
		if ("PDF".equals(tipo)) {
			
			System.out.println("\n\n\n@@@@@@ PDF ENTRO .:: 2 ::.@@@@@\n\n\n");
			
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(15, 100);              
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Numero de Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Emision","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
								
				while (rs.next()) {
					String nomEpo 				= rs.getString(2)==null?"":rs.getString(2);
					String nomPro 				= rs.getString(3)==null?"":rs.getString(3);
					String numDoc 				= rs.getString(4)==null?"":rs.getString(4);
					String fecEmi 				= rs.getString(5)==null?"":rs.getString(5);
					String fecVen 				= rs.getString(6)==null?"":rs.getString(6);
					String moneda 				= rs.getString(7)==null?"":rs.getString(7);
					String tFacto 				= rs.getString(8)==null?"":rs.getString(8);
					String monto 				= rs.getString(9)==null?"":rs.getString(9);
					String porDes 				= rs.getString(10)==null?"":rs.getString(10);
					String monDes 				= rs.getString(11)==null?"":rs.getString(11);
					String status 				= rs.getString(12)==null?"":rs.getString(12);
					String refere 				= rs.getString(13)==null?"":rs.getString(13);
					String benefi 				= rs.getString("beneficiario")==null?"":rs.getString("beneficiario");
					String porBen 				= rs.getString("fn_porc_beneficiario")==null?"":rs.getString("fn_porc_beneficiario");
					String impRBe 				= rs.getString("fn_importe_recibir_benef")==null?"":rs.getString("fn_importe_recibir_benef");
					
					pdfDoc.setCell(nomEpo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nomPro,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numDoc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecEmi,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecVen,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tFacto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(monto, 2, false),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(porDes + " %","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(monDes, 2, false),"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(status,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(refere,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(benefi,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(porBen,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(impRBe, 2, false),"formas",ComunesPDF.CENTER);									
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
		}		
		return nombreArchivo;
	}
	
	public List getConditions()	{
		return conditions;
	}


	public void setNo_docto(String no_docto) {
		this.no_docto = no_docto;
	}


	public String getNo_docto() {
		return no_docto;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}


	public String getIc_pyme() {
		return ic_pyme;
	}


	public void setIc_estatus_docto(String ic_estatus_docto) {
		this.ic_estatus_docto = ic_estatus_docto;
	}


	public String getIc_estatus_docto() {
		return ic_estatus_docto;
	}


	public void setDf_fecha_docto_de(String df_fecha_docto_de) {
		this.df_fecha_docto_de = df_fecha_docto_de;
	}


	public String getDf_fecha_docto_de() {
		return df_fecha_docto_de;
	}


	public void setDf_fecha_docto_a(String df_fecha_docto_a) {
		this.df_fecha_docto_a = df_fecha_docto_a;
	}


	public String getDf_fecha_docto_a() {
		return df_fecha_docto_a;
	}


	public void setDf_fecha_venc_de(String df_fecha_venc_de) {
		this.df_fecha_venc_de = df_fecha_venc_de;
	}


	public String getDf_fecha_venc_de() {
		return df_fecha_venc_de;
	}


	public void setDf_fecha_venc_a(String df_fecha_venc_a) {
		this.df_fecha_venc_a = df_fecha_venc_a;
	}


	public String getDf_fecha_venc_a() {
		return df_fecha_venc_a;
	}


	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}


	public String getFn_monto_de() {
		return fn_monto_de;
	}


	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}


	public String getFn_monto_a() {
		return fn_monto_a;
	}


	public void setIc_beneficiario(String ic_beneficiario) {
		this.ic_beneficiario = ic_beneficiario;
	}


	public String getIc_beneficiario() {
		return ic_beneficiario;
	}


	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}


	public String getIc_moneda() {
		return ic_moneda;
	}
}