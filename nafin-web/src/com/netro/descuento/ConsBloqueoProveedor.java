package com.netro.descuento;

import com.netro.pdf.ComunesPDF;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.util.List;
import java.util.HashMap;
//import com.netro.parametrosgrales.*;
import java.sql.*;
import javax.naming.*;
import net.sf.json.JSONArray;

public class ConsBloqueoProveedor implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConsBloqueoProveedor.class);
	private List   conditions;
	private List   listaGrid;
	private String nafinElectronico;
	private String rfc;
	private String icEpo;
	private String usuario;
	private String informacion;
	private String icIf;
	private String icPyme;

	public ConsBloqueoProveedor(){}

	/**
	 * Obtiene las llaves primarias,
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){
		return null;
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */	
	public String getDocumentSummaryQueryForIds(List ids){
		return null;
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */		
	public String getAggregateCalculationQuery(){
		return null;
	}

	/**
	 * Realiza la consulta para generar los archivos sin usar paginaci�n
	 * @return sentencia sql 
	 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile(E)");

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		conditions                = new ArrayList();

		if(this.informacion.equals("CONSULTA_DATA")){

			qrySentencia.append("SELECT   I.IC_IF, I.IC_EPO, C.IC_PYME, E.CG_RAZON_SOCIAL AS EPO,\n" );
			qrySentencia.append("         P.CG_RAZON_SOCIAL AS PYME, P.CG_RFC AS RFC, P.CG_EMAIL AS EMAIL,\n" );
			qrySentencia.append("         N.IC_NAFIN_ELECTRONICO AS NE,\n" );
			qrySentencia.append("         (SELECT CS_BLOQUEO\n" );
			qrySentencia.append("            FROM COMREL_BLOQUEO_PYME_X_IF_EPO\n" );
			qrySentencia.append("           WHERE IC_IF = I.IC_IF\n" );
			qrySentencia.append("             AND IC_EPO = C.IC_EPO\n" );
			qrySentencia.append("             AND IC_PYME = C.IC_PYME\n" );
			qrySentencia.append("             AND IC_NAFIN_ELECTRONICO = N.IC_NAFIN_ELECTRONICO)\n" );
			qrySentencia.append("                                                               AS BLOQUEO_ANT,\n" );
			qrySentencia.append("         (SELECT CG_CAUSA_BLOQUEO\n" );
			qrySentencia.append("            FROM COMREL_BLOQUEO_PYME_X_IF_EPO\n" );
			qrySentencia.append("           WHERE IC_IF = I.IC_IF\n" );
			qrySentencia.append("             AND IC_EPO = C.IC_EPO\n" );
			qrySentencia.append("             AND IC_PYME = C.IC_PYME\n" );
			qrySentencia.append("             AND IC_NAFIN_ELECTRONICO = N.IC_NAFIN_ELECTRONICO)\n" );
			qrySentencia.append("                                                             AS CAUSA_BLOQUEO,\n" );
			qrySentencia.append("         (SELECT TO_CHAR (DF_MODIFICACION,\n" );
			qrySentencia.append("                          'DD/MM/YYYY HH:MI:SS')\n" );
			qrySentencia.append("            FROM COMREL_BLOQUEO_PYME_X_IF_EPO\n" );
			qrySentencia.append("           WHERE IC_IF = I.IC_IF\n" );
			qrySentencia.append("             AND IC_EPO = C.IC_EPO\n" );
			qrySentencia.append("             AND IC_PYME = C.IC_PYME\n" );
			qrySentencia.append("             AND IC_NAFIN_ELECTRONICO = N.IC_NAFIN_ELECTRONICO) AS FECHA_MOD,\n" );
			qrySentencia.append("         (SELECT IC_USUARIO\n" );
			qrySentencia.append("            FROM COMREL_BLOQUEO_PYME_X_IF_EPO\n" );
			qrySentencia.append("           WHERE IC_IF = I.IC_IF\n" );
			qrySentencia.append("             AND IC_EPO = C.IC_EPO\n" );
			qrySentencia.append("             AND IC_PYME = C.IC_PYME\n" );
			qrySentencia.append("             AND IC_NAFIN_ELECTRONICO = N.IC_NAFIN_ELECTRONICO) AS USUARIO\n" );
			qrySentencia.append("    FROM COMREL_IF_EPO I,\n" );
			qrySentencia.append("         COMREL_PYME_EPO C,\n" );
			qrySentencia.append("         COMCAT_EPO E,\n" );
			qrySentencia.append("         COMCAT_PYME P,\n" );
			qrySentencia.append("         COMREL_NAFIN N\n" );
			qrySentencia.append("   WHERE I.IC_EPO = C.IC_EPO\n" );
			qrySentencia.append("     AND C.IC_EPO = E.IC_EPO\n" );
			qrySentencia.append("     AND C.IC_PYME = P.IC_PYME\n" );
			qrySentencia.append("     AND C.CS_HABILITADO = 'S'\n" );
			qrySentencia.append("     AND E.CS_HABILITADO = 'S'\n" );
			qrySentencia.append("     AND P.CS_HABILITADO = 'S'\n" );
			qrySentencia.append("     AND C.CG_PYME_EPO_INTERNO IS NOT NULL\n" );
			qrySentencia.append("     AND N.IC_EPO_PYME_IF = C.IC_PYME\n" );
			qrySentencia.append("     AND N.CG_TIPO = 'P' \n");

			if(!this.icIf.equals("")){
				qrySentencia.append("     AND I.IC_IF = ? \n"               );
				conditions.add(icIf);
			}
			if(!this.icEpo.equals("")){
				qrySentencia.append("     AND I.IC_EPO = ? \n"              );
				conditions.add(icEpo);
			}
			if(!this.icPyme.equals("")){
				qrySentencia.append("     AND C.IC_PYME = ? \n"             );
				conditions.add(icPyme);
			}
			if(!this.nafinElectronico.equals("")){
				qrySentencia.append("     AND N.IC_NAFIN_ELECTRONICO = ? \n");
				conditions.add(nafinElectronico);
			}
			if(!this.rfc.equals("")){
				qrySentencia.append("     AND P.CG_RFC = ? \n"              );
				conditions.add(rfc);
			}
			qrySentencia.append("ORDER BY I.IC_IF, I.IC_EPO, C.IC_PYME");

		} else if(this.informacion.equals("CONSULTA_HISTORICO")){

			qrySentencia.append("SELECT   B.IC_IF, B.IC_PYME, B.IC_EPO, \n"                                  );
			qrySentencia.append("         B.CS_BLOQUEO AS BLOQUEO_ANT,\n"                                    );
			qrySentencia.append("         E.CG_RAZON_SOCIAL AS EPO, \n"                                      );
			qrySentencia.append("         P.CG_RAZON_SOCIAL AS PYME,\n"                                      );
			qrySentencia.append("         B.IC_NAFIN_ELECTRONICO AS NE, \n"                                  );
			qrySentencia.append("         P.CG_RFC AS RFC, P.CG_EMAIL AS EMAIL,\n"                           );
			qrySentencia.append("         TO_CHAR (B.DF_MODIFICACION, 'DD/MM/YYYY HH:MI:SS') AS FECHA_MOD,\n");
			qrySentencia.append("         B.CG_CAUSA_BLOQUEO AS CAUSA_BLOQUEO,\n"                            );
			qrySentencia.append("         B.IC_USUARIO AS USUARIO\n"                                         );
			qrySentencia.append("    FROM COMHIS_BLOQUEO_PYME_X_IF_EPO B,\n"                                 );
			qrySentencia.append("         COMCAT_PYME P,\n"                                                  );
			qrySentencia.append("         COMCAT_EPO E \n"                                                   );
			//qrySentencia.append("         COMREL_NAFIN N\n"                                                  );
			qrySentencia.append("   WHERE B.IC_PYME = P.IC_PYME\n"                                           );
			qrySentencia.append("     AND B.IC_EPO = E.IC_EPO\n"                                             );
			//qrySentencia.append("     AND B.IC_PYME = N.IC_EPO_PYME_IF\n"                                    );
			//qrySentencia.append("     AND N.CG_TIPO = 'P'\n"                                                 );

			if(!this.icEpo.equals("")){
				qrySentencia.append("     AND B.IC_EPO = ? \n"              );
				conditions.add(icEpo);
			}
			if(!this.nafinElectronico.equals("")){
				qrySentencia.append("     AND B.IC_NAFIN_ELECTRONICO = ? \n");
				conditions.add(nafinElectronico);
			}
			if(!this.rfc.equals("")){
				qrySentencia.append("     AND P.CG_RFC = ? \n"              );
				conditions.add(rfc);
			}
			if(!this.icIf.equals("")){
				qrySentencia.append("     AND B.IC_IF = ? \n"               );
				conditions.add(icIf);
			}
			if(!this.icPyme.equals("")){
				qrySentencia.append("     AND B.IC_PYME = ? \n"             );
				conditions.add(icPyme);
		}
		qrySentencia.append("ORDER BY B.IC_IF, B.IC_PYME, B.IC_EPO"     );

		}

		log.debug("Sentencia SQL: \n" + qrySentencia.toString());
		log.debug("Condiciones: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");

		return qrySentencia.toString();
	}

	/**
	 * Genera el archivo PDF o CSV con los datos obtenidos de getDocumentQueryFile
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rsAPI
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile(E)");

		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();

		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual   = fechaActual.substring(0,2);
			String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual  = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

			pdfDoc.setTable(9,100);
			pdfDoc.setCell("EPO "                     , "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Pyme "                    , "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("N@E "                     , "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("RFC "                     , "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Email "                   , "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Bloqueo Hist�rico "       , "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha y Hora Modificaci�n", "celda01", ComunesPDF.CENTER); 
			pdfDoc.setCell("Usuario "                 , "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Causa del Bloqueo "       , "celda01", ComunesPDF.CENTER);

			while (rs.next()){
				String epo      = (rs.getString("EPO")           == null) ? "" : rs.getString("EPO");
				String pyme     = (rs.getString("PYME")          == null) ? "" : rs.getString("PYME");
				String ne       = (rs.getString("NE")            == null) ? "" : rs.getString("NE");
				String rfc      = (rs.getString("RFC")           == null) ? "" : rs.getString("RFC");
				String email    = (rs.getString("EMAIL")         == null) ? "" : rs.getString("EMAIL");
				String fechaMod = (rs.getString("FECHA_MOD")     == null) ? "" : rs.getString("FECHA_MOD");
				String usuario  = (rs.getString("USUARIO")       == null) ? "" : rs.getString("USUARIO");
				String causa    = (rs.getString("CAUSA_BLOQUEO") == null) ? "" : rs.getString("CAUSA_BLOQUEO");
				String bloqueo  = (rs.getString("BLOQUEO_ANT")   == null) ? "" : rs.getString("BLOQUEO_ANT");

				if(bloqueo.equals("S")){
					bloqueo = "Bloqueado";
				} else if(bloqueo.equals("N")){
					bloqueo = "Desbloqueado";
				}

				pdfDoc.setCell(epo,      "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(pyme,     "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(ne,       "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(rfc,      "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(email,    "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(bloqueo,  "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(fechaMod, "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(usuario,  "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(causa,    "formas", ComunesPDF.LEFT);
			}

			pdfDoc.addTable();
			pdfDoc.endDocument();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}	
		
		log.info("crearCustomFile(S)");
		return nombreArchivo;
	}

	/**
	 * Metodo para generar un archivo PDF utilizando paginaci�n
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		return null;
	}

/***************************************************************
 *          M�TODOS CORRESPONDIENTES AL FODEA 25-2015          *
 ***************************************************************/
	public String cambiaEstatusBloqueo(){

		log.info("cambiaEstatusBloqueo(E)");

		String  mensaje = "";
		boolean success = false;
		int     rows    = 0;

		StringBuffer      sqlInsert = new StringBuffer();
		StringBuffer      sqlUpdate = new StringBuffer();
		StringBuffer      sqlHist   = new StringBuffer();
		AccesoDB          con       = new AccesoDB();
		PreparedStatement ps        = null;

		String valoresActuales   = "";

		HashMap mapaGrid  = new HashMap();
		String icEpo      = "";
		String icIf       = "";
		String icPyme     = "";
		String ne         = ""; // nafin electronico
		String estatusAnt = "";
		String estatusAct = "";
		String fecha      = "";
		String causa      = "";

		// Armo las consultas correspondientes
		sqlInsert.append("INSERT INTO COMREL_BLOQUEO_PYME_X_IF_EPO "                   );
		sqlInsert.append("(IC_IF, IC_EPO, IC_PYME, CG_CAUSA_BLOQUEO, DF_MODIFICACION, ");
		sqlInsert.append("CS_BLOQUEO, IC_USUARIO, IC_NAFIN_ELECTRONICO)"               );
		sqlInsert.append("VALUES (?, ?, ?, ?, SYSDATE, 'S', ?, ?)"                     );

		sqlUpdate.append("UPDATE COMREL_BLOQUEO_PYME_X_IF_EPO SET ");
		sqlUpdate.append("DF_MODIFICACION = SYSDATE, "             );
		sqlUpdate.append("CS_BLOQUEO = ?, "                        );
		sqlUpdate.append("CG_CAUSA_BLOQUEO = ?, "                  );
		sqlUpdate.append("IC_USUARIO = ? "                         );
		sqlUpdate.append("WHERE IC_IF = ? "                        );
		sqlUpdate.append("AND IC_EPO = ? "                         );
		sqlUpdate.append("AND IC_PYME = ? "                        );
		sqlUpdate.append("AND IC_NAFIN_ELECTRONICO = ? "           );

		sqlHist.append("INSERT INTO COMHIS_BLOQUEO_PYME_X_IF_EPO"                                          );
		sqlHist.append("(IC_MODIFICACION, IC_IF, IC_EPO, IC_PYME, CG_CAUSA_BLOQUEO, "                      );
		sqlHist.append(" DF_MODIFICACION, CS_BLOQUEO, IC_USUARIO, IC_NAFIN_ELECTRONICO) "                  );
		sqlHist.append("VALUES ((SELECT NVL(MAX(IC_MODIFICACION),0)+1 FROM COMHIS_BLOQUEO_PYME_X_IF_EPO)," );
		sqlHist.append("?, ?, ?, ?, SYSDATE, ?, ?, ?)"                                                     );

		try{

			con.conexionDB();

			for(int i=0; i<listaGrid.size(); i++){

				valoresActuales   = "";

				ps   = null;
				rows = 0;

				mapaGrid   = new HashMap();
				mapaGrid   = (HashMap) listaGrid.get(i);
				icIf       = (String) mapaGrid.get("IC_IF");
				icEpo      = (String) mapaGrid.get("IC_EPO");
				icPyme     = (String) mapaGrid.get("IC_PYME");
				ne         = (String) mapaGrid.get("NE");
				estatusAnt = (String) mapaGrid.get("BLOQUEO_ANT");
				estatusAct = (String) mapaGrid.get("BLOQUEO_ACT");
				fecha      = (String) mapaGrid.get("FECHA_MOD");
				causa      = (String) mapaGrid.get("CAUSA_BLOQUEO");

				System.out.println("estatusAnt: " + estatusAnt);

				if(!estatusAnt.equals(estatusAct)){
					if(estatusAct.equals("S")){ // Se bloquea el registro por primera vez
						if(estatusAnt.equals("")){
							log.debug("Sentencia SQL: " + sqlInsert.toString());
							ps = con.queryPrecompilado(sqlInsert.toString());
							ps.clearParameters();
							ps.setInt(   1, Integer.parseInt(icIf)  );
							ps.setInt(   2, Integer.parseInt(icEpo) );
							ps.setInt(   3, Integer.parseInt(icPyme));
							ps.setString(4, causa                   );
							ps.setString(5, usuario                 );
							ps.setLong(  6, Long.parseLong(ne)      );
							rows = ps.executeUpdate();
						} else if(estatusAnt.equals("N")){ // Ya se hab�a bloqueado anteriormente y solo se actualiza su estado
							log.debug("Sentencia SQL: " + sqlUpdate.toString());
							ps = con.queryPrecompilado(sqlUpdate.toString());
							ps.clearParameters();
							ps.setString(1, estatusAct              );
							ps.setString(2, causa                   );
							ps.setString(3, usuario                 );
							ps.setInt(   4, Integer.parseInt(icIf)  );
							ps.setInt(   5, Integer.parseInt(icEpo) );
							ps.setInt(   6, Integer.parseInt(icPyme));
							ps.setLong(  7, Long.parseLong(ne)      );
							rows = ps.executeUpdate();
						}

					} else if(estatusAct.equals("N")){ // Se desbloquea el registro
							log.debug("Sentencia SQL: " + sqlUpdate.toString());
							ps = con.queryPrecompilado(sqlUpdate.toString());
							ps.clearParameters();
							ps.setString(1, estatusAct              );
							ps.setString(2, causa                   );
							ps.setString(3, usuario                 );
							ps.setInt(   4, Integer.parseInt(icIf)  );
							ps.setInt(   5, Integer.parseInt(icEpo) );
							ps.setInt(   6, Integer.parseInt(icPyme));
							ps.setLong(  7, Long.parseLong(ne)      );
							rows = ps.executeUpdate();
					}
				}
				log.debug("cambiaEstatusBloqueo.Estatus anterior: <<"       + estatusAnt + ">>");
				log.debug("cambiaEstatusBloqueo.Estatus actual: <<"         + estatusAct + ">>");
				log.debug("cambiaEstatusBloqueo.Registros actualizados: <<" + rows       + ">>");

				if(rows > 0){
					log.debug("Actualizo el hist�rico: Sentencia: \n" + sqlHist.toString());
					rows = 0;
					ps = null;
					ps = con.queryPrecompilado(sqlHist.toString());
					ps.clearParameters();
					ps.setInt(   1, Integer.parseInt(icIf)  );
					ps.setInt(   2, Integer.parseInt(icEpo) );
					ps.setInt(   3, Integer.parseInt(icPyme));
					ps.setString(4, causa                   );
					ps.setString(5, estatusAct              );
					ps.setString(6, usuario                 );
					ps.setLong(  7, Long.parseLong(ne)      );
					rows = ps.executeUpdate();

					// Inserto en bit�cora
					log.debug("Inserto en bit�cora");

					if(estatusAct.equals("S")){
						estatusAct = "bloqueada";
					} else if(estatusAct.equals("N")){
						estatusAct = "desbloqueada";
					}
					valoresActuales = "N@E Pyme: " + ne + ", ic_epo: " + icEpo + "-" + estatusAct;

					log.debug("nafin electronico: " + this.nafinElectronico);
					log.debug("usuario: " + this.usuario);
					log.debug("valoresActuales: " + valoresActuales);

					Bitacora.grabarEnBitacora(con, "BLOQPROV", "I", this.nafinElectronico, this.usuario, "", valoresActuales);

				}
				success = true;
			}

			if(rows > 0){
				mensaje = "Los cambios se realizaron con �xito";
			} else{
				mensaje = "No se encontraron registros para actualizar";
			}

		}catch(Exception e){
			success = false;
			log.warn("cambiaEstatusBloqueo.Exception. " + e);
			mensaje = "Error al actualizar los registros. " + e;
		}finally{
			try{
				if(ps!=null)
					ps.close();
			}catch(SQLException ex){
				log.warn(ex);
			}
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(success);
				con.cierraConexionDB();
			}
		}

		log.info("cambiaEstatusBloqueo(S)");
		return mensaje;
	}

/*****************************************
 *          GETTERS AND SETTERS          *
 *****************************************/
	public List getConditions() {
		return conditions;
	}

	public List getListaGrid() {
		return listaGrid;
	}

	public void setListaGrid(List listaGrid) {
		this.listaGrid = listaGrid;
	}

	public String getNafinElectronico() {
		return nafinElectronico;
	}

	public void setNafinElectronico(String nafinElectronico) {
		this.nafinElectronico = nafinElectronico;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getIcEpo() {
		return icEpo;
	}

	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getInformacion() {
		return informacion;
	}

	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}

	public String getIcIf() {
		return icIf;
	}

	public void setIcIf(String icIf) {
		this.icIf = icIf;
	}

	public String getIcPyme() {
		return icPyme;
	}

	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}

}