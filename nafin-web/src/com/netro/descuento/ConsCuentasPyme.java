package com.netro.descuento;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsCuentasPyme implements IQueryGeneratorRegExtJS {
private final static Log log = ServiceLocator.getInstance().getLog(ConsCuentasPyme.class);
	private String paginaOffset;
	private String paginaNo;
	private String icCuentaBancaria;
	private String icPyme;
	StringBuffer qrySentencia = new StringBuffer("");
	private List 		conditions;
	public ConsCuentasPyme() {
	}
	public String getAggregateCalculationQuery() {
		return "";
	}

	public String getDocumentQuery() {
		
	return "";
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		return "";
		
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append("select * from " +
								"(SELECT   pi.cs_vobo_if, ci.cg_razon_social as intermediario,ci.ic_if " +
								"    FROM comrel_pyme_if pi, comrel_cuenta_bancaria cb,comcat_if ci  " +
								"   WHERE pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria  " +
								"     AND cb.cs_borrado = 'N'  " +
								"     AND pi.cs_borrado = 'N' " +
								"     AND ci.IC_IF = pi.IC_IF  " +
								"     and cb.IC_MONEDA = (select IC_MONEDA from comrel_cuenta_bancaria where ic_cuenta_bancaria=? ) " +
								
								"     and cb.ic_cuenta_bancaria not in (?)  " +
								"     and cb.ic_pyme = ? " +
								"     group by pi.cs_vobo_if, ci.ic_if,ci.cg_razon_social " +
								"     order by intermediario, pi.cs_vobo_if desc) " +
								"     where cs_vobo_if = 'S' " +
								"     union all " +
								"select * from " +
								"(SELECT   pi.cs_vobo_if, ci.cg_razon_social as intermediario,ci.ic_if " +
								"    FROM comrel_pyme_if pi, comrel_cuenta_bancaria cb,comcat_if ci  " +
								"   WHERE pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria  " +
								"     AND cb.cs_borrado = 'N'  " +
								"     AND pi.cs_borrado = 'N' " +
								"     AND ci.IC_IF = pi.IC_IF  " +
								"     and cb.IC_MONEDA = (select IC_MONEDA from comrel_cuenta_bancaria where ic_cuenta_bancaria=? ) " +
								
								"     and cb.ic_cuenta_bancaria not in (?) " +
								"     and cb.ic_pyme = ? " +
								"     and not exists(select * from " +
								"    (SELECT   pi.cs_vobo_if as s1,pi.ic_if as s2 " +
								"    FROM comrel_pyme_if pi, comrel_cuenta_bancaria cb " +
								"   WHERE pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria  " +
								"     AND cb.cs_borrado = 'N'  " +
								"     AND pi.cs_borrado = 'N' " +
								"     and cb.IC_MONEDA = (select IC_MONEDA from comrel_cuenta_bancaria where ic_cuenta_bancaria=? ) " +
								
								"     and cb.ic_cuenta_bancaria not in (?)  " +
								"     and cb.ic_pyme = ? " +
								"     group by pi.cs_vobo_if,pi.IC_IF " +
								"     ) xx " +
								"     where xx.s1 = 'S' " +
								"     and xx.s2=ci.ic_if) " +
								"     group by pi.cs_vobo_if, ci.ic_if,ci.cg_razon_social " +
								"     order by intermediario, pi.cs_vobo_if desc) " +
								"     where cs_vobo_if='N' ");

			conditions.add(icCuentaBancaria);
			conditions.add(icCuentaBancaria);
			conditions.add(icPyme);
			
			conditions.add(icCuentaBancaria);
			conditions.add(icCuentaBancaria);
			conditions.add(icPyme);
			
			conditions.add(icCuentaBancaria);
			conditions.add(icCuentaBancaria);
			conditions.add(icPyme);

	
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
	
		try {
	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return "";
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		return "";
	}//1367
		public List getConditions(){
		return this.conditions;
	}

	public void setPaginaOffset(String paginaOffset) {
		this.paginaOffset = paginaOffset;
	}

	public String getPaginaOffset() {
		return paginaOffset;
	}

	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}

	public String getPaginaNo() {
		return paginaNo;
	}


	public void setIcCuentaBancaria(String icCuentaBancaria) {
		this.icCuentaBancaria = icCuentaBancaria;
	}


	public String getIcCuentaBancaria() {
		return icCuentaBancaria;
	}


	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}


	public String getIcPyme() {
		return icPyme;
	}




	
	

}