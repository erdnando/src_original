package com.netro.descuento;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ParametrosIf {

	private final static Log log = ServiceLocator.getInstance().getLog(ParametrosIf.class);
  private String icIf;
  
  public ParametrosIf() { }
  
  /**
   * Obtiene los estatus de los documentos a mostrar por If pantalla Parametros x IF
   * @return lista de estatus par cada If
   */
  public Registros layout(){
	log.info("layout (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer("");
    List         conditions	  = new ArrayList();
    Registros    registros    = new Registros();
		AccesoDB     con          = new AccesoDB(); 
		
		try{
			con.conexionDB();
			qrySentencia.append(
      "SELECT i.ic_if, i.cg_razon_social, "+"\n"+
      "  NVL(pxi.cs_layout_completo_venc, 'S') AS cs_layout_completo_venc, "+"\n"+
      "  NVL(pxi.cs_layout_completo_oper, 'S') AS cs_layout_completo_oper, "+"\n"+
      "  NVL(pxi.cs_layout_doctos_venc, 'N') AS cs_layout_doctos_venc "+"\n"+
      " FROM comcat_if i, com_param_x_if pxi "+"\n"+
      " WHERE i.cs_habilitado = 'S' "+"\n"+
      "  AND i.ic_if = pxi.ic_if (+)"
      );
      if (icIf.equals("")){ 
      } else {
        qrySentencia.append(" AND i.ic_if = "+icIf+ " " );
      }
      
      qrySentencia.append(" ORDER BY i.cg_razon_social" );	
      
			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("layout  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
		log.info("qrySentencia :\n \n"+qrySentencia.toString()+" \n \n");
		log.info("layout (S) ");
		return registros;
															
	}

/**Pantalla Parametros x IF
 * Almacena los parametros generales referentes al Layout de vencimientos y operados. Pantalla Parametros x IF
 * @param clavesIF Lista de IFs.
 * @param layoutsCompletoVenc Lista que contiene S si se desea ver el layout completo de vencimientos. N de lo contrario
 * @param layoutsCompletoOper Lista que contiene S si se desea ver el layout completo de operados. N de lo contrario
 * @param layoutsDoctosVenc   Lista que contiene S si se desea ver el layout completo de DoctosVenc. N de lo contrario
 */
public void guardarParametrosLayout(List clavesIF, List layoutsCompletoVenc, List layoutsCompletoOper, List layoutsDoctosVenc) throws NafinException {
  log.info("guardarParametrosLayout (E) ");

	try {
		if (clavesIF == null || layoutsCompletoVenc == null || layoutsCompletoOper == null || layoutsDoctosVenc == null ||
				clavesIF.size() == 0 || layoutsCompletoVenc.size() == 0 || layoutsCompletoOper.size() == 0 || layoutsDoctosVenc.size() == 0) {//FODEA 015 - 2009 ACF
			throw new Exception("Los parametros no pueden ser nulos ni vacios");
		}
	} catch(Exception e) {
		System.out.println("Error en los parametros recibidos. " + 
				e.getMessage() + "\n" +
				" clavesIF =" + clavesIF + "\n" +
				" layoutsCompletoVenc =" + layoutsCompletoVenc + "\n" +
				" layoutsCompletoOper =" + layoutsCompletoOper);
		throw new NafinException("SIST0001");
	}
	
	AccesoDB con = null;
	boolean resultado = true;
	try{
		con = new AccesoDB();
		con.conexionDB();

		String qrySentencia  = 
				" SELECT count(*) FROM com_param_x_if " +
				" WHERE ic_if = ?";
		PreparedStatement ps = con.queryPrecompilado(qrySentencia);

		qrySentencia = " INSERT INTO com_param_x_if (" +
						" ic_if, cs_layout_completo_venc, cs_layout_completo_oper, cs_layout_doctos_venc) " +//FODEA 015 - 2009 ACF
						" VALUES(?,?,?,?) ";//FODEA 015 - 2009 ACF
		PreparedStatement psInsert = con.queryPrecompilado(qrySentencia);
		
		qrySentencia = " UPDATE com_param_x_if " +
					" SET cs_layout_completo_venc = ?, " +
					" cs_layout_completo_oper = ?, " +
          " cs_layout_doctos_venc = ? " +//FODEA 015 - 2009 ACF
					" WHERE ic_if = ?";
		PreparedStatement psUpdate = con.queryPrecompilado(qrySentencia);	
		
		for (int i = 0; i < clavesIF.size(); i++) {
			int claveIF = Integer.parseInt((String)clavesIF.get(i));
			String cs_layout_completo_venc = (String)layoutsCompletoVenc.get(i);
			String cs_layout_completo_oper = (String)layoutsCompletoOper.get(i);
      String cs_layout_doctos_venc = (String)layoutsDoctosVenc.get(i);//FODEA 015 - 2009 ACF
			
			ps.setInt(1, claveIF);
			ResultSet rs = ps.executeQuery();
			rs.next();
			int numRegistros = rs.getInt(1);
			rs.close();
			
			if (numRegistros == 0) { //no existe -->insertar registro
				psInsert.clearParameters();
				psInsert.setInt(1, claveIF);
				psInsert.setString(2, cs_layout_completo_venc);
				psInsert.setString(3, cs_layout_completo_oper);
        psInsert.setString(4, cs_layout_doctos_venc);//FODEA 015 - 2009 ACF
				psInsert.executeUpdate();
			} else { //Ya existe --> actualiza valores
				psUpdate.clearParameters();
				psUpdate.setString(1, cs_layout_completo_venc);
				psUpdate.setString(2, cs_layout_completo_oper);
        psUpdate.setString(3, cs_layout_doctos_venc);//FODEA 015 2009 ACF
				psUpdate.setInt(4, claveIF);
				psUpdate.executeUpdate();
			}
		}
		ps.close();
		psUpdate.close();
		psInsert.close();
    log.info("guardarParametrosLayout (S) ");
    
	}catch(Exception e){
		resultado = false;
		e.printStackTrace();
    log.info("guardarParametrosLayout (Exception) ");
		throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta()){
			con.terminaTransaccion(resultado);
			con.cierraConexionDB();
		}
	}
}

  /**
   * Obtiene los estatus de los documentos a mostrar Pantalla Parametros Generales
   * @return lista de estatus 
   */
  public Registros layoutParamGen(){
	log.info("layoutParamGen (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer("");
    List         conditions	  = new ArrayList();
    Registros    registros    = new Registros();
		AccesoDB     con          = new AccesoDB(); 
		
		try{
			con.conexionDB();
			qrySentencia.append(
      "SELECT cs_layout_completo_venc, cs_layout_completo_oper "+"\n"+
      " FROM com_param_gral  "
      );
      
			registros = con.consultarDB(qrySentencia.toString());
      
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("layoutParamGen  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
		log.info("qrySentencia :\n \n"+qrySentencia.toString()+" \n \n");
		log.info("layoutParamGen (S) ");
		return registros;
															
	} 
  
/** Pantalla Parametros generales
 * Almacena los parametros generales referentes al Layout de vencimientos y operados.
 * @param cs_layout_completo_venc S si se desea ver el layout completo de vencimientos. 		N de lo contrario
 * @param cs_layout_completo_venc S si se desea ver el layout completo de operados. 		N de lo contrario
 */
public void guardarParametrosGralLayout(String cs_layout_completo_venc,
		String cs_layout_completo_oper) throws NafinException {
    
  log.info("guardarParametrosGralLayout (E) ");
	try {
		if (cs_layout_completo_venc == null || cs_layout_completo_oper == null ||
				cs_layout_completo_venc.equals("") || cs_layout_completo_oper.equals("")) {
				throw new Exception("Los parametros no pueden ser nulos ni vacios");
		}
	} catch(Exception e) {
		System.out.println("Error en los parametros recibidos. " + 
				e.getMessage() + "\n" +
				" cs_layout_completo_venc =" + cs_layout_completo_venc + "\n" +
				" cs_layout_completo_oper = " + cs_layout_completo_oper);
		throw new NafinException("SIST0001");
	}
	
	AccesoDB con			= null;
	boolean resultado = true;
	try{
		con = new AccesoDB();
		con.conexionDB();
		
		String qrySentencia = " UPDATE com_param_gral " +
				" SET cs_layout_completo_venc = ?, " +
				" cs_layout_completo_oper = ? ";
		PreparedStatement ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1, cs_layout_completo_venc);
		ps.setString(2, cs_layout_completo_oper);
		ps.executeUpdate();
		ps.close();
    log.info("guardarParametrosGralLayout (S) ");
	}catch(Exception e){
		resultado = false;
		e.printStackTrace();
    log.info("guardarParametrosGralLayout (Exception) ");
		throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta()){
			con.terminaTransaccion(resultado);
			con.cierraConexionDB();
		}
	}
}
  

  public void setIcIf(String icIf)  {
    this.icIf = icIf;
  }

  public String getIcIf()  {
    return icIf;
  }
  
}