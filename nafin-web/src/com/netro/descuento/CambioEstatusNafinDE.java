package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CambioEstatusNafinDE implements IQueryGenerator, IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(CambioEstatusNafinDE.class);
	private String paginaOffset;
	private String paginaNo;
	private String ic_epo             = "";
	private String ic_pyme            = "";
	private String ig_numero_docto    = "";
	private String df_fecha_vencMin   = "";
	private String df_fecha_vencMax   = "";
	private String ic_moneda          = "";
	private String fn_montoMin        = "";
	private String fn_montoMax        = "";
	private String ic_cambio_estatus  = "";
	private String df_fecha_doctoMin  = "";
	private String df_fecha_doctoMax  = "";
	private String dc_fecha_cambioMin = "";
	private String dc_fecha_cambioMax = "";
	private String tipoFactoraje      = "";
	private String ic_banco_fondeo    = ""; 
	private String firmaMancomunada   = "";
	private List conditions;

/////Version EXTJS
	public String getAggregateCalculationQuery(){

		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		log.info("getAggregateCalculationQuery (E)");

		try{
			qrySentencia.append(
				" select /*+use_nl (ce d m) index (ce IN_COMHIS_CAM_ESTA_02_NUK) */ "+
				" count(1) as CambioEstatusNafinDE"+
				" ,sum(D.fn_monto) as fn_monto"+
				" ,sum(D.fn_monto) as fn_monto_dscto"+
				" ,D.ic_moneda"+
				" ,M.cd_nombre"+
				" from "+
				" comhis_cambio_estatus ce"+
				" , com_documento d "+
				" , comcat_moneda m"+
				" where");
				if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("S") && !ic_epo.equals("") ){
					qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
				} else if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("N") &&  !ic_epo.equals("")){
					qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32) ");
				}else if (ic_cambio_estatus.equals("") &&  ic_epo.equals("")){
					qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
				} else if (!ic_cambio_estatus.equals("")){
					qrySentencia.append("  ce.ic_cambio_estatus = ? ");
					conditions.add(ic_cambio_estatus);
				}

			qrySentencia.append(" and d.ic_moneda=m.ic_moneda"+
				" and d.ic_documento = ce.ic_documento");

			if (!ic_epo.equals("")){
				qrySentencia.append(" and d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if (!ic_pyme.equals("")){
				qrySentencia.append(" and d.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!ic_moneda.equals("")){
				qrySentencia.append(" and d.ic_moneda = ? ");
				conditions.add(ic_moneda);
			}
			if (!ig_numero_docto.equals("")){
				qrySentencia.append(" and d.ig_numero_docto = ? ");
				conditions.add(ig_numero_docto.trim());
			}
			if(!fn_montoMin.equals("")){
				if(!fn_montoMax.equals("")){
					qrySentencia.append(" and d.fn_monto between ? and ? ");
					conditions.add(fn_montoMin);
					conditions.add(fn_montoMax);
				}
			}
			if(!df_fecha_doctoMin.equals("")){
				if(!df_fecha_doctoMax.equals("")){
					qrySentencia.append(" and d.df_fecha_docto >= TO_DATE(?,'dd/mm/yyyy') and d.df_fecha_docto < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_doctoMin);
					conditions.add(df_fecha_doctoMax);
				}
			}
			if(!df_fecha_vencMin.equals("")){
				if(!df_fecha_vencMax.equals("")){
					qrySentencia.append(" and d.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') and d.df_fecha_venc  < TO_DATE(?,'dd/mm/yyyy') + 1 ");
					conditions.add(df_fecha_vencMin);
					conditions.add(df_fecha_vencMax);
				}
			}
			if(!dc_fecha_cambioMin.equals("")){
				if(!dc_fecha_cambioMax.equals("")){
					qrySentencia.append(" and ce.dc_fecha_cambio >= TO_DATE(?,'dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE(?,'dd/mm/yyyy') + 1 ");
					conditions.add(dc_fecha_cambioMin);
					conditions.add(dc_fecha_cambioMax);
				}
			}
			if(!tipoFactoraje.equals("")){
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL= ? ");
				conditions.add(tipoFactoraje);
			}

			if("".equals(tipoFactoraje) && "".equals(ig_numero_docto) && "".equals(ic_cambio_estatus) && "".equals(ic_epo) && "".equals(ic_pyme) && "".equals(ic_moneda) && "".equals(df_fecha_vencMin) && "".equals(dc_fecha_cambioMin) && "".equals(fn_montoMin))
				qrySentencia.append(" and ce.dc_fecha_cambio >= TRUNC(sysdate) AND ce.dc_fecha_cambio < TRUNC(sysdate)+1 ");

			qrySentencia.append(" group by D.ic_moneda,M.cd_nombre");
			qrySentencia.append(" order by D.ic_moneda");

			log.info("qrySentencia: " + qrySentencia.toString());
		} catch(Exception e){
			log.warn("CambioEstatusNafinDE::getDocumentQueryFileException. " + e);
		}
		log.info("getAggregateCalculationQuery (S)");
		return qrySentencia.toString();
	}

	public String getDocumentQuery(){
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();
		log.info("getDocumentQuery (E)");
		try{
			qrySentencia.append(
			" select /*+use_nl (ce d) index (ce IN_COMHIS_CAM_ESTA_02_NUK) */" +
			" d.ic_documento, to_char(ce.dc_fecha_cambio,'ddmmyyyyhh24miss') as fecha_cambio,"+
			"  d.ic_documento||'_'||to_char(ce.dc_fecha_cambio,'ddmmyyyyhh24miss') as orden "+
			" ,'CambioEstatusNafinDE::getDocumentQuery'"+
			" from "+
			" comhis_cambio_estatus ce"+
			" , com_documento d, comrel_producto_epo pe "+
			" where");
			if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("S") && !ic_epo.equals("") ){
				qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
			} else if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("N") &&  !ic_epo.equals("")){
				qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32) ");
			} else if (ic_cambio_estatus.equals("") &&  ic_epo.equals("")){
				qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
			} else if (!ic_cambio_estatus.equals("")){
				qrySentencia.append("  ce.ic_cambio_estatus = ? ");
				conditions.add(ic_cambio_estatus);
			}
			qrySentencia.append(
			" and d.ic_documento = ce.ic_documento" +
			" and d.ic_epo = pe.ic_epo " +
			" and pe.ic_producto_nafin = 1 " );
			//" and (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) ");
			if (!ic_epo.equals("")){
				qrySentencia.append(" and d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if (!ic_pyme.equals("")){
				qrySentencia.append(" and d.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!ic_moneda.equals("")){
				qrySentencia.append(" and d.ic_moneda = ? ");
				conditions.add(ic_moneda);
			}
			if (!ig_numero_docto.equals("")){
				qrySentencia.append(" and d.ig_numero_docto = ? ");
				conditions.add(ig_numero_docto.trim());
			}
			if(!fn_montoMin.equals("")){
				if(!fn_montoMax.equals("")){
					qrySentencia.append(" and d.fn_monto between ? and ? ");
					conditions.add(fn_montoMin);
					conditions.add(fn_montoMax);
				}
			}
			if(!df_fecha_doctoMin.equals("")){
				if(!df_fecha_doctoMax.equals("")){
					qrySentencia.append(" and d.df_fecha_docto >= TO_DATE(?,'dd/mm/yyyy') and d.df_fecha_docto < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_doctoMin);
					conditions.add(df_fecha_doctoMax);
				}
			}
			if(!df_fecha_vencMin.equals("")){
				if(!df_fecha_vencMax.equals("")){
					qrySentencia.append(" and d.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') and d.df_fecha_venc  < TO_DATE(?,'dd/mm/yyyy') + 1 ");
					conditions.add(df_fecha_vencMin);
					conditions.add(df_fecha_vencMax);
				}
			}
			if(!dc_fecha_cambioMin.equals("")){
				if(!dc_fecha_cambioMax.equals("")){
					qrySentencia.append(" and ce.dc_fecha_cambio >= TO_DATE(?,'dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE(?,'dd/mm/yyyy') + 1 ");
					conditions.add(dc_fecha_cambioMin);
					conditions.add(dc_fecha_cambioMax);
				}
			}
			if(!tipoFactoraje.equals("")){
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL= ? ");
				conditions.add(tipoFactoraje);
			}

			if("".equals(tipoFactoraje)&&"".equals(ig_numero_docto)&&"".equals(ic_cambio_estatus)&&"".equals(ic_epo)&&"".equals(ic_pyme)&&"".equals(ic_moneda)&&"".equals(df_fecha_vencMin)&&"".equals(dc_fecha_cambioMin)&&"".equals(fn_montoMin))
				qrySentencia.append(" and ce.dc_fecha_cambio >= TRUNC(sysdate) and ce.dc_fecha_cambio < TRUNC(sysdate)+1 ");

			qrySentencia.append(" ORDER BY orden ");

			log.info("qrySentencia: " + qrySentencia.toString());
		} catch(Exception e){
			log.warn("CambioEstatusNafinDE::getDocumentQueryException. " + e);
		}
		log.info("getDocumentQuery (S)");
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(List pageIds){
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();
		log.info("getDocumentSummaryQueryForIds (E)");
		qrySentencia.append(
		" select (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO"+
		" , (decode (p.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+
		" , d.ig_numero_docto , TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"+
		" , TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto"+
		" , TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') as dc_fecha_cambio"+
		" , d.ic_moneda, m.cd_nombre, d.fn_monto, decode(d.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl) as fn_aforo"+
		" , (decode(d.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl)/100) * d.fn_monto AS FN_MONTO_DSCTO , cce.cd_descripcion as CambioEstatus"+
		" , ce.ct_cambio_motivo, cce.ic_cambio_estatus, ce.fn_monto_anterior "+
		" , (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as nombreIf "+
		" , d.cs_dscto_especial "+
		" , decode(d.cs_dscto_especial,'D',i2.cg_razon_social,'I',i2.cg_razon_social,'') as beneficiario "+
		" , decode(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'-1') as fn_porc_beneficiario "+
		" , decode(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'-1') as RECIBIR_BENEF "+
		" , TO_CHAR(ce.df_fecha_venc_anterior,'dd/mm/yyyy') as fechaVencAnt "+
		" ,ce.dc_fecha_cambio as fechaCambio, ce.FN_MONTO_NUEVO "+
		" ,d.ic_documento, ce.cg_nombre_usuario"+// FODEA 002 - 2009
		" ,ctf.cg_nombre nombreFactoraje "+//MODIFICACION FODEA 042-2009 FVR
		", ma.cg_razon_social as NOMBREMANDANTE "+
		", d.ic_epo as ic_epo "+
		" , NVL (cpe.cs_pub_firma_manc, 'N') AS pub_firma_manc "+
		" from com_documento d, comcat_moneda m "+
		" , comcat_epo e, comcat_pyme p, comcat_cambio_estatus cce"+
		" , comhis_cambio_estatus ce, comvis_aforo cv, comcat_if I, comcat_if i2 "+
		" , comcat_tipo_factoraje ctf "+//MODIFICACION FODEA 042-2009 FVR
		" , COMCAT_MANDANTE ma " +
		" , comrel_producto_epo cpe"+
		" where  d.ic_pyme=p.ic_pyme"+
		" and d.ic_epo=e.ic_epo"+
		" and d.ic_epo = cv.ic_epo "+
		" and d.ic_moneda=m.ic_moneda"+
		" and d.ic_documento = ce.ic_documento"+
		" and d.cs_dscto_especial = ctf.cc_tipo_factoraje"+//MODIFICACION FODEA 042-2009 FVR
		" and ce.ic_cambio_estatus=cce.ic_cambio_estatus"+
		" AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "+
		" AND cpe.ic_epo =d.ic_epo "+
		" AND cpe.ic_producto_nafin = 1 ");
		/*if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("S") && !ic_epo.equals("") ){
			qrySentencia.append(" and  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
		} else if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("N") &&  !ic_epo.equals("")){
			qrySentencia.append(" and   ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32) ");
		} else if (ic_cambio_estatus.equals("") &&  ic_epo.equals("")){
			qrySentencia.append("  and ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
		} else if (!ic_cambio_estatus.equals("")){
			qrySentencia.append(" and  ce.ic_cambio_estatus = "+ic_cambio_estatus);
		}*/

		qrySentencia.append( " and D.ic_if = I.ic_if(+) "+//FODEA 002 - 2009
		" and d.ic_beneficiario = i2.ic_if (+)"+
		" AND ( ");

		for (int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			conditions.add(new Long(lItem.get(0).toString()));
			conditions.add(new String(lItem.get(1).toString()));
			if (i >= 1){
				qrySentencia.append(" OR " );
			}
			qrySentencia.append(" ( D.ic_documento = ? AND CE.dc_fecha_cambio = TO_DATE(?,'ddmmyyyyhh24miss') ) ");
		}

		qrySentencia.append(") order by d.ig_numero_docto,fechaCambio ");
		log.info("qrySentencia: " + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getDocumentSummaryQueryForIds (S)");
		return qrySentencia.toString();
	}

	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile (E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		try{
			qrySentencia.append(
				" select (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO"+
				" , (decode (p.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+
				" , d.ig_numero_docto , TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"+
				" , TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto"+
				" , TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') as dc_fecha_cambio"+
				" , d.ic_moneda, m.cd_nombre, d.fn_monto, decode(d.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl) as fn_aforo"+
				" , (decode(d.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl)/100) * d.fn_monto AS FN_MONTO_DSCTO , cce.cd_descripcion as CambioEstatus"+
				" , ce.ct_cambio_motivo, cce.ic_cambio_estatus, ce.fn_monto_anterior "+
				" , (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as nombreIf "+
				" , d.cs_dscto_especial "+
				" , decode(d.cs_dscto_especial,'D',i2.cg_razon_social,'I',i2.cg_razon_social,'') as beneficiario "+
				" , decode(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'') as fn_porc_beneficiario "+
				" , decode(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'') as RECIBIR_BENEF "+
				" , TO_CHAR(ce.df_fecha_venc_anterior,'dd/mm/yyyy') as fechaVencAnt, ce.FN_MONTO_NUEVO "+
				" , d.ic_documento, ce.cg_nombre_usuario"+	//FODEA 002 - 2009
				" , ctf.cg_nombre nombreFactoraje "+//MODIFICACION FODEA 042-2009 FVR
				", ma.cg_razon_social as NOMBREMANDANTE "+
				", d.ic_epo as ic_epo "+
				" from com_documento d, comcat_moneda m "+
				" , comcat_epo e, comcat_pyme p, comcat_cambio_estatus cce"+
				" , comhis_cambio_estatus ce, comvis_aforo cv, comcat_if I, comcat_if i2, "+
				" comrel_producto_epo pe " +
				" ,comcat_tipo_factoraje ctf "+//MODIFICACION FODEA 042-2009 FVR
				" , COMCAT_MANDANTE ma " +
				" where  d.ic_pyme=p.ic_pyme"+
				" and d.ic_epo=e.ic_epo"+
				" and d.ic_epo = pe.ic_epo " +
				" and pe.ic_producto_nafin = 1 " +
				" AND ma.IC_MANDANTE(+) = d.IC_MANDANTE " +
				(!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") + 
				//" and (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
				" and d.ic_epo = cv.ic_epo "+
				" and d.ic_moneda=m.ic_moneda"+
				" and d.ic_documento = ce.ic_documento"+
				" and d.cs_dscto_especial = ctf.cc_tipo_factoraje"+//MODIFICACION FODEA 042-2009 FVR
				" and ce.ic_cambio_estatus=cce.ic_cambio_estatus");
				
				if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("S") && !ic_epo.equals("") ){
					qrySentencia.append(" and  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
				}else if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("N") &&  !ic_epo.equals("")){
					qrySentencia.append(" and ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32) ");
				}else if (ic_cambio_estatus.equals("") &&  ic_epo.equals("")){
					qrySentencia.append(" and ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
				}else if (!ic_cambio_estatus.equals("")){
					qrySentencia.append(" and ce.ic_cambio_estatus = ? ");
					conditions.add(ic_cambio_estatus);
				}

				qrySentencia.append(" and D.ic_if = I.ic_if(+) "+//FODEA 002 - 2009
				" and d.ic_beneficiario = i2.ic_if (+)");

			if (!ic_epo.equals("")){
				qrySentencia.append(" and d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if (!ic_pyme.equals("")){
				qrySentencia.append(" and d.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!ic_moneda.equals("")){
				qrySentencia.append(" and d.ic_moneda = ? ");
				conditions.add(ic_moneda);
			}
			if (!ig_numero_docto.equals("")){
				qrySentencia.append(" and d.ig_numero_docto = ? ");
				conditions.add(ig_numero_docto.trim());
			}
			if(!fn_montoMin.equals("")){
				if(!fn_montoMax.equals("")){
					qrySentencia.append(" and d.fn_monto between ? and ? ");
					conditions.add(fn_montoMin);
					conditions.add(fn_montoMax);
				}
			}
			if(!df_fecha_doctoMin.equals("")){
				if(!df_fecha_doctoMax.equals("")){
					qrySentencia.append(" and d.df_fecha_docto >= TO_DATE(?,'dd/mm/yyyy') and d.df_fecha_docto < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_doctoMin);
					conditions.add(df_fecha_doctoMax);
				}
			}
			if (!df_fecha_vencMin.equals("")){
				if(!df_fecha_vencMax.equals("")){
					qrySentencia.append(" and d.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') and d.df_fecha_venc  < TO_DATE(?,'dd/mm/yyyy') + 1 ");
					conditions.add(df_fecha_vencMin);
					conditions.add(df_fecha_vencMax);
				}
			}
			if(!dc_fecha_cambioMin.equals("")){
				if(!dc_fecha_cambioMax.equals("")){
					qrySentencia.append(" and ce.dc_fecha_cambio >= TO_DATE(?,'dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE(?,'dd/mm/yyyy') + 1 ");
					conditions.add(dc_fecha_cambioMin);
					conditions.add(dc_fecha_cambioMax);
				}
			}
			if(!tipoFactoraje.equals("")){
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL= ? ");
				conditions.add(tipoFactoraje);
			}
			if("".equals(tipoFactoraje)&&"".equals(ig_numero_docto)&&"".equals(ic_cambio_estatus)&&"".equals(ic_epo)&&"".equals(ic_pyme)&&"".equals(ic_moneda)&&"".equals(df_fecha_vencMin)&&"".equals(dc_fecha_cambioMin)&&"".equals(fn_montoMin))
				qrySentencia.append(" and ce.dc_fecha_cambio >= TRUNC(sysdate) and ce.dc_fecha_cambio < TRUNC(sysdate)+1 ");

			qrySentencia.append(" order by d.ig_numero_docto, ce.dc_fecha_cambio");

			log.info("qrySentencia: " + qrySentencia);

		} catch(Exception e){
			log.warn("CambioEstatusNafinDE::getDocumentQueryFileException. " + e);
		}
		log.info("getDocumentQueryFile (S)");
		return qrySentencia.toString();
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){

		log.info("crearPageCustomFile (E)");
		String nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
		boolean SIN_COMAS = false;
		int numRegistros = 0;
		String colsp = "";
		
		String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual   = fechaActual.substring(0,2);
		String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual  = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		"",
		(String)session.getAttribute("sesExterno"),
		(String)session.getAttribute("strNombre"),
		(String)session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
		
		pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
		//	De despliegue
		
		String nombreEpo;
		String nombrePyme;
		String numeroDocumento;
		String fechaVencimiento;
		String fechaDocumento;
		String fechaCambioEstatus;
		String monedaNombre;
		int monedaClave;
		String monto;
		double dblPorcentaje = 0;
		double dblRecurso = 0;
		String montoDescuento;
		String cambioEstatus;
		String cambioMotivo;
		int IcCambioEstatus = 0;
		String FnMontoAnterior = "";
		String nombreTipoFactoraje="";
		String NombreIf="";
		String beneficiario = "";
		double porciento_beneficiario = 0;
		double importe_a_recibir_beneficiario = 0;
		String fechaVencAnt = "";
		//============================================================================>> FODEA 002 - 2009 (I)
		String nombreUsuario = "";
		//============================================================================>> FODEA 002 - 2009 (F)
		String nombreRealFactoraje = "";
		String NombreMandante = "";
		
		try {
		while (rs.next()) 
			{
				numRegistros++;
				if(numRegistros==1){	//Muestra el encabezado de resultados{
				pdfDoc.setLTable(12, 100);
				pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre del proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. de documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setLCell("Fecha de Cambio de Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Documento","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setLCell("Porcentaje Descuento","celda01",ComunesPDF.CENTER);
				
				
				pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Recurso Garantia","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cambio estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Usuario","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Anterior","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento Anterior","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Beneficiario","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("% Beneficiario","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Importe a Recibir","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Mandante","celda01",ComunesPDF.CENTER);
				

				} //fin del encabezado de resultados
				
				nombreEpo = (rs.getString("NOMBREEPO") == null) ? "" : rs.getString("NOMBREEPO");
				nombrePyme = (rs.getString("NOMBREPYME") == null) ? "" : rs.getString("NOMBREPYME");
				numeroDocumento = (rs.getString("IG_NUMERO_DOCTO") == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
				fechaVencimiento = (rs.getString("DF_FECHA_VENC") == null) ? "" : rs.getString("DF_FECHA_VENC");
				fechaDocumento = (rs.getString("DF_FECHA_DOCTO") == null) ? "" : rs.getString("DF_FECHA_DOCTO");
				fechaCambioEstatus = (rs.getString("DC_FECHA_CAMBIO") == null) ? "" : rs.getString("DC_FECHA_CAMBIO");
				monedaClave = Integer.parseInt(rs.getString("IC_MONEDA"));
				monedaNombre = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
				monto = (rs.getString("FN_MONTO") == null) ? "0.00" : rs.getString("FN_MONTO");
				dblPorcentaje = Double.parseDouble(rs.getString("FN_AFORO"));
				montoDescuento = (rs.getString("FN_MONTO_DSCTO") == null) ? "0.00" : rs.getString("FN_MONTO_DSCTO");
				cambioEstatus = (rs.getString("CAMBIOESTATUS") == null) ? "" : rs.getString("CAMBIOESTATUS");
				cambioMotivo = (rs.getString("CT_CAMBIO_MOTIVO") == null) ? "" : rs.getString("CT_CAMBIO_MOTIVO");
				IcCambioEstatus = Integer.parseInt(rs.getString("IC_CAMBIO_ESTATUS"));
				FnMontoAnterior = (rs.getString("FN_MONTO_ANTERIOR")==null)?"":rs.getString("FN_MONTO_ANTERIOR").trim();
				beneficiario = (rs.getString("beneficiario")==null)?"":rs.getString("beneficiario").trim();
				porciento_beneficiario = Double.parseDouble(rs.getString("fn_porc_beneficiario"));
				importe_a_recibir_beneficiario = Double.parseDouble(rs.getString("RECIBIR_BENEF"));
				fechaVencAnt = (rs.getString("fechaVencAnt")==null)?"":rs.getString("fechaVencAnt");
				NombreMandante= (rs.getString("NOMBREMANDANTE")==null)?"":rs.getString("NOMBREMANDANTE").trim();
							
				if (ic_cambio_estatus.equals("4") || ic_cambio_estatus.equals("5") ||
					ic_cambio_estatus.equals("6"))
						montoDescuento = new Double(new Double(monto).doubleValue() * (dblPorcentaje / 100)).toString();
				if(IcCambioEstatus==28) {
					cambioEstatus += " "+fechaVencimiento;					
					monto = (rs.getString("FN_MONTO_NUEVO")==null)?"":rs.getString("FN_MONTO_NUEVO").trim();
					montoDescuento = new Double(Double.parseDouble(monto) * (dblPorcentaje/100)).toString();

				}
				// Factoraje Vencido
				nombreTipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
				nombreRealFactoraje = (rs.getString("nombreFactoraje")==null)?"":rs.getString("nombreFactoraje");
				NombreIf = "";
				if(nombreTipoFactoraje.equals("V") || nombreTipoFactoraje.equals("I")) {//MODIFICACION FODEA 042-2009
					NombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
					montoDescuento = monto;
					dblPorcentaje = 100;
				}
				if(nombreTipoFactoraje.equals("A") ) {//MODIFICACION FODEA 042-2009
					NombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
				}
				if(IcCambioEstatus==29){ //estatus 29=Programado a Negociable foda 025-2005
					NombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
					dblPorcentaje = Double.parseDouble(rs.getString("FN_AFORO"));
					montoDescuento = new Double(new Double(monto).doubleValue() * (dblPorcentaje / 100)).toString();	
				}
//============================================================================>> FODEA 002 - 2009 (I)
				nombreUsuario = rs.getString("CG_NOMBRE_USUARIO") == null?"":rs.getString("CG_NOMBRE_USUARIO");
//============================================================================>> FODEA 002 - 2009 (F)
//				nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal":(nombreTipoFactoraje.equals("V"))?"Vencido":(nombreTipoFactoraje.equals("D"))?"Distribuido":"";
				if("N".equals(nombreTipoFactoraje))
					nombreTipoFactoraje = "Normal";
				else if("V".equals(nombreTipoFactoraje))
					nombreTipoFactoraje = "Vencido";
				else if("D".equals(nombreTipoFactoraje))
					nombreTipoFactoraje = "Distribuido";
				else if("C".equals(nombreTipoFactoraje))
					nombreTipoFactoraje = "Nota de Cr�dito";
				else if("A".equals(nombreTipoFactoraje))
					nombreTipoFactoraje = "Autom�tico";
	
				dblRecurso = new Double(monto).doubleValue() - new Double(montoDescuento).doubleValue();
				
				//System.out.println("cambioMotivo>>>>>>>>>>>>"+cambioMotivo);
				cambioMotivo=cambioMotivo.replaceAll("\\n"," ");
				cambioMotivo=cambioMotivo.replaceAll("\\r"," ");
				
				
				//Fodea 060-2010
				ic_epo = (rs.getString("ic_epo")==null)?"":rs.getString("ic_epo");
				ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
				
				firmaMancomunada= BeanParamDscto.getOperaFirmaMancomunada(ic_epo, 1);
			
			
			if( IcCambioEstatus != 8 ||   IcCambioEstatus != 28  ||  (IcCambioEstatus != 37 &&  IcCambioEstatus != 38 &&  IcCambioEstatus != 39 &&  IcCambioEstatus != 40 && firmaMancomunada.equals("N") ) )  { /* estatus "Modificacion de Montos y Fechas" */ 
	
					
					pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreEpo,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombrePyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(NombreIf,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaCambioEstatus,"formas",ComunesPDF.CENTER);
					
					pdfDoc.setLCell(monedaNombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreRealFactoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2,SIN_COMAS),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+"%","formas",ComunesPDF.CENTER);
					
					pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(dblRecurso,2,true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS),"formas",ComunesPDF.RIGHT);
					
					pdfDoc.setLCell(cambioEstatus,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(cambioMotivo,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombreUsuario,"formas",ComunesPDF.CENTER);
					
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(FnMontoAnterior,2,true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(fechaVencAnt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.CENTER);

						if(0 < porciento_beneficiario) 
							pdfDoc.setLCell(Comunes.formatoDecimal(porciento_beneficiario,2,false)+"%","formas",ComunesPDF.CENTER);
							else 
							pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						if(0 < importe_a_recibir_beneficiario) 
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false),"formas",ComunesPDF.RIGHT);
								
								else 
								pdfDoc.setLCell("","formas",ComunesPDF.CENTER);

						pdfDoc.setLCell(NombreMandante,"formas",ComunesPDF.CENTER);
					
					}		else if( IcCambioEstatus != 8 || IcCambioEstatus != 28   && !firmaMancomunada.equals("N") )  { /* estatus "Modificacion de Montos y Fechas" */ 
						
					pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreEpo,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombrePyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(NombreIf,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaCambioEstatus,"formas",ComunesPDF.CENTER);
					
					pdfDoc.setLCell(monedaNombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreRealFactoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2,SIN_COMAS),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+"%","formas",ComunesPDF.CENTER);
					
					pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(dblRecurso,2,true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS),"formas",ComunesPDF.RIGHT);
					
					pdfDoc.setLCell(cambioEstatus,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(cambioMotivo,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombreUsuario,"formas",ComunesPDF.LEFT);
					
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(FnMontoAnterior,2,true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(fechaVencAnt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.CENTER);
						

						
						if(0 < porciento_beneficiario) 
							pdfDoc.setLCell(Comunes.formatoDecimal(porciento_beneficiario,2,true)+"%","formas",ComunesPDF.CENTER);
							else 
							pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						if(0 < importe_a_recibir_beneficiario) 
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false),"formas",ComunesPDF.RIGHT);
								else 
								pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(NombreMandante,"formas",ComunesPDF.CENTER);
					}
			} //fin del while

			pdfDoc.addLTable();
			pdfDoc.endDocument();
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} 
		log.info("crearPageCustomFile (S)");
		return nombreArchivo;
	}

/**
 * Genera el archivo CSV o PDF seg�n el tipo que se le indique.
 * En el caso del PDF, el archivo generado no comtempla paginaci�n, imprime todos los registros que trae la consulta.
 * @param request
 * @param rs
 * @param path
 * @param tipo: CSV o PDF
 * @return nombre del archivo
 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");
		HttpSession  session          = request.getSession();
		CreaArchivo  archivo          = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();

		String  nombreArchivo = "";
		boolean SIN_COMAS     = false;
		int     numRegistros  = 0;

		String nombreEpo;
		String nombrePyme;
		String numeroDocumento;
		String fechaVencimiento;
		String fechaDocumento;
		String fechaCambioEstatus;
		String monedaNombre;
		int monedaClave;
		String monto;
		double dblPorcentaje = 0;
		double dblRecurso = 0;
		String montoDescuento;
		String cambioEstatus;
		String cambioMotivo;
		int IcCambioEstatus = 0;
		String FnMontoAnterior = "";
		String nombreTipoFactoraje="";
		String NombreIf="";
		String beneficiario = "";
		double porciento_beneficiario = 0;
		double importe_a_recibir_beneficiario = 0;
		String fechaVencAnt = "";
		//============================================================================>> FODEA 002 - 2009 (I)
		String nombreUsuario = "";
		//============================================================================>> FODEA 002 - 2009 (F)
		String nombreRealFactoraje = "";
		String NombreMandante = "";

		if(tipo.equals("CSV")){

			try{
			while (rs.next()){
				numRegistros++;
				if(numRegistros==1){ //Muestra el encabezado de resultados
					contenidoArchivo.append("EPO,Nombre del proveedor,Nombre IF,No. de documento,Fecha de Vencimiento,Fecha de Documento,Fecha de Cambio de Estatus,Moneda,Tipo Factoraje,Monto Documento,Monto a descontar,Tipo de cambio estatus,Causa,Usuario,"); //FODEA 002 - 2009
					contenidoArchivo.append("Porcentaje Descuento,Recurso Garantia,Monto Anterior,Fecha de Vencimiento Anterior,Beneficiario,% Beneficiario, Importe a Recibir ");
					contenidoArchivo.append(" , Mandante");
					contenidoArchivo.append(", \n");
				} //fin del encabezado de resultados

				nombreEpo                      = (rs.getString("NOMBREEPO")         == null) ? "" : rs.getString("NOMBREEPO");
				nombrePyme                     = (rs.getString("NOMBREPYME")        == null) ? "" : rs.getString("NOMBREPYME");
				numeroDocumento                = (rs.getString("IG_NUMERO_DOCTO")   == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
				fechaVencimiento               = (rs.getString("DF_FECHA_VENC")     == null) ? "" : rs.getString("DF_FECHA_VENC");
				fechaDocumento                 = (rs.getString("DF_FECHA_DOCTO")    == null) ? "" : rs.getString("DF_FECHA_DOCTO");
				fechaCambioEstatus             = (rs.getString("DC_FECHA_CAMBIO")   == null) ? "" : rs.getString("DC_FECHA_CAMBIO");
				monedaClave                    = rs.getInt("IC_MONEDA");
				monedaNombre                   = (rs.getString("CD_NOMBRE")         == null) ? "" : rs.getString("CD_NOMBRE");
				monto                          = (rs.getString("FN_MONTO")          == null) ? "0.00" : rs.getString("FN_MONTO");
				dblPorcentaje                  = rs.getDouble("FN_AFORO");
				montoDescuento                 = (rs.getString("FN_MONTO_DSCTO")    == null) ? "0.00" : rs.getString("FN_MONTO_DSCTO");
				cambioEstatus                  = (rs.getString("CAMBIOESTATUS")     == null) ? "" : rs.getString("CAMBIOESTATUS");
				cambioMotivo                   = (rs.getString("CT_CAMBIO_MOTIVO")  == null) ? "" : rs.getString("CT_CAMBIO_MOTIVO");
				IcCambioEstatus                = rs.getInt("IC_CAMBIO_ESTATUS");
				FnMontoAnterior                = (rs.getString("FN_MONTO_ANTERIOR") == null) ? "" : rs.getString("FN_MONTO_ANTERIOR").trim();
				beneficiario                   = (rs.getString("beneficiario")      == null) ? "" : rs.getString("beneficiario").trim();
				porciento_beneficiario         = rs.getDouble("fn_porc_beneficiario");
				importe_a_recibir_beneficiario = rs.getDouble("RECIBIR_BENEF");
				fechaVencAnt                   = (rs.getString("fechaVencAnt")      == null) ? "" : rs.getString("fechaVencAnt");
				NombreMandante                 = (rs.getString("NOMBREMANDANTE")    == null) ? "" : rs.getString("NOMBREMANDANTE").trim();

				if (ic_cambio_estatus.equals("4") || ic_cambio_estatus.equals("5") || ic_cambio_estatus.equals("6")) 
					montoDescuento = new Double(new Double(monto).doubleValue() * (dblPorcentaje / 100)).toString();
				if(IcCambioEstatus == 28){
					cambioEstatus += " "+fechaVencimiento;
					monto = (rs.getString("FN_MONTO_NUEVO")==null)?"":rs.getString("FN_MONTO_NUEVO").trim();
					montoDescuento = new Double(Double.parseDouble(monto) * (dblPorcentaje/100)).toString();
				}
				// Factoraje Vencido
				nombreTipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL") ==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
				nombreRealFactoraje = (rs.getString("nombreFactoraje")   ==null)?"":rs.getString("nombreFactoraje");
				NombreIf = "";
				if(nombreTipoFactoraje.equals("V") || nombreTipoFactoraje.equals("I")) {//MODIFICACION FODEA 042-2009
					NombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
					montoDescuento = monto;
					dblPorcentaje = 100;
				}
				if(nombreTipoFactoraje.equals("A") ) {//MODIFICACION FODEA 042-2009
					NombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
				}
				if(IcCambioEstatus==29){ //estatus 29=Programado a Negociable foda 025-2005
					NombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
					dblPorcentaje = rs.getDouble("FN_AFORO");
					montoDescuento = new Double(new Double(monto).doubleValue() * (dblPorcentaje / 100)).toString();
				}
//============================================================================>> FODEA 002 - 2009 (I)
				nombreUsuario = rs.getString("CG_NOMBRE_USUARIO") == null?"":rs.getString("CG_NOMBRE_USUARIO");
//============================================================================>> FODEA 002 - 2009 (F)
//			nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal":(nombreTipoFactoraje.equals("V"))?"Vencido":(nombreTipoFactoraje.equals("D"))?"Distribuido":"";
				if("N".equals(nombreTipoFactoraje))
					nombreTipoFactoraje = "Normal";
				else if("V".equals(nombreTipoFactoraje))
					nombreTipoFactoraje = "Vencido";
				else if("D".equals(nombreTipoFactoraje))
					nombreTipoFactoraje = "Distribuido";
				else if("C".equals(nombreTipoFactoraje))
					nombreTipoFactoraje = "Nota de Cr�dito";
				else if("A".equals(nombreTipoFactoraje))
					nombreTipoFactoraje = "Autom�tico";

				dblRecurso = new Double(monto).doubleValue() - new Double(montoDescuento).doubleValue();

				//System.out.println("cambioMotivo>>>>>>>>>>>>"+cambioMotivo);
				cambioMotivo=cambioMotivo.replaceAll("\\n"," ");
				cambioMotivo=cambioMotivo.replaceAll("\\r"," ");

				//Fodea 060-2010
				ic_epo = (rs.getString("ic_epo")==null)?"":rs.getString("ic_epo");
				ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

				firmaMancomunada= BeanParamDscto.getOperaFirmaMancomunada(ic_epo, 1);

				if(IcCambioEstatus != 8 || IcCambioEstatus != 28 || (IcCambioEstatus != 37 && IcCambioEstatus != 38 && IcCambioEstatus != 39 && IcCambioEstatus != 40 && firmaMancomunada.equals("N"))){ /* estatus "Modificacion de Montos y Fechas" */

					contenidoArchivo.append( nombreEpo.replace(',',' ')+
					","+nombrePyme.replace(',',' ')+","+NombreIf.replace(',',' ')+","+numeroDocumento+
					","+fechaVencimiento+","+fechaDocumento+","+fechaCambioEstatus+ //El fin de agregar el simbolo "'" es que las fechas no cambien cuando se abre en Excel
					","+monedaNombre+","+nombreRealFactoraje+",$"+Comunes.formatoDecimal(monto,2,SIN_COMAS)+

					",$"+Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS)+","+cambioEstatus+","+cambioMotivo+","+nombreUsuario+","+Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+","+dblRecurso+","+FnMontoAnterior+","+fechaVencAnt+","+beneficiario.replace(',',' '));//FODEA 002 - 2009

					if(0 != porciento_beneficiario) 
						contenidoArchivo.append(","+porciento_beneficiario);
					else
						contenidoArchivo.append(", ");
					if(0 != importe_a_recibir_beneficiario) 
						contenidoArchivo.append(","+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false)); 
					else
						contenidoArchivo.append(", ");

					contenidoArchivo.append(","+NombreMandante+",");

				} else if( IcCambioEstatus != 8 || IcCambioEstatus != 28   && !firmaMancomunada.equals("N")){ /* estatus "Modificacion de Montos y Fechas" */ 

					contenidoArchivo.append( nombreEpo.replace(',',' ')+
					","+nombrePyme.replace(',',' ')+","+NombreIf.replace(',',' ')+","+numeroDocumento+
					","+fechaVencimiento+","+fechaDocumento+","+fechaCambioEstatus+ //El fin de agregar el simbolo "'" es que las fechas no cambien cuando se abre en Excel
					","+monedaNombre+","+nombreRealFactoraje+",$"+Comunes.formatoDecimal(monto,2,SIN_COMAS)+

					",$"+Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS)+","+cambioEstatus+","+cambioMotivo+","+nombreUsuario+","+Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+","+dblRecurso+","+FnMontoAnterior+","+fechaVencAnt+","+beneficiario.replace(',',' '));//FODEA 002 - 2009

					if(0 != porciento_beneficiario) 
						contenidoArchivo.append(","+porciento_beneficiario);
					else
						contenidoArchivo.append(", ");
					if(0 != importe_a_recibir_beneficiario) 
						contenidoArchivo.append(","+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false)); 
					else
						contenidoArchivo.append(", ");

					contenidoArchivo.append(","+NombreMandante+",");

				}

				contenidoArchivo.append(", \n");

				if(numRegistros == 1000){
					numRegistros = 0;
					archivo.make(contenidoArchivo.toString(), path, ".csv");
					contenidoArchivo = new StringBuffer();//Limpio 
				}

			} //fin del while

			archivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = archivo.nombre;

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo ", e);
			}

		} else if(tipo.equals("PDF")){

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
			
			try {
				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				"",
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);

				while (rs.next()) {
					numRegistros++;
					if(numRegistros==1){ //Muestra el encabezado de resultados{
						pdfDoc.setLTable(12, 100);
						pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Nombre del proveedor","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Nombre IF","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. de documento","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);

						pdfDoc.setLCell("Fecha de Cambio de Estatus","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Documento","celda01",ComunesPDF.CENTER);

						pdfDoc.setLCell("Porcentaje Descuento","celda01",ComunesPDF.CENTER);

						pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Recurso Garantia","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto a descontar","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Tipo de cambio estatus","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Causa","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Usuario","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Anterior","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha de Vencimiento Anterior","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("% Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Importe a Recibir","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Mandante","celda01",ComunesPDF.CENTER);
						pdfDoc.setLHeaders();
					} //fin del encabezado de resultados

					nombreEpo                      = (rs.getString("NOMBREEPO")        == null) ? ""     : rs.getString("NOMBREEPO");
					nombrePyme                     = (rs.getString("NOMBREPYME")       == null) ? ""     : rs.getString("NOMBREPYME");
					numeroDocumento                = (rs.getString("IG_NUMERO_DOCTO")  == null) ? ""     : rs.getString("IG_NUMERO_DOCTO");
					fechaVencimiento               = (rs.getString("DF_FECHA_VENC")    == null) ? ""     : rs.getString("DF_FECHA_VENC");
					fechaDocumento                 = (rs.getString("DF_FECHA_DOCTO")   == null) ? ""     : rs.getString("DF_FECHA_DOCTO");
					fechaCambioEstatus             = (rs.getString("DC_FECHA_CAMBIO")  == null) ? ""     : rs.getString("DC_FECHA_CAMBIO");
					monedaNombre                   = (rs.getString("CD_NOMBRE")        == null) ? ""     : rs.getString("CD_NOMBRE");
					monto                          = (rs.getString("FN_MONTO")         == null) ? "0.00" : rs.getString("FN_MONTO");
					montoDescuento                 = (rs.getString("FN_MONTO_DSCTO")   == null) ? "0.00" : rs.getString("FN_MONTO_DSCTO");
					cambioEstatus                  = (rs.getString("CAMBIOESTATUS")    == null) ? ""     : rs.getString("CAMBIOESTATUS");
					cambioMotivo                   = (rs.getString("CT_CAMBIO_MOTIVO") == null) ? ""     : rs.getString("CT_CAMBIO_MOTIVO");
					FnMontoAnterior                = (rs.getString("FN_MONTO_ANTERIOR")== null) ? ""     :rs.getString("FN_MONTO_ANTERIOR").trim();
					beneficiario                   = (rs.getString("beneficiario")     == null) ? ""     :rs.getString("beneficiario").trim();
					fechaVencAnt                   = (rs.getString("fechaVencAnt")     == null) ? ""     :rs.getString("fechaVencAnt");
					NombreMandante                 = (rs.getString("NOMBREMANDANTE")   == null) ? ""     :rs.getString("NOMBREMANDANTE").trim();

					monedaClave                    = Integer.parseInt(  (rs.getString("IC_MONEDA")            == null) ? "0"   : rs.getString("IC_MONEDA"));
					IcCambioEstatus                = Integer.parseInt(  (rs.getString("IC_CAMBIO_ESTATUS")    == null) ? "0"   : rs.getString("IC_CAMBIO_ESTATUS"));
					dblPorcentaje                  = Double.parseDouble((rs.getString("FN_AFORO")             == null) ? "0.0" : rs.getString("FN_AFORO"));
					porciento_beneficiario         = Double.parseDouble((rs.getString("fn_porc_beneficiario") == null) ? "0.0" : rs.getString("fn_porc_beneficiario"));
					importe_a_recibir_beneficiario = Double.parseDouble((rs.getString("RECIBIR_BENEF")        == null) ? "0.0" : rs.getString("RECIBIR_BENEF"));

					if (ic_cambio_estatus.equals("4") || ic_cambio_estatus.equals("5") ||
						ic_cambio_estatus.equals("6"))
						montoDescuento = new Double(new Double(monto).doubleValue() * (dblPorcentaje / 100)).toString();
					if(IcCambioEstatus==28) {
						cambioEstatus += " "+fechaVencimiento;             
						monto = (rs.getString("FN_MONTO_NUEVO")==null)?"0":rs.getString("FN_MONTO_NUEVO").trim();
						montoDescuento = new Double(Double.parseDouble(monto) * (dblPorcentaje/100)).toString();
					}
					// Factoraje Vencido
					nombreTipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
					nombreRealFactoraje = (rs.getString("nombreFactoraje")==null)?"":rs.getString("nombreFactoraje");
					NombreIf = "";
					if(nombreTipoFactoraje.equals("V") || nombreTipoFactoraje.equals("I")) {//MODIFICACION FODEA 042-2009
						NombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
						montoDescuento = monto;
						dblPorcentaje = 100;
					}
					if(nombreTipoFactoraje.equals("A") ) {//MODIFICACION FODEA 042-2009
						NombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
					}
					if(IcCambioEstatus==29){ //estatus 29=Programado a Negociable foda 025-2005
						NombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
						dblPorcentaje = Double.parseDouble(rs.getString("FN_AFORO"));
						montoDescuento = new Double(new Double(monto).doubleValue() * (dblPorcentaje / 100)).toString();   
					}
					//============================================================================>> FODEA 002 - 2009 (I)
					nombreUsuario = rs.getString("CG_NOMBRE_USUARIO") == null?"":rs.getString("CG_NOMBRE_USUARIO");
					//============================================================================>> FODEA 002 - 2009 (F)
					//          nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal":(nombreTipoFactoraje.equals("V"))?"Vencido":(nombreTipoFactoraje.equals("D"))?"Distribuido":"";
					if("N".equals(nombreTipoFactoraje))
						nombreTipoFactoraje = "Normal";
					else if("V".equals(nombreTipoFactoraje))
						nombreTipoFactoraje = "Vencido";
					else if("D".equals(nombreTipoFactoraje))
						nombreTipoFactoraje = "Distribuido";
					else if("C".equals(nombreTipoFactoraje))
						nombreTipoFactoraje = "Nota de Cr�dito";
					else if("A".equals(nombreTipoFactoraje))
						nombreTipoFactoraje = "Autom�tico";

					dblRecurso = new Double(monto).doubleValue() - new Double(montoDescuento).doubleValue();

					//System.out.println("cambioMotivo>>>>>>>>>>>>"+cambioMotivo);
					cambioMotivo=cambioMotivo.replaceAll("\\n"," ");
					cambioMotivo=cambioMotivo.replaceAll("\\r"," ");

					//Fodea 060-2010
					ic_epo = (rs.getString("ic_epo")==null)?"":rs.getString("ic_epo");
					ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);

					firmaMancomunada= BeanParamDscto.getOperaFirmaMancomunada(ic_epo, 1);

				if( IcCambioEstatus != 8 ||   IcCambioEstatus != 28  ||  (IcCambioEstatus != 37 &&  IcCambioEstatus != 38 &&  IcCambioEstatus != 39 &&  IcCambioEstatus != 40 && firmaMancomunada.equals("N") ) )  { /* estatus "Modificacion de Montos y Fechas" */

					pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreEpo,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombrePyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(NombreIf,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaCambioEstatus,"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell(monedaNombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreRealFactoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2,SIN_COMAS),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+"%","formas",ComunesPDF.CENTER);

					pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(dblRecurso,2,true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS),"formas",ComunesPDF.RIGHT);

					pdfDoc.setLCell(cambioEstatus,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(cambioMotivo,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombreUsuario,"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell("$"+Comunes.formatoDecimal(FnMontoAnterior,2,true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(fechaVencAnt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.CENTER);

					if(0 < porciento_beneficiario)
						pdfDoc.setLCell(Comunes.formatoDecimal(porciento_beneficiario,2,false)+"%","formas",ComunesPDF.CENTER);
					else
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					if(0 < importe_a_recibir_beneficiario) 
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false),"formas",ComunesPDF.RIGHT);
					else
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);

					pdfDoc.setLCell(NombreMandante,"formas",ComunesPDF.CENTER);

				} else if( IcCambioEstatus != 8 || IcCambioEstatus != 28   && !firmaMancomunada.equals("N") )  { /* estatus "Modificacion de Montos y Fechas" */ 

					pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreEpo,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombrePyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(NombreIf,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaCambioEstatus,"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell(monedaNombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreRealFactoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2,SIN_COMAS),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+"%","formas",ComunesPDF.CENTER);

					pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(dblRecurso,2,true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS),"formas",ComunesPDF.RIGHT);

					pdfDoc.setLCell(cambioEstatus,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(cambioMotivo,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombreUsuario,"formas",ComunesPDF.LEFT);

					pdfDoc.setLCell("$"+Comunes.formatoDecimal(FnMontoAnterior,2,true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(fechaVencAnt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.CENTER);

					if(0 < porciento_beneficiario) 
						pdfDoc.setLCell(Comunes.formatoDecimal(porciento_beneficiario,2,true)+"%","formas",ComunesPDF.CENTER);
					else 
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					if(0 < importe_a_recibir_beneficiario) 
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false),"formas",ComunesPDF.RIGHT);
					else 
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(NombreMandante,"formas",ComunesPDF.CENTER);
					}
				} //fin del while

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} 

		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	

/////Version Extjs fin
  public CambioEstatusNafinDE()  {  }
	public String getAggregateCalculationQuery(HttpServletRequest request)  {
    StringBuffer qrySentencia = new StringBuffer();

    String ic_epo             = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
    String ic_pyme            = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
    String ig_numero_docto    = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto");
    String df_fecha_vencMin   = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
    String df_fecha_vencMax   = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
    String ic_moneda          = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
    String fn_montoMin        = (request.getParameter("fn_montoMin") == null) ? "" : request.getParameter("fn_montoMin");
    String fn_montoMax        = (request.getParameter("fn_montoMax") == null) ? "" : request.getParameter("fn_montoMax");
    String ic_cambio_estatus  = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
    String df_fecha_doctoMin  = (request.getParameter("df_fecha_doctoMin") == null) ? "" : request.getParameter("df_fecha_doctoMin");
    String df_fecha_doctoMax  = (request.getParameter("df_fecha_doctoMax") == null) ? "" : request.getParameter("df_fecha_doctoMax");
    String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null) ? "" : request.getParameter("dc_fecha_cambioMin");
    String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null) ? "" : request.getParameter("dc_fecha_cambioMax");
    String tipoFactoraje      = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
    String  ic_banco_fondeo   = (request.getParameter("ic_banco_fondeo") == null)? "": request.getParameter("ic_banco_fondeo");
    String  firmaMancomunada   = (request.getParameter("firmaMancomunada") == null)? "": request.getParameter("firmaMancomunada");

		System.out.println("firmaMancomunada  "+firmaMancomunada);

    try{
			qrySentencia.append(
				" select /*+use_nl (ce d m) index (ce IN_COMHIS_CAM_ESTA_02_NUK) */ "+
        "  count(1) as CambioEstatusNafinDE"+
				" ,sum(D.fn_monto) "+
				" ,sum(D.fn_monto) "+
				" ,D.ic_moneda"+
				" ,M.cd_nombre"+
				" from "+
				" comhis_cambio_estatus ce"+
				" , com_documento d "+
				" , comcat_moneda m"+
				" where");
				if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("S") && !ic_epo.equals("") ){
					qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
				}else	if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("N") &&  !ic_epo.equals("")){
					qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32) ");
				}else	if (ic_cambio_estatus.equals("") &&  ic_epo.equals("")){
					qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
			 }else	if (!ic_cambio_estatus.equals("")){
						qrySentencia.append("  ce.ic_cambio_estatus = "+ic_cambio_estatus);
				}
							 
			qrySentencia.append(" and d.ic_moneda=m.ic_moneda"+
				" and d.ic_documento = ce.ic_documento");

			if (!ic_epo.equals(""))
				qrySentencia.append(" and d.ic_epo = "+ic_epo);
			if (!ic_pyme.equals(""))
				qrySentencia.append(" and d.ic_pyme = "+ic_pyme);
			
			if (!ic_moneda.equals(""))
				qrySentencia.append(" and d.ic_moneda = "+ic_moneda);
			if (!ig_numero_docto.equals(""))
				qrySentencia.append(" and d.ig_numero_docto = '"+ig_numero_docto.trim()+"' ");
			if(!fn_montoMin.equals(""))
			{
				if(!fn_montoMax.equals(""))
					qrySentencia.append(" and d.fn_monto between "+fn_montoMin+" and "+fn_montoMax);
				else
					qrySentencia.append(" and d.fn_monto = "+fn_montoMin);
			}
			if(!df_fecha_doctoMin.equals(""))
			{
				if(!df_fecha_doctoMax.equals(""))
					qrySentencia.append(" and d.df_fecha_docto >= TO_DATE('"+df_fecha_doctoMin+"','dd/mm/yyyy') and d.df_fecha_docto < TO_DATE('"+df_fecha_doctoMax+"','dd/mm/yyyy')+1 ");
				else
					qrySentencia.append(" and d.df_fecha_docto >= TO_DATE('"+df_fecha_doctoMin+"','dd/mm/yyyy')  AND d.df_fecha_docto < TO_DATE('"+df_fecha_doctoMin+"','dd/mm/yyyy')+1 ");
			}
			if(!df_fecha_vencMin.equals(""))
			{
				if(!df_fecha_vencMax.equals(""))
					qrySentencia.append(" and d.df_fecha_venc >= TO_DATE('"+df_fecha_vencMin+"','dd/mm/yyyy') and d.df_fecha_venc  < TO_DATE('"+df_fecha_vencMax+"','dd/mm/yyyy') + 1 ");
				else
					qrySentencia.append(" and d.df_fecha_venc >= TO_DATE('"+df_fecha_vencMin+"','dd/mm/yyyy') AND d.df_fecha_venc < TO_DATE('"+df_fecha_vencMin+"','dd/mm/yyyy')+1 ");
			}
			if(!dc_fecha_cambioMin.equals(""))
			{
				if(!dc_fecha_cambioMax.equals(""))
					qrySentencia.append(" and ce.dc_fecha_cambio >= TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE('"+dc_fecha_cambioMax+"','dd/mm/yyyy') + 1 ");
			}
			if(!tipoFactoraje.equals(""))
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ");
			if("".equals(tipoFactoraje)&&"".equals(ig_numero_docto)&&"".equals(ic_cambio_estatus)&&"".equals(ic_epo)&&"".equals(ic_pyme)&&"".equals(ic_moneda)&&"".equals(df_fecha_vencMin)&&"".equals(dc_fecha_cambioMin)&&"".equals(fn_montoMin))
				qrySentencia.append(" and ce.dc_fecha_cambio >= TRUNC(sysdate) AND ce.dc_fecha_cambio < TRUNC(sysdate)+1 ");

      qrySentencia.append(" group by D.ic_moneda,M.cd_nombre");
      qrySentencia.append(" order by D.ic_moneda");
			
		 System.out.println("qrySentencia "+qrySentencia.toString());
    }catch(Exception e){
      System.out.println("CambioEstatusNafinDE::getDocumentQueryFileException "+e);
    }
    return qrySentencia.toString();
  }

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		StringBuffer qrySentencia = new StringBuffer();
		String  ic_banco_fondeo   = (request.getParameter("ic_banco_fondeo") == null)? "": request.getParameter("ic_banco_fondeo");
    String  firmaMancomunada   = (request.getParameter("firmaMancomunada") == null)? "": request.getParameter("firmaMancomunada");
	  String ic_epo             = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
    String ic_cambio_estatus             = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
  
		qrySentencia.append(
        " select (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO"+
				" , (decode (p.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+
				" , d.ig_numero_docto , TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"+
				" , TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto"+
				" , TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') as dc_fecha_cambio"+
				" , d.ic_moneda, m.cd_nombre, d.fn_monto, decode(d.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl) as fn_aforo"+
				" , (decode(d.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl)/100) * d.fn_monto AS FN_MONTO_DSCTO , cce.cd_descripcion as CambioEstatus"+
				" , ce.ct_cambio_motivo, cce.ic_cambio_estatus, ce.fn_monto_anterior "+
				" , (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as nombreIf "+
				" , d.cs_dscto_especial "+
				" , decode(d.cs_dscto_especial,'D',i2.cg_razon_social,'I',i2.cg_razon_social,'') as beneficiario "+
		    " , decode(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'') as fn_porc_beneficiario "+
		    " , decode(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'') as RECIBIR_BENEF "+
				" , TO_CHAR(ce.df_fecha_venc_anterior,'dd/mm/yyyy') as fechaVencAnt "+
        " ,ce.dc_fecha_cambio as fechaCambio, ce.FN_MONTO_NUEVO "+
				" ,d.ic_documento, ce.cg_nombre_usuario"+// FODEA 002 - 2009
				" ,ctf.cg_nombre nombreFactoraje "+//MODIFICACION FODEA 042-2009 FVR
				", ma.cg_razon_social as NOMBREMANDANTE "+
				", d.ic_epo as ic_epo "+
				
				" from com_documento d, comcat_moneda m "+
				" , comcat_epo e, comcat_pyme p, comcat_cambio_estatus cce"+
				" , comhis_cambio_estatus ce, comvis_aforo cv, comcat_if I, comcat_if i2 "+
				" , comcat_tipo_factoraje ctf "+//MODIFICACION FODEA 042-2009 FVR
				" , COMCAT_MANDANTE ma " +	
				
				" where  d.ic_pyme=p.ic_pyme"+
				" and d.ic_epo=e.ic_epo"+
				" and d.ic_epo = cv.ic_epo "+
				" and d.ic_moneda=m.ic_moneda"+
				" and d.ic_documento = ce.ic_documento"+
				" and d.cs_dscto_especial = ctf.cc_tipo_factoraje"+//MODIFICACION FODEA 042-2009 FVR
				" and ce.ic_cambio_estatus=cce.ic_cambio_estatus"+
				" AND ma.IC_MANDANTE(+) = d.IC_MANDANTE " );
      	/*if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("S") && !ic_epo.equals("") ){
					qrySentencia.append(" and  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
				}else	if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("N") &&  !ic_epo.equals("")){
					qrySentencia.append(" and   ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32) ");
				}else	if (ic_cambio_estatus.equals("") &&  ic_epo.equals("")){
					qrySentencia.append("  and ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
			 }else	if (!ic_cambio_estatus.equals("")){
						qrySentencia.append(" and  ce.ic_cambio_estatus = "+ic_cambio_estatus);
				}*/
				
				
				
		qrySentencia.append(		" and D.ic_if = I.ic_if(+) "+//FODEA 002 - 2009
				" and d.ic_beneficiario = i2.ic_if (+)"+
  			" AND ( ");
      
			Iterator it = ids.iterator();
			i = 0;
			while (it.hasNext()) {
				i++;
				String llave = (String)it.next();
				String claveDocumento = llave.substring(0, llave.indexOf("_"));
				String fechaCambio = llave.substring(llave.indexOf("_") + 1);
				if (i > 1) {
					qrySentencia.append(" OR " );
				}
				qrySentencia.append(" ( D.ic_documento = " + claveDocumento + " AND CE.dc_fecha_cambio = TO_DATE('" + fechaCambio + "','ddmmyyyyhh24miss') ) ");
			}
			qrySentencia.append(") order by d.ig_numero_docto,fechaCambio ");
		 System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();

  }

	public String getDocumentQuery(HttpServletRequest request)
	{
    StringBuffer qrySentencia = new StringBuffer();

    String ic_epo             = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
    String ic_pyme            = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
    String ig_numero_docto    = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto");
    String df_fecha_vencMin   = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
    String df_fecha_vencMax   = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
    String ic_moneda          = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
    String fn_montoMin        = (request.getParameter("fn_montoMin") == null) ? "" : request.getParameter("fn_montoMin");
    String fn_montoMax        = (request.getParameter("fn_montoMax") == null) ? "" : request.getParameter("fn_montoMax");
    String ic_cambio_estatus  = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
    String df_fecha_doctoMin  = (request.getParameter("df_fecha_doctoMin") == null) ? "" : request.getParameter("df_fecha_doctoMin");
    String df_fecha_doctoMax  = (request.getParameter("df_fecha_doctoMax") == null) ? "" : request.getParameter("df_fecha_doctoMax");
    String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null) ? "" : request.getParameter("dc_fecha_cambioMin");
    String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null) ? "" : request.getParameter("dc_fecha_cambioMax");
    String tipoFactoraje      = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
    String  ic_banco_fondeo   = (request.getParameter("ic_banco_fondeo") == null)? "": request.getParameter("ic_banco_fondeo");
    String  firmaMancomunada   = (request.getParameter("firmaMancomunada") == null)? "": request.getParameter("firmaMancomunada");
System.out.println("firmaMancomunada  "+firmaMancomunada);

	  try{
			qrySentencia.append(
        " select /*+use_nl (ce d) index (ce IN_COMHIS_CAM_ESTA_02_NUK) */" +
        " d.ic_documento||'_'||to_char(ce.dc_fecha_cambio,'ddmmyyyyhh24miss')"+
        " ,'CambioEstatusNafinDE::getDocumentQuery'"+
				" from "+
        " comhis_cambio_estatus ce"+
				" , com_documento d, comrel_producto_epo pe "+
				" where");		
				if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("S") && !ic_epo.equals("") ){
					qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
				}else	if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("N") &&  !ic_epo.equals("")){
					qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32) ");
				}else	if (ic_cambio_estatus.equals("") &&  ic_epo.equals("")){
					qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");			
			 }else	if (!ic_cambio_estatus.equals("")){
						qrySentencia.append("  ce.ic_cambio_estatus = "+ic_cambio_estatus);
				} else {
					qrySentencia.append("  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
				}
				
				
			qrySentencia.append(	" and d.ic_documento = ce.ic_documento" +
				" and d.ic_epo = pe.ic_epo " +
				" and pe.ic_producto_nafin = 1 " );
				//" and (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) ");

			if (!ic_epo.equals(""))
				qrySentencia.append(" and d.ic_epo = "+ic_epo);
			if (!ic_pyme.equals(""))
				qrySentencia.append(" and d.ic_pyme = "+ic_pyme);
			
			if (!ic_moneda.equals(""))
				qrySentencia.append(" and d.ic_moneda = "+ic_moneda);
			if (!ig_numero_docto.equals(""))
				qrySentencia.append(" and d.ig_numero_docto = '"+ig_numero_docto.trim()+"' ");
			if(!fn_montoMin.equals(""))
			{
				if(!fn_montoMax.equals(""))
					qrySentencia.append(" and d.fn_monto between "+fn_montoMin+" and "+fn_montoMax);
				else
					qrySentencia.append(" and d.fn_monto = "+fn_montoMin);
			}
			if(!df_fecha_doctoMin.equals(""))
			{
				if(!df_fecha_doctoMax.equals(""))
					qrySentencia.append(" and d.df_fecha_docto >= TO_DATE('"+df_fecha_doctoMin+"','dd/mm/yyyy') and d.df_fecha_docto < TO_DATE('"+df_fecha_doctoMax+"','dd/mm/yyyy')+1 ");
				else
					qrySentencia.append(" and d.df_fecha_docto >= TO_DATE('"+df_fecha_doctoMin+"','dd/mm/yyyy') AND d.df_fecha_docto < TO_DATE('"+df_fecha_doctoMin+"','dd/mm/yyyy')+1 ");
			}
			if(!df_fecha_vencMin.equals(""))
			{
				if(!df_fecha_vencMax.equals(""))
					qrySentencia.append(" and d.df_fecha_venc >= TO_DATE('"+df_fecha_vencMin+"','dd/mm/yyyy') and d.df_fecha_venc < TO_DATE('"+df_fecha_vencMax+"','dd/mm/yyyy')+1 ");
				else
					qrySentencia.append(" and d.df_fecha_venc >= TO_DATE('"+df_fecha_vencMin+"','dd/mm/yyyy') and d.df_fecha_venc < TO_DATE('"+df_fecha_vencMin+"','dd/mm/yyyy')+1 ");
			}
			if(!dc_fecha_cambioMin.equals(""))
			{
				if(!dc_fecha_cambioMax.equals(""))
					qrySentencia.append(" and ce.dc_fecha_cambio >= TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE('"+dc_fecha_cambioMax+"','dd/mm/yyyy')+1 ");
			}
			if(!tipoFactoraje.equals(""))
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ");
			if("".equals(tipoFactoraje)&&"".equals(ig_numero_docto)&&"".equals(ic_cambio_estatus)&&"".equals(ic_epo)&&"".equals(ic_pyme)&&"".equals(ic_moneda)&&"".equals(df_fecha_vencMin)&&"".equals(dc_fecha_cambioMin)&&"".equals(fn_montoMin))
				qrySentencia.append(" and ce.dc_fecha_cambio >= TRUNC(sysdate) and ce.dc_fecha_cambio < TRUNC(sysdate)+1 ");

	  qrySentencia.append(" ORDER BY 1 ");

//    System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
    }catch(Exception e){
      System.out.println("CambioEstatusNafinDE::getDocumentQueryException "+e);
    }
    return qrySentencia.toString();
  }

	public String getDocumentQueryFile(HttpServletRequest request)
  {
    StringBuffer qrySentencia = new StringBuffer();

    String ic_epo             = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
    String ic_pyme            = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
    String ig_numero_docto    = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto");
    String df_fecha_vencMin   = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
    String df_fecha_vencMax   = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
    String ic_moneda          = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
    String fn_montoMin        = (request.getParameter("fn_montoMin") == null) ? "" : request.getParameter("fn_montoMin");
    String fn_montoMax        = (request.getParameter("fn_montoMax") == null) ? "" : request.getParameter("fn_montoMax");
    String ic_cambio_estatus  = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
    String df_fecha_doctoMin  = (request.getParameter("df_fecha_doctoMin") == null) ? "" : request.getParameter("df_fecha_doctoMin");
    String df_fecha_doctoMax  = (request.getParameter("df_fecha_doctoMax") == null) ? "" : request.getParameter("df_fecha_doctoMax");
    String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null) ? "" : request.getParameter("dc_fecha_cambioMin");
    String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null) ? "" : request.getParameter("dc_fecha_cambioMax");
    String tipoFactoraje      = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
    String  ic_banco_fondeo   = (request.getParameter("ic_banco_fondeo") == null)? "": request.getParameter("ic_banco_fondeo");
    String  firmaMancomunada   = (request.getParameter("firmaMancomunada") == null)? "": request.getParameter("firmaMancomunada");

		try{
			qrySentencia.append(
        " select (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO"+
				" , (decode (p.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+
				" , d.ig_numero_docto , TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"+
				" , TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto"+
				" , TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') as dc_fecha_cambio"+
				" , d.ic_moneda, m.cd_nombre, d.fn_monto, decode(d.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl) as fn_aforo"+
				" , (decode(d.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl)/100) * d.fn_monto AS FN_MONTO_DSCTO , cce.cd_descripcion as CambioEstatus"+
				" , ce.ct_cambio_motivo, cce.ic_cambio_estatus, ce.fn_monto_anterior "+
				" , (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as nombreIf "+
				" , d.cs_dscto_especial "+
				" , decode(d.cs_dscto_especial,'D',i2.cg_razon_social,'I',i2.cg_razon_social,'') as beneficiario "+
		        " , decode(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'') as fn_porc_beneficiario "+
		        " , decode(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'') as RECIBIR_BENEF "+
				" , TO_CHAR(ce.df_fecha_venc_anterior,'dd/mm/yyyy') as fechaVencAnt, ce.FN_MONTO_NUEVO "+
				" , d.ic_documento, ce.cg_nombre_usuario"+	//FODEA 002 - 2009
				" , ctf.cg_nombre nombreFactoraje "+//MODIFICACION FODEA 042-2009 FVR
				", ma.cg_razon_social as NOMBREMANDANTE "+
				", d.ic_epo as ic_epo "+
				
				" from com_documento d, comcat_moneda m "+
				" , comcat_epo e, comcat_pyme p, comcat_cambio_estatus cce"+
				" , comhis_cambio_estatus ce, comvis_aforo cv, comcat_if I, comcat_if i2, "+
				" comrel_producto_epo pe " +
				" ,comcat_tipo_factoraje ctf "+//MODIFICACION FODEA 042-2009 FVR
				" , COMCAT_MANDANTE ma " +				
				" where  d.ic_pyme=p.ic_pyme"+
				" and d.ic_epo=e.ic_epo"+
				" and d.ic_epo = pe.ic_epo " +
				" and pe.ic_producto_nafin = 1 " +
				" AND ma.IC_MANDANTE(+) = d.IC_MANDANTE " +
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") + 
				//" and (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
				" and d.ic_epo = cv.ic_epo "+
				" and d.ic_moneda=m.ic_moneda"+
				" and d.ic_documento = ce.ic_documento"+
				" and d.cs_dscto_especial = ctf.cc_tipo_factoraje"+//MODIFICACION FODEA 042-2009 FVR
				" and ce.ic_cambio_estatus=cce.ic_cambio_estatus");
				
				if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("S") && !ic_epo.equals("") ){
					qrySentencia.append(" and  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");
				}else	if (ic_cambio_estatus.equals("") && firmaMancomunada.equals("N") &&  !ic_epo.equals("")){
					qrySentencia.append(" and   ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32) ");
				}else	if (ic_cambio_estatus.equals("") &&  ic_epo.equals("")){
					qrySentencia.append(" and  ce.ic_cambio_estatus in (2,4,5,6,8,23,24,28,29,31,32,37,38,39) ");					
			 }else	if (!ic_cambio_estatus.equals("")){
						qrySentencia.append(" and   ce.ic_cambio_estatus = "+ic_cambio_estatus);
				}
				
				
				qrySentencia.append(	" and D.ic_if = I.ic_if(+) "+//FODEA 002 - 2009
				" and d.ic_beneficiario = i2.ic_if (+)");

			if (!ic_epo.equals(""))
				qrySentencia.append(" and d.ic_epo = "+ic_epo);
			if (!ic_pyme.equals(""))
				qrySentencia.append(" and d.ic_pyme = "+ic_pyme);
			if (!ic_cambio_estatus.equals(""))
				qrySentencia.append(" and ce.ic_cambio_estatus = "+ic_cambio_estatus);
			if (!ic_moneda.equals(""))
				qrySentencia.append(" and d.ic_moneda = "+ic_moneda);
			if (!ig_numero_docto.equals(""))
				qrySentencia.append(" and d.ig_numero_docto = '"+ig_numero_docto.trim()+"' ");
			if(!fn_montoMin.equals(""))
			{
				if(!fn_montoMax.equals(""))
					qrySentencia.append(" and d.fn_monto between "+fn_montoMin+" and "+fn_montoMax);
				else
					qrySentencia.append(" and d.fn_monto = "+fn_montoMin);
			}
			if(!df_fecha_doctoMin.equals(""))
			{
				if(!df_fecha_doctoMax.equals(""))
					qrySentencia.append(" and d.df_fecha_docto >= TO_DATE('"+df_fecha_doctoMin+"','dd/mm/yyyy') and d.df_fecha_docto < TO_DATE('"+df_fecha_doctoMax+"','dd/mm/yyyy')+1 ");
				else
					qrySentencia.append(" and d.df_fecha_docto >= TO_DATE('"+df_fecha_doctoMin+"','dd/mm/yyyy') and d.df_fecha_docto < TO_DATE('"+df_fecha_doctoMin+"','dd/mm/yyyy')+1 ");
			}
			if(!df_fecha_vencMin.equals(""))
			{
				if(!df_fecha_vencMax.equals(""))
					qrySentencia.append(" and d.df_fecha_venc >= TO_DATE('"+df_fecha_vencMin+"','dd/mm/yyyy') and d.df_fecha_venc < TO_DATE('"+df_fecha_vencMax+"','dd/mm/yyyy') + 1 ");
				else
					qrySentencia.append(" and d.df_fecha_venc >= TO_DATE('"+df_fecha_vencMin+"','dd/mm/yyyy') and d.df_fecha_venc < TO_DATE('"+df_fecha_vencMin+"','dd/mm/yyyy')+1 ");
			}
			if(!dc_fecha_cambioMin.equals(""))
			{
				if(!dc_fecha_cambioMax.equals(""))
					qrySentencia.append(" and ce.dc_fecha_cambio >= TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE('"+dc_fecha_cambioMax+"','dd/mm/yyyy')+1 ");
			}
			if(!tipoFactoraje.equals(""))
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ");
			if("".equals(tipoFactoraje)&&"".equals(ig_numero_docto)&&"".equals(ic_cambio_estatus)&&"".equals(ic_epo)&&"".equals(ic_pyme)&&"".equals(ic_moneda)&&"".equals(df_fecha_vencMin)&&"".equals(dc_fecha_cambioMin)&&"".equals(fn_montoMin))
				qrySentencia.append(" and ce.dc_fecha_cambio >= TRUNC(sysdate) and ce.dc_fecha_cambio < TRUNC(sysdate)+1 ");

      qrySentencia.append(" order by d.ig_numero_docto, ce.dc_fecha_cambio");


			System.out.println("qrySentencia"+qrySentencia);

    }catch(Exception e){
      System.out.println("CambioEstatusNafinDE::getDocumentQueryFileException "+e);
    }
    return qrySentencia.toString();
  }


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}


	public String getIc_pyme() {
		return ic_pyme;
	}


	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}


	public String getIg_numero_docto() {
		return ig_numero_docto;
	}


	public void setDf_fecha_vencMin(String df_fecha_vencMin) {
		this.df_fecha_vencMin = df_fecha_vencMin;
	}


	public String getDf_fecha_vencMin() {
		return df_fecha_vencMin;
	}


	public void setDf_fecha_vencMax(String df_fecha_vencMax) {
		this.df_fecha_vencMax = df_fecha_vencMax;
	}


	public String getDf_fecha_vencMax() {
		return df_fecha_vencMax;
	}


	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}


	public String getIc_moneda() {
		return ic_moneda;
	}


	public void setFn_montoMin(String fn_montoMin) {
		this.fn_montoMin = fn_montoMin;
	}


	public String getFn_montoMin() {
		return fn_montoMin;
	}


	public void setFn_montoMax(String fn_montoMax) {
		this.fn_montoMax = fn_montoMax;
	}


	public String getFn_montoMax() {
		return fn_montoMax;
	}


	public void setIc_cambio_estatus(String ic_cambio_estatus) {
		this.ic_cambio_estatus = ic_cambio_estatus;
	}


	public String getIc_cambio_estatus() {
		return ic_cambio_estatus;
	}


	public void setDf_fecha_doctoMin(String df_fecha_doctoMin) {
		this.df_fecha_doctoMin = df_fecha_doctoMin;
	}


	public String getDf_fecha_doctoMin() {
		return df_fecha_doctoMin;
	}


	public void setDf_fecha_doctoMax(String df_fecha_doctoMax) {
		this.df_fecha_doctoMax = df_fecha_doctoMax;
	}


	public String getDf_fecha_doctoMax() {
		return df_fecha_doctoMax;
	}


	public void setDc_fecha_cambioMin(String dc_fecha_cambioMin) {
		this.dc_fecha_cambioMin = dc_fecha_cambioMin;
	}


	public String getDc_fecha_cambioMin() {
		return dc_fecha_cambioMin;
	}


	public void setDc_fecha_cambioMax(String dc_fecha_cambioMax) {
		this.dc_fecha_cambioMax = dc_fecha_cambioMax;
	}


	public String getDc_fecha_cambioMax() {
		return dc_fecha_cambioMax;
	}


	public void setTipoFactoraje(String tipoFactoraje) {
		this.tipoFactoraje = tipoFactoraje;
	}


	public String getTipoFactoraje() {
		return tipoFactoraje;
	}


	public void setIc_banco_fondeo(String ic_banco_fondeo) {
		this.ic_banco_fondeo = ic_banco_fondeo;
	}


	public String getIc_banco_fondeo() {
		return ic_banco_fondeo;
	}


	public void setFirmaMancomunada(String firmaMancomunada) {
		this.firmaMancomunada = firmaMancomunada;
	}


	public String getFirmaMancomunada() {
		return firmaMancomunada;
	}

}