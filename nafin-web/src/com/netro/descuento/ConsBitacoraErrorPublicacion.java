package com.netro.descuento;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsBitacoraErrorPublicacion implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsBitacoraErrorPublicacion.class);
	
	/**
	 * Constructor de la clase.
	 */
	public ConsBitacoraErrorPublicacion(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 	strSQL;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String directorio_publicacion;
 
	private String clave_epo;
	private String fecha_publicacion_ini;
	private String fecha_publicacion_fin;
	
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery(){
		
		log.info("getAggregateCalculationQuery(E)");
		
		strSQL 		= new StringBuffer();
		conditions 	= new ArrayList();
 
		strSQL.append(
			"SELECT 																"  +
			"	COUNT(BIT.IC_ERROR_PUBLICACION) NUMERO_REGISTROS	"  +
			"FROM 																"  +
			"	BIT_ERROR_PUBLICACION BIT 									"  +
			" WHERE 																"  +
			" 	BIT.DF_PUBLICACION >= 	TO_DATE(?, 'dd/mm/yyyy')     AND "  +
      	"	BIT.DF_PUBLICACION < 	TO_DATE(?, 'dd/mm/yyyy') + 1     "
		);

		conditions.add(fecha_publicacion_ini);
		conditions.add(fecha_publicacion_fin);

		// Si se especifico la Clave de la EPO
		if(clave_epo != null && !clave_epo.equals("")){
			strSQL.append(" AND BIT.IC_EPO = ?");
			conditions.add(new Long(clave_epo));
		}
 
		log.debug("strSQL     = <" + strSQL.toString()+ ">");
		log.debug("conditions = <" + conditions       + ">");
		
		log.info("getAggregateCalculationQuery(S)");
		
		return strSQL.toString();
		
	}//getAggregateCalculationQuery
	
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		
		log.info("getDocumentQuery(E)");
		
		strSQL 		= new StringBuffer();
		conditions 	= new ArrayList();
 
		strSQL.append(
			"SELECT 																"  +
			"	BIT.IC_ERROR_PUBLICACION AS ID_DOCUMENTO				"  +
			"FROM 																"  +
			"	BIT_ERROR_PUBLICACION BIT 									"  +
			" WHERE 																"  +
			" 	BIT.DF_PUBLICACION >= 	TO_DATE(?, 'dd/mm/yyyy')     AND "  +
      	"	BIT.DF_PUBLICACION < 	TO_DATE(?, 'dd/mm/yyyy') + 1     "
		);
		
		conditions.add(fecha_publicacion_ini);
		conditions.add(fecha_publicacion_fin);
		
		// Si se especifico la Clave de la EPO
		if(clave_epo != null && !clave_epo.equals("")){
			strSQL.append(" AND BIT.IC_EPO = ?");
			conditions.add(new Long(clave_epo));
		}
		
		strSQL.append(" ORDER BY BIT.DF_PUBLICACION ASC");
		
		log.debug("strSQL     = <" + strSQL.toString()+ ">");
		log.debug("conditions = <" + conditions       + ">");
		
		log.info("getDocumentQuery(F)");
		
		return strSQL.toString();
		
	}//getDocumentQuery
	
	/**
	* Obtiene el query necesario para mostrar la informaci�n completa de 
	* una p�gina a partir de las llaves primarias enviadas como par�metro
	* @return Cadena con la consulta de SQL, para obtener la informaci�n
	* 	completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		
		log.info("getDocumentSummaryQueryForIds(E)");
		
		strSQL 		= new StringBuffer();
		conditions 	= new ArrayList();
 
		strSQL.append(
			"SELECT																						"  +
			"	BIT.IC_ERROR_PUBLICACION 						AS IC_ERROR_PUBLICACION,	"  +
			"	BIT.IC_EPO 											AS CLAVE_EPO,					"  +
			"  NVL(EPO.CG_RAZON_SOCIAL,BIT.IC_EPO)      	AS NOMBRE_EPO, 				"  +
			"	BIT.CG_NUMERO_PROVEEDOR 						AS NUMERO_PROVEEDOR,			"  +
			"	BIT.IG_NUMERO_DOCUMENTO 						AS NUMERO_DOCTO,				"  +
			"	TO_CHAR(BIT.DF_PUBLICACION,'DD/MM/YYYY HH24:MI:SS') 	AS FECHA_PUBLICACION,		"  +
			"	BIT.DF_VENCIMIENTO 								AS FECHA_VENCIMIENTO,		"  +
			"	BIT.IC_MONEDA 										AS CLAVE_MONEDA,				"  +
			"	BIT.FN_MONTO 										AS MONTO,						"  +
			"	BIT.CC_TIPO_FACTORAJE 							AS CLAVE_TIPO_FACTORAJE,	"  +
			"	BIT.CG_USUARIO_PUBLICA 							AS NOMBRE_USUARIO,			"  +
			"	BIT.CG_EMAIL 										AS EMAIL_USUARIO,				"  +
			"  MON.CD_NOMBRE                             AS NOMBRE_MONEDA,				"  +
			"	FAC.CG_NOMBRE                             AS NOMBRE_TIPO_FACTORAJE	"	+
			"FROM 								"	+	
			"	BIT_ERROR_PUBLICACION BIT, "  +
			"	COMCAT_EPO            EPO,	"  +
			"	COMCAT_MONEDA         MON, "	+
			"	COMCAT_TIPO_FACTORAJE FAC	"	+			
			" WHERE 								"  +
			"	BIT.IC_MONEDA = MON.IC_MONEDA									AND "	 +
			"  BIT.CC_TIPO_FACTORAJE = FAC.CC_TIPO_FACTORAJE 			AND "	 +    
			"	BIT.IC_EPO = EPO.IC_EPO(+) AND "  +
			"  BIT.IC_ERROR_PUBLICACION IN (  "
		);
		
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			
			if(i > 0){strSQL.append(", ");}
			
			strSQL.append("?");
			//conditions.add(new BigInteger(lItem.get(0).toString()));
			conditions.add(lItem.get(0).toString());
		}

		strSQL.append(" ) ");
		
		strSQL.append(" ORDER BY BIT.DF_PUBLICACION ASC ");
		
		log.debug("strSQL     = <" + strSQL.toString()+ ">");
		log.debug("conditions = <" + conditions       + ">");
		
		log.info("getDocumentSummaryQueryForIds(S)");
		
		return strSQL.toString();
		
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
		
		log.info("getDocumentQueryFile(E)");
		
		strSQL 		= new StringBuffer();
		conditions 	= new ArrayList();
 
		strSQL.append(
			"SELECT																									"  +
			"	BIT.IC_ERROR_PUBLICACION 						AS IC_ERROR_PUBLICACION,				"  +
			"	BIT.IC_EPO 											AS CLAVE_EPO,								"  +
			"  NVL(EPO.CG_RAZON_SOCIAL,BIT.IC_EPO)      	AS NOMBRE_EPO, 							"  +
			"	BIT.CG_NUMERO_PROVEEDOR 						AS NUMERO_PROVEEDOR,						"  +
			"	BIT.IG_NUMERO_DOCUMENTO 						AS NUMERO_DOCTO,							"  +
			"	TO_CHAR(BIT.DF_PUBLICACION,'DD/MM/YYYY HH24:MI:SS') 	AS FECHA_PUBLICACION,	"  +
			"	BIT.DF_VENCIMIENTO 								AS FECHA_VENCIMIENTO,					"  +
			"	BIT.IC_MONEDA 										AS CLAVE_MONEDA,							"  +
			"	BIT.FN_MONTO 										AS MONTO,									"  +
			"	BIT.CC_TIPO_FACTORAJE 							AS CLAVE_TIPO_FACTORAJE,				"  +
			"	BIT.CG_USUARIO_PUBLICA 							AS NOMBRE_USUARIO,						"  +
			"	BIT.CG_EMAIL 										AS EMAIL_USUARIO,							"  +
			"  MON.CD_NOMBRE                             AS NOMBRE_MONEDA,							"  +
			"	FAC.CG_NOMBRE                             AS NOMBRE_TIPO_FACTORAJE				"	+
			"FROM 								"	+	
			"	BIT_ERROR_PUBLICACION BIT, "  +
			"	COMCAT_EPO            EPO,	"  +
			"	COMCAT_MONEDA         MON, "	+
			"	COMCAT_TIPO_FACTORAJE FAC	"	+
			" WHERE 								"  +
			"	BIT.IC_MONEDA = MON.IC_MONEDA									AND "	 +
			"  BIT.CC_TIPO_FACTORAJE = FAC.CC_TIPO_FACTORAJE 			AND "	 +    
			"	BIT.IC_EPO 			 = 	EPO.IC_EPO(+) 						AND "  +
			" 	BIT.DF_PUBLICACION >= 	TO_DATE(?, 'dd/mm/yyyy')     	AND "  +
      	"	BIT.DF_PUBLICACION < 	TO_DATE(?, 'dd/mm/yyyy') + 1      "
		);
		
		conditions.add(fecha_publicacion_ini);
		conditions.add(fecha_publicacion_fin);
		
		// Si se especifico la Clave de la EPO
		if(clave_epo != null && !clave_epo.equals("")){
			strSQL.append(" AND BIT.IC_EPO = ?");
			conditions.add(new Long(clave_epo));
		}
		
		strSQL.append(" ORDER BY BIT.DF_PUBLICACION ASC");
		
		log.debug("strSQL     = <" + strSQL.toString()+ ">");
		log.debug("conditions = <" + conditions       + ">");
		
		log.info("getDocumentQueryFile(S)");
		
		return strSQL.toString();
		
	}//getDocumentQueryFile
	
		/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos con PAGINADOR*/		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo)
	{	
		String nombreArchivo = "";		return  nombreArchivo;
	}
	
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) SIN paginador*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet reg, String path, String tipo) {
		String nombreArchivo = "";
		if("CSV".equals(tipo))
		{
				String linea = "";
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				StringBuffer 	contenidoArchivo 	= new StringBuffer();
				CreaArchivo 	archivo 			= new CreaArchivo();
				String temp = "", temp1 = "";
					
				try {
				
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
					writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
					buffer = new BufferedWriter(writer);
					buffer.write(linea);
					
				
					linea="EPO,N�mero�de Proveedor,No.�de Documento,Fecha�de Publicaci�n, Fecha�de Vencimiento,Moneda,Monto,"+
							"Tipo de Factoraje,Nombre de Usuario, Email de Usuario," + "\n";
				
					buffer.write(linea);
					
						while (reg.next()) 
						{
							String sEpo 			= (reg.getString("NOMBRE_EPO") == null ? "" : reg.getString("NOMBRE_EPO"));
							String iProveedor		= (reg.getString("NUMERO_PROVEEDOR") == null ? "" : reg.getString("NUMERO_PROVEEDOR"));
							String iNoDocto		= (reg.getString("NUMERO_DOCTO")== null) ? "": reg.getString("NUMERO_DOCTO");
							String sFchPub    	= (reg.getString("FECHA_PUBLICACION") == null ? "" : reg.getString("FECHA_PUBLICACION"));
							String sFchVen			= (reg.getString("FECHA_VENCIMIENTO") == null ? "" : reg.getString("FECHA_VENCIMIENTO"));
							String sMoneda 		= (reg.getString("NOMBRE_MONEDA") == null ? "" : reg.getString("NOMBRE_MONEDA"));
							String iMonto	 		= (reg.getString("MONTO") == null ? "" : reg.getString("MONTO"));
							String sTipoFact  	= (reg.getString("NOMBRE_TIPO_FACTORAJE") == null ? "" : reg.getString("NOMBRE_TIPO_FACTORAJE"));
							String sUsuario 		= (reg.getString("NOMBRE_USUARIO") == null ? "" : reg.getString("NOMBRE_USUARIO"));
							String sCorreoUser	= (reg.getString("EMAIL_USUARIO") == null ? "" : reg.getString("EMAIL_USUARIO"));
							
							
							linea =	sEpo+","+ iProveedor +","+ iNoDocto +","+ sFchPub +","+ sFchVen +","+ sMoneda	+","+ Comunes.formatoDecimal(iMonto,2,false) +","+ sTipoFact +","+
										sUsuario +","+ sCorreoUser+"\n";
									
							buffer.write(linea);
						}
						
					buffer.close();
				} catch (Throwable e) 
				{
					throw new AppException("Error al generar el archivo ", e);
				} 
		}
		return  nombreArchivo;
	}
	
	//GETTERS
  /**
	 * Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions(){return conditions;}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {return paginaNo;}
	
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset(){return paginaOffset;}
	
	//Parametros del formulario
	public String getClave_epo(){return clave_epo;}
	public String getFecha_publicacion_ini(){return fecha_publicacion_ini;}
	public String getFecha_publicacion_fin(){return fecha_publicacion_fin;}
	public String getDirectorio_publicacion(){return directorio_publicacion;}
	
	//SETTERS
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo){paginaNo = newPaginaNo;}
  
   /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset){this.paginaOffset=paginaOffset;}
		
	//Parametros del formulario
	public void setClave_epo(String clave_epo){this.clave_epo = clave_epo;}
	public void setFecha_publicacion_ini(String fecha_publicacion_ini){this.fecha_publicacion_ini = fecha_publicacion_ini;}
	public void setFecha_publicacion_fin(String fecha_publicacion_fin){this.fecha_publicacion_fin = fecha_publicacion_fin;}
	public void setDirectorio_publicacion(String directorio_publicacion){this.directorio_publicacion = directorio_publicacion;}
	
}