package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Clase para la pantalla Descuento Electonico/ Capturas/Reasignacion Documentos Negociables
 */
public class ConsReasignacionDoctos implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsReasignacionDoctos.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_epo;
	private String ic_pyme;
	private String totaMDolar;
	private String totaMnacional;
	private String acuse;
	private String fechaAcuse;
	private String horaAcuse;
	private String usuario;
	private String documentos;
	private String causaReasignacion;

	 
	public ConsReasignacionDoctos() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		
		qrySentencia.append(" SELECT e.cg_razon_social as  NOMBRE_EPO, "+
			"  p.cg_razon_social as  NOMBRE_PROVEEDOR,  "+
			 " d.ig_numero_docto AS NO_DOCUMENTO,   "+
			 " TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS FECHA_DOCUMENTO,   "+
			 " TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS FECHA_VENCIMIENTO,   "+  
			 " m.cd_nombre AS MONEDA,   "+
			 " m.ic_moneda AS IC_MONEDA,   "+  
			 " ctp.cg_nombre AS TIPO_FACTORAJE,   "+
			 " d.fn_monto AS MONTO_DOCUMENTO,   "+
			 " NVL (d.cg_campo1, ' ') AS CAMPO_1,   "+
			 " NVL (d.cg_campo2, ' ') AS CAMPO_2,   "+
			 " NVL (d.cg_campo3, ' ') AS CAMPO_3,  "+ 
			 " NVL (d.cg_campo4, ' ') AS CAMPO_4,  "+ 
			 " NVL (d.cg_campo5, ' ') AS CAMPO_5,  "+ 
			 " d.ic_documento AS IC_DOCUMENTO   "+
			 " FROM com_documento d, comcat_moneda m, comcat_tipo_factoraje ctp , comcat_epo e, comcat_pyme p  "+
			 " WHERE d.cs_dscto_especial = ctp.cc_tipo_factoraje  "+
			 " AND m.ic_moneda = d.ic_moneda   "+
			 " AND d.ic_estatus_docto = ?   "+
			 " and e.ic_epo =  d.ic_epo  "+
			 " and p.ic_pyme=  d.ic_pyme  ");
				conditions.add("2");	
				
			if(!ic_epo.equals("")) {
				qrySentencia.append(" AND d.ic_epo =  ?   ");
				conditions.add(ic_epo);	
			}
			if(!ic_pyme.equals("")) {
				qrySentencia.append(" AND d.ic_pyme = ?    ");
				conditions.add(ic_pyme);	
			}	
			if(!documentos.equals("")) {
			qrySentencia.append(" AND d.ic_documento in( "+documentos+")") ;
		}
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int camposAd= 0, columnas = 9;
		
		try {
		
			Vector nombresCampo = new Vector(5);
			nombresCampo = this.getCamposAdicionales(ic_epo);
			camposAd=  nombresCampo.size();
			columnas += camposAd;
				
	
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
            (String) session.getAttribute("strNombre"),
            (String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
 
			pdfDoc.setTable(3,60);
			pdfDoc.setCell("","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda Nacional","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Dolares","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("No. total de documentos reasignados","formas",ComunesPDF.LEFT);		
			pdfDoc.setCell(totaMnacional,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(totaMDolar,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("Número de Acuse","formas",ComunesPDF.LEFT);		
			pdfDoc.setCell(acuse,"formas",ComunesPDF.LEFT,2);
			pdfDoc.setCell("Fecha de reasignación","formas",ComunesPDF.LEFT);		
			pdfDoc.setCell(fechaAcuse,"formas",ComunesPDF.LEFT,2);
			pdfDoc.setCell("fechaAcuse de reasignación","formas",ComunesPDF.LEFT);		
			pdfDoc.setCell(horaAcuse,"formas",ComunesPDF.LEFT,2);
			pdfDoc.setCell("Usuario","formas",ComunesPDF.LEFT);		
			pdfDoc.setCell(usuario,"formas",ComunesPDF.LEFT,2);			
			pdfDoc.addTable();
			
			pdfDoc.setTable(columnas,100);
			pdfDoc.setCell("Nombre EPO","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre Proveedor","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("No. Documento","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha Documento","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha Vencimiento","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo Factoraje","celda02",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Documento","celda02",ComunesPDF.CENTER);			
			 for(int j=0;j<camposAd;j++){
				Vector lovRegistro = (Vector) nombresCampo.get(j);
				pdfDoc.setCell(lovRegistro.get(0).toString(),"celda02",ComunesPDF.CENTER);
			}
			pdfDoc.setCell("Causa Reasignación","celda02",ComunesPDF.CENTER);
		
			while (rs.next())	{
				
			String nombreEPo = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
			String nombreProveedor = (rs.getString("NOMBRE_PROVEEDOR") == null) ? "" : rs.getString("NOMBRE_PROVEEDOR");
			String no_documento = (rs.getString("NO_DOCUMENTO") == null) ? "" : rs.getString("NO_DOCUMENTO");
			String fecha_documento = (rs.getString("FECHA_DOCUMENTO") == null) ? "" : rs.getString("FECHA_DOCUMENTO");
			String fecha_vencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
			String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
			String tipo_factoraje = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
			String monto_documento = (rs.getString("MONTO_DOCUMENTO") == null) ? "" : rs.getString("MONTO_DOCUMENTO");
			String campo1 = (rs.getString("CAMPO_1") == null) ? "" : rs.getString("CAMPO_1");
			String campo2 = (rs.getString("CAMPO_2") == null) ? "" : rs.getString("CAMPO_2");
			String campo3 = (rs.getString("CAMPO_3") == null) ? "" : rs.getString("CAMPO_3");
			String campo4 = (rs.getString("CAMPO_4") == null) ? "" : rs.getString("CAMPO_4");
			String campo5  = (rs.getString("CAMPO_5") == null) ? "" : rs.getString("CAMPO_5");
								
			pdfDoc.setCell(nombreEPo,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(nombreProveedor,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(no_documento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fecha_documento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fecha_vencimiento,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(tipo_factoraje,"formas",ComunesPDF.CENTER);			
			pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_documento,2),"formas",ComunesPDF.RIGHT);
			for (int i=0; i<nombresCampo.size(); i++) { 			
				Vector lovRegistro = (Vector) nombresCampo.get(i);
				if(i==0) pdfDoc.setCell(campo1,"formas",ComunesPDF.CENTER);
				if(i==1) pdfDoc.setCell(campo2,"formas",ComunesPDF.CENTER);
				if(i==2) pdfDoc.setCell(campo3,"formas",ComunesPDF.CENTER); 
				if(i==3) pdfDoc.setCell(campo4,"formas",ComunesPDF.CENTER);
				if(i==4) pdfDoc.setCell(campo5,"formas",ComunesPDF.CENTER);		
			}			
				pdfDoc.setCell(causaReasignacion,"formas",ComunesPDF.LEFT);										
			}
		
			pdfDoc.addTable();
			pdfDoc.endDocument();
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
		
	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}



/**
 *  Metodo para obtener los datos de la PYME
 */  
public List  getDatoPyme( String epo, String noElectronico, String noProveedor ,  String noDocto, String rfc, String tipoPyme  ){
	log.info("getDatoPyme----- (E)");

	AccesoDB con = new AccesoDB();
	ResultSet rs = null;	
	List registros  = new ArrayList();
	HashMap datos = new HashMap();
	String saceptacion ="", spyme="", mensaje="",  ic_pyme ="", nomPyme = "", r_f_c = "", direccion = "", colonia = "", telefono = "";
			
	try{
		con.conexionDB();
		
		String qry = "select cs_aceptacion, ic_pyme from comrel_pyme_epo where ic_epo = " + epo;
		if(!noElectronico.equals(""))
			qry += " and ic_pyme = (select ic_epo_pyme_if from comrel_nafin where ic_nafin_electronico = " + noElectronico + ") ";
		if(!noProveedor.equals(""))
			qry += " and cg_pyme_epo_interno = '" + noProveedor +"' ";
		if(!noDocto.equals(""))
			qry += " and ic_pyme = (select ic_pyme from com_documento where ig_numero_docto = '" + noDocto + "' and ic_epo = " + epo + ") ";
		if(!rfc.equals(""))
			qry += " and ic_pyme = (select ic_pyme from comcat_pyme where cg_rfc = '" + rfc + "')";
		
		log.info("qry----- "+qry);
		
		rs = con.queryDB(qry);
				
		if(rs.next()){
			saceptacion = rs.getString("cs_aceptacion");
			spyme = rs.getString("ic_pyme");
		}else{
			mensaje= "La información capturada debe de corresponder a una PYME relacionada a la EPO seleccionada";
		}
		rs.close();
		con.cierraStatement();
	
		if(saceptacion.equals("H") && tipoPyme.equals("origen") ){ //H es Habilitado, por lo tanto, no es una pyme valida
			mensaje= "La PYME origen No debe estar parametrizada a descuento electrónico";	
			
		}else if(!saceptacion.equals("H") && !saceptacion.equals("") && tipoPyme.equals("destino") ){
			mensaje= "La PYME destino debe esta parametrizada a descuento electrónico";
	
		}else if(!saceptacion.equals("") && !spyme.equals("") &&  (   tipoPyme.equals("origen")  ||  tipoPyme.equals("destino") ) ){
		
			qry = "select p.ic_pyme as ic_pyme, p.cg_razon_social as pyme, p.cg_rfc as rfc, d.cg_calle || ' ' || d.cg_numero_ext as direccion " +
					", d.cg_colonia as colonia, d.cg_telefono1 as telefono " +
					"from comcat_pyme p, com_domicilio d " +
					"where p.ic_pyme = " + spyme +
					" and d.ic_pyme = p.ic_pyme " +
					"and d.cs_fiscal = 'S'";
			rs = con.queryDB(qry);
			if(rs.next()){
				ic_pyme =rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				nomPyme =rs.getString("pyme")==null?"":rs.getString("pyme");  
				r_f_c =rs.getString("rfc")==null?"":rs.getString("rfc");  
				direccion =rs.getString("direccion")==null?"":rs.getString("direccion");  
				colonia =rs.getString("colonia")==null?"":rs.getString("colonia"); 
				telefono =rs.getString("telefono")==null?"":rs.getString("telefono"); 				
			}
			rs.close();
			con.cierraStatement();
		}
		
		
		datos = new HashMap();
		datos.put("IC_YME",ic_pyme);
		datos.put("PYME",nomPyme);
		datos.put("RFC",r_f_c);	
		datos.put("DIRECCION",direccion);	
		datos.put("COLONIA",colonia);	
		datos.put("TELEFONO",telefono);
		datos.put("MENSAJE",mensaje);
		registros.add(datos);	
			
	} catch (Exception e) {
		log.error("getDatoPyme  Error: " + e);
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	} 
	log.info("getDatoPyme----- (S)");
	return registros;
}

public Vector  getCamposAdicionales( String epo  ){ 
	log.info("getCamposAdicionales----- (E)");

	AccesoDB con = new AccesoDB();
	ResultSet rs = null;	
	
	Vector vCamposAdcionales = new Vector();
	Vector vca = null;		
	try{
		con.conexionDB();
	
		String query = "SELECT INITCAP(CG_NOMBRE_CAMPO) as nombreCampoAd FROM comrel_visor " +
										"WHERE ic_epo = " + epo +
										" AND ic_producto_nafin=1 " +
										"AND ic_no_campo BETWEEN 1 AND 5 " +
										"ORDER BY ic_no_campo ASC";
		rs = con.queryDB(query);
		while (rs.next()) {
			vca = new Vector();
			vca.add(rs.getString("nombreCampoAd")==null?"":rs.getString("nombreCampoAd"));					
			vCamposAdcionales.addElement(vca);
		}
		rs.close();
			
	} catch (Exception e) {
		log.error("getCamposAdicionales  Error: " + e);
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	} 
	log.info("getCamposAdicionales----- (S)");
	return vCamposAdcionales;
}


	
	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getTotaMDolar() {
		return totaMDolar;
	}

	public void setTotaMDolar(String totaMDolar) {
		this.totaMDolar = totaMDolar;
	}

	public String getTotaMnacional() {
		return totaMnacional;
	}

	public void setTotaMnacional(String totaMnacional) {
		this.totaMnacional = totaMnacional;
	}

	public String getAcuse() {
		return acuse;
	}

	public void setAcuse(String acuse) {
		this.acuse = acuse;
	}

	public String getFechaAcuse() {
		return fechaAcuse;
	}

	public void setFechaAcuse(String fechaAcuse) {
		this.fechaAcuse = fechaAcuse;
	}

	public String getHoraAcuse() {
		return horaAcuse;
	}

	public void setHoraAcuse(String horaAcuse) {
		this.horaAcuse = horaAcuse;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getDocumentos() {
		return documentos;
	}

	public void setDocumentos(String documentos) {
		this.documentos = documentos;
	}

	public String getCausaReasignacion() {
		return causaReasignacion;
	}

	public void setCausaReasignacion(String causaReasignacion) {
		this.causaReasignacion = causaReasignacion;
	}




	

}