package com.netro.descuento;

import java.io.InputStream;

import java.rmi.RemoteException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/********************************************************************************************
 * Fodea:       33-2014                                                                     *
 * Descripci�n: Pantalla Administraci�n/Cuentas CLABE Swift/Consultas Bit�cora CLABE        *
 * Elabor�:     Agust�n Bautista Ruiz                                                       *
 * Fecha:       04/11/2014                                                                  *
 ********************************************************************************************/
public class ConsBitacoraCuentasClabe implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(ConsBitacoraCuentasClabe.class);
	StringBuffer qrySentencia = new StringBuffer();
	private List   conditions;
	private String fechaInicio;
	private String fechaFinal;
	private String icBitDescarga;
	private String rutaDestino;
	private String extension;

	public ConsBitacoraCuentasClabe() {}
	public String getDocumentQueryFile(){ return null;}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){ return null;}
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){ return null;}

	/**
	 * Obtiene las llaves primarias
	 * @return qrySentencia
	 */
	public String getDocumentQuery(){

		log.info("getDocumentQuery (E)");
		qrySentencia = new StringBuffer();
		conditions   = new ArrayList();

		qrySentencia.append(
			  "SELECT   IC_BIT_DESCARGA "
			+ "FROM     BIT_DESCARGA_CUENTAS_CLABE "
			+ "WHERE    DF_DESCARGA >= TO_DATE(?, 'dd/mm/yyyy') "
			+ "         AND DF_DESCARGA < TO_DATE(?, 'dd/mm/yyyy') + 1 "
			+ "ORDER BY DF_DESCARGA DESC "
		);

		conditions.add(this.fechaInicio);
		conditions.add(this.fechaFinal);

		log.info("Sentencia: "   + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getDocumentQuery (S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene los registros totales
	 * @return qrySentencia
	 */
	public String getAggregateCalculationQuery(){

		log.info("getAggregateCalculationQuery (E)");
		qrySentencia = new StringBuffer();
		conditions   = new ArrayList();

		qrySentencia.append(
			  "SELECT COUNT(IC_BIT_DESCARGA) "
			+ "FROM   BIT_DESCARGA_CUENTAS_CLABE "
			+ "WHERE  DF_DESCARGA >= TO_DATE(?, 'dd/mm/yyyy') "
			+ "       AND DF_DESCARGA < TO_DATE(?, 'dd/mm/yyyy') + 1 "
		);

		conditions.add(this.fechaInicio);
		conditions.add(this.fechaFinal);

		log.info("Sentencia: "   + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getAggregateCalculationQuery (S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene la consulta a partir de las llaves primarias
	 * @return qrySentencia
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){

		log.info("getDocumentSummaryQueryForIds (E)");
		StringBuffer condicion = new StringBuffer();
		qrySentencia = new StringBuffer();
		conditions   = new ArrayList();

		condicion.append("  WHERE IC_BIT_DESCARGA IN (");
		for (int i = 0; i < ids.size(); i++){
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				condicion.append(",");
			}
			condicion.append(" ? " );
			conditions.add(new Long(lItem.get(0).toString()));
		}
		condicion.append(") ");

		qrySentencia.append(
			  "SELECT   IC_BIT_DESCARGA, "
			+ "         TO_CHAR(DF_DESCARGA, 'dd/mm/yyyy HH24:MI:SS') AS FECHA_DESCARGA, "
			+ "         CG_NOMBRE_USUARIO, "
			+ "         DBMS_LOB.GETLENGTH(BI_ARCHIVO_TXT) AS ARCHIVO_TXT, "
			+ "         DBMS_LOB.GETLENGTH(BI_ARCHIVO_XLS) AS ARCHIVO_EXCEL "
			+ "FROM     BIT_DESCARGA_CUENTAS_CLABE "
			+ condicion.toString()
			+ "ORDER BY DF_DESCARGA DESC "
		);

		log.info("Sentencia: "   + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getDocumentSummaryQueryForIds (S)");
		return qrySentencia.toString();

	}

	/**
	 * Realiza la consulta para obteber el archivo txt o xls cargados en bit�cora
	 * @return nombreArchivo
	 */
	public String descargaArchivo() throws RemoteException{

		log.info("descargaArchivo (E)");
		AccesoDB con         = new AccesoDB();
		String nombreArchivo = "";
		String columna       = "";
		qrySentencia         = new StringBuffer();
		
		try{
			/***** Defino el campo del que se estraer� el archivo *****/
			if(this.extension.equals("txt")){
				columna = "BI_ARCHIVO_TXT";
			} else if(this.extension.equals("xls")){
				columna = "BI_ARCHIVO_XLS";
			}

			con.conexionDB();

			qrySentencia.append("SELECT " + columna + " FROM BIT_DESCARGA_CUENTAS_CLABE WHERE IC_BIT_DESCARGA = ? ");
			log.info("Sentencia: " + qrySentencia.toString());

			PreparedStatement ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, Integer.parseInt(this.icBitDescarga));
			ResultSet rs = ps.executeQuery();

			if (rs.next()){
				InputStream inStream = rs.getBinaryStream(columna);
				CreaArchivo creaArchivo = new CreaArchivo();
				if (!creaArchivo.make(inStream, this.rutaDestino, "." + this.extension)){
					throw new AppException("Error al generar el archivo en " + this.rutaDestino);
				}
				nombreArchivo = creaArchivo.getNombre();
				inStream.close();
			}

		} catch(Exception e){
			log.warn("descargaArchivo(Exception)::.." +  e);
			throw new AppException("Error Inesperado", e);
		} finally{
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}

		log.info("descargaArchivo (S)");
		return nombreArchivo;
	}

/************************************************************************
 *                          GETTERS AND SETTERS                         *
 ************************************************************************/
	public List getConditions(){
		return conditions;
	}

	public String getFechaInicio(){
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio){
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFinal(){
		return fechaFinal;
	}

	public void setFechaFinal(String fechaFinal){
		this.fechaFinal = fechaFinal;
	}

	public String getIcBitDescarga(){
		return icBitDescarga;
	}

	public void setIcBitDescarga(String icBitDescarga){
		this.icBitDescarga = icBitDescarga;
	}



	public String getRutaDestino() {
		return rutaDestino;
	}

	public void setRutaDestino(String rutaDestino){
		this.rutaDestino = rutaDestino;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension){
		this.extension = extension;
	}

}