package com.netro.descuento;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.IQueryGenerator;

public class CambioEstatusPymeDE implements IQueryGenerator {
	public CambioEstatusPymeDE(){}
	
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		String condicion = "";
		StringBuffer qrySentencia = new StringBuffer();
		HttpSession session = request.getSession();

		String icPyme = (String)session.getAttribute("iNoCliente");
		String icEpo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String igNumeroDocto = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto");
		String dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
		String dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
		String icMoneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
		String icCambioEstatus = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
		String fnMontoMin = (request.getParameter("fn_montoMin") == null) ? "" : request.getParameter("fn_montoMin");
		String fnMontoMax = (request.getParameter("fn_montoMax") == null) ? "" : request.getParameter("fn_montoMax");
		String dcFechaCambioMin = (request.getParameter("dc_fecha_cambioMin") == null) ? "" : request.getParameter("dc_fecha_cambioMin");
		String dcFechaCambioMax = (request.getParameter("dc_fecha_cambioMax") == null) ? "" : request.getParameter("dc_fecha_cambioMax");
		String dfFechaDoctoMin = (request.getParameter("df_fecha_doctoMin") == null) ? "" : request.getParameter("df_fecha_doctoMin");
		String dfFechaDoctoMax = (request.getParameter("df_fecha_doctoMax") == null) ? "" : request.getParameter("df_fecha_doctoMax");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") ==null) ? "" : request.getParameter("tipoFactoraje");
		String fechaActual = (request.getParameter("fechaActual") == null) ? "" : request.getParameter("fechaActual");
		String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");

		/* Para manejo de idioma ingl�s, en el jsp se define un campo "idiomaUsuario"
		 * con los valores "ES" para espa�ol o "EN" para ingl�s.
		 */
		String idiomaUsuario =
				(request.getSession().getAttribute("sesIdiomaUsuario") == null ) ? "ES" :
				(String) request.getSession().getAttribute("sesIdiomaUsuario");

		String campoNombreMoneda = null;
		if (idiomaUsuario.equals("EN")) {
			campoNombreMoneda = " M.cd_nombre_ing ";
		} else {
			campoNombreMoneda = " M.cd_nombre ";
		}

	try {
        /** Optimizacion 2018.11.23 */
		qrySentencia.append(
				"select " +
				"  /*+ ordered index(m CP_COMCAT_MONEDA_PK) */ " +
				"        COUNT (1) AS cambestpymede,  " +
				"        SUM (d.fn_monto),  " +
				"        SUM (d.fn_monto_dscto),  " +
				"        d.ic_moneda, " +
				campoNombreMoneda + ", " +
				"    	 'CambioEstatusPymeDE::getAggregateCalculationQuery' " +
				"from  " +
				"        com_documento d,  " +
				"        comhis_cambio_estatus ce, " +
				"        comcat_moneda m " +
				"where   d.ic_documento = ce.ic_documento " +
				"        and d.ic_moneda = m.ic_moneda " +
				"        and ce.ic_cambio_estatus in (2,4,5,6,8,23,28,29,41)  " +
				"        and d.ic_pyme = " + icPyme );

         if (!icEpo.equals(""))
            condicion += " and d.ic_epo = " + icEpo;
         if (!icCambioEstatus.equals(""))
				if (icCambioEstatus.equals("23"))
					condicion += " and ce.ic_cambio_estatus in (23,41)";
				else
					condicion += " and ce.ic_cambio_estatus = "+icCambioEstatus.trim();
         if (!icMoneda.equals(""))
            condicion += " and d.ic_moneda = " + icMoneda.trim();
         if (!igNumeroDocto.equals(""))
            condicion += " and d.ig_numero_docto = '"+igNumeroDocto.trim()+"'";
         if(!dcFechaCambioMin.equals(""))
            if(!dcFechaCambioMax.equals(""))
               condicion += " and trunc(ce.dc_fecha_cambio) between TO_DATE('"+dcFechaCambioMin.trim()+"','dd/mm/yyyy') and TO_DATE('"+dcFechaCambioMax.trim()+"','dd/mm/yyyy') ";
         if(!fnMontoMin.equals(""))
            if(!fnMontoMax.equals(""))
               condicion += " and d.fn_monto between " + fnMontoMin.trim() + " and "+ fnMontoMax.trim() +" ";
            else
               condicion += " and d.fn_monto = " + fnMontoMin.trim()+" ";
         if(!dfFechaDoctoMin.equals(""))
            if(!dfFechaDoctoMax.equals(""))
               condicion += " and trunc(d.df_fecha_docto) between TO_DATE('" + dfFechaDoctoMin + "','dd/mm/yyyy') and TO_DATE('" + dfFechaDoctoMax + "','dd/mm/yyyy') ";
         if(!dfFechaVencMin.equals(""))
            if(!dfFechaVencMax.equals(""))
               condicion += " and trunc(d.df_fecha_venc"+sFechaVencPyme+") between TO_DATE('" + dfFechaVencMin + "','dd/mm/yyyy') and TO_DATE('" + dfFechaVencMax + "','dd/mm/yyyy') ";
         if(!tipoFactoraje.equals(""))
            condicion += " and d.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ";
         if(icEpo.equals("") && icCambioEstatus.equals("") && icMoneda.equals("") && igNumeroDocto.equals("") && dcFechaCambioMin.equals("") && fnMontoMin.equals("") && fnMontoMax.equals("") && dfFechaDoctoMin.equals("") && dfFechaVencMin.equals("") && tipoFactoraje.equals(""))
            condicion += " and trunc(ce.dc_fecha_cambio) = TO_DATE('"+fechaActual+"','dd/mm/yyyy') ";

        qrySentencia.append(" " + condicion);
     	qrySentencia.append(" group by d.ic_moneda," + campoNombreMoneda);
      	qrySentencia.append(" order by d.ic_moneda");

    	System.out.println("EL query del AggregateCalculationQuery es: "+qrySentencia.toString());

    }catch(Exception e){
      System.out.println("CambioEstatusPymeDE::getAggregateCalculationQuery "+e);
    }
    return qrySentencia.toString();
  }

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		
		int i=0;
		StringBuffer pedido = new StringBuffer();
		StringBuffer fecha = new StringBuffer();
		String condicion = "";
		String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");
		String mandant 		 = (request.getParameter("mandant") == null) ? "" : request.getParameter("mandant");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") ==null) ? "" : request.getParameter("tipoFactoraje");
		//System.out.println("::::::::::::::::::::::::: tipoFactoraje ::::::::::::::::::::::::: "+tipoFactoraje);
		StringBuffer qrySentencia = new StringBuffer();
		
		for (Iterator it = ids.iterator(); it.hasNext();i++){
		
			if (i>0)
				condicion += " OR ";
			
			String temp = it.next().toString();
			int ind = temp.indexOf('|');
			String temp_doc = temp.substring(0,ind);
			String temp_fec = temp.substring(ind+1,temp.length());
			
			condicion += " d.ic_documento = " + temp_doc +
					" and to_char(ce.dc_fecha_cambio,'dd/mm/yyyy hh24:mm:ss') = '" + temp_fec + "' ";
		}
		
		
		/* Para manejo de idioma ingl�s, en el jsp se define un campo "idiomaUsuario"
		* con los valores "ES" para espa�ol o "EN" para ingl�s.
		*/
		String idiomaUsuario =
		(request.getSession().getAttribute("sesIdiomaUsuario") == null ) ? "ES" :
		(String) request.getSession().getAttribute("sesIdiomaUsuario");
		String campoNombreMoneda = null;
		String campoCambioEstatus = null;
		if (idiomaUsuario.equals("EN")) {
			campoNombreMoneda = " M.cd_nombre_ing ";
			campoCambioEstatus = " cce.cd_descripcion_ing ";
		} else {
			campoNombreMoneda = " M.cd_nombre ";
			campoCambioEstatus = " cce.cd_descripcion ";
		}
		
		StringBuffer mandantecamp = new StringBuffer();
		StringBuffer mandantetabs = new StringBuffer();
		StringBuffer mandantecond = new StringBuffer();
		
		System.out.println(" ����� mandant -:Consulta:- ���� "+mandant+ " �����");
		
		if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
			mandantecamp.append( " cm.cg_razon_social AS mandante, " );
			mandantetabs.append(	"	,comcat_mandante cm " );
			if("M".equals(tipoFactoraje)){
				mandantecond.append(" AND d.ic_mandante = cm.ic_mandante" );		 
			}else{
				mandantecond.append(" AND d.ic_mandante = cm.ic_mandante(+)" );		 
			}
		}
		
		if(!"".equals(mandant)){
			mandantecond.append(	"  AND cm.IC_MANDANTE = "+mandant+" " );
		}

		qrySentencia.append(
				"select " +
				"    /*+ " +
				"        INDEX (CE CP_COMHIS_CAMBIO_ESTATUS_PK) " +
				"        INDEX (E CP_COMCAT_EPO_PK) " +
				"        INDEX (I CP_COMCAT_IF_PK) " +
				"        INDEX (I2 CP_COMCAT_IF_PK) " +
				"        INDEX (M CP_COMCAT_MONEDA_PK) " +
				"        INDEX (CCE CP_COMCAT_CAMBIO_ESTATUS_PK) " +
				"        USE_NL (D CE CV E I I2 M CCE) " +
				"    */  " +
				"        (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) AS NOMBRE_EPO,  " +
				"        d.ig_numero_docto,  " +
				"        TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') AS DF_FECHA_DOCTO,  " +
				"        TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'dd/mm/yyyy') AS DF_FECHA_VENC,  " +
				"        TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') AS DC_FECHA_CAMBIO,  " +
				"        d.ic_moneda,  " +
				campoNombreMoneda + ", " +
				"        d.fn_monto, " +
				"        d.fn_monto_dscto, " +
				campoCambioEstatus + ", " +
				"        ce.ct_cambio_motivo,  " +
				"        decode(d.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl) as fn_aforo,  " +
				"        cce.ic_cambio_estatus,  " +
				"        ce.fn_monto_anterior,  " +
				"        d.fn_porc_anticipo,  " +
				"        (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as NombreIF,  " +
				"        d.cs_dscto_especial,  " +
				"        I2.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO,  " +
				"        d.FN_PORC_BENEFICIARIO,  " +
				"        (d.FN_MONTO * d.FN_PORC_BENEFICIARIO ) / 100 RECIBIR_BENEFICIARIO," +
				"    		ce.fn_monto_nuevo, "+
				" 			tf.cg_nombre as TIPO_FACTORAJE, "+mandantecamp+ 
				"        CASE WHEN (cce.ic_cambio_estatus = 28) THEN ce.fn_monto_nuevo ELSE d.fn_monto END AS monto, " +
				"			CASE WHEN(d.cs_dscto_especial IN ('V', 'D', 'I') ) THEN 100 " +
				" 			WHEN (cce.ic_cambio_estatus in (4,5,6,8,28,29)) THEN decode(d.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl) " +
				" 			ELSE d.fn_porc_anticipo END AS porcentaje_descuento, " +
				" 			'CambioEstatusPymeDE::getDocumentSummaryQueryForIds' " +
				"from  " +
				"        com_documento d,  " +
				"        comhis_cambio_estatus ce,  " +
				"        comvis_aforo cv,  " +
				"        comcat_epo e,  " +
				"        comcat_if I,  " +
				"        comcat_if I2,  " +
				"        comcat_moneda m,  " +
				"        comcat_cambio_estatus cce, "+
				"  		comcat_tipo_factoraje tf "+mandantetabs+
				"where   d.ic_epo = e.ic_epo   " +
				"        and d.ic_moneda = m.ic_moneda  " +
				"        and d.ic_documento = ce.ic_documento  " +
				"        and d.ic_epo = cv.ic_epo  " +
				"        and ce.ic_cambio_estatus = cce.ic_cambio_estatus  " +
				"        and d.ic_if = I.ic_if(+)  " +
				"        and d.ic_beneficiario = I2.ic_if (+)  " +
				"        and ce.ic_cambio_estatus in (2,4,5,6,8,23,28,29,41) " +
				" 			and d.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+mandantecond+
				"		   and (" + condicion + ")" +
				"ORDER BY ce.ic_cambio_estatus,d.ig_numero_docto,ce.dc_fecha_cambio ");
		
		System.out.println("CambioEstatusPymeDE.getDocumentSummaryQueryForIds:: "+qrySentencia.toString());
		return qrySentencia.toString();
	}

	public String getDocumentQuery(HttpServletRequest request) {
		String condicion = "";
		StringBuffer qrySentencia = new StringBuffer();
		HttpSession session = request.getSession();
		String icPyme = (String)session.getAttribute("iNoCliente");
		String icEpo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String igNumeroDocto = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto");
		String dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
		String dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
		String icMoneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
		String icCambioEstatus = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
		String fnMontoMin = (request.getParameter("fn_montoMin") == null) ? "" : request.getParameter("fn_montoMin");
		String fnMontoMax = (request.getParameter("fn_montoMax") == null) ? "" : request.getParameter("fn_montoMax");
		String dcFechaCambioMin = (request.getParameter("dc_fecha_cambioMin") == null) ? "" : request.getParameter("dc_fecha_cambioMin");
		String dcFechaCambioMax = (request.getParameter("dc_fecha_cambioMax") == null) ? "" : request.getParameter("dc_fecha_cambioMax");
		String dfFechaDoctoMin = (request.getParameter("df_fecha_doctoMin") == null) ? "" : request.getParameter("df_fecha_doctoMin");
		String dfFechaDoctoMax = (request.getParameter("df_fecha_doctoMax") == null) ? "" : request.getParameter("df_fecha_doctoMax");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") ==null) ? "" : request.getParameter("tipoFactoraje");
		String fechaActual = (request.getParameter("fechaActual") == null) ? "" : request.getParameter("fechaActual");
		String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");

		try{
			qrySentencia.append(
					"select " +
					"    /*+ " +
					"        INDEX (D IN_COM_DOCUMENTO_02_NUK)  " +
					"    */  " +
					"        d.ic_documento||'|'||TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy hh24:mm:ss'), " +
					"    	 'CambioEstatusPymeDE::getDocumentQuery' " +
					"from  " +
					"        com_documento d,  " +
					"        comhis_cambio_estatus ce " +
					"where   d.ic_documento = ce.ic_documento " +
					"        and ce.ic_cambio_estatus in (2,4,5,6,8,23,28,29,41)   " +
					"        and d.ic_pyme =  " + icPyme);
		
			if (!icEpo.equals(""))
				condicion += " and d.ic_epo = " + icEpo;
			if (!icCambioEstatus.equals(""))
				if (icCambioEstatus.equals("23"))
				condicion += " and ce.ic_cambio_estatus in (23,41)";
				else
				condicion += " and ce.ic_cambio_estatus = "+icCambioEstatus.trim();
			if (!icMoneda.equals(""))
				condicion += " and d.ic_moneda = " + icMoneda.trim();
			if (!igNumeroDocto.equals(""))
				condicion += " and d.ig_numero_docto = '"+igNumeroDocto.trim()+"'";
			if(!dcFechaCambioMin.equals(""))
				if(!dcFechaCambioMax.equals(""))
					condicion += " and trunc(ce.dc_fecha_cambio) between TO_DATE('"+dcFechaCambioMin.trim()+"','dd/mm/yyyy') and TO_DATE('"+dcFechaCambioMax.trim()+"','dd/mm/yyyy') ";
			if(!fnMontoMin.equals(""))
				if(!fnMontoMax.equals(""))
					condicion += " and d.fn_monto between " + fnMontoMin.trim() + " and "+ fnMontoMax.trim() +" ";
				else
					condicion += " and d.fn_monto = " + fnMontoMin.trim()+" ";
			if(!dfFechaDoctoMin.equals(""))
				if(!dfFechaDoctoMax.equals(""))
					condicion += " and trunc(d.df_fecha_docto) between TO_DATE('" + dfFechaDoctoMin + "','dd/mm/yyyy') and TO_DATE('" + dfFechaDoctoMax + "','dd/mm/yyyy') ";
			if(!dfFechaVencMin.equals(""))
				if(!dfFechaVencMax.equals(""))
					condicion += " and trunc(d.df_fecha_venc"+sFechaVencPyme+") between TO_DATE('" + dfFechaVencMin + "','dd/mm/yyyy') and TO_DATE('" + dfFechaVencMax + "','dd/mm/yyyy') ";
			if(!tipoFactoraje.equals(""))
				condicion += " and d.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ";
			if(icEpo.equals("") && icCambioEstatus.equals("") && icMoneda.equals("") && igNumeroDocto.equals("") && dcFechaCambioMin.equals("") && fnMontoMin.equals("") && fnMontoMax.equals("") && dfFechaDoctoMin.equals("") && dfFechaVencMin.equals("") && tipoFactoraje.equals(""))
				condicion += " and trunc(ce.dc_fecha_cambio) = TO_DATE('"+fechaActual+"','dd/mm/yyyy') ";
			
			qrySentencia.append(" " + condicion +
					" order by d.ig_numero_docto,ce.dc_fecha_cambio ");
			
			System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
			
		}catch(Exception e){
			System.out.println("CambioEstatusPymeDE::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentQueryFile(HttpServletRequest request)
  {
    String condicion = "";
    StringBuffer qrySentencia = new StringBuffer();

		HttpSession session = request.getSession();
		String icPyme = (String)session.getAttribute("iNoCliente");

	String icEpo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
	String igNumeroDocto = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto");
	String dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
	String dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
	String icMoneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
	String icCambioEstatus = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
	String fnMontoMin = (request.getParameter("fn_montoMin") == null) ? "" : request.getParameter("fn_montoMin");
	String fnMontoMax = (request.getParameter("fn_montoMax") == null) ? "" : request.getParameter("fn_montoMax");
	String dcFechaCambioMin = (request.getParameter("dc_fecha_cambioMin") == null) ? "" : request.getParameter("dc_fecha_cambioMin");
	String dcFechaCambioMax = (request.getParameter("dc_fecha_cambioMax") == null) ? "" : request.getParameter("dc_fecha_cambioMax");
	String dfFechaDoctoMin = (request.getParameter("df_fecha_doctoMin") == null) ? "" : request.getParameter("df_fecha_doctoMin");
	String dfFechaDoctoMax = (request.getParameter("df_fecha_doctoMax") == null) ? "" : request.getParameter("df_fecha_doctoMax");
	String tipoFactoraje = (request.getParameter("tipoFactoraje") ==null) ? "" : request.getParameter("tipoFactoraje");
	String fechaActual = (request.getParameter("fechaActual") == null) ? "" : request.getParameter("fechaActual");
	String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");
	String mandant = (request.getParameter("mandant") == null) ? "" : request.getParameter("mandant");
		/* Para manejo de idioma ingl�s, en el jsp se define un campo "idiomaUsuario"
		 * con los valores "ES" para espa�ol o "EN" para ingl�s.
		 */
		String idiomaUsuario =
				(request.getSession().getAttribute("sesIdiomaUsuario") == null ) ? "ES" :
				(String) request.getSession().getAttribute("sesIdiomaUsuario");

		String campoNombreMoneda = null;
		String campoCambioEstatus = null;
		if (idiomaUsuario.equals("EN")) {
			campoNombreMoneda = " M.cd_nombre_ing ";
			campoCambioEstatus = " cce.cd_descripcion_ing ";
		} else {
			campoNombreMoneda = " M.cd_nombre ";
			campoCambioEstatus = " cce.cd_descripcion ";
		}
		
		StringBuffer mandantecamp = new StringBuffer();
		StringBuffer mandantetabs = new StringBuffer();
		StringBuffer mandantecond = new StringBuffer();
		
		System.out.println(" ������ mandant -:Genera archivo:- ���� "+mandant+ " ������");
		if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
				mandantecamp.append( " cm.cg_razon_social AS mandante, " );
				mandantetabs.append(	"	,comcat_mandante cm " );
				if("M".equals(tipoFactoraje)){
				mandantecond.append(" AND d.ic_mandante = cm.ic_mandante" );		 
				}else{
				mandantecond.append(" AND d.ic_mandante = cm.ic_mandante(+)" );		 
				}
		}
		if(!"".equals(mandant)){
			mandantecond.append(	"  AND cm.IC_MANDANTE = "+mandant+" " );
		}

    try{
			qrySentencia.append(

			"select " +
			"    /*+ " +
			"        INDEX (CE CP_COMHIS_CAMBIO_ESTATUS_PK) " +
			"        INDEX (E CP_COMCAT_EPO_PK) " +
			"        INDEX (I CP_COMCAT_IF_PK) " +
			"        INDEX (I2 CP_COMCAT_IF_PK) " +
			"        INDEX (M CP_COMCAT_MONEDA_PK) " +
			"        INDEX (CCE CP_COMCAT_CAMBIO_ESTATUS_PK) " +
			"        USE_NL (D CE CV E I I2 M CCE) " +
			"    */  " +
			"        (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) AS NOMBRE_EPO,  " +
			"        d.ig_numero_docto,  " +
			"        TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') AS DF_FECHA_DOCTO,  " +
			"        TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'dd/mm/yyyy') AS DF_FECHA_VENC,  " +
			"        TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') AS DC_FECHA_CAMBIO,  " +
			"        d.ic_moneda,  " +
			campoNombreMoneda + ", " +
			"        d.fn_monto, " +
			"        d.fn_monto_dscto, " +
			campoCambioEstatus + ", " +
			"        ce.ct_cambio_motivo,  " +
			"        decode(d.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl) as fn_aforo,  " +
			"        cce.ic_cambio_estatus,  " +
			"        ce.fn_monto_anterior,  " +
			"        d.fn_porc_anticipo,  " +
			"        (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as NombreIF,  " +
			"        d.cs_dscto_especial,  " +
			"        I2.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO,  " +
			"        d.FN_PORC_BENEFICIARIO,  " +
			"        (d.FN_MONTO * d.FN_PORC_BENEFICIARIO ) / 100 RECIBIR_BENEFICIARIO," +
			"    	   ce.fn_monto_nuevo, "+
			" 			tf.cg_nombre as TIPO_FACTORAJE, "+mandantecamp+
			"		   'CambioEstatusPymeDE::getDocumentQueryFile' " +
			"from  " +
			"        com_documento d,  " +
			"        comhis_cambio_estatus ce,  " +
			"        comvis_aforo cv,  " +
			"        comcat_epo e,  " +
			"        comcat_if I,  " +
			"        comcat_if I2,  " +
			"        comcat_moneda m,  " +
			"        comcat_cambio_estatus cce, " +
			"  		comcat_tipo_factoraje tf " +mandantetabs+
			"where   d.ic_epo = e.ic_epo   " +
			"        and d.ic_moneda = m.ic_moneda  " +
			"        and d.ic_documento = ce.ic_documento  " +
			"        and d.ic_epo = cv.ic_epo  " +
			"        and ce.ic_cambio_estatus = cce.ic_cambio_estatus  " +
			"        and d.ic_if = I.ic_if(+)  " +
			"        and d.ic_beneficiario = I2.ic_if (+)  " +
			"        and ce.ic_cambio_estatus in (2,4,5,6,8,23,28,29,41) " +
			"  		and d.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+mandantecond+
			"        and d.ic_pyme =  " + icPyme);

         if (!icEpo.equals(""))
            condicion += " and d.ic_epo = " + icEpo;
         if (!icCambioEstatus.equals(""))
				if (icCambioEstatus.equals("23"))
				condicion += " and ce.ic_cambio_estatus in (23,41)";
				else
				condicion += " and ce.ic_cambio_estatus = "+icCambioEstatus.trim();
         if (!icMoneda.equals(""))
            condicion += " and d.ic_moneda = " + icMoneda.trim();
         if (!igNumeroDocto.equals(""))
            condicion += " and d.ig_numero_docto = '"+igNumeroDocto.trim()+"'";
         if(!dcFechaCambioMin.equals(""))
            if(!dcFechaCambioMax.equals(""))
               condicion += " and trunc(ce.dc_fecha_cambio) between TO_DATE('"+dcFechaCambioMin.trim()+"','dd/mm/yyyy') and TO_DATE('"+dcFechaCambioMax.trim()+"','dd/mm/yyyy') ";
         if(!fnMontoMin.equals(""))
            if(!fnMontoMax.equals(""))
               condicion += " and d.fn_monto between " + fnMontoMin.trim() + " and "+ fnMontoMax.trim() +" ";
            else
               condicion += " and d.fn_monto = " + fnMontoMin.trim()+" ";
         if(!dfFechaDoctoMin.equals(""))
            if(!dfFechaDoctoMax.equals(""))
               condicion += " and trunc(d.df_fecha_docto) between TO_DATE('" + dfFechaDoctoMin + "','dd/mm/yyyy') and TO_DATE('" + dfFechaDoctoMax + "','dd/mm/yyyy') ";
         if(!dfFechaVencMin.equals(""))
            if(!dfFechaVencMax.equals(""))
               condicion += " and trunc(d.df_fecha_venc"+sFechaVencPyme+") between TO_DATE('" + dfFechaVencMin + "','dd/mm/yyyy') and TO_DATE('" + dfFechaVencMax + "','dd/mm/yyyy') ";
         if(!tipoFactoraje.equals(""))
            condicion += " and d.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ";
         if(icEpo.equals("") && icCambioEstatus.equals("") && icMoneda.equals("") && igNumeroDocto.equals("") && dcFechaCambioMin.equals("") && fnMontoMin.equals("") && fnMontoMax.equals("") && dfFechaDoctoMin.equals("") && dfFechaVencMin.equals("") && tipoFactoraje.equals(""))
            condicion += " and trunc(ce.dc_fecha_cambio) = TO_DATE('"+fechaActual+"','dd/mm/yyyy') ";

	     qrySentencia.append(" " + condicion +
	       	" order by ce.ic_cambio_estatus,d.ig_numero_docto,ce.dc_fecha_cambio ");

	    System.out.println("EL query de DocumentQueryFile es: "+qrySentencia.toString());

    }catch(Exception e){
      System.out.println("CambioEstatusPymeDE::getDocumentQueryFileException "+e);
    }
    return qrySentencia.toString();
  }

}