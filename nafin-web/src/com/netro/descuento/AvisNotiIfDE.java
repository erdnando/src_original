package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.IQueryGeneratorThreadRegExtJS;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

public class AvisNotiIfDE implements IQueryGenerator, IQueryGeneratorThreadRegExtJS{// IQueryGeneratorRegExtJS
  public AvisNotiIfDE() { }
	
	public String getAggregateCalculationQuery(HttpServletRequest request){
    String envia = (request.getParameter("envia") == null) ? "S" : request.getParameter("envia");
    String icEpo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
    String dfFechaNotMin = (request.getParameter("df_fecha_notMin") == null) ? "" : request.getParameter("df_fecha_notMin");
    String dfFechaNotMax = (request.getParameter("df_fecha_notMax") == null) ? "" : request.getParameter("df_fecha_notMax");
    String dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
    String dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
    String ccAcuse3 = (request.getParameter("cc_acuse3") == null) ? "" : request.getParameter("cc_acuse3");
    String fechaActual = (request.getParameter("FechaActual") == null) ? "" : request.getParameter("FechaActual");
    String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");

    String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");

		SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
		fechaActual = fecha2.format(new java.util.Date());

      // me traje la condici�n de todos los campos 
			String condicion = "";
			if(!dfFechaNotMin.equals(""))
			{
				if(!dfFechaNotMax.equals(""))
				condicion += " and A3.df_fecha_hora between TO_DATE('" + dfFechaNotMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaNotMax + " 23:59','dd/mm/yyyy hh24:mi') ";
			}
			if(!dfFechaVencMin.equals(""))
			{
				if(!dfFechaVencMax.equals(""))
				condicion += " and D.df_fecha_venc between TO_DATE('" + dfFechaVencMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaVencMax + " 23:59','dd/mm/yyyy hh24:mi') ";
			}			
			if (!icEpo.equals(""))
				condicion += " AND D.ic_epo = "+ icEpo;
			if (!ccAcuse3.equals(""))
				condicion += " AND A3.cc_acuse = '" + ccAcuse3 + "' ";
			if(!tipoFactoraje.equals(""))
				condicion += " and D.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ";
			if( icEpo.equals("") && dfFechaNotMin.equals("") && dfFechaNotMax.equals("") && ccAcuse3.equals("") && tipoFactoraje.equals("") )
				condicion += " and A3.df_fecha_hora between TO_DATE('" + fechaActual + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + fechaActual + " 23:59','dd/mm/yyyy hh24:mi') ";

			String query_Reporte =
						"SELECT   /*+index(ie) use_nl(d ds s a3 ie e p m)*/ " +
						"         COUNT (1) AS result_size, SUM (d.fn_monto) AS fn_monto, " +
						"         SUM (d.fn_monto_dscto) AS fn_monto_dscto, m.cd_nombre, d.ic_moneda, " +
						"         'AvisNotiIfDE::getAggregateCalculationQuery' AS pantalla " +
						"    FROM com_documento d, " +
						"         com_docto_seleccionado ds, " +
						"         com_solicitud s, " +
						"         com_acuse3 a3, " +
						"         comrel_if_epo ie, " +
						"         comcat_epo e, " +
						"         comcat_pyme p, " +
						"         comcat_moneda m " +
						"   WHERE d.ic_documento = ds.ic_documento " +
						"     AND d.cs_dscto_especial != 'C' " +
						"     AND ds.ic_documento = s.ic_documento " +
						"     AND s.cc_acuse = a3.cc_acuse " +
						"     AND d.ic_if = ie.ic_if " +	//Modificado por FODEA 17 antes ds.ic_if
						"     AND ds.ic_epo = ie.ic_epo " +
						"     AND ie.ic_if = "+iNoCliente+" " +
						"     AND ie.cs_vobo_nafin = 'S' " +
						"     AND d.ic_epo = e.ic_epo " +
						"     AND e.cs_habilitado = 'S' " +
						"     AND d.ic_pyme = p.ic_pyme " +
						"     AND d.ic_moneda = m.ic_moneda " +
						condicion;
						
    query_Reporte += " GROUP BY D.IC_MONEDA,M.CD_NOMBRE "+
                    " ORDER BY D.IC_MONEDA";
  	return query_Reporte;
  }
	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){

    String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
	  String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
	  String sOperaFactConMandato = (request.getParameter("sOperaFactConMandato") == null) ? "" : request.getParameter("sOperaFactConMandato");
	 
	 
	 
    StringBuffer query = new StringBuffer();
		query.append(
				"SELECT (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social), "+
				"to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, A3.cc_acuse, "+
				//"(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social), "+ a modificar por FODEA 17
				" decode(ds.cs_opera_fiso, 'S', i.cg_razon_social, 'N', (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)), "+  //FODEA 17
				"D.ig_numero_docto, to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, "+
				"to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, M.cd_nombre, D.fn_monto, "+
				"D.fn_porc_anticipo, D.fn_monto_dscto, D.CS_DSCTO_ESPECIAL "+
				", tf.cg_nombre as TIPO_FACTORAJE "+
				" ,'AvisNotiIfDE::getDocumentSummaryQueryForIds' AS PANTALLA, "+ 
				" decode(DS.cs_opera_fiso, 'S', P.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				"FROM COM_DOCUMENTO D, COM_DOCTO_SELECCIONADO DS, COMCAT_EPO E, comcat_if i, "+
				"COM_ACUSE3 A3, COMCAT_PYME P, COMCAT_MONEDA M, COMREL_IF_EPO IE, COM_SOLICITUD S , comcat_tipo_factoraje tf "+
				"WHERE D.ic_documento = DS.ic_documento " +
				" AND d.cs_dscto_especial != 'C' "+
				"AND DS.ic_documento = S.ic_documento " +
				"AND S.cc_acuse = A3.cc_acuse " +
				"AND D.ic_if = IE.ic_if " +	//Modificado por FODEA 17 antes ds.ic_if
				"  AND DS.ic_if = i.ic_if(+) "   + //FODEA 17
				"AND DS.ic_epo = IE.ic_epo " +
				"AND IE.ic_if = "+ iNoCliente + 
				" AND IE.cs_vobo_nafin = 'S' " +
				"AND D.ic_epo = E.ic_epo " +
				"AND D.ic_pyme = P.ic_pyme " +
				"AND D.ic_moneda = M.ic_moneda " +
				" AND D.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+
				" AND D.ic_documento in (");

			for (Iterator it = ids.iterator(); it.hasNext();){
				query.append(it.next().toString() + ",");
			}
			query = query.delete(query.length()-1,query.length());
			query.append(")");

		System.out.println("querySummaryforIds: "+query.toString());

		return query.toString();

  }

  // Devuelve el query de llaves primarias
	public String getDocumentQuery(HttpServletRequest request){

    String envia = (request.getParameter("envia") == null) ? "S" : request.getParameter("envia");
    String icEpo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
    String dfFechaNotMin = (request.getParameter("df_fecha_notMin") == null) ? "" : request.getParameter("df_fecha_notMin");
    String dfFechaNotMax = (request.getParameter("df_fecha_notMax") == null) ? "" : request.getParameter("df_fecha_notMax");
    String dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
    String dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
    String ccAcuse3 = (request.getParameter("cc_acuse3") == null) ? "" : request.getParameter("cc_acuse3");
    String fechaActual = (request.getParameter("FechaActual") == null) ? "" : request.getParameter("FechaActual");
    String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");

    String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");

		SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
		fechaActual = fecha2.format(new java.util.Date());


      // me traje la condici�n de todos los campos 
			String condicion = "";
			if(!dfFechaNotMin.equals(""))
			{
				if(!dfFechaNotMax.equals(""))
				condicion += " and A3.df_fecha_hora between TO_DATE('" + dfFechaNotMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaNotMax + " 23:59','dd/mm/yyyy hh24:mi') ";
			}
			if(!dfFechaVencMin.equals(""))
			{
				if(!dfFechaVencMax.equals(""))
				condicion += " and D.df_fecha_venc between TO_DATE('" + dfFechaVencMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaVencMax + " 23:59','dd/mm/yyyy hh24:mi') ";
			}			
			if (!icEpo.equals(""))
				condicion += " AND D.ic_epo = "+ icEpo;
			if (!ccAcuse3.equals(""))
				condicion += " AND A3.cc_acuse = '" + ccAcuse3 + "' ";
			if(!tipoFactoraje.equals(""))
				condicion += " and D.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ";
			if( icEpo.equals("") && dfFechaNotMin.equals("") && dfFechaNotMax.equals("") && ccAcuse3.equals("") && tipoFactoraje.equals("") ) {
				condicion += 
					"   AND a3.df_fecha_hora >= TO_DATE ('"+fechaActual+"', 'dd/mm/yyyy') " +
					"   AND a3.df_fecha_hora < (TO_DATE ('"+fechaActual+"', 'dd/mm/yyyy') + 1) ";
			}
			
  	String query_Reporte = 
		"SELECT /*+use_nl(d m ds p a3 ie e)*/ " +
		"       d.ic_documento, 'AvisNotiIfDE::getDocumentQuery' AS pantalla " +
		"  FROM com_solicitud s, " +
		"       com_documento d, " +
		"       comcat_moneda m, " +
		"       com_docto_seleccionado ds, " +
		"       comcat_pyme p, " +
		"       com_acuse3 a3, " +
		"       comrel_if_epo ie, " +
		"       comcat_epo e " +
		" WHERE d.ic_documento = ds.ic_documento " +
		"   AND d.cs_dscto_especial != 'C' " +
		"   AND ds.ic_documento = s.ic_documento " +
		"   AND s.cc_acuse = a3.cc_acuse " +
		"   AND d.ic_if = ie.ic_if " + //Modificado por FODEA 17 antes ds.ic_if
		"   AND ds.ic_epo = ie.ic_epo " +
		"   AND ie.ic_if = "+iNoCliente+" " +
		"   AND ie.cs_vobo_nafin = 'S' " +
		"   AND d.ic_epo = e.ic_epo " +
		"   AND e.cs_habilitado = 'S' " +
		"   AND d.ic_pyme = p.ic_pyme " +
		"   AND d.ic_moneda = m.ic_moneda " +
		condicion ;
	
	System.out.println("query_Reporte: "+query_Reporte);
  
  	return query_Reporte;
  }
	
	public String getDocumentQueryFile(HttpServletRequest request){
		String envia = (request.getParameter("envia") == null) ? "S" : request.getParameter("envia");
		String icEpo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String dfFechaNotMin = (request.getParameter("df_fecha_notMin") == null) ? "" : request.getParameter("df_fecha_notMin");
		String dfFechaNotMax = (request.getParameter("df_fecha_notMax") == null) ? "" : request.getParameter("df_fecha_notMax");
		String dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
		String dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
		String ccAcuse3 = (request.getParameter("cc_acuse3") == null) ? "" : request.getParameter("cc_acuse3");
		String fechaActual = (request.getParameter("FechaActual") == null) ? "" : request.getParameter("FechaActual");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
		String tipo = (request.getParameter("tipo") == null) ? "" : request.getParameter("tipo");
		
		SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
		fechaActual = fecha2.format(new java.util.Date());

      // me traje la condici�n de todos los campos 
		String condicion = "";
		if(!dfFechaNotMin.equals("")) {
			if(!dfFechaNotMax.equals(""))
				condicion += " and A3.df_fecha_hora between TO_DATE('" + dfFechaNotMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaNotMax + " 23:59','dd/mm/yyyy hh24:mi') ";
		}
		if(!dfFechaVencMin.equals("")) {
			if(!dfFechaVencMax.equals(""))
				condicion += " and D.df_fecha_venc between TO_DATE('" + dfFechaVencMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaVencMax + " 23:59','dd/mm/yyyy hh24:mi') ";
		}			
		if (!icEpo.equals(""))
			condicion += " AND D.ic_epo = "+ icEpo;
		if (!ccAcuse3.equals(""))
			condicion += " AND A3.cc_acuse = '" + ccAcuse3 + "' ";
		if(!tipoFactoraje.equals(""))
			condicion += " and D.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ";
		if( icEpo.equals("") && dfFechaNotMin.equals("") && dfFechaNotMax.equals("") && ccAcuse3.equals("") && tipoFactoraje.equals("") )
			condicion += " and A3.df_fecha_hora between TO_DATE('" + fechaActual + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + fechaActual + " 23:59','dd/mm/yyyy hh24:mi') ";

  		String query_Reporte =
			" SELECT /*+index(ie) use_nl(d ds s p a3 ie e m)*/"   +
			"         (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social),"   +
			" decode(ds.cs_opera_fiso, 'S', i.cg_razon_social, 'N', (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)) AS NOMPYME,"+ //FODEA 17
			"         TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') fecha_not, a3.cc_acuse,"   +
			"         (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social),"   +
			"         d.ig_numero_docto,"   +
			"         TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') fecha_emision,"   +
			"         TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') fecha_vencimiento,"   +
			"         m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto,"   +
			"         d.cs_dscto_especial, d.fn_monto_ant, '' AS ic_docto_asociado,"   +
			"	 		 		tf.cg_nombre as TIPO_FACTORAJE, "+
			"			 d.ic_documento, "  +
			"			 d.ic_epo, "  +
			"         'AvisNotiIfDE::getDocumentQueryFile' AS pantalla, "   +
			"		decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
			"   FROM com_documento d,"   +
			"        com_docto_seleccionado ds,"   +
			"        comcat_if i,"   + //FODEA 17
			"        com_solicitud s,"   +
			"        comcat_pyme p,"   +
			"        com_acuse3 a3,"   +
			"        comrel_if_epo ie,"   +
			"        comcat_epo e,"   +
			"        comcat_moneda m,"   +
			"		   	 comcat_tipo_factoraje tf "+
			"  WHERE d.ic_documento = ds.ic_documento"   +
			"    AND ds.ic_documento = s.ic_documento"   +
			"    AND d.ic_epo = e.ic_epo"   +
			"    AND ds.ic_if = i.ic_if(+)"   + //FODEA 17
			"    AND d.ic_pyme = p.ic_pyme"   +
			"    AND d.ic_moneda = m.ic_moneda"   +
			"    AND s.cc_acuse = a3.cc_acuse"   +
			"    AND d.ic_if = ie.ic_if"   + //Modificado por FODEA 17 antes ds.ic_if
			"    AND ds.ic_epo = ie.ic_epo"   +
			"    AND ie.ic_if = "+ iNoCliente + 
			"    AND ie.cs_vobo_nafin = 'S'"   +
			"    AND e.cs_habilitado = 'S' " +
			"    AND d.cs_dscto_especial != 'C'"   +
			" 	 AND d.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+
			"	AND rownum <= 500 "+
			condicion;

		if("D".equals(tipo)) {
 			query_Reporte +=
				" UNION ALL"   +
				" SELECT /*+index(d) use_nl(d d2 ds s p a3 ie e m)*/"   +
				"         (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social),"   +
				" decode(ds.cs_opera_fiso, 'S', i.cg_razon_social, 'N', (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)) AS NOMPYME, "+ //FODEA 17
				"         TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') fecha_not, a3.cc_acuse,"   +
				"         (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social),"   +
				"         d.ig_numero_docto,"   +
				"         TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') fecha_emision,"   +
				"         TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') fecha_vencimiento,"   +
				"         m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto,"   +
				"         d.cs_dscto_especial, d.fn_monto_ant, d2.ig_numero_docto AS ic_docto_asociado,"   +
				"				  tf.cg_nombre AS tipo_factoraje, "+
				"			 d.ic_documento, "  +
				"			 d.ic_epo, "  +
				"         'AvisNotiIfDE::getDocumentQueryFile' AS pantalla, "   +
				"		decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				"   FROM com_documento d,"   +
				"        com_documento d2,"   +
				"        com_docto_seleccionado ds,"   +
				"        comcat_if i,"   + //FODEA 17
				"        com_solicitud s,"   +
				"        comcat_pyme p,"   +
				"        com_acuse3 a3,"   +
				"        comrel_if_epo ie,"   +
				"        comcat_epo e,"   +
				"        comcat_moneda m,"   +
				"				 comcat_tipo_factoraje tf "+
				"  WHERE d.ic_documento = ds.ic_documento"   +
				"    AND d.ic_docto_asociado = s.ic_documento"   +
				"    AND d.ic_docto_asociado = d2.ic_documento"   +
				"    AND d.ic_epo = e.ic_epo"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND s.cc_acuse = a3.cc_acuse"   +
				"    AND d.ic_if = ie.ic_if"   + //Modificado por FODEA 17 antes ds.ic_if
				"    AND ds.ic_if = i.ic_if(+)"   + //FODEA 17
				"    AND ds.ic_epo = ie.ic_epo"   +
				"    AND e.cs_habilitado = 'S' "   +
				"    AND ie.ic_if = "+ iNoCliente + 
				"    AND ie.cs_vobo_nafin = 'S'"   +
				"		 AND d.cs_dscto_especial = tf.cc_tipo_factoraje "+
				"	AND rownum <= 500 "+
				condicion;
			 query_Reporte +=
				" UNION ALL"   +
				" SELECT /*+index(d) use_nl(d ds s p a3 ie e m nd)*/"   +
				"         distinct " +
				"         (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social),"   +
				" decode(ds.cs_opera_fiso, 'S', i.cg_razon_social, 'N', (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)) AS NOMPYME, "+ //FODEA 17
				"         TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') fecha_not, a3.cc_acuse,"   +
				"         (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social),"   +
				"         d.ig_numero_docto,"   +
				"         TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') fecha_emision,"   +
				"         TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') fecha_vencimiento,"   +
				"         m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto,"   +
				"         d.cs_dscto_especial, d.fn_monto_ant, '' AS ic_docto_asociado,"   +
				"				  tf.cg_nombre AS tipo_factoraje, "+
				"			 d.ic_documento, "  +
				"			 d.ic_epo, "  +
				"         'AvisNotiIfDE::getDocumentQueryFile' AS pantalla, "   +
				"		decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				"   FROM com_documento d,"   +
				"        com_docto_seleccionado ds,"   +
				"        comcat_if i,"   + //FODEA 17
				"        com_solicitud s,"   +
				"        comcat_pyme p,"   +
				"        com_acuse3 a3,"   +
				"        comrel_if_epo ie,"   +
				"        comcat_epo e,"   +
				"        comcat_moneda m,"   +
				"			comcat_tipo_factoraje tf, "+
				"			comrel_nota_docto nd " +
				"  WHERE d.ic_documento = ds.ic_documento"   +
				"    AND nd.ic_documento     = s.ic_documento "  +
				"    AND nd.ic_nota_credito  = d.ic_documento "  +
				"    AND d.ic_epo = e.ic_epo"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND s.cc_acuse = a3.cc_acuse"   +
				"    AND d.ic_if = ie.ic_if"   + //Modificado por FODEA 17 antes ds.ic_if
				"    AND ds.ic_if = i.ic_if(+)"   + //FODEA 17
				"    AND ds.ic_epo = ie.ic_epo"   +
				"    AND e.cs_habilitado = 'S' "   +
				"    AND ie.ic_if = "+ iNoCliente + 
				"    AND ie.cs_vobo_nafin = 'S'"   +
				"		 AND d.cs_dscto_especial = tf.cc_tipo_factoraje "+
				"	AND rownum <= 500 "+
				condicion;	
		}
 
		System.out.println("query_Reporte: "+query_Reporte);
		return query_Reporte;
	}
	
	/****************************************************************************
	 *		 							     MIGRACION 											 *
	 *                      	  Gustavo Arellano										 *
	 *                           	 AvisNotiIfDE											 *
	 ***************************************************************************/
	 
	private List conditions;
	StringBuffer strQuery;
	private String iNoCliente;
	private String dfFechaNotMin;
	private String dfFechaNotMax;
	private String fechaActual;
	private String dfFechaVencMin;
	private String dfFechaVencMax;
	private String icEpo;
	private String ccAcuse3;
	private String tipoFactoraje;
	private String tipo;
	private String tipo_;
	private int countReg;
	private static final Log log = ServiceLocator.getInstance().getLog(AvisNotiIfDE.class);
	
	public String getAggregateCalculationQuery() {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		
		SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
		fechaActual = fecha2.format(new java.util.Date());

      // me traje la condici�n de todos los campos 
			String condicion = "";
			if(!dfFechaNotMin.equals(""))
			{
				if(!dfFechaNotMax.equals(""))
				condicion += " and A3.df_fecha_hora between TO_DATE('" + dfFechaNotMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaNotMax + " 23:59','dd/mm/yyyy hh24:mi') ";
			}
			if(!dfFechaVencMin.equals(""))
			{
				if(!dfFechaVencMax.equals(""))
				condicion += " and D.df_fecha_venc between TO_DATE('" + dfFechaVencMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaVencMax + " 23:59','dd/mm/yyyy hh24:mi') ";
			}			
			if (!icEpo.equals(""))
				condicion += " AND D.ic_epo = "+ icEpo;
			if (!ccAcuse3.equals(""))
				condicion += " AND A3.cc_acuse = '" + ccAcuse3 + "' ";
			if(!tipoFactoraje.equals(""))
				condicion += " and D.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ";
			if( icEpo.equals("") && dfFechaNotMin.equals("") && dfFechaNotMax.equals("") && ccAcuse3.equals("") && tipoFactoraje.equals("") )
				condicion += " and A3.df_fecha_hora between TO_DATE('" + fechaActual + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + fechaActual + " 23:59','dd/mm/yyyy hh24:mi') ";

			strQuery.append(
						"SELECT   /*+index(ie) use_nl(d ds s a3 ie e p m)*/ " +
						"         COUNT (1) AS result_size, SUM (d.fn_monto) AS fn_monto, " +
						"         SUM (d.fn_monto_dscto) AS fn_monto_dscto, m.cd_nombre, d.ic_moneda, " +
						"         'AvisNotiIfDE::getAggregateCalculationQuery' AS pantalla " +
						"    FROM com_documento d, " +
						"         com_docto_seleccionado ds, " +
						"         com_solicitud s, " +
						"         com_acuse3 a3, " +
						"         comrel_if_epo ie, " +
						"         comcat_epo e, " +
						"         comcat_pyme p, " +
						"         comcat_moneda m " +
						"   WHERE d.ic_documento = ds.ic_documento " +
						"     AND d.cs_dscto_especial != 'C' " +
						"     AND ds.ic_documento = s.ic_documento " +
						"     AND s.cc_acuse = a3.cc_acuse " +
						"     AND d.ic_if = ie.ic_if " + //Modificado por FODEA 17 antes ds.ic_if
						"     AND ds.ic_epo = ie.ic_epo " +
						"     AND ie.ic_if = "+iNoCliente+" " +
						"     AND ie.cs_vobo_nafin = 'S' " +
						"     AND d.ic_epo = e.ic_epo " +
						"     AND e.cs_habilitado = 'S' " +
						"     AND d.ic_pyme = p.ic_pyme " +
						"     AND d.ic_moneda = m.ic_moneda " +
						condicion);
						
    strQuery.append(" GROUP BY D.IC_MONEDA,M.CD_NOMBRE "+
                    " ORDER BY D.IC_MONEDA");
		
		System.out.println("getAggregateCalculationQuery: "+strQuery.toString());
		return strQuery.toString();
	}
	
	public String getDocumentQuery() {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		
		SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
		fechaActual = fecha2.format(new java.util.Date());


      // me traje la condici�n de todos los campos 
			String condicion = "";
			if(!dfFechaNotMin.equals(""))
			{
				if(!dfFechaNotMax.equals(""))
				condicion += " and A3.df_fecha_hora between TO_DATE('" + dfFechaNotMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaNotMax + " 23:59','dd/mm/yyyy hh24:mi') ";
			}
			if(!dfFechaVencMin.equals(""))
			{
				if(!dfFechaVencMax.equals(""))
				condicion += " and D.df_fecha_venc between TO_DATE('" + dfFechaVencMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaVencMax + " 23:59','dd/mm/yyyy hh24:mi') ";
			}			
			if (!icEpo.equals(""))
				condicion += " AND D.ic_epo = "+ icEpo;
			if (!ccAcuse3.equals(""))
				condicion += " AND A3.cc_acuse = '" + ccAcuse3 + "' ";
			if(!tipoFactoraje.equals(""))
				condicion += " and D.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ";
			if( icEpo.equals("") && dfFechaNotMin.equals("") && dfFechaNotMax.equals("") && ccAcuse3.equals("") && tipoFactoraje.equals("") ) {
				condicion += 
					"   AND a3.df_fecha_hora >= TO_DATE ('"+fechaActual+"', 'dd/mm/yyyy') " +
					"   AND a3.df_fecha_hora < (TO_DATE ('"+fechaActual+"', 'dd/mm/yyyy') + 1) ";
			}
			
		strQuery.append( 
			"SELECT /*+use_nl(d m ds p a3 ie e)*/ " +
			"       d.ic_documento, 'AvisNotiIfDE::getDocumentQuery' AS pantalla " +
			"  FROM com_solicitud s, " +
			"       com_documento d, " +
			"       comcat_moneda m, " +
			"       com_docto_seleccionado ds, " +
			"       comcat_pyme p, " +
			"       com_acuse3 a3, " +
			"       comrel_if_epo ie, " +
			"       comcat_epo e " +
			" WHERE d.ic_documento = ds.ic_documento " +
			"   AND d.cs_dscto_especial != 'C' " +
			"   AND ds.ic_documento = s.ic_documento " +
			"   AND s.cc_acuse = a3.cc_acuse " +
			"   AND d.ic_if = ie.ic_if " + //Modificado por FODEA 17 antes ds.ic_if
			"   AND ds.ic_epo = ie.ic_epo " +
			"   AND ie.ic_if = "+iNoCliente+" " +
			"   AND ie.cs_vobo_nafin = 'S' " +
			"   AND d.ic_epo = e.ic_epo " +
			"   AND e.cs_habilitado = 'S' " +
			"   AND d.ic_pyme = p.ic_pyme " +
			"   AND d.ic_moneda = m.ic_moneda " +
			condicion );
		
		System.out.println("getDocumentQuery: "+strQuery.toString());
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		StringBuffer clavesCombinaciones = new StringBuffer("");
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		
		for (int i = 0; i < pageIds.size(); i++) {
			List lItem = (ArrayList)pageIds.get(i);
			clavesCombinaciones.append("'"+lItem.get(0).toString()+"'"+",");
		}
		clavesCombinaciones.deleteCharAt(clavesCombinaciones.length()-1);
		
		strQuery.append(
				"SELECT (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) AS NUEVAEPO, "+
				"to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, A3.cc_acuse, "+
				//"(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) AS NOMPYME, "+ //Linea a modificar por FODEA 17
				" decode(DS.cs_opera_fiso, 'S', I.cg_razon_social, 'N', (decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social)) AS NOMPYME, "+ //FODEA 17
				"D.ig_numero_docto, to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, "+
				"to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, M.cd_nombre, D.fn_monto, "+
				"D.fn_porc_anticipo, D.fn_monto_dscto, D.CS_DSCTO_ESPECIAL "+
				", tf.cg_nombre as TIPO_FACTORAJE "+
				" ,'AvisNotiIfDE::getDocumentSummaryQueryForIds' AS PANTALLA, "+  
				"		decode(DS.cs_opera_fiso, 'S', P.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				" FROM COM_DOCUMENTO D, COM_DOCTO_SELECCIONADO DS, COMCAT_EPO E, COMCAT_IF I, "+ //Se agrega la tabla comcat_if FODEA 17
				"COM_ACUSE3 A3, COMCAT_PYME P, COMCAT_MONEDA M, COMREL_IF_EPO IE, COM_SOLICITUD S , comcat_tipo_factoraje tf "+
				"WHERE D.ic_documento = DS.ic_documento " +
				" AND d.cs_dscto_especial != 'C' "+
				" AND DS.ic_if = I.ic_if "+ //FODEA 17
				" AND DS.ic_documento = S.ic_documento " +  
				" AND S.cc_acuse = A3.cc_acuse " +
				" AND D.ic_if = IE.ic_if " + //Modificado por FODEA 17 antes ds.ic_if
				" AND DS.ic_epo = IE.ic_epo " +
				" AND IE.ic_if = "+ iNoCliente + 
				" AND IE.cs_vobo_nafin = 'S' " +   
				" AND D.ic_epo = E.ic_epo " +              
				" AND D.ic_pyme = P.ic_pyme " +  
				" AND D.ic_moneda = M.ic_moneda " +
				" AND D.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+
				" AND D.ic_documento in ("+clavesCombinaciones+")");
   
      System.out.println("\n\n\n getDocumentSummaryQueryForIds(): " + strQuery.toString());
		return strQuery.toString();
	}
	
	/*Este metodo debe generar todos lo archivos*/
	public String getDocumentQueryFile(){
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		System.out.println("getDocumentQueryFile");
		SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
		fechaActual = fecha2.format(new java.util.Date());
		
      // me traje la condici�n de todos los campos 
		String condicion = "";
		
		if(!dfFechaNotMin.equals("")) {
			if(!dfFechaNotMax.equals(""))
				condicion += " and A3.df_fecha_hora between TO_DATE('" + dfFechaNotMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaNotMax + " 23:59','dd/mm/yyyy hh24:mi') ";
		}
		if(!dfFechaVencMin.equals("")) {
			if(!dfFechaVencMax.equals(""))
				condicion += " and D.df_fecha_venc between TO_DATE('" + dfFechaVencMin + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + dfFechaVencMax + " 23:59','dd/mm/yyyy hh24:mi') ";
		}			
		if (!icEpo.equals(""))
			condicion += " AND D.ic_epo = "+ icEpo;
		if (!ccAcuse3.equals(""))
			condicion += " AND A3.cc_acuse = '" + ccAcuse3 + "' ";
		if(!tipoFactoraje.equals(""))
			condicion += " and D.CS_DSCTO_ESPECIAL='"+tipoFactoraje+"' ";
		if( icEpo.equals("") && dfFechaNotMin.equals("") && dfFechaNotMax.equals("") && ccAcuse3.equals("") && tipoFactoraje.equals("") )
			condicion += " and A3.df_fecha_hora between TO_DATE('" + fechaActual + " 00:00','dd/mm/yyyy hh24:mi') and TO_DATE('" + fechaActual + " 23:59','dd/mm/yyyy hh24:mi') ";

		
		strQuery.append(
		" SELECT /*+index(ie) use_nl(d ds s p a3 ie e m)*/"   +
			" (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) AS NUEVAEPO, "+ //Agregado por FODEA 17
			//"         (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social),"   + Linea a modificar por FODEA 17
			" decode(ds.cs_opera_fiso, 'S', i.cg_razon_social, 'N', (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)) AS NOMPYME, "+ //FODEA 17
			"         TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') fecha_not, a3.cc_acuse,"   +
			"         (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social),"   +
			"         d.ig_numero_docto,"   +
			"         TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') fecha_emision,"   +
			"         TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') fecha_vencimiento,"   +
			"         m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto,"   +
			"         d.cs_dscto_especial, d.fn_monto_ant, '' AS ic_docto_asociado,"   +
			"	 		 tf.cg_nombre as TIPO_FACTORAJE, "+
			"			 d.ic_documento, "  +
			"			 d.ic_epo, "  +
			"         'AvisNotiIfDE::getDocumentQueryFile' AS pantalla, "   +
			"		decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
			"   FROM com_documento d,"   +
			"        com_docto_seleccionado ds,"   +
			"        comcat_if i,"   + //FODEA 17
			"        com_solicitud s,"   +
			"        comcat_pyme p,"   +
			"        com_acuse3 a3,"   +
			"        comrel_if_epo ie,"   +
			"        comcat_epo e,"   +
			"        comcat_moneda m,"   +
			"		   	 comcat_tipo_factoraje tf "+
			"  WHERE d.ic_documento = ds.ic_documento"   +
			"    AND ds.ic_documento = s.ic_documento"   +
			"    AND ds.ic_if = i.ic_if(+)"   + //FODEA 17
			"    AND d.ic_epo = e.ic_epo"   +
			"    AND d.ic_pyme = p.ic_pyme"   +
			"    AND d.ic_moneda = m.ic_moneda"   +
			"    AND s.cc_acuse = a3.cc_acuse"   +
			"    AND d.ic_if = ie.ic_if"   + //Modificado por FODEA 17 antes ds.ic_if
			"    AND ds.ic_epo = ie.ic_epo"   +
			"    AND ie.ic_if = "+ iNoCliente + 
			"    AND ie.cs_vobo_nafin = 'S'"   +
			"    AND e.cs_habilitado = 'S' " +
			"    AND d.cs_dscto_especial != 'C'"   +
			" 	 AND d.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+
			condicion);
			
			if("D".equals(tipo)) {
 			strQuery.append(
				" UNION ALL"   +
				" SELECT /*+index(d) use_nl(d d2 ds s p a3 ie e m)*/"   +
				" (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) AS NUEVAEPO, "+ //Agregado por FODEA 17
				//"         (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social),"   + Linea a modifcar por FODEA 17
				" decode(ds.cs_opera_fiso, 'S', i.cg_razon_social, 'N', (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)) AS NOMPYME, "+ //FODEA 17
				"         TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') fecha_not, a3.cc_acuse,"   +
				"         (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social),"   +
				"         d.ig_numero_docto,"   +
				"         TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') fecha_emision,"   +
				"         TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') fecha_vencimiento,"   +
				"         m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto,"   +
				"         d.cs_dscto_especial, d.fn_monto_ant, d2.ig_numero_docto AS ic_docto_asociado,"   +
				"				  tf.cg_nombre AS tipo_factoraje, "+
				"			 d.ic_documento, "  +
				"			 d.ic_epo, "  +
				"         'AvisNotiIfDE::getDocumentQueryFile' AS pantalla, "   +
				"		decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				"   FROM com_documento d,"   +
				"        com_documento d2,"   +
				"        com_docto_seleccionado ds,"   +
				"        comcat_if i,"   + //FODEA 17
				"        com_solicitud s,"   +
				"        comcat_pyme p,"   +
				"        com_acuse3 a3,"   +
				"        comrel_if_epo ie,"   +
				"        comcat_epo e,"   +
				"        comcat_moneda m,"   +
				"				 comcat_tipo_factoraje tf "+
				"  WHERE d.ic_documento = ds.ic_documento"   +
				"    AND d.ic_docto_asociado = s.ic_documento"   +
				"    AND d.ic_docto_asociado = d2.ic_documento"   +
				"    AND d.ic_epo = e.ic_epo"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND s.cc_acuse = a3.cc_acuse"   +
				"    AND d.ic_if = ie.ic_if"   + //Modificado por FODEA 17 antes ds.ic_if
				"    AND ds.ic_if = i.ic_if(+)"   + //FODEA 17
				"    AND ds.ic_epo = ie.ic_epo"   +
				"    AND e.cs_habilitado = 'S' "   +
				"    AND ie.ic_if = "+ iNoCliente + 
				"    AND ie.cs_vobo_nafin = 'S'"   +
				"		 AND d.cs_dscto_especial = tf.cc_tipo_factoraje "+
				condicion);
			 strQuery.append(
				" UNION ALL"   +
				" SELECT /*+index(d) use_nl(d ds s p a3 ie e m nd)*/"   +
				"         distinct " +
				" (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) AS NUEVAEPO, "+ //Agregado por FODEA 17
				//"         (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social),"   +
				" decode(ds.cs_opera_fiso, 'S', i.cg_razon_social, 'N', (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)) AS NOMPYME, "+ //FODEA 17
				"         TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') fecha_not, a3.cc_acuse,"   +
				"         (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social),"   +
				"         d.ig_numero_docto,"   +
				"         TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') fecha_emision,"   +
				"         TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') fecha_vencimiento,"   +
				"         m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto,"   +
				"         d.cs_dscto_especial, d.fn_monto_ant, '' AS ic_docto_asociado,"   +
				"				  tf.cg_nombre AS tipo_factoraje, "+
				"			 d.ic_documento, "  +
				"			 d.ic_epo, "  +
				"         'AvisNotiIfDE::getDocumentQueryFile' AS pantalla, "   +
				"		decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				"   FROM com_documento d,"   +
				"        com_docto_seleccionado ds,"   +
				"        comcat_if i,"   + //FODEA 17
				"        com_solicitud s,"   +
				"        comcat_pyme p,"   +
				"        com_acuse3 a3,"   +
				"        comrel_if_epo ie,"   +
				"        comcat_epo e,"   +
				"        comcat_moneda m,"   +
				"			comcat_tipo_factoraje tf, "+
				"			comrel_nota_docto nd " +
				"  WHERE d.ic_documento = ds.ic_documento"   +
				"    AND nd.ic_documento     = s.ic_documento "  +
				"    AND nd.ic_nota_credito  = d.ic_documento "  +
				"    AND d.ic_epo = e.ic_epo"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND s.cc_acuse = a3.cc_acuse"   +
				"    AND d.ic_if = ie.ic_if"   + //Modificado por FODEA 17 antes ds.ic_if
				"    AND ds.ic_if = i.ic_if(+)"   + //FODEA 17
				"    AND ds.ic_epo = ie.ic_epo"   +
				"    AND e.cs_habilitado = 'S' "   +
				"    AND ie.ic_if = "+ iNoCliente + 
				"    AND ie.cs_vobo_nafin = 'S'"   +
				"		 AND d.cs_dscto_especial = tf.cc_tipo_factoraje "+
				condicion);	
		}
		System.out.println("\n\n\n getDocumentQueryFile:: " + strQuery.toString() + "\n\n");
      
		return strQuery.toString();
	}
	
		/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo = new CreaArchivo();
		int totalDoctosMN        = 0;
		int totalDoctosDL        = 0;
		double totalMontoMN      = 0;
		double totalMontoDL      = 0;
		double totalMontoDsctoMN = 0;
		double totalMontoDsctoDL = 0;

		if ("XLS".equals(tipo)) {
			try {
				int registros = 0;

				while(rs.next()){
					if(registros==0) {
						contenidoArchivo.append("EPO, Fecha de notificaci�n, Acuse de notificaci�n, Nombre del proveedor, Numero de documento, Fecha de emisi�n, Fecha de vencimiento, Moneda");
						contenidoArchivo.append(", Monto, Porcentaje Descuento, Recurso Garantia, Monto Descontar, Referencia ");
					}
					registros++;
					//String nuevaepo       = rs.getString(1)                  ==null?"":rs.getString(1); Modificado por FODEA 7
					String nuevaepo         = rs.getString("NUEVAEPO")         ==null?"":rs.getString("NUEVAEPO"); //FODEA 17
					String fecha_not        = rs.getString("fecha_not")        ==null?"":rs.getString("fecha_not");
					String cc_acuse         = rs.getString("cc_acuse")         ==null?"":rs.getString("cc_acuse");
					String nompyme          = rs.getString("NOMPYME")          ==null?"":rs.getString("NOMPYME"); //FODEA 17
					String ig_numero_docto  = rs.getString("ig_numero_docto")  ==null?"":rs.getString("ig_numero_docto");
					String fecha_emision    = rs.getString("fecha_emision")    ==null?"":rs.getString("fecha_emision");
					String fecha_vencimiento= rs.getString("fecha_vencimiento")==null?"":rs.getString("fecha_vencimiento");
					String cd_nombre        = rs.getString("cd_nombre")        ==null?"":rs.getString("cd_nombre");
					String fn_monto         = rs.getString("fn_monto")         ==null?"":rs.getString("fn_monto");
					String fn_porc_anticipo = rs.getString("fn_porc_anticipo") ==null?"":rs.getString("fn_porc_anticipo");
					String fn_monto_dscto   = rs.getString("fn_monto_dscto")   ==null?"":rs.getString("fn_monto_dscto");
					String cs_dscto_especial= rs.getString("cs_dscto_especial")==null?"":rs.getString("cs_dscto_especial");
					String tipo_factoraje   = rs.getString("tipo_factoraje")   ==null?"":rs.getString("tipo_factoraje");
					String referencia_pyme  = rs.getString("REFERENCIA_PYME")  ==null?"":rs.getString("REFERENCIA_PYME"); //FODEA 17

					double resta = new Double(fn_monto).doubleValue() - new Double(fn_monto_dscto).doubleValue();
					String dblRecurso = Double.toString(resta);
					//nuevaepo = nuevaepo.replaceAll("*","");
					//nompyme  = nompyme.replaceAll("*","");

					contenidoArchivo.append("\n"+
						nuevaepo.replaceAll(",","").trim() +","+
						fecha_not +","+
						cc_acuse +","+
						nompyme.replaceAll(",","").trim() +","+
						ig_numero_docto +","+    
						fecha_emision +","+
						fecha_vencimiento +","+
						cd_nombre +","+  
						//tipo_factoraje +","+
						Comunes.formatoDecimal(fn_monto,2,false) +","+
						Comunes.formatoDecimal(fn_porc_anticipo,0,false)  +","+
						Comunes.formatoDecimal(dblRecurso, 2, false) + "," +
						Comunes.formatoDecimal(fn_monto_dscto,2,false)+","+
						referencia_pyme);
						//Float.toString((Float.parseFloat(fn_monto) - Float.parseFloat(fn_monto_dscto))) +","+
						//fn_monto_dscto +",");
				}

				if(registros >= 1){
					if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
						nombreArchivo="ERROR";
					}else{
						nombreArchivo = archivo.nombre;
					}
				}	
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}

		else if("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
				horaActual, "formas", ComunesPDF.RIGHT);
				pdfDoc.addText(" ", "formas", ComunesPDF.RIGHT);
                
                String mensaje = "Las operaciones que aparecen en esta p�gina fueron notificadas a la EMPRESA DE PRIMER ORDEN de conformidad y para efectos de los art�culos 427 de la Ley General de T�tulos y Operaciones de Cr�dito, 32 C del C�digo Fiscal de la Federaci�n y 2038 del C�digo Civil Federal seg�n corresponda, para los efectos legales conducentes.\n"+
			    "Por otra parte, a partir del 17 de octubre de 2018, el PROVEEDOR ha manifestado bajo protesta de decir verdad, que s� ha emitido o emitir� a la EMPRESA DE PRIMER ORDEN el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.";
                pdfDoc.addText(mensaje, "formas", ComunesPDF.LEFT);
			    pdfDoc.addText(" ", "formas", ComunesPDF.RIGHT);
                
				pdfDoc.setLTable(14, 100); //Se agrego una columna por FODEA 17
				pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de notificaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Acuse de notificaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre del proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER); 
				pdfDoc.setLCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Recurso en Garantia","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Referencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();
				countReg = 0;
				while (rs.next()){
					String nuevaepo          = rs.getString("NUEVAEPO")==null?"":rs.getString("NUEVAEPO"); //FODEA 17
					String fecha_not         = rs.getString("fecha_not")==null?"":rs.getString("fecha_not");
					String cc_acuse          = rs.getString("cc_acuse")==null?"":rs.getString("cc_acuse");
					String nompyme           = rs.getString("NOMPYME")==null?"":rs.getString("NOMPYME"); //FODEA 17
					String ig_numero_docto   = rs.getString("ig_numero_docto")==null?"":rs.getString("ig_numero_docto");
					String fecha_emision     = rs.getString("fecha_emision")==null?"":rs.getString("fecha_emision");
					String fecha_vencimiento = rs.getString("fecha_vencimiento")==null?"":rs.getString("fecha_vencimiento");
					String cd_nombre         = rs.getString("cd_nombre")==null?"":rs.getString("cd_nombre");
					String fn_monto          = rs.getString("fn_monto")==null?"0":rs.getString("fn_monto");
					String fn_porc_anticipo  = rs.getString("fn_porc_anticipo")==null?"":rs.getString("fn_porc_anticipo");
					String fn_monto_dscto    = rs.getString("fn_monto_dscto")==null?"0":rs.getString("fn_monto_dscto");
					String cs_dscto_especial = rs.getString("cs_dscto_especial")==null?"":rs.getString("cs_dscto_especial");
					String tipo_factoraje    = rs.getString("tipo_factoraje")==null?"":rs.getString("tipo_factoraje");
					String referencia_pyme   = rs.getString("REFERENCIA_PYME")==null?"":rs.getString("REFERENCIA_PYME");

					String recGarantia = Float.toString((Float.parseFloat(fn_monto) - Float.parseFloat(fn_monto_dscto)));

					pdfDoc.setLCell(nuevaepo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_not,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cc_acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nompyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_emision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_vencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cd_nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipo_factoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(fn_monto,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(fn_porc_anticipo,2) + "%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(recGarantia,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(fn_monto_dscto,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(referencia_pyme,"formas",ComunesPDF.CENTER);
					
					if(cd_nombre.equals("MONEDA NACIONAL")){
						totalDoctosMN++;
						totalMontoMN+=Double.parseDouble(fn_monto);
						 totalMontoDsctoMN+=Double.parseDouble(fn_monto_dscto);
					} else{
						totalDoctosDL++;
						totalMontoDL+=Double.parseDouble(fn_monto);
						totalMontoDsctoDL+=Double.parseDouble(fn_monto_dscto);
					}
					countReg++;
					//System.out.println("Registros procesados: " + countReg );
				}
				pdfDoc.addLTable();

				try{
					pdfDoc.setLTable(4, 100);
					pdfDoc.setLCell("Totales ",           "celda01",ComunesPDF.CENTER,4);
					pdfDoc.setLCell("Tipo de moneda ",    "celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Total de Documentos","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto ",             "celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto DEscuento ",   "celda01",ComunesPDF.CENTER);
					pdfDoc.setLHeaders();
					pdfDoc.setLCell("MONEDA NACIONAL","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(""+totalDoctosMN,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoDsctoMN,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("DOLAR AMERICANO","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(""+totalDoctosDL,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoDL,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoDsctoDL,2),"formas",ComunesPDF.CENTER);
					pdfDoc.addLTable();
				}catch(Exception e){
					System.err.println("Error en los totales." + e);
				}

				pdfDoc.endDocument();

			}catch (Throwable e) {
				throw new AppException("Error al generar el archivo",e);
			}finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo 	= new CreaArchivo();
		
		if ("PDFPrint".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setLTable(14, 100);     //Se agrego una columna por FODEA 17         
				pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de notificaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Acuse de notificaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre del proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Recurso en Garantia","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Referencia","celda01",ComunesPDF.CENTER);
								
				while (rs.next()) {
					//String nuevaepo 			= rs.getString(1)==null?"":rs.getString(1);
					String nuevaepo 			= rs.getString("NUEVAEPO")==null?"":rs.getString("NUEVAEPO"); //FODEA 17
					String fecha_not 			= rs.getString("fecha_not")==null?"":rs.getString("fecha_not");
					String cc_acuse 			= rs.getString("cc_acuse")==null?"":rs.getString("cc_acuse");
					//String nompyme 			= rs.getString(4)==null?"":rs.getString(4);
					String nompyme 			= rs.getString("NOMPYME")==null?"":rs.getString("NOMPYME"); //FODEA 17
					String ig_numero_docto 	= rs.getString("ig_numero_docto")==null?"":rs.getString("ig_numero_docto");
					String fecha_emision 	= rs.getString("fecha_emision")==null?"":rs.getString("fecha_emision");
					String fecha_vencimiento= rs.getString("fecha_vencimiento")==null?"":rs.getString("fecha_vencimiento");
					String cd_nombre 			= rs.getString("cd_nombre")==null?"":rs.getString("cd_nombre");
					String fn_monto 			= rs.getString("fn_monto")==null?"":rs.getString("fn_monto");
					String fn_porc_anticipo = rs.getString("fn_porc_anticipo")==null?"":rs.getString("fn_porc_anticipo");
					String fn_monto_dscto 	= rs.getString("fn_monto_dscto")==null?"":rs.getString("fn_monto_dscto");
					String cs_dscto_especial= rs.getString("cs_dscto_especial")==null?"":rs.getString("cs_dscto_especial");
					String tipo_factoraje 	= rs.getString("tipo_factoraje")==null?"":rs.getString("tipo_factoraje");
					String referencia_pyme 	= rs.getString("REFERENCIA_PYME")==null?"":rs.getString("REFERENCIA_PYME");
					
					pdfDoc.setLCell(nuevaepo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_not,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cc_acuse,"formas",ComunesPDF.CENTER);   
					pdfDoc.setLCell(nompyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_emision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_vencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cd_nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipo_factoraje,"formas",ComunesPDF.CENTER);   
					pdfDoc.setLCell(fn_monto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fn_porc_anticipo,"formas",ComunesPDF.CENTER);     
               pdfDoc.setLCell(Float.toString((Float.parseFloat(fn_monto) - Float.parseFloat(fn_monto_dscto))),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fn_monto_dscto,"formas",ComunesPDF.CENTER);	
					pdfDoc.setLCell(referencia_pyme,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
		}		
		return nombreArchivo;
	}
	
	public List getConditions()	
	{
		return conditions;
	}


	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}

 
	public String getINoCliente() {
		return iNoCliente;
	}


	public void setDfFechaNotMin(String dfFechaNotMin) {
		this.dfFechaNotMin = dfFechaNotMin;
	}


	public String getDfFechaNotMin() {
		return dfFechaNotMin;
	}


	public void setDfFechaNotMax(String dfFechaNotMax) {
		this.dfFechaNotMax = dfFechaNotMax;
	}


	public String getDfFechaNotMax() {
		return dfFechaNotMax;
	}


	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}


	public String getFechaActual() {
		return fechaActual;
	}


	public void setDfFechaVencMin(String dfFechaVencMin) {
		this.dfFechaVencMin = dfFechaVencMin;
	}


	public String getDfFechaVencMin() {
		return dfFechaVencMin;
	}


	public void setDfFechaVencMax(String dfFechaVencMax) {
		this.dfFechaVencMax = dfFechaVencMax;
	}


	public String getDfFechaVencMax() {
		return dfFechaVencMax;
	}


	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}


	public String getIcEpo() {
		return icEpo;
	}


	public void setCcAcuse3(String ccAcuse3) {
		this.ccAcuse3 = ccAcuse3;
	}


	public String getCcAcuse3() {
		return ccAcuse3;
	}


	public void setTipoFactoraje(String tipoFactoraje) {
		this.tipoFactoraje = tipoFactoraje;
	}


	public String getTipoFactoraje() {
		return tipoFactoraje;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getTipo() {
		return tipo;
	}


   public void setTipo_(String tipo_) {
      this.tipo_ = tipo_;
   }

	public int getCountReg() {
		return countReg;
	}
}
