package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.CQueryHelperRegExtJS;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsDoctosNafinDE implements IQueryGeneratorPS, IQueryGeneratorRegExtJS{
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsDoctosNafinDE.class);
	private int numList = 1;
	private String envia;
	private String ic_epo;
	private String ic_if;
	private String ic_pyme;
	private String ig_numero_docto;
	private String df_fecha_docto_de;
	private String df_fecha_docto_a;
	private String df_fecha_venc_de;
	private String df_fecha_venc_a;
	private String ic_moneda;
	private String fn_monto_de;
	private String fn_monto_a;
	private String ic_estatus_docto;
	private String df_fecha_seleccion_de;
	private String df_fecha_seleccion_a;
	private String in_tasa_aceptada_de;
	private String in_tasa_aceptada_a;
	private String ord_if;
	private String ord_pyme;
	private String ord_epo;
	private String ord_fec_venc;
	private String operacion;
	private String ic_banco_fondeo;
	private String tipoFactoraje;
	private String paginaNo;
	private String Perfil;
	private String 	numero_siaff ;
	private boolean 	estaHabilitadoNumeroSIAFF;
	private String ic_documento;
	private List conditions;
	private String  infoProcesadaEn;
	private String mandanteDoc;
	private String operaFideicomiso;
	private String strPerfil;
	private String duplicado;
	private String validaDoctoEPO;
	private String copade;
	private String contrato;
	
	
	public String getDocumentQuery() {
	   log.info("getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer(512); //512 Tama�o inicial

		conditions=new ArrayList();

    // MPCS.- Parametros para descuento automatico

		conditions=this.getConditions1();
		String campos1[] = { "nombreEpo", "D.DF_FECHA_VENC", "nombrePyme" };
		String orden1[] =  { ord_epo, ord_fec_venc, ord_pyme };
		String campos2[] = { "nombreEpo", "nombreIf", "D.DF_FECHA_VENC", "nombrePyme" };
		String orden2[] =  { ord_epo, ord_if, ord_fec_venc, ord_pyme };

		String ic_documento	 = "";

		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 			= (String) numero.get("IC_EPO");
			ic_documento	= (String) numero.get("IC_DOCUMENTO");
		}


		if (ic_estatus_docto.equals("")
			|| ic_estatus_docto.equals("1")
			|| ic_estatus_docto.equals("2")
			|| ic_estatus_docto.equals("5")
			|| ic_estatus_docto.equals("6")
			|| ic_estatus_docto.equals("7")
			|| ic_estatus_docto.equals("9")
			|| ic_estatus_docto.equals("10")
			|| ic_estatus_docto.equals("21")
			|| ic_estatus_docto.equals("23")
			|| ic_estatus_docto.equals("28")
			|| ic_estatus_docto.equals("29")
			|| ic_estatus_docto.equals("30")
			|| ic_estatus_docto.equals("31")
			|| ic_estatus_docto.equals("33"))
		{

		log.debug("Paso x aqui 1.5--------------");
			qrySentencia.append(
					"select /*+ index (D IN_COM_DOCUMENTO_04_NUK) */ D.ic_documento  "
        			+ ",'ConsDoctosNafinDE::getDocumentQuery'"
					+ " from com_documento D, comrel_producto_epo pe, comcat_epo E, com_docto_seleccionado ds "
					+ " where "
					+ " d.ic_epo = pe.ic_epo " +
					" AND pe.ic_producto_nafin = 1 " +
					" AND ds.IC_DOCUMENTO (+)= d.IC_DOCUMENTO "+ //Agrega la tabla com_docto_seleccionado para mejorar rapidez
					" AND pe.ic_epo = E.ic_epo " +
          (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
          (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
					" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) ");

			if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
					qrySentencia.append(" and ds.cc_acuse like ? ")  ;
			if (operaFideicomiso.equals("S")){
						qrySentencia.append(" and ds.CS_OPERA_FISO = 'S' ");
					}
			  /*qrySentencia.append(" and exists (select 1"   +
                        "    from com_docto_seleccionado ds"   +
                        " 	 where d.ic_documento=ds.ic_documento"   +
                        " 	 and   ds.cc_acuse like ?) "  );*/
			
			if (!ic_epo.equals(""))
				qrySentencia.append(" and D.ic_epo = ?");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				qrySentencia.append(" and D.ic_pyme = ?");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				qrySentencia.append(" and D.ic_if = ?");
			if (!ig_numero_docto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ?");

			if (!df_fecha_docto_de.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO >= trunc(TO_DATE(?,'DD/MM/YYYY'))  ");
			}
			if (!df_fecha_docto_a.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals(""))
				qrySentencia.append(" and D.DF_FECHA_VENC >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.DF_FECHA_VENC < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ?");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				qrySentencia.append(" and D.fn_monto between ? and ?");
			//System.err.println("ic_estatus_docto = " + ic_estatus_docto);
			if (!ic_estatus_docto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ?");
			else
      		{ if (infoProcesadaEn.equals("5")||infoProcesadaEn.equals("8")||infoProcesadaEn.equals("9"))
          		qrySentencia.append(" and D.ic_estatus_docto in (3,4,11) ");
        	  else
          		qrySentencia.append(" and D.ic_estatus_docto in(2,3) ");
      		}
			if (!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=?");
			// Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				qrySentencia.append(" AND D.ic_documento = ? ");
			}

			//INI Fodea 38-2014
			if (!duplicado.equals(""))
				qrySentencia.append(" and D.CG_DUPLICADO = '" + this.duplicado + "' ");
			//FIN Fodea 38-2014


			if (!"".equals(validaDoctoEPO)   ) {
			    qrySentencia.append(" AND D.CS_VALIDACION_JUR = '" + this.validaDoctoEPO + "' ");			   
			}
			
			//QC-2018
			
			if (!"".equals(contrato)  ) {			
			qrySentencia.append(" AND upper(trim(D.CG_NUM_CONTRATO)) LIKE upper(?) ");
			}
				
			if (!"".equals(copade)  ) {				
				qrySentencia.append(" AND upper(trim(D.CG_NUM_COPADE)) LIKE upper(?) ");
			}

		
			String criterioOrdenamiento =
				Comunes.ordenarCampos(orden1, campos1);
			if (!criterioOrdenamiento.equals(""))
				criterioOrdenamiento = " order by " + criterioOrdenamiento;

			qrySentencia.append(criterioOrdenamiento);
		}
		else if (  ic_estatus_docto.equals("3")
				|| ic_estatus_docto.equals("4")
				|| ic_estatus_docto.equals("11")
				|| ic_estatus_docto.equals("12")
				|| ic_estatus_docto.equals("16")
				|| ic_estatus_docto.equals("24")
				|| ic_estatus_docto.equals("26"))//FODEA 005 - 2009 ACF
		{
				log.debug("Paso x aqui 1.6 -------------");

				if(!in_tasa_aceptada_de.equals("")
					&& !in_tasa_aceptada_a.equals("")
					&& !df_fecha_seleccion_de.equals("")
					&& !df_fecha_seleccion_a.equals("")){

					qrySentencia.append(
							"select /*+ INDEX (s IN_COM_SOLICITUD_13_NUK) */ D.ic_documento " +
              			",'ConsDoctosNafinDE::getDocumentQuery'" +
							" from com_documento D, com_docto_seleccionado DS, comcat_epo E," +
							" com_solicitud S, comrel_producto_epo pe " +
							" where S.ic_documento = DS.ic_documento " +
							" and  D.ic_documento = S.ic_documento " +
							" AND D.ic_epo = pe.ic_epo " +
							" AND pe.ic_epo = E.ic_epo " +
              (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
              (Perfil.equals("ADMIN BANCOMEXT")?" and S.ic_banco_fondeo= 2":"") +
							" AND pe.ic_producto_nafin = 1 ");

					if (operaFideicomiso.equals("S")){
						qrySentencia.append(" and ds.CS_OPERA_FISO = 'S' ");
					}
          			if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
            			qrySentencia.append(" and ds.cc_acuse like ? ")  ;
				}
				else if(!df_fecha_seleccion_de.equals("")
					&& !df_fecha_seleccion_a.equals("")){
					qrySentencia.append(
							"select /*+ INDEX (s IN_COM_SOLICITUD_13_NUK) */  D.ic_documento "
              				+ ",'ConsDoctosNafinDE::getDocumentQuery'"
							+ " from com_documento D, com_solicitud S, comrel_producto_epo pe , comcat_epo E,com_docto_seleccionado ds "
							+ " where D.ic_documento = S.ic_documento " +
							" AND ds.IC_DOCUMENTO (+)= d.IC_DOCUMENTO "+ //Agrega la tabla com_docto_seleccionado para mejorar rapidez

							" AND D.ic_epo = pe.ic_epo " +
							" AND pe.ic_epo = E.ic_epo " +
              (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
              (Perfil.equals("ADMIN BANCOMEXT")?" and S.ic_banco_fondeo= 2":"") +
							" AND pe.ic_producto_nafin = 1 ");

            		/*if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
              		qrySentencia.append(" and exists (select 1"   +
                              "    from com_docto_seleccionado ds"   +
                              " 	 where d.ic_documento=ds.ic_documento"   +
                              " 	 and   ds.cc_acuse like ?) "  );
										
						if (operaFideicomiso.equals("S"))
              		qrySentencia.append(" and exists (select 1"   +
                              "    from com_docto_seleccionado ds"   +
                              " 	 where d.ic_documento=ds.ic_documento"   +
                              " 	 and   ds.CS_OPERA_FISO = 'S') "  );*/
						if (operaFideicomiso.equals("S")){
						qrySentencia.append(" and ds.CS_OPERA_FISO = 'S' ");
					}
          			if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
							qrySentencia.append(" and ds.cc_acuse like ? ")  ;
				}
				else if(!in_tasa_aceptada_de.equals("")
				&& !in_tasa_aceptada_a.equals("")){
					qrySentencia.append(
							"select /*+ index (D IN_COM_DOCUMENTO_04_NUK) */ D.ic_documento  "
              				+ ",'ConsDoctosNafinDE::getDocumentQuery'"
							+ " from com_documento D, com_docto_seleccionado DS, comrel_producto_epo pe ,comcat_epo E"
							+ " where d.ic_documento = DS.ic_documento " +
							" AND D.ic_epo = pe.ic_epo " +
							" AND D.ic_epo = E.ic_epo " +
              (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
              (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
							" AND pe.ic_producto_nafin = 1 ");
              		if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
                		qrySentencia.append(" and ds.cc_acuse like ? "  );
						if (operaFideicomiso.equals("S")){
							qrySentencia.append(" and ds.CS_OPERA_FISO = 'S' ");
						}
					}
					else{
					qrySentencia.append(
							"select /*+ index (D IN_COM_DOCUMENTO_04_NUK) */ D.ic_documento "
              				+ ",'ConsDoctosNafinDE::getDocumentQuery'"
							+ " from com_documento D, comrel_producto_epo pe, comcat_epo E, com_docto_seleccionado ds "
							+ " where " +
							"  ds.IC_DOCUMENTO (+)= d.IC_DOCUMENTO "+ //Agrega la tabla com_docto_seleccionado para mejorar rapidez
							" AND D.ic_epo = pe.ic_epo " +
							" AND D.ic_epo = E.ic_epo " +
              (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
              (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
							" AND pe.ic_producto_nafin = 1 ");

		            if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
                		qrySentencia.append(" and ds.cc_acuse like ? "  );
						if (operaFideicomiso.equals("S")){
							qrySentencia.append(" and ds.CS_OPERA_FISO = 'S' ");
						}
		            /*if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
		              qrySentencia.append(" and exists (select 1"   +
                              "    from com_docto_seleccionado ds"   +
                              " 	 where d.ic_documento=ds.ic_documento"   +
                              " 	 and   ds.cc_acuse like ?) "  );
						if (operaFideicomiso.equals("S"))//puede tener "",5,8,9
              		qrySentencia.append(" and exists (select 1"   +
                              "    from com_docto_seleccionado ds"   +
                              " 	 where d.ic_documento=ds.ic_documento"   +
                              " 	 and   ds.CS_OPERA_FISO = 'S') "  );*/
					}

			if (!ic_epo.equals(""))
				qrySentencia.append(" and D.ic_epo = ?");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				qrySentencia.append(" and D.ic_pyme = ?");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				qrySentencia.append(" and D.ic_if = ?");
			if (!ig_numero_docto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ?");

			if (!df_fecha_docto_de.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO >= TO_DATE(?,'DD/MM/YYYY')  ");
			}
			if (!df_fecha_docto_a.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO < TO_DATE(?,'DD/MM/YYYY') + 1 ");
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals(""))
				qrySentencia.append(" and D.DF_FECHA_VENC >= TO_DATE(?,'DD/MM/YYYY') and D.DF_FECHA_VENC < TO_DATE(?,'DD/MM/YYYY') + 1 ");
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ?");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				qrySentencia.append(" and D.fn_monto between ? and ?");


			if (!ic_estatus_docto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ?");
			if (!ic_estatus_docto.equals("3")
				&& !ic_estatus_docto.equals("24"))
			{
				if (!df_fecha_seleccion_de.equals("")
					&& !df_fecha_seleccion_a.equals("")){
					qrySentencia.append(" AND s.df_fecha_solicitud >= to_date(?,'dd/mm/yyyy') AND s.df_fecha_solicitud < to_date(?,'dd/mm/yyyy') + 1 ");
				}
			}
			if (!in_tasa_aceptada_de.equals("")
				&& !in_tasa_aceptada_a.equals(""))
				qrySentencia.append(" and DS.in_tasa_aceptada between ? and ?");
			if (!"".equals(tipoFactoraje))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=?");
			// Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				qrySentencia.append(" AND D.ic_documento = ? ");
			}


			if (!"".equals(validaDoctoEPO)   ) {
			    qrySentencia.append(" AND D.CS_VALIDACION_JUR = '" + this.validaDoctoEPO + "' ");			   
			}
			
			//QC-2018
			if (!"".equals(contrato)  ) {			
			qrySentencia.append(" AND upper(trim(D.CG_NUM_CONTRATO)) LIKE upper(?) ");
			}
				
			if (!"".equals(copade)  ) {				
				qrySentencia.append(" AND upper(trim(D.CG_NUM_COPADE)) LIKE upper(?) ");
			}
			
			String criterioOrdenamiento =
				Comunes.ordenarCampos(orden2, campos2);
			if (!criterioOrdenamiento.equals(""))
				criterioOrdenamiento = " order by " + criterioOrdenamiento;

			qrySentencia.append(criterioOrdenamiento);

		} else {
			System.out.println(
				"ic_estatus_docto value not handled???????:  " + ic_estatus_docto);
		}


		int iIndex = qrySentencia.indexOf("where  and");
		if(iIndex > 0){
			String toWhere = qrySentencia.substring(0,qrySentencia.indexOf("where  and"));
			System.out.println(toWhere);
			qrySentencia = new StringBuffer(toWhere + "where" + qrySentencia.substring(qrySentencia.indexOf("where  and")+10,qrySentencia.length()));
		}


		log.debug("..:: qrySentencia : "+qrySentencia.toString());
		log.debug("..:: ic_estatus_docto : "+ic_estatus_docto);

		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(List ids) {
	   log.info("getDocumentSummaryQueryForIds(E)");
		

		StringBuffer query = new StringBuffer();
		StringBuffer clavesDocumentos = new StringBuffer();
		conditions=new ArrayList();
		for (int i = 0; i < ids.size(); i++){
			
			List lItem = (ArrayList)ids.get(i);
			clavesDocumentos.append("?,");
			conditions.add(new Integer(lItem.get(0).toString()));
		}
			
		
		
		
		
		clavesDocumentos = clavesDocumentos.delete(clavesDocumentos.length()-1,
				clavesDocumentos.length());


		if (ic_estatus_docto.equals("")
			|| ic_estatus_docto.equals("1")
			|| ic_estatus_docto.equals("2")
			|| ic_estatus_docto.equals("5")
			|| ic_estatus_docto.equals("6")
			|| ic_estatus_docto.equals("7")
			|| ic_estatus_docto.equals("9")
			|| ic_estatus_docto.equals("10")
			|| ic_estatus_docto.equals("21")
			|| ic_estatus_docto.equals("23")
			|| ic_estatus_docto.equals("28")
			|| ic_estatus_docto.equals("29")
			|| ic_estatus_docto.equals("30")
			|| ic_estatus_docto.equals("31")
			|| ic_estatus_docto.equals("33"))
		{
		log.debug("Paso x aqui 1.9 ---------");

			query.append(
				"select /*+ use_nl(d pe cv e p m i i2 tf ma ed) */ "
                                        +" (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) as nombreEpo, "
					+ " (decode (P.cs_habilitado,'N','*','S',' ')||' '||(INITCAP(P.cg_razon_social))) as nombrePyme, "
					+ " D.ig_numero_docto, TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') as DF_FECHA_DOCTO, "
					+ " TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') as DF_FECHA_VENC, M.cd_nombre as MONEDA, D.fn_monto, decode(D.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl) as fn_aforo, "
					+ " D.fn_monto_dscto, ED.cd_descripcion, D.ic_moneda, D.fn_porc_anticipo, D.ic_estatus_docto, "
					+ " D.ic_documento, D.cs_cambio_importe, "
					+ " (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as nombreIf, "
					+ " D.CS_DSCTO_ESPECIAL, decode(d.cs_dscto_especial,'D',i2.cg_razon_social, 'I',i2.cg_razon_social,'') as beneficiario, "
					+ " decode(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'') as fn_porc_beneficiario ,"
					+ " decode(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'') as RECIBIR_BENEF "
					+ " ,to_char(D.DF_ALTA, 'dd/mm/yyyy') df_alta "
					+ " ,'consDoctosDE::getDocumentSummaryQueryForIds' as pantalla, "
					+ " D.cc_acuse, D.ic_epo, "
					+ " TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') as DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO, "
					+ " tf.cg_nombre as TIPO_FACTORAJE, "
					+ " ma.cg_razon_social as NOMBREMANDANTE, "
					+ " 'N/A' as FIDEICOMISO, "
					+ " CASE D.CG_DUPLICADO WHEN 'S' THEN 'Si' ELSE 'No' END AS DUPLICADO" //Fodea 38-2014
					+ " , D.CC_ACUSE AS ACUSE_DOCTO" //F000-2015
					+ " ,(SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = D.CC_ACUSE) AS MUESTRA_VISOR "
					+" , decode( d.cs_dscto_especial,  'D', decode (d.CS_VALIDACION_JUR, 'S', 'SI', 'N', 'NO' ), '' )  as  VALIDACION_JUR "
				   +" , d.CG_NUM_CONTRATO  ,d.CG_NUM_COPADE " //QC2018
				
					+ " from comcat_epo E, comcat_pyme P, com_documento D, comcat_moneda M,"
					+ " comcat_estatus_docto ED, comvis_aforo cv, comcat_if I, comcat_if I2, "
					+ " comrel_producto_epo pe, "
					+ " COMCAT_TIPO_FACTORAJE tf "
					+ " , COMCAT_MANDANTE ma "

					+ " where D.ic_epo = E.ic_epo "
					+ " AND D.ic_epo = pe.ic_epo "
					//+ " AND pe.ic_epo = E.ic_epo "
					+ " AND pe.ic_producto_nafin = 1 "
					+ " and D.ic_epo = cv.ic_epo "
					+ " and D.ic_pyme = P.ic_pyme "
					+ " and D.ic_moneda = M.ic_moneda "
					+ " and D.ic_estatus_docto = ED.ic_estatus_docto "
					+ " and D.ic_beneficiario = I2.ic_if (+)"
					+ " and D.ic_if = I.ic_if(+)"
					+ " and d.cs_dscto_especial = tf.cc_tipo_factoraje "
					+ " AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "
					+ " and D.ic_documento in (" + clavesDocumentos + ")");
			numList = 1;

		}
		else if (
			ic_estatus_docto.equals("3")
				|| ic_estatus_docto.equals("4")
				|| ic_estatus_docto.equals("11")
				|| ic_estatus_docto.equals("12")
				|| ic_estatus_docto.equals("16")
				|| ic_estatus_docto.equals("24")
				|| ic_estatus_docto.equals("26"))//FODEA 005 - 2009 ACF
		{
			log.debug("Paso x aqui 1.10----------");
			
			// Obtener condiciones de consulta del plazo
			String plazo 					= null;
			String condicionesPlazo 	= null;
			String tablasPlazo			= null;
			if(
				ic_estatus_docto.equals("3") 	// Seleccionada Pyme
					||
				ic_estatus_docto.equals("24")	// En proceso de Autorizacion IF
			){
				// Fodea 048 - 2012 -- Consulta Plazo, para doctos con estatus: 3 (Seleccionada Pyme) y 24 (En Proceso de Autorizacion IF)
				plazo 			= 
					" ( " +
						"  DECODE( PE.CS_FECHA_VENC_PYME, 'S', D.DF_FECHA_VENC_PYME, D.DF_FECHA_VENC )" + " - " +
						" 	(CASE  " +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
						"    TRUNC(SYSDATE) " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	END) " + 
					" ) " + 	
					" AS ig_plazo, ";
					
				condicionesPlazo =
						" 	AND D.ic_if                = iexp.ic_if  "  +
						" 	AND D.ic_epo               = iexp.ic_epo "  +
						" 	AND iexp.ic_producto_nafin = 1           "  +
						"  AND D.ic_epo               = pe.ic_epo   "  +
						"  AND pe.ic_producto_nafin   = 1           ";
						
				tablasPlazo		= 
						" , comrel_if_epo_x_producto iexp " +
						" , comrel_producto_epo      pe   ";
						
			} else if( 
				ic_estatus_docto.equals("26") // Programado Pyme
			){ 
			
				// Fodea 048 - 2012 -- Consulta Plazo, para doctos con estatus: 26 (Programado Pyme)
				plazo 			=
					" ( "  +
						"  DECODE( PE.CS_FECHA_VENC_PYME, 'S', D.DF_FECHA_VENC_PYME, D.DF_FECHA_VENC )" + " - " +
						" 	(CASE  "  +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN "  +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN "  +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 2, '+') " +
						" 	END) "  + 
					" ) "  + 	
					" AS ig_plazo, ";
					
				condicionesPlazo =
						" 	AND D.ic_if                = iexp.ic_if  "  +
						" 	AND D.ic_epo               = iexp.ic_epo "  +
						" 	AND iexp.ic_producto_nafin = 1           "  +
						"  AND D.ic_epo               = pe.ic_epo   "  +
						"  AND pe.ic_producto_nafin   = 1           ";
						
				tablasPlazo		= 
						" , comrel_if_epo_x_producto iexp " +
						" , comrel_producto_epo      pe   ";
						
			} else {
 
				// " 	DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(DS.df_fecha_seleccion), 54, sigfechahabilxepo (E.ic_epo, TRUNC(DS.df_fecha_seleccion), 1, '+')), S.ig_plazo ) AS ig_plazo, "   +
				plazo 				= " S.ig_plazo AS ig_plazo, ";
				condicionesPlazo 	= null;
				tablasPlazo			= null;
				
			}
			
			query.append(
						
				" SELECT /*+   leading(d)      use_nl(PE, TF, MA, D,DS,S,CC1,CD,P,E,I,I2,ED,M) */ "+
				" 	E.cg_razon_social nombreEpo,"   +
				"  	P.cg_razon_social nombrePyme,"   +
				"  	I.cg_razon_social AS nombreIf,"   +
				"  	D.ig_numero_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') AS df_fecha_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS df_fecha_venc,"   +
				"  	M.cd_nombre as MONEDA,"   +
				"  	D.fn_monto,"   +
				"		D.ic_estatus_docto, " +
				"  	D.fn_porc_anticipo,"   +
				"  	D.fn_monto_dscto,"   +
				"  	DS.in_tasa_aceptada,"   +
				"  	ED.cd_descripcion,"   +
				"  	TO_CHAR(S.df_fecha_solicitud,'DD/MM/YYYY') AS df_fecha_solicitud,"   +
				"  	D.ic_moneda, D.ic_documento,"   +
				"  	D.cs_cambio_importe,"   +
				"  	D.CS_DSCTO_ESPECIAL,"   +
				"  	DECODE(d.cs_dscto_especial,'D',i2.cg_razon_social,'I',i2.cg_razon_social,'') AS beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'') AS fn_porc_beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'') AS RECIBIR_BENEF,"   +
				"  	DECODE(d.cs_dscto_especial,'D',ds.in_importe_recibir - ds.fn_importe_recibir_benef,'I',ds.in_importe_recibir - ds.fn_importe_recibir_benef,'') AS NETO_REC_PYME,"   +
				"  	DS.cc_acuse,"   +
				"  	DS.in_importe_recibir,"   +
				"  	CASE WHEN cc1.fn_monto_pago IS NOT NULL THEN"   +
				"  		cc1.fn_monto_pago"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_monto_pago IS NOT NULL THEN"   +
				"  			cd.fn_monto_pago"   +
				"  		END"   +
				" 	END AS fn_monto_pago,"   +
				"  	CASE WHEN cc1.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  		cc1.fn_porc_docto_aplicado"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  			cd.fn_porc_docto_aplicado"   +
				"  		END"   +
				"  	END AS porcDoctoAplicado,"   +
				//" 	DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(DS.df_fecha_seleccion), 54, sigfechahabilxepo (E.ic_epo, TRUNC(DS.df_fecha_seleccion), 1, '+')), S.ig_plazo ) AS ig_plazo,"   +
				plazo  +
				" 	DS.in_importe_interes,"   +
				"  	D.ic_pyme,"   +
				"   I.ig_tipo_piso, DS.fn_remanente, "+ //foda 015-2005
		    	"   to_char(D.DF_ALTA, 'dd/mm/yyyy') df_alta, "+
				"  	'consDoctosNafinDE::getDocumentSummaryQueryForIds' AS pantalla, "   +
				" 		D.ic_epo, " +
				" 	TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO, " +
				"	TO_CHAR(ds.df_programacion, 'dd/mm/yyyy') AS fecha_registro_operacion, "+//FODEA 005 - 2009 ACF
				"  tf.cg_nombre as TIPO_FACTORAJE, " +
				"  ma.cg_razon_social as NOMBREMANDANTE, " +
				"  DECODE(ds.CS_OPERA_FISO,'S',I3.cg_razon_social,'N/A') as FIDEICOMISO, "+
				"  CASE D.CG_DUPLICADO WHEN 'S' THEN 'Si' ELSE 'No' END AS DUPLICADO "+ //Fodea 38-2014
				" , D.CC_ACUSE AS ACUSE_DOCTO" +//F000-2015
				" ,(SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = D.CC_ACUSE) AS MUESTRA_VISOR " +
				" , decode( d.cs_dscto_especial,  'D', decode (d.CS_VALIDACION_JUR, 'S', 'SI', 'N', 'NO' ), '' )  as  VALIDACION_JUR "+
				" , d.CG_NUM_CONTRATO  ,d.CG_NUM_COPADE "+ //QC2018
			
				"  FROM comcat_epo E,"   +
				"  	comcat_pyme P,"   +
				"  	comcat_if I,"   +
				"  	comcat_if I2,"   +
				"		comcat_if I3, "+
				"  	com_documento D,"   +
				" 		COMCAT_TIPO_FACTORAJE tf, " +
				"  	comcat_moneda M,"   +
				"  	com_docto_seleccionado DS,"   +
				"  	comcat_estatus_docto ED,"   +
				"  	com_solicitud S,"   +
				"     COMCAT_MANDANTE ma,"+
				"  	(SELECT /*+index(cc cp_com_cobro_credito_pk)*/"   +
				"     cc.ic_documento, cc.fn_porc_docto_aplicado ,"   +
				"  	SUM(cc.fn_monto_pago) AS fn_monto_pago"   +
				"  	FROM com_cobro_credito cc"   +
				" 	where cc.ic_documento in (" + clavesDocumentos + ") " +
				"  	GROUP BY cc.ic_documento, cc.fn_porc_docto_aplicado) cc1,"   +
				"  	fop_cobro_disposicion cd "   +
				( tablasPlazo != null?tablasPlazo:"" ) +
				"  WHERE D.ic_epo = E.ic_epo"   +
				"  	AND D.ic_pyme = P.ic_pyme"   +
				"  	AND D.ic_moneda = M.ic_moneda"   +
				"  	AND D.ic_documento = DS.ic_documento"   +
				"  	AND D.ic_estatus_docto = ED.ic_estatus_docto"   +
				"  	AND D.ic_documento = S.ic_documento(+)"   +
				"  	AND D.ic_if = I.ic_if(+)"   +
				"  	AND D.ic_beneficiario = I2.ic_if (+)"   +
				"  	AND DS.ic_documento = CC1.ic_documento(+)"   +
				"  	AND DS.ic_documento = CD.ic_documento(+)"   +
				"     AND d.cs_dscto_especial = tf.cc_tipo_factoraje "+
				"     AND ma.IC_MANDANTE(+) = d.IC_MANDANTE " +
				"		AND  I3.ic_if= ds.ic_if"+
            " 		AND D.ic_documento in (" + clavesDocumentos + ") " +
            ( condicionesPlazo != null?condicionesPlazo:"" )
        );
			for (int i = 0; i < ids.size(); i++){
			
			List lItem = (ArrayList)ids.get(i);
			
			conditions.add(new Integer(lItem.get(0).toString()));
		}

			numList = 2;
			//System.out.println("ConsDoctosNafinDE::DOCUMENT SUMMARY FOR IDS:  "+query);

		} else {
			System.out.println("ic_estatus_docto value not handled:  " + ic_estatus_docto);
		}
	log.debug("-------------query-------1------------------");
		log.debug("..:: query : "+query.toString());
		log.debug("..:: conditions : "+conditions.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
		return query.toString();
	}

	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		numList = 1;

		conditions=new ArrayList();
		this.getConditions1();
		String hint = "";
		String campos1[] = { "nombreEpo", "D.df_fecha_venc", "nombrePyme" };
		String orden1[] = { ord_epo, ord_fec_venc, ord_pyme };
		String campos2[] = { "nombreEpo", "nombreIf", "D.df_fecha_venc", "nombrePyme" };
		String orden2[] = { ord_epo, ord_if, ord_fec_venc, ord_pyme };
		
		//String	ic_documento = "";

		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 				= (String) numero.get("IC_EPO");
			ic_documento		= (String) numero.get("IC_DOCUMENTO");
		}

		if (ic_estatus_docto.equals("")
			|| ic_estatus_docto.equals("1")
			|| ic_estatus_docto.equals("2")
			|| ic_estatus_docto.equals("5")
			|| ic_estatus_docto.equals("6")
			|| ic_estatus_docto.equals("7")
			|| ic_estatus_docto.equals("9")
			|| ic_estatus_docto.equals("10")
			|| ic_estatus_docto.equals("21")
			|| ic_estatus_docto.equals("23")
			|| ic_estatus_docto.equals("28")
			|| ic_estatus_docto.equals("29")
			|| ic_estatus_docto.equals("30")
			|| ic_estatus_docto.equals("31")
			|| ic_estatus_docto.equals("33"))
		{

			log.debug("Paso x aqui 1.3---------------");

			if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
						condicion.append(" and ds.cc_acuse like ? ");
					if (operaFideicomiso.equals("S")){
						condicion.append(" and ds.CS_OPERA_FISO = 'S' ");
					}
			if (!ic_epo.equals(""))
				condicion.append("and D.ic_epo = ? ");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				condicion.append("and D.ic_pyme = ? ");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				condicion.append("and D.ic_if = ? ");
			if (!ig_numero_docto.equals(""))
				condicion.append("and D.ig_numero_docto = ? ");
			if (!df_fecha_docto_de.equals("")){
				condicion.append("and D.df_fecha_docto >= trunc(TO_DATE(?,'DD/MM/YYYY'))  ");
			}
			if (!df_fecha_docto_a.equals("")){
				condicion.append("and D.df_fecha_docto < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			}
			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals(""))
				condicion.append("and D.df_fecha_venc >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.df_fecha_venc < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				condicion.append("and D.ic_moneda = ? ");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				condicion.append("and D.fn_monto between ? and ? ");
			if (!ic_estatus_docto.equals(""))
				condicion.append("and D.ic_estatus_docto = ? ");
			else
      		{ if (infoProcesadaEn.equals("5")||infoProcesadaEn.equals("8")||infoProcesadaEn.equals("9"))
          		condicion.append("and D.ic_estatus_docto in (3,4,11) ");
        	  else
          		condicion.append("and D.ic_estatus_docto in(2,3) ");
      		}
			if (!tipoFactoraje.equals(""))
				condicion.append("and D.cs_dscto_especial = ? ");
			// Revisar esta seccion
			if(ic_documento!=null&&!ic_documento.equals("")){
				condicion.append(" AND D.ic_documento = ? ");
			}

			//INI Fodea 38-2014
			if (!duplicado.equals(""))
				condicion.append(" and D.CG_DUPLICADO = '" + this.duplicado + "' ");
			//FIN Fodea 38-2014
			
			if (!"".equals(validaDoctoEPO)   ) {
			    condicion.append(" AND D.CS_VALIDACION_JUR = '" + this.validaDoctoEPO + "' ");			   
			}

			//QC-2018
			if (!"".equals(contrato)  ) {			
			condicion.append(" AND upper(trim(D.CG_NUM_CONTRATO)) LIKE upper(?) ");
			}
				
			if (!"".equals(copade)  ) {				
				condicion.append(" AND upper(trim(D.CG_NUM_COPADE)) LIKE upper(?) ");
			}
			
			// Obtiene todos los documentos menos las notas de credito.
			StringBuffer sQryTodos =  new StringBuffer(
				"select /*+ use_nl(d m cv pe e)*/ "+
				" count(1) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
				"sum(D.fn_monto * (decode(D.ic_moneda, 1, cv.fn_aforo, 54, cv.fn_aforo_dl) / 100)) AS FN_MONTO_DSCTO, "+
				"M.cd_nombre, M.ic_moneda, 'D' as tipoDocto "+
				"from com_documento D, comcat_moneda M, comvis_aforo CV, comrel_producto_epo pe , comcat_epo E,com_docto_seleccionado ds "+
				"where D.ic_moneda = M.ic_moneda "+
				" AND ds.IC_DOCUMENTO (+)= d.IC_DOCUMENTO "+ //Agrega la tabla com_docto_seleccionado para mejorar rapidez

				"and D.ic_epo = CV.ic_epo "+
				" AND pe.ic_epo = D.ic_epo " +
				" AND D.ic_epo = E.ic_epo " +
				" AND pe.ic_producto_nafin = 1 " +
				//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
				//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
				condicion.toString() +
				"and D.cs_dscto_especial != 'C' "+
				"group by M.ic_moneda, M.cd_nombre");

			// Obtiene �nicamente los documentos negociables sin notas de cr�dito.
			StringBuffer sNegociable = new StringBuffer(
				"select /*+ use_nl(d m cv pe e)*/ "+
				" count(1) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
				"sum(D.fn_monto * (decode(D.ic_moneda, 1, cv.fn_aforo, 54, cv.fn_aforo_dl) / 100)) AS FN_MONTO_DSCTO, "+
				"M.cd_nombre, M.ic_moneda, 'DN' as tipoDocto "+
				"from com_documento D, comcat_moneda M, comvis_aforo CV, comrel_producto_epo pe , comcat_epo E,com_docto_seleccionado ds "+
				"where D.ic_moneda = M.ic_moneda "+
				" AND ds.IC_DOCUMENTO (+)= d.IC_DOCUMENTO "+ //Agrega la tabla com_docto_seleccionado para mejorar rapidez
				"and D.ic_epo = CV.ic_epo "+
				" AND pe.ic_epo = D.ic_epo " +
				" AND pe.ic_producto_nafin = 1 " +
				" AND D.ic_epo = E.ic_epo " +
				//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
				//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
				condicion.toString() +
				"and D.ic_estatus_docto = 2 "+
				"and D.cs_dscto_especial != 'C' "+
				"group by M.ic_moneda, M.cd_nombre");

			// Obtiene los documentos negociables que son notas de cr�dito
			StringBuffer sNotasCredito = new StringBuffer(
				"select /*+ use_nl(d m cv pe e)*/ "+
				" count(1) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
				"sum(D.fn_monto * (decode(D.ic_moneda, 1, cv.fn_aforo, 54, cv.fn_aforo_dl) / 100)) AS FN_MONTO_DSCTO, "+
				"M.cd_nombre, M.ic_moneda, 'NC' as tipoDocto "+
				"from com_documento D, comcat_moneda M, comvis_aforo CV, comrel_producto_epo pe , comcat_epo E, com_docto_seleccionado ds "+
				"where D.ic_moneda = M.ic_moneda "+
				" AND ds.IC_DOCUMENTO (+)= d.IC_DOCUMENTO "+ //Agrega la tabla com_docto_seleccionado para mejorar rapidez
				"and D.ic_epo = CV.ic_epo "+
				" AND pe.ic_epo = D.ic_epo " +
				" AND pe.ic_producto_nafin = 1 " +
				" AND D.ic_epo = E.ic_epo " +
				//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
				//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
				condicion +
				"and D.ic_estatus_docto = 2 "+
				"and D.cs_dscto_especial = 'C' "+
				"group by M.ic_moneda, M.cd_nombre");

			numList++;
			sQryTodos.append(" UNION ALL "+ sNotasCredito.toString());
			this.getConditions1();
			if(!ic_estatus_docto.equals("2")) {
				numList++;
				sQryTodos.append(" UNION ALL "+ sNegociable.toString());
				this.getConditions1();
			}
			
			qrySentencia.append(" select * from ("+sQryTodos.toString()+") order by ic_moneda ");

			//System.out.println("getAggregateCalculationQuery"+qrySentencia);
		}
		else if (
				ic_estatus_docto.equals("3")
				|| ic_estatus_docto.equals("4")
				|| ic_estatus_docto.equals("11")
				|| ic_estatus_docto.equals("12")
				|| ic_estatus_docto.equals("16")
				|| ic_estatus_docto.equals("24")
				|| ic_estatus_docto.equals("26"))//FODEA 005 - 2009 ACF
		{
			log.debug("Paso x aqui 1.4--------------");
			//hint predeterminado
			hint = " /*+  use_nl(d cv m s ds e pe) index(d IN_COM_DOCUMENTO_04_NUK) */ ";
			
			if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
						condicion.append(" and ds.cc_acuse like ? ");
					if (operaFideicomiso.equals("S")){
						condicion.append(" and ds.CS_OPERA_FISO = 'S' ");
					}
					
			if (!ic_epo.equals(""))
				condicion.append("and D.ic_epo = ? ");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				condicion.append("and D.ic_pyme = ? ");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				condicion.append("and D.ic_if = ? ");
			if (!ig_numero_docto.equals("")) {
				condicion.append("and D.ig_numero_docto = ? ");
			}

			if (!df_fecha_docto_de.equals("")){
				condicion.append("and D.df_fecha_docto >= TO_DATE(?,'DD/MM/YYYY')  ");
			}
			if (!df_fecha_docto_a.equals("")){
				condicion.append("and D.df_fecha_docto < TO_DATE(?,'DD/MM/YYYY') + 1 ");
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals("")){
				hint = " /*+  USE_NL(d cv m s ds e pe) index(d IN_COM_DOCUMENTO_09_NUK) */ ";
				condicion.append("and D.df_fecha_venc >= TO_DATE(?,'DD/MM/YYYY') AND D.df_fecha_venc < TO_DATE(?,'DD/MM/YYYY') + 1 ");
			}
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				condicion.append("and D.ic_moneda = ? ");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				condicion.append("and D.fn_monto between ? and ? ");

			if (!ic_estatus_docto.equals("")) {
				condicion.append("and D.ic_estatus_docto = ? ");
			}
			if (!ic_estatus_docto.equals("3") && !ic_estatus_docto.equals("24")) {
				if (!df_fecha_seleccion_de.equals("") && !df_fecha_seleccion_a.equals("")){
					condicion.append(" AND s.df_fecha_solicitud >= trunc(TO_DATE(?,'DD/MM/YYYY')) AND s.df_fecha_solicitud < trunc(TO_DATE(?,'DD/MM/YYYY')+1) ");
					hint = " /*+  use_nl(d cv m s ds e pe) INDEX (s IN_COM_SOLICITUD_13_NUK) */ ";
				}
			}
			if (!in_tasa_aceptada_de.equals("") && !in_tasa_aceptada_a.equals(""))
				condicion.append("and DS.in_tasa_aceptada between ? and ? ");
			if (!"".equals(tipoFactoraje))
				condicion.append("and D.cs_dscto_especial = ? ");
			// Revisar esta seccion
			if(ic_documento!=null&&!ic_documento.equals("")){
				condicion.append(" AND D.ic_documento = ? ");
			}

			if (!"".equals(validaDoctoEPO)   ) {
			    condicion.append(" AND D.CS_VALIDACION_JUR = '" + this.validaDoctoEPO + "' ");			   
			}
			
			//QC-2018
			if (!"".equals(contrato)  ) {			
				condicion.append(" AND upper(trim(D.CG_NUM_CONTRATO)) LIKE upper(?) ");
			}
				
			if (!"".equals(copade)  ) {				
				condicion.append(" AND upper(trim(D.CG_NUM_COPADE)) LIKE upper(?) ");
			}
			
			if(!in_tasa_aceptada_de.equals("") && !in_tasa_aceptada_a.equals("") &&
				!df_fecha_seleccion_de.equals("") && !df_fecha_seleccion_a.equals("")) {

				  qrySentencia.append(
						"select "+hint+" count(*) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
						"sum(fn_monto_dscto) AS FN_MONTO_DSCTO, "+
          				"M.cd_nombre, M.ic_moneda, 'D' as tipoDocto "+
						"from com_documento D, com_docto_seleccionado DS, com_solicitud S, comcat_epo E "+
						"comcat_moneda M, comvis_aforo CV, comrel_producto_epo pe "+
						"where S.ic_documento = DS.ic_documento "+
						"and D.ic_documento = S.ic_documento "+
						"and D.ic_epo = CV.ic_epo "+
						" AND pe.ic_epo = D.ic_epo " +
						" AND pe.ic_producto_nafin = 1 " +
						" AND pe.ic_epo = E.ic_epo " +
						//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
            (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
            (Perfil.equals("ADMIN BANCOMEXT")?" and S.ic_banco_fondeo= 2":"") +
						//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
						"and D.ic_moneda = M.ic_moneda ");
				
			}
			else if(!df_fecha_seleccion_de.equals("") && !df_fecha_seleccion_a.equals("")){

				qrySentencia.append(
						"select "+hint+" count(*) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
						"sum(fn_monto_dscto) AS FN_MONTO_DSCTO, "+
          			"M.cd_nombre, M.ic_moneda, 'D' as tipoDocto "+
						"from com_documento D, com_solicitud S,comcat_moneda M, comvis_aforo CV, comcat_epo E , "+
						" comrel_producto_epo pe,com_docto_seleccionado ds " +
						"where D.ic_documento = S.ic_documento "+
				" AND ds.IC_DOCUMENTO (+)= d.IC_DOCUMENTO "+ //Agrega la tabla com_docto_seleccionado para mejorar rapidez

						"and D.ic_epo = CV.ic_epo "+
						" AND pe.ic_epo = D.ic_epo " +
						" AND pe.ic_producto_nafin = 1 " +
						" AND pe.ic_epo = E.ic_epo " +
						//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
            (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
            (Perfil.equals("ADMIN BANCOMEXT")?" and S.ic_banco_fondeo= 2":"") +
						//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
						"and D.ic_moneda = M.ic_moneda ");
        		
			}
			else if(!in_tasa_aceptada_de.equals("")	&& !in_tasa_aceptada_a.equals("")){

				qrySentencia.append(
						"select "+hint+" count(*) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
						"sum(fn_monto_dscto) AS FN_MONTO_DSCTO, "+
        				"M.cd_nombre, M.ic_moneda, 'D' as tipoDocto "+
						"from com_documento D, com_docto_seleccionado DS, comcat_moneda M, comcat_epo E  " +
						" ,comvis_aforo CV, comrel_producto_epo pe "+
						"where D.ic_epo = CV.ic_epo " +
						" AND pe.ic_epo = D.ic_epo " +
						" AND pe.ic_epo = E.ic_epo " +
						//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
            (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
            (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
						" AND pe.ic_producto_nafin = 1 " );
						//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) ");
      			
			} else {

				qrySentencia.append(
						"select "+hint+" count(*) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
						"sum(fn_monto_dscto) AS FN_MONTO_DSCTO, "+
          				"M.cd_nombre, M.ic_moneda, 'D' as tipoDocto "+
						"from com_documento D, comcat_moneda M, comvis_aforo CV, comrel_producto_epo pe, comcat_epo E, com_docto_seleccionado ds "+
						" where D.ic_moneda = M.ic_moneda "+
						" AND ds.IC_DOCUMENTO (+)= d.IC_DOCUMENTO "+ //Agrega la tabla com_docto_seleccionado para mejorar rapidez

						" and D.ic_epo = CV.ic_epo " +
						" AND pe.ic_epo = D.ic_epo " +
						" AND pe.ic_epo = E.ic_epo " +
						//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
            (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
            (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
						" AND pe.ic_producto_nafin = 1 " );
						//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " );
          		
			}
			qrySentencia.append("and D.cs_dscto_especial != 'C' ");


			qrySentencia.append(condicion.toString() +
								" GROUP BY M.ic_moneda, M.cd_nombre"+
                                " order by M.ic_moneda ");
			//System.out.println("::QUERY DE CALCULATION "+qrySentencia);

		} else {
			System.out.println("ic_estatus_docto value not handled:  " + ic_estatus_docto);
		}
		int iIndex = qrySentencia.toString().indexOf("where  and");
		if(iIndex > 0){
			String toWhere = qrySentencia.toString().substring(0, qrySentencia.toString().indexOf("where  and"));
			System.out.println(toWhere);
			qrySentencia = new StringBuffer( toWhere + "where" + qrySentencia.toString().substring(qrySentencia.toString().indexOf("where  and")+10,qrySentencia.length()) );
		}


		log.debug("..:: qrySentencia : "+qrySentencia.toString());
		log.debug("..:: ic_estatus_docto : "+ic_estatus_docto);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}

	public List getConditions(){
	return this.conditions;
	}
	public List getConditions1() {
	if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF ){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 				= (String) numero.get("IC_EPO");
			ic_documento 		= (String) numero.get("IC_DOCUMENTO");
		}

		if (ic_estatus_docto.equals("")
			|| ic_estatus_docto.equals("1")
			|| ic_estatus_docto.equals("2")
			|| ic_estatus_docto.equals("5")
			|| ic_estatus_docto.equals("6")
			|| ic_estatus_docto.equals("7")
			|| ic_estatus_docto.equals("9")
			|| ic_estatus_docto.equals("10")
			|| ic_estatus_docto.equals("21")
			|| ic_estatus_docto.equals("23")
			|| ic_estatus_docto.equals("28")
			|| ic_estatus_docto.equals("29")
			|| ic_estatus_docto.equals("30")
			|| ic_estatus_docto.equals("31")
			|| ic_estatus_docto.equals("33"))
		{
		
		log.debug("Paso x aqui 1.1---------------");

	      if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
			conditions.add(infoProcesadaEn+"%");
			if (!ic_epo.equals(""))
				conditions.add(new Integer(ic_epo));
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				conditions.add(new Integer(ic_pyme));
			if (!ic_if.equals("T") && !ic_if.equals(""))
				conditions.add(new Integer(ic_if));
			if (!ig_numero_docto.equals(""))
				conditions.add(ig_numero_docto);
			if (!df_fecha_docto_de.equals("")){
				conditions.add(df_fecha_docto_de);
			}
			if (!df_fecha_docto_a.equals("")){
				conditions.add(df_fecha_docto_a);
			}
			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals("")){
				conditions.add(df_fecha_venc_de);
				conditions.add(df_fecha_venc_a);
			}
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				conditions.add(new Integer(ic_moneda));
			if (!fn_monto_de.equals("") && !fn_monto_a.equals("")){
				conditions.add(new Double(fn_monto_de));
				conditions.add(new Double(fn_monto_a));
			}
			if (!ic_estatus_docto.equals(""))
				conditions.add(new Integer(ic_estatus_docto));
			if (!tipoFactoraje.equals(""))
				conditions.add(tipoFactoraje);
			// Condiciones por Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				conditions.add(ic_documento);
			}

		}
		else if (
			ic_estatus_docto.equals("3")
				|| ic_estatus_docto.equals("4")
				|| ic_estatus_docto.equals("11")
				|| ic_estatus_docto.equals("12")
				|| ic_estatus_docto.equals("16")
				|| ic_estatus_docto.equals("24")
				|| ic_estatus_docto.equals("26"))//FODEA 005 - 2009 ACF
		{

		log.debug("Paso x aqui 1.2-------------");

	      if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
			conditions.add(infoProcesadaEn+"%");
			if (!ic_epo.equals(""))
				conditions.add(new Integer(ic_epo));
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				conditions.add(new Integer(ic_pyme));
			if (!ic_if.equals("T") && !ic_if.equals(""))
				conditions.add(new Integer(ic_if));
			if (!ig_numero_docto.equals(""))
				conditions.add(ig_numero_docto);

			if (!df_fecha_docto_de.equals("")){
				conditions.add(df_fecha_docto_de);
			}
			if (!df_fecha_docto_a.equals("")){
				conditions.add(df_fecha_docto_a);
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals("")){
				conditions.add(df_fecha_venc_de);
				conditions.add(df_fecha_venc_a);
			}
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				conditions.add(new Integer(ic_moneda));
			if (!fn_monto_de.equals("") && !fn_monto_a.equals("")){
				conditions.add(new Double(fn_monto_de));
				conditions.add(new Double(fn_monto_a));
			}
			if (!ic_estatus_docto.equals(""))
				conditions.add(new Integer(ic_estatus_docto));
			if (!ic_estatus_docto.equals("3")
				&& !ic_estatus_docto.equals("24"))
			{
				if (!df_fecha_seleccion_de.equals("")
					&& !df_fecha_seleccion_a.equals("")){
					conditions.add(df_fecha_seleccion_de);
					conditions.add(df_fecha_seleccion_a);
					}
			}
			if (!in_tasa_aceptada_de.equals("")
				&& !in_tasa_aceptada_a.equals("")){
				conditions.add(new Double(in_tasa_aceptada_de));
				conditions.add(new Double(in_tasa_aceptada_a));
				}
			if (!"".equals(tipoFactoraje))
				conditions.add(tipoFactoraje);
			// Condiciones por Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				conditions.add(ic_documento);
			}

		}

			// QC-2018
			if (!"".equals(contrato)  ) {
				conditions.add(contrato);
			}
			if (!"".equals(copade)  ) {
				conditions.add(copade);
			}

		log.info("getConditions(S)");
		return conditions;
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
			StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		//campos de la parametrizacion de documentos
		String  consultaDoctos = "";
		consultaDoctos = ", TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO ";

    // MPCS.- Parametros para descuento automatico

		conditions=new ArrayList();

    // MPCS.- Parametros para descuento automatico

		conditions=this.getConditions1();
		String campos1[] = { "nombreEpo", "D.DF_FECHA_VENC", "nombrePyme" };
		String orden1[] =  { ord_epo, ord_fec_venc, ord_pyme };
		String campos2[] = { "nombreEpo", "nombreIf", "D.DF_FECHA_VENC", "nombrePyme" };
		String orden2[] =  { ord_epo, ord_if, ord_fec_venc, ord_pyme };

		String ic_documento	 = "";

		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 			= (String) numero.get("IC_EPO");
			ic_documento	= (String) numero.get("IC_DOCUMENTO");
		}

		if (ic_estatus_docto.equals("")
			|| ic_estatus_docto.equals("1")
			|| ic_estatus_docto.equals("2")
			|| ic_estatus_docto.equals("5")
			|| ic_estatus_docto.equals("6")
			|| ic_estatus_docto.equals("7")
			|| ic_estatus_docto.equals("9")
			|| ic_estatus_docto.equals("10")
			|| ic_estatus_docto.equals("21")
			|| ic_estatus_docto.equals("23")
			|| ic_estatus_docto.equals("28")
			|| ic_estatus_docto.equals("29")
			|| ic_estatus_docto.equals("30")
			|| ic_estatus_docto.equals("31")
			|| ic_estatus_docto.equals("33"))
		{
			//log.debug("Paso x aqui 1.7 ----------");

			qrySentencia.append("SELECT /*+	INDEX(d IN_COM_DOCUMENTO_04_NUK)	USE_NL(pe cv e ed d m tf i i2 p) */ " +
				" (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) as nombreEpo, " +
				" (decode (P.cs_habilitado,'N','*','S',' ')||' '||(INITCAP(P.cg_razon_social))) as nombrePyme, " +
				" D.ig_numero_docto, TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') as DF_FECHA_DOCTO, " +
				" TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') as DF_FECHA_VENC, M.cd_nombre, D.fn_monto, decode(D.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl) as fn_aforo, " +
				" D.fn_monto_dscto, ED.cd_descripcion, D.ic_moneda, D.fn_porc_anticipo, D.ic_estatus_docto, " +
				" D.ic_documento, D.cs_cambio_importe, "+
				" (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as nombreIf, " +
				" D.CS_DSCTO_ESPECIAL, decode(d.cs_dscto_especial,'D',i2.cg_razon_social,'I',i2.cg_razon_social,'') as beneficiario, " +
		    " decode(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'') as fn_porc_beneficiario, " +
		    " decode(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'') as RECIBIR_BENEF, "+
		    " to_char(D.DF_ALTA, 'dd/mm/yyyy') df_alta, "+
		    " 'ConsDoctosNafinDE::getDocumentQueryFile' as Pantalla,P.cg_rfc as rfc, "+
			 " D.cc_acuse, D.ic_epo, " +
			 " tf.cg_nombre AS TIPO_FACTORAJE " +
			 ", ma.cg_razon_social as NOMBREMANDANTE, "+
			 "	'N/A' as FIDEICOMISO, "+
			 " CASE D.CG_DUPLICADO WHEN 'S' THEN 'Si' ELSE 'No' END AS DUPLICADO" +
			 consultaDoctos +
			      " , decode( d.cs_dscto_especial,  'D', decode (d.CS_VALIDACION_JUR, 'S', 'SI', 'N', 'NO' ), '' )  as  VALIDACION_JUR "+
				" , d.CG_NUM_CONTRATO  ,d.CG_NUM_COPADE "+ //QC2018
			
				" FROM comrel_producto_epo pe, comvis_aforo cv, comcat_epo E, " +
				" comcat_estatus_docto ED, com_documento D, comcat_moneda M, " +
				" comcat_tipo_factoraje tf, comcat_mandante ma, " +
				" comcat_if I, comcat_if I2, comcat_pyme P "+
				" WHERE D.ic_epo = E.ic_epo "+
				" AND D.ic_epo = pe.ic_epo " +
				" AND pe.ic_producto_nafin = 1 " +
				" AND D.ic_epo = E.ic_epo " +
				" AND ma.IC_MANDANTE(+) = d.IC_MANDANTE " +
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
				" and D.ic_epo = cv.ic_epo "+
				" and D.ic_pyme = P.ic_pyme "+
				" and D.ic_moneda = M.ic_moneda "+
				" and D.ic_estatus_docto = ED.ic_estatus_docto "+
				" and D.ic_beneficiario = I2.ic_if (+)"+
				" and D.cs_dscto_especial = tf.cc_tipo_factoraje " +
				" and D.ic_if = I.ic_if(+)");
			System.out.print(qrySentencia);
			if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
			  qrySentencia.append(" and exists (select 1"   +
                        "    from com_docto_seleccionado ds"   +
                        " 	 where d.ic_documento=ds.ic_documento"   +
                        " 	 and   ds.cc_acuse like ?) ")  ;
			if (!ic_epo.equals(""))
				qrySentencia.append(" and D.ic_epo = ?");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				qrySentencia.append(" and D.ic_pyme = ?");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				qrySentencia.append(" and D.ic_if = ?");
			if (!ig_numero_docto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ?");

			if (!df_fecha_docto_de.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO >= trunc(TO_DATE(?,'DD/MM/YYYY'))  ");
			}
			if (!df_fecha_docto_a.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals(""))
				qrySentencia.append(" and D.DF_FECHA_VENC >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.DF_FECHA_VENC < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ?");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				qrySentencia.append(" and D.fn_monto between ? and ?");
			if (!ic_estatus_docto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ?");
			else
      		{ if (infoProcesadaEn.equals("5")||infoProcesadaEn.equals("8")||infoProcesadaEn.equals("9"))
          		qrySentencia.append(" and D.ic_estatus_docto in (3,4,11) ");
        	  else
          		qrySentencia.append(" and D.ic_estatus_docto in(2,3) ");
			    
      		}
			if (!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=?");

			// Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				qrySentencia.append(" AND D.ic_documento = ? ");
			}


			if (!"".equals(validaDoctoEPO)   ) {
			    qrySentencia.append(" AND D.CS_VALIDACION_JUR = '" + this.validaDoctoEPO + "' ");
			}

			//QC-2018
			if (!"".equals(contrato)  ) {			
			qrySentencia.append(" AND upper(trim(D.CG_NUM_CONTRATO)) LIKE upper(?) ");
			}
				
			if (!"".equals(copade)  ) {				
				qrySentencia.append(" AND upper(trim(D.CG_NUM_COPADE)) LIKE upper(?) ");
			}
			
			String criterioOrdenamiento =
				Comunes.ordenarCampos(orden1, campos1);
			if (!criterioOrdenamiento.equals(""))
				criterioOrdenamiento = " order by " + criterioOrdenamiento;

			qrySentencia.append(criterioOrdenamiento);
		}
		else if (
			ic_estatus_docto.equals("3")
				|| ic_estatus_docto.equals("4")
				|| ic_estatus_docto.equals("11")
				|| ic_estatus_docto.equals("12")
				|| ic_estatus_docto.equals("16")
				|| ic_estatus_docto.equals("24")
				|| ic_estatus_docto.equals("26"))//FODEA 005 - 2009 ACF
		{
		log.debug("Paso x aqui 1.8 ----------");
		
			// Obtener condiciones de consulta del plazo
			String plazo 					= null;
			String condicionesPlazo 	= null;
			String tablasPlazo			= null;
			if(
				ic_estatus_docto.equals("3") 	// Seleccionada Pyme
					||
				ic_estatus_docto.equals("24")	// En proceso de Autorizacion IF
			){
			
				// Fodea 048 - 2012 -- Consulta Plazo, para doctos con estatus: 3 (Seleccionada Pyme) y 24 (En Proceso de Autorizacion IF)
				plazo 			= 
					" ( " +
						"  DECODE( PE.CS_FECHA_VENC_PYME, 'S', D.DF_FECHA_VENC_PYME, D.DF_FECHA_VENC )" + " - " +
						" 	(CASE  " +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
						"    TRUNC(SYSDATE) " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	END) " + 
					" ) " + 	
					" AS ig_plazo, ";
					
				condicionesPlazo =
						" 	AND D.ic_if                = iexp.ic_if  "  +
						" 	AND D.ic_epo               = iexp.ic_epo "  +
						" 	AND iexp.ic_producto_nafin = 1           ";
						/*
						+
						"  AND D.ic_epo               = pe.ic_epo   "  +
						"  AND pe.ic_producto_nafin   = 1           ";
						*/
						
				tablasPlazo		= 
						" , comrel_if_epo_x_producto iexp ";
						/*
						+
						" , comrel_producto_epo      pe   ";
						*/
						
			} else if( 
				ic_estatus_docto.equals("26") // Programado Pyme
			){ 
			
				// Fodea 048 - 2012 -- Consulta Plazo, para doctos con estatus: 26 (Programado Pyme)
				plazo 			=
					" ( "  +
						"  DECODE( PE.CS_FECHA_VENC_PYME, 'S', D.DF_FECHA_VENC_PYME, D.DF_FECHA_VENC )" + " - " +
						" 	(CASE  "  +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN "  +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN "  +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 2, '+') " +
						" 	END) "  + 
					" ) "  + 	
					" AS ig_plazo, ";
					
				condicionesPlazo =
						" 	AND D.ic_if                = iexp.ic_if  "  +
						" 	AND D.ic_epo               = iexp.ic_epo "  +
						" 	AND iexp.ic_producto_nafin = 1           ";
						/*
						 +
						"  AND D.ic_epo               = pe.ic_epo   "  +
						"  AND pe.ic_producto_nafin   = 1           ";
						*/
						
				tablasPlazo		= 
						" , comrel_if_epo_x_producto iexp " ;
						/*
						 + 
						 " , comrel_producto_epo      pe   ";
						*/
			} else {
				
				// " 	DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(DS.df_fecha_seleccion), 54, sigfechahabilxepo (E.ic_epo, TRUNC(DS.df_fecha_seleccion), 1, '+')), S.ig_plazo ) AS ig_plazo, "   +
				plazo 				= " S.ig_plazo AS ig_plazo, ";
				condicionesPlazo 	= null;
				tablasPlazo			= null;
			}
			
			qrySentencia.append(
				"  SELECT " +
                //"/*+  use_nl(d ds cc1 s iexp i i2 i3 p e pe) */  "   +
                "/* ordered  use_nl(d dds s cc1 p pe e i i2 i3 ed m tf ma cd) index (d IN_COM_DOCUMENTO_07_NUK) */"+
				"    E.cg_razon_social nombreEpo,"   +
				"  	P.cg_razon_social nombrePyme,"   +
				"  	I.cg_razon_social AS nombreIf,"   +
				"  	D.ig_numero_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') AS df_fecha_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS df_fecha_venc,"   +
				"  	M.cd_nombre,"   +
				"  	D.fn_monto,"   +
				"  	D.ic_estatus_docto,"   +
				"  	D.fn_porc_anticipo,"   +
				"  	D.fn_monto_dscto,"   +
				"  	DS.in_tasa_aceptada,"   +
				"  	ED.cd_descripcion,"   +
				"  	TO_CHAR(S.df_fecha_solicitud,'DD/MM/YYYY') AS df_fecha_solicitud,"   +
				"  	D.ic_moneda, D.ic_documento,"   +
				"  	D.cs_cambio_importe,"   +
				"  	D.CS_DSCTO_ESPECIAL,"   +
				"  	DECODE(d.cs_dscto_especial,'D',i2.cg_razon_social,'I',i2.cg_razon_social,'') AS beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'') AS fn_porc_beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'') AS RECIBIR_BENEF,"   +
				"  	DECODE(d.cs_dscto_especial,'D',ds.in_importe_recibir - ds.fn_importe_recibir_benef,'I',ds.in_importe_recibir - ds.fn_importe_recibir_benef,'') AS NETO_REC_PYME,"   +
				"  	DS.cc_acuse,"   +
				"  	DS.in_importe_recibir,"   +
				"  	CASE WHEN cc1.fn_monto_pago IS NOT NULL THEN"   +
				"  		cc1.fn_monto_pago"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_monto_pago IS NOT NULL THEN"   +
				"  			cd.fn_monto_pago"   +
				"  		END"   +
				" 	END AS fn_monto_pago,"   +
				"  	CASE WHEN cc1.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  		cc1.fn_porc_docto_aplicado"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  			cd.fn_porc_docto_aplicado"   +
				"  		END"   +
				"  	END AS porcDoctoAplicado,"   +
				// " 	DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(DS.df_fecha_seleccion), 54, sigfechahabilxepo (E.ic_epo, TRUNC(DS.df_fecha_seleccion), 1, '+')), S.ig_plazo ) AS ig_plazo,"   +
				"  decode( d.cs_dscto_especial,  'D', decode (d.CS_VALIDACION_JUR, 'S', 'SI', 'N', 'NO' ), '' )  as  VALIDACION_JUR, "+
	
				plazo +
				" 	DS.in_importe_interes,"   +
				"  	D.ic_pyme,"   +
				"   I.ig_tipo_piso, DS.fn_remanente, "+ //foda 015-2005
		    	"   to_char(D.DF_ALTA, 'dd/mm/yyyy') df_alta, "+
				"  	'ConsDoctosNafinDE::getDocumentQueryFile' AS pantalla,P.cg_rfc as rfc, "+
				" 		D.ic_epo, " +
				"	TO_CHAR(ds.df_programacion, 'dd/mm/yyyy') AS fecha_registro_operacion, "+//FODEA 005 - 2009 ACF
				"    tf.cg_nombre AS TIPO_FACTORAJE "+
				"  , ma.cg_razon_social as NOMBREMANDANTE, " +
				"	DECODE(ds.cs_opera_fiso,'S',I3.cg_razon_social,'N/A') as FIDEICOMISO"+
				consultaDoctos + "," +
				" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND d.ic_docto_asociado IS NOT NULL THEN 'true' ELSE 'false' END AS existeDocumentoAsociado ," +
				" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM com_documento WHERE ic_docto_asociado = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredSimpleApl, " +
				" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_NOTA_CREDITO = d.ic_documento) THEN 'true' ELSE 'false' END AS existeRefEnComrelNotaDocto, " +
				" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_DOCUMENTO = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredMultApl " +
				" , d.CG_NUM_CONTRATO  ,d.CG_NUM_COPADE "+ //QC2018
			
				"  FROM"   +
				"  	com_documento D,              "   +
				"  	com_docto_seleccionado DS,"   +
				"  	com_solicitud S,"   +
				"  	(SELECT " +
                //  "       /*+index(cc cp_com_cobro_credito_pk)*/"+
                "   cc.ic_documento, cc.fn_porc_docto_aplicado ,"   +
				"  	SUM(cc.fn_monto_pago) AS fn_monto_pago"   +
				"  	FROM com_cobro_credito cc"   +
				"  	GROUP BY cc.ic_documento, cc.fn_porc_docto_aplicado) cc1,"   +
				"  	comcat_pyme P,"   +
                "   comrel_producto_epo pe, " +
                "   comcat_epo E,"   +
				"  	comcat_if I,"   +
				"  	comcat_if I2,"   +
				"	comcat_if I3,"+
				" 	comcat_estatus_docto ED,"   +
				"  	comcat_moneda M,"   +
				"   COMCAT_TIPO_FACTORAJE tf, " +
				"   COMCAT_MANDANTE ma, "	+
                "   fop_cobro_disposicion cd"   +                
				(tablasPlazo != null?tablasPlazo:"") + // Fodea 048 - 2012 -- Tabla para asociada a la consulta del Plazo, estatus 3, 24 y 26.
				"  WHERE D.ic_epo = E.ic_epo"   +
				" 		AND D.ic_epo = pe.ic_epo " +
				"  AND ma.IC_MANDANTE(+) = d.IC_MANDANTE " +
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        (Perfil.equals("ADMIN BANCOMEXT")?" and S.ic_banco_fondeo= 2":"") +
				" 		AND pe.ic_producto_nafin = 1 " +
				"  	AND D.ic_pyme = P.ic_pyme"   +
				"  	AND D.ic_moneda = M.ic_moneda"   +
				"  	AND D.ic_documento = DS.ic_documento"   +
				"  	AND D.ic_estatus_docto = ED.ic_estatus_docto"   +
				"  	AND D.ic_documento = S.ic_documento(+)"   +
				"  	AND D.ic_documento = CD.ic_documento(+)"   +
				"  	AND D.ic_if = I.ic_if(+)"   +
				"		AND I3.ic_if = DS.ic_if "+
				"  	AND D.ic_beneficiario = I2.ic_if (+)"   +
				"     AND D.cs_dscto_especial = tf.cc_tipo_factoraje " +
				"  	AND DS.ic_documento = CC1.ic_documento(+)" ) ;

     		if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
        		qrySentencia.append(" and ds.cc_acuse like ? ");
			if (operaFideicomiso.equals("S")){
						qrySentencia.append(" and ds.CS_OPERA_FISO = 'S' ");
					}
			if (!ic_epo.equals(""))
				qrySentencia.append(" and D.ic_epo = ?");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				qrySentencia.append(" and D.ic_pyme = ?");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				qrySentencia.append(" and D.ic_if = ?");
			if (!ig_numero_docto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ?");

			if (!df_fecha_docto_de.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO >= trunc(TO_DATE(?,'DD/MM/YYYY'))  ");
			}
			if (!df_fecha_docto_a.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals(""))
				qrySentencia.append(" and D.DF_FECHA_VENC >= TO_DATE(?,'DD/MM/YYYY') and D.DF_FECHA_VENC < TO_DATE(?,'DD/MM/YYYY') + 1 ");
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ?");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				qrySentencia.append(" and D.fn_monto between ? and ?");


			if (!ic_estatus_docto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ?");
			if (!ic_estatus_docto.equals("3")
				&& !ic_estatus_docto.equals("24"))
			{
				if (!df_fecha_seleccion_de.equals("")
					&& !df_fecha_seleccion_a.equals(""))
					qrySentencia.append(
						" and S.df_fecha_solicitud >= TO_DATE(?,'DD/MM/YYYY') and S.df_fecha_solicitud < TO_DATE(?,'DD/MM/YYYY')+1 ");
			}
			if (!in_tasa_aceptada_de.equals("")
				&& !in_tasa_aceptada_a.equals(""))
				qrySentencia.append(" and DS.in_tasa_aceptada between ? and ?");
			if (!"".equals(tipoFactoraje))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=?");

			// Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				qrySentencia.append(" AND D.ic_documento = ? ");
			}
			if (!"".equals(validaDoctoEPO)   ) {
			     qrySentencia.append(" AND D.CS_VALIDACION_JUR = '" + this.validaDoctoEPO + "' ");	
			}
			
			//QC-2018
			if (!"".equals(contrato)  ) {			
			qrySentencia.append(" AND upper(trim(D.CG_NUM_CONTRATO)) LIKE upper(?) ");
			}
				
			if (!"".equals(copade)  ) {				
				qrySentencia.append(" AND upper(trim(D.CG_NUM_COPADE)) LIKE upper(?) ");
			}
			
			// Fodea 048 - 2012 -- Condiciones para el Plazo, estatus 3, 24 y 26.
			if( condicionesPlazo != null ){
				qrySentencia.append(condicionesPlazo);
			}

			String criterioOrdenamiento =
				Comunes.ordenarCampos(orden2, campos2);
			if (!criterioOrdenamiento.equals(""))
				criterioOrdenamiento = " order by " + criterioOrdenamiento;

			qrySentencia.append(criterioOrdenamiento);
			//System.out.println(qrySentencia);
		} else {
			System.out.println(
				"ic_estatus_docto value not handled:  " + ic_estatus_docto);
		}

		// this creates an "and" clause right after the where
		//qrySentencia = qrySentencia.replaceFirst("where and","where");
		int iIndex = qrySentencia.indexOf("where  and");
		if(iIndex > 0){
			String toWhere = qrySentencia.substring(0,qrySentencia.indexOf("where  and"));
			System.out.println(toWhere);
			qrySentencia = new StringBuffer(toWhere + "where" + qrySentencia.substring(qrySentencia.indexOf("where  and")+10,qrySentencia.length()));
		}

		log.debug("-------------query----2---------------------"+validaDoctoEPO); 
		log.debug("..:: qrySentencia :---"+qrySentencia.toString());     
		log.info("getDocumentQueryFileExtjs(S)");
		return qrySentencia.toString();
	}

/**
 * Genera el archivo CSV o PDF seg�n el tipo que se le indique.
 * En el caso del PDF, el archivo generado no comtempla paginaci�n, imprime todos los registros que trae la consulta.
 * @param request
 * @param rs
 * @param path
 * @param tipo: CSV o PDF
 * @return nombre del archivo
 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)" );
		String nombreArchivo          = "";
		HttpSession session           = request.getSession();
		StringBuffer contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer     = null;
		BufferedWriter buffer         = null;
		int total                     = 0;

		String NOM_EPO                  = "";
		String NOM_PYME                 = "";
		String NOM_IF                   = "";
		String IG_NUMERO_DOCTO          = "";
		String DF_FECHA_DOCTO           = "";
		String DF_FECHA_VENC            = "";
		String RFC                      = "";
		String NOM_MONEDA               = "";
		String FN_MONTO                 = "";
		String FN_MONTO_DSCTO           = "";
		String IN_TASA_ACEPTADA         = "";
		String ESTATUS_DOCTO            = "";
		String DF_ALTA                  = "";
		String IC_EPO                   = "";
		String CC_ACUSE                 = "";
		String NombreMandante           = "";
		String FECHA_SOLICITUD          = "";
		String strEstatus               = "";
		String IC_DOCUMENTO             = "";
		String CS_CAMBIO_IMPORTE        = "";
		String Parametros               = "";
		String nombreTipoFactoraje      = "";
		String tipoFactorajeDesc        = "";
		String tasa                     = "";
		String plazo                    = "";
		String porc_docto_aplicado      = "";
		String beneficiario             = "";
		String cc_acuse                 = "";
		String numeroPedido             = "";
		String nomArchivo               = "";
		String FECHA_ENTREGA            = "";
		String TIPO_COMPRA              = "";
		String CLAVE_PRESUPUESTARIA     = "";
		String PERIODO                  = "";
		String fideicomiso              = "";
		String numeroDocumento          = "";
		String fecha_registro_operacion = "";
		String df_programacion          = "";
		String duplicado                = "";
		String ESTATUS_DOCTO_NOMBRE     = "";
		String validacion ="";
		String contratoArch ="";
		String copadeArch ="";

		double dblPorcentaje                  = 0;
		double dblRecurso                     = 0;
		double montoPago                      = 0;
		double importe_recibir                = 0;
		double importe_depositar_pyme         = 0;
		double importe_interes                = 0;
		double importe_a_recibir_beneficiario = 0;
		double porciento_beneficiario         = 0;
		double neto_a_recibir_pyme            = 0;

		int IC_MONEDA      = 0;
		int registros      = 0;
		int numRegistros   = 0;
		int numRegistrosMN = 0;
		int numRegistrosDL = 0;
		HashMap datos = new HashMap();
		boolean siaff                                       = false;
		boolean esNotaDeCreditoAplicada                     = false;
		boolean esNotaDeCreditoSimpleAplicada               = false;
		boolean esDocumentoConNotaDeCreditoSimpleAplicada   = false;
		boolean esDocumentoConNotaDeCreditoMultipleAplicada = false;
		String nombresDoctos = ",Fecha de Recepci�n de Bienes y Servicios, Tipo de Compra (procedimiento), Clasificador por Objeto del Gasto, Plazo M�ximo";

		if(tipo.equals("CSV")){
                    /* EGOY Se obtiene el total de la consulta, mas de 1000 registros no trae los campos adicionales*/
		    String infoRegresar ="";
		    CQueryHelperRegExtJS queryHelperE = new CQueryHelperRegExtJS(new ConsDoctosNafinDE());
		    infoRegresar  = queryHelperE.getJSONResultCount(request);
		    JSONObject jsonObjPrueba = new JSONObject();
		    jsonObjPrueba=JSONObject.fromObject(infoRegresar);
		    System.out.println(jsonObjPrueba.get("registros" ));
		    JSONArray jsonArray = new JSONArray();
		    jsonArray= (JSONArray)jsonObjPrueba.get("registros" );
		    Iterator itJsonArray =jsonArray.iterator();
                    int coutJsonObj=0;
                    int sumaTotal=0;
                    while(itJsonArray.hasNext()){
                        System.out.println(coutJsonObj);
                        JSONObject jsonObjectTMP=JSONObject.fromObject(itJsonArray.next());
                        String strjsonObjectTMP=(String)jsonObjectTMP.get("CONSDOCTOSNAFINDE");
                        if(strjsonObjectTMP != null && !strjsonObjectTMP.equals("")){
                            sumaTotal+= Integer.parseInt(strjsonObjectTMP);
                        }
                        coutJsonObj++;
                    }
		    /* EGOY Se obtiene el total de la consulta, mas de 1000 registros no trae los campos adicionales*/
			try {

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);					
	
				ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
	
				List nombresCamposAdicionales = new ArrayList();
				int numeroCamposAdicionales = 0;
				String strCamposAdicionales = "";
				if(!ic_epo.equals("")&&sumaTotal<1000){numeroCamposAdicionales = BeanParamDscto.getNumeroCamposAdicionales(ic_epo);}
				if(!ic_epo.equals("")&&sumaTotal<1000){nombresCamposAdicionales = BeanParamDscto.getNombresCamposAdicionales(ic_epo);}
				for(int i = 0; i < nombresCamposAdicionales.size(); i++){strCamposAdicionales += "," + nombresCamposAdicionales.get(i);}
	
				boolean estaHabilitadoNumeroSIAFF = true;
				if(!numero_siaff.equals("")){
					String claveEPO = getClaveEPO(numero_siaff);
					if(!BeanParamDscto.estaHabilitadoNumeroSIAFF(claveEPO)){
						estaHabilitadoNumeroSIAFF = false;
					}else{
						estaHabilitadoNumeroSIAFF = true;
					}
				}
	
				if(ic_estatus_docto.equals("") || ic_estatus_docto.equals("1") || ic_estatus_docto.equals("2") || ic_estatus_docto.equals("5") || ic_estatus_docto.equals("6") || ic_estatus_docto.equals("7") || ic_estatus_docto.equals("9") || ic_estatus_docto.equals("10") || ic_estatus_docto.equals("21")|| ic_estatus_docto.equals("23")|| ic_estatus_docto.equals("28") || ic_estatus_docto.equals("29")|| ic_estatus_docto.equals("30")|| ic_estatus_docto.equals("31") || ic_estatus_docto.equals("33")) {//FODEA 005 - 2009 ACF
					if(strPerfil.equals("PROMO NAFIN")){
						contenidoArchivo.append("Nombre EPO,PYME,IF,FIDEICOMISO,R.F.C,Num. Documento,Fecha emision,Fecha vencimiento,Moneda,Tipo Factoraje,Monto,Monto Descontar,Estatus,Porcentaje Descuento,Recurso Garantia,Beneficiario,% Beneficiario,Importe a Recibir Beneficiario,Fecha para Efectuar Descuento,Fecha Alta Docto.,Digito Identificador"+nombresDoctos+strCamposAdicionales);
					}else{
					contenidoArchivo.append   ("Nombre EPO,PYME,IF,FIDEICOMISO,Num. Documento,Fecha emision,Fecha vencimiento,Moneda,Tipo Factoraje,Monto,Monto Descontar,Estatus,Porcentaje Descuento,Recurso Garantia,Beneficiario,% Beneficiario,Importe a Recibir Beneficiario,Fecha para Efectuar Descuento,Fecha Alta Docto.,Notificado como Duplicado,Digito Identificador"+nombresDoctos+strCamposAdicionales+",");
					contenidoArchivo.append("Mandante, Validaci�n de Documento EPO ");
					}
	
					contenidoArchivo.append(",Contrato, COPADE ");
					contenidoArchivo.append("\n");
					
					while(rs.next()){
						NOM_EPO              = (rs.getString(1)                         ==null)?""    :rs.getString(1);
						NOM_PYME             = (rs.getString(2)                         == null)?""   :rs.getString(2);
						IG_NUMERO_DOCTO      = (rs.getString(3)                         ==null)?""    :rs.getString(3);
						DF_FECHA_DOCTO       = (rs.getString(4)                         ==null)?""    :rs.getString(4);
						DF_FECHA_VENC        = (rs.getString(5)                         ==null)?""    :rs.getString(5);
						NOM_MONEDA           = (rs.getString(6)                         ==null)?""    :rs.getString(6);
						IC_MONEDA            = Integer.parseInt((rs.getString(11)       ==null)?"0"   :rs.getString(11));
						FN_MONTO             = (rs.getString(7)                         ==null)?"0.00":rs.getString(7);
						dblPorcentaje        = Double.parseDouble((rs.getString(12)     ==null)?"0"   :rs.getString(12));
						strEstatus           = (rs.getString(13)                        ==null)?""    :rs.getString(13);
						FN_MONTO_DSCTO       = (rs.getString(9)                         ==null)?"0.00":rs.getString(9);
						ESTATUS_DOCTO        = (rs.getString(10)                        ==null)?""    :rs.getString(10);
						IC_DOCUMENTO         = (rs.getString(14)                        ==null)?""    :rs.getString(14);
						CS_CAMBIO_IMPORTE    = (rs.getString(15)                        ==null)?""    :rs.getString(15);
						NOM_IF               = (rs.getString(16)                        ==null)?""    :rs.getString(16).trim();
						RFC                  = (rs.getString(23)                        ==null)?""    :rs.getString(23).trim();
						DF_ALTA              = (rs.getString("DF_ALTA")                 ==null)?""    :rs.getString("DF_ALTA").trim();
						IC_EPO               = (rs.getString("IC_EPO")                  ==null)?""    :rs.getString("IC_EPO").trim();
						CC_ACUSE             = (rs.getString("CC_ACUSE")                ==null)?""    :rs.getString("CC_ACUSE").trim();
						NombreMandante       = (rs.getString("NOMBREMANDANTE")          ==null)?""    :rs.getString("NOMBREMANDANTE").trim();
						fideicomiso          = (rs.getString("FIDEICOMISO")             ==null)?""    :rs.getString("FIDEICOMISO").trim();
						duplicado            = (rs.getString("DUPLICADO")               ==null)?""    :rs.getString("DUPLICADO").trim(); //Fodea 38-2014
						//si hay bandera de documentos se recuperan los valores
			//			if(banderaTablaDoctos.equals("1")){
						FECHA_ENTREGA        = (rs.getString("DF_ENTREGA")              ==null)?""    :rs.getString("DF_ENTREGA");
						TIPO_COMPRA          = (rs.getString("CG_TIPO_COMPRA")          ==null)?""    :rs.getString("CG_TIPO_COMPRA");
						CLAVE_PRESUPUESTARIA = (rs.getString("CG_CLAVE_PRESUPUESTARIA") ==null)?""    :rs.getString("CG_CLAVE_PRESUPUESTARIA");
						PERIODO              = (rs.getString("CG_PERIODO")              ==null)?""    :rs.getString("CG_PERIODO");
			//			}
						validacion          = (rs.getString("VALIDACION_JUR")          ==null)?""    :rs.getString("VALIDACION_JUR");
						contratoArch          = (rs.getString("cg_num_contrato")          ==null)?""    :rs.getString("cg_num_contrato");
						copadeArch          = (rs.getString("cg_num_copade")          ==null)?""    :rs.getString("cg_num_copade");
						
						
						if (datos.containsKey(IC_EPO)){
							siaff = ((Boolean)datos.get(IC_EPO)).booleanValue();
						} else{
							siaff = BeanParamDscto.estaHabilitadoNumeroSIAFF(IC_EPO);
							datos.put(IC_EPO,new Boolean(siaff));
						}
	
						if(siaff){
							numero_siaff = getNumeroSIAFF(IC_EPO,IC_DOCUMENTO);
						}else{
							numero_siaff = "";
						}
	
						beneficiario = (rs.getString(18)==null)?"":rs.getString(18).trim();
						porciento_beneficiario =Double.parseDouble((rs.getString(19)==null)?"0":rs.getString(19));
						importe_a_recibir_beneficiario = Double.parseDouble((rs.getString(20)==null)?"0":rs.getString(20) );
	
						if(strEstatus.equals("2") || strEstatus.equals("5") || strEstatus.equals("6") ||
							strEstatus.equals("7") || strEstatus.equals("9") || strEstatus.equals("10")){
								dblPorcentaje = Double.parseDouble(rs.getString(8));
								FN_MONTO_DSCTO = new Double(new Double(FN_MONTO).doubleValue() * (dblPorcentaje / 100)).toString();
						}
	
						tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 
						nombreTipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
						if(nombreTipoFactoraje.equals("V")) {
							FN_MONTO_DSCTO = FN_MONTO;
							dblPorcentaje = 100;
						}
	
						/*FODEA 005 - 2009 ACF
									if (strEstatus.equals("26")){//26 = Programado foda 025-2005
										df_programacion = (rs.getString("df_programacion")==null)?"":rs.getString("df_programacion").trim();
										dblPorcentaje = rs.getDouble(8);
										FN_MONTO_DSCTO = new Double(new Double(FN_MONTO).doubleValue() * (dblPorcentaje / 100)).toString();
									}
						*/
						//			nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal":(nombreTipoFactoraje.equals("V"))?"Vencido":(nombreTipoFactoraje.equals("D"))?"Distribuido":"";
						tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 
	
						/*
						if("N".equals(nombreTipoFactoraje))
							nombreTipoFactoraje = "Normal";
						else if("V".equals(nombreTipoFactoraje))
							nombreTipoFactoraje = "Vencido";
						else if("D".equals(nombreTipoFactoraje))
							nombreTipoFactoraje = "Distribuido";
						else if("C".equals(nombreTipoFactoraje))
							nombreTipoFactoraje = "Nota de Cr�dito";
						*/
	
						dblRecurso = new Double(FN_MONTO).doubleValue() - new Double(FN_MONTO_DSCTO).doubleValue();				
						registros++;
						/*GENERAMOS EL ARCHIVO*/
						if(strPerfil.equals("PROMO NAFIN")){
							contenidoArchivo.append( "\""+NOM_EPO.replace(',',' ')+"\",\""+NOM_PYME.replace(',',' ')+"\",\""+NOM_IF.replace(',',' ')+"\",\""+fideicomiso.replaceAll(",","")+"\",\""+RFC+"\","+IG_NUMERO_DOCTO+","
								+DF_FECHA_DOCTO+","+DF_FECHA_VENC+","+NOM_MONEDA+","+tipoFactorajeDesc+","+FN_MONTO+","
								+FN_MONTO_DSCTO+","+ESTATUS_DOCTO+","+Comunes.formatoDecimal(dblPorcentaje,0,false)+","+dblRecurso);
							} else{
						contenidoArchivo.append("\""+NOM_EPO.replace(',',' ')+"\",\""+NOM_PYME.replace(',',' ')+"\",\""+NOM_IF.replace(',',' ')+"\",\""+fideicomiso.replaceAll(",","")+"\","+IG_NUMERO_DOCTO+","
								+DF_FECHA_DOCTO+","+DF_FECHA_VENC+","+NOM_MONEDA+","+tipoFactorajeDesc+","+FN_MONTO+","
								+FN_MONTO_DSCTO+","+ESTATUS_DOCTO+","+Comunes.formatoDecimal(dblPorcentaje,0,false)+","+dblRecurso);
						}
	
						if(!"".equals(beneficiario)) contenidoArchivo.append(","+beneficiario.replace(',',' ')); else contenidoArchivo.append(",");
						if(0 != porciento_beneficiario) contenidoArchivo.append(","+porciento_beneficiario); else contenidoArchivo.append(",");
						if(0 != importe_a_recibir_beneficiario) contenidoArchivo.append(","+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false)); else contenidoArchivo.append(",");
	
						contenidoArchivo.append("," + df_programacion + ","+DF_ALTA+","+duplicado+","+numero_siaff);
	
						//si hay bandera de documentos se recuperan los valores
						//	if(banderaTablaDoctos.equals("1")){
						PERIODO=PERIODO.equals("0")?"":PERIODO;
						contenidoArchivo.append("," + FECHA_ENTREGA + "," + TIPO_COMPRA + "," + CLAVE_PRESUPUESTARIA + "," + PERIODO);
						//	}
						//============================================================================>> FODEA 002 - 2009 (I)
						if(!ic_epo.equals("")&&sumaTotal<1000){
							List datosCamposAdicionales = new ArrayList();
							if(!ic_epo.equals("")){datosCamposAdicionales = BeanParamDscto.getDatosCamposAdicionales(IC_DOCUMENTO, ic_epo, numeroCamposAdicionales);}
								for(int i = 0; i < numeroCamposAdicionales; i++){
									contenidoArchivo.append("," + datosCamposAdicionales.get(i));
							}
						}
	
						contenidoArchivo.append(","+NombreMandante);
						contenidoArchivo.append(","+validacion);
						
						contenidoArchivo.append(","+contratoArch); //QC-2018
						contenidoArchivo.append(","+copadeArch); //QC-2018
						
					    
						//============================================================================>> FODEA 002 - 2009 (F)		
						contenidoArchivo.append("\n");
	
						total++;
						if(total==1000){
							total=0;	
							buffer.write(contenidoArchivo.toString());
							contenidoArchivo = new StringBuffer();//Limpio  
						}
				
					} // while
					buffer.write(contenidoArchivo.toString());
					buffer.close();	
					contenidoArchivo = new StringBuffer();//Limpio   
					
				} //if estatus="",2,5,6,7,9,10
	
					/*			Seleccionada Pyme					Operada						Operada Pagada				Operada Pendiente de Pago        Aplicado a Credito            En Proceso de Autorizaci�n IF		Programado Pyme*/
				if(ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12") || ic_estatus_docto.equals("16") || ic_estatus_docto.equals("24") || ic_estatus_docto.equals("26")){//FODEA 005 - 2009 ACF
					//rs = con.queryDB(qrySentencia);
				System.out.println("***************************************************************************************"+strPerfil);
				if(strPerfil.equals("PROMO NAFIN")){
						contenidoArchivo.append("Nombre EPO,PYME,IF,FIDEICOMISO,R.F.C,Num. Documento,Num. Acuse," +
							"Fecha emision,Fecha vencimiento,Plazo,Moneda,Tipo Factoraje," +
							"Monto,Porcentaje descuento,Recurso en garantia, Monto Descontar," +
							"Intereses del documento,Monto a recibir,Tasa,Estatus," +
							"Importe aplicado a credito,Porcentaje del documento aplicado," +
							"Importe a depositar a la Pyme,");
				}else{
					contenidoArchivo.append("Nombre EPO,PYME,IF,FIDEICOMISO,Num. Documento,Num. Acuse," +
							"Fecha emision,Fecha vencimiento,Plazo,Moneda,Tipo Factoraje," +
							"Monto,Porcentaje descuento,Recurso en garantia, Monto Descontar," +
							"Intereses del documento,Monto a recibir,Tasa,Estatus," +
							"Importe aplicado a credito,Porcentaje del documento aplicado," +
							"Importe a depositar a la Pyme,");
				}
			//============================================================================>> FODEA 002 - 2009 (I)
					if (ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12")){
						contenidoArchivo.append("Fecha Aut. IF,");
					}
					contenidoArchivo.append("Neto a Recibir,Doctos Aplicados a Nota de Credito,Beneficiario,% Beneficiario,Importe a Recibir Beneficiario,Fecha Alta Docto.,Digito Identificador"+nombresDoctos+"");
					contenidoArchivo.append(strCamposAdicionales);
					if(ic_estatus_docto.equals("26")){contenidoArchivo.append(",Fecha Registro Operaci�n");}//FODEA 005 - 2009 ACF
					contenidoArchivo.append(",Validaci�n de Documento EPO");
					contenidoArchivo.append(",Contrato, COPADE ");					
					contenidoArchivo.append( "\n");
			//============================================================================>> FODEA 002 - 2009 (F)		
					String ig_tipo_piso="",fn_remanente="";
					while (rs.next()){
						NOM_EPO = (rs.getString("NOMBREEPO") == null) ? "" : rs.getString("NOMBREEPO");
						NOM_PYME = (rs.getString("NOMBREPYME") == null) ? "" : rs.getString("NOMBREPYME");
						NOM_IF = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
						IG_NUMERO_DOCTO = (rs.getString("IG_NUMERO_DOCTO") == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
						cc_acuse = rs.getString("CC_ACUSE");
						DF_FECHA_DOCTO = (rs.getString("DF_FECHA_DOCTO") == null) ? "" : rs.getString("DF_FECHA_DOCTO");
						DF_FECHA_VENC = (rs.getString("DF_FECHA_VENC") == null) ? "" : rs.getString("DF_FECHA_VENC");
						plazo = (rs.getString("IG_PLAZO") == null) ? "" : rs.getString("IG_PLAZO");
						NOM_MONEDA = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
						IC_MONEDA = Integer.parseInt(rs.getString("IC_MONEDA"));
						FN_MONTO = (rs.getString("FN_MONTO") == null) ? "0.00" : rs.getString("FN_MONTO");
						dblPorcentaje =Double.parseDouble(rs.getString("FN_PORC_ANTICIPO")==null?"0":rs.getString("FN_PORC_ANTICIPO"));
						FN_MONTO_DSCTO = (rs.getString("FN_MONTO_DSCTO")==null)?"0.00":rs.getString("FN_MONTO_DSCTO");
						importe_interes =Double.parseDouble(rs.getString("IN_IMPORTE_INTERES")==null?"0":rs.getString("IN_IMPORTE_INTERES"));
						importe_recibir =Double.parseDouble(rs.getString("IN_IMPORTE_RECIBIR")==null?"0":rs.getString("IN_IMPORTE_RECIBIR")); 
						IN_TASA_ACEPTADA = (rs.getString("IN_TASA_ACEPTADA") == null) ? "" : rs.getString("IN_TASA_ACEPTADA");
						ESTATUS_DOCTO = (rs.getString("CD_DESCRIPCION") == null) ? "" : rs.getString("CD_DESCRIPCION");
						FECHA_SOLICITUD = (rs.getString("DF_FECHA_SOLICITUD") == null) ? "" : rs.getString("DF_FECHA_SOLICITUD");
						IC_DOCUMENTO = (rs.getString("IC_DOCUMENTO") == null) ? "" : rs.getString("IC_DOCUMENTO");
						CS_CAMBIO_IMPORTE = (rs.getString("CS_CAMBIO_IMPORTE") == null) ? "" : rs.getString("CS_CAMBIO_IMPORTE");
						porc_docto_aplicado	= (rs.getString("PORCDOCTOAPLICADO") == null) ? "" : rs.getString("porcDoctoAplicado");
						montoPago = Double.parseDouble((rs.getString("FN_MONTO_PAGO")==null)?"0":rs.getString("FN_MONTO_PAGO") );
						beneficiario = (rs.getString("BENEFICIARIO")==null)?"":rs.getString("BENEFICIARIO").trim();
						porciento_beneficiario =Double.parseDouble((rs.getString("FN_PORC_BENEFICIARIO")==null)?"0":rs.getString("FN_PORC_BENEFICIARIO"));
						importe_a_recibir_beneficiario =Double.parseDouble( (rs.getString("RECIBIR_BENEF")==null)?"0":rs.getString("RECIBIR_BENEF"));
						neto_a_recibir_pyme =Double.parseDouble((rs.getString("NETO_REC_PYME")==null)?"0":rs.getString("NETO_REC_PYME")); 
						DF_ALTA = (rs.getString("DF_ALTA")==null)?"":rs.getString("DF_ALTA").trim();
						RFC = (rs.getString("RFC")==null)?"":rs.getString("RFC").trim();
						ig_tipo_piso = (rs.getString("ig_tipo_piso")==null)?"":rs.getString("ig_tipo_piso");
						fn_remanente = (rs.getString("fn_remanente")==null)?"":rs.getString("fn_remanente");
						// Factoraje Vencido
						tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 
						nombreTipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
						IC_EPO 		= (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO").trim();
						fecha_registro_operacion = rs.getString("fecha_registro_operacion")==null?"":rs.getString("fecha_registro_operacion");//FODEA 005 - 2009 ACF
						///*******2
						fideicomiso	= (rs.getString("FIDEICOMISO")==null)?"":rs.getString("FIDEICOMISO").trim();
						validacion          = (rs.getString("VALIDACION_JUR")          ==null)?""    :rs.getString("VALIDACION_JUR");
						contratoArch          = (rs.getString("cg_num_contrato")          ==null)?""    :rs.getString("cg_num_contrato");
						copadeArch          = (rs.getString("cg_num_copade")          ==null)?""    :rs.getString("cg_num_copade");
						
						
						if(porc_docto_aplicado.equals("0")){
							porc_docto_aplicado="";
						}
						
							if (datos.containsKey(IC_EPO)) {				 
								siaff = ((Boolean)datos.get(IC_EPO)).booleanValue();
							
							}else {	
													
								siaff = BeanParamDscto.estaHabilitadoNumeroSIAFF(IC_EPO);
								datos.put(IC_EPO,new Boolean(siaff));
							}
							
							if(siaff){
								numero_siaff = getNumeroSIAFF(IC_EPO,IC_DOCUMENTO);						
							}else{
								numero_siaff = "";
							}
							
							/*	
							if(BeanParamDscto.estaHabilitadoNumeroSIAFF(IC_EPO)){				
								numeroSIAFF = getNumeroSIAFF(IC_EPO,IC_DOCUMENTO);						
							}else{
									numeroSIAFF = "";						
							}
							*/
			
						//si hay bandera de documentos se recuperan los valores
			//			if(banderaTablaDoctos.equals("1")){
							FECHA_ENTREGA 					= (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
							TIPO_COMPRA 						= (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
							CLAVE_PRESUPUESTARIA 		= (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
							PERIODO 								= (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");
			//			}
							PERIODO=PERIODO.equals("0")?"":PERIODO;
			/*			if(nombreTipoFactoraje.equals("V")) {
							FN_MONTO_DSCTO = FN_MONTO;
							dblPorcentaje = 100;
						}
			*/
			//			nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal":(nombreTipoFactoraje.equals("V"))?"Vencido":(nombreTipoFactoraje.equals("D"))?"Distribuido":"";
						tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 
						
							/*
						if("N".equals(nombreTipoFactoraje))
							nombreTipoFactoraje = "Normal";
						else if("V".equals(nombreTipoFactoraje))
							nombreTipoFactoraje = "Vencido";
						else if("D".equals(nombreTipoFactoraje))
							nombreTipoFactoraje = "Distribuido";
						else if("C".equals(nombreTipoFactoraje))
							nombreTipoFactoraje = "Nota de Cr�dito";
						*/
						
						dblRecurso = new Double(FN_MONTO).doubleValue() - new Double(FN_MONTO_DSCTO).doubleValue();
						importe_depositar_pyme = importe_recibir - montoPago;
			
						// Fodea 002 - 2010
						//esNotaDeCreditoSimpleAplicada						= ( nombreTipoFactoraje.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeDocumentoAsociado(String.valueOf(IC_DOCUMENTO)) )?true:false;
						//esDocumentoConNotaDeCreditoSimpleAplicada		= ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(String.valueOf(IC_DOCUMENTO)))?true:false;
							
						//esNotaDeCreditoAplicada 							= ( nombreTipoFactoraje.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(String.valueOf(IC_DOCUMENTO)))?true:false;
						//esDocumentoConNotaDeCreditoMultipleAplicada	= ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(String.valueOf(IC_DOCUMENTO)))?true:false;
			
						esNotaDeCreditoSimpleAplicada = "true".equals(rs.getString("existeDocumentoAsociado"))?true:false;
						esDocumentoConNotaDeCreditoSimpleAplicada = "true".equals(rs.getString("esDocConNotaDeCredSimpleApl"))?true:false;
							
						esNotaDeCreditoAplicada = "true".equals(rs.getString("existeRefEnComrelNotaDocto"))?true:false;
						esDocumentoConNotaDeCreditoMultipleAplicada	= "true".equals(rs.getString("esDocConNotaDeCredMultApl"))?true:false;
			
						
						if(esNotaDeCreditoSimpleAplicada){
							numeroDocumento									= getNumeroDoctoAsociado(String.valueOf(IC_DOCUMENTO)); 
						}else if(esDocumentoConNotaDeCreditoSimpleAplicada){
							numeroDocumento									= getNumeroNotaCreditoAsociada(String.valueOf(IC_DOCUMENTO));
						}
						
						registros++;
						/*GENERAMOS EL ARCHIVO*/
					if(strPerfil.equals("PROMO NAFIN")){
						contenidoArchivo.append("\"" + NOM_EPO.replace(',',' ') + "\",\"" + NOM_PYME.replace(',',' ') +
								"\",\"" + NOM_IF.replace(',',' ') +"\",\""+fideicomiso.replaceAll(",","")+ "\",\"" + RFC.replace(',',' ') + "\"," + IG_NUMERO_DOCTO +
								"," + cc_acuse+"," + DF_FECHA_DOCTO + "," + DF_FECHA_VENC +
								"," + plazo + "," +  NOM_MONEDA + "," + tipoFactorajeDesc +
								"," + FN_MONTO + "," + dblPorcentaje + "," + dblRecurso +
								"," + FN_MONTO_DSCTO +
								"," + Comunes.formatoDecimal(importe_interes,2,false) +
								"," + Comunes.formatoDecimal(importe_recibir,2,false) +
								"," + IN_TASA_ACEPTADA + "," + ESTATUS_DOCTO +
								"," + Comunes.formatoDecimal(montoPago,2,false) +
								"," + porc_docto_aplicado) ;
					}else{
					contenidoArchivo.append("\"" + NOM_EPO.replace(',',' ') + "\",\"" + NOM_PYME.replace(',',' ') +
								"\",\"" + NOM_IF.replace(',',' ') +"\",\""+fideicomiso.replaceAll(",","")+ "\"," + IG_NUMERO_DOCTO +
								"," + cc_acuse+"," + DF_FECHA_DOCTO + "," + DF_FECHA_VENC +
								"," + plazo + "," +  NOM_MONEDA + "," + tipoFactorajeDesc +
								"," + FN_MONTO + "," + dblPorcentaje + "," + dblRecurso +
								"," + FN_MONTO_DSCTO +
								"," + Comunes.formatoDecimal(importe_interes,2,false) +
								"," + Comunes.formatoDecimal(importe_recibir,2,false) +
								"," + IN_TASA_ACEPTADA + "," + ESTATUS_DOCTO +
								"," + Comunes.formatoDecimal(montoPago,2,false) +
								"," + porc_docto_aplicado) ;
					}
			
						if(ic_estatus_docto.equals("16") ){
							if(ig_tipo_piso.equals("1")){
								if(!fn_remanente.equals("")){
									contenidoArchivo.append("," + Comunes.formatoDecimal(Double.parseDouble(fn_remanente),2,false));
								}else{
									contenidoArchivo.append("," + " ");
								}
							}else{
								contenidoArchivo.append("," + Comunes.formatoDecimal(importe_depositar_pyme,2,false));
							}
						}else{
							contenidoArchivo.append("," + Comunes.formatoDecimal(importe_depositar_pyme,2,false));
						}
		
						if (ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12")) {
							contenidoArchivo.append("," + FECHA_SOLICITUD);
						}
						contenidoArchivo.append("," + Comunes.formatoDecimal(neto_a_recibir_pyme,2,false));
						
						// Fodea 002 - 2010.
						contenidoArchivo.append(",");
						if(esNotaDeCreditoAplicada || esDocumentoConNotaDeCreditoMultipleAplicada){
							contenidoArchivo.append("Si");	
						}
						if(esNotaDeCreditoSimpleAplicada || esDocumentoConNotaDeCreditoSimpleAplicada){
							contenidoArchivo.append("\""+numeroDocumento+"\"");	
						}
						
						if(!"".equals(beneficiario)) contenidoArchivo.append(","+beneficiario.replace(',',' ')); else contenidoArchivo.append(",");
						if(0 != porciento_beneficiario) contenidoArchivo.append(","+porciento_beneficiario); else contenidoArchivo.append(",");
						if(0 != importe_a_recibir_beneficiario) contenidoArchivo.append(","+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false));  else contenidoArchivo.append(",");
			
						contenidoArchivo.append(","+DF_ALTA+","+numero_siaff);
			
						//si hay bandera de documentos se recuperan los valores
			//			if(banderaTablaDoctos.equals("1")){
							contenidoArchivo.append("," + FECHA_ENTREGA + "," + TIPO_COMPRA + "," + CLAVE_PRESUPUESTARIA + "," + PERIODO);
			//			}
			//============================================================================>> FODEA 002 - 2009 (I)
						if(!ic_epo.equals("")){
							List datosCamposAdicionales = new ArrayList();
							if(!ic_epo.equals("")){datosCamposAdicionales = BeanParamDscto.getDatosCamposAdicionales(IC_DOCUMENTO, ic_epo, numeroCamposAdicionales);}
							for(int i = 0; i < numeroCamposAdicionales; i++){
								contenidoArchivo.append("," + datosCamposAdicionales.get(i));
							}
						}
						if(ic_estatus_docto.equals("26")){contenidoArchivo.append(","+fecha_registro_operacion);}//FODEA 005 - 2009 ACF
						contenidoArchivo.append(","+validacion);
						
						contenidoArchivo.append(","+contratoArch); //QC-2018
						contenidoArchivo.append(","+copadeArch);  //QC-2018  				
						
						contenidoArchivo.append("\n");
			//============================================================================>> FODEA 002 - 2009 (F)
						total++;
							if(total==1000){					
								total=0;	
								buffer.write(contenidoArchivo.toString());
								contenidoArchivo = new StringBuffer();//Limpio  
							}
						
						
					} // while
					
					buffer.write(contenidoArchivo.toString());
						buffer.close();	
						contenidoArchivo = new StringBuffer();//Limpio   
						
				} //if estatus=3,4,11,12
	
			} catch(Throwable e){
				System.err.println("NOM_EPO: " + NOM_EPO);
				System.err.println("IG_NUMERO_DOCTO: " + IG_NUMERO_DOCTO);
				System.err.println("total: " + total);
				throw new AppException("Error al generar el archivo ", e);
			}

		} else if(tipo.equals("PDF")){

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			List nombresCamposAdicionales = new ArrayList();
			int numeroCamposAdicionales = 0;
			int cont=0;
			String strCamposAdicionales = "";
			ParametrosDescuento BeanParamDscto = null;
			String numeroSIAFF = (request.getParameter("digito")==null)?"":request.getParameter("digito");

			try{
				BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
			}catch(Exception e){}

			try {
				if(!ic_epo.equals("")){numeroCamposAdicionales = BeanParamDscto.getNumeroCamposAdicionales(ic_epo);}
				if(!ic_epo.equals("")){nombresCamposAdicionales = BeanParamDscto.getNombresCamposAdicionales(ic_epo);}
					String tiposFactorajes="";
				if(!ic_epo.equals(""))
					tiposFactorajes+=BeanParamDscto.getParametrosFactoraje(ic_epo);
				boolean banderaMandate=tiposFactorajes.indexOf("M")>0?true:false;
				if(ic_epo.equals(""))
					banderaMandate=true;
				for(int i = 0; i < nombresCamposAdicionales.size(); i++){strCamposAdicionales += "," + nombresCamposAdicionales.get(i);}
				BigDecimal montoTotalMN = new BigDecimal("0.00");
				BigDecimal montoTotalDL = new BigDecimal("0.00");
				BigDecimal montoTotalDsctoMN = new BigDecimal("0.00");
				BigDecimal montoTotalDsctoDL = new BigDecimal("0.00");
				registros = 0;
				numRegistros = 0;
				numRegistrosMN = 0;
				numRegistrosDL = 0;

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				"",
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
				if(ic_estatus_docto.equals("") || ic_estatus_docto.equals("1") || ic_estatus_docto.equals("2") || ic_estatus_docto.equals("5") || ic_estatus_docto.equals("6") || ic_estatus_docto.equals("7") || ic_estatus_docto.equals("9") || ic_estatus_docto.equals("10") || ic_estatus_docto.equals("21")|| ic_estatus_docto.equals("23")|| ic_estatus_docto.equals("28") || ic_estatus_docto.equals("29")|| ic_estatus_docto.equals("30")|| ic_estatus_docto.equals("31")|| ic_estatus_docto.equals("33")) {//FODEA 005 - 2009 ACF
			//rs = con.queryDB(qrySentencia);

				pdfDoc.setLTable(11,100);
				pdfDoc.setLCell("A", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre EPO", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("PYME", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("IF", "celda01",ComunesPDF.CENTER);
				//pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("FIDEICOMISO", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Num. Documento", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha emisi�n", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha vencimiento", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo Factoraje", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("B", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Porcentaje Descuento", "celda01",ComunesPDF.CENTER);

				pdfDoc.setLCell("Recurso Garantia", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Descontar", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus", "celda01",ComunesPDF.CENTER);

				pdfDoc.setLCell("Beneficiario", "celda01",ComunesPDF.CENTER);

				pdfDoc.setLCell("% Beneficiario", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Importe a Recibir Beneficiario", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha para Efectuar Descuento", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Alta Docto.", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Notificado como Duplicado", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("C", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Digito Identificador", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Recepci�n de Bienes y Servicios", "celda01",ComunesPDF.CENTER);

				pdfDoc.setLCell("Tipo de Compra (procedimiento)", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Clasificador por Objeto del Gasto", "celda01",ComunesPDF.CENTER);

				pdfDoc.setLCell("Plazo M�ximo", "celda01",ComunesPDF.CENTER);
				cont=0;
				int  columnaVacia1 =2;
				if (!"".equals(ic_epo) ){					
					columnaVacia1 +=5;
					for(int i = 0; i < nombresCamposAdicionales.size(); i++){
						cont++;
						pdfDoc.setLCell(nombresCamposAdicionales.get(i).toString(), "celda01",ComunesPDF.CENTER);
					}				
					pdfDoc.setLCell("D", "celda01",ComunesPDF.CENTER);
					columnaVacia1 +=5;
				}
				 
				 if ("true".equals(mandanteDoc) ){
					pdfDoc.setLCell("Mandante", "celda01",ComunesPDF.CENTER);
					cont++;
				}
				 
				if ("false".equals(mandanteDoc) ) { 
				    columnaVacia1 +=1; 
				}
							
				pdfDoc.setLCell("Validaci�n de Documento EPO", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Contrato", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("COPADE", "celda01",ComunesPDF.CENTER);
								
				for(int i = cont; i <columnaVacia1 ; i++){
					pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setLHeaders();

				while(rs.next()){
					NOM_EPO              = (rs.getString(1)==null)?"":rs.getString(1);
					NOM_PYME             = (rs.getString(2) == null)?"":rs.getString(2);
					IG_NUMERO_DOCTO      = (rs.getString(3)==null)?"":rs.getString(3);
					DF_FECHA_DOCTO       = (rs.getString(4)==null)?"":rs.getString(4);
					DF_FECHA_VENC        = (rs.getString(5)==null)?"":rs.getString(5);
					NOM_MONEDA           = (rs.getString(6)==null)?"":rs.getString(6);
					IC_MONEDA            = Integer.parseInt((rs.getString(11)==null)?"0":rs.getString(11)); 
					FN_MONTO             = (rs.getString(7)==null)?"0.00":rs.getString(7);
					dblPorcentaje        = Double.parseDouble((rs.getString(12)==null)?"0.00":rs.getString(12));
					strEstatus           = (rs.getString(13)==null)?"":rs.getString(13);
					FN_MONTO_DSCTO       = (rs.getString(9)==null)?"0.00":rs.getString(9);
					ESTATUS_DOCTO_NOMBRE = (rs.getString(10)==null)?"":rs.getString(10);
					IC_DOCUMENTO         = (rs.getString(14)==null)?"":rs.getString(14);
					CS_CAMBIO_IMPORTE    = (rs.getString(15)==null)?"":rs.getString(15);
					NOM_IF               = (rs.getString(16)==null)?"":rs.getString(16).trim();
					RFC                  = (rs.getString(23)==null)?"":rs.getString(23).trim();
					DF_ALTA              = (rs.getString("DF_ALTA")==null)?"":rs.getString("DF_ALTA").trim();
					IC_EPO               = (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO").trim();
					CC_ACUSE             = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE").trim();
					NombreMandante       = (rs.getString("NOMBREMANDANTE")==null)?"":rs.getString("NOMBREMANDANTE").trim();
					fideicomiso          = (rs.getString("FIDEICOMISO")==null)?"":rs.getString("FIDEICOMISO").trim();
					duplicado            = (rs.getString("DUPLICADO")==null)?"":rs.getString("DUPLICADO").trim();
					//si hay bandera de documentos se recuperan los valores
					//       if(banderaTablaDoctos.equals("1")){
					FECHA_ENTREGA        = (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
					TIPO_COMPRA          = (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
					CLAVE_PRESUPUESTARIA = (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
					PERIODO              = (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");
					//       }
					validacion          = (rs.getString("VALIDACION_JUR")          ==null)?""    :rs.getString("VALIDACION_JUR");
					contratoArch          = (rs.getString("cg_num_contrato")          ==null)?""    :rs.getString("cg_num_contrato");
					copadeArch          = (rs.getString("cg_num_copade")          ==null)?""    :rs.getString("cg_num_copade");
						
					if (datos.containsKey(IC_EPO)) {
						siaff = ((Boolean)datos.get(IC_EPO)).booleanValue();
					}else {
						siaff = BeanParamDscto.estaHabilitadoNumeroSIAFF(IC_EPO);
						datos.put(IC_EPO,new Boolean(siaff));
					}
					if(siaff){
						numeroSIAFF = getNumeroSIAFF(IC_EPO,IC_DOCUMENTO);                               
					}else{
						numeroSIAFF = "";
					}

					beneficiario = (rs.getString(18)==null)?"":rs.getString(18).trim();
					porciento_beneficiario = Double.parseDouble((rs.getString(19)==null)?"0.00":rs.getString(19));
					importe_a_recibir_beneficiario = Double.parseDouble((rs.getString(20)==null)?"0.00":rs.getString(20));

					if(strEstatus.equals("2") || strEstatus.equals("5") || strEstatus.equals("6") ||
						strEstatus.equals("7") || strEstatus.equals("9") || strEstatus.equals("10")){
						dblPorcentaje = Double.parseDouble(rs.getString(8));
						FN_MONTO_DSCTO = new Double(new Double(FN_MONTO).doubleValue() * (dblPorcentaje / 100)).toString();
					}

					tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 
					nombreTipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
					if(nombreTipoFactoraje.equals("V")) {
						FN_MONTO_DSCTO = FN_MONTO;
						dblPorcentaje = 100;
					}

				//       nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal":(nombreTipoFactoraje.equals("V"))?"Vencido":(nombreTipoFactoraje.equals("D"))?"Distribuido":"";
					tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 

					dblRecurso = new Double(FN_MONTO).doubleValue() - new Double(FN_MONTO_DSCTO).doubleValue();
					if(IC_MONEDA == 54) {//D�lar
						montoTotalDL = montoTotalDL.add(new BigDecimal(FN_MONTO));
						montoTotalDsctoDL = montoTotalDsctoDL.add(new BigDecimal(FN_MONTO_DSCTO));
						numRegistrosDL++;
					} else if (IC_MONEDA == 1) {
						montoTotalMN = montoTotalMN.add(new BigDecimal(FN_MONTO));
						montoTotalDsctoMN = montoTotalDsctoMN.add(new BigDecimal(FN_MONTO_DSCTO));
						numRegistrosMN++;
					}
					registros++;
					/*GENERAMOS EL ARCHIVO*/

					pdfDoc.setLCell("A", "celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(NOM_EPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(NOM_PYME,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(NOM_IF,"formas",ComunesPDF.CENTER);
					//pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fideicomiso,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(IG_NUMERO_DOCTO,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(DF_FECHA_DOCTO,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(DF_FECHA_VENC,"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell(NOM_MONEDA,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoFactorajeDesc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(FN_MONTO,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("B", "celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(dblPorcentaje,2,false)+"%","formas",ComunesPDF.CENTER);

					pdfDoc.setLCell("$"+Comunes.formatoDecimal(dblRecurso,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(FN_MONTO_DSCTO,2),"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell(ESTATUS_DOCTO_NOMBRE,"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.CENTER); 

					pdfDoc.setLCell(porciento_beneficiario!=0?Comunes.formatoDecimal(porciento_beneficiario,0,false)+"%":"","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false),"formas",ComunesPDF.CENTER); 

					pdfDoc.setLCell(df_programacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(DF_ALTA,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(duplicado,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("C", "celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(numeroSIAFF,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(FECHA_ENTREGA,"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell(TIPO_COMPRA,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(CLAVE_PRESUPUESTARIA,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(PERIODO.equals("0")?"":PERIODO,"formas",ComunesPDF.CENTER);

				//============================================================================>> FODEA 002 - 2009 (I)
					//cont=0;
					if(!ic_epo.equals("")){
						List datosCamposAdicionales = new ArrayList();
						if(!ic_epo.equals("")){datosCamposAdicionales = BeanParamDscto.getDatosCamposAdicionales(IC_DOCUMENTO, ic_epo, numeroCamposAdicionales);}
						for(int i = 0; i < numeroCamposAdicionales; i++){
							pdfDoc.setLCell(datosCamposAdicionales.get(i).toString(),"formas",ComunesPDF.CENTER);
							//cont++;
						}
					}
					if (!"".equals(ic_epo) ){
						pdfDoc.setLCell("D", "celda01",ComunesPDF.CENTER);
					    }
					if ("true".equals(mandanteDoc)  ) {
					    pdfDoc.setLCell(NombreMandante,"formas",ComunesPDF.CENTER);
					    
					}
				    
					pdfDoc.setLCell(validacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(contratoArch,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(copadeArch,"formas",ComunesPDF.CENTER);
					
					
					for(int i = cont; i <columnaVacia1 ; i++){
						pdfDoc.setLCell("", "formas",ComunesPDF.CENTER);
					}
					//============================================================================>> FODEA 002 - 2009 (F)

						//contenidoArchivo = contenidoArchivo+linea;
						//System.out.println(registros);
				} // while

			} //if estatus="",2,5,6,7,9,10

			/*       Seleccionada Pyme             Operada                 Operada Pagada          Operada Pendiente de Pago        Aplicado a Credito            En Proceso de Autorizaci�n IF    Programado Pyme*/
			if(ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12") || ic_estatus_docto.equals("16") || ic_estatus_docto.equals("24") || ic_estatus_docto.equals("26")){//FODEA 005 - 2009 ACF
			//rs = con.queryDB(qrySentencia);
			System.out.println("***************************************************************************************");
			pdfDoc.setLTable(12,100);
			pdfDoc.setLCell("A", "celda01",ComunesPDF.CENTER);
		    pdfDoc.setLCell("Nombre EPO", "celda01",ComunesPDF.CENTER);
		    pdfDoc.setLCell("PYME", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("IF", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("FIDEICOMISO", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Num. Documento", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Num. Acuse", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Fecha emisi�n", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Fecha vencimiento", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Plazo", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Moneda", "celda01",ComunesPDF.CENTER);
		     
		      pdfDoc.setLCell("Tipo Factoraje", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("B", "celda01",ComunesPDF.CENTER); 
		    
		     pdfDoc.setLCell("Monto Documento", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Porcentaje descuento", "celda01",ComunesPDF.CENTER);
		    
		     pdfDoc.setLCell("Recurso Garantia", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Monto Descontar", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Intereses del documento", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Monto a recibir", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Tasa", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Estatus", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Importe aplicado a credito", "celda01",ComunesPDF.CENTER);
		     
		     pdfDoc.setLCell("Porcentaje del documento aplicado", "celda01",ComunesPDF.CENTER);
		     
		     
		     
		     pdfDoc.setLCell("Importe a depositar a la Pyme.", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("C", "celda01",ComunesPDF.CENTER);
		     
		     
		   
		   //============================================================================>> FODEA 002 - 2009 (I)
		   if (ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12")){
		      pdfDoc.setLCell("Fecha Aut. IF", "celda01",ComunesPDF.CENTER);
		   }
		     pdfDoc.setLCell("Neto a Recibir", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Doctos Aplicados a Nota de Credito", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Beneficiario", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("% Beneficiario", "celda01",ComunesPDF.CENTER);
		     
		     pdfDoc.setLCell("Importe a Recibir Beneficiario", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Fecha Alta Docto.", "celda01",ComunesPDF.CENTER);
		     pdfDoc.setLCell("Digito Identificador", "celda01",ComunesPDF.CENTER);
		     
		   if (!(ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12"))) {
		         pdfDoc.setLCell("Fecha de Recepci�n de Bienes y Servicios", "celda01",ComunesPDF.CENTER);
		         
		         pdfDoc.setLCell("Tipo de Compra (procedimiento)", "celda01",ComunesPDF.CENTER);
		         pdfDoc.setLCell("Clasificador por Objeto del Gasto", "celda01",ComunesPDF.CENTER);
		         pdfDoc.setLCell("Plazo M�ximo", "celda01",ComunesPDF.CENTER);
		         pdfDoc.setLCell("D", "celda01",ComunesPDF.CENTER);
		      }else{
		         
		         pdfDoc.setLCell("Fecha de Recepci�n de Bienes y Servicios", "celda01",ComunesPDF.CENTER);
		         pdfDoc.setLCell("Clasificador por Objeto del Gasto", "celda01",ComunesPDF.CENTER);
		         pdfDoc.setLCell("Plazo M�ximo", "celda01",ComunesPDF.CENTER);
		         pdfDoc.setLCell("D", "celda01",ComunesPDF.CENTER);
		         pdfDoc.setLCell("Tipo de Compra (procedimiento)", "celda01",ComunesPDF.CENTER);
		      }
		     
		     
		    
		     int columnaVacia=4;
		     cont=0;
		     for(int i = 0; i < nombresCamposAdicionales.size(); i++){
		      pdfDoc.setLCell(nombresCamposAdicionales.get(i).toString(), "celda01",ComunesPDF.CENTER);
		      cont++;
		     }
		     if ("true".equals(mandanteDoc) )
		     pdfDoc.setLCell("Mandante","celda01",ComunesPDF.CENTER);
		    if ("false".equals(mandanteDoc) ){ 
				columnaVacia +=1;
		    }
		     
		     pdfDoc.setLCell("Validaci�n de Documento EPO","celda01",ComunesPDF.CENTER);
		     		    
		   if(ic_estatus_docto.equals("26")){
				pdfDoc.setLCell("Fecha Registro Operaci�n", "celda01",ComunesPDF.CENTER);
			}else  {
				columnaVacia +=1;
			}//FODEA 005 - 2009 ACF
		  
		     if (!(ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12"))) {
		      columnaVacia +=1;
		      }
		      	  
				pdfDoc.setLCell("Contrato", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("COPADE", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
				
			  for(int i = cont; i <columnaVacia ; i++){
		         pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
		     }
		   //============================================================================>> FODEA 002 - 2009 (F)
		   String ig_tipo_piso="",fn_remanente="";
			pdfDoc.setLHeaders();
		   while (rs.next()){
		      NOM_EPO = (rs.getString("NOMBREEPO") == null) ? "" : rs.getString("NOMBREEPO");
		      NOM_PYME = (rs.getString("NOMBREPYME") == null) ? "" : rs.getString("NOMBREPYME");
		      NOM_IF = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
		      IG_NUMERO_DOCTO = (rs.getString("IG_NUMERO_DOCTO") == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
		      cc_acuse = rs.getString("CC_ACUSE");
		      DF_FECHA_DOCTO = (rs.getString("DF_FECHA_DOCTO") == null) ? "" : rs.getString("DF_FECHA_DOCTO");
		      DF_FECHA_VENC = (rs.getString("DF_FECHA_VENC") == null) ? "" : rs.getString("DF_FECHA_VENC");
		      plazo = (rs.getString("IG_PLAZO") == null) ? "" : rs.getString("IG_PLAZO");
		      NOM_MONEDA = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
		      IC_MONEDA = Integer.parseInt((rs.getString("IC_MONEDA")==null)?"0":rs.getString("IC_MONEDA"));
		      FN_MONTO = (rs.getString("FN_MONTO") == null) ? "0.00" : rs.getString("FN_MONTO");
		      dblPorcentaje = Double.parseDouble((rs.getString("FN_PORC_ANTICIPO")==null)?"0.00":rs.getString("FN_PORC_ANTICIPO"));
		      FN_MONTO_DSCTO = (rs.getString("FN_MONTO_DSCTO")==null)?"0.00":rs.getString("FN_MONTO_DSCTO");
		      importe_interes = Double.parseDouble((rs.getString("IN_IMPORTE_INTERES")==null)?"0.00":rs.getString("IN_IMPORTE_INTERES"));
		      importe_recibir = Double.parseDouble((rs.getString("IN_IMPORTE_RECIBIR")==null)?"0.00":rs.getString("IN_IMPORTE_RECIBIR"));
		      IN_TASA_ACEPTADA = (rs.getString("IN_TASA_ACEPTADA") == null) ? "" : rs.getString("IN_TASA_ACEPTADA");
		      ESTATUS_DOCTO_NOMBRE = (rs.getString("CD_DESCRIPCION") == null) ? "" : rs.getString("CD_DESCRIPCION");
		      FECHA_SOLICITUD = (rs.getString("DF_FECHA_SOLICITUD") == null) ? "" : rs.getString("DF_FECHA_SOLICITUD");
		      IC_DOCUMENTO = (rs.getString("IC_DOCUMENTO") == null) ? "" : rs.getString("IC_DOCUMENTO");
		      CS_CAMBIO_IMPORTE = (rs.getString("CS_CAMBIO_IMPORTE") == null) ? "" : rs.getString("CS_CAMBIO_IMPORTE");
		      porc_docto_aplicado  = (rs.getString("PORCDOCTOAPLICADO") == null) ? "" : rs.getString("porcDoctoAplicado");
		      montoPago = Double.parseDouble((rs.getString("FN_MONTO_PAGO")==null)?"0.00":rs.getString("FN_MONTO_PAGO"));
		      beneficiario = (rs.getString("BENEFICIARIO")==null)?"":rs.getString("BENEFICIARIO").trim();
		      porciento_beneficiario = Double.parseDouble((rs.getString("FN_PORC_BENEFICIARIO")==null)?"0":rs.getString("FN_PORC_BENEFICIARIO"));
		      importe_a_recibir_beneficiario = Double.parseDouble((rs.getString("RECIBIR_BENEF")==null)?"0.00":rs.getString("RECIBIR_BENEF"));
		      neto_a_recibir_pyme =  Double.parseDouble((rs.getString("NETO_REC_PYME")==null)?"0.00":rs.getString("NETO_REC_PYME"));
		      DF_ALTA = (rs.getString("DF_ALTA")==null)?"":rs.getString("DF_ALTA").trim();
		      //RFC = (rs.getString("RFC")==null)?"":rs.getString("RFC").trim();
		      ESTATUS_DOCTO = (rs.getString("ic_estatus_docto")==null)?"":rs.getString("ic_estatus_docto");
		      ig_tipo_piso = (rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO");
		      fn_remanente = (rs.getString("FN_REMANENTE")==null)?"":rs.getString("FN_REMANENTE");
		      // Factoraje Vencido
		      tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 
		      nombreTipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
		      IC_EPO      = (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO").trim();
		      fecha_registro_operacion = rs.getString("FECHA_REGISTRO_OPERACION")==null?"":rs.getString("FECHA_REGISTRO_OPERACION");//FODEA 005 - 2009 ACF
		      ///*******2
		      fideicomiso = (rs.getString("FIDEICOMISO")==null)?"":rs.getString("FIDEICOMISO").trim();
		         if (datos.containsKey(IC_EPO)) {           
		             siaff = ((Boolean)datos.get(IC_EPO)).booleanValue();
		         
		         }else {  
		                           
		            siaff = BeanParamDscto.estaHabilitadoNumeroSIAFF(IC_EPO);
		            datos.put(IC_EPO,new Boolean(siaff));
		         }
		         
		         if(siaff){
		            numeroSIAFF = getNumeroSIAFF(IC_EPO,IC_DOCUMENTO);                
		         }else{
		            numeroSIAFF = "";
		         }
		         


		      //si hay bandera de documentos se recuperan los valores
		   //       if(banderaTablaDoctos.equals("1")){
		         FECHA_ENTREGA              = (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
		         TIPO_COMPRA                   = (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
		         CLAVE_PRESUPUESTARIA       = (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
		         PERIODO                       = (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");
		   //       }


		   //       nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal":(nombreTipoFactoraje.equals("V"))?"Vencido":(nombreTipoFactoraje.equals("D"))?"Distribuido":"";
		      tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE"); 
		      validacion          = (rs.getString("VALIDACION_JUR")          ==null)?""    :rs.getString("VALIDACION_JUR");
				contratoArch          = (rs.getString("cg_num_contrato")          ==null)?""    :rs.getString("cg_num_contrato");
				copadeArch          = (rs.getString("cg_num_copade")          ==null)?""    :rs.getString("cg_num_copade");
						
		      
		      dblRecurso = new Double(FN_MONTO).doubleValue() - new Double(FN_MONTO_DSCTO).doubleValue();
		      importe_depositar_pyme = importe_recibir - montoPago;


		      //esNotaDeCreditoSimpleAplicada = "true".equals(reg.getString("existeDocumentoAsociado"))?true:false;
		      //esDocumentoConNotaDeCreditoSimpleAplicada = "true".equals(reg.getString("esDocConNotaDeCredSimpleApl"))?true:false;
		         
		      //esNotaDeCreditoAplicada = "true".equals(reg.getString("existeRefEnComrelNotaDocto"))?true:false;
		      //esDocumentoConNotaDeCreditoMultipleAplicada   = "true".equals(reg.getString("esDocConNotaDeCredMultApl"))?true:false;
		      esNotaDeCreditoSimpleAplicada                = ( nombreTipoFactoraje.equals("C") && (ESTATUS_DOCTO.equals("3") || ESTATUS_DOCTO.equals("4") || ESTATUS_DOCTO.equals("11")) && BeanParamDscto.existeDocumentoAsociado(String.valueOf(IC_DOCUMENTO)) )?true:false;
		      esDocumentoConNotaDeCreditoSimpleAplicada    = ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(String.valueOf(IC_DOCUMENTO)))?true:false;
		   
		      esNotaDeCreditoAplicada                      = ( nombreTipoFactoraje.equals("C") && (ESTATUS_DOCTO.equals("3") || ESTATUS_DOCTO.equals("4") || ESTATUS_DOCTO.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(String.valueOf(IC_DOCUMENTO)) )?true:false;
		      esDocumentoConNotaDeCreditoMultipleAplicada  = ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(String.valueOf(IC_DOCUMENTO)))?true:false;
		   
		      
		      if(esNotaDeCreditoSimpleAplicada){
		         numeroDocumento                           = getNumeroDoctoAsociado(String.valueOf(IC_DOCUMENTO)); 
		      }else if(esDocumentoConNotaDeCreditoSimpleAplicada){
		         numeroDocumento                           = getNumeroNotaCreditoAsociada(String.valueOf(IC_DOCUMENTO));
		      }
		      
		      registros++;
		      /*GENERAMOS EL ARCHIVO*/
		   
		            pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
		            pdfDoc.setLCell(NOM_EPO,"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(NOM_PYME,"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(NOM_IF,"formas",ComunesPDF.CENTER);
		            //pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(fideicomiso,"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(IG_NUMERO_DOCTO,"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(cc_acuse,"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(DF_FECHA_DOCTO,"formas",ComunesPDF.RIGHT);
		            pdfDoc.setLCell(DF_FECHA_VENC,"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(NOM_MONEDA,"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(tipoFactorajeDesc,"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
		            
		            
		            pdfDoc.setLCell("$"+Comunes.formatoDecimal(FN_MONTO,2),"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(Comunes.formatoDecimal(dblPorcentaje,2,false)+"%","formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell("$"+Comunes.formatoDecimal(dblRecurso,2),"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell("$"+Comunes.formatoDecimal(FN_MONTO_DSCTO,2),"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_interes,2,true),"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_recibir,2,true),"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(Comunes.formatoDecimal(IN_TASA_ACEPTADA,4)+"%","formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(ESTATUS_DOCTO_NOMBRE,"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoPago,2,true),"formas",ComunesPDF.CENTER);
		            pdfDoc.setLCell(Comunes.formatoDecimal(porc_docto_aplicado,2,false)+"%","formas",ComunesPDF.CENTER);
		            
		            
		   

		      if(ic_estatus_docto.equals("16") ){
		         if(ig_tipo_piso.equals("1")){
		            if(!fn_remanente.equals("")){
		               pdfDoc.setLCell("$"+Comunes.formatoDecimal(Double.parseDouble(fn_remanente),2,true),"formas",ComunesPDF.CENTER);
		            }else{
		               pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
		            }
		         }else{
		         pdfDoc.setLCell("$"+Comunes.formatoDecimal(Double.parseDouble(fn_remanente),2,true),"formas",ComunesPDF.CENTER);
		         }
		      }else{
		         pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_depositar_pyme,2,true),"formas",ComunesPDF.CENTER);
		      }
		      
		      pdfDoc.setLCell("C","celda01",ComunesPDF.CENTER);
		      
		      if (ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12")) {
		         pdfDoc.setLCell(FECHA_SOLICITUD,"formas",ComunesPDF.CENTER);
		      }
		      pdfDoc.setLCell("$"+Comunes.formatoDecimal(neto_a_recibir_pyme,2,true),"formas",ComunesPDF.CENTER);
		      
		      // Fodea 002 - 2010.
		   
		      if(esNotaDeCreditoAplicada || esDocumentoConNotaDeCreditoMultipleAplicada){
		         pdfDoc.setLCell("Si","formas",ComunesPDF.CENTER);
		      }else
		      if(esNotaDeCreditoSimpleAplicada || esDocumentoConNotaDeCreditoSimpleAplicada){
		      pdfDoc.setLCell("\""+numeroDocumento+"\"","formas",ComunesPDF.CENTER);
		      }else{
		         pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
		      }
		      
		      if(!"".equals(beneficiario)) pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.CENTER); else pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
		      if(0 != porciento_beneficiario) pdfDoc.setLCell(Comunes.formatoDecimal(porciento_beneficiario,2,false)+"%","formas",ComunesPDF.CENTER);else pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
		      if(0 != importe_a_recibir_beneficiario) pdfDoc.setLCell(Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,true),"formas",ComunesPDF.CENTER);else pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
		      
		      
		      pdfDoc.setLCell(DF_ALTA,"formas",ComunesPDF.CENTER);
		      pdfDoc.setLCell(numeroSIAFF,"formas",ComunesPDF.CENTER);
		      pdfDoc.setLCell(FECHA_ENTREGA,"formas",ComunesPDF.CENTER);
		      if (!(ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12"))) {
		         pdfDoc.setLCell(TIPO_COMPRA,"formas",ComunesPDF.CENTER);
		         pdfDoc.setLCell(CLAVE_PRESUPUESTARIA,"formas",ComunesPDF.CENTER);
		         pdfDoc.setLCell(PERIODO.equals("0")?"":PERIODO,"formas",ComunesPDF.CENTER);
		         pdfDoc.setLCell("D","celda01",ComunesPDF.CENTER);
		   
		      }else{
		         pdfDoc.setLCell(CLAVE_PRESUPUESTARIA,"formas",ComunesPDF.CENTER);
		         pdfDoc.setLCell(PERIODO.equals("0")?"":PERIODO,"formas",ComunesPDF.CENTER);
		         pdfDoc.setLCell("D","celda01",ComunesPDF.CENTER);
		         pdfDoc.setLCell(TIPO_COMPRA,"formas",ComunesPDF.CENTER);
		         
		      }
		      
		      

		      //si hay bandera de documentos se recuperan los valores
		   //       if(banderaTablaDoctos.equals("1")){
		         
		         
		   //       }
		      cont=0;
		   //============================================================================>> FODEA 002 - 2009 (I)
		      if(!ic_epo.equals("")){
		         List datosCamposAdicionales = new ArrayList();
		         if(!ic_epo.equals("")){datosCamposAdicionales = BeanParamDscto.getDatosCamposAdicionales(IC_DOCUMENTO, ic_epo, numeroCamposAdicionales);}
		         for(int i = 0; i < numeroCamposAdicionales; i++){
		            cont++;
		            pdfDoc.setLCell(datosCamposAdicionales.get(i).toString(),"formas",ComunesPDF.CENTER);
		         }
		      }
		      if(mandanteDoc.equals("true"))
		     pdfDoc.setLCell(NombreMandante,"formas",ComunesPDF.CENTER);
		     
		     pdfDoc.setLCell(validacion, "formas",ComunesPDF.CENTER); 
		       
		      if(ic_estatus_docto.equals("26")){
					pdfDoc.setLCell(fecha_registro_operacion,"formas",ComunesPDF.CENTER); 
				}//FODEA 005 - 2009 ACF
		      		     
		      pdfDoc.setLCell(contratoArch, "formas",ComunesPDF.CENTER);
		      pdfDoc.setLCell(copadeArch, "formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("", "formas",ComunesPDF.CENTER);
				
			  for(int i = cont; i <columnaVacia ; i++){
		         pdfDoc.setLCell("", "formas",ComunesPDF.CENTER);
		     }
					
		   //============================================================================>> FODEA 002 - 2009 (F)
		      //contenidoArchivo = contenidoArchivo+linea;
		   } // while
		   } //if estatus=3,4,11,12


		     
		    /* while (reg.next()) {
		         String razonEpo = (reg.getString("RAZON_SOCIAL_EPO") == null) ? "" : reg.getString("RAZON_SOCIAL_EPO");
		         String razonPyme = (reg.getString("RAZON_SOCIAL_PYME") == null) ? "" : reg.getString("RAZON_SOCIAL_PYME");
		         String rfc = (reg.getString("RFC") == null) ? "" : reg.getString("RFC");
		         String domicilio = (reg.getString("DOMICILIO") == null) ? "" : reg.getString("DOMICILIO");
		         String contacto = (reg.getString("CONTACTO") == null) ? "" : reg.getString("CONTACTO");
		         String telefono = (reg.getString("TELEFONO") == null) ? "" : reg.getString("TELEFONO");
		         String fecha = (reg.getString("FECHA") == null) ? "" : reg.getString("FECHA");
		         
		         pdfDoc.setLCell(razonEpo,"formas",ComunesPDF.CENTER);
		         pdfDoc.setLCell(razonPyme,"formas",ComunesPDF.CENTER);
		         pdfDoc.setLCell(rfc,"formas",ComunesPDF.CENTER);
		         pdfDoc.setLCell(domicilio,"formas",ComunesPDF.CENTER);
		         pdfDoc.setLCell(contacto,"formas",ComunesPDF.CENTER);
		         pdfDoc.setLCell(telefono,"formas",ComunesPDF.CENTER);
		         pdfDoc.setLCell(fecha,"formas",ComunesPDF.CENTER);
		      }*/
		     pdfDoc.addLTable();
		     pdfDoc.endDocument();
		   
		   
		   } catch (Throwable e) {
				throw new AppException("Error al generar el archivo",e);
			}



		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}								
											
											
											
											
											
											
											
		
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, 
												 String path, String tipo) {
		HttpSession session = request.getSession();
		String nombreArchivo = "";
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		  ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
		int cont=0;
		
			
		String NOM_EPO="", NOM_PYME="", NOM_IF="", IG_NUMERO_DOCTO="", DF_FECHA_DOCTO="", DF_FECHA_VENC="", RFC="";
		String NOM_MONEDA="", FN_MONTO="", FN_MONTO_DSCTO="", IN_TASA_ACEPTADA="", ESTATUS_DOCTO="", duplicado = "";
		String DF_ALTA="";
		String IC_EPO="";
		String CC_ACUSE="";
		double dblPorcentaje = 0, dblRecurso = 0; int IC_MONEDA;
		double montoPago = 0;
		String FECHA_SOLICITUD="", strEstatus = "", IC_DOCUMENTO = "", CS_CAMBIO_IMPORTE="", Parametros = "";
		String nombreTipoFactoraje="";
		String tipoFactorajeDesc ="";
		String tasa = "";
		String plazo = "";
		String porc_docto_aplicado = "";
		String NombreMandante = "";
		String beneficiario = "";
		double importe_recibir = 0;
		double importe_depositar_pyme = 0;
		double importe_interes = 0;
		String ESTATUS_DOCTO_NOMBRE = "";
		int porciento_beneficiario = 0;
		double importe_a_recibir_beneficiario = 0;
		double neto_a_recibir_pyme = 0;
		String cc_acuse="";
		String numeroPedido = "";
		String nomArchivo = "";
		String FECHA_ENTREGA = "", TIPO_COMPRA = "", CLAVE_PRESUPUESTARIA = "", PERIODO = "";
		
		HashMap datos = new HashMap();
		boolean siaff = false;
		String numeroSIAFF   = (request.getParameter("digito")==null)?"":request.getParameter("digito");

		// Fodea 002 - 2010
		boolean esNotaDeCreditoAplicada                 = false;
		boolean esDocumentoConNotaDeCreditoMultipleAplicada   = false;
		
		boolean esNotaDeCreditoSimpleAplicada                 = false;
		boolean esDocumentoConNotaDeCreditoSimpleAplicada     = false;
		String  numeroDocumento                               = "";
		String fideicomiso = null;
		String fecha_registro_operacion = "";//FODEA 005 - 2009 ACF
		ParametrosDescuento     BeanParamDscto=null;
		try{
			BeanParamDscto          = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		}catch(Exception e){}
		
		List nombresCamposAdicionales = new ArrayList();
		int numeroCamposAdicionales = 0;
		String strCamposAdicionales = "";
		
		try {
			if(!ic_epo.equals("")){numeroCamposAdicionales = BeanParamDscto.getNumeroCamposAdicionales(ic_epo);}
			if(!ic_epo.equals("")){nombresCamposAdicionales = BeanParamDscto.getNombresCamposAdicionales(ic_epo);}
			String tiposFactorajes="";
			if(!ic_epo.equals(""))
				tiposFactorajes+=BeanParamDscto.getParametrosFactoraje(ic_epo);
			boolean banderaMandate=tiposFactorajes.indexOf("M")>0?true:false;
			if(ic_epo.equals(""))
				banderaMandate=true;
			for(int i = 0; i < nombresCamposAdicionales.size(); i++){strCamposAdicionales += "," + nombresCamposAdicionales.get(i);}
			BigDecimal montoTotalMN = new BigDecimal("0.00");
			BigDecimal montoTotalDL = new BigDecimal("0.00");
			BigDecimal montoTotalDsctoMN = new BigDecimal("0.00");
			BigDecimal montoTotalDsctoDL = new BigDecimal("0.00");
			int registros = 0, numRegistros = 0, numRegistrosMN = 0, numRegistrosDL = 0;

		  String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		  String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		  String diaActual = fechaActual.substring(0,2);
		  String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		  String anioActual = fechaActual.substring(6,10);
		  String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
		  pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		  "",
		  (String)session.getAttribute("sesExterno"),
		  (String)session.getAttribute("strNombre"),
		  (String)session.getAttribute("strNombreUsuario"),
		  (String)session.getAttribute("strLogo"),
		  (String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
		  pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
		 
		if(ic_estatus_docto.equals("") || ic_estatus_docto.equals("1") || ic_estatus_docto.equals("2") || ic_estatus_docto.equals("5") || ic_estatus_docto.equals("6") || ic_estatus_docto.equals("7") || ic_estatus_docto.equals("9") || ic_estatus_docto.equals("10") || ic_estatus_docto.equals("21")|| ic_estatus_docto.equals("23")|| ic_estatus_docto.equals("28") || ic_estatus_docto.equals("29")|| ic_estatus_docto.equals("30")|| ic_estatus_docto.equals("31")|| ic_estatus_docto.equals("33")) {//FODEA 005 - 2009 ACF
		//rs = con.queryDB(qrySentencia);
		
		  pdfDoc.setLTable(11,100);
		  pdfDoc.setLCell("A", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Nombre EPO", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("PYME", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("IF", "celda01",ComunesPDF.CENTER);
		  //pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("FIDEICOMISO", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Num. Documento", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Fecha emisi�n", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Fecha vencimiento", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Moneda", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Tipo Factoraje", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Monto", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("B", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Porcentaje Descuento", "celda01",ComunesPDF.CENTER);
		  
		  
		  
		  pdfDoc.setLCell("Recurso Garantia", "celda01",ComunesPDF.CENTER);
		 pdfDoc.setLCell("Monto Descontar", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Estatus", "celda01",ComunesPDF.CENTER);
		 
		  
		  pdfDoc.setLCell("Beneficiario", "celda01",ComunesPDF.CENTER);
		  
		pdfDoc.setLCell("% Beneficiario", "celda01",ComunesPDF.CENTER);
		pdfDoc.setLCell("Importe a Recibir Beneficiario", "celda01",ComunesPDF.CENTER);
		pdfDoc.setLCell("Fecha para Efectuar Descuento", "celda01",ComunesPDF.CENTER);
		pdfDoc.setLCell("Fecha Alta Docto.", "celda01",ComunesPDF.CENTER);
		pdfDoc.setLCell("Notificado como Duplicado", "celda01",ComunesPDF.CENTER);
		pdfDoc.setLCell("C", "celda01",ComunesPDF.CENTER);
		pdfDoc.setLCell("Digito Identificador", "celda01",ComunesPDF.CENTER);
		pdfDoc.setLCell("Fecha de Recepci�n de Bienes y Servicios", "celda01",ComunesPDF.CENTER);
		 
		  
		  
		  pdfDoc.setLCell("Tipo de Compra (procedimiento)", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Clasificador por Objeto del Gasto", "celda01",ComunesPDF.CENTER);
		  
		  pdfDoc.setLCell("Plazo M�ximo", "celda01",ComunesPDF.CENTER);
		 cont=0;
		  for(int i = 0; i < nombresCamposAdicionales.size(); i++){
			cont++;
			pdfDoc.setLCell(nombresCamposAdicionales.get(i).toString(), "celda01",ComunesPDF.CENTER);
		  }
		  if(mandanteDoc.equals("true"))
		  pdfDoc.setLCell("Mandante", "celda01",ComunesPDF.CENTER);
		  else
		  pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
			for(int i = cont; i <4 ; i++){
			pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
		  }
	   pdfDoc.setLHeaders();
			
		String df_programacion = "";
		while(reg.next()){
			NOM_EPO = (reg.getString(1)==null)?"":reg.getString(1);
			NOM_PYME = (reg.getString(2) == null)?"":reg.getString(2);
			IG_NUMERO_DOCTO = (reg.getString(3)==null)?"":reg.getString(3);
			DF_FECHA_DOCTO = (reg.getString(4)==null)?"":reg.getString(4);
			DF_FECHA_VENC = (reg.getString(5)==null)?"":reg.getString(5);
			NOM_MONEDA = (reg.getString(6)==null)?"":reg.getString(6);
			IC_MONEDA = Integer.parseInt(reg.getString(11));
			FN_MONTO = (reg.getString(7)==null)?"0.00":reg.getString(7);
			dblPorcentaje = Double.parseDouble(reg.getString(12));
			strEstatus = (reg.getString(13)==null)?"":reg.getString(13);
			FN_MONTO_DSCTO = (reg.getString(9)==null)?"0.00":reg.getString(9);
			ESTATUS_DOCTO_NOMBRE = (reg.getString(10)==null)?"":reg.getString(10);
			IC_DOCUMENTO = (reg.getString(14)==null)?"":reg.getString(14);
			CS_CAMBIO_IMPORTE = (reg.getString(15)==null)?"":reg.getString(15);
			NOM_IF = (reg.getString(16)==null)?"":reg.getString(16).trim();
			RFC = (reg.getString(23)==null)?"":reg.getString(23).trim();
			DF_ALTA     = (reg.getString("DF_ALTA")==null)?"":reg.getString("DF_ALTA").trim();
			IC_EPO      = (reg.getString("IC_EPO")==null)?"":reg.getString("IC_EPO").trim();
			CC_ACUSE    = (reg.getString("CC_ACUSE")==null)?"":reg.getString("CC_ACUSE").trim();
			NombreMandante = (reg.getString("NOMBREMANDANTE")==null)?"":reg.getString("NOMBREMANDANTE").trim();
			fideicomiso = (reg.getString("FIDEICOMISO")==null)?"":reg.getString("FIDEICOMISO").trim();
			duplicado = (reg.getString("DUPLICADO")==null)?"":reg.getString("DUPLICADO").trim();
			//si hay bandera de documentos se recuperan los valores
	//       if(banderaTablaDoctos.equals("1")){
				FECHA_ENTREGA              = (reg.getString("DF_ENTREGA")==null)?"":reg.getString("DF_ENTREGA");
				TIPO_COMPRA                   = (reg.getString("CG_TIPO_COMPRA")==null)?"":reg.getString("CG_TIPO_COMPRA");
				CLAVE_PRESUPUESTARIA       = (reg.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":reg.getString("CG_CLAVE_PRESUPUESTARIA");
				PERIODO                       = (reg.getString("CG_PERIODO")==null)?"":reg.getString("CG_PERIODO");
	//       }

					////*****1  
					
				if (datos.containsKey(IC_EPO)) {           
					 siaff = ((Boolean)datos.get(IC_EPO)).booleanValue();
				
				}else {  
										
					siaff = BeanParamDscto.estaHabilitadoNumeroSIAFF(IC_EPO);
					datos.put(IC_EPO,new Boolean(siaff));
				}
				  
				if(siaff){
					numeroSIAFF = getNumeroSIAFF(IC_EPO,IC_DOCUMENTO);                               
				}else{
					numeroSIAFF = "";
				}
				

			
				
		
			beneficiario = (reg.getString(18)==null)?"":reg.getString(18).trim();
			porciento_beneficiario = Integer.parseInt(reg.getString(19));
			importe_a_recibir_beneficiario = Double.parseDouble(reg.getString(20));

			if(strEstatus.equals("2") || strEstatus.equals("5") || strEstatus.equals("6") ||
				strEstatus.equals("7") || strEstatus.equals("9") || strEstatus.equals("10")){
					dblPorcentaje = Double.parseDouble(reg.getString(8));
					FN_MONTO_DSCTO = new Double(new Double(FN_MONTO).doubleValue() * (dblPorcentaje / 100)).toString();
			}
			
			tipoFactorajeDesc = (reg.getString("TIPO_FACTORAJE")==null)?"":reg.getString("TIPO_FACTORAJE"); 
			nombreTipoFactoraje = (reg.getString("CS_DSCTO_ESPECIAL")==null)?"":reg.getString("CS_DSCTO_ESPECIAL");
			if(nombreTipoFactoraje.equals("V")) {
				FN_MONTO_DSCTO = FN_MONTO;
				dblPorcentaje = 100;
			}
			

	//       nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal":(nombreTipoFactoraje.equals("V"))?"Vencido":(nombreTipoFactoraje.equals("D"))?"Distribuido":"";
			tipoFactorajeDesc = (reg.getString("TIPO_FACTORAJE")==null)?"":reg.getString("TIPO_FACTORAJE"); 
			

			
			dblRecurso = new Double(FN_MONTO).doubleValue() - new Double(FN_MONTO_DSCTO).doubleValue();
			if(IC_MONEDA == 54) {//D�lar
				montoTotalDL = montoTotalDL.add(new BigDecimal(FN_MONTO));
				montoTotalDsctoDL = montoTotalDsctoDL.add(new BigDecimal(FN_MONTO_DSCTO));
				numRegistrosDL++;
			} else if (IC_MONEDA == 1) {
				montoTotalMN = montoTotalMN.add(new BigDecimal(FN_MONTO));
				montoTotalDsctoMN = montoTotalDsctoMN.add(new BigDecimal(FN_MONTO_DSCTO));
				numRegistrosMN++;
			}
			registros++;
			/*GENERAMOS EL ARCHIVO*/
	  
				pdfDoc.setLCell("A", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(NOM_EPO,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(NOM_PYME,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(NOM_IF,"formas",ComunesPDF.CENTER);
				//pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fideicomiso,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(IG_NUMERO_DOCTO,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(DF_FECHA_DOCTO,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(DF_FECHA_VENC,"formas",ComunesPDF.CENTER);

				pdfDoc.setLCell(NOM_MONEDA,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(tipoFactorajeDesc,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(FN_MONTO,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("B", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(Comunes.formatoDecimal(dblPorcentaje,2,false)+"%","formas",ComunesPDF.CENTER);
				
				
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(dblRecurso,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(FN_MONTO_DSCTO,2),"formas",ComunesPDF.CENTER);
				
				pdfDoc.setLCell(ESTATUS_DOCTO_NOMBRE,"formas",ComunesPDF.CENTER);

			 pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.CENTER); 
			 
			 pdfDoc.setLCell(porciento_beneficiario!=0?Comunes.formatoDecimal(porciento_beneficiario,0,false)+"%":"","formas",ComunesPDF.CENTER);
			 pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false),"formas",ComunesPDF.CENTER); 
			
			
			pdfDoc.setLCell(df_programacion,"formas",ComunesPDF.CENTER);
			pdfDoc.setLCell(DF_ALTA,"formas",ComunesPDF.CENTER);
			pdfDoc.setLCell(duplicado,"formas",ComunesPDF.CENTER);
			pdfDoc.setLCell("C", "celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell(numeroSIAFF,"formas",ComunesPDF.CENTER);
			pdfDoc.setLCell(FECHA_ENTREGA,"formas",ComunesPDF.CENTER);
			
			
			
			pdfDoc.setLCell(TIPO_COMPRA,"formas",ComunesPDF.CENTER);
			pdfDoc.setLCell(CLAVE_PRESUPUESTARIA,"formas",ComunesPDF.CENTER);
			pdfDoc.setLCell(PERIODO.equals("0")?"":PERIODO,"formas",ComunesPDF.CENTER);



	//============================================================================>> FODEA 002 - 2009 (I)
			cont=0;
			if(!ic_epo.equals("")){
				List datosCamposAdicionales = new ArrayList();
				if(!ic_epo.equals("")){datosCamposAdicionales = BeanParamDscto.getDatosCamposAdicionales(IC_DOCUMENTO, ic_epo, numeroCamposAdicionales);}
				for(int i = 0; i < numeroCamposAdicionales; i++){
					pdfDoc.setLCell(datosCamposAdicionales.get(i).toString(),"formas",ComunesPDF.CENTER);
					cont++;
				}
			}
			
		if(mandanteDoc.equals("true"))
		  pdfDoc.setLCell(NombreMandante,"formas",ComunesPDF.CENTER);
		  else
		  pdfDoc.setLCell("", "formas",ComunesPDF.CENTER);
			
			for(int i = cont; i <4 ; i++){
				pdfDoc.setLCell("", "formas",ComunesPDF.CENTER);
		  }
	//============================================================================>> FODEA 002 - 2009 (F)
		

			//contenidoArchivo = contenidoArchivo+linea;
			//System.out.println(registros);
		} // while
			  

	
	} //if estatus="",2,5,6,7,9,10

	/*       Seleccionada Pyme             Operada                 Operada Pagada          Operada Pendiente de Pago        Aplicado a Credito            En Proceso de Autorizaci�n IF    Programado Pyme*/
	if(ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12") || ic_estatus_docto.equals("16") || ic_estatus_docto.equals("24") || ic_estatus_docto.equals("26")){//FODEA 005 - 2009 ACF
		//rs = con.queryDB(qrySentencia);
	 System.out.println("***************************************************************************************");
	
			pdfDoc.setLTable(12,100);
			pdfDoc.setLCell("A", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Nombre EPO", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("PYME", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("IF", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("FIDEICOMISO", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Num. Documento", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Num. Acuse", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Fecha emisi�n", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Fecha vencimiento", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Plazo", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Moneda", "celda01",ComunesPDF.CENTER);
		  
			pdfDoc.setLCell("Tipo Factoraje", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("B", "celda01",ComunesPDF.CENTER); 
		 
		  pdfDoc.setLCell("Monto Documento", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Porcentaje descuento", "celda01",ComunesPDF.CENTER);
		 
		  pdfDoc.setLCell("Recurso Garantia", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Monto Descontar", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Intereses del documento", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Monto a recibir", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Tasa", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Estatus", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Importe aplicado a credito", "celda01",ComunesPDF.CENTER);
		  
		  pdfDoc.setLCell("Porcentaje del documento aplicado", "celda01",ComunesPDF.CENTER);
		  
		  
		  
		  pdfDoc.setLCell("Importe a depositar a la Pyme.", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("C", "celda01",ComunesPDF.CENTER);
		  
		  
	 
	//============================================================================>> FODEA 002 - 2009 (I)
		if (ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12")){
			pdfDoc.setLCell("Fecha Aut. IF", "celda01",ComunesPDF.CENTER);
		}
		  pdfDoc.setLCell("Neto a Recibir", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Doctos Aplicados a Nota de Credito", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Beneficiario", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("% Beneficiario", "celda01",ComunesPDF.CENTER);
		  
		  pdfDoc.setLCell("Importe a Recibir Beneficiario", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Fecha Alta Docto.", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setLCell("Digito Identificador", "celda01",ComunesPDF.CENTER);
		  
		if (!(ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12"))) {
				pdfDoc.setLCell("Fecha de Recepci�n de Bienes y Servicios", "celda01",ComunesPDF.CENTER);
				
				pdfDoc.setLCell("Tipo de Compra (procedimiento)", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Clasificador por Objeto del Gasto", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo M�ximo", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("D", "celda01",ComunesPDF.CENTER);
			}else{
				
				pdfDoc.setLCell("Fecha de Recepci�n de Bienes y Servicios", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Clasificador por Objeto del Gasto", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo M�ximo", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("D", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Compra (procedimiento)", "celda01",ComunesPDF.CENTER);
			}
		  
		  
		 
		  
		  cont=0;
		  for(int i = 0; i < nombresCamposAdicionales.size(); i++){
			pdfDoc.setLCell(nombresCamposAdicionales.get(i).toString(), "celda01",ComunesPDF.CENTER);
			cont++;
		  }
			if(mandanteDoc.equals("true"))
		  pdfDoc.setLCell("Mandante","celda01",ComunesPDF.CENTER);
		  else
		  pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
		  
		if(ic_estatus_docto.equals("26")){pdfDoc.setLCell("Fecha Registro Operaci�n", "celda01",ComunesPDF.CENTER);}else{pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);}//FODEA 005 - 2009 ACF
		for(int i = cont; i <5 ; i++){
				pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
		  }
		  if (!(ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12"))) {
				pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
			}
			pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("", "celda01",ComunesPDF.CENTER);
			pdfDoc.setLHeaders();
	//============================================================================>> FODEA 002 - 2009 (F)
		String ig_tipo_piso="",fn_remanente="";
		while (reg.next()){
			NOM_EPO = (reg.getString("NOMBREEPO") == null) ? "" : reg.getString("NOMBREEPO");
			NOM_PYME = (reg.getString("NOMBREPYME") == null) ? "" : reg.getString("NOMBREPYME");
			NOM_IF = (reg.getString("NOMBREIF")==null)?"":reg.getString("NOMBREIF").trim();
			IG_NUMERO_DOCTO = (reg.getString("IG_NUMERO_DOCTO") == null) ? "" : reg.getString("IG_NUMERO_DOCTO");
			cc_acuse = reg.getString("CC_ACUSE");
			DF_FECHA_DOCTO = (reg.getString("DF_FECHA_DOCTO") == null) ? "" : reg.getString("DF_FECHA_DOCTO");
			DF_FECHA_VENC = (reg.getString("DF_FECHA_VENC") == null) ? "" : reg.getString("DF_FECHA_VENC");
			plazo = (reg.getString("IG_PLAZO") == null) ? "" : reg.getString("IG_PLAZO");
			NOM_MONEDA = (reg.getString("MONEDA") == null) ? "" : reg.getString("MONEDA");
			IC_MONEDA = Integer.parseInt(reg.getString("IC_MONEDA"));
			FN_MONTO = (reg.getString("FN_MONTO") == null) ? "0.00" : reg.getString("FN_MONTO");
			dblPorcentaje = Double.parseDouble(reg.getString("FN_PORC_ANTICIPO"));
			FN_MONTO_DSCTO = (reg.getString("FN_MONTO_DSCTO")==null)?"0.00":reg.getString("FN_MONTO_DSCTO");
			importe_interes = Double.parseDouble(reg.getString("IN_IMPORTE_INTERES"));
			importe_recibir = Double.parseDouble(reg.getString("IN_IMPORTE_RECIBIR"));
			IN_TASA_ACEPTADA = (reg.getString("IN_TASA_ACEPTADA") == null) ? "" : reg.getString("IN_TASA_ACEPTADA");
			ESTATUS_DOCTO_NOMBRE = (reg.getString("CD_DESCRIPCION") == null) ? "" : reg.getString("CD_DESCRIPCION");
			FECHA_SOLICITUD = (reg.getString("DF_FECHA_SOLICITUD") == null) ? "" : reg.getString("DF_FECHA_SOLICITUD");
			IC_DOCUMENTO = (reg.getString("IC_DOCUMENTO") == null) ? "" : reg.getString("IC_DOCUMENTO");
			CS_CAMBIO_IMPORTE = (reg.getString("CS_CAMBIO_IMPORTE") == null) ? "" : reg.getString("CS_CAMBIO_IMPORTE");
			porc_docto_aplicado  = (reg.getString("PORCDOCTOAPLICADO") == null) ? "" : reg.getString("porcDoctoAplicado");
			montoPago = Double.parseDouble(reg.getString("FN_MONTO_PAGO"));
			beneficiario = (reg.getString("BENEFICIARIO")==null)?"":reg.getString("BENEFICIARIO").trim();
			porciento_beneficiario = Integer.parseInt(reg.getString("FN_PORC_BENEFICIARIO"));
			importe_a_recibir_beneficiario = Double.parseDouble(reg.getString("RECIBIR_BENEF"));
			neto_a_recibir_pyme =  Double.parseDouble(reg.getString("NETO_REC_PYME"));
			DF_ALTA = (reg.getString("DF_ALTA")==null)?"":reg.getString("DF_ALTA").trim();
			//RFC = (reg.getString("RFC")==null)?"":reg.getString("RFC").trim();
			ESTATUS_DOCTO = (reg.getString("ic_estatus_docto")==null)?"":reg.getString("ic_estatus_docto");
			ig_tipo_piso = (reg.getString("IG_TIPO_PISO")==null)?"":reg.getString("IG_TIPO_PISO");
			fn_remanente = (reg.getString("FN_REMANENTE")==null)?"":reg.getString("FN_REMANENTE");
			// Factoraje Vencido
			tipoFactorajeDesc = (reg.getString("TIPO_FACTORAJE")==null)?"":reg.getString("TIPO_FACTORAJE"); 
			nombreTipoFactoraje = (reg.getString("CS_DSCTO_ESPECIAL")==null)?"":reg.getString("CS_DSCTO_ESPECIAL");
			IC_EPO      = (reg.getString("IC_EPO")==null)?"":reg.getString("IC_EPO").trim();
			fecha_registro_operacion = reg.getString("FECHA_REGISTRO_OPERACION")==null?"":reg.getString("FECHA_REGISTRO_OPERACION");//FODEA 005 - 2009 ACF
			///*******2
			fideicomiso = (reg.getString("FIDEICOMISO")==null)?"":reg.getString("FIDEICOMISO").trim();
				if (datos.containsKey(IC_EPO)) {           
					 siaff = ((Boolean)datos.get(IC_EPO)).booleanValue();
				
				}else {  
										
					siaff = BeanParamDscto.estaHabilitadoNumeroSIAFF(IC_EPO);
					datos.put(IC_EPO,new Boolean(siaff));
				}
				
				if(siaff){
					numeroSIAFF = getNumeroSIAFF(IC_EPO,IC_DOCUMENTO);                
				}else{
					numeroSIAFF = "";
				}
				


			//si hay bandera de documentos se recuperan los valores
	//       if(banderaTablaDoctos.equals("1")){
				FECHA_ENTREGA              = (reg.getString("DF_ENTREGA")==null)?"":reg.getString("DF_ENTREGA");
				TIPO_COMPRA                   = (reg.getString("CG_TIPO_COMPRA")==null)?"":reg.getString("CG_TIPO_COMPRA");
				CLAVE_PRESUPUESTARIA       = (reg.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":reg.getString("CG_CLAVE_PRESUPUESTARIA");
				PERIODO                       = (reg.getString("CG_PERIODO")==null)?"":reg.getString("CG_PERIODO");
	//       }


	//       nombreTipoFactoraje = (nombreTipoFactoraje.equals("N"))?"Normal":(nombreTipoFactoraje.equals("V"))?"Vencido":(nombreTipoFactoraje.equals("D"))?"Distribuido":"";
			tipoFactorajeDesc = (reg.getString("TIPO_FACTORAJE")==null)?"":reg.getString("TIPO_FACTORAJE"); 
			

			
			dblRecurso = new Double(FN_MONTO).doubleValue() - new Double(FN_MONTO_DSCTO).doubleValue();
			importe_depositar_pyme = importe_recibir - montoPago;


			//esNotaDeCreditoSimpleAplicada = "true".equals(reg.getString("existeDocumentoAsociado"))?true:false;
			//esDocumentoConNotaDeCreditoSimpleAplicada = "true".equals(reg.getString("esDocConNotaDeCredSimpleApl"))?true:false;
				
			//esNotaDeCreditoAplicada = "true".equals(reg.getString("existeRefEnComrelNotaDocto"))?true:false;
			//esDocumentoConNotaDeCreditoMultipleAplicada   = "true".equals(reg.getString("esDocConNotaDeCredMultApl"))?true:false;
			esNotaDeCreditoSimpleAplicada                = ( nombreTipoFactoraje.equals("C") && (ESTATUS_DOCTO.equals("3") || ESTATUS_DOCTO.equals("4") || ESTATUS_DOCTO.equals("11")) && BeanParamDscto.existeDocumentoAsociado(String.valueOf(IC_DOCUMENTO)) )?true:false;
			esDocumentoConNotaDeCreditoSimpleAplicada    = ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(String.valueOf(IC_DOCUMENTO)))?true:false;
	
			esNotaDeCreditoAplicada                      = ( nombreTipoFactoraje.equals("C") && (ESTATUS_DOCTO.equals("3") || ESTATUS_DOCTO.equals("4") || ESTATUS_DOCTO.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(String.valueOf(IC_DOCUMENTO)) )?true:false;
			esDocumentoConNotaDeCreditoMultipleAplicada  = ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(String.valueOf(IC_DOCUMENTO)))?true:false;
	
			
			if(esNotaDeCreditoSimpleAplicada){
				numeroDocumento                           = getNumeroDoctoAsociado(String.valueOf(IC_DOCUMENTO)); 
			}else if(esDocumentoConNotaDeCreditoSimpleAplicada){
				numeroDocumento                           = getNumeroNotaCreditoAsociada(String.valueOf(IC_DOCUMENTO));
			}
			
			registros++;
			/*GENERAMOS EL ARCHIVO*/
	  
					pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(NOM_EPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(NOM_PYME,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(NOM_IF,"formas",ComunesPDF.CENTER);
					//pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fideicomiso,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(IG_NUMERO_DOCTO,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cc_acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(DF_FECHA_DOCTO,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(DF_FECHA_VENC,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(NOM_MONEDA,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoFactorajeDesc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
					
					
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(FN_MONTO,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(dblPorcentaje,2,false)+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(dblRecurso,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(FN_MONTO_DSCTO,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_interes,2,true),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_recibir,2,true),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(IN_TASA_ACEPTADA,4)+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(ESTATUS_DOCTO_NOMBRE,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoPago,2,true),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(porc_docto_aplicado,2,false)+"%","formas",ComunesPDF.CENTER);
					
					
		

			if(ic_estatus_docto.equals("16") ){
				if(ig_tipo_piso.equals("1")){
					if(!fn_remanente.equals("")){
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(Double.parseDouble(fn_remanente),2,true),"formas",ComunesPDF.CENTER);
					}else{
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					}
				}else{
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(Double.parseDouble(fn_remanente),2,true),"formas",ComunesPDF.CENTER);
				}
			}else{
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_depositar_pyme,2,true),"formas",ComunesPDF.CENTER);
			}
			
			pdfDoc.setLCell("C","celda01",ComunesPDF.CENTER);
			
			if (ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12")) {
				pdfDoc.setLCell(FECHA_SOLICITUD,"formas",ComunesPDF.CENTER);
			}
			pdfDoc.setLCell("$"+Comunes.formatoDecimal(neto_a_recibir_pyme,2,true),"formas",ComunesPDF.CENTER);
			
			// Fodea 002 - 2010.
		
			if(esNotaDeCreditoAplicada || esDocumentoConNotaDeCreditoMultipleAplicada){
				pdfDoc.setLCell("Si","formas",ComunesPDF.CENTER);
			}else
			if(esNotaDeCreditoSimpleAplicada || esDocumentoConNotaDeCreditoSimpleAplicada){
			pdfDoc.setLCell("\""+numeroDocumento+"\"","formas",ComunesPDF.CENTER);
			}else{
				pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
			}
			
			if(!"".equals(beneficiario)) pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.CENTER); else pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
			if(0 != porciento_beneficiario) pdfDoc.setLCell(Comunes.formatoDecimal(porciento_beneficiario,2,false)+"%","formas",ComunesPDF.CENTER);else pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
			if(0 != importe_a_recibir_beneficiario) pdfDoc.setLCell(Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,true),"formas",ComunesPDF.CENTER);else pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
			
			
			pdfDoc.setLCell(DF_ALTA,"formas",ComunesPDF.CENTER);
			pdfDoc.setLCell(numeroSIAFF,"formas",ComunesPDF.CENTER);
			pdfDoc.setLCell(FECHA_ENTREGA,"formas",ComunesPDF.CENTER);
			if (!(ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12"))) {
				pdfDoc.setLCell(TIPO_COMPRA,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(CLAVE_PRESUPUESTARIA,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(PERIODO.equals("0")?"":PERIODO,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("D","celda01",ComunesPDF.CENTER);
		
			}else{
				pdfDoc.setLCell(CLAVE_PRESUPUESTARIA,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(PERIODO.equals("0")?"":PERIODO,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("D","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(TIPO_COMPRA,"formas",ComunesPDF.CENTER);
				
			}
			
			

			//si hay bandera de documentos se recuperan los valores
	//       if(banderaTablaDoctos.equals("1")){
				
				
	//       }
			cont=0;
	//============================================================================>> FODEA 002 - 2009 (I)
			if(!ic_epo.equals("")){
				List datosCamposAdicionales = new ArrayList();
				if(!ic_epo.equals("")){datosCamposAdicionales = BeanParamDscto.getDatosCamposAdicionales(IC_DOCUMENTO, ic_epo, numeroCamposAdicionales);}
				for(int i = 0; i < numeroCamposAdicionales; i++){
					cont++;
					pdfDoc.setLCell(datosCamposAdicionales.get(i).toString(),"formas",ComunesPDF.CENTER);
				}
			}
			if(mandanteDoc.equals("true"))
		  pdfDoc.setLCell(NombreMandante,"formas",ComunesPDF.CENTER);
		  else
		  pdfDoc.setLCell("", "formas",ComunesPDF.CENTER);
			if(ic_estatus_docto.equals("26")){pdfDoc.setLCell(fecha_registro_operacion,"formas",ComunesPDF.CENTER); }else{ pdfDoc.setLCell("", "formas",ComunesPDF.CENTER);  }//FODEA 005 - 2009 ACF
			for(int i = cont; i <5 ; i++){
				pdfDoc.setLCell("", "formas",ComunesPDF.CENTER);
		  }
		  
		  if (!(ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11") || ic_estatus_docto.equals("12"))) {
				pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
		
			}
			pdfDoc.setLCell("", "formas",ComunesPDF.CENTER);
			pdfDoc.setLCell("", "formas",ComunesPDF.CENTER);
			pdfDoc.setLCell("", "formas",ComunesPDF.CENTER);
	//============================================================================>> FODEA 002 - 2009 (F)
			//contenidoArchivo = contenidoArchivo+linea;
		} // while
	} //if estatus=3,4,11,12


		  
		 /* while (reg.next()) {
				String razonEpo = (reg.getString("RAZON_SOCIAL_EPO") == null) ? "" : reg.getString("RAZON_SOCIAL_EPO");
				String razonPyme = (reg.getString("RAZON_SOCIAL_PYME") == null) ? "" : reg.getString("RAZON_SOCIAL_PYME");
				String rfc = (reg.getString("RFC") == null) ? "" : reg.getString("RFC");
				String domicilio = (reg.getString("DOMICILIO") == null) ? "" : reg.getString("DOMICILIO");
				String contacto = (reg.getString("CONTACTO") == null) ? "" : reg.getString("CONTACTO");
				String telefono = (reg.getString("TELEFONO") == null) ? "" : reg.getString("TELEFONO");
				String fecha = (reg.getString("FECHA") == null) ? "" : reg.getString("FECHA");
				
				pdfDoc.setLCell(razonEpo,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(razonPyme,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rfc,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(domicilio,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(contacto,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(telefono,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fecha,"formas",ComunesPDF.CENTER);
			}*/
		  pdfDoc.addLTable();
		  pdfDoc.endDocument();
		
		
		}
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		
		return nombreArchivo;
	}
	
	public ArrayList getConditions(HttpServletRequest request){
		log.info("getConditions(E)");
    	ArrayList conditions = new ArrayList();

		String envia =(request.getParameter("envia") == null)? "": request.getParameter("envia");
		String ic_epo =(request.getParameter("ic_epo") == null)? "": request.getParameter("ic_epo");
		String ic_if =(request.getParameter("ic_if") == null)? "": request.getParameter("ic_if");
		String ic_pyme =(request.getParameter("ic_pyme") == null)? "": request.getParameter("ic_pyme");
		String ig_numero_docto =(request.getParameter("ig_numero_docto") == null)? "": request.getParameter("ig_numero_docto");
		String df_fecha_docto_de =(request.getParameter("df_fecha_docto_de") == null)? "": request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a =(request.getParameter("df_fecha_docto_a") == null)? "": request.getParameter("df_fecha_docto_a");
		String df_fecha_venc_de =(request.getParameter("df_fecha_venc_de") == null)? "": request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a =(request.getParameter("df_fecha_venc_a") == null)? "": request.getParameter("df_fecha_venc_a");
		String ic_moneda =(request.getParameter("ic_moneda") == null)? "": request.getParameter("ic_moneda");
		String fn_monto_de =(request.getParameter("fn_monto_de") == null)? "": request.getParameter("fn_monto_de");
		String fn_monto_a =(request.getParameter("fn_monto_a") == null)? "": request.getParameter("fn_monto_a");
		String ic_estatus_docto =(request.getParameter("ic_estatus_docto") == null)? "": request.getParameter("ic_estatus_docto");
		String df_fecha_seleccion_de =(request.getParameter("df_fecha_seleccion_de") == null)? "": request.getParameter("df_fecha_seleccion_de");
		String df_fecha_seleccion_a =(request.getParameter("df_fecha_seleccion_a") == null)? "": request.getParameter("df_fecha_seleccion_a");

		String in_tasa_aceptada_de =(request.getParameter("in_tasa_aceptada_de") == null)? "": request.getParameter("in_tasa_aceptada_de");
		String in_tasa_aceptada_a =(request.getParameter("in_tasa_aceptada_a") == null)? "": request.getParameter("in_tasa_aceptada_a");
		String ord_if =(request.getParameter("ord_if") == null)? "": request.getParameter("ord_if");
		String ord_pyme =(request.getParameter("ord_pyme") == null)? "": request.getParameter("ord_pyme");
		String ord_epo =(request.getParameter("ord_epo") == null)? "": request.getParameter("ord_epo");
		String ord_fec_venc =(request.getParameter("ord_fec_venc") == null)? "": request.getParameter("ord_fec_venc");
		String operacion =(request.getParameter("operacion") == null)? "": request.getParameter("operacion");
		String tipoFactoraje =(request.getParameter("tipoFactoraje") == null)? "": request.getParameter("tipoFactoraje");
		// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
		String paginaNo =(request.getParameter("paginaNo") == null)? "1": request.getParameter("paginaNo");
		String Perfil =(request.getParameter("Perfil") == null)? "": request.getParameter("Perfil");

		String nomArchivo =(request.getParameter("nomArchivo") == null)? "": request.getParameter("nomArchivo");
		String  infoProcesadaEn =(request.getParameter("infoProcesadaEn") == null)? "": request.getParameter("infoProcesadaEn");

		String 	numero_siaff 					= (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");

		boolean 	estaHabilitadoNumeroSIAFF 	= (request.getAttribute("estaHabilitadoNumeroSIAFF") != null && request.getAttribute("estaHabilitadoNumeroSIAFF").equals("true"))?true:false;
		String ic_documento		= "";

		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF ){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 				= (String) numero.get("IC_EPO");
			ic_documento 		= (String) numero.get("IC_DOCUMENTO");
		}

		if (ic_estatus_docto.equals("")
			|| ic_estatus_docto.equals("1")
			|| ic_estatus_docto.equals("2")
			|| ic_estatus_docto.equals("5")
			|| ic_estatus_docto.equals("6")
			|| ic_estatus_docto.equals("7")
			|| ic_estatus_docto.equals("9")
			|| ic_estatus_docto.equals("10")
			|| ic_estatus_docto.equals("21")
			|| ic_estatus_docto.equals("23")
			|| ic_estatus_docto.equals("28")
			|| ic_estatus_docto.equals("29")
			|| ic_estatus_docto.equals("30")
			|| ic_estatus_docto.equals("31"))
		{
		log.debug("Paso x aqui 1.1---------------");

	      if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
			conditions.add(infoProcesadaEn+"%");
			if (!ic_epo.equals(""))
				conditions.add(new Integer(ic_epo));
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				conditions.add(new Integer(ic_pyme));
			if (!ic_if.equals("T") && !ic_if.equals(""))
				conditions.add(new Integer(ic_if));
			if (!ig_numero_docto.equals(""))
				conditions.add(ig_numero_docto);
			if (!df_fecha_docto_de.equals("")){
				conditions.add(df_fecha_docto_de);
			}
			if (!df_fecha_docto_a.equals("")){
				conditions.add(df_fecha_docto_a);
			}
			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals("")){
				conditions.add(df_fecha_venc_de);
				conditions.add(df_fecha_venc_a);
			}
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				conditions.add(new Integer(ic_moneda));
			if (!fn_monto_de.equals("") && !fn_monto_a.equals("")){
				conditions.add(new Double(fn_monto_de));
				conditions.add(new Double(fn_monto_a));
			}
			if (!ic_estatus_docto.equals(""))
				conditions.add(new Integer(ic_estatus_docto));
			if (!tipoFactoraje.equals(""))
				conditions.add(tipoFactoraje);
			// Condiciones por Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				conditions.add(ic_documento);
			}

		}
		else if (
			ic_estatus_docto.equals("3")
				|| ic_estatus_docto.equals("4")
				|| ic_estatus_docto.equals("11")
				|| ic_estatus_docto.equals("12")
				|| ic_estatus_docto.equals("16")
				|| ic_estatus_docto.equals("24")
				|| ic_estatus_docto.equals("26"))//FODEA 005 - 2009 ACF
		{

		log.debug("Paso x aqui 1.2-------------");

	      if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
			conditions.add(infoProcesadaEn+"%");
			if (!ic_epo.equals(""))
				conditions.add(new Integer(ic_epo));
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				conditions.add(new Integer(ic_pyme));
			if (!ic_if.equals("T") && !ic_if.equals(""))
				conditions.add(new Integer(ic_if));
			if (!ig_numero_docto.equals(""))
				conditions.add(ig_numero_docto);

			if (!df_fecha_docto_de.equals("")){
				conditions.add(df_fecha_docto_de);
			}
			if (!df_fecha_docto_a.equals("")){
				conditions.add(df_fecha_docto_a);
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals("")){
				conditions.add(df_fecha_venc_de);
				conditions.add(df_fecha_venc_a);
			}
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				conditions.add(new Integer(ic_moneda));
			if (!fn_monto_de.equals("") && !fn_monto_a.equals("")){
				conditions.add(new Double(fn_monto_de));
				conditions.add(new Double(fn_monto_a));
			}
			if (!ic_estatus_docto.equals(""))
				conditions.add(new Integer(ic_estatus_docto));
			if (!ic_estatus_docto.equals("3")
				&& !ic_estatus_docto.equals("24"))
			{
				if (!df_fecha_seleccion_de.equals("")
					&& !df_fecha_seleccion_a.equals("")){
					conditions.add(df_fecha_seleccion_de);
					conditions.add(df_fecha_seleccion_a);
					}
			}
			if (!in_tasa_aceptada_de.equals("")
				&& !in_tasa_aceptada_a.equals("")){
				conditions.add(new Double(in_tasa_aceptada_de));
				conditions.add(new Double(in_tasa_aceptada_a));
				}
			if (!"".equals(tipoFactoraje))
				conditions.add(tipoFactoraje);
			// Condiciones por Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				conditions.add(ic_documento);
			}

		}

		log.info("getConditions(S)");
		return conditions;
	}

	public String getAggregateCalculationQuery(HttpServletRequest request){
		log.info("getAggregateCalculationQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		numList = 1;

		String envia = (request.getParameter("envia") == null)? "": request.getParameter("envia");
		String ic_epo =(request.getParameter("ic_epo") == null)? "": request.getParameter("ic_epo");
		String ic_if =(request.getParameter("ic_if") == null)? "": request.getParameter("ic_if");
		String ic_pyme =(request.getParameter("ic_pyme") == null)? "": request.getParameter("ic_pyme");
		String ig_numero_docto =(request.getParameter("ig_numero_docto") == null)? "": request.getParameter("ig_numero_docto");
		String df_fecha_docto_de =(request.getParameter("df_fecha_docto_de") == null)? "": request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a =(request.getParameter("df_fecha_docto_a") == null)? "": request.getParameter("df_fecha_docto_a");
		String df_fecha_venc_de =(request.getParameter("df_fecha_venc_de") == null)? "": request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a =(request.getParameter("df_fecha_venc_a") == null)? "": request.getParameter("df_fecha_venc_a");
		String ic_moneda =(request.getParameter("ic_moneda") == null)? "": request.getParameter("ic_moneda");
		String fn_monto_de =(request.getParameter("fn_monto_de") == null)? "": request.getParameter("fn_monto_de");
		String fn_monto_a =(request.getParameter("fn_monto_a") == null)? "": request.getParameter("fn_monto_a");
		String ic_estatus_docto =(request.getParameter("ic_estatus_docto") == null)? "": request.getParameter("ic_estatus_docto");
		String df_fecha_seleccion_de =(request.getParameter("df_fecha_seleccion_de") == null)? "": request.getParameter("df_fecha_seleccion_de");
		String df_fecha_seleccion_a =(request.getParameter("df_fecha_seleccion_a") == null)? "": request.getParameter("df_fecha_seleccion_a");
		String in_tasa_aceptada_de =(request.getParameter("in_tasa_aceptada_de") == null)? "": request.getParameter("in_tasa_aceptada_de");
		String in_tasa_aceptada_a =(request.getParameter("in_tasa_aceptada_a") == null)? "": request.getParameter("in_tasa_aceptada_a");
		String ord_if =(request.getParameter("ord_if") == null)? "": request.getParameter("ord_if");
		String ord_pyme =(request.getParameter("ord_pyme") == null)? "": request.getParameter("ord_pyme");
		String ord_epo =(request.getParameter("ord_epo") == null)? "": request.getParameter("ord_epo");
		String ord_fec_venc =(request.getParameter("ord_fec_venc") == null)? "": request.getParameter("ord_fec_venc");
		String operacion =(request.getParameter("operacion") == null)? "": request.getParameter("operacion");
		String tipoFactoraje =(request.getParameter("tipoFactoraje") == null)? "": request.getParameter("tipoFactoraje");
    	// MPCS.- Parametros para descuento automatico
		String  infoProcesadaEn =(request.getParameter("infoProcesadaEn") == null)? "": request.getParameter("infoProcesadaEn");
		// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
		String paginaNo = (request.getParameter("paginaNo") == null)? "1": request.getParameter("paginaNo");
		String nomArchivo =(request.getParameter("nomArchivo") == null)? "": request.getParameter("nomArchivo");
		String  ic_banco_fondeo =(request.getParameter("ic_banco_fondeo") == null)? "": request.getParameter("ic_banco_fondeo");
		String hint = "";
		String campos1[] = { "nombreEpo", "D.df_fecha_venc", "nombrePyme" };
		String orden1[] = { ord_epo, ord_fec_venc, ord_pyme };
		String campos2[] = { "nombreEpo", "nombreIf", "D.df_fecha_venc", "nombrePyme" };
		String orden2[] = { ord_epo, ord_if, ord_fec_venc, ord_pyme };
		String Perfil =(request.getParameter("Perfil") == null)? "": request.getParameter("Perfil");

		String	numero_siaff 					= (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
		boolean 	estaHabilitadoNumeroSIAFF 	= (request.getAttribute("estaHabilitadoNumeroSIAFF") != null && request.getAttribute("estaHabilitadoNumeroSIAFF").equals("true"))?true:false;
		String	ic_documento = "";

		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 				= (String) numero.get("IC_EPO");
			ic_documento		= (String) numero.get("IC_DOCUMENTO");
		}

		if (ic_estatus_docto.equals("")
			|| ic_estatus_docto.equals("1")
			|| ic_estatus_docto.equals("2")
			|| ic_estatus_docto.equals("5")
			|| ic_estatus_docto.equals("6")
			|| ic_estatus_docto.equals("7")
			|| ic_estatus_docto.equals("9")
			|| ic_estatus_docto.equals("10")
			|| ic_estatus_docto.equals("21")
			|| ic_estatus_docto.equals("23")
			|| ic_estatus_docto.equals("28")
			|| ic_estatus_docto.equals("29")
      || ic_estatus_docto.equals("30")
      || ic_estatus_docto.equals("31"))
		{

			log.debug("Paso x aqui 1.3---------------");

			if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
			  condicion.append(" and exists (select 1"   +
											" from com_docto_seleccionado ds"   +
											" where d.ic_documento=ds.ic_documento"   +
											" and ds.cc_acuse like ?) ");
			if (!ic_epo.equals(""))
				condicion.append("and D.ic_epo = ? ");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				condicion.append("and D.ic_pyme = ? ");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				condicion.append("and D.ic_if = ? ");
			if (!ig_numero_docto.equals(""))
				condicion.append("and D.ig_numero_docto = ? ");
			if (!df_fecha_docto_de.equals("")){
				condicion.append("and D.df_fecha_docto >= trunc(TO_DATE(?,'DD/MM/YYYY'))  ");
			}
			if (!df_fecha_docto_a.equals("")){
				condicion.append("and D.df_fecha_docto < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			}
			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals(""))
				condicion.append("and D.df_fecha_venc >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.df_fecha_venc < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				condicion.append("and D.ic_moneda = ? ");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				condicion.append("and D.fn_monto between ? and ? ");
			if (!ic_estatus_docto.equals(""))
				condicion.append("and D.ic_estatus_docto = ? ");
			else
      		{ if (infoProcesadaEn.equals("5")||infoProcesadaEn.equals("8")||infoProcesadaEn.equals("9"))
          		condicion.append("and D.ic_estatus_docto in (3,4,11) ");
        	  else
          		condicion.append("and D.ic_estatus_docto in(2,3) ");
      		}
			if (!tipoFactoraje.equals(""))
				condicion.append("and D.cs_dscto_especial = ? ");
			// Revisar esta seccion
			if(ic_documento!=null&&!ic_documento.equals("")){
				condicion.append(" AND D.ic_documento = ? ");
			}

			if (!"".equals(validaDoctoEPO)   ) {
			    condicion.append(" AND D.CS_VALIDACION_JUR = '" + this.validaDoctoEPO + "' ");
			}

			// Obtiene todos los documentos menos las notas de credito.
			StringBuffer sQryTodos =  new StringBuffer(
				"select /*+ use_nl(d m cv pe e)*/ "+
				" count(1) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
				"sum(D.fn_monto * (decode(D.ic_moneda, 1, cv.fn_aforo, 54, cv.fn_aforo_dl) / 100)) AS FN_MONTO_DSCTO, "+
				"M.cd_nombre, M.ic_moneda, 'D' as tipoDocto "+
				"from com_documento D, comcat_moneda M, comvis_aforo CV, comrel_producto_epo pe , comcat_epo E "+
				"where D.ic_moneda = M.ic_moneda "+
				"and D.ic_epo = CV.ic_epo "+
				" AND pe.ic_epo = D.ic_epo " +
				" AND D.ic_epo = E.ic_epo " +
				" AND pe.ic_producto_nafin = 1 " +
				//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
				//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
				condicion.toString() +
				"and D.cs_dscto_especial != 'C' "+
				"group by M.ic_moneda, M.cd_nombre");

			// Obtiene �nicamente los documentos negociables sin notas de cr�dito.
			StringBuffer sNegociable = new StringBuffer(
				"select /*+ use_nl(d m cv pe e)*/ "+
				" count(1) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
				"sum(D.fn_monto * (decode(D.ic_moneda, 1, cv.fn_aforo, 54, cv.fn_aforo_dl) / 100)) AS FN_MONTO_DSCTO, "+
				"M.cd_nombre, M.ic_moneda, 'DN' as tipoDocto "+
				"from com_documento D, comcat_moneda M, comvis_aforo CV, comrel_producto_epo pe , comcat_epo E "+
				"where D.ic_moneda = M.ic_moneda "+
				"and D.ic_epo = CV.ic_epo "+
				" AND pe.ic_epo = D.ic_epo " +
				" AND pe.ic_producto_nafin = 1 " +
				" AND D.ic_epo = E.ic_epo " +
				//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
				//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
				condicion.toString() +
				"and D.ic_estatus_docto = 2 "+
				"and D.cs_dscto_especial != 'C' "+
				"group by M.ic_moneda, M.cd_nombre");

			// Obtiene los documentos negociables que son notas de cr�dito
			StringBuffer sNotasCredito = new StringBuffer(
				"select /*+ use_nl(d m cv pe e)*/ "+
				" count(1) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
				"sum(D.fn_monto * (decode(D.ic_moneda, 1, cv.fn_aforo, 54, cv.fn_aforo_dl) / 100)) AS FN_MONTO_DSCTO, "+
				"M.cd_nombre, M.ic_moneda, 'NC' as tipoDocto "+
				"from com_documento D, comcat_moneda M, comvis_aforo CV, comrel_producto_epo pe , comcat_epo E "+
				"where D.ic_moneda = M.ic_moneda "+
				"and D.ic_epo = CV.ic_epo "+
				" AND pe.ic_epo = D.ic_epo " +
				" AND pe.ic_producto_nafin = 1 " +
				" AND D.ic_epo = E.ic_epo " +
				//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
				//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
				condicion +
				"and D.ic_estatus_docto = 2 "+
				"and D.cs_dscto_especial = 'C' "+
				"group by M.ic_moneda, M.cd_nombre");

			numList++;
			sQryTodos.append(" UNION ALL "+ sNotasCredito.toString());
			if(!ic_estatus_docto.equals("2")) {
				numList++;
				sQryTodos.append(" UNION ALL "+ sNegociable.toString());
			}

			qrySentencia.append(" select * from ("+sQryTodos.toString()+") order by ic_moneda ");

			//System.out.println("getAggregateCalculationQuery"+qrySentencia);
		}
		else if (
				ic_estatus_docto.equals("3")
				|| ic_estatus_docto.equals("4")
				|| ic_estatus_docto.equals("11")
				|| ic_estatus_docto.equals("12")
				|| ic_estatus_docto.equals("16")
				|| ic_estatus_docto.equals("24")
				|| ic_estatus_docto.equals("26"))//FODEA 005 - 2009 ACF
		{
			log.debug("Paso x aqui 1.4--------------");
			//hint predeterminado
			hint = " /*+  use_nl(d cv m s ds e pe) index(d IN_COM_DOCUMENTO_04_NUK) */ ";

			if (!ic_epo.equals(""))
				condicion.append("and D.ic_epo = ? ");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				condicion.append("and D.ic_pyme = ? ");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				condicion.append("and D.ic_if = ? ");
			if (!ig_numero_docto.equals("")) {
				condicion.append("and D.ig_numero_docto = ? ");
			}

			if (!df_fecha_docto_de.equals("")){
				condicion.append("and D.df_fecha_docto >= TO_DATE(?,'DD/MM/YYYY')  ");
			}
			if (!df_fecha_docto_a.equals("")){
				condicion.append("and D.df_fecha_docto < TO_DATE(?,'DD/MM/YYYY') + 1 ");
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals("")){
				hint = " /*+  USE_NL(d cv m s ds e pe) index(d IN_COM_DOCUMENTO_09_NUK) */ ";
				condicion.append("and D.df_fecha_venc >= TO_DATE(?,'DD/MM/YYYY') AND D.df_fecha_venc < TO_DATE(?,'DD/MM/YYYY') + 1 ");
			}
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				condicion.append("and D.ic_moneda = ? ");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				condicion.append("and D.fn_monto between ? and ? ");

			if (!ic_estatus_docto.equals("")) {
				condicion.append("and D.ic_estatus_docto = ? ");
			}
			if (!ic_estatus_docto.equals("3") && !ic_estatus_docto.equals("24")) {
				if (!df_fecha_seleccion_de.equals("") && !df_fecha_seleccion_a.equals("")){
					condicion.append(" AND s.df_fecha_solicitud >= trunc(TO_DATE(?,'DD/MM/YYYY')) AND s.df_fecha_solicitud < trunc(TO_DATE(?,'DD/MM/YYYY')+1) ");
					hint = " /*+  use_nl(d cv m s ds e pe) INDEX (s IN_COM_SOLICITUD_13_NUK) */ ";
				}
			}
			if (!in_tasa_aceptada_de.equals("") && !in_tasa_aceptada_a.equals(""))
				condicion.append("and DS.in_tasa_aceptada between ? and ? ");
			if (!"".equals(tipoFactoraje))
				condicion.append("and D.cs_dscto_especial = ? ");
			// Revisar esta seccion
			if(ic_documento!=null&&!ic_documento.equals("")){
				condicion.append(" AND D.ic_documento = ? ");
			}
			if (!"".equals(validaDoctoEPO)   ) {
			    condicion.append(" AND D.CS_VALIDACION_JUR = '" + this.validaDoctoEPO + "' ");
			}

			if(!in_tasa_aceptada_de.equals("") && !in_tasa_aceptada_a.equals("") &&
				!df_fecha_seleccion_de.equals("") && !df_fecha_seleccion_a.equals("")) {

				  qrySentencia.append(
						"select "+hint+" count(*) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
						"sum(fn_monto_dscto) AS FN_MONTO_DSCTO, "+
          				"M.cd_nombre, M.ic_moneda, 'D' as tipoDocto "+
						"from com_documento D, com_docto_seleccionado DS, com_solicitud S, comcat_epo E "+
						"comcat_moneda M, comvis_aforo CV, comrel_producto_epo pe "+
						"where S.ic_documento = DS.ic_documento "+
						"and D.ic_documento = S.ic_documento "+
						"and D.ic_epo = CV.ic_epo "+
						" AND pe.ic_epo = D.ic_epo " +
						" AND pe.ic_producto_nafin = 1 " +
						" AND pe.ic_epo = E.ic_epo " +
						//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
            (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
            (Perfil.equals("ADMIN BANCOMEXT")?" and S.ic_banco_fondeo= 2":"") +
						//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
						"and D.ic_moneda = M.ic_moneda ");
				if (!infoProcesadaEn.equals("")) // infoProcesadaEn puede tener "",5-Descuento Autom�tico,8-Operaci�n Telefonica,9-Cobranza Manual
					qrySentencia.append(" and ds.cc_acuse like ? ");
			}
			else if(!df_fecha_seleccion_de.equals("") && !df_fecha_seleccion_a.equals("")){

				qrySentencia.append(
						"select "+hint+" count(*) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
						"sum(fn_monto_dscto) AS FN_MONTO_DSCTO, "+
          			"M.cd_nombre, M.ic_moneda, 'D' as tipoDocto "+
						"from com_documento D, com_solicitud S,comcat_moneda M, comvis_aforo CV, comcat_epo E , "+
						" comrel_producto_epo pe " +
						"where D.ic_documento = S.ic_documento "+
						"and D.ic_epo = CV.ic_epo "+
						" AND pe.ic_epo = D.ic_epo " +
						" AND pe.ic_producto_nafin = 1 " +
						" AND pe.ic_epo = E.ic_epo " +
						//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
            (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
            (Perfil.equals("ADMIN BANCOMEXT")?" and S.ic_banco_fondeo= 2":"") +
						//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
						"and D.ic_moneda = M.ic_moneda ");
        		if (!infoProcesadaEn.equals("")) // infoProcesadaEn puede tener "",5-Descuento Autom�tico,8-Operaci�n Telefonica,9-Cobranza Manual
          			qrySentencia.append(" and exists (select 1"   +
                        							" from com_docto_seleccionado ds"   +
                          							" where d.ic_documento=ds.ic_documento"   +
                          							" and ds.cc_acuse like ?) ");
			}
			else if(!in_tasa_aceptada_de.equals("")	&& !in_tasa_aceptada_a.equals("")){

				qrySentencia.append(
						"select "+hint+" count(*) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
						"sum(fn_monto_dscto) AS FN_MONTO_DSCTO, "+
        				"M.cd_nombre, M.ic_moneda, 'D' as tipoDocto "+
						"from com_documento D, com_docto_seleccionado DS, comcat_moneda M, comcat_epo E  " +
						" ,comvis_aforo CV, comrel_producto_epo pe "+
						"where D.ic_epo = CV.ic_epo " +
						" AND pe.ic_epo = D.ic_epo " +
						" AND pe.ic_epo = E.ic_epo " +
						//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
            (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
            (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
						" AND pe.ic_producto_nafin = 1 " );
						//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) ");
      			if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
        			qrySentencia.append(" and ds.cc_acuse like ? ");
			} else {

				qrySentencia.append(
						"select "+hint+" count(*) AS ConsDoctosNafinDE, sum(fn_monto) AS FN_MONTO, "+
						"sum(fn_monto_dscto) AS FN_MONTO_DSCTO, "+
          				"M.cd_nombre, M.ic_moneda, 'D' as tipoDocto "+
						"from com_documento D, comcat_moneda M, comvis_aforo CV, comrel_producto_epo pe, comcat_epo E "+
						" where D.ic_moneda = M.ic_moneda "+
						" and D.ic_epo = CV.ic_epo " +
						" AND pe.ic_epo = D.ic_epo " +
						" AND pe.ic_epo = E.ic_epo " +
						//" AND E.ic_banco_fondeo = " + ic_banco_fondeo + "" +
            (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
            (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
						" AND pe.ic_producto_nafin = 1 " );
						//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " );
          		if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
            		qrySentencia.append(" and exists (select 1"   +
                            						" from com_docto_seleccionado ds"   +
                            						" where d.ic_documento=ds.ic_documento"   +
                            						" and ds.cc_acuse like ?) ");
			}
			qrySentencia.append("and D.cs_dscto_especial != 'C' ");


			qrySentencia.append(condicion.toString() +
								" GROUP BY M.ic_moneda, M.cd_nombre"+
                                " order by M.ic_moneda ");
			//System.out.println("::QUERY DE CALCULATION "+qrySentencia);   

		} else {
			System.out.println("ic_estatus_docto value not handled:  " + ic_estatus_docto);
		}
		int iIndex = qrySentencia.toString().indexOf("where  and");
		if(iIndex > 0){
			String toWhere = qrySentencia.toString().substring(0, qrySentencia.toString().indexOf("where  and"));
			System.out.println(toWhere);
			qrySentencia = new StringBuffer( toWhere + "where" + qrySentencia.toString().substring(qrySentencia.toString().indexOf("where  and")+10,qrySentencia.length()) );
		}


		log.debug("..:: qrySentencia : "+qrySentencia.toString());
		log.debug("..:: ic_estatus_docto : "+ic_estatus_docto);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}

	public String getDocumentQuery(HttpServletRequest request){
		log.info("getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer(512); //512 Tama�o inicial

		String envia =(request.getParameter("envia") == null)? "": request.getParameter("envia");
		String ic_epo =(request.getParameter("ic_epo") == null)? "": request.getParameter("ic_epo");
		String ic_if =(request.getParameter("ic_if") == null)? "": request.getParameter("ic_if");
		String ic_pyme =(request.getParameter("ic_pyme") == null)? "": request.getParameter("ic_pyme");
		String ig_numero_docto =(request.getParameter("ig_numero_docto") == null)? "": request.getParameter("ig_numero_docto");
		String df_fecha_docto_de =(request.getParameter("df_fecha_docto_de") == null)	? "": request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a =	(request.getParameter("df_fecha_docto_a") == null)? "": request.getParameter("df_fecha_docto_a");

		String df_fecha_venc_de =	(request.getParameter("df_fecha_venc_de") == null)? "": request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a =(request.getParameter("df_fecha_venc_a") == null)? "": request.getParameter("df_fecha_venc_a");
		String ic_moneda =(request.getParameter("ic_moneda") == null)? "": request.getParameter("ic_moneda");
		String fn_monto_de =(request.getParameter("fn_monto_de") == null)? "": request.getParameter("fn_monto_de");
		String fn_monto_a =(request.getParameter("fn_monto_a") == null)? "": request.getParameter("fn_monto_a");
		String ic_estatus_docto =	(request.getParameter("ic_estatus_docto") == null)? "": request.getParameter("ic_estatus_docto");
		String df_fecha_seleccion_de =(request.getParameter("df_fecha_seleccion_de") == null)? "": request.getParameter("df_fecha_seleccion_de");
		String df_fecha_seleccion_a =(request.getParameter("df_fecha_seleccion_a") == null)? "": request.getParameter("df_fecha_seleccion_a");
		String in_tasa_aceptada_de =(request.getParameter("in_tasa_aceptada_de") == null)? "": request.getParameter("in_tasa_aceptada_de");
		String in_tasa_aceptada_a =(request.getParameter("in_tasa_aceptada_a") == null)? "": request.getParameter("in_tasa_aceptada_a");
		String ord_if =(request.getParameter("ord_if") == null)? "": request.getParameter("ord_if");
		String ord_pyme =(request.getParameter("ord_pyme") == null)? "": request.getParameter("ord_pyme");
		String ord_epo =(request.getParameter("ord_epo") == null)? "": request.getParameter("ord_epo");
		String ord_fec_venc =(request.getParameter("ord_fec_venc") == null)	? "": request.getParameter("ord_fec_venc");
		String operacion =(request.getParameter("operacion") == null)? "": request.getParameter("operacion");
		String tipoFactoraje =(request.getParameter("tipoFactoraje") == null)? "": request.getParameter("tipoFactoraje");
		// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
		String paginaNo =(request.getParameter("paginaNo") == null)? "1": request.getParameter("paginaNo");
		String nomArchivo =(request.getParameter("nomArchivo") == null)? "": request.getParameter("nomArchivo");
		String  ic_banco_fondeo =(request.getParameter("ic_banco_fondeo") == null)? "": request.getParameter("ic_banco_fondeo");
    String Perfil =(request.getParameter("Perfil") == null)? "": request.getParameter("Perfil");

    // MPCS.- Parametros para descuento automatico
		String  infoProcesadaEn =
			(request.getParameter("infoProcesadaEn") == null)
				? ""
				: request.getParameter("infoProcesadaEn");

		String campos1[] = { "nombreEpo", "D.DF_FECHA_VENC", "nombrePyme" };
		String orden1[] =  { ord_epo, ord_fec_venc, ord_pyme };
		String campos2[] = { "nombreEpo", "nombreIf", "D.DF_FECHA_VENC", "nombrePyme" };
		String orden2[] =  { ord_epo, ord_if, ord_fec_venc, ord_pyme };

		String 	numero_siaff   				= (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
		boolean 	estaHabilitadoNumeroSIAFF 	= (request.getAttribute("estaHabilitadoNumeroSIAFF") != null && request.getAttribute("estaHabilitadoNumeroSIAFF").equals("true"))?true:false;
		String ic_documento	 = "";

		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 			= (String) numero.get("IC_EPO");
			ic_documento	= (String) numero.get("IC_DOCUMENTO");
		}


		if (ic_estatus_docto.equals("")
			|| ic_estatus_docto.equals("1")
			|| ic_estatus_docto.equals("2")
			|| ic_estatus_docto.equals("5")
			|| ic_estatus_docto.equals("6")
			|| ic_estatus_docto.equals("7")
			|| ic_estatus_docto.equals("9")
			|| ic_estatus_docto.equals("10")
			|| ic_estatus_docto.equals("21")
			|| ic_estatus_docto.equals("23")
			|| ic_estatus_docto.equals("28")
			|| ic_estatus_docto.equals("29")
			|| ic_estatus_docto.equals("30")
			|| ic_estatus_docto.equals("31"))
		{

		log.debug("Paso x aqui 1.5--------------");
			qrySentencia.append(
					"select /*+ index (D IN_COM_DOCUMENTO_04_NUK) */ D.ic_documento  "
        			+ ",'ConsDoctosNafinDE::getDocumentQuery'"
					+ " from com_documento D, comrel_producto_epo pe, comcat_epo E "
					+ " where "
					+ " d.ic_epo = pe.ic_epo " +
					" AND pe.ic_producto_nafin = 1 " +
					" AND pe.ic_epo = E.ic_epo " +
          (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
          (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
					" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) ");

			if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
			  qrySentencia.append(" and exists (select 1"   +
                        "    from com_docto_seleccionado ds"   +
                        " 	 where d.ic_documento=ds.ic_documento"   +
                        " 	 and   ds.cc_acuse like ?) "  );
			if (!ic_epo.equals(""))
				qrySentencia.append(" and D.ic_epo = ?");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				qrySentencia.append(" and D.ic_pyme = ?");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				qrySentencia.append(" and D.ic_if = ?");
			if (!ig_numero_docto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ?");

			if (!df_fecha_docto_de.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO >= trunc(TO_DATE(?,'DD/MM/YYYY'))  ");
			}
			if (!df_fecha_docto_a.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals(""))
				qrySentencia.append(" and D.DF_FECHA_VENC >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.DF_FECHA_VENC < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ?");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				qrySentencia.append(" and D.fn_monto between ? and ?");
			if (!ic_estatus_docto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ?");
			else
      		{ if (infoProcesadaEn.equals("5")||infoProcesadaEn.equals("8")||infoProcesadaEn.equals("9"))
          		qrySentencia.append(" and D.ic_estatus_docto in (3,4,11) ");
        	  else
          		qrySentencia.append(" and D.ic_estatus_docto in(2,3) ");
      		}
			if (!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=?");

			// Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				qrySentencia.append(" AND D.ic_documento = ? ");
			}

			String criterioOrdenamiento =
				Comunes.ordenarCampos(orden1, campos1);
			if (!criterioOrdenamiento.equals(""))
				criterioOrdenamiento = " order by " + criterioOrdenamiento;

			qrySentencia.append(criterioOrdenamiento);
		}
		else if (  ic_estatus_docto.equals("3")
				|| ic_estatus_docto.equals("4")
				|| ic_estatus_docto.equals("11")
				|| ic_estatus_docto.equals("12")
				|| ic_estatus_docto.equals("16")
				|| ic_estatus_docto.equals("24")
				|| ic_estatus_docto.equals("26"))//FODEA 005 - 2009 ACF
		{
				log.debug("Paso x aqui 1.6 -------------");

				if(!in_tasa_aceptada_de.equals("")
					&& !in_tasa_aceptada_a.equals("")
					&& !df_fecha_seleccion_de.equals("")
					&& !df_fecha_seleccion_a.equals("")){

					qrySentencia.append(
							"select /*+ INDEX (s IN_COM_SOLICITUD_13_NUK) */ D.ic_documento " +
              			",'ConsDoctosNafinDE::getDocumentQuery'" +
							" from com_documento D, com_docto_seleccionado DS, comcat_epo E," +
							" com_solicitud S, comrel_producto_epo pe " +
							" where S.ic_documento = DS.ic_documento " +
							" and  D.ic_documento = S.ic_documento " +
							" AND D.ic_epo = pe.ic_epo " +
							" AND pe.ic_epo = E.ic_epo " +
              (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
              (Perfil.equals("ADMIN BANCOMEXT")?" and S.ic_banco_fondeo= 2":"") +
							" AND pe.ic_producto_nafin = 1 ");

          			if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
            			qrySentencia.append(" and ds.cc_acuse like ? ")  ;
				}
				else if(!df_fecha_seleccion_de.equals("")
					&& !df_fecha_seleccion_a.equals("")){
					qrySentencia.append(
							"select /*+ INDEX (s IN_COM_SOLICITUD_13_NUK) */  D.ic_documento "
              				+ ",'ConsDoctosNafinDE::getDocumentQuery'"
							+ " from com_documento D, com_solicitud S, comrel_producto_epo pe , comcat_epo E "
							+ " where D.ic_documento = S.ic_documento " +
							" AND D.ic_epo = pe.ic_epo " +
							" AND pe.ic_epo = E.ic_epo " +
              (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
              (Perfil.equals("ADMIN BANCOMEXT")?" and S.ic_banco_fondeo= 2":"") +
							" AND pe.ic_producto_nafin = 1 ");

            		if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
              		qrySentencia.append(" and exists (select 1"   +
                              "    from com_docto_seleccionado ds"   +
                              " 	 where d.ic_documento=ds.ic_documento"   +
                              " 	 and   ds.cc_acuse like ?) "  );
				}
				else if(!in_tasa_aceptada_de.equals("")
				&& !in_tasa_aceptada_a.equals("")){
					qrySentencia.append(
							"select /*+ index (D IN_COM_DOCUMENTO_04_NUK) */ D.ic_documento  "
              				+ ",'ConsDoctosNafinDE::getDocumentQuery'"
							+ " from com_documento D, com_docto_seleccionado DS, comrel_producto_epo pe ,comcat_epo E"
							+ " where d.ic_documento = DS.ic_documento " +
							" AND D.ic_epo = pe.ic_epo " +
							" AND D.ic_epo = E.ic_epo " +
              (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
              (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
							" AND pe.ic_producto_nafin = 1 ");
              		if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
                		qrySentencia.append(" and ds.cc_acuse like ? "  );
					}
					else{
					qrySentencia.append(
							"select /*+ index (D IN_COM_DOCUMENTO_04_NUK) */ D.ic_documento "
              				+ ",'ConsDoctosNafinDE::getDocumentQuery'"
							+ " from com_documento D, comrel_producto_epo pe, comcat_epo E "
							+ " where " +
							" D.ic_epo = pe.ic_epo " +
							" AND D.ic_epo = E.ic_epo " +
              (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
              (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
							" AND pe.ic_producto_nafin = 1 ");

		            if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
		              qrySentencia.append(" and exists (select 1"   +
                              "    from com_docto_seleccionado ds"   +
                              " 	 where d.ic_documento=ds.ic_documento"   +
                              " 	 and   ds.cc_acuse like ?) "  );
					}

			if (!ic_epo.equals(""))
				qrySentencia.append(" and D.ic_epo = ?");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				qrySentencia.append(" and D.ic_pyme = ?");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				qrySentencia.append(" and D.ic_if = ?");
			if (!ig_numero_docto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ?");

			if (!df_fecha_docto_de.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO >= TO_DATE(?,'DD/MM/YYYY')  ");
			}
			if (!df_fecha_docto_a.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO < TO_DATE(?,'DD/MM/YYYY') + 1 ");
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals(""))
				qrySentencia.append(" and D.DF_FECHA_VENC >= TO_DATE(?,'DD/MM/YYYY') and D.DF_FECHA_VENC < TO_DATE(?,'DD/MM/YYYY') + 1 ");
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ?");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				qrySentencia.append(" and D.fn_monto between ? and ?");


			if (!ic_estatus_docto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ?");
			if (!ic_estatus_docto.equals("3")
				&& !ic_estatus_docto.equals("24"))
			{
				if (!df_fecha_seleccion_de.equals("")
					&& !df_fecha_seleccion_a.equals("")){
					qrySentencia.append(" AND s.df_fecha_solicitud >= to_date(?,'dd/mm/yyyy') AND s.df_fecha_solicitud < to_date(?,'dd/mm/yyyy') + 1 ");
				}
			}
			if (!in_tasa_aceptada_de.equals("")
				&& !in_tasa_aceptada_a.equals(""))
				qrySentencia.append(" and DS.in_tasa_aceptada between ? and ?");
			if (!"".equals(tipoFactoraje))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=?");
			// Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				qrySentencia.append(" AND D.ic_documento = ? ");
			}

			String criterioOrdenamiento =
				Comunes.ordenarCampos(orden2, campos2);
			if (!criterioOrdenamiento.equals(""))
				criterioOrdenamiento = " order by " + criterioOrdenamiento;

			qrySentencia.append(criterioOrdenamiento);

		} else {
			System.out.println(
				"ic_estatus_docto value not handled???????:  " + ic_estatus_docto);
		}


		int iIndex = qrySentencia.indexOf("where  and");
		if(iIndex > 0){
			String toWhere = qrySentencia.substring(0,qrySentencia.indexOf("where  and"));
			System.out.println(toWhere);
			qrySentencia = new StringBuffer(toWhere + "where" + qrySentencia.substring(qrySentencia.indexOf("where  and")+10,qrySentencia.length()));
		}


		log.debug("..:: qrySentencia : "+qrySentencia.toString());
		log.debug("..:: ic_estatus_docto : "+ic_estatus_docto);

		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
  }

 	public String getDocumentQueryFile(HttpServletRequest request){
		log.info("getDocumentQueryFile(E)");

		StringBuffer qrySentencia = new StringBuffer(2048);  //2048 es el tama�o inicial del stringbuffer
		String envia =(request.getParameter("envia") == null)? "": request.getParameter("envia");
		String ic_epo =(request.getParameter("ic_epo") == null)? "": request.getParameter("ic_epo");
		String ic_if =(request.getParameter("ic_if") == null)? "": request.getParameter("ic_if");
		String ic_pyme =(request.getParameter("ic_pyme") == null)? "": request.getParameter("ic_pyme");
		String ig_numero_docto =(request.getParameter("ig_numero_docto") == null)? "": request.getParameter("ig_numero_docto");
		String df_fecha_docto_de =(request.getParameter("df_fecha_docto_de") == null)? "": request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a =(request.getParameter("df_fecha_docto_a") == null)? "": request.getParameter("df_fecha_docto_a");

		String df_fecha_venc_de =(request.getParameter("df_fecha_venc_de") == null)? "": request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a =(request.getParameter("df_fecha_venc_a") == null)? "": request.getParameter("df_fecha_venc_a");
		String ic_moneda =(request.getParameter("ic_moneda") == null)? "": request.getParameter("ic_moneda");
		String fn_monto_de =(request.getParameter("fn_monto_de") == null)? "": request.getParameter("fn_monto_de");
		String fn_monto_a =(request.getParameter("fn_monto_a") == null)? "": request.getParameter("fn_monto_a");
		String ic_estatus_docto =(request.getParameter("ic_estatus_docto") == null)? "": request.getParameter("ic_estatus_docto");
		String df_fecha_seleccion_de =(request.getParameter("df_fecha_seleccion_de") == null)? "": request.getParameter("df_fecha_seleccion_de");
		String df_fecha_seleccion_a =(request.getParameter("df_fecha_seleccion_a") == null)? "": request.getParameter("df_fecha_seleccion_a");
		String in_tasa_aceptada_de =(request.getParameter("in_tasa_aceptada_de") == null)? "": request.getParameter("in_tasa_aceptada_de");
		String in_tasa_aceptada_a =(request.getParameter("in_tasa_aceptada_a") == null)? "": request.getParameter("in_tasa_aceptada_a");
		String ord_if =(request.getParameter("ord_if") == null)? "": request.getParameter("ord_if");
		String ord_pyme =(request.getParameter("ord_pyme") == null)? "": request.getParameter("ord_pyme");
		String ord_epo =(request.getParameter("ord_epo") == null)? "": request.getParameter("ord_epo");
		String ord_fec_venc =(request.getParameter("ord_fec_venc") == null)? "": request.getParameter("ord_fec_venc");
		String operacion =(request.getParameter("operacion") == null)? "": request.getParameter("operacion");
		String tipoFactoraje =(request.getParameter("tipoFactoraje") == null)? "": request.getParameter("tipoFactoraje");
		// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
		String paginaNo =(request.getParameter("paginaNo") == null)? "1": request.getParameter("paginaNo");
		String nomArchivo =(request.getParameter("nomArchivo") == null)? "": request.getParameter("nomArchivo");
		String  ic_banco_fondeo =(request.getParameter("ic_banco_fondeo") == null)? "": request.getParameter("ic_banco_fondeo");
    String Perfil =(request.getParameter("Perfil") == null)? "": request.getParameter("Perfil");

		//campos de la parametrizacion de documentos
		String  consultaDoctos = "";
		consultaDoctos = ", TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO ";

    // MPCS.- Parametros para descuento automatico
		String  infoProcesadaEn =
			(request.getParameter("infoProcesadaEn") == null)
				? ""
				: request.getParameter("infoProcesadaEn");

		String campos1[] = { "nombreEpo", "D.DF_FECHA_VENC", "nombrePyme" };
		String orden1[] =  { ord_epo, ord_fec_venc, ord_pyme };
		String campos2[] = { "nombreEpo", "nombreIf", "D.DF_FECHA_VENC", "nombrePyme" };
		String orden2[] =  { ord_epo, ord_if, ord_fec_venc, ord_pyme };

		String 	numero_siaff   				= (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
		boolean 	estaHabilitadoNumeroSIAFF 	= (request.getAttribute("estaHabilitadoNumeroSIAFF") != null && request.getAttribute("estaHabilitadoNumeroSIAFF").equals("true"))?true:false;
		String ic_documento	 = "";

		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 			= (String) numero.get("IC_EPO");
			ic_documento	= (String) numero.get("IC_DOCUMENTO");
		}

		if (ic_estatus_docto.equals("")
			|| ic_estatus_docto.equals("1")
			|| ic_estatus_docto.equals("2")
			|| ic_estatus_docto.equals("5")
			|| ic_estatus_docto.equals("6")
			|| ic_estatus_docto.equals("7")
			|| ic_estatus_docto.equals("9")
			|| ic_estatus_docto.equals("10")
			|| ic_estatus_docto.equals("21")
			|| ic_estatus_docto.equals("23")
			|| ic_estatus_docto.equals("28")
			|| ic_estatus_docto.equals("29")
			|| ic_estatus_docto.equals("30")
			|| ic_estatus_docto.equals("31"))
		{
			log.debug("Paso x aqui 1.7 ----------");

			qrySentencia.append("SELECT /*+	INDEX(d IN_COM_DOCUMENTO_04_NUK)	USE_NL(pe cv e ed d m tf i i2 p) */ " +
				" (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) as nombreEpo, " +
				" (decode (P.cs_habilitado,'N','*','S',' ')||' '||(INITCAP(P.cg_razon_social))) as nombrePyme, " +
				" D.ig_numero_docto, TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') as DF_FECHA_DOCTO, " +
				" TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') as DF_FECHA_VENC, M.cd_nombre, D.fn_monto, decode(D.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl) as fn_aforo, " +
				" D.fn_monto_dscto, ED.cd_descripcion, D.ic_moneda, D.fn_porc_anticipo, D.ic_estatus_docto, " +
				" D.ic_documento, D.cs_cambio_importe, "+
				" (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as nombreIf, " +
				" D.CS_DSCTO_ESPECIAL, decode(d.cs_dscto_especial,'D',i2.cg_razon_social,'I',i2.cg_razon_social,'') as beneficiario, " +
		    " decode(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'') as fn_porc_beneficiario, " +
		    " decode(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'') as RECIBIR_BENEF, "+
		    " to_char(D.DF_ALTA, 'dd/mm/yyyy') df_alta, "+
		    " 'ConsDoctosNafinDE::getDocumentQueryFile' as Pantalla,P.cg_rfc as rfc, "+
			 " D.cc_acuse, D.ic_epo, " +
			 " tf.cg_nombre AS TIPO_FACTORAJE " +
			 ", ma.cg_razon_social as NOMBREMANDANTE "+
			 consultaDoctos +
				" FROM comrel_producto_epo pe, comvis_aforo cv, comcat_epo E, " +
				" comcat_estatus_docto ED, com_documento D, comcat_moneda M, " +
				" comcat_tipo_factoraje tf, comcat_mandante ma, " +
				" comcat_if I, comcat_if I2, comcat_pyme P "+
				" WHERE D.ic_epo = E.ic_epo "+
				" AND D.ic_epo = pe.ic_epo " +
				" AND pe.ic_producto_nafin = 1 " +
				" AND D.ic_epo = E.ic_epo " +
				" AND ma.IC_MANDANTE(+) = d.IC_MANDANTE " +
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
				" and D.ic_epo = cv.ic_epo "+
				" and D.ic_pyme = P.ic_pyme "+
				" and D.ic_moneda = M.ic_moneda "+
				" and D.ic_estatus_docto = ED.ic_estatus_docto "+
				" and D.ic_beneficiario = I2.ic_if (+)"+
				" and D.cs_dscto_especial = tf.cc_tipo_factoraje " +
				" and D.ic_if = I.ic_if(+)");
			System.out.print(qrySentencia);
			if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
			  qrySentencia.append(" and exists (select 1"   +
                        "    from com_docto_seleccionado ds"   +
                        " 	 where d.ic_documento=ds.ic_documento"   +
                        " 	 and   ds.cc_acuse like ?) ")  ;
			if (!ic_epo.equals(""))
				qrySentencia.append(" and D.ic_epo = ?");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				qrySentencia.append(" and D.ic_pyme = ?");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				qrySentencia.append(" and D.ic_if = ?");
			if (!ig_numero_docto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ?");

			if (!df_fecha_docto_de.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO >= trunc(TO_DATE(?,'DD/MM/YYYY'))  ");
			}
			if (!df_fecha_docto_a.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals(""))
				qrySentencia.append(" and D.DF_FECHA_VENC >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.DF_FECHA_VENC < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ?");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				qrySentencia.append(" and D.fn_monto between ? and ?");
			if (!ic_estatus_docto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ?");
			else
      		{ if (infoProcesadaEn.equals("5")||infoProcesadaEn.equals("8")||infoProcesadaEn.equals("9"))
          		qrySentencia.append(" and D.ic_estatus_docto in (3,4,11) ");
        	  else
          		qrySentencia.append(" and D.ic_estatus_docto in(2,3) ");
      		}
			if (!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=?");

			// Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				qrySentencia.append(" AND D.ic_documento = ? ");
			}

			if (!"".equals(validaDoctoEPO)   ) {
			    qrySentencia.append(" AND D.CS_VALIDACION_JUR = '" + this.validaDoctoEPO + "' ");
			}	
			String criterioOrdenamiento =
				Comunes.ordenarCampos(orden1, campos1);
			if (!criterioOrdenamiento.equals(""))
				criterioOrdenamiento = " order by " + criterioOrdenamiento;

			qrySentencia.append(criterioOrdenamiento);
		}
		else if (
			ic_estatus_docto.equals("3")
				|| ic_estatus_docto.equals("4")
				|| ic_estatus_docto.equals("11")
				|| ic_estatus_docto.equals("12")
				|| ic_estatus_docto.equals("16")
				|| ic_estatus_docto.equals("24")
				|| ic_estatus_docto.equals("26"))//FODEA 005 - 2009 ACF
		{
		log.debug("Paso x aqui 1.8 ----------");
		
			// Obtener condiciones de consulta del plazo
			String plazo 					= null;
			String condicionesPlazo 	= null;
			String tablasPlazo			= null;
			if(
				ic_estatus_docto.equals("3") 	// Seleccionada Pyme
					||
				ic_estatus_docto.equals("24")	// En proceso de Autorizacion IF
			){
			
				// Fodea 048 - 2012 -- Consulta Plazo, para doctos con estatus: 3 (Seleccionada Pyme) y 24 (En Proceso de Autorizacion IF)
				plazo 			= 
					" ( " +
						"  DECODE( PE.CS_FECHA_VENC_PYME, 'S', D.DF_FECHA_VENC_PYME, D.DF_FECHA_VENC )" + " - " +
						" 	(CASE  " +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
						"    TRUNC(SYSDATE) " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	END) " + 
					" ) " + 	
					" AS ig_plazo, ";
					
				condicionesPlazo =
						" 	AND D.ic_if                = iexp.ic_if  "  +
						" 	AND D.ic_epo               = iexp.ic_epo "  +
						" 	AND iexp.ic_producto_nafin = 1           ";
						/*
						+
						"  AND D.ic_epo               = pe.ic_epo   "  +
						"  AND pe.ic_producto_nafin   = 1           ";
						*/
						
				tablasPlazo		= 
						" , comrel_if_epo_x_producto iexp ";
						/*
						+
						" , comrel_producto_epo      pe   ";
						*/
						
			} else if( 
				ic_estatus_docto.equals("26") // Programado Pyme
			){ 
			
				// Fodea 048 - 2012 -- Consulta Plazo, para doctos con estatus: 26 (Programado Pyme)
				plazo 			=
					" ( "  +
						"  DECODE( PE.CS_FECHA_VENC_PYME, 'S', D.DF_FECHA_VENC_PYME, D.DF_FECHA_VENC )" + " - " +
						" 	(CASE  "  +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN "  +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN "  +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 2, '+') " +
						" 	END) "  + 
					" ) "  + 	
					" AS ig_plazo, ";
					
				condicionesPlazo =
						" 	AND D.ic_if                = iexp.ic_if  "  +
						" 	AND D.ic_epo               = iexp.ic_epo "  +
						" 	AND iexp.ic_producto_nafin = 1           ";
						/*
						 +
						"  AND D.ic_epo               = pe.ic_epo   "  +
						"  AND pe.ic_producto_nafin   = 1           ";
						*/
						
				tablasPlazo		= 
						" , comrel_if_epo_x_producto iexp " ;
						/*
						 + 
						 " , comrel_producto_epo      pe   ";
						*/
			} else {
				
				// " 	DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(DS.df_fecha_seleccion), 54, sigfechahabilxepo (E.ic_epo, TRUNC(DS.df_fecha_seleccion), 1, '+')), S.ig_plazo ) AS ig_plazo, "   +
				plazo 				= " S.ig_plazo AS ig_plazo, ";
				condicionesPlazo 	= null;
				tablasPlazo			= null;
			}
			
			qrySentencia.append(
				"  SELECT /*+"   +
				"         use_nl(D,DS,S,CC1,CD,P,E,I,I2,ED,M)"   +
				"      */"   +
				"    E.cg_razon_social nombreEpo,"   +
				"  	P.cg_razon_social nombrePyme,"   +
				"  	I.cg_razon_social AS nombreIf,"   +
				"  	D.ig_numero_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') AS df_fecha_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS df_fecha_venc,"   +
				"  	M.cd_nombre,"   +
				"  	D.fn_monto,"   +
				"  	D.fn_porc_anticipo,"   +
				"  	D.fn_monto_dscto,"   +
				"  	DS.in_tasa_aceptada,"   +
				"  	ED.cd_descripcion,"   +
				"  	TO_CHAR(S.df_fecha_solicitud,'DD/MM/YYYY') AS df_fecha_solicitud,"   +
				"  	D.ic_moneda, D.ic_documento,"   +
				"  	D.cs_cambio_importe,"   +
				"  	D.CS_DSCTO_ESPECIAL,"   +
				"  	DECODE(d.cs_dscto_especial,'D',i2.cg_razon_social,'I',i2.cg_razon_social,'') AS beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'') AS fn_porc_beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'') AS RECIBIR_BENEF,"   +
				"  	DECODE(d.cs_dscto_especial,'D',ds.in_importe_recibir - ds.fn_importe_recibir_benef,'I',ds.in_importe_recibir - ds.fn_importe_recibir_benef,'') AS NETO_REC_PYME,"   +
				"  	DS.cc_acuse,"   +
				"  	DS.in_importe_recibir,"   +
				"  	CASE WHEN cc1.fn_monto_pago IS NOT NULL THEN"   +
				"  		cc1.fn_monto_pago"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_monto_pago IS NOT NULL THEN"   +
				"  			cd.fn_monto_pago"   +
				"  		END"   +
				" 	END AS fn_monto_pago,"   +
				"  	CASE WHEN cc1.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  		cc1.fn_porc_docto_aplicado"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  			cd.fn_porc_docto_aplicado"   +
				"  		END"   +
				"  	END AS porcDoctoAplicado,"   +
				// " 	DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(DS.df_fecha_seleccion), 54, sigfechahabilxepo (E.ic_epo, TRUNC(DS.df_fecha_seleccion), 1, '+')), S.ig_plazo ) AS ig_plazo,"   +
				plazo +
				" 	DS.in_importe_interes,"   +
				"  	D.ic_pyme,"   +
				"   I.ig_tipo_piso, DS.fn_remanente, "+ //foda 015-2005
		    	"   to_char(D.DF_ALTA, 'dd/mm/yyyy') df_alta, "+
				"  	'ConsDoctosNafinDE::getDocumentQueryFile' AS pantalla,P.cg_rfc as rfc, "+
				" 		D.ic_epo, " +
				"	TO_CHAR(ds.df_programacion, 'dd/mm/yyyy') AS fecha_registro_operacion, "+//FODEA 005 - 2009 ACF
				"    tf.cg_nombre AS TIPO_FACTORAJE "+
				"  , ma.cg_razon_social as NOMBREMANDANTE " +
				consultaDoctos + "," +
				" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND d.ic_docto_asociado IS NOT NULL THEN 'true' ELSE 'false' END AS existeDocumentoAsociado ," +
				" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM com_documento WHERE ic_docto_asociado = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredSimpleApl, " +
				" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_NOTA_CREDITO = d.ic_documento) THEN 'true' ELSE 'false' END AS existeRefEnComrelNotaDocto, " +
				" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_DOCUMENTO = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredMultApl " +
				"  FROM"   +
				"  	com_documento D,              "   +
				"  	com_docto_seleccionado DS,"   +
				"  	com_solicitud S,"   +
				"  	(SELECT /*+index(cc cp_com_cobro_credito_pk)*/cc.ic_documento, cc.fn_porc_docto_aplicado ,"   +
				"  	SUM(cc.fn_monto_pago) AS fn_monto_pago"   +
				"  	FROM com_cobro_credito cc"   +
				"  	GROUP BY cc.ic_documento, cc.fn_porc_docto_aplicado) cc1,"   +
				"  	fop_cobro_disposicion cd,"   +
				"  	comcat_pyme P,"   +
				"     comcat_epo E,"   +
				"  	comcat_if I,"   +
				"  	comcat_if I2,"   +
				"  	comcat_estatus_docto ED,"   +
				"  	comcat_moneda M,"   +
				" 		comrel_producto_epo pe, " +
				"		COMCAT_TIPO_FACTORAJE tf " +
				"    , COMCAT_MANDANTE ma "	+
				(tablasPlazo != null?tablasPlazo:"") + // Fodea 048 - 2012 -- Tabla para asociada a la consulta del Plazo, estatus 3, 24 y 26.
				"  WHERE D.ic_epo = E.ic_epo"   +
				" 		AND D.ic_epo = pe.ic_epo " +
				"  AND pe.ic_epo = E.ic_epo " +
				"  AND ma.IC_MANDANTE(+) = d.IC_MANDANTE " +
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        (Perfil.equals("ADMIN BANCOMEXT")?" and S.ic_banco_fondeo= 2":"") +
				" 		AND pe.ic_producto_nafin = 1 " +
				"  	AND D.ic_pyme = P.ic_pyme"   +
				"  	AND D.ic_moneda = M.ic_moneda"   +
				"  	AND D.ic_documento = DS.ic_documento"   +
				"  	AND D.ic_estatus_docto = ED.ic_estatus_docto"   +
				"  	AND D.ic_documento = S.ic_documento(+)"   +
				"  	AND D.ic_documento = CD.ic_documento(+)"   +
				"  	AND D.ic_if = I.ic_if(+)"   +
				"  	AND D.ic_beneficiario = I2.ic_if (+)"   +
				"     AND D.cs_dscto_especial = tf.cc_tipo_factoraje " +
				"  	AND DS.ic_documento = CC1.ic_documento(+)" ) ;

     		if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
        		qrySentencia.append(" and ds.cc_acuse like ? ");
			if (!ic_epo.equals(""))
				qrySentencia.append(" and D.ic_epo = ?");
			if (!ic_pyme.equals("T") && !ic_pyme.equals(""))
				qrySentencia.append(" and D.ic_pyme = ?");
			if (!ic_if.equals("T") && !ic_if.equals(""))
				qrySentencia.append(" and D.ic_if = ?");
			if (!ig_numero_docto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ?");

			if (!df_fecha_docto_de.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO >= trunc(TO_DATE(?,'DD/MM/YYYY'))  ");
			}
			if (!df_fecha_docto_a.equals("")){
				qrySentencia.append(" and D.DF_FECHA_DOCTO < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
			}

			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals(""))
				qrySentencia.append(" and D.DF_FECHA_VENC >= TO_DATE(?,'DD/MM/YYYY') and D.DF_FECHA_VENC < TO_DATE(?,'DD/MM/YYYY') + 1 ");
			if (!ic_moneda.equals("T") && !ic_moneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ?");
			if (!fn_monto_de.equals("") && !fn_monto_a.equals(""))
				qrySentencia.append(" and D.fn_monto between ? and ?");


			if (!ic_estatus_docto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ?");
			if (!ic_estatus_docto.equals("3")
				&& !ic_estatus_docto.equals("24"))
			{
				if (!df_fecha_seleccion_de.equals("")
					&& !df_fecha_seleccion_a.equals(""))
					qrySentencia.append(
						" and S.df_fecha_solicitud >= TO_DATE(?,'DD/MM/YYYY') and S.df_fecha_solicitud < TO_DATE(?,'DD/MM/YYYY')+1 ");
			}
			if (!in_tasa_aceptada_de.equals("")
				&& !in_tasa_aceptada_a.equals(""))
				qrySentencia.append(" and DS.in_tasa_aceptada between ? and ?");
			if (!"".equals(tipoFactoraje))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=?");

			// Reubicar
			if(ic_documento!=null&&!ic_documento.equals("")){
				qrySentencia.append(" AND D.ic_documento = ? ");
			}
			// Fodea 048 - 2012 -- Condiciones para el Plazo, estatus 3, 24 y 26.
			if( condicionesPlazo != null ){
				qrySentencia.append(condicionesPlazo);
			}

			if (!"".equals(validaDoctoEPO)   ) {
			    qrySentencia.append(" AND D.CS_VALIDACION_JUR = '" + this.validaDoctoEPO + "' ");
			}
			String criterioOrdenamiento =
				Comunes.ordenarCampos(orden2, campos2);
			if (!criterioOrdenamiento.equals(""))
				criterioOrdenamiento = " order by " + criterioOrdenamiento;

			qrySentencia.append(criterioOrdenamiento);
			//System.out.println(qrySentencia);
		} else {
			System.out.println(
				"ic_estatus_docto value not handled:  " + ic_estatus_docto);
		}

		// this creates an "and" clause right after the where
		//qrySentencia = qrySentencia.replaceFirst("where and","where");
		int iIndex = qrySentencia.indexOf("where  and");
		if(iIndex > 0){
			String toWhere = qrySentencia.substring(0,qrySentencia.indexOf("where  and"));
			System.out.println(toWhere);
			qrySentencia = new StringBuffer(toWhere + "where" + qrySentencia.substring(qrySentencia.indexOf("where  and")+10,qrySentencia.length()));
		}

		log.debug("-------------query----2---------------------");
		log.debug("..:: qrySentencia : "+qrySentencia.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
  }

	/**
	 *
	 *
	 */

	public int getNumList(HttpServletRequest request){
		String ic_estatus_docto =(request.getParameter("ic_estatus_docto") == null)? "": request.getParameter("ic_estatus_docto");
		return this.numList;
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		String ic_estatus_docto =
			(request.getParameter("ic_estatus_docto") == null)
				? ""
				: request.getParameter("ic_estatus_docto");

		StringBuffer query = new StringBuffer();
		StringBuffer clavesDocumentos = new StringBuffer();

		for (Iterator it = ids.iterator(); it.hasNext();){
			it.next();
			clavesDocumentos.append("?,");
		}
		clavesDocumentos = clavesDocumentos.delete(clavesDocumentos.length()-1,
				clavesDocumentos.length());


		if (ic_estatus_docto.equals("")
			|| ic_estatus_docto.equals("1")
			|| ic_estatus_docto.equals("2")
			|| ic_estatus_docto.equals("5")
			|| ic_estatus_docto.equals("6")
			|| ic_estatus_docto.equals("7")
			|| ic_estatus_docto.equals("9")
			|| ic_estatus_docto.equals("10")
			|| ic_estatus_docto.equals("21")
			|| ic_estatus_docto.equals("23")
			|| ic_estatus_docto.equals("28")
			|| ic_estatus_docto.equals("29")
			|| ic_estatus_docto.equals("30")
			|| ic_estatus_docto.equals("31"))
		{
		log.debug("Paso x aqui 1.9 ---------");

			query.append(
				"select /*+ use_nl(d pe cv e p m i i2 tf ma ed) */ " +
                                        "(decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) as nombreEpo, "
					+ " (decode (P.cs_habilitado,'N','*','S',' ')||' '||(INITCAP(P.cg_razon_social))) as nombrePyme, "
					+ " D.ig_numero_docto, TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') as DF_FECHA_DOCTO, "
					+ " TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY'), M.cd_nombre, D.fn_monto, decode(D.ic_moneda,1,cv.fn_aforo,54,cv.fn_aforo_dl) as fn_aforo, "
					+ " D.fn_monto_dscto, ED.cd_descripcion, D.ic_moneda, D.fn_porc_anticipo, D.ic_estatus_docto, "
					+ " D.ic_documento, D.cs_cambio_importe, "
					+ " (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as nombreIf, "
					+ " D.CS_DSCTO_ESPECIAL, decode(d.cs_dscto_especial,'D',i2.cg_razon_social, 'I',i2.cg_razon_social,'') as beneficiario, "
					+ " decode(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'') as fn_porc_beneficiario ,"
					+ " decode(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'') as RECIBIR_BENEF "
					+ " ,to_char(D.DF_ALTA, 'dd/mm/yyyy') df_alta "
					+ " ,'consDoctosDE::getDocumentSummaryQueryForIds' as pantalla, "
					+ " D.cc_acuse, D.ic_epo, "
					+ " TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO, "
					+ " tf.cg_nombre as TIPO_FACTORAJE, "
					+ " ma.cg_razon_social as NOMBREMANDANTE "

					+ " from comcat_epo E, comcat_pyme P, com_documento D, comcat_moneda M,"
					+ " comcat_estatus_docto ED, comvis_aforo cv, comcat_if I, comcat_if I2, "
					+ " comrel_producto_epo pe, "
					+ " COMCAT_TIPO_FACTORAJE tf "
					+ " , COMCAT_MANDANTE ma "

					+ " where D.ic_epo = E.ic_epo "
					+ " AND D.ic_epo = pe.ic_epo "
					//+ " AND pe.ic_epo = E.ic_epo "
					+ " AND pe.ic_producto_nafin = 1 "
					+ " and D.ic_epo = cv.ic_epo "
					+ " and D.ic_pyme = P.ic_pyme "
					+ " and D.ic_moneda = M.ic_moneda "
					+ " and D.ic_estatus_docto = ED.ic_estatus_docto "
					+ " and D.ic_beneficiario = I2.ic_if (+)"
					+ " and D.ic_if = I.ic_if(+)"
					+ " and d.cs_dscto_especial = tf.cc_tipo_factoraje "
					+ " AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "
					+ " and D.ic_documento in (" + clavesDocumentos + ")");
			numList = 1;

		}
		else if (
			ic_estatus_docto.equals("3")
				|| ic_estatus_docto.equals("4")
				|| ic_estatus_docto.equals("11")
				|| ic_estatus_docto.equals("12")
				|| ic_estatus_docto.equals("16")
				|| ic_estatus_docto.equals("24")
				|| ic_estatus_docto.equals("26"))//FODEA 005 - 2009 ACF
		{
			log.debug("Paso x aqui 1.10----------");
			
			// Obtener condiciones de consulta del plazo
			String plazo 					= null;
			String condicionesPlazo 	= null;
			String tablasPlazo			= null;
			if(
				ic_estatus_docto.equals("3") 	// Seleccionada Pyme
					||
				ic_estatus_docto.equals("24")	// En proceso de Autorizacion IF
			){
				// Fodea 048 - 2012 -- Consulta Plazo, para doctos con estatus: 3 (Seleccionada Pyme) y 24 (En Proceso de Autorizacion IF)
				plazo 			= 
					" ( " +
						"  DECODE( PE.CS_FECHA_VENC_PYME, 'S', D.DF_FECHA_VENC_PYME, D.DF_FECHA_VENC )" + " - " +
						" 	(CASE  " +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
						"    TRUNC(SYSDATE) " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	END) " + 
					" ) " + 	
					" AS ig_plazo, ";
					
				condicionesPlazo =
						" 	AND D.ic_if                = iexp.ic_if  "  +
						" 	AND D.ic_epo               = iexp.ic_epo "  +
						" 	AND iexp.ic_producto_nafin = 1           "  +
						"  AND D.ic_epo               = pe.ic_epo   "  +
						"  AND pe.ic_producto_nafin   = 1           ";
						
				tablasPlazo		= 
						" , comrel_if_epo_x_producto iexp " +
						" , comrel_producto_epo      pe   ";
						
			} else if( 
				ic_estatus_docto.equals("26") // Programado Pyme
			){ 
			
				// Fodea 048 - 2012 -- Consulta Plazo, para doctos con estatus: 26 (Programado Pyme)
				plazo 			=
					" ( "  +
						"  DECODE( PE.CS_FECHA_VENC_PYME, 'S', D.DF_FECHA_VENC_PYME, D.DF_FECHA_VENC )" + " - " +
						" 	(CASE  "  +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN "  +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN "  +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 2, '+') " +
						" 	END) "  + 
					" ) "  + 	
					" AS ig_plazo, ";
					
				condicionesPlazo =
						" 	AND D.ic_if                = iexp.ic_if  "  +
						" 	AND D.ic_epo               = iexp.ic_epo "  +
						" 	AND iexp.ic_producto_nafin = 1           "  +
						"  AND D.ic_epo               = pe.ic_epo   "  +
						"  AND pe.ic_producto_nafin   = 1           ";
						
				tablasPlazo		= 
						" , comrel_if_epo_x_producto iexp " +
						" , comrel_producto_epo      pe   ";
						
			} else {
 
				// " 	DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(DS.df_fecha_seleccion), 54, sigfechahabilxepo (E.ic_epo, TRUNC(DS.df_fecha_seleccion), 1, '+')), S.ig_plazo ) AS ig_plazo, "   +
				plazo 				= " S.ig_plazo AS ig_plazo, ";
				condicionesPlazo 	= null;
				tablasPlazo			= null;
				
			}
			
			query.append(
						
				" SELECT /*+   leading(d)      use_nl(PE, TF, MA, D,DS,S,CC1,CD,P,E,I,I2,ED,M) */ "+
				" 	E.cg_razon_social nombreEpo,"   +
				"  	P.cg_razon_social nombrePyme,"   +
				"  	I.cg_razon_social AS nombreIf,"   +
				"  	D.ig_numero_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') AS df_fecha_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS df_fecha_venc,"   +
				"  	M.cd_nombre,"   +
				"  	D.fn_monto,"   +
				"  	D.fn_porc_anticipo,"   +
				"  	D.fn_monto_dscto,"   +
				"  	DS.in_tasa_aceptada,"   +
				"  	ED.cd_descripcion,"   +
				"  	TO_CHAR(S.df_fecha_solicitud,'DD/MM/YYYY') AS df_fecha_solicitud,"   +
				"  	D.ic_moneda, D.ic_documento,"   +
				"  	D.cs_cambio_importe,"   +
				"  	D.CS_DSCTO_ESPECIAL,"   +
				"  	DECODE(d.cs_dscto_especial,'D',i2.cg_razon_social,'I',i2.cg_razon_social,'') AS beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'I',d.fn_porc_beneficiario,'') AS fn_porc_beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'I',d.fn_monto * (d.fn_porc_beneficiario / 100),'') AS RECIBIR_BENEF,"   +
				"  	DECODE(d.cs_dscto_especial,'D',ds.in_importe_recibir - ds.fn_importe_recibir_benef,'I',ds.in_importe_recibir - ds.fn_importe_recibir_benef,'') AS NETO_REC_PYME,"   +
				"  	DS.cc_acuse,"   +
				"  	DS.in_importe_recibir,"   +
				"  	CASE WHEN cc1.fn_monto_pago IS NOT NULL THEN"   +
				"  		cc1.fn_monto_pago"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_monto_pago IS NOT NULL THEN"   +
				"  			cd.fn_monto_pago"   +
				"  		END"   +
				" 	END AS fn_monto_pago,"   +
				"  	CASE WHEN cc1.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  		cc1.fn_porc_docto_aplicado"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  			cd.fn_porc_docto_aplicado"   +
				"  		END"   +
				"  	END AS porcDoctoAplicado,"   +
				//" 	DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(DS.df_fecha_seleccion), 54, sigfechahabilxepo (E.ic_epo, TRUNC(DS.df_fecha_seleccion), 1, '+')), S.ig_plazo ) AS ig_plazo,"   +
				plazo  +
				" 	DS.in_importe_interes,"   +
				"  	D.ic_pyme,"   +
				"   I.ig_tipo_piso, DS.fn_remanente, "+ //foda 015-2005
		    	"   to_char(D.DF_ALTA, 'dd/mm/yyyy') df_alta, "+
				"  	'consDoctosNafinDE::getDocumentSummaryQueryForIds' AS pantalla, "   +
				" 		D.ic_epo, " +
				" 	TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO, " +
				"	TO_CHAR(ds.df_programacion, 'dd/mm/yyyy') AS fecha_registro_operacion, "+//FODEA 005 - 2009 ACF
				"  tf.cg_nombre as TIPO_FACTORAJE, " +
				" ma.cg_razon_social as NOMBREMANDANTE " +
				"  FROM comcat_epo E,"   +
				"  	comcat_pyme P,"   +
				"  	comcat_if I,"   +
				"  	comcat_if I2,"   +
				"  	com_documento D,"   +
				" 		COMCAT_TIPO_FACTORAJE tf, " +
				"  	comcat_moneda M,"   +
				"  	com_docto_seleccionado DS,"   +
				"  	comcat_estatus_docto ED,"   +
				"  	com_solicitud S,"   +
				"     COMCAT_MANDANTE ma,"+
				"  	(SELECT /*+index(cc cp_com_cobro_credito_pk)*/"   +
				"     cc.ic_documento, cc.fn_porc_docto_aplicado ,"   +
				"  	SUM(cc.fn_monto_pago) AS fn_monto_pago"   +
				"  	FROM com_cobro_credito cc"   +
				" 	where cc.ic_documento in (" + clavesDocumentos + ") " +
				"  	GROUP BY cc.ic_documento, cc.fn_porc_docto_aplicado) cc1,"   +
				"  	fop_cobro_disposicion cd "   +
				( tablasPlazo != null?tablasPlazo:"" ) +
				"  WHERE D.ic_epo = E.ic_epo"   +
				"  	AND D.ic_pyme = P.ic_pyme"   +
				"  	AND D.ic_moneda = M.ic_moneda"   +
				"  	AND D.ic_documento = DS.ic_documento"   +
				"  	AND D.ic_estatus_docto = ED.ic_estatus_docto"   +
				"  	AND D.ic_documento = S.ic_documento(+)"   +
				"  	AND D.ic_if = I.ic_if(+)"   +
				"  	AND D.ic_beneficiario = I2.ic_if (+)"   +
				"  	AND DS.ic_documento = CC1.ic_documento(+)"   +
				"  	AND DS.ic_documento = CD.ic_documento(+)"   +
				"     AND d.cs_dscto_especial = tf.cc_tipo_factoraje "+
				"     AND ma.IC_MANDANTE(+) = d.IC_MANDANTE " +
            " 		AND D.ic_documento in (" + clavesDocumentos + ") " +
            ( condicionesPlazo != null?condicionesPlazo:"" )
        );

			numList = 2;
			//System.out.println("ConsDoctosNafinDE::DOCUMENT SUMMARY FOR IDS:  "+query);

		} else {
			System.out.println("ic_estatus_docto value not handled:  " + ic_estatus_docto);
		}
	log.debug("-------------query-------1------------------");
		log.debug("..:: query : "+query.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
		return query.toString();
  }

 	 public HashMap getPartesDelNumeroSIAFF(String numeroSIAFF){
		log.info("getPartesDelNumeroSIAFF(E)");
		HashMap resultado = new HashMap();

		BigInteger	icEPO 		= new BigInteger("-1");
		BigInteger	icDocumento = new BigInteger("-1");

		if(numeroSIAFF != null && !numeroSIAFF.equals("") && numeroSIAFF.length() == 15){
			icEPO 		= new BigInteger(numeroSIAFF.substring(0,4));
			icDocumento = new BigInteger(numeroSIAFF.substring(4,15));
		}

		resultado.put("IC_EPO",			icEPO.toString());
		resultado.put("IC_DOCUMENTO",	icDocumento.toString());

		log.info("getPartesDelNumeroSIAFF(S)");
		return resultado;
	}


	public String getNumeroSIAFF(String ic_epo,String ic_documento){
		return getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(ic_documento);
	}

	public String getNumeroDeCuatroDigitos(String numero){

		String 			claveEPO 	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();

      for(int i = 0;i<(4-claveEPO.length());i++){
			resultado.append("0");
      }
      resultado.append(claveEPO);

      return resultado.toString();
  }

  public String getNumeroDeOnceDigitos(String numero){

		String 			claveDocto	= (numero == null )?"":numero.trim();
      StringBuffer 	resultado 	= new StringBuffer();

      for(int i = 0;i<(11-claveDocto.length());i++){
			resultado.append("0");
      }
      resultado.append(claveDocto);

      return resultado.toString();
  }

   public String getClaveEPO(String numeroSIAFF){
		BigInteger	icEPO 		= new BigInteger("-1");
		if(numeroSIAFF != null && !numeroSIAFF.equals("") && numeroSIAFF.length() == 15){
			icEPO 		= new BigInteger(numeroSIAFF.substring(0,4));
		}
		return icEPO.toString();
	}

	public void setNumList(int numList) {
		this.numList = numList;
	}


	public int get_numList() {
		return numList;
	}


	public void setEnvia(String envia) {
		this.envia = envia;
	}


	public String getEnvia() {
		return envia;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public String getIc_if() {
		return ic_if;
	}


	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}


	public String getIc_pyme() {
		return ic_pyme;
	}


	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}


	public String getIg_numero_docto() {
		return ig_numero_docto;
	}


	public void setDf_fecha_docto_de(String df_fecha_docto_de) {
		this.df_fecha_docto_de = df_fecha_docto_de;
	}


	public String getDf_fecha_docto_de() {
		return df_fecha_docto_de;
	}


	public void setDf_fecha_docto_a(String df_fecha_docto_a) {
		this.df_fecha_docto_a = df_fecha_docto_a;
	}


	public String getDf_fecha_docto_a() {
		return df_fecha_docto_a;
	}


	public void setDf_fecha_venc_de(String df_fecha_venc_de) {
		this.df_fecha_venc_de = df_fecha_venc_de;
	}


	public String getDf_fecha_venc_de() {
		return df_fecha_venc_de;
	}


	public void setDf_fecha_venc_a(String df_fecha_venc_a) {
		this.df_fecha_venc_a = df_fecha_venc_a;
	}


	public String getDf_fecha_venc_a() {
		return df_fecha_venc_a;
	}


	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}


	public String getIc_moneda() {
		return ic_moneda;
	}


	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}


	public String getFn_monto_de() {
		return fn_monto_de;
	}


	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}


	public String getFn_monto_a() {
		return fn_monto_a;
	}


	public void setIc_estatus_docto(String ic_estatus_docto) {
		this.ic_estatus_docto = ic_estatus_docto;
	}


	public String getIc_estatus_docto() {
		return ic_estatus_docto;
	}


	public void setDf_fecha_seleccion_de(String df_fecha_seleccion_de) {
		this.df_fecha_seleccion_de = df_fecha_seleccion_de;
	}


	public String getDf_fecha_seleccion_de() {
		return df_fecha_seleccion_de;
	}


	public void setDf_fecha_seleccion_a(String df_fecha_seleccion_a) {
		this.df_fecha_seleccion_a = df_fecha_seleccion_a;
	}


	public String getDf_fecha_seleccion_a() {
		return df_fecha_seleccion_a;
	}


	public void setIn_tasa_aceptada_de(String in_tasa_aceptada_de) {
		this.in_tasa_aceptada_de = in_tasa_aceptada_de;
	}


	public String getIn_tasa_aceptada_de() {
		return in_tasa_aceptada_de;
	}


	public void setIn_tasa_aceptada_a(String in_tasa_aceptada_a) {
		this.in_tasa_aceptada_a = in_tasa_aceptada_a;
	}


	public String getIn_tasa_aceptada_a() {
		return in_tasa_aceptada_a;
	}


	public void setOrd_if(String ord_if) {
		this.ord_if = ord_if;
	}


	public String getOrd_if() {
		return ord_if;
	}


	public void setOrd_pyme(String ord_pyme) {
		this.ord_pyme = ord_pyme;
	}


	public String getOrd_pyme() {
		return ord_pyme;
	}


	public void setOrd_epo(String ord_epo) {
		this.ord_epo = ord_epo;
	}


	public String getOrd_epo() {
		return ord_epo;
	}


	public void setOrd_fec_venc(String ord_fec_venc) {
		this.ord_fec_venc = ord_fec_venc;
	}


	public String getOrd_fec_venc() {
		return ord_fec_venc;
	}


	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}


	public String getOperacion() {
		return operacion;
	}


	public void setIc_banco_fondeo(String ic_banco_fondeo) {
		this.ic_banco_fondeo = ic_banco_fondeo;
	}


	public String getIc_banco_fondeo() {
		return ic_banco_fondeo;
	}


	public void setTipoFactoraje(String tipoFactoraje) {
		this.tipoFactoraje = tipoFactoraje;
	}


	public String getTipoFactoraje() {
		return tipoFactoraje;
	}


	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}


	public String getPaginaNo() {
		return paginaNo;
	}


	public void setPerfil(String Perfil) {
		this.Perfil = Perfil;
	}


	public String getPerfil() {
		return Perfil;
	}


	public void setNumero_siaff(String numero_siaff) {
		this.numero_siaff = numero_siaff;
	}


	public String getNumero_siaff() {
		return numero_siaff;
	}


	public void setEstaHabilitadoNumeroSIAFF(boolean estaHabilitadoNumeroSIAFF) {
		this.estaHabilitadoNumeroSIAFF = estaHabilitadoNumeroSIAFF;
	}


	public boolean isEstaHabilitadoNumeroSIAFF() {
		return estaHabilitadoNumeroSIAFF;
	}


	public void setIc_documento(String ic_documento) {
		this.ic_documento = ic_documento;
	}


	public String getIc_documento() {
		return ic_documento;
	}


	public void setConditions(List conditions) {
		this.conditions = conditions;
	}


	public List get_conditions() {
		return conditions;
	}


	public void setInfoProcesadaEn(String infoProcesadaEn) {
		this.infoProcesadaEn = infoProcesadaEn;
	}


	public String getInfoProcesadaEn() {
		return infoProcesadaEn;
	}
	
	public String getNumeroDoctoAsociado(String ic_nota_credito)
		throws Exception{

			System.out.println("13consulta2.jsp::getNumeroDoctoAsociado(E)");

			if(ic_nota_credito == null || ic_nota_credito.equals("")){
				System.out.println("13consulta2.jsp::getNumeroDoctoAsociado(S)");
				return "";
			}

			AccesoDB 		con 				= new AccesoDB();
			StringBuffer 	qrySentencia	= new StringBuffer();
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			String			resultado		= "";

			try {

				con.conexionDB();
				qrySentencia.append(
					"SELECT             "  +
					"  IG_NUMERO_DOCTO  "  +
					"FROM               "  +
					"COM_DOCUMENTO      "  +
					"WHERE              "  +
					"	IC_DOCUMENTO IN  "  +
					"		(             "  +
					"			SELECT IC_DOCTO_ASOCIADO FROM COM_DOCUMENTO WHERE IC_DOCUMENTO = ? "  +
					"     )             ");

				lVarBind.add(new Integer(ic_nota_credito));

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("IG_NUMERO_DOCTO");
					resultado = (resultado == null)?"":resultado;
				}

		}catch(Exception e){
			System.out.println("13consulta2.jsp::getNumeroDoctoAsociado(Exception)");
			System.out.println("13consulta2.jsp::getNumeroDoctoAsociado.ic_nota_credito = <"+ic_nota_credito+">");
			e.printStackTrace();
			throw new Exception("No se pudo consultar el documento especificado.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("13consulta2.jsp::getNumeroDoctoAsociado(S)");
		}

		return resultado;
	}
	public String getNumeroNotaCreditoAsociada(String ic_documento)
		throws Exception{

			System.out.println("13consulta2.jsp::getNumeroNotaCreditoAsociada(E)");

			if(ic_documento == null || ic_documento.equals("")){
				System.out.println("13consulta2.jsp::getNumeroNotaCreditoAsociada(S)");
				return "";
			}

			AccesoDB 		con 				= new AccesoDB();
			StringBuffer 	qrySentencia	= new StringBuffer();
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			String			resultado		= "";

			try {

				con.conexionDB();
				qrySentencia.append(
					"SELECT                  "  +
					"  IG_NUMERO_DOCTO       "  +
					"FROM                    "  +
					"  COM_DOCUMENTO         "  +
					"WHERE                   "  +
					"  IC_DOCTO_ASOCIADO = ? ");

				lVarBind.add(new Integer(ic_documento));

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("IG_NUMERO_DOCTO");
					resultado = (resultado == null)?"":resultado;
				}

		}catch(Exception e){
			System.out.println("13consulta2.jsp::getNumeroNotaCreditoAsociada(Exception)");
			System.out.println("13consulta2.jsp::getNumeroNotaCreditoAsociada.ic_documento = <"+ic_documento+">");
			e.printStackTrace();
			throw new Exception("No se pudo consultar el documento especificado.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("13consulta2.jsp::getNumeroNotaCreditoAsociada(S)");
		}

		return resultado;
	}


	public void setMandanteDoc(String mandanteDoc) {
		this.mandanteDoc = mandanteDoc;
	}


	public String getMandanteDoc() {
		return mandanteDoc;
	}
	
public void setOperaFideicomiso(String operaFideicomiso) {
		this.operaFideicomiso = operaFideicomiso;
	}


	public String getOperaFideicomiso() {
		return operaFideicomiso;
	}

	public String getStrPerfil() {
		return strPerfil;
	}

	public void setStrPerfil(String strPerfil) {
		this.strPerfil = strPerfil;
	}

	public String getDuplicado() {
		return duplicado;
	}

	public void setDuplicado(String duplicado) {
		this.duplicado = duplicado;
	}


    public void setValidaDoctoEPO(String validaDoctoEPO) {
	this.validaDoctoEPO = validaDoctoEPO;
    }

    public String getValidaDoctoEPO() {
	return validaDoctoEPO;
    }


	public void setCopade(String copade) {
		this.copade = copade;
	}

	public String getCopade() {
		return copade;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getContrato() {
		return contrato;
	}


}
