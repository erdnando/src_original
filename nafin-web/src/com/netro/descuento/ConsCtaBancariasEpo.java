package com.netro.descuento;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCtaBancariasEpo implements IQueryGeneratorRegExtJS {
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsCtaBancariasEpo.class);
	private String paginaOffset;
	private String paginaNo;
	private List 	conditions;
	StringBuffer	qrySentencia	= new StringBuffer("");
	private String ic_epo;
	private String ic_if;
	private String txtBanco;
	private String txtSucursal;
	private String txtCuenta;
	private String cboPlaza;
	private String txtCuentaClabe;
	private String noBancoFondeo;
	private String operanFideicomisoEPO;

	public ConsCtaBancariasEpo() {
	}

	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentQuery() {

		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(
			" SELECT relifepo.ic_if, relifepo.ic_epo, e.cg_razon_social, DECODE (if.cs_habilitado,'n','* ','s',' ')||' '||if.cg_razon_social AS nomif "+
			" FROM comcat_if if, comrel_if_epo relifepo, comcat_plaza cp, comcat_epo e "+
			" WHERE if.ic_if = relifepo.ic_if " +
			" AND e.ic_epo = relifepo.ic_epo " +
			" AND relifepo.ic_plaza = cp.ic_plaza (+) " 	);


		if("N".equals(operanFideicomisoEPO)	){
			qrySentencia.append( " AND relifepo.cg_num_cuenta IS NOT NULL ");
		}
		
		if( ic_epo != null &&	!"".equals(ic_epo)	){
			qrySentencia.append(" AND relifepo.ic_epo = ? ");
			conditions.add(ic_epo);
		}

		if( ic_if != null &&	!"".equals(ic_if)	){
			qrySentencia.append(" AND relifepo.ic_if = ? ");
			conditions.add(ic_if);
		}

		if(	txtBanco != null &&	!"".equals(txtBanco)	){
			qrySentencia.append(" AND relifepo.cg_banco = ? ");
			conditions.add(txtBanco);
		}

		if(	txtSucursal != null &&	!"".equals(txtSucursal)	){
			qrySentencia.append(" AND relifepo.ig_sucursal = ? ");
			conditions.add(txtSucursal);
		}

		if(	txtCuenta != null &&	!"".equals(txtCuenta)	){
			qrySentencia.append(" AND relifepo.cg_num_cuenta = ? ");
			conditions.add(txtCuenta);
		}

		if(	txtCuentaClabe != null &&	!"".equals(txtCuentaClabe)	){
			qrySentencia.append(" AND relifepo.cg_cuenta_clabe = ? ");
			conditions.add(txtCuentaClabe);
		}

		if(	cboPlaza != null &&	!"".equals(cboPlaza)		){
			qrySentencia.append(" AND cp.ic_plaza = ? ");
			conditions.add(cboPlaza);
		}

		if(	noBancoFondeo != null &&	!"".equals(noBancoFondeo)	){
			qrySentencia.append(" AND e.ic_banco_fondeo = ? ");
			conditions.add(noBancoFondeo);
		}

		qrySentencia.append(" ORDER BY 3,4 ");
		
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(List pageIds) { 
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		qrySentencia.append(
			" SELECT if.cs_tipo, "+ /*parte select*/
			" DECODE (if.cs_habilitado,'N','* ','S',' ')||' '||if.cg_razon_social AS nomif, "+
			" DECODE (relifepo.cg_banco,null,'Sin Banco',' ','Sin Banco', relifepo.cg_banco) AS nombanco, "+
			" DECODE (relifepo.cg_num_cuenta, null,'Sin Cuenta', ' ','Sin Cuenta',relifepo.cg_num_cuenta) AS nocta, "+
			" DECODE (relifepo.cs_aceptacion, null,'N', relifepo.cs_aceptacion) AS aceptacion, "+
			" DECODE (cp.cd_descripcion, null, 'Sin Plaza', cp.cd_descripcion ||','||cp.cg_estado) AS plaza, "+
			" DECODE (relifepo.ig_sucursal, null,'Sin Sucursal',' ','Sin Sucursal',relifepo.ig_sucursal ) AS sucursal, "+
			" e.cg_razon_social AS nomepo, relifepo.ic_epo, relifepo.ic_if, "+
			" DECODE (relifepo.cg_cuenta_clabe, null,'Sin Cuenta',relifepo.cg_cuenta_clabe) AS clabe, "+
			" DECODE (relifepo.cs_autorizacion_clabe, null,'N', relifepo.cs_autorizacion_clabe) AS autoriza , "+
			" 'N' as OPERA_FIDEICOMISO_IF  " +
			" FROM comcat_if if, comrel_if_epo relifepo, comcat_plaza cp, comcat_epo e "+
			" WHERE if.ic_if = relifepo.ic_if " +
			" AND e.ic_epo = relifepo.ic_epo " +
			" AND relifepo.ic_plaza = cp.ic_plaza (+) " );

		if("N".equals(operanFideicomisoEPO)	){
			qrySentencia.append( " AND relifepo.cg_num_cuenta IS NOT NULL ");
		}
		
		if( ic_epo != null &&	!"".equals(ic_epo)	){
			qrySentencia.append(" AND relifepo.ic_epo = ? ");
			conditions.add(ic_epo);
		}

		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i==0) {
				qrySentencia.append("  AND  ( "); 
			} else {
				qrySentencia.append("  OR  "); 
			}
			qrySentencia.append(" ( relifepo.ic_if = ? AND relifepo.ic_epo = ? ) "); 
			conditions.add(new Long(lItem.get(0).toString()));
			conditions.add(new Long(lItem.get(1).toString()));
		}//for(int i=0;i<ids.size();i++)
		qrySentencia.append("  ) ");

		qrySentencia.append(" ORDER BY 8,2 ");

		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		return "";
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {		
		return "";
	}

	public void setPaginaOffset(String paginaOffset) {
		this.paginaOffset = paginaOffset;
	}

	public String getPaginaOffset() {
		return paginaOffset;
	}

	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}

	public String getPaginaNo() {
		return paginaNo;
	}

	public void setClaveEpo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getClaveEpo() {
		return ic_epo;
	}

	public void setClaveIf(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getClaveIf() {
		return ic_if;
	}

	public void setBanco(String txtBanco) {
		this.txtBanco = txtBanco;
	}

	public String getBanco() {
		return txtBanco;
	}

	public void setSucursal(String txtSucursal){
		this.txtSucursal = txtSucursal;
	}

	public String getSucursal(){
		return txtSucursal;
	}

	public void setCuenta(String txtCuenta){
		this.txtCuenta = txtCuenta;
	}

	public String getCuenta(){
		return txtCuenta;
	}

	public void setPlaza(String cboPlaza){
		this.cboPlaza = cboPlaza;
	}

	public String getPlaza(){
		return cboPlaza;
	}

	public void setCuentaClabe(String clabe){
		this.txtCuentaClabe = clabe;
	}
	
	public String getCuentaClabe(){
		return txtCuentaClabe;
	}
	
	public void setBancoFondeo(String noBancoFondeo){
		this.noBancoFondeo = noBancoFondeo;
	}

	public String getBancoFondeo(){
		return noBancoFondeo;
	}

	public List getConditions(){
		return this.conditions;
	}

	public String getOperanFideicomisoEPO() {
		return operanFideicomisoEPO;
	}

	public void setOperanFideicomisoEPO(String operanFideicomisoEPO) {
		this.operanFideicomisoEPO = operanFideicomisoEPO;
	}

}