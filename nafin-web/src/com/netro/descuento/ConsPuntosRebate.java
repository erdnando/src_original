package com.netro.descuento;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 *
 * @author Andrea Isabel Luna Aguill�n
 */

public class ConsPuntosRebate implements IQueryGeneratorRegExtJS  {
	public ConsPuntosRebate() {	}
	
	private List conditions;
	StringBuffer strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsPuntosRebate.class);//Variable para enviar mensajes
	
	private String claveEPO;
	
	public String getAggregateCalculationQuery() {
		return "";
	}
	public String getDocumentQuery() {
		conditions	= new ArrayList();
		strQuery 	= new StringBuffer();
		return strQuery.toString();
	}
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		return strQuery.toString();
	}
	public String getDocumentQueryFile() {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		
		log.debug("ic_epo= " + this.claveEPO);
		
		strQuery.append("SELECT ie.ic_if, ie.ic_producto_nafin, i.cg_razon_social," +
											"fg_puntos_rebate, '' AS puntos_rebate" +
								" FROM comcat_if i," +
										"comrel_tasa_autorizada ta," +
										"comcat_tasa t," +
										"comrel_if_epo_moneda iem," +
										"(SELECT   ic_tasa, MAX (dc_fecha) AS dc_fecha" +
											" FROM com_mant_tasa" +
										" GROUP BY ic_tasa) x," +
										"(SELECT   tb.ic_epo, tb.ic_tasa," +
													"MAX (tb.dc_fecha_tasa) AS dc_fecha_tasa" +
											" FROM comrel_tasa_base tb" +
												" WHERE tb.ic_epo = ? AND tb.cs_tasa_activa = 'S'" +
											" GROUP BY tb.ic_epo, tb.ic_tasa) tbu," +
											"comrel_tasa_base tb," +
											"com_mant_tasa mt," +
											"comrel_if_epo_x_producto ie" +
									" WHERE t.ic_tasa = x.ic_tasa" +
									" AND t.cs_disponible = 'S'" +
									" AND ta.ic_epo = ?" +
									" AND ta.ic_if = i.ic_if" +
									" AND ta.ic_epo = tbu.ic_epo" +
									" AND ta.dc_fecha_tasa = tbu.dc_fecha_tasa" +
									" AND tbu.ic_tasa = t.ic_tasa" +
									" AND iem.ic_epo = ta.ic_epo" +
									" AND iem.ic_if = ta.ic_if" +
									" AND i.cs_habilitado = 'S'" +
									" AND tbu.ic_tasa = tb.ic_tasa" +
									" AND tb.dc_fecha_tasa = tbu.dc_fecha_tasa" +
									" AND mt.dc_fecha = x.dc_fecha" +
									" AND t.ic_tasa = mt.ic_tasa" +
									" AND iem.ic_epo = ie.ic_epo" +
									" AND iem.ic_if = ie.ic_if" +
									" AND ie.ic_producto_nafin = 1" +
								" GROUP BY ie.ic_if, ie.ic_producto_nafin, i.cg_razon_social, fg_puntos_rebate");
		conditions.add(this.claveEPO);
		conditions.add(this.claveEPO);
		
		log.debug("getDocumentQueryFile strQuery = " + strQuery.toString());
		log.debug("getDocumentQueryFile conditions = " + conditions);
		return strQuery.toString();
	}
	/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	 public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs,String path,String tipo) {
		String nombreArchivo = "";
		return nombreArchivo;
	 }
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	 public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		return "";
	 }
	 public List getConditions() {
		return conditions;
	 }
	 public String getClaveEPO() {
		return claveEPO;
	 }
	 public void setClaveEPO(String claveEPO) {
		this.claveEPO = claveEPO;
	 }
}