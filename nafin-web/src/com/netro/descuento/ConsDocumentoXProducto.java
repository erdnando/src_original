package com.netro.descuento;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 *
 * @author Andrea Isabel Luna Aguill�n
 */
public class ConsDocumentoXProducto implements IQueryGeneratorRegExtJS {
	public ConsDocumentoXProducto() {	}
	
	private List conditions;
	StringBuffer strQuery;
	private String claveEPO;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsDocumentoXProducto.class);//Variables para enviar mensajes al Log.
	public String getAggregateCalculationQuery() {
		return "";
	}
	public String getDocumentQuery(){
		conditions 	= new ArrayList();
		strQuery		= new StringBuffer();
		return strQuery.toString();
	}
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer();
		return strQuery.toString();
	}
	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		strQuery = new StringBuffer();
			strQuery.append("SELECT   epo.cg_nombre_comercial, prod.ic_nombre, docto.cd_descripcion");
			strQuery.append(" FROM comcat_clase_docto docto,");
			strQuery.append("			comcat_epo epo,");
         strQuery.append("			comcat_producto_nafin prod,");
         strQuery.append("			comrel_producto_docto relprodocto");
			strQuery.append("	WHERE docto.ic_clase_docto = relprodocto.ic_clase_docto");
			strQuery.append("		AND prod.ic_producto_nafin = relprodocto.ic_producto_nafin");
			strQuery.append("		AND epo.ic_epo = relprodocto.ic_epo");
			strQuery.append("		AND epo.ic_epo = ? ORDER BY 1");
			conditions.add(this.claveEPO);
		return strQuery.toString();
	}
		/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)  {
		String nombreArchivo = "";
		return nombreArchivo;
	}
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}	
	public List getConditions(){
		return conditions;
	}
	public void setClaveEPO(String claveEPO) {
		this.claveEPO = claveEPO;
	}
	public String getClaveEPO() {
		return claveEPO;
	}
}	