package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Clase para la pantalla de Admin IF Descuesto Electornico - Capturas -Liberacion de Limites IF
 * (Captura y Consulta)
 * autor:Deysi Laura Hern�ndez Contreras
 */
public class LiberacionLimitesIF   implements IQueryGeneratorRegExtJS {

	private ArrayList conditions = null;
	private final static Log log = ServiceLocator.getInstance().getLog(LiberacionLimitesIF.class);
	private String fechaIni;
	private String fechaFin;
	private String claveEPO;
	private String claveIF;
	private String clavePyme;
	private String fechaHoy;
	private String tipoUsuario;
	private String documentos;
	private String estatus;
	private String pantalla;
	

	public LiberacionLimitesIF() {	}	
  /**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  

  
  	public String getAggregateCalculationQuery() {
		
		log.info("getAggregateCalculationQuery(E) ::..");
		this.conditions = new ArrayList();
		StringBuffer queryCons = new StringBuffer();

		

		log.debug("lsQrySentencia: "+queryCons);
		log.debug("conditions::"+this.conditions);
		log.info("getAggregateCalculationQuery(S) ::..");
		return queryCons.toString();
	}
	
  
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E) ::..");
		this.conditions = new ArrayList();
		StringBuffer queryCons = new StringBuffer();

		

		log.debug("lsQrySentencia: "+queryCons);
		log.debug("conditions::"+this.conditions);
		log.info("getDocumentQuery(S) ::..");
		return queryCons.toString();
	}

  
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		StringBuffer queryCons 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		
		log.debug("..:: qrySentencia "+queryCons.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return queryCons.toString();
	}
	
	  
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		StringBuffer queryCons 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		queryCons.append( " select  /*+ index(d IN_COM_DOCUMENTO_09_NUK) use_nl(d s p i m) */  "+
							  " i.ic_if  as  IC_IF , "+
								" i.cg_razon_social as NOMBRE_IF ,  "+
								" d.ic_moneda  as IC_MONEDA, " +
								" m.cd_nombre AS MONEDA, " +
								" d.ic_documento as IC_DOCUMENTO,"+											
								" d.fn_monto_dscto as MONTO_DESC, "+
								" d.ic_epo AS IC_EPO , " +										
								" d.ig_numero_docto  AS NUM_DOCTO , ");
								if (tipoUsuario.equals("NAFIN")) {
									queryCons.append(  " p.cg_razon_social as NOMBRE_PYME,  ");
								}else  {
									queryCons.append( 	" decode(ds.cs_opera_fiso,'N',p.cg_razon_social,'S',i2.cg_razon_social) as NOMBRE_PYME, ");
								}
							queryCons.append( " s.FG_TASA_FONDEO_NAFIN as TASA_FONDEO, "+
								" s.IG_NUMERO_PRESTAMO as NUMERO_PRESTAMO, "+
								" TO_CHAR(d.df_fecha_venc, 'DD/MM/YYYY') as FECHA_VENC , "+
								" decode(ds.cs_opera_fiso,'N',' ','S',p.cg_razon_social) as REFERENCIA , "+
								" decode(CS_OPERA_FISO, 'S', i2.cg_razon_social , 'N', 'N/A') as NOMBRE_FIDEICOMISO  ");
								
						if (pantalla.equals("Consulta")){
							queryCons.append(" , TO_CHAR (b.df_fecha_prepago, 'DD/MM/YYYY') AS FECHA_PREPAGO ");
						}
						queryCons.append(" from com_documento d, "+
								" comcat_if i, "+
								" comcat_moneda m, "+
								" com_solicitud s, " +
								" comcat_pyme p , "+
								" COM_DOCTO_SELECCIONADO ds, " +
								" comcat_if i2 ");
								
						
								
					if (pantalla.equals("Consulta")){	
						queryCons.append(", bit_bitacora_libif b ");
					}
								
					queryCons.append(" where d.ic_if = i.ic_if " +															
								" and m.ic_moneda = d.ic_moneda "+
								" and s.ic_documento = d.ic_documento "+
								"  and d.ic_pyme = p.ic_pyme ");											 
		
		 queryCons.append( " AND ds.ic_documento = d.ic_documento  "+
								 " and ds.ic_if = i2.ic_if(+)    ");
	
		if (pantalla.equals("Consulta")){	
			queryCons.append(" AND d.ic_documento = b.ic_documento ");
		}
		
		if (estatus!=null && !estatus.equals("")){
			queryCons.append("  and ic_estatus_docto = ?  ");
			conditions.add( estatus );		
		} 
		
		if (documentos!=null && !documentos.equals("")){
			queryCons.append(" and d.ic_documento  in( "+documentos+") ");				
		}  
		
		
		if (fechaIni!=null && !fechaIni.equals("")){
			queryCons.append(" and d.df_fecha_venc >= trunc(to_date( ? ,'dd/mm/yyyy')) ");
			conditions.add( (!fechaIni.equals("")?fechaIni:fechaHoy) );			
		}
		
		if (fechaFin!=null && !fechaFin.equals("")){
			queryCons.append(" and d.df_fecha_venc < trunc(to_date( ? ,'dd/mm/yyyy')+1) ");
			conditions.add( (!fechaFin.equals("")?fechaFin:fechaHoy) );			
		}
		
	
		if (claveEPO!=null && !claveEPO.equals("")){
			queryCons.append("  AND	d.ic_epo = ? ");
			conditions.add(claveEPO);
		}		
		
		if (tipoUsuario.equals("NAFIN")) {
			if(!claveIF.equals("") ){
				queryCons.append(" and d.ic_if = ? ");
				conditions.add(claveIF);
			}else{
				queryCons.append(" and d.ic_if in ("+
                             " SELECT /* + index( ci CP_COMCAT_IF_PK) use_nl(ci rie epo cbf )  */  "+
									  "	DISTINCT ci.ic_if "+
                             "  FROM comcat_if ci , "+
                             "       comrel_if_epo_x_producto rie, "+
                             "       comcat_epo epo, "+
                             "       comcat_banco_fondeo cbf"+
                             "  WHERE  ci.cs_habilitado = 'S'"+
                             "  AND rie.ic_if = ci.ic_if"+
                             "  AND	rie.ic_epo = epo.ic_epo");
            if(!claveEPO.equals("")){
					queryCons.append("  AND	rie.ic_epo = ? ");
					conditions.add(claveEPO);
				}
            queryCons.append("  AND epo.ic_banco_fondeo = cbf.ic_banco_fondeo"+
									  "  AND  rie.ic_producto_nafin = 1 )");				
			}
		}else { 
			queryCons.append(" and d.ic_if = ? ");
			conditions.add(claveIF);	
		}
		
		if(!clavePyme.equals("")){
			queryCons.append(" and p.ic_pyme = ? ");
			conditions.add(clavePyme);
		}
		
		
		
		log.debug("..:: qrySentencia "+queryCons.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQueryFile(S)");
		return queryCons.toString();
	}
	
	

  
  /**
	 * metodo que genera archivo por pagina
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		return "";
	}
  
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipoArchivo) {
		String  nombreArchivo ="";	
		ComunesPDF pdfDoc = new ComunesPDF();
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo creaArchivo = new CreaArchivo();
		String fecha_prepago ="", nombreFideicomiso ="";
		
		try {		
		
			HttpSession session = request.getSession();
			if(tipoArchivo.equals("pdf")) { 
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				if (tipoUsuario.equals("IF")) {		
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				}else if (tipoUsuario.equals("NAFIN")) { 
				
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				}
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);		
				if (tipoUsuario.equals("IF")) {	
					pdfDoc.addText("Se realiz� la liberaci�n de los vencimientos  ","formas",ComunesPDF.CENTER);	
				}else if (tipoUsuario.equals("NAFIN")) { 
					pdfDoc.addText("Se realiz� la liberaci�n de los vencimientos de los siguientes Intermediarios Financieros: ","formas",ComunesPDF.CENTER);	
				}
				
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
				
				if (tipoUsuario.equals("NAFIN")) { 
					pdfDoc.setTable(9, 80);
					pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
				}else  {
					pdfDoc.setTable(8, 80);
				}
				pdfDoc.setCell("Pyme","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa de Fondeo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Prestamo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				if (tipoUsuario.equals("NAFIN")) { 
					pdfDoc.setCell("Nombre FIDEICOMISO","celda01",ComunesPDF.CENTER);				
				}
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);		
				if (tipoUsuario.equals("IF")) { 
					pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);				
				}		
			
			}	
			
			if(tipoArchivo.equals("csv")) { 
			
				if (tipoUsuario.equals("NAFIN")) { 
					contenidoArchivo.append(" Intermediario Financiero " + ",");
				}
				contenidoArchivo.append(" Pyme " + ","+
												" N�mero de documento " + ","+
												" Tasa de Fondeo " + ","+
												" N�mero de Prestamo " + ","+
												" Moneda " + ",");
				if (tipoUsuario.equals("NAFIN")) { 
					contenidoArchivo.append(" Nombre FIDEICOMISO " + ",");
				}
				contenidoArchivo.append(" Monto a Descontar " + ","+
												" Fecha de vencimiento ,  " +
												" Fecha de Prepago " );			
		
				if (tipoUsuario.equals("IF")) { 
					contenidoArchivo.append(", Referencia ");
				}	
			
				contenidoArchivo.append(" \n");
			}							
			
			while (rs.next()) {
			
				String nombreIF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
				String nombrePyme = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
				String num_docto  = (rs.getString("NUM_DOCTO") == null) ? "" : rs.getString("NUM_DOCTO");
				String tasaFondeo = (rs.getString("TASA_FONDEO") == null) ? "" : rs.getString("TASA_FONDEO");
				String num_prestamo  = (rs.getString("NUMERO_PRESTAMO") == null) ? "" : rs.getString("NUMERO_PRESTAMO");
				String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
				String monto_desc = (rs.getString("MONTO_DESC") == null) ? "" : rs.getString("MONTO_DESC");
				String fecha_venc = (rs.getString("FECHA_VENC") == null) ? "" : rs.getString("FECHA_VENC");
				String  referencia = (rs.getString("REFERENCIA") == null) ? "" : rs.getString("REFERENCIA"); 
				
				if (pantalla.equals("Consulta")){
				 fecha_prepago = (rs.getString("FECHA_PREPAGO") == null) ? "" : rs.getString("FECHA_PREPAGO");
				}
				nombreFideicomiso = (rs.getString("NOMBRE_FIDEICOMISO") == null) ? "" : rs.getString("NOMBRE_FIDEICOMISO");
				
				if(tipoArchivo.equals("pdf")) { 
					if (tipoUsuario.equals("NAFIN")) { 
						pdfDoc.setCell(nombreIF,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setCell(nombrePyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tasaFondeo+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_prestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);					
					if (tipoUsuario.equals("NAFIN")) { 
						pdfDoc.setCell(nombreFideicomiso,"formas",ComunesPDF.CENTER);
					}
					
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(fecha_venc,"formas",ComunesPDF.CENTER);		
					if (tipoUsuario.equals("IF")) { 
						pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
					}				
				
				}
				
				if(tipoArchivo.equals("csv")) { 
				
					if (tipoUsuario.equals("NAFIN")) { 
						contenidoArchivo.append(nombreIF.replaceAll(",", "")+ ",");					
					}
					contenidoArchivo.append(nombrePyme.replaceAll(",", "")+ ","+
													num_docto.replaceAll(",", "")+ ","+																	
													tasaFondeo+"%,"+
													num_prestamo.replaceAll(",", "")+ ","+
													moneda.replaceAll(",", "")+ ",");
					if (tipoUsuario.equals("NAFIN")) { 
						contenidoArchivo.append(nombreFideicomiso.replaceAll(",", "")+ ",");
					}
					contenidoArchivo.append(monto_desc.replaceAll(",", "")+ ","+
													fecha_venc.replaceAll(",", "")+ ","+ 
													fecha_prepago.replaceAll(",", ""));
				
					if (tipoUsuario.equals("IF")) { 
						contenidoArchivo.append(" , "+referencia.replaceAll(",", ""));
					}	
				
					contenidoArchivo.append(" \n");
				}
				
			}
			if(tipoArchivo.equals("pdf")) { 	
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}
			
			if(tipoArchivo.equals("csv")) { 	
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
			}	
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
		
				
	}

	public String getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	public String getClaveEPO() {
		return claveEPO;
	}

	public void setClaveEPO(String claveEPO) {
		this.claveEPO = claveEPO;
	}

	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	public String getClavePyme() {
		return clavePyme;
	}

	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}

	public String getFechaHoy() {
		return fechaHoy;
	}

	public void setFechaHoy(String fechaHoy) {
		this.fechaHoy = fechaHoy;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getDocumentos() {
		return documentos;
	}

	public void setDocumentos(String documentos) {
		this.documentos = documentos;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	    
public List  getCatalogoIF(String ic_epo ){
	AccesoDB con = new AccesoDB();
	
	HashMap datos =  new HashMap();
	List registros  = new ArrayList();
	PreparedStatement ps	= null;
	ResultSet rs = null;	
	StringBuffer qrySentencia = new StringBuffer("");
	List lVarBind		= new ArrayList();
		
	try{
		con.conexionDB();
						
		lVarBind		= new ArrayList();				
		qrySentencia.append( " SELECT DISTINCT ci.ic_if,  ci.cg_razon_social "+
									"  FROM  comcat_if ci , comrel_if_epo_x_producto rie,  "+
									"        comcat_epo epo, comcat_banco_fondeo cbf"+
									"  WHERE ci.cs_habilitado = 'S'"+
									"  AND   rie.ic_if = ci.ic_if"+
									"  AND	rie.ic_epo = epo.ic_epo"+
									"  AND  epo.ic_banco_fondeo = cbf.ic_banco_fondeo"+
									"  AND 	rie.ic_producto_nafin = ?  ");
									
		lVarBind.add(new Integer(1));		
		if(!ic_epo.equals("")){
			qrySentencia.append(" AND	rie.ic_epo = ?  ");
			lVarBind.add(ic_epo);
		}
		
      qrySentencia.append("  Order by cg_razon_social")  ;
		
		ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
		rs = ps.executeQuery();		
		
		while(rs.next()) {			
			datos = new HashMap();			
			datos.put("clave", rs.getString(1));
			datos.put("descripcion", rs.getString(2));
			registros.add(datos);
		}
		rs.close();
		con.cierraStatement();
	
	} catch (Exception e) {
		System.err.println("Error: " + e);
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	} 
	return registros;
}

	public String getPantalla() {
		return pantalla;
	}

	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}

	
}