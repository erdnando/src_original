package com.netro.descuento;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Bitacora;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

public class ParametrizacionDsctoAutomaticoDE implements IQueryGeneratorPS { 
	
	/**  
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ParametrizacionDsctoAutomaticoDE.class);
	
	private boolean boolValoresAnteriores = true;
	
	public static final int DESCUENTO_AUTOMATICO_PYME 				= 1;
	public static final int EPOS_DSCTO_AUTOMATICO_OBLIGATORIO 	= 2;
	public static final int PARAMETRIZACION 							= 3;
	
	private int			numList				= 1;
	private int 		tipoConsulta		= 0;
	private int 		clavePyme			= 0;
	private ArrayList conditions			= null;  
	private static final String[] NOMBRESN = { "Orden",
															 "Modalidad",
															 "Epo",  
															 "Pyme",															 
															 "If",   
															 "Moneda",
															 "Factoraje",
															 "Pantalla",
															 "Descuento Autom�tico"
															 };     
															 
	private static final String[] NOMBRESV = { "Modalidad",
															 "Epo",  
															 "Pyme",															 
															 "If",   
															 "Moneda",
															 "Factoraje",
															 "Pantalla",
															 "Descuento Autom�tico"
														};      
	private static final String[] nombresD = { "Modalidad", "Epo", "Pyme", "If", "Moneda", "Factoraje", "Pantalla",	 "Descuento Autom�tico" }; //2017_003      
	
	private static final String[] nombresA = { "Orden", "Modalidad", "Epo", "Pyme", "If", "Moneda", "Factoraje", "Pantalla", "Descuento Autom�tico"  };     
	     
        
	//BITACORA
	StringBuffer valoresAnteriores = new StringBuffer();
	StringBuffer valoresActuales = new StringBuffer();
	
	// PARAMETROS ESPECIALES
	public int getTipoConsulta(){
		return this.tipoConsulta;
	}

	public void setTipoConsulta(int tipoConsulta){
		this.tipoConsulta = tipoConsulta;
	}
 
	public int getClavePyme(){
		return this.clavePyme;
	}

	public void setClavePyme(int clavePyme){
		this.clavePyme = clavePyme;
	}
 
	// METODOS DEL IQueryGenerator
	public ParametrizacionDsctoAutomaticoDE(){}

	/**
	 * Use parameters in this request to constuct a dynamic query 
	 * for certain documents.  The result set will contain only 
	 * Primary Key data that will be used later to page through
	 * data that summarizes the Documents.
	 */
	public String getAggregateCalculationQuery(HttpServletRequest request){
		return null;
	};
	
	/**
	 * In order to reduce the size of the com_documento result set, we need
	 * a query which returns only results that will actually be displayed to the
	 * user.
	 * 
	 * @param ic_estatus_docto query depends on the estatus
	 * @param ids these are the ids of the Documents we need to display
	 * 
	 */
	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
		return null;
	};

	/**
	 * Use parameters in this request to constuct a dynamic query 
	 * for certain documents.  The result set will contain only 
	 * Primary Key data that will be used later to page through
	 * data that summarizes the Documents.
	 */
	public String getDocumentQuery(HttpServletRequest request){
		return null;
	};

	/**
	 * Sirve para regresar el query con todos los datos 
	 * de acuerdo a los criterios de b�squeda.
	 * Para generar el archivo a petici�n del 
	 * usuario.
	 */
	public String getDocumentQueryFile(HttpServletRequest request){
		
		StringBuffer query 	= new StringBuffer();
		conditions 				= new ArrayList();
		
		if(this.tipoConsulta == ParametrizacionDsctoAutomaticoDE.DESCUENTO_AUTOMATICO_PYME){
			query.append(" select cs_dscto_automatico"+
							 " from comcat_pyme"+
							 " where ic_pyme = ? "); 
			conditions.add(new Integer(this.clavePyme));
			
		}else if(this.tipoConsulta == ParametrizacionDsctoAutomaticoDE.EPOS_DSCTO_AUTOMATICO_OBLIGATORIO){
			
			query.append(
						" SELECT e.cg_razon_social NOMBRE "   +
						" FROM comrel_producto_epo pe, comcat_epo e "   +
						" WHERE pe.ic_epo=e.ic_epo "   +
						" 		and pe.ic_epo in ( select pi.ic_epo "   +
						" 				from comrel_pyme_if pi,comrel_cuenta_bancaria cb "   +
						"        	where "   +
						" 				cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria "   +
						"     		and pi.cs_borrado = 'N' "   +
						" 			and cb.cs_borrado = 'N' "   +
						" 			and cb.ic_pyme=?) "   +
						" 		and pe.cs_dscto_auto_obligatorio = 'S' "   +
						" 		and e.cs_habilitado = 'S' " +
						" 		and pe.ic_producto_nafin = ? "   +
						" ORDER BY 1 "
			);
			conditions.add(new Integer(this.clavePyme));
			conditions.add(new Integer(1));
			
		}else if(this.tipoConsulta == ParametrizacionDsctoAutomaticoDE.PARAMETRIZACION){

			query.append(
						"   select /*+ use_nl(py,cb,pi,e,if,m,mo) */"   +
						"         e.ic_epo, e.cg_razon_social as NOMBRE_EPO,"   +
						"        if.cg_razon_social as NOMBRE_IF, if.ic_if, "   +
						" 	   cb.ic_cuenta_bancaria, cb.cg_banco, cb.cg_numero_cuenta, cb.cg_sucursal,"   +
						"        pi.cs_dscto_automatico, "   +
						" 	   1 numFilas, "   +
						" 	   m.cd_nombre, "   +
						" 	   mo.numMonedas, " + 
						" 	   mo.marca, " + 
						"	   m.ic_moneda, "   +
						"	   pe.cs_dscto_auto_obligatorio obligatorio, "   +
						"	   pi.cs_dscto_automatico_dia,"+
						"	   '15dsctoaut.jsp' as pantalla"	+
						"  from "   +
						" 	  comrel_cuenta_bancaria cb, "   +
						" 	comcat_pyme py, "   +
						" 	  comrel_pyme_if pi, "   +
						" 	  comrel_producto_epo pe, "   +
						" 	  comcat_epo e, "   +
						" 	  comcat_if if, "   +
						" 	  comcat_moneda m,"   +
						"  (select pi.ic_epo as Epo, count(1) as numMonedas, cb.ic_moneda"   +
						"          , sum(decode(pi.CS_DSCTO_AUTOMATICO,'S',1,0)) marca"   +
						"     from comrel_cuenta_bancaria cb, comrel_pyme_if pi"   +
						"     where cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria"   +
						"     and pi.cs_vobo_if = 'S'"   +
						"     and pi.cs_borrado = 'N'"   +
						"     and cb.cs_borrado = 'N'"   +
						"     and cb.ic_pyme = ? "+ //ses_ic_pyme   +
						"     and pi.cs_opera_descuento = 'S' " + 
						"     group by pi.ic_epo, cb.ic_moneda) 	mo"   +
						"  where py.ic_pyme = cb.ic_pyme"   +
						"  and cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria"   +
						"  and pe.ic_epo = e.ic_epo"   +
						"  and pe.ic_producto_nafin=1"  +
						"  and e.ic_epo = pi.ic_epo"   +
						"  and if.ic_if = pi.ic_if"   +
//						"  and vc.ic_epo = e.ic_epo"   +
						"  and mo.Epo = e.ic_epo"   +
						"  and mo.ic_moneda = m.ic_moneda"   +
						"  and cb.ic_moneda = m.ic_moneda"   +
						"  and pi.cs_borrado = 'N'"   +
						"  and cb.cs_borrado = 'N'"   +
						"  and pi.cs_vobo_if = 'S'"   +
						"  and pi.cs_opera_descuento = 'S' " + // Foda 061 - 2004
						"  and e.cs_habilitado = 'S' " + // Foda 066 - 2005
						"  and py.ic_pyme = ? "+//ses_ic_pyme   +
						"  and cb.ic_pyme= ? "+//ses_ic_pyme  +
						"  order by 1, 14"  
			);		
			conditions.add(new Integer(this.clavePyme));
			conditions.add(new Integer(this.clavePyme));
			conditions.add(new Integer(this.clavePyme));		
		}		
		return query.toString();		
	};
	
	public ArrayList getConditions(HttpServletRequest request){
		return this.conditions;
	}
		
	public int getNumList(HttpServletRequest request){
		return this.numList;
	}
   
   
   /**
    * garellano - FODEA 025-2012
    * Descuento Automatico
    * Busca el IF en la tabla COMREL_DSCTO_AUT_PYME si existe retorna true
    * @return Si existe IF retorna TRUE de lo contrario FALSE
    * @param cc_tipo_factoraje
    * @param ic_moneda
    * @param ic_if
    * @param ic_epo
    * @param ic_pyme
    */
    public boolean existeParametrizacion(String ic_epo, String ic_if, String ic_pyme, String ic_moneda, String cc_tipo_factoraje){
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;      
      boolean           exiteIf  =  false;
		List lVarBind		= new ArrayList();
		log.debug("existeParametrizacion (E)");
		 try{
         con.conexionDB();
			
         String strQry = "SELECT count(1) AS existe_if  "  +
                        "  FROM comrel_dscto_aut_pyme   "  +
                        " WHERE ic_if = ?               "  +
                        "   AND ic_epo = ?              "  +
                        "   AND ic_pyme = ?             "  +
                        "   AND ic_moneda = ?           "  +
                        "   AND cc_tipo_factoraje = ?   ";
		     

	 lVarBind		= new ArrayList();
         lVarBind.add(new Integer(ic_if));
         lVarBind.add(new Integer(ic_epo));
         lVarBind.add(new Integer(ic_pyme));
         lVarBind.add(new Integer(ic_moneda));
         lVarBind.add(cc_tipo_factoraje); 
		     
		    log.debug("strQry "+strQry+ "  \n  lVarBind "+lVarBind);
		     
	ps = con.queryPrecompilado(strQry,lVarBind );	
         rs = ps.executeQuery();
         
         if(rs.next() && rs!=null){
            exiteIf = Integer.parseInt(rs.getString("existe_if").trim())>0?true:false;
         }
			rs.close();
       	ps.close();      
			
      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  existeParametrizacion (S)" +e);
         throw new RuntimeException(e);      
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}		
      } 
      log.debug("existeParametrizacion (S)");
      return exiteIf;
    }
    
    /**
	 * Inserta nueva parametrizacion por IF
	 * @param cc_acuse
	 * @param cg_nombre_usuario
	 * @param ic_usuario
	 * @param cs_dscto_automatico_proc
	 * @param cs_dscto_automatico_dia
	 * @param cs_orden
	 * @param cc_tipo_factoraje
	 * @param ic_moneda
	 * @param ic_pyme
	 * @param ic_if
	 * @param ic_epo
	 */
    public boolean insertaParametrizacion(String ic_epo, 
												String ic_if, 
												String ic_pyme, 
												String ic_moneda, 
												String cc_tipo_factoraje, 
												String cs_orden, 
												String cs_dscto_automatico_dia, 
												String cs_dscto_automatico_proc,
												String ic_usuario, 
												String cg_nombre_usuario, 
												String cc_acuse, 
												String strLogin, String operarDsctoAut ){
      
		AccesoDB 	      con   =  new AccesoDB();
      PreparedStatement ps    =  null;
		boolean hayInsercion 	= false;
		StringBuffer texto = new StringBuffer();	
		StringBuffer insertBitacoraGral = new StringBuffer();	
		List lVarBind = new ArrayList();
		String tipoFactoraje ="";
		
		log.debug("insertaParametrizacion (E)");						
		
      try{
			con.conexionDB();
			
			String dsctoAutomatico = this.obtieneOperaDescuentoAutomatico(ic_pyme); 
			
			if(cc_tipo_factoraje.equals("V") )  { cs_orden ="0"; }
			if(cc_tipo_factoraje.equals("N") ||  "D".equals(cc_tipo_factoraje) ||  "A".equals(cc_tipo_factoraje) )  { 
				if( cs_orden.equals("") || cs_orden.equals("0") ) { 
					cs_orden ="";				
				}
                        }
                        if("N".equals(cs_dscto_automatico_dia)  )  {                        
                             cs_orden ="";                           
                        }
                            
          
			if("N".equals(cc_tipo_factoraje) ||  "A".equals(cc_tipo_factoraje)  )  { 
			    tipoFactoraje ="NORMAL"; 
			}else  if("V".equals(cc_tipo_factoraje)  )  { 
			    tipoFactoraje ="VENCIDO"; 
			}else  if("D".equals(cc_tipo_factoraje)  )  { 
			    tipoFactoraje ="DISTRIBUIDO"; 
			}
	  
	  
	  
			String strQry = " INSERT INTO	 COMREL_DSCTO_AUT_PYME 		" +
							" (IC_PYME,	IC_EPO, IC_IF,	IC_MONEDA,	CS_ORDEN, CS_DSCTO_AUTOMATICO_DIA,	CS_DSCTO_AUTOMATICO_PROC, CC_TIPO_FACTORAJE,	"+
							"	IC_USUARIO,	CG_NOMBRE_USUARIO, DF_FECHAHORA_OPER, 	CC_ACUSE	 ) " +
							" VALUES  (	 ?,  ?,  ?, 	?,   ?, 	?, ?,	?, ?, ?,	SYSDATE,	?	)" ;
							
			lVarBind = new ArrayList();			
			lVarBind.add(new Integer(ic_pyme));
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer(ic_if));
			lVarBind.add(new Integer(ic_moneda));
			lVarBind.add(cs_orden);
			lVarBind.add(cs_dscto_automatico_dia);
			lVarBind.add(cs_dscto_automatico_proc);
			lVarBind.add(cc_tipo_factoraje);
			lVarBind.add(ic_usuario);
			lVarBind.add(cg_nombre_usuario);
			lVarBind.add(cc_acuse==null?"":cc_acuse);
			
			System.out.println(" strQry ---"+strQry);
			System.out.println(" lVarBind ---"+lVarBind);
			
			ps = con.queryPrecompilado(strQry,lVarBind);	   
			ps.executeUpdate();
			ps.close();
			
			//============================ NECESARIO PARA LA BITACORA ===============================================			
			insertBitacoraGral = new StringBuffer();					 
			 insertBitacoraGral.append(
				"\n INSERT INTO BIT_CAMBIOS_GRAL ( 		" +
				"\n IC_CAMBIO,  								" +
				"\n CC_PANTALLA_BITACORA,  				" +
				"\n CG_CLAVE_AFILIADO, 						" +
				"\n IC_NAFIN_ELECTRONICO, 					" +
				"\n DF_CAMBIO, 								" +
				"\n IC_USUARIO, 								" +
				"\n CG_NOMBRE_USUARIO, 						" +
				"\n CG_ANTERIOR, 								" +
				"\n CG_ACTUAL, 								" +
				"\n IC_GRUPO_EPO, 							" +
				"\n IC_EPO ) 									" +
				"\n VALUES(  									" +
				"\n SEQ_BIT_CAMBIOS_GRAL.nextval, 		" +
				"\n 'DESAUTOMA', 								" +  // Descuento Automatico - Nueva Version
				"\n 'P', 										" +
				"\n ?, 											" +
				"\n SYSDATE, 									" +
				"\n ?,  											" +
				"\n ?, 											" +  
				"\n '',  										" +  
				"\n ?, 											" +
				"\n '', 											" +
				"\n '' ) 										" );	
				
				String nafElecPyme = sgetNoNafinElectronico(ic_pyme, "P");
				String datosPyme 		= obtieneDatosPymeEpoIf(ic_pyme, "PYME");
				String datosEpo 		= obtieneDatosPymeEpoIf(ic_epo, "EPO");
				String datosIf 		= obtieneDatosPymeEpoIf(ic_if, "IF");
				String datosMoneda 	= obtieneDatosPymeEpoIf(ic_moneda, "MONEDA");
				
				texto = new StringBuffer();
				if(cc_tipo_factoraje.equals("N")   ||  "D".equals(cc_tipo_factoraje)  ||  "A".equals(cc_tipo_factoraje) )  {
					texto.append("Orden=").append(cs_orden).append("\n");
				}
				String tipoModalidad ="";
				if(cs_dscto_automatico_dia.equals("P"))
					tipoModalidad ="PRIMER D�A DESDE SU PUBLICACI�N";
				if(cs_dscto_automatico_dia.equals("U"))
					tipoModalidad ="�LTIMO D�A DESDE SU VENCIMIENTO CONSIDERANDO D�AS DE M�NIMOS";
				if(cs_dscto_automatico_dia.equals("N"))
					tipoModalidad ="NO AUTORIZO PARAMETRIZACION";
                                if(  "A".equals(cs_dscto_automatico_dia) )
                                    tipoModalidad ="DESCUENTO AUTOM�TICO EPO";
				
				texto.append("Modalidad==").append(tipoModalidad).append("\n");
				texto.append("Epo=").append(datosEpo).append("\n");
				texto.append("Pyme=").append(datosPyme).append("\n");
				texto.append("If=").append(datosIf).append("\n");
				texto.append("Moneda=").append(datosMoneda).append("\n");
				texto.append("Factoraje=").append(tipoFactoraje).append("\n");  
				texto.append("Pantalla=").append("Descuento Autom�tico").append("\n"); 
				if(!operarDsctoAut.equals(dsctoAutomatico))  {				
					texto.append("Descuento Automatico=").append(operarDsctoAut).append("\n"); 
				}
				
				lVarBind = new ArrayList();			      
				lVarBind.add(new Integer(nafElecPyme));   
				lVarBind.add(strLogin);
				lVarBind.add(cg_nombre_usuario);
				lVarBind.add(texto.toString());  
							               
				ps = con.queryPrecompilado(insertBitacoraGral.toString(),lVarBind);	   
				ps.executeUpdate();
				ps.close();
			//=======================================================================================================
				
			hayInsercion = true;
						
				
      }catch(Exception e){
			con.terminaTransaccion(false);
         e.printStackTrace();
			log.error("Error en  insertaParametrizacion (S)" +e);
         throw new RuntimeException(e);      
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}			
      } 
		log.debug("insertaParametrizacion (S)");	
		return hayInsercion;
    }
	 
	 /**
	 * Si el IF ya est� parametrizado, entonces actualiza valores
	 * @param cc_tipo_factoraje
	 * @param ic_moneda
	 * @param ic_if
	 * @param ic_epo
	 * @param ic_pyme
	 * @param cs_dscto_automatico_dia
	 * @param cs_orden
	 */
	 public boolean actualizaParametrizacion(String cs_orden,
													String cs_dscto_automatico_dia,
													String ic_pyme,
													String ic_epo,
													String ic_if,
													String ic_moneda,
													String cc_tipo_factoraje,
													String ic_usuario,
													String cg_nombre_usuario,
													String cc_acuse,
													String strLogin,
													String operarDsctoAut,  
													String clavePyme){
										
		log.debug("actualizaParametrizacion (E)");	
		
		boolean hayActualiacion = false;		 	
		AccesoDB 	      con   =  new AccesoDB();
      PreparedStatement ps    =  null;
		List lVarBind		= new ArrayList();
							
		String strInsertModalidad = "", strValueModalidad = "", strInsertOrden = "", strValueOrden = "";
			
		if(!cs_dscto_automatico_dia.equals("") || cs_dscto_automatico_dia!=null){
			strInsertModalidad = " CS_DSCTO_AUTOMATICO_DIA = ? , ";
		}
		
		if(!cs_orden.equals("") || cs_orden!=null){
			strInsertOrden = "  CS_ORDEN = ? ,  ";
		}
		if(cc_tipo_factoraje.equals("V")  )  { cs_orden ="0"; }
		if(cc_tipo_factoraje.equals("N")  ||  "D".equals(cc_tipo_factoraje) ||  "A".equals(cc_tipo_factoraje) )  { 
			if( cs_orden.equals("") || cs_orden.equals("0") ) { 
				cs_orden ="";				
			}
		}
		if(cs_dscto_automatico_dia.equals("N"))  {
			cs_orden ="";				
		}
		
	  try{
			con.conexionDB();	 
			
			//============================ REALIZA UPDATE A LA TABLA DE COMREL_DSCTO_AUT_PYME  ===============================================
					
			String strQry = " UPDATE 									" +
							" 			COMREL_DSCTO_AUT_PYME 			" +
							"		SET 										" +
							"			"+strInsertOrden+"				" +
							"			"+strInsertModalidad+"			" +
							"			CS_DSCTO_AUTOMATICO_PROC = ? ," + //DESCUENTO AUTOMATICO ACTIVADO = S
							"			DF_FECHAHORA_OPER = SYSDATE	" + 
							"			,IC_USUARIO = ?					" +
							"			,CG_NOMBRE_USUARIO = ?			" ;
							if(cc_acuse!=null){
								strQry += "			,CC_ACUSE = ?						";	
							}
				strQry+=	"		WHERE										" +
							"			IC_PYME = ?							" +
							"			AND IC_EPO = ?						" +
							"			AND IC_IF = ?						" +
							"			AND IC_MONEDA = ?					" +
							"			AND CC_TIPO_FACTORAJE = ?		";
			
					
			int puntero = 1;
			if(!cs_orden.equals("") || cs_orden!=null){
				lVarBind.add(cs_orden);
			}
			if(!cs_dscto_automatico_dia.equals("") || cs_dscto_automatico_dia!=null){
                            if("A".equals(cc_tipo_factoraje) ){   
                                cs_dscto_automatico_dia ="A";
                            }                            
                            lVarBind.add(cs_dscto_automatico_dia);
			}
			if(cs_dscto_automatico_dia.equals("N"))  {
				lVarBind.add("N");			
			}else  {
				lVarBind.add("S");				
			}
			
			lVarBind.add(ic_usuario);
			lVarBind.add(cg_nombre_usuario);
			if(cc_acuse!=null){
				lVarBind.add(cc_acuse);
			}
			lVarBind.add(new Integer(ic_pyme));
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer(ic_if));
			lVarBind.add(new Integer(ic_moneda));
			lVarBind.add(cc_tipo_factoraje);		
			
			log.debug(" strQry ---"+strQry);
			log.debug(" lVarBind ---"+lVarBind);   
			
			ps = con.queryPrecompilado(strQry, lVarBind);											   
			ps.executeUpdate();
			ps.close();
				
			//============================ NECESARIO PARA LA BITACORA ===============================================			
    
			boolValoresAnteriores	=	true;
			List datosAnteriores 				= 	obtieneIF(ic_epo, ic_pyme, cc_tipo_factoraje, ic_moneda, ic_if, operarDsctoAut);   
			List datosActuales					=	new ArrayList();		
			
			String datosPyme 		= obtieneDatosPymeEpoIf(ic_pyme, "PYME");
			String datosEpo 		= obtieneDatosPymeEpoIf(ic_epo, "EPO");   
			String datosIf 		= obtieneDatosPymeEpoIf(ic_if, "IF");
			String datosMoneda 	= obtieneDatosPymeEpoIf(ic_moneda, "MONEDA");  
				
			datosActuales =	new ArrayList();
			if(cc_tipo_factoraje.equals("N") ||  "D".equals(cc_tipo_factoraje) ||  "A".equals(cc_tipo_factoraje))  { 
                            datosActuales.add(cs_orden==null?"":cs_orden);  
                        }
			String tipoModalidad ="";
			if(cs_dscto_automatico_dia.equals("P"))
				tipoModalidad ="PRIMER D�A DESDE SU PUBLICACI�N";
			if(cs_dscto_automatico_dia.equals("U"))
				tipoModalidad ="�LTIMO D�A DESDE SU VENCIMIENTO CONSIDERANDO D�AS DE M�NIMOS";
			if(cs_dscto_automatico_dia.equals("N"))
				tipoModalidad ="NO AUTORIZO PARAMETRIZACION";
                        if( "A".equals(cs_dscto_automatico_dia))
                            tipoModalidad ="DESCUENTO AUTOM�TICO EPO";
			
			String tipoFactoraje ="";
			if("N".equals(cc_tipo_factoraje) ||  "A".equals(cc_tipo_factoraje) )  {  
			    tipoFactoraje ="NORMAL"; 
			}else  if("V".equals(cc_tipo_factoraje)  )  { 
			    tipoFactoraje ="VENCIDO"; 
			}else  if("D".equals(cc_tipo_factoraje)  )  { 
			    tipoFactoraje ="DISTRIBUIDO"; 
                        }
	      
			datosActuales.add(tipoModalidad);
			datosActuales.add(datosEpo);
			datosActuales.add(datosPyme);
			datosActuales.add(datosIf);   
			datosActuales.add(datosMoneda);
			datosActuales.add(tipoFactoraje);  
			datosActuales.add("Descuento Autom�tico");   
			
			valoresActuales = new StringBuffer();		
			if(cc_tipo_factoraje.equals("N") || "D".equals(cc_tipo_factoraje)|| "A".equals(cc_tipo_factoraje) )  {  
                            valoresActuales.append(	"Orden="			+ (cs_orden==null?"":cs_orden) + "\n");   
                        }
			valoresActuales.append(	"Modalidad==="	+ tipoModalidad+"\n"		);
			valoresActuales.append(	"Epo="			+  datosEpo  + "\n"						);
			valoresActuales.append(	"Pyme="			+ datosPyme + "\n"						);
			valoresActuales.append(	"If="				+  datosIf	 + "\n"						);
			valoresActuales.append(	"Moneda="		+  datosMoneda	 + "\n"					);
			valoresActuales.append(	"Factoraje="	+ tipoFactoraje  + "\n"					);
			valoresActuales.append(	"Pantalla="	+  ("Descuento Autom�tico")	 + "\n"					);
			
				
			//obtengo el valor de Descuento Automatico Actual
			String dsctoAutomatico = this.obtieneOperaDescuentoAutomatico(ic_pyme);  
							
			if(!operarDsctoAut.equals(dsctoAutomatico))  {				
				valoresActuales.append("Descuento Automatico = " + (operarDsctoAut.equals("S")?"Activado":"Desactivado"));
				datosActuales.add(operarDsctoAut);				
			}			
			
			// Guarda en bitacora 	
			if(cc_tipo_factoraje.equals("N"))  {
				grabaBitacora(con, datosAnteriores, datosActuales, ic_pyme, strLogin, NOMBRESN, cc_tipo_factoraje);	
			}else if(cc_tipo_factoraje.equals("V"))  {
				grabaBitacora(con, datosAnteriores, datosActuales, ic_pyme, strLogin, NOMBRESV, cc_tipo_factoraje);	
			}else if(cc_tipo_factoraje.equals("D"))  {
				grabaBitacora(con, datosAnteriores, datosActuales, ic_pyme, strLogin, nombresD, cc_tipo_factoraje);
                        }else if( "A".equals(cc_tipo_factoraje) )  {
			            grabaBitacora(con, datosAnteriores, datosActuales, ic_pyme, strLogin, nombresA, cc_tipo_factoraje);     
			}
						
			hayActualiacion = true;
						
      }catch(Exception e){
			hayActualiacion  = false;
			con.terminaTransaccion(false);
         e.printStackTrace();
			log.error("Error en  actualizaParametrizacion (S)" +e);
         throw new RuntimeException(e);      
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}		
      } 
		log.debug("actualizaParametrizacion (S)");	
		return hayActualiacion;
	 }
		
	/**
	 * Actualiza registros al seleccionar Operar Descuento Automatico F025-2012
	 * @autor Gustavo Arellano
	 * @return 
	 * @param operarDsctoAut
	 * @param ic_pyme
	 * @param strLogin  
	 */
	public boolean actualizaOperarDescuentoAutomatico(String operarDsctoAut, String ic_pyme, String strLogin, String xEPOIF){
		boolean hayActualiacion = false;		 	
		AccesoDB 	      con   =  new AccesoDB();
      PreparedStatement ps    =  null;
		
		log.debug("actualizaOperarDescuentoAutomatico (E)");		   
		 
      try{	      	
      	con.conexionDB();	
				
			List	lVarBind	= new ArrayList();
				
			//============================ NECESARIO PARA LA BITACORA ===============================================
			List datosAnteriores 				= 	new ArrayList();   
			List datosActuales					=	new ArrayList();			
			StringBuffer valoresAnteriores 	= 	new StringBuffer();
			StringBuffer valoresActuales 		= 	new StringBuffer();
			
			String nombrePyme = obtieneDatosPymeEpoIf(ic_pyme, "PYME");
			String dsctoAutomatico = this.obtieneOperaDescuentoAutomatico(ic_pyme);  
			
			valoresAnteriores.append("Pyme = " + nombrePyme);
			valoresAnteriores.append("Descuento Automatico = " + (dsctoAutomatico.equals("S")?"Activado":"Desactivado"));	
			datosAnteriores.add(nombrePyme);
			datosAnteriores.add(dsctoAutomatico);
			//=======================================================================================================
      	
			String strQry = " UPDATE COMCAT_PYME 				" +
							"		SET CS_DSCTO_AUTOMATICO = ?	" +
							"		WHERE	IC_PYME = ?					";
 			
			lVarBind		= new ArrayList();
			lVarBind.add( operarDsctoAut);
			lVarBind.add(new Integer(ic_pyme.trim()));
			
			ps = con.queryPrecompilado(strQry, lVarBind);	
			ps.executeUpdate();			
			ps.close();
			
			if(xEPOIF.equals("S"))  { // solo cuando se cambia la opci�n Si Autorizo y NO Autorizo
			
				strQry = " UPDATE COMREL_DSCTO_AUT_PYME				" +
							"	SET CS_DSCTO_AUTOMATICO_PROC = ? 		"+
							"	WHERE	IC_PYME = ?					"+
							" AND CS_DSCTO_AUTOMATICO_DIA  in('P', 'U')  ";
		
				lVarBind		= new ArrayList();
				lVarBind.add( operarDsctoAut);
				lVarBind.add(new Integer(ic_pyme.trim()));			
				ps = con.queryPrecompilado(strQry, lVarBind);			
				ps.executeUpdate();
				ps.close();			
			}						
				
			
			//============================ NECESARIO PARA LA BITACORA ===============================================
			valoresActuales.append("Pyme = " + nombrePyme);
			valoresActuales.append("Descuento Automatico = " + (this.obtieneOperaDescuentoAutomatico(ic_pyme).equals("S")?"Activado":"Desactivado"));		
			datosActuales.add(nombrePyme);
			datosActuales.add(operarDsctoAut);
			String[] nombres = {"Pyme","Descuento Automatico"};
			
			//Para Actualizar el Descuento Automatico siempre y y cuando sea diferente
			if(!operarDsctoAut.equals(dsctoAutomatico))  {
				this.grabaBitacoraA(con, datosAnteriores, datosActuales, ic_pyme, strLogin, nombres);	
			}
			//=======================================================================================================
			     	  				
			hayActualiacion = true;
						   
      }catch(Exception e){
			hayActualiacion  = false;
			con.terminaTransaccion(false);
         e.printStackTrace();
			log.error("Error en  actualizaOperarDescuentoAutomatico (S)" +e);
         throw new RuntimeException(e);      
      }finally{
      	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}	
      } 
		log.debug("actualizaOperarDescuentoAutomatico (S)");	
		return hayActualiacion;   
	}
	
	public void grabaBitacoraA(AccesoDB con, List datosAnteriores, List datosActuales, String ic_pyme, String claveUsuario, String[] nombres) {
		log.debug("grabaBitacoraA (E)");	
		
		try{   
			con.conexionDB();
			Bitacora bitacora = new Bitacora();			
			List cambios = Bitacora.getCambiosBitacora(datosAnteriores, datosActuales, nombres);
			valoresAnteriores.append(cambios.get(0));
			valoresActuales.append(cambios.get(1));

			Bitacora.grabarEnBitacora(con, "DESAUTOMA", "P",
				sgetNoNafinElectronico(ic_pyme, "P"), claveUsuario,
				valoresAnteriores.toString(), valoresActuales.toString());
					
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error en  grabaBitacoraA (S)" +e);
		}	
			log.debug("grabaBitacoraA (S)");	
	}
	
	
	/**
	 * Obtiene el listado de las IF con relacion EPO y PyME
	 * @autor Gustavo Arellano
	 * @param ic_epo
	 * @param clavePyme
	 * @param cc_tipo_factoraje
	 * @return
	 */
	public List obtieneListaIF(String ic_epo, String clavePyme, String cc_tipo_factoraje){
		String 	ic_moneda	=	null,
					ic_if			=	null;
		return obtieneListaIF(ic_epo, clavePyme, cc_tipo_factoraje, ic_moneda, ic_if);
	}  
	
	
    /**
     * @autor Gustavo Arellano     
     * @param icEpo
     * @param clavePyme
     * @param ccTipoFactoraje
     * @param icMoneda
     * @param icIf
     * @return Obtiene el IF con relacion [ IC_EPO, IC_PYME, IC_MONEDA ]
     */
	public List obtieneListaIF(String icEpo, String clavePyme, String ccTipoFactoraje, String icMoneda, String icIf){
	    log.debug("obtieneListaIF (E)");	
	    
	    AccesoDB 	      con   =  new AccesoDB();
	    ResultSet 	      rs	   =  null;
	    PreparedStatement ps    =  null;
	    List ls = new ArrayList();
	    List lVarBind		= new ArrayList();
	    boolean obtieneUnicoRegistro = false;
	    StringBuilder qrySentencia = new StringBuilder();
	    /*
	    * Si IC_MONEDA && IC_IF son distintos de NULL
	    * entonces obtiene �nicamente el registro deseado ...
	    */
	    if(icMoneda!=null && icIf!=null){
		obtieneUnicoRegistro	=	true;
	    }
		
	    try{
		con.conexionDB();   
		qrySentencia.append(" SELECT "+
				    "	DAP.CS_ORDEN, ");
                
                if ("A".equals(ccTipoFactoraje)   ) {   //2019 QC  
                    qrySentencia.append(" DECODE(DAP.CS_DSCTO_AUTOMATICO_DIA,'','A', DAP.CC_TIPO_FACTORAJE) AS CS_DSCTO_AUTOMATICO_DIA , ");
                    qrySentencia.append(" DECODE(dap.cs_dscto_automatico_dia,'A','Descuento Autom�tico EPO','Descuento Autom�tico EPO') AS modalidad , "); 
                    qrySentencia.append(" DECODE(DAP.CC_TIPO_FACTORAJE,'','A') AS CC_TIPO_FACTORAJE , ");
                }else  {
		    qrySentencia.append(" DAP.CS_DSCTO_AUTOMATICO_DIA  AS CS_DSCTO_AUTOMATICO_DIA ,  ");
                    qrySentencia.append(  "	DECODE(dap.cs_dscto_automatico_dia,'U','�ltimo d�a desde su vencimiento considerando d�as de m�nimos','P','Primer d�a desde su publicaci�n') AS modalidad, " );
                    qrySentencia.append(" DAP.CC_TIPO_FACTORAJE  as  CC_TIPO_FACTORAJE ,  ");
                }		
                qrySentencia.append("	a.IC_EPO, a.NOMBRE_EPO, a.IC_PYME,  a.cg_razon_social," +
				    "	nvl(dap.cs_orden,a.cs_orden), a.nombre_if, a.ic_if, " +
				    "	a.ic_cuenta_bancaria, a.cg_banco, a.cg_numero_cuenta, " +
				    "	a.CG_SUCURSAL, a.DF_VOBO_IF, a.IC_MONEDA, a.CD_NOMBRE, " +
				    
				    "	a.pantalla " +
				    "	FROM " +
				    "	( " +
				    "	SELECT /*+ use_nl(py,cb,pi,e,if,m,mo) */ " +
				    "	e.ic_epo, e.cg_razon_social AS nombre_epo, py.ic_pyme,  py.cg_razon_social, " +
				    "	pi.ig_orden AS cs_orden, IF.cg_razon_social AS nombre_if, IF.ic_if, " +
				    "   cb.ic_cuenta_bancaria, cb.cg_banco, cb.cg_numero_cuenta, " +
				    "   cb.cg_sucursal, pi.df_vobo_if, m.ic_moneda, m.cd_nombre, " +
				    "   '15dsctoaut02ext.jsp' AS pantalla " +
				    "  	FROM comrel_cuenta_bancaria cb, " +
				    " 	comcat_pyme py, " +
				    "   comrel_pyme_if pi, " +
				    "   comrel_producto_epo pe, " +
				    "   comcat_epo e, " +
				    "   comcat_if IF, " +
				    "   comcat_moneda m " );
		
	    if ("D".equals(ccTipoFactoraje)  || "V".equals(ccTipoFactoraje)  ) {			    
		qrySentencia.append(    "  , COMREL_IF_EPO IE ");
	    }
		
	    qrySentencia.append(    " 	WHERE py.ic_pyme = cb.ic_pyme " +
				    "	AND m.ic_moneda IN(54, 1) 	" +
				    "   AND cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria " +
				    "   AND pe.ic_epo = e.ic_epo " +
				    "   AND pe.ic_producto_nafin = 1 " +
				    "   AND e.ic_epo = pi.ic_epo " +
				    "   AND IF.ic_if = pi.ic_if " +
				    "   AND cb.ic_moneda = m.ic_moneda " +
				    "   AND pi.cs_borrado = 'N' " +
				    "   AND cb.cs_borrado = 'N' " +
				    "   AND pi.cs_vobo_if = 'S' " +
				    "   AND pi.cs_opera_descuento = 'S' " +
				    "   AND e.cs_habilitado = 'S' " +
				    "   AND py.ic_pyme = ? " +
				    "   AND cb.ic_pyme = ? " +
				    "   and PI.IC_EPO = ? ");
									
				    /*
				    * Si IC_MONEDA && IC_IF son distintos de NULL
				     * entonces obtiene �nicamente el registro deseado ...
				    */
		if(obtieneUnicoRegistro){
		    qrySentencia.append(" AND PI.IC_EPO  = ? "	+
					" AND m.ic_moneda	= ? "	+
					" AND pi.ic_if =	? ");	
		}
		
		if ("D".equals(ccTipoFactoraje)   || "V".equals(ccTipoFactoraje)  ) {			    
		    qrySentencia.append(" AND pi.ic_epo = ie.ic_epo "+
					" AND pi.ic_if = ie.IC_IF  " );
		}
		if ("D".equals(ccTipoFactoraje)   ) {	
		    qrySentencia.append(" AND ie.CS_FACTORAJE_DISTRIBUIDO = ? ");   
		}else  if ("V".equals(ccTipoFactoraje)   ) {	
		    qrySentencia.append(" AND ie.CS_FACTORAJE_VENCIMIENTO = ? ");   
          	}  else  if ("A".equals(ccTipoFactoraje)   ) {   //2019 QC   
                    qrySentencia.append(" AND pe.CS_DESC_AUTO_EPO = ? ");   
                }
          	
		qrySentencia.append("  ) a, " +
				    "   COMREL_DSCTO_AUT_PYME DAP " +
				    " 	WHERE " +
				    "   A.IC_IF   = DAP.IC_IF(+) " +
				    "  	AND A.IC_EPO  = DAP.IC_EPO(+)  " +
				    "  AND A.ic_pyme = DAP.IC_PYME(+) " +
				    "  AND a.IC_MONEDA = DAP.IC_MONEDA(+) " +
				    "  AND ? = DAP.CC_TIPO_FACTORAJE(+) ");    
									
										  
                        
		lVarBind = new ArrayList();
		lVarBind.add(new Integer(clavePyme));
		lVarBind.add(new Integer(clavePyme));
		lVarBind.add(new Integer(icEpo)); 
		    /*
		     * Si IC_MONEDA && IC_IF son distintos de NULL
		     * entonces obtiene �nicamente el registro deseado ...
		     */
		if(obtieneUnicoRegistro){
		    lVarBind.add(new Integer(icEpo)); 
		    lVarBind.add(new Integer(icMoneda)); 
		    lVarBind.add(new Integer(icIf)); 
		}
		if ("D".equals(ccTipoFactoraje)   || "V".equals(ccTipoFactoraje)   ) {	
		  lVarBind.add("S");
		}
                if ("A".equals(ccTipoFactoraje)   ) {   //2019 QC
                    lVarBind.add("S");  
                }
		lVarBind.add(ccTipoFactoraje);
		
		
		log.debug("strQry  "+qrySentencia.toString()+" \n lVarBind  "+lVarBind);
		
		ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);	
		rs = 	ps.executeQuery();            
         		
		HashMap  datos =  new HashMap();
		while(rs.next()){
		    datos = new HashMap();   
					
		    datos.put("IC_PYME",rs.getString("IC_PYME"));
		    datos.put("IC_EPO",   rs.getString("IC_EPO")  );
		    datos.put("NOMBRE_EPO", rs.getString("NOMBRE_EPO") );
		    datos.put("IC_IF",  rs.getString("IC_IF")  );
		    datos.put("NOMBRE_IF", rs.getString("NOMBRE_IF") );
		    datos.put("IC_MONEDA", rs.getString("IC_MONEDA") );
		    datos.put("NOMBRE_MONEDA", rs.getString("CD_NOMBRE") );
		    datos.put("FECHA_LIBERACION", rs.getString("DF_VOBO_IF"));
		    datos.put("CS_ORDEN",  rs.getString("CS_ORDEN")  );
		    datos.put("CS_DSCTO_AUTOMATICO_DIA",  rs.getString("CS_DSCTO_AUTOMATICO_DIA") );
		    datos.put("CC_TIPO_FACTORAJE",  rs.getString("CC_TIPO_FACTORAJE") );
		    ls.add(datos);
		    
		}   
		rs.close();
			
	}catch(Exception e){
	    e.printStackTrace();
	    con.terminaTransaccion(false);
	    log.error("Error en  obtieneListaIF  " +e);
	    throw new RuntimeException(e);      
	}finally{
	    if(con.hayConexionAbierta()) {
		con.terminaTransaccion(true);
		con.cierraConexionDB();
	    }				
	}
	log.debug("obtieneListaIF (S)");	
	return ls;
    }
	
	/**
	 * 
	 * @return 
	 * @param ic_if
	 * @param ic_moneda
	 * @param cc_tipo_factoraje
	 * @param clavePyme
	 * @param ic_epo
	 */
	public List obtieneIF(String ic_epo, String clavePyme, String cc_tipo_factoraje, String ic_moneda, String ic_if, String  operarDsctoAut){
		AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      List ls = new ArrayList();
		boolean obtieneUnicoRegistro = false;
		log.debug("obtieneIF (E)");	
		List lVarBind		= new ArrayList();
		/*
		 * Si IC_MONEDA && IC_IF son distintos de NULL
		 * entonces obtiene �nicamente el registro deseado ...
		 */
		if(ic_moneda!=null && ic_if!=null){
			obtieneUnicoRegistro	=	true;
		}
		
		//obtengo el valor de Descuento Automatico Actual
		String dsctoAutomatico = this.obtieneOperaDescuentoAutomatico(clavePyme);  
			
		if(!operarDsctoAut.equals(dsctoAutomatico) )  {		
			String[] NOMBRES = {"Descuento Automatico"};
		}
		
		String tipoFactoraje ="";
		if("N".equals(cc_tipo_factoraje) || "A".equals(cc_tipo_factoraje)  )  { 
		    tipoFactoraje ="NORMAL"; 
		}else  if("V".equals(cc_tipo_factoraje)  )  { 
		    tipoFactoraje ="VENCIDO"; 
		}else  if("D".equals(cc_tipo_factoraje)  )  { 
		    tipoFactoraje ="DISTRIBUIDO"; 
		}
				
			
      try{
        
         StringBuilder strQry = new StringBuilder();   
          
         strQry.append(  "SELECT DAP.CS_ORDEN , DAP.CS_DSCTO_AUTOMATICO_DIA  AS CS_DSCTO_AUTOMATICO_DIA ,  "+
             "  DECODE(dap.cs_dscto_automatico_dia,'U','�ltimo d�a desde su vencimiento considerando d�as de m�nimos','P','Primer d�a desde su publicaci�n', 'A','Descuento Autom�tico EPO'   ) AS modalidad, " +
             "  a.IC_EPO, a.NOMBRE_EPO, a.IC_PYME,  a.cg_razon_social," +
                                                                            "       	nvl(dap.cs_orden,a.cs_orden), a.nombre_if, a.ic_if, " +
									"       	a.ic_cuenta_bancaria, a.cg_banco, a.cg_numero_cuenta, " +
									"       	a.CG_SUCURSAL, a.DF_VOBO_IF, a.IC_MONEDA, a.CD_NOMBRE, " +
									"       	a.pantalla " +
									"FROM " +
									"	( " +
									"SELECT /*+ use_nl(py,cb,pi,e,if,m,mo) */ " +
									"       e.ic_epo, e.cg_razon_social AS nombre_epo, py.ic_pyme, py.cg_razon_social, " +
									"       pi.ig_orden AS cs_orden, IF.cg_razon_social AS nombre_if, IF.ic_if, " +
									"       cb.ic_cuenta_bancaria, cb.cg_banco, cb.cg_numero_cuenta, " +
									"       cb.cg_sucursal, pi.df_vobo_if, m.ic_moneda, m.cd_nombre, " +
									"       '15dsctoaut02ext.jsp' AS pantalla " +
									"  FROM comrel_cuenta_bancaria cb, " +
									"       comcat_pyme py, " +    
									"       comrel_pyme_if pi, " +
									"       comrel_producto_epo pe, " +
									"       comcat_epo e, " +
									"       comcat_if IF, " +
									"       comcat_moneda m " +
									" WHERE py.ic_pyme = cb.ic_pyme " +
									"	 AND m.ic_moneda IN(54, 1) 	" +
									"   AND cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria " +
									"   AND pe.ic_epo = e.ic_epo " +
									"   AND pe.ic_producto_nafin = 1 " +
									"   AND e.ic_epo = pi.ic_epo " +
									"   AND IF.ic_if = pi.ic_if " +
									"   AND cb.ic_moneda = m.ic_moneda " +
									"   AND pi.cs_borrado = 'N' " +
									"   AND cb.cs_borrado = 'N' " +
									"   AND pi.cs_vobo_if = 'S' " +
									"   AND pi.cs_opera_descuento = 'S' " +
									"   AND e.cs_habilitado = 'S' " +
									"   AND py.ic_pyme = ? " +
									"   AND cb.ic_pyme = ? " +
									"   and PI.IC_EPO = ? ") ;
									
									/*
									 * Si IC_MONEDA && IC_IF son distintos de NULL
									 * entonces obtiene �nicamente el registro deseado ...
									 */
									if(obtieneUnicoRegistro){
										strQry.append("	AND PI.IC_EPO 		= ?	"	+
													"	AND m.ic_moneda	=	?	"	+
													"	AND pi.ic_if		=	?	");	
									}
									
						strQry.append( "   ) a, " +
									"   COMREL_DSCTO_AUT_PYME DAP " +
									" WHERE " +
									"      A.IC_IF   = DAP.IC_IF(+) " +
									"  AND A.IC_EPO  = DAP.IC_EPO(+)  " +
									"  AND A.ic_pyme = DAP.IC_PYME(+) " +
									"  AND a.IC_MONEDA = DAP.IC_MONEDA(+) " +
									"  AND ? = DAP.CC_TIPO_FACTORAJE(+) " );    
										  
         con.conexionDB();                   
			lVarBind		= new ArrayList();        
         lVarBind.add(new Integer(clavePyme));
         lVarBind.add(new Integer(clavePyme));
         lVarBind.add(new Integer(ic_epo)); 			
			/*
			 * Si IC_MONEDA && IC_IF son distintos de NULL
			 * entonces obtiene �nicamente el registro deseado ...
			 */
			if(obtieneUnicoRegistro){
				lVarBind.add(new Integer(ic_epo)); 
				lVarBind.add(new Integer(ic_moneda)); 
				lVarBind.add(new Integer(ic_if)); 
			}
			 lVarBind.add(cc_tipo_factoraje);
         
                            log.debug ("strQry =============>  "+strQry.toString()  +"\n "+lVarBind);  
          
			ps = con.queryPrecompilado(strQry.toString(), lVarBind);	
         rs         		= 	ps.executeQuery();         
         HashMap  datos =  null;
      
				
         if(rs.next()){
				ls = new ArrayList();
                                                               
                                                                
				if(cc_tipo_factoraje.equals("N") || cc_tipo_factoraje.equals("D") || "A".equals(cc_tipo_factoraje) )  {  				  
					ls.add(	rs.getString("CS_ORDEN")==null?"":rs.getString("CS_ORDEN"));				    
				}
				String tipoModalidad ="";
				if(rs.getString("CS_DSCTO_AUTOMATICO_DIA").equals("P"))
				tipoModalidad ="PRIMER D�A DESDE SU PUBLICACI�N";
				if(rs.getString("CS_DSCTO_AUTOMATICO_DIA").equals("U"))
					tipoModalidad ="�LTIMO D�A DESDE SU VENCIMIENTO CONSIDERANDO D�AS DE M�NIMOS";
				if(rs.getString("CS_DSCTO_AUTOMATICO_DIA").equals("N"))
					tipoModalidad ="NO AUTORIZO PARAMETRIZACION";
                                if("A".equals(rs.getString("CS_DSCTO_AUTOMATICO_DIA")) )
                                    tipoModalidad ="DESCUENTO AUTOM�TICO EPO";
				
			
            ls.add(	tipoModalidad					); 
            ls.add(	rs.getString("NOMBRE_EPO")                  	);  
            ls.add(	rs.getString("CG_RAZON_SOCIAL")                	);
            ls.add(	rs.getString("NOMBRE_IF")                   	);
            ls.add(	rs.getString("CD_NOMBRE")               	);
				ls.add(	tipoFactoraje);
				
				ls.add(	"Descuento Autom�tico");
				if(!operarDsctoAut.equals(dsctoAutomatico))  {	
					ls.add(	dsctoAutomatico);
				}
				
				
			
				if(boolValoresAnteriores){		
					valoresAnteriores = new StringBuffer();
					
					if ("N".equals(cc_tipo_factoraje)   || "D".equals(cc_tipo_factoraje) || "A".equals(cc_tipo_factoraje) ) {
						valoresAnteriores.append(	"Orden="			+ rs.getString("CS_ORDEN")==null?"":rs.getString("CS_ORDEN") + "\n");
					}
					valoresAnteriores.append(	"Modalidad===="	+ tipoModalidad + "\n"															);
					valoresAnteriores.append(	"Epo="			+ rs.getString("NOMBRE_EPO") + "\n"										);
					valoresAnteriores.append(	"Pyme="			+ rs.getString("CG_RAZON_SOCIAL") + "\n"								);
					valoresAnteriores.append(	"If="				+ rs.getString("NOMBRE_IF") + "\n"										);
					valoresAnteriores.append(	"Moneda="		+ rs.getString("CD_NOMBRE") + "\n"										);
					valoresAnteriores.append(	"Factoraje="   + tipoFactoraje + "\n"		);
					valoresAnteriores.append(	"Pantalla="   + ("Descuento Autom�tico") + "\n"		);
					if(!operarDsctoAut.equals(dsctoAutomatico))  {				
						valoresAnteriores.append("Descuento Autom�tico = " + dsctoAutomatico+ "\n"		);
					}
				}

         }   
			rs.close();
			
      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false );
			log.error("Error en  obtieneIF  " +e);
         throw new RuntimeException(e);      
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}				 
      }
		log.debug("obtieneIF (S)");	
		return ls;
	}  
	
	
    /**
     *
     * @param icPyme
     * @param icProductoNafin
     * @param ccTipoFactoraje
     * @param csFactorajeVencido
     * @param csAceptacion
     * @param operacion
     * @param csFactorajeDistribuido
     * @return
     */
    public List listaHashMapEpos(String icPyme, int icProductoNafin, String ccTipoFactoraje, String csFactorajeVencido, String csAceptacion, String operacion, String csFactorajeDistribuido ){
	log.debug("listaHashMapEpos (E)");
	AccesoDB con   =  new AccesoDB();
	ResultSet rs	   =  null;
	PreparedStatement ps    =  null;
	List  ls  = new ArrayList();
	HashMap datos = null;
	StringBuilder qrySentencia = new StringBuilder();
	List lVarBind	= new ArrayList();
	
	try{
	  
	    con.conexionDB(); 
	  
	    qrySentencia = new StringBuilder();
	  
	  
	    if (   "N".equals(ccTipoFactoraje)   ) { 
		qrySentencia = new StringBuilder();
		qrySentencia.append(" SELECT dap.IC_EPO, e.CG_RAZON_SOCIAL AS NOMBRE_EPO "  +
				    " FROM  comrel_pyme_epo dap,  comcat_epo e   "  +
				    " WHERE e.ic_epo = dap.ic_epo  "  +
				    " AND dap.cs_aceptacion = ? "  +
				    " AND dap.ic_pyme = ? ")  ;   
	
	    }else  if(ccTipoFactoraje.equals("D") ){
		
		qrySentencia = new StringBuilder();	   
		qrySentencia.append(" SELECT dap.IC_EPO, e.CG_RAZON_SOCIAL AS NOMBRE_EPO  	"  +
				    " FROM  comrel_pyme_epo dap,  comrel_producto_epo pre, comcat_epo e    "  +
				    " WHERE e.ic_epo = dap.ic_epo   "  +
				    " AND dap.ic_epo = pre.ic_epo  "  +
				    " AND dap.ic_epo = pre.ic_epo  "  +
				    " AND dap.cs_aceptacion = ? "  +
				    " AND dap.ic_pyme = ? "  +
				    " AND pre.ic_producto_nafin = ?   " 	+
				    " AND pre.CS_FACTORAJE_DISTRIBUIDO = ?  ")	;  
         
	    }else  if (   "V".equals(ccTipoFactoraje) ||   "obtieneEposVencimiento".equals(operacion) ) { 	 
		qrySentencia = new StringBuilder();
		qrySentencia.append(" SELECT dap.IC_EPO, e.CG_RAZON_SOCIAL AS NOMBRE_EPO  	"  +
				    " FROM  comrel_pyme_epo dap,  comrel_producto_epo pre, comcat_epo e    "  +
				    " WHERE e.ic_epo = dap.ic_epo   "  +
				    " AND dap.ic_epo = pre.ic_epo  "  +
				    " AND dap.ic_epo = pre.ic_epo  "  +
				    " AND dap.cs_aceptacion = ? "  +
				    " AND dap.ic_pyme = ? "  +
				    " AND pre.ic_producto_nafin = ?   " 	+
				    " AND pre.CS_FACTORAJE_VENCIDO = ?  ")	; 
                
                
                
	        }else  if (   "A".equals(ccTipoFactoraje)  ) {        
	            qrySentencia = new StringBuilder();
	            qrySentencia.append(" SELECT dap.IC_EPO, e.CG_RAZON_SOCIAL AS NOMBRE_EPO        "  +
	                                " FROM  comrel_pyme_epo dap,  comrel_producto_epo pre, comcat_epo e    "  +
	                                " WHERE e.ic_epo = dap.ic_epo   "  +
	                                " AND dap.ic_epo = pre.ic_epo  "  +
	                                " AND dap.ic_epo = pre.ic_epo  "  +
	                                " AND dap.cs_aceptacion = ? "  +
	                                " AND dap.ic_pyme = ? "  +
	                                " AND pre.ic_producto_nafin = ?   " +
	                                " AND pre.CS_DESC_AUTO_EPO = ?  "+
									" AND dap.cs_desc_automa_epo = ?  ") ; 
	            
	               
                
	    }
			
			
	    lVarBind		= new ArrayList();
	    if ( "N".equals(ccTipoFactoraje) ) { 	    
		lVarBind.add(csAceptacion);
		lVarBind.add(new Integer(icPyme));
		
	     }else  if(   "V".equals(ccTipoFactoraje) ) {
		
		lVarBind.add(csAceptacion);
		lVarBind.add(new Integer(icPyme));
		lVarBind.add(new Integer(icProductoNafin));
		lVarBind.add(csFactorajeVencido);
		
	    }else  if( "D".equals(ccTipoFactoraje) ) {
		
		lVarBind.add(csAceptacion);
		lVarBind.add(new Integer(icPyme));
		lVarBind.add(new Integer(icProductoNafin));
		lVarBind.add( csFactorajeDistribuido);
                
                
            }else  if (   "A".equals(ccTipoFactoraje)  ) {   
	        lVarBind.add(csAceptacion);
	        lVarBind.add(new Integer(icPyme));
	        lVarBind.add(new Integer(icProductoNafin));
	        lVarBind.add( "S"); 
			 lVarBind.add( "S");   
                
	    }
	 
	    log.debug("Query  ==== "+ccTipoFactoraje+" ==  "+qrySentencia+" \n  lVarBind "+lVarBind);
						
	     ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);		
	     rs	= ps.executeQuery();	
			
	     while(rs.next()){
		datos = new HashMap();
		datos.put(  "IC_EPO",  rs.getString("IC_EPO")  );
		datos.put(  "NOMBRE_EPO", rs.getString("NOMBRE_EPO") );
		ls.add(datos);
	     } 
	    rs.close();
			
	}catch(Exception e){
	    con.terminaTransaccion(false);
	    e.printStackTrace();
	    log.error("Error en  listaHashMapEpos  " +e);
	    throw new RuntimeException(e);      
	}finally{
	    if(con.hayConexionAbierta()) {
		con.terminaTransaccion(true);
		con.cierraConexionDB();
	    }
	}
	log.debug("listaHashMapEpos (S)");
	return ls;
    }
	
	/**
	 * 
	 * @return 
	 * @param ic_pyme
	 */
	public String obtieneOperaDescuentoAutomatico(String ic_pyme){
		AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		String operaDescuentoAutomatico = null;
		log.debug("obtieneOperaDescuentoAutomatico (E)");
      try{
         String strQry = null;
			
			strQry = "SELECT cs_dscto_automatico FROM comcat_pyme WHERE ic_pyme = ?";          
         
			con.conexionDB();    
         ps = con.queryPrecompilado(strQry);		
         ps.setInt(1, Integer.parseInt(ic_pyme));
         rs         = ps.executeQuery();     
			
         if(rs.next()){
            operaDescuentoAutomatico = rs.getString("cs_dscto_automatico")==null?"":rs.getString("cs_dscto_automatico");
         }
			rs.close();
			
      }catch(Exception e){
			con.terminaTransaccion(false);
         e.printStackTrace();
			log.error("Error en  obtieneOperaDescuentoAutomatico  " +e);
         throw new RuntimeException(e);      
      }finally{
         if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
		log.debug("obtieneOperaDescuentoAutomatico (S)");
		return operaDescuentoAutomatico;
	}
	
	/**
	 * Obtiene datos de los catatlos Epo, Pyme, If, Moneda segun sea el caso
	 * @return 
	 * @param ic_pyme
	 */
	public String obtieneDatosPymeEpoIf(String ic, String tipo){
		AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		String datos 				= "";
		log.debug("obtieneDatosPymeEpoIf (E)");
      try{
         String strQry = null;	
			
			if(tipo.toUpperCase().equals("PYME")){
				strQry = "SELECT cg_razon_social AS nombre FROM comcat_pyme WHERE ic_pyme = ?";          
         }
			
			else if(tipo.toUpperCase().equals("EPO")){
				strQry = "SELECT cg_razon_social AS nombre FROM comcat_epo WHERE ic_epo = ?";  
			}
			
			else if(tipo.toUpperCase().equals("IF")){
				strQry = "SELECT cg_razon_social AS nombre FROM comcat_if WHERE ic_if = ?";  
			}
			
			else if(tipo.toUpperCase().equals("MONEDA")){
				strQry = "SELECT cd_nombre AS nombre FROM comcat_moneda WHERE ic_moneda = ?";  
			}
			
			con.conexionDB();    
         ps = con.queryPrecompilado(strQry);		
         ps.setInt(1, Integer.parseInt(ic));
         rs         = ps.executeQuery();     
			
         if(rs.next()){
            datos = rs.getString("nombre")==null?"":rs.getString("nombre");
         }
			rs.close();
			
      }catch(Exception e){ 
			con.terminaTransaccion(false);
         e.printStackTrace();
			log.error("Error en  obtieneDatosPymeEpoIf  " +e);
         throw new RuntimeException(e);      
      }finally{
         if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
		log.debug("obtieneDatosPymeEpoIf (S)");
		return datos;
	}
	
	/**
	 * 
	 * @return 
	 * @param operacion
	 * @param cs_aceptacion
	 * @param cs_factoraje_vencido
	 * @param cc_tipo_factoraje
	 * @param ic_producto_nafin
	 * @param ic_pyme
	 */
	public int existenEposFactorajeVencido(String ic_pyme, int ic_producto_nafin, String cc_tipo_factoraje, String cs_factoraje_vencido, String cs_aceptacion, String operacion){
		AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      List ls = new ArrayList();
		HashMap datos = null;	
		int countRegistros = 0;
		log.debug("existenEposFactorajeVencido (E)");
      try{
         String strQry = null;
			
			strQry = "SELECT count(1)	as cuenta_registros  	"  +
							"FROM                                             	"  +
							"    comrel_pyme_epo dap,                       	"  +
							"    comrel_producto_epo pre,                      "  +
							"    comcat_epo e                                  "  +
							"WHERE e.ic_epo = dap.ic_epo                       "  +
							"     AND dap.ic_epo = pre.ic_epo                  "  +
							"     AND dap.ic_epo = pre.ic_epo                  "  +
							"     AND dap.cs_aceptacion = ?                    "  +
							"     AND dap.ic_pyme = ?                      		"  +
							"     AND pre.ic_producto_nafin = ?                " 	+
							"     AND pre.CS_FACTORAJE_VENCIDO = ?             "	;  
         
			con.conexionDB();    
         ps = con.queryPrecompilado(strQry);		
         ps.setString(1, cs_aceptacion);
         ps.setInt(2, Integer.parseInt(ic_pyme));
			ps.setInt(3, 1);
			ps.setString(4, "S");
			
         rs	= ps.executeQuery();			         
         if(rs.next()){
				countRegistros = rs.getInt("cuenta_registros");
         }
			rs.close();
			
      }catch(Exception e){
			con.terminaTransaccion(false);
         e.printStackTrace();
			log.error("Error en  existenEposFactorajeVencido  " +e);
         throw new RuntimeException(e);      
      }finally{
         if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
		log.debug("existenEposFactorajeVencido (S)");
		return countRegistros;
	}
	
	/**
	 * 
	 *  @autor Gustavo Arellano
	 * @return 
	 * @param strNombreUsuario
	 * @param cveUsuario
	 * @param cc_acuse
	 * @param cc_tipo_factoraje
	 * @param arrId
	 * @param arrOrden
	 * @param arrPyme
	 * @param arrIf
	 * @param arrEpo
	 * @param arrMoneda
	 * @param arrModalidad
	 */
	public boolean procesoActualizacionParametrizacion(String[] arrModalidad,
																	String[] arrMoneda,
																	String[] arrEpo,
																	String[] arrIf,
																	String[] arrPyme,
																	String[] arrOrden,
																	String[] arrId,
																	String cc_tipo_factoraje,
																	String cc_acuse,
																	String cveUsuario,
																	String strNombreUsuario,
																	String operarDsctoAut,  
																	String clavePyme
																	){
		
		log.debug("procesoActualizacionParametrizacion ------(E)");
		
		log.debug("clavePyme  == "+clavePyme );
		
		
		boolean existeParametrizacion 	= false;
      boolean actualizacionExitosa 		= false;
		
      try{  
				
         for(int i=0; i<arrId.length; i++){      
			
				arrOrden[i] = arrOrden[i].equals("")?"0":arrOrden[i];	
				arrModalidad[i] = arrModalidad[i].equals("")?"P":arrModalidad[i];	
				
				existeParametrizacion = this.existeParametrizacion(arrEpo[i], arrIf[i], arrPyme[i], arrMoneda[i], cc_tipo_factoraje);
             
				String desAuto="S";
				if(arrModalidad[i].equals("N"))  {
					desAuto="N";		
				}
			
				if(existeParametrizacion){
					actualizacionExitosa = this.actualizaParametrizacion(arrOrden[i], arrModalidad[i], arrPyme[i], arrEpo[i], arrIf[i], arrMoneda[i], cc_tipo_factoraje, cveUsuario, strNombreUsuario, cc_acuse, cveUsuario, operarDsctoAut, clavePyme);
				}else{  
					actualizacionExitosa = this.insertaParametrizacion(arrEpo[i], arrIf[i], arrPyme[i], arrMoneda[i], cc_tipo_factoraje, arrOrden[i], arrModalidad[i], desAuto, cveUsuario, strNombreUsuario, cc_acuse, cveUsuario, operarDsctoAut); 
				}					
			}
                  
			actualizacionExitosa = true;	
			
      }catch(Exception e){
         actualizacionExitosa = false;      
         e.printStackTrace();
			log.error("Error en  procesoActualizacionParametrizacion  " +e);
         throw new RuntimeException(e);      
      }
		log.debug("procesoActualizacionParametrizacion (S)");
		return actualizacionExitosa;
	}
	
	/**
	 * Graba en Bitacora
	 * @param nombres
	 * @param claveUsuario
	 * @param ic_pyme
	 * @param datosActuales
	 * @param datosAnteriores
	 * @param con
	 */
	public void grabaBitacora(AccesoDB con, List datosAnteriores, List datosActuales, String ic_pyme, String claveUsuario, String[] nombres, String cc_tipo_factoraje) {
		
		log.debug("grabaBitacora (E)");
           
		try{   
			con.conexionDB();
			StringBuffer valoresAnteriores = new StringBuffer();
			StringBuffer valoresActuales = new StringBuffer();
			
			if(cc_tipo_factoraje.equals("N")) {			
				for(int i=0; i < NOMBRESN.length-1; i++) {			
					String valorAnterior = datosAnteriores.get(i).toString();
					String valorActual = datosActuales.get(i).toString();	 			
					valoresAnteriores.append(NOMBRESN[i] + "=" + valorAnterior + "\n");
					valoresActuales.append(NOMBRESN[i] + "=" + valorActual + "\n");  			
				}
			}else  if(cc_tipo_factoraje.equals("V")) {	
			
				for(int i=0; i < NOMBRESV.length-1; i++) {			
						String valorAnterior = datosAnteriores.get(i).toString();
						String valorActual = datosActuales.get(i).toString();	 			
						valoresAnteriores.append(NOMBRESV[i] + "=" + valorAnterior + "\n");
						valoresActuales.append(NOMBRESV[i] + "=" + valorActual + "\n");  			
					}
			}else  if(  "D".equals(cc_tipo_factoraje)  ) {	
			
			    for(int i=0; i < nombresD.length-1; i++) {			
				String valorAnterior = datosAnteriores.get(i).toString();
				String valorActual = datosActuales.get(i).toString();	 			
				valoresAnteriores.append(nombresD[i] + "=" + valorAnterior + "\n");
				valoresActuales.append(nombresD[i] + "=" + valorActual + "\n");  			
			    }
			    }else  if(  "A".equals(cc_tipo_factoraje)  ) {  
			    
			        for(int i=0; i < nombresA.length-1; i++) {                  
			            String valorAnterior = datosAnteriores.get(i).toString();
			            String valorActual = datosActuales.get(i).toString();   
                                                                        
			            valoresAnteriores.append(nombresA[i] + "=" + valorAnterior + "\n");
			            valoresActuales.append(nombresA[i] + "=" + valorActual + "\n");                         
			        }
			    }
			
			Bitacora.grabarEnBitacora(con, "DESAUTOMA", "P",
				sgetNoNafinElectronico(ic_pyme, "P"), claveUsuario,
				valoresAnteriores.toString(), valoresActuales.toString());
					
		}catch(Exception e){
			con.terminaTransaccion(false);
         e.printStackTrace();
			log.error("Error en  grabaBitacora  " +e);
         throw new RuntimeException(e);         
      }finally{       
          if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
		log.debug("grabaBitacora (S)");
	}
	
	/**
	 * Obtiene el n�mero de N@E asociado al afiliado
	 * @param claveAfiliado Clave del afiliado (ic_epo, ic_if, ic_pyme, etc)
	 * @param con Conexion a la BD.
	 * @param tipoAfiliado Tipo de Afiliado: E Epo P Pyme I If... etc.
	 */
	public String sgetNoNafinElectronico(String claveAfiliado, String tipoAfiliado){
		AccesoDB	con   =  new AccesoDB();
		log.debug("sgetNoNafinElectronico (E)");
		String numNafinElectronico = null;
		String strSQL =
				" SELECT ic_nafin_electronico " +
				" FROM comrel_nafin " +
				" WHERE ic_epo_pyme_if = ? " +
				" 	AND cg_tipo = ? ";
		try{
			con.conexionDB();
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(claveAfiliado));
			ps.setString(2, tipoAfiliado);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
					numNafinElectronico = rs.getString("ic_nafin_electronico");
			}
			rs.close();
			ps.close();
		}catch(Exception e){
			con.terminaTransaccion(false);
         e.printStackTrace();
			log.error("Error en  sgetNoNafinElectronico  " +e);
         throw new RuntimeException(e);         
      }finally{       
        if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
		
		log.debug("sgetNoNafinElectronico (S)");
		
		return numNafinElectronico;
	}
	
	/**
	 * Obtiene en una lista la fecha y hora actual ...
	 * @return 
	 */
	public List fechaHoraOperacion(){
		List ls = new ArrayList();
		
		Calendar cal= Calendar.getInstance();
		int diaHoy 	= 	cal.get(Calendar.DAY_OF_MONTH);
		int mes		=	cal.get(Calendar.MONTH);
		int anio 	=	cal.get(Calendar.YEAR);
		
		int hora	=	cal.get(Calendar.HOUR);
		int min	=	cal.get(Calendar.MINUTE);
		int seg	=	cal.get(Calendar.SECOND);
		
		ls.add( diaHoy + "/" + mes + "/" + anio );
		ls.add( hora + ":" + min +":" + seg);
		
		return ls;
	}
	
	
	
	/**
	 * 
	 * @return 
	 * @param login
	 * @param num_sirac_pyme
	 * @param producto
	 * @param ic_pyme
	 * @param nombre_pyme
	 * @param rfc_pyme
	 * @param num_pyme
	 * @param ic_epo
	 */
	public List getProveedores(String ic_epo, String num_pyme, String rfc_pyme, String nombre_pyme,
		  String ic_pyme,String producto,String num_sirac_pyme, String login) {
		  
		  AccesoDB	con   =  new AccesoDB();
			log.debug("getProveedores (E)");
			StringBuffer condicion = new StringBuffer();
			String		qrySentencia	= "";
			String 		hint 				= "";
			PreparedStatement 	ps				= null;
			ResultSet				rs				= null;
			HashMap		datos	=	null;
			List 			lista	=	new ArrayList();
			List 			conditions	=	new ArrayList();
			
			try {		
				con.conexionDB();
		
					boolean consideraUsuario = false;
					String claveAfiliado = null;   
					String tipoAfiliado = null;
		
					if(login != null && !login.equals("")) {
		
						UtilUsr utilUsr = new UtilUsr();
						Usuario usr = utilUsr.getUsuario(login);
						claveAfiliado = usr.getClaveAfiliado();
						tipoAfiliado = usr.getTipoAfiliado();
		
						if (tipoAfiliado != null && tipoAfiliado.equals("P") && claveAfiliado != null && !claveAfiliado.equals("")) {
							condicion.append("	AND p.ic_pyme = ? ");
							consideraUsuario = true;
						}
					}
		
		
					if(!"".equals(num_pyme)){
						condicion.append("	AND crn.ic_nafin_electronico = ? ");
					}
					
					if(!"".equals(rfc_pyme)) {
						if (rfc_pyme.indexOf('*')==0) {	//Se elimina el primer * para permitir que se use el indice sobre cg_rfc
							rfc_pyme = rfc_pyme.substring(1);
						}
						condicion.append(" AND cg_rfc LIKE nvl(replace(upper(?), '*', '%'), '%') ");
						hint =  "/*+  use_nl(p pe crn) index(p IN_COMCAT_PYME_11_NUK) */";
					}
					
					if(!"".equals(nombre_pyme)) {
						condicion.append("	AND cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
						hint = " /*+ index (p in_comcat_pyme_13_ctxcat) */ ";
					}
						
					if(!"".equals(num_sirac_pyme))
						condicion.append(" AND p.in_numero_sirac = ? ");
		
					//Cuando la busqueda sea por N@E o RFC se omite la tabla com_documento
					boolean consideraComDocumento;
					if (!"".equals(num_pyme) || !"".equals(rfc_pyme)) {
						consideraComDocumento = false;
					} else {
						consideraComDocumento = true;
					}
					
					qrySentencia =
						" SELECT   " +
						((consideraComDocumento)?" /*+ use_nl(p pe crn d)*/":hint) +
						"           distinct p.ic_pyme,"   +
						"           '' as cg_pyme_epo_interno"+
						"				, p.cg_razon_social,"   +
						"           crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega,"   +
						"           crn.ic_nafin_electronico"   +
						"     FROM comrel_pyme_epo pe"   +
						((consideraComDocumento)?", com_documento d":"") +
						" 		 , comrel_nafin crn"   +
						" 		 , comcat_pyme p		 "   +
						"    WHERE p.ic_pyme = pe.ic_pyme"   +
						((consideraComDocumento)?" AND d.ic_pyme = pe.ic_pyme AND d.ic_epo = pe.ic_epo ":"") +
						"    AND pe.ic_pyme = crn.ic_epo_pyme_if"   +
						"    AND crn.cg_tipo = 'P'"   +
						"    AND pe.cs_habilitado = 'S'"   +
						"    AND pe.cg_pyme_epo_interno IS NOT NULL "   +
						"    AND p.cs_habilitado = 'S'"   +
						condicion+" "+
						" ORDER BY p.cg_razon_social";
						 
					
					if(consideraUsuario) {
						conditions.add(claveAfiliado);
					}
					if(!"".equals(num_pyme)) {
						conditions.add(num_pyme);
					}
					if(!"".equals(rfc_pyme)) {
						conditions.add(rfc_pyme);
					}
					if(!"".equals(nombre_pyme)) {
						conditions.add(nombre_pyme);
					}
					if(!"".equals(num_sirac_pyme)){
						conditions.add(num_sirac_pyme);
					}
					ps = con.queryPrecompilado(qrySentencia, conditions);
					
		
				rs = ps.executeQuery();
				while(rs.next()) {
					datos = new HashMap();
					datos.put("clave",rs.getString(1));
					datos.put("1",rs.getString(2));
					datos.put("3",rs.getString(3));
					datos.put("descripcion",rs.getString(4));
					datos.put("5",rs.getString(5));
					lista.add(datos);  
				}
				
				rs.close();
				ps.close();
			} catch(Exception e){
				con.terminaTransaccion(false);
				e.printStackTrace();				
				log.error("Error en  getProveedores  " +e);
				throw new RuntimeException(e);
			}finally{
				 if(con.hayConexionAbierta()) {
					con.terminaTransaccion(true);
					con.cierraConexionDB();
				}
			}
			log.debug("getProveedores (S)");
			return lista;
		}
	
	/**
	 * Obtiene la direccion de la PYME
	 * @return 
	 * @param ic_pyme
	 */
	public List getDomicilioPyme(String ic_pyme) {
		AccesoDB	con   		=  new AccesoDB();
		String	condicion	= "";
		List 		lista 		= new ArrayList();
		HashMap 	datos 		= null;
		PreparedStatement 	ps				= null;
		ResultSet				rs				= null;
		log.debug("getDomicilioPyme (E)");
		try {
			con.conexionDB();
			
			String sQuery = " SELECT P.cg_rfc as rfcPyme, D.cg_calle||' '||D.cg_numero_ext||' '||D.cg_numero_int as domicilio, "+
							" D.cg_colonia as colonia, D.cn_cp as codigoPostal, D.cg_telefono1 as telefono1 "+
							" FROM comcat_pyme P, com_domicilio D "+
							" WHERE P.ic_pyme = D.ic_pyme "+
							" AND P.ic_pyme = ? ";
					
			ps = con.queryPrecompilado(sQuery);
			ps.setString(1, ic_pyme);
			rs = ps.executeQuery();
			if(rs.next()) {
				datos = new HashMap();
				datos.put("rfcPyme", (rs.getString("rfcPyme")==null?"":rs.getString("rfcPyme")));
				datos.put("domicilio", (rs.getString("domicilio")==null?"":rs.getString("domicilio")));
				datos.put("colonia", (rs.getString("colonia")==null?"":rs.getString("colonia")));
				datos.put("codigoPostal", (rs.getString("codigoPostal")==null?"":rs.getString("codigoPostal")));
				datos.put("telefono1", (rs.getString("telefono1")==null?"":rs.getString("telefono1")));
				lista.add(datos);
			}
			  
				rs.close(); 
				ps.close();
		} catch(Exception e){
			log.error("Error en  getDomicilioPyme  " +e);
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
			log.debug("getDomicilioPyme (S)");
		return lista;
		}
		
		/**
	 * 	 metodo que unicamente actualiza la parametrizaci�n del Descuento Autom�tico
	 * @return 
	 * @param strLogin
	 * @param ic_pyme
	 * @param operarDsctoAut
	 */
		public boolean actOperarDescuentoAutomatico(String operarDsctoAut, String ic_pyme, String strLogin, String xEPOIF){
		boolean hayActualiacion = false;		 	
		AccesoDB 	      con   =  new AccesoDB();
      PreparedStatement ps    =  null;
		List	lVarBind	= new ArrayList();
			
		log.debug("actOperarDescuentoAutomatico (E)");		   
		 
      try{	      	
      	con.conexionDB();	
			
			String strQry = " UPDATE COMCAT_PYME 				" +
							"		SET CS_DSCTO_AUTOMATICO = ?	" +
							"		WHERE	IC_PYME = ?					";
 			
			lVarBind		= new ArrayList();
			lVarBind.add( operarDsctoAut);
			lVarBind.add(new Integer(ic_pyme.trim()));
			
			ps = con.queryPrecompilado(strQry, lVarBind);	
			ps.executeUpdate();			
			ps.close();
			
			if(xEPOIF.equals("S"))  { // solo cuando se cambia la opci�n Si Autorizo y NO Autorizo
				strQry = " UPDATE COMREL_DSCTO_AUT_PYME				" +
							"	SET CS_DSCTO_AUTOMATICO_PROC = ? 		"+
							"	WHERE	IC_PYME = ?					"+
							" AND CS_DSCTO_AUTOMATICO_DIA  in('P', 'U')  ";
		
				lVarBind		= new ArrayList();
				lVarBind.add( operarDsctoAut);
				lVarBind.add(new Integer(ic_pyme.trim()));			
				ps = con.queryPrecompilado(strQry, lVarBind);						
				ps.executeUpdate();
				ps.close();			
			}     	  				
			hayActualiacion = true;
						   
      }catch(Exception e){
			hayActualiacion  = false;
			con.terminaTransaccion(false);
         e.printStackTrace();
			log.error("Error en  actOperarDescuentoAutomatico (S)" +e);
         throw new RuntimeException(e);      
      }finally{
      	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}	
      } 
		log.debug("actOperarDescuentoAutomatico (S)");	
		return hayActualiacion;   
	}
	
    /**
     * 2017_003
     * @param icPyme     
     * @return
     */
      public int existenEposFactorajeDistribuido(String icPyme)throws AppException {
	
	log.debug("existenEposFactorajeDistribuido (E)");
	AccesoDB con   =  new AccesoDB();
	ResultSet rs	   =  null;
	PreparedStatement ps    =  null;	
	int total =0;
	StringBuilder qrySentencia = new StringBuilder();
	List lVarBind	= new ArrayList();
	
	try{
	  
	    con.conexionDB(); 
	  	  
	    qrySentencia = new StringBuilder();	   
	    qrySentencia.append(" SELECT count(*) as total "  +
				    " FROM  comrel_pyme_epo dap,  comrel_producto_epo pre, comcat_epo e    "  +
				    " WHERE e.ic_epo = dap.ic_epo   "  +
				    " AND dap.ic_epo = pre.ic_epo  "  +
				    " AND dap.ic_epo = pre.ic_epo  "  +
				    " AND dap.cs_aceptacion = ? "  +
				    " AND dap.ic_pyme = ? "  +
				    " AND pre.ic_producto_nafin = ?   " 	+
				    " AND pre.CS_FACTORAJE_DISTRIBUIDO = ?  ")	;  
         	
	    lVarBind		= new ArrayList();
	    lVarBind.add("H");
	    lVarBind.add(new Integer(icPyme));
	    lVarBind.add("1");
	    lVarBind.add( "S");
	    	
	    log.debug("Query  "+qrySentencia);			
	    log.debug("lVarBind "+lVarBind);			
			
	     ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);		
	     rs	= ps.executeQuery();	
			
	     if(rs.next()){
		total =rs.getInt("total");
	     } 
	    rs.close();
			
	}catch(Exception e) {
	    log.error("Exception en existenEposFactorajeDistribuido. "+e);
	    throw new AppException("Error al obtener respuesta sobre epos exitentes para factoraje distribuido  ");
	} finally {
	    if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	    }
	}
	log.debug("existenEposFactorajeDistribuido (S)");
	return total;
    }
    
    
    
    ///////////////////////////////////
    /**
     * Obtengo ls epos que estan parametrizadas para operar Descuento Autom�tico EPO
     * @param icPyme 
     * @return
     * @throws AppException
     */
    public int getEposOperanDescAutomticoEPO(String icPyme)throws AppException {
      
      log.debug("getEposOperanDescAutomticoEPO (E)");
      AccesoDB con   =  new AccesoDB();
      ResultSet rs       =  null;
      PreparedStatement ps    =  null;        
      int total =0;
      StringBuilder qrySentencia = new StringBuilder();
      List lVarBind   = new ArrayList();
      
      try{
        
          con.conexionDB(); 
                
          qrySentencia = new StringBuilder();    
          qrySentencia.append(" SELECT count(*) as total "  +
                                  " FROM  comrel_pyme_epo dap,  comrel_producto_epo pre, comcat_epo e    "  +
                                  " WHERE e.ic_epo = dap.ic_epo   "  +
                                  " AND dap.ic_epo = pre.ic_epo  "  +
                                  " AND dap.ic_epo = pre.ic_epo  "  +
                                  " AND dap.cs_aceptacion = ? "  +
                                  " AND dap.ic_pyme = ? "  +
                                  " AND pre.ic_producto_nafin = ?   "         +
                                  " AND pre.CS_DESC_AUTO_EPO = ?  " +
							      " AND dap.cs_desc_automa_epo = ?   ")  ;  
              
          lVarBind            = new ArrayList();
          lVarBind.add("H");
          lVarBind.add(new Integer(icPyme));
          lVarBind.add("1");
          lVarBind.add( "S");
		  lVarBind.add( "S");
              
          log.debug("Query  "+qrySentencia +"\n lVarBind "+lVarBind);                    
                      
           ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);             
           rs = ps.executeQuery();    
                      
           if(rs.next()){
              total =rs.getInt("total");
           } 
          rs.close();
                      
      }catch(Exception e) {
          log.error("Exception en getEposOperanDescAutomticoEPO. "+e);
          throw new AppException("Error al obtener respuesta sobre epos  que operen Descuento Autom�tico EPO  ");
      } finally {
          if(con.hayConexionAbierta()){
              con.cierraConexionDB();
          }
      }
      log.debug("getEposOperanDescAutomticoEPO (S)");
      return total;
    }

    /**
     * metodo para saber si se opera Descuento Autom�tico EPO
     * @return
	 * @throws NafinException
	 */
    public String getPymeDescAutoEPO(String clavePyme ) throws NafinException {

        AccesoDB con = new AccesoDB();

        StringBuilder qrySentencia = new StringBuilder();       
        List varBind = new ArrayList();
        String respuesta ="N";
        try {

            con.conexionDB();
           
            qrySentencia = new StringBuilder();
            qrySentencia.append(" SELECT  COUNT(*) AS TOTAL  FROM  comrel_pyme_epo " + 
								" WHERE cs_desc_automa_epo    =  ?   "+
								 " and ic_pyme = ?  "); 
			varBind.add("S");
			varBind.add(clavePyme);
            
            log.debug("qrySentencia  " + qrySentencia.toString() + " varBind    \n " + varBind);

            PreparedStatement ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
				if(rs.getInt(1)>0) {
                    respuesta ="S";
                }                
            }
            rs.close();
            ps.close();


        } catch (Exception e) {
            log.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return respuesta;
    }
}
