package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaCedulaEvaluacionPYME {
	
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaCedulaEvaluacionPYME.class);

	public HashMap generaCedulaEvaluacion(HashMap parametros)
		throws Exception{
	
		log.info("generaCedulaEvaluacion(E)");
		
		String ic_epo 				= (parametros.get("claveEPO")							== null)?"":(String) parametros.get("claveEPO");
		String ic_pyme 			= (parametros.get("clavePYME")						== null)?"":(String) parametros.get("clavePYME");
		String ic_epo1 			= (parametros.get("claveEPOSRelacionadas")		== null)?"":(String) parametros.get("claveEPOSRelacionadas");
		String ic_moneda 			= (parametros.get("claveMoneda")						== null)?"":(String) parametros.get("claveMoneda");
		String factor_cre 		= (parametros.get("factorCredicadenas")			== null)?"":(String) parametros.get("factorCredicadenas");
		String meses_consec 		= (parametros.get("ultimosMesesConsecutivos")	== null)?"":(String) parametros.get("ultimosMesesConsecutivos");
		String meses_con_pub 	= (parametros.get("mesesConPublicacion")			== null)?"":(String) parametros.get("mesesConPublicacion");
		String meses_prom 		= (parametros.get("mesesAPromediar")				== null)?"":(String) parametros.get("mesesAPromediar");
		String meses_eval 		= (parametros.get("mesesAEvaluar")					== null)?"":(String) parametros.get("mesesAEvaluar");
		String periodo_inicio 	= (parametros.get("fechaDeValuacionInicial")		== null)?"":(String) parametros.get("fechaDeValuacionInicial");
		String periodo_fin 		= (parametros.get("fechaDeValuacionFinal")		== null)?"":(String) parametros.get("fechaDeValuacionFinal");
	
		String hidAction 			= (parametros.get("hidAction")						== null)?"":(String) parametros.get("hidAction");
		String hidAction812 		= (parametros.get("hidAction812")					== null)?"":(String) parametros.get("hidAction812");
 
		String nombreUsuario 	= (parametros.get("nombreUsuario")					== null)?"":(String) parametros.get("nombreUsuario");
		
		HashMap 	resultado					= new HashMap();
		List		detalleOperaciones		= new ArrayList();
		List		totalDetalleOperaciones	= new ArrayList();
		HashMap 	registroDetalle			= null;
		
		// Variables auxiliares
		String	fecha1			= "";
		String	fecha2			= "";
		String	aux_inicio		= "";
		String	aux_fin			= "";
		String	aux_cal			= "";
		String	dia_				= "";
		String	mes_				= "";
		String	anyo_				= "";
		String	meses[] 			= {"Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"};
		String 	ic_epo2 			= "231";		// No. de cadena de FONACOT 2.
		String 	query 			= "";
		
		AccesoDB 			con 	= new AccesoDB();
		PreparedStatement ps 	= null;
		ResultSet 			rs 	= null;
		
		int 		DocsPubs 				= 0;
		double	MntDocsPubs 			= 0;
		int 		DocsOper					= 0;
		double	MntDocsOper 			= 0;
		int 		DocsSinOper 			= 0;
		double	MntDocsSinOper 		= 0;
		
		double 	totalDocsPubs 			= 0;
		double	totalMntDocsPubs		= 0;
		double 	totalDocsOper			= 0;
		double	totalMntDocsOper		= 0;
		double 	totalDocsSinOper		= 0;
		double	totalMntDocsSinOper	= 0;
		
		String 	rs_fechaHoy				= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		Calendar fechaVal 				= new GregorianCalendar();
		
		dia_	= rs_fechaHoy.substring(0, 2);
		mes_	= rs_fechaHoy.substring(3, 5);
		anyo_	= rs_fechaHoy.substring(6, 10);
		
		fechaVal.set(Integer.parseInt(anyo_), Integer.parseInt(mes_)-1, Integer.parseInt(dia_));
		fechaVal.add(Calendar.MONTH, -1);
		fechaVal.set(Calendar.DAY_OF_MONTH, fechaVal.getActualMaximum(Calendar.DAY_OF_MONTH));
		periodo_fin = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fechaVal.getTime());
		if( !"".equals(meses_prom) && "".equals(hidAction812) ) {
			
			dia_				= periodo_fin.substring(0, 2);
			mes_				= periodo_fin.substring(3, 5);
			anyo_				= periodo_fin.substring(6, 10);
			fechaVal.set(Integer.parseInt(anyo_), Integer.parseInt(mes_)-1, Integer.parseInt(dia_));	
			fechaVal.add(Calendar.MONTH, -(Integer.parseInt(meses_prom)-1));
			fechaVal.set(Calendar.DAY_OF_MONTH, fechaVal.getActualMinimum(Calendar.DAY_OF_MONTH));
			periodo_inicio = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fechaVal.getTime());
			
		} else {
			
			if(!"".equals(hidAction812)) {
				dia_				= periodo_fin.substring(0, 2);
				mes_				= periodo_fin.substring(3, 5);
				anyo_				= periodo_fin.substring(6, 10);
				fechaVal.set(Integer.parseInt(anyo_), Integer.parseInt(mes_)-1, Integer.parseInt(dia_));	
				fechaVal.add(Calendar.MONTH, -(Integer.parseInt(meses_eval)-1));
				fechaVal.set(Calendar.DAY_OF_MONTH, fechaVal.getActualMinimum(Calendar.DAY_OF_MONTH));
				periodo_inicio = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fechaVal.getTime());
			}
			
		}
		
		resultado.put("fechaDeValuacionInicial",	periodo_inicio	);
		resultado.put("fechaDeValuacionFinal",		periodo_fin		);
		
		try {
			
			// Conectar a la Base de Datos
			con.conexionDB();
			
			// "GENERAR_CEDULA" � "GENERAR_CEDULA_8x12"
			if("G".equals(hidAction) || "G".equals(hidAction812) ) {
				
				String 	rs_nomPyme						= "";
				String 	rs_rfc							= "";
				String 	rs_tel							= "";
				String 	rs_nomEpo						= "";
				String 	rs_fechaIngreso				= "";
				String 	rs_fechaInicio					= "";
				String 	rs_mtoCredito					= "";
				
				int 		mesespublicando 				= 0;
				int 		mesesporevaluar_pub 			= 0;
				int 		mesesporevaluar_oper 		= 0;
				int 		mesesporevaluar_sin_oper 	= 0;
				boolean	publicaconsec					= false;
				boolean	suceptible						= true;
				
				query = 
					" SELECT TO_CHAR(MIN(df_alta), 'dd/mm/yyyy') AS fechainicio "   +
					" FROM com_documento "   +
					" WHERE ic_pyme = ? "  +
					" AND ic_epo = ? ";
				ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(ic_pyme));
				ps.setInt(2, Integer.parseInt(ic_epo1));
				rs = ps.executeQuery();
				ps.clearParameters();
				if(rs.next())
					rs_fechaInicio	= (rs.getString("fechainicio")==null)?"":rs.getString("fechainicio");
				rs.close();
				if(ps!=null) ps.close();
				
				query = 
					" SELECT TO_CHAR(SYSDATE, 'dd/mm/yyyy') AS fechahoy,"   +
					"     pym.cg_razon_social AS nompyme,"   +
					"     pym.cg_rfc AS rfc,"   +
					"     pym.cg_telefono AS tel,"   +
					"     epo.cg_razon_social AS nomepo,"   +
					"     TO_CHAR(rpe.df_aceptacion, 'dd/mm/yyyy') AS fechaingreso"   +
					" FROM comcat_pyme pym, comcat_epo epo, comrel_pyme_epo rpe"   +
					" WHERE rpe.ic_epo  = epo.ic_epo "   +
					" AND rpe.ic_pyme  = pym.ic_pyme "   +
					" AND pym.ic_pyme = ? "   +
					" AND epo.ic_epo = ? "  ;
				ps = con.queryPrecompilado(query);
				ps.setInt(1, Integer.parseInt(ic_pyme));
				ps.setInt(2, Integer.parseInt(ic_epo1));
				rs = ps.executeQuery();
				ps.clearParameters();
				if(rs.next()) {
					rs_fechaHoy			= (rs.getString("fechahoy")		==null)?"":rs.getString("fechahoy");
					rs_nomPyme			= (rs.getString("nompyme")			==null)?"":rs.getString("nompyme");
					rs_rfc				= (rs.getString("rfc")				==null)?"":rs.getString("rfc");
					rs_tel				= (rs.getString("tel")				==null)?"":rs.getString("tel");
					rs_nomEpo			= (rs.getString("nomepo")			==null)?"":rs.getString("nomepo");
					rs_fechaIngreso	= (rs.getString("fechaingreso")	==null)?"":rs.getString("fechaingreso");
				}
				rs.close();
				if(ps!=null) ps.close();
			
				int mesesOperando = 0;
				if(!"".equals(rs_fechaInicio)) {
					Calendar ini = new GregorianCalendar();
					dia_	= rs_fechaInicio.substring(0, 2);
					mes_	= rs_fechaInicio.substring(3, 5);
					anyo_	= rs_fechaInicio.substring(6, 10);
					ini.set(Integer.parseInt(anyo_), Integer.parseInt(mes_)-1, Integer.parseInt(dia_));	
					String fecha_aux1 = rs_fechaInicio.substring(3, 10);
					String fecha_aux2 = rs_fechaHoy.substring(3, 10);
					mesesOperando 		= 1;
					while(!fecha_aux1.equals(fecha_aux2)){
						ini.add(Calendar.MONTH, 1);
						fecha_aux1 = new java.text.SimpleDateFormat("MM/yyyy").format(ini.getTime());
						mesesOperando++;
					}
				}
			
				resultado.put("fechaEmision",					rs_fechaHoy		);
				resultado.put("nombreProveedor",				rs_nomPyme		);
				resultado.put("rfc",								rs_rfc			);
				resultado.put("telefonos",						rs_tel			);
				resultado.put("nombreEpo",						rs_nomEpo		);

				resultado.put("fechaIngresoCadena",			rs_fechaIngreso);
				resultado.put("fechaInicioOperaciones",	rs_fechaInicio	);
				resultado.put("numeroMesesOperando",		String.valueOf(mesesOperando)	);
						
				int 		i		= 0;
				int 		ind	= 0;
				String 	aux	= "";
	
				if( !"".equals(periodo_inicio) && !"".equals(periodo_fin) ) {
					
					Calendar cal = new GregorianCalendar();
					dia_			= periodo_fin.substring(0, 2);
					mes_			= periodo_fin.substring(3, 5);
					anyo_			= periodo_fin.substring(6, 10);
					cal.set(Integer.parseInt(anyo_), Integer.parseInt(mes_)-1, Integer.parseInt(dia_));	
					aux_inicio	= periodo_inicio.substring(3, 10);
					aux_fin 		= periodo_fin.substring(3, 10);
					
					while(!fecha1.equals(periodo_inicio)) {

						aux_cal 	= new java.text.SimpleDateFormat("MM/yyyy").format(cal.getTime());
						mes_		= aux_cal.substring(0, 2);
						anyo_		= aux_cal.substring(3, 7);
						aux		= meses[Integer.parseInt(mes_)-1]+"/"+anyo_;
						
						if(aux_inicio.equals(aux_cal)) 
							fecha1 = periodo_inicio;
						else
							fecha1 = cal.getActualMinimum(Calendar.DAY_OF_MONTH)+"/"+aux_cal;
						
						if(aux_fin.equals(aux_cal)) 
							fecha2 = periodo_fin;
						else
							fecha2 = cal.getActualMaximum(Calendar.DAY_OF_MONTH)+"/"+aux_cal;
						
						if(fecha1.length()<10)
							fecha1 = "0"+fecha1;
						
						if(fecha2.length()<10)
							fecha2 = "0"+fecha2;
						
						cal.add(Calendar.MONTH, -1);
						i++;
						
						if(!"".equals(rs_fechaInicio)) {
							
							//PUBLICADOS
							if(ic_epo1.equals("30")) {	// No. de cadena de FONACOT 1.
								
								ind = 0;
								query = " select count(FONACOT.noDocto), nvl(sum(FONACOT.monto),0) from ("   +
									"		select cad1.ig_numero_docto as noDocto, cad1.fn_monto as monto"   +
									"       from com_documento cad1"   +
									"       where cad1.ic_epo = ?"   +
									"       and cad1.ic_pyme = ?"   +
									"       and cad1.ic_moneda = ?"   +
									"       and cad1.fn_monto > 0"   +
									"       and TRUNC(cad1.df_alta) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"       and (select count(1) from com_documento cad2"   +
									"				where cad2.ic_epo = ?"   +
									"				and cad2.ic_pyme = ?"   +
									"				and cad2.ic_moneda = ?"   +
									"				and cad2.fn_monto > 0"   +
									"				and cad2.ic_estatus_docto <> 5"   +
									"				and cad2.ig_numero_docto like '%'||cad1.ig_numero_docto) = 0"   +
									"       union all"   +
									"       select cad1.ig_numero_docto as noDocto, cad1.fn_monto as monto"   +
									"       from com_documento cad1"   +
									"       where cad1.ic_epo = ?"   +
									"       and cad1.ic_pyme = ?"   +
									"       and cad1.ic_moneda = ?"   +
									"       and cad1.fn_monto > 0"   +
									"       and TRUNC(cad1.df_alta) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"       and (select count(1) from com_documento cad2"   +
									"				where cad2.ic_epo = ?"   +
									"				and cad2.ic_pyme = ?"   +
									"				and cad2.ic_moneda = ?"   +
									"				and cad2.fn_monto > 0"   +
									"				and cad2.ic_estatus_docto <> 5"   +
									"				and cad2.ig_numero_docto like '%'||cad1.ig_numero_docto) > 0"   +
									"       union all  "   +
									"       select cad2.ig_numero_docto as noDocto, cad2.fn_monto as monto"   +
									"       from com_documento cad2"   +
									"       where cad2.ic_epo = ?"   +
									"       and cad2.ic_pyme = ?"   +
									"       and cad2.ic_moneda = ?"   +
									"       and cad2.fn_monto > 0"   +
									"       and cad2.ic_estatus_docto <> 5"   +
									"       and TRUNC(cad2.df_alta) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"       and cad2.ig_numero_docto not in ("   +
									"       	select c2.ig_numero_docto"   +
									"           from com_documento c2"   +
									"           where c2.ic_epo = ?"   +
									"           and c2.ic_pyme = ?"   +
									"           and c2.ic_moneda = ?"   +
									"           and c2.fn_monto > 0"   +
									"           and c2.ic_estatus_docto <> 5"   +
									"           and (select count(1) from com_documento c1"   +
									"					where c1.ic_epo = ?"   +
									"					and c1.ic_pyme = ?"   +
									"					and c1.ic_moneda = ?"   +
									"					and c1.fn_monto > 0"   +
									"					and TRUNC(c1.df_alta) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"					and c2.ig_numero_docto like '%'||c1.ig_numero_docto) > 0"   +
									"		)"   +
									"       and (select count(1) from com_documento cad1"   +
									"       		where cad1.ic_epo = ?"   +
									"       		and cad1.ic_pyme = ?"   +
									"       		and cad1.ic_moneda = ?"   +
									"       		and cad1.fn_monto > 0"   +
									"       		and TRUNC(cad1.df_alta) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"       		and cad2.ig_numero_docto like '%'||cad1.ig_numero_docto) = 0"   +
									" ) FONACOT"  ;
								ps = con.queryPrecompilado(query);
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
					
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
					
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								
							} else {
								query = " SELECT COUNT(*), SUM(doc.fn_monto) "   +
											" FROM com_documento doc "   +
											" WHERE doc.ic_epo = ? "   +
											" AND doc.ic_pyme = ? "   +
											" AND doc.ic_moneda = ? "   +
											" AND doc.fn_monto > 0 "   +
											" AND TRUNC(doc.df_alta) BETWEEN TRUNC(TO_DATE( ? , 'dd/mm/yyyy')) AND TRUNC(TO_DATE( ? , 'dd/mm/yyyy'))"  ;
								//log.debug("<br> query: "+query);								
								ps = con.queryPrecompilado(query);
								ps.setString(1, ic_epo1);
								ps.setString(2, ic_pyme);
								ps.setString(3, ic_moneda);
								ps.setString(4, fecha1);
								ps.setString(5, fecha2);
							}
							rs = ps.executeQuery();
							ps.clearParameters();
							if(rs.next()) {
								DocsPubs 			= rs.getInt(1);
								if(DocsPubs>0)
									mesesporevaluar_pub++;
								if(Integer.parseInt(meses_consec)>=i && "".equals(hidAction812)) {
									if(DocsPubs>0)
										mesespublicando++;
									else
										mesespublicando=0;
									if(mesespublicando==Integer.parseInt(meses_consec))
										publicaconsec = true;
								} else {
									if(DocsPubs>0)
										mesespublicando++;
								}
								totalDocsPubs 		+= DocsPubs;
								MntDocsPubs 		= rs.getString(2)==null?0:Double.parseDouble(rs.getString(2));
								totalMntDocsPubs	+= MntDocsPubs;
								if(i<=Integer.parseInt(meses_consec) && "".equals(hidAction812))
									if(MntDocsPubs==0)
										suceptible = false;
							}
							rs.close();
							if(ps!=null) ps.close();
				
							//OPERADOS
							if(ic_epo1.equals("30")) {	// No. de cadena de FONACOT 1.
								ind 	= 0;
								query = " select count(FONACOT.noDocto), nvl(sum(FONACOT.monto),0) from ("   +
									"		select cad1.ig_numero_docto as noDocto, cad1.fn_monto as monto"   +
									" 		from com_documento cad1, com_docto_seleccionado ds1"   +
									" 		where cad1.ic_documento = ds1.ic_documento"   +
									" 		and cad1.ic_epo = ?"   +
									" 		and cad1.ic_pyme = ?"   +
									" 		and cad1.ic_moneda = ?"   +
									" 		and cad1.ic_estatus_docto in (4, 11, 12, 16)"   +
									" 		and TRUNC(ds1.df_fecha_seleccion) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									" 		and (select count(1) from com_documento cad2"   +
									"				where cad2.ic_epo = ?"   +
									"				and cad2.ic_pyme = ?"   +
									"				and cad2.ic_moneda = ?"   +
									"				and cad2.ic_estatus_docto <> 5"   +
									"				and cad2.ig_numero_docto like '%'||cad1.ig_numero_docto) = 0"   +
									"		union all"   +
									" 		select cad1.ig_numero_docto as noDocto, cad1.fn_monto as monto"   +
									" 		from com_documento cad1, com_docto_seleccionado ds1"   +
									" 		where cad1.ic_documento = ds1.ic_documento"   +
									" 		and cad1.ic_epo = ?"   +
									" 		and cad1.ic_pyme = ?"   +
									" 		and cad1.ic_moneda = ?"   +
									" 		and cad1.ic_estatus_docto in (4, 11, 12, 16)"   +
									" 		and TRUNC(ds1.df_fecha_seleccion) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									" 		and (select count(1) from com_documento cad2"   +
									"				where cad2.ic_epo = ?"   +
									"				and cad2.ic_pyme = ?"   +
									"				and cad2.ic_moneda = ?"   +
									"				and cad2.ic_estatus_docto <> 5"   +
									"				and cad2.ig_numero_docto like '%'||cad1.ig_numero_docto) > 0"   +
									"		union all"   +
									"		select cad2.ig_numero_docto as noDocto, cad2.fn_monto as monto"   +
									" 		from com_documento cad2, com_docto_seleccionado ds1"   +
									" 		where cad2.ic_documento = ds1.ic_documento"   +
									" 		and cad2.ic_epo = ?"   +
									"		and cad2.ic_pyme = ?"   +
									"		and cad2.ic_moneda = ?"   +
									"		and cad2.ic_estatus_docto <> 5"   +
									"		and TRUNC(ds1.df_fecha_seleccion) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"		and cad2.ig_numero_docto not in ("   +
									"				select c2.ig_numero_docto"   +
									"     			from com_documento c2"   +
									"				where c2.ic_epo = ?"   +
									"				and c2.ic_pyme = ?"   +
									"				and c2.ic_moneda = ?"   +
									"				and c2.ic_estatus_docto <> 5"   +
									"				and (select count(1) from com_documento c1, com_docto_seleccionado ds1"   +
									"						where c1.ic_documento = ds1.ic_documento"   +
									"						and c1.ic_epo = ?"   +
									"						and c1.ic_pyme = ?"   +
									"						and c1.ic_moneda = ?"   +
									"						and c1.ic_estatus_docto in (4, 11, 12, 16)"   +
									"						and TRUNC(ds1.df_fecha_seleccion) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"						and c2.ig_numero_docto like '%'||c1.ig_numero_docto) > 0"   +
									"		)"   +
									"		and (select count(1) from com_documento cad1, com_docto_seleccionado ds1"   +
									"				where cad1.ic_documento = ds1.ic_documento"   +
									"				and cad1.ic_epo = ?"   +
									"				and cad1.ic_pyme = ?"   +
									"				and cad1.ic_moneda = ?"   +
									"				and cad1.ic_estatus_docto in (4, 11, 12, 16)"   +
									"				and TRUNC(ds1.df_fecha_seleccion) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"				and cad2.ig_numero_docto like '%'||cad1.ig_numero_docto) = 0"   +
									" ) FONACOT"  ;
								ps = con.queryPrecompilado(query);
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
					
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
							} else {
								query = " SELECT COUNT( * ), SUM(doc.fn_monto) "   +
											" FROM com_documento doc, com_docto_seleccionado sel "   +
											" WHERE doc.ic_documento = sel.ic_documento "   +
											" AND doc.ic_epo = ? "   +
											" AND doc.ic_pyme = ? "   +
											" AND doc.ic_moneda = ? "   +
											" AND doc.ic_estatus_docto IN (4, 11, 12, 16) "   +
											" AND TRUNC(sel.df_fecha_seleccion) BETWEEN TRUNC(TO_DATE(?, 'dd/mm/yyyy')) AND TRUNC(TO_DATE(?, 'dd/mm/yyyy'))"  ;
								ps = con.queryPrecompilado(query);
								ps.setString(1, ic_epo1);
								ps.setString(2, ic_pyme);
								ps.setString(3, ic_moneda);
								ps.setString(4, fecha1);
								ps.setString(5, fecha2);
							}
							rs = ps.executeQuery();
							ps.clearParameters();
							if(rs.next()) {
								DocsOper 			= rs.getInt(1);
								if(DocsOper>0)
									mesesporevaluar_oper++;
								totalDocsOper 		+= DocsOper;
								MntDocsOper 		= rs.getString(2)==null?0:Double.parseDouble(rs.getString(2));
								totalMntDocsOper	+= MntDocsOper;
							}
							rs.close();
							if(ps!=null) ps.close();
				
							//SIN OPERAR
							if(ic_epo1.equals("30")) {	// No. de cadena de FONACOT 1.
								ind = 0;
								query = " select count(FONACOT.noDocto), nvl(sum(FONACOT.monto),0) from ("   +
									"		select cad1.ig_numero_docto as noDocto, cad1.fn_monto as monto"   +
									"       from com_documento cad1"   +
									"       where cad1.ic_epo = ?"   +
									"       and cad1.ic_pyme = ?"   +
									"       and cad1.ic_moneda = ?"   +
									"       and cad1.fn_monto > 0"   +
									"       and cad1.ic_estatus_docto in (9, 10)"   +
									"       and TRUNC(cad1.df_fecha_venc) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"       and (select count(1) from com_documento cad2"   +
									"				where cad2.ic_epo = ?"   +
									"				and cad2.ic_pyme = ?"   +
									"				and cad2.ic_moneda = ?"   +
									"				and cad2.fn_monto > 0"   +
									"				and cad2.ic_estatus_docto <> 5"   +
									"				and cad2.ig_numero_docto like '%'||cad1.ig_numero_docto) = 0"   +
									"       union all"   +
									"       select cad1.ig_numero_docto as noDocto, cad1.fn_monto as monto"   +
									"       from com_documento cad1"   +
									"       where cad1.ic_epo = ?"   +
									"       and cad1.ic_pyme = ?"   +
									"       and cad1.ic_moneda = ?"   +
									"       and cad1.fn_monto > 0"   +
									"       and cad1.ic_estatus_docto in (9, 10)"   +
									"       and TRUNC(cad1.df_fecha_venc) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"       and (select count(1) from com_documento cad2"   +
									"				where cad2.ic_epo = ?"   +
									"				and cad2.ic_pyme = ?"   +
									"				and cad2.ic_moneda = ?"   +
									"				and cad2.fn_monto > 0"   +
									"				and cad2.ic_estatus_docto <> 5"   +
									"				and cad2.ig_numero_docto like '%'||cad1.ig_numero_docto) > 0"   +
									"       union all  "   +
									"       select cad2.ig_numero_docto as noDocto, cad2.fn_monto as monto"   +
									"       from com_documento cad2"   +
									"       where cad2.ic_epo = ?"   +
									"       and cad2.ic_pyme = ?"   +
									"       and cad2.ic_moneda = ?"   +
									"       and cad2.fn_monto > 0"   +
									"       and cad2.ic_estatus_docto <> 5"   +
									"       and TRUNC(cad2.df_fecha_venc) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"       and cad2.ig_numero_docto not in ("   +
									"       	select c2.ig_numero_docto"   +
									"           from com_documento c2"   +
									"           where c2.ic_epo = ?"   +
									"           and c2.ic_pyme = ?"   +
									"           and c2.ic_moneda = ?"   +
									"           and c2.fn_monto > 0"   +
									"           and c2.ic_estatus_docto <> 5"   +
									"           and (select count(1) from com_documento c1"   +
									"					where c1.ic_epo = ?"   +
									"					and c1.ic_pyme = ?"   +
									"					and c1.ic_moneda = ?"   +
									"					and c1.fn_monto > 0"   +
									"					and c1.ic_estatus_docto in (9, 10)"   +
									"					and TRUNC(c1.df_fecha_venc) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"					and c2.ig_numero_docto like '%'||c1.ig_numero_docto) > 0"   +
									"		)"   +
									"       and (select count(1) from com_documento cad1"   +
									"       		where cad1.ic_epo = ?"   +
									"       		and cad1.ic_pyme = ?"   +
									"       		and cad1.ic_moneda = ?"   +
									"       		and cad1.fn_monto > 0"   +
									"       		and cad1.ic_estatus_docto in (9, 10)"   +
									"       		and TRUNC(cad1.df_fecha_venc) between TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) and TRUNC(TO_DATE( ? ,'dd/mm/yyyy'))"   +
									"       		and cad2.ig_numero_docto like '%'||cad1.ig_numero_docto) = 0"   +
									" ) FONACOT"  ;
								ps = con.queryPrecompilado(query);
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
					
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
					
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo2);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
								ps.setString(++ind, ic_epo1);
								ps.setString(++ind, ic_pyme);
								ps.setString(++ind, ic_moneda);
								ps.setString(++ind, fecha1);
								ps.setString(++ind, fecha2);
							} else {
								query = " SELECT COUNT(*), SUM(doc.fn_monto) "   +
											" FROM com_documento doc "   +
											" WHERE doc.ic_epo = ? "   +
											" AND doc.ic_pyme = ? "   +
											" AND doc.ic_moneda = ? "   +
											" AND doc.ic_estatus_docto IN (9, 10) "   +
											" AND doc.fn_monto > 0 "   +
											" AND TRUNC(doc.df_fecha_venc) BETWEEN TRUNC(TO_DATE(?, 'dd/mm/yyyy')) AND TRUNC(TO_DATE(?, 'dd/mm/yyyy'))"  ;
								ps = con.queryPrecompilado(query);
								ps.setString(1, ic_epo1);
								ps.setString(2, ic_pyme);
								ps.setString(3, ic_moneda);
								ps.setString(4, fecha1);
								ps.setString(5, fecha2);
							}
							rs = ps.executeQuery();
							ps.clearParameters();
							if(rs.next()) {
								DocsSinOper 		= rs.getInt(1);
								if(DocsSinOper>0)
									mesesporevaluar_sin_oper++;
								totalDocsSinOper 	+= DocsSinOper;
								MntDocsSinOper 		= rs.getString(2)==null?0:Double.parseDouble(rs.getString(2));
								totalMntDocsSinOper	+= MntDocsSinOper;
							}
							rs.close();
							if(ps!=null) ps.close();
						//if(!"".equals(rs_fechaInicio))
						} else {
							if("".equals(hidAction812)) {
								suceptible 				= false;
								DocsPubs					= 0;
								totalDocsPubs			= 0;
								MntDocsPubs				= 0;
								totalMntDocsPubs		= 0;
								DocsOper 				= 0;
								totalDocsOper 			= 0;
								MntDocsOper 			= 0;
								totalMntDocsOper		= 0;
								DocsSinOper 			= 0;
								totalDocsSinOper 		= 0;
								MntDocsSinOper 		= 0;
								totalMntDocsSinOper	= 0;
							}
						}

						registroDetalle = new HashMap();	
						registroDetalle.put("MES_ANIO",				aux);
						registroDetalle.put("NUMERO_PUBLICADOS",	(DocsPubs	 > 0)?  ""+Comunes.formatoDecimal(DocsPubs,			0):"");
						registroDetalle.put("MONTO_PUBLICADOS",	(DocsPubs	 > 0)?"$ "+Comunes.formatoDecimal(MntDocsPubs,		2):"");
						registroDetalle.put("NUMERO_OPERADOS",		(DocsOper	 > 0)?  ""+Comunes.formatoDecimal(DocsOper,			0):"");
						registroDetalle.put("MONTO_OPERADOS",		(DocsOper	 > 0)?"$ "+Comunes.formatoDecimal(MntDocsOper,		2):"");
						registroDetalle.put("NUMERO_SIN_OPERAR",	(DocsSinOper > 0)?  ""+Comunes.formatoDecimal(DocsSinOper,		0):"");
						registroDetalle.put("MONTO_SIN_OPERAR",	(DocsSinOper > 0)?"$ "+Comunes.formatoDecimal(MntDocsSinOper,	2):"");
						detalleOperaciones.add(registroDetalle);
						
					}//while(!fecha2.equals(periodo_fin))
					
				}//	if(!"".equals(periodo_inicio) && !"".equals(periodo_fin))
				
				// AGREGAR DETALLE DE LAS OPERACIONES 
				resultado.put("detalleOperaciones", detalleOperaciones );
				
				//log.debug("<br>mesesporevaluar_pub:      "+mesesporevaluar_pub      );
				//log.debug("<br>mesesporevaluar_oper:     "+mesesporevaluar_oper     );
				//log.debug("<br>mesesporevaluar_sin_oper: "+mesesporevaluar_sin_oper );

				String aux_meses_prom = meses_prom;
				if(!"".equals(hidAction812)) {
					meses_prom 				= new Integer(mesespublicando).toString();
					mesesporevaluar_pub	=  mesesporevaluar_oper = mesesporevaluar_sin_oper = Integer.parseInt(meses_prom);
				}
	
				if("".equals(hidAction812)) {
					if(publicaconsec)
						suceptible = true;
					else
						suceptible = false;
				} else {
					if(mesespublicando >= Integer.parseInt(meses_con_pub))
						suceptible = true;
					else
						suceptible = false;
				}

				// AGREGAR TOTAL
				registroDetalle = new HashMap();	
				registroDetalle.put("MES_ANIO",				"Total:");
				registroDetalle.put("NUMERO_PUBLICADOS",	(totalDocsPubs		>0)?  ""+Comunes.formatoDecimal(totalDocsPubs,		 0):"");
				registroDetalle.put("MONTO_PUBLICADOS",	(totalDocsPubs		>0)?"$ "+Comunes.formatoDecimal(totalMntDocsPubs,	 2):"");
				registroDetalle.put("NUMERO_OPERADOS",		(totalDocsOper		>0)?  ""+Comunes.formatoDecimal(totalDocsOper,		 0):"");
				registroDetalle.put("MONTO_OPERADOS",		(totalDocsOper		>0)?"$ "+Comunes.formatoDecimal(totalMntDocsOper,	 2):"");
				registroDetalle.put("NUMERO_SIN_OPERAR",	(totalDocsSinOper	>0)?  ""+Comunes.formatoDecimal(totalDocsSinOper,	 0):"");
				registroDetalle.put("MONTO_SIN_OPERAR",	(totalDocsSinOper	>0)?"$ "+Comunes.formatoDecimal(totalMntDocsSinOper,2):"");
				totalDetalleOperaciones.add(registroDetalle);
						
				// AGREGAR PROMEDIO POR MES
				registroDetalle = new HashMap();	
				registroDetalle.put("MES_ANIO",				"Promedio por mes:");
				registroDetalle.put("NUMERO_PUBLICADOS",	(totalDocsPubs		>0)?  ""+Comunes.formatoDecimal(Math.round(totalDocsPubs/mesesporevaluar_pub),			0):"");
				registroDetalle.put("MONTO_PUBLICADOS",	(totalDocsPubs		>0)?"$ "+Comunes.formatoDecimal(totalMntDocsPubs/mesesporevaluar_pub,						2):"");
				registroDetalle.put("NUMERO_OPERADOS",		(totalDocsOper		>0)?  ""+Comunes.formatoDecimal(Math.round(totalDocsOper/mesesporevaluar_oper),			0):"");
				registroDetalle.put("MONTO_OPERADOS",		(totalDocsOper		>0)?"$ "+Comunes.formatoDecimal(totalMntDocsOper/mesesporevaluar_oper,						2):"");
				registroDetalle.put("NUMERO_SIN_OPERAR",	(totalDocsSinOper	>0)?  ""+Comunes.formatoDecimal(Math.round(totalDocsSinOper/mesesporevaluar_sin_oper),	0):"");
				registroDetalle.put("MONTO_SIN_OPERAR",	(totalDocsSinOper	>0)?"$ "+Comunes.formatoDecimal(totalMntDocsSinOper/mesesporevaluar_sin_oper,				2):"");
				totalDetalleOperaciones.add(registroDetalle);

				// AGREGAR TOTALES DEL DETALLE DE OPERACIONES 
				resultado.put("totalDetalleOperaciones", totalDetalleOperaciones );
				
				double mntoCredOtorgar = Double.parseDouble(factor_cre)*totalMntDocsPubs/mesesporevaluar_pub;
 
				resultado.put( "montoCreditoOtorgar", 	(mntoCredOtorgar	>0)?"$ "+Comunes.formatoDecimal(mntoCredOtorgar,2):"" );
				resultado.put( "susceptible",				suceptible				?"Si":"No"														);
 
				meses_prom = aux_meses_prom;
	
				resultado.put("criterioEvaluacion",		"".equals(hidAction812)?"1":"2"	);
				resultado.put("solicito",					nombreUsuario							);
	
				if( 
					!suceptible 
						&& 
					"6".equals(factor_cre) 
						&& 
					"6".equals(meses_consec) 
						&& 
					"6".equals(meses_prom) 
						&& 
					"".equals(hidAction812)
				){			
					resultado.put("showBotonGenerarCedula8x12pie",new Boolean(true));
				} else {
					resultado.put("showBotonGenerarCedula8x12pie",new Boolean(false));
				}
					
				if("".equals(hidAction812)) {
					resultado.put("factores","F.C.P. (" + factor_cre + ", " + meses_consec + ", " + meses_prom +")");
				}else{
					// Agregar nombreFactor
					resultado.put("factores","F.P.E. (" +factor_cre + ", " + meses_con_pub + ", " + meses_eval +")");		
				}
				
			}
			
		} catch(Exception e){
 		
			log.error("generaCedulaEvaluacion(Exception)");
			log.error("generaCedulaEvaluacion.ic_epo         = <" + ic_epo          + ">");
			log.error("generaCedulaEvaluacion.ic_pyme        = <" + ic_pyme         + ">");
			log.error("generaCedulaEvaluacion.ic_epo1        = <" + ic_epo1         + ">");
			log.error("generaCedulaEvaluacion.ic_moneda      = <" + ic_moneda       + ">");
			log.error("generaCedulaEvaluacion.factor_cre     = <" + factor_cre      + ">");
			log.error("generaCedulaEvaluacion.meses_consec   = <" + meses_consec    + ">");
			log.error("generaCedulaEvaluacion.meses_con_pub  = <" + meses_con_pub   + ">");
			log.error("generaCedulaEvaluacion.meses_prom     = <" + meses_prom      + ">");
			log.error("generaCedulaEvaluacion.meses_eval     = <" + meses_eval      + ">");
			log.error("generaCedulaEvaluacion.periodo_inicio = <" + periodo_inicio  + ">");
			log.error("generaCedulaEvaluacion.periodo_fin    = <" + periodo_fin     + ">");
			log.error("generaCedulaEvaluacion.hidAction      = <" + hidAction       + ">");
			log.error("generaCedulaEvaluacion.hidAction812   = <" + hidAction812    + ">");
			log.error("generaCedulaEvaluacion.nombreUsuario  = <" + nombreUsuario   + ">");
			e.printStackTrace();
				
			throw e;
				
		} finally {
				
			if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
			if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
				
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
				
			log.info("generaCedulaEvaluacion(S)");
				
		}
			
		return resultado;
	
	}
	
	/**
	 *	Crea un archivo CSV con el detalle de la C�dula de Evaluaci�n de la PYME.
	 *	@throws AppException   
	 *	
	 *	@param directorio <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 *	@param cedula <tt>HashMap</tt> con el detalle de la cedula de evaluaci�n de la PYME.
	 *
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 *
	 */	
	public static String generaArchivoCSV( String directorio, HashMap cedula )
		throws AppException {

		log.info("generaArchivoCSV(E)");
		
		FileOutputStream		out 				= null;
		BufferedWriter 		csv 				= null;
		CreaArchivo 			archivo 			= null;
		String					nombreArchivo 	= null;
 
		try {
 
			String fechaEmision					= (String) cedula.get("fechaEmision");
			String nombreProveedor				= (String) cedula.get("nombreProveedor");
			String rfc								= (String) cedula.get("rfc");
			String telefonos						= (String) cedula.get("telefonos");
			String nombreEpo						= (String) cedula.get("nombreEpo");
			String fechaIngresoCadena			= (String) cedula.get("fechaIngresoCadena");
			String fechaInicioOperaciones		= (String) cedula.get("fechaInicioOperaciones");
			String numeroMesesOperando			= (String) cedula.get("numeroMesesOperando");
			List 	 detalleOperaciones			= (List)   cedula.get("detalleOperaciones");
			List	 totalDetalleOperaciones	= (List)   cedula.get("totalDetalleOperaciones");
			String montoCreditoOtorgar 		= (String) cedula.get("montoCreditoOtorgar");
			String susceptible					= (String) cedula.get("susceptible");
			String criterioEvaluacion			= (String) cedula.get("criterioEvaluacion");
			String solicito 						= (String) cedula.get("solicito");
			
			fechaEmision 				= fechaEmision 				== null?"":fechaEmision;
			nombreProveedor 			= nombreProveedor 			== null?"":nombreProveedor;
			rfc 							= rfc 							== null?"":rfc;
			telefonos 					= telefonos 					== null?"":telefonos;
			nombreEpo 					= nombreEpo 					== null?"":nombreEpo;
			fechaIngresoCadena 		= fechaIngresoCadena 		== null?"":fechaIngresoCadena;
			fechaInicioOperaciones 	= fechaInicioOperaciones 	== null?"":fechaInicioOperaciones;
			numeroMesesOperando 		= numeroMesesOperando 		== null?"":numeroMesesOperando;
			detalleOperaciones 		= detalleOperaciones 		== null?new ArrayList():detalleOperaciones;
			totalDetalleOperaciones = totalDetalleOperaciones 	== null?new ArrayList():totalDetalleOperaciones;
			montoCreditoOtorgar 		= montoCreditoOtorgar 		== null?"":montoCreditoOtorgar;
			susceptible 				= susceptible 					== null?"":susceptible;
			criterioEvaluacion 		= criterioEvaluacion 		== null?"":criterioEvaluacion;
			solicito 					= solicito 						== null?"":solicito;
			
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".csv";
			out 				= new FileOutputStream(directorio+nombreArchivo);
			csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
 
			csv.write("C�DULA DE OPERACI�N EN CADENAS PRODUCTIVAS\nPARA EVALUACI�N DE L�NEA DE CR�DITO SIMPLE");
			
			csv.write("\n \n,,,Fecha de emisi�n,");
			csv.write(fechaEmision);
			
			csv.write("\n \n,Nombre del Proveedor,");
			csv.write("\"");csv.write(nombreProveedor.replaceAll("\"","\"\""));csv.write("\"");
			csv.write("\n,R.F.C.,");
			csv.write(rfc);
			csv.write("\n,Tel�fonos,");
			csv.write(telefonos);
			
			csv.write("\n \n,\"Nombre de la EPO:");
			csv.write(nombreEpo.replaceAll("\"","\"\""));csv.write("\"");
			
			csv.write("\n \nDetalle de Operaciones\nMes,Documentos Publicados,,Documentos Operados,,Documentos Sin Operar");
			csv.write("\n,Numero,Monto,Numero,Monto,Numero,Monto");
	
			for(int i=0,ctaRegistros = 0;i<detalleOperaciones.size();i++,ctaRegistros++){
				
				HashMap registro = (HashMap) detalleOperaciones.get(i);
		
				String mesAnio 			= (registro.get("MES_ANIO")				== null)?"":(String) registro.get("MES_ANIO");
				String numeroPublicados = (registro.get("NUMERO_PUBLICADOS")	== null)?"":(String) registro.get("NUMERO_PUBLICADOS");
				String montoPublicados 	= (registro.get("MONTO_PUBLICADOS")		== null)?"":(String) registro.get("MONTO_PUBLICADOS");
				String numeroOperados 	= (registro.get("NUMERO_OPERADOS")		== null)?"":(String) registro.get("NUMERO_OPERADOS");
				String montoOperados 	= (registro.get("MONTO_OPERADOS")		== null)?"":(String) registro.get("MONTO_OPERADOS");
				String numeroSinOperar 	= (registro.get("NUMERO_SIN_OPERAR")	== null)?"":(String) registro.get("NUMERO_SIN_OPERAR");
				String montoSinOperar 	= (registro.get("MONTO_SIN_OPERAR")		== null)?"":(String) registro.get("MONTO_SIN_OPERAR");
		
				numeroPublicados 			= numeroPublicados.replaceAll("[, ]","");
				montoPublicados 			= montoPublicados.replaceAll("[, ]","");
				numeroOperados				= numeroOperados.replaceAll("[, ]","");
				montoOperados 				= montoOperados.replaceAll("[, ]","");
				numeroSinOperar			= numeroSinOperar.replaceAll("[, ]","");
				montoSinOperar 			= montoSinOperar.replaceAll("[, ]","");
				
				csv.write("\n");
				csv.write(mesAnio);				csv.write(",");
				csv.write(numeroPublicados);	csv.write(",");
				csv.write(montoPublicados);	csv.write(",");
				csv.write(numeroOperados);		csv.write(",");
				csv.write(montoOperados);		csv.write(",");
				csv.write(numeroSinOperar);	csv.write(",");
				csv.write(montoSinOperar);		// csv.write(",");
				
				// 6. Guardar en disco cada 100 registros
				if( ctaRegistros == 100 ){ 
					csv.flush(); 
					ctaRegistros = 0;
				}
				
			}
 
			for(int i=0;i<totalDetalleOperaciones.size();i++){
				
				HashMap registro = (HashMap) totalDetalleOperaciones.get(i);
		
				String descripcionTotal	= (registro.get("MES_ANIO")				== null)?"":(String) registro.get("MES_ANIO");
				String numeroPublicados = (registro.get("NUMERO_PUBLICADOS")	== null)?"":(String) registro.get("NUMERO_PUBLICADOS");
				String montoPublicados 	= (registro.get("MONTO_PUBLICADOS")		== null)?"":(String) registro.get("MONTO_PUBLICADOS");
				String numeroOperados 	= (registro.get("NUMERO_OPERADOS")		== null)?"":(String) registro.get("NUMERO_OPERADOS");
				String montoOperados 	= (registro.get("MONTO_OPERADOS")		== null)?"":(String) registro.get("MONTO_OPERADOS");
				String numeroSinOperar 	= (registro.get("NUMERO_SIN_OPERAR")	== null)?"":(String) registro.get("NUMERO_SIN_OPERAR");
				String montoSinOperar 	= (registro.get("MONTO_SIN_OPERAR")		== null)?"":(String) registro.get("MONTO_SIN_OPERAR");
		
				numeroPublicados 			= numeroPublicados.replaceAll("[, ]","");
				montoPublicados 			= montoPublicados.replaceAll("[, ]","");
				numeroOperados				= numeroOperados.replaceAll("[, ]","");
				montoOperados 				= montoOperados.replaceAll("[, ]","");
				numeroSinOperar			= numeroSinOperar.replaceAll("[, ]","");
				montoSinOperar 			= montoSinOperar.replaceAll("[, ]","");
				
				csv.write("\n");
				csv.write(descripcionTotal); csv.write(",");
				csv.write(numeroPublicados); csv.write(",");
				csv.write(montoPublicados);  csv.write(",");
				csv.write(numeroOperados);   csv.write(",");
				csv.write( montoOperados);   csv.write(",");
				csv.write(numeroSinOperar);  csv.write(",");
				csv.write(montoSinOperar);// csv.write(",");
			}
 
			csv.write("\n \nMonto M�ximo del Cr�dito a Otorgar por esta Cadena:,");
			csv.write(montoCreditoOtorgar.replaceAll("[, ]",""));
			csv.write(",,,,");
			csv.write("Susceptible: ");
			csv.write(susceptible);
			
			csv.write("\n \nCriterio de Evaluaci�n:,");
			csv.write(criterioEvaluacion);
			csv.write("\n \nSolicit�:,");
			csv.write(solicito);
			
			csv.write("\n \n,_____________________________\n,Autoriz�");	
			
		} catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoCSV(OutOfMemoryError)");
			log.error("generaArchivoCSV.message     = <" + om.getMessage() + ">");
			log.error("generaArchivoCSV.freeMemory  = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error("generaArchivoCSV.directorio  = <" + directorio  		+ ">");
			log.error("generaArchivoCSV.cedula      = <" + cedula   			+ ">");
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoCSV(Exception)");
			log.error("generaArchivoCSV.message     = <" + e.getMessage() + ">");
			log.error("generaArchivoCSV.directorio  = <" + directorio     + ">");
			log.error("generaArchivoCSV.cedula      = <" + cedula   		  + ">");
			e.printStackTrace(); 
			
			throw new AppException("Ocurri� un error al generar el CSV con la informaci�n de la c�dula.");
			
		} finally {
 
			// Cerrar archivo
			if(csv 	!= null )	try{csv.close();}catch(Exception e){};
			
			log.info("generaArchivoCSV(S)");
			
		}
		
		return nombreArchivo;
		
	}
	
	/**
	 *	Crea un archivo PDF con el detalle de la C�dula de Evaluaci�n de la PYME.
	 *	@throws AppException   
	 *	
	 *	@param directorio             <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 * @param directorioPublicacion 	<tt>String</tt> con la ruta WEB del directorio de publicacion.
	 *	@param cedula 						<tt>HashMap</tt> con el detalle de la c�dula de evaluaci�n de la PYME.
	 * @param session 					<tt>HttpSession</tt> con la sesi�n del usuario.
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 *
	 */	
	public static String generaArchivoPDF( String directorio, String directorioPublicacion, HashMap cedula, HttpSession session  )
		throws AppException {

		log.info("generaArchivoPDF(E)");
		
		CreaArchivo	archivo 			= null;
		String		nombreArchivo	= null;
		ComunesPDF	pdf 				= null;
 
		try {

			String fechaEmision					= (String) cedula.get("fechaEmision");
			String nombreProveedor				= (String) cedula.get("nombreProveedor");
			String rfc								= (String) cedula.get("rfc");
			String telefonos						= (String) cedula.get("telefonos");
			String nombreEpo						= (String) cedula.get("nombreEpo");
			String fechaIngresoCadena			= (String) cedula.get("fechaIngresoCadena");
			String fechaInicioOperaciones		= (String) cedula.get("fechaInicioOperaciones");
			String numeroMesesOperando			= (String) cedula.get("numeroMesesOperando");
			List 	 detalleOperaciones			= (List)   cedula.get("detalleOperaciones");
			List	 totalDetalleOperaciones	= (List)   cedula.get("totalDetalleOperaciones");
			String montoCreditoOtorgar 		= (String) cedula.get("montoCreditoOtorgar");
			String susceptible					= (String) cedula.get("susceptible");
			String criterioEvaluacion			= (String) cedula.get("criterioEvaluacion");
			String solicito 						= (String) cedula.get("solicito");
			String factores 						= (String) cedula.get("factores");
			
			fechaEmision 				= fechaEmision 				== null?"":fechaEmision;
			nombreProveedor 			= nombreProveedor 			== null?"":nombreProveedor;
			rfc 							= rfc 							== null?"":rfc;
			telefonos 					= telefonos 					== null?"":telefonos;
			nombreEpo 					= nombreEpo 					== null?"":nombreEpo;
			fechaIngresoCadena 		= fechaIngresoCadena 		== null?"":fechaIngresoCadena;
			fechaInicioOperaciones 	= fechaInicioOperaciones 	== null?"":fechaInicioOperaciones;
			numeroMesesOperando 		= numeroMesesOperando 		== null?"":numeroMesesOperando;
			detalleOperaciones 		= detalleOperaciones 		== null?new ArrayList():detalleOperaciones;
			totalDetalleOperaciones = totalDetalleOperaciones 	== null?new ArrayList():totalDetalleOperaciones;
			montoCreditoOtorgar 		= montoCreditoOtorgar 		== null?"":montoCreditoOtorgar;
			susceptible 				= susceptible 					== null?"":susceptible;
			criterioEvaluacion 		= criterioEvaluacion 		== null?"":criterioEvaluacion;
			solicito 					= solicito 						== null?"":solicito;
			factores 					= factores 						== null?"":factores;
			
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".pdf";
			
			// Sacando la fecha para encabezado de Excel
			String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaActual.substring(0,2);
			String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   	= fechaActual.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			String strEncabezado = "M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
										  " ----------------------------- " + horaActual;

			pdf = new ComunesPDF(1,directorio+nombreArchivo);//, "P�gina ", false, true, false);
			
			// Agregar Cabecera
			
			pdf.encabezadoConImagenes(
				pdf,
				"C�DULA DE OPERACI�N EN CADENAS PRODUCTIVAS",
				"PARA EVALUACI�N DE L�NEA DE CR�DITO SIMPLE",
				"",
				"",
				"",
				(String)session.getAttribute("strLogo"),
				directorioPublicacion
			);
			
			pdf.setTable(4, 95, new float[]{1f,2f,6f,1f}); 
																
			pdf.setCell( "", 								"formas",  ComunesPDF.CENTER,1,1,0);
			pdf.setCell( "",								"formas",  ComunesPDF.CENTER,1,1,0);
			pdf.setCell( "Fecha de emisi�n:",		"formas",  ComunesPDF.RIGHT,1,1,0);
			pdf.setCell( fechaEmision, 				"formasB", ComunesPDF.LEFT,1,1,0);
				
			pdf.setCell( "", 								"formas",  ComunesPDF.CENTER,1,1,0);
			pdf.setCell( "Nombre del Proveedor:",	"formas",  ComunesPDF.RIGHT,1,1,0);
			pdf.setCell( nombreProveedor, 			"formasB", ComunesPDF.LEFT,1,1,0);
			pdf.setCell( "",								"formas",  ComunesPDF.CENTER,1,1,0);
			
			pdf.setCell( "", 								"formas",  ComunesPDF.CENTER,1,1,0);
			pdf.setCell( "R.F.C.:",						"formas",  ComunesPDF.RIGHT,1,1,0);
			pdf.setCell( rfc, 							"formasB", ComunesPDF.LEFT,1,1,0);
			pdf.setCell( "",								"formas",  ComunesPDF.CENTER,1,1,0);
			
			pdf.setCell( "", 								"formas",  ComunesPDF.CENTER,1,1,0);
			pdf.setCell( "Tel�fonos:",					"formas",  ComunesPDF.RIGHT,1,1,0);
			pdf.setCell( telefonos, 					"formas",  ComunesPDF.LEFT,1,1,0);
			pdf.setCell( "",								"formas",  ComunesPDF.CENTER,1,1,0);
			
			pdf.addTable();
			
			pdf.addText(" ","formas", ComunesPDF.CENTER );
			
			pdf.setTable(4, 95, new float[]{1f,2f,6f,1f}); 
			
			pdf.setCell( "", 								"formas",  ComunesPDF.CENTER,1,1,0);
			pdf.setCell( "Nombre de la EPO:",		"formas",  ComunesPDF.RIGHT,1,1,0);
			pdf.setCell( nombreEpo, 					"formasB", ComunesPDF.LEFT,1,1,0);
			pdf.setCell( "",								"formas",  ComunesPDF.CENTER,1,1,0);
			
			pdf.addTable();
			
			pdf.addText(" ","formas", ComunesPDF.CENTER );
			
			pdf.setTable(3, 70, new float[]{1f,1f,1f}); 
																
			pdf.setCell( "Fecha de Ingreso a la Cadena", 	"subrayado", ComunesPDF.CENTER,1,1,0);
			pdf.setCell( "Fecha de Inicio de Operaciones",	"subrayado", ComunesPDF.CENTER,1,1,0);
			pdf.setCell( "N�m. Meses Operando",					"subrayado",  ComunesPDF.CENTER,1,1,0);
			
			pdf.setCell( fechaIngresoCadena, 					"formasB", ComunesPDF.CENTER,1,1,0);
			pdf.setCell( fechaInicioOperaciones,				"formasB", ComunesPDF.CENTER,1,1,0);
			pdf.setCell( numeroMesesOperando,					"formasB",  ComunesPDF.CENTER,1,1,0);
			
			pdf.addTable();
			
			pdf.addText(" ","formas", ComunesPDF.CENTER );
			
			pdf.setTable(1, 99, new float[]{1f}); 
			pdf.setCell( "Detalle de Operaciones",	"formas",   ComunesPDF.LEFT,1,1,0);
			pdf.addTable();
						
			pdf.setTable(7, 99, new float[]{1f,0.6f,1f,0.6f,1f,0.6f,1f});
			
			pdf.setCell( "Mes / A�o", 					"celda01", ComunesPDF.CENTER,1,2,1);
			pdf.setCell( "Documentos Publicados", 	"celda01", ComunesPDF.CENTER,2,1,1);
			pdf.setCell( "Documentos Operados", 	"celda01", ComunesPDF.CENTER,2,1,1);
			pdf.setCell( "Documentos Sin Operar", 	"celda01", ComunesPDF.CENTER,2,1,1);
			
			//pdf.setCell( "", 								"celda01", ComunesPDF.CENTER,1,2,1);
			pdf.setCell( "N�mero", 						"celda01", ComunesPDF.RIGHT,1,1,1);
			pdf.setCell( "Monto", 						"celda01", ComunesPDF.RIGHT,1,1,1);
			pdf.setCell( "N�mero", 						"celda01", ComunesPDF.RIGHT,1,1,1);
			pdf.setCell( "Monto", 						"celda01", ComunesPDF.RIGHT,1,1,1);
			pdf.setCell( "N�mero", 						"celda01", ComunesPDF.RIGHT,1,1,1);
			pdf.setCell( "Monto", 						"celda01", ComunesPDF.RIGHT,1,1,1);
			
			for(int i=0;i<detalleOperaciones.size();i++){
				
			   HashMap registro = (HashMap) detalleOperaciones.get(i);

			   String mesAnio          = (registro.get("MES_ANIO")             == null)?"":(String) registro.get("MES_ANIO");
			   String numeroPublicados = (registro.get("NUMERO_PUBLICADOS")    == null)?"":(String) registro.get("NUMERO_PUBLICADOS");
			   String montoPublicados  = (registro.get("MONTO_PUBLICADOS")     == null)?"":(String) registro.get("MONTO_PUBLICADOS");
			   String numeroOperados   = (registro.get("NUMERO_OPERADOS")      == null)?"":(String) registro.get("NUMERO_OPERADOS");
			   String montoOperados    = (registro.get("MONTO_OPERADOS")       == null)?"":(String) registro.get("MONTO_OPERADOS");
			   String numeroSinOperar  = (registro.get("NUMERO_SIN_OPERAR")    == null)?"":(String) registro.get("NUMERO_SIN_OPERAR");
			   String montoSinOperar   = (registro.get("MONTO_SIN_OPERAR")     == null)?"":(String) registro.get("MONTO_SIN_OPERAR");

			   pdf.setCell( mesAnio,           "formas",      ComunesPDF.CENTER,1,1,1);
			   pdf.setCell( numeroPublicados,  "formasMini",  ComunesPDF.RIGHT,1,1,1);
			   pdf.setCell( montoPublicados,   "formasMini",  ComunesPDF.RIGHT,1,1,1);
			   pdf.setCell( numeroOperados,    "formasMini",  ComunesPDF.RIGHT,1,1,1);
			   pdf.setCell( montoOperados,     "formasMini",  ComunesPDF.RIGHT,1,1,1);
			   pdf.setCell( numeroSinOperar,   "formasMini",  ComunesPDF.RIGHT,1,1,1);
			   pdf.setCell( montoSinOperar,    "formasMini",  ComunesPDF.RIGHT,1,1,1);
				
			}

			for(int i=0;i<totalDetalleOperaciones.size();i++){
				
				HashMap registro = (HashMap) totalDetalleOperaciones.get(i);
		
			   String descripcionTotal = (registro.get("MES_ANIO")          == null)?"":(String) registro.get("MES_ANIO");
			   String numeroPublicados = (registro.get("NUMERO_PUBLICADOS") == null)?"":(String) registro.get("NUMERO_PUBLICADOS");
			   String montoPublicados  = (registro.get("MONTO_PUBLICADOS")  == null)?"":(String) registro.get("MONTO_PUBLICADOS");
			   String numeroOperados   = (registro.get("NUMERO_OPERADOS")   == null)?"":(String) registro.get("NUMERO_OPERADOS");
			   String montoOperados    = (registro.get("MONTO_OPERADOS")    == null)?"":(String) registro.get("MONTO_OPERADOS");
			   String numeroSinOperar  = (registro.get("NUMERO_SIN_OPERAR") == null)?"":(String) registro.get("NUMERO_SIN_OPERAR");
			   String montoSinOperar   = (registro.get("MONTO_SIN_OPERAR")  == null)?"":(String) registro.get("MONTO_SIN_OPERAR");

			   pdf.setCell( descripcionTotal,  "celda01",     ComunesPDF.RIGHT,1,1,1);
			   pdf.setCell( numeroPublicados,  "formasMini",  ComunesPDF.RIGHT,1,1,1);
			   pdf.setCell( montoPublicados,   "formasMini",  ComunesPDF.RIGHT,1,1,1);
			   pdf.setCell( numeroOperados,    "formasMini",  ComunesPDF.RIGHT,1,1,1);
			   pdf.setCell( montoOperados,     "formasMini",  ComunesPDF.RIGHT,1,1,1);
			   pdf.setCell( numeroSinOperar,   "formasMini",  ComunesPDF.RIGHT,1,1,1);
			   pdf.setCell( montoSinOperar,    "formasMini",  ComunesPDF.RIGHT,1,1,1);
					
			}
			/***** INICIO: El siguiente Bloque es un truco barato para que se vea el borde inferior de la tabla *****/
		   pdf.setCell( " ",  "formasMini",  ComunesPDF.RIGHT,1,1,0);
		   pdf.setCell( " ",  "formasMini",  ComunesPDF.RIGHT,1,1,0);
		   pdf.setCell( " ",  "formasMini",  ComunesPDF.RIGHT,1,1,0);
		   pdf.setCell( " ",  "formasMini",  ComunesPDF.RIGHT,1,1,0);
		   pdf.setCell( " ",  "formasMini",  ComunesPDF.RIGHT,1,1,0);
		   pdf.setCell( " ",  "formasMini",  ComunesPDF.RIGHT,1,1,0);
		   pdf.setCell( " ",  "formasMini",  ComunesPDF.RIGHT,1,1,0);
			/***** FIN *****/
			pdf.addTable();

			pdf.setTable(1, 99, new float[]{1f}); 
			pdf.setCell( factores,	"formas",   ComunesPDF.LEFT,1,1,0);
			pdf.addTable();

			pdf.setTable(3, 99, new float[]{3.5f,3f,3f}); 
			
			pdf.setCell( "Monto M�ximo del Cr�dito a Otorgar por esta Cadena:",  "formasB",  ComunesPDF.LEFT,1,1,0);
			pdf.setCell( montoCreditoOtorgar, 												"formas",   ComunesPDF.LEFT,1,1,0);
			pdf.setCell( "Susceptible: "+susceptible,										"formasB",  ComunesPDF.CENTER,1,1,0);
			
			pdf.addTable();
 
			pdf.addText(" ","formas", ComunesPDF.CENTER );
			
			pdf.setTable(2, 99, new float[]{1f,9f}); 
			
			pdf.setCell( "Solicit�:",	"formas",   ComunesPDF.LEFT,1,1,0);
			pdf.setCell( solicito,		"formasB",  ComunesPDF.LEFT,1,1,0);
			
			pdf.addTable();
			
			pdf.addText(" ","formas", ComunesPDF.CENTER );
			
			pdf.setTable(1, 99, new float[]{1f}); 
			pdf.setCell( "Criterio de Evaluaci�n: "+criterioEvaluacion,	"formas",   ComunesPDF.LEFT,1,1,0);
			pdf.addTable();
			
			pdf.addText(" ","formas", ComunesPDF.CENTER );
			pdf.addText(" ","formas", ComunesPDF.CENTER );
			pdf.addText("_______________________________________",	"formas", ComunesPDF.CENTER );
			pdf.addText("Autoriz�",												"formas", ComunesPDF.CENTER );
			
			pdf.endDocument();
 
		} catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoPDF(OutOfMemoryError)");
			log.error("generaArchivoPDF.message                = <" + om.getMessage()        + ">");
			log.error("generaArchivoPDF.freeMemory             = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error("generaArchivoPDF.directorio             = <" + directorio             + ">");
			log.error("generaArchivoPDF.directorioPublicacion  = <" + directorioPublicacion  + ">");
			log.error("generaArchivoPDF.cedula                 = <" + cedula   			      + ">");
			log.error("generaArchivoPDF.session                = <" + session                + ">");
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoPDF(Exception)");
			log.error("generaArchivoPDF.message                = <" + e.getMessage()         + ">");
			log.error("generaArchivoPDF.directorio             = <" + directorio             + ">");
			log.error("generaArchivoPDF.directorioPublicacion  = <" + directorioPublicacion  + ">");
			log.error("generaArchivoPDF.cedula                 = <" + cedula   			      + ">");
			log.error("generaArchivoPDF.session                = <" + session                + ">");
			e.printStackTrace(); 
			
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n de la c�dula.");
			
		} finally {
 
			log.info("generaArchivoPDF(S)");
			
		}
		
		return nombreArchivo;
		
	}
	
}