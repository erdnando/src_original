package com.netro.descuento;

import java.io.Serializable;

public class ValidacionCatalogo implements Serializable{

	private static final long serialVersionUID = 82010L;
	
	private 	StringBuffer 	bufferClave				= null;
	private 	StringBuffer 	bufferDescripcion		= null;
	private 	StringBuffer 	bufferErrorGenerico	= null;
	private 	boolean			errorClave 				= false;
	private 	boolean			errorDescripcion 		= false;
	private 	boolean			errorGenerico 			= false;
	private 	int 				numeroLinea				= -1;
	private  String 			clave						= null;
	
	public ValidacionCatalogo(String clave,int numeroLinea){
		this.clave 			= clave;
		this.numeroLinea 	= numeroLinea;
	}
	
	public String getClave(){
		return this.clave;
	}
	
	public int getNumeroLinea(){
		return this.numeroLinea;
	}
	
	public void appendError(String campo,String descripcion){
		if("1".equals(campo)){
				errorClave = true;
				if(bufferClave == null) bufferClave = new StringBuffer();
				bufferClave.append(descripcion);
		}else if("2".equals(campo)){
				errorDescripcion = true;
				if(bufferDescripcion == null) bufferDescripcion = new StringBuffer();
				bufferDescripcion.append(descripcion);
		}else{ // Error Generico
			errorGenerico = true;
			if(bufferErrorGenerico == null) bufferErrorGenerico = new StringBuffer();
				bufferErrorGenerico.append(descripcion);
		}
	}
	
	public boolean hayError(){
		return (this.errorClave || this.errorDescripcion || this.errorGenerico);	
	}
	
	public boolean hayErrorDeClave(){
		return this.errorClave;
	}
	public boolean hayErrorDeDescripcion(){
		return this.errorDescripcion;
	}
	public boolean hayErrorGenerico(){
		return this.errorGenerico;
	}
	
	
	public String getErroresClave(){	
		if (bufferClave == null) return "";
		return bufferClave.toString();
	}		
	public String getErroresDescripcion(){
		if (bufferDescripcion == null) return "";
		return bufferDescripcion.toString();
	}
	public String getErrorGenerico(){
		if (bufferErrorGenerico == null) return "";
		return bufferErrorGenerico.toString();
	}
	
}
