package com.netro.descuento;

import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsolidadoTasas implements IQueryGeneratorRegExtJS {//DBM

	
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsolidadoTasas.class);

	StringBuffer qrySentencia = new StringBuffer("");
	StringBuffer	condicion  =new StringBuffer(); 		
	private List   conditions;

	private String ic_tasa="";
	private String moneda="";
	private boolean primera_consulta = false;
	
	//Variables del PDF
	String tasa;
	Hashtable fechaTasa = new Hashtable();
	Hashtable nombreTasa = new Hashtable();
	Hashtable valorTasa = new Hashtable();
				
			
	/*Constructor*/
	public ConsolidadoTasas() { }
	
  
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery() {
		return "";
	}
	
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() { // las  LLAVES
		return "";	
	}
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {		
		return "";	
	}
	
	
	public String getDocumentQueryFile() {
		
		log.info("getDocumentQueryFile(E) :::...");
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
	
		if(primera_consulta == true) {
			if(!"".equals(moneda)) {
				condicion.append( "AND t.ic_moneda = ?" );  
				conditions.add(moneda);
			}
			if(!"".equals(ic_tasa)) {
				condicion.append( " AND t.ic_tasa  = ? " );  
				conditions.add(ic_tasa);
			}	
			qrySentencia.append( " SELECT TO_CHAR(mt.dc_fecha,'dd/mm/yyyy') as dc_fecha,"+
										" 		mt.ic_tasa, t.cd_nombre,mt.fn_valor"+
										" FROM (select ic_tasa,max(dc_fecha) as dc_fecha from com_mant_tasa "+
										" 		group by ic_tasa) x, "+
										" 		com_mant_tasa mt, comcat_tasa t"+
										" WHERE x.ic_tasa = mt.ic_tasa "+
										" AND x.dc_fecha = mt.dc_fecha "+
										" AND x.ic_tasa = t.ic_tasa " + condicion +
										" AND t.cs_disponible = 'S' ");
		}
		
		
		else {			
			if (!"".equals(ic_tasa)) {
				if( !ic_tasa.equals("TODAS")) {
					condicion.append( " AND tb.ic_tasa  = ? " );  
					conditions.add(ic_tasa);
				}
				else {
					condicion.append( "AND tb.ic_tasa in (select ic_tasa from comcat_tasa  where ic_moneda=? and cs_disponible='S')" );  
					conditions.add(moneda);
				}
			}
			
			qrySentencia.append( "SELECT tb.ic_epo, (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEpo"+
										" , x.ic_if , (decode (i.cs_habilitado,'N','*','S',' ')||' '||i.cg_razon_social) as nombreIf"+
										" , tb.cg_rel_mat, tb.fn_puntos, tb.ic_tasa"+
										" from comrel_tasa_base tb, comcat_epo e, comcat_if i"+
										" , ("+
										" select tb.ic_epo, tb.ic_tasa, ta.ic_if"+
										" , max(tb.dc_fecha_tasa) as dc_fecha_tasa"+
										" FROM comrel_tasa_base tb, comrel_tasa_autorizada ta"+
										" where "+
										" ta.dc_fecha_tasa = tb.dc_fecha_tasa " + condicion +
										" and tb.cs_vobo_nafin = 'S'"+
										" and ta.cs_vobo_epo = 'S'"+
										" and ta.cs_vobo_if = 'S'"+
										" and ta.cs_vobo_nafin = 'S'"+
										" group by tb.ic_epo, tb.ic_tasa, ta.ic_if"+
										" ) x"+
										" WHERE"+
										" tb.dc_fecha_tasa = x.dc_fecha_tasa"+
										" and tb.ic_epo = x.ic_epo"+
										" and tb.ic_tasa = x.ic_tasa"+
										" and x.ic_if = i.ic_if and tb.ic_epo = e.ic_epo"+
										" order by e.cg_razon_social,i.cg_razon_social");
										
		} //else			
					
		
		log.info("getDocumentQueryFile :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	} 
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos con PAGINADOR*/		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo)
	{	
		String nombreArchivo = "";
		return  nombreArchivo;
	}
	
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) SIN paginador*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet reg, String path, String tipo) {

		String nombreArchivo = "";
		try{
			if(primera_consulta == true) {
				while (reg.next()) {
					tasa = reg.getString("IC_TASA");
					fechaTasa.put(tasa,reg.getString("DC_FECHA"));
					nombreTasa.put(tasa,reg.getString("CD_NOMBRE"));
					valorTasa.put(tasa,reg.getString("FN_VALOR"));
				}					
			}
			else if(primera_consulta == false) {
				String claveTasa;
				String claveEpo;
				String nombreEpo;
				String claveIf;
				String nombreIf;
				String relacionMatematica;
				String claveEpoAnterior="";
				String puntos;
				double tasaBase=0.00000;;
			
				if("PDF".equals(tipo)) {	
					HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
					
					String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual		= fechaActual.substring(0,2);
					String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual		= fechaActual.substring(6,10);
					String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
					pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
					
				
					pdfDoc.setTable(4, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
				
					while(reg.next()) {
						claveTasa = (reg.getString("IC_TASA") == null) ? "" : reg.getString("IC_TASA");
						claveEpo = (reg.getString("IC_EPO") == null) ? "" : reg.getString("IC_EPO");
						nombreEpo = (reg.getString("NOMBREEPO") == null) ? "" : reg.getString("NOMBREEPO");
						claveIf = (reg.getString("IC_IF") == null) ? "" : reg.getString("IC_IF");
						nombreIf = (reg.getString("NOMBREIF") == null) ? "" : reg.getString("NOMBREIF");
		
						relacionMatematica = (reg.getString("CG_REL_MAT") == null) ? "" : reg.getString("CG_REL_MAT");
						puntos = (reg.getString("FN_PUNTOS") == null) ? "0" : reg.getString("FN_PUNTOS");
		
						if (relacionMatematica.equals("+"))
							tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())+Double.parseDouble(puntos);
						else if (relacionMatematica.equals("-"))
							tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())-Double.parseDouble(puntos);
						else if (relacionMatematica.equals("*"))
							tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())*Double.parseDouble(puntos);
						else if (relacionMatematica.equals("/"))
							tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())/Double.parseDouble(puntos);
		
						if (!claveEpo.equals(claveEpoAnterior))
						{
							if (!claveEpoAnterior.equals(""))
								pdfDoc.setCell("","formas",ComunesPDF.LEFT,4);
						
							claveEpoAnterior	= claveEpo;
						
							pdfDoc.setCell(nombreEpo,"celda01",ComunesPDF.CENTER,4);
							pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Nombre de la Tasa","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Valor de la Tasa","celda01",ComunesPDF.CENTER);
					
						} //fin if ($ic_epo != $ic_epoAnterior)
						pdfDoc.setCell(nombreIf,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(nombreTasa.get(claveTasa)+" "+relacionMatematica+" "+puntos,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell((fechaTasa.get(claveTasa)).toString(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("%"+Comunes.formatoDecimal(tasaBase,5) ,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
				else if ("CSV".equals(tipo))
				{
					try {
						CreaArchivo archivo		= new CreaArchivo();
						nombreArchivo	   = archivo.nombreArchivo()+".xls";
			
						ComunesXLS xlsDoc	= new ComunesXLS(path+nombreArchivo);
						xlsDoc.setTabla(4);
						
						while(reg.next()) {
							claveTasa = (reg.getString("IC_TASA") == null) ? "" : reg.getString("IC_TASA");
							claveEpo = (reg.getString("IC_EPO") == null) ? "" : reg.getString("IC_EPO");
							nombreEpo = (reg.getString("NOMBREEPO") == null) ? "" : reg.getString("NOMBREEPO");
							claveIf = (reg.getString("IC_IF") == null) ? "" : reg.getString("IC_IF");
							nombreIf = (reg.getString("NOMBREIF") == null) ? "" : reg.getString("NOMBREIF");
			
							relacionMatematica = (reg.getString("CG_REL_MAT") == null) ? "" : reg.getString("CG_REL_MAT");
							puntos = (reg.getString("FN_PUNTOS") == null) ? "0" : reg.getString("FN_PUNTOS");
			
							if (relacionMatematica.equals("+"))
								tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())+Double.parseDouble(puntos);
							else if (relacionMatematica.equals("-"))
								tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())-Double.parseDouble(puntos);
							else if (relacionMatematica.equals("*"))
								tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())*Double.parseDouble(puntos);
							else if (relacionMatematica.equals("/"))
								tasaBase = Double.parseDouble(valorTasa.get(claveTasa).toString())/Double.parseDouble(puntos);
			
							if (!claveEpo.equals(claveEpoAnterior))
							{
								if (!claveEpoAnterior.equals("")) {
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 4);
									
								}
								claveEpoAnterior	= claveEpo;
							
								xlsDoc.setCelda(nombreEpo, "celda01", ComunesXLS.CENTER, 4); 
								xlsDoc.setCelda("Intermediario Financiero", "formasb", ComunesXLS.CENTER, 1);							
								xlsDoc.setCelda("Nombre de la Tasa", "formasb", ComunesXLS.CENTER, 1);							
								xlsDoc.setCelda("Fecha", "formasb", ComunesXLS.CENTER, 1);							
								xlsDoc.setCelda("Valor de la Tasa", "formasb", ComunesXLS.CENTER, 1);							

							} //fin if ($ic_epo != $ic_epoAnterior)
							
							xlsDoc.setCelda(nombreIf,    "formas", ComunesXLS.LEFT, 1);
							xlsDoc.setCelda(nombreTasa.get(claveTasa)+" "+relacionMatematica+" "+puntos,    "formas", ComunesXLS.LEFT, 1);
							xlsDoc.setCelda(String.valueOf(fechaTasa.get(claveTasa)),    "formas", ComunesXLS.CENTER, 1);
							xlsDoc.setCelda("%"+Comunes.formatoDecimal(tasaBase,5),    "formas", ComunesXLS.CENTER, 1);
						}
						
						xlsDoc.cierraTabla();
						xlsDoc.cierraXLS();
						
					} catch (Throwable e) 
					{
						throw new AppException("Error al generar el archivo ", e);
					} 
				}
			}
			return  nombreArchivo;
		}	catch(Throwable e){
			throw new AppException("Error al generar el archivo",e);
		}finally{
			try{
			}catch(Exception e){}
		}	
			
	}
	
	
	/*****************************************************
									SETTERS         
	*******************************************************/
	
	public void setTasa (String ic_tasa ) { this.ic_tasa = ic_tasa; }
	
	public void setMoneda (String moneda) { this.moneda = moneda; }
	
	public void setConsulta (boolean primera_consulta) { this.primera_consulta = primera_consulta; }
	
	/*****************************************************
								GETTERS
	*******************************************************/
	public String getTasa() { return ic_tasa; }
	
	public String getMoneda () {  return moneda; }
	
	
	public List getConditions() {  return conditions;  }  
  
	

	

}