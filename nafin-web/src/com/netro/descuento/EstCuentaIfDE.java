package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;

public class EstCuentaIfDE implements IQueryGenerator, IQueryGeneratorRegExtJS
{
  public EstCuentaIfDE()
  {
  }
	public String getAggregateCalculationQuery(HttpServletRequest request)
  {
    String condicion = "";
    StringBuffer qrySentencia = new StringBuffer();
    
	String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
	String cliente = (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
	String prestamo = ( request.getParameter("prestamo") == null) ? "" :  request.getParameter("prestamo");
	String FechaCorte = (request.getParameter("FechaCorte") == null) ? "" : request.getParameter("FechaCorte");
	String moneda = (request.getParameter("moneda") == null) ? "" : request.getParameter("moneda");
	
    try{
			qrySentencia.append(
				"select " +
				"    /*+ " +
				"		 INDEX (E IN_COM_ESTADO_CUENTA_01_NUK) " +
				"    */  " +
				"        count(1), " +
				"        sum(fg_montooperado), " +
				"        sum(fg_saldoinsoluto), " +
				"        sum(fg_capitalvigente), " +
				"        sum(fg_capitalvencido), " +
				"        sum(fg_interesvencido), " +
				"        sum(fg_interesmorat), " +
				"        sum(fg_adeudototal), " +
				"        M.cd_nombre, " +
				"        E.ic_moneda, " +
				"    	 'EstCuentaIfDE::getAggregateCalculationQuery' " +
				"from  " +
				"        COM_ESTADO_CUENTA E, " +
				"        COMCAT_MONEDA M " +
				"where   E.ic_moneda = M.ic_moneda " +
				"    and E.ic_if = " + ic_if ); 

         if(!moneda.equals(""))
         	condicion += " and E.ic_moneda="+moneda;
         if(!cliente.equals(""))
         	condicion += " and E.ig_cliente="+cliente;
         if(!prestamo.equals(""))
         	condicion += " and E.ig_prestamo="+prestamo;
         if(!FechaCorte.equals(""))
            condicion += " AND DF_FECHAFINMES >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAFINMES < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
 
        qrySentencia.append(" " + condicion);
     	qrySentencia.append(" group by E.ic_moneda,m.cd_nombre");
      	qrySentencia.append(" order by E.ic_moneda");
	       
    	System.out.println("EL query del AggregateCalculationQuery es: "+qrySentencia.toString());
    
    }catch(Exception e){
      System.out.println("EstCuentaIfDE::getAggregateCalculationQuery "+e);
    }
    return qrySentencia.toString();
  }

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids)
  {
  	
 	int i=0;
    StringBuffer qrySentencia = new StringBuffer();
      /*
       qrySentencia.append(
			"select " +
			"    /*+ " +
			"        INDEX (I CP_COMCAT_IF_PK)" +
			"        INDEX (M CP_COMCAT_MOENDA_PK)" +
			"    *//* " +
			"        cg_nombrecliente, to_char(df_fechoperacion, 'dd/mm/yyyy'), ig_prestamo,  " +
			"		 ig_tasareferencial, fg_spread, fg_margen, fg_montooperado, fg_saldoinsoluto,  " +
			"		 fg_capitalvigente, fg_capitalvencido, fg_interesvencido, fg_interesmorat, fg_adeudototal,  " +
			"		 E.ic_moneda, ig_cliente, M.cd_nombre,  " +
			"		 to_char(df_fechafinmes, 'dd/mm/yyyy'), decode(I.ig_tipo_piso,1,'',2,ic_financiera) as ic_financiera,  " +
			"		 cg_razon_social, ig_codsucursal, cg_nomsucursal, ig_tipointermediario, cg_tipointermediario_desc, cg_descmodpago,  " +
			"		 ig_prodbanco, cg_prodbanco_desc, ig_subapl, cg_subapl_desc, ig_proyecto,  " +
			"		 ig_contrato, ig_disposicion, ig_frecinteres, ig_freccapital, to_char(df_fechvencimiento, 'dd/mm/yyyy'),  " +
			"		 cg_descripciontasa, cg_relmat1, fg_tasamoratoria, ig_sancion, to_char(df_1acuotaven, 'dd/mm/yyyy'),  " +
			"		 ig_diavencimiento, ig_diaprovision, fg_interesprovi, fg_sobretasamor, fg_comisiongtia, fg_sobtasagtia_porc,  " +
			"		 fg_finadicotorg, fg_finanadicrecup, fg_totalfinan, fg_capitalrecup, fg_interesrecup, fg_morarecup, fg_saldonafin,  " +
			"		 fg_saldobursatil, fg_valortasa, fg_tasatotal, cg_edocartera, ig_numero_electronico,  " +
			" 		 fg_intcobradoxanticip, fg_otrosadeudos, " +
			"    	 'EstCuentaIfDE::getDocumentQueryFile' " +
			"from  " +
			"        COM_ESTADO_CUENTA E,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   E.ic_moneda = M.ic_moneda  " +
			//"    and E.ic_if = I.ic_if " +
			"	 and E.ic_estado_cuenta in ( ");

       */
		qrySentencia.append(
			"select " +
			"    /*+ " +
			"        INDEX (I CP_COMCAT_IF_PK)" +
			"        INDEX (M CP_COMCAT_MOENDA_PK)" +
			"    */ " +
			"        cg_nombrecliente, to_char(df_fechoperacion, 'dd/mm/yyyy'), ig_prestamo,  " +
			"		 ig_tasareferencial, fg_spread, fg_margen, fg_montooperado, fg_saldoinsoluto,  " +
			"		 fg_capitalvigente, fg_capitalvencido, fg_interesvencido, fg_interesmorat, fg_adeudototal,  " +
			"		 E.ic_moneda, ig_cliente, M.cd_nombre,  " +
			"		 to_char(df_fechafinmes, 'dd/mm/yyyy'), decode(I.ig_tipo_piso,1,'',2,ic_financiera) as ic_financiera,  " +
			"		 cg_razon_social, ig_codsucursal, cg_nomsucursal, ig_tipointermediario, cg_tipointermediario_desc, cg_descmodpago,  " +
			"		 ig_prodbanco, cg_prodbanco_desc, ig_subapl, cg_subapl_desc, ig_proyecto,  " +
			"		 ig_contrato, ig_disposicion, ig_frecinteres, ig_freccapital, to_char(df_fechvencimiento, 'dd/mm/yyyy'),  " +
			"		 cg_descripciontasa, cg_relmat1, fg_tasamoratoria, ig_sancion, to_char(df_1acuotaven, 'dd/mm/yyyy'),  " +
			"		 ig_diavencimiento, ig_diaprovision, fg_interesprovi, fg_sobretasamor, fg_comisiongtia, fg_sobtasagtia_porc,  " +
			"		 fg_finadicotorg, fg_finanadicrecup, fg_totalfinan, fg_capitalrecup, fg_interesrecup, fg_morarecup, fg_saldonafin,  " +
			"		 fg_saldobursatil, fg_valortasa, fg_tasatotal, cg_edocartera, ig_numero_electronico,  " +
			" 		 fg_intcobradoxanticip, fg_otrosadeudos, " +
			"    	 'EstCuentaIfDE::getDocumentQueryFile' " +
			"from  " +
			"        COM_ESTADO_CUENTA E,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   E.ic_moneda = M.ic_moneda  " +
			"    and E.ic_if = I.ic_if " +
	        "	 and E.ic_estado_cuenta in ( ");
	
	for (Iterator it = ids.iterator(); it.hasNext();i++){
    	if(i>0)
      		qrySentencia.append(",");
		qrySentencia.append("'"+it.next().toString()+"'");
	}
	
	qrySentencia.append(") " +
		" order by ic_moneda, ig_cliente, ig_prestamo ");
	
    System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
	
	return qrySentencia.toString();

  }
	
	public String getDocumentQuery(HttpServletRequest request)
	{
    String condicion = "";
    StringBuffer qrySentencia = new StringBuffer();
    
	String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
	String cliente = (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
	String prestamo = ( request.getParameter("prestamo") == null) ? "" :  request.getParameter("prestamo");
	String FechaCorte = (request.getParameter("FechaCorte") == null) ? "" : request.getParameter("FechaCorte");
	String moneda = (request.getParameter("moneda") == null) ? "" : request.getParameter("moneda");
	
    try{
			qrySentencia.append(
				"select " +
				"    /*+ " +
				"        INDEX (E IN_COM_ESTADO_CUENTA_01_NUK) " +
				"    */  " +
				"        ic_estado_cuenta, " +
				"    	 'EstCuentaIfDE::getDocumentQuery' " +
				"from  " +
				"        COM_ESTADO_CUENTA " +
				"WHERE   ic_if = " + ic_if); 

         if(!moneda.equals(""))
         	condicion += " and ic_moneda="+moneda;
         if(!cliente.equals(""))
         	condicion += " and ig_cliente="+cliente;
         if(!prestamo.equals(""))
         	condicion += " and ig_prestamo="+prestamo;
         if(!FechaCorte.equals(""))
            condicion += " AND DF_FECHAFINMES >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAFINMES < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";

	     qrySentencia.append(" " + condicion +
	       	" order by ic_moneda, ig_cliente, ig_prestamo ");
				       
    	 System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
    	 
    }catch(Exception e){
      System.out.println("EstCuentaIfDE::getDocumentQueryException "+e);
    }
    return qrySentencia.toString();
  }
	
	public String getDocumentQueryFile(HttpServletRequest request)
  {
    //String condicion = "";
   // StringBuffer qrySentencia = new StringBuffer();
    
	String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
	String cliente = (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
	String prestamo = ( request.getParameter("prestamo") == null) ? "" :  request.getParameter("prestamo");
	String FechaCorte = (request.getParameter("FechaCorte") == null) ? "" : request.getParameter("FechaCorte");String FechaFin = (request.getParameter("FechaFin") == null) ? "" : request.getParameter("FechaFin");
	String moneda = (request.getParameter("moneda") == null) ? "" : request.getParameter("moneda");
	String qry = "";
	
    try{
    		ArmaQueryFile queryf = new ArmaQueryFile();
    		qry = queryf.regresaQueryEdoCta(ic_if,cliente,prestamo,FechaCorte,moneda);

	    System.out.println("EL query de DocumentQueryFile es: "+qry);//qrySentencia.toString());
      
    }catch(Exception e){
      System.out.println("EstCuentaIfDE::getDocumentQueryFileException "+e);
    }
    return qry;//qrySentencia.toString();
  }
  
  /*****************************************************************************
	*										MIGRACION												 *
	*										garellano												 *
	*                         	 15/junio/2012										 		 *  
	****************************************************************************/
	public  StringBuffer strQuery;
	private List conditions = new ArrayList(); 
	private String moneda;
	private String cliente;
	private String prestamo;
	private String FechaCorte;
	private String ic_if;
  private String tipoLinea;
	/**   
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() { // Obtiene los totales 
		String condicion = "";
		StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append(
			"select " +      
			"    /*+ " +
			"		 INDEX (E IN_COM_ESTADO_CUENTA_01_NUK) " +
			"    */  " +
			"        count(1), " +
			"        sum(fg_montooperado), " +
			"        sum(fg_saldoinsoluto), " +
			"        sum(fg_capitalvigente), " +
			"        sum(fg_capitalvencido), " +
			"        sum(fg_interesvencido), " +
			"        sum(fg_interesmorat), " +
			"        sum(fg_adeudototal), " +
			"        M.cd_nombre, " +
			"        E.ic_moneda, " +
			"    	 'EstCuentaIfDE::getAggregateCalculationQuery' " +
			"from  " +
			"        COM_ESTADO_CUENTA E, " +
			"        COMCAT_MONEDA M " +
			"where   E.ic_moneda = M.ic_moneda " +
			"    and E.ic_if = " + ("C".equals(tipoLinea)?"12":ic_if) ); 

			if(!moneda.equals(""))
				condicion += " and E.ic_moneda="+moneda;
			if(!cliente.equals(""))
				condicion += " and E.ig_cliente="+cliente;
			if(!prestamo.equals(""))
				condicion += " and E.ig_prestamo="+prestamo;
			if(!FechaCorte.equals(""))
				condicion += " AND DF_FECHAFINMES >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAFINMES < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
 
			qrySentencia.append(" " + condicion);
			qrySentencia.append(" group by E.ic_moneda,m.cd_nombre");
			qrySentencia.append(" order by E.ic_moneda");

         System.out.println("\n\n ...::::::: getAggregateCalculationQuery :::::.......   " + qrySentencia);

		 return qrySentencia.toString();
 	}  
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */	 
	public String getDocumentQuery(){  // para las llaves primarias		
		String condicion = "";
		StringBuffer qrySentencia = new StringBuffer();
	
		qrySentencia.append(
			"select " +
			"    /*+ " +
			"        INDEX (E IN_COM_ESTADO_CUENTA_01_NUK) " +
			"    */  " +
			"        ic_estado_cuenta, " +
			"    	 'EstCuentaIfDE::getDocumentQuery' " +
			"from  " +
			"        COM_ESTADO_CUENTA " +
			"WHERE   ic_if = " + ("C".equals(tipoLinea)?"12":ic_if)); 
		
		if(!moneda.equals(""))
			condicion += " and ic_moneda="+moneda;
		if(!cliente.equals(""))
			condicion += " and ig_cliente="+cliente;
		if(!prestamo.equals(""))
			condicion += " and ig_prestamo="+prestamo;
		if(!FechaCorte.equals(""))
			condicion += " AND DF_FECHAFINMES >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAFINMES < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
		
		qrySentencia.append(" " + condicion + " order by ic_moneda, ig_cliente, ig_prestamo ");
      
      System.out.println("\n\n ....::::: getDocumentQuery ::::::....  " + qrySentencia);
      
		 return qrySentencia.toString();	
 	}  
	   
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */	  		
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 			
		StringBuffer qrySentencia = new StringBuffer();
		
		qrySentencia.append(
			"SELECT                                                                         "  +
         "       /*                                                                      "  +
         "           INDEX (I CP_COMCAT_IF_PK)                                           "  +
         "           INDEX (M CP_COMCAT_MOENDA_PK)                                       "  +
         "       */                                                                      "  +
         "       ig_cliente, TO_CHAR (df_fechoperacion, 'dd/mm/yyyy'), cg_nombrecliente, "  +
         "       ig_prestamo, ig_tasareferencial, fg_spread, fg_margen, fg_montooperado, "  +
         "       fg_saldoinsoluto, fg_capitalvigente, fg_capitalvencido,                 "  +
         "       fg_interesvencido, fg_interesmorat, fg_adeudototal,  e.ic_moneda,        "  +
         "        'EstCuentaIfDE::getDocumentQueryFile'                                   " +
         "  FROM com_estado_cuenta e, comcat_if i, comcat_moneda m                       "  +
         " WHERE e.ic_moneda = m.ic_moneda AND e.ic_if = i.ic_if                         " +
			"	 and E.ic_estado_cuenta in ( ");
		
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0){ qrySentencia.append(","); }
			qrySentencia.append("'"+lItem.get(0).toString()+"'");
		}
		qrySentencia.append(") order by ic_moneda, ig_cliente, ig_prestamo ");
		
		System.out.println("\n\n .....::::: getDocumentSummaryQueryForIds ::::::.....  " + qrySentencia);
		return qrySentencia.toString();
 	}  
	
	public String getDocumentQueryFile(){ // para todos los registros				
		String qry = "";
		
		ArmaQueryFile queryf = new ArmaQueryFile();
		qry = queryf.regresaQueryEdoCta((("C".equals(tipoLinea))?"12":ic_if),cliente,prestamo,FechaCorte,moneda);

		System.out.println("\n\n ....::::::: getDocumentQueryFile :::::....  " + qry);    
		return qry;      
 	}     
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();

			if ("XLS".equals(tipo)) {
				try {
					int registros = 0;	
					while(rs.next()){
						if(registros==0) { 
							contenidoArchivo.append("Nombre del proveedor, Numero del proveedor, Numero de Transferencia, Fecha de pago, Montonto total de la transferencia, Moneda, Fecha de la factura, Monto de la factura, Fecha de operacion");
						}
						registros++;
						String nomproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
							
						contenidoArchivo.append("\n"+
							nomproveedor.replaceAll(",","") + "," +
							numproveedor + "," +
							numtrans + "," +
							fechapago.replaceAll("-","/") + "," +
							"$ " +  Comunes.formatoDecimal(montocheque, 2, false) + "," +
							moneda + "," +
							fechafactura.replaceAll("-","/") + "," +
							"$ " + Comunes.formatoDecimal(montofactura, 2, false) + "," +
							fechaoperacion.replaceAll("-","/") + ",");
					}
					
					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}	 
				} catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}else if ("PDF".equals(tipo)) {
				try {
					HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
							
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.setTable(9, 80);
					pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Numero del Proveedor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Numero de Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Pagado","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Total de la Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operacion","celda01",ComunesPDF.CENTER);
													
					while (rs.next()) {
						String nombreproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numeroproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
						 
						pdfDoc.setCell(nombreproveedor,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(numeroproveedor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numtrans,"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(fechapago,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechafactura,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(fechaoperacion,"formas",ComunesPDF.CENTER);				
					}
					pdfDoc.addTable();
					pdfDoc.endDocument();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}else if("ZIP".equals(tipo)){
				try{
          writer = null;
          buffer = null;
          int total = 0;
          String fechaCorte = FechaCorte.replace('/','_');
          nombreArchivo = "archedocuenta_"+fechaCorte+"_"+ic_if+"_S.txt";
        
          //nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
          writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, false),"ISO-8859-1");
          buffer = new BufferedWriter(writer);
      
          contenidoArchivo = new StringBuffer();	
          contenidoArchivo.append("FechaFinMes         ;Moneda              ;Descripcion         ;Intermediario       ;Descripcion         ;CodSucursal"+
													"         ;NomSucursal         ;TipIntermediario    ;Descripcion         ;DescModPago         ;ProdBanco           ;Descripcion"+
													"         ;SubApl              ;Descripcion         ;Cliente             ;NombreCliente       ;Proyecto            ;Contrato            ;Prestamo"+
													"            ;NumElectronico      ;Disposicion         ;FrecInteres         ;FrecCapital         ;FechOperacion       ;FechVencimiento"+
													"     ;TasaReferencial     ;DescripcionTasa     ;RelMat1             ;Spread              ;Margen              ;TasaMoratoria       ;Sancion"+
													"             ;1aCuotaVen          ;DiaVencimiento      ;DiaProvision        ;MontoOperado        ;SaldoInsoluto       ;CapitalVigente      ;CapitalVencido"+
													"      ;InteresProvi        ;IntCobAnt           ;InteresGravProv     ;IVAProv             ;InteresVencido      ;InteresVencidoGravado;IVAVencido"+
													"          ;InteresMorat        ;InteresMoratGravado ;IVAsobreMoratorios  ;SobreTasaMor        ;SobreTasaMorGravado ;OtrosAdeudos        ;ComisionGtia"+
													"        ;Comisiones          ;SobTasaGtia%        ;FinAdicOtorg        ;FinanAdicRecup      ;TotalFinan          ;AdeudoTotal         ;CapitalRecup"+
													"        ;InteresRecup        ;InteresRecupGravado ;MoraRecup           ;MoraRecupGravado    ;IVA Recup           ;SubsidioAplicado    ;SaldoNafin"+
													"          ;SaldoBursatil       ;ValorTasa           ;TasaTotal           ;EdoCartera          ;\n");
  
        
       while (rs.next())	{					
          contenidoArchivo.append(Comunes.formatoFijo(rs.getString("FechaFinMes"),11," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("Moneda"),2," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("DescMoneda"),30," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("ic_financiera"),5," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("DescIf"),60," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("CodSucursal"),4," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("NomSucursal"),31," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("TipIntermediario"),2," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("Descripcion"),30," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("DescModPago"),25," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("ProdBanco"),6," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("Descripcion"),60," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("SubApl"),3," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("Descripcion"),40," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("Cliente"),12," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("NombreCliente"),150," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("Proyecto"),12," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("Contrato"),12," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("Prestamo"),12," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("NumElectronico"),11," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("Disposicion"),3," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("FrecInteres"),2," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("FrecCapital"),2," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("FechOperacion"),11," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("FechVencimiento"),11," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("TasaReferencial"),4," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("DescripcionTasa"),30," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("RelMat1"),2," ","A")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Spread"),2,false),7," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Margen"),2,false),7," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TasaMoratoria"),4,false),7," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("Sancion"),3," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("aCuotaVen"),11," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("DiaVencimiento"),2," ","A")+";"+ 
						Comunes.formatoFijo(rs.getString("DiaProvision"),11," ","A")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoOperado"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SaldoInsoluto"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("CapitalVigente"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("CapitalVencido"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresProvi"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IntCobAnt"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresGravProv"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IVAProv"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresVencido"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresVencidoGravado"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IVAVencido"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresMorat"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresMoratGravado"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IVAsobreMoratorios"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SobreTasaMor"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SobreTasaMorGravado"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("OtrosAdeudos"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("ComisionGtia"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Comisiones"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("SobTasaGtiaporc"),8," ","A")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("FinAdicOtorg"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("FinanAdicRecup"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TotalFinan"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("AdeudoTotal"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("CapitalRecup"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresRecup"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("InteresRecupGravado"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MoraRecup"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MoraRecupGravado"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IVARecup"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SubsidioAplicado"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SaldoNafin"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("SaldoBursatil"),2,false),19," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("ValorTasa"),4,false),7," ","D")+";"+ 
						Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TasaTotal"),4,false),7," ","D")+";"+ 
						Comunes.formatoFijo(rs.getString("EdoCartera"),2," ","A")+";\n");	
          
          total++;
          if(total==1000){					
            total=0;	
            buffer.write(contenidoArchivo.toString());
            contenidoArchivo = new StringBuffer();//Limpio  
          }
          
        }//while(rs.next()){
          
        buffer.write(contenidoArchivo.toString());
        buffer.close();	
        contenidoArchivo = new StringBuffer();//Limpio  
      } catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
      } finally {
        try {
          rs.close();
        } catch(Exception e) {}
      }
			}
		return nombreArchivo;	
	}	
	
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo 	= new CreaArchivo();
		
		if ("PDF".equals(tipo)) {   
			int nRow = 0, numMon = 0, numDL = 0;
			double totMontoOperMon    = 0.0, totMontoOperDL    = 0.0;
			double totSaldoInsMon     = 0.0, totSaldoInsDL     = 0.0;
			double totCapitalVigMon   = 0.0, totCapitalVigDL   = 0.0;
			double totCapitalVencMon  = 0.0, totCapitalVencDL  = 0.0;
			double totInteresVencMon  = 0.0, totInteresVencDL  = 0.0;
			double totInteresMoratMon = 0.0, totInteresMoratDL = 0.0;
			double totAdeudoTotMon    = 0.0, totAdeudoTotDL    = 0.0;	
			int mx = 0;	int dl = 0;
			
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(14, 100);
				pdfDoc.setCell("Cliente", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre del Cliente", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Operaci�n", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Pr�stamo", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa ref.", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Spread", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Margen", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Operado", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Saldo insoluto", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Capital vigente", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Capital vencido", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Inter�s vencido", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Inter�s moratorio", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Adeudo Total", "formasmenB", ComunesPDF.CENTER);
								
				while (rs.next()) {
					String ig_cliente 			= rs.getString("ig_cliente")==null?"":rs.getString("ig_cliente");
					String fecha_oper 			= rs.getString(2)==null?"":rs.getString(2);
					String cg_nombrecliente 	= rs.getString("cg_nombrecliente")==null?"":rs.getString("cg_nombrecliente");
					String ig_prestamo 			= rs.getString("ig_prestamo")==null?"":rs.getString("ig_prestamo");
					String ig_tasareferencial 	= rs.getString("ig_tasareferencial")==null?"":rs.getString("ig_tasareferencial");
					String fg_spread 				= rs.getString("fg_spread")==null?"":rs.getString("fg_spread");
					String fg_margen				= rs.getString("fg_margen")==null?"":rs.getString("fg_margen");
					String fg_montooperado 		= rs.getString("fg_montooperado")==null?"":rs.getString("fg_montooperado");
					String fg_saldoinsoluto 	= rs.getString("fg_saldoinsoluto")==null?"":rs.getString("fg_saldoinsoluto");
					String fg_capitalvigente 	= rs.getString("fg_capitalvigente")==null?"":rs.getString("fg_capitalvigente");
					String fg_capitalvencido 	= rs.getString("fg_capitalvencido")==null?"":rs.getString("fg_capitalvencido");
					String fg_interesvencido	= rs.getString("fg_interesvencido")==null?"":rs.getString("fg_interesvencido");
					String fg_interesmorat 		= rs.getString("fg_interesmorat")==null?"":rs.getString("fg_interesmorat");
					String fg_adeudototal 		= rs.getString("fg_adeudototal")==null?"":rs.getString("fg_adeudototal");
					
					Vector vecDatos = new Vector();
					for(int i=1; i<=16; i++) {
                  vecDatos.add(rs.getString(i));   
					}
				  
					if(rs.getString(14).equals("54")){
						numDL++;
						totMontoOperDL    += Double.valueOf(rs.getString(7)).doubleValue();
						totSaldoInsDL     += Double.valueOf(rs.getString(8)).doubleValue();
						totCapitalVigDL   += Double.valueOf(rs.getString(9)).doubleValue();
						totCapitalVencDL  += Double.valueOf(rs.getString(10)).doubleValue();
						totInteresVencDL  += Double.valueOf(rs.getString(11)).doubleValue();
						totInteresMoratDL += Double.valueOf(rs.getString(12)).doubleValue();
						totAdeudoTotDL    += Double.valueOf(rs.getString(13)).doubleValue();
					} else {
						numMon++;
						totMontoOperMon    += Double.valueOf(rs.getString(7)).doubleValue();
						totSaldoInsMon     += Double.valueOf(rs.getString(8)).doubleValue();
						totCapitalVigMon   += Double.valueOf(rs.getString(9)).doubleValue();
						totCapitalVencMon  += Double.valueOf(rs.getString(10)).doubleValue();
						totInteresVencMon  += Double.valueOf(rs.getString(11)).doubleValue();
						totInteresMoratMon += Double.valueOf(rs.getString(12)).doubleValue();
						totAdeudoTotMon    += Double.valueOf(rs.getString(13)).doubleValue();
					}
				
					pdfDoc.setCell(vecDatos.get(0).toString().trim(), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(vecDatos.get(2).toString().trim(), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(vecDatos.get(1).toString().trim(), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(vecDatos.get(3).toString().trim(), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(vecDatos.get(4).toString().trim(), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(vecDatos.get(5).toString().trim(), 5, false), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(vecDatos.get(6).toString().trim(), 2, false), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(7).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(8).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(9).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(10).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(11).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(12).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(13).toString().trim(), 2), "formasmen", ComunesPDF.CENTER);
					nRow++;
				}
				
				if(nRow > 0) { 
					if(numDL > 0){
						pdfDoc.setCell("TOTAL PARCIAL D�LARES", "formasmenB", ComunesPDF.RIGHT, 7);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totMontoOperDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totCapitalVigDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totCapitalVencDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresVencDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresMoratDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totAdeudoTotDL).toString(), 2), "formasmen", ComunesPDF.CENTER);
					}           
					if(numMon>0){
						pdfDoc.setCell("TOTAL PARCIAL MONEDA NACIONAL", "formasmenB", ComunesPDF.RIGHT, 7);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totMontoOperMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totSaldoInsMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totCapitalVigMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totCapitalVencMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresVencMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totInteresMoratMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totAdeudoTotMon).toString(), 2), "formasmen", ComunesPDF.CENTER);
					}
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
		}		
		return nombreArchivo;
	}
	
	public List getConditions() {
		return conditions;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}   


	public String getMoneda() {
		return moneda;
	}


	public void setCliente(String cliente) {
		this.cliente = cliente;
	}


	public String getCliente() {
		return cliente;
	}


	public void setPrestamo(String prestamo) {
		this.prestamo = prestamo;
	}


	public String getPrestamo() {
		return prestamo;
	}


	public void setFechaCorte(String FechaCorte) {
		this.FechaCorte = FechaCorte;
	}


	public String getFechaCorte() {
		return FechaCorte;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public String getIc_if() {
		return ic_if;
	}


  public void setTipoLinea(String tipoLinea)
  {
    this.tipoLinea = tipoLinea;
  }


  public String getTipoLinea()
  {
    return tipoLinea;
  }

}