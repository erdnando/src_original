package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class LimitesPorIF implements IQueryGeneratorRegExtJS {
	public LimitesPorIF() { }
	
	private List conditions;
	StringBuffer strQuery;
	
	//Condiciones de la consulta
	private String iNoCliente;
	private String icIF;
	
	private static final Log log = ServiceLocator.getInstance().getLog(LimitesPorIF.class);
	
   public String getQuery(){
	 String stQuery = "";
	  
	 stQuery = "SELECT (i.ic_if || '' || cpe.ic_epo) as ID, " +
	           "       I.cg_razon_social AS nombre_if, " +
		        "       ie.in_limite_epo AS limite, " +
				  "  CASE   " +
				  "  WHEN ie.fn_limite_utilizado <> 0  AND ie.in_limite_epo <> 0  "+
		        "     THEN  ROUND(((ie.fn_limite_utilizado/ie.in_limite_epo)*100),5) " +
				  "  ELSE 0  " +
				  "  END AS porcentaje, "+         
		        "       (ie.in_limite_epo - ie.fn_limite_utilizado) disponible, " +
				  "      nvl(ie.in_limite_epo,0) - ( nvl(ie.fn_limite_utilizado, 0)  + NVL (ie.fn_monto_comprometido, 0)  )  as  disponible_des, " +
		        "       CASE WHEN ie.cs_activa_limite = 'S' THEN 'Activa' ELSE 'Inactiva' END AS estatus , " +
				  "       ie.fn_monto_comprometido AS monto_comprometido, " +
				  "       ie.fn_monto_comprometido_dl AS monto_comprometido_dl, " +				  
		        "       i.ic_if as claveIf, " + //Fodea 022-2010 fvr
		        "	    TO_char(ie.df_venc_linea, 'dd/mm/yyyy')	AS fechalinea,  " +
		        "       TO_char(ie.df_cambio_admon, 'dd/mm/yyyy') AS fechacambio,  " +
              "       CASE WHEN ie.CS_FECHA_LIMITE = 'S' THEN 'SI' ELSE 'NO' END as csFechalimite " +
				  
				   "   ,ie.in_limite_epo_dl AS limite_dl " +					
					" , CASE "+
					"	WHEN ie.fn_limite_utilizado_dl<>0 and  ie.in_limite_epo_dl <>0   "+
					"		THEN ROUND (((ie.fn_limite_utilizado_dl / ie.in_limite_epo_dl) * 100), 5     )  "+
					"	 ELSE 0  "+
					"  END AS porcentaje_dl "+						
		        "  , (ie.in_limite_epo_dl - ie.fn_limite_utilizado_dl) DISPONIBLE_DL " +  
				  "  , nvl(ie.in_limite_epo_dl,0) - ( nvl(ie.fn_limite_utilizado_dl, 0)  + NVL (ie.fn_monto_comprometido_dl, 0)  )  as  DISPONIBLE_DES_DL ,  " +				   
				 
				 " decode(ie.CS_ACTIVA_LIMITE , 'S', 'Activa' ,'N' , 'Inactiva') as DESCRIPCION_ESTATUS,   "+        
				 "  TO_CHAR (ie.DF_BLOQ_DES, 'dd/mm/yyyy') as FECHA_BLOQ_DESB,   "+
				 " ie.CG_USUARIO_BLOQ_DES as USUARIO_BLOQ_DESB,   "+
				 " ie.CG_DEPENDENCIA_BLOQ_DES as DEPENDENCIA_BLOQ_DESB   "+  
									
		        "  FROM comcat_if i, comrel_if_epo ie,comrel_producto_epo cpe " +
		        " WHERE ie.ic_if = i.ic_if " +
		        "       and UPPER(ie.cs_vobo_nafin) = 'S' " +
		        "       AND  (  ie.in_limite_epo <> 0    or   ie.in_limite_epo_dl <> 0  ) " +
		        "       and cpe.ic_epo=ie.ic_epo " +
		        "       and cpe.ic_producto_nafin=1 " +
		        "       and cpe.cs_limite_pyme!='S' ";
	
	 return stQuery;
	}
	
	public String getAggregateCalculationQuery() {
		conditions	= new ArrayList();
		strQuery	= new StringBuffer();
		
		log.debug("getAggregateCalculationQuery)"+strQuery.toString());
		log.debug("getAggregateCalculationQuery)"+conditions);
		
		return strQuery.toString();
   }
  
   public String getDocumentQuery() {
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		
		strQuery.append(getQuery());
		
		if(!iNoCliente.equals("")){
		 strQuery.append("    And ie.ic_epo = ? ");
		 conditions.add(iNoCliente);
		}
		if(!icIF.equals("")){
		 strQuery.append("    And ie.ic_if = ? ");
		 conditions.add(icIF);
		}
		
		strQuery.append(" ORDER BY i.cg_razon_social "); 
		
		log.debug("getDocumentQuery"+strQuery.toString());
		log.debug("getDocumentQuery)"+conditions);
		
		return strQuery.toString();
  }
  
  public String getDocumentSummaryQueryForIds(List pageIds) {
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		
		strQuery.append("SELECT * FROM ( ");
		strQuery.append(getQuery());
		
		if(!iNoCliente.equals("")){
		 strQuery.append("    And ie.ic_epo = ? ");
		 conditions.add(iNoCliente);
		}
		if(!icIF.equals("")){
		 strQuery.append("    And ie.ic_if = ? ");
		 conditions.add(icIF);
		}
		
		strQuery.append(" ORDER BY i.cg_razon_social "); 
		
		strQuery.append(" ) Tab ");
		
		 for(int i=0;i<pageIds.size();i++) {
		  List lItem = (List)pageIds.get(i);
		  if(i==0) {
		   strQuery.append(" WHERE Tab.ID in ( ");
		  }
		  strQuery.append("?");
		  conditions.add(new Long(lItem.get(0).toString()));					
		  if(i!=(pageIds.size()-1)) {
		   strQuery.append(",");
		  }else{
		   strQuery.append(" ) ");
		  }			
		 }
		
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		
		return strQuery.toString();
  }
  
  public String getDocumentQueryFile() {
	   conditions	= new ArrayList();
		strQuery		= new StringBuffer();	
		
		strQuery.append(getQuery());
		
		if(!iNoCliente.equals("")){
		 strQuery.append("    And ie.ic_epo = ? ");
		 conditions.add(iNoCliente);
		}
		if(!icIF.equals("")){
		 strQuery.append("    And ie.ic_if = ? ");
		 conditions.add(icIF);
		}
		
		strQuery.append(" ORDER BY i.cg_razon_social "); 
		   
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		
		return strQuery.toString();
  }
  
  
  /**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");
		String linea = "";
		String nombreArchivo = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;

		try {
			if(tipo.equals("CSV")) {
				linea = "Intermediario Financiero, "+
				" Monto Limite Moneda Nacional ,Porcentaje de utilizaci�n Moneda Nacional ,Monto Disponible Moneda Nacional ," +
				" Monto Limite D�lar Americano ,Porcentaje de utilizaci�n D�lar Americano ,Monto Disponible D�lar Americano ," +		  
				" Estatus,Monto Comprometido Moneda Nacional , Monto Comprometido D�lar Americano ,   Monto Disponible despu�s de Comprometido Moneda Nacional, Monto Disponible despu�s de Comprometido D�lar Americano,Fecha Vencimiento L�nea de Cr�dito,Fecha Cambio de Administraci�n,Validar Fechas L�mite , "+
				" Estatus, Fecha de Bloqueo/Desbloqueo, Usuario de Bloqueo/Desbloqueo  ,  Dependencia que Bloqueo/Desbloqueo \n";

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);

				while (rs.next()){
					String nombreIF             = (rs.getString("NOMBRE_IF")             == null) ? "" : rs.getString("NOMBRE_IF");
					String limite               = (rs.getString("LIMITE")                == null) ? "" : rs.getString("LIMITE");
					String porcentaje           = (rs.getString("PORCENTAJE")            == null) ? "" : rs.getString("PORCENTAJE");
					String disponible           = (rs.getString("DISPONIBLE")            == null) ? "" : rs.getString("DISPONIBLE");
					String estatus              = (rs.getString("ESTATUS")               == null) ? "" : rs.getString("ESTATUS");
					String montoComprometido    = (rs.getString("MONTO_COMPROMETIDO")    == null) ? "0": rs.getString("MONTO_COMPROMETIDO");
					String montoComprometido_dl = (rs.getString("MONTO_COMPROMETIDO_DL") == null) ? "0": rs.getString("MONTO_COMPROMETIDO_DL");
					String fechaLinea           = (rs.getString("FECHALINEA")            == null) ? "" : rs.getString("FECHALINEA");
					String fechaCambio          = (rs.getString("FECHACAMBIO")           == null) ? "" : rs.getString("FECHACAMBIO");
					String fechaLimite          = (rs.getString("CSFECHALIMITE")         == null) ? "" : rs.getString("CSFECHALIMITE");
					String limite_dl            = (rs.getString("LIMITE_DL")             == null) ? "" : rs.getString("LIMITE_DL");
					String porcentaje_dl        = (rs.getString("PORCENTAJE_DL")         == null) ? "" : rs.getString("PORCENTAJE_DL");
					String disponible_dl        = (rs.getString("DISPONIBLE_DL")         == null) ? "" : rs.getString("DISPONIBLE_DL");
					String disponible_des_dl    = (rs.getString("DISPONIBLE_DES_DL")     == null) ? "" : rs.getString("DISPONIBLE_DES_DL");
					String disponible_des       = (rs.getString("DISPONIBLE_DES")        == null) ? "" : rs.getString("DISPONIBLE_DES");
					String descripcionEstatus   = (rs.getString("DESCRIPCION_ESTATUS")   == null) ? "" : rs.getString("DESCRIPCION_ESTATUS");
					String fecha_bloq_des       = (rs.getString("FECHA_BLOQ_DESB")       == null) ? "" : rs.getString("FECHA_BLOQ_DESB");
					String usuario_bloq_des     = (rs.getString("USUARIO_BLOQ_DESB")     == null) ? "" : rs.getString("USUARIO_BLOQ_DESB");
					String dependencia_bloq_des = (rs.getString("DEPENDENCIA_BLOQ_DESB") == null) ? "" : rs.getString("DEPENDENCIA_BLOQ_DESB");

					linea = nombreIF.replace(',',' ') + ", " +
						limite.replace(',',' ') + ", " +
						porcentaje.replace(',',' ') + ", " +
						disponible.replace(',',' ') + ", " +
						limite_dl.replace(',',' ') + ", " +
						porcentaje_dl.replace(',',' ') + ", " +
						disponible_dl.replace(',',' ') + ", " +
						estatus.replace(',',' ') + ", " +
						montoComprometido.replace(',',' ') + ", " +
						montoComprometido_dl.replace(',',' ') + ", " +
						disponible_des.replace(',',' ') + ", " +
						disponible_des_dl.replace(',',' ') + ", " +
						fechaLinea.replace(',',' ') + ", " +
						fechaCambio.replace(',',' ') + ", " +
						fechaLimite.replace(',',' ') + ", " +
						descripcionEstatus.replace(',',' ') + ","+
						fecha_bloq_des.replace(',',' ') + ","+
						usuario_bloq_des.replace(',',' ') + ","+
						dependencia_bloq_des.replace(',',' ') + ","+
						"\n";	
					buffer.write(linea);
				}
				buffer.close();
			} else if(tipo.equals("PDF")){
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
				pdfDoc.setLTable(19,100);
				pdfDoc.setLCell("Intermediario Financiero", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Limite Moneda Nacional ", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Porcentaje de utilizaci�n Moneda Nacional", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Disponible Moneda Nacional ", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Limite D�lar Americano ", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Porcentaje de utilizaci�n D�lar Americano ", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Disponible D�lar Americano", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Comprometido  Moneda Nacional", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Comprometido D�lar Americano", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Disponible despu�s de Comprometido Moneda Nacional ", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Disponible despu�s de Comprometido D�lar Americano ", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Vencimiento L�nea de Cr�dito", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Cambio de Administraci�n", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Validar Fechas L�mite", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(" Estatus ", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(" Fecha de Bloqueo/Desbloqueo", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(" Usuario de Bloqueo/Desbloqueo", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell( "Dependencia que Bloqueo/Desbloqueo", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();

				while (rs.next()) {
					String nombreIF             = (rs.getString("NOMBRE_IF")             == null) ? "" : rs.getString("NOMBRE_IF");
					String limite               = (rs.getString("LIMITE")                == null) ? "" : rs.getString("LIMITE");
					String porcentaje           = (rs.getString("PORCENTAJE")            == null) ? "" : rs.getString("PORCENTAJE");
					String disponible           = (rs.getString("DISPONIBLE")            == null) ? "" : rs.getString("DISPONIBLE");
					String estatus              = (rs.getString("ESTATUS")               == null) ? "" : rs.getString("ESTATUS");
					String montoComprometido    = (rs.getString("MONTO_COMPROMETIDO")    == null) ? "0": rs.getString("MONTO_COMPROMETIDO");
					String montoComprometido_dl = (rs.getString("MONTO_COMPROMETIDO_DL") == null) ? "0": rs.getString("MONTO_COMPROMETIDO_DL");
					String fechaLinea           = (rs.getString("FECHALINEA")            == null) ? "" : rs.getString("FECHALINEA");
					String fechaCambio          = (rs.getString("FECHACAMBIO")           == null) ? "" : rs.getString("FECHACAMBIO");
					String fechaLimite          = (rs.getString("CSFECHALIMITE")         == null) ? "" : rs.getString("CSFECHALIMITE");
					String limite_dl            = (rs.getString("LIMITE_DL")             == null) ? "" : rs.getString("LIMITE_DL");
					String porcentaje_dl        = (rs.getString("PORCENTAJE_DL")         == null) ? "" : rs.getString("PORCENTAJE_DL");
					String disponible_dl        = (rs.getString("DISPONIBLE_DL")         == null) ? "" : rs.getString("DISPONIBLE_DL");
					String disponible_des_dl    = (rs.getString("DISPONIBLE_DES_DL")     == null) ? "" : rs.getString("DISPONIBLE_DES_DL");
					String disponible_des       = (rs.getString("DISPONIBLE_DES")        == null) ? "" : rs.getString("DISPONIBLE_DES");
					String descripcionEstatus   = (rs.getString("DESCRIPCION_ESTATUS")   == null) ? "" : rs.getString("DESCRIPCION_ESTATUS");
					String fecha_bloq_des       = (rs.getString("FECHA_BLOQ_DESB")       == null) ? "" : rs.getString("FECHA_BLOQ_DESB");
					String usuario_bloq_des     = (rs.getString("USUARIO_BLOQ_DESB")     == null) ? "" : rs.getString("USUARIO_BLOQ_DESB");
					String dependencia_bloq_des = (rs.getString("DEPENDENCIA_BLOQ_DESB") == null) ? "" : rs.getString("DEPENDENCIA_BLOQ_DESB");

					pdfDoc.setLCell(nombreIF,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(limite,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(Comunes.formatoDecimal(porcentaje,5)+"%","formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(disponible,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(limite_dl,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(Comunes.formatoDecimal(porcentaje_dl,5)+"%","formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(disponible_dl,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoComprometido,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoComprometido_dl,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(disponible_des,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(disponible_des_dl,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(fechaLinea,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaCambio,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaLimite,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(descripcionEstatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_bloq_des,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(usuario_bloq_des,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(dependencia_bloq_des,"formas",ComunesPDF.LEFT);
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}
		} catch (Throwable e){
			throw new AppException("Error al generar el archivo",e);
		} finally{
			try {
				rs.close();
			} catch(Exception e){}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}

	public void setIcIF(String icIF) {
		this.icIF = icIF;
	}

	public String getIcIF() {
		return icIF;
	}

	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}

	public String getINoCliente() {
		return iNoCliente;
	}
	
	
  

}