package com.netro.descuento;


import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.IQueryGeneratorThreadRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsSolicNafinDE  implements IQueryGeneratorPS, IQueryGeneratorThreadRegExtJS{//IQueryGeneratorThreadRegExtJS

	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsSolicNafinDE.class);
			
	private int numList = 1;	
	private String cc_acuse;
	private String ic_epo;
	private String fn_monto_dsctoMin;
	private String fn_monto_dsctoMax; 
	private String ic_if;
	private String df_fecha_solicitudMin;   
	private String df_fecha_solicitudMax;
	private String ic_pyme;
	private String ic_estado;
	private String ic_folioMin;
	private String ic_estatus_solic;
	private String ic_moneda;
	private String ic_tasa;
	private String tipo_busqueda;
	private String ordenadoBy;
	private String esBANCOMEXT;
	private int countReg;
	//private String esTotal2;
	private String aplicaFloating;
	
	
	public ConsSolicNafinDE(){}

	public int getNumList(HttpServletRequest request){
		return 1;
	}

	public ArrayList getConditions(HttpServletRequest request){

		ArrayList condiciones = new ArrayList();
		String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo") == null) ? "" : request.getParameter("ic_banco_fondeo");
		String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
		String ic_pyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
		String ic_folioMin = (request.getParameter("ic_folioMin")== null) ? "" : request.getParameter("ic_folioMin").trim();
		String ic_estatus_solic = (request.getParameter("ic_estatus_solic") == null) ? "" : request.getParameter("ic_estatus_solic");
		String ic_moneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
		String cc_acuse = (request.getParameter("cc_acuse") == null) ? "" : request.getParameter("cc_acuse").trim();
		String fn_monto_dsctoMin = (request.getParameter("fn_monto_dsctoMin") == null) ? "" : request.getParameter("fn_monto_dsctoMin");
		String fn_monto_dsctoMax = (request.getParameter("fn_monto_dsctoMax") == null) ? "" : request.getParameter("fn_monto_dsctoMax");
		String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") == null) ? "" : request.getParameter("df_fecha_solicitudMin");
		String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") == null) ? "" : request.getParameter("df_fecha_solicitudMax");
		String ic_estado = (request.getParameter("ic_estado") == null) ? "" : request.getParameter("ic_estado");
		String ordenaIf = (request.getParameter("ordenaIf") == null) ? "" : request.getParameter("ordenaIf");
		String ordenaPyme = (request.getParameter("ordenaPyme") == null) ? "" : request.getParameter("ordenaPyme");
		String ordenaEpo = (request.getParameter("ordenaEpo") == null) ? "" : request.getParameter("ordenaEpo");
		String ordenaFecha = (request.getParameter("ordenaFecha") == null) ? "" : request.getParameter("ordenaFecha");
		String ordenaEstatus = (request.getParameter("ordenaEstatus") == null) ? "" : request.getParameter("ordenaEstatus");
		String ordenaFolio = (request.getParameter("ordenaFolio") == null) ? "" : request.getParameter("ordenaFolio");
		String tipo_busqueda = (request.getParameter("tipo_busqueda") == null) ? "" : request.getParameter("tipo_busqueda");
		//String esTotal2 = (request.getParameter("esTotal2") != null) ? request.getParameter("esTotal2") : "";
		
		if (ic_if.equals("") && ic_epo.equals("") && ic_moneda.equals("")
			&& ic_estatus_solic.equals("") && ic_estado.equals("")
			&& ic_folioMin.equals("") && cc_acuse.equals("")
			&& fn_monto_dsctoMin.equals("") && df_fecha_solicitudMin.equals("")
			&& !tipo_busqueda.equals(""))
		{
			df_fecha_solicitudMin = (new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}

		if (!ic_if.equals("")) {
			condiciones.add(new Integer(ic_if));
		}
		if (!ic_epo.equals("")) {
			condiciones.add(new Integer(ic_epo));
			condiciones.add(new Integer(ic_epo));
			condiciones.add(new Integer(ic_epo));
			condiciones.add(new Integer(ic_epo));
		
			if (!ic_pyme.equals("") && !ic_pyme.equals("T"))
				condiciones.add(new Integer(ic_pyme));
		}
		if(!ic_folioMin.equals("")) {
			condiciones.add(ic_folioMin);
		}
		if (!ic_estatus_solic.equals("")) {
			condiciones.add(new Integer(ic_estatus_solic));
		}
		if (!ic_moneda.equals("")) {
			condiciones.add(new Integer(ic_moneda));
		}
		if (!cc_acuse.equals("")) {
			condiciones.add(cc_acuse);
		}
		if(!fn_monto_dsctoMin.equals("")) {
		  if(!fn_monto_dsctoMax.equals("")) {
			  condiciones.add(new Integer(fn_monto_dsctoMin));
			  condiciones.add(new Integer(fn_monto_dsctoMax));
		  } else {
			  condiciones.add(new Integer(fn_monto_dsctoMin));
		  }
		}
		if(!df_fecha_solicitudMin.equals("")) {
			if(!df_fecha_solicitudMax.equals("")) {
				condiciones.add(df_fecha_solicitudMin);
				condiciones.add(df_fecha_solicitudMax);
			} else {
				condiciones.add(df_fecha_solicitudMin);
			}
		}
		if (!ic_estado.equals("")) {
			condiciones.add(new Integer(ic_estado));
		}

		return condiciones;
	}

	public String getAggregateCalculationQuery(HttpServletRequest request){
  // Este metodo generara el query de los totales 
		StringBuffer query = new StringBuffer("");
		StringBuffer condicion = new StringBuffer("");
		String criterioOrdenamiento=" group by d.ic_moneda,'ConsSolicNafinDE.java' order by d.ic_moneda ";
		// Parametros de la pagina
		String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
		String ic_pyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
		String ic_folioMin = (request.getParameter("ic_folioMin")== null) ? "" : request.getParameter("ic_folioMin").trim();
		String ic_estatus_solic = (request.getParameter("ic_estatus_solic") == null) ? "" : request.getParameter("ic_estatus_solic");
		String ic_moneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
		String cc_acuse = (request.getParameter("cc_acuse") == null) ? "" : request.getParameter("cc_acuse").trim();
		String fn_monto_dsctoMin = (request.getParameter("fn_monto_dsctoMin") == null) ? "" : request.getParameter("fn_monto_dsctoMin");
		String fn_monto_dsctoMax = (request.getParameter("fn_monto_dsctoMax") == null) ? "" : request.getParameter("fn_monto_dsctoMax");
		String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") == null) ? "" : request.getParameter("df_fecha_solicitudMin");
		String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") == null) ? "" : request.getParameter("df_fecha_solicitudMax");
		String ic_estado = (request.getParameter("ic_estado") == null) ? "" : request.getParameter("ic_estado");
		String tipo_busqueda = (request.getParameter("tipo_busqueda") == null) ? "" : request.getParameter("tipo_busqueda");
		String ic_banco_fondeo = (request.getParameter("bancoFondeo") == null) ? "" : request.getParameter("bancoFondeo");
		String tipoTasa = (request.getParameter("tipoTasa") == null) ? "" : request.getParameter("tipoTasa");
				
		if(!tipoTasa.equals("")) {
			condicion.append(" and ds.CG_TIPO_TASA ='"+tipoTasa+"'");		
		}
		// Generacion del query
		if (ic_if.equals("") && ic_epo.equals("") && ic_moneda.equals("")
			&& ic_estatus_solic.equals("") && ic_estado.equals("")
			&& ic_folioMin.equals("") && cc_acuse.equals("")
			&& fn_monto_dsctoMin.equals("") && df_fecha_solicitudMin.equals("")
			&& !tipo_busqueda.equals(""))
		{
			df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}
		if (!ic_if.equals("")) {
			  condicion.append(" and ds.ic_if = ? ");
		}
		if (!ic_epo.equals("")) {
			condicion.append(" AND ds.ic_epo = ? ");
			condicion.append(" and d.ic_epo = ? ");
			condicion.append(" and pe.ic_epo = ? ");
			condicion.append(" and e.ic_epo = ? ");
			
			if (!ic_pyme.equals("") && !ic_pyme.equals("T")){
				condicion.append(" and d.ic_pyme = ? ");
			}	
		}
		if(!ic_folioMin.equals(""))
		{
			condicion.append(" and s.ic_folio = ? ");
		}
		if (!ic_estatus_solic.equals(""))
		{
			condicion.append(" and s.ic_estatus_solic = ? ");
		}
		if (!ic_moneda.equals(""))
		{
			condicion.append(" and d.ic_moneda = ? ");
		}
		if (!cc_acuse.equals(""))
		{
			condicion.append(" and s.cc_acuse = ? ");
		}
		if(!fn_monto_dsctoMin.equals("")) {
		  if(!fn_monto_dsctoMax.equals("")) {
			  condicion.append(" and d.fn_monto_dscto between ? and ? ");
		  } else {
			  condicion.append(" and d.fn_monto_dscto = ? ");
		  }
		}
		if(!df_fecha_solicitudMin.equals("")) {
			if(!df_fecha_solicitudMax.equals("")) {
			  condicion.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
			} else {
			  condicion.append(" and s.df_fecha_solicitud = TO_DATE(?,'dd/mm/yyyy') ");
			}
		}
		if (!ic_estado.equals(""))
		{
		   condicion.append( " and d.ic_pyme in (select distinct ic_pyme from com_domicilio where ic_estado = ? and cs_fiscal = 'S') ");
		}
		//if(!tipo_busqueda.equals(""))		
		if(!tipo_busqueda.equals("") && !tipo_busqueda.equals("F"))
			condicion.append(" and s.cg_tipo_busqueda = '" + tipo_busqueda + "'");
		
		if(tipo_busqueda.equals("A")&&ic_estatus_solic.equals(""))
			condicion.append(" and s.ic_estatus_solic in (3,5,6)");

		query.append(" select  /*+ index (s in_com_solicitud_13_nuk)  */"+
				"		d.ic_moneda CVE_MONEDA,"   +
				" 	   count(ds.ic_documento) TOTAL_DOCTOS, "   +
				" 	   sum(nvl(d.fn_monto,0)) MONTO_DOCTOS, "   +
				" 	   sum(nvl(d.fn_monto_dscto,0)) MONTO_DESC_DOCTOS, "   +
				" 	   sum(nvl(ds.in_importe_recibir,0)) MONTO_RECIBIR_DOCTOS, "   +
				" 	   sum(decode(s.ic_estatus_solic,2,1,0)) TOTAL_SOLIC_RECIBIDAS, "   +
				" 	   sum(decode(s.ic_estatus_solic,3,1,0)) TOTAL_SOLIC_OPER, "   +
				" 	   sum(decode(s.ic_estatus_solic,4,1,0)) TOTAL_SOLIC_RECH,	  "   +
				" 	   sum(decode(s.ic_estatus_solic,2,nvl(d.fn_monto_dscto,0),0)) MONTO_DESC_RECIB, "   +
				" 	   sum(decode(s.ic_estatus_solic,3,nvl(d.fn_monto_dscto,0),0)) MONTO_DESC_OPER, "   +
				" 	   sum(decode(s.ic_estatus_solic,4,nvl(d.fn_monto_dscto,0),0)) MONTO_DESC_RECH,	"+
				" 	   sum(nvl(d.fn_monto,0)-nvl(d.fn_monto_dscto,0)) MONTO_RECURSO, "   +
				" 	   sum(nvl(ds.in_importe_interes,0)) MONTO_INTERES, " +
				" 	   sum((nvl(s.FG_PORC_COMISION_FONDEO,0)*nvl(ds.in_importe_interes,0))/100) MONTO_COMISION, " +
				"	  'ConsSolicNafinDE.java' ORIGENQUERY "  +
				" from "   +
				" 	com_docto_seleccionado ds, "   +
				" 	com_solicitud s, "   +
				" 	com_documento d , "   +
				" 	comrel_pyme_epo pe,  "   +
				" 	comcat_epo e, " +
				"  comrel_producto_epo prodepo " +
				//"   (select ic_folio, count(*) as procesada from com_control04 where cs_estatus = 'S' group by ic_folio) procAut " +
				" where s.ic_documento=ds.ic_documento and "   +
				" 	ds.ic_epo = e.ic_epo and "+
				" 	e.ic_epo = prodepo.ic_epo and " +
				"  prodepo.ic_producto_nafin = 1 and " +
				//"  (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) and " +
				" 	ds.ic_documento=d.ic_documento and "   +
				" 	d.ic_pyme = pe.ic_pyme and "   +
				" 	d.ic_epo = pe.ic_epo "+
				(!ic_banco_fondeo.equals("")?" and s.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
				//" 	s.ic_folio = procAut.ic_folio (+) and "   +
				//" 	s.cs_tipo_solicitud='C' 
				condicion.toString()); 				

		query.append(criterioOrdenamiento);
		System.out.println("Totales:"+query.toString());
		return query.toString();
	}

 	public String getDocumentQuery(HttpServletRequest request){
		// Genera el query que devuelve las llaves primarias de la consulta
		StringBuffer query = new StringBuffer("");
		StringBuffer condicion = new StringBuffer("");
		String criterioOrdenamiento=" ORDER BY DS.IC_DOCUMENTO ";
		// Parametros de la pagina
		String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
		String ic_pyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
		String ic_folioMin = (request.getParameter("ic_folioMin")== null) ? "" : request.getParameter("ic_folioMin").trim();
		String ic_estatus_solic = (request.getParameter("ic_estatus_solic") == null) ? "" : request.getParameter("ic_estatus_solic");
		String ic_moneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
		String cc_acuse = (request.getParameter("cc_acuse") == null) ? "" : request.getParameter("cc_acuse").trim();
		String fn_monto_dsctoMin = (request.getParameter("fn_monto_dsctoMin") == null) ? "" : request.getParameter("fn_monto_dsctoMin");
		String fn_monto_dsctoMax = (request.getParameter("fn_monto_dsctoMax") == null) ? "" : request.getParameter("fn_monto_dsctoMax");
		String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") == null) ? "" : request.getParameter("df_fecha_solicitudMin");
		String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") == null) ? "" : request.getParameter("df_fecha_solicitudMax");
		String ic_estado = (request.getParameter("ic_estado") == null) ? "" : request.getParameter("ic_estado");
		String tipo_busqueda = (request.getParameter("tipo_busqueda") == null) ? "" : request.getParameter("tipo_busqueda");
		String ic_banco_fondeo = (request.getParameter("bancoFondeo") == null) ? "" : request.getParameter("bancoFondeo");
		String ic_tasa = (request.getParameter("tipoTasa") == null) ? "" : request.getParameter("tipoTasa");
		// Generacion del query		
		if(!ic_tasa.equals("")) {
			condicion.append(" and ds.CG_TIPO_TASA ='"+ic_tasa+"'");		
		}		
		if (ic_if.equals("") && ic_epo.equals("") && ic_moneda.equals("")
			&& ic_estatus_solic.equals("") && ic_estado.equals("")
			&& ic_folioMin.equals("") && cc_acuse.equals("")
			&& fn_monto_dsctoMin.equals("") && df_fecha_solicitudMin.equals("")
			&& !tipo_busqueda.equals(""))
		{
			df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}
		if (!ic_if.equals("")) {
			  condicion.append(" and ds.ic_if = ? ");
		}
		if (!ic_epo.equals("")) {
			condicion.append(" AND ds.ic_epo = ? ");
			condicion.append(" and d.ic_epo= ? ");
			condicion.append(" and pe.ic_epo= ? ");
			condicion.append(" and e.ic_epo= ? ");
	 		
		  if (!ic_pyme.equals("") && !ic_pyme.equals("T")){
			condicion.append(" and d.ic_pyme = ? ");
		  }
		}
		if(!ic_folioMin.equals(""))
		{
			condicion.append(" and s.ic_folio = ? ");
		}
		if (!ic_estatus_solic.equals(""))
		{
			condicion.append(" and s.ic_estatus_solic = ? ");
		}
		if (!ic_moneda.equals(""))
		{
			condicion.append(" and d.ic_moneda = ? ");
		}
		if (!cc_acuse.equals(""))
		{
			condicion.append(" and s.cc_acuse = ? ");
		}
		
		if(!fn_monto_dsctoMin.equals("")){
		  if(!fn_monto_dsctoMax.equals("")){
			  condicion.append(" and d.fn_monto_dscto between ? and ? ");
		  } else {
			  condicion.append(" and d.fn_monto_dscto = ?");
		  }
		}
		if(!df_fecha_solicitudMin.equals("")){
			if(!df_fecha_solicitudMax.equals("")) {
			  condicion.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
			} else {
			  condicion.append(" and s.df_fecha_solicitud = TO_DATE(?,'dd/mm/yyyy') ");
			}
		}
		if (!ic_estado.equals("")) {
			condicion.append( " and d.ic_pyme in (select distinct ic_pyme from com_domicilio where ic_estado = ? and cs_fiscal = 'S') ");
		}
		if(!tipo_busqueda.equals(""))
			condicion.append(" and s.cg_tipo_busqueda = '" + tipo_busqueda + "'");
		
		if(tipo_busqueda.equals("A")&&ic_estatus_solic.equals(""))
			condicion.append(" and s.ic_estatus_solic in (3,5,6)");



			query.append(" select  /*+ index (s in_com_solicitud_13_nuk)  */"+
				" ds.ic_documento "+
				" from "   +
				" 	com_docto_seleccionado ds, "   +
				" 	com_solicitud s, " +
				" 	com_documento d , " +
				" 	comrel_pyme_epo pe,  "   +
				" 	comcat_epo e, " +
				"  comrel_producto_epo prodepo " +
//				"   (select ic_folio, count(*) as procesada from com_control04 where cs_estatus = 'S' group by ic_folio) procAut " +
				" where s.ic_documento=ds.ic_documento and "   +
				" 	ds.ic_documento=d.ic_documento and "   +
				" 	ds.ic_epo = e.ic_epo and " +
				" 	d.ic_pyme = pe.ic_pyme and "   +
				" 	d.ic_epo = pe.ic_epo and " +
				" 	e.ic_epo = prodepo.ic_epo and " +
				"  prodepo.ic_producto_nafin = 1 " +
				(!ic_banco_fondeo.equals("")?" and s.ic_banco_fondeo="+ic_banco_fondeo+" ":"") + 
				//"  (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) " +

/*				" 	s.ic_folio = procAut.ic_folio (+) and "   +
				" 	s.cs_tipo_solicitud='C' " */
				condicion.toString()); 				

		query.append(criterioOrdenamiento);
		System.out.println("query.toString() "+query.toString());

		return query.toString();
	}

	public String getDocumentQueryFile(HttpServletRequest request){
	  // Genera el query que devuelve toda la informacion de la consulta
		StringBuffer query = new StringBuffer("");
		StringBuffer condicion = new StringBuffer("");
		String criterioOrdenamiento="";
		// Parametros de la pagina
		String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
		String ic_pyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
		String ic_folioMin = (request.getParameter("ic_folioMin")== null) ? "" : request.getParameter("ic_folioMin").trim();
		String ic_estatus_solic = (request.getParameter("ic_estatus_solic") == null) ? "" : request.getParameter("ic_estatus_solic");
		String ic_moneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
		String cc_acuse = (request.getParameter("cc_acuse") == null) ? "" : request.getParameter("cc_acuse").trim();
		String fn_monto_dsctoMin = (request.getParameter("fn_monto_dsctoMin") == null) ? "" : request.getParameter("fn_monto_dsctoMin");
		String fn_monto_dsctoMax = (request.getParameter("fn_monto_dsctoMax") == null) ? "" : request.getParameter("fn_monto_dsctoMax");
		String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") == null) ? "" : request.getParameter("df_fecha_solicitudMin");
		String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") == null) ? "" : request.getParameter("df_fecha_solicitudMax");
		String ic_estado = (request.getParameter("ic_estado") == null) ? "" : request.getParameter("ic_estado");
		String ordenaIf = (request.getParameter("ordenaIf") == null) ? "" : request.getParameter("ordenaIf");
		String ordenaPyme = (request.getParameter("ordenaPyme") == null) ? "" : request.getParameter("ordenaPyme");
		String ordenaEpo = (request.getParameter("ordenaEpo") == null) ? "" : request.getParameter("ordenaEpo");
		String ordenaFecha = (request.getParameter("ordenaFecha") == null) ? "" : request.getParameter("ordenaFecha");
		String ordenaEstatus = (request.getParameter("ordenaEstatus") == null) ? "" : request.getParameter("ordenaEstatus");
		String ordenaFolio = (request.getParameter("ordenaFolio") == null) ? "" : request.getParameter("ordenaFolio");
		String tipo_busqueda = (request.getParameter("tipo_busqueda") == null) ? "" : request.getParameter("tipo_busqueda");
		String ic_banco_fondeo = (request.getParameter("bancoFondeo") == null) ? "" : request.getParameter("bancoFondeo");
		String tipoTasa = (request.getParameter("tipoTasa") == null) ? "" : request.getParameter("tipoTasa");
		System.out.println("tipoTasa "+tipoTasa);
		System.out.println("****************** ");
		
		
		// Generacion del query		
		if(!tipoTasa.equals("")) {
			condicion.append(" and ds.CG_TIPO_TASA ='"+tipoTasa+"'");		
		}		
		//Esta variable indica si se tienen que consultar los documentos si es 1 si se consultaran
//		String 	banderaTablaDoctos 		= (request.getParameter("banderaTablaDoctos") == null) ? "" : request.getParameter("banderaTablaDoctos"); 
		String  consultaDoctos = "";
//		if(banderaTablaDoctos.equals("1")){
			consultaDoctos = ", e.ic_epo, TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO ";
//		}
		
		//String campos[]={"nombreEpo","nombreIf","df_fecha_solicitud","nombrePyme","cd_descripcion","ic_folio"};
		//String orden[]={ordenaEpo,ordenaIf,ordenaFecha,ordenaPyme,ordenaEstatus,ordenaFolio};
		// Generacion del query
		if (ic_if.equals("") && ic_epo.equals("") && ic_moneda.equals("")
			&& ic_estatus_solic.equals("") && ic_estado.equals("")
			&& ic_folioMin.equals("") && cc_acuse.equals("")
			&& fn_monto_dsctoMin.equals("") && df_fecha_solicitudMin.equals("")
			&& !tipo_busqueda.equals(""))
		{
			df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}
	  //_____________________________________ Criterios de ORDENAMIENTO
		  
		  //criterioOrdenamiento=Comunes.ordenarCampos(orden,campos);
		  criterioOrdenamiento =  ordenadoBy;
		  //if (!criterioOrdenamiento.equals(""))
			//criterioOrdenamiento=" order by "+criterioOrdenamiento;
	  //__________________________________ Fin de Criterios de ORDENAMIENTO
		if (!ic_if.equals("")) {
			  condicion.append(" and ds.ic_if =  ? ");
		}
		if (!ic_epo.equals("")) {
			condicion.append(" AND ds.ic_epo = ? ");
			condicion.append(" and d.ic_epo= ? ");
			condicion.append(" and pe.ic_epo= ? ");
			condicion.append(" and e.ic_epo= ? ");
	 		
	 
		  
		  if (!ic_pyme.equals("") && !ic_pyme.equals("T"))
			condicion.append(" and d.ic_pyme = ? ");
		}
		if(!ic_folioMin.equals(""))
		{
			condicion.append(" and s.ic_folio = ? ");
		}
		if (!ic_estatus_solic.equals(""))
		{
			condicion.append(" and s.ic_estatus_solic = ? ");
		}
		if (!ic_moneda.equals(""))
		{
			condicion.append(" and d.ic_moneda = ? ");
		}
		if (!cc_acuse.equals(""))
		{
			condicion.append(" and s.cc_acuse = ? ");
		}
		if(!fn_monto_dsctoMin.equals(""))
		{
		  if(!fn_monto_dsctoMax.equals(""))
		  {
			  condicion.append(" and d.fn_monto_dscto between ? and ? ");
		  } else
		  {
			  condicion.append(" and d.fn_monto_dscto = ? ");
		  }
		}
		if(!df_fecha_solicitudMin.equals(""))
		{
			if(!df_fecha_solicitudMax.equals(""))
			{
			  condicion.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
			}
			else
			{
			  condicion.append(" and s.df_fecha_solicitud = TO_DATE(?,'dd/mm/yyyy') ");
			}
		}
		if (!ic_estado.equals(""))
		{
		   condicion.append( " and d.ic_pyme in (select distinct ic_pyme from com_domicilio where ic_estado = ? and cs_fiscal = 'S') ");
		}
		if(!tipo_busqueda.equals(""))
			condicion.append(" and s.cg_tipo_busqueda = '" + tipo_busqueda + "'");
		
		if(tipo_busqueda.equals("A")&&ic_estatus_solic.equals(""))
			condicion.append(" and s.ic_estatus_solic in (3,5,6)");

		//query.append(" select /*+ ordered index(ds IN_COM_DOCTO_SELEC_04_NUK) use_nl(ds,d,pe,s,p,ps,a,cc,e,i,i2,es,m) */ 'Interna' as tipo "+
		query.append(" select /*+ index (s in_com_solicitud_13_nuk)  */ 'Interna' as tipo "+
		" , (decode (i.cs_habilitado,'N','*','S',' ')||' '||i.cg_razon_social) as nombreIF"+
		" , (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO"+
		" , (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+
		" , substr(s.ic_folio,1,10)||'-'||substr(s.ic_folio,11,1)as ic_folio"+
		" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as df_fecha_solicitud"+
		" , d.ic_moneda, m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto, ds.in_importe_recibir"+
		" , ds.in_tasa_aceptada, ds.in_importe_interes, es.cd_descripcion"+
		" , es.ic_estatus_solic, a.ig_numero_prestamo "+
		" , s.ig_numero_prestamo as NUMERO_PRESTAMO"+
		" , TO_CHAR(s.df_operacion,'DD/MM/YYYY') as df_operacion"+
		//" , decode (s.ic_estatus_solic, 3, decode ( procAut.procesada, 1, 'Automatica', Null, 'Manual'), '') as proceso"+
		" , DECODE (s.ic_estatus_solic,3 "+
		//" ,nvl2(procaut.ic_folio,'Automatica','Manual'), '') AS proceso "+
		" , DECODE (s.cg_tipo_busqueda,'A','Autom�tica','M','Manual'), '') AS proceso "+
		" , TO_CHAR(s.df_v_descuento, 'DD/MM/YYYY') as FECHA_VENCIMIENTO" +
		" , p.in_numero_sirac "+
		" ,	s.fg_porc_comision_fondeo as fg_porc_comision_fondeo " + 
		" , ds.in_importe_recibir as in_importe_recibir " + 
		" , decode(d.cs_dscto_especial, 'D', i2.cg_razon_social, 'I', i2.cg_razon_social, '') as beneficiario"+
		" , decode(d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, 0) as fn_porc_beneficiario"+
		" , decode(d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, 0) as RECIBIR_BENEF " +
		" , decode(d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I',ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 0) as NETO_REC_PYME "+
		" , d.cs_dscto_especial , s.fg_tasa_fondeo_nafin as TASA_FONDEO, bf.cd_descripcion NOM_BANCO_FONDEO ");

		query.append(consultaDoctos);
		query.append( ",decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as tipoTasa "); 
	
		query.append(", 'ConsSolicNafinDE.java' ORIGENQUERY ");

		/*" from com_solicitud s, comcat_estatus_solic es"+
		" , com_docto_seleccionado ds, com_documento d"+
		" , comcat_moneda m, comcat_epo e, comcat_pyme p, comrel_pyme_epo pe"+
		" , comcat_if i, com_cobro_credito cc, com_pedido_seleccionado ps, com_anticipo a, comcat_if i2"+
		", (select ic_folio, count(*) as procesada from com_control04 where cs_estatus = 'S' group by ic_folio) procAut"+
		" where"+
		" s.ic_documento=ds.ic_documento"+
		" and ds.ic_documento=d.ic_documento"+
		" and ds.ic_if=i.ic_if"+
		" and ds.ic_epo=e.ic_epo"+
		" and d.ic_pyme=p.ic_pyme"+
		" and d.ic_moneda=m.ic_moneda"+
		" and s.ic_estatus_solic=es.ic_estatus_solic"+
		" and d.ic_epo = pe.ic_epo and d.ic_pyme = pe.ic_pyme"+
		" and s.cs_tipo_solicitud='C'"+
		" and ds.ic_documento = cc.ic_documento(+) "+
		" and cc.ic_pedido = ps.ic_pedido(+)"+
		" and d.ic_beneficiario = i2.ic_if (+) "+
		" and ps.ic_pedido = a.ic_pedido(+)"+
		" and s.ic_folio = procAut.ic_folio (+) "+ condicion.toString());
	   */ 
		
		
		
		query.append(" FROM   "+
		" com_docto_seleccionado  ds,/*300,558*/ "+
		" com_documento  d, /*506,570*/ "+
		" comrel_pyme_epo pe,/*369,557*/ "+
		" com_solicitud s, /*298,664*/ "+
		" comcat_pyme p, /*91,777*/ "+
	//	" (SELECT IC_FOLIO FROM com_control04 /*259,849*/ "+
	//	"  WHERE cs_estatus = 'S') procaut, "+
		" com_pedido_seleccionado ps, /*89*/ "+
		" com_anticipo a, /*86*/"+
		" com_cobro_credito cc, /*88*/ "+
		" comcat_epo e, /*258*/ "+
		" comcat_if i, /*125*/ "+
		" comcat_if i2, /*125*/ "+
		" comcat_estatus_solic es, /*11*/ "+
		" comcat_moneda m, "+
		" comrel_producto_epo prodepo, " +
		" comcat_banco_fondeo bf "+
		" WHERE s.ic_documento = ds.ic_documento "+
		" AND ds.ic_documento = d.ic_documento "+
		" AND ds.ic_if = i.ic_if "+
		" AND ds.ic_epo = e.ic_epo "+
		" AND e.ic_epo = prodepo.ic_epo " +
		" AND prodepo.ic_producto_nafin = 1  " +
		//" AND (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) " +
		" AND d.ic_pyme = p.ic_pyme "+
		" AND d.ic_moneda = m.ic_moneda "+
		" AND s.ic_estatus_solic = es.ic_estatus_solic "+
		" AND d.ic_epo = pe.ic_epo "+
		" AND d.ic_pyme = pe.ic_pyme "+
		" AND s.ic_banco_fondeo = bf.ic_banco_fondeo " +
		" AND s.cs_tipo_solicitud = 'C' "+
		" AND ds.ic_documento = cc.ic_documento (+) "+
		" AND cc.ic_pedido = ps.ic_pedido (+) "+
		" AND d.ic_beneficiario = i2.ic_if (+) "+
		" AND ps.ic_pedido = a.ic_pedido (+) "+
		//	" AND s.ic_folio = procaut.ic_folio (+) "+
		" AND ds.ic_if=ds.ic_if " +
		(!ic_banco_fondeo.equals("")?" and s.ic_banco_fondeo="+ic_banco_fondeo+" ":"") + 
		condicion.toString());
		
		//query.append(criterioOrdenamiento);		
		//System.out.println(query);
			System.out.println("getDocumentQueryFile "+query.toString());
		return query.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
  // Genera el query que extrae solo un segmento de la informacion
	StringBuffer query = new StringBuffer("");
	
	//query.append(" select /*+ ordered index(ds IN_COM_DOCTO_SELEC_04_NUK) use_nl(ds,d,pe,s,p,ps,a,cc,e,i,i2,es,m) */ 'Interna' as tipo "+
		query.append(" select /*+ index (s in_com_solicitud_13_nuk)  */ 'Interna' as tipo "+
		" , (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(ds.cs_opera_fiso,'N',i.cg_razon_social,'S',d.ic_if))) as nombreIF "+
		" , (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(ds.cs_opera_fiso,'N','N/A','S',ds.ic_if))) as nombreFideicomiso"+
		" , (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO"+
		" , (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+
		" , substr(s.ic_folio,1,10)||'-'||substr(s.ic_folio,11,1)as ic_folio"+
		" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as df_fecha_solicitud"+
		" , d.ic_moneda, m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto, ds.in_importe_recibir"+
		" , ds.in_tasa_aceptada, ds.in_importe_interes, es.cd_descripcion"+
		" , es.ic_estatus_solic, a.ig_numero_prestamo "+
		" , s.ig_numero_prestamo as NUMERO_PRESTAMO"+
		" , TO_CHAR(s.df_operacion,'DD/MM/YYYY') as df_operacion"+
		//" , decode (s.ic_estatus_solic, 3, decode ( procAut.procesada, 1, 'Automatica', Null, 'Manual'), '') as proceso"+
		",DECODE (s.ic_estatus_solic,3 "+
		//" nvl2(procaut.ic_folio,'Automatica','Manual'), "+
		" , DECODE (s.cg_tipo_busqueda,'A','Autom�tica','M','Manual'), '') AS proceso "+
		" , TO_CHAR(s.df_v_descuento, 'DD/MM/YYYY') as FECHA_VENCIMIENTO" +
		" , p.in_numero_sirac "+
		" ,	s.fg_porc_comision_fondeo as fg_porc_comision_fondeo " + 
		" , ds.in_importe_recibir as in_importe_recibir " + 
		" , decode(d.cs_dscto_especial, 'D', i2.cg_razon_social, 'I', i2.cg_razon_social, '') as beneficiario"+
		" , decode(d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, 0) as fn_porc_beneficiario"+
		" , decode(d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, 0) as RECIBIR_BENEF " +
		" , decode(d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 0) as NETO_REC_PYME "+
		" , d.cs_dscto_especial , s.fg_tasa_fondeo_nafin as TASA_FONDEO, bf.cd_descripcion NOM_BANCO_FONDEO ");
		
		query.append( ", D.ic_epo, TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO ");
		query.append( ",decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as tipoTasa "); 
		query.append( ", 'ConsSolicNafinDE.java' ORIGENQUERY ");
		/*
		" from com_solicitud s, comcat_estatus_solic es"+
		" , com_docto_seleccionado ds, com_documento d"+
		" , comcat_moneda m, comcat_epo e, comcat_pyme p, comrel_pyme_epo pe"+
		" , comcat_if i, com_cobro_credito cc, com_pedido_seleccionado ps, com_anticipo a, comcat_if i2"+
		", (select ic_folio, count(*) as procesada from com_control04 where cs_estatus = 'S' group by ic_folio) procAut"+
		" where"+
		" s.ic_documento=ds.ic_documento"+
		" and ds.ic_documento=d.ic_documento"+
		" and ds.ic_if=i.ic_if"+
		" and ds.ic_epo=e.ic_epo"+
		" and d.ic_pyme=p.ic_pyme"+
		" and d.ic_moneda=m.ic_moneda"+
		" and s.ic_estatus_solic=es.ic_estatus_solic"+
		" and d.ic_epo = pe.ic_epo and d.ic_pyme = pe.ic_pyme"+
		" and s.cs_tipo_solicitud='C'"+
		" and ds.ic_documento = cc.ic_documento(+) "+
		" and cc.ic_pedido = ps.ic_pedido(+)"+
			" and d.ic_beneficiario = i2.ic_if (+) "+
		" and ps.ic_pedido = a.ic_pedido(+)"+
		" and s.ic_folio = procAut.ic_folio (+) "+
		*/
		
		query.append(" FROM   "+
		" com_docto_seleccionado  ds,/*300,558*/ "+
		" com_documento  d, /*506,570*/ "+
		" comrel_pyme_epo pe,/*369,557*/ "+
		" com_solicitud s, /*298,664*/ "+
		" comcat_pyme p, /*91,777*/ "+
	//	" (SELECT IC_FOLIO FROM com_control04 /*259,849*/ "+
	//	"  WHERE cs_estatus = 'S') procaut, "+
		" com_pedido_seleccionado ps, /*89*/ "+
		" com_anticipo a, /*86*/"+
		" com_cobro_credito cc, /*88*/ "+
		" comcat_epo e, /*258*/ "+
		" comcat_if i, /*125*/ "+
		" comcat_if i2, /*125*/ "+
		" comcat_estatus_solic es, /*11*/ "+
		" comcat_moneda m, "+
		" comcat_banco_fondeo bf"+
		" WHERE s.ic_documento = ds.ic_documento "+
		" AND ds.ic_documento = d.ic_documento "+
		" AND ds.ic_if = i.ic_if "+
		" AND ds.ic_epo = e.ic_epo "+
		" AND d.ic_pyme = p.ic_pyme "+
		" AND d.ic_moneda = m.ic_moneda "+
		" AND s.ic_estatus_solic = es.ic_estatus_solic "+
		" AND d.ic_epo = pe.ic_epo "+
		" AND d.ic_pyme = pe.ic_pyme "+
		" AND s.ic_banco_fondeo = bf.ic_banco_fondeo " +
	//	" AND s.cs_tipo_solicitud = 'C' "+
		" AND ds.ic_documento = cc.ic_documento (+) "+
		" AND cc.ic_pedido = ps.ic_pedido (+) "+
		" AND d.ic_beneficiario = i2.ic_if (+) "+
		" AND ps.ic_pedido = a.ic_pedido (+) "+
	//	" AND s.ic_folio = procaut.ic_folio (+) "+
		" and ds.ic_if=ds.ic_if "+
			" AND D.ic_documento in (");
		for (int i = 1; i <= ids.size(); i++) {
		  query.append("?,");
		}
		query = query.delete(query.length()-1,query.length());
		query.append(")");
		System.out.println(query.toString());
		System.out.println("getDocumentSummaryQueryForIds "+query.toString());
	return query.toString();
  }
  
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
	
		String nombreArchivo=null;
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		int registros=0;
		String metodo="", cs_tipo_solicitud = "C",  tipoSolicitud ="",  nombreIf ="", nombreEpo ="", nombrePyme ="", 
			folio ="", fechaSolicitud ="", numeroPrestamo ="", fechaOperacion ="", tasaFondeo ="", monedaNombre ="", 
			monedaClave ="",  dblMonto =  "", strPorcentaje = "", strRecurso = "", dblTotalMontoPesos = "", dblTotalMontoDolares = "", 
			montoDescuento ="",  montoOperar ="",  tasaAceptada ="",  importeInteres ="",  estatusSolicitud ="", claveEstatusSolic ="", 
			noPrestamo = "", fechaVencimiento ="", numeroSirac ="",  fg_porc_comision_fondeo = "", ccAcuse = "", montoComision = "",
			montoTotalComisionMN = "", montoTotalComisionDL = "", beneficiario = "", porciento_beneficiario = "", importe_a_recibir_beneficiario = "", 
			neto_a_recibir_pyme = "", nombreBancoDeFondeo = "", destipoTasa ="", FECHA_ENTREGA = "", TIPO_COMPRA = "", CLAVE_PRESUPUESTARIA = "", PERIODO = "";
		boolean SIN_COMAS = false;
		
	
		try{
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),	"1",
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
				
			pdfDoc.setLTable(13,100);
			pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Origen","celda01",ComunesPDF.CENTER);						
			pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("PYME","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Num. solicitud","celda01",ComunesPDF.CENTER);			
			pdfDoc.setLCell("Monto Documento","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Recurso en garant�a","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Monto Descuento","celda01",ComunesPDF.CENTER);			
			pdfDoc.setLCell("Nombre IF","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Monto a Operar IF","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tasa PYME IF","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Monto est. int. IF","celda01",ComunesPDF.CENTER);
			
			pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Nombre FIDEICOMISO","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Monto a Operar FIDEICOMISO","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tasa PYME FIDEICOMISO","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Monto est. int. FIDEICOMISO","celda01",ComunesPDF.CENTER);			
			pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);					
			pdfDoc.setLCell("Numero de Prestamo","celda01",ComunesPDF.CENTER);								
			pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Fecha Valor","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tasa de Fondeo","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Numero cliente SIRAC","celda01",ComunesPDF.CENTER);
			
			pdfDoc.setLCell("C","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Numero de Credito Aplicado","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("M�todo de Operaci�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Neto a recibir PYME","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("% Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Importe a recibir beneficiario","celda01",ComunesPDF.CENTER);
			if(esBANCOMEXT.equals("N"))  {
				pdfDoc.setLCell("Banco de fondeo","celda01",ComunesPDF.CENTER);
			}
			pdfDoc.setLCell("Fecha de recepci�n de bienes y servicios","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tipo de Compra(procedimiento)","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Clasificador por Objeto del Gasto","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Plazo M�ximo","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Referencia Tasa","celda01",ComunesPDF.CENTER);
			if(esBANCOMEXT.equals("S"))  {
				pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
			}
	
			while (rs.next()){			
				tipoSolicitud = (rs.getString("TIPO") == null) ? "" : rs.getString("TIPO");
			  nombreIf = (rs.getString("NOMBREIF") == null) ? "" : rs.getString("NOMBREIF");
			  String nombreFideicomiso = (rs.getString("NOMBREFIDEICOMISO") == null) ? "" : rs.getString("NOMBREFIDEICOMISO"); 
			  nombreEpo = (rs.getString("NOMBREEPO") == null) ? "" : rs.getString("NOMBREEPO");
			  nombrePyme = (rs.getString("NOMBREPYME") == null) ? "" : rs.getString("NOMBREPYME");
			  folio = (rs.getString("IC_FOLIO") == null) ? "" : rs.getString("IC_FOLIO");
			  fechaSolicitud = (rs.getString("DF_FECHA_SOLICITUD") == null) ? "" : rs.getString("DF_FECHA_SOLICITUD");
			  monedaNombre = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
			  monedaClave = rs.getString("ic_moneda");
			  dblMonto = rs.getString("FN_MONTO");
			  strPorcentaje = (rs.getString("FN_PORC_ANTICIPO") == null) ? "0.00" : rs.getString("FN_PORC_ANTICIPO");			 
			  montoDescuento = (rs.getString("FN_MONTO_DSCTO") == null) ? "0.00" : rs.getString("FN_MONTO_DSCTO");
			  
			  String montoOperarF = (rs.getString("IN_IMPORTE_RECIBIR") == null) ? "0.00" : rs.getString("IN_IMPORTE_RECIBIR");
			  String tasaAceptadaF = (rs.getString("IN_TASA_ACEPTADA") == null) ? "0.00" : rs.getString("IN_TASA_ACEPTADA");
			  String importeInteresF = (rs.getString("IN_IMPORTE_INTERES") == null) ? "0.00" : rs.getString("IN_IMPORTE_INTERES");
			  
			  montoOperar = (rs.getString("IN_IMPORTE_RECIBIR_IF") == null) ? "0.00" : rs.getString("IN_IMPORTE_RECIBIR_IF");
			  tasaAceptada = (rs.getString("IN_TASA_ACEPTADA_IF") == null) ? "0.00" : rs.getString("IN_TASA_ACEPTADA_IF");
			  importeInteres = (rs.getString("IN_IMPORTE_INTERES_IF") == null) ? "0.00" : rs.getString("IN_IMPORTE_INTERES_IF");
			  
			
			  estatusSolicitud = (rs.getString("CD_DESCRIPCION") == null) ? "" : rs.getString("CD_DESCRIPCION");
			  claveEstatusSolic = rs.getString("IC_ESTATUS_SOLIC");
			  metodo = (rs.getString("PROCESO") == null) ? "" : rs.getString("PROCESO");
			  noPrestamo = (rs.getString("IG_NUMERO_PRESTAMO") == null) ? "" : (rs.getString("IG_NUMERO_PRESTAMO").equals("0"))?"":rs.getString("IG_NUMERO_PRESTAMO") ;
				numeroPrestamo = (rs.getString("NUMERO_PRESTAMO") == null) ? "" : rs.getString("NUMERO_PRESTAMO");
				fechaOperacion = (rs.getString("DF_OPERACION") == null) ? "" : rs.getString("DF_OPERACION");
				tasaFondeo = (rs.getString("TASA_FONDEO") == null) ? "" : Comunes.formatoDecimal(rs.getString("TASA_FONDEO"),4);
				fechaVencimiento = rs.getString("FECHA_VENCIMIENTO");
				numeroSirac = rs.getString("IN_NUMERO_SIRAC");
				//fg_porc_comision_fondeo = (rs.getString("fg_porc_comision_fondeo") == null) ? "" : rs.getString("fg_porc_comision_fondeo");
				//montoComision = rs.getDouble("in_importe_recibir");
				beneficiario = rs.getString("beneficiario")==null?"":rs.getString("beneficiario");
				porciento_beneficiario = rs.getString("fn_porc_beneficiario");
				importe_a_recibir_beneficiario = rs.getString("RECIBIR_BENEF");
				neto_a_recibir_pyme = rs.getString("NETO_REC_PYME");			
				FECHA_ENTREGA 					= (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
				TIPO_COMPRA 						= (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
				CLAVE_PRESUPUESTARIA 		= (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
				PERIODO 								= (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");
				destipoTasa 								= (rs.getString("tipoTasa")==null)?"":rs.getString("tipoTasa");
				nombreBancoDeFondeo = (rs.getString("NOM_BANCO_FONDEO")==null)?"":rs.getString("NOM_BANCO_FONDEO");
			
				pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(tipoSolicitud,"formas",ComunesPDF.CENTER);				
				pdfDoc.setLCell(nombreEpo,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(nombrePyme,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(folio,"formas",ComunesPDF.CENTER);								
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(dblMonto,2),"formas",ComunesPDF.RIGHT);				
				pdfDoc.setLCell(strPorcentaje+"%","formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(strPorcentaje,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescuento,2),"formas",ComunesPDF.RIGHT);							
				pdfDoc.setLCell(nombreIf,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoOperar,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setLCell(tasaAceptada,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(importeInteres,2),"formas",ComunesPDF.RIGHT);				
				
				pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(nombreFideicomiso,"formas",ComunesPDF.LEFT);
				if(montoOperarF.equals("N/A")){
					pdfDoc.setLCell(montoOperarF,"formas",ComunesPDF.CENTER);
				}else{
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoOperarF,2),"formas",ComunesPDF.RIGHT);
				}
				pdfDoc.setLCell(tasaAceptadaF,"formas",ComunesPDF.CENTER);
				if(importeInteresF.equals("N/A")){
					pdfDoc.setLCell(importeInteresF,"formas",ComunesPDF.CENTER);
				}else{
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(importeInteresF,2),"formas",ComunesPDF.RIGHT);						
				}
				pdfDoc.setLCell(estatusSolicitud,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(numeroPrestamo,"formas",ComunesPDF.CENTER);									
				pdfDoc.setLCell(monedaNombre,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaSolicitud,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaOperacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(tasaFondeo,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(numeroSirac,"formas",ComunesPDF.CENTER);
				
				pdfDoc.setLCell("C","formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(numeroPrestamo,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(metodo,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(neto_a_recibir_pyme,2) ,"formas",ComunesPDF.RIGHT);
				pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(porciento_beneficiario+"%","formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2) ,"formas",ComunesPDF.RIGHT);
				if(esBANCOMEXT.equals("N"))  {
					pdfDoc.setLCell(nombreBancoDeFondeo,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell(FECHA_ENTREGA,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(TIPO_COMPRA,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(CLAVE_PRESUPUESTARIA,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(PERIODO,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(destipoTasa,"formas",ComunesPDF.CENTER);
				if(esBANCOMEXT.equals("S"))  {
					pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
				}
		       if (cs_tipo_solicitud.equals("E"))
				    strRecurso = "0";
				else
					strRecurso = new Double(new Double(dblMonto).doubleValue() - new Double(montoDescuento).doubleValue()).toString();
			}
		pdfDoc.addLTable();
		pdfDoc.endDocument();
	
		
	}catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
	
 }

/**
 * Genera el archivo CSV o PDF seg�n el tipo que se le indique.
 * En el caso del PDF, el archivo generado no comtempla paginaci�n, imprime todos los registros que trae la consulta.
 * @param request
 * @param rs
 * @param path
 * @param tipo: CSV o PDF
 * @return nombre del archivo
 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		StringBuffer contenidoArchivo = new StringBuffer();

		if(tipo.equals("CSV")){

			String cs_tipo_solicitud  = "C";
			String strRecurso         = "";
		   String Monto              = "";
			boolean SIN_COMAS         = false;
			int total                 = 0;
			int registros             = 0;
			OutputStreamWriter writer = null;
			BufferedWriter buffer     = null;

			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);

				contenidoArchivo.append("Origen,EPO,PYME,Num. Solicitud,Fecha Valor,Moneda,Monto Documento");
				contenidoArchivo.append(", Porcentaje de Descuento,Recurso en Garant�a, Monto Descuento,Nombre If");
				contenidoArchivo.append(", Monto a Operar If, Tasa PYME If,Monto Est. Int. If,Nombre Fideicomiso");
				contenidoArchivo.append(", Monto a Operar Fideicomiso, Tasa PYME Fideicomiso,Monto Est. Int. Fideicomiso");
				contenidoArchivo.append(", Estatus,N�mero Prestamo,Fecha Operaci�n,Tasa de Fondeo,Fecha Vencimiento");
				contenidoArchivo.append(",N�mero Cliente SIRAC,No. de Cr�dito Aplicado,M�todo de Operaci�n");
				contenidoArchivo.append(",Neto a Recibir Pyme,Beneficiario,% Beneficiario,Importe a Recibir Beneficiario");
				if(esBANCOMEXT.equals("N")){
					contenidoArchivo.append(",Banco de Fondeo");
				}
				contenidoArchivo.append(",Fecha de Recepci�n de Bienes y Servicios, Tipo Compra (procedimiento), Clasificador por Objeto del Gasto, Plazo M�ximo");
				contenidoArchivo.append(", Referencia Tasa");
				contenidoArchivo.append("\n");

				while (rs.next()){

					String tipoSolicitud       = (rs.getString("tipo")                    == null) ? ""     : rs.getString("tipo");
					String nombreIf            = (rs.getString("nombreIF")                == null) ? ""     : rs.getString("nombreIF");
					String nombreFideicomiso   = (rs.getString("nombreFideicomiso")       == null) ? ""     : rs.getString("nombreFideicomiso");
					String nombreEpo           = (rs.getString("nombreEPO")               == null) ? ""     : rs.getString("nombreEPO");
					String nombrePyme          = (rs.getString("nombrePYME")              == null) ? ""     : rs.getString("nombrePYME");
					String folio               = (rs.getString("ic_folio")                == null) ? ""     : rs.getString("ic_folio");
					String fechaSolicitud      = (rs.getString("df_fecha_solicitud")      == null) ? ""     : rs.getString("df_fecha_solicitud");
					String monedaNombre        = (rs.getString("cd_nombre")               == null) ? ""     : rs.getString("cd_nombre");
					String strPorcentaje       = (rs.getString("fn_porc_anticipo")        == null) ? "0.00" : rs.getString("fn_porc_anticipo");
					String montoDescuento      = (rs.getString("fn_monto_dscto")          == null) ? "0.00" : rs.getString("fn_monto_dscto");
					String montoOperarF        = (rs.getString("in_importe_recibir")      == null) ? "0.00" : rs.getString("in_importe_recibir");
					String tasaAceptadaF       = (rs.getString("in_tasa_aceptada")        == null) ? "0.00" : rs.getString("in_tasa_aceptada");
					String importeInteresF     = (rs.getString("in_importe_interes")      == null) ? "0.00" : rs.getString("in_importe_interes");
					String montoOperar         = (rs.getString("in_importe_recibir_if")   == null) ? "0.00" : rs.getString("in_importe_recibir_if");
					String tasaAceptada        = (rs.getString("in_tasa_aceptada_if")     == null) ? "0.00" : rs.getString("in_tasa_aceptada_if");
					String importeInteres      = (rs.getString("in_importe_interes_if")   == null) ? "0.00" : rs.getString("in_importe_interes_if");
					String estatusSolicitud    = (rs.getString("cd_descripcion")          == null) ? ""     : rs.getString("cd_descripcion");
					String numeroPrestamo      = (rs.getString("NUMERO_PRESTAMO")         == null) ? ""     : rs.getString("NUMERO_PRESTAMO");
					String fechaOperacion      = (rs.getString("df_operacion")            == null) ? ""     : rs.getString("df_operacion");
					String tasaFondeo          = (rs.getString("TASA_FONDEO")             == null) ? ""     : Comunes.formatoDecimal(rs.getString("TASA_FONDEO"),4);
					String fechaVencimiento    = (rs.getString("FECHA_VENCIMIENTO")       == null) ? ""     : rs.getString("FECHA_VENCIMIENTO");
					String numeroSirac         = (rs.getString("in_numero_sirac")         == null) ? ""     : rs.getString("in_numero_sirac");
					String noPrestamo          = (rs.getString("ig_numero_prestamo")      == null) ? ""     : (rs.getString("ig_numero_prestamo").equals("0"))?"":rs.getString("IG_NUMERO_PRESTAMO");
					String metodo              = (rs.getString("proceso")                 == null) ? ""     : rs.getString("proceso");
					String beneficiario        = (rs.getString("beneficiario")            == null) ? ""     : rs.getString("beneficiario");
					String nombreBancoDeFondeo = (rs.getString("NOM_BANCO_FONDEO")        == null) ? ""     : rs.getString("NOM_BANCO_FONDEO");
					String FECHA_ENTREGA       = (rs.getString("DF_ENTREGA")              == null) ? ""     : rs.getString("DF_ENTREGA");
					String TIPO_COMPRA         = (rs.getString("CG_TIPO_COMPRA")          == null) ? ""     : rs.getString("CG_TIPO_COMPRA");
					String CLAVE_PRESUPUESTARIA= (rs.getString("CG_CLAVE_PRESUPUESTARIA") == null) ? ""     : rs.getString("CG_CLAVE_PRESUPUESTARIA");
					String PERIODO             = (rs.getString("CG_PERIODO")              == null) ? ""     : rs.getString("CG_PERIODO");
					String destipoTasa         = (rs.getString("tipoTasa")                == null) ? ""     : rs.getString("tipoTasa");

					int monedaClave                       = rs.getInt("ic_moneda");
					double dblMonto                       = rs.getDouble("fn_monto");
					int claveEstatusSolic                 = rs.getInt("ic_estatus_solic");
					double importe_a_recibir_beneficiario = rs.getDouble("RECIBIR_BENEF");
					double porciento_beneficiario         = rs.getDouble("fn_porc_beneficiario");
					double neto_a_recibir_pyme            = rs.getDouble("NETO_REC_PYME");

					if (cs_tipo_solicitud.equals("E")){
						strRecurso = "0";
					} else{
						strRecurso = new Double(new Double(dblMonto).doubleValue() - new Double(montoDescuento).doubleValue()).toString();
					}

					if(montoOperarF.equals("N/A")){
						Monto = montoOperarF;
					}else{
						Monto = Comunes.formatoDecimal(montoOperarF,2,SIN_COMAS);
					}

					String Importe = "";
					if(importeInteresF.equals("N/A")){
					Importe =  importeInteresF;
					} else{
						Importe = Comunes.formatoDecimal(importeInteresF,2,SIN_COMAS); 
					}

					String Tasa = "";
					if(tasaAceptadaF.equals("N/A")){
						Tasa =  tasaAceptadaF;
					} else{
						Tasa = Comunes.formatoDecimal(tasaAceptadaF,2,SIN_COMAS);
					}


					contenidoArchivo.append(tipoSolicitud+
					","+nombreEpo.replace(',',' ')+","+nombrePyme.replace(',',' ')+","+folio+
					","+fechaSolicitud+","+monedaNombre+
					","+Comunes.formatoDecimal(dblMonto,2,SIN_COMAS)+","+strPorcentaje+
					","+strRecurso+
					","+Comunes.formatoDecimal(montoDescuento,2,SIN_COMAS)+

					","+nombreIf.replace(',',' ')+
					","+Comunes.formatoDecimal(montoOperar,2,SIN_COMAS)+
					//","+ Monto +
					","+Comunes.formatoDecimal(tasaAceptada,5,SIN_COMAS)+
					","+Comunes.formatoDecimal(importeInteres,2,SIN_COMAS)+

					","+nombreFideicomiso.replace(',',' ')+

					//","+Comunes.formatoDecimal(montoOperarF,2,SIN_COMAS)+
					","+ Monto +
					//","+Comunes.formatoDecimal(tasaAceptadaF,5,SIN_COMAS)+
					","+ Tasa +
					//","+Comunes.formatoDecimal(importeInteresF,2,SIN_COMAS)+
					"," + Importe +         

					","+estatusSolicitud+
					","+numeroPrestamo+
					","+fechaOperacion+
					","+tasaFondeo+
					","+fechaVencimiento+
					","+numeroSirac+
					","+noPrestamo+
					","+metodo);

					if(0 != neto_a_recibir_pyme) contenidoArchivo.append(","+neto_a_recibir_pyme); else contenidoArchivo.append(", ");
					if(!"".equals(beneficiario)) contenidoArchivo.append(","+beneficiario.replace(',',' ')); else contenidoArchivo.append(", ");
					if(0 != porciento_beneficiario) contenidoArchivo.append(","+porciento_beneficiario); else contenidoArchivo.append(", ");
					if(0 != importe_a_recibir_beneficiario) contenidoArchivo.append(","+importe_a_recibir_beneficiario); else contenidoArchivo.append(", ");
					if(esBANCOMEXT.equals("N"))  {
						contenidoArchivo.append(","+nombreBancoDeFondeo);
					}

					contenidoArchivo.append("," + FECHA_ENTREGA + "," + TIPO_COMPRA + "," + CLAVE_PRESUPUESTARIA + "," + PERIODO);
					if( claveEstatusSolic==1 || claveEstatusSolic==3 || claveEstatusSolic==5 ){
						contenidoArchivo.append("," +destipoTasa);
					} else{ 
						contenidoArchivo.append("  , N/A ");
					} 
					contenidoArchivo.append("\n");

					registros++;
					total++;
					if(total==1000){
						total=0;
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio
					}
				}
				buffer.write(contenidoArchivo.toString());
				buffer.close();
				contenidoArchivo = new StringBuffer();//Limpio

				//creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				//nombreArchivo = creaArchivo.getNombre();
			} catch(Exception e) {
				throw new AppException("Error al generar el archivo csv ", e);
			}

		} else if(tipo.equals("PDF")){

			try{

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				int cont = 0;

				String cs_tipo_solicitud = "";
				String strRecurso = "";

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),  "1",
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(13,100);
				pdfDoc.setLCell("A",                                        "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Origen",                                   "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO",                                      "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("PYME",                                     "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Num. solicitud",                           "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Documento",                          "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Porcentaje de Descuento",                  "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Recurso en garant�a",                      "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Descuento",                          "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre IF",                                "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Operar IF",                        "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa PYME IF",                             "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto est. int. IF",                       "celda01",ComunesPDF.CENTER);

				pdfDoc.setLCell("B",                                        "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre FIDEICOMISO",                       "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Operar FIDEICOMISO",               "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa PYME FIDEICOMISO",                    "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto est. int. FIDEICOMISO",              "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus",                                  "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Numero de Prestamo",                       "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda",                                   "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Valor",                              "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Operaci�n",                       "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa de Fondeo",                           "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento",                     "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Numero cliente SIRAC",                     "celda01",ComunesPDF.CENTER);

				pdfDoc.setLCell("C",                                        "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Numero de Credito Aplicado",               "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("M�todo de Operaci�n",                      "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Neto a recibir PYME",                      "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Beneficiario",                             "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("% Beneficiario",                           "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Importe a recibir beneficiario",           "celda01",ComunesPDF.CENTER);
				if(esBANCOMEXT.equals("N")){
					pdfDoc.setLCell("Banco de fondeo",                       "celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell("Fecha de recepci�n de bienes y servicios", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Compra(procedimiento)",            "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Clasificador por Objeto del Gasto",        "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo M�ximo",                             "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Referencia Tasa",                          "celda01",ComunesPDF.CENTER);
				if(esBANCOMEXT.equals("S")){
					pdfDoc.setLCell(" ",                                     "celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setLHeaders();
				countReg = 0;
				while (rs.next()){
					String tipoSolicitud                  = (rs.getString("TIPO")                    == null) ? ""     : rs.getString("TIPO");
					String nombreIf                       = (rs.getString("NOMBREIF")                == null) ? ""     : rs.getString("NOMBREIF");
					String nombreFideicomiso              = (rs.getString("NOMBREFIDEICOMISO")       == null) ? ""     : rs.getString("NOMBREFIDEICOMISO");
					String nombreEpo                      = (rs.getString("NOMBREEPO")               == null) ? ""     : rs.getString("NOMBREEPO");
					String nombrePyme                     = (rs.getString("NOMBREPYME")              == null) ? ""     : rs.getString("NOMBREPYME");
					String folio                          = (rs.getString("IC_FOLIO")                == null) ? ""     : rs.getString("IC_FOLIO");
					String fechaSolicitud                 = (rs.getString("DF_FECHA_SOLICITUD")      == null) ? ""     : rs.getString("DF_FECHA_SOLICITUD");
					String monedaNombre                   = (rs.getString("CD_NOMBRE")               == null) ? ""     : rs.getString("CD_NOMBRE");
					String monedaClave                    = rs.getString("ic_moneda");
					String dblMonto                       = rs.getString("FN_MONTO");
					String strPorcentaje                  = (rs.getString("FN_PORC_ANTICIPO")        == null) ? "0.00" : rs.getString("FN_PORC_ANTICIPO");
					String montoDescuento                 = (rs.getString("FN_MONTO_DSCTO")          == null) ? "0.00" : rs.getString("FN_MONTO_DSCTO");

					String montoOperarF                   = (rs.getString("IN_IMPORTE_RECIBIR")      == null) ? "0.00" : rs.getString("IN_IMPORTE_RECIBIR");
					String tasaAceptadaF                  = (rs.getString("IN_TASA_ACEPTADA")        == null) ? "0.00" : rs.getString("IN_TASA_ACEPTADA");
					String importeInteresF                = (rs.getString("IN_IMPORTE_INTERES")      == null) ? "0.00" : rs.getString("IN_IMPORTE_INTERES");

					String montoOperar                    = (rs.getString("IN_IMPORTE_RECIBIR_IF")   == null) ? "0.00" : rs.getString("IN_IMPORTE_RECIBIR_IF");
					String tasaAceptada                   = (rs.getString("IN_TASA_ACEPTADA_IF")     == null) ? "0.00" : rs.getString("IN_TASA_ACEPTADA_IF");
					String importeInteres                 = (rs.getString("IN_IMPORTE_INTERES_IF")   == null) ? "0.00" : rs.getString("IN_IMPORTE_INTERES_IF");

					String estatusSolicitud               = (rs.getString("CD_DESCRIPCION")          == null) ? ""     : rs.getString("CD_DESCRIPCION");
					String metodo                         = (rs.getString("PROCESO")                 == null) ? ""     : rs.getString("PROCESO");
					String noPrestamo                     = (rs.getString("IG_NUMERO_PRESTAMO")      == null) ? ""     : (rs.getString("IG_NUMERO_PRESTAMO").equals("0"))?"":rs.getString("IG_NUMERO_PRESTAMO") ;
					String numeroPrestamo                 = (rs.getString("NUMERO_PRESTAMO")         == null) ? ""     : rs.getString("NUMERO_PRESTAMO");
					String fechaOperacion                 = (rs.getString("DF_OPERACION")            == null) ? ""     : rs.getString("DF_OPERACION");
					String tasaFondeo                     = (rs.getString("TASA_FONDEO")             == null) ? ""     : Comunes.formatoDecimal(rs.getString("TASA_FONDEO"),4);

					String beneficiario                   = rs.getString("beneficiario")             ==null?"":rs.getString("beneficiario");
					String FECHA_ENTREGA                  = (rs.getString("DF_ENTREGA")              ==null) ? ""      : rs.getString("DF_ENTREGA");
					String TIPO_COMPRA                    = (rs.getString("CG_TIPO_COMPRA")          ==null) ? ""      : rs.getString("CG_TIPO_COMPRA");
					String CLAVE_PRESUPUESTARIA           = (rs.getString("CG_CLAVE_PRESUPUESTARIA") ==null) ? ""      : rs.getString("CG_CLAVE_PRESUPUESTARIA");
					String PERIODO                        = (rs.getString("CG_PERIODO")              ==null) ? ""      : rs.getString("CG_PERIODO");
					String destipoTasa                    = (rs.getString("tipoTasa")                ==null) ? ""      : rs.getString("tipoTasa");
					String nombreBancoDeFondeo            = (rs.getString("NOM_BANCO_FONDEO")        ==null) ? ""      : rs.getString("NOM_BANCO_FONDEO");

					String fechaVencimiento               = rs.getString("FECHA_VENCIMIENTO");
					String numeroSirac                    = rs.getString("IN_NUMERO_SIRAC");
					String porciento_beneficiario         = rs.getString("fn_porc_beneficiario");
					String importe_a_recibir_beneficiario = rs.getString("RECIBIR_BENEF");
					String neto_a_recibir_pyme            = rs.getString("NETO_REC_PYME");
					String claveEstatusSolic              = rs.getString("IC_ESTATUS_SOLIC");

					if (cs_tipo_solicitud.equals("E"))
						strRecurso = "0";
					else
						strRecurso = new Double(new Double(dblMonto).doubleValue() - new Double(montoDescuento).doubleValue()).toString();

					pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoSolicitud,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreEpo,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombrePyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(folio,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(dblMonto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(strPorcentaje+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(strRecurso,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescuento,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(nombreIf,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoOperar,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(tasaAceptada,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(importeInteres,2),"formas",ComunesPDF.RIGHT);

					pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreFideicomiso,"formas",ComunesPDF.LEFT);
					if(montoOperarF.equals("N/A")){
						pdfDoc.setLCell(montoOperarF,"formas",ComunesPDF.CENTER);
					}else{
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoOperarF,2),"formas",ComunesPDF.RIGHT);
					}
					pdfDoc.setLCell(tasaAceptadaF,"formas",ComunesPDF.CENTER);
					if(importeInteresF.equals("N/A")){
						pdfDoc.setLCell(importeInteresF,"formas",ComunesPDF.CENTER);
					}else{
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(importeInteresF,2),"formas",ComunesPDF.RIGHT);
					}
					pdfDoc.setLCell(estatusSolicitud,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numeroPrestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(monedaNombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaSolicitud,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaOperacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tasaFondeo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numeroSirac,"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell("C","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numeroPrestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(metodo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(neto_a_recibir_pyme,2) ,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(porciento_beneficiario+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(importe_a_recibir_beneficiario,2) ,"formas",ComunesPDF.RIGHT);
					if(esBANCOMEXT.equals("N")){
						pdfDoc.setLCell(nombreBancoDeFondeo,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setLCell(FECHA_ENTREGA,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(TIPO_COMPRA,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(CLAVE_PRESUPUESTARIA,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(PERIODO,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(destipoTasa,"formas",ComunesPDF.CENTER);
					if(esBANCOMEXT.equals("S")){
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					}

					countReg++;
					//System.out.println("Registros procesados: " + countReg);
				}
			pdfDoc.addLTable();
			pdfDoc.endDocument();

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo ", e);
			}

		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}



	public String getDocumentQueryFile() {

		log.info("getDocumentQuery (E)");

	// Genera el query que devuelve las llaves primarias de la consulta
		StringBuffer query = new StringBuffer("");
		conditions = new ArrayList();
	
		String criterioOrdenamiento=" ORDER BY ds.IC_DOCUMENTO ";
		
		String  consultaDoctos = "";
//		if(banderaTablaDoctos.equals("1")){
			consultaDoctos = ", e.ic_epo, TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO ";
//		}

		query.append(" select /*+ index (s in_com_solicitud_13_nuk)  */ 'Interna' as tipo "+
		" , (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(ds.cs_opera_fiso,'N',i.cg_razon_social,'S',i3.cg_razon_social))) as nombreIF "+
		" , (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(ds.cs_opera_fiso,'N','N/A','S',i4.cg_razon_social))) as nombreFideicomiso "+
		" , (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO"+
		" , (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+
		" , substr(s.ic_folio,1,10)||'-'||substr(s.ic_folio,11,1)as ic_folio"+
		" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as df_fecha_solicitud"+
		" , d.ic_moneda, m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto "+
		" , (decode (ds.cs_opera_fiso,'N','N/A','S',ds.in_importe_recibir)) as in_importe_recibir "+
		" , (decode (ds.cs_opera_fiso,'S',ds.in_importe_recibir_fondeo,ds.in_importe_recibir)) as in_importe_recibir_if "+
		" , (decode (ds.cs_opera_fiso,'N','N/A','S',ds.in_tasa_aceptada)) as in_tasa_aceptada "+
		" , (decode (ds.cs_opera_fiso,'S',ds.in_tasa_aceptada_fondeo,ds.in_tasa_aceptada)) as in_tasa_aceptada_if "+
		//" , ds.in_tasa_aceptada " +
		" , (decode (ds.cs_opera_fiso,'N','N/A','S',ds.in_importe_interes)) as in_importe_interes "+
		" , (decode (ds.cs_opera_fiso,'S',ds.in_importe_interes_fondeo,ds.in_importe_interes)) as in_importe_interes_if "+
		//ds.in_importe_recibir, ds.in_tasa_aceptada, ds.in_importe_interes "+
		" ,es.cd_descripcion"+
		" , es.ic_estatus_solic, a.ig_numero_prestamo "+
		" , s.ig_numero_prestamo as NUMERO_PRESTAMO"+
		" , TO_CHAR(s.df_operacion,'DD/MM/YYYY') as df_operacion"+
		//" , decode (s.ic_estatus_solic, 3, decode ( procAut.procesada, 1, 'Automatica', Null, 'Manual'), '') as proceso"+
		" , DECODE (s.ic_estatus_solic,3 "+
		//" ,nvl2(procaut.ic_folio,'Automatica','Manual'), '') AS proceso "+
		" , DECODE (s.cg_tipo_busqueda,'A','Autom�tica','M','Manual'), '') AS proceso "+
		" , TO_CHAR(s.df_v_descuento, 'DD/MM/YYYY') as FECHA_VENCIMIENTO" +
		" , p.in_numero_sirac "+
		" ,	s.fg_porc_comision_fondeo as fg_porc_comision_fondeo " +
		" , ds.in_importe_recibir as in_importe_recibir " + 
		" , decode(d.cs_dscto_especial, 'D', i2.cg_razon_social, 'I', i2.cg_razon_social, '') as beneficiario"+
		" , decode(d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, 0) as fn_porc_beneficiario"+
		" , decode(d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, 0) as RECIBIR_BENEF " +
		" , decode(d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I',ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 0) as NETO_REC_PYME "+
		" , d.cs_dscto_especial , s.fg_tasa_fondeo_nafin as TASA_FONDEO, bf.cd_descripcion NOM_BANCO_FONDEO ");
		
		query.append(consultaDoctos);
		query.append( ",decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial' || ''|| decode ( s.CS_FLOATING, 'S',' / Floating') ,'O','Oferta de Tasas','N','Negociada')  as tipoTasa "); 
		query.append( ", s.CC_ACUSE AS IC_ACUSE");
		query.append(", 'ConsSolicNafinDE.java' ORIGENQUERY ");

		query.append(" FROM   "+
		" com_docto_seleccionado  ds,/*300,558*/ "+
		" com_documento  d, /*506,570*/ "+
		" comrel_pyme_epo pe,/*369,557*/ "+
		" com_solicitud s, /*298,664*/ "+
		" comcat_pyme p, /*91,777*/ "+
	//	" (SELECT IC_FOLIO FROM com_control04 /*259,849*/ "+
	//	"  WHERE cs_estatus = 'S') procaut, "+
		" com_pedido_seleccionado ps, /*89*/ "+
		" com_anticipo a, /*86*/"+
		" com_cobro_credito cc, /*88*/ "+
		" comcat_epo e, /*258*/ "+
		" comcat_if i, /*125*/ "+
		" comcat_if i2, /*125*/ "+
		" comcat_if i3, comcat_if i4, "+
		" comcat_estatus_solic es, /*11*/ "+
		" comcat_moneda m, "+
		" comrel_producto_epo prodepo, " +
		" comcat_banco_fondeo bf "+
		" WHERE s.ic_documento = ds.ic_documento "+
		" AND ds.ic_documento = d.ic_documento "+
		" AND ds.ic_if = i.ic_if "+
		" AND ds.ic_epo = e.ic_epo "+
		" AND e.ic_epo = prodepo.ic_epo " +
		" AND prodepo.ic_producto_nafin = 1  " +
		//" AND (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) " +
		" AND d.ic_pyme = p.ic_pyme "+
		" AND d.ic_moneda = m.ic_moneda "+
		" AND s.ic_estatus_solic = es.ic_estatus_solic "+
		" AND d.ic_epo = pe.ic_epo "+
		" AND d.ic_pyme = pe.ic_pyme "+
		" AND s.ic_banco_fondeo = bf.ic_banco_fondeo " +
		" AND s.cs_tipo_solicitud = 'C' "+
		" AND ds.ic_documento = cc.ic_documento (+) "+
		" AND cc.ic_pedido = ps.ic_pedido (+) "+
		" AND d.ic_beneficiario = i2.ic_if (+) "+
		" AND ps.ic_pedido = a.ic_pedido (+) "+

	   //Agregado
		"  and d.ic_if=i3.ic_if (+) "+
      "  and ds.ic_if = i4.ic_if (+) "+		
		//	" AND s.ic_folio = procaut.ic_folio (+) "+
		" AND ds.ic_if=ds.ic_if " );

		// Condiciones
		if(!ic_tasa.equals("")){
			query.append(" and ds.CG_TIPO_TASA = ? ");
			conditions.add(ic_tasa);
		}		
		if (ic_if.equals("") && ic_epo.equals("") && ic_moneda.equals("")
			&& ic_estatus_solic.equals("") && ic_estado.equals("")
			&& ic_folioMin.equals("") && cc_acuse.equals("")
			&& fn_monto_dsctoMin.equals("") && df_fecha_solicitudMin.equals("")
			&& !tipo_busqueda.equals(""))
		{
			df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}
		if (!ic_if.equals("")) {
			  query.append(" and ds.ic_if = ? ");
			  conditions.add(new Long(ic_if));
		}
		if (!ic_epo.equals("")) {
			query.append(" AND ds.ic_epo = ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and d.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and pe.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and e.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
	 		
		  if (!ic_pyme.equals("") && !ic_pyme.equals("T")){
			query.append(" and d.ic_pyme = ? ");
			conditions.add(new Long(ic_pyme));
		  }
		}
		if(!ic_folioMin.equals(""))
		{
			query.append(" and s.ic_folio = ? ");
			conditions.add(ic_folioMin);
		}
		if (!ic_estatus_solic.equals(""))
		{
			query.append(" and s.ic_estatus_solic = ? ");
			conditions.add(ic_estatus_solic);
		}
		if (!ic_moneda.equals(""))
		{
			query.append(" and d.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}
		if (!cc_acuse.equals(""))
		{
			query.append(" and s.cc_acuse = ? ");
			conditions.add(cc_acuse);
		}
		
		if(!fn_monto_dsctoMin.equals("")){
		  if(!fn_monto_dsctoMax.equals("")){
			  query.append(" and d.fn_monto_dscto between ? and ? ");
			  conditions.add(fn_monto_dsctoMin);
			  conditions.add(fn_monto_dsctoMax);
		  } else {
			  query.append(" and d.fn_monto_dscto = ?");
			  conditions.add(fn_monto_dsctoMin);
		  }
		}
		if(!df_fecha_solicitudMin.equals("")){
			if(!df_fecha_solicitudMax.equals("")) {
			  query.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
			  conditions.add(df_fecha_solicitudMin);
			  conditions.add(df_fecha_solicitudMax);
			} else {
			  query.append(" and s.df_fecha_solicitud = TO_DATE(?,'dd/mm/yyyy') ");
			  conditions.add(df_fecha_solicitudMin);
			}
		}
		if (!ic_estado.equals("")) {
			query.append( " and d.ic_pyme in (select distinct ic_pyme from com_domicilio where ic_estado = ? and cs_fiscal = 'S') ");
			conditions.add(ic_estado);
		}
		//if(!tipo_busqueda.equals("")){
		if(!tipo_busqueda.equals("") && !tipo_busqueda.equals("F")){
			query.append(" and s.cg_tipo_busqueda = ? ");
			conditions.add(tipo_busqueda);
		}
		//Agregado
		if(tipo_busqueda.equals("F")){
			query.append(" and ds.cs_opera_fiso = 'S' ");
		}
		if(tipo_busqueda.equals("A")&&ic_estatus_solic.equals("")){
			query.append(" and s.ic_estatus_solic in (3,5,6)");
		}
		
		if (!"".equals(aplicaFloating)  ) {
			query.append(" AND s.CS_FLOATING = ?");  
			conditions.add(aplicaFloating);
		}
				

		query.append(criterioOrdenamiento); 
		
		log.info("qrySentencia  "+query);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return query.toString();	
	
 }

 public String getAggregateCalculationQuery() {
 
 log.info("getAggregateCalculationQuery(E)");
	 // Este metodo generara el query de los totales 
		StringBuffer query = new StringBuffer("");
		conditions =new ArrayList();
		
		String criterioOrdenamiento=" group by d.ic_moneda,'ConsSolicNafinDE.java' order by d.ic_moneda ";
		
		
		query.append(" select  /*+ index (s in_com_solicitud_13_nuk)  */"+
				"		d.ic_moneda CVE_MONEDA,"   +
				" 	   count(ds.ic_documento) TOTAL_DOCTOS, "   +
				" 	   sum(nvl(d.fn_monto,0)) MONTO_DOCTOS, "   +
				" 	   sum(nvl(d.fn_monto_dscto,0)) MONTO_DESC_DOCTOS, "   +
				" 	   sum(nvl(ds.in_importe_recibir,0)) MONTO_RECIBIR_DOCTOS, "   +
				" 	   sum(decode(s.ic_estatus_solic,2,1,0)) TOTAL_SOLIC_RECIBIDAS, "   +
				" 	   sum(decode(s.ic_estatus_solic,3,1,0)) TOTAL_SOLIC_OPER, "   +
				" 	   sum(decode(s.ic_estatus_solic,4,1,0)) TOTAL_SOLIC_RECH,	  "   +
				" 	   sum(decode(s.ic_estatus_solic,2,nvl(d.fn_monto_dscto,0),0)) MONTO_DESC_RECIB, "   +
				" 	   sum(decode(s.ic_estatus_solic,3,nvl(d.fn_monto_dscto,0),0)) MONTO_DESC_OPER, "   +
				" 	   sum(decode(s.ic_estatus_solic,4,nvl(d.fn_monto_dscto,0),0)) MONTO_DESC_RECH,	"+
				" 	   sum(nvl(d.fn_monto,0)-nvl(d.fn_monto_dscto,0)) MONTO_RECURSO, "   +
				" 	   sum(nvl(ds.in_importe_interes,0)) MONTO_INTERES, " +
				" 	   sum((nvl(s.FG_PORC_COMISION_FONDEO,0)*nvl(ds.in_importe_interes,0))/100) MONTO_COMISION, " +
				"	  'ConsSolicNafinDE.java' ORIGENQUERY "  +
				" from "   +
				" 	com_docto_seleccionado ds, "   +
				" 	com_solicitud s, "   +
				" 	com_documento d , "   +
				" 	comrel_pyme_epo pe,  "   +
				" 	comcat_epo e, " +
				"  comrel_producto_epo prodepo " +
				//"   (select ic_folio, count(*) as procesada from com_control04 where cs_estatus = 'S' group by ic_folio) procAut " +
				" where s.ic_documento=ds.ic_documento and "   +
				" 	ds.ic_epo = e.ic_epo and "+
				" 	e.ic_epo = prodepo.ic_epo and " +
				"  prodepo.ic_producto_nafin = 1 and " +
				//"  (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) and " +
				" 	ds.ic_documento=d.ic_documento and "   +
				" 	d.ic_pyme = pe.ic_pyme and "   +
				" 	d.ic_epo = pe.ic_epo ");
				//" 	d.ic_epo = pe.ic_epo and "  +
				//"  ds.cs_opera_fiso = 'N' ");
			// Condiciones
		
		//Agregado fodea 17
		//System.out.println("####El value de esTotal2 es: "+esTotal2);
		//if(esTotal2.equals("")){
		//	query.append(" and ds.cs_opera_fiso = 'N' ");
		//}else{
		//	query.append(" and ds.cs_opera_fiso = 'S' ");
		//}
		
						
		if(!ic_tasa.equals("")) {
			query.append(" and ds.CG_TIPO_TASA = ? ");	
			conditions.add(ic_tasa);	
		}
		// Generacion del query
		if (ic_if.equals("") && ic_epo.equals("") && ic_moneda.equals("")
			&& ic_estatus_solic.equals("") && ic_estado.equals("")
			&& ic_folioMin.equals("") && cc_acuse.equals("")
			&& fn_monto_dsctoMin.equals("") && df_fecha_solicitudMin.equals("")
			&& !tipo_busqueda.equals(""))
		{
			df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}
		if (!ic_if.equals("")) {
			   query.append(" and ds.ic_if = ? ");
			  conditions.add(new Long(ic_if));
		}
		if (!ic_epo.equals("")) {
			query.append(" AND ds.ic_epo = ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and d.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and pe.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and e.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
			
			if (!ic_pyme.equals("") && !ic_pyme.equals("T")){
				query.append(" and d.ic_pyme = ? ");
				conditions.add(new Long(ic_pyme));
			}
		}
		if(!ic_folioMin.equals(""))
		{
			query.append(" and s.ic_folio = ? ");
			conditions.add(ic_folioMin);
		}
		if (!ic_estatus_solic.equals(""))
		{
			query.append(" and s.ic_estatus_solic = ? ");
			conditions.add(ic_estatus_solic);
		}
		if (!ic_moneda.equals(""))
		{
			query.append(" and d.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}
		if (!cc_acuse.equals(""))
		{
			query.append(" and s.cc_acuse = ? ");
			conditions.add(cc_acuse);
		}
		if(!fn_monto_dsctoMin.equals("")) {
		  if(!fn_monto_dsctoMax.equals("")) {
			  query.append(" and d.fn_monto_dscto between ? and ? ");
			  conditions.add(fn_monto_dsctoMin);
			  conditions.add(fn_monto_dsctoMax);
		  } else {
			  query.append(" and d.fn_monto_dscto = ?");
			  conditions.add(fn_monto_dsctoMin);
		  }
		}
		if(!df_fecha_solicitudMin.equals("")) {
			if(!df_fecha_solicitudMax.equals("")) {
			  query.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
			  conditions.add(df_fecha_solicitudMin);
			  conditions.add(df_fecha_solicitudMax);
		} else {
			  query.append(" and s.df_fecha_solicitud = TO_DATE(?,'dd/mm/yyyy') ");
			  conditions.add(df_fecha_solicitudMin);
			}
		}
		if (!ic_estado.equals(""))
		{
		   query.append( " and d.ic_pyme in (select distinct ic_pyme from com_domicilio where ic_estado = ? and cs_fiscal = 'S') ");
			conditions.add(ic_estado);
		}
		//if(!tipo_busqueda.equals("")){
		if(!tipo_busqueda.equals("") && !tipo_busqueda.equals("F")){
			query.append(" and s.cg_tipo_busqueda = ? ");
			conditions.add(tipo_busqueda);
		}
				//Agregado
		//if(tipo_busqueda.equals("F")){
		//	query.append(" and ds.cs_opera_fiso = 'S' ");
		//}	
		
		if(tipo_busqueda.equals("A")&&ic_estatus_solic.equals("")){
			query.append(" and s.ic_estatus_solic in (3,5,6)");
		}
			
			
		if (!"".equals(aplicaFloating)  ) {
			query.append(" AND s.CS_FLOATING = ?"); 
			conditions.add(aplicaFloating);
		}
				
		query.append(criterioOrdenamiento);
		//System.out.println("Totales:"+query.toString());
		
		log.info("getAggregateCalculationQuery "+query.toString());
		log.info("getAggregateCalculationQuery(S)");
		return query.toString();
 }
 
 public String getDocumentSummaryQueryForIds(List pageIds) {
	// Genera el query que extrae solo un segmento de la informacion
	
	log.info("getDocumentSummaryQueryForIds(E)");
	StringBuffer query = new StringBuffer("");
	conditions 		= new ArrayList();
	
	//query.append(" select /*+ ordered index(ds IN_COM_DOCTO_SELEC_04_NUK) use_nl(ds,d,pe,s,p,ps,a,cc,e,i,i2,es,m) */ 'Interna' as tipo "+
		query.append(" select /*+ index (s in_com_solicitud_13_nuk)  */ 'Interna' as tipo "+
		" , ds.cs_opera_fiso as opera_fiso "+
		" , (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(ds.cs_opera_fiso,'N',i.cg_razon_social,'S',i3.cg_razon_social))) as nombreIF "+
		" , (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(ds.cs_opera_fiso,'N','N/A','S',i4.cg_razon_social))) as nombreFideicomiso "+
		" , (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO"+
		" , (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+
		" , substr(s.ic_folio,1,10)||'-'||substr(s.ic_folio,11,1)as ic_folio"+
		" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as df_fecha_solicitud"+
		" , d.ic_moneda, m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto "+
		" , (decode (ds.cs_opera_fiso,'N','N/A','S',ds.in_importe_recibir)) as in_importe_recibir "+
		" , (decode (ds.cs_opera_fiso,'S',ds.in_importe_recibir_fondeo,ds.in_importe_recibir)) as in_importe_recibir_if "+
		" , (decode (ds.cs_opera_fiso,'N','N/A','S',ds.in_tasa_aceptada)) as in_tasa_aceptada "+
		" , (decode (ds.cs_opera_fiso,'S',ds.in_tasa_aceptada_fondeo,ds.in_tasa_aceptada)) as in_tasa_aceptada_if "+		
		//" , ds.in_tasa_aceptada " +
		" , (decode (ds.cs_opera_fiso,'N','N/A','S',ds.in_importe_interes)) as in_importe_interes "+		
		" , (decode (ds.cs_opera_fiso,'S',ds.in_importe_interes_fondeo,ds.in_importe_interes)) as in_importe_interes_if "+	
		//" , ds.in_importe_interes " +
		" , es.cd_descripcion " +
		" , es.ic_estatus_solic, a.ig_numero_prestamo "+
		" , s.ig_numero_prestamo as NUMERO_PRESTAMO"+
		" , TO_CHAR(s.df_operacion,'DD/MM/YYYY') as df_operacion"+
		//" , decode (s.ic_estatus_solic, 3, decode ( procAut.procesada, 1, 'Automatica', Null, 'Manual'), '') as proceso"+
		",DECODE (s.ic_estatus_solic,3 "+
		//" nvl2(procaut.ic_folio,'Automatica','Manual'), "+
		" , DECODE (s.cg_tipo_busqueda,'A','Autom�tica','M','Manual'), '') AS proceso "+
		" , TO_CHAR(s.df_v_descuento, 'DD/MM/YYYY') as FECHA_VENCIMIENTO" +
		" , p.in_numero_sirac "+
		" ,	s.fg_porc_comision_fondeo as fg_porc_comision_fondeo " + 
		//" , ds.in_importe_recibir as in_importe_recibir " + 
		" , decode(d.cs_dscto_especial, 'D', i2.cg_razon_social, 'I', i2.cg_razon_social, '') as beneficiario"+
		" , decode(d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, 0) as fn_porc_beneficiario"+
		" , decode(d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, 0) as RECIBIR_BENEF " +
		" , decode(d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 0) as NETO_REC_PYME "+
		" , d.cs_dscto_especial , s.fg_tasa_fondeo_nafin as TASA_FONDEO, bf.cd_descripcion NOM_BANCO_FONDEO ");
		
		query.append( ", D.ic_epo, TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO ");
		query.append( ",decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial' || ''|| decode ( s.CS_FLOATING, 'S',' / Floating') ,'O','Oferta de Tasas','N','Negociada')  as tipoTasa "); 
		query.append( ", s.CC_ACUSE AS IC_ACUSE");
		query.append( ", 'ConsSolicNafinDE.java' ORIGENQUERY ");
		/*
		" from com_solicitud s, comcat_estatus_solic es"+
		" , com_docto_seleccionado ds, com_documento d"+
		" , comcat_moneda m, comcat_epo e, comcat_pyme p, comrel_pyme_epo pe"+
		" , comcat_if i, com_cobro_credito cc, com_pedido_seleccionado ps, com_anticipo a, comcat_if i2"+
		", (select ic_folio, count(*) as procesada from com_control04 where cs_estatus = 'S' group by ic_folio) procAut"+
		" where"+
		" s.ic_documento=ds.ic_documento"+
		" and ds.ic_documento=d.ic_documento"+
		" and ds.ic_if=i.ic_if"+
		" and ds.ic_epo=e.ic_epo"+
		" and d.ic_pyme=p.ic_pyme"+
		" and d.ic_moneda=m.ic_moneda"+
		" and s.ic_estatus_solic=es.ic_estatus_solic"+
		" and d.ic_epo = pe.ic_epo and d.ic_pyme = pe.ic_pyme"+
		" and s.cs_tipo_solicitud='C'"+
		" and ds.ic_documento = cc.ic_documento(+) "+
		" and cc.ic_pedido = ps.ic_pedido(+)"+
			" and d.ic_beneficiario = i2.ic_if (+) "+
		" and ps.ic_pedido = a.ic_pedido(+)"+
		" and s.ic_folio = procAut.ic_folio (+) "+
		*/
		query.append(", (SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = s.CC_ACUSE) AS MUESTRA_VISOR ");
		query.append(" FROM   "+
		" com_docto_seleccionado  ds,/*300,558*/ "+
		" com_documento  d, /*506,570*/ "+
		" comrel_pyme_epo pe,/*369,557*/ "+
		" com_solicitud s, /*298,664*/ "+
		" comcat_pyme p, /*91,777*/ "+
	//	" (SELECT IC_FOLIO FROM com_control04 /*259,849*/ "+
	//	"  WHERE cs_estatus = 'S') procaut, "+
		" com_pedido_seleccionado ps, /*89*/ "+
		" com_anticipo a, /*86*/"+
		" com_cobro_credito cc, /*88*/ "+
		" comcat_epo e, /*258*/ "+
		" comcat_if i, /*125*/ "+
		" comcat_if i2, /*125*/ "+
		" comcat_if i3, comcat_if i4, "+
		" comcat_estatus_solic es, /*11*/ "+
		" comcat_moneda m, "+
		" comcat_banco_fondeo bf"+
		" WHERE s.ic_documento = ds.ic_documento "+
		" AND ds.ic_documento = d.ic_documento "+
		" AND ds.ic_if = i.ic_if "+
		" AND ds.ic_epo = e.ic_epo "+
		" AND d.ic_pyme = p.ic_pyme "+
		" AND d.ic_moneda = m.ic_moneda "+
		" AND s.ic_estatus_solic = es.ic_estatus_solic "+
		" AND d.ic_epo = pe.ic_epo "+
		" AND d.ic_pyme = pe.ic_pyme "+
		" AND s.ic_banco_fondeo = bf.ic_banco_fondeo " +
	//	" AND s.cs_tipo_solicitud = 'C' "+
		" AND ds.ic_documento = cc.ic_documento (+) "+
		" AND cc.ic_pedido = ps.ic_pedido (+) "+
		" AND d.ic_beneficiario = i2.ic_if (+) "+
		" AND ps.ic_pedido = a.ic_pedido (+) "+
	   //Agregado
		"  and d.ic_if=i3.ic_if (+) "+
      "  and ds.ic_if = i4.ic_if (+) "+ 
	//	" AND s.ic_folio = procaut.ic_folio (+) "+
		" and ds.ic_if=ds.ic_if "+
			" AND ( ");
			
		for (int i = 0; i < pageIds.size(); i++) {
	List lItem = (ArrayList)pageIds.get(i);
		
			if(i > 0){query.append("  OR  ");}
					query.append(" D.ic_documento = ? ");
				conditions.add(new Long(lItem.get(0).toString()));
				System.out.println("todos los ids: "+new Long(lItem.get(0).toString()));
		}
		
		query.append(" ) ");
		
		log.info("qrySentencia "+query);
		log.info("conditions "+conditions);
		
		//System.out.println(query.toString());
		System.out.println("getDocumentSummaryQueryForIds "+query.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
	return query.toString();
 }
 
 
 public String getDocumentQuery() {
 
		 log.info("getDocumentQuery(E)");
	
	// Genera el query que devuelve las llaves primarias de la consulta
		StringBuffer query = new StringBuffer("");
		conditions 		= new ArrayList();
	
		String criterioOrdenamiento=" ORDER BY ds.IC_DOCUMENTO ";

		
		query.append(" select  /*+ index (s in_com_solicitud_13_nuk)  */"+
				" ds.ic_documento "+
				" from "   +
				" 	com_docto_seleccionado ds, "   +
				" 	com_solicitud s, " +
				" 	com_documento d , " +
				" 	comrel_pyme_epo pe,  "   +
				" 	comcat_epo e, " +
				"  comrel_producto_epo prodepo " +
//				"   (select ic_folio, count(*) as procesada from com_control04 where cs_estatus = 'S' group by ic_folio) procAut " +
				" where s.ic_documento=ds.ic_documento and "   +
				" 	ds.ic_documento=d.ic_documento and "   +
				" 	ds.ic_epo = e.ic_epo and " +
				" 	d.ic_pyme = pe.ic_pyme and "   +
				" 	d.ic_epo = pe.ic_epo and " +
				" 	e.ic_epo = prodepo.ic_epo and " +
				"  prodepo.ic_producto_nafin = 1 " );
			
		// Condiciones
		
		if(!ic_tasa.equals("")){
			query.append(" and ds.CG_TIPO_TASA = ? ");	
			conditions.add(ic_tasa);
		}		
		if (ic_if.equals("") && ic_epo.equals("") && ic_moneda.equals("")
			&& ic_estatus_solic.equals("") && ic_estado.equals("")
			&& ic_folioMin.equals("") && cc_acuse.equals("")
			&& fn_monto_dsctoMin.equals("") && df_fecha_solicitudMin.equals("")
			&& !tipo_busqueda.equals(""))
		{
			df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}
		if (!ic_if.equals("")) {
			  query.append(" and ds.ic_if = ? ");
			  conditions.add(new Long(ic_if));
		}
		if (!ic_epo.equals("")) {
			query.append(" AND ds.ic_epo = ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and d.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and pe.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and e.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
	 		
		  if (!ic_pyme.equals("") && !ic_pyme.equals("T")){
			query.append(" and d.ic_pyme = ? ");
			conditions.add(new Long (ic_pyme));
		  }
		}
		if(!ic_folioMin.equals(""))
		{
			query.append(" and s.ic_folio = ? ");
			conditions.add(ic_folioMin);
		}
		if (!ic_estatus_solic.equals(""))
		{
			query.append(" and s.ic_estatus_solic = ? ");
			conditions.add(ic_estatus_solic);
		}
		if (!ic_moneda.equals(""))
		{
			query.append(" and d.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}
		if (!cc_acuse.equals(""))
		{
			query.append(" and s.cc_acuse = ? ");
			conditions.add(cc_acuse);
		}
		
		if(!fn_monto_dsctoMin.equals("")){
		  if(!fn_monto_dsctoMax.equals("")){
			  query.append(" and d.fn_monto_dscto between ? and ? ");
			  conditions.add(fn_monto_dsctoMin);
			  conditions.add(fn_monto_dsctoMax);
		  } else {
			  query.append(" and d.fn_monto_dscto = ?");
			  conditions.add(fn_monto_dsctoMin);
		  }
		}
		if(!df_fecha_solicitudMin.equals("")){
			if(!df_fecha_solicitudMax.equals("")) {
			  query.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
			  conditions.add(df_fecha_solicitudMin);
			  conditions.add(df_fecha_solicitudMax);
			} else {
			  query.append(" and s.df_fecha_solicitud = TO_DATE(?,'dd/mm/yyyy') ");
			  conditions.add(df_fecha_solicitudMin);
			}
		}
		if (!ic_estado.equals("")) {
			query.append( " and d.ic_pyme in (select distinct ic_pyme from com_domicilio where ic_estado = ? and cs_fiscal = 'S') ");
			conditions.add(ic_estado);
		}
		//if(!tipo_busqueda.equals("")){
		if(!tipo_busqueda.equals("") && !tipo_busqueda.equals("F")){
			query.append(" and s.cg_tipo_busqueda = ? ");
			conditions.add(tipo_busqueda);
		}
		//Agregado
		if(tipo_busqueda.equals("F")){
			query.append(" and ds.cs_opera_fiso = 'S' ");
		}		
		if(tipo_busqueda.equals("A")&&ic_estatus_solic.equals("")){
			query.append(" and s.ic_estatus_solic in (3,5,6)");
		}
				

		if (!"".equals(aplicaFloating)  ) {
			query.append(" AND s.CS_FLOATING = ?"); 
			conditions.add(aplicaFloating);
		} 

		query.append(criterioOrdenamiento); 
		
		log.info("qrySentencia  "+query);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return query.toString();	
 }
 
 //Agregado fodea 17
 	public Registros getTotales2(){
		log.info("getTotales2(E)");
		String qrySentencia = null;
		Registros registros = new Registros();
		StringBuffer condicion = new StringBuffer();
			
		AccesoDB con = null;
		
	
		try{		
			con = new AccesoDB();
			con.conexionDB();
			StringBuffer query = new StringBuffer("");
			conditions =new ArrayList();
		
			String criterioOrdenamiento=" group by d.ic_moneda,'ConsSolicNafinDE.java' order by d.ic_moneda ";
			
			query.append(" select  /*+ index (s in_com_solicitud_13_nuk)  */"+
				"		d.ic_moneda CVE_MONEDA,"   +
				" 	   count(ds.ic_documento) TOTAL_DOCTOS, "   +
				" 	   sum(nvl(d.fn_monto,0)) MONTO_DOCTOS, "   +
				" 	   sum(nvl(d.fn_monto_dscto,0)) MONTO_DESC_DOCTOS, "   +
				" 	   sum(nvl(ds.in_importe_recibir,0)) MONTO_RECIBIR_DOCTOS, "   +
				" 	   sum(decode(s.ic_estatus_solic,2,1,0)) TOTAL_SOLIC_RECIBIDAS, "   +
				" 	   sum(decode(s.ic_estatus_solic,3,1,0)) TOTAL_SOLIC_OPER, "   +
				" 	   sum(decode(s.ic_estatus_solic,4,1,0)) TOTAL_SOLIC_RECH,	  "   +
				" 	   sum(decode(s.ic_estatus_solic,2,nvl(d.fn_monto_dscto,0),0)) MONTO_DESC_RECIB, "   +
				" 	   sum(decode(s.ic_estatus_solic,3,nvl(d.fn_monto_dscto,0),0)) MONTO_DESC_OPER, "   +
				" 	   sum(decode(s.ic_estatus_solic,4,nvl(d.fn_monto_dscto,0),0)) MONTO_DESC_RECH,	"+
				" 	   sum(nvl(d.fn_monto,0)-nvl(d.fn_monto_dscto,0)) MONTO_RECURSO, "   +
				" 	   sum(nvl(ds.in_importe_interes,0)) MONTO_INTERES, " +
				" 	   sum(nvl(ds.in_importe_interes_fondeo,0)) MONTO_INTERES_IF, " +
				" 	   sum((nvl(s.FG_PORC_COMISION_FONDEO,0)*nvl(ds.in_importe_interes,0))/100) MONTO_COMISION, " +
				"	  'ConsSolicNafinDE.java' ORIGENQUERY "  +
				" from "   +
				" 	com_docto_seleccionado ds, "   +
				" 	com_solicitud s, "   +
				" 	com_documento d , "   +
				" 	comrel_pyme_epo pe,  "   +
				" 	comcat_epo e, " +
				"  comrel_producto_epo prodepo " +
				//"   (select ic_folio, count(*) as procesada from com_control04 where cs_estatus = 'S' group by ic_folio) procAut " +
				" where s.ic_documento=ds.ic_documento and "   +
				" 	ds.ic_epo = e.ic_epo and "+
				" 	e.ic_epo = prodepo.ic_epo and " +
				"  prodepo.ic_producto_nafin = 1 and " +
				//"  (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) and " +
				" 	ds.ic_documento=d.ic_documento and "   +
				" 	d.ic_pyme = pe.ic_pyme and "   +
				" 	d.ic_epo = pe.ic_epo ");
				//" 	d.ic_epo = pe.ic_epo and "  +
				//"  ds.cs_opera_fiso = 'N' ");
			// Condiciones
		
		//Agregado fodea 17
		//System.out.println("####El value de esTotal2 es: "+esTotal2);
		//if(esTotal2.equals("")){
		//	query.append(" and ds.cs_opera_fiso = 'N' ");
		//}else{
			query.append(" and ds.cs_opera_fiso = 'S' ");
		//}
		
						
		if(!ic_tasa.equals("")) {
			query.append(" and ds.CG_TIPO_TASA = ? ");	
			conditions.add(ic_tasa);	
		}
		// Generacion del query
		if (ic_if.equals("") && ic_epo.equals("") && ic_moneda.equals("")
			&& ic_estatus_solic.equals("") && ic_estado.equals("")
			&& ic_folioMin.equals("") && cc_acuse.equals("")
			&& fn_monto_dsctoMin.equals("") && df_fecha_solicitudMin.equals("")
			&& !tipo_busqueda.equals(""))
		{
			df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}
		if (!ic_if.equals("")) {
			   query.append(" and ds.ic_if = ? ");
			  conditions.add(new Long(ic_if));
		}
		if (!ic_epo.equals("")) {
			query.append(" AND ds.ic_epo = ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and d.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and pe.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
			query.append(" and e.ic_epo= ? ");
			conditions.add(new Long(ic_epo));
			
			if (!ic_pyme.equals("") && !ic_pyme.equals("T")){
				query.append(" and d.ic_pyme = ? ");
				conditions.add(new Long(ic_pyme));
			}
		}
		if(!ic_folioMin.equals(""))
		{
			query.append(" and s.ic_folio = ? ");
			conditions.add(ic_folioMin);
		}
		if (!ic_estatus_solic.equals(""))
		{
			query.append(" and s.ic_estatus_solic = ? ");
			conditions.add(ic_estatus_solic);
		}
		if (!ic_moneda.equals(""))
		{
			query.append(" and d.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}
		if (!cc_acuse.equals(""))
		{
			query.append(" and s.cc_acuse = ? ");
			conditions.add(cc_acuse);
		}
		if(!fn_monto_dsctoMin.equals("")) {
		  if(!fn_monto_dsctoMax.equals("")) {
			  query.append(" and d.fn_monto_dscto between ? and ? ");
			  conditions.add(fn_monto_dsctoMin);
			  conditions.add(fn_monto_dsctoMax);
		  } else {
			  query.append(" and d.fn_monto_dscto = ?");
			  conditions.add(fn_monto_dsctoMin);
		  }
		}
		if(!df_fecha_solicitudMin.equals("")) {
			if(!df_fecha_solicitudMax.equals("")) {
			  query.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
			  conditions.add(df_fecha_solicitudMin);
			  conditions.add(df_fecha_solicitudMax);
		} else {
			  query.append(" and s.df_fecha_solicitud = TO_DATE(?,'dd/mm/yyyy') ");
			  conditions.add(df_fecha_solicitudMin);
			}
		}
		if (!ic_estado.equals(""))
		{
		   query.append( " and d.ic_pyme in (select distinct ic_pyme from com_domicilio where ic_estado = ? and cs_fiscal = 'S') ");
			conditions.add(ic_estado);
		}
		//if(!tipo_busqueda.equals("")){
		if(!tipo_busqueda.equals("") && !tipo_busqueda.equals("F")){
			query.append(" and s.cg_tipo_busqueda = ? ");
			conditions.add(tipo_busqueda);
		}
				//Agregado
		if(tipo_busqueda.equals("F")){
			query.append(" and ds.cs_opera_fiso = 'S' ");
		}	
					
		  if ("A".equals(tipo_busqueda)  && "".equals(ic_estatus_solic)   ) {
			query.append(" and s.ic_estatus_solic in (3,5,6)");
			}
		
		
		if (!"".equals(aplicaFloating)  ) {
			query.append(" AND s.CS_FLOATING = ?"); 
			conditions.add(aplicaFloating);
		}
				
				
		query.append(criterioOrdenamiento);
		log.info("qurytotales2"+query.toString());
		registros = con.consultarDB(query.toString(),conditions);
		
		return registros;
		
			
			}catch(Exception e){
				log.error("Error m�todo getTotales2: ",e);
				throw new AppException("Error en consulta para ventana Bloqueo de IF", e);
			} finally{
				log.info("getTotales2(S)");	
				if(con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}
					
			
		}//fin metodo

 
 
 
 /**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

 

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_epo() { 
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_folio() {
		return ic_folioMin;
	}

	public void setIc_folio(String ic_folio) {
		this.ic_folioMin = ic_folio;
	}

	public String getDf_fecha_solicitudMin() {
		return df_fecha_solicitudMin;
	}

	public void setDf_fecha_solicitudMin(String df_fecha_solicitudMin) {
		this.df_fecha_solicitudMin = df_fecha_solicitudMin;
	}

	public String getDf_fecha_solicitudMax() {
		return df_fecha_solicitudMax;
	}

	public void setDf_fecha_solicitudMax(String df_fecha_solicitudMax) {
		this.df_fecha_solicitudMax = df_fecha_solicitudMax;
	}

	public String getIc_estatus_solic() {
		return ic_estatus_solic;
	}

	public void setIc_estatus_solic(String ic_estatus_solic) {
		this.ic_estatus_solic = ic_estatus_solic;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getFn_montoMin() {
		return fn_monto_dsctoMin;
	}

	public void setFn_montoMin(String fn_montoMin) {
		this.fn_monto_dsctoMin = fn_montoMin;
	}

	public String getFn_montoMax() {
		return fn_monto_dsctoMax;
	}

	public void setFn_montoMax(String fn_montoMax) {
		this.fn_monto_dsctoMax = fn_montoMax;
	}


	public String getTipoTasa() {
		return ic_tasa;
	}

	public void setTipoTasa(String ic_tasa) {
		this.ic_tasa = ic_tasa;
	}

	public String getTipoBusqueda() {
		return tipo_busqueda;
	}

	public void setTipoBusqueda(String tipo_busqueda) {
		this.tipo_busqueda = tipo_busqueda;
	}

	public String getAcuse() {
		return cc_acuse;
	}

	public void setAcuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}
	
	public String getEstado() {
		return ic_estado;
	}

	public void setEstado(String ic_estado) {
		this.ic_estado = ic_estado;
	}

	public String getOrdenadoBy() {
		return ordenadoBy;
	}

	public void setOrdenadoBy(String ordenadoBy) {
		this.ordenadoBy = ordenadoBy;
	}

	public String getEsBANCOMEXT() {
		return esBANCOMEXT;
	}

	public void setEsBANCOMEXT(String esBANCOMEXT) {
		this.esBANCOMEXT = esBANCOMEXT; 
	}

	public int getCountReg() {
		return countReg;
	}
	

	public void setAplicaFloating(String aplicaFloating) {
		this.aplicaFloating = aplicaFloating;
	}

	public String getAplicaFloating() {
		return aplicaFloating;
	}
}
