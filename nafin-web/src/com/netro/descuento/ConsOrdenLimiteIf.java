package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsOrdenLimiteIf implements IQueryGeneratorRegExtJS {
	public ConsOrdenLimiteIf() { }
	
	private List conditions;
	StringBuffer strQuery;
	
	//Condiciones de la consulta
	private String iNoCliente;
	private String icIF;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsOrdenLimiteIf.class);
	
   public String getQuery(){ 
	 String stQuery = "";
	 
	 stQuery = "SELECT (i.ic_if || '' || cpe.ic_epo) as ID, " +
				  " 		cpe.ic_epo as CLAVEEPO, "+ 
	           "      to_char( ie.ig_num_orden) AS orden, " +
				  "      I.cg_razon_social AS nombre_if, " +
		        "      ie.in_limite_epo AS limite, " +
				  "  		CASE "+
				  "  			WHEN ie.fn_limite_utilizado<>0 and  ie.in_limite_epo <>0   "+
		        "      	THEN	ROUND(((ie.fn_limite_utilizado/ie.in_limite_epo)*100),5)  " +
				  "	 		ELSE 0  "+
				  "  		END AS Porcentaje, "+						
		        "      nvl(ie.in_limite_epo,0) - ( nvl(ie.fn_limite_utilizado, 0)  + NVL (ie.fn_monto_comprometido, 0)  )  as  disponible_des, " +
				  "      (ie.in_limite_epo - ie.fn_limite_utilizado) disponible, " +
		        "      CASE WHEN ie.cs_activa_limite = 'S' THEN 'Activa' ELSE 'Inactiva' END AS estatus , " +
				  "      ie.fn_monto_comprometido AS monto_comprometido, " +
				  "      ie.fn_monto_comprometido_dl AS monto_comprometido_dl, " +				  
		        "      i.ic_if as claveIf, " + //Fodea 022-2010 fvr
		        "	   TO_char(ie.df_venc_linea, 'dd/mm/yyyy')	AS fechalinea,  " +
		        "      TO_char(ie.df_cambio_admon, 'dd/mm/yyyy') AS fechacambio,  " +
              "      CASE WHEN ie.CS_FECHA_LIMITE = 'S' THEN 'SI' ELSE 'NO' END as csFechalimite " +
				     "  , ie.in_limite_epo_dl AS limite_dl " +
					" , CASE "+
					"	WHEN ie.fn_limite_utilizado_dl<>0 and  ie.in_limite_epo_dl <>0   "+
					"		THEN ROUND (((ie.fn_limite_utilizado_dl / ie.in_limite_epo_dl) * 100), 5     )  "+
					"	 ELSE 0  "+
					"  END AS porcentaje_dl,  "+		
				  "   (ie.in_limite_epo_dl - ie.fn_limite_utilizado_dl) disponible_dl,  " +
				  "  nvl(ie.in_limite_epo_dl,0) - ( nvl(ie.fn_limite_utilizado_dl, 0)  + NVL (ie.fn_monto_comprometido_dl, 0)  ) as disponible_des_dl  "+				  
				  
		        "  FROM comcat_if i, comrel_if_epo ie,comrel_producto_epo cpe " +
		        " WHERE ie.ic_if = i.ic_if " +
		        "       and UPPER(ie.cs_vobo_nafin) = 'S' " +
		        "       AND ( ie.in_limite_epo <> 0  OR  ie.in_limite_epo_dl <> 0 ) " +
		        "       and cpe.ic_epo=ie.ic_epo " +
		        "       and cpe.ic_producto_nafin=1 " +
		        "       and cpe.cs_limite_pyme!='S' "+
				  "       and i.CS_OPERA_FIDEICOMISO = 'N' ";
				  
	 return stQuery;
	}
	
	public String getAggregateCalculationQuery() {
		conditions	= new ArrayList();
		strQuery	= new StringBuffer();
		
		log.debug("getAggregateCalculationQuery)"+strQuery.toString());
		log.debug("getAggregateCalculationQuery)"+conditions);
		
		return strQuery.toString();
   }
  
   public String getDocumentQuery() {
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		
		strQuery.append(getQuery());
		
		if(!iNoCliente.equals("")){
		 strQuery.append("    And ie.ic_epo = ? ");
		 conditions.add(iNoCliente);
		}
		if(!icIF.equals("")){
		 strQuery.append("    And ie.ic_if = ? ");
		 conditions.add(icIF);
		}
		
		strQuery.append(" ORDER BY i.cg_razon_social "); 
		
		log.debug("getDocumentQuery"+strQuery.toString());
		log.debug("getDocumentQuery)"+conditions);
		
		return strQuery.toString();
  }
  
  public String getDocumentSummaryQueryForIds(List pageIds) {
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		
		strQuery.append("SELECT * FROM ( ");
		strQuery.append(getQuery());
		
		if(!iNoCliente.equals("")){
		 strQuery.append("    And ie.ic_epo = ? ");
		 conditions.add(iNoCliente);
		}
		if(!icIF.equals("")){
		 strQuery.append("    And ie.ic_if = ? ");
		 conditions.add(icIF);
		}
		
		strQuery.append(" ORDER BY i.cg_razon_social "); 
		
		strQuery.append(" ) Tab ");
		
		 for(int i=0;i<pageIds.size();i++) {
		  List lItem = (List)pageIds.get(i);
		  if(i==0) {
		   strQuery.append(" WHERE Tab.ID in ( ");
		  }
		  strQuery.append("?");
		  conditions.add(new Long(lItem.get(0).toString()));					
		  if(i!=(pageIds.size()-1)) {
		   strQuery.append(",");
		  }else{
		   strQuery.append(" ) ");
		  }			
		 }
		
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		
		return strQuery.toString();
  }
  
  public String getDocumentQueryFile() {
	   conditions	= new ArrayList();
		strQuery		= new StringBuffer();	
		
		strQuery.append(getQuery());
		
		if(!iNoCliente.equals("")){
		 strQuery.append("    And ie.ic_epo = ? ");
		 conditions.add(iNoCliente);
		}
		if(!icIF.equals("")){
		 strQuery.append("    And ie.ic_if = ? ");
		 conditions.add(icIF);
		}
		
		strQuery.append(" ORDER BY i.cg_razon_social "); 
		   
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		
		return strQuery.toString();
  }
  
  
  /**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)
	{
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
      
		try {
		 if(tipo.equals("CSV")) {
		  linea = "Intermediario Financiero,Monto Limite,Porcentaje de utilizaci�n,Monto Disponible,Estatus,Monto Comprometido,Monto Disponible despu�s de Comprometido,Fecha Vencimiento L�nea de Cr�dito,Fecha Cambio de Administraci�n,Validar Fechas L�mite\n";
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		  writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		  buffer = new BufferedWriter(writer);
		  buffer.write(linea);
			 
		  while (rs.next()) {
		   String nombreIF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
			String limite = (rs.getString("LIMITE") == null) ? "" : rs.getString("LIMITE");
			String porcentaje = (rs.getString("PORCENTAJE") == null) ? "" : rs.getString("PORCENTAJE");
			String disponible = (rs.getString("DISPONIBLE") == null) ? "" : rs.getString("DISPONIBLE");
			String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
			String montoComprometido = (rs.getString("MONTO_COMPROMETIDO") == null) ? "" : rs.getString("MONTO_COMPROMETIDO");
			String fechaLinea = (rs.getString("FECHALINEA") == null) ? "" : rs.getString("FECHALINEA");
			String fechaCambio = (rs.getString("FECHACAMBIO") == null) ? "" : rs.getString("FECHACAMBIO");
			String fechaLimite = (rs.getString("CSFECHALIMITE") == null) ? "" : rs.getString("CSFECHALIMITE");
		
			linea = nombreIF.replace(',',' ') + ", " + 
			        limite.replace(',',' ') + ", " +
					  porcentaje.replace(',',' ') + ", " +
					  disponible.replace(',',' ') + ", " +
					  estatus.replace(',',' ') + ", " +
					  montoComprometido.replace(',',' ') + ", " +
					  disponible.replace(',',' ') + ", " +
					  fechaLinea.replace(',',' ') + ", " +
					  fechaCambio.replace(',',' ') + ", " +
					  fechaLimite.replace(',',' ') + "\n";				 
			buffer.write(linea);
		  }
		  buffer.close();
		 }
		 else if(tipo.equals("PDF")){
		  HttpSession session = request.getSession();
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		  ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
		  String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		  String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		  String diaActual = fechaActual.substring(0,2);
		  String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		  String anioActual = fechaActual.substring(6,10);
		  String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
		  pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		  session.getAttribute("iNoNafinElectronico").toString(),
		  (String)session.getAttribute("sesExterno"),
		  (String)session.getAttribute("strNombre"),
		  (String)session.getAttribute("strNombreUsuario"),
		  (String)session.getAttribute("strLogo"),
		  (String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
		  pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
		  pdfDoc.setTable(10,100);
		  pdfDoc.setCell("Intermediario Financiero", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Monto Limite", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Porcentaje de utilizaci�n", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Monto Disponible", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Estatus", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Monto Comprometido", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Monto Disponible despu�s de Comprometido", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Fecha Vencimiento L�nea de Cr�dito", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Fecha Cambio de Administraci�n", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Validar Fechas L�mite", "celda01",ComunesPDF.CENTER);
		  
		  while (rs.next()) {
		   String nombreIF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
			String limite = (rs.getString("LIMITE") == null) ? "" : rs.getString("LIMITE");
			String porcentaje = (rs.getString("PORCENTAJE") == null) ? "" : rs.getString("PORCENTAJE");
			String disponible = (rs.getString("DISPONIBLE") == null) ? "" : rs.getString("DISPONIBLE");
			String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
			String montoComprometido = (rs.getString("MONTO_COMPROMETIDO") == null) ? "0" : rs.getString("MONTO_COMPROMETIDO");
			String fechaLinea = (rs.getString("FECHALINEA") == null) ? "" : rs.getString("FECHALINEA");
			String fechaCambio = (rs.getString("FECHACAMBIO") == null) ? "" : rs.getString("FECHACAMBIO");
			String fechaLimite = (rs.getString("CSFECHALIMITE") == null) ? "" : rs.getString("CSFECHALIMITE");
		
			pdfDoc.setCell(nombreIF,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(limite,2),"formas",ComunesPDF.RIGHT);
			//pdfDoc.setCell(limite,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("%"+Comunes.formatoDecimal(porcentaje,5),"formas",ComunesPDF.RIGHT);
			//pdfDoc.setCell(porcentaje,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(disponible,2),"formas",ComunesPDF.RIGHT);
			//pdfDoc.setCell(disponible,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(montoComprometido,2),"formas",ComunesPDF.RIGHT);
			//pdfDoc.setCell(montoComprometido,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(disponible,2),"formas",ComunesPDF.RIGHT);
			//pdfDoc.setCell(disponible,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaLinea,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaLimite,"formas",ComunesPDF.CENTER);
		  }
		  pdfDoc.addTable();
		  pdfDoc.endDocument();
		 }
		}
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
   }
	
	public boolean guardarModificaciones(String[] cveIf,String[] cveEpo, String[] orden, String total){
	log.info("guardarModificaciones(E)");	
	AccesoDB con = new AccesoDB();
		boolean exito = true;
		
		PreparedStatement ps = null;
		try {
			con.conexionDB();
			int num= Integer.parseInt(total);
			for(int i=0;i<num;i++){
					StringBuffer strSQL = null;
					String claveIf = cveIf[i];
					String claveEpo = cveEpo[i];
					String dato = orden[i];
			
					strSQL = new StringBuffer();
					strSQL.append(" UPDATE comrel_if_epo "); 
					strSQL.append(" SET ig_num_orden = ? " );
					strSQL.append(" WHERE ic_epo = ? "); 
					strSQL.append(" AND ic_if = ?");
			
					ps = con.queryPrecompilado(strSQL.toString());						
					if(!dato.equals("")){
						ps.setInt(1,Integer.parseInt(dato));
					}else{
						ps.setString(1,"");
					}
					ps.setInt(2,Integer.parseInt(claveEpo) );
					ps.setInt(3, Integer.parseInt(claveIf));
			
					log.info(" Sentencia::: "+strSQL);
					ps.executeUpdate();
					if(ps != null)
						ps.close();
			}
			return exito;
		}catch(Exception e){
			exito = false;
			log.error("Error en guardarModificaciones: ",e);
			throw new AppException("Error en guardarModificaciones", e);
		}finally{
			log.info("guardarModificaciones(S)");
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	
	}
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}

	public void setIcIF(String icIF) {
		this.icIF = icIF;
	}

	public String getIcIF() {
		return icIF;
	}

	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}

	public String getINoCliente() {
		return iNoCliente;
	}
	
	
  

}