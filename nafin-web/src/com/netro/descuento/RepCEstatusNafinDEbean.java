package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class RepCEstatusNafinDEbean implements IQueryGeneratorRegExtJS {
	
	/* Cambios de Estatus:
	 * 2) Seleccionada PYME a Negociable  
	 * 7) Operada Pendiente de pago 
	 * 8) Modificacion de Importe 
	 * 28) Notas de Credito 
	 * 29)Pogramado a negociable
   * 32)Programado Pyme a Seleccionado Pyme
	 **/
	//private int iNoCambioEstatus;
	
	//Agregado para FODEA 17
	private String tipoBusqueda = "" ; // ""?
	
	private final static Log log = ServiceLocator.getInstance().getLog(RepCEstatusNafinExt.class);
	
	StringBuffer qrySentencia = new StringBuffer("");
	StringBuffer	condicion  =new StringBuffer(); 		
	private List   conditions;
	private int iNoCambioEstatus;
	
	
	public RepCEstatusNafinDEbean() { }
	
		/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery() { 
		return "";
	}
	
	
	/**
	* Obtiene el query para la CONSULTA
	* @return Cadena con la consulta de SQL
	*/
	public String getDocumentQuery() { 
		return "";
	}

	/**
	* Obtiene el query para obtener la consulta PAGINACION
	* @return Cadena con la consulta de SQL, para obtener la consulta
	*/
	public String getDocumentSummaryQueryForIds(List pageIds) {	
		return "";
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.Objeto 
	 * Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos con PAGINADOR*/		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){	
		return "";
	}	
	
	//public void setNoCambioEstatus(int iNoCambioEstatus) {
	//	this.iNoCambioEstatus = iNoCambioEstatus;
	//}
	
	public Registros getQueryPorCambioEstatus7() {
		StringBuffer sbQuery = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		List 		conditions = new ArrayList();
		Registros registros = new Registros();
		AccesoDB con = null;
		
		try{
		con = new AccesoDB();
		con.conexionDB();
		
		hint.append("/*+ use_nl(d ds ce p e i i2 m tf) index(ce) */ ");
		sbQuery.append(" SELECT "+hint+" (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) as nombreEPO ");
		sbQuery.append(" ,(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) as nombrePYME "+
							" , (decode (I.cs_habilitado,'N','*','S',' ')||' '||(decode(DS.cs_opera_fiso,'N',I.cg_razon_social,'S',I3.cg_razon_social))) as nombreIF "+	
							" , (decode (I.cs_habilitado,'N','*','S',' ')||' '||(decode(DS.cs_opera_fiso,'N','N/A','S',I4.cg_razon_social))) as nombreFideicomiso ");
	
		sbQuery.append(",D.cc_acuse as numeroAcuse ");

		sbQuery.append(",D.ig_numero_docto as numeroDocto "+
						   ",M.cd_nombre as nombreMoneda "+
						   ",M.ic_moneda as claveMoneda ");
					
		//sbQuery.append(",D.cs_dscto_especial as tipoFactoraje " +
		sbQuery.append(",D.cs_dscto_especial as tipoFactoraje " +
						   ",D.fn_monto as montoDocto "+
						   ",I2.cg_razon_social as beneficiario " +
						   ",D.fn_porc_beneficiario as porcBeneficiario ");
							
		sbQuery.append(",DS.fn_importe_recibir_benef as importeARecibirBeneficiario " +
						   ",D.fn_porc_anticipo as porcentaje " +
						   ",D.fn_monto_dscto as montoDscto "+
						   //",DS.in_tasa_aceptada as tasaAceptada "+
						   " , (decode (DS.cs_opera_fiso,'N','N/A','S',DS.in_tasa_aceptada)) as tasaAceptada "+
							" , (decode (DS.cs_opera_fiso,'S',DS.in_tasa_aceptada_fondeo,DS.in_tasa_aceptada)) as tasaAceptadaIf "+		
							//",DS.in_importe_interes as importeInteres " +
							" , (decode (DS.cs_opera_fiso,'N','N/A','S',DS.in_importe_interes)) as importeInteres "+		
							" , (decode (DS.cs_opera_fiso,'S',DS.in_importe_interes_fondeo,DS.in_importe_interes)) as importeInteresIf "+									   
							//",DS.in_importe_recibir as importeRecibir "+
							" , (decode (DS.cs_opera_fiso,'N','N/A','S',DS.in_importe_recibir)) as importeRecibir "+
							" , (decode (DS.cs_opera_fiso,'S',DS.in_importe_recibir_fondeo,DS.in_importe_recibir)) as importeRecibirIf "+		   
							
							",(DS.in_importe_recibir - DS.fn_importe_recibir_benef) as netoRecibir ");				
	
		sbQuery.append(",substr(S.ic_folio,1,10)||'-'||substr(S.ic_folio,11,1) as numeroSolicitud ");
	
		sbQuery.append(", TF.CG_NOMBRE AS NOMBRE_TIPO_FACTORAJE"); // I.A. -- FODEA 042 - Agosto/2009
		
		sbQuery.append(", 'Query:: RepCEstatusNafinDEbean.java'");
	//FROM
		sbQuery.append(" FROM com_documento D, com_docto_seleccionado DS, ");
		sbQuery.append(" com_solicitud S, ");
		sbQuery.append(" comhis_cambio_estatus CE, comcat_pyme P, comcat_epo E, ");
		sbQuery.append(" comcat_if I, comcat_if I2, comcat_if I3, comcat_if I4, ");
		sbQuery.append(" comcat_moneda M ");
			
		sbQuery.append(" , COMCAT_TIPO_FACTORAJE TF "); // I.A. -- FODEA 042 - Agosto/2009
			
		// Armado de las CONDICIONES. 
		sbQuery.append(" WHERE D.ic_pyme = P.ic_pyme "+
						   " AND D.ic_epo = E.ic_epo " +
						   " AND D.ic_moneda = M.ic_moneda " +
						   " AND D.ic_documento = CE.ic_documento ");	
	
		sbQuery.append(" AND D.ic_if = I.ic_if "+
						   " AND D.ic_documento = DS.ic_documento ");
			
		sbQuery.append(" AND DS.ic_documento = S.ic_documento ");
	
		sbQuery.append(" AND D.ic_beneficiario = I2.ic_if(+) "+
							" AND D.ic_if = I3.ic_if (+) "+
							" AND DS.ic_if = I4.ic_if (+) ");
		
		sbQuery.append(" AND D.CS_DSCTO_ESPECIAL = TF.CC_TIPO_FACTORAJE "); // I.A. -- FODEA 042 - Agosto/2009
			
		if(tipoBusqueda.equals("N") ){
			sbQuery.append("AND DS.cs_opera_fiso = 'N' ");
		}else if(tipoBusqueda.equals("F") ){
			sbQuery.append("AND DS.cs_opera_fiso = 'S' ");
		}
		
		sbQuery.append(
				" AND TO_CHAR(CE.dc_fecha_cambio,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') " +
				" AND CE.ic_cambio_estatus = ? " );
		
		conditions.add("7");
		log.info("query: "+sbQuery.toString());
		registros = con.consultarDB(sbQuery.toString(),conditions);
		
		return registros;
		}catch(Exception e){
				//log.error("Error m�todo getTotales2: ",e);
				throw new AppException("Error", e);
		} finally{
				//log.info("getTotales2(S)");	
				if(con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}						
	}
	
	
	public String getStringQueryPorCambioEstatus7() {
		StringBuffer sbQuery = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		//List 		conditions = new ArrayList();
	
		hint.append("/*+ use_nl(d ds ce p e i i2 m tf) index(ce) */ ");
		sbQuery.append(" SELECT "+hint+" (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) as nombreEPO ");
		sbQuery.append(" ,(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) as nombrePYME "+
							" , (decode (I.cs_habilitado,'N','*','S',' ')||' '||(decode(DS.cs_opera_fiso,'N',I.cg_razon_social,'S',I3.cg_razon_social))) as nombreIF "+	
							" , (decode (I.cs_habilitado,'N','*','S',' ')||' '||(decode(DS.cs_opera_fiso,'N','N/A','S',I4.cg_razon_social))) as nombreFideicomiso ");
	
		sbQuery.append(",D.cc_acuse as numeroAcuse ");

		sbQuery.append(",D.ig_numero_docto as numeroDocto "+
						   ",M.cd_nombre as nombreMoneda "+
						   ",M.ic_moneda as claveMoneda ");
					
		//sbQuery.append(",D.cs_dscto_especial as tipoFactoraje " +
		sbQuery.append(",D.cs_dscto_especial as tipoFactoraje " +
						   ",D.fn_monto as montoDocto "+
						   ",I2.cg_razon_social as beneficiario " +
						   ",D.fn_porc_beneficiario as porcBeneficiario ");
							
		sbQuery.append(",DS.fn_importe_recibir_benef as importeARecibirBeneficiario " +
						   ",D.fn_porc_anticipo as porcentaje " +
						   ",D.fn_monto_dscto as montoDscto "+
						   //",DS.in_tasa_aceptada as tasaAceptada "+
						   " , (decode (DS.cs_opera_fiso,'N','N/A','S',DS.in_tasa_aceptada)) as tasaAceptada "+
							" , (decode (DS.cs_opera_fiso,'S',DS.in_tasa_aceptada_fondeo,DS.in_tasa_aceptada)) as tasaAceptadaIf "+		
							//",DS.in_importe_interes as importeInteres " +
							" , (decode (DS.cs_opera_fiso,'N','N/A','S',DS.in_importe_interes)) as importeInteres "+		
							" , (decode (DS.cs_opera_fiso,'S',DS.in_importe_interes_fondeo,DS.in_importe_interes)) as importeInteresIf "+									   
							//",DS.in_importe_recibir as importeRecibir "+
							" , (decode (DS.cs_opera_fiso,'N','N/A','S',DS.in_importe_recibir)) as importeRecibir "+
							" , (decode (DS.cs_opera_fiso,'S',DS.in_importe_recibir_fondeo,DS.in_importe_recibir)) as importeRecibirIf "+		   
							
							",(DS.in_importe_recibir - DS.fn_importe_recibir_benef) as netoRecibir ");				
	
		sbQuery.append(",substr(S.ic_folio,1,10)||'-'||substr(S.ic_folio,11,1) as numeroSolicitud ");
	
		sbQuery.append(", TF.CG_NOMBRE AS NOMBRE_TIPO_FACTORAJE"); // I.A. -- FODEA 042 - Agosto/2009
		
		sbQuery.append(", 'Query:: RepCEstatusNafinDEbean.java'");
	//FROM
		sbQuery.append(" FROM com_documento D, com_docto_seleccionado DS, ");
		sbQuery.append(" com_solicitud S, ");
		sbQuery.append(" comhis_cambio_estatus CE, comcat_pyme P, comcat_epo E, ");
		sbQuery.append(" comcat_if I, comcat_if I2, comcat_if I3, comcat_if I4, ");
		sbQuery.append(" comcat_moneda M ");
			
		sbQuery.append(" , COMCAT_TIPO_FACTORAJE TF "); // I.A. -- FODEA 042 - Agosto/2009
			
		// Armado de las CONDICIONES. 
		sbQuery.append(" WHERE D.ic_pyme = P.ic_pyme "+
						   " AND D.ic_epo = E.ic_epo " +
						   " AND D.ic_moneda = M.ic_moneda " +
						   " AND D.ic_documento = CE.ic_documento ");	
	
		sbQuery.append(" AND D.ic_if = I.ic_if "+
						   " AND D.ic_documento = DS.ic_documento ");
			
		sbQuery.append(" AND DS.ic_documento = S.ic_documento ");
	
		sbQuery.append(" AND D.ic_beneficiario = I2.ic_if(+) "+
							" AND D.ic_if = I3.ic_if (+) "+
							" AND DS.ic_if = I4.ic_if (+) ");
		
		sbQuery.append(" AND D.CS_DSCTO_ESPECIAL = TF.CC_TIPO_FACTORAJE "); // I.A. -- FODEA 042 - Agosto/2009
			
		if(tipoBusqueda.equals("N") ){
			sbQuery.append("AND DS.cs_opera_fiso = 'N' ");
		}else if(tipoBusqueda.equals("F") ){
			sbQuery.append("AND DS.cs_opera_fiso = 'S' ");
		}
		
		sbQuery.append(
				" AND TO_CHAR(CE.dc_fecha_cambio,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') " +
				" AND CE.ic_cambio_estatus = 7 " );

		return sbQuery.toString();
	
	}
	
	
	public String getQueryPorCambioEstatus() {
	StringBuffer sbQuery = new StringBuffer();
	StringBuffer hint = new StringBuffer();
	
	hint.append("/*+ use_nl(d ds ce p e i i2 m tf) index(ce) */ ");
		 
		// Armado de los CAMPOS.
			sbQuery.append(" SELECT "+hint+" (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) as nombreEPO ");
		if(iNoCambioEstatus==2 || iNoCambioEstatus==7 || iNoCambioEstatus==8 || iNoCambioEstatus==29 || iNoCambioEstatus==32 ) 
			sbQuery.append(",(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) as nombrePYME "+
						   ",(decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as nombreIF ");
		
		if(iNoCambioEstatus==40)
			sbQuery.append(",(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) as nombrePYME ");
			
		if(iNoCambioEstatus==2 || iNoCambioEstatus==7 || iNoCambioEstatus==28 || iNoCambioEstatus==29 || iNoCambioEstatus==32 || iNoCambioEstatus==40)
			sbQuery.append(",D.cc_acuse as numeroAcuse ");
		else if(iNoCambioEstatus == 8)
			sbQuery.append(",CE.cc_acuse as numeroAcuse ");
			
			sbQuery.append(",D.ig_numero_docto as numeroDocto "+
						   ",M.cd_nombre as nombreMoneda "+
						   ",M.ic_moneda as claveMoneda ");
			
		if(iNoCambioEstatus==2 || iNoCambioEstatus==7 || iNoCambioEstatus==8 || iNoCambioEstatus==29 || iNoCambioEstatus==32 || iNoCambioEstatus==40)
			sbQuery.append(",D.cs_dscto_especial as tipoFactoraje " +
						   ",D.fn_monto as montoDocto "+
						   ",I2.cg_razon_social as beneficiario " +
						   ",D.fn_porc_beneficiario as porcBeneficiario ");
		else if(iNoCambioEstatus == 28 || iNoCambioEstatus==29)
			sbQuery.append(",'Normal' as tipoFactoraje ");
		
		if(iNoCambioEstatus==40){
			sbQuery.append(", (d.fn_porc_beneficiario * d.fn_monto) / 100 AS importe_a_recibir "+
								", TO_CHAR (d.df_fecha_venc_pyme, 'DD/MM/YYYY') AS df_fecha_venc_pyme "+
								",D.fn_porc_anticipo as porcentaje "+
								",D.fn_monto_dscto as montoDscto ");
		}
		
		if(iNoCambioEstatus==2 || iNoCambioEstatus==7 || iNoCambioEstatus==29 || iNoCambioEstatus==32 )
			sbQuery.append(",DS.fn_importe_recibir_benef as importeARecibirBeneficiario " +
						   ",D.fn_porc_anticipo as porcentaje " +
						   ",D.fn_monto_dscto as montoDscto "+
						   ",DS.in_tasa_aceptada as tasaAceptada "+
						   ",DS.in_importe_interes as importeInteres " +
						   ",DS.in_importe_recibir as importeRecibir "+
						   ",(DS.in_importe_recibir - DS.fn_importe_recibir_benef) as netoRecibir ");
		else if(iNoCambioEstatus==8)
			sbQuery.append(",DECODE (D.ic_estatus_docto, 3, DS.fn_importe_recibir_benef, " +
						   " 4, DS.fn_importe_recibir_benef, 11, DS.fn_importe_recibir_benef, " +
						   " 12, DS.fn_importe_recibir_benef, 16, DS.fn_importe_recibir_benef,  " +
						   " ((D.fn_porc_beneficiario * D.fn_monto) / 100)) AS importeARecibirBeneficiario " +
						   ",decode(D.ic_moneda, 1, cv.fn_aforo, 54, cv.fn_aforo_dl) as porcentaje "+
						   ",(decode(D.ic_moneda, 1, cv.fn_aforo, 54, cv.fn_aforo_dl) /100) * D.fn_monto as montoDscto ");
		
		if(iNoCambioEstatus==8 || iNoCambioEstatus==28 || iNoCambioEstatus==29 || iNoCambioEstatus==40) {
			sbQuery.append(",TO_CHAR(D.df_fecha_docto,'DD/MM/YYYY') as fechaDocumento "+
						   ",TO_CHAR(D.df_fecha_venc,'DD/MM/YYYY') as fechaVencimiento "+
						   ",CE.fn_monto_anterior as montoAnterior "+
						   ",CE.ct_cambio_motivo as causa ");
		}
		if(iNoCambioEstatus==7)
			sbQuery.append(",substr(S.ic_folio,1,10)||'-'||substr(S.ic_folio,11,1) as numeroSolicitud ");
		if(iNoCambioEstatus==8)
			sbQuery.append(",TO_CHAR(CE.df_fecha_venc_anterior,'DD/MM/YYYY') as fechaVencAnt ");
		
		if(iNoCambioEstatus==28){
			sbQuery.append(",CE.fn_monto_nuevo as montoNuevo ");
		}
		
			sbQuery.append(", TF.CG_NOMBRE AS NOMBRE_TIPO_FACTORAJE"); // I.A. -- FODEA 042 - Agosto/2009
		
		  sbQuery.append(", 'Query:: RepCEstatusNafinDEbean.java'");
		
		// Armado de las TABLAS
//			sbQuery.append(" FROM com_documento D, com_docto_seleccionado DS, comrel_producto_epo pe, ");
			
		if (iNoCambioEstatus==40)
			sbQuery.append(" FROM com_documento D, ");
		else
			sbQuery.append(" FROM com_documento D, com_docto_seleccionado DS, ");
			
		if(iNoCambioEstatus==7)
			sbQuery.append(" com_solicitud S, ");
		if(iNoCambioEstatus==8)
			sbQuery.append(" comvis_aforo CV, ");
			
			sbQuery.append(" comhis_cambio_estatus CE, comcat_pyme P, comcat_epo E, ");
			
		if(iNoCambioEstatus==2 || iNoCambioEstatus==7 || iNoCambioEstatus==8 || iNoCambioEstatus==29 || iNoCambioEstatus==32 )
			sbQuery.append(" comcat_if I, comcat_if I2, ");
		
		if( iNoCambioEstatus==40)
			sbQuery.append(" comcat_if I2, ");
			
			sbQuery.append(" comcat_moneda M ");
			
			sbQuery.append(" , COMCAT_TIPO_FACTORAJE TF "); // I.A. -- FODEA 042 - Agosto/2009
			
		// Armado de las CONDICIONES. 
			sbQuery.append(" WHERE D.ic_pyme = P.ic_pyme "+
						   " AND D.ic_epo = E.ic_epo " +
						   " AND D.ic_moneda = M.ic_moneda " +
						   " AND D.ic_documento = CE.ic_documento ");
		if(iNoCambioEstatus==2 || iNoCambioEstatus==7 || iNoCambioEstatus==29 || iNoCambioEstatus==32)
			sbQuery.append(" AND D.ic_if = I.ic_if "+
						   " AND D.ic_documento = DS.ic_documento ");
			
		if(iNoCambioEstatus==7)
			sbQuery.append(" AND DS.ic_documento = S.ic_documento ");
		if(iNoCambioEstatus==8)
			sbQuery.append(" AND D.ic_epo = CV.ic_epo "+
						   " AND D.ic_if = I.ic_if(+) ");
		if(iNoCambioEstatus==8 || iNoCambioEstatus==28)
			sbQuery.append(" AND D.ic_documento = DS.ic_documento(+) ");
		
		if(iNoCambioEstatus==2 || iNoCambioEstatus==7 || iNoCambioEstatus==8 || iNoCambioEstatus==29 || iNoCambioEstatus==32 || iNoCambioEstatus==40)
			sbQuery.append(" AND D.ic_beneficiario = I2.ic_if(+) ");
		
			sbQuery.append("AND D.CS_DSCTO_ESPECIAL = TF.CC_TIPO_FACTORAJE "); // I.A. -- FODEA 042 - Agosto/2009
			
		sbQuery.append(
				" AND TO_CHAR(CE.dc_fecha_cambio,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY') " +
				" AND CE.ic_cambio_estatus = " + iNoCambioEstatus
//				" AND pe.ic_producto_nafin = 1 AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) "
//				" AND pe.ic_producto_nafin = 1 "
				);
						   
		// Armado del ORDEN.
		if(iNoCambioEstatus==28)
			sbQuery.append("ORDER BY D.ig_numero_docto, CE.dc_fecha_cambio");
		
    log.info("sbQuery::: "+sbQuery);
    
		return sbQuery.toString();
	}
	

	/**
	* Obtiene el query para obtener la consulta SIN PAGINACION
	* @return Cadena con la consulta de SQL, para obtener la consulta
	*/
	public String getDocumentQueryFile() {
		
		log.info("getDocumentQueryFile(E) :::...");
		
		qrySentencia 	= new StringBuffer();
		String query 	= "";
		conditions 		= new ArrayList();		//binds
		
		//RepCEstatusNafinDEbean consulta = new RepCEstatusNafinDEbean();
		//consulta.setNoCambioEstatus(iNoCambioEstatus);
		
		//conditions.add(String.valueOf(iNoCambioEstatus));
		if(iNoCambioEstatus != 7)
			query = getQueryPorCambioEstatus();
		else
			query = getStringQueryPorCambioEstatus7();
		log.info("getDocumentQueryFile :::...  "+query);  // --???
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQueryFile(S)");
		return query;	
	}
	



	
/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) SIN paginador*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
				
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
				int iNoMoneda = 0;
				double nacional = 0;
				double nacional_desc = 0;
				double nacional_int = 0;
				double nacional_descIF = 0;
				double dolares = 0;
				double dolares_desc = 0;
				double dolares_int = 0;
				double dolares_descIF = 0;
				double dMontoDscto = 0;
				double dImporteInteres = 0;
				double dImporteRecibir = 0;
				double dTasaAceptada = 0;
				double dMontoDocto = 0;
				double dPorcentaje = 0;
				double dblRecurso = 0;
				String sNombrePYME = "";
				String sNombreEPO = "";
				String sNombreIF = "";
				String sNoDocumento = "";
				String sAcuse = "";
				String sNombreMoneda = "";
				String sTipoFactoraje = "";
				String nombreTipoFactoraje = "";
				String sNoSolicitud = "";
				String sNetoRecibir = "";
				String sBeneficiario = "";
				String sPorcBeneficiario = "";
				String sImpRecibirBenef = "";
				boolean dolaresDocto = false;
				boolean nacionalDocto = false;
			  // Declaraci�n e inicializaci�n de variables
				double total_mn = 0.0;
				double total_dolares = 0.0;
				double dblTotMontoDescuentoMN = 0.0;
				double dblTotMontoDescuentoDL = 0.0;
				dMontoDscto = 0;
				double dblTotRecursoMN = 0.0;
				double dblTotRecursoDL = 0.0;
				double df_total_ant_mn = 0.0;
				double df_total_ant_dol = 0.0;
				String MontoAnterior = "";
				String sFechaDocto = "";
				String sFechaVto = "";
				String sCausa = "";
				
//		Seleccionada PYME a Negociable		
				if(iNoCambioEstatus == 2)  {
					pdfDoc.setTable(18, 100); //numero de columnas y el ancho que ocupara la tabla en el documento

				pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
				pdfDoc.setCell("Seleccionada PYME a Negociable","formas",ComunesPDF.LEFT,17);	
				pdfDoc.setCell("","formas",ComunesPDF.LEFT,18);	

					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto int.","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Operar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero Acuse IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Neto a Recibir PYME","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					
					while (rs.next()) 
					{
						sNombrePYME 			= (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreEPO 				= (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						sNombreIF 				= (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sNoDocumento 			= (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sNombreMoneda 			= (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje			= (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje").trim();
						nombreTipoFactoraje  = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto 			= rs.getDouble("montoDocto");
						dPorcentaje 			= rs.getDouble("porcentaje");
						dblRecurso 				= dMontoDocto - dMontoDscto;
						dMontoDscto 			= rs.getDouble("montoDscto");
						dTasaAceptada 			= rs.getDouble("tasaAceptada");
						dImporteInteres 		= rs.getDouble("importeInteres");
						dImporteRecibir 		= rs.getDouble("importeRecibir");
						sAcuse 					= (rs.getString("numeroAcuse")==null)?"":rs.getString("numeroAcuse").trim();
						sNetoRecibir 			= (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?"$ "+Comunes.formatoDecimal(rs.getString("netoRecibir"), 2):"";
						sBeneficiario 			= (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?rs.getString("beneficiario"):"";
						sPorcBeneficiario 	= (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?rs.getString("porcBeneficiario")+" %":"";
						sImpRecibirBenef 		= (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?"$ "+Comunes.formatoDecimal(rs.getString("importeARecibirBeneficiario"), 2):"";
						iNoMoneda 				= rs.getInt("claveMoneda");
	
						if(sTipoFactoraje.equals("V") || sTipoFactoraje.equals("D") || sTipoFactoraje.equals("I")) {
							  dMontoDscto = dMontoDocto;
							  dPorcentaje = 100;
						  }
				
						if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacional_int += dImporteInteres;
							nacional_descIF += dImporteRecibir;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolares_int += dImporteInteres;
							dolares_descIF += dImporteRecibir;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,0)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblRecurso,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(Comunes.formatoDecimal(dTasaAceptada,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteInteres,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteRecibir,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sAcuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNetoRecibir,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(sBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sPorcBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sImpRecibirBenef,"formas",ComunesPDF.RIGHT);
						
					}	//while
					/*
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_descIF,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_descIF,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}
					*/
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
				// Operada - Operada Pendiente de pago				
				else if(iNoCambioEstatus == 7)  { 
										
					
					pdfDoc.setTable(13, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
					
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Operada - Operada Pendiente de pago","formas",ComunesPDF.LEFT,12);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,13);	

					pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("N�mero Solicitud","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa IF","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto int. IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Operar IF","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Nombre Fideicomiso","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa Fideicomiso","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto int. Fideicomiso","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Operar Fideicomiso","celda01",ComunesPDF.CENTER);					
					
					pdfDoc.setCell("N�mero Acuse IF","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Neto a Recibir PYME","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					
					while (rs.next()) 
					{
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						String sNombreFideicomiso = (rs.getString("nombreFideicomiso")==null)?"":rs.getString("nombreFideicomiso").trim(); 
						sNombrePYME = (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreIF = (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje");
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto = rs.getDouble("montoDocto");
						dPorcentaje = rs.getDouble("porcentaje");
						dblRecurso = dMontoDocto - dMontoDscto;
						dMontoDscto = rs.getDouble("montoDscto");
						
						String dTasaAceptadaF = rs.getString("tasaAceptada");
						String dImporteInteresF = rs.getString("importeInteres");
						String dImporteRecibirF = rs.getString("importeRecibir");
						
						dTasaAceptada = rs.getDouble("tasaAceptadaIf");
						dImporteInteres = rs.getDouble("importeInteresIf");
						dImporteRecibir = rs.getDouble("importeRecibirIf");
								
						
						
						sAcuse = (rs.getString("numeroAcuse")==null)?"":rs.getString("numeroAcuse").trim();
						sNetoRecibir = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ? "$ "+Comunes.formatoDecimal(rs.getString("netoRecibir"), 2):"";
						sBeneficiario = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ? rs.getString("beneficiario"):"";
						sPorcBeneficiario = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ? rs.getString("porcBeneficiario")+" %":"";
						sImpRecibirBenef = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ? "$ "+Comunes.formatoDecimal(rs.getString("importeARecibirBeneficiario"), 2):"";
						sNoSolicitud = (rs.getString("numeroSolicitud")==null)?"":rs.getString("numeroSolicitud").trim();
						iNoMoneda = rs.getInt("claveMoneda");
						// Para factoraje Vencido y Distribuido.
						if(sTipoFactoraje.equals("V") || sTipoFactoraje.equals("D") || sTipoFactoraje.equals("I")) {
							dMontoDscto = dMontoDocto;
							dPorcentaje = 100;
						}
				
						  if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacional_int += dImporteInteres;
							nacional_descIF += dImporteRecibir;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolares_int += dImporteInteres;
							dolares_descIF += dImporteRecibir;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
											
						pdfDoc.setCell(sNoSolicitud,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblRecurso,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);	
						pdfDoc.setCell(Comunes.formatoDecimal(dTasaAceptada,2)+"%" ,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteInteres,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteRecibir,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(sNombreFideicomiso,"formas",ComunesPDF.LEFT);	
						
						if(dTasaAceptadaF.equals("N/A")){
							pdfDoc.setCell(dTasaAceptadaF,"formas",ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dTasaAceptadaF,2),"formas",ComunesPDF.RIGHT);						
						}						
						
						//pdfDoc.setCell(Comunes.formatoDecimal(dTasaAceptadaF,2)+"%" ,"formas",ComunesPDF.CENTER);
						
						if(dImporteInteresF.equals("N/A")){
							pdfDoc.setCell(dImporteInteresF,"formas",ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteInteresF,2),"formas",ComunesPDF.RIGHT);						
						}						
						
						//pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteInteresF,2),"formas",ComunesPDF.RIGHT);
						
						if(dImporteRecibirF.equals("N/A")){
							pdfDoc.setCell(dImporteRecibirF,"formas",ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteRecibirF,2),"formas",ComunesPDF.RIGHT);
						}
						//pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteRecibirF,2) ,"formas",ComunesPDF.RIGHT);						
						
						pdfDoc.setCell(sAcuse,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sNetoRecibir,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sPorcBeneficiario,"formas",ComunesPDF.CENTER);
						//pdfDoc.setCell(Comunes.formatoDecimal(sPorcBeneficiario,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sImpRecibirBenef,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.RIGHT);
					}	//while
					/*
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,7);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_descIF,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,10);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,7);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_descIF,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,10);
					}
					*/
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
// Modificaci�n de Montos y/o Fechas de Vencimiento
				else if(iNoCambioEstatus == 8)  {
					pdfDoc.setTable(19, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
					
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Modificaci�n de Montos y/o Fechas de Vencimiento","formas",ComunesPDF.LEFT,18);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,19);	

					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Documento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Anterior","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Fecha de Vencimiento Anterior","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Acuse","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Causa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					
					sNombreEPO = "";
					sNombrePYME = "";
					sNombreIF = "";
					sNoDocumento = "";
					sAcuse = "";
					sNombreMoneda = "";
					iNoMoneda = 0;
					dMontoDocto = 0;
					dolaresDocto = false;
					nacionalDocto = false;
					dPorcentaje = 0;
					dblRecurso = 0;
					nacional_desc = 0;
					String fechaVencAnt = "";

					while (rs.next()) 
					{
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						sNombrePYME = (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreIF = (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sFechaDocto = (rs.getString("fechaDocumento")==null)?"":rs.getString("fechaDocumento").trim();
						sFechaVto = (rs.getString("fechaVencimiento")==null)?"":rs.getString("fechaVencimiento").trim();
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje");
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto = rs.getDouble("montoDocto");
						MontoAnterior = (rs.getString("montoAnterior")==null)?"":rs.getString("montoAnterior").trim();
						fechaVencAnt = (rs.getString("fechaVencAnt")==null)?"":rs.getString("fechaVencAnt");
						dPorcentaje = rs.getDouble("porcentaje");	// Aforo.
						dblRecurso = dMontoDocto - dMontoDscto;
						dMontoDscto = rs.getDouble("montoDscto");
						sAcuse = (rs.getString("numeroAcuse")==null)?"":rs.getString("numeroAcuse").trim();
						sCausa = (rs.getString("causa")==null)?"":rs.getString("causa").trim();
						sBeneficiario = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?rs.getString("beneficiario"):"";
						sPorcBeneficiario = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?rs.getString("porcBeneficiario")+" %":"";
						sImpRecibirBenef = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?"$ "+Comunes.formatoDecimal(rs.getString("importeARecibirBeneficiario"), 2):"";
						iNoMoneda = rs.getInt("claveMoneda");
						// Para factoraje Vencido y Distribuido.
						if(sTipoFactoraje.equals("V") || sTipoFactoraje.equals("D") || sTipoFactoraje.equals("I")) {
							dMontoDscto = dMontoDocto;
							dPorcentaje = 100;
						}

						if (iNoMoneda == 1) {
							total_mn += dMontoDocto;
							dblTotMontoDescuentoMN += dMontoDscto;
							dblTotRecursoMN += dblRecurso;
							nacionalDocto = true;
							df_total_ant_mn += rs.getDouble("MontoAnterior");
						}
						if (iNoMoneda == 54) {
							total_dolares += dMontoDocto;
							dblTotMontoDescuentoDL += dMontoDscto;
							dblTotRecursoDL += dblRecurso;
							dolaresDocto = true;
							df_total_ant_dol += rs.getDouble("MontoAnterior");
						}
						
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sFechaDocto,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sFechaVto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(MontoAnterior,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(fechaVencAnt,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblRecurso,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sAcuse,"formas",ComunesPDF.CENTER);
												
						pdfDoc.setCell(sCausa,"formas",ComunesPDF.CENTER);						
						pdfDoc.setCell(sBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sPorcBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sImpRecibirBenef,"formas",ComunesPDF.RIGHT);
						
					}	//while
					/*
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(total_mn,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(df_total_ant_mn,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotRecursoMN,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotMontoDescuentoMN,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(total_dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(df_total_ant_dol,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotRecursoDL,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotMontoDescuentoDL,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}
					*/
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
// Aplicaci�n de Nota de Cr�dito
				else if(iNoCambioEstatus == 28)  {
					pdfDoc.setTable(10, 100); //numero de columnas y el ancho que ocupara la tabla en el documento

					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Aplicaci�n de Nota de Cr�dito","formas",ComunesPDF.LEFT,9);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,10);	

					pdfDoc.setCell("Nombre de la EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Acuse","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Anterior","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Nuevo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Causa","celda01",ComunesPDF.CENTER);
					
					double dMontoNuevo = 0, dMontoAnterior = 0;
					sNombreEPO = "";
					total_dolares = 0;
					total_mn = 0;
					dMontoDscto = 0;
					double dblTotalMontoPesos = 0;
					double dblTotalMontoDolares = 0;
					dolaresDocto = false;
					nacionalDocto = false;
					while (rs.next()) 
					{
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO");
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto");
						sAcuse = (rs.getString("numeroAcuse")==null)?"":rs.getString("numeroAcuse");
						sFechaDocto = (rs.getString("fechaDocumento")==null)?"":rs.getString("fechaDocumento");
						sFechaVto = (rs.getString("fechaVencimiento")==null)?"":rs.getString("fechaVencimiento");
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje");
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda");
						dMontoAnterior = rs.getDouble("montoAnterior");
						dMontoNuevo = rs.getDouble("montoNuevo");
						sCausa = (rs.getString("causa")==null)?"":rs.getString("causa");
						iNoMoneda = rs.getInt("claveMoneda");
				
						if (iNoMoneda == 1) {
							df_total_ant_mn += dMontoAnterior;
							total_mn += dMontoNuevo;
							dblTotalMontoPesos += dMontoDscto;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							df_total_ant_dol += dMontoAnterior;
							total_dolares += dMontoNuevo;
							dblTotalMontoDolares += dMontoDscto;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sAcuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sFechaDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sFechaVto,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoAnterior,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoNuevo,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sCausa,"formas",ComunesPDF.CENTER);
						
					}	//while
					/*
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,7);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(df_total_ant_mn,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(total_mn,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,7);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(df_total_ant_dol,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(total_dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					*/
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
//Programado a Negociable				
				else if(iNoCambioEstatus == 29)  {
					pdfDoc.setTable(13, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
	
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Programado a Negociable","formas",ComunesPDF.LEFT,12);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,13);	

					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero Acuse IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Causa","celda01",ComunesPDF.CENTER);
					
					while (rs.next()) 
					{
						sNombrePYME = (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						sNombreIF = (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sFechaDocto = (rs.getString("fechaDocumento")==null)?"":rs.getString("fechaDocumento");
						sFechaVto = (rs.getString("fechaVencimiento")==null)?"":rs.getString("fechaVencimiento");
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje").trim();
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto = rs.getDouble("montoDocto");
						dPorcentaje = rs.getDouble("porcentaje");
						dMontoDscto = rs.getDouble("montoDscto");
						sAcuse = (rs.getString("numeroAcuse")==null)?"":rs.getString("numeroAcuse").trim();
						sCausa = (rs.getString("causa")==null)?"":rs.getString("causa");
						iNoMoneda = rs.getInt("claveMoneda");
						
						// Para Factoraje Vencido y Distribuido.
						  if(sTipoFactoraje.equals("V") || sTipoFactoraje.equals("D") || sTipoFactoraje.equals("I")) {
							  dMontoDscto = dMontoDocto;
							  dPorcentaje = 100;
						  }
				
						if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sFechaDocto,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sFechaVto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,0)+"%" ,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sAcuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sCausa,"formas",ComunesPDF.CENTER);
						
					}	//while
					/*
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
					}
					*/
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
//Programado Pyme a Seleccionado Pyme				
				else if(iNoCambioEstatus == 32)  {
					pdfDoc.setTable(16, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
	
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Programado Pyme a Seleccionado Pyme","formas",ComunesPDF.LEFT,15);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,16);	

					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto int.","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Neto a Recibir PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					
					while (rs.next()) 
					{
						sNombrePYME = (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						sNombreIF = (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje");
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto = rs.getDouble("montoDocto");
						dPorcentaje = rs.getDouble("porcentaje");
						dblRecurso = dMontoDocto - dMontoDscto;
						dMontoDscto = rs.getDouble("montoDscto");
						dTasaAceptada = rs.getDouble("tasaAceptada");
						dImporteInteres = rs.getDouble("importeInteres");
						dImporteRecibir = rs.getDouble("IMPORTERECIBIR");
						sNetoRecibir = (sTipoFactoraje.equals("D"))  ? "$ "+Comunes.formatoDecimal(rs.getString("netoRecibir"), 2):"";
						sBeneficiario = (sTipoFactoraje.equals("D"))  ? rs.getString("beneficiario"):"";
						sPorcBeneficiario = (sTipoFactoraje.equals("D"))  ? rs.getString("porcBeneficiario")+" %":"";
						sImpRecibirBenef = (sTipoFactoraje.equals("D"))  ? "$ "+Comunes.formatoDecimal(rs.getString("importeARecibirBeneficiario"), 2):"";
						iNoMoneda = rs.getInt("claveMoneda");


					   if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacional_int += dImporteInteres;
							nacional_descIF += dImporteRecibir;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolares_int += dImporteInteres;
							dolares_descIF += dImporteRecibir;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblRecurso,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(Comunes.formatoDecimal(dTasaAceptada,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteInteres,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sNetoRecibir,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sPorcBeneficiario,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sImpRecibirBenef,"formas",ComunesPDF.RIGHT);
						
					}	//while
					/*
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
					}
					*/
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
// Pre Negociable a Negociable				
				else if(iNoCambioEstatus == 40)  {
					pdfDoc.setTable(14, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Pre Negociable a Negociable","formas",ComunesPDF.LEFT,13);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,14);	

					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					
					while (rs.next()) 
					{
						sNombrePYME = (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						//sNombreIF = (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sFechaDocto = (rs.getString("fechaDocumento")==null)?"":rs.getString("fechaDocumento");
						sFechaVto = (rs.getString("fechaVencimiento")==null)?"":rs.getString("fechaVencimiento");
						
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje");
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto = rs.getDouble("montoDocto");
						dPorcentaje = rs.getDouble("porcentaje");
						dblRecurso = dMontoDocto - dMontoDscto;
						dMontoDscto = rs.getDouble("montoDscto");
						/*dTasaAceptada = rs.getDouble("tasaAceptada");
						dImporteInteres = rs.getDouble("importeInteres");
						dImporteRecibir = rs.getDouble("importeRecibir");
						*/
						sNetoRecibir = (sTipoFactoraje.equals("D"))  ? "$ "+Comunes.formatoDecimal(rs.getString("netoRecibir"), 2):"";
						sBeneficiario = (sTipoFactoraje.equals("D"))  ? rs.getString("beneficiario"):"";
						sPorcBeneficiario = (sTipoFactoraje.equals("D"))  ? rs.getString("porcBeneficiario")+" %":"";
						sImpRecibirBenef = (sTipoFactoraje.equals("D"))  ? "$ "+Comunes.formatoDecimal(rs.getString("importeARecibirBeneficiario"), 2):"";
						iNoMoneda = rs.getInt("claveMoneda");
				
				
					   if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacional_int += dImporteInteres;
							nacional_descIF += dImporteRecibir;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolares_int += dImporteInteres;
							dolares_descIF += dImporteRecibir;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sFechaDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sFechaVto,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,2)+"%" ,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sPorcBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sImpRecibirBenef,"formas",ComunesPDF.RIGHT);
						
					}	//while
					/*
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,3);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,3);
					}
					*/
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
				
				
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		} // if (tipo)	
		return  nombreArchivo;
	}	
	
	

	
	/******************** SETTER'S ********************/
	 
	public void setNoCambioEstatus (int iNoCambioEstatus ) { this.iNoCambioEstatus = iNoCambioEstatus; }
	
	/******************** GETTER'S ********************/	
	
	public List getConditions() {  return conditions;  } 


//Setter
	public void setTipoBusqueda(String tipoBusqueda){
		this.tipoBusqueda = tipoBusqueda;
	}
	
}