package com.netro.descuento;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsDoctosPreNegdDet implements IQueryGeneratorPS {


	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsDoctosPendDet.class);
	private int numList = 1;
	private ArrayList valoresBind = new ArrayList();
	
	public ConsDoctosPreNegdDet() {}
  
	public String getAggregateCalculationQuery(HttpServletRequest request){
		log.info("getAggregateCalculationQuery(E)");
		String querySentencia= "";		
		log.debug(" valoresBind = " +this.valoresBind);
		log.info("getAggregateCalculationQuery(S)");
		return querySentencia;
	}

	public int getNumList(HttpServletRequest request){
		return numList;	
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		numList = 1;
		
		String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 
		
		StringBuffer query = new StringBuffer();
		StringBuffer lstdoctos = new StringBuffer();
		int coma=0,criterio=0;
		String query1=null;
		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
		


		for (Iterator it = ids.iterator(); it.hasNext();){
			it.next();
			lstdoctos.append("?,");
		}
		lstdoctos = lstdoctos.delete(lstdoctos.length()-1,lstdoctos.length());
		lstdoctos.append(")");
			
		
		query.append(
			 "SELECT   p.cg_razon_social nompyme, d.ig_numero_docto numdocto, " +
				"         TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') fecemision, " +
				"         TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') fecvenc, " +
				"         m.cd_nombre nommoneda, d.fn_monto monto, f.cg_nombre nomfactoraje, " +
				"         a.cc_acuse acuse, d.ic_estatus_docto estatus " +
				"    FROM com_documento d, " +
				"         com_acuse1 a, " +
				"         comcat_pyme p, " +
				"         comcat_tipo_factoraje f, " +
				"         comcat_moneda m " +
				"   WHERE d.cc_acuse = a.cc_acuse " +
				"     AND d.ic_pyme = p.ic_pyme " +
				"     AND d.cs_dscto_especial = f.cc_tipo_factoraje " +
				"     AND d.ic_moneda = m.ic_moneda " +
				"		AND d.ic_documento in ("+lstdoctos+
				" ORDER BY d.df_fecha_docto ");
			
		
		
		
		log.debug(" query prima: "+query.toString());
		log.debug(" valoresBind : " +ids);
		log.info("getDocumentSummaryQueryForIds(S)");
		return query.toString();

  }

// Devuelve el query de llaves primarias
	public String getDocumentQuery(HttpServletRequest request){
		log.info("getDocumentQuery(E)");
		
		this.valoresBind = new ArrayList();
		
		String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 
		
		String cc_acuse = (request.getParameter("cc_acuse") == null)?"":request.getParameter("cc_acuse");
		String num_acuse = (request.getParameter("num_acuse") == null)?"":request.getParameter("num_acuse");

		
		int coma=0,criterio = 0;
		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");

		String operado= (sesIdiomaUsuario.equals("EN"))?"Operate":"Operado";

		StringBuffer query_Reporte = new StringBuffer();
		query_Reporte.append(
				"SELECT   d.ic_documento " +
				"    FROM com_documento d, " +
				"         com_acuse1 a, " +
				"         comcat_pyme p, " +
				"         comcat_tipo_factoraje f, " +
				"         comcat_moneda m " +
				"   WHERE d.cc_acuse = a.cc_acuse " +
				"     AND d.ic_pyme = p.ic_pyme " +
				"     AND d.cs_dscto_especial = f.cc_tipo_factoraje " +
				"     AND d.ic_moneda = m.ic_moneda " +				
				//"		AND d.ic_estatus_docto in (28,29) "+
				"     AND d.ic_epo = ? " +
				"     AND a.cc_acuse = ? " +
				"ORDER BY d.df_fecha_docto ");
		
				this.valoresBind.add(new Integer(iNoCliente));
				this.valoresBind.add(num_acuse);
			
	   log.debug(" query_Reporte = "+query_Reporte.toString());
		log.debug(" = " +this.valoresBind);
		log.info("getDocumentQueryFile(E)");
		return query_Reporte.toString();  }

	public String getDocumentQueryFile(HttpServletRequest request){
		log.info("getDocumentQueryFile(E)");
		
		
		return "";
  }

	public ArrayList getConditions(HttpServletRequest request){
		return this.valoresBind;
	}
	


 

}