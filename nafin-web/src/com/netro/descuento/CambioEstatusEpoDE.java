package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorThreadRegExtJS;
import netropology.utilerias.ServiceLocator;

import netropology.utilerias.SessionFake;

import org.apache.commons.logging.Log;


public class CambioEstatusEpoDE implements IQueryGenerator ,  IQueryGeneratorThreadRegExtJS  {

	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CambioEstatusEpoDE.class);
	private String icPyme;
	private String ic_epo;
	private String igNumeroDocto;
	private String dfFechaVencMin;
	private String dfFechaVencMax;
	private String icMoneda;
	private String icCambioEstatus;
	private String fnMontoMin;
	private String fnMontoMax;
	private String dcFechaCambioMin;
	private String dcFechaCambioMax;
	private String fechaActual;
	private String txt_opera_fac_venc;
	private String strAforo;
        private String strAforoDL;
	private String idioma;
        private int countReg;
	
  public CambioEstatusEpoDE() {  }
  
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		StringBuffer qrySentencia = new StringBuffer();

		String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 

		String icPyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
		String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String igNumeroDocto = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto").trim();
		String dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
		String dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
		String icMoneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
		String icCambioEstatus = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
		String fnMontoMin = (request.getParameter("fn_montoMin") == null) ? "" : request.getParameter("fn_montoMin");
		String fnMontoMax = (request.getParameter("fn_montoMax") == null) ? "" : request.getParameter("fn_montoMax");
		String dcFechaCambioMin = (request.getParameter("dc_fecha_cambioMin") == null) ? "" : request.getParameter("dc_fecha_cambioMin");
		String dcFechaCambioMax = (request.getParameter("dc_fecha_cambioMax") == null) ? "" : request.getParameter("dc_fecha_cambioMax");
		String fechaActual = (request.getParameter("fechaActual") == null) ? "" : request.getParameter("fechaActual");

		String txt_opera_fac_venc = (request.getParameter("txt_opera_fac_venc") == null) ? "" : request.getParameter("txt_opera_fac_venc").trim();
		String strAforo = (request.getParameter("txt_strAforo") == null) ? "" : request.getParameter("txt_strAforo").trim();
		String strAforoDL = (request.getParameter("txt_strAforoDL") == null) ? "" : request.getParameter("txt_strAforoDL").trim();

		
				
		try {
			qrySentencia.append(" SELECT d.ic_moneda " +
					",m.cd_nombre" + idioma + " as cd_nombre " +
					", COUNT (1), SUM (d.fn_monto) ");
			if("V".equals(txt_opera_fac_venc))
				qrySentencia.append("        ,SUM (DECODE (cs_dscto_especial, 'V', d.fn_monto, d.fn_monto * decode(d.ic_moneda,1,"+strAforo+",54,"+strAforoDL+")))");
			else // se agrega validacion para que tome 100% del monto a descontar cuando sea Factoraje Vencimiento Infonavit - FODEA 042/2009 -
				qrySentencia.append(" ,SUM (d.fn_monto * DECODE (d.ic_moneda, 1, decode(d.cs_dscto_especial, 'I', 1, "+strAforo+"), 54,"+strAforoDL+")) ");
				//qrySentencia.append("        ,SUM (d.fn_monto * decode(d.ic_moneda,1,"+strAforo+",54,"+strAforoDL+"))");
											
			qrySentencia.append(
				"   FROM com_documento d,"   +
				"        comhis_cambio_estatus ce, "   +
				"        comcat_pyme p,"   +
				"        comcat_if i,"   +
				"        comcat_moneda m, "   +				
				"        comcat_cambio_estatus cce"   +
				"  WHERE d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_documento = ce.ic_documento"   +
				"    AND ce.ic_cambio_estatus = cce.ic_cambio_estatus"   +
				"    AND d.ic_if = i.ic_if (+) "+
				"    AND d.ic_epo = "+ic_epo);

			if (!"".equals(icPyme))
				qrySentencia.append(" AND D.ic_pyme = " + icPyme.trim());
			
			if (!"".equals(icCambioEstatus))
				qrySentencia.append(" AND CE.ic_cambio_estatus = "+icCambioEstatus.trim());
			else
				qrySentencia.append(" AND CE.ic_cambio_estatus IN (4, 5, 6, 7, 8, 16, 24, 28,29) ");
			
			if (!"".equals(icMoneda))
				qrySentencia.append(" AND D.ic_moneda = " + icMoneda.trim());
			
			if (!"".equals(igNumeroDocto))
				qrySentencia.append(" AND D.ig_numero_docto = '"+igNumeroDocto.trim()+"' ");
			
			if(!"".equals(dcFechaCambioMin)) {
				if(!"".equals(dcFechaCambioMax))
					qrySentencia.append(" AND TRUNC (ce.dc_fecha_cambio) BETWEEN TRUNC (TO_DATE ('"+dcFechaCambioMin.trim()+"', 'dd/mm/yyyy')) AND TRUNC (TO_DATE ('"+dcFechaCambioMax.trim()+"', 'dd/mm/yyyy')) ");
			}
			if(!"".equals(fnMontoMin)) {
				if(!"".equals(fnMontoMax)) {
					qrySentencia.append(" AND D.fn_monto between " + fnMontoMin.trim() + " AND "+ fnMontoMax.trim() +" ");
				} else {
					qrySentencia.append(" AND D.fn_monto = " + fnMontoMin.trim()+" ");
				}
			}
			if(!"".equals(dfFechaVencMin)) {
				if(!"".equals(dfFechaVencMax))
					qrySentencia.append( " AND TRUNC (D.df_fecha_venc) BETWEEN TRUNC (TO_DATE ('" + dfFechaVencMin + "', 'dd/mm/yyyy')) AND TRUNC (TO_DATE ('" + dfFechaVencMax + "', 'dd/mm/yyyy')) ");
			}
			if("".equals(icPyme) && "".equals(icCambioEstatus) && "".equals(icMoneda) && "".equals(igNumeroDocto) && "".equals(dcFechaCambioMin) && "".equals(fnMontoMin) && "".equals(dfFechaVencMin))
			   qrySentencia.append(" AND trunc(CE.dc_fecha_cambio) = trunc(TO_DATE('"+fechaActual+"','dd/mm/yyyy')) ");
			qrySentencia.append(" GROUP BY d.ic_moneda,  m.cd_nombre ORDER BY D.ic_moneda ");
			System.out.println("Query de getAggregateCalculationQuery:\n"+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("CambioEstatusEpoDE::getAggregateCalculationQuery "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 

		int i=0;
		StringBuffer qrySentencia = new StringBuffer();
		String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String icCambioEstatus = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");

    
		qrySentencia.append(
				" SELECT (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)"   +
				"              AS cg_razon_social,"   +
				"        d.ig_numero_docto, TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy'),"   +
				"        TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy'),"   +
				"        TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy'), d.ic_moneda,"   +
				"        m.cd_nombre" + idioma + " as cd_nombre, " +
				" 			d.fn_monto, d.fn_monto_dscto,"   +
				"        cce.cd_descripcion" + idioma + " as cd_descripcion, ce.ct_cambio_motivo,"   +
				"        cce.ic_cambio_estatus, ce.fn_monto_anterior, cs_dscto_especial,"   +
				"        (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social)"   +
				"              AS nombreif, ce.fn_monto_nuevo, "+
				"		TO_CHAR (d.df_fecha_venc_pyme, 'dd/mm/yyyy'), "+
				"		ce.cg_nombre_usuario as nombre_usuario, "+//FODEA 015 - 2009 ACF
				"		ma.CG_RAZON_SOCIAL as mandante, "+ //FODEA 023 - 2009 
				"     tf.cg_nombre as TIPO_FACTORAJE,  "+
				"    'CambioEstatusEpoDE::getDocumentSummaryQueryForIds' "   +
				"   FROM com_documento d,"   +
				"        comhis_cambio_estatus ce, "   +
				"        comcat_pyme p,"   +
				"        comcat_if i, "   +
				"        comcat_moneda m, "   +				
				"        comcat_cambio_estatus cce, "   +
				" 			COMCAT_MANDANTE ma,  "+
				" 			COMCAT_TIPO_FACTORAJE tf "+
				"  WHERE d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_documento = ce.ic_documento"   +
				"    AND ce.ic_cambio_estatus = cce.ic_cambio_estatus"   +
				"    AND d.ic_if = i.ic_if (+) "+
				"    AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "+
				"    AND d.cs_dscto_especial = tf.cc_tipo_factoraje "+
				"    AND d.ic_epo = "+ic_epo+
				
				"    AND d.ic_documento in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				qrySentencia.append(",");
			}
			qrySentencia.append(" "+it.next().toString()+" ");
		}
    qrySentencia.append(")");
    
    if(icCambioEstatus.equals(""))
      qrySentencia.append("    AND ce.ic_cambio_estatus IN (4, 5, 6, 7, 8, 16, 24, 28,29) ");
    else
      qrySentencia.append("    AND ce.ic_cambio_estatus = "+icCambioEstatus);
    
		qrySentencia.append(" ORDER BY ce.ic_cambio_estatus, d.ig_numero_docto, ce.dc_fecha_cambio ");
		System.out.println("el query queda de la siguiente manera:\n"+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {
    	StringBuffer qrySentencia = new StringBuffer();
		String icPyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
		String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String igNumeroDocto = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto").trim();
		String dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
		String dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
		String icMoneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
		String icCambioEstatus = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
		String fnMontoMin = (request.getParameter("fn_montoMin") == null) ? "" : request.getParameter("fn_montoMin");
		String fnMontoMax = (request.getParameter("fn_montoMax") == null) ? "" : request.getParameter("fn_montoMax");
		String dcFechaCambioMin = (request.getParameter("dc_fecha_cambioMin") == null) ? "" : request.getParameter("dc_fecha_cambioMin");
		String dcFechaCambioMax = (request.getParameter("dc_fecha_cambioMax") == null) ? "" : request.getParameter("dc_fecha_cambioMax");
		String fechaActual = (request.getParameter("fechaActual") == null) ? "" : request.getParameter("fechaActual");
		try {
			qrySentencia.append(
				" SELECT d.ic_documento, ce.dc_fecha_cambio, 'CambioEstatusEpoDE::getDocumentQuery' "   +
				"   FROM com_documento d,"   +
				"        comhis_cambio_estatus ce, "   +
				"        comcat_pyme p,"   +
				"        comcat_if i, "   +
				"        comcat_moneda m, "   +				
				"        comcat_cambio_estatus cce"   +
				"  WHERE d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_documento = ce.ic_documento"   +
				"    AND ce.ic_cambio_estatus = cce.ic_cambio_estatus"   +
				"    AND d.ic_if = i.ic_if (+) "+
				"    AND d.ic_epo = "+ic_epo);
			if (!"".equals(icPyme))
				qrySentencia.append(" AND D.ic_pyme = " + icPyme.trim());
			
			if (!"".equals(icCambioEstatus))
				qrySentencia.append(" AND CE.ic_cambio_estatus = "+icCambioEstatus.trim());
			else
				qrySentencia.append(" AND CE.ic_cambio_estatus IN (4, 5, 6, 7, 8, 16, 24, 28,29) ");
			
			if (!"".equals(icMoneda))
				qrySentencia.append(" AND D.ic_moneda = " + icMoneda.trim());
			
			if (!"".equals(igNumeroDocto))
				qrySentencia.append(" AND D.ig_numero_docto = '"+igNumeroDocto.trim()+"' ");
			
			if(!"".equals(dcFechaCambioMin)) {
				if(!"".equals(dcFechaCambioMax))
					qrySentencia.append(" AND TRUNC (ce.dc_fecha_cambio) BETWEEN TRUNC (TO_DATE ('"+dcFechaCambioMin.trim()+"', 'dd/mm/yyyy')) AND TRUNC (TO_DATE ('"+dcFechaCambioMax.trim()+"', 'dd/mm/yyyy')) ");
			}
			if(!"".equals(fnMontoMin)) {
				if(!"".equals(fnMontoMax)) {
					qrySentencia.append(" AND D.fn_monto between " + fnMontoMin.trim() + " AND "+ fnMontoMax.trim() +" ");
				} else {
					qrySentencia.append(" AND D.fn_monto = " + fnMontoMin.trim()+" ");
				}
			}
			if(!"".equals(dfFechaVencMin)) {
				if(!"".equals(dfFechaVencMax))
					qrySentencia.append( " AND TRUNC (D.df_fecha_venc) BETWEEN TRUNC (TO_DATE ('" + dfFechaVencMin + "', 'dd/mm/yyyy')) AND TRUNC (TO_DATE ('" + dfFechaVencMax + "', 'dd/mm/yyyy')) ");
			}
			if("".equals(icPyme) && "".equals(icCambioEstatus) && "".equals(icMoneda) && "".equals(igNumeroDocto) && "".equals(dcFechaCambioMin) && "".equals(fnMontoMin) && "".equals(dfFechaVencMin))
			   qrySentencia.append(" AND trunc(CE.dc_fecha_cambio) = trunc(TO_DATE('"+fechaActual+"','dd/mm/yyyy')) ");
			qrySentencia.append("  ORDER BY ce.dc_fecha_cambio");
            System.out.println("EL query de la llave primaria:\n"+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("CambioEstatusEpoDE::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {

		String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 

		StringBuffer qrySentencia = new StringBuffer();
		String icPyme = (request.getParameter("ic_pyme") == null) ? "" : request.getParameter("ic_pyme");
		String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String igNumeroDocto = (request.getParameter("ig_numero_docto") == null) ? "" : request.getParameter("ig_numero_docto").trim();
		String dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin");
		String dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax");
		String icMoneda = (request.getParameter("ic_moneda") == null) ? "" : request.getParameter("ic_moneda");
		String icCambioEstatus = (request.getParameter("ic_cambio_estatus") == null) ? "" : request.getParameter("ic_cambio_estatus");
		String fnMontoMin = (request.getParameter("fn_montoMin") == null) ? "" : request.getParameter("fn_montoMin");
		String fnMontoMax = (request.getParameter("fn_montoMax") == null) ? "" : request.getParameter("fn_montoMax");
		String dcFechaCambioMin = (request.getParameter("dc_fecha_cambioMin") == null) ? "" : request.getParameter("dc_fecha_cambioMin");
		String dcFechaCambioMax = (request.getParameter("dc_fecha_cambioMax") == null) ? "" : request.getParameter("dc_fecha_cambioMax");
		String fechaActual = (request.getParameter("fechaActual") == null) ? "" : request.getParameter("fechaActual");
		try {
			qrySentencia.append(
				" SELECT (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)"   +
				"              AS cg_razon_social,"   +
				"        d.ig_numero_docto, TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy'),"   +
				"        TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy'),"   +
				"        TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy'), d.ic_moneda,"   +
				"        m.cd_nombre" + idioma + " as cd_nombre, d.fn_monto, d.fn_monto_dscto,"   +
				"        cce.cd_descripcion" + idioma + " as cd_descripcion, ce.ct_cambio_motivo,"   +
				"        cce.ic_cambio_estatus, ce.fn_monto_anterior, cs_dscto_especial,"   +
				"        (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social)"   +
				"              AS nombreif, ce.fn_monto_nuevo, "+
				"		TO_CHAR (d.df_fecha_venc_pyme, 'dd/mm/yyyy'), "+
				"		ce.cg_nombre_usuario as nombre_usuario, "+//FODEA 015 - 2009 ACF
				"		ma.CG_RAZON_SOCIAL as mandante, "+ //FODEA 023 - 2009 
				"     tf.cg_nombre as TIPO_FACTORAJE,  "+
				"  'CambioEstatusEpoDE::getDocumentQueryFile' "   +
				"   FROM com_documento d,"   +
				"        comhis_cambio_estatus ce, "   +
				"        comcat_pyme p,"   +
				"        comcat_if i, "   +
				"        comcat_moneda m, "   +	
				" 			COMCAT_MANDANTE ma,  "+
				"        comcat_cambio_estatus cce, "   +
				" 			 COMCAT_TIPO_FACTORAJE tf "+
				"  WHERE d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_documento = ce.ic_documento"   +
				"    AND ce.ic_cambio_estatus = cce.ic_cambio_estatus"   +
				"    AND d.ic_if = i.ic_if (+) "+
				"    AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "+
				"    AND d.cs_dscto_especial = tf.cc_tipo_factoraje " +
				"    AND d.ic_epo = "+ic_epo);
			if (!"".equals(icPyme))
				qrySentencia.append(" AND D.ic_pyme = " + icPyme.trim());
			
			if (!"".equals(icCambioEstatus))
				qrySentencia.append(" AND CE.ic_cambio_estatus = "+icCambioEstatus.trim());
			else
				qrySentencia.append(" AND CE.ic_cambio_estatus IN (4, 5, 6, 7, 8, 16, 24, 28,29) ");
			
			if (!"".equals(icMoneda))
				qrySentencia.append(" AND D.ic_moneda = " + icMoneda.trim());
			
			if (!"".equals(igNumeroDocto))
				qrySentencia.append(" AND D.ig_numero_docto = '"+igNumeroDocto.trim()+"' ");
			
			if(!"".equals(dcFechaCambioMin)) {
				if(!"".equals(dcFechaCambioMax))
					qrySentencia.append(" AND TRUNC (ce.dc_fecha_cambio) BETWEEN TRUNC (TO_DATE ('"+dcFechaCambioMin.trim()+"', 'dd/mm/yyyy')) AND TRUNC (TO_DATE ('"+dcFechaCambioMax.trim()+"', 'dd/mm/yyyy')) ");
			}
			if(!"".equals(fnMontoMin)) {
				if(!"".equals(fnMontoMax)) {
					qrySentencia.append(" AND D.fn_monto between " + fnMontoMin.trim() + " AND "+ fnMontoMax.trim() +" ");
				} else {
					qrySentencia.append(" AND D.fn_monto = " + fnMontoMin.trim()+" ");
				}
			}
			if(!"".equals(dfFechaVencMin)) {
				if(!"".equals(dfFechaVencMax))
					qrySentencia.append( " AND TRUNC(D.df_fecha_venc) BETWEEN TRUNC (TO_DATE ('" + dfFechaVencMin + "', 'dd/mm/yyyy')) AND TRUNC (TO_DATE ('" + dfFechaVencMax + "', 'dd/mm/yyyy')) ");
			}
			if("".equals(icPyme) && "".equals(icCambioEstatus) && "".equals(icMoneda) && "".equals(igNumeroDocto) && "".equals(dcFechaCambioMin) && "".equals(fnMontoMin) && "".equals(dfFechaVencMin))
			   qrySentencia.append(" AND trunc(CE.dc_fecha_cambio) = trunc(TO_DATE('"+fechaActual+"','dd/mm/yyyy')) ");
			qrySentencia.append("  ORDER BY ce.ic_cambio_estatus, d.ig_numero_docto, ce.dc_fecha_cambio ");
			
			System.out.println("Query de getDocumentQueryFile:\n"+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("CambioEstatusEpoDE::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}
	
	/**
	 * METODOS DE LA MIGRACION
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
			log.info("getAggregateCalculationQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();


			qrySentencia.append(" SELECT d.ic_moneda " +
					",m.cd_nombre" + idioma + " as MONEDA " +
					", COUNT (1) AS TOTAL_REGISTROS, SUM (d.fn_monto)  as MONTO_TOTAL");
			if("V".equals(txt_opera_fac_venc)){
				qrySentencia.append("        ,SUM (DECODE (cs_dscto_especial, 'V', d.fn_monto, d.fn_monto * decode(d.ic_moneda,1,?,54,?))) AS MONTO_DESCUENTO");
			conditions.add(strAforo);
			conditions.add(strAforoDL);
			}else {// se agrega validacion para que tome 100% del monto a descontar cuando sea Factoraje Vencimiento Infonavit - FODEA 042/2009 -
				qrySentencia.append(" ,SUM (d.fn_monto * DECODE (d.ic_moneda, 1, decode(d.cs_dscto_especial, 'I', 1, ? ), 54, ? )) AS MONTO_DESCUENTO");
				conditions.add(strAforo);
				conditions.add(strAforoDL);
			}								
			qrySentencia.append(
				"   FROM com_documento d,"   +
				"        comhis_cambio_estatus ce, "   +
				"        comcat_pyme p,"   +
				"        comcat_if i,"   +
				"        comcat_moneda m, "   +				
				"        comcat_cambio_estatus cce"   +
				"  WHERE d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_documento = ce.ic_documento"   +
				"    AND ce.ic_cambio_estatus = cce.ic_cambio_estatus"   +
				"    AND d.ic_if = i.ic_if (+) "+
				"    AND d.ic_epo = ? ");
				conditions.add(ic_epo);

			if (!"".equals(icPyme)){
				qrySentencia.append(" AND D.ic_pyme = ? " );
				conditions.add(icPyme.trim());
			}
			
			if (!"".equals(icCambioEstatus)){
				qrySentencia.append(" AND CE.ic_cambio_estatus = ? ");
				conditions.add(icCambioEstatus.trim());
			}else{
				qrySentencia.append(" AND CE.ic_cambio_estatus IN (4, 5, 6, 7, 8, 16, 24, 28,29) ");
			}
			if (!"".equals(icMoneda)){
				qrySentencia.append(" AND D.ic_moneda =  ? " );
				conditions.add(icMoneda.trim());
			}
			
			if (!"".equals(igNumeroDocto)){
				qrySentencia.append(" AND D.ig_numero_docto = ?  ");
				conditions.add(igNumeroDocto.trim());
			}
			
			if(!"".equals(dcFechaCambioMin)) {
				if(!"".equals(dcFechaCambioMax)){
					qrySentencia.append(" AND TRUNC (ce.dc_fecha_cambio) BETWEEN TRUNC (TO_DATE ( ? , 'dd/mm/yyyy')) AND TRUNC (TO_DATE ( ? , 'dd/mm/yyyy')) ");
					conditions.add(dcFechaCambioMin.trim());
					conditions.add(dcFechaCambioMax.trim());
				}
			}
			if(!"".equals(fnMontoMin)) {
				if(!"".equals(fnMontoMax)) {
					qrySentencia.append(" AND D.fn_monto between ?  AND ?  ");
					conditions.add(fnMontoMin.trim());
					conditions.add(fnMontoMax.trim());
				} else {
					qrySentencia.append(" AND D.fn_monto = ? ");
					conditions.add(fnMontoMin.trim());
				}
			}
			if(!"".equals(dfFechaVencMin)) {
				if(!"".equals(dfFechaVencMax)){
					qrySentencia.append( " AND TRUNC (D.df_fecha_venc) BETWEEN TRUNC (TO_DATE ( ?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE ( ?, 'dd/mm/yyyy')) ");
					conditions.add(dfFechaVencMin.trim());
					conditions.add(dfFechaVencMax.trim());
				}
			}
			if("".equals(icPyme) && "".equals(icCambioEstatus) && "".equals(icMoneda) && "".equals(igNumeroDocto) && "".equals(dcFechaCambioMin) && "".equals(fnMontoMin) && "".equals(dfFechaVencMin)){
			   qrySentencia.append(" AND trunc(CE.dc_fecha_cambio) = trunc(TO_DATE( ?,'dd/mm/yyyy')) ");
				 conditions.add(fechaActual);
			}
			qrySentencia.append(" GROUP BY d.ic_moneda,  m.cd_nombre ORDER BY D.ic_moneda ");
			
			log.debug("..:: qrySentencia: " + qrySentencia.toString());
			log.debug("..:: conditions: " + conditions);
			log.info("getAggregateCalculationQuery(S)");
			return qrySentencia.toString();
	}


	public String getDocumentQuery(){
	log.info("getDocumentQuery(E)");
		qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append(" SELECT d.ic_documento, ce.ct_cambio_motivo, ce.dc_fecha_cambio, 'CambioEstatusEpoDE::getDocumentQuery' ");
		qrySentencia.append("   FROM com_documento d,");
		qrySentencia.append("        comhis_cambio_estatus ce, ");
		qrySentencia.append("        comcat_pyme p,");
		qrySentencia.append("        comcat_if i, ");
		qrySentencia.append("        comcat_moneda m, ");
		qrySentencia.append("        comcat_cambio_estatus cce");
		qrySentencia.append("  WHERE d.ic_pyme = p.ic_pyme");
		qrySentencia.append("    AND d.ic_moneda = m.ic_moneda");
		qrySentencia.append("    AND d.ic_documento = ce.ic_documento");
		qrySentencia.append("    AND ce.ic_cambio_estatus = cce.ic_cambio_estatus");
		qrySentencia.append("    AND d.ic_if = i.ic_if (+) ");
		qrySentencia.append("    AND d.ic_epo = ? ");

		conditions.add(ic_epo);

		if (!"".equals(icPyme)){
			qrySentencia.append(" AND D.ic_pyme = ? ");
			conditions.add(icPyme.trim());
		}
		if (!"".equals(icCambioEstatus)){
			qrySentencia.append(" AND CE.ic_cambio_estatus = ? ");
			conditions.add(icCambioEstatus.trim());
		}else{
			qrySentencia.append(" AND CE.ic_cambio_estatus IN (4, 5, 6, 7, 8, 16, 24, 28,29) ");
		}
		if (!"".equals(icMoneda)){
			qrySentencia.append(" AND D.ic_moneda = ? ");
			conditions.add(icMoneda.trim());
		}
		if (!"".equals(igNumeroDocto)){
			qrySentencia.append(" AND D.ig_numero_docto = ? ");
			conditions.add(igNumeroDocto.trim());
		}
		if(!"".equals(dcFechaCambioMin)) {
			if(!"".equals(dcFechaCambioMax)){
				qrySentencia.append(" AND TRUNC (ce.dc_fecha_cambio) BETWEEN TRUNC (TO_DATE ( ? , 'dd/mm/yyyy')) AND TRUNC (TO_DATE ( ? , 'dd/mm/yyyy')) ");
				conditions.add(dcFechaCambioMin.trim());
				conditions.add(dcFechaCambioMax.trim());
			}
		}
		if(!"".equals(fnMontoMin)) {
			if(!"".equals(fnMontoMax)) {
				qrySentencia.append(" AND D.fn_monto between ? AND ? ");
				conditions.add(fnMontoMin.trim());
				conditions.add(fnMontoMax.trim());
			} else {
				qrySentencia.append(" AND D.fn_monto = ? ");
				conditions.add(fnMontoMin.trim());
			}
		}
		if(!"".equals(dfFechaVencMin)) {
			if(!"".equals(dfFechaVencMax)){
				qrySentencia.append( " AND TRUNC (D.df_fecha_venc) BETWEEN TRUNC (TO_DATE ( ? , 'dd/mm/yyyy')) AND TRUNC (TO_DATE ( ?, 'dd/mm/yyyy')) ");
				conditions.add(dfFechaVencMin);
				conditions.add(dfFechaVencMax);
			}
		}
		if("".equals(icPyme) && "".equals(icCambioEstatus) && "".equals(icMoneda) && "".equals(igNumeroDocto) && "".equals(dcFechaCambioMin) && "".equals(fnMontoMin) && "".equals(dfFechaVencMin)) {
			qrySentencia.append(" AND trunc(CE.dc_fecha_cambio) = trunc(TO_DATE( ?,'dd/mm/yyyy')) ");
			conditions.add(fechaActual);
		}
		qrySentencia.append("  ORDER BY ce.dc_fecha_cambio");

		log.debug("..:: qrySentencia: " + qrySentencia.toString());
		log.debug("..:: conditions: " + conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}


	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append(" SELECT (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social) AS NOMBRE_PYME,");
		qrySentencia.append("        d.ig_numero_docto AS NUM_DOCUMENTO, ");
		qrySentencia.append("        TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS  FECHA_VENCIMIENTO ,");
		qrySentencia.append("        TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS FECHA_EMISION  ,");
		qrySentencia.append("        TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS FECHA_CAMBIO ,");
		qrySentencia.append("         d.ic_moneda,");
		qrySentencia.append("        m.cd_nombre" + idioma + " as MONEDA, ");
		qrySentencia.append("        d.fn_monto AS  MONTO   ");
		if("V".equals(txt_opera_fac_venc)){
			qrySentencia.append("     ,(DECODE (cs_dscto_especial, 'V', d.fn_monto, d.fn_monto * decode(d.ic_moneda,1,?,54,?))) AS MONTO_DESCONTAR");
			conditions.add(strAforo);
			conditions.add(strAforoDL);
		} else {// se agrega validacion para que tome 100% del monto a descontar cuando sea Factoraje Vencimiento Infonavit - FODEA 042/2009 -
			qrySentencia.append(" ,  (d.fn_monto * DECODE (d.ic_moneda, 1, decode(d.cs_dscto_especial, 'I', 1, ? ), 54, ? )) AS MONTO_DESCONTAR");
			conditions.add(strAforo);
			conditions.add(strAforoDL);
		}

		qrySentencia.append("        ,cce.cd_descripcion" + idioma + " as TIPO_CAMBIO, ");
		qrySentencia.append("        ce.ct_cambio_motivo AS CAUSA,");
		qrySentencia.append("        cce.ic_cambio_estatus AS IC_CAMBIO_ESTATUS,");
		qrySentencia.append("        ce.fn_monto_anterior AS MONTO_ANTERIOR  , ");
		qrySentencia.append("        cs_dscto_especial,");
		qrySentencia.append("        (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social) AS NOMBRE_IF,");
		qrySentencia.append("        ce.fn_monto_nuevo, ");
		qrySentencia.append("        TO_CHAR (d.df_fecha_venc_pyme, 'dd/mm/yyyy') AS FECHA_VENCIMIENTO_PYME , ");
		qrySentencia.append("        ce.cg_nombre_usuario as USUARIO, ");//FODEA 015 - 2009 ACF
		qrySentencia.append("        ma.CG_RAZON_SOCIAL as MANDANTE, "); //FODEA 023 - 2009 
		qrySentencia.append("        tf.cg_nombre as TIPO_FACTORAJE,  ");
		qrySentencia.append("        'CambioEstatusEpoDE::getDocumentSummaryQueryForIds' ");
		qrySentencia.append("   FROM com_documento d,");
		qrySentencia.append("        comhis_cambio_estatus ce, ");
		qrySentencia.append("        comcat_pyme p,");
		qrySentencia.append("        comcat_if i, ");
		qrySentencia.append("        comcat_moneda m, ");
		qrySentencia.append("        comcat_cambio_estatus cce, ");
		qrySentencia.append("        COMCAT_MANDANTE ma,  ");
		qrySentencia.append("        COMCAT_TIPO_FACTORAJE tf ");
		qrySentencia.append("  WHERE d.ic_pyme = p.ic_pyme");
		qrySentencia.append("    AND d.ic_moneda = m.ic_moneda");
		qrySentencia.append("    AND d.ic_documento = ce.ic_documento");
		qrySentencia.append("    AND ce.ic_cambio_estatus = cce.ic_cambio_estatus");
		qrySentencia.append("    AND d.ic_if = i.ic_if (+) ");
		qrySentencia.append("    AND ma.IC_MANDANTE(+) = d.IC_MANDANTE ");
		qrySentencia.append("    AND d.cs_dscto_especial = tf.cc_tipo_factoraje ");
		qrySentencia.append("    AND d.ic_epo = ? ");
		conditions.add(ic_epo);

		for(int i=0;i<pageIds.size();i++) {
			List lItem = (List)pageIds.get(i);
			if(i==0) {
				qrySentencia.append(" AND d.ic_documento in  ( ");
			}
			qrySentencia.append("?");
			conditions.add(new Long(lItem.get(0).toString()));
			if(i!=(pageIds.size()-1)) {
				qrySentencia.append(",");
			} else {
				qrySentencia.append(" ) ");
			}
		}

		for(int i=0;i<pageIds.size();i++) {
			List lItem = (List)pageIds.get(i);
			if(i==0) {
				qrySentencia.append(" AND ce.ct_cambio_motivo in ( ");
			}
			qrySentencia.append("?");
			conditions.add(new String(lItem.get(1).toString()));
			if(i!=(pageIds.size()-1)) {
				qrySentencia.append(",");
			} else {
				qrySentencia.append(" ) ");
			}
		}

		if(icCambioEstatus.equals("")){
			qrySentencia.append(" AND ce.ic_cambio_estatus IN (4, 5, 6, 7, 8, 16, 24, 28,29) ");
		}else{
			qrySentencia.append(" AND ce.ic_cambio_estatus = ? ");
			conditions.add(icCambioEstatus);
		}
		qrySentencia.append(" ORDER BY ce.ic_cambio_estatus, d.ig_numero_docto, ce.dc_fecha_cambio ");

		log.debug("..:: qrySentencia "+qrySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}


	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append(" SELECT (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social) AS NOMBRE_PYME,");
		qrySentencia.append("        d.ig_numero_docto AS NUM_DOCUMENTO, ");
		qrySentencia.append("        TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS  FECHA_VENCIMIENTO ,");
		qrySentencia.append("        TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS FECHA_EMISION  ,");
		qrySentencia.append("        TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS FECHA_CAMBIO ,");
		qrySentencia.append("        d.ic_moneda,");
		qrySentencia.append("        m.cd_nombre" + idioma + " as MONEDA, ");
		qrySentencia.append("        d.fn_monto AS  MONTO  , ");
		qrySentencia.append("        d.fn_monto_dscto AS MONTO_DESCONTAR ,");
		qrySentencia.append("        cce.cd_descripcion" + idioma + " as TIPO_CAMBIO, ");
		qrySentencia.append("        ce.ct_cambio_motivo AS CAUSA,");
		qrySentencia.append("        cce.ic_cambio_estatus,");
		qrySentencia.append("        ce.fn_monto_anterior AS MONTO_ANTERIOR  , ");
		qrySentencia.append("        cs_dscto_especial,");
		qrySentencia.append("        (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social) AS NOMBRE_IF,");
		qrySentencia.append("        ce.fn_monto_nuevo, ");
		qrySentencia.append("        TO_CHAR (d.df_fecha_venc_pyme, 'dd/mm/yyyy') AS FECHA_VENCIMIENTO_PYME , ");
		qrySentencia.append("        ce.cg_nombre_usuario as USUARIO, ");//FODEA 015 - 2009 ACF
		qrySentencia.append("        ma.CG_RAZON_SOCIAL as MANDANTE, "); //FODEA 023 - 2009 
		qrySentencia.append("        tf.cg_nombre as TIPO_FACTORAJE,  ");
		qrySentencia.append("        'CambioEstatusEpoDE::getDocumentSummaryQueryForIds' ");
		qrySentencia.append("   FROM com_documento d,");
		qrySentencia.append("        comhis_cambio_estatus ce, ");
		qrySentencia.append("        comcat_pyme p,");
		qrySentencia.append("        comcat_if i, ");
		qrySentencia.append("        comcat_moneda m, ");
		qrySentencia.append("        comcat_cambio_estatus cce, ");
		qrySentencia.append("        COMCAT_MANDANTE ma,  ");
		qrySentencia.append("        COMCAT_TIPO_FACTORAJE tf ");
		qrySentencia.append("  WHERE d.ic_pyme = p.ic_pyme");
		qrySentencia.append("    AND d.ic_moneda = m.ic_moneda");
		qrySentencia.append("    AND d.ic_documento = ce.ic_documento");
		qrySentencia.append("    AND ce.ic_cambio_estatus = cce.ic_cambio_estatus");
		qrySentencia.append("    AND d.ic_if = i.ic_if (+) ");
		qrySentencia.append("    AND ma.IC_MANDANTE(+) = d.IC_MANDANTE ");
		qrySentencia.append("    AND d.cs_dscto_especial = tf.cc_tipo_factoraje ");
		qrySentencia.append("    AND d.ic_epo = ? ");
		conditions.add(ic_epo);

		if(icCambioEstatus.equals("")){
			qrySentencia.append("    AND ce.ic_cambio_estatus IN (4, 5, 6, 7, 8, 16, 24, 28,29) ");
		}else{
			qrySentencia.append("    AND ce.ic_cambio_estatus = ? ");
			conditions.add(icCambioEstatus);	
		}

		if (!"".equals(icPyme)){
			qrySentencia.append(" AND D.ic_pyme = ? ");
			conditions.add(icPyme.trim());
		}
		if (!"".equals(icCambioEstatus)){
			qrySentencia.append(" AND CE.ic_cambio_estatus = ? ");
			conditions.add(icCambioEstatus.trim());
		}else{
			qrySentencia.append(" AND CE.ic_cambio_estatus IN (4, 5, 6, 7, 8, 16, 24, 28,29) ");
		}

		if (!"".equals(icMoneda)){
			qrySentencia.append(" AND D.ic_moneda = ? ");
			conditions.add(icMoneda.trim());
		}

		if (!"".equals(igNumeroDocto)){
			qrySentencia.append(" AND D.ig_numero_docto = ? ");
			conditions.add(igNumeroDocto.trim());
		}

		if(!"".equals(dcFechaCambioMin)) {
			if(!"".equals(dcFechaCambioMax)){
				qrySentencia.append(" AND TRUNC (ce.dc_fecha_cambio) BETWEEN TRUNC (TO_DATE ( ? , 'dd/mm/yyyy')) AND TRUNC (TO_DATE ( ? , 'dd/mm/yyyy')) ");
				conditions.add(dcFechaCambioMin.trim());
				conditions.add(dcFechaCambioMax.trim());
			}
		}
		if(!"".equals(fnMontoMin)) {
			if(!"".equals(fnMontoMax)) {
				qrySentencia.append(" AND D.fn_monto between ? AND ? ");
				conditions.add(fnMontoMin.trim());
				conditions.add(fnMontoMax.trim());
			} else {
				qrySentencia.append(" AND D.fn_monto = ? ");
				conditions.add(fnMontoMin.trim());
			}
		}
		if(!"".equals(dfFechaVencMin)) {
			if(!"".equals(dfFechaVencMax)){
				qrySentencia.append( " AND TRUNC (D.df_fecha_venc) BETWEEN TRUNC (TO_DATE ( ? , 'dd/mm/yyyy')) AND TRUNC (TO_DATE ( ?, 'dd/mm/yyyy')) ");
				conditions.add(dfFechaVencMin);
				conditions.add(dfFechaVencMax);
			}
		}
		if("".equals(icPyme) && "".equals(icCambioEstatus) && "".equals(icMoneda) && "".equals(igNumeroDocto) && "".equals(dcFechaCambioMin) && "".equals(fnMontoMin) && "".equals(dfFechaVencMin)) {
			qrySentencia.append(" AND trunc(CE.dc_fecha_cambio) = trunc(TO_DATE( ?,'dd/mm/yyyy')) ");
			conditions.add(fechaActual);
		}

		qrySentencia.append(" ORDER BY ce.ic_cambio_estatus, d.ig_numero_docto, ce.dc_fecha_cambio ");

		log.info("..:: qrySentencia: "+qrySentencia.toString());
		log.info("..:: conditions: "+conditions);
		log.info("getDocumentQueryFile(S)");

		return qrySentencia.toString();
	}

	/**
	 * 
	 * @return 
	 * @param tipo
	 * @param path
	 * @param reg
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();		
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		
		boolean bOperaFactConMandato=false;
		boolean bTipoFactoraje = false;
		boolean bOperaFactorajeVencInfonavit = false;
		boolean bOperaFactorajeVencido = false;
		boolean bFactorajeIf = false;
	
		double montoTotalMN=0, montoTotalDL=0;
		int numeroTotalDocsMN=0, numeroTotalDocsDols=0,  i=0;
		BigDecimal montoTotalDsctoMN=new BigDecimal("0.0"); BigDecimal montoTotalDsctoDL=new BigDecimal("0.0");
		BigDecimal porcentaje=new BigDecimal(strAforo); BigDecimal mtodoc=null;
		BigDecimal porcentajeDL=new BigDecimal(strAforoDL);
		BigDecimal mtodesc=new BigDecimal("0.0");
		String  nombrePyme ="", num_documento ="", fecha_emision="", fecha_vencimiento ="", fecha_venc_pyme ="", 
			fecha_cambio ="", moneda  ="", tipo_Factoraje ="", monto ="", tipo_Cambio ="", causa ="", usuario ="",
			monto_anterior ="", mandante ="", tipoFactoraje="", NombreIf="", nombreTipoFactoraje ="", icCambioEstatus =""; 
	
	
		try {
		
			ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
			
			ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
			
			String sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(ic_epo);
				
			Hashtable alParamEPO1 = new Hashtable(); 
			alParamEPO1 = BeanParamDscto.getParametrosEPO(ic_epo,1);	
			if (alParamEPO1!=null) {
				bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
				bOperaFactorajeVencido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
				bOperaFactorajeVencInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;
				bFactorajeIf								=	("N".equals(alParamEPO1.get("FACTORAJE_IF").toString()))?false:true; //FODEA Noviembre 2012
			}
			bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencInfonavit||bFactorajeIf)?true:false;
	
	
			if ("PDF".equals(tipo)) {			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				int columnas =12;
				
				if(!"".equals(sFechaVencPyme)) { columnas =columnas+1; }
				if(bTipoFactoraje || icCambioEstatus.equals("29") ) { columnas =columnas+1; }
				if(bTipoFactoraje || icCambioEstatus.equals("29")  || bOperaFactConMandato || bOperaFactorajeVencInfonavit) { columnas =columnas+1; }
				if(bOperaFactConMandato){  columnas =columnas+1; }
					
					
				pdfDoc.setLTable(columnas, 100);							
				pdfDoc.setLCell("Proveedor ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Num. Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Emisi�n ","celda01",ComunesPDF.CENTER);
				if(!"".equals(sFechaVencPyme)) {
				pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell("Fecha Vencimiento Proveedor ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de cambio de estatus ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje || icCambioEstatus.equals("29") ) {
				pdfDoc.setLCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell("Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Descontar ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cambio de estatus ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Usuario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Anterior ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje || icCambioEstatus.equals("29")  || bOperaFactConMandato || bOperaFactorajeVencInfonavit) {
				pdfDoc.setLCell("Nombre IF ","celda01",ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){ 
				pdfDoc.setLCell("Mandante ","celda01",ComunesPDF.CENTER);
				}
				
				while (rs.next()) {
				
					if(Integer.parseInt( rs.getString(12)) == 28){ //aplicacion de nota de credito
						 mtodoc=new BigDecimal(rs.getString("fn_monto_nuevo"));
					}else{
					 mtodoc=new BigDecimal(rs.getString(8));
					}
					
					if ("1".equals(rs.getString(6))) {
						mtodesc=mtodoc.multiply(porcentaje);
					} else if("54".equals(rs.getString(6))) { //dolar
						mtodesc=mtodoc.multiply(porcentajeDL);
					}
					
					nombreTipoFactoraje = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE");
				 
				 // Fodea 042 - 2009 Factoraje Vencimiento Infonavit 
					if(bOperaFactorajeVencido || bOperaFactorajeVencInfonavit) { // Para Factoraje Vencido o Vencimiento Infonavit
						tipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
						// si factoraje vencido � Vencimiento Infonavit: se aplica 100% de anticipo
						NombreIf = "";
						if(tipoFactoraje.equals("V") || tipoFactoraje.equals("I")||tipoFactoraje.equals("A") )  {
							mtodesc = mtodoc;
							NombreIf = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
						}
						if(tipoFactoraje.equals("C"))  {
							mtodesc = mtodoc;
						}
					}					
					// Fodea 023 2009 mandato
					if(tipoFactoraje.equals("M") && bOperaFactConMandato)  {							
						NombreIf = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					}
					if(icCambioEstatus.equals("29")){//Estatus 29 = Programado a Negociable Foda 025-2005
						tipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
						NombreIf = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
						if ("1".equals(rs.getString(6))) {
							mtodesc=mtodoc.multiply(porcentaje);
						} else if("54".equals(rs.getString(6))) {//dolar
							mtodesc=mtodoc.multiply(porcentajeDL);
						}
					}

					//para los totales 
					if ("1".equals(rs.getString(6))) {   //moneda nacional
						montoTotalDsctoMN = montoTotalDsctoMN.add(mtodesc);
						montoTotalMN = montoTotalMN + Double.parseDouble((String)rs.getString(8));
						numeroTotalDocsMN++;
					}
					else if("54".equals(rs.getString(6))) { //dolar
						montoTotalDsctoDL = montoTotalDsctoDL.add(mtodesc);
						montoTotalDL = montoTotalDL + Double.parseDouble((String)rs.getString(8)); 
						numeroTotalDocsDols++;
					}
			
					nombrePyme = (rs.getString(1)==null)?"":rs.getString(1).trim();
					num_documento = (rs.getString(2)==null)?"":rs.getString(2).trim(); 
					fecha_emision=  (rs.getString(4)==null)?"":rs.getString(4).trim(); 
					fecha_vencimiento =  (rs.getString(3)==null)?"":rs.getString(3).trim();
					fecha_venc_pyme = (rs.getString(17)==null)?"":rs.getString(17).trim();	
					fecha_cambio = (rs.getString(5)==null)?"":rs.getString(5).trim();	
					moneda = (rs.getString(7)==null)?"":rs.getString(7).trim();	
					tipo_Factoraje = (rs.getString(20)==null)?"":rs.getString(20).trim();	
					monto = (rs.getString(8)==null)?"":rs.getString(8).trim();	
					tipo_Cambio = (rs.getString(10)==null)?"":rs.getString(10).trim();
					causa = (rs.getString(11)==null)?"":rs.getString(11).trim();
					usuario = (rs.getString(18)==null)?"":rs.getString(18).trim();
					monto_anterior = (rs.getString(13)==null)?"":rs.getString(13).trim();
					mandante = (rs.getString(19)==null)?"":rs.getString(19).trim();

					if(Integer.parseInt( rs.getString(12)) == 8){ 
						monto = (Integer.parseInt( rs.getString(12)) == 28)?rs.getString("fn_monto_nuevo").trim():rs.getString(8).trim();
						if(Integer.parseInt( rs.getString(12)) == 28){ //si ic_cambio_estatus=28 (aplicacion de nota de credito)
							 tipo_Cambio = rs.getString(10).trim()+"\n"+rs.getString(5).trim();
						}
						if(bOperaFactorajeVencido  || icCambioEstatus.equals("29")  ||  bOperaFactConMandato || bOperaFactorajeVencInfonavit) { 
							NombreIf  = NombreIf;
						}else  { 
							NombreIf ="";
						}						 
					}

					if(!icCambioEstatus.equals("28")) {
						monto_anterior = "";
					}

					pdfDoc.setLCell(nombrePyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(num_documento,"formas",ComunesPDF.CENTER);	
					pdfDoc.setLCell(fecha_emision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_vencimiento,"formas",ComunesPDF.CENTER);
					if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setLCell(fecha_venc_pyme,"formas",ComunesPDF.CENTER);	
					}
					pdfDoc.setLCell(fecha_cambio,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					if(bTipoFactoraje || icCambioEstatus.equals("29") ) {
						pdfDoc.setLCell(tipo_Factoraje,"formas",ComunesPDF.CENTER);	
					}
					pdfDoc.setLCell(Comunes.formatoMoneda2(monto,true) ,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(Comunes.formatoMoneda2(mtodesc.toPlainString(),true),"formas",ComunesPDF.RIGHT);	
					pdfDoc.setLCell(tipo_Cambio,"formas",ComunesPDF.CENTER);	
					pdfDoc.setLCell(causa,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(usuario,"formas",ComunesPDF.LEFT);					
					pdfDoc.setLCell(Comunes.formatoMoneda2(monto_anterior,true) ,"formas",ComunesPDF.RIGHT);
					if(bTipoFactoraje || icCambioEstatus.equals("29")  || bOperaFactConMandato || bOperaFactorajeVencInfonavit) {
						pdfDoc.setLCell(NombreIf,"formas",ComunesPDF.LEFT);	
					}
					if(bOperaFactConMandato){ 
						pdfDoc.setLCell(mandante,"formas",ComunesPDF.LEFT);								
					}
					 
				}//while (rs.next()) {
							
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}
				
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				//reg.close();
			} catch(Exception e) {}
		}
		return nombreArchivo;
	}



	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo = "";

		try{
			HttpSession session = request.getSession();
			StringBuffer contenidoArchivo = new StringBuffer();
			ComunesPDF pdfDoc = new ComunesPDF();
			CreaArchivo creaArchivo = new CreaArchivo();
			boolean bOperaFactConMandato=false;
			boolean bTipoFactoraje = false;
			boolean bOperaFactorajeVencInfonavit = false;
			boolean bOperaFactorajeVencido = false;
			boolean bFactorajeIf = false;

			double montoTotalMN=0, montoTotalDL=0;
			int numeroTotalDocsMN=0, numeroTotalDocsDols=0,  i=0;
			BigDecimal montoTotalDsctoMN=new BigDecimal("0.0"); BigDecimal montoTotalDsctoDL=new BigDecimal("0.0");
			BigDecimal porcentaje=new BigDecimal(strAforo); BigDecimal mtodoc=null;
			BigDecimal porcentajeDL=new BigDecimal(strAforoDL);
			BigDecimal mtodesc=new BigDecimal("0.0");
			String  nombrePyme ="", num_documento ="", fecha_emision="", fecha_vencimiento ="", fecha_venc_pyme ="", 
			fecha_cambio ="", moneda  ="", tipo_Factoraje ="", monto ="", tipo_Cambio ="", causa ="", usuario ="",
			monto_anterior ="", mandante ="", tipoFactoraje="", NombreIf="", nombreTipoFactoraje ="", icCambioEstatus ="";

			ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
			ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
			String sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(ic_epo);

			Hashtable alParamEPO1 = new Hashtable();
			alParamEPO1 = BeanParamDscto.getParametrosEPO(ic_epo,1);
			if (alParamEPO1!=null){
				bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
				bOperaFactorajeVencido = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
				bOperaFactorajeVencInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;
				bFactorajeIf = ("N".equals(alParamEPO1.get("FACTORAJE_IF").toString()))?false:true; //FODEA Noviembre 2012
			}
			bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactConMandato || bOperaFactorajeVencInfonavit||bFactorajeIf)?true:false;

			if (tipo.equals("CSV")){

				contenidoArchivo.append("Proveedor " + ",");
				contenidoArchivo.append("Num. Documento " + ",");
				contenidoArchivo.append("Fecha Emisi�n " + ",");
				if(!"".equals(sFechaVencPyme)) {
				contenidoArchivo.append("Fecha de Vencimiento" + ","); 
				}
				contenidoArchivo.append("Fecha Vencimiento Proveedor " + ",");
				contenidoArchivo.append("Fecha de cambio de estatus " + ",");
				contenidoArchivo.append("Moneda " + ",");
				if(bTipoFactoraje || icCambioEstatus.equals("29") ) {
				contenidoArchivo.append("Tipo Factoraje " + ",");
				}
				contenidoArchivo.append("Monto " + ",");
				contenidoArchivo.append("Monto a Descontar " + ",");
				contenidoArchivo.append("Tipo de cambio de estatus " + ",");
				contenidoArchivo.append("Causa " + ",");
				contenidoArchivo.append("Usuario " + ",");
				contenidoArchivo.append("Monto Anterior " + ",");
				if(bTipoFactoraje || icCambioEstatus.equals("29")  || bOperaFactConMandato || bOperaFactorajeVencInfonavit) {
				contenidoArchivo.append("Nombre IF " + ",");
				}
				if(bOperaFactConMandato){ 
				contenidoArchivo.append("Mandante " + ",");
				}
				contenidoArchivo.append("\n");
				countReg = 0;
				while (rs.next()){
					if(Integer.parseInt( rs.getString(12)) == 28){ //aplicacion de nota de credito
						 mtodoc=new BigDecimal(rs.getString("fn_monto_nuevo"));
					}else{
						mtodoc=new BigDecimal(rs.getString(8));
					}
					if ("1".equals(rs.getString(6))) {
						mtodesc=mtodoc.multiply(porcentaje);
					} else if("54".equals(rs.getString(6))) { //dolar
						mtodesc=mtodoc.multiply(porcentajeDL);
					}
					nombreTipoFactoraje = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE");
				 // Fodea 042 - 2009 Factoraje Vencimiento Infonavit 
					if(bOperaFactorajeVencido || bOperaFactorajeVencInfonavit) { // Para Factoraje Vencido o Vencimiento Infonavit
						tipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
						// si factoraje vencido � Vencimiento Infonavit: se aplica 100% de anticipo
						NombreIf = "";
						if(tipoFactoraje.equals("V") || tipoFactoraje.equals("I")||tipoFactoraje.equals("A") ){
							mtodesc = mtodoc;
							NombreIf = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
						}
						if(tipoFactoraje.equals("C")){
							mtodesc = mtodoc;
						}
					}
					// Fodea 023 2009 mandato
					if(tipoFactoraje.equals("M") && bOperaFactConMandato){
						NombreIf = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					}
					if(icCambioEstatus.equals("29")){//Estatus 29 = Programado a Negociable Foda 025-2005
						tipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
						NombreIf = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
						if ("1".equals(rs.getString(6))) {
							mtodesc=mtodoc.multiply(porcentaje);
						} else if("54".equals(rs.getString(6))) {//dolar
							mtodesc=mtodoc.multiply(porcentajeDL);
						}
					}

					//para los totales 
					if ("1".equals(rs.getString(6))) {   //moneda nacional
						montoTotalDsctoMN = montoTotalDsctoMN.add(mtodesc);
						montoTotalMN = montoTotalMN + Double.parseDouble((String)rs.getString(8));
						numeroTotalDocsMN++;
					}
					else if("54".equals(rs.getString(6))) { //dolar
						montoTotalDsctoDL = montoTotalDsctoDL.add(mtodesc);
						montoTotalDL = montoTotalDL + Double.parseDouble((String)rs.getString(8)); 
						numeroTotalDocsDols++;
					}

					 nombrePyme        = (rs.getString(1)  ==null)?"":rs.getString(1).trim();
					 num_documento     = (rs.getString(2)  ==null)?"":rs.getString(2).trim();
					 fecha_emision     = (rs.getString(4)  ==null)?"":rs.getString(4).trim();
					 fecha_vencimiento = (rs.getString(3)  ==null)?"":rs.getString(3).trim();
					 fecha_venc_pyme   = (rs.getString(17) ==null)?"":rs.getString(17).trim();
					 fecha_cambio      = (rs.getString(5)  ==null)?"":rs.getString(5).trim();
					 moneda            = (rs.getString(7)  ==null)?"":rs.getString(7).trim();
					 tipo_Factoraje    = (rs.getString(20) ==null)?"":rs.getString(20).trim();
					 monto             = (rs.getString(8)  ==null)?"":rs.getString(8).trim();
					 tipo_Cambio       = (rs.getString(10) ==null)?"":rs.getString(10).trim();
					 causa             = (rs.getString(11) ==null)?"":rs.getString(11).trim();
					 usuario           = (rs.getString(18) ==null)?"":rs.getString(18).trim();
					 monto_anterior    = (rs.getString(13) ==null)?"":rs.getString(13).trim();
					 mandante          = (rs.getString(19) ==null)?"":rs.getString(19).trim();

					 if(Integer.parseInt( rs.getString(12)) == 8){
						monto = (Integer.parseInt( rs.getString(12)) == 28)?rs.getString("fn_monto_nuevo").trim():rs.getString(8).trim();
						if(Integer.parseInt( rs.getString(12)) == 28){ //si ic_cambio_estatus=28 (aplicacion de nota de credito)
							 tipo_Cambio = rs.getString(10).trim()+"\n"+rs.getString(5).trim();
						}
						if(bOperaFactorajeVencido  || icCambioEstatus.equals("29")  ||  bOperaFactConMandato || bOperaFactorajeVencInfonavit){
							NombreIf = NombreIf;
						} else{
							NombreIf ="";
						}
					}
					if(Integer.parseInt( rs.getString(12)) == 28){
						monto_anterior =monto_anterior;
					}else {
						monto_anterior ="";
					}
					contenidoArchivo.append(nombrePyme.replaceAll(",", "")+ ",");
					contenidoArchivo.append(num_documento.replaceAll(",", "")+ ",");
					contenidoArchivo.append(fecha_emision.replaceAll(",", "")+ ",");
					contenidoArchivo.append(fecha_vencimiento.replaceAll(",", "")+ ",");
					if(!"".equals(sFechaVencPyme)) {
					contenidoArchivo.append(fecha_venc_pyme.replaceAll(",", "")+ ",");
					}
					contenidoArchivo.append(fecha_cambio.replaceAll(",", "")+ ",");
					contenidoArchivo.append(moneda.replaceAll(",", "")+ ",");
					if(bTipoFactoraje || icCambioEstatus.equals("29") ) {
					contenidoArchivo.append(tipo_Factoraje.replaceAll(",", "")+ ",");
					}
					contenidoArchivo.append(monto.replaceAll(",", "")+ ",");
					contenidoArchivo.append(mtodesc.toPlainString().replaceAll(",", "")+ ",");
					contenidoArchivo.append( tipo_Cambio.replaceAll(",", "")+ ",");
					contenidoArchivo.append(causa.replaceAll(",", "")+ ",");
					contenidoArchivo.append(usuario.replaceAll(",", "")+ ",");
					contenidoArchivo.append(monto_anterior.replaceAll(",", "")+ ",");
					if(bTipoFactoraje || icCambioEstatus.equals("29")  || bOperaFactConMandato || bOperaFactorajeVencInfonavit) {
					contenidoArchivo.append(NombreIf.replaceAll(",", "")+ ",");
					}
					if(bOperaFactConMandato){
					contenidoArchivo.append(mandante.replaceAll(",", "")+ ",");
					}
					contenidoArchivo.append("\n");

					countReg++;
				}

				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();

			} else if(tipo.equals("PDF")){

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));  
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				int columnas =12;

				if(!"".equals(sFechaVencPyme)) { columnas =columnas+1; }
				if(bTipoFactoraje || icCambioEstatus.equals("29") ) { columnas =columnas+1; }
				if(bTipoFactoraje || icCambioEstatus.equals("29")  || bOperaFactConMandato || bOperaFactorajeVencInfonavit) { columnas =columnas+1; }
				if(bOperaFactConMandato){  columnas =columnas+1; }

				pdfDoc.setLTable(columnas, 100);                    
				pdfDoc.setLCell("Proveedor ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Num. Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Emisi�n ","celda01",ComunesPDF.CENTER);
				if(!"".equals(sFechaVencPyme)) {
					pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell("Fecha Vencimiento Proveedor ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de cambio de estatus ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje || icCambioEstatus.equals("29") ) {
					pdfDoc.setLCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell("Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Descontar ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cambio de estatus ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Usuario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Anterior ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje || icCambioEstatus.equals("29")  || bOperaFactConMandato || bOperaFactorajeVencInfonavit) {
					pdfDoc.setLCell("Nombre IF ","celda01",ComunesPDF.CENTER);
				}
				if(bOperaFactConMandato){ 
				pdfDoc.setLCell("Mandante ","celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setLHeaders();

				countReg = 0;
				while (rs.next()) {

					if(Integer.parseInt( rs.getString(12)) == 28){ //aplicacion de nota de credito
						mtodoc=new BigDecimal(rs.getString("fn_monto_nuevo"));
					}else{
						mtodoc=new BigDecimal(rs.getString(8));
					}

					if ("1".equals(rs.getString(6))) {
						mtodesc=mtodoc.multiply(porcentaje);
					} else if("54".equals(rs.getString(6))) { //dolar
						mtodesc=mtodoc.multiply(porcentajeDL);
					}

					nombreTipoFactoraje = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE");

					// Fodea 042 - 2009 Factoraje Vencimiento Infonavit 
					if(bOperaFactorajeVencido || bOperaFactorajeVencInfonavit) { // Para Factoraje Vencido o Vencimiento Infonavit
						tipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
						// si factoraje vencido � Vencimiento Infonavit: se aplica 100% de anticipo
						NombreIf = "";
						if(tipoFactoraje.equals("V") || tipoFactoraje.equals("I")||tipoFactoraje.equals("A") )  {
							mtodesc = mtodoc;
							NombreIf = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
						}
						if(tipoFactoraje.equals("C"))  {
							mtodesc = mtodoc;
						}
					}
					// Fodea 023 2009 mandato
					if(tipoFactoraje.equals("M") && bOperaFactConMandato)  {                   
						NombreIf = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					}
					if(icCambioEstatus.equals("29")){//Estatus 29 = Programado a Negociable Foda 025-2005
						tipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
						NombreIf = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
						if ("1".equals(rs.getString(6))) {
							mtodesc=mtodoc.multiply(porcentaje);
						} else if("54".equals(rs.getString(6))) {//dolar
							mtodesc=mtodoc.multiply(porcentajeDL);
						}
					}
					//para los totales 
					if ("1".equals(rs.getString(6))) {   //moneda nacional
						montoTotalDsctoMN = montoTotalDsctoMN.add(mtodesc);
						montoTotalMN = montoTotalMN + Double.parseDouble((String)rs.getString(8));
						numeroTotalDocsMN++;
					}
					else if("54".equals(rs.getString(6))) { //dolar
						montoTotalDsctoDL = montoTotalDsctoDL.add(mtodesc);
						montoTotalDL = montoTotalDL + Double.parseDouble((String)rs.getString(8)); 
						numeroTotalDocsDols++;
					}

					nombrePyme = (rs.getString(1)==null)?"":rs.getString(1).trim();
					num_documento = (rs.getString(2)==null)?"":rs.getString(2).trim();
					fecha_emision=  (rs.getString(4)==null)?"":rs.getString(4).trim();
					fecha_vencimiento =  (rs.getString(3)==null)?"":rs.getString(3).trim();
					fecha_venc_pyme = (rs.getString(17)==null)?"":rs.getString(17).trim();
					fecha_cambio = (rs.getString(5)==null)?"":rs.getString(5).trim();
					moneda = (rs.getString(7)==null)?"":rs.getString(7).trim();
					tipo_Factoraje = (rs.getString(20)==null)?"":rs.getString(20).trim();
					monto = (rs.getString(8)==null)?"":rs.getString(8).trim();
					tipo_Cambio = (rs.getString(10)==null)?"":rs.getString(10).trim();
					causa = (rs.getString(11)==null)?"":rs.getString(11).trim();
					usuario = (rs.getString(18)==null)?"":rs.getString(18).trim();
					monto_anterior = (rs.getString(13)==null)?"":rs.getString(13).trim();
					mandante = (rs.getString(19)==null)?"":rs.getString(19).trim();

					if(Integer.parseInt( rs.getString(12)) == 8){ 
						monto = (Integer.parseInt( rs.getString(12)) == 28)?rs.getString("fn_monto_nuevo").trim():rs.getString(8).trim();
						if(Integer.parseInt( rs.getString(12)) == 28){ //si ic_cambio_estatus=28 (aplicacion de nota de credito)
							tipo_Cambio = rs.getString(10).trim()+"\n"+rs.getString(5).trim();
						}
						if(bOperaFactorajeVencido  || icCambioEstatus.equals("29")  ||  bOperaFactConMandato || bOperaFactorajeVencInfonavit) {
							NombreIf  = NombreIf;
						} else{
							NombreIf ="";
						}
					}

					if(!icCambioEstatus.equals("28")) {
						monto_anterior = "";
					}

					pdfDoc.setLCell(nombrePyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(num_documento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_emision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_vencimiento,"formas",ComunesPDF.CENTER);
					if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setLCell(fecha_venc_pyme,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setLCell(fecha_cambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					if(bTipoFactoraje || icCambioEstatus.equals("29") ) {
						pdfDoc.setLCell(tipo_Factoraje,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setLCell(Comunes.formatoMoneda2(monto,true) ,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(Comunes.formatoMoneda2(mtodesc.toPlainString(),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(tipo_Cambio,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causa,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(usuario,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(Comunes.formatoMoneda2(monto_anterior,true) ,"formas",ComunesPDF.RIGHT);
					if(bTipoFactoraje || icCambioEstatus.equals("29")  || bOperaFactConMandato || bOperaFactorajeVencInfonavit) {
						pdfDoc.setLCell(NombreIf,"formas",ComunesPDF.LEFT);
					}
					if(bOperaFactConMandato){
						pdfDoc.setLCell(mandante,"formas",ComunesPDF.LEFT);
					}
					countReg++;
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}
			System.out.println("session.getAttribute(\"strNombre\") === "+ session.getAttribute("strNombre"));
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {e.printStackTrace();}
		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}


	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIcPyme() {
		return icPyme;
	}

	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIgNumeroDocto() {
		return igNumeroDocto;
	}

	public void setIgNumeroDocto(String igNumeroDocto) {
		this.igNumeroDocto = igNumeroDocto;
	}

	public String getDfFechaVencMin() {
		return dfFechaVencMin;
	}

	public void setDfFechaVencMin(String dfFechaVencMin) {
		this.dfFechaVencMin = dfFechaVencMin;
	}

	public String getDfFechaVencMax() {
		return dfFechaVencMax;
	}

	public void setDfFechaVencMax(String dfFechaVencMax) {
		this.dfFechaVencMax = dfFechaVencMax;
	}

	public String getIcMoneda() {
		return icMoneda;
	}

	public void setIcMoneda(String icMoneda) {
		this.icMoneda = icMoneda;
	}

	public String getIcCambioEstatus() {
		return icCambioEstatus;
	}

	public void setIcCambioEstatus(String icCambioEstatus) {
		this.icCambioEstatus = icCambioEstatus;
	}

	public String getFnMontoMin() {
		return fnMontoMin;
	}

	public void setFnMontoMin(String fnMontoMin) {
		this.fnMontoMin = fnMontoMin;
	}

	public String getFnMontoMax() {
		return fnMontoMax;
	}

	public void setFnMontoMax(String fnMontoMax) {
		this.fnMontoMax = fnMontoMax;
	}

	public String getDcFechaCambioMin() {
		return dcFechaCambioMin;
	}

	public void setDcFechaCambioMin(String dcFechaCambioMin) {
		this.dcFechaCambioMin = dcFechaCambioMin;
	}

	public String getDcFechaCambioMax() {
		return dcFechaCambioMax;
	}

	public void setDcFechaCambioMax(String dcFechaCambioMax) {
		this.dcFechaCambioMax = dcFechaCambioMax;
	}

	public String getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}

	public String getTxt_opera_fac_venc() {
		return txt_opera_fac_venc;
	}

	public void setTxt_opera_fac_venc(String txt_opera_fac_venc) {
		this.txt_opera_fac_venc = txt_opera_fac_venc;
	}

	public String getStrAforo() {
		return strAforo;
	}

	public void setStrAforo(String strAforo) {
		this.strAforo = strAforo;
	}

	public String getStrAforoDL() {
		return strAforoDL;
	}

	public void setStrAforoDL(String strAforoDL) {
		this.strAforoDL = strAforoDL;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public int getCountReg() {
		return countReg;
	}

}