package com.netro.descuento;

import com.netro.exception.NafinException;
import java.io.OutputStreamWriter;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import com.netro.pdf.ComunesPDF;
import javax.servlet.http.HttpSession;

/**
 * Clase para realizar la consulta de los  documentos  de factoraje distriuido
 */
public class ConsRevisionJuridica implements  IQueryGeneratorRegExtJS{	
	
    private List conditions;
    StringBuilder strQuery;
    
    private String icEpo   = "";  
    private String icPyme   = "";
    private String dfFechaVencMin   = "";
    private String dfFechaVencMax   = "";
    private String numContrato   = "";
    private String numDocumento   = "";
    private String numCopade   = "";
    private String campo1   = "";
    private String campo2   = "";
    private String campo3   = "";
    private String campo4   = ""; 
    private String campo5   = "";
    
    private int numCamposAdic =0;
    private String camposAdicionales ="";
     
    //para el PDF  del Acuse 
    private String acuse ="";
    private String acuseFormateado ="";
    private String recibo ="";
    private String fechaValida = "";
    private String horaValida = "";
    private String usuarioValida ="";    
    
    private int totalDoctosMN =0;
    private String totalMontoDoctosMN ="";
    private String totalMontoDescontarMN ="";  
	
    private int totalDoctosDL =0;
    private String totalMontoDoctosDL ="";
    private String totalMontoDescontarDL ="";
    
   
   	
    private static final Log LOG = ServiceLocator.getInstance().getLog(ConsRevisionJuridica.class);//Variables para enviar mensajes al Log.
    
    /**
     *Metodo para obtener los totales 
     * @return
     */
    public String getAggregateCalculationQuery() {
	return "";
    }
    
    /**
     * 
     * @return
     */
    public String getDocumentQuery(){
	conditions 	= new ArrayList();
	strQuery		= new StringBuilder();
	return strQuery.toString();
    }

    /**
     *metodo para sacar paginaci�n
     * @param pageIds
     * @return
     */
    public String getDocumentSummaryQueryForIds(List pageIds){
	conditions = new ArrayList();
	strQuery = new StringBuilder();
	return strQuery.toString();
    }
	
    /**
     * metodo para sacar cuando no hay paginaci�n
     * @return
     */
    public String getDocumentQueryFile(){
	conditions = new ArrayList();
	strQuery = new StringBuilder();
	
	strQuery.append( " SELECT   'N' as SELECCION ,  "+
			 " d.ic_documento  AS IC_DOCUMENTO, "+
			 " pe.cg_pyme_epo_interno  AS CLAVE_PROVEEDOR,"+
			 " p.cg_razon_social  AS NOMBRE_PROVEEDOR, "+
			 " d.IG_NUMERO_DOCTO  AS NUM_DOCUMENTO, "+
			 " TO_CHAR (d.df_alta, 'dd/mm/yyyy')  AS FECHA_EMISION,  "+
			 " TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy')  AS FECHA_VENC, "+
			 " d.ic_moneda as IC_MONEDA , "+
			 " mon.CD_NOMBRE  AS MONEDA, "+
			 " d.fn_monto  AS MONTO_DOCTO, "+
			 " (d.fn_monto * 1) AS MONTO_DESCONTAR, "+
			 " d.cg_campo1 AS CAMPO0,  "+
			 " d.cg_campo2  AS CAMPO1, "+
			 " d.cg_campo3  AS CAMPO2, "+
			 " d.cg_campo4  AS CAMPO3, "+
			 " d.cg_campo5  AS CAMPO4, "+
			 " d.CG_NUM_CONTRATO  AS NUM_CONTRATO, "+
			 " d.CG_NUM_COPADE  AS NUM_COPADE, "+
			 " i.ic_if  AS CLAVE_CESIONARIO, "+
			 " i.cg_razon_social  AS NOMBRE_CESIONARIO, "+
			 " ds.ic_if  AS CLAVE_INTERMEDIARIO,  "+
			 " ifs.cg_razon_social AS IF_FONDEADOR     "+

			 " FROM com_documento d, "+
			 " com_docto_seleccionado ds, "+
			 " comcat_if i, "+
			 " comcat_moneda mon, "+
			 " comcat_pyme p, "+
			 " comrel_if_epo ie, "+
			 " comrel_pyme_epo pe,  "+
			 " comcat_if ifs   "+
			 
			 " WHERE d.ic_estatus_docto = ?  "+			  
			 " AND d.cs_dscto_especial = ? "+
			 " AND d.ic_beneficiario = i.ic_if  "+
			 " AND d.ic_moneda = mon.ic_moneda  "+
			 " AND d.ic_pyme = p.ic_pyme  "+
			 " AND ie.ic_if(+) = d.ic_beneficiario  "+			
			 " AND d.ic_epo = pe.ic_epo  "+
			 " AND d.ic_pyme = pe.ic_pyme  "+
			 " and d.ic_documento = ds.ic_documento "+
			 " and ds.ic_if = ifs.ic_if  " );   
		
		conditions.add("3");
		
		conditions.add("D");
	
	    if (!"".equals(icEpo)   ) {
	      strQuery.append(" AND d.ic_epo =  ?  ");
	      conditions.add(icEpo);
	      
	      strQuery.append(" AND ie.ic_epo(+) =  ?  ");
	      conditions.add(icEpo);
	    }
	    
	    if (!"".equals(icPyme)   ) {
	      strQuery.append(" AND d.ic_pyme=  ?  ");
	      conditions.add(icPyme);
	    }
	if (!"".equals(dfFechaVencMin)   &&  !"".equals(dfFechaVencMax)   ) {
	    strQuery.append(" and d.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') and d.df_fecha_venc < TO_DATE(?,'dd/mm/yyyy') + 1 ");
	    conditions.add(dfFechaVencMin);
	    conditions.add(dfFechaVencMax);
	}
	
	if (!"".equals(numContrato)    ) {
	    strQuery.append("  AND d.CG_NUM_CONTRATO LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ") ; 
	    conditions.add(numContrato);
	}
	
	if (!"".equals(numDocumento)    ) {
	    strQuery.append(" AND d.ig_numero_docto  LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ") ;
	    conditions.add(numDocumento);
	}
	
	if (!"".equals(numCopade)    ) {
	    strQuery.append(" AND d.CG_NUM_COPADE LIKE  NVL (REPLACE (UPPER (?), '*', '%'), '%') ") ;
	    conditions.add(numCopade);
	}
	
	if (!"".equals(campo1)    ) {
	    strQuery.append("   AND d.cg_campo1 = ? ") ;
	    conditions.add(campo1);
	}
	
	if (!"".equals(campo2)    ) {
	    strQuery.append("   AND d.cg_campo2 = ?  ") ;
	    conditions.add(campo2);
	}
	if (!"".equals(campo3)    ) {
	    strQuery.append("   AND d.cg_campo3 = ?  ") ;
	    conditions.add(campo3);
	}
	
	if (!"".equals(campo4)    ) {
	     strQuery.append("   AND d.cg_campo4 = ?  ") ;
	    conditions.add(campo4);
	}
	if (!"".equals(campo5)    ) {
	     strQuery.append("   AND d.cg_campo5 = ? ") ;
	    conditions.add(campo5);
	}
	
	
	if (!"".equals(acuse)    ) {
	    strQuery.append(" AND d.CC_ACUSE_JUR = ?  " +
			    " AND d.CS_VALIDACION_JUR = ?   ");
	    conditions.add(acuse);
	    conditions.add("S");	    
	}else {
	    strQuery.append(" AND d.CS_VALIDACION_JUR = ?   ");
	    conditions.add("N");	      
	}
	
	 strQuery.append(" ORDER BY  d.ic_documento ASC   ");
	
	
	
	LOG.debug(" strQuery.toString()  "+ strQuery.toString()  );
	LOG.debug(" conditions  "+ conditions   );
		
	return strQuery.toString();   
    }
  
    /**
     * metodo para crear  archivos 
     * @param request
     * @param rs
     * @param path
     * @param tipo
     * @return
     */
    public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)  {
	
	String nombreArchivo = "";	
	OutputStreamWriter writer = null;
	BufferedWriter buffer = null;
	HttpSession session = request.getSession();	
	ComunesPDF pdfDoc = new ComunesPDF();
	StringBuilder linea = new StringBuilder();  
	LOG.debug(" crearCustomFile (E)  " );
	
	try {	
	  
	   if ("CSV".equals(tipo)   ) { //archivo del grid de consulta       
	       
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		buffer = new BufferedWriter(writer);
		linea = new StringBuilder(); 
		linea.append("Clave del Proveedor, Nombre del Proveedor, N�mero de Documento,  Fecha de Emisi�n , " +
		       " Fecha de Vencimiento, Moneda, Monto del Documento,  Monto a Descontar,  "+camposAdicionales+		       
		       " , N�mero de Contrato,  N�mero COPADE, Clave Cesionario,  Nombre Cesionario (Beneficiario),  Clave del Int. Financiero,  " +
		       " IF Fondeador \n");
		buffer.write(linea.toString()); 
		while (rs.next()) {
		    String claveProveedor    	= (rs.getString("CLAVE_PROVEEDOR") == null) ? "" : rs.getString("CLAVE_PROVEEDOR");
		    String nombreProveedor    	= (rs.getString("NOMBRE_PROVEEDOR") == null) ? "" : rs.getString("NOMBRE_PROVEEDOR");
		    String nmDocumento    	= (rs.getString("NUM_DOCUMENTO") == null) ? "" : rs.getString("NUM_DOCUMENTO");
		    String fechaEmision    	= (rs.getString("FECHA_EMISION") == null) ? "" : rs.getString("FECHA_EMISION");
		    String fechaVencimiento    	= (rs.getString("FECHA_VENC") == null) ? "" : rs.getString("FECHA_VENC");
		    String moneda    		= (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
		    String montoDocto    	= (rs.getString("MONTO_DOCTO") == null) ? "" : rs.getString("MONTO_DOCTO");
		    String montoDescontar    	= (rs.getString("MONTO_DESCONTAR") == null) ? "" : rs.getString("MONTO_DESCONTAR");
		    String nmContrato        	= (rs.getString("NUM_CONTRATO") == null) ? "" : rs.getString("NUM_CONTRATO");		
		    String nmCopade          	= (rs.getString("NUM_COPADE") == null) ? "" : rs.getString("NUM_COPADE");
		    String claveCesionario    	= (rs.getString("CLAVE_CESIONARIO") == null) ? "" : rs.getString("CLAVE_CESIONARIO");
		    String nombreCesionario   	= (rs.getString("NOMBRE_CESIONARIO") == null) ? "" : rs.getString("NOMBRE_CESIONARIO");
		    String claveIntermediario 	= (rs.getString("CLAVE_INTERMEDIARIO") == null) ? "" : rs.getString("CLAVE_INTERMEDIARIO");
		    String ifFondeador        	= (rs.getString("IF_FONDEADOR") == null) ? "" : rs.getString("IF_FONDEADOR");
		   	  
		    linea = new StringBuilder(); 
		    linea.append( claveProveedor.replace(',',' ')    + ", " +
			    nombreProveedor.replace(',',' ')    + ", " +
			    nmDocumento.replace(',',' ')    + ", " +
			    fechaEmision.replace(',',' ')    + ", " +
			    fechaVencimiento.replace(',',' ')    + ", " +
			    moneda.replace(',',' ')    + ", " +
			    montoDocto.replace(',',' ')    + ", " +
			    montoDescontar.replace(',',' ')    + ", ");			    
		    for(int x=0; x<numCamposAdic;x++){				
			linea.append(  (rs.getString("CAMPO"+x) == null) ? "" : rs.getString("CAMPO"+x) + ", " );
		    }	   
		    linea.append( nmContrato.replace(',',' ')    + ", " +
		        nmCopade.replace(',',' ')    + ", " +
			claveCesionario.replace(',',' ')    + ", " +
			nombreCesionario.replace(',',' ')    + ", " +
			claveIntermediario.replace(',',' ')    + ", " +
			ifFondeador.replace(',',' ')    + "\n ") ;		    
			    
		    buffer.write(linea.toString());
		}
		rs.close();
		buffer.close();
	    
	   }else  if ("PDF".equals(tipo)   ) { //archivo del grid de consulta
	      					
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(2, path + nombreArchivo);				
		String [] meses= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		session.getAttribute("iNoNafinElectronico").toString(),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);		
		pdfDoc.addText("La autentificaci�n se llev� a cabo con �xito \n Recibo: "+recibo,"formasB",ComunesPDF.CENTER);			
		pdfDoc.addText("\n ","formas",ComunesPDF.LEFT);		
		pdfDoc.setTable(2,50);
		pdfDoc.setCell("Cifras de Control","celda01",ComunesPDF.CENTER,2);
		pdfDoc.setCell("N�mero de Acuse","formas");		
		pdfDoc.setCell(acuseFormateado,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Fecha de Validaci�n","formas");
		pdfDoc.setCell(fechaValida,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Hora de Validaci�n","formas");
		pdfDoc.setCell(horaValida,"formas",ComunesPDF.LEFT);
		pdfDoc.setCell("Usuario de Validaci�n","formas");
		pdfDoc.setCell(usuarioValida,"formas",ComunesPDF.LEFT);
		pdfDoc.addTable();		
		pdfDoc.addText("\n ","formas",ComunesPDF.LEFT);		
		int columnas  = 12;  		
		int  difColum  =  5 - numCamposAdic ;			
		
		pdfDoc.setTable(columnas);
		pdfDoc.setCell("Acuse Validaci�n de Documentos ","celda01",ComunesPDF.CENTER,columnas);
		pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Clave del Proveedor","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto del Documento","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("","celda01",ComunesPDF.CENTER, 3);
				
		pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
		List<String> list = new ArrayList<String>(Arrays.asList(camposAdicionales.split(",")));
		for (int i = 0; i <list.size(); i++) {
		    pdfDoc.setCell(list.get(i).toString(),"celda01",ComunesPDF.CENTER);
		}
		pdfDoc.setCell("N�mero de Contrato","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("N�mero COPADE","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Clave Cesionario","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre Cesionario (Beneficiario)","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Clave del Int. Financiero","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("IF Fondeador","celda01",ComunesPDF.CENTER);
		if(difColum>0){
		    pdfDoc.setCell("","celda01",ComunesPDF.CENTER, difColum);
		}				
		while (rs.next()) {
		    String claveProveedor    	= (rs.getString("CLAVE_PROVEEDOR") == null) ? "" : rs.getString("CLAVE_PROVEEDOR");
		    String nombreProveedor    	= (rs.getString("NOMBRE_PROVEEDOR") == null) ? "" : rs.getString("NOMBRE_PROVEEDOR");
		    String nmDocumento    	= (rs.getString("NUM_DOCUMENTO") == null) ? "" : rs.getString("NUM_DOCUMENTO");
		    String fechaEmision    	= (rs.getString("FECHA_EMISION") == null) ? "" : rs.getString("FECHA_EMISION");
		    String fechaVencimiento    	= (rs.getString("FECHA_VENC") == null) ? "" : rs.getString("FECHA_VENC");
		    String moneda    		= (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
		    String montoDocto    	= (rs.getString("MONTO_DOCTO") == null) ? "" : rs.getString("MONTO_DOCTO");
		    String montoDescontar    	= (rs.getString("MONTO_DESCONTAR") == null) ? "" : rs.getString("MONTO_DESCONTAR");
		    String nmContrato        	= (rs.getString("NUM_CONTRATO") == null) ? "" : rs.getString("NUM_CONTRATO");		
		    String nmCopade          	= (rs.getString("NUM_COPADE") == null) ? "" : rs.getString("NUM_COPADE");
		    String claveCesionario    	= (rs.getString("CLAVE_CESIONARIO") == null) ? "" : rs.getString("CLAVE_CESIONARIO");
		    String nombreCesionario   	= (rs.getString("NOMBRE_CESIONARIO") == null) ? "" : rs.getString("NOMBRE_CESIONARIO");
		    String claveIntermediario 	= (rs.getString("CLAVE_INTERMEDIARIO") == null) ? "" : rs.getString("CLAVE_INTERMEDIARIO");
		    String ifFondeador        	= (rs.getString("IF_FONDEADOR") == null) ? "" : rs.getString("IF_FONDEADOR");
		 
		    pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);		    
		    pdfDoc.setCell(claveProveedor,"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell(nombreProveedor,"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell(nmDocumento,"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoDocto+"", 2),"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoDescontar+"", 2),"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);	
		    		    
		    pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
		    for(int x=0; x<numCamposAdic;x++){										    
			pdfDoc.setCell( (rs.getString("CAMPO"+x) == null) ? "" : rs.getString("CAMPO"+x),"formas",ComunesPDF.CENTER);
		    }	
		    pdfDoc.setCell(nmContrato,"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell(nmCopade,"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell(claveCesionario,"formas",ComunesPDF.CENTER);		    
		    pdfDoc.setCell(nombreCesionario,"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell(claveIntermediario,"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell(ifFondeador,"formas",ComunesPDF.CENTER);
		    if(difColum>0){
			pdfDoc.setCell("","formas",ComunesPDF.CENTER, difColum);
		    }
		}
		pdfDoc.addTable();
		
		//totales  
		pdfDoc.addText("\n ","formas",ComunesPDF.LEFT);
		pdfDoc.setTable(4,50);
		pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Total de Documentos","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Total de Monto de Documentos","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Total de Monto a Descontar","celda01",ComunesPDF.CENTER);		
		if(totalDoctosMN>0){
		    pdfDoc.setCell("MONEDA NACIONAL","formas",ComunesPDF.CENTER);
		    pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoDoctosMN+"", 2),"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoDescontarMN+"", 2),"formas",ComunesPDF.CENTER);
		}
		if(totalDoctosDL>0){
		    pdfDoc.setCell("DOLAR AMERICANO","formas",ComunesPDF.CENTER);
		    pdfDoc.setCell(String.valueOf(totalDoctosDL) ,"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoDoctosDL+"", 2),"formas",ComunesPDF.CENTER);
		    pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalMontoDescontarDL+"", 2),"formas",ComunesPDF.CENTER);
		}		
		pdfDoc.addTable();
		
		pdfDoc.endDocument();
		
	   }
		LOG.debug(" nombreArchivo  "+nombreArchivo );
	    
	}catch (Throwable e) {
	    throw new AppException("Error al generar el archivo",e);
	}
	LOG.debug(" crearCustomFile (s)  " );
	return nombreArchivo;
    }
    
    /**
     *  metodo para crear archivo con paginaci�n
     * @param request
     * @param reg
     * @param path
     * @param tipo
     * @return
     */
    public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
	return "";
    }	
   
  
    /**
     * Metodo para obtener el nombre de la Pyme 
     * @param txtNafelec
     * @return
     * @throws AppException
     */
  public Map<String,Object> getNombrePyme(int txtNafelec ) throws AppException {
	
	LOG.info ("getNombrePyme e ");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	ResultSet rs = null;
	StringBuilder strSQL = new StringBuilder(); 
	Map<String,Object> datos = new HashMap<>();
	
	try{
	    
	    con.conexionDB();
	    	    
	    strSQL.append(" SELECT  distinct pym.ic_pyme, pym.cg_razon_social"   +
			"   FROM comrel_nafin crn, comrel_pyme_epo cpe, comcat_pyme pym"   +
			"   WHERE crn.ic_epo_pyme_if = cpe.ic_pyme"   +
			"   AND cpe.ic_pyme = pym.ic_pyme"   +
			"   AND crn.ic_nafin_electronico = ?"   +
			"   AND crn.cg_tipo = 'P'"   +
			"   AND cpe.cs_habilitado = 'S'"   +
			"   AND cpe.cg_pyme_epo_interno IS NOT NULL"   );
	    
	    LOG.debug(strSQL.toString() );
	    LOG.debug(txtNafelec );
	    ps = con.queryPrecompilado(strSQL.toString());
	    ps.setInt(1,txtNafelec);
	    rs = ps.executeQuery();
	    
	    if(rs.next()){
		datos.put("icPyme",rs.getString(1)==null?"":rs.getString(1));
		datos.put("txtNombre", rs.getString(2)==null?"":rs.getString(2));				
	    }
	    rs.close();
	    ps.close();
		
	    return datos;
		
	}catch(Exception e) {
	    LOG.error("Exception en getExisteicIFxReferencia. "+e);
	    throw new AppException("Error al obtener el ic_if de la epo  pemex con respecto a la referencia  ");
	} finally {
	    if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	    }
	}	
    }
  
    /**
     * metodo para realizar la revisi�n
     * @param datos
     * @param icDocumento
     * @return
     * @throws NafinException
     */
    public boolean  procesaRevJuridica( Map<String,Object> datos ,  String [] icDocumento ) throws NafinException {
    
	LOG.info ("procesaRevJuridica e ");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	PreparedStatement ps1 = null;
	StringBuilder strSQL = new StringBuilder(); 
	boolean exito = false;
	List varBind;
	varBind = new ArrayList();
	try{
	    
	    con.conexionDB();
	    	    
	    strQuery = new StringBuilder(); 
	    strSQL.append(  " insert into com_acuse1(cc_acuse, in_total_docto_mn, fn_total_monto_mn, "+
			"  in_total_docto_dl, fn_total_monto_dl, ic_usuario,  cg_recibo_electronico, df_fechahora_carga ) "+
			" values(?, ?, ? , ? , ?, ?, ?, to_date('"+(String)datos.get("FECHA")+"','dd/mm/yyyy') ) " );	  
	    
	    varBind = new ArrayList();
	    varBind.add((String)datos.get("ACUSE") );	
	    varBind.add((String)datos.get("TOTALMN") );	
	    varBind.add((String)datos.get("TMONTOMN") );
	    varBind.add((String)datos.get("TOTALDL") );	
	    varBind.add((String)datos.get("TMONTODL")  );
	    varBind.add((String)datos.get("USUARIO")  );
	    varBind.add((String) datos.get("RECIBO")  );
	    
	    LOG.debug("strSQL   "+strSQL.toString() +"\n varBind "+varBind );
	    
	    ps = con.queryPrecompilado(strSQL.toString(), varBind);
	    ps.executeUpdate();
	    ps.close();
	      
	    for(int i=0;i<icDocumento.length;i++) {
		strSQL = new StringBuilder(); 
		strSQL.append(  " Update com_documento   "+
			    " set CS_VALIDACION_JUR  = ? ,   "+
			    " CC_ACUSE_JUR   = ?  "+ 
			    " WHERE  IC_DOCUMENTO = ?  ");
		varBind = new ArrayList();
		varBind.add("S");
		varBind.add((String)datos.get("ACUSE"));
		varBind.add(icDocumento[i]);		
		LOG.debug("strSQL   "+strSQL.toString() +"\n varBind "+varBind );				
		ps1 = con.queryPrecompilado(strSQL.toString(), varBind);
		ps1.executeUpdate();
		ps1.close();
	  }
	    	    
	    exito = true;
	}catch(Exception e) {
	    LOG.error("Exception en procesaRevJuridica. "+e);
	    throw new AppException("Error al realizar la revisi�n Juridica del factoraje distribuido  ");
	} finally {
	    if (con.hayConexionAbierta()) {
		con.terminaTransaccion(exito);
		con.cierraConexionDB();
	    }
	}	
	return exito;
  }
  
  
   public List getConditions(){
	return conditions;
    }


    public void setIcEpo(String icEpo) {
	this.icEpo = icEpo;
    }

    public String getIcEpo() {
	return icEpo;
    }

    public void setIcPyme(String icPyme) {
	this.icPyme = icPyme;
    }

    public String getIcPyme() {
	return icPyme;
    }


    public void setDfFechaVencMin(String dfFechaVencMin) {
	this.dfFechaVencMin = dfFechaVencMin;
    }

    public String getDfFechaVencMin() {
	return dfFechaVencMin;
    }

    public void setDfFechaVencMax(String dfFechaVencMax) {
	this.dfFechaVencMax = dfFechaVencMax;
    }

    public String getDfFechaVencMax() {
	return dfFechaVencMax;
    }

    public void setNumContrato(String numContrato) {
	this.numContrato = numContrato;
    }

    public String getNumContrato() {
	return numContrato;
    }

    public void setNumDocumento(String numDocumento) {
	this.numDocumento = numDocumento;
    }

    public String getNumDocumento() {
	return numDocumento;
    }

    public void setNumCopade(String numCopade) {
	this.numCopade = numCopade;
    }

    public String getNumCopade() {
	return numCopade;
    }

    public void setCampo1(String campo1) {
	this.campo1 = campo1;
    }

    public String getCampo1() {
	return campo1;
    }

    public void setCampo2(String campo2) {
	this.campo2 = campo2;
    }

    public String getCampo2() {
	return campo2;
    }

    public void setCampo3(String campo3) {
	this.campo3 = campo3;
    }

    public String getCampo3() {
	return campo3;
    }

    public void setCampo4(String campo4) {
	this.campo4 = campo4;
    }

    public String getCampo4() {
	return campo4;
    }

    public void setCampo5(String campo5) {
	this.campo5 = campo5;
    }

    public String getCampo5() {
	return campo5;
    }

    public void setAcuse(String acuse) {
	this.acuse = acuse;
    }

    public String getAcuse() {
	return acuse;
    }

    public void setRecibo(String recibo) {
	this.recibo = recibo;
    }

    public String getRecibo() {
	return recibo;
    }
    public void setFechaValida(String fechaValida) {
	this.fechaValida = fechaValida;
    }

    public String getFechaValida() {
	return fechaValida;
    }

    public void setHoraValida(String horaValida) {
	this.horaValida = horaValida;
    }

    public String getHoraValida() {
	return horaValida;
    }

    public void setUsuarioValida(String usuarioValida) {
	this.usuarioValida = usuarioValida;
    }

    public String getUsuarioValida() {
	return usuarioValida;
    }

    public void setNumCamposAdic(int numCamposAdic) {
	this.numCamposAdic = numCamposAdic;
    }

    public int getNumCamposAdic() {
	return numCamposAdic;
    }

    public void setCamposAdicionales(String camposAdicionales) {
	this.camposAdicionales = camposAdicionales;
    }

    public String getCamposAdicionales() {
	return camposAdicionales;
    }

    public void setTotalMontoDoctosMN(String totalMontoDoctosMN) {
	this.totalMontoDoctosMN = totalMontoDoctosMN;
    }

    public String getTotalMontoDoctosMN() {
	return totalMontoDoctosMN;
    }


    public void setTotalMontoDoctosDL(String totalMontoDoctosDL) {
	this.totalMontoDoctosDL = totalMontoDoctosDL;
    }

    public String getTotalMontoDoctosDL() {
	return totalMontoDoctosDL;
    }

    public void setTotalDoctosMN(int totalDoctosMN) {
	this.totalDoctosMN = totalDoctosMN;
    }

    public int getTotalDoctosMN() {
	return totalDoctosMN;
    }

    public void setTotalDoctosDL(int totalDoctosDL) {
	this.totalDoctosDL = totalDoctosDL;
    }

    public int getTotalDoctosDL() {
	return totalDoctosDL;
    }

    public void setTotalMontoDescontarMN(String totalMontoDescontarMN) {
	this.totalMontoDescontarMN = totalMontoDescontarMN;
    }

    public String getTotalMontoDescontarMN() {
	return totalMontoDescontarMN;
    }

    public void setTotalMontoDescontarDL(String totalMontoDescontarDL) {
	this.totalMontoDescontarDL = totalMontoDescontarDL;
    }

    public String getTotalMontoDescontarDL() {
	return totalMontoDescontarDL;
    }

    public void setAcuseFormateado(String acuseFormateado) {
	this.acuseFormateado = acuseFormateado;
    }

    public String getAcuseFormateado() {
	return acuseFormateado;
    }

}
