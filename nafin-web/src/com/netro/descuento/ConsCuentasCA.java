package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCuentasCA implements IQueryGeneratorRegExtJS {
 
private final static Log log = ServiceLocator.getInstance().getLog(ConsCuentasCA.class);
 
	StringBuffer 		query;
	private List 		conditions;
	
	/*
	private String 	operacion;
	private String 	paginaOffset;
	private String 	paginaNo;
	*/
	
	//Constructor
	public ConsCuentasCA(){}
	
	/**
	 * M�todo para obtener las llaves primarias
	 * @return 
	 */
	public String getDocumentQuery(){
		
		query 							= new StringBuffer();
		conditions 						= new ArrayList();
		
		String	tablaEpo				= "";
		String	condicionEpo		= "";
		String 	campoEpo 			= "'' NOMBRE_EPO ";
		
		boolean 	agregarClaveEpo 	= false;
		if( 
				"P".equals(this.tipoAfiliado) ||
				"I".equals(this.tipoAfiliado) ||
				"B".equals(this.tipoAfiliado)
		){
		
			tablaEpo 		= " ,comcat_epo epo ";
			condicionEpo 	= " AND epo.ic_epo = c.ic_epo ";
			campoEpo 		= " epo.cg_razon_social AS NOMBRE_EPO ";
 
			if("EPO".equals(this.tipoUsuario)) {
				condicionEpo 		+= " AND epo.ic_epo = ? ";
				agregarClaveEpo 	= 	true;
			}
			
		}
		
		// Construir query
		query.append(
			" 	  SELECT   	                                                      "  +
			"				c.ic_cuenta                                             	"  +
			"     FROM                                                           "  +
			"          	com_cuentas        c,                                    "  +
			"          	comcat_bancos_tef  bt,                                   "  +
			"          	comcat_moneda      m                                     "
		);
		query.append( 	
			tablaEpo 
		);
		query.append(
			"    WHERE c.ic_moneda          = m.ic_moneda                        "  +
			"      AND c.ic_bancos_tef      = bt.ic_bancos_tef                   "
		);
		query.append( 
			condicionEpo 
		);
		query.append(
			"      AND ic_nafin_electronico = ?                                  "  + 
			"      AND ic_producto_nafin    = ?                                  "  + 
			"		 AND c.ic_tipo_cuenta     != 1                                 "  + 
			" ORDER BY c.ic_tipo_cuenta DESC                                     "  
		);
		
		// Agregar condiciones
		if(agregarClaveEpo){
			conditions.add(this.claveEPO);
		}
		conditions.add(this.numeroNafinElectronico);
		conditions.add(this.claveProducto);
		
		log.debug("getDocumentQuery.claveEPO                = <" + this.claveEPO          		 +">");
		log.debug("getDocumentQuery.numeroNafinElectronico  = <" + this.numeroNafinElectronico  +">");
		log.debug("getDocumentQuery.claveProducto           = <" + this.claveProducto     		 +">");
		
		log.debug("getDocumentQuery.query                   = <" + query.toString()     			 +">");
		log.debug("getDocumentQuery.conditions              = <" + conditions                   +">");
		
		return query.toString();
		
	}//getDocumentQuery
	
	/**
	 * M�todo para obtener el los registros por p�gina 
	 * @return 
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		
		query 							= new StringBuffer();
		conditions 						= new ArrayList();
 
		String	tablaEpo				= "";
		String	condicionEpo		= "";
		String 	campoEpo 			= "'' NOMBRE_EPO ";
		
		boolean 	agregarClaveEpo 	= false;
		if( 
				"P".equals(this.tipoAfiliado) ||
				"I".equals(this.tipoAfiliado) ||
				"B".equals(this.tipoAfiliado)
		){
		
			tablaEpo 		= " ,comcat_epo epo ";
			condicionEpo 	= " AND epo.ic_epo = c.ic_epo ";
			campoEpo 		= " epo.cg_razon_social AS NOMBRE_EPO ";
 
			if("EPO".equals(this.tipoUsuario)) {
				condicionEpo 		+= " AND epo.ic_epo = ? ";
				agregarClaveEpo 	= 	true;
			}
			
		}
		
		// Construir query
		query.append(
			" 	  SELECT   	                                                          "  +
			"				c.ic_cuenta,                                             	 "  +
			"				DECODE (c.ic_tipo_cuenta, 40, c.cg_cuenta, 'N/A') AS CLABE,  "  +
			"          	DECODE (c.ic_tipo_cuenta, 1, c.cg_cuenta,  'N/A') AS SPEUA,  "  +
			"           c.ic_moneda,                                                 "  +
			"          	m.cd_nombre                                       AS MONEDA, "  +
			"           c.ic_bancos_tef,                                             "  +
			"           bt.cd_descripcion											  AS BANCO,  "  +
			"           c.cg_tipo_afiliado,                                          "
		);
		query.append(campoEpo);query.append(",");
		query.append( 	
			"           c.ic_tipo_cuenta,                                        							"  +
			"           DECODE (c.ic_tipo_cuenta, 50, c.cg_cuenta, 		   'N/A') AS SWIFT,  			"  +
			"			   DECODE (c.ic_tipo_cuenta, 50, c.cg_plaza_swift,    'N/A') AS PLAZA_SWIFT,   	"  +
			"			   DECODE (c.ic_tipo_cuenta, 50, c.cg_sucursal_swift, 'N/A') AS SUCURSAL_SWIFT, 	"  +
			"				DECODE (c.cg_tipo_afiliado, 'E','EPO', 'P','Proveedor', 'D','Distribuidor', 'I','IF Bancario/No Bancario', 'B','IF/Beneficiario', '' ) AS TIPO_AFILIADO "  +
			"     FROM                                                           "  +
			"          	com_cuentas        c,                                    "  +
			"          	comcat_bancos_tef  bt,                                   "  +
			"          	comcat_moneda      m                                     "
		);
		query.append( 	
			tablaEpo 
		);
		query.append(
			"    WHERE c.ic_moneda          = m.ic_moneda                        "  +
			"      AND c.ic_bancos_tef      = bt.ic_bancos_tef                   "
		);
		query.append( 
			condicionEpo 
		);
		query.append(
			"      AND ic_nafin_electronico = ?                                  "  + 
			"      AND ic_producto_nafin    = ?                                  "  +
			"		 AND c.ic_cuenta IN ( "
		);
		for(int i=0;i<pageIds.size();i++) {
			if(i>0){
				query.append(",");
			}
			query.append("?");
		};
		query.append(
			"		 )                                                             "  +
			"		 AND c.ic_tipo_cuenta     != 1                                 "  + 
			" ORDER BY c.ic_tipo_cuenta DESC                                     "  
		);
		
		// Agregar condiciones
		if(agregarClaveEpo){
			conditions.add(this.claveEPO);
		}
		conditions.add(this.numeroNafinElectronico);
		conditions.add(this.claveProducto);
		for(int i=0;i<pageIds.size();i++) {
			List item = (List)pageIds.get(i);
			conditions.add(item.get(0).toString());	
		}
		
		log.debug("getDocumentSummaryQueryForIds.claveEPO                = <" + this.claveEPO          		 	+">");
		log.debug("getDocumentSummaryQueryForIds.numeroNafinElectronico  = <" + this.numeroNafinElectronico  	+">");
		log.debug("getDocumentSummaryQueryForIds.claveProducto           = <" + this.claveProducto     		 	+">");
		log.debug("getDocumentSummaryQueryForIds.pageIds                 = <" + pageIds                  		+">");
		
		log.debug("getDocumentSummaryQueryForIds.query                   = <" + query.toString()     			+">");
		log.debug("getDocumentSummaryQueryForIds.conditions              = <" + conditions                   	+">");
		
		return query.toString();
		
	}//getDocumentSummaryQueryForIds
	
	/**
	 * M�todo para calcular los totales 
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		return "";
	}//getAggregateCalculationQuery
	
	
	public List getConditions() {
		return conditions;
	}
 
	/**
	 * M�todo para generar el Archivo  PDF o CSV con todos los registos generados por la consulta  
	 * @return 
	 */
	public String getDocumentQueryFile(){
		
		query 							= new StringBuffer();
		conditions 						= new ArrayList();
		
		String	tablaEpo				= "";
		String	condicionEpo		= "";
		String 	campoEpo 			= "'' NOMBRE_EPO ";
		
		boolean 	agregarClaveEpo 	= false;
		if( 
				"P".equals(this.tipoAfiliado) ||
				"I".equals(this.tipoAfiliado) ||
				"B".equals(this.tipoAfiliado)
		){
		
			tablaEpo 		= " ,comcat_epo epo ";
			condicionEpo 	= " AND epo.ic_epo = c.ic_epo ";
			campoEpo 		= " epo.cg_razon_social AS NOMBRE_EPO ";
 
			if("EPO".equals(this.tipoUsuario)) {
				condicionEpo 		+= " AND epo.ic_epo = ? ";
				agregarClaveEpo 	= 	true;
			}
			
		}
		
		// Construir query
		query.append(
			" 	  SELECT   	                                                      "  +
			"				c.ic_cuenta,                                             	 "  +
			"				DECODE (c.ic_tipo_cuenta, 40, c.cg_cuenta, 'N/A') AS CLABE,  "  +
			"          	DECODE (c.ic_tipo_cuenta, 1, c.cg_cuenta,  'N/A') AS SPEUA,  "  +
			"           c.ic_moneda,                                                 "  +
			"          	m.cd_nombre                                       AS MONEDA, "  +
			"           c.ic_bancos_tef,                                             "  +
			"           bt.cd_descripcion											  AS BANCO,  "  +
			"           c.cg_tipo_afiliado,                                          "
		);
		query.append(campoEpo);query.append(",");
		query.append( 	
			"           c.ic_tipo_cuenta,                                        							"  +
			"           DECODE (c.ic_tipo_cuenta, 50, c.cg_cuenta, 		   'N/A') AS SWIFT,  			"  +
			"			   DECODE (c.ic_tipo_cuenta, 50, c.cg_plaza_swift,    'N/A') AS PLAZA_SWIFT,   	"  +
			"			   DECODE (c.ic_tipo_cuenta, 50, c.cg_sucursal_swift, 'N/A') AS SUCURSAL_SWIFT, 	"  +
			"				DECODE (c.cg_tipo_afiliado, 'E','EPO', 'P','Proveedor', 'D','Distribuidor', 'I','IF Bancario/No Bancario', 'B','IF/Beneficiario', '' ) AS TIPO_AFILIADO "  +
			"     FROM                                                           "  +
			"          	com_cuentas        c,                                    "  +
			"          	comcat_bancos_tef  bt,                                   "  +
			"          	comcat_moneda      m                                     "
		);
		query.append( 	
			tablaEpo 
		);
		query.append(
			"    WHERE c.ic_moneda          = m.ic_moneda                        "  +
			"      AND c.ic_bancos_tef      = bt.ic_bancos_tef                   "
		);
		query.append( 
			condicionEpo 
		);
		query.append(
			"      AND ic_nafin_electronico = ?                                  "  + 
			"      AND ic_producto_nafin    = ?                                  "  + 
			"		 AND c.ic_tipo_cuenta     != 1                                 "  + 
			" ORDER BY c.ic_tipo_cuenta DESC                                     "  
		);
		
		// Agregar condiciones
		if(agregarClaveEpo){
			conditions.add(this.claveEPO);
		}
		conditions.add(this.numeroNafinElectronico);
		conditions.add(this.claveProducto);
		
		log.debug("getDocumentQueryFile.claveEPO                = <" + this.claveEPO          		 +">");
		log.debug("getDocumentQueryFile.numeroNafinElectronico  = <" + this.numeroNafinElectronico +">");
		log.debug("getDocumentQueryFile.claveProducto           = <" + this.claveProducto     		 +">");
		
		log.debug("getDocumentQueryFile.query                   = <" + query.toString()     		 +">");
		log.debug("getDocumentQueryFile.conditions              = <" + conditions                  +">");
		
		return query.toString();
		
	}//getDocumentQueryFile
 
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
 
		ComunesPDF 				pdfDoc 			= null;
		String 					nombreArchivo 	= null;
		
		if( "PDF".equals(tipo) ){
			
			try{
	 
				// 1. Definir nombre y ruta del Archivo PDF
				nombreArchivo 			= Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc 					= new ComunesPDF(2, path + nombreArchivo);
					
				// 2. Crear cabecera
				String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				HttpSession session 	= request.getSession();
				pdfDoc.encabezadoConImagenes(
					pdfDoc,
					session.getAttribute("strPais")==null?"":(String) session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico").toString(),
					session.getAttribute("sesExterno")==null?"":(String)	session.getAttribute("sesExterno"),
					session.getAttribute("strNombre")==null?"":(String) session.getAttribute("strNombre"),
					session.getAttribute("strNombreUsuario")==null?"":(String) session.getAttribute("strNombreUsuario"),
					session.getAttribute("strLogo")==null?"":(String)	session.getAttribute("strLogo"),
					session.getServletContext().getAttribute("strDirectorioPublicacion")==null?"":(String) session.getServletContext().getAttribute("strDirectorioPublicacion")
				);
						 
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
					
				// 3. Definir cabecera de la tabla del reporte
				pdfDoc.setTable(8,100);
				pdfDoc.setCell("Tipo de Afiliado",		"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("EPO",						"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda",					"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Banco",						"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta Clabe",			"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta Swift",			"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Plaza Swift",				"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Sucursal Swift",			"celda01rep", ComunesPDF.CENTER);
					
				//4. Agregar los registros correspondientes a la consulta
				while (reg.next()) {
						
					pdfDoc.setCell( reg.getString("TIPO_AFILIADO"),		"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( reg.getString("NOMBRE_EPO"),			"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( reg.getString("MONEDA"),				"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( reg.getString("BANCO"),				"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( reg.getString("CLABE"),				"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( reg.getString("SWIFT"),				"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( reg.getString("PLAZA_SWIFT"),		"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( reg.getString("SUCURSAL_SWIFT"),	"formasrep", ComunesPDF.CENTER );
						
				}
					
				pdfDoc.addTable();
				pdfDoc.endDocument();
	
			} catch(Throwable e) {
				
				log.error("crearPageCustomFile(Exception)");
				e.printStackTrace();
				
				throw new AppException("Error al generar el archivo", e);
				
			} finally {
				
			}
			
		}
 
		return nombreArchivo;
		
	}
 						
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {
		return "";
	}
 
	//ATRIBUTOS DE LA CLASE
	private String 	tipoAfiliado; 				// cmb_tipo_afiliado
	private String 	tipoUsuario;				// strTipoUsuario
	private String 	claveEPO;					// iNoCliente
	private String 	numeroNafinElectronico; // no_nafin
	private String 	claveProducto; 			// no_producto
 
	public String getTipoAfiliado() {

		return tipoAfiliado;

	}

 
	public void setTipoAfiliado(String tipoAfiliado) {

		this.tipoAfiliado = tipoAfiliado;

	}

 
	public String getTipoUsuario() {

		return tipoUsuario;

	}

 
	public void setTipoUsuario(String tipoUsuario) {

		this.tipoUsuario = tipoUsuario;

	}

 
	public String getClaveEPO() {

		return claveEPO;

	}


 
	public void setClaveEPO(String claveEPO) {

		this.claveEPO = claveEPO;

	}

 
	public String getNumeroNafinElectronico() {

		return numeroNafinElectronico;

	}

 
	public void setNumeroNafinElectronico(String numeroNafinElectronico) {

		this.numeroNafinElectronico = numeroNafinElectronico;

	}

 
	public String getClaveProducto() {

		return claveProducto;

	}

 
	public void setClaveProducto(String claveProducto) {

		this.claveProducto = claveProducto;

	}

	
}//ConsCuentasCA