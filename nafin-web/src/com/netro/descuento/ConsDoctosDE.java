package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CQueryHelperExtJS;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsDoctosDE implements IQueryGeneratorPS, IQueryGeneratorRegExtJS{	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsDoctosDE.class);
	private int numList = 1;
	private ArrayList valoresBind = new ArrayList();
	private int countReg;
	private List conditions;
	private String cg_pyme_epo_interno;
	private String ic_pyme;
	private String ic_if;
	private String ic_estatus_docto;
	private String ic_moneda;
	private String cc_acuse;
	private String no_docto;
	private String df_fecha_docto_de;
	private String df_fecha_docto_a;
	private String fn_monto_de;
	private String fn_monto_a;
	private String df_fecha_venc_de;
	private String df_fecha_venc_a;
	private String ord_proveedor;
	private String ord_if;
	private String ord_monto;
	private String ord_fec_venc;
	private String ord_estatus;
	private String camp_dina1;
	private String camp_dina2;
	private String operaFirmaM;
	private String sFechaVencPyme;
	private String numero_siaff;
	private String ic_documento;
	private String ic_epo;
	private String strAforo;
	private String strAforoDL;
	private String sesIdiomaUsuario;
	private String idioma; 
	private String strPerfilCons;
	private String iNoCliente;
	private String operado;
	private String banderaTablaDoctos;
	private String cmbFactoraje;
	private String validaDoctoEPO; 
	private boolean bOperaFactorajeDistribuido;
	private String contrato;
   private String copade;  
	  
	
	public ConsDoctosDE() {}
  
	public String getAggregateCalculationQuery(HttpServletRequest request){
		log.info("getAggregateCalculationQuery(E)");
		this.numList = 1;
		this.valoresBind = new ArrayList();
		List valoresBindCondiciones = new ArrayList();
		
		String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 

		String cg_pyme_epo_interno = (request.getParameter("cg_pyme_epo_interno")==null)?"":request.getParameter("cg_pyme_epo_interno").trim();
		String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme").trim();
		String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if").trim();
		String ic_estatus_docto = (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto").trim();
		String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String cc_acuse = (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String no_docto = (request.getParameter("no_docto")==null)?"":request.getParameter("no_docto");
		String df_fecha_docto_de = (request.getParameter("df_fecha_docto_de")==null)?"":request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a = (request.getParameter("df_fecha_docto_a")==null)?"":request.getParameter("df_fecha_docto_a");
		String fn_monto_de = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String df_fecha_venc_de = (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a = (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
		String ord_proveedor = (request.getParameter("ord_proveedor")==null)?"":request.getParameter("ord_proveedor");
		String ord_if = (request.getParameter("ord_if")==null)?"":request.getParameter("ord_if");
		String ord_monto = (request.getParameter("ord_monto")==null)?"":request.getParameter("ord_monto");
		String ord_fec_venc = (request.getParameter("ord_fec_venc")==null)?"":request.getParameter("ord_fec_venc");
		String ord_estatus = (request.getParameter("ord_estatus")==null)?"":request.getParameter("ord_estatus");
		String camp_dina1= (request.getParameter("camp_dina1") == null) ? "" : request.getParameter("camp_dina1");
		String camp_dina2= (request.getParameter("camp_dina2") == null) ? "" : request.getParameter("camp_dina2");
		int coma = 0;
		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
		String strAforo = (String)request.getSession().getAttribute("strAforo");
		String strAforoDL = (String)request.getSession().getAttribute("strAforoDL");
		String operaNotasC = (request.getParameter("operaNotasC") == null) ? "N" : request.getParameter("operaNotasC");
		String operaFirmaM = (request.getParameter("operaFirmaM") == null) ? "N" : request.getParameter("operaFirmaM");	//agrego parametro
		boolean muestraNegociables = false;
		muestraNegociables = "2".equals(ic_estatus_docto)?true:false;
		String querySentencia;
		String querySentenciaNego;
		String querySentenciaNC;
		StringBuffer condicion = new StringBuffer();
		String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");			
		
		String numero_siaff 		= (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
		String ic_documento		= "";
		String ic_epo				= "";
		
		String strPerfilCons = (String)request.getSession().getAttribute("sesPerfil");//FODEA 053-2009 FVR
		String cmbFactorajec    = (request.getParameter("cmbFactoraje") == null) ? "" : request.getParameter("cmbFactoraje");	
		String validaDoctoEPOc   = (request.getParameter("validaDoctoEPO") == null) ? "" : request.getParameter("validaDoctoEPO");
		 contrato   = (request.getParameter("contrato") == null) ? "" : request.getParameter("contrato");
		 copade   = (request.getParameter("copade") == null) ? "" : request.getParameter("copade");
		
  		
		if(!numero_siaff.equals("")){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);
			ic_epo 			= (String) numero.get("IC_EPO");
			ic_documento 	= (String) numero.get("IC_DOCUMENTO");
		}	


		if (!"".equals(cmbFactorajec)   ) {
		    condicion.append(" AND D.CS_DSCTO_ESPECIAL = ? ");
		    valoresBindCondiciones.add(cmbFactorajec);
		}
		if (!"".equals(validaDoctoEPOc)   ) {
		    condicion.append(" AND D.CS_VALIDACION_JUR = ? ");
		    valoresBindCondiciones.add(validaDoctoEPOc);
		}
		
		if(ic_pyme.equals("") && cc_acuse.equals("") && no_docto.equals("") &&
				ic_moneda.equals("") && df_fecha_docto_de.equals("") &&
				df_fecha_docto_a.equals("") && fn_monto_de.equals("") && fn_monto_a.equals("") &&
				df_fecha_venc_de.equals("") && df_fecha_venc_a.equals("") && ic_if.equals("") &&
				ic_estatus_docto.equals("") && camp_dina1.equals("") && camp_dina2.equals("") && 
				ic_epo.equals("") && ic_documento.equals("")){
			
			if(!"".equals(strPerfilCons) && "ADMIN EPO MAN0".equals(strPerfilCons)){//FODEA 053-2009 FVR
				condicion.append(" AND D.ic_estatus_docto in (?,?) ");
				valoresBindCondiciones.add(new Integer(30));	//Pendientes
				valoresBindCondiciones.add(new Integer(31));	//No Pendientes				
			}else if(!"".equals(strPerfilCons) && "ADMIN EPO MAN1".equals(strPerfilCons)){//FODEA 053-2009 FVR
				condicion.append(" AND D.ic_estatus_docto in (?,?,?,?) ");
				valoresBindCondiciones.add(new Integer(28));	//Pre Negociables
				valoresBindCondiciones.add(new Integer(29));	//Pre No negociables
				valoresBindCondiciones.add(new Integer(30));	//Pendientes
				valoresBindCondiciones.add(new Integer(31));	//No Pendientes
			}else{//FODEA 053-2009 FVR
				condicion.append(" AND D.ic_estatus_docto = ? ");
				muestraNegociables = true;
				valoresBindCondiciones.add(new Integer(2));
			}
		}else {
			if (!ic_pyme.equals("")) {
				condicion.append(" AND D.ic_pyme = ? ");
				valoresBindCondiciones.add(new Integer(ic_pyme));
			}
			if (!cc_acuse.equals("")) {
				if(cc_acuse.trim().indexOf("4")==0) { // si el acuse es de modificacion de montos
					condicion.append(" AND D.ic_documento in (select ic_documento from comhis_cambio_est_modif_monto where cc_acuse=? )");
					valoresBindCondiciones.add(cc_acuse);
				}
				else {
					condicion.append(" AND D.cs_cambio_importe=? AND D.cc_acuse = ? ");
					valoresBindCondiciones.add("N");
					valoresBindCondiciones.add(cc_acuse);
				}
			} //if acuse
	
			if (!no_docto.equals("")) {
				condicion.append(" AND D.ig_numero_docto = ? ");
				valoresBindCondiciones.add(no_docto);
			}
			if (!ic_moneda.equals("")) {
				condicion.append(" AND D.ic_moneda = ? ");
				valoresBindCondiciones.add(new Integer(ic_moneda));
			}
			if (!df_fecha_docto_de.equals("") && !df_fecha_docto_a.equals("")) {
				condicion.append(" AND D.df_fecha_docto >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.df_fecha_docto < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
				valoresBindCondiciones.add(df_fecha_docto_de);
				valoresBindCondiciones.add(df_fecha_docto_a);
			}
			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals("")) {
				condicion.append(" AND D.df_fecha_venc >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.df_fecha_venc < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
				valoresBindCondiciones.add(df_fecha_venc_de);
				valoresBindCondiciones.add(df_fecha_venc_a);
			}
			if (!fn_monto_de.equals("") && !fn_monto_a.equals("")) {
				condicion.append(" AND D.fn_monto BETWEEN ? and ? ");
				valoresBindCondiciones.add(new Double(fn_monto_de));
				valoresBindCondiciones.add(new Double(fn_monto_a));
			}
			if (!ic_if.equals("")) {
				condicion.append(" AND D.ic_if = ? ");
				valoresBindCondiciones.add(new Integer(ic_if));
			}
			if (!ic_estatus_docto.equals("")) {
				if (ic_estatus_docto.equals("4")) {
					condicion.append(" AND D.ic_estatus_docto in (?,?) ");
					valoresBindCondiciones.add(new Integer(4));	//Operado
					valoresBindCondiciones.add(new Integer(16));	//Aplicado a credito
				} else {
					condicion.append(" AND D.ic_estatus_docto = ? ");
					valoresBindCondiciones.add(new Integer(ic_estatus_docto));
				}
			}
			if (!camp_dina1.equals("")) {
				condicion.append(" AND D.cg_campo1 = ? ");
				valoresBindCondiciones.add(camp_dina1);
			}
			if (!camp_dina2.equals("")) {
				condicion.append(" AND D.cg_campo2 = ? ");
				valoresBindCondiciones.add(camp_dina2);
			}
	
			//FODEA 053-2009 FVR 
			if(!"ADMIN EPO MAN0".equals(strPerfilCons) && !"ADMIN EPO MAN1".equals(strPerfilCons)){//FODEA 053-2009 FVR
			
				if (!ic_if.equals("") && ic_estatus_docto.equals("") && ic_moneda.equals("")){
					//condicion.append(" AND D.ic_estatus_docto in (?,?,?,?) ");
					condicion.append(" AND D.ic_estatus_docto in (?,?,?,?,?) ");//FODEA 005 - 2009 ACF
					valoresBindCondiciones.add(new Integer(3)); //Seleccionada Pyme
					valoresBindCondiciones.add(new Integer(4)); //Operada
					valoresBindCondiciones.add(new Integer(11));	//Operada Pagada 
					valoresBindCondiciones.add(new Integer(16));	//Aplicada a credito
					valoresBindCondiciones.add(new Integer(26)); //Programado PyME-- FODEA 005 - 2009 ACF
				} else if(!ic_moneda.equals("") && ic_estatus_docto.equals("") && ic_if.equals("")){
					condicion.append(" AND D.ic_estatus_docto = ? ");
					muestraNegociables = true;
					valoresBindCondiciones.add(new Integer(2)); //Negociable
				} else if(ic_estatus_docto.equals("") || !operaFirmaM.equals("S")){
					//condicion.append(" AND D.ic_estatus_docto not in (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
					condicion.append(" AND D.ic_estatus_docto not in (?,?,?,?,?,?,?,?,?,?,?,?,?) ");//FODEA 005 - 2009 ACF
					valoresBindCondiciones.add(new Integer(8)); //Seleccionada IF (Obsoleto)
					valoresBindCondiciones.add(new Integer(13)); //Cancelado
					valoresBindCondiciones.add(new Integer(14)); //Incumplimiento Pend. de Pago
					valoresBindCondiciones.add(new Integer(15)); //Pedido sin Operar
					valoresBindCondiciones.add(new Integer(16)); //Aplicado a Credito
					valoresBindCondiciones.add(new Integer(17)); //Credito Vencido
					valoresBindCondiciones.add(new Integer(18)); //Cancelado Pendiente de Pago
					valoresBindCondiciones.add(new Integer(19)); //Pedido Sustituto
					valoresBindCondiciones.add(new Integer(20)); //Rechazado IF
					valoresBindCondiciones.add(new Integer(22)); //Pendiente de pago IF
					valoresBindCondiciones.add(new Integer(23)); //Pignorado
					//valoresBindCondiciones.add(new Integer(26)); //Programado -- FODEA 005 - 2009 ACF
					valoresBindCondiciones.add(new Integer(28)); //Pre Negociable
					valoresBindCondiciones.add(new Integer(29)); //Pre No Negociable
				}
			}//CIERRE IF PERFIL FODEA 053-2009 FVR
			// Revisar esta seccion
			if(!ic_epo.equals("")){
				condicion.append(" AND D.ic_epo = ? ");
				valoresBindCondiciones.add(new Integer(ic_epo));
			}
			if(!ic_documento.equals("")){
				condicion.append(" AND D.ic_documento = ? ");
				valoresBindCondiciones.add(new Integer(ic_documento));
			}
		} //else
		
		
		if (!"".equals(contrato)  ) {
			condicion.append(" AND upper(trim(D.CG_NUM_CONTRATO)) LIKE upper(?) ");
			valoresBindCondiciones.add(contrato);
		}
		
		if (!"".equals(copade)  ) {
			condicion.append(" AND upper(trim(D.CG_NUM_COPADE)) LIKE upper(?)  ");
			valoresBindCondiciones.add(copade);
		}
		
		String query_Reporte =
			"select "+
			"	/*+ ordered use_nl(d,m) */ " +
			" count(1) AS ConsDoctosDE "+
			",sum(fn_monto) AS FN_MONTO "+
			",sum(NVL(fn_monto_dscto,(fn_monto*decode(D.ic_moneda,1,?,54,?)))) AS FN_MONTO_DSCTO "+
			",M.cd_nombre"+ idioma + " as cd_nombre " +
			",'D '||to_char(D.ic_moneda) as moneda"+
			" FROM com_documento D,comcat_moneda M "+
			" WHERE D.ic_moneda = M.ic_moneda"+
			" AND D.ic_epo = ? "+
			" AND cs_dscto_especial != ? "+
			condicion+" "+
			" GROUP BY d.IC_MONEDA, M.CD_NOMBRE"+idioma;
			
		querySentenciaNego =
			"select "+
			"	/*+ ordered use_nl(d,m) */ " +
			" count(1) AS ConsDoctosDE "+
			",sum(fn_monto) AS FN_MONTO "+
			",sum(NVL(fn_monto_dscto,(fn_monto*decode(D.ic_moneda,1,?,54,?)))) AS FN_MONTO_DSCTO "+
			",M.cd_nombre"+idioma + " as cd_nombre " +
			",'DN'||to_char(D.ic_moneda) as moneda"+
			" FROM com_documento D,comcat_moneda M "+
			" WHERE D.ic_moneda = M.ic_moneda"+
			" AND D.ic_epo = ? "+
			" AND d.ic_estatus_docto = ? "+
			" AND cs_dscto_especial != ? "+
			condicion+" "+
			" GROUP BY d.IC_MONEDA, M.CD_NOMBRE"+idioma;
			
		querySentenciaNC =
			"select "+
			"	/*+ ordered use_nl(d,m) */ " +
			" count(1) AS ConsDoctosDE "+
			",sum(fn_monto) AS FN_MONTO "+
			",sum(NVL(fn_monto_dscto,(fn_monto*decode(D.ic_moneda,1,?,54,?)))) AS FN_MONTO_DSCTO "+
			",M.cd_nombre"+ idioma + " as cd_nombre " +
			",'NC'||to_char(D.ic_moneda) as moneda"+
			" FROM com_documento D,comcat_moneda M "+
			" WHERE D.ic_moneda = M.ic_moneda"+
			" AND D.ic_epo = ? "+
			" AND d.ic_estatus_docto = ? "+
			" AND cs_dscto_especial = ? "+
			condicion+" "+
			" GROUP BY d.IC_MONEDA, M.CD_NOMBRE"+idioma;

		
		
		//Variables Bind que siempre aplican:  (variables bind de "query_Reporte")
		this.valoresBind.add(new Double(strAforo));
		this.valoresBind.add(new Double(strAforoDL));

		this.valoresBind.add(new Integer(iNoCliente));
		this.valoresBind.add("C");
		this.valoresBind.addAll(valoresBindCondiciones);

	
		if("S".equals(operaNotasC)) {
			query_Reporte += 
				" UNION ALL "+
				querySentenciaNC;
			//Variables Bind que aplican para "querySentenciaNC"
			this.valoresBind.add(new Double(strAforo));
			this.valoresBind.add(new Double(strAforoDL));
			this.valoresBind.add(new Integer(iNoCliente));
			if (ic_estatus_docto.equals("")){
				this.valoresBind.add(new Integer(2));	//Negociable
			}else{
				this.valoresBind.add(new Integer(ic_estatus_docto));	//Estatus seleccionado
			}
			//this.valoresBind.add(new Integer(2));	//Negociable
			this.valoresBind.add("C");
			this.valoresBind.addAll(valoresBindCondiciones);
	
			if(!muestraNegociables) {
				query_Reporte += 
					" UNION ALL "+
					querySentenciaNego;
					//Variables Bind que aplican para "querySentenciaNego"
				this.valoresBind.add(new Double(strAforo));
				this.valoresBind.add(new Double(strAforoDL));
				this.valoresBind.add(new Integer(iNoCliente));
				this.valoresBind.add(new Integer(2));	//Negociable
				
				this.valoresBind.add("C");
				this.valoresBind.addAll(valoresBindCondiciones);
			}
		}
		
		querySentencia = 
			" select * from ("+
			query_Reporte+
			") "+
			" ORDER BY MONEDA ";
		log.debug(" querySentencia = "+querySentencia);		
		log.debug(" valoresBind = " +this.valoresBind);
		log.info("getAggregateCalculationQuery(S)");
		return querySentencia;
	}

	public int getNumList(HttpServletRequest request){
		return numList;	
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		numList = 5;
		
		String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 
		
		StringBuffer query = new StringBuffer();
		StringBuffer lstdoctos = new StringBuffer();
		int coma=0,criterio=0;
		String query1="";
		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
		String ord_proveedor = (request.getParameter("ord_proveedor")==null)?"0":request.getParameter("ord_proveedor");
		String ord_if = (request.getParameter("ord_if")==null)?"0":request.getParameter("ord_if");
		String ord_monto = (request.getParameter("ord_monto")==null)?"0":request.getParameter("ord_monto");
		String ord_fec_venc = (request.getParameter("ord_fec_venc")==null)?"0":request.getParameter("ord_fec_venc");
		String ord_estatus = (request.getParameter("ord_estatus")==null)?"0":request.getParameter("ord_estatus");
		String operado="";
		String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");			


		for (Iterator it = ids.iterator(); it.hasNext();){
			it.next();
			lstdoctos.append("?,");
		}
		lstdoctos = lstdoctos.delete(lstdoctos.length()-1,lstdoctos.length());
		lstdoctos.append(")");
		
		operado= (sesIdiomaUsuario.equals("EN"))?"Operate":"Operado";
		
		query.append(
			" SELECT   /*+ ordered first_rows use_nl(d modif ds det s ed m i i2 pe p ma tf) */ " +
			" D.ic_documento, "   +
			" (decode (PE.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social)as cg_razon_social_pyme,  "   +
			" D.ic_epo, "   +
			" (decode (cs_cambio_importe,'N',D.cc_acuse,'S',modif.cc_acuse)) as cc_acuse,  "   +
			" D.ig_numero_docto, "   +
			" TO_CHAR(D.df_fecha_docto,'DD/MM/YYYY') AS df_fecha_docto,  "   +
			" TO_CHAR(D.df_fecha_venc,'DD/MM/YYYY') AS df_fecha_venc, "   +
			" M.cd_nombre" +idioma+ " as cd_nombre, " +
			" D.fn_monto, "   +
			" D.cs_dscto_especial,  "   +
			" D.fn_porc_anticipo, "   +
			" D.fn_monto_dscto,  "   +
			" Decode(D.ic_estatus_docto, 16,'" +operado+"', ED.cd_descripcion" +idioma+")as cd_descripcion,  "   +
			" (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social)as cg_razon_social_if,  "   +
			" S.ic_folio, "   +
			" D.ic_estatus_docto, "   +
			" D.ic_moneda, "   +
			" PE.cg_pyme_epo_interno, "   +
			" D.ct_referencia,  "   +
			" cs_cambio_importe, "   +
			" det.detalles, "   +
			" D.cg_campo1, "   +
			" D.cg_campo2, "   +
			" D.cg_campo3, "   +
			" D.cg_campo4, "   +
			" D.cg_campo5,  "   +
			/*
			" decode(D.cs_dscto_especial, 'D', i2.cg_razon_social, 'I', i2.cg_razon_social, '') as beneficiario, "   +
			" decode(D.cs_dscto_especial, 'D', D.fn_porc_beneficiario, 'I', D.fn_porc_beneficiario, '') as fn_porc_beneficiario,  "   +
			" decode(D.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, '') as RECIBIR_BENEF,  "   +
			" decode(D.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '0') as NETO_REC_PYME ,"   +
			*/
			
			" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN i2.cg_razon_social ELSE '' END AS beneficiario, " +
			" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN d.fn_porc_beneficiario ELSE 0 END AS fn_porc_beneficiario, " +
			" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN NVL(ds.fn_importe_recibir_benef, d.fn_monto * d.fn_porc_beneficiario / 100 ) ELSE 0 END AS RECIBIR_BENEF, " +
			" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN ds.in_importe_recibir - (ds.fn_importe_recibir_benef) ELSE 0 END AS NETO_REC_PYME, " +

			//" TO_CHAR(ds.df_programacion,'DD/MM/YYYY') as df_programacion, " +
			" TO_CHAR(D.df_fecha_venc_pyme,'DD/MM/YYYY') AS df_fecha_venc_pyme, "   +
			" TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO, " +
			" ma.CG_RAZON_SOCIAL, "+
			" tf.cg_nombre as TIPO_FACTORAJE, " +
			" TO_CHAR(ds.df_programacion, 'dd/mm/yyyy') AS fecha_registro_operacion, "+//FODEA 005 - 2009 ACF
			" DECODE(                                                  "  +
			"           S.IC_ESTATUS_SOLIC,                            "  +
			"           1, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (1) Seleccionada IF
			"           2, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (2) En proceso
			"           3, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (3) Operada
			"           5, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (5) Operada Pagada
			"          10, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (10) Operado con Fondeo Propio
			"          'N/A'                                           "  + // PARA TODOS LOS OTROS CASOS
			"       ) AS FECHA_AUTORIZACION_IF,                        "  + // Fodea 040 - 2011 By JSHD 
			" 'N' nota_simple, 'N' nota_simple_docto, 'N' nota_multiple, 'N' nota_multiple_docto, '' numero_docto,"+	//Campos a llenar durante el llamado de la consulta 13consulta01ext.data.jsp
			" 'ConsDoctosDE::getDocumentSummaryQueryForIds' "   +			
			" ,(SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = (decode (cs_cambio_importe,'N',D.cc_acuse,'S',modif.cc_acuse))) AS MUESTRA_VISOR " +
			" , decode( d.cs_dscto_especial,  'D', decode (d.CS_VALIDACION_JUR, 'S', 'SI', 'N', 'NO' ), '' )  as  VALIDACION_JUR "+
			
			", d.CG_NUM_CONTRATO AS CONTRATO , d.CG_NUM_COPADE  AS COPADE "+ 
			
			" FROM "   +
			" ( select * from com_documento "+
			"   where ic_documento in ( " + lstdoctos.toString()+
			" ) D, "   +
			" (SELECT	 "   +
			" 		ic_documento, "   +
			" 		cc_acuse "   +
			"  FROM "   +
			"  	  comhis_cambio_est_modif_monto ce "   +
			"  WHERE ce.ic_documento in ( "+ lstdoctos.toString()+
			" ) modif, "   +
			" ( select * from com_docto_seleccionado "+
			"   where ic_documento in ( " + lstdoctos.toString()+
			" ) ds , "   +
			" (SELECT /*+ INDEX(dt) */ "   +
			" 		dt.ic_documento, "   +
			" 		COUNT(dt.ic_documento) AS detalles "   +
			"  FROM "   +
			"  	  com_documento_detalle dt"   +
			"  WHERE dt.ic_documento in ( " + lstdoctos.toString()+
			"  GROUP BY dt.ic_documento) det, "   +
			" ( select * from com_solicitud "+
			"   where ic_documento in ( " + lstdoctos.toString()+
			" ) S, "   +
			" comcat_estatus_docto ED, "   +
			" comcat_moneda M, "   +
			" comcat_if I, "   +
			" comcat_if i2, "   +
			" comrel_pyme_epo PE, "   +
			" comcat_pyme P, "   +
			" COMCAT_MANDANTE ma, "+
			" COMCAT_TIPO_FACTORAJE tf "+
			" WHERE D.ic_pyme = PE.ic_pyme  "   +
			"  AND PE.ic_pyme = P.ic_pyme  "   +
			"  AND D.ic_epo = PE.ic_epo  "   +
			"  AND D.ic_moneda = M.ic_moneda  "   +
			"  AND D.ic_estatus_docto = ED.ic_estatus_docto  "   +
			"  AND D.ic_if = I.ic_if(+)  "   +
			"  AND ds.ic_documento = S.ic_documento(+)  "   +
			"  AND D.ic_documento = det.ic_documento (+)  "   +
			"  AND D.ic_documento = modif.ic_documento (+)  "   +
			" AND D.ic_documento = ds.ic_documento (+)  "   +
			"  AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "+
			" AND d.cs_dscto_especial = tf.cc_tipo_factoraje  "+
			" AND d.ic_beneficiario = i2.ic_if (+) " );
			
		coma=0;
		if (!ord_proveedor.equals("") || !ord_if.equals("") || !ord_monto.equals("") || !ord_fec_venc.equals("") || !ord_estatus.equals("")) {
			query.append(" ORDER BY ");
      for (criterio=1;criterio<=5;criterio++){
        if (!ord_proveedor.equals("")) {
          if (Integer.parseInt(ord_proveedor)==criterio) {
            coma++;
            query1= (coma == 1)?" D.ic_pyme ":", D.ic_pyme ";
          }
        }
        if (!ord_if.equals("")) {
          if (Integer.parseInt(ord_if)==criterio) {
            coma++;
            query1= (coma == 1)?" D.ic_if ":", D.ic_if ";
          }
        }
        if (!ord_monto.equals("")) {
          if (Integer.parseInt(ord_monto)==criterio) {
            coma++;
            query1= (coma == 1)?" D.fn_monto ":", D.fn_monto ";
          }
        }
        if (!ord_fec_venc.equals("")) {
          if (Integer.parseInt(ord_fec_venc)==criterio) {
            coma++;
            query1= (coma == 1)?" D.df_fecha_venc ":", D.df_fecha_venc ";
          }
        }
        if (!ord_estatus.equals("")) {
          if (Integer.parseInt(ord_estatus)==criterio) {
            coma++;
            query1= (coma == 1)?" D.ic_estatus_docto ":", D.ic_estatus_docto ";
          }
        }
        query.append(query1);
        query1="";
      }//Fin del for para los criterios
		} //if de ordenamiento
		
		log.debug(" query : "+query.toString());
		log.debug(" valoresBind : " +this.valoresBind);
		log.info("getDocumentSummaryQueryForIds(S)");
		return query.toString();

  }

// Devuelve el query de llaves primarias
	public String getDocumentQuery(HttpServletRequest request){
		log.info("getDocumentQuery(E)");
		
		this.valoresBind = new ArrayList();
		String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 

  	String cg_pyme_epo_interno = (request.getParameter("cg_pyme_epo_interno")==null)?"":request.getParameter("cg_pyme_epo_interno").trim();
  	String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme").trim();
  	String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if").trim();
    String ic_estatus_docto = (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto").trim();
  	String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
  	String cc_acuse = (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
  	String no_docto = (request.getParameter("no_docto")==null)?"":request.getParameter("no_docto");
  	String df_fecha_docto_de = (request.getParameter("df_fecha_docto_de")==null)?"":request.getParameter("df_fecha_docto_de");
  	String df_fecha_docto_a = (request.getParameter("df_fecha_docto_a")==null)?"":request.getParameter("df_fecha_docto_a");
  	String fn_monto_de = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
  	String fn_monto_a = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
  	String df_fecha_venc_de = (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
  	String df_fecha_venc_a = (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
  	String ord_proveedor = (request.getParameter("ord_proveedor")==null)?"":request.getParameter("ord_proveedor");
  	String ord_if = (request.getParameter("ord_if")==null)?"":request.getParameter("ord_if");
  	String ord_monto = (request.getParameter("ord_monto")==null)?"":request.getParameter("ord_monto");
  	String ord_fec_venc = (request.getParameter("ord_fec_venc")==null)?"":request.getParameter("ord_fec_venc");
  	String ord_estatus = (request.getParameter("ord_estatus")==null)?"":request.getParameter("ord_estatus");
  	String camp_dina1= (request.getParameter("camp_dina1") == null) ? "" : request.getParameter("camp_dina1");
  	String camp_dina2= (request.getParameter("camp_dina2") == null) ? "" : request.getParameter("camp_dina2");
	String operaFirmaM = (request.getParameter("operaFirmaM") == null) ? "N" : request.getParameter("operaFirmaM");	//agrego parametro
	String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");
	String numero_siaff   = (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
	String ic_documento		= "";
	String ic_epo				= "";
	String strAforo = (String)request.getSession().getAttribute("strAforo");
	String strAforoDL = (String)request.getSession().getAttribute("strAforoDL");
	
	String strPerfilCons = (String)request.getSession().getAttribute("sesPerfil");//FODEA 053-2009 FVR
	String cmbFactorajec   = (request.getParameter("cmbFactoraje") == null) ? "" : request.getParameter("cmbFactoraje");
	String validaDoctoEPOc   = (request.getParameter("validaDoctoEPO") == null) ? "" : request.getParameter("validaDoctoEPO");
   contrato   = (request.getParameter("contrato") == null) ? "" : request.getParameter("contrato");	
	copade   = (request.getParameter("copade") == null) ? "" : request.getParameter("copade");
	    
		int coma = 0,criterio=0;
		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
	 
		if(!numero_siaff.equals("")){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);
			ic_epo 			= (String) numero.get("IC_EPO");
			ic_documento 	= (String) numero.get("IC_DOCUMENTO");
		}

		StringBuffer query_Reporte = new StringBuffer("SELECT /*+ index(D IN_COM_DOCUMENTO_09_NUK) */  D.ic_documento"+
      ",'ConsDoctosDE::getDocumentQuery'"+
  		" FROM com_documento D"+
  		" WHERE D.ic_epo = ?");
		
		this.valoresBind.add(new Integer(iNoCliente));


		if (!"".equals(cmbFactorajec)   ) {
		    query_Reporte.append(" AND D.CS_DSCTO_ESPECIAL = ? ");
		    this.valoresBind.add(cmbFactorajec);
		}
		if (!"".equals(validaDoctoEPOc)   ) {
		    query_Reporte.append(" AND D.CS_VALIDACION_JUR = ? ");
		    this.valoresBind.add(validaDoctoEPOc);
		}
		
		if (!"".equals(contrato)  ) {
				query_Reporte.append(" AND upper(trim(D.CG_NUM_CONTRATO)) LIKE upper(?) ");
				this.valoresBind.add(contrato);
		}
		if (!"".equals(copade)  ) {
				query_Reporte.append(" AND upper(trim(D.CG_NUM_COPADE)) LIKE upper(?) ");
				this.valoresBind.add(copade);
			}
			

		if(ic_pyme.equals("") && cc_acuse.equals("") && no_docto.equals("") &&
				ic_moneda.equals("") && df_fecha_docto_de.equals("") &&
				df_fecha_docto_a.equals("") && fn_monto_de.equals("") && fn_monto_a.equals("") &&
				df_fecha_venc_de.equals("") && df_fecha_venc_a.equals("") && ic_if.equals("") &&
				ic_estatus_docto.equals("") && camp_dina1.equals("") && camp_dina2.equals("") &&
				ic_epo.equals("")	&& ic_documento.equals("") ){
		
			if(!"".equals(strPerfilCons) && "ADMIN EPO MAN0".equals(strPerfilCons)){//FODEA 053-2009 FVR
				query_Reporte.append(" AND D.ic_estatus_docto in (?,?) ");
				this.valoresBind.add(new Integer(30));	//Pendientes
				this.valoresBind.add(new Integer(31));	//No Pendientes
			}else if(!"".equals(strPerfilCons) && "ADMIN EPO MAN1".equals(strPerfilCons)){//FODEA 053-2009 FVR
				query_Reporte.append(" AND D.ic_estatus_docto in (?,?,?,?) ");
				this.valoresBind.add(new Integer(28));	//Pre Negociables
				this.valoresBind.add(new Integer(29));	//Pre No Negociables
				this.valoresBind.add(new Integer(30));	//Pendientes
				this.valoresBind.add(new Integer(31));	//No Pendientes
			}else{//FODEA 053-2009 FVR
				query_Reporte.append(" AND D.ic_estatus_docto = ? ");
				this.valoresBind.add(new Integer(2));	//Negociable			    
			}

		
			if (!ord_proveedor.equals("") || !ord_if.equals("") || !ord_monto.equals("") || !ord_fec_venc.equals("") || !ord_estatus.equals("")) {
				query_Reporte.append(" ORDER BY ");
	      for (criterio=1;criterio<=5;criterio++){
		      if (!ord_proveedor.equals("")) {
			      if (Integer.parseInt(ord_proveedor)==criterio) {
				      coma++;
					    query_Reporte.append( (coma == 1)?" D.ic_pyme ":", D.ic_pyme ");
						}
	        }
		      if (!ord_if.equals("")) {
			      if (Integer.parseInt(ord_if)==criterio) {
				      coma++;
					    query_Reporte.append( (coma == 1)?" D.ic_if ":", D.ic_if " );
	          }
		      }
			    if (!ord_monto.equals("")) {
				    if (Integer.parseInt(ord_monto)==criterio) {
					    coma++;
	            query_Reporte.append( (coma == 1)?" D.fn_monto ":", D.fn_monto ");
		        }
			    }
				  if (!ord_fec_venc.equals("")) {
					  if (Integer.parseInt(ord_fec_venc)==criterio) {
						  coma++;
	            query_Reporte.append( (coma == 1)?" D.df_fecha_venc ":", D.df_fecha_venc ");
		        }
			    }
				  if (!ord_estatus.equals("")) {
						if (Integer.parseInt(ord_estatus)==criterio) {
	            coma++;
		          query_Reporte.append( (coma == 1)?" D.ic_estatus_docto ":", D.ic_estatus_docto ");
			      }
				  }
				}//Fin del for para los criterios
			} else{//if
				query_Reporte.append(" ORDER BY d.ic_documento");
			}
		//out.println("query_Reporte: <br>"+query_Reporte);
		} //if de valores vacios.
		else {
		//out.println("query_Reporte: "+query_Reporte+"<br>");
			if (!ic_pyme.equals("")) {
				query_Reporte.append(" AND D.ic_pyme = ? ");
				this.valoresBind.add(new Integer(ic_pyme));
			}
			if (!cc_acuse.equals("")) {
				if(cc_acuse.trim().indexOf("4")==0) { // si el acuse es de modificacion de montos
					query_Reporte.append( " AND D.ic_documento in (select ic_documento from comhis_cambio_est_modif_monto where cc_acuse=?) ");
					this.valoresBind.add(cc_acuse);
				}
				else {
					query_Reporte.append(" AND D.cs_cambio_importe=? AND D.cc_acuse = ? ");
					this.valoresBind.add("N");
					this.valoresBind.add(cc_acuse);
				}
			} //if acuse
	
			if (!no_docto.equals("")) {
				query_Reporte.append(" AND D.ig_numero_docto = ? ");
				this.valoresBind.add(no_docto);
			}
			if (!ic_moneda.equals("")) {
				query_Reporte.append(" AND D.ic_moneda = ? ");
				this.valoresBind.add(new Integer(ic_moneda));
			}
			if (!df_fecha_docto_de.equals("") && !df_fecha_docto_a.equals("")) {
				query_Reporte.append(" AND D.df_fecha_docto >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.df_fecha_docto < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
				this.valoresBind.add(df_fecha_docto_de);
				this.valoresBind.add(df_fecha_docto_a);
			}
			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals("")) {
				query_Reporte.append(" AND D.df_fecha_venc >= TO_DATE(?||' 00:00','DD/MM/YYYY HH24:MI') and D.df_fecha_venc < TO_DATE(?||' 00:00','DD/MM/YYYY HH24:MI')+1 ");
				this.valoresBind.add(df_fecha_venc_de);
				this.valoresBind.add(df_fecha_venc_a);
			}
			if (!fn_monto_de.equals("") && !fn_monto_a.equals("")) {
				query_Reporte.append(" AND D.fn_monto BETWEEN ? and ? ");
				this.valoresBind.add(new Double(fn_monto_de));
				this.valoresBind.add(new Double(fn_monto_a));
			}
			if (!ic_if.equals("")) {
				query_Reporte.append(" AND D.ic_if = ? ");
				this.valoresBind.add(new Integer(ic_if));
			}
			if (!ic_estatus_docto.equals("")) {
				if (ic_estatus_docto.equals("4")) {
					query_Reporte.append(" AND D.ic_estatus_docto in (?,?) ");
					this.valoresBind.add(new Integer(4));	//Operado
					this.valoresBind.add(new Integer(16));	//Aplicado a Credito
				} else {
					query_Reporte.append(" AND D.ic_estatus_docto = ? ");
					this.valoresBind.add(new Integer(ic_estatus_docto));
				}
			}
			if (!camp_dina1.equals("")) {
				query_Reporte.append(" AND D.cg_campo1 = ? ");
				this.valoresBind.add(camp_dina1);
			}
			if (!camp_dina2.equals("")) {
				query_Reporte.append(" AND D.cg_campo2 = ? ");
				this.valoresBind.add(camp_dina2);
			}
	
			//FODEA 053-2009 FVR 
			if(!"ADMIN EPO MAN0".equals(strPerfilCons) && !"ADMIN EPO MAN1".equals(strPerfilCons)){//FODEA 053-2009 FVR
				if (!ic_if.equals("") && ic_estatus_docto.equals("") && ic_moneda.equals("")){
					//query_Reporte.append(" AND D.ic_estatus_docto in (?,?,?,?) ");
					query_Reporte.append(" AND D.ic_estatus_docto in (?,?,?,?,?) ");//FODEA 005 - 2009 ACF
					this.valoresBind.add(new Integer(3)); //Seleccionada Pyme
					this.valoresBind.add(new Integer(4)); //Operada
					this.valoresBind.add(new Integer(11));	//Operada Pagada 
					this.valoresBind.add(new Integer(16));	//Aplicada a credito
					this.valoresBind.add(new Integer(26)); //Programado PyME -- FODEA 005 - 2009 ACF
				}else if(!ic_moneda.equals("") && ic_estatus_docto.equals("") && ic_if.equals("")){
					query_Reporte.append(" AND D.ic_estatus_docto = ? ");
					this.valoresBind.add(new Integer(2)); //Negociable
				}
				else if(ic_estatus_docto.equals("") || !operaFirmaM.equals("S")){
					//query_Reporte.append(" AND D.ic_estatus_docto not in (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
					query_Reporte.append(" AND D.ic_estatus_docto not in (?,?,?,?,?,?,?,?,?,?,?,?,?) ");//FODEA 005 - 2009 ACF
					this.valoresBind.add(new Integer(8)); //Seleccionada IF (Obsoleto)
					this.valoresBind.add(new Integer(13)); //Cancelado
					this.valoresBind.add(new Integer(14)); //Incumplimiento Pend. de Pago
					this.valoresBind.add(new Integer(15)); //Pedido sin Operar
					this.valoresBind.add(new Integer(16)); //Aplicado a Credito
					this.valoresBind.add(new Integer(17)); //Credito Vencido
					this.valoresBind.add(new Integer(18)); //Cancelado Pendiente de Pago
					this.valoresBind.add(new Integer(19)); //Pedido Sustituto
					this.valoresBind.add(new Integer(20)); //Rechazado IF
					this.valoresBind.add(new Integer(22)); //Pendiente de pago IF
					this.valoresBind.add(new Integer(23)); //Pignorado
					//this.valoresBind.add(new Integer(26)); //Programado -- FODEA 005 - 2009 ACF
					this.valoresBind.add(new Integer(28)); //Pre Negociable
					this.valoresBind.add(new Integer(29)); //Pre No Negociable
				}
			}//CIERRE IF PERFIL FODEA 053-2009 FVR
			// Reubicar
			if(!ic_epo.equals("")){
				query_Reporte.append(" AND D.ic_epo = ? ");
				this.valoresBind.add(new Integer(ic_epo));
			}
			if(!ic_documento.equals("")){
				query_Reporte.append(" AND D.ic_documento = ? ");
				this.valoresBind.add(new Integer(ic_documento));
			}
					
			coma=0;
	
			if (!ord_proveedor.equals("") || !ord_if.equals("") || !ord_monto.equals("") || !ord_fec_venc.equals("") || !ord_estatus.equals("")) {
				query_Reporte.append(" ORDER BY ");
				for (criterio=1;criterio<=5;criterio++){
					if (!ord_proveedor.equals("")) {
						if (Integer.parseInt(ord_proveedor)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.ic_pyme ":", D.ic_pyme ");
						}
					}
					if (!ord_if.equals("")) {
						if (Integer.parseInt(ord_if)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.ic_if ":", D.ic_if ");
						}
					}
					if (!ord_monto.equals("")) {
						if (Integer.parseInt(ord_monto)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.fn_monto ":", D.fn_monto ");
						}
					}
					if (!ord_fec_venc.equals("")) {
						if (Integer.parseInt(ord_fec_venc)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.df_fecha_venc ":", D.df_fecha_venc ");
						}
					}
					if (!ord_estatus.equals("")) {
						if (Integer.parseInt(ord_estatus)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.ic_estatus_docto ":", D.ic_estatus_docto ");
						}
					}
				}//Fin del for para los criterios
			} else{// if del ordenamiento
				query_Reporte.append(" ORDER BY d.ic_documento");
			}
			//log.info("Document Query = " +query_Reporte);
		} //else
		log.debug(" query_Reporte = " +query_Reporte);
		log.debug(" valoresBind = " +this.valoresBind);
		log.info("getDocumentQuery(S)");
		return query_Reporte.toString();
  }

	public String getDocumentQueryFile(HttpServletRequest request){
		log.info("getDocumentQueryFile(E)");
		
		this.valoresBind = new ArrayList();
		
		String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 

		String cg_pyme_epo_interno = (request.getParameter("cg_pyme_epo_interno")==null)?"":request.getParameter("cg_pyme_epo_interno").trim();
		String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme").trim();
		String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if").trim();
		String ic_estatus_docto = (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto").trim();
		String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String cc_acuse = (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String no_docto = (request.getParameter("no_docto")==null)?"":request.getParameter("no_docto");
		String df_fecha_docto_de = (request.getParameter("df_fecha_docto_de")==null)?"":request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a = (request.getParameter("df_fecha_docto_a")==null)?"":request.getParameter("df_fecha_docto_a");
		String fn_monto_de = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String df_fecha_venc_de = (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a = (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
		String ord_proveedor = (request.getParameter("ord_proveedor")==null)?"":request.getParameter("ord_proveedor");
		String ord_if = (request.getParameter("ord_if")==null)?"":request.getParameter("ord_if");
		String ord_monto = (request.getParameter("ord_monto")==null)?"":request.getParameter("ord_monto");
		String ord_fec_venc = (request.getParameter("ord_fec_venc")==null)?"":request.getParameter("ord_fec_venc");
		String ord_estatus = (request.getParameter("ord_estatus")==null)?"":request.getParameter("ord_estatus");
		String camp_dina1= (request.getParameter("camp_dina1") == null) ? "" : request.getParameter("camp_dina1");
		String camp_dina2= (request.getParameter("camp_dina2") == null) ? "" : request.getParameter("camp_dina2");
		String operaFirmaM = (request.getParameter("operaFirmaM") == null) ? "N" : request.getParameter("operaFirmaM");	//agrego parametro
		String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");			
		String numero_siaff   = (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
		String ic_documento		= "";
		String ic_epo				= "";
		String strAforo = (String)request.getSession().getAttribute("strAforo");
		String strAforoDL = (String)request.getSession().getAttribute("strAforoDL");
		String cmbFactorajec   = (request.getParameter("cmbFactoraje") == null) ? "" : request.getParameter("cmbFactoraje");	
		String validaDoctoEPOc   = (request.getParameter("validaDoctoEPO") == null) ? "" : request.getParameter("validaDoctoEPO");
	   contrato   = (request.getParameter("contrato") == null) ? "" : request.getParameter("contrato");
	   copade   = (request.getParameter("copade") == null) ? "" : request.getParameter("copade");

		boolean ocultarDoctosAplicados = (
			ic_estatus_docto.equals("") 	|| 
			ic_estatus_docto.equals("1") 	|| 
			ic_estatus_docto.equals("2") 	|| 
			ic_estatus_docto.equals("5")	|| 
			ic_estatus_docto.equals("6") 	|| 
			ic_estatus_docto.equals("7") 	|| 
			ic_estatus_docto.equals("9")	|| 
			ic_estatus_docto.equals("10") || 
			ic_estatus_docto.equals("21") || 
			ic_estatus_docto.equals("23")	|| 
			ic_estatus_docto.equals("28") || 
			ic_estatus_docto.equals("29")	|| 
			ic_estatus_docto.equals("30")	|| 
			ic_estatus_docto.equals("31") )?true:false;
		
		String strPerfilCons = (String)request.getSession().getAttribute("sesPerfil");//FODEA 053-2009 FVR

		if(!numero_siaff.equals("")) {
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);
			
			ic_epo 			= (String) numero.get("IC_EPO");
			ic_documento 	= (String) numero.get("IC_DOCUMENTO");
			
			log.info("IC_EPO: " + ic_epo);
			log.info("IC_DOCUMENTO: " + ic_documento);
		}

		int coma=0,criterio = 0;
		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");

		String operado= (sesIdiomaUsuario.equals("EN"))?"Operate":"Operado";

		StringBuffer query_Reporte = new StringBuffer();
		query_Reporte.append(
				" SELECT /*+ USE_NL(p i i2 ed m ds s d pe modif ma tf) */ "   +
				" D.ic_documento, "   +
				" (decode (PE.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social)as cg_razon_social_pyme,  "   +
				" D.ic_epo, "   +
				" (decode (cs_cambio_importe,'N',D.cc_acuse,'S',modif.cc_acuse)) as cc_acuse,  "   +
				" D.ig_numero_docto, "   +
				" TO_CHAR(D.df_fecha_docto,'DD/MM/YYYY'),  "   +
				" TO_CHAR(D.df_fecha_venc,'DD/MM/YYYY'), "   +
				" M.cd_nombre"+idioma+ " as cd_nombre, " +
				" D.fn_monto, "   +
				" D.cs_dscto_especial,  "   +
				" D.fn_porc_anticipo, "   +
				" D.fn_monto_dscto,  "   +
				" Decode(D.ic_estatus_docto, 16,'" +operado+"', ED.cd_descripcion"+idioma+")as cd_descripcion,  "   +
				" (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social)as cg_razon_social_if,  "   +
				" S.ic_folio, "   +
				" D.ic_estatus_docto, "   +
				" D.ic_moneda, "   +
				" PE.cg_pyme_epo_interno, "   +
				" D.ct_referencia,  cs_cambio_importe, 0 detalles, D.cg_campo1, D.cg_campo2, D.cg_campo3, D.cg_campo4, D.cg_campo5,"   +
				/*
				" decode(D.cs_dscto_especial, 'D', i2.cg_razon_social, 'I', i2.cg_razon_social, '') as beneficiario, "   +
				" decode(D.cs_dscto_especial, 'D', D.fn_porc_beneficiario, 'I', D.fn_porc_beneficiario, '') as fn_porc_beneficiario,  "   +
				" decode(D.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, '') as RECIBIR_BENEF,  "   +
				" decode(D.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '0') as NETO_REC_PYME ,"   +
				*/
				
				" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN i2.cg_razon_social ELSE '' END AS beneficiario, " +
				" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN d.fn_porc_beneficiario ELSE 0 END AS fn_porc_beneficiario, " +
				" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN NVL(ds.fn_importe_recibir_benef, d.fn_monto * d.fn_porc_beneficiario / 100 ) ELSE 0 END AS RECIBIR_BENEF, " +
				" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN ds.in_importe_recibir - (ds.fn_importe_recibir_benef) ELSE 0 END AS NETO_REC_PYME, " +
				
				" ?,?,"+
				//" TO_CHAR(ds.df_programacion,'DD/MM/YYYY') as df_programacion, " +	
				" TO_CHAR(D.df_fecha_venc_pyme,'DD/MM/YYYY') AS df_fecha_venc_pyme, "   +
				" TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO, " +
				" ma.CG_RAZON_SOCIAL, "+
				" tf.cg_nombre as TIPO_FACTORAJE, " +
				" TO_CHAR(ds.df_programacion, 'dd/mm/yyyy') AS fecha_registro_operacion, "+//FODEA 005 - 2009 ACF
				(	!ocultarDoctosAplicados?
					" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND d.ic_docto_asociado IS NOT NULL THEN 'true' ELSE 'false' END AS existeDocumentoAsociado, " +
					" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM com_documento WHERE ic_docto_asociado = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredSimpleApl, " +
					" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_NOTA_CREDITO = d.ic_documento) THEN 'true' ELSE 'false' END AS existeRefEnComrelNotaDocto, " +
					" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_DOCUMENTO = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredMultApl, " 
					:""
				)+
				" DECODE(                                                  "  +
				"           S.IC_ESTATUS_SOLIC,                            "  +
				"           1, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (1) Seleccionada IF
				"           2, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (2) En proceso
				"           3, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (3) Operada
				"           5, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (5) Operada Pagada
				"          10, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "  + // (10) Operado con Fondeo Propio
				"          'N/A'                                           "  + // PARA TODOS LOS OTROS CASOS
				"       ) AS FECHA_AUTORIZACION_IF,                        "  + // Fodea 040 - 2011 By JSHD 
				" 'N' nota_simple, 'N' nota_simple_docto, 'N' nota_multiple, 'N' nota_multiple_docto, '' numero_docto,"+	//Campos a llenar durante el llamado de la consulta 13consulta01ext.data.jsp
				
				" 'ConsDoctosDE::getDocumentQueryFile' "   +
				" , decode( d.cs_dscto_especial,  'D', decode (d.CS_VALIDACION_JUR, 'S', 'SI', 'N', 'NO' ), '' )  as  VALIDACION_JUR "+
				" ,d.CG_NUM_CONTRATO AS CONTRATO, d.CG_NUM_COPADE  AS COPADE "+
				
				" FROM "   +
				" comcat_pyme P, "   +
				" comcat_if I, "   +
				" comcat_if i2, "   +
				" comcat_estatus_docto ED, "   +
				" comcat_moneda M, "   +
				" com_docto_seleccionado ds , "   +
				" com_solicitud S, "   +
				" com_documento D, "+
				" COMCAT_MANDANTE ma, "+
			   " COMCAT_TIPO_FACTORAJE tf, "+
				" (SELECT * FROM comrel_pyme_epo WHERE ic_epo = ? ) PE, "   +
				" comhis_cambio_est_modif_monto modif " +
				" WHERE D.ic_pyme = PE.ic_pyme  "   +
				"  AND PE.ic_pyme = P.ic_pyme  "   +
				"  AND D.ic_epo = PE.ic_epo  "   +
				"  AND D.ic_moneda = M.ic_moneda  "   +
				"  AND D.ic_estatus_docto = ED.ic_estatus_docto  "   +
				"  AND D.ic_if = I.ic_if(+)  "   +
				"  AND ds.ic_documento = S.ic_documento(+)  "   +
				"  AND D.ic_documento = modif.ic_documento (+)  "   +
				" AND D.ic_documento = ds.ic_documento (+)  "   +
				" AND d.ic_beneficiario = i2.ic_if (+) "   +
				"  AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "+
				" AND d.cs_dscto_especial = tf.cc_tipo_factoraje  "+
				" AND D.ic_epo = ?" +
				" AND PE.ic_epo= ?");
		
				this.valoresBind.add(new Double(strAforo));
				this.valoresBind.add(new Double(strAforoDL));
				this.valoresBind.add(new Integer(iNoCliente));
				this.valoresBind.add(new Integer(iNoCliente));
				this.valoresBind.add(new Integer(iNoCliente));
		
			    if (!"".equals(cmbFactorajec)   ) {
			    query_Reporte.append(" AND D.CS_DSCTO_ESPECIAL = ? ");
			    this.valoresBind.add(cmbFactorajec);
			    }
			    if (!"".equals(validaDoctoEPOc)   ) {
				query_Reporte.append(" AND D.CS_VALIDACION_JUR = ? ");
				this.valoresBind.add(validaDoctoEPOc);
			    }
				 
				if (!"".equals(contrato)   ) {
					query_Reporte.append(" AND upper(trim(D.CG_NUM_CONTRATO)) LIKE upper(?) ");
					this.valoresBind.add(contrato);
			    }
				if (!"".equals(copade)  ) {
				 query_Reporte.append(" AND upper(trim(D.CG_NUM_COPADE)) LIKE upper(?) ");
				 this.valoresBind.add(copade);
				}
				
					
			    
		if(ic_pyme.equals("") && cc_acuse.equals("") && no_docto.equals("") &&
			ic_moneda.equals("") && df_fecha_docto_de.equals("") &&
			df_fecha_docto_a.equals("") && fn_monto_de.equals("") && fn_monto_a.equals("") &&
			df_fecha_venc_de.equals("") && df_fecha_venc_a.equals("") && ic_if.equals("") &&
			ic_estatus_docto.equals("") && camp_dina1.equals("") && camp_dina2.equals("") &&
			ic_epo.equals("")	&& ic_documento.equals("")) {
	
			if(!"".equals(strPerfilCons) && "ADMIN EPO MAN0".equals(strPerfilCons)){//FODEA 053-2009 FVR
				query_Reporte.append(" AND D.ic_estatus_docto in (?,?) ");
				this.valoresBind.add(new Integer(30));	//Pendientes
				this.valoresBind.add(new Integer(31));	//No Pendientes				
			}else if(!"".equals(strPerfilCons) && "ADMIN EPO MAN1".equals(strPerfilCons)){//FODEA 053-2009 FVR
				query_Reporte.append(" AND D.ic_estatus_docto in (?,?,?,?) ");
				this.valoresBind.add(new Integer(28));	//Pre Negociables
				this.valoresBind.add(new Integer(29));	//Pre No Negociables
				this.valoresBind.add(new Integer(30));	//Pendientes
				this.valoresBind.add(new Integer(31));	//No Pendientes
			}else{//FODEA 053-2009 FVR
				query_Reporte.append(" AND D.ic_estatus_docto = ? ");
				this.valoresBind.add(new Integer(2));	//Negociables
			}
	
			if (!ord_proveedor.equals("") || !ord_if.equals("") || !ord_monto.equals("") || !ord_fec_venc.equals("") || !ord_estatus.equals("")) {
				query_Reporte.append(" ORDER BY ");
				for (criterio=1;criterio<=5;criterio++){
					if (!ord_proveedor.equals("")) {
						if (Integer.parseInt(ord_proveedor)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.ic_pyme ":", D.ic_pyme " );
						}
					}
					if (!ord_if.equals("")) {
						if (Integer.parseInt(ord_if)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.ic_if ":", D.ic_if " );
						}
					}
					if (!ord_monto.equals("")) {
						if (Integer.parseInt(ord_monto)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.fn_monto ":", D.fn_monto " );
						}
					}
					if (!ord_fec_venc.equals("")) {
						if (Integer.parseInt(ord_fec_venc)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.df_fecha_venc ":", D.df_fecha_venc " );
						}
					}
					if (!ord_estatus.equals("")) {
						if (Integer.parseInt(ord_estatus)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.ic_estatus_docto ":", D.ic_estatus_docto " );
						}
					}
				}//Fin del for para los criterios
			} else{// if del ordenamiento
				query_Reporte.append(" ORDER BY d.ic_documento");
			}
			//out.println("query_Reporte: <br>"+query_Reporte);
		} //if de valores vacios.
		else {
			//out.println("query_Reporte: "+query_Reporte+"<br>");
			if (!ic_pyme.equals("")) {
				query_Reporte.append(" AND D.ic_pyme = ? ");
				this.valoresBind.add(new Integer(Integer.parseInt(ic_pyme)));
			}
			if (!cc_acuse.equals("")) {
				if(cc_acuse.trim().indexOf("4")==0) { // si el acuse es de modificacion de montos
					query_Reporte.append(" AND modif.cc_acuse = ? ");
					this.valoresBind.add(cc_acuse);
				}
				else {
					query_Reporte.append(" AND D.cs_cambio_importe=? AND D.cc_acuse = ? ");
					this.valoresBind.add("N");
					this.valoresBind.add(cc_acuse);
				}
			} //if acuse
	
			if (!no_docto.equals("")) {
				query_Reporte.append(" AND D.ig_numero_docto = ? ");
				this.valoresBind.add(no_docto);
			}
			if (!ic_moneda.equals("")) {
				query_Reporte.append(" AND D.ic_moneda = ? ");
				this.valoresBind.add(new Integer(ic_moneda));
			}
			if (!df_fecha_docto_de.equals("") && !df_fecha_docto_a.equals("")) {
				query_Reporte.append(" AND D.df_fecha_docto >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.df_fecha_docto < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
				this.valoresBind.add(df_fecha_docto_de);
				this.valoresBind.add(df_fecha_docto_a);
			}
			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals("")) {
				query_Reporte.append(" AND D.df_fecha_venc >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.df_fecha_venc < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
				this.valoresBind.add(df_fecha_venc_de);
				this.valoresBind.add(df_fecha_venc_a);
			}
			if (!fn_monto_de.equals("") && !fn_monto_a.equals("")) {
				query_Reporte.append(" AND D.fn_monto BETWEEN ? and ? ");
				this.valoresBind.add(new Double(fn_monto_de));
				this.valoresBind.add(new Double(fn_monto_a));
			}
			if (!ic_if.equals("")) {
				query_Reporte.append(" AND D.ic_if = ? ");
				this.valoresBind.add(new Integer(ic_if));
			}
			if (!ic_estatus_docto.equals("")) {
				if (ic_estatus_docto.equals("4")) {
					query_Reporte.append(" AND D.ic_estatus_docto in (?,?) ");
					this.valoresBind.add(new Integer(4));	//Operada
					this.valoresBind.add(new Integer(16));	//Aplicado a credito
				} else {
					query_Reporte.append(" AND D.ic_estatus_docto = ? ");
					this.valoresBind.add(new Integer(ic_estatus_docto));
				}
			}
			if (!camp_dina1.equals("")) {
				query_Reporte.append(" AND D.cg_campo1 = ? ");
				this.valoresBind.add(camp_dina1);
			}
			if (!camp_dina2.equals("")) {
				query_Reporte.append(" AND D.cg_campo2 = ? ");
				this.valoresBind.add(camp_dina2);
			}
	
			//FODEA 053-2009 FV
			if(!"ADMIN EPO MAN0".equals(strPerfilCons) && !"ADMIN EPO MAN1".equals(strPerfilCons) ){//FODEA 053-2009 FVR
			
			
				if (!ic_if.equals("") && ic_estatus_docto.equals("") && ic_moneda.equals("")){
					//query_Reporte.append(" AND D.ic_estatus_docto in (?,?,?,?) ");
					query_Reporte.append(" AND D.ic_estatus_docto in (?,?,?,?,?) ");//FODEA 005 - 2009 ACF
					this.valoresBind.add(new Integer(3)); //Seleccionada Pyme
					this.valoresBind.add(new Integer(4)); //Operada
					this.valoresBind.add(new Integer(11));	//Operada Pagada 
					this.valoresBind.add(new Integer(16));	//Aplicada a credito
					this.valoresBind.add(new Integer(26)); //Programado PyME -- FODEA 005 - 2009 ACF
				}else if(!ic_moneda.equals("") && ic_estatus_docto.equals("") && ic_if.equals("")){
					query_Reporte.append(" AND D.ic_estatus_docto = ? ");
					this.valoresBind.add(new Integer(2)); //Negociable
				}
				else if(ic_estatus_docto.equals("") || !operaFirmaM.equals("S")){
					//query_Reporte.append(" AND D.ic_estatus_docto not in (?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
					query_Reporte.append(" AND D.ic_estatus_docto not in (?,?,?,?,?,?,?,?,?,?,?,?,?) ");//FODEA 005 - 2009
					this.valoresBind.add(new Integer(8)); //Seleccionada IF (Obsoleto)
					this.valoresBind.add(new Integer(13)); //Cancelado
					this.valoresBind.add(new Integer(14)); //Incumplimiento Pend. de Pago
					this.valoresBind.add(new Integer(15)); //Pedido sin Operar
					this.valoresBind.add(new Integer(16)); //Aplicado a Credito
					this.valoresBind.add(new Integer(17)); //Credito Vencido
					this.valoresBind.add(new Integer(18)); //Cancelado Pendiente de Pago
					this.valoresBind.add(new Integer(19)); //Pedido Sustituto
					this.valoresBind.add(new Integer(20)); //Rechazado IF
					this.valoresBind.add(new Integer(22)); //Pendiente de pago IF
					this.valoresBind.add(new Integer(23)); //Pignorado
					//this.valoresBind.add(new Integer(26)); //Programado -- FODEA 005 - 2009 ACF
					this.valoresBind.add(new Integer(28)); //Pre Negociable
					this.valoresBind.add(new Integer(29)); //Pre No Negociable
				}
			}//CIERRE IF PERFILES FODEA 053-2009 FVR
			// Reubicar
			if(!ic_epo.equals("")){
				query_Reporte.append(" AND D.ic_epo = ? ");
				this.valoresBind.add(new Integer(ic_epo));
			}
			if(!ic_documento.equals("")){
				query_Reporte.append(" AND D.ic_documento = ? ");
				this.valoresBind.add(new Integer(ic_documento));
			}
			
			coma=0;
			if (!ord_proveedor.equals("") || !ord_if.equals("") || !ord_monto.equals("") || !ord_fec_venc.equals("") || !ord_estatus.equals("")) {
				query_Reporte.append(" ORDER BY ");
				for (criterio=1;criterio<=5;criterio++){
					if (!ord_proveedor.equals("")) {
						if (Integer.parseInt(ord_proveedor)==criterio) {
							coma++;
							query_Reporte.append((coma == 1)?" D.ic_pyme ":", D.ic_pyme ");
						}
					}
					if (!ord_if.equals("")) {
						if (Integer.parseInt(ord_if)==criterio) {
							coma++;
							query_Reporte.append((coma == 1)?" D.ic_if ":", D.ic_if ");
						}
					}
					if (!ord_monto.equals("")) {
						if (Integer.parseInt(ord_monto)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.fn_monto ":", D.fn_monto ");
						}
					}
					if (!ord_fec_venc.equals("")) {
						if (Integer.parseInt(ord_fec_venc)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.df_fecha_venc ":", D.df_fecha_venc ");
						}
					}
					if (!ord_estatus.equals("")) {
						if (Integer.parseInt(ord_estatus)==criterio) {
							coma++;
							query_Reporte.append( (coma == 1)?" D.ic_estatus_docto ":", D.ic_estatus_docto ");
						}
					}
				}//Fin del for para los criterios
			} else{// if del ordenamiento
				query_Reporte.append(" ORDER BY d.ic_documento");
			}
			//out.println(query_Reporte);
		} //else */
	   log.debug(" query_Reporte = "+query_Reporte.toString());
		log.debug(" = " +this.valoresBind);
		log.info("getDocumentQueryFile(S)");
		return query_Reporte.toString();
  }

	public ArrayList getConditions(HttpServletRequest request){
		return this.valoresBind;
	}
	
/*
	ArrayList condiciones = new ArrayList();
	
	String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
	sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
	String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 
	
	String cg_pyme_epo_interno = (request.getParameter("cg_pyme_epo_interno")==null)?"":request.getParameter("cg_pyme_epo_interno").trim();
	String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme").trim();
	String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if").trim();
	String ic_estatus_docto = (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto").trim();
	String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
	String cc_acuse = (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
  	String no_docto = (request.getParameter("no_docto")==null)?"":request.getParameter("no_docto");
  	String df_fecha_docto_de = (request.getParameter("df_fecha_docto_de")==null)?"":request.getParameter("df_fecha_docto_de");
  	String df_fecha_docto_a = (request.getParameter("df_fecha_docto_a")==null)?"":request.getParameter("df_fecha_docto_a");
  	String fn_monto_de = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
  	String fn_monto_a = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
  	String df_fecha_venc_de = (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
  	String df_fecha_venc_a = (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
  	String ord_proveedor = (request.getParameter("ord_proveedor")==null)?"":request.getParameter("ord_proveedor");
  	String ord_if = (request.getParameter("ord_if")==null)?"":request.getParameter("ord_if");
  	String ord_monto = (request.getParameter("ord_monto")==null)?"":request.getParameter("ord_monto");
  	String ord_fec_venc = (request.getParameter("ord_fec_venc")==null)?"":request.getParameter("ord_fec_venc");
  	String ord_estatus = (request.getParameter("ord_estatus")==null)?"":request.getParameter("ord_estatus");
  	String camp_dina1= (request.getParameter("camp_dina1") == null) ? "" : request.getParameter("camp_dina1");
  	String camp_dina2= (request.getParameter("camp_dina2") == null) ? "" : request.getParameter("camp_dina2");
	String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");
	String numero_siaff = (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
	String ic_documento		= "";
	String ic_epo				= "";
	
	int coma=0,criterio = 0;
	String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");

   String strAforo = (String)request.getSession().getAttribute("strAforo");
	String strAforoDL = (String)request.getSession().getAttribute("strAforoDL");

	condiciones.add(new Double(strAforo));
	condiciones.add(new Double(strAforoDL));

	condiciones.add(iNoCliente);
	condiciones.add(iNoCliente);
	condiciones.add(iNoCliente);
	condiciones.add(iNoCliente);
	
	if(!numero_siaff.equals("")){
		HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);
		
		ic_epo 			= (String) numero.get("IC_EPO");
		ic_documento 	= (String) numero.get("IC_DOCUMENTO");
	}

	if(ic_pyme.equals("") && cc_acuse.equals("") && no_docto.equals("") &&
		ic_moneda.equals("") && df_fecha_docto_de.equals("") &&
		df_fecha_docto_a.equals("") && fn_monto_de.equals("") && fn_monto_a.equals("") &&
		df_fecha_venc_de.equals("") && df_fecha_venc_a.equals("") && ic_if.equals("") &&
		ic_estatus_docto.equals("") && camp_dina1.equals("") && camp_dina2.equals("") && 
		ic_epo.equals("") && ic_documento.equals("")){
		// valores vacios	
	} //if de valores vacios.
	else {
		//out.println("query_Reporte: "+query_Reporte+"<br>");
		if (!ic_pyme.equals(""))
			condiciones.add(ic_pyme);

		if (!cc_acuse.equals("")) {
			if(cc_acuse.trim().indexOf("4")==0) { // si el acuse es de modificacion de montos
				condiciones.add(cc_acuse);
				condiciones.add(cc_acuse);
			}
			else {
				condiciones.add(cc_acuse);
			}
		} //if acuse

		if (!no_docto.equals("")){
			condiciones.add(no_docto);
		}
		if (!ic_moneda.equals("")){
			condiciones.add(new Integer(ic_moneda));
		}
		if (!df_fecha_docto_de.equals("") && !df_fecha_docto_a.equals("")){
			condiciones.add(df_fecha_docto_de);
			condiciones.add(df_fecha_docto_a);
		}
		if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals("")){
			condiciones.add(df_fecha_venc_de);
			condiciones.add(df_fecha_venc_a);
		}
		if (!fn_monto_de.equals("") && !fn_monto_a.equals("")){
			condiciones.add(new Double(fn_monto_de));
			condiciones.add(new Double(fn_monto_a));
		}
		if (!ic_if.equals("")){
			condiciones.add(new Integer(ic_if));
		}
		if (!ic_estatus_docto.equals("")) {
		  	 if (!ic_estatus_docto.equals("4")) {
				condiciones.add(new Integer(ic_estatus_docto));
			 }
		}
		if (!camp_dina1.equals("")){
			condiciones.add(camp_dina1);
		}
		if (!camp_dina2.equals("")){
			condiciones.add(camp_dina2);
		}
		// Condiciones por Reubicar
		if(!ic_epo.equals("")){
			condiciones.add(new Integer(ic_epo));
		}
		if(!ic_documento.equals("")){
			condiciones.add(ic_documento);
		}

	  } 
	  log.info("El array de condiciones = "+condiciones);
	  return condiciones;
  }
*/

  public HashMap getPartesDelNumeroSIAFF(String numeroSIAFF){
		HashMap resultado = new HashMap();

		BigInteger	icEPO 		= new BigInteger("-1");
		BigInteger	icDocumento = new BigInteger("-1");

		if(numeroSIAFF != null && !numeroSIAFF.equals("") && numeroSIAFF.length() == 15){
			icEPO 		= new BigInteger(numeroSIAFF.substring(0,4));
			icDocumento = new BigInteger(numeroSIAFF.substring(4,15));
		}

		resultado.put("IC_EPO",			icEPO.toString());
		resultado.put("IC_DOCUMENTO",	icDocumento.toString());

		return resultado;
	}


	/*******************************************************************************
	 *       Migraci�n. Implementa IQueryGeneratorThreadRegExtJS.java              *
	 *******************************************************************************/
	
	
	public String getDocumentQuery() {
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	public String getAggregateCalculationQuery() {
		return null;
	}

/**
 * Se forma la consulta para generar los archivos pdf y csv
 * @return qrySentencia
 */
	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		int coma = 0;
		int criterio = 0;

		boolean ocultarDoctosAplicados = (
			ic_estatus_docto.equals("")   || 
			ic_estatus_docto.equals("1")  || 
			ic_estatus_docto.equals("2")  || 
			ic_estatus_docto.equals("5")  || 
			ic_estatus_docto.equals("6")  || 
			ic_estatus_docto.equals("7")  || 
			ic_estatus_docto.equals("9")  || 
			ic_estatus_docto.equals("10") || 
			ic_estatus_docto.equals("21") || 
			ic_estatus_docto.equals("23") || 
			ic_estatus_docto.equals("28") || 
			ic_estatus_docto.equals("29") || 
			ic_estatus_docto.equals("30") || 
			ic_estatus_docto.equals("31") )?true:false;

		if(!numero_siaff.equals("")) {
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);
			ic_epo         = (String) numero.get("IC_EPO");
			ic_documento   = (String) numero.get("IC_DOCUMENTO");
			log.info("IC_EPO: " + ic_epo);
			log.info("IC_DOCUMENTO: " + ic_documento);
		}

		/***** Se arman las condiciones extra *****/
		conditions.add(strAforo);
		conditions.add(strAforoDL);
		conditions.add(iNoCliente);
		conditions.add(iNoCliente);
		conditions.add(iNoCliente);

		if (!"".equals(contrato)   ) {
			condicion.append(" AND upper(trim(D.CG_NUM_CONTRATO)) LIKE upper(?) ");
			this.conditions.add(contrato);
		}
		if (!"".equals(copade)   ) {
			condicion.append(" AND upper(trim(D.CG_NUM_COPADE)) LIKE upper(?) ");
			this.conditions.add(copade);
		}

		if(ic_pyme.equals("")          && cc_acuse.equals("")          && no_docto.equals("")   &&
			ic_moneda.equals("")        && df_fecha_docto_de.equals("") &&
			df_fecha_docto_a.equals("") && fn_monto_de.equals("")       && fn_monto_a.equals("") &&
			df_fecha_venc_de.equals("") && df_fecha_venc_a.equals("")   && ic_if.equals("") &&
			ic_estatus_docto.equals("") && camp_dina1.equals("")        && camp_dina2.equals("") &&
			ic_epo.equals("")           && ic_documento.equals("")){

			if(!"".equals(strPerfilCons) && "ADMIN EPO MAN0".equals(strPerfilCons)){
				condicion.append(" AND D.ic_estatus_docto in (?,?) ");
				conditions.add("30"); //Pendientes
				conditions.add("31"); //No Pendientes
			} else if(!"".equals(strPerfilCons) && "ADMIN EPO MAN1".equals(strPerfilCons)){
				condicion.append(" AND D.ic_estatus_docto in (?,?,?,?) ");
				conditions.add("28"); //Pre Negociables
				conditions.add("29"); //Pre No Negociables
				conditions.add("30"); //Pendientes
				conditions.add("31"); //No Pendientes
			}else{
				condicion.append(" AND D.ic_estatus_docto = ? ");
				conditions.add("2"); //Negociables
			}


			if (!"".equals(cmbFactoraje)   ) {
			    condicion.append(" AND D.CS_DSCTO_ESPECIAL = ? ");
			    this.conditions.add(cmbFactoraje);
			}
			if (!"".equals(validaDoctoEPO)   ) {
			    condicion.append(" AND D.CS_VALIDACION_JUR = ? ");
			    this.conditions.add(validaDoctoEPO);
			}
			
			if (!ord_proveedor.equals("") || !ord_if.equals("") || !ord_monto.equals("") || !ord_fec_venc.equals("") || !ord_estatus.equals("")) {
				condicion.append(" ORDER BY ");
				for (criterio=1;criterio<=5;criterio++){
					if (!ord_proveedor.equals("")) {
						if (Integer.parseInt(ord_proveedor)==criterio) {
							coma++;
							condicion.append( (coma == 1)?" D.ic_pyme ":", D.ic_pyme " );
						}
					}
					if (!ord_if.equals("")) {
						if (Integer.parseInt(ord_if)==criterio) {
							coma++;
							condicion.append( (coma == 1)?" D.ic_if ":", D.ic_if " );
						}
					}
					if (!ord_monto.equals("")) {
						if (Integer.parseInt(ord_monto)==criterio) {
							coma++;
							condicion.append( (coma == 1)?" D.fn_monto ":", D.fn_monto " );
						}
					}
					if (!ord_fec_venc.equals("")) {
						if (Integer.parseInt(ord_fec_venc)==criterio) {
							coma++;
							condicion.append( (coma == 1)?" D.df_fecha_venc ":", D.df_fecha_venc " );
						}
					}
					if (!ord_estatus.equals("")) {
						if (Integer.parseInt(ord_estatus)==criterio) {
							coma++;
							condicion.append( (coma == 1)?" D.ic_estatus_docto ":", D.ic_estatus_docto " );
						}
					}
				}
			} else{
				condicion.append(" ORDER BY d.ic_documento");
			}

		} else{

			if (!ic_pyme.equals("")) {
				condicion.append(" AND D.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!cc_acuse.equals("")) {
				if(cc_acuse.trim().indexOf("4")==0) { // si el acuse es de modificacion de montos
					condicion.append(" AND modif.cc_acuse = ? ");
					conditions.add(cc_acuse);
				} else {
					condicion.append(" AND D.cs_cambio_importe=? AND D.cc_acuse = ? ");
					conditions.add("N");
					conditions.add(cc_acuse);
				}
			}
			if (!no_docto.equals("")) {
				condicion.append(" AND D.ig_numero_docto = ? ");
				conditions.add(no_docto);
			}
			if (!ic_moneda.equals("")) {
				condicion.append(" AND D.ic_moneda = ? ");
				conditions.add(new Integer(ic_moneda));
			}
			if (!df_fecha_docto_de.equals("") && !df_fecha_docto_a.equals("")) {
				condicion.append(" AND D.df_fecha_docto >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.df_fecha_docto < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
				conditions.add(df_fecha_docto_de);
				conditions.add(df_fecha_docto_a);
			}
			if (!df_fecha_venc_de.equals("") && !df_fecha_venc_a.equals("")) {
				condicion.append(" AND D.df_fecha_venc >= trunc(TO_DATE(?,'DD/MM/YYYY')) and D.df_fecha_venc < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");
				conditions.add(df_fecha_venc_de);
				conditions.add(df_fecha_venc_a);
			}
			if (!fn_monto_de.equals("") && !fn_monto_a.equals("")) {
				condicion.append(" AND D.fn_monto BETWEEN ? and ? ");
				conditions.add(fn_monto_de);
				conditions.add(fn_monto_a);
			}
			if (!ic_if.equals("")) {
				condicion.append(" AND D.ic_if = ? ");
				conditions.add(ic_if);
			}
			if (!ic_estatus_docto.equals("")) {
				if (ic_estatus_docto.equals("4")) {
					condicion.append(" AND D.ic_estatus_docto in (?,?) ");
					conditions.add("4");  //Operada
					conditions.add("16"); //Aplicado a credito
				} else {
					condicion.append(" AND D.ic_estatus_docto = ? ");
					conditions.add(ic_estatus_docto);
				}
			}
			if (!camp_dina1.equals("")) {
				condicion.append(" AND D.cg_campo1 = ? ");
				conditions.add(camp_dina1);
			}
			if (!camp_dina2.equals("")) {
				condicion.append(" AND D.cg_campo2 = ? ");
				conditions.add(camp_dina2);
			}

			if(!"ADMIN EPO MAN0".equals(strPerfilCons) && !"ADMIN EPO MAN1".equals(strPerfilCons) ){//FODEA 053-2009 FVR
				if (!ic_if.equals("") && ic_estatus_docto.equals("") && ic_moneda.equals("")){
					condicion.append(" AND D.ic_estatus_docto in (?,?,?,?,?) ");//FODEA 005 - 2009 ACF
					conditions.add("3"); //Seleccionada Pyme
					conditions.add("4"); //Operada
					conditions.add("11"); //Operada Pagada 
					conditions.add("16"); //Aplicada a credito
					conditions.add("26"); //Programado PyME -- FODEA 005 - 2009 ACF
				} else if(!ic_moneda.equals("") && ic_estatus_docto.equals("") && ic_if.equals("")){
					condicion.append(" AND D.ic_estatus_docto = ? ");
					conditions.add(new Integer(2)); //Negociable
				} else if(ic_estatus_docto.equals("") || !operaFirmaM.equals("S")){
					condicion.append(" AND D.ic_estatus_docto not in (?,?,?,?,?,?,?,?,?,?,?,?,?) ");//FODEA 005 - 2009
					conditions.add("8"); //Seleccionada IF (Obsoleto)
					conditions.add("13"); //Cancelado
					conditions.add("14"); //Incumplimiento Pend. de Pago
					conditions.add("15"); //Pedido sin Operar
					conditions.add("16"); //Aplicado a Credito
					conditions.add("17"); //Credito Vencido
					conditions.add("18"); //Cancelado Pendiente de Pago
					conditions.add("19"); //Pedido Sustituto
					conditions.add("20"); //Rechazado IF
					conditions.add("22"); //Pendiente de pago IF
					conditions.add("23"); //Pignorado
					conditions.add("28"); //Pre Negociable
					conditions.add("29"); //Pre No Negociable
				}
			}

			if(!ic_epo.equals("")){
				condicion.append(" AND D.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_documento.equals("")){
				condicion.append(" AND D.ic_documento = ? ");
				conditions.add(ic_documento);
			}


			if (!"".equals(cmbFactoraje)   ) {
			    condicion.append(" AND D.CS_DSCTO_ESPECIAL = ? ");
			    this.conditions.add(cmbFactoraje);
			}
			if (!"".equals(validaDoctoEPO)   ) {
			    condicion.append(" AND D.CS_VALIDACION_JUR = ? ");
			    this.conditions.add(validaDoctoEPO);
			}
		    
			coma=0;
			if (!ord_proveedor.equals("") || !ord_if.equals("") || !ord_monto.equals("") || !ord_fec_venc.equals("") || !ord_estatus.equals("")) {
				condicion.append(" ORDER BY ");
				for (criterio=1;criterio<=5;criterio++){
					if (!ord_proveedor.equals("")) {
						if (Integer.parseInt(ord_proveedor)==criterio) {
							coma++;
							condicion.append((coma == 1)?" D.ic_pyme ":", D.ic_pyme ");
						}
					}
					if (!ord_if.equals("")) {
						if (Integer.parseInt(ord_if)==criterio) {
							coma++;
							condicion.append((coma == 1)?" D.ic_if ":", D.ic_if ");
						}
					}
					if (!ord_monto.equals("")) {
						if (Integer.parseInt(ord_monto)==criterio) {
							coma++;
							condicion.append( (coma == 1)?" D.fn_monto ":", D.fn_monto ");
						}
					}
					if (!ord_fec_venc.equals("")) {
						if (Integer.parseInt(ord_fec_venc)==criterio) {
							coma++;
							condicion.append( (coma == 1)?" D.df_fecha_venc ":", D.df_fecha_venc ");
						}
					}
					if (!ord_estatus.equals("")) {
						if (Integer.parseInt(ord_estatus)==criterio) {
							coma++;
							condicion.append( (coma == 1)?" D.ic_estatus_docto ":", D.ic_estatus_docto ");
						}
					}
				}//Fin del for para los criterios
			} else{// if del ordenamiento
				condicion.append(" ORDER BY d.ic_documento");
			}
		}

		/***** Se genera el query principal *****/
		qrySentencia.append(" SELECT /*+ USE_NL(p i i2 ed m ds s d pe modif ma tf) */ ");
		qrySentencia.append(" D.ic_documento, ");
		qrySentencia.append(" (decode (PE.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social)as cg_razon_social_pyme,  ");
		qrySentencia.append(" D.ic_epo, ");
		qrySentencia.append(" (decode (cs_cambio_importe,'N',D.cc_acuse,'S',modif.cc_acuse)) as cc_acuse,  ");
		qrySentencia.append(" D.ig_numero_docto, ");
		qrySentencia.append(" TO_CHAR(D.df_fecha_docto,'DD/MM/YYYY'),  ");
		qrySentencia.append(" TO_CHAR(D.df_fecha_venc,'DD/MM/YYYY'), ");
		qrySentencia.append(" M.cd_nombre"+idioma+ " as cd_nombre, ");
		qrySentencia.append(" D.fn_monto, ");
		qrySentencia.append(" D.cs_dscto_especial,  ");
		qrySentencia.append(" D.fn_porc_anticipo, ");
		qrySentencia.append(" D.fn_monto_dscto,  ");
		qrySentencia.append(" Decode(D.ic_estatus_docto, 16,'" +operado+"', ED.cd_descripcion"+idioma+")as cd_descripcion,  ");
		qrySentencia.append(" (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social)as cg_razon_social_if,  ");
		qrySentencia.append(" S.ic_folio, ");
		qrySentencia.append(" D.ic_estatus_docto, ");
		qrySentencia.append(" D.ic_moneda, ");
		qrySentencia.append(" PE.cg_pyme_epo_interno, ");
		qrySentencia.append(" D.ct_referencia,  cs_cambio_importe, 0 detalles, D.cg_campo1, D.cg_campo2, D.cg_campo3, D.cg_campo4, D.cg_campo5,");
		qrySentencia.append(" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN i2.cg_razon_social ELSE '' END AS beneficiario, ");
		qrySentencia.append(" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN d.fn_porc_beneficiario ELSE 0 END AS fn_porc_beneficiario, ");
		qrySentencia.append(" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN NVL(ds.fn_importe_recibir_benef, d.fn_monto * d.fn_porc_beneficiario / 100 ) ELSE 0 END AS RECIBIR_BENEF, ");
		qrySentencia.append(" CASE WHEN d.cs_dscto_especial IN ('D','I') THEN ds.in_importe_recibir - (ds.fn_importe_recibir_benef) ELSE 0 END AS NETO_REC_PYME, ");
		qrySentencia.append(" ?,?,");
		qrySentencia.append(" TO_CHAR(D.df_fecha_venc_pyme,'DD/MM/YYYY') AS df_fecha_venc_pyme, ");
		qrySentencia.append(" TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO, ");
		qrySentencia.append(" ma.CG_RAZON_SOCIAL, ");
		qrySentencia.append(" tf.cg_nombre as TIPO_FACTORAJE, ");
		qrySentencia.append(" TO_CHAR(ds.df_programacion, 'dd/mm/yyyy') AS fecha_registro_operacion, ");
		if(!ocultarDoctosAplicados){
			qrySentencia.append(" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND d.ic_docto_asociado IS NOT NULL THEN 'true' ELSE 'false' END AS existeDocumentoAsociado, ");
			qrySentencia.append(" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM com_documento WHERE ic_docto_asociado = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredSimpleApl, ");
			qrySentencia.append(" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_NOTA_CREDITO = d.ic_documento) THEN 'true' ELSE 'false' END AS existeRefEnComrelNotaDocto, ");
			qrySentencia.append(" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_DOCUMENTO = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredMultApl, "); 
		}
		qrySentencia.append(" DECODE(                                                  ");
		qrySentencia.append("           S.IC_ESTATUS_SOLIC,                            ");
		qrySentencia.append("           1, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "); // (1) Seleccionada IF
		qrySentencia.append("           2, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "); // (2) En proceso
		qrySentencia.append("           3, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "); // (3) Operada
		qrySentencia.append("           5, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "); // (5) Operada Pagada
		qrySentencia.append("          10, TO_CHAR(S.DF_FECHA_SOLICITUD,'DD/MM/YYYY'), "); // (10) Operado con Fondeo Propio
		qrySentencia.append("          'N/A'                                           "); // PARA TODOS LOS OTROS CASOS
		qrySentencia.append("       ) AS FECHA_AUTORIZACION_IF,                        "); // Fodea 040 - 2011 By JSHD 
		qrySentencia.append(" 'N' nota_simple, 'N' nota_simple_docto, 'N' nota_multiple, 'N' nota_multiple_docto, '' numero_docto,"); //Campos a llenar durante el llamado de la consulta 13consulta01ext.data.jsp
		qrySentencia.append(" 'ConsDoctosDE::getDocumentQueryFile' ");
		qrySentencia.append(" , decode( d.cs_dscto_especial,  'D', decode (d.CS_VALIDACION_JUR, 'S', 'SI', 'N', 'NO' ), '' )  as  VALIDACION_JUR ");
		
		qrySentencia.append(",  d.CG_NUM_CONTRATO AS CONTRATO, d.CG_NUM_COPADE AS COPADE "); 
			
		qrySentencia.append(" FROM ");
		qrySentencia.append(" comcat_pyme P, ");
		qrySentencia.append(" comcat_if I, ");
		qrySentencia.append(" comcat_if i2, ");
		qrySentencia.append(" comcat_estatus_docto ED, ");
		qrySentencia.append(" comcat_moneda M, ");
		qrySentencia.append(" com_docto_seleccionado ds , ");
		qrySentencia.append(" com_solicitud S, ");
		qrySentencia.append(" com_documento D, ");
		qrySentencia.append(" COMCAT_MANDANTE ma, ");
		qrySentencia.append(" COMCAT_TIPO_FACTORAJE tf, ");
		qrySentencia.append(" (SELECT * FROM comrel_pyme_epo WHERE ic_epo = ? ) PE, ");
		qrySentencia.append(" comhis_cambio_est_modif_monto modif ");
		qrySentencia.append(" WHERE D.ic_pyme = PE.ic_pyme  ");
		qrySentencia.append("   AND PE.ic_pyme = P.ic_pyme  ");
		qrySentencia.append("   AND D.ic_epo = PE.ic_epo  ");
		qrySentencia.append("   AND D.ic_moneda = M.ic_moneda  ");
		qrySentencia.append("   AND D.ic_estatus_docto = ED.ic_estatus_docto  ");
		qrySentencia.append("   AND D.ic_if = I.ic_if(+)  ");
		qrySentencia.append("   AND ds.ic_documento = S.ic_documento(+)  ");
		qrySentencia.append("   AND D.ic_documento = modif.ic_documento (+)  ");
		qrySentencia.append("   AND D.ic_documento = ds.ic_documento (+)  ");
		qrySentencia.append("   AND d.ic_beneficiario = i2.ic_if (+) ");
		qrySentencia.append("   AND ma.IC_MANDANTE(+) = d.IC_MANDANTE ");
		qrySentencia.append("   AND d.cs_dscto_especial = tf.cc_tipo_factoraje  ");
		qrySentencia.append("   AND D.ic_epo = ?");
		qrySentencia.append("   AND PE.ic_epo= ?");
		
		qrySentencia.append(condicion.toString());
		
		log.info("query: " + qrySentencia.toString());
		log.info("conditions: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		CreaArchivo archivo = new CreaArchivo();
		Registros regCamposAdicionales = null;

		String fechaEntrega             = "";
		String tipoCompra               = "";
		String clavePresupuestaria      = "";
		String periodo                  = "";
		String IC_PYME                  = "";
		String CC_ACUSE                 = "";
		String IG_NUMERO_DOCTO          = "";
		String DF_FECHA_DOCTO           = "";
		String DF_FECHA_VENC            = "";
		String DF_FECHA_VENC_PYME       = "";
		String IC_MONEDA                = "";
		String CS_DSCTO_ESPECIAL        = "";
		String IC_ESTATUS_DOCTO         = "";
		String IC_IF                    = "";
		String IC_FOLIO                 = "";
		String NO_INT_PYME              = "";
		String CT_REFERENCIA            = "";
		String CS_CAMBIO_IMPORTE        = "";
		String CG_CAMPO1                = "";
		String CG_CAMPO2                = "";
		String CG_CAMPO3                = "";
		String CG_CAMPO4                = "";
		String CG_CAMPO5                = "";
		String nombreTipoFactoraje      = "";
		String icDocumento              = "";
		String numeroSIAFF              = "";
		String mandante                 = "";
		String tipoFactorajeDesc        = "";
		String fecha_registro_operacion = "";
		String fechaAutorizacionIF      = "";
		String beneficiario             = "";
		String sFechaVencPyme           = "";
		String cd17                     = "";
		String cd18                     = "";
		String cd19                     = "";
		String cd20                     = "";
		String cd21                     = "";
		String numeroDocumento          = "";

		int nRegistros        = 0;
		int numRegistros      = 0;
		int numRegistrosMN    = 0;
		int numRegistrosDL    = 0;
		int vtmn              = 0;
		int vesp              = 0;
		int vnumd             = 0;
		int cont              = 0;
		int coma              = 0;
		int total_doctos_dol  = 0;
		int total_doctos_mn   = 0;
		int val               = 0;
		int IC_DOCUMENTO      = 0;
		int IC_EPO            = 0;
		int IC2_ESTATUS_DOCTO = 0;
		int NO_MONEDA         = 0;
		int tam               = 0;
		int totCol            = 0;
		int nCol              = 0;

		BigDecimal montoTotalMN      = new BigDecimal("0.00");
		BigDecimal montoTotalDL      = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoMN = new BigDecimal("0.00");
		BigDecimal montoTotalDsctoDL = new BigDecimal("0.00");

	   BigDecimal saldoMN           = new BigDecimal(0);
	   BigDecimal saldoUSD          = new BigDecimal(0);
	   BigDecimal totalDocsNegMN    = new BigDecimal(0);
	   BigDecimal totalDocsNegUSD   = new BigDecimal(0);
	   BigDecimal totalNCNegMN      = new BigDecimal(0);
	   BigDecimal totalNCNegUSD     = new BigDecimal(0);


		double total_dolares                  = 0;
		double total_mn                       = 0;
		double FN_MONTO                       = 0;
		double FN_PORC_ANTICIPO               = 0;
		double FN_MONTO_DSCTO                 = 0;
		double dblPorciento                   = 0;
		double dblMontoDescuento              = 0;
		double dblTotalDescuentoPesos         = 0;
		double dblTotalDescuentoDolares       = 0;
		double porciento_beneficiario         = 0;
		double neto_a_recibir_pyme            = 0;
		double importe_a_recibir_beneficiario = 0;

		boolean gen_archivo                                 = false;
		boolean dolaresDocto                                = false;
		boolean nacionalDocto                               = false;
		boolean bandera_neto_a_recibir_pyme                 = false;
		boolean muestraNegociables                          = false;
		boolean bOperaFactorajeVencido                      = false;
		boolean bOperaFactorajeDistribuido                  = false;
		boolean bOperaFactorajeNotaDeCredito                = false;
		boolean bOperaFactorajeVencInfonavit                = false;
		boolean bFactorajeIf                                = false;
		boolean esNotaDeCreditoAplicada                     = false;
		boolean esDocumentoConNotaDeCreditoMultipleAplicada = false;
		boolean esNotaDeCreditoSimpleAplicada               = false;
		boolean esDocumentoConNotaDeCreditoSimpleAplicada   = false;
		boolean bOperaFactConMandato                        = false;
		boolean bTipoFactoraje                              = false;
		boolean bMostrarFechaAutorizacionIFInfoDoctos       = false;
		boolean  ocultarDoctosAplicados = (
			ic_estatus_docto.equals("")   || 
			ic_estatus_docto.equals("1")  || 
			ic_estatus_docto.equals("2")  || 
			ic_estatus_docto.equals("5")  || 
			ic_estatus_docto.equals("6")  || 
			ic_estatus_docto.equals("7")  || 
			ic_estatus_docto.equals("9")  || 
			ic_estatus_docto.equals("10") || 
			ic_estatus_docto.equals("21") || 
			ic_estatus_docto.equals("23") || 
			ic_estatus_docto.equals("28") || 
			ic_estatus_docto.equals("29") || 
			ic_estatus_docto.equals("30") || 
			ic_estatus_docto.equals("31") )?true:false;

		try{
			if(tipo.equals("PDF")){

				/***** A S E R C A *****/
				String ic_epo_aserca = "314";
				boolean layOutAserca = false;
				if(ic_epo_aserca.equals(iNoCliente))
					layOutAserca = true;

				ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
				ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
				sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(iNoCliente);
				boolean estaHabilitadoNumeroSIAFF = BeanParamDscto.estaHabilitadoNumeroSIAFF(iNoCliente);

				muestraNegociables = "2".equals(ic_estatus_docto)?true:false;
				if(ic_pyme.equals("")       && cc_acuse.equals("")          && no_docto.equals("")   &&
				ic_moneda.equals("")        && df_fecha_docto_de.equals("") && camp_dina1.equals("") &&
				df_fecha_docto_a.equals("") && fn_monto_de.equals("")       && fn_monto_a.equals("") &&
				df_fecha_venc_de.equals("") && df_fecha_venc_a.equals("")   && ic_if.equals("")      &&
				ic_estatus_docto.equals("") && camp_dina2.equals("")) {
					muestraNegociables = true;
				}
				if (!ic_moneda.equals("") && ic_estatus_docto.equals("")) {
					muestraNegociables = true;
				}

				Hashtable alParamEPO1 = new Hashtable(); 
				alParamEPO1 = BeanParamDscto.getParametrosEPO(iNoCliente,1);
				if (alParamEPO1!=null) {
					bOperaFactConMandato                  = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
					bOperaFactorajeVencido                = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
					bOperaFactorajeDistribuido            = ("N".equals(alParamEPO1.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
					bOperaFactorajeNotaDeCredito          = ("N".equals(alParamEPO1.get("OPERA_NOTAS_CRED").toString()))?false:true;
					bOperaFactorajeVencInfonavit          = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;
					bFactorajeIf                          = ("N".equals(alParamEPO1.get("FACTORAJE_IF").toString()))?false:true;
					bMostrarFechaAutorizacionIFInfoDoctos = "S".equals((String) alParamEPO1.get("CS_SHOW_FCHA_AUT_IF_INFO_DCTOS") )?true:false;
				}

				bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaDeCredito || bOperaFactConMandato || bOperaFactorajeVencInfonavit||bFactorajeIf)?true:false;

				int totalcd = 0;
				totalcd = BeanSeleccionDocumento.getTotalCamposAdicionales(iNoCliente);
				if(totalcd >=1) {
					regCamposAdicionales = new Registros();
					regCamposAdicionales = BeanParamDscto.getCamposAdicionales(iNoCliente);
				}

				/***** ARMA EL ARCHIVO PDF *****/
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
				horaActual, "formas", ComunesPDF.RIGHT);
				pdfDoc.addText(" ", "formas", ComunesPDF.RIGHT);

				while(rs.next()){

					IC_DOCUMENTO       = Integer.parseInt((rs.getString(1)==null)?"0":rs.getString(1));
					icDocumento        = (rs.getString(1)==null)?"0":rs.getString(1);
					IC_PYME            = (rs.getString(2)==null)?"":rs.getString(2);
					IC_EPO             = Integer.parseInt((rs.getString(3)==null)?"0":rs.getString(3));
					CC_ACUSE           = (rs.getString(4)==null)?"":rs.getString(4);
					IG_NUMERO_DOCTO    = (rs.getString(5)==null)?"":rs.getString(5);
					DF_FECHA_DOCTO     = (rs.getString(6)==null)?"":rs.getString(6);
					DF_FECHA_VENC      = (rs.getString(7)==null)?"":rs.getString(7);
					IC_MONEDA          = (rs.getString(8)==null)?"":rs.getString(8);
					FN_MONTO           = Double.parseDouble((rs.getString(9)==null)?"0":rs.getString(9));
					CS_DSCTO_ESPECIAL  = (rs.getString(10)==null)?"":rs.getString(10);
					FN_PORC_ANTICIPO   = Double.parseDouble((rs.getString(11)==null)?"0":rs.getString(11));
					FN_MONTO_DSCTO     = Double.parseDouble((rs.getString(12)==null)?"0":rs.getString(12));
					IC_ESTATUS_DOCTO   = (rs.getString(13)==null)?"":rs.getString(13);
					IC_IF              = (rs.getString(14)==null)?"":rs.getString(14);
					IC_FOLIO           = (rs.getString(15)==null)?"":rs.getString(15);
					IC2_ESTATUS_DOCTO  = Integer.parseInt((rs.getString(16)==null)?"0":rs.getString(16));
					NO_MONEDA          = Integer.parseInt((rs.getString(17)==null)?"0":rs.getString(17));
					NO_INT_PYME        = (rs.getString(18)==null)?"":rs.getString(18);
					CT_REFERENCIA      = (rs.getString(19)==null)?"":rs.getString(19);
					CS_CAMBIO_IMPORTE  = (rs.getString(20)==null)?"":rs.getString(20).trim();
					DF_FECHA_VENC_PYME = (rs.getString("DF_FECHA_VENC_PYME")==null)?"":rs.getString("DF_FECHA_VENC_PYME");

					mandante                 = (rs.getString(38)==null)?"":rs.getString(38);
					tipoFactorajeDesc        = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE").trim();
					fecha_registro_operacion = rs.getString("fecha_registro_operacion")==null?"":rs.getString("fecha_registro_operacion");
					fechaAutorizacionIF      = rs.getString("fecha_autorizacion_if")   ==null?"":rs.getString("fecha_autorizacion_if");

					numeroSIAFF                    = getNumeroSIAFF(Integer.toString(IC_EPO),Integer.toString(IC_DOCUMENTO));
					beneficiario                   = rs.getString("beneficiario")==null?"":rs.getString("beneficiario");
					porciento_beneficiario         = Double.parseDouble(rs.getString("fn_porc_beneficiario")==null?"0":rs.getString("fn_porc_beneficiario"));
					importe_a_recibir_beneficiario = Double.parseDouble(rs.getString("RECIBIR_BENEF")==null?"0":rs.getString("RECIBIR_BENEF"));
					String validacionJUR = (rs.getString("VALIDACION_JUR")==null)?"":rs.getString("VALIDACION_JUR");
					String contratoArch = (rs.getString("CONTRATO")==null)?"":rs.getString("CONTRATO");
					String copadeArch = (rs.getString("COPADE")==null)?"":rs.getString("COPADE");
					
					if(bandera_neto_a_recibir_pyme)
						neto_a_recibir_pyme = Double.parseDouble(rs.getString("NETO_REC_PYME")==null?"0":rs.getString("NETO_REC_PYME"));

					if( banderaTablaDoctos.equals("1") ){
						fechaEntrega        = (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
						tipoCompra          = (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
						clavePresupuestaria = (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
						periodo             = (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");
					}
					nombreTipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
					// Fodea 002 - 2010
					if(!ocultarDoctosAplicados){
						esNotaDeCreditoSimpleAplicada                = ( nombreTipoFactoraje.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeDocumentoAsociado(icDocumento) )?true:false;
						esDocumentoConNotaDeCreditoSimpleAplicada    = ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(icDocumento))?true:false;
						esNotaDeCreditoAplicada                      = (nombreTipoFactoraje.equals("C") && (ic_estatus_docto.equals("3") || ic_estatus_docto.equals("4") || ic_estatus_docto.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(icDocumento))?true:false;
						esDocumentoConNotaDeCreditoMultipleAplicada  = (!nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(icDocumento))?true:false;
						if(esNotaDeCreditoSimpleAplicada){
							numeroDocumento                           = BeanSeleccionDocumento.getNumeroDoctoAsociado(icDocumento); 
						}else if(esDocumentoConNotaDeCreditoSimpleAplicada){
							numeroDocumento                           = BeanSeleccionDocumento.getNumeroNotaCreditoAsociada(icDocumento);
						}
					}

					if(nRegistros==0){
						List lEncabezados = new ArrayList();
						lEncabezados.add("A");
						lEncabezados.add("Nombre Proveedor");
						lEncabezados.add("No. Documento");
						lEncabezados.add("No. Acuse");
						lEncabezados.add("Fecha Emisi�n");
						lEncabezados.add("Fecha Vencimiento");
						if(!"".equals(sFechaVencPyme)) {
							lEncabezados.add("Fecha Vencimiento Proveedor");
						}
						lEncabezados.add("Moneda");
						if(!layOutAserca) {
							if(bTipoFactoraje){
								lEncabezados.add("Tipo Factoraje");
							}
							lEncabezados.add("Monto");
							lEncabezados.add("Estatus");
							lEncabezados.add("IF");
							lEncabezados.add("No. Solicitud");
							if (regCamposAdicionales != null){
								while (regCamposAdicionales.next()){
									lEncabezados.add(regCamposAdicionales.getString("NOMBRE_CAMPO").trim());
								}
							}
							lEncabezados.add("No. Proveedor");
						}
						totCol = lEncabezados.size();
						pdfDoc.setLTable(totCol, 100);
						for(int i = 0; i< totCol; i++){
						   pdfDoc.setLCell(""+lEncabezados.get(i), "celda01", ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("B", "celda01", ComunesPDF.CENTER);
						nCol++;
						if(!layOutAserca) {
							pdfDoc.setLCell("Referencia", "celda01", ComunesPDF.CENTER);
							pdfDoc.setLCell("Clave Estatus", "celda01", ComunesPDF.CENTER);
							pdfDoc.setLCell("% Descuento", "celda01", ComunesPDF.CENTER);
							pdfDoc.setLCell("Monto a Descontar", "celda01", ComunesPDF.CENTER);
							nCol+=4;
						}
						if(!ocultarDoctosAplicados){
							pdfDoc.setLCell("Doctos Aplicados a Nota de Credito", "celda01", ComunesPDF.CENTER);
							nCol++;
						}

						if(!layOutAserca){
							if(bOperaFactorajeDistribuido || bOperaFactorajeVencInfonavit){
								if(bandera_neto_a_recibir_pyme){
									pdfDoc.setLCell("Neto Recibir Pyme", "celda01", ComunesPDF.CENTER);
									nCol++;
								}
								pdfDoc.setLCell("Beneficiario", "celda01", ComunesPDF.CENTER);
								pdfDoc.setLCell(" % Beneficiario", "celda01", ComunesPDF.CENTER);
								pdfDoc.setLCell("Importe a Recibir BeneficiarioB", "celda01", ComunesPDF.CENTER);
								nCol+=3;
							}
						} else {
							pdfDoc.setLCell("Monto", "celda01", ComunesPDF.CENTER);
							pdfDoc.setLCell(" % Descuento", "celda01", ComunesPDF.CENTER);
							pdfDoc.setLCell("Monto a Descontar", "celda01", ComunesPDF.CENTER);
							pdfDoc.setLCell("Estatus", "formas", ComunesPDF.CENTER);
							nCol+=4;
							if (regCamposAdicionales != null){
								while (regCamposAdicionales.next()){
									pdfDoc.setLCell(regCamposAdicionales.getString("NOMBRE_CAMPO").trim(), "celda01", ComunesPDF.CENTER);
									nCol++;
								}
							}
						}
						if(estaHabilitadoNumeroSIAFF){
							pdfDoc.setLCell("Digito Identificador", "celda01", ComunesPDF.CENTER);
							nCol++;
							//lEncabezados.add("Digito Identificador");
						}

						if(banderaTablaDoctos.equals("1")) {
							pdfDoc.setLCell("Fecha de Recepci�n de Bienes y Servicios", "celda01", ComunesPDF.CENTER);
							pdfDoc.setLCell("Tipo de Compra (procedimiento)", "celda01", ComunesPDF.CENTER);
							pdfDoc.setLCell("Clasificador por Objeto del Gasto", "celda01", ComunesPDF.CENTER);
							pdfDoc.setLCell("Plazo M�ximo", "celda01", ComunesPDF.CENTER);
							nCol+=4;
						}

						if(bOperaFactConMandato){
							pdfDoc.setLCell("Mandante", "celda01", ComunesPDF.CENTER);
							nCol++;
						}

						if(ic_estatus_docto.equals("26")){
							pdfDoc.setLCell("Fecha Registro Operaci�n", "celda01", ComunesPDF.CENTER);
							nCol++;
						}

						if(bMostrarFechaAutorizacionIFInfoDoctos){
							pdfDoc.setLCell("Fecha Autorizaci�n IF", "celda01", ComunesPDF.CENTER);
							nCol++;
						}
                                                //QC-2018
                                                if(bOperaFactorajeDistribuido==true){
                                                    pdfDoc.setLCell("Validaci�n de Documentos EPO", "celda01", ComunesPDF.CENTER);
                                                }
						if ("ADMIN PEMEX AMP".equals(strPerfilCons)  ) {
							pdfDoc.setLCell("Contrato", "celda01", ComunesPDF.CENTER);
							nCol++;
							pdfDoc.setLCell("COPADE", "celda01", ComunesPDF.CENTER);
							nCol++;
						}
						tam = totCol-nCol;
						if (tam > 0){
							pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,tam);
						}
						pdfDoc.setLHeaders();
						countReg = 0;
					} //if registros == 0"

					if (IC2_ESTATUS_DOCTO == 2 || IC2_ESTATUS_DOCTO == 5 || IC2_ESTATUS_DOCTO == 6 || IC2_ESTATUS_DOCTO == 7 || IC2_ESTATUS_DOCTO == 9 || IC2_ESTATUS_DOCTO == 10) {
						if(NO_MONEDA == 1) {
							dblPorciento = new Double(strAforo).doubleValue() * 100;
						} else if(NO_MONEDA == 54) {
							dblPorciento = new Double(strAforoDL).doubleValue() * 100;
						}
					} else{
						dblPorciento = FN_PORC_ANTICIPO;
					}
					dblMontoDescuento = FN_MONTO * (dblPorciento / 100); //FN_MONTO_DSCTO;
					/* Para factoraje Vencido */
					CS_DSCTO_ESPECIAL = (CS_DSCTO_ESPECIAL==null)?"":CS_DSCTO_ESPECIAL.trim();
					/* si factoraje vencido y distribuido: se aplica 100% de anticipo */
					if(CS_DSCTO_ESPECIAL.equals("V") || CS_DSCTO_ESPECIAL.equals("D") || CS_DSCTO_ESPECIAL.equals("C") || CS_DSCTO_ESPECIAL.equals("I")) {
						dblMontoDescuento=FN_MONTO;
						dblPorciento = 100;
					}

					CS_DSCTO_ESPECIAL = tipoFactorajeDesc ;

					val++;
					gen_archivo = true;
					if(IC_PYME!=null && !IC_PYME.equals("")) { IC_PYME=IC_PYME.replace(',',' '); }
					if(IC_IF == null){IC_IF="";}else{ IC_IF=IC_IF.replace(',',' '); } if(CT_REFERENCIA==null){CT_REFERENCIA="";}
					pdfDoc.setLCell("A", "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(IC_PYME, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(IG_NUMERO_DOCTO, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(CC_ACUSE, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(DF_FECHA_DOCTO, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(DF_FECHA_VENC, "formas", ComunesPDF.CENTER);

					if(!"".equals(sFechaVencPyme)){
						pdfDoc.setLCell(DF_FECHA_VENC_PYME, "formas", ComunesPDF.CENTER);
					}

					pdfDoc.setLCell(IC_MONEDA, "formas", ComunesPDF.CENTER);
					if(!layOutAserca) {
						if(bTipoFactoraje) {
							pdfDoc.setLCell(CS_DSCTO_ESPECIAL, "formas", ComunesPDF.CENTER);
						}
						pdfDoc.setLCell(Comunes.formatoDecimal(FN_MONTO,2,false), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(IC_ESTATUS_DOCTO, "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(IC_IF, "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(IC_FOLIO, "formas", ComunesPDF.CENTER);

						if (regCamposAdicionales != null){
							cont = 1;
							while (regCamposAdicionales.next()){
								if ( cont == 1 ) {
									CG_CAMPO1 = (rs.getString(22)==null)?"":rs.getString(22).trim();
									pdfDoc.setLCell(CG_CAMPO1,"formas",ComunesPDF.CENTER);
								}
								if (cont == 2) {
									CG_CAMPO2 = (rs.getString(23)==null)?"":rs.getString(23).trim();
									pdfDoc.setLCell(CG_CAMPO2,"formas",ComunesPDF.CENTER);
								}
								if (cont == 3) {
									CG_CAMPO3 = (rs.getString(24)==null)?"":rs.getString(24).trim();
									pdfDoc.setLCell(CG_CAMPO3,"formas",ComunesPDF.CENTER);
								}
								if (cont == 4) {
									CG_CAMPO4 = (rs.getString(25)==null)?"":rs.getString(25).trim();
									pdfDoc.setLCell(CG_CAMPO4,"formas",ComunesPDF.CENTER);
								}
								if (cont == 5) {
									CG_CAMPO5 = (rs.getString(26)==null)?"":rs.getString(26).trim();
									pdfDoc.setLCell(CG_CAMPO5,"formas",ComunesPDF.CENTER);
								}
								cont ++;
							}
						}
						pdfDoc.setLCell(NO_INT_PYME, "formas", ComunesPDF.CENTER);
					}
					pdfDoc.setLCell("B", "formas", ComunesPDF.CENTER);
					if(!layOutAserca) {
						pdfDoc.setLCell(CT_REFERENCIA, "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(Integer.toString(IC2_ESTATUS_DOCTO), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(Comunes.formatoDecimal(String.valueOf(dblPorciento),0,false), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(String.valueOf(dblMontoDescuento), "formas", ComunesPDF.CENTER);
					}
					if(!ocultarDoctosAplicados){
						if(esNotaDeCreditoSimpleAplicada || esDocumentoConNotaDeCreditoSimpleAplicada ){
							// Notas de Credito Simple   
							pdfDoc.setLCell(numeroDocumento, "formas", ComunesPDF.CENTER);
						}else if(esNotaDeCreditoAplicada || esDocumentoConNotaDeCreditoMultipleAplicada){
							// Notas de Credito Multiple 
							pdfDoc.setLCell("Si", "formas", ComunesPDF.CENTER);
						}else{
							pdfDoc.setLCell("", "formas", ComunesPDF.CENTER);
						}
					}

					if(!layOutAserca){
						if(bOperaFactorajeDistribuido || bOperaFactorajeVencInfonavit){
							if(bandera_neto_a_recibir_pyme && 0 != neto_a_recibir_pyme){
								pdfDoc.setLCell(Comunes.formatoDecimal(neto_a_recibir_pyme,2,false), "formas", ComunesPDF.CENTER);
							}
							pdfDoc.setLCell(beneficiario, "formas", ComunesPDF.CENTER);
							if(0 != porciento_beneficiario)
								pdfDoc.setLCell(String.valueOf(porciento_beneficiario), "forma", ComunesPDF.CENTER);
							else
								pdfDoc.setLCell("", "formas", ComunesPDF.CENTER);
							if(0 != importe_a_recibir_beneficiario)
								pdfDoc.setLCell(Comunes.formatoDecimal(importe_a_recibir_beneficiario,2,false), "formas", ComunesPDF.CENTER);
							else
								pdfDoc.setLCell("", "formas", ComunesPDF.CENTER);
						}
					} else {
						pdfDoc.setLCell(Comunes.formatoDecimal(FN_MONTO,2,false), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(Comunes.formatoDecimal(String.valueOf(dblPorciento),0,false), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(Comunes.formatoDecimal(FN_MONTO,2,false), "formas", ComunesPDF.CENTER);
						if (regCamposAdicionales != null){
							cont = 1;
							while (regCamposAdicionales.next()){
								if ( cont == 1 ) {
									CG_CAMPO1 = (rs.getString(22)==null)?"":rs.getString(22).trim();
									pdfDoc.setLCell(CG_CAMPO1,"formas",ComunesPDF.CENTER);
								}
								if (cont == 2) {
									CG_CAMPO2 = (rs.getString(23)==null)?"":rs.getString(23).trim();
									pdfDoc.setLCell(CG_CAMPO2,"formas",ComunesPDF.CENTER);
								}
								if (cont == 3) {
									CG_CAMPO3 = (rs.getString(24)==null)?"":rs.getString(24).trim();
									pdfDoc.setLCell(CG_CAMPO3,"formas",ComunesPDF.CENTER);
								}
								if (cont == 4) {
									CG_CAMPO4 = (rs.getString(25)==null)?"":rs.getString(25).trim();
									pdfDoc.setLCell(CG_CAMPO4,"formas",ComunesPDF.CENTER);
								}
								if (cont == 5) {
									CG_CAMPO5 = (rs.getString(26)==null)?"":rs.getString(26).trim();
									pdfDoc.setLCell(CG_CAMPO5,"formas",ComunesPDF.CENTER);
								}
								cont ++;
							}
						}
					}

					if(estaHabilitadoNumeroSIAFF){
						pdfDoc.setLCell(numeroSIAFF, "formasmen", ComunesPDF.CENTER);
					}

					if(banderaTablaDoctos.equals("1")) {
						pdfDoc.setLCell(fechaEntrega, "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell(tipoCompra, "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell(clavePresupuestaria, "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell(periodo, "formasmen", ComunesPDF.CENTER);
					}
					if(bOperaFactConMandato){
						pdfDoc.setLCell(mandante, "formasmen", ComunesPDF.CENTER);
					}

					if(ic_estatus_docto.equals("26")){
						pdfDoc.setLCell(fecha_registro_operacion, "formasmen", ComunesPDF.CENTER);
					}

					if(bMostrarFechaAutorizacionIFInfoDoctos){
						pdfDoc.setLCell(fechaAutorizacionIF, "formasmen", ComunesPDF.CENTER);
					}
                                        // QC-2018 
                                        if(bOperaFactorajeDistribuido==true){
                                            pdfDoc.setLCell(validacionJUR, "formasmen", ComunesPDF.CENTER);
                                        }
				   if ("ADMIN PEMEX AMP".equals(strPerfilCons)  ) {
						pdfDoc.setLCell(contratoArch, "formasmen", ComunesPDF.CENTER);
					  pdfDoc.setLCell(copadeArch, "formasmen", ComunesPDF.CENTER);
					}
					
					if (tam > 0){
						pdfDoc.setLCell("", "formasmen", ComunesPDF.CENTER,tam);
					}
					nRegistros++;
					countReg++;
				} //FIN DEL WHILE
				try{
					/***** OBTENGO LOS TOTALES *****/
					CQueryHelperExtJS queryHelper = new CQueryHelperExtJS(new ConsDoctosDE());
					List vecTotales = queryHelper.getResultCount(request);
					if(vecTotales.size()>0){
						int i = 0;
						int col=0;
						List vecColumnas = new ArrayList();
						for(i=0;i<vecTotales.size();i++){
							vecColumnas = (List)vecTotales.get(i);
							//vecColumnas = (Vector)vecTotales.get(i);
							String tipoDoc = vecColumnas.get(4).toString();
							String descTipoDoc = " Documentos ";
							if(bOperaFactorajeNotaDeCredito) {
								String moneda = tipoDoc.substring(2,tipoDoc.length());
								BigDecimal monto = new BigDecimal(vecColumnas.get(1).toString());
								if(tipoDoc.length()>2)
									tipoDoc = vecColumnas.get(4).toString().substring(0,2);
								if("NC".equals(tipoDoc)) {
									descTipoDoc = " Notas de Credito ";
									if("1".equals(moneda))
										totalNCNegMN=monto;
									else if("54".equals(moneda))
										totalNCNegUSD=monto;
								} else if("DN".equals(tipoDoc)) {
									descTipoDoc = " Documentos Negociables ";
									if("1".equals(moneda))
										totalDocsNegMN=monto;
									else if("54".equals(moneda))
										totalDocsNegUSD=monto;
								} else {
									if("1".equals(moneda))
										totalDocsNegMN=monto;
									else if("54".equals(moneda))
										totalDocsNegUSD=monto;
								}
							}
							if(!"".equals(sFechaVencPyme)) {
								col=8;
							}else{
								col=7;
							}
							if(bOperaFactorajeVencido||bOperaFactorajeDistribuido||bOperaFactorajeNotaDeCredito || bOperaFactorajeVencInfonavit) { col=col+1;}
							pdfDoc.setLCell("Monto Total" + descTipoDoc + vecColumnas.get(3).toString(), "formasmen", ComunesPDF.RIGHT, col);
							pdfDoc.setLCell("$ "+Comunes.formatoDecimal(vecColumnas.get(1).toString(),2), "formasmen", ComunesPDF.LEFT,2);
							pdfDoc.setLCell("$ "+Comunes.formatoDecimal(vecColumnas.get(2).toString(),2), "formasmen", ComunesPDF.LEFT,  totCol - (col+2)  );
							pdfDoc.setLCell("Total" + descTipoDoc + vecColumnas.get(3).toString(), "formasmen", ComunesPDF.RIGHT, col);
							pdfDoc.setLCell(vecColumnas.get(0).toString(), "formasmen", ComunesPDF.LEFT, totCol - col);
						}
						if(bOperaFactorajeNotaDeCredito && muestraNegociables) {
							saldoMN = saldoMN.add(totalDocsNegMN);
							saldoUSD = saldoUSD.add(totalDocsNegUSD);
							saldoMN = saldoMN.subtract(totalNCNegMN);
							saldoUSD = saldoUSD.subtract(totalNCNegUSD);
							if(totalDocsNegMN.floatValue()>0) {
								pdfDoc.setLCell("Total Saldo Moneda Nacional Documentos Negociables", "formasmen", ComunesPDF.RIGHT,col);
								pdfDoc.setLCell("$ "+Comunes.formatoDecimal(saldoMN.toPlainString(),2), "formasmen", ComunesPDF.LEFT,2);
								pdfDoc.setLCell("", "formasmen", ComunesPDF.LEFT, totCol - (col+2) );
							}
							if(totalDocsNegUSD.floatValue()>0) {
								pdfDoc.setLCell("Total Saldo Dolares Documentos Negociables", "formasmen", ComunesPDF.RIGHT, col);
								pdfDoc.setLCell("$ "+Comunes.formatoDecimal(saldoUSD.toPlainString(),2), "formasmen", ComunesPDF.LEFT,2);
								pdfDoc.setLCell("", "formasmen", ComunesPDF.LEFT, totCol - (col+2) );
							}
						}
					}
				} catch (Exception e){
					log.error("No pude obtener los totales");
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}
		} catch (Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}
/*******
 *******/ 
	public String getNumeroSIAFF(String ic_epo,String ic_documento){
		return getNumeroDeCuatroDigitos(ic_epo) + getNumeroDeOnceDigitos(ic_documento);
	}

	public String getNumeroDeCuatroDigitos(String numero){
		String       claveEPO  = (numero == null )?"":numero.trim();
		StringBuffer resultado = new StringBuffer();
		for(int i = 0;i<(4-claveEPO.length());i++){
			resultado.append("0");
		}
		resultado.append(claveEPO);
		return resultado.toString();
	}

	public String getNumeroDeOnceDigitos(String numero){
		String       claveDocto = (numero == null )?"":numero.trim();
		StringBuffer resultado  = new StringBuffer();
		for(int i = 0;i<(11-claveDocto.length());i++){
			resultado.append("0");
		}
		resultado.append(claveDocto);
		return resultado.toString();
	}

/*******************************************************************************
 *                       GETTERS AND SETTERS                                   *
 *******************************************************************************/
	public List getConditions() {
		return conditions;
	}

	public int getCountReg() {
		return countReg;
	}

	public String getCg_pyme_epo_interno() {
		return cg_pyme_epo_interno;
	}

	public void setCg_pyme_epo_interno(String cg_pyme_epo_interno) {
		this.cg_pyme_epo_interno = cg_pyme_epo_interno;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_estatus_docto() {
		return ic_estatus_docto;
	}

	public void setIc_estatus_docto(String ic_estatus_docto) {
		this.ic_estatus_docto = ic_estatus_docto;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getCc_acuse() {
		return cc_acuse;
	}

	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}

	public String getNo_docto() {
		return no_docto;
	}

	public void setNo_docto(String no_docto) {
		this.no_docto = no_docto;
	}

	public String getDf_fecha_docto_de() {
		return df_fecha_docto_de;
	}

	public void setDf_fecha_docto_de(String df_fecha_docto_de) {
		this.df_fecha_docto_de = df_fecha_docto_de;
	}

	public String getDf_fecha_docto_a() {
		return df_fecha_docto_a;
	}

	public void setDf_fecha_docto_a(String df_fecha_docto_a) {
		this.df_fecha_docto_a = df_fecha_docto_a;
	}

	public String getFn_monto_de() {
		return fn_monto_de;
	}

	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}

	public String getFn_monto_a() {
		return fn_monto_a;
	}

	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}

	public String getDf_fecha_venc_de() {
		return df_fecha_venc_de;
	}

	public void setDf_fecha_venc_de(String df_fecha_venc_de) {
		this.df_fecha_venc_de = df_fecha_venc_de;
	}

	public String getDf_fecha_venc_a() {
		return df_fecha_venc_a;
	}

	public void setDf_fecha_venc_a(String df_fecha_venc_a) {
		this.df_fecha_venc_a = df_fecha_venc_a;
	}

	public String getOrd_proveedor() {
		return ord_proveedor;
	}

	public void setOrd_proveedor(String ord_proveedor) {
		this.ord_proveedor = ord_proveedor;
	}

	public String getOrd_if() {
		return ord_if;
	}

	public void setOrd_if(String ord_if) {
		this.ord_if = ord_if;
	}

	public String getOrd_monto() {
		return ord_monto;
	}

	public void setOrd_monto(String ord_monto) {
		this.ord_monto = ord_monto;
	}

	public String getOrd_fec_venc() {
		return ord_fec_venc;
	}

	public void setOrd_fec_venc(String ord_fec_venc) {
		this.ord_fec_venc = ord_fec_venc;
	}

	public String getOrd_estatus() {
		return ord_estatus;
	}

	public void setOrd_estatus(String ord_estatus) {
		this.ord_estatus = ord_estatus;
	}

	public String getCamp_dina1() {
		return camp_dina1;
	}

	public void setCamp_dina1(String camp_dina1) {
		this.camp_dina1 = camp_dina1;
	}

	public String getCamp_dina2() {
		return camp_dina2;
	}

	public void setCamp_dina2(String camp_dina2) {
		this.camp_dina2 = camp_dina2;
	}

	public String getOperaFirmaM() {
		return operaFirmaM;
	}

	public void setOperaFirmaM(String operaFirmaM) {
		this.operaFirmaM = operaFirmaM;
	}

	public String getSFechaVencPyme() {
		return sFechaVencPyme;
	}

	public void setSFechaVencPyme(String sFechaVencPyme) {
		this.sFechaVencPyme = sFechaVencPyme;
	}

	public String getNumero_siaff() {
		return numero_siaff;
	}

	public void setNumero_siaff(String numero_siaff) {
		this.numero_siaff = numero_siaff;
	}

	public String getIc_documento() {
		return ic_documento;
	}

	public void setIc_documento(String ic_documento) {
		this.ic_documento = ic_documento;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getStrAforo() {
		return strAforo;
	}

	public void setStrAforo(String strAforo) {
		this.strAforo = strAforo;
	}

	public String getStrAforoDL() {
		return strAforoDL;
	}

	public void setStrAforoDL(String strAforoDL) {
		this.strAforoDL = strAforoDL;
	}

	public String getSesIdiomaUsuario() {
		return sesIdiomaUsuario;
	}

	public void setSesIdiomaUsuario(String sesIdiomaUsuario) {
		this.sesIdiomaUsuario = sesIdiomaUsuario;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getStrPerfilCons() {
		return strPerfilCons;
	}

	public void setStrPerfilCons(String strPerfilCons) {
		this.strPerfilCons = strPerfilCons;
	}

	public String getINoCliente() {
		return iNoCliente;
	}

	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}

	public String getOperado() {
		return operado;
	}

	public void setOperado(String operado) {
		this.operado = operado;
	}

	public String getBanderaTablaDoctos() {
		return banderaTablaDoctos;
	}

	public void setBanderaTablaDoctos(String banderaTablaDoctos) {
		this.banderaTablaDoctos = banderaTablaDoctos;
	}


	public void setCmbFactoraje(String cmbFactoraje) {
	    this.cmbFactoraje = cmbFactoraje;
	}
    
	public String getCmbFactoraje() {
	    return cmbFactoraje;
	}
    
	public void setValidaDoctoEPO(String validaDoctoEPO) {
	    this.validaDoctoEPO = validaDoctoEPO;
	}
    
	public String getValidaDoctoEPO() {
	    return validaDoctoEPO; 
	}


    public void setBOperaFactorajeDistribuido(boolean bOperaFactorajeDistribuido) {
        this.bOperaFactorajeDistribuido = bOperaFactorajeDistribuido;
    }
    
    public boolean getBOperaFactorajeDistribuido() {
        return bOperaFactorajeDistribuido; 
    }

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public String getContrato() {
		return contrato;
	}

	public void setCopade(String copade) {
		this.copade = copade;
	}

	public String getCopade() {
		return copade;
	}

}
