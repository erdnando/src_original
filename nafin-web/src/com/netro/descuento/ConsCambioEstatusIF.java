package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsCambioEstatusIF implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsCambioEstatusIF.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String claveIF;
	private String bTipoFactoraje;
	private String bOperaFactorajeDist;
	private String bOperaFactVencimientoInfonavit;
	private String sOperaFactConMandato;
	private String ic_cambio_estatus;
   private String bOperaFactAutomatico;
	 
	public ConsCambioEstatusIF() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		if(ic_cambio_estatus.equals("2")) {
			qrySentencia.append(" SELECT  "+
				//"	(DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social) AS NOMBRE_PYME,"   + //Linea a modificar por FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', i2.cg_razon_social, 'N', (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)) AS NOMBRE_PYME,"+ //FODEA 17
				"        (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social) AS NOMBRE_EPO ,"   +
				"        d.ig_numero_docto AS NUM_DOCTO , d.cc_acuse  AS NUM_ACUSE_IF  , m.cd_nombre AS MONEDA , d.fn_monto AS MONTO_DOCTO , "+
				"			d.fn_porc_anticipo AS POR_DESC ,"   +
				"        d.fn_monto - d.fn_monto_dscto As REC_GARANTIA,  "+
				"        d.fn_monto_dscto AS MONTO_DESCONTAR  , "+
				//"ds.in_tasa_aceptada AS TASA , "+ //Linea a modificar por FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada) AS TASA , "+ //FODEA 17
				//"ds.in_importe_interes AS MONTO_INT , "+ //Linea a modificar por FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', ds.in_importe_interes_fondeo, 'N', ds.in_importe_interes) AS MONTO_INT , "	+ //FODEA 17
				//"ds.in_importe_recibir AS MONTO_OPERAR ," + //Linea a modificar por FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir ) AS MONTO_OPERAR ,"+ //FODEA 17
				"        m.ic_moneda as IC_MONEDA, d.cs_dscto_especial,"   +
				"        DECODE (d.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS BANCO_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS SUCURSAL_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS CUENTA_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, '') AS PORC_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, '') AS IMP_RECIBIR,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ds.in_importe_recibir - ds.fn_importe_recibir_benef, 'I', ds.in_importe_recibir - ds.fn_importe_recibir_benef, '')  AS NETO_RECIBIR ,"   +
				"		   tf.cg_nombre AS TIPO_FACTORAJE, "+
				"			'' as NUM_SOLICITUD,   "+
				"		decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				"   FROM comcat_pyme p,"   +
				"        comcat_epo e,"   +
				"        com_documento d,"   +
				"        comcat_moneda m,"   +
				"        comhis_cambio_estatus ce,"   +
				"        com_docto_seleccionado ds,"   +
				"        comrel_if_epo ie,"   +
				"        comcat_if i, comcat_if i2, comrel_producto_epo pe,"   +
				"		   comcat_tipo_factoraje tf"+
				"  WHERE d.ic_pyme = p.ic_pyme"   +
				"    AND d.cs_dscto_especial != 'C'"   +
				"    AND d.ic_epo = e.ic_epo"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = 1 "   +			
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_documento = ce.ic_documento"   +
				"    AND ce.ic_cambio_estatus = 2"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND d.ic_if = ?" +
				"    AND TO_CHAR (ce.dc_fecha_cambio, 'DD/MM/YYYY') = TO_CHAR (SYSDATE, 'DD/MM/YYYY')"   +
				"    AND d.ic_epo = ie.ic_epo (+)"   +
				"    AND d.ic_beneficiario = ie.ic_if (+)"   +
				"	  AND d.cs_dscto_especial = tf.cc_tipo_factoraje"+
				"	  AND d.ic_if = i2.ic_if (+) "+ //FODEA 17
				"    AND ie.ic_if = i.ic_if (+)") ;
				conditions.add(claveIF);	
				
		} else if(ic_cambio_estatus.equals("7")) {
			
			qrySentencia.append(" SELECT (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) AS NOMBRE_EPO , "+
  			  //"(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) AS NOMBRE_PYME , " + Linea a modificar por FODEA 17
			  "	decode(DS.cs_opera_fiso, 'S', i2.cg_razon_social, 'N', (DECODE (P.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || P.cg_razon_social)) AS NOMBRE_PYME,"+ //FODEA 17
           " substr(S.ic_folio,1,10)||'-'||substr(S.ic_folio,11,1) as NUM_SOLICITUD , " +
           " D.ig_numero_docto as NUM_DOCTO , M.cd_nombre as MONEDA , D.fn_monto as MONTO_DOCTO , "+
			  " D.fn_porc_anticipo as POR_DESC , D.fn_monto_dscto as MONTO_DESCONTAR , " +
			  " d.fn_monto - d.fn_monto_dscto As REC_GARANTIA,  "+ 
           //" DS.in_tasa_aceptada as TASA , "+ linea a modificar por FODEA 17
			  "	decode(DS.cs_opera_fiso, 'S', DS.in_tasa_aceptada_fondeo, 'N', DS.in_tasa_aceptada) AS TASA , "+ //FODEA 17
			  //" DS.in_importe_interes as MONTO_INT , "+ Linea a modificar por FODEA 17
			  "	decode(DS.cs_opera_fiso, 'S', DS.in_importe_interes_fondeo, 'N', DS.in_importe_interes) AS MONTO_INT , "	+ //FODEA 17
			  //"DS.in_importe_recibir as MONTO_OPERAR , " +	Linea a modificar por FODEA 17
			  "	decode(DS.cs_opera_fiso, 'S', DS.in_importe_recibir_fondeo, 'N', DS.in_importe_recibir ) AS MONTO_OPERAR ,"+ //FODEA 17
           " M.ic_moneda as IC_MONEDA, D.CS_DSCTO_ESPECIAL "+
			  " , decode(D.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') as BENEFICIARIO  " +
			  " , decode(D.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') as BANCO_BENEFICIARIO  " +
			  " , decode(D.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') as SUCURSAL_BENEFICIARIO  " +
			  " , decode(D.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') as CUENTA_BENEFICIARIO  " +
			  " , decode(D.cs_dscto_especial, 'D', D.fn_porc_beneficiario, 'I', D.fn_porc_beneficiario, '') as PORC_BENEFICIARIO  " +
			  " , decode(D.cs_dscto_especial, 'D', DS.fn_importe_recibir_benef, 'I', DS.fn_importe_recibir_benef, '') as IMP_RECIBIR  " +
			  " , decode(D.cs_dscto_especial, 'D', DS.in_importe_recibir - (DS.fn_importe_recibir_benef), 'I', DS.in_importe_recibir - (DS.fn_importe_recibir_benef), '') as NETO_RECIBIR " +
			  " , tf.cg_nombre AS TIPO_FACTORAJE"+
			  " , '' as  NUM_ACUSE_IF, "  +
			  "		decode(DS.cs_opera_fiso, 'S', P.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
           " FROM comcat_pyme P, comcat_epo E, com_documento D, comcat_moneda M, "+
           " comhis_cambio_estatus CE, com_docto_seleccionado DS, com_solicitud S, comcat_tipo_factoraje tf " +
			  " ,comrel_if_epo ie " +
			  " ,comcat_if i,comcat_if i2, comrel_producto_epo pe " +
			  " WHERE D.ic_epo = E.ic_epo "+
			  " AND d.ic_epo = pe.ic_epo"   +
			  " AND pe.ic_producto_nafin = 1 "   +
			  " AND d.cs_dscto_especial = tf.cc_tipo_factoraje "+
			  " and D.ic_pyme = P.ic_pyme " +
           " and D.ic_moneda = M.ic_moneda "+
			  " and D.ic_documento = CE.ic_documento " +
           " and CE.ic_cambio_estatus = 7 "+
			  " and D.ic_documento = DS.ic_documento " +
           " and DS.ic_documento = S.ic_documento "+
			  " and D.ic_if =  ?"  +
           " and TO_CHAR(CE.dc_fecha_cambio,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')"+
			  " AND D.ic_epo = ie.ic_epo (+) " +
   		  " AND D.ic_beneficiario = ie.ic_if (+) " +
			  " AND D.ic_if = i2.ic_if (+) "+ //FODEA 17
			  " AND ie.ic_if = i.ic_if (+) ");
			  conditions.add(claveIF);	
			  
		} else if(ic_cambio_estatus.equals("26")) {
		
			qrySentencia.append(" SELECT  d.ic_documento ,  "+ 
				" (DECODE (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social)  ,"   +
				" (DECODE (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) AS NOMBRE_EPO ,"   +
				" D.ig_numero_docto AS NUM_DOCTO , D.cc_acuse, M.cd_nombre AS MONEDA , D.fn_monto AS MONTO_DOCTO , D.fn_porc_anticipo AS POR_DESC,"   +
				" D.fn_monto_dscto AS MONTO_DESCONTAR , M.ic_moneda AS IC_MONEDA , D.CS_DSCTO_ESPECIAL"   +
				"  , DECODE(D.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS BENEF"   +
				"  , DECODE(D.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS BANCO_BENEF"   +
				"  , DECODE(D.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS SUCURSAL_BENEF"   +
				"  , DECODE(D.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS CTA_BENEF"   +
				"  , DECODE(D.cs_dscto_especial, 'D', D.fn_porc_beneficiario, 'I', D.fn_porc_beneficiario, '') AS PORC_BENEF"   +
				"  , TO_CHAR(D.DF_FECHA_DOCTO,'dd/mm/yyyy') AS FECHA_EMISION, TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') AS FECHA_VENCIMIENTO, "   +
				"  '0' AS MONTO_INT, " +
				"  '0' AS MONTO_OPERAR " +
				//"	,	decode(ds.cs_opera_fiso, 'S', P.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				" FROM comcat_pyme P, comcat_epo E, com_documento D, comcat_moneda M,"   +
				" comhis_cambio_estatus CE"   +
				" ,comrel_if_epo ie"   +
				" ,comcat_if i, comrel_producto_epo pe "   +
				" WHERE D.ic_pyme = P.ic_pyme"   +
				"  AND d.cs_dscto_especial != 'C'"   +
				"  AND D.ic_epo = E.ic_epo"   +
				"  AND d.ic_epo = pe.ic_epo"   +
				"  AND pe.ic_producto_nafin = 1 "   +				
				"  AND D.ic_moneda = M.ic_moneda"   +
				"  AND D.ic_documento = CE.ic_documento"   +
				"  AND CE.ic_cambio_estatus = 26"   +
				"  AND D.ic_if =  ? "+  
				"  AND TO_CHAR(CE.dc_fecha_cambio,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')"   +
				"  AND D.ic_epo = ie.ic_epo (+)"   +
				"  AND D.ic_beneficiario = ie.ic_if (+)"   +
				"  AND ie.ic_if = i.ic_if (+) ")  ;
				 conditions.add(claveIF);	
				 
		} else if(ic_cambio_estatus.equals("32")) {
			
			qrySentencia.append(" SELECT (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) as NOMBRE_EPO , "+
				//" (decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) AS NOMBRE_PYME, " + Linea a modificar por FODEA 17
				"	decode(DS.cs_opera_fiso, 'S', i2.cg_razon_social, 'N', (DECODE (P.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || P.cg_razon_social)) AS NOMBRE_PYME,"+ //FODEA 17
            " D.ig_numero_docto AS NUM_DOCTO , M.cd_nombre AS MONEDA , D.fn_monto AS MONTO_DOCTO , D.fn_porc_anticipo AS POR_DESC , D.fn_monto_dscto AS MONTO_DESCONTAR , " +
				" D.fn_monto -  D.fn_monto_dscto AS REC_GARANTIA ,  "+
            //" DS.in_tasa_aceptada as TASA , "+	Linea a modificar por FODEA 17
				"	decode(DS.cs_opera_fiso, 'S', DS.in_tasa_aceptada_fondeo, 'N', DS.in_tasa_aceptada) AS TASA , "+ //FODEA 17
				//"DS.in_importe_interes as MONTO_INT,"+ Linea a modificar FODEA 17
				"	decode(DS.cs_opera_fiso, 'S', DS.in_importe_interes_fondeo, 'N', DS.in_importe_interes) AS MONTO_INT , "	+ //FODEA 17
				" DS.in_importe_recibir, " +
            " M.ic_moneda, D.CS_DSCTO_ESPECIAL "+
	   		" , decode(D.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') as BENEFICIARIO  " +
		  		" , decode(D.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') as BANCO_BENEFICIARIO  " +
			  	" , decode(D.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') as SUCURSAL_BENEFICIARIO  " +
				" , decode(D.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') as CUENTA_BENEFICIARIO  " +
				" , decode(D.cs_dscto_especial, 'D', D.fn_porc_beneficiario, 'I', D.fn_porc_beneficiario, '') as PORC_BENEFICIARIO  " +
				" , decode(D.cs_dscto_especial, 'D', DS.fn_importe_recibir_benef, 'I', DS.fn_importe_recibir_benef, '') as IMP_RECIBIR  " +
				" , decode(D.cs_dscto_especial, 'D', DS.in_importe_recibir - (DS.fn_importe_recibir_benef), 'I', DS.in_importe_recibir - (DS.fn_importe_recibir_benef), '') as NETO_RECIBIR " +
				" , tf.cg_nombre AS TIPO_FACTORAJE,  "+
				" '0' as MONTO_OPERAR  , "+
				"  '' as NUM_ACUSE_IF ,  "+
				"	'' as NUM_SOLICITUD, "+
				"		decode(DS.cs_opera_fiso, 'S', P.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
            " FROM comcat_pyme P, comcat_epo E, com_documento D, comcat_moneda M, "+
            " comhis_cambio_estatus CE, com_docto_seleccionado DS, "+
				" comcat_tipo_factoraje tf " +
				" ,comrel_if_epo ie " +
				" ,comcat_if i, comcat_if i2, comrel_producto_epo pe " +
            " WHERE D.ic_epo = E.ic_epo "+
      		" AND d.ic_epo = pe.ic_epo"   +
			   " AND pe.ic_producto_nafin = 1 "   +
    			" AND d.cs_dscto_especial != 'C' "+
		    	" AND d.cs_dscto_especial = tf.cc_tipo_factoraje "+
			   " and D.ic_pyme = P.ic_pyme " +
            " and D.ic_moneda = M.ic_moneda "+
			   " and D.ic_documento = CE.ic_documento " +
            " and CE.ic_cambio_estatus = 32 "+
			   " and D.ic_documento = DS.ic_documento " +
			   " and D.ic_if = ? "  +
            " and TO_CHAR(CE.dc_fecha_cambio,'DD/MM/YYYY') = TO_CHAR(SYSDATE,'DD/MM/YYYY')"+
				" AND D.ic_epo = ie.ic_epo (+) " +
				" AND D.ic_beneficiario = ie.ic_if (+) " +
				"  AND D.ic_if = i2.ic_if (+) "+ //FODEA 17
				" AND ie.ic_if = i.ic_if (+) ");					  
				conditions.add(claveIF);		
		}
		
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
	
		String 	beneficiario ="", banco_beneficiario ="", sucursal_beneficiario ="",  cuenta_beneficiario ="", 
				por_beneficiario  ="", imp_recibir ="", neto_recibir ="",nombre_epo="",  num_docto ="",ic_moneda ="",
				nombre_pyme ="", acuse_if ="", moneda ="", tipo_factoraje ="", monto_desc ="", porc_desc ="", 
				rec_garantia ="",  tasa="", monto_int ="", monto_oper ="", monto_docto  ="", num_solicitud ="",
				fecha_emision ="",  fecha_vencimiento ="", referencia_pyme ="";
				
			double nacional = 0, nacional_desc = 0, nacional_int = 0, nacional_descIF = 0, dolares = 0, dolares_desc = 0,  dolares_int = 0,  dolares_descIF = 0;
			int  doc_nacional =0,doc_dolares =0;	
			
		try {
		
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(), 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			}
				
			if(ic_cambio_estatus.equals("2")  &&  tipo.equals("PDF")  ) {
				
				pdfDoc.addText("  " ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("Estatus: Seleccionada PYME a Negociable" ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("  " ,"formas",ComunesPDF.LEFT);
				
				pdfDoc.setTable(15,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Nombre PYME/Cedente","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Acuse IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje.equals("S")) {
					pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				}		
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER); //Fodea 17
				
				if(bTipoFactoraje.equals("N")) {
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				}
				       
				if(bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ) {
					pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Cuenta Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 7); 
				}
			}
				
			if(ic_cambio_estatus.equals("7")  &&  tipo.equals("PDF")  ) {
				
				pdfDoc.addText("  " ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("Estatus: Operada - Operada Pendiente de Pago" ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("  " ,"formas",ComunesPDF.LEFT);
				
				pdfDoc.setTable(15,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME/Cedente","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("N�mero Solicitud ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje.equals("S")) {
					pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				}		
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER); //Fodea 17
				if(bTipoFactoraje.equals("N")) {
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				}
				
				if(bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ) {
					pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Cuenta Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 7); 
				}
			}
			
			
			if(ic_cambio_estatus.equals("26")  &&  tipo.equals("PDF")  ) {
				
				pdfDoc.addText("  " ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("Estatus: Pignorado a Negociable" ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("  " ,"formas",ComunesPDF.LEFT);
				
				pdfDoc.setTable(9,100); //era 8,100
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Emision","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);		
				pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER); //Fodea 17
			}			
			
			if(ic_cambio_estatus.equals("32")  &&  tipo.equals("PDF")  ) {
				
				pdfDoc.addText("  " ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("Estatus: Programado Pyme a Seleccionado Pyme " ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("  " ,"formas",ComunesPDF.LEFT);				
				pdfDoc.setTable(13,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME/Cedente","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje.equals("S")) {
					pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				}		
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER); //Fodea 17
				if(bTipoFactoraje.equals("N")) {
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				}				
				if(bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ) {
					pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Cuenta Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 5);				
				}
			}
						
			
			if(ic_cambio_estatus.equals("2") ||  ic_cambio_estatus.equals("7")   ||  ic_cambio_estatus.equals("32")) {
				 
				while (rs.next())	{		
					nombre_pyme = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
					nombre_epo = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					num_docto = (rs.getString("NUM_DOCTO") == null) ? "" : rs.getString("NUM_DOCTO");
					acuse_if = (rs.getString("NUM_ACUSE_IF") == null) ? "" : rs.getString("NUM_ACUSE_IF");
					moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					tipo_factoraje = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
					monto_docto = (rs.getString("MONTO_DOCTO") == null) ? "0" : rs.getString("MONTO_DOCTO");
					porc_desc = (rs.getString("POR_DESC") == null) ? "" : rs.getString("POR_DESC");
					rec_garantia = (rs.getString("REC_GARANTIA") == null) ? "" : rs.getString("REC_GARANTIA");
					monto_desc = (rs.getString("MONTO_DESCONTAR") == null) ? "0" : rs.getString("MONTO_DESCONTAR");
					tasa = (rs.getString("TASA") == null) ? "" : rs.getString("TASA");
					monto_int =  (rs.getString("MONTO_INT") == null) ? "0" : rs.getString("MONTO_INT");
					monto_oper = (rs.getString("MONTO_OPERAR") == null) ? "0" : rs.getString("MONTO_OPERAR");
					beneficiario = (rs.getString("BENEFICIARIO") == null) ? "" : rs.getString("BENEFICIARIO");
					banco_beneficiario = (rs.getString("BANCO_BENEFICIARIO") == null) ? "" : rs.getString("BANCO_BENEFICIARIO");
					sucursal_beneficiario = (rs.getString("SUCURSAL_BENEFICIARIO") == null) ? "" : rs.getString("SUCURSAL_BENEFICIARIO");
					cuenta_beneficiario = (rs.getString("CUENTA_BENEFICIARIO") == null) ? "" : rs.getString("CUENTA_BENEFICIARIO");
					por_beneficiario = (rs.getString("PORC_BENEFICIARIO") == null) ? "" : rs.getString("PORC_BENEFICIARIO");
					imp_recibir = (rs.getString("IMP_RECIBIR") == null) ? "0" : rs.getString("IMP_RECIBIR");
					neto_recibir = (rs.getString("NETO_RECIBIR") == null) ? "0" : rs.getString("NETO_RECIBIR");
					ic_moneda = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");
					num_solicitud = (rs.getString("NUM_SOLICITUD") == null) ? "" : rs.getString("NUM_SOLICITUD");
					referencia_pyme = (rs.getString("REFERENCIA_PYME") == null) ? "" : rs.getString("REFERENCIA_PYME");
					
					if (ic_moneda.equals("1")) {	
						nacional+=Double.parseDouble((String)monto_docto); 
						nacional_desc+=Double.parseDouble((String)monto_desc); 
						nacional_int+=Double.parseDouble((String)monto_int); 
						nacional_descIF+= Double.parseDouble((String)monto_oper); 
						doc_nacional++;
					}
	
					if (ic_moneda.equals("54")) {						 
						dolares+=Double.parseDouble((String)monto_docto); 
						dolares_desc+=Double.parseDouble((String)monto_desc); 
						dolares_int+=Double.parseDouble((String)monto_int); 
						dolares_descIF+= Double.parseDouble((String)monto_oper); 	
						doc_dolares++;
					}
					
					if(ic_cambio_estatus.equals("2")  &&  tipo.equals("PDF")  ) {
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);							
						pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(acuse_if,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						if(bTipoFactoraje.equals("S")) {
							pdfDoc.setCell(tipo_factoraje,"formas",ComunesPDF.CENTER);
						}		
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_docto,2),"formas",ComunesPDF.RIGHT);				
						pdfDoc.setCell(porc_desc+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(rec_garantia,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_int,2),"formas",ComunesPDF.RIGHT);				
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_oper,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER);	//FODEA 17
						if(bTipoFactoraje.equals("N")) {
							pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
						}
					
						if(bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ) {
							pdfDoc.setCell("B","formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(banco_beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(por_beneficiario+"%","formas",ComunesPDF.CENTER);	
							pdfDoc.setCell("$"+Comunes.formatoDecimal(imp_recibir,2),"formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell("$"+Comunes.formatoDecimal(neto_recibir,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 7); 
						}		
					}
					
					
					if(ic_cambio_estatus.equals("7")  &&  tipo.equals("PDF")  ) {
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);						
						pdfDoc.setCell(num_solicitud,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);						
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						if(bTipoFactoraje.equals("S")) {
							pdfDoc.setCell(tipo_factoraje,"formas",ComunesPDF.CENTER);
						}		
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_docto,2),"formas",ComunesPDF.RIGHT);				
						pdfDoc.setCell(porc_desc+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(rec_garantia,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_int,2),"formas",ComunesPDF.RIGHT);				
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_oper,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA17
						if(bTipoFactoraje.equals("N")) {
							pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
						}
					
						if(bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ) {
							pdfDoc.setCell("B","formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(banco_beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(por_beneficiario+"%","formas",ComunesPDF.CENTER);	
							pdfDoc.setCell("$"+Comunes.formatoDecimal(imp_recibir,2),"formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell("$"+Comunes.formatoDecimal(neto_recibir,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 7); 
						}								
					}
					
					if(ic_cambio_estatus.equals("32")  &&  tipo.equals("PDF")  ) {
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);						
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);						
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						if(bTipoFactoraje.equals("S")) {
							pdfDoc.setCell(tipo_factoraje,"formas",ComunesPDF.CENTER);
						}		
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_docto,2),"formas",ComunesPDF.RIGHT);				
						pdfDoc.setCell(porc_desc+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(rec_garantia,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_int,2),"formas",ComunesPDF.RIGHT);	
						pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
						if(bTipoFactoraje.equals("N")) {
							pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
						}
					
						if(bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ) {
							pdfDoc.setCell("B","formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(banco_beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(por_beneficiario+"%","formas",ComunesPDF.CENTER);	
							pdfDoc.setCell("$"+Comunes.formatoDecimal(imp_recibir,2),"formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell("$"+Comunes.formatoDecimal(neto_recibir,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 5); 
						}								
					}
					
					
				} //while 
			
				if(tipo.equals("PDF")   &&  (   ic_cambio_estatus.equals("2") ||   ic_cambio_estatus.equals("7"))   ) {				
					if (doc_nacional>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						if(bTipoFactoraje.equals("N")) { pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3); }
						if(bTipoFactoraje.equals("S")) { pdfDoc.setCell("","formas",ComunesPDF.CENTER, 4); }
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_descIF), 2),"formas",ComunesPDF.RIGHT);
						if(bTipoFactoraje.equals("N")) { 	pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);	}
					}
					if (doc_dolares>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
						if(bTipoFactoraje.equals("N")) { pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3); }
						if(bTipoFactoraje.equals("S")) { pdfDoc.setCell("","formas",ComunesPDF.CENTER, 4); }
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_descIF), 2),"formas",ComunesPDF.RIGHT);
						if(bTipoFactoraje.equals("N")) { 	pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);	}
					}	
				}
				
				
					if(tipo.equals("PDF")   &&  ic_cambio_estatus.equals("32") ) {				
					if (doc_nacional>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						if(bTipoFactoraje.equals("S")) { pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3); }
						if(bTipoFactoraje.equals("N")) { pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2); }
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_int), 2),"formas",ComunesPDF.RIGHT);
						if(bTipoFactoraje.equals("N")) { pdfDoc.setCell("","formas",ComunesPDF.CENTER,2); }
					}
					if (doc_dolares>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
						if(bTipoFactoraje.equals("N")) { pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2); }
						if(bTipoFactoraje.equals("S")) { pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3); }
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);		
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_int), 2),"formas",ComunesPDF.RIGHT);
						if(bTipoFactoraje.equals("N")) { pdfDoc.setCell("","formas",ComunesPDF.CENTER,2); }
					}	
				}
			}
					
			
			if(ic_cambio_estatus.equals("26") ) {	
				
				while (rs.next())	{		
					
					nombre_epo = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					num_docto = (rs.getString("NUM_DOCTO") == null) ? "" : rs.getString("NUM_DOCTO");
					fecha_emision = (rs.getString("FECHA_EMISION") == null) ? "" : rs.getString("FECHA_EMISION");
					fecha_vencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					monto_docto = (rs.getString("MONTO_DOCTO") == null) ? "0" : rs.getString("MONTO_DOCTO");
					porc_desc = (rs.getString("POR_DESC") == null) ? "" : rs.getString("POR_DESC");
					monto_desc = (rs.getString("MONTO_DESCONTAR") == null) ? "0" : rs.getString("MONTO_DESCONTAR");
					ic_moneda = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");
				
					if (ic_moneda.equals("1")) {	
						nacional+=Double.parseDouble((String)monto_docto); 
						nacional_desc+=Double.parseDouble((String)monto_desc); 			
						doc_nacional++;
					}	
					if (ic_moneda.equals("54")) {						 
						dolares+=Double.parseDouble((String)monto_docto); 
						dolares_desc+=Double.parseDouble((String)monto_desc); 						
						doc_dolares++;
					}
					
					pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_emision,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_vencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_docto,2),"formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell(porc_desc+"%","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);						
					
				}// while (rs.next())	
				
				if(tipo.equals("PDF")   &&  ic_cambio_estatus.equals("26") ) {				
					if (doc_nacional>0){ 						
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_desc), 2),"formas",ComunesPDF.RIGHT);
					}
					if (doc_dolares>0){ 						
						pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_desc), 2),"formas",ComunesPDF.RIGHT);					
					}	
				}
			}
			
			if(tipo.equals("PDF") ) {
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			}
			if(tipo.equals("CSV") ) {
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}




	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	public String getBTipoFactoraje() {
		return bTipoFactoraje;
	}

	public void setBTipoFactoraje(String bTipoFactoraje) {
		this.bTipoFactoraje = bTipoFactoraje;
	}

	public String getBOperaFactorajeDist() {
		return bOperaFactorajeDist;
	}

	public void setBOperaFactorajeDist(String bOperaFactorajeDist) {
		this.bOperaFactorajeDist = bOperaFactorajeDist;
	}

	public String getBOperaFactVencimientoInfonavit() {
		return bOperaFactVencimientoInfonavit;
	}

	public void setBOperaFactVencimientoInfonavit(String bOperaFactVencimientoInfonavit) {
		this.bOperaFactVencimientoInfonavit = bOperaFactVencimientoInfonavit;
	}

	public String getSOperaFactConMandato() {
		return sOperaFactConMandato;
	}
  
	public void setSOperaFactConMandato(String sOperaFactConMandato) {
		this.sOperaFactConMandato = sOperaFactConMandato;
	}

	public String getIc_cambio_estatus() {
		return ic_cambio_estatus;
	}

	public void setIc_cambio_estatus(String ic_cambio_estatus) {
		this.ic_cambio_estatus = ic_cambio_estatus;
	}


   public void setBOperaFactAutomatico(String bOperaFactAutomatico) {
      this.bOperaFactAutomatico = bOperaFactAutomatico;
   }


   public String getBOperaFactAutomatico() {
      return bOperaFactAutomatico;
   }

}