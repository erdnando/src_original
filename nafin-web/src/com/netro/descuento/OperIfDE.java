package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log; //MOD
//MOD

public class OperIfDE implements IQueryGenerator, IQueryGeneratorRegExtJS{

  public OperIfDE()   
  {
  }
	public String getAggregateCalculationQuery(HttpServletRequest request)
  {
    String condicion = "";   
    StringBuffer qrySentencia = new StringBuffer();
    
	String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
	String cliente = (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
	String prestamo = ( request.getParameter("prestamo") == null) ? "" :  request.getParameter("prestamo");
	String FechaCorte = (request.getParameter("FechaCorte") == null) ? "" : request.getParameter("FechaCorte");
//	String FechaInicio = (request.getParameter("FechaInicio") == null) ? "" : request.getParameter("FechaInicio");
//	String FechaFin = (request.getParameter("FechaFin") == null) ? "" : request.getParameter("FechaFin");
	String moneda = (request.getParameter("moneda") == null) ? "" : request.getParameter("moneda");

	
    try{   
			qrySentencia.append(
				"select " +
				"    /*+ " +
				"        INDEX (O IN_COM_OPERADO_01_NUK) " +
				"        USE_NL (O M) " +
				"    */  " +
				"        count(1), " +
				"        sum(fg_montooper), " +
				"        sum(fg_montoprimcuot), " +
				"        sum(fg_netootorgado), " +
				"        M.cd_nombre, " +
				"        O.ic_moneda, " +
				"    	 'OperIfDE::getAggregateCalculationQuery' " +
				"from  " +
				"        COM_OPERADO O, " +
				"        COMCAT_MONEDA M " +
				"where   O.ic_moneda = M.ic_moneda " );
				//"    and O.ic_if = " + ic_if ); 

         if(!ic_if.equals(""))
         	condicion += " and o.ic_if="+ic_if;
         if(!moneda.equals(""))
         	condicion += " and o.ic_moneda="+moneda;
         if(!cliente.equals(""))
         	condicion += " and o.ig_cliente="+cliente;
         if(!prestamo.equals(""))
         	condicion += " and o.ig_prestamo="+prestamo;
          if(!FechaCorte.equals(""))
            condicion += " AND DF_FECHAOPERACION >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAOPERACION < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
 	
         	
         	
         /*if(!FechaInicio.equals(""))
            condicion += " and df_fechaoperacion between to_date('"+FechaInicio+"', 'dd/mm/yyyy') " +
               " and to_date('"+ FechaFin +"', 'dd/mm/yyyy') ";
         else
            condicion += " and to_char(df_fechaoperacion, 'dd/mm/yyyy') = to_char(sysdate, 'dd/mm/yyyy')";*/
 
        qrySentencia.append(" " + condicion);
     	qrySentencia.append(" group by o.ic_moneda,m.cd_nombre");
      	qrySentencia.append(" order by o.ic_moneda");
	       
    	System.out.println("EL query del AggregateCalculationQuery es: "+qrySentencia.toString());
    
    }catch(Exception e){
      System.out.println("OperIfDE::getAggregateCalculationQuery "+e);
    }
    return qrySentencia.toString();
  }

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids)
  {
  	
 	int i=0;
    StringBuffer qrySentencia = new StringBuffer();
 	
		qrySentencia.append(
			"select " +
			"    /*+ " +
			"        INDEX (O CP_COM_OPERADO_PK)" +
			"        INDEX (M CP_COMCAT_MONEDA_PK)" +
			"        USE_NL (O)" +
			"    */ " +
			"        ig_cliente, cg_nombrecliente, ig_prestamo, ig_numelectronico, " +
			"        to_char(df_fechaoperacion, 'dd/mm/yyyy'), to_char(df_fechapripagcap, 'dd/mm/yyyy')," + 
			"        ig_tasa, fg_spread, fg_margen, fg_montooper, fg_montoprimcuot, fg_netootorgado, " +
			"        O.ic_moneda, M.cd_nombre, " +
			"        decode(I.ig_tipo_piso,1,'',2,ic_financiera) as ic_financiera, cg_razon_social, " +
			"        ig_estatus, fg_tipocambio, cg_calificacion, ig_baseoperacion, cg_descbaseop, ig_succliente, " +
			"        ig_proyecto, ig_contrato, ig_disposicion, ig_estado, ig_municipio, ig_subaplicacion," + 
			"        ig_suctram, cg_relmat, cg_acteco, cg_aniomod, ig_estrato, cg_afianz, ig_freccap, " +
			"        ig_frecint, ig_numcoutas, ig_tipoamort, cg_modpago, cg_centrofin, ig_sucinter, " +
			"        fg_montoultcout, fg_montorecal, fg_montoporgar, fg_comnodisp, ig_tipogar, cg_primabruta, " +
			"        fg_intcobradoxanticip," +
			"    	 'OperIfDE::getDocumentSummaryQueryFile' " +
			"from  " +
			"        COM_OPERADO O,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   O.ic_moneda = M.ic_moneda  " +
			"    and O.ic_if = I.ic_if " +
	        "	 and O.ic_operado in ( ");
	
	for (Iterator it = ids.iterator(); it.hasNext();i++){
    	if(i>0)
      		qrySentencia.append(",");
		qrySentencia.append("'"+it.next().toString()+"'");
	}
	
	qrySentencia.append(") " +
		" order by ic_moneda, ig_cliente, to_char(df_fechaoperacion, 'dd/mm/yyyy')");
	
    System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
	
	return qrySentencia.toString();

  }
	
	public String getDocumentQuery(HttpServletRequest request)
	{
    String condicion = "";
    StringBuffer qrySentencia = new StringBuffer();
    
	String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
	String cliente = (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
	String prestamo = ( request.getParameter("prestamo") == null) ? "" :  request.getParameter("prestamo");
	String FechaCorte = (request.getParameter("FechaCorte") == null) ? "" : request.getParameter("FechaCorte");
//	String FechaInicio = (request.getParameter("FechaInicio") == null) ? "" : request.getParameter("FechaInicio");
//	String FechaFin = (request.getParameter("FechaFin") == null) ? "" : request.getParameter("FechaFin");
	String moneda = (request.getParameter("moneda") == null) ? "" : request.getParameter("moneda");

    try{
			qrySentencia.append(
				"select " +
				"    /*+ " +
				"        INDEX (IN_COM_OPERADO_01_NUK) " +
				"    */  " +
				"        ic_operado, " +
				"    	 'OperIfDE::getDocumentQuery' " +
				"from  " +
				"        COM_OPERADO " +
				"WHERE   ic_if = " + ic_if); 

         if(!moneda.equals(""))
         	condicion += " and ic_moneda="+moneda;
         if(!cliente.equals(""))
         	condicion += " and ig_cliente="+cliente;
         if(!prestamo.equals(""))
         	condicion += " and ig_prestamo="+prestamo;
         	
         if(!FechaCorte.equals(""))
            condicion += " AND DF_FECHAOPERACION >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAOPERACION < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
 		
         	
         /*if(!FechaInicio.equals(""))
            condicion += " and df_fechaoperacion between to_date('"+FechaInicio+"', 'dd/mm/yyyy') " +
               " and to_date('"+ FechaFin +"', 'dd/mm/yyyy') ";
         else
            condicion += " and to_char(df_fechaoperacion, 'dd/mm/yyyy') = to_char(sysdate, 'dd/mm/yyyy')";*/
 
	     qrySentencia.append(" " + condicion +
	       	" order by ic_moneda, ig_cliente, to_char(df_fechaoperacion, 'dd/mm/yyyy') ");
				       
    	 System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
    	 
    }catch(Exception e){
      System.out.println("OperIfDE::getDocumentQueryException "+e);
    }
    return qrySentencia.toString();
  }
	
	public String getDocumentQueryFile(HttpServletRequest request)
  {
    /*String condicion = "";
    StringBuffer qrySentencia = new StringBuffer();*/
    
	String ic_if = (request.getParameter("ic_if") == null) ? "" : request.getParameter("ic_if");
	String cliente = (request.getParameter("cliente") == null) ? "" : request.getParameter("cliente");
	String prestamo = ( request.getParameter("prestamo") == null) ? "" :  request.getParameter("prestamo");
	String FechaCorte = (request.getParameter("FechaCorte") == null) ? "" : request.getParameter("FechaCorte");
//	String FechaInicio = (request.getParameter("FechaInicio") == null) ? "" : request.getParameter("FechaInicio");
//	String FechaFin = (request.getParameter("FechaFin") == null) ? "" : request.getParameter("FechaFin");
	String moneda = (request.getParameter("moneda") == null) ? "" : request.getParameter("moneda");
	String qry = "";
	
    try{
    	    ArmaQueryFile queryf = new ArmaQueryFile();
    		qry = queryf.regresaQueryOper(ic_if,cliente,prestamo,FechaCorte,moneda);		
    		
		/*	qrySentencia.append(
        
			"select " +
			"    /*+ " +
			"        INDEX (O IN_COM_OPERADO_01_NUK)" +
			"        USE_NL (O M)" +
			"    */ /*" +
			" IG_ESTATUS	AS	Status              "   +
			" ,to_char(O.DF_FECHAOPERACION,'dd/mm/yyyy')	AS	FechaOperacion      "   +
			" ,O.IC_MONEDA	AS	CodMoneda           "   +
			" ,M.CD_NOMBRE	AS	DescMoneda          "   +
			" ,O.FG_TIPOCAMBIO	AS	TipoCambio          "   +
			" ,O.IC_IF	AS	Intermediario       "   +
			" ,I.CG_RAZON_SOCIAL	AS	NomIntermediario    "   +
			" ,O.CG_CALIFICACION	AS	Calificacion        "   +
			" ,O.IG_BASEOPERACION	AS	BaseOperacion       "   +
			" ,O.CG_DESCBASEOP	AS	DescBaseOp          "   +
			" ,O.IG_CLIENTE	AS	Cliente             "   +
			" ,O.CG_NOMBRECLIENTE	AS	NombreCliente       "   +
			" ,O.IG_SUCCLIENTE	AS	SucCliente          "   +
			" ,O.IG_PROYECTO	AS	Proyecto            "   +
			" ,O.IG_CONTRATO	AS	Contrato            "   +
			" ,O.IG_PRESTAMO	AS	Prestamo            "   +
			" ,O.IG_NUMELECTRONICO	AS	NumElectronico      "   +
			" ,O.IG_DISPOSICION	AS	Disposicion         "   +
			" ,O.IG_ESTADO	AS	Estado              "   +
			" ,O.IG_MUNICIPIO	AS	Municipio           "   +
			" ,O.IG_SUBAPLICACION	AS	SubAplicacion       "   +
			" ,O.IG_SUCTRAM	AS	SucTram             "   +
			" ,to_char(O.DF_FECHAPRIPAGCAP,'dd/mm/yyyy')	AS	FechaPriPagCap      "   +
			" ,O.IG_TASA	AS	Tasa                "   +
			" ,O.CG_RELMAT	AS	RelMat              "   +
			" ,O.FG_SPREAD	AS	Spread              "   +
			" ,O.FG_MARGEN	AS	Margen              "   +
			" ,O.CG_ACTECO	AS	ActEco              "   +
			" ,O.CG_ANIOMOD	AS	AnioMOD            "   +
			" ,O.IG_ESTRATO	AS	Estrato             "   +
			" ,O.CG_AFIANZ	AS	Afianz              "   +
			" ,O.IG_FRECCAP	AS	FrecCap             "   +
			" ,O.IG_FRECINT	AS	FrecInt             "   +
			" ,O.IG_NUMCOUTAS	AS	NumCuotas           "   +
			" ,O.IG_TIPOAMORT	AS	TipoAmort           "   +
			" ,O.CG_MODPAGO	AS	ModPago             "   +
			" ,O.CG_CENTROFIN	AS	CentroFin           "   +
			" ,O.IG_SUCINTER	AS	SucInter            "   +
			" ,O.FG_MONTOOPER	AS	MontoOper           "   +
			" ,O.FG_MONTOPRIMCUOT	AS	MontoPrimCuot       "   +
			" ,O.FG_MONTOULTCOUT	AS	MontoUltCuota       "   +
			" ,O.FG_MONTORECAL	AS	MontoRecal          "   +
			" ,O.FG_MONTOPORGAR	AS	ComisionPorGar      "   +
			" ,O.FG_COMNODISP	AS	ComNoDisp           "   +
			" ,O.FG_INTCOBRADOXANTICIP	AS	IntCobAnt           "   +
			" ,O.IG_TIPOGAR	AS	TipoGar             "   +
			" ,O.FG_NETOOTORGADO	AS	NetoOtorgado        "   +
			" ,O.CG_PRIMABRUTA	AS	PrimaBruta"+
			" ,I.ic_financiera"+
			" ,'OperIfDE::getDocumentQueryFile' " +
			"from  " +
			"        COM_OPERADO O,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   O.ic_moneda = M.ic_moneda  " +
			"    and O.ic_if = I.ic_if " +
			"	 and O.ic_if = " + ic_if);

         if(!moneda.equals(""))
         	condicion += " and o.ic_moneda="+moneda;
         if(!cliente.equals(""))
         	condicion += " and o.ig_cliente="+cliente;
         if(!prestamo.equals(""))
         	condicion += " and o.ig_prestamo="+prestamo;
         if(!FechaCorte.equals(""))
            condicion += " AND DF_FECHAOPERACION >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAOPERACION < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";

         	*/
        /* if(!FechaInicio.equals(""))
            condicion += " and df_fechaoperacion between to_date('"+FechaInicio+"', 'dd/mm/yyyy') " +
               " and to_date('"+ FechaFin +"', 'dd/mm/yyyy') ";
         else
            condicion += " and to_char(df_fechaoperacion, 'dd/mm/yyyy') = to_char(sysdate, 'dd/mm/yyyy')";*/
 
	 /*    qrySentencia.append(" " + condicion +
	       	" order by O.ic_moneda, O.ig_cliente, to_char(df_fechaoperacion, 'dd/mm/yyyy') ");*/

	    System.out.println("EL query de DocumentQueryFile es: "+qry);//qrySentencia.toString());
      
    }catch(Exception e){
      System.out.println("OperIfDE::getDocumentQueryFileException "+e);
    }
    return qry;//qrySentencia.toString();
  }
    /*****************************************************************************
	*										MIGRACION												 *
	*										garellano												 *
	*                         	 22/junio/2012										 		 *  
	****************************************************************************/
	public  StringBuffer strQuery;
	private List conditions = new ArrayList(); 
	private String paramNumDocto;
	private String parametro;
	private String moneda;
	private String cliente;
	private String prestamo;
	private String FechaCorte;
	private String ic_if;
	private Registros vecTotales;
  private String tipoLinea;
	
	
	private final static Log log = ServiceLocator.getInstance().getLog(OperIfDE.class);
	
	/**     
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() { // Obtiene los totales 
		String condicion = "";
		StringBuffer qrySentencia = new StringBuffer();

		try{
			qrySentencia.append(
				"select " +
				"    /*+ " +
				"        INDEX (O IN_COM_OPERADO_01_NUK) " +
				"        USE_NL (O M) " +
				"    */  " +
				"        count(1) as numero_registros, " +
				"        sum(fg_montooper)as MONTO_OPERADO, " +
				"        sum(fg_montoprimcuot)  as monto_primer_cuota , " +
				"        sum(fg_netootorgado) as neto_otorgado, " +
				"        M.cd_nombre as tipo_moneda, " +
				"        O.ic_moneda, " +
				"    	 'OperIfDE::getAggregateCalculationQuery' " +
				"from  " +
				"        COM_OPERADO O, " +
				"        COMCAT_MONEDA M " +
				"where   O.ic_moneda = M.ic_moneda ");// +
				//"    and O.ic_if = " + ic_if ); 

         if(!ic_if.equals(""))
         	condicion += " and o.ic_if="+ic_if;
         if(!moneda.equals(""))
         	condicion += " and o.ic_moneda="+moneda;
         if(!cliente.equals(""))
         	condicion += " and o.ig_cliente="+cliente;
         if(!prestamo.equals(""))
         	condicion += " and o.ig_prestamo="+prestamo;
          if(!FechaCorte.equals(""))
            condicion += " AND DF_FECHAOPERACION >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAOPERACION < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
 	
         	
         	
         /*if(!FechaInicio.equals(""))
            condicion += " and df_fechaoperacion between to_date('"+FechaInicio+"', 'dd/mm/yyyy') " +
               " and to_date('"+ FechaFin +"', 'dd/mm/yyyy') ";
         else
            condicion += " and to_char(df_fechaoperacion, 'dd/mm/yyyy') = to_char(sysdate, 'dd/mm/yyyy')";*/
 
        qrySentencia.append(" " + condicion);
     	qrySentencia.append(" group by o.ic_moneda,m.cd_nombre");
      	qrySentencia.append(" order by o.ic_moneda");
	       
    	System.out.println("EL query del AggregateCalculationQuery es: "+qrySentencia.toString());
    
    }catch(Exception e){
      System.out.println("OperIfDE::getAggregateCalculationQuery "+e);
    }
    return qrySentencia.toString();
 	}  
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */	 
	public String getDocumentQuery(){  // para las llaves primarias		
		String condicion = "";
		StringBuffer qrySentencia = new StringBuffer();

		try{
			qrySentencia.append(
				"select " +
				"    /*+ " +
				"        INDEX (IN_COM_OPERADO_01_NUK) " +
				"    */  " +
				"        ic_operado, " +
				"    	 'OperIfDE::getDocumentQuery' " +
				"from  " +
				"        COM_OPERADO " +
				"WHERE   ic_if = " + ic_if); 

         if(!moneda.equals(""))
         	condicion += " and ic_moneda="+moneda;
         if(!cliente.equals(""))
         	condicion += " and ig_cliente="+cliente;
         if(!prestamo.equals(""))
         	condicion += " and ig_prestamo="+prestamo;
         	
         if(!FechaCorte.equals(""))
            condicion += " AND DF_FECHAOPERACION >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAOPERACION < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";
 		
         	
         /*if(!FechaInicio.equals(""))
            condicion += " and df_fechaoperacion between to_date('"+FechaInicio+"', 'dd/mm/yyyy') " +
               " and to_date('"+ FechaFin +"', 'dd/mm/yyyy') ";
         else
            condicion += " and to_char(df_fechaoperacion, 'dd/mm/yyyy') = to_char(sysdate, 'dd/mm/yyyy')";*/
 
	     qrySentencia.append(" " + condicion +
	       	" order by ic_moneda, ig_cliente, to_char(df_fechaoperacion, 'dd/mm/yyyy') ");
				       
    	 System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
    	 
    }catch(Exception e){
      System.out.println("OperIfDE::getDocumentQueryException "+e);
    }
    return qrySentencia.toString();
 	}  
	   
	/**
	 * Obtiene el query necesario para mostrar la informaci?n completa de 
	 * una p?gina a partir de las llaves primarias enviadas como par?metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci?n
	 * 	completa de los registros con las llaves especificadas
	 */	  		
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 			
		StringBuffer qrySentencia = new StringBuffer();
		
		qrySentencia.append(
			"select " +
			"    /*+ " +
			"        INDEX (O CP_COM_OPERADO_PK)" +
			"        INDEX (M CP_COMCAT_MONEDA_PK)" +
			"        USE_NL (O)" +
			"    */ " +
			"        ig_cliente as cliente, cg_nombrecliente as nombre_cliente, ig_prestamo as prestamo, ig_numelectronico as numero_electronico, " +
			"        to_char(df_fechaoperacion, 'dd/mm/yyyy') as fecha_operacion, to_char(df_fechapripagcap, 'dd/mm/yyyy') as fecha_primer_pago," + 
			"        ig_tasa as tasa, fg_spread as spread, fg_margen as margen, fg_montooper as monto_operado, fg_montoprimcuot as monto_primer_cuota, fg_netootorgado as neto_otorgado, " +
			"        O.ic_moneda, M.cd_nombre, " +
			"        decode(I.ig_tipo_piso,1,'',2,ic_financiera) as ic_financiera, cg_razon_social, " +
			"        ig_estatus, fg_tipocambio, cg_calificacion, ig_baseoperacion, cg_descbaseop, ig_succliente, " +
			"        ig_proyecto, ig_contrato, ig_disposicion, ig_estado, ig_municipio, ig_subaplicacion," + 
			"        ig_suctram, cg_relmat, cg_acteco, cg_aniomod, ig_estrato, cg_afianz, ig_freccap, " +
			"        ig_frecint, ig_numcoutas, ig_tipoamort, cg_modpago, cg_centrofin, ig_sucinter, " +
			"        fg_montoultcout, fg_montorecal, fg_montoporgar, fg_comnodisp, ig_tipogar, cg_primabruta, " +
			"        fg_intcobradoxanticip," +
			"    	 'OperIfDE::getDocumentSummaryQueryFile' " +
			"from  " +
			"        COM_OPERADO O,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   O.ic_moneda = M.ic_moneda  " +
			"    and O.ic_if = I.ic_if " +
	        "	 and O.ic_operado in ( ");
	
	for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0){ qrySentencia.append(","); }
			qrySentencia.append("'"+lItem.get(0).toString()+"'");
		}
	
	qrySentencia.append(") " +
		" order by ic_moneda, ig_cliente, to_char(df_fechaoperacion, 'dd/mm/yyyy')");
	
    System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
	
    return qrySentencia.toString();		
 	}  
	
	public String getDocumentQueryFile(){ // para todos los registros				
		String qry = "";
		try{
    	    ArmaQueryFile queryf = new ArmaQueryFile();
    		qry = queryf.regresaQueryOper((("C".equals(tipoLinea))?"12":ic_if),cliente,prestamo,FechaCorte,moneda);
    		
		/*	qrySentencia.append(
        
			"select " +
			"    /*+ " +
			"        INDEX (O IN_COM_OPERADO_01_NUK)" +
			"        USE_NL (O M)" +
			"    */ /*" +
			" IG_ESTATUS	AS	Status              "   +
			" ,to_char(O.DF_FECHAOPERACION,'dd/mm/yyyy')	AS	FechaOperacion      "   +
			" ,O.IC_MONEDA	AS	CodMoneda           "   +
			" ,M.CD_NOMBRE	AS	DescMoneda          "   +
			" ,O.FG_TIPOCAMBIO	AS	TipoCambio          "   +
			" ,O.IC_IF	AS	Intermediario       "   +
			" ,I.CG_RAZON_SOCIAL	AS	NomIntermediario    "   +
			" ,O.CG_CALIFICACION	AS	Calificacion        "   +
			" ,O.IG_BASEOPERACION	AS	BaseOperacion       "   +
			" ,O.CG_DESCBASEOP	AS	DescBaseOp          "   +
			" ,O.IG_CLIENTE	AS	Cliente             "   +
			" ,O.CG_NOMBRECLIENTE	AS	NombreCliente       "   +
			" ,O.IG_SUCCLIENTE	AS	SucCliente          "   +
			" ,O.IG_PROYECTO	AS	Proyecto            "   +
			" ,O.IG_CONTRATO	AS	Contrato            "   +
			" ,O.IG_PRESTAMO	AS	Prestamo            "   +
			" ,O.IG_NUMELECTRONICO	AS	NumElectronico      "   +
			" ,O.IG_DISPOSICION	AS	Disposicion         "   +
			" ,O.IG_ESTADO	AS	Estado              "   +
			" ,O.IG_MUNICIPIO	AS	Municipio           "   +
			" ,O.IG_SUBAPLICACION	AS	SubAplicacion       "   +
			" ,O.IG_SUCTRAM	AS	SucTram             "   +
			" ,to_char(O.DF_FECHAPRIPAGCAP,'dd/mm/yyyy')	AS	FechaPriPagCap      "   +
			" ,O.IG_TASA	AS	Tasa                "   +
			" ,O.CG_RELMAT	AS	RelMat              "   +
			" ,O.FG_SPREAD	AS	Spread              "   +
			" ,O.FG_MARGEN	AS	Margen              "   +
			" ,O.CG_ACTECO	AS	ActEco              "   +
			" ,O.CG_ANIOMOD	AS	AnioMOD            "   +
			" ,O.IG_ESTRATO	AS	Estrato             "   +
			" ,O.CG_AFIANZ	AS	Afianz              "   +
			" ,O.IG_FRECCAP	AS	FrecCap             "   +
			" ,O.IG_FRECINT	AS	FrecInt             "   +
			" ,O.IG_NUMCOUTAS	AS	NumCuotas           "   +
			" ,O.IG_TIPOAMORT	AS	TipoAmort           "   +
			" ,O.CG_MODPAGO	AS	ModPago             "   +
			" ,O.CG_CENTROFIN	AS	CentroFin           "   +
			" ,O.IG_SUCINTER	AS	SucInter            "   +
			" ,O.FG_MONTOOPER	AS	MontoOper           "   +
			" ,O.FG_MONTOPRIMCUOT	AS	MontoPrimCuot       "   +
			" ,O.FG_MONTOULTCOUT	AS	MontoUltCuota       "   +
			" ,O.FG_MONTORECAL	AS	MontoRecal          "   +
			" ,O.FG_MONTOPORGAR	AS	ComisionPorGar      "   +
			" ,O.FG_COMNODISP	AS	ComNoDisp           "   +
			" ,O.FG_INTCOBRADOXANTICIP	AS	IntCobAnt           "   +
			" ,O.IG_TIPOGAR	AS	TipoGar             "   +
			" ,O.FG_NETOOTORGADO	AS	NetoOtorgado        "   +
			" ,O.CG_PRIMABRUTA	AS	PrimaBruta"+
			" ,I.ic_financiera"+
			" ,'OperIfDE::getDocumentQueryFile' " +
			"from  " +
			"        COM_OPERADO O,  " +
			"        COMCAT_IF I,   " +
			"        COMCAT_MONEDA M " +
			"WHERE   O.ic_moneda = M.ic_moneda  " +
			"    and O.ic_if = I.ic_if " +
			"	 and O.ic_if = " + ic_if);

         if(!moneda.equals(""))
         	condicion += " and o.ic_moneda="+moneda;
         if(!cliente.equals(""))
         	condicion += " and o.ig_cliente="+cliente;
         if(!prestamo.equals(""))
         	condicion += " and o.ig_prestamo="+prestamo;
         if(!FechaCorte.equals(""))
            condicion += " AND DF_FECHAOPERACION >= to_date('"+FechaCorte+"', 'dd/mm/yyyy')" + " AND DF_FECHAOPERACION < to_date('"+FechaCorte+"', 'dd/mm/yyyy') + 1";

         	*/
        /* if(!FechaInicio.equals(""))
            condicion += " and df_fechaoperacion between to_date('"+FechaInicio+"', 'dd/mm/yyyy') " +
               " and to_date('"+ FechaFin +"', 'dd/mm/yyyy') ";
         else
            condicion += " and to_char(df_fechaoperacion, 'dd/mm/yyyy') = to_char(sysdate, 'dd/mm/yyyy')";*/
 
	 /*    qrySentencia.append(" " + condicion +
	       	" order by O.ic_moneda, O.ig_cliente, to_char(df_fechaoperacion, 'dd/mm/yyyy') ");*/

	    System.out.println("EL query de DocumentQueryFile es: "+qry);//qrySentencia.toString());
      
    }catch(Exception e){
      System.out.println("OperIfDE::getDocumentQueryFileException "+e);
    }
    return qry;//qrySentencia.toString();
 	}     
	         
	/**
	 * En este m?todo se debe realizar la implementaci?n de la generaci?n de archivo
	 * con base en el resultset que se recibe como par?metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar? el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		
		//
		int total = 0;
		//
			if ("TXT".equals(tipo)) {
				try {
					int registros = 0;	
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
					while(rs.next()){
						if(registros==0) { 
							//contenidoArchivo.append("Nombre del proveedor, Numero del proveedor, Numero de Transferencia, Fecha de pago, Montonto total de la transferencia, Moneda, Fecha de la factura, Monto de la factura, Fecha de operacion");
							contenidoArchivo.append("Status              ;FechaOperacion      ;CodMoneda           ;DescMoneda          ;TipoCambio          ;Intermediario       ;NomIntermediario    ;Calificacion        ;BaseOperacion       ;DescBaseOp          ;Cliente             ;NombreCliente       ;SucCliente          ;Proyecto            ;Contrato            ;Prestamo            ;NumElectronico      ;Disposicion         ;Estado              ;Municipio           ;SubAplicacion       ;SucTram             ;FechaPriPagCap      ;Tasa                ;RelMat              ;Spread              ;Margen              ;ActEco              ;Anio/Mod            ;Estrato             ;Afianz              ;FrecCap             ;FrecInt             ;NumCuotas           ;TipoAmort           ;ModPago             ;CentroFin           ;SucInter            ;MontoOper           ;MontoPrimCuot       ;MontoUltCuota       ;MontoRecal          ;ComisionPorGar      ;ComNoDisp           ;IntCobAnt           ;TipoGar             ;NetoOtorgado        ;PrimaBruta          \n");
							
						}
						registros++;
						String status = rs.getString("STATUS")==null?"":rs.getString("STATUS");
						String fechaOperacion = rs.getString("FECHAOPERACION")==null?"":rs.getString("FECHAOPERACION");
						String codMoneda = rs.getString("CODMONEDA")==null?"":rs.getString("CODMONEDA");
						String descMoneda = rs.getString("DESCMONEDA")==null?"":rs.getString("DESCMONEDA");
						String tipoCambio = rs.getString("TIPOCAMBIO")==null?"":rs.getString("TIPOCAMBIO");
												
						String intermediario = rs.getString("IC_FINANCIERA")==null?"":rs.getString("IC_FINANCIERA");
						String nombreIntermediario = rs.getString("NOMINTERMEDIARIO")==null?"":rs.getString("NOMINTERMEDIARIO");
						String calificacion = rs.getString("CALIFICACION")==null?"":rs.getString("CALIFICACION");
						String baseOperacion = rs.getString("BASEOPERACION")==null?"":rs.getString("BASEOPERACION");
						String baseDescipcionOperacion = rs.getString("DESCBASEOP")==null?"":rs.getString("DESCBASEOP");
						String cliente = rs.getString("CLIENTE")==null?"":rs.getString("CLIENTE");
						
						String nombreCliente = rs.getString("NOMBRECLIENTE")==null?"":rs.getString("NOMBRECLIENTE");
						String sucCliente = rs.getString("SUCCLIENTE")==null?"":rs.getString("SUCCLIENTE");
						String proyecto = rs.getString("PROYECTO")==null?"":rs.getString("PROYECTO");
						String contrato = rs.getString("CONTRATO")==null?"":rs.getString("CONTRATO");
						String prestamo = rs.getString("PRESTAMO")==null?"":rs.getString("PRESTAMO");
						String numeroElectronico = rs.getString("NUMELECTRONICO")==null?"":rs.getString("NUMELECTRONICO");
						
						String disposicion = rs.getString("DISPOSICION")==null?"":rs.getString("DISPOSICION");
						String estado = rs.getString("ESTADO")==null?"":rs.getString("ESTADO");
						String municipio = rs.getString("MUNICIPIO")==null?"":rs.getString("MUNICIPIO");
						String subAplicacion = rs.getString("SUBAPLICACION")==null?"":rs.getString("SUBAPLICACION");
						String sucTramite = rs.getString("SUCTRAM")==null?"":rs.getString("SUCTRAM");
						String fechaPriPagCap = rs.getString("FECHAPRIPAGCAP")==null?"":rs.getString("FECHAPRIPAGCAP");
						
						String tasa = rs.getString("TASA")==null?"":rs.getString("TASA");
						String relMat = rs.getString("RELMAT")==null?"":rs.getString("RELMAT");
						String spread = rs.getString("SPREAD")==null?"":rs.getString("SPREAD");
						String margen = rs.getString("MARGEN")==null?"":rs.getString("MARGEN");
						String actEco = rs.getString("ACTECO")==null?"":rs.getString("ACTECO");
						String anioMod = rs.getString("ANIOMOD")==null?"":rs.getString("ANIOMOD");//OK
						
						String estrato = rs.getString("ESTRATO")==null?"":rs.getString("ESTRATO");
						String afianz = rs.getString("AFIANZ")==null?"":rs.getString("AFIANZ");
						String frecCap = rs.getString("FRECCAP")==null?"":rs.getString("FRECCAP");
						String frecInt = rs.getString("FRECINT")==null?"":rs.getString("FRECINT");
						String numCuotas = rs.getString("NUMCUOTAS")==null?"":rs.getString("NUMCUOTAS");
						String tipoAmort = rs.getString("TIPOAMORT")==null?"":rs.getString("TIPOAMORT");//OK
						
						String modPago = rs.getString("MODPAGO")==null?"":rs.getString("MODPAGO");
						String centroFin = rs.getString("CENTROFIN")==null?"":rs.getString("CENTROFIN");
						String sucInter = rs.getString("SUCINTER")==null?"":rs.getString("SUCINTER");
						String montoOper = rs.getString("MONTOOPER")==null?"":rs.getString("MONTOOPER");
						String montoPrimCuot = rs.getString("MONTOPRIMCUOT")==null?"":rs.getString("MONTOPRIMCUOT");
						String montoUltCuota = rs.getString("MONTOULTCUOTA")==null?"":rs.getString("MONTOULTCUOTA");
						
						String montoRecal = rs.getString("MONTORECAL")==null?"":rs.getString("MONTORECAL");
						String comisionPorGar = rs.getString("COMISIONPORGAR")==null?"":rs.getString("COMISIONPORGAR");
						String comNoDisp = rs.getString("COMNODISP")==null?"":rs.getString("COMNODISP");
						String intCobAnt = rs.getString("INTCOBANT")==null?"":rs.getString("INTCOBANT");
						String tipoGar = rs.getString("TIPOGAR")==null?"":rs.getString("TIPOGAR");
						String netoOtorgado = rs.getString("NETOOTORGADO")==null?"":rs.getString("NETOOTORGADO");
						String primaBruta = rs.getString("PRIMABRUTA")==null?"":rs.getString("PRIMABRUTA");
						
						
						contenidoArchivo.append(
							Comunes.formatoFijo(rs.getString("Status"),2," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("FechaOperacion"),10," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("CodMoneda"),2," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("DescMoneda"),30," ","A")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TipoCambio"),4,false),15," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("ic_financiera"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("NomIntermediario"),60," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Calificacion"),13," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("BaseOperacion"),6," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("DescBaseOp"),60," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Cliente"),12," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("NombreCliente"),70," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("SucCliente"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Proyecto"),12," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Contrato"),12," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Prestamo"),12," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("NumElectronico"),11," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Disposicion"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Estado"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Municipio"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("SubAplicacion"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("SucTram"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("FechaPriPagCap"),20," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Tasa"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("RelMat"),2," ","A")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Spread"),4,false),7," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Margen"),4,false),7," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("ActEco"),20," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("AnioMOD"),10," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Estrato"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Afianz"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("FrecCap"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("FrecInt"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("NumCuotas"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("TipoAmort"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("ModPago"),20," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("CentroFin"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("SucInter"),5," ","A")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoOper"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoPrimCuot"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoUltCuota"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoRecal"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("ComisionPorGar"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("ComNoDisp"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IntCobAnt"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("TipoGar"),5," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("NetoOtorgado"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("PrimaBruta"),6," ","D")+";\n"
						);
						
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio  	
				/*	if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}	*/ 
				} catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}if ("ZIP".equals(tipo)) {
				try {
					int registros = 0;	
					//nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
					FechaCorte = FechaCorte.replace('/','_');
					nombreArchivo = "archoper_"+FechaCorte+"_"+ic_if+"_S.txt";
					
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
					while(rs.next()){
						if(registros==0) { 
							//contenidoArchivo.append("Nombre del proveedor, Numero del proveedor, Numero de Transferencia, Fecha de pago, Montonto total de la transferencia, Moneda, Fecha de la factura, Monto de la factura, Fecha de operacion");
							contenidoArchivo.append("Status              ;FechaOperacion      ;CodMoneda           ;DescMoneda          ;TipoCambio          ;Intermediario       ;NomIntermediario    ;Calificacion        ;BaseOperacion       ;DescBaseOp          ;Cliente             ;NombreCliente       ;SucCliente          ;Proyecto            ;Contrato            ;Prestamo            ;NumElectronico      ;Disposicion         ;Estado              ;Municipio           ;SubAplicacion       ;SucTram             ;FechaPriPagCap      ;Tasa                ;RelMat              ;Spread              ;Margen              ;ActEco              ;Anio/Mod            ;Estrato             ;Afianz              ;FrecCap             ;FrecInt             ;NumCuotas           ;TipoAmort           ;ModPago             ;CentroFin           ;SucInter            ;MontoOper           ;MontoPrimCuot       ;MontoUltCuota       ;MontoRecal          ;ComisionPorGar      ;ComNoDisp           ;IntCobAnt           ;TipoGar             ;NetoOtorgado        ;PrimaBruta          \n");
							
						}
						registros++;
						String status = rs.getString("STATUS")==null?"":rs.getString("STATUS");
						String fechaOperacion = rs.getString("FECHAOPERACION")==null?"":rs.getString("FECHAOPERACION");
						String codMoneda = rs.getString("CODMONEDA")==null?"":rs.getString("CODMONEDA");
						String descMoneda = rs.getString("DESCMONEDA")==null?"":rs.getString("DESCMONEDA");
						String tipoCambio = rs.getString("TIPOCAMBIO")==null?"":rs.getString("TIPOCAMBIO");
												
						String intermediario = rs.getString("IC_FINANCIERA")==null?"":rs.getString("IC_FINANCIERA");
						String nombreIntermediario = rs.getString("NOMINTERMEDIARIO")==null?"":rs.getString("NOMINTERMEDIARIO");
						String calificacion = rs.getString("CALIFICACION")==null?"":rs.getString("CALIFICACION");
						String baseOperacion = rs.getString("BASEOPERACION")==null?"":rs.getString("BASEOPERACION");
						String baseDescipcionOperacion = rs.getString("DESCBASEOP")==null?"":rs.getString("DESCBASEOP");
						String cliente = rs.getString("CLIENTE")==null?"":rs.getString("CLIENTE");
						
						String nombreCliente = rs.getString("NOMBRECLIENTE")==null?"":rs.getString("NOMBRECLIENTE");
						String sucCliente = rs.getString("SUCCLIENTE")==null?"":rs.getString("SUCCLIENTE");
						String proyecto = rs.getString("PROYECTO")==null?"":rs.getString("PROYECTO");
						String contrato = rs.getString("CONTRATO")==null?"":rs.getString("CONTRATO");
						String prestamo = rs.getString("PRESTAMO")==null?"":rs.getString("PRESTAMO");
						String numeroElectronico = rs.getString("NUMELECTRONICO")==null?"":rs.getString("NUMELECTRONICO");
						
						String disposicion = rs.getString("DISPOSICION")==null?"":rs.getString("DISPOSICION");
						String estado = rs.getString("ESTADO")==null?"":rs.getString("ESTADO");
						String municipio = rs.getString("MUNICIPIO")==null?"":rs.getString("MUNICIPIO");
						String subAplicacion = rs.getString("SUBAPLICACION")==null?"":rs.getString("SUBAPLICACION");
						String sucTramite = rs.getString("SUCTRAM")==null?"":rs.getString("SUCTRAM");
						String fechaPriPagCap = rs.getString("FECHAPRIPAGCAP")==null?"":rs.getString("FECHAPRIPAGCAP");
						
						String tasa = rs.getString("TASA")==null?"":rs.getString("TASA");
						String relMat = rs.getString("RELMAT")==null?"":rs.getString("RELMAT");
						String spread = rs.getString("SPREAD")==null?"":rs.getString("SPREAD");
						String margen = rs.getString("MARGEN")==null?"":rs.getString("MARGEN");
						String actEco = rs.getString("ACTECO")==null?"":rs.getString("ACTECO");
						String anioMod = rs.getString("ANIOMOD")==null?"":rs.getString("ANIOMOD");//OK
						
						String estrato = rs.getString("ESTRATO")==null?"":rs.getString("ESTRATO");
						String afianz = rs.getString("AFIANZ")==null?"":rs.getString("AFIANZ");
						String frecCap = rs.getString("FRECCAP")==null?"":rs.getString("FRECCAP");
						String frecInt = rs.getString("FRECINT")==null?"":rs.getString("FRECINT");
						String numCuotas = rs.getString("NUMCUOTAS")==null?"":rs.getString("NUMCUOTAS");
						String tipoAmort = rs.getString("TIPOAMORT")==null?"":rs.getString("TIPOAMORT");//OK
						
						String modPago = rs.getString("MODPAGO")==null?"":rs.getString("MODPAGO");
						String centroFin = rs.getString("CENTROFIN")==null?"":rs.getString("CENTROFIN");
						String sucInter = rs.getString("SUCINTER")==null?"":rs.getString("SUCINTER");
						String montoOper = rs.getString("MONTOOPER")==null?"":rs.getString("MONTOOPER");
						String montoPrimCuot = rs.getString("MONTOPRIMCUOT")==null?"":rs.getString("MONTOPRIMCUOT");
						String montoUltCuota = rs.getString("MONTOULTCUOTA")==null?"":rs.getString("MONTOULTCUOTA");
						
						String montoRecal = rs.getString("MONTORECAL")==null?"":rs.getString("MONTORECAL");
						String comisionPorGar = rs.getString("COMISIONPORGAR")==null?"":rs.getString("COMISIONPORGAR");
						String comNoDisp = rs.getString("COMNODISP")==null?"":rs.getString("COMNODISP");
						String intCobAnt = rs.getString("INTCOBANT")==null?"":rs.getString("INTCOBANT");
						String tipoGar = rs.getString("TIPOGAR")==null?"":rs.getString("TIPOGAR");
						String netoOtorgado = rs.getString("NETOOTORGADO")==null?"":rs.getString("NETOOTORGADO");
						String primaBruta = rs.getString("PRIMABRUTA")==null?"":rs.getString("PRIMABRUTA");
						
						
						contenidoArchivo.append(
							Comunes.formatoFijo(rs.getString("Status"),2," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("FechaOperacion"),10," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("CodMoneda"),2," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("DescMoneda"),30," ","A")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("TipoCambio"),4,false),15," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("ic_financiera"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("NomIntermediario"),60," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Calificacion"),13," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("BaseOperacion"),6," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("DescBaseOp"),60," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Cliente"),12," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("NombreCliente"),70," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("SucCliente"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Proyecto"),12," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Contrato"),12," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Prestamo"),12," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("NumElectronico"),11," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Disposicion"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Estado"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Municipio"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("SubAplicacion"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("SucTram"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("FechaPriPagCap"),20," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Tasa"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("RelMat"),2," ","A")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Spread"),4,false),7," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("Margen"),4,false),7," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("ActEco"),20," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("AnioMOD"),10," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("Estrato"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("Afianz"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("FrecCap"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("FrecInt"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("NumCuotas"),5," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("TipoAmort"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("ModPago"),20," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("CentroFin"),5," ","A")+";"+ 
							Comunes.formatoFijo(rs.getString("SucInter"),5," ","A")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoOper"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoPrimCuot"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoUltCuota"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("MontoRecal"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("ComisionPorGar"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("ComNoDisp"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("IntCobAnt"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("TipoGar"),5," ","D")+";"+ 
							Comunes.formatoFijo(Comunes.formatoDecimal(rs.getString("NetoOtorgado"),2,false),19," ","D")+";"+ 
							Comunes.formatoFijo(rs.getString("PrimaBruta"),6," ","D")+";\n"
						);
						
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio  	
				/*	if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}	*/ 
				} catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}else if ("PDF".equals(tipo)) {
				try {
					HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
							
					pdfDoc.addText("M?xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.setTable(9, 80);
					pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Numero del Proveedor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Numero de Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Pagado","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Total de la Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operacion","celda01",ComunesPDF.CENTER);
													
					while (rs.next()) {
						String nombreproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numeroproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
						 
						pdfDoc.setCell(nombreproveedor,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(numeroproveedor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numtrans,"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(fechapago,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechafactura,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(fechaoperacion,"formas",ComunesPDF.CENTER);				
					}
					pdfDoc.addTable();
					pdfDoc.endDocument();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}
		log.error(">>>>>>>>>>>>>>>>>>>>>>>>>> "+nombreArchivo);	
		return nombreArchivo;	
	}	
	
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo 	= new CreaArchivo();
		
		if ("PDF".equals(tipo)) {   
			int TOTAL_CAMPOS = 18;
			int nRow = 0, numMon = 0, numDL = 0;
			double totMntOperMon        = 0.0,    totMntOperDL       = 0.0;
         double totMntPrimCuotMon  	= 0.0,    totMntPrimCuotDL 	 = 0.0;
         double totNetoOtorgMon      = 0.0,    totNetoOtorgDL     = 0.0;
			int numCols = 0;
			float anchos[] = null;
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			
				int nColumn = 0;
        
        if(parametro.equals("S"))
          nColumn = 12;
        else
          nColumn = 11;
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
        pdfDoc.setTable(nColumn, 100);
        pdfDoc.setCell("Cliente","formasmenB",ComunesPDF.CENTER);		
        pdfDoc.setCell("Nombre del Cliente","formasmenB",ComunesPDF.CENTER);
        pdfDoc.setCell("Pr�stamo","formasmenB",ComunesPDF.CENTER);
        
        if(parametro.equals("S"))
          pdfDoc.setCell("No. Elec","formasmenB",ComunesPDF.CENTER);
            
        pdfDoc.setCell("Fecha Operacion","formasmenB",ComunesPDF.CENTER);
        pdfDoc.setCell("Fecha 1er. Pago","formasmenB",ComunesPDF.CENTER);
        pdfDoc.setCell("Tasa","formasmenB",ComunesPDF.CENTER);
       // pdfDoc.setCell("Moneda","formasmenB",ComunesPDF.CENTER);
        pdfDoc.setCell("Spread","formasmenB",ComunesPDF.CENTER);
        pdfDoc.setCell("Margen","formasmenB",ComunesPDF.CENTER);
        pdfDoc.setCell("Monto Operado","formasmenB",ComunesPDF.CENTER);
        pdfDoc.setCell("Monto 1er. cuota","formasmenB",ComunesPDF.CENTER);
        pdfDoc.setCell("Neto Otorgado","formasmenB",ComunesPDF.CENTER);

				while (rs.next()) {
					Vector vecDatos = new Vector();	   
					for(int i=1; i<=TOTAL_CAMPOS; i++) {
						vecDatos.add(rs.getString(i));
					}
					//String numeroDocumento = rs.getString("ig_numelectronico")==null?"":rs.getString("ig_numelectronico");//FODEA 015 - 2009 ACF
					if(rs.getString(3).equals("54")){
						numDL++;
            totMntOperDL      += Double.valueOf(rs.getString(10)).doubleValue();
            totMntPrimCuotDL  += Double.valueOf(rs.getString(11)).doubleValue();
            totNetoOtorgDL    += Double.valueOf(rs.getString(12)).doubleValue();
					} else {
						numMon++; 
            totMntOperMon     += Double.valueOf(rs.getString(10)).doubleValue();
            totMntPrimCuotMon += Double.valueOf(rs.getString(11)).doubleValue();
            totNetoOtorgMon   += Double.valueOf(rs.getString(12)).doubleValue();
					}        
          pdfDoc.setCell(vecDatos.get(0).toString().trim(),"formasmen",ComunesPDF.LEFT);		
          pdfDoc.setCell(vecDatos.get(1).toString().trim(),"formasmen",ComunesPDF.CENTER);
          pdfDoc.setCell(vecDatos.get(2).toString().trim(),"formasmen",ComunesPDF.CENTER);
          if(parametro.equals("S"))
            pdfDoc.setCell(vecDatos.get(3).toString().trim(),"formasmen",ComunesPDF.LEFT);
          pdfDoc.setCell(vecDatos.get(4).toString().trim(),"formasmen",ComunesPDF.CENTER);
          pdfDoc.setCell(vecDatos.get(5).toString().trim(),"formasmen",ComunesPDF.CENTER);
          //pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(6).toString().trim(), 2),"formasmen",ComunesPDF.CENTER);
			 pdfDoc.setCell(vecDatos.get(6).toString().trim(),"formasmen",ComunesPDF.CENTER);
         // pdfDoc.setCell(vecDatos.get(13).toString().trim(),"formasmen",ComunesPDF.CENTER);//MONEDA
          
			 //pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(7).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
			 pdfDoc.setCell(vecDatos.get(7).toString().trim(),"formasmen",ComunesPDF.CENTER);
          //pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(8).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
			 pdfDoc.setCell(vecDatos.get(8).toString().trim(),"formasmen",ComunesPDF.CENTER);
          pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(9).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
          pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(10).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
          pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(11).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
          //pdfDoc.setCell("$ "+Comunes.formatoDecimal(vecDatos.get(12).toString().trim(), 2),"formasmen",ComunesPDF.RIGHT);
          //FODEA 015 - 2009 ACF (I)   
					nRow++;
				}
				/*
				if(nRow > 0) { 
					if(numMon > 0){
					  if(parametro.equals("S")) {
						pdfDoc.setCell("Total Parcial Moneda Nacional","formasmenB",ComunesPDF.RIGHT,9);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totMntOperMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totMntPrimCuotMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totNetoOtorgMon).toString(), 2),"formasmen",ComunesPDF.RIGHT);
					//FODEA 015 - 2009 ACF (I)
                  if(parametro.equals("S")){
                    pdfDoc.setCell(" ","formasmen",ComunesPDF.LEFT);
                  }
					}          
					if(numDL > 0){  
						 if(parametro.equals("S")) {	
							pdfDoc.setCell("Total Parcial D�lares","formasmenB",ComunesPDF.RIGHT,9);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totMntOperDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totMntPrimCuotDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(totNetoOtorgDL).toString(), 2),"formasmen",ComunesPDF.RIGHT);
							//FODEA 015 - 2009 ACF (I)
							if(paramNumDocto.equals("S")){
								pdfDoc.setCell(" ","formasmen",ComunesPDF.LEFT);
							}
							//FODEA 015 - 2009 ACF (F)
						}
					}
					      
					//Registros vecTotales = queryHelperRegExtJS.getResultCount(request);
					
					if(vecTotales.getNumeroRegistros()>0){
						int j = 0;
						List vecColumnas = null;
						for(j=0;j<vecTotales.getNumeroRegistros();j++){
							vecColumnas = vecTotales.getData(j);
							if(parametro.equals("S")) {	 	
								pdfDoc.setCell("TOTAL "+vecColumnas.get(4).toString(),"formasmenB",ComunesPDF.RIGHT,9);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(1).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(2).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								pdfDoc.setCell("$ "+Comunes.formatoDecimal(new Double(vecColumnas.get(3).toString()).toString(), 2),"formasmen",ComunesPDF.RIGHT);
								//FODEA 015 - 2009 ACF (I)
								if(parametro.equals("S")){
									pdfDoc.setCell(" ","formasmen",ComunesPDF.LEFT);
								}
								//FODEA 015 - 2009 ACF (F)
							}
						}	
					}
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
       }*/
				pdfDoc.addTable();
				pdfDoc.endDocument();
      } catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
		}		
		return nombreArchivo;    
	}
	
	public List getConditions() {
		return conditions;
	}

  
  public void setParamNumDocto(String paramNumDocto) {
    this.paramNumDocto = paramNumDocto;
  }


  public void setParametro(String parametro) {
    this.parametro = parametro;
  }


  public void setMoneda(String moneda) {
    this.moneda = moneda;
  }


  public void setCliente(String cliente) {
    this.cliente = cliente;
  }


  public void setPrestamo(String prestamo) {
    this.prestamo = prestamo;
  }


  public void setFechaCorte(String FechaCorte) {
    this.FechaCorte = FechaCorte;
  }


  public void setIc_if(String ic_if) {
    this.ic_if = ic_if;
  }


  public void setVecTotales(Registros vecTotales) {
    this.vecTotales = vecTotales;
  }

	public Registros getDatosFechaCorte(){
	log.info("getDatosFechaCorte(E)");
		StringBuffer qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT	");
			qrySentencia.append(	"	 DISTINCT TO_CHAR (DF_FECHAOPERACION, 'DD/MM/YYYY') AS CLAVE, 	");
			qrySentencia.append(	"	 to_char(DF_FECHAOPERACION,'DD/MM/YYYY') AS DESCRIPCION,	");
			qrySentencia.append(	"	 DF_FECHAOPERACION	");
			qrySentencia.append(	"FROM  	");
			qrySentencia.append(	"	  COM_OPERADO  	");
			qrySentencia.append(	"WHERE  	");
			qrySentencia.append(	"	  IC_IF = ?	");
			if(cliente!=null && !"".equals(cliente))
				qrySentencia.append(	" AND IG_CLIENTE = ? ");
				
			qrySentencia.append(		"ORDER BY 	\n"+
										"	 DF_FECHAOPERACION DESC	");

			conditions.add(ic_if);
			if(cliente!=null && !"".equals(cliente))
				conditions.add(cliente);
			
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosFechaCorte  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia Fecha de corte: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosFechaCorte (S) ");
		return registros;
															
	}
	
		public Registros catalogoMoneda(){
	log.info("getDatosFechaCorte(E)");
		StringBuffer qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT DISTINCT	\n"+
										"	 M.IC_MONEDA as clave,		\n"+
										"	 M.CD_NOMBRE as descripcion		\n"+
										"FROM 		\n"+
										"	 COMCAT_MONEDA M, 		\n"+
										"	 COMCAT_TASA T		\n"+
										"WHERE 		\n"+
										"	 M.IC_MONEDA = T.IC_MONEDA AND 		\n"+
										"	 T.CS_DISPONIBLE = 'S' AND		\n"+
										"	 M.IC_MONEDA = 1		\n"+
										"UNION		\n"+
										"SELECT DISTINCT 		\n"+
										"	 M.IC_MONEDA as clave, 		\n"+
										"	 M.CD_NOMBRE as descripcion		\n"+
										"FROM 		\n"+
										"	 COMCAT_MONEDA M , 		\n"+
										"	 COMCAT_TASA T ,		\n"+
										"	 COM_TIPO_CAMBIO TC    	\n"+
										"WHERE     	\n"+
										"	  M.IC_MONEDA = T.IC_MONEDA AND     	\n"+
										"	  T.CS_DISPONIBLE = 'S' AND    	\n"+
										"	  M.IC_MONEDA = TC.IC_MONEDA    	\n"+
										"ORDER BY 2    ");

			
			
			registros = con.consultarDB(qrySentencia.toString());
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosFechaCorte  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia Fecha de corte: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosFechaCorte (S) ");
		return registros;
															
	}


  public void setTipoLinea(String tipoLinea)
  {
    this.tipoLinea = tipoLinea;
  }


  public String getTipoLinea()
  {
    return tipoLinea;
  }
}