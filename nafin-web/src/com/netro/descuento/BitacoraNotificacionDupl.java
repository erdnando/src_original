package com.netro.descuento;

import com.netro.pdf.ComunesPDF;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorThreadRegExtJS;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

/********************************************************************************************
 * Fodea:       38-2014                                                                     *
 * Descripci�n: Productos/Descuento/Consultas/Bit�cora de Notificaciones por Duplicidad     *
 * Elabor�:     Agust�n Bautista Ruiz                                                       *
 * Fecha:       24/10/2014                                                                  *
 ********************************************************************************************/
public class BitacoraNotificacionDupl implements IQueryGeneratorThreadRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(BitacoraNotificacionDupl.class);
	private List   conditions;
	private int    countReg;
	private String icEpo;
	private String icPyme;
	private String numDocto;
	private String fechaInicio;
	private String fechaFinal;

	public BitacoraNotificacionDupl(){}

	/**
	 * Obtiene las llaves primarias
	 * @return qrySentencia
	 */
	public String getDocumentQuery(){

		log.info("getDocumentQuery (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		conditions                = new ArrayList();

		if(!this.icEpo.equals("")){
			condicion.append("  AND  D.IC_EPO = ? ");
			conditions.add(this.icEpo);
		}
		if(!this.icPyme.equals("")){
			condicion.append("  AND  D.IC_PYME = ? ");
			conditions.add(this.icPyme);
		}
		if(!this.numDocto.equals("")){
			condicion.append("  AND  D.IG_NUMERO_DOCTO = ? ");
			conditions.add(this.numDocto);
		}
		if(!this.fechaInicio.equals("") && !this.fechaFinal.equals("")){
			condicion.append("  AND  D.DF_FECHA_ALERT >= TO_DATE(?, 'dd/mm/yyyy') ");
			condicion.append("  AND  D.DF_FECHA_ALERT < TO_DATE(?, 'dd/mm/yyyy') + 1 ");
			conditions.add(this.fechaInicio);
			conditions.add(this.fechaFinal);
		}

		qrySentencia.append(
			"SELECT D.IC_DOCUMENTO "                          +
			"FROM   COM_BIT_DUPLICADOS D, "                   +
			"       COMCAT_EPO E, "                           +
			"       COMCAT_PYME P, "                          +
			"       COMCAT_MONEDA M, "                        +
			"       COMCAT_ESTATUS_DOCTO S "                  +
			"WHERE  D.IC_EPO = E.IC_EPO "                     +
			"  AND  D.IC_PYME = P.IC_PYME "                   +
			"  AND  D.IC_MONEDA = M.IC_MONEDA "               +
			"  AND  D.IC_ESTATUS_DOCTO = S.IC_ESTATUS_DOCTO " +
			condicion.toString()                              +
			"ORDER BY D.IC_DOCUMENTO ASC "
		);

		log.info("Sentencia: "   + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getDocumentQuery (S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene los registros totales
	 * @return qrySentencia
	 */
	public String getAggregateCalculationQuery(){

		log.info("getAggregateCalculationQuery (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		conditions                = new ArrayList();

		if(!this.icEpo.equals("")){
			condicion.append("  AND  D.IC_EPO = ? ");
			conditions.add(this.icEpo);
		}
		if(!this.icPyme.equals("")){
			condicion.append("  AND  D.IC_PYME = ? ");
			conditions.add(this.icPyme);
		}
		if(!this.numDocto.equals("")){
			condicion.append("  AND  D.IG_NUMERO_DOCTO = ? ");
			conditions.add(this.numDocto);
		}
		if(!this.fechaInicio.equals("") && !this.fechaFinal.equals("")){
			condicion.append("  AND  D.DF_FECHA_ALERT >= TO_DATE(?, 'dd/mm/yyyy') ");
			condicion.append("  AND  D.DF_FECHA_ALERT < TO_DATE(?, 'dd/mm/yyyy') + 1 ");
			conditions.add(this.fechaInicio);
			conditions.add(this.fechaFinal);
		}

		qrySentencia.append(
			"SELECT COUNT(D.IC_DOCUMENTO) "                   +
			"FROM   COM_BIT_DUPLICADOS D, "                   +
			"       COMCAT_EPO E, "                           +
			"       COMCAT_PYME P, "                          +
			"       COMCAT_MONEDA M, "                        +
			"       COMCAT_ESTATUS_DOCTO S "                  +
			"WHERE  D.IC_EPO = E.IC_EPO "                     +
			"  AND  D.IC_PYME = P.IC_PYME "                   +
			"  AND  D.IC_MONEDA = M.IC_MONEDA "               +
			"  AND  D.IC_ESTATUS_DOCTO = S.IC_ESTATUS_DOCTO " +
			condicion.toString()
		);

		log.info("Sentencia: "   + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getAggregateCalculationQuery (S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene la consulta a partir de las llaves primarias
	 * @return qrySentencia
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){

		log.info("getDocumentSummaryQueryForIds (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		conditions                = new ArrayList();

		condicion.append("  AND  D.IC_DOCUMENTO IN (");
		for (int i = 0; i < ids.size(); i++){
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				condicion.append(",");
			}
			condicion.append(" ? " );
			conditions.add(new Long(lItem.get(0).toString()));
		}
		condicion.append(") ");

		qrySentencia.append(
			"SELECT   D.IC_DOCUMENTO, "                                                     +
			"         D.IG_NUMERO_DOCTO AS NO_DOCTO, "                                      +
			"         E.CG_RAZON_SOCIAL AS EPO, "                                           +
			"         P.CG_RAZON_SOCIAL AS PYME, "                                          +
			"         TO_CHAR(D.DF_FECHA_DOCTO, 'dd/mm/yyyy') AS FECHA_EMISION, "           +
			"         M.CD_NOMBRE AS MONEDA, "                                              +
			"         D.FN_MONTO AS MONTO, "                                                +
			"         D.IC_USUARIO AS USUARIO_PUBLICO, "                                    +
			"         D.CG_NOMBRE_USUARIO AS NOMBRE_USUARIO, "                              +
			"         TO_CHAR(D.DF_FECHA_ALERT, 'dd/mm/yyyy HH24:MI:SS') AS FECHA_ALERTA, " +
			"         S.CD_DESCRIPCION AS ESTATUS_PUBLICACION "                             +
			"FROM     COM_BIT_DUPLICADOS D, "                                               +
			"         COMCAT_EPO E, "                                                       +
			"         COMCAT_PYME P, "                                                      +
			"         COMCAT_MONEDA M, "                                                    +
			"         COMCAT_ESTATUS_DOCTO S "                                              +
			"WHERE    D.IC_EPO = E.IC_EPO "                                                 +
			"     AND D.IC_PYME = P.IC_PYME "                                               +
			"     AND D.IC_MONEDA = M.IC_MONEDA "                                           +
			"     AND D.IC_ESTATUS_DOCTO = S.IC_ESTATUS_DOCTO "                             +
			condicion.toString()                                                            +
			"ORDER BY D.IC_DOCUMENTO ASC "
		);

		log.info("Sentencia: "   + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getDocumentSummaryQueryForIds (S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene la consulta con todos los registros para generar los archivos CSV y PDF.
	 * Es decir, no contempla paginaci�n.
	 * @return qrySentencia
	 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		conditions                = new ArrayList();

		if(!this.icEpo.equals("")){
			condicion.append("  AND  D.IC_EPO = ? ");
			conditions.add(this.icEpo);
		}
		if(!this.icPyme.equals("")){
			condicion.append("  AND  D.IC_PYME = ? ");
			conditions.add(this.icPyme);
		}
		if(!this.numDocto.equals("")){
			condicion.append("  AND  D.IG_NUMERO_DOCTO = ? ");
			conditions.add(this.numDocto);
		}
		if(!this.fechaInicio.equals("") && !this.fechaFinal.equals("")){
			condicion.append("  AND  D.DF_FECHA_ALERT >= TO_DATE(?, 'dd/mm/yyyy') ");
			condicion.append("  AND  D.DF_FECHA_ALERT < TO_DATE(?, 'dd/mm/yyyy') + 1 ");
			conditions.add(this.fechaInicio);
			conditions.add(this.fechaFinal);
		}

		qrySentencia.append(
		"SELECT D.IG_NUMERO_DOCTO AS NO_DOCTO, "                                      +
		"       E.CG_RAZON_SOCIAL AS EPO, "                                           +
		"       P.CG_RAZON_SOCIAL AS PYME, "                                          +
		"       TO_CHAR(D.DF_FECHA_DOCTO, 'dd/mm/yyyy') AS FECHA_EMISION, "           +
		"       M.CD_NOMBRE AS MONEDA, "                                              +
		"       D.FN_MONTO AS MONTO, "                                                +
		"       D.IC_USUARIO AS USUARIO_PUBLICO, "                                    +
		"       D.CG_NOMBRE_USUARIO AS NOMBRE_USUARIO, "                              +
		"       TO_CHAR(D.DF_FECHA_ALERT, 'dd/mm/yyyy HH24:MI:SS') AS FECHA_ALERTA, " +
		"       S.CD_DESCRIPCION AS ESTATUS_PUBLICACION "                             +
		"FROM   COM_BIT_DUPLICADOS D, "                                               +
		"       COMCAT_EPO E, "                                                       +
		"       COMCAT_PYME P, "                                                      +
		"       COMCAT_MONEDA M, "                                                    +
		"       COMCAT_ESTATUS_DOCTO S "                                              +
		"WHERE  D.IC_EPO = E.IC_EPO "                                                 +
		"  AND  D.IC_PYME = P.IC_PYME "                                               +
		"  AND  D.IC_MONEDA = M.IC_MONEDA "                                           +
		"  AND  D.IC_ESTATUS_DOCTO = S.IC_ESTATUS_DOCTO "                             +
		condicion.toString()                                                          +
		"ORDER BY D.IC_DOCUMENTO ASC "
		);

		log.info("Sentencia: "   + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getDocumentQueryFile (S)");
		return qrySentencia.toString();
	}

	/**
	 * Genera los archivos CSV y PDF sin utilizar la paginaci�n, los datos que recibe en el request vienen de la consulta
	 * en el m�todo getDocumentQueryFile()
	 * @param request
	 * @param rs
	 * @param path
	 * @param tipo CSV o PDF
	 * @return nombreArchivo
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");
		HttpSession session     = request.getSession();
		CreaArchivo creaArchivo = new CreaArchivo();
		String nombreArchivo    = "";
		String nombreEpo        = "";
		String nombrePyme       = "";
		String numDocto         = "";
		String fechaEmision     = "";
		String moneda           = "";
		String monto            = "";
		String icUsuario        = "";
		String nombreUsuario    = "";
		String fechaAlerta      = "";
		String estatus          = "";

		if(tipo.equals("PDF")){
			try{
				ComunesPDF pdfDoc = new ComunesPDF();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(10,100);
				pdfDoc.setLCell("Bit�cora de Notificaciones por Duplicidad", "celda01", ComunesPDF.LEFT, 10);
				pdfDoc.setLCell("EPO",                    "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("PYME",                   "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Docto",              "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de emisi�n",       "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda",                 "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto",                  "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Usuario p�blico",        "celda01",ComunesPDF.CENTER); 
				pdfDoc.setLCell("Nombre usuario",         "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de alerta",        "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus de publicaci�n", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();

				countReg = 0;
				while (rs.next()){

					nombreEpo     = (rs.getString("EPO")                 == null) ? "" : rs.getString("EPO");
					nombrePyme    = (rs.getString("PYME")                == null) ? "" : rs.getString("PYME");
					numDocto      = (rs.getString("NO_DOCTO")            == null) ? "" : rs.getString("NO_DOCTO");
					fechaEmision  = (rs.getString("FECHA_EMISION")       == null) ? "" : rs.getString("FECHA_EMISION");
					moneda        = (rs.getString("MONEDA")              == null) ? "" : rs.getString("MONEDA");
					monto         = (rs.getString("MONTO")               == null) ? "" : rs.getString("MONTO");
					icUsuario     = (rs.getString("USUARIO_PUBLICO")     == null) ? "" : rs.getString("USUARIO_PUBLICO");
					nombreUsuario = (rs.getString("NOMBRE_USUARIO")      == null) ? "" : rs.getString("NOMBRE_USUARIO");
					fechaAlerta   = (rs.getString("FECHA_ALERTA")        == null) ? "" : rs.getString("FECHA_ALERTA");
					estatus       = (rs.getString("ESTATUS_PUBLICACION") == null) ? "" : rs.getString("ESTATUS_PUBLICACION");

					/***** Formato de cantidades *****/
					if(!monto.equals("")){
						monto = "$" + Comunes.formatoDecimal(monto, 2);
					}

					pdfDoc.setLCell(nombreEpo,     "formas", ComunesPDF.LEFT);
					pdfDoc.setLCell(nombrePyme,    "formas", ComunesPDF.LEFT);
					pdfDoc.setLCell(numDocto,      "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaEmision,  "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,        "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(monto,         "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(icUsuario,     "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreUsuario, "formas", ComunesPDF.LEFT);
					pdfDoc.setLCell(fechaAlerta,   "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,       "formas", ComunesPDF.CENTER);
					countReg++;
					//System.out.println("Registros procesados: " + countReg);
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF. ", e);
			}
		} else if(tipo.equals("CSV")){
			try{

				StringBuffer contenidoArchivo = new StringBuffer();
				contenidoArchivo.append("Bit�cora de Notificaciones por Duplicidad,");
				contenidoArchivo.append("\n");
				contenidoArchivo.append("EPO,");
				contenidoArchivo.append("PYME,");
				contenidoArchivo.append("No. Docto,");
				contenidoArchivo.append("Fecha de emisi�n,");
				contenidoArchivo.append("Moneda,");
				contenidoArchivo.append("Monto,");
				contenidoArchivo.append("Usuario p�blico,");
				contenidoArchivo.append("Nombre usuario,");
				contenidoArchivo.append("Fecha de alerta,");
				contenidoArchivo.append("Estatus de publicaci�n,");
				contenidoArchivo.append("\n");

				while (rs.next()){

					nombreEpo     = (rs.getString("EPO")                 == null) ? "" : rs.getString("EPO");
					nombrePyme    = (rs.getString("PYME")                == null) ? "" : rs.getString("PYME");
					numDocto      = (rs.getString("NO_DOCTO")            == null) ? "" : rs.getString("NO_DOCTO");
					fechaEmision  = (rs.getString("FECHA_EMISION")       == null) ? "" : rs.getString("FECHA_EMISION");
					moneda        = (rs.getString("MONEDA")              == null) ? "" : rs.getString("MONEDA");
					monto         = (rs.getString("MONTO")               == null) ? "" : rs.getString("MONTO");
					icUsuario     = (rs.getString("USUARIO_PUBLICO")     == null) ? "" : rs.getString("USUARIO_PUBLICO");
					nombreUsuario = (rs.getString("NOMBRE_USUARIO")      == null) ? "" : rs.getString("NOMBRE_USUARIO");
					fechaAlerta   = (rs.getString("FECHA_ALERTA")        == null) ? "" : rs.getString("FECHA_ALERTA");
					estatus       = (rs.getString("ESTATUS_PUBLICACION") == null) ? "" : rs.getString("ESTATUS_PUBLICACION");

					/***** Formato de cantidades *****/
					if(!monto.equals("")){
						monto = Comunes.formatoDecimal(monto, 2);
					}

					contenidoArchivo.append(nombreEpo.replace(',',' ')     +",");
					contenidoArchivo.append(nombrePyme.replace(',',' ')    +",");
					contenidoArchivo.append(numDocto.replace(',',' ')      +",");
					contenidoArchivo.append(fechaEmision.replace(',',' ')  +",");
					contenidoArchivo.append(moneda.replace(',',' ')        +",");
					contenidoArchivo.append(monto.replace(',',' ')         +",");
					contenidoArchivo.append(icUsuario.replace(',',' ')     +",");
					contenidoArchivo.append(nombreUsuario.replace(',',' ') +",");
					contenidoArchivo.append(fechaAlerta.replace(',',' ')   +",");
					contenidoArchivo.append(estatus.replace(',',' ')       +",");
					contenidoArchivo.append("\n");

				}

				if(!creaArchivo.make(contenidoArchivo.toString(), path, ".csv")){
					nombreArchivo = "ERROR";
				} else{
					nombreArchivo = creaArchivo.nombre;
				}

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo CSV. ", e);
			}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;

	}

	/**
	 * Genera los archivos DPF utilizando paginaci�n
	 * @return nombreArchivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){

		log.info("crearPageCustomFile (E)");
		HttpSession session     = request.getSession();
		CreaArchivo creaArchivo = new CreaArchivo();
		String nombreArchivo    = "";
		String nombreEpo        = "";
		String nombrePyme       = "";
		String numDocto         = "";
		String fechaEmision     = "";
		String moneda           = "";
		String monto            = "";
		String icUsuario        = "";
		String nombreUsuario    = "";
		String fechaAlerta      = "";
		String estatus          = "";

		if(tipo.equals("PDF")){
			try{
				ComunesPDF pdfDoc = new ComunesPDF();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(10,100);
				pdfDoc.setLCell("Bit�cora de Notificaciones por Duplicidad", "celda01", ComunesPDF.LEFT, 10);
				pdfDoc.setLCell("EPO",                    "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("PYME",                   "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Docto",              "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de emisi�n",       "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda",                 "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto",                  "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Usuario p�blico",        "celda01",ComunesPDF.CENTER); 
				pdfDoc.setLCell("Nombre usuario",         "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de alerta",        "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus de publicaci�n", "celda01",ComunesPDF.CENTER);

				while (rs.next()){

					nombreEpo     = (rs.getString("EPO")                 == null) ? "" : rs.getString("EPO");
					nombrePyme    = (rs.getString("PYME")                == null) ? "" : rs.getString("PYME");
					numDocto      = (rs.getString("NO_DOCTO")            == null) ? "" : rs.getString("NO_DOCTO");
					fechaEmision  = (rs.getString("FECHA_EMISION")       == null) ? "" : rs.getString("FECHA_EMISION");
					moneda        = (rs.getString("MONEDA")              == null) ? "" : rs.getString("MONEDA");
					monto         = (rs.getString("MONTO")               == null) ? "" : rs.getString("MONTO");
					icUsuario     = (rs.getString("USUARIO_PUBLICO")     == null) ? "" : rs.getString("USUARIO_PUBLICO");
					nombreUsuario = (rs.getString("NOMBRE_USUARIO")      == null) ? "" : rs.getString("NOMBRE_USUARIO");
					fechaAlerta   = (rs.getString("FECHA_ALERTA")        == null) ? "" : rs.getString("FECHA_ALERTA");
					estatus       = (rs.getString("ESTATUS_PUBLICACION") == null) ? "" : rs.getString("ESTATUS_PUBLICACION");

					/***** Formato de cantidades *****/
					if(!monto.equals("")){
						monto = "$" + Comunes.formatoDecimal(monto, 2);
					}

					pdfDoc.setLCell(nombreEpo,     "formas", ComunesPDF.LEFT);
					pdfDoc.setLCell(nombrePyme,    "formas", ComunesPDF.LEFT);
					pdfDoc.setLCell(numDocto,      "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaEmision,  "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,        "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(monto,         "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(icUsuario,     "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreUsuario, "formas", ComunesPDF.LEFT);
					pdfDoc.setLCell(fechaAlerta,   "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,       "formas", ComunesPDF.CENTER);
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF. ", e);
			}
		} else if(tipo.equals("CSV")){
			try{

				StringBuffer contenidoArchivo = new StringBuffer();
				contenidoArchivo.append("Bit�cora de Notificaciones por Duplicidad,");
				contenidoArchivo.append("\n");
				contenidoArchivo.append("EPO,");
				contenidoArchivo.append("PYME,");
				contenidoArchivo.append("No. Docto,");
				contenidoArchivo.append("Fecha de emisi�n,");
				contenidoArchivo.append("Moneda,");
				contenidoArchivo.append("Monto,");
				contenidoArchivo.append("Usuario p�blico,");
				contenidoArchivo.append("Nombre usuario,");
				contenidoArchivo.append("Fecha de alerta,");
				contenidoArchivo.append("Estatus de publicaci�n,");
				contenidoArchivo.append("\n");

				while (rs.next()){

					nombreEpo     = (rs.getString("EPO")                 == null) ? "" : rs.getString("EPO");
					nombrePyme    = (rs.getString("PYME")                == null) ? "" : rs.getString("PYME");
					numDocto      = (rs.getString("NO_DOCTO")            == null) ? "" : rs.getString("NO_DOCTO");
					fechaEmision  = (rs.getString("FECHA_EMISION")       == null) ? "" : rs.getString("FECHA_EMISION");
					moneda        = (rs.getString("MONEDA")              == null) ? "" : rs.getString("MONEDA");
					monto         = (rs.getString("MONTO")               == null) ? "" : rs.getString("MONTO");
					icUsuario     = (rs.getString("USUARIO_PUBLICO")     == null) ? "" : rs.getString("USUARIO_PUBLICO");
					nombreUsuario = (rs.getString("NOMBRE_USUARIO")      == null) ? "" : rs.getString("NOMBRE_USUARIO");
					fechaAlerta   = (rs.getString("FECHA_ALERTA")        == null) ? "" : rs.getString("FECHA_ALERTA");
					estatus       = (rs.getString("ESTATUS_PUBLICACION") == null) ? "" : rs.getString("ESTATUS_PUBLICACION");

					/***** Formato de cantidades *****/
					if(!monto.equals("")){
						monto = Comunes.formatoDecimal(monto, 2);
					}

					contenidoArchivo.append(nombreEpo.replace(',',' ')     +",");
					contenidoArchivo.append(nombrePyme.replace(',',' ')    +",");
					contenidoArchivo.append(numDocto.replace(',',' ')      +",");
					contenidoArchivo.append(fechaEmision.replace(',',' ')  +",");
					contenidoArchivo.append(moneda.replace(',',' ')        +",");
					contenidoArchivo.append(monto.replace(',',' ')         +",");
					contenidoArchivo.append(icUsuario.replace(',',' ')     +",");
					contenidoArchivo.append(nombreUsuario.replace(',',' ') +",");
					contenidoArchivo.append(fechaAlerta.replace(',',' ')   +",");
					contenidoArchivo.append(estatus.replace(',',' ')       +",");
					contenidoArchivo.append("\n");

				}

				if(!creaArchivo.make(contenidoArchivo.toString(), path, ".csv")){
					nombreArchivo = "ERROR";
				}	else{
					nombreArchivo = creaArchivo.nombre;
				}

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo CSV. ", e);
			}
		}
		return nombreArchivo;
	}

/************************************************************************
 *                          GETTERS AND SETTERS                         *
 ************************************************************************/
 	public List getConditions(){
		return conditions;
	}

	public String getIcEpo(){
		return icEpo;
	}

	public void setIcEpo(String icEpo){
		this.icEpo = icEpo;
	}

	public String getIcPyme(){
		return icPyme;
	}

	public void setIcPyme(String icPyme){
		this.icPyme = icPyme;
	}

	public String getNumDocto(){
		return numDocto;
	}

	public void setNumDocto(String numDocto){
		this.numDocto = numDocto;
	}

	public String getFechaInicio(){
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio){
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFinal(){
		return fechaFinal;
	}

	public void setFechaFinal(String fechaFinal){
		this.fechaFinal = fechaFinal;
	}

	public int getCountReg() {
		return countReg;
	}
}