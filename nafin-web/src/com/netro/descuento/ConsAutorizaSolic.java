package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsAutorizaSolic implements IQueryGeneratorRegExtJS  {
 
 //Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsAutorizaSolic.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_if;
	private String ic_epo;
	private String ic_folio;
	private String ic_moneda;
	private String ic_estatus_solic;
	private String cs_tipo_solicitud;
	private String pantalla;
	private String noSolicitudes;
	

	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
	
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	


	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		

	
		log.debug("qrySentencia---------------> "+qrySentencia);
		log.debug("conditions----------------->"+conditions);
		
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer clavesDocumentos 	= new StringBuffer();		

		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		StringBuffer qrySentenciaC 	= new StringBuffer();
		StringBuffer qrySentenciaE 	= new StringBuffer();
		
		if(pantalla.equals("Consulta"))  {
		
			if (cs_tipo_solicitud.equals("C") || cs_tipo_solicitud.equals("")) {
			
				qrySentenciaC.append(" SELECT " +
					" i.cg_razon_social AS NOMBRE_IF,  " +
					" e.cg_razon_social AS NOMBRE_EPO,  " +
					" p.cg_razon_social AS NOMBRE_PYME,     " +  
					"  s.ic_folio AS NUM_SOLICITUD,   " +
					"  m.cd_nombre AS MONEDA,  " +
					" m.ic_moneda as IC_MONEDA , "+
					" f.CG_NOMBRE AS   TIPO_FACTORAJE, " +
					" ds.in_importe_recibir  as MONTO_OPERAR , " +
					" es.cd_descripcion as ESTATUS_ACTUAL, " +
					" s.ic_estatus_solic as IC_ESTATUS_ACTUAL , " +
					" '3' as ESTATUS_ASIGNAR,   " +				
					" 'Interna' as ORIGEN ,  " +
					" '' as CAUSA_RECHAZO ,   " +
					" '' as NUMERO_PRESTAMO ,   " +
					" '' as FECHA_OPERACION , " +
					"	 DECODE (d.cs_dscto_especial,'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef),  0 ) as NETO_RECIBIR ,  " +
					" DECODE (d.cs_dscto_especial, 'D', ci.cg_razon_social, '')as BENEFICIARIO,  " +
					" DECODE (d.cs_dscto_especial,'D', d.fn_porc_beneficiario,0 )  as PORCEN_BENEFICIARIO ,  " +
					" DECODE (d.cs_dscto_especial,'D', ds.fn_importe_recibir_benef, 0) as IMPORTE_RECIBIR  " +					
					" FROM  com_solicitud s, com_documento d, com_docto_seleccionado ds"+
					" , comcat_moneda m, comcat_epo e, comcat_pyme p, comcat_if i"+
					" , comcat_estatus_solic es, comrel_pyme_epo pe"+
					" , comcat_if ci"+
					" , COMCAT_TIPO_FACTORAJE  f"+
					" WHERE  s.ic_documento=ds.ic_documento"+
					" and ds.ic_documento=d.ic_documento"+
						" and d.ic_moneda=m.ic_moneda"+
						" and d.ic_epo=e.ic_epo"+
						" and ds.ic_if=i.ic_if"+
						" and d.ic_pyme=p.ic_pyme"+
						" and s.ic_estatus_solic = es.ic_estatus_solic"+					
						" and s.cs_tipo_solicitud='C'"+
						" and d.ic_epo = pe.ic_epo and d.ic_pyme = pe.ic_pyme"+
						" and e.cs_habilitado='S'"+
						" and pe.cs_habilitado='S'"+
						" and i.cs_habilitado='S'"+
						" and d.ic_beneficiario = ci.ic_if(+)"+
						"  and f.CC_TIPO_FACTORAJE = d.CS_DSCTO_ESPECIAL ");
					
				if (!ic_estatus_solic.equals("")) {
					qrySentenciaC.append(" and s.ic_estatus_solic = ? ");
					conditions.add(ic_estatus_solic);	
				}
					
				if (!ic_if.equals("")) {
					qrySentenciaC.append(" and ds.ic_if = ? ");
					conditions.add(ic_if);
				} 
				if (!ic_epo.equals("")) {
					qrySentenciaC.append(" and ds.ic_epo = ? ");
					conditions.add(ic_epo);			
				}
	
				if (!ic_moneda.equals("")) {
					qrySentenciaC.append(" and d.ic_moneda = ? ");
					conditions.add(ic_moneda);			
				}
				if (!cs_tipo_solicitud.equals("")) {				
					qrySentenciaC.append(" and s.cs_tipo_solicitud = ? ");
					conditions.add(cs_tipo_solicitud);				
				}
				if (!ic_folio.equals("")) {
					qrySentenciaC.append(" and s.ic_folio in( "+ic_folio +")");	
				}
				if (ic_estatus_solic.equals("1")) {
					qrySentenciaC.append(" and s.ic_bloqueo = ? ");
					conditions.add(new Integer("1"));
					
				} else if (ic_estatus_solic.equals("2")) {
					qrySentenciaC.append(" and s.ic_bloqueo != ? ");
					conditions.add(new Integer("3"));
				}		
			}		
			
			if (cs_tipo_solicitud.equals("E") || cs_tipo_solicitud.equals("")) {	
			
				qrySentenciaE.append(" SELECT "+
						" (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social  ) AS NOMBRE_IF, "+
						" '(No Aplica)' as NOMBRE_EPO,  "+
						" (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' '|| p.cg_razon_social ) AS NOMBRE_PYME , "+
						" s.ic_folio as  NUM_SOLICITUD, "+
						" m.cd_nombre as MONEDA,   "+
						" m.ic_moneda as IC_MONEDA , "+
						" '' as TIPO_FACTORAJE ,  "+
						" 0 as MONTO_OPERAR ,  "+
						" es.cd_descripcion as ESTATUS_ACTUAL, " +
						" s.ic_estatus_solic as IC_ESTATUS_ACTUAL , " +
						" '3' as ESTATUS_ASIGNAR,   " +						
						" 'Externa' as ORIGEN ,  "+
						" '' as CAUSA_RECHAZO,  "+
						" '' as NUMERO_PRESTAMO, "+
						" '' as FECHA_OPERACION,  "+
						" 0 as NETO_RECIBIR , "+
						" '' as BENEFICIARIO ,"+
						" 0 as PORCEN_BENEFICIARIO , "+
						" 0 as IMPORTE_RECIBIR  "+
						" FROM  com_solicitud s, comcat_if i, com_docto_ext de, comcat_estatus_solic es"+
						" , comcat_pyme p, comcat_moneda m"+
						" WHERE  s.ic_folio = de.ic_folio"+
						" and de.ic_moneda = m.ic_moneda"+
						" and de.ic_if = i.ic_if"+
						" and de.ic_pyme = p.ic_pyme"+
						" and s.ic_estatus_solic = es.ic_estatus_solic"+									
						" and i.cs_habilitado='S'"+
						" and p.cs_habilitado='S'");
				
				if (!ic_estatus_solic.equals("")) {
					qrySentenciaE.append(" and s.ic_estatus_solic = ? ");
					conditions.add(ic_estatus_solic);	
				}
				
				if (!ic_if.equals("")) {
					qrySentenciaE.append(" and de.ic_if = ? ");
					conditions.add(ic_if);
				} 
				
				if (!ic_moneda.equals("")) {
					qrySentenciaE.append(" and de.ic_moneda = ? ");
					conditions.add(ic_moneda);
				}
				if (!cs_tipo_solicitud.equals("")) {				
					qrySentenciaE.append(" and s.cs_tipo_solicitud = ? ");
					conditions.add(cs_tipo_solicitud);
				}
				if (!ic_folio.equals("")) {
					qrySentenciaE.append(" and s.ic_folio in( "+ic_folio +")");				
				}
				
				if (ic_estatus_solic.equals("1")) {
					qrySentenciaE.append(" and s.ic_bloqueo = ? ");
					conditions.add(new Integer("1"));
					
				} else if (ic_estatus_solic.equals("2")) {
					qrySentenciaE.append(" and s.ic_bloqueo != ? ");
					conditions.add(new Integer("3"));
				}
			}
			
			if (cs_tipo_solicitud.equals("C")) {
				qrySentencia.append(qrySentenciaC);
			} else if (cs_tipo_solicitud.equals("E")) {
				qrySentencia.append(qrySentenciaE);
			} else {
				qrySentencia.append(qrySentenciaC+" UNION ALL "+qrySentenciaE);
			}
			
			
			
		}else if(pantalla.equals("Acuse"))  {
		
			qrySentencia.append(" SELECT " + 
					"  i.cg_razon_social as NOMBRE_IF, " + 
					" e.cg_razon_social as NOMBRE_EPO,  " + 
					" p.cg_razon_social as NOMBRE_PYME, " + 
					" s.ic_folio as NUM_SOLICITUD, " + 
					" d.ic_moneda as IC_MONEDA, " + 
					" m.cd_nombre AS MONEDA ,  " + 
					" f.CG_NOMBRE AS  TIPO_FACTORAJE,  " + 
					" ds.in_importe_recibir AS MONTO_OPERAR,    " + 
					" es.cd_descripcion AS ESTATUS_ACTUAL,  " + 
					" s.cg_causa_rechazo AS CAUSA_RECHAZO,  " + 
					" decode(d.cs_dscto_especial,'D', DS.in_importe_recibir - DS.fn_importe_recibir_benef,0) as NETO_RECIBIR, " + 
					" decode(d.cs_dscto_especial,'D',ci.cg_razon_social,'') as BENEFICIARIO, " + 
					" decode(d.cs_dscto_especial,'D', d.fn_porc_beneficiario,0) as PORCEN_BENEFICIARIO, " + 
					" decode(d.cs_dscto_especial,'D', DS.fn_importe_recibir_benef,0) as IMPORTE_RECIBIR  " +  					
					" FROM  com_solicitud s, com_documento d, com_docto_seleccionado ds " + 
					" , comcat_moneda m, comcat_epo e, comcat_pyme p, comcat_if i " + 
					" , comcat_estatus_solic es,comcat_if ci, COMCAT_TIPO_FACTORAJE  f  " + 
					" where s.ic_documento=ds.ic_documento  " + 
					" and ds.ic_documento=d.ic_documento  " + 
					" and d.ic_moneda=m.ic_moneda  " + 
					" and s.ic_estatus_solic=es.ic_estatus_solic  " + 
					" and d.ic_epo=e.ic_epo  " + 
					" and ds.ic_if=i.ic_if  " + 
					" and d.ic_pyme=p.ic_pyme  " + 
					" and s.cs_tipo_solicitud = 'C'  " + 
					"  and s.ic_folio in ("+noSolicitudes+")  " + 
					" and d.ic_beneficiario = ci.ic_if(+)  " + 
					" and f.CC_TIPO_FACTORAJE = d.CS_DSCTO_ESPECIAL   " + 
					
					"  UNION ALL  " + 
					
					" SELECT    " + 
					" i.cg_razon_social as NOMBRE_IF,  " + 
					"  '(No Aplica)' as NOMBRE_EPO,   " + 
					" p.cg_razon_social as NOMBRE_PYME,  " + 
					" s.ic_folio as NUM_SOLICITUD,  " + 
					" m.ic_moneda as IC_MONEDA,  " + 
					" m.cd_nombre AS MONEDA , " + 
					" '' AS   TIPO_FACTORAJE,  " + 
					" 0 AS MONTO_OPERAR,    " + 
					" es.cd_descripcion AS ESTATUS_ACTUAL,   " + 
					" s.cg_causa_rechazo AS CAUSA_RECHAZO,   " + 
					" 0 as NETO_RECIBIR,  " + 
					" '' as BENEFICIARIO,  " + 
					" 0 as PORCEN_BENEFICIARIO,  " + 
					" 0 as IMPORTE_RECIBIR      " + 
					" FROM com_solicitud s, comcat_if i, com_docto_ext de, comcat_estatus_solic es  " + 
					" , comcat_pyme p, comcat_moneda m " + 
					" where s.ic_folio = de.ic_folio  " + 
					" and de.ic_moneda = m.ic_moneda  " + 
					" and de.ic_if = i.ic_if  " + 
					" and de.ic_pyme = p.ic_pyme  " + 
					" and s.cs_tipo_solicitud = 'E'  " + 
					" and s.ic_estatus_solic = es.ic_estatus_solic  " + 
					" and s.ic_folio in ("+noSolicitudes+") ");
			
		}
		
		qrySentencia.append(" order by NOMBRE_IF, NOMBRE_EPO, NOMBRE_PYME ");
		
		log.info("qrySentencia  ------"+cs_tipo_solicitud+"----"+pantalla+"-----"+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int i=0;
		try {

		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
	}	
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int numRegistrosMN =0, numRegistrosDL=0;
		BigDecimal montoTotalMN = new BigDecimal("0.00");
		BigDecimal montoTotalDL = new BigDecimal("0.00");
		
		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
					
				pdfDoc.setTable(13, 100);
				pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�m. de Solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Estatus Solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Causa Rechazo","celda01",ComunesPDF.CENTER);			
				pdfDoc.setCell("Neto Recibir Pyme","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
				
				while (rs.next())	{		
					String nombreIF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
					String nombreEPO = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					String nombrePYME = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
					String numSolicitud = (rs.getString("NUM_SOLICITUD") == null) ? "" : rs.getString("NUM_SOLICITUD");
					String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					String tipoFatoraje = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
					String montoOperar = (rs.getString("MONTO_OPERAR") == null) ? "" : rs.getString("MONTO_OPERAR");
					String estatus = (rs.getString("ESTATUS_ACTUAL") == null) ? "" : rs.getString("ESTATUS_ACTUAL");
					String causaRechazo = (rs.getString("CAUSA_RECHAZO") == null) ? "" : rs.getString("CAUSA_RECHAZO");
					String netoRecibir = (rs.getString("NETO_RECIBIR") == null) ? "" : rs.getString("NETO_RECIBIR");
					String beneficiaroo = (rs.getString("BENEFICIARIO") == null) ? "" : rs.getString("BENEFICIARIO");
					String porBeneficiario = (rs.getString("PORCEN_BENEFICIARIO") == null) ? "" : rs.getString("PORCEN_BENEFICIARIO");
					String importeRecibir = (rs.getString("IMPORTE_RECIBIR") == null) ? "" : rs.getString("IMPORTE_RECIBIR");
					
					String claveMoneda = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");
											
					if (claveMoneda.equals("1")) { //moneda nacional
						numRegistrosMN++;
						if (!montoOperar.equals("")) {
							montoTotalMN = montoTotalMN.add(new BigDecimal(montoOperar));
						}				
					} else if(claveMoneda.equals("54")) {	//dolar
						numRegistrosDL++;
						if (!montoOperar.equals("")) {
							montoTotalDL = montoTotalDL.add(new BigDecimal(montoOperar));
						}
					}
			
					pdfDoc.setCell(nombreIF,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(nombreEPO,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(nombrePYME,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(numSolicitud,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoFatoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoOperar,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(causaRechazo,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(netoRecibir,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(beneficiaroo,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(porBeneficiario+"%","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeRecibir,2),"formas",ComunesPDF.RIGHT);
				}		
				
				pdfDoc.setCell("Numero de documentos en M.N.","celda01",ComunesPDF.RIGHT, 3);				
				pdfDoc.setCell(String.valueOf(numRegistrosMN),"formas",ComunesPDF.RIGHT);					
				pdfDoc.setCell("Total M.N.","celda01",ComunesPDF.RIGHT, 2);	
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalMN,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(" ","formas",ComunesPDF.RIGHT, 6 );
				
				pdfDoc.setCell("Numero de documentos en  D�lares","celda01",ComunesPDF.RIGHT, 3);				
				pdfDoc.setCell(String.valueOf(numRegistrosDL),"formas",ComunesPDF.RIGHT);				
				pdfDoc.setCell("Total USD","celda01",ComunesPDF.RIGHT, 2 );	
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalDL,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(" ","formas",ComunesPDF.RIGHT, 6 );
								
				pdfDoc.addTable();
				pdfDoc.endDocument();
							
	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		log.debug("crearCustomFile (S)");
		return nombreArchivo;
	}	


	/**
	 * metodo para cambiar el estatus de la solicitud 
	 * @return 
	 * @param fechaOperacion
	 * @param numPrestamo
	 * @param causaRechazo
	 * @param estatusAsignar
	 * @param noSolicitud
	 * @param numRegistros
	 */
	
	public String  cambioEstatus_solic(int  numRegistros,  String [] noSolicitud , String [] estatusAsignar, String [] causaRechazo, 	String [] numPrestamo , String [] fechaOperacion ){
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer("");		
		boolean transactionOk = true;
		List varBind = new ArrayList();	
		StringBuffer noSolicitudes = new StringBuffer("");
		
		try{	
			con.conexionDB();
						
			for (int i=0;i<numRegistros;i++)  {
			
				String ic_folio = noSolicitud[i];
				String ic_estatus_solic = estatusAsignar[i];
				String cg_causa_rechazo = causaRechazo[i];
				String ig_numero_prestamo = numPrestamo[i];
				String df_operacion = fechaOperacion[i];	
				noSolicitudes.append(ic_folio +",");
					
				qrySentencia = new StringBuffer("");
			
				if (ic_estatus_solic.equals("3")) {	 //solicitud Operada...
				
					qrySentencia.append("update com_solicitud"+
						" set ic_estatus_solic = ?,  " +
						" cg_causa_rechazo=NULL , "+						
						" df_operacion=TO_DATE('"+df_operacion+"','dd/mm/yyyy') , "+						
						" ig_numero_prestamo= ?  , "+
						" cg_tipo_busqueda = ?   "+
						" where ic_folio = ? ");
					varBind = new ArrayList();	
					varBind.add(new Integer(3));					
					varBind.add(ig_numero_prestamo);
					varBind.add("M");
					varBind.add(ic_folio);
					
				} else if (ic_estatus_solic.equals("4")) {  //solicitud Rechazada
				
					qrySentencia.append(" update com_solicitud "+
									 " set ic_estatus_solic= ? " + 
									 " ,cg_causa_rechazo= ? "+
									 " ,cg_tipo_busqueda = ? "+
									 " where ic_folio = ? ");
					varBind = new ArrayList();	
					varBind.add(new Integer(4));
					varBind.add(cg_causa_rechazo);				
					varBind.add("M");
					varBind.add(ic_folio);
				}	
				
				log.debug("qrySentencia.toString() ...............> " +qrySentencia.toString());   
				log.debug("varBind  ..................>" +varBind); 
				
				ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
				ps.executeUpdate();
				ps.close();
					
		} //fin del for
		
		noSolicitudes.deleteCharAt(noSolicitudes.length()-1);
					
	} catch(Exception e) {
		transactionOk=false;	
		noSolicitudes = new StringBuffer("");
		e.printStackTrace();
		log.error("error al Cancelaci�n de documentos  " +e); 
	} finally {	
		if(con.hayConexionAbierta()) {
			con.terminaTransaccion(transactionOk);
			con.cierraConexionDB();	
		}
	}    
		return noSolicitudes.toString();
	}	
	
	/**
	 * 
	 * @return 
	 * @param path
	 * @param registrosProcesados
	 * @param request
	 */
	public String impReporteOperacionesPDF(  HttpServletRequest request, Vector registrosProcesados, String path  ) {
		log.debug("impReporteOperacionesPDF (E)");		
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		AccesoDB con =new AccesoDB();
		boolean transactionOk = true;
		try {
			
			con.conexionDB();

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.addText("  ","formas",ComunesPDF.CENTER);
				pdfDoc.addText("  ","formas",ComunesPDF.CENTER);
				pdfDoc.addText("REPORTE DE OPERACIONES POR SOLICITUD  ","formasB",ComunesPDF.CENTER);
				pdfDoc.addText("  ","formas",ComunesPDF.CENTER);
				pdfDoc.addText("Fecha de Proceso: "+fechaActual+"  "+horaActual,"formas",ComunesPDF.LEFT);
			
			
				pdfDoc.setTable(11, 100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Folio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Clave Sirac","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Del Acreditado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("R.F.C","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Delg. Municipio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Domicilio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Colonia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("C.P.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);	
				
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Const.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Emisor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Clave Estrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Ventas Netas Totales","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Vtas. Netas Exp.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Clave / Actividad","celda01",ComunesPDF.CENTER,2);				
				pdfDoc.setCell("Principales Productos","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Clave Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("C","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Importe Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Clave Tipo De Credito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Esquema Amortizaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER,2);
				pdfDoc.setCell("FPAGCAP","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" FPAGINT","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				
				 
				pdfDoc.setCell("D","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa Int. U. Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Rel. Mat. U. Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Sobretasa U. Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa Int. Interm","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Rel. Mat. Interm","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Sobretasa Interm.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("FECHPPAGCAP","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("FECHPPAGINT","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("D�a De Pago","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Venc.","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("E","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Comunidad","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de docto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Solicitud TNUF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Neto Recibir PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Importe a Recibir del Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				
				int numRegistros = registrosProcesados.size();

				for (int i = 0; i < numRegistros; i++) {
					Vector registro = (Vector) registrosProcesados.get(i);
					
					String ic_folio = registro.get(0).toString();
					String in_numero_sirac = registro.get(1).toString();
					String cg_razon_social = registro.get(2).toString();
					String cg_rfc = registro.get(3).toString();
					String ifb = registro.get(4).toString();
					String estado = registro.get(5).toString();
					String cg_municipio = registro.get(6).toString();
					String domicilio = registro.get(7).toString();
					String cg_colonia = registro.get(8).toString();
					String cg_cp = registro.get(9).toString();
					String fecha_const = registro.get(10).toString();
					String estrato = registro.get(11).toString();
					String fn_ventas_net_tot = registro.get(12).toString();
					String fn_ventas_net_exp = registro.get(13).toString();
					String sector = registro.get(14).toString();
					String cg_productos = registro.get(15).toString();
					String moneda = registro.get(16).toString();
					String estatus = registro.get(17).toString();
					String fn_monto = registro.get(18).toString();
					String fn_monto_dscto = registro.get(19).toString();
					String tipo_credito = registro.get(20).toString();
					String esquema_amort = registro.get(21).toString();
					String plazo = registro.get(22).toString();
					String ppc = registro.get(23).toString();
					String ppi = registro.get(24).toString();
					String tasauf = registro.get(25).toString();
					String rmuf = registro.get(26).toString();
					String stuf = registro.get(27).toString();
					String tnuf = registro.get(28).toString();
					String fechappc = registro.get(29).toString();
					String fechappi = registro.get(30).toString();
					String in_dia_pago = registro.get(31).toString();
					String fechavenc = registro.get(32).toString();
					String tasaif = registro.get(33).toString();
					String cg_rmif = registro.get(34).toString();
					String in_stif = registro.get(35).toString();
					String ne_epo = registro.get(36).toString();
					String ne_pyme = registro.get(37).toString();
					String ig_numero_docto = registro.get(38).toString();
					String oficina = registro.get(39).toString();
					String funcionario = registro.get(41).toString();
					String in_numero_amort = registro.get(42).toString();
					String tabla_amort = registro.get(43).toString();
					String emisor = registro.get(44).toString();
					String descuentoEspecial = registro.get(45).toString();
					String netoRecibirPyme = registro.get(46).toString();
					String beneficiario = registro.get(47).toString();
					String porcBeneficiario = registro.get(48).toString();
					String importeRecibirBenef = registro.get(49).toString();
					
					String in_numero = "", fn_importe ="", amort_fecha ="";
					qrySentencia = new StringBuffer("");	
						qrySentencia.append(" SELECT  IN_NUMERO, FN_IMPORTE"+
					" , TO_CHAR(DF_FECHA,'DD/MM/YYYY') AMORT_FECHA "+
					" FROM COM_AMORTIZACION WHERE ic_folio = '"+ic_folio+"'");
		
					ResultSet rs  = con.queryDB(qrySentencia.toString());
					
					while(rs.next()) {
						in_numero = rs.getString("IN_NUMERO");
						fn_importe = rs.getString("FN_IMPORTE");
						amort_fecha = rs.getString("AMORT_FECHA");												
					}
					rs.close();
					con.cierraStatement();
					
					if(i>0){
						pdfDoc.setCell(" ","celda03",ComunesPDF.CENTER,11);	
					}					
					pdfDoc.setCell("Direcci�n Estatal ","celda01",ComunesPDF.LEFT, 2);
					pdfDoc.setCell(oficina,"formas",ComunesPDF.LEFT,9);
					pdfDoc.setCell("Intermediario Financiero ","celda01",ComunesPDF.LEFT, 2);
					pdfDoc.setCell(ifb,"formas",ComunesPDF.LEFT,9);
					
					pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ic_folio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_numero_sirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(cg_rfc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estado,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_municipio,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(domicilio,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(cg_colonia,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(cg_cp,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.LEFT);
					
					pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_const,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(emisor,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(estrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_ventas_net_tot,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_ventas_net_exp,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(sector,"formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell(cg_productos,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
									
					pdfDoc.setCell("C","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto_dscto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(tipo_credito,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(esquema_amort,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(plazo+" d�as","formas",ComunesPDF.CENTER,2);
					pdfDoc.setCell(ppc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ppi,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					
					
					pdfDoc.setCell("D","formas",ComunesPDF.CENTER);
					 if(descuentoEspecial.equals("V")) { 
						pdfDoc.setCell("Tasa Fact. Venc.","formas",ComunesPDF.CENTER);
					 }else { 
						pdfDoc.setCell(tasauf,"formas",ComunesPDF.CENTER);
					 }
					pdfDoc.setCell(rmuf,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(stuf,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tasaif,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_rmif,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_stif,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechappc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechappi,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_dia_pago,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechavenc,"formas",ComunesPDF.CENTER);
					
					pdfDoc.setCell("E","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ne_epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ne_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tnuf,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((descuentoEspecial.equals("D"))?"$ "+Comunes.formatoDecimal(netoRecibirPyme,2): " ","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(beneficiario,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(porcBeneficiario+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell((descuentoEspecial.equals("D"))?"$ "+Comunes.formatoDecimal(importeRecibirBenef,2): " ","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
				
					pdfDoc.setCell("Clave / Amortizaci�n  ","celda01",ComunesPDF.LEFT, 2);
					pdfDoc.setCell(tabla_amort,"formas",ComunesPDF.LEFT, 5);
					pdfDoc.setCell("No. Cuota  ","celda01",ComunesPDF.CENTER );
					pdfDoc.setCell("Fecha  ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe  ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
										
					pdfDoc.setCell("No. Amortizaci�n ","celda01",ComunesPDF.LEFT, 2);
					pdfDoc.setCell(in_numero_amort,"formas",ComunesPDF.LEFT, 5);
					pdfDoc.setCell(in_numero,"formas",ComunesPDF.CENTER );
					pdfDoc.setCell(amort_fecha,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_importe,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.LEFT);
									
				}//for
		
				pdfDoc.addTable();
				pdfDoc.endDocument();

		} catch(Throwable e) {
			transactionOk=false;	
			e.printStackTrace();
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {				
				
			} catch(Exception e) {}
				if(con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();	
			}  
		}
		
		log.debug("impReporteOperacionesPDF (S)");
		return nombreArchivo;
	}	
	
	/**
	 * 
	 * @return 
	 * @param path
	 * @param registrosProcesados
	 * @param request
	 */
	public String impDescargaArchivo(  HttpServletRequest request, Vector registrosProcesados, String path  ) {
		log.debug("impDescargaArchivo (E)");		
		String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer("");		
		boolean transactionOk = true;
		
		try {
			
			con.conexionDB();

			contenidoArchivo.append("folio solicitud,numero SIRAC,PYME,RFC,IF,Estado"+
				",Delg.,Domicilio,Colonia,C.P.,Fecha de constitucion,Estrato"+
				",Ventas Netas Totales,Ventas Netas de Exportacion,Actividad"+
				",Principales productos,Moneda,Tipo Factoraje,Monto,Monto a Operar,Tipo de credito"+
				",Esquema de amortizacion,Plazo,FPAGCAP,FPAGINT,Tasa Usuario Final"+
				",Relacion Matematica Usuario Final,Sobretasa Usuario Final,Tasa IF"+
				",Relacion Matematica IF,Sobretasa IF,FECHPPAGCAP,FECHPPAGINT"+
				",Dia de pago,Fecha Vencimiento,Numero Electronico EPO"+
				",Numero Electronico PYME,Numero de documento,Solicitud TNUF,Oficina"+
				",Funcionario,Emisor,Clave/Nombre"+
				",Neto a Recibir PyME, Beneficiario, % Beneficiario,Importe a Recibir Beneficiario,Amortizaciones\n");
				
			int numRegistros = registrosProcesados.size();

			for (int i = 0; i < numRegistros; i++) {
				Vector registro = (Vector) registrosProcesados.get(i);
				String folio = registro.get(0).toString();
				String numeroSirac = registro.get(1).toString();
				String nombrePyme = registro.get(2).toString();
				String rfc = registro.get(3).toString();
				String nombreIf = registro.get(4).toString();
				String estado = registro.get(5).toString();
				String delegacion = registro.get(6).toString();
				String domicilio = registro.get(7).toString();
				String colonia = registro.get(8).toString();
				String codigoPostal = registro.get(9).toString();
				String fechaConstitucion = registro.get(10).toString();
				String estrato = registro.get(11).toString();
				String ventasNetasTotales = registro.get(12).toString();
				String ventasNetasExportacion = registro.get(13).toString();
				String actividad = registro.get(14).toString();
				String principalesProductos = registro.get(15).toString();
				String nombreMoneda = registro.get(16).toString();
				String monto = registro.get(18).toString();
				String montoDescuento = registro.get(19).toString();
				String tipoCredito = registro.get(20).toString();
				String esquemaAmortizacion = registro.get(21).toString();
				String plazo = registro.get(22).toString();
				String periodoPagoCap = registro.get(23).toString();
				String periodoPagoInt = registro.get(24).toString();				
				String tasaUf = registro.get(25).toString();
				String relMatUf = registro.get(26).toString();
				String sobreTasaUf = registro.get(27).toString();
				String solicitudTNUF = registro.get(28).toString();
				String fechaPagoCap = registro.get(29).toString();
				String fechaPagoInt = registro.get(30).toString();
				String diaPago = registro.get(31).toString();
				String fechaVencimiento = registro.get(32).toString();
				String tasaIf = registro.get(33).toString();
				String relMatIf = registro.get(34).toString();
				String sobreTasaIf = registro.get(35).toString();
				String numElectronicoEpo = registro.get(36).toString();
				String numElectronicoPyme = registro.get(37).toString();
				String numeroDocumento = registro.get(38).toString();
				String oficina = registro.get(39).toString();
				String funcionario = registro.get(40).toString();
				String tablaAmortizacion = registro.get(43).toString();
				String emisor = registro.get(44).toString();
				String descuentoEspecial = registro.get(45).toString();
				String tipoFactoraje = (descuentoEspecial.equals("N"))?"Normal": (descuentoEspecial.equals("V"))?"Vencido" : (descuentoEspecial.equals("D"))?"Distribuido" : (descuentoEspecial.equals("A"))?"Autom�tico" :"";
				String netoRecibirPyme = registro.get(46).toString();
				String beneficiario = registro.get(47).toString();
				String porcBeneficiario = registro.get(48).toString();
				String importeRecibirBenef = registro.get(49).toString();
				
				qrySentencia = new StringBuffer("");	
				qrySentencia.append("select in_numero, fn_importe"+
					" , TO_CHAR(df_fecha, 'DD/MM/YYYY') DF_FECHA"+
					" from com_amortizacion"+
					" where ic_folio = '"+folio+"'"+
					" order by in_numero");
				rs = con.queryDB(qrySentencia.toString());
				String amortizaciones = "", importeAmortizacion ="", fechaAmortizacion ="", numeroAmortizacion = "";

				while(rs.next()) {
					numeroAmortizacion = rs.getString("IN_NUMERO");
					importeAmortizacion = rs.getString("FN_IMPORTE");
					fechaAmortizacion = rs.getString("DF_FECHA");
					if (amortizaciones.equals("")) {
						amortizaciones = numeroAmortizacion+",$"+Comunes.formatoDecimal(importeAmortizacion,2,false)+","+fechaAmortizacion;
					} else {
						amortizaciones = amortizaciones+","+numeroAmortizacion+",$"+Comunes.formatoDecimal(importeAmortizacion,2,false)+","+fechaAmortizacion;
					}
				}
				rs.close();
				con.cierraStatement();
					
				contenidoArchivo.append(folio+","+numeroSirac+
						","+nombrePyme.replace(',',' ')+","+rfc+","+nombreIf.replace(',',' ')+
						","+estado.replace(',',' ')+","+delegacion.replace(',',' ')+
						","+domicilio.replace(',',' ')+","+colonia.replace(',',' ')+
						","+codigoPostal+","+fechaConstitucion+","+estrato.replace(',',' ')+
						",$"+Comunes.formatoDecimal(ventasNetasTotales,2,false)+
						",$"+Comunes.formatoDecimal(ventasNetasExportacion,2,false)+
						","+actividad.replace(',',' ')+","+principalesProductos.replace(',',' ')+
						","+nombreMoneda+","+tipoFactoraje+",$"+Comunes.formatoDecimal(monto,2,false)+
						",$"+Comunes.formatoDecimal(montoDescuento,2,false)+","+tipoCredito+
						","+esquemaAmortizacion+","+plazo+","+periodoPagoCap+","+periodoPagoInt+
						","+tasaUf+","+relMatUf+","+sobreTasaUf+","+tasaIf+","+relMatIf+
						","+sobreTasaIf+","+fechaPagoCap+","+fechaPagoInt+","+diaPago+
						","+fechaVencimiento+","+numElectronicoEpo+","+numElectronicoPyme+
						","+numeroDocumento+","+solicitudTNUF+","+oficina.replace(',',' ')+
						","+funcionario.replace(',',' ')+","+emisor.replace(',',' ')+
						","+tablaAmortizacion.replace(',',' ')+
						","+netoRecibirPyme+","+beneficiario.replace(',',' ')+","+porcBeneficiario+","+importeRecibirBenef+
						","+amortizaciones.replace(',',' ')+"\n");					
			}
					
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
					
		} catch(Throwable e) {
			transactionOk=false;	
			e.printStackTrace();
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {				
				
			} catch(Exception e) {}
				if(con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();	
			}  
		}
		log.debug("impDescargaArchivo (S)");
		return nombreArchivo;
	}
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}	

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_folio() {
		return ic_folio;
	}

	public void setIc_folio(String ic_folio) {
		this.ic_folio = ic_folio;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getIc_estatus_solic() {
		return ic_estatus_solic;
	}

	public void setIc_estatus_solic(String ic_estatus_solic) {
		this.ic_estatus_solic = ic_estatus_solic;
	}

	public String getCs_tipo_solicitud() {
		return cs_tipo_solicitud;
	}

	public void setCs_tipo_solicitud(String cs_tipo_solicitud) {
		this.cs_tipo_solicitud = cs_tipo_solicitud;
	}

	public String getPantalla() {
		return pantalla;
	}

	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}

	public String getNoSolicitudes() {
		return noSolicitudes;
	}

	public void setNoSolicitudes(String noSolicitudes) {
		this.noSolicitudes = noSolicitudes;
	}

}



