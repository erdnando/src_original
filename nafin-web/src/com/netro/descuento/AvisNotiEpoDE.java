package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.FileInputStream;

import java.math.BigInteger;

import java.security.MessageDigest;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class AvisNotiEpoDE implements IQueryGeneratorPS, IQueryGeneratorRegExtJS {
	private int numList = 1;
	private ArrayList conditions = null;
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(AvisNotiEpoDE.class);

	public int getNumList(HttpServletRequest request){
		return numList;
	}

	public AvisNotiEpoDE() {	}

	public ArrayList getConditions(HttpServletRequest request){

		return this.conditions;
  }

	/**
	 * Metodo que genera el query para obtener totales de la consulta.
	 * Cualquier modificacion a este m�todo debe ser evaluada, para determinar que
	 * optimizaci�n aplica m�s, entrar por com_acuse3 o por com_documento, para
	 * que de esta manera se agregue el c�digo donde corresponda.
	 * En caso que ninguna de las dos aplique, ser� necesario crear una secci�n
	 * para el caso en particular.
	 * @param request Objeto HTTPServletRequest
	 * @return Cadena con la consulta del query para calculo de totales.
	 */
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		log.info("getAggregateCalculationQuery(E) ::..");
		conditions = new ArrayList();
		ArrayList conditionsNCM = new ArrayList();
		String sesIdiomaUsuario = (String)(request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario = (sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
		String envia = (request.getParameter("envia") == null)?"":request.getParameter("envia");
		String icIf = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
		String dfFechaNotMin = (request.getParameter("df_fecha_notMin") == null)?"":request.getParameter("df_fecha_notMin");
		String dfFechaNotMax = (request.getParameter("df_fecha_notMax") == null)?"":request.getParameter("df_fecha_notMax");
		String dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null)?"":request.getParameter("df_fecha_vencMin");
		String dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null)?"":request.getParameter("df_fecha_vencMax");
		String ccAcuse3 = (request.getParameter("cc_acuse3") == null)?"":request.getParameter("cc_acuse3");
		String fechaActual = (request.getParameter("FechaActual") == null)?"":request.getParameter("FechaActual");
		String strCampos = (request.getParameter("strCampos") == null)?"":request.getParameter("strCampos");
		String numero_siaff = (request.getParameter("numero_siaff") == null)?"":request.getParameter("numero_siaff");
		String clavePyme = request.getAttribute("clavePyme") == null?"":(String)request.getAttribute("clavePyme");//FODEA 028 - 2010 ACF
		String ic_documento = "";
		String ic_epo = "";

		if (!numero_siaff.equals("")) {
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);
			ic_epo = (String)numero.get("IC_EPO");
			ic_documento = (String)numero.get("IC_DOCUMENTO");
		}

		String operaNC = (String)request.getSession().getAttribute("operaNC");log.debug("..:: operaNC: "+operaNC);
		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");

		SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
		fechaActual = fecha2.format(new java.util.Date());
		String prefijoTablaJoin = "s.";
		//me traje la condici�n de todos los campos
		StringBuffer condicion = new StringBuffer();
		//HInts y orden de tablas de manera predeterminada:
		String hint = " /*+ use_nl(a3 s d m)*/ ";
		String tablas = "com_documento d, com_solicitud s, com_acuse3 a3, comcat_moneda m";
		String hintNC = "/*+use_nl(a3 s ds d m)*/ ";
		String tablasNC = "com_documento d, com_docto_seleccionado ds, com_solicitud s, com_acuse3 a3, comcat_moneda m";
		String hintNCM = " /*+ use_nl(a3 s d m nd) index(nd in_comrel_nota_docto_01_nuk)*/ ";//FODEA 012 - 2010 ACF
		String tablasNCM = "com_acuse3 a3, com_solicitud s, com_documento d, comcat_moneda m, comrel_nota_docto nd"; // com_docto_seleccionado ds,
		boolean hintPredeterminado = false;
		//------------- INICIO ---------- USO de hints/orden de tablas predeterminado
		//Fecha de Vencimiento
		if (!"".equals(dfFechaVencMin) && dfFechaVencMin != null) {
			if (!"".equals(dfFechaVencMax)) {
				condicion.append(
					" AND d.df_fecha_venc >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))"+
					" AND d.df_fecha_venc < TRUNC(TO_DATE(?, 'dd/mm/yyyy') + 1)");
				this.conditions.add(dfFechaVencMin);
				this.conditions.add(dfFechaVencMax);
				hintPredeterminado = true;
			}
		}
		//Intermediario Finaciero
		if (!"".equals(icIf) && icIf != null) {
			condicion.append(" AND d.ic_if = ?");
			this.conditions.add(new Integer(icIf));
			hintPredeterminado = true;
		}
		//Clave del documento obtenido a partir del N�mero SIAFF
		if(!ic_documento.equals("")){
			condicion.append(" AND d.ic_documento = ?");
			this.conditions.add(new Double(ic_documento));
			hintPredeterminado = true;
		}
    //No. de Proveedor //FODEA 028 - 2010 ACF (I)
		if(!clavePyme.equals("")){
			condicion.append(" AND d.ic_pyme = ?");
			this.conditions.add(new Integer(clavePyme));
			hintPredeterminado = true;
		}
    //FODEA 028 - 2010 ACF (I)
		//------------- FIN ---------- USO de hints/orden de tablas predeterminado

		if (hintPredeterminado == false) {
			//-------------------------------------------------------------------------
			//POR Razones de optimizacion, para las siguientes condiciones,
			//aplica otro orden de tablas y hints
			hint =
					" /*+ leading(a3) USE_NL(a3 s d m) */ ";
			tablas = "com_acuse3 a3, com_solicitud s, com_documento d, comcat_moneda m";
			hintNC = "/*+use_nl(a3 s ds d m) leading(a3) */ ";
			tablasNC = "com_acuse3 a3, com_solicitud s, com_documento d, com_docto_seleccionado ds, comcat_moneda m";
			//hintNCM = " /*+ leading(a3) USE_NL(a3 s s ds m nd) */ ";
      hintNCM = " /*+use_nl(a3 s d m nd) index(nd in_comrel_nota_docto_01_nuk)*/ ";//FODEA 012 - 2010 ACF
			tablasNCM = "com_acuse3 a3, com_solicitud s, com_documento d, comcat_moneda m, comrel_nota_docto nd";// com_docto_seleccionado ds,
		}
		//Fecha de Notificaci�n
		if (!"".equals(dfFechaNotMin) && dfFechaNotMin != null) {
			if (!"".equals(dfFechaNotMax)) {
				condicion.append(
					" AND a3.df_fecha_hora >= TO_DATE(?, 'dd/mm/yyyy')"+
					" AND a3.df_fecha_hora < TO_DATE(?, 'dd/mm/yyyy') + 1");
			    this.conditions.add(dfFechaNotMin);
					this.conditions.add(dfFechaNotMax);
			}
		}
		//Acuse de Notificaci�n
		if (!"".equals(ccAcuse3) && ccAcuse3 != null) {
			condicion.append(" AND a3.cc_acuse = ?");
			this.conditions.add(ccAcuse3);
		}
		//Si no se hay ning�n criterio de consulta, se toman las notificaciones del d�a
		if ("".equals(condicion.toString())) {
			condicion.append(
				" AND a3.df_fecha_hora >= TRUNC(SYSDATE)"+
				" AND a3.df_fecha_hora < TRUNC(SYSDATE + 1)");
		}
		//-------------------------------------------------------------------------
		String query_Reporte =
			" SELECT " + hint +
			" d.fn_monto AS fn_monto,"+
			" d.fn_monto_dscto AS fn_monto_dscto,"+
			" m.cd_nombre" + idioma + " AS cd_nombre,"+
			" d.ic_moneda"+
			" FROM " + tablas +
			" WHERE d.ic_moneda = m.ic_moneda"+
			" AND d.ic_documento = s.ic_documento"+
			" AND s.cc_acuse = a3.cc_acuse"+
			condicion +
			" AND a3.ic_producto_nafin = ? " +
			" AND d.ic_epo = ? "+
			" AND d.ic_estatus_docto in (4,11) ";

		this.conditions.add(new Integer(1));	//Descuento Electronico
		this.conditions.add(new Integer(iNoCliente));

		if ("S".equals(operaNC)) {
			query_Reporte =
			query_Reporte +
			" UNION ALL"+
			" SELECT " + hintNC +
			" d.fn_monto AS fn_monto, "+
			" d.fn_monto_dscto AS fn_monto_dscto, "+
			" m.cd_nombre" + idioma + " AS cd_nombre, "+
			" d.ic_moneda"+
			" FROM " + tablasNC +
			" WHERE d.ic_moneda = m.ic_moneda"+
			" AND d.ic_documento = ds.ic_documento"+
			" AND ds.ic_epo = d.ic_epo "+
			" AND d.ic_docto_asociado = s.ic_documento"+
			" AND s.cc_acuse = a3.cc_acuse"+
			condicion +
			" AND a3.ic_producto_nafin = ? " +
			" AND d.ic_epo = ? "+
			" AND d.ic_estatus_docto in (4,11) ";
			conditionsNCM.addAll(conditions);
			//Las condiciones de este query deben ser iguales a las del query de arriba, para que funcione la sig. linea
			this.conditions.addAll(conditions);
			// Fodea 002 - 2010
			query_Reporte =
			query_Reporte +
			" UNION ALL"+
			" SELECT " + hintNCM + " DISTINCT" +
			" d.fn_monto AS fn_monto,"+
			" d.fn_monto_dscto AS fn_monto_dscto,"+
			" m.cd_nombre" + idioma + " AS cd_nombre,"+
			" d.ic_moneda"+
			" FROM " + tablasNCM +
			" WHERE d.ic_moneda = m.ic_moneda"+
			" AND nd.ic_documento = s.ic_documento"+
			" AND nd.ic_nota_credito = d.ic_documento"+
			" AND s.cc_acuse = a3.cc_acuse"+
			condicion +
			" AND a3.ic_producto_nafin = ?"+
			" AND d.ic_epo = ?"+
			" AND d.ic_estatus_docto in (4,11) ";
			//Las condiciones de este query deben ser iguales a las del query de arriba, para que funcione la sig. linea
			this.conditions.addAll(conditionsNCM);
		}

		query_Reporte =
			" SELECT COUNT(1) AS result_size,"+
			" SUM(fn_monto) AS fn_monto,"+
			" SUM(fn_monto_dscto) AS fn_monto_dscto,"+
			" cd_nombre,"+
			" ic_moneda,"+
			" 'AvisNotiEpoDE::getAggregateCalculationQuery' AS pantalla"+
			" FROM (" + query_Reporte + ")"+
			" GROUP BY ic_moneda, cd_nombre"+
			" ORDER BY ic_moneda";

		log.debug("query_Reporte: "+query_Reporte);
		log.debug("conditions: "+this.conditions);
		log.info("getAggregateCalculationQuery(S) ::..");
		return query_Reporte;
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		String strCampos    = (request.getParameter("strCampos")     == null) ? "" : request.getParameter("strCampos");

		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
		String operaNC = (String)request.getSession().getAttribute("operaNC");
		StringBuffer query = new StringBuffer();
		StringBuffer clavesDocumentos = new StringBuffer();

		String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";

		this.numList = 1;
		for(int x = 0;x<ids.size();x++ ) {
          clavesDocumentos.append("?,");
      }
		clavesDocumentos = clavesDocumentos.delete(clavesDocumentos.length()-1, clavesDocumentos.length());

		query.append(
			"SELECT /*+ use_nl(d ds s a3 pe p i m ie i2) */ (decode (I.cs_habilitado,'N','*','S',' ')||' '||I3.cg_razon_social), "+
			"	to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, "+
			"	A3.cc_acuse, " +
			"	(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social), "+
			"	D.ig_numero_docto, "+
			"	to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, " +
			"	to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, "+
			"   M.cd_nombre"+idioma+ " as cd_nombre, " +
			"	D.fn_monto, "+
			"	D.fn_porc_anticipo, "+
			"	D.fn_monto_dscto, " +
			"	IE.ig_sucursal, "+
			"	IE.cg_banco, "+
			"	IE.cg_num_cuenta, "+
			"	PE.cg_pyme_epo_interno, "+
			"	D.ic_moneda, " +
			"	D.ic_documento, " +
			"	D.cs_detalle, " +
			"	D.CT_REFERENCIA " +
			strCampos +
			"	, I2.cg_razon_social as beneficiario " +
			"	, d.fn_porc_beneficiario as por_beneficiario " +
			"	, DS.fn_importe_recibir_benef as importe_a_recibir " +
			"	, DS.in_tasa_aceptada, d.cs_dscto_especial, '' as doctoasociado " +
			"	,to_char(D.df_fecha_venc_pyme, 'DD/MM/YYYY') df_fecha_venc_pyme "+

			" ,TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO " +
			" , tf.cg_nombre AS TIPO_FACTORAJE "+
			" ,'AvisNotiEpoDE::getDocumentSummaryQueryForIds' AS PANTALLA "+
			" FROM COM_DOCUMENTO D, "+
			"	COM_DOCTO_SELECCIONADO DS, "+
			"	COMCAT_IF I, "+
			"	COMREL_PYME_EPO PE, " +
			"	COM_ACUSE3 A3, "+
			"	COMCAT_PYME P, "+
			"	COMCAT_MONEDA M, "+
			"	COMREL_IF_EPO IE, "+
			"	COM_SOLICITUD S, " +
			"	comcat_if I2 ," +
			"	comcat_if I3 ," +
			"  comcat_tipo_factoraje tf " +
			"WHERE D.ic_documento = DS.ic_documento " +
			" AND DS.ic_documento = S.ic_documento " +
			" AND S.cc_acuse = A3.cc_acuse " +
			" AND D.ic_if = I3.ic_if " +
			" AND DS.ic_if = IE.ic_if " +			
			" AND IE.ic_if = I.ic_if " +
			" AND IE.ic_epo = PE.ic_epo "+
			" AND P.ic_pyme = PE.ic_pyme "+
			" AND D.ic_pyme = P.ic_pyme " +
 			" AND D.ic_epo = PE.ic_epo " +
			" AND d.ic_beneficiario=I2.ic_if(+) " +
			" AND D.ic_moneda = M.ic_moneda " +
			" AND D.cs_dscto_especial = tf.cc_tipo_factoraje "+
			" AND d.ic_estatus_docto in (4,11) "+
			" AND D.ic_documento in ("+clavesDocumentos+")");

		if("S".equals(operaNC)) {
			this.numList = 3; // Anterior 2
			query.append(
				" UNION ALL " +
				"SELECT /*+ use_nl(d ds s a3 pe p i m ie i2) */ " +
				"  distinct " +
				"  (decode (I.cs_habilitado,'N','*','S',' ')||' '||I3.cg_razon_social), "+
				"	to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, "+
				"	A3.cc_acuse, " +
				"	(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social), "+
				"	D.ig_numero_docto, "+
				"	to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, " +
				"	to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, "+
				"   M.cd_nombre"+idioma+ " as cd_nombre, " +
				"	D.fn_monto, "+
				"	D.fn_porc_anticipo, "+
				"	D.fn_monto_dscto, " +
				"	IE.ig_sucursal, "+
				"	IE.cg_banco, "+
				"	IE.cg_num_cuenta, "+
				"	PE.cg_pyme_epo_interno, "+
				"	D.ic_moneda, " +
				"	D.ic_documento, " +
				"	D.cs_detalle, " +
				"	D.CT_REFERENCIA " +
				strCampos +
				"	, I2.cg_razon_social as beneficiario " +
				"	, d.fn_porc_beneficiario as por_beneficiario " +
				"	, DS.fn_importe_recibir_benef as importe_a_recibir " +
				"	, DS.in_tasa_aceptada, d.cs_dscto_especial, d2.ig_numero_docto as doctoasociado " +
				"	,to_char(D.df_fecha_venc_pyme, 'DD/MM/YYYY') df_fecha_venc_pyme "+
				" ,TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO " +
				" , tf.cg_nombre AS TIPO_FACTORAJE "+
				" ,'AvisNotiEpoDE::getDocumentSummaryQueryForIds' AS PANTALLA "+
				" FROM COM_DOCUMENTO D, "+
				" COM_DOCUMENTO D2, "+
				"	COM_DOCTO_SELECCIONADO DS, "+
				"	COMCAT_IF I, "+
				"	COMREL_PYME_EPO PE, " +
				"	COM_ACUSE3 A3, "+
				"	COMCAT_PYME P, "+
				"	COMCAT_MONEDA M, "+
				"	COMREL_IF_EPO IE, "+
				"	COM_SOLICITUD S, " +
				"	comcat_if I2 , " +
				"	comcat_if I3 , " +
				"  comcat_tipo_factoraje tf "+
				"WHERE D.ic_documento = DS.ic_documento " +
				" AND d.ic_docto_asociado = d2.ic_documento "+
				" AND D.ic_docto_asociado = S.ic_documento " +
				" AND S.cc_acuse = A3.cc_acuse " +
				" AND D.ic_if = I3.ic_if " +
				" AND DS.ic_if = IE.ic_if " +
				" AND IE.ic_if = I.ic_if " +
				" AND IE.ic_epo = PE.ic_epo "+
				" AND P.ic_pyme = PE.ic_pyme "+
				" AND D.ic_pyme = P.ic_pyme " +
 				" AND D.ic_epo = PE.ic_epo " +
				" AND d.ic_beneficiario=I2.ic_if(+) " +
				" AND D.ic_moneda = M.ic_moneda " +
				" AND D.cs_dscto_especial = tf.cc_tipo_factoraje "+
				" AND d.ic_estatus_docto in (4,11) "+
				" AND D.ic_documento in ("+clavesDocumentos+")");
			// Fodea 002 - 2010. Notas de Credito Multiple
			query.append(
				" UNION ALL " +
				"SELECT /*+ use_nl(d ds s a3 pe p i m ie i2 nd) */  " +
				"  distinct " +
				"  (decode (I.cs_habilitado,'N','*','S',' ')||' '||I3.cg_razon_social), "+
				"	to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, "+
				"	A3.cc_acuse, " +
				"	(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social), "+
				"	D.ig_numero_docto, "+
				"	to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, " +
				"	to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, "+
				"   M.cd_nombre"+idioma+ " as cd_nombre, " +
				"	D.fn_monto, "+
				"	D.fn_porc_anticipo, "+
				"	D.fn_monto_dscto, " +
				"	IE.ig_sucursal, "+
				"	IE.cg_banco, "+
				"	IE.cg_num_cuenta, "+
				"	PE.cg_pyme_epo_interno, "+
				"	D.ic_moneda, " +
				"	D.ic_documento, " +
				"	D.cs_detalle, " +
				"	D.CT_REFERENCIA " +
				strCampos +
				"	, I2.cg_razon_social as beneficiario " +
				"	, d.fn_porc_beneficiario as por_beneficiario " +
				"	, DS.fn_importe_recibir_benef as importe_a_recibir " +
				"	, DS.in_tasa_aceptada, d.cs_dscto_especial, '' as doctoasociado " +
				"	,to_char(D.df_fecha_venc_pyme, 'DD/MM/YYYY') df_fecha_venc_pyme "+
				" ,TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO " +
				" , tf.cg_nombre AS TIPO_FACTORAJE "+
				" ,'AvisNotiEpoDE::getDocumentSummaryQueryForIds' AS PANTALLA "+
				" FROM COM_DOCUMENTO D, "+
				"	COM_DOCTO_SELECCIONADO DS, "+
				"	COMCAT_IF I, "+
				"	COMREL_PYME_EPO PE, " +
				"	COM_ACUSE3 A3, "+
				"	COMCAT_PYME P, "+
				"	COMCAT_MONEDA M, "+
				"	COMREL_IF_EPO IE, "+
				"	COM_SOLICITUD S, " +
				"	comcat_if I2 , " +
				"	comcat_if I3 , " +
				"  comcat_tipo_factoraje tf, "+
				"  comrel_nota_docto nd " +
				"WHERE D.ic_documento = DS.ic_documento " +
				" AND nd.ic_documento     = s.ic_documento "  +
				" AND nd.ic_nota_credito  = d.ic_documento "  +
				" AND S.cc_acuse = A3.cc_acuse " +
				" AND D.ic_if = I3.ic_if " +
				" AND DS.ic_if = IE.ic_if " +
				" AND IE.ic_if = I.ic_if " +
				" AND IE.ic_epo = PE.ic_epo "+
				" AND P.ic_pyme = PE.ic_pyme "+
				" AND D.ic_pyme = P.ic_pyme " +
 				" AND D.ic_epo = PE.ic_epo " +
				" AND d.ic_beneficiario=I2.ic_if(+) " +
				" AND D.ic_moneda = M.ic_moneda " +
				" AND D.cs_dscto_especial = tf.cc_tipo_factoraje "+
				" AND d.ic_estatus_docto in (4,11) "+
				" AND D.ic_documento in ("+clavesDocumentos+")");

		}

		log.debug("query: "+query.toString());
		log.debug("conditions::"+this.conditions);
		log.info("getDocumentSummaryQueryForIds(S) ::..");
		return query.toString();
	}

	/**
	 * Metodo que genera el query para obtener las llaves primarias para
	 * la paginacion en la consulta.
	 * Cualquier modificacion a este m�todo debe ser evaluada, para determinar que
	 * optimizaci�n aplica m�s, entrar por com_acuse3 o por com_documento, para
	 * que de esta manera se agregue el c�digo donde corresponda.
	 * En caso que ninguna de las dos aplique, ser� necesario crear una secci�n
	 * para el caso en particular.
	 * @param request Objeto HTTP Request
	 * @return Cadena con la consulta del query para generacion de claves.
	 */
	public String getDocumentQuery(HttpServletRequest request) {
		log.info("getDocumentQuery(E) ::..");
		this.conditions = new ArrayList();
		ArrayList conditionsNCM = new ArrayList();

		String envia = (request.getParameter("envia") == null)?"":request.getParameter("envia"),
		icIf = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if"),
		dfFechaNotMin = (request.getParameter("df_fecha_notMin") == null)?"":request.getParameter("df_fecha_notMin"),
		dfFechaNotMax = (request.getParameter("df_fecha_notMax") == null)?"":request.getParameter("df_fecha_notMax"),
		dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null)?"":request.getParameter("df_fecha_vencMin"),
		dfFechaVencMax = (request.getParameter("df_fecha_vencMax") == null)?"":request.getParameter("df_fecha_vencMax"),
		ccAcuse3 = (request.getParameter("cc_acuse3") == null)?"":request.getParameter("cc_acuse3"),
		fechaActual = (request.getParameter("FechaActual") ==null)?"":request.getParameter("FechaActual"),
		strCampos = (request.getParameter("strCampos") == null)?"":request.getParameter("strCampos"),
		numero_siaff = (request.getParameter("numero_siaff") == null)?"":request.getParameter("numero_siaff");
		String clavePyme = request.getAttribute("clavePyme") == null?"":(String)request.getAttribute("clavePyme");//FODEA 028 - 2010 ACF

		String ic_documento = "";
		String ic_epo = "";

		if (!numero_siaff.equals("")) {
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);
			ic_epo = (String)numero.get("IC_EPO");
			ic_documento = (String)numero.get("IC_DOCUMENTO");
		}

	  // No traigo strCampos porque son para el detalle no lo necesito aqui
		String sesIdiomaUsuario = (String)(request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";
		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
		String operaNC = (String)request.getSession().getAttribute("operaNC");

		SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
		fechaActual = fecha2.format(new java.util.Date());

		StringBuffer condicion = new StringBuffer();

		//HInts y orden de tablas de manera predeterminada:
		String hint = " /*+ use_nl(a3 s d)*/ ";
		String tablas = " com_documento d, com_solicitud s, com_acuse3 a3 ";
		String hintNC = " /*+ USE_NL(a3 s ds d) */ ";
		String tablasNC = " com_documento d, com_docto_seleccionado ds, com_solicitud s, com_acuse3 a3 ";
		String hintNCM = " /*+ use_nl(a3 s d nd) index(nd in_comrel_nota_docto_01_nuk)*/ ";//FODEA 012 - 2010 ACF
		String tablasNCM = " com_acuse3 a3, com_solicitud s, com_documento d, comrel_nota_docto nd ";//com_docto_seleccionado ds,
		boolean hintPredeterminado = false;
		//------------- INICIO ---------- USO de hints/orden de tablas predeterminado
		if(!"".equals(dfFechaVencMin) && dfFechaVencMin != null) {
			if(!"".equals(dfFechaVencMax)) {
				condicion.append(
					" and d.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') "+
					" and d.df_fecha_venc < TO_DATE(?,'dd/mm/yyyy')+1 ");
				this.conditions.add(dfFechaVencMin);
				this.conditions.add(dfFechaVencMax);
				hintPredeterminado = true;
			}
		}

		if (!"".equals(icIf) && icIf != null){
			condicion.append(" AND d.ic_if = ? ");
			this.conditions.add(new Integer (icIf));
			hintPredeterminado = true;
		}

		if(!ic_documento.equals("")){
			condicion.append(" AND d.ic_documento = ? ");
			this.conditions.add(new Double(ic_documento));
			hintPredeterminado = true;
		}
    //FODEA 028 - 2010 ACF (I)
		if(!clavePyme.equals("")){
			condicion.append(" AND d.ic_pyme = ? ");
			this.conditions.add(new Integer(clavePyme));
			hintPredeterminado = true;
		}
    //FODEA 028 - 2010 ACF (I)
		//------------- FIN ---------- USO de hints/orden de tablas predeterminado

		if (hintPredeterminado == false) {

			//-------------------------------------------------------------------------
			//POR Razones de optimizacion, para las siguientes condiciones,
			//aplica otro orden de tablas y hints
			hint =
					" /*+ leading(a3) USE_NL(a3 s d) */ ";
			tablas = "com_acuse3 a3, com_solicitud s, com_documento d";
			hintNC = " /*+ leading(a3) USE_NL(a3 s ds d) */ ";
			tablasNC = "com_acuse3 a3, com_solicitud s,  com_documento d, com_docto_seleccionado ds";
			// Fodea 002 - 2010
			hintNCM = " /*+ use_nl(a3 s d nd) index(nd in_comrel_nota_docto_01_nuk)*/ ";//FODEA 012 - 2010 ACF
			tablasNCM = "com_acuse3 a3, com_solicitud s,  com_documento d, comrel_nota_docto nd "; // com_docto_seleccionado ds,
		}

		if(!"".equals(dfFechaNotMin) && dfFechaNotMin != null) {
			if(!"".equals(dfFechaNotMax)) {
				condicion.append(
					" and a3.df_fecha_hora >= TO_DATE(?,'dd/mm/yyyy') "+
					" and a3.df_fecha_hora < TO_DATE(?,'dd/mm/yyyy')+1 ");
					this.conditions.add(dfFechaNotMin);
					this.conditions.add(dfFechaNotMax);
			}
		}

		if (!"".equals(ccAcuse3) && ccAcuse3 != null){
			condicion.append(" AND a3.cc_acuse = ? ");
			this.conditions.add(ccAcuse3);
		}

		if("".equals(condicion.toString())) {
			condicion.append(
				" and a3.df_fecha_hora >= TRUNC(SYSDATE) "+
				" and a3.df_fecha_hora < TRUNC(SYSDATE+1) ");
		}

		//-------------------------------------------------------------------------


		String query_Reporte =
			" SELECT   "+hint+
			"        d.ic_documento, 'AvisNotiEpoDE::getDocumentQuery' AS pantalla"   +
			"   FROM " + tablas +
			"  WHERE d.ic_documento = s.ic_documento"   +
			"    AND s.cc_acuse = a3.cc_acuse"  +
			 condicion +
			"    AND a3.ic_producto_nafin = ? "  +
			"    AND D.ic_epo = ? "+
			"  AND d.ic_estatus_docto in (4,11) ";

		this.conditions.add(new Integer(1));	//1= Descuento Electronico
		this.conditions.add(new Integer(iNoCliente));

		if("S".equals(operaNC)) {

			query_Reporte =
				query_Reporte +
				" UNION ALL "+
				"SELECT  " + hintNC +
				" D.ic_documento"+
				" , 'AvisNotiEpoDE::getDocumentQuery' AS PANTALLA "+
				" FROM " + tablasNC +
				" WHERE d.ic_documento = ds.ic_documento " +
				" AND d.ic_docto_asociado = s.ic_documento " +
				" AND s.cc_acuse = a3.cc_acuse " +
				condicion +
				" AND a3.ic_producto_nafin = ? "  +
				" AND D.ic_epo = ? "+
				" AND d.ic_estatus_docto in (4,11) ";
				
				conditionsNCM.addAll(conditions);// Fodea 002 - 2010. Nota de Credito Multiple
				//Para que la sig instruccion se VALIDA, es necesario que sean las mismas condiciones que el query de arriba.
				this.conditions.addAll(conditions);

			// Fodea 002 - 2010. Mostrar Notas de Credito Multiple
			query_Reporte =
				query_Reporte +
				" UNION ALL "+
				"SELECT  " + hintNCM +
				"  distinct "+
				" D.ic_documento"+
				" , 'AvisNotiEpoDE::getDocumentQuery' AS PANTALLA "+
				" FROM " + tablasNCM +
				" WHERE " +
				//" d.ic_documento 		= ds.ic_documento AND " +
				"     nd.ic_documento     	= s.ic_documento " +
				" AND nd.ic_nota_credito  	= d.ic_documento " +
				" AND s.cc_acuse = a3.cc_acuse " +
				condicion +
				" AND a3.ic_producto_nafin = ? "  +
				" AND D.ic_epo = ? "+
				" AND d.ic_estatus_docto in (4,11) ";
				//Para que la sig instruccion se VALIDA, es necesario que sean las mismas condiciones que el query de arriba.
				this.conditions.addAll(conditionsNCM);
		}

		log.debug("query_Reporte: "+query_Reporte);
		log.debug("conditions::"+this.conditions);
		log.info("getDocumentQuery(S) ::..");
		return query_Reporte;
	}

	/**
	 * Metodo que genera el query para obtener un archivo de la consulta.
	 * Cualquier modificacion a este m�todo debe ser evaluada, para determinar que
	 * optimizaci�n aplica m�s, entrar por com_acuse3 o por com_documento, para
	 * que de esta manera se agregue el c�digo donde corresponda.
	 * En caso que ninguna de las dos aplique, ser� necesario crear una secci�n
	 * para el caso en particular.
	 * Para notas de credito y notas de credito multiple, se hace muy ineficiente
	 * el acceso por acuse 3, por lo que lleva otra logica... el acceso se hace
	 * por com_documento...
	 * @param request Objeto HTTP Request
	 * @return Cadena con la consulta del query para generacion de archivo.
	 */
	public String getDocumentQueryFile(HttpServletRequest request) {
		log.info("getDocumentQueryFile(E) ::..");
		conditions = new ArrayList();
		ArrayList conditionsNCM = new ArrayList();
		String	envia          = (request.getParameter("envia")           == null) ? "" : request.getParameter("envia"),
					icIf           = (request.getParameter("ic_if")           == null) ? "" : request.getParameter("ic_if"),
					dfFechaNotMin  = (request.getParameter("df_fecha_notMin") == null) ? "" : request.getParameter("df_fecha_notMin"),
					dfFechaNotMax  = (request.getParameter("df_fecha_notMax") == null) ? "" : request.getParameter("df_fecha_notMax"),
					dfFechaVencMin = (request.getParameter("df_fecha_vencMin") == null) ? "" : request.getParameter("df_fecha_vencMin"),
					dfFechaVencMax		= (request.getParameter("df_fecha_vencMax") == null) ? "" : request.getParameter("df_fecha_vencMax"),
					ccAcuse3       = (request.getParameter("cc_acuse3")       == null) ? "" : request.getParameter("cc_acuse3"),
					fechaActual    = (request.getParameter("FechaActual")     == null) ? "" : request.getParameter("FechaActual"),
					strCampos    	= (request.getParameter("strCampos")     == null) ? "" : request.getParameter("strCampos");
		String numero_siaff 	= (request.getParameter("numero_siaff")     == null) ? "" : request.getParameter("numero_siaff");
		String clavePyme = request.getAttribute("clavePyme")==null?"":(String)request.getAttribute("clavePyme");//FODEA 028 - 2010 ACF

		String ic_documento = "";
		String ic_epo = "";

		if(!numero_siaff.equals("")){
			HashMap numero = this.getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 			= (String) numero.get("IC_EPO");
			ic_documento 	= (String) numero.get("IC_DOCUMENTO");
		}
		String prefijoTablaJoin = "s.";
		String prefijoJoins = "";
		String sesIdiomaUsuario = (String) (request.getSession().getAttribute("sesIdiomaUsuario"));
		sesIdiomaUsuario=(sesIdiomaUsuario == null)?"ES":sesIdiomaUsuario;
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":"";

		String	operaNC 	= (String)request.getSession().getAttribute("operaNC");
		String	iNoCliente 	= (String)request.getSession().getAttribute("iNoCliente");

		SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
		fechaActual = fecha2.format(new java.util.Date());

		StringBuffer sbCondicionAdic = new StringBuffer(" "),
		sbColumnasNC = new StringBuffer(
			" (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social), "+
			"	to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, "+
			"	A3.cc_acuse, " +
			"	(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social), "+
			"	D.ig_numero_docto, "+
			"	to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, " +
			"	to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, "+
			"   M.cd_nombre"+idioma+ " as cd_nombre, " +
			"	D.fn_monto, "+
			"	D.fn_porc_anticipo, "+
			"	D.fn_monto_dscto, " +
			"	IE.ig_sucursal, "+
			"	IE.cg_banco, "+
			"	IE.cg_num_cuenta, "+
			"	PE.cg_pyme_epo_interno, "+
			"	D.ic_moneda, " +
			"	D.ic_documento, " +
			"	D.cs_detalle, " +
			"	D.CT_REFERENCIA " +
			strCampos +
			"	, I2.cg_razon_social as beneficiario " +
			"	, d.fn_porc_beneficiario as por_beneficiario " +
			"	, DS.fn_importe_recibir_benef as importe_a_recibir " +
			"	, DS.in_tasa_aceptada " +
			"	, replace(I.cg_rfc, '-', '') as CG_RFC_IF, d.cs_dscto_especial, (select d2.ig_numero_docto from com_documento d2 where d2.ic_documento = d.ic_docto_asociado) as doctoasociado" +
			"	, D.fn_monto_ant "+
			"	, to_char(D.df_fecha_venc_pyme, 'DD/MM/YYYY') df_fecha_venc_pyme "+
			" , TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO " +
			" , tf.cg_nombre AS TIPO_FACTORAJE "+
			"   , 'AvisNotiEpoDE::getDocumentQueryFile' AS PANTALLA,IE.CG_CUENTA_CLABE cuentaClabe "+
			"   FROM ");

		StringBuffer sbColumnas = new StringBuffer(
			" (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social), "+
			"	to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, "+
			"	A3.cc_acuse, " +
			"	(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social), "+
			"	D.ig_numero_docto, "+
			"	to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, " +
			"	to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, "+
			"   M.cd_nombre"+idioma+ " as cd_nombre, " +
			"	D.fn_monto, "+
			"	D.fn_porc_anticipo, "+
			"	D.fn_monto_dscto, " +
			"	IE.ig_sucursal, "+
			"	IE.cg_banco, "+
			"	IE.cg_num_cuenta, "+
			"	PE.cg_pyme_epo_interno, "+
			"	D.ic_moneda, " +
			"	D.ic_documento, " +
			"	D.cs_detalle, " +
			"	D.CT_REFERENCIA " +
			strCampos +
			"	, I2.cg_razon_social as beneficiario " +
			"	, d.fn_porc_beneficiario as por_beneficiario " +
			"	, DS.fn_importe_recibir_benef as importe_a_recibir " +
			"	, DS.in_tasa_aceptada " +
			"	, replace(I.cg_rfc, '-', '') as CG_RFC_IF, d.cs_dscto_especial, '' as doctoasociado" +
			"	, D.fn_monto_ant "+
			"	, to_char(D.df_fecha_venc_pyme, 'DD/MM/YYYY') df_fecha_venc_pyme "+

			" , TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO " +
			" , tf.cg_nombre AS TIPO_FACTORAJE "+
			"   , 'AvisNotiEpoDE::getDocumentQueryFile' AS PANTALLA,IE.CG_CUENTA_CLABE cuentaClabe "+
			"   FROM ");


		String sHint = "";
		String sHintNC = "";
		String sHintNCM = "";
		StringBuffer sbTablas = new StringBuffer(" ");
		StringBuffer sbTablasNC = new StringBuffer(" ");
		StringBuffer sbTablasNCM = new StringBuffer(" ");

		String prefijo = "PREFIJO_TABLA_IC_DOCUMENTO.";
		String prefijo2 = "PREFIJO_JOINS";
		String prefijo3 = "PREFIJO_JOINS_NCM"; // Fodea 002 - 2010.

		StringBuffer sbCondicion = new StringBuffer(
			"  WHERE d.ic_documento = " + prefijo + "ic_documento"   +
			"    AND ds.ic_documento = s.ic_documento"   +
			"    AND s.cc_acuse = a3.cc_acuse"   +
			"    AND d.ic_if = ie.ic_if"   +
			"    AND ie.ic_if = i.ic_if"   + prefijo2+
			"    AND ie.ic_epo = pe.ic_epo"   +
			"    AND p.ic_pyme = pe.ic_pyme"   +
			"    AND d.ic_pyme = p.ic_pyme"   +
			"    AND d.ic_beneficiario = i2.ic_if (+)"   +
			"    AND d.ic_moneda = m.ic_moneda"  +
			"    AND d.ic_estatus_docto in (4,11) "+
			"    AND ds.ic_epo= ? "+
			"    AND d.ic_epo = ? "+
			"    AND ie.ic_epo = ? " +
			"    AND pe.ic_epo = ? " +
			"    AND a3.ic_producto_nafin = ? ");
		this.conditions.add(new Integer(iNoCliente));
		this.conditions.add(new Integer(iNoCliente));
		this.conditions.add(new Integer(iNoCliente));
		this.conditions.add(new Integer(iNoCliente));
		this.conditions.add(new Integer(1));
		
		StringBuffer sbCondicionNC = new StringBuffer(
			"  WHERE d.ic_documento = ds.ic_documento"   +
			"    AND d.ic_docto_asociado = s.ic_documento"   +
			"    AND s.cc_acuse = a3.cc_acuse"   +
			"    AND d.ic_if = ie.ic_if"   +
			"    AND ie.ic_if = i.ic_if"   + prefijo2+
			"    AND ie.ic_epo = pe.ic_epo"   +
			"    AND p.ic_pyme = pe.ic_pyme"   +
			"    AND d.ic_pyme = p.ic_pyme"   +
			"    AND d.ic_beneficiario = i2.ic_if (+)"   +
			"    AND d.ic_moneda = m.ic_moneda"  +
			"    AND ds.ic_epo= ? " +
			"    AND d.ic_epo = ? " +
			"    AND ie.ic_epo = ? " +
			"    AND pe.ic_epo = ? " +
			" 	  AND a3.ic_producto_nafin = ? " +
			"    AND d.ic_estatus_docto in (4,11) ");	//No me gust� la idea de dejarlo fijo... pero como correcci�n rapida es lo mas facil.

		StringBuffer sbCondicionNCM = new StringBuffer(
			"  WHERE d.ic_documento = ds.ic_documento"   +
			"	  AND nd.ic_documento     = s.ic_documento "  +
			"	  AND nd.ic_nota_credito  = d.ic_documento "  +
			"    AND s.cc_acuse = a3.cc_acuse"   +
			"    AND d.ic_if = ie.ic_if"   +
			"    AND ie.ic_if = i.ic_if"   + prefijo3+
			"    AND ie.ic_epo = pe.ic_epo"   +
			"    AND p.ic_pyme = pe.ic_pyme"   +
			"    AND d.ic_pyme = p.ic_pyme"   +
			"    AND d.ic_beneficiario = i2.ic_if (+)"   +
			"    AND d.ic_moneda = m.ic_moneda"  +
			"    AND ds.ic_epo= ? " +
			"    AND d.ic_epo = ? " +
			"    AND ie.ic_epo = ? " +
			"    AND pe.ic_epo = ? " +
			" 	  AND a3.ic_producto_nafin = ? " +			
			"    AND d.ic_estatus_docto in (4,11) " +
			"    AND d.cs_dscto_especial = 'C' ");

		if(("".equals(icIf) || icIf== null) &&
			("".equals(dfFechaNotMin) || dfFechaNotMin== null) &&
			("".equals(dfFechaNotMax) || dfFechaNotMax== null) &&
			("".equals(dfFechaVencMin) || dfFechaVencMin== null) &&
			("".equals(dfFechaVencMax) || dfFechaVencMax== null) &&
			("".equals(ccAcuse3) || ccAcuse3 == null) &&
			("".equals(clavePyme) || clavePyme == null) &&//FODEA 028 - 2010 ACF
			("".equals(ic_documento) || ic_documento == null)
			||
			(!"".equals(dfFechaNotMin) && dfFechaNotMin != null) ||
			(!"".equals(dfFechaNotMax) && dfFechaNotMax != null) ||
			(!"".equals(ccAcuse3) && ccAcuse3 != null)) {
			//Si no hay ningun criterio de busqueda o los criterios de busqueda
			//involucran a la fecha de notificacion o clave del acuse...
			//Entra en esta secci�n
			prefijoTablaJoin = "s.";
			prefijoJoins = "	AND d.ic_pyme = pe.ic_pyme " +
   						   "	AND d.ic_epo = pe.ic_epo ";
			
			//if (iNoCliente.equals("256") || iNoCliente.equals("30")|| iNoCliente.equals("7")|| iNoCliente.equals("2")){
				//sHint = " /*+ leading(a3) */ ";
			//} else {
				//sHint = " /*+ leading(a3) use_nl(s,ds,d,pe,p,ie,i,i2,m)*/ ";
			//}
			//sHintNC = " /*+ use_nl(s,ds,d,pe,p,ie,i,i2,m) leading(d) */ ";
			//sHintNCM = " /*+ use_nl(d nd s a3 ds pe p ie i i2 m tf) leading(d) */ ";
			
			sbTablas.append(
				" 	com_acuse3 a3,"   +
				"  com_solicitud s,"   +
				"  com_docto_seleccionado ds,    "   +
				"  com_documento d,"   +
				"     comrel_pyme_epo pe,"   +
				"  comcat_pyme p,"   +
				" 	  comrel_if_epo  ie,"   +
				" 	comcat_if i,"   +
				" 	comcat_if i2,"   +
				"  comcat_moneda m, "+
				"  comcat_tipo_factoraje tf " );
			sbTablasNC.append(
				" 	com_acuse3 a3,"   +
				"  com_solicitud s,"   +
				"  com_documento d,"   +
				"  com_docto_seleccionado ds,    "   +
				"     comrel_pyme_epo  pe,"   +
				"  comcat_pyme p,"   +
				" 	comrel_if_epo  ie,"   +
				" 	comcat_if i,"   +
				" 	comcat_if i2,"   +
				"  comcat_moneda m, " +
				"  comcat_tipo_factoraje tf ");
			sbTablasNCM.append(
				" 	com_acuse3 a3,"   +
				"  com_solicitud s,"   +
				"  com_documento d,"   +
				"  com_docto_seleccionado ds,    "   +
				"  comrel_pyme_epo  pe,"   +
				"  comcat_pyme p,"   +
				" 	comrel_if_epo  ie,"   +
				" 	comcat_if i,"   +
				" 	comcat_if i2,"   +
				"  comcat_moneda m, " +
				"  comcat_tipo_factoraje tf, " +
				"  comrel_nota_docto nd ");

			if(("".equals(dfFechaNotMin) || dfFechaNotMin== null) &&
			   ("".equals(dfFechaNotMax) || dfFechaNotMax== null) &&
			   ("".equals(ccAcuse3) || ccAcuse3 == null) ) {
					//Si todos los criterios de busqueda vienen vacios
					//se toman los operados del d�a unicamente
					sbCondicionAdic.append(" and A3.df_fecha_hora >= TRUNC(SYSDATE) ");
					sbCondicionAdic.append(" and A3.df_fecha_hora < TRUNC(SYSDATE+1) ");
			} else {
				//Se proporciono por lo menos un parametro
				if(!"".equals(dfFechaNotMin) && dfFechaNotMin != null) {
					if(!"".equals(dfFechaNotMax)) {
						sbCondicionAdic.append(" and A3.df_fecha_hora >= TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
						sbCondicionAdic.append(" and A3.df_fecha_hora < TRUNC(TO_DATE(?,'dd/mm/yyyy')+1) ");
						this.conditions.add(dfFechaNotMin);
						this.conditions.add(dfFechaNotMax);
					}
				}
				if (!"".equals(ccAcuse3) && ccAcuse3 != null) {
					sbCondicionAdic.append(" AND A3.cc_acuse = ? ");
					this.conditions.add(ccAcuse3);
				}
			} //Fin del if-else por el numero de parametros
		} else {
			//Esta secci�n se ejecuta cuando al menos hay un parametro de busqueda
			//que no tenga relacion con el acuse.

			//Y por motivos de optimizacion de la consulta, si la consulta no involucra el acuse,
			//se requiere hacer un query diferente.
			prefijoTablaJoin = "ds.";

			sHintNC = " /*+ use_nl(d ds s pe p a3 ie i i2 m) leading(d) */ ";
			sHintNCM = " /*+ use_nl(d nd s a3 ds pe p ie i i2 m tf) leading(d) */ ";
			sbTablas.append(
				"      com_documento d, "+
				"      com_docto_seleccionado ds, "+
				"      com_solicitud s, "+
				"      comcat_pyme p, "+
				"      comrel_pyme_epo pe, "+
				"      com_acuse3 a3, "+
				"      comrel_if_epo ie, "+
				"      comcat_if i, "+
				"      comcat_if i2, "+
				"      comcat_moneda m, "+
				"      comcat_tipo_factoraje tf ");
			sbTablasNC.append(
				"      com_documento d, "+
				"      com_docto_seleccionado ds, "+
				"      com_solicitud s, "+
				"      comcat_pyme p, "+
				"      comrel_pyme_epo pe, "+
				"      com_acuse3 a3, "+
				"      comrel_if_epo ie, "+
				"      comcat_if i, "+
				"      comcat_if i2, "+
				"      comcat_moneda m, "+
				"      comcat_tipo_factoraje tf ");
			sbTablasNCM.append(
				"      com_documento d, "+
				"      com_docto_seleccionado ds, "+
				"      com_solicitud s, "+
				"      comcat_pyme p, "+
				"      comrel_pyme_epo pe, "+
				"      com_acuse3 a3, "+
				"      comrel_if_epo ie, "+
				"      comcat_if i, "+
				"      comcat_if i2, "+
				"      comcat_moneda m, "+
				"      comcat_tipo_factoraje tf, "+
				"      comrel_nota_docto nd ");

		} //fin else
		if(!"".equals(dfFechaVencMin) && dfFechaVencMin != null) {
			if(!"".equals(dfFechaVencMax)) {
				sHint = " /*+ index(d IN_COM_DOCUMENTO_09_NUK) leading(d) USE_NL (d ds s pe p a3 ie i i2 m) */ ";
				//sHintNC = sHint;
				//sHintNCM = " /*+ index(d IN_COM_DOCUMENTO_09_NUK) leading(d) USE_NL (d ds s pe p a3 ie i i2 m nd) */ ";
				sbCondicionAdic.append(" and D.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') ");
				sbCondicionAdic.append(" and D.df_fecha_venc < TO_DATE(?,'dd/mm/yyyy')+1 ");

				this.conditions.add(dfFechaVencMin);
				this.conditions.add(dfFechaVencMax);
			}
		}
		if (!"".equals(icIf) && icIf != null) {
			sbCondicionAdic.append(" AND d.ic_if = ?  AND ie.ic_if = ?  AND i.ic_if = ? ");
			this.conditions.add(new Integer(icIf));
			this.conditions.add(new Integer(icIf));
			this.conditions.add(new Integer(icIf));
		}

		if(!ic_documento.equals("")){
			sbCondicionAdic.append(" AND D.ic_documento = ? ");
			this.conditions.add(new Double(ic_documento));
		}
    //FODEA 028 - 2010 ACF (I)
		if(!clavePyme.equals("")){
			sbCondicionAdic.append(" AND d.ic_pyme = ? ");
			this.conditions.add(new Integer(clavePyme));
		}
    //FODEA 028 - 2010 ACF (I)
		sbCondicionAdic.append(" AND d.cs_dscto_especial = tf.cc_tipo_factoraje ");  
		int indice = sbCondicion.toString().indexOf(prefijo);
		//De acuerdo a las condiciones adicionales, se determino que prefijo aplica
		//para la optimizacion: tomar s.ic_documento o ds.ic_documento
		sbCondicion.replace(indice, indice + prefijo.length(), prefijoTablaJoin);
		int indice2 = sbCondicion.toString().indexOf(prefijo2);
		int indice3 = sbCondicionNC.toString().indexOf(prefijo2);
		int indice4 = sbCondicionNCM.toString().indexOf(prefijo3);
		sbCondicion.replace(indice2, indice2 + prefijo2.length(), prefijoJoins);
		sbCondicionNC.replace(indice3, indice3 + prefijo2.length(), prefijoJoins);
		sbCondicionNCM.replace(indice4, indice4 + prefijo3.length(), prefijoJoins);

		String query_Reporte =
			"SELECT "+sHint+ " " + sbColumnas.toString()+sbTablas.toString()+sbCondicion.toString()+sbCondicionAdic.toString();

		if("S".equals(operaNC)) {

			query_Reporte =
				query_Reporte+
				" UNION ALL "+
				"SELECT "+sHintNC + " " + sbColumnasNC.toString()+sbTablasNC.toString()+sbCondicionNC.toString()+sbCondicionAdic.toString();

			conditionsNCM.addAll(conditions); // Fodea 002 - 2010. Condiciones Notas de Credito Multiple
			//Para que la siguiente linea funcione, las variables bind deben de aplicar en el mismo orden en los dos queries.
			this.conditions.addAll(conditions);

			// Fodea 002 - 2010. Notas de Credito Multiple
			query_Reporte =
				query_Reporte+
				" UNION ALL "+
				"SELECT "+sHintNCM + " DISTINCT " + sbColumnas.toString()+sbTablasNCM.toString()+sbCondicionNCM.toString()+sbCondicionAdic.toString();
			//Para que la siguiente linea funcione, las variables bind deben de aplicar en el mismo orden en los dos queries.
			this.conditions.addAll(conditionsNCM);

		}//if("S".equals(operaNC))
		log.debug("query_Reporte::"+query_Reporte);
		log.debug("conditions::"+this.conditions);
		log.info("getDocumentQueryFile(S) ::..");
		return query_Reporte;
	}

	public HashMap getPartesDelNumeroSIAFF(String numeroSIAFF){
		HashMap resultado = new HashMap();

		BigInteger	icEPO 		= new BigInteger("-1");
		BigInteger	icDocumento = new BigInteger("-1");

		if(numeroSIAFF != null && !numeroSIAFF.equals("") && numeroSIAFF.length() == 15){
			icEPO 	= new BigInteger(numeroSIAFF.substring(0,4));
			icDocumento = new BigInteger(numeroSIAFF.substring(4,15));
		}

		resultado.put("IC_EPO",			icEPO.toString());
		resultado.put("IC_DOCUMENTO",	icDocumento.toString());

		return resultado;
	}

	public String getClaveEPOenSession(HttpServletRequest request){
		String clave = (String)request.getSession().getAttribute("iNoCliente");
		return clave;
	}
	
	
	
	/*******************************************************************************************/
	//MIGRACION EPO 
	/******************************************************************************************/
	
	private String 	paginaOffset;
	private String 	paginaNo;		
	private ArrayList conditionsNCM = new ArrayList();	
	private String ic_if;
	private String df_fecha_notMin;
	private String df_fecha_notMax;
	private String df_fecha_vencMin;
	private String df_fecha_vencMax;
	private String cc_acuse3;
	private String numero_siaff;
	private String clavePyme;
	private String claveEpo;
	private String operaNC;
	private String fechaActual;
	private String idioma;
	private String strCampos;
	private String operaFVPyme;
	private String banderaTablaDoctos;
	private String mensaje;
	public String tipo;
	
		
  
	/**
	 * metodo que obtiene el total de documentos por moneda
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E) ::..");
		conditions = new ArrayList();
		ArrayList conditionsNCM = new ArrayList();	
		String ic_documento = "";
		String ic_epo = "";

		if (!numero_siaff.equals("")) {
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);
			ic_epo = (String)numero.get("IC_EPO");
			ic_documento = (String)numero.get("IC_DOCUMENTO");
		}		
		String prefijoTablaJoin = "s.";
		//me traje la condici�n de todos los campos
		StringBuffer condicion = new StringBuffer();
		//HInts y orden de tablas de manera predeterminada:
		String hint = //" /*+ use_nl(a3 s d m)*/ ";
                    " /*+ ordered index(d IN_COM_DOCUMENTO_07_NUK)  */ ";
        
		String tablas = //"com_documento d, com_solicitud s, com_acuse3 a3, comcat_moneda m";
                        " com_documento d, comcat_moneda m, com_solicitud s, com_acuse3 a3 ";
		String hintNC = "/*+use_nl(a3 s ds d m)*/ ";
		String tablasNC = "com_documento d, com_docto_seleccionado ds, com_solicitud s, com_acuse3 a3, comcat_moneda m";
		String hintNCM = " /*+ use_nl(a3 s d m nd) index(nd in_comrel_nota_docto_01_nuk)*/ ";//FODEA 012 - 2010 ACF
		String tablasNCM = "com_acuse3 a3, com_solicitud s, com_documento d, comcat_moneda m, comrel_nota_docto nd"; // com_docto_seleccionado ds,
		boolean hintPredeterminado = false;
		//------------- INICIO ---------- USO de hints/orden de tablas predeterminado
		//Fecha de Vencimiento
		if (!"".equals(df_fecha_vencMin) && df_fecha_vencMin != null) {
			if (!"".equals(df_fecha_vencMax)) {
				condicion.append(
					" AND d.df_fecha_venc >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))"+
					" AND d.df_fecha_venc < TRUNC(TO_DATE(?, 'dd/mm/yyyy') + 1)");
				this.conditions.add(df_fecha_vencMin);
				this.conditions.add(df_fecha_vencMax);
				hintPredeterminado = true;
			}
		}
		//Intermediario Finaciero
		if (!"".equals(ic_if) && ic_if != null) {
			condicion.append(" AND d.ic_if = ?");
			this.conditions.add(new Integer(ic_if));
			hintPredeterminado = true;
		}
		//Clave del documento obtenido a partir del N�mero SIAFF
		if(!ic_documento.equals("")){
			condicion.append(" AND d.ic_documento = ?");
			this.conditions.add(new Double(ic_documento));
			hintPredeterminado = true;
		}
    //No. de Proveedor //FODEA 028 - 2010 ACF (I)	
		if (!"".equals(clavePyme) && clavePyme != null) {
			condicion.append(" AND d.ic_pyme = ?");
			this.conditions.add(new Integer(clavePyme));
			hintPredeterminado = true;
		}
    //FODEA 028 - 2010 ACF (I)
		//------------- FIN ---------- USO de hints/orden de tablas predeterminado
		if (hintPredeterminado == false) {
			//-------------------------------------------------------------------------
			//POR Razones de optimizacion, para las siguientes condiciones,
			//aplica otro orden de tablas y hints
			hint =
					" /*+ leading(a3) USE_NL(a3 s d m) */ ";
			tablas = "com_acuse3 a3, com_solicitud s, com_documento d, comcat_moneda m";
			hintNC = "/*+use_nl(a3 s ds d m) leading(a3) */ ";
			tablasNC = "com_acuse3 a3, com_solicitud s, com_documento d, com_docto_seleccionado ds, comcat_moneda m";
			//hintNCM = " /*+ leading(a3) USE_NL(a3 s s ds m nd) */ ";
      hintNCM = " /*+use_nl(a3 s d m nd) index(nd in_comrel_nota_docto_01_nuk)*/ ";//FODEA 012 - 2010 ACF
			tablasNCM = "com_acuse3 a3, com_solicitud s, com_documento d, comcat_moneda m, comrel_nota_docto nd";// com_docto_seleccionado ds,
		}
		//Fecha de Notificaci�n
		if (!"".equals(df_fecha_notMin) && df_fecha_notMin != null) {
			if (!"".equals(df_fecha_notMax)) {
				condicion.append(
					" AND a3.df_fecha_hora >= TO_DATE(?, 'dd/mm/yyyy')"+
					" AND a3.df_fecha_hora < TO_DATE(?, 'dd/mm/yyyy') + 1");
			    this.conditions.add(df_fecha_notMin);
					this.conditions.add(df_fecha_notMax);
			}
		}
		//Acuse de Notificaci�n
		if (!"".equals(cc_acuse3) && cc_acuse3 != null) {
			condicion.append(" AND a3.cc_acuse = ?");
			this.conditions.add(cc_acuse3);
		}
		//Si no se hay ning�n criterio de consulta, se toman las notificaciones del d�a
		if ("".equals(condicion.toString())) {
			condicion.append(
				" AND a3.df_fecha_hora >= TRUNC(SYSDATE)"+
				" AND a3.df_fecha_hora < TRUNC(SYSDATE + 1)");
		}
		//-------------------------------------------------------------------------
		String query_Reporte =
			" SELECT " + 
			" d.fn_monto AS fn_monto,"+
			" d.fn_monto_dscto AS fn_monto_dscto,"+
			" m.cd_nombre" + idioma + " AS cd_nombre,"+
			" d.ic_moneda"+
			" FROM " + tablas +
			" WHERE d.ic_moneda = m.ic_moneda"+
			" AND d.ic_documento = s.ic_documento"+
			" AND s.cc_acuse = a3.cc_acuse"+
			"  AND d.ic_estatus_docto in (4,11) "+
			condicion +
			" AND a3.ic_producto_nafin = ? " +
			" AND d.ic_epo = ? ";		

		this.conditions.add(new Integer(1));	//Descuento Electronico
		this.conditions.add(new Integer(claveEpo));

		if ("S".equals(operaNC)) {
			query_Reporte =
			query_Reporte +
			" UNION ALL"+
			" SELECT " + hintNC +
			" d.fn_monto AS fn_monto, "+
			" d.fn_monto_dscto AS fn_monto_dscto, "+
			" m.cd_nombre" + idioma + " AS cd_nombre, "+
			" d.ic_moneda"+
			" FROM " + tablasNC +
			" WHERE d.ic_moneda = m.ic_moneda"+
			" AND d.ic_documento = ds.ic_documento"+
			" AND ds.ic_epo = d.ic_epo "+
			" AND d.ic_docto_asociado = s.ic_documento"+
			" AND s.cc_acuse = a3.cc_acuse"+
			condicion +
			" AND a3.ic_producto_nafin = ? " +
			" AND d.ic_epo = ? ";
			conditionsNCM.addAll(conditions);
			//Las condiciones de este query deben ser iguales a las del query de arriba, para que funcione la sig. linea
			this.conditions.addAll(conditions);
			query_Reporte +="    AND d.ic_estatus_docto in (4,11) ";
			// Fodea 002 - 2010
			query_Reporte =
			query_Reporte +
			" UNION ALL"+
			" SELECT " + hintNCM + " DISTINCT" +
			" d.fn_monto AS fn_monto,"+
			" d.fn_monto_dscto AS fn_monto_dscto,"+
			" m.cd_nombre" + idioma + " AS cd_nombre,"+
			" d.ic_moneda"+
			" FROM " + tablasNCM +
			" WHERE d.ic_moneda = m.ic_moneda"+
			" AND nd.ic_documento = s.ic_documento"+
			" AND nd.ic_nota_credito = d.ic_documento"+
			" AND s.cc_acuse = a3.cc_acuse"+
			" AND d.ic_estatus_docto in (4,11) "+
			condicion +
			" AND a3.ic_producto_nafin = ?"+
			" AND d.ic_epo = ?";
			//Las condiciones de este query deben ser iguales a las del query de arriba, para que funcione la sig. linea
			this.conditions.addAll(conditionsNCM);
		}
		query_Reporte =
			" SELECT COUNT(1) AS result_size,"+
			" SUM(fn_monto) AS fn_monto,"+
			" SUM(fn_monto_dscto) AS fn_monto_dscto,"+
			" cd_nombre,"+
			" ic_moneda,"+
			" 'AvisNotiEpoDE::getAggregateCalculationQuery' AS pantalla"+
			" FROM (" + query_Reporte + ")"+
			" GROUP BY ic_moneda, cd_nombre"+
			" ORDER BY ic_moneda";

		log.debug("query_Reporte: "+query_Reporte);
		log.debug("conditions: "+this.conditions);
		log.info("getAggregateCalculationQuery(S) ::..");
		return query_Reporte;
	}
	
	
	/**
	 * metodo que obtiene los Id's de los documentos  a mostrar en la consulta	 * 
	 * @return 
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E) ::..");
		this.conditions = new ArrayList();
		ArrayList conditionsNCM = new ArrayList();

		String ic_documento = "";
		String ic_epo = "";

		if (!numero_siaff.equals("")) {
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);
			ic_epo = (String)numero.get("IC_EPO");
			ic_documento = (String)numero.get("IC_DOCUMENTO");
		}
	  

		StringBuffer condicion = new StringBuffer();

		//HInts y orden de tablas de manera predeterminada:
		//String hint = " /*+ use_nl(a3 s d)*/ "; 
        String hint = " /*+ ordered index (d IN_COM_DOCUMENTO_07_NUK) */"; 
		String tablas = " com_documento d, com_solicitud s, com_acuse3 a3 ";
		String hintNC = " /*+ USE_NL(a3 s ds d) */ ";
		String tablasNC = " com_documento d, com_docto_seleccionado ds, com_solicitud s, com_acuse3 a3 ";
		String hintNCM = " /*+ use_nl(a3 s d nd) index(nd in_comrel_nota_docto_01_nuk)*/ ";//FODEA 012 - 2010 ACF
		String tablasNCM = " com_acuse3 a3, com_solicitud s, com_documento d, comrel_nota_docto nd ";//com_docto_seleccionado ds,
		boolean hintPredeterminado = false;
		//------------- INICIO ---------- USO de hints/orden de tablas predeterminado
		if(!"".equals(df_fecha_vencMin) && df_fecha_vencMin != null) {
			if(!"".equals(df_fecha_vencMax)) {
				condicion.append(
					" and d.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') "+
					" and d.df_fecha_venc < TO_DATE(?,'dd/mm/yyyy')+1 ");
				this.conditions.add(df_fecha_vencMin);
				this.conditions.add(df_fecha_vencMax);
				hintPredeterminado = true;
			}
		}

		if (!"".equals(ic_if) && ic_if != null){
			condicion.append(" AND d.ic_if = ? ");
			this.conditions.add(new Integer (ic_if));
			hintPredeterminado = true;
		}

		if(!ic_documento.equals("")){
			condicion.append(" AND d.ic_documento = ? ");
			this.conditions.add(new Double(ic_documento));
			hintPredeterminado = true;
		}
		
    //FODEA 028 - 2010 ACF (I)
		if (!"".equals(clavePyme) && clavePyme != null){
			condicion.append(" AND d.ic_pyme = ? ");
			this.conditions.add(new Integer(clavePyme));
			hintPredeterminado = true;
		}
    //FODEA 028 - 2010 ACF (I)
		//------------- FIN ---------- USO de hints/orden de tablas predeterminado

		if (hintPredeterminado == false) {

			//-------------------------------------------------------------------------
			//POR Razones de optimizacion, para las siguientes condiciones,
			//aplica otro orden de tablas y hints
			hint =
					" /*+ leading(a3) USE_NL(a3 s d) */ ";
			tablas = "com_acuse3 a3, com_solicitud s, com_documento d";
			hintNC = " /*+ leading(a3) USE_NL(a3 s ds d) */ ";
			tablasNC = "com_acuse3 a3, com_solicitud s,  com_documento d, com_docto_seleccionado ds";
			// Fodea 002 - 2010
			hintNCM = " /*+ use_nl(a3 s d nd) index(nd in_comrel_nota_docto_01_nuk)*/ ";//FODEA 012 - 2010 ACF
			tablasNCM = "com_acuse3 a3, com_solicitud s,  com_documento d, comrel_nota_docto nd "; // com_docto_seleccionado ds,
		}

		if(!"".equals(df_fecha_notMin) && df_fecha_notMin != null) {
			if(!"".equals(df_fecha_notMax)) {
				condicion.append(
					" and a3.df_fecha_hora >= TO_DATE(?,'dd/mm/yyyy') "+
					" and a3.df_fecha_hora < TO_DATE(?,'dd/mm/yyyy')+1 ");
					this.conditions.add(df_fecha_notMin);
					this.conditions.add(df_fecha_notMax);
			}
		}

		if (!"".equals(cc_acuse3) && cc_acuse3 != null){
			condicion.append(" AND a3.cc_acuse = ? ");
			this.conditions.add(cc_acuse3);
		}

		if("".equals(condicion.toString())) {
			condicion.append(
				" and a3.df_fecha_hora >= TRUNC(SYSDATE) "+
				" and a3.df_fecha_hora < TRUNC(SYSDATE+1) ");
		}

		//-------------------------------------------------------------------------


		String query_Reporte =
			" SELECT   "+hint+
			"        d.ic_documento, 'AvisNotiEpoDE::getDocumentQuery' AS pantalla"   +
			"   FROM " + tablas +
			"  WHERE d.ic_documento = s.ic_documento"   +
			"    AND s.cc_acuse = a3.cc_acuse"  +
			 condicion +
			"    AND a3.ic_producto_nafin = ? "  +
			"    AND D.ic_epo = ? "+
			"    AND d.ic_estatus_docto in (4,11) ";

		this.conditions.add(new Integer(1));	//1= Descuento Electronico
		this.conditions.add(new Integer(claveEpo));

		if("S".equals(operaNC)) {

			query_Reporte =
				query_Reporte +
				" UNION ALL "+
				"SELECT  " + hintNC +
				" D.ic_documento"+
				" , 'AvisNotiEpoDE::getDocumentQuery' AS PANTALLA "+
				" FROM " + tablasNC +
				" WHERE d.ic_documento = ds.ic_documento " +
				" AND d.ic_docto_asociado = s.ic_documento " +
				" AND s.cc_acuse = a3.cc_acuse " +
				condicion +
				" AND a3.ic_producto_nafin = ? "  +
				" AND D.ic_epo = ? "+
				"  AND d.ic_estatus_docto in (4,11) ";
   
				conditionsNCM.addAll(conditions);// Fodea 002 - 2010. Nota de Credito Multiple
				//Para que la sig instruccion se VALIDA, es necesario que sean las mismas condiciones que el query de arriba.
				this.conditions.addAll(conditions);			

			// Fodea 002 - 2010. Mostrar Notas de Credito Multiple
			query_Reporte =
				query_Reporte +
				" UNION ALL "+
				"SELECT  " + hintNCM +
				"  distinct "+
				" D.ic_documento"+
				" , 'AvisNotiEpoDE::getDocumentQuery' AS PANTALLA "+
				" FROM " + tablasNCM +
				" WHERE " +
				//" d.ic_documento 		= ds.ic_documento AND " +
				"     nd.ic_documento     	= s.ic_documento " +
				" AND nd.ic_nota_credito  	= d.ic_documento " +
				" AND s.cc_acuse = a3.cc_acuse " +
				condicion +
				" AND a3.ic_producto_nafin = ? "  +
				" AND D.ic_epo = ? "+
				"  AND d.ic_estatus_docto in (4,11) ";
				//Para que la sig instruccion se VALIDA, es necesario que sean las mismas condiciones que el query de arriba.
				this.conditions.addAll(conditionsNCM);
		}

		log.debug("query_Reporte: "+query_Reporte);
		log.debug("conditions::"+this.conditions);
		log.info("getDocumentQuery(S) ::..");
		return query_Reporte;
	}
	
	
	/**
	 * metodo quue obtiene el query final de la consulta
	 *@return 
	 * @param pageIds
	 */
	
	public String getDocumentSummaryQueryForIds(List pageIds) {  
		log.info("getDocumentSummaryQueryForIds(E)");
		StringBuffer query 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		query.append(
			"SELECT /*+ use_nl(d ds s a3 pe p i m ie i2) */ (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(DS.cs_opera_fiso,'N',I.cg_razon_social,'S',I3.cg_razon_social))) as INTERMEDIARIO, "+
			"	to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOTIFICACION, "+
			"	A3.cc_acuse as ACUSE_NOTIFICACION , " +
			"	(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) as NOMBRE_PROVEEDOR , "+
			"	D.ig_numero_docto as NUM_DOCTO , "+
			"	to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, " +
			"	to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, "+
			"   M.cd_nombre"+idioma+ " as MONEDA, " +
			"	D.fn_monto as MONTO , "+
			"	D.fn_porc_anticipo as POR_DESCUENTO, "+
			"	D.fn_monto_dscto as MONTO_DESCONTAR , " +
			"	IE.ig_sucursal as CLAVE_BANCO, "+
			"	IE.cg_banco as BANCO, "+
			"	IE.cg_num_cuenta as NUM_CUENTA, "+
			"	PE.cg_pyme_epo_interno as NUM_PROVEEDOR , "+
			"	D.ic_moneda  as IC_MONEDA, " +
			"	D.ic_documento as IC_DOCUMENTO, " +
			"	D.cs_detalle , " +
			"	D.CT_REFERENCIA  as REFERENCIA" +
			strCampos +
			"	, I2.cg_razon_social as BENEFICIARIO " +
			"	, d.fn_porc_beneficiario as POR_BENEFICIARIO " +
			"	, DS.fn_importe_recibir_benef as IMP_RECIBIR_BENE " +
			//"	, DS.in_tasa_aceptada"+
			//Fodea17
			", NVL(DS.in_tasa_aceptada_fondeo,DS.in_tasa_aceptada) as in_tasa_aceptada"+
			", d.cs_dscto_especial , '' as DOCT_APLICADO " +
			"	,to_char(D.df_fecha_venc_pyme, 'DD/MM/YYYY') as DF_FECHA_VEN_PYME "+
			" ,TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA as TIPO_COMPRA , D.CG_CLAVE_PRESUPUESTARIA as CLASIFICADOR, D.CG_PERIODO as PLAZO_MAXIMO " +
			" , tf.cg_nombre AS TIPO_FACTORAJE "+
			", RELLENA_CEROS(d.ic_epo, 4)|| '' || RELLENA_CEROS(d.ic_documento, 11) as DIG_IDENTIFICADOR "+						
			" ,'AvisNotiEpoDE::getDocumentSummaryQueryForIds' AS PANTALLA "+
			" FROM COM_DOCUMENTO D, "+
			"	COM_DOCTO_SELECCIONADO DS, "+
			"	COMCAT_IF I, "+
			"	COMCAT_IF I3, "+
			"	COMREL_PYME_EPO PE, " +
			"	COM_ACUSE3 A3, "+
			"	COMCAT_PYME P, "+
			"	COMCAT_MONEDA M, "+
			"	COMREL_IF_EPO IE, "+
			"	COM_SOLICITUD S, " +
			"	comcat_if I2 ," +
			"  comcat_tipo_factoraje tf " +
			"WHERE D.ic_documento = DS.ic_documento " +
			" AND DS.ic_documento = S.ic_documento " +
			" AND S.cc_acuse = A3.cc_acuse " +
			" AND D.ic_if = IE.ic_if " +			
			" AND D.ic_if = I3.ic_if(+) " +
			" AND IE.ic_if = I.ic_if " +
			" AND IE.ic_epo = PE.ic_epo "+
			" AND P.ic_pyme = PE.ic_pyme "+
			" AND D.ic_pyme = P.ic_pyme " +
 			" AND D.ic_epo = PE.ic_epo " +
			" AND d.ic_beneficiario=I2.ic_if(+) " +
			" AND D.ic_moneda = M.ic_moneda " +
			" AND D.cs_dscto_especial = tf.cc_tipo_factoraje ");
			for(int i=0;i<pageIds.size();i++) {
					List lItem = (List)pageIds.get(i);
					if(i==0) {
						query.append(" AND d.ic_documento in  ( ");
					}
					query.append("?");
					conditions.add(new Long(lItem.get(0).toString()));					
					if(i!=(pageIds.size()-1)) {
						query.append(",");
					} else {
						query.append(" ) ");
					}			
				}
				

		if("S".equals(operaNC)) {
			this.numList = 3; // Anterior 2
			query.append(
				" UNION ALL " +
				"SELECT /*+ use_nl(d ds s a3 pe p i m ie i2) */ " +
				"  distinct " +
				"  (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(DS.cs_opera_fiso,'N',I.cg_razon_social,'S',I3.cg_razon_social))), "+
				"	to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, "+
				"	A3.cc_acuse, " +
				"	(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social), "+
				"	D.ig_numero_docto, "+
				"	to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, " +
				"	to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, "+
				"   M.cd_nombre"+idioma+ " as cd_nombre, " +
				"	D.fn_monto, "+
				"	D.fn_porc_anticipo, "+
				"	D.fn_monto_dscto, " +
				"	IE.ig_sucursal, "+
				"	IE.cg_banco, "+
				"	IE.cg_num_cuenta, "+
				"	PE.cg_pyme_epo_interno, "+
				"	D.ic_moneda, " +
				"	D.ic_documento, " +
				"	D.cs_detalle, " +
				"	D.CT_REFERENCIA " +
				strCampos +
				"	, I2.cg_razon_social as beneficiario " +
				"	, d.fn_porc_beneficiario as por_beneficiario " +
				"	, DS.fn_importe_recibir_benef as importe_a_recibir " +
				//"	, DS.in_tasa_aceptada"+
			//Fodea17
			   "  , NVL(DS.in_tasa_aceptada_fondeo,DS.in_tasa_aceptada) as in_tasa_aceptada"+				
				" , d.cs_dscto_especial, d2.ig_numero_docto as DOCT_APLICADO " +
				"	,to_char(D.df_fecha_venc_pyme, 'DD/MM/YYYY') as  DF_FECHA_VEN_PYME "+
				" ,TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO " +
				" , tf.cg_nombre AS TIPO_FACTORAJE "+
				", RELLENA_CEROS(d.ic_epo, 4)|| '' || RELLENA_CEROS(d.ic_documento, 11) as DIG_IDENTIFICADOR "+		
				" ,'AvisNotiEpoDE::getDocumentSummaryQueryForIds' AS PANTALLA "+
				" FROM COM_DOCUMENTO D, "+
				" COM_DOCUMENTO D2, "+
				"	COM_DOCTO_SELECCIONADO DS, "+
				"	COMCAT_IF I, "+
				"	COMCAT_IF I3, "+
				"	COMREL_PYME_EPO PE, " +
				"	COM_ACUSE3 A3, "+
				"	COMCAT_PYME P, "+
				"	COMCAT_MONEDA M, "+
				"	COMREL_IF_EPO IE, "+
				"	COM_SOLICITUD S, " +
				"	comcat_if I2 , " +
				"  comcat_tipo_factoraje tf "+
				"WHERE D.ic_documento = DS.ic_documento " +
				" AND d.ic_docto_asociado = d2.ic_documento "+
				" AND D.ic_docto_asociado = S.ic_documento " +
				" AND S.cc_acuse = A3.cc_acuse " +
				" AND D.ic_if = IE.ic_if " +
				" AND IE.ic_if = I.ic_if " +
				" AND D.ic_if = I3.ic_if(+) " +
				" AND IE.ic_epo = PE.ic_epo "+
				" AND P.ic_pyme = PE.ic_pyme "+
				" AND D.ic_pyme = P.ic_pyme " +
 				" AND D.ic_epo = PE.ic_epo " +
				" AND d.ic_beneficiario=I2.ic_if(+) " +
				" AND D.ic_moneda = M.ic_moneda " +
				" AND D.cs_dscto_especial = tf.cc_tipo_factoraje ");
				
				
				for(int i=0;i<pageIds.size();i++) {
					List lItem = (List)pageIds.get(i);
					if(i==0) {
						query.append(" AND d.ic_documento in  ( ");
					}
					query.append("?");
					conditions.add(new Long(lItem.get(0).toString()));					
					if(i!=(pageIds.size()-1)) {
						query.append(",");
					} else {
						query.append(" ) ");
					}			
				}
				
				
			// Fodea 002 - 2010. Notas de Credito Multiple
			query.append(
				" UNION ALL " +
				"SELECT /*+ use_nl(d ds s a3 pe p i m ie i2 nd) */  " +
				"  distinct " +
				"  (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(DS.cs_opera_fiso,'N',I.cg_razon_social,'S',I3.cg_razon_social))), "+
				"	to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, "+
				"	A3.cc_acuse, " +
				"	(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social), "+
				"	D.ig_numero_docto, "+
				"	to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, " +
				"	to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, "+
				"   M.cd_nombre"+idioma+ " as cd_nombre, " +
				"	D.fn_monto, "+
				"	D.fn_porc_anticipo, "+
				"	D.fn_monto_dscto, " +
				"	IE.ig_sucursal, "+
				"	IE.cg_banco, "+
				"	IE.cg_num_cuenta, "+
				"	PE.cg_pyme_epo_interno, "+
				"	D.ic_moneda, " +
				"	D.ic_documento, " + 
				"	D.cs_detalle, " +
				"	D.CT_REFERENCIA " +
				strCampos +
				"	, I2.cg_razon_social as beneficiario " +
				"	, d.fn_porc_beneficiario as por_beneficiario " +
				"	, DS.fn_importe_recibir_benef as importe_a_recibir " +
				//"	, DS.in_tasa_aceptada "+
			//Fodea17
				" , NVL(DS.in_tasa_aceptada_fondeo,DS.in_tasa_aceptada) as in_tasa_aceptada"+				
				" , d.cs_dscto_especial, '' as DOCT_APLICADO " +
				"	,to_char(D.df_fecha_venc_pyme, 'DD/MM/YYYY') as DF_FECHA_VEN_PYME "+
				" ,TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO " +
				" , tf.cg_nombre AS TIPO_FACTORAJE "+
				", RELLENA_CEROS(d.ic_epo, 4)|| '' || RELLENA_CEROS(d.ic_documento, 11) as DIG_IDENTIFICADOR "+		
				" ,'AvisNotiEpoDE::getDocumentSummaryQueryForIds' AS PANTALLA "+
				" FROM COM_DOCUMENTO D, "+
				"	COM_DOCTO_SELECCIONADO DS, "+
				"	COMCAT_IF I, "+
				"	COMCAT_IF I3, "+
				"	COMREL_PYME_EPO PE, " +
				"	COM_ACUSE3 A3, "+
				"	COMCAT_PYME P, "+
				"	COMCAT_MONEDA M, "+
				"	COMREL_IF_EPO IE, "+
				"	COM_SOLICITUD S, " +
				"	comcat_if I2 , " +
				"  comcat_tipo_factoraje tf, "+
				"  comrel_nota_docto nd " +
				"WHERE D.ic_documento = DS.ic_documento " +
				" AND nd.ic_documento     = s.ic_documento "  +
				" AND nd.ic_nota_credito  = d.ic_documento "  +
				" AND S.cc_acuse = A3.cc_acuse " +
				" AND D.ic_if = IE.ic_if " +
				" AND D.ic_if = I3.ic_if(+) " +
				" AND IE.ic_if = I.ic_if " +
				" AND IE.ic_epo = PE.ic_epo "+
				" AND P.ic_pyme = PE.ic_pyme "+
				" AND D.ic_pyme = P.ic_pyme " +
 				" AND D.ic_epo = PE.ic_epo " +
				" AND d.ic_beneficiario=I2.ic_if(+) " +
				" AND D.ic_moneda = M.ic_moneda " +
				" AND D.cs_dscto_especial = tf.cc_tipo_factoraje ");
				
				for(int i=0;i<pageIds.size();i++) {
					List lItem = (List)pageIds.get(i);
					if(i==0) {
						query.append(" AND d.ic_documento in  ( ");
					}
					query.append("?");
					conditions.add(new Long(lItem.get(0).toString()));					
					if(i!=(pageIds.size()-1)) {
						query.append(",");
					} else {
						query.append(" ) ");
					}			
				}				
		}   
		
	
		query.append(" AND i.ic_if = i3.ic_if");
		
		log.info("..:: qrySentencia "+query.toString());
		log.info("..:: conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		
		return 	 query.toString();	
	}
	
	/**
	 * metodo que obtiene el query para obtener 
	 * todos los registros de la consulta
	 * @return 
	 */
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		StringBuffer	query = new StringBuffer();
		conditions = new ArrayList();			
		String ic_documento = "";
		String ic_epo = "";
		String query_Reporte ="";
		
		//este query se ejecuta solo para la EPO 211 Exportar Interfase con Detalle 
		if(tipo.equals("Interfase-EPO211") )	{
			
			
			query.append(" SELECT (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(DS.cs_opera_fiso,'N',I.cg_razon_social,'S',I3.cg_razon_social))) as nombreIf, "+
			"	to_char(A3.df_fecha_hora, 'DD/MM/YYYY') as fecha_not, A3.cc_acuse, PE.cg_pyme_epo_interno, "+
			"	(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) as nombrePyme, DET.cg_campo2 as ig_numero_docto, "+
			"	to_char(D.df_fecha_docto, 'DD/MM/YYYY') as fecha_emision, to_char(D.df_fecha_venc, 'DD/MM/YYYY') as fecha_vencimiento, "+
			"	M.cd_nombre as nombreMoneda, DET.cg_campo6 as fn_monto, IE.ig_sucursal, IE.cg_banco, IE.cg_num_cuenta, "+
			"	D.fn_porc_anticipo, (TO_NUMBER(DET.cg_campo6,'99999999999.99') * (D.fn_porc_anticipo/100)) as fn_monto_dscto, "+
			//"	DS.in_tasa_aceptada,
			" NVL(DS.in_tasa_aceptada_fondeo,DS.in_tasa_aceptada) as in_tasa_aceptada"+
			" DET.cg_campo8 as ct_referencia, DET.cg_campo9 as cg_campo1, DET.cg_campo10 as cg_campo2, "+
			"	D.cg_campo3, D.cg_campo4, D.cg_campo5, I2.cg_razon_social as nombreBeneficiario, "+
			"	D.fn_porc_beneficiario as por_beneficiario, DS.fn_importe_recibir_benef as importe_a_recibir, D.ic_moneda "+
			", RELLENA_CEROS(d.ic_epo, 4)|| '' || RELLENA_CEROS(d.ic_documento, 11) as DIG_IDENTIFICADOR "+	
			
			"	FROM com_documento D, com_docto_seleccionado DS, comcat_if I, comrel_pyme_epo PE, com_acuse3 A3, comcat_pyme P, "+
			"	comcat_moneda M, comrel_if_epo IE, com_solicitud S, comcat_if I2,comcat_if I3, com_documento_detalle DET "+
			"	WHERE D.ic_documento = DS.ic_documento "+
			"	AND D.ic_epo = ? "+
			"	AND DS.ic_documento = S.ic_documento "+
			"	AND S.cc_acuse = A3.cc_acuse "+
			"	AND DS.ic_if = IE.ic_if "+
			"	AND IE.ic_epo = ? "+
			"	AND IE.ic_if = I.ic_if "+
			"	AND D.ic_if = I3.ic_if(+) "+
			"	AND IE.ic_epo = PE.ic_epo "+
			"	AND P.ic_pyme = PE.ic_pyme "+
			"	AND D.ic_pyme = P.ic_pyme "+
			"	AND d.ic_beneficiario=I2.ic_if(+) "+
			"	AND D.ic_moneda = M.ic_moneda "+
			"	AND D.ic_documento = DET.ic_documento ");
			this.conditions.add(new Integer(claveEpo));
			this.conditions.add(new Integer(claveEpo));
					
			if (!"".equals(ic_if)){
				query.append(" AND d.ic_if = ?");
				this.conditions.add(new Integer(ic_if));
			}
			if(!"".equals(df_fecha_notMin) && !"".equals(df_fecha_notMax)){
				query.append(" AND A3.df_fecha_hora >= TRUNC(TO_DATE(? ,'dd/mm/yyyy')) ");
				query.append(" and A3.df_fecha_hora < TRUNC(TO_DATE( ? ,'dd/mm/yyyy') + 1) ");
				this.conditions.add(df_fecha_notMin);
				this.conditions.add(df_fecha_notMax);				
			}
			
			if(!"".equals(df_fecha_vencMin) && !"".equals(df_fecha_vencMax)){
				query.append("  AND D.df_fecha_venc >= TRUNC(TO_DATE( ? ,'dd/mm/yyyy')) ");
				query.append(" and D.df_fecha_venc < TRUNC(TO_DATE( ? ,'dd/mm/yyyy') + 1) ");
				this.conditions.add(df_fecha_vencMin);
				this.conditions.add(df_fecha_vencMax);	
			}
			
			if (!"".equals(cc_acuse3)){
				query.append(" AND A3.cc_acuse = ? ");
				this.conditions.add(cc_acuse3);	
			}
			
			if( "".equals(ic_if) && "".equals(df_fecha_notMin) && "".equals(df_fecha_notMax) && "".equals(df_fecha_vencMin) && "".equals(df_fecha_vencMax) && "".equals(cc_acuse3) ){
					query.append(" and A3.df_fecha_hora >= TRUNC(SYSDATE) ");
					query.append(" and A3.df_fecha_hora < TRUNC(SYSDATE+1) ");					
			}
		
			query_Reporte = query.toString();
			
		}else {
	
			if(!numero_siaff.equals("")){
				HashMap numero = this.getPartesDelNumeroSIAFF(numero_siaff);
	
				ic_epo 			= (String) numero.get("IC_EPO");
				ic_documento 	= (String) numero.get("IC_DOCUMENTO");
			}
			String prefijoTablaJoin = "s.";
			String prefijoJoins = "";
			
			StringBuffer sbCondicionAdic = new StringBuffer(" "),
			sbColumnasNC = new StringBuffer(
				" (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(DS.cs_opera_fiso,'N',I.cg_razon_social,'S',I3.cg_razon_social))), "+
				"	to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, "+
				"	A3.cc_acuse, " +
				"	(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social), "+
				"	D.ig_numero_docto, "+
				"	to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, " +
				"	to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, "+
				"   M.cd_nombre"+idioma+ " as cd_nombre, " +
				"	D.fn_monto, "+
				"	D.fn_porc_anticipo, "+
				"	D.fn_monto_dscto, " +
				"	IE.ig_sucursal, "+
				"	IE.cg_banco, "+
				"	IE.cg_num_cuenta, "+
				"	PE.cg_pyme_epo_interno, "+
				"	D.ic_moneda, " +
				"	D.ic_documento, " +
				"	D.cs_detalle, " +
				"	D.CT_REFERENCIA " +
				strCampos +
				"	, I2.cg_razon_social as beneficiario " +
				"	, d.fn_porc_beneficiario as por_beneficiario " +
				"	, DS.fn_importe_recibir_benef as importe_a_recibir " +
				//"	, DS.in_tasa_aceptada " +
				//Fodea17
				", NVL(DS.in_tasa_aceptada_fondeo,DS.in_tasa_aceptada) as in_tasa_aceptada"+
				"	, replace(I.cg_rfc, '-', '') as CG_RFC_IF, d.cs_dscto_especial, (select d2.ig_numero_docto from com_documento d2 where d2.ic_documento = d.ic_docto_asociado) as doctoasociado" +
				"	, D.fn_monto_ant "+
				"	, to_char(D.df_fecha_venc_pyme, 'DD/MM/YYYY') as  DF_FECHA_VEN_PYME "+
				" , TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO " +
				" , tf.cg_nombre AS TIPO_FACTORAJE "+
				", RELLENA_CEROS(d.ic_epo, 4)|| '' || RELLENA_CEROS(d.ic_documento, 11) as DIG_IDENTIFICADOR "+	
				"  , 'AvisNotiEpoDE::getDocumentQueryFile' AS PANTALLA,IE.CG_CUENTA_CLABE cuentaClabe "+
				"   FROM ");
	
			StringBuffer sbColumnas = new StringBuffer(
				" (decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(DS.cs_opera_fiso,'N',I.cg_razon_social,'S',I3.cg_razon_social))), "+
				"	to_char(A3.df_fecha_hora, 'DD/MM/YYYY') FECHA_NOT, "+
				"	A3.cc_acuse, " +
				"	(decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social), "+
				"	D.ig_numero_docto, "+
				"	to_char(D.df_fecha_docto, 'DD/MM/YYYY') FECHA_EMISION, " +
				"	to_char(D.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENCIMIENTO, "+
				"   M.cd_nombre"+idioma+ " as cd_nombre, " +
				"	D.fn_monto, "+
				"	D.fn_porc_anticipo, "+
				"	D.fn_monto_dscto, " +
				"	IE.ig_sucursal, "+
				"	IE.cg_banco, "+
				"	IE.cg_num_cuenta, "+
				"	PE.cg_pyme_epo_interno, "+
				"	D.ic_moneda, " +
				"	D.ic_documento, " +
				"	D.cs_detalle, " +
				"	D.CT_REFERENCIA " +
				strCampos +
				"	, I2.cg_razon_social as beneficiario " +
				"	, d.fn_porc_beneficiario as por_beneficiario " +
				"	, DS.fn_importe_recibir_benef as importe_a_recibir " +
				//"	, DS.in_tasa_aceptada " +
				//Fodea17
				" , NVL(DS.in_tasa_aceptada_fondeo,DS.in_tasa_aceptada) as in_tasa_aceptada"+
				"	, replace(I.cg_rfc, '-', '') as CG_RFC_IF, d.cs_dscto_especial, '' as doctoasociado" +
				"	, D.fn_monto_ant "+
				"	, to_char(D.df_fecha_venc_pyme, 'DD/MM/YYYY') as  DF_FECHA_VEN_PYME "+	
				" , TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO " +
				" , tf.cg_nombre AS TIPO_FACTORAJE "+
				", RELLENA_CEROS(d.ic_epo, 4)|| '' || RELLENA_CEROS(d.ic_documento, 11) as DIG_IDENTIFICADOR "+	
				"   , 'AvisNotiEpoDE::getDocumentQueryFile' AS PANTALLA,IE.CG_CUENTA_CLABE cuentaClabe "+
				"   FROM ");	
	
			String sHint = "";
			String sHintNC = "";
			String sHintNCM = "";
			StringBuffer sbTablas = new StringBuffer(" ");
			StringBuffer sbTablasNC = new StringBuffer(" ");
			StringBuffer sbTablasNCM = new StringBuffer(" ");
	
			String prefijo = "PREFIJO_TABLA_IC_DOCUMENTO.";
			String prefijo2 = "PREFIJO_JOINS";
			String prefijo3 = "PREFIJO_JOINS_NCM"; // Fodea 002 - 2010.
	
			StringBuffer sbCondicion = new StringBuffer(
				"  WHERE d.ic_documento = " + prefijo + "ic_documento"   +
				"    AND ds.ic_documento = s.ic_documento"   +
				"    AND s.cc_acuse = a3.cc_acuse"   +
				"    AND d.ic_if = ie.ic_if"   +
				"    AND d.ic_if = I3.ic_if(+)"   +
				"    AND ie.ic_if = i.ic_if"   + prefijo2+
				"    AND ie.ic_epo = pe.ic_epo"   +
				"    AND p.ic_pyme = pe.ic_pyme"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_beneficiario = i2.ic_if (+)"   +
				"    AND d.ic_moneda = m.ic_moneda"  +
				"  	 AND d.ic_estatus_docto in (4,11) "+
				"    AND ds.ic_epo= ? "+
				"    AND d.ic_epo = ? "+
				"    AND ie.ic_epo = ? " +
				"    AND pe.ic_epo = ? " +
				"    AND a3.ic_producto_nafin = ? ");
			this.conditions.add(new Integer(claveEpo));
			this.conditions.add(new Integer(claveEpo));
			this.conditions.add(new Integer(claveEpo));
			this.conditions.add(new Integer(claveEpo));
			this.conditions.add(new Integer(1));
				
			StringBuffer sbCondicionNC = new StringBuffer(
				"  WHERE d.ic_documento = ds.ic_documento"   +
				"    AND d.ic_docto_asociado = s.ic_documento"   +
				"    AND s.cc_acuse = a3.cc_acuse"   +
				"    AND d.ic_if = ie.ic_if"   +
				"    AND d.ic_if = I3.ic_if(+)"   +
				"    AND ie.ic_if = i.ic_if"   + prefijo2+
				"    AND ie.ic_epo = pe.ic_epo"   +
				"    AND p.ic_pyme = pe.ic_pyme"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_beneficiario = i2.ic_if (+)"   +
				"    AND d.ic_moneda = m.ic_moneda"  +
				"    AND ds.ic_epo= ? " +
				"    AND d.ic_epo = ? " +
				"    AND ie.ic_epo = ? " +
				"    AND pe.ic_epo = ? " +
				" 	  AND a3.ic_producto_nafin = ? " +				
				"    AND d.ic_estatus_docto in (4,11) ");	//No me gust� la idea de dejarlo fijo... pero como correcci�n rapida es lo mas facil.
	
			StringBuffer sbCondicionNCM = new StringBuffer(
				"  WHERE d.ic_documento = ds.ic_documento"   +
				"	  AND nd.ic_documento     = s.ic_documento "  +
				"	  AND nd.ic_nota_credito  = d.ic_documento "  +
				"    AND s.cc_acuse = a3.cc_acuse"   +
				"    AND d.ic_if = ie.ic_if"   +
				"    AND d.ic_if = I3.ic_if(+)"   +
				"    AND ie.ic_if = i.ic_if"   + prefijo3+
				"    AND ie.ic_epo = pe.ic_epo"   +
				"    AND p.ic_pyme = pe.ic_pyme"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_beneficiario = i2.ic_if (+)"   +
				"    AND d.ic_moneda = m.ic_moneda"  +
				"    AND ds.ic_epo= ? " +
				"    AND d.ic_epo = ? " +
				"    AND ie.ic_epo = ? " +
				"    AND pe.ic_epo = ? " +
				" 	  AND a3.ic_producto_nafin = ? " +				
				"    AND d.ic_estatus_docto in (4,11) " +
				"    AND d.cs_dscto_especial = 'C' ");
		
			if(("".equals(ic_if) || ic_if== null) &&
				("".equals(df_fecha_notMin) || df_fecha_notMin== null) &&
				("".equals(df_fecha_notMax) || df_fecha_notMax== null) &&
				("".equals(df_fecha_vencMin) || df_fecha_vencMin== null) &&
				("".equals(df_fecha_vencMax) || df_fecha_vencMax== null) &&
				("".equals(cc_acuse3) || cc_acuse3 == null) &&
				("".equals(clavePyme) || clavePyme == null) &&//FODEA 028 - 2010 ACF
				("".equals(ic_documento) || ic_documento == null)
				||
				(!"".equals(df_fecha_notMin) && df_fecha_notMin != null) ||
				(!"".equals(df_fecha_notMax) && df_fecha_notMax != null) ||
				(!"".equals(cc_acuse3) && cc_acuse3 != null)) {
				//Si no hay ningun criterio de busqueda o los criterios de busqueda
				//involucran a la fecha de notificacion o clave del acuse...
				//Entra en esta secci�n
				prefijoTablaJoin = "s.";
				prefijoJoins = "	AND d.ic_pyme = pe.ic_pyme " +
									 "	AND d.ic_epo = pe.ic_epo ";
				
				//if (iNoCliente.equals("256") || iNoCliente.equals("30")|| iNoCliente.equals("7")|| iNoCliente.equals("2")){
					//sHint = " /*+ leading(a3) */ ";
				//} else {
					//sHint = " /*+ leading(a3) use_nl(s,ds,d,pe,p,ie,i,i2,m)*/ ";
				//}
				//sHintNC = " /*+ use_nl(s,ds,d,pe,p,ie,i,i2,m) leading(d) */ ";
				//sHintNCM = " /*+ use_nl(d nd s a3 ds pe p ie i i2 m tf) leading(d) */ ";
				
				sbTablas.append(
					" 	com_acuse3 a3,"   +
					"  com_solicitud s,"   +
					"  com_docto_seleccionado ds,    "   +
					"  com_documento d,"   +
					"     comrel_pyme_epo pe,"   +
					"  comcat_pyme p,"   +
					" 	  comrel_if_epo  ie,"   +
					" 	comcat_if i,"   +
					" 	comcat_if i2,"   +
					" 	comcat_if I3,"+ 
					"  comcat_moneda m, "+
					"  comcat_tipo_factoraje tf " );
				sbTablasNC.append(
					" 	com_acuse3 a3,"   +
					"  com_solicitud s,"   +
					"  com_documento d,"   +
					"  com_docto_seleccionado ds,    "   +
					"     comrel_pyme_epo  pe,"   +
					"  comcat_pyme p,"   +
					" 	comrel_if_epo  ie,"   +
					" 	comcat_if i,"   +
					" 	comcat_if i2,"   +
					" 	comcat_if I3,"+ 
					"  comcat_moneda m, " +
					"  comcat_tipo_factoraje tf ");
				sbTablasNCM.append(
					" 	com_acuse3 a3,"   +
					"  com_solicitud s,"   +
					"  com_documento d,"   +
					"  com_docto_seleccionado ds,    "   +
					"  comrel_pyme_epo  pe,"   +
					"  comcat_pyme p,"   +
					" 	comrel_if_epo  ie,"   +
					" 	comcat_if i,"   +
					" 	comcat_if i2,"   +
					" 	comcat_if I3," +
					"  comcat_moneda m, " +
					"  comcat_tipo_factoraje tf, " +
					"  comrel_nota_docto nd ");
				if(("".equals(df_fecha_notMin) || df_fecha_notMin== null) &&
					 ("".equals(df_fecha_notMax) || df_fecha_notMax== null) &&
					 ("".equals(cc_acuse3) || cc_acuse3 == null) ) {
						//Si todos los criterios de busqueda vienen vacios
						//se toman los operados del d�a unicamente
						sbCondicionAdic.append(" and A3.df_fecha_hora >= TRUNC(SYSDATE) ");
						sbCondicionAdic.append(" and A3.df_fecha_hora < TRUNC(SYSDATE+1) ");
				} else {
					//Se proporciono por lo menos un parametro
					if(!"".equals(df_fecha_notMin) && df_fecha_notMin != null) {
						if(!"".equals(df_fecha_notMax)) {
							sbCondicionAdic.append(" and A3.df_fecha_hora >= TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
							sbCondicionAdic.append(" and A3.df_fecha_hora < TRUNC(TO_DATE(?,'dd/mm/yyyy')+1) ");
							this.conditions.add(df_fecha_notMin);
							this.conditions.add(df_fecha_notMax);
						}
					}
					if (!"".equals(cc_acuse3) && cc_acuse3 != null) {
						sbCondicionAdic.append(" AND A3.cc_acuse = ? ");
						this.conditions.add(cc_acuse3);
					}
				} //Fin del if-else por el numero de parametros
			} else {
				//Esta secci�n se ejecuta cuando al menos hay un parametro de busqueda
				//que no tenga relacion con el acuse.
	
				//Y por motivos de optimizacion de la consulta, si la consulta no involucra el acuse,
				//se requiere hacer un query diferente.
				prefijoTablaJoin = "ds.";
	
				sHintNC = " /*+ use_nl(d ds s pe p a3 ie i i2 m) leading(d) */ ";
				sHintNCM = " /*+ use_nl(d nd s a3 ds pe p ie i i2 m tf) leading(d) */ ";
				sbTablas.append(
					"      com_documento d, "+
					"      com_docto_seleccionado ds, "+
					"      com_solicitud s, "+
					"      comcat_pyme p, "+
					"      comrel_pyme_epo pe, "+
					"      com_acuse3 a3, "+
					"      comrel_if_epo ie, "+
					"      comcat_if i, "+
					"      comcat_if i2, "+
					"      comcat_if I3, "+
					"      comcat_moneda m, "+
					"      comcat_tipo_factoraje tf ");
				sbTablasNC.append(
					"      com_documento d, "+
					"      com_docto_seleccionado ds, "+
					"      com_solicitud s, "+
					"      comcat_pyme p, "+
					"      comrel_pyme_epo pe, "+
					"      com_acuse3 a3, "+
					"      comrel_if_epo ie, "+
					"      comcat_if i, "+
					"      comcat_if i2, "+
					"      comcat_if I3, "+
					"      comcat_moneda m, "+
					"      comcat_tipo_factoraje tf ");
				sbTablasNCM.append(
					"      com_documento d, "+
					"      com_docto_seleccionado ds, "+
					"      com_solicitud s, "+
					"      comcat_pyme p, "+
					"      comrel_pyme_epo pe, "+
					"      com_acuse3 a3, "+
					"      comrel_if_epo ie, "+
					"      comcat_if i, "+
					"      comcat_if i2, "+
					"      comcat_if I3, "+
					"      comcat_moneda m, "+
					"      comcat_tipo_factoraje tf, "+
					"      comrel_nota_docto nd ");
	
			} //fin else
			if(!"".equals(df_fecha_vencMin) && df_fecha_vencMin != null) {
				if(!"".equals(df_fecha_vencMax)) {
					sHint = " /*+ index(d IN_COM_DOCUMENTO_09_NUK) leading(d) USE_NL (d ds s pe p a3 ie i i2 m) */ ";
					//sHintNC = sHint;
					//sHintNCM = " /*+ index(d IN_COM_DOCUMENTO_09_NUK) leading(d) USE_NL (d ds s pe p a3 ie i i2 m nd) */ ";
					sbCondicionAdic.append(" and D.df_fecha_venc >= TO_DATE(?,'dd/mm/yyyy') ");
					sbCondicionAdic.append(" and D.df_fecha_venc < TO_DATE(?,'dd/mm/yyyy')+1 ");
	
					this.conditions.add(df_fecha_vencMin);
					this.conditions.add(df_fecha_vencMax);
				}else
        {
          sHint = " /*+ leading(a3)*/ ";
        }
			}else
      {
        //sHint = " /*+ leading(a3)*/ ";
        sHint = " /*+ ordered*/ ";
      }
      
			if (!"".equals(ic_if) && ic_if != null) {    
				sbCondicionAdic.append(" AND d.ic_if = ?  AND ie.ic_if = ?  AND i.ic_if = ? ");
				this.conditions.add(new Integer(ic_if));
				this.conditions.add(new Integer(ic_if));
				this.conditions.add(new Integer(ic_if));
			}
	
			if(!ic_documento.equals("")){
				sbCondicionAdic.append(" AND D.ic_documento = ? ");
				this.conditions.add(new Double(ic_documento));
			}
			//FODEA 028 - 2010 ACF (I)
			if(!clavePyme.equals("")){
				sbCondicionAdic.append(" AND d.ic_pyme = ? ");
				this.conditions.add(new Integer(clavePyme));
			}
			//FODEA 028 - 2010 ACF (I)
			sbCondicionAdic.append(" AND d.cs_dscto_especial = tf.cc_tipo_factoraje ");
			int indice = sbCondicion.toString().indexOf(prefijo);
			//De acuerdo a las condiciones adicionales, se determino que prefijo aplica
			//para la optimizacion: tomar s.ic_documento o ds.ic_documento
			sbCondicion.replace(indice, indice + prefijo.length(), prefijoTablaJoin);
			int indice2 = sbCondicion.toString().indexOf(prefijo2);
			int indice3 = sbCondicionNC.toString().indexOf(prefijo2);
			int indice4 = sbCondicionNCM.toString().indexOf(prefijo3);
			sbCondicion.replace(indice2, indice2 + prefijo2.length(), prefijoJoins);
			sbCondicionNC.replace(indice3, indice3 + prefijo2.length(), prefijoJoins);
			sbCondicionNCM.replace(indice4, indice4 + prefijo3.length(), prefijoJoins);
	
			query_Reporte =
				"SELECT "+sHint+ " " + sbColumnas.toString()+sbTablas.toString()+sbCondicion.toString()+sbCondicionAdic.toString();
	
			if("S".equals(operaNC)) {
	
				query_Reporte =
					query_Reporte+
					" UNION ALL "+
					"SELECT "+sHintNC + " " + sbColumnasNC.toString()+sbTablasNC.toString()+sbCondicionNC.toString()+sbCondicionAdic.toString();
	
				conditionsNCM.addAll(conditions); // Fodea 002 - 2010. Condiciones Notas de Credito Multiple
				//Para que la siguiente linea funcione, las variables bind deben de aplicar en el mismo orden en los dos queries.
				this.conditions.addAll(conditions);
	
				// Fodea 002 - 2010. Notas de Credito Multiple
				query_Reporte =
					query_Reporte+
					" UNION ALL "+
					"SELECT "+sHintNCM + " DISTINCT " + sbColumnas.toString()+sbTablasNCM.toString()+sbCondicionNCM.toString()+sbCondicionAdic.toString();
				//Para que la siguiente linea funcione, las variables bind deben de aplicar en el mismo orden en los dos queries.
				this.conditions.addAll(conditionsNCM);
	
			}//if("S".equals(operaNC))
		}
		
		query_Reporte=  query_Reporte+" AND i.ic_if = i3.ic_if";
		
		log.debug("query_Reporte::"+query_Reporte);
		log.debug("conditions::"+this.conditions);
		log.info("getDocumentQueryFile(S) ::..");
		return query_Reporte;
		
	}
	
	
	/**
	 * metodo que genera archivo por pagina
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		
		boolean bOperaFactorajeVencInfonavit  = false;	
		boolean operaNotasDeCredito                = false;	// Fodea 002 - 2010
		boolean aplicarNotasDeCreditoAVariosDoctos = false;	// Fodea 002 - 2010
		boolean estaHabilitadoNumeroSIAFF  = false;
		String bOperaFactorajeDistribuido ="N"; 
		boolean agregarNumeroSIAFFEnFilaNueva = true;
		Vector vctContenidoCampos = new Vector();
		String ifNombre   ="", fechaNotificacion ="", acuseNotificacion = "", pymeNombre  ="", 	numeroDocto  ="", fechaEmision   ="", 	
					fechaVencimiento  = "", monedaNombre   ="", 	monto    ="",  strPorcentaje   ="", trMontoDescuento = "", 	sucursal ="", 	banco  ="", 
					noCuenta  ="", 	noPyme  ="", 	ic_documento   ="", fechaVencimientoP = "", 	beneficiario   ="", por_beneficiario  = "",  
					importe_a_recibir ="", tasaAceptada = "",  strCT_REFERENCIA = "", 	rs_docto_asociado ="",rs_tipo_factoraje = "", rs_tipo_factoraje_desc ="",
					strMontoDescuento ="" ,	fechaEntrega 	="", 	tipoCompra 	="", 	clavePresupuestaria ="", 	periodo ="" ,	numeroSIAFF="",
					strCampo ="", ic_moneda  =""; 
					int    colspan = 0, i=0,  monedaNacional = 0, monedaDolar = 0;
			double montoN=0.0,  montoD=0.0, montoDescontarN=0.0, montoDescontarD=0.0;			
			
			
		try {
	
			ParametrosDescuento BeanParamDscto	= ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		
			bOperaFactorajeDistribuido = BeanParamDscto.getfacVencido( claveEpo,  "1") ;
			
			estaHabilitadoNumeroSIAFF 	= BeanParamDscto.estaHabilitadoNumeroSIAFF(claveEpo); // VERIFICAR SI ESTA HABILITADO EL NUMERO SIAFF 
		
			Hashtable alParamEPO = new Hashtable(); 			
			if(!claveEpo.equals("")) {
				alParamEPO = BeanParamDscto.getParametrosEPO(claveEpo, 1);
				if (alParamEPO!=null) {
				bOperaFactorajeVencInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true; //FODEA 042 - Agosto/2009
				}
			}
	
			// VERIFICAR SI LA EPO TIENE PARAMETRIZADO EL USO DE UNA NOTA DE CREDITO EN MULTIPLES DOCUMENTOS	
			// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Operacion de las Notas de Credito
			operaNotasDeCredito                = BeanParamDscto.operaNotasDeCreditoParaDescuentoElectronico(claveEpo);
			// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Opcion de Aplicar una Nota de Credito a Varios Documentos
			aplicarNotasDeCreditoAVariosDoctos = BeanParamDscto.hasAplicarNotaDeCreditoAVariosDoctosEnabled(claveEpo);
			
			List  campos = BeanParamDscto.getCamposAdicionales( claveEpo,  "1");
				
		
			if ("PDF".equals(tipo)) {	
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				 
				pdfDoc.addText(" ","formasB",ComunesPDF.CENTER);
				pdfDoc.addText(mensaje,"formasB",ComunesPDF.JUSTIFIED);
				pdfDoc.addText(" ","formasB",ComunesPDF.CENTER);
			
				
				while (rs.next())	{
				
					vctContenidoCampos.clear();
					ifNombre          = (rs.getString( 1) == null) ? "" :    rs.getString( 1).trim();
					fechaNotificacion = (rs.getString( 2) == null) ? "" :    rs.getString( 2).trim();
					acuseNotificacion = (rs.getString( 3) == null) ? "" :    rs.getString( 3).trim();
					pymeNombre        = (rs.getString( 4) == null) ? "" :    rs.getString( 4).trim();
					numeroDocto       = (rs.getString( 5) == null) ? "" :    rs.getString( 5).trim();
					fechaEmision      = (rs.getString( 6) == null) ? "" :    rs.getString( 6).trim();
					fechaVencimiento  = (rs.getString( 7) == null) ? "" :    rs.getString( 7).trim();
					monedaNombre      = (rs.getString( 8) == null) ? "" :    rs.getString( 8).trim();
					monto             = (rs.getString( 9) == null) ? "0.0" : rs.getString( 9).trim();
					strPorcentaje     = (rs.getString(10) == null) ? "0.0" : rs.getString(10).trim();
					strMontoDescuento = (rs.getString(11) == null) ? "0.0" : rs.getString(11).trim();
					sucursal          = (rs.getString(12) == null) ? "" :    rs.getString(12).trim();
					banco             = (rs.getString(13) == null) ? "" :    rs.getString(13).trim();
					noCuenta          = (rs.getString(14) == null) ? "" :    rs.getString(14).trim();
					noPyme            = (rs.getString(15) == null) ? "" :    rs.getString(15).trim();
					ic_documento      = (rs.getString(17) == null) ? "" :    rs.getString(17).trim();			
					fechaVencimientoP = (rs.getString("DF_FECHA_VEN_PYME")==null)?"":rs.getString("DF_FECHA_VEN_PYME").trim();
					beneficiario      = (rs.getString("beneficiario")      == null) ? "" : rs.getString("beneficiario").trim();
					por_beneficiario  = (rs.getString("por_beneficiario")  == null) ? "" : rs.getString("por_beneficiario").trim();
					importe_a_recibir = (rs.getString("imp_recibir_bene") == null) ? "" : rs.getString("imp_recibir_bene").trim();
					tasaAceptada = (rs.getString("in_tasa_aceptada") == null) ? "" : rs.getString("in_tasa_aceptada");						
					strCT_REFERENCIA = (rs.getString("REFERENCIA") == null) ? "" : rs.getString("REFERENCIA");
					rs_docto_asociado = (rs.getString("DOCT_APLICADO") == null) ? "" :    rs.getString("DOCT_APLICADO").trim();
					rs_tipo_factoraje = (rs.getString("cs_dscto_especial") == null) ? "" :    rs.getString("cs_dscto_especial").trim();
					rs_tipo_factoraje_desc = (rs.getString("TIPO_FACTORAJE") == null) ? "" :    rs.getString("TIPO_FACTORAJE"); 					
					ic_moneda =  (rs.getString("IC_MONEDA") == null) ? "" :    rs.getString("IC_MONEDA"); 
					numeroSIAFF  = (rs.getString("DIG_IDENTIFICADOR")==null)?"":rs.getString("DIG_IDENTIFICADOR");
					
					int cam=1;
					for (int x=0; x<campos.size(); x++) { 
						String nombre ="CAMPO"+cam;					
						strCampo = rs.getString(nombre);									
						strCampo = (strCampo == null) ? "" : strCampo;					
						vctContenidoCampos.add(strCampo);				
						cam++;						
					}  				  
								
					int cols = 13;
					if("S".equals(operaNC))
						cols++;
					if("S".equals(operaFVPyme))
						cols++;
					float widthsDocs[] = new float[cols];
					for(int d=0;d<cols;d++) {
						if(d>0)
							widthsDocs[d] = 1;
						else if(d==0)
							widthsDocs[d] = (float)0.5;
						else
							widthsDocs[d] = 1;   
					}//for

					//si existe la parametrizacion
					if( banderaTablaDoctos.equals("1") ){
						fechaEntrega 				= (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
						tipoCompra 					= (rs.getString("TIPO_COMPRA")==null)?"":rs.getString("TIPO_COMPRA");
						clavePresupuestaria = (rs.getString("CLASIFICADOR")==null)?"":rs.getString("CLASIFICADOR");
						periodo 						= (rs.getString("PLAZO_MAXIMO")==null)?"":rs.getString("PLAZO_MAXIMO");
					} 
					if(i==0) {
						pdfDoc.setTable(cols,100,widthsDocs);
						pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Intermediario Finaciero","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de Notificaci�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Acuse de Notificaci�n" ,"celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("N�mero de Proveedor","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Nombre del proveedor ","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de Emisi�n ","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de Vencimiento ","celda01",ComunesPDF.CENTER);		
						if("S".equals(operaFVPyme)) {
							pdfDoc.setCell("Fecha de Vencimiento del Proveedor","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
						if("S".equals(operaNC)){
							pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);	
						}
						pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);		
						pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);		
						pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);	
						
						pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Clave del Banco","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Banco","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("N�mero de Cuenta","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
					
						if("S".equals(operaNC)) {						
							pdfDoc.setCell("Documento Aplicado","celda01",ComunesPDF.CENTER);
						}
						
						for (int x=0; x<campos.size(); x++) { 
							List lovRegistro = (List) campos.get(x);
							strCampos = (String) lovRegistro.get(1);	
							pdfDoc.setCell(strCampos,"celda01",ComunesPDF.CENTER);
						}	
						colspan =  cols - 5 - campos.size();
						
						if("S".equals(operaNC)) {
							cols--;
							colspan--;
						}
						
						if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit) {
							colspan -= 3;
							pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Importe a recibir beneficiario","celda01",ComunesPDF.CENTER);
						}//if(bOperaFactorajeDistribuido)
	
						if(estaHabilitadoNumeroSIAFF && colspan>0){ // El acuse de registro individual se agregara en la misma linea
							pdfDoc.setCell("D�gito Identificador","celda01",ComunesPDF.CENTER);						
							colspan--;
							agregarNumeroSIAFFEnFilaNueva = false;
						}
						if(colspan>0)
							pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER,colspan);
	
						// Mostrar encabezado del Numero SIAFF
						if(estaHabilitadoNumeroSIAFF && agregarNumeroSIAFFEnFilaNueva ){
							pdfDoc.setCell("C","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("D�gito Identificador","celda01",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);						
						}
						String indice_columna = "C";
						if(estaHabilitadoNumeroSIAFF && agregarNumeroSIAFFEnFilaNueva){
							//esto significa que ya hay columna C entonces la nueva se va a llamar D
							indice_columna = "D";
						}
						//vars para parametrizacion docto
						if( banderaTablaDoctos.equals("1") ){
							pdfDoc.setCell(indice_columna,"celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha de Recepci�n de Bienes y Servicios","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Tipo de Compra (procedimiento)","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Clasificador por Objeto del Gasto","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Plazo M�ximo","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);

							if("S".equals(operaFVPyme)) {
								pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							}
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							if("S".equals(operaNC))
								pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						}
					pdfDoc.setHeaders();
				}//if(i==0)
				
				
				pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ifNombre,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaNotificacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(acuseNotificacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(noPyme,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(pymeNombre,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numeroDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
	 			if("S".equals(operaFVPyme)) {
					pdfDoc.setCell(fechaVencimientoP,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.setCell(monedaNombre,"formas",ComunesPDF.CENTER);
	 			if("S".equals(operaNC)){
					pdfDoc.setCell(rs_tipo_factoraje_desc,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.setCell("$ "+Comunes.formatoMoneda2(monto, false),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(Comunes.formatoDecimal(strPorcentaje,0, false)+" %","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$ "+Comunes.formatoMoneda2(strMontoDescuento, false),"formas",ComunesPDF.RIGHT);

				pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(sucursal,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(banco,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(noCuenta,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell((strCT_REFERENCIA.equals("")) ? " " : strCT_REFERENCIA,"formas",ComunesPDF.CENTER);
	 			if("S".equals(operaNC)){
					pdfDoc.setCell(rs_docto_asociado,"formas",ComunesPDF.CENTER);
				}
				for (int x=0; x<campos.size(); x++) { 				 
					pdfDoc.setCell((((String)vctContenidoCampos.get(x)).equals("") ? " " : (String)vctContenidoCampos.get(x)),"formas",ComunesPDF.CENTER);
				}
				i++;
				if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit) {
					pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(por_beneficiario,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(importe_a_recibir,"formas",ComunesPDF.CENTER);
				}//if(bOperaFactorajeDistribuido)

				if(estaHabilitadoNumeroSIAFF && !agregarNumeroSIAFFEnFilaNueva){
					pdfDoc.setCell(numeroSIAFF,"formas",ComunesPDF.CENTER);
				}

				if(colspan>0)
					pdfDoc.setCell("","formas",ComunesPDF.CENTER,colspan);

				if(estaHabilitadoNumeroSIAFF && agregarNumeroSIAFFEnFilaNueva ){
						pdfDoc.setCell("C","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numeroSIAFF,"formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					 //pdfDoc.setCell("","formas",ComunesPDF.CENTER,11);
				}
				String indice_columna = "C";

				if(estaHabilitadoNumeroSIAFF && agregarNumeroSIAFFEnFilaNueva){
					//esto significa que ya hay columna C entonces la nueva se va a llamar D
					indice_columna = "D";
				}
				//vars para parametrizacion docto
				if( banderaTablaDoctos.equals("1") ){
					pdfDoc.setCell(indice_columna,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaEntrega,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoCompra,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(clavePresupuestaria,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(periodo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		 			if("S".equals(operaFVPyme)) {
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
		 			if("S".equals(operaNC))
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				}
		
			//tOTSLES
	
			if(ic_moneda.equals("1")) {
				monedaNacional ++;
				montoN +=  Double.parseDouble((String)monto);
				montoDescontarN +=  Double.parseDouble((String)strMontoDescuento);
				
			}else {
				 monedaDolar ++;				 
				montoD +=  Double.parseDouble((String)monto);
				montoDescontarD +=  Double.parseDouble((String)strMontoDescuento);
			}
		
			} //fin del while
		
			pdfDoc.addTable();
			pdfDoc.endDocument();			
		}			
	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				//reg.close();
			} catch(Exception e) {}
		
		}	
		return nombreArchivo;
	}
	
	
		public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipoArchivo) {
		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		
		boolean bOperaFactorajeVencInfonavit  = false;	
		boolean operaNotasDeCredito                = false;	// Fodea 002 - 2010
		boolean aplicarNotasDeCreditoAVariosDoctos = false;	// Fodea 002 - 2010
		boolean estaHabilitadoNumeroSIAFF  = false;
		String bOperaFactorajeDistribuido ="N"; 
		boolean agregarNumeroSIAFFEnFilaNueva = true;
		Vector vctContenidoCampos = new Vector();
		String ifNombre   ="", fechaNotificacion ="", acuseNotificacion = "", pymeNombre  ="", 	numeroDocto  ="", fechaEmision   ="", 	
					fechaVencimiento  = "", monedaNombre   ="", 	monto    ="",  strPorcentaje   ="", trMontoDescuento = "", 	sucursal ="", 	banco  ="", 
					noCuenta  ="", 	noPyme  ="", 	ic_documento   ="", fechaVencimientoP = "", 	beneficiario   ="", por_beneficiario  = "",  
					importe_a_recibir ="", tasaAceptada = "",  strCT_REFERENCIA = "", 	rs_docto_asociado ="",rs_tipo_factoraje = "", rs_tipo_factoraje_desc ="",
					strMontoDescuento ="" ,	fechaEntrega 	="", 	tipoCompra 	="", 	clavePresupuestaria ="", 	periodo ="" ,	numeroSIAFF="",
					strCampo ="", ic_moneda  ="", noCuentaCLABE ="", rfc_IF ="", rs_monto_ant ="", codigoHASH   = "";
		int    colspan = 0, i=0,  monedaNacional = 0, monedaDolar = 0;
		double montoN=0.0,  montoD=0.0, montoDescontarN=0.0, montoDescontarD=0.0;			
		
					
		try {
	
			ParametrosDescuento 		BeanParamDscto					= ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
		
			ISeleccionDocumento BeanSeleccionDocumento 			= ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
			
			bOperaFactorajeDistribuido = BeanParamDscto.getfacVencido( claveEpo,  "1") ;
			
			estaHabilitadoNumeroSIAFF 	= BeanParamDscto.estaHabilitadoNumeroSIAFF(claveEpo); // VERIFICAR SI ESTA HABILITADO EL NUMERO SIAFF 
		
			Hashtable alParamEPO = new Hashtable(); 			
			if(!claveEpo.equals("")) {
				alParamEPO = BeanParamDscto.getParametrosEPO(claveEpo, 1);
				if (alParamEPO!=null) {
				bOperaFactorajeVencInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true; //FODEA 042 - Agosto/2009
				}
			}
	
			// VERIFICAR SI LA EPO TIENE PARAMETRIZADO EL USO DE UNA NOTA DE CREDITO EN MULTIPLES DOCUMENTOS	
			// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Operacion de las Notas de Credito
			operaNotasDeCredito                = BeanParamDscto.operaNotasDeCreditoParaDescuentoElectronico(claveEpo);
			// Fodea 002 - 2010. Verificar si la EPO tiene Habilitada la Opcion de Aplicar una Nota de Credito a Varios Documentos
			aplicarNotasDeCreditoAVariosDoctos = BeanParamDscto.hasAplicarNotaDeCreditoAVariosDoctosEnabled(claveEpo);
			
			List  campos = BeanParamDscto.getCamposAdicionales( claveEpo,  "1");
			StringBuffer contenidoArchivo = new StringBuffer();
			
				
			if ("CSV".equals(tipoArchivo) &&  tipo.equals("Interfase-EPO211")) {
				contenidoArchivo.append("Intermediario financiero,Fecha de notificacion,Acuse de notificacion,Numero de Proveedor,Nombre del proveedor,Numero de documento,Fecha de emision,Fecha de vencimiento,Moneda,Monto,Sucursal,Banco,Numero de Cuenta,Porcentaje Descuento,Monto de Descuento,Tasa de Descuento,Referencia");
				for (int x=0; x<campos.size(); x++) { 
					List lovRegistro = (List) campos.get(x);
					strCampos = (String) lovRegistro.get(1);	
					contenidoArchivo.append(","+strCampos);
				}		
				contenidoArchivo.append("\r\n");

				while(rs.next()) {
					contenidoArchivo.append( (rs.getString("nombreIf")==null?"":rs.getString("nombreIf").replace(',',' '))+","+
						(rs.getString("fecha_not")==null?"":rs.getString("fecha_not"))+","+
						(rs.getString("cc_acuse")==null?"":rs.getString("cc_acuse"))+","+
						(rs.getString("cg_pyme_epo_interno")==null?"":rs.getString("cg_pyme_epo_interno"))+","+
						(rs.getString("nombrePyme")==null?"":rs.getString("nombrePyme").replace(',',' '))+","+
						(rs.getString("ig_numero_docto")==null?"":rs.getString("ig_numero_docto"))+","+
						(rs.getString("fecha_emision")==null?"":rs.getString("fecha_emision"))+","+
						(rs.getString("fecha_vencimiento")==null?"":rs.getString("fecha_vencimiento"))+","+
						(rs.getString("nombreMoneda")==null?"":rs.getString("nombreMoneda"))+","+
						(rs.getString("fn_monto")==null?"":rs.getString("fn_monto"))+","+
						(rs.getString("ig_sucursal")==null?"":rs.getString("ig_sucursal"))+","+
						(rs.getString("cg_banco")==null?"":rs.getString("cg_banco").replace(',',' '))+","+
						(rs.getString("cg_num_cuenta")==null?"":rs.getString("cg_num_cuenta"))+","+
						(rs.getString("fn_porc_anticipo")==null?"":rs.getString("fn_porc_anticipo"))+","+
						(rs.getString("fn_monto_dscto")==null?"":rs.getString("fn_monto_dscto"))+","+
						(rs.getString("in_tasa_aceptada")==null?"":rs.getString("in_tasa_aceptada"))+","+
						(rs.getString("ct_referencia")==null?"":rs.getString("ct_referencia").replace(',',' ')) );						
						int cam=1;
						for (int x=0; x<campos.size(); x++) { 
							String nombre ="cg_campo"+cam;					
							strCampo = (rs.getString(nombre)==null?"":rs.getString(nombre))+","+     																
							contenidoArchivo.append(strCampo);				
							cam++;						
						}  					
						contenidoArchivo.append("\r\n");			
				}				
			}			
			
			if ("CSV".equals(tipoArchivo)) {				
				
				log.info("----------operaNC---------------"+operaNC);
			
				String 	encabezadoRFC = (tipo.equals("Interfase-RFC")) ?"RFC Intermediario Finaciero"+",":"";
				String docto = "",  factoraje = "";
				if("S".equals(operaNC) && ( tipo.equals("Archivo") ||  tipo.equals("ArchivoCLABE") ) ) {
					docto = "," +"Documento Aplicado";
					factoraje = ","+"Tipo Factoraje";
				} else if("S".equals(operaNC) && (tipo.equals("Archivo Detalle") || tipo.equals("Interfase Detalle"))) {
					docto = ","+"Monto Original"+","+ "Documento Aplicado";
					factoraje = ","+"Tipo Factoraje";
				}
				//Variable para archivo con cuenta clabe				
				StringBuffer contenidoArchivoCB = new StringBuffer();				
				StringBuffer contenidoArchivoCBIN = new StringBuffer();
				StringBuffer contenidoArchivoA = new StringBuffer();
				
				//para el archivo Generar CSV de todos los registros
				if(tipo.equals("Archivo") ||  tipo.equals("Archivo Detalle") ){
				
					contenidoArchivoA = new StringBuffer(	"Intermediario Finaciero , Fecha de Notificaci�n , Acuse de Notificaci�n, N�mero de Proveedor, "+
					" Nombre del proveedor, N�mero de Documento, Fecha de Emisi�n , Fecha Vencimiento, ");			
					if("S".equals(operaFVPyme)) { 			contenidoArchivoA.append("Fecha de Vencimiento del Proveedor"+","); 	}	
					contenidoArchivoA.append("Moneda "+factoraje+","+" Monto ,Sucursal, Banco , N�mero de Cuenta"+docto);
					if(estaHabilitadoNumeroSIAFF) { 		contenidoArchivoA.append(" , D�gito Identificador  ");			}
					if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit ){ 		contenidoArchivoA.append(","+" Beneficiario , % Beneficiario , Importe a recibir beneficiario"); 	}
					if(banderaTablaDoctos.equals("1")){    	contenidoArchivoA.append(",Fecha de Recepci�n de Bienes y Servicios,Tipo de Compra (procedimiento),Clasificador por Objeto del Gasto,Plazo M�ximo"); 			}
					contenidoArchivoA.append("\n");
					
				}
				
				//para el archivo Generar CSV de todos los registros CLAVE
				if(tipo.equals("ArchivoCLABE")){
					
					contenidoArchivoCB = new StringBuffer(	"Intermediario Finaciero , Fecha de Notificaci�n , Acuse de Notificaci�n, N�mero de Proveedor, "+
					" Nombre del proveedor, N�mero de Documento, Fecha de Emisi�n , Fecha Vencimiento, ");			
					if("S".equals(operaFVPyme)) {  contenidoArchivoCB.append("Fecha de Vencimiento del Proveedor"+","); 		}
					contenidoArchivoCB.append("Moneda  "+factoraje+" ,Monto , Sucursal , Banco , Cuenta Clabe "+	docto);
									// Agregar numero SIAFF, si este se encuentra habilitado
					if(estaHabilitadoNumeroSIAFF) {		contenidoArchivoCB.append(", D�gito Identificador  "); 		}
					if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit ){	contenidoArchivoCB.append(","+" Beneficiario, % Beneficiario , Importe a recibir beneficiario");			}
					//vars para parametrizacion docto
					if(banderaTablaDoctos.equals("1")){ 		contenidoArchivoCB.append(",Fecha de Recepcion de Bienes y Servicios,Tipo de Compra (procedimiento),Clasificador por Objeto del Gasto,Plazo M�ximo");  		}
					//** Contenido archivo cuenta CLABE
					contenidoArchivoCB.append("\n");
					
				}
				
				if(tipo.equals("Interfase Detalle")  || tipo.equals("Interfase Hash") ){
				
					contenidoArchivoA = new StringBuffer(	"Intermediario Finaciero , Fecha de Notificaci�n , Acuse de Notificaci�n, N�mero de Proveedor, "+
					" Nombre del proveedor, N�mero de Documento, Fecha de Emisi�n , Fecha Vencimiento, ");			
					if("S".equals(operaFVPyme)) { 		contenidoArchivoA.append("Fecha de Vencimiento del Proveedor"+","); 		}
					contenidoArchivoA.append("Moneda "+factoraje+","+" Monto ,Sucursal, Banco , N�mero de Cuenta"+docto);
					for (int x=0; x<campos.size(); x++) { 
						List lovRegistro = (List) campos.get(x);
						strCampos = (String) lovRegistro.get(1);	
						contenidoArchivoA.append(","+strCampos);
					}				
					// Agregar numero SIAFF, si este se encuentra habilitado
					if(estaHabilitadoNumeroSIAFF) { 	contenidoArchivoA.append(" , D�gito Identificador  ");  		}
					if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit ){ 		contenidoArchivoA.append(","+" Beneficiario , % Beneficiario , Importe a recibir beneficiario");  			}
					//vars para parametrizacion docto
					if(banderaTablaDoctos.equals("1")){  		contenidoArchivoA.append(",Fecha de Recepci�n de Bienes y Servicios,Tipo de Compra (procedimiento),Clasificador por Objeto del Gasto,Plazo M�ximo"); 			}
					contenidoArchivoA.append("\n");
				} 
				
				if(tipo.equals("Interfase")  || tipo.equals("Interfase-RFC")  || tipo.equals("Interfase ExpHash") ){
				
					contenidoArchivo = new StringBuffer(
					encabezadoRFC+"Intermediario Finaciero , Fecha de Notificaci�n, Acuse de Notificaci�n ,"+
										 " N�mero de Proveedor, Nombre del proveedor, N�mero de Documento, Fecha de Emisi�n , Fecha Vencimiento, ");	
					if("S".equals(operaFVPyme)) {  		contenidoArchivo.append("Fecha de Vencimiento del Proveedor ,"); 		}					
					contenidoArchivo.append("Moneda, Monto, Sucursal, Banco, N�mero de Cuenta, Porcentaje de Descuento, Monto a Descontar, Tasa de Descuento, Referencia ");
					for (int x=0; x<campos.size(); x++) { 
						List lovRegistro = (List) campos.get(x);
						strCampos = (String) lovRegistro.get(1);	
						contenidoArchivo.append(","+strCampos);
					}		
					if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit){ 		contenidoArchivo.append( ",Beneficiario,  % Beneficiario , Importe a recibir beneficiario");  		}
					// Agregar Columna de Numero SIAFF, si este numero se encuentra habilitado
					if(estaHabilitadoNumeroSIAFF){ 		contenidoArchivo.append(", D�gito Identificador "); 	}
					//vars para parametrizacion docto
					if(banderaTablaDoctos.equals("1")){  		contenidoArchivo.append(",Fecha de Recepci�n de Bienes y Servicios,Tipo de Compra (procedimiento),Clasificador por Objeto del Gasto,Plazo M�ximo"); 	}
					contenidoArchivo.append("\n");		
					
				}							
					
				if(tipo.equals("Interfase ExpClabe") ){
					contenidoArchivoCBIN = new StringBuffer(
						encabezadoRFC+"Intermediario Finaciero , Fecha de Notificaci�n, Acuse de Notificaci�n ,"+
											 " N�mero de Proveedor, Nombre del proveedor, N�mero de Documento, Fecha de Emisi�n , Fecha Vencimiento, ");	
					if("S".equals(operaFVPyme)) { 	contenidoArchivoCBIN.append("Fecha de Vencimiento del Proveedor ,"); 		}				
						//** Contenido archivo cuenta CLABE
					contenidoArchivoCBIN.append("Moneda  "+factoraje+","+" Monto, Sucursal, Banco, Cuenta Clabe , Porcentaje de Descuento , Monto a Descontar,  Tasa de Descuento,  Referencia ");
					for (int x=0; x<campos.size(); x++) { 
						List lovRegistro = (List) campos.get(x);
						strCampos = (String) lovRegistro.get(1);						
						contenidoArchivoCBIN.append(","+strCampos);
					}		
					if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit){ 		contenidoArchivoCBIN.append(", Beneficiario ,% Beneficiario ,  Importe a recibir beneficiario ");  		}
						// Agregar Columna de Numero SIAFF, si este numero se encuentra habilitado
					if(estaHabilitadoNumeroSIAFF) {  	contenidoArchivoCBIN.append(", D�gito Identificador"); 		}
					//vars para parametrizacion docto
					if(banderaTablaDoctos.equals("1")){  	contenidoArchivoCBIN.append(",Fecha de Recepci�n de Bienes y Servicios,Tipo de Compra (procedimiento),Clasificador por Objeto del Gasto,Plazo M�ximo");  		}
						contenidoArchivoCBIN.append("\n");
					} // FIN de: if (tipo.equals("Interfase ExpClabe"))
					
					
				//para los datos			
				while(rs.next()){
				
					vctContenidoCampos.clear();
					ifNombre          = (rs.getString( 1) == null) ? "" :    rs.getString( 1).trim();
					fechaNotificacion = (rs.getString( 2) == null) ? "" :    rs.getString( 2).trim();
					acuseNotificacion = (rs.getString( 3) == null) ? "" :    rs.getString( 3).trim();
					pymeNombre        = (rs.getString( 4) == null) ? "" :    rs.getString( 4).trim();
					numeroDocto       = (rs.getString( 5) == null) ? "" :    rs.getString( 5).trim();
					fechaEmision      = (rs.getString( 6) == null) ? "" :    rs.getString( 6).trim();
					fechaVencimiento  = (rs.getString( 7) == null) ? "" :    rs.getString( 7).trim();
					monedaNombre      = (rs.getString( 8) == null) ? "" :    rs.getString( 8).trim();
					monto             = (rs.getString( 9) == null) ? "0.0" : rs.getString( 9).trim();
					strPorcentaje     = (rs.getString(10) == null) ? "0.0" : rs.getString(10).trim();
					strMontoDescuento = (rs.getString(11) == null) ? "0.0" : rs.getString(11).trim();
					sucursal          = (rs.getString(12) == null) ? "" :    rs.getString(12).trim();
					banco             = (rs.getString(13) == null) ? "" :    rs.getString(13).trim();
					noCuenta          = (rs.getString(14) == null) ? "" :    rs.getString(14).trim();
					noCuentaCLABE     = (rs.getString("cuentaClabe") == null) ? "" :    rs.getString("cuentaClabe").trim();
					noPyme            = (rs.getString(15) == null) ? "" :    rs.getString(15).trim();
					ic_documento      = (rs.getString(17) == null) ? "" :    rs.getString(17).trim();
					fechaVencimientoP = (rs.getString("DF_FECHA_VEN_PYME") == null) ? "" :    rs.getString("DF_FECHA_VEN_PYME").trim();
					beneficiario      = (rs.getString("beneficiario")      == null) ? "" : rs.getString("beneficiario").trim();
					por_beneficiario  = (rs.getString("por_beneficiario")  == null) ? "" : rs.getString("por_beneficiario").trim();
					importe_a_recibir = (rs.getString("importe_a_recibir") == null) ? "" : rs.getString("importe_a_recibir").trim();
					tasaAceptada = (rs.getString("in_tasa_aceptada") == null) ? "" : rs.getString("in_tasa_aceptada");
					strCT_REFERENCIA = (rs.getString("CT_REFERENCIA") == null) ? "" : rs.getString("CT_REFERENCIA");
					rfc_IF = (tipo.equals("Interfase-RFC")) ? rs.getString("CG_RFC_IF")+",":"";
					rs_docto_asociado = (rs.getString("doctoasociado") == null) ? "" :    rs.getString("doctoasociado").trim();
					rs_tipo_factoraje = (rs.getString("cs_dscto_especial") == null) ? "" :    rs.getString("cs_dscto_especial").trim();
					rs_monto_ant = (rs.getString("fn_monto_ant") == null) ? "" :    rs.getString("fn_monto_ant").trim();
					rs_tipo_factoraje_desc = (rs.getString("TIPO_FACTORAJE") == null) ? "" :    rs.getString("TIPO_FACTORAJE");
					numeroSIAFF  = (rs.getString("DIG_IDENTIFICADOR")==null)?"":rs.getString("DIG_IDENTIFICADOR");
		
					if(banderaTablaDoctos.equals("1")){	
						fechaEntrega 				= (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
						tipoCompra 					= (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
						clavePresupuestaria = (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
						periodo 						= (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");			
					}
					int cam=1;
					for (int x=0; x<campos.size(); x++) { 
						String nombre ="CAMPO"+cam;					
						strCampo = rs.getString(nombre);									
						strCampo = (strCampo == null) ? "" : strCampo;					
						vctContenidoCampos.add(strCampo);				
						cam++;						
					}  		
					ifNombre = ifNombre.replace('*',' ').trim(); pymeNombre = pymeNombre.replace('*',' ').trim();
					// Fodea 002 - 2010
					if("S".equals(operaNC) && (tipo.equals("Archivo Detalle")||tipo.equals("Interfase Detalle"))) {
						if(rs_tipo_factoraje.equals("C") && operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos ){
							String icNotaCredito = ic_documento;
							rs_docto_asociado = "\"" + BeanSeleccionDocumento.getNumerosDeDocumento(icNotaCredito,",") + "\"";
						}
					}
					if("S".equals(operaNC) && ( tipo.equals("Archivo")  || tipo.equals("ArchivoCLABE")) ) { 
						docto = ","+rs_docto_asociado.replace(',',' ');
						factoraje = ","+rs_tipo_factoraje_desc.replace(',',' ');
					} else if("S".equals(operaNC) && (tipo.equals("Archivo Detalle")||tipo.equals("Interfase Detalle"))) {
						if(rs_tipo_factoraje.equals("C") && operaNotasDeCredito && aplicarNotasDeCreditoAVariosDoctos){ 
							docto = ","+rs_monto_ant+","+rs_docto_asociado;
						}else{
							docto = ","+rs_monto_ant+","+rs_docto_asociado.replace(',',' ');
						}
						factoraje = ","+rs_tipo_factoraje_desc.replace(',',' ');
					}
					
					//para el archivo Generar CSV de todos los registros
					if(tipo.equals("Archivo") ||  tipo.equals("Archivo Detalle")){
						contenidoArchivoA.append(	ifNombre.replace(',',' ')+","+	fechaNotificacion+","+	acuseNotificacion+","+noPyme+","+
							pymeNombre.replace(',',' ')+","+numeroDocto+","+fechaEmision+","+	fechaVencimiento+",");
						if("S".equals(operaFVPyme)) { 		contenidoArchivoA.append( 	fechaVencimientoP+","); 		}
						contenidoArchivoA.append(		monedaNombre+factoraje+","+		Comunes.formatoDecimal(monto,2,false)+","+
							sucursal+","+banco.replace(',',' ')+","+	noCuenta+docto);
						// Agregar numero SIAFF, si este se encuentra habilitado
						contenidoArchivoA.append(estaHabilitadoNumeroSIAFF?","+numeroSIAFF:"");						
						if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit){					
							contenidoArchivoA.append("," + beneficiario.replace(',',' ') + "," + por_beneficiario + "," + importe_a_recibir);						
						}										
						//vars para parametrizacion docto
						if(banderaTablaDoctos.equals("1")){	 	contenidoArchivoA.append( ","+fechaEntrega+","+tipoCompra+","+clavePresupuestaria+","+periodo);  }				
						contenidoArchivoA.append("\n");				
					}
					
					if(tipo.equals("ArchivoCLABE")) {
						contenidoArchivoCB.append( 	ifNombre.replace(',',' ')+","+ 	fechaNotificacion+","+ 	acuseNotificacion+","+ 	noPyme+","+
							pymeNombre.replace(',',' ')+","+ 	numeroDocto+","+ 	fechaEmision+","+ 	fechaVencimiento+",");
						if("S".equals(operaFVPyme)) { 	contenidoArchivoCB.append( 	fechaVencimientoP+","); 		}
						contenidoArchivoCB.append( 	monedaNombre+factoraje+","+ 	Comunes.formatoDecimal(monto,2,false)+","+sucursal+","+
						banco.replace(',',' ')+","+	noCuentaCLABE+docto);							
						contenidoArchivoCB.append(estaHabilitadoNumeroSIAFF?","+numeroSIAFF:"");
						if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit){
							contenidoArchivoCB.append("," + beneficiario.replace(',',' ') + "," + por_beneficiario + "," + importe_a_recibir);
						}
						if(banderaTablaDoctos.equals("1")){	contenidoArchivoCB.append( ","+fechaEntrega+","+tipoCompra+","+clavePresupuestaria+","+periodo); 	}
						contenidoArchivoCB.append("\n");									
					}
					
					if(tipo.equals("Interfase Detalle") || tipo.equals("Interfase Hash") ){
						contenidoArchivoA.append(	ifNombre.replace(',',' ')+","+ 	fechaNotificacion+","+	acuseNotificacion+","+ noPyme+","+
							pymeNombre.replace(',',' ')+","+	numeroDocto+","+ fechaEmision+","+fechaVencimiento+",");
						if("S".equals(operaFVPyme)) { contenidoArchivoA.append( fechaVencimientoP+",");		}						
						contenidoArchivoA.append(		monedaNombre+factoraje+","+ 	Comunes.formatoDecimal(monto,2,false)+","+	sucursal+","+banco.replace(',',' ')+","+	noCuenta+docto);			
						for (int x=0; x<campos.size(); x++) { 
							String campo = ((String)vctContenidoCampos.get(x)).equals("") ? " " : (String)vctContenidoCampos.get(x);
							contenidoArchivoA.append("," +campo );
						}							
						contenidoArchivoA.append(estaHabilitadoNumeroSIAFF?","+numeroSIAFF:"");												
						if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit){					
							contenidoArchivoA.append("," + beneficiario.replace(',',' ') + "," + por_beneficiario + "," + importe_a_recibir);
						}
						if(banderaTablaDoctos.equals("1")){	
							contenidoArchivoA.append( ","+fechaEntrega+","+tipoCompra+","+clavePresupuestaria+","+periodo);
						}
						contenidoArchivoA.append("\n");
					}
					
					if(tipo.equals("Interfase") || tipo.equals("Interfase-RFC")  || tipo.equals("Interfase ExpHash")){
						
						contenidoArchivo.append( rfc_IF+ ifNombre.replace(',',' ')+","+	fechaNotificacion+","+	acuseNotificacion+","+
							noPyme+"," +	pymeNombre.replace(',',' ')+","+	numeroDocto+","+	fechaEmision+","+	fechaVencimiento+"," );							
						if("S".equals(operaFVPyme)) { 	contenidoArchivo.append( 	fechaVencimientoP+","); 	}
						contenidoArchivo.append( 	monedaNombre+","+		Comunes.formatoDecimal(monto,2,false)+","+	sucursal+","+ banco.replace(',',' ') +","+
						noCuenta+ "," +	strPorcentaje+ "," +	strMontoDescuento + "," +	tasaAceptada + "," +	strCT_REFERENCIA);								
						for (int x=0; x<campos.size(); x++) { 
							String campo = ((String)vctContenidoCampos.get(x)).equals("") ? " " : (String)vctContenidoCampos.get(x);
							contenidoArchivo.append("," +campo );
						}	
						if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit){							
							contenidoArchivo.append("," + beneficiario.replace(',',' ') + "," + por_beneficiario + "," + importe_a_recibir);
						}	
						contenidoArchivo.append(estaHabilitadoNumeroSIAFF?","+numeroSIAFF:"");
						if(banderaTablaDoctos.equals("1")){	
							contenidoArchivo.append(","+fechaEntrega+","+tipoCompra+","+clavePresupuestaria+","+periodo);
						}
						contenidoArchivo.append("\n");
					
					}	// FIN de: if(tipo.equals("Interfase"))
					
					if(tipo.equals("Interfase ExpClabe") ){
					
						contenidoArchivoCBIN.append( rfc_IF+ ifNombre.replace(',',' ')+","+	fechaNotificacion+","+	acuseNotificacion+","+
							noPyme+"," + pymeNombre.replace(',',' ')+","+	numeroDocto+","+ fechaEmision+","+fechaVencimiento+",");
							
						if("S".equals(operaFVPyme)) { 	contenidoArchivoCBIN.append( fechaVencimientoP+",");			}
						
						contenidoArchivoCBIN.append( monedaNombre+","+	Comunes.formatoDecimal(monto,2,false)+","+	sucursal+","+	banco.replace(',',' ') +","+
						noCuentaCLABE+ "," +	strPorcentaje+ "," +	strMontoDescuento + "," +	tasaAceptada + "," +strCT_REFERENCIA);
						
						for (int x=0; x<campos.size(); x++) { 
							String campo = ((String)vctContenidoCampos.get(x)).equals("") ? " " : (String)vctContenidoCampos.get(x);
							contenidoArchivoCBIN.append("," +campo );
						}	
						if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit){							
							contenidoArchivoCBIN.append("," + beneficiario.replace(',',' ') + "," + por_beneficiario + "," + importe_a_recibir);
						}					
						contenidoArchivoCBIN.append( estaHabilitadoNumeroSIAFF?","+numeroSIAFF:"");
						if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit){						
								contenidoArchivoCB.append("," + beneficiario.replace(',',' ') + "," + por_beneficiario + "," + importe_a_recibir);
						}
						contenidoArchivoCBIN.append(","+fechaEntrega+","+tipoCompra+","+clavePresupuestaria+","+periodo);
						contenidoArchivoCBIN.append("\n");
					}			
					i++;	
				} //fin del while
					
				if(contenidoArchivoCB.length()>0){
					contenidoArchivo = contenidoArchivoCB;
				}
				if(contenidoArchivoCBIN.length()>0){
					contenidoArchivo = contenidoArchivoCBIN;
				}
				if(contenidoArchivoA.length()>0){
					contenidoArchivo = contenidoArchivoA;
				}				
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
				
				
				if(tipo.equals("Interfase Hash")  || tipo.equals("Interfase ExpHash") ){
					
					String Ruta=path+nombreArchivo;
					java.io.File f = new java.io.File(Ruta);

					byte[] aByte = new byte[4096];//IHJ

					FileInputStream 	fis	= new FileInputStream(f);
					MessageDigest 		md 	= MessageDigest.getInstance("MD5");//IHJ
					md.reset();//IHJ

					while((i = fis.read(aByte, 0, aByte.length)) != -1)//IHJ
						md.update(aByte, 0, i);//IHJ

					byte[] digest = md.digest();//IHJ

					StringBuffer hexString = new StringBuffer();//IHJ
					for (int j=0;j<digest.length;j++) {//IHJ
						String aux = Integer.toHexString(0xFF & digest[j]);//IHJ
						if(aux.length()==1) {//IHJ
							aux = "0"+aux;//IHJ
						}
						hexString.append(aux);//IHJ
					}//IHJ

					codigoHASH = hexString.toString();
					
					creaArchivo.make(codigoHASH, path, ".txt");
					nombreArchivo = creaArchivo.getNombre();				
				}	
			}
			
			/**
			 * genera archivo PDF con todos los registros 
			 */
			if ("PDF".equals(tipoArchivo)) {	
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				 
				pdfDoc.addText(" ","formasB",ComunesPDF.CENTER);
				pdfDoc.addText(mensaje,"formasB",ComunesPDF.JUSTIFIED);
				pdfDoc.addText(" ","formasB",ComunesPDF.CENTER);
				int total_registros =0;
				while (rs.next())	{
					total_registros++;
					vctContenidoCampos.clear();
					ifNombre          = (rs.getString( 1) == null) ? "" :    rs.getString( 1).trim();
					fechaNotificacion = (rs.getString( 2) == null) ? "" :    rs.getString( 2).trim();
					acuseNotificacion = (rs.getString( 3) == null) ? "" :    rs.getString( 3).trim();
					pymeNombre        = (rs.getString( 4) == null) ? "" :    rs.getString( 4).trim();
					numeroDocto       = (rs.getString( 5) == null) ? "" :    rs.getString( 5).trim();
					fechaEmision      = (rs.getString( 6) == null) ? "" :    rs.getString( 6).trim();
					fechaVencimiento  = (rs.getString( 7) == null) ? "" :    rs.getString( 7).trim();
					monedaNombre      = (rs.getString( 8) == null) ? "" :    rs.getString( 8).trim();
					monto             = (rs.getString( 9) == null) ? "0.0" : rs.getString( 9).trim();
					strPorcentaje     = (rs.getString(10) == null) ? "0.0" : rs.getString(10).trim();
					strMontoDescuento = (rs.getString(11) == null) ? "0.0" : rs.getString(11).trim();
					sucursal          = (rs.getString(12) == null) ? "" :    rs.getString(12).trim();
					banco             = (rs.getString(13) == null) ? "" :    rs.getString(13).trim();
					noCuenta          = (rs.getString(14) == null) ? "" :    rs.getString(14).trim();
					noPyme            = (rs.getString(15) == null) ? "" :    rs.getString(15).trim();
					ic_documento      = (rs.getString(17) == null) ? "" :    rs.getString(17).trim();
					fechaVencimientoP = (rs.getString("DF_FECHA_VEN_PYME")==null)?"":rs.getString("DF_FECHA_VEN_PYME").trim();
					beneficiario      = (rs.getString("beneficiario")      == null) ? "" : rs.getString("beneficiario").trim();
					por_beneficiario  = (rs.getString("por_beneficiario")  == null) ? "" : rs.getString("por_beneficiario").trim();
					importe_a_recibir = (rs.getString("importe_a_recibir") == null) ? "" : rs.getString("importe_a_recibir").trim();
					tasaAceptada = (rs.getString("in_tasa_aceptada") == null) ? "" : rs.getString("in_tasa_aceptada");
					strCT_REFERENCIA = (rs.getString("CT_REFERENCIA") == null) ? "" : rs.getString("CT_REFERENCIA");
					rs_docto_asociado = (rs.getString("doctoasociado") == null) ? "" :    rs.getString("doctoasociado").trim();
					rs_tipo_factoraje = (rs.getString("cs_dscto_especial") == null) ? "" :    rs.getString("cs_dscto_especial").trim();
					rs_tipo_factoraje_desc = (rs.getString("TIPO_FACTORAJE") == null) ? "" :    rs.getString("TIPO_FACTORAJE"); 
					ic_moneda =  (rs.getString("IC_MONEDA") == null) ? "" :    rs.getString("IC_MONEDA"); 
					numeroSIAFF  = (rs.getString("DIG_IDENTIFICADOR")==null)?"":rs.getString("DIG_IDENTIFICADOR");
	
					int cam=1;
					for (int x=0; x<campos.size(); x++) { 
						String nombre ="CAMPO"+cam;					
						strCampo = rs.getString(nombre);									
						strCampo = (strCampo == null) ? "" : strCampo;					
						vctContenidoCampos.add(strCampo);				
						cam++;						
					}  				
						
					int cols = 13;
					if("S".equals(operaNC))
						cols++;
					if("S".equals(operaFVPyme))
						cols++;
					float widthsDocs[] = new float[cols];
					for(int d=0;d<cols;d++) {
						if(d>0)
							widthsDocs[d] = 1;
						else if(d==0)
							widthsDocs[d] = (float)0.5;
						else
							widthsDocs[d] = 1;   
					}//for

					//si existe la parametrizacion
					if( banderaTablaDoctos.equals("1") ){
						fechaEntrega 				= (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
						tipoCompra 					= (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
						clavePresupuestaria = (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
						periodo 						= (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");
					} 
					if(i==0) {
						pdfDoc.setLTable(cols,100,widthsDocs);
						pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Intermediario Finaciero","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha de Notificaci�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Acuse de Notificaci�n" ,"celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("N�mero de Proveedor","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Nombre del proveedor ","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha de Emisi�n ","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha de Vencimiento ","celda01",ComunesPDF.CENTER);		
						if("S".equals(operaFVPyme)) {
							pdfDoc.setLCell("Fecha de Vencimiento del Proveedor","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
						if("S".equals(operaNC)){
							pdfDoc.setLCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);	
						}
						pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);		
						pdfDoc.setLCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);		
						pdfDoc.setLCell("Monto a Descontar","celda01",ComunesPDF.CENTER);	
						
						pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Clave del Banco","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Banco","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("N�mero de Cuenta","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Referencia","celda01",ComunesPDF.CENTER);
						
						if("S".equals(operaNC)) {
							pdfDoc.setLCell("Documento Aplicado","celda01",ComunesPDF.CENTER);
						}

						for (int x=0; x<campos.size(); x++) {
							List lovRegistro = (List) campos.get(x);
							strCampos = (String) lovRegistro.get(1);
							pdfDoc.setLCell(strCampos,"celda01",ComunesPDF.CENTER);
						}	
						colspan =  cols - 5 - campos.size();
						if("S".equals(operaNC)) {
							cols--;
							colspan--;							
						}
											
						if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit) {
							colspan -= 3;
							pdfDoc.setLCell("Beneficiario","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("% Beneficiario","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Importe a recibir beneficiario","celda01",ComunesPDF.CENTER);
						}//if(bOperaFactorajeDistribuido)
	
						if(estaHabilitadoNumeroSIAFF && colspan>0){ // El acuse de registro individual se agregara en la misma linea
							pdfDoc.setLCell("D�gito Identificador","celda01",ComunesPDF.CENTER);						
							colspan--;
							agregarNumeroSIAFFEnFilaNueva = false;
						}
						if(colspan>0)
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER,colspan);
	
						// Mostrar encabezado del Numero SIAFF
						if(estaHabilitadoNumeroSIAFF && agregarNumeroSIAFFEnFilaNueva ){
							pdfDoc.setLCell("C","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("D�gito Identificador","celda01",ComunesPDF.CENTER,2);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
						}
						String indice_columna = "C";
						if(estaHabilitadoNumeroSIAFF && agregarNumeroSIAFFEnFilaNueva){
							//esto significa que ya hay columna C entonces la nueva se va a llamar D
							indice_columna = "D";
						}
						//vars para parametrizacion docto
						if( banderaTablaDoctos.equals("1") ){
							pdfDoc.setLCell(indice_columna,"celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Fecha de Recepci�n de Bienes y Servicios","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Tipo de Compra (procedimiento)","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Clasificador por Objeto del Gasto","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Plazo M�ximo","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);

							if("S".equals(operaFVPyme)) {
								pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							}
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							if("S".equals(operaNC))
								pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
						}
					pdfDoc.setLHeaders();
				}//if(i==0)
				
				
				pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(ifNombre,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaNotificacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(acuseNotificacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(noPyme,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(pymeNombre,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(numeroDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaEmision,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
	 			if("S".equals(operaFVPyme)) {
					pdfDoc.setLCell(fechaVencimientoP,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell(monedaNombre,"formas",ComunesPDF.CENTER);
	 			if("S".equals(operaNC)){
					pdfDoc.setLCell(rs_tipo_factoraje_desc,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell("$ "+Comunes.formatoMoneda2(monto, false),"formas",ComunesPDF.RIGHT);
				pdfDoc.setLCell(Comunes.formatoDecimal(strPorcentaje,0, false)+" %","formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+Comunes.formatoMoneda2(strMontoDescuento, false),"formas",ComunesPDF.RIGHT);

				pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(sucursal,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(banco,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(noCuenta,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell((strCT_REFERENCIA.equals("")) ? " " : strCT_REFERENCIA,"formas",ComunesPDF.CENTER);
	 			if("S".equals(operaNC)) {
					pdfDoc.setLCell(rs_docto_asociado,"formas",ComunesPDF.CENTER);
				}
				for (int x=0; x<campos.size(); x++) { 				 
					pdfDoc.setLCell((((String)vctContenidoCampos.get(x)).equals("") ? " " : (String)vctContenidoCampos.get(x)),"formas",ComunesPDF.CENTER);
				}
				i++;
				if(bOperaFactorajeDistribuido.equals("S") || bOperaFactorajeVencInfonavit) {
					pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(por_beneficiario,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(importe_a_recibir,"formas",ComunesPDF.CENTER);
				}//if(bOperaFactorajeDistribuido)

				if(estaHabilitadoNumeroSIAFF && !agregarNumeroSIAFFEnFilaNueva){
					pdfDoc.setLCell(numeroSIAFF,"formas",ComunesPDF.CENTER);
				}

				if(colspan>0)
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER,colspan);

				if(estaHabilitadoNumeroSIAFF && agregarNumeroSIAFFEnFilaNueva ){
						pdfDoc.setLCell("C","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(numeroSIAFF,"formas",ComunesPDF.CENTER,2);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					 //pdfDoc.setLCell("","formas",ComunesPDF.CENTER,11);
				}
				String indice_columna = "C";

				if(estaHabilitadoNumeroSIAFF && agregarNumeroSIAFFEnFilaNueva){
					//esto significa que ya hay columna C entonces la nueva se va a llamar D
					indice_columna = "D";
				}
				//vars para parametrizacion docto
				if( banderaTablaDoctos.equals("1") ){
					pdfDoc.setLCell(indice_columna,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaEntrega,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCompra,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(clavePresupuestaria,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(periodo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
		 			if("S".equals(operaFVPyme)) {
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					}
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
		 			if("S".equals(operaNC))
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
				}
		
			if(ic_moneda.equals("1")) {
				monedaNacional ++;
				montoN +=  Double.parseDouble((String)monto);
				montoDescontarN +=  Double.parseDouble((String)strMontoDescuento);
				
			}else {
				 monedaDolar ++;
				montoD +=  Double.parseDouble((String)monto);
				montoDescontarD +=  Double.parseDouble((String)strMontoDescuento);
			}
			} //fin del while

			int span = 3;
			if("S".equals(operaNC))
				span++;
			if("S".equals(operaFVPyme))
				span++;
			pdfDoc.setLCell(" Total MONEDA NACIONAL:","celda01",ComunesPDF.CENTER,5);
			pdfDoc.setLCell(String.valueOf(monedaNacional),"formas",ComunesPDF.CENTER,2);
			pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER,span);
			pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoN,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setLCell(" ","celda01");
			pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoDescontarN,2),"formas",ComunesPDF.RIGHT);
			
			pdfDoc.setLCell(" Total DOLAR AMERICANO:","celda01",ComunesPDF.CENTER,5);
			pdfDoc.setLCell( String.valueOf(monedaDolar),"formas",ComunesPDF.CENTER,2);
			pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER,span);
			pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoD,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setLCell(" ","celda01");
			pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoDescontarD,2),"formas",ComunesPDF.RIGHT);		
			
			pdfDoc.addLTable();
			pdfDoc.endDocument();
		}			
	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}


	
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getDf_fecha_notMin() {
		return df_fecha_notMin;
	}

	public void setDf_fecha_notMin(String df_fecha_notMin) {
		this.df_fecha_notMin = df_fecha_notMin;
	}

	public String getDf_fecha_notMax() {
		return df_fecha_notMax;
	}

	public void setDf_fecha_notMax(String df_fecha_notMax) {
		this.df_fecha_notMax = df_fecha_notMax;
	}

	public String getDf_fecha_vencMin() {
		return df_fecha_vencMin;
	}

	public void setDf_fecha_vencMin(String df_fecha_vencMin) {
		this.df_fecha_vencMin = df_fecha_vencMin;
	}

	public String getDf_fecha_vencMax() {
		return df_fecha_vencMax;
	}

	public void setDf_fecha_vencMax(String df_fecha_vencMax) {
		this.df_fecha_vencMax = df_fecha_vencMax;
	}  

	public String getCc_acuse3() {
		return cc_acuse3;
	}

	public void setCc_acuse3(String cc_acuse3) {
		this.cc_acuse3 = cc_acuse3;
	}

	public String getNumero_siaff() {
		return numero_siaff;
	}

	public void setNumero_siaff(String numero_siaff) {
		this.numero_siaff = numero_siaff;
	}

	public String getClavePyme() {
		return clavePyme;
	}

	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}

	public String getClaveEpo() {
		return claveEpo;
	}

	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	public String getOperaNC() {
		return operaNC;
	}

	public void setOperaNC(String operaNC) {
		this.operaNC = operaNC;
	}

	public String getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getStrCampos() {
		return strCampos;
	}

	public void setStrCampos(String strCampos) {
		this.strCampos = strCampos;
	}

	public String getOperaFVPyme() {
		return operaFVPyme;
	}

	public void setOperaFVPyme(String operaFVPyme) {
		this.operaFVPyme = operaFVPyme;
	}

	public String getBanderaTablaDoctos() {
		return banderaTablaDoctos;
	}

	public void setBanderaTablaDoctos(String banderaTablaDoctos) {
		this.banderaTablaDoctos = banderaTablaDoctos;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}