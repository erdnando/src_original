package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import org.apache.commons.logging.Log;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

public class OperCredEleNafinDE implements IQueryGenerator,IQueryGeneratorRegExtJS {
	
	private static final Log log = ServiceLocator.getInstance().getLog(OperCredEleNafinDE.class);

  public OperCredEleNafinDE()  {  }
  private List 		conditions;
  	StringBuffer 		qrySentencia;
  private String	ic_if="",fechaOper="",noSirac="",acuse="",noPrestamo="",fechaOperFin="",
  fechaEnvioIf="",fechaEnvioIfFin="",estatus="";
  
public String getAggregateCalculationQuery() {
 StringBuffer qrySentencia = new StringBuffer();    

    try{
		qrySentencia.append(
			" SELECT /*+use_nl(sp p1 p2 cd tc m p e ti tuf i)*/ "+
			" 		 count(1) as OperCredEleNafinDE "+
			"   FROM com_solic_portal sp,"   +
			"        comcat_pyme p,"   +
			"        comcat_tasa ti,"   +
			"        comcat_tasa tuf,"   +
			"        comcat_if i,"   +
			"        comcat_emisor e,"   +
			"        comcat_moneda m,"   +
			"        comcat_tipo_credito tc,"   +
			"        comcat_clase_docto cd,"   +
			"        comcat_periodicidad p1,"   +
			"        comcat_periodicidad p2,"   +
			"        comcat_tabla_amort ta"   +
			"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
			"    AND ti.ic_tasa = sp.ic_tasaif"   +
			"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
			"    AND i.ic_if = sp.ic_if"   +
			"    AND sp.ic_emisor = e.ic_emisor (+)"   +
			"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
			"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
			"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
			"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
			"    AND sp.ic_moneda = m.ic_moneda (+)"   +
			"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)" );
								
         qrySentencia.append((acuse.equals("")?"":" and SP.cc_acuse ='"+acuse+"'"));
			qrySentencia.append((ic_if.equals("")?"":" and SP.ic_if = "+ic_if));
			qrySentencia.append((fechaOper.equals("")?"":" and SP.df_operacion >=  TRUNC(TO_DATE('"+fechaOper+"','DD/MM/YYYY')) "));
			qrySentencia.append((fechaOperFin.equals("")?"":" and SP.df_operacion <  TRUNC(TO_DATE('"+fechaOperFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIf.equals("")?"":" and SP.df_carga >=  TRUNC(TO_DATE('"+fechaEnvioIf+"','DD/MM/YYYY')) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIfFin.equals("")?"":" and SP.df_carga <  TRUNC(TO_DATE('"+fechaEnvioIfFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((noSirac.equals("")?"":" and SP.ig_clave_sirac = "+noSirac));
			qrySentencia.append((noPrestamo.equals("")?"":" and SP.ig_numero_prestamo = "+noPrestamo));
			qrySentencia.append((estatus.equals("")?"":" AND sp.ic_estatus_solic = "+estatus));//FODEA 011-2010 fvr

    }catch(Exception e){
      System.out.println("OperCredEleNafinDE::getDocumentQueryFileException "+e);
    }
    return qrySentencia.toString();
  }
	public String getAggregateCalculationQuery(HttpServletRequest request)  {
		
    StringBuffer qrySentencia = new StringBuffer();  
	 
	String	ic_if 		= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if"),
			fechaOper 	= (request.getParameter("fechaOper")==null)?"":request.getParameter("fechaOper").trim(),
			noSirac 	= (request.getParameter("noSirac")==null)?"":request.getParameter("noSirac").trim(),
			acuse 		= (request.getParameter("acuse")==null)?"":request.getParameter("acuse").trim(),
			noPrestamo	= (request.getParameter("noPrestamo")==null)?"":request.getParameter("noPrestamo").trim();
			
	//FODEA 011-2010 FVR			
	String fechaOperFin 	= (request.getParameter("fechaOperFin")==null)?"":request.getParameter("fechaOperFin").trim();
	String fechaEnvioIf 	= (request.getParameter("fechaEnvioIf")==null)?"":request.getParameter("fechaEnvioIf").trim();
	String fechaEnvioIfFin 	= (request.getParameter("fechaEnvioIfFin")==null)?"":request.getParameter("fechaEnvioIfFin").trim();
	String estatus 	= (request.getParameter("estatus")==null)?"":request.getParameter("estatus").trim();

    try{
		qrySentencia.append(
			" SELECT /*+use_nl(sp p1 p2 cd tc m p e ti tuf i)*/ "+
			" 		 count(1) as OperCredEleNafinDE "+
			"   FROM com_solic_portal sp,"   +
			"        comcat_pyme p,"   +
			"        comcat_tasa ti,"   +
			"        comcat_tasa tuf,"   +
			"        comcat_if i,"   +
			"        comcat_emisor e,"   +
			"        comcat_moneda m,"   +
			"        comcat_tipo_credito tc,"   +
			"        comcat_clase_docto cd,"   +
			"        comcat_periodicidad p1,"   +
			"        comcat_periodicidad p2,"   +
			"        comcat_tabla_amort ta"   +
			"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
			"    AND ti.ic_tasa = sp.ic_tasaif"   +
			"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
			"    AND i.ic_if = sp.ic_if"   +
			"    AND sp.ic_emisor = e.ic_emisor (+)"   +
			"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
			"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
			"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
			"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
			"    AND sp.ic_moneda = m.ic_moneda (+)"   +
			"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)" );
			//"    AND sp.ic_estatus_solic = 3 ");
						
         qrySentencia.append((acuse.equals("")?"":" and SP.cc_acuse ='"+acuse+"'"));
			qrySentencia.append((ic_if.equals("")?"":" and SP.ic_if = "+ic_if));
			qrySentencia.append((fechaOper.equals("")?"":" and SP.df_operacion >=  TRUNC(TO_DATE('"+fechaOper+"','DD/MM/YYYY')) "));
			qrySentencia.append((fechaOperFin.equals("")?"":" and SP.df_operacion <  TRUNC(TO_DATE('"+fechaOperFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIf.equals("")?"":" and SP.df_carga >=  TRUNC(TO_DATE('"+fechaEnvioIf+"','DD/MM/YYYY')) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIfFin.equals("")?"":" and SP.df_carga <  TRUNC(TO_DATE('"+fechaEnvioIfFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((noSirac.equals("")?"":" and SP.ig_clave_sirac = "+noSirac));
			qrySentencia.append((noPrestamo.equals("")?"":" and SP.ig_numero_prestamo = "+noPrestamo));
			qrySentencia.append((estatus.equals("")?"":" AND sp.ic_estatus_solic = "+estatus));//FODEA 011-2010 fvr

    }catch(Exception e){
      System.out.println("OperCredEleNafinDE::getDocumentQueryFileException "+e);
    }
    return qrySentencia.toString();
  }
	public String getDocumentSummaryQueryForIds(List pageIds){
	
		StringBuffer qrySentencia = new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(
			" SELECT   /*+use_nl(sp p1 p2 cd tc m p e tituf i)*/p.cg_razon_social"   +
			"              AS nombrepyme, sp.ig_clave_sirac AS clavesirac,"   +
			"        sp.ig_numero_docto AS numdocto,"   +
			"        TO_CHAR (sp.df_etc, 'DD/MM/YYYY') AS fechaetc,"   +
			"        TO_CHAR (sp.df_v_descuento, 'DD/MM/YYYY') AS fechadscto,"   +
			"        m.cd_nombre AS nombremoneda, tc.cd_descripcion AS desctipocred,"   +
			"        sp.fn_importe_docto AS importedocto,"   +
			"        sp.fn_importe_dscto AS importedscto,"   +
			"        sp.ic_solic_portal AS numfolio,"   +
			"        TO_CHAR (sp.df_operacion, 'DD/MM/YYYY') AS fechaoperacion,"   +
			"        sp.ig_numero_prestamo AS numprestamo, sp.cc_acuse AS numacuse,"   +
			"        i.cg_razon_social AS nombreif"  +
			" 			,p1.cd_descripcion AS periodicidad_pago," +
			"        TO_CHAR (sp.df_carga, 'DD/MM/YYYY') AS fechaenvioif,"   +
			"        sp.ic_error_proceso AS errorproceso,"   +
			"        sp.cc_codigo_aplicacion AS codigoaplicacion,"   +
			"        ep.cg_descripcion AS errordescripcion"   +
			"   FROM com_solic_portal sp,"   +
			"        comcat_pyme p,"   +
			"        comcat_tasa ti,"   +
			"        comcat_tasa tuf,"   +
			"        comcat_if i,"   +
			"        comcat_emisor e,"   +
			"        comcat_moneda m,"   +
			"        comcat_tipo_credito tc,"   +
			"        comcat_clase_docto cd,"   +
			"        comcat_periodicidad p1,"   +
			"        comcat_periodicidad p2,"   +
			"        comcat_tabla_amort ta,"   +
			"        comcat_error_procesos ep"   +
			"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
			"    AND ti.ic_tasa = sp.ic_tasaif"   +
			"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
			"    AND i.ic_if = sp.ic_if"   +
			"    AND sp.ic_emisor = e.ic_emisor (+)"   +
			"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
			"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
			"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
			"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
			"    AND sp.ic_moneda = m.ic_moneda (+)"   +
			"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)"+
			"    AND sp.ic_error_proceso = ep.ic_error_proceso (+)"+//FODEA 011-2010 FVR
			"    AND sp.cc_codigo_aplicacion = ep.cc_codigo_aplicacion (+)"+//FODEA 011-2010 FVR
			//"    AND sp.ic_estatus_solic = 3 "+  FODEA 011-2010 FVR
			"    AND sp.IC_SOLIC_PORTAL in(");
	
		
		 for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0) {
				qrySentencia.append(" , ");
			}
			qrySentencia.append(" ? ");
			conditions.add(new Long(lItem.get(0).toString()));
     
      
		}
 		qrySentencia.append(" ) ");
		
		System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids)  {
  		int i=0;
		StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append(
			" SELECT   /*+use_nl(sp p1 p2 cd tc m p e tituf i)*/p.cg_razon_social"   +
			"              AS nombrepyme, sp.ig_clave_sirac AS clavesirac,"   +
			"        sp.ig_numero_docto AS numdocto,"   +
			"        TO_CHAR (sp.df_etc, 'DD/MM/YYYY') AS fechaetc,"   +
			"        TO_CHAR (sp.df_v_descuento, 'DD/MM/YYYY') AS fechadscto,"   +
			"        m.cd_nombre AS nombremoneda, tc.cd_descripcion AS desctipocred,"   +
			"        sp.fn_importe_docto AS importedocto,"   +
			"        sp.fn_importe_dscto AS importedscto,"   +
			"        sp.ic_solic_portal AS numfolio,"   +
			"        TO_CHAR (sp.df_operacion, 'DD/MM/YYYY') AS fechaoperacion,"   +
			"        sp.ig_numero_prestamo AS numprestamo, sp.cc_acuse AS numacuse,"   +
			"        i.cg_razon_social AS nombreif"  +
			" 			,p1.cd_descripcion AS periodicidad_pago," +
			"        TO_CHAR (sp.df_carga, 'DD/MM/YYYY') AS fechaenvioif,"   +
			"        sp.ic_error_proceso AS errorproceso,"   +
			"        sp.cc_codigo_aplicacion AS codigoaplicacion,"   +
			"        ep.cg_descripcion AS errordescripcion"   +
			"   FROM com_solic_portal sp,"   +
			"        comcat_pyme p,"   +
			"        comcat_tasa ti,"   +
			"        comcat_tasa tuf,"   +
			"        comcat_if i,"   +
			"        comcat_emisor e,"   +
			"        comcat_moneda m,"   +
			"        comcat_tipo_credito tc,"   +
			"        comcat_clase_docto cd,"   +
			"        comcat_periodicidad p1,"   +
			"        comcat_periodicidad p2,"   +
			"        comcat_tabla_amort ta,"   +
			"        comcat_error_procesos ep"   +
			"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
			"    AND ti.ic_tasa = sp.ic_tasaif"   +
			"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
			"    AND i.ic_if = sp.ic_if"   +
			"    AND sp.ic_emisor = e.ic_emisor (+)"   +
			"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
			"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
			"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
			"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
			"    AND sp.ic_moneda = m.ic_moneda (+)"   +
			"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)"+
			"    AND sp.ic_error_proceso = ep.ic_error_proceso (+)"+//FODEA 011-2010 FVR
			"    AND sp.cc_codigo_aplicacion = ep.cc_codigo_aplicacion (+)"+//FODEA 011-2010 FVR
			//"    AND sp.ic_estatus_solic = 3 "+  FODEA 011-2010 FVR
			"    AND sp.IC_SOLIC_PORTAL in(");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
          		qrySentencia.append(",");
        	}
			qrySentencia.append("'"+it.next().toString()+"'");
		}
		qrySentencia.append(")");
		System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
  }
	
	public String getDocumentQuery(){
	StringBuffer qrySentencia = new StringBuffer();
	conditions 		= new ArrayList();
    	try{
			qrySentencia.append(
				" SELECT /*+use_nl(sp p1 p2 cd tc m p e ti tuf i)*/ "+
				"        sp.IC_SOLIC_PORTAL,'OperCredEleNafinDE::getDocumentQuery'"   +
				"   FROM com_solic_portal sp,"   +
				"        comcat_pyme p,"   +
				"        comcat_tasa ti,"   +
				"        comcat_tasa tuf,"   +
				"        comcat_if i,"   +
				"        comcat_emisor e,"   +
				"        comcat_moneda m,"   +
				"        comcat_tipo_credito tc,"   +
				"        comcat_clase_docto cd,"   +
				"        comcat_periodicidad p1,"   +
				"        comcat_periodicidad p2,"   +
				"        comcat_tabla_amort ta"   +
				"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
				"    AND ti.ic_tasa = sp.ic_tasaif"   +
				"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
				"    AND i.ic_if = sp.ic_if"   +
				"    AND sp.ic_emisor = e.ic_emisor (+)"   +
				"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
				"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
				"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
				"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
				"    AND sp.ic_moneda = m.ic_moneda (+)"   +
				"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)" );
				//"    AND sp.ic_estatus_solic = 3");
            qrySentencia.append((acuse.equals("")?"":" and SP.cc_acuse ='"+acuse+"'"));
			qrySentencia.append((ic_if.equals("")?"":" and SP.ic_if = "+ic_if));
			qrySentencia.append((fechaOper.equals("")?"":" and SP.df_operacion >=  TRUNC(TO_DATE('"+fechaOper+"','DD/MM/YYYY')) "));
			qrySentencia.append((fechaOperFin.equals("")?"":" and SP.df_operacion <  TRUNC(TO_DATE('"+fechaOperFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIf.equals("")?"":" and SP.df_carga >=  TRUNC(TO_DATE('"+fechaEnvioIf+"','DD/MM/YYYY')) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIfFin.equals("")?"":" and SP.df_carga <  TRUNC(TO_DATE('"+fechaEnvioIfFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((noSirac.equals("")?"":" and SP.ig_clave_sirac = "+noSirac));
			qrySentencia.append((noPrestamo.equals("")?"":" and SP.ig_numero_prestamo = "+noPrestamo));
			qrySentencia.append((estatus.equals("")?"":" AND sp.ic_estatus_solic = "+estatus));//FODEA 011-2010 fvr

		    System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("OperCredEleNafinDE::getDocumentQueryException "+e);
    	}
		return qrySentencia.toString();
	}
	public String getDocumentQuery(HttpServletRequest request)	{
    	StringBuffer qrySentencia = new StringBuffer();
		String	ic_if 		= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if"),
			fechaOper 	= (request.getParameter("fechaOper")==null)?"":request.getParameter("fechaOper").trim(),
			noSirac 	= (request.getParameter("noSirac")==null)?"":request.getParameter("noSirac").trim(),
			acuse 		= (request.getParameter("acuse")==null)?"":request.getParameter("acuse").trim(),
			noPrestamo	= (request.getParameter("noPrestamo")==null)?"":request.getParameter("noPrestamo").trim();
			
		String fechaOperFin 	= (request.getParameter("fechaOperFin")==null)?"":request.getParameter("fechaOperFin").trim();
		String fechaEnvioIf 	= (request.getParameter("fechaEnvioIf")==null)?"":request.getParameter("fechaEnvioIf").trim();
		String fechaEnvioIfFin 	= (request.getParameter("fechaEnvioIfFin")==null)?"":request.getParameter("fechaEnvioIfFin").trim();
		String estatus 	= (request.getParameter("estatus")==null)?"":request.getParameter("estatus").trim();

    	try{
			qrySentencia.append(
				" SELECT /*+use_nl(sp p1 p2 cd tc m p e ti tuf i)*/ "+
				"        sp.IC_SOLIC_PORTAL,'OperCredEleNafinDE::getDocumentQuery'"   +
				"   FROM com_solic_portal sp,"   +
				"        comcat_pyme p,"   +
				"        comcat_tasa ti,"   +
				"        comcat_tasa tuf,"   +
				"        comcat_if i,"   +
				"        comcat_emisor e,"   +
				"        comcat_moneda m,"   +
				"        comcat_tipo_credito tc,"   +
				"        comcat_clase_docto cd,"   +
				"        comcat_periodicidad p1,"   +
				"        comcat_periodicidad p2,"   +
				"        comcat_tabla_amort ta"   +
				"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
				"    AND ti.ic_tasa = sp.ic_tasaif"   +
				"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
				"    AND i.ic_if = sp.ic_if"   +
				"    AND sp.ic_emisor = e.ic_emisor (+)"   +
				"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
				"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
				"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
				"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
				"    AND sp.ic_moneda = m.ic_moneda (+)"   +
				"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)" );
				//"    AND sp.ic_estatus_solic = 3");
            qrySentencia.append((acuse.equals("")?"":" and SP.cc_acuse ='"+acuse+"'"));
			qrySentencia.append((ic_if.equals("")?"":" and SP.ic_if = "+ic_if));
			qrySentencia.append((fechaOper.equals("")?"":" and SP.df_operacion >=  TRUNC(TO_DATE('"+fechaOper+"','DD/MM/YYYY')) "));
			qrySentencia.append((fechaOperFin.equals("")?"":" and SP.df_operacion <  TRUNC(TO_DATE('"+fechaOperFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIf.equals("")?"":" and SP.df_carga >=  TRUNC(TO_DATE('"+fechaEnvioIf+"','DD/MM/YYYY')) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIfFin.equals("")?"":" and SP.df_carga <  TRUNC(TO_DATE('"+fechaEnvioIfFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((noSirac.equals("")?"":" and SP.ig_clave_sirac = "+noSirac));
			qrySentencia.append((noPrestamo.equals("")?"":" and SP.ig_numero_prestamo = "+noPrestamo));
			qrySentencia.append((estatus.equals("")?"":" AND sp.ic_estatus_solic = "+estatus));//FODEA 011-2010 fvr

		    System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("OperCredEleNafinDE::getDocumentQueryException "+e);
    	}
		return qrySentencia.toString();
	}
	public String getDocumentQueryFile() {
	 qrySentencia = new StringBuffer();
		conditions 		= new ArrayList();			
	    try {
			qrySentencia.append(
				" SELECT   /*+use_nl(sp p1 p2 cd tc m p e tituf i)*/p.cg_razon_social"   +
				"              AS nombrepyme, sp.ig_clave_sirac AS clavesirac,"   +
				"        sp.ig_numero_docto AS numdocto,"   +
				"        TO_CHAR (sp.df_etc, 'DD/MM/YYYY') AS fechaetc,"   +
				"        TO_CHAR (sp.df_v_descuento, 'DD/MM/YYYY') AS fechadscto,"   +
				"        m.cd_nombre AS nombremoneda, tc.cd_descripcion AS desctipocred,"   +
				"        sp.fn_importe_docto AS importedocto,"   +
				"        sp.fn_importe_dscto AS importedscto,"   +
				"        sp.ic_solic_portal AS numfolio,"   +
				"        TO_CHAR (sp.df_operacion, 'DD/MM/YYYY') AS fechaoperacion,"   +
				"        sp.ig_numero_prestamo AS numprestamo, sp.cc_acuse AS numacuse,"   +
				"        i.cg_razon_social AS nombreif"  +
				" 			,p1.cd_descripcion AS periodicidad_pago," +
				"        TO_CHAR (sp.df_carga, 'DD/MM/YYYY') AS fecha_envio_if,"   +
				"        sp.ic_error_proceso AS error_proceso,"   +
				"        sp.cc_codigo_aplicacion AS codigoaplicacion,"   +
				"        ep.cg_descripcion AS error_descripcion"   +//FODEA 011-2010 FVR
				"   FROM com_solic_portal sp,"   +
				"        comcat_pyme p,"   +
				"        comcat_tasa ti,"   +
				"        comcat_tasa tuf,"   +
				"        comcat_if i,"   +
				"        comcat_emisor e,"   +
				"        comcat_moneda m,"   +
				"        comcat_tipo_credito tc,"   +
				"        comcat_clase_docto cd,"   +
				"        comcat_periodicidad p1,"   +
				"        comcat_periodicidad p2,"   +
				"        comcat_tabla_amort ta,"   +
				"        comcat_error_procesos ep"   +
				"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
				"    AND ti.ic_tasa = sp.ic_tasaif"   +
				"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
				"    AND i.ic_if = sp.ic_if"   +
				"    AND sp.ic_emisor = e.ic_emisor (+)"   +
				"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
				"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
				"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
				"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
				"    AND sp.ic_moneda = m.ic_moneda (+)"   +
				"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)" +
				"    AND sp.ic_error_proceso = ep.ic_error_proceso (+)"+//FODEA 011-2010 FVR
				"    AND sp.cc_codigo_aplicacion = ep.cc_codigo_aplicacion (+)");//FODEA 011-2010 FVR
				//"    AND sp.ic_estatus_solic = 3");
            qrySentencia.append((acuse.equals("")?"":" and SP.cc_acuse ='"+acuse+"'"));
			qrySentencia.append((ic_if.equals("")?"":" and SP.ic_if = "+ic_if));
			qrySentencia.append((fechaOper.equals("")?"":" and SP.df_operacion >=  TRUNC(TO_DATE('"+fechaOper+"','DD/MM/YYYY')) "));
			qrySentencia.append((fechaOperFin.equals("")?"":" and SP.df_operacion <  TRUNC(TO_DATE('"+fechaOperFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIf.equals("")?"":" and SP.df_carga >=  TRUNC(TO_DATE('"+fechaEnvioIf+"','DD/MM/YYYY')) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIfFin.equals("")?"":" and SP.df_carga <  TRUNC(TO_DATE('"+fechaEnvioIfFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((noSirac.equals("")?"":" and SP.ig_clave_sirac = "+noSirac));
			qrySentencia.append((noPrestamo.equals("")?"":" and SP.ig_numero_prestamo = "+noPrestamo));
			qrySentencia.append((estatus.equals("")?"":" AND sp.ic_estatus_solic = "+estatus));//FODEA 011-2010 fvr

	    }catch(Exception e){
			System.out.println("OperCredEleNafinDE::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}
	public String getDocumentQueryFile(HttpServletRequest request) {
    	StringBuffer qrySentencia = new StringBuffer();
		String	ic_if 		= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if"),
			fechaOper 	= (request.getParameter("fechaOper")==null)?"":request.getParameter("fechaOper").trim(),
			noSirac 	= (request.getParameter("noSirac")==null)?"":request.getParameter("noSirac").trim(),
			acuse 		= (request.getParameter("acuse")==null)?"":request.getParameter("acuse").trim(),
			noPrestamo	= (request.getParameter("noPrestamo")==null)?"":request.getParameter("noPrestamo").trim();
			
		//FODEA 011-2010 FVR -INI
		String fechaOperFin 	= (request.getParameter("fechaOperFin")==null)?"":request.getParameter("fechaOperFin").trim();
		String fechaEnvioIf 	= (request.getParameter("fechaEnvioIf")==null)?"":request.getParameter("fechaEnvioIf").trim();
		String fechaEnvioIfFin 	= (request.getParameter("fechaEnvioIfFin")==null)?"":request.getParameter("fechaEnvioIfFin").trim();
		String estatus 	= (request.getParameter("estatus")==null)?"":request.getParameter("estatus").trim();
		//FODEA 011-2010 FVR -FIN
			
	    try {
			qrySentencia.append(
				" SELECT   /*+use_nl(sp p1 p2 cd tc m p e tituf i)*/p.cg_razon_social"   +
				"              AS nombrepyme, sp.ig_clave_sirac AS clavesirac,"   +
				"        sp.ig_numero_docto AS numdocto,"   +
				"        TO_CHAR (sp.df_etc, 'DD/MM/YYYY') AS fechaetc,"   +
				"        TO_CHAR (sp.df_v_descuento, 'DD/MM/YYYY') AS fechadscto,"   +
				"        m.cd_nombre AS nombremoneda, tc.cd_descripcion AS desctipocred,"   +
				"        sp.fn_importe_docto AS importedocto,"   +
				"        sp.fn_importe_dscto AS importedscto,"   +
				"        sp.ic_solic_portal AS numfolio,"   +
				"        TO_CHAR (sp.df_operacion, 'DD/MM/YYYY') AS fechaoperacion,"   +
				"        sp.ig_numero_prestamo AS numprestamo, sp.cc_acuse AS numacuse,"   +
				"        i.cg_razon_social AS nombreif"  +
				" 			,p1.cd_descripcion AS periodicidad_pago," +
				"        TO_CHAR (sp.df_carga, 'DD/MM/YYYY') AS fecha_envio_if,"   +
				"        sp.ic_error_proceso AS error_proceso,"   +
				"        sp.cc_codigo_aplicacion AS codigoaplicacion,"   +
				"        ep.cg_descripcion AS error_descripcion"   +//FODEA 011-2010 FVR
				"   FROM com_solic_portal sp,"   +
				"        comcat_pyme p,"   +
				"        comcat_tasa ti,"   +
				"        comcat_tasa tuf,"   +
				"        comcat_if i,"   +
				"        comcat_emisor e,"   +
				"        comcat_moneda m,"   +
				"        comcat_tipo_credito tc,"   +
				"        comcat_clase_docto cd,"   +
				"        comcat_periodicidad p1,"   +
				"        comcat_periodicidad p2,"   +
				"        comcat_tabla_amort ta,"   +
				"        comcat_error_procesos ep"   +
				"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
				"    AND ti.ic_tasa = sp.ic_tasaif"   +
				"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
				"    AND i.ic_if = sp.ic_if"   +
				"    AND sp.ic_emisor = e.ic_emisor (+)"   +
				"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
				"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
				"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
				"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
				"    AND sp.ic_moneda = m.ic_moneda (+)"   +
				"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)" +
				"    AND sp.ic_error_proceso = ep.ic_error_proceso (+)"+//FODEA 011-2010 FVR
				"    AND sp.cc_codigo_aplicacion = ep.cc_codigo_aplicacion (+)");//FODEA 011-2010 FVR
				//"    AND sp.ic_estatus_solic = 3");
            qrySentencia.append((acuse.equals("")?"":" and SP.cc_acuse ='"+acuse+"'"));
			qrySentencia.append((ic_if.equals("")?"":" and SP.ic_if = "+ic_if));
			qrySentencia.append((fechaOper.equals("")?"":" and SP.df_operacion >=  TRUNC(TO_DATE('"+fechaOper+"','DD/MM/YYYY')) "));
			qrySentencia.append((fechaOperFin.equals("")?"":" and SP.df_operacion <  TRUNC(TO_DATE('"+fechaOperFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIf.equals("")?"":" and SP.df_carga >=  TRUNC(TO_DATE('"+fechaEnvioIf+"','DD/MM/YYYY')) "));//FODEA 011-2010 fvr
			qrySentencia.append((fechaEnvioIfFin.equals("")?"":" and SP.df_carga <  TRUNC(TO_DATE('"+fechaEnvioIfFin+"','DD/MM/YYYY')+1) "));//FODEA 011-2010 fvr
			qrySentencia.append((noSirac.equals("")?"":" and SP.ig_clave_sirac = "+noSirac));
			qrySentencia.append((noPrestamo.equals("")?"":" and SP.ig_numero_prestamo = "+noPrestamo));
			qrySentencia.append((estatus.equals("")?"":" AND sp.ic_estatus_solic = "+estatus));//FODEA 011-2010 fvr

	    }catch(Exception e){
			System.out.println("OperCredEleNafinDE::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
	
	String nombreArchivo = "";
				
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
				
					pdfDoc.setLTable(18, 108); //numero de columnas y el ancho que ocupara la tabla en el documento
				
					pdfDoc.setLCell("Nombre del Cliente","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("N�mero de Sirac","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Periodicidad de Pago de Capital","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha Emisi�n T�tulo de Cr�dito","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha Vencimiento Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo de Cr�dito","celda01",ComunesPDF.CENTER);//pendiente
					pdfDoc.setLCell("Monto del Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto del Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("N�mero de Folio","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("N�mero de Pr�stamo","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("N�mero de Acuse","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de Env�o del IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("C�digo de Error","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Descripci�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Intermediario","celda01",ComunesPDF.CENTER);
					
					while (rs.next()) 
						{
							String rs_nomCliente	= (rs.getString(1)==null)?"":rs.getString(1);
							String rs_numSirac		= (rs.getString(2)==null)?"":rs.getString(2);
							String rs_numDoc		= (rs.getString(3)==null)?"":rs.getString(3);
							String rs_period_pago	= (rs.getString("periodicidad_pago")==null)?"":rs.getString("periodicidad_pago");
							String rs_fechaEmision	= (rs.getString(4)==null)?"":rs.getString(4);
							String rs_fechaVenc		= (rs.getString(5)==null)?"":rs.getString(5);
							String rs_moneda		= (rs.getString(6)==null)?"":rs.getString(6);
							String rs_tipoCred		= (rs.getString(7)==null)?"":rs.getString(7);
							String rs_montoDoc		= (rs.getString(8)==null)?"":rs.getString(8);
							String rs_montoDesc		= (rs.getString(9)==null)?"":rs.getString(9);
							String rs_folio			= (rs.getString(10)==null)?"":rs.getString(10);
							String rs_fechaOper		= (rs.getString(11)==null)?"":rs.getString(11);
							String rs_numPrestamo	= (rs.getString(12)==null)?"":rs.getString(12);
							rs_numPrestamo	= rs_numPrestamo.equals("0")?"":rs_numPrestamo;
							String rs_numAcuse		= (rs.getString(13)==null)?"":rs.getString(13);
											
							//FODEA 011-2010 FVR-INI
							String rs_fechaenvioif	= (rs.getString("fecha_envio_if")==null)?"":rs.getString("fecha_envio_if");
							String rs_codigo_error	= ((rs.getString("codigoaplicacion")==null)?"":rs.getString("codigoaplicacion"))+
															  ((rs.getString("errorproceso")==null)?"":rs.getString("errorproceso"));
							rs_codigo_error=rs_codigo_error.equals("0")?"":rs_codigo_error;
							String rs_errordescripcion	= (rs.getString("errordescripcion")==null)?"":rs.getString("errordescripcion");
							
							//FODEA 011-2010 FVR-FIN
							String rs_intermed		= (rs.getString(14)==null)?"":rs.getString(14);
							if(rs_numDoc.equals("0"))  {  rs_numDoc =""; }							
							
							pdfDoc.setLCell(rs_nomCliente,"formas",ComunesPDF.LEFT);
							pdfDoc.setLCell(rs_numSirac,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(rs_numDoc,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(rs_period_pago,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(rs_fechaEmision,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(rs_fechaVenc,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(rs_moneda,"formas",ComunesPDF.LEFT);
							pdfDoc.setLCell(rs_tipoCred,"formas",ComunesPDF.LEFT);
							if(rs_montoDoc.equals("0"))  {	
								pdfDoc.setLCell(" " ,"formas",ComunesPDF.RIGHT);
							}else  {
								pdfDoc.setLCell("$"+Comunes.formatoDecimal(rs_montoDoc,2) ,"formas",ComunesPDF.RIGHT);
							}
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(rs_montoDesc,2) ,"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell(rs_folio,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(rs_fechaOper,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(rs_numPrestamo,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(rs_numAcuse,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(rs_fechaenvioif,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(rs_codigo_error,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(rs_errordescripcion,"formas",ComunesPDF.LEFT);
							pdfDoc.setLCell(rs_intermed,"formas",ComunesPDF.LEFT);
						}							
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}
		} 
		return  nombreArchivo;					
	}

/**
 * Genera el archivo CSV o PDF seg�n el tipo que se le indique.
 * En el caso del PDF, el archivo generado no comtempla paginaci�n, imprime todos los registros que trae la consulta.
 * @param request
 * @param rs
 * @param path
 * @param tipo: CSV o PDF
 * @return nombre del archivo
 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo = "";

		if("CSV".equals(tipo)){

			String linea = "";
			OutputStreamWriter writer = null;
			BufferedWriter buffer = null;
			try {

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
				linea="Nombre del Cliente,"+
						"N�mero de Sirac,"+
						"N�mero de Documento,"+
						"Periodicidad de Pago de Capital,"+
						"Fecha Emisi�n T�tulo de Cr�dito,"+
						"Fecha Vencimiento Descuento,"+
						"Moneda,"+
						"Tipo de Cr�dito,"+
						"Monto del Documento,"+
						"Monto del Descuento,"+
						"N�mero de Folio,"+
						"Fecha de Operaci�n,"+
						"N�mero de Pr�stamo,"+
						"N�mero de Acuse,"+
						"Fecha de Env�o del IF,"+
						"C�digo de Error,"+
						"Descripci�n,"+
						"Intermediario";
				buffer.write(linea);

				while (rs.next()){
					String rs_nomCliente	= (rs.getString(1)==null)?"":rs.getString(1);
					String rs_numSirac		= (rs.getString(2)==null)?"":rs.getString(2);
					String rs_numDoc		= (rs.getString(3)==null)?"":rs.getString(3);
					String rs_fechaEmision	= (rs.getString(4)==null)?"":rs.getString(4);
					String rs_fechaVenc		= (rs.getString(5)==null)?"":rs.getString(5);
					String rs_moneda		= (rs.getString(6)==null)?"":rs.getString(6);
					String rs_tipoCred		= (rs.getString(7)==null)?"":rs.getString(7);
					String rs_montoDoc		= (rs.getString(8)==null)?"":rs.getString(8);
					String rs_montoDesc		= (rs.getString(9)==null)?"":rs.getString(9);
					String rs_folio			= (rs.getString(10)==null)?"":rs.getString(10);
					String rs_fechaOper		= (rs.getString(11)==null)?"":rs.getString(11);
					String rs_numPrestamo	= (rs.getString(12)==null)?"":rs.getString(12);
					String rs_numAcuse		= (rs.getString(13)==null)?"":rs.getString(13);
					String rs_intermed		= (rs.getString(14)==null)?"":rs.getString(14);
					String rs_period_pago	= (rs.getString("periodicidad_pago")==null)?"":rs.getString("periodicidad_pago");
					//FODEA 011-2010 FVR-INI
					String rs_fechaenvioif	= (rs.getString("fecha_envio_if")==null)?"":rs.getString("fecha_envio_if");
					String rs_codigo_error	= ((rs.getString("codigoaplicacion")==null)?"":rs.getString("codigoaplicacion"))+
													  ((rs.getString("error_proceso")==null)?"":rs.getString("error_proceso"));
					String rs_errordescripcion	= (rs.getString("error_descripcion")==null)?"":rs.getString("error_descripcion");
					//FODEA 011-2010 FVR-FIN

					//Comunes.formatoDecimal(sMonto,5,false)
					linea =	"\n"+
								rs_nomCliente.replace(',',' ')+","+
								rs_numSirac+","+
								rs_numDoc+","+
								rs_period_pago+","+
								rs_fechaEmision+","+
								rs_fechaVenc+","+
								rs_moneda+","+
								rs_tipoCred.replace(',',' ')+","+
								rs_montoDoc+","+
								rs_montoDesc+","+
								rs_folio+","+
								rs_fechaOper+","+
								rs_numPrestamo+","+
								rs_numAcuse+","+
								rs_fechaenvioif+","+//FODEA 011-2010 FVR
								rs_codigo_error+","+//FODEA 011-2010 FVR
								rs_errordescripcion+","+//FODEA 011-2010 FVR
								rs_intermed.replace(',',' ');
					buffer.write(linea);
				}
				buffer.close();
			} catch (Throwable e){
				throw new AppException("Error al generar el archivo ", e);
			}

		} else if(tipo.equals("PDF")){

			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);

				String meses[]       = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual   = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual     = fechaActual.substring(0,2);
				String mesActual     = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual    = fechaActual.substring(6,10);
				String horaActual    = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));  

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(18, 108); //numero de columnas y el ancho que ocupara la tabla en el documento

				pdfDoc.setLCell("Nombre del Cliente","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de Sirac","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Periodicidad de Pago de Capital","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Emisi�n T�tulo de Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Vencimiento Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Cr�dito","celda01",ComunesPDF.CENTER);//pendiente
				pdfDoc.setLCell("Monto del Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto del Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de Folio","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de Pr�stamo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de Acuse","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Env�o del IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("C�digo de Error","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Descripci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Intermediario","celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();

				while (rs.next()) {
					String rs_nomCliente    = (rs.getString(1)==null)?"":rs.getString(1);
					String rs_numSirac      = (rs.getString(2)==null)?"":rs.getString(2);
					String rs_numDoc        = (rs.getString(3)==null)?"":rs.getString(3);
					String rs_period_pago   = (rs.getString("periodicidad_pago")==null)?"":rs.getString("periodicidad_pago");
					String rs_fechaEmision  = (rs.getString(4)==null)?"":rs.getString(4);
					String rs_fechaVenc     = (rs.getString(5)==null)?"":rs.getString(5);
					String rs_moneda        = (rs.getString(6)==null)?"":rs.getString(6);
					String rs_tipoCred      = (rs.getString(7)==null)?"":rs.getString(7);
					String rs_montoDoc      = (rs.getString(8)==null)?"":rs.getString(8);
					String rs_montoDesc     = (rs.getString(9)==null)?"":rs.getString(9);
					String rs_folio         = (rs.getString(10)==null)?"":rs.getString(10);
					String rs_fechaOper     = (rs.getString(11)==null)?"":rs.getString(11);
					String rs_numPrestamo   = (rs.getString(12)==null)?"":rs.getString(12);
					rs_numPrestamo          = rs_numPrestamo.equals("0")?"":rs_numPrestamo;
					String rs_numAcuse      = (rs.getString(13)==null)?"":rs.getString(13);

				//FODEA 011-2010 FVR-INI
				String rs_fechaenvioif  = (rs.getString("fecha_envio_if")==null)?"":rs.getString("fecha_envio_if");
				String rs_codigo_error  = ((rs.getString("codigoaplicacion")==null)?"":rs.getString("codigoaplicacion"))+
				((rs.getString("error_proceso")==null)?"":rs.getString("error_proceso"));
				rs_codigo_error=rs_codigo_error.equals("0")?"":rs_codigo_error;
				String rs_errordescripcion = (rs.getString("error_descripcion")==null)?"":rs.getString("error_descripcion");

				//FODEA 011-2010 FVR-FIN
				String rs_intermed = (rs.getString(14)==null)?"":rs.getString(14);
				if(rs_numDoc.equals("0")) {  rs_numDoc =""; }

				pdfDoc.setLCell(rs_nomCliente,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(rs_numSirac,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_numDoc,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_period_pago,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_fechaEmision,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_fechaVenc,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_moneda,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(rs_tipoCred,"formas",ComunesPDF.LEFT);
				if(rs_montoDoc.equals("0")){
					pdfDoc.setLCell(" " ,"formas",ComunesPDF.RIGHT);
				}	else{
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(rs_montoDoc,2) ,"formas",ComunesPDF.RIGHT);
				}
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(rs_montoDesc,2) ,"formas",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_folio,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_fechaOper,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_numPrestamo,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_numAcuse,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_fechaenvioif,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_codigo_error,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_errordescripcion,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(rs_intermed,"formas",ComunesPDF.LEFT);
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}

		}
		log.info("crearCustomFile (S)");
		return  nombreArchivo;
	}

	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public String getIc_if() {
		return ic_if;
	}


	public void setFechaOper(String fechaOper) {
		this.fechaOper = fechaOper;
	}


	public String getFechaOper() {
		return fechaOper;
	}


	public void setNoSirac(String noSirac) {
		this.noSirac = noSirac;
	}


	public String getNoSirac() {
		return noSirac;
	}


	public void setAcuse(String acuse) {
		this.acuse = acuse;
	}


	public String getAcuse() {
		return acuse;
	}


	public void setNoPrestamo(String noPrestamo) {
		this.noPrestamo = noPrestamo;
	}


	public String getNoPrestamo() {
		return noPrestamo;
	}


	public void setFechaOperFin(String fechaOperFin) {
		this.fechaOperFin = fechaOperFin;
	}


	public String getFechaOperFin() {
		return fechaOperFin;
	}


	public void setFechaEnvioIf(String fechaEnvioIf) {
		this.fechaEnvioIf = fechaEnvioIf;
	}


	public String getFechaEnvioIf() {
		return fechaEnvioIf;
	}


	public void setFechaEnvioIfFin(String fechaEnvioIfFin) {
		this.fechaEnvioIfFin = fechaEnvioIfFin;
	}


	public String getFechaEnvioIfFin() {
		return fechaEnvioIfFin;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	public String getEstatus() {
		return estatus;
	}
}