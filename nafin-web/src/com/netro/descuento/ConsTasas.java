package com.netro.descuento;

import com.netro.cadenas.ConsSeleccionIF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsTasas implements IQueryGeneratorRegExtJS {
	public ConsTasas() {	}
	
	private List conditions;
	StringBuffer strQuery;
	
	//Variables de Sesi�n
	private String iNoCliente;
	private String tipoConsulta;
	private String idioma;
	
	//Condiciones de la consulta
   private String fechaAnterior;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsSeleccionIF.class);
	
	public String getQuery(){
		
		strQuery	=	new StringBuffer();
		
		//Informaci�n General de Tasas
		if(tipoConsulta.equals("GeneralT")) {
		strQuery.append("SELECT (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social) AS nombreif, " +
				          "        ta.cs_vobo_epo, ta.cs_vobo_if, ta.cs_vobo_nafin, " +
				          "        TO_CHAR (ta.dc_fecha_tasa, 'DD/MM/YYYY HH24:MI:SS') AS dc_fecha_tasa, " +
				          "        t.cd_nombre AS nombretasa, m.cd_nombre AS nombremoneda, i.ic_if, " +
				          "		    p.cg_descripcion AS rangoplazo, ta.cg_rel_mat as relmat, ta.fn_puntos as ptos, mt.fn_valor as valor, " +
				          "		    TO_CHAR (tb.dc_fecha_tasa, 'DD/MM/YYYY HH24:MI:SS') AS fechatasa, " +
							 "        (Case When ta.cg_rel_mat = '+' Then (mt.fn_valor + ta.fn_puntos) " +
							 "              When ta.cg_rel_mat = '-' Then (mt.fn_valor - ta.fn_puntos) " +
							 "              When ta.cg_rel_mat = '*' Then (mt.fn_valor * ta.fn_puntos) " +
							 "              When ta.cg_rel_mat = '/' Then " + 
							 "                   (Case When ta.fn_puntos != 0 Then (mt.fn_valor / ta.fn_puntos) End) " +
							 "         End) as tasaAplicar " +
							 "  FROM  comcat_if i, " +
				          "        comrel_tasa_autorizada ta, " +
				          "        comcat_tasa t, "   +
				          "        comcat_moneda m, "   +
				          "        comrel_if_epo_moneda iem, " +
							 "       (SELECT ic_tasa, MAX (dc_fecha) AS dc_fecha FROM com_mant_tasa GROUP BY ic_tasa) x, " +
				          "       (SELECT tb.ic_epo, tb.ic_tasa, MAX (tb.dc_fecha_tasa) AS dc_fecha_tasa"   +
				          "        FROM comrel_tasa_base tb WHERE tb.ic_epo = " +iNoCliente+
				          "		    AND tb.CS_TASA_ACTIVA = 'S' GROUP BY tb.ic_epo, tb.ic_tasa) tbu, " +
				          "		    comrel_tasa_base tb,  comcat_plazo p, com_mant_tasa mt " +
				          " WHERE t.ic_tasa = x.ic_tasa " +
				          "   AND t.cs_disponible = 'S' " +
				          "   AND ta.ic_if = i.ic_if " +
				          "   AND ta.ic_epo = tbu.ic_epo"   +
				          "   AND ta.dc_fecha_tasa = tbu.dc_fecha_tasa " +
				          "   AND tbu.ic_tasa = t.ic_tasa " +
				          "   AND t.ic_moneda = m.ic_moneda " +
				          "   AND iem.ic_epo = ta.ic_epo " +
				          "   AND iem.ic_if = ta.ic_if " +
				          "   AND iem.ic_moneda = m.ic_moneda " +
				          "   AND i.cs_habilitado = 'S' " +
				          "   AND tbu.ic_tasa = tb.ic_tasa " +
    			          "   AND tb.dc_fecha_tasa = tbu.dc_fecha_tasa " +
    			          "   AND tb.ic_plazo = p.ic_plazo " +
    			          "   AND mt.dc_fecha = x.dc_fecha " +
    			          "   AND t.ic_tasa = mt.ic_tasa " +
							 "   AND M.IC_MONEDA in (1,54) "
							 );
		}
		//Informaci�n Historico Tasas
		else if(tipoConsulta.equals("HistoricoT")){
		strQuery.append("SELECT DISTINCT TO_CHAR (tb.dc_fecha_tasa, 'DD/MM/YYYY') as dc_fecha_tasa, tb.ic_epo " +
		                "  FROM comrel_tasa_base tb ");
		}
		else if(tipoConsulta.equals("HistoricoGeneralT")){
		strQuery.append("SELECT  (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social) AS nombreif," +
				          "        ta.cs_vobo_epo, ta.cs_vobo_if, ta.cs_vobo_nafin," +
				          "        TO_CHAR (ta.dc_fecha_tasa, 'DD/MM/YYYY HH24:MI:SS') as dc_fecha_tasa, t.cd_nombre AS nombretasa," +
				          "        m.cd_nombre"+idioma+" as nombremoneda, i.ic_if, m.ic_moneda, ta.cg_rel_mat as relmat, ta.fn_puntos as ptos, " +
        		          "		    p.cg_descripcion as rangoplazo, mt.fn_valor as valor, " +
							 "        (Case When ta.cg_rel_mat = '+' Then (mt.fn_valor + ta.fn_puntos) " +
							 "              When ta.cg_rel_mat = '-' Then (mt.fn_valor - ta.fn_puntos) " +
							 "              When ta.cg_rel_mat = '*' Then (mt.fn_valor * ta.fn_puntos) " +
							 "              When ta.cg_rel_mat = '/' Then " + 
							 "                   (Case When ta.fn_puntos != 0 Then (mt.fn_valor / ta.fn_puntos) End) " +
							"         End) as tasaAplicar" +
				          "  FROM  comcat_if i," +
				          "        comrel_tasa_autorizada ta," +
				          "        comcat_tasa t," +
				          "        comcat_moneda m," +
				          " 		 comrel_tasa_base tb, " +
        		          "		    comcat_plazo p, " +
        		          "		    com_mant_tasa mt, " +
				          "        (SELECT ic_tasa, MAX (dc_fecha) AS dc_fecha FROM com_mant_tasa GROUP BY ic_tasa) x, "
							 );
		}
		
		return strQuery.toString();
	}
	
	public String getAggregateCalculationQuery(){
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
	
	   log.debug("getAggregateCalculationQuery " + strQuery.toString());
		log.debug("getAggregateCalculationQuery) " + conditions);
		
		return strQuery.toString();
	}
	
	public String getDocumentQuery() {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
	
	   log.debug("getDocumentQuery " + strQuery.toString());
		log.debug("getDocumentQuery) " + conditions);
		
		return strQuery.toString();
	}
	
	public String getDocumentQueryFile(){
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
	   
		if(tipoConsulta.equals("GeneralT")){
		 strQuery.append(getQuery());
		 strQuery.append(" AND ta.ic_if in( " +
		                 " SELECT b.ic_if " +
		                 "   FROM comcat_if a, comrel_if_epo b" +
			       		  "  WHERE b.ic_epo = " +iNoCliente+
			 				  "    AND a.ic_if = b.ic_if"   +
							  "    AND b.cs_vobo_nafin = 'S'"   +
							  "    AND a.cs_habilitado = 'S'"   +
							  "    AND b.ic_if IN (SELECT DISTINCT rm.ic_if FROM comrel_if_epo_moneda rm " +
							  "                    WHERE rm.ic_epo = "+iNoCliente+") " +
							  "                ) ORDER BY 7 DESC "
							 );
		}
		else if(tipoConsulta.equals("HistoricoT")){
		 strQuery.append(getQuery());
		 if(fechaAnterior.equals("")){
		   
			strQuery.append(" ,(SELECT MAX (tb2.dc_fecha_tasa) AS fecha FROM comrel_tasa_base tb2 " +
					          "   WHERE tb2.ic_epo = "+iNoCliente+" AND tb2.cs_vobo_nafin = 'S') tbu "); 
			  
			 if(!iNoCliente.equals("")){			 
			 strQuery.append(" WHERE tb.ic_epo = ? AND tb.cs_vobo_nafin = 'S' " +
			                 "  AND TRUNC (tb.dc_fecha_tasa) != TRUNC (tbu.fecha) ");
			 conditions.add(iNoCliente);
		    }
		 }
		 else{
			 
			if(!iNoCliente.equals("")){
			 strQuery.append(" FROM comrel_tasa_base tb WHERE tb.ic_epo = ? AND tb.cs_vobo_nafin = 'S' ");
			 conditions.add(iNoCliente);
		    }
			 
			 strQuery.append("  AND TRUNC (tb.dc_fecha_tasa) != TRUNC (TO_DATE (?, 'DD/MM/YY')) ");
			 conditions.add(fechaAnterior);
		 }
		}
		else if(tipoConsulta.equals("HistoricoGeneralT")){
		 strQuery.append(getQuery());
		 if(!fechaAnterior.equals("")){
		 strQuery.append(" (SELECT tb2.ic_epo, tb2.ic_tasa, tb2.dc_fecha_tasa FROM comrel_tasa_base tb2 " +
					        "   WHERE tb2.ic_epo = "+iNoCliente+
					        "     AND tb2.cs_vobo_nafin = 'S' " +
					        "     AND TRUNC (tb2.dc_fecha_tasa) = TRUNC (TO_DATE (?, 'dd/mm/yyyy'))) tbu "
							 );
		 conditions.add(fechaAnterior);
		 }
		 else{
		 strQuery.append(" (SELECT tb.ic_epo, tb.ic_tasa, MAX (tb.dc_fecha_tasa) AS dc_fecha_tasa FROM comrel_tasa_base tb " +
					        "  WHERE tb.ic_epo = ? AND tb.cs_vobo_nafin = 'S'" +
					        "  GROUP BY tb.ic_epo, tb.ic_tasa) tbu "
							 );
       conditions.add(iNoCliente);
		 }
		 
		 strQuery.append("  WHERE t.ic_tasa = x.ic_tasa " +
				           "    AND t.cs_disponible = 'S'" +
				           "    AND ta.ic_epo = ? " +
				           "    AND ta.ic_if = i.ic_if " +
				           "    AND t.ic_moneda = m.ic_moneda " +
				           "    AND ta.ic_epo = tbu.ic_epo " +
				           "    AND ta.dc_fecha_tasa = tbu.dc_fecha_tasa " +
				           "    AND tbu.ic_tasa = t.ic_tasa "   +
                       "    AND tbu.ic_tasa = tb.ic_tasa " +
 				           "    AND tb.dc_fecha_tasa = tbu.dc_fecha_tasa " +
				           "    AND tb.ic_plazo = p.ic_plazo " +
				           "    AND mt.dc_fecha = x.dc_fecha " +
				           "    AND t.ic_tasa = mt.ic_tasa " +
				           "    AND ta.cs_vobo_nafin = 'S' "
							 );
		 conditions.add(iNoCliente);
		 
		 strQuery.append("    AND ta.ic_if in (SELECT b.ic_if FROM COMCAT_IF a, COMREL_IF_EPO b, comrel_producto_if cpi " +
				           "             WHERE b.ic_epo = " +iNoCliente+
				           "             AND a.ic_if=b.ic_if "+
				           "             AND b.cs_vobo_nafin='S' "+
				           "             AND a.cs_habilitado='S' "+
				           "             AND b.cs_bloqueo = 'N'"+
				           "             AND cpi.ic_if = a.ic_if " + 
                       "             AND cpi.ic_producto_nafin = 1 " +
				           "             AND b.ic_if in (select distinct rm.ic_if from  COMREL_IF_EPO_MONEDA rm where rm.ic_epo = "+iNoCliente+")) "
							  );

		 
		 strQuery.append("  ORDER BY m.ic_moneda ");
		}
		 
	   log.debug("getDocumentQueryFile " + strQuery.toString());
		log.debug("getDocumentQueryFile) " + conditions);
		
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		
		log.debug("getDocumentSummaryQueryForIds " + strQuery.toString());
		log.debug("getDocumentSummaryQueryForIds) " + conditions);
		
		return strQuery.toString();
	}
	
	
	/** M�todo en el cual se debe realizar la implementaci�n de la generaci�n del archivo
	 * en base al resulset que se recibe como par�metro. El ResulSet se obtiene de la consulta
	 * generada al invocar getDocumentQueryFile()
	 * @param request HttRequest empleado principalmente para obtener el onjeto session
	 * @param rs ResulSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta f�sica donde se generar� el archivo
	 * @param tipo cadena que identifica el tipo o variante de archivo que va a generar
	 * @return Cadena con la ruta del archivo generado.
	 */
	 public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)	{
		String nombreArchivo = "";
	   return nombreArchivo;
	 }
	 
	 
	 /** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	  * con base en el objeto registros que recibe como par�metro.
	  * @param request HttRequest empleado principalmente para obtener el objeto session
	  * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	  * @param path Ruta donde se generar� el archivo
	  * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	  * @return Cadena con la ruta del archivo generado
	  */
	  public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		  return "";
	  }
	  
	  public List getConditions() {
			return conditions;
	  }

	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}

	public String getINoCliente() {
		return iNoCliente;
	}

	public void setFechaAnterior(String fechaAnterior) {
		this.fechaAnterior = fechaAnterior;
	}

	public String getFechaAnterior() {
		return fechaAnterior;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public String getIdioma() {
		return idioma;
	}
		
}