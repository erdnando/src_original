package com.netro.descuento;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsBitaTasaPrefeNegoNafinExt implements IQueryGeneratorRegExtJS {//DBM

	
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsBitaTasaPrefeNegoNafinExt.class);

	StringBuffer qrySentencia = new StringBuffer("");
	StringBuffer	condicion  =new StringBuffer(); 		
	private List   conditions;

	private String tipoTasa="";
	private String epo="";
	private String claveIF="";
	private String moneda="";
	private String proveedor="";
	private String estatus="";
	private String fecha_registro_1="";
	private String fecha_registro_2="";
	private String  aplicaFloating ="";
	
	/*Constructor*/
	public ConsBitaTasaPrefeNegoNafinExt() { }
	
  
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery() {
	/*
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

      if (!"".equals(tipoTasa)) {
			condicion.append( " and b.CG_TIPOTASA = ?" );  
			conditions.add(tipoTasa);
		}
		if(!"".equals(epo)) {
			condicion.append( "and b.ic_epo = ?" );  
			conditions.add(epo);
		}
			
		if (!"".equals(claveIF)) {
			condicion.append( "and b.ic_if = ? " );  
			conditions.add(claveIF);
		}
			
		if (!"".equals(moneda)) {
			condicion.append( " and b.IC_MONEDA = ?" );  
			conditions.add(moneda);
			log.info("condicion :::..."+condicion);
			log.info("conditions :::..."+conditions);
		}
		if(!"0".equals(proveedor)) {
			condicion.append( "and b.ic_pyme = ?" );  
			conditions.add(proveedor);
		}
			
		if (!"".equals(estatus)) {
			condicion.append( "and b.cg_estatus = ? " );  
			conditions.add(estatus);
		}
		
		if (!"".equals(fecha_registro_1) && !"".equals(fecha_registro_2)) {
			condicion.append( "AND b.DF_FECHA_HORA >= TO_DATE(?,'dd/mm/YYYY') " +
									"AND b.DF_FECHA_HORA < TO_DATE(?,'dd/mm/YYYY')+1  ");  
			conditions.add(fecha_registro_1);
			conditions.add(fecha_registro_2);
		}

		qrySentencia.append("select count(1) total "+
												" from  "+
												" Bit_Tasas_Preferenciales b, "+
												" comcat_epo e, "+
												" comcat_pyme p, "+
												" comcat_moneda m "+
												" where  "+
												" b.ic_epo = e.ic_epo "+
												" and b.ic_pyme = p.ic_pyme "+
												" and b.ic_moneda = m.ic_moneda "+	condicion +
												" ORDER BY  b.DF_FECHA_HORA   ");
			
			
		log.info("getDocumentQuery :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
		*/
		return "";
	}
	
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() { // las  LLAVES
		log.info("getDocumentQuery(E) :::...");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
		if (!"".equals(tipoTasa)) {
			condicion.append( " and b.CG_TIPOTASA = ?" );  
			conditions.add(tipoTasa);
		}
		if(!"".equals(epo)) {
			condicion.append( "and b.ic_epo = ?" );  
			conditions.add(epo);
		}
			
		if (!"".equals(claveIF)) {
			condicion.append( "and b.ic_if = ? " );  
			conditions.add(claveIF);
		}
			
		if (!"".equals(moneda)) {
			condicion.append( " and b.IC_MONEDA = ?" );  
			conditions.add(moneda);
		}
		
		if(!proveedor.equals("0")) {
			condicion.append( "and b.ic_pyme = ?" );  
			conditions.add(proveedor);
		}
		
		if (!"".equals(fecha_registro_1) && !"".equals(fecha_registro_2)) {
			condicion.append( "AND b.DF_FECHA_HORA >= TO_DATE(?,'dd/mm/YYYY') " +
									"AND b.DF_FECHA_HORA < TO_DATE(?,'dd/mm/YYYY')+1  ");  
			conditions.add(fecha_registro_1);
			conditions.add(fecha_registro_2);
		}

		if (!"".equals(aplicaFloating)  ) {	
			condicion.append(" and b.CS_TASA_FLOATING = ? ");
			conditions.add(aplicaFloating);
		}	

		qrySentencia.append("select 	b.ic_Bitacora, b.IC_CUENTA_BANCARIA, b.IC_IF, b.IC_EPO, b.cg_tipoTasa, b.IC_PLAZO, b.IC_ORDEN, e.CG_RAZON_SOCIAL AS nombreEpo , p.CG_RAZON_SOCIAL AS nombreProveedor ,"+
									" CD_NOMBRE AS moneda, "+
									" DECODE(b.CG_TIPOTASA,'P','PREFERENCIAL','N','NEGOCIADA','') || '' ||  decode (b.CS_TASA_FLOATING , 'S', '/ FLOATING') as tipoTasa,"+
									"  b.CG_ESTATUS as estatus , "+
									"  DECODE(b.CG_ESTATUS,'E ','ELIMINAR','A ','ALTA') as estatusTasa,"+
									" b.FN_PUNTOS as puntos, "+
									" b.FN_VALOR	 as valor, "+
									" To_char(b.df_fecha_hora, 'DD/MM/YYYY HH24:MI:SS') as fechaRegistro,"+
									" b.CG_USUARIO as Usuario "+
									" from  "+
									" Bit_Tasas_Preferenciales b, "+
									" comcat_epo e, "+
									" comcat_pyme p, "+
									" comcat_moneda m "+
									" where  "+
									" b.ic_epo = e.ic_epo "+
									" and b.ic_pyme = p.ic_pyme "+
									" and b.ic_moneda = m.ic_moneda "+	condicion);
			
		if (!"".equals(estatus)) {
			qrySentencia.append(" and b.cg_estatus = '"+estatus+"'");
		}
		
		qrySentencia.append("  ORDER BY  b.DF_FECHA_HORA   ");
		
		log.info("getDocumentQuery :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * Obtiene el query necesario para mostrar la información completa de 
	 * una página a partir de las llaves primarias enviadas como parámetro
	 * @return Cadena con la consulta de SQL, para obtener la información
	 * completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) 
	{		
		log.info("getDocumentSummaryQueryForIds(E) :::...");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
		if (!"".equals(tipoTasa)) {
			condicion.append( " and b.CG_TIPOTASA = ?" );  
			conditions.add(tipoTasa);
		}
		if(!"".equals(epo)) {
			condicion.append( "and b.ic_epo = ?" );  
			conditions.add(epo);
		}
			
		if (!"".equals(claveIF)) {
			condicion.append( "and b.ic_if = ? " );  
			conditions.add(claveIF);
		}
			
		if (!"".equals(moneda)) {
			condicion.append( " and b.IC_MONEDA = ?" );  
			conditions.add(moneda);
		}
		if(!"0".equals(proveedor)) {
			condicion.append( "and b.ic_pyme = ?" );  
			conditions.add(proveedor);
		}
		
		if (!"".equals(fecha_registro_1) && !"".equals(fecha_registro_2)) {
			condicion.append( "AND b.DF_FECHA_HORA >= TO_DATE(?,'dd/mm/YYYY') " +
									"AND b.DF_FECHA_HORA < TO_DATE(?,'dd/mm/YYYY')+1  ");  
			conditions.add(fecha_registro_1);
			conditions.add(fecha_registro_2);
		}

		if (!"".equals(aplicaFloating)  ) {	
			condicion.append(" and b.CS_TASA_FLOATING = ? ");
			conditions.add(aplicaFloating);
		}	


		qrySentencia.append("select b.ic_Bitacora, 	b.IC_CUENTA_BANCARIA, b.IC_IF, b.IC_EPO, b.cg_tipoTasa, b.IC_PLAZO, b.IC_ORDEN , e.CG_RAZON_SOCIAL AS nombreEpo , p.CG_RAZON_SOCIAL AS nombreProveedor ,"+
									" CD_NOMBRE AS moneda, "+
									" DECODE(b.CG_TIPOTASA,'P','PREFERENCIAL','N','NEGOCIADA','') || '' ||  decode (b.CS_TASA_FLOATING , 'S', '/ FLOATING') as tipoTasa,"+
									" DECODE (b.CG_ESTATUS,'E   ','Eliminar','A   ','Alta')   as estatus, "+
									"  DECODE(b.CG_ESTATUS,'E','ELIMINAR','A','ALTA') as estatusTasa,"+
									" b.FN_PUNTOS as puntos, "+
									" b.FN_VALOR	 as valor, "+
									" To_char(b.df_fecha_hora, 'DD/MM/YYYY HH24:MI:SS') as fechaRegistro,"+
									" b.CG_USUARIO as Usuario "+
									" from  "+
									" Bit_Tasas_Preferenciales b, "+
									" comcat_epo e, "+
									" comcat_pyme p, "+
									" comcat_moneda m "+
									" where  "+
									" b.ic_epo = e.ic_epo "+
									" and b.ic_pyme = p.ic_pyme "+
									" and b.ic_moneda = m.ic_moneda "+	
									condicion );
										
			qrySentencia.append(" AND (");
			for(int i = 0; i < pageIds.size(); i++){
				List lItem = (ArrayList)pageIds.get(i);					
				if(i > 0){ qrySentencia.append("OR"); }
				qrySentencia.append(" (b.ic_Bitacora = " +lItem.get(0).toString());
				qrySentencia.append(" AND b.IC_CUENTA_BANCARIA = "+lItem.get(1).toString());
				qrySentencia.append(" AND b.IC_IF = "+lItem.get(2).toString());
				qrySentencia.append(" AND b.IC_EPO =  "+lItem.get(3).toString());
				qrySentencia.append(" AND b.cg_tipoTasa = '"+lItem.get(4).toString()+"'");
				qrySentencia.append(" AND b.IC_PLAZO =  "+lItem.get(5).toString());
				qrySentencia.append(" AND b.IC_ORDEN = ? )");
				conditions.add(new Long(lItem.get(6).toString()));
			}		
			qrySentencia.append(" ) ");
			if (!"".equals(estatus)) {
				qrySentencia.append(" and b.cg_estatus = '"+estatus+"'");
			}
			
			qrySentencia.append("  ORDER BY  b.DF_FECHA_HORA   ");
		
		log.info("getDocumentSummaryQueryForIds :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		
		log.info("getDocumentQueryFile(E) :::...");
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
	
		if (!"".equals(tipoTasa)) {
			condicion.append( " and b.CG_TIPOTASA = ?" );  
			conditions.add(tipoTasa);
		}
		if(!"".equals(epo)) {
			condicion.append( "and b.ic_epo = ?" );  
			conditions.add(epo);
		}
			
		if (!"".equals(claveIF)) {
			condicion.append( "and b.ic_if = ? " );  
			conditions.add(claveIF);
		}
			
		if (!"".equals(moneda)) {
			condicion.append( " and b.IC_MONEDA = ?" );  
			conditions.add(moneda);
		}
		if(!"0".equals(proveedor)) {
			condicion.append( "and b.ic_pyme = ?" );  
			conditions.add(proveedor);
		}
			
		if (!"".equals(estatus)) {
			condicion.append( "and b.cg_estatus = ? " );  
			conditions.add(estatus);
		}
		
		if (!"".equals(fecha_registro_1) && !"".equals(fecha_registro_2)) {
			condicion.append( "AND b.DF_FECHA_HORA >= TO_DATE(?,'dd/mm/YYYY') " +
									"AND b.DF_FECHA_HORA < TO_DATE(?,'dd/mm/YYYY')+1  ");  
			conditions.add(fecha_registro_1);
			conditions.add(fecha_registro_2);
		}

		if (!"".equals(aplicaFloating)  ) {	
			condicion.append(" and b.CS_TASA_FLOATING = ? ");
			conditions.add(aplicaFloating);
		}	


		qrySentencia.append( "select b.ic_Bitacora, 	b.IC_CUENTA_BANCARIA, b.IC_IF, b.IC_EPO, b.cg_tipoTasa, b.IC_PLAZO, b.IC_ORDEN , b.ic_tasa, e.CG_RAZON_SOCIAL AS nombreEpo , p.CG_RAZON_SOCIAL AS nombreProveedor ,"+
									" CD_NOMBRE AS moneda, "+
									" DECODE(b.CG_TIPOTASA,'P','PREFERENCIAL','N','NEGOCIADA','') || '' ||  decode (b.CS_TASA_FLOATING , 'S', '/ FLOATING') as tipoTasa,"+
									" DECODE (b.CG_ESTATUS,'E   ','Eliminar','A   ','Alta')   as estatus, "+
									"  DECODE(b.CG_ESTATUS,'E','ELIMINAR','A','ALTA') as estatusTasa,"+
									" b.FN_PUNTOS as puntos, "+
									" b.FN_VALOR	 as valor, "+
									" To_char(b.df_fecha_hora, 'DD/MM/YYYY HH24:MI:SS') as fechaRegistro,"+
									" b.CG_USUARIO as Usuario "+
									" from  "+
									" Bit_Tasas_Preferenciales b, "+
									" comcat_epo e, "+
									" comcat_pyme p, "+
									" comcat_moneda m "+
									" where  "+
									" b.ic_epo = e.ic_epo "+
									" and b.ic_pyme = p.ic_pyme "+
									" and b.ic_moneda = m.ic_moneda " + condicion +				 
									" ORDER BY  b.DF_FECHA_HORA");
		
		log.info("getDocumentQueryFile :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	} 
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos con PAGINADOR*/		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo)
	{	
		String nombreArchivo = "";
		return  nombreArchivo;
	}
	
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) SIN paginador*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet reg, String path, String tipo) {

		String nombreArchivo = "";
		if("CSV".equals(tipo))
		{
				String linea = "";
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				StringBuffer 	contenidoArchivo 	= new StringBuffer();
				CreaArchivo 	archivo 			= new CreaArchivo();
				String temp = "", temp1 = "";
					
				try {
				
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
					writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
					buffer = new BufferedWriter(writer);
					buffer.write(linea);
					
					linea="EPO,Proveedor,Moneda,Tipo Tasa, Estatus,Puntos,Valor,Fecha Registro,Usuario," + "\n";
					
					buffer.write(linea);
					
					while (reg.next()) 
					{
						String sEpo 			= (reg.getString("nombreEpo") == null ? "" : reg.getString("nombreEpo"));
						String sProveedor		= (reg.getString("nombreProveedor") == null ? "" : reg.getString("nombreProveedor"));
						String sMoneda			= (reg.getString("moneda")== null) ? "": reg.getString("moneda");
						String sTipoTasa  	= (reg.getString("tipoTasa") == null ? "" : reg.getString("tipoTasa"));
						String sEstatus 		= (reg.getString("estatus") == null ? "" : reg.getString("estatus"));
						String sPuntos 		= (reg.getString("puntos") == null ? "" : reg.getString("puntos"));
						String sValor 			= (reg.getString("valor") == null ? "" : reg.getString("valor"));
						String sFchReg 		= (reg.getString("fechaRegistro") == null ? "" : reg.getString("fechaRegistro"));
						String sUsuario 		= (reg.getString("Usuario") == null ? "" : reg.getString("Usuario"));
						
						linea =	sEpo.replaceAll(",", "")+","+ sProveedor.replaceAll(",", "")+","+ sMoneda+","+ sTipoTasa+","+sEstatus+","+
							sPuntos+","+Comunes.formatoDecimal(sValor,2,false)+","+sFchReg+","+sUsuario.replaceAll(",", "")+"\n";
								
						buffer.write(linea);
					}
					
						
					buffer.close();
				} catch (Throwable e) 
				{
					throw new AppException("Error al generar el archivo ", e);
				} 
		}
		
			return  nombreArchivo;
	}
	
	
	/*****************************************************
									SETTERS         
	*******************************************************/
	
	public void setTipoTasa (String tipoTasa ) { this.tipoTasa = tipoTasa; }
	
	public void setEpo (String epo ) { this.epo = epo ; }
	
	public void setClaveIF(String claveIF) { this.claveIF = claveIF; }
	
	public void setMoneda (String moneda) { this.moneda = moneda; }
	
	public void setProveedor (String proveedor) { this.proveedor = proveedor; }
	
	public void setEstatus (String estatus) { this.estatus = estatus; }
	
	public void setFecha_registro_de (String fecha_registro_1) { this.fecha_registro_1 = fecha_registro_1; }
	
	public void setFecha_registro_a (String fecha_registro_2) { this.fecha_registro_2 = fecha_registro_2; }
	
	/*****************************************************
								GETTERS
	*******************************************************/
	public String getTipoTasa() { return tipoTasa; }
	
	public String getEpo() { return epo; }
	
	public String getClaveIF() { return claveIF; }
	
	public String getMoneda () {  return moneda; }
	
	public String getProveedor() { return proveedor ; }
	
	public String getEstatus () {  return estatus; }
	
	public String getFecha_registro_de () { return fecha_registro_1; }
	
	public String getFecha_registro_a  () { return fecha_registro_2; }
	
	public List getConditions() {  return conditions;  }

	public void setAplicaFloating(String aplicaFloating) {
		this.aplicaFloating = aplicaFloating;
	}

	public String getAplicaFloating() {
		return aplicaFloating;
	}


}
