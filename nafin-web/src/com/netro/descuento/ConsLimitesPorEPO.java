package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsLimitesPorEPO implements IQueryGeneratorRegExtJS {//DBM

	
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsLimitesPorEPO.class);

	StringBuffer qrySentencia = new StringBuffer("");
	StringBuffer	condicion  =new StringBuffer(); 		
	private List   conditions;

	private String limitePyme="";
	private String epo="";
	private String bancoFondeo="";
	private String claveIF="";
	private String proveedor="";
	private int tipoConsulta =0;
	private int query = 0;
	
	/*Constructor*/
	public ConsLimitesPorEPO() { }
	
  
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery() { 
		return "";
	}
	
	/**
	* Obtiene el query para la CONSULTA
	* @return Cadena con la consulta de SQL
	*/
	public String getDocumentQuery() { // las  LLAVES
		log.info("getDocumentQuery(E) :::...");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
				
		//]Consulta cuando NO se eligio un proveedor y NO se eligio una EPO con limite
		if (query == 1) { //if("".equals(proveedor) && !"S".equals(limitePyme)) {
			
			if(!"".equals(epo)) {
				condicion.append( "and ie.ic_epo = ?" );  
				conditions.add(epo);
			}
				
			if (!"".equals(claveIF)) {
				condicion.append( "and ie.ic_if = ?" );  
				conditions.add(claveIF);
			}
				
			if (!"".equals(bancoFondeo)) {
				condicion.append( "and e.ic_banco_fondeo = ?" );  
				conditions.add(bancoFondeo);
			}
		
			qrySentencia.append( " SELECT ie.ic_epo, ie.ic_if " +
										" FROM comcat_epo e, comrel_if_epo ie, comcat_if i "+			
										" WHERE ie.ic_epo=e.ic_epo "+
										" 		AND ie.ic_if = i.ic_if "+
										" 		AND ie.cs_vobo_nafin = 'S' "+
										" 		and  ( ie.in_limite_epo != 0   or  ie.in_limite_epo_dl != 0  )  "+ condicion +				 
										" ORDER BY e.cg_razon_social, i.cg_razon_social");
		}
		
		
		else if (query == 2) { //else if(!"".equals(epo)&& "S".equals(limitePyme)) {
			
			conditions.add(epo);
			
			if (!"".equals(claveIF)) {
				condicion.append( "and lp.ic_if = ?" );  
				conditions.add(claveIF);
			}
			
			if (!"".equals(proveedor)) {
				condicion.append( "and lp.ic_pyme = ?" );  
				conditions.add(proveedor);
			}
						
			qrySentencia.append( " SELECT lp.ic_pyme, lp.ic_epo, lp.ic_if "+
										" FROM comcat_pyme p, comrel_limite_pyme_if_x_epo lp, comcat_if i, comcat_epo e, "+
										" 		comrel_if_epo  ie  "+  //F022-2011
										" WHERE lp.ic_pyme=p.ic_pyme "+
										"		AND lp.ic_epo=e.ic_epo "+
										"		AND lp.ic_if = i.ic_if "+
										"		AND lp.fn_limite != 0 "+
										"		AND lp.ic_epo=? "+condicion+
										" 		and ie.ic_epo = e.ic_epo "+ //F022-2011
										" 		and ie.ic_if = i.ic_if "+ //F022-2011	
										" ORDER BY p.cg_razon_social, i.cg_razon_social ");
		}
		
		log.info("getDocumentQuery :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	* Obtiene el query para obtener la consulta
	* @return Cadena con la consulta de SQL, para obtener la consulta
	*/
	public String getDocumentSummaryQueryForIds(List pageIds) 
	{		
		log.info("getDocumentSummaryQueryForIds(E) :::...");
		log.info("query) :::..."+query);
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
		//]Consulta cuando NO se eligio un proveedor y NO se eligio una EPO con limite		
		
		
		if (query == 1) {//if("".equals(proveedor) && !"S".equals(limitePyme)) {
			
			tipoConsulta = 1;
			
			qrySentencia.append( " SELECT /*+use_nl (e ie) index(ie)*/ (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ')||' '|| e.cg_razon_social) AS nombre_epo, " +
										" 		ie.in_limite_epo AS limite,  " +
										" 	 CASE "+
										"	WHEN ie.fn_limite_utilizado <>0 and  ie.in_limite_epo <>0   "+	
										"		THEN   ROUND (((ie.fn_limite_utilizado / ie.in_limite_epo) * 100),5) "+
										"		ELSE 0  "+
										"  END AS porcentaje,  "+
										" 		(ie.in_limite_epo - ie.fn_limite_utilizado) AS monto, " +
										" 		(DECODE (i.cs_habilitado, 'N', '*', 'S', ' ')||' '||i.cg_razon_social) if_nombre, " +
										"		DECODE (ie.cs_activa_limite,'S','Activa','N','Inactiva') AS estatus, " + //Fodea19-2005
										//" 		ie.cs_activa_limite AS estatus,  " + //Foda19_2005
										" 		TO_CHAR (ie.df_venc_linea, 'dd/mm/yyyy') AS df_venc_linea, " + //INICIO: CODIGO POR FODEA 051-2008
										" 		ie.ig_aviso_venc_linea AS aviso_venc_linea,  " +
										"  	TO_CHAR (ie.df_cambio_admon, 'dd/mm/yyyy') AS cambio_admon, " +
										"     ie.ig_aviso_cambio_admon AS aviso_cambio_admon,  " + // FIN: CODIGO POR FODrs_producto
										"     ie.fn_monto_comprometido AS monto_comprometido, " + // FODEA 005 - 2009
										"		DECODE (ie.CS_FECHA_LIMITE,'S','SI','N','NO') AS csFechalimite " + //F022-2011
										//"     ie.CS_FECHA_LIMITE as csFechalimite "+ //F022-2011										
										" 	,ie.in_limite_epo_dl AS limite_dl  " +
										" , CASE "+
										"	WHEN ie.fn_limite_utilizado_dl<>0 and  ie.in_limite_epo_dl <>0   "+
										"		THEN ROUND (((ie.fn_limite_utilizado_dl / ie.in_limite_epo_dl) * 100), 5     )  "+
										"	 ELSE 0  "+
										"  END AS porcentaje_dl "+		
										" 	,(ie.in_limite_epo_dl - ie.fn_limite_utilizado_dl) AS monto_dl " +
										"  ,ie.fn_monto_comprometido_dl AS monto_comprometido_dl " + 
										"  ,nvl(ie.in_limite_epo,0) - ( nvl(ie.fn_limite_utilizado, 0)  + NVL (ie.fn_monto_comprometido, 0)  )  as  disponible_des " +
										"  , nvl(ie.in_limite_epo_dl,0) - ( nvl(ie.fn_limite_utilizado_dl, 0)  + NVL (ie.fn_monto_comprometido_dl, 0)  )  as  disponible_des_dl, " +
										
										 " TO_CHAR (ie.DF_BLOQ_DES, 'dd/mm/yyyy')  as FECHA_BLOQ_DESB,   "+
										 " ie.CG_USUARIO_BLOQ_DES as USUARIO_BLOQ_DESB,   "+
										 " ie.CG_DEPENDENCIA_BLOQ_DES as DEPENDENCIA_BLOQ_DESB   "+ 
										 
										" FROM comcat_epo e, comrel_if_epo ie, comcat_if i "+			
										" WHERE ie.ic_epo=e.ic_epo "+
										" 		AND ie.ic_if = i.ic_if "+
										" 		AND ie.cs_vobo_nafin = 'S' "+
										" 		and ( ie.in_limite_epo != 0   or  ie.in_limite_epo_dl != 0  ) "+
										condicion);
										
			qrySentencia.append(" AND (");
			for(int i = 0; i < pageIds.size(); i++){
				List lItem = (ArrayList)pageIds.get(i);					
				if(i > 0){ qrySentencia.append("OR"); }
				qrySentencia.append("( ie.ic_epo = ? )");				qrySentencia.append(" AND ie.ic_if = ? ");
					
				conditions.add(lItem.get(0).toString());				conditions.add(lItem.get(1).toString());
			}		
			qrySentencia.append(" ) " +
			" ORDER BY e.cg_razon_social, i.cg_razon_social");
		}
		
		
		else if (query == 2) {//else if(!"".equals(epo)&& "S".equals(limitePyme)) {
			
			tipoConsulta = 2;
						
			qrySentencia.append( " SELECT (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as epo, "+
										" 		(decode (i.cs_habilitado,'N','*','S',' ')||' '||i.cg_razon_social) as if,"+
										" 		(decode (p.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as pyme, "+
										"		lp.fn_limite AS limite, "+
										"		ROUND(((nvl(lp.fn_limite_utilizado,0)/lp.fn_limite)*100),5) Porcentaje, "+
										"		(lp.fn_limite- nvl(lp.fn_limite_utilizado,0)) as utilizado,"+
										"		DECODE (lp.cs_activa_limite, 'S','Activa','N','Inactiva') AS estatus, " +
										"		lp.fn_monto_comprometido AS monto_comprometido, "+
										"		DECODE (ie.CS_FECHA_LIMITE,'S','SI','N','NO') AS csFechalimite , " + //F022-2011		
										 
										" FROM comcat_pyme p, comrel_limite_pyme_if_x_epo lp, comcat_if i, comcat_epo e, "+
										" 		comrel_if_epo  ie  "+  //F022-2011
										" WHERE lp.ic_pyme=p.ic_pyme "+
										"		AND lp.ic_epo=e.ic_epo "+
										"		AND lp.ic_if = i.ic_if "+
										"		AND lp.fn_limite != 0 "+ condicion+
										//"		AND lp.ic_epo=? "+condicion+
										" 		and ie.ic_epo = e.ic_epo "+ //F022-2011
										" 		and ie.ic_if = i.ic_if "); //F022-2011	
								
				qrySentencia.append(" AND (");
				for(int i = 0; i < pageIds.size(); i++){
					List lItem = (ArrayList)pageIds.get(i);					
					if(i > 0){ qrySentencia.append("OR"); }
					qrySentencia.append(" lp.ic_pyme = ? ");
					qrySentencia.append(" AND lp.ic_epo = ? ");
					qrySentencia.append(" AND lp.ic_if = ? ");
					conditions.add(lItem.get(0).toString());
					conditions.add(lItem.get(1).toString());
					conditions.add(lItem.get(2).toString());
				}											
				qrySentencia.append(" ) " +
										" ORDER BY e.cg_razon_social, i.cg_razon_social");
		}
		
		log.info("getDocumentSummaryQueryForIds :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		
		log.info("getDocumentQueryFile(E) :::...");
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
		if (query == 1) {//if("".equals(proveedor) && !"S".equals(limitePyme)) {								
		
			tipoConsulta = 1;
			if(!"".equals(epo)) {
				condicion.append( "and ie.ic_epo = ?" );  
				conditions.add(epo);
			}
				
			if (!"".equals(claveIF)) {
				condicion.append( "and ie.ic_if = ?" );  
				conditions.add(claveIF);
			}
				
			if (!"".equals(bancoFondeo)) {
				condicion.append( "and e.ic_banco_fondeo = ?" );  
				conditions.add(bancoFondeo);
			}
		
			qrySentencia.append( " SELECT /*+use_nl (e ie) index(ie)*/ (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ')||' '|| e.cg_razon_social) AS nombre_epo, " +
										" 		ie.in_limite_epo AS limite,  " +
									   "  CASE WHEN ie.fn_limite_utilizado <> 0  AND ie.in_limite_epo <> 0  "+									
										" 		THEN ROUND (((ie.fn_limite_utilizado / ie.in_limite_epo) * 100),5)  " +
										" 	ELSE 0 " +
										"  END AS porcentaje,   "+
										" 		(ie.in_limite_epo - ie.fn_limite_utilizado) AS monto, " +
										" 		(DECODE (i.cs_habilitado, 'N', '*', 'S', ' ')||' '||i.cg_razon_social) if_nombre, " +
										"		DECODE (ie.cs_activa_limite,'S','Activa','N','Inactiva') AS estatus, " + //Fodea19-2005
										" 		TO_CHAR (ie.df_venc_linea, 'dd/mm/yyyy') AS df_venc_linea, " + //INICIO: CODIGO POR FODEA 051-2008
										" 		ie.ig_aviso_venc_linea AS aviso_venc_linea,  " +
										"  	TO_CHAR (ie.df_cambio_admon, 'dd/mm/yyyy') AS cambio_admon, " +
										"     ie.ig_aviso_cambio_admon AS aviso_cambio_admon,  " + // FIN: CODIGO POR FODrs_producto
										"     ie.fn_monto_comprometido AS monto_comprometido, " + // FODEA 005 - 2009
										"		DECODE (ie.CS_FECHA_LIMITE,'S','SI','N','NO') AS csFechalimite " + //F022-2011
																				
										" 	,ie.in_limite_epo_dl AS limite_dl  " +
										" , CASE "+
										"	WHEN ie.fn_limite_utilizado_dl<>0 and  ie.in_limite_epo_dl <>0   "+
										"		THEN ROUND (((ie.fn_limite_utilizado_dl / ie.in_limite_epo_dl) * 100), 5     )  "+
										"	 ELSE 0  "+
										"  END AS porcentaje_dl "+		
										" 	,(ie.in_limite_epo_dl - ie.fn_limite_utilizado_dl) AS monto_dl " +
										"  ,ie.fn_monto_comprometido_dl AS monto_comprometido_dl " + 
										
										"  ,nvl(ie.in_limite_epo,0) - ( nvl(ie.fn_limite_utilizado, 0)  + NVL (ie.fn_monto_comprometido, 0)  )  as  disponible_des " +
										"  , nvl(ie.in_limite_epo_dl,0) - ( nvl(ie.fn_limite_utilizado_dl, 0)  + NVL (ie.fn_monto_comprometido_dl, 0)  )  as  disponible_des_dl ,  " +
										
										 " TO_CHAR (ie.DF_BLOQ_DES, 'dd/mm/yyyy')  as FECHA_BLOQ_DESB,   "+
										 " ie.CG_USUARIO_BLOQ_DES as USUARIO_BLOQ_DESB,   "+
										 " ie.CG_DEPENDENCIA_BLOQ_DES as DEPENDENCIA_BLOQ_DESB   "+ 
										 
										" FROM comcat_epo e, comrel_if_epo ie, comcat_if i "+			
										" WHERE ie.ic_epo=e.ic_epo "+
										" 		AND ie.ic_if = i.ic_if "+
										" 		AND ie.cs_vobo_nafin = 'S' "+
										" 		AND ( ie.in_limite_epo != 0  or     ie.in_limite_epo_dl != 0)  "+ condicion +				 
										" ORDER BY e.cg_razon_social, i.cg_razon_social");
		}
		
		else if (query == 2) {//else if(!"".equals(epo)&& "S".equals(limitePyme)) {	
			tipoConsulta = 2;
			conditions.add(epo);
			
			if (!"".equals(claveIF)) {
				condicion.append( "and lp.ic_if = ?" );  
				conditions.add(claveIF);
			}
			
			if (!"".equals(proveedor)) {
				condicion.append( "and lp.ic_pyme = ?" );   
				conditions.add(proveedor);
			}			
			
			qrySentencia.append( " SELECT (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as epo, "+
										" 		(decode (i.cs_habilitado,'N','*','S',' ')||' '||i.cg_razon_social) as if,"+
										" 		(decode (p.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as pyme, "+
										"		lp.fn_limite AS limite, "+
										"		ROUND(((nvl(lp.fn_limite_utilizado,0)/lp.fn_limite)*100),5) Porcentaje, "+
										"		(lp.fn_limite- nvl(lp.fn_limite_utilizado,0)) as utilizado,"+
										"		DECODE (lp.cs_activa_limite, 'S','Activa','N','Inactiva') AS estatus, " +
										"		lp.fn_monto_comprometido AS monto_comprometido, "+
										"		DECODE (ie.CS_FECHA_LIMITE,'S','SI','N','NO') AS csFechalimite " + //F022-2011	
										
										"  ,nvl(ie.in_limite_epo,0) - ( nvl(ie.fn_limite_utilizado, 0)  + NVL (ie.fn_monto_comprometido, 0)  )  as  disponible_des " +
										"  , nvl(ie.in_limite_epo_dl,0) - ( nvl(ie.fn_limite_utilizado_dl, 0)  + NVL (ie.fn_monto_comprometido_dl, 0)  )  as  disponible_des_dl , " +
																		 
										" FROM comcat_pyme p, comrel_limite_pyme_if_x_epo lp, comcat_if i, comcat_epo e, "+										
										" 		comrel_if_epo  ie  "+  //F022-2011
										" WHERE lp.ic_pyme=p.ic_pyme "+
										"		AND lp.ic_epo=e.ic_epo "+
										"		AND lp.ic_if = i.ic_if "+
										"		AND lp.fn_limite != 0   "+
										"		AND lp.ic_epo=? "+condicion+
										" 		and ie.ic_epo = e.ic_epo "+ //F022-2011
										" 		and ie.ic_if = i.ic_if "+ //F022-2011	
										" ORDER BY p.cg_razon_social, i.cg_razon_social ");
		}
		
		log.info("getDocumentQueryFile :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	} 
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos con PAGINADOR*/
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){

		log.info("crearCustomFile (E)");
		String nombreArchivo = "";

		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
				if(tipoConsulta == 1) {
					pdfDoc.setLTable(21, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
				
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,12);
					pdfDoc.setLCell("Vencimiento L�nea de Cr�dito","celda01",ComunesPDF.CENTER,2);
					pdfDoc.setLCell("Cambio de Administraci�n","celda01",ComunesPDF.CENTER,3);
					pdfDoc.setLCell("Cambio de Administraci�n","celda01",ComunesPDF.CENTER,4);
					
					pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto L�mite Moneda Nacional","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Porcentaje de Utilizaci�n Moneda Nacional","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible Moneda Nacional","celda01",ComunesPDF.CENTER);					
					pdfDoc.setLCell("Monto L�mite D�lar Americano","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Porcentaje de Utilizaci�n D�lar Americano ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible D�lar Americano","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Comprometido Moneda Nacional","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible despu�s de Comprometido Moneda Nacional","celda01",ComunesPDF.CENTER);					
					pdfDoc.setLCell("Monto Comprometido D�lar Americano","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible despu�s de Comprometido D�lar Americano","celda01",ComunesPDF.CENTER);					
				
					pdfDoc.setLCell("Fecha","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Aviso D�as Previos","celda01",ComunesPDF.CENTER);//pendiente
					
					pdfDoc.setLCell("Fecha","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Aviso D�as Previos","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Validar Fechas L�mite","celda01",ComunesPDF.CENTER);	
					
					pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(" Fecha de Bloqueo/Desbloqueo ", "celda01",ComunesPDF.CENTER);
				   pdfDoc.setLCell(" Usuario de Bloqueo/Desbloqueo   ", "celda01",ComunesPDF.CENTER);
				   pdfDoc.setLCell( "Dependencia que Bloqueo/Desbloqueo ", "celda01",ComunesPDF.CENTER);

					while (reg.next())		{
						String sEpo 			= (reg.getString("NOMBRE_EPO") == null ? "" : reg.getString("NOMBRE_EPO"));
						String sIf 				= (reg.getString("IF_NOMBRE") == null ? "" : reg.getString("IF_NOMBRE"));
						String sLimite			= (reg.getString("LIMITE")== null) ? "": reg.getString("LIMITE");
						String sPorcentaje 	= (reg.getString("PORCENTAJE") == null ? "" : reg.getString("PORCENTAJE"));
						String sMonto 			= (reg.getString("MONTO") == null ? "" : reg.getString("MONTO"));
						String sEstatus 		= (reg.getString("ESTATUS") == null ? "" : reg.getString("ESTATUS"));
						String sFchVenc 		= (reg.getString("DF_VENC_LINEA") == null ? "" : reg.getString("DF_VENC_LINEA"));
						String sAvisoVenc 	= ((reg.getString("AVISO_VENC_LINEA") == null || (reg.getString("AVISO_VENC_LINEA")).equals("0")) ? "" : reg.getString("AVISO_VENC_LINEA"));
						String sFchAdmon 		= (reg.getString("CAMBIO_ADMON") == null ? "" : reg.getString("CAMBIO_ADMON"));
						String sAvisoAdmon	= ((reg.getString("AVISO_CAMBIO_ADMON") == null || (reg.getString("AVISO_CAMBIO_ADMON")).equals("0")) ? "" : reg.getString("AVISO_CAMBIO_ADMON"));
						String sMontoC 		= (reg.getString("MONTO_COMPROMETIDO") == null ? "" : reg.getString("MONTO_COMPROMETIDO"));
						String sFchLimite		= (reg.getString("CSFECHALIMITE") == null ? "" : reg.getString("CSFECHALIMITE"));
						
						String sLimite_dl		= (reg.getString("LIMITE_DL")== null) ? "0": reg.getString("LIMITE_DL");
						String sPorcentaje_dl = (reg.getString("PORCENTAJE_DL") == null ? "0" : reg.getString("PORCENTAJE_DL"));
						String sMonto_dl    	= (reg.getString("MONTO_DL") == null ? "0" : reg.getString("MONTO_DL"));
						String sMontoC_dl 		= (reg.getString("MONTO_COMPROMETIDO_DL") == null ? "0" : reg.getString("MONTO_COMPROMETIDO_DL"));
					
						String disponible_des_dl = (reg.getString("DISPONIBLE_DES_DL") == null) ? "" : reg.getString("DISPONIBLE_DES_DL");
						String disponible_des = (reg.getString("DISPONIBLE_DES") == null) ? "" : reg.getString("DISPONIBLE_DES");
						
						String usuario_bloq_des = (reg.getString("USUARIO_BLOQ_DESB") == null) ? "" : reg.getString("USUARIO_BLOQ_DESB");
						String dependencia_bloq_des = (reg.getString("DEPENDENCIA_BLOQ_DESB") == null) ? "" : reg.getString("DEPENDENCIA_BLOQ_DESB");
						String fecha_bloq_des = (reg.getString("FECHA_BLOQ_DESB") == null) ? "" : reg.getString("FECHA_BLOQ_DESB");
											
											
						pdfDoc.setLCell(sEpo,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(sIf,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sLimite,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(Comunes.formatoDecimal(sPorcentaje,5)+"%" ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMonto,2) ,"formas",ComunesPDF.RIGHT);						
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sLimite_dl,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(Comunes.formatoDecimal(sPorcentaje_dl,5)+"%" ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMonto_dl,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMontoC,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(disponible_des.toString(),2) ,"formas",ComunesPDF.RIGHT);						
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMontoC_dl,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(disponible_des_dl.toString(),2) ,"formas",ComunesPDF.RIGHT);						
						
						pdfDoc.setLCell(sFchVenc,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(sAvisoVenc,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setLCell(sFchAdmon,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(sAvisoAdmon,"formas",ComunesPDF.CENTER);						
						pdfDoc.setLCell(sFchLimite,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setLCell(sEstatus,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(fecha_bloq_des,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(usuario_bloq_des,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(dependencia_bloq_des,"formas",ComunesPDF.LEFT);
				
					}	
				}
				else if (tipoConsulta == 2) {
					
					pdfDoc.setLTable(10, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
				
					pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Nombre de la PyME","celda01",ComunesPDF.CENTER);					
					pdfDoc.setLCell("Monto L�mite","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Porcentaje de Utilizaci�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Comprometido","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible despu�s de Comprometido","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Validar Fechas L�mite","celda01",ComunesPDF.CENTER);
										
					
					while (reg.next())
					{
						String sEpo 			= (reg.getString("EPO") == null ? "" : reg.getString("EPO"));
						String sIf 				= (reg.getString("IF") == null ? "" : reg.getString("IF"));
						String sPyme 			= (reg.getString("PYME") == null ? "" : reg.getString("PYME"));
						String sLimite			= (reg.getString("LIMITE")== null) ? "": reg.getString("LIMITE");
						String sPorcentaje 	= (reg.getString("PORCENTAJE") == null ? "" : reg.getString("PORCENTAJE"));
						String sMonto 			= (reg.getString("UTILIZADO") == null ? "" : reg.getString("UTILIZADO"));
						String sEstatus 		= (reg.getString("ESTATUS") == null ? "" : reg.getString("ESTATUS"));
						String sMontoC 		= (reg.getString("MONTO_COMPROMETIDO") == null ? "0" : reg.getString("MONTO_COMPROMETIDO"));
						BigDecimal bg_mnt_1  = sMonto.equals("")? new BigDecimal("0.0") : new BigDecimal(sMonto);
						BigDecimal bg_mnt_2  = sMontoC.equals("")? new BigDecimal("0.0") : new BigDecimal(sMontoC);
						BigDecimal bg_mnt_3  = bg_mnt_1.subtract(bg_mnt_2);					
						
						String sFchLimite		= (reg.getString("CSFECHALIMITE") == null ? "" : reg.getString("CSFECHALIMITE"));
			
						pdfDoc.setLCell(sEpo,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(sIf,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(sPyme,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sLimite,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(Comunes.formatoDecimal(sPorcentaje,5)+"%" ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMonto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(sEstatus,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMontoC,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(bg_mnt_3.toPlainString(),2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(sFchLimite,"formas",ComunesPDF.CENTER);
				
					}	
					
				}
			
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		} // if (tipo)	
		return  nombreArchivo;
	}
	
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) SIN paginador*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet reg, String path, String tipo){

		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		if("CSV".equals(tipo)){

				String linea = "";
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				StringBuffer contenidoArchivo = new StringBuffer();
				DecimalFormat df = new DecimalFormat("0.00000");
				try {
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
					writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
					buffer = new BufferedWriter(writer);
					buffer.write(linea);

					if(tipoConsulta == 1) {
						linea="EPO,Intermediario Financiero,Monto L�mite Moneda Nacional ,Porcentaje de Utilizaci�n Moneda Nacional ,Monto Disponible Moneda Nacional," +
							"  Monto L�mite D�lar Americano  ,Porcentaje de Utilizaci�n D�lar Americano ,Monto Disponible D�lar Americano, "                             +
							" Monto Comprometido Moneda Nacional ,Monto Disponible despu�s de Comprometido Moneda Nacional, "                                            +
							" Monto Comprometido D�lar Americano ,Monto Disponible despu�s de Comprometido D�lar Americano,"                                             +
							" Fecha Vencimiento L�nea de Cr�dito, Aviso D�as Previos, "                                                                                  +
							" Fecha Cambio de Administraci�n,Aviso D�as Previos,  Validar Fechas L�mite,"                                                                +
							" Estatus,  Fecha de Bloqueo/Desbloqueo ,  Usuario de Bloqueo/Desbloqueo , Dependencia qu e Bloqueo/Desbloqueo   "                           +
							"\n";
					}

					else if (tipoConsulta == 2){
						linea="EPO,Intermediario Financiero,Nombre de la PyME,Monto L�mite,Porcentaje de Utilizaci�n,Monto Disponible,Estatus," +
							"Monto Comprometido,Monto Disponible despu�s de Comprometido,Validar Fechas L�mite," + "\n";
					}

					buffer.write(linea);

					if(tipoConsulta == 1){
						while (reg.next()){
							String sEpo                 = (reg.getString("NOMBRE_EPO")            == null ? ""  : reg.getString("NOMBRE_EPO"));
							String sIf                  = (reg.getString("IF_NOMBRE")             == null ? ""  : reg.getString("IF_NOMBRE"));
							String sLimite              = (reg.getString("LIMITE")                == null ? ""  : reg.getString("LIMITE"));
							String sPorcentaje          = (reg.getString("PORCENTAJE")            == null ? ""  : reg.getString("PORCENTAJE"));
							String sMonto               = (reg.getString("MONTO")                 == null ? ""  : reg.getString("MONTO"));
							String sEstatus             = (reg.getString("ESTATUS")               == null ? ""  : reg.getString("ESTATUS"));
							String sFchVenc             = (reg.getString("DF_VENC_LINEA")         == null ? ""  : reg.getString("DF_VENC_LINEA"));
							String sAvisoVenc           = (reg.getString("AVISO_VENC_LINEA")      == null ? ""  : reg.getString("AVISO_VENC_LINEA"));
							String sFchAdmon            = (reg.getString("CAMBIO_ADMON")          == null ? ""  : reg.getString("CAMBIO_ADMON"));
							String sAvisoAdmon          = (reg.getString("AVISO_CAMBIO_ADMON")    == null ? ""  : reg.getString("AVISO_CAMBIO_ADMON"));
							String sMontoC              = (reg.getString("MONTO_COMPROMETIDO")    == null ? ""  : reg.getString("MONTO_COMPROMETIDO"));
							String sFchLimite           = (reg.getString("CSFECHALIMITE")         == null ? ""  : reg.getString("CSFECHALIMITE"));
							String sLimite_dl           = (reg.getString("LIMITE_DL")             == null ? ""  : reg.getString("LIMITE_DL"));
							String sPorcentaje_dl       = (reg.getString("PORCENTAJE_DL")         == null ? "0" : reg.getString("PORCENTAJE_DL"));
							String sMonto_dl            = (reg.getString("MONTO_DL")              == null ? ""  : reg.getString("MONTO_DL"));
							String sMontoC_dl           = (reg.getString("MONTO_COMPROMETIDO_DL") == null ? ""  : reg.getString("MONTO_COMPROMETIDO_DL"));
							String disponible_des_dl    = (reg.getString("DISPONIBLE_DES_DL")     == null ? ""  : reg.getString("DISPONIBLE_DES_DL"));
							String disponible_des       = (reg.getString("DISPONIBLE_DES")        == null ? ""  : reg.getString("DISPONIBLE_DES"));
							String usuario_bloq_des     = (reg.getString("USUARIO_BLOQ_DESB")     == null ? ""  : reg.getString("USUARIO_BLOQ_DESB"));
							String dependencia_bloq_des = (reg.getString("DEPENDENCIA_BLOQ_DESB") == null ? ""  : reg.getString("DEPENDENCIA_BLOQ_DESB"));
							String fecha_bloq_des       = (reg.getString("FECHA_BLOQ_DESB")       == null ? ""  : reg.getString("FECHA_BLOQ_DESB"));

							double dPorcentaje = Double.parseDouble(sPorcentaje);
							linea = sEpo.replaceAll(",", "")+","+ sIf.replaceAll(",", "")+","+
								"\"$"+Comunes.formatoMoneda2(String.valueOf(sLimite),false)+"\","+
								df.format(dPorcentaje)+"%"+","+
								"\"$"+Comunes.formatoMoneda2(String.valueOf(sMonto),false)+"\","+
								"\"$"+Comunes.formatoMoneda2(String.valueOf(sLimite_dl),false)+"\","+
								sPorcentaje_dl+"%"+","+
								"\"$"+Comunes.formatoMoneda2(String.valueOf(sMonto_dl),false)+"\","+
								Comunes.formatoDecimal(sMontoC,2,false)+","+	Comunes.formatoDecimal(disponible_des,2,false)+","+
								Comunes.formatoDecimal(sMontoC_dl,2,false)+","+	Comunes.formatoDecimal(disponible_des_dl,2,false)+","+
								sFchVenc+","+sAvisoVenc+","+
								sFchAdmon+","+sAvisoAdmon+","+sFchLimite+","+
								sEstatus+","+
								fecha_bloq_des.replaceAll(",", "")+","+
								usuario_bloq_des.replaceAll(",", "")+","+
								dependencia_bloq_des.replaceAll(",", "")+","+
								"\n";

							buffer.write(linea);
						}
					}
					else if (tipoConsulta == 2){
						while (reg.next()){
							String sEpo        = (reg.getString("EPO")                == null ? "" : reg.getString("EPO"));
							String sIf         = (reg.getString("IF")                 == null ? "" : reg.getString("IF"));
							String sPyme       = (reg.getString("PYME")               == null ? "" : reg.getString("PYME"));
							String sLimite     = (reg.getString("LIMITE")             == null ? "" : reg.getString("LIMITE"));
							String sPorcentaje = (reg.getString("PORCENTAJE")         == null ? "" : reg.getString("PORCENTAJE"));
							String sMonto      = (reg.getString("UTILIZADO")          == null ? "" : reg.getString("UTILIZADO"));
							String sEstatus    = (reg.getString("ESTATUS")            == null ? "" : reg.getString("ESTATUS"));
							String sMontoC     = (reg.getString("MONTO_COMPROMETIDO") == null ? "" : reg.getString("MONTO_COMPROMETIDO"));
							
							BigDecimal bg_mnt_1 = sMonto.equals("")? new BigDecimal("0.0") : new BigDecimal(sMonto);
							BigDecimal bg_mnt_2 = sMontoC.equals("")? new BigDecimal("0.0") : new BigDecimal(sMontoC);
							BigDecimal bg_mnt_3 = bg_mnt_1.subtract(bg_mnt_2);

							String sMontoDespues = String.valueOf(bg_mnt_3);
							String sFchLimite    = (reg.getString("CSFECHALIMITE") == null ? "" : reg.getString("CSFECHALIMITE"));

							double dPorcentaje = Double.parseDouble(sPorcentaje);

							linea =sEpo.replaceAll(",", "")+","+ sIf.replaceAll(",", "")+","+sPyme.replaceAll(",", "")+","+"\"$"+Comunes.formatoMoneda2(String.valueOf(sLimite),false)+"\","+
									df.format(dPorcentaje)+"%"+","+"\"$"+Comunes.formatoMoneda2(String.valueOf(sMonto),false)+"\","+
									sEstatus+","+Comunes.formatoDecimal(sMontoC,2,false)+","+Comunes.formatoDecimal(sMontoDespues,2,false)+","+sFchLimite+"\n";
							buffer.write(linea);
						}
					}
					buffer.close();
				} catch (Throwable e) {
					throw new AppException("Error al generar el archivo ", e);
				} 
		} else if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				if(tipoConsulta == 1) {
					pdfDoc.setLTable(21, 100); //numero de columnas y el ancho que ocupara la tabla en el documento

					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,12);
					pdfDoc.setLCell("Vencimiento L�nea de Cr�dito","celda01",ComunesPDF.CENTER,2);
					pdfDoc.setLCell("Cambio de Administraci�n","celda01",ComunesPDF.CENTER,3);
					pdfDoc.setLCell("Cambio de Administraci�n","celda01",ComunesPDF.CENTER,4);
					pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto L�mite Moneda Nacional","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Porcentaje de Utilizaci�n Moneda Nacional","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible Moneda Nacional","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto L�mite D�lar Americano","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Porcentaje de Utilizaci�n D�lar Americano ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible D�lar Americano","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Comprometido Moneda Nacional","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible despu�s de Comprometido Moneda Nacional","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Comprometido D�lar Americano","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible despu�s de Comprometido D�lar Americano","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Aviso D�as Previos","celda01",ComunesPDF.CENTER);//pendiente
					pdfDoc.setLCell("Fecha","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Aviso D�as Previos","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Validar Fechas L�mite","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(" Fecha de Bloqueo/Desbloqueo ", "celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(" Usuario de Bloqueo/Desbloqueo   ", "celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell( "Dependencia que Bloqueo/Desbloqueo ", "celda01",ComunesPDF.CENTER);
					pdfDoc.setLHeaders();

					while (reg.next()){
						String sEpo                 = (reg.getString("NOMBRE_EPO") == null ? "" : reg.getString("NOMBRE_EPO"));
						String sIf                  = (reg.getString("IF_NOMBRE") == null ? "" : reg.getString("IF_NOMBRE"));
						String sLimite              = (reg.getString("LIMITE")== null) ? "": reg.getString("LIMITE");
						String sPorcentaje          = (reg.getString("PORCENTAJE") == null ? "" : reg.getString("PORCENTAJE"));
						String sMonto               = (reg.getString("MONTO") == null ? "" : reg.getString("MONTO"));
						String sEstatus             = (reg.getString("ESTATUS") == null ? "" : reg.getString("ESTATUS"));
						String sFchVenc             = (reg.getString("DF_VENC_LINEA") == null ? "" : reg.getString("DF_VENC_LINEA"));
						String sAvisoVenc           = ((reg.getString("AVISO_VENC_LINEA") == null || (reg.getString("AVISO_VENC_LINEA")).equals("0")) ? "" : reg.getString("AVISO_VENC_LINEA"));
						String sFchAdmon            = (reg.getString("CAMBIO_ADMON") == null ? "" : reg.getString("CAMBIO_ADMON"));
						String sAvisoAdmon          = ((reg.getString("AVISO_CAMBIO_ADMON") == null || (reg.getString("AVISO_CAMBIO_ADMON")).equals("0")) ? "" : reg.getString("AVISO_CAMBIO_ADMON"));
						String sMontoC              = (reg.getString("MONTO_COMPROMETIDO") == null ? "" : reg.getString("MONTO_COMPROMETIDO"));
						String sFchLimite           = (reg.getString("CSFECHALIMITE") == null ? "" : reg.getString("CSFECHALIMITE"));
						String sLimite_dl           = (reg.getString("LIMITE_DL")== null) ? "0": reg.getString("LIMITE_DL");
						String sPorcentaje_dl       = (reg.getString("PORCENTAJE_DL") == null ? "0" : reg.getString("PORCENTAJE_DL"));
						String sMonto_dl            = (reg.getString("MONTO_DL") == null ? "0" : reg.getString("MONTO_DL"));
						String sMontoC_dl           = (reg.getString("MONTO_COMPROMETIDO_DL") == null ? "0" : reg.getString("MONTO_COMPROMETIDO_DL"));
						String disponible_des_dl    = (reg.getString("DISPONIBLE_DES_DL") == null) ? "" : reg.getString("DISPONIBLE_DES_DL");
						String disponible_des       = (reg.getString("DISPONIBLE_DES") == null) ? "" : reg.getString("DISPONIBLE_DES");
						String usuario_bloq_des     = (reg.getString("USUARIO_BLOQ_DESB") == null) ? "" : reg.getString("USUARIO_BLOQ_DESB");
						String dependencia_bloq_des = (reg.getString("DEPENDENCIA_BLOQ_DESB") == null) ? "" : reg.getString("DEPENDENCIA_BLOQ_DESB");
						String fecha_bloq_des       = (reg.getString("FECHA_BLOQ_DESB") == null) ? "" : reg.getString("FECHA_BLOQ_DESB");

						pdfDoc.setLCell(sEpo,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(sIf,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sLimite,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(Comunes.formatoDecimal(sPorcentaje,5)+"%" ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMonto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sLimite_dl,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(Comunes.formatoDecimal(sPorcentaje_dl,5)+"%" ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMonto_dl,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMontoC,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(disponible_des.toString(),2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMontoC_dl,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(disponible_des_dl.toString(),2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(sFchVenc,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(sAvisoVenc,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(sFchAdmon,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(sAvisoAdmon,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(sFchLimite,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(sEstatus,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(fecha_bloq_des,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(usuario_bloq_des,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(dependencia_bloq_des,"formas",ComunesPDF.LEFT);
					}
				} else if (tipoConsulta == 2) {

					pdfDoc.setLTable(10, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
					pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Nombre de la PyME","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto L�mite","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Porcentaje de Utilizaci�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Comprometido","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Disponible despu�s de Comprometido","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Validar Fechas L�mite","celda01",ComunesPDF.CENTER);
					pdfDoc.setLHeaders();

					while (reg.next()) {
						String sEpo         = (reg.getString("EPO") == null ? "" : reg.getString("EPO"));
						String sIf          = (reg.getString("IF") == null ? "" : reg.getString("IF"));
						String sPyme        = (reg.getString("PYME") == null ? "" : reg.getString("PYME"));
						String sLimite      = (reg.getString("LIMITE")== null) ? "": reg.getString("LIMITE");
						String sPorcentaje  = (reg.getString("PORCENTAJE") == null ? "" : reg.getString("PORCENTAJE"));
						String sMonto       = (reg.getString("UTILIZADO") == null ? "" : reg.getString("UTILIZADO"));
						String sEstatus     = (reg.getString("ESTATUS") == null ? "" : reg.getString("ESTATUS"));
						String sMontoC      = (reg.getString("MONTO_COMPROMETIDO") == null ? "0" : reg.getString("MONTO_COMPROMETIDO"));
						BigDecimal bg_mnt_1 = "".equals(sMonto)? new BigDecimal("0.0") : new BigDecimal(sMonto);
						BigDecimal bg_mnt_2 = "".equals(sMontoC)? new BigDecimal("0.0") : new BigDecimal(sMontoC);
						BigDecimal bg_mnt_3 = bg_mnt_1.subtract(bg_mnt_2);
						String sFchLimite   = (reg.getString("CSFECHALIMITE") == null ? "" : reg.getString("CSFECHALIMITE"));

						pdfDoc.setLCell(sEpo,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(sIf,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(sPyme,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sLimite,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(Comunes.formatoDecimal(sPorcentaje,5)+"%" ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMonto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(sEstatus,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(sMontoC,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(bg_mnt_3.toPlainString(),2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(sFchLimite,"formas",ComunesPDF.CENTER);

					}
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo ",e);
			}
		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	 /**
	 * Este m�todo VERIFICA si la EPO tiene activo el limite por pyme
	 * @param ic_if clave del IF
	 * @param ic_epo clave de la EPO
	 * @throws AppException Cuando ocurre alg�n error en la consulta.
	 */
	 public String verificaLimitesEPO(String ic_epo) throws AppException {
		log.info("verificaLimitesEPO (E)");
		String qrySentencia="",condicion1="";
		PreparedStatement ps	= null;
		ResultSet			rs	= null;
		String rs_epo="", rs_limite_pyme="";
		String rs_pyme="", rs_nombre_pyme="";
		List listPyme = new ArrayList();
		HashMap mapPyme = null;
		
		AccesoDB con = new AccesoDB();
	
		try{
			con.conexionDB();		
			
			qrySentencia = " SELECT DISTINCT cpe.ic_epo, cpe.cs_limite_pyme  "+
								"  FROM comrel_producto_epo cpe "+
								"  WHERE cpe.cs_limite_pyme= 'S' "+
								"  AND cpe.ic_epo=? ";
				
			log.debug(":::::QUERY::::::"+qrySentencia);
			log.debug("ic_epo = "+ic_epo);
			
			try {
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,Integer.parseInt(ic_epo));
				rs = ps.executeQuery();
				ps.clearParameters();
				
				if(rs.next()) {
					rs_epo 			= (rs.getString(1)==null)?"":rs.getString(1);
					rs_limite_pyme = (rs.getString(2)==null)?"":rs.getString(2); 
				}
				else{
					rs_limite_pyme = "";
				}
				rs.close();
				if(ps!=null) 
					ps.close();
					
				
			} catch (SQLException sqle) { throw new AppException("No Existen Limites asociados a alguna EPO para este IF."); }
														//throw new NafinException("DSCT0032");
		} catch (Exception e) {
					log.info("verificaLimitesEPO -ERROR- ");
					e.printStackTrace();
		} finally {
				if (con.hayConexionAbierta()) con.cierraConexionDB();
		  }
		log.info("verificaLimitesEPO (S)");
		return rs_limite_pyme;
	} // Fin Metodo verificaLimitesEPO
	/**
	 * Este m�todo obtiene las Pymes asociadas a las EPOs con limite 
	 * @param ic_if clave del IF
	 * @param ic_epo clave de la EPO
	 * @throws AppException Cuando ocurre alg�n error en la consulta.
	 */	
	public List limitesPyme(String ic_epo,String ic_if) throws AppException {
		log.info("limitesPyme (E)");
		String qrySentencia="",condicion1="";
		PreparedStatement ps	= null;
		ResultSet			rs	= null;
		String rs_epo="", rs_limite_pyme="";
		String rs_pyme="", rs_nombre_pyme="";
		List listPyme = new ArrayList();
		HashMap mapPyme = null;
		
		AccesoDB con = new AccesoDB();
	
		try {
			con.conexionDB();		
				
			if(!"".equals(ic_if))
				condicion1 = " and cpl.ic_if = ? ";
			qrySentencia =	" SELECT DISTINCT cpl.ic_pyme, p.cg_razon_social "+
								" FROM comrel_limite_pyme_if_x_epo cpl, comcat_pyme p "+
								" WHERE cpl.ic_pyme=p.ic_pyme "+
								" AND fn_limite !=0 "+
								" AND cpl.ic_epo =? "+ condicion1+
								" ORDER BY 2"  ;
			log.debug(":::::QUERY::::::"+qrySentencia);
			log.debug("ic_epo = "+ic_epo);
			log.debug("ic_if = "+ic_if);
			
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_epo));
			if (!"".equals(ic_if)) {
				ps.setInt(2,Integer.parseInt(ic_if));
			}
	
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()) {
				mapPyme = new HashMap();
				rs_pyme 			= (rs.getString(1)==null)?"":rs.getString(1);
				rs_nombre_pyme = (rs.getString(2)==null)?"":rs.getString(2);	
				mapPyme.put("rs_pyme",rs_pyme);
				mapPyme.put("rs_nombre_pyme",rs_nombre_pyme);
				listPyme.add(mapPyme);
			}
			rs.close();
			if(ps!=null) 
				ps.close(); 
		
		} catch (Exception e) {
					log.info("limitesPyme -ERROR- ");
					e.printStackTrace();
		} finally {
				if (con.hayConexionAbierta()) con.cierraConexionDB();
		  }
		log.info("limitesPyme (S)");
		return listPyme;
	} // Fin Metodo verificaLimitesEPO
	
	/*****************************************************
									SETTERS         
	*******************************************************/
	
	public void setLimitePyme (String limitePyme ) { this.limitePyme = limitePyme; }
	
	public void setClaveIF(String claveIF) { this.claveIF = claveIF; }
	
	public void setEpo (String epo ) { this.epo = epo ; }
	
	public void setBancoFondeo (String bancoFondeo) { this.bancoFondeo = bancoFondeo; }
	
	public void setProveedor (String proveedor) { this.proveedor = proveedor; }
	
	public void setTipoConsulta (int query) { this.query = query; }
	
	/*****************************************************
								GETTERS
	*******************************************************/
	public String getLimitePyme() { return limitePyme; }

	public String getClaveIF() { return claveIF; }
	
	public String getEpo() { return epo; }
	
	public String getBancoFondeo() { return bancoFondeo; }
	
	public String getProveedor() { return proveedor ; }
	
	public List getConditions() {  return conditions;  }  
  
	

	

}