package com.netro.descuento;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;

public class RepCEstatusPymeDEbean{
 	 
	private String qrysentencia="";
	private String iccambioestatusdocto="";
	private String sFechaVencPyme="";
	private String  operaFactConMandato="";
	private String icepo="";
	private String icpyme="";
	private Registros registros;
	private List conditions;
	private boolean operafactoraje=false;
	
	public void setOperaFactConMandato(String operaFactConMandato) {
		this.operaFactConMandato=operaFactConMandato;
	}  
	public void setIccambioestatusdocto(String iccambioestatusdocto){		
		this.iccambioestatusdocto=iccambioestatusdocto;
	}
	
	public void setFechaVencPyme(String sFechaVencPyme){		
		this.sFechaVencPyme=sFechaVencPyme;
	}
	
	public void setIceEpo(String icepo){
		this.icepo=icepo;
	}
	
	public void setIcePyme(String icPyme) {
		this.icpyme = icPyme;
	}

	public Registros executeQuery() {
		AccesoDB con = new AccesoDB();
		Registros registros = new Registros();
		conditions = new ArrayList();
		try{
			con.conexionDB();
			conditions.add(this.icpyme);
			conditions.add(this.icepo);
			registros = con.consultarDB(this.getQrysentencia(),conditions);
			return registros;
		} catch(Exception e) {
			throw new AppException("RepDoctosPymeDEbean::ExecuteQuery(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}
	public void setQrysentencia(String x){
    String campos="",condiciones="",tablas="",ordenarPor="";    
    //5  Baja  //6  Descuento fisisco  //7  pagado anticipado
	//2  Seleccionado Pyme a negociable //8  modidificacion de montos
	//26 pignorado a negociable //28 aplicacion de nota de credito //29 Programado a Negociable
	
		campos =" SELECT (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' '|| e.cg_razon_social) As nombreepo	, "+
				"       d.ig_numero_docto, TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') As fecha_docto, "+
				"       TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'DD/MM/YYYY') As fecha_venc, m.cd_nombre, "+((!this.iccambioestatusdocto.equals("28"))?"d.fn_monto, ":"")+
				"       "+((this.iccambioestatusdocto.equals("8")||this.iccambioestatusdocto.equals("29"))?"ce":"d")+".cc_acuse, "+ 
				((!this.iccambioestatusdocto.equals("28"))?"d":"m" )+".ic_moneda, "+
				((this.iccambioestatusdocto.equals("8"))?" CE.fn_monto_anterior, CE.ct_cambio_motivo, ":"")+			
				((!this.iccambioestatusdocto.equals("28"))?" d.cs_dscto_especial, (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social) AS nombreif, ":"")+
				((this.iccambioestatusdocto.equals("2"))?"	DS.IN_IMPORTE_RECIBIR - ((D.FN_MONTO * D.FN_PORC_BENEFICIARIO ) / 100) NETO_RECIBIR, ":"")+			
				((!this.iccambioestatusdocto.equals("28"))?" i2.cg_razon_social AS nombre_beneficiario, d.fn_porc_beneficiario, ":"");

		campos +=(!this.iccambioestatusdocto.equals("8") && !this.iccambioestatusdocto.equals("28")&& !this.iccambioestatusdocto.equals("29"))?" (d.fn_monto * d.fn_porc_beneficiario) / 100 recibir_beneficiario ,m.cd_nombre_ing AS namecurrency ":"";

		campos +=(this.iccambioestatusdocto.equals("8"))?
				"	DECODE (D.ic_estatus_docto, 3, DS.fn_importe_recibir_benef, " +
				"	4, DS.fn_importe_recibir_benef, 11, DS.fn_importe_recibir_benef, " +
				"	12, DS.fn_importe_recibir_benef, 16, DS.fn_importe_recibir_benef,  " +
	            "   ((d.fn_porc_beneficiario * d.fn_monto) / 100 )) AS recibir_beneficiario, "+
				"	TO_CHAR(ce.df_fecha_venc_anterior,'dd/mm/yyyy') as fechaVencAnt, "	+	
				"	m.cd_nombre_ing AS namecurrency ":"";
					
		campos +=(this.iccambioestatusdocto.equals("28")||this.iccambioestatusdocto.equals("29"))?
				" CE.fn_monto_anterior , " +
				" CE.fn_monto_nuevo ," +
				" CE.ct_cambio_motivo, " +
				" 'Normal' AS tipofactoraje ":"";
		
		
		if("S".equals(operaFactConMandato)){
			campos += ",cm.cg_razon_social AS mandante";
			}
		
		campos += " ,tf.cg_nombre as TIPO_FACTORAJE "+
					 " ,'RepCEstatusPymeDEbean.java' as claseBean ";
																		
		condiciones =" WHERE d.ic_epo = e.ic_epo "+
					 "  AND d.ic_moneda = m.ic_moneda "+
					 ((this.iccambioestatusdocto.equals("2")|| this.iccambioestatusdocto.equals("29"))?"  AND d.ic_documento = ds.ic_documento ":"")+
					 (this.iccambioestatusdocto.equals("8")||(this.iccambioestatusdocto.equals("28"))?"  AND d.ic_documento = ds.ic_documento(+) ":"")+
					 "  AND d.ic_documento = ce.ic_documento "+
					 "  AND d.ic_estatus_docto = ed.ic_estatus_docto ";
		condiciones +=(this.iccambioestatusdocto.equals("8")||this.iccambioestatusdocto.equals("26")
					   ||this.iccambioestatusdocto.equals("28")||this.iccambioestatusdocto.equals("29"))?" AND ce.ic_cambio_estatus = "+ this.iccambioestatusdocto :
					   " AND d.ic_estatus_docto = "+ this.iccambioestatusdocto ;
		condiciones +=			   
					 "  AND d.ic_pyme = p.ic_pyme "+
					 ((this.iccambioestatusdocto.equals("2"))?" AND ce.ic_cambio_estatus = 2 ":"")+
					 "  AND d.ic_pyme = ? "+
					 "  AND d.ic_epo = ? "+
					 "  AND ce.dc_fecha_cambio >= TRUNC(SYSDATE) AND  ce.dc_fecha_cambio < TRUNC(SYSDATE) + 1 ";
					 
           
		condiciones +=(!this.iccambioestatusdocto.equals("28"))?"  AND d.ic_if = i.ic_if(+) "+
                  "  AND d.ic_beneficiario = i2.ic_if(+) ":"";
		
		if("S".equals(operaFactConMandato)){
			condiciones +=	" AND d.ic_mandante = cm.ic_mandante(+)" ;
			}	
		
		condiciones += "    AND d.cs_dscto_especial = tf.CC_TIPO_FACTORAJE  ";
					 					 					 	    
		tablas = " FROM com_documento d, "+
				 ((this.iccambioestatusdocto.equals("2")||this.iccambioestatusdocto.equals("8")||this.iccambioestatusdocto.equals("28") ||this.iccambioestatusdocto.equals("29"))?" com_docto_seleccionado ds, ":"")+
				 "      comcat_pyme p, "+
				 "      comhis_cambio_estatus ce, "+
				 " 		comcat_epo e, "+
				 ((!this.iccambioestatusdocto.equals("28"))?" comcat_if i, comcat_if i2, ":"")+
				 "      comcat_estatus_docto ed, "+
				 "      comcat_moneda m ";		
		
		if("S".equals(operaFactConMandato)){
			tablas +=	"	,comcat_mandante cm "; 
		}
		
		tablas += " , comcat_tipo_factoraje tf ";
		
		ordenarPor = (this.iccambioestatusdocto.equals("28"))?" order by ig_numero_docto,ce.DC_FECHA_CAMBIO ":"";
		
      this.qrysentencia = campos  + tablas + condiciones + ordenarPor ;
	}
	
	
	public String getOperaFactConMandato() {
		return this.operaFactConMandato;
	}	
	public String getIccambioestatusdocto(){		
		return	this.iccambioestatusdocto;	
	}
	public String getQrysentencia(){
		return	this.qrysentencia;
	}
		
}//class RepCEstatusPymeDEbean