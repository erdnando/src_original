package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 *
 * @author Andrea Isabel Luna Aguill�n
 */

public class ConsCalculoRebate  implements IQueryGeneratorRegExtJS{
	
	public ConsCalculoRebate() {	}
	
	private List conditions;
	StringBuffer strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsCalculoRebate.class);
	
	private String claveEpo;
	private String claveIf;
	private String moneda;
	private String fechaOperacionMin;
	private String fechaOperacionMax;
	private String fechaVencMin;
	private String fechaVencMax;
	
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		strQuery		= new StringBuffer();
		conditions	= new ArrayList();
		strQuery.append("SELECT m1.cd_nombre as nombreMoneda," +
										"a.montoTotalIntereses as montoTotalIntereses," +
										"a.montototalrebate as montoTotalRebate" +
								" FROM " +
									"(SELECT /*+  index( ifepoprod CP_COMREL_IF_EPO_X_PRODUCTO_PK ) */ " +
											"m.ic_moneda                                   			AS ic_moneda," +
											"sum(dselec.in_importe_interes)                     	AS montoTotalIntereses," +
											"sum((d.fn_monto_dscto*ifepoprod.fg_puntos_rebate/100*solicitud.ig_plazo)/360)" +
																													" AS montoTotalRebate" +
									" FROM " +
										"com_documento d," +
										"comrel_pyme_epo rel_pyme_epo," +
										"comcat_pyme pyme," +
										"comcat_if intermediario," +
										"com_solicitud solicitud," +
										"comcat_moneda m," +
										"com_docto_seleccionado dselec," +
										"comrel_if_epo_x_producto ifepoprod" +
									" WHERE " +
											"d.ic_epo                     = rel_pyme_epo.ic_epo" +
										" AND d.ic_pyme                   = rel_pyme_epo.ic_pyme" +
										" AND d.ic_pyme                   = pyme.ic_pyme" +
										" AND D.IC_ESTATUS_DOCTO          IN (4, 11, 12 )  /*DOCUMENTOS OPERADOS*/" +
										" AND d.ic_if                     = intermediario.ic_if" +
										" AND d.ic_documento              = solicitud.ic_documento" +
										" AND d.ic_moneda                 = m.ic_moneda" +
										" AND d.ic_documento              = dselec.ic_documento" +
										" AND d.ic_if                     = ifepoprod.ic_if" +
										" AND d.ic_epo                    = ifepoprod.ic_epo" +
										" AND ifepoprod.ic_producto_nafin = 1 " +
										" AND ifepoprod.fg_puntos_rebate is not null");
										
		if(this.fechaOperacionMin!=null&&this.fechaOperacionMax!=null&&!"".equals(this.fechaOperacionMin)&&!"".equals(this.fechaOperacionMax)){
			strQuery.append(
										" AND solicitud.df_fecha_solicitud >= to_date(?,'DD/MM/YYYY')" +
										" AND solicitud.df_fecha_solicitud < to_date(?,'DD/MM/YYYY')+1 "			
			);
			conditions.add(this.fechaOperacionMin);
			conditions.add(this.fechaOperacionMax);
		}
		if(this.fechaVencMin!=null&&this.fechaVencMax!=null&&!"".equals(this.fechaVencMin)&&!"".equals(this.fechaVencMax)){
			strQuery.append(
										" AND d.df_fecha_venc >= to_date(?,'DD/MM/YYYY')" +
										" AND d.df_fecha_venc < to_date(?,'DD/MM/YYYY')+1 "			
			);
			conditions.add(this.fechaVencMin);
			conditions.add(this.fechaVencMax);
		}
		if(this.claveEpo!=null&&!"".equals(this.claveEpo)){
					strQuery.append(" AND d.ic_epo = ?");
					conditions.add(this.claveEpo);
		}
		if(this.claveIf!=null&&!"".equals(this.claveIf)){
					strQuery.append(" AND d.ic_if  = ?");
					conditions.add(this.claveIf);
		}
		if(this.moneda!=null&&!"".equals(this.moneda)){
					strQuery.append(" AND d.ic_moneda = ?");
					conditions.add(this.moneda);
		}
		strQuery.append(" GROUP BY m.ic_moneda) a," + 
									"comcat_moneda m1" +
		" WHERE a.ic_moneda = m1.ic_moneda");
		
		log.debug("getAggregateCalculationQuery strQuery = " + strQuery.toString());
		log.debug("getAggregateCalculationQuery conditions = " + conditions);
		return strQuery.toString();
	}
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() {
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		strQuery.append("SELECT /*+  index( ifepoprod CP_COMREL_IF_EPO_X_PRODUCTO_PK ) */ " +
								"d.ic_documento" +
							" FROM com_documento d," +
									"comrel_pyme_epo rel_pyme_epo," +
									"comcat_pyme pyme,"	+
									"comcat_if intermediario,"	+
									"com_solicitud solicitud,"	+
									"comcat_moneda m,"	+
									"com_docto_seleccionado dselec,"	+
									"comrel_if_epo_x_producto ifepoprod"	+
							" WHERE "	+
									"d.ic_epo                     = rel_pyme_epo.ic_epo"	+
								" AND d.ic_pyme                   = rel_pyme_epo.ic_pyme"	+
								" AND d.ic_pyme                   = pyme.ic_pyme"	+
								" AND D.IC_ESTATUS_DOCTO          IN (4, 11, 12 )  /* DOCUMENTOS OPERADOS */"	+
								" AND d.ic_if                     = intermediario.ic_if"	+
								" AND d.ic_documento              = solicitud.ic_documento"	+
								" AND d.ic_moneda                 = m.ic_moneda"	+
								" AND d.ic_documento              = dselec.ic_documento"	+
								" AND d.ic_if                     = ifepoprod.ic_if"	+
								" AND d.ic_epo                    = ifepoprod.ic_epo"	+
								" AND ifepoprod.ic_producto_nafin = 1 "	+
								" AND ifepoprod.fg_puntos_rebate is not null");
		if(this.fechaOperacionMin!=null&&!"".equals(this.fechaOperacionMin)&&this.fechaOperacionMax!=null&&!"".equals(this.fechaOperacionMax)){
			strQuery.append(" AND solicitud.df_fecha_solicitud >= to_date(?,'DD/MM/YYYY')"	+
								" AND solicitud.df_fecha_solicitud < to_date(?,'DD/MM/YYYY')+1");
			conditions.add(this.fechaOperacionMin);
			conditions.add(this.fechaOperacionMax);
		}
		if(this.fechaVencMin!=null&&this.fechaVencMax!=null&&!"".equals(this.fechaVencMin)&&!"".equals(this.fechaVencMax)){
			strQuery.append(
								" AND d.df_fecha_venc >= to_date(?,'DD/MM/YYYY')" +
								" AND d.df_fecha_venc < to_date(?,'DD/MM/YYYY')+1 "			
			);
			conditions.add(this.fechaVencMin);
			conditions.add(this.fechaVencMax);
		}
		if(this.claveEpo!=null&&!"".equals(this.claveEpo)){
			strQuery.append(" AND d.ic_epo = ?");
			conditions.add(this.claveEpo);
		}
		if(this.claveIf!=null&&!"".equals(this.claveIf)){
			strQuery.append(" AND d.ic_if  = ?");
			conditions.add(this.claveIf);
		}
		if(this.moneda!=null&&!"".equals(this.moneda)){
			strQuery.append(" AND d.ic_moneda = ?");
			conditions.add(this.moneda);
		}
		strQuery.append(" ORDER BY solicitud.df_fecha_solicitud");
		log.debug(" getDocumentQuery " + strQuery.toString());
		log.debug(" getDocumentQuery " + conditions.toString());
		return strQuery.toString();
	}
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		strQuery.append("SELECT /*+  index( ifepoprod CP_COMREL_IF_EPO_X_PRODUCTO_PK ) */ " +
								"d.ig_numero_docto                             as numerodocumento," +
								"rel_pyme_epo.cg_pyme_epo_interno              AS numeroProveedor," +
								"pyme.cg_razon_social                          AS nombreProveedor," +
								"intermediario.cg_razon_social                 AS nombreIf," +
								"TO_CHAR(solicitud.df_fecha_solicitud, 'dd/mm/yyyy') AS fechaNotificacion," +
								"to_char(d.df_fecha_venc, 'dd/mm/yyyy')        as fechavencimiento," +
								"d.FN_MONTO_DSCTO                              as montodocumento," +
								"m.cd_nombre                                   as moneda," +
								"solicitud.ig_plazo                            as plazoendias," +
								"dselec.in_tasa_aceptada                       AS tasaOperada," +
								"dselec.in_importe_interes                     AS montoIntereses," +
								"ifepoprod.fg_puntos_rebate                    AS puntosRebate," +
								"(d.FN_MONTO_DSCTO*ifepoprod.fg_puntos_rebate/100*solicitud.ig_plazo)/360 " +
                                                 "AS montoRebate" +
							" FROM " +
								"com_documento d," +
								"comrel_pyme_epo rel_pyme_epo," +
								"comcat_pyme pyme," +
								"comcat_if intermediario," +
								"com_solicitud solicitud," +
								"comcat_moneda m," +
								"com_docto_seleccionado dselec," +
								"comrel_if_epo_x_producto ifepoprod" +
							" WHERE " +
								"d.ic_epo                     = rel_pyme_epo.ic_epo" +
								" AND d.ic_pyme                   = rel_pyme_epo.ic_pyme" +
								" AND d.ic_pyme                   = pyme.ic_pyme" +
								" AND D.IC_ESTATUS_DOCTO          IN (4, 11, 12 )  /*DOCUMENTOS OPERADOS*/" +
								" AND d.ic_if                     = intermediario.ic_if" +
								" AND d.ic_documento              = solicitud.ic_documento" +
								" AND d.ic_moneda                 = m.ic_moneda" +
								" AND d.ic_documento              = dselec.ic_documento" +
								" AND d.ic_if                     = ifepoprod.ic_if" +
								" AND d.ic_epo                    = ifepoprod.ic_epo" +
								" AND ifepoprod.ic_producto_nafin = 1 " +
								" AND ifepoprod.fg_puntos_rebate is not null");
		strQuery.append(" AND ( ");
			for(int i=0;i<pageIds.size();i++){
				List lItem = (ArrayList)pageIds.get(i);
				if(i>0){
					strQuery.append(" OR ");
				}
				strQuery.append(" (d.ic_documento = ?) ");
				conditions.add(new Long(lItem.get(0).toString()));
			}
		strQuery.append(")");
		strQuery.append(" ORDER BY fechaNotificacion");
		//						"AND d.ic_documento in (?,?,?,?,?)" +
		log.debug(" getDocumentSummaryQueryForIds " + conditions.toString());
		log.debug(" getDocumentSummaryQueryForIds " + strQuery.toString());
		return strQuery.toString();
	}
	public String getDocumentQueryFile() {
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		
		log.debug("IF =" + this.claveIf);
		log.debug("Moneda = " + this.moneda);
		log.debug("Fecha Op. Min =" + this.fechaOperacionMin);
		log.debug("Fecha Op. Min =" + this.fechaOperacionMax);
		
		strQuery.append("SELECT /*+  index( ifepoprod CP_COMREL_IF_EPO_X_PRODUCTO_PK ) */ " +
								"d.ig_numero_docto                             as numerodocumento," +
								"rel_pyme_epo.cg_pyme_epo_interno              AS numeroProveedor," +
								"pyme.cg_razon_social                          AS nombreProveedor," +
								"intermediario.cg_razon_social                 AS nombreIf," +
								"TO_CHAR(solicitud.df_fecha_solicitud, 'dd/mm/yyyy') AS fechaNotificacion," +
								"to_char(d.df_fecha_venc, 'dd/mm/yyyy')        as fechavencimiento," +
								"d.FN_MONTO_DSCTO                              as montodocumento," +
								"m.cd_nombre                                   as moneda," +
								"solicitud.ig_plazo                            as plazoendias," +
								"dselec.in_tasa_aceptada                       AS tasaOperada," +
								"dselec.in_importe_interes                     AS montoIntereses," +
								"ifepoprod.fg_puntos_rebate                    AS puntosRebate," +
								"(d.FN_MONTO_DSCTO*ifepoprod.fg_puntos_rebate/100*solicitud.ig_plazo)/360 " +
																							 "AS montoRebate" +
							" FROM " +
								"com_documento d," +
								"comrel_pyme_epo rel_pyme_epo," +
								"comcat_pyme pyme," +
								"comcat_if intermediario," +
								"com_solicitud solicitud," +
								"comcat_moneda m," +
								"com_docto_seleccionado dselec," +
								"comrel_if_epo_x_producto ifepoprod" +
							" WHERE " +
								"d.ic_epo                     = rel_pyme_epo.ic_epo" +
							" AND d.ic_pyme                   = rel_pyme_epo.ic_pyme" +
							" AND d.ic_pyme                   = pyme.ic_pyme" +
							" AND D.IC_ESTATUS_DOCTO          IN (4, 11, 12 )  /*DOCUMENTOS OPERADOS*/" +
							" AND d.ic_if                     = intermediario.ic_if" +
							" AND d.ic_documento              = solicitud.ic_documento" +
							" AND d.ic_moneda                 = m.ic_moneda" +
							" AND d.ic_documento              = dselec.ic_documento" +
							" AND d.ic_if                     = ifepoprod.ic_if" +
							" AND d.ic_epo                    = ifepoprod.ic_epo" +
							" AND ifepoprod.ic_producto_nafin = 1 " +
							" AND ifepoprod.fg_puntos_rebate is not null");
		if(this.fechaOperacionMin!=null && this.fechaOperacionMax!=null && !"".equals(this.fechaOperacionMin) && !"".equals(this.fechaOperacionMax)){
			strQuery.append(
							" AND solicitud.df_fecha_solicitud >= to_date(?,'DD/MM/YYYY')" +
							" AND solicitud.df_fecha_solicitud < to_date(?,'DD/MM/YYYY')+1 ");
			conditions.add(this.fechaOperacionMin);
			conditions.add(this.fechaOperacionMax);
		}
		if(this.fechaVencMin!=null&&this.fechaVencMax!=null&&!"".equals(this.fechaVencMin)&&!"".equals(this.fechaVencMax)){
			strQuery.append(
										" AND d.df_fecha_venc >= to_date(?,'DD/MM/YYYY')" +
										" AND d.df_fecha_venc < to_date(?,'DD/MM/YYYY')+1 "			
			);
			conditions.add(this.fechaVencMin);
			conditions.add(this.fechaVencMax);
		}
		if(this.claveEpo!=null&&!"".equals(this.claveEpo)){
					strQuery.append(" AND d.ic_epo = ?");
					conditions.add(this.claveEpo);
		}
		if(this.claveIf!=null && !"".equals(this.claveIf)){
			strQuery.append(" AND d.ic_if  = ?");
			conditions.add(this.claveIf);
		}
		if(this.moneda!=null && !"".equals(this.moneda)){
			strQuery.append(" AND d.ic_moneda = ?");
			conditions.add(this.moneda);
		}
		strQuery.append(" ORDER BY fechaNotificacion");			
		log.debug("getDocumentQueryFile strQuery = " + strQuery.toString());
		log.debug("getDocumentQueryFile conditions = " + conditions);
		return strQuery.toString();
	}
	/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	 public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		double montoRebateTotalMonedaNacional = 0;
		double montoInteresTotalModedaNacional = 0;
		double montoRebateTotalDolar = 0;
		double montoInteresTotalDolar = 0;
		
		if("CSV".equals(tipo)){
			linea = "N�mero Documento, N�mero Proveedor, Nombre Proveedor, IF, Fecha de Notificaci�n, Fecha de Vencimiento, Monto del Documento, Moneda, Plazo en D�as, Tasa Operada, Monto Intereses, Puntos Rebate, Monto del Rebate\n";
			try{
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
				
				while(rs.next()){
					String numeroDocumento		= (rs.getString("NUMERODOCUMENTO") == null) ? "" : rs.getString("NUMERODOCUMENTO");
					String numeroProveedor		= (rs.getString("NUMEROPROVEEDOR") == null) ? "" : rs.getString("NUMEROPROVEEDOR");
					String nombreProveedor		= (rs.getString("NOMBREPROVEEDOR") == null) ? "" : rs.getString("NOMBREPROVEEDOR");
					String nombreIf				= (rs.getString("NOMBREIF") == null) ? "" : rs.getString("NOMBREIF");
					String fechaNotificacion	= (rs.getString("FECHANOTIFICACION") == null) ? "" : rs.getString("FECHANOTIFICACION");
					String fechaVencimiento		= (rs.getString("FECHAVENCIMIENTO") == null) ? "" : rs.getString("FECHAVENCIMIENTO");
					String montoDocumento		= (rs.getString("MONTODOCUMENTO") == null) ? "" : rs.getString("MONTODOCUMENTO");
					String moneda					= (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					String plazoEnDias			= (rs.getString("PLAZOENDIAS") == 	null) ? "" : rs.getString("PLAZOENDIAS");
					String tasaOperada			= (rs.getString("TASAOPERADA") == null) ? "" : rs.getString("TASAOPERADA");
					double montoInteres			= Double.parseDouble((rs.getString("MONTOINTERESES") == null) ? "" : rs.getString("MONTOINTERESES"));
					String puntosRebate			= (rs.getString("PUNTOSREBATE") == null) ? "" : rs.getString("PUNTOSREBATE");
					double montoRebate			= Double.parseDouble((rs.getString("MONTOREBATE") == null) ? "0" : rs.getString("MONTOREBATE"));
					
					String montoInt = Double.toString(montoInteres);
					String montoReb = Double.toString(montoRebate);
					linea =	numeroDocumento.replace(',',' ') + ", " +
								numeroProveedor.replace(',',' ') + ", " +
								nombreProveedor.replace(',',' ') + ", " +
								nombreIf.replace(',',' ') + ", " +
								fechaNotificacion.replace(',',' ') + ", " +
								fechaVencimiento.replace(',',' ') + ", " +
								'$' + montoDocumento.replace(',',' ') + ", " +
								moneda.replace(',',' ') + ", " +
								plazoEnDias.replace(',',' ') + ", " +
								tasaOperada.replace(',',' ')+ '%' + ", " +
								'$' + montoInt.replace(',',' ') + ", " +
								puntosRebate.replace(',',' ') + '%' + ", " +
								'$' + montoReb.replace(',',' ') + "\n";
					buffer.write(linea);
					if("MONEDA NACIONAL".equals(moneda)){
						montoRebateTotalMonedaNacional += montoRebate;
						montoInteresTotalModedaNacional += montoInteres;
					}else if("DOLAR AMERICANO".equals(moneda)){
						montoRebateTotalDolar += montoRebate;
						montoInteresTotalDolar += montoInteres;
					}
				}
				String montoIntTotMonedaNacional = Double.toString(montoInteresTotalModedaNacional);
				String montoIntRebMonedaNacional = Double.toString(montoRebateTotalMonedaNacional);
				String montoIntTotDolar = Double.toString(montoInteresTotalDolar);
				String montoIntRebDolar = Double.toString(montoRebateTotalDolar);
				montoIntTotMonedaNacional = Comunes.formatoDecimal(montoIntTotMonedaNacional,2);
				montoIntRebMonedaNacional = Comunes.formatoDecimal(montoIntRebMonedaNacional,2);
				montoIntTotDolar = Comunes.formatoDecimal(montoIntTotDolar,2);
				montoIntRebDolar = Comunes.formatoDecimal(montoIntRebDolar,2);
				String espacio = "";
				linea = "TOTALES, " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				"Moneda Nacional" + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				'$' + montoIntTotMonedaNacional.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				'$' + montoIntRebMonedaNacional.replace(',',' ') + "\n";
				buffer.write(linea);
				linea = espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				"Dolar Americano" + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				'$' + montoIntTotDolar.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				'$' + montoIntRebDolar.replace(',',' ') + "\n";
				buffer.write(linea);
				buffer.close();
				
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo, e");
			}finally{
				try{
					rs.close();
				}catch(Exception e){}
			}
		}
		else if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				pdfDoc.setTable(13, 100);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Notificaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto del Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo en D�as","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa Operada","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Intereses","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Puntos Rebate","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto del Rebate","celda01",ComunesPDF.CENTER);
				
				while (rs.next()){
					String numeroDocumento		= (rs.getString("NUMERODOCUMENTO") == null) ? "" : rs.getString("NUMERODOCUMENTO");
					String numeroProveedor		= (rs.getString("NUMEROPROVEEDOR") == null) ? "" : rs.getString("NUMEROPROVEEDOR");
					String nombreProveedor		= (rs.getString("NOMBREPROVEEDOR") == null) ? "" : rs.getString("NOMBREPROVEEDOR");
					String nombreIf				= (rs.getString("NOMBREIF") == null) ? "" : rs.getString("NOMBREIF");
					String fechaNotificacion	= (rs.getString("FECHANOTIFICACION") == null) ? "" : rs.getString("FECHANOTIFICACION");
					String fechaVencimiento		= (rs.getString("FECHAVENCIMIENTO") == null) ? "" : rs.getString("FECHAVENCIMIENTO");
					String montoDocumento		= (rs.getString("MONTODOCUMENTO") == null) ? "" : rs.getString("MONTODOCUMENTO");
					String moneda					= (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					String plazoEnDias			= (rs.getString("PLAZOENDIAS") == 	null) ? "" : rs.getString("PLAZOENDIAS");
					String tasaOperada			= (rs.getString("TASAOPERADA") == null) ? "" : rs.getString("TASAOPERADA");
					double montoInteres			= Double.parseDouble((rs.getString("MONTOINTERESES") == null) ? "" : rs.getString("MONTOINTERESES"));
					String puntosRebate			= (rs.getString("PUNTOSREBATE") == null) ? "0" : rs.getString("PUNTOSREBATE");
					double montoRebate			= Double.parseDouble((rs.getString("MONTOREBATE") == null) ? "0" : rs.getString("MONTOREBATE"));
					
					pdfDoc.setCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numeroProveedor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreProveedor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaNotificacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(montoDocumento,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoEnDias,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(tasaOperada,2) + '%',"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(montoInteres,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(puntosRebate,2) + '%',"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(montoRebate,2),"formas",ComunesPDF.CENTER);

					if("MONEDA NACIONAL".equals(moneda)){
						montoRebateTotalMonedaNacional += montoRebate;
						montoInteresTotalModedaNacional += montoInteres;
					}else if("DOLAR AMERICANO".equals(moneda)){
						montoRebateTotalDolar += montoRebate;
						montoInteresTotalDolar += montoInteres;
					}
				}
				pdfDoc.addTable();
				
				pdfDoc.setTable(13,100);
				pdfDoc.setCell("TOTALES","celda01",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,5);
				pdfDoc.setCell("MONEDA NACIONAL","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoInteresTotalModedaNacional,2),"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoRebateTotalMonedaNacional,2),"formasrep",ComunesPDF.CENTER);
				pdfDoc.addTable();
				
				pdfDoc.setTable(13,100);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,7);
				pdfDoc.setCell("DOLAR AMERICANO","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoInteresTotalDolar,2),"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoRebateTotalDolar,2),"formasrep",ComunesPDF.CENTER);
				pdfDoc.addTable();
				
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		}	
		return nombreArchivo;
	 }
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	 public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		String nombreArchivo = "";
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		double montoRebateTotalMonedaNacional = 0;
		double montoInteresTotalModedaNacional = 0;
		double montoRebateTotalDolar = 0;
		double montoInteresTotalDolar = 0;
		
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				pdfDoc.setTable(13, 100);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Notificaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto del Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo en D�as","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa Operada","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Intereses","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Puntos Rebate","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto del Rebate","celda01",ComunesPDF.CENTER);
				
				while (reg.next()){
					String numeroDocumento		= (reg.getString("NUMERODOCUMENTO") == null) ? "" : reg.getString("NUMERODOCUMENTO");
					String numeroProveedor		= (reg.getString("NUMEROPROVEEDOR") == null) ? "" : reg.getString("NUMEROPROVEEDOR");
					String nombreProveedor		= (reg.getString("NOMBREPROVEEDOR") == null) ? "" : reg.getString("NOMBREPROVEEDOR");
					String nombreIf				= (reg.getString("NOMBREIF") == null) ? "" : reg.getString("NOMBREIF");
					String fechaNotificacion	= (reg.getString("FECHANOTIFICACION") == null) ? "" : reg.getString("FECHANOTIFICACION");
					String fechaVencimiento		= (reg.getString("FECHAVENCIMIENTO") == null) ? "" : reg.getString("FECHAVENCIMIENTO");
					String montoDocumento		= (reg.getString("MONTODOCUMENTO") == null) ? "" : reg.getString("MONTODOCUMENTO");
					String moneda					= (reg.getString("MONEDA") == null) ? "" : reg.getString("MONEDA");
					String plazoEnDias			= (reg.getString("PLAZOENDIAS") == 	null) ? "" : reg.getString("PLAZOENDIAS");
					String tasaOperada			= (reg.getString("TASAOPERADA") == null) ? "" : reg.getString("TASAOPERADA");
					double montoInteres			= Double.parseDouble((reg.getString("MONTOINTERESES") == null) ? "" : reg.getString("MONTOINTERESES"));
					String puntosRebate			= (reg.getString("PUNTOSREBATE") == null) ? "0" : reg.getString("PUNTOSREBATE");
					double montoRebate			= Double.parseDouble((reg.getString("MONTOREBATE") == null) ? "0" : reg.getString("MONTOREBATE"));
					
					pdfDoc.setCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numeroProveedor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreProveedor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaNotificacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(montoDocumento,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoEnDias,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(tasaOperada,2) + '%',"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(montoInteres,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(puntosRebate,2) + '%',"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(montoRebate,2),"formas",ComunesPDF.CENTER);
					if("MONEDA NACIONAL".equals(moneda)){
						montoRebateTotalMonedaNacional += montoRebate;
						montoInteresTotalModedaNacional += montoInteres;
					}else if("DOLAR AMERICANO".equals(moneda)){
						montoRebateTotalDolar += montoRebate;
						montoInteresTotalDolar += montoInteres;
					}
				}
				pdfDoc.addTable();
				
				pdfDoc.setTable(13,100);
				pdfDoc.setCell("TOTALES","celda01",ComunesPDF.CENTER,2);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,5);
				pdfDoc.setCell("MONEDA NACIONAL","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoInteresTotalModedaNacional,2),"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoRebateTotalMonedaNacional,2),"formasrep",ComunesPDF.CENTER);
				pdfDoc.addTable();
				
				pdfDoc.setTable(13,100);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,7);
				pdfDoc.setCell("DOLAR AMERICANO","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoInteresTotalDolar,2),"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoRebateTotalDolar,2),"formasrep",ComunesPDF.CENTER);
				pdfDoc.addTable();
				
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		}	
		return nombreArchivo;
	 }
	 public List getConditions() {
		return conditions;
	 }
	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}
	public String getClaveIf() {
		return claveIf;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setFechaOperacionMin(String fechaOperacionMin) {
		this.fechaOperacionMin = fechaOperacionMin;
	}
	public String getFechaOperacionMin() {
		return fechaOperacionMin;
	}
	public void setFechaOperacionMax(String fechaOperacionMax) {
		this.fechaOperacionMax = fechaOperacionMax;
	}
	public String getFechaOperacionMax() {
		return fechaOperacionMax;
	}
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}
	public String getClaveEpo() {
		return claveEpo;
	}


	public void setFechaVencMin(String fechaVencMin) {
		this.fechaVencMin = fechaVencMin;
	}


	public String getFechaVencMin() {
		return fechaVencMin;
	}


	public void setFechaVencMax(String fechaVencMax) {
		this.fechaVencMax = fechaVencMax;
	}


	public String getFechaVencMax() {
		return fechaVencMax;
	}
}