package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class RepCEstatusNafinExt  implements IQueryGeneratorRegExtJS {
	
	/* Cambios de Estatus:
	 * 	2)	Seleccionada PYME a Negociable  
	 * 	7) Operada Pendiente de pago 
	 * 	8) Modificacion de Importe 
	 *   28) Notas de Credito 
	 *   29) Pogramado a negociable
    *	  32) Programado Pyme a Seleccionado Pyme
	 *   40) Pre Negociable a Negociable
	 **/
	private final static Log log = ServiceLocator.getInstance().getLog(RepCEstatusNafinExt.class);
	
	StringBuffer qrySentencia = new StringBuffer("");
	StringBuffer	condicion  =new StringBuffer(); 		
	private List   conditions;
	private int iNoCambioEstatus;
	
	public RepCEstatusNafinExt() { }
	
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery() { 
		return "";
	}
	
	
	/**
	* Obtiene el query para la CONSULTA
	* @return Cadena con la consulta de SQL
	*/
	public String getDocumentQuery() { 
		return "";
	}

	/**
	* Obtiene el query para obtener la consulta PAGINACION
	* @return Cadena con la consulta de SQL, para obtener la consulta
	*/
	public String getDocumentSummaryQueryForIds(List pageIds) {	
		return "";
	}
	
	/**
	* Obtiene el query para obtener la consulta SIN PAGINACION
	* @return Cadena con la consulta de SQL, para obtener la consulta
	*/
	public String getDocumentQueryFile() {
		
		log.info("getDocumentQueryFile(E) :::...");
		
		qrySentencia 	= new StringBuffer();
		String query 	= "";
		conditions 		= new ArrayList();		//binds
		
		RepCEstatusNafinDEbean consulta = new RepCEstatusNafinDEbean();
		consulta.setNoCambioEstatus(iNoCambioEstatus);
		//conditions.add(String.valueOf(iNoCambioEstatus));
		if(iNoCambioEstatus != 7)
			query = consulta.getQueryPorCambioEstatus();
		else
			query = consulta.getStringQueryPorCambioEstatus7();
		log.info("getDocumentQueryFile :::...  "+query);  // --???
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQueryFile(S)");
		return query;	
	}
	
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.Objeto 
	 * Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos con PAGINADOR*/		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){	
		return "";
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) SIN paginador*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
				
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
				int iNoMoneda = 0;
				double nacional = 0;
				double nacional_desc = 0;
				double nacional_int = 0;
				double nacional_descIF = 0;
				double dolares = 0;
				double dolares_desc = 0;
				double dolares_int = 0;
				double dolares_descIF = 0;
				double dMontoDscto = 0;
				double dImporteInteres = 0;
				double dImporteRecibir = 0;
				double dTasaAceptada = 0;
				double dMontoDocto = 0;
				double dPorcentaje = 0;
				double dblRecurso = 0;
				String sNombrePYME = "";
				String sNombreEPO = "";
				String sNombreIF = "";
				String sNoDocumento = "";
				String sAcuse = "";
				String sNombreMoneda = "";
				String sTipoFactoraje = "";
				String nombreTipoFactoraje = "";
				String sNoSolicitud = "";
				String sNetoRecibir = "";
				String sBeneficiario = "";
				String sPorcBeneficiario = "";
				String sImpRecibirBenef = "";
				boolean dolaresDocto = false;
				boolean nacionalDocto = false;
			  // Declaraci�n e inicializaci�n de variables
				double total_mn = 0.0;
				double total_dolares = 0.0;
				double dblTotMontoDescuentoMN = 0.0;
				double dblTotMontoDescuentoDL = 0.0;
				dMontoDscto = 0;
				double dblTotRecursoMN = 0.0;
				double dblTotRecursoDL = 0.0;
				double df_total_ant_mn = 0.0;
				double df_total_ant_dol = 0.0;
				String MontoAnterior = "";
				String sFechaDocto = "";
				String sFechaVto = "";
				String sCausa = "";
				
//		Seleccionada PYME a Negociable		
				if(iNoCambioEstatus == 2)  {
					pdfDoc.setTable(18, 100); //numero de columnas y el ancho que ocupara la tabla en el documento

				pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
				pdfDoc.setCell("Seleccionada PYME a Negociable","formas",ComunesPDF.LEFT,17);	
				pdfDoc.setCell("","formas",ComunesPDF.LEFT,18);	

					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto int.","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Operar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero Acuse IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Neto a Recibir PYME","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					
					while (rs.next()) 
					{
						sNombrePYME 			= (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreEPO 				= (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						sNombreIF 				= (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sNoDocumento 			= (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sNombreMoneda 			= (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje			= (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje").trim();
						nombreTipoFactoraje  = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto 			= rs.getDouble("montoDocto");
						dPorcentaje 			= rs.getDouble("porcentaje");
						dblRecurso 				= dMontoDocto - dMontoDscto;
						dMontoDscto 			= rs.getDouble("montoDscto");
						dTasaAceptada 			= rs.getDouble("tasaAceptada");
						dImporteInteres 		= rs.getDouble("importeInteres");
						dImporteRecibir 		= rs.getDouble("importeRecibir");
						sAcuse 					= (rs.getString("numeroAcuse")==null)?"":rs.getString("numeroAcuse").trim();
						sNetoRecibir 			= (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?"$ "+Comunes.formatoDecimal(rs.getString("netoRecibir"), 2):"";
						sBeneficiario 			= (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?rs.getString("beneficiario"):"";
						sPorcBeneficiario 	= (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?rs.getString("porcBeneficiario")+" %":"";
						sImpRecibirBenef 		= (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?"$ "+Comunes.formatoDecimal(rs.getString("importeARecibirBeneficiario"), 2):"";
						iNoMoneda 				= rs.getInt("claveMoneda");
	
						if(sTipoFactoraje.equals("V") || sTipoFactoraje.equals("D") || sTipoFactoraje.equals("I")) {
							  dMontoDscto = dMontoDocto;
							  dPorcentaje = 100;
						  }
				
						if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacional_int += dImporteInteres;
							nacional_descIF += dImporteRecibir;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolares_int += dImporteInteres;
							dolares_descIF += dImporteRecibir;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,0)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblRecurso,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(Comunes.formatoDecimal(dTasaAceptada,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteInteres,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteRecibir,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sAcuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNetoRecibir,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(sBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sPorcBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sImpRecibirBenef,"formas",ComunesPDF.RIGHT);
						
					}	//while
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_descIF,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_descIF,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}
					
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
// Operada - Operada Pendiente de pago				
				else if(iNoCambioEstatus == 7)  { 
					pdfDoc.setTable(13, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
					
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Operada - Operada Pendiente de pago","formas",ComunesPDF.LEFT,18);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,19);	

					pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("N�mero Solicitud","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa IF","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto int. IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Operar IF","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Nombre Fideicomiso","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa Fideicomiso","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto int. Fideicomiso","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Operar Fideicomiso","celda01",ComunesPDF.CENTER);					
					
					pdfDoc.setCell("N�mero Acuse IF","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Neto a Recibir PYME","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					
					while (rs.next()) 
					{
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						String sNombreFideicomiso = (rs.getString("nombreFideicomiso")==null)?"":rs.getString("nombreFideicomiso").trim(); 
						sNombrePYME = (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreIF = (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje");
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto = rs.getDouble("montoDocto");
						dPorcentaje = rs.getDouble("porcentaje");
						dblRecurso = dMontoDocto - dMontoDscto;
						dMontoDscto = rs.getDouble("montoDscto");
						
						String dTasaAceptadaF = rs.getString("tasaAceptada");
						String dImporteInteresF = rs.getString("importeInteres");
						String dImporteRecibirF = rs.getString("importeRecibir");
						
						dTasaAceptada = rs.getDouble("tasaAceptadaIf");
						dImporteInteres = rs.getDouble("importeInteresIf");
						dImporteRecibir = rs.getDouble("importeRecibirIf");
								
						
						
						sAcuse = (rs.getString("numeroAcuse")==null)?"":rs.getString("numeroAcuse").trim();
						sNetoRecibir = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ? "$ "+Comunes.formatoDecimal(rs.getString("netoRecibir"), 2):"";
						sBeneficiario = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ? rs.getString("beneficiario"):"";
						sPorcBeneficiario = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ? rs.getString("porcBeneficiario")+" %":"";
						sImpRecibirBenef = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ? "$ "+Comunes.formatoDecimal(rs.getString("importeARecibirBeneficiario"), 2):"";
						sNoSolicitud = (rs.getString("numeroSolicitud")==null)?"":rs.getString("numeroSolicitud").trim();
						iNoMoneda = rs.getInt("claveMoneda");
						// Para factoraje Vencido y Distribuido.
						if(sTipoFactoraje.equals("V") || sTipoFactoraje.equals("D") || sTipoFactoraje.equals("I")) {
							dMontoDscto = dMontoDocto;
							dPorcentaje = 100;
						}
				
						  if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacional_int += dImporteInteres;
							nacional_descIF += dImporteRecibir;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolares_int += dImporteInteres;
							dolares_descIF += dImporteRecibir;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
											
						pdfDoc.setCell(sNoSolicitud,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblRecurso,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);	
						pdfDoc.setCell(Comunes.formatoDecimal(dTasaAceptada,2)+"%" ,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteInteres,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteRecibir,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(sNombreFideicomiso,"formas",ComunesPDF.LEFT);	
						
						if(dTasaAceptadaF.equals("N/A")){
							pdfDoc.setCell(dTasaAceptadaF,"formas",ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dTasaAceptadaF,2),"formas",ComunesPDF.RIGHT);						
						}						
						
						//pdfDoc.setCell(Comunes.formatoDecimal(dTasaAceptadaF,2)+"%" ,"formas",ComunesPDF.CENTER);
						
						if(dImporteInteresF.equals("N/A")){
							pdfDoc.setCell(dImporteInteresF,"formas",ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteInteresF,2),"formas",ComunesPDF.RIGHT);						
						}						
						
						//pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteInteresF,2),"formas",ComunesPDF.RIGHT);
						
						if(dImporteRecibirF.equals("N/A")){
							pdfDoc.setCell(dImporteRecibirF,"formas",ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteRecibirF,2),"formas",ComunesPDF.RIGHT);
						}
						//pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteRecibirF,2) ,"formas",ComunesPDF.RIGHT);						
						
						pdfDoc.setCell(sAcuse,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sNetoRecibir,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sPorcBeneficiario,"formas",ComunesPDF.CENTER);
						//pdfDoc.setCell(Comunes.formatoDecimal(sPorcBeneficiario,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sImpRecibirBenef,"formas",ComunesPDF.RIGHT);
						
					}	//while
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,7);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_descIF,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,7);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_descIF,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}
					
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
// Modificaci�n de Montos y/o Fechas de Vencimiento
				else if(iNoCambioEstatus == 8)  {
					pdfDoc.setTable(19, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
					
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Modificaci�n de Montos y/o Fechas de Vencimiento","formas",ComunesPDF.LEFT,18);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,19);	

					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Documento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Anterior","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Fecha de Vencimiento Anterior","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Acuse","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Causa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					
					sNombreEPO = "";
					sNombrePYME = "";
					sNombreIF = "";
					sNoDocumento = "";
					sAcuse = "";
					sNombreMoneda = "";
					iNoMoneda = 0;
					dMontoDocto = 0;
					dolaresDocto = false;
					nacionalDocto = false;
					dPorcentaje = 0;
					dblRecurso = 0;
					nacional_desc = 0;
					String fechaVencAnt = "";

					while (rs.next()) 
					{
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						sNombrePYME = (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreIF = (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sFechaDocto = (rs.getString("fechaDocumento")==null)?"":rs.getString("fechaDocumento").trim();
						sFechaVto = (rs.getString("fechaVencimiento")==null)?"":rs.getString("fechaVencimiento").trim();
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje");
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto = rs.getDouble("montoDocto");
						MontoAnterior = (rs.getString("montoAnterior")==null)?"":rs.getString("montoAnterior").trim();
						fechaVencAnt = (rs.getString("fechaVencAnt")==null)?"":rs.getString("fechaVencAnt");
						dPorcentaje = rs.getDouble("porcentaje");	// Aforo.
						dblRecurso = dMontoDocto - dMontoDscto;
						dMontoDscto = rs.getDouble("montoDscto");
						sAcuse = (rs.getString("numeroAcuse")==null)?"":rs.getString("numeroAcuse").trim();
						sCausa = (rs.getString("causa")==null)?"":rs.getString("causa").trim();
						sBeneficiario = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?rs.getString("beneficiario"):"";
						sPorcBeneficiario = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?rs.getString("porcBeneficiario")+" %":"";
						sImpRecibirBenef = (sTipoFactoraje.equals("D")) || (sTipoFactoraje.equals("I")) ?"$ "+Comunes.formatoDecimal(rs.getString("importeARecibirBeneficiario"), 2):"";
						iNoMoneda = rs.getInt("claveMoneda");
						// Para factoraje Vencido y Distribuido.
						if(sTipoFactoraje.equals("V") || sTipoFactoraje.equals("D") || sTipoFactoraje.equals("I")) {
							dMontoDscto = dMontoDocto;
							dPorcentaje = 100;
						}

						if (iNoMoneda == 1) {
							total_mn += dMontoDocto;
							dblTotMontoDescuentoMN += dMontoDscto;
							dblTotRecursoMN += dblRecurso;
							nacionalDocto = true;
							df_total_ant_mn += rs.getDouble("MontoAnterior");
						}
						if (iNoMoneda == 54) {
							total_dolares += dMontoDocto;
							dblTotMontoDescuentoDL += dMontoDscto;
							dblTotRecursoDL += dblRecurso;
							dolaresDocto = true;
							df_total_ant_dol += rs.getDouble("MontoAnterior");
						}
						
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sFechaDocto,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sFechaVto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(MontoAnterior,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(fechaVencAnt,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblRecurso,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sAcuse,"formas",ComunesPDF.CENTER);
												
						pdfDoc.setCell(sCausa,"formas",ComunesPDF.CENTER);						
						pdfDoc.setCell(sBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sPorcBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sImpRecibirBenef,"formas",ComunesPDF.RIGHT);
						
					}	//while
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(total_mn,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(df_total_ant_mn,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotRecursoMN,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotMontoDescuentoMN,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(total_dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(df_total_ant_dol,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotRecursoDL,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotMontoDescuentoDL,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}
					
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
// Aplicaci�n de Nota de Cr�dito
				else if(iNoCambioEstatus == 28)  {
					pdfDoc.setTable(10, 100); //numero de columnas y el ancho que ocupara la tabla en el documento

					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Aplicaci�n de Nota de Cr�dito","formas",ComunesPDF.LEFT,9);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,10);	

					pdfDoc.setCell("Nombre de la EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Acuse","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Anterior","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Nuevo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Causa","celda01",ComunesPDF.CENTER);
					
					double dMontoNuevo = 0, dMontoAnterior = 0;
					sNombreEPO = "";
					total_dolares = 0;
					total_mn = 0;
					dMontoDscto = 0;
					double dblTotalMontoPesos = 0;
					double dblTotalMontoDolares = 0;
					dolaresDocto = false;
					nacionalDocto = false;
					while (rs.next()) 
					{
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO");
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto");
						sAcuse = (rs.getString("numeroAcuse")==null)?"":rs.getString("numeroAcuse");
						sFechaDocto = (rs.getString("fechaDocumento")==null)?"":rs.getString("fechaDocumento");
						sFechaVto = (rs.getString("fechaVencimiento")==null)?"":rs.getString("fechaVencimiento");
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje");
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda");
						dMontoAnterior = rs.getDouble("montoAnterior");
						dMontoNuevo = rs.getDouble("montoNuevo");
						sCausa = (rs.getString("causa")==null)?"":rs.getString("causa");
						iNoMoneda = rs.getInt("claveMoneda");
				
						if (iNoMoneda == 1) {
							df_total_ant_mn += dMontoAnterior;
							total_mn += dMontoNuevo;
							dblTotalMontoPesos += dMontoDscto;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							df_total_ant_dol += dMontoAnterior;
							total_dolares += dMontoNuevo;
							dblTotalMontoDolares += dMontoDscto;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sAcuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sFechaDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sFechaVto,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoAnterior,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoNuevo,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sCausa,"formas",ComunesPDF.CENTER);
						
					}	//while
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,7);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(df_total_ant_mn,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(total_mn,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,7);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(df_total_ant_dol,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(total_dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
//Programado a Negociable				
				else if(iNoCambioEstatus == 29)  {
					pdfDoc.setTable(13, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
	
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Programado a Negociable","formas",ComunesPDF.LEFT,12);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,13);	

					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero Acuse IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Causa","celda01",ComunesPDF.CENTER);
					
					while (rs.next()) 
					{
						sNombrePYME = (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						sNombreIF = (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sFechaDocto = (rs.getString("fechaDocumento")==null)?"":rs.getString("fechaDocumento");
						sFechaVto = (rs.getString("fechaVencimiento")==null)?"":rs.getString("fechaVencimiento");
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje").trim();
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto = rs.getDouble("montoDocto");
						dPorcentaje = rs.getDouble("porcentaje");
						dMontoDscto = rs.getDouble("montoDscto");
						sAcuse = (rs.getString("numeroAcuse")==null)?"":rs.getString("numeroAcuse").trim();
						sCausa = (rs.getString("causa")==null)?"":rs.getString("causa");
						iNoMoneda = rs.getInt("claveMoneda");
						
						// Para Factoraje Vencido y Distribuido.
						  if(sTipoFactoraje.equals("V") || sTipoFactoraje.equals("D") || sTipoFactoraje.equals("I")) {
							  dMontoDscto = dMontoDocto;
							  dPorcentaje = 100;
						  }
				
						if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sFechaDocto,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sFechaVto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,0)+"%" ,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sAcuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sCausa,"formas",ComunesPDF.CENTER);
						
					}	//while
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
					}
					
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
//Programado Pyme a Seleccionado Pyme				
				else if(iNoCambioEstatus == 32)  {
					pdfDoc.setTable(16, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
	
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Programado Pyme a Seleccionado Pyme","formas",ComunesPDF.LEFT,15);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,16);	

					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto int.","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Neto a Recibir PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					
					while (rs.next()) 
					{
						sNombrePYME = (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						sNombreIF = (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje");
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto = rs.getDouble("montoDocto");
						dPorcentaje = rs.getDouble("porcentaje");
						dblRecurso = dMontoDocto - dMontoDscto;
						dMontoDscto = rs.getDouble("montoDscto");
						dTasaAceptada = rs.getDouble("tasaAceptada");
						dImporteInteres = rs.getDouble("importeInteres");
						dImporteRecibir = rs.getDouble("IMPORTERECIBIR");
						sNetoRecibir = (sTipoFactoraje.equals("D"))  ? "$ "+Comunes.formatoDecimal(rs.getString("netoRecibir"), 2):"";
						sBeneficiario = (sTipoFactoraje.equals("D"))  ? rs.getString("beneficiario"):"";
						sPorcBeneficiario = (sTipoFactoraje.equals("D"))  ? rs.getString("porcBeneficiario")+" %":"";
						sImpRecibirBenef = (sTipoFactoraje.equals("D"))  ? "$ "+Comunes.formatoDecimal(rs.getString("importeARecibirBeneficiario"), 2):"";
						iNoMoneda = rs.getInt("claveMoneda");


					   if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacional_int += dImporteInteres;
							nacional_descIF += dImporteRecibir;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolares_int += dImporteInteres;
							dolares_descIF += dImporteRecibir;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblRecurso,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(Comunes.formatoDecimal(dTasaAceptada,2)+"%" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dImporteInteres,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sNetoRecibir,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sPorcBeneficiario,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sImpRecibirBenef,"formas",ComunesPDF.RIGHT);
						
					}	//while
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
					}
					
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
// Pre Negociable a Negociable				
				else if(iNoCambioEstatus == 40)  {
					pdfDoc.setTable(14, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT,1);	
					pdfDoc.setCell("Pre Negociable a Negociable","formas",ComunesPDF.LEFT,13);	
					pdfDoc.setCell("","formas",ComunesPDF.LEFT,14);	

					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					
					while (rs.next()) 
					{
						sNombrePYME = (rs.getString("nombrePYME")==null)?"":rs.getString("nombrePYME").trim();
						sNombreEPO = (rs.getString("nombreEPO")==null)?"":rs.getString("nombreEPO").trim();
						//sNombreIF = (rs.getString("nombreIF")==null)?"":rs.getString("nombreIF").trim();
						sFechaDocto = (rs.getString("fechaDocumento")==null)?"":rs.getString("fechaDocumento");
						sFechaVto = (rs.getString("fechaVencimiento")==null)?"":rs.getString("fechaVencimiento");
						
						sNoDocumento = (rs.getString("numeroDocto")==null)?"":rs.getString("numeroDocto").trim();
						sNombreMoneda = (rs.getString("nombreMoneda")==null)?"":rs.getString("nombreMoneda").trim();
						sTipoFactoraje = (rs.getString("tipoFactoraje")==null)?"":rs.getString("tipoFactoraje");
						nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE").trim();
						dMontoDocto = rs.getDouble("montoDocto");
						dPorcentaje = rs.getDouble("porcentaje");
						dblRecurso = dMontoDocto - dMontoDscto;
						dMontoDscto = rs.getDouble("montoDscto");
						/*dTasaAceptada = rs.getDouble("tasaAceptada");
						dImporteInteres = rs.getDouble("importeInteres");
						dImporteRecibir = rs.getDouble("importeRecibir");
						*/
						sNetoRecibir = (sTipoFactoraje.equals("D"))  ? "$ "+Comunes.formatoDecimal(rs.getString("netoRecibir"), 2):"";
						sBeneficiario = (sTipoFactoraje.equals("D"))  ? rs.getString("beneficiario"):"";
						sPorcBeneficiario = (sTipoFactoraje.equals("D"))  ? rs.getString("porcBeneficiario")+" %":"";
						sImpRecibirBenef = (sTipoFactoraje.equals("D"))  ? "$ "+Comunes.formatoDecimal(rs.getString("importeARecibirBeneficiario"), 2):"";
						iNoMoneda = rs.getInt("claveMoneda");
				
				
					   if (iNoMoneda == 1) {
							nacional += dMontoDocto;
							nacional_desc += dMontoDscto;
							nacional_int += dImporteInteres;
							nacional_descIF += dImporteRecibir;
							nacionalDocto = true;
						}
						if (iNoMoneda == 54) {
							dolares += dMontoDocto;
							dolares_desc += dMontoDscto;
							dolares_int += dImporteInteres;
							dolares_descIF += dImporteRecibir;
							dolaresDocto = true;
						}
						
						pdfDoc.setCell(sNombrePYME,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreEPO,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sNombreIF,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(sFechaDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sFechaVto,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(sNoDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sNombreMoneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDocto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(dPorcentaje,2)+"%" ,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dMontoDscto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(sBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sPorcBeneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sImpRecibirBenef,"formas",ComunesPDF.RIGHT);
						
					}	//while
					if(nacionalDocto) {
						pdfDoc.setCell("Total MN","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(nacional_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,3);
					}
					if (dolaresDocto) {	
						pdfDoc.setCell("Total USD","formas",ComunesPDF.LEFT,8);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dolares_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,3);
					}
					
					pdfDoc.addTable();
					pdfDoc.endDocument();
				}
				
				
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		} // if (tipo)	
		return  nombreArchivo;
	}
	
	
	/******************** SETTER'S ********************/
	 
	public void setNoCambioEstatus (int iNoCambioEstatus ) { this.iNoCambioEstatus = iNoCambioEstatus; }
	
	/******************** GETTER'S ********************/	
	
	public List getConditions() {  return conditions;  }    
	
}  