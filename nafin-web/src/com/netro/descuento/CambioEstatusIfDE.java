package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

public class CambioEstatusIfDE implements IQueryGenerator, IQueryGeneratorRegExtJS{

	public CambioEstatusIfDE(){}

	public String getAggregateCalculationQuery(HttpServletRequest request)
	{
    String ses_ic_if = (String)request.getSession().getAttribute("iNoCliente");
    StringBuffer qrySentencia = new StringBuffer();
	 StringBuffer qryCondicion = new StringBuffer();
    String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
    String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
    String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
    String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null)?"":request.getParameter("dc_fecha_cambioMin");
    String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null)?"":request.getParameter("dc_fecha_cambioMax");
    String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
    String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
		String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus") == null)?"":request.getParameter("ic_cambio_estatus");
		String hint = "";
	    try{
			//hint pedeterminado
			hint = " /*+ use_nl(ce d ds pe m e) index(ce IN_COMHIS_CAM_ESTA_02_NUK) */ ";
			
			if (!ic_epo.equals("")) {
				qryCondicion.append(" and d.ic_epo = "+ic_epo);
			}
			if (!ic_pyme.equals("")) {
				qryCondicion.append(" and d.ic_pyme = "+ic_pyme);
			}
			if (!ic_moneda.equals("")) {
				qryCondicion.append(" and d.ic_moneda = "+ic_moneda);
			}
			if(!dc_fecha_cambioMin.equals("")) {
				hint = " /*+ use_nl(ce d ds pe m e) index(ce CP_COMHIS_CAMBIO_ESTATUS_PK) */ ";
				if(!dc_fecha_cambioMax.equals("")) {
					qryCondicion.append(" and ce.dc_fecha_cambio >= TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE('"+dc_fecha_cambioMax+"','dd/mm/yyyy')+1 ");
				} else {
					qryCondicion.append(" and ce.dc_fecha_cambio >= TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy')+1 ");
				}
			}
			if(!fn_montoMin.equals("")) {
				if(!fn_montoMax.equals("")) {
					qryCondicion.append(" and ds.in_importe_recibir between "+fn_montoMin+" and "+fn_montoMax);
				} else {
					qryCondicion.append(" and ds.in_importe_recibir = "+fn_montoMin);
				}
			}
			if (!ic_cambio_estatus.equals("")) {
				if (ic_cambio_estatus.equals("23"))
					qryCondicion.append(" and ce.ic_cambio_estatus in (23, 41)");
				else
					qryCondicion.append(" and ce.ic_cambio_estatus = "+ic_cambio_estatus);
			} else {
				qryCondicion.append(" and ce.ic_cambio_estatus in (2, 23, 32, 34, 41)");
			}
			
			if("".equals(ic_epo) && "".equals(ic_pyme) && "".equals(ic_moneda) && 
					"".equals(dc_fecha_cambioMin) && 
					"".equals(fn_montoMin) && 
					"".equals(ic_cambio_estatus)) {
				hint = " /*+ use_nl(ce d ds pe m e) index(ce CP_COMHIS_CAMBIO_ESTATUS_PK) */ ";
				qryCondicion.append(" and ce.dc_fecha_cambio >= TRUNC(sysdate) and ce.dc_fecha_cambio < TRUNC(sysdate)+1 ");
			}


			qrySentencia.append(
					" SELECT " + hint + "count(1) as CambioEstatusIfDE"+
					" ,SUM(D.fn_monto) "+
					" ,SUM(D.fn_monto_dscto)"+
					//" ,SUM(DS.in_importe_recibir)"+ //Linea a modificar por FODEA 17
					" ,SUM(decode(ds.cs_opera_fiso, 'S', nvl(ds.in_importe_recibir_fondeo,0), 'N', DS.in_importe_recibir))"+ //FODEA 17
					" ,D.ic_moneda"+
					" ,M.cd_nombre"+
					" FROM "+
					" com_docto_seleccionado ds, com_documento d, "+
					" comcat_moneda m, "+
					" comhis_cambio_estatus ce, "+
					" comcat_epo e, comrel_producto_epo pe " +
					" WHERE"+
					" ds.ic_documento = d.ic_documento"+
					" AND d.cs_dscto_especial != 'C' "+
					" AND d.ic_moneda = m.ic_moneda"+
					" AND d.ic_documento = ce.ic_documento"+
					" AND d.ic_epo = e.ic_epo " +
					" AND d.ic_epo = pe.ic_epo " +
					" AND pe.ic_producto_nafin = 1 " +
					//" and (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
					" AND e.cs_habilitado = 'S' " +
					" AND d.ic_if = "+ses_ic_if+" " +
					qryCondicion);
			
			qrySentencia.append(" GROUP BY D.ic_moneda, M.cd_nombre");
			qrySentencia.append(" ORDER BY D.ic_moneda");
			
			System.out.println("Query de getAggregateCalculationQuery:\n"+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("CambioEstatusIfDE::getDocumentQueryFileException "+e);
		}
	return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
	
		AccesoDB con = new AccesoDB();
		String fechaSigDol = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		try {
			con.conexionDB();
			String qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			ps.executeUpdate();
			ps.close();
			qrySentencia = 
				" SELECT TO_CHAR(sigfechahabilxepo (0, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha"   +
				"   FROM dual"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ResultSet rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
			}
			rs.close();ps.close();
		} catch (Exception e) {
			System.out.println("Exception: "+e);
		} finally {
			con.terminaTransaccion(true);
			if (con.hayConexionAbierta() == true)
				con.cierraConexionDB();
		}
		
		int i=0;
		StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append(
				" select /*+ use_nl(ds d m e p cce ce pe) */ "+
				" (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO "+
				//" , (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+	//Linea a modificar por FODEA 17
				" , (decode (ds.cs_opera_fiso, 'S', i.cg_razon_social, 'N', (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social))) as nombrePYME"+ //FODEA 17
				" , d.ic_moneda, m.cd_nombre"+
				//" , ds.in_importe_recibir"+	//Linea a modificar por FODEA 17
				" , decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir) as in_importe_recibir"+ //FODEA 17
				" , decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME"+ //Agregado por FODEA 17
				" , cce.cd_descripcion as CambioEstatus"+
				" , TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') as dc_fecha_cambio"+
				" , ce.ct_cambio_motivo, d.ig_numero_docto,"+
				//" ds.in_tasa_aceptada"+	//Linea a modificar por FODEA 17
				" decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada) as in_tasa_aceptada"+ //FODEA 17
//				" , trunc(d.df_fecha_venc-ds.df_fecha_seleccion) as plazo"+
				" , TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaSigDol+"', 'dd/mm/yyyy')) as plazo " +
				" , d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto "+
				" , TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto"+
				" , TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') df_fecha_venc"+
				" ,'CambioEstatusIfDE::getDocumentSummaryQueryForIds'"+
				" from "+
				" com_docto_seleccionado ds, comcat_if i, com_documento d"+
				" , comcat_moneda m, comcat_epo e, comcat_pyme p, comcat_cambio_estatus cce"+
				" , comhis_cambio_estatus ce, comrel_pyme_epo pe"+
				" where"+
				" ds.ic_documento = d.ic_documento"+
				" and ds.ic_if = i.ic_if"+ //FODEA 17
				" AND d.cs_dscto_especial != 'C' "+
				" and d.ic_pyme = p.ic_pyme"+
				" and d.ic_epo = e.ic_epo"+
				" and d.ic_moneda = m.ic_moneda"+
				" and d.ic_documento = ce.ic_documento"+
				" and ce.ic_cambio_estatus = cce.ic_cambio_estatus"+
				" and d.ic_pyme = pe.ic_pyme "+
        " and d.ic_epo = pe.ic_epo"+
        " and ce.ic_cambio_estatus in (2, 23,32, 34,41)"+
				" and ( ");
		String[] arrPK = null;
		Iterator it = ids.iterator();
		while (it.hasNext()) {
			arrPK = it.next().toString().split("\\|");
			qrySentencia.append(" DS.ic_documento = " + arrPK[0] + " AND CE.dc_fecha_cambio = TO_DATE('" + arrPK[1] + "','ddmmyyyyhh24miss') ");
		  if(it.hasNext()) {
				qrySentencia.append(" OR ");
			}			
		}
		
		qrySentencia.append(")");
		qrySentencia.append(" order by d.ic_moneda, ce.ic_cambio_estatus, d.ic_documento");
    
		System.out.println("el query queda de la siguiente manera:\n"+qrySentencia.toString());
	return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request){
	
	String ses_ic_if = (String)request.getSession().getAttribute("iNoCliente");
	StringBuffer qrySentencia = new StringBuffer();
	StringBuffer qryCondicion = new StringBuffer();
	String hint = "";
	String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
	String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
	String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null)?"":request.getParameter("dc_fecha_cambioMin");
	String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null)?"":request.getParameter("dc_fecha_cambioMax");
	String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
	String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
	String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus") == null)?"":request.getParameter("ic_cambio_estatus");
	hint = " /*+ use_nl(ce d ds pe m e) index(ce IN_COMHIS_CAM_ESTA_02_NUK) */ ";
		try{

	
			if (!ic_epo.equals("")) {
				qryCondicion.append(" and d.ic_epo = "+ic_epo);
			}
			if (!ic_pyme.equals("")) {
				qryCondicion.append(" and d.ic_pyme = "+ic_pyme);
			}
			if (!ic_moneda.equals("")) {
				qryCondicion.append(" and d.ic_moneda = "+ic_moneda);
			}
			if(!dc_fecha_cambioMin.equals("")) {
				hint = " /*+ use_nl(ce d ds pe m e) index(ce CP_COMHIS_CAMBIO_ESTATUS_PK) */ ";
				if(!dc_fecha_cambioMax.equals("")) {
					qryCondicion.append(" and ce.dc_fecha_cambio >= TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE('"+dc_fecha_cambioMax+"','dd/mm/yyyy')+1 ");
				} else {
					qryCondicion.append(" and ce.dc_fecha_cambio >= TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy')+1 ");
				}
			}
			if(!fn_montoMin.equals("")) {
				if(!fn_montoMax.equals("")) {
					qryCondicion.append(" and ds.in_importe_recibir between "+fn_montoMin+" and "+fn_montoMax);
				} else {
					qryCondicion.append(" and ds.in_importe_recibir = "+fn_montoMin);
				}
			}
			if (!ic_cambio_estatus.equals("")) {
					if (ic_cambio_estatus.equals("23"))
						qryCondicion.append(" and ce.ic_cambio_estatus in (23,41)");
					else
						qryCondicion.append(" and ce.ic_cambio_estatus = "+ic_cambio_estatus);
			} else {
				qryCondicion.append(" and ce.ic_cambio_estatus in (2, 23,32, 34,41)");
			}
			if("".equals(ic_epo) && "".equals(ic_pyme) && "".equals(ic_moneda) && "".equals(dc_fecha_cambioMin) &&
					"".equals(fn_montoMin) && 
					"".equals(ic_cambio_estatus)) {
				hint = " /*+ use_nl(ce d ds pe m e) index(ce CP_COMHIS_CAMBIO_ESTATUS_PK) */ ";
				qryCondicion.append(" and ce.dc_fecha_cambio >= TRUNC(sysdate) AND ce.dc_fecha_cambio < TRUNC(sysdate) + 1 ");
			}


			qrySentencia.append(
					" SELECT " + hint + " ds.ic_documento||'|'||to_char(ce.dc_fecha_cambio,'ddmmyyyyhh24miss')"+
					" ,'CambioEstatusIfDE::getDocumentQuery'"+
					" FROM "+
					" com_docto_seleccionado ds, "+
					" com_documento d, "+
					" comhis_cambio_estatus ce, "+
					" comcat_epo e, comrel_producto_epo pe "+
					" WHERE "+
					" ds.ic_documento=d.ic_documento"+
					" AND d.cs_dscto_especial != 'C' "+
					" AND d.ic_documento = ce.ic_documento"+
					" AND d.ic_epo = e.ic_epo " +
					" AND d.ic_epo = pe.ic_epo " +
					" AND pe.ic_producto_nafin = 1 " +
					//" AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) " +
					" AND e.cs_habilitado = 'S' " +
					" AND d.ic_if="+ses_ic_if+" " +
					qryCondicion);			
			System.out.println("Query de getDocumentQuery:\n"+qrySentencia.toString());
	    }catch(Exception e){
	      System.out.println("CambioEstatusIfDE::getDocumentQueryException "+e);
	    }
	return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {
		
		AccesoDB con = new AccesoDB();
		String fechaSigDol = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		try {
			con.conexionDB();
			String qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			ps.executeUpdate();
			ps.close();
			qrySentencia = 
				" SELECT TO_CHAR(sigfechahabilxepo (0, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha"   +
				"   FROM dual"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ResultSet rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
			}
			rs.close();ps.close();
		} catch (Exception e) {
			System.out.println("Exception: "+e);
		} finally {
			con.terminaTransaccion(true);
			if (con.hayConexionAbierta() == true)
				con.cierraConexionDB();
		}
		
	String ses_ic_if = (String)request.getSession().getAttribute("iNoCliente");
	StringBuffer qrySentencia = new StringBuffer();
	StringBuffer qryCondicion = new StringBuffer();
	String hint = "";

	String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
	String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
	String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null)?"":request.getParameter("dc_fecha_cambioMin");
	String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null)?"":request.getParameter("dc_fecha_cambioMax");
	String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
	String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
	String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus") == null)?"":request.getParameter("ic_cambio_estatus");
		//Hint predeterminado
		hint =  " /*+ use_nl(ds d m e p cce ce pe prodepo) index(cce CP_COMCAT_CAMBIO_ESTATUS_PK) index(ce IN_COMHIS_CAM_ESTA_02_NUK)*/ ";
		try{

			if (!ic_epo.equals("")) {
				qryCondicion.append(" and d.ic_epo = "+ic_epo);
			}
			if (!ic_pyme.equals("")) {
				qryCondicion.append(" and d.ic_pyme = "+ic_pyme);
			}
			if (!ic_moneda.equals("")) {
				qryCondicion.append(" and d.ic_moneda = "+ic_moneda);
			}
			if(!dc_fecha_cambioMin.equals("")) {
				hint = " /*+ use_nl(ce d ds pe m e) index(ce CP_COMHIS_CAMBIO_ESTATUS_PK) */ ";
				if(!dc_fecha_cambioMax.equals("")) {
					qryCondicion.append(" and ce.dc_fecha_cambio >= TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE('"+dc_fecha_cambioMax+"','dd/mm/yyyy')+1 ");
				} else {
					qryCondicion.append(" and ce.dc_fecha_cambio >= TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy')+1 ");
				}
			}
			if(!fn_montoMin.equals("")) {
				if(!fn_montoMax.equals("")) {
					qryCondicion.append(" and ds.in_importe_recibir between "+fn_montoMin+" and "+fn_montoMax);
				} else {
					qryCondicion.append(" and ds.in_importe_recibir = "+fn_montoMin);
				}
			}
			if (!ic_cambio_estatus.equals("")) {
					if (ic_cambio_estatus.equals("23"))
						qryCondicion.append(" and ce.ic_cambio_estatus in (23,41)");
					else
						qryCondicion.append(" and ce.ic_cambio_estatus = "+ic_cambio_estatus);		
			} else {
				qryCondicion.append(" and ce.ic_cambio_estatus in (2, 23,32, 34,41)");
			}
			
			if("".equals(ic_epo) && "".equals(ic_pyme) && "".equals(ic_moneda) && 
					"".equals(dc_fecha_cambioMin) && 
					"".equals(fn_montoMin) && 
					"".equals(ic_cambio_estatus)) {
				hint = " /*+ use_nl(ce d ds pe m e) index(ce CP_COMHIS_CAMBIO_ESTATUS_PK) */ ";
				qryCondicion.append(" AND ce.dc_fecha_cambio >= TRUNC(sysdate) AND ce.dc_fecha_cambio < TRUNC(sysdate)+1 ");
			}

			qrySentencia.append(
					" select " + hint + 
					" (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO "+
					//" , (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+ Linea a modificar por FODEA 17
					" , (decode (ds.cs_opera_fiso, 'S', i.cg_razon_social, 'N', (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social))) as nombrePYME"+ //FODEA 17
					" , d.ic_moneda, m.cd_nombre"+
					//" , ds.in_importe_recibir"+	//Linea a modificar por FODEA 17
					" , decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir) as in_importe_recibir"+ //FODEA 17
					" , decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME"+ //Agregado por FODEA 17
					" , cce.cd_descripcion as CambioEstatus"+
					" , TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') as dc_fecha_cambio"+
					" , ce.ct_cambio_motivo, d.ig_numero_docto,"+
					//" ds.in_tasa_aceptada"+ //Linea a modificar por FODEA 17
					" decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada) as in_tasa_aceptada"+	//FODEA 17
//					" , trunc(d.df_fecha_venc-ds.df_fecha_seleccion) as plazo"+
					" , TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaSigDol+"', 'dd/mm/yyyy')) as plazo " +
					" , d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto "+
					" , TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto"+
					" , TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') df_fecha_venc"+
					" ,'CambioEstatusIfDE::getDocumentQueryFile'"+
					" from "+
					" com_docto_seleccionado ds, comcat_if i, com_documento d"+
					" , comcat_moneda m, comcat_epo e, comcat_pyme p, comcat_cambio_estatus cce"+
					" , comhis_cambio_estatus ce, comrel_pyme_epo pe, comrel_producto_epo prodepo "+
					" where"+
					" ds.ic_documento = d.ic_documento"+
					" and ds.ic_if = i.ic_if"+ //FODEA 17
					" AND d.cs_dscto_especial != 'C' "+
					" and d.ic_pyme = p.ic_pyme"+
					" and d.ic_epo = e.ic_epo"+
					" and d.ic_epo = prodepo.ic_epo " +
					" and prodepo.ic_producto_nafin = 1 " +
					//" and (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) " +
					" and e.cs_habilitado = 'S' " +
					" and d.ic_moneda = m.ic_moneda"+
					" and d.ic_documento = ce.ic_documento"+
					" and ce.ic_cambio_estatus = cce.ic_cambio_estatus"+
					" and d.ic_if ="+ses_ic_if+        
					" and d.ic_pyme = pe.ic_pyme "+
					" and d.ic_epo = pe.ic_epo" +
					qryCondicion);

			System.out.println("Query de getDocumentQueryFile:\n"+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("CambioEstatusIfDE::getDocumentQueryFileException "+e);
		}
	return qrySentencia.toString();
	}

/*******************************************************************************
 *          Migraci�n. Implementa IQueryGeneratorRegExtJS.java                 *
 *******************************************************************************/
	private static final Log log = ServiceLocator.getInstance().getLog(CambioEstatusIfDE.class);
	private List conditions;
	private String ic_epo;
	private String ic_pyme;
	private String ic_moneda;
	private String dc_fecha_cambioMin;
	private String dc_fecha_cambioMax;
	private String fn_montoMin;
	private String fn_montoMax;
	private String ic_cambio_estatus;
	private String ic_if;

	public String getDocumentQuery() {
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	public String getAggregateCalculationQuery() {
		return null;
	}

/**
 * Se forma la consulta para generar los archivos pdf y csv.
 * Previamente obtiene la la siguiente fecha habil por epo.
 * @return qrySentencia
 */
	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryCondicion = new StringBuffer();
		String hint = "";
		conditions = new ArrayList();

		AccesoDB con = new AccesoDB();
		String fechaSigDol = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());

		try{
			con.conexionDB();
			String sentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			PreparedStatement ps = con.queryPrecompilado(sentencia);
			ps.executeUpdate();
			ps.close();
			sentencia = " SELECT TO_CHAR(sigfechahabilxepo (0, TRUNC(SYSDATE), 1, '+'),'dd/mm/yyyy') fecha FROM dual";
			ps = con.queryPrecompilado(sentencia);
			ResultSet rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				fechaSigDol = rs.getString(1)==null?fechaSigDol:rs.getString(1);
			}
		} catch(Throwable e) {
			log.warn("Error al obtener la siguiente fecha habil por epo. " + e);
			throw new AppException("getDocumentQueryFile.Exception. ", e);
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta() == true)
				con.cierraConexionDB();
		}

		hint = " /*+ use_nl(ds d m e p cce ce pe prodepo) index(cce CP_COMCAT_CAMBIO_ESTATUS_PK) index(ce IN_COMHIS_CAM_ESTA_02_NUK)*/ ";
		//Las siguientes dos variables siempre estar�n presentes en la consulta
		conditions.add(fechaSigDol);
		conditions.add(getIc_if());

		if (!getIc_epo().equals("")) {
			qryCondicion.append(" and d.ic_epo = ? ");
			conditions.add(getIc_epo());
		}
		if (!getIc_pyme().equals("")) {
			qryCondicion.append(" and d.ic_pyme = ? ");
			conditions.add(getIc_pyme());
		}
		if (!getIc_moneda().equals("")) {
			qryCondicion.append(" and d.ic_moneda = ? ");
			conditions.add(getIc_moneda());
		}
		if(!getDc_fecha_cambioMin().equals("")) {
			hint = " /*+ use_nl(ce d ds pe m e) index(ce CP_COMHIS_CAMBIO_ESTATUS_PK) */ ";
			if(!getDc_fecha_cambioMax().equals("")) {
				qryCondicion.append(" and ce.dc_fecha_cambio >= TO_DATE(?,'dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE(?,'dd/mm/yyyy')+1 ");
				conditions.add(getDc_fecha_cambioMin());
				conditions.add(getDc_fecha_cambioMax());
			} else {
				qryCondicion.append(" and ce.dc_fecha_cambio >= TO_DATE(?,'dd/mm/yyyy') and ce.dc_fecha_cambio < TO_DATE(?,'dd/mm/yyyy')+1 ");
				conditions.add(getDc_fecha_cambioMin());
				conditions.add(getDc_fecha_cambioMin());
			}
		}
		if(!getFn_montoMin().equals("")) {
			if(!getFn_montoMax().equals("")) {
				qryCondicion.append(" and ds.in_importe_recibir between ? and ? ");
				conditions.add(getFn_montoMin());
				conditions.add(getFn_montoMax());
			} else {
				qryCondicion.append(" and ds.in_importe_recibir = ? ");
				conditions.add(getFn_montoMin());
			}
		}
		if (!getIc_cambio_estatus().equals("")) {
			if (getIc_cambio_estatus().equals("23")){
				qryCondicion.append(" and ce.ic_cambio_estatus in (23,41)");
			} else{
				qryCondicion.append(" and ce.ic_cambio_estatus = ? ");
				conditions.add(getIc_cambio_estatus());
			}
		} else {
			qryCondicion.append(" and ce.ic_cambio_estatus in (2, 23,32, 34,41)");
		}
		if("".equals(getIc_epo()) && "".equals(getIc_pyme()) && "".equals(getIc_moneda()) && 
			"".equals(getDc_fecha_cambioMin()) && 
			"".equals(getFn_montoMin()) && 
			"".equals(getIc_cambio_estatus())) {
			hint = " /*+ use_nl(ce d ds pe m e) index(ce CP_COMHIS_CAMBIO_ESTATUS_PK) */ ";
			qryCondicion.append(" AND ce.dc_fecha_cambio >= TRUNC(sysdate) AND ce.dc_fecha_cambio < TRUNC(sysdate)+1 ");
		}

		qrySentencia.append(" select " + hint);
		qrySentencia.append(" (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO ");
		qrySentencia.append(" ,(decode (ds.cs_opera_fiso, 'S', i.cg_razon_social, 'N', (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social))) as nombrePYME"); //FODEA 17
		qrySentencia.append(" , d.ic_moneda, m.cd_nombre");
		qrySentencia.append(" , decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir) as in_importe_recibir"); //FODEA 17
		qrySentencia.append(" , decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME"); //Agregado por FODEA 17
		qrySentencia.append(" , cce.cd_descripcion as CambioEstatus");
		qrySentencia.append(" , TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') as dc_fecha_cambio");
		qrySentencia.append(" , ce.ct_cambio_motivo, d.ig_numero_docto,");
		qrySentencia.append(" decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada) as in_tasa_aceptada"); //FODEA 17
		qrySentencia.append(" , TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE(?, 'dd/mm/yyyy')) as plazo ");
		qrySentencia.append(" , d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto ");
		qrySentencia.append(" , TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto");
		qrySentencia.append(" , TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') df_fecha_venc");
		qrySentencia.append(" ,'CambioEstatusIfDE::getDocumentQueryFile'");
		qrySentencia.append(" from ");
		qrySentencia.append(" com_docto_seleccionado ds, comcat_if i, com_documento d");
		qrySentencia.append(" , comcat_moneda m, comcat_epo e, comcat_pyme p, comcat_cambio_estatus cce");
		qrySentencia.append(" , comhis_cambio_estatus ce, comrel_pyme_epo pe, comrel_producto_epo prodepo ");
		qrySentencia.append(" where");
		qrySentencia.append(" ds.ic_documento = d.ic_documento");
		qrySentencia.append(" and ds.ic_if = i.ic_if"); //FODEA 17
		qrySentencia.append(" AND d.cs_dscto_especial != 'C' ");
		qrySentencia.append(" and d.ic_pyme = p.ic_pyme");
		qrySentencia.append(" and d.ic_epo = e.ic_epo");
		qrySentencia.append(" and d.ic_epo = prodepo.ic_epo ");
		qrySentencia.append(" and prodepo.ic_producto_nafin = 1 ");
		qrySentencia.append(" and e.cs_habilitado = 'S' ");
		qrySentencia.append(" and d.ic_moneda = m.ic_moneda");
		qrySentencia.append(" and d.ic_documento = ce.ic_documento");
		qrySentencia.append(" and ce.ic_cambio_estatus = cce.ic_cambio_estatus");
		qrySentencia.append(" and d.ic_if = ? ");
		qrySentencia.append(" and d.ic_pyme = pe.ic_pyme ");
		qrySentencia.append(" and d.ic_epo = pe.ic_epo");
		qrySentencia.append(qryCondicion);

		log.info("qrySentencia: " + qrySentencia.toString());
		log.info("conditions: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();

		String nombreEPO = "";
		String nombrePyme = "";
		String cd_nombre = "";
		String ig_numero_docto = "";
		String df_fecha_docto = "";
		String df_fecha_venc = "";
		String in_tasa_aceptada = "";
		String plazo = "";
		String fn_monto = "";
		String fn_porc_anticipo = "";
		String referenciaPyme = "";
		String fn_monto_dscto = "";
		String in_importe_recibir = "";
		String CambioEstatus = "";
		String dc_fecha_cambio = "";
		String ct_cambio_motivo = "";
		String referencia_pyme = "";

		try{
			if(tipo.equals("PDF")){

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(17, 100);
				pdfDoc.setLCell("EPO", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Proveedor", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Numero Documento", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Emisi�n", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Vencimiento", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Documento", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Porcentaje Descuento", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Recurso en Garant�a", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Descontar", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Operar", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Cambio de Estatus", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha del Cambio de Estatus", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Referencia", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLHeaders();

				while (rs.next()) {

					nombreEPO          = (rs.getString("nombreEPO")          ==null)?"" :rs.getString("nombreEPO");
					nombrePyme         = (rs.getString("nombrePyme")         ==null)?"" :rs.getString("nombrePyme");
					cd_nombre          = (rs.getString("cd_nombre")          ==null)?"" :rs.getString("cd_nombre");
					ig_numero_docto    = (rs.getString("ig_numero_docto")    ==null)?"" :rs.getString("ig_numero_docto");
					df_fecha_docto     = (rs.getString("df_fecha_docto")     ==null)?"" :rs.getString("df_fecha_docto");
					df_fecha_venc      = (rs.getString("df_fecha_venc")      ==null)?"" :rs.getString("df_fecha_venc");
					in_tasa_aceptada   = (rs.getString("in_tasa_aceptada")   ==null)?"0":rs.getString("in_tasa_aceptada");
					plazo              = (rs.getString("plazo")              ==null)?"0":rs.getString("plazo");
					fn_monto           = (rs.getString("fn_monto")           ==null)?"0":rs.getString("fn_monto");
					fn_porc_anticipo   = (rs.getString("fn_porc_anticipo")   ==null)?"0":rs.getString("fn_porc_anticipo");
					referenciaPyme     = (rs.getString("REFERENCIA_PYME")    ==null)?"0":rs.getString("REFERENCIA_PYME");
					fn_monto_dscto     = (rs.getString("fn_monto_dscto")     ==null)?"0":rs.getString("fn_monto_dscto");
					in_importe_recibir = (rs.getString("in_importe_recibir") ==null)?"0" :rs.getString("in_importe_recibir");
					CambioEstatus      = (rs.getString("CambioEstatus")      ==null)?"" :rs.getString("CambioEstatus");
					dc_fecha_cambio    = (rs.getString("dc_fecha_cambio")    ==null)?"" :rs.getString("dc_fecha_cambio");
					ct_cambio_motivo   = (rs.getString("ct_cambio_motivo")   ==null)?"" :rs.getString("ct_cambio_motivo");
					referencia_pyme    = (rs.getString("referencia_pyme")    ==null)?"" :rs.getString("referencia_pyme");

					pdfDoc.setLCell(nombreEPO, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(nombrePyme, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(cd_nombre, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(ig_numero_docto, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(df_fecha_docto, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(df_fecha_venc, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(in_tasa_aceptada, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$" +Comunes.formatoDecimal(fn_monto,2), "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(fn_porc_anticipo,2)+"%", "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(referenciaPyme, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(fn_monto_dscto,2), "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(in_importe_recibir,2), "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(CambioEstatus, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(dc_fecha_cambio, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(ct_cambio_motivo, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(referencia_pyme, "formas", ComunesPDF.CENTER);
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();

			}
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo "+ tipo +". ", e);
		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		  return null;
	}

/*******************************************************************************
 *                       GETTERS AND SETTERS                                   *
 *******************************************************************************/
	public List getConditions(){
		return conditions; 
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getDc_fecha_cambioMin() {
		return dc_fecha_cambioMin;
	}

	public void setDc_fecha_cambioMin(String dc_fecha_cambioMin) {
		this.dc_fecha_cambioMin = dc_fecha_cambioMin;
	}

	public String getDc_fecha_cambioMax() {
		return dc_fecha_cambioMax;
	}

	public void setDc_fecha_cambioMax(String dc_fecha_cambioMax) {
		this.dc_fecha_cambioMax = dc_fecha_cambioMax;
	}

	public String getFn_montoMin() {
		return fn_montoMin;
	}

	public void setFn_montoMin(String fn_montoMin) {
		this.fn_montoMin = fn_montoMin;
	}

	public String getFn_montoMax() {
		return fn_montoMax;
	}

	public void setFn_montoMax(String fn_montoMax) {
		this.fn_montoMax = fn_montoMax;
	}

	public String getIc_cambio_estatus() {
		return ic_cambio_estatus;
	}

	public void setIc_cambio_estatus(String ic_cambio_estatus) {
		this.ic_cambio_estatus = ic_cambio_estatus;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}
}
