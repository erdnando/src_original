package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.File;
import java.io.FileInputStream;

import java.math.BigInteger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Fecha;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsCanTrans implements IQueryGeneratorPS, IQueryGeneratorRegExtJS  {

	private int numList = 1;

	public ArrayList getConditions(HttpServletRequest request){
    	
		System.out.println("getConditions ::::(E)");
		
		ArrayList conditions = new ArrayList();

		String envia =(request.getParameter("envia") == null)? "": request.getParameter("envia");
		String ic_epo =(request.getParameter("ic_epo") == null)? "": request.getParameter("ic_epo");
		String ic_if =(request.getParameter("ic_if") == null)? "": request.getParameter("ic_if");
		String ic_pyme =(request.getParameter("ic_pyme") == null)? "": request.getParameter("ic_pyme");
		String ig_numero_docto =(request.getParameter("ig_numero_docto") == null)? "": request.getParameter("ig_numero_docto");
		String df_fecha_docto_de =(request.getParameter("df_fecha_docto_de") == null)? "": request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a =(request.getParameter("df_fecha_docto_a") == null)? "": request.getParameter("df_fecha_docto_a");
		String df_fecha_venc_de =(request.getParameter("df_fecha_venc_de") == null)? "": request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a =(request.getParameter("df_fecha_venc_a") == null)? "": request.getParameter("df_fecha_venc_a");
		String ic_moneda =(request.getParameter("ic_moneda") == null)? "": request.getParameter("ic_moneda");
		String fn_monto_de =(request.getParameter("fn_monto_de") == null)? "": request.getParameter("fn_monto_de");
		String fn_monto_a =(request.getParameter("fn_monto_a") == null)? "": request.getParameter("fn_monto_a");
		String ic_estatus_docto =(request.getParameter("ic_estatus_docto") == null)? "": request.getParameter("ic_estatus_docto");
		String df_fecha_seleccion_de =(request.getParameter("df_fecha_seleccion_de") == null)? "": request.getParameter("df_fecha_seleccion_de");
		String df_fecha_seleccion_a =(request.getParameter("df_fecha_seleccion_a") == null)? "": request.getParameter("df_fecha_seleccion_a");
		
		String df_fecha_autorizaif_de =(request.getParameter("df_fecha_autorizaif_de") == null)? "": request.getParameter("df_fecha_autorizaif_de");
		String df_fecha_autorizaif_a =(request.getParameter("df_fecha_autorizaif_a") == null)? "": request.getParameter("df_fecha_autorizaif_a");

		String in_tasa_aceptada_de =(request.getParameter("in_tasa_aceptada_de") == null)? "": request.getParameter("in_tasa_aceptada_de");
		String in_tasa_aceptada_a =(request.getParameter("in_tasa_aceptada_a") == null)? "": request.getParameter("in_tasa_aceptada_a");
		String ord_if =(request.getParameter("ord_if") == null)? "": request.getParameter("ord_if");
		String ord_pyme =(request.getParameter("ord_pyme") == null)? "": request.getParameter("ord_pyme");
		String ord_epo =(request.getParameter("ord_epo") == null)? "": request.getParameter("ord_epo");
		String ord_fec_venc =(request.getParameter("ord_fec_venc") == null)? "": request.getParameter("ord_fec_venc");
		String operacion =(request.getParameter("operacion") == null)? "": request.getParameter("operacion");
		String tipoFactoraje =(request.getParameter("tipoFactoraje") == null)? "": request.getParameter("tipoFactoraje");
		// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
		String paginaNo =(request.getParameter("paginaNo") == null)? "1": request.getParameter("paginaNo");
		String Perfil =(request.getParameter("Perfil") == null)? "": request.getParameter("Perfil");

		String nomArchivo =(request.getParameter("nomArchivo") == null)? "": request.getParameter("nomArchivo");
		String  infoProcesadaEn =(request.getParameter("infoProcesadaEn") == null)? "": request.getParameter("infoProcesadaEn");

		String 	numero_siaff 					= (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
				
		boolean 	estaHabilitadoNumeroSIAFF 	= (request.getAttribute("estaHabilitadoNumeroSIAFF") != null && request.getAttribute("estaHabilitadoNumeroSIAFF").equals("true"))?true:false;
		String ic_documento		= "";
		
		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF ){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 				= (String) numero.get("IC_EPO");
			ic_documento 		= (String) numero.get("IC_DOCUMENTO");
			
		}

		
			if (!ic_epo.equals(""))
				conditions.add(new Integer(ic_epo));
			
			if (!ic_pyme.equals("") )
				conditions.add(new Integer(ic_pyme));
			
			if (!ic_if.equals("") )
				conditions.add(new Integer(ic_if));
			
			if (!ig_numero_docto.equals(""))
				conditions.add(ig_numero_docto);
		
			if (!ic_estatus_docto.equals(""))
				if("4".equals(ic_estatus_docto)){
					conditions.add(new Integer(ic_estatus_docto));
					conditions.add(new Integer("11"));
				}else{
					conditions.add(new Integer(ic_estatus_docto));
				}
				
			
			if (!df_fecha_seleccion_de.equals("") && !df_fecha_seleccion_a.equals("")){
					conditions.add(df_fecha_seleccion_de);
					conditions.add(df_fecha_seleccion_a);
			}
			
			if (!df_fecha_autorizaif_de.equals("") && !df_fecha_autorizaif_a.equals("")){
					conditions.add(df_fecha_autorizaif_de);
					conditions.add(df_fecha_autorizaif_a);
			}
				
	
		System.out.println("*****************************************");		
		System.out.println("getConditions ::::"+conditions);
		System.out.println("*****************************************");		
		
		
		System.out.println("getConditions ::::(S)");
		
		return conditions;
	}
/**
	 * 
	 * @return 
	 * @param request
	 */
	public String getAggregateCalculationQuery(HttpServletRequest request){
		
		System.out.println("getAggregateCalculationQuery ::::(E)");
		
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		numList = 1;

		String envia = (request.getParameter("envia") == null)? "": request.getParameter("envia");
		String ic_epo =(request.getParameter("ic_epo") == null)? "": request.getParameter("ic_epo");
		String ic_if =(request.getParameter("ic_if") == null)? "": request.getParameter("ic_if");
		String ic_pyme =(request.getParameter("ic_pyme") == null)? "": request.getParameter("ic_pyme");
		String ig_numero_docto =(request.getParameter("ig_numero_docto") == null)? "": request.getParameter("ig_numero_docto");
		String df_fecha_docto_de =(request.getParameter("df_fecha_docto_de") == null)? "": request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a =(request.getParameter("df_fecha_docto_a") == null)? "": request.getParameter("df_fecha_docto_a");
		String df_fecha_venc_de =(request.getParameter("df_fecha_venc_de") == null)? "": request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a =(request.getParameter("df_fecha_venc_a") == null)? "": request.getParameter("df_fecha_venc_a");
		String ic_moneda =(request.getParameter("ic_moneda") == null)? "": request.getParameter("ic_moneda");
		String fn_monto_de =(request.getParameter("fn_monto_de") == null)? "": request.getParameter("fn_monto_de");
		String fn_monto_a =(request.getParameter("fn_monto_a") == null)? "": request.getParameter("fn_monto_a");
		String ic_estatus_docto =(request.getParameter("ic_estatus_docto") == null)? "": request.getParameter("ic_estatus_docto");
		String df_fecha_seleccion_de =(request.getParameter("df_fecha_seleccion_de") == null)? "": request.getParameter("df_fecha_seleccion_de");
		String df_fecha_seleccion_a =(request.getParameter("df_fecha_seleccion_a") == null)? "": request.getParameter("df_fecha_seleccion_a");
		String in_tasa_aceptada_de =(request.getParameter("in_tasa_aceptada_de") == null)? "": request.getParameter("in_tasa_aceptada_de");
		String in_tasa_aceptada_a =(request.getParameter("in_tasa_aceptada_a") == null)? "": request.getParameter("in_tasa_aceptada_a");
		String ord_if =(request.getParameter("ord_if") == null)? "": request.getParameter("ord_if");
		String ord_pyme =(request.getParameter("ord_pyme") == null)? "": request.getParameter("ord_pyme");
		String ord_epo =(request.getParameter("ord_epo") == null)? "": request.getParameter("ord_epo");
		String ord_fec_venc =(request.getParameter("ord_fec_venc") == null)? "": request.getParameter("ord_fec_venc");
		String operacion =(request.getParameter("operacion") == null)? "": request.getParameter("operacion");
		String tipoFactoraje =(request.getParameter("tipoFactoraje") == null)? "": request.getParameter("tipoFactoraje");
		
		String df_fecha_autorizaif_de =(request.getParameter("df_fecha_autorizaif_de") == null)? "": request.getParameter("df_fecha_autorizaif_de");
		String df_fecha_autorizaif_a =(request.getParameter("df_fecha_autorizaif_a") == null)? "": request.getParameter("df_fecha_autorizaif_a");
		
    	// MPCS.- Parametros para descuento automatico
		String  infoProcesadaEn =(request.getParameter("infoProcesadaEn") == null)? "": request.getParameter("infoProcesadaEn");
		// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
		String paginaNo = (request.getParameter("paginaNo") == null)? "1": request.getParameter("paginaNo");
		String nomArchivo =(request.getParameter("nomArchivo") == null)? "": request.getParameter("nomArchivo");
		String  ic_banco_fondeo =(request.getParameter("ic_banco_fondeo") == null)? "": request.getParameter("ic_banco_fondeo");
		String hint = "";
		String campos1[] = { "nombreEpo", "D.df_fecha_venc", "nombrePyme" };
		String orden1[] = { ord_epo, ord_fec_venc, ord_pyme };
		String campos2[] = { "nombreEpo", "nombreIf", "D.df_fecha_venc", "nombrePyme" };
		String orden2[] = { ord_epo, ord_if, ord_fec_venc, ord_pyme };
		String Perfil =(request.getParameter("Perfil") == null)? "": request.getParameter("Perfil");

		String	numero_siaff 					= (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
		boolean 	estaHabilitadoNumeroSIAFF 	= (request.getAttribute("estaHabilitadoNumeroSIAFF") != null && request.getAttribute("estaHabilitadoNumeroSIAFF").equals("true"))?true:false;
		String	ic_documento = "";
	

		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 				= (String) numero.get("IC_EPO");
			ic_documento		= (String) numero.get("IC_DOCUMENTO");
			
		}

		if (
				ic_estatus_docto.equals("3")
				|| ic_estatus_docto.equals("4")
				|| ic_estatus_docto.equals("11")
				|| ic_estatus_docto.equals("12")
				|| ic_estatus_docto.equals("16")
				|| ic_estatus_docto.equals("24"))
		{
			
		System.out.println("Paso por aqui 1");
			
			hint = " /*+  use_nl(d cv m s pe e) index(d IN_COM_DOCUMENTO_04_NUK) */ ";//ACF
			
			if (!ic_epo.equals(""))
				condicion.append("and D.ic_epo = ? ");
			
			if (!ic_pyme.equals("") )
				condicion.append("and D.ic_pyme = ? ");
			
			if (!ic_if.equals("") )
				condicion.append("and D.ic_if = ? ");
			
			if (!ig_numero_docto.equals("")) {
				condicion.append("and D.ig_numero_docto = ? ");
			}
				
							
							
		
			if (!ic_estatus_docto.equals("")) {
				if("4".equals(ic_estatus_docto)){
					condicion.append("and D.ic_estatus_docto in (?,?) ");
				}else{
					condicion.append("and D.ic_estatus_docto = ? ");
				}
			}
			
			
			if (!df_fecha_seleccion_de.equals("")
			&& !df_fecha_seleccion_a.equals("")){
					condicion.append(" AND s.df_fecha_solicitud BETWEEN (to_date(?,'dd/mm/yyyy'))AND (to_date(?,'dd/mm/yyyy')+1 )");
		}
		if (!df_fecha_autorizaif_de.equals("")
			&& !df_fecha_autorizaif_a.equals("")){
					condicion.append(" AND d.df_fecha_docto BETWEEN (to_date(?,'dd/mm/yyyy'))AND (to_date(?,'dd/mm/yyyy')+1) ");
		}
		
		if( (df_fecha_seleccion_de.equals("") || df_fecha_seleccion_a.equals("")) && (df_fecha_autorizaif_de.equals("")||df_fecha_autorizaif_a.equals(""))  ){
			
			condicion.append(" AND (( s.DF_OPERACION>= TRUNC(SYSDATE)  AND s.DF_OPERACION< TRUNC(SYSDATE) + 1)");
			condicion.append(" OR ");
			condicion.append(" ( d.df_fecha_venc>= TRUNC(SYSDATE)  AND d.df_fecha_venc< TRUNC(SYSDATE) + 1 AND d.ic_estatus_docto = 4 )) ");
		}
			
		
		System.out.println("****Paso por aqui 22***************************");
		
		
				qrySentencia.append(
						"select "+hint+" count(*) AS ConsCanTrans, sum(fn_monto) AS FN_MONTO, "+
						"sum(fn_monto_dscto) AS FN_MONTO_DSCTO, "+
          				"M.cd_nombre, M.ic_moneda, 'D' as tipoDocto "+
						"from com_documento D, comcat_moneda M, comvis_aforo CV, comrel_producto_epo pe, comcat_epo E, com_solicitud S "+
						" where D.ic_moneda = M.ic_moneda "+
						" and D.ic_epo = CV.ic_epo " +
						" AND pe.ic_epo = D.ic_epo " +
						" AND pe.ic_epo = E.ic_epo " +
						
            (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
            (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
						" AND pe.ic_producto_nafin = 1 " );
						
          		if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
            		qrySentencia.append(" and exists (select 1"   +
                            						" from com_docto_seleccionado ds"   +
                            						" where d.ic_documento=ds.ic_documento"   +
                            						" and ds.cc_acuse like ?) ");
		
			qrySentencia.append("and D.cs_dscto_especial != 'C' ");

		//qrySentencia.append(" AND s.DF_OPERACION>= TRUNC(SYSDATE) AND s.DF_OPERACION< TRUNC(SYSDATE) + 1 ");
		
		

			qrySentencia.append(condicion.toString() +
								" GROUP BY M.ic_moneda, M.cd_nombre"+
                                " order by M.ic_moneda ");
		
		
		}
		
		int iIndex = qrySentencia.toString().indexOf("where  and");
		if(iIndex > 0){
			String toWhere = qrySentencia.toString().substring(0, qrySentencia.toString().indexOf("where  and"));
			System.out.println(toWhere);
			qrySentencia = new StringBuffer( toWhere + "where" + qrySentencia.toString().substring(qrySentencia.toString().indexOf("where  and")+10,qrySentencia.length()) );
		}

	
		System.out.println("*****************************************************************************");
		System.out.println("ConsCanTrans::getAggregateCalculationQuery::::::::  "+qrySentencia.toString());
		System.out.println("*****************************************************************************");
		
		System.out.println("ConsCanTrans::getAggregateCalculationQuery::::(S)");
		
		
		
		return qrySentencia.toString();
	}

	public String getDocumentQuery(HttpServletRequest request){
		
		System.out.println("getDocumentQuery :::: (E)" );
		
		String qrySentencia = null;

		String envia =(request.getParameter("envia") == null)? "": request.getParameter("envia");
		String ic_epo =(request.getParameter("ic_epo") == null)? "": request.getParameter("ic_epo");
		String ic_if =(request.getParameter("ic_if") == null)? "": request.getParameter("ic_if");
		String ic_pyme =(request.getParameter("ic_pyme") == null)? "": request.getParameter("ic_pyme");
		String ig_numero_docto =(request.getParameter("ig_numero_docto") == null)? "": request.getParameter("ig_numero_docto");
		String df_fecha_docto_de =(request.getParameter("df_fecha_docto_de") == null)	? "": request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a =	(request.getParameter("df_fecha_docto_a") == null)? "": request.getParameter("df_fecha_docto_a");
	
		String df_fecha_venc_de =	(request.getParameter("df_fecha_venc_de") == null)? "": request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a =(request.getParameter("df_fecha_venc_a") == null)? "": request.getParameter("df_fecha_venc_a");
		String ic_moneda =(request.getParameter("ic_moneda") == null)? "": request.getParameter("ic_moneda");
		String fn_monto_de =(request.getParameter("fn_monto_de") == null)? "": request.getParameter("fn_monto_de");
		String fn_monto_a =(request.getParameter("fn_monto_a") == null)? "": request.getParameter("fn_monto_a");
		String ic_estatus_docto =	(request.getParameter("ic_estatus_docto") == null)? "": request.getParameter("ic_estatus_docto");
		String df_fecha_seleccion_de =(request.getParameter("df_fecha_seleccion_de") == null)? "": request.getParameter("df_fecha_seleccion_de");
		String df_fecha_seleccion_a =(request.getParameter("df_fecha_seleccion_a") == null)? "": request.getParameter("df_fecha_seleccion_a");
		
		String df_fecha_autorizaif_de =(request.getParameter("df_fecha_autorizaif_de") == null)? "": request.getParameter("df_fecha_autorizaif_de");
		String df_fecha_autorizaif_a =(request.getParameter("df_fecha_autorizaif_a") == null)? "": request.getParameter("df_fecha_autorizaif_a");
		
		String in_tasa_aceptada_de =(request.getParameter("in_tasa_aceptada_de") == null)? "": request.getParameter("in_tasa_aceptada_de");
		String in_tasa_aceptada_a =(request.getParameter("in_tasa_aceptada_a") == null)? "": request.getParameter("in_tasa_aceptada_a");
		String ord_if =(request.getParameter("ord_if") == null)? "": request.getParameter("ord_if");
		String ord_pyme =(request.getParameter("ord_pyme") == null)? "": request.getParameter("ord_pyme");
		String ord_epo =(request.getParameter("ord_epo") == null)? "": request.getParameter("ord_epo");
		String ord_fec_venc =(request.getParameter("ord_fec_venc") == null)	? "": request.getParameter("ord_fec_venc");
		String operacion =(request.getParameter("operacion") == null)? "": request.getParameter("operacion");
		String tipoFactoraje =(request.getParameter("tipoFactoraje") == null)? "": request.getParameter("tipoFactoraje");
		// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
		String paginaNo =(request.getParameter("paginaNo") == null)? "1": request.getParameter("paginaNo");
		String nomArchivo =(request.getParameter("nomArchivo") == null)? "": request.getParameter("nomArchivo");
		String  ic_banco_fondeo =(request.getParameter("ic_banco_fondeo") == null)? "": request.getParameter("ic_banco_fondeo");
      String Perfil =(request.getParameter("Perfil") == null)? "": request.getParameter("Perfil");

    
		String  infoProcesadaEn =	(request.getParameter("infoProcesadaEn") == null)? ""	: request.getParameter("infoProcesadaEn");

	
		String 	numero_siaff   				= (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
		boolean 	estaHabilitadoNumeroSIAFF 	= (request.getAttribute("estaHabilitadoNumeroSIAFF") != null && request.getAttribute("estaHabilitadoNumeroSIAFF").equals("true"))?true:false;
		String ic_documento	 = "";
		//String ic_epo				= "";
		//String ig_numero_docto 	= "";
		// * String cc_acuse = "";

		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 			= (String) numero.get("IC_EPO");
			ic_documento	= (String) numero.get("IC_DOCUMENTO");
		}


						qrySentencia =
							"select D.ic_documento "
              				+ ",'ConsCanTrans::getDocumentQuery'"
							+ " from com_documento D, comrel_producto_epo pe, comcat_epo E, com_solicitud S "
							+ " where " +
							" D.ic_epo = pe.ic_epo " +
							" AND pe.ic_epo = E.ic_epo " +
							
              (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
              (Perfil.equals("ADMIN BANCOMEXT")?" and E.ic_banco_fondeo= 2":"") +
							" AND pe.ic_producto_nafin = 1 ";
						
		            if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
		              qrySentencia += " and exists (select 1"   +
                              "    from com_docto_seleccionado ds"   +
                              " 	 where d.ic_documento=ds.ic_documento"   +
                              " 	 and   ds.cc_acuse like ?) "  ;

			if (!ic_epo.equals(""))
				qrySentencia += " and D.ic_epo = ?";
			
			if (!ic_pyme.equals("") )
				qrySentencia += " and D.ic_pyme = ?";
			
			if (!ic_if.equals("") )
				qrySentencia += " and D.ic_if = ?";
			
			if (!ig_numero_docto.equals(""))
				qrySentencia += " and D.ig_numero_docto = ?";
				
			
			

			if (!ic_estatus_docto.equals("")){
				if("4".equals(ic_estatus_docto)){
					qrySentencia += " and D.ic_estatus_docto in (?,?) ";
				}else{
					qrySentencia += " and D.ic_estatus_docto = ?";
				}
				
			}
			
			
			if (!ic_estatus_docto.equals("3")	&& !ic_estatus_docto.equals("24"))
			{
				if (!df_fecha_seleccion_de.equals("")
					&& !df_fecha_seleccion_a.equals("")){
							qrySentencia += " AND s.df_operacion BETWEEN (to_date(?,'dd/mm/yyyy'))AND (to_date(?,'dd/mm/yyyy')+1 )";
				}
				if (!df_fecha_autorizaif_de.equals("")
					&& !df_fecha_autorizaif_a.equals("")){
							qrySentencia += " AND s.df_fecha_solicitud BETWEEN (to_date(?,'dd/mm/yyyy'))AND (to_date(?,'dd/mm/yyyy')+1) ";
				}
			}
			
		if( (df_fecha_seleccion_de.equals("") || df_fecha_seleccion_a.equals("")) && (df_fecha_autorizaif_de.equals("")||df_fecha_autorizaif_a.equals(""))  ){
			
			qrySentencia += " AND (( s.df_operacion>= TRUNC(SYSDATE)  AND s.DF_OPERACION< TRUNC(SYSDATE) + 1)";
			qrySentencia += " OR ";
			qrySentencia += " ( d.df_fecha_venc>= TRUNC(SYSDATE)  AND d.df_fecha_venc< TRUNC(SYSDATE) + 1 AND d.ic_estatus_docto = 4 )) ";
		}
		qrySentencia += " AND D.ic_documento = S.ic_documento(+)";

		int iIndex = qrySentencia.indexOf("where  and");
		if(iIndex > 0){
			String toWhere = qrySentencia.substring(0,qrySentencia.indexOf("where  and"));
			System.out.println(toWhere);
			qrySentencia = toWhere + "where" + qrySentencia.substring(qrySentencia.indexOf("where  and")+10,qrySentencia.length());
		}
		
		System.out.println("******************************************************");
		System.out.println("ConsCanTrans::getDocumentQuery  "+qrySentencia);
		System.out.println("******************************************************");	
		
		System.out.println("ConsCanTrans::getDocumentQuery (S) ");

		return qrySentencia;
  }

 	public String getDocumentQueryFile(HttpServletRequest request){
		
		System.out.println("getDocumentQueryFile:::: (E)");
		
		String qrySentencia = null;

		String envia =(request.getParameter("envia") == null)? "": request.getParameter("envia");
		String ic_epo =(request.getParameter("ic_epo") == null)? "": request.getParameter("ic_epo");
		String ic_if =(request.getParameter("ic_if") == null)? "": request.getParameter("ic_if");
		String ic_pyme =(request.getParameter("ic_pyme") == null)? "": request.getParameter("ic_pyme");
		String ig_numero_docto =(request.getParameter("ig_numero_docto") == null)? "": request.getParameter("ig_numero_docto");
		String df_fecha_docto_de =(request.getParameter("df_fecha_docto_de") == null)? "": request.getParameter("df_fecha_docto_de");
		String df_fecha_docto_a =(request.getParameter("df_fecha_docto_a") == null)? "": request.getParameter("df_fecha_docto_a");
		
		String df_fecha_venc_de =(request.getParameter("df_fecha_venc_de") == null)? "": request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a =(request.getParameter("df_fecha_venc_a") == null)? "": request.getParameter("df_fecha_venc_a");
		String ic_moneda =(request.getParameter("ic_moneda") == null)? "": request.getParameter("ic_moneda");
		String fn_monto_de =(request.getParameter("fn_monto_de") == null)? "": request.getParameter("fn_monto_de");
		String fn_monto_a =(request.getParameter("fn_monto_a") == null)? "": request.getParameter("fn_monto_a");
		String ic_estatus_docto =(request.getParameter("ic_estatus_docto") == null)? "": request.getParameter("ic_estatus_docto");
		String df_fecha_seleccion_de =(request.getParameter("df_fecha_seleccion_de") == null)? "": request.getParameter("df_fecha_seleccion_de");
		String df_fecha_seleccion_a =(request.getParameter("df_fecha_seleccion_a") == null)? "": request.getParameter("df_fecha_seleccion_a");
		
		String df_fecha_autorizaif_de =(request.getParameter("df_fecha_autorizaif_de") == null)? "": request.getParameter("df_fecha_autorizaif_de");
		String df_fecha_autorizaif_a =(request.getParameter("df_fecha_autorizaif_a") == null)? "": request.getParameter("df_fecha_autorizaif_a");
		
		String in_tasa_aceptada_de =(request.getParameter("in_tasa_aceptada_de") == null)? "": request.getParameter("in_tasa_aceptada_de");
		String in_tasa_aceptada_a =(request.getParameter("in_tasa_aceptada_a") == null)? "": request.getParameter("in_tasa_aceptada_a");
		String ord_if =(request.getParameter("ord_if") == null)? "": request.getParameter("ord_if");
		String ord_pyme =(request.getParameter("ord_pyme") == null)? "": request.getParameter("ord_pyme");
		String ord_epo =(request.getParameter("ord_epo") == null)? "": request.getParameter("ord_epo");
		String ord_fec_venc =(request.getParameter("ord_fec_venc") == null)? "": request.getParameter("ord_fec_venc");
		String operacion =(request.getParameter("operacion") == null)? "": request.getParameter("operacion");
		String tipoFactoraje =(request.getParameter("tipoFactoraje") == null)? "": request.getParameter("tipoFactoraje");
		// Linea a agregar para la paginacion al igual que el campo escondido con el mismo nombre.
		String paginaNo =(request.getParameter("paginaNo") == null)? "1": request.getParameter("paginaNo");
		String nomArchivo =(request.getParameter("nomArchivo") == null)? "": request.getParameter("nomArchivo");
		String  ic_banco_fondeo =(request.getParameter("ic_banco_fondeo") == null)? "": request.getParameter("ic_banco_fondeo");
    String Perfil =(request.getParameter("Perfil") == null)? "": request.getParameter("Perfil");
		
		//campos de la parametrizacion de documentos
		String  consultaDoctos = "";
		consultaDoctos = ", TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO ";
				
    // MPCS.- Parametros para descuento automatico
		String  infoProcesadaEn =	(request.getParameter("infoProcesadaEn") == null)	? "" : request.getParameter("infoProcesadaEn");

	
		String 	numero_siaff   				= (request.getParameter("numero_siaff") == null) ? "" : request.getParameter("numero_siaff");
		boolean 	estaHabilitadoNumeroSIAFF 	= (request.getAttribute("estaHabilitadoNumeroSIAFF") != null && request.getAttribute("estaHabilitadoNumeroSIAFF").equals("true"))?true:false;
		String ic_documento	 = "";
		
		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);

			ic_epo 			= (String) numero.get("IC_EPO");
			ic_documento	= (String) numero.get("IC_DOCUMENTO");
			
		}
	
			qrySentencia =
				"  SELECT /*+"   +
				"         use_nl(D,DS,S,CC1,CD,P,E,I,I2,ED,M)"   +
				"         index(D in_com_documento_04_nuk)"   +
				"         index(CD cp_fop_cobro_disposicion_pk)"   +
				"         index(M cp_comcat_moneda_pk)"   +
				"         index(ED cp_comcat_estatus_docto_pk)"   +
				"      */"   +
				"    E.cg_razon_social nombreEpo,"   +
				"  	P.cg_razon_social nombrePyme,"   +
				"  	I.cg_razon_social AS nombreIf,"   +
				"  	D.ig_numero_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') AS df_fecha_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS df_fecha_venc,"   +
				"  	M.cd_nombre,"   +
				"  	D.fn_monto,"   +
				"  	D.fn_porc_anticipo,"   +
				"  	D.fn_monto_dscto,"   +
				"  	DS.in_tasa_aceptada,"   +
				"  	ED.cd_descripcion,"   +
				"  	TO_CHAR(S.df_fecha_solicitud,'DD/MM/YYYY') AS df_fecha_solicitud,"   +
				"  	D.ic_moneda, D.ic_documento,"   +
				"  	D.cs_cambio_importe,"   +
				"  	D.CS_DSCTO_ESPECIAL,"   +
				"  	DECODE(d.cs_dscto_especial,'D',i2.cg_razon_social,'') AS beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'') AS fn_porc_beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'') AS RECIBIR_BENEF,"   +
				"  	DECODE(d.cs_dscto_especial,'D',ds.in_importe_recibir - ds.fn_importe_recibir_benef,'') AS NETO_REC_PYME,"   +
				"  	DS.cc_acuse,"   +
				"  	DS.in_importe_recibir,"   +
				"  	CASE WHEN cc1.fn_monto_pago IS NOT NULL THEN"   +
				"  		cc1.fn_monto_pago"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_monto_pago IS NOT NULL THEN"   +
				"  			cd.fn_monto_pago"   +
				"  		END"   +
				" 	END AS fn_monto_pago,"   +
				"  	CASE WHEN cc1.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  		cc1.fn_porc_docto_aplicado"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  			cd.fn_porc_docto_aplicado"   +
				"  		END"   +
				"  	END AS porcDoctoAplicado,"   +
				" 	DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(DS.df_fecha_seleccion), 54, sigfechahabilxepo (E.ic_epo, TRUNC(DS.df_fecha_seleccion), 1, '+')), S.ig_plazo ) AS ig_plazo,"   +
				" 	DS.in_importe_interes,"   +
				"  	D.ic_pyme,"   +
				"   I.ig_tipo_piso, DS.fn_remanente, "+ //foda 015-2005
		    	"   to_char(D.DF_ALTA, 'dd/mm/yyyy') df_alta, "+
				"  	'ConsCanTrans::getDocumentQueryFile' AS pantalla,P.cg_rfc as rfc, "+
				" 		D.ic_epo " +
				consultaDoctos + 
				"  FROM"   +
				"  	com_documento D,              "   +
				"  	com_docto_seleccionado DS,"   +
				"  	com_solicitud S,"   +
				"  	(SELECT /*+index(cc cp_com_cobro_credito_pk)*/cc.ic_documento, cc.fn_porc_docto_aplicado ,"   +
				"  	SUM(cc.fn_monto_pago) AS fn_monto_pago"   +
				"  	FROM com_cobro_credito cc"   +
				"  	GROUP BY cc.ic_documento, cc.fn_porc_docto_aplicado) cc1,"   +
				"  	fop_cobro_disposicion cd,"   +
				"  	comcat_pyme P,"   +
				"     comcat_epo E,"   +
				"  	comcat_if I,"   +
				"  	comcat_if I2,"   +
				"  	comcat_estatus_docto ED,"   +
				"  	comcat_moneda M,"   +
				" 		comrel_producto_epo pe " +
				"  WHERE D.ic_epo = E.ic_epo"   +
				" 		AND D.ic_epo = pe.ic_epo " +
				" AND pe.ic_epo = E.ic_epo " +				
        (!ic_banco_fondeo.equals("")?" and E.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        (Perfil.equals("ADMIN BANCOMEXT")?" and S.ic_banco_fondeo= 2":"") +
				" 		AND pe.ic_producto_nafin = 1 " +				
				"  	AND D.ic_pyme = P.ic_pyme"   +
				"  	AND D.ic_moneda = M.ic_moneda"   +
				"  	AND D.ic_documento = DS.ic_documento"   +
				"  	AND D.ic_estatus_docto = ED.ic_estatus_docto"   +
				"  	AND D.ic_documento = S.ic_documento(+)"   +
				"  	AND D.ic_documento = CD.ic_documento(+)"   +
				"  	AND D.ic_if = I.ic_if(+)"   +
				"  	AND D.ic_beneficiario = I2.ic_if (+)"   +
				"  	AND DS.ic_documento = CC1.ic_documento(+)"  ;

     	
			if (!ic_epo.equals(""))
				qrySentencia += " and D.ic_epo = ?";
			
			if (!ic_pyme.equals("") )
				qrySentencia += " and D.ic_pyme = ?";
			
			if (!ic_if.equals("") && !ic_if.equals(""))
				qrySentencia += " and D.ic_if = ?";
			
			if (!ig_numero_docto.equals(""))
				qrySentencia += " and D.ig_numero_docto = ?";

							
				
		
			if (!ic_estatus_docto.equals("")){
				if("4".equals(ic_estatus_docto)){
					qrySentencia += " and D.ic_estatus_docto in (?,?) ";
				}else{
					qrySentencia += " and D.ic_estatus_docto = ? ";
				}
			}
			
			if (!ic_estatus_docto.equals("3")	&& !ic_estatus_docto.equals("24"))
			{
				if (!df_fecha_seleccion_de.equals("")		&& !df_fecha_seleccion_a.equals(""))
					qrySentencia
						+= " and TO_DATE((TO_CHAR(S.df_operacion,'DD/MM/YYYY')),'DD/MM/YYYY') between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ";
				
				if (!df_fecha_autorizaif_de.equals("")
					&& !df_fecha_autorizaif_a.equals("")){
							qrySentencia += "AND S.df_fecha_solicitud BETWEEN (to_date(?,'dd/mm/yyyy'))AND (to_date(?,'dd/mm/yyyy')+1)";
				}
			}
			
		
		int iIndex = qrySentencia.indexOf("where  and");
		if(iIndex > 0){
			String toWhere = qrySentencia.substring(0,qrySentencia.indexOf("where  and"));
			System.out.println(toWhere);
			qrySentencia = toWhere + "where" + qrySentencia.substring(qrySentencia.indexOf("where  and")+10,qrySentencia.length());
		}
		
		System.out.println("******************************************************");
		System.out.println("ConsCanTrans::getDocumentQueryFile: "+qrySentencia);
		System.out.println("******************************************************");	
		System.out.println("ConsCanTrans::getDocumentQueryFile: (S) ");

		return qrySentencia;
  }

	/**
	 *
	 *
	 */

	public int getNumList(HttpServletRequest request){
		return this.numList;
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
		
		System.out.println("getDocumentSummaryQueryForIds ::: (E)");
		
		String ic_estatus_docto =
			(request.getParameter("ic_estatus_docto") == null)
				? ""
				: request.getParameter("ic_estatus_docto");

		StringBuffer query = new StringBuffer();
		StringBuffer clavesDocumentos = new StringBuffer();
		for (Iterator it = ids.iterator(); it.hasNext();){
			it.next();
			clavesDocumentos.append("?,");
		}
		clavesDocumentos = clavesDocumentos.delete(clavesDocumentos.length()-1,
		clavesDocumentos.length());

  
			query.append(
				"  SELECT"   +
				"  	 /*+"   +
				"         use_nl(D,DS,S,CC1,CD,P,E,I,I2,ED,M)"   +
				"         index(D cp_com_documento_pk)"   +
				"         index(CD cp_fop_cobro_disposicion_pk)"   +
				"         index(M cp_comcat_moneda_pk)"   +
				"         index(ED cp_comcat_estatus_docto_pk)"   +
				"      */  "   +
				" 	E.cg_razon_social nombreEpo,"   +
				"  	P.cg_razon_social nombrePyme,"   +
				"  	I.cg_razon_social AS nombreIf,"   +
				"  	D.ig_numero_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') AS df_fecha_docto,"   +
				"  	TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS df_fecha_venc,"   +
				"  	M.cd_nombre,"   +
				"  	D.fn_monto,"   +
				"  	D.fn_porc_anticipo,"   +
				"  	D.fn_monto_dscto,"   +
				"  	DS.in_tasa_aceptada,"   +
				"  	ED.cd_descripcion,"   +
				"  	TO_CHAR(S.df_fecha_solicitud,'DD/MM/YYYY') AS df_fecha_solicitud,"   +
				"  	D.ic_moneda, D.ic_documento,"   +
				"  	D.cs_cambio_importe,"   +
				"  	D.CS_DSCTO_ESPECIAL,"   +
				"  	DECODE(d.cs_dscto_especial,'D',i2.cg_razon_social,'') AS beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_porc_beneficiario,'') AS fn_porc_beneficiario,"   +
				"  	DECODE(d.cs_dscto_especial,'D',d.fn_monto * (d.fn_porc_beneficiario / 100),'') AS RECIBIR_BENEF,"   +
				"  	DECODE(d.cs_dscto_especial,'D',ds.in_importe_recibir - ds.fn_importe_recibir_benef,'') AS NETO_REC_PYME,"   +
				"  	DS.cc_acuse,"   +
				"  	DS.in_importe_recibir,"   +
				"  	CASE WHEN cc1.fn_monto_pago IS NOT NULL THEN"   +
				"  		cc1.fn_monto_pago"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_monto_pago IS NOT NULL THEN"   +
				"  			cd.fn_monto_pago"   +
				"  		END"   +
				" 	END AS fn_monto_pago,"   +
				"  	CASE WHEN cc1.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  		cc1.fn_porc_docto_aplicado"   +
				"  	ELSE"   +
				"  		CASE WHEN cd.fn_porc_docto_aplicado IS NOT NULL THEN"   +
				"  			cd.fn_porc_docto_aplicado"   +
				"  		END"   +
				"  	END AS porcDoctoAplicado,"   +
				" 	DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc)-DECODE(D.ic_moneda, 1, TRUNC(DS.df_fecha_seleccion), 54, sigfechahabilxepo (E.ic_epo, TRUNC(DS.df_fecha_seleccion), 1, '+')), S.ig_plazo ) AS ig_plazo,"   +
				" 	DS.in_importe_interes,"   +
				"  	D.ic_pyme,"   +
				"   I.ig_tipo_piso, DS.fn_remanente, "+ //foda 015-2005
		    	"   to_char(D.DF_ALTA, 'dd/mm/yyyy') df_alta, "+
				"  	'ConsCanTrans::getDocumentSummaryQueryForIds' AS pantalla, "   +
				" 		D.ic_epo, " +
				" 	TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO, " +
				"   TO_CHAR (s.DF_OPERACION, 'dd/mm/yyyy') DF_OPERACION "+    // Fodea 036 Mejoras II
				"  FROM comcat_epo E,"   +
				"  	comcat_pyme P,"   +
				"  	comcat_if I,"   +
				"  	comcat_if I2,"   +
				"  	com_documento D,"   +
				"  	comcat_moneda M,"   +
				"  	com_docto_seleccionado DS,"   +
				"  	comcat_estatus_docto ED,"   +
				"  	com_solicitud S,"   +
				"  	(SELECT /*+index(cc cp_com_cobro_credito_pk)*/"   +
				"     cc.ic_documento, cc.fn_porc_docto_aplicado ,"   +
				"  	SUM(cc.fn_monto_pago) AS fn_monto_pago"   +
				"  	FROM com_cobro_credito cc"   +
				" 	where cc.ic_documento in (" + clavesDocumentos + ") " +
				"  	GROUP BY cc.ic_documento, cc.fn_porc_docto_aplicado) cc1,"   +
				"  	fop_cobro_disposicion cd"   +
				"  WHERE D.ic_epo = E.ic_epo"   +
				"  	AND D.ic_pyme = P.ic_pyme"   +
				"  	AND D.ic_moneda = M.ic_moneda"   +
				"  	AND D.ic_documento = DS.ic_documento"   +
				"  	AND D.ic_estatus_docto = ED.ic_estatus_docto"   +
				"  	AND D.ic_documento = S.ic_documento(+)"   +
				"  	AND D.ic_if = I.ic_if(+)"   +
				"  	AND D.ic_beneficiario = I2.ic_if (+)"   +
				"  	AND DS.ic_documento = CC1.ic_documento(+)"   +
				"  	AND DS.ic_documento = CD.ic_documento(+)"   +
				//" 		AND s.DF_OPERACION>= TRUNC(SYSDATE) AND s.DF_OPERACION< TRUNC(SYSDATE) + 1 "+				
           " 	and D.ic_documento in (" + clavesDocumentos + ") ");

			numList = 2;


		System.out.println("*******************************************");
		System.out.println("ConsCanTrans::PageQuery:  "+query);
		System.out.println("*******************************************");
		System.out.println("getDocumentSummaryQueryForIds ::: (S)");
		
		
		return query.toString();
  }
	
 
 	 public HashMap getPartesDelNumeroSIAFF(String numeroSIAFF){
		HashMap resultado = new HashMap();

		BigInteger	icEPO 		= new BigInteger("-1");
		BigInteger	icDocumento = new BigInteger("-1");

		if(numeroSIAFF != null && !numeroSIAFF.equals("") && numeroSIAFF.length() == 15){
			icEPO 		= new BigInteger(numeroSIAFF.substring(0,4));
			icDocumento = new BigInteger(numeroSIAFF.substring(4,15));
		}

		resultado.put("IC_EPO",			icEPO.toString());
		resultado.put("IC_DOCUMENTO",	icDocumento.toString());

		return resultado;
	}
	
	
		/******MIGRACI�N NAFIN ******************/
 
 //Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsCanTrans.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_banco_fondeo;
	private String ic_epo;
	private String ic_if;
	private String df_fecha_seleccion_de;
	private String df_fecha_seleccion_a;
	private String df_fecha_autorizaif_de;
	private String df_fecha_autorizaif_a;
	private String ig_numero_docto;
	private String ic_pyme;
	private String perfil;
	private String infoProcesadaEn;
	private String numero_siaff;
	private String estaHabilitadoNumeroSIAFF;
	private String ic_documento;
	private String ic_estatus_docto;


	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		qrySentencia.append(" Select count(D.ic_documento) as total " +
								  " from com_documento D, comrel_producto_epo pe, comcat_epo E, com_solicitud S "+
								  " where  D.ic_epo = pe.ic_epo " +
								  " AND pe.ic_epo = E.ic_epo "+															
								 " AND pe.ic_producto_nafin = 1 ");
						
		         /*   if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
		              qrySentencia.append(" and exists (select 1"   +
                              "    from com_docto_seleccionado ds"   +
                              " 	 where d.ic_documento=ds.ic_documento"   +
                              " 	 and   ds.cc_acuse like ?) " ) ;

			*/

			if (!ic_epo.equals("")){
				qrySentencia .append(" and D.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			
			if (!ic_pyme.equals("") ) {
				qrySentencia.append(" and D.ic_pyme = ?");
				conditions.add(ic_pyme);
			}
			
			if (!ic_if.equals("") ) {
				qrySentencia.append( " and D.ic_if = ?");
				conditions.add(ic_if);
			}
			
			if (!ig_numero_docto.equals("")) {
				qrySentencia.append(" and D.ig_numero_docto = ?");
				conditions.add(ig_numero_docto);
			}
			
			if (!ic_estatus_docto.equals("")){
				if("4".equals(ic_estatus_docto)){
					qrySentencia.append( " and D.ic_estatus_docto in (?,?) ");
					conditions.add(new Integer(ic_estatus_docto));
					conditions.add(new Integer("11"));
				}else{
					qrySentencia.append( " and D.ic_estatus_docto = ? ");
					conditions.add(ic_estatus_docto);
				}
			}
			
			if (!ic_estatus_docto.equals("3")	&& !ic_estatus_docto.equals("24"))	{
				if (!df_fecha_seleccion_de.equals("")	&& !df_fecha_seleccion_a.equals("")){
					qrySentencia .append(" AND s.df_operacion BETWEEN (to_date(?,'dd/mm/yyyy'))AND (to_date(?,'dd/mm/yyyy')+1 )");
					conditions.add(df_fecha_seleccion_de);
					conditions.add(df_fecha_seleccion_a);
				}
				if (!df_fecha_autorizaif_de.equals("")	&& !df_fecha_autorizaif_a.equals("")){
					qrySentencia .append( " AND s.df_fecha_solicitud BETWEEN (to_date(?,'dd/mm/yyyy'))AND (to_date(?,'dd/mm/yyyy')+1) ");
					conditions.add(df_fecha_autorizaif_de);
					conditions.add(df_fecha_autorizaif_a);
				}
			}
			
			if( (df_fecha_seleccion_de.equals("") || df_fecha_seleccion_a.equals("")) && (df_fecha_autorizaif_de.equals("")||df_fecha_autorizaif_a.equals(""))  ){
				qrySentencia.append(" AND (( s.df_operacion>= TRUNC(SYSDATE)  AND s.DF_OPERACION< TRUNC(SYSDATE) + 1)");
				qrySentencia.append(" OR ");
				qrySentencia.append( " ( d.df_fecha_venc>= TRUNC(SYSDATE)  AND d.df_fecha_venc< TRUNC(SYSDATE) + 1 AND d.ic_estatus_docto = 4 )) ");
			}
			
			qrySentencia.append(" AND D.ic_documento = S.ic_documento(+)");
			
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	


	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
/*		
		if(!numero_siaff.equals("") && estaHabilitadoNumeroSIAFF.equals("S")){
			HashMap numero = getPartesDelNumeroSIAFF(numero_siaff);
			ic_epo 			= (String) numero.get("IC_EPO");
			ic_documento	= (String) numero.get("IC_DOCUMENTO");
		}
*/
		qrySentencia.append(" Select D.ic_documento as  IC_DOCUMENTO  " +
								  " from com_documento D, comrel_producto_epo pe, comcat_epo E, com_solicitud S "+
								  " where  D.ic_epo = pe.ic_epo " +
								  " AND pe.ic_epo = E.ic_epo "+															
								 " AND pe.ic_producto_nafin = 1 ");
						
		          /*  if (!infoProcesadaEn.equals(""))//puede tener "",5,8,9
		              qrySentencia.append(" and exists (select 1"   +
                              "    from com_docto_seleccionado ds"   +
                              " 	 where d.ic_documento=ds.ic_documento"   +
                              " 	 and   ds.cc_acuse like ?) " ) ;
										
										*/

			
			if (!ic_epo.equals("")){
				qrySentencia .append(" and D.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			
			if (!ic_pyme.equals("") ) {
				qrySentencia.append(" and D.ic_pyme = ?");
				conditions.add(ic_pyme);
			}
			
			if (!ic_if.equals("") ) {
				qrySentencia.append( " and D.ic_if = ?");
				conditions.add(ic_if);
			}
			
			if (!ig_numero_docto.equals("")) {
				qrySentencia.append(" and D.ig_numero_docto = ?");
				conditions.add(ig_numero_docto);
			}
			
			if (!ic_estatus_docto.equals("")){
				if("4".equals(ic_estatus_docto)){
					qrySentencia.append( " and D.ic_estatus_docto in (?,?) ");
					conditions.add(new Integer(ic_estatus_docto));
					conditions.add(new Integer("11"));
				}else{
					qrySentencia.append( " and D.ic_estatus_docto = ? ");
					conditions.add(ic_estatus_docto);
				}
			}
			
			if (!ic_estatus_docto.equals("3")	&& !ic_estatus_docto.equals("24"))	{
				if (!df_fecha_seleccion_de.equals("")	&& !df_fecha_seleccion_a.equals("")){
					qrySentencia .append(" AND s.df_operacion BETWEEN (to_date(?,'dd/mm/yyyy'))AND (to_date(?,'dd/mm/yyyy')+1 )");
					conditions.add(df_fecha_seleccion_de);
					conditions.add(df_fecha_seleccion_a);
				}
				if (!df_fecha_autorizaif_de.equals("")	&& !df_fecha_autorizaif_a.equals("")){
					qrySentencia .append( " AND s.df_fecha_solicitud BETWEEN (to_date(?,'dd/mm/yyyy'))AND (to_date(?,'dd/mm/yyyy')+1) ");
					conditions.add(df_fecha_autorizaif_de);
					conditions.add(df_fecha_autorizaif_a);
				}
			}
			
			if( (df_fecha_seleccion_de.equals("") || df_fecha_seleccion_a.equals("")) && (df_fecha_autorizaif_de.equals("")||df_fecha_autorizaif_a.equals(""))  ){
				qrySentencia.append(" AND (( s.df_operacion>= TRUNC(SYSDATE)  AND s.DF_OPERACION< TRUNC(SYSDATE) + 1)");
				qrySentencia.append(" OR ");
				qrySentencia.append( " ( d.df_fecha_venc>= TRUNC(SYSDATE)  AND d.df_fecha_venc< TRUNC(SYSDATE) + 1 AND d.ic_estatus_docto = 4 )) ");
			}
			
			qrySentencia.append(" AND D.ic_documento = S.ic_documento(+)");

			/*int iIndex = qrySentencia.indexOf("where  and");
			if(iIndex > 0){
				String toWhere = qrySentencia.substring(0,qrySentencia.indexOf("where  and"));
				System.out.println(toWhere);
				qrySentencia .append(toWhere + "where" + qrySentencia.substring(qrySentencia.indexOf("where  and")+10,qrySentencia.length())) ;
			}
		*/
	
		log.debug("qrySentencia---------------> "+qrySentencia);
		log.debug("conditions----------------->"+conditions);
		
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer clavesDocumentos 	= new StringBuffer();		

		for(int i=0;i<pageIds.size();i++) {
				List lItem = (ArrayList)pageIds.get(i);
				clavesDocumentos.append( (new Long(lItem.get(0).toString()))+",");			
		}//for(int i=0;i<ids.size();i++)
		
		clavesDocumentos.deleteCharAt(clavesDocumentos.length()-1);
		
		qrySentencia.append(
				"  SELECT"   +
				"  	 /*+"   +
				"         use_nl(D,DS,S,CC1,CD,P,E,I,I2,ED,M)"   +
				"         index(D cp_com_documento_pk)"   +
				"         index(CD cp_fop_cobro_disposicion_pk) " +
				"         index(M cp_comcat_moneda_pk)"   +
				"         index(ED cp_comcat_estatus_docto_pk)"   +
				"      */  "   +
				" D.ic_documento AS IC_DOCUMENTO ," +
			   " E.cg_razon_social AS  NOMBRE_EPO, " +
				" p.in_numero_sirac AS NUM_SIRAC, " +
				" p.cg_razon_social AS  NOMBRE_PYME, "+
				" D.ig_numero_docto AS NUM_DOCUMENTO , " +
				" ED.cd_descripcion AS ESTATUS,  " +
				" d.ic_estatus_docto  as IC_ESTATUS,  "+
				" M.cd_nombre AS MONEDA, " +
				" D.fn_monto AS MONTO_DOCUMENTO , " +
				" D.fn_porc_anticipo AS PORCE_DESCUENTO ,  " +
				" '0' AS RECURSO_GARANTIA , " +
				" D.fn_monto_dscto AS MONTO_DESCONTAR , " +
				" decode(ds.CS_OPERA_FISO,'S', ds.IN_IMPORTE_INTERES_FONDEO , 'N', ds.in_importe_interes)  AS MONTO_INTERES, " +
				" decode(ds.CS_OPERA_FISO,'S', ds.IN_IMPORTE_RECIBIR_FONDEO, 'N', ds.in_importe_recibir)  AS MONTO_OPERAR, " +
				" '' AS SELECCIONAR ,   " +     
				" '' as DF_FECHA_DOCTO, " +
				" TO_CHAR (s.df_fecha_solicitud, 'DD/MM/YYYY') AS FECHA_SOLICITUD,  " +
				" ''  as IC_ESTATUS_NUEVO,  "+
				" ''  as ESTATUS_NUEVO , "+
				" ''  as ESTATUS_NUEVO_A , "+
				" decode(ds.CS_OPERA_FISO,'S', i.cg_razon_social, 'N', i.cg_razon_social)  as NOMBRE_IF , "+				
				" decode(ds.CS_OPERA_FISO,'S', i3.cg_razon_social, 'N', 'N/A')  as NOMBRE_FIDEICOMISO,"+
				" decode(ds.CS_OPERA_FISO,'S', ds.in_importe_interes, 'N', '0')  as MONTO_INTERES_FONDEO,  "+
				" decode(ds.CS_OPERA_FISO,'S', ds.in_importe_recibir, 'N', '0')  as MONTO_OPERAR_FONDEO  "+				
		"  FROM comcat_epo E,"   +
				"  	comcat_pyme P,"   +
				"  	comcat_if I,"   +
				"  	comcat_if I2,"   +
				"  	comcat_if I3,"   +
				"  	com_documento D,"   +
				"  	comcat_moneda M,"   +
				"  	com_docto_seleccionado DS,"   +
				"  	comcat_estatus_docto ED,"   +
				"  	com_solicitud S,"   +
				"  	(SELECT /*+index(cc cp_com_cobro_credito_pk)*/"   +
				"     cc.ic_documento, cc.fn_porc_docto_aplicado ,"   +
				"  	SUM(cc.fn_monto_pago) AS fn_monto_pago"   +
				"  	FROM com_cobro_credito cc"   +
				" 	where cc.ic_documento in (" + clavesDocumentos.toString() + ") " +
				"  	GROUP BY cc.ic_documento, cc.fn_porc_docto_aplicado) cc1,"   +
				"  	fop_cobro_disposicion cd"   +
				"  WHERE D.ic_epo = E.ic_epo"   +
				"  	AND D.ic_pyme = P.ic_pyme"   +
				"  	AND D.ic_moneda = M.ic_moneda"   +
				"  	AND D.ic_documento = DS.ic_documento"   +
				"  	AND D.ic_estatus_docto = ED.ic_estatus_docto"   +
				"  	AND D.ic_documento = S.ic_documento(+)"   +
				"  	AND D.ic_if = I.ic_if(+)"   +
				"  	AND D.ic_beneficiario = I2.ic_if (+)"   +
				"  	AND DS.ic_documento = CC1.ic_documento(+)"   +
				"  	AND DS.ic_documento = CD.ic_documento(+)"   +					
				"     and ds.ic_if = i3.ic_if " +
           " 	and D.ic_documento in (" + clavesDocumentos.toString() + ") ");

			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int i=0;
		try {

		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
	}	
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int i=0;
			
		try {

	
	} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		log.debug("crearCustomFile (S)");
		return nombreArchivo;
	}	


	/**
	 * Metodo apra realiar la busqueda de Pyme  
	 * @return 
	 * @param num_electronico
	 * @param ic_epo
	 */

	public List  busquedadatosPymes(String ic_epo, String num_electronico){
		
		List datos =new ArrayList();	
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer("");
		List lVarBind		= new ArrayList();	
		boolean transactionOk = true;
		
		try{	
			con.conexionDB();
		
			qrySentencia.append( " SELECT pym.ic_pyme, pym.cg_razon_social"   +
										"   FROM comrel_nafin crn, comrel_pyme_epo cpe, comcat_pyme pym , comcat_epo E"   +
										"  WHERE crn.ic_epo_pyme_if = cpe.ic_pyme"   +
										"    AND cpe.ic_pyme = pym.ic_pyme"   +
										"    AND crn.ic_nafin_electronico = ? "   +
										"    AND crn.cg_tipo = 'P'"   +
										"    AND cpe.cs_habilitado = 'S'"   +
										"    AND cpe.cg_pyme_epo_interno IS NOT NULL"  +
										"	 AND cpe.ic_epo = E.ic_epo " +										
										"    AND cpe.ic_epo = ? ");
								  
			
			lVarBind.add(num_electronico);			
			lVarBind.add(ic_epo);
			
			log.debug("qrySentencia  "+qrySentencia);
			log.debug("lVarBind  "+lVarBind);
						
			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();		
			while(rs.next()){
				String  ic_pyme = rs.getString(1)==null?"":rs.getString(1);
				String  txtNombre = rs.getString(2)==null?"":rs.getString(2);		
				datos.add(ic_pyme);
				datos.add(txtNombre);							
			} 
			rs.close();
			ps.close();
		
	} catch(Exception e) {
		transactionOk = false;
		e.printStackTrace();
	} finally {	
		if (con.hayConexionAbierta()) {
			con.terminaTransaccion(transactionOk);
			con.cierraConexionDB();
		}
	}
		return datos;
	}
	
	/**
	 *  Metodo para obtener los dias reales en la consulta 
	 * @return 
	 * @param fechaActual
	 * @param ic_documento
	 */
	public int  diasreales(String  ic_documento, String fechaActual ){
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer("");
		List lVarBind		= new ArrayList();		
		String estatusDocto ="";
		int diasreales = 0;
		boolean transactionOk = true;
		try{	
			con.conexionDB();
		
			qrySentencia.append( " SELECT d.ic_estatus_docto, to_char(decode(cs_fecha_venc_pyme,'N', (  d.df_fecha_venc " +
							"        - DECODE (rpe.ig_dias_minimo, " +
							"                  NULL, pn.in_dias_minimo, " +
							"                  rpe.ig_dias_minimo " +
							"                 ) " +
							"       ),'S',(  d.df_fecha_venc_pyme " +
							"        - DECODE (rpe.ig_dias_minimo, " +
							"                  NULL, pn.in_dias_minimo, " +
							"                  rpe.ig_dias_minimo " +
							"                 ) " +
							"       )),'DD/MM/YYYY') AS fechavenc "+
							"  FROM comcat_producto_nafin pn, comrel_producto_epo rpe, com_documento d " +
							" WHERE pn.ic_producto_nafin = rpe.ic_producto_nafin " +
							"   AND rpe.ic_epo = d.ic_epo " +
							"   AND pn.ic_producto_nafin = ? " +
							"   AND ic_documento = ? ");
				lVarBind.add(new Integer(1));
				lVarBind.add(ic_documento);
			
			log.debug("qrySentencia  "+qrySentencia);
			log.debug("lVarBind  "+lVarBind);
						
			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();		
			while(rs.next()){
				String fechaVencDiasMin = (rs.getString("fechavenc") != null) ? rs.getString("fechavenc") : "";
				//String fechaVencDiasMin = rs.getString("fechavenc");
				if(!fechaVencDiasMin.equals("")){
				diasreales = Fecha.restaFechas(fechaActual,fechaVencDiasMin);
				} else{
					log.warn("Ocurri� un error al obtener los d�as reales. Una de las variables es nula");
					log.warn("fechaActual: <" + fechaActual + ">");
					log.warn("fechaVencDiasMin: <" + fechaVencDiasMin + ">");
					diasreales = 0;
				}
			}
			rs.close();
			ps.close();
		
	} catch(Exception e) {
		transactionOk = false;
		e.printStackTrace();
	} finally {			
		if(con.hayConexionAbierta()) {
			con.terminaTransaccion(transactionOk);
			con.cierraConexionDB();	
		}
			
	}
		return diasreales;
}

	/**
	 * 
	 * @return 
	 * @param txtUploadPath
	 * @param nombreArchivo
	 * @param fileSize
	 * @param extension
	 * @param strNombreUsuario
	 * @param causasCancelacion
	 * @param newEstatusDocto
	 * @param ic_documento
	 */
	public String   cancelaDocto(String  ic_documento, String newEstatusDocto, String causaCancelacion , String strNombreUsuario, String extension, String fileSize , String nombreArchivo ,  String txtUploadPath  ){
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer("");
		String respuesta="Cancelacion_Error";
		boolean transactionOk = true;
		
		try{	
			con.conexionDB();
			
			java.io.File file = new File(txtUploadPath+"/"+nombreArchivo);
		//PASO 1 de 12
			qrySentencia = new StringBuffer("");
			qrySentencia.append(" insert into comhis_cambio_estatus  " +
									" (DC_FECHA_CAMBIO, IC_DOCUMENTO, IC_CAMBIO_ESTATUS, CT_CAMBIO_MOTIVO, " +
									" DF_OPERACION, BI_AUTORIZACION, CG_NOMBRE_USUARIO, CG_EXTENSION) values( " +
									" SYSDATE, ? , ?, ?, " +
									" (select DF_FECHA_SOLICITUD from com_solicitud WHERE ic_documento = ?),?,?,?)  ");
			
				System.out.println("==========>> strSQL = "+qrySentencia);
				
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.setInt(2, Integer.parseInt(newEstatusDocto));			
				ps.setString(3, causaCancelacion);	
				ps.setInt(4, Integer.parseInt(ic_documento));
				ps.setBinaryStream(5, new FileInputStream(file), Integer.parseInt(fileSize));
				ps.setString(6, strNombreUsuario);
				ps.setString(7, extension);	
				ps.executeUpdate();
				ps.close();
				
				//PASO 2 de 12	
				qrySentencia = new StringBuffer("");
				qrySentencia.append("DELETE com_amortizacion   WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) ");	
				System.out.println("==========>> strSQL = "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();
				
				//PASO 3 de 12	
				qrySentencia = new StringBuffer("");
				qrySentencia.append(" DELETE com_control05   WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) ");	
				System.out.println("==========>> strSQL = "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();
				
				//PASO 4 de 12	
				qrySentencia = new StringBuffer("");
				qrySentencia.append("DELETE com_control04             WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) ");	
				System.out.println("==========>> strSQL = "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();	
				
				//PASO 5 de 12	
				qrySentencia = new StringBuffer("");
				qrySentencia.append(" DELETE com_control03             WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) ");	
				System.out.println("==========>> strSQL = "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();	
				
				//PASO 6 de 12	
				qrySentencia = new StringBuffer("");
				qrySentencia.append(" DELETE com_control02             WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) ");	
				System.out.println("==========>> strSQL = "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();		
				
				//PASO 7 de 12	
				qrySentencia = new StringBuffer("");
				qrySentencia.append(" DELETE com_control01             WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) ");	
				System.out.println("==========>> strSQL = "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();	
			
				//PASO 8 de 12	
				qrySentencia = new StringBuffer("");
				qrySentencia.append("DELETE com_solicitud             WHERE ic_documento = ? ");	
				System.out.println("==========>> strSQL = "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();
				
				//PASO 9 de 12	
				qrySentencia = new StringBuffer("");
				qrySentencia.append("DELETE com_cobro_credito       	 WHERE ic_documento = ? ");	
				System.out.println("==========>> strSQL = "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();	
				
				//PASO 10 de 12	
				qrySentencia = new StringBuffer("");
				qrySentencia.append(" UPDATE com_docto_seleccionado SET in_importe_recibir = NULL WHERE ic_documento = ? ");	
				System.out.println("==========>> strSQL = "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();	
				
				//31 - 2
				//42 - 9
				//43 - 10
				//PASO 11 de 12	
				qrySentencia = new StringBuffer("");
				qrySentencia.append(" UPDATE com_documento set ic_estatus_docto = 3 WHERE ic_documento = ? ");	
				System.out.println("==========>> strSQL = "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();		
				
				//PASO 12 de 12	
				qrySentencia = new StringBuffer("");
				qrySentencia.append(" UPDATE com_documento set ic_estatus_docto = 2 WHERE ic_documento = ? ");	
				System.out.println("==========>> strSQL = "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());	
				ps.setInt(1, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				ps.close();	
				
				if(newEstatusDocto.equals("42") || newEstatusDocto.equals("43")){
					qrySentencia = new StringBuffer("");
					qrySentencia.append(" UPDATE com_documento set ic_estatus_docto = ? WHERE ic_documento = ? ");	
					System.out.println("==========>> strSQL = "+qrySentencia.toString());
					ps = con.queryPrecompilado(qrySentencia.toString());		
					ps.setInt(1, Integer.parseInt(newEstatusDocto)==42?9:10);
					ps.setInt(2, Integer.parseInt(ic_documento));
					ps.executeUpdate();
					ps.close();
				}
				respuesta="Cancelacion_Exitosa";
		
	} catch(Exception e) {
		transactionOk=false;
		respuesta="Cancelacion_Error";
		e.printStackTrace();
		log.error("error al Cancelaci�n de documentos  " +e); 
	} finally {	
		if(con.hayConexionAbierta()) {
			con.terminaTransaccion(transactionOk);
			con.cierraConexionDB();	
		}
	}    
		return respuesta;
	}

    public String   cancelaDocumentos(String []  ic_docs_ic_est_nue, String newEstatusDocto, String causaCancelacion , String strNombreUsuario, String extension, String fileSize , String nombreArchivo ,  String txtUploadPath  ){
            PreparedStatement       ps                              = null;
            ResultSet                       rs                              = null;
            AccesoDB con =new AccesoDB();
            StringBuffer qrySentencia = new StringBuffer("");
            String respuesta="Cancelacion_Error";
            boolean transactionOk = true;
            
            try{    
                    con.conexionDB();
                    
                    java.io.File file = new File(txtUploadPath+"/"+nombreArchivo);
            for (String tmpIcDocIcEsta: ic_docs_ic_est_nue){
                StringTokenizer stTok = new StringTokenizer(tmpIcDocIcEsta, "|");
                while (stTok.hasMoreElements()) {
                    ic_documento=stTok.nextElement().toString();
                    newEstatusDocto=stTok.nextElement().toString();
                }
            //PASO 1 de 12
                    qrySentencia = new StringBuffer("");
                    qrySentencia.append(" insert into comhis_cambio_estatus  " +
                                                                    " (DC_FECHA_CAMBIO, IC_DOCUMENTO, IC_CAMBIO_ESTATUS, CT_CAMBIO_MOTIVO, " +
                                                                    " DF_OPERACION, BI_AUTORIZACION, CG_NOMBRE_USUARIO, CG_EXTENSION) values( " +
                                                                    " SYSDATE, ? , ?, ?, " +
                                                                    " (select DF_FECHA_SOLICITUD from com_solicitud WHERE ic_documento = ?),?,?,?)  ");
                    
                            System.out.println("==========>> strSQL = "+qrySentencia);
                            
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.setInt(2, Integer.parseInt(newEstatusDocto));                        
                            ps.setString(3, causaCancelacion);      
                            ps.setInt(4, Integer.parseInt(ic_documento));
                            ps.setBinaryStream(5, new FileInputStream(file), Integer.parseInt(fileSize));
                            ps.setString(6, strNombreUsuario);
                            ps.setString(7, extension);     
                            ps.executeUpdate();
                            ps.close();
                            
                            //PASO 2 de 12  
                            qrySentencia = new StringBuffer("");
                            qrySentencia.append("DELETE com_amortizacion   WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) "); 
                            System.out.println("==========>> strSQL = "+qrySentencia.toString());
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.executeUpdate();
                            ps.close();
                            
                            //PASO 3 de 12  
                            qrySentencia = new StringBuffer("");
                            qrySentencia.append(" DELETE com_control05   WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) ");   
                            System.out.println("==========>> strSQL = "+qrySentencia.toString());
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.executeUpdate();
                            ps.close();
                            
                            //PASO 4 de 12  
                            qrySentencia = new StringBuffer("");
                            qrySentencia.append("DELETE com_control04             WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) ");  
                            System.out.println("==========>> strSQL = "+qrySentencia.toString());
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.executeUpdate();
                            ps.close();     
                            
                            //PASO 5 de 12  
                            qrySentencia = new StringBuffer("");
                            qrySentencia.append(" DELETE com_control03             WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) "); 
                            System.out.println("==========>> strSQL = "+qrySentencia.toString());
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.executeUpdate();
                            ps.close();     
                            
                            //PASO 6 de 12  
                            qrySentencia = new StringBuffer("");
                            qrySentencia.append(" DELETE com_control02             WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) "); 
                            System.out.println("==========>> strSQL = "+qrySentencia.toString());
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.executeUpdate();
                            ps.close();             
                            
                            //PASO 7 de 12  
                            qrySentencia = new StringBuffer("");
                            qrySentencia.append(" DELETE com_control01             WHERE ic_folio = (select ic_folio from com_solicitud WHERE ic_documento = ?) "); 
                            System.out.println("==========>> strSQL = "+qrySentencia.toString());
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.executeUpdate();
                            ps.close();     
                    
                            //PASO 8 de 12  
                            qrySentencia = new StringBuffer("");
                            qrySentencia.append("DELETE com_solicitud             WHERE ic_documento = ? ");        
                            System.out.println("==========>> strSQL = "+qrySentencia.toString());
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.executeUpdate();
                            ps.close();
                            
                            //PASO 9 de 12  
                            qrySentencia = new StringBuffer("");
                            qrySentencia.append("DELETE com_cobro_credito            WHERE ic_documento = ? ");     
                            System.out.println("==========>> strSQL = "+qrySentencia.toString());
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.executeUpdate();
                            ps.close();     
                            
                            //PASO 10 de 12 
                            qrySentencia = new StringBuffer("");
                            qrySentencia.append(" UPDATE com_docto_seleccionado SET in_importe_recibir = NULL WHERE ic_documento = ? ");    
                            System.out.println("==========>> strSQL = "+qrySentencia.toString());
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.executeUpdate();
                            ps.close();     
                            
                            //31 - 2
                            //42 - 9
                            //43 - 10
                            //PASO 11 de 12 
                            qrySentencia = new StringBuffer("");
                            qrySentencia.append(" UPDATE com_documento set ic_estatus_docto = 3 WHERE ic_documento = ? ");  
                            System.out.println("==========>> strSQL = "+qrySentencia.toString());
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.executeUpdate();
                            ps.close();             
                            
                            //PASO 12 de 12 
                            qrySentencia = new StringBuffer("");
                            qrySentencia.append(" UPDATE com_documento set ic_estatus_docto = 2 WHERE ic_documento = ? ");  
                            System.out.println("==========>> strSQL = "+qrySentencia.toString());
                            ps = con.queryPrecompilado(qrySentencia.toString());    
                            ps.setInt(1, Integer.parseInt(ic_documento));
                            ps.executeUpdate();
                            ps.close();     
                            
                            if(newEstatusDocto.equals("42") || newEstatusDocto.equals("43")){
                                    qrySentencia = new StringBuffer("");
                                    qrySentencia.append(" UPDATE com_documento set ic_estatus_docto = ? WHERE ic_documento = ? ");  
                                    System.out.println("==========>> strSQL = "+qrySentencia.toString());
                                    ps = con.queryPrecompilado(qrySentencia.toString());            
                                    ps.setInt(1, Integer.parseInt(newEstatusDocto)==42?9:10);
                                    ps.setInt(2, Integer.parseInt(ic_documento));
                                    ps.executeUpdate();
                                    ps.close();
                            }
                respuesta="Cancelacion_Exitosa";
            }
            
    } catch(Exception e) {
            transactionOk=false;
            respuesta="Cancelacion_Error";
            e.printStackTrace();
            log.error("error al Cancelaci�n de documentos  " +e); 
    } finally {     
            if(con.hayConexionAbierta()) {
                    con.terminaTransaccion(transactionOk);
                    con.cierraConexionDB(); 
            }
    }    
            return respuesta;
    }

	
	/**
	 *  Metodo para la impresion del archivo PDF en el acuse
	 * @return 
	 * @param path
	 * @param datos
	 * @param request  
	 */
	public String crearArchivoPDF(  HttpServletRequest request, List datos, String path, String ic_folio) {
		log.debug("crearArchivoPDF (E)");		
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		try {


			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.addText("  ","formas",ComunesPDF.CENTER);
				pdfDoc.addText("  ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" La autentificaci�n se llev� a cabo con �xito  "+ic_folio,"formasB",ComunesPDF.CENTER);
				pdfDoc.addText("  ","formas",ComunesPDF.CENTER);
				pdfDoc.addText("  ","formas",ComunesPDF.CENTER);
			
				pdfDoc.setTable(16, 100);
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Num Sirac","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Documento","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);			
				pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre IF ","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Intereses del documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Recibir IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Interes del docto. FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Recibir FIDEICOMISO","celda01",ComunesPDF.CENTER);	
				

                                Iterator datosIterator = datos.iterator();
                                while(datosIterator.hasNext()){
                                    ArrayList elemento = (ArrayList)datosIterator.next();
                                    pdfDoc.setCell(elemento.get(0).toString(),"formas",ComunesPDF.LEFT);
                                    pdfDoc.setCell(elemento.get(1).toString(),"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(elemento.get(2).toString(),"formas",ComunesPDF.LEFT);
                                    pdfDoc.setCell(elemento.get(3).toString(),"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(elemento.get(4).toString(),"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(elemento.get(5).toString(),"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell("$"+Comunes.formatoDecimal(elemento.get(6).toString(),2) ,"formas",ComunesPDF.RIGHT);
                                    pdfDoc.setCell(elemento.get(7).toString()+"%","formas",ComunesPDF.CENTER);
                                    
                                    pdfDoc.setCell("$"+Comunes.formatoDecimal(elemento.get(8).toString(),2) ,"formas",ComunesPDF.RIGHT);
                                    pdfDoc.setCell("$"+Comunes.formatoDecimal(elemento.get(9).toString(),2) ,"formas",ComunesPDF.RIGHT);
                                    pdfDoc.setCell(elemento.get(16).toString() ,"formas",ComunesPDF.RIGHT);
                                    pdfDoc.setCell("$"+Comunes.formatoDecimal(elemento.get(10).toString(),2) ,"formas",ComunesPDF.RIGHT);
                                    pdfDoc.setCell("$"+Comunes.formatoDecimal(elemento.get(11).toString(),2) ,"formas",ComunesPDF.RIGHT);
                              
                                    pdfDoc.setCell(elemento.get(13).toString() ,"formas",ComunesPDF.LEFT);
                                    if(!elemento.get(14).toString().equals("0")) {
                                            pdfDoc.setCell("$"+Comunes.formatoDecimal(elemento.get(14).toString(),2) ,"formas",ComunesPDF.RIGHT);
                                    }else {
                                            pdfDoc.setCell("N/A" ,"formas",ComunesPDF.CENTER);					
                                    }
                                    if(!elemento.get(15).toString().equals("0")) {
                                            pdfDoc.setCell("$"+Comunes.formatoDecimal(elemento.get(15).toString(),2) ,"formas",ComunesPDF.RIGHT);
                                    }else {
                                            pdfDoc.setCell("N/A" ,"formas",ComunesPDF.CENTER);					
                                    }
                                }			
			  
			
				pdfDoc.addTable();
				pdfDoc.endDocument();
				
	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {				
			} catch(Exception e) {}
		  
		}
		log.debug("crearArchivoPDF (S)");
		return nombreArchivo;
	}	
	
	
	
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}	

	public String getIc_banco_fondeo() {
		return ic_banco_fondeo;
	}

	public void setIc_banco_fondeo(String ic_banco_fondeo) {
		this.ic_banco_fondeo = ic_banco_fondeo;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getDf_fecha_seleccion_de() {
		return df_fecha_seleccion_de;
	}

	public void setDf_fecha_seleccion_de(String df_fecha_seleccion_de) {
		this.df_fecha_seleccion_de = df_fecha_seleccion_de;
	}

	public String getDf_fecha_seleccion_a() {
		return df_fecha_seleccion_a;
	}

	public void setDf_fecha_seleccion_a(String df_fecha_seleccion_a) {
		this.df_fecha_seleccion_a = df_fecha_seleccion_a;
	}

	public String getDf_fecha_autorizaif_de() {
		return df_fecha_autorizaif_de;
	}

	public void setDf_fecha_autorizaif_de(String df_fecha_autorizaif_de) {
		this.df_fecha_autorizaif_de = df_fecha_autorizaif_de;
	}

	public String getDf_fecha_autorizaif_a() {
		return df_fecha_autorizaif_a;
	}

	public void setDf_fecha_autorizaif_a(String df_fecha_autorizaif_a) {
		this.df_fecha_autorizaif_a = df_fecha_autorizaif_a;
	}

	public String getIg_numero_docto() {
		return ig_numero_docto;
	}

	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getInfoProcesadaEn() {
		return infoProcesadaEn;
	}

	public void setInfoProcesadaEn(String infoProcesadaEn) {
		this.infoProcesadaEn = infoProcesadaEn;
	}

	public String getNumero_siaff() {
		return numero_siaff;
	}

	public void setNumero_siaff(String numero_siaff) {
		this.numero_siaff = numero_siaff;
	}

	public String getEstaHabilitadoNumeroSIAFF() {
		return estaHabilitadoNumeroSIAFF;
	}

	public void setEstaHabilitadoNumeroSIAFF(String estaHabilitadoNumeroSIAFF) {
		this.estaHabilitadoNumeroSIAFF = estaHabilitadoNumeroSIAFF;
	}

	public String getIc_documento() {
		return ic_documento;
	}

	public void setIc_documento(String ic_documento) {
		this.ic_documento = ic_documento;
	}

	public String getIc_estatus_docto() {
		return ic_estatus_docto;
	}

	public void setIc_estatus_docto(String ic_estatus_docto) {
		this.ic_estatus_docto = ic_estatus_docto;
	}
}



