package com.netro.descuento;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CatalogoErroresWSIF{
  
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoErroresWSIF.class);
	
	/**
	  * 
	  * Procesa la Carga Masiva de Descripciones de Error y posteriormente 
	  * lo almacena de manera temporal (COMTMP_ERROR_IF)
	  *
	  * @param strDirectorio Ruta del archivo
	  * @param esNombreArchivo Nombre del archivo
	  *
	  * @return Clase <tt>ResultadosValidacionCatalogo</tt> con los resultados
	  *         de la Validacion.
	  *
	  */
	 public ResultadosValidacionCatalogo procesarCargaMasiva(
			String strDirectorio,
			String esNombreArchivo,
			String claveIF
		)throws AppException{
	 
			log.info("procesarCargaMasiva(E)");

			String _fechaMsg = (new SimpleDateFormat ("yyyyMMdd_HHmmss")).format(new java.util.Date());

			ResultadosValidacionCatalogo 	validacion 		= new ResultadosValidacionCatalogo();
			AccesoDB 							con				= new AccesoDB();
			PreparedStatement 				ps					= null;
			ResultSet		  					rs 				= null;
			StringBuffer 						qrySentencia	= null;
			int   								NUM_CAMPOS		= 2;
			boolean								bOk				= true;
			
			try{
				
				// VALIDAR LA EXTENSION DEL ARCHIVO
				String extension = esNombreArchivo.substring(esNombreArchivo.length()-3,esNombreArchivo.length());
				if(!"txt".equalsIgnoreCase(extension)){
					validacion.appendErrores("Error: El archivo comprimido no es txt\n");
					// TODO: CODIGO RETURN
				}
 
				// ABRIR ARCHIVO CARGADO
				java.io.File 		lofArchivo 				= new java.io.File(esNombreArchivo);
				BufferedReader 	br 						= new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo)));
							
				// OBTENER ID DEL PROCESO DE CARGA
				con.conexionDB();
				int idProceso = 0;
				qrySentencia  = new StringBuffer();
				qrySentencia.append(" select seq_comtmp_error_if.nextval as id from dual ");
				ps = con.queryPrecompilado(qrySentencia.toString());
				rs = ps.executeQuery();
				if(rs.next()){
					idProceso = rs.getInt("id");
				}
				rs.close();
				ps.close();
				validacion.setIcProceso(idProceso);

				// INSERTAR REGISTROS LEIDOS EN LA TABLA TEMPORAL COMTMP_TASAS
				qrySentencia = new StringBuffer();
				qrySentencia.append(
					"INSERT INTO                "  +
					"	COMTMP_ERROR_IF          "  +
					"	(	                      "  +
					"		IC_NUMERO_PROCESO,    "  +
					"		IC_NUMERO_LINEA,      "  +
					"		CC_ERROR_IF, 			 "  +
					"		CG_DESCRIPCION        "  +
					"	)                        "  +
					"  VALUES                   "  +
					"	(                        "  +
					"		?,                    "  +
					"		?,                    "  +
					"		?,                    "  +
					"		?                     "  +
					"	) "
				);
				ps = con.queryPrecompilado(qrySentencia.toString());
				int 			lineas			= 0;
				int 			numLinea			= 1;
				String 		lsLinea 			= "";
				String [] 	campos			= null;
				int 			iNumRegTrans 	= 0;
				if(validacion.getErrores() == null || validacion.getErrores().length()==0){
					for (lineas = 0,numLinea = 1; (lsLinea = br.readLine()) != null; lineas++,numLinea++) {
 
						validacion.setErrorLinea(false);
						lsLinea = lsLinea.trim();
						System.out.println("linea["+numLinea+"]: " + lsLinea);	
						// VALIDAR EL NUMERO DE CAMPOS
						try{
	
							campos 		= lsLinea.split("\\|",-1);
							if( campos.length == 0){
								System.out.println(numLinea+","+(campos.length == 0?"<>":campos[0]));
								validacion.agregaRegistro(numLinea,"");
								validacion.appendErrores(numLinea,"","El n&uacute;mero de campos es incorrecto: "+campos.length+".");
								validacion.setErrorLinea(true);
								validacion.incNumRegErr();
								continue;
							} else if( campos.length < NUM_CAMPOS ){
								System.out.println(numLinea+","+campos[0]);
								validacion.agregaRegistro(numLinea,campos[0]);
								validacion.appendErrores(numLinea,"","El n&uacute;mero de campos es incorrecto: "+campos.length+".");
								validacion.setErrorLinea(true);
								validacion.incNumRegErr();
								continue;
							}
							
						}catch(Exception e){
							System.out.println(numLinea+","+(campos.length == 0?"":campos[0]));
							e.printStackTrace();
							validacion.agregaRegistro(numLinea,(campos.length == 0?"":campos[0]));
							validacion.appendErrores(numLinea,"","El n&uacute;mero de campos es incorrecto: "+campos.length+".");
							validacion.setErrorLinea(true);
							validacion.incNumRegErr();
							continue;
						}
 
						// VALIDAR CAMPO 1. CLAVE DEL ERROR
						boolean 	claveValida 	 	= true;
						int 		longMaximaClave  	= 8;
						String 	clave 				= campos[0];
						// Acondicionar cadena
						clave = (clave.length() > longMaximaClave)?clave.substring(0,longMaximaClave+1):clave;
						// Agregar Registro a la lista de Validaciones
						validacion.agregaRegistro(numLinea,clave);
						// Validar Longitud del Clave
						if(clave.length() > longMaximaClave){
							validacion.appendErrores(numLinea,"1","La clave a dar de alta es mayor a la requerida (8 caracteres).");
							validacion.setErrorLinea(true);
							claveValida = false;
						}
						
						// Validar que la Clave del Error tenga el formato correcto
						if(claveValida){
							if( clave.length() == 0 || clave.trim().equals("") ){
								validacion.appendErrores(numLinea,"1","La clave es un campo obligatorio.");
								validacion.setErrorLinea(true);
								claveValida = false;
							}else if(!Comunes.esAlfanumerico(clave)){ 
								validacion.appendErrores(numLinea,"1","La clave a dar de alta debe ser alfanumérica.");
								validacion.setErrorLinea(true);
								claveValida = false;
							}
							
						}
						// Verificar que la Clave del Error cargada no exista en el Catalogo de Errores del IF
						if(claveValida && existeClaveEnElCatalogoWSIF(clave,claveIF)){
							validacion.appendErrores(numLinea,"1","La clave de error a dar de alta ya se encuentra registrada.");
							validacion.setErrorLinea(true);
							claveValida = false;
						}
 
						// VALIDAR CAMPO 2. DESCRIPCION
						boolean 	descripcionValida 		= true;
						int		longMaximaDescripcion 	= 255;
						String 	descripcion 				= campos[1];
						// Acondicionar Cadena
						descripcion = (descripcion.length() > longMaximaDescripcion)?descripcion.substring(0,longMaximaDescripcion+1):descripcion;
						// Validar la longitud del Estatus
						if(descripcion.length() > longMaximaDescripcion){
							validacion.appendErrores(numLinea,"2","La descripción del error es mayor a la requerida (255 caracteres).");
							validacion.setErrorLinea(true);
							descripcionValida = false;
						}
						// Validar que la Descripcion del Error tenga el formato correcto
						if(descripcionValida){
							if( descripcion.length() == 0 || descripcion.trim().equals("") ){
								validacion.appendErrores(numLinea,"2","La descripción es un campo obligatorio.");
								validacion.setErrorLinea(true);
								descripcionValida = false;
							}else if(!Comunes.esAlfanumerico(descripcion," ")){ 
								validacion.appendErrores(numLinea,"2","La descripción a dar de alta debe ser alfanumérica.");
								validacion.setErrorLinea(true);
								descripcionValida = false;
							}
						}
						// Verificar que la Descripcion del Error a dar de alta no exista en el Catalogo de Errores del IF
						if(descripcionValida && existeDescripcionEnElCatalogoWSIF(descripcion,claveIF)){
							validacion.appendErrores(numLinea,"2","La descripción del error a dar de alta ya se encuentra registrada.");
							validacion.setErrorLinea(true);
							descripcionValida = false;
						}
						
						// ACTUALIZAR CONTEO DE REGISTROS CON Y SIN ERROR
						if(validacion.getErrorLinea()){
							validacion.incNumRegErr();
						}else{
							validacion.incNumRegOk();
						}
											
						// INSERTAR EN LA TABLA TEMPORAL EL CONTENIDO DEL REGISTRO	
						ps.setInt(1,    idProceso);
						ps.setInt(2,    numLinea);
						ps.setString(3, clave);
						ps.setString(4, descripcion);
						ps.execute();
						if(iNumRegTrans>=3500){
							con.terminaTransaccion(true);
							iNumRegTrans = 0;
							System.gc();	//Invocacion al garbage collector para "liberar" memoria
						}
						
						iNumRegTrans++;
						
					} // for
				}
				con.terminaTransaccion(true);
				ps.close();

				// BUSCAR CLAVES REPETIDAS
				qrySentencia = new StringBuffer();
				qrySentencia.append(
					"SELECT 											"  + 
					"	CLAVE											"  +
					"FROM 											"  +
					"  (												"  +
					"		SELECT 									"  + 
					"			CC_ERROR_IF CLAVE,				"  +
					"			COUNT(1) REPETICIONES 			"  +
					"		FROM 										"  +
					"			COMTMP_ERROR_IF					"  +
					"		WHERE  									"  +
					"			IC_NUMERO_PROCESO = ? 			"  +
					"		GROUP BY  								"  +
					"			IC_NUMERO_PROCESO,CC_ERROR_IF	"  +
					" ) A 											"  +
					" WHERE  										"  +
					"	A.REPETICIONES > 1					");
				System.out.println(qrySentencia);
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setInt(1,idProceso);
				rs = ps.executeQuery();
				while(rs.next()){
					validacion.appendErrorClaveRepetida(rs.getString("CLAVE")," La Clave del Error esta Repetida.");
				}
				rs.close();
				ps.close();
				
				// BUSCAR DESCRIPCIONES REPETIDAS
				qrySentencia = new StringBuffer();
				qrySentencia.append(
					"SELECT DISTINCT									"  + 
					"	CC_ERROR_IF AS CLAVE							"  +
					"FROM 												"  +
					"	COMTMP_ERROR_IF ERROR, 						"  +
					"  (													"  +
					"		SELECT 										"  + 
					"			CG_DESCRIPCION DESCRIPCION,		"  +
					"			COUNT(1) REPETICIONES 				"  +
					"		FROM 											"  +
					"			COMTMP_ERROR_IF						"  +
					"		WHERE  										"  +
					"			IC_NUMERO_PROCESO = ? 				"  +
					"		GROUP BY  									"  +
					"			IC_NUMERO_PROCESO,CG_DESCRIPCION	"  +
					" ) A 												"  +
					" WHERE  											"  +
					"  ERROR.IC_NUMERO_PROCESO = ? 				   AND "  +
					"  ERROR.CG_DESCRIPCION    = A.DESCRIPCION   AND "  +
					"	A.REPETICIONES          > ?	         "
					
				);
				System.out.println(qrySentencia);
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setInt(1,idProceso);
				ps.setInt(2,idProceso);
				ps.setInt(3,1);
				rs = ps.executeQuery();
				while(rs.next()){
					validacion.appendErrorDescripcionRepetida(rs.getString("CLAVE")," La Descripcion del Error esta Repetida.");
				}
				rs.close();
				ps.close();
				

			}catch(Exception e){
				log.error("procesarCargaMasiva(Exception)");
				e.printStackTrace();
				bOk = false;
				throw new AppException("Error en la Validacion de la Carga Masiva");
			}finally{
				if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
				if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
				if(con.hayConexionAbierta()){
					con.terminaTransaccion(bOk);
					con.cierraConexionDB();
				}
				log.info("procesarCargaMasiva(S)");
			}
			return validacion;
	}
	
	
	private boolean existeClaveEnElCatalogoWSIF(String clave,String claveIF){

		log.info("existeClaveEnElCatalogoWSIF(E)");
		
		AccesoDB 				con					= new AccesoDB();
    	PreparedStatement 	ps 					= null;
    	ResultSet 				rs 					= null;
		StringBuffer        	query 				= new StringBuffer();
		
		boolean					existeClave			= false;
		String 					valor					= null; 
 
		if(clave 		== null || clave.trim().equals("")) 		{ log.info("existeClaveEnElCatalogoWSIF(S)"); return existeClave; }
		if(claveIF 		== null || claveIF.trim().equals("")) 		{ log.info("existeClaveEnElCatalogoWSIF(S)"); return existeClave; }
		
		try {
			
			con.conexionDB();
			
			query.append(
				"SELECT 																	"  + 
				"	DECODE(COUNT(1),0,'false','true') AS EXISTE 				"  + 
				"FROM 																	"  +
				"  COM_ERROR_IF  				ERROR									"  +
				"WHERE  																	"  +
				"  ERROR.IC_IF        	= ?           	AND 					"  + // claveIF
				"  ERROR.CC_ERROR_IF		= ?										"    // Clave del Error
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, claveIF);
			ps.setString(2, clave);
			rs = ps.executeQuery();
 
			if(rs != null && rs.next()){
				valor 			= rs.getString("EXISTE");
				existeClave 	= (valor != null && valor.equals("true"))?true:false;	
			}
			
		}catch(Exception e){
			log.debug("existeClaveEnElCatalogoWSIF(Exception)");
			log.debug("existeClaveEnElCatalogoWSIF.clave    	= <"+clave+">");
			log.debug("existeClaveEnElCatalogoWSIF.claveIF    	= <"+claveIF+">");
			e.printStackTrace();
			throw new AppException("Error al revisar la Clave del Error especificada ya se encuentra parametrizada con un IF proporcionado");
		}finally{
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("existeClaveEnElCatalogoWSIF(S)");
		}
		
		return existeClave;
	}
	
	private boolean existeDescripcionEnElCatalogoWSIF(String descripcion,String claveIF){

		log.info("existeDescripcionEnElCatalogoWSIF(E)");
		
		AccesoDB 				con					= new AccesoDB();
    	PreparedStatement 	ps 					= null;
    	ResultSet 				rs 					= null;
		StringBuffer        	query 				= new StringBuffer();
		
		boolean					existeDescripcion	= false;
		String 					valor					= null; 
 
		if(descripcion == null || descripcion.trim().equals("")) { log.info("existeDescripcionEnElCatalogoWSIF(S)"); return existeDescripcion; }
		if(claveIF 		== null || claveIF.trim().equals("")) 		{ log.info("existeDescripcionEnElCatalogoWSIF(S)"); return existeDescripcion; }
		
		try {
			
			con.conexionDB();
			
			query.append(
				"SELECT 																	"  + 
				"	DECODE(COUNT(1),0,'false','true') AS EXISTE 				"  + 
				"FROM 																	"  +
				"  COM_ERROR_IF  				ERROR									"  +
				"WHERE  																	"  +
				"  ERROR.IC_IF        	= ?           	AND 					"  + // claveIF
				"  ERROR.CG_DESCRIPCION	= ?										"    // Descripcion del Error
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, claveIF);
			ps.setString(2, descripcion);
			rs = ps.executeQuery();
 
			if(rs != null && rs.next()){
				valor 					= rs.getString("EXISTE");
				existeDescripcion 	= (valor != null && valor.equals("true"))?true:false;	
			}
			
		}catch(Exception e){
			log.debug("existeDescripcionEnElCatalogoWSIF(Exception)");
			log.debug("existeDescripcionEnElCatalogoWSIF.descripcion  = <"+descripcion+">");
			log.debug("existeDescripcionEnElCatalogoWSIF.claveIF    	= <"+claveIF+">");
			e.printStackTrace();
			throw new AppException("Error al revisar si la Descripcion del Error ya se encuentra parametrizada con el IF indicado.");
		}finally{
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("existeDescripcionEnElCatalogoWSIF(S)");
		}
		
		return existeDescripcion;
	}
	
	public ArrayList getRegistrosValidos(ArrayList lineas, int idProceso) throws AppException{
			
		log.info("getRegistrosValidos(E)");
		
		AccesoDB 				con				= new AccesoDB();
    	PreparedStatement 	ps 				= null;
    	ResultSet 				rs 				= null;
		StringBuffer        	query 			= new StringBuffer();
		StringBuffer        	variablesBind 	= new StringBuffer();
		
		ArrayList				registros		= new ArrayList();
		
		if(lineas == null || lineas.size() == 0){
			log.info("getRegistrosValidos(S)");
			return registros;
		}	
		
		try {
			
			con.conexionDB();
			
			for(int i=0;i<lineas.size();i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");
			}
			
			query.append(
				"SELECT                        							"  + 
				"  IC_NUMERO_LINEA,            							"  +
				"  CC_ERROR_IF,                     					"  +
				"  CG_DESCRIPCION       									"  +
				"FROM                          							"  + 
				"  COMTMP_ERROR_IF                                	"  +
				"WHERE                                          	"  + 
				"  IC_NUMERO_PROCESO        =   ?               	"  +
				"  AND IC_NUMERO_LINEA      IN ("+variablesBind.toString()+")	"  +
				"ORDER BY IC_NUMERO_LINEA      							"		
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,idProceso);
			for(int i=0;i<lineas.size();i++){
				ps.setString(i+2, (String)lineas.get(i));	
			}			
			rs = ps.executeQuery();
 
			while(rs != null && rs.next()){
				
				HashMap registro = new HashMap();
				String valor = rs.getString("IC_NUMERO_LINEA");
				registro.put("LINEA", 			(valor == null)?"":valor);
				valor = rs.getString("CC_ERROR_IF");
				registro.put("CLAVE", 			(valor == null)?"":valor);
				valor = rs.getString("CG_DESCRIPCION");
				registro.put("DESCRIPCION", 	(valor == null)?"":valor);				
				registros.add(registro);
				
			}
			
		}catch(Exception e){
			log.debug("getRegistrosValidos(Exception)");
			log.debug("getRegistrosValidos.lineas    = <"+lineas+">");
			log.debug("getRegistrosValidos.idProceso = <"+idProceso+">");
			e.printStackTrace();
			throw new AppException("getRegistrosValidos(Exception)");
		}finally{
			if (rs 	!= null) 										try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("getRegistrosValidos(S)");
		}
				
		return registros;	
	}
	
	public boolean insertarRegistros(String claveIF,String lstNumerosLineaOk,String icProceso){

		log.info("insertarRegistros(E)");
		
		AccesoDB 				con				= new AccesoDB();
    	PreparedStatement 	ps 				= null;
    	ResultSet 				rs 				= null;
		StringBuffer        	query 			= new StringBuffer();
		StringBuffer        	variablesBind 	= new StringBuffer();
		
		boolean					operacionExitosa	= true;
		
		if(claveIF 				== null 	|| claveIF.trim().equals("")				)	{ log.info("insertarRegistros(S)"); return false; }
		if(lstNumerosLineaOk == null 	|| lstNumerosLineaOk.trim().equals("")	)	{ log.info("insertarRegistros(S)"); return false; }
		if(icProceso 			== null 	|| icProceso.trim().equals("")			)	{ log.info("insertarRegistros(S)"); return false; }
 
		String[] lineas = lstNumerosLineaOk.split(",");
		
		try {
			
			con.conexionDB();
			
			for(int i=0;i<lineas.length;i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");
			}
			
			query.append(
				"INSERT INTO COM_ERROR_IF									"  +
				"	(																"  +
				"		IC_IF,													"  +
				"		CC_ERROR_IF,                                 "  +
				"		CG_DESCRIPCION											"  +
				"	)																"  +
				"SELECT                        							"  + 
				"  ?,            												"  +
				"  CC_ERROR_IF,                     					"  +
				"  CG_DESCRIPCION       									"  +
				"FROM                          							"  + 
				"  COMTMP_ERROR_IF                                	"  +
				"WHERE                                          	"  + 
				"  IC_NUMERO_PROCESO        =   ?               	"  +
				"  AND IC_NUMERO_LINEA      IN ("+variablesBind.toString()+")	"  +
				"ORDER BY IC_NUMERO_LINEA      							"		
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,Integer.parseInt(claveIF));
			ps.setInt(2,Integer.parseInt(icProceso));
			for(int i=0;i<lineas.length;i++){
				ps.setString(i+3, lineas[i]);	
			}			
			ps.executeUpdate();
 
		}catch(Exception e){
			log.debug("insertarRegistros(Exception)");
			log.debug("insertarRegistros.claveIF    			= <"+claveIF+">");
			log.debug("insertarRegistros.lstNumerosLineaOk 	= <"+lstNumerosLineaOk+">");
			log.debug("insertarRegistros.icProceso 			= <"+icProceso+">");
			e.printStackTrace();
			operacionExitosa = false;
		}finally{
			if (ps 	!= null) 										try { ps.close(); 	} catch(SQLException e) {}
			if (con 	!= null && con.hayConexionAbierta()){ 	
				con.terminaTransaccion(operacionExitosa);
				con.cierraConexionDB();
			} 
			log.info("insertarRegistros(S)");
		}
				
		return operacionExitosa;	
	}
	
}