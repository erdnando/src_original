package com.netro.descuento;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class RepDoctoSolicxEstatusDEbean {
	
	private int iNoEstatusDocto;
	private int iNoEstatusSolic;
	private String tipoOper="";;
 
	public RepDoctoSolicxEstatusDEbean() { }
	
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(RepDoctoSolicxEstatusDEbean.class);
	
	public void setNoEstatusDocto(int iNoEstatusDocto) {
		this.iNoEstatusDocto = iNoEstatusDocto;
		this.iNoEstatusSolic = 0;
	}
	
	public void setNoEstatusSolic(int iNoEstatusSolic) {
		this.iNoEstatusSolic = iNoEstatusSolic;
		this.iNoEstatusDocto = 0;
	}
	
	private String sCampo[] = { 
		/*  0 */	" (decode (E.cs_habilitado,'N','*','S',' ')||' '||E.cg_razon_social) as nombreEpo" ,
		/*  1 */	" (decode (P.cs_habilitado,'N','*','S',' ')||' '||P.cg_razon_social) as nombrePyme" ,
		/*  2 */	" (decode (I.cs_habilitado,'N','*','S',' ')||' '||I.cg_razon_social) as nombreIF" ,
		/*  3 */	" D.ig_numero_docto as numeroDocto",
		/*  4 */	" M.cd_nombre as nombreMoneda",
		/*  5 */	" D.ic_moneda as numeroMoneda",
		/*  6 */	" D.fn_monto as montoDocto",
		/*  7 */	" D.cs_dscto_especial as tipoFactoraje",
				
		/*  8 */	" TO_CHAR(D.df_fecha_docto,'DD/MM/YYYY') as fechaDocto",
		/*  9 */	" TO_CHAR(D.df_fecha_venc,'DD/MM/YYYY') as fechaVenc",
				
		/* 10 */	" I2.cg_razon_social as beneficiario",
		/* 11 */	" D.fn_porc_beneficiario as porcBeneficiario",
		/* 12 */	" DS.fn_importe_recibir_benef as importeBeneficiario",
		/* 13 */	" (D.fn_porc_beneficiario * fn_monto) / 100 as importeBeneficiario",
				
		/* 14 */	" D.fn_porc_anticipo as porcAnticipo",   
		/* 15 */	" D.fn_monto_dscto as montoDscto",   
		/* 16 */	" DS.in_importe_interes as importeInteres",
		/* 17 */	" DS.in_importe_recibir as importeRecibir",
				
		/* 18 */	" A1.cc_acuse as acuse1",
		/* 19 */	" decode(D.ic_moneda, 1, CV.fn_aforo, 54, CV.fn_aforo_dl) as aforo",
		/* 20 */	" DS.in_tasa_aceptada as tasaAceptada",
		/* 21 */	" (D.df_fecha_venc - TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'),'DD/MM/YYYY')) as plazo",//DEPRECATED
		/* 22 */	" (DS.in_importe_recibir - DS.fn_importe_recibir_benef) as importeNeto",
		/* 23 */	" A3.cc_acuse as acuse3",
		/* 24 */	" substr(S.ic_folio,1,10)||'-'||substr(S.ic_folio,11,1) as numeroSolicitud",
				
		/* 25 */	" CASE WHEN CC1.fn_monto_pago IS NOT NULL THEN"   +
					" 	CC1.fn_monto_pago"   +
					" WHEN CD.fn_monto_pago IS NOT NULL THEN"   +
					" 		CD.fn_monto_pago"   +
					"		WHEN ds.fn_porc_cobro_dscto is not null then (ds.fn_porc_cobro_dscto*DS.in_importe_recibir/100)"+
					" END as montoPago",
				
		/* 26 */	" CASE WHEN CC1.fn_porc_docto_aplicado IS NOT NULL THEN"   +
					" 	CC1.fn_porc_docto_aplicado"   +
					" 	WHEN CD.fn_porc_docto_aplicado IS NOT NULL THEN"   +
					" 		CD.fn_porc_docto_aplicado"   +
					"		WHEN ds.fn_porc_cobro_dscto is not null then ds.fn_porc_cobro_dscto"+
					" END as porcDoctoAplicado",
		/* 27 */	" i.ig_tipo_piso ",
		/* 28 */    " ds.fn_remanente ",
		/* 29 */	" TO_CHAR(DS.df_programacion,'DD/MM/YYYY') as df_programacion ", //Foda 25-smj 28/04/2005
		/* 30 */ " TF.CG_NOMBRE AS NOMBRE_TIPO_FACTORAJE ",
		/* 31 */	 "   decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as tipoTasa ",
		// Fodea 048 - 2012 -- Consulta Plazo, para doctos con estatus: 3 (Seleccionada Pyme) y 24 (En Proceso de Autorizacion IF)
		/* 32 */ " ( " +
						"  DECODE( PE.CS_FECHA_VENC_PYME, 'S', D.DF_FECHA_VENC_PYME, D.DF_FECHA_VENC )" + " - " +
						" 	(CASE  " +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
						"    TRUNC(SYSDATE) " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	END) " + 
					" ) " + 	
					" AS PLAZO ",
		// Fodea 048 - 2012 -- Consulta Plazo, para doctos con estatus: 26 (Programado Pyme)
		/* 33 */ " ( "  +
						"  DECODE( PE.CS_FECHA_VENC_PYME, 'S', D.DF_FECHA_VENC_PYME, D.DF_FECHA_VENC )" + " - " +
						" 	(CASE  "  +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN "  +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN "  +
						"    sigfechahabilxepo(D.ic_epo, TRUNC(SYSDATE), 2, '+') " +
						" 	END) "  + 
					" ) "  + 	
					" AS PLAZO ",
		// Fodea 017 - 2013 -- (Seleccionada Pyme)
		/* 34 */" 	(decode (i.cs_habilitado,'N','*','S',' ')||' '||(decode(ds.cs_opera_fiso,'N','N/A','S',i3.cg_razon_social))) as NOMBRE_FID ",
		/* 35 */"  (decode (ds.cs_opera_fiso,'N','N/A','S',ds.in_importe_recibir)) as IMPORTERECIBIR_FID ",
		/* 36 */"  (decode (ds.cs_opera_fiso,'S',ds.in_importe_recibir_fondeo,ds.in_importe_recibir)) as IMPORTERECIBIR ",
		/* 37 */"  (decode (ds.cs_opera_fiso,'N','N/A','S',ds.in_tasa_aceptada)) as TASAACEPTADA_FID ",
		/* 38 */"  (decode (ds.cs_opera_fiso,'S',ds.in_tasa_aceptada_fondeo,ds.in_tasa_aceptada)) as TASAACEPTADA ",
		/* 39 */"  (decode (ds.cs_opera_fiso,'N','N/A','S',ds.in_importe_interes)) as IMPORTEINTERES_FID ",         
		/* 40 */"  (decode (ds.cs_opera_fiso,'S',ds.in_importe_interes_fondeo,ds.in_importe_interes)) as IMPORTEINTERES ",
		/* 41 */"  ds.cs_opera_fiso as BANDERA_FISO "

		
		};
		
	private	String sTabla[] = { 
		/*  0 */	"COM_DOCUMENTO D",
		/*  1 */	"COM_DOCTO_SELECCIONADO DS",
		/*  2 */	"COM_SOLICITUD S",
		/*  3 */	"COM_ACUSE1 A1",
		/*  4 */	"COM_ACUSE2 A2",
		/*  5 */	"COM_ACUSE3 A3",
		/*  6 */	"COMCAT_PYME P",
		/*  7 */	"COMCAT_EPO E",
		/*  8 */	"COMCAT_IF I",
		/*  9 */	"COMCAT_IF I2",
		/* 10 */	"COMVIS_AFORO CV",
		/* 11 */	"COMCAT_MONEDA M",
		/* 12 */	"COMHIS_CAMBIO_ESTATUS CCE",
		/* 13 */	"FOP_COBRO_DISPOSICION CD",
		/* 14 */ "COMREL_PRODUCTO_EPO PE", 
		/* 15 */ "COMCAT_TIPO_FACTORAJE TF",
		// Fodea 048 - 2012 -- Tabla para asociada a la consulta del Plazo, estatus 3, 24 y 26.
		/* 16 */ "COMREL_IF_EPO_X_PRODUCTO IEXP ",
		//Fodea 017 - 2013 
		/*  17 */	"COMCAT_IF I3"
	};
		
		// La vista es usada solo para las Aplicadas a Cr�dito.
	private	String sVista = " (select CC.ic_documento, CC.fn_porc_docto_aplicado,"   +
						" sum(CC.fn_monto_pago) as fn_monto_pago"   +
						" from com_cobro_credito CC"   +
						" group by CC.ic_documento, CC.fn_porc_docto_aplicado) CC1 ";
		
	private	String sCondicion[] = {
		/*  0 */	" D.ic_epo = E.ic_epo",	
		/*  1 */	" D.ic_pyme = P.ic_pyme",
		/*  2 */	" D.ic_epo = CV.ic_epo",
		/*  3 */	" D.cc_acuse = A1.cc_acuse",
		/*  4 */	" D.ic_moneda = M.ic_moneda",
		/*  5 */	" D.ic_documento = DS.ic_documento",
		/*  6 */	" D.ic_if = I.ic_if",
		/*  7 */	" D.cc_acuse = A1.cc_acuse",
		/*  8 */	" DS.cc_acuse = A2.cc_acuse",
		/*  9 */	" DS.ic_documento = S.ic_documento",
		/* 10  */	" S.cc_acuse = A3.cc_acuse",
		/* 11 */	" CCE.ic_documento = D.ic_documento",
		
		/* 12 */	" D.ic_if = I.ic_if(+)",
		/* 13 */	" D.ic_beneficiario = I2.ic_if(+)",
		/* 14 */	" DS.ic_documento = CC1.ic_documento(+)",
		/* 15 */	" DS.ic_documento = CD.ic_documento(+)",
		
		/* 16 */	" D.df_fecha_venc >= TRUNC(SYSDATE) AND D.df_fecha_venc < TRUNC(SYSDATE) + 1 ",  //Query Afinado GEAG 17/12/2007
		/* 17 */	" A1.df_fechahora_carga >= TRUNC(SYSDATE) AND A1.df_fechahora_carga < TRUNC(SYSDATE) + 1 ",
		/* 18 */	" A2.df_fecha_hora >= TRUNC(SYSDATE) AND A2.df_fecha_hora < TRUNC(SYSDATE) + 1 ", //Query Afinado GEAG 17/12/2007
		/* 19 */	" A3.df_fecha_hora >= TRUNC(SYSDATE) AND A3.df_fecha_hora < TRUNC(SYSDATE) + 1 ",  //QUERY AFINADO SMJ 05/07/05
		/* 20 */	" S.df_fecha_solicitud >= TRUNC(SYSDATE) AND S.df_fecha_solicitud < TRUNC(SYSDATE) + 1 ", //Query Afinado GEAG 17/12/2007
		/* 21 */	" CCE.dc_fecha_cambio >= TRUNC(SYSDATE) AND CCE.dc_fecha_cambio < TRUNC(SYSDATE) + 1 ", //Query Afinado GEAG 17/12/2007
		/* 22 */	" S.cs_tipo_solicitud = 'C' ",
		/* 23 */	" CCE.ic_cambio_estatus = 27",
		/* 24 */	" D.ic_estatus_docto = ",
		/* 25 */	" S.ic_estatus_solic = ",
		/* 26 */	" CCE.dc_fecha_cambio = (SELECT MAX(CCE2.dc_fecha_cambio) FROM comhis_cambio_estatus CCE2 WHERE CCE2.ic_documento = D.ic_documento AND CCE2.ic_cambio_estatus = 27)",
//		/* 27 */ " D.ic_epo = pe.ic_epo AND pe.ic_producto_nafin = 1 AND (pe.ic_modalidad IS NULL OR pe.ic_modalidad = 1) "
		/* 27 */ " D.ic_epo = pe.ic_epo AND pe.ic_producto_nafin = 1  ",
		/* 28 */ " D.CS_DSCTO_ESPECIAL = TF.CC_TIPO_FACTORAJE ",		
		/* 29 */	" A2.ic_producto_nafin = 1	", 
		// Fodea 048 - 2012 -- Condiciones para el Plazo, estatus 3, 24 y 26.
		/* 30 */ " D.IC_IF                = IEXP.IC_IF  ",			
		/* 31 */ " D.IC_EPO               = IEXP.IC_EPO ",			
		/* 32 */ " IEXP.IC_PRODUCTO_NAFIN = 1           ",
		// Fodea 017 - 2013
		/* 33 */	" ds.ic_if = i3.ic_if (+) ",
		/* 34 */	" ds.cs_opera_fiso='S' ",
		/* 35 */	" ds.cs_opera_fiso='N' "
	};
	
	private	String sOrden[] = {
		/*  0 */	" E.cg_razon_social",
		/*  1 */	" D.ig_numero_docto",
		/*  2 */	" D.df_fecha_docto",
		/*  3 */	" D.df_fecha_venc",
		/*  4 */	" M.cd_nombre",
		/*  5 */	" D.fn_monto",
		/*  6 */	" D.cc_acuse",
		/*  7 */	" D.fn_monto_dscto",
		/*  8 */	" I.cg_razon_social ",
		/*  9 */	" DS.in_tasa_aceptada ",
		/* 10 */	" DS.in_importe_interes ",
		/* 11 */	" DS.in_importe_recibir "
					};	
	private String banderaFISO;
	
	public String getQueryPorEstatus() {
   			
		StringBuffer sbQuery = new StringBuffer("");
		String sSelect = "SELECT";
		String sReferencia = "'RepDoctoSolicxEstatusDEBean:getQueryPorEstatus("+iNoEstatusDocto+")' as PANTALLA";
	
		try {
 
		// D O C U M E N T O S
		switch(iNoEstatusDocto) {
		 	case 2:	// Negociables.
		 		sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ index(A2) ordered use_nl(D, A1, P, E, I, M) */ ");
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+ sCampo[3] +","+ sCampo[18] +","+ sCampo[8] +","+
								sCampo[9] +","+ sCampo[4] +","+ sCampo[5] +","+ sCampo[6] +","+ sCampo[19] +","+
								sCampo[7] +","+ sCampo[2] +","+ sCampo[10] +","+ sCampo[11] +","+ sCampo[13] +","+ sCampo[30] +","+
								sReferencia);
				// Tablas
				sbQuery.append(" FROM " + sTabla[3] +","+ sTabla[0] +","+ sTabla[6] +","+ sTabla[7] +","+
								sTabla[8] +","+ sTabla[9] +","+ sTabla[10] +","+ sTabla[11] + "," + sTabla[14] + "," + sTabla[15]);
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[2] +
								" AND "+ sCondicion[7] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[12] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[17] +							
								" AND "+ sCondicion[24] + iNoEstatusDocto  +// D.ic_estatus_docto = 2
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28]);	
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[2] +","+ sOrden[3] +","+
								sOrden[4] +","+ sOrden[5] +","+ sOrden[6]);
				break;
			case 3:	// Seleccionados Pyme.
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ index(A2 IN_ACUSE2_02_NUK)   use_nl(D, DS,  I, I2, E, M,tf) */ ");
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+ sCampo[3] +","+ sCampo[8] +","+ sCampo[9] +","+
								sCampo[2] +","+ sCampo[38] +","+ 
								sCampo[32]+","+ // Plazo 
								sCampo[4] +","+ sCampo[5] +","+
								sCampo[6] +","+ sCampo[14] +","+ sCampo[15] +","+ sCampo[36] +","+ sCampo[40] +","+
								sCampo[7] +","+ sCampo[22] +","+ sCampo[10] +","+ sCampo[11] +","+ sCampo[12] +","+ sCampo[30] +","+
								sCampo[29] +","+ // Fodea 005-2009 24 hrs
								sCampo[31] +","+// Fodea 031-2011
								sCampo[34] +","+ sCampo[35] +","+ sCampo[37] +","+ sCampo[39] +","+sCampo[41] +","+// FODEA 17 2013
								sReferencia);
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[4] +","+ sTabla[6] +","+
								sTabla[7] +","+ sTabla[8] + ", "+ sTabla[9] +","+ sTabla[11]+","+ sTabla[14] +","+ sTabla[15]
								+ "," + sTabla[16] + "," + sTabla[17] 
							);
				
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[8] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[18] +
								" AND "+ sCondicion[29] +
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28] +
								" AND "+ sCondicion[30] +
								" AND "+ sCondicion[31] +
								" AND "+ sCondicion[32] +
								" AND "+ sCondicion[33] 
							);
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[2] +","+ sOrden[3] +","+
								sOrden[4] +","+ sOrden[5] +","+ sOrden[7] +","+ sOrden[8] +","+ sOrden[9] +","+
								sOrden[10] +","+ sOrden[11] );
				break;
			case 24:// En Proceso de Autorizacion IF.
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ ordered index(D IN_COM_DOCUMENTO_04_NUK) index(A2) use_nl(D, DS, A2, P, I, I2, E, M) */ ");
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+ sCampo[3] +","+ sCampo[8] +","+ sCampo[9] +","+
								sCampo[2] +","+ sCampo[38] +","+ 
								sCampo[32]+","+ // Plazo 
								sCampo[4] +","+ sCampo[5] +","+
								sCampo[6] +","+ sCampo[14] +","+ sCampo[15] +","+ sCampo[36] +","+ sCampo[40] +","+
								sCampo[7] +","+ sCampo[22] +","+ sCampo[10] +","+ sCampo[11] +","+ sCampo[12] +","+ sCampo[30] +","+sCampo[31] +","+// Fodea 031-2011
								sCampo[34] +","+ sCampo[35] +","+ sCampo[37] +","+ sCampo[39] +","+sCampo[41] +","+// FODEA 17 2013
								sReferencia);
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[4] +","+ sTabla[6] +","+
								sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]
								+ "," + sTabla[16] + "," + sTabla[17] 
							);
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[8] +
								" AND "+ sCondicion[13] +
								// " AND "+ sCondicion[18] + // Fodea 000 - 2012. Fodea 000 - 2012. ADMIN - Proceso de Aut IF
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28] +
								" AND "+ sCondicion[30] +
								" AND "+ sCondicion[31] +
								" AND "+ sCondicion[32]	+
								" AND "+ sCondicion[33]
							);
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
								
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[2] +","+ sOrden[3] +","+
								sOrden[4] +","+ sOrden[5] +","+ sOrden[7] +","+ sOrden[8] +","+ sOrden[9] +","+
								sOrden[10] +","+ sOrden[11] );
				break;
			case 11:// Operada Pagada.
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ index(A2) use_nl(D, M, I, I2, E) */ ");
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+ sCampo[2] +","+ sCampo[24] +","+ sCampo[3] +","+
								sCampo[4] +","+ sCampo[5] +","+ sCampo[6] +","+ sCampo[14] +","+ sCampo[38] +","+ 
								sCampo[15] +","+ sCampo[9] +","+ sCampo[7] +","+ sCampo[22] +","+
								sCampo[10] +","+ sCampo[11] +","+ sCampo[12] +","+ sCampo[30] +","+ sCampo[31] +","+
								sCampo[34] +","+ sCampo[37]  +","+sCampo[41] +","+// FODEA 17 2013
								sReferencia);
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[2] +","+ sTabla[6] +","+ 
								sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11]  +","+ sTabla[14] +","+ sTabla[15]+ "," + sTabla[17]);
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[16] +
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28]+
								" AND "+ sCondicion[33]);
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[3] +","+ sOrden[4] +","+ 
								sOrden[5] +","+ sOrden[7] +","+ sOrden[8] +","+ sOrden[9] );
				break;
			case 16:// Aplicado a Credito.
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ index(A2) use_nl(D, M, I, S, E, CD, CC1) */ ");
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+ sCampo[2] +","+ sCampo[24] +","+ sCampo[3] +","+
								sCampo[4] +","+ sCampo[5] +","+ sCampo[6] +","+ sCampo[14] +","+ sCampo[15] +","+ 
								sCampo[20] +","+ sCampo[17] +","+ sCampo[7] +","+ sCampo[25] +","+
								sCampo[26] +","+ sCampo[16] +","+ sCampo[27] + "," + sCampo[28] + "," + sCampo[30] + "," + sReferencia );
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[2] +","+ sTabla[6] +","+
								sTabla[8] +","+ sTabla[7] +","+ sTabla[11] +","+ sVista +","+ sTabla[13] +","+ sTabla[14] +","+ sTabla[15] );
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[14] +
								" AND "+ sCondicion[15] +
								" AND "+ sCondicion[20] +
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28]);
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[4] +","+ sOrden[7] +","+
								sOrden[8] +","+ sOrden[9] +","+ sOrden[11] );
				break;
			case 23:// Pignorado.
				sbQuery = new StringBuffer(sSelect);
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+  sCampo[3] +","+ sCampo[18] +","+ sCampo[8] +","+
								sCampo[9] +","+ sCampo[4] +","+ sCampo[5] +","+ sCampo[6] +","+ sCampo[19] +","+
								sCampo[7] +","+ sCampo[2] +","+ sCampo[10] +","+ sCampo[11] +","+
								sCampo[13] +","+ sCampo[30] +","+ sReferencia );
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[3] +","+ sTabla[12] +","+
								sTabla[6] +","+ sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+
								sTabla[10] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]);
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[2] +
								" AND "+ sCondicion[3] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[11] +
								" AND "+ sCondicion[12] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[26] +
								" AND "+ sCondicion[21] +
								" AND "+ sCondicion[23] +
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28]);
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[2] +","+ sOrden[3] +","+
								sOrden[4] +","+ sOrden[5] +","+ sOrden[6] );
				break;
				
			
				case 26:	// Programado
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ index(A2) use_nl(D, DS, A2, I, I2, E, M) */ ");
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+ sCampo[3] +","+ sCampo[8] +","+ sCampo[9] +","+
								sCampo[2] +","+ sCampo[20] +","+ 
								sCampo[33]+","+ // Plazo 
								sCampo[4] +","+ sCampo[5] +","+
								sCampo[6] +","+ sCampo[14] +","+ sCampo[15] +","+ sCampo[16] +","+ sCampo[17] +","+
								sCampo[7] +","+ sCampo[29] +","+ sCampo[10] +","+ sCampo[11] +","+ sCampo[12] +","+ 
								sCampo[22] +","+ sCampo[30] +","+ sCampo[31] +","+sReferencia);
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[4] +","+ sTabla[6] +","+
								sTabla[7] +","+ sTabla[8] + ", "+ sTabla[9] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]
								+ "," + sTabla[16] 
							);
				
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[8] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[18] +
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28] +
								" AND "+ sCondicion[30] +
								" AND "+ sCondicion[31] +
								" AND "+ sCondicion[32]
							);
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[2] +","+ sOrden[3] +","+
								sOrden[4] +","+ sOrden[5] +","+ sOrden[7] +","+ sOrden[8] +","+ sOrden[9] +","+
								sOrden[10] +","+ sOrden[11] );
				break;
				
				
			default:
				break;
		} //fin_doctos
		
		// S O L I C I T U D E S
		switch(iNoEstatusSolic) {
			case 1: // Seleccionada IF
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ use_nl(I, I2, P, E) */ ");
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+ sCampo[2] +","+ sCampo[3] +","+ sCampo[23] +","+ 
								sCampo[24] +","+ sCampo[22] +","+ sCampo[4] +","+ sCampo[5] +","+ sCampo[6] +","+ 
								sCampo[14] +","+ sCampo[15] +","+ sCampo[38] +","+ sCampo[36] +","+ sCampo[40] +","+
								sCampo[7] +","+ sCampo[22] +","+ sCampo[10] +","+ sCampo[11] +","+
								sCampo[12] +","+ sCampo[30] +","+ sCampo[31] +","+
								sCampo[34] +","+ sCampo[35] +","+ sCampo[37] +","+ sCampo[39] +","+sCampo[41] +","+// FODEA 17 2013
								sReferencia);
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[5] +","+ sTabla[2] +","+
								sTabla[6] +","+ sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]
								+ "," + sTabla[17] 
								);
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[10] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[19] +
								" AND "+ sCondicion[22] +
								" AND "+ sCondicion[25] + iNoEstatusSolic +
								" AND "+ sCondicion[27] + 
								" AND "+ sCondicion[28] +
								" AND "+ sCondicion[33]);
								
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[4] +","+ sOrden[5] +","+
								sOrden[7] +","+ sOrden[8] +","+ sOrden[9] +","+ sOrden[10] +","+ sOrden[11] );
				break;
			case 2:	// En Proceso
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ use_nl(P, E, I, I2, M) */ ");//Mod SMJ 05/07/05
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+ sCampo[2] +","+ sCampo[24] +","+ sCampo[3] +","+
								sCampo[4] +","+ sCampo[5] +","+ sCampo[6] +","+ sCampo[14] +","+ sCampo[15] +","+
								sCampo[38] +","+ sCampo[36] +","+ sCampo[40] +","+ sCampo[7] +","+
								sCampo[22] +","+ sCampo[10] +","+ sCampo[11] +","+ sCampo[12] +","+
								sCampo[30] +","+
								sCampo[34] +","+ sCampo[35] +","+ sCampo[37] +","+ sCampo[39] +","+sCampo[41] +","+// FODEA 17 2013
								sReferencia);
				/*/ Tablas ----- SE COMENTA PARA PONER EL QUERY AFINADO-----SMJ 05/07/05
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[2] +","+ sTabla[5] +","+ 
								sTabla[6] +","+ sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11] );*/
								
					// Tablas
				sbQuery.append(" FROM " + sTabla[5] +","+ sTabla[2] +","+ sTabla[1] +","+ sTabla[0] +","+ 
								sTabla[6] +","+ sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]
								+ "," + sTabla[17] 
								);				
								
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[10] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[19] +
								" AND "+ sCondicion[22] +
								" AND "+ sCondicion[25] + iNoEstatusSolic +
								" AND "+ sCondicion[27] + 
								" AND "+ sCondicion[28]+
								" AND "+ sCondicion[33]);
							
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[4] +","+ sOrden[5] +","+ 
								sOrden[7] +","+ sOrden[8] +","+ sOrden[9] +","+ sOrden[10] +","+ sOrden[11] );
				break;
			case 3:	// Operada
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ use_nl(P, E, I, I2) */ ");
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+ sCampo[2] +","+ sCampo[24] +","+ sCampo[3] +","+
								sCampo[4] +","+ sCampo[5] +","+ sCampo[6] +","+ sCampo[14] +","+ sCampo[15] +","+
								sCampo[38] +","+ sCampo[36] +","+ sCampo[7] +","+ sCampo[22] +","+
				       	sCampo[10] +","+ sCampo[11] +","+ sCampo[12] +","+ sCampo[30] +","+ sCampo[29] +","+ sCampo[31] +","+
							sCampo[34] +","+ sCampo[37] +","+ sCampo[35] +","+sCampo[41] +","+// FODEA 17 2013
							sReferencia );
              
				// Tablas
				sbQuery.append(" FROM " +sTabla[0] +","+ sTabla[1] +","+ sTabla[2] +","+ sTabla[6] +","+ 
								sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]+ "," + sTabla[17]  );
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[20] +
								" AND "+ sCondicion[25] + iNoEstatusSolic +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28] +
								" AND "+ sCondicion[33]);
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[4] +","+ sOrden[7] +","+ 
								sOrden[8] +","+ sOrden[9] +","+ sOrden[11] );
				break;
			case 10: // Operado con Fondeo Propio
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ use_nl(I, I2, P, E) */ ");
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+ sCampo[2] +","+ sCampo[3] +","+ sCampo[23] +","+ 
								sCampo[24] +","+ sCampo[22] +","+ sCampo[4] +","+ sCampo[5] +","+ sCampo[6] +","+ 
								sCampo[14] +","+ sCampo[15] +","+ sCampo[20] +","+ sCampo[16] +","+ sCampo[17] +","+
								sCampo[7] +","+ sCampo[22] +","+ sCampo[10] +","+ sCampo[11] +","+
								sCampo[12] +","+ sCampo[30] +","+ sCampo[31] +","+ sReferencia);
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[5] +","+ sTabla[2] +","+
								sTabla[6] +","+ sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15] );
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[10] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[19] +
								" AND "+ sCondicion[22] +
								" AND "+ sCondicion[25] + iNoEstatusSolic +
								" AND "+ sCondicion[27] + 
								" AND "+ sCondicion[28] );
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[4] +","+ sOrden[5] +","+
								sOrden[7] +","+ sOrden[8] +","+ sOrden[9] +","+ sOrden[10] +","+ sOrden[11] );
				break;
			default:
				break;
		}	
		log.debug("sbQuery::::::::"+iNoEstatusDocto+":::::: "+sbQuery);
		} catch(Exception e) {
			log.error("RepDoctoSolicxEstatusDEbean::RepDoctoSolicxEstatusDEbean Exception "+e);
			e.printStackTrace();
		} 		
	
	return sbQuery.toString();		
	} // getQueryPorCambioEstatus()



	/**
	 * 
	 * @return  
	 */

	public String getQueryPorEstatusAggregateCalculationQuery() {
   			
		StringBuffer sbQuery = new StringBuffer("");
		String sSelect = "SELECT";
		String sReferencia = "'RepDoctoSolicxEstatusDEBean:getQueryPorEstatus("+iNoEstatusDocto+")' as PANTALLA";
	
		try {
 
		// D O C U M E N T O S
		switch(iNoEstatusDocto) {
		 	case 2:	// Negociables.
		 		sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ index(A2) ordered use_nl(D, A1, P, E, I, M) */ ");
				// Campos
				sbQuery.append( "   m.cd_nombre AS MONEDA, " + 
									" count(*)  as NUM_DOCTOS,  " +
									" sum(d.fn_monto) as  TOTAL_MONTO_DOCTO,  " +
									" '' as TOTAL_REC_GAR,  " +
									" sum( d.fn_monto *  ( DECODE (d.ic_moneda, 1, CV.fn_aforo, 54, CV.fn_aforo_dl)/100))    as MONTO_DESC,     " +    
									" '' as MONTO_INT,  " +
									" '' as MONTO_OPER    ");
				// Tablas
				sbQuery.append(" FROM " + sTabla[3] +","+ sTabla[0] +","+ sTabla[6] +","+ sTabla[7] +","+
								sTabla[8] +","+ sTabla[9] +","+ sTabla[10] +","+ sTabla[11] + "," + sTabla[14] + "," + sTabla[15]);
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[2] +
								" AND "+ sCondicion[7] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[12] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[17] +							
								" AND "+ sCondicion[24] + iNoEstatusDocto  +// D.ic_estatus_docto = 2
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28]);	
				// group by
				sbQuery.append("   group by  m.cd_nombre   " );  
				
				
				break;
			case 3:	// Seleccionados Pyme.
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ index(A2 IN_ACUSE2_02_NUK)   use_nl(D, DS,  I, I2, E, M,tf) */ ");
				// Campos
				
				sbQuery.append(  " m.cd_nombre   as MONEDA,    "+
				"     count(*) as  NUM_DOCTOS,  "+
				"      SUM(d.fn_monto)  as TOTAL_MONTO_DOCTO, "+
				" '' as TOTAL_REC_GAR ,  "+
				" SUM(d.fn_monto_dscto)   as MONTO_DESC,   "+
				" SUM (DECODE (ds.cs_opera_fiso,'S', ds.in_importe_interes_fondeo,   ds.in_importe_interes  )  )  as MONTO_INT,  "+
				" SUM (DECODE (ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo,  ds.in_importe_recibir   )  ) as MONTO_OPER ,    "+
				" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_interes)   ) as MONTO_INT_FISO   ,     "+
				" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_recibir) )  as MONTO_OPER_FISO    ");
		
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[4] +","+ sTabla[6] +","+
								sTabla[7] +","+ sTabla[8] + ", "+ sTabla[9] +","+ sTabla[11]+","+ sTabla[14] +","+ sTabla[15]
								+ "," + sTabla[16] + "," + sTabla[17] 
							);
				
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[8] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[18] +
								" AND "+ sCondicion[29] +
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28] +
								" AND "+ sCondicion[30] +
								" AND "+ sCondicion[31] +
								" AND "+ sCondicion[32] +
								" AND "+ sCondicion[33] 
							);
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
							
							if(banderaFISO.equals("S")){
								sbQuery.append(" AND cs_opera_fiso    = 'S'  ");
							}else  {
								sbQuery.append(" AND cs_opera_fiso    = 'N'  ");								
							}
							
							
				// Orden
				sbQuery.append("  GROUP BY  m.cd_nombre  " );
				break;
			case 24:// En Proceso de Autorizacion IF.
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ ordered index(D IN_COM_DOCUMENTO_04_NUK) index(A2) use_nl(D, DS, A2, P, I, I2, E, M) */ ");
				// Campos
				sbQuery.append(  " m.cd_nombre   as MONEDA,    "+
				"     count(*) as  NUM_DOCTOS,  "+
				"      SUM(d.fn_monto)  as TOTAL_MONTO_DOCTO, "+
				" '' as TOTAL_REC_GAR ,  "+
				" SUM(d.fn_monto_dscto)   as MONTO_DESC,   "+
				" SUM (DECODE (ds.cs_opera_fiso,'S', ds.in_importe_interes_fondeo,   ds.in_importe_interes  )  )  as MONTO_INT,  "+
				" SUM (DECODE (ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo,  ds.in_importe_recibir   )  ) as MONTO_OPER ,    "+
				" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_interes)   ) as MONTO_INT_FISO   ,     "+
				" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_recibir) )  as MONTO_OPER_FISO    ");
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[4] +","+ sTabla[6] +","+
								sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]
								+ "," + sTabla[16] + "," + sTabla[17] 
							);
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[8] +
								" AND "+ sCondicion[13] +
								// " AND "+ sCondicion[18] + // Fodea 000 - 2012. Fodea 000 - 2012. ADMIN - Proceso de Aut IF
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28] +
								" AND "+ sCondicion[30] +
								" AND "+ sCondicion[31] +
								" AND "+ sCondicion[32]	+
								" AND "+ sCondicion[33]
							);
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
							
							if(banderaFISO.equals("S")){
								sbQuery.append(" AND cs_opera_fiso    = 'S'  ");
							}else  {
								sbQuery.append(" AND cs_opera_fiso    = 'N'  ");								
							}
								
				// GROUP BY
				sbQuery.append(" GROUP BY  m.cd_nombre  ");
				break;
			case 11:// Operada Pagada.
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ index(A2) use_nl(D, M, I, I2, E) */ ");
				// Campos
											
				sbQuery.append(  " m.cd_nombre   as MONEDA,    "+
					"     count(*) as  NUM_DOCTOS,  "+
					"      SUM(d.fn_monto)  as TOTAL_MONTO_DOCTO, "+
					" '' as TOTAL_REC_GAR ,  "+
					" SUM(d.fn_monto_dscto)   as MONTO_DESC,   "+
					" SUM (DECODE (ds.cs_opera_fiso,'S', ds.in_importe_interes_fondeo,   ds.in_importe_interes  )  )  as MONTO_INT,  "+
					" SUM (DECODE (ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo,  ds.in_importe_recibir   )  ) as MONTO_OPER ,    "+
					" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_interes)   ) as MONTO_INT_FISO   ,     "+
					" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_recibir) )  as MONTO_OPER_FISO    ");
			
			
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[2] +","+ sTabla[6] +","+ 
								sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11]  +","+ sTabla[14] +","+ sTabla[15]+ "," + sTabla[17]);
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[16] +
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28]+
								" AND "+ sCondicion[33]);
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
							
						if(banderaFISO.equals("S")){
							sbQuery.append(" AND cs_opera_fiso    = 'S'  ");
						}else  {
							sbQuery.append(" AND cs_opera_fiso    = 'N'  ");								
						}
							
				sbQuery.append("  GROUP BY  m.cd_nombre "  );
				break;
			case 16:// Aplicado a Credito.
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ index(A2) use_nl(D, M, I, S, E, CD, CC1) */ ");
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+ sCampo[2] +","+ sCampo[24] +","+ sCampo[3] +","+
								sCampo[4] +","+ sCampo[5] +","+ sCampo[6] +","+ sCampo[14] +","+ sCampo[15] +","+ 
								sCampo[20] +","+ sCampo[17] +","+ sCampo[7] +","+ sCampo[25] +","+
								sCampo[26] +","+ sCampo[16] +","+ sCampo[27] + "," + sCampo[28] + "," + sCampo[30] + "," + sReferencia );
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[2] +","+ sTabla[6] +","+
								sTabla[8] +","+ sTabla[7] +","+ sTabla[11] +","+ sVista +","+ sTabla[13] +","+ sTabla[14] +","+ sTabla[15] );
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[14] +
								" AND "+ sCondicion[15] +
								" AND "+ sCondicion[20] +
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28]);
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[4] +","+ sOrden[7] +","+
								sOrden[8] +","+ sOrden[9] +","+ sOrden[11] );
				break;
			case 23:// Pignorado.
				sbQuery = new StringBuffer(sSelect);
				// Campos
				sbQuery.append(sCampo[0] +","+ sCampo[1] +","+  sCampo[3] +","+ sCampo[18] +","+ sCampo[8] +","+
								sCampo[9] +","+ sCampo[4] +","+ sCampo[5] +","+ sCampo[6] +","+ sCampo[19] +","+
								sCampo[7] +","+ sCampo[2] +","+ sCampo[10] +","+ sCampo[11] +","+
								sCampo[13] +","+ sCampo[30] +","+ sReferencia );
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[3] +","+ sTabla[12] +","+
								sTabla[6] +","+ sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+
								sTabla[10] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]);
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[2] +
								" AND "+ sCondicion[3] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[11] +
								" AND "+ sCondicion[12] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[26] +
								" AND "+ sCondicion[21] +
								" AND "+ sCondicion[23] +
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28]);
				// Orden
				sbQuery.append(" ORDER BY "+ sOrden[0] +","+ sOrden[1] +","+ sOrden[2] +","+ sOrden[3] +","+
								sOrden[4] +","+ sOrden[5] +","+ sOrden[6] );
				break;
				
			
				case 26:	// Programado
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ index(A2) use_nl(D, DS, A2, I, I2, E, M) */ ");
				// Campos
			sbQuery.append("   m.cd_nombre AS MONEDA , " +
								"	 count(*)  as NUM_DOCTOS , " +
								" sum(d.fn_monto) as  TOTAL_MONTO_DOCTO,   " +
								" '' as TOTAL_REC_GAR,  " +
								" sum(d.fn_monto_dscto ) as MONTO_DESC, " +
								" sum(  ds.in_importe_interes )  as MONTO_INT, " +
								" sum (ds.in_importe_recibir)  as MONTO_OPER  ");
								
					// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[4] +","+ sTabla[6] +","+
								sTabla[7] +","+ sTabla[8] + ", "+ sTabla[9] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]
								+ "," + sTabla[16] 
							);
				
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[8] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[18] +
								" AND "+ sCondicion[24] + iNoEstatusDocto +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28] +
								" AND "+ sCondicion[30] +
								" AND "+ sCondicion[31] +
								" AND "+ sCondicion[32]
							);
				// Orden
				sbQuery.append(" GROUP BY  m.cd_nombre  "  );
				break;
				
				
			default:
				break;
		} //fin_doctos
		
		// S O L I C I T U D E S
		switch(iNoEstatusSolic) {
			case 1: // Seleccionada IF
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ use_nl(I, I2, P, E) */ ");
				// Campos
				sbQuery.append(  " m.cd_nombre   as MONEDA,    "+
				"     count(*) as  NUM_DOCTOS,  "+
				"      SUM(d.fn_monto)  as TOTAL_MONTO_DOCTO, "+
				" '' as TOTAL_REC_GAR ,  "+
				" SUM(d.fn_monto_dscto)   as MONTO_DESC,   "+
				" SUM (DECODE (ds.cs_opera_fiso,'S', ds.in_importe_interes_fondeo,   ds.in_importe_interes  )  )  as MONTO_INT,  "+
				" SUM (DECODE (ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo,  ds.in_importe_recibir   )  ) as MONTO_OPER ,    "+
				" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_interes)   ) as MONTO_INT_FISO   ,     "+
				" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_recibir) )  as MONTO_OPER_FISO    ");
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[5] +","+ sTabla[2] +","+
								sTabla[6] +","+ sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]
								+ "," + sTabla[17] 
								);
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[10] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[19] +
								" AND "+ sCondicion[22] +
								" AND "+ sCondicion[25] + iNoEstatusSolic +
								" AND "+ sCondicion[27] + 
								" AND "+ sCondicion[28] +
								" AND "+ sCondicion[33]);
								
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
							
							if(banderaFISO.equals("S")){
								sbQuery.append(" AND cs_opera_fiso    = 'S'  ");
							}else  {
								sbQuery.append(" AND cs_opera_fiso    = 'N'  ");								
							}
							
				// Orden
				sbQuery.append(" GROUP BY  m.cd_nombre " );
				break;
			case 2:	// En Proceso
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ use_nl(P, E, I, I2, M) */ ");//Mod SMJ 05/07/05
				// Campos
				sbQuery.append(  " m.cd_nombre   as MONEDA,    "+
					"     count(*) as  NUM_DOCTOS,  "+
					"      SUM(d.fn_monto)  as TOTAL_MONTO_DOCTO, "+
					" '' as TOTAL_REC_GAR ,  "+
					" SUM(d.fn_monto_dscto)   as MONTO_DESC,   "+
					" SUM (DECODE (ds.cs_opera_fiso,'S', ds.in_importe_interes_fondeo,   ds.in_importe_interes  )  )  as MONTO_INT,  "+
					" SUM (DECODE (ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo,  ds.in_importe_recibir   )  ) as MONTO_OPER ,    "+
					" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_interes)   ) as MONTO_INT_FISO   ,     "+
					" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_recibir) )  as MONTO_OPER_FISO    ");
											
					// Tablas
				sbQuery.append(" FROM " + sTabla[5] +","+ sTabla[2] +","+ sTabla[1] +","+ sTabla[0] +","+ 
								sTabla[6] +","+ sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]
								+ "," + sTabla[17] 
								);				
								
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[10] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[19] +
								" AND "+ sCondicion[22] +
								" AND "+ sCondicion[25] + iNoEstatusSolic +
								" AND "+ sCondicion[27] + 
								" AND "+ sCondicion[28]+
								" AND "+ sCondicion[33]);
							
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
							
							if(banderaFISO.equals("S")){
								sbQuery.append(" AND cs_opera_fiso    = 'S'  ");
							}else  {
								sbQuery.append(" AND cs_opera_fiso    = 'N'  ");								
							}
							
				// Orden
				sbQuery.append("  GROUP BY  m.cd_nombre  ")  ;
				break;
			case 3:	// Operada
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ use_nl(P, E, I, I2) */ ");
				// Campos
				sbQuery.append(  " m.cd_nombre   as MONEDA,    "+
					"     count(*) as  NUM_DOCTOS,  "+
					"      SUM(d.fn_monto)  as TOTAL_MONTO_DOCTO, "+
					" '' as TOTAL_REC_GAR ,  "+
					" SUM(d.fn_monto_dscto)   as MONTO_DESC,   "+
					" SUM (DECODE (ds.cs_opera_fiso,'S', ds.in_importe_interes_fondeo,   ds.in_importe_interes  )  )  as MONTO_INT,  "+
					" SUM (DECODE (ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo,  ds.in_importe_recibir   )  ) as MONTO_OPER ,    "+
					" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_interes)   ) as MONTO_INT_FISO   ,     "+
					" SUM (DECODE (ds.cs_opera_fiso, 'N', '0', 'S', ds.in_importe_recibir) )  as MONTO_OPER_FISO    ");
			
              
				// Tablas
				sbQuery.append(" FROM " +sTabla[0] +","+ sTabla[1] +","+ sTabla[2] +","+ sTabla[6] +","+ 
								sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15]+ "," + sTabla[17]  );
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[20] +
								" AND "+ sCondicion[25] + iNoEstatusSolic +
								" AND "+ sCondicion[27] +
								" AND "+ sCondicion[28] +
								" AND "+ sCondicion[33]);
							if(tipoOper.equals("ON"))
							{
								sbQuery.append(" AND "+ sCondicion[35]);
							}
							if(tipoOper.equals("FID"))
							{
								sbQuery.append(" AND "+ sCondicion[34]);
							}
			
							if(banderaFISO.equals("S")){
								sbQuery.append(" AND cs_opera_fiso    = 'S'  ");
							}else  {
								sbQuery.append(" AND cs_opera_fiso    = 'N'  ");								
							}
							//  GROUP BY
						sbQuery.append("  GROUP BY  m.cd_nombre  ")  ;
				
				break;
			case 10: // Operado con Fondeo Propio
				sbQuery = new StringBuffer(sSelect);
				// Hints
				sbQuery.append(" /*+ use_nl(I, I2, P, E) */ ");
				// Campos
			sbQuery.append("   m.cd_nombre AS MONEDA , " +
								"	 count(*)  as NUM_DOCTOS , " +
								" sum(d.fn_monto) as  TOTAL_MONTO_DOCTO,   " +
								" '' as TOTAL_REC_GAR,  " +
								" sum(d.fn_monto_dscto ) as MONTO_DESC, " +
								" sum(  ds.in_importe_interes )  as MONTO_INT, " +
								" sum (ds.in_importe_recibir)  as MONTO_OPER  ");
				// Tablas
				sbQuery.append(" FROM " + sTabla[0] +","+ sTabla[1] +","+ sTabla[5] +","+ sTabla[2] +","+
								sTabla[6] +","+ sTabla[8] +","+ sTabla[9] +","+ sTabla[7] +","+ sTabla[11] +","+ sTabla[14] +","+ sTabla[15] );
				// Condiciones
				sbQuery.append(" WHERE "+ sCondicion[5] +
								" AND "+ sCondicion[9] +
								" AND "+ sCondicion[0] +
								" AND "+ sCondicion[1] +
								" AND "+ sCondicion[6] +
								" AND "+ sCondicion[4] +
								" AND "+ sCondicion[10] +
								" AND "+ sCondicion[13] +
								" AND "+ sCondicion[19] +
								" AND "+ sCondicion[22] +
								" AND "+ sCondicion[25] + iNoEstatusSolic +
								" AND "+ sCondicion[27] + 
								" AND "+ sCondicion[28] );
				// GROUP BY
				sbQuery.append(" GROUP BY  m.cd_nombre "  );
				break;
			default:
				break;
		}	
		log.debug("sbQuery::::::::"+iNoEstatusDocto+":::::: "+sbQuery);
		} catch(Exception e) {
			log.error("RepDoctoSolicxEstatusDEbean::RepDoctoSolicxEstatusDEbean Exception "+e);
			e.printStackTrace();
		} 			
	return sbQuery.toString();		
	} // getQueryPorCambioEstatus()



	public void setTipoOper(String tipoOper) {
		this.tipoOper = tipoOper;
	}


	public String getTipoOper() {
		return tipoOper;
	}  

	public String getBanderaFISO() {
		return banderaFISO;
	}

	public void setBanderaFISO(String banderaFISO) {
		this.banderaFISO = banderaFISO;
	}



} // Fin de la clase.
