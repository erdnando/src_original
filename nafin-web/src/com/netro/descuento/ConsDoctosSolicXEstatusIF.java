package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsDoctosSolicXEstatusIF implements IQueryGeneratorRegExtJS {
    

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsDoctosSolicXEstatusIF.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String estatus;
	private String claveIF;
	private String bTipoFactoraje;
	private String bOperaFactorajeDist;
	private String bOperaFactVencimientoInfonavit;
	private String sOperaFactConMandato;
	private String bOperaFactAutomatico;
   private String generaExp;
	private String idEpo;
	private String fecha;
	public ConsDoctosSolicXEstatusIF() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		if(generaExp != null){
			qrySentencia=new StringBuffer("");
			qrySentencia.append(
			"SELECT   /*+  leading (d) use_nl(ds d p pe e ie i cd m tf)*/ d.ig_numero_docto, pe.cg_pyme_epo_interno AS clave_pyme, "+
			" DECODE (ds.cs_opera_fiso, 'S', 99999, p.in_numero_sirac ) AS in_numero_sirac, DECODE (ds.cs_opera_fiso, 'S', ifs.cg_razon_social, p.cg_razon_social ) AS nombrepyme, "+
			" d.ic_epo AS claveepo, e.cg_razon_social AS nombreepo, ds.ic_if AS claveif, ifs.cg_razon_social AS nombreif, TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS df_fecha_docto, "+
			" TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS df_fecha_venc, d.ic_moneda, m.cd_nombre, tf.cg_nombre AS tipo_factoraje, d.fn_monto, d.fn_porc_anticipo, d.fn_monto - d.fn_monto_dscto AS recursogarantia,"+
			" d.fn_monto_dscto, DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_importe_interes_fondeo, 0), ds.in_importe_interes ) AS in_importe_interes, DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_importe_recibir_fondeo, 0),"+
			" ds.in_importe_recibir ) AS in_importe_recibir, DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_tasa_aceptada_fondeo, 0), ds.in_tasa_aceptada ) AS in_tasa_aceptada, (  d.df_fecha_venc  - DECODE (d.ic_moneda, "+
			" 1, TRUNC (SYSDATE), 54, TO_DATE ('"+fecha+"', 'dd/mm/yyyy')  )  ) plazo, d.ic_estatus_docto, cd.cd_descripcion, DECODE (ds.cs_opera_fiso, 'S', p.cg_razon_social, d.ct_referencia "+
			" ) AS ct_referencia, d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, d.cg_campo5, TO_CHAR (d.df_alta, 'dd/mm/yyyy') AS df_alta, d.ic_documento FROM com_docto_seleccionado ds,"+
			" com_documento d, comcat_pyme p, comrel_pyme_epo pe, comcat_epo e, comrel_if_epo ie, comcat_if i, comcat_estatus_docto cd, comcat_moneda m, comcat_tipo_factoraje tf, comrel_nafin rn,"+
			" comcat_if ifs WHERE ds.ic_documento = d.ic_documento AND ds.ic_if = ifs.ic_if AND ds.ic_epo = d.ic_epo  AND d.ic_pyme = p.ic_pyme  AND d.ic_pyme = d.ic_pyme  AND d.ic_moneda = m.ic_moneda"+
			" AND d.ic_estatus_docto = cd.ic_estatus_docto  AND d.cs_dscto_especial = tf.cc_tipo_factoraje  AND ie.ic_if = i.ic_if(+) AND d.ic_epo = pe.ic_epo AND d.ic_pyme = pe.ic_pyme AND d.ic_epo = e.ic_epo"+
			" AND ie.ic_if(+) = d.ic_beneficiario AND pe.cs_habilitado = 'S'  AND cs_dscto_especial != 'C' AND cd.ic_estatus_docto = 3 AND d.ic_estatus_docto = 3 "+
			//"AND ie.ic_epo(+) = "+idEpo+ AND pe.ic_epo = "+idEpo+"  AND e.ic_epo = "+idEpo+" AND d.ic_epo = "+idEpo+" AND ds.ic_epo ="+idEpo+" 
			"AND rn.ic_epo_pyme_if(+) = d.ic_beneficiario"+
			" AND rn.cg_tipo(+) = 'I'  AND ROWNUM BETWEEN 1 AND 350   AND d.ic_if = "+claveIF+"  AND ds.df_fecha_seleccion >= TRUNC (SYSDATE) AND ds.df_fecha_seleccion < TRUNC (SYSDATE + 1)  ORDER BY p.cg_razon_social " ); 
		} else if(estatus.equals("1") ) {
	
			qrySentencia.append(" SELECT   /*+ ordered use_nl(e b f c ie i d) index (a IN_COM_DOCUMENTO_04_NUK)*/"   +
						"	e.ic_if, "+
						"	(DECODE (c.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || c.cg_razon_social) as  NOMBRE_EPO,  e.IC_EPO,"+
						"	a.ig_numero_docto as NUM_DOCTO ,"   +
						" 	TO_CHAR (a.df_fecha_docto, 'DD/MM/YYYY') FECHA_DOCTO, "+
						" 	TO_CHAR (a.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENC,"   +
						//"  (DECODE (f.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || f.cg_razon_social) as NOMBRE_CLIENTE ,"+ Linea a modificar por FODEA 17
						" decode(e.cs_opera_fiso, 'S', i2.cg_razon_social, 'N', (DECODE (f.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || f.cg_razon_social)) as NOMBRE_CLIENTE ,"+ //FODEA 17
						//" 	e.in_tasa_aceptada as TASA ,"   +	//Linea a modificar por FODEA 17
						"	decode(e.cs_opera_fiso, 'S', e.in_tasa_aceptada_fondeo, 'N', e.in_tasa_aceptada) as TASA ,"	+ //FODEA 17
						"  DECODE( pe.cs_fecha_venc_pyme, 'S', a.df_fecha_venc_pyme, a.df_fecha_venc )" + " - " +
						" 	(CASE  " +
						" 		WHEN a.ic_moneda = 1 OR (a.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
						"    	TRUNC(SYSDATE) " +
						" 		WHEN a.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
						"    	sigfechahabilxepo(a.ic_epo, TRUNC(SYSDATE), 1, '+') " +
						" 	END) AS PLAZO_DIAS, " +
						"  d.cd_nombre AS MONEDA ,  a.ic_moneda as IC_MONEDA,"+
						"	a.fn_monto AS MONTO_DOCTO ,"   +
						" 	a.fn_monto - a.fn_monto_dscto As REC_GARANTIA,  "+
						"  a.fn_porc_anticipo  AS POR_DESC , "+
						" 	a.fn_monto_dscto AS MONTO_DESC ,"+
						//"  e.in_importe_interes AS  MONTO_INT ,  "+	//Linea a modificar por FODEA 17
						"	decode(e.cs_opera_fiso, 'S', e.in_importe_interes_fondeo, 'N', e.in_importe_interes) AS  MONTO_INT , "+ //FODEA 17
						//" 	e.in_importe_recibir AS MONTO_OPER,  "+	//Linea a modificar por FODEA 17
						" decode(e.cs_opera_fiso, 'S', e.in_importe_recibir_fondeo, 'N', e.in_importe_recibir) as MONTO_OPER, "+ //Fodea 17
						"  a.cs_dscto_especial,"   +
						"  DECODE (a.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS BENEFICIARIO,"   +
						"  DECODE (a.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS BANCO_BENEFICIARIO,"   +
						"  DECODE (a.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS SUCURSAL_BENEFICIARIO,"   +
						"  DECODE (a.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS CUENTA_BENEFICIARIO,"   +
						"  DECODE (a.cs_dscto_especial, 'D', a.fn_porc_beneficiario, 'I', a.fn_porc_beneficiario, '') AS PORC_BENEFICIARIO,"   +
						"  DECODE (a.cs_dscto_especial, 'D', e.fn_importe_recibir_benef, 'I', e.fn_importe_recibir_benef, '') AS IMP_RECIBIR,"   +
						"  DECODE (a.cs_dscto_especial, 'D', e.in_importe_recibir - (e.fn_importe_recibir_benef), 'I', e.in_importe_recibir - (e.fn_importe_recibir_benef), '') AS NETO_RECIBIR,"   +
						"  tf.cg_nombre AS TIPO_FACTORAJE,"+
					   "  TO_CHAR (e.DF_PROGRAMACION, 'DD/MM/YYYY') as FECHA_REGISTRO,  "+
						" 	decode(e.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as REFERENCIA, "+
						"	decode(e.cs_opera_fiso, 'S', f.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
						"  FROM com_documento a, com_docto_seleccionado e, com_acuse2 b, " +
						"  		comcat_pyme f, comcat_epo c, comrel_if_epo ie, comcat_if i, comcat_if i2, comcat_moneda d, "   +
						"        comrel_producto_epo pe, " +
						"			comcat_tipo_factoraje tf,"+
					   "        comrel_if_epo_x_producto iexp "+
						"  WHERE a.ic_if = ? "+
						"  AND e.cc_acuse = b.cc_acuse"   +
						"  AND a.ic_moneda = d.ic_moneda"   +
						"  AND a.ic_documento = e.ic_documento"   +
						"  AND e.DF_FECHA_SELECCION >= TRUNC (SYSDATE)"   +
						"  AND e.DF_FECHA_SELECCION < TRUNC(SYSDATE+1)"   +
						//" and c.ic_epo= ? "+ 
						"  AND a.ic_epo = c.ic_epo"   +
						"  AND a.ic_epo = pe.ic_epo"   +
						"  AND pe.ic_producto_nafin = 1 "   +					
						"  AND a.ic_pyme = f.ic_pyme"   +
						"  AND a.ic_estatus_docto = 3"   +
						"  AND a.ic_epo = ie.ic_epo (+)"   +
						"  AND a.ic_beneficiario = ie.ic_if (+)"   +
						"  AND ie.ic_if = i.ic_if (+)"   +
						"  AND e.ic_if = i2.ic_if (+)"   +	//FODEA 17
						"  AND a.cs_dscto_especial != 'C'"   +
						"	AND a.cs_dscto_especial = tf.cc_tipo_factoraje"+
					   " 	AND a.ic_if = iexp.ic_if " +
					   " 	AND a.ic_epo = iexp.ic_epo " +
					   " 	AND iexp.ic_producto_nafin = 1 " + 
						"  ORDER BY c.cg_razon_social,"   +
						"           a.ig_numero_docto,"   +
						"           a.df_fecha_docto,"   +
						"           a.df_fecha_venc,"   +
						"           d.cd_nombre,"   +
						"           a.fn_monto,"   +
						"           a.fn_monto_dscto,"   +
						"           f.cg_razon_social,"   +
						"           e.in_tasa_aceptada,"   +
						"           e.in_importe_interes,"   +
						"           e.in_importe_recibir ") ;						
						conditions.add(claveIF);	
						
		} else   if(estatus.equals("2") ) {
		
			qrySentencia.append(" SELECT   /*+ index(a in_com_documento_04_nuk) index(d cp_comcat_moneda_pk) index(b) use_nl(a,e,b,f,ie,c,i,d)*/"   +
				"  e.ic_if, (DECODE (c.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || c.cg_razon_social) as NOMBRE_EPO , "+
				"	a.ig_numero_docto as  NUM_DOCTO,"   +
				"	TO_CHAR (a.df_fecha_docto, 'DD/MM/YYYY') FECHA_DOCTO, "+
				" TO_CHAR (a.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENC,"   +
				//"  (DECODE (f.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || f.cg_razon_social) as NOMBRE_CLIENTE ,"+ Linea a modificar por FODEA 17
				" decode(e.cs_opera_fiso, 'S', i2.cg_razon_social, 'N', (DECODE (f.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || f.cg_razon_social)) as NOMBRE_CLIENTE ,"+ //Fodea 17
				//" e.in_tasa_aceptada as TASA ,"   +	Linea a modificar por FODEA 17
				"	decode(e.cs_opera_fiso, 'S', e.in_tasa_aceptada_fondeo, 'N', e.in_tasa_aceptada) as TASA ,"	+ //FODEA 17
				"	DECODE( pe.cs_fecha_venc_pyme, 'S', a.df_fecha_venc_pyme, a.df_fecha_venc )" + " - " +
				"	(CASE  " +
				" 	WHEN a.ic_moneda = 1 OR (a.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
				"  TRUNC(SYSDATE) " +
				" 	WHEN a.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
				"  sigfechahabilxepo(a.ic_epo, TRUNC(SYSDATE), 1, '+') " +
				" 	END) AS PLAZO_DIAS, " +
				"	d.cd_nombre as MONEDA , a.ic_moneda as IC_MONEDA,  a.fn_monto AS MONTO_DOCTO ,"   +
				" 	a.fn_monto - a.fn_monto_dscto As REC_GARANTIA,  "+
				"	a.fn_porc_anticipo AS POR_DESC  , a.fn_monto_dscto AS MONTO_DESC ,"+
				//" e.in_importe_interes AS MONTO_INT , "+	Linea a modificar por FODEA 17
				"	decode(e.cs_opera_fiso, 'S', e.in_importe_interes_fondeo, 'N', e.in_importe_interes) AS  MONTO_INT , "+ //FODEA 17
				//"	e.in_importe_recibir  AS MONTO_OPER , "+	Linea a modificar por FODEA 17
				" decode(e.cs_opera_fiso, 'S', e.in_importe_recibir_fondeo, 'N', e.in_importe_recibir) as MONTO_OPER, "+ //Fodea 17
				"	a.cs_dscto_especial,"   +
				"	DECODE (a.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS BENEFICIARIO,"   +
				"	DECODE (a.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS BANCO_BENEFICIARIO,"   +
				"	DECODE (a.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS SUCURSAL_BENEFICIARIO,"   +
				"  DECODE (a.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS CUENTA_BENEFICIARIO,"   +
				"	DECODE (a.cs_dscto_especial, 'D', a.fn_porc_beneficiario, 'I', a.fn_porc_beneficiario, '') AS PORC_BENEFICIARIO,"   +
				"	DECODE (a.cs_dscto_especial, 'D', e.fn_importe_recibir_benef, 'I', e.fn_importe_recibir_benef, '') AS IMP_RECIBIR,"   +
				" 	DECODE (a.cs_dscto_especial, 'D', e.in_importe_recibir - (e.fn_importe_recibir_benef), 'I', e.in_importe_recibir - (e.fn_importe_recibir_benef), '') AS NETO_RECIBIR,"   +
				"	tf.cg_nombre as TIPO_FACTORAJE,"+
				" 	decode(e.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as REFERENCIA,  "+ //Fodea 031-2011
				" '' as FECHA_REGISTRO, " +
				"	decode(e.cs_opera_fiso, 'S', f.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				"  FROM com_documento a, com_docto_seleccionado e, com_acuse2 b, " +
				"	comcat_pyme f, comrel_if_epo ie, comcat_epo c, comrel_producto_epo pe, comcat_if i, comcat_if i2, comcat_moneda d, "   +
				"	comcat_tipo_factoraje tf,"+
				"	comrel_if_epo_x_producto iexp "+
				"  WHERE a.ic_documento = e.ic_documento"   +
				"    AND e.cc_acuse = b.cc_acuse"   +
				"    AND a.ic_moneda = d.ic_moneda"   +
				"    AND a.ic_epo = c.ic_epo"   +
				"    AND a.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = 1 "   +
				"    AND a.ic_pyme = f.ic_pyme"   +
				"    AND a.ic_epo = ie.ic_epo (+)"   +
				"    AND a.ic_beneficiario = ie.ic_if (+)"   +
				"    AND ie.ic_if = i.ic_if (+)"   +
				"  AND e.ic_if = i2.ic_if (+)"   +	//FODEA 17
				"    AND a.ic_if = ? "+
				"    AND a.ic_estatus_docto = 24"   +
				"    AND a.cs_dscto_especial != 'C'"   +
				"	   AND a.cs_dscto_especial = tf.cc_tipo_factoraje"+
				" 	 AND a.ic_if = iexp.ic_if " +
				" 	 AND a.ic_epo = iexp.ic_epo " +
				" 	 AND iexp.ic_producto_nafin = 1 " + 
				"  ORDER BY c.cg_razon_social,"   +
				"           a.ig_numero_docto,"   +
				"           a.df_fecha_docto,"   +
				"           a.df_fecha_venc,"   +
				"           d.cd_nombre,"   +
				"           a.fn_monto,"   +
				"           a.fn_monto_dscto,"   +
				"           f.cg_razon_social,"   +
				"           e.in_tasa_aceptada,"   +
				"           e.in_importe_interes,"   +
				"           e.in_importe_recibir" );
				conditions.add(claveIF);
				//conditions.add(claveIF);
				
		} else   if(estatus.equals("3") ) {
		
			qrySentencia.append(" SELECT   /*+ index(s) use_nl(s d ds p ie e i m) */ "   +
				"        (DECODE (h.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || h.cg_razon_social) AS NOMBRE_EPO,"   +
				//"        (DECODE (c.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || c.cg_razon_social) AS NOMBRE_PYME, "+ //Linea a modificar por FODEA 17
				"	decode(e.cs_opera_fiso, 'S', i2.cg_razon_social, 'N', (DECODE (c.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || c.cg_razon_social)) AS NOMBRE_PYME, "+ //FODEA 17
				"			f.cg_razon_social AS nombreif,"   +
				"        a.ig_numero_docto AS NUM_DOCTO , "+
				"			b.cc_acuse AS NUM_ACUSE_IF , " +
				" 			SUBSTR (g.ic_folio, 1, 10) || '-' || SUBSTR (g.ic_folio, 11, 1) as NUM_SOLICITUD ,  "+
				"			d.cd_nombre as MONEDA ,"   +
				"        a.ic_moneda as IC_MONEDA , a.fn_monto as MONTO_DOCTO , "+
				"			a.fn_porc_anticipo as POR_DESC , "  +
				" 			a.fn_monto_dscto as  MONTO_DESC , "+
				" 			a.fn_monto - a.fn_monto_dscto As REC_GARANTIA,  "+
				//" 			e.in_tasa_aceptada as TASA ,  "+	Linea a modificar por FODEA 17
				"	decode(e.cs_opera_fiso, 'S', e.in_tasa_aceptada_fondeo, 'N', e.in_tasa_aceptada) as TASA ,"	+ //FODEA 17
				//"			e.in_importe_interes as MONTO_INT ,"   +	Linea a modificar por FODEA 17
				"	decode(e.cs_opera_fiso, 'S', e.in_importe_interes_fondeo, 'N', e.in_importe_interes) AS  MONTO_INT , "+ //FODEA 17
				//"        e.in_importe_recibir as MONTO_OPER  , "+	Linea a modificar por FODEA 17
				" decode(e.cs_opera_fiso, 'S', e.in_importe_recibir_fondeo, 'N', e.in_importe_recibir) as MONTO_OPER, "+ //Fodea 17
				"			'Comunidades' AS ORIGEN,  "+
				"			TO_CHAR (a.df_fecha_docto, 'DD/MM/YYYY') as FECHA_EMISION ,"   +
				"        TO_CHAR (a.df_fecha_venc, 'DD/MM/YYYY') as FECHA_VENC , "   +
				"        g.ig_plazo as PLAZO ,"   +
				"        a.cs_dscto_especial, "+
				"		   DECODE (a.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS BANCO_BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS SUCURSAL_BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS CUENTA_BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', a.fn_porc_beneficiario, 'I', a.fn_porc_beneficiario, '') AS PORC_BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', e.fn_importe_recibir_benef, 'I', e.fn_importe_recibir_benef, '') AS IMP_RECIBIR,"   +
				"        DECODE (a.cs_dscto_especial, 'D', e.in_importe_recibir - (e.fn_importe_recibir_benef), 'I', e.in_importe_recibir - (e.fn_importe_recibir_benef), '') AS NETO_RECIBIR,"   +
				"			tf.cg_nombre as TIPO_FACTORAJE,"+
				" 			decode(e.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as REFERENCIA, "+
				"	decode(e.cs_opera_fiso, 'S', c.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				"   FROM com_acuse3 b,"   +
				"        com_solicitud g,"   +
				"        com_docto_seleccionado e,"   +
				"        com_documento a,"   +
				"        comcat_moneda d,"   +
				"        comcat_if f,"   +
				"        comcat_epo h,"   +
				"        comrel_producto_epo pe, " +
				"        comrel_if_epo ie,"   +
				"        comcat_if i,"   +
				"        comcat_if i2,"   +
				"        comcat_pyme c,"   +
				"			comcat_tipo_factoraje tf"+
				"  WHERE a.ic_epo = h.ic_epo"   +
				"    AND a.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = 1 "   +
				"    AND a.ic_pyme = c.ic_pyme"   +
				"    AND g.cc_acuse = b.cc_acuse"   +
				"    AND a.ic_moneda = d.ic_moneda"   +
				"    AND a.ic_if = f.ic_if"   +
				"    AND a.ic_documento = e.ic_documento"   +
				"    AND e.ic_documento = g.ic_documento"   +
				"    AND b.df_fecha_hora >= TRUNC (SYSDATE)"   +
				"    AND b.df_fecha_hora < TRUNC (SYSDATE+1)"   +
				"    AND a.ic_epo = ie.ic_epo (+)"   +
				"    AND a.ic_beneficiario = ie.ic_if (+)"   +
				"    AND ie.ic_if = i.ic_if (+)"   +
				"  AND e.ic_if = i2.ic_if (+)"   +	//FODEA 17
				"    AND g.ic_estatus_solic = 1"   +
				"    AND a.ic_if = ? "+
				"    AND a.cs_dscto_especial != 'C'"   +
				"	  AND a.cs_dscto_especial = tf.cc_tipo_factoraje"+
				"  ORDER BY NOMBRE_EPO, NUM_DOCTO, MONEDA, MONTO_DOCTO, MONTO_DESC,  "+
				" nombreif, TASA, MONTO_INT, MONTO_OPER") ;
				conditions.add(claveIF);
		
		} else   if(estatus.equals("4") ) {
		
			qrySentencia.append(" SELECT   /*+ index(s in_com_solicitud_02_nuk) use_nl(s,ds,d,p,e,ie,i) */"   +
				"        ds.ic_if, (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social) AS NOMBRE_EPO,"   +
				//"        (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social) AS NOMBRE_PYME,"   + Linea a modificar por FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', i2.cg_razon_social, 'N', (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)) AS NOMBRE_PYME, "+ //FODEA 17
				"        SUBSTR (s.ic_folio, 1, 10) || '-' || SUBSTR (s.ic_folio, 11, 1) as NUM_SOLICITUD ,  "+
				"			d.ig_numero_docto as NUM_DOCTO ,"   +
				"        m.cd_nombre as MONEDA ,"+
				"			d.ic_moneda as IC_MONEDA, "+
				"			d.fn_monto AS MONTO_DOCTO , "+
				"			d.fn_porc_anticipo AS POR_DESC , "+
				"			d.fn_monto_dscto AS MONTO_DESC ,"   +
				//"        ds.in_tasa_aceptada AS TASA, "+	//Linea a modificar por FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada) as TASA ,"	+ //FODEA 17
				//"			ds.in_importe_interes AS MONTO_INT, "+	Linea a modificar por FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', ds.in_importe_interes_fondeo, 'N', ds.in_importe_interes) AS  MONTO_INT , "+ //FODEA 17
				//"			ds.in_importe_recibir as MONTO_OPER , "+	Linea a modificar por FODEA 17
				" decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir) as MONTO_OPER, "+ //Fodea 17
				"			'Internas' as ORIGEN ,"   +
				" 			d.fn_monto - d.fn_monto_dscto As REC_GARANTIA,  "+
				"        TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') as FECHA_EMISION,  "+
				"			TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') as FECHA_VENC ,"   +
				"        s.ig_plazo as PLAZO ,"   +
				"        d.cs_dscto_especial, "+
				"			DECODE (d.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS BANCO_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS SUCURSAL_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS CUENTA_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, '') AS PORC_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, '') AS IMP_RECIBIR,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '')  AS NETO_RECIBIR ,"   +
				"			tf.cg_nombre AS TIPO_FACTORAJE , "+	
				"			'' as NUM_ACUSE_IF,  "+
				"			'' as REFERENCIA, "+
				"	decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				"   FROM com_solicitud s,"   +
				"        com_docto_seleccionado ds,"   +
				"        com_documento d,"   +
				"        comcat_moneda m,"   +
				"        comcat_epo e,"   +
				"        comrel_producto_epo pe, " +
				"        comrel_if_epo ie,"   +
				"        comcat_if i,"   +
				"        comcat_if i2,"   +
				"        comcat_pyme p,"   +
				"			comcat_tipo_factoraje tf"+
				"  WHERE s.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_documento = d.ic_documento"   +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = 1 "   +
			   "    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_epo = e.ic_epo"   +
				"    AND d.ic_epo = ie.ic_epo (+)"   +
				"    AND d.ic_beneficiario = ie.ic_if (+)"   +
				"    AND ie.ic_if = i.ic_if (+)"   +
				"  AND ds.ic_if = i2.ic_if (+)"   +	//FODEA 17
				"    AND s.ic_estatus_solic = 2"   +
				"    AND d.ic_if = ? " +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND s.df_fecha_solicitud >= TRUNC (SYSDATE)"   +
				"    AND s.df_fecha_solicitud < TRUNC (SYSDATE+1)"   +
				"    AND d.cs_dscto_especial != 'C'"   +
				"	  AND d.cs_dscto_especial = tf.cc_tipo_factoraje"+
				"  ORDER BY NOMBRE_EPO,"   +
				"           NUM_DOCTO,"   +
				"           MONEDA,"   +
				"           MONTO_DOCTO,"   +
				"           MONTO_DESC,"   +
				"           NOMBRE_PYME,"   +
				"           TASA,"   +
				"           MONTO_INT,"   +
				"           MONTO_OPER" );
				conditions.add(claveIF);
				
		} else   if(estatus.equals("5") ) {	
		
			qrySentencia.append("SELECT   /*+ index(s in_com_solicitud_02_nuk) use_nl(s d ds p ie e i) */	"   +
				"        ds.ic_if, (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social) AS NOMBRE_EPO,"   +
				//"        (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social) AS NOMBRE_PYME,"   + Linea a modificar por FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', i2.cg_razon_social, 'N', (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)) AS NOMBRE_PYME, "+ //FODEA 17
				"        SUBSTR (s.ic_folio, 1, 10) || '-' || SUBSTR (s.ic_folio, 11, 1) as NUM_SOLICITUD  ,"+
				"			d.ig_numero_docto as NUM_DOCTO ,"   +
				"        m.cd_nombre as MONEDA, d.ic_moneda AS IC_MONEDA, "+
				"			d.fn_monto AS MONTO_DOCTO , "+
				"			d.fn_porc_anticipo as POR_DESC ," +
				"			d.fn_monto_dscto as MONTO_DESC ,"   +
				"			d.fn_monto - d.fn_monto_dscto as REC_GARANTIA ,"   +				
				//"        ds.in_tasa_aceptada as TASA ,"+ Linea a modificar por FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada) as TASA ,"	+ //FODEA 17
				//"			ds.in_importe_recibir as MONTO_OPER , "+	Linea a modificar por FODEA 17
				" decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir) as MONTO_OPER, "+ //Fodea 17
				"			'Internas' AS ORIGEN,"   +
				"        TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') as FECHA_EMISION , "+
				"			TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') as FECHA_VENC ,"   +			
				"        s.ig_plazo as  PLAZO ,"   +
				"        d.cs_dscto_especial, "+
				"			DECODE (d.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS BANCO_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS SUCURSAL_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS CUENTA_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, '') AS PORC_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, '') AS IMP_RECIBIR,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '') AS NETO_RECIBIR,"   +
				"			tf.cg_nombre AS TIPO_FACTORAJE,"+
				"			'0'AS MONTO_INT , "+
				"			'' as PLAZO_DIAS , "+
				"			'' as NUM_ACUSE_IF , "+
				" 			decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as REFERENCIA, "+
				"	decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				"     FROM com_solicitud s,"   +
				"          com_documento d,"   +
				"          com_docto_seleccionado ds,"   +
				"          comcat_pyme p,"   +
				"          comrel_if_epo ie,"   +
				"          comcat_epo e,"   +
				"          comrel_producto_epo pe, " +
				"          comcat_if i,"   +
				"          comcat_if i2,"   +
				"          comcat_moneda m,"  +
				"			  comcat_tipo_factoraje tf"+
				"  WHERE s.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_documento = d.ic_documento"   +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = 1 "   +				
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_epo = e.ic_epo"   +
				"    AND d.ic_epo = ie.ic_epo (+)"   +
				"    AND d.ic_beneficiario = ie.ic_if (+)"   +
				"    AND ie.ic_if = i.ic_if (+)"   +
				"  AND ds.ic_if = i2.ic_if (+)"   +	//FODEA 17
				"    AND s.ic_estatus_solic = 3"   +
				"    AND d.ic_if = ? " +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND s.df_fecha_solicitud >= TRUNC (SYSDATE)"   +
				"    AND s.df_fecha_solicitud < TRUNC (SYSDATE+1)"   +
				"    AND d.cs_dscto_especial != 'C'"   +
				"	  AND d.cs_dscto_especial = tf.cc_tipo_factoraje"+
				"  ORDER BY NOMBRE_EPO,"   +
				"           NUM_DOCTO,"   +
				"           MONEDA,"   +
				"           MONTO_DESC,"   +
				"           NOMBRE_PYME,"   +
				"           TASA,"   +
				"           IMP_RECIBIR")  ;
				conditions.add(claveIF);
				
		} else   if(estatus.equals("11") ) {
		
			qrySentencia.append(" SELECT   /*+ index(s) use_nl(s d ds p ie e i m) */ "   +
				"        (DECODE (h.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || h.cg_razon_social) AS NOMBRE_EPO,"   +
				"        (DECODE (c.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || c.cg_razon_social) AS NOMBRE_PYME, "+ 
				" 			f.cg_razon_social AS nombreif,"   +
				"        a.ig_numero_docto as NUM_DOCTO , "+
				"			b.cc_acuse AS NUM_ACUSE_IF , " +
				" 			SUBSTR (g.ic_folio, 1, 10) || '-' || SUBSTR (g.ic_folio, 11, 1) as NUM_SOLICITUD,"+
				" 			d.cd_nombre as MONEDA,"   +
				"        a.ic_moneda as IC_MONEDA, "+
				"			a.fn_monto AS MONTO_DOCTO ,"+
				"			a.fn_porc_anticipo AS POR_DESC,  "+
				"			a.fn_monto_dscto AS MONTO_DESC,  "+
				"			a.fn_monto - a.fn_monto_dscto AS REC_GARANTIA,  "+				
				"			e.in_tasa_aceptada AS TASA , "+
				"			e.in_importe_interes AS MONTO_INT ,"   + 
				"        e.in_importe_recibir AS MONTO_OPER ,"+ 
				" 			'Comunidades' AS ORIGEN, " +
				"			TO_CHAR (a.df_fecha_docto, 'DD/MM/YYYY') AS FECHA_EMISION ,"   +
				"        TO_CHAR (a.df_fecha_venc, 'DD/MM/YYYY') AS FECHA_VENC , "   +
				"        g.ig_plazo AS PLAZO ,"   +
				"        a.cs_dscto_especial, "+
				"		   DECODE (a.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS BANCO_BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS SUCURSAL_BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS CUENTA_BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', a.fn_porc_beneficiario, 'I', a.fn_porc_beneficiario, '') AS PORC_BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', e.fn_importe_recibir_benef, 'I', e.fn_importe_recibir_benef, '') AS IMP_RECIBIR,"   +
				"        DECODE (a.cs_dscto_especial, 'D', e.in_importe_recibir - (e.fn_importe_recibir_benef), 'I', e.in_importe_recibir - (e.fn_importe_recibir_benef), '') AS NETO_RECIBIR ,"   +
				"	tf.cg_nombre as TIPO_FACTORAJE,"+			
				" 	decode(e.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as REFERENCIA, "+
                                "       '' REFERENCIA_PYME "+ //Agregado por FODEA 17
				"   FROM com_acuse3 b,"   +
				"        com_solicitud g,"   +
				"        com_docto_seleccionado e,"   +
				"        com_documento a,"   +
				"        comcat_moneda d,"   +
				"        comcat_if f,"   +
				"        comcat_epo h,"   +
				"        comrel_producto_epo pe, " +
				"        comrel_if_epo ie,"   +
				"        comcat_if i,"   +
				//"        comcat_if i2,"   +
				"        comcat_pyme c,"   +
				"        comcat_tipo_factoraje tf"+
				"  WHERE a.ic_epo = h.ic_epo"   +
				"    AND a.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = 1 "   +
				"    AND a.ic_pyme = c.ic_pyme"   +
				"    AND g.cc_acuse = b.cc_acuse"   +
				"    AND a.ic_moneda = d.ic_moneda"   +
				"    AND a.ic_if = f.ic_if"   +
				"    AND a.ic_documento = e.ic_documento"   +
				"    AND e.ic_documento = g.ic_documento"   +
                                "    AND b.ic_producto_nafin = 1 "   +
				"    AND b.df_fecha_hora >= TRUNC (SYSDATE)"   +
				"    AND b.df_fecha_hora < TRUNC (SYSDATE+1)"   +
				"    AND a.ic_epo = ie.ic_epo (+)"   +
				"    AND a.ic_beneficiario = ie.ic_if (+)"   +
				"    AND ie.ic_if = i.ic_if (+)"   +
				"    AND g.ic_estatus_solic = 10 "   + // Operado con Fondeo Propio
				"    AND e.ic_if = ? "+
				"    AND a.cs_dscto_especial != 'C'"   +
				"    AND a.cs_dscto_especial = tf.cc_tipo_factoraje"+
				"  ORDER BY NOMBRE_EPO, NUM_DOCTO, MONEDA, MONTO_DOCTO, MONTO_DESC, nombreif, TASA, MONTO_INT, MONTO_OPER");
				conditions.add(claveIF);
				
		} else   if(estatus.equals("6") ) {
		
			qrySentencia.append(" SELECT   /*+ use_nl(d,m,e,ie,i,p,s,ds) */"   +
				"        d.ic_if, (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social) AS NOMBRE_EPO,"   +
				//"        (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social) AS NOMBRE_PYME,"   +	Linea a modificar por FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', i2.cg_razon_social, 'N', (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)) AS NOMBRE_PYME, "+ //FODEA 17
				"        SUBSTR (s.ic_folio, 1, 10) || '-' || SUBSTR (s.ic_folio, 11, 1) as NUM_SOLICITUD  ,"+
				"			d.ig_numero_docto as NUM_DOCTO ,"   +
				"        m.cd_nombre as MONEDA , m.ic_moneda AS IC_MONEDA, "+
				"			d.fn_monto AS MONTO_DOCTO , "+
				//"ds.in_tasa_aceptada AS TASA  , "+ Linea a modificar por FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada) as TASA ,"	+ //FODEA 17
				"			d.fn_porc_anticipo AS POR_DESC ,"   +
				"        d.fn_monto_dscto AS  MONTO_DESC ,   d.fn_monto -  d.fn_monto_dscto AS  REC_GARANTIA ,  "+				
				"			TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENC, 'Internas' AS ORIGEN,"   +
				"        d.cs_dscto_especial, "+
				"			DECODE (d.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS BANCO_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS SUCURSAL_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS CUENTA_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', d.fn_porc_beneficiario, 'I', d.fn_porc_beneficiario, '') AS PORC_BENEFICIARIO,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ds.fn_importe_recibir_benef, 'I', ds.fn_importe_recibir_benef, '') AS IMP_RECIBIR,"   +
				"        DECODE (d.cs_dscto_especial, 'D', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), 'I', ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '')   AS NETO_RECIBIR ,"   +
				"			tf.cg_nombre AS TIPO_FACTORAJE,"+				
				" 			decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as REFERENCIA, "+			
				"			'0' as MONTO_INT ,  "+
				"			'0' as MONTO_OPER,   "+
				"	decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', '') as REFERENCIA_PYME "+ //Agregado por FODEA 17
				"   FROM com_documento d,"   +
				"        comcat_moneda m,"   +
				"        comcat_epo e,"   +
				"        comrel_producto_epo pe, " +
				"        comrel_if_epo ie,"   +
				"        comcat_if i,"   +
				"        comcat_if i2,"   +
				"        comcat_pyme p,"   +
				"        com_solicitud s,"   +
				"        com_docto_seleccionado ds,"   +
				"			comcat_tipo_factoraje tf"+
				"  WHERE d.ic_if = ? "   +
				"    AND d.ic_epo = e.ic_epo"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = 1 "   +				
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND d.ic_documento = s.ic_documento"   +
				"    AND d.df_fecha_venc >= TRUNC (SYSDATE)"   +
				"    AND d.df_fecha_venc < TRUNC (SYSDATE+1)"   +
				"    AND d.ic_epo = ie.ic_epo (+)"   +
				"    AND d.ic_beneficiario = ie.ic_if (+)"   +
				"    AND ie.ic_if = i.ic_if (+)"   +
				"  AND ds.ic_if = i2.ic_if (+)"   +	//FODEA 17
				"    AND d.ic_estatus_docto = 11"   +
				"    AND d.cs_dscto_especial != 'C'"   +
				"	  AND d.cs_dscto_especial = tf.cc_tipo_factoraje"+
				"  ORDER BY NOMBRE_EPO,"   +
				"           NUM_DOCTO,"   +
				"           FECHA_VENC,"   +
				"           MONEDA,"   +
				"           MONTO_DOCTO,"   +
				"           MONTO_DESC,"   +
				"           NOMBRE_PYME,"   +
				"           TASA");
				conditions.add(claveIF);
				
		} else   if(estatus.equals("9") ) {
		
			qrySentencia.append(" SELECT   /*+ ordered use_nl(e b f c ie i d) index (a IN_COM_DOCUMENTO_04_NUK)*/"   +
				"        e.ic_if, (DECODE (c.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || c.cg_razon_social) AS NOMBRE_EPO ," +
				"			a.ig_numero_docto AS NUM_DOCTO ,"   +
				"        TO_CHAR (a.df_fecha_docto, 'DD/MM/YYYY') AS  FECHA_EMISION, "+
				"			TO_CHAR (a.df_fecha_venc, 'DD/MM/YYYY') FECHA_VENC,"   +
				"        (DECODE (f.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || f.cg_razon_social) AS NOMBRE_PYME, "+ 
				"			e.in_tasa_aceptada AS TASA ,"   +	
				"    DECODE (pe.cs_fecha_venc_pyme,  "+
            "       'S', a.df_fecha_venc_pyme,   "+
            "       a.df_fecha_venc   "+
            "     )   - (CASE   "+
            "    WHEN a.ic_moneda = 1   "+
			   "    OR (a.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P')   "+
            "        THEN sigfechahabilxepo (a.ic_epo, TRUNC (SYSDATE), 1, '+')  "+
            "    WHEN a.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P'  "+
            "      THEN sigfechahabilxepo (a.ic_epo, TRUNC (SYSDATE), 2, '+')  "+
            " 	END  "+
            " 	) AS PLAZO_DIAS, "+
				"        d.cd_nombre  as MONEDA, a.ic_moneda IC_MONEDA, a.fn_monto AS MONTO_DOCTO ,"   +
				"        a.fn_porc_anticipo AS POR_DESC , "+
				"			a.fn_monto_dscto AS MONTO_DESC , "+
				"			e.in_importe_interes AS MONTO_INT , "+	
				"			e.in_importe_recibir AS   IMP_RECIBIR , a.cs_dscto_especial,"   +
				"        DECODE (a.cs_dscto_especial, 'D', i.cg_razon_social, 'I', i.cg_razon_social, '') AS BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', ie.cg_banco, 'I', ie.cg_banco, '') AS banco_benef,"   +
				"        DECODE (a.cs_dscto_especial, 'D', ie.ig_sucursal, 'I', ie.ig_sucursal, '') AS sucursal_benef,"   +
				"        DECODE (a.cs_dscto_especial, 'D', ie.cg_num_cuenta, 'I', ie.cg_num_cuenta, '') AS cta_benef,"   +
				"        DECODE (a.cs_dscto_especial, 'D', a.fn_porc_beneficiario, 'I', a.fn_porc_beneficiario, '') AS PORC_BENEFICIARIO,"   +
				"        DECODE (a.cs_dscto_especial, 'D', e.fn_importe_recibir_benef, 'I', e.fn_importe_recibir_benef, '') AS importe_recibir,"   +
				"        DECODE (a.cs_dscto_especial, 'D', e.in_importe_recibir - (e.fn_importe_recibir_benef), 'I', e.in_importe_recibir - (e.fn_importe_recibir_benef), '') AS NETO_RECIBIR,"   +
				"        tf.cg_nombre AS TIPO_FACTORAJE,"+
				"        (DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social) AS INTERMEDIARIO ,"   +
				"        b.cc_acuse as NUM_ACUSE,"   +
				"		 	cm.cg_razon_social as MANDATARIO, " +
				"		 	a.ct_referencia as REFERENCIA, "+
				"      	TO_CHAR (e.df_fecha_seleccion, 'DD/MM/YYYY') AS FECHA_REGISTRO, " +      
				" 			decode(e.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as REFERENCIA_TASA , "+
				"       '0' AS MONTO_OPER   " +
				"   FROM com_documento a, com_docto_seleccionado e, com_acuse2 b, " +
				"        comcat_pyme f, comcat_epo c, comrel_if_epo ie, comcat_if i, comcat_moneda d, "   +
				"        comrel_producto_epo pe, " +
				"			comcat_tipo_factoraje tf, "+
				"			comcat_mandante cm, "+
				"        comrel_if_epo_x_producto iexp "+
				"  WHERE e.ic_if = ? "+
				"    AND e.cc_acuse = b.cc_acuse"   +
				"    AND a.ic_moneda = d.ic_moneda"   +
				"    AND a.ic_documento = e.ic_documento"   +
				"    AND a.ic_mandante = cm.ic_mandante(+)"   +
				"    AND b.df_fecha_hora >= TRUNC (SYSDATE)"   +
				"    AND b.df_fecha_hora < TRUNC(SYSDATE+1)"   +
				"    AND a.ic_epo = c.ic_epo"   +
				"    AND a.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = 1 "   +
				"    AND a.ic_pyme = f.ic_pyme"   +
				"    AND a.ic_estatus_docto = 26"   +
				"    AND a.ic_epo = ie.ic_epo (+)"   +
				"    AND a.ic_beneficiario = ie.ic_if (+)"   +
				"    AND ie.ic_if = i.ic_if (+)"   +
				"    AND a.cs_dscto_especial != 'C'"   +
				"	  AND a.cs_dscto_especial = tf.cc_tipo_factoraje"+
				"	  AND a.ic_if                = iexp.ic_if  " +
				" 	  AND a.ic_epo               = iexp.ic_epo " +
				" 	  AND iexp.ic_producto_nafin = 1           " + 
				"  ORDER BY c.cg_razon_social,"   +
				"           a.ig_numero_docto,"   +
				"           a.df_fecha_docto,"   +
				"           a.df_fecha_venc,"   +
				"           d.cd_nombre,"   +
				"           a.fn_monto,"   +
				"           a.fn_monto_dscto,"   +
				"           f.cg_razon_social,"   +
				"           e.in_tasa_aceptada,"   +
				"           e.in_importe_interes,"   +
				"           e.in_importe_recibir" ) ;
				conditions.add(claveIF);
		
		}
		
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
	
				
		String nombre_epo ="" , num_docto ="", fecha_docto  ="", fecha_venc  ="", nombre_cliente  ="", tasa ="", 
				 plazo_dias  ="",  moneda ="", tipo_factoraje ="", monto_docto ="", por_desc ="", rec_garantia ="", 
				 monto_desc ="", monto_int ="", monto_oper ="", beneficiario ="", banco_beneficiario ="", sucursal_beneficiario ="", 
				 cuenta_beneficiario ="", por_beneficiario  ="", imp_recibir ="", neto_recibir ="", fecha_registro ="", 
				 referencia="", ic_moneda ="", fecha_emision ="",  num_solicitud ="", plazo ="", origen ="", num_acuse_if ="",
				 nombre_pyme ="", referencia_pyme= "";
			
		double nacional = 0, nacional_desc = 0, nacional_int = 0, nacional_descIF = 0, dolares = 0, dolares_desc = 0,  dolares_int = 0,  dolares_descIF = 0;
		int  doc_nacional =0,doc_dolares =0;
	
		try {
		
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(), 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			}
				
			if(tipo.equals("PDF") && estatus.equals("1")) {
				int column =0;
				column = 15; //era 15
				if(bTipoFactoraje.equals("N")) { column +=3; } //era 2
				
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Seleccionada PYME","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(column,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Cliente/Cedente ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo (d�as) ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Monto Int. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje.equals("S")) {
					pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);		
					pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Banco Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Cuenta Beneficiaria","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER); 
					pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Registro Operaci�n ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia Tasa ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER,3);
				}else if(bTipoFactoraje.equals("N")) {
					pdfDoc.setCell("Fecha Registro Operaci�n ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia Tasa ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
				}
				//pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
			}	
				
			if(tipo.equals("PDF") && estatus.equals("2")) {
				int column = 15; //era 15
				if(bTipoFactoraje.equals("N")) { column +=2; }
				
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("En Proceso de Autorizaci�n IF","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(column,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Cliente/Cedente ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo (d�as) ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Monto Int. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje.equals("S")) {
					pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Banco Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Cuenta Beneficiaria","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER); 
					pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia Tasa ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER,4);
				}else if(bTipoFactoraje.equals("N")) {
					pdfDoc.setCell("Referencia Tasa ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
				}
			}
				
				
			if(tipo.equals("PDF") &&  ( estatus.equals("3")  )) {
				
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Seleccionada IF","formas",ComunesPDF.LEFT); 				
				pdfDoc.addTable();
				
				pdfDoc.setTable(12,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Solicitud ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar ","celda01",ComunesPDF.CENTER);
					
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);					
				pdfDoc.setCell("Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Monto Int. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Origen ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Acuse IF ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje.equals("S")) {
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 4);	//era 5				
					pdfDoc.setCell("C","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);	
					if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S")){ 
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Cuenta Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);	
					}else if (bOperaFactorajeDist.equals("N") || bOperaFactVencimientoInfonavit.equals("N") ){ 
						pdfDoc.setCell("Referencia Tasa...","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 3); //era 4
					}
					pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 1); //era 2
				}else if(bTipoFactoraje.equals("N")) {
					pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 3); //era 4
				}
			}
			if(tipo.equals("PDF") && estatus.equals("11")) {
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Operado con Fondeo Propio ","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(12,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Solicitud ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar ","celda01",ComunesPDF.CENTER);
					
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);					
				pdfDoc.setCell("Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Monto Int. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Origen ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Acuse IF ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje.equals("S")) {
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 5);				
					pdfDoc.setCell("C","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);	
					if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S")){ 
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Cuenta Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);	
					}else if (bOperaFactorajeDist.equals("N") || bOperaFactVencimientoInfonavit.equals("N") ){ 
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 4); 
					}
					pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 2);
				}else if(bTipoFactoraje.equals("N")) {
					pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 4); 
				}
			}
			if(tipo.equals("PDF") && estatus.equals("4")) {
				int col = 18;
				if(bTipoFactoraje.equals("S")){ col=17; }
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("En Proceso","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				pdfDoc.setTable(col,100);	//era 17
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Cliente/Cedente ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Solicitud ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento ","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Monto Int. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Origen ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje.equals("S")) {
					pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
					if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Cuenta Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
					}else {
						pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER,7);	
					}
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER,7);
				}else
					pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
			}	
			
			
			if(tipo.equals("PDF") && estatus.equals("5")) {	
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Operada","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				pdfDoc.setTable(15,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Cliente/Cedente ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Solicitud ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha  Emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento ","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Monto a Operar ","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Origen ","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje.equals("S")) {
					pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);			
					if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Cuenta Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 3);
					}else  if (bOperaFactorajeDist.equals("N") || bOperaFactVencimientoInfonavit.equals("N") ){
						pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER,10);	//era 11
					}	
				}else if(bTipoFactoraje.equals("N")) {
					pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 11);	
				}
			}	
						
			if(tipo.equals("PDF") && estatus.equals("6")) {
			
				int num = 14;   
				if(!bTipoFactoraje.equals("S")) { num +=2; }
				
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Operada Pagada","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(num,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Cliente/Cedente ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Solicitud ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto a Descontar ","celda01",ComunesPDF.CENTER);							
				pdfDoc.setCell("Fecha de Vencimiento ","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Fecha de Pago ","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Origen ","celda01",ComunesPDF.CENTER);
				
				if(bTipoFactoraje.equals("S")) {
						pdfDoc.setCell("B","celda01",ComunesPDF.CENTER); 
						pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);	
					if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Cuenta Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);	
						pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER); //FODEA 17
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER,3);	
					}else  if (bOperaFactorajeDist.equals("N") || bOperaFactVencimientoInfonavit.equals("N") ){
						pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER); //FODEA 17
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER,10);	
					}					
				}else {
					pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER); //FODEA 17
				}
			}	
						
			if(tipo.equals("PDF") && estatus.equals("9")) {
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Programado PYME","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(15,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Pyme ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Emision ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento ","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje  ","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Monto de Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo Dias ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Interes","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a recibir","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Acuse","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Neto a Recibir","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Porcentaje del Beneficiario","celda01",ComunesPDF.CENTER);					
				pdfDoc.setCell("Mandatario","celda01",ComunesPDF.CENTER);					
				pdfDoc.setCell("Fecha del Registro de la operaci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia ","celda01",ComunesPDF.CENTER);	//FODEA 17
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER, 4);		//era 5		
			}	
			  if(tipo.equals("CSV") && estatus.equals("1") ) {
				contenidoArchivo.append("ESTATUS: Seleccionada PYME \n");
				contenidoArchivo.append(" \n  Nombre EPO, N�mero Documento, Fecha Documento,Fecha Vencimiento,Nombre Cliente/Cedente,Tasa,Plazo (d�as) ,"+
								"Moneda,Monto Documento,Porcentaje de Descuento,Recurso en Garant�a,Monto a Descontar,Monto Int.,Monto a Operar ,");
							if(bTipoFactoraje.equals("S")) {
								contenidoArchivo.append("Tipo Factoraje ,Beneficiario ,Banco Beneficiario ,Sucursal Beneficiaria,Cuenta Beneficiaria,% Beneficiario ,"+
								"Importe a Recibir Beneficiario,Neto a Recibir PyME,");
							}
				contenidoArchivo.append(" Fecha Registro Operaci�n,Referencia Tasa, Referencia \n");
			}
			if(tipo.equals("CSV") && estatus.equals("2") ) {
				contenidoArchivo.append("ESTATUS: En Proceso de Autorizaci�n IF  \n");
				contenidoArchivo.append(" \n Nombre EPO, N�mero Documento, Fecha Documento,Fecha Vencimiento,Nombre Cliente/Cedente,Tasa,Plazo (d�as) ,"+
								"Moneda,Monto Documento,Porcentaje de Descuento,Recurso en Garant�a,Monto a Descontar,Monto Int.,Monto a Operar ,");
				if(bTipoFactoraje.equals("S") ) {		
					contenidoArchivo.append("Tipo Factoraje ,Beneficiario ,Banco Beneficiario ,Sucursal Beneficiaria,Cuenta Beneficiaria,% Beneficiario ,"+
													"Importe a Recibir Beneficiario,Neto a Recibir PyME,");
						}
								
							contenidoArchivo.append("Referencia Tasa, Referencia \n");
			}
			
			if(tipo.equals("CSV") &&  ( estatus.equals("3") ||  estatus.equals("11") )  ) {
			
				if( estatus.equals("3") )  { contenidoArchivo.append("Estatus: Seleccionada IF \n"); }
				if( estatus.equals("11") )  { contenidoArchivo.append("Estatus: Operado con Fondeo Propio \n"); }
				
				contenidoArchivo.append(" \n Nombre EPO , Nombre PYME , N�mero de Documento ,Emisi�n ,Fecha de Vencimiento,N�mero Solicitud ,Moneda ,Monto Documento, "+
					" Porcentaje de Descuento,Recurso en Garant�a, Monto a Descontar ,Tasa ,Plazo,Monto Int.,Monto a Operar ,Origen ,N�mero Acuse IF , ");
				if(bTipoFactoraje.equals("S")) {
					contenidoArchivo.append("Tipo Factoraje ,");
					if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
						contenidoArchivo.append("Beneficiario,Banco Beneficiario,Sucursal Beneficiaria,Cuenta Beneficiario, "+
						" % Beneficiario, Importe a Recibir Beneficiario, Neto a Recibir PyME, ");
					}else if (bOperaFactorajeDist.equals("N") || bOperaFactVencimientoInfonavit.equals("N") ){ 
						contenidoArchivo.append( "Referencia Tasa, ");
					}
					contenidoArchivo.append( "Referencia Tasa, ");
				}else if(bTipoFactoraje.equals("N")) {
					contenidoArchivo.append( "Referencia Tasa, ");
				}
				contenidoArchivo.append( "Referencia \n ");
			}
			
			if(tipo.equals("CSV") &&  estatus.equals("5")) {				
				contenidoArchivo.append("Estatus: Operada  \n");
				contenidoArchivo.append(" \n Nombre EPO , Nombre Cliente/Cedente , N�mero Solicitud , N�mero de Documento , Fecha  Emisi�n , Fecha de Vencimiento,  Moneda,  Monto Documento , "+
					" Porcentaje de Descuento ,  Recurso en Garant�a ,Monto a Descontar ,Tasa ,Plazo, Monto a Operar, Origen , ");
				if(bTipoFactoraje.equals("S")) {
					contenidoArchivo.append("Tipo Factoraje , ");
				}
				if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
					contenidoArchivo.append(" Beneficiario, Banco Beneficiario, Sucursal Beneficiaria, Cuenta Beneficiario, % Beneficiario, " +
						" Importe a Recibir Beneficiario,  Neto a Recibir PyME , ");
				}
				contenidoArchivo.append("Referencia Tasa , ");	
				contenidoArchivo.append("Referencia , ");	
				contenidoArchivo.append( "\n ");
			}
		
			if(tipo.equals("CSV") && estatus.equals("6")) {					
				contenidoArchivo.append("Estatus: Operada Pagada  \n");
				contenidoArchivo.append(" Nombre EPO ,  Nombre Cliente/Cedente , N�mero Solicitud , N�mero de Documento,   Moneda, " );
				if(bTipoFactoraje.equals("S")) {
					contenidoArchivo.append("Tipo Factoraje,  ");
				}	
				contenidoArchivo.append("Monto, Tasa , Porcentaje de Descuento , "+
												"	Recurso en Garant�a ,  Monto a Descontar , Fecha de Vencimiento , Fecha de Pago , Origen , ");													
									
				if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
					contenidoArchivo.append("Beneficiario, Banco Beneficiario,  Sucursal Beneficiaria,  Cuenta Beneficiario , "+					
					"% Beneficiario, Importe a Recibir Beneficiario,  Neto a Recibir PyME,");					
				}	
				contenidoArchivo.append(" Referencia Tasa , ");
				contenidoArchivo.append(" Referencia , ");
				contenidoArchivo.append( "\n ");
			}	
						
			if(tipo.equals("CSV") && estatus.equals("9")) {
				contenidoArchivo.append(" Estatus: Programado PYME \n ");
				contenidoArchivo.append("Nombre EPO , Nombre Pyme, N�mero de Documento , Fecha de Emision ,Fecha de Vencimiento,  Moneda , Tipo Factoraje , "+
				"Monto de Documento ,Porcentaje de Descuento ,Monto a Descontar, Tasa , Plazo Dias ,Monto Interes, Importe a recibir,Intermediario Financiero , "+
				" N�mero de Acuse,  Referencia,  Neto a Recibir,  Beneficiario, Porcentaje del Beneficiario, Mandatario, Fecha del Registro de la operaci�n , Referencia Tasa, Referencia \n");							
			}			
			
			
			if(generaExp!=null){
			
			} else if(estatus.equals("1") ||  estatus.equals("2") ) {
				while (rs.next())	{				
					nombre_epo = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					num_docto = (rs.getString("NUM_DOCTO") == null) ? "" : rs.getString("NUM_DOCTO");
					fecha_docto = (rs.getString("FECHA_DOCTO") == null) ? "" : rs.getString("FECHA_DOCTO");
					fecha_venc = (rs.getString("FECHA_VENC") == null) ? "" : rs.getString("FECHA_VENC");
					nombre_cliente = (rs.getString("NOMBRE_CLIENTE") == null) ? "" : rs.getString("NOMBRE_CLIENTE");
					tasa = (rs.getString("TASA") == null) ? "" : rs.getString("TASA");
					plazo_dias = (rs.getString("PLAZO_DIAS") == null) ? "" : rs.getString("PLAZO_DIAS");
					moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					tipo_factoraje = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
					monto_docto = (rs.getString("MONTO_DOCTO") == null) ? "0" : rs.getString("MONTO_DOCTO");
					por_desc = (rs.getString("POR_DESC") == null) ? "" : rs.getString("POR_DESC");
					rec_garantia = (rs.getString("REC_GARANTIA") == null) ? "0" : rs.getString("REC_GARANTIA");
					monto_desc = (rs.getString("MONTO_DESC") == null) ? "0" : rs.getString("MONTO_DESC");
					monto_int = (rs.getString("MONTO_INT") == null) ? "0" : rs.getString("MONTO_INT");
					monto_oper = (rs.getString("MONTO_OPER") == null) ? "0" : rs.getString("MONTO_OPER");
					beneficiario = (rs.getString("BENEFICIARIO") == null) ? "" : rs.getString("BENEFICIARIO");
					banco_beneficiario = (rs.getString("BANCO_BENEFICIARIO") == null) ? "" : rs.getString("BANCO_BENEFICIARIO");
					sucursal_beneficiario = (rs.getString("SUCURSAL_BENEFICIARIO") == null) ? "" : rs.getString("SUCURSAL_BENEFICIARIO");
					cuenta_beneficiario = (rs.getString("CUENTA_BENEFICIARIO") == null) ? "" : rs.getString("CUENTA_BENEFICIARIO");
					por_beneficiario = (rs.getString("PORC_BENEFICIARIO") == null) ? "" : rs.getString("PORC_BENEFICIARIO");
					imp_recibir = (rs.getString("IMP_RECIBIR") == null) ? "0" : rs.getString("IMP_RECIBIR");
					neto_recibir = (rs.getString("NETO_RECIBIR") == null) ? "0" : rs.getString("NETO_RECIBIR");
				   fecha_registro = (rs.getString("FECHA_REGISTRO") == null) ? "" : rs.getString("FECHA_REGISTRO");
					referencia = (rs.getString("REFERENCIA") == null) ? "" : rs.getString("REFERENCIA");
					ic_moneda = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");
					referencia_pyme = (rs.getString("REFERENCIA_PYME") == null) ? "" : rs.getString("REFERENCIA_PYME"); //FODEA 17
					if (ic_moneda.equals("1")) {	
						nacional+=Double.parseDouble((String)monto_docto);
						nacional_desc+=Double.parseDouble((String)monto_desc);  
						nacional_int+=Double.parseDouble((String)monto_int);    
						nacional_descIF+=Double.parseDouble((String)monto_oper);   
					   doc_nacional++;
					}
					if (ic_moneda.equals("54")) {	
						dolares+=Double.parseDouble((String)monto_docto);
						dolares_desc+=Double.parseDouble((String)monto_desc);  
						dolares_int+=Double.parseDouble((String)monto_int);    
						dolares_descIF+=Double.parseDouble((String)monto_oper);   
						doc_dolares++;
					}
					
					if(tipo.equals("PDF")   &&  estatus.equals("1") ) {
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_venc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_cliente,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo_dias,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_docto,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(por_desc +"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(rec_garantia,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_int,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_oper,2),"formas",ComunesPDF.RIGHT);	
						if(bTipoFactoraje.equals("S")) {					
							pdfDoc.setCell("B","formas",ComunesPDF.CENTER);				
							pdfDoc.setCell(tipo_factoraje,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(banco_beneficiario,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(por_beneficiario+"%","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(imp_recibir,2),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(neto_recibir,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fecha_registro,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,3);	
						}else if(bTipoFactoraje.equals("N")) {					
							pdfDoc.setCell(fecha_registro,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
						}	
						
					}
						if(tipo.equals("PDF")   &&  estatus.equals("2") ) {
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_venc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_cliente,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo_dias,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_docto,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(por_desc +"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(rec_garantia,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_int,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_oper,2),"formas",ComunesPDF.RIGHT);				
						if(bTipoFactoraje.equals("S")) {					
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(tipo_factoraje,"formas",ComunesPDF.CENTER);			
						pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(banco_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(por_beneficiario+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(imp_recibir,2),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(neto_recibir,2) ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,4); //era 5
						}else if(bTipoFactoraje.equals("N")) {					
							pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
						}		
					}
					
					if(tipo.equals("CSV")  &&  estatus.equals("1") ) {
						contenidoArchivo.append(nombre_epo.replace(',',' ') +","+
							num_docto.replace(',',' ') +","+ fecha_docto+","+
							fecha_venc+","+ 	nombre_cliente.replace(',',' ') +","+
							tasa+","+ 	plazo_dias+","+ 	moneda+","+
							"\"$"+Comunes.formatoDecimal(monto_docto,2,false)+"\","+							
							por_desc +"% ,"+
							"\"$"+Comunes.formatoDecimal(rec_garantia,2,false)+"\","+							
							"\"$"+Comunes.formatoDecimal(monto_desc,2,false)+"\","+							
							"\"$"+Comunes.formatoDecimal(monto_int,2,false)+"\","+							
							"\"$"+Comunes.formatoDecimal(monto_oper,2,false)+"\",");							
							if(bTipoFactoraje.equals("S")) {					
							contenidoArchivo.append(tipo_factoraje.replace(',',' ') +","+
							beneficiario.replace(',',' ') +","+
							banco_beneficiario.replace(',',' ') +","+
							sucursal_beneficiario.replace(',',' ') +","+
							cuenta_beneficiario.replace(',',' ') +","+
							por_beneficiario+"%"+","+
							"\"$"+Comunes.formatoDecimal(imp_recibir,2,false)+"\","+							
							"\"$"+Comunes.formatoDecimal(neto_recibir,2,false)+"\",");
						}
						contenidoArchivo.append(fecha_registro +","+
						referencia.replace(',',' ') +","+	
						referencia_pyme.replace(',',' ') +"\n"); //FODEA 17
					}
					
					if(tipo.equals("CSV")  &&  estatus.equals("2") ) {
						contenidoArchivo.append(nombre_epo.replace(',',' ') +","+
							num_docto.replace(',',' ') +","+ fecha_docto+","+
							fecha_venc+","+ 	nombre_cliente.replace(',',' ') +","+
							tasa+","+ 	plazo_dias+","+ 	moneda+","+
							"\"$"+Comunes.formatoDecimal(monto_docto,2,false)+"\","+							
							por_desc +"% ,"+
							"\"$"+Comunes.formatoDecimal(rec_garantia,2,false)+"\","+							
							"\"$"+Comunes.formatoDecimal(monto_desc,2,false)+"\","+							
							"\"$"+Comunes.formatoDecimal(monto_int,2,false)+"\","+							
							"\"$"+Comunes.formatoDecimal(monto_oper,2,false)+"\",");	
							if(bTipoFactoraje.equals("S")) {					
							contenidoArchivo.append(tipo_factoraje.replace(',',' ') +","+
							beneficiario.replace(',',' ') +","+
							banco_beneficiario.replace(',',' ') +","+
							sucursal_beneficiario.replace(',',' ') +","+
							cuenta_beneficiario.replace(',',' ') +","+
							por_beneficiario+"%"+","+
							"\"$"+Comunes.formatoDecimal(imp_recibir,2,false)+"\","+							
							"\"$"+Comunes.formatoDecimal(neto_recibir,2,false)+"\",");
						}
						contenidoArchivo.append(referencia.replace(',',' ') +",");
						contenidoArchivo.append(referencia_pyme.replace(',',' ') +"\n");	//FODEA 17
					}
					
				} //while (rs.next())
				
				if(tipo.equals("PDF")   &&  estatus.equals("1") ) {				
				if (doc_nacional>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_descIF), 2),"formas",ComunesPDF.RIGHT);
						if(bTipoFactoraje.equals("N")) { 
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
						}
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					
					if (doc_dolares>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_descIF), 2),"formas",ComunesPDF.RIGHT);
						if(bTipoFactoraje.equals("N")) { 
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
						}
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}	
				}
				
				if(tipo.equals("PDF")   &&  estatus.equals("2") ) {				
				
					if (doc_nacional>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_descIF), 2),"formas",ComunesPDF.RIGHT);
						if(bTipoFactoraje.equals("N")) { 
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					
					if (doc_dolares>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 6);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_descIF), 2),"formas",ComunesPDF.RIGHT);
						if(bTipoFactoraje.equals("N")) { 
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}	
				}
				
				
				
			}//	if(estatus.equals("1") ||  estatus.equals("2") ) {
			
			
			if(estatus.equals("3") || estatus.equals("4")  || estatus.equals("5") || estatus.equals("11")) {
				while (rs.next())	{				
					nombre_epo = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					nombre_pyme = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
					num_docto = (rs.getString("NUM_DOCTO") == null) ? "" : rs.getString("NUM_DOCTO");
					fecha_emision = (rs.getString("FECHA_EMISION") == null) ? "" : rs.getString("FECHA_EMISION");
					fecha_venc = (rs.getString("FECHA_VENC") == null) ? "" : rs.getString("FECHA_VENC");
					num_solicitud= (rs.getString("NUM_SOLICITUD") == null) ? "" : rs.getString("NUM_SOLICITUD");
					moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					tipo_factoraje = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
					monto_docto = (rs.getString("MONTO_DOCTO") == null) ? "0" : rs.getString("MONTO_DOCTO");
					por_desc = (rs.getString("POR_DESC") == null) ? "" : rs.getString("POR_DESC");
					rec_garantia = (rs.getString("REC_GARANTIA") == null) ? "0" : rs.getString("REC_GARANTIA");
					monto_desc = (rs.getString("MONTO_DESC") == null) ? "0" : rs.getString("MONTO_DESC");
					tasa = (rs.getString("TASA") == null) ? "" : rs.getString("TASA");
					plazo = (rs.getString("PLAZO") == null) ? "" : rs.getString("PLAZO");					
					monto_int = (rs.getString("MONTO_INT") == null) ? "0" : rs.getString("MONTO_INT");
					monto_oper = (rs.getString("MONTO_OPER") == null) ? "0" : rs.getString("MONTO_OPER");
					origen = (rs.getString("ORIGEN") == null) ? "0" : rs.getString("ORIGEN");
					num_acuse_if = (rs.getString("NUM_ACUSE_IF") == null) ? "0" : rs.getString("NUM_ACUSE_IF");					
					beneficiario = (rs.getString("BENEFICIARIO") == null) ? "" : rs.getString("BENEFICIARIO");
					banco_beneficiario = (rs.getString("BANCO_BENEFICIARIO") == null) ? "" : rs.getString("BANCO_BENEFICIARIO");
					sucursal_beneficiario = (rs.getString("SUCURSAL_BENEFICIARIO") == null) ? "" : rs.getString("SUCURSAL_BENEFICIARIO");
					cuenta_beneficiario = (rs.getString("CUENTA_BENEFICIARIO") == null) ? "" : rs.getString("CUENTA_BENEFICIARIO");
					por_beneficiario = (rs.getString("PORC_BENEFICIARIO") == null) ? "" : rs.getString("PORC_BENEFICIARIO");
					imp_recibir = (rs.getString("IMP_RECIBIR") == null) ? "0" : rs.getString("IMP_RECIBIR");
					neto_recibir = (rs.getString("NETO_RECIBIR") == null) ? "0" : rs.getString("NETO_RECIBIR");			  
					referencia = (rs.getString("REFERENCIA") == null) ? "" : rs.getString("REFERENCIA");
					ic_moneda = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");
					referencia_pyme = (rs.getString("REFERENCIA_PYME") == null) ? "" : rs.getString("REFERENCIA_PYME"); //FODEA 17
								
					if (ic_moneda.equals("1")) {	
						nacional+=Double.parseDouble((String)monto_docto);
						nacional_desc+=Double.parseDouble((String)monto_desc);  
						nacional_int+=Double.parseDouble((String)monto_int);    
						nacional_descIF+=Double.parseDouble((String)monto_oper);   
					   doc_nacional++;
					}
					if (ic_moneda.equals("54")) {	
						dolares+=Double.parseDouble((String)monto_docto);
						dolares_desc+=Double.parseDouble((String)monto_desc);  
						dolares_int+=Double.parseDouble((String)monto_int);    
						dolares_descIF+=Double.parseDouble((String)monto_oper);   
						doc_dolares++;
					}
										
					if(tipo.equals("PDF") && ( estatus.equals("3") ||  estatus.equals("11") )  ) {				
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_emision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_venc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_solicitud,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda, "formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(monto_docto,2) ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(por_desc+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(rec_garantia,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2) ,"formas",ComunesPDF.RIGHT);
	
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_oper,2) ,"formas",ComunesPDF.RIGHT);						
						pdfDoc.setCell(origen,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_acuse_if,"formas",ComunesPDF.CENTER);
						if(bTipoFactoraje.equals("S")) {
							pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 5);					
							pdfDoc.setCell("C","formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(tipo_factoraje,"formas",ComunesPDF.CENTER);	
							if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
								pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(banco_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(por_beneficiario+"%","formas",ComunesPDF.CENTER);	
								pdfDoc.setCell("$"+Comunes.formatoDecimal(imp_recibir,2) ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell("$"+Comunes.formatoDecimal(neto_recibir,2) ,"formas",ComunesPDF.RIGHT);
							}else if (bOperaFactorajeDist.equals("N") || bOperaFactVencimientoInfonavit.equals("N") ){ 
								pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 4);
							}
							pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 2);
						}else if(bTipoFactoraje.equals("N")) {
							pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 4);
						}
					}
					
					if(tipo.equals("CSV")  &&   ( estatus.equals("3") ||  estatus.equals("11")) ) {				
						contenidoArchivo.append(nombre_epo.replace(',',' ') +","+
								nombre_pyme.replace(',',' ') +","+num_docto +","+
								fecha_emision +","+ fecha_venc +","+
								num_solicitud +","+moneda +","+
								"\"$"+Comunes.formatoDecimal(monto_docto,2,false)+"\","+
								por_desc+"%,"+
								"\"$"+Comunes.formatoDecimal(rec_garantia,2,false)+"\","+
								"\"$"+Comunes.formatoDecimal(monto_desc,2,false)+"\","+								
								tasa+","+plazo+","+						
								"\"$"+Comunes.formatoDecimal(monto_int,2,false)+"\","+
								"\"$"+Comunes.formatoDecimal(monto_oper,2,false)+"\","+
								origen +","+num_acuse_if +",");
						if(bTipoFactoraje.equals("S")) {
							contenidoArchivo.append(tipo_factoraje+",");
							if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
								contenidoArchivo.append(beneficiario.replace(',',' ')+","+
								banco_beneficiario.replace(',',' ')+","+
								sucursal_beneficiario.replace(',',' ')+","+
								cuenta_beneficiario+","+
								por_beneficiario+"%"+","+	 
								"\"$"+Comunes.formatoDecimal(imp_recibir,2,false)+"\","+
								"\"$"+Comunes.formatoDecimal(neto_recibir,2,false)+"\",");
							}else if (bOperaFactorajeDist.equals("N") || bOperaFactVencimientoInfonavit.equals("N") ){ 
								contenidoArchivo.append(referencia.replace(',',' ')+",");								
							}
							contenidoArchivo.append(referencia.replace(',',' ')+",");	
						}else if(bTipoFactoraje.equals("N")) {
							contenidoArchivo.append(referencia.replace(',',' ')+",");	
						}
						contenidoArchivo.append("\n");	
					}
					
					
					if(tipo.equals("PDF") && estatus.equals("4")) {		
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_solicitud,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_emision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_venc,"formas",ComunesPDF.CENTER);				
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_docto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(por_desc+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(rec_garantia,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER); 
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_oper,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(origen, "formas",ComunesPDF.CENTER);
						if(bTipoFactoraje.equals("S")) {
							pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
							pdfDoc.setCell(tipo_factoraje,"formas",ComunesPDF.CENTER);
							if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
								pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(banco_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(por_beneficiario +"%","formas",ComunesPDF.CENTER);	
								pdfDoc.setCell("$"+Comunes.formatoDecimal(imp_recibir,2) ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell("$"+Comunes.formatoDecimal(neto_recibir,2) ,"formas",ComunesPDF.RIGHT);	
								pdfDoc.setCell(referencia_pyme, "formas",ComunesPDF.CENTER); //FODEA 17
							}else {
								pdfDoc.setCell(referencia_pyme, "formas",ComunesPDF.CENTER); //FODEA 17
								pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,7);	
							}
							pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,7);	
						}else
							pdfDoc.setCell(referencia_pyme, "formas",ComunesPDF.CENTER); //FODEA 17
					}	
					
					if(tipo.equals("PDF") && estatus.equals("5")) {	
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_solicitud,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_emision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_venc,"formas",ComunesPDF.CENTER);				
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_docto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(por_desc+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(rec_garantia,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_oper,2) ,"formas",ComunesPDF.RIGHT);
								
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER); 
						pdfDoc.setCell(origen,"formas",ComunesPDF.CENTER);
						if(bTipoFactoraje.equals("S")) {
							pdfDoc.setCell(tipo_factoraje,"formas",ComunesPDF.CENTER);			
							if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
								pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(banco_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(por_beneficiario+"%","formas",ComunesPDF.CENTER);	
								pdfDoc.setCell("$"+Comunes.formatoDecimal(imp_recibir,2) ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell("$"+Comunes.formatoDecimal(neto_recibir,2) ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
								pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 3);
							}else  if (bOperaFactorajeDist.equals("N") || bOperaFactVencimientoInfonavit.equals("N") ){
								pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
								pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,10);	
							}	
						}else if(bTipoFactoraje.equals("N")) {
							pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
							pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 11);	
						}
					}
					
					if(tipo.equals("CSV") && estatus.equals("5")) {		
						contenidoArchivo.append(nombre_epo.replace(',',' ') +","+
							nombre_pyme.replace(',',' ') +","+ 
							num_solicitud+","+num_docto+","+	fecha_emision+","+fecha_venc+","+moneda+","+
							"\"$"+Comunes.formatoDecimal(monto_docto,2,false)+"\","+
							por_desc+"%"+","+
							"\"$"+Comunes.formatoDecimal(rec_garantia,2,false)+"\","+
							"\"$"+Comunes.formatoDecimal(monto_desc,2,false)+"\","+
							tasa+","+plazo+","+
							"\"$"+Comunes.formatoDecimal(monto_oper,2,false)+"\","+
							origen+",");
							
						if(bTipoFactoraje.equals("S")) {
							contenidoArchivo.append(tipo_factoraje+",");
						}
						if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
						contenidoArchivo.append(beneficiario.replace(',',' ') +","+
							banco_beneficiario.replace(',',' ') +","+
							sucursal_beneficiario.replace(',',' ') +","+
							cuenta_beneficiario.replace(',',' ') +","+
							por_beneficiario+"%"+","+
							"\"$"+Comunes.formatoDecimal(imp_recibir,2,false)+"\","+
							"\"$"+Comunes.formatoDecimal(neto_recibir,2,false)+"\",");
						}
						contenidoArchivo.append(referencia.replace(',',' ') +",");
						contenidoArchivo.append(referencia_pyme.replace(',',' ') +"\n"); //FODEA 17
					}
					
				
				}//while (rs.next())	{
				
				
				//totales 
				if(tipo.equals("PDF") && ( estatus.equals("3") || estatus.equals("11")) ) {
					if (doc_nacional>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 4);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 1);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_descIF), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,7);
					}
					
					if (doc_dolares>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 4);
						pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 1);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_descIF), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,7);
					}	
				}
				
					if(tipo.equals("PDF") && estatus.equals("4")) {
					if (doc_nacional>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_descIF), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
					
					if (doc_dolares>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_descIF), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}	
				}
				
				if(tipo.equals("PDF") && estatus.equals("5")) {
					if (doc_nacional>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_descIF), 2),"formas",ComunesPDF.RIGHT);			
					}
					
					if (doc_dolares>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);				
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_descIF), 2),"formas",ComunesPDF.RIGHT);					
					}	
				}				
			}
		
			if( estatus.equals("6") ) {
			
				while (rs.next())	{				
					nombre_epo = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					nombre_pyme = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
					num_solicitud= (rs.getString("NUM_SOLICITUD") == null) ? "" : rs.getString("NUM_SOLICITUD");
					num_docto = (rs.getString("NUM_DOCTO") == null) ? "" : rs.getString("NUM_DOCTO");
					moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					tipo_factoraje = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
					monto_docto = (rs.getString("MONTO_DOCTO") == null) ? "0" : rs.getString("MONTO_DOCTO");
					tasa = (rs.getString("TASA") == null) ? "" : rs.getString("TASA");
					por_desc = (rs.getString("POR_DESC") == null) ? "" : rs.getString("POR_DESC");
					rec_garantia = (rs.getString("REC_GARANTIA") == null) ? "0" : rs.getString("REC_GARANTIA");
					monto_desc = (rs.getString("MONTO_DESC") == null) ? "0" : rs.getString("MONTO_DESC");
					fecha_venc = (rs.getString("FECHA_VENC") == null) ? "" : rs.getString("FECHA_VENC");									
					origen = (rs.getString("ORIGEN") == null) ? "" : rs.getString("ORIGEN");
					beneficiario = (rs.getString("BENEFICIARIO") == null) ? "" : rs.getString("BENEFICIARIO");
					banco_beneficiario = (rs.getString("BANCO_BENEFICIARIO") == null) ? "" : rs.getString("BANCO_BENEFICIARIO");
					sucursal_beneficiario = (rs.getString("SUCURSAL_BENEFICIARIO") == null) ? "" : rs.getString("SUCURSAL_BENEFICIARIO");
					cuenta_beneficiario = (rs.getString("CUENTA_BENEFICIARIO") == null) ? "" : rs.getString("CUENTA_BENEFICIARIO");
					por_beneficiario = (rs.getString("PORC_BENEFICIARIO") == null) ? "" : rs.getString("PORC_BENEFICIARIO");
					imp_recibir = (rs.getString("IMP_RECIBIR") == null) ? "0" : rs.getString("IMP_RECIBIR");
					neto_recibir = (rs.getString("NETO_RECIBIR") == null) ? "0" : rs.getString("NETO_RECIBIR");
				  	referencia = (rs.getString("REFERENCIA") == null) ? "" : rs.getString("REFERENCIA");
					ic_moneda = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");
					referencia_pyme = (rs.getString("REFERENCIA_PYME") == null) ? "" : rs.getString("REFERENCIA_PYME"); //FODEA 17
					
					if (ic_moneda.equals("1")) {	
						nacional+=Double.parseDouble((String)monto_docto);
						nacional_desc+=Double.parseDouble((String)monto_desc); 					   
					   doc_nacional++;
					}
					if (ic_moneda.equals("54")) {	
						dolares+=Double.parseDouble((String)monto_docto);
						dolares_desc+=Double.parseDouble((String)monto_desc);  						
						doc_dolares++;
					}
					
					if(tipo.equals("PDF") && estatus.equals("6")) {
					
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_solicitud,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(monto_docto), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(por_desc+"%","formas",ComunesPDF.CENTER);						
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(rec_garantia), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(monto_desc), 2),"formas",ComunesPDF.RIGHT);					
						pdfDoc.setCell(fecha_venc,"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(fecha_venc,"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(origen,"formas",ComunesPDF.CENTER);
						
						if(bTipoFactoraje.equals("S")) {
								pdfDoc.setCell("B","formas",ComunesPDF.CENTER); 
								pdfDoc.setCell(tipo_factoraje,"formas",ComunesPDF.CENTER);	
							if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
								pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(banco_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);	
								pdfDoc.setCell(por_beneficiario+"%","formas",ComunesPDF.CENTER);	
								pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(imp_recibir), 2),"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(neto_recibir), 2),"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
								pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,3);	
							}else  if (bOperaFactorajeDist.equals("N") || bOperaFactVencimientoInfonavit.equals("N") ){
								pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
								pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,10);	
							}					
						}else {
							pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
						}
					}
					
					if(tipo.equals("CSV") && estatus.equals("6")) {
										
						contenidoArchivo.append("\n" +nombre_epo.replace(',',' ') +","+
							nombre_pyme.replace(',',' ') +","+
							num_solicitud +","+num_docto +","+ moneda +",");							
							if(bTipoFactoraje.equals("S")) {
								contenidoArchivo.append(tipo_factoraje+",");
							}						
							contenidoArchivo.append("\"$"+Comunes.formatoDecimal(monto_docto,2,false)+"\","+							
							tasa +","+ por_desc+"%" +","+
							"\"$"+Comunes.formatoDecimal(rec_garantia,2,false)+"\","+							
							"\"$"+Comunes.formatoDecimal(monto_desc,2,false)+"\","+							
							fecha_venc +","+ fecha_venc +","+origen +",");
						
						
						if (bOperaFactorajeDist.equals("S") || bOperaFactVencimientoInfonavit.equals("S") ){ 
							contenidoArchivo.append(beneficiario.replace(',',' ') +","+
								banco_beneficiario.replace(',',' ') +","+
								sucursal_beneficiario.replace(',',' ') +","+
								cuenta_beneficiario.replace(',',' ') +","+
								por_beneficiario+"%"+","+
								"\"$"+Comunes.formatoDecimal(imp_recibir,2,false)+"\","+	
								"\"$"+Comunes.formatoDecimal(neto_recibir,2,false)+"\",");
						}	
						contenidoArchivo.append(referencia+",");	
						contenidoArchivo.append(referencia_pyme+"\n");
					}				
					
					
				}	//while 
				
				if(tipo.equals("PDF") && estatus.equals("6")) {
					if (doc_nacional>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,3);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_desc), 2),"formas",ComunesPDF.RIGHT);
						if(bTipoFactoraje.equals("N")) { 
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);	
						}else if(bTipoFactoraje.equals("S")) { 
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);	
						}
									
					}
					
					if (doc_dolares>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,3);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_desc), 2),"formas",ComunesPDF.RIGHT);
						if(bTipoFactoraje.equals("N")) { 
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);	
						}else if(bTipoFactoraje.equals("S")) { 
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);	
						}				
										
					}	
				}					
				
			}
			
			
			if( estatus.equals("9") ) {
				String  intermediario = "", num_acuse  ="",  mandatario  ="", referencia_tasa  ="";
				while (rs.next())	{				
					nombre_epo = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					nombre_pyme = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
					num_docto = (rs.getString("NUM_DOCTO") == null) ? "" : rs.getString("NUM_DOCTO");
					fecha_emision = (rs.getString("FECHA_EMISION") == null) ? "" : rs.getString("FECHA_EMISION");
					fecha_venc = (rs.getString("FECHA_VENC") == null) ? "" : rs.getString("FECHA_VENC");				
					moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");					
					tipo_factoraje = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
					monto_docto = (rs.getString("MONTO_DOCTO") == null) ? "0" : rs.getString("MONTO_DOCTO");
					por_desc = (rs.getString("POR_DESC") == null) ? "" : rs.getString("POR_DESC");					
					monto_desc = (rs.getString("MONTO_DESC") == null) ? "0" : rs.getString("MONTO_DESC");
					intermediario = (rs.getString("INTERMEDIARIO") == null) ? "" : rs.getString("INTERMEDIARIO");	
					tasa = (rs.getString("TASA") == null) ? "" : rs.getString("TASA");
					plazo_dias  = (rs.getString("PLAZO_DIAS") == null) ? "" : rs.getString("PLAZO_DIAS");						
					monto_int = (rs.getString("MONTO_INT") == null) ? "" : rs.getString("MONTO_INT");				
					imp_recibir = (rs.getString("IMP_RECIBIR") == null) ? "0" : rs.getString("IMP_RECIBIR");				
					num_acuse = (rs.getString("NUM_ACUSE") == null) ? "" : rs.getString("NUM_ACUSE");
					referencia = (rs.getString("REFERENCIA") == null) ? "" : rs.getString("REFERENCIA");				
					neto_recibir = (rs.getString("NETO_RECIBIR") == null) ? "0" : rs.getString("NETO_RECIBIR");
					beneficiario = (rs.getString("BENEFICIARIO") == null) ? "" : rs.getString("BENEFICIARIO");
					por_beneficiario = (rs.getString("PORC_BENEFICIARIO") == null) ? "" : rs.getString("PORC_BENEFICIARIO");					
					mandatario = (rs.getString("MANDATARIO") == null) ? "" : rs.getString("MANDATARIO");
					fecha_registro = (rs.getString("FECHA_REGISTRO") == null) ? "" : rs.getString("FECHA_REGISTRO");					
					referencia_tasa = (rs.getString("REFERENCIA_TASA") == null) ? "" : rs.getString("REFERENCIA_TASA");
					ic_moneda = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");
									
					if (ic_moneda.equals("1")) {	
						nacional+=Double.parseDouble((String)monto_docto);
						nacional_desc+=Double.parseDouble((String)monto_desc);
						nacional_int+=Double.parseDouble((String)monto_int);
						nacional_descIF+=Double.parseDouble((String)imp_recibir);				
						doc_nacional++;
					}
					if (ic_moneda.equals("54")) {	
						dolares+=Double.parseDouble((String)monto_docto);
						dolares_desc+=Double.parseDouble((String)monto_desc);
						dolares_int+=Double.parseDouble((String)monto_int);
						dolares_descIF+=Double.parseDouble((String)imp_recibir);				
						doc_dolares++;
					}					
					if(tipo.equals("PDF") && estatus.equals("9")) {
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_emision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_venc,"formas",ComunesPDF.CENTER);				
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tipo_factoraje,"formas",ComunesPDF.CENTER);				
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(monto_docto), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(por_desc+"%", "formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(monto_desc), 2),"formas",ComunesPDF.RIGHT);						
						pdfDoc.setCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo_dias,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(monto_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(imp_recibir), 2),"formas",ComunesPDF.RIGHT);
									
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_acuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(neto_recibir), 2),"formas",ComunesPDF.RIGHT);
					   pdfDoc.setCell(beneficiario, "formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(por_beneficiario +"%","formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(mandatario,"formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(fecha_registro,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referencia_tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);					
					}
					
					
					if(tipo.equals("CSV") && estatus.equals("9")) {
						contenidoArchivo.append(nombre_epo.replace(',',' ') +","+
						nombre_pyme.replace(',',' ') +","+num_docto+","+	fecha_emision +","+ fecha_venc+","+ 				
						moneda+","+ tipo_factoraje+","+ 
						"\"$"+Comunes.formatoDecimal(monto_docto,2,false)+"\","+
						por_desc+"%"+","+ 
						"\"$"+Comunes.formatoDecimal(monto_desc,2,false)+"\","+
						tasa+","+plazo_dias+","+
						"\"$"+Comunes.formatoDecimal(monto_int,2,false)+"\","+
						"\"$"+Comunes.formatoDecimal(imp_recibir,2,false)+"\","+
						intermediario.replace(',',' ') +","+
						num_acuse.replace(',',' ') +","+
						referencia.replace(',',' ') +","+
						"\"$"+Comunes.formatoDecimal(neto_recibir,2,false)+"\","+
						beneficiario.replace(',',' ') +","+
						por_beneficiario+"%"+","+ 
						mandatario.replace(',',' ') +","+
						fecha_registro +","+
						referencia_tasa.replace(',',' ') +"\n");
					}				
				}// while
				
					if(tipo.equals("PDF") && estatus.equals("9")) {
					if (doc_nacional>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);					
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(nacional_descIF), 2),"formas",ComunesPDF.RIGHT);						
					}
					
					//if (doc_dolares>0){ 
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);					
						pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_desc), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_int), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(dolares_descIF), 2),"formas",ComunesPDF.RIGHT);
																				
					//}	
				}
				
				
			}
			
			
			if(generaExp!=null){
			String  strFecha ="",   strMes= "", strHora = "", strMinuto = "", strSegundo = "";
			String pyme="",sirac="",nomPyme="",claveEpo="",claveIf="",nomIf="",claveMon="",claveEst="",estatus="",c1="",c2="",c3="",c4="",c5="",claveDoc="";
			GregorianCalendar grcFecha = new GregorianCalendar();
			java.util.Date	 dtmFecha = new java.util.Date();
			grcFecha.setTime(dtmFecha);
			strMes = Integer.toString(grcFecha.get(Calendar.MONTH) + 1);
			strMes = (strMes.length() > 1) ? strMes.toString() : "0" + strMes.toString();
			strHora = Integer.toString(grcFecha.get(Calendar.HOUR_OF_DAY));
			strHora = (strHora.length() > 1) ? strHora.toString() : "0" + strHora.toString();
			strMinuto = Integer.toString(grcFecha.get(Calendar.MINUTE));
			strMinuto = (strMinuto.length() > 1) ? strMinuto.toString() : "0" + strMinuto.toString();
			strSegundo = Integer.toString(grcFecha.get(Calendar.SECOND));
			strSegundo = (strSegundo.length() > 1) ? strSegundo.toString() : "0" + strSegundo.toString();
			strFecha = grcFecha.get(Calendar.DAY_OF_MONTH) + "/" +
			strMes.toString() + "/" +
			grcFecha.get(Calendar.YEAR);
			contenidoArchivo= new StringBuffer("");
			contenidoArchivo.append(" H," + strFecha + "," + strHora + ":" + strMinuto + ":" + strSegundo + ",Doctos / Solic. por Estatus \n");
			contenidoArchivo.append(" D , N�mero Documento EPO, Clave PYME, N�mero SIRAC,Nombre PYME, Clave EPO, "+
										"Nombre EPO, Clave IF, Nombre IF, Fecha Documento, Fecha Vencimiento, Clave Moneda, Moneda,Tipo Factoraje, Monto Documento, Porcentaje Descuento,"+
										"Recurso Garant�a, Monto Descuento, Monto Inter�s, Monto Operar, Tasa Interes, Plazo, Clave Estatus, Estatus, Referencia,  "+
										" CG_CAMPO1, CG_CAMPO2, CG_CAMPO3, CG_CAMPO4, CG_CAMPO5, Fecha Alta, IC_DOCUMENTO \n");
				while (rs.next())	{				
					num_docto= (rs.getString("IG_NUMERO_DOCTO") == null)  ? "" : rs.getString("IG_NUMERO_DOCTO");
					pyme 		= (rs.getString("CLAVE_PYME") == null) 		? "" : rs.getString("CLAVE_PYME");
					sirac 	= (rs.getString("IN_NUMERO_SIRAC") == null) 	? "" : rs.getString("IN_NUMERO_SIRAC");
					nomPyme 	= (rs.getString("NOMBREPYME") == null) 		? "" : rs.getString("NOMBREPYME");
					claveEpo	= (rs.getString("CLAVEEPO") == null) 			? "" : rs.getString("CLAVEEPO"); 
					nombre_epo=(rs.getString("NOMBREEPO") == null) 			? "" : rs.getString("NOMBREEPO");	
					claveIf 	= (rs.getString("CLAVEIF") == null) 			? "" : rs.getString("CLAVEIF");	
					nomIf		= (rs.getString("NOMBREIF") == null) 			? "" : rs.getString("NOMBREIF");						
					fecha_docto=(rs.getString("DF_FECHA_DOCTO") == null) 	? "" : rs.getString("DF_FECHA_DOCTO");					
					fecha_venc=(rs.getString("DF_FECHA_VENC") == null) 	? "" : rs.getString("DF_FECHA_VENC");
					claveMon	= (rs.getString("IC_MONEDA") == null) 			? "" : rs.getString("IC_MONEDA");
					moneda 	= (rs.getString("CD_NOMBRE") == null) 			? "" : rs.getString("CD_NOMBRE");
					tipo_factoraje=(rs.getString("TIPO_FACTORAJE") == null) 	? "" : rs.getString("TIPO_FACTORAJE");
					monto_docto = (rs.getString("FN_MONTO") == null) 			? "0" : rs.getString("FN_MONTO");
					por_desc = (rs.getString("FN_PORC_ANTICIPO") == null) 	? "" : rs.getString("FN_PORC_ANTICIPO");
					rec_garantia = (rs.getString("RECURSOGARANTIA") == null) ? "0" : rs.getString("RECURSOGARANTIA");
					monto_desc= (rs.getString("FN_MONTO_DSCTO") == null) 		? "0" : rs.getString("FN_MONTO_DSCTO");
					monto_int = (rs.getString("IN_IMPORTE_INTERES") == null) ? "0" : rs.getString("IN_IMPORTE_INTERES");
					imp_recibir=(rs.getString("IN_IMPORTE_RECIBIR") == null) ? "0" : rs.getString("IN_IMPORTE_RECIBIR");
					tasa 		=  (rs.getString("IN_TASA_ACEPTADA") == null) 	? "" : rs.getString("IN_TASA_ACEPTADA");
					plazo_dias =(rs.getString("PLAZO") == null) 					? "" : rs.getString("PLAZO");					
					claveEst	=  (rs.getString("IC_ESTATUS_DOCTO") == null) 	? "" : rs.getString("IC_ESTATUS_DOCTO");
					estatus	=  (rs.getString("CD_DESCRIPCION") == null) 		? "" : rs.getString("CD_DESCRIPCION");
					referencia =(rs.getString("CT_REFERENCIA") == null) 		? "" : rs.getString("CT_REFERENCIA");
					c1			=  (rs.getString("CG_CAMPO1") == null) 			? "" : rs.getString("CG_CAMPO1");
					c2			=  (rs.getString("CG_CAMPO2") == null) 			? "" : rs.getString("CG_CAMPO2");
					c3			=  (rs.getString("CG_CAMPO3") == null) 			? "" : rs.getString("CG_CAMPO3");
					c4			=  (rs.getString("CG_CAMPO4") == null) 			? "" : rs.getString("CG_CAMPO4");
					c5			=  (rs.getString("CG_CAMPO5") == null) 			? "" : rs.getString("CG_CAMPO5");
				   fecha_registro = (rs.getString("DF_ALTA") == null) 		? "" : rs.getString("DF_ALTA");
					claveDoc	=  (rs.getString("IC_DOCUMENTO") == null) 		? "" : rs.getString("IC_DOCUMENTO");
					
					contenidoArchivo.append(
						"D,"+num_docto+","+pyme+","+sirac+","+nomPyme.replace(',',' ')+","+claveEpo+","+nombre_epo.replace(',',' ')+","+claveIf+","+nomIf.replace(',',' ')+","+
						fecha_docto+","+fecha_venc+","+claveMon+","+moneda+","+tipo_factoraje+","+monto_docto+","+por_desc+","+rec_garantia+","+monto_desc+","+monto_int+","+
						imp_recibir+","+tasa+","+plazo_dias+","+claveEst+","+estatus.replace(',',' ')+","+referencia.replace('\n',' ')+","+c1.replace(',',' ')+","+
						c2.replace(',',' ')+","+c3.replace(',',' ')+","+c4.replace(',',' ')+","+c5.replace(',',' ')+","+fecha_registro+","+claveDoc+", \n"
						);						
				} //while (rs.next())
			}
			
			if(tipo.equals("PDF") ) {
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			}
			if(tipo.equals("CSV") ) {
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	public String getBTipoFactoraje() {
		return bTipoFactoraje;
	}

	public void setBTipoFactoraje(String bTipoFactoraje) {
		this.bTipoFactoraje = bTipoFactoraje;
	}

	public String getBOperaFactorajeDist() {
		return bOperaFactorajeDist;
	}

	public void setBOperaFactorajeDist(String bOperaFactorajeDist) {
		this.bOperaFactorajeDist = bOperaFactorajeDist;
	}

	public String getBOperaFactVencimientoInfonavit() {
		return bOperaFactVencimientoInfonavit;
	}

	public void setBOperaFactVencimientoInfonavit(String bOperaFactVencimientoInfonavit) {
		this.bOperaFactVencimientoInfonavit = bOperaFactVencimientoInfonavit;
	}

	public String getSOperaFactConMandato() {
		return sOperaFactConMandato;
	}

	public void setSOperaFactConMandato(String sOperaFactConMandato) {
		this.sOperaFactConMandato = sOperaFactConMandato;
	}


   public void setBOperaFactAutomatico(String bOperaFactAutomatico) {
      this.bOperaFactAutomatico = bOperaFactAutomatico;
   }


   public String getBOperaFactAutomatico() {
      return bOperaFactAutomatico;
   }


	public void setGeneraExp(String generaExp) {
		this.generaExp = generaExp;
	}


	public String getGeneraExp() {
		return generaExp;
	}


	public void setIdEpo(String idEpo) {
		this.idEpo = idEpo;
	}


	public String getIdEpo() {
		return idEpo;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getFecha() {
		return fecha;
	}

}