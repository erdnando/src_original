package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class DesPublicProvedorNoHabilitado implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

	public DesPublicProvedorNoHabilitado()
	{
	}	
		//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(DesPublicProvedorNoHabilitado.class);
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;		
	private String directorio;	
  public String ic_banco_fondeo;
  public String ic_epo;
  public String ic_pyme; 
  public String paginar;
  public String df_fecha_publicacion_de;
  public String df_fecha_publicacion_a;

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	 
    log.debug("-------------------------:: ");		
		log.debug("ic_banco_fondeo:: "+ ic_banco_fondeo);
		log.debug("ic_epo:: "+ic_epo);
		log.debug("ic_pyme:: "+ic_pyme);		
		log.debug("paginaOffset:: "+paginaOffset);
    log.debug("paginaNo:: "+paginaNo);  
    log.debug("df_fecha_publicacion_de:: "+df_fecha_publicacion_de);  
    log.debug("df_fecha_publicacion_a:: "+df_fecha_publicacion_a);  
	  log.debug("-------------------------:: "); 
        		
      
	  
	  	qrySentencia.append("SELECT count(1) total, 'DesPublicProvedor ::getAggregateCalculationQuery' origen " );
		              
       
             qrySentencia.append(" FROM com_documento d,"+
                       " comcat_pyme p, "+
                       " comrel_pyme_epo er, "+
                       " comcat_epo e, "+
                       " comrel_nafin rn, "+
                       " comcat_moneda m, "+
                       " com_contacto c, "+
                       " comcat_if i "+
                       " ");
         
        qrySentencia.append("  where d.DF_ALTA >= TO_DATE(?,'dd/mm/YYYY')    "+
                    " AND d.DF_ALTA < TO_DATE(?,'dd/mm/YYYY')+1  ");
         
        conditions.add(df_fecha_publicacion_de);
        conditions.add(df_fecha_publicacion_a);             
                    
                    
       if(!ic_banco_fondeo.equals("0") ){
        qrySentencia.append(" and e.ic_banco_fondeo = ? ");
        conditions.add(ic_banco_fondeo);
      }
      
       if(!ic_pyme.equals("0") ){
        qrySentencia.append(" and d.ic_pyme = ? ");
        conditions.add(ic_pyme);
      }
      
       if(!ic_epo.equals("0") ){
        qrySentencia.append(" and d.ic_epo = ? ");
        conditions.add(ic_epo);
      }
          
      
       qrySentencia.append(" AND d.ic_moneda = m.ic_moneda "+                   
                             " AND d.ic_epo = er.ic_epo "+
                             " AND d.ic_pyme = er.ic_pyme "+
                             " AND p.ic_pyme = er.ic_pyme "+
                             " AND p.ic_pyme = c.ic_pyme "+
                             " AND c.cs_primer_contacto = 'S' "+
                             " AND er.ic_pyme = rn.ic_epo_pyme_if "+
                             " AND rn.cg_tipo = 'P' "+
                             " AND er.cs_aceptacion <>'H' "+
                             " AND er.cs_habilitado = 'S' "+
                             " AND e.ic_epo = er.ic_epo "+
                             " AND d.ic_if = i.ic_if(+) "+
                             //" AND c.cg_tel ='0' "+   
                             "");
                             
      
    
                  
      qrySentencia.append(" group by d.ic_pyme, d.ic_if, d.ic_epo, e.cg_razon_social, "+
                    "          er.cg_pyme_epo_interno, "+
                    "          rn.ic_nafin_electronico, "+
                    "          p.cg_rfc, "+
                    "          c.cg_tel, "+
                    "          p.cg_razon_social, "+
                    "          m.cd_nombre "+
                    " order by d.ic_pyme, d.ic_if, d.ic_epo, e.cg_razon_social, "+
                    "          er.cg_pyme_epo_interno, "+
                     "          rn.ic_nafin_electronico, "+
                    "          p.cg_rfc, "+
                    "          c.cg_tel, "+
                    "          p.cg_razon_social, "+
                    "          m.cd_nombre" );
                    
                    
                    
			log.debug("getAggregateCalculationQuery "+conditions.toString());
			log.debug("getAggregateCalculationQuery "+qrySentencia.toString());
	
		return qrySentencia.toString();
		
	}
		
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
     log.debug("-------------------------:: ");		
		log.debug("ic_banco_fondeo:: "+ ic_banco_fondeo);
		log.debug("ic_epo:: "+ic_epo);
		log.debug("ic_pyme:: "+ic_pyme);		
		log.debug("paginaOffset:: "+paginaOffset);
    log.debug("paginaNo:: "+paginaNo);  
    log.debug("df_fecha_publicacion_de:: "+df_fecha_publicacion_de);  
    log.debug("df_fecha_publicacion_a:: "+df_fecha_publicacion_a);  
	  log.debug("-------------------------:: "); 
        		
            
                        
    qrySentencia.append(" Select  d.ic_pyme, d.ic_if, d.ic_epo,  "+
                   "  NVL (SUM (D.fn_monto), 0) AS TOTAL,"+
                    " COUNT (d.ig_numero_docto) AS numerodocumentos,"+
                    " e.cg_razon_social as nombrepo, "+                   
                    " er.cg_pyme_epo_interno AS pyme, "+
                    " rn.ic_nafin_electronico as nafinelectronico, "+
                    " p.cg_rfc as rfc, "+
                    " c.cg_tel as telefono, "+
                    " p.cg_razon_social as razonsocialproveedor, "+
                    " m.cd_nombre moneda ");
        
             qrySentencia.append(" FROM com_documento d,"+
                       " comcat_pyme p, "+
                       " comrel_pyme_epo er, "+
                       " comcat_epo e, "+
                       " comrel_nafin rn, "+                       
                       " comcat_moneda m, "+
                       " com_contacto c, "+
                       " comcat_if i "+
                       "  ");
         
        qrySentencia.append("  where d.DF_ALTA >= TO_DATE(?,'dd/mm/YYYY')    "+
                    " AND d.DF_ALTA < TO_DATE(?,'dd/mm/YYYY')+1  ");
         
        conditions.add(df_fecha_publicacion_de);
        conditions.add(df_fecha_publicacion_a);             
                    
                    
       if(!ic_banco_fondeo.equals("0") ){
        qrySentencia.append(" and e.ic_banco_fondeo = ? ");
        conditions.add(ic_banco_fondeo);
      }
      
       if(!ic_pyme.equals("0") ){
        qrySentencia.append(" and d.ic_pyme = ? ");
        conditions.add(ic_pyme);
      }
      
       if(!ic_epo.equals("0") ){
        qrySentencia.append(" and d.ic_epo = ? ");
        conditions.add(ic_epo);
      }
          
      qrySentencia.append(" AND d.ic_moneda = m.ic_moneda "+                   
                             " AND d.ic_epo = er.ic_epo "+
                             " AND d.ic_pyme = er.ic_pyme "+
                             " AND p.ic_pyme = er.ic_pyme "+
                             " AND p.ic_pyme = c.ic_pyme "+
                             " AND c.cs_primer_contacto = 'S' "+
                             " AND er.ic_pyme = rn.ic_epo_pyme_if "+
                             " AND rn.cg_tipo = 'P' "+
                             " AND er.cs_aceptacion <>'H' "+
                             " AND er.cs_habilitado = 'S' "+
                             " AND e.ic_epo = er.ic_epo "+
                             " AND d.ic_if = i.ic_if(+) "+
                             //" AND c.cg_tel ='0' "+   
                             "");
                  
      qrySentencia.append(" group by d.ic_pyme, d.ic_if, d.ic_epo, e.cg_razon_social, "+
                    "          er.cg_pyme_epo_interno, "+
                    "          rn.ic_nafin_electronico, "+
                    "          p.cg_rfc, "+
                    "          c.cg_tel, "+
                    "          p.cg_razon_social, "+
                    "          m.cd_nombre "+
                    " order by d.ic_pyme, d.ic_if, d.ic_epo, e.cg_razon_social, "+
                    "          er.cg_pyme_epo_interno, "+
                     "          rn.ic_nafin_electronico, "+
                    "          p.cg_rfc, "+
                    "          c.cg_tel, "+
                    "          p.cg_razon_social, "+
                    "          m.cd_nombre" );
					
		
			log.debug("getDocumentQuery "+conditions.toString());
			log.debug("getDocumentQuery "+qrySentencia.toString());

			return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
  
   log.debug("-------------------------:: ");		
		log.debug("ic_banco_fondeo:: "+ ic_banco_fondeo);
		log.debug("ic_epo:: "+ic_epo);
		log.debug("ic_pyme:: "+ic_pyme);		
		log.debug("paginaOffset:: "+paginaOffset);
    log.debug("paginaNo:: "+paginaNo);  
    log.debug("df_fecha_publicacion_de:: "+df_fecha_publicacion_de);  
    log.debug("df_fecha_publicacion_a:: "+df_fecha_publicacion_a);  
	  log.debug("-------------------------:: "); 
        		
            
   	
     qrySentencia.append(" Select  d.ic_pyme, d.ic_if, d.ic_epo, "+
                   "  NVL (SUM (D.fn_monto), 0) AS TOTAL,"+
                    " COUNT (d.ig_numero_docto) AS numerodocumentos,"+
                    " e.cg_razon_social as nombrepo, "+                   
                    " er.cg_pyme_epo_interno AS pyme, "+
                    " rn.ic_nafin_electronico as nafinelectronico, "+
                    " p.cg_rfc as rfc, "+
                    " c.cg_tel as telefono, "+
                    " p.cg_razon_social as razonsocialproveedor, "+
                    " m.cd_nombre moneda ");
        
             qrySentencia.append(" FROM com_documento d,"+
                       " comcat_pyme p, "+
                       " comrel_pyme_epo er, "+
                       " comcat_epo e, "+
                       " comrel_nafin rn, "+                       
                       " comcat_moneda m, "+
                        " com_contacto c, "+
                       " comcat_if i "+
                       "  ");
         
        qrySentencia.append("  where d.DF_ALTA >= TO_DATE(?,'dd/mm/YYYY')    "+
                    " AND d.DF_ALTA < TO_DATE(?,'dd/mm/YYYY')+1  ");
         
        conditions.add(df_fecha_publicacion_de);
        conditions.add(df_fecha_publicacion_a);             
                    
                    
       if(!ic_banco_fondeo.equals("0") ){
        qrySentencia.append(" and e.ic_banco_fondeo = ? ");
        conditions.add(ic_banco_fondeo);
      }
      
       if(!ic_pyme.equals("0") ){
        qrySentencia.append(" and d.ic_pyme = ? ");
        conditions.add(ic_pyme);
      }
      
       if(!ic_epo.equals("0") ){
        qrySentencia.append(" and d.ic_epo = ? ");
        conditions.add(ic_epo);
      }
          
      qrySentencia.append(" AND d.ic_moneda = m.ic_moneda "+                   
                             " AND d.ic_epo = er.ic_epo "+
                             " AND d.ic_pyme = er.ic_pyme "+
                             " AND p.ic_pyme = er.ic_pyme "+
                             " AND p.ic_pyme = c.ic_pyme "+
                             " AND c.cs_primer_contacto = 'S' "+
                             " AND er.ic_pyme = rn.ic_epo_pyme_if "+
                             " AND rn.cg_tipo = 'P' "+
                             " AND er.cs_aceptacion <>'H' "+
                             " AND er.cs_habilitado = 'S' "+
                             " AND e.ic_epo = er.ic_epo "+
                             " AND d.ic_if = i.ic_if(+) "+
                             //" AND c.cg_tel ='0' "+   
                             "");
                  
      
                
     qrySentencia.append("   AND ( ");
      
      for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			String clavePyme = lItem.get(0).toString();
			String claveIf = lItem.get(1).toString();
			String claveEpo = lItem.get(2).toString();
			if(i>0) {
				qrySentencia.append(" OR ");
			}
			if (clavePyme.equals("")	||	clavePyme.equals("0")){
				qrySentencia.append(" (d.ic_pyme IS NULL ");
			}else{
				qrySentencia.append(" (d.ic_pyme = ? ");
				conditions.add(new Long(clavePyme));			
			}
			if (claveIf.equals("")	||	claveIf.equals("0")	){
				qrySentencia.append(" AND d.ic_if IS NULL ");
			}else{
				qrySentencia.append(" AND  d.ic_if = ? ");
				conditions.add(new Long(claveIf));			
			}
			if (claveEpo.equals("")	||	claveEpo.equals("0")){
				qrySentencia.append(" AND d.ic_epo IS NULL )");
			}else{
				qrySentencia.append(" AND d.ic_epo = ? )");
				conditions.add(new Long(claveEpo));			
			}

		}//for(int i=0;i<ids.size();i++)
 		qrySentencia.append(" ) ");

    
     qrySentencia.append(" group by d.ic_pyme, d.ic_if, d.ic_epo, e.cg_razon_social, "+
                    "          er.cg_pyme_epo_interno, "+
                    "          rn.ic_nafin_electronico, "+
                    "          p.cg_rfc, "+
                    "          c.cg_tel, "+
                    "          p.cg_razon_social, "+
                    "          m.cd_nombre "+
                    " order by d.ic_pyme, d.ic_if, d.ic_epo, e.cg_razon_social, "+
                    "          er.cg_pyme_epo_interno, "+
                     "          rn.ic_nafin_electronico, "+
                    "          p.cg_rfc, "+
                    "          c.cg_tel, "+
                    "          p.cg_razon_social, "+
                    "          m.cd_nombre" );
                    
                    
			log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
			log.debug(" getDocumentSummaryQueryForIds "+qrySentencia.toString());
	
		
		return qrySentencia.toString();
	}
	

	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
     log.debug("-------------------------:: ");		
		log.debug("ic_banco_fondeo:: "+ ic_banco_fondeo);
		log.debug("ic_epo:: "+ic_epo);
		log.debug("ic_pyme:: "+ic_pyme);		
		log.debug("paginaOffset:: "+paginaOffset);
    log.debug("paginaNo:: "+paginaNo);  
    log.debug("df_fecha_publicacion_de:: "+df_fecha_publicacion_de);  
    log.debug("df_fecha_publicacion_a:: "+df_fecha_publicacion_a);  
	  log.debug("-------------------------:: "); 
        		
            

			
      qrySentencia.append(" Select   d.ic_pyme, d.ic_if, d.ic_epo, "+
                   "  NVL (SUM (D.fn_monto), 0) AS TOTAL,"+
                    " COUNT (d.ig_numero_docto) AS numerodocumentos,"+
                    " e.cg_razon_social as nombrepo, "+                   
                    " er.cg_pyme_epo_interno AS pyme, "+
                    " rn.ic_nafin_electronico as nafinelectronico, "+
                    " p.cg_rfc as rfc, "+
                    " c.cg_tel as telefono, "+
                    " p.cg_razon_social as razonsocialproveedor, "+
                    " m.cd_nombre moneda ");
        
             qrySentencia.append(" FROM com_documento d,"+
                       " comcat_pyme p, "+
                       " comrel_pyme_epo er, "+
                       " comcat_epo e, "+
                       " comrel_nafin rn, "+                      
                       " comcat_moneda m, "+
                       " com_contacto c, "+
                       " comcat_if i "+
                       "  ");
         
        qrySentencia.append("  where d.DF_ALTA >= TO_DATE(?,'dd/mm/YYYY')    "+
                    " AND d.DF_ALTA < TO_DATE(?,'dd/mm/YYYY')+1  ");
         
        conditions.add(df_fecha_publicacion_de);
        conditions.add(df_fecha_publicacion_a);             
                    
                    
       if(!ic_banco_fondeo.equals("0") ){
        qrySentencia.append(" and e.ic_banco_fondeo = ? ");
        conditions.add(ic_banco_fondeo);
      }
      
       if(!ic_pyme.equals("0") ){
        qrySentencia.append(" and d.ic_pyme = ? ");
        conditions.add(ic_pyme);
      }
      
       if(!ic_epo.equals("0") ){
        qrySentencia.append(" and d.ic_epo = ? ");
        conditions.add(ic_epo);
      }
          
      qrySentencia.append(" AND d.ic_moneda = m.ic_moneda "+                   
                             " AND d.ic_epo = er.ic_epo "+
                             " AND d.ic_pyme = er.ic_pyme "+
                             " AND p.ic_pyme = er.ic_pyme "+
                             " AND p.ic_pyme = c.ic_pyme "+
                             " AND c.cs_primer_contacto = 'S' "+
                             " AND er.ic_pyme = rn.ic_epo_pyme_if "+
                             " AND rn.cg_tipo = 'P' "+
                             " AND er.cs_aceptacion <>'H' "+
                             " AND er.cs_habilitado = 'S' "+
                             " AND e.ic_epo = er.ic_epo "+
                             " AND d.ic_if = i.ic_if(+) "+
                             //" AND c.cg_tel ='0' "+   
                             "");
                  
      qrySentencia.append(" group by d.ic_pyme, d.ic_if, d.ic_epo, e.cg_razon_social, "+
                    "          er.cg_pyme_epo_interno, "+
                    "          rn.ic_nafin_electronico, "+
                    "          p.cg_rfc, "+
                    "          c.cg_tel, "+
                    "          p.cg_razon_social, "+
                    "          m.cd_nombre "+
                    " order by d.ic_pyme, d.ic_if, d.ic_epo, e.cg_razon_social, "+
                    "          er.cg_pyme_epo_interno, "+
                     "          rn.ic_nafin_electronico, "+
                    "          p.cg_rfc, "+
                    "          c.cg_tel, "+
                    "          p.cg_razon_social, "+
                    "          m.cd_nombre" );
                    
		
		
			log.debug("getDocumentQueryFile "+conditions.toString());
			log.debug("getDocumentQueryFile "+qrySentencia.toString());
		
		return qrySentencia.toString();
	}//getDocumentQueryFile

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		try {
			HttpSession session = request.getSession();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[]	=	{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual	=	fechaActual.substring(0,2);
			String mesActual	=	meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual	=	fechaActual.substring(6,10);
			String horaActual	=	new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			(String)session.getAttribute("sesExterno"),
			(String)session.getAttribute("strNombre"),
			(String)session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),
			(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

			pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
			horaActual, "formas", ComunesPDF.CENTER);

			pdfDoc.setLTable(9, 100);
			pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,6);
			pdfDoc.setLCell("Documentos negociables","celda01",ComunesPDF.CENTER,2);
			pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("No Proveedor","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("No. Nafin Electr�nico","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("RFC","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tel�fono Contacto","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Nombre � Razon Social","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Total","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);

			String  nombrepo= "", nombreif = "", Noproveedor = "", nafinelectronico= "", rfc="", telefono= "",  razonsocial = "", total= "", monto ="", moneda="";

			while (rs.next()) {
				nombrepo = (rs.getString("nombrepo") == null) ? "" : rs.getString("nombrepo");
				Noproveedor = (rs.getString("pyme") == null) ? "" : rs.getString("pyme");
				nafinelectronico= (rs.getString("nafinelectronico") == null) ? "" : rs.getString("nafinelectronico");
				rfc=(rs.getString("rfc") == null)?"":rs.getString("rfc"); 
				telefono= (rs.getString("telefono") == null) ? "0" : rs.getString("telefono");
				razonsocial = (rs.getString("razonsocialproveedor") == null) ? "" : rs.getString("razonsocialproveedor"); 
				total= (rs.getString("NumeroDocumentos") == null) ? "" : rs.getString("NumeroDocumentos");
				monto =(rs.getString("Total") == null) ? "" : rs.getString("Total");
				moneda =(rs.getString("moneda") == null) ? "" : rs.getString("moneda");
			
				pdfDoc.setLCell(nombrepo,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(Noproveedor,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(nafinelectronico,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(rfc,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(telefono,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(razonsocial,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(total,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$" + Comunes.formatoDecimal(monto, 2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
			}
			pdfDoc.addLTable();
			pdfDoc.endDocument();

		}catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}finally {
		}
		return nombreArchivo;
	}

/**
 * Genera el archivo CSV o PDF seg�n el tipo que se le indique.
 * En el caso del PDF, el archivo generado no comtempla paginaci�n, imprime todos los registros que trae la consulta.
 * @param request
 * @param rs
 * @param path
 * @param tipo: CSV o PDF
 * @return nombre del archivo
 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
	
		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo = new CreaArchivo();
		
		if ("CSV".equals(tipo)){
			try {
				int registros = 0;
				String  nombrepo= "", nombreif = "", Noproveedor = "", nafinelectronico= "", rfc="", telefono= "",  razonsocial = "", total= "", monto ="", moneda="";

				while(rs.next()){
					if(registros==0) {
						contenidoArchivo.append(" EPO"+ "," +
							" No Proveedor"+ "," +
							" No. Nafin Electronico"+ "," +
							" RFC"+ "," +
							" Telefono Contacto"+ "," +
							" Nombre � Razon Social"+ "," +
							" Total"+ "," +
							" Monto"+ "," +
							" Moneda"+ "\n");
					}
					registros++;
					nombrepo         = (rs.getString("nombrepo")             == null) ? "" : rs.getString("nombrepo");
					Noproveedor      = (rs.getString("pyme")                 == null) ? "" : rs.getString("pyme");
					nafinelectronico = (rs.getString("nafinelectronico")     == null) ? "" : rs.getString("nafinelectronico");
					rfc              = (rs.getString("rfc")                  == null) ? "" : rs.getString("rfc"); 
					telefono         = (rs.getString("telefono")             == null) ? "0": rs.getString("telefono");
					razonsocial      = (rs.getString("razonsocialproveedor") == null) ? "" : rs.getString("razonsocialproveedor"); 
					total            = (rs.getString("NumeroDocumentos")     == null) ? "" : rs.getString("NumeroDocumentos");
					monto            = (rs.getString("Total")                == null) ? "" : rs.getString("Total");
					moneda           = (rs.getString("moneda")               == null) ? "" : rs.getString("moneda");

					contenidoArchivo.append( nombrepo.replaceAll(",","") +","+
						Noproveedor.replaceAll(",","") +","+
						nafinelectronico.replaceAll(",","") +","+
						rfc.replaceAll(",","") +","+
						telefono.replaceAll(",","") +","+
						razonsocial.replaceAll(",","") +","+
						total.replaceAll(",","") +","+
						monto.replaceAll(",","") +","+
						moneda.replaceAll(",","") +"\n");
				}

				if(registros >= 1){
					if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
						nombreArchivo="ERROR";
					}else{
						nombreArchivo = archivo.nombre;
					}
				}
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		} else if("PDF".equals(tipo)){
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
				horaActual, "formas", ComunesPDF.CENTER);

				pdfDoc.setLTable(9, 100);
				pdfDoc.setLCell("EPO",                   "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("No Proveedor",          "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Nafin Electr�nico", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("RFC",                   "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Tel�fono Contacto",     "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre � Razon Social", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Total",                 "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto",                 "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda",                "celda01", ComunesPDF.CENTER);
				pdfDoc.setLHeaders();

				String  nombrepo= "", nombreif = "", Noproveedor = "", nafinelectronico= "", rfc="", telefono= "",  razonsocial = "", total= "", monto ="", moneda="";

				while (rs.next()){
					nombrepo         = (rs.getString("nombrepo")             == null) ? "" : rs.getString("nombrepo");
					Noproveedor      = (rs.getString("pyme")                 == null) ? "" : rs.getString("pyme");
					nafinelectronico = (rs.getString("nafinelectronico")     == null) ? "" : rs.getString("nafinelectronico");
					rfc              = (rs.getString("rfc")                  == null) ? "" : rs.getString("rfc");
					telefono         = (rs.getString("telefono")             == null) ? "0": rs.getString("telefono");
					razonsocial      = (rs.getString("razonsocialproveedor") == null) ? "" : rs.getString("razonsocialproveedor");
					total            = (rs.getString("NumeroDocumentos")     == null) ? "" : rs.getString("NumeroDocumentos");
					monto            = (rs.getString("Total")                == null) ? "" : rs.getString("Total");
					moneda           = (rs.getString("moneda")               == null) ? "" : rs.getString("moneda");

					pdfDoc.setLCell(nombrepo,                               "formas",ComunesPDF.LEFT  );
					pdfDoc.setLCell(Noproveedor,                            "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nafinelectronico,                       "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(rfc,                                    "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(telefono,                               "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(razonsocial,                            "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(total,                                  "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$" + Comunes.formatoDecimal(monto, 2), "formas",ComunesPDF.RIGHT );
					pdfDoc.setLCell(moneda,                                 "formas",ComunesPDF.CENTER);
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo ", e);
			} finally{
				try{
					rs.close();
				} catch(Exception e){}
			}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
 	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	
	public String getDirectorio() {
		return directorio;
	}

	public void setDirectorio(String directorio) {
		this.directorio = directorio;
	}

  public String getIc_banco_fondeo() {
    return ic_banco_fondeo;
  }

  public void setIc_banco_fondeo(String ic_banco_fondeo) {
    this.ic_banco_fondeo = ic_banco_fondeo;
  }

  public String getIc_epo() {
    return ic_epo;
  }

  public void setIc_epo(String ic_epo) {
    this.ic_epo = ic_epo;
  }

  public String getIc_pyme() {
    return ic_pyme;
  }

  public void setIc_pyme(String ic_pyme) {
    this.ic_pyme = ic_pyme;
  }

  public String getPaginar() {
    return paginar;
  }

  public void setPaginar(String paginar) {
    this.paginar = paginar;
  }

  public String getDf_fecha_publicacion_de() {
    return df_fecha_publicacion_de;
  }

  public void setDf_fecha_publicacion_de(String df_fecha_publicacion_de) {
    this.df_fecha_publicacion_de = df_fecha_publicacion_de;
  }

  public String getDf_fecha_publicacion_a() {
    return df_fecha_publicacion_a;
  }

  public void setDf_fecha_publicacion_a(String df_fecha_publicacion_a) {
    this.df_fecha_publicacion_a = df_fecha_publicacion_a;
  }
	

	
}