package com.netro.descuento;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class RepDoctosPymeDEbean{

	private String qrysentencia="";
	private String icestatusdocto="";
	private String icepo="";
	private String icpyme="";
	private String sFechaVencPyme="";
	private String operaFactConMandato="";
	private boolean operafactoraje=false;
	private List conditions;
	private Registros registros;

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(RepDoctosPymeDEbean.class);
	
	
	public void setOperaFactConMandato(String operaFactConMandato) {
		this.operaFactConMandato=operaFactConMandato;
	}
	public void setIcestatusdocto(String icestatusdocto){
		this.icestatusdocto=icestatusdocto;
	}
	public void setIcepo(String icepo){
		this.icepo=icepo;
	}
	public void setOperafactoraje(boolean operaFactoraje){
		this.operafactoraje=operaFactoraje;
	}

	public void setFechaVencPyme(String sFechaVencPyme){
		this.sFechaVencPyme=sFechaVencPyme;
	}
	public void setIcePyme(String icPyme) {
		this.icpyme = icPyme;
	}

	public Registros executeQuery() {
		AccesoDB con = new AccesoDB();
		Registros registros = new Registros();
		conditions = new ArrayList();
		try{
			con.conexionDB();
			if (!this.icestatusdocto.equals("16"))	{
				conditions.add(this.icepo);
				conditions.add(this.icpyme);
				if (this.icestatusdocto.equals("24"))	{	//Para el caso de AutorIF
					conditions.add(this.icepo);
					conditions.add(this.icepo);
				}
				if (this.icestatusdocto.equals("4") && this.operafactoraje == true)	{	//Para el caso de Operada
					conditions.add(this.icepo);
					conditions.add(this.icpyme);
				}
			}else	{
				conditions.add(this.icpyme);
				conditions.add(this.icpyme);
				conditions.add(this.icepo);
			}
			registros = con.consultarDB(this.getQrysentencia(),conditions);
			return registros;
		} catch(Exception e) {
			throw new AppException("RepDoctosPymeDEbean::ExecuteQuery(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}
	
	public void setQrysentencia(String valorx){
    	StringBuffer campos = new StringBuffer("");
		StringBuffer condiciones = new StringBuffer("");
		StringBuffer tablas = new StringBuffer("");
		StringBuffer ordenarPor = new StringBuffer("");
    	AccesoDB con = new AccesoDB();
		
    	try {
    		
			con.conexionDB();
			
			String qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			ps.executeUpdate();
			ps.close();
 
			if(!this.icestatusdocto.equals("16")) {

				campos.append(
					" a.ic_epo,(   DECODE (c.cs_habilitado, 'N', '*', 'S', ' ')|| ' '|| c.cg_razon_social) "+(this.icestatusdocto.equals("4")?" AS NOMBREEPO":"NOMBREEPO")+", "+
					" a.ig_numero_docto, "+((this.icestatusdocto.equals("2")||this.icestatusdocto.equals("23"))?"a":this.icestatusdocto.equals("4")?"cs":"b")+".cc_acuse, "+
					" TO_CHAR (a.df_fecha_docto, 'DD/MM/YYYY') fecha_docto, "+
					" TO_CHAR (a.df_fecha_venc"+sFechaVencPyme+", 'DD/MM/YYYY') fecha_venc, d.cd_nombre, d.cd_nombre_ing AS namecurrency, "+
					" a.ic_moneda, a.fn_monto, ");

				campos.append(
					(this.icestatusdocto.equals("2")||this.icestatusdocto.equals("23"))?" DECODE (a.ic_moneda,1, cv.fn_aforo,54, cv.fn_aforo_dl) AS fn_aforo, ":"");

				campos.append(
					//" ( 	DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || i.cg_razon_social ) AS nombreif, "+ //Linea a modificar FODEA 17
					"	decode(ds.cs_opera_fiso, 'S', i3.cg_razon_social, 'N', (decode (i.cs_habilitado, 'N', '*', 'S', ' ')||' '||i.cg_razon_social)) AS nombreif,"+		//FODEA 17
					" a.cs_dscto_especial, i2.cg_razon_social AS nombre_beneficiario, "+
					" a.fn_porc_beneficiario, ");

				campos.append(
					(this.icestatusdocto.equals("2")||this.icestatusdocto.equals("23"))?
					" (a.fn_monto * a.fn_porc_beneficiario) / 100 recibir_beneficiario ":"");
				String auxPlazo = null;
				
				// Fodea 048 - 2012 - Fondeo Propio
				// Para agilizar la consulta, obtener la fecha del Siguiente d�a h�bil por EPO
				String fechaHabilSiguienteXEpo = null; //new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				if( !this.icestatusdocto.equals("4") ){
					
					qrySentencia =
						" SELECT "  +
						"   TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha "   +
						" FROM   "  +
						"   DUAL "  ;
					ps 				= con.queryPrecompilado(qrySentencia);
					ps.setInt(1, Integer.parseInt(this.icepo));
					ResultSet rs 	= ps.executeQuery();
					
					ps.clearParameters();
					if(rs.next()) {
						fechaHabilSiguienteXEpo = rs.getString(1);//==null?fechaHabilSiguienteXEpo:rs.getString(1);
					}
					rs.close();
					ps.close();
					
					// Este error NUNCA DEBER�A DE OCRRIR se deja para avisar al usuario que se presento un problema con la
					// funcion sigfechahabilxepo y de esta manera evitar que se le muestre un plazo erroneo
					if( fechaHabilSiguienteXEpo == null ){
						throw new AppException("No se pudo determinar la fecha del d�a h�bil siguiente por EPO");
					}
					
				}
				
				if(
						this.icestatusdocto.equals("4")  //  "4":	Operada	
				){
				
					// auxPlazo = " DECODE(a.ic_estatus_docto, 4, cs.ig_plazo, a.df_fecha_venc"+sFechaVencPyme+"- DECODE(a.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaSigDol+"', 'dd/mm/yyyy'))) PLAZO, ";
					auxPlazo = "  cs.ig_plazo PLAZO, ";
				
				// Fodea 048 - 2012 - Fondeo Propio
				} else if(
						this.icestatusdocto.equals("3")	//  "3": Seleccionada Pyme
							|| 
						this.icestatusdocto.equals("24")	// "24": En Proceso de Autorizacion IF
				){ 
	
					auxPlazo = 
						" ( " +
							" a.df_fecha_venc"+sFechaVencPyme + " - " +
							" 	(CASE  " +
							" 	WHEN a.ic_moneda = 1 OR (a.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
							"    TRUNC(SYSDATE) " +
							" 	WHEN a.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
							"   TO_DATE('"+fechaHabilSiguienteXEpo+"', 'dd/mm/yyyy')  "  +
							//"    sigfechahabilxepo(a.ic_epo, TRUNC(SYSDATE), 1, '+') " +
							" 	END) " + 
						" ) " + 	
						" AS PLAZO, ";
				
				// Fodea 048 - 2012 - Fondeo Propio
				} else if( this.icestatusdocto.equals("26") ) { // "26": Programado Pyme
					
					// Nota: como funciona el siguiente dia habil cuando se consulte el documento en su d�a programado 
					// Con el propsito de agilizar la consulta, obtener la fecha del segundo d�a h�bil sguiente por EPO
					String segundaFechaHabilSiguienteXEpo = null;
					qrySentencia =
						" SELECT "  +
						"   TO_CHAR(sigfechahabilxepo (?, TO_DATE( ?, 'DD/MM/YYYY' ), 1, '+'), 'dd/mm/yyyy') fecha "  +
						" FROM   "  +
						"   DUAL "  ;
					ps 				= con.queryPrecompilado(qrySentencia);
					ps.setInt(1, Integer.parseInt(this.icepo));
					ps.setString(2, fechaHabilSiguienteXEpo);
					ResultSet rs 	= ps.executeQuery();
					
					ps.clearParameters();
					if(rs.next()) {
						segundaFechaHabilSiguienteXEpo = rs.getString(1);
					}
					// Este error NUNCA DEBER�A DE OCRRIR se deja para avisar al usuario que se presento un problema con la
					// funcion sigfechahabilxepo y de esta manera evitar que se le muestre un plazo erroneo
					if( segundaFechaHabilSiguienteXEpo == null ){
						throw new AppException("No se pudo determinar la fecha del segundo d�a h�bil siguiente por EPO");
					}
					rs.close();
					ps.close();
 
					// ya sabemos que ser� para el siguiente d�a h�bil x epo
					auxPlazo = 
						" ( "  +
							" a.df_fecha_venc"+sFechaVencPyme + " - "  +
							" 	(CASE  "  +
							" 	WHEN a.ic_moneda = 1 OR (a.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN "  +
							// "    TRUNC(SYSDATE) " +
							"    TO_DATE ('"+fechaHabilSiguienteXEpo+"', 'dd/mm/yyyy') "  +
							" 	WHEN a.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN "  +
							//"    sigfechahabilxepo(a.ic_epo, TRUNC(SYSDATE), 1, '+') " +
							"   TO_DATE ('"+segundaFechaHabilSiguienteXEpo+"', 'dd/mm/yyyy')  "  +
							" 	END) "  + 
						" ) "  + 	
						" AS PLAZO, ";
 
				} else { // Para todos los demas estatus
 
					auxPlazo = " (TRUNC (a.df_fecha_venc"+sFechaVencPyme+") - DECODE (a.ic_moneda, 1, TRUNC (SYSDATE), 54, TO_DATE ('"+fechaHabilSiguienteXEpo+"', 'dd/mm/yyyy'))) PLAZO, ";
					
				}
				
				campos.append(
					(this.icestatusdocto.equals("3")||this.icestatusdocto.equals("24")||this.icestatusdocto.equals("4")||this.icestatusdocto.equals("26"))?
					" e.in_tasa_aceptada, "+
					auxPlazo+
					" (a.df_fecha_venc - TO_DATE(TO_CHAR(SYSDATE, 'DD/MM/YYYY'),'DD/MM/YYYY')) OTRO_PLAZO, "+	
					" a.fn_porc_anticipo, "+
					" a.fn_monto_dscto, "+
					" e.in_importe_interes, "+
					" e.in_importe_recibir, "+
					" e.IN_IMPORTE_RECIBIR - (e.fn_importe_recibir_benef) NETO_RECIBIR, "+
					" e.fn_importe_recibir_benef RECIBIR_BENEFICIARIO ":"");

				campos.append(
					(this.icestatusdocto.equals("26") || this.icestatusdocto.equals("3"))? // Fodea 005-2009 24 hrs
					" ,TO_CHAR (e.df_programacion, 'DD/MM/YYYY') df_programacion ":"");
				campos.append(" ,tf.cg_nombre as TIPO_FACTORAJE, "+
					" 'RepDoctosPymeDEbean.java' as claseBean ");
			} else {
				campos.append(
					" c.cg_razon_social AS nombreepo, "+
					//" (   DECODE (i.cs_habilitado, 'N', '*', 'S', ' ')  || ' ' || i.cg_razon_social ) AS nombreif, "+ Linea modificada por FODEA 17 
					"	decode(ds.cs_opera_fiso, 'S', i3.cg_razon_social, 'N', (decode (i.cs_habilitado, 'N', '*', 'S', ' ')||' '||i.cg_razon_social)) AS nombreif,"+		//FODEA 17
					" a.ig_numero_docto, "+
					" TO_CHAR (a.df_fecha_docto, 'DD/MM/YYYY') AS df_fecha_docto, "+
					" TO_CHAR (a.df_fecha_venc"+sFechaVencPyme+", 'DD/MM/YYYY') AS df_fecha_venc, "+
					" d.cd_nombre AS nombremoneda, a.fn_monto, a.fn_porc_anticipo, "+
					" a.fn_monto_dscto, e.in_tasa_aceptada, "+
					" ed.cd_descripcion AS estatusdocumento, "+
					" TO_CHAR (cs.df_fecha_solicitud, 'DD/MM/YYYY') AS df_fecha_solicitud, "+
					" a.ic_moneda, a.ic_documento, e.in_importe_recibir, "+
					" a.cs_cambio_importe, det.detalles, a.cs_dscto_especial, "+
					" a.ic_estatus_docto, cc1.fn_monto_pago, e.cc_acuse, "+
					" e.in_importe_recibir - e.fn_importe_recibir_benef neto_recibir, "+
					" i2.cg_razon_social AS nombre_beneficiario, a.fn_porc_beneficiario, "+
					" e.fn_importe_recibir_benef recibir_beneficiario, "+
					" cc1.fn_porc_docto_aplicado AS porcdoctoaplicado, cs.ig_plazo, "+
					" e.in_importe_interes,i.ig_tipo_piso, e.fn_remanente, "+
					" tf.cg_nombre as TIPO_FACTORAJE, "+
					" 'RepDoctosPymeDEbean.java' as claseBean ");
			}
			
			if("S".equals(operaFactConMandato)){
			campos.append(",cm.cg_razon_social AS mandante");
			}
			
			if(!this.icestatusdocto.equals("16")){
				condiciones.append(
					" WHERE "+((this.icestatusdocto.equals("2")||this.icestatusdocto.equals("23"))?"a":"e")+".cc_acuse = b.cc_acuse "+
					" AND a.ic_documento = ds.ic_documento  "+ //FODEA 17
					"  AND a.ic_epo = c.ic_epo "+
					"  AND a.ic_moneda = d.ic_moneda "+
					"   AND ds.ic_if = i3.ic_if(+)  ");	//FODEA 17
				condiciones.append("    AND a.cs_dscto_especial = tf.CC_TIPO_FACTORAJE  ");
				
				condiciones.append(
					(this.icestatusdocto.equals("2")||this.icestatusdocto.equals("23"))?"  AND a.ic_epo = cv.ic_epo ":"");

				condiciones.append(
					(this.icestatusdocto.equals("2"))?" AND b.df_fechahora_carga >= TRUNC(SYSDATE)  AND b.df_fechahora_carga < TRUNC(SYSDATE) + 1 ":"");
					
					
					
				condiciones.append(
					(this.icestatusdocto.equals("23"))?" AND cce.ic_documento = a.ic_documento "+
					" AND cce.ic_cambio_estatus = 27 "+
					" AND cce.dc_fecha_cambio = "+
					"        (SELECT MAX (cce2.dc_fecha_cambio) "+
					"           FROM comhis_cambio_estatus cce2 "+
					"          WHERE cce2.ic_documento = a.ic_documento "+
					"            AND cce2.ic_cambio_estatus = 27) "+
					" AND cce.dc_fecha_cambio >= TRUNC(SYSDATE)  AND cce.dc_fecha_cambio < TRUNC(SYSDATE) + 1 ":"");

				condiciones.append(
					(this.icestatusdocto.equals("3")||this.icestatusdocto.equals("4")||this.icestatusdocto.equals("26"))?
						" AND a.ic_documento = e.ic_documento "+
						" AND b.ic_producto_nafin = 1 "+
						" AND b.df_fecha_hora >= TRUNC(SYSDATE)  AND b.df_fecha_hora < TRUNC(SYSDATE) + 1 "
					:
						""
				);
				
				// Fodea 000 - 2012. Fodea 000 - 2012. ADMIN - Proceso de Aut IF	
				condiciones.append(
					(this.icestatusdocto.equals("24"))?
						" AND a.ic_documento = e.ic_documento "+
						" AND b.ic_producto_nafin = 1 "
					:
						""
				);
  
				condiciones.append(
					(this.icestatusdocto.equals("4"))?" AND e.ic_documento = cs.ic_documento ":"");

				condiciones.append(
					" AND a.ic_epo = ? "+
					"  AND a.ic_estatus_docto = "+ this.icestatusdocto+
					"  AND a.ic_pyme = ? "+
					"  AND a.ic_if = i.ic_if"+ ((this.icestatusdocto.equals("2")||this.icestatusdocto.equals("23"))?"(+)":"")+
					"  AND a.ic_beneficiario = i2.ic_if(+) ");

				condiciones.append(
					(this.icestatusdocto.equals("24"))?	"	AND E.IC_EPO= ? "+
					"	AND C.IC_EPO= ? ":"");
				
				// Fodea 048 - 2012 - Fondeo Propio
				if(
					this.icestatusdocto.equals("3")
						||
					this.icestatusdocto.equals("24")
						||
					this.icestatusdocto.equals("26")
				){
					condiciones.append(
						" 	AND a.ic_if   = iexp.ic_if     " +
						" 	AND a.ic_epo  = iexp.ic_epo    " +
						" 	AND iexp.ic_producto_nafin = 1 "
					);
				}
	  
		    } else {
		    	condiciones.append(
					"  WHERE a.ic_epo = c.ic_epo  "+
					"    AND a.ic_pyme = p.ic_pyme  "+
					"    AND a.ic_estatus_docto = 16  "+//Aplicado a credito
					"    AND a.ic_moneda = d.ic_moneda  "+
					"    AND a.ic_documento = e.ic_documento  "+
					"    AND a.ic_estatus_docto = ed.ic_estatus_docto  "+
					"    AND a.cs_dscto_especial = tf.CC_TIPO_FACTORAJE  "+
					"    AND a.ic_documento = cs.ic_documento(+) "+
					"    AND a.ic_if = i.ic_if(+)  "+
					"    AND a.ic_documento = cc1.ic_documento(+) "+
					"    AND a.ic_documento = det.ic_documento(+) "+
					"    AND a.ic_beneficiario = i2.ic_if(+)  "+
					"    AND a.ic_pyme = ? "+
					" 	  AND e.df_fecha_seleccion >= TRUNC(SYSDATE)  AND e.df_fecha_seleccion < TRUNC(SYSDATE) + 1 "+
					"    AND a.ic_epo = ? ");
		    }
				
			if("S".equals(operaFactConMandato)){
			condiciones.append(" AND a.ic_mandante = cm.ic_mandante(+)" );
			}	

		    tablas.append(
				"     com_documento a, "+
				((this.icestatusdocto.equals("2")||this.icestatusdocto.equals("23"))?"comvis_aforo cv, ":"COM_DOCTO_SELECCIONADO e, ") +
				((this.icestatusdocto.equals("4")||this.icestatusdocto.equals("16"))?"com_solicitud cs, ":"") );

			if(!this.icestatusdocto.equals("16"))
				 tablas.append("     com_acuse"+((this.icestatusdocto.equals("2")||this.icestatusdocto.equals("23"))?1:2)+" b, ");

			 tablas.append(
				"     comcat_epo c, "+
				"     comcat_moneda d, "+
				"     com_docto_seleccionado ds, "+	//FODEA 17
				"     comcat_if i, "+
				"     comcat_if i2, "+
				"     comcat_if i3 ");	//FODEA 17

			 tablas.append(
				(this.icestatusdocto.equals("23"))?" ,comhis_cambio_estatus cce ":
				(this.icestatusdocto.equals("16"))?" ,comcat_pyme p, comcat_estatus_docto ed, "+
				" (SELECT   ic_documento, COUNT (ic_documento) AS detalles "+
				" FROM com_documento_detalle " +
				" GROUP BY ic_documento) det, "+
				"   (SELECT   cc.ic_documento, cc.fn_porc_docto_aplicado, "+
				"             SUM (cc.fn_monto_pago) AS fn_monto_pago "+
				"        FROM com_cobro_credito cc, com_documento d "+
				"       WHERE cc.ic_documento = d.ic_documento "+
				"             AND d.ic_pyme = ? "+
				"    GROUP BY cc.ic_documento, cc.fn_porc_docto_aplicado) cc1 ":"");
			
			if("S".equals(operaFactConMandato)){
			 tablas.append("	,comcat_mandante cm " );
			}	

			tablas.append(" , comcat_tipo_factoraje tf ");
			if (!this.icestatusdocto.equals("4")){
					ordenarPor.append(
						" ORDER BY c.cg_razon_social, "+
						"     a.ig_numero_docto, "+
						"     a.df_fecha_docto, "+
						"     a.df_fecha_venc, "+
						"     d.cd_nombre, "+
						"     a.fn_monto, "+
						"     a.fn_monto_dscto ");
			}
			
			// Fodea 048 - 2012 - Fondeo Propio
			if(!this.icestatusdocto.equals("16")){
				
				if(
					this.icestatusdocto.equals("3")
						||
					this.icestatusdocto.equals("24")
						||
					this.icestatusdocto.equals("26")
				){
					tablas.append(" , comrel_if_epo_x_producto iexp ");
				}
				
			}
			
			//Negociable y Pignorado sin cambios ordenarPor
			//SelPyme,AutorIF
			ordenarPor.append(
				(this.icestatusdocto.equals("3")||this.icestatusdocto.equals("24")||this.icestatusdocto.equals("16") ||this.icestatusdocto.equals("26"))?
				" ,i.cg_razon_social, e.in_tasa_aceptada, e.in_importe_interes, e.in_importe_recibir ":"");
			ordenarPor.append(
				(this.icestatusdocto.equals("3")||this.icestatusdocto.equals("24"))?", b.cc_acuse ":(this.icestatusdocto.equals("16") ||this.icestatusdocto.equals("26"))?", e.cc_acuse ":"");

			if (this.icestatusdocto.equals("4")){
				ordenarPor.append(
					" ORDER BY NOMBREEPO, ig_numero_docto,fecha_docto,fecha_venc,cd_nombre,fn_monto,fn_monto_dscto, nombre_beneficiario, "+
					" in_tasa_aceptada, in_importe_interes, in_importe_recibir, cc_acuse ");
			}

			if (this.icestatusdocto.equals("4") && this.operafactoraje ){
	        this.qrysentencia =
				"select * from ( "+
				" select " + campos.toString() + " from " + tablas.toString() + condiciones.toString()+
				" UNION ALL "+
				" select " + campos + " from " + tablas.toString() + remplazar(condiciones.toString(),"e.ic_documento = cs.ic_documento","a.ic_docto_asociado = cs.ic_documento") +
				" )"+
				ordenarPor.toString();
			} else {
				this.qrysentencia = " select " + campos.toString() + " from " + tablas.toString() + condiciones.toString() + ordenarPor.toString() ;
			}
			//this.operafactoraje=false;
			log.debug(":::::::::::::::::: qrysentencia ::::::"+icestatusdocto+":::::::::"+qrysentencia);
		} catch(Exception e) {
			log.error("RepDoctosPymeDEbean::setQrysentencia Exception "+e);			
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
	}

	public String getOperaFactConMandato() {
		return this.operaFactConMandato;
	}
	public String getIcestatusdocto(){
		return	this.icestatusdocto;
	}
	public String getIcepo(){
		return	this.icepo;
	}
	public String getQrysentencia(){
		return	this.qrysentencia;
	}
	public boolean getOperafactoraje(){
		return this.operafactoraje;
	}

	public static String remplazar(String psWord, String psReplace, String psNewSeg) {
	     StringBuffer lsNewStr = new StringBuffer();
	     int liFound = 0;
	     int liLastPointer=0;
	     do {
	       liFound = psWord.indexOf(psReplace, liLastPointer);

	       if ( liFound < 0 )
	          lsNewStr.append(psWord.substring(liLastPointer,psWord.length()));

	       else {
	          if (liFound > liLastPointer)
	             lsNewStr.append(psWord.substring(liLastPointer,liFound));
	          lsNewStr.append(psNewSeg);
	          liLastPointer = liFound + psReplace.length();
	       }
	     }while (liFound > -1);
	     return lsNewStr.toString();
	}
	
}//class PymeRepDocSolEstatusBean