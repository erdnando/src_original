package com.netro.descuento;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsBitaTasaPrefeNego implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

	private String aplicaFloating;

	public ConsBitaTasaPrefeNego(){}	
		//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ConsBitaTasaPrefeNego.class);
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;		
	private String directorio;
	private String ic_epo;
	private String ic_pyme; 
	private String paginar;
	private String df_fecha_registro_de;
	private String df_fecha_registro_a;
	private String estatus;
	private String lstMoneda;
	private String lstTipoTasa;
	private String lses_ic_if;
	private String lstEPO;
	
	
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
 
	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		qrySentencia.append("select count(1) total "+
												" from  "+
												" Bit_Tasas_Preferenciales b, "+
												" comcat_epo e, "+
												" comcat_pyme p, "+
												" comcat_moneda m "+
												" where  "+
												" b.ic_epo = e.ic_epo "+
												" and b.ic_pyme = p.ic_pyme "+
												" and b.ic_moneda = m.ic_moneda ");

      
			
				if(lses_ic_if !=null ) {
				  qrySentencia.append(" and b.ic_if = ? ");
						conditions.add(lses_ic_if);
			}


			if(lstTipoTasa !=null ) {
				  qrySentencia.append(" and b.CG_TIPOTASA = ? ");
						conditions.add(lstTipoTasa);
			}

			if(lstEPO !=null ) {
				  qrySentencia.append(" and b.ic_epo = ? ");
						conditions.add(lstEPO);
			} 

			if(lstMoneda !=null) {
				  qrySentencia.append(" and b.IC_MONEDA = ? ");
						conditions.add(lstMoneda);
			}
      
			if(!ic_pyme.equals("0") ) {
				  qrySentencia.append(" and b.ic_pyme = ? ");
						conditions.add(ic_pyme);
			}
			
			if(estatus!=null ) {
				  qrySentencia.append(" and b.cg_estatus = '"+estatus+"'");
						//conditions.add(estatus);
			}
			
     if(df_fecha_registro_de !=null && df_fecha_registro_a !=null ) {
				
			   qrySentencia.append("  AND b.DF_FECHA_HORA >= TO_DATE(?,'dd/mm/YYYY')    "+
                    " AND b.DF_FECHA_HORA < TO_DATE(?,'dd/mm/YYYY')+1  ");
						conditions.add(df_fecha_registro_de);
						conditions.add(df_fecha_registro_a);
				}		
			
			if (!"".equals(aplicaFloating)  ) {	
					qrySentencia.append(" and b.CS_TASA_FLOATING = ? ");
					conditions.add(aplicaFloating);
				}	
			
			qrySentencia.append("  ORDER BY  b.DF_FECHA_HORA   ");
				
			log.debug("getAggregateCalculationQuery "+conditions.toString());
			log.debug("getAggregateCalculationQuery "+qrySentencia.toString());
	
		return qrySentencia.toString();
		
	}
		
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		   qrySentencia.append("SELECT b.ic_bitacora ic_bitacora, b.ic_cuenta_bancaria ic_cuenta_bancaria, " +
									"         b.ic_if ic_if, b.ic_epo ic_epo, b.cg_tipotasa cg_tipotasa, " +
									"         b.ic_plazo ic_plazo, b.ic_orden ic_orden, " +
									"         e.cg_razon_social AS nombreepo, p.cg_razon_social AS nombreproveedor, " +
									"         cd_nombre AS moneda, " +
									"         DECODE (b.cg_tipotasa, " +
									"                 'P', 'PREFERENCIAL', " +
									"                 'N', 'NEGOCIADA', " +
									"                 '' " +
									"                ) || '' ||  decode (b.CS_TASA_FLOATING , 'S', '/ FLOATING') AS tipotasa, " +
									"         RTRIM(b.cg_estatus) AS estatus, " +
									"         DECODE (RTRIM(b.cg_estatus), 'E', 'ELIMINAR', 'A', 'ALTA') AS estatustasa, " +
									"         b.fn_puntos AS puntos, b.fn_valor AS valor, " +
									"         TO_CHAR (b.df_fecha_hora, 'DD/MM/YYYY HH24:MI:SS') AS fecharegistro, " +
									"         b.cg_usuario AS usuario " +
									"    FROM bit_tasas_preferenciales b, " +
									"         comcat_epo e, " +
									"         comcat_pyme p, " +
									"         comcat_moneda m, comcat_plazo z " +
									"   WHERE b.ic_epo = e.ic_epo " +
									"     AND b.ic_pyme = p.ic_pyme " +
									"     AND b.ic_moneda = m.ic_moneda AND b.ic_plazo = z.ic_plazo AND z.ic_producto_nafin = 1");		
          
    	if(lses_ic_if!=null && !lses_ic_if.equals("") ) {      
				  qrySentencia.append(" and b.ic_if = ? ");  
						conditions.add(lses_ic_if);  
			}

                 
   		if(lstTipoTasa!=(null) && !lstTipoTasa.equals("") ) {
				  qrySentencia.append(" and b.CG_TIPOTASA = ? ");
						conditions.add(lstTipoTasa);
			}
 
			if(lstEPO!=null && !lstEPO.equals("")) { 
				  qrySentencia.append(" and b.ic_epo = ? ");
						conditions.add(lstEPO);
			}

			if(lstMoneda!=null && !lstMoneda.equals("")) {
				  qrySentencia.append(" and b.IC_MONEDA = ? ");
						conditions.add(lstMoneda);
			}
      
			if(!ic_pyme.equals("0") && !ic_pyme.equals("") ) {
				  qrySentencia.append(" and b.ic_pyme = ? ");
						conditions.add(ic_pyme);
			}
			
			if(estatus!=null && !estatus.equals("") ) {
				 qrySentencia.append(" and b.cg_estatus = '"+estatus+"'");
						//conditions.add(estatus);
			}
			if((df_fecha_registro_de!=null && df_fecha_registro_a!=null) && (!df_fecha_registro_de.equals("") && !df_fecha_registro_a.equals(""))) {
				
			   qrySentencia.append("  AND b.DF_FECHA_HORA >= TO_DATE(?,'dd/mm/YYYY')    "+
                    " AND b.DF_FECHA_HORA < TO_DATE(?,'dd/mm/YYYY')+1  ");
						conditions.add(df_fecha_registro_de);
						conditions.add(df_fecha_registro_a);
				}
				
			if (!"".equals(aplicaFloating)  ) {	
					qrySentencia.append(" and b.CS_TASA_FLOATING = ? ");
					conditions.add(aplicaFloating);
				}	
			qrySentencia.append("UNION ALL " +
						"SELECT   b.ic_bit_oferta ic_bitacora, b.ic_cuenta_bancaria ic_cuenta_bancaria, " +
						"         b.ic_if ic_if, b.ic_epo ic_epo, b.cg_tipotasa cg_tipotasa, " +
						"         NULL ic_plazo, NULL ic_orden, e.cg_razon_social AS nombreepo, " +
						"         p.cg_razon_social AS nombreproveedor, cd_nombre AS moneda, " +
						"         DECODE (b.cg_tipotasa, " +
						"                 'P', 'PREFERENCIAL', " +
						"                 'N', 'NEGOCIADA', " +
						"                 'O', 'OFERTA DE TASA', " +
						"                 '' " +
						"                ) AS tipotasa, " +
						"         RTRIM(b.cg_estatus) AS estatus, " +
						"         DECODE (RTRIM(b.cg_estatus), " +
						"                 'E', 'ELIMINAR', " +
						"                 'A', 'ALTA', " +
						"                 'M', 'MODIFICACION' " +
						"                ) AS estatustasa, " +
						"         b.fn_puntos AS puntos, NULL AS valor, " +
						"         TO_CHAR (b.df_fecha_hora, 'DD/MM/YYYY HH24:MI:SS') AS fecharegistro, " +
						"         b.cg_usuario AS usuario " +
						"    FROM bit_tasas_pref_oferta b, comcat_epo e, comcat_pyme p, " +
						"         comcat_moneda m, com_oferta_tasa_if_epo o " +
						"   WHERE b.ic_epo = e.ic_epo " +
						"     AND b.ic_pyme = p.ic_pyme " +
						"     AND b.ic_moneda = m.ic_moneda AND b.ic_oferta_tasa = o.ic_oferta_tasa ");
						
			if(lses_ic_if!=null && !lses_ic_if.equals("")) {
				qrySentencia.append(" and b.ic_if = ? ");
				conditions.add(lses_ic_if);
			}
        
   		if(lstTipoTasa!=null && !lstTipoTasa.equals("")) {
				qrySentencia.append(" and b.CG_TIPOTASA = ? ");
				conditions.add(lstTipoTasa);
			}

			if(lstEPO!=null && !lstEPO.equals("")) {
				qrySentencia.append(" and b.ic_epo = ? ");
				conditions.add(lstEPO);  
			}

			if(lstMoneda!=null && !lstMoneda.equals("")) {
				qrySentencia.append(" and b.IC_MONEDA = ? ");
				conditions.add(lstMoneda);
			}
      
			if(!ic_pyme.equals("0") && !ic_pyme.equals("") ) { 
				qrySentencia.append(" and b.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}       
			  
			if(estatus!=null && !estatus.equals("")) {
				 qrySentencia.append(" and b.cg_estatus = '"+estatus+"'");
						//conditions.add(estatus);
			}
			
			if((df_fecha_registro_de!=null && df_fecha_registro_a!=null) && (!df_fecha_registro_de.equals("") && !df_fecha_registro_a.equals("")) ) {
				qrySentencia.append("  AND b.DF_FECHA_HORA >= TO_DATE(?,'dd/mm/YYYY')    "+
											 " AND b.DF_FECHA_HORA < TO_DATE(?,'dd/mm/YYYY')+1  ");
				conditions.add(df_fecha_registro_de);   
				conditions.add(df_fecha_registro_a);   
			}  
   
			qrySentencia.append(" ORDER BY ic_bitacora ");

			log.debug("getDocumentQuery:::::: "+conditions.toString());
			log.debug("getDocumentQuery "+qrySentencia.toString());

			return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la información completa de 
	 * una página a partir de las llaves primarias enviadas como parámetro
	 * @return Cadena con la consulta de SQL, para obtener la información
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
		
  qrySentencia.append("SELECT   b.ic_bitacora ic_bitacora, b.ic_cuenta_bancaria ic_cuenta_bancaria, " +
							"         b.ic_if ic_if, b.ic_epo ic_epo, b.cg_tipotasa cg_tipotasa, " +
							"         b.ic_plazo ic_plazo, b.ic_orden ic_orden, " +
							"         e.cg_razon_social AS nombreepo, p.cg_razon_social AS nombreproveedor, " +
							"         cd_nombre AS moneda, z.in_plazo_dias plazo_dias, null monto_desde,  " +
							"         null monto_hasta, null datos_anteriores, null datos_actuales, " +
							"         DECODE (b.cg_tipotasa, " +
							"                 'P', 'PREFERENCIAL', " +
							"                 'N', 'NEGOCIADA', " +
							"                 '' " +
							"                ) || '' ||  decode (b.CS_TASA_FLOATING , 'S', '/ FLOATING') AS tipotasa, " +
							"         RTRIM(b.cg_estatus) AS estatus, " +
							"         DECODE (RTRIM(b.cg_estatus), 'E', 'ELIMINAR', 'A', 'ALTA') AS estatustasa, " +
							"         b.fn_puntos AS puntos, b.fn_valor AS valor, " +
							"         TO_CHAR (b.df_fecha_hora, 'DD/MM/YYYY HH24:MI:SS') AS fecharegistro, " +
							"         b.cg_usuario AS usuario " +
							"    FROM bit_tasas_preferenciales b, " +
							"         comcat_epo e, " +
							"         comcat_pyme p, " +
							"         comcat_moneda m, " +
							"         comcat_plazo z " +
							"   WHERE b.ic_epo = e.ic_epo " +
							"     AND b.ic_pyme = p.ic_pyme " +
							"     AND b.ic_moneda = m.ic_moneda " +
							"     AND b.ic_plazo = z.ic_plazo " +
							"     AND z.ic_producto_nafin = 1 ");
												     
    
	/*	if(lses_ic_if !=null ) {
				  qrySentencia.append(" and b.ic_if = ? ");
						conditions.add(lses_ic_if);
			}
			
			if(lstTipoTasa !=null ) {
				  qrySentencia.append(" and b.CG_TIPOTASA = ? ");
						conditions.add(lstTipoTasa);
			}

			if(lstEPO !=null ) {
				  qrySentencia.append(" and b.ic_epo = ? ");
						conditions.add(lstEPO);
			}

			if(lstMoneda !=null) {
				  qrySentencia.append(" and b.IC_MONEDA = ? ");
						conditions.add(lstMoneda);
			}
      
			if(!ic_pyme.equals("0") ) {
				  qrySentencia.append(" and b.ic_pyme = ? ");
						conditions.add(ic_pyme);
			}
			
			if(estatus!=null ) {
				 qrySentencia.append(" and b.cg_estatus = '"+estatus+"'");
					//	conditions.add(estatus);
			}
			
      if(df_fecha_registro_de !=null && df_fecha_registro_a !=null ) {
				
			   qrySentencia.append("  AND b.DF_FECHA_HORA >= TO_DATE(?,'dd/mm/YYYY')    "+
                    " AND b.DF_FECHA_HORA < TO_DATE(?,'dd/mm/YYYY')+1  ");
						conditions.add(df_fecha_registro_de);
						conditions.add(df_fecha_registro_a);
				}	 
				*/
			
		if(lstMoneda !=null && !lstMoneda.equals("")) {
			qrySentencia.append(" and b.IC_MONEDA = ? ");
			conditions.add(lstMoneda);
		}	
		
		qrySentencia.append("   AND ( ");
      
      for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0) {
				qrySentencia.append(" OR ");
			}
			qrySentencia.append(" (b.ic_Bitacora = " +lItem.get(0).toString());
			qrySentencia.append(" AND b.IC_CUENTA_BANCARIA = "+lItem.get(1).toString());
			qrySentencia.append(" AND b.IC_IF = "+lItem.get(2).toString());
			qrySentencia.append(" AND b.IC_EPO =  "+lItem.get(3).toString());
			qrySentencia.append(" AND b.cg_tipoTasa = '"+lItem.get(4).toString()+"'");
			qrySentencia.append(" AND b.IC_PLAZO =  "+lItem.get(5).toString());
			qrySentencia.append(" AND b.IC_ORDEN = ? )");
			conditions.add(new Long(lItem.get(6).toString()));

		}//for(int i=0;i<ids.size();i++)
 		qrySentencia.append(" ) ");
		
		qrySentencia.append("UNION ALL " +
								"SELECT   b.ic_bit_oferta ic_bitacora, b.ic_cuenta_bancaria ic_cuenta_bancaria, " +
								"         b.ic_if ic_if, b.ic_epo ic_epo, b.cg_tipotasa cg_tipotasa, " +
								"         NULL ic_plazo, NULL ic_orden, e.cg_razon_social AS nombreepo, " +
								"         p.cg_razon_social AS nombreproveedor, cd_nombre AS moneda, " +
								"         null plazo_dias, o.fg_montodesde monto_desde, o.fg_montohasta monto_hasta,  " +
								"         b.cg_anterior datos_anteriores, b.cg_actual datos_actuales, " +
								"         DECODE (b.cg_tipotasa, " +
								"                 'P', 'PREFERENCIAL', " +
								"                 'N', 'NEGOCIADA', " +
								"                 'O', 'OFERTA DE TASA', " +
								"                 '' " +
								"                )  AS tipotasa, " +
								"         RTRIM(b.cg_estatus) AS estatus, " +
								"         DECODE (RTRIM(b.cg_estatus), " +
								"                 'E', 'ELIMINAR', " +
								"                 'A', 'ALTA', " +
								"                 'M', 'MODIFICACION' " +
								"                ) AS estatustasa, " +
								"         b.fn_puntos AS puntos, NULL AS valor, " +
								"         TO_CHAR (b.df_fecha_hora, 'DD/MM/YYYY HH24:MI:SS') AS fecharegistro, " +
								"         b.cg_usuario AS usuario " +
								"    FROM bit_tasas_pref_oferta b,  " +
								"    	 comcat_epo e, comcat_pyme p, " +
								"         comcat_moneda m, " +
								"         com_oferta_tasa_if_epo o " +
								"   WHERE b.ic_epo = e.ic_epo " +
								"     AND b.ic_pyme = p.ic_pyme " +
								"     AND b.ic_moneda = m.ic_moneda " +
								"     AND b.ic_oferta_tasa = o.ic_oferta_tasa ");
		
		if(lstMoneda !=null && !lstMoneda.equals("")) {
			qrySentencia.append(" and b.IC_MONEDA = ? ");
			conditions.add(lstMoneda);
		}	  
		
		qrySentencia.append("   AND ( ");   
      
      for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0) {
				qrySentencia.append(" OR ");
			}
			qrySentencia.append(" (b.ic_bit_oferta = " +lItem.get(0).toString());
			qrySentencia.append(" AND b.IC_CUENTA_BANCARIA = "+lItem.get(1).toString());
			qrySentencia.append(" AND b.IC_IF = "+lItem.get(2).toString());
			qrySentencia.append(" AND b.IC_EPO =  "+lItem.get(3).toString());
			qrySentencia.append(" AND b.cg_tipoTasa = ? ) ");
			conditions.add(lItem.get(4).toString());

		}//for(int i=0;i<ids.size();i++)
		
		qrySentencia.append(" ) ");
		
   	qrySentencia.append("  ORDER BY  fecharegistro   ");
		
		log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
		log.debug(" getDocumentSummaryQueryForIds "+qrySentencia.toString());
	
		
		return qrySentencia.toString();
	}
	

	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
  
		log.info("lstTipoTasa: "+lstTipoTasa);
		log.info("lstEPO: "+lstEPO);	
		log.info("lstMoneda: "+lstMoneda);	
		log.info("ic_pyme: "+ic_pyme);	
		log.info("estatus: "+estatus);	
		log.info("df_fecha_registro_de: "+df_fecha_registro_de);	
		log.info("df_fecha_registro_a: "+df_fecha_registro_a);
		log.info("lses_ic_if: "+lses_ic_if);
		  
		
			qrySentencia.append("select b.ic_Bitacora, 	b.IC_CUENTA_BANCARIA, b.IC_IF, b.IC_EPO, b.cg_tipoTasa, b.IC_PLAZO, b.IC_ORDEN , b.ic_tasa, e.CG_RAZON_SOCIAL AS nombreEpo , p.CG_RAZON_SOCIAL AS nombreProveedor ,"+
												" CD_NOMBRE AS moneda, "+
												" DECODE(b.CG_TIPOTASA,'P','PREFERENCIAL','N','NEGOCIADA','') || '' ||  decode (b.CS_TASA_FLOATING , 'S', '/ FLOATING') as tipoTasa,"+
												"  RTRIM(b.CG_ESTATUS) as estatus, "+
												"  DECODE(RTRIM(b.CG_ESTATUS),'E ','ELIMINAR','A ','ALTA') as estatusTasa,"+
												" b.FN_PUNTOS as puntos, "+
												" b.FN_VALOR	 as valor, "+
												" To_char(b.df_fecha_hora, 'DD/MM/YYYY HH24:MI:SS') as fechaRegistro,"+
												" b.CG_USUARIO as Usuario "+
												" from  "+
												" Bit_Tasas_Preferenciales b, "+
												" comcat_epo e, "+
												" comcat_pyme p, "+
												" comcat_moneda m "+
												" where  "+
												" b.ic_epo = e.ic_epo "+
												" and b.ic_pyme = p.ic_pyme "+
												" and b.ic_moneda = m.ic_moneda ");
												
    	if(lses_ic_if !=null && !lses_ic_if.equals("")) {
				  qrySentencia.append(" and b.ic_if = ? ");
						conditions.add(lses_ic_if);
			}
			
		if(lstTipoTasa !=null && !lstTipoTasa.equals("")) {
				  qrySentencia.append(" and b.CG_TIPOTASA = ? ");
						conditions.add(lstTipoTasa);
			}
 
			if(lstEPO !=null && !lstEPO.equals("")) {
				  qrySentencia.append(" and b.ic_epo = ? ");
						conditions.add(lstEPO);
			}
 
			if(lstMoneda !=null && !lstMoneda.equals("")) {
				  qrySentencia.append(" and b.IC_MONEDA = ? ");
						conditions.add(lstMoneda);
			}
      
			if(!ic_pyme.equals("0") && !ic_pyme.equals("")) {
				  qrySentencia.append(" and b.ic_pyme = ? ");
						conditions.add(ic_pyme);
			}
			
			if(estatus!=null && !estatus.equals("")) {
				qrySentencia.append(" and b.cg_estatus = '"+estatus+"'");
					//	conditions.add(estatus);
			}
			
     if((df_fecha_registro_de !=null && df_fecha_registro_a !=null) && (!df_fecha_registro_de.equals("") && !df_fecha_registro_a.equals("")) ) {
				
			   qrySentencia.append(" AND b.DF_FECHA_HORA >= TO_DATE(?,'dd/mm/YYYY')    "+
                    " AND b.DF_FECHA_HORA < TO_DATE(?,'dd/mm/YYYY')+1  ");
						conditions.add(df_fecha_registro_de);
						conditions.add(df_fecha_registro_a);
				}	
		
		
				if (!"".equals(aplicaFloating)  ) {	
					qrySentencia.append(" and b.CS_TASA_FLOATING = ? ");
					conditions.add(aplicaFloating);
				}		
			
			
			qrySentencia.append("  ORDER BY  b.DF_FECHA_HORA   ");
			
			log.debug("getDocumentQueryFile "+conditions.toString());
			log.debug("getDocumentQueryFile "+qrySentencia.toString());
		
		return qrySentencia.toString();
	}//getDocumentQueryFile
	
	/****************************************************************************
	*										MIGRACION 
	 ***************************************************************************/ 
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, Registros rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
			
		if ("XLS".equals(tipo)) {
			try {
				int registros = 0;
				
				while(rs.next()){
					if(registros==0) {
						contenidoArchivo.append("EPO,Proveedor,Moneda,Tipo Tasa,Estatus,Puntos,Valor,Plazo,Desde,Hasta,Fecha Registro,Usuario,Datos Anteriores,Datos Actuales");
					}
					registros++;		
					String ic_bitacora 			= rs.getString("ic_bitacora")==null?"":rs.getString("ic_bitacora");
					String ic_cuenta_bancaria	= rs.getString("ic_cuenta_bancaria")==null?"":rs.getString("ic_cuenta_bancaria");
					String ic_if 					= rs.getString("ic_if")==null?"":rs.getString("ic_if");
					String ic_epo					= rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
					String cg_tipotasa 			= rs.getString("cg_tipotasa")==null?"":rs.getString("cg_tipotasa");
					String ic_plazo				= rs.getString("ic_plazo")==null?"":rs.getString("ic_plazo"); 
					String ic_orden 				= rs.getString("ic_orden")==null?"":rs.getString("ic_orden");
					String nombreepo				= rs.getString("nombreepo")==null?"":rs.getString("nombreepo");
					String nombreproveedor		= rs.getString("nombreproveedor")==null?"":rs.getString("nombreproveedor");	
					String moneda					= rs.getString("moneda")==null?"":rs.getString("moneda");	
					String plazo_dias				= rs.getString("plazo_dias")==null?"":rs.getString("plazo_dias");	
					String monto_desde			= rs.getString("monto_desde")==null?"":Comunes.formatoDecimal(rs.getString("monto_desde"),2); 
					String monto_hasta			= rs.getString("monto_hasta")==null?"":Comunes.formatoDecimal(rs.getString("monto_hasta"),2); 
					String datos_anteriores		= rs.getString("datos_anteriores")==null?"":rs.getString("datos_anteriores");	
					String datos_actuales		= rs.getString("datos_actuales")==null?"":rs.getString("datos_actuales");	
					String tipotasa				= rs.getString("tipotasa")==null?"":rs.getString("tipotasa");	
					String puntos					= rs.getString("puntos")==null?"":rs.getString("puntos");	
					String valor					= rs.getString("valor")==null?"":rs.getString("valor");	
					String fechaRegistro			= rs.getString("fechaRegistro")==null?"":rs.getString("fechaRegistro");	
					String Usuario					= rs.getString("Usuario")==null?"":rs.getString("Usuario");
					String estatus2				= (rs.getString("estatus").trim() == null) ? "" : rs.getString("estatus").trim();		
					
					String estatusTasa = "";
					
					if("O".equals(rs.getString("cg_tipotasa"))){
						valor= "N/A";
						plazo_dias= "N/A";
					}else{
						monto_desde= "N/A";
						monto_hasta= "N/A";
						datos_anteriores= "N/A";
						datos_actuales= "N/A";
					}
				
					if(estatus2.equals("A")) {
						estatusTasa = "Alta";			 
					 }else if(estatus2.equals("E")) {
						estatusTasa = "Eliminar";
					 }else if(estatus2.equals("M")) {
						estatusTasa = "Modificacion";
					 }
																
					contenidoArchivo.append("\n"+nombreepo.replace(',',' ')+","+
						nombreproveedor.replace(',',' ')+","+
						moneda.replace(',',' ')+","+tipotasa.replace(',',' ')+","+
						estatusTasa.replace(',',' ')+","+
						puntos.replace(',',' ')+","+
						valor.replace(',',' ')+","+
						plazo_dias.replace(',',' ')+","+
						monto_desde.replace(',',' ')+","+
						monto_hasta.replace(',',' ')+","+
						fechaRegistro.replace(',',' ')+","+
						Usuario.replace(',',' ')+","+
						datos_anteriores.replace(',',' ')+","+
						datos_actuales.replace(',',' '));
				}
				
				if(registros >= 1){
					if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
						nombreArchivo="ERROR";
					}else{
						nombreArchivo = archivo.nombre;
					}
				}	
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
		}
		return nombreArchivo;	
	}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";  
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
			
		if ("XLS".equals(tipo)) {
			try {
				int registros = 0;
				
				while(rs.next()){
					if(registros==0) {
						contenidoArchivo.append("Intermediario Financiero,Fecha de Afiliacion del IF,Fecha de Aceptacion IF de Cuenta Bancaria / Usuario,Numero Electronico,Clave SIRAC,RFC,Nombre PyME,Fecha de Aceptacion del Convenio Unico Electronico,Fecha de Firma Autografa de CU");
					}
					registros++;		
					String 	operacion   	= ( request.getParameter("operacion") == null ) ? "1" : request.getParameter("operacion");
					String snom_if 			= rs.getString("intermediario_financiero")==null?"":rs.getString("intermediario_financiero");
					String sfecha_afil_if	= rs.getString("fecha_de_afiliacion_del_if")==null?"":rs.getString("fecha_de_afiliacion_del_if");
					String sfecha_aut_if 	= rs.getString("fecha_acept_if_cta_banc_usr")==null?"":rs.getString("fecha_acept_if_cta_banc_usr");
					String snumero_nafele	= rs.getString("numero_electronico")==null?"":rs.getString("numero_electronico");
					String snumero_sirac 	= rs.getString("clave_sirac")==null?"":rs.getString("clave_sirac");
					String srfc					= rs.getString("rfc")==null?"":rs.getString("rfc"); 
					String snombre_pyme 		= rs.getString("nombre_pyme")==null?"":rs.getString("nombre_pyme");
					String sfecha_acep_pyme	= rs.getString("fecha_acept_cu_electronico")==null?"":rs.getString("fecha_acept_cu_electronico");
					String sfecha_firma_pyme= rs.getString("fecha_firma_autografa_cu")==null?"":rs.getString("fecha_firma_autografa_cu");	
											
					contenidoArchivo.append("\n"+snom_if.replace(',',' ')+","+sfecha_afil_if.replace(',',' ')+","+sfecha_aut_if.replace(',',' ')+","+snumero_nafele.replace(',',' ')+
													","+snumero_sirac.replace(',',' ')+","+srfc.replace(',',' ')+","+snombre_pyme.replace(',',' ')+","+sfecha_acep_pyme.replace(',',' ')+
													","+sfecha_firma_pyme.replace(',',' '));
				}
				
				if(registros >= 1){
					if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
						nombreArchivo="ERROR";
					}else{
						nombreArchivo = archivo.nombre;
					}
				}	
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}
		return nombreArchivo;	
	}	
/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.  
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
 	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	
	public String getDirectorio() {
		return directorio;
	}

	public void setDirectorio(String directorio) {
		this.directorio = directorio;
	}

 
  public String getIc_epo() {
    return ic_epo;
  }

  public void setIc_epo(String ic_epo) {
    this.ic_epo = ic_epo;
  }

  public String getIc_pyme() {
    return ic_pyme;
  }

  public void setIc_pyme(String ic_pyme) {
    this.ic_pyme = ic_pyme;
  }

  public String getPaginar() {
    return paginar;
  }

  public void setPaginar(String paginar) {
    this.paginar = paginar;
  }

  public String getDf_fecha_registro_de() {
    return df_fecha_registro_de;
  }

  public void setDf_fecha_registro_de(String df_fecha_registro_de) {
    this.df_fecha_registro_de = df_fecha_registro_de;
  }

  public String getDf_fecha_registro_a() {
    return df_fecha_registro_a;
  }

  public void setDf_fecha_registro_a(String df_fecha_registro_a) {
    this.df_fecha_registro_a = df_fecha_registro_a;
  }

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getLstMoneda() {
		return lstMoneda;
	}

	public void setLstMoneda(String lstMoneda) {
		this.lstMoneda = lstMoneda;
	}

	public String getLstTipoTasa() { 
		return lstTipoTasa;
	}

	public void setLstTipoTasa(String lstTipoTasa) {
		this.lstTipoTasa = lstTipoTasa;
	}

	public String getLses_ic_if() {
		return lses_ic_if;
	}

	public void setLses_ic_if(String lses_ic_if) {
		this.lses_ic_if = lses_ic_if;
	}

	public String getLstEPO() {
		return lstEPO;
	}

	public void setLstEPO(String lstEPO) {
		this.lstEPO = lstEPO;
	}

	public void setAplicaFloating(String aplicaFloating) {
		this.aplicaFloating = aplicaFloating;
	}

	public String getAplicaFloating() {
		return aplicaFloating;
	}

}
