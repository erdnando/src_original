package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsAfianzadora implements  IQueryGeneratorRegExtJS{
	private final static Log log = ServiceLocator.getInstance().getLog(ConsAfianzadora.class);
	private String noNE;
	private String nombre;
	private String estado;
	private String paginaOffset;
	private String paginaNo;
	StringBuffer 	strSQL;
	private List 	conditions;
		/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)  {
		String nombreArchivo = "";
		if("CSV".equals(tipo)){
			String linea = "";
			OutputStreamWriter writer = null;
			BufferedWriter buffer = null;
			
			StringBuffer 	contenidoArchivo 	= new StringBuffer();
			CreaArchivo 	archivo 			= new CreaArchivo();
			try {
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
					writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
					buffer = new BufferedWriter(writer);
					buffer.write(linea);
					
					//Encabezado
					linea="No. Afianzadora CNSF,N�mero de Nafin Electr�nico,Nombre o Raz�n Social,Domicilio,Estado,Tel�fono,Fecha de Alta Afianzadora\n";
					buffer.write(linea);
					
					String afianzadora,nafElec,razonSocial,domicilio,estado,telefono,fechaAlta;
					while (rs.next()) {
						
						 afianzadora= (rs.getString("cnsf") == null) ? "" : rs.getString("cnsf");
						 nafElec=(rs.getString("noNafin") == null) ? "" : rs.getString("noNafin");
						 razonSocial=(rs.getString("razonSocial") == null) ? "" : rs.getString("razonSocial");
						 domicilio=(rs.getString("domicilio") == null) ? "" : rs.getString("domicilio");
						 estado=(rs.getString("estado") == null) ? "" : rs.getString("estado");
						 telefono=(rs.getString("telefono") == null) ? "" : rs.getString("telefono");
						 fechaAlta=(rs.getString("fechaAlta") == null) ? "" : rs.getString("fechaAlta");
						 
	
						linea =afianzadora+","+nafElec.replace(',',' ')+","+razonSocial.replace(',',' ')+","+domicilio.replace(',',' ')+","+estado.replace(',',' ')+","
						+telefono.replace(',',' ')+","+fechaAlta.replace(',',' ')+"\n";
						
						buffer.write(linea);
					}
					buffer.close();
				} catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
					} finally {
						try {
							rs.close();
						} catch(Exception e) {}
					}
		}else	if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				"",
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				//pdfDoc.setTable(16, 100);
				List lEncabezados = new ArrayList();
				
			
				
				lEncabezados.add("No. Afianzadora CNSF");
				lEncabezados.add("N�mero de Nafin Electr�nico");
				lEncabezados.add("Nombre o Raz�n Social");
				lEncabezados.add("Domicilio");
				lEncabezados.add("Estado");
				lEncabezados.add("Tel�fono");
				lEncabezados.add("Fecha de Alta Afianzadora");
				pdfDoc.setTable(lEncabezados, "formasmenB", 100);
				
				String afianzadora,nafElec,razonSocial,domicilio,estado,telefono,fechaAlta;
				while (rs.next()){
					afianzadora= (rs.getString("cnsf") == null) ? "" : rs.getString("cnsf");
					 nafElec=(rs.getString("noNafin") == null) ? "" : rs.getString("noNafin");
					 razonSocial=(rs.getString("razonSocial") == null) ? "" : rs.getString("razonSocial");
					 domicilio=(rs.getString("domicilio") == null) ? "" : rs.getString("domicilio");
					 estado=(rs.getString("estado") == null) ? "" : rs.getString("estado");
					 telefono=(rs.getString("telefono") == null) ? "" : rs.getString("telefono");
					 fechaAlta=(rs.getString("fechaAlta") == null) ? "" : rs.getString("fechaAlta");
					
					
					pdfDoc.setCell(afianzadora,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nafElec,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(razonSocial,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(domicilio,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(estado,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaAlta,"formas",ComunesPDF.CENTER);
					
				}
				//pdfDoc.addTable();
				
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}
		}	
	
		return nombreArchivo;
	}
	
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
			
			return  nombreArchivo;
	}
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery(){
    log.info("getAggregateCalculationQuery(I) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		log.info("getAggregateCalculationQuery(F)::..");
		return strSQL.toString();
	}//getAggregateCalculationQuery
	
    /**
    * Obtiene el query para obtener las llaves primarias de la consulta
    * @return Cadena con la consulta de SQL, para obtener llaves primarias
    */
    public String getDocumentQuery(){
      return "";
    }//getDocumentQuery
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
    return "";	
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
    log.info("getDocumentQueryFile(I) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

    strSQL.append( "  select  "+
		" a.IC_AFIANZADORA   as  noAfianza, "+
		" n.ic_nafin_electronico  as noNafin, "+
		" a.CG_RAZON_SOCIAL  as razonSocial, "+
		" d.cg_calle ||'  ' || d.cg_colonia ||'  ' || d.cg_municipio   as domicilio, "+
		" e.cd_nombre  as estado,"+
		" d.cg_telefono1 as telefono ,"+
		" to_char(a.DF_ALTA, 'dd/mm/yyyy hh24:mi:ss') as fechaAlta, "+
		" a.cg_numero_cnsf as cnsf "+
		" from fe_afianzadora a, "+
		" com_domicilio d,  "+
		" COMCAT_ESTADO e,  "+
		" COMCAT_PAIS p ,"+
		" comrel_nafin n "+
		" where d.IC_AFIANZADORA = a.IC_AFIANZADORA "+
		" and d.IC_ESTADO = e.IC_ESTADO "+
		" AND d.IC_PAIS = p.IC_PAIS "+
		" and n.cg_tipo = 'A' "+
		" and n.ic_epo_pyme_if = a.IC_AFIANZADORA "); 
    
		if (!noNE.equals("")) {
			  strSQL.append("and n.ic_nafin_electronico = ? ");
			  conditions.add(noNE);
			}
		if (!nombre.equals("")) {
				strSQL.append("and a.CG_RAZON_SOCIAL LIKE '"+nombre+"%'");
			}
		if (!estado.equals("")) {
		  strSQL.append("and d.IC_ESTADO = ? ");
		  conditions.add(estado);
		}
    
		log.debug("strSQL:: " + strSQL.toString());
		log.debug("conditions:: " + conditions);

		log.info("getDocumentQueryFile(F) ::..");
		return strSQL.toString();
	}//getDocumentQueryFile

/**
	 * Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions() {
		return conditions;
	}






	
	public ConsAfianzadora() {
	}


	public void setNoNE(String noNE) {
		this.noNE = noNE;
	}


	public String getNoNE() {
		return noNE;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getNombre() {
		return nombre;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getEstado() {
		return estado;
	}


	public void setPaginaOffset(String paginaOffset) {
		this.paginaOffset = paginaOffset;
	}


	public String getPaginaOffset() {
		return paginaOffset;
	}


	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}


	public String getPaginaNo() {
		return paginaNo;
	}
}