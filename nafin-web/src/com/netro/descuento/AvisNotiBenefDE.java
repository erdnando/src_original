package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class AvisNotiBenefDE implements IQueryGenerator,  IQueryGeneratorRegExtJS { 

	public AvisNotiBenefDE(){ }

		public String getAggregateCalculationQuery(HttpServletRequest request){
			String envia = (request.getParameter("envia") == null) ? "" : request.getParameter("envia");
			String icEpo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
			String dfFechaNotMin = (request.getParameter("df_fecha_notMin") == null) ? "" : request.getParameter("df_fecha_notMin");
			String dfFechaNotMax = (request.getParameter("df_fecha_notMax") == null) ? "" : request.getParameter("df_fecha_notMax");
			String ccAcuse3 = (request.getParameter("cc_acuse3") == null) ? "" : request.getParameter("cc_acuse3");
			String fechaActual = (request.getParameter("FechaActual") == null) ? "" : request.getParameter("FechaActual");
			String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");

			String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");

			SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
			fechaActual = fecha2.format(new java.util.Date());
			String SQLHint = "";

			String condicion = "";
			if(!dfFechaNotMin.equals(""))
			{
				if(!dfFechaNotMax.equals("")) {
					SQLHint = "/*+ leading(s) use_nl(s ds d a3 m)*/";
					condicion += " and s.df_fecha_solicitud >= TO_DATE('" + dfFechaNotMin + "','dd/mm/yyyy') " +
								 " and s.df_fecha_solicitud < TO_DATE('" + dfFechaNotMax + "','dd/mm/yyyy')+1 ";
				}
			}
			if (!icEpo.equals("")) {
				condicion += " AND d.ic_epo = "+ icEpo;
				SQLHint = (SQLHint.equals(""))?"/*+ use_nl(d s a3)*/":SQLHint;
			}
			if (!ccAcuse3.equals("")) {
				condicion += " AND a3.cc_acuse = '" + ccAcuse3 + "' ";
				SQLHint = "/*+ index(a3 cp_com_acuse3_pk) use_nl(a3 s d)*/";
			}
			if( icEpo.equals("") && dfFechaNotMin.equals("") && dfFechaNotMax.equals("") && ccAcuse3.equals("") && tipoFactoraje.equals("") ) {
				condicion += " AND s.df_fecha_solicitud >= to_date('" + fechaActual + "', 'dd/mm/yyyy')" +
							 " AND s.df_fecha_solicitud < to_date('" + fechaActual + "', 'dd/mm/yyyy') + 1";
				SQLHint = "/*+ leading(s) use_nl(s ds d a3 m)*/";
			}

			String query_Reporte =

				"SELECT " + SQLHint +
					"	count(1) as RESULT_SIZE, " +
					"	sum(d.fn_monto) as FN_MONTO, " +
					"	sum(ds.fn_importe_recibir_benef) as FN_IMPORTE_RECIBIR_BENEF, " +
					"	m.cd_nombre, " +
					"	d.ic_moneda, " +
					"	'AvisNotiBenefDE::getAggregateCalculationQuery' AS PANTALLA " +
					"FROM 	com_solicitud s, " +
					"		com_docto_seleccionado ds, " +
					"       com_documento d, " +
					"       com_acuse3 a3, " +
					"       comcat_moneda m " +
					"WHERE s.ic_documento = ds.ic_documento " +
					"	   AND ds.ic_documento = d.ic_documento " +
					"	   AND s.cc_acuse = a3.cc_acuse " +
					"      AND d.ic_moneda = m.ic_moneda " +
					condicion +
					"  	   AND d.ic_beneficiario = " +  iNoCliente ;
			query_Reporte +=	" GROUP BY D.IC_MONEDA, M.CD_NOMBRE "+
							    " ORDER BY D.IC_MONEDA ";
			System.out.println("QUERY::getAggregateCalculationQuery: "+query_Reporte);
			return query_Reporte;
		}

		public String getDocumentSummaryQueryForIds(HttpServletRequest request, Collection ids){

			String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");

			StringBuffer query = new StringBuffer();
				query.append(
					"SELECT /*+ index(d cp_com_documento_pk)*/ " +
						" 	(DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social), " +
						"	TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') fecha_not, " +
						"	a3.cc_acuse, (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social), " +
						"	d.ig_numero_docto, " +
						"   TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') fecha_emision, " +
						"	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') fecha_vencimiento, " +
						"	m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, " +
						"	d.fn_monto_dscto, d.cs_dscto_especial, " +
						"	i.cg_razon_social, " +
						"	I2.cg_razon_social as beneficiario, " +
						"	d.fn_porc_beneficiario as por_beneficiario, " +
						"	ds.fn_importe_recibir_benef as importe_a_recibir, " +
						"	'AvisNotiBenefDE::getAggregateCalculationQuery' AS PANTALLA " +
						"FROM 	com_documento d, " +
						"		com_docto_seleccionado ds, " +
						"       comcat_epo e, " +
						"       com_acuse3 a3, " +
						"       comcat_pyme p, " +
						"       comcat_moneda m, " +
						"       comcat_if i, " +
						"       com_solicitud s, " +
						"		comcat_if I2 " +
						"WHERE d.ic_documento = ds.ic_documento " +
						"	   AND ds.ic_documento = s.ic_documento " +
						"	   AND s.cc_acuse = a3.cc_acuse " +
						"	   AND d.ic_beneficiario = i.ic_if " +
						"  	   AND i.ic_if = " +  iNoCliente +
						"	   AND d.ic_epo = e.ic_epo " +
						" 	   AND d.ic_pyme = p.ic_pyme "+
						"	   AND d.ic_beneficiario=I2.ic_if " +
						"      AND d.ic_moneda = m.ic_moneda " +
					" 	   AND D.ic_documento in (");

				for (Iterator it = ids.iterator(); it.hasNext();){
					query.append(it.next().toString() + ",");
				}
				query = query.delete(query.length()-1, query.length());
				query.append(")");

			System.out.println("QUERY::getDocumentSummaryQueryForIds: "+query.toString());
			return query.toString();
		}

		public String getDocumentQuery(HttpServletRequest request){
			String envia = (request.getParameter("envia") == null) ? "S" : request.getParameter("envia");
			String icEpo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
			String dfFechaNotMin = (request.getParameter("df_fecha_notMin") == null) ? "" : request.getParameter("df_fecha_notMin");
			String dfFechaNotMax = (request.getParameter("df_fecha_notMax") == null) ? "" : request.getParameter("df_fecha_notMax");
			String ccAcuse3 = (request.getParameter("cc_acuse3") == null) ? "" : request.getParameter("cc_acuse3");
			String fechaActual = (request.getParameter("FechaActual") == null) ? "" : request.getParameter("FechaActual");

			String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");

			SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
			fechaActual = fecha2.format(new java.util.Date());
			String SQLHint = "";

			String condicion = "";
			if(!dfFechaNotMin.equals(""))
			{
				if(!dfFechaNotMax.equals("")) {
					condicion += " and s.df_fecha_solicitud >= TO_DATE('" + dfFechaNotMin + "','dd/mm/yyyy') " +
					" and s.df_fecha_solicitud < TO_DATE('" + dfFechaNotMax + "','dd/mm/yyyy')+1 ";
					SQLHint = "/*+ leading(s) use_nl(s d a3)*/";
				}
			}
			if (!icEpo.equals("")) {
				condicion += " AND D.ic_epo = "+ icEpo;
				SQLHint = (SQLHint.equals(""))?"/*+ use_nl(s d a3)*/":SQLHint;
			}
			if (!ccAcuse3.equals("")) {
				SQLHint = "/*+ index(a3 cp_com_acuse3_pk) use_nl(a3, s, d)*/";
				condicion += " AND A3.cc_acuse = '" + ccAcuse3 + "' ";
			}
			if( icEpo.equals("") && dfFechaNotMin.equals("") && dfFechaNotMax.equals("") && ccAcuse3.equals("") ) {
				condicion += " AND s.df_fecha_solicitud >= to_date('" + fechaActual + "', 'dd/mm/yyyy')" +
				" AND s.df_fecha_solicitud < to_date('" + fechaActual + "', 'dd/mm/yyyy') + 1";
				SQLHint = "/*+ leading(s) use_nl(s d a3)*/";
			}

			String query_Reporte =
					"SELECT  " + SQLHint +
						"	d.ic_documento, " +
						"	'AvisNotiBenefDE::getDocumentQuery' AS PANTALLA " +
						"FROM 	com_solicitud s, " +
						"		com_documento d, " +
						"       com_acuse3 a3 " +
						"WHERE s.ic_documento = d.ic_documento " +
						"	   AND s.cc_acuse = a3.cc_acuse " +
						condicion +
						"  	   AND d.ic_beneficiario = " +  iNoCliente ;
			System.out.println("QUERY::getDocumentQuery: "+query_Reporte);
			return query_Reporte;
		}

		public String getDocumentQueryFile(HttpServletRequest request){
			String envia = (request.getParameter("envia") == null) ? "S" : request.getParameter("envia");
			String icEpo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
			String dfFechaNotMin = (request.getParameter("df_fecha_notMin") == null) ? "" : request.getParameter("df_fecha_notMin");
			String dfFechaNotMax = (request.getParameter("df_fecha_notMax") == null) ? "" : request.getParameter("df_fecha_notMax");
			String ccAcuse3 = (request.getParameter("cc_acuse3") == null) ? "" : request.getParameter("cc_acuse3");
			String fechaActual = (request.getParameter("FechaActual") == null) ? "" : request.getParameter("FechaActual");

			String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");

			SimpleDateFormat fecha2 = new SimpleDateFormat ("dd/MM/yyyy");
			fechaActual = fecha2.format(new java.util.Date());
			String SQLHint = "";

			String condicion = "";
			if(!dfFechaNotMin.equals(""))
			{
				if(!dfFechaNotMax.equals("")) {
					condicion += " and s.df_fecha_solicitud >= TO_DATE('" + dfFechaNotMin + "','dd/mm/yyyy') " +
								 " and s.df_fecha_solicitud < TO_DATE('" + dfFechaNotMax + "','dd/mm/yyyy')+1 ";
					SQLHint = "/*+ leading(s) use_nl(s ds d a3)*/";
				}

			}
			if (!icEpo.equals("")) {
				condicion += " AND d.ic_epo = "+ icEpo;
				SQLHint = (SQLHint.equals(""))?"/*+ use_nl(s d a3)*/":SQLHint;
			}
			if (!ccAcuse3.equals("")) {
				SQLHint = "/*+ index(a3 cp_com_acuse3_pk) use_nl(s, ds, d, a3)*/";
				condicion += " AND a3.cc_acuse = '" + ccAcuse3 + "' ";
			}
			if( icEpo.equals("") && dfFechaNotMin.equals("") && dfFechaNotMax.equals("") && ccAcuse3.equals("") ) {
				condicion += " AND s.df_fecha_solicitud >= to_date('" + fechaActual + "', 'dd/mm/yyyy')" +
							 " AND s.df_fecha_solicitud < to_date('" + fechaActual + "', 'dd/mm/yyyy') + 1";
				SQLHint = "/*+ leading(s) use_nl(s ds d a3)*/";
			}


			String query_Reporte =
				"SELECT " + SQLHint +
					"	(DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social), " +
					"	TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') fecha_not, " +
					"	a3.cc_acuse, (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social), " +
					"	d.ig_numero_docto, " +
					"   TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') fecha_emision, " +
					"	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') fecha_vencimiento, " +
					"	m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, " +
					"	d.fn_monto_dscto, d.cs_dscto_especial, " +
					"	i.cg_razon_social, " +
					"	I2.cg_razon_social as beneficiario, " +
					"	d.fn_porc_beneficiario as por_beneficiario, " +
					"	ds.fn_importe_recibir_benef as importe_a_recibir " +
					"FROM 	com_solicitud s, " +
					"		com_docto_seleccionado ds, " +
					"		com_documento d, " +
					"       com_acuse3 a3, " +
					"       comcat_epo e, " +
					"       comcat_pyme p, " +
					"       comcat_moneda m, " +
					"       comcat_if i, " +
					"		comcat_if I2 " +
					"WHERE s.ic_documento = ds.ic_documento " +
					"	   AND ds.ic_documento = d.ic_documento " +
					"	   AND s.cc_acuse = a3.cc_acuse " +
					"	   AND d.ic_beneficiario = i.ic_if " +
					"  	   AND d.ic_beneficiario = " +  iNoCliente +
					"	   AND d.ic_epo = e.ic_epo " +
					" 	   AND d.ic_pyme = p.ic_pyme "+
					"	   AND d.ic_beneficiario=I2.ic_if " +
					"      AND d.ic_moneda = m.ic_moneda " +
					condicion;
			System.out.println("QUERY::getDocumentQueryFile: "+query_Reporte);
			return query_Reporte;
		}
		
		
		
//*********************MIGRACION IF******************
		
		
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(AvisNotiBenefDE.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String claveEpo;
	private String claveIF;
	private String df_fecha_notMin;
	private String df_fecha_notMax;
	private String cc_acuse;
	private String tipoFactoraje;
	private String fechaActual;
	
	
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		String SQLHint ="", condicion ="";
		
		if(!df_fecha_notMin.equals("") &&  !df_fecha_notMax.equals("")) {
			SQLHint = "/*+ leading(s) use_nl(s ds d a3 m)*/";
			condicion += " and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') " +
							" and s.df_fecha_solicitud < TO_DATE(? ,'dd/mm/yyyy')+1 ";	
			conditions.add(df_fecha_notMin);
			conditions.add(df_fecha_notMax);
		}
				
		if (!claveEpo.equals("")) {
			condicion += " AND d.ic_epo = ? ";
			SQLHint = (SQLHint.equals(""))?"/*+ use_nl(d s a3)*/":SQLHint;
			conditions.add(claveEpo);
		}
		
		if (!cc_acuse.equals("")) {
			condicion += " AND a3.cc_acuse = ?  ";
			SQLHint = "/*+ index(a3 cp_com_acuse3_pk) use_nl(a3 s d)*/";
			conditions.add(cc_acuse);
		}
		
		if( claveEpo.equals("") && df_fecha_notMin.equals("") && df_fecha_notMax.equals("") && cc_acuse.equals("") ) {
			condicion += " AND s.df_fecha_solicitud >= to_date( ? , 'dd/mm/yyyy')" +
							 " AND s.df_fecha_solicitud < to_date( ? , 'dd/mm/yyyy') + 1";
			SQLHint = "/*+ leading(s) use_nl(s d a3)*/";
			conditions.add(fechaActual);	
			conditions.add(fechaActual);	
		}
			
		 qrySentencia.append(
				"SELECT " + SQLHint +
				"	m.cd_nombre as MONEDA,  " +	
				"	count(1) as NUM_DOCTOS, " +
				"	sum(d.fn_monto_dscto) as TOTAL_MONTO, " +
				"	sum(ds.fn_importe_recibir_benef) as TOTAL_MONTO_IMPORTE " +							
				" FROM 	com_solicitud s, " +
				" com_docto_seleccionado ds, " +
				" com_documento d, " +
				" com_acuse3 a3, " +
				" comcat_moneda m " +
				" WHERE s.ic_documento = ds.ic_documento " +
				" AND ds.ic_documento = d.ic_documento " +
				" AND s.cc_acuse = a3.cc_acuse " +
				" AND d.ic_moneda = m.ic_moneda " +
				condicion +
				" AND d.ic_beneficiario = ? ") ;
				qrySentencia.append(" GROUP BY D.IC_MONEDA, M.CD_NOMBRE "+
										  " ORDER BY D.IC_MONEDA ");								 
				conditions.add(claveIF);
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		String SQLHint ="", condicion ="";
		
		if(!df_fecha_notMin.equals("")  && !df_fecha_notMax.equals("")) {
			condicion += " and s.df_fecha_solicitud >= TO_DATE( ? ,'dd/mm/yyyy') " +
							 " and s.df_fecha_solicitud < TO_DATE( ? ,'dd/mm/yyyy')+1 ";
			SQLHint = "/*+ leading(s) use_nl(s d a3)*/";
			conditions.add(df_fecha_notMin);
			conditions.add(df_fecha_notMax);
		}
		if (!claveEpo.equals("")) {
			condicion += " AND D.ic_epo = ? ";
			SQLHint = (SQLHint.equals(""))?"/*+ use_nl(s d a3)*/":SQLHint;
			conditions.add(claveEpo);
		}
		if (!cc_acuse.equals("")) {
			SQLHint = "/*+ index(a3 cp_com_acuse3_pk) use_nl(a3, s, d)*/";
			condicion += " AND A3.cc_acuse =  ? ";
			conditions.add(cc_acuse);			
		}
		
		if( claveEpo.equals("") && df_fecha_notMin.equals("") && df_fecha_notMax.equals("") && cc_acuse.equals("") ) {
			condicion += " AND s.df_fecha_solicitud >= to_date( ? , 'dd/mm/yyyy')" +
							 " AND s.df_fecha_solicitud < to_date( ? , 'dd/mm/yyyy') + 1";
			SQLHint = "/*+ leading(s) use_nl(s d a3)*/";
			conditions.add(fechaActual);	
			conditions.add(fechaActual);	
		}

		qrySentencia.append(
				"SELECT  " + SQLHint +
				"	d.ic_documento, " +
				"	'AvisNotiBenefDE::getDocumentQuery' AS PANTALLA " +
				"FROM 	com_solicitud s, " +
				"	com_documento d, " +
				"  com_acuse3 a3 " +
				"WHERE s.ic_documento = d.ic_documento " +
				"AND s.cc_acuse = a3.cc_acuse " +
				condicion +"  AND d.ic_beneficiario =  ? " ) ;
				conditions.add(claveIF);	
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		qrySentencia.append(
					"SELECT /*+ index(d cp_com_documento_pk)*/ " +
						" 	(DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social) as NOMBRE_EPO, " +
						"	TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') as FECHA_NOTIFICACION, " +
						"	a3.cc_acuse as ACUSE_NOTIFICACION  , (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social) as NOMBRE_PYME, " +
						"	d.ig_numero_docto as NUM_DOCUMENTO , " +
						"   TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') as FECHA_EMISION, " +
						"	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') as FECHA_VENCIMIENTO, " +
						"	m.cd_nombre as  MONEDA , d.fn_monto , d.fn_porc_anticipo as PORCE_BENEFICIARIO , " +
						"	d.fn_monto_dscto  as MONTO_DESCUENTO , d.cs_dscto_especial, " +
						"	i.cg_razon_social  , " +
						"	I2.cg_razon_social as BENEFICIARIO, " +
						"	d.fn_porc_beneficiario as PORCE_BENEFICIARIO, " +
						"	ds.fn_importe_recibir_benef as IMPORTE_RECIBIR " +						
						"FROM com_documento d, " +
						"	com_docto_seleccionado ds, " +
						"  comcat_epo e, " +
						"  com_acuse3 a3, " +
						"  comcat_pyme p, " +
						"  comcat_moneda m, " +
						"  comcat_if i, " +
						"  com_solicitud s, " +
						"	comcat_if I2 " +
						"WHERE d.ic_documento = ds.ic_documento " +
						"	AND ds.ic_documento = s.ic_documento " +
						"	AND s.cc_acuse = a3.cc_acuse " +
						"	AND d.ic_beneficiario = i.ic_if " +
						"  AND i.ic_if =  ? "  +
						"	AND d.ic_epo = e.ic_epo " +
						" 	AND d.ic_pyme = p.ic_pyme "+
						"	AND d.ic_beneficiario=I2.ic_if " +
						"  AND d.ic_moneda = m.ic_moneda " );
					conditions.add(claveIF);	
					
					
		qrySentencia.append(" AND (");
		for (int i = 0; i < pageIds.size(); i++) { 
			List lItem = (ArrayList)pageIds.get(i);
			if(i > 0){
				qrySentencia.append("  OR  ");
			}
			qrySentencia.append(" D.ic_documento = ? ");
			conditions.add(new Long(lItem.get(0).toString()));
		}
		qrySentencia.append(" ) ");
			
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		String SQLHint ="", condicion ="";
	
	
		if(!df_fecha_notMin.equals("") &&  !df_fecha_notMax.equals("")) {
			condicion += " and s.df_fecha_solicitud >= TO_DATE(? ,'dd/mm/yyyy') " +
							 " and s.df_fecha_solicitud < TO_DATE(? ,'dd/mm/yyyy')+1 ";
			SQLHint = "/*+ leading(s) use_nl(s ds d a3)*/";
			conditions.add(df_fecha_notMin);	
			conditions.add(df_fecha_notMax);	
		}
		
		if (!claveEpo.equals("")) {
				condicion += " AND d.ic_epo = ? ";
				SQLHint = (SQLHint.equals(""))?"/*+ use_nl(s d a3)*/":SQLHint;
				conditions.add(claveEpo);	
			}
			if (!cc_acuse.equals("")) {
				SQLHint = "/*+ index(a3 cp_com_acuse3_pk) use_nl(s, ds, d, a3)*/";
				condicion += " AND a3.cc_acuse = ?  ";
				conditions.add(cc_acuse);	
			}
			
			if( claveEpo.equals("") && df_fecha_notMin.equals("") && df_fecha_notMax.equals("") && cc_acuse.equals("") ) {
				condicion += " AND s.df_fecha_solicitud >= to_date( ? , 'dd/mm/yyyy')" +
							 " AND s.df_fecha_solicitud < to_date(? , 'dd/mm/yyyy') + 1";
				SQLHint = "/*+ leading(s) use_nl(s ds d a3)*/";
				conditions.add(fechaActual);	
				conditions.add(fechaActual);	
			}


			qrySentencia.append(
				"SELECT " + SQLHint +
					"	(DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social) as NOMBRE_EPO, " +
					"	TO_CHAR (a3.df_fecha_hora, 'DD/MM/YYYY') as FECHA_NOTIFICACION, " +
					"	a3.cc_acuse as ACUSE_NOTIFICACION , (DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social) as NOMBRE_PYME , " +
					"	d.ig_numero_docto as NUM_DOCUMENTO, " +
					"   TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') as FECHA_EMISION, " +
					"	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') as FECHA_VENCIMIENTO, " +
					"	m.cd_nombre as MONEDA , d.fn_monto, d.fn_porc_anticipo, " +
					"	d.fn_monto_dscto as MONTO_DESCUENTO , d.cs_dscto_especial, " +
					"	i.cg_razon_social, " +
					"	I2.cg_razon_social as BENEFICIARIO, " +
					"	d.fn_porc_beneficiario as PORCE_BENEFICIARIO, " +
					"	ds.fn_importe_recibir_benef as IMPORTE_RECIBIR " +
					"FROM 	com_solicitud s, " +
					"		com_docto_seleccionado ds, " +
					"		com_documento d, " +
					"       com_acuse3 a3, " +
					"       comcat_epo e, " +
					"       comcat_pyme p, " +
					"       comcat_moneda m, " +
					"       comcat_if i, " +
					"		comcat_if I2 " +
					"WHERE s.ic_documento = ds.ic_documento " +
					"	   AND ds.ic_documento = d.ic_documento " +
					"	   AND s.cc_acuse = a3.cc_acuse " +
					"	   AND d.ic_beneficiario = i.ic_if " +					
					"	   AND d.ic_epo = e.ic_epo " +
					" 	   AND d.ic_pyme = p.ic_pyme "+
					"	   AND d.ic_beneficiario=I2.ic_if " +
					"      AND d.ic_moneda = m.ic_moneda "+
					condicion+
					"    AND d.ic_beneficiario =  ? " );
					conditions.add(claveIF);	
					
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		

		try {
		
			HttpSession session = request.getSession();
			String tipoUsuario = (String)session.getAttribute("strTipoUsuario");
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
					
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
			pdfDoc.setTable(12, 100);			
			pdfDoc.setCell("EPO ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de notificaci�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Acuse de notificaci�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre del proveedor","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("N�mero de documento","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de emisi�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Descuento","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Porcentaje Beneficiario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Importe Recibir Beneficiario","celda01",ComunesPDF.CENTER);
			
		
			while (rs.next()) {
				String nombre_epo = (rs.getString("NOMBRE_EPO")==null)?" ":rs.getString("NOMBRE_EPO");  
				String fecha_not = (rs.getString("FECHA_NOTIFICACION")==null)?" ":rs.getString("FECHA_NOTIFICACION");  
				String acuse_not = (rs.getString("ACUSE_NOTIFICACION")==null)?" ":rs.getString("ACUSE_NOTIFICACION");  
				String nombre_pyme = (rs.getString("NOMBRE_PYME")==null)?" ":rs.getString("NOMBRE_PYME");  
				String num_documento = (rs.getString("NUM_DOCUMENTO")==null)?" ":rs.getString("NUM_DOCUMENTO");  
				String fecha_emision = (rs.getString("FECHA_EMISION")==null)?" ":rs.getString("FECHA_EMISION");  
				String fecha_vencimiento = (rs.getString("FECHA_VENCIMIENTO")==null)?" ":rs.getString("FECHA_VENCIMIENTO");  
				String moneda = (rs.getString("MONEDA")==null)?" ":rs.getString("MONEDA");  
				String monto_desc = (rs.getString("MONTO_DESCUENTO")==null)?" ":rs.getString("MONTO_DESCUENTO");  
				String beneficiario = (rs.getString("BENEFICIARIO")==null)?" ":rs.getString("BENEFICIARIO");  
				String porc_benefi = (rs.getString("PORCE_BENEFICIARIO")==null)?" ":rs.getString("PORCE_BENEFICIARIO");  
				String importe_recibir = (rs.getString("IMPORTE_RECIBIR")==null)?" ":rs.getString("IMPORTE_RECIBIR");  
			
				pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(fecha_not,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(acuse_not,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(num_documento,"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell(fecha_emision,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fecha_vencimiento,"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(monto_desc), 2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(beneficiario,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(porc_benefi+"%","formas",ComunesPDF.CENTER);	
				pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(importe_recibir), 2),"formas",ComunesPDF.RIGHT);				
			}
		
			pdfDoc.addTable();
			pdfDoc.endDocument();
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();	
		
		try {
		
			contenidoArchivo.append("EPO, Fecha de notificaci�n, Acuse de notificaci�n, Nombre del proveedor, N�mero de documento, Fecha de emisi�n, Fecha de vencimiento,	 Moneda,	 Monto Descuento,	 Beneficiario,	 Porcentaje Beneficiario,	 Importe Recibir Beneficiario\n ");
		
			while (rs.next()) {
				String nombre_epo = (rs.getString("NOMBRE_EPO")==null)?" ":rs.getString("NOMBRE_EPO");  
				String fecha_not = (rs.getString("FECHA_NOTIFICACION")==null)?" ":rs.getString("FECHA_NOTIFICACION");  
				String acuse_not = (rs.getString("ACUSE_NOTIFICACION")==null)?" ":rs.getString("ACUSE_NOTIFICACION");  
				String nombre_pyme = (rs.getString("NOMBRE_PYME")==null)?" ":rs.getString("NOMBRE_PYME");  
				String num_documento = (rs.getString("NUM_DOCUMENTO")==null)?" ":rs.getString("NUM_DOCUMENTO");  
				String fecha_emision = (rs.getString("FECHA_EMISION")==null)?" ":rs.getString("FECHA_EMISION");  
				String fecha_vencimiento = (rs.getString("FECHA_VENCIMIENTO")==null)?" ":rs.getString("FECHA_VENCIMIENTO");  
				String moneda = (rs.getString("MONEDA")==null)?" ":rs.getString("MONEDA");  
				String monto_desc = (rs.getString("MONTO_DESCUENTO")==null)?" ":rs.getString("MONTO_DESCUENTO");  
				String beneficiario = (rs.getString("BENEFICIARIO")==null)?" ":rs.getString("BENEFICIARIO");  
				String porc_benefi = (rs.getString("PORCE_BENEFICIARIO")==null)?" ":rs.getString("PORCE_BENEFICIARIO");  
				String importe_recibir = (rs.getString("IMPORTE_RECIBIR")==null)?" ":rs.getString("IMPORTE_RECIBIR");  
				
				contenidoArchivo.append(nombre_epo.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(fecha_not.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(acuse_not.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(nombre_pyme.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(num_documento.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(fecha_emision.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(fecha_vencimiento.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(moneda.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(monto_desc.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(beneficiario.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(porc_benefi.replaceAll(",", "")+ "%,");	
				contenidoArchivo.append(importe_recibir.replaceAll(",", "")+ "\n");	
				
			}
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
	}
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getClaveEpo() {
		return claveEpo;
	}

	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	public String getDf_fecha_notMin() {
		return df_fecha_notMin;
	}

	public void setDf_fecha_notMin(String df_fecha_notMin) {
		this.df_fecha_notMin = df_fecha_notMin;
	}

	public String getDf_fecha_notMax() {
		return df_fecha_notMax;
	}

	public void setDf_fecha_notMax(String df_fecha_notMax) {
		this.df_fecha_notMax = df_fecha_notMax;
	}

	public String getCc_acuse() {
		return cc_acuse;
	}

	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}

	public String getTipoFactoraje() {
		return tipoFactoraje;
	}

	public void setTipoFactoraje(String tipoFactoraje) {
		this.tipoFactoraje = tipoFactoraje;
	}

	public String getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(String fechaActual) {
		this.fechaActual = fechaActual;
	}
}








