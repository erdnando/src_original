package com.netro.descuento;

import com.netro.cadenas.ConsSeleccionIF;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class DesPubEpoDET implements IQueryGeneratorRegExtJS {
  public DesPubEpoDET() {  }
	
	private List conditions;
	StringBuffer strQuery;
	
	//Variables de Sesi�n
	private String iNoCliente;
	
	//Condiciones de la consulta
   private String fechaRegIni;
	private String fechaRegFin;
	private boolean mandante;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsSeleccionIF.class);
	
   public String getQuery(){
	 String stQuery = "";
	 
	       stQuery = " SELECT E.cg_pyme_epo_interno, " +
						  " 	     D.IG_NUMERO_DOCTO, " +
						  " 	     TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') as DF_FECHA_DOCTO, " +
						  " 	     TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') as DF_FECHA_VENC, " +
						  "   	  M.cd_nombre as cd_nombre, " +
						  " 	     D.FN_MONTO, " +
						  " 	     D.CS_DSCTO_ESPECIAL, " +
						  " 	     D.CT_REFERENCIA, " +
						  " 	     D.CG_CAMPO1, " +
						  " 	     D.CG_CAMPO2, " +
						  "	     D.CS_DETALLE, " +   
						  "        D.IC_DOCUMENTO, " +  
						  "        TO_CHAR(D.DF_ALTA,'DD/MM/YYYY') as DF_ALTA ";
						 
	 if(iNoCliente.equals("211")){
	       stQuery += "	     ,COUNT(DD.IC_DOCTO_DETALLE) "   +
							"	     ,SUM(DD.CG_CAMPO6) "  +
						   " FROM   com_documento D, "   +
							"	      com_documento_detalle DD,"   +
							" 	      comrel_pyme_epo E,"   +
							" 	      comcat_moneda M"   +
							" WHERE  D.IC_DOCUMENTO=DD.IC_DOCUMENTO(+) " +                        
							" 	 AND D.IC_PYME=E.IC_PYME " +
							" 	 AND D.IC_EPO=E.IC_EPO " +
							" 	 AND D.IC_MONEDA=M.IC_MONEDA ";
	  if (!fechaRegIni.equals("")) {
	       stQuery += "   AND D.df_alta >= to_date('" + fechaRegIni + "','dd/mm/yyyy') ";
	  }
	  if (!fechaRegFin.equals("")){
	        stQuery += "   AND D.df_alta < TO_DATE('" + fechaRegFin + "', 'dd/mm/yyyy') + 1 ";
	  }
	  if(!"".equals(iNoCliente)){
		     stQuery += "   AND D.IC_EPO = " + iNoCliente;
	  }
	       stQuery += "  GROUP BY E.cg_pyme_epo_interno "  +
							"	   ,D.IG_NUMERO_DOCTO "  +
							"	   ,TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') " +
							"	   ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') "  +
							"	   ,M.CD_NOMBRE "+
							"	   ,D.FN_MONTO " +
							"	   ,D.CS_DSCTO_ESPECIAL " +
							"	   ,D.CT_REFERENCIA " +
							"	   ,D.CG_CAMPO1 " +
							"	   ,D.CG_CAMPO2 " +
							"	   ,D.CS_DETALLE " +
							"	   ,D.IC_DOCUMENTO " +
							"	   ,TO_CHAR(D.DF_ALTA,'DD/MM/YYYY' )";
	  
	 }else {
	       stQuery += "      ,ma.CG_RAZON_SOCIAL as mandante " +  // Fodea 023 2009
							"      ,tf.cg_nombre as TIPO_FACTORAJE " +
							" FROM  com_documento D, " +
							" 	     comrel_pyme_epo E, " +
							" 	     comcat_moneda M, " +
							" 	     COMCAT_MANDANTE ma, " +
							"       COMCAT_TIPO_FACTORAJE tf "+
							" WHERE D.IC_PYME=E.IC_PYME " +
							" 	 AND D.IC_EPO=E.IC_EPO " +
							" 	 AND D.IC_MONEDA=M.IC_MONEDA " +
							"   AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "+
							"   AND d.cs_dscto_especial = tf.cc_tipo_factoraje ";
	  if (!fechaRegIni.equals("")) {
	       stQuery += "   AND D.df_alta >= to_date('" + fechaRegIni + "','dd/mm/yyyy') ";
	  }
	  if (!fechaRegFin.equals("")){
	        stQuery += "  AND D.df_alta < to_date('" + fechaRegFin + "', 'dd/mm/yyyy') + 1 ";
	  }
	  if(!"".equals(iNoCliente)){
		     stQuery += "   AND D.IC_EPO = " + iNoCliente;
	  }
	 }
	 stQuery += " ORDER BY D.DF_ALTA ";
	 
	 return stQuery;
  }
  
  public String getAggregateCalculationQuery() {
		conditions	= new ArrayList();
		strQuery	= new StringBuffer();
		
		 strQuery.append("SELECT Count(1) as TotalRegistros, SUM(FN_MONTO) as TotalMonto, CD_NOMBRE FROM ( ");
		 strQuery.append(getQuery());
		 strQuery.append(" ) TabCalc Group by CD_NOMBRE ");

		log.debug("getAggregateCalculationQuery)"+strQuery.toString());
		log.debug("getAggregateCalculationQuery)"+conditions);
		
		return strQuery.toString();
  }
  
  public String getDocumentQuery() {
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		
		strQuery.append(getQuery());
		
		log.debug("getDocumentQuery"+strQuery.toString());
		log.debug("getDocumentQuery)"+conditions);
		
		return strQuery.toString();
  }
  
  public String getDocumentSummaryQueryForIds(List pageIds) {
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		
		strQuery.append("SELECT * FROM ( ");
		strQuery.append(getQuery());
		strQuery.append(" ) Tab ");
		
		 for(int i=0;i<pageIds.size();i++) {
		  List lItem = (List)pageIds.get(i);
		  if(i==0) {
		   strQuery.append(" WHERE Tab.ic_documento in ( ");
		  }
		  strQuery.append("?");
		  conditions.add(new Long(lItem.get(11).toString()));					
		  if(i!=(pageIds.size()-1)) {
		   strQuery.append(",");
		  }else{
		   strQuery.append(" ) ");
		  }			
		 }
		
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		
		return strQuery.toString();
	}
	
	public String getDocumentQueryFile() {
	   conditions	= new ArrayList();
		strQuery		= new StringBuffer();	
		
		strQuery.append(getQuery());
		   
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		
		return strQuery.toString();
	}
	
	
	/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		log.info("crearCustomFile (E)");
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		int nacional = 0;
		double montoNacional = 0.00;
			 
		int dolarAmericano = 0;
	   double montoDolarAmericano = 0.00;
		
		try {
		 if(tipo.equals("CSV")) {
		    if(mandante == true){
			  linea = "Fecha de publicaci�n,Proveedor,N�mero de Documento,Fecha Emisi�n,Fecha Vencimiento,Moneda,Monto,Tipo,Referencia,Adicional 1,Adicional 2,Mandante\n";
			 } else {
			  linea = "Fecha de publicaci�n,Proveedor,N�mero de Documento,Fecha Emisi�n,Fecha Vencimiento,Moneda,Monto,Tipo,Referencia,Adicional 1,Adicional 2\n";
			 }
			 nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			 writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			 buffer = new BufferedWriter(writer);
			 buffer.write(linea);
			 
			 if(mandante == true){
			 while (rs.next()) {
			  String dfAlta = (rs.getString("DF_ALTA") == null) ? "" : rs.getString("DF_ALTA");
			  String proveedor = (rs.getString("CG_PYME_EPO_INTERNO") == null) ? "" : rs.getString("CG_PYME_EPO_INTERNO");
			  String igNumDocto	= (rs.getString("IG_NUMERO_DOCTO") == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
			  String dfFechaDocto = (rs.getString("DF_FECHA_DOCTO") == null) ? "" : rs.getString("DF_FECHA_DOCTO");
			  String dfFechaVenc =	(rs.getString("DF_FECHA_VENC") == null) ? "" : rs.getString("DF_FECHA_VENC");
			  String nombre = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
			  String monto	=	(rs.getString("FN_MONTO") == null) ? "" : rs.getString("FN_MONTO");
			  String tipoFac = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
			  String referencia = (rs.getString("CT_REFERENCIA") == null) ? "" : rs.getString("CT_REFERENCIA");
			  String adicionalUno	=	(rs.getString("CG_CAMPO1") == null) ? "" : rs.getString("CG_CAMPO1");
			  String adicionalDos = (rs.getString("CG_CAMPO2") == null) ? "" : rs.getString("CG_CAMPO2");
			  String mandante = (rs.getString("MANDANTE") == null) ? "" : rs.getString("MANDANTE");
		     
			  if(nombre.equals("MONEDA NACIONAL")){
				  montoNacional += Double.valueOf(monto.trim()).doubleValue();
				  nacional ++;
			  }
			  else if(nombre.equals("DOLAR AMERICANO")){
				  montoDolarAmericano += Double.valueOf(monto.trim()).doubleValue();
				  dolarAmericano ++;
			  }
			  
			   linea = dfAlta.replace(',',' ') + ", " + 
			           proveedor.replace(',',' ') + ", " +
						  igNumDocto.replace(',',' ') + ", " +
						  dfFechaDocto.replace(',',' ') + ", " +
						  dfFechaVenc.replace(',',' ') + ", " +
						  nombre.replace(',',' ') + ", " +
						  monto.replace(',',' ') + ", " +
						  tipoFac.replace(',',' ') + ", " +
						  referencia.replace(',',' ') + ", " +
						  adicionalUno.replace(',',' ') + ", " +
						  adicionalDos.replace(',',' ') + ", " +
						  mandante.replace(',',' ') + "\n";
						  
			  buffer.write(linea);
			 }} else{
			 			 while (rs.next()) {
			  String dfAlta = (rs.getString("DF_ALTA") == null) ? "" : rs.getString("DF_ALTA");
			  String proveedor = (rs.getString("CG_PYME_EPO_INTERNO") == null) ? "" : rs.getString("CG_PYME_EPO_INTERNO");
			  String igNumDocto	= (rs.getString("IG_NUMERO_DOCTO") == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
			  String dfFechaDocto = (rs.getString("DF_FECHA_DOCTO") == null) ? "" : rs.getString("DF_FECHA_DOCTO");
			  String dfFechaVenc =	(rs.getString("DF_FECHA_VENC") == null) ? "" : rs.getString("DF_FECHA_VENC");
			  String nombre = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
			  String monto	=	(rs.getString("FN_MONTO") == null) ? "" : rs.getString("FN_MONTO");
			  String tipoFac = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
			  String referencia = (rs.getString("CT_REFERENCIA") == null) ? "" : rs.getString("CT_REFERENCIA");
			  String adicionalUno	=	(rs.getString("CG_CAMPO1") == null) ? "" : rs.getString("CG_CAMPO1");
			  String adicionalDos = (rs.getString("CG_CAMPO2") == null) ? "" : rs.getString("CG_CAMPO2");
			  
			  if(nombre.equals("MONEDA NACIONAL")){
				  montoNacional += Double.valueOf(monto.trim()).doubleValue();
				  nacional ++;
			  }
			  else if(nombre.equals("DOLAR AMERICANO")){
				  montoDolarAmericano += Double.valueOf(monto.trim()).doubleValue();
				  dolarAmericano ++;
			  }
			  
			   linea = dfAlta.replace(',',' ') + ", " + 
			           proveedor.replace(',',' ') + ", " +
						  igNumDocto.replace(',',' ') + ", " +
						  dfFechaDocto.replace(',',' ') + ", " +
						  dfFechaVenc.replace(',',' ') + ", " +
						  nombre.replace(',',' ') + ", " +
						  monto.replace(',',' ') + ", " +
						  tipoFac.replace(',',' ') + ", " +
						  referencia.replace(',',' ') + ", " +
						  adicionalUno.replace(',',' ') + ", " +
						  adicionalDos.replace(',',' ') + "\n";
						  
			  buffer.write(linea);
			 }
			 }
			 
			 
			 if( nacional != 0) {
			  linea = "Totales MONEDA NACIONAL,,,,," +Integer.toString(nacional) +","+ Double.toString(montoNacional);
			  buffer.write(linea);
			 }
			 buffer.newLine();
			 if(dolarAmericano != 0) {
			  linea = "Totales DOLAR AMERICANO,,,,," +Integer.toString(dolarAmericano) +","+ Double.toString(montoDolarAmericano);
			  buffer.write(linea);
			 }
			 
			 buffer.close();

			} else if(tipo.equals("PDF")){

				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
				if(mandante == true){
					pdfDoc.setLTable(12,100);
				} else{
					pdfDoc.setLTable(11,100);
				}
				pdfDoc.setLCell("Fecha de publicaci�n", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Proveedor", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de Documento", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Emisi�n", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Vencimiento", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Referencia", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Adicional 1", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Adicional 2", "celda01",ComunesPDF.CENTER);
				if(mandante == true){
					pdfDoc.setLCell("Mandante", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();
				while (rs.next()){
					String dfAlta = (rs.getString("DF_ALTA") == null) ? "" : rs.getString("DF_ALTA");
					String proveedor = (rs.getString("CG_PYME_EPO_INTERNO") == null) ? "" : rs.getString("CG_PYME_EPO_INTERNO");
					String igNumDocto	= (rs.getString("IG_NUMERO_DOCTO") == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
					String dfFechaDocto = (rs.getString("DF_FECHA_DOCTO") == null) ? "" : rs.getString("DF_FECHA_DOCTO");
					String dfFechaVenc =	(rs.getString("DF_FECHA_VENC") == null) ? "" : rs.getString("DF_FECHA_VENC");
					String nombre = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
					String monto	=	(rs.getString("FN_MONTO") == null) ? "" : rs.getString("FN_MONTO");
					String tipoFac = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
					String referencia = (rs.getString("CT_REFERENCIA") == null) ? "" : rs.getString("CT_REFERENCIA");
					String adicionalUno	=	(rs.getString("CG_CAMPO1") == null) ? "" : rs.getString("CG_CAMPO1");
					String adicionalDos = (rs.getString("CG_CAMPO2") == null) ? "" : rs.getString("CG_CAMPO2");
					String mandante = (rs.getString("MANDANTE") == null) ? "" : rs.getString("MANDANTE");

					if(nombre.equals("MONEDA NACIONAL")){
						montoNacional += Double.valueOf(monto.trim()).doubleValue();
						nacional ++;
					}
					else if(nombre.equals("DOLAR AMERICANO")){
						montoDolarAmericano += Double.valueOf(monto.trim()).doubleValue();
						dolarAmericano ++;
					}

					pdfDoc.setLCell(dfAlta,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(proveedor,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(igNumDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dfFechaDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dfFechaVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoFac,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(referencia,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(adicionalUno,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(adicionalDos,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(mandante,"formas",ComunesPDF.CENTER);
				}} else {
					while (rs.next()) {
					String dfAlta = (rs.getString("DF_ALTA") == null) ? "" : rs.getString("DF_ALTA");
					String proveedor = (rs.getString("CG_PYME_EPO_INTERNO") == null) ? "" : rs.getString("CG_PYME_EPO_INTERNO");
					String igNumDocto	= (rs.getString("IG_NUMERO_DOCTO") == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
					String dfFechaDocto = (rs.getString("DF_FECHA_DOCTO") == null) ? "" : rs.getString("DF_FECHA_DOCTO");
					String dfFechaVenc =	(rs.getString("DF_FECHA_VENC") == null) ? "" : rs.getString("DF_FECHA_VENC");
					String nombre = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
					String monto	=	(rs.getString("FN_MONTO") == null) ? "" : rs.getString("FN_MONTO");
					String tipoFac = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
					String referencia = (rs.getString("CT_REFERENCIA") == null) ? "" : rs.getString("CT_REFERENCIA");
					String adicionalUno	=	(rs.getString("CG_CAMPO1") == null) ? "" : rs.getString("CG_CAMPO1");
					String adicionalDos = (rs.getString("CG_CAMPO2") == null) ? "" : rs.getString("CG_CAMPO2");

					if(nombre.equals("MONEDA NACIONAL")){
						montoNacional += Double.valueOf(monto.trim()).doubleValue();
						nacional ++;
					} else if(nombre.equals("DOLAR AMERICANO")){
						montoDolarAmericano += Double.valueOf(monto.trim()).doubleValue();
						dolarAmericano ++;
					}

					pdfDoc.setLCell(dfAlta,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(proveedor,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(igNumDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dfFechaDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dfFechaVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoFac,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(referencia,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(adicionalUno,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(adicionalDos,"formas",ComunesPDF.CENTER);
				}}
				if(mandante == true){
					if(nacional !=0){
						pdfDoc.setLCell("Totales MONEDA NACIONAL","formas",ComunesPDF.CENTER, 6);
						pdfDoc.setLCell(Integer.toString(nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoNacional,2),"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER, 4);
					}
					if(dolarAmericano != 0){
						pdfDoc.setLCell("Totales DOLAR AMERICANO","formas",ComunesPDF.CENTER, 6);
						pdfDoc.setLCell(Integer.toString(dolarAmericano),"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDolarAmericano,2),"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER, 4);
					}
				} else{
					if(nacional !=0){
						pdfDoc.setLCell("Totales MONEDA NACIONAL","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setLCell(Integer.toString(nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoNacional,2),"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER, 4);
					}
					if(dolarAmericano != 0){
						pdfDoc.setLCell("Totales DOLAR AMERICANO","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setLCell(Integer.toString(dolarAmericano),"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDolarAmericano,2),"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER, 4);
					}
				}
/*
				pdfDoc.addLTable();
				pdfDoc.setLTable(3,50);
				if(nacional !=0){
					pdfDoc.setLCell("Totales MONEDA NACIONAL","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Integer.toString(nacional),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Double.toString(montoNacional),"formas",ComunesPDF.CENTER);
				}
				if(dolarAmericano != 0){
					pdfDoc.setLCell("Totales DOLAR AMERICANO","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Integer.toString(dolarAmericano),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Double.toString(montoDolarAmericano),"formas",ComunesPDF.CENTER);
				}
*/
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}
		}
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}
	

	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}

	public void setFechaRegIni(String fechaRegIni) {
		this.fechaRegIni = fechaRegIni;
	}

	public String getFechaRegIni() {
		return fechaRegIni;
	}

	public void setFechaRegFin(String fechaRegFin) {
		this.fechaRegFin = fechaRegFin;
	}

	public String getFechaRegFin() {
		return fechaRegFin;
	}

	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}

	public String getINoCliente() {
		return iNoCliente;
	}

	public void setMandante(boolean mandante) {
		this.mandante = mandante;
	}

	public boolean isMandante() {
		return mandante;
	}
}