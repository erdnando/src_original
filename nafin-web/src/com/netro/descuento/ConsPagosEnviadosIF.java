package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsPagosEnviadosIF implements IQueryGeneratorRegExtJS {

	
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsPagosEnviadosIF.class);

	StringBuffer qrySentencia = new StringBuffer("");
	StringBuffer	condicion  =new StringBuffer(); 		
	private List   conditions;

	private String sOrden="";
	private String sFechaDel="";
	private String sFechaAl="";
	private String sFechaVencIni="";
	private String sFechaVencFin="";
	private String sFechaProIni="";
	private String sFechaProFin="";
	private String sFechaPagoIni="";
	private String sFechaPagoFin="";
	
	private String claveIF="";
	private String sTipoBanco="";
	
	
	/*Constructor*/
	public ConsPagosEnviadosIF() { }
	
	
	
  
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery() 
	{
		//return "";
		
		log.info("getAggregateCalculationQuery(E)*************************************** ::..");
		
		conditions 		= new ArrayList();
		condicion		= new StringBuffer();
		qrySentencia 	= new StringBuffer();
		StringBuffer strQuery		=	new StringBuffer();
		
		conditions.add(claveIF);
		if(!"".equals(sFechaVencIni) && !"".equals(sFechaVencFin) ){//BUSQUEDA POR FECHA DE VENCIMIENTO
			condicion.append( "AND TRUNC(df_periodo_fin) >= TO_DATE(?,'dd/mm/yyyy')" );  
			condicion.append(	"AND TRUNC(df_periodo_fin) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_periodo_fin ";
			conditions.add(sFechaVencIni);
			conditions.add(sFechaVencFin);
		}
		else if(!"".equals(sFechaProIni) && !"".equals(sFechaProFin) ){//BUSQUEDA POR FECHA PROBABLE DE PAGO
			condicion.append( "AND TRUNC(df_probable_pago) >= TO_DATE(?,'dd/mm/yyyy')" ); 
			condicion.append(	"AND TRUNC(df_probable_pago) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_probable_pago ";
			conditions.add(sFechaProIni);
			conditions.add(sFechaProFin);
		}
		else if(!"".equals(sFechaPagoIni) && !"".equals(sFechaPagoFin) ){//BUSQUEDA POR FECHA PAGO
			condicion.append( "AND TRUNC(df_deposito) >= TO_DATE(?,'dd/mm/yyyy') " );
			condicion.append(	"AND TRUNC(df_deposito) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_deposito ";
			conditions.add(sFechaPagoIni);
			conditions.add(sFechaPagoFin);
		}
		else{
			//conditions.add("");
			sOrden=" df_registro ";
		}
		
		strQuery.append(" SELECT "	+		
			            "    EP.ic_encabezado_pago as CVE_ENCABEZADO_PAGO, count(*) as DETALLES	"+
						   " 	  ,EP.ig_importe_deposito as IMPORTE_DEPOSITO"   +
						   	  
						   " FROM com_encabezado_pago EP"   +
							"		,com_detalle_pago d" +
						   " 	   ,comcat_financiera F"   +
						   " 	  ,comcat_referencia_inter RI"   +
							
						   " WHERE EP.ic_financiera = F.ic_financiera"   +
							"		AND EP.ic_encabezado_pago = d.ic_encabezado_pago(+) " +
							" 	 	AND EP.ic_referencia_inter = RI.ic_referencia_inter"  +
						   "     AND EP.ic_if = ?"   +
							condicion +
							" group by EP.ic_encabezado_pago, EP.ig_importe_deposito ");
							//condicion );
							
					
		qrySentencia.append(
		" SELECT SUM (DETALLES) as TOTAL_PAGOS , SUM (IMPORTE_DEPOSITO) AS TOTAL_IMPORTE  "+
		" FROM ("+strQuery.toString()+")" +
		" ORDER BY CVE_ENCABEZADO_PAGO ");
		
		
		
		//log.info("strQuery  "+strQuery);
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)"); 

		return qrySentencia.toString();	
	
	}
	
	public String getDocumentQuery() // comienza por obtener las llaves primarias	
	{
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		condicion		= new StringBuffer();
		
		conditions.add(claveIF);
		if(!"".equals(sFechaVencIni) && !"".equals(sFechaVencFin) ){//BUSQUEDA POR FECHA DE VENCIMIENTO
			condicion.append( "AND TRUNC(df_periodo_fin) >= TO_DATE(?,'dd/mm/yyyy')" );  
			condicion.append(	"AND TRUNC(df_periodo_fin) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_periodo_fin ";
			conditions.add(sFechaVencIni);
			conditions.add(sFechaVencFin);
		}
		else if(!"".equals(sFechaProIni) && !"".equals(sFechaProFin) ){//BUSQUEDA POR FECHA PROBABLE DE PAGO
			condicion.append( "AND TRUNC(df_probable_pago) >= TO_DATE(?,'dd/mm/yyyy')" ); 
			condicion.append(	"AND TRUNC(df_probable_pago) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_probable_pago ";
			conditions.add(sFechaProIni);
			conditions.add(sFechaProFin);
		}
		else if(!"".equals(sFechaPagoIni) && !"".equals(sFechaPagoFin) ){//BUSQUEDA POR FECHA PAGO
			condicion.append( "AND TRUNC(df_deposito) >= TO_DATE(?,'dd/mm/yyyy') " );
			condicion.append(	"AND TRUNC(df_deposito) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_deposito ";
			conditions.add(sFechaPagoIni);
			conditions.add(sFechaPagoFin);
		}
		else{
			//conditions.add("");
			sOrden=" df_registro ";
		}
//conditions.add(claveIF);
		if(!"".equals(sFechaVencIni) && !"".equals(sFechaVencFin) ){//BUSQUEDA POR FECHA DE VENCIMIENTO
			condicion.append( "AND TRUNC(df_periodo_fin) >= TO_DATE(?,'dd/mm/yyyy')" );  
			condicion.append(	"AND TRUNC(df_periodo_fin) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_periodo_fin ";
			conditions.add(sFechaVencIni);
			conditions.add(sFechaVencFin);
		}
		else if(!"".equals(sFechaProIni) && !"".equals(sFechaProFin) ){//BUSQUEDA POR FECHA PROBABLE DE PAGO
			condicion.append( "AND TRUNC(df_probable_pago) >= TO_DATE(?,'dd/mm/yyyy')" ); 
			condicion.append(	"AND TRUNC(df_probable_pago) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_probable_pago ";
			conditions.add(sFechaProIni);
			conditions.add(sFechaProFin);
		}
		else if(!"".equals(sFechaPagoIni) && !"".equals(sFechaPagoFin) ){//BUSQUEDA POR FECHA PAGO
			condicion.append( "AND TRUNC(df_deposito) >= TO_DATE(?,'dd/mm/yyyy') " );
			condicion.append(	"AND TRUNC(df_deposito) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_deposito ";
			conditions.add(sFechaPagoIni);
			conditions.add(sFechaPagoFin);
		}
		else{
			//conditions.add("");
			sOrden=" df_registro ";
		}
		
		qrySentencia.append(" SELECT /*+ use_nl(EP, F, RI) index(EP CP_COM_ENCABEZADO_PAGO_PK) */"	+
			            "    EP.ic_encabezado_pago as CVE_ENCABEZADO_PAGO"   +
						   " FROM com_encabezado_pago EP"   +
							"	  ,com_detalle_pago d" +
						   " 	  ,comcat_financiera F"   +
						   " 	  ,comcat_referencia_inter RI"   +
						   " WHERE EP.ic_financiera = F.ic_financiera"   +
							"		AND EP.ic_encabezado_pago = d.ic_encabezado_pago(+)"	+
						   " 	 	AND EP.ic_referencia_inter = RI.ic_referencia_inter"   +
						   "     AND EP.ic_if = ?"   +
							condicion +
						     
						   " ORDER BY"   +  sOrden);
				
				
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		condicion		= new StringBuffer();
		
		conditions.add(claveIF);
		if(!"".equals(sFechaVencIni) && !"".equals(sFechaVencFin) ){//BUSQUEDA POR FECHA DE VENCIMIENTO
			condicion.append( "AND TRUNC(df_periodo_fin) >= TO_DATE(?,'dd/mm/yyyy')" );  
			condicion.append(	"AND TRUNC(df_periodo_fin) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_periodo_fin ";
			conditions.add(sFechaVencIni);
			conditions.add(sFechaVencFin);
		}
		else if(!"".equals(sFechaProIni) && !"".equals(sFechaProFin) ){//BUSQUEDA POR FECHA PROBABLE DE PAGO
			condicion.append( "AND TRUNC(df_probable_pago) >= TO_DATE(?,'dd/mm/yyyy')" ); 
			condicion.append(	"AND TRUNC(df_probable_pago) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_probable_pago ";
			conditions.add(sFechaProIni);
			conditions.add(sFechaProFin);
		}
		else if(!"".equals(sFechaPagoIni) && !"".equals(sFechaPagoFin) ){//BUSQUEDA POR FECHA PAGO
			condicion.append( "AND TRUNC(df_deposito) >= TO_DATE(?,'dd/mm/yyyy') " );
			condicion.append(	"AND TRUNC(df_deposito) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_deposito ";
			conditions.add(sFechaPagoIni);
			conditions.add(sFechaPagoFin);
		}
		else{
			//conditions.add("");
			sOrden=" df_registro ";
		}
		
		qrySentencia.append(" SELECT /*+ use_nl(EP, F, RI) index(EP CP_COM_ENCABEZADO_PAGO_PK) */"	+
			            "    EP.ic_encabezado_pago as CVE_ENCABEZADO_PAGO"   +
						   " 	  ,EP.ic_if as CVE_IF"   +
						   " 	  ,EP.ig_sucursal as CVE_SUCURSAL"   +
						   " 	  ,EP.ic_moneda as CVE_MONEDA"   +
						   " 	  ,TO_CHAR(EP.df_periodo_fin,'DD/MM/YYYY') as FECHA_PERIODO_FIN"   +
						   " 	  ,TO_CHAR(EP.df_probable_pago,'DD/MM/YYYY') as FECHA_PROB_PAGO"   +
						   " 	  ,TO_CHAR(EP.df_deposito,'DD/MM/YYYY') as FECHA_DEPOSITO"   +
						   " 	  ,TO_CHAR(EP.df_registro,'DD/MM/YYYY') as FECHA_REGISTRO"   +
						   " 	  ,EP.ig_importe_deposito as IMPORTE_DEPOSITO"   +
						   " 	  ,F.cd_nombre as NOMBRE_BANCO"   +
						   " 	  ,RI.cg_descripcion as REFERENCIA_INTER"   +
						   " 	  ,EP.cg_referencia_banco as REFERENCIA_BANCO"   +
							
							" 	  ,d.ic_encabezado_pago" +
							"	  ,d.ig_subaplicacion as SUBAPLICACION" +
							"	  ,d.ig_prestamo as PRESTAMO"	+
							"	  ,d.ig_cliente as CLIENTE_SIRAC" +
							"    ,d.fg_amortizacion as TOTAL_AMORTIZACION" + 
							" 	  ,d.fg_interes as INTERES"	+ 
							"	  ,d.fg_subsidio as SUBSIDIO"	+
							"	  ,d.fg_comision as COMISION" +
							"	  ,d.fg_iva as IVA" +
							"	  ,d.fg_totalexigible as TOTAL_EXIGIBLE"	+
							"    ,DECODE (d.cg_concepto_pago,'AN', 'Anticipado','VE','Vencimiento','CV', 'Cartera Vencida','CO','Comision') AS CONCEPTO_PAGO"   +
							//"	  ,d.cg_concepto_pago as CONCEPTO_PAGO"	+
							"    ,DECODE (d.cg_origen_pago,'I', 'Intermediario','A','Acreditado') AS ORIGEN_PAGO"   +
							//"	  ,d.cg_origen_pago as ORIGEN_PAGO"		+
							"	  ,d.fg_interes_mora as INTERES_MORA"	+
							"	  ,d.ig_sucursal as SUCURSAL" +
							"	  ,d.cg_acreditado as ACREDITADO " +
							"	  ,TO_CHAR(d.df_operacion,'DD/MM/YYYY') as FECHA_OPERACION" +
							"	  ,TO_CHAR(d.df_vencimiento,'DD/MM/YYYY') as FECHA_VENCIMIENTO" +
							"    ,TO_CHAR(d.df_pago,'DD/MM/YYYY') as FECHA_PAGO" +
							"	  ,d.fn_saldo_insoluto as SALDO_INSOLUTO" +
							"	  ,d.ig_tasa as TASA" +
							"	  ,d.ig_dias as DIAS" +
	  
						   " FROM com_encabezado_pago EP"   +
							"	  ,com_detalle_pago d" +
						   " 	  ,comcat_financiera F"   +
						   " 	  ,comcat_referencia_inter RI"   +
						   " WHERE EP.ic_financiera = F.ic_financiera"   +
							"		AND EP.ic_encabezado_pago = d.ic_encabezado_pago(+)"	+
						   " 	 	AND EP.ic_referencia_inter = RI.ic_referencia_inter" +
						   "     AND EP.ic_if = ?"    + 
							condicion);
		qrySentencia.append(" AND (");
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
				
			if(i > 0){qrySentencia.append("  OR  ");}
			
			qrySentencia.append("( EP.ic_encabezado_pago = ? )");
			conditions.add(lItem.get(0).toString());
		}
		qrySentencia.append(" ) " +
		" ORDER BY"   +
						   sOrden);

		
		log.info("getDocumentSummaryQueryForIds  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		condicion		= new StringBuffer();
		
		conditions.add(claveIF);
		if(!"".equals(sFechaVencIni) && !"".equals(sFechaVencFin) ){//BUSQUEDA POR FECHA DE VENCIMIENTO
			condicion.append( "AND TRUNC(df_periodo_fin) >= TO_DATE(?,'dd/mm/yyyy')" );  
			condicion.append(	"AND TRUNC(df_periodo_fin) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_periodo_fin ";
			conditions.add(sFechaVencIni);
			conditions.add(sFechaVencFin);
		}
		else if(!"".equals(sFechaProIni) && !"".equals(sFechaProFin) ){//BUSQUEDA POR FECHA PROBABLE DE PAGO
			condicion.append( "AND TRUNC(df_probable_pago) >= TO_DATE(?,'dd/mm/yyyy')" ); 
			condicion.append(	"AND TRUNC(df_probable_pago) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_probable_pago ";
			conditions.add(sFechaProIni);
			conditions.add(sFechaProFin);
		}
		else if(!"".equals(sFechaPagoIni) && !"".equals(sFechaPagoFin) ){//BUSQUEDA POR FECHA PAGO
			condicion.append( "AND TRUNC(df_deposito) >= TO_DATE(?,'dd/mm/yyyy') " );
			condicion.append(	"AND TRUNC(df_deposito) <= TO_DATE(?,'dd/mm/yyyy')" );
			
			sOrden=" df_deposito ";
			conditions.add(sFechaPagoIni);
			conditions.add(sFechaPagoFin);
		}
		else{
			//conditions.add("");
			sOrden=" df_registro ";
		}
		
			qrySentencia.append(" SELECT /*+ use_nl(EP, F, RI) index(EP CP_COM_ENCABEZADO_PAGO_PK) */"	+
			            "    EP.ic_encabezado_pago as CVE_ENCABEZADO_PAGO"   +
						   " 	  ,EP.ic_if as CVE_IF"   +
						   " 	  ,EP.ig_sucursal as CVE_SUCURSAL"   +
						   " 	  ,EP.ic_moneda as CVE_MONEDA"   +
						   " 	  ,TO_CHAR(EP.df_periodo_fin,'DD/MM/YYYY') as FECHA_PERIODO_FIN"   +
						   " 	  ,TO_CHAR(EP.df_probable_pago,'DD/MM/YYYY') as FECHA_PROB_PAGO"   +
						   " 	  ,TO_CHAR(EP.df_deposito,'DD/MM/YYYY') as FECHA_DEPOSITO"   +
						   " 	  ,TO_CHAR(EP.df_registro,'DD/MM/YYYY') as FECHA_REGISTRO"   +
						   " 	  ,EP.ig_importe_deposito as IMPORTE_DEPOSITO"   +
						   " 	  ,F.cd_nombre as NOMBRE_BANCO"   +
						   " 	  ,RI.cg_descripcion as REFERENCIA_INTER"   +
						   " 	  ,EP.cg_referencia_banco as REFERENCIA_BANCO"   +
							
							" 	  ,d.ic_encabezado_pago" +
							"	  ,d.ig_subaplicacion as SUBAPLICACION" +
							"	  ,d.ig_prestamo as PRESTAMO"	+
							"	  ,d.ig_cliente as CLIENTE_SIRAC" +
							"    ,d.fg_amortizacion as TOTAL_AMORTIZACION" + 
							" 	  ,d.fg_interes as INTERES"	+ 
							"	  ,d.fg_subsidio as SUBSIDIO"	+
							"	  ,d.fg_comision as COMISION" +
							"	  ,d.fg_iva as IVA" +
							"	  ,d.fg_totalexigible as TOTAL_EXIGIBLE"	+
							"    ,DECODE (d.cg_concepto_pago,'AN', 'Anticipado','VE','Vencimiento','CV', 'Cartera Vencida','CO','Comision') AS CONCEPTO_PAGO"   +
							//"	  ,d.cg_concepto_pago as CONCEPTO_PAGO"	+
							"    ,DECODE (d.cg_origen_pago,'I', 'Intermediario','A','Acreditado') AS ORIGEN_PAGO"   +
							//"	  ,d.cg_origen_pago as ORIGEN_PAGO"		+
							"	  ,d.fg_interes_mora as INTERES_MORA"	+
							"	  ,d.ig_sucursal as SUCURSAL" +
							"	  ,d.cg_acreditado as ACREDITADO " +
							"	  ,TO_CHAR(d.df_operacion,'DD/MM/YYYY') as FECHA_OPERACION" +
							"	  ,TO_CHAR(d.df_vencimiento,'DD/MM/YYYY') as FECHA_VENCIMIENTO" +
							"    ,TO_CHAR(d.df_pago,'DD/MM/YYYY') as FECHA_PAGO" +
							"	  ,d.fn_saldo_insoluto as SALDO_INSOLUTO" +
							"	  ,d.ig_tasa as TASA" +
							"	  ,d.ig_dias as DIAS" +
	  
						   " FROM com_encabezado_pago EP"   +
							"	  ,com_detalle_pago d" +
						   " 	  ,comcat_financiera F"   +
						   " 	  ,comcat_referencia_inter RI"   +
						   " WHERE EP.ic_financiera = F.ic_financiera"   +
							"		AND EP.ic_encabezado_pago = d.ic_encabezado_pago(+)"	+
						   " 	 	AND EP.ic_referencia_inter = RI.ic_referencia_inter"   +
						   "     AND EP.ic_if = ?"   +
							
							condicion +
			
						   " ORDER BY"   +  sOrden);
				
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditionssususus "+conditions);
		log.info("getDocumentQueryFile(S)"); //
		return qrySentencia.toString();	
	} 
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo)
	{	
		
		return  "";
	}
	
	/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) sin paginador*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet reg, String path, String tipo) {
		String nombreArchivo = "";
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
				/*Imprime Encabezados*/
				if(sTipoBanco.equals("NB")) 
				{
					pdfDoc.setTable(13, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
					
					pdfDoc.setCell("E","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Clave del I.F.","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Clave direcci�n estatal","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Clave moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Banco de servicio","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha probable de pago","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de dep�sito","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe de dep�sito","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Referencia Banco","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia Intermediario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER,2);
				
					pdfDoc.setCell("D","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Subaplicaci�n","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Pr�stamo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Clave SIRAC Cliente","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Capital","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Inter�s","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moratorios","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Subsidio","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Comisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("IVA","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe Pago","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Concepto Pago","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Origen Pago","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER,13);
				}
				
				else if(sTipoBanco.equals("B")) 
				{
					pdfDoc.setTable(16, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
					
					pdfDoc.setCell("E","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Clave del I.F.","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Clave direcci�n estatal IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Clave moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Banco de servicio","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha probable de pago","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de dep�sito","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe de dep�sito","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Referencia Banco","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia Intermediario","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER,6);
				
					pdfDoc.setCell("D","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Sucursal","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("Subaplicaci�n","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Pr�stamo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Acreditado","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Operacion","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Pago","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Saldo Insoluto","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Capital","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("D�as","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Inter�s","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Comisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("IVA","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe Pago","celda01",ComunesPDF.CENTER);	//16
					pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
					
				}
				/*Fin Imprime Encabezados*/
				
				
				/////////////////////////////
				HashMap hmap =  new HashMap(); ///
				String temp = "", temp1 = ""; int total_regis = 0;
				while (reg.next())
				{
					String ic_encabezado =  (reg.getString("CVE_ENCABEZADO_PAGO") == null ? "" : reg.getString("CVE_ENCABEZADO_PAGO"));
					String importe_dep=(reg.getString("IMPORTE_DEPOSITO")== null) ? "": reg.getString("IMPORTE_DEPOSITO");
					String subapli =(reg.getString("SUBAPLICACION") == null) ? "" 	  : reg.getString("SUBAPLICACION");
					
					/////////////////////////////////////
					////			ENCABEZADOS		   	////
					/////////////////////////////////////
					
					if( !temp.equals(ic_encabezado))
					{
						if(sTipoBanco.equals("NB")) 
						{								
							String tip = "E";
							String clave_if  = (reg.getString("CVE_IF") == null) ? "" 				: reg.getString("CVE_IF");
							String clave_dir = (reg.getString("CVE_SUCURSAL") == null) ? "" 		: reg.getString("CVE_SUCURSAL");
							String clave_mon = (reg.getString("CVE_MONEDA") == null) ? ""			: reg.getString("CVE_MONEDA");
							String bco_ser   = (reg.getString("NOMBRE_BANCO") == null) ? "" 		: reg.getString("NOMBRE_BANCO");
							String fecha_ven = (reg.getString("FECHA_PERIODO_FIN") == null) ? "" : reg.getString("FECHA_PERIODO_FIN").trim();
							String fecha_pro = (reg.getString("FECHA_PROB_PAGO") == null) ? "" 	: reg.getString("FECHA_PROB_PAGO").trim();
							String fecha_dep = (reg.getString("FECHA_DEPOSITO") == null) ? "" 	: reg.getString("FECHA_DEPOSITO").trim();
							String refer_int = (reg.getString("REFERENCIA_INTER") == null) ? "" 	: reg.getString("REFERENCIA_INTER");
							String refer_bco = (reg.getString("REFERENCIA_BANCO") == null) ? "" 	: reg.getString("REFERENCIA_BANCO").trim();
		
							pdfDoc.setCell(tip,"celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(clave_if,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(clave_dir,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(clave_mon,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(bco_ser,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fecha_ven,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fecha_pro,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fecha_dep,"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(importe_dep,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(refer_bco,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(refer_int,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						}
						else if(sTipoBanco.equals("B")) 
						{	
							String tip = "E";
							String clave_if  = (reg.getString("CVE_IF") == null) ? "" 				: reg.getString("CVE_IF");
							String clave_dir = (reg.getString("CVE_SUCURSAL") == null) ? "" 		: reg.getString("CVE_SUCURSAL");
							String clave_mon = (reg.getString("CVE_MONEDA") == null) ? ""			: reg.getString("CVE_MONEDA");
							String fecha_pro = (reg.getString("FECHA_PROB_PAGO") == null) ? "" 	: reg.getString("FECHA_PROB_PAGO").trim();
							String fecha_dep = (reg.getString("FECHA_DEPOSITO") == null) ? "" 	: reg.getString("FECHA_DEPOSITO").trim();
							String bco_ser   = (reg.getString("NOMBRE_BANCO") == null) ? "" 		: reg.getString("NOMBRE_BANCO");
							String refer_int = (reg.getString("REFERENCIA_INTER") == null) ? "" 	: reg.getString("REFERENCIA_INTER");
							String refer_bco = (reg.getString("REFERENCIA_BANCO") == null) ? "" 	: reg.getString("REFERENCIA_BANCO").trim();
		
							pdfDoc.setCell(tip,"celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(clave_if,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(clave_dir,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(clave_mon,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(bco_ser,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fecha_pro,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fecha_dep,"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(importe_dep,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(refer_bco,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(refer_int,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
						}
						
						temp = ic_encabezado;
					}			
					
					/////////////////////////////////////
					////				DETALLES				////
					/////////////////////////////////////
					
					if( subapli != null && !"".equals(subapli))
					{
						if(sTipoBanco.equals("NB")) 
						{
							String tip1 = "D";
							String prestamo=(reg.getString("PRESTAMO") == null) ? ""			  : reg.getString("PRESTAMO");
							String clave_sir=(reg.getString("CLIENTE_SIRAC") == null) ? ""   : reg.getString("CLIENTE_SIRAC");
							String capital = (reg.getString("TOTAL_AMORTIZACION") == null) ? "" 	: reg.getString("TOTAL_AMORTIZACION");
							String interes =(reg.getString("INTERES") == null) ? ""					: reg.getString("INTERES");
							String mora =(reg.getString("INTERES_MORA") == null) ? "" 				: reg.getString("INTERES_MORA");
							String subsidio =(reg.getString("SUBSIDIO") == null) ? "" 				: reg.getString("SUBSIDIO");
							String comision =(reg.getString("COMISION") == null) ? "" 				: reg.getString("COMISION");
							String iva =(reg.getString("IVA") == null) ? "" 							: reg.getString("IVA");
							String importe_pago =(reg.getString("TOTAL_EXIGIBLE") == null) ? "" 	: reg.getString("TOTAL_EXIGIBLE");
							String concep_pago=(reg.getString("CONCEPTO_PAGO") == null) ? "" 		: reg.getString("CONCEPTO_PAGO");
							String origen_pago=(reg.getString("ORIGEN_PAGO") == null) ? "" 		: reg.getString("ORIGEN_PAGO");
							
							pdfDoc.setCell(tip1,"celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(subapli,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(prestamo,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(clave_sir,"formas",ComunesPDF.CENTER);			
							pdfDoc.setCell("$"+Comunes.formatoDecimal(capital,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(interes,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(mora,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(subsidio,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(comision,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(iva,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(importe_pago,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(concep_pago,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(origen_pago,"formas",ComunesPDF.CENTER);
						}
						
						else if(sTipoBanco.equals("B")) 
						{		
							String tip1 = "D";///////////////AQUI!!!
							String sucursal 	= (reg.getString("SUCURSAL") == null) ? "" 				: reg.getString("SUCURSAL").trim();
							String prestamo=(reg.getString("PRESTAMO") == null) ? ""			  			: reg.getString("PRESTAMO");
							String acredita 	= (reg.getString("ACREDITADO") == null) ? "" 			: reg.getString("ACREDITADO").trim();
							String fechaOper 	= (reg.getString("FECHA_OPERACION") == null) ? "" 		: reg.getString("FECHA_OPERACION").trim();
							String fechaVenc 	= (reg.getString("FECHA_VENCIMIENTO") == null) ? "" 	: reg.getString("FECHA_VENCIMIENTO").trim();
							String fechaPago 	= (reg.getString("FECHA_PAGO") == null) ? "" 			: reg.getString("FECHA_PAGO").trim();
							String saldo 		= (reg.getString("SALDO_INSOLUTO") == null) ? ""		: reg.getString("SALDO_INSOLUTO").trim();
							String tasa 		= (reg.getString("TASA") == null) ? "0" 					: reg.getString("TASA").trim();
							String capital = (reg.getString("TOTAL_AMORTIZACION") == null) ? "" 		: reg.getString("TOTAL_AMORTIZACION");
							String dias 		= (reg.getString("DIAS") == null) ? "" 					: reg.getString("DIAS").trim();
							String interes =(reg.getString("INTERES") == null) ? ""					: reg.getString("INTERES");
							String comision =(reg.getString("COMISION") == null) ? "" 				: reg.getString("COMISION");
							String iva =(reg.getString("IVA") == null) ? "" 							: reg.getString("IVA");
							String importe_pago =(reg.getString("TOTAL_EXIGIBLE") == null) ? "" 	: reg.getString("TOTAL_EXIGIBLE");
							
							pdfDoc.setCell(tip1,"celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(sucursal,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(subapli,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(prestamo,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(acredita,"formas",ComunesPDF.CENTER);		
							pdfDoc.setCell(fechaOper,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaVenc,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaPago,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(saldo,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(tasa,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(capital,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(dias,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(interes,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(comision,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(iva,2) ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(importe_pago,2) ,"formas",ComunesPDF.CENTER);
							
						}	
						total_regis++;
					}
					hmap.put(ic_encabezado,importe_dep);
					
				}
				//Para agregar Totales!
				
				double total_importe = 0;
				
				Iterator it = hmap.entrySet().iterator();
				String aux = "";
				while(it.hasNext())
				{
					
					Map.Entry e = (Map.Entry)it.next();
					aux = e.getValue().toString();
					total_importe +=  Double.parseDouble(aux);
				}
							
				pdfDoc.setCell("Total Pagos","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell(String.valueOf(total_regis),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(total_importe,2) ,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
				/*
				 * para crear los encabezados de otra manera
				 * List lEncabezados = new ArrayList();
				 * lEncabezados.add("");
				 * pdfDoc.setTable(lEncabezados, "formasmenB", 100);
				 * */
				
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		}
		
		else if("CSV".equals(tipo))
		{
				String linea = "";
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				StringBuffer 	contenidoArchivo 	= new StringBuffer();
				CreaArchivo 	archivo 			= new CreaArchivo();
				String temp = "", temp1 = "";
					
				try 
				{
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
					writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
					buffer = new BufferedWriter(writer);
					buffer.write(linea);
					
					//obtiene el tipo de banco
					
					//Encabezado segun el tipo de Banco
					if(sTipoBanco.equals("NB")) 
					{	// Si es IF no Bancario.
						linea="E,Clave del I.F.,Clave direcci�n estatal,Clave moneda,Banco de servicio,Fecha de vencimiento, Fecha probable de pago, "+
						"Fecha de dep�sito,Importe de dep�sito,Referencia Banco,Referencia Intermediario, " +"\n"+
						"D,Subaplicaci�n,Pr�stamo,Clave SIRAC Cliente,Capital,Intereses,Moratorios,Subsidio,Comisi�n,IVA,Importe Pago,Concepto Pago,Origen Pago" +"\n";
					} 
					else if(sTipoBanco.equals("B")) // IF Bancario.
					{	
						linea="E,Clave del I.F.,Clave direcci�n estatal IF,Clave moneda,Banco de servicio,"+
						"Fecha probable de pago,Fecha de dep�sito,Importe de dep�sito,Referencia Banco,Referencia Intermediario, " +"\n"+
						"D,Sucursal,Subaplicaci�n,Pr�stamo, Acreditado, Fecha Operacion, Fecha Vencimiento, Fecha Pago, Saldo Insoluto, " +
						"Tasa, Capital, D�as, Inter�s, Comisi�n, IVA,Importe Pago" +"\n";
					}
									
					buffer.write(linea);
					
					while (reg.next()) 
					{
						String ic_encabezado =  (reg.getString("CVE_ENCABEZADO_PAGO") == null ? "" : reg.getString("CVE_ENCABEZADO_PAGO"));
						String importe_dep=(reg.getString("IMPORTE_DEPOSITO")== null) ? "": reg.getString("IMPORTE_DEPOSITO");
						String subapli =(reg.getString("SUBAPLICACION") == null) ? "" 	  : reg.getString("SUBAPLICACION");
					
						if( !temp.equals(ic_encabezado))
						{	
							String tip = "E";
							String clave_if  = (reg.getString("CVE_IF") == null) ? "" 				: reg.getString("CVE_IF");
							String clave_dir = (reg.getString("CVE_SUCURSAL") == null) ? "" 		: reg.getString("CVE_SUCURSAL");
							String clave_mon = (reg.getString("CVE_MONEDA") == null) ? ""			: reg.getString("CVE_MONEDA");
							String fecha_ven = (reg.getString("FECHA_PERIODO_FIN") == null) ? "" : reg.getString("FECHA_PERIODO_FIN").trim();
							String fecha_pro = (reg.getString("FECHA_PROB_PAGO") == null) ? "" 	: reg.getString("FECHA_PROB_PAGO").trim();
							String fecha_dep = (reg.getString("FECHA_DEPOSITO") == null) ? "" 	: reg.getString("FECHA_DEPOSITO").trim();
        					String bco_ser   = (reg.getString("NOMBRE_BANCO") == null) ? "" 		: reg.getString("NOMBRE_BANCO");
							String refer_int = (reg.getString("REFERENCIA_INTER") == null) ? "" 	: reg.getString("REFERENCIA_INTER");
							String refer_bco = (reg.getString("REFERENCIA_BANCO") == null) ? "" 	: reg.getString("REFERENCIA_BANCO").trim();
		
							temp = ic_encabezado;
							
							if(sTipoBanco.equals("NB"))
							{
								linea =	tip+","+ clave_if.replaceAll(",", "")+","+clave_dir.replaceAll(",", "")+","+clave_mon+","+bco_ser.replaceAll(",", "")+","
									+fecha_ven+","+fecha_pro+","+fecha_dep+","+Comunes.formatoDecimal(importe_dep,2,false)+","+refer_bco.replaceAll(",", "")+","+refer_int.replaceAll(",", "")+"\n";
							}
							else if(sTipoBanco.equals("B"))
							{
								linea =	tip+","+ clave_if.replaceAll(",", "")+","+clave_dir.replaceAll(",", "")+","+clave_mon+","+bco_ser.replaceAll(",", "")+","+fecha_pro+","+
										fecha_dep+","+Comunes.formatoDecimal(importe_dep,2,false)+","+refer_bco.replaceAll(",", "")+","+refer_int.replaceAll(",", "")+"\n";
							}		
									
							buffer.write(linea);
						}
						
						
						if( subapli != null && !"".equals(subapli))
						{		
							String tip1 = "D";

							String prestamo	= (reg.getString("PRESTAMO") == null) ? ""			  	: reg.getString("PRESTAMO");
							String clavesirac = (reg.getString("CLIENTE_SIRAC") == null) ? "" 		: reg.getString("CLIENTE_SIRAC").trim();
							String capital 	= (reg.getString("TOTAL_AMORTIZACION") == null) ? "" 	: reg.getString("TOTAL_AMORTIZACION");
							String interes 	= (reg.getString("INTERES") == null) ? ""					: reg.getString("INTERES");
							String subsidio   = (reg.getString("SUBSIDIO") == null) ? "" 				: reg.getString("SUBSIDIO").trim();
							String comision 	=(reg.getString("COMISION") == null) ? "" 				: reg.getString("COMISION");
							String iva 			=(reg.getString("IVA") == null) ? "" 						: reg.getString("IVA");
							String imp_pago 	=(reg.getString("TOTAL_EXIGIBLE") == null) ? "" 		: reg.getString("TOTAL_EXIGIBLE");
							String concepto   = (reg.getString("CONCEPTO_PAGO") == null) ? "" 		: reg.getString("CONCEPTO_PAGO").trim();
							String origen     = (reg.getString("ORIGEN_PAGO") == null) ? "" 			: reg.getString("ORIGEN_PAGO").trim();
							String mora 		= (reg.getString("INTERES_MORA") == null) ? "" 				: reg.getString("INTERES_MORA").trim();
							String sucursal 	= (reg.getString("SUCURSAL") == null) ? "" 				: reg.getString("SUCURSAL").trim();
							String acredita 	= (reg.getString("ACREDITADO") == null) ? "" 			: reg.getString("ACREDITADO").trim();
							String fechaOper 	= (reg.getString("FECHA_OPERACION") == null) ? "" 		: reg.getString("FECHA_OPERACION").trim();
							String fechaVenc 	= (reg.getString("FECHA_VENCIMIENTO") == null) ? "" 	: reg.getString("FECHA_VENCIMIENTO").trim();
							String fechaPago 	= (reg.getString("FECHA_PAGO") == null) ? "" 			: reg.getString("FECHA_PAGO").trim();
							String saldo 		= (reg.getString("SALDO_INSOLUTO") == null) ? ""		: reg.getString("SALDO_INSOLUTO").trim();
							String tasa 		= (reg.getString("TASA") == null) ? "0" 					: reg.getString("TASA").trim();
							String dias 		= (reg.getString("DIAS") == null) ? "" 					: reg.getString("DIAS").trim();
							
							
							if(sTipoBanco.equals("NB")) 
							{						
								linea =	tip1+","+ subapli + "," + prestamo + "," + clavesirac.replace(',',' ') + "," + Comunes.formatoDecimal(capital,2,false) + "," 
								+ Comunes.formatoDecimal(interes,2,false) +","+ Comunes.formatoDecimal(mora,2,false) + "," + Comunes.formatoDecimal(subsidio,2,false)+ "," 
								+Comunes.formatoDecimal(comision,2,false)+","+Comunes.formatoDecimal(iva,2,false)+","+Comunes.formatoDecimal(imp_pago,2,false)+ "," 
								+ concepto.replace(',',' ') + ","+ origen.replace(',',' ') +"\n";
							}
							else if(sTipoBanco.equals("B")) 
							{
								linea =	tip1+","+ sucursal + "," +subapli+","+prestamo+","+acredita.replace(',',' ')+","+fechaOper+","+fechaVenc+","+fechaPago+","
										+Comunes.formatoDecimal(saldo,2,false)+","+tasa+" %,"+ Comunes.formatoDecimal(capital,2,false)+","+dias+","
										+Comunes.formatoDecimal(interes,2,false)+","+Comunes.formatoDecimal(comision,2,false)+","+Comunes.formatoDecimal(iva,2,false)+","
										+Comunes.formatoDecimal(imp_pago,2,false)+"\n";
							}
							
							buffer.write(linea);
						}
					}
					
					buffer.close();
				} catch (Throwable e) 
				{
					throw new AppException("Error al generar el archivo ", e);
				} 
		}
		
			return  nombreArchivo;
	}
	
	 
	
/*****************************************************
					 SETTERS         
*******************************************************/
	
	public void setsOrden(String sorden) {
		this.sOrden = sorden;
	}
	public void setsFechaDel(String fechadel) {
		this.sFechaDel = fechadel;
	}

	public void setssFechaAl(String fechaal) {
		this.sFechaAl = fechaal;
	}
	
	public void setsFechaVencIni(String fchvenc1) {
		this.sFechaVencIni = fchvenc1;
	}
	
	public void setsFechaVencFin(String fchvenc2) {
		this.sFechaVencFin = fchvenc2;
	}
	
	public void setsFechaProIni(String fchpro1) {
		this.sFechaProIni = fchpro1;
	}
	
	public void setsFechaProFin(String fchpro2) {
		this.sFechaProFin = fchpro2;
	}
	
	public void setsFechaPagoIni(String fchpag1) {
		this.sFechaPagoIni = fchpag1;
	}
	
	public void setsFechaPagoFin(String fchpag2) {
		this.sFechaPagoFin = fchpag2;
	}
	
	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}
	
	public void setsTipoBanco(String sTipoBanco) {
		this.sTipoBanco = sTipoBanco;
	}
	/*****************************************************
					 GETTERS
	*******************************************************/
	public String getClaveIF() {
		return claveIF;
	}

	public String getsOrden() {
		return sOrden;
	}
	
	public String getsFechaDel() {
		return sFechaDel;
	}

	public String getssFechaAl() {
		return sFechaAl;
	}
	
	public String getsFechaVencIni() {
		return sFechaVencIni;
	}
	
	public String getsFechaVencFin() {
		return sFechaVencFin;
	}
	
	public String getsFechaProIni() {
		return sFechaProIni;
	}
	
	public String getsFechaProFin() {
		return sFechaProFin ;
	}
	
	public String getsFechaPagoIni() {
		return sFechaPagoIni;
	}
	
	public String getsFechaPagoFin() {
		return sFechaPagoFin;
	}
	
	public String getsTipoBanco() {
		return sTipoBanco;
	}
	
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	

	

}