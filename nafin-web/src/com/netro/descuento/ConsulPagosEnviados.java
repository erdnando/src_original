package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsulPagosEnviados implements  IQueryGeneratorRegExtJS {
	public ConsulPagosEnviados() {  }
	private List 	conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	StringBuffer 	strQuery;
	StringBuffer qrySentencia = new StringBuffer("");
	private static final Log log = ServiceLocator.getInstance().getLog(ConsulPagosEnviados.class);//Variable para enviar mensajes al log.
	private String fechaVencimientoIni;
	private String fechaVencimientoFin;
	private String fechaProbaPagoIni;
	private String fechaProbaPagoFin;
	private String fechaPagoIni;
	private String fechaPagoFin;
	private String fechaRegistroIni;
	private String fechaRegistroFin;
	private String numIntermediario;
	private String ic_if;
	private String sOpcion;
	private String sEncabezadoPago;
	private String cs_tipo;
	
	
	public String getAggregateCalculationQuery() {   
		log.info("getAggregateCalculationQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();

		
		return qrySentencia.toString();	
 	}  
	
		 
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();   
		strQuery 		= new StringBuffer(); 
		String sOrden = "";
		StringBuffer condicion    = new StringBuffer();
		log.info("getDocumentQueryFile(E)");
			if(sOpcion.equals("C3")){
				strQuery.append(
					" SELECT "   +
	            "      /*+"	+
               "      use_nl(EP,F,RI)"	+
               "      index(EP CP_COM_ENCABEZADO_PAGO_PK)"	+
               "      */"	+
	            "       EP.IC_ENCABEZADO_PAGO"   +
				   " 	  ,I.ic_financiera"   +
				   " 	  ,I.ic_of_controladora"   +
				   " 	  ,EP.IC_MONEDA"   +
				   " 	  ,TO_CHAR(EP.DF_PERIODO_FIN,'DD/MM/YYYY') AS DF_PERIODO_FIN"   +
				   " 	  ,TO_CHAR(EP.DF_PROBABLE_PAGO,'DD/MM/YYYY') AS DF_PROBABLE_PAGO"   +
				   " 	  ,TO_CHAR(EP.DF_DEPOSITO,'DD/MM/YYYY') AS DF_DEPOSITO "   +
				   " 	  ,TO_CHAR(EP.DF_REGISTRO,'DD/MM/YYYY') AS DF_REGISTRO"   +
				   " 	  ,EP.IG_IMPORTE_DEPOSITO"   +
				   " 	  ,EP.IC_FINANCIERA CVE_BANCO"   +
				   " 	  ,F.CD_NOMBRE BANCO"   +
				   " 	  ,EP.IC_REFERENCIA_INTER CVE_REFERENCIA_INTER"   +
				   " 	  ,RI.CG_DESCRIPCION REFERENCIA_INTER"   +
				   " 	  ,EP.CG_REFERENCIA_BANCO"   +
				   " FROM"   +
				   "       COM_ENCABEZADO_PAGO EP"   +
				   "	  ,COMCAT_IF I"+
				   " 	  ,COMCAT_FINANCIERA F"   +
				   " 	  ,COMCAT_REFERENCIA_INTER RI"   +
				   " WHERE "   +
				   "       EP.IC_FINANCIERA=F.IC_FINANCIERA"   +
				   "	   AND EP.IC_IF = I.IC_IF"+
				   " 	   AND EP.IC_REFERENCIA_INTER=RI.IC_REFERENCIA_INTER"   +
				   "       AND EP.IC_ENCABEZADO_PAGO ="+sEncabezadoPago);
			}else if(sOpcion.equals("C2")){
				strQuery.append(
					 " SELECT "   +
		          "       IC_DETALLE_PAGO"   +
					 " 	  ,IC_ENCABEZADO_PAGO"   +
					 " 	  ,IG_SUBAPLICACION"   +
					 " 	  ,IG_PRESTAMO"   +
					 " 	  ,IG_CLIENTE"   +
					 " 	  ,FG_AMORTIZACION"   +
					 " 	  ,FG_INTERES"   +
					 " 	  ,FG_SUBSIDIO"   +
					 " 	  ,FG_COMISION"   +
					 " 	  ,FG_IVA"   +
					 " 	  ,FG_TOTALEXIGIBLE"   +
					 " 	  ,DECODE(cg_concepto_pago, 'AN', 'Anticipado', 'VE','Vencimiento','CV','Cartera Vencida','CO','Comision') AS cg_concepto_pago"   +
					 " 	  ,DECODE(cg_origen_pago, 'I', 'Intermediario', 'A','Acreditado') AS cg_origen_pago"   +
					 " 	  ,FG_INTERES_MORA"   +
					 " 	  ,IG_SUCURSAL"   +
					 " 	  ,CG_ACREDITADO"   +
					 " 	  ,to_char(DF_OPERACION,'dd/mm/yyy') AS DF_OPERACION"   +
					 " 	  ,to_char(DF_VENCIMIENTO,'dd/mm/yyy') AS DF_VENCIMIENTO"   +
					 " 	  ,to_char(DF_PAGO,'dd/mm/yyy') AS DF_PAGO"   +
					 " 	  ,FN_SALDO_INSOLUTO"   +
					 " 	  ,IG_TASA"   +
					 " 	  ,IG_DIAS"   +
					 " FROM"   +
					 "       COM_DETALLE_PAGO"   +
					 " WHERE"   +
					 "       IC_ENCABEZADO_PAGO = "+sEncabezadoPago+
					 " ORDER BY"   +
					 "       IC_DETALLE_PAGO	");
			}else if(!sOpcion.equals("C2") && !sOpcion.equals("C3")){ 
			if(!"".equals(ic_if)){
				condicion.append(" AND E.ic_if = ? ");
				conditions.add(ic_if);
			}
			if(!"".equals(numIntermediario)){
   			condicion.append(" AND I.ic_financiera = ? ");
				conditions.add(numIntermediario);
			}
			if(!fechaVencimientoIni.equals("") && !fechaVencimientoFin.equals("")){
				condicion.append(" AND TRUNC(DF_PERIODO_FIN) >= to_date(?,'dd/mm/yyyy') "   +
					   " AND TRUNC(DF_PERIODO_FIN) <= to_date(?,'dd/mm/yyyy') ");
				sOrden=" DF_PERIODO_FIN ";
				conditions.add(fechaVencimientoIni);
				conditions.add(fechaVencimientoFin);
			}else if(!fechaProbaPagoIni.equals("") && !fechaProbaPagoFin.equals("")){//BUSQUEDA POR FECHA PROBABLE DE PAGO
				condicion.append(" AND TRUNC(DF_PROBABLE_PAGO) >= to_date(?,'dd/mm/yyyy') "   +
					   " AND TRUNC(DF_PROBABLE_PAGO) <= to_date(?,'dd/mm/yyyy') ");
				sOrden=" DF_PROBABLE_PAGO ";
				conditions.add(fechaProbaPagoIni);
				conditions.add(fechaProbaPagoFin);
			}else if(!fechaPagoIni.equals("") && !fechaPagoFin.equals("")){//BUSQUEDA POR FECHA PAGO
				condicion.append(" AND TRUNC(DF_DEPOSITO) >= to_date(?,'dd/mm/yyyy') "   +
					   " AND TRUNC(DF_DEPOSITO) <= to_date(?,'dd/mm/yyyy') ");
				sOrden=" DF_DEPOSITO ";
				conditions.add(fechaPagoIni);
				conditions.add(fechaPagoFin);
			}else if(!fechaRegistroIni.equals("") && !fechaRegistroFin.equals("")){//BUSQUEDA POR FECHA REGISTRO
				condicion.append(" AND TRUNC(DF_REGISTRO) >= to_date(?,'dd/mm/yyyy') "   +
					   " AND TRUNC(DF_REGISTRO) <= to_date(?,'dd/mm/yyyy') ");
				sOrden=" DF_REGISTRO ";
				conditions.add(fechaRegistroIni);
				conditions.add(fechaRegistroFin);
			}else{
				sOrden=" DF_REGISTRO ";
			}
			strQuery.append(" select "   +
		             "       /*+"   +
						"       use_nl(E)"   +
						"       index(E CP_COM_ENCABEZADO_PAGO_PK)"   +
						"       */"   +
						" 	   E.IC_ENCABEZADO_PAGO"   +
						" 	   ,I.IC_FINANCIERA"   +
						" 	   ,E.IC_MONEDA"   +
						" 	   ,TO_CHAR(E.DF_PERIODO_FIN,'DD/MM/YYYY') FECHA_VENCIMIENTO"   +
						" 	   ,TO_CHAR(E.DF_PROBABLE_PAGO,'DD/MM/YYYY') FECHA_PROB"   +
						" 	   ,TO_CHAR(E.DF_DEPOSITO,'DD/MM/YYYY') FECHA_PAGO"   +
						" 	   ,TO_CHAR(E.DF_REGISTRO,'DD/MM/YYYY') FECHA_REG"   +
						" 	   ,E.CG_NOMBRE_ARCHIVO       "   +
						"  from"   +
						"       com_encabezado_pago E"   +
						"		,comcat_if i"+
						"  where E.CG_NOMBRE_ARCHIVO is not null "	+
						"		AND E.ic_if = I.ic_if "+ condicion.toString()+
						" ORDER BY"   +
				  		sOrden);
			}
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+sEncabezadoPago);
		log.info("getDocumentQueryFile(S)");
		return strQuery.toString();
		
 	} 
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs2, String path, String tipo) {
		String nombreArchivo = "";
		log.debug("crearCustomFile(E) cadena de ruta del archivo generado ");
		HttpSession session = request.getSession();
		int registros=0;
		int iSubRegistros=0;
		int numColEnc = 0;
		ComunesPDF pdfDoc = new ComunesPDF();;
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();	
			double dImporteTotal=0;
			String cadAux = "";
		if(!sOpcion.equals("C2")){
	
			try{
				if (rs2.next()) {  
					String sClaveEncabezado_E  = 	rs2.getString("IC_ENCABEZADO_PAGO")==null?"":rs2.getString("IC_ENCABEZADO_PAGO");//1
					String sClaveIF_E          =		rs2.getString("IC_FINANCIERA")==null?"":rs2.getString("IC_FINANCIERA");//2
					String sClaveDirEstatal_E  =		rs2.getString("IC_OF_CONTROLADORA")==null?"":rs2.getString("IC_OF_CONTROLADORA");//3
					String sClaveMoneda_E      =		rs2.getString("IC_MONEDA")==null?"":rs2.getString("IC_MONEDA");//4
					String sFechaVencimiento_E =		rs2.getString("DF_PERIODO_FIN")==null?"":rs2.getString("DF_PERIODO_FIN");//5
					String sFechaProbPago_E    =		rs2.getString("DF_PROBABLE_PAGO")==null?"":rs2.getString("DF_PROBABLE_PAGO");//6
					String sFechaDeposito_E    =		rs2.getString("DF_DEPOSITO")==null?"":rs2.getString("DF_DEPOSITO");//7
					String sImporteDeposito_E  =		rs2.getString("IG_IMPORTE_DEPOSITO")==null?"":rs2.getString("IG_IMPORTE_DEPOSITO");//9
					String sBancoServicio_E    =		rs2.getString("BANCO")==null?"":rs2.getString("BANCO");//11
					String sReferenciaInter_E  =		rs2.getString("REFERENCIA_INTER")==null?"":rs2.getString("REFERENCIA_INTER");//13
					String sReferenciaBanco_E  = 	rs2.getString("CG_REFERENCIA_BANCO")==null?"":rs2.getString("CG_REFERENCIA_BANCO");//14
					if(tipo.equals("CSV")){
						if("NB".equals(cs_tipo)) {
							contenidoArchivo.append("E,Clave del I.F.,Clave direcci�n estatal,Clave moneda,Banco de servicio,Fecha de vencimiento,Fecha probable de pago,Fecha de dep�sito,Importe de dep�sito,Referencia Banco,Referencia Intermediario,,,\n");
							if(!sOpcion.equals("C3")){
								contenidoArchivo.append("D,Subaplicaci�n,Pr�stamo,Clave SIRAC Cliente,Capital,Intereses,Moratorios,Subsidio,Comisi�n,IVA,Importe Pago,Concepto Pago,Origen Pago,\n");
							}
						} else if("B".equals(cs_tipo)) {
							contenidoArchivo.append("E,Clave del I.F.,Clave direcci�n estatal,Clave moneda,Banco de servicio,Fecha probable de pago,Fecha de dep�sito,Importe de dep�sito,Referencia Banco,Referencia Intermediario,,,\n");
							if(!sOpcion.equals("C3")){
								contenidoArchivo.append("D,Sucursal,Subaplicaci�n,Pr�stamo,Acreditado,Fecha Operaci�n,Fecha Vencimiento,Fecha Pago,Saldo Insoluto,Tasa,Capital,D�as,Intereses,Comisi�n,IVA,Importe Pago,\n");
							}
						}
						if("NB".equals(cs_tipo)) {
							contenidoArchivo.append("E,"+sClaveIF_E.replace(',',' ')+","+sClaveDirEstatal_E.replace(',',' ')+","+sClaveMoneda_E+","+sBancoServicio_E.replace(',',' ')+","+sFechaVencimiento_E+","+sFechaProbPago_E+","+sFechaDeposito_E+","+Comunes.formatoDecimal(sImporteDeposito_E,2,false)+","+sReferenciaBanco_E.replace(',',' ')+","+sReferenciaInter_E.replace(',',' ')+",,,\n");
						} else if("B".equals(cs_tipo)) {
							contenidoArchivo.append("E,"+sClaveIF_E.replace(',',' ')+","+sClaveDirEstatal_E.replace(',',' ')+","+sClaveMoneda_E+","+sBancoServicio_E.replace(',',' ')+","+sFechaProbPago_E+","+sFechaDeposito_E+","+Comunes.formatoDecimal(sImporteDeposito_E,2,false)+","+sReferenciaBanco_E.replace(',',' ')+","+sReferenciaInter_E.replace(',',' ')+",,,\n");		
						}
					}
					if(tipo.equals("PDF")){
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
						pdfDoc = new ComunesPDF(2, path + nombreArchivo);
						pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
												 ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
												 (String)session.getAttribute("sesExterno"),
												 (String) session.getAttribute("strNombre"),
                                     (String) session.getAttribute("strNombreUsuario"),
												 (String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
						float anchos[]={2,8,8,8,8,8,8,8,8,8};
						numColEnc = 10;
						if("NB".equals(cs_tipo))
							numColEnc++;
						pdfDoc.setTable(numColEnc,100,anchos);
						pdfDoc.setCell("E","formasmenB",ComunesPDF.CENTER);		
						pdfDoc.setCell("Clave del I.F.","formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Clave direcci�n estatal","formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Clave moneda","formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Banco de servicio","formasmenB",ComunesPDF.CENTER);
						if("NB".equals(cs_tipo))
							pdfDoc.setCell("Fecha de vencimiento","formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha probable de pago","formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de dep�sito","formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Importe de dep�sito","formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Referencia Banco","formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Referencia Intermediario","formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,numColEnc);
						String men=(sOpcion.equals("C3")?"":"men");
						pdfDoc.setCell("E","formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell(sClaveIF_E,"formas"+men,ComunesPDF.LEFT);
						pdfDoc.setCell(sClaveDirEstatal_E,"formas"+men,ComunesPDF.LEFT);
						pdfDoc.setCell(sClaveMoneda_E,"formas"+men,ComunesPDF.LEFT);
						pdfDoc.setCell(sBancoServicio_E,"formas"+men,ComunesPDF.CENTER);
						if("NB".equals(cs_tipo))
							pdfDoc.setCell(sFechaVencimiento_E,"formas"+men,ComunesPDF.LEFT);
						pdfDoc.setCell(sFechaProbPago_E,"formas"+men,ComunesPDF.LEFT);
						pdfDoc.setCell(sFechaDeposito_E,"formas"+men,ComunesPDF.LEFT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sImporteDeposito_E,2),"formas"+men,ComunesPDF.RIGHT);
						pdfDoc.setCell(sReferenciaBanco_E,"formas"+men,ComunesPDF.LEFT);
						pdfDoc.setCell(sReferenciaInter_E,"formas"+men,ComunesPDF.LEFT);
				}
			}
			if(tipo.equals("CSV")){
					creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
					nombreArchivo = creaArchivo.getNombre();
			}
			if(tipo.equals("PDF")){
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			} catch(Exception e) {}
		}
	}else if(!sOpcion.equals("C3")){
		 HashMap  encabezado=	this.getEncabezado(sEncabezadoPago,sOpcion );
		 String sClaveEncabezado_E  = 	encabezado.get("IC_ENCABEZADO_PAGO")==null?"":encabezado.get("IC_ENCABEZADO_PAGO").toString();//1
		 String sClaveIF_E          =		encabezado.get("IC_FINANCIERA")==null?"":encabezado.get("IC_FINANCIERA").toString();//2
		 String sClaveDirEstatal_E  =		encabezado.get("IC_OF_CONTROLADORA")==null?"":encabezado.get("IC_OF_CONTROLADORA").toString();//3
		 String sClaveMoneda_E      =		encabezado.get("IC_MONEDA")==null?"":encabezado.get("IC_MONEDA").toString();//4
		 String sFechaVencimiento_E =		encabezado.get("DF_PERIODO_FIN")==null?"":encabezado.get("DF_PERIODO_FIN").toString();//5
		 String sFechaProbPago_E    =		encabezado.get("DF_PROBABLE_PAGO")==null?"":encabezado.get("DF_PROBABLE_PAGO").toString();//6
		 String sFechaDeposito_E    =		encabezado.get("DF_DEPOSITO")==null?"":encabezado.get("DF_DEPOSITO").toString();//7
		 String sImporteDeposito_E  =		encabezado.get("IG_IMPORTE_DEPOSITO")==null?"":encabezado.get("IG_IMPORTE_DEPOSITO").toString();//9
		 String sBancoServicio_E    =		encabezado.get("BANCO")==null?"":encabezado.get("BANCO").toString();//11
		 String sReferenciaInter_E  =		encabezado.get("REFERENCIA_INTER")==null?"":encabezado.get("REFERENCIA_INTER").toString();//13
		 String sReferenciaBanco_E  = 	encabezado.get("CG_REFERENCIA_BANCO")==null?"":encabezado.get("CG_REFERENCIA_BANCO").toString();//14
		 if(tipo.equals("CSV")){
			if("NB".equals(cs_tipo)) {
				contenidoArchivo.append("E,Clave del I.F.,Clave direcci�n estatal,Clave moneda,Banco de servicio,Fecha de vencimiento,Fecha probable de pago,Fecha de dep�sito,Importe de dep�sito,Referencia Banco,Referencia Intermediario,,,\n");
				if(!sOpcion.equals("C3")){
					contenidoArchivo.append("D,Subaplicaci�n,Pr�stamo,Clave SIRAC Cliente,Capital,Intereses,Moratorios,Subsidio,Comisi�n,IVA,Importe Pago,Concepto Pago,Origen Pago,\n");
				}
			 } else if("B".equals(cs_tipo)) {
				contenidoArchivo.append("E,Clave del I.F.,Clave direcci�n estatal,Clave moneda,Banco de servicio,Fecha probable de pago,Fecha de dep�sito,Importe de dep�sito,Referencia Banco,Referencia Intermediario,,,\n");
				if(!sOpcion.equals("C3")){
					contenidoArchivo.append("D,Sucursal,Subaplicaci�n,Pr�stamo,Acreditado,Fecha Operaci�n,Fecha Vencimiento,Fecha Pago,Saldo Insoluto,Tasa,Capital,D�as,Intereses,Comisi�n,IVA,Importe Pago,\n");
				}
			}
			if("NB".equals(cs_tipo)) {
				contenidoArchivo.append("E,"+sClaveIF_E.replace(',',' ')+","+sClaveDirEstatal_E.replace(',',' ')+","+sClaveMoneda_E+","+sBancoServicio_E.replace(',',' ')+","+sFechaVencimiento_E+","+sFechaProbPago_E+","+sFechaDeposito_E+","+Comunes.formatoDecimal(sImporteDeposito_E,2,false)+","+sReferenciaBanco_E.replace(',',' ')+","+sReferenciaInter_E.replace(',',' ')+",,,\n");
			} else if("B".equals(cs_tipo)) {
				contenidoArchivo.append("E,"+sClaveIF_E.replace(',',' ')+","+sClaveDirEstatal_E.replace(',',' ')+","+sClaveMoneda_E+","+sBancoServicio_E.replace(',',' ')+","+sFechaProbPago_E+","+sFechaDeposito_E+","+Comunes.formatoDecimal(sImporteDeposito_E,2,false)+","+sReferenciaBanco_E.replace(',',' ')+","+sReferenciaInter_E.replace(',',' ')+",,,\n");		
			}
		}else if(tipo.equals("PDF")){
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
												 ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
												 (String)session.getAttribute("sesExterno"),
												 (String) session.getAttribute("strNombre"),
                                     (String) session.getAttribute("strNombreUsuario"),
												 (String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

			float anchos[]={2,8,8,8,8,8,8,8,8,8,8,8,8};
			if(!sOpcion.equals("C3")){
				numColEnc = 13;
			}else{
				numColEnc = 11;
			}
			if("B".equals(cs_tipo)) {
				numColEnc = 16;			
				float anchosAux[]={2,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8};
				anchos = anchosAux;
			}
			pdfDoc.setTable(numColEnc,100,anchos);
			pdfDoc.setCell("E","formasmenB",ComunesPDF.CENTER);		
			pdfDoc.setCell("Clave del I.F.","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Clave direcci�n estatal","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Clave moneda","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Banco de servicio","formasmenB",ComunesPDF.CENTER);
			if("NB".equals(cs_tipo))
				pdfDoc.setCell("Fecha de vencimiento","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha probable de pago","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de dep�sito","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Importe de dep�sito","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Referencia Banco","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell("Referencia Intermediario","formasmenB",ComunesPDF.CENTER);
			if(!sOpcion.equals("C3")){
				pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
			}
			if("B".equals(cs_tipo)) {
				pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER);
			}
			if(!sOpcion.equals("C3")){
				pdfDoc.setCell("D","formasmenB",ComunesPDF.CENTER);
				if("NB".equals(cs_tipo)) {
					pdfDoc.setCell("Subaplicaci�n","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Pr�stamo","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Clave SIRAC Cliente","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Capital","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Intereses","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Moratorios","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Subsidio","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Comisi�n","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("IVA","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe Pago","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Concepto Pago","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Origen Pago","formasmenB",ComunesPDF.CENTER);
				} else if("B".equals(cs_tipo)) { 
					pdfDoc.setCell("Sucursal","formasmenB",ComunesPDF.CENTER);				
					pdfDoc.setCell("Subaplicaci�n","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Pr�stamo","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Acreditado","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operaci�n","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Vencimiento","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Pago","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Saldo Insoluto","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Capital","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Dias","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Intereses","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Comisi�n","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("IVA","formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe Pago","formasmenB",ComunesPDF.CENTER);
				}
			}
			pdfDoc.setCell("","formas",ComunesPDF.CENTER,numColEnc);
			String men=(sOpcion.equals("C3")?"":"men");
			pdfDoc.setCell("E","formasmenB",ComunesPDF.CENTER);
			pdfDoc.setCell(sClaveIF_E,"formas"+men,ComunesPDF.LEFT);
			pdfDoc.setCell(sClaveDirEstatal_E,"formas"+men,ComunesPDF.LEFT);
			pdfDoc.setCell(sClaveMoneda_E,"formas"+men,ComunesPDF.LEFT);
			pdfDoc.setCell(sBancoServicio_E,"formas"+men,ComunesPDF.CENTER);
			if("NB".equals(cs_tipo))
				pdfDoc.setCell(sFechaVencimiento_E,"formas"+men,ComunesPDF.LEFT);
			pdfDoc.setCell(sFechaProbPago_E,"formas"+men,ComunesPDF.LEFT);
			pdfDoc.setCell(sFechaDeposito_E,"formas"+men,ComunesPDF.LEFT);
			pdfDoc.setCell("$ "+Comunes.formatoDecimal(sImporteDeposito_E,2),"formas"+men,ComunesPDF.RIGHT);
			pdfDoc.setCell(sReferenciaBanco_E,"formas"+men,ComunesPDF.LEFT);
			pdfDoc.setCell(sReferenciaInter_E,"formas"+men,ComunesPDF.LEFT);
			if(!sOpcion.equals("C3")){
				pdfDoc.setCell("","formas"+men,ComunesPDF.LEFT);
				pdfDoc.setCell("","formas"+men,ComunesPDF.LEFT);
			}
			if("B".equals(cs_tipo)) {
				pdfDoc.setCell("","formas"+men,ComunesPDF.CENTER);
						pdfDoc.setCell("","formas"+men,ComunesPDF.CENTER);
						pdfDoc.setCell("","formas"+men,ComunesPDF.CENTER);
						pdfDoc.setCell("","formas"+men,ComunesPDF.CENTER);
					}
		}
		
		try{
			while (rs2.next()) {
				log.debug("crearCustomFile(E) dentro de while  ");
				String sSubaplicacion_D  = (rs2.getString("ig_subaplicacion") == null) ? "" : rs2.getString("ig_subaplicacion").trim();
				String sPrestamo_D       = (rs2.getString("ig_prestamo") == null) ? "" : rs2.getString("ig_prestamo").trim();
				String sClaveSirac_D     = (rs2.getString("ig_cliente") == null) ? "" : rs2.getString("ig_cliente").trim();
				String sCapital_D        = (rs2.getString("fg_amortizacion") == null) ? "" : rs2.getString("fg_amortizacion").trim();
				String sInteres_D        = (rs2.getString("fg_interes") == null) ? "" : rs2.getString("fg_interes").trim();
				String sMoratorios_D     = (rs2.getString("fg_interes_mora") == null) ? "" : rs2.getString("fg_interes_mora").trim();
				String sSubsidio_D       = (rs2.getString("fg_subsidio") == null) ? "" : rs2.getString("fg_subsidio").trim();
				String sComision_D       = (rs2.getString("fg_comision") == null) ? "" : rs2.getString("fg_comision").trim();
				String sIVA_D            = (rs2.getString("fg_iva") == null) ? "" : rs2.getString("fg_iva").trim();
				String sImportePago_D    = (rs2.getString("fg_totalexigible") == null) ? "" : rs2.getString("fg_totalexigible").trim();
				String sConcepto_D       = (rs2.getString("cg_concepto_pago") == null) ? "" : rs2.getString("cg_concepto_pago").trim();
				String sOrigen_D         = (rs2.getString("cg_origen_pago") == null) ? "" : rs2.getString("cg_origen_pago").trim();
				String rs_sucursal 			= (rs2.getString("IG_SUCURSAL") == null) ? "" : rs2.getString("IG_SUCURSAL");
				String rs_acreditado 		= (rs2.getString("cg_acreditado") == null) ? "" : rs2.getString("cg_acreditado").trim();
				String rs_fechaOperacion	= (rs2.getString("df_operacion") == null) ? "" : rs2.getString("df_operacion").trim();
				String rs_fechaVenc 		= (rs2.getString("df_vencimiento") == null) ? "" : rs2.getString("df_vencimiento").trim();
				String rs_fechaPago 		= (rs2.getString("df_pago") == null) ? "" : rs2.getString("df_pago").trim();
				double rs_saldoInsoluto 	= rs2.getDouble("fn_saldo_insoluto");
				String rs_tasa 				= (rs2.getString("ig_tasa") == null) ? "" : rs2.getString("ig_tasa").trim();
				String rs_dias 				= (rs2.getString("ig_dias") == null) ? "" : rs2.getString("ig_dias");
				if(tipo.equals("CSV")){	
					if("NB".equals(cs_tipo)) {
						cadAux = ",";
						contenidoArchivo.append(
								"D,"+
								sSubaplicacion_D.replace(',',' ')+","+
								sPrestamo_D.replace(',',' ')+","+
								sClaveSirac_D.replace(',',' ')+","+
								Comunes.formatoDecimal(sCapital_D,2,false)+","+
								Comunes.formatoDecimal(sInteres_D,2,false)+","+
								Comunes.formatoDecimal(sMoratorios_D,2,false)+","+
								Comunes.formatoDecimal(sSubsidio_D,2,false)+","+
								Comunes.formatoDecimal(sComision_D,2,false)+","+
								Comunes.formatoDecimal(sIVA_D,2,false)+","+
								Comunes.formatoDecimal(sImportePago_D,2,false)+","+
								sConcepto_D.replace(',',' ')+","+
								sOrigen_D.replace(',',' ')+",\n");
					} else if("B".equals(cs_tipo)) {
						contenidoArchivo.append(
								"D,"+
								rs_sucursal.replace(',',' ')+","+
								sSubaplicacion_D.replace(',',' ')+","+
								sPrestamo_D.replace(',',' ')+","+
								rs_acreditado.replace(',',' ')+","+
								rs_fechaOperacion.replace(',',' ')+","+
								rs_fechaVenc.replace(',',' ')+","+
								rs_fechaPago.replace(',',' ')+","+
								Comunes.formatoDecimal(rs_saldoInsoluto,2,false)+","+
								rs_tasa.replace(',',' ')+","+
								Comunes.formatoDecimal(sCapital_D,2,false)+","+
								rs_dias.replace(',',' ')+","+
								Comunes.formatoDecimal(sInteres_D,2,false)+","+
								Comunes.formatoDecimal(sComision_D,2,false)+","+
								Comunes.formatoDecimal(sIVA_D,2,false)+","+
								Comunes.formatoDecimal(sImportePago_D,2,false)+",\n");
					}
				}else if(tipo.equals("PDF")){
					pdfDoc.setCell("D","formasmenB",ComunesPDF.LEFT);
					if("NB".equals(cs_tipo)) {
						pdfDoc.setCell(sSubaplicacion_D,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(sPrestamo_D,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(sClaveSirac_D,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sCapital_D,2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sInteres_D,2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sMoratorios_D,2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sSubsidio_D,2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sComision_D,2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sIVA_D,2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sImportePago_D,2),"formasmen",ComunesPDF.RIGHT);
						pdfDoc.setCell(sConcepto_D,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(sOrigen_D,"formasmen",ComunesPDF.LEFT);
					} else if("B".equals(cs_tipo)) {
						pdfDoc.setCell(rs_sucursal,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(sSubaplicacion_D,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(sPrestamo_D,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(rs_acreditado,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(rs_fechaOperacion,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(rs_fechaVenc,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(rs_fechaPago,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(rs_saldoInsoluto,2,false),"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(rs_tasa,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sCapital_D,2,false),"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell(rs_dias,"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sInteres_D,2,false),"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sComision_D,2,false),"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sIVA_D,2,false),"formasmen",ComunesPDF.LEFT);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal(sImportePago_D,2,false),"formasmen",ComunesPDF.LEFT);
					}
				}
				iSubRegistros++;
				registros++;
			}//fin while 2
				dImporteTotal += Double.parseDouble(sImporteDeposito_E);
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			} catch(Exception e) {}
		}
		
		if(tipo.equals("CSV")){
				if(iSubRegistros!=0){
					contenidoArchivo.append("Total Pagos,"+registros+",,,,,,"+cadAux+Comunes.formatoDecimal(dImporteTotal,2,false)+",\n");
				}
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
		}
		if(tipo.equals("PDF")){
			if(iSubRegistros>0){
				pdfDoc.setCell("","formasmen",ComunesPDF.CENTER,numColEnc);
				pdfDoc.setCell("Total Pagos","formasmenB",ComunesPDF.LEFT,2);
				pdfDoc.setCell(String.valueOf(registros),"formasmen",ComunesPDF.LEFT);
				int colsTotal = 5;
				if("B".equals(cs_tipo))
					colsTotal--;
				pdfDoc.setCell("","formasmen",ComunesPDF.CENTER,colsTotal--);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(dImporteTotal,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setCell("","formasmen",ComunesPDF.CENTER,4);
				if("B".equals(cs_tipo)) {
					pdfDoc.setCell("","formasmen",ComunesPDF.CENTER,4);
				}
			}
				pdfDoc.addTable();
				pdfDoc.endDocument();
		}
	}
		return nombreArchivo;	
 }
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		return "";   
	}
	
	public HashMap getEncabezado(String sEncabezadoPago, String sOpcion){
		log.info("getEncabezado (E)");    
		AccesoDB 	con = new AccesoDB();    
		conditions = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean		commit = true;   
		strQuery 		= new StringBuffer();
		List varBind = new ArrayList();    
		HashMap	registros = new HashMap();
		String totalAdeudo = "0";
		try{
			con.conexionDB();
			if(sOpcion.equals("C2") || sOpcion.equals("C3")){
				strQuery.append(
					" SELECT "   +
	            "      /*+"	+
               "      use_nl(EP,F,RI)"	+
               "      index(EP CP_COM_ENCABEZADO_PAGO_PK)"	+
               "      */"	+
	            "       EP.IC_ENCABEZADO_PAGO"   +   
				   " 	  ,I.ic_financiera"   +
				   " 	  ,I.ic_of_controladora"   +
				   " 	  ,EP.IC_MONEDA"   +
				   " 	  ,TO_CHAR(EP.DF_PERIODO_FIN,'DD/MM/YYYY') AS DF_PERIODO_FIN"   +
				   " 	  ,TO_CHAR(EP.DF_PROBABLE_PAGO,'DD/MM/YYYY') AS DF_PROBABLE_PAGO"   +
				   " 	  ,TO_CHAR(EP.DF_DEPOSITO,'DD/MM/YYYY') AS DF_DEPOSITO "   +
				   " 	  ,TO_CHAR(EP.DF_REGISTRO,'DD/MM/YYYY') AS DF_REGISTRO"   +
				   " 	  ,EP.IG_IMPORTE_DEPOSITO"   +
				   " 	  ,EP.IC_FINANCIERA CVE_BANCO"   +
				   " 	  ,F.CD_NOMBRE BANCO"   +
				   " 	  ,EP.IC_REFERENCIA_INTER CVE_REFERENCIA_INTER"   +
				   " 	  ,RI.CG_DESCRIPCION REFERENCIA_INTER"   +
				   " 	  ,EP.CG_REFERENCIA_BANCO"   +
				   " FROM"   +
				   "       COM_ENCABEZADO_PAGO EP"   +
				   "	  ,COMCAT_IF I"+
				   " 	  ,COMCAT_FINANCIERA F"   +
				   " 	  ,COMCAT_REFERENCIA_INTER RI"   +
				   " WHERE "   +
				   "       EP.IC_FINANCIERA=F.IC_FINANCIERA"   +
				   "	   AND EP.IC_IF = I.IC_IF"+
				   " 	   AND EP.IC_REFERENCIA_INTER=RI.IC_REFERENCIA_INTER"   +
				   "       AND EP.IC_ENCABEZADO_PAGO ="+sEncabezadoPago);
			}
			log.info("query (E) "+ strQuery.toString());
			ps = con.queryPrecompilado(strQuery.toString());
			rs = ps.executeQuery();
			if( rs.next() ){
				registros.put("IC_ENCABEZADO_PAGO",rs.getString("IC_ENCABEZADO_PAGO")==null?"":rs.getString("IC_ENCABEZADO_PAGO"));
				registros.put("IC_FINANCIERA",rs.getString("IC_FINANCIERA")==null?"":rs.getString("IC_FINANCIERA"));
				registros.put("IC_OF_CONTROLADORA",rs.getString("IC_OF_CONTROLADORA")==null?"":rs.getString("IC_OF_CONTROLADORA"));
				registros.put("IC_MONEDA",rs.getString("IC_MONEDA")==null?"":rs.getString("IC_MONEDA"));
				registros.put("DF_PERIODO_FIN",rs.getString("DF_PERIODO_FIN")==null?"":rs.getString("DF_PERIODO_FIN"));
				registros.put("DF_PROBABLE_PAGO",rs.getString("DF_PROBABLE_PAGO")==null?"":rs.getString("DF_PROBABLE_PAGO"));
				registros.put("DF_DEPOSITO",rs.getString("DF_DEPOSITO")==null?"":rs.getString("DF_DEPOSITO"));
				registros.put("DF_REGISTRO",rs.getString("DF_REGISTRO")==null?"":rs.getString("DF_REGISTRO"));
				registros.put("IG_IMPORTE_DEPOSITO",rs.getString("IG_IMPORTE_DEPOSITO")==null?"":rs.getString("IG_IMPORTE_DEPOSITO"));
				registros.put("CVE_BANCO",rs.getString("CVE_BANCO")==null?"":rs.getString("CVE_BANCO"));
				registros.put("BANCO",rs.getString("BANCO")==null?"":rs.getString("BANCO"));
				registros.put("CVE_REFERENCIA_INTER",rs.getString("CVE_REFERENCIA_INTER")==null?"":rs.getString("CVE_REFERENCIA_INTER"));
				registros.put("REFERENCIA_INTER",rs.getString("REFERENCIA_INTER")==null?"":rs.getString("REFERENCIA_INTER"));
				registros.put("IC_ENCABEZADO_PAGO",rs.getString("IC_ENCABEZADO_PAGO")==null?"":rs.getString("IC_ENCABEZADO_PAGO"));
				registros.put("CG_REFERENCIA_BANCO",rs.getString("CG_REFERENCIA_BANCO")==null?"":rs.getString("CG_REFERENCIA_BANCO"));
			}
			rs.close();
			ps.close();	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getValorTotal "+e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getEncabezado (S)");
		}	
		return registros;
 	}
	  
	public JSONArray getTotales(String encabezado){
		log.info("getDataCurvaTabla (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		Registros registros  = new Registros();
		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
		HashMap totales = new HashMap();
		JSONArray registrosTC= new JSONArray();
		try{
			con.conexionDB();
			 qrySentencia.append(
						"  SELECT   /*+index(ie) use_nl(d ds s a3 ie e p m)*/ " +
						"   count (  distinct (IC_DETALLE_PAGO||IC_ENCABEZADO_PAGO))  as total_reg, sum (FG_TOTALEXIGIBLE) as totalPagos" +
						"   FROM COM_DETALLE_PAGO " +
						"   WHERE IC_ENCABEZADO_PAGO = " +
						encabezado);
			
			rs=con.queryDB(qrySentencia.toString());
			if(rs.next()) {
				totales = new HashMap();
				totales.put("REGISTROS",rs.getString("TOTAL_REG"));
				totales.put("TOTAL_PAGOS",rs.getString("TOTALPAGOS"));
				registrosTC.add(totales);
			} 
			
		}catch(Exception e){
			log.error("getDataCurvaTabla  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getDataCurvaTabla (S) ");
		return registrosTC;
	}
	public List getConditions() {
		return conditions;
	}
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
	

// variables requeridas 
	public String getFechaVencimientoIni() {
		return fechaVencimientoIni;
	}

	public void setFechaVencimientoIni(String fechaVencimientoIni) {
		this.fechaVencimientoIni = fechaVencimientoIni;
	}
	
	public String getFechaVencimientoFin() {
		return fechaVencimientoFin;
	}

	public void setFechaVencimientoFin(String fechaVencimientoFin) {
		this.fechaVencimientoFin = fechaVencimientoFin;
	}
	
	
	
	public String getFechaProbaPagoIni() {
		return fechaProbaPagoIni;
	}

	public void setFechaProbaPagoIni(String fechaProbaPagoIni) {
		this.fechaProbaPagoIni = fechaProbaPagoIni;
	}
	
	public String getFechaProbaPagoFin() {
		return fechaProbaPagoFin;
	}

	public void setFechaProbaPagoFin(String fechaProbaPagoFin) {
		this.fechaProbaPagoFin = fechaProbaPagoFin;
	}
	
	
	public String getFechaPagoIni() {
		return fechaPagoIni;
	}

	public void setFechaPagoIni(String fechaPagoIni) {
		this.fechaPagoIni = fechaPagoIni;
	}
	
	public String getFechaPagoFin() {
		return fechaPagoFin;
	}

	public void setFechaPagoFin(String fechaPagoFin) {
		this.fechaPagoFin = fechaPagoFin;
	}
	
	
	
	
	public String getFechaRegistroIni() {
		return fechaRegistroIni;
	}

	public void setFechaRegistroIni(String fechaRegistroIni) {
		this.fechaRegistroIni = fechaRegistroIni;
	}
	
	public String getFechaRegistroFin() {
		return fechaRegistroFin;
	}

	public void setfechaRegistroFin(String fechaRegistroFin) {
		this.fechaRegistroFin = fechaRegistroFin;
	}
	
	
	
	
	public String getNumIntermediario() {
		return numIntermediario;
	}

	public void setNumIntermediario(String numIntermediario) {
		this.numIntermediario = numIntermediario;
	}
	
	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}
	
	public String getSopcion() {
		return sOpcion;
	}

	public void setSopcion(String sOpcion) {
		this.sOpcion = sOpcion;
	}
	
	public String getEncabezadoPago() {
		return sEncabezadoPago;
	}

	public void setEncabezadoPago(String sEncabezadoPago) {
		this.sEncabezadoPago = sEncabezadoPago;
	}
	
	public String getCs_tipo() {
		return cs_tipo;
	}

	public void setCs_tipo(String cs_tipo) {
		this.cs_tipo = cs_tipo;
	}
	
	
	

}