package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsDocsFide implements IQueryGeneratorRegExtJS {
	
	public ConsDocsFide() {
		//Constructor
	}
	
	private String consulta="";
	private String num_epo;
	private String num_pyme;
	private String nom_if;
	private String num_docto;
	private String fec_noti_ini;
	private String fec_noti_fin;
	private String cve_moneda;
	private String monto_ini;
	private String monto_fin;
	private String cve_estatus;
	private String fec_ven_ini;
	private String fec_ven_fin;
	private String fec_ope_ini;
	private String fec_ope_fin;
	private String perfil="";

	private static final Log log = ServiceLocator.getInstance().getLog(ConsDocsFide.class);//Variable para enviar mensajes al log.

	public String getAggregateCalculationQuery()	{
		log.info(" getAggregateCalculationQuery(E)");
		this.variablesBind = new ArrayList();
		StringBuffer 	qrySentencia 	= new StringBuffer();
    
		qrySentencia.append( "SELECT  /*+ use_nl(s ds d a3 e i p) leading(ds) index(ds IN_COM_DOCTO_SELEC_04_NUK)*/ "+
			" m.cd_nombre AS cve_moneda, COUNT (ds.ic_documento) AS registros,  "+
         " SUM (d.fn_monto) AS monto_doc_tot,  "+
         " SUM (DECODE (d.fn_monto_dscto,'', d.fn_monto, d.fn_monto - d.fn_monto_dscto  )   ) AS monto_rec_garan_tot,  "+
         " SUM (d.fn_monto_dscto) AS monto_desc_tot,  "+
         " SUM (ds.in_importe_interes) AS monto_int_tot_pyme,  "+
         " SUM (ds.in_importe_recibir) AS monto_ope_tot_pyme,   "+
         " SUM (ds.in_importe_interes_fondeo) AS monto_int_tot_if,  "+
         " SUM (ds.in_importe_recibir_fondeo) AS monto_ope_tot_if,  "+
         " SUM (ds.in_importe_if) AS ing_neto_tot_if,  "+
         " SUM (ds.in_importe_epo) AS ing_neto_tot_fiso,   "+
         " SUM (ds.in_importe_x_operacion) AS tot_contra,  "+
         " SUM (ds.in_importe_nafin) AS ing_neto_tot_nafin   "+
			
			" FROM com_docto_seleccionado ds,  "+
         " com_documento d,  "+
         " comcat_epo e,   "+
         " comcat_pyme p,   "+
         " comcat_if i,   "+
         " comcat_if i2,   "+
         " com_solicitud s,   "+
         " com_acuse3 a3,   "+
         " comcat_moneda m  "+
			" WHERE ds.ic_documento = d.ic_documento  "+
			" AND ds.ic_epo = e.ic_epo   "+
			" AND ds.ic_if = i.ic_if   "+
			" AND d.ic_if = i2.ic_if   "+
			" AND d.ic_pyme = p.ic_pyme   "+
			" AND d.ic_documento = s.ic_documento   "+
			" AND d.ic_moneda = m.ic_moneda   "+
			" AND s.cc_acuse = a3.cc_acuse   "+
			" AND ds.cs_opera_fiso = ?   "+
			" AND s.ic_estatus_solic IN (? ,?, ? )  ");

			this.variablesBind.add("S");
			this.variablesBind.add("3");
			this.variablesBind.add("5");
			this.variablesBind.add("6");  
				
		try{
		
			if(!num_epo.equals("")){
				qrySentencia.append("    AND E.ic_epo = ? ");
				this.variablesBind.add(num_epo);
			}
			
			if(!num_pyme.equals("")){
				qrySentencia.append("    AND P.ic_pyme = ? ");
					this.variablesBind.add(num_pyme);
			}
			
			if(!nom_if.equals("")){
				qrySentencia.append("    AND I.ic_if = ? ");
					this.variablesBind.add(nom_if);
			}
			
			if(!num_docto.equals("")){
				qrySentencia.append("    AND D.ig_numero_docto = ? ");
					this.variablesBind.add(num_docto);
			}
			
			if(!"".equals(fec_noti_ini) && !"".equals(fec_noti_fin)){
				qrySentencia.append("    AND A3.df_fecha_hora >= TO_DATE (?, 'dd/mm/yyyy') ");
				qrySentencia.append("    AND A3.df_fecha_hora < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				this.variablesBind.add(fec_noti_ini);
				this.variablesBind.add(fec_noti_fin);
			}
			
			if(!cve_moneda.equals("")){
				qrySentencia.append("    AND D.ic_moneda = ? ");
					this.variablesBind.add(cve_moneda);
			}
			
			if(!monto_ini.equals("")) {
				if(!monto_fin.equals("")) {
					qrySentencia.append(" and D.fn_monto between "+monto_ini+" and "+monto_fin);
				}
			}
			
			if(!"".equals(fec_ven_ini)&&!"".equals(fec_ven_fin)){
				qrySentencia.append("    AND D.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy') ");
				qrySentencia.append("    AND D.df_fecha_venc < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				this.variablesBind.add(fec_ven_ini);
				this.variablesBind.add(fec_ven_fin);
			}
			
			if(!"".equals(fec_ope_ini)&&!"".equals(fec_ope_fin)){
				qrySentencia.append("    AND S.df_operacion >= TO_DATE (?, 'dd/mm/yyyy') ");
				qrySentencia.append("    AND S.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				this.variablesBind.add(fec_ope_ini);
				this.variablesBind.add(fec_ope_fin);
			}
			
			if(!cve_estatus.equals("")){
				qrySentencia.append("    AND D.ic_estatus_docto = ? ");
				this.variablesBind.add(cve_estatus);
			}else{
				qrySentencia.append("    AND D.ic_estatus_docto in (?,?,?) ");
				this.variablesBind.add("4");
				this.variablesBind.add("11");
				this.variablesBind.add("12");  
			}
		
			qrySentencia.append(" GROUP BY M.cd_nombre ");
			
			
			log.debug("qrySentencia   "+qrySentencia.toString());
			
		}catch (Exception e){
			log.error("getAggregateCalculationQueryException "+e);
		}
	
		log.info("getAggregateCalculationQuery(S) ");
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds)  {
	log.info("getDocumentSummaryQueryForIds(E) ");
		int i=0;
		this.variablesBind = new ArrayList();
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer clavesCombinaciones = new StringBuffer("");
		
		for(i=0; i<pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			clavesCombinaciones.append("?,");
			this.variablesBind.add(new Long(lItem.get(0).toString()));
		}
		
		clavesCombinaciones = clavesCombinaciones.delete(clavesCombinaciones.length()-1,clavesCombinaciones.length());
		
		if(consulta.equals("D")){ //Para consulta detalle
			qrySentencia.append(
						" SELECT /*+ index(ds IN_COM_DOCTO_SELEC_04_NUK, a3 cp_com_acuse3_pk) use_nl(ds a3 s d)*/ DS.ic_documento, E.cg_razon_social as NOM_EPO, P.cg_razon_social as NOM_PYME, D.ig_numero_docto as NUM_DOC, "+
						"  TO_CHAR(A3.df_fecha_hora, 'dd/mm/yyyy') as FEC_NOTI, TO_CHAR (D.df_fecha_venc, 'dd/mm/yyyy') as FEC_VEN,  "+
						"  TO_CHAR(S.df_operacion, 'dd/mm/yyyy') as FEC_OPE, S.ig_plazo as PLAZO, T.cd_descripcion as ESTATUS,  "+
						"  M.cd_nombre as MONEDA, D.fn_monto as MONTO_DOC, decode(D.fn_monto_dscto, '', D.fn_monto, D.fn_monto - D.fn_monto_dscto) as REC_GARAN, D.fn_monto_dscto as MONTO_DESC, "+
						"  I.cg_razon_social as NOM_FIDE, DS.in_tasa_aceptada as TASA_PYME, DS.in_importe_interes as MONTO_INT_PYME, DS.in_importe_recibir as MONTO_OPE_PYME, "+
						"  I2.cg_razon_social as NOM_IF, DS.in_tasa_aceptada_fondeo as TASA_IF, DS.in_importe_interes_fondeo as MONTO_INT_IF, DS.in_importe_recibir_fondeo as MONTO_OPE_IF, "+
						"  DS.in_tasa_x_if as TASA_IF_ING, DS.in_importe_if as ING_NETO_IF, DS.in_tasa_epo as DIF_TASAS, DS.in_importe_epo as ING_NETO_FISO, "+
						"  DS.in_tasa_x_fondeo as TASA_FONDEO_IF, DS.in_importe_x_fondeo as ING_FONDEO, DS.in_tasa_x_operacion as PORC_CONTRA_EPO, "+
						"  DS.in_importe_x_operacion as CONTRA_EPO, DS.in_importe_nafin as ING_NETO_NAFIN "+
						" FROM com_docto_seleccionado DS, com_documento D, comcat_epo E, comcat_pyme P, comcat_if I, comcat_if I2, com_solicitud S, com_acuse3 A3, comcat_moneda M, comcat_estatus_docto T ");
		}else if(consulta.equals("C")){ //Para consulta consolidado
			qrySentencia.append(
						" SELECT /*+ index(ds IN_COM_DOCTO_SELEC_04_NUK, a3 cp_com_acuse3_pk) use_nl(ds a3 s d)*/ P.cg_rfc as RFC, P.cg_razon_social as NOM_PYME, E.cg_razon_social as NOM_EPO, count(D.ig_numero_docto) as TOT_DOC_CON, "+
						" TO_CHAR (D.df_fecha_venc, 'dd/mm/yyyy') as FEC_VEN, TO_CHAR(S.df_operacion, 'dd/mm/yyyy') as FEC_OPE,  "+
						" T.cd_descripcion as ESTATUS, M.cd_nombre as MONEDA, sum(D.fn_monto) as MONTO_DOC,  "+
						" sum(decode(D.fn_monto_dscto, '', D.fn_monto, D.fn_monto - D.fn_monto_dscto)) as REC_GARAN, "+
						" sum(D.fn_monto_dscto) as MONTO_DESC, sum(DS.in_importe_interes) as MONTO_INT_PYME, sum(DS.in_importe_recibir) as MONTO_OPE_PYME,  "+
						" sum(DS.in_importe_interes_fondeo) as MONTO_INT_IF, sum(DS.in_importe_recibir_fondeo) as MONTO_OPE_IF,  "+
						" sum(DS.in_importe_if) as ING_NETO_IF, sum(DS.in_importe_epo) as ING_NETO_FISO, sum(DS.in_importe_x_fondeo) as ING_FONDEO,  "+
						" sum(DS.in_importe_x_operacion) as CONTRA_EPO, sum(DS.in_importe_nafin) as ING_NETO_NAFIN  "+
						" FROM comcat_pyme P, com_docto_seleccionado DS, com_documento D, comcat_epo E, comcat_if I, comcat_if I2, com_solicitud S, com_acuse3 A3, comcat_moneda M, comcat_estatus_docto T  ");				
		}
		
		qrySentencia.append(" WHERE DS.ic_documento = D.ic_documento " +
						" AND DS.ic_epo = E.ic_epo " +
						" AND DS.ic_if = I.ic_if" +
						" AND D.ic_if = I2.ic_if" +
						" AND D.ic_pyme = P.ic_pyme " +
						" AND D.ic_documento = S.ic_documento " +
						" AND D.ic_moneda = M.ic_moneda " +
						" AND D.ic_estatus_docto = T.ic_estatus_docto " +
						" AND S.cc_acuse = A3.cc_acuse ");
					
		qrySentencia.append("   AND DS.ic_documento IN("+clavesCombinaciones+")");
		
		if(consulta.equals("C")){ //Para consulta consolidado
			qrySentencia.append(" GROUP BY  P.cg_rfc, P.cg_razon_social, E.cg_razon_social, TO_CHAR (D.df_fecha_venc, 'dd/mm/yyyy') , "+
			" TO_CHAR(S.df_operacion, 'dd/mm/yyyy'), t.cd_descripcion, m.cd_nombre ");
		}
		
		log.debug("qrySentencia:::  "+qrySentencia.toString());
		
		log.info("getDocumentSummaryQueryForIds(S))");
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery()	{ //genera todos los id's
		
		log.info("getDocumentQuery(E)");
	
		this.variablesBind = new ArrayList();
		StringBuffer qrySentencia = new StringBuffer();
    	StringBuffer condicion	= new StringBuffer();
		
		qrySentencia.append(
					" 	SELECT /*+ index(ds IN_COM_DOCTO_SELEC_04_NUK, a3 cp_com_acuse3_pk) use_nl(ds a3 s d)*/ DS.ic_documento"   +
					"  FROM com_docto_seleccionado DS, com_documento D, comcat_epo E, comcat_if I, comcat_pyme P, com_solicitud S, com_acuse3 A3 "+
					"	WHERE DS.ic_documento = D.ic_documento "+
					"	AND DS.ic_documento = S.ic_documento "+
					"	AND DS.ic_epo = E.ic_epo "+
					"	AND DS.ic_if = I.ic_if "+
					"	AND DS.cs_opera_fiso = 'S' "+
					"	AND D.ic_pyme = P.ic_pyme "+
					"	AND S.ic_estatus_solic in (3,5,6) "+
					"	AND S.cc_acuse = A3.cc_acuse ");
		
		try{
		
			if(!num_epo.equals("")){
				condicion.append("    AND E.ic_epo = ? ");
					this.variablesBind.add(num_epo);
			}
			
			if(!num_pyme.equals("")){
				condicion.append("    AND P.ic_pyme = ? ");
					this.variablesBind.add(num_pyme);
			}
			
			if(!nom_if.equals("")){
				condicion.append("    AND I.ic_if = ? ");
					this.variablesBind.add(nom_if);
			}
			
			if(!num_docto.equals("")){
				condicion.append("    AND D.ig_numero_docto = ? ");
					this.variablesBind.add(num_docto);
			}
			
			if(!"".equals(fec_noti_ini) && !"".equals(fec_noti_fin)){
				condicion.append("    AND A3.df_fecha_hora >= TO_DATE (?, 'dd/mm/yyyy') ");
				condicion.append("    AND A3.df_fecha_hora < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				this.variablesBind.add(fec_noti_ini);
				this.variablesBind.add(fec_noti_fin);
			}
			
			if(!cve_moneda.equals("")){
				condicion.append("    AND D.ic_moneda = ? ");
					this.variablesBind.add(cve_moneda);
			}
			
			if(!monto_ini.equals("")) {
				if(!monto_fin.equals("")) {
					condicion.append(" and D.fn_monto between "+monto_ini+" and "+monto_fin);
				}
			}
			
			if(!"".equals(fec_ven_ini)&&!"".equals(fec_ven_fin)){
				condicion.append("    AND D.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy') ");
				condicion.append("    AND D.df_fecha_venc < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				this.variablesBind.add(fec_ven_ini);
				this.variablesBind.add(fec_ven_fin);
			}
			
			if(!"".equals(fec_ope_ini)&&!"".equals(fec_ope_fin)){
				condicion.append("    AND S.df_operacion >= TO_DATE (?, 'dd/mm/yyyy') ");
				condicion.append("    AND S.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				this.variablesBind.add(fec_ope_ini);
				this.variablesBind.add(fec_ope_fin);
			}
			
			if(!cve_estatus.equals("")){
				condicion.append("    AND D.ic_estatus_docto = ? ");
				this.variablesBind.add(cve_estatus);
			}else{
				condicion.append("    AND D.ic_estatus_docto in (?,?,?) ");
				this.variablesBind.add("4");
				this.variablesBind.add("11");
				this.variablesBind.add("12");
			}
						
			qrySentencia.append(condicion.toString());
			
			log.debug("qrySentencia )"+qrySentencia.toString());
			
		}catch (Exception e){
			log.error("getDocumentQueryException "+e);
		}
		
		
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}


	public String getDocumentQueryFile() {
		this.variablesBind = new ArrayList();
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();

		if(consulta.equals("D")){ //Para consulta detalle
			qrySentencia.append(
						" SELECT /*+ index(ds IN_COM_DOCTO_SELEC_04_NUK, a3 cp_com_acuse3_pk) use_nl(ds a3 s d)*/ DS.ic_documento, E.cg_razon_social as NOM_EPO, P.cg_razon_social as NOM_PYME, D.ig_numero_docto as NUM_DOC, "+
						"  TO_CHAR(A3.df_fecha_hora, 'dd/mm/yyyy') as FEC_NOTI, TO_CHAR (D.df_fecha_venc, 'dd/mm/yyyy') as FEC_VEN,  "+
						"  TO_CHAR(S.df_operacion, 'dd/mm/yyyy') as FEC_OPE, S.ig_plazo as PLAZO, T.cd_descripcion as ESTATUS,  "+
						"  M.cd_nombre as MONEDA, D.fn_monto as MONTO_DOC, decode(D.fn_monto_dscto, '', D.fn_monto, D.fn_monto - D.fn_monto_dscto) as REC_GARAN, D.fn_monto_dscto as MONTO_DESC, "+
						"  I.cg_razon_social as NOM_FIDE, DS.in_tasa_aceptada as TASA_PYME, DS.in_importe_interes as MONTO_INT_PYME, DS.in_importe_recibir as MONTO_OPE_PYME, "+
						"  I2.cg_razon_social as NOM_IF, DS.in_tasa_aceptada_fondeo as TASA_IF, DS.in_importe_interes_fondeo as MONTO_INT_IF, DS.in_importe_recibir_fondeo as MONTO_OPE_IF, "+
						"  DS.in_tasa_x_if as TASA_IF_ING, DS.in_importe_if as ING_NETO_IF, DS.in_tasa_epo as DIF_TASAS, DS.in_importe_epo as ING_NETO_FISO, "+
						"  DS.in_tasa_x_fondeo as TASA_FONDEO_IF, DS.in_importe_x_fondeo as ING_FONDEO, DS.in_tasa_x_operacion as PORC_CONTRA_EPO, "+
						"  DS.in_importe_x_operacion as CONTRA_EPO, DS.in_importe_nafin as ING_NETO_NAFIN "+
						" FROM com_docto_seleccionado DS, com_documento D, comcat_epo E, comcat_pyme P, comcat_if I, comcat_if I2, com_solicitud S, com_acuse3 A3, comcat_moneda M, comcat_estatus_docto T ");
		}else if(consulta.equals("C")){ //Para consulta consolidado
			qrySentencia.append(
						" SELECT /*+ index(ds IN_COM_DOCTO_SELEC_04_NUK, a3 cp_com_acuse3_pk) use_nl(ds a3 s d)*/ P.cg_rfc as RFC, P.cg_razon_social as NOM_PYME, E.cg_razon_social as NOM_EPO, count(D.ig_numero_docto) as TOT_DOC_CON, "+
						" TO_CHAR (D.df_fecha_venc, 'dd/mm/yyyy') as FEC_VEN, TO_CHAR(S.df_operacion, 'dd/mm/yyyy') as FEC_OPE,  "+
						" T.cd_descripcion as ESTATUS, M.cd_nombre as MONEDA, sum(D.fn_monto) as MONTO_DOC,  "+
						" sum(decode(D.fn_monto_dscto, '', D.fn_monto, D.fn_monto - D.fn_monto_dscto)) as REC_GARAN, "+
						" sum(D.fn_monto_dscto) as MONTO_DESC, sum(DS.in_importe_interes) as MONTO_INT_PYME, sum(DS.in_importe_recibir) as MONTO_OPE_PYME,  "+
						" sum(DS.in_importe_interes_fondeo) as MONTO_INT_IF, sum(DS.in_importe_recibir_fondeo) as MONTO_OPE_IF,  "+
						" sum(DS.in_importe_if) as ING_NETO_IF, sum(DS.in_importe_epo) as ING_NETO_FISO, sum(DS.in_importe_x_fondeo) as ING_FONDEO,  "+
						" sum(DS.in_importe_x_operacion) as CONTRA_EPO, sum(DS.in_importe_nafin) as ING_NETO_NAFIN  "+
						" FROM comcat_pyme P, com_docto_seleccionado DS, com_documento D, comcat_epo E, comcat_if I, comcat_if I2, com_solicitud S, com_acuse3 A3, comcat_moneda M, comcat_estatus_docto T ");				
		}
		
		qrySentencia.append(" WHERE DS.ic_documento = D.ic_documento " +
						" AND DS.ic_epo = E.ic_epo " +
						" AND DS.ic_if = I.ic_if "+
						" AND D.ic_if = I2.ic_if "+
						" AND D.ic_pyme = P.ic_pyme " +
						" AND D.ic_documento = S.ic_documento " +
						" AND D.ic_moneda = M.ic_moneda " +
						" AND D.ic_estatus_docto = T.ic_estatus_docto " +
						" AND S.cc_acuse = A3.cc_acuse ");
	
		try{
		
			if(!num_epo.equals("")){
				condicion.append("    AND E.ic_epo = ? ");
					this.variablesBind.add(num_epo);
			}
			
			if(!num_pyme.equals("")){
				condicion.append("    AND P.ic_pyme = ? ");
					this.variablesBind.add(num_pyme);
			}
			
			if(!nom_if.equals("")){
				condicion.append("    AND I.ic_if = ? ");
					this.variablesBind.add(nom_if);
			}
			
			if(!num_docto.equals("")){
				condicion.append("    AND D.ig_numero_docto = ? ");
					this.variablesBind.add(num_docto);
			}
			
			if(!"".equals(fec_noti_ini) && !"".equals(fec_noti_fin)){
				condicion.append("    AND A3.df_fecha_hora >= TO_DATE (?, 'dd/mm/yyyy') ");
				condicion.append("    AND A3.df_fecha_hora < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				this.variablesBind.add(fec_noti_ini);
				this.variablesBind.add(fec_noti_fin);
			}
			
			if(!cve_moneda.equals("")){
				condicion.append("    AND D.ic_moneda = ? ");
					this.variablesBind.add(cve_moneda);
			}
			
			if(!monto_ini.equals("")) {
				if(!monto_fin.equals("")) {
					condicion.append(" and D.fn_monto between "+monto_ini+" and "+monto_fin);
				}
			}
			
			if(!"".equals(fec_ven_ini)&&!"".equals(fec_ven_fin)){
				condicion.append("    AND D.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy') ");
				condicion.append("    AND D.df_fecha_venc < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				this.variablesBind.add(fec_ven_ini);
				this.variablesBind.add(fec_ven_fin);
			}
			
			if(!"".equals(fec_ope_ini)&&!"".equals(fec_ope_fin)){
				condicion.append("    AND S.df_operacion >= TO_DATE (?, 'dd/mm/yyyy') ");
				condicion.append("    AND S.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				this.variablesBind.add(fec_ope_ini);
				this.variablesBind.add(fec_ope_fin);
			}
			
			if(!cve_estatus.equals("")){
				condicion.append("    AND D.ic_estatus_docto = ? ");
				this.variablesBind.add(cve_estatus);
			}else{
				condicion.append("    AND D.ic_estatus_docto in (?,?,?) ");
				this.variablesBind.add("4");
				this.variablesBind.add("11");
				this.variablesBind.add("12");
			}
			
			qrySentencia.append(condicion.toString());
			
			qrySentencia.append(" AND DS.ic_documento IN ( SELECT DS.ic_documento "+
            " FROM com_docto_seleccionado DS, com_documento D, comcat_epo E, comcat_if I, com_solicitud S, com_acuse3 A3, comcat_pyme P "+
				" WHERE DS.ic_documento = D.ic_documento AND DS.ic_documento = S.ic_documento AND DS.ic_epo = e.ic_epo AND DS.ic_if = I.ic_if "+
            " AND DS.cs_opera_fiso = 'S' AND D.ic_estatus_docto IN (4, 11, 12) AND D.ic_pyme = P.ic_pyme AND S.ic_estatus_solic IN (3, 5, 6) "+
            " AND S.cc_acuse = A3.cc_acuse ) ");
			
			if(consulta.equals("C")){ //Para consulta consolidado
				qrySentencia.append(" GROUP BY  P.cg_rfc, P.cg_razon_social, E.cg_razon_social, TO_CHAR (D.df_fecha_venc, 'dd/mm/yyyy') , "+
				" TO_CHAR(S.df_operacion, 'dd/mm/yyyy'), t.cd_descripcion, m.cd_nombre ");
			}
			
		}catch (Exception e){
			log.debug("ConsDocsFide::getDocumentQueryException "+e);
		}
	
		log.debug("getDocumentQueryFile)"+qrySentencia.toString());
		return qrySentencia.toString();
	}


	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo = new CreaArchivo();

		int registros = 0;
		String rfc = "", nomEpo= "", nomPyme = "", numDoc= "", fecNoti="", fecVen= "",  fecOpe = "", plazo= "", estatus ="", moneda ="", monDoc ="",
							recGaran = "", monDesc = "", nomFide = "", tasa = "", monInt = "", MonOpe = "", nomIf = "", tasaIf = "", monIntIf = "",
							monOpeIf = "", tasaIfIng = "", ingNetoIf = "", difTasas = "", ingNetoFiso = "", tasaFon = "", ingFon = "", porConEpo = "",
							conEpo = "", ingNetoNafin = "";

		if ("CSV".equals(tipo)){
			try {
				if(consulta.equals("D")){
					while(rs.next()){
						if(registros==0){
							contenidoArchivo.append(" Nombre de EPO"+ "," +
															" Nombre PYME"+ "," +
															" Número de Documento"+ "," +
															" Fecha de Notificación"+ "," +
															" Fecha de Vencimiento"+ "," +
															" Fecha de Operación"+ "," +
															" Plazo"+ "," +
															" Estatus"+ "," +
															" Moneda"+ "," +
															" Monto del Documento"+ "," +
															" Recurso en Garantía"+ "," +
															" Monto a Descontar"+ "," +
															" Nombre Fideicomiso"+ "," +
															" Tasa"+ "," +
															" Monto Int."+ "," +
															" Monto a Operar"+ "," +
															" Nombre IF"+ "," +
															" Tasa"+ "," +
															" Monto Int."+ "," +
															" Monto a Operar"+ ",");
							if(perfil.equals("EPO")){
								contenidoArchivo.append(" % Contraprestación"+ "," +
																" Contraprestación"+ "," +
																" Diferencial de Tasas"+ "," +
																" Ingreso Neto FISO" +"\n");
							} else{
								contenidoArchivo.append(" Tasa IF"+ "," +
																" Ingreso Neto IF"+ "," +
																" Diferencial de Tasas"+ "," +
																" Ingreso Neto FISO"+ "," +
																" Tasa de Fondeo al IF"+ "," +
																" Ingreso por FONDEO"+ "," +
																" % Contraprestación a EPO"+ "," +
																" Contraprestación a EPO"+ "," +
																" Ingreso Neto NAFIN" +"\n");
							}
						}
						registros++;
						nomEpo       = (rs.getString("NOM_EPO")         == null) ? "" : rs.getString("NOM_EPO");
						nomPyme      = (rs.getString("NOM_PYME")        == null) ? "" : rs.getString("NOM_PYME");
						numDoc       = (rs.getString("NUM_DOC")         == null) ? "" : rs.getString("NUM_DOC");
						fecNoti      = (rs.getString("FEC_NOTI")        == null) ? "":rs.getString("FEC_NOTI");
						fecVen       = (rs.getString("FEC_VEN")         == null) ? "" : rs.getString("FEC_VEN");
						fecOpe       = (rs.getString("FEC_OPE")         == null) ? "" : rs.getString("FEC_OPE");
						plazo        = (rs.getString("PLAZO")           == null) ? "" : rs.getString("PLAZO");
						estatus      = (rs.getString("ESTATUS")         == null) ? "" : rs.getString("ESTATUS");
						moneda       = (rs.getString("MONEDA")          == null) ? "" : rs.getString("MONEDA");
						monDoc       = (rs.getString("MONTO_DOC")       == null) ? "" : rs.getString("MONTO_DOC");
						recGaran     = (rs.getString("REC_GARAN")       == null) ? "" : rs.getString("REC_GARAN");
						monDesc      = (rs.getString("MONTO_DESC")      == null) ? "" : rs.getString("MONTO_DESC");
						nomFide      = (rs.getString("NOM_FIDE")        == null) ? "" : rs.getString("NOM_FIDE");
						tasa         = (rs.getString("TASA_PYME")       == null) ? "" : rs.getString("TASA_PYME");
						monInt       = (rs.getString("MONTO_INT_PYME")  == null) ? "" : rs.getString("MONTO_INT_PYME");
						MonOpe       = (rs.getString("MONTO_OPE_PYME")  == null) ? "" : rs.getString("MONTO_OPE_PYME");
						nomIf        = (rs.getString("NOM_IF")          == null) ? "" : rs.getString("NOM_IF");
						tasaIf       = (rs.getString("TASA_IF")         == null) ? "" : rs.getString("TASA_IF");
						monIntIf     = (rs.getString("MONTO_INT_IF")    == null) ? "" : rs.getString("MONTO_INT_IF");
						monOpeIf     = (rs.getString("MONTO_OPE_IF")    == null) ? "" : rs.getString("MONTO_OPE_IF");
						tasaIfIng    = (rs.getString("TASA_IF_ING")     == null) ? "" : rs.getString("TASA_IF_ING");
						ingNetoIf    = (rs.getString("ING_NETO_IF")     == null) ? "" : rs.getString("ING_NETO_IF");
						difTasas     = (rs.getString("DIF_TASAS")       == null) ? "" : rs.getString("DIF_TASAS");
						ingNetoFiso  = (rs.getString("ING_NETO_FISO")   == null) ? "" : rs.getString("ING_NETO_FISO");
						tasaFon      = (rs.getString("TASA_FONDEO_IF")  == null) ? "" : rs.getString("TASA_FONDEO_IF");
						ingFon       = (rs.getString("ING_FONDEO")      == null) ? "" : rs.getString("ING_FONDEO");
						porConEpo    = (rs.getString("PORC_CONTRA_EPO") == null) ? "" : rs.getString("PORC_CONTRA_EPO");
						conEpo       = (rs.getString("CONTRA_EPO")      == null) ? "" : rs.getString("CONTRA_EPO");
						ingNetoNafin = (rs.getString("ING_NETO_NAFIN")  == null) ? "" : rs.getString("ING_NETO_NAFIN");

						contenidoArchivo.append( nomEpo.replaceAll(",","") +","+
														nomPyme.replaceAll(",","") +","+
														numDoc.replaceAll(",","") +","+
														fecNoti.replaceAll(",","") +","+
														fecVen.replaceAll(",","") +","+
														fecOpe.replaceAll(",","") +","+
														plazo.replaceAll(",","") +","+
														estatus.replaceAll(",","") +","+
														moneda.replaceAll(",","") +","+
														monDoc.replaceAll(",","") +","+
														recGaran.replaceAll(",","") +","+
														monDesc.replaceAll(",","") +","+
														nomFide.replaceAll(",","") +","+
														tasa.replaceAll(",","") +","+
														monInt.replaceAll(",","") +","+
														MonOpe.replaceAll(",","") +","+
														nomIf.replaceAll(",","") +","+
														tasaIf.replaceAll(",","") +","+
														monIntIf.replaceAll(",","") +","+
														monOpeIf.replaceAll(",","") +","); 
						if(perfil.equals("EPO")){
							contenidoArchivo.append(porConEpo.replaceAll(",","") +","+
															conEpo.replaceAll(",","") +","+
															difTasas.replaceAll(",","") +","+
															ingNetoFiso.replaceAll(",","") +"\n");
						} else{
							contenidoArchivo.append(tasaIfIng.replaceAll(",","") +","+
															ingNetoIf.replaceAll(",","") +","+
															difTasas.replaceAll(",","") +","+
															ingNetoFiso.replaceAll(",","") +","+
															tasaFon.replaceAll(",","") +","+
															ingFon.replaceAll(",","") +","+
															porConEpo.replaceAll(",","") +","+
															conEpo.replaceAll(",","") +","+
															ingNetoNafin.replaceAll(",","") +"\n");
						}	
					}
					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}
				} else if(consulta.equals("C")){
					while(rs.next()){
						if(registros==0){
							contenidoArchivo.append(" RFC"+ "," +
															" Nombre PYME"+ ",");
							if(!perfil.equals("EPO"))
								contenidoArchivo.append(" EPO"+ ",");
							contenidoArchivo.append(" Total Doctos."+ "," +
															" Fecha de Vencimiento"+ "," +
															" Fecha de Operación"+ "," +
															" Estatus"+ "," +
															" Moneda"+ "," +
															" Monto Documentos"+ "," +
															" Recurso en Garantía"+ "," +
															" Monto Descuento"+ "," +
															" Monto Int."+ "," +
															" Monto a Operar"+ "," +
															" Monto Int."+ "," +
															" Monto a Operar"+ ",");
							if(perfil.equals("EPO")){
								contenidoArchivo.append(" Contraprestación"+ "," +
																" Ingreso Neto FISO" +"\n");
							}else{
								contenidoArchivo.append(
																" Ingreso Neto IF"+ "," +
																" Ingreso Neto FISO"+ "," +
																" Ingreso por FONDEO"+ "," +
																" Contraprestación a EPO"+ "," +
																" Ingreso Neto NAFIN" +"\n");
							}
						}
						registros++;
						rfc          = (rs.getString("RFC")            == null) ? "" : rs.getString("RFC");
						nomPyme      = (rs.getString("NOM_PYME")       == null) ? "" : rs.getString("NOM_PYME");
						nomEpo       = (rs.getString("NOM_EPO")        == null) ? "" : rs.getString("NOM_EPO");
						numDoc       = (rs.getString("TOT_DOC_CON")    == null) ? "" : rs.getString("TOT_DOC_CON");
						fecVen       = (rs.getString("FEC_VEN")        == null) ? "" : rs.getString("FEC_VEN");
						fecOpe       = (rs.getString("FEC_OPE")        == null) ? "" : rs.getString("FEC_OPE");
						estatus      = (rs.getString("ESTATUS")        == null) ? "" : rs.getString("ESTATUS");
						moneda       = (rs.getString("MONEDA")         == null) ? "" : rs.getString("MONEDA");
						monDoc       = (rs.getString("MONTO_DOC")      == null) ? "" : rs.getString("MONTO_DOC");
						recGaran     = (rs.getString("REC_GARAN")      == null) ? "" : rs.getString("REC_GARAN");
						monDesc      = (rs.getString("MONTO_DESC")     == null) ? "" : rs.getString("MONTO_DESC");
						monInt       = (rs.getString("MONTO_INT_PYME") == null) ? "" : rs.getString("MONTO_INT_PYME");
						MonOpe       = (rs.getString("MONTO_OPE_PYME") == null) ? "" : rs.getString("MONTO_OPE_PYME");
						monIntIf     = (rs.getString("MONTO_INT_IF")   == null) ? "" : rs.getString("MONTO_INT_IF");
						monOpeIf     = (rs.getString("MONTO_OPE_IF")   == null) ? "" : rs.getString("MONTO_OPE_IF");
						ingNetoIf    = (rs.getString("ING_NETO_IF")    == null) ? "" : rs.getString("ING_NETO_IF");
						ingNetoFiso  = (rs.getString("ING_NETO_FISO")  == null) ? "" : rs.getString("ING_NETO_FISO");
						ingFon       = (rs.getString("ING_FONDEO")     == null) ? "" : rs.getString("ING_FONDEO");
						conEpo       = (rs.getString("CONTRA_EPO")     == null) ? "" : rs.getString("CONTRA_EPO");
						ingNetoNafin = (rs.getString("ING_NETO_NAFIN") == null) ? "" : rs.getString("ING_NETO_NAFIN");

						contenidoArchivo.append(rfc.replaceAll(",","") +","+
														nomPyme.replaceAll(",","") +",");
						if(!perfil.equals("EPO"))
							contenidoArchivo.append(nomEpo.replaceAll(",","") +",");
						contenidoArchivo.append(numDoc.replaceAll(",","") +","+
														fecVen.replaceAll(",","") +","+
														fecOpe.replaceAll(",","") +","+
														estatus.replaceAll(",","") +","+
														moneda.replaceAll(",","") +","+
														monDoc.replaceAll(",","") +","+
														recGaran.replaceAll(",","") +","+
														monDesc.replaceAll(",","") +","+
														monInt.replaceAll(",","") +","+
														MonOpe.replaceAll(",","") +","+
														monIntIf.replaceAll(",","") +","+
														monOpeIf.replaceAll(",","") +",");
						if(perfil.equals("EPO")){
							contenidoArchivo.append(conEpo.replaceAll(",","") +","+
															ingNetoFiso.replaceAll(",","") +"\n");
						}else{
							contenidoArchivo.append(ingNetoIf.replaceAll(",","") +","+
															ingNetoFiso.replaceAll(",","") +","+
															ingFon.replaceAll(",","") +","+
															conEpo.replaceAll(",","") +","+
															ingNetoNafin.replaceAll(",","") +"\n");
						}
					}
					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}
				}
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			}

		} else if(tipo.equals("PDF")){

			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] =  {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual   =  new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual  =  fechaActual.substring(0,2);
				String mesActual  =  meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual =  fechaActual.substring(6,10);
				String horaActual =  new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("México, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
				horaActual, "formas", ComunesPDF.RIGHT);

				if(consulta.equals("D")){
					pdfDoc.setLTable(11, 100);
					pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Nombre de EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Número de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de Notificación","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de Operación","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto del Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Recurso en Garantía","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Nombre Fideicomiso","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tasa","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("C","celda01",ComunesPDF.CENTER);
					if(perfil.equals("EPO")){
						pdfDoc.setLCell("% Contraprestación","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Contraprestación","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Diferencial de Tasas","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Ingreso Neto FISO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,6);
					}else{
						pdfDoc.setLCell("Tasa IF","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Ingreso Neto IF","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Diferencial de Tasas","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Ingreso Neto FISO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Tasa de Fondeo al IF","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Ingreso por FONDEO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("% Contraprestación a EPO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Contraprestación a EPO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Ingreso Neto NAFIN","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setLHeaders();

					while (rs.next()) {
						nomEpo = (rs.getString("NOM_EPO")             == null) ? "" : rs.getString("NOM_EPO");
						nomPyme = (rs.getString("NOM_PYME")           == null) ? "" : rs.getString("NOM_PYME");
						numDoc= (rs.getString("NUM_DOC")              == null) ? "" : rs.getString("NUM_DOC");
						fecNoti=(rs.getString("FEC_NOTI")             == null) ? "" : rs.getString("FEC_NOTI");
						fecVen= (rs.getString("FEC_VEN")              == null) ? "" : rs.getString("FEC_VEN");
						fecOpe = (rs.getString("FEC_OPE")             == null) ? "" : rs.getString("FEC_OPE");
						plazo= (rs.getString("PLAZO")                 == null) ? "" : rs.getString("PLAZO");
						estatus =(rs.getString("ESTATUS")             == null) ? "" : rs.getString("ESTATUS");
						moneda =(rs.getString("MONEDA")               == null) ? "" : rs.getString("MONEDA");
						monDoc =(rs.getString("MONTO_DOC")            == null) ? "" : rs.getString("MONTO_DOC");
						recGaran =(rs.getString("REC_GARAN")          == null) ? "" : rs.getString("REC_GARAN");
						monDesc =(rs.getString("MONTO_DESC")          == null) ? "" : rs.getString("MONTO_DESC");
						nomFide =(rs.getString("NOM_FIDE")            == null) ? "" : rs.getString("NOM_FIDE");
						tasa =(rs.getString("TASA_PYME")              == null) ? "" : rs.getString("TASA_PYME");
						monInt =(rs.getString("MONTO_INT_PYME")       == null) ? "" : rs.getString("MONTO_INT_PYME");
						MonOpe =(rs.getString("MONTO_OPE_PYME")       == null) ? "" : rs.getString("MONTO_OPE_PYME");
						nomIf =(rs.getString("NOM_IF")                == null) ? "" : rs.getString("NOM_IF");
						tasaIf =(rs.getString("TASA_IF")              == null) ? "" : rs.getString("TASA_IF");
						monIntIf =(rs.getString("MONTO_INT_IF")       == null) ? "" : rs.getString("MONTO_INT_IF");
						monOpeIf =(rs.getString("MONTO_OPE_IF")       == null) ? "" : rs.getString("MONTO_OPE_IF");
						tasaIfIng =(rs.getString("TASA_IF_ING")       == null) ? "" : rs.getString("TASA_IF_ING");
						ingNetoIf =(rs.getString("ING_NETO_IF")       == null) ? "" : rs.getString("ING_NETO_IF");
						difTasas =(rs.getString("DIF_TASAS")          == null) ? "" : rs.getString("DIF_TASAS");
						ingNetoFiso =(rs.getString("ING_NETO_FISO")   == null) ? "" : rs.getString("ING_NETO_FISO");
						tasaFon =(rs.getString("TASA_FONDEO_IF")      == null) ? "" : rs.getString("TASA_FONDEO_IF");
						ingFon =(rs.getString("ING_FONDEO")           == null) ? "" : rs.getString("ING_FONDEO");
						porConEpo =(rs.getString("PORC_CONTRA_EPO")   == null) ? "" : rs.getString("PORC_CONTRA_EPO");
						conEpo =(rs.getString("CONTRA_EPO")           == null) ? "" : rs.getString("CONTRA_EPO");
						ingNetoNafin =(rs.getString("ING_NETO_NAFIN") == null) ? "" : rs.getString("ING_NETO_NAFIN");

						pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(nomEpo,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(nomPyme,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(numDoc,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(fecNoti,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(fecVen,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(fecOpe,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monDoc,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(recGaran,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monDesc,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(nomFide,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monInt,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(MonOpe,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(nomIf,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(tasaIf,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monIntIf,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monOpeIf,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("C","formas",ComunesPDF.CENTER);
						if(perfil.equals("EPO")){
							pdfDoc.setLCell(porConEpo,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(conEpo,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell(difTasas,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoFiso,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("","formas",ComunesPDF.LEFT,6);
						}else{
							pdfDoc.setLCell(tasaIfIng,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoIf,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell(difTasas,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoFiso,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell(tasaFon,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingFon,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell(porConEpo,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(conEpo,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoNafin,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
						}
					}
				}else if(consulta.equals("C")){
					if(perfil.equals("EPO"))
						pdfDoc.setLTable(10, 100);
					else
						pdfDoc.setLTable(11, 100);
					pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("RFC","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					if(!perfil.equals("EPO"))
						pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Total Doctos.","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de Operación","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Documentos","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Recurso en Garantía","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
					if(perfil.equals("EPO")){
						pdfDoc.setLCell("Monto Descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Contraprestación","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Ingreso Neto FISO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,2);
					}else{
						pdfDoc.setLCell("Monto Descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Ingreso Neto IF","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Ingreso Neto FISO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Ingreso por FONDEO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Contraprestación a EPO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Ingreso Neto NAFIN","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setLHeaders();

					while(rs.next()){
						rfc = (rs.getString("RFC")                    == null) ? "" : rs.getString("RFC");
						nomPyme = (rs.getString("NOM_PYME")           == null) ? "" : rs.getString("NOM_PYME");
						nomEpo = (rs.getString("NOM_EPO")             == null) ? "" : rs.getString("NOM_EPO");
						numDoc= (rs.getString("TOT_DOC_CON")          == null) ? "" : rs.getString("TOT_DOC_CON");
						fecVen= (rs.getString("FEC_VEN")              == null) ? "" : rs.getString("FEC_VEN");
						fecOpe = (rs.getString("FEC_OPE")             == null) ? "" : rs.getString("FEC_OPE");
						estatus =(rs.getString("ESTATUS")             == null) ? "" : rs.getString("ESTATUS");
						moneda =(rs.getString("MONEDA")               == null) ? "" : rs.getString("MONEDA");
						monDoc =(rs.getString("MONTO_DOC")            == null) ? "" : rs.getString("MONTO_DOC");
						recGaran =(rs.getString("REC_GARAN")          == null) ? "" : rs.getString("REC_GARAN");
						monDesc =(rs.getString("MONTO_DESC")          == null) ? "" : rs.getString("MONTO_DESC");
						monInt =(rs.getString("MONTO_INT_PYME")       == null) ? "" : rs.getString("MONTO_INT_PYME");
						MonOpe =(rs.getString("MONTO_OPE_PYME")       == null) ? "" : rs.getString("MONTO_OPE_PYME");
						monIntIf =(rs.getString("MONTO_INT_IF")       == null) ? "" : rs.getString("MONTO_INT_IF");
						monOpeIf =(rs.getString("MONTO_OPE_IF")       == null) ? "" : rs.getString("MONTO_OPE_IF");
						ingNetoIf =(rs.getString("ING_NETO_IF")       == null) ? "" : rs.getString("ING_NETO_IF");
						ingNetoFiso =(rs.getString("ING_NETO_FISO")   == null) ? "" : rs.getString("ING_NETO_FISO");
						ingFon =(rs.getString("ING_FONDEO")           == null) ? "" : rs.getString("ING_FONDEO");
						conEpo =(rs.getString("CONTRA_EPO")           == null) ? "" : rs.getString("CONTRA_EPO");
						ingNetoNafin =(rs.getString("ING_NETO_NAFIN") == null) ? "" : rs.getString("ING_NETO_NAFIN");

						pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(rfc,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(nomPyme,"formas",ComunesPDF.LEFT);
						if(!perfil.equals("EPO"))
							pdfDoc.setLCell(nomEpo,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(numDoc,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(fecVen,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(fecOpe,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(estatus,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(moneda,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monDoc,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(recGaran,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
						if(perfil.equals("EPO")){
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(monDesc,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(monInt,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(MonOpe,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(monIntIf,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(monOpeIf,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(conEpo,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoFiso,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("","formas",ComunesPDF.LEFT,2);
						}else{
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(monDesc,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(monInt,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(MonOpe,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(monIntIf,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(monOpeIf,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoIf,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoFiso,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingFon,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(conEpo,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoNafin,2),"formas",ComunesPDF.RIGHT);
						}
					}
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();

			}catch (Throwable e) {
				throw new AppException("Error al generar el archivo",e);
			}

		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		log.debug("crearCustomFile (E)");
		
		try {
			HttpSession session = request.getSession();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[]	=	{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual	=	fechaActual.substring(0,2);
			String mesActual	=	meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual	=	fechaActual.substring(6,10);
			String horaActual	=	new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			(String)session.getAttribute("sesExterno"),
			(String)session.getAttribute("strNombre"),
			(String)session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),
			(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

			pdfDoc.addText("México, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
			horaActual, "formas", ComunesPDF.CENTER);
			
			String rfc = "", nomEpo= "", nomPyme = "", numDoc= "", fecNoti="", fecVen= "",  fecOpe = "", plazo= "", estatus ="", moneda ="", monDoc ="",
							  recGaran = "", monDesc = "", nomFide = "", tasa = "", monInt = "", MonOpe = "", nomIf = "", tasaIf = "", monIntIf = "",
							  monOpeIf = "", tasaIfIng = "", ingNetoIf = "", difTasas = "", ingNetoFiso = "", tasaFon = "", ingFon = "", porConEpo = "",
							  conEpo = "", ingNetoNafin = "";
			
			if(consulta.equals("D")){
				pdfDoc.setLTable(11, 100);
				pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre de EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Número de Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Notificación","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Operación","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto del Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Recurso en Garantía","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre Fideicomiso","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("C","celda01",ComunesPDF.CENTER);
				if(perfil.equals("EPO")){
					pdfDoc.setLCell("% Contraprestación","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Contraprestación","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Diferencial de Tasas","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Ingreso Neto FISO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,6);
				}else{
					pdfDoc.setLCell("Tasa IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Ingreso Neto IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Diferencial de Tasas","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Ingreso Neto FISO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tasa de Fondeo al IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Ingreso por FONDEO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("% Contraprestación a EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Contraprestación a EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Ingreso Neto NAFIN","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
				}
				
				while (rs.next()) {
					nomEpo = (rs.getString("NOM_EPO") == null) ? "" : rs.getString("NOM_EPO");
					nomPyme = (rs.getString("NOM_PYME") == null) ? "" : rs.getString("NOM_PYME");
					numDoc= (rs.getString("NUM_DOC") == null) ? "" : rs.getString("NUM_DOC");
					fecNoti=(rs.getString("FEC_NOTI") == null)?"":rs.getString("FEC_NOTI");
					fecVen= (rs.getString("FEC_VEN") == null) ? "" : rs.getString("FEC_VEN");
					fecOpe = (rs.getString("FEC_OPE") == null) ? "" : rs.getString("FEC_OPE");
					plazo= (rs.getString("PLAZO") == null) ? "" : rs.getString("PLAZO");
					estatus =(rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
					moneda =(rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					monDoc =(rs.getString("MONTO_DOC") == null) ? "" : rs.getString("MONTO_DOC");
					recGaran =(rs.getString("REC_GARAN") == null) ? "" : rs.getString("REC_GARAN");
					monDesc =(rs.getString("MONTO_DESC") == null) ? "" : rs.getString("MONTO_DESC");
					nomFide =(rs.getString("NOM_FIDE") == null) ? "" : rs.getString("NOM_FIDE");
					tasa =(rs.getString("TASA_PYME") == null) ? "" : rs.getString("TASA_PYME");
					monInt =(rs.getString("MONTO_INT_PYME") == null) ? "" : rs.getString("MONTO_INT_PYME");
					MonOpe =(rs.getString("MONTO_OPE_PYME") == null) ? "" : rs.getString("MONTO_OPE_PYME");
					nomIf =(rs.getString("NOM_IF") == null) ? "" : rs.getString("NOM_IF");
					tasaIf =(rs.getString("TASA_IF") == null) ? "" : rs.getString("TASA_IF");
					monIntIf =(rs.getString("MONTO_INT_IF") == null) ? "" : rs.getString("MONTO_INT_IF");
					monOpeIf =(rs.getString("MONTO_OPE_IF") == null) ? "" : rs.getString("MONTO_OPE_IF");
					tasaIfIng =(rs.getString("TASA_IF_ING") == null) ? "" : rs.getString("TASA_IF_ING");
					ingNetoIf =(rs.getString("ING_NETO_IF") == null) ? "" : rs.getString("ING_NETO_IF");
					difTasas =(rs.getString("DIF_TASAS") == null) ? "" : rs.getString("DIF_TASAS");
					ingNetoFiso =(rs.getString("ING_NETO_FISO") == null) ? "" : rs.getString("ING_NETO_FISO");
					tasaFon =(rs.getString("TASA_FONDEO_IF") == null) ? "" : rs.getString("TASA_FONDEO_IF");
					ingFon =(rs.getString("ING_FONDEO") == null) ? "" : rs.getString("ING_FONDEO");
					porConEpo =(rs.getString("PORC_CONTRA_EPO") == null) ? "" : rs.getString("PORC_CONTRA_EPO");
					conEpo =(rs.getString("CONTRA_EPO") == null) ? "" : rs.getString("CONTRA_EPO");
					ingNetoNafin =(rs.getString("ING_NETO_NAFIN") == null) ? "" : rs.getString("ING_NETO_NAFIN");
	
					pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nomEpo,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nomPyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numDoc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecNoti,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecVen,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecOpe,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monDoc,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(recGaran,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monDesc,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(nomFide,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(tasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monInt,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(MonOpe,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(nomIf,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(tasaIf,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monIntIf,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monOpeIf,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("C","formas",ComunesPDF.CENTER);
					if(perfil.equals("EPO")){
						pdfDoc.setLCell(porConEpo,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(conEpo,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(difTasas,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoFiso,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("","formas",ComunesPDF.LEFT,6);
					}else{
						pdfDoc.setLCell(tasaIfIng,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoIf,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(difTasas,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoFiso,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(tasaFon,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingFon,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(porConEpo,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(conEpo,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoNafin,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
					}
				}
				
			}else if(consulta.equals("C")){
				if(perfil.equals("EPO"))
					pdfDoc.setLTable(10, 100);
				else
					pdfDoc.setLTable(11, 100);
				pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre PYME","celda01",ComunesPDF.CENTER);
				if(!perfil.equals("EPO"))
					pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Total Doctos.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Operación","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Documentos","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Recurso en Garantía","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
				if(perfil.equals("EPO")){
					pdfDoc.setLCell("Monto Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Contraprestación","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Ingreso Neto FISO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,2);
				}else{
					pdfDoc.setLCell("Monto Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Int.","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto a Operar","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Ingreso Neto IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Ingreso Neto FISO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Ingreso por FONDEO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Contraprestación a EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Ingreso Neto NAFIN","celda01",ComunesPDF.CENTER);
				}
				
				while(rs.next()){
					rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
					nomPyme = (rs.getString("NOM_PYME") == null) ? "" : rs.getString("NOM_PYME");
					nomEpo = (rs.getString("NOM_EPO") == null) ? "" : rs.getString("NOM_EPO");
					numDoc= (rs.getString("TOT_DOC_CON") == null) ? "" : rs.getString("TOT_DOC_CON");
					fecVen= (rs.getString("FEC_VEN") == null) ? "" : rs.getString("FEC_VEN");
					fecOpe = (rs.getString("FEC_OPE") == null) ? "" : rs.getString("FEC_OPE");
					estatus =(rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
					moneda =(rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					monDoc =(rs.getString("MONTO_DOC") == null) ? "" : rs.getString("MONTO_DOC");
					recGaran =(rs.getString("REC_GARAN") == null) ? "" : rs.getString("REC_GARAN");
					monDesc =(rs.getString("MONTO_DESC") == null) ? "" : rs.getString("MONTO_DESC");
					monInt =(rs.getString("MONTO_INT_PYME") == null) ? "" : rs.getString("MONTO_INT_PYME");
					MonOpe =(rs.getString("MONTO_OPE_PYME") == null) ? "" : rs.getString("MONTO_OPE_PYME");
					monIntIf =(rs.getString("MONTO_INT_IF") == null) ? "" : rs.getString("MONTO_INT_IF");
					monOpeIf =(rs.getString("MONTO_OPE_IF") == null) ? "" : rs.getString("MONTO_OPE_IF");
					ingNetoIf =(rs.getString("ING_NETO_IF") == null) ? "" : rs.getString("ING_NETO_IF");
					ingNetoFiso =(rs.getString("ING_NETO_FISO") == null) ? "" : rs.getString("ING_NETO_FISO");
					ingFon =(rs.getString("ING_FONDEO") == null) ? "" : rs.getString("ING_FONDEO");
					conEpo =(rs.getString("CONTRA_EPO") == null) ? "" : rs.getString("CONTRA_EPO");
					ingNetoNafin =(rs.getString("ING_NETO_NAFIN") == null) ? "" : rs.getString("ING_NETO_NAFIN");
					
					pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(rfc,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nomPyme,"formas",ComunesPDF.LEFT);
					if(!perfil.equals("EPO"))
						pdfDoc.setLCell(nomEpo,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numDoc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecVen,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecOpe,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monDoc,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(recGaran,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
					if(perfil.equals("EPO")){
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monDesc,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monInt,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(MonOpe,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monIntIf,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monOpeIf,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(conEpo,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoFiso,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("","formas",ComunesPDF.LEFT,2);
					}else{
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monDesc,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monInt,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(MonOpe,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monIntIf,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(monOpeIf,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoIf,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoFiso,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingFon,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(conEpo,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(ingNetoNafin,2),"formas",ComunesPDF.RIGHT);
					}
				}
			}
			pdfDoc.addLTable();
			pdfDoc.endDocument();
			
		}catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}

		return nombreArchivo;
	}
	
	public List getConditions() {
		log.debug("*************getConditions=" + variablesBind);
		return variablesBind;
	}
	private ArrayList variablesBind = null;

	/**
	 * Metodos Getters & Setters de la Clase
	*/
	
	public void setConsulta(String tipo) {
		this.consulta = tipo;
	}
	
	public String getConsulta() {
		return consulta;
	}
	
	public void setNum_epo(String num_epo) {
		this.num_epo = num_epo;
	}

	public String getNum_epo() {
		return num_epo;
	}

	public void setNum_pyme(String num_pyme) {
		this.num_pyme = num_pyme;
	}

	public String getNum_pyme() {
		return num_pyme;
	}

	public void setNom_if(String nom_if) {
		this.nom_if = nom_if;
	}

	public String getNom_if() {
		return nom_if;
	}

	public void setNum_docto(String num_docto) {
		this.num_docto = num_docto;
	}

	public String getNum_docto() {
		return num_docto;
	}

	public void setFec_noti_ini(String fec_noti_ini) {
		this.fec_noti_ini = fec_noti_ini;
	}

	public String getFec_noti_ini() {
		return fec_noti_ini;
	}

	public void setFec_noti_fin(String fec_noti_fin) {
		this.fec_noti_fin = fec_noti_fin;
	}

	public String getFec_noti_fin() {
		return fec_noti_fin;
	}

	public void setCve_moneda(String cve_moneda) {
		this.cve_moneda = cve_moneda;
	}

	public String getCve_moneda() {
		return cve_moneda;
	}

	public void setMonto_ini(String monto_ini) {
		this.monto_ini = monto_ini;
	}

	public String getMonto_ini() {
		return monto_ini;
	}

	public void setMonto_fin(String monto_fin) {
		this.monto_fin = monto_fin;
	}

	public String getMonto_fin() {
		return monto_fin;
	}

	public void setCve_estatus(String cve_estatus) {
		this.cve_estatus = cve_estatus;
	}

	public String getCve_estatus() {
		return cve_estatus;
	}

	public void setFec_ven_ini(String fec_ven_ini) {
		this.fec_ven_ini = fec_ven_ini;
	}

	public String getFec_ven_ini() {
		return fec_ven_ini;
	}

	public void setFec_ven_fin(String fec_ven_fin) {
		this.fec_ven_fin = fec_ven_fin;
	}

	public String getFec_ven_fin() {
		return fec_ven_fin;
	}

	public void setFec_ope_ini(String fec_ope_ini) {
		this.fec_ope_ini = fec_ope_ini;
	}

	public String getFec_ope_ini() {
		return fec_ope_ini;
	}

	public void setFec_ope_fin(String fec_ope_fin) {
		this.fec_ope_fin = fec_ope_fin;
	}

	public String getFec_ope_fin() {
		return fec_ope_fin;
	}		


	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}


	public String getPerfil() {
		return perfil;
	}
	
}