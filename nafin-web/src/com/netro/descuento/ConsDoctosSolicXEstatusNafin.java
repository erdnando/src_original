package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsDoctosSolicXEstatusNafin implements IQueryGeneratorRegExtJS  {
	
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsDoctosSolicXEstatusNafin.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	private String tipoOper="";
	StringBuffer qrySentencia = new StringBuffer("");
	RepDoctoSolicxEstatusDEbean baseQuery=null;
	String estatus;
	private String banderaFISO;
   
	public ConsDoctosSolicXEstatusNafin(RepDoctoSolicxEstatusDEbean inic) {
		baseQuery=inic;
	}	
	
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
		
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		baseQuery.setTipoOper(tipoOper);
		qrySentencia.append(baseQuery.getQueryPorEstatus());	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	
	public Registros getDocumentQueryFile_Con() {
		log.info("getDocumentQueryFile_Con(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		Registros registros = new Registros();
		
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			baseQuery.setTipoOper(tipoOper);
			baseQuery.setBanderaFISO(banderaFISO);
			qrySentencia.append(baseQuery.getQueryPorEstatusAggregateCalculationQuery());	
			registros = con.consultarDB(qrySentencia.toString()); 
			
		} catch(Exception e) {
			throw new AppException("getDocumentQueryFile_Con  ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); 
				con.cierraConexionDB();
			}
		}
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile_Con(S)");
		return registros;	
	}
	
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
	log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
	
				
		int i = 0;
	int ic_moneda = 0;
	int doc_nacional = 0;
	int doc_dolares = 0;
	double nacionalNC = 0;
	double nacional_descNC = 0;
	double dolaresNC = 0;
	double dolares_descNC = 0;
	double nacional = 0;
	double nacional_desc = 0;
	double nacional_int = 0;
	double nacional_descIF = 0;
	double dolares = 0;
	double dolares_desc = 0;
	double dolares_int = 0;
	double dolares_descIF = 0;
	double fn_monto_dscto = 0;
	double in_importe_interes = 0;
	double in_importe_recibir = 0;
	double in_tasa_aceptada = 0;
	double fn_monto = 0;
	double dblPorcentaje = 0;
	double dblRecurso = 0;
	double dblMontoDescuento = 0;
	double dblTotalMontoDescuentoPesos = 0;
	double dblTotalMontoDescuentoDolares = 0;
	double dblImporteDepositar = 0;
	double dblImporteAplicado = 0;
	double dblImporteInteres = 0;
	String cg_razon_social_PYME = "";
	String cg_razon_social_EPO = "";
	String ig_numero_docto = "";
	String df_fecha_venc = "";
	String cg_razon_social_IF = "";
	String cc_acuse = "";
	String cd_nombre = "";
	String ic_folio = "";
	String df_fecha_docto = "";
	String plazo = "";
	String origen = "";
	boolean dolaresDocto = false;
	boolean nacionalDocto = false;
	String tipoFactoraje = "";
	String nombreTipoFactoraje = "";
	String NombreIf = "";
	String ig_numero_prestamo = "";
	String lsNumeroPedido = "", lsNumPrestamoNafin = "", lsImporteAplicado = "", lsPorcentajeDocto = "", lsImporteDepositar = "";
	String fecha_reg_ope = ""; // Fodea 005-2009 24 hrs
  String df_fecha_registro_operacion =""; //Fodea 037-2010 Reporte IFS de Programada PYME
	String tipoTasa =""; //Fodea 031-2011
	
	/////////////////////////FODEA 17 2013
	String cg_razon_social_FID = "";
	String in_tasa_aceptada_FID = "0";
	String in_importe_interes_FID_str="",in_importe_recibir_FID_str="";
	double in_importe_interes_FID = 0;
	double in_importe_recibir_FID = 0;
	String operaFiso="";
		
	boolean nacionalDocto_fid = false;boolean dolaresDocto_fid = false;
	double nacional_fid = 0,nacionalNC_fid = 0,nacional_descNC_fid = 0,dblMontoDescuento_fid = 0,fn_monto_fid = 0,
		dblPorcentaje_fid = 0,dblTotalMontoDescuentoDolares_fid = 0,dblTotalMontoDescuentoPesos_fid=0
		,fn_monto_dscto_fid=0,nacional_desc_fid = 0,nacional_int_fid = 0,nacional_descIF_fid = 0,
		dolares_desc_fid = 0, dolares_int_fid = 0, dolares_descIF_fid = 0,in_importe_interes_fid=0,in_importe_recibir_fid=0,
		nacional_int_fiso_s=0,nacional_descIF_fiso_s=0,dolares_descIF_fiso_s=0,dolares_int_fiso_s = 0;
		
	int  doc_nacional_fid =0,doc_dolares_fid =0,dolaresNC_fid=0,dolares_descNC_fid=0,dolares_fid = 0;
	////////////////////////////////////////
		try {
		
			if(tipo.equals("PDF") ) {
		
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				String pais=(String)session.getAttribute("strPais");
				String inonafin=(String)session.getAttribute("iNoNafinElectronico");
				String sesExterno=(String)session.getAttribute("sesExterno");
				String strNombre=(String) session.getAttribute("strNombre");
				String strNombreUsuario=(String) session.getAttribute("strNombreUsuario");
				String strLogo=(String)session.getAttribute("strLogo");
				String dirpub=(String)session.getServletContext().getAttribute("strDirectorioPublicacion");
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			}
				int column =0;
			if(tipo.equals("PDF") && estatus.equals("0")) {
				
				column = 16;		
				pdfDoc.setTable(8);pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);pdfDoc.setCell("Negociables","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(column,100);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER);pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Documento ","celda01",ComunesPDF.CENTER);pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto Documento ","celda01",ComunesPDF.CENTER);pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("N�mero de Acuse ","celda01",ComunesPDF.CENTER);pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER); 
				}
			if(tipo.equals("PDF") && estatus.equals("1")) {
				
				column = 19;
				pdfDoc.setTable(8);pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);pdfDoc.setCell("Seleccionada PYME","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(column,100);				
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo (d�as) ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int. IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa FIDEICOMISO","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int. FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Fecha Registro Operaci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				}	
				
			if(tipo.equals("PDF") && estatus.equals("2")) {
				 column = 19;				
				
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("En Proceso de Autorizaci�n IF","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(column,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo (d�as) ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Nombre IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int. IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre FIDEICOMISO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa FIDEICOMISO","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int. FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Referencia Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
			}			
			if(tipo.equals("PDF") && estatus.equals("3")) {
				 column = 17;				
				
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Seleccionada IF","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(column,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int. IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa FIDEICOMISO","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int. FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar FIDEICOMISO","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Origen","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Acuse IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Referencia Tasa ","celda01",ComunesPDF.CENTER);			
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				
			}			
			if(tipo.equals("PDF") && estatus.equals("4")) {
				 column = 17;				
				
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("En Proceso","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(column,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int. IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa FIDEICOMISO","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int. FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
			}	
			if(tipo.equals("PDF") && estatus.equals("5")) {
				 column = 16;				
				
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Operada","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(column,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa FIDEICOMISO","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
			}	
			if(tipo.equals("PDF") && estatus.equals("6")) {
				 column = 21;				
				
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Operado con Fondeo Propio","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(column,100);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Int. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Origen ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Acuse IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER); 
			}
			if(tipo.equals("PDF") && estatus.equals("7")) {
				 column = 17;				
				
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Operada Pagada","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(column,100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa FIDEICOMISO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Pago","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Referencia Tasa","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
			}
			if(tipo.equals("PDF") && estatus.equals("8")) {
				 column = 22;				
				
				pdfDoc.setTable(8);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Programado PyME","formas",ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				pdfDoc.setTable(column,100);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario Financiero ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo (d�as) ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Monto Int. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% Beneficiario ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Fecha Registro Operaci�n","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Referencia Tasa ","celda01",ComunesPDF.CENTER);	 
			}
			
			
			if(tipo.equals("CSV") && estatus.equals("1") ) {
				contenidoArchivo.append(" ESTATUS: Seleccionada PYME ");
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append(" \n ");
				contenidoArchivo.append(" Nombre EPO, Nombre PYME, N�mero Documento, Fecha Documento, Fecha Vencimiento,Plazo (d�as), Moneda,  Tipo Factoraje, Monto Documento ,Porcentaje de Descuento, Recurso en Garant�a, Monto a Descontar,Nombre IF,Tasa IF, Monto Int. IF ,Monto a Operar IF,Nombre FIDEICOMISO,Tasa FIDEICOMISO,Monto Int. FIDEICOMISO,Monto a Operar FIDEICOMISO, Neto a Recibir PYME, Beneficiario, % Beneficiario ,Importe a Recibir Beneficiario ,Fecha Registro Operaci�n, Referencia Tasa ");
			   contenidoArchivo.append(" \n "); 
			}
			if(tipo.equals("CSV") && estatus.equals("2") ) {
				contenidoArchivo.append(" ESTATUS: En Proceso de Autorizaci�n IF ");
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append(" Nombre EPO, Nombre PYME,N�mero de Documento, Fecha Documento, Fecha Vencimiento ,Plazo (d�as) ,Moneda ,Tipo Factoraje, Monto Documento, Porcentaje de Descuento, Recurso en Garant�a, Monto a Descontar, Nombre IF,Tasa IF, Monto Int. IF ,Monto a Operar IF,Nombre FIDEICOMISO,Tasa FIDEICOMISO,Monto Int. FIDEICOMISO,Monto a Operar FIDEICOMISO,Neto a Recibir PYME, Beneficiario, % Beneficiario, Importe a Recibir Beneficiario, Referencia Tasa  "); 
				contenidoArchivo.append(" \n ");
			}	
			if(tipo.equals("CSV") && estatus.equals("3") ) {
				contenidoArchivo.append(" ESTATUS: Seleccionada IF ");
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append("Nombre EPO, Nombre PYME, Nombre IF, N�mero de Documento, N�mero Solicitud, Moneda, Tipo Factoraje, Monto Documento, Porcentaje de Descuento, Recurso en Garant�a, Monto a Descontar, Tasa, Monto Int., Monto a Operar, Origen, N�mero Acuse IF, Neto a Recibir PYME, Beneficiario, % Beneficiario, Importe a Recibir Beneficiario , Referencia Tasa  "); 
				contenidoArchivo.append(" \n "); 
			}
			if(tipo.equals("CSV") && estatus.equals("5") ) {
				contenidoArchivo.append(" ESTATUS: Operada ");
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append(" \n ");
				contenidoArchivo.append(" Nombre EPO, Nombre PYME, N�mero de Solicitud, N�mero Documento, Moneda, Tipo Factoraje, Monto Documento, Porcentaje de Descuento ,Recurso en Garant�a, Monto a Descontar, Nombre IF, Tasa IF, Monto a Operar IF, Nombre FIDEICOMISO,Tasa FIDEICOMISO,Monto a Operar FIDEICOMISO,Neto a Recibir PYME, Beneficiario, % Beneficiario, Importe a Recibir Beneficiario, Referencia Tasa ");
				contenidoArchivo.append(" \n ");
			}
			if(tipo.equals("CSV") && estatus.equals("6") ) {
				contenidoArchivo.append(" ESTATUS: Operado con Fondeo Propio ");
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append("Nombre EPO, Nombre PYME, Nombre IF, N�mero de Documento, N�mero Solicitud, Moneda, Tipo Factoraje, Monto Documento, Porcentaje de Descuento, Recurso en Garant�a, Monto a Descontar, Tasa, Monto Int., Monto a Operar, Origen, N�mero Acuse IF, Neto a Recibir PYME, Beneficiario, % Beneficiario, Importe a Recibir Beneficiario , Referencia Tasa  "); 
				contenidoArchivo.append(" \n "); 
			}
			if(tipo.equals("CSV") && estatus.equals("7") ) {
				contenidoArchivo.append(" ESTATUS: Operada Pagada ");
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append(" Nombre EPO, Nombre PYME,  N�mero de Solicitud, N�mero Documento, Moneda, Tipo Factoraje, Monto Documento, Porcentaje de Descuento, Recurso en Garant�a, Monto a Descontar,Nombre IF, Tasa IF, Nombre FIDEICOMISO,Tasa FIDEICOMISO, Fecha Vencimiento, Fecha Pago,  Neto a Recibir PYME, Beneficiario, % Beneficiario, Importe a Recibir Beneficiario , Referencia Tasa ");
				contenidoArchivo.append(" \n ");
			}
			if(tipo.equals("CSV") && estatus.equals("8") ) {
				contenidoArchivo.append(" ESTATUS: Programado PyME ");
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append(" \n "); 
				contenidoArchivo.append(" Nombre EPO, Nombre PYME, N�mero de Documento, Fecha Documento, Fecha Vencimiento, Intermediario Financiero, Tasa, Plazo (d�as), Moneda, Tipo Factoraje, Monto Documento, Porcentaje de Descuento, Recurso en Garant�a ,Monto a Descontar, Monto Int., Monto a Operar, Neto a Recibir PYME, Beneficiario, % Beneficiario, Importe a Recibir Beneficiario, Fecha Registro Operaci�n , Referencia Tasa "); 
				contenidoArchivo.append(" \n "); 
			}
			
			if(tipo.equals("PDF") &&  estatus.equals("0") ) {
				while (rs.next()) {
					cg_razon_social_EPO = (rs.getString("NOMBREEPO")==null)?"":rs.getString("NOMBREEPO").trim();
					cg_razon_social_PYME = (rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME").trim();
					ig_numero_docto = (rs.getString("NUMERODOCTO")==null)?"":rs.getString("NUMERODOCTO").trim();
					cc_acuse = (rs.getString("ACUSE1")==null)?"":rs.getString("ACUSE1").trim();
					df_fecha_docto = (rs.getString("FECHADOCTO")==null)?"":rs.getString("FECHADOCTO").trim();
					df_fecha_venc = (rs.getString("FECHAVENC")==null)?"":rs.getString("FECHAVENC").trim();
					cd_nombre = (rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA").trim();
					ic_moneda = rs.getInt("NUMEROMONEDA");
					fn_monto = rs.getDouble("MONTODOCTO");
					dblPorcentaje = rs.getDouble("AFORO");
					dblMontoDescuento = fn_monto * (dblPorcentaje / 100);
			
					// Para factoraje Vencido
					tipoFactoraje = (rs.getString("TIPOFACTORAJE")==null)?"":rs.getString("TIPOFACTORAJE");
					nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE");
					NombreIf = "";
					if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("C") || tipoFactoraje.equals("I") ) {
						NombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
						dblMontoDescuento = fn_monto;
						dblPorcentaje = 100;
					}
					if(tipoFactoraje.equals("A") ){
						NombreIf = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
					}
					
					dblRecurso = fn_monto - dblMontoDescuento;
			
					if (ic_moneda == 1) {
						if(tipoFactoraje.equals("C")) {
							nacionalNC += fn_monto;
							nacional_descNC += dblMontoDescuento;
						} else {
							dblTotalMontoDescuentoPesos += dblMontoDescuento;
							nacional+=fn_monto;
							nacionalDocto = true;
							doc_nacional++;
						}
					}
					if (ic_moneda == 54) {
						if(tipoFactoraje.equals("C")) {
							dolaresNC += fn_monto;
							dolares_descNC += dblMontoDescuento;
						} else {
							dblTotalMontoDescuentoDolares += dblMontoDescuento;
							dolares+=fn_monto;
							dolaresDocto = true;
							doc_dolares++;
						}
					}
													
						pdfDoc.setCell(cg_razon_social_EPO,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cg_razon_social_PYME,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_venc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cd_nombre,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+ Comunes.formatoMoneda2(String.valueOf(dblRecurso),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dblMontoDescuento),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(cc_acuse ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(NombreIf ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell((rs.getString("BENEFICIARIO")==null?"":rs.getString("BENEFICIARIO")),"formas",ComunesPDF.LEFT);
						pdfDoc.setCell( (rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I"))?rs.getString("PORCBENEFICIARIO")+"%": "" ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell( (rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?"$ " + Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2)+" %": "","formas",ComunesPDF.RIGHT);				
									
				} //while (rs.next())
				
							
				if (doc_nacional>0){ 
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 1);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 4);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional-nacionalNC),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dblTotalMontoDescuentoPesos-nacional_descNC),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}
					
					if (doc_dolares>0){ 
						pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 1);
						pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,4);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares-dolaresNC),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dblTotalMontoDescuentoDolares-dolares_descNC),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,5);
					}	
				
		}//fin estus 0
		if((tipo.equals("PDF") || tipo.equals("CSV")) &&  estatus.equals("1") ) {
			while (rs.next()) {
				cg_razon_social_EPO = (rs.getString("NOMBREEPO")==null)?"":rs.getString("NOMBREEPO").trim();
				cg_razon_social_PYME = (rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME").trim();
				ig_numero_docto = (rs.getString("NUMERODOCTO")==null)?"":rs.getString("NUMERODOCTO").trim();
				df_fecha_docto = (rs.getString("FECHADOCTO")==null)?"":rs.getString("FECHADOCTO").trim();
				df_fecha_venc = (rs.getString("FECHAVENC")==null)?"":rs.getString("FECHAVENC").trim();
				cg_razon_social_IF = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
				in_tasa_aceptada = rs.getDouble("TASAACEPTADA");
				plazo = rs.getString("PLAZO") == null?"0":rs.getString("PLAZO");
				cd_nombre = (rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA").trim();
				ic_moneda = rs.getInt("NUMEROMONEDA");
				fn_monto = rs.getDouble("MONTODOCTO");
				dblPorcentaje = rs.getDouble("PORCANTICIPO");
				fn_monto_dscto = rs.getDouble("MONTODSCTO");
				in_importe_interes = rs.getDouble("IMPORTEINTERES");
				in_importe_recibir = rs.getDouble("IMPORTERECIBIR");
				// Para factoraje Vencido
				nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE");
				tipoFactoraje = (rs.getString("TIPOFACTORAJE")==null)?"":rs.getString("TIPOFACTORAJE");
				df_fecha_registro_operacion = (rs.getString("df_programacion")==null)?"":rs.getString("df_programacion"); // Fodea 005-2009 24 hrs
				tipoTasa = (rs.getString("tipoTasa")==null)?"":rs.getString("tipoTasa");
				
				///Fodea 17 2013
				operaFiso= (rs.getString("BANDERA_FISO")==null)?"":rs.getString("BANDERA_FISO");
				cg_razon_social_FID=(rs.getString("NOMBRE_FID")==null)?"":rs.getString("NOMBRE_FID").trim();
				in_tasa_aceptada_FID=(rs.getString("TASAACEPTADA_FID")==null)?"0":rs.getString("TASAACEPTADA_FID").trim();
				if(operaFiso.equals("N"))
				{
					in_importe_interes_FID_str =(rs.getString("IMPORTEINTERES_FID")==null)?"":rs.getString("IMPORTEINTERES_FID");
					in_importe_recibir_FID_str =(rs.getString("IMPORTERECIBIR_FID")==null)?"":rs.getString("IMPORTERECIBIR_FID");
					in_importe_interes_FID = 0;
					in_importe_recibir_FID = 0;
				}else
				{
					in_importe_interes_FID = rs.getDouble("IMPORTEINTERES_FID");
					in_importe_recibir_FID = rs.getDouble("IMPORTERECIBIR_FID");
				}
				
				NombreIf = "";
				if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I")) {
					fn_monto_dscto = fn_monto;
					dblPorcentaje = 100;
				}
		
				dblRecurso = fn_monto - fn_monto_dscto;
		
				if(!tipoFactoraje.equals("C")) { // No se toma en cuenta para la suma de los montos las Notas de Cr�dito.
					if (ic_moneda == 1) {
						if(operaFiso.equals("N"))
					{
						nacional+=fn_monto;
						nacional_desc+=fn_monto_dscto;
						nacional_int+=in_importe_interes;
						nacional_descIF+=in_importe_recibir;
						nacionalDocto = true;
						doc_nacional++;
					}
					else if( operaFiso.equals("S") ){
							nacionalDocto_fid = true;
							nacional_fid+=fn_monto;
							nacional_desc_fid+=fn_monto_dscto;
							nacional_int_fid+=in_importe_interes;
							nacional_descIF_fid+=in_importe_recibir;
							doc_nacional_fid++;
							nacional_int_fiso_s+=in_importe_interes_FID;
							nacional_descIF_fiso_s+=in_importe_recibir_FID;
					}
					}
					if(ic_moneda == 54) {
						if(operaFiso.equals("N"))
						{
							dolares+=fn_monto;
							dolares_desc+=fn_monto_dscto;
							dolares_int+=in_importe_interes;
							dolares_descIF+=in_importe_recibir;
							dolaresDocto = true;
							doc_dolares++;
						}
						else if( operaFiso.equals("S") ){
							dolares_fid+=fn_monto;
								dolares_desc_fid+=fn_monto_dscto;
								dolares_int_fid+=in_importe_interes;
								dolares_descIF_fid+=in_importe_recibir;
								dolaresDocto_fid = true;
								doc_dolares_fid++;
								dolares_int_fiso_s+=in_importe_interes_fid;
								dolares_descIF_fiso_s+=in_importe_recibir_fid;
						}
					}
				}
				if(tipo.equals("PDF"))
				{
					pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_EPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_PYME,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_venc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cd_nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);						
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+ Comunes.formatoMoneda2(String.valueOf(dblRecurso),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cg_razon_social_IF,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cg_razon_social_FID,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada_FID+"%","formas",ComunesPDF.CENTER);
										
					pdfDoc.setCell("B","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(operaFiso.equals("N")?in_importe_interes_FID_str:"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes_FID),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(operaFiso.equals("N")?in_importe_recibir_FID_str:"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir_FID),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?"$" + Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2): "","formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("BENEFICIARIO"): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("PORCBENEFICIARIO")+"%": "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I"))?"$" + Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell(df_fecha_registro_operacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
				}
				if(tipo.equals("CSV"))
				{
					contenidoArchivo.append(cg_razon_social_EPO.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(cg_razon_social_PYME.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(ig_numero_docto.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(df_fecha_docto.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(df_fecha_venc.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(plazo.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(cd_nombre.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(nombreTipoFactoraje.trim().replaceAll(",", "")+",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false)+"\",");
					contenidoArchivo.append("\""+Comunes.formatoMoneda2(String.valueOf(dblPorcentaje),false)+"%"+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(dblRecurso),false)+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false)+"\",");
					
					contenidoArchivo.append(cg_razon_social_IF.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(in_tasa_aceptada+",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes),false)+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false)+"\",");
					
					//Fodea 17 2013
					contenidoArchivo.append(cg_razon_social_FID.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(in_tasa_aceptada_FID+",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes_FID),false)+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir_FID),false)+"\",");
					
					if( rs.getString("TIPOFACTORAJE").equals("D")  ||  rs.getString("TIPOFACTORAJE").equals("I")  ){
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2)+"\",");
					contenidoArchivo.append(rs.getString("BENEFICIARIO").trim().replaceAll(",", "")+"\",");
					contenidoArchivo.append(rs.getString("PORCBENEFICIARIO").trim().replaceAll(",", "")+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2)+"\",");
					}else {
					contenidoArchivo.append(",,,,");
					}
					contenidoArchivo.append(df_fecha_registro_operacion.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(tipoTasa.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(" \n "); 
						i++;
				}
		} //while (rs.next())
				
							
				if (doc_nacional>0){ 
					if(tipo.equals("PDF"))
					{
						pdfDoc.setCell("Totales Normal","celda01",ComunesPDF.CENTER, 19);
						
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
					}
				}
				if (nacionalDocto_fid) { 
					if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 19);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_nacional_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
				}
					
					if (doc_dolares>0){
						if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
					}
					if (dolaresDocto_fid){ 
						if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 19);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
					}
				
		}//fin 1
		if((tipo.equals("PDF") || tipo.equals("CSV")) &&  estatus.equals("2") ) {
			while (rs.next()) {
				cg_razon_social_EPO = (rs.getString("NOMBREEPO")==null)?"":rs.getString("NOMBREEPO").trim();
				cg_razon_social_PYME = (rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME").trim();
				ig_numero_docto = (rs.getString("NUMERODOCTO")==null)?"":rs.getString("NUMERODOCTO").trim();
				df_fecha_docto = (rs.getString("FECHADOCTO")==null)?"":rs.getString("FECHADOCTO").trim();
				df_fecha_venc = (rs.getString("FECHAVENC")==null)?"":rs.getString("FECHAVENC").trim();
				cg_razon_social_IF = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
				in_tasa_aceptada = rs.getDouble("TASAACEPTADA");
				plazo = rs.getString("PLAZO") == null?"0":rs.getString("PLAZO");
				cd_nombre = (rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA").trim();
				ic_moneda = rs.getInt("NUMEROMONEDA");
				fn_monto = rs.getDouble("MONTODOCTO");
				dblPorcentaje = rs.getDouble("PORCANTICIPO");
				fn_monto_dscto = rs.getDouble("MONTODSCTO");
				in_importe_interes = rs.getDouble("IMPORTEINTERES");
				in_importe_recibir = rs.getDouble("IMPORTERECIBIR");
				tipoTasa = (rs.getString("tipoTasa")==null)?"":rs.getString("tipoTasa").trim();
								
				///Fodea 17 2013
				operaFiso= (rs.getString("BANDERA_FISO")==null)?"":rs.getString("BANDERA_FISO");
				cg_razon_social_FID=(rs.getString("NOMBRE_FID")==null)?"":rs.getString("NOMBRE_FID").trim();
				in_tasa_aceptada_FID=(rs.getString("TASAACEPTADA_FID")==null)?"0":rs.getString("TASAACEPTADA_FID").trim();
				if(operaFiso.equals("N"))
				{
					in_importe_interes_FID_str =(rs.getString("IMPORTEINTERES_FID")==null)?"":rs.getString("IMPORTEINTERES_FID");
					in_importe_recibir_FID_str =(rs.getString("IMPORTERECIBIR_FID")==null)?"":rs.getString("IMPORTERECIBIR_FID");
					in_importe_interes_FID = 0;
					in_importe_recibir_FID = 0;
				}else
				{
					in_importe_interes_FID = rs.getDouble("IMPORTEINTERES_FID");
					in_importe_recibir_FID = rs.getDouble("IMPORTERECIBIR_FID");
				}
		
				// Para factoraje Vencido
				nombreTipoFactoraje  = (rs.getString("nombre_tipo_factoraje")==null)?"":rs.getString("nombre_tipo_factoraje");
				tipoFactoraje = (rs.getString("TIPOFACTORAJE")==null)?"":rs.getString("TIPOFACTORAJE");
				NombreIf = "";
				if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I")) {
					fn_monto_dscto = fn_monto;
					dblPorcentaje = 100;
				}
		
				dblRecurso = fn_monto - fn_monto_dscto;
		
				if (ic_moneda == 1) {
					if(operaFiso.equals("N"))
					{
						nacional+=fn_monto;
						nacional_desc+=fn_monto_dscto;
						nacional_int+=in_importe_interes;
						nacional_descIF+=in_importe_recibir;
						nacionalDocto = true;
						doc_nacional++;
					}
					else if( operaFiso.equals("S") ){
							nacionalDocto_fid = true;
							nacional_fid+=fn_monto;
							nacional_desc_fid+=fn_monto_dscto;
							nacional_int_fid+=in_importe_interes;
							nacional_descIF_fid+=in_importe_recibir;
							doc_nacional_fid++;
							nacional_int_fiso_s+=in_importe_interes_FID;
							nacional_descIF_fiso_s+=in_importe_recibir_FID;
					}
				}
				
		
				if(ic_moneda == 54)	{
					if(operaFiso.equals("N"))
					{
						dolares+=fn_monto;
						dolares_desc+=fn_monto_dscto;
						dolares_int+=in_importe_interes;
						dolares_descIF+=in_importe_recibir;
						dolaresDocto = true;
						doc_dolares++;
					}
					else if( operaFiso.equals("S") ){
						dolares_fid+=fn_monto;
							dolares_desc_fid+=fn_monto_dscto;
							dolares_int_fid+=in_importe_interes;
							dolares_descIF_fid+=in_importe_recibir;
							dolaresDocto_fid = true;
							doc_dolares_fid++;
							dolares_int_fiso_s+=in_importe_interes_fid;
							dolares_descIF_fiso_s+=in_importe_recibir_fid;
					}
				}
				if(tipo.equals("PDF"))
				{
					pdfDoc.setCell("A","formas",ComunesPDF.CENTER);		
					pdfDoc.setCell(cg_razon_social_EPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_PYME,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_venc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cd_nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);						
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+ Comunes.formatoMoneda2(String.valueOf(dblRecurso),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cg_razon_social_IF,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cg_razon_social_FID,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada_FID+"%","formas",ComunesPDF.CENTER);
										
					pdfDoc.setCell("B","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(operaFiso.equals("N")?in_importe_interes_FID_str:"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes_FID),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(operaFiso.equals("N")?in_importe_recibir_FID_str:"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir_FID),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?"$" + Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2): "","formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("BENEFICIARIO"): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("PORCBENEFICIARIO")+"%": "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I"))?"$" + Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
				}
				if(tipo.equals("CSV"))
				{
					contenidoArchivo.append(cg_razon_social_EPO.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(cg_razon_social_PYME.trim().replaceAll(",", "")+",");			
					contenidoArchivo.append(ig_numero_docto.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(df_fecha_docto.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(df_fecha_venc.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(plazo.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(cd_nombre.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(nombreTipoFactoraje.trim().replaceAll(",", "")+",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false)+"\",");
					contenidoArchivo.append("\""+Comunes.formatoMoneda2(String.valueOf(dblPorcentaje),false)+"%"+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(dblRecurso),false)+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false)+"\",");
					
					contenidoArchivo.append(cg_razon_social_IF.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(in_tasa_aceptada+",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes),false)+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false)+"\",");
					
					//Fodea 17 2013
					contenidoArchivo.append(cg_razon_social_FID.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(in_tasa_aceptada_FID.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(operaFiso.equals("N")?in_importe_interes_FID_str+",":"\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes_FID),false)+"\",");
					contenidoArchivo.append(operaFiso.equals("N")?in_importe_recibir_FID_str+",":"\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir_FID),false)+"\",");
					
					
					if( rs.getString("TIPOFACTORAJE").equals("D")  ||  rs.getString("TIPOFACTORAJE").equals("I")  ){
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2)+"\",");
					contenidoArchivo.append(rs.getString("BENEFICIARIO").trim().replaceAll(",", "")+"\",");	
					contenidoArchivo.append(rs.getString("PORCBENEFICIARIO").trim().replaceAll(",", "")+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2)+"\",");
					}else {
					contenidoArchivo.append(",,,,");
					}	
					contenidoArchivo.append(tipoTasa.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(" \n ");
					i++;
				}
		} //while (rs.next())
				
							
				if (doc_nacional>0){ 
					if(tipo.equals("PDF"))
					{
						
						pdfDoc.setCell("Totales Normal","celda01",ComunesPDF.CENTER, 19);
											
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
					}
				}
				if (nacionalDocto_fid) { 
					if(tipo.equals("PDF"))
						{
								pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 19);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_nacional_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
				}
					
					if (doc_dolares>0){
						if(tipo.equals("PDF"))
						{							
							pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("monto int fideicomiso","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("monto oper fideicomiso","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
					}
					if (dolaresDocto_fid){ 
						if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 19);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
					}
					
				
		}//fin 2
		if((tipo.equals("PDF") || tipo.equals("CSV")) &&  estatus.equals("3") ) {
			while (rs.next()) {
				cg_razon_social_EPO = (rs.getString("NOMBREEPO")==null)?"":rs.getString("NOMBREEPO").trim();
				cg_razon_social_PYME = (rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME").trim();
				cg_razon_social_IF = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
				ig_numero_docto = (rs.getString("NUMERODOCTO")==null)?"":rs.getString("NUMERODOCTO").trim();
				cc_acuse = (rs.getString("ACUSE3")==null)?"":rs.getString("ACUSE3").trim();
				ic_folio = (rs.getString("NUMEROSOLICITUD")==null)?"":rs.getString("NUMEROSOLICITUD").trim();
				cd_nombre = (rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA").trim();
				ic_moneda = rs.getInt("NUMEROMONEDA");
				fn_monto = rs.getDouble("MONTODOCTO");
				dblPorcentaje = rs.getDouble("PORCANTICIPO");
				fn_monto_dscto = rs.getDouble("MONTODSCTO");
				in_tasa_aceptada = rs.getDouble("TASAACEPTADA");
				in_importe_interes = rs.getDouble("IMPORTEINTERES");
				in_importe_recibir = rs.getDouble("IMPORTERECIBIR");
				origen = rs.getString(15);
				tipoTasa = (rs.getString("tipoTasa")==null?"":rs.getString("tipoTasa"));
				
				///Fodea 17 2013
				operaFiso= (rs.getString("BANDERA_FISO")==null)?"":rs.getString("BANDERA_FISO");
				cg_razon_social_FID=(rs.getString("NOMBRE_FID")==null)?"":rs.getString("NOMBRE_FID").trim();
				in_tasa_aceptada_FID=(rs.getString("TASAACEPTADA_FID")==null)?"0":rs.getString("TASAACEPTADA_FID").trim();
				if(operaFiso.equals("N"))
				{
					in_importe_interes_FID_str =(rs.getString("IMPORTEINTERES_FID")==null)?"":rs.getString("IMPORTEINTERES_FID");
					in_importe_recibir_FID_str =(rs.getString("IMPORTERECIBIR_FID")==null)?"":rs.getString("IMPORTERECIBIR_FID");
					in_importe_interes_FID = 0;
					in_importe_recibir_FID = 0;
				}else
				{
					in_importe_interes_FID = rs.getDouble("IMPORTEINTERES_FID");
					in_importe_recibir_FID = rs.getDouble("IMPORTERECIBIR_FID");
				}
				
				// Para factoraje Vencido
				nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null?"":rs.getString("NOMBRE_TIPO_FACTORAJE"));
				tipoFactoraje = (rs.getString("TIPOFACTORAJE")==null)?"":rs.getString("TIPOFACTORAJE");
				NombreIf = "";
				if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I") ) {
					fn_monto_dscto = fn_monto;
					dblPorcentaje = 100;
				}
		
				 dblRecurso = fn_monto - fn_monto_dscto;
		
				if (ic_moneda == 1) {
					if(operaFiso.equals("N"))
					{
						nacional+=fn_monto;
						nacional_desc+=fn_monto_dscto;
						nacional_int+=in_importe_interes;
						nacional_descIF+=in_importe_recibir;
						nacionalDocto = true;
						doc_nacional++;
					}
					else if( operaFiso.equals("S") ){
							nacionalDocto_fid = true;
							nacional_fid+=fn_monto;
							nacional_desc_fid+=fn_monto_dscto;
							nacional_int_fid+=in_importe_interes;
							nacional_descIF_fid+=in_importe_recibir;
							doc_nacional_fid++;
							nacional_int_fiso_s+=in_importe_interes_FID;
							nacional_descIF_fiso_s+=in_importe_recibir_FID;
					}
				}
		
				if(ic_moneda == 54) {
					if(operaFiso.equals("N"))
					{
						dolares+=fn_monto;
						dolares_desc+=fn_monto_dscto;
						dolares_int+=in_importe_interes;
						dolares_descIF+=in_importe_recibir;
						dolaresDocto = true;
						doc_dolares++;
					}
					else if( operaFiso.equals("S") ){
						dolares_fid+=fn_monto;
							dolares_desc_fid+=fn_monto_dscto;
							dolares_int_fid+=in_importe_interes;
							dolares_descIF_fid+=in_importe_recibir;
							dolaresDocto_fid = true;
							doc_dolares_fid++;
							dolares_int_fiso_s+=in_importe_interes_fid;
							dolares_descIF_fiso_s+=in_importe_recibir_fid;
					}
				}
				if(tipo.equals("PDF"))
				{
					pdfDoc.setCell("A","formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(cg_razon_social_EPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_PYME,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ic_folio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cd_nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+ Comunes.formatoMoneda2(String.valueOf(dblRecurso),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cg_razon_social_IF,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cg_razon_social_FID,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada_FID+"%","formas",ComunesPDF.CENTER);
										
					pdfDoc.setCell("B","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(operaFiso.equals("N")?in_importe_interes_FID_str:"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes_FID),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(operaFiso.equals("N")?in_importe_recibir_FID_str:"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir_FID),false),"formas",ComunesPDF.RIGHT);
					
					pdfDoc.setCell("Comunidades","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cc_acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?"$" + Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2): "","formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("BENEFICIARIO"): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("PORCBENEFICIARIO")+"%": "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I"))?"$" + Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				}
				if(tipo.equals("CSV"))
				{
					contenidoArchivo.append(cg_razon_social_EPO.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(cg_razon_social_PYME.trim().replaceAll(",", "")+",");
					
					contenidoArchivo.append(ig_numero_docto.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(ic_folio.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(cd_nombre.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(nombreTipoFactoraje.trim().replaceAll(",", "")+",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false)+"\",");
					contenidoArchivo.append("\""+Comunes.formatoMoneda2(String.valueOf(dblPorcentaje),false)+"%"+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(dblRecurso),false)+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false)+"\",");
					contenidoArchivo.append(cg_razon_social_IF.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(in_tasa_aceptada+",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes),false)+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false)+"\",");
					
					//Fodea 17 2013
					contenidoArchivo.append(cg_razon_social_FID.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(in_tasa_aceptada_FID+",");
					contenidoArchivo.append(operaFiso.equals("N")?in_importe_interes_FID_str:"\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes_FID),false)+"\",");
					contenidoArchivo.append(operaFiso.equals("N")?in_importe_recibir_FID_str:"\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir_FID),false)+"\",");
					
					
					contenidoArchivo.append("Comunidades ,");
					contenidoArchivo.append(cc_acuse+",");
				 if( rs.getString("TIPOFACTORAJE").equals("D")  ||  rs.getString("TIPOFACTORAJE").equals("I")  ){
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2)+"\",");
					contenidoArchivo.append(rs.getString("BENEFICIARIO").trim().replaceAll(",", "")+"\",");
					contenidoArchivo.append(rs.getString("PORCBENEFICIARIO").trim().replaceAll(",", "")+"\",");	
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2)+"\",");
					}else {
					contenidoArchivo.append(",,,,");
					}	
					contenidoArchivo.append(tipoTasa.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(" \n "); 
					i++;
				}
		} //while (rs.next())
				
							
			if (doc_nacional>0){ 
					if(tipo.equals("PDF"))
					{
						
						pdfDoc.setCell("Totales Normal","celda01",ComunesPDF.CENTER, 19);
											
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
					}
				}
				if (nacionalDocto_fid) { 
					if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 19);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_nacional_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
				}
					
					if (doc_dolares>0){
						if(tipo.equals("PDF"))
						{	
							pdfDoc.setCell("Totales Normal","celda01",ComunesPDF.CENTER, 19);
							pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
					}
					if (dolaresDocto_fid){ 
						if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 19);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
					}
				
		}//fin 3
		if((tipo.equals("PDF") || tipo.equals("CSV")) &&  estatus.equals("4") ) {
			while (rs.next()) {
				cg_razon_social_EPO = (rs.getString("NOMBREEPO")==null)?"":rs.getString("NOMBREEPO").trim();
				cg_razon_social_PYME = (rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME").trim();
				cg_razon_social_IF = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
				ic_folio = (rs.getString("NUMEROSOLICITUD")==null)?"":rs.getString("NUMEROSOLICITUD").trim();
				ig_numero_docto = (rs.getString("NUMERODOCTO")==null)?"":rs.getString("NUMERODOCTO").trim();
				cd_nombre = (rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA").trim();
				ic_moneda = rs.getInt("NUMEROMONEDA");
				fn_monto = rs.getDouble("MONTODOCTO");
				dblPorcentaje = rs.getDouble("PORCANTICIPO");
				fn_monto_dscto = rs.getDouble("MONTODSCTO");
				in_tasa_aceptada = rs.getDouble("TASAACEPTADA");
				in_importe_interes = rs.getDouble("IMPORTEINTERES");
				in_importe_recibir = rs.getDouble("IMPORTERECIBIR");
				
				///Fodea 17 2013
				operaFiso= (rs.getString("BANDERA_FISO")==null)?"":rs.getString("BANDERA_FISO");
				cg_razon_social_FID=(rs.getString("NOMBRE_FID")==null)?"":rs.getString("NOMBRE_FID").trim();
				in_tasa_aceptada_FID=(rs.getString("TASAACEPTADA_FID")==null)?"0":rs.getString("TASAACEPTADA_FID").trim();
				if(operaFiso.equals("N"))
				{
					in_importe_interes_FID_str =(rs.getString("IMPORTEINTERES_FID")==null)?"":rs.getString("IMPORTEINTERES_FID");
					in_importe_recibir_FID_str =(rs.getString("IMPORTERECIBIR_FID")==null)?"":rs.getString("IMPORTERECIBIR_FID");
					in_importe_interes_FID = 0;
					in_importe_recibir_FID = 0;
				}else
				{
					in_importe_interes_FID = rs.getDouble("IMPORTEINTERES_FID");
					in_importe_recibir_FID = rs.getDouble("IMPORTERECIBIR_FID");
				}
		
		
				// Para factoraje Vencido
				nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null?"":rs.getString("NOMBRE_TIPO_FACTORAJE"));
				tipoFactoraje = (rs.getString("TIPOFACTORAJE")==null)?"":rs.getString("TIPOFACTORAJE");
				NombreIf = "";
				if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I")) {
					fn_monto_dscto = fn_monto;
					dblPorcentaje = 100;
				}
		
				dblRecurso = fn_monto - fn_monto_dscto;
		
				if (ic_moneda == 1) {
					if(operaFiso.equals("N"))
					{
						nacional+=fn_monto;
						nacional_desc+=fn_monto_dscto;
						nacional_int+=in_importe_interes;
						nacional_descIF+=in_importe_recibir;
						nacionalDocto = true;
						doc_nacional++;
					}
					else if( operaFiso.equals("S") ){
							nacionalDocto_fid = true;
							nacional_fid+=fn_monto;
							nacional_desc_fid+=fn_monto_dscto;
							nacional_int_fid+=in_importe_interes;
							nacional_descIF_fid+=in_importe_recibir;
							doc_nacional_fid++;
							nacional_int_fiso_s+=in_importe_interes_FID;
							nacional_descIF_fiso_s+=in_importe_recibir_FID;
					}
				}
		
				if(ic_moneda == 54) {
					if(operaFiso.equals("N"))
					{
						dolares+=fn_monto;
						dolares_desc+=fn_monto_dscto;
						dolares_int+=in_importe_interes;
						dolares_descIF+=in_importe_recibir;
						dolaresDocto = true;
						doc_dolares++;
					}
					else if( operaFiso.equals("S") ){
						dolares_fid+=fn_monto;
							dolares_desc_fid+=fn_monto_dscto;
							dolares_int_fid+=in_importe_interes;
							dolares_descIF_fid+=in_importe_recibir;
							dolaresDocto_fid = true;
							doc_dolares_fid++;
							dolares_int_fiso_s+=in_importe_interes_fid;
							dolares_descIF_fiso_s+=in_importe_recibir_fid;
					}
				}
				if(tipo.equals("PDF"))
				{
					pdfDoc.setCell("A","formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(cg_razon_social_EPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_PYME,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ic_folio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cd_nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+ Comunes.formatoMoneda2(String.valueOf(dblRecurso),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cg_razon_social_IF,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cg_razon_social_FID,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada_FID+"%","formas",ComunesPDF.CENTER);
										
					pdfDoc.setCell("B","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(operaFiso.equals("N")?in_importe_interes_FID_str:"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes_FID),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(operaFiso.equals("N")?in_importe_recibir_FID_str:"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir_FID),false),"formas",ComunesPDF.RIGHT);
					
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?"$" + Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2): "","formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("BENEFICIARIO"): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("PORCBENEFICIARIO")+"%": "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I"))?"$" + Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				 }
			
		} //while (rs.next())
				
							
				if (doc_nacional>0){ 
					if(tipo.equals("PDF"))
					{
						
						pdfDoc.setCell("Totales Normal","celda01",ComunesPDF.CENTER, 17);
											
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,14);
					}
				}
				if (nacionalDocto_fid) { 
					if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 17);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_nacional_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,14);
						}
				}
					
					if (doc_dolares>0){
						if(tipo.equals("PDF"))
						{							
							pdfDoc.setCell("Total USD","celda01",ComunesPDF.CENTER, 17);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,14);
						}
					}
					if (dolaresDocto_fid){ 
						if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 17);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,14);
						}
					}
				
		}//fin 4
		if((tipo.equals("PDF") || tipo.equals("CSV")) &&  estatus.equals("5") ) {
			while (rs.next()) {
				cg_razon_social_EPO = (rs.getString("NOMBREEPO")==null)?"":rs.getString("NOMBREEPO").trim();
				cg_razon_social_PYME = (rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME").trim();
				cg_razon_social_IF = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
				ic_folio = (rs.getString("NUMEROSOLICITUD")==null)?"":rs.getString("NUMEROSOLICITUD").trim();
				ig_numero_docto = (rs.getString("NUMERODOCTO")==null)?"":rs.getString("NUMERODOCTO").trim();
				cd_nombre = (rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA").trim();
				ic_moneda = rs.getInt("NUMEROMONEDA");
				fn_monto = rs.getDouble("MONTODOCTO");
				dblPorcentaje = rs.getDouble("PORCANTICIPO");
				fn_monto_dscto = rs.getDouble("MONTODSCTO");
				in_tasa_aceptada = rs.getDouble("TASAACEPTADA");
				in_importe_recibir = rs.getDouble("IMPORTERECIBIR");
				tipoTasa = (rs.getString("tipoTasa")==null?"":rs.getString("tipoTasa"));
				
				///Fodea 17 2013
				operaFiso= (rs.getString("BANDERA_FISO")==null)?"":rs.getString("BANDERA_FISO");
				cg_razon_social_FID=(rs.getString("NOMBRE_FID")==null)?"":rs.getString("NOMBRE_FID").trim();
				in_tasa_aceptada_FID=(rs.getString("TASAACEPTADA_FID")==null)?"0":rs.getString("TASAACEPTADA_FID").trim();
				if(operaFiso.equals("N"))
				{
				//	in_importe_interes_FID_str =(rs.getString("IMPORTEINTERES_FID")==null)?"":rs.getString("IMPORTEINTERES_FID");
					in_importe_recibir_FID_str =(rs.getString("IMPORTERECIBIR_FID")==null)?"":rs.getString("IMPORTERECIBIR_FID");
					in_importe_interes_FID = 0;
					in_importe_recibir_FID = 0;
				}else
				{
				//	in_importe_interes_FID = rs.getDouble("IMPORTEINTERES_FID");
					in_importe_recibir_FID = rs.getDouble("IMPORTERECIBIR_FID");
				}
				
				// Para factoraje Vencido
				nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null?"":rs.getString("NOMBRE_TIPO_FACTORAJE"));
				tipoFactoraje = (rs.getString("TIPOFACTORAJE")==null)?"":rs.getString("TIPOFACTORAJE");
				NombreIf = "";
				if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I")) {
					fn_monto_dscto = fn_monto;
					dblPorcentaje = 100;
				}
		
				 dblRecurso = fn_monto - fn_monto_dscto;
		
				if(!tipoFactoraje.equals("C")) { // No se toma en cuenta para la suma de los montos las Notas de Cr�dito.
					if (ic_moneda == 1) {
						if(operaFiso.equals("N"))
					{
						nacional+=fn_monto;
						nacional_desc+=fn_monto_dscto;
						nacional_descIF+=in_importe_recibir;
						nacionalDocto = true;
						doc_nacional++;
					}
					else if( operaFiso.equals("S") ){
							nacionalDocto_fid = true;
							nacional_fid+=fn_monto;
							nacional_desc_fid+=fn_monto_dscto;
							nacional_descIF_fid+=in_importe_recibir;
							doc_nacional_fid++;
							nacional_int_fiso_s+=in_importe_interes_FID;
							nacional_descIF_fiso_s+=in_importe_recibir_FID;
					}
					}
					if(ic_moneda == 54) {
						if(operaFiso.equals("N"))
					{
						dolares+=fn_monto;
						dolares_desc+=fn_monto_dscto;
						dolares_descIF+=in_importe_recibir;
						dolaresDocto = true;
						doc_dolares++;
					}
					else if( operaFiso.equals("S") ){
						dolares_fid+=fn_monto;
							dolares_desc_fid+=fn_monto_dscto;
							dolares_descIF_fid+=in_importe_recibir;
							dolaresDocto_fid = true;
							doc_dolares_fid++;
							dolares_int_fiso_s+=in_importe_interes_fid;
							dolares_descIF_fiso_s+=in_importe_recibir_fid;
					}
					}
				}
				if(tipo.equals("PDF"))
				{
					pdfDoc.setCell("A","formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(cg_razon_social_EPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_PYME,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ic_folio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cd_nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+ Comunes.formatoMoneda2(String.valueOf(dblRecurso),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cg_razon_social_IF,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cg_razon_social_FID,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada_FID+"%","formas",ComunesPDF.CENTER);
					
					pdfDoc.setCell("B","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(operaFiso.equals("N")?in_importe_recibir_FID_str:"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir_FID),false),"formas",ComunesPDF.RIGHT);
					
					
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?"$" + Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2): "","formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("BENEFICIARIO"): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("PORCBENEFICIARIO")+"%": "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I"))?"$" + Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell("","formas",ComunesPDF.CENTER, 10);
				 }
				 if(tipo.equals("CSV"))
				 {
					 i++;		
					contenidoArchivo.append(cg_razon_social_EPO.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(cg_razon_social_PYME.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(ic_folio.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(ig_numero_docto.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(cd_nombre.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(nombreTipoFactoraje.trim().replaceAll(",", "")+",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false)+"\",");
					contenidoArchivo.append("\""+Comunes.formatoMoneda2(String.valueOf(dblPorcentaje),false)+"%"+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(dblRecurso),false)+"\",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false)+"\",");
					contenidoArchivo.append(cg_razon_social_IF.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(in_tasa_aceptada+",");
					contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false)+"\",");
					
					//Fodea 17 2013
					contenidoArchivo.append(cg_razon_social_FID.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(in_tasa_aceptada_FID+",");
					//contenidoArchivo.append(operaFiso.equals("N")?in_importe_interes_FID_str:"\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes_FID),false)+"\",");
					contenidoArchivo.append(operaFiso.equals("N")?in_importe_recibir_FID_str+",":"\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir_FID),false)+"\",");
					
			
					if( rs.getString("TIPOFACTORAJE").equals("D")  ||  rs.getString("TIPOFACTORAJE").equals("I")  ){
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2)+"\",");
					contenidoArchivo.append(rs.getString("BENEFICIARIO").trim().replaceAll(",", "")+"\",");
					contenidoArchivo.append(rs.getString("PORCBENEFICIARIO").trim().replaceAll(",", "")+"\",");	
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2)+"\",");
					}else {
					contenidoArchivo.append(",,,,");
					}	
					contenidoArchivo.append(tipoTasa.trim().replaceAll(",", "")+",");
					contenidoArchivo.append(" \n ");
				 }
			
		} //while (rs.next())							
				if (doc_nacional>0){ 
					if(tipo.equals("PDF"))
					{
						
						pdfDoc.setCell("Totales Normal","celda01",ComunesPDF.CENTER, 16);
											
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						//pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);						
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,14);
					}
				}
				if (nacionalDocto_fid) { 
					if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 16);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_nacional_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							//pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							//pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,14);
						}
				}
					
					if (doc_dolares>0){
						if(tipo.equals("PDF"))
						{		
						
							pdfDoc.setCell("Total USD","celda01",ComunesPDF.CENTER,16);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							//pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);						
							pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,14);
						}
					}
					if (dolaresDocto_fid){ 
						if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 16);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							//pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							//pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,14);
							
						}
					}
				
		}//fin5
		if((tipo.equals("PDF") || tipo.equals("CSV")) &&  estatus.equals("6") ) {
			while (rs.next()) {
				cg_razon_social_EPO = (rs.getString("NOMBREEPO")==null)?"":rs.getString("NOMBREEPO").trim();
				cg_razon_social_PYME = (rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME").trim();
				cg_razon_social_IF = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
				ig_numero_docto = (rs.getString("NUMERODOCTO")==null)?"":rs.getString("NUMERODOCTO").trim();
				cc_acuse = (rs.getString("ACUSE3")==null)?"":rs.getString("ACUSE3").trim();
				ic_folio = (rs.getString("NUMEROSOLICITUD")==null)?"":rs.getString("NUMEROSOLICITUD").trim();
				cd_nombre = (rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA").trim();
				ic_moneda = rs.getInt("NUMEROMONEDA");
				fn_monto = rs.getDouble("MONTODOCTO");
				dblPorcentaje = rs.getDouble("PORCANTICIPO");
				fn_monto_dscto = rs.getDouble("MONTODSCTO");
				in_tasa_aceptada = rs.getDouble("TASAACEPTADA");
				in_importe_interes = rs.getDouble("IMPORTEINTERES");
				in_importe_recibir = rs.getDouble("IMPORTERECIBIR");
				origen = rs.getString(15);
				tipoTasa = (rs.getString("tipoTasa")==null?"":rs.getString("tipoTasa"));
				// Para factoraje Vencido
				nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null?"":rs.getString("NOMBRE_TIPO_FACTORAJE"));
				tipoFactoraje = (rs.getString("TIPOFACTORAJE")==null)?"":rs.getString("TIPOFACTORAJE");
			
				NombreIf = "";
				if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I") ) {
					fn_monto_dscto = fn_monto;
					dblPorcentaje = 100;
				}
		
				 dblRecurso = fn_monto - fn_monto_dscto;
		
				if (ic_moneda == 1) {
					nacional+=fn_monto;
					nacional_desc+=fn_monto_dscto;
					nacional_int+=in_importe_interes;
					nacional_descIF+=in_importe_recibir;
					nacionalDocto = true;
					doc_nacional++;
				}
		
				if(ic_moneda == 54) {
					dolares+=fn_monto;
					dolares_desc+=fn_monto_dscto;
					dolares_int+=in_importe_interes;
					dolares_descIF+=in_importe_recibir;
					dolaresDocto = true;
					doc_dolares++;
				}
				if(tipo.equals("PDF"))
				{
					pdfDoc.setCell(cg_razon_social_EPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_PYME,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_IF,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ic_folio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cd_nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+ Comunes.formatoMoneda2(String.valueOf(dblRecurso),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(in_tasa_aceptada+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("Comunidades","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cc_acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?"$" + Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2): "","formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("BENEFICIARIO"): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("PORCBENEFICIARIO")+"%": "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I"))?"$" + Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2): "","formas",ComunesPDF.RIGHT);				
				   pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.CENTER);
				 }
				 if(tipo.equals("CSV"))
				 {
					 	i++;
						contenidoArchivo.append(cg_razon_social_EPO.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(cg_razon_social_PYME.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(cg_razon_social_IF.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(ig_numero_docto.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(ic_folio.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(cd_nombre.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(nombreTipoFactoraje.trim().replaceAll(",", "")+",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false)+"\",");
						contenidoArchivo.append("\""+Comunes.formatoMoneda2(String.valueOf(dblPorcentaje),false)+"%"+"\",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(dblRecurso),false)+"\",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false)+"\",");
						contenidoArchivo.append(in_tasa_aceptada+",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes),false)+"\",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false)+"\",");
						contenidoArchivo.append("Comunidades ,");
						contenidoArchivo.append(cc_acuse+",");
					 if( rs.getString("TIPOFACTORAJE").equals("D")  ||  rs.getString("TIPOFACTORAJE").equals("I")  ){
						contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2)+"\",");
						contenidoArchivo.append(rs.getString("BENEFICIARIO").trim().replaceAll(",", "")+"\",");
						contenidoArchivo.append(rs.getString("PORCBENEFICIARIO").trim().replaceAll(",", "")+"\",");	
						contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2)+"\",");
						}else {
						contenidoArchivo.append(",,,,");
						}	
						contenidoArchivo.append(tipoTasa.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(" \n ");
				 }
			
		} //while (rs.next())							
				if (doc_nacional>0){ 
					if(tipo.equals("PDF"))
					{
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,7);
					}
				}
					
					if (doc_dolares>0){
						if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
							pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,7);
						}
					}	
				
		}//fin 6
		if((tipo.equals("PDF") || tipo.equals("CSV")) &&  estatus.equals("7") ) {
			while (rs.next()) {
				cg_razon_social_EPO = (rs.getString("NOMBREEPO")==null)?"":rs.getString("NOMBREEPO").trim();
				cg_razon_social_PYME = (rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME").trim();
				cg_razon_social_IF = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
				ic_folio = (rs.getString("NUMEROSOLICITUD")==null)?"":rs.getString("NUMEROSOLICITUD").trim();
				ig_numero_docto = (rs.getString("NUMERODOCTO")==null)?"":rs.getString("NUMERODOCTO").trim();
				cd_nombre = (rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA").trim();
				ic_moneda = rs.getInt("NUMEROMONEDA");
				fn_monto = rs.getDouble("MONTODOCTO");
				dblPorcentaje = rs.getDouble("PORCANTICIPO");
				in_tasa_aceptada = rs.getDouble("TASAACEPTADA");
				fn_monto_dscto = rs.getDouble("MONTODSCTO");
				df_fecha_venc = (rs.getString("FECHAVENC")==null)?"":rs.getString("FECHAVENC").trim();
				tipoTasa = (rs.getString("tipoTasa")==null)?"":rs.getString("tipoTasa").trim();
				
				///Fodea 17 2013
				operaFiso= (rs.getString("BANDERA_FISO")==null)?"":rs.getString("BANDERA_FISO");
				cg_razon_social_FID=(rs.getString("NOMBRE_FID")==null)?"":rs.getString("NOMBRE_FID").trim();
				in_tasa_aceptada_FID=(rs.getString("TASAACEPTADA_FID")==null)?"0":rs.getString("TASAACEPTADA_FID").trim();
			
				// Para factoraje Vencido
				nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null?"":rs.getString("NOMBRE_TIPO_FACTORAJE"));
				tipoFactoraje = (rs.getString("TIPOFACTORAJE")==null)?"":rs.getString("TIPOFACTORAJE");
				NombreIf = "";
				if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I")) {
					fn_monto_dscto = fn_monto;
					dblPorcentaje = 100;
				}
		
				dblRecurso = fn_monto - fn_monto_dscto;
		
				if(!tipoFactoraje.equals("C")) { // No se toma en cuenta para la suma de los montos las Notas de Cr�dito.
					if (ic_moneda == 1) {
					if(operaFiso.equals("N"))
					{
						nacional+=fn_monto;
						nacional_desc+=fn_monto_dscto;
						nacionalDocto = true;
						doc_nacional++;
					}
					else if( operaFiso.equals("S") ){
							nacionalDocto_fid = true;
							nacional_fid+=fn_monto;
							nacional_desc_fid+=fn_monto_dscto;
							doc_nacional_fid++;
					}
						
					}
					if(ic_moneda == 54) {
						if(operaFiso.equals("N"))
						{
							dolares+=fn_monto;
							dolares_desc+=fn_monto_dscto;
							dolaresDocto = true;
							doc_dolares++;					
						}
						else if( operaFiso.equals("S") ){
							dolares_fid+=fn_monto;
							dolares_desc_fid+=fn_monto_dscto;
							dolaresDocto_fid = true;
							doc_dolares_fid++;
						}
					}
				}
				if(tipo.equals("PDF"))
				{
					pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_EPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_PYME,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ic_folio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cd_nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+ Comunes.formatoMoneda2(String.valueOf(dblRecurso),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cg_razon_social_IF,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada+"","formas",ComunesPDF.CENTER);
					
					//Fodea 17 2013
					pdfDoc.setCell(cg_razon_social_FID,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada_FID,"formas",ComunesPDF.CENTER);

					pdfDoc.setCell(df_fecha_venc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_venc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?"$" + Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2): "","formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("BENEFICIARIO"): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("PORCBENEFICIARIO")+"%": "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I"))?"$" + Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER, 11);
				 }
				 if(tipo.equals("CSV"))
				 {
					 	i++;
						contenidoArchivo.append(cg_razon_social_EPO.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(cg_razon_social_PYME.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(ic_folio.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(ig_numero_docto.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(cd_nombre.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(nombreTipoFactoraje.trim().replaceAll(",", "")+",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false)+"\",");
						contenidoArchivo.append("\""+Comunes.formatoMoneda2(String.valueOf(dblPorcentaje),false)+"%"+"\",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(dblRecurso),false)+"\",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false)+"\",");
						contenidoArchivo.append(cg_razon_social_IF.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(in_tasa_aceptada+",");
						//Fodea 17 2013
						contenidoArchivo.append(cg_razon_social_FID.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(in_tasa_aceptada_FID+",");
										
						contenidoArchivo.append(df_fecha_venc.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(df_fecha_venc.trim().replaceAll(",", "")+",");
						if( rs.getString("TIPOFACTORAJE").equals("D")  ||  rs.getString("TIPOFACTORAJE").equals("I")  ){
							contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2)+"\",");
							contenidoArchivo.append(rs.getString("BENEFICIARIO").trim().replaceAll(",", "")+"\",");
							contenidoArchivo.append(rs.getString("PORCBENEFICIARIO").trim().replaceAll(",", "")+"\",");	
							contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2)+"\",");
							}else {
							contenidoArchivo.append(",,,,");
							}	
							contenidoArchivo.append(tipoTasa.trim().replaceAll(",", "")+",");
							contenidoArchivo.append(" \n ");
				 }
			
		} //while (rs.next())							
				if (doc_nacional>0){ 
					if(tipo.equals("PDF"))
					{
						pdfDoc.setCell("Totales Normal","celda01",ComunesPDF.CENTER, 17);
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
					}
				}
				if (nacionalDocto_fid) { 
					if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 17);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
							pdfDoc.setCell(String.valueOf(doc_nacional_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
							
						}
				}
					
					if (doc_dolares>0){
						if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Normal USD","celda01",ComunesPDF.CENTER, 17);
							pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
							pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
						}
					}	
					if (dolaresDocto_fid){ 
						if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso USD","celda01",ComunesPDF.CENTER, 16);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 3);
							pdfDoc.setCell(String.valueOf(doc_dolares_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							//pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,6);
							
							
						}
					}
				
		}//fin 7
		
		log.debug(" estatus ---------------->  "+estatus);
		
		if((tipo.equals("PDF") || tipo.equals("CSV")) &&  estatus.equals("8") ) {
			while (rs.next()) {
				cg_razon_social_EPO = (rs.getString("NOMBREEPO")==null)?"":rs.getString("NOMBREEPO").trim();
				cg_razon_social_PYME = (rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME").trim();
			
				ig_numero_docto = (rs.getString("NUMERODOCTO")==null)?"":rs.getString("NUMERODOCTO").trim();
				df_fecha_docto = (rs.getString("FECHADOCTO")==null)?"":rs.getString("FECHADOCTO").trim();
				df_fecha_venc = (rs.getString("FECHAVENC")==null)?"":rs.getString("FECHAVENC").trim();
				plazo = (rs.getString("PLAZO")==null)?"0":rs.getString("PLAZO").trim();
				cd_nombre = (rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA").trim();
				ic_moneda = rs.getInt("NUMEROMONEDA");
				fn_monto = rs.getDouble("MONTODOCTO");
				dblPorcentaje = rs.getDouble("PORCANTICIPO");
				fn_monto_dscto = rs.getDouble("MONTODSCTO");
				cg_razon_social_IF = (rs.getString("NOMBREIF")==null)?"":rs.getString("NOMBREIF").trim();
				in_tasa_aceptada = rs.getDouble("TASAACEPTADA");
				in_importe_interes = rs.getDouble("IMPORTEINTERES");
				in_importe_recibir = rs.getDouble("IMPORTERECIBIR");
				
				///Fodea 17 2013
				/*
				operaFiso= (rs.getString("BANDERA_FISO")==null)?"":rs.getString("BANDERA_FISO");
				cg_razon_social_FID=(rs.getString("NOMBRE_FID")==null)?"":rs.getString("NOMBRE_FID").trim();
				//in_tasa_aceptada_FID=(rs.getString("TASAACEPTADA_FID")==null)?"0":rs.getString("TASAACEPTADA_FID").trim();
				if(operaFiso.equals("N"))
				{
					in_importe_interes_FID_str =(rs.getString("IMPORTEINTERES_FID")==null)?"":rs.getString("IMPORTEINTERES_FID");
					in_importe_recibir_FID_str =(rs.getString("IMPORTERECIBIR_FID")==null)?"":rs.getString("IMPORTERECIBIR_FID");
					in_importe_interes_FID = 0;
					in_importe_recibir_FID = 0;
				}else
				{
					in_importe_interes_FID = rs.getDouble("IMPORTEINTERES_FID");
					in_importe_recibir_FID = rs.getDouble("IMPORTERECIBIR_FID");
				}
				*/
				// Para factoraje Vencido
				nombreTipoFactoraje = (rs.getString("NOMBRE_TIPO_FACTORAJE")==null)?"":rs.getString("NOMBRE_TIPO_FACTORAJE");
				tipoFactoraje = (rs.getString("TIPOFACTORAJE")==null)?"":rs.getString("TIPOFACTORAJE");
				fecha_reg_ope = (rs.getString("df_programacion")==null)?"":rs.getString("df_programacion"); // Fodea 005-2009 24 hrs
				tipoTasa = (rs.getString("tipoTasa")==null)?"":rs.getString("tipoTasa");
				NombreIf = "";
				if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("I")) {
					fn_monto_dscto = fn_monto;
					dblPorcentaje = 100;
				}
		
				dblRecurso = fn_monto - fn_monto_dscto;
		
				if(!tipoFactoraje.equals("C")) { // No se toma en cuenta para la suma de los montos las Notas de Cr�dito.
					if (ic_moneda == 1) {
					if(operaFiso.equals("N"))
					{
						nacional+=fn_monto;
						nacional_desc+=fn_monto_dscto;
						nacional_int+=in_importe_interes;
						nacional_descIF+=in_importe_recibir;
						nacionalDocto = true;
						doc_nacional++;
					}
					else if( operaFiso.equals("S") ){
							nacionalDocto_fid = true;
							nacional_fid+=fn_monto;
							nacional_desc_fid+=fn_monto_dscto;
							nacional_int_fid+=in_importe_interes;
							nacional_descIF_fid+=in_importe_recibir;
							doc_nacional_fid++;
							nacional_int_fiso_s+=in_importe_interes_FID;
							nacional_descIF_fiso_s+=in_importe_recibir_FID;
					}
				}
				
		
				if(ic_moneda == 54)	{
					if(operaFiso.equals("N"))
					{
						dolares+=fn_monto;
						dolares_desc+=fn_monto_dscto;
						dolares_int+=in_importe_interes;
						dolares_descIF+=in_importe_recibir;
						dolaresDocto = true;
						doc_dolares++;
					}
					else if( operaFiso.equals("S") ){
						dolares_fid+=fn_monto;
							dolares_desc_fid+=fn_monto_dscto;
							dolares_int_fid+=in_importe_interes;
							dolares_descIF_fid+=in_importe_recibir;
							dolaresDocto_fid = true;
							doc_dolares_fid++;
							dolares_int_fiso_s+=in_importe_interes_fid;
							dolares_descIF_fiso_s+=in_importe_recibir_fid;
					}
				}				
				}
				if(tipo.equals("PDF"))		{
				
					pdfDoc.setCell(cg_razon_social_EPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_PYME,"formas",ComunesPDF.CENTER);
										
					pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_fecha_venc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_razon_social_IF,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_tasa_aceptada+"","formas",ComunesPDF.CENTER);
					
				
					pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cd_nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+" %","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+ Comunes.formatoMoneda2(String.valueOf(dblRecurso),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?"$" + Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2): "","formas",ComunesPDF.CENTER);
				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("BENEFICIARIO"): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I")) ?rs.getString("PORCBENEFICIARIO")+"%": "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell((rs.getString("TIPOFACTORAJE").equals("D")) || (rs.getString("TIPOFACTORAJE").equals("I"))?"$" + Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2): "","formas",ComunesPDF.RIGHT);				
					pdfDoc.setCell(fecha_reg_ope,"formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.CENTER);
				 }
				 if(tipo.equals("CSV"))
				 {
					 	i++;					
						contenidoArchivo.append(cg_razon_social_EPO.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(cg_razon_social_PYME.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(ig_numero_docto.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(df_fecha_docto.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(df_fecha_venc.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(cg_razon_social_IF.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(in_tasa_aceptada+",");
						contenidoArchivo.append(plazo.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(cd_nombre.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(nombreTipoFactoraje.trim().replaceAll(",", "")+",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto),false)+"\",");
						contenidoArchivo.append("\""+Comunes.formatoMoneda2(String.valueOf(dblPorcentaje),false)+"%"+"\",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(dblRecurso),false)+"\",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(fn_monto_dscto),false)+"\",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_interes),false)+"\",");
						contenidoArchivo.append("\"$"+Comunes.formatoMoneda2(String.valueOf(in_importe_recibir),false)+"\",");
									
					
						if( rs.getString("TIPOFACTORAJE").equals("D")  ||  rs.getString("TIPOFACTORAJE").equals("I")  ){
						contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTENETO"), 2)+"\",");
						contenidoArchivo.append(rs.getString("BENEFICIARIO").trim().replaceAll(",", "")+"\",");
						contenidoArchivo.append(rs.getString("PORCBENEFICIARIO").trim().replaceAll(",", "")+"\",");	
						contenidoArchivo.append("\"$"+Comunes.formatoDecimal(rs.getString("IMPORTEBENEFICIARIO"), 2)+"\",");
						}else {
						contenidoArchivo.append(",,,,");
						}	
						contenidoArchivo.append(fecha_reg_ope.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(tipoTasa.trim().replaceAll(",", "")+",");
						contenidoArchivo.append(" \n ");
				 }
			
		} //while (rs.next())							
				if (doc_nacional>0){ 
					if(tipo.equals("PDF"))
					{
						pdfDoc.setCell("Totales Normal","celda01",ComunesPDF.CENTER, 19);
						
						pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
						pdfDoc.setCell(String.valueOf(doc_nacional),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF),false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
					}
				}
				if (nacionalDocto_fid) { 
					if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 19);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_nacional_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(nacional_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
				}
					
					if (doc_dolares>0){
						if(tipo.equals("PDF"))
						{							
							pdfDoc.setCell("Total USD","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
					}
					if (dolaresDocto_fid){ 
						if(tipo.equals("PDF"))
						{
							pdfDoc.setCell("Totales Fideicomiso","celda01",ComunesPDF.CENTER, 19);
							pdfDoc.setCell("Total MN","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 2);
							pdfDoc.setCell(String.valueOf(doc_dolares_fid),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER, 5);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_desc_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fid),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_int_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("$"+Comunes.formatoMoneda2(String.valueOf(dolares_descIF_fiso_s),false),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER,16);
						}
					}	
				
		}//fin8
		
						
			if(tipo.equals("PDF") ) {
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			}
			if(tipo.equals("CSV") ) {
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	public String getEstatus() {
		return estatus;
	}


	public void setTipoOper(String tipoOper) {
		this.tipoOper = tipoOper;
	}


	public String getTipoOper() {
		return tipoOper;
	}

	public String getBanderaFISO() {
		return banderaFISO;
	}

	public void setBanderaFISO(String banderaFISO) {
		this.banderaFISO = banderaFISO;
	}
	 
	 
	
}