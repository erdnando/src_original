package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsDoctosPymeDE implements IQueryGeneratorPS, IQueryGeneratorRegExtJS{
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsDoctosPymeDE.class);

  private int numList = 1;
  
  public ArrayList getConditions(HttpServletRequest request){
	log.info("getConditions(E)");
    ArrayList conditions = new ArrayList();    
		String operacion = (request.getParameter("operacion") == null ) ? "" : request.getParameter("operacion");
		String noEpo = (request.getParameter("no_epo") == null) ? "" : request.getParameter("no_epo");
		String noIf = (request.getParameter("no_if") == null) ? "" : request.getParameter("no_if");
		String noDocto = (request.getParameter("no_docto") == null) ? "" : request.getParameter("no_docto");
		String dfFechaDoctode = (request.getParameter("df_fecha_docto_de") == null) ? "" : request.getParameter("df_fecha_docto_de");
		String dfFechaDoctoa = (request.getParameter("df_fecha_docto_a") == null) ? "" : request.getParameter("df_fecha_docto_a");
		String campDina1 = (request.getParameter("camp_dina1") == null) ? "" : request.getParameter("camp_dina1");
		String campDina2 = (request.getParameter("camp_dina2") == null) ? "" : request.getParameter("camp_dina2");
		String ordIf = (request.getParameter("ord_if") == null) ? "" : request.getParameter("ord_if");
		String ordNoDocto = (request.getParameter("ord_no_docto") == null) ? "" : request.getParameter("ord_no_docto");
		String ordEpo = (request.getParameter("ord_epo") == null) ? "" : request.getParameter("ord_epo");
		String ordFecVenc = (request.getParameter("ord_fec_venc") == null) ? "" : request.getParameter("ord_fec_venc");
		String dfFechaVencde = (request.getParameter("df_fecha_venc_de") == null) ? "" : request.getParameter("df_fecha_venc_de");
		String dfFechaVenca = (request.getParameter("df_fecha_venc_a") == null) ? "" : request.getParameter("df_fecha_venc_a");
		String noMoneda = (request.getParameter("no_moneda") == null) ? "" : request.getParameter("no_moneda");
		String montoDe = (request.getParameter("monto_de") == null) ? "" : request.getParameter("monto_de");
		String montoA = (request.getParameter("monto_a") == null) ? "" : request.getParameter("monto_a");
		String noEstatusDocto = (request.getParameter("no_estatus_docto") == null) ? "" : request.getParameter("no_estatus_docto");
		String fechaIfde = (request.getParameter("fecha_if_de") == null) ? "" : request.getParameter("fecha_if_de");
		String fechaIfa = (request.getParameter("fecha_if_a") == null) ? "" : request.getParameter("fecha_if_a");
		String tasade = (request.getParameter("tasa_de") == null) ? "" : request.getParameter("tasa_de");
		String tasaa = (request.getParameter("tasa_a") == null) ? "" : request.getParameter("tasa_a");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
		String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");			
		String sOperaFactConMandato = (request.getParameter("sOperaFactConMandato") == null) ? "" : request.getParameter("sOperaFactConMandato");
		String mandant = (request.getParameter("mandant") == null) ? "" : request.getParameter("mandant");

		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
		String strAforo = (String)request.getSession().getAttribute("aforoPantalla");

		if (noEstatusDocto.equals("") || noEstatusDocto.equals("1") || noEstatusDocto.equals("2") || noEstatusDocto.equals("5") || noEstatusDocto.equals("6") || noEstatusDocto.equals("7") || noEstatusDocto.equals("9") || noEstatusDocto.equals("10") || noEstatusDocto.equals("21") || noEstatusDocto.equals("23"))
		{
      		conditions.add(new Integer(iNoCliente));
			if (!noEpo.equals("")) {
        		conditions.add(new Integer(noEpo));
      		}
			if (!noIf.equals("")) {
        		conditions.add(new Integer(noIf));
      		}
			if (!noDocto.equals("")) {
        		conditions.add(noDocto);
      		}
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals(""))) {
		        conditions.add(dfFechaDoctode);
		        conditions.add(dfFechaDoctoa);
      		}
			if(!tipoFactoraje.equals(""))
        		conditions.add(tipoFactoraje);
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals(""))) {
        		conditions.add(dfFechaVencde);
        		conditions.add(dfFechaVenca);
      		}
			if (!noMoneda.equals(""))
        		conditions.add(new Integer(noMoneda));
			if ((!montoDe.equals("")) && (!montoA.equals(""))) {
        		conditions.add(new Double(montoDe));
        		conditions.add(new Double(montoA));
      		}
			if (!noEstatusDocto.equals(""))
        		conditions.add(new Integer(noEstatusDocto));
			if (!campDina1.equals(""))
        		conditions.add(campDina1);
			if (!campDina2.equals(""))
        		conditions.add(campDina2);
		}
		//else  if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24")){
		else  if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24") || noEstatusDocto.equals("26")){//FODEA 005 - 2009 ACF
	        conditions.add(new Integer(iNoCliente));
	        conditions.add(new Integer(iNoCliente));
        
			if(!noEpo.equals(""))
        		conditions.add(new Integer(noEpo));
			if(!noIf.equals(""))
        		conditions.add(new Integer(noIf));
			if (!noDocto.equals(""))
        		conditions.add(noDocto);
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals(""))) {
        		conditions.add(dfFechaDoctode);
        		conditions.add(dfFechaDoctoa);
      		}
			if(!tipoFactoraje.equals(""))
        		conditions.add(tipoFactoraje);
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals(""))) {
        		conditions.add(dfFechaVencde);
        		conditions.add(dfFechaVenca);
      		}
			if (!noMoneda.equals(""))
        		conditions.add(new Integer(noMoneda));
			if ((!montoDe.equals("")) && (!montoA.equals(""))) {
        		conditions.add(new Double(montoDe));
        		conditions.add(new Double(montoA));
      		}
			if (!noEstatusDocto.equals(""))
        		conditions.add(new Integer(noEstatusDocto));

			if (noEstatusDocto.equals("4") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") ) { //Solo cuando es Operada y, Operada Pagada, Operada Pendiente de pago y Aplicado a credito se toma encuenta la fecha aut IF.
				if ((!fechaIfde.equals("")) && (!fechaIfa.equals(""))) {
            		conditions.add(fechaIfde);
            		conditions.add(fechaIfa);
				}
			}
			if ((!tasade.equals("") ) && (!tasaa.equals(""))) {
        		conditions.add(new Double(tasade));
        		conditions.add(new Double(tasaa));
      		}
			if (!campDina1.equals(""))
        		conditions.add(campDina1);
			if (!campDina2.equals(""))
        		conditions.add(campDina2);
    }//else if
		//FODEA 005 - 2009 ACF (I)
		/*else if(noEstatusDocto.equals("26")){
			
        	conditions.add(new Integer(iNoCliente));
        	conditions.add(new Integer(iNoCliente));
			if(!noEpo.equals(""))
        		conditions.add(new Integer(noEpo));
			if(!noIf.equals(""))
        		conditions.add(new Integer(noIf));
			if (!noDocto.equals(""))
        		conditions.add(noDocto);
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals(""))) {
        		conditions.add(dfFechaDoctode);
        		conditions.add(dfFechaDoctoa);
      		}
			if(!tipoFactoraje.equals(""))
        		conditions.add(tipoFactoraje);
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals(""))) {
		        conditions.add(dfFechaVencde);
		        conditions.add(dfFechaVenca);
	      	}
			if (!noMoneda.equals(""))
        		conditions.add(new Integer(noMoneda));
			if ((!montoDe.equals("")) && (!montoA.equals(""))) {
		        conditions.add(new Double(montoDe));
		        conditions.add(new Double(montoA));
      		}*/
				//FODEA 005 - 2009 ACF (F)
				/*No ya que es solo programado - 26
			if (!noEstatusDocto.equals(""))
        		conditions.add(new Integer(noEstatusDocto));*/
        		
        	/*
			if (noEstatusDocto.equals("4") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") ) { //Solo cuando es Operada y, Operada Pagada, Operada Pendiente de pago y Aplicado a credito se toma encuenta la fecha aut IF.
				if ((!fechaIfde.equals("")) && (!fechaIfa.equals(""))) {
		            conditions.add(fechaIfde);
		            conditions.add(fechaIfa);
				}
			}
			if ((!tasade.equals("") ) && (!tasaa.equals(""))) {
        		conditions.add(new Double(tasade));
        		conditions.add(new Double(tasaa));
      		}
			if (!campDina1.equals(""))
        		conditions.add(campDina1);
			if (!campDina2.equals(""))
        		conditions.add(campDina2);*/
    	//}//else if //FODEA 005 - 2009 ACF
    log.info("getConditions(S)");
    return conditions;
  }

	public String getAggregateCalculationQuery(HttpServletRequest request) {
		log.info("getAggregateCalculationQuery(E)");
		this.numList = 1;
		StringBuffer qrySentencia = new StringBuffer();
		String operacion = (request.getParameter("operacion") == null ) ? "" : request.getParameter("operacion");
		String noEpo = (request.getParameter("no_epo") == null) ? "" : request.getParameter("no_epo");
		String noIf = (request.getParameter("no_if") == null) ? "" : request.getParameter("no_if");
		String noDocto = (request.getParameter("no_docto") == null) ? "" : request.getParameter("no_docto");
		String dfFechaDoctode = (request.getParameter("df_fecha_docto_de") == null) ? "" : request.getParameter("df_fecha_docto_de");
		String dfFechaDoctoa = (request.getParameter("df_fecha_docto_a") == null) ? "" : request.getParameter("df_fecha_docto_a");
		String campDina1 = (request.getParameter("camp_dina1") == null) ? "" : request.getParameter("camp_dina1");
		String campDina2 = (request.getParameter("camp_dina2") == null) ? "" : request.getParameter("camp_dina2");
		String ordIf = (request.getParameter("ord_if") == null) ? "" : request.getParameter("ord_if");
		String ordNoDocto = (request.getParameter("ord_no_docto") == null) ? "" : request.getParameter("ord_no_docto");
		String ordEpo = (request.getParameter("ord_epo") == null) ? "" : request.getParameter("ord_epo");
		String ordFecVenc = (request.getParameter("ord_fec_venc") == null) ? "" : request.getParameter("ord_fec_venc");
		String dfFechaVencde = (request.getParameter("df_fecha_venc_de") == null) ? "" : request.getParameter("df_fecha_venc_de");
		String dfFechaVenca = (request.getParameter("df_fecha_venc_a") == null) ? "" : request.getParameter("df_fecha_venc_a");
		String noMoneda = (request.getParameter("no_moneda") == null) ? "" : request.getParameter("no_moneda");
		String montoDe = (request.getParameter("monto_de") == null) ? "" : request.getParameter("monto_de");
		String montoA = (request.getParameter("monto_a") == null) ? "" : request.getParameter("monto_a");
		String noEstatusDocto = (request.getParameter("no_estatus_docto") == null) ? "" : request.getParameter("no_estatus_docto");
		String fechaIfde = (request.getParameter("fecha_if_de") == null) ? "" : request.getParameter("fecha_if_de");
		String fechaIfa = (request.getParameter("fecha_if_a") == null) ? "" : request.getParameter("fecha_if_a");
		String tasade = (request.getParameter("tasa_de") == null) ? "" : request.getParameter("tasa_de");
		String tasaa = (request.getParameter("tasa_a") == null) ? "" : request.getParameter("tasa_a");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
		String strAforo = (String)request.getSession().getAttribute("aforoPantalla");
		String OperaFactorajeDeNotaCredito = (request.getParameter("OperaFactorajeNotaDeCredito")==null)?"":request.getParameter("OperaFactorajeNotaDeCredito");
		String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "1" : request.getParameter("sFechaVencPyme");
		String sOperaFactConMandato = (request.getParameter("sOperaFactConMandato") == null) ? "" : request.getParameter("sOperaFactConMandato");
		String mandant = (request.getParameter("mandant") == null) ? "" : request.getParameter("mandant");

		boolean muestraNegociables = false;		

		/* Para manejo de idioma ingl�s, en el jsp se define un campo "idiomaUsuario"
		 * con los valores "ES" para espa�ol o "EN" para ingl�s.
		 */
		String idiomaUsuario =
				(request.getSession().getAttribute("sesIdiomaUsuario") == null ) ? "ES" :
				(String) request.getSession().getAttribute("sesIdiomaUsuario");
		String campoNombreMoneda = null;
		if (idiomaUsuario.equals("EN")) {
			campoNombreMoneda = " M.cd_nombre_ing ";
		} else {
			campoNombreMoneda = " M.cd_nombre ";
		}

		
		if (noEstatusDocto.equals("") || noEstatusDocto.equals("1") || noEstatusDocto.equals("2") || noEstatusDocto.equals("5") || noEstatusDocto.equals("6") || noEstatusDocto.equals("7") || noEstatusDocto.equals("9") || noEstatusDocto.equals("10") || noEstatusDocto.equals("21") || noEstatusDocto.equals("23"))
		{			
			muestraNegociables = (noEstatusDocto.equals("2"))?true:false;
			qrySentencia.append(
  			"select count(*) AS ConsDoctosPymeDE"+
				",sum(D.fn_monto) AS FN_MONTO "+
				",sum(decode(D.cs_dscto_especial,'V',D.fn_monto,'D',D.fn_monto,'C',D.fn_monto,'I',D.fn_monto,decode(D.ic_estatus_docto,1,D.fn_monto_dscto,9,D.fn_monto_dscto,10,D.fn_monto_dscto,21,D.fn_monto_dscto,(D.fn_monto*"+strAforo+")))) AS FN_MONTO_DSCTO "+
        		"," + campoNombreMoneda +
		        ",D.ic_moneda"+
		        " from com_documento D"+
		        " ,comcat_moneda M"+
						"	where D.ic_moneda = M.ic_moneda "+
		        " and D.ic_pyme = ?"
      		);

			if (!noEpo.equals(""))
				qrySentencia.append(" and D.ic_epo = ?") ;
			if (!noIf.equals(""))
				qrySentencia.append(" and D.ic_if = ?");
			if (!noDocto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ? ");
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals("")))
				qrySentencia.append(" and D.DF_FECHA_DOCTO between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if(!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=? ");
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals("")))
				qrySentencia.append(" and D.DF_FECHA_VENC"+sFechaVencPyme+" between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if (!noMoneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ? ");
			if ((!montoDe.equals("")) && (!montoA.equals("")))
				qrySentencia.append(" and D.fn_monto between ? and ? ");
			if (!noEstatusDocto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ? ");
			else
				qrySentencia.append(" and D.ic_estatus_docto in (1, 2, 5, 6, 7, 9, 10) ");
			if (!campDina1.equals(""))
				qrySentencia.append(" and D.CG_CAMPO1 = ?");
			if (!campDina2.equals(""))
				qrySentencia.append(" and D.CG_CAMPO2 = ?");
			
		}
		//else  if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24")){
		else  if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24") || noEstatusDocto.equals("26")){//FODEA 005 - 2009 ACF

			qrySentencia.append(
  			"select count(*) AS ConsDoctosPymeDE "+
				",sum(D.fn_monto) AS FN_MONTO "+
        ",sum((D.fn_monto * (decode(D.cs_dscto_especial,'V',100,'D',100,'C',100,'I',100,D.fn_porc_anticipo)/ 100))) as FN_MONTO_DSCTO"+
        "," + campoNombreMoneda +
        ",D.ic_moneda"+
        " from com_documento D"+
        " ,com_docto_seleccionado DS"+
        " ,com_solicitud S"+
        " ,comcat_moneda M"+
				"	where D.IC_DOCUMENTO = DS.IC_DOCUMENTO"+
        " AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO(+)"+
        " AND D.IC_MONEDA = M.IC_MONEDA"+
        " AND D.ic_pyme = ?" + 
        " AND D.ic_pyme = ?" );

			if(!noEpo.equals(""))
				qrySentencia.append(" and D.ic_epo = ? ");
			if(!noIf.equals(""))
				qrySentencia.append(" and D.ic_if = ? ");
			if (!noDocto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ? ");
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals("")))
				qrySentencia.append(" and D.DF_FECHA_DOCTO between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if(!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=? ");
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals("")))
				qrySentencia.append(" and D.DF_FECHA_VENC"+sFechaVencPyme+" between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if (!noMoneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ? ");
			if ((!montoDe.equals("")) && (!montoA.equals("")))
				qrySentencia.append(" and D.fn_monto between ? and ? ");
			if (!noEstatusDocto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ? ");
			if (noEstatusDocto.equals("4") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") ) { //Solo cuando es Operada y, Operada Pagada, Operada Pendiente de pago y Aplicado a credito se toma encuenta la fecha aut IF.
				if ((!fechaIfde.equals("")) && (!fechaIfa.equals(""))) {
					qrySentencia.append(" and TO_DATE(TO_CHAR(S.df_fecha_solicitud, 'DD/MM/YYYY'), 'DD/MM/YYYY') between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				}
			}
			if ((!tasade.equals("") ) && (!tasaa.equals("")))
				qrySentencia.append(" and DS.in_tasa_aceptada between ? and ? ");
			if (!campDina1.equals(""))
				qrySentencia.append(" and D.CG_CAMPO1 = ?");
			if (!campDina2.equals(""))
				qrySentencia.append(" and D.CG_CAMPO2 = ?");

    }//else if
	 //FODEA 005 - 2009 ACF (I)
	/*else if(noEstatusDocto.equals("26")){
		qrySentencia.append(
				  		"select count(*) AS ConsDoctosPymeDE "+
						",sum(D.fn_monto) AS FN_MONTO "+
				        ",sum((D.fn_monto * (decode(D.cs_dscto_especial,'V',100,'D',100,'C',100,'I',100,D.fn_porc_anticipo)/ 100))) as FN_MONTO_DSCTO"+
				        "," + campoNombreMoneda +
				        ",D.ic_moneda"+
				        " from com_documento D"+
				        " ,com_docto_seleccionado DS"+
				        " ,com_solicitud S"+
				        " ,comcat_moneda M"+
						"	where D.IC_DOCUMENTO = DS.IC_DOCUMENTO"+
						" AND D.ic_estatus_docto = 26 "+ //26-Promamada
				        " AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO(+)"+
				        " AND D.IC_MONEDA = M.IC_MONEDA"+
				        " AND D.ic_pyme = ?" + 
				        " AND D.ic_pyme = ?" );
		if(!noEpo.equals(""))
			qrySentencia.append(" and D.ic_epo = ? ");
		if(!noIf.equals(""))
			qrySentencia.append(" and D.ic_if = ? ");
		if (!noDocto.equals(""))
			qrySentencia.append(" and D.ig_numero_docto = ? ");
		if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals("")))
			qrySentencia.append(" and D.DF_FECHA_DOCTO between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
		if(!tipoFactoraje.equals(""))
			qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=? ");
		if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals("")))
			qrySentencia.append(" and D.DF_FECHA_VENC"+sFechaVencPyme+" between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
		if (!noMoneda.equals(""))
			qrySentencia.append(" and D.ic_moneda = ? ");
		if ((!montoDe.equals("")) && (!montoA.equals("")))
			qrySentencia.append(" and D.fn_monto between ? and ? ");*/
		//FODEA 005 - 2009 ACF (F)
		/*
		if (noEstatusDocto.equals("4") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") ) { //Solo cuando es Operada y, Operada Pagada, Operada Pendiente de pago y Aplicado a credito se toma encuenta la fecha aut IF.
			if ((!fechaIfde.equals("")) && (!fechaIfa.equals(""))) {
				qrySentencia.append(" and TO_DATE(TO_CHAR(S.df_fecha_solicitud, 'DD/MM/YYYY'), 'DD/MM/YYYY') between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			}
		}
		if ((!tasade.equals("") ) && (!tasaa.equals("")))
			qrySentencia.append(" and DS.in_tasa_aceptada between ? and ? ");
		if (!campDina1.equals(""))
			qrySentencia.append(" and D.CG_CAMPO1 = ?");
		if (!campDina2.equals(""))
			qrySentencia.append(" and D.CG_CAMPO2 = ?");*/

    //}//else if        //FODEA 005 - 2009 ACF
    
    System.out.println("Consulta Pyme ok**************************OperaFactorajeDeNotaCredito:"+OperaFactorajeDeNotaCredito);
    
    String qrySentenciaConsulta = "";       			
	
	String qryNegocNoNotasCredito = qrySentencia.toString();
	
	String qryNegocSiNotasCredito = qrySentencia.toString();
	
	qryNegocNoNotasCredito += " AND cs_dscto_especial != 'C' "+
							  " AND d.ic_estatus_docto = 2 " +  " GROUP BY D.IC_MONEDA,CD_NOMBRE ";
							  
	qryNegocSiNotasCredito += " AND cs_dscto_especial = 'C' "+
							  " AND d.ic_estatus_docto = 2 " + " GROUP BY D.IC_MONEDA,CD_NOMBRE ";
	
	qrySentencia.append(" AND cs_dscto_especial != 'C' GROUP BY D.IC_MONEDA,CD_NOMBRE  "); //todos excepto notas de Credito
	
	qrySentenciaConsulta = remplazar(qrySentencia.toString(),",D.ic_moneda",",'D ' || to_char(d.ic_moneda) as ic_moneda");
	
	if(OperaFactorajeDeNotaCredito.equals("S")) {
		this.numList++;
		qrySentenciaConsulta += " UNION ALL " + remplazar(qryNegocSiNotasCredito,",D.ic_moneda",",'NC ' || to_char(d.ic_moneda) as ic_moneda") ;
		if(!muestraNegociables) {
			this.numList++;
			qrySentenciaConsulta += " UNION ALL " + remplazar(qryNegocNoNotasCredito,",D.ic_moneda",",'DN ' || to_char(d.ic_moneda) as ic_moneda");
		}	
	}
    
    /*
    qrySentencia.append (" GROUP BY D.IC_MONEDA,CD_NOMBRE "+
                    " ORDER BY D.IC_MONEDA");*/

    qrySentenciaConsulta = " Select * from ( "+qrySentenciaConsulta+" ) "+
                    			  " ORDER BY IC_MONEDA ";
   
	System.out.println("QUERY DE TOTALES OK: "+qrySentenciaConsulta);
		//return qrySentencia.toString();
		log.info("getAggregateCalculationQuery(S)");
		return qrySentenciaConsulta;
	}
	/**
	 * James Clark
	 *
	 * In order to reduce the size of the com_documento result set, we need
	 * a query which returns only results that will actually be displayed to the
	 * user.
	 *
	 * @param ic_estatus_docto query depends on the estatus
	 * @param ids these are the ids of the Documents we need to display
	 *
	 */
	public int getNumList(HttpServletRequest request){
		/*
	   String noEstatusDocto = (request.getParameter("no_estatus_docto") == null) ? "" : request.getParameter("no_estatus_docto");	
		int numList = 1;
		if (noEstatusDocto.equals("") || noEstatusDocto.equals("1") || noEstatusDocto.equals("2") || noEstatusDocto.equals("5") || noEstatusDocto.equals("6") || noEstatusDocto.equals("7") || noEstatusDocto.equals("9") || noEstatusDocto.equals("10") || noEstatusDocto.equals("21") || noEstatusDocto.equals("23")){
			numList = 1;
		}else if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24")){
			numList = 2;
		}*/
		return this.numList;
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		
		log.info("getDocumentSummaryQueryForIds(E)");
		String noEpo 			= (request.getParameter("no_epo") == null) ? "" : request.getParameter("no_epo");
		// IMPORTANTE: EL CAMPO NO_EPO ES OBLIGATORIO
		String mandant	 		= (request.getParameter("mandant") == null) ? "" : request.getParameter("mandant");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
 
		String noEstatusDocto = (request.getParameter("no_estatus_docto") == null) ? "" : request.getParameter("no_estatus_docto");	  
		String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");

		/* Para manejo de idioma ingl�s, en el jsp se define un campo "idiomaUsuario"
		 * con los valores "ES" para espa�ol o "EN" para ingl�s.
		 */
		 
		String idiomaUsuario =
			(request.getSession().getAttribute("sesIdiomaUsuario") == null ) ? "ES" :
			(String) request.getSession().getAttribute("sesIdiomaUsuario");

		String campoNombreMoneda = null;
		String campoNombreEstatusDocto = null;
		
		if (idiomaUsuario.equals("EN")) {
			campoNombreMoneda = " M.cd_nombre_ing ";
			campoNombreEstatusDocto = " ED.cd_descripcion_ing ";
		} else {
			campoNombreMoneda = " M.cd_nombre ";
			campoNombreEstatusDocto = " ED.cd_descripcion ";
		}
		
		

		if (noEstatusDocto.equals("") || noEstatusDocto.equals("1") || noEstatusDocto.equals("2") || noEstatusDocto.equals("5") || noEstatusDocto.equals("6") || noEstatusDocto.equals("7") || noEstatusDocto.equals("9") || noEstatusDocto.equals("10") || noEstatusDocto.equals("21") || noEstatusDocto.equals("23")){
			this.numList = 1;
		//}else if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24")){
		}else if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24") || noEstatusDocto.equals("26")){//FODEA 005 - 2009 ACF
			this.numList = 2;
		}/*else if (noEstatusDocto.equals("26")){
			this.numList = 2;
		}*/
		
		StringBuffer query = new StringBuffer();
		StringBuffer mandantecond = new StringBuffer();
		StringBuffer mandantetabs = new StringBuffer();
		StringBuffer mandantecamp = new StringBuffer();
		StringBuffer clavesDocumentos = new StringBuffer();
		
		for(int x = 0;x<ids.size();x++ ) {
			clavesDocumentos.append("?,");
		}
		
		clavesDocumentos = clavesDocumentos.delete(clavesDocumentos.length()-1, clavesDocumentos.length());
		//System.out.println(" ����� mandant -:Consulta:- ���� "+mandant+ " �����");
		
		
		if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
				mandantecamp.append( " cm.cg_razon_social AS mandante, " );
				mandantetabs.append(	"	,comcat_mandante cm " );
				if("M".equals(tipoFactoraje)){
				mandantecond.append(" AND d.ic_mandante = cm.ic_mandante" );		 
				}else{
				mandantecond.append(" AND d.ic_mandante = cm.ic_mandante(+)" );		 
				}
		}
		if(!"".equals(mandant)){
			mandantecond.append(	"  AND cm.IC_MANDANTE = "+mandant+" " );
		}
		
		if (noEstatusDocto.equals("")   ||  
		    noEstatusDocto.equals("1")  || // No Negociable
		    noEstatusDocto.equals("2")  || // Negociable
			 noEstatusDocto.equals("5")  || // Baja
			 noEstatusDocto.equals("6")  || // Descuento Fisico
			 noEstatusDocto.equals("7")  || // Pagado Anticipado
			 noEstatusDocto.equals("9")  || // Vencido sin Operar
			 noEstatusDocto.equals("10") || // Pagado sin Operar
			 noEstatusDocto.equals("21") || // Bloqueado
			 noEstatusDocto.equals("23")) { // Pignorado
			query.append(
				" SELECT /*+ use_nl(d,ed,m,i,i2,e,det,p,tf,cm)*/ E.cg_razon_social AS nombre_epo, "+
				"	D.ig_numero_docto, "+
				"	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') AS df_fecha_docto, " +
				"	TO_CHAR(D.DF_FECHA_VENC"+sFechaVencPyme+",'DD/MM/YYYY') AS df_fecha_venc, "+
				campoNombreMoneda + ", "+
				"	D.fn_monto, "+
				"	D.fn_porc_anticipo, "+
				"	ED.ic_estatus_docto, "+
				"	D.fn_monto_dscto, " +
				campoNombreEstatusDocto + ", " +
				"	D.ic_moneda, "+
				"	D.ic_documento, "+
				"	D.cs_cambio_importe, "+
				//"	(decode (I.cs_habilitado, 'N', '*', 'S', ' ') ||' '||I.cg_razon_social) as nombre_if, "+	//Linea a modificar FODEA 17
				"	decode(ds.cs_opera_fiso, 'S', I3.cg_razon_social, 'N', (decode (I.cs_habilitado, 'N', '*', 'S', ' ')||' '||I.cg_razon_social)) as nombre_if,"+		//FODEA 17
				"	D.cs_detalle, " +
				"  	D.cg_campo1, D.cg_campo2, D.cg_campo3, D.cg_campo4, D.cg_campo5, " +
				"	D.CS_DSCTO_ESPECIAL, " +
				"	I2.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO, " +
				"	D.FN_PORC_BENEFICIARIO, " +
				"	(D.FN_MONTO * D.FN_PORC_BENEFICIARIO ) / 100 RECIBIR_BENEFICIARIO, " +
				"	D.CT_REFERENCIA, " +
				
				" TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO, " +
				" tf.cg_nombre as TIPO_FACTORAJE, "+mandantecamp+ 	
				" 'N' nota_simple, 'N' nota_simple_docto, 'N' nota_multiple, 'N' nota_multiple_docto, '' numero_docto,"+ //Campos a llenar durante el llamado de la consulta 13consulta02ext.data.jsp
				"	'ConsDoctosPymeDE::getDocumentSummaryQueryForIds'"+
				" FROM  " +
				"	com_documento D, "+
				"	com_docto_seleccionado ds, "+		//FODEA 17
				"	comcat_estatus_docto ED, "+
				"	comcat_moneda M, " +
				"	comcat_if I, " +
				"	comcat_if I2, " +
				"	comcat_if I3, " +
				" 	comcat_epo E, "+
				"	comcat_pyme P, "+
				"  comcat_tipo_factoraje tf " +mandantetabs+
				" WHERE D.ic_epo = E.ic_epo " +
				"	and D.ic_pyme = P.ic_pyme " +
				"	and D.ic_documento = DS.ic_documento(+) "+ //FODEA 17
				"	and D.ic_if = I.ic_if(+) " +
				"	and ds.ic_if = I3.ic_if(+) "+		//FODEA 17
				"	and D.ic_moneda = M.ic_moneda " +
				"	and D.ic_estatus_docto = ED.ic_estatus_docto "+
				"	AND D.ic_beneficiario = I2.ic_if (+) "+
				" and D.ic_documento in (" + clavesDocumentos + ")" +
				" AND D.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+mandantecond );
		} 
		
		else if (noEstatusDocto.equals("3")  || // Seleccionada Pyme
		         noEstatusDocto.equals("4")  || // Operada
					noEstatusDocto.equals("8")  || // Seleccionado IF(obsoleto)
					noEstatusDocto.equals("11") || // Operada Pagada
					noEstatusDocto.equals("12") || // Operada Pendiente de Pago
					noEstatusDocto.equals("16") || // Aplicado a Credito
					noEstatusDocto.equals("24") || // En proceso de Autorizacion IF
					noEstatusDocto.equals("26")) { // Programado Pyme - FODEA 005 - 2009 ACF
 
			// Obtener fecha del siguiente dia habil por EPO
			AccesoDB 			con 							= new AccesoDB();
			PreparedStatement ps 							= null;
			ResultSet 			rs 							= null;
			String 				fechaHabilSiguienteXEpo = null; //new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			try {
				
				con.conexionDB();
				
				String qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
				ps = con.queryPrecompilado(qrySentencia);
				ps.executeUpdate();
				ps.close();
				
				qrySentencia = 
					" SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha"   +
					"   FROM dual"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1, Integer.parseInt(noEpo));
				rs = ps.executeQuery();
				ps.clearParameters();
				if(rs.next()) {
					fechaHabilSiguienteXEpo = rs.getString(1); //==null?fechaHabilSiguienteXEpo:rs.getString(1);
				}
 	
			} catch (Exception e) {
				System.out.println("Exception: "+e);
			} finally {
				
				if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
				if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
				
				con.terminaTransaccion(true);
				if (con.hayConexionAbierta() == true)
					con.cierraConexionDB();
				
			}
			
			// Este error NUNCA DEBER�A DE OCURRIR se deja para avisar al usuario que se presento un problema con la
			// funcion sigfechahabilxepo y de esta manera evitar que se le muestre un plazo erroneo
			if( fechaHabilSiguienteXEpo == null ){
				throw new AppException("No se pudo determinar la fecha del d�a h�bil siguiente por EPO");
			}
		
			// Obtener condiciones de consulta del plazo
			String plazo 				= null;
			String condicionPlazo 	= null;
			String tablaPlazo			= null;
			if(
				noEstatusDocto.equals("3") 	// Seleccionada Pyme
					||
				noEstatusDocto.equals("24")	// En proceso de Autorizacion IF
			){
				plazo 			= 
					" ( " +
						" D.df_fecha_venc"+sFechaVencPyme + " - " +
						" 	(CASE  " +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
						"  	TRUNC(SYSDATE) " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
						"  	TO_DATE( '" + fechaHabilSiguienteXEpo + "', 'DD/MM/YYYY' ) " +
						" 	END) " + 
					" ) " + 	
					" AS ig_plazo, ";
					
				condicionPlazo =
						" 	AND D.ic_if   = iexp.ic_if     " +
						" 	AND D.ic_epo  = iexp.ic_epo    " +
						" 	AND iexp.ic_producto_nafin = 1 ";
						
				tablaPlazo		= 
						" , comrel_if_epo_x_producto iexp ";
						
			} else if( 
				noEstatusDocto.equals("26") // Programado Pyme
			){ 
			
				// Nota: como funciona el siguiente dia habil cuando se consulte el documento en su d�a programado 
				// Con el propsito de agilizar la consulta, obtener la fecha del segundo d�a h�bil sguiente por EPO
				con = new AccesoDB();
				String segundaFechaHabilSiguienteXEpo = null;
				ps = null;
				rs = null;
				try {
					
					con.conexionDB();
					
					String qrySentencia 	= "ALTER SESSION SET NLS_TERRITORY = MEXICO";
					ps 						= con.queryPrecompilado(qrySentencia);
					ps.executeUpdate();
					ps.close();
					   
					qrySentencia = 
						" SELECT TO_CHAR( sigfechahabilxepo (?, TO_DATE(?,'DD/MM/YYYY'), 1, '+'), 'dd/mm/yyyy' ) fecha "   +
						"   FROM dual"  ;
					ps = con.queryPrecompilado(qrySentencia);
					ps.setInt(1, 		Integer.parseInt(noEpo) );
					ps.setString(2, 	fechaHabilSiguienteXEpo );
					rs = ps.executeQuery();
					ps.clearParameters();
					if(rs.next()) {
						segundaFechaHabilSiguienteXEpo = rs.getString(1);
					} 
					
					// Este error NUNCA DEBER�A DE OCURRIR se deja para avisar al usuario que se presento un problema con la
					// funcion sigfechahabilxepo y de esta manera evitar que se le muestre un plazo erroneo
					if( segundaFechaHabilSiguienteXEpo == null ){
						throw new AppException("No se pudo determinar la fecha del segundo d�a h�bil siguiente por EPO");
					}
										
				} catch (Exception e) {
					System.out.println("Exception: "+e);
				} finally {
					
					if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
					if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
					
					con.terminaTransaccion(true);
					if (con.hayConexionAbierta() == true)
						con.cierraConexionDB();
					
				}
		
				// Agregar vaiables asociadas al plazo
				plazo 			=
					" ( "  +
						" D.df_fecha_venc"+sFechaVencPyme + " - "  +
						" 	(CASE  "  +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN "  +
						"  	TO_DATE( '" + fechaHabilSiguienteXEpo + "',        'DD/MM/YYYY' ) " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN "  +
						"  	TO_DATE( '" + segundaFechaHabilSiguienteXEpo + "', 'DD/MM/YYYY' ) " + 
						" 	END) "  + 
					" ) "  + 	
					" AS ig_plazo, ";
					
				condicionPlazo =
						" 	AND D.ic_if   = iexp.ic_if     " +
						" 	AND D.ic_epo  = iexp.ic_epo    " +
						" 	AND iexp.ic_producto_nafin = 1 ";
						
				tablaPlazo		= 
						" , comrel_if_epo_x_producto iexp ";
 	
			} else {
				
				// "   DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc"+sFechaVencPyme+")-DECODE(D.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaHabilSiguienteXEpo+"', 'dd/mm/yyyy')), S.ig_plazo ) ig_plazo, "
				plazo 			= " NVL(S.ig_plazo, TRUNC(D.df_fecha_venc"+sFechaVencPyme+")-DECODE(D.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaHabilSiguienteXEpo+"', 'dd/mm/yyyy')) ) ig_plazo, ";
				condicionPlazo = null;
				tablaPlazo		= null;
				
			}     
			
			// Construir Query
			query.append(
				"SELECT /*+use_nl(d s ds ed e p i i2 m cc1 cd tf cm)*/ " +
				" E.cg_razon_social AS nombre_epo, "+
				//"	(decode (I.cs_habilitado, 'N', '*', 'S', ' ') ||' '||I.cg_razon_social) as nombre_if, "+	//Linea a modificar FODEA 17
				"	decode(DS.cs_opera_fiso, 'S', I3.cg_razon_social, 'N', (decode (I.cs_habilitado, 'N', '*', 'S', ' ')||' '||I.cg_razon_social)) as nombre_if,"+	//FODEA 17
				"	D.ig_numero_docto, "+
				"	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') AS df_fecha_docto, "+
				"	TO_CHAR(D.DF_FECHA_VENC"+sFechaVencPyme+",'DD/MM/YYYY') AS df_fecha_venc, " +
				campoNombreMoneda + ", "+
				"	D.fn_monto, "+
				"	D.fn_porc_anticipo, "+
				"	D.fn_monto_dscto, "+
				"	DS.in_tasa_aceptada, "+
				campoNombreEstatusDocto + ", " +
				"	TO_CHAR(S.df_fecha_solicitud,'DD/MM/YYYY') AS df_fecha_solicitud, "+
				"	D.ic_moneda, "+
				"	D.ic_documento, " +
				"	DS.in_importe_recibir, "+
				"	D.cs_cambio_importe, " +
				"	D.cs_detalle, "+
				"	D.cg_campo1, D.cg_campo2, D.cg_campo3, D.cg_campo4, D.cg_campo5, " +
				"	D.CS_DSCTO_ESPECIAL, "+
				"	D.ic_estatus_docto, "+
				" 	CASE WHEN cc1.fn_monto_pago IS NOT NULL THEN"   +
				" 		cc1.fn_monto_pago"   +
				" 	ELSE "   +
				" 		CASE WHEN cd.fn_monto_pago IS NOT NULL THEN"   +
				" 			cd.fn_monto_pago"   +
				" 		END"   +
				"	END as fn_monto_pago," +
				"	DS.cc_acuse,"+
				"	DS.IN_IMPORTE_RECIBIR - DS.fn_importe_recibir_benef NETO_RECIBIR, " +
				"	I2.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO, " +
				"	D.FN_PORC_BENEFICIARIO, " +
				"	DS.fn_importe_recibir_benef  RECIBIR_BENEFICIARIO, " +
				" 	CASE WHEN cc1.fn_porc_docto_aplicado IS NOT NULL THEN " +
				" 		cc1.fn_porc_docto_aplicado " +
				" 	ELSE " +
				" 		CASE WHEN cd.fn_porc_docto_aplicado IS NOT NULL THEN " +
				" 			cd.fn_porc_docto_aplicado " +
				" 		END " +
				" 	END as porcDoctoAplicado, " +
				plazo +
				"	DS.in_importe_interes, "+
				"   I.ig_tipo_piso, DS.fn_remanente, "+ //foda 015-2005
				"	D.CT_REFERENCIA, " +
				
				" TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO, " +
				" tf.cg_nombre as TIPO_FACTORAJE, "+mandantecamp+
				" TO_CHAR(ds.df_programacion, 'dd/mm/yyyy') AS fecha_registro_operacion, "+//FODEA 005 - 2009 ACF
				" 'N' nota_simple, 'N' nota_simple_docto, 'N' nota_multiple, 'N' nota_multiple_docto, '' numero_docto,"+	//Campos a llenar durante el llamado de la consulta 13consulta02ext.data.jsp
				"	'ConsDoctosPymeDE::getDocumentSummaryQueryForIds'"+
				" FROM comcat_epo E, "+
				"	comcat_pyme P, "+
				"	comcat_if I, "+
				"	comcat_if I2, " +
				"	comcat_if I3, " +
				"	com_documento D, " +
				"	comcat_moneda M, "+
				"	com_docto_seleccionado DS, "+
				"	comcat_estatus_docto ED, "+
				"	com_solicitud S, " +
				"	(select cc.ic_documento, cc.fn_porc_docto_aplicado , " +
				"		sum(cc.fn_monto_pago) as fn_monto_pago " +
				"	from com_cobro_credito cc " +
				"	where cc.ic_documento in (" + clavesDocumentos + ") " +
				"	group by cc.ic_documento, cc.fn_porc_docto_aplicado) cc1, " +
				" 	fop_cobro_disposicion cd, " +
				"  comcat_tipo_factoraje tf " +mandantetabs+
				( tablaPlazo != null?tablaPlazo:"" )  +
 				" WHERE D.ic_epo = E.ic_epo " +
				"	AND D.ic_pyme = P.ic_pyme " +
				"	AND D.ic_moneda = M.ic_moneda " +
				"	AND D.ic_documento = DS.ic_documento " +
				"	AND D.ic_estatus_docto = ED.ic_estatus_docto " +
				"	AND D.ic_documento = S.ic_documento(+) " +
				"	AND D.ic_if = I.ic_if(+)"+
				"	AND DS.ic_if = I3.ic_if(+)"+		//FODEA 17
				"	AND D.ic_documento = CC1.ic_documento(+) " +
				"	AND D.ic_documento = CD.ic_documento(+) " +
				"	AND D.ic_beneficiario = I2.ic_if (+) "+
				" 	AND D.ic_documento in (" + clavesDocumentos + ")" +
				"  AND D.cs_dscto_especial = tf.CC_TIPO_FACTORAJE "+ mandantecond +
				( condicionPlazo != null?condicionPlazo:"" )
				);
		}
		//FODEA 005 - 2009 ACF (I)
		/*else if (noEstatusDocto.equals("26")) { //26-Programado Foda 025-2005
			query.append(
				"SELECT /*+use_nl(d s ds ed e p i i2 m cc1 cd tf cm)*\/ "+
				" E.cg_razon_social, "+
				"	(decode (I.cs_habilitado, 'N', '*', 'S', ' ') ||' '||I.cg_razon_social) as cg_razon_social, "+
				"	D.ig_numero_docto, "+
				"	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY'), "+
				"	TO_CHAR(D.DF_FECHA_VENC"+sFechaVencPyme+",'DD/MM/YYYY'), " +
				campoNombreMoneda + ", "+
				"	D.fn_monto, "+
				"	D.fn_porc_anticipo, "+
				"	D.fn_monto_dscto, "+
				"	DS.in_tasa_aceptada, "+
				campoNombreEstatusDocto + ", " +
				"	TO_CHAR(S.df_fecha_solicitud,'DD/MM/YYYY'), "+
				"	D.ic_moneda, "+
				"	D.ic_documento, " +
				"	DS.in_importe_recibir, "+
				"	D.cs_cambio_importe, " +
				"	D.cs_detalle, "+
				"	D.cg_campo1, D.cg_campo2, D.cg_campo3, D.cg_campo4, D.cg_campo5, " +
				"	D.CS_DSCTO_ESPECIAL, "+
				"	D.ic_estatus_docto, "+
				" 	CASE WHEN cc1.fn_monto_pago IS NOT NULL THEN"   +
				" 		cc1.fn_monto_pago"   +
				" 	ELSE "   +
				" 		CASE WHEN cd.fn_monto_pago IS NOT NULL THEN"   +
				" 			cd.fn_monto_pago"   +
				" 		END"   +
				"	END as fn_monto_pago," +
				"	DS.cc_acuse,"+
				"	DS.IN_IMPORTE_RECIBIR - DS.fn_importe_recibir_benef NETO_RECIBIR, " +
				"	I2.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO, " +
				"	D.FN_PORC_BENEFICIARIO, " +
				"	DS.fn_importe_recibir_benef  RECIBIR_BENEFICIARIO, " +
				" 	CASE WHEN cc1.fn_porc_docto_aplicado IS NOT NULL THEN " +
				" 		cc1.fn_porc_docto_aplicado " +
				" 	ELSE " +
				" 		CASE WHEN cd.fn_porc_docto_aplicado IS NOT NULL THEN " +
				" 			cd.fn_porc_docto_aplicado " +
				" 		END " +
				" 	END as porcDoctoAplicado, " +
				"	S.ig_plazo, DS.in_importe_interes, "+
				"   I.ig_tipo_piso, DS.fn_remanente, TO_CHAR(ds.df_programacion,'DD/MM/YYYY') as df_programacion, "+
				"	D.CT_REFERENCIA, " +
				
				" TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO, " +
				" tf.cg_nombre as TIPO_FACTORAJE, "+mandantecamp+
				"	'ConsDoctosPymeDE::getDocumentSummaryQueryForIds'"+
				" FROM comcat_epo E, "+
				"	comcat_pyme P, "+
				"	comcat_if I, "+
				"	comcat_if I2, " +
				"	com_documento D, " +
				"	comcat_moneda M, "+
				"	com_docto_seleccionado DS, "+
				"	comcat_estatus_docto ED, "+
				"	com_solicitud S, " +
				"	(select cc.ic_documento, cc.fn_porc_docto_aplicado , " +
				"		sum(cc.fn_monto_pago) as fn_monto_pago " +
				"	from com_cobro_credito cc " +
				"	where cc.ic_documento in (" + clavesDocumentos + ") " +
				"	group by cc.ic_documento, cc.fn_porc_docto_aplicado) cc1, " +
				" 	fop_cobro_disposicion cd, " +
				"  comcat_tipo_factoraje tf " +mandantetabs+
				" WHERE D.ic_epo = E.ic_epo " +
				"	AND D.ic_pyme = P.ic_pyme " +
				"	AND D.ic_moneda = M.ic_moneda " +
				"	AND D.ic_documento = DS.ic_documento " +
				"	AND D.ic_estatus_docto = ED.ic_estatus_docto " +
				"	AND D.ic_documento = S.ic_documento(+) " +
				"	AND D.ic_if = I.ic_if(+)"+
				"	AND D.ic_documento = CC1.ic_documento(+) " +
				"	AND D.ic_documento = CD.ic_documento(+) " +
				"	AND D.ic_beneficiario = I2.ic_if (+) "+
				" 	AND D.ic_documento in (" + clavesDocumentos + ")" +
				"  AND D.cs_dscto_especial = tf.CC_TIPO_FACTORAJE "+ mandantecond );
		}*/
		//FODEA 005 - 2009 ACF (F)
		else {
			System.out.println("ic_estatus_docto value not handled:  " + noEstatusDocto);
		}
		System.out.println("PageQuery:  "+query.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
		return query.toString();
	}


	/**
	 * James Clark
	 *
	 * Use parameters in this request to constuct a dynamic query
	 * for certain documents.  The result set will contain only
	 * Primary Key data that will be used later to page through
	 * data that summarizes the Documents.
	 */
	public String getDocumentQuery(HttpServletRequest request){
		log.info("getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();

    String operacion = (request.getParameter("operacion") == null ) ? "" : request.getParameter("operacion");
    String noEpo = (request.getParameter("no_epo") == null) ? "" : request.getParameter("no_epo");
    String noIf = (request.getParameter("no_if") == null) ? "" : request.getParameter("no_if");
    String noDocto = (request.getParameter("no_docto") == null) ? "" : request.getParameter("no_docto");
    String dfFechaDoctode = (request.getParameter("df_fecha_docto_de") == null) ? "" : request.getParameter("df_fecha_docto_de");
    String dfFechaDoctoa = (request.getParameter("df_fecha_docto_a") == null) ? "" : request.getParameter("df_fecha_docto_a");
    String campDina1 = (request.getParameter("camp_dina1") == null) ? "" : request.getParameter("camp_dina1");
    String campDina2 = (request.getParameter("camp_dina2") == null) ? "" : request.getParameter("camp_dina2");
    String ordIf = (request.getParameter("ord_if") == null) ? "" : request.getParameter("ord_if");
    String ordNoDocto = (request.getParameter("ord_no_docto") == null) ? "" : request.getParameter("ord_no_docto");
    String ordEpo = (request.getParameter("ord_epo") == null) ? "" : request.getParameter("ord_epo");
    String ordFecVenc = (request.getParameter("ord_fec_venc") == null) ? "" : request.getParameter("ord_fec_venc");
    String dfFechaVencde = (request.getParameter("df_fecha_venc_de") == null) ? "" : request.getParameter("df_fecha_venc_de");
    String dfFechaVenca = (request.getParameter("df_fecha_venc_a") == null) ? "" : request.getParameter("df_fecha_venc_a");
    String noMoneda = (request.getParameter("no_moneda") == null) ? "" : request.getParameter("no_moneda");
    String montoDe = (request.getParameter("monto_de") == null) ? "" : request.getParameter("monto_de");
    String montoA = (request.getParameter("monto_a") == null) ? "" : request.getParameter("monto_a");
    String noEstatusDocto = (request.getParameter("no_estatus_docto") == null) ? "" : request.getParameter("no_estatus_docto");
    String fechaIfde = (request.getParameter("fecha_if_de") == null) ? "" : request.getParameter("fecha_if_de");
    String fechaIfa = (request.getParameter("fecha_if_a") == null) ? "" : request.getParameter("fecha_if_a");
    String tasade = (request.getParameter("tasa_de") == null) ? "" : request.getParameter("tasa_de");
    String tasaa = (request.getParameter("tasa_a") == null) ? "" : request.getParameter("tasa_a");
    String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
    String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
	 String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");
	 String sOperaFactConMandato = (request.getParameter("sOperaFactConMandato") == null) ? "" : request.getParameter("sOperaFactConMandato");
	 String mandant = (request.getParameter("mandant") == null) ? "" : request.getParameter("mandant");

		if (noEstatusDocto.equals("") || noEstatusDocto.equals("1") || noEstatusDocto.equals("2") || noEstatusDocto.equals("5") || noEstatusDocto.equals("6") || noEstatusDocto.equals("7") || noEstatusDocto.equals("9") || noEstatusDocto.equals("10") || noEstatusDocto.equals("21") || noEstatusDocto.equals("23"))
		{
			qrySentencia.append(
				"select D.ic_documento"+
        ",'ConsDoctosPymeDE::getDocumentQuery'"+
        " from com_documento D"+
				"	where D.ic_pyme = ? ");

			if (!noEpo.equals(""))
				qrySentencia.append(" and D.ic_epo = ? ") ;
			if (!noIf.equals(""))
				qrySentencia.append(" and D.ic_if = ? ");
			if (!noDocto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ? ");
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals("")))
				qrySentencia.append(" and D.DF_FECHA_DOCTO between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if(!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=? ");
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals("")))
				qrySentencia.append(" and D.DF_FECHA_VENC"+sFechaVencPyme+" between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if (!noMoneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ? ");
			if ((!montoDe.equals("")) && (!montoA.equals("")))
				qrySentencia.append(" and D.fn_monto between ? and ? ");
			if (!noEstatusDocto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ? ");
			else
				qrySentencia.append(" and D.ic_estatus_docto in (1, 2, 5, 6, 7, 9, 10, 23) ");
			if (!campDina1.equals(""))
				qrySentencia.append(" and D.CG_CAMPO1 = ? ");
			if (!campDina2.equals(""))
				qrySentencia.append(" and D.CG_CAMPO2 = ? ");

			int coma=0;
			if (!ordNoDocto.equals("") || !ordEpo.equals("") || !ordFecVenc.equals("") )
			{
				if (ordNoDocto.equals("")){ordNoDocto = "0";}
				if (ordEpo.equals("")){ordEpo = "0";}
				if (ordFecVenc.equals("")){ordFecVenc = "0";}
				if ((Integer.parseInt(ordNoDocto) >= 1 && Integer.parseInt(ordNoDocto) <= 4) || (Integer.parseInt(ordEpo) >= 1 && Integer.parseInt(ordEpo) <= 4) || (Integer.parseInt(ordFecVenc) >= 1 && Integer.parseInt(ordFecVenc) <= 4))
				{
					qrySentencia.append(" order by ");
					for (int o=1; o<=4; o++) {
						if (Integer.parseInt(ordNoDocto) == o) {
							coma++;
							qrySentencia.append((coma == 1)?" D.ig_numero_docto ":", D.ig_numero_docto ");
						}
						if (Integer.parseInt(ordEpo) == o) {
							coma++;
							qrySentencia.append((coma == 1)?" D.ic_epo ":", D.ic_epo ");
						}
						if (Integer.parseInt(ordFecVenc) == o) {
							coma++;
							qrySentencia.append((coma == 1)?" D.df_fecha_venc"+sFechaVencPyme+"":", D.df_fecha_venc"+sFechaVencPyme+"");
						}
					} // for
				} // entre 1 y 4.
			}//if de ordenamiento
		}
		//else  if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24")){
		else  if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24") || noEstatusDocto.equals("26")){//FODEA 005 - 2009 ACF

			qrySentencia.append(
				"select DS.ic_documento"+
        ",'ConsDoctosPymeDE::getDocumentQuery'"+
        " from com_documento D"+
        " ,com_docto_seleccionado DS"+
        " ,com_solicitud S"+
				"	where D.IC_DOCUMENTO = DS.IC_DOCUMENTO"+
        " AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO(+)"+
        " AND D.ic_pyme = ? "+
        " AND D.ic_pyme = ? ");

			if(!noEpo.equals(""))
				qrySentencia.append(" and D.ic_epo = ? ");
			if(!noIf.equals(""))
				qrySentencia.append(" and D.ic_if = ? ");
			if (!noDocto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ? ");
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals("")))
				qrySentencia.append(" and D.DF_FECHA_DOCTO between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if(!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=? ");
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals("")))
				qrySentencia.append(" and D.DF_FECHA_VENC"+sFechaVencPyme+" between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if (!noMoneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ? ");
			if ((!montoDe.equals("")) && (!montoA.equals("")))
				qrySentencia.append(" and D.fn_monto between ? and ? ");
			if (!noEstatusDocto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ? ");
			if (noEstatusDocto.equals("4") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") ) { //Solo cuando es Operada y, Operada Pagada, Operada Pendiente de pago y Aplicado a credito se toma encuenta la fecha aut IF.
				if ((!fechaIfde.equals("")) && (!fechaIfa.equals(""))) {
					qrySentencia.append(" and TO_DATE(TO_CHAR(S.df_fecha_solicitud, 'DD/MM/YYYY'), 'DD/MM/YYYY') between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				}
			}
			if ((!tasade.equals("") ) && (!tasaa.equals("")))
				qrySentencia.append(" and DS.in_tasa_aceptada between ? and ? ");
			if (!campDina1.equals(""))
				qrySentencia.append(" and D.CG_CAMPO1 = ?");
			if (!campDina2.equals(""))
				qrySentencia.append(" and D.CG_CAMPO2 = ?");
    	}
		//FODEA 005 - 2009 ACF (I)
    	/*else if (noEstatusDocto.equals("26")) //26-Programado NCM
		{
			qrySentencia.append(
							"select DS.ic_documento, "+
					        " 'ConsDoctosPymeDE::getDocumentQuery' "+
					        " FROM com_documento D, "+
					        " 	   com_docto_seleccionado DS, "+
					        " 	   com_solicitud S "+
							" WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO "+
							" AND D.ic_estatus_docto = 26 "+ //26-Promamada
					        " AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO(+) "+
					        " AND D.ic_pyme = ? "+
					        " AND D.ic_pyme = ? ");
			if(!noEpo.equals(""))
				qrySentencia.append(" and D.ic_epo = ? ");
			if(!noIf.equals(""))
				qrySentencia.append(" and D.ic_if = ? ");
			if (!noDocto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ? ");
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals("")))
				qrySentencia.append(" and D.DF_FECHA_DOCTO between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if(!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=? ");
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals("")))
				qrySentencia.append(" and D.DF_FECHA_VENC"+sFechaVencPyme+" between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if (!noMoneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ? ");
			if ((!montoDe.equals("")) && (!montoA.equals("")))
				qrySentencia.append(" and D.fn_monto between ? and ? ");
			/*
			if (noEstatusDocto.equals("4") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") ) { //Solo cuando es Operada y, Operada Pagada, Operada Pendiente de pago y Aplicado a credito se toma encuenta la fecha aut IF.
				if ((!fechaIfde.equals("")) && (!fechaIfa.equals(""))) {
					qrySentencia.append(" and TO_DATE(TO_CHAR(S.df_fecha_solicitud, 'DD/MM/YYYY'), 'DD/MM/YYYY') between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				}
			}*/
			
			/*
			if ((!tasade.equals("") ) && (!tasaa.equals("")))
				qrySentencia.append(" and DS.in_tasa_aceptada between ? and ? ");
			if (!campDina1.equals(""))
				qrySentencia.append(" and D.CG_CAMPO1 = ?");
			if (!campDina2.equals(""))
				qrySentencia.append(" and D.CG_CAMPO2 = ?");
			*/
		//}
		//FODEA 005 - 2009 ACF (F)
    	//else if
		System.out.println("PKQuery:  "+qrySentencia.toString());
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}

	/**
	 * RJV
	 *
	 * Sirve para regresar el query con todos los datos
	 * de acuerdo a los criterios de b�squeda.
	 * Para generar el archivo a petici�n del
	 * usuario.
	 */

	public String getDocumentQueryFile(HttpServletRequest request) {
		log.info("getDocumentQueryFile(E)");
	 StringBuffer qrySentencia = new StringBuffer();
	 StringBuffer mandantecond = new StringBuffer();
	 StringBuffer mandantetabs = new StringBuffer();
	 StringBuffer mandantecamp = new StringBuffer();
    String operacion = (request.getParameter("operacion") == null ) ? "" : request.getParameter("operacion");
    String noEpo = (request.getParameter("no_epo") == null) ? "" : request.getParameter("no_epo");
    // IMPORTANTE: EL CAMPO NO_EPO ES OBLIGATORIO
    String noIf = (request.getParameter("no_if") == null) ? "" : request.getParameter("no_if");
    String noDocto = (request.getParameter("no_docto") == null) ? "" : request.getParameter("no_docto");
    String dfFechaDoctode = (request.getParameter("df_fecha_docto_de") == null) ? "" : request.getParameter("df_fecha_docto_de");
    String dfFechaDoctoa = (request.getParameter("df_fecha_docto_a") == null) ? "" : request.getParameter("df_fecha_docto_a");
    String campDina1 = (request.getParameter("camp_dina1") == null) ? "" : request.getParameter("camp_dina1");
    String campDina2 = (request.getParameter("camp_dina2") == null) ? "" : request.getParameter("camp_dina2");
    String ordIf = (request.getParameter("ord_if") == null) ? "" : request.getParameter("ord_if");
    String ordNoDocto = (request.getParameter("ord_no_docto") == null) ? "" : request.getParameter("ord_no_docto");
    String ordEpo = (request.getParameter("ord_epo") == null) ? "" : request.getParameter("ord_epo");
    String ordFecVenc = (request.getParameter("ord_fec_venc") == null) ? "" : request.getParameter("ord_fec_venc");
    String dfFechaVencde = (request.getParameter("df_fecha_venc_de") == null) ? "" : request.getParameter("df_fecha_venc_de");
    String dfFechaVenca = (request.getParameter("df_fecha_venc_a") == null) ? "" : request.getParameter("df_fecha_venc_a");
    String noMoneda = (request.getParameter("no_moneda") == null) ? "" : request.getParameter("no_moneda");
    String montoDe = (request.getParameter("monto_de") == null) ? "" : request.getParameter("monto_de");
    String montoA = (request.getParameter("monto_a") == null) ? "" : request.getParameter("monto_a");
    String noEstatusDocto = (request.getParameter("no_estatus_docto") == null) ? "" : request.getParameter("no_estatus_docto");
    String fechaIfde = (request.getParameter("fecha_if_de") == null) ? "" : request.getParameter("fecha_if_de");
    String fechaIfa = (request.getParameter("fecha_if_a") == null) ? "" : request.getParameter("fecha_if_a");
    String tasade = (request.getParameter("tasa_de") == null) ? "" : request.getParameter("tasa_de");
    String tasaa = (request.getParameter("tasa_a") == null) ? "" : request.getParameter("tasa_a");
    String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
    String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
	 String sFechaVencPyme = (request.getParameter("sFechaVencPyme") == null) ? "" : request.getParameter("sFechaVencPyme");
	 String sOperaFactConMandato = (request.getParameter("sOperaFactConMandato") == null) ? "" : request.getParameter("sOperaFactConMandato");
	 String mandant = (request.getParameter("mandant") == null) ? "" : request.getParameter("mandant");
 
		/* Para manejo de idioma ingl�s, en el jsp se define un campo "idiomaUsuario"
		 * con los valores "ES" para espa�ol o "EN" para ingl�s.
		 */
		String idiomaUsuario =
				(request.getSession().getAttribute("sesIdiomaUsuario") == null ) ? "ES" :
				(String) request.getSession().getAttribute("sesIdiomaUsuario");

		String campoNombreMoneda = null;
		String campoNombreEstatusDocto = null;
		
		if (idiomaUsuario.equals("EN")) {
			campoNombreMoneda = " M.cd_nombre_ing ";
			campoNombreEstatusDocto = " ED.cd_descripcion_ing ";
		} else {
			campoNombreMoneda = " M.cd_nombre ";
			campoNombreEstatusDocto = " ED.cd_descripcion ";
		}
		
		//System.out.println(" ������ mandant -:Genera archivo:- ���� "+mandant+ " ������");
		
		if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
				mandantecamp.append( " cm.cg_razon_social AS mandante, " );
				mandantetabs.append(	"	,comcat_mandante cm " );
				if("M".equals(tipoFactoraje)){
				mandantecond.append(" AND d.ic_mandante = cm.ic_mandante" );		 
				}else{
				mandantecond.append(" AND d.ic_mandante = cm.ic_mandante(+)" );		 
				}
		}
		if(!"".equals(mandant)){
			mandantecond.append(	"  AND cm.IC_MANDANTE = "+mandant+" " );
		}

		if (noEstatusDocto.equals("")   || 
			 noEstatusDocto.equals("1")  || // No Negociable
			 noEstatusDocto.equals("2")  || // Negociable
			 noEstatusDocto.equals("5")  || // Baja
			 noEstatusDocto.equals("6")  || // Descuento Fisico
			 noEstatusDocto.equals("7")  || // Pagado Anticipado
			 noEstatusDocto.equals("9")  || // Vencido sin Operar
			 noEstatusDocto.equals("10") || // Pagado sin Operar
			 noEstatusDocto.equals("21") || // Bloqueado
			 noEstatusDocto.equals("23"))   // Pignorado
		{
			qrySentencia.append(
          " SELECT E.cg_razon_social, "+
					"	D.ig_numero_docto, "+
					"	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY'), " +
					"	TO_CHAR(D.DF_FECHA_VENC"+sFechaVencPyme+",'DD/MM/YYYY'), "+
					campoNombreMoneda + ", "+
					"	D.fn_monto, "+
					"	D.fn_porc_anticipo, "+
					"	ED.ic_estatus_docto, "+
					"	D.fn_monto_dscto, " +
					campoNombreEstatusDocto + ", "+
					"	D.ic_moneda, "+
					"	D.ic_documento, "+
					"	D.cs_cambio_importe, "+
					//"	(decode (I.cs_habilitado, 'N', '*', 'S', ' ') ||' '||I.cg_razon_social) as cg_razon_social, "+		//linea a modificar FODEA 17
					"	decode(ds.cs_opera_fiso, 'S', I3.cg_razon_social, 'N', (decode (I.cs_habilitado, 'N', '*', 'S', ' ')||' '||I.cg_razon_social)) as cg_razon_social,"+	//FODEA 17
          			"	D.cs_detalle, " +
					" 	D.cg_campo1, D.cg_campo2, D.cg_campo3, D.cg_campo4, D.cg_campo5, " +
					"	D.CS_DSCTO_ESPECIAL, " +
					"	I2.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO, " +
					"	D.FN_PORC_BENEFICIARIO, " +
					"	(D.FN_MONTO * D.FN_PORC_BENEFICIARIO ) / 100 RECIBIR_BENEFICIARIO, " +
					"	D.CT_REFERENCIA, " +
					" TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO, " +
					" tf.cg_nombre as TIPO_FACTORAJE, "+mandantecamp+
					" 	'ConsDoctosPymeDE::getDocumentQueryFile'"+
					" FROM comcat_epo E, "+
					"	comcat_pyme P, "+
					"	com_documento D, "+
					"	com_docto_seleccionado ds, "+		//FODEA 17
					"	comcat_moneda M, " +
					"	comcat_estatus_docto ED, "+
					"	comcat_if I, " +
					"	comcat_if I2, " +
					"	comcat_if I3, " +		//FODEA 17
					"  comcat_tipo_factoraje tf " +mandantetabs+
					" WHERE D.ic_epo = E.ic_epo " +
					"	and D.ic_pyme = P.ic_pyme " +
					"	and D.ic_if = I.ic_if(+) " +
					"	and ds.ic_if = I3.ic_if(+) " +		//FODEA 17
					"	and D.ic_moneda = M.ic_moneda " +
					"	and D.ic_estatus_docto = ED.ic_estatus_docto "+
					"	AND D.ic_pyme = ?" + 
					"  AND d.ic_documento = ds.ic_documento(+) "+
					"  AND D.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+mandantecond+
					"	AND D.ic_beneficiario = I2.ic_if (+) ");

			if (!noEpo.equals(""))
				qrySentencia.append(" and D.ic_epo = ?") ;
			if (!noIf.equals(""))
				qrySentencia.append(" and D.ic_if = ?");
			if (!noDocto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ? ");
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals("")))
				qrySentencia.append(" and D.DF_FECHA_DOCTO between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if(!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=? ");
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals("")))
				qrySentencia.append(" and D.DF_FECHA_VENC"+sFechaVencPyme+" between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if (!noMoneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ? ");
			if ((!montoDe.equals("")) && (!montoA.equals("")))
				qrySentencia.append(" and D.fn_monto between ? and ? ");
			if (!noEstatusDocto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ? ");
			else
				qrySentencia.append(" and D.ic_estatus_docto in (1, 2, 5, 6, 7, 9, 10) ");
			if (!campDina1.equals(""))
				qrySentencia.append(" and D.CG_CAMPO1 = ?");
			if (!campDina2.equals(""))
				qrySentencia.append(" and D.CG_CAMPO2 = ?");

			int coma=0;
			if (!ordNoDocto.equals("") || !ordEpo.equals("") || !ordFecVenc.equals("") )
			{
				if (ordNoDocto.equals("")){ordNoDocto = "0";}
				if (ordEpo.equals("")){ordEpo = "0";}
				if (ordFecVenc.equals("")){ordFecVenc = "0";}
				if ((Integer.parseInt(ordNoDocto) >= 1 && Integer.parseInt(ordNoDocto) <= 4) || (Integer.parseInt(ordEpo) >= 1 && Integer.parseInt(ordEpo) <= 4) || (Integer.parseInt(ordFecVenc) >= 1 && Integer.parseInt(ordFecVenc) <= 4))
				{
					qrySentencia.append(" order by ");
					for (int o=1; o<=4; o++) {
						if (Integer.parseInt(ordNoDocto) == o) {
							coma++;
							qrySentencia.append((coma == 1)?" D.ig_numero_docto ":", D.ig_numero_docto ");
						}
						if (Integer.parseInt(ordEpo) == o) {
							coma++;
							qrySentencia.append((coma == 1)?" D.ic_epo ":", D.ic_epo ");
						}
						if (Integer.parseInt(ordFecVenc) == o) {
							coma++;
							qrySentencia.append((coma == 1)?" D.df_fecha_venc"+sFechaVencPyme+"":", D.df_fecha_venc"+sFechaVencPyme+"");
						}
					} // for
				} // entre 1 y 4.
			}//if de ordenamiento
		}
		else if (noEstatusDocto.equals("3")  || // Seleccionada Pyme
		         noEstatusDocto.equals("4")  || // Operada
					noEstatusDocto.equals("8")  || // Seleccionado IF(obsoleto)
					noEstatusDocto.equals("11") || // Operada Pagada
					noEstatusDocto.equals("12") || // Operada Pendiente de Pago
					noEstatusDocto.equals("16") || // Aplicado a Credito
					noEstatusDocto.equals("24") || // En Proceso de Autorizacion IF
					noEstatusDocto.equals("26"))   // Programado Pyme -- FODEA 005 - 2009 ACF
		{
			
			 // Obtener fecha del siguiente d�a h�bil por EPO
			 AccesoDB 				con 							= new AccesoDB();
			 String 					fechaHabilSiguienteXEpo = null; //new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			 PreparedStatement 	ps 							= null;
			 ResultSet 				rs 							= null;
			 try {
					
				con.conexionDB();
					
				String qrySentencia1 = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
				ps 						= con.queryPrecompilado(qrySentencia1);
				ps.executeUpdate();
				ps.close();
					
				qrySentencia1 = 
					" SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha"   +
					"   FROM dual"  ;
				ps = con.queryPrecompilado(qrySentencia1);
				ps.setInt(1, Integer.parseInt(noEpo));
				rs = ps.executeQuery();
				ps.clearParameters();
				if(rs.next()) {
					fechaHabilSiguienteXEpo = rs.getString(1); //==null?fechaHabilSiguienteXEpo:rs.getString(1);
				}
 
			} catch (Exception e) {
				System.out.println("Exception: "+e);
			} finally {
					
				if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
				if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
				
				con.terminaTransaccion(true);
				if (con.hayConexionAbierta() == true)
					con.cierraConexionDB();
					
			}
			
			// Este error NUNCA DEBER�A DE OCURRIR se deja para avisar al usuario que se presento un problema con la
			// funcion sigfechahabilxepo y de esta manera evitar que se le muestre un plazo erroneo
			if( fechaHabilSiguienteXEpo == null ){
				throw new AppException("No se pudo determinar la fecha del d�a h�bil siguiente por EPO");
			}
	
			// Obtener condiciones de consulta del plazo
			String plazo 				= null;
			String condicionPlazo 	= null;
			String tablaPlazo			= null;
			if(
				noEstatusDocto.equals("3") 	// Seleccionada Pyme
					||
				noEstatusDocto.equals("24")	// En proceso de Autorizacion IF
			){
				plazo 			= 
					" ( " +
						" D.df_fecha_venc"+sFechaVencPyme + " - " +
						" 	(CASE  " +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
						"    TRUNC(SYSDATE) " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
						"    TO_DATE( '"+fechaHabilSiguienteXEpo+"', 'DD/MM/YYYY' ) " +
						" 	END) " + 
					" ) " + 	
					" AS ig_plazo, ";
					
				condicionPlazo =
						" 	AND D.ic_if   = iexp.ic_if     " +
						" 	AND D.ic_epo  = iexp.ic_epo    " +
						" 	AND iexp.ic_producto_nafin = 1 ";
						
				tablaPlazo		= 
						" , comrel_if_epo_x_producto iexp ";
						
			} else if( 
				noEstatusDocto.equals("26") // Programado Pyme
			){ 
			
				// Nota: como funciona el siguiente dia habil cuando se consulte el documento en su d�a programado 
				// Con el propsito de agilizar la consulta, obtener la fecha del segundo d�a h�bil sguiente por EPO
				con = new AccesoDB();
				String segundaFechaHabilSiguienteXEpo = null;
				ps = null;
				rs = null;
				try {
					
					con.conexionDB();
					
					String qrySentencia1 	= "ALTER SESSION SET NLS_TERRITORY = MEXICO";
					ps 						= con.queryPrecompilado(qrySentencia1);
					ps.executeUpdate();
					ps.close();
					   
					qrySentencia1 = 
						" SELECT TO_CHAR( sigfechahabilxepo (?, to_date(?,'DD/MM/YYYY'), 1, '+'), 'dd/mm/yyyy' ) fecha "   +
						"   FROM dual"  ;
					ps = con.queryPrecompilado(qrySentencia1);
					ps.setInt(1, 		Integer.parseInt(noEpo) );
					ps.setString(2, 	fechaHabilSiguienteXEpo );
					rs = ps.executeQuery();
					ps.clearParameters();
					if(rs.next()) {
						segundaFechaHabilSiguienteXEpo = rs.getString(1);
					} 
					
					// Este error NUNCA DEBER�A DE OCURRIR se deja para avisar al usuario que se presento un problema con la
					// funcion sigfechahabilxepo y de esta manera evitar que se le muestre un plazo erroneo
					if( segundaFechaHabilSiguienteXEpo == null ){
						throw new AppException("No se pudo determinar la fecha del segundo d�a h�bil siguiente por EPO");
					}
										
				} catch (Exception e) {
					System.out.println("Exception: "+e);
				} finally {
					
					if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
					if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
					
					con.terminaTransaccion(true);
					if (con.hayConexionAbierta() == true)
						con.cierraConexionDB();
					
				}
		
				// Agregar vaiables asociadas al plazo
				plazo 			=
					" ( "  +
						" D.df_fecha_venc"+sFechaVencPyme + " - "  +
						" 	(CASE  "  +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN "  +
						"    TO_DATE( '"+fechaHabilSiguienteXEpo+"',        'DD/MM/YYYY' ) " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN "  +
						"    TO_DATE( '"+segundaFechaHabilSiguienteXEpo+"', 'DD/MM/YYYY' ) " +
						" 	END) "  + 
					" ) "  + 	
					" AS ig_plazo, ";
					
				condicionPlazo =
						" 	AND D.ic_if   = iexp.ic_if     " +
						" 	AND D.ic_epo  = iexp.ic_epo    " +
						" 	AND iexp.ic_producto_nafin = 1 ";
						
				tablaPlazo		= 
						" , comrel_if_epo_x_producto iexp ";
 	
			} else {
				
				//"   DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc"+sFechaVencPyme+")-DECODE(D.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaHabilSiguienteXEpo+"', 'dd/mm/yyyy')), S.ig_plazo ) ig_plazo, ";
				plazo 			= "   NVL(S.ig_plazo, TRUNC(D.df_fecha_venc"+sFechaVencPyme+")-DECODE(D.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaHabilSiguienteXEpo+"', 'dd/mm/yyyy')) ) ig_plazo, "; 
				condicionPlazo = null;
				tablaPlazo		= null;
				
			} 
			
			// Construir Query
			qrySentencia.append("SELECT E.cg_razon_social, "+
					//"	(decode (I.cs_habilitado, 'N', '*', 'S', ' ') ||' '||I.cg_razon_social) as cg_razon_social, "+	//linea a modificar por FODEA 17
					"	decode(DS.cs_opera_fiso, 'S', I3.cg_razon_social, 'N', (decode (I.cs_habilitado, 'N', '*', 'S', ' ')||' '||I.cg_razon_social)) as cg_razon_social,"+	//FODEA 17
					"	D.ig_numero_docto, "+
					"	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY'), "+
					"	TO_CHAR(D.DF_FECHA_VENC"+sFechaVencPyme+",'DD/MM/YYYY'), " +
					campoNombreMoneda + ", "+
					"	D.fn_monto, "+
					"	D.fn_porc_anticipo, "+
					"	D.fn_monto_dscto, "+
					"	DS.in_tasa_aceptada, "+
					campoNombreEstatusDocto + ", "+
					"	TO_CHAR(S.df_fecha_solicitud,'DD/MM/YYYY'), "+
					"	D.ic_moneda, "+
					"	D.ic_documento, " +
					"	DS.in_importe_recibir, "+
					"	D.cs_cambio_importe, " +
          			"	D.cs_detalle, " +
					" 	D.cg_campo1, D.cg_campo2, D.cg_campo3, D.cg_campo4, D.cg_campo5, " +
					"	D.CS_DSCTO_ESPECIAL, "+
					"	D.ic_estatus_docto, "+
					" 	CASE WHEN cc1.fn_monto_pago IS NOT NULL THEN"   +
					" 		cc1.fn_monto_pago"   +
					" 	ELSE "   +
					" 		CASE WHEN cd.fn_monto_pago IS NOT NULL THEN"   +
					" 			cd.fn_monto_pago"   +
					" 		END"   +
					"	END as fn_monto_pago," +
					"	DS.cc_acuse,"+
					"	DS.IN_IMPORTE_RECIBIR - DS.fn_importe_recibir_benef NETO_RECIBIR, " +
					"	I2.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO, " +
					"	D.FN_PORC_BENEFICIARIO, " +
					"	DS.fn_importe_recibir_benef  RECIBIR_BENEFICIARIO, " +
					" 	CASE WHEN cc1.fn_porc_docto_aplicado IS NOT NULL THEN " +
					" 		cc1.fn_porc_docto_aplicado " +
					" 	ELSE " +
					" 		CASE WHEN cd.fn_porc_docto_aplicado IS NOT NULL THEN " +
					" 			cd.fn_porc_docto_aplicado " +
					" 		END " +
					" 	END as porcDoctoAplicado, " +
					plazo  + 
					"	DS.in_importe_interes, "+
					"   I.ig_tipo_piso, DS.fn_remanente, "+ //foda 015-2005
					"	D.CT_REFERENCIA, " +
					" TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO, " +
					" tf.cg_nombre as TIPO_FACTORAJE, "+mandantecamp+
					" TO_CHAR(ds.df_programacion, 'dd/mm/yyyy') AS fecha_registro_operacion, "+//FODEA 005 - 2009 ACF
					" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND d.ic_docto_asociado IS NOT NULL THEN 'true' ELSE 'false' END AS existeDocumentoAsociado, " +
					" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM com_documento WHERE ic_docto_asociado = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredSimpleApl, " +
					" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_NOTA_CREDITO = d.ic_documento) THEN 'true' ELSE 'false' END AS existeRefEnComrelNotaDocto, " +
					" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_DOCUMENTO = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredMultApl, " +
					"	'ConsDoctosPymeDE::getDocumentSummaryQueryForIds'"+
					" FROM comcat_epo E, "+
					"	comcat_pyme P, "+
					"	comcat_if I, "+
					"	comcat_if I2, " +
					"	comcat_if I3, " +	//FODEA 17
					"	com_documento D, " +
					"	comcat_moneda M, "+
					"	com_docto_seleccionado DS, "+
					"	comcat_estatus_docto ED, "+
					"	com_solicitud S, " +
					"	(select cc.ic_documento, cc.fn_porc_docto_aplicado , " +
					"		sum(cc.fn_monto_pago) as fn_monto_pago " +
					"	from com_cobro_credito cc, com_documento d " +
					"	where cc.ic_documento = d.ic_documento " +
					"		and d.ic_pyme = ?" + 
					"	group by cc.ic_documento, cc.fn_porc_docto_aplicado) cc1, " +
					" 	fop_cobro_disposicion cd, " +
					"  comcat_tipo_factoraje tf " +mandantetabs +
					( tablaPlazo != null?tablaPlazo:"" ) +
					" WHERE D.ic_epo = E.ic_epo " +
					"	AND D.ic_pyme = P.ic_pyme " +
					"	AND D.ic_moneda = M.ic_moneda " +
					"	AND D.ic_documento = DS.ic_documento " +
					"	AND D.ic_estatus_docto = ED.ic_estatus_docto " +
					"	AND D.ic_documento = S.ic_documento(+) " +
					"	AND D.ic_if = I.ic_if(+)"+
					"	AND DS.ic_if = I3.ic_if(+)"+	//FODEA 17
					"	AND D.ic_documento = CC1.ic_documento(+) " +
					"	AND D.ic_documento = CD.ic_documento(+) " +
					"	AND D.ic_beneficiario = I2.ic_if (+) "+
					"  AND D.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+mandantecond+
					"	AND D.ic_pyme = ? " +
					(condicionPlazo != null?condicionPlazo:"" )
				);

			if(!noEpo.equals(""))
				qrySentencia.append(" and D.ic_epo = ? ");
			if(!noIf.equals(""))
				qrySentencia.append(" and D.ic_if = ? ");
			if (!noDocto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ? ");
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals("")))
				qrySentencia.append(" and D.DF_FECHA_DOCTO between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if(!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=? ");
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals("")))
				qrySentencia.append(" and D.DF_FECHA_VENC"+sFechaVencPyme+" between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if (!noMoneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ? ");
			if ((!montoDe.equals("")) && (!montoA.equals("")))
				qrySentencia.append(" and D.fn_monto between ? and ? ");
			if (!noEstatusDocto.equals(""))
				qrySentencia.append(" and D.ic_estatus_docto = ? ");
			if (noEstatusDocto.equals("4") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") ) { //Solo cuando es Operada y, Operada Pagada, Operada Pendiente de pago y Aplicado a credito se toma encuenta la fecha aut IF.
				if ((!fechaIfde.equals("")) && (!fechaIfa.equals(""))) {
					qrySentencia.append(" and TO_DATE(TO_CHAR(S.df_fecha_solicitud, 'DD/MM/YYYY'), 'DD/MM/YYYY') between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				}
			}
			if ((!tasade.equals("") ) && (!tasaa.equals("")))
				qrySentencia.append(" and DS.in_tasa_aceptada between ? and ? ");
			if (!campDina1.equals(""))
				qrySentencia.append(" and D.CG_CAMPO1 = ?");
			if (!campDina2.equals(""))
				qrySentencia.append(" and D.CG_CAMPO2 = ?");
		}
		//FODEA 005 - 2009 ACF (I)
		/*else if (noEstatusDocto.equals("26")) //26-Programado NCM
		{	
			
			qrySentencia.append("SELECT E.cg_razon_social, "+
					"	(decode (I.cs_habilitado, 'N', '*', 'S', ' ') ||' '||I.cg_razon_social) as cg_razon_social, "+
					"	D.ig_numero_docto, "+
					"	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY'), "+
					"	TO_CHAR(D.DF_FECHA_VENC"+sFechaVencPyme+",'DD/MM/YYYY'), " +
					campoNombreMoneda + ", "+
					"	D.fn_monto, "+
					"	D.fn_porc_anticipo, "+
					"	D.fn_monto_dscto, "+
					"	DS.in_tasa_aceptada, "+
					campoNombreEstatusDocto + ", "+
					"	TO_CHAR(S.df_fecha_solicitud,'DD/MM/YYYY'), "+
					"	D.ic_moneda, "+
					"	D.ic_documento, " +
					"	DS.in_importe_recibir, "+
					"	D.cs_cambio_importe, " +
          			"	D.cs_detalle, " +
					" 	D.cg_campo1, D.cg_campo2, D.cg_campo3, D.cg_campo4, D.cg_campo5, " +
					"	D.CS_DSCTO_ESPECIAL, "+
					"	D.ic_estatus_docto, "+
					" 	CASE WHEN cc1.fn_monto_pago IS NOT NULL THEN"   +
					" 		cc1.fn_monto_pago"   +
					" 	ELSE "   +
					" 		CASE WHEN cd.fn_monto_pago IS NOT NULL THEN"   +
					" 			cd.fn_monto_pago"   +
					" 		END"   +
					"	END as fn_monto_pago," +
					"	DS.cc_acuse,"+
					"	DS.IN_IMPORTE_RECIBIR - DS.fn_importe_recibir_benef NETO_RECIBIR, " +
					"	I2.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO, " +
					"	D.FN_PORC_BENEFICIARIO, " +
					"	DS.fn_importe_recibir_benef  RECIBIR_BENEFICIARIO, " +
					" 	CASE WHEN cc1.fn_porc_docto_aplicado IS NOT NULL THEN " +
					" 		cc1.fn_porc_docto_aplicado " +
					" 	ELSE " +
					" 		CASE WHEN cd.fn_porc_docto_aplicado IS NOT NULL THEN " +
					" 			cd.fn_porc_docto_aplicado " +
					" 		END " +
					" 	END as porcDoctoAplicado, " +
					"	S.ig_plazo, DS.in_importe_interes, "+
					"   I.ig_tipo_piso, DS.fn_remanente, TO_CHAR(ds.df_programacion,'DD/MM/YYYY') as df_programacion, "+
					"	D.CT_REFERENCIA, " +
					
					" TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO, " +
					" tf.cg_nombre as TIPO_FACTORAJE, "+mandantecamp+
					"	'ConsDoctosPymeDE::getDocumentSummaryQueryForIds'"+
					" FROM comcat_epo E, "+
					"	comcat_pyme P, "+
					"	comcat_if I, "+
					"	comcat_if I2, " +
					"	com_documento D, " +
					"	comcat_moneda M, "+
					"	com_docto_seleccionado DS, "+
					"	comcat_estatus_docto ED, "+
					"	com_solicitud S, " +
					"	(select cc.ic_documento, cc.fn_porc_docto_aplicado , " +
					"		sum(cc.fn_monto_pago) as fn_monto_pago " +
					"	from com_cobro_credito cc, com_documento d " +
					"	where cc.ic_documento = d.ic_documento " +
					"		and d.ic_pyme = ?" + 
					"	group by cc.ic_documento, cc.fn_porc_docto_aplicado) cc1, " +
					" 	fop_cobro_disposicion cd, " +
					"  comcat_tipo_factoraje tf " +mandantetabs+
					" WHERE D.ic_epo = E.ic_epo " +
					"   AND D.ic_estatus_docto = 26 "+ //26-Promamada					
					"	AND D.ic_pyme = P.ic_pyme " +
					"	AND D.ic_moneda = M.ic_moneda " +
					"	AND D.ic_documento = DS.ic_documento " +
					"	AND D.ic_estatus_docto = ED.ic_estatus_docto " +
					"	AND D.ic_documento = S.ic_documento(+) " +
					"	AND D.ic_if = I.ic_if(+)"+
					"	AND D.ic_documento = CC1.ic_documento(+) " +
					"	AND D.ic_documento = CD.ic_documento(+) " +
					"	AND D.ic_beneficiario = I2.ic_if (+) "+
					"  AND D.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+mandantecond+
					"	AND D.ic_pyme = ? ");

			if(!noEpo.equals(""))
				qrySentencia.append(" and D.ic_epo = ? ");
			if(!noIf.equals(""))
				qrySentencia.append(" and D.ic_if = ? ");
			if (!noDocto.equals(""))
				qrySentencia.append(" and D.ig_numero_docto = ? ");
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals("")))
				qrySentencia.append(" and D.DF_FECHA_DOCTO between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if(!tipoFactoraje.equals(""))
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=? ");
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals("")))
				qrySentencia.append(" and D.DF_FECHA_VENC"+sFechaVencPyme+" between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			if (!noMoneda.equals(""))
				qrySentencia.append(" and D.ic_moneda = ? ");
			if ((!montoDe.equals("")) && (!montoA.equals("")))
				qrySentencia.append(" and D.fn_monto between ? and ? ");
			/*
			if (noEstatusDocto.equals("4") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") ) { //Solo cuando es Operada y, Operada Pagada, Operada Pendiente de pago y Aplicado a credito se toma encuenta la fecha aut IF.
				if ((!fechaIfde.equals("")) && (!fechaIfa.equals(""))) {
					qrySentencia.append(" and TO_DATE(TO_CHAR(S.df_fecha_solicitud, 'DD/MM/YYYY'), 'DD/MM/YYYY') between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				}
			}
			if ((!tasade.equals("") ) && (!tasaa.equals("")))
				qrySentencia.append(" and DS.in_tasa_aceptada between ? and ? ");
			if (!campDina1.equals(""))
				qrySentencia.append(" and D.CG_CAMPO1 = ?");
			if (!campDina2.equals(""))
				qrySentencia.append(" and D.CG_CAMPO2 = ?");
			*/
		//}
		//FODEA 005 - 2009 ACF (I)
		else{
			System.out.println("ic_estatus_docto value not handled:  " + noEstatusDocto);
		}
		System.out.println("PageQuery:  "+qrySentencia);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
  }
    
  public static String remplazar(String psWord, String psReplace, String psNewSeg) {
     StringBuffer lsNewStr = new StringBuffer();
     int liFound = 0;
     int liLastPointer=0;	
     do {	
       liFound = psWord.indexOf(psReplace, liLastPointer);

       if ( liFound < 0 )
          lsNewStr.append(psWord.substring(liLastPointer,psWord.length()));

       else {	
          if (liFound > liLastPointer)
             lsNewStr.append(psWord.substring(liLastPointer,liFound));	
          lsNewStr.append(psNewSeg);
          liLastPointer = liFound + psReplace.length();
       }	
     }while (liFound > -1);	
     return lsNewStr.toString();
  }

/*******************************************************************************
 *          Migraci�n. Implementa IQueryGeneratorRegExtJS.java                 *
 *******************************************************************************/
	private List conditions;
	private String noEpo;
	private String noIf;
	private String noDocto;
	private String dfFechaDoctode;
	private String dfFechaDoctoa;
	private String campDina1;
	private String campDina2;
	private String ordIf;
	private String ordNoDocto;
	private String ordEpo;
	private String ordFecVenc;
	private String dfFechaVencde;
	private String dfFechaVenca;
	private String noMoneda;
	private String montoDe;
	private String montoA;
	private String noEstatusDocto;
	private String fechaIfde;
	private String fechaIfa;
	private String tasade;
	private String tasaa;
	private String tipoFactoraje;
	private String iNoCliente;
	private String sFechaVencPyme;
	private String sOperaFactConMandato;
	private String mandant;
	private String idiomaUsuario;
	private String sOperaFactVencimientoInfonavit;
	private String iNoEPO;
	private String ic_epo_aserca;

	public String getDocumentQuery() {
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	public String getAggregateCalculationQuery() {
		return null;
	}

	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer mandantecond = new StringBuffer();
		StringBuffer mandantetabs = new StringBuffer();
		StringBuffer mandantecamp = new StringBuffer();
		conditions = new ArrayList();

		String campoNombreMoneda = null;
		String campoNombreEstatusDocto = null;

		if (idiomaUsuario.equals("EN")) {
			campoNombreMoneda = " M.cd_nombre_ing ";
			campoNombreEstatusDocto = " ED.cd_descripcion_ing ";
		} else {
			campoNombreMoneda = " M.cd_nombre ";
			campoNombreEstatusDocto = " ED.cd_descripcion ";
		}

		if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
				mandantecamp.append( " cm.cg_razon_social AS mandante, ");
				mandantetabs.append(	"	,comcat_mandante cm " );
				if("M".equals(tipoFactoraje)){
				mandantecond.append(" AND d.ic_mandante = cm.ic_mandante" );
				}else{
				mandantecond.append(" AND d.ic_mandante = cm.ic_mandante(+)");
				}
		}
		if(!"".equals(mandant)){
			mandantecond.append(	"  AND cm.IC_MANDANTE = "+mandant+" ");
		}

		if (noEstatusDocto.equals("")   || 
			 noEstatusDocto.equals("1")  || // No Negociable
			 noEstatusDocto.equals("2")  || // Negociable
			 noEstatusDocto.equals("5")  || // Baja
			 noEstatusDocto.equals("6")  || // Descuento Fisico
			 noEstatusDocto.equals("7")  || // Pagado Anticipado
			 noEstatusDocto.equals("9")  || // Vencido sin Operar
			 noEstatusDocto.equals("10") || // Pagado sin Operar
			 noEstatusDocto.equals("21") || // Bloqueado
			 noEstatusDocto.equals("23"))   // Pignorado
		{
			qrySentencia.append(
					" SELECT E.cg_razon_social, "+
					" D.ig_numero_docto, "+
					" TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY'), " +
					" TO_CHAR(D.DF_FECHA_VENC"+sFechaVencPyme+",'DD/MM/YYYY'), "+
					campoNombreMoneda + ", "+
					" D.fn_monto, "+
					" D.fn_porc_anticipo, "+
					" ED.ic_estatus_docto, "+
					" D.fn_monto_dscto, " +
					campoNombreEstatusDocto + ", "+
					" D.ic_moneda, "+
					" D.ic_documento, "+
					" D.cs_cambio_importe, "+
					" decode(ds.cs_opera_fiso, 'S', I3.cg_razon_social, 'N', (decode (I.cs_habilitado, 'N', '*', 'S', ' ')||' '||I.cg_razon_social)) as cg_razon_social,"+	//FODEA 17
					" D.cs_detalle, " +
					" D.cg_campo1, D.cg_campo2, D.cg_campo3, D.cg_campo4, D.cg_campo5, " +
					" D.CS_DSCTO_ESPECIAL, " +
					" I2.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO, " +
					" D.FN_PORC_BENEFICIARIO, " +
					" (D.FN_MONTO * D.FN_PORC_BENEFICIARIO ) / 100 RECIBIR_BENEFICIARIO, " +
					" D.CT_REFERENCIA, " +
					" TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO, " +
					" tf.cg_nombre as TIPO_FACTORAJE, "+mandantecamp+
					" 'ConsDoctosPymeDE::getDocumentQueryFile'"+
					" FROM comcat_epo E, "+
					" comcat_pyme P, "+
					" com_documento D, "+
					" com_docto_seleccionado ds, "+		//FODEA 17
					" comcat_moneda M, " +
					" comcat_estatus_docto ED, "+
					" comcat_if I, " +
					" comcat_if I2, " +
					" comcat_if I3, " +		//FODEA 17
					" comcat_tipo_factoraje tf " +mandantetabs+
					" WHERE D.ic_epo = E.ic_epo " +
					" and D.ic_pyme = P.ic_pyme " +
					" and D.ic_if = I.ic_if(+) " +
					" and ds.ic_if = I3.ic_if(+) " +		//FODEA 17
					" and D.ic_moneda = M.ic_moneda " +
					" and D.ic_estatus_docto = ED.ic_estatus_docto "+
					" AND D.ic_pyme = ?" + 
					" AND d.ic_documento = ds.ic_documento(+) "+
					" AND D.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+mandantecond+
					" AND D.ic_beneficiario = I2.ic_if (+) ");
			conditions.add(iNoCliente);
			if (!noEpo.equals("")){
				qrySentencia.append(" and D.ic_epo = ?");
				conditions.add(noEpo);
			}
			if (!noIf.equals("")){
				qrySentencia.append(" and D.ic_if = ?");
				conditions.add(noIf);
			}
			if (!noDocto.equals("")){
				qrySentencia.append(" and D.ig_numero_docto = ? ");
				conditions.add(noDocto);
			}
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals(""))){
				qrySentencia.append(" and D.DF_FECHA_DOCTO between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(dfFechaDoctode);
				conditions.add(dfFechaDoctoa);
			}
			if(!tipoFactoraje.equals("")){
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=? ");
				conditions.add(tipoFactoraje);
			}
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals(""))){
				qrySentencia.append(" and D.DF_FECHA_VENC"+sFechaVencPyme+" between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(dfFechaVencde);
				conditions.add(dfFechaVenca);
			}
			if (!noMoneda.equals("")){
				qrySentencia.append(" and D.ic_moneda = ? ");
				conditions.add(noMoneda);
			}
			if ((!montoDe.equals("")) && (!montoA.equals(""))){
				qrySentencia.append(" and D.fn_monto between ? and ? ");
				conditions.add(new Double(montoDe));
				conditions.add(new Double(montoA));
			}
			if (!noEstatusDocto.equals("")){
				qrySentencia.append(" and D.ic_estatus_docto = ? ");
				conditions.add(noEstatusDocto);
			} else{
				qrySentencia.append(" and D.ic_estatus_docto in (1, 2, 5, 6, 7, 9, 10) ");
			}
			if (!campDina1.equals("")){
				qrySentencia.append(" and D.CG_CAMPO1 = ?");
				conditions.add(campDina1);
			}
			if (!campDina2.equals("")){
				qrySentencia.append(" and D.CG_CAMPO2 = ?");
				conditions.add(campDina2);
			}
			int coma=0;
			if (!ordNoDocto.equals("") || !ordEpo.equals("") || !ordFecVenc.equals("") ){
				if (ordNoDocto.equals("")){ordNoDocto = "0";}
				if (ordEpo.equals("")){ordEpo = "0";}
				if (ordFecVenc.equals("")){ordFecVenc = "0";}
				if ((Integer.parseInt(ordNoDocto) >= 1 && Integer.parseInt(ordNoDocto) <= 4) || (Integer.parseInt(ordEpo) >= 1 && Integer.parseInt(ordEpo) <= 4) || (Integer.parseInt(ordFecVenc) >= 1 && Integer.parseInt(ordFecVenc) <= 4)){
					qrySentencia.append(" order by ");
					for (int o=1; o<=4; o++) {
						if (Integer.parseInt(ordNoDocto) == o) {
							coma++;
							qrySentencia.append((coma == 1)?" D.ig_numero_docto ":", D.ig_numero_docto ");
						}
						if (Integer.parseInt(ordEpo) == o) {
							coma++;
							qrySentencia.append((coma == 1)?" D.ic_epo ":", D.ic_epo ");
						}
						if (Integer.parseInt(ordFecVenc) == o) {
							coma++;
							qrySentencia.append((coma == 1)?" D.df_fecha_venc"+sFechaVencPyme+"":", D.df_fecha_venc"+sFechaVencPyme+"");
						}
					} // for
				} // entre 1 y 4.
			}//if de ordenamiento
		} else if ( noEstatusDocto.equals("3")  || // Seleccionada Pyme
						noEstatusDocto.equals("4")  || // Operada
						noEstatusDocto.equals("8")  || // Seleccionado IF(obsoleto)
						noEstatusDocto.equals("11") || // Operada Pagada
						noEstatusDocto.equals("12") || // Operada Pendiente de Pago
						noEstatusDocto.equals("16") || // Aplicado a Credito
						noEstatusDocto.equals("24") || // En Proceso de Autorizacion IF
						noEstatusDocto.equals("26"))   // Programado Pyme -- FODEA 005 - 2009 ACF
		{
			
			 // Obtener fecha del siguiente d�a h�bil por EPO
			 AccesoDB con = new AccesoDB();
			 String fechaHabilSiguienteXEpo = null; //new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			 PreparedStatement ps = null;
			 ResultSet rs = null;
			 try {

				con.conexionDB();

				String qrySentencia1 = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
				ps = con.queryPrecompilado(qrySentencia1);
				ps.executeUpdate();
				ps.close();

				qrySentencia1 = " SELECT TO_CHAR(sigfechahabilxepo (?, TRUNC(SYSDATE), 1, '+'), 'dd/mm/yyyy') fecha FROM dual";
				ps = con.queryPrecompilado(qrySentencia1);
				ps.setInt(1, Integer.parseInt(noEpo));
				rs = ps.executeQuery();
				ps.clearParameters();
				if(rs.next()) {
					fechaHabilSiguienteXEpo = rs.getString(1); //==null?fechaHabilSiguienteXEpo:rs.getString(1);
				}
 
			} catch (Exception e) {
				System.out.println("Exception: "+ e);
			} finally {
				if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
				if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
				con.terminaTransaccion(true);
				if (con.hayConexionAbierta() == true)
					con.cierraConexionDB();
			}
			// Este error NUNCA DEBER�A DE OCURRIR se deja para avisar al usuario que se presento un problema con la
			// funcion sigfechahabilxepo y de esta manera evitar que se le muestre un plazo erroneo
			if( fechaHabilSiguienteXEpo == null ){
				throw new AppException("No se pudo determinar la fecha del d�a h�bil siguiente por EPO");
			}

			// Obtener condiciones de consulta del plazo
			String plazo 				= null;
			String condicionPlazo 	= null;
			String tablaPlazo			= null;
			if(
				noEstatusDocto.equals("3") 	// Seleccionada Pyme
					||
				noEstatusDocto.equals("24")	// En proceso de Autorizacion IF
			){
				plazo = 
					" ( " +
						" D.df_fecha_venc"+sFechaVencPyme + " - " +
						" 	(CASE  " +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN " +
						"    TRUNC(SYSDATE) " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN " +
						"    TO_DATE( '"+fechaHabilSiguienteXEpo+"', 'DD/MM/YYYY' ) " +
						" 	END) " + 
					" ) " + 	
					" AS ig_plazo, ";
					
				condicionPlazo =
						" 	AND D.ic_if   = iexp.ic_if     " +
						" 	AND D.ic_epo  = iexp.ic_epo    " +
						" 	AND iexp.ic_producto_nafin = 1 ";
						
				tablaPlazo		= 
						" , comrel_if_epo_x_producto iexp ";
						
			} else if( 
				noEstatusDocto.equals("26") // Programado Pyme
			){ 
			
				// Nota: como funciona el siguiente dia habil cuando se consulte el documento en su d�a programado 
				// Con el propsito de agilizar la consulta, obtener la fecha del segundo d�a h�bil sguiente por EPO
				con = new AccesoDB();
				String segundaFechaHabilSiguienteXEpo = null;
				ps = null;
				rs = null;
				try {
					
					con.conexionDB();
					
					String qrySentencia1 	= "ALTER SESSION SET NLS_TERRITORY = MEXICO";
					ps 						= con.queryPrecompilado(qrySentencia1);
					ps.executeUpdate();
					ps.close();
					   
					qrySentencia1 = 
						" SELECT TO_CHAR( sigfechahabilxepo (?, to_date(?,'DD/MM/YYYY'), 1, '+'), 'dd/mm/yyyy' ) fecha "   +
						"   FROM dual"  ;
					ps = con.queryPrecompilado(qrySentencia1);
					ps.setInt(1, 		Integer.parseInt(noEpo) );
					ps.setString(2, 	fechaHabilSiguienteXEpo );
					rs = ps.executeQuery();
					ps.clearParameters();
					if(rs.next()) {
						segundaFechaHabilSiguienteXEpo = rs.getString(1);
					} 
					
					// Este error NUNCA DEBER�A DE OCURRIR se deja para avisar al usuario que se presento un problema con la
					// funcion sigfechahabilxepo y de esta manera evitar que se le muestre un plazo erroneo
					if( segundaFechaHabilSiguienteXEpo == null ){
						throw new AppException("No se pudo determinar la fecha del segundo d�a h�bil siguiente por EPO");
					}
										
				} catch (Exception e) {
					System.out.println("Exception: "+e);
				} finally {
					
					if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
					if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
					
					con.terminaTransaccion(true);
					if (con.hayConexionAbierta() == true)
						con.cierraConexionDB();
					
				}
		
				// Agregar vaiables asociadas al plazo
				plazo 			=
					" ( "  +
						" D.df_fecha_venc"+sFechaVencPyme + " - "  +
						" 	(CASE  "  +
						" 	WHEN D.ic_moneda = 1 OR (D.ic_moneda = 54 AND iexp.cs_tipo_fondeo = 'P') THEN "  +
						"    TO_DATE( '"+fechaHabilSiguienteXEpo+"',        'DD/MM/YYYY' ) " +
						" 	WHEN D.ic_moneda = 54 AND iexp.cs_tipo_fondeo <> 'P' THEN "  +
						"    TO_DATE( '"+segundaFechaHabilSiguienteXEpo+"', 'DD/MM/YYYY' ) " +
						" 	END) "  + 
					" ) "  + 	
					" AS ig_plazo, ";
					
				condicionPlazo =
						" 	AND D.ic_if   = iexp.ic_if     " +
						" 	AND D.ic_epo  = iexp.ic_epo    " +
						" 	AND iexp.ic_producto_nafin = 1 ";
						
				tablaPlazo		= 
						" , comrel_if_epo_x_producto iexp ";
 	
			} else {
				
				//"   DECODE(D.ic_estatus_docto, 3, TRUNC(D.df_fecha_venc"+sFechaVencPyme+")-DECODE(D.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaHabilSiguienteXEpo+"', 'dd/mm/yyyy')), S.ig_plazo ) ig_plazo, ";
				plazo 			= "   NVL(S.ig_plazo, TRUNC(D.df_fecha_venc"+sFechaVencPyme+")-DECODE(D.ic_moneda, 1, TRUNC(SYSDATE), 54, TO_DATE('"+fechaHabilSiguienteXEpo+"', 'dd/mm/yyyy')) ) ig_plazo, "; 
				condicionPlazo = null;
				tablaPlazo		= null;
				
			} 
			
			// Construir Query
			qrySentencia.append("SELECT E.cg_razon_social, "+
					//"	(decode (I.cs_habilitado, 'N', '*', 'S', ' ') ||' '||I.cg_razon_social) as cg_razon_social, "+	//linea a modificar por FODEA 17
					"	decode(DS.cs_opera_fiso, 'S', I3.cg_razon_social, 'N', (decode (I.cs_habilitado, 'N', '*', 'S', ' ')||' '||I.cg_razon_social)) as cg_razon_social,"+	//FODEA 17
					"	D.ig_numero_docto, "+
					"	TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY'), "+
					"	TO_CHAR(D.DF_FECHA_VENC"+sFechaVencPyme+",'DD/MM/YYYY'), " +
					campoNombreMoneda + ", "+
					"	D.fn_monto, "+
					"	D.fn_porc_anticipo, "+
					"	D.fn_monto_dscto, "+
					"	DS.in_tasa_aceptada, "+
					campoNombreEstatusDocto + ", "+
					"	TO_CHAR(S.df_fecha_solicitud,'DD/MM/YYYY'), "+
					"	D.ic_moneda, "+
					"	D.ic_documento, " +
					"	DS.in_importe_recibir, "+
					"	D.cs_cambio_importe, " +
          			"	D.cs_detalle, " +
					" 	D.cg_campo1, D.cg_campo2, D.cg_campo3, D.cg_campo4, D.cg_campo5, " +
					"	D.CS_DSCTO_ESPECIAL, "+
					"	D.ic_estatus_docto, "+
					" 	CASE WHEN cc1.fn_monto_pago IS NOT NULL THEN"   +
					" 		cc1.fn_monto_pago"   +
					" 	ELSE "   +
					" 		CASE WHEN cd.fn_monto_pago IS NOT NULL THEN"   +
					" 			cd.fn_monto_pago"   +
					" 		END"   +
					"	END as fn_monto_pago," +
					"	DS.cc_acuse,"+
					"	DS.IN_IMPORTE_RECIBIR - DS.fn_importe_recibir_benef NETO_RECIBIR, " +
					"	I2.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO, " +
					"	D.FN_PORC_BENEFICIARIO, " +
					"	DS.fn_importe_recibir_benef  RECIBIR_BENEFICIARIO, " +
					" 	CASE WHEN cc1.fn_porc_docto_aplicado IS NOT NULL THEN " +
					" 		cc1.fn_porc_docto_aplicado " +
					" 	ELSE " +
					" 		CASE WHEN cd.fn_porc_docto_aplicado IS NOT NULL THEN " +
					" 			cd.fn_porc_docto_aplicado " +
					" 		END " +
					" 	END as porcDoctoAplicado, " +
					plazo  + 
					"	DS.in_importe_interes, "+
					"   I.ig_tipo_piso, DS.fn_remanente, "+ //foda 015-2005
					"	D.CT_REFERENCIA, " +
					" TO_CHAR (D.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, D.CG_TIPO_COMPRA, D.CG_CLAVE_PRESUPUESTARIA, D.CG_PERIODO, " +
					" tf.cg_nombre as TIPO_FACTORAJE, "+mandantecamp+
					" TO_CHAR(ds.df_programacion, 'dd/mm/yyyy') AS fecha_registro_operacion, "+//FODEA 005 - 2009 ACF
					" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND d.ic_docto_asociado IS NOT NULL THEN 'true' ELSE 'false' END AS existeDocumentoAsociado, " +
					" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM com_documento WHERE ic_docto_asociado = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredSimpleApl, " +
					" CASE WHEN d.cs_dscto_especial = 'C' AND d.ic_estatus_docto in (3,4,11) AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_NOTA_CREDITO = d.ic_documento) THEN 'true' ELSE 'false' END AS existeRefEnComrelNotaDocto, " +
					" CASE WHEN d.cs_dscto_especial <> 'C' AND EXISTS (SELECT 1 FROM COMREL_NOTA_DOCTO WHERE IC_DOCUMENTO = d.ic_documento) THEN 'true' ELSE 'false' END AS esDocConNotaDeCredMultApl, " +
					"	'ConsDoctosPymeDE::getDocumentSummaryQueryForIds'"+
					" FROM comcat_epo E, "+
					"	comcat_pyme P, "+
					"	comcat_if I, "+
					"	comcat_if I2, " +
					"	comcat_if I3, " +	//FODEA 17
					"	com_documento D, " +
					"	comcat_moneda M, "+
					"	com_docto_seleccionado DS, "+
					"	comcat_estatus_docto ED, "+
					"	com_solicitud S, " +
					"	(select cc.ic_documento, cc.fn_porc_docto_aplicado , " +
					"		sum(cc.fn_monto_pago) as fn_monto_pago " +
					"	from com_cobro_credito cc, com_documento d " +
					"	where cc.ic_documento = d.ic_documento " +
					"		and d.ic_pyme = ?" + 
					"	group by cc.ic_documento, cc.fn_porc_docto_aplicado) cc1, " +
					" 	fop_cobro_disposicion cd, " +
					"  comcat_tipo_factoraje tf " +mandantetabs +
					( tablaPlazo != null?tablaPlazo:"" ) +
					" WHERE D.ic_epo = E.ic_epo " +
					"	AND D.ic_pyme = P.ic_pyme " +
					"	AND D.ic_moneda = M.ic_moneda " +
					"	AND D.ic_documento = DS.ic_documento " +
					"	AND D.ic_estatus_docto = ED.ic_estatus_docto " +
					"	AND D.ic_documento = S.ic_documento(+) " +
					"	AND D.ic_if = I.ic_if(+)"+
					"	AND DS.ic_if = I3.ic_if(+)"+	//FODEA 17
					"	AND D.ic_documento = CC1.ic_documento(+) " +
					"	AND D.ic_documento = CD.ic_documento(+) " +
					"	AND D.ic_beneficiario = I2.ic_if (+) "+
					"  AND D.cs_dscto_especial = tf.CC_TIPO_FACTORAJE"+mandantecond+
					"	AND D.ic_pyme = ? " +
					(condicionPlazo != null?condicionPlazo:"" )
				);
			conditions.add(iNoCliente);
			conditions.add(iNoCliente);
			if(!noEpo.equals("")){
				qrySentencia.append(" and D.ic_epo = ? ");
				conditions.add(noEpo);
			}
			if(!noIf.equals("")){
				qrySentencia.append(" and D.ic_if = ? ");
				conditions.add(noIf);
			}
			if (!noDocto.equals("")){
				qrySentencia.append(" and D.ig_numero_docto = ? ");
				conditions.add(noDocto);
			}
			if ((!dfFechaDoctode.equals("")) && (!dfFechaDoctoa.equals(""))){
				qrySentencia.append(" and D.DF_FECHA_DOCTO between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(dfFechaDoctode);
				conditions.add(dfFechaDoctoa);
			}
			if(!tipoFactoraje.equals("")){
				qrySentencia.append(" and D.CS_DSCTO_ESPECIAL=? ");
				conditions.add(tipoFactoraje);
			}
			if ((!dfFechaVencde.equals("")) && (!dfFechaVenca.equals(""))){
				qrySentencia.append(" and D.DF_FECHA_VENC"+sFechaVencPyme+" between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(dfFechaVencde);
				conditions.add(dfFechaVenca);
			}
			if (!noMoneda.equals("")){
				qrySentencia.append(" and D.ic_moneda = ? ");
				conditions.add(noMoneda);
			}
			if ((!montoDe.equals("")) && (!montoA.equals(""))){
				qrySentencia.append(" and D.fn_monto between ? and ? ");
				conditions.add(montoDe);
				conditions.add(montoA);
			}
			if (!noEstatusDocto.equals("")){
				qrySentencia.append(" and D.ic_estatus_docto = ? ");
				conditions.add(noEstatusDocto);
			}
			if (noEstatusDocto.equals("4") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") ) { //Solo cuando es Operada y, Operada Pagada, Operada Pendiente de pago y Aplicado a credito se toma encuenta la fecha aut IF.
				if ((!fechaIfde.equals("")) && (!fechaIfa.equals(""))) {
					qrySentencia.append(" and TO_DATE(TO_CHAR(S.df_fecha_solicitud, 'DD/MM/YYYY'), 'DD/MM/YYYY') between TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
					conditions.add(fechaIfde);
					conditions.add(fechaIfa);
				}
			}
			if ((!tasade.equals("") ) && (!tasaa.equals(""))){
				qrySentencia.append(" and DS.in_tasa_aceptada between ? and ? ");
				conditions.add(tasade);
				conditions.add(tasaa);
			}
			if (!campDina1.equals("")){
				qrySentencia.append(" and D.CG_CAMPO1 = ?");
				conditions.add(campDina1);
			}
			if (!campDina2.equals("")){
				qrySentencia.append(" and D.CG_CAMPO2 = ?");
				conditions.add(campDina2);
			}
		} else{
			System.out.println("ic_estatus_docto value not handled:  " + noEstatusDocto);
		}

		log.info("qrySentencia: " + qrySentencia.toString());
		log.info("conditions: " + getConditions().toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();

		String fechaEntrega = "";
		String tipoCompra = "";
		String clavePresupuestaria = "";
		String periodo = "";
		boolean bOperaFactConMandato           = false;
		boolean bOperaFactorajeVencido         = false;
		boolean bOperaFactorajeDistribuido     = false;
		boolean bOperaFactorajeNotaDeCredito   = false;
		boolean bOperaFactVencimientoInfonavit = false;
		boolean bTipoFactoraje                 = false;
		boolean ocultarDoctosAplicados         = (noEstatusDocto.equals("")  ||
																noEstatusDocto.equals("1") ||
																noEstatusDocto.equals("2") ||
																noEstatusDocto.equals("5") ||
																noEstatusDocto.equals("6") ||
																noEstatusDocto.equals("7") ||
																noEstatusDocto.equals("9") ||
																noEstatusDocto.equals("10")||
																noEstatusDocto.equals("21")||
																noEstatusDocto.equals("23")||
																noEstatusDocto.equals("28")||
																noEstatusDocto.equals("29")||
																noEstatusDocto.equals("30")||
																noEstatusDocto.equals("31"))?true:false;

		int nRow                        = 0;
		int val                         = 0;
		int numDocumentosMN             = 0;
		int numDocumentosDL             = 0;
		int coma                        = 0;
		double totalDolares             = 0;
		double totalMn                  = 0;
		double dblPorcientoActual       = 0;
		double dblMontoDescuento        = 0;
		double dblTotalDescuentoPesos   = 0;
		double dblTotalDescuentoDolares = 0;
		double dblPagosRealizados       = 0;
		double dblMontoCredito          = 0;
		double dblIntereses             = 0;
		double dblMontoPago             = 0;
		double dblImporteDepositar      = 0;
		double fnMonto                  = 0;
		double fnPorciento              = 0;
		double fnMontoDscto             = 0;
		double importeRecibir           = 0;
		double importeDepositar         = 0;
		String query                    = "";
		String nombreEpo                = "";
		String nombreIf                 = "";
		String igNumeroDocto            = "";
		String dfFechaDocto             = "";
		String dfFechaVenc              = "";
		String nombreMoneda             = "";
		String inTasaAceptada           = "";
		String estatusDocto             = "";
		String fechaSolicitud           = "";
		String icMoneda                 = "";
		String strEstatusDocto          = "";
		String icDocumento              = "";
		String cs_cambio_importe        = "";
		String cg_razon_social_IF       = "";
		String cgCampo1                 = "";
		String cgCampo2                 = "";
		String cgCampo3                 = "";
		String cgCampo4                 = "";
		String cgCampo5                 = "";
		String nombreTipoFactoraje      = "";
		String tipoFactorajeDesc        = "";
		String plazo                    = null;
		String importeInteres           = null;
		String netoRecibirPyme          = "";
		String nombreBeneficiario       = "";
		String porcBeneficiario         = "";
		String recibirBeneficiario      = "";
		String lineacd                  = "";
		String ccAcuse                  = "";
		String ct_referencia            = "";
		String fecha_registro_operacion = "";

		String rs_fn_porc_docto_aplicado                   = "";
		String numeroDocumento                             = "";
		String esNotaDeCreditoSimpleAplicada               = "";
		String esDocumentoConNotaDeCreditoSimpleAplicada   = "";
		String esNotaDeCreditoAplicada                     = "";
		String esDocumentoConNotaDeCreditoMultipleAplicada = "";

		List<String> lEncabezados = new ArrayList<String>();

		try{
			if(tipo.equals("PDF")){
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				sOperaFactConMandato           = "";
				sOperaFactVencimientoInfonavit = "";

				ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
				ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

				Hashtable alParamEPO = new Hashtable(); 
				if(!iNoEPO.equals("")) {
					alParamEPO = BeanParamDscto.getParametrosEPO(iNoEPO, 1);
					if (alParamEPO!=null) {	
						bOperaFactConMandato         = ("N".equals(alParamEPO.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
						bOperaFactorajeVencido       = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_VENCIDO").toString()))?false:true;
						bOperaFactorajeDistribuido   = ("N".equals(alParamEPO.get("PUB_EPO_FACTORAJE_DISTRIBUIDO").toString()))?false:true;
						bOperaFactorajeNotaDeCredito = ("N".equals(alParamEPO.get("OPERA_NOTAS_CRED").toString()))?false:true;
						bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;
					}
				}

				bTipoFactoraje = (bOperaFactorajeVencido || bOperaFactorajeDistribuido || bOperaFactorajeNotaDeCredito || bOperaFactConMandato || bOperaFactVencimientoInfonavit)?true:false;
				sOperaFactConMandato = (bOperaFactConMandato == true)?"S":"N";
				sOperaFactVencimientoInfonavit = (bOperaFactVencimientoInfonavit == true)?"S":"N";
				double dblPorciento = 0, dblPorcientoDL = 0;
				if (!noEpo.equals("")) {
					Registros reg = new Registros();
					reg = BeanParamDscto.getTipoAforoPantalla(noEpo);
					while(reg.next()){
						dblPorciento = Double.parseDouble(reg.getString(1));
						dblPorcientoDL = Double.parseDouble(reg.getString(2));
					}
				}

				String banderaTablaDoctos = "";
				boolean mostrarOpcionPignorado=false;
				Registros regPorcentaje = null;
				Registros regCamposAdicionales = null;
				Registros regCamposDetalle = null;
				int totalcd=0;
				if(!noEpo.equals("")){
					mostrarOpcionPignorado = BeanParamDscto.mostrarOpcionPignorado(noEpo, iNoCliente);
					String sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(noEpo);
					if(BeanSeleccionDocumento.bgetExisteParamDocs(noEpo)){
						banderaTablaDoctos = "1";
					}
					totalcd = BeanSeleccionDocumento.getTotalCamposAdicionales(iNoCliente);
					if(totalcd >=1) {
						regCamposAdicionales = new Registros();
						regCamposAdicionales = BeanParamDscto.getCamposAdicionales(iNoCliente);
					}
					regCamposDetalle = new Registros();
					regCamposDetalle = BeanParamDscto.getCamposAdicionalesDetalle(noEpo);
				}

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual   = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				if (noEstatusDocto.equals("") || noEstatusDocto.equals("1") || noEstatusDocto.equals("2") || noEstatusDocto.equals("5") || noEstatusDocto.equals("6") || noEstatusDocto.equals("7") || noEstatusDocto.equals("9") || noEstatusDocto.equals("10") || noEstatusDocto.equals("21") || noEstatusDocto.equals("23")){
					boolean layOutAserca = false;
					if(noEstatusDocto.equals("1") && ic_epo_aserca.equals(noEpo)){
						layOutAserca = true;
					}
					while (rs.next()){
						nombreEpo           = rs.getString("CG_RAZON_SOCIAL");
						igNumeroDocto       = rs.getString("IG_NUMERO_DOCTO");
						dfFechaDocto        = (rs.getString(3)==null)?"":rs.getString(3);
						dfFechaVenc         = (rs.getString(4)==null)?"":rs.getString(4);
						nombreMoneda        = rs.getString("CD_NOMBRE");
						fnMonto             = Double.parseDouble((rs.getString("FN_MONTO")==null)?"0":rs.getString("FN_MONTO"));
						fnPorciento         = Double.parseDouble((rs.getString("FN_PORC_ANTICIPO")==null)?"0":rs.getString("FN_PORC_ANTICIPO"));
						strEstatusDocto     = rs.getString(8);
						fnMontoDscto        = Double.parseDouble((rs.getString("FN_MONTO_DSCTO")==null)?"0":rs.getString("FN_MONTO_DSCTO"));
						estatusDocto        = rs.getString(10);
						icMoneda            = rs.getString(11);
						icDocumento         = rs.getString(12);
						cs_cambio_importe   = rs.getString(13);
						cg_razon_social_IF  = (rs.getString(14)==null)?"":rs.getString(14).trim();
						nombreBeneficiario  = (rs.getString("NOMBRE_BENEFICIARIO")==null)?"":rs.getString("NOMBRE_BENEFICIARIO");
						porcBeneficiario    = (rs.getString("FN_PORC_BENEFICIARIO")==null)?"":rs.getString("FN_PORC_BENEFICIARIO");
						recibirBeneficiario = (rs.getString("RECIBIR_BENEFICIARIO")==null)?"":rs.getString("RECIBIR_BENEFICIARIO");
						lineacd = "";
						int cont = 1;
						if (regCamposAdicionales != null){
							while (regCamposAdicionales.next()){
								if ( cont == 1 ) {
									cgCampo1 = (rs.getString(16)==null)?"":rs.getString(16).trim(); lineacd +=","+cgCampo1;
								}
								if (cont == 2) {
									cgCampo2 = (rs.getString(17)==null)?"":rs.getString(17).trim(); lineacd +=","+cgCampo2;
								}
								if (cont == 3) {
									cgCampo3 = (rs.getString(18)==null)?"":rs.getString(18).trim(); lineacd +=","+cgCampo3;
								}
								if (cont == 4) {
									cgCampo4 = (rs.getString(19)==null)?"":rs.getString(19).trim(); lineacd +=","+cgCampo4;
								}
								if (cont == 5) {
									cgCampo5 = (rs.getString(20)==null)?"":rs.getString(20).trim(); lineacd +=","+cgCampo5;
								}
								cont ++;
							}
						}
						ct_referencia = (rs.getString("ct_referencia")==null)?"":rs.getString("ct_referencia");

						fechaEntrega            = (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
						tipoCompra              = (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
						clavePresupuestaria = (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
						periodo                 = (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");
						String smandant = "";
						if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
							smandant = (rs.getString("mandante")==null)?"":rs.getString("mandante");
						}
						if (icMoneda.equals("54")) {
							totalDolares = totalDolares + fnMonto;
							numDocumentosDL ++;
						} else if (icMoneda.equals("1")) {
							totalMn = totalMn + fnMonto;
							numDocumentosMN ++;
						}
						//Correccion de Porcentaje de Descuento
						if (strEstatusDocto.equals("2") || strEstatusDocto.equals("5") || strEstatusDocto.equals("6") || strEstatusDocto.equals("7")){
							dblMontoDescuento = (fnMonto * dblPorciento);
							dblPorcientoActual = dblPorciento * 100;
						} else {
							dblMontoDescuento = fnMontoDscto;
							dblPorcientoActual = fnPorciento;
						}
						if(bTipoFactoraje) { // Para Factoraje Vencido
							if(nombreTipoFactoraje.equals("V")||nombreTipoFactoraje.equals("D")||nombreTipoFactoraje.equals("C")||nombreTipoFactoraje.equals("I")) { //aqui
								dblMontoDescuento = fnMonto;
								dblPorcientoActual = 100;
							}
							tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE");
							nombreTipoFactoraje = tipoFactorajeDesc;
						}

						if(layOutAserca) {
							if (nRow == 0){
								lEncabezados.add("Nombre EPO");
								lEncabezados.add("Fecha Emisi�n");
								lEncabezados.add("Fecha Vencimiento");
								lEncabezados.add("Moneda");
								lEncabezados.add("Monto");
								lEncabezados.add("Porcentaje Descuento");
								lEncabezados.add("Monto Descuento");
								lEncabezados.add("Estatus");
								if (regCamposAdicionales != null){
									while (regCamposAdicionales.next()){
										lEncabezados.add(regCamposAdicionales.getString("NOMBRE_CAMPO"));
									}
								}
								if(banderaTablaDoctos.equals("1")) {
									lEncabezados.add("Fecha de Recepci�n de Bienes y Servicios");
									lEncabezados.add("Tipo Compra(procedimiento)");
									lEncabezados.add("Clasificador por Objeto del Gasto");
									lEncabezados.add("Plazo M�ximo");
								}
								pdfDoc.setLTable(lEncabezados.size(),100);
								for(int i = 0; i< lEncabezados.size(); i++){
									pdfDoc.setLCell(lEncabezados.get(i).toString(),"celda01",ComunesPDF.CENTER);
								}
								pdfDoc.setLHeaders();
								nRow++;
							}
							pdfDoc.setLCell(nombreEpo, "formas", ComunesPDF.CENTER);
							pdfDoc.setLCell(dfFechaDocto, "formas", ComunesPDF.CENTER);
							pdfDoc.setLCell(dfFechaVenc, "formas", ComunesPDF.CENTER);
							pdfDoc.setLCell(nombreMoneda, "formas", ComunesPDF.CENTER);
							pdfDoc.setLCell("$" + Comunes.formatoDecimal(fnMonto,2), "formas", ComunesPDF.CENTER);
							pdfDoc.setLCell(Comunes.formatoDecimal(dblPorcientoActual,0) + "%", "formas", ComunesPDF.CENTER);
							pdfDoc.setLCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formas", ComunesPDF.CENTER);
							pdfDoc.setLCell(cgCampo1, "formas", ComunesPDF.CENTER);
							pdfDoc.setLCell(cgCampo2, "formas", ComunesPDF.CENTER);
							pdfDoc.setLCell(cgCampo3, "formas", ComunesPDF.CENTER);
							pdfDoc.setLCell(cgCampo4, "formas", ComunesPDF.CENTER);
							pdfDoc.setLCell(cgCampo5, "formas", ComunesPDF.CENTER);
						} else{
							if (nRow == 0){
								lEncabezados.add("Nombre EPO");
								lEncabezados.add("IF");
								lEncabezados.add("Num. Documento");
								lEncabezados.add("Fecha Emisi�n");
								lEncabezados.add("Fecha Vencimiento");
								lEncabezados.add("Moneda");
								if(bOperaFactorajeVencido||bOperaFactorajeDistribuido || bOperaFactorajeNotaDeCredito || bOperaFactVencimientoInfonavit){
									lEncabezados.add("Tipo Factoraje");
								}
								lEncabezados.add("Monto");
								lEncabezados.add("Monto Descuento");
								lEncabezados.add("Estatus");
								if (regCamposAdicionales != null){
									while (regCamposAdicionales.next()){
										lEncabezados.add(regCamposAdicionales.getString("NOMBRE_CAMPO"));
									}
								}
								lEncabezados.add("Referencia");
								lEncabezados.add("Porcentaje Descuento");
								if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) { 
									lEncabezados.add("Beneficiario");
									lEncabezados.add("% Beneficiario");
									lEncabezados.add("Importe a Recibir Beneficiario");
								}
								if(!ocultarDoctosAplicados){
									lEncabezados.add("Doctos Aplicados a Nota de Credito");
								}
								if(banderaTablaDoctos.equals("1")) {
									lEncabezados.add("Fecha de Recepci�n de Bienes y Servicios");
									lEncabezados.add("Tipo Compra (procedimiento)");
									lEncabezados.add("Clasificador por Objeto del Gasto");
									lEncabezados.add("Plazo M�ximo");
								}
								if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
									lEncabezados.add("Mandante");
								}
								pdfDoc.setLTable(lEncabezados.size(),100);
								for(int i = 0; i< lEncabezados.size(); i++){
									pdfDoc.setLCell(lEncabezados.get(i).toString(),"celda01",ComunesPDF.CENTER);
								}
								pdfDoc.setLHeaders();
								nRow++;
							}
							pdfDoc.setLCell(nombreEpo, "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(cg_razon_social_IF, "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(igNumeroDocto, "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(dfFechaDocto, "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(dfFechaVenc, "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(nombreMoneda, "formasmen", ComunesPDF.CENTER);
							if(bOperaFactorajeVencido||bOperaFactorajeDistribuido || bOperaFactorajeNotaDeCredito || bOperaFactVencimientoInfonavit){
								pdfDoc.setLCell(nombreTipoFactoraje, "formasmen", ComunesPDF.CENTER);
							}
							pdfDoc.setLCell("$" + Comunes.formatoDecimal(fnMonto,2), "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell("$" + Comunes.formatoDecimal(dblMontoDescuento,2), "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(estatusDocto, "formasmen", ComunesPDF.CENTER);
							if (regCamposAdicionales != null){
								while (regCamposAdicionales.next()){
									pdfDoc.setLCell(regCamposAdicionales.getString("IC_NO_CAMPO"),"formasmen",ComunesPDF.CENTER);
								}
								}
							pdfDoc.setLCell(ct_referencia, "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(Comunes.formatoDecimal(dblPorcientoActual,0) + "%", "formasmen", ComunesPDF.CENTER);
							if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
								pdfDoc.setLCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
								pdfDoc.setLCell(Comunes.formatoDecimal(porcBeneficiario,0) + "%", "formasmen", ComunesPDF.CENTER);
								pdfDoc.setLCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
							}
							// Fodea 002 - 2010
							// Debido a que para esta condicion solo se traen doctos con estatus: 1, 2, 5, 6, 7, 9, 10, 23
							// No hay ningun detalle de Notas de Credito aplicada a multiples documentos que se pueda ver.
							if(!ocultarDoctosAplicados){
								pdfDoc.setLCell("", "formasmen", ComunesPDF.CENTER);
							}
							if(banderaTablaDoctos.equals("1")) {
								pdfDoc.setLCell(fechaEntrega, "formasmen", ComunesPDF.CENTER);
								pdfDoc.setLCell(tipoCompra, "formasmen", ComunesPDF.CENTER);
								pdfDoc.setLCell(clavePresupuestaria, "formasmen", ComunesPDF.CENTER);
								pdfDoc.setLCell(periodo, "formasmen", ComunesPDF.CENTER);
							}
							if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
								pdfDoc.setLCell(smandant, "formasmen", ComunesPDF.CENTER);
							}
						}
					} // TODO: Fin del while
				} else if (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("8") || noEstatusDocto.equals("11") || noEstatusDocto.equals("12") || noEstatusDocto.equals("16") || noEstatusDocto.equals("24") || noEstatusDocto.equals("26")){
					String ig_tipo_piso="",fn_remanente="";
					nRow = 0;
					while (rs.next()) {
						nombreEpo                = rs.getString(1);
						nombreIf                 = (rs.getString(2)==null)?"":rs.getString(2).trim();
						igNumeroDocto            = rs.getString(3);
						dfFechaDocto             = rs.getString(4);
						dfFechaVenc              = (rs.getString(5)==null)?"":rs.getString(5);
						nombreMoneda             = rs.getString(6);
						fnMonto                  = Double.parseDouble((rs.getString("FN_MONTO")==null)?"0":rs.getString("FN_MONTO"));
						fnPorciento              = Double.parseDouble((rs.getString("FN_PORC_ANTICIPO")==null)?"0":rs.getString("FN_PORC_ANTICIPO"));
						fnMontoDscto             = Double.parseDouble((rs.getString("FN_MONTO_DSCTO")==null)?"0":rs.getString("FN_MONTO_DSCTO"));
						inTasaAceptada           = (rs.getString(10)==null)?"0":rs.getString(10);
						estatusDocto             = rs.getString(11);
						fechaSolicitud           = (rs.getString(12)== null)? "" : rs.getString(12); //rsConsulta.getString(12);
						icMoneda                 = rs.getString(13);
						icDocumento              = rs.getString(14);
						importeRecibir           = Double.parseDouble((rs.getString(15)==null)?"0":rs.getString(15));
						importeDepositar         = Double.parseDouble((rs.getString(15)==null)?"0":rs.getString(15));
						cs_cambio_importe        = rs.getString(16);
						ccAcuse                  = (rs.getString("cc_acuse")==null)?"":rs.getString("cc_acuse"); //15/12/2004
						netoRecibirPyme          = (rs.getString("NETO_RECIBIR")==null)?"":rs.getString("NETO_RECIBIR");
						nombreBeneficiario       = (rs.getString("NOMBRE_BENEFICIARIO")==null)?"":rs.getString("NOMBRE_BENEFICIARIO");
						porcBeneficiario         = (rs.getString("FN_PORC_BENEFICIARIO")==null)?"":rs.getString("FN_PORC_BENEFICIARIO");
						recibirBeneficiario      = (rs.getString("RECIBIR_BENEFICIARIO")==null)?"":rs.getString("RECIBIR_BENEFICIARIO");
						plazo                    = (rs.getString("IG_PLAZO")==null)?"":rs.getString("IG_PLAZO");
						importeInteres           = (rs.getString("IN_IMPORTE_INTERES")==null)?"":rs.getString("IN_IMPORTE_INTERES");
						fechaEntrega             = (rs.getString("DF_ENTREGA")==null)?"":rs.getString("DF_ENTREGA");
						tipoCompra               = (rs.getString("CG_TIPO_COMPRA")==null)?"":rs.getString("CG_TIPO_COMPRA");
						clavePresupuestaria      = (rs.getString("CG_CLAVE_PRESUPUESTARIA")==null)?"":rs.getString("CG_CLAVE_PRESUPUESTARIA");
						periodo                  = (rs.getString("CG_PERIODO")==null)?"":rs.getString("CG_PERIODO");
						lineacd                  = "";
						ig_tipo_piso             = (rs.getString("ig_tipo_piso")==null)?"":rs.getString("ig_tipo_piso");
						fn_remanente             = (rs.getString("fn_remanente")==null)?"":rs.getString("fn_remanente");
						fecha_registro_operacion = (rs.getString("fecha_registro_operacion")==null)?"":rs.getString("fecha_registro_operacion");//FODEA 005 - 2009 ACF
						if (regCamposAdicionales != null){
							int cont = 0;
							while (regCamposAdicionales.next()){
								if ( cont == 1 ) {
									cgCampo1 = (rs.getString(16)==null)?"":rs.getString(16).trim(); lineacd +=","+cgCampo1;
								}
								if (cont == 2) {
									cgCampo2 = (rs.getString(17)==null)?"":rs.getString(17).trim(); lineacd +=","+cgCampo2;
								}
								if (cont == 3) {
									cgCampo3 = (rs.getString(18)==null)?"":rs.getString(18).trim(); lineacd +=","+cgCampo3;
								}
								if (cont == 4) {
									cgCampo4 = (rs.getString(19)==null)?"":rs.getString(19).trim(); lineacd +=","+cgCampo4;
								}
								if (cont == 5) {
									cgCampo5 = (rs.getString(20)==null)?"":rs.getString(20).trim(); lineacd +=","+cgCampo5;
								}
								cont ++;
							}
						}
						ct_referencia = (rs.getString("ct_referencia")==null)?"":rs.getString("ct_referencia");
						if (Integer.parseInt((rs.getString("IC_ESTATUS_DOCTO")==null)?"0":rs.getString("IC_ESTATUS_DOCTO"))==16) {
							rs_fn_porc_docto_aplicado  = (rs.getString("PORCDOCTOAPLICADO") == null) ? "" : rs.getString("PORCDOCTOAPLICADO");
							dblMontoPago = Double.parseDouble((rs.getString("FN_MONTO_PAGO")==null)?"0":rs.getString("FN_MONTO_PAGO"));
							dblImporteDepositar = importeRecibir - dblMontoPago;
						} else{
							dblImporteDepositar = 0;
						}
						dblMontoDescuento = (fnMonto * (fnPorciento / 100));
						nombreTipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
						// Fodea 002 - 2010
						if(!ocultarDoctosAplicados){
							esNotaDeCreditoSimpleAplicada                = ( nombreTipoFactoraje.equals("C") && (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("11")) && BeanParamDscto.existeDocumentoAsociado(icDocumento) )?"S":"N";
							esDocumentoConNotaDeCreditoSimpleAplicada    = ( !nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoSimpleAplicada(icDocumento))?"S":"N";
							esNotaDeCreditoAplicada                      = (nombreTipoFactoraje.equals("C") && (noEstatusDocto.equals("3") || noEstatusDocto.equals("4") || noEstatusDocto.equals("11")) && BeanParamDscto.existeReferenciaEnComrelNotaDocto(icDocumento))?"S":"N";
							esDocumentoConNotaDeCreditoMultipleAplicada  = (!nombreTipoFactoraje.equals("C") && BeanParamDscto.esDocumentoConNotaDeCreditoMultipleAplicada(icDocumento))?"S":"N";
							if("S".equals(esNotaDeCreditoSimpleAplicada)){
								numeroDocumento                           = BeanSeleccionDocumento.getNumeroDoctoAsociado(icDocumento); 
							}else if("S".equals(esDocumentoConNotaDeCreditoSimpleAplicada)){
								numeroDocumento                           = BeanSeleccionDocumento.getNumeroNotaCreditoAsociada(icDocumento);
							}
						}
						fnMontoDscto = dblMontoDescuento;//aqui agregado 15/12/2004
						if(nombreTipoFactoraje.equals("V")||nombreTipoFactoraje.equals("D")||nombreTipoFactoraje.equals("C")||nombreTipoFactoraje.equals("I")) {//aqui agregado 15/12/2004
							fnMontoDscto = fnMonto;
							fnPorciento = 100;
						}
						tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE");
						nombreTipoFactoraje = tipoFactorajeDesc;
						String smandant = "";
						if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
							smandant = (rs.getString("mandante")==null)?"":rs.getString("mandante");
						}
						if (nRow == 0){
							lEncabezados.add("Nombre EPO");
							lEncabezados.add("IF");
							lEncabezados.add("Num. Documento");
							lEncabezados.add("Num. Acuse");
							lEncabezados.add("Fecha Emisi�n");
							lEncabezados.add("Fecha Vencimiento");
							lEncabezados.add("Plazo");
							lEncabezados.add("Moneda");
							if(bTipoFactoraje){
								lEncabezados.add("Tipo Factoraje");
							}
							lEncabezados.add("Monto Documento");
							lEncabezados.add("Porcentaje de Dscto.");
							lEncabezados.add("Monto a Descontar");
							lEncabezados.add("Intereses del Docto.");
							lEncabezados.add("Monto a Recibir");
							lEncabezados.add("Tasa");
							lEncabezados.add("Estatus");
							lEncabezados.add("Fecha Aut. IF");
							lEncabezados.add("Importe aplicado a cr�dito");
							lEncabezados.add("Porcentaje del documento aplicado");
							lEncabezados.add("Importe a Depositar a la Pyme");
							if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
								lEncabezados.add("Neto a Recibir Pyme");
								lEncabezados.add("Beneficiario");
								lEncabezados.add("% Beneficiario");
								lEncabezados.add("Importe a Recibir Beneficiario");
							}
							if (regCamposAdicionales != null){
								while (regCamposAdicionales.next()){
									lEncabezados.add(regCamposAdicionales.getString("IC_NO_CAMPO"));
								}
							}
							lEncabezados.add("Referencia");
							if(!ocultarDoctosAplicados){
								lEncabezados.add("Doctos Aplicados a Nota de Credito");
							}
							if(banderaTablaDoctos.equals("1")) {
								lEncabezados.add("Fecha de Recepci�n de Bienes y Servicios");
								lEncabezados.add("Tipo Compra (procedimiento)");
								lEncabezados.add("Clasificador por Objeto del Gasto");
								lEncabezados.add("Plazo M�ximo");
							}
							if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje) ){
								lEncabezados.add("Mandante");
							}
							if(noEstatusDocto.equals("26")){
								lEncabezados.add("Fecha Registro Operaci�n");
							}//FODEA 005 - 2009 ACF
							pdfDoc.setLTable(lEncabezados.size(),100);
							for(int i = 0; i< lEncabezados.size(); i++){
								pdfDoc.setLCell(lEncabezados.get(i).toString(),"celda01",ComunesPDF.CENTER);
							}
							pdfDoc.setLHeaders();
							nRow++;
						}
						pdfDoc.setLCell(nombreEpo, "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell(nombreIf, "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell(igNumeroDocto, "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell(ccAcuse, "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell(dfFechaDocto, "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell(dfFechaVenc, "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell(plazo, "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell(nombreMoneda, "formasmen", ComunesPDF.CENTER);
						if(bTipoFactoraje){
							pdfDoc.setLCell(nombreTipoFactoraje, "formasmen", ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("$" + Comunes.formatoDecimal(fnMonto,2), "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell(Comunes.formatoDecimal(fnPorciento,0) + "%", "formasmen", ComunesPDF.CENTER);
						pdfDoc.setLCell("$" + Comunes.formatoDecimal(fnMontoDscto,2), "formasmen", ComunesPDF.CENTER);
						if(nombreTipoFactoraje.equals("C")){
							pdfDoc.setLCell("", "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell("", "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell("", "formasmen", ComunesPDF.CENTER);
						}else{
							pdfDoc.setLCell("$" + Comunes.formatoDecimal(importeInteres,2), "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell("$" + Comunes.formatoDecimal(importeRecibir,2), "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(Comunes.formatoDecimal(inTasaAceptada,0) + "%", "formasmen", ComunesPDF.CENTER);
						}
						pdfDoc.setLCell(estatusDocto, "formasmen", ComunesPDF.CENTER);
						if(!noEstatusDocto.equals("16")){
							pdfDoc.setLCell("", "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell("", "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell("", "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell("", "formasmen", ComunesPDF.CENTER);
						}else{
							pdfDoc.setLCell(fechaSolicitud, "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell("$" + Comunes.formatoDecimal(dblMontoPago,2), "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(Comunes.formatoDecimal(rs_fn_porc_docto_aplicado,0) + "%", "formasmen", ComunesPDF.CENTER);
							if(ig_tipo_piso.equals("1")){
								if (!fn_remanente.equals("")){
									pdfDoc.setLCell("$" + Comunes.formatoDecimal(fn_remanente,2), "formasmen", ComunesPDF.CENTER);
								}else {
									pdfDoc.setLCell("", "formasmen", ComunesPDF.CENTER);
								}
							}else{
								pdfDoc.setLCell("$" + Comunes.formatoDecimal(dblImporteDepositar,2), "formasmen", ComunesPDF.CENTER);
							}
						}
						if(bOperaFactorajeDistribuido || bOperaFactVencimientoInfonavit) {
							pdfDoc.setLCell("$" + Comunes.formatoDecimal(netoRecibirPyme,2), "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(nombreBeneficiario, "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(Comunes.formatoDecimal(porcBeneficiario,0) + "%", "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell("$" + Comunes.formatoDecimal(recibirBeneficiario,2), "formasmen", ComunesPDF.CENTER);
						}
						if (regCamposAdicionales != null){
							while (regCamposAdicionales.next()){
								pdfDoc.setLCell(regCamposAdicionales.getString("IC_NO_CAMPO"),"formasmen",ComunesPDF.CENTER);
							}
						}
						pdfDoc.setLCell(ct_referencia, "formasmen", ComunesPDF.CENTER);

						// Fodea 002 - 2010
						// Debido a que para esta condicion solo se traen doctos con estatus: 1, 2, 5, 6, 7, 9, 10, 23
						// No hay ningun detalle de Notas de Credito aplicada a multiples documentos que se pueda ver.
						if(!ocultarDoctosAplicados){
							if("S".equals(esNotaDeCreditoSimpleAplicada) || "S".equals(esDocumentoConNotaDeCreditoSimpleAplicada) ){
								pdfDoc.setLCell(numeroDocumento, "formasmen", ComunesPDF.CENTER);
							} else if("S".equals(esNotaDeCreditoAplicada) || "S".equals(esDocumentoConNotaDeCreditoMultipleAplicada)){
								pdfDoc.setLCell("Si", "formasmen", ComunesPDF.CENTER);
							} else{
								pdfDoc.setLCell("", "formasmen", ComunesPDF.CENTER);
							}
						}
						if(banderaTablaDoctos.equals("1")) {
							pdfDoc.setLCell(fechaEntrega, "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(tipoCompra, "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(clavePresupuestaria, "formasmen", ComunesPDF.CENTER);
							pdfDoc.setLCell(periodo, "formasmen", ComunesPDF.CENTER);
						}
						if("M".equals(tipoFactoraje) || "".equals(tipoFactoraje)){
							pdfDoc.setLCell(smandant, "formasmen", ComunesPDF.CENTER);
						}
						if (noEstatusDocto.equals("26")){
							pdfDoc.setLCell(fecha_registro_operacion, "formasmen", ComunesPDF.CENTER);
						}//FODEA 005 - 2009 ACF
					} // TODO: Fin del while
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} // TODO: Fin del if(PDF)

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo. ", e);
		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}

/*******************************************************************************
 *                       GETTERS AND SETTERS                                   *
 *******************************************************************************/
	public List getConditions(){
		return conditions; 
	}

	public String getNoEpo() {
		return noEpo;
	}

	public void setNoEpo(String noEpo) {
		this.noEpo = noEpo;
	}

	public String getNoIf() {
		return noIf;
	}

	public void setNoIf(String noIf) {
		this.noIf = noIf;
	}

	public String getNoDocto() {
		return noDocto;
	}

	public void setNoDocto(String noDocto) {
		this.noDocto = noDocto;
	}

	public String getDfFechaDoctode() {
		return dfFechaDoctode;
	}

	public void setDfFechaDoctode(String dfFechaDoctode) {
		this.dfFechaDoctode = dfFechaDoctode;
	}

	public String getDfFechaDoctoa() {
		return dfFechaDoctoa;
	}

	public void setDfFechaDoctoa(String dfFechaDoctoa) {
		this.dfFechaDoctoa = dfFechaDoctoa;
	}

	public String getCampDina1() {
		return campDina1;
	}

	public void setCampDina1(String campDina1) {
		this.campDina1 = campDina1;
	}

	public String getCampDina2() {
		return campDina2;
	}

	public void setCampDina2(String campDina2) {
		this.campDina2 = campDina2;
	}

	public String getOrdIf() {
		return ordIf;
	}

	public void setOrdIf(String ordIf) {
		this.ordIf = ordIf;
	}

	public String getOrdNoDocto() {
		return ordNoDocto;
	}

	public void setOrdNoDocto(String ordNoDocto) {
		this.ordNoDocto = ordNoDocto;
	}

	public String getOrdEpo() {
		return ordEpo;
	}

	public void setOrdEpo(String ordEpo) {
		this.ordEpo = ordEpo;
	}

	public String getOrdFecVenc() {
		return ordFecVenc;
	}

	public void setOrdFecVenc(String ordFecVenc) {
		this.ordFecVenc = ordFecVenc;
	}

	public String getDfFechaVencde() {
		return dfFechaVencde;
	}

	public void setDfFechaVencde(String dfFechaVencde) {
		this.dfFechaVencde = dfFechaVencde;
	}

	public String getDfFechaVenca() {
		return dfFechaVenca;
	}

	public void setDfFechaVenca(String dfFechaVenca) {
		this.dfFechaVenca = dfFechaVenca;
	}

	public String getNoMoneda() {
		return noMoneda;
	}

	public void setNoMoneda(String noMoneda) {
		this.noMoneda = noMoneda;
	}

	public String getMontoDe() {
		return montoDe;
	}

	public void setMontoDe(String montoDe) {
		this.montoDe = montoDe;
	}

	public String getMontoA() {
		return montoA;
	}

	public void setMontoA(String montoA) {
		this.montoA = montoA;
	}

	public String getNoEstatusDocto() {
		return noEstatusDocto;
	}

	public void setNoEstatusDocto(String noEstatusDocto) {
		this.noEstatusDocto = noEstatusDocto;
	}

	public String getFechaIfde() {
		return fechaIfde;
	}

	public void setFechaIfde(String fechaIfde) {
		this.fechaIfde = fechaIfde;
	}

	public String getFechaIfa() {
		return fechaIfa;
	}

	public void setFechaIfa(String fechaIfa) {
		this.fechaIfa = fechaIfa;
	}

	public String getTasade() {
		return tasade;
	}

	public void setTasade(String tasade) {
		this.tasade = tasade;
	}

	public String getTasaa() {
		return tasaa;
	}

	public void setTasaa(String tasaa) {
		this.tasaa = tasaa;
	}

	public String getTipoFactoraje() {
		return tipoFactoraje;
	}

	public void setTipoFactoraje(String tipoFactoraje) {
		this.tipoFactoraje = tipoFactoraje;
	}

	public String getINoCliente() {
		return iNoCliente;
	}

	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}

	public String getSFechaVencPyme() {
		return sFechaVencPyme;
	}

	public void setSFechaVencPyme(String sFechaVencPyme) {
		this.sFechaVencPyme = sFechaVencPyme;
	}

	public String getSOperaFactConMandato() {
		return sOperaFactConMandato;
	}

	public void setSOperaFactConMandato(String sOperaFactConMandato) {
		this.sOperaFactConMandato = sOperaFactConMandato;
	}

	public String getMandant() {
		return mandant;
	}

	public void setMandant(String mandant) {
		this.mandant = mandant;
	}

	public String getIdiomaUsuario() {
		return idiomaUsuario;
	}

	public void setIdiomaUsuario(String idiomaUsuario) {
		this.idiomaUsuario = idiomaUsuario;
	}

	public String getSOperaFactVencimientoInfonavit() {
		return sOperaFactVencimientoInfonavit;
	}

	public void setSOperaFactVencimientoInfonavit(String sOperaFactVencimientoInfonavit) {
		this.sOperaFactVencimientoInfonavit = sOperaFactVencimientoInfonavit;
	}
	
	public String getINoEPO() {
		return iNoEPO;
	}

	public void setINoEPO(String iNoEPO) {
		this.iNoEPO = iNoEPO;
	}

	public String getIc_epo_aserc() {
		return ic_epo_aserca;
	}

	public void setIc_epo_aserc(String ic_epo_aserca) {
		this.ic_epo_aserca = ic_epo_aserca;
	}
}