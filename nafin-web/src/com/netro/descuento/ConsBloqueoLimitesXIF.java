package com.netro.descuento;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Bitacora;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsBloqueoLimitesXIF implements IQueryGeneratorRegExtJS {//DBM

	
///Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsBloqueoLimitesXIF.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_epo;
	private String ic_if;
	
	public ConsBloqueoLimitesXIF() { }
	
  
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery() { 
		return "";
	}
	
	/**
	* Obtiene el query para la CONSULTA
	* @return Cadena con la consulta de SQL
	*/
	public String getDocumentQuery() { 
		log.info("getDocumentQuery(E) :::...");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
		
		log.info("getDocumentQuery :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	* Obtiene el query para obtener la consulta
	* @return Cadena con la consulta de SQL, para obtener la consulta
	*/
	public String getDocumentSummaryQueryForIds(List pageIds) 	{		
		log.info("getDocumentSummaryQueryForIds(E) :::...");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
	
		log.info("getDocumentSummaryQueryForIds :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		
		log.info("getDocumentQueryFile(E) :::...");
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();	
	
	
		qrySentencia.append( " SELECT  i.ic_if as  IC_IF ,  "+
									" 	ie.ic_epo as  IC_EPO ,  "+
									" i.CG_RAZON_SOCIAL  as  NOMBRE_IF,  "+
									" ie.in_limite_epo as MONTO_LIMITE_MN,  "+
									" CASE "+
									" WHEN ie.fn_limite_utilizado <> 0  AND ie.in_limite_epo <> 0 "+
									" THEN "+
									" ROUND ((  (  ie.fn_limite_utilizado / ie.in_limite_epo   )  * 100 ),  5 ) "+
									"  ELSE 0  "+
									"  END as PORCENTAJE_UTL_MN,     "+
									" (nvl(ie.in_limite_epo, 0) - nvl(ie.fn_limite_utilizado, 0))  as MONTO_DISP_MN, "+
									" ie.in_limite_epo_dl as MONTO_LIMITE_DL, "+
									" CASE "+
									" WHEN ie.fn_limite_utilizado_dl <> 0  AND ie.in_limite_epo_dl <> 0 "+
									" THEN  "+
									" ROUND ((  (  ie.fn_limite_utilizado_dl / ie.in_limite_epo_dl   )  * 100 ),  5 )  "+
									" ELSE 0  "+
									" END as PORCENTAJE_UTL_DL,  "+
									 " (nvl(ie.in_limite_epo_dl, 0) - nvl(ie.fn_limite_utilizado_dl, 0))  as MONTO_DISP_DL,   "+
									"  nvl(ie.fn_monto_comprometido, 0) as MONTO_COMPROMETIDO_MN,  "+
									" nvl(ie.fn_monto_comprometido_dl, 0) as MONTO_COMPROMETIDO_DL,  "+
									" NVL (ie.in_limite_epo, 0) - (  NVL (ie.fn_limite_utilizado, 0)  + NVL (ie.fn_monto_comprometido, 0) )  as MONTO_DIS_DES_MN,  "+
									" NVL (ie.in_limite_epo_dl, 0) - (  NVL (ie.fn_limite_utilizado_dl, 0)  + NVL (ie.fn_monto_comprometido_dl, 0) )  as MONTO_DIS_DES_DL,  "+
									" TO_CHAR (ie.df_venc_linea, 'dd/mm/yyyy')  as FECHA_VEN_LINEA,  "+
									" TO_CHAR (ie.df_cambio_admon, 'dd/mm/yyyy') as FECHA_CAMB_ADM,   "+
									" decode (ie.cs_fecha_limite, 'S', 'SI', 'NO')  as VALIDA_FECHA_LIM,   "+
									" ie.CS_ACTIVA_LIMITE  as ESTATUS,   "+									
									" decode(ie.CS_ACTIVA_LIMITE , 'S', 'checked' ,'N' , '') as AUX_ESTATUS,   "+  
									" decode(ie.CS_ACTIVA_LIMITE , 'S', 'Activa' ,'N' , 'Inactiva') as DESCRIPCION_ESTATUS,   "+  
									" TO_CHAR (ie.DF_BLOQ_DES, 'dd/mm/yyyy')  as FECHA_BLOQ_DESB,   "+
									" ie.CG_USUARIO_BLOQ_DES as USUARIO_BLOQ_DESB,   "+
									" ie.CG_DEPENDENCIA_BLOQ_DES as DEPENDENCIA_BLOQ_DESB,   "+  
									" 'N' as SELECCIONADOS	,  "+
									"  ie.IC_IF_EPO_BLOQ_DES   as   IC_IF_EPO_BLOQ_DES  "  +     
									
									" FROM   "+
									" comcat_if i,  "+
									" COMREL_IF_EPO  ie ,  "+
									" comrel_producto_epo cpe " +
									" where i.ic_if = ie.ic_if  "+
									" AND (ie.in_limite_epo <> 0 OR ie.in_limite_epo_dl <> 0)  "+
									" AND UPPER (ie.cs_vobo_nafin) = 'S'  "+
									" AND cpe.ic_epo = ie.ic_epo  "+
									" AND cpe.ic_producto_nafin = 1  "+
									"  AND cpe.cs_limite_pyme != 'S'  "+						 
									" and  ie.ic_epo = ?  ") ; 	
									
									conditions.add(ic_epo);
									
			if (!"".equals(ic_if) )  {
				qrySentencia.append(" AND ie.ic_if  = ? ") ;
				conditions.add(ic_if);
			}
			
			qrySentencia.append(" order by i.CG_RAZON_SOCIAL  ") ; 	
			
		log.info("getDocumentQueryFile :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	} 
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos con PAGINADOR*/		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo)	{	
		String nombreArchivo = "";
		try {	
	
		}catch(Throwable e){
			throw new AppException("Error al generar el archivo",e);
		}finally{
			try{
			}catch(Exception e){}
		}
		return  nombreArchivo;
	}
	
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) SIN paginador*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet reg, String path, String tipo) {

		String nombreArchivo = "";
		
		
		return  nombreArchivo;
	}
	
		
	
	
	public void bactualizaBloqueo( String ic_reg_ifs[],  String cs_activacion[] , String claveEPO, String iNoUsuario, String csAcuseRecibo,String usuarioBloqueo,String dependenciaBloqueo	) {
			
		log.info("bactualizaBloqueo(E)");
		AccesoDB con =new AccesoDB();
		boolean bOkActualiza = true;
		StringBuffer	strSQL = new StringBuffer();
		List	lVarBind	= new ArrayList();
		PreparedStatement ps	= null;
		
		try{
		
			con.conexionDB();
			
			for(int i=0;i<ic_reg_ifs.length;i++){	
			
				String ic_if  = ic_reg_ifs[i].toString();
				String cs_acti_lim  = cs_activacion[i].toString();
					
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Consulta para determinar si hubo cambio de activacion					
				strSQL = new StringBuffer();
					strSQL.append(	" SELECT ie.cs_activa_limite, n.ic_nafin_electronico as neIF " +
					" FROM comrel_if_epo ie, comrel_nafin n " +
					" 	WHERE ie.ic_epo = n.ic_epo_pyme_if " +
					" and ie.ic_epo = ? "+
					" and ie.ic_if = ? " +
					" AND n.cg_tipo = ? ");
					
				lVarBind		= new ArrayList();
				lVarBind.add(claveEPO);	
				lVarBind.add(ic_if);	
				lVarBind.add("E");	
				
				log.debug("strSQL "+strSQL.toString());							
				log.debug("lVarBind "+lVarBind);
				
				PreparedStatement psCons = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ResultSet rsCons = psCons.executeQuery();
				rsCons.next();
				String limiteActivado = (rsCons.getString("cs_activa_limite") == null ? "N" : rsCons.getString("cs_activa_limite"));
				String neIF = rsCons.getString("neIF");
				rsCons.close();
				psCons.close();
				
				log.debug("limiteActivado ====="+limiteActivado);
				log.debug("cs_acti_lim ======="+cs_acti_lim);
					
				if ( !limiteActivado.equals(cs_acti_lim )) {  //Si hay cambio de activacion
						Bitacora.grabarEnBitacora(con, "BLOLIMITEXIF", "E",
							neIF, iNoUsuario,
						" cs_activa_limite=" + limiteActivado + "\n" +
						" ic_if="+ ic_if + "\n" +
						" ic_epo=" + claveEPO, 
						" cs_activa_limite=" + cs_acti_lim + "\n" +
						" ic_if=" + ic_if + "\n" +
						" ic_epo=" + claveEPO + "\n" + 
						" ig_recibo=" + csAcuseRecibo);
				}
				
				
				strSQL = new StringBuffer();
				strSQL.append( " Update  COMREL_IF_EPO   "+
						" SET  CS_ACTIVA_LIMITE  = ? ,  "+
						" DF_BLOQ_DES = Sysdate , "+
						" CG_USUARIO_BLOQ_DES   =  ? ,  "+
						" CG_DEPENDENCIA_BLOQ_DES   =  ? ,  "+
						" IC_IF_EPO_BLOQ_DES = ?   "+
						" where ic_if  =  ?"+
						" and  ic_epo = ? ");
						
				lVarBind		= new ArrayList();
				lVarBind.add(cs_acti_lim);
				lVarBind.add(usuarioBloqueo);
				lVarBind.add(dependenciaBloqueo);
				lVarBind.add(claveEPO); // la EPO Activa o Inactiva				
				lVarBind.add(ic_if);
				lVarBind.add(claveEPO);	
				
				log.debug("strSQL "+strSQL.toString());							
				log.debug("lVarBind "+lVarBind);
				
				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();	
				
			
			}
			
		} catch (Exception e) {
			bOkActualiza = false;
			throw new AppException("Error al actualizar bactualizaBloqueo ", e);
		} finally {
			log.info("bactualizaBloqueo(S)");
			if (con.hayConexionAbierta())  {
				con.terminaTransaccion(bOkActualiza);
				con.cierraConexionDB();
			}
		}
	}
	
	
		/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


}