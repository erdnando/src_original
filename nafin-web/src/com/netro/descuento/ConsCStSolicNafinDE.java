package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCStSolicNafinDE  implements IQueryGenerator, IQueryGeneratorRegExtJS  {

  public ConsCStSolicNafinDE()  {  }
	public String getAggregateCalculationQuery(HttpServletRequest request){
  // Este metodo generara el query de los totales 
    StringBuffer query = new StringBuffer("");
    StringBuffer condicion = new StringBuffer("");
    String criterioOrdenamiento=" group by d.ic_moneda,'ConsCStSolicNafinDE::getAggregateCalculationQuery()'  order by d.ic_moneda ";
    // Parametros de la pagina
    String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
    String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
    String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
    String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
    String ic_folio = (request.getParameter("ic_folio") == null)?"":request.getParameter("ic_folio");
    String ig_plazoMin = (request.getParameter("ig_plazoMin") == null)?"":request.getParameter("ig_plazoMin");
    String ig_plazoMax = (request.getParameter("ig_plazoMax") == null)?"":request.getParameter("ig_plazoMax");
    String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
    String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus") == null)?"":request.getParameter("ic_cambio_estatus");
    String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
    String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
    String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") == null)?"":request.getParameter("df_fecha_solicitudMin");
    String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") == null)?"":request.getParameter("df_fecha_solicitudMax");
    String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null)?"":request.getParameter("dc_fecha_cambioMin");
    String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null)?"":request.getParameter("dc_fecha_cambioMax");
	 String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo") == null) ? "" : request.getParameter("ic_banco_fondeo");
    // Generacion del query
		if (!ic_if.equals(""))
				condicion.append(" and ds.ic_if = "+ic_if);
		if (!ic_folio.equals(""))
				condicion.append(" and s.ic_folio = "+ic_folio);
		if (!ic_epo.equals(""))
				condicion.append(" and d.ic_epo = "+ic_epo);
		if (!ic_pyme.equals(""))
				condicion.append(" and d.ic_pyme = "+ic_pyme);
		if (!ic_moneda.equals(""))
				condicion.append(" and d.ic_moneda = "+ic_moneda);
		if (!ic_cambio_estatus.equals(""))
				condicion.append(" and ce.ic_cambio_estatus = "+ic_cambio_estatus);
		if(!dc_fecha_cambioMin.equals("")) {
				if(!dc_fecha_cambioMax.equals(""))
					condicion.append(" and TO_DATE(TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and TO_DATE('"+dc_fecha_cambioMax+"','dd/mm/yyyy') ");
		else
					condicion.append(" and TO_DATE(TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') ");
		}
		if(!ig_plazoMin.equals("")) {
				if(!ig_plazoMax.equals(""))
					condicion.append(" and s.ig_plazo between "+ig_plazoMin+" and "+ig_plazoMax);
				else
					condicion.append(" and s.ig_plazo = "+ig_plazoMin);
		}
		if(!fn_montoMin.equals(""))
		{
				if(!fn_montoMax.equals(""))
					condicion.append(" and d.fn_monto between "+fn_montoMin+" and "+fn_montoMax);
				else
					condicion.append(" and d.fn_monto = "+fn_montoMin);
		}
		if(!df_fecha_solicitudMin.equals(""))
		{
				if(!df_fecha_solicitudMax.equals(""))
					condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE('"+df_fecha_solicitudMin+"','dd/mm/yyyy') and TO_DATE('"+df_fecha_solicitudMax+"','dd/mm/yyyy') ");
				else
					condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('"+df_fecha_solicitudMin+"','dd/mm/yyyy') ");
		}
		query.append(" select d.ic_moneda, count(*) TOTAL_DOC ,sum(nvl(d.fn_monto,0)) MONTO_DOC, sum(nvl(d.fn_monto_dscto,0)) MONTO_DESC , 'ConsCStSolicNafinDE::getAggregateCalculationQuery()' ORIGENQRY "   +
                "  from com_solicitud s"   +
                "  , com_docto_seleccionado ds"   +
                "  , com_documento d"   +
                "  , comhis_cambio_estatus ce"   +
                "  , comrel_pyme_epo pe, comrel_producto_epo prodepo "   +
                "  , comcat_epo e "   +//FODEA 007 - 2009
                "  where"   +
                "  s.ic_documento=ds.ic_documento"   +
                "  and ds.ic_documento=d.ic_documento"   +
                "  and d.ic_documento = ce.ic_documento"   +
                "  and d.ic_epo = prodepo.ic_epo "   +
				"  and prodepo.ic_producto_nafin = 1 "   +
				//"  and (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) "   +
                "  and d.ic_epo = pe.ic_epo "   +
                "  and d.ic_pyme = pe.ic_pyme"   +
					 //(!ic_banco_fondeo.equals("")?" and s.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
                " and d.ic_epo = e.ic_epo"+//FODEA 007 - 2009
           (!ic_banco_fondeo.equals("")?" and e.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +//FODEA 007 - 2009
                "  and ce.ic_cambio_estatus = 7 "  +condicion.toString());
		query.append(criterioOrdenamiento);
    return query.toString();
	 
  }

 	public String getDocumentQuery(HttpServletRequest request){
  // Genera el query que devuelve las llaves primarias de la consulta
    StringBuffer query = new StringBuffer("");
    StringBuffer condicion = new StringBuffer("");
    String criterioOrdenamiento=" ORDER BY DS.IC_DOCUMENTO,CE.DC_FECHA_CAMBIO ";
    // Parametros de la pagina
    String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
    String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
    String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
    String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
    String ic_folio = (request.getParameter("ic_folio") == null)?"":request.getParameter("ic_folio");
    String ig_plazoMin = (request.getParameter("ig_plazoMin") == null)?"":request.getParameter("ig_plazoMin");
    String ig_plazoMax = (request.getParameter("ig_plazoMax") == null)?"":request.getParameter("ig_plazoMax");
    String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
    String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus") == null)?"":request.getParameter("ic_cambio_estatus");
    String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
    String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
    String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") == null)?"":request.getParameter("df_fecha_solicitudMin");
    String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") == null)?"":request.getParameter("df_fecha_solicitudMax");
    String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null)?"":request.getParameter("dc_fecha_cambioMin");
    String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null)?"":request.getParameter("dc_fecha_cambioMax");
	 String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo") == null) ? "" : request.getParameter("ic_banco_fondeo");
    // Generacion del query
		if (!ic_if.equals(""))
				condicion.append(" and ds.ic_if = "+ic_if);
		if (!ic_folio.equals(""))
				condicion.append(" and s.ic_folio = "+ic_folio);
		if (!ic_epo.equals(""))
				condicion.append(" and d.ic_epo = "+ic_epo);
		if (!ic_pyme.equals(""))
				condicion.append(" and d.ic_pyme = "+ic_pyme);
		if (!ic_moneda.equals(""))
				condicion.append(" and d.ic_moneda = "+ic_moneda);
		if (!ic_cambio_estatus.equals(""))
				condicion.append(" and ce.ic_cambio_estatus = "+ic_cambio_estatus);
		if(!dc_fecha_cambioMin.equals("")) {
				if(!dc_fecha_cambioMax.equals(""))
					condicion.append(" and TO_DATE(TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and TO_DATE('"+dc_fecha_cambioMax+"','dd/mm/yyyy') ");
		else
					condicion.append(" and TO_DATE(TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') ");
		}
		if(!ig_plazoMin.equals("")) {
				if(!ig_plazoMax.equals(""))
					condicion.append(" and s.ig_plazo between "+ig_plazoMin+" and "+ig_plazoMax);
				else
					condicion.append(" and s.ig_plazo = "+ig_plazoMin);
		}
		if(!fn_montoMin.equals(""))
		{
				if(!fn_montoMax.equals(""))
					condicion.append(" and d.fn_monto between "+fn_montoMin+" and "+fn_montoMax);
				else
					condicion.append(" and d.fn_monto = "+fn_montoMin);
		}
		if(!df_fecha_solicitudMin.equals(""))
		{
				if(!df_fecha_solicitudMax.equals(""))
					condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE('"+df_fecha_solicitudMin+"','dd/mm/yyyy') and TO_DATE('"+df_fecha_solicitudMax+"','dd/mm/yyyy') ");
				else
					condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('"+df_fecha_solicitudMin+"','dd/mm/yyyy') ");
		}
			query.append(" select DS.IC_DOCUMENTO||to_char(CE.DC_FECHA_CAMBIO,'ddmmyyyyhhmiss') "   +
                    "  from com_solicitud s"   +
                    "  , com_docto_seleccionado ds"   +
                    "  , com_documento d"   +
                    "  , comhis_cambio_estatus ce"   +
                    "  , comrel_pyme_epo pe, comrel_producto_epo prodepo "   +
                    "  , comcat_epo e "   +//FODEA 007 - 2009
                    "  where"   +
                    "  s.ic_documento=ds.ic_documento"   +
                    "  and ds.ic_documento=d.ic_documento"   +
                    "  and d.ic_documento = ce.ic_documento"   +
							"  and d.ic_epo = prodepo.ic_epo "   +
							"  and prodepo.ic_producto_nafin = 1 "   +
							"  and (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) "   +
                    "  and d.ic_epo = pe.ic_epo "   +
                    "  and d.ic_pyme = pe.ic_pyme"   +
						  //(!ic_banco_fondeo.equals("")?" and s.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
                    " and d.ic_epo = e.ic_epo"+//FODEA 007 - 2009
                    (!ic_banco_fondeo.equals("")?" and e.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +//FODEA 007 - 2009
                    "  and ce.ic_cambio_estatus = 7"  +condicion.toString()); 
			query.append(criterioOrdenamiento);
    return query.toString();
  }

	public String getDocumentQueryFile(HttpServletRequest request){
  // Genera el query que devuelve toda la informacion de la consulta
    StringBuffer query = new StringBuffer("");
    StringBuffer condicion = new StringBuffer("");
    // Parametros de la pagina
    String operacion = (request.getParameter("operacion") == null)?"":request.getParameter("operacion");
    String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
    String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
    String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
    String ic_folio = (request.getParameter("ic_folio") == null)?"":request.getParameter("ic_folio");
    String ig_plazoMin = (request.getParameter("ig_plazoMin") == null)?"":request.getParameter("ig_plazoMin");
    String ig_plazoMax = (request.getParameter("ig_plazoMax") == null)?"":request.getParameter("ig_plazoMax");
    String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
    String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus") == null)?"":request.getParameter("ic_cambio_estatus");
    String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
    String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
    String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") == null)?"":request.getParameter("df_fecha_solicitudMin");
    String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") == null)?"":request.getParameter("df_fecha_solicitudMax");
    String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null)?"":request.getParameter("dc_fecha_cambioMin");
    String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null)?"":request.getParameter("dc_fecha_cambioMax");
	 String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo") == null) ? "" : request.getParameter("ic_banco_fondeo");
    // Generacion del query
		if (!ic_if.equals(""))
				condicion.append(" and ds.ic_if = "+ic_if);
		if (!ic_folio.equals(""))
				condicion.append(" and s.ic_folio = "+ic_folio);
		if (!ic_epo.equals(""))
				condicion.append(" and d.ic_epo = "+ic_epo);
		if (!ic_pyme.equals(""))
				condicion.append(" and d.ic_pyme = "+ic_pyme);
		if (!ic_moneda.equals(""))
				condicion.append(" and d.ic_moneda = "+ic_moneda);
		if (!ic_cambio_estatus.equals(""))
				condicion.append(" and ce.ic_cambio_estatus = "+ic_cambio_estatus);
		if(!dc_fecha_cambioMin.equals("")) {
				if(!dc_fecha_cambioMax.equals(""))
					condicion.append(" and TO_DATE(TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and TO_DATE('"+dc_fecha_cambioMax+"','dd/mm/yyyy') ");
		else
					condicion.append(" and TO_DATE(TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') ");
		}
		if(!ig_plazoMin.equals("")) {
				if(!ig_plazoMax.equals(""))
					condicion.append(" and s.ig_plazo between "+ig_plazoMin+" and "+ig_plazoMax);
				else
					condicion.append(" and s.ig_plazo = "+ig_plazoMin);
		}
		if(!fn_montoMin.equals(""))
		{
				if(!fn_montoMax.equals(""))
					condicion.append(" and d.fn_monto between "+fn_montoMin+" and "+fn_montoMax);
				else
					condicion.append(" and d.fn_monto = "+fn_montoMin);
		}
		if(!df_fecha_solicitudMin.equals(""))
		{
				if(!df_fecha_solicitudMax.equals(""))
					condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE('"+df_fecha_solicitudMin+"','dd/mm/yyyy') and TO_DATE('"+df_fecha_solicitudMax+"','dd/mm/yyyy') ");
				else
					condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('"+df_fecha_solicitudMin+"','dd/mm/yyyy') ");
		}
		query.append(" select (decode (i.cs_habilitado,'N','*','S',' ')||' '||i.cg_razon_social) as nombreIF"+
				" , (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO"+
				" , (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+
				" , substr(s.ic_folio,1,10)||'-'||substr(s.ic_folio,11,1) as ic_folio"+
				" , d.ic_moneda, m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto"+
				" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as df_fecha_solicitud"+
				" , cce.cd_descripcion as CambioEstatus"+
				" , TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') as dc_fecha_cambio"+
				" , ce.ct_cambio_motivo,'ConsCStSolicNafinDE::getDocumentQueryFile()' ORIGENQRY"+
				" from com_solicitud s"+
				" , com_docto_seleccionado ds, com_documento d"+
				" , comcat_moneda m, comcat_epo e, comcat_pyme p, comcat_cambio_estatus cce"+
				" ,comhis_cambio_estatus ce, comcat_if i, comrel_pyme_epo pe, comrel_producto_epo prodepo "+
				" where"+
				" s.ic_documento=ds.ic_documento"+
				" and ds.ic_documento=d.ic_documento"+
				" and d.ic_pyme=p.ic_pyme"+
				" and d.ic_epo=e.ic_epo"+
				"  and d.ic_epo = prodepo.ic_epo "   +
				"  and prodepo.ic_producto_nafin = 1 "   +
				"  and (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) "   +
				" and ds.ic_if=i.ic_if"+
				" and d.ic_moneda=m.ic_moneda"+
				" and d.ic_documento = ce.ic_documento"+
				" and d.ic_epo = pe.ic_epo and d.ic_pyme = pe.ic_pyme"+
				" and ce.ic_cambio_estatus=cce.ic_cambio_estatus"+
				" and ce.ic_cambio_estatus = 7 "+
				//(!ic_banco_fondeo.equals("")?" and s.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +
        " and d.ic_epo = e.ic_epo"+//FODEA 007 - 2009
        (!ic_banco_fondeo.equals("")?" and e.ic_banco_fondeo="+ic_banco_fondeo+" ":"") +//FODEA 007 - 2009
        " "+condicion.toString());
    return query.toString();
  }

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
  // Genera el query que extrae solo un segmento de la informacion
    StringBuffer query = new StringBuffer("");
    StringBuffer condicion = new StringBuffer("");
    // Parametros de la pagina
    String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus") == null)?"":request.getParameter("ic_cambio_estatus");
    String dc_fecha_cambioMin = (request.getParameter("dc_fecha_cambioMin") == null)?"":request.getParameter("dc_fecha_cambioMin");
    String dc_fecha_cambioMax = (request.getParameter("dc_fecha_cambioMax") == null)?"":request.getParameter("dc_fecha_cambioMax");
    // Generacion del query
		if (!ic_cambio_estatus.equals(""))
				condicion.append(" and ce.ic_cambio_estatus = "+ic_cambio_estatus);
		if(!dc_fecha_cambioMin.equals("")) {
				if(!dc_fecha_cambioMax.equals(""))
					condicion.append(" and TO_DATE(TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') and TO_DATE('"+dc_fecha_cambioMax+"','dd/mm/yyyy') ");
		else
					condicion.append(" and TO_DATE(TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('"+dc_fecha_cambioMin+"','dd/mm/yyyy') ");
		}
		query.append(" select (decode (i.cs_habilitado,'N','*','S',' ')||' '||i.cg_razon_social) as nombreIF"+
				" , (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as nombreEPO"+
				" , (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as nombrePYME"+
				" , substr(s.ic_folio,1,10)||'-'||substr(s.ic_folio,11,1) as ic_folio"+
				" , d.ic_moneda, m.cd_nombre, d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto"+
				" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as df_fecha_solicitud"+
				" , cce.cd_descripcion as CambioEstatus"+
				" , TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') as dc_fecha_cambio"+
				" , ce.ct_cambio_motivo,'ConsCStSolicNafinDE::getDocumentQueryFile()' ORIGENQRY"+
				" from com_solicitud s"+
				" , com_docto_seleccionado ds, com_documento d"+
				" , comcat_moneda m, comcat_epo e, comcat_pyme p, comcat_cambio_estatus cce"+
				" ,comhis_cambio_estatus ce, comcat_if i, comrel_pyme_epo pe"+
				" where"+
				" s.ic_documento=ds.ic_documento"+
				" and ds.ic_documento=d.ic_documento"+
				" and d.ic_pyme=p.ic_pyme"+
				" and d.ic_epo=e.ic_epo"+
				" and ds.ic_if=i.ic_if"+
				" and d.ic_moneda=m.ic_moneda"+
				" and d.ic_documento = ce.ic_documento"+
				" and d.ic_epo = pe.ic_epo and d.ic_pyme = pe.ic_pyme"+
				" and ce.ic_cambio_estatus=cce.ic_cambio_estatus"+
				" and ce.ic_cambio_estatus = 7 "+
        " AND DS.ic_documento||to_char(CE.DC_FECHA_CAMBIO,'ddmmyyyyhhmiss') in (");
        for (Iterator it = ids.iterator(); it.hasNext();){
          query.append("'"+it.next().toString() + "',");
        }
        query = query.delete(query.length()-1,query.length());
        query.append(") "+ condicion.toString());
    return query.toString();
  }
  
  //**********************************MIGRACION******************************************
  
  private final static Log log = ServiceLocator.getInstance().getLog(ConsCStSolicNafinDE.class);
	private String paginaOffset;
	private String paginaNo;
	private List conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_banco_fondeo;
	private String ic_epo;
	private String ic_pyme;
	private String ic_if;
	private String ic_folio;
	private String ig_plazoMin;
	private String ig_plazoMax;
	private String ic_moneda;
	private String ic_cambio_estatus;
	private String fn_montoMin;
	private String fn_montoMax;
	private String df_fecha_solicitudMin;
	private String df_fecha_solicitudMax;
	private String dc_fecha_cambioMin;
	private String dc_fecha_cambioMax;
	
	
  
  public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append(" select m.cd_nombre as MONEDA , count(*) NO_DOCUMENTOS ," +
                " sum(nvl(d.fn_monto,0)) AS MONTO_TOTAL, "                           +
                " sum(nvl(d.fn_monto_dscto,0)) AS   MONT_DESCONTAR_TOTAL  "          +
                "  from com_solicitud s"                                             +
                "  , com_docto_seleccionado ds"                                      +
                "  , com_documento d"                                                +
                "  , comhis_cambio_estatus ce"                                       +
                "  , comrel_pyme_epo pe, comrel_producto_epo prodepo "               +
                "  , comcat_epo e "                                                  +
                "  , comcat_moneda m  "                                              +
                "  where"                                                            +
                "  s.ic_documento=ds.ic_documento"                                   +
                "  and ds.ic_documento=d.ic_documento"                               +
                "  and d.ic_documento = ce.ic_documento"                             +
                "  and d.ic_epo = prodepo.ic_epo "                                   +
                "  and prodepo.ic_producto_nafin = 1 "                               +
                "  and d.ic_epo = pe.ic_epo "                                        +
                "  and d.ic_pyme = pe.ic_pyme"                                       +
                " and d.ic_epo = e.ic_epo"                                           +
                "  and ce.ic_cambio_estatus = 7 "                                    +
                " and d.ic_moneda = m.ic_moneda ");

		if(!ic_banco_fondeo.equals("")) {
			qrySentencia.append(" and e.ic_banco_fondeo=  ? ");
			conditions.add(ic_banco_fondeo);
		}
		if (!ic_if.equals("")){
			qrySentencia.append(" and ds.ic_if = ?");
			conditions.add(ic_if);
		}
		if (!ic_folio.equals("")){
			qrySentencia.append(" and s.ic_folio = ?");
			conditions.add(ic_folio);
		}
		if (!ic_epo.equals("")){
			qrySentencia.append(" and d.ic_epo = ?");
			conditions.add(ic_epo);
		}
		if (!ic_pyme.equals("")){
			qrySentencia.append(" and d.ic_pyme = ? ");
			conditions.add(ic_pyme);
		}
		if (!ic_moneda.equals("")) {
			qrySentencia.append(" and d.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}
		if (!ic_cambio_estatus.equals("")){
			qrySentencia.append(" and ce.ic_cambio_estatus = ? ");
			conditions.add(ic_cambio_estatus);
		}
		if(!dc_fecha_cambioMin.equals("") && !dc_fecha_cambioMax.equals("") ) {
			qrySentencia.append(" and TO_DATE(TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE( ? ,'dd/mm/yyyy') and TO_DATE(? ,'dd/mm/yyyy') ");
			conditions.add(dc_fecha_cambioMin);
			conditions.add(dc_fecha_cambioMax);
		}
		if(!ig_plazoMin.equals("") && !ig_plazoMax.equals("")) {
			qrySentencia.append(" and s.ig_plazo between ?  and ? ");
			conditions.add(ig_plazoMin);
			conditions.add(ig_plazoMax);
		}
		if(!fn_montoMin.equals("") &&  !fn_montoMax.equals("") ){
			qrySentencia.append(" and d.fn_monto between ?  and  ?");
			conditions.add(fn_montoMin);
			conditions.add(fn_montoMax);
		}
		if(!df_fecha_solicitudMin.equals("") && !df_fecha_solicitudMax.equals("")) {
			qrySentencia.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE( ? ,'dd/mm/yyyy') and TO_DATE( ? ,'dd/mm/yyyy') ");
			conditions.add(df_fecha_solicitudMin);
			conditions.add(df_fecha_solicitudMax);
		}

		qrySentencia.append(" group by m.cd_nombre   order by m.cd_nombre ");

		log.info("qrySentencia: "+qrySentencia);
		log.info("conditions: "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}

/**
 * se obtiene las llaves primaria 
 * @return 
 */
	public String getDocumentQuery(){

		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		qrySentencia.append(" select DS.IC_DOCUMENTO||to_char(CE.DC_FECHA_CAMBIO,'ddmmyyyyhhmiss') " +
                    "  from com_solicitud s"                                                       +
                    "  , com_docto_seleccionado ds"                                                +
                    "  , com_documento d"                                                          +
                    "  , comhis_cambio_estatus ce"                                                 +
                    "  , comrel_pyme_epo pe, comrel_producto_epo prodepo "                         +
                    "  , comcat_epo e "                                                            +//FODEA 007 - 2009
                    "  where"                                                                      +
                    "  s.ic_documento=ds.ic_documento"                                             +
                    "  and ds.ic_documento=d.ic_documento"                                         +
                    "  and d.ic_documento = ce.ic_documento"                                       +
                    "  and d.ic_epo = prodepo.ic_epo "                                             +
                    "  and prodepo.ic_producto_nafin = 1 "                                         +
                    "  and (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) "            +
                    "  and d.ic_epo = pe.ic_epo "                                                  +
                    "  and d.ic_pyme = pe.ic_pyme"                                                 +
                    " and d.ic_epo = e.ic_epo"                                                     +
                    "  and ce.ic_cambio_estatus = 7 ");

		if(!ic_banco_fondeo.equals("")) {
			qrySentencia.append(" and e.ic_banco_fondeo=  ? ");
			conditions.add(ic_banco_fondeo);
		}
		if (!ic_if.equals("")){
			qrySentencia.append(" and ds.ic_if = ?");
			conditions.add(ic_if);
		}
		if (!ic_folio.equals("")){
			qrySentencia.append(" and s.ic_folio = ?");
			conditions.add(ic_folio);
		}
		if (!ic_epo.equals("")){
			qrySentencia.append(" and d.ic_epo = ?");
			conditions.add(ic_epo);
		}
		if (!ic_pyme.equals("")){
			qrySentencia.append(" and d.ic_pyme = ? ");
			conditions.add(ic_pyme);
		}
		if (!ic_moneda.equals("")) {
			qrySentencia.append(" and d.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}
		if (!ic_cambio_estatus.equals("")){
			qrySentencia.append(" and ce.ic_cambio_estatus = ? ");
			conditions.add(ic_cambio_estatus);
		}
		if(!dc_fecha_cambioMin.equals("") && !dc_fecha_cambioMax.equals("") ) {
			qrySentencia.append(" and TO_DATE(TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE( ? ,'dd/mm/yyyy') and TO_DATE(? ,'dd/mm/yyyy') ");
			conditions.add(dc_fecha_cambioMin);
			conditions.add(dc_fecha_cambioMax);
		}
		if(!ig_plazoMin.equals("") && !ig_plazoMax.equals("")) {
			qrySentencia.append(" and s.ig_plazo between ?  and ? ");
			conditions.add(ig_plazoMin);
			conditions.add(ig_plazoMax);
		}
		if(!fn_montoMin.equals("") &&  !fn_montoMax.equals("") ){
			qrySentencia.append(" and d.fn_monto between ?  and  ?");
			conditions.add(fn_montoMin);
			conditions.add(fn_montoMax);
		}
		if(!df_fecha_solicitudMin.equals("") && !df_fecha_solicitudMax.equals("")) {
			qrySentencia.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE( ? ,'dd/mm/yyyy') and TO_DATE( ? ,'dd/mm/yyyy') ");
			conditions.add(df_fecha_solicitudMin);
			conditions.add(df_fecha_solicitudMax);
		}

		log.info("qrySentencia: "+qrySentencia);
		log.info("conditions: "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}

/**
 * 
 * @return 
 * @param pageIds
 */
	public String getDocumentSummaryQueryForIds(List pageIds) {

		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append(" SELECT  (decode (i.cs_habilitado,'N','*','S',' ')||' '||i.cg_razon_social) as NOMBRE_IF " +
				" , (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as NOMBRE_EPO "                    +
				" , (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as NOMBRE_PROVEEDOR "             +
				" , substr(s.ic_folio,1,10)||'-'||substr(s.ic_folio,11,1) as NUM_SOLICITUD "                              +
				" , m.cd_nombre as MONEDA "                                                                               +
				" , m.ic_moneda as IC_MONEDA, "                                                                           +
				"  TO_CHAR(d.fn_monto,'FM999999999.99') as MONTO, "                                                       +
				"  d.fn_porc_anticipo as PORCE_DESC , "                                                                   +
				"   d.fn_monto -  d.fn_monto_dscto    AS  RECURSO_GARAN, "                                                +
				"  TO_CHAR(d.fn_monto_dscto,'FM999999999.99') as MONTO_DESCONTAR "                                        +
				" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as FECHA_REG "                                             +
				" , cce.cd_descripcion as TIPO_CAMBIO "                                                                   +
				" , TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') as FECHA_CAMBIO "                                            +
				" , ce.ct_cambio_motivo as CAUSA "                                                                        +
				" from com_solicitud s"                                                                                   +
				" , com_docto_seleccionado ds, com_documento d"                                                           +
				" , comcat_moneda m, comcat_epo e, comcat_pyme p, comcat_cambio_estatus cce"                              +
				" ,comhis_cambio_estatus ce, comcat_if i, comrel_pyme_epo pe"                                             +
				" where"                                                                                                  +
				" s.ic_documento=ds.ic_documento"                                                                         +
				" and ds.ic_documento=d.ic_documento"                                                                     +
				" and d.ic_pyme=p.ic_pyme"                                                                                +
				" and d.ic_epo=e.ic_epo"                                                                                  +
				" and ds.ic_if=i.ic_if"                                                                                   +
				" and d.ic_moneda=m.ic_moneda"                                                                            +
				" and d.ic_documento = ce.ic_documento"                                                                   +
				" and d.ic_epo = pe.ic_epo and d.ic_pyme = pe.ic_pyme"                                                    +
				" and ce.ic_cambio_estatus=cce.ic_cambio_estatus"                                                         +
				" and ce.ic_cambio_estatus = 7 ");

			qrySentencia.append(" AND (");

				for (int i = 0; i < pageIds.size(); i++) {
					List lItem = (ArrayList)pageIds.get(i);

					if(i > 0){qrySentencia.append("  OR  ");}

					qrySentencia.append("  DS.ic_documento||to_char(CE.DC_FECHA_CAMBIO,'ddmmyyyyhhmiss')  = ? ");
					conditions.add(lItem.get(0).toString());
				}

				qrySentencia.append(" ) ");

		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append(" SELECT  (decode (i.cs_habilitado,'N','*','S',' ')||' '||i.cg_razon_social) as NOMBRE_IF " +
				" , (decode (e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as NOMBRE_EPO "                    +
				" , (decode (pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as NOMBRE_PROVEEDOR "             +
				" , substr(s.ic_folio,1,10)||'-'||substr(s.ic_folio,11,1) as NUM_SOLICITUD "                              +
				" , m.cd_nombre as MONEDA "                                                                               +
				" , m.ic_moneda as IC_MONEDA, "                                                                           +
				"  d.fn_monto as MONTO , "                                                                                +
				"  d.fn_porc_anticipo as PORCE_DESC ,"                                                                    +
				"  d.fn_monto -  d.fn_monto_dscto   AS  RECURSO_GARAN, "                                                  +
				"  d.fn_monto_dscto as MONTO_DESCONTAR "                                                                  +
				" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as FECHA_REG "                                             +
				" , cce.cd_descripcion as TIPO_CAMBIO "                                                                   +
				" , TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') as FECHA_CAMBIO "                                            +
				" , ce.ct_cambio_motivo as CAUSA "                                                                        +
				" from com_solicitud s"                                                                                   +
				" , com_docto_seleccionado ds, com_documento d"                                                           +
				" , comcat_moneda m, comcat_epo e, comcat_pyme p, comcat_cambio_estatus cce"                              +
				" ,comhis_cambio_estatus ce, comcat_if i, comrel_pyme_epo pe"                                             +
				" where"                                                                                                  +
				" s.ic_documento=ds.ic_documento"                                                                         +
				" and ds.ic_documento=d.ic_documento"                                                                     +
				" and d.ic_pyme=p.ic_pyme"                                                                                +
				" and d.ic_epo=e.ic_epo"                                                                                  +
				" and ds.ic_if=i.ic_if"                                                                                   +
				" and d.ic_moneda=m.ic_moneda"                                                                            +
				" and d.ic_documento = ce.ic_documento"                                                                   +
				" and d.ic_epo = pe.ic_epo and d.ic_pyme = pe.ic_pyme"                                                    +
				" and ce.ic_cambio_estatus=cce.ic_cambio_estatus"                                                         +
				" and ce.ic_cambio_estatus = 7 ");

			if(!ic_banco_fondeo.equals("")) {
				qrySentencia.append(" and e.ic_banco_fondeo=  ? ");
				conditions.add(ic_banco_fondeo);
			}
			if (!ic_if.equals("")){
				qrySentencia.append(" and ds.ic_if = ?");
				conditions.add(ic_if);
			}
			if (!ic_folio.equals("")){
				qrySentencia.append(" and s.ic_folio = ?");
				conditions.add(ic_folio);
			}
			if (!ic_epo.equals("")){
				qrySentencia.append(" and d.ic_epo = ?");
				conditions.add(ic_epo);
			}
			if (!ic_pyme.equals("")){
				qrySentencia.append(" and d.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!ic_moneda.equals("")) {
				qrySentencia.append(" and d.ic_moneda = ? ");
				conditions.add(ic_moneda);
			}
			if (!ic_cambio_estatus.equals("")){
				qrySentencia.append(" and ce.ic_cambio_estatus = ? ");
				conditions.add(ic_cambio_estatus);
			}
			if(!dc_fecha_cambioMin.equals("") && !dc_fecha_cambioMax.equals("") ) {
				qrySentencia.append(" and TO_DATE(TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE( ? ,'dd/mm/yyyy') and TO_DATE(? ,'dd/mm/yyyy') ");
				conditions.add(dc_fecha_cambioMin);
				conditions.add(dc_fecha_cambioMax);
			}
			if(!ig_plazoMin.equals("") && !ig_plazoMax.equals("")) {
				qrySentencia.append(" and s.ig_plazo between ?  and ? ");
				conditions.add(ig_plazoMin);
				conditions.add(ig_plazoMax);
			}
			if(!fn_montoMin.equals("") &&  !fn_montoMax.equals("") ){
				qrySentencia.append(" and d.fn_monto between ?  and  ?");
				conditions.add(fn_montoMin);
				conditions.add(fn_montoMax);
			}
			if(!df_fecha_solicitudMin.equals("") && !df_fecha_solicitudMax.equals("")) {
				qrySentencia.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE( ? ,'dd/mm/yyyy') and TO_DATE( ? ,'dd/mm/yyyy') ");
				conditions.add(df_fecha_solicitudMin);
				conditions.add(df_fecha_solicitudMax);
			}

		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual   = fechaActual.substring(0,2);
			String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual  = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	

			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

			pdfDoc.setLTable(13, 100);
			pdfDoc.setLCell("IF","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Proveedor","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Num. de Solicitud","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Monto ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Fecha Reg. Sol.","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tipo Cambio de Estatus","celda01",ComunesPDF.CENTER); 
			pdfDoc.setLCell("Fecha Cambio de Estatus","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Causa","celda01",ComunesPDF.CENTER);

			while (rs.next()){
				String nombreIF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
				String nombreEPO = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
				String nombreProveedor = (rs.getString("NOMBRE_PROVEEDOR") == null) ? "" : rs.getString("NOMBRE_PROVEEDOR");
				String num_solicitud = (rs.getString("NUM_SOLICITUD") == null) ? "" : rs.getString("NUM_SOLICITUD");
				String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
				String monto = (rs.getString("MONTO") == null) ? "" : rs.getString("MONTO");
				String porce_desc = (rs.getString("PORCE_DESC") == null) ? "" : rs.getString("PORCE_DESC");
				String recurso_garan = (rs.getString("RECURSO_GARAN") == null) ? "" : rs.getString("RECURSO_GARAN");
				String monto_descon = (rs.getString("MONTO_DESCONTAR") == null) ? "" : rs.getString("MONTO_DESCONTAR");
				String fecha_reg = (rs.getString("FECHA_REG") == null) ? "" : rs.getString("FECHA_REG");
				String tipo_cambio = (rs.getString("TIPO_CAMBIO") == null) ? "" : rs.getString("TIPO_CAMBIO");
				String fecha_cambio = (rs.getString("FECHA_CAMBIO") == null) ? "" : rs.getString("FECHA_CAMBIO");
				String causa = (rs.getString("CAUSA") == null) ? "" : rs.getString("CAUSA");
				String ic_moneda = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");

				pdfDoc.setLCell(nombreIF,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(nombreEPO,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(nombreProveedor,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(num_solicitud,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setLCell(porce_desc+"%","formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(recurso_garan,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto_descon,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setLCell(fecha_reg,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(tipo_cambio,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(fecha_cambio,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(causa,"formas",ComunesPDF.LEFT);
			
			}

			pdfDoc.addLTable();
			pdfDoc.endDocument();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}

		}
		return nombreArchivo;

	}

/**
 * Genera el archivo CSV o PDF seg�n el tipo que se le indique.
 * En el caso del PDF, el archivo generado no comtempla paginaci�n, imprime todos los registros que trae la consulta.
 * @param request
 * @param rs
 * @param path
 * @param tipo: CSV o PDF
 * @return nombre del archivo
 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");

		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();

		if(tipo.equals("CSV")){
			try {

				contenidoArchivo.append("IF,  EPO , Proveedor,  N�m. de Solicitud, Moneda, Monto ,Porcentaje de Descuento, Recurso en Garant�a, Monto a Descontar, Fecha Reg. Sol., Tipo Cambio de Estatus, Fecha Cambio de Estatus, Causa \n");						

				while (rs.next()){
					String nombreIF        = (rs.getString("NOMBRE_IF")        == null) ? "" : rs.getString("NOMBRE_IF");
					String nombreEPO       = (rs.getString("NOMBRE_EPO")       == null) ? "" : rs.getString("NOMBRE_EPO");
					String nombreProveedor = (rs.getString("NOMBRE_PROVEEDOR") == null) ? "" : rs.getString("NOMBRE_PROVEEDOR");
					String num_solicitud   = (rs.getString("NUM_SOLICITUD")    == null) ? "" : rs.getString("NUM_SOLICITUD");
					String moneda          = (rs.getString("MONEDA")           == null) ? "" : rs.getString("MONEDA");
					String monto           = (rs.getString("MONTO")            == null) ? "" : rs.getString("MONTO");
					String porce_desc      = (rs.getString("PORCE_DESC")       == null) ? "" : rs.getString("PORCE_DESC");
					String recurso_garan   = (rs.getString("RECURSO_GARAN")    == null) ? "" : rs.getString("RECURSO_GARAN");
					String monto_descon    = (rs.getString("MONTO_DESCONTAR")  == null) ? "" : rs.getString("MONTO_DESCONTAR");
					String fecha_reg       = (rs.getString("FECHA_REG")        == null) ? "" : rs.getString("FECHA_REG");
					String tipo_cambio     = (rs.getString("TIPO_CAMBIO")      == null) ? "" : rs.getString("TIPO_CAMBIO");
					String fecha_cambio    = (rs.getString("FECHA_CAMBIO")     == null) ? "" : rs.getString("FECHA_CAMBIO");
					String causa           = (rs.getString("CAUSA")            == null) ? "" : rs.getString("CAUSA");

				contenidoArchivo.append(nombreIF.replaceAll(",","")        +","+
												nombreEPO.replaceAll(",","")       +","+
												nombreProveedor.replaceAll(",","") +","+
												num_solicitud.replaceAll(",","")   +","+
												moneda.replaceAll(",","")          +","+
												monto.replaceAll(",","")           +","+
												porce_desc.replaceAll(",","")      +","+
												recurso_garan.replaceAll(",","")   +","+
												monto_descon.replaceAll(",","")    +","+
												fecha_reg.replaceAll(",","")       +","+
												tipo_cambio.replaceAll(",","")     +","+
												fecha_cambio.replaceAll(",","")    +","+
												causa.replaceAll(",","")+"\n");
				}

				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			}
		} else if(tipo.equals("PDF")){

			try {

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));  

				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(13, 100);
				pdfDoc.setLCell("IF",                      "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO",                     "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Proveedor",               "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�m. de Solicitud",       "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda",                  "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto ",                  "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Porcentaje de Descuento", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Recurso en Garant�a",     "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Descontar",       "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Reg. Sol.",         "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo Cambio de Estatus",  "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Cambio de Estatus", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa",                   "celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();

				while (rs.next()){
					String nombreIF        = (rs.getString("NOMBRE_IF")        == null) ? "" : rs.getString("NOMBRE_IF");
					String nombreEPO       = (rs.getString("NOMBRE_EPO")       == null) ? "" : rs.getString("NOMBRE_EPO");
					String nombreProveedor = (rs.getString("NOMBRE_PROVEEDOR") == null) ? "" : rs.getString("NOMBRE_PROVEEDOR");
					String num_solicitud   = (rs.getString("NUM_SOLICITUD")    == null) ? "" : rs.getString("NUM_SOLICITUD");
					String moneda          = (rs.getString("MONEDA")           == null) ? "" : rs.getString("MONEDA");
					String monto           = (rs.getString("MONTO")            == null) ? "" : rs.getString("MONTO");
					String porce_desc      = (rs.getString("PORCE_DESC")       == null) ? "" : rs.getString("PORCE_DESC");
					String recurso_garan   = (rs.getString("RECURSO_GARAN")    == null) ? "" : rs.getString("RECURSO_GARAN");
					String monto_descon    = (rs.getString("MONTO_DESCONTAR")  == null) ? "" : rs.getString("MONTO_DESCONTAR");
					String fecha_reg       = (rs.getString("FECHA_REG")        == null) ? "" : rs.getString("FECHA_REG");
					String tipo_cambio     = (rs.getString("TIPO_CAMBIO")      == null) ? "" : rs.getString("TIPO_CAMBIO");
					String fecha_cambio    = (rs.getString("FECHA_CAMBIO")     == null) ? "" : rs.getString("FECHA_CAMBIO");
					String causa           = (rs.getString("CAUSA")            == null) ? "" : rs.getString("CAUSA");
					String ic_moneda       = (rs.getString("IC_MONEDA")        == null) ? "" : rs.getString("IC_MONEDA");

					pdfDoc.setLCell(nombreIF,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombreEPO,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombreProveedor,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(num_solicitud,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(porce_desc+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(recurso_garan,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto_descon,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(fecha_reg,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipo_cambio,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(fecha_cambio,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causa,"formas",ComunesPDF.LEFT);

				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			}
		}

		log.debug("crearCustomFile (S)");
		return nombreArchivo;
	}
  
  
  /**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones
	 */
	public List getConditions() {  return conditions;  }
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}

/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_banco_fondeo() {
		return ic_banco_fondeo;
	}

	public void setIc_banco_fondeo(String ic_banco_fondeo) {
		this.ic_banco_fondeo = ic_banco_fondeo;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_folio() {
		return ic_folio;
	}

	public void setIc_folio(String ic_folio) {
		this.ic_folio = ic_folio;
	}

	public String getIg_plazoMin() {
		return ig_plazoMin;
	}

	public void setIg_plazoMin(String ig_plazoMin) {
		this.ig_plazoMin = ig_plazoMin;
	}

	public String getIg_plazoMax() {
		return ig_plazoMax;
	}

	public void setIg_plazoMax(String ig_plazoMax) {
		this.ig_plazoMax = ig_plazoMax;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getIc_cambio_estatus() {
		return ic_cambio_estatus;
	}

	public void setIc_cambio_estatus(String ic_cambio_estatus) {
		this.ic_cambio_estatus = ic_cambio_estatus;
	}

	public String getFn_montoMin() {
		return fn_montoMin;
	}

	public void setFn_montoMin(String fn_montoMin) {
		this.fn_montoMin = fn_montoMin;
	}

	public String getFn_montoMax() {
		return fn_montoMax;
	}

	public void setFn_montoMax(String fn_montoMax) {
		this.fn_montoMax = fn_montoMax;
	}

	public String getDf_fecha_solicitudMin() {
		return df_fecha_solicitudMin;
	}

	public void setDf_fecha_solicitudMin(String df_fecha_solicitudMin) {
		this.df_fecha_solicitudMin = df_fecha_solicitudMin;
	}

	public String getDf_fecha_solicitudMax() {
		return df_fecha_solicitudMax;
	}

	public void setDf_fecha_solicitudMax(String df_fecha_solicitudMax) {
		this.df_fecha_solicitudMax = df_fecha_solicitudMax;
	}

	public String getDc_fecha_cambioMin() {
		return dc_fecha_cambioMin;
	}

	public void setDc_fecha_cambioMin(String dc_fecha_cambioMin) {
		this.dc_fecha_cambioMin = dc_fecha_cambioMin;
	}

	public String getDc_fecha_cambioMax() {
		return dc_fecha_cambioMax;
	}

	public void setDc_fecha_cambioMax(String dc_fecha_cambioMax) {
		this.dc_fecha_cambioMax = dc_fecha_cambioMax;
	}

  
  
  
  
  
  
  
  
  
}