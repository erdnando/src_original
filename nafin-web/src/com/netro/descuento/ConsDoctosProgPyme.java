package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;

public class ConsDoctosProgPyme implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

	private List 	conditions;
	StringBuffer 	strQuery;
	StringBuffer 	condicion;
	private String operacion	   = "";
	private String paginaOffset 	= "0";	
	private String paginaNo       = "1";
	private String ic_if        	= "";
	private String ic_epo        	= "";
	private String hidAction		= "";
	private String tipoUsuario		= "";
 
	//Constructor
	public ConsDoctosProgPyme(){

	}

	public String getAggregateCalculationQuery() {
		conditions = new ArrayList();
		strQuery = new StringBuffer(
		"SELECT   /*+ index(A2) use_nl(D, DS, A2, I, I2, E, M) */ " +
"      count(1) AS totaldoctos, " +
"      sum(d.fn_monto) AS totalmontodocto, " +
"      sum(d.fn_monto_dscto) AS totalmontodscto, " +
"      sum(ds.in_importe_interes) AS totalimporteint, " +
"      sum(ds.in_importe_recibir) AS totalimporterecibir, " +
"      sum(ds.in_importe_recibir - ds.fn_importe_recibir_benef ) AS totalimporteneto, " +
"      d.ic_moneda AS icmoneda, " +
"      m.cd_nombre AS nombre_moneda " +
" FROM com_documento d, " +
"      com_docto_seleccionado ds, " +
"      com_acuse2 a2, " +
"      comcat_pyme p, " +
"      comcat_epo e, " +
"      comcat_if i, " +
"      comcat_if i2, " +
"      comcat_moneda m, " +
"      comrel_producto_epo pe, " +
"      comcat_tipo_factoraje tf " +
"	WHERE d.ic_documento = ds.ic_documento " +
"     AND d.ic_epo = e.ic_epo " +
"     AND d.ic_pyme = p.ic_pyme " +
"     AND d.ic_if = i.ic_if " +
"     AND d.ic_moneda = m.ic_moneda " +
"     AND ds.cc_acuse = a2.cc_acuse " +
"     AND d.ic_beneficiario = i2.ic_if(+) " +
"     AND d.cs_dscto_especial = tf.cc_tipo_factoraje " +
"     AND d.ic_estatus_docto = ? " +
"     AND d.ic_epo = pe.ic_epo " +
"     AND pe.ic_producto_nafin = ? "  );
				
		conditions.add(new Integer("26"));
		conditions.add(new Integer("1"));
		

		
		if(!"".equals(ic_epo)) {
			strQuery.append(" and d.ic_epo = ? ");
			conditions.add(ic_epo);
		}
		
		if(!"".equals(ic_if)) {
			strQuery.append(" and d.ic_if = ? ");
			conditions.add(ic_if);
		}	
		
		strQuery.append(" group by d.ic_epo, d.ic_if, d.ic_moneda,m.cd_nombre "+
							" order by d.ic_moneda ");
		
		System.out.println("ConsDoctosProgPyme::getAggregateCalculationQuery " + strQuery.toString());
		return strQuery.toString();
  	}  // Fin getAggregateCalculationQuery

	public String getDocumentQuery(){
		conditions = new ArrayList();
		strQuery = new StringBuffer(
		"SELECT   /*+ index(A2) use_nl(D, DS, A2, I, I2, E, M) */ " +
"      d.ic_documento " +
" FROM com_documento d, " +
"      com_docto_seleccionado ds, " +
"      com_acuse2 a2, " +
"      comcat_pyme p, " +
"      comcat_epo e, " +
"      comcat_if i, " +
"      comcat_if i2, " +
"      comcat_moneda m, " +
"      comrel_producto_epo pe, " +
"      comcat_tipo_factoraje tf " +
"	WHERE d.ic_documento = ds.ic_documento " +
"     AND d.ic_epo = e.ic_epo " +
"     AND d.ic_pyme = p.ic_pyme " +
"     AND d.ic_if = i.ic_if " +
"     AND d.ic_moneda = m.ic_moneda " +
"     AND ds.cc_acuse = a2.cc_acuse " +
"     AND d.ic_beneficiario = i2.ic_if(+) " +
"     AND d.cs_dscto_especial = tf.cc_tipo_factoraje " +
"     AND d.ic_estatus_docto = ? " +
"     AND d.ic_epo = pe.ic_epo " +
"     AND pe.ic_producto_nafin = ? "  );
		
		conditions.add(new Integer("26"));
		conditions.add(new Integer("1"));
		

		
		if(!"".equals(ic_epo)) {
			strQuery.append(" and d.ic_epo = ? ");
			conditions.add(ic_epo);
		}
		
		if(!"".equals(ic_if)) {
			strQuery.append(" and d.ic_if = ? ");
			conditions.add(ic_if);
		}	
		
		System.out.println("ConsDoctosProgPyme::getDocumentQuery " + strQuery.toString() + "\n" + conditions);
		return strQuery.toString();
  	}

public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
//		 SimpleDateFormat sdfFecha = new SimpleDateFormat ("dd/MM/yyyy");
		strQuery = new StringBuffer("SELECT   /*+ index(A2) use_nl(D, DS, A2, I, I2, E, M) */ "+
"      (   DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') " +
"       || ' ' " +
"       || e.cg_razon_social " +
"      ) AS nombreepo, " +
"      (   DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') " +
"       || ' ' " +
"       || p.cg_razon_social " +
"      ) AS nombrepyme, " +
"      d.ig_numero_docto AS numerodocto, " +
"      TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') AS fechadocto, " +
"      TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS fechavenc, " +
"      (   DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') " +
"       || ' ' " +
"       || i.cg_razon_social " +
"      ) AS nombreif, " +
"      ds.in_tasa_aceptada AS tasaaceptada, " +
"      (  d.df_fecha_venc " +
"       - DECODE (d.ic_moneda, " +
"                 1, TRUNC (SYSDATE), " +
"                 54, TO_DATE ('19/03/2010', 'dd/mm/yyyy') " +
"                ) " +
"      ) AS plazo, " +
"      m.cd_nombre AS nombremoneda, d.ic_moneda AS numeromoneda, " +
"      d.fn_monto AS montodocto, d.fn_porc_anticipo AS porcanticipo, " +
"      d.fn_monto_dscto AS montodscto, " +
"      ds.in_importe_interes AS importeinteres, " +
"      ds.in_importe_recibir AS importerecibir, " +
"      d.cs_dscto_especial AS tipofactoraje, " +
"      TO_CHAR (ds.df_programacion, 'DD/MM/YYYY') AS df_programacion, " +
"      i2.cg_razon_social AS beneficiario, " +
"      d.fn_porc_beneficiario AS porcbeneficiario, " +
"      ds.fn_importe_recibir_benef AS importebeneficiario, " +
"      (ds.in_importe_recibir - ds.fn_importe_recibir_benef " +
"      ) AS importeneto, tf.cg_nombre AS nombre_tipo_factoraje, " +
"		 cm.cg_razon_social as nombre_mandante, " +
"		 a2.cc_acuse as acuse, "+
"		 d.ct_referencia as referencia, "+
"      TO_CHAR (ds.df_fecha_seleccion, 'DD/MM/YYYY') AS fechaseleccion, " +
"      'ConsDoctosProgPyme:getQueryPorEstatus(26)' AS pantalla " +
" FROM com_documento d, " +
"      com_docto_seleccionado ds, " +
"      com_acuse2 a2, " +
"      comcat_pyme p, " +
"      comcat_epo e, " +
"      comcat_if i, " +
"      comcat_if i2, " +
"      comcat_moneda m, " +
"      comrel_producto_epo pe, " +
"      comcat_tipo_factoraje tf, " +
"		 comcat_mandante cm " +
"	WHERE d.ic_documento = ds.ic_documento " +
"     AND d.ic_epo = e.ic_epo " +
"     AND d.ic_pyme = p.ic_pyme " +
"     AND d.ic_if = i.ic_if " +
"     AND d.ic_moneda = m.ic_moneda " +
"     AND ds.cc_acuse = a2.cc_acuse " +
"     AND d.ic_beneficiario = i2.ic_if(+) " +
"		AND d.ic_mandante = cm.ic_mandante(+) " +
"     AND d.cs_dscto_especial = tf.cc_tipo_factoraje " +
"     AND d.ic_estatus_docto = ? " +
"     AND d.ic_epo = pe.ic_epo " +
"     AND pe.ic_producto_nafin = ? " );

		conditions.add(new Integer(26));
		conditions.add(new Integer(1));
	
		if(!"".equals(ic_epo)) {
			strQuery.append(" and d.ic_epo = ? ");
			conditions.add(new Integer(ic_epo));
		}
		
		if(!"".equals(ic_if)) {
			strQuery.append(" and d.ic_if = ? ");
			conditions.add(new Integer(ic_if));
		}	
    
			for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i==0) {
				strQuery.append(" and d.ic_documento  in ( ");
			}
			strQuery.append("?");
			conditions.add(new Long(lItem.get(0).toString()));
			if(i!=(pageIds.size()-1)) {
				strQuery.append(","); 
			} else {
				strQuery.append(" ) ");
			}
			
		}	
			System.out.println(" ConsDoctosProgPyme:::getDocumentSummaryQueryForIds " + strQuery );
			System.out.println( "conditions: " + conditions);
			return strQuery.toString();
		 }

	public String getDocumentQueryFile(){
		conditions = new ArrayList();      
		strQuery = new StringBuffer("SELECT   /*+ index(A2) use_nl(D, DS, A2, I, I2, E, M) */ ");
		if("IF".equals(tipoUsuario)){
			strQuery.append("      (   DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') " +
			"       || ' ' " +
			"       || e.cg_razon_social " +
			"      ) AS nombre_epo, " );
		}
		
		strQuery.append("      (   DECODE (p.cs_habilitado, 'N', '*', 'S', ' ') " +
		"       || ' ' " +
		"       || p.cg_razon_social " +
		"      ) AS nombre_pyme, " +
		"      d.ig_numero_docto AS numero_de_documento, " +
		"      TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') AS fecha_de_emision, " +
		"      TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS fecha_de_vencimiento, " +
		"      m.cd_nombre AS nombre_moneda, " +
		"      tf.cg_nombre AS tipo_factoraje, " +
		"      NVL(d.fn_monto,0) AS monto_documento, " +
		"      NVL(d.fn_porc_anticipo,0) AS porcentaje_descuento, " +
		"      NVL(d.fn_monto_dscto,0) AS montodscto, " +
		"      (   DECODE (i.cs_habilitado, 'N', '*', 'S', ' ') " +
		"       || ' ' " +
		"       || i.cg_razon_social " +
		"      ) AS intermediario_financiero, " +
		"      ds.in_tasa_aceptada AS tasa_aceptada, " +
		"      (  d.df_fecha_venc " +
		"       - DECODE (d.ic_moneda, " +
		"                 1, TRUNC (SYSDATE), " +
		"                 54, TO_DATE ('19/03/2010', 'dd/mm/yyyy') " +
		"                ) " +
		"      ) AS plazo_dias, " +
		"      NVL(ds.in_importe_interes,0) AS monto_int, " +
		"      NVL(ds.in_importe_recibir,0) AS importe_a_recibir, " +
		"		 d.cc_acuse as acuse, "+
		"		 d.ct_referencia as referencia, "+
		"      (ds.in_importe_recibir - ds.fn_importe_recibir_benef " +
		"      ) AS importeneto, " +
		"      i2.cg_razon_social AS beneficiario, " +
		"      NVL(d.fn_porc_beneficiario,0) AS porcentaje_beneficiario, " +
		"      NVL(ds.fn_importe_recibir_benef,0) AS importe_beneficiario, " +
		"		 cm.cg_razon_social as nombre_mandante, " +
		"      TO_CHAR (ds.df_fecha_seleccion, 'DD/MM/YYYY') AS fecha_registro_operacion " +		
		" FROM com_documento d, " +
		"      com_docto_seleccionado ds, " +
		"      com_acuse2 a2, " +
		"      comcat_pyme p, " +
		"      comcat_epo e, " +
		"      comcat_if i, " +
		"      comcat_if i2, " +
		"      comcat_moneda m, " +
		"      comrel_producto_epo pe, " +
		"      comcat_tipo_factoraje tf, " +
		"		 comcat_mandante cm " +
		"	WHERE d.ic_documento = ds.ic_documento " +
		"     AND d.ic_epo = e.ic_epo " +
		"     AND d.ic_pyme = p.ic_pyme " +
		"     AND d.ic_if = i.ic_if " +
		"     AND d.ic_moneda = m.ic_moneda " +
		"     AND ds.cc_acuse = a2.cc_acuse " +
		"     AND d.ic_beneficiario = i2.ic_if(+) " +
		"		AND d.ic_mandante = cm.ic_mandante(+) " +
		"     AND d.cs_dscto_especial = tf.cc_tipo_factoraje " +
		"     AND d.ic_estatus_docto = ? " +
		"     AND d.ic_epo = pe.ic_epo " +
		"     AND pe.ic_producto_nafin = ? ");

		conditions.add(new Integer(26));
		conditions.add(new Integer(1));

		if(!"".equals(ic_epo)) {
			strQuery.append(" and d.ic_epo = ? ");
			conditions.add(ic_epo);
		}	
		
		if(!"".equals(ic_if)) {
			strQuery.append(" and d.ic_if = ? ");
			conditions.add(ic_if);
		}	

			System.out.println(" ConsDoctosProgPyme:::getDocumentQueryFile " + strQuery );
			System.out.println( "conditions:getDocumentQueryFile " + conditions);
			return strQuery.toString();
			
		}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		StringBuffer linea = new StringBuffer(1024);
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		String tipoUsuario = (String)session.getAttribute("strTipoUsuario");

		if ("CSV".equals(tipo)) {
			if("IF".equals(tipoUsuario)){
				linea.append(" Nombre Epo,");
			}
			linea.append(" Nombre Pyme, N�mero de Documento, Fecha de Emisi�n, Fecha de Vencimiento, ");
			linea.append(" Moneda, Tipo de Factoraje, Monto de Documento, Porcentaje de Descuento, Monto a Descontar, Intermediario Financiero,");
			linea.append(" Tasa,Plazo Dias,Monto Interes,Importe a Recibir,N�mero de Acuse,Referencia,Neto a Recibir, ");
			linea.append(" Beneficiario,Porcentaje del Beneficiario,Mandatario,Fecha del Registro de la operaci�n \n");

			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea.toString());
				
				while (rs.next()) {
					linea.setLength(0);
					String nombreEpo = "";
					if("IF".equals(tipoUsuario)){
						nombreEpo = (rs.getString("nombre_epo") == null) ? "" : rs.getString("nombre_epo");
					}
					String nombrePyme  = (rs.getString("nombre_pyme") == null) ? "" : rs.getString("nombre_pyme");
					String numeroDocto = (rs.getString("numero_de_documento") == null) ? "" : rs.getString("numero_de_documento");
					String fechaDocto  = (rs.getString("fecha_de_emision") == null) ? "" : rs.getString("fecha_de_emision");
					String fechaVenc = (rs.getString("fecha_de_vencimiento") == null) ? "" : rs.getString("fecha_de_vencimiento");
					String nombreIf = (rs.getString("intermediario_financiero") == null) ? "" : rs.getString("intermediario_financiero");
					String tasaAceptada = (rs.getString("tasa_aceptada") == null) ? "" : rs.getString("tasa_aceptada");
					String plazo = (rs.getString("plazo_dias") == null) ? "" : rs.getString("plazo_dias");
					String nombreMoneda = (rs.getString("nombre_moneda") == null) ? "" : rs.getString("nombre_moneda");
					String montoDocto = (rs.getString("monto_documento") == null) ? "0" : rs.getString("monto_documento");
					String porcAnticipo = (rs.getString("porcentaje_descuento") == null) ? "0" : rs.getString("porcentaje_descuento");
					String montoDscto = (rs.getString("montodscto") == null) ? "0" : rs.getString("montodscto");
					String importeInteres = (rs.getString("monto_int") == null) ? "0" : rs.getString("monto_int");
					String importeRecibir = (rs.getString("importe_a_recibir") == null) ? "0" : rs.getString("importe_a_recibir");
					String tipoFactoraje = (rs.getString("tipo_factoraje") == null) ? "" : rs.getString("tipo_factoraje");
					String fechaProgramacion = (rs.getString("fecha_registro_operacion") == null) ? "" : rs.getString("fecha_registro_operacion");
					String beneficiario = (rs.getString("beneficiario") == null) ? "" : rs.getString("beneficiario");
					String porcBeneficiario = (rs.getString("porcentaje_beneficiario") == null) ? "0" : rs.getString("porcentaje_beneficiario");
					String importeBeneficiario = (rs.getString("importe_beneficiario") == null) ? "0" : rs.getString("importe_beneficiario");
					String importeNeto = (rs.getString("importeneto") == null) ? "0" : rs.getString("importeneto");
					String nombreTipoFactoraje = (rs.getString("tipo_factoraje") == null) ? "" : rs.getString("tipo_factoraje");
					String nombreMandante = (rs.getString("nombre_mandante") == null) ? "" : rs.getString("nombre_mandante");
					String acuse = (rs.getString("acuse") == null) ? "" : rs.getString("acuse");
					String referencia = (rs.getString("referencia") == null) ? "" : rs.getString("referencia");
					String fechaSeleccion = (rs.getString("fecha_registro_operacion") == null) ? "" : rs.getString("fecha_registro_operacion");

					if("IF".equals(tipoUsuario)){
						linea.append(nombreEpo.replace(',',' ')+", ");
					}			
					linea.append(nombrePyme.replace(',',' ')+", " +
							numeroDocto.replace(',',' ')+", " +
							fechaDocto.replace(',',' ')+", " +
							fechaVenc.replace(',',' ')+", " +
							nombreMoneda.replace(',',' ')+", " +
							nombreTipoFactoraje.replace(',',' ')+"," +
							"$"+montoDocto.replace(',',' ')+"," +
							porcAnticipo.replace(',',' ')+"%"+"," +
							"$"+montoDscto.replace(',',' ')+"," +
							nombreIf.replace(',',' ')+", " +
							tasaAceptada.replace(',',' ')+", " +
							plazo.replace(',',' ')+", " +
							"$"+importeInteres.replace(',',' ')+"," +
							"$"+importeRecibir.replace(',',' ')+"," +
							acuse.replace(',',' ')+"," +
							referencia.replace(',',' ')+"," +
							"$"+importeNeto.replace(',',' ')+"," +
							beneficiario.replace(',',' ')+"," +
							porcBeneficiario.replace(',',' ')+"%"+"," +
							nombreMandante.replace(',',' ')+"," +
							fechaSeleccion.replace(',',' ')+"\n");
			
					buffer.write(linea.toString());
				}//fin while
				buffer.close();
			} catch (Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		} else if ("PDF".equals(tipo)) {
			try {

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				float anchoCeldaA[] = {2f, 9.038f, 9.038f, 7.692f, 7.692f, 7.692f, 8.692f, 8.692f, 8.692f, 7.692f, 7.692f, 7.692f, 7.692f, 7.692f};
				pdfDoc.setTable(12,100,anchoCeldaA);		
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				if("IF".equals(tipoUsuario)){
					pdfDoc.setCell("Nombre Epo","celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setCell("Nombre Pyme","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda,","celda01",ComunesPDF.CENTER);		
				pdfDoc.setCell("Tipo de Factoraje","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto de Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento,","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
				if(!"IF".equals(tipoUsuario)){
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				}
			
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasas","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo Dias","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Interes","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe a Recibir","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Acuse","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Neto a Recibir","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("Beneficiario,","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje del Beneficiario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Mandatario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha del Registro de la operaci�n","celda01",ComunesPDF.CENTER);			
				while (rs.next()) {
					String nombreEpo = "";
					if("IF".equals(tipoUsuario)){
						nombreEpo = (rs.getString("nombre_epo") == null) ? "" : rs.getString("nombre_epo");
					}
					String nombrePyme  = (rs.getString("nombre_pyme") == null) ? "" : rs.getString("nombre_pyme");
					String numeroDocto = (rs.getString("numero_de_documento") == null) ? "" : rs.getString("numero_de_documento");
					String fechaDocto  = (rs.getString("fecha_de_emision") == null) ? "" : rs.getString("fecha_de_emision");
					String fechaVenc = (rs.getString("fecha_de_vencimiento") == null) ? "" : rs.getString("fecha_de_vencimiento");
					String nombreIf = (rs.getString("intermediario_financiero") == null) ? "" : rs.getString("intermediario_financiero");
					String tasaAceptada = (rs.getString("tasa_aceptada") == null) ? "" : rs.getString("tasa_aceptada");
					String plazo = (rs.getString("plazo_dias") == null) ? "" : rs.getString("plazo_dias");
					String nombreMoneda = (rs.getString("nombre_moneda") == null) ? "" : rs.getString("nombre_moneda");
					String montoDocto = (rs.getString("monto_documento") == null) ? "0" : rs.getString("monto_documento");
					String porcAnticipo = (rs.getString("porcentaje_descuento") == null) ? "0" : rs.getString("porcentaje_descuento");
					String montoDscto = (rs.getString("montodscto") == null) ? "0" : rs.getString("montodscto");
					String importeInteres = (rs.getString("monto_int") == null) ? "0" : rs.getString("monto_int");
					String importeRecibir = (rs.getString("importe_a_recibir") == null) ? "0" : rs.getString("importe_a_recibir");
					String tipoFactoraje = (rs.getString("tipo_factoraje") == null) ? "" : rs.getString("tipo_factoraje");
					String fechaProgramacion = (rs.getString("fecha_registro_operacion") == null) ? "" : rs.getString("fecha_registro_operacion");
					String beneficiario = (rs.getString("beneficiario") == null) ? "" : rs.getString("beneficiario");
					String porcBeneficiario = (rs.getString("porcentaje_beneficiario") == null) ? "0" : rs.getString("porcentaje_beneficiario");
					String importeBeneficiario = (rs.getString("importe_beneficiario") == null) ? "0" : rs.getString("importe_beneficiario");
					String importeNeto = (rs.getString("importeneto") == null) ? "0" : rs.getString("importeneto");
					String nombreTipoFactoraje = (rs.getString("tipo_factoraje") == null) ? "" : rs.getString("tipo_factoraje");
					String nombreMandante = (rs.getString("nombre_mandante") == null) ? "" : rs.getString("nombre_mandante");
					String acuse = (rs.getString("acuse") == null) ? "" : rs.getString("acuse");
					String referencia = (rs.getString("referencia") == null) ? "" : rs.getString("referencia");
					String fechaSeleccion = (rs.getString("fecha_registro_operacion") == null) ? "" : rs.getString("fecha_registro_operacion");
					
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					if("IF".equals(tipoUsuario)){
						pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.CENTER);		
					}			
					
					pdfDoc.setCell(nombrePyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numeroDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreMoneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreTipoFactoraje,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDocto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(porcAnticipo+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDscto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(nombreIf,"formas",ComunesPDF.CENTER);
					
					//B
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);			
					pdfDoc.setCell(tasaAceptada,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeInteres,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeRecibir,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(importeNeto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(porcBeneficiario+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreMandante,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaSeleccion,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}

		return nombreArchivo;
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	/*--------------------LOS METODOS GET----------------------*/
		
 
	public List getConditions() {
		return conditions;
	}
	public String getOperacion(){
		return operacion;
	}
		
	public String getPaginaNo() {
		return paginaNo;
	}
	public String getPaginaOffset() {
		return paginaOffset;
	}
 
	public String getIc_if(){
		return ic_if;
	}
	
	public String getIc_epo() {
		return ic_epo;
	}

	/*--------------------LOS METODOS SET----------------------*/
 
	public void setOperacion(String newOperacion){
		operacion = newOperacion;
	}

	public void setPaginaOffset(String newPaginaOffset) {
		paginaOffset = newPaginaOffset;
	}
	public void setPaginaNo(String newPaginaNo) {
		paginaNo = newPaginaNo;
}

	public void setIc_if(String ic_if){
		this.ic_if = ic_if;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public void setHidAction(String hidAction) {
		this.hidAction = hidAction;
	}


	public String getHidAction() {
		return hidAction;
	}


	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}


	public String getTipoUsuario() {
		return tipoUsuario;
	}



  }