package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class CorreosNotificaciones implements IQueryGeneratorRegExtJS  , IQueryGeneratorReg {	
		
	public CorreosNotificaciones() {}

	// Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(CorreosNotificaciones.class);
  
	public  StringBuffer strQuery;
	private List conditions = new ArrayList(); 
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer strSQL;
	private String paginaOffset;
	private String paginaNo;	
	private String claveIf;
	private String claveTransaccion;		
	private String claveEstatus;

	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getAggregateCalculationQuery(I)");
			
		strSQL.append(" SELECT COUNT(*) numero_registros");
		strSQL.append(" FROM gti_correo_notif");		
		strSQL.append(" WHERE ic_if = ? ");
		
		conditions.add(new Integer(claveIf));

		if(claveTransaccion != null && !claveTransaccion.equals("")){
				strSQL.append(" AND ic_tipo_operacion = ?");
			conditions.add(new Integer(claveTransaccion));
		}
		if(claveEstatus != null && !claveEstatus.equals("")){
			strSQL.append(" AND ic_estatus_notif = ?");
			conditions.add(new Integer(claveEstatus));
		}

		log.debug("..:: strSQL : " + strSQL.toString());
		log.debug("..:: conditions : " + conditions);
		
		log.info("getAggregateCalculationQuery(F)");
		return strSQL.toString();
	}//getAggregateCalculationQuery
	
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getDocumentQuery(I)");
		
		strSQL.append(" SELECT ic_if");
		strSQL.append(" FROM gti_correo_notif");		
		strSQL.append(" WHERE ic_if = ? ");
		
		conditions.add(new Integer(claveIf));

		if(claveTransaccion != null && !claveTransaccion.equals("")){
			strSQL.append(" AND ic_tipo_operacion = ?");
			conditions.add(new Integer(claveTransaccion));
		}
		if(claveEstatus != null && !claveEstatus.equals("")){
			strSQL.append(" AND ic_estatus_notif = ?");
			conditions.add(new Integer(claveEstatus));
		}				
		
		log.debug("..:: strSQL : " + strSQL.toString());
		log.debug("..:: conditions : " + conditions);
		
		log.info("getDocumentQuery(F)");
		
		return strSQL.toString();
	}//getDocumentQuery
	
	/**
	* Obtiene el query necesario para mostrar la informaci�n completa de 
	* una p�gina a partir de las llaves primarias enviadas como par�metro
	* @return Cadena con la consulta de SQL, para obtener la informaci�n
	* 	completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getDocumentSummaryQueryForIds(I)");

		strSQL.append(" SELECT cn.cg_mail_cuentas_if, ");
		strSQL.append(" top.cg_descripcion, ");
		strSQL.append(" ic_estatus_notif ");
		strSQL.append(" FROM gti_correo_notif cn,");
		strSQL.append(" gticat_tipooperacion top");
		strSQL.append(" WHERE cn.ic_tipo_operacion = top.ic_tipo_operacion");
		strSQL.append(" AND (");
				
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			
			if(i > 0){strSQL.append("  OR  ");}
					
			strSQL.append("ic_if = ?");
			conditions.add(new Long(lItem.get(0).toString()));
		}
		
		strSQL.append(" ) ");

		if(claveTransaccion != null && !claveTransaccion.equals("")){
			strSQL.append(" AND top.ic_tipo_operacion = ?");
			conditions.add(new Integer(claveTransaccion));
		}
		if(claveEstatus != null && !claveEstatus.equals("")){
			strSQL.append(" AND ic_estatus_notif = ?");
			conditions.add(new Integer(claveEstatus));
		}												
		
		strSQL.append(" ORDER BY 1 DESC, 1 ASC");
		
		log.debug("..:: strSQL : " + strSQL.toString());
		log.debug("..:: conditions : " + conditions);
		
		log.info("getDocumentSummaryQueryForIds(F)");
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
	
	/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String getDocumentQueryFile(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getDocumentQueryFile(I)");
    
		strSQL.append(" SELECT cn.cg_mail_cuentas_if CUENTA, ");		
		strSQL.append(" top.cg_descripcion TANSACCION, ");
		strSQL.append(" ic_estatus_notif ESTATUS_A_NOTIFICAR");
		strSQL.append(" FROM gti_correo_notif cn,");
		strSQL.append(" gticat_tipooperacion top");
		strSQL.append(" WHERE cn.ic_tipo_operacion = top.ic_tipo_operacion");
		strSQL.append(" AND cn.ic_if = ?");
		
		conditions.add(new Integer(claveIf));

		if(claveTransaccion != null && !claveTransaccion.equals("")){
			strSQL.append(" AND cn.ic_tipo_operacion = ?");
			conditions.add(new Integer(claveTransaccion));
		}
		if(claveEstatus != null && !claveEstatus.equals("")){
			strSQL.append(" AND cn.ic_estatus_notif = ?");
			conditions.add(new Integer(claveEstatus));
		}

		strSQL.append(" ORDER BY 1 DESC");
		
		log.debug("..:: strSQL : " + strSQL.toString());
		log.debug("..:: conditions : " + conditions);
		
		log.info("getDocumentQueryFile(F)");
		return strSQL.toString();
	}//getDocumentQueryFile
	
	/***************************************************************************
	 * 																								*
	 * 									MIGRACION 												*	
	 * 																								*
	 **************************************************************************/
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo 	= new CreaArchivo();
		
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(4, 100);
				pdfDoc.setCell("No","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Transacci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus a Notificar","celda01",ComunesPDF.CENTER);
				int x = 0;
				
				while (rs.next()) {   
					String cuenta = (rs.getString("cg_mail_cuentas_if") == null) ? "" : rs.getString("cg_mail_cuentas_if");
					String transaccion  = (rs.getString("cg_descripcion") == null) ? "" : rs.getString("cg_descripcion");
					String estatus  = (rs.getString("ic_estatus_notif") == null) ? "" : rs.getString("ic_estatus_notif");
					
					pdfDoc.setCell(++x + "","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cuenta,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(transaccion,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(Integer.parseInt(estatus)==1 ? "RECHAZADA" : "AUTORIZADA IF","formas",ComunesPDF.CENTER);
					 
				}  
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
		}		
		return nombreArchivo;
	}
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {

		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		
		if ("XLS".equals(tipo)) {
			linea = "No, Cuenta, Transaccion, Estatus a Notificar\n";
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
				int x = 0;
				 
				while (rs.next()) { 
					String cg_mail_cuentas_if = (rs.getString("CUENTA") == null) ? "" : rs.getString("CUENTA");
					String cg_descripcion  = (rs.getString("TANSACCION") == null) ? "" : rs.getString("TANSACCION");
					String ic_estatus_notif  = (rs.getString("ESTATUS_A_NOTIFICAR") == null) ? "" : rs.getString("ESTATUS_A_NOTIFICAR");
					linea = ++x + ", " + cg_mail_cuentas_if.replaceAll(",","  ") +", " + cg_descripcion.replaceAll(","," ") + ", " + (Integer.parseInt(ic_estatus_notif)==1 ? "RECHAZADA" : "AUTORIZADA IF") + "\n";
					buffer.write(linea);  
				}  
				buffer.close();
			} catch (Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {} 
			}
		} else if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(4, 100);
				pdfDoc.setCell("No","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Transacci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus a Notificar","celda01",ComunesPDF.CENTER);
							
				while (rs.next()) {
					String cuenta = (rs.getString("CUENTA") == null) ? "" : rs.getString("CUENTA");
					String transaccion  = (rs.getString("TRANSACCION") == null) ? "" : rs.getString("TRANSACCION");
					String estatus  = (rs.getString("ESTATUS_A_NOTIFICAR") == null) ? "" : rs.getString("ESTATUS_A_NOTIFICAR");
					
					pdfDoc.setCell(cuenta,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(transaccion,"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.RIGHT);
					
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}
		return nombreArchivo;	
	}	
	
	//GETTERS
  /**
	 * Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions(){return conditions;}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {return paginaNo;}
	
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset(){return paginaOffset;}
	
	//Parametros del formulario
	public String getClaveIf(){return claveIf;}
	public String getClaveTransaccion(){return claveTransaccion;}
	public String getClaveEstatus(){return claveEstatus;}
	
	//SETTERS
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo){paginaNo = newPaginaNo;}
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset){this.paginaOffset=paginaOffset;}
		
	//Parametros del formulario	
	public void setClaveIf(String claveIf){this.claveIf = claveIf;}
	public void setClaveTransaccion(String claveTransaccion){this.claveTransaccion = claveTransaccion;}
	public void setClaveEstatus(String claveEstatus){this.claveEstatus = claveEstatus;}

}