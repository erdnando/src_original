package com.netro.descuento;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de B�squeda Avanzada, como aparece en el JSP: nafin-web/00utils/buscavanprov.jsp
 *
 *
 * @author Jes�s Salim Hern�ndez David
 * @since  Fodea 017 - 2011, 28/12/2011 06:23:45 p.m.
 *
 */
public class BusquedaAvanzadaI {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(BusquedaAvanzadaI.class);
	
	public static JSONArray getProveedores(
						String ic_producto_nafin, 
						String ic_epo, 
						String num_pyme, 
						String rfc_pyme, 
						String nombre_pyme, 
						String ic_pyme, 
						String pantalla,
						String ic_if,
						String ret_num_pyme) 
	throws AppException{
		
		log.info("getProveedores(E)");
 
		AccesoDB 			con 				= null;
		PreparedStatement ps					= null;
		ResultSet			rs					= null;
		
		String				condicion		= "";
		String				condicionDoc	= "";
		String				tablaDoc			= "";
		String				qrySentencia	= "";
 
		String 				campoProveedor = "0"; //Geag
		
		JSONArray 			registros 		= new JSONArray();
		JSONObject			registro 		= null;
	
		boolean descripcionConNumeroProveedor = false;
		if( "true".equals(ret_num_pyme) ){
			descripcionConNumeroProveedor = true;
		}

		try {
				
			con = new AccesoDB();
			con.conexionDB();
			
			if(!"".equals(ic_pyme)) {
				
				condicion = "";

				if(!"".equals(ic_epo)) {
					condicion 		+= "    AND r.ic_epo = ?";
					campoProveedor = 	"r.cg_pyme_epo_interno"; //Geag
				} else {
					//Si la EPO es nula, entonces el numero de proveedor no tiene sentido
					//por lo cual se pone un valor fijo para poder hacer un distinct
					campoProveedor = "0"; //Geag
				}
			
				qrySentencia = 
					" SELECT /*+index(crn CP_COMREL_NAFIN_PK) use_nl(r p)*/"   +
	//				"        p.ic_pyme, r.cg_pyme_epo_interno num_prov, p.cg_razon_social,"   +
					"        distinct p.ic_pyme, " + campoProveedor + " as num_prov, p.cg_razon_social,"   +	//Geag
					"        crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega, "   +
					"        crn.ic_nafin_electronico"   +
					"			,p.cg_rfc "  +
					"   FROM comrel_nafin crn, comrel_pyme_epo r, comcat_pyme p"   +
					"  WHERE crn.ic_epo_pyme_if = p.ic_pyme"   +
					"    AND p.ic_pyme = r.ic_pyme"   +
					"    AND crn.cg_tipo = 'P'"   +
					"    AND p.ic_pyme = ?"   +
					"    AND r.ic_pyme = ?"+condicion;

				if(!ic_if.equals("")){
					qrySentencia +=  
						" AND p.ic_pyme in ( select "  +
						"	      distinct "  +
						"		      cuenta.ic_pyme "  + 
						"     from  "  +
						"	      comrel_pyme_if rel, "  +
						"	      comrel_cuenta_bancaria cuenta "  +
						"     where "  +
						"	      rel.cs_vobo_if = 'S' and "  + 
						"	      rel.cs_borrado = 'N' and "  +
						"	      rel.ic_if      =  ?  and "  +
						"	      rel.ic_cuenta_bancaria = cuenta.ic_cuenta_bancaria and "  +
						"	cuenta.cs_borrado = 'N' ) ";
				}

				log.debug("qrySentencia(TAG1) = <"+ qrySentencia + ">");
				
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,Integer.parseInt(ic_pyme));
				ps.setInt(2,Integer.parseInt(ic_pyme));
				int cont = 2;
				if(!"".equals(ic_epo)){
					cont++;ps.setInt(cont,Integer.parseInt(ic_epo));
				}
				if(!"".equals(ic_if)){ 
					cont++;ps.setString(cont,ic_if);
				}
				
			} else {
				
				condicion = "";
			
				if(!"".equals(ic_epo)) {
					condicion 		+= "    AND pe.ic_epo = ?";
					campoProveedor = 	"pe.cg_pyme_epo_interno"; //Geag
				} else {
					//Si la EPO es nula, entonces el numero de proveedor no tiene sentido
					//por lo cual se pone un valor fijo para poder hacer un distinct
					campoProveedor = "0"; //Geag
				}
				
				if(!"".equals(num_pyme))
					condicion += "    AND cg_pyme_epo_interno LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
				if(!"".equals(rfc_pyme))
					condicion += "    AND cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
				if(!"".equals(nombre_pyme))
					condicion += "    AND cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
				if(!("2".equals(ic_producto_nafin)||"5".equals(ic_producto_nafin)) && !pantalla.equals("listReq") && !pantalla.equals("consSinNumProv")) {
					condicionDoc = 
						"    AND d.ic_pyme = pe.ic_pyme"   +
						"    AND d.ic_epo = pe.ic_epo";
					tablaDoc = "com_documento d,";
				}
			
				if( pantalla.equals("MantenimientoDoctos") || pantalla.equals("InformacionDocumentos") || pantalla.equals("CambioEstatus") ) {
					condicionDoc = 
						"		AND PE.cs_habilitado 			= 	'S' " +
						"		AND P.cs_habilitado 				= 	'S' " +
						"		AND P.cs_invalido 				!= 'S' " +
						"		AND d.ic_pyme 	= pe.ic_pyme			 " +
						"		AND pe.cs_num_proveedor 		= 'N'  " +  // Tiene CG_PYME_EPO_INTERNO
						"		AND d.ic_epo 	= pe.ic_epo";
					tablaDoc = "com_documento d,";
				}
			
				qrySentencia = 
					" SELECT "+
                    //"/*+index(pe CP_COMREL_PYME_EPO_PK) index(p CP_COMCAT_PYME_PK) use_nl(pe d p)*/ " +
	//				"        p.ic_pyme, pe.cg_pyme_epo_interno, p.cg_razon_social," +
					"        distinct p.ic_pyme, " + campoProveedor + " as cg_pyme_epo_interno, p.cg_razon_social," + //GEAG
					"        crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega," +
					"        crn.ic_nafin_electronico" +
					"			,p.cg_rfc "  +
					"   FROM comrel_pyme_epo pe, "+tablaDoc+"comrel_nafin crn, comcat_pyme p" +
					"  WHERE p.ic_pyme = pe.ic_pyme" +
					"    AND p.ic_pyme = crn.ic_epo_pyme_if" +
					condicionDoc+
					"    AND crn.cg_tipo = 'P' "+condicion;
				
				if(!pantalla.equals("listReq") && !pantalla.equals("consSinNumProv") && !pantalla.equals("MantenimientoDoctos") && !pantalla.equals("InformacionDocumentos") && !pantalla.equals("CambioEstatus") ){
				     qrySentencia += "    AND pe.cs_habilitado = 'S'"+
				     		"    AND p.cs_habilitado = 'S'"+
				     		"    AND pe.cg_pyme_epo_interno IS NOT NULL ";
				}
				if(pantalla.equals("consSinNumProv")){
					qrySentencia += 
				     		" AND pe.cg_pyme_epo_interno IS NULL "+
				     		" AND pe.cs_num_proveedor = 'S'";
				}
				
				if(!ic_if.equals("")){
					qrySentencia +=  
						" AND p.ic_pyme in ( select "  +
						"	      distinct "  +
						"		      cuenta.ic_pyme "  + 
						"     from  "  +
						"	      comrel_pyme_if rel, "  +
						"	      comrel_cuenta_bancaria cuenta "  +
						"     where "  +
						"	      rel.cs_vobo_if = 'S' and "  + 
						"	      rel.cs_borrado = 'N' and "  +
						"	      rel.ic_if      =  ?  and "  +
						"	      rel.ic_cuenta_bancaria = cuenta.ic_cuenta_bancaria and "  +
						"	cuenta.cs_borrado = 'N' ) ";
				}
				
				qrySentencia += 
					"  GROUP BY p.ic_pyme," +
					"           pe.cg_pyme_epo_interno," +
					"           p.cg_razon_social," +
					"           RPAD (pe.cg_pyme_epo_interno, 10, ' ') || p.cg_razon_social," +
					"           crn.ic_nafin_electronico" +
					"           ,p.cg_rfc " +
					"  ORDER BY p.cg_razon_social"  ;
			
				log.debug("qrySentencia(TAG2) = <"+ qrySentencia + ">");
				
				ps = con.queryPrecompilado(qrySentencia);
				int cont = 0;
				if(!"".equals(ic_epo)) {
					cont++;ps.setInt(cont,Integer.parseInt(ic_epo));
				}
				if(!"".equals(num_pyme)) {
					cont++;ps.setString(cont,num_pyme);
				}
				if(!"".equals(rfc_pyme)) {
					cont++;ps.setString(cont,rfc_pyme);
				}
				if(!"".equals(nombre_pyme)) {
					cont++;ps.setString(cont,nombre_pyme);
				}
				if(!"".equals(ic_if)) {
					cont++;ps.setString(cont,ic_if);
				}
				
			} // fin else
			
			rs = ps.executeQuery();
			int 		numeroRegistros 	= 0;
			String 	descripcion			= null;
			while(rs.next()) {
				
				numeroRegistros++;
 
				if(descripcionConNumeroProveedor) {
					descripcion = rs.getString(2) + " " + rs.getString(3);
				} else if( "2".equals(ic_producto_nafin) || "5".equals(ic_producto_nafin) ) {
					descripcion = rs.getString(3);	
				} else {
					descripcion = rs.getString(4);
				}
									
				registro = new JSONObject();
				registro.put("clave",					rs.getString(1));
				registro.put("descripcion",			descripcion);
				if( "true".equals(ret_num_pyme) ){
					registro.put("num_pyme",			rs.getString(2)); // num_prov
				}
				registro.put("nombre_pyme",			rs.getString(3)); // cg_razon_social  
				registro.put("nafin_electronico",	rs.getString(5)); // ic_nafin_electronico 
				registro.put("rfc",						rs.getString(6));
				registros.add(registro);
				
				// La b�squeda est� restringida a s�lo 1001 registros
				if(numeroRegistros > 1000){
					break;
				}
 
			}
 
			/*
			if( 			numeroRegistros > 	1000 ){
				resultado.put("mensaje","Demasiados registros encontrados, favor de ser mas espec�fico en la b�squeda");
			} else if( 	numeroRegistros == 		0 ){
				resultado.put("mensaje","No existe informaci�n con los criterios determinados");
			} 
			*/
				
		} catch(Exception e){
		
			log.error("getProveedores(Exception)");
			log.error("ic_producto_nafin = <" + ic_producto_nafin + ">");
			log.error("ic_epo            = <" + ic_epo            + ">");
			log.error("num_pyme          = <" + num_pyme          + ">"); 
			log.error("rfc_pyme          = <" + rfc_pyme          + ">"); 
			log.error("nombre_pyme       = <" + nombre_pyme       + ">"); 
			log.error("ic_pyme           = <" + ic_pyme           + ">");
			log.error("pantalla          = <" + pantalla          + ">");
			log.error("ic_if             = <" + ic_if             + ">");
			log.error(e);
			throw new AppException( "Ocurri� un error al realizar la b�squeda de los proveedores: " + e.getMessage() );
			
		} finally {
		
			if(rs != null){ try { rs.close();}catch(Exception e){} }
			if(ps != null){ try { ps.close();}catch(Exception e){} }
				
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		
			log.info("getProveedores(S)");
			
		}
		
		return registros;
		
	}
	
}