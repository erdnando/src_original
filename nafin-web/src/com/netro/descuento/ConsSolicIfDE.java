package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.security.MessageDigest;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CQueryHelperRegExtJS;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsSolicIfDE  implements IQueryGeneratorPS,  IQueryGeneratorRegExtJS {


	StringBuffer 		qrySentencia;
	StringBuffer hint;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsSolicIfDE .class);

	private int numList = 1;
	private String ic_if;
	private String ic_epo;
	private String ic_pyme;
	private String ic_folio;
	private String df_fecha_solicitudMin;
	private String df_fecha_solicitudMax;
	private String df_fecha_vencMin;
	private String df_fecha_vencMax;
	private String ic_estatus_solic;
	private String ic_moneda;
	private String fn_montoMin;
	private String fn_montoMax;
	private String tipoFactoraje;
	private String tipoTasa;
	private String tipoArchivo;
	private String bTipoFactoraje;
	private String bOperaFactorajeDist;
	private String bOperaFactorajeVencINFO;
	private String bOperaFactorajeAutomatico;
	private String bOperaPubHash;
	private String ordenadoBy;
	private String aplicaFloating;
	
	public ConsSolicIfDE() { }

	public int getNumList(HttpServletRequest request){
		return numList;
	}

	public ArrayList getConditions(HttpServletRequest request){
		log.info("..:: ConsSolicIfDE - getConditions(I)");
		ArrayList conditions = new ArrayList();

		// Este metodo generara el query de los totales
		StringBuffer query = new StringBuffer("");
		StringBuffer condicion = new StringBuffer("");
		String criterioOrdenamiento=" group by d.ic_moneda,'ConsSolicIfDE.java' order by d.ic_moneda ";
		// Parametros de la pagina
		String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
		String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
		String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
		String ic_folio = (request.getParameter("ic_folio") == null)?"":request.getParameter("ic_folio");
		String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") == null)?"":request.getParameter("df_fecha_solicitudMin");
		String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") == null)?"":request.getParameter("df_fecha_solicitudMax");
		String df_fecha_vencMin = (request.getParameter("df_fecha_vencMin") == null)?"":request.getParameter("df_fecha_vencMin");
		String df_fecha_vencMax = (request.getParameter("df_fecha_vencMax") == null)?"":request.getParameter("df_fecha_vencMax");
		String ic_estatus_solic = (request.getParameter("ic_estatus_solic") == null)?"":request.getParameter("ic_estatus_solic");
		String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
		String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
		String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
		// Generacion del query

		if (ic_epo.equals("")	&& ic_estatus_solic.equals("") && ic_moneda.equals("")
			&& df_fecha_vencMin.equals("") && df_fecha_solicitudMin.equals("")
			&& fn_montoMin.equals("") && ic_folio.equals("")) {//ACF
				df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}

		if (!ic_if.equals(""))
			conditions.add(new Integer(ic_if));

		if (!ic_epo.equals(""))
			conditions.add(new Integer(ic_epo));

		if (!ic_pyme.equals(""))
			conditions.add(new Integer(ic_pyme));

		if (!ic_moneda.equals(""))
			conditions.add(new Integer(ic_moneda));

		if(!df_fecha_vencMin.equals("")) {
			if(!df_fecha_vencMax.equals("")) {
				//condicion.append(" and s.DF_V_DESCUENTO between TO_DATE('"+df_fecha_vencMin+"','dd/mm/yyyy') and TO_DATE('"+df_fecha_vencMax+"','dd/mm/yyyy') ");
				conditions.add(df_fecha_vencMin);
				conditions.add(df_fecha_vencMax);
			} else {
				//condicion.append(" and s.DF_V_DESCUENTO = TO_DATE('"+df_fecha_vencMin+"','dd/mm/yyyy') ");
				conditions.add(df_fecha_vencMin);
				conditions.add(df_fecha_vencMin);
			}
		}

		if(!fn_montoMin.equals("")) {
			if(!fn_montoMax.equals("")) {
				//condicion.append(" and d.fn_monto between "+fn_montoMin+" and "+fn_montoMax);
				conditions.add(new Double(fn_montoMin));
				conditions.add(new Double(fn_montoMax));
			} else {
				//condicion.append(" and d.fn_monto = "+fn_montoMin);
				conditions.add(new Double(fn_montoMin));
			}
		}

		if (!ic_estatus_solic.equals(""))
			conditions.add(new Integer(ic_estatus_solic));

		if (!ic_folio.equals(""))
			conditions.add(ic_folio);

		if(!df_fecha_solicitudMin.equals("")) {
			if(!df_fecha_solicitudMax.equals("")) {
				//condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE('"+df_fecha_solicitudMin+"','dd/mm/yyyy') and TO_DATE('"+df_fecha_solicitudMax+"','dd/mm/yyyy') ");
				conditions.add(df_fecha_solicitudMin);
				conditions.add(df_fecha_solicitudMax);
			} else {
				//condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('"+df_fecha_solicitudMin+"','dd/mm/yyyy') ");
				conditions.add(df_fecha_solicitudMin);
				conditions.add(df_fecha_solicitudMin);
			}
		}

		if(!tipoFactoraje.equals(""))
			conditions.add(tipoFactoraje);

		log.info("conditions:: " + conditions);
		log.info("..:: ConsSolicIfDE - getConditions(F)");
		return conditions;
	}

	public String getAggregateCalculationQuery(HttpServletRequest request){
		log.info("..:: ConsSolicIfDE - getAggregateCalculationQuery(I)");
		// Este metodo generara el query de los totales
		StringBuffer query = new StringBuffer("");
		StringBuffer condicion = new StringBuffer("");
		String hint = "";
		String criterioOrdenamiento=" group by d.ic_moneda,'ConsSolicIfDE.java' order by d.ic_moneda ";
		// Parametros de la pagina
		String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
		String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
		String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
		String ic_folio = (request.getParameter("ic_folio") == null)?"":request.getParameter("ic_folio");
		String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") == null)?"":request.getParameter("df_fecha_solicitudMin");
		String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") == null)?"":request.getParameter("df_fecha_solicitudMax");
		String df_fecha_vencMin = (request.getParameter("df_fecha_vencMin") == null)?"":request.getParameter("df_fecha_vencMin");
		String df_fecha_vencMax = (request.getParameter("df_fecha_vencMax") == null)?"":request.getParameter("df_fecha_vencMax");
		String ic_estatus_solic = (request.getParameter("ic_estatus_solic") == null)?"":request.getParameter("ic_estatus_solic");
		String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
		String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
		String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
		String tipoTasa = (request.getParameter("tipoTasa") == null) ? "" : request.getParameter("tipoTasa");
		log.info("tipoTasa******************* "+tipoTasa);
		// Generacion del query
		numList = 1;
		if(!tipoTasa.equals("")) {
			condicion.append(" and ds.CG_TIPO_TASA ='"+tipoTasa+"'");
		}

		if (ic_epo.equals("")	&& ic_estatus_solic.equals("") && ic_moneda.equals("")
			&& df_fecha_vencMin.equals("") && df_fecha_solicitudMin.equals("")
			&& fn_montoMin.equals("") && ic_folio.equals("")) {//ACF
			df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}

		if (!ic_epo.equals(""))
			condicion.append( " and d.ic_epo = ? ");

		if (!ic_pyme.equals(""))
			condicion.append( " and d.ic_pyme = ? ");

		if (!ic_moneda.equals("")){
			condicion.append(" and d.ic_moneda = ? ");
			//hint += " d IN_COM_DOCUMENTO_05_NUK," ;
		}

		if(!df_fecha_vencMin.equals("")) {
			if(!df_fecha_vencMax.equals("")) {
				//condicion.append(" and s.DF_V_DESCUENTO between TO_DATE(?,'dd/mm/yyyy') and TO_DATE(?,'dd/mm/yyyy')+1 ");
				condicion.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
			} else {
				//condicion.append(" and s.DF_V_DESCUENTO = TO_DATE(?,'dd/mm/yyyy') ");
				condicion.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
			}
			hint += " s IN_COM_SOLICITUD_14_NUK," ;
		}

		if(!fn_montoMin.equals("")) {
			if(!fn_montoMax.equals("")) {
				condicion.append(" and d.fn_monto between ? and ? ");
			}
			else {
				condicion.append(" and d.fn_monto = ? ");
			}
		}

		if (!ic_estatus_solic.equals("")){
			condicion.append(" and s.ic_estatus_solic = ? ");
			//hint += " s IN_COM_SOLICITUD_02_NUK," ;
		}

		if (!ic_folio.equals(""))
			condicion.append(" and s.ic_folio = ? ");

		if(!df_fecha_solicitudMin.equals("")) {
			if(!df_fecha_solicitudMax.equals("")) {
				//condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE(?,'dd/mm/yyyy') and TO_DATE(?,'dd/mm/yyyy') ");
				condicion.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and  s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
			} else {
				//condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE(?,'dd/mm/yyyy') ");
				condicion.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and  s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
			}
			hint += " s IN_COM_SOLICITUD_13_NUK," ;
		}

		if(!tipoFactoraje.equals("")) {
			condicion.append(" and d.CS_DSCTO_ESPECIAL= ? ");
		} else {
			condicion.append(" and d.CS_DSCTO_ESPECIAL!='C' ");//new
		}

		if(!"".equals(hint) && hint != null)
		{
				hint = " index( " + hint.substring(0,hint.length()-1) + ")";
		}

			//query.append(" select /*+ ordered use_nl(s,d,pe) index(ds IN_COM_DOCTO_SELEC_03_NUK)*/ "+
		query.append(
			" select /*+ ordered " + hint + " use_nl(s ds d pe e prodepo) */ "+//ACF
			"     d.ic_moneda CVE_MONEDA,"   +
			" 	   count(ds.ic_documento) TOTAL_DOCTOS, "   +
			" 	   sum(nvl(d.fn_monto,0)) MONTO_DOCTOS, "   +
			" 	   sum(nvl(d.fn_monto_dscto,0)) MONTO_DESC_DOCTOS, "   +
			" 	   sum(nvl(ds.in_importe_recibir,0)) MONTO_RECIBIR_DOCTOS, "   +
			" 	   sum(decode(s.ic_estatus_solic,1,1,2,1,0)) TOTAL_SOLIC_X_AUTO, "   +
			" 	   sum(decode(s.ic_estatus_solic,3,1,10,1,0)) TOTAL_SOLIC_OPER, "   +
			" 	   sum(decode(s.ic_estatus_solic,4,1,0)) TOTAL_SOLIC_RECH,      "   +
			//" 	   sum(decode(s.ic_estatus_solic,1,nvl(ds.in_importe_recibir,0),2,nvl(ds.in_importe_recibir,0),0)) MONTO_SOLIC_X_AUTO, "   +	//linea a modificar por FODEA 17
			"		sum(decode(ds.cs_opera_fiso, 'S', nvl(DECODE (s.ic_estatus_solic,1, NVL (ds.in_importe_recibir, 0),2, NVL (ds.in_importe_recibir, 0),0),0), 'N', decode(s.ic_estatus_solic,1,nvl(ds.in_importe_recibir,0),2,nvl(ds.in_importe_recibir,0),0))) MONTO_SOLIC_X_AUTO,	"	+ //FODEA 17
			//" 	   sum(decode(s.ic_estatus_solic,3,nvl(ds.in_importe_recibir,0),10,nvl(ds.in_importe_recibir,0),0)) MONTO_SOLIC_OPER, "   +	//Linea a modificar por FODEA 17
			"		sum(decode(ds.cs_opera_fiso, 'S', nvl(DECODE (s.ic_estatus_solic,3, NVL (ds.in_importe_recibir, 0),10, NVL (ds.in_importe_recibir, 0),0), 0), 'N', decode(s.ic_estatus_solic,3,nvl(ds.in_importe_recibir,0),10,nvl(ds.in_importe_recibir,0),0)))	MONTO_SOLIC_OPER, "	+ //FODEA 17
			//" 	   sum(decode(s.ic_estatus_solic,4,nvl(ds.in_importe_recibir,0),0)) MONTO_SOLIC_RECH,    "+	//Linea a modificar por FODEA 17
			"		sum(decode(ds.cs_opera_fiso, 'S', nvl(DECODE (s.ic_estatus_solic,4, NVL (ds.in_importe_recibir, 0),0), 0), 'N', decode(s.ic_estatus_solic,4,nvl(ds.in_importe_recibir,0),0))) MONTO_SOLIC_RECH,	"	+ //FODEA 17
			" 	   sum(nvl(d.fn_monto,0)-nvl(d.fn_monto_dscto,0)) MONTO_RECURSO, "   +
			" 	   sum(nvl(ds.in_importe_interes,0)) MONTO_INTERES,'ConsSolicIfDE::getAggregateCalculationQuery' ORIGENQUERY "  +
			" from "   +
			" 	com_solicitud s, "   +
			" 	com_docto_seleccionado ds, "   +
			" 	com_documento d "   +
			" where s.ic_documento=ds.ic_documento and "   +
			" 	ds.ic_documento=d.ic_documento and "   +
			" 	d.ic_epo = ds.ic_epo and " +//ACF
			" 	d.ic_if=? "   + //Modificado por fodea 17 antes ds.ic_if
			//" 	s.cs_tipo_solicitud='C' " +
			condicion.toString());

		query.append(criterioOrdenamiento);
		log.info("..:: query : "+query.toString());
		log.info("..:: ConsSolicIfDE - getAggregateCalculationQuery(F)");
		return query.toString();
	}

 	public String getDocumentQuery(HttpServletRequest request){
		log.info("..:: ConsSolicIfDE - getDocumentQuery(I)");
		//log.info("********Variables bind con interfaz IQueryGeneratorPS ");
		// Genera el query que devuelve las llaves primarias de la consulta
		StringBuffer query = new StringBuffer("");
		StringBuffer condicion = new StringBuffer("");
		String criterioOrdenamiento=" ORDER BY D.IC_DOCUMENTO ";
		// Parametros de la pagina
		String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
		String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
		String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
		String ic_folio = (request.getParameter("ic_folio") == null)?"":request.getParameter("ic_folio");
		String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") == null)?"":request.getParameter("df_fecha_solicitudMin");
		String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") == null)?"":request.getParameter("df_fecha_solicitudMax");
		String df_fecha_vencMin = (request.getParameter("df_fecha_vencMin") == null)?"":request.getParameter("df_fecha_vencMin");
		String df_fecha_vencMax = (request.getParameter("df_fecha_vencMax") == null)?"":request.getParameter("df_fecha_vencMax");
		String ic_estatus_solic = (request.getParameter("ic_estatus_solic") == null)?"":request.getParameter("ic_estatus_solic");
		String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
		String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
		String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
		String hint = "";
		String tipoTasa = (request.getParameter("tipoTasa") == null) ? "" : request.getParameter("tipoTasa");

		if(!tipoTasa.equals("")) {
			condicion.append(" and ds.CG_TIPO_TASA ='"+tipoTasa+"'");
		}

		numList = 1;
			if (ic_epo.equals("")	&& ic_estatus_solic.equals("") && ic_moneda.equals("")
				&& df_fecha_vencMin.equals("") && df_fecha_solicitudMin.equals("")
				&& fn_montoMin.equals("") && ic_folio.equals("")) {//ACF
				df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
			}

			if (!ic_epo.equals(""))
				condicion.append( " and d.ic_epo = ? ");

			if (!ic_pyme.equals(""))
				condicion.append( " and d.ic_pyme = ? ");

			if (!ic_moneda.equals("")){
				condicion.append(" and d.ic_moneda = ? ");
				//hint += " d IN_COM_DOCUMENTO_05_NUK," ;
			}

			if(!df_fecha_vencMin.equals("")) {
				if(!df_fecha_vencMax.equals("")) {
					//condicion.append(" and s.DF_V_DESCUENTO between TO_DATE(?,'dd/mm/yyyy') and TO_DATE(?,'dd/mm/yyyy') ");
					condicion.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
				} else {
					//condicion.append(" and s.DF_V_DESCUENTO = TO_DATE(?,'dd/mm/yyyy') ");
					condicion.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
				}
				hint += " s IN_COM_SOLICITUD_14_NUK," ;
			}

			if(!fn_montoMin.equals("")) {
				if(!fn_montoMax.equals("")) {
					condicion.append(" and d.fn_monto between ? and ? ");
				} else {
					condicion.append(" and d.fn_monto = ? ");
				}
			}

			if (!ic_estatus_solic.equals("")){
				condicion.append(" and s.ic_estatus_solic = ? ");
				//hint += " s IN_COM_SOLICITUD_02_NUK," ;
			}

			if (!ic_folio.equals(""))
				condicion.append(" and s.ic_folio = ? ");

			if(!df_fecha_solicitudMin.equals("")) {
				if(!df_fecha_solicitudMax.equals("")) {
					//condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE(?,'dd/mm/yyyy') and TO_DATE(?,'dd/mm/yyyy') ");
					condicion.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
				} else {
					//condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE(?,'dd/mm/yyyy') ");
					condicion.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
				}
				hint += " s IN_COM_SOLICITUD_13_NUK," ;
			}

			if(!tipoFactoraje.equals("")) {
				condicion.append(" and d.CS_DSCTO_ESPECIAL=  ? ");
			} else {
				condicion.append(" and d.CS_DSCTO_ESPECIAL!='C' "); //
			}

			if(!"".equals(hint) && hint != null)
			{
				hint = " index( " + hint.substring(0,hint.length()-1) + ")";
			}

			query.append(
				" select /*+ ordered " + hint + " use_nl(d,s) */ "+//ACF
				" d.ic_documento "+
				" ,'ConsSolicIfDE::getDocumentQuery' Origen " +
				" from "   +
				" com_solicitud s, " +
				" com_documento d" +
				" ,com_docto_seleccionado ds "+
				" where s.ic_documento=d.ic_documento  "   +
				" and s.ic_documento = ds.ic_documento "+
				"	AND ds.ic_documento = d.ic_documento" +
				" and	d.ic_if = ?  "   + condicion.toString());

				//" 	and s.cs_tipo_solicitud='C' " +

		query.append(criterioOrdenamiento);
		log.info("..:: query : "+query.toString());
		log.info("..:: ConsSolicIfDE - getDocumentQuery(F)");
		return query.toString();
	}

	public String getDocumentQueryFile(HttpServletRequest request){
		log.info("..:: ConsSolicIfDE - getDocumentQueryFile(I)");
		// Genera el query que devuelve toda la informacion de la consulta
		StringBuffer query = new StringBuffer("");
		StringBuffer condicion = new StringBuffer("");
		StringBuffer condicionAux = new StringBuffer("");
		StringBuffer condOperamandato = new StringBuffer("");
		StringBuffer campoOperamandato = new StringBuffer("");
		StringBuffer tabOperamandato = new StringBuffer("");
		String criterioOrdenamiento="";

		// Parametros de la pagina
		String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
		String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
		String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
		String ic_folio = (request.getParameter("ic_folio") == null)?"":request.getParameter("ic_folio");
		String df_fecha_solicitudMin = (request.getParameter("df_fecha_solicitudMin") == null)?"":request.getParameter("df_fecha_solicitudMin");
		String df_fecha_solicitudMax = (request.getParameter("df_fecha_solicitudMax") == null)?"":request.getParameter("df_fecha_solicitudMax");
		String df_fecha_vencMin = (request.getParameter("df_fecha_vencMin") == null)?"":request.getParameter("df_fecha_vencMin");
		String df_fecha_vencMax = (request.getParameter("df_fecha_vencMax") == null)?"":request.getParameter("df_fecha_vencMax");
		String ic_estatus_solic = (request.getParameter("ic_estatus_solic") == null)?"":request.getParameter("ic_estatus_solic");
		String ic_moneda = (request.getParameter("ic_moneda") == null)?"":request.getParameter("ic_moneda");
		String fn_montoMin = (request.getParameter("fn_montoMin") == null)?"":request.getParameter("fn_montoMin");
		String fn_montoMax = (request.getParameter("fn_montoMax") == null)?"":request.getParameter("fn_montoMax");
		String ordenaEpo = (request.getParameter("ordenaEpo") == null)?"":request.getParameter("ordenaEpo");
		String ordenaPyme = (request.getParameter("ordenaPyme") == null)?"":request.getParameter("ordenaPyme");
		String ordenaFecha = (request.getParameter("ordenaFecha") == null)?"":request.getParameter("ordenaFecha");
		String ordenaMonto = (request.getParameter("ordenaMonto") == null)?"":request.getParameter("ordenaMonto");
		String ordenaEstatus = (request.getParameter("ordenaEstatus") == null)?"":request.getParameter("ordenaEstatus");
		String ordenaFolio = (request.getParameter("ordenaFolio") == null)?"":request.getParameter("ordenaFolio");
		String tipoFactoraje = (request.getParameter("tipoFactoraje") == null) ? "" : request.getParameter("tipoFactoraje");
		String tipoArchivo = (request.getParameter("tipoArchivo") == null) ? "" : request.getParameter("tipoArchivo");
		String campos[]={"nombreEpo","nombrePyme","TO_DATE(df_fecha_venc,'DD/MM/YYYY')","fn_monto","cd_descripcion","folio"};
		String orden[]={ordenaEpo,ordenaPyme,ordenaFecha,ordenaMonto,ordenaEstatus,ordenaFolio};
		String hint = "";
		String tipoTasa = (request.getParameter("tipoTasa") == null) ? "" : request.getParameter("tipoTasa");


		numList = 1;
    	// Generacion del query
		if(!tipoTasa.equals("")) {
			condicion.append(" and ds.CG_TIPO_TASA ='"+tipoTasa+"'");
		}
		if (ic_epo.equals("")	&& ic_estatus_solic.equals("") && ic_moneda.equals("")
				&& df_fecha_vencMin.equals("") && df_fecha_solicitudMin.equals("")
				&& fn_montoMin.equals("") && ic_folio.equals("") ) {
			df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}

//_____________________________________ Criterios de ORDENAMIENTO
		criterioOrdenamiento=Comunes.ordenarCampos(orden,campos);

		if (!criterioOrdenamiento.equals(""))
			criterioOrdenamiento=" order by "+criterioOrdenamiento;

//__________________________________ Fin de Criterios de ORDENAMIENTO

		if (!ic_epo.equals(""))
			condicion.append( " and d.ic_epo = ? ");

		if (!ic_pyme.equals(""))
			condicion.append( " and d.ic_pyme = ? ");

		if (!ic_moneda.equals("")){
			condicion.append(" and d.ic_moneda = ? ");
			//hint += " d IN_COM_DOCUMENTO_05_NUK," ;
		}


		if(!df_fecha_vencMin.equals("")) {
			if(!df_fecha_vencMax.equals("")) {
				//condicion.append(" and s.DF_V_DESCUENTO between TO_DATE(?,'dd/mm/yyyy') and TO_DATE(?,'dd/mm/yyyy') ");
				condicion.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
			}
			else {
				//condicion.append(" and s.DF_V_DESCUENTO = TO_DATE(?,'dd/mm/yyyy') ");
				condicion.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
			}
			hint += " s IN_COM_SOLICITUD_14_NUK," ;
		}

		if(!fn_montoMin.equals("")) {
			if(!fn_montoMax.equals("")) {
				condicion.append(" and d.fn_monto between ? and ? ");
			}
			else {
				condicion.append(" and d.fn_monto = ? ");
			}
		}

		if (!ic_estatus_solic.equals("")) {
			condicion.append(" and s.ic_estatus_solic = ? ");
			//hint += " s IN_COM_SOLICITUD_02_NUK," ;
		}

		if (!ic_folio.equals("")) {
			condicion.append(" and s.ic_folio = ? ");
		}


		if(!df_fecha_solicitudMin.equals("")) {
			if(!df_fecha_solicitudMax.equals("")) {
				//condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') between TO_DATE(?,'dd/mm/yyyy') and TO_DATE(?,'dd/mm/yyyy') ");
				condicion.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
			}
			else {
				//condicion.append(" and TO_DATE(TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE(?,'dd/mm/yyyy') ");
				condicion.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
			}
			hint += " s IN_COM_SOLICITUD_13_NUK," ;
		}
		if(!tipoFactoraje.equals("")) {
			condicionAux.append(" and d.CS_DSCTO_ESPECIAL= ? ");
		} else {
			condicionAux.append(" and d.CS_DSCTO_ESPECIAL!='C' ");//new
		}

		if(!"".equals(hint) && hint != null)
			{
				hint = " index( " + hint.substring(0,hint.length()-1) + ")";
			}

		query.append(
			" SELECT   /*+ ordered " + hint + " use_nl(s ds d pe p ie e i ci m es prodepo tf) */"   +//ACF
			" 	   'Interna' AS tipo, s.ic_folio AS folio,"   +
			//"        (DECODE (pe.cs_habilitado, 'N', '* ', 'S', '  ')  || p.cg_razon_social) AS nombrepyme,"   + //Linea a modificar por FODEA 17
			" , decode(ds.cs_opera_fiso, 'S', i2.cg_razon_social , 'N', (decode(pe.cs_habilitado,'N','*','S','')||' '||p.cg_razon_social)) AS nombrepyme"+ //FODEA 17
			"        (DECODE (e.cs_habilitado, 'N', '* ', 'S', '  ')  || e.cg_razon_social) AS nombreepo,"   +
			"        d.ig_numero_docto, TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS df_fecha_docto,"   +
			"        TO_CHAR (s.df_v_descuento, 'dd/mm/yyyy') AS df_fecha_venc, es.ic_estatus_solic, d.ic_moneda, m.cd_nombre,"   +
			"        d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto,"+
			//"			ds.in_importe_interes,"   + //Linea a modificar por FODEA 17
			" , decode(ds.cs_opera_fiso, 'S', ds.in_importe_interes_fondeo, 'N', ds.in_importe_interes ) as INTERES"+	//FODEA 17
			"        TO_CHAR (s.df_fecha_solicitud, 'dd/mm/yyyy') AS df_fecha_solicitud,"+
			//"			ds.in_tasa_aceptada, s.ig_plazo,"   + //Linea a modificar por FODEA 17
			"  decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada  ) as TASA_PYME, s.ig_plazo,"+	//FODEA 17
			//"        ds.in_importe_recibir AS montooperar,"+ //Linea a modificar por FODEA 17
			"  decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir  ) AS montooperar "+	//FODEA 17
			"			es.cd_descripcion, s.ic_folio, s.cc_acuse, s.ic_oficina,"   +
			"        d.ic_pyme, p.in_numero_sirac, p.cg_razon_social AS razon_pyme, d.ic_epo, d.ic_if,"   +
			"        i.cg_razon_social AS razon_if, TO_CHAR (d.df_alta, 'dd/mm/yyyy') AS df_alta,"   +
			"        TO_CHAR (s.df_fecha_solicitud, 'dd/mm/yyyy') AS df_fecha_solicitud, s.ic_tasaif,"   +
			"        TO_CHAR (s.df_operacion, 'dd/mm/yyyy') AS df_operacion, s.ig_numero_prestamo, s.cg_causa_rechazo,"   +
			"        d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, d.cg_campo5, d.cs_dscto_especial,"   +
			"        s.ig_numero_prestamo, TO_CHAR (s.df_operacion, 'DD/MM/YYYY') AS df_operacion, d.ct_referencia,"   +
			"        pe.cg_pyme_epo_interno, s.fg_porc_comision_fondeo AS porcentaje_comision,"   +
			"        ds.in_importe_recibir * (s.fg_porc_comision_fondeo / 100) AS monto_comision,"   +
			"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ci.cg_razon_social, '') AS benef,"   +
			"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ie.cg_banco, '') AS banco_benef,"   +
			"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ie.ig_sucursal, '') AS sucursal_benef,"   +
			"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ie.cg_num_cuenta, '') AS cta_benef,"   +
			"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , d.fn_porc_beneficiario, '') AS porc_benef,"   +
			"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ds.fn_importe_recibir_benef, '') AS importe_recibir,"   +
			"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '') AS neto_rec_pyme,"   +
			"        TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fechavencdoc "+
			"		 ,d.fn_monto_ant, '' as ic_docto_asociado"+
			"		 ,s.fg_tasa_fondeo_nafin"+
			"      ,tf.cg_nombre AS TIPO_FACTORAJE " +
			"        ,'ConsSolicIfDE::getDocumentQueryFile' origenquery"   +
			"  , decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as tipoTasa "+

			"   FROM com_solicitud s,"   +
			"        com_docto_seleccionado ds,"   +
			"        com_documento d,"   +
			"        comrel_pyme_epo pe,"   +
			"        comcat_pyme p,"   +
			"        comrel_if_epo ie,"   +
			"        comcat_epo e,"   +
			"        comcat_if i,"   +
			"        comcat_if i2,"   + //FODEA 17
			"        comcat_if ci,"   +
			"        comcat_moneda m,"   +
			"        comcat_estatus_solic es,"+
			" 			comrel_producto_epo prodepo,"+
			"        comcat_tipo_factoraje tf "+
			"  WHERE s.ic_documento = ds.ic_documento"   +
			"    AND ds.ic_documento = d.ic_documento"   +
			"    AND d.ic_pyme = p.ic_pyme"   +
			"    AND d.ic_epo = prodepo.ic_epo " +
			"    AND prodepo.ic_producto_nafin = 1 " +
			//"    AND (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) " +
			"    AND ds.ic_epo = e.ic_epo"   +
			"    AND d.ic_if = i.ic_if"   +
			"    AND ds.ic_if = i2.ic_if"   + //FODEA 17
			"    AND d.ic_moneda = m.ic_moneda"   +
			"    AND d.ic_pyme = pe.ic_pyme"   +
			"    AND d.ic_epo = pe.ic_epo"   +
			"    AND	d.ic_epo = ds.ic_epo" +//ACF
			"    AND s.ic_estatus_solic = es.ic_estatus_solic"   +
			"    AND ie.ic_if (+) = d.ic_beneficiario"   +
			"    AND ie.ic_epo (+) = d.ic_epo"   +
			"    AND ie.ic_if = ci.ic_if (+)"   +
			"    AND s.cs_tipo_solicitud = 'C'"   +
			" 	  AND e.cs_habilitado = 'S' " +
			"    AND ds.ic_if = ? "+
			"    and d.cs_dscto_especial = tf.cc_tipo_factoraje "	+
			condicion.toString()+
			condicionAux.toString()	);

			if(tipoArchivo.equals("IVARIABLED")) {
				numList = 2;
				query.append(
					" UNION ALL "   +
					" SELECT   /*+ ordered use_nl(s ds d d2 pe p ie e i ci m es prodepo tf) */"   +//ACF
					" 	   'Interna' AS tipo, s.ic_folio AS folio,"   +
					//"        (DECODE (pe.cs_habilitado, 'N', '*', 'S', '') || '' || p.cg_razon_social) AS nombrepyme,"   + //Linea a modificar por FODEA 17
					" 			decode(ds.cs_opera_fiso, 'S', i2.cg_razon_social , 'N', (decode(pe.cs_habilitado,'N','*','S','')||' '||p.cg_razon_social)) AS nombrepyme, "+ //FODEA 17
					"        (DECODE (e.cs_habilitado, 'N', '*', 'S', '') || '' || e.cg_razon_social) AS nombreepo,"   +
					"        d.ig_numero_docto, TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS df_fecha_docto,"   +
					"        TO_CHAR (s.df_v_descuento, 'dd/mm/yyyy') AS df_fecha_venc, es.ic_estatus_solic, d.ic_moneda, m.cd_nombre,"   +
					"        d.fn_monto, d.fn_porc_anticipo, d.fn_monto_dscto,"+
					//"			ds.in_importe_interes,"   + //Linea a modificar por FODEA 17
					"  decode(ds.cs_opera_fiso, 'S', ds.in_importe_interes_fondeo, 'N', ds.in_importe_interes ) as INTERES ,"+	//FODEA 17
					"        TO_CHAR (s.df_fecha_solicitud, 'dd/mm/yyyy') AS df_fecha_solicitud,"+
					//"	ds.in_tasa_aceptada, s.ig_plazo,"   + //Linea a modificar por FODEA 17
					"  decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada  ) as TASA_PYME, s.ig_plazo,"+	//FODEA 17
					//"        ds.in_importe_recibir AS montooperar,"+ //Linea a modificar por FODEA 17
					"  decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir  ) AS montooperar,, "+	//FODEA 17
					"	es.cd_descripcion, s.ic_folio, s.cc_acuse, s.ic_oficina,"   +
					"        d.ic_pyme, p.in_numero_sirac, p.cg_razon_social AS razon_pyme, d.ic_epo, d.ic_if,"   +
					"        i.cg_razon_social AS razon_if, TO_CHAR (d.df_alta, 'dd/mm/yyyy') AS df_alta,"   +
					"        TO_CHAR (s.df_fecha_solicitud, 'dd/mm/yyyy') AS df_fecha_solicitud, s.ic_tasaif,"   +
					"        TO_CHAR (s.df_operacion, 'dd/mm/yyyy') AS df_operacion, s.ig_numero_prestamo, s.cg_causa_rechazo,"   +
					"        d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, d.cg_campo5, d.cs_dscto_especial,"   +
					"        s.ig_numero_prestamo, TO_CHAR (s.df_operacion, 'DD/MM/YYYY') AS df_operacion, d.ct_referencia,"   +
					"        pe.cg_pyme_epo_interno, s.fg_porc_comision_fondeo AS porcentaje_comision,"   +
					"        ds.in_importe_recibir * (s.fg_porc_comision_fondeo / 100) AS monto_comision,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ci.cg_razon_social, '') AS benef,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ie.cg_banco, '') AS banco_benef,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ie.ig_sucursal, '') AS sucursal_benef,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ie.cg_num_cuenta, '') AS cta_benef,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , d.fn_porc_beneficiario, '') AS porc_benef,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ds.fn_importe_recibir_benef, '') AS importe_recibir,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '') AS neto_rec_pyme,"   +
					"        TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fechavencdoc "+
					"		 ,d.fn_monto_ant, d2.ig_numero_docto as ic_docto_asociado"+
					"		 ,s.fg_tasa_fondeo_nafin"+
					"      ,tf.cg_nombre AS TIPO_FACTORAJE " +
					"        ,'ConsSolicIfDE::getDocumentQueryFile' origenquery"   +
					"  , decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as tipoTasa "+
					"   FROM com_solicitud s,"   +
					"        com_documento d,"   +
					"        com_documento d2,"   +
					"        com_docto_seleccionado ds,"   +
					"        comrel_pyme_epo pe,"   +
					"        comcat_pyme p,"   +
					"        comrel_if_epo ie,"   +
					"        comcat_epo e,"   +
					"        comcat_if i,"   +
					"        comcat_if i2,"   + //FODEA 17
					"        comcat_if ci,"   +
					"        comcat_moneda m,"   +
					"        comcat_estatus_solic es, comrel_producto_epo prodepo, " +
					"        comcat_tipo_factoraje tf "+
					"  WHERE s.ic_documento = d.ic_docto_asociado"   +
					"    AND ds.ic_documento = d.ic_documento"   +
					"    AND d2.ic_documento = d.ic_docto_asociado"   +
					"    AND d.ic_pyme = p.ic_pyme"   +
					"    AND ds.ic_epo = e.ic_epo"   +
					"    AND d.ic_if = i.ic_if"   +
					"    AND ds.ic_if = i2.ic_if"   + //FODEA 17
					"    AND d.ic_moneda = m.ic_moneda"   +
					"    AND d.ic_pyme = pe.ic_pyme"   +
					"    AND d.ic_epo = pe.ic_epo"   +
					"    AND d.ic_epo = prodepo.ic_epo " +
					"    AND	d.ic_epo = ds.ic_epo" +//ACF
					"    AND prodepo.ic_producto_nafin = 1 " +
					//"    AND (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad = 1) " +
					"    AND s.ic_estatus_solic = es.ic_estatus_solic"   +
					"    AND ie.ic_if (+) = d.ic_beneficiario"   +
					"    AND ie.ic_epo (+) = d.ic_epo"   +
					"    AND ie.ic_if = ci.ic_if (+)"   +
					"    AND s.cs_tipo_solicitud = 'C'"   +
					" 	  AND e.cs_habilitado = 'S' " +
					"    AND d.ic_if = ? "+  //era ds.ic_if cambiado FODEA 17
					"    and d.cs_dscto_especial = tf.cc_tipo_factoraje "	+
					condicion.toString()+
					condicionAux.toString());
			}

		query.append(criterioOrdenamiento);
		log.info("..:: query : "+query.toString());
		log.info("..:: ConsSolicIfDE - getDocumentQueryFile(F)");
     	return query.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
		log.info("..:: ConsSolicIfDE - getDocumentSummaryQueryForIds(I)");
		// Genera el query que extrae solo un segmento de la informacion

		StringBuffer query = new StringBuffer("");
		StringBuffer clavesDocumentos = new StringBuffer();
		for (Iterator it = ids.iterator(); it.hasNext();){
			it.next();
			clavesDocumentos.append("?,");
		}
		clavesDocumentos = clavesDocumentos.delete(clavesDocumentos.length()-1,
		clavesDocumentos.length());

    	String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
		query.append(
			" select /*+ ordered use_nl(s,d,e,i,pe,p,ie,ci,es,m) index(ds IN_COM_DOCTO_SELEC_03_NUK) */ "+
			" 'Interna' as tipo, s.ic_folio as folio"+
			//" ,(decode(pe.cs_habilitado,'N','*','S','')||' '||p.cg_razon_social) as nombrePYME"+ //Linea a modificar por FODEA 17
			" , decode(ds.cs_opera_fiso, 'S', i2.cg_razon_social , 'N', (decode(pe.cs_habilitado,'N','*','S','')||' '||p.cg_razon_social)) as NOMBRE_PYME"+ //FODEA 17
			" ,(decode(e.cs_habilitado,'N','*','S','')||' '||e.cg_razon_social) as nombreEPO"+
			" , d.ig_numero_docto, TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as df_fecha_docto"+
			" , TO_CHAR(s.df_v_descuento,'dd/mm/yyyy') as df_fecha_venc"+
			" ,	es.ic_estatus_solic, d.ic_moneda, m.cd_nombre"+
			" , d.fn_monto, d.fn_porc_anticipo"+
			" , d.fn_monto_dscto,"+
			//"	ds.in_importe_interes"+ //Linea a modificar por FODEA 17
			"  decode(ds.cs_opera_fiso, 'S', ds.in_importe_interes_fondeo, 'N', ds.in_importe_interes ) as INTERES"+	//FODEA 17
			" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as df_fecha_solicitud"+
			//" , ds.in_tasa_aceptada,"+ //Linea a modificar por FODEA 17
			"  ,decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada  ) as TASA_PYME, "+	//FODEA 17
			"	s.ig_plazo,"+
			//"	ds.in_importe_recibir as montoOperar"+ //Linea a modificar por FODEA 17
			"  decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir  ) as montoOperar "+	//FODEA 17
			" , es.cd_descripcion, s.ic_folio"+
			" , s.cc_acuse, s.ic_oficina, d.ic_pyme, p.in_numero_sirac, p.cg_razon_social as razon_pyme " +
			" , d.ic_epo, d.ic_if, i.cg_razon_social as razon_if " +
			" , TO_CHAR(d.df_alta,'dd/mm/yyyy') as df_alta "+
			" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as df_fecha_solicitud, s.ic_tasaif " +
			" , TO_CHAR(s.df_operacion,'dd/mm/yyyy') as df_operacion, s.ig_numero_prestamo, s.cg_causa_rechazo " +
			" , d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, d.cg_campo5 "+
			" , d.CS_DSCTO_ESPECIAL " +
			" , s.ig_numero_prestamo"+
			" , TO_CHAR(s.df_operacion,'DD/MM/YYYY') as df_operacion, d.ct_referencia, pe.cg_pyme_epo_interno " +
			" , s.fg_porc_comision_fondeo as PORCENTAJE_COMISION, ds.in_importe_recibir * (s.fg_porc_comision_fondeo/100) as MONTO_COMISION "+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ci.cg_razon_social,'') as BENEF"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ie.cg_banco,'') as BANCO_BENEF"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ie.ig_sucursal,'') as SUCURSAL_BENEF"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ie.cg_num_cuenta,'') as CTA_BENEF"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,d.fn_porc_beneficiario,'') as PORC_BENEF"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ds.fn_importe_recibir_benef,'') as IMPORTE_RECIBIR"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ds.in_importe_recibir - (ds.fn_importe_recibir_benef),'') as NETO_REC_PYME, 'ConsSolicIfDE::getDocumentSummaryQueryForIds' ORIGENQUERY "+
			" , d.ic_estatus_docto, d.ic_documento " +
			" , s.fg_tasa_fondeo_nafin AS TASA_FONDEO "+
			" , tf.cg_nombre AS TIPO_FACTORAJE "+
			"  , decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as tipoTasa "+
			" from "   +
			" 	com_solicitud s, "   +
			" 	com_docto_seleccionado ds, "   +
			" 	com_documento d , "   +
			" 	comcat_epo e,  "   +
			" 	comcat_if i, "   +
			" 	comcat_if i2, "   + //FODEA 17
			" 	comrel_pyme_epo pe , "   +
			" 	comcat_pyme p, "   +
			" 	comrel_if_epo ie, "   +
			" 	comcat_if ci, "   +
			" 	comcat_estatus_solic es, "   +
			" 	comcat_moneda m,  "   +
			"  comcat_tipo_factoraje tf "+
			" where s.ic_documento=ds.ic_documento and "   +
			" 	ds.ic_documento=d.ic_documento and "   +
			" 	d.ic_pyme=p.ic_pyme and "   +
			" 	ds.ic_epo=e.ic_epo and "   +
			" 	d.ic_if = i.ic_if and "   +
			" 	ds.ic_if = i2.ic_if and "   + //FODEA 17
			" 	d.ic_moneda=m.ic_moneda and "   +
			" 	d.ic_pyme = pe.ic_pyme and "   +
			" 	d.ic_epo = pe.ic_epo and "   +
			"  d.ic_epo = ds.ic_epo and" +//ACF
			" 	s.ic_estatus_solic=es.ic_estatus_solic and "   +
//			" 	ds.ic_if = ? and "   +
			" 	ie.ic_if(+) = d.ic_beneficiario and "   +
			" 	ie.ic_epo(+)=d.ic_epo and "   +
			" 	ie.ic_if = ci.ic_if(+) and "   +
			" 	s.cs_tipo_solicitud='C' " +
			"  and d.CS_DSCTO_ESPECIAL!='C' " +//new
			"  and d.cs_dscto_especial = tf.cc_tipo_factoraje "+
			" AND D.ic_documento in (" + clavesDocumentos + ") ");

		log.info("..:: query : "+query.toString());
		log.info("..:: ConsSolicIfDE - getDocumentSummaryQueryForIds(F)");
		return query.toString();
	}

/**************************************
     * Metodos Para la Migraci�n      *
 **************************************/

	public String getAggregateCalculationQuery() {
			log.info("getAggregateCalculationQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			hint 	= new StringBuffer();

		if(!df_fecha_vencMin.equals("")) {
				hint.append(" s IN_COM_SOLICITUD_14_NUK,") ;
		}
		if(!df_fecha_solicitudMin.equals("")) {
			hint.append(" s IN_COM_SOLICITUD_13_NUK,") ;
		}

		if(!"".equals(hint.toString()) && hint.toString() != null){
			hint.append("index( " + hint.toString().substring(0,hint.toString().length()-1) + ")");
		}

		qrySentencia.append(
			" select" +
            //"/*+ ordered " + hint + " use_nl(s ds d pe e prodepo) */ "+//ACF
            " /*+  index (d IN_COM_DOCUMENTO_12_NUK) index (s IN_COM_SOLICITUD_13_NUK ) */" +
			"     d.ic_moneda CVE_MONEDA,"   +
			" 		decode( d.ic_moneda,'1', 'Total Moneda Nacional', '54','Total D�lares') AS MONEDA  , "+
			" 	   count(ds.ic_documento) NUM_DOCTOS, "   +
			" 	   sum(nvl(d.fn_monto,0)) TOTAL_MONTOS_DOCTO, "   +
			" 	   sum(nvl(d.fn_monto_dscto,0)) TOTAL_MONTOS_DESC, "   +
			" 	   sum(nvl(ds.in_importe_recibir,0)) MONTO_RECIBIR_DOCTOS, "   +
			" 	   sum(decode(s.ic_estatus_solic,1,1,2,1,0)) TOTAL_SOLIC_X_AUTO, "   +
			" 	   sum(decode(s.ic_estatus_solic,3,1,10,1,0)) TOTAL_SOLIC_OPER, "   +
			" 	   sum(decode(s.ic_estatus_solic,4,1,0)) TOTAL_SOLIC_RECH,      "   +
			//" 	   sum(decode(s.ic_estatus_solic,1,nvl(ds.in_importe_recibir,0),2,nvl(ds.in_importe_recibir,0),0)) MONTO_SOLIC_X_AUTO, "   +	//Linea a modificar por FODEA 17
			"		sum(decode(ds.cs_opera_fiso, 'S', nvl(DECODE (s.ic_estatus_solic,1, NVL (ds.in_importe_recibir, 0),2, NVL (ds.in_importe_recibir, 0),0),0), 'N', decode(s.ic_estatus_solic,1,nvl(ds.in_importe_recibir,0),2,nvl(ds.in_importe_recibir,0),0))) MONTO_SOLIC_X_AUTO,	"	+ //FODEA 17
			//" 	   sum(decode(s.ic_estatus_solic,3,nvl(ds.in_importe_recibir,0),10,nvl(ds.in_importe_recibir,0),0)) MONTO_SOLIC_OPER, "   +	//Linea a modificar por FODEA 17
			"		sum(decode(ds.cs_opera_fiso, 'S', nvl(DECODE (s.ic_estatus_solic,3, NVL (ds.in_importe_recibir, 0),10, NVL (ds.in_importe_recibir, 0),0), 0), 'N', decode(s.ic_estatus_solic,3,nvl(ds.in_importe_recibir,0),10,nvl(ds.in_importe_recibir,0),0)))	MONTO_SOLIC_OPER, "	+ //FODEA 17
			//" 	   sum(decode(s.ic_estatus_solic,4,nvl(ds.in_importe_recibir,0),0)) MONTO_SOLIC_RECH,    "+	//Linea a modificar por FODEA 17
			"		sum(decode(ds.cs_opera_fiso, 'S', nvl(DECODE (s.ic_estatus_solic,4, NVL (ds.in_importe_recibir, 0),0), 0), 'N', decode(s.ic_estatus_solic,4,nvl(ds.in_importe_recibir,0),0))) MONTO_SOLIC_RECH,	"	+ //FODEA 17
			" 	   sum(nvl(d.fn_monto,0)-nvl(d.fn_monto_dscto,0)) MONTO_RECURSO, "   +
			" 	   sum(nvl(ds.in_importe_interes,0)) MONTO_INTERES	"  +
			"	from "   +
			" 	com_solicitud s, "   +
			" 	com_docto_seleccionado ds, "   +
			" 	com_documento d , "   +
			" 	comcat_epo e,  "   +
			" 	comcat_if i, "   +
			" 	comrel_pyme_epo pe , "   +
			" 	comcat_pyme p, "   +
			" 	comrel_if_epo ie, "   +
			" 	comcat_if ci, "   +
			" 	comcat_estatus_solic es, "   +
			" 	comcat_moneda m,  "   +
			"  comcat_tipo_factoraje tf "+
			" where s.ic_documento=ds.ic_documento and "   +
			" 	ds.ic_documento=d.ic_documento and "   +
			" 	d.ic_pyme=p.ic_pyme and "   +
			" 	ds.ic_epo=e.ic_epo and "   +
			" 	d.ic_if = i.ic_if and "   +
			" 	d.ic_moneda=m.ic_moneda and "   +
			" 	d.ic_pyme = pe.ic_pyme and "   +
			" 	d.ic_epo = pe.ic_epo and "   +
			"  d.ic_epo = ds.ic_epo and" +//ACF
			" 	s.ic_estatus_solic=es.ic_estatus_solic and "   +
			" 	ie.ic_if(+) = d.ic_beneficiario and "   +
			" 	ie.ic_epo(+)=d.ic_epo and "   +
			" 	ie.ic_if = ci.ic_if(+) and "   +
			" 	s.cs_tipo_solicitud='C' " +
			"  and d.CS_DSCTO_ESPECIAL!='C' " +//new
			"  and d.cs_dscto_especial = tf.cc_tipo_factoraje "+
			" 	 and  d.ic_if=? "   ); //FODEA 17 antes ds.ic_if

			conditions.add(ic_if);

			if(!tipoTasa.equals("")) {
				qrySentencia.append(" and ds.CG_TIPO_TASA = ? ");
				conditions.add(tipoTasa);
			}
			if (ic_epo.equals("")	&& ic_estatus_solic.equals("") && ic_moneda.equals("")
				&& df_fecha_vencMin.equals("") && df_fecha_solicitudMin.equals("")
				&& fn_montoMin.equals("") && ic_folio.equals("")) {//ACF
				df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
			}
			if (!ic_epo.equals("")){
				qrySentencia.append( " and d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if (!ic_pyme.equals("")) {
				qrySentencia.append( " and d.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!ic_moneda.equals("")){
				qrySentencia.append(" and d.ic_moneda = ? ");
				conditions.add(ic_moneda);
			}

			if(!df_fecha_vencMin.equals("")) {
				if(!df_fecha_vencMax.equals("")) {
					qrySentencia.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_vencMin);
					conditions.add(df_fecha_vencMax);

				} else {
					qrySentencia.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_vencMin);
					conditions.add(df_fecha_vencMax);
				}
			}

			if(!fn_montoMin.equals("")) {
				if(!fn_montoMax.equals("")) {
					qrySentencia.append(" and d.fn_monto between ? and ? ");
					conditions.add(fn_montoMin);
					conditions.add(fn_montoMax);
				}else {
					qrySentencia.append(" and d.fn_monto = ? ");
					conditions.add(fn_montoMin);
				}
			}
			if (!ic_estatus_solic.equals("")){
				qrySentencia.append(" and s.ic_estatus_solic = ? ");
				conditions.add(ic_estatus_solic);
			}
			if (!ic_folio.equals("")) {
				qrySentencia.append(" and s.ic_folio = ? ");
				conditions.add(ic_folio);
			}

			if(!df_fecha_solicitudMin.equals("")) {
				if(!df_fecha_solicitudMax.equals("")) {
					qrySentencia.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and  s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_solicitudMin);
					conditions.add(df_fecha_solicitudMax);
				} else {
					qrySentencia.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and  s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_solicitudMin);
					conditions.add(df_fecha_solicitudMax);
				}
			}
			if(!tipoFactoraje.equals("")) {
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL= ? ");
				conditions.add(tipoFactoraje);
			} else {
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL!=? ");//new
				conditions.add("C");
			}

			if (!"".equals(aplicaFloating)  ) {	
				qrySentencia.append(" AND s.CS_FLOATING = ? "); 
				conditions.add(aplicaFloating);
			}

			qrySentencia.append("group by d.ic_moneda  order by d.ic_moneda ");


		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();

	}

	 /**
	 *  obtiene los registros unicos
	 * @return
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		hint 	= new StringBuffer();

		if(!df_fecha_vencMin.equals("")) {
			hint.append(" s IN_COM_SOLICITUD_14_NUK,");
		}
		if(!df_fecha_solicitudMin.equals("")) {
			hint.append(" s IN_COM_SOLICITUD_13_NUK, ");
		}


		qrySentencia.append(
				" select /*+ ordered  use_nl(d,s) */ "+
				" d.ic_documento "+
				" ,'ConsSolicIfDE::getDocumentQuery' Origen " +
				" from "   +
				" 	com_solicitud s, "   +
				" 	com_docto_seleccionado ds, "   +
				" 	com_documento d , "   +
				" 	comcat_epo e,  "   +
				" 	comcat_if i, "   +
				" 	comrel_pyme_epo pe , "   +
				" 	comcat_pyme p, "   +
				" 	comrel_if_epo ie, "   +
				" 	comcat_if ci, "   +
				" 	comcat_estatus_solic es, "   +
				" 	comcat_moneda m,  "   +
				"  comcat_tipo_factoraje tf "+
				" where s.ic_documento=ds.ic_documento and "   +
				" 	ds.ic_documento=d.ic_documento and "   +
				" 	d.ic_pyme=p.ic_pyme and "   +
				" 	ds.ic_epo=e.ic_epo and "   +
				" 	d.ic_if = i.ic_if and "   +
				" 	d.ic_moneda=m.ic_moneda and "   +
				" 	d.ic_pyme = pe.ic_pyme and "   +
				" 	d.ic_epo = pe.ic_epo and "   +
				"  d.ic_epo = ds.ic_epo and" +//ACF
				" 	s.ic_estatus_solic=es.ic_estatus_solic and "   +
				" 	ie.ic_if(+) = d.ic_beneficiario and "   +
				" 	ie.ic_epo(+)=d.ic_epo and "   +
				" 	ie.ic_if = ci.ic_if(+) and "   +
				" 	s.cs_tipo_solicitud='C' " +
				"  and d.CS_DSCTO_ESPECIAL!='C' " +//new
				"  and d.cs_dscto_especial = tf.cc_tipo_factoraje "+
				" and	d.ic_if = ?  ");
			conditions.add(ic_if);

		if(!tipoTasa.equals("")) {
			qrySentencia.append(" and ds.CG_TIPO_TASA = ?");
			conditions.add(tipoTasa);
		}

		if (ic_epo.equals("")	&& ic_estatus_solic.equals("") && ic_moneda.equals("")
				&& df_fecha_vencMin.equals("") && df_fecha_solicitudMin.equals("")
				&& fn_montoMin.equals("") && ic_folio.equals("")) {//ACF
				df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}
		if (!ic_epo.equals("")) {
			qrySentencia.append( " and d.ic_epo = ? ");
			conditions.add(ic_epo);
		}

		if (!ic_pyme.equals("")) {
			qrySentencia.append( " and d.ic_pyme = ? ");
			conditions.add(ic_pyme);
		}

		if (!ic_moneda.equals("")){
			qrySentencia.append(" and d.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}

		if(!df_fecha_vencMin.equals("")) {
			if(!df_fecha_vencMax.equals("")) {
				qrySentencia.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
				conditions.add(df_fecha_vencMin);
				conditions.add(df_fecha_vencMax);
			} else {
				qrySentencia.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
				conditions.add(df_fecha_vencMin);
				conditions.add(df_fecha_vencMax);
			}
		}

		if(!fn_montoMin.equals("")) {
				if(!fn_montoMax.equals("")) {
					qrySentencia.append(" and d.fn_monto between ? and ? ");
					conditions.add(fn_montoMin);
					conditions.add(fn_montoMax);
				} else {
					qrySentencia.append(" and d.fn_monto = ? ");
					conditions.add(fn_montoMin);
				}
			}

			if (!ic_estatus_solic.equals("")){
				qrySentencia.append(" and s.ic_estatus_solic = ? ");
				conditions.add(ic_estatus_solic);
			}

			if (!ic_folio.equals("")) {
				qrySentencia.append(" and s.ic_folio = ? ");
				conditions.add(ic_folio);
			}

			if(!df_fecha_solicitudMin.equals("")) {
				if(!df_fecha_solicitudMax.equals("")) {
					qrySentencia.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_solicitudMin);
					conditions.add(df_fecha_solicitudMax);
				} else {
					qrySentencia.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_solicitudMin);
					conditions.add(df_fecha_solicitudMax);
				}
			}

			if(!tipoFactoraje.equals("")) {
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL=  ? ");
				conditions.add(tipoFactoraje);
			} else {
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL!= ? ");
				conditions.add("C");
			}

			if (!"".equals(aplicaFloating)  ) {	
				qrySentencia.append(" AND s.CS_FLOATING = ? "); 
				conditions.add(aplicaFloating);
			}
			
		   qrySentencia.append(" ORDER BY D.IC_DOCUMENTO ");


		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();

	}

	/**
	 * realiza la paginaci�n
	 * @return
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {

		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();


		qrySentencia.append(
			" select /*+ ordered use_nl(s,d,e,i,pe,p,ie,ci,es,m) index(ds IN_COM_DOCTO_SELEC_03_NUK) */ "+
			" 'Interna' as ORIGEN, "+
			" s.ic_folio as FOLIO_SOLIC"+
			//" ,(decode(pe.cs_habilitado,'N','*','S','')||' '||p.cg_razon_social) as NOMBRE_PYME"+	//Linea a modificar por FODEA 17
			" , decode(ds.cs_opera_fiso, 'S', i2.cg_razon_social , 'N', (decode(pe.cs_habilitado,'N','*','S','')||' '||p.cg_razon_social)) as NOMBRE_PYME"+ //FODEA 17
			" ,(decode(e.cs_habilitado,'N','*','S','')||' '||e.cg_razon_social) as EPO_RELACIONADA"+
			" , d.ig_numero_docto as NUM_DOCTO ,"+
			" TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as FECHA_EMISION"+
			" , TO_CHAR(s.df_v_descuento,'dd/mm/yyyy') as FECHA_VENC_SOLIC"+
			" ,	es.ic_estatus_solic as IC_ESTATUS , "+
			"  d.ic_moneda as IC_MONEDA, "+
			"  m.cd_nombre AS MONEDA"+
			" , d.fn_monto AS MONTO_DOCTO  , "+
			" d.fn_porc_anticipo as POR_DESC "+
			" , d.fn_monto_dscto  as MONTO_DESC "+
			//" , ds.in_importe_interes as INTERES"+		//Linea a modificar por FODEA 17
			" , decode(ds.cs_opera_fiso, 'S', ds.in_importe_interes_fondeo, 'N', ds.in_importe_interes ) as INTERES"+	//FODEA 17
			" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as FECHA_SOLIC_DESC, "+
			//"  ds.in_tasa_aceptada as TASA_PYME , "+	//Linea a modificar por FODEA 17
			"  decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada  ) as TASA_PYME, "+	//FODEA 17
			" s.ig_plazo as PLAZO , "+
			//" ds.in_importe_recibir as MONTO_OPERAR"+ Linea a modificar por FODEA 17
			"  decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir  ) as MONTO_OPERAR "+	//FODEA 17
			" , decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', d.ct_referencia) as REFERENCIA_PYME"+		//Campo agregado por FODEA 17
			" , es.cd_descripcion as ESTATUS "+
			" , s.cc_acuse as CC_ACUSE , s.ic_oficina as IC_OFICINA , d.ic_pyme AS IC_PYME, p.in_numero_sirac as IN_NUMERO_SIRAC, p.cg_razon_social as razon_pyme " +
			" , d.ic_epo, d.ic_if, i.cg_razon_social as razon_if " +
			" , TO_CHAR(d.df_alta,'dd/mm/yyyy') as df_alta "+
			" , s.ic_tasaif " +
			" , TO_CHAR(s.df_operacion,'dd/mm/yyyy') as FECHA_OPERACION, "+
			" s.cg_causa_rechazo " +
			" , d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, d.cg_campo5 "+
			" , d.CS_DSCTO_ESPECIAL " +
			" , s.ig_numero_prestamo as NUM_PRESTAMO , "+
			"  TO_CHAR(s.df_operacion,'DD/MM/YYYY')  , "+
			" d.ct_referencia, pe.cg_pyme_epo_interno " +
			" , s.fg_porc_comision_fondeo as PORCENTAJE_COMISION, ds.in_importe_recibir * (s.fg_porc_comision_fondeo/100) as MONTO_COMISION "+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ci.cg_razon_social,'') as BENEFICIARIO"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ie.cg_banco,'') as BANCO_BENEFICIARIO"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ie.ig_sucursal,'') as SUCURSAL_BENEFICIARIA"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ie.cg_num_cuenta,'') as CUENTA_BENEFICIARIA"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,d.fn_porc_beneficiario,'') as PORC_BENEFICIARIO"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ds.fn_importe_recibir_benef,'') as IMPORTE_BENEFICIARIO"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ds.in_importe_recibir - (ds.fn_importe_recibir_benef),'') as NETO_RECIBIR, 'ConsSolicIfDE::getDocumentSummaryQueryForIds' ORIGENQUERY "+
			" , d.ic_estatus_docto  as IC_ESTATUS_DOCTO , d.ic_documento AS IC_DOCUMENTO " +
			"  ,d.fn_monto_ant, "+
			"  d.ig_numero_docto as ic_docto_asociado"+
			" , s.fg_tasa_fondeo_nafin AS TASA_FONDEO "+
			" , tf.cg_nombre AS TIPO_FACTORAJE "+
			"  , decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as REFERENCIA "+
			" , d.fn_monto - d.fn_monto_dscto  as RECURSO_GARAN  "+
			" ,  TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fechavencdoc "+
			" , S.CC_ACUSE " +
			" ,(SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = S.CC_ACUSE) AS MUESTRA_VISOR " +
			" from "   +
			" 	com_solicitud s, "   +
			" 	com_docto_seleccionado ds, "   +
			" 	com_documento d , "   +
			" 	comcat_epo e,  "   +
			" 	comcat_if i, "   +
			" 	comcat_if i2, "   +	//FODEA 17
			" 	comrel_pyme_epo pe , "   +
			" 	comcat_pyme p, "   +
			" 	comrel_if_epo ie, "   +
			" 	comcat_if ci, "   +
			" 	comcat_estatus_solic es, "   +
			" 	comcat_moneda m,  "   +
			"  comcat_tipo_factoraje tf "+
			" where s.ic_documento=ds.ic_documento and "   +
			" 	ds.ic_documento=d.ic_documento and "   +
			" 	d.ic_pyme=p.ic_pyme and "   +
			" 	ds.ic_epo=e.ic_epo and "   +
			" 	d.ic_if = i.ic_if and "   +
			" 	ds.ic_if = i2.ic_if and "   +	//FODEA 17
			" 	d.ic_moneda=m.ic_moneda and "   +
			"  d.ic_pyme = p.ic_pyme and " +		//Agregado por FODEA 17
			" 	d.ic_pyme = pe.ic_pyme and "   +
			" 	d.ic_epo = pe.ic_epo and "   +
			"  d.ic_epo = ds.ic_epo and" +//ACF
			" 	s.ic_estatus_solic=es.ic_estatus_solic and "   +

			" 	ie.ic_if(+) = d.ic_beneficiario and "   +
			" 	ie.ic_epo(+)=d.ic_epo and "   +
			" 	ie.ic_if = ci.ic_if(+) and "   +
			" 	s.cs_tipo_solicitud='C' " +
			"  and d.CS_DSCTO_ESPECIAL!='C' " +//new
			"  and d.cs_dscto_especial = tf.cc_tipo_factoraje " +
			" AND (  ");

			for (int i = 0; i < pageIds.size(); i++) {
				List lItem = (ArrayList)pageIds.get(i);

				if(i > 0){qrySentencia.append("  OR  ");}
					qrySentencia.append(" d.ic_documento = ? ");
				conditions.add(new Long(lItem.get(0).toString()));
				}

			 qrySentencia.append(" ) ");


			if(!ordenadoBy.equals("")) {
				qrySentencia.append(" order by " +ordenadoBy);
			}else {
				qrySentencia.append(" order by d.ic_documento ");

			}

		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();

	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		qrySentencia.append(
			" select /*+ ordered use_nl(s,d,e,i,pe,p,ie,ci,es,m) index(ds IN_COM_DOCTO_SELEC_03_NUK) */ "+
			" 'Interna' as ORIGEN, "+
			" s.ic_folio as FOLIO_SOLIC"+
			//" ,(decode(pe.cs_habilitado,'N','*','S',' ')||' '||p.cg_razon_social) as NOMBRE_PYME"+	Linea a modificar por FODEA 17
			" , decode(ds.cs_opera_fiso, 'S', i2.cg_razon_social , 'N', (decode(pe.cs_habilitado,'N','*','S','')||' '||p.cg_razon_social)) as NOMBRE_PYME"+ //FODEA 17
			" ,(decode(e.cs_habilitado,'N','*','S',' ')||' '||e.cg_razon_social) as EPO_RELACIONADA"+
			" , d.ig_numero_docto as NUM_DOCTO ,"+
			" TO_CHAR(d.df_fecha_docto,'dd/mm/yyyy') as FECHA_EMISION"+
			" , TO_CHAR(s.df_v_descuento,'dd/mm/yyyy') as FECHA_VENC_SOLIC"+
			" ,	es.ic_estatus_solic as IC_ESTATUS , "+
			"  d.ic_moneda as IC_MONEDA, "+
			"  m.cd_nombre AS MONEDA"+
			" , d.fn_monto AS MONTO_DOCTO  , "+
			" d.fn_porc_anticipo as POR_DESC "+
			" , d.fn_monto_dscto  as MONTO_DESC "+
			//" , ds.in_importe_interes as INTERES"+	Linea a modificar por FODEA 17
			" , decode(ds.cs_opera_fiso, 'S', ds.in_importe_interes_fondeo, 'N', ds.in_importe_interes ) as INTERES"+	//FODEA 17
			" , TO_CHAR(s.df_fecha_solicitud,'dd/mm/yyyy') as FECHA_SOLIC_DESC, "+
			//"  ds.in_tasa_aceptada as TASA_PYME , "+	Linea a modificarpor FODEA 17
			"  decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada  ) as TASA_PYME, "+	//FODEA 17
			" s.ig_plazo as PLAZO , "+
			//" ds.in_importe_recibir as MONTO_OPERAR"+	Linea a modificar por FODEA 17
			"  decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir  ) as MONTO_OPERAR "+	//FODEA 17
			" , decode(ds.cs_opera_fiso, 'S' ,p.cg_razon_social , 'N', d.ct_referencia) as REFERENCIA_PYME"+	//campo agregado por FODEA 17
			" , es.cd_descripcion as ESTATUS "+
			" , s.cc_acuse, s.ic_oficina, d.ic_pyme, "+
			" p.in_numero_sirac as IN_NUMERO_SIRAC, "+
			" p.cg_razon_social as razon_pyme " +
			" , d.ic_epo, "+
			" d.ic_if, "+
			" i.cg_razon_social as razon_if " +
			" , TO_CHAR(d.df_alta,'dd/mm/yyyy') as df_alta "+
			" , s.ic_tasaif " +
			" , TO_CHAR(s.df_operacion,'dd/mm/yyyy') as FECHA_OPERACION, "+
			" s.ig_numero_prestamo as NUM_PRESTAMO ," +
			" s.cg_causa_rechazo " +
			" , d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, d.cg_campo5 "+
			" , d.CS_DSCTO_ESPECIAL,  " +
			" d.ct_referencia, "+
			" pe.cg_pyme_epo_interno  as CG_PYME_EPO_INTERNO" +
			" , s.fg_porc_comision_fondeo as PORCENTAJE_COMISION, "+
			" ds.in_importe_recibir * (s.fg_porc_comision_fondeo/100) as MONTO_COMISION "+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ci.cg_razon_social,'') as BENEFICIARIO"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ie.cg_banco,'') as BANCO_BENEFICIARIO"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ie.ig_sucursal,'') as SUCURSAL_BENEFICIARIA"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ie.cg_num_cuenta,'') as CUENTA_BENEFICIARIA"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,d.fn_porc_beneficiario,'') as PORC_BENEFICIARIO"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ds.fn_importe_recibir_benef,'') as IMPORTE_BENEFICIARIO"+
			" , decode(d.cs_dscto_especial,'D', d.cs_dscto_especial,'I' ,ds.in_importe_recibir - (ds.fn_importe_recibir_benef),'') as NETO_RECIBIR, "+
			"  d.ic_estatus_docto as IC_ESTATUS_DOCTO , d.ic_documento " +
			" ,d.fn_monto_ant, '' as ic_docto_asociado"+
			" , s.fg_tasa_fondeo_nafin AS TASA_FONDEO "+
			" , tf.cg_nombre AS TIPO_FACTORAJE "+
			"  , decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as REFERENCIA "+
			" , d.fn_monto - d.fn_monto_dscto  as RECURSO_GARAN  "+
			" ,  TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fechavencdoc "+
			" from "   +
			" 	com_solicitud s, "   +
			" 	com_docto_seleccionado ds, "   +
			" 	com_documento d , "   +
			" 	comcat_epo e,  "   +
			" 	comcat_if i, "   +
			" 	comcat_if i2, "   +	//FODEA 17
			" 	comrel_pyme_epo pe , "   +
			" 	comcat_pyme p, "   +
			" 	comrel_if_epo ie, "   +
			" 	comcat_if ci, "   +
			" 	comcat_estatus_solic es, "   +
			" 	comcat_moneda m,  "   +
			"  comcat_tipo_factoraje tf "+
			" where s.ic_documento=ds.ic_documento and "   +
			" 	ds.ic_documento=d.ic_documento and "   +
			" 	d.ic_pyme=p.ic_pyme and "   +
			" 	ds.ic_epo=e.ic_epo and "   +
			" 	d.ic_if = i.ic_if and "   +
			" 	ds.ic_if = i2.ic_if and "   +	//FODEA 17
			" 	d.ic_moneda=m.ic_moneda and "   +
			"  d.ic_pyme = p.ic_pyme and" +	//Agregado por FODEA 17
			" 	d.ic_pyme = pe.ic_pyme and "   +
			" 	d.ic_epo = pe.ic_epo and "   +
			"  d.ic_epo = ds.ic_epo and" +//ACF
			" 	s.ic_estatus_solic=es.ic_estatus_solic and "   +
			" 	ie.ic_if(+) = d.ic_beneficiario and "   +
			" 	ie.ic_epo(+)=d.ic_epo and "   +
			" 	ie.ic_if = ci.ic_if(+) and "   +
			" 	s.cs_tipo_solicitud='C' " +
			"  and d.cs_dscto_especial = tf.cc_tipo_factoraje "+
			" and	d.ic_if =  "+ic_if+"");

			if(!tipoTasa.equals("")) {
			qrySentencia.append(" and ds.CG_TIPO_TASA = ?");
			conditions.add(tipoTasa);
		}

		if (ic_epo.equals("")	&& ic_estatus_solic.equals("") && ic_moneda.equals("")
				&& df_fecha_vencMin.equals("") && df_fecha_solicitudMin.equals("")
				&& fn_montoMin.equals("") && ic_folio.equals("")) {//ACF
				df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}
		if (!ic_epo.equals("")) {
			qrySentencia.append( " and d.ic_epo = ? ");
			conditions.add(ic_epo);
		}

		if (!ic_pyme.equals("")) {
			qrySentencia.append( " and d.ic_pyme = ? ");
			conditions.add(ic_pyme);
		}

		if (!ic_moneda.equals("")){
			qrySentencia.append(" and d.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}

		if(!df_fecha_vencMin.equals("")) {
			if(!df_fecha_vencMax.equals("")) {
				qrySentencia.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
				conditions.add(df_fecha_vencMin);
				conditions.add(df_fecha_vencMax);
			} else {
				qrySentencia.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
				conditions.add(df_fecha_vencMin);
				conditions.add(df_fecha_vencMax);
			}
		}

		if(!fn_montoMin.equals("")) {
				if(!fn_montoMax.equals("")) {
					qrySentencia.append(" and d.fn_monto between ? and ? ");
					conditions.add(fn_montoMin);
					conditions.add(fn_montoMax);
				} else {
					qrySentencia.append(" and d.fn_monto = ? ");
					conditions.add(fn_montoMin);
				}
			}

			if (!ic_estatus_solic.equals("")){
				qrySentencia.append(" and s.ic_estatus_solic = ? ");
				conditions.add(ic_estatus_solic);
			}

			if (!ic_folio.equals("")) {
				qrySentencia.append(" and s.ic_folio = ? ");
				conditions.add(ic_folio);
			}

			if(!df_fecha_solicitudMin.equals("")) {
				if(!df_fecha_solicitudMax.equals("")) {
					qrySentencia.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_solicitudMin);
					conditions.add(df_fecha_solicitudMax);
				} else {
					qrySentencia.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_solicitudMin);
					conditions.add(df_fecha_solicitudMax);
				}
			}

			if(!tipoFactoraje.equals("")) {
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL=  ? ");
				conditions.add(tipoFactoraje);
			} else {
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL!= ? ");
				conditions.add("C");
			}

			if (!"".equals(aplicaFloating)  ) {	
				qrySentencia.append(" AND s.CS_FLOATING = ? "); 
				conditions.add(aplicaFloating);
			}


		if(tipoArchivo.equals("IVARIABLED")) {
				qrySentencia.append(
					" UNION ALL "   +
				" SELECT   /*+ ordered use_nl(s ds d d2 pe p ie e i ci m es prodepo tf) */"   +//ACF
					" 	    	'Interna' AS ORIGEN, " +
					" 		 	s.ic_folio AS FOLIO_SOLIC,"   +
					//"        (DECODE (pe.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social) AS NOMBRE_PYME,"   +	Linea a modificar por FODEA 17
					" 			decode(ds.cs_opera_fiso, 'S', i2.cg_razon_social , 'N', (decode(pe.cs_habilitado,'N','*','S','')||' '||p.cg_razon_social)) as NOMBRE_PYME, "+ //FODEA 17
					"        (DECODE (e.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || e.cg_razon_social) AS EPO_RELACIONADA,"   +
					"        d.ig_numero_docto as NUM_DOCTO, "+
					"			TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS FECHA_EMISION,"   +
					"        TO_CHAR (s.df_v_descuento, 'dd/mm/yyyy') AS FECHA_VENC_SOLIC, " +
					"			es.ic_estatus_solic as IC_ESTATUS ,"+
					"			d.ic_moneda as IC_MONEDA , "+
					"			m.cd_nombre as MONEDA ,"   +
					"        d.fn_monto as MONTO_DOCTO , " +
					"			d.fn_porc_anticipo as POR_DESC, "+
					" 			d.fn_monto_dscto as MONTO_DESC , "+
					//" 			ds.in_importe_interes as INTERES ,"   +	Linea a modificar por FODEA 17
					"  decode(ds.cs_opera_fiso, 'S', ds.in_importe_interes_fondeo, 'N', ds.in_importe_interes ) as INTERES ,"+	//FODEA 17
					"        TO_CHAR (s.df_fecha_solicitud, 'dd/mm/yyyy') AS FECHA_SOLIC_DESC, "+
					//" 			ds.in_tasa_aceptada AS TASA_PYME," +	Linea a modificar por FODEA 17
					"  decode(ds.cs_opera_fiso, 'S', ds.in_tasa_aceptada_fondeo, 'N', ds.in_tasa_aceptada  ) as TASA_PYME, "+	//FODEA 17
					"			s.ig_plazo AS PLAZO,"   +
					//"        ds.in_importe_recibir AS MONTO_OPERAR, "+	Linea a modificar por FODEA 17
					"  decode(ds.cs_opera_fiso, 'S', ds.in_importe_recibir_fondeo, 'N', ds.in_importe_recibir  ) as MONTO_OPERAR, "+	//FODEA 17
					" 	decode(ds.cs_opera_fiso, 'S', p.cg_razon_social, 'N', d.ct_referencia) AS REFERENCIA_PYME," +	//Campo agregado por FODEA 17
					"			es.cd_descripcion AS ESTATUS, " +
					"			s.cc_acuse, s.ic_oficina,"   +
					"        d.ic_pyme, "+
					" 			p.in_numero_sirac AS IN_NUMERO_SIRAC, "+
					"			p.cg_razon_social AS razon_pyme, "+
					"			d.ic_epo, " +
					"			d.ic_if,"   +
					"        i.cg_razon_social AS razon_if, "+
					"			TO_CHAR (d.df_alta, 'dd/mm/yyyy') AS df_alta,"   +
					"        s.ic_tasaif,"   +
					"        TO_CHAR (s.df_operacion, 'dd/mm/yyyy') AS FECHA_OPERACION, "+
					"			s.ig_numero_prestamo AS NUM_PRESTAMO, "+
					" 			s.cg_causa_rechazo,"   +
					"        d.cg_campo1, d.cg_campo2, d.cg_campo3, d.cg_campo4, d.cg_campo5, "+
					"			d.cs_dscto_especial,"   +
					"			d.ct_referencia,"   +
					"        pe.cg_pyme_epo_interno as   CG_PYME_EPO_INTERNO ,	"+
					" 	 		s.fg_porc_comision_fondeo AS PORCENTAJE_COMISION,"   +
					"        ds.in_importe_recibir * (s.fg_porc_comision_fondeo / 100) AS MONTO_COMISION,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ci.cg_razon_social, '') AS BENEFICIARIO,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ie.cg_banco, '') AS BANCO_BENEFICIARIO,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ie.ig_sucursal, '') AS SUCURSAL_BENEFICIARIA,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ie.cg_num_cuenta, '') AS CUENTA_BENEFICIARIA,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , d.fn_porc_beneficiario, '') AS PORC_BENEFICIARIO,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ds.fn_importe_recibir_benef, '') AS IMPORTE_BENEFICIARIO,"   +
					"        DECODE (d.cs_dscto_especial, 'D', d.cs_dscto_especial,'I' , ds.in_importe_recibir - (ds.fn_importe_recibir_benef), '') AS NETO_RECIBIR,"   +
					"       d.ic_estatus_docto as IC_ESTATUS_DOCTO, "+
					"		  d.ic_documento " +
					"      ,d.fn_monto_ant, "+
					"      d2.ig_numero_docto as ic_docto_asociado"+
					"		 ,s.fg_tasa_fondeo_nafin  AS TASA_FONDEO "+
					"      ,tf.cg_nombre AS TIPO_FACTORAJE " +
					"  	 , decode(ds.CG_TIPO_TASA,'B','Tasa Base','P','Preferencial','O','Oferta de Tasas','N','Negociada') as tipoTasa "+
					" 		, d.fn_monto - d.fn_monto_dscto  as RECURSO_GARAN  "+
					" 		,  TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fechavencdoc "+
					"   FROM com_solicitud s,"   +
					"        com_documento d,"   +
					"        com_documento d2,"   +
					"        com_docto_seleccionado ds,"   +
					"        comrel_pyme_epo pe,"   +
					"        comcat_pyme p,"   +
					"        comrel_if_epo ie,"   +
					"        comcat_epo e,"   +
					"        comcat_if i,"   +
					"        comcat_if i2,"   +
					"        comcat_if ci,"   +
					"        comcat_moneda m,"   +
					"        comcat_estatus_solic es, comrel_producto_epo prodepo, " +
					"        comcat_tipo_factoraje tf "+
					"  WHERE s.ic_documento = d.ic_docto_asociado"   +
					"    AND ds.ic_documento = d.ic_documento"   +
					"    AND d2.ic_documento = d.ic_docto_asociado"   +
					"    AND d.ic_pyme = p.ic_pyme"   +
					"    AND ds.ic_epo = e.ic_epo"   +
					"    AND d.ic_if = i.ic_if"   +
					"    AND ds.ic_if = i2.ic_if"   +	//FODEA 17
					"    AND d.ic_moneda = m.ic_moneda"   +
					"	  AND d.ic_pyme = p.ic_pyme"	+	//Agregado por FODEA 17
					"    AND d.ic_pyme = pe.ic_pyme"   +
					"    AND d.ic_epo = pe.ic_epo"   +
					"    AND d.ic_epo = prodepo.ic_epo " +
					"    AND	d.ic_epo = ds.ic_epo" +//ACF
					"    AND prodepo.ic_producto_nafin = 1 " +
					"    AND s.ic_estatus_solic = es.ic_estatus_solic"   +
					"    AND ie.ic_if (+) = d.ic_beneficiario"   +
					"    AND ie.ic_epo (+) = d.ic_epo"   +
					"    AND ie.ic_if = ci.ic_if (+)"   +
					"    AND s.cs_tipo_solicitud = 'C'"   +
					" 	  AND e.cs_habilitado = 'S' " +
					"    and d.cs_dscto_especial = tf.cc_tipo_factoraje "	+
					" and	d.ic_if =  "+ic_if+"");

							if(!tipoTasa.equals("")) {
			qrySentencia.append(" and ds.CG_TIPO_TASA = ?");
			conditions.add(tipoTasa);
		}

		if (ic_epo.equals("")	&& ic_estatus_solic.equals("") && ic_moneda.equals("")
				&& df_fecha_vencMin.equals("") && df_fecha_solicitudMin.equals("")
				&& fn_montoMin.equals("") && ic_folio.equals("")) {//ACF
				df_fecha_solicitudMin=(new SimpleDateFormat ("dd/MM/yyyy")).format(new java.util.Date());
		}
		if (!ic_epo.equals("")) {
			qrySentencia.append( " and d.ic_epo = ? ");
			conditions.add(ic_epo);
		}

		if (!ic_pyme.equals("")) {
			qrySentencia.append( " and d.ic_pyme = ? ");
			conditions.add(ic_pyme);
		}

		if (!ic_moneda.equals("")){
			qrySentencia.append(" and d.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}

		if(!df_fecha_vencMin.equals("")) {
			if(!df_fecha_vencMax.equals("")) {
				qrySentencia.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
				conditions.add(df_fecha_vencMin);
				conditions.add(df_fecha_vencMax);
			} else {
				qrySentencia.append(" and s.DF_V_DESCUENTO >= TO_DATE(?,'dd/mm/yyyy') and s.DF_V_DESCUENTO < TO_DATE(?,'dd/mm/yyyy')+1 ");
				conditions.add(df_fecha_vencMin);
				conditions.add(df_fecha_vencMax);
			}
		}

		if(!fn_montoMin.equals("")) {
				if(!fn_montoMax.equals("")) {
					qrySentencia.append(" and d.fn_monto between ? and ? ");
					conditions.add(fn_montoMin);
					conditions.add(fn_montoMax);
				} else {
					qrySentencia.append(" and d.fn_monto = ? ");
					conditions.add(fn_montoMin);
				}
			}

			if (!ic_estatus_solic.equals("")){
				qrySentencia.append(" and s.ic_estatus_solic = ? ");
				conditions.add(ic_estatus_solic);
			}

			if (!ic_folio.equals("")) {
				qrySentencia.append(" and s.ic_folio = ? ");
				conditions.add(ic_folio);
			}

			if(!df_fecha_solicitudMin.equals("")) {
				if(!df_fecha_solicitudMax.equals("")) {
					qrySentencia.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_solicitudMin);
					conditions.add(df_fecha_solicitudMax);
				} else {
					qrySentencia.append(" and s.df_fecha_solicitud >= TO_DATE(?,'dd/mm/yyyy') and s.df_fecha_solicitud < TO_DATE(?,'dd/mm/yyyy')+1 ");
					conditions.add(df_fecha_solicitudMin);
					conditions.add(df_fecha_solicitudMax);
				}
			}

			if(!tipoFactoraje.equals("")) {
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL=  ? ");
				conditions.add(tipoFactoraje);
			} else {
				qrySentencia.append(" and d.CS_DSCTO_ESPECIAL!= ? ");
				conditions.add("C");
			}

		}

		if(!ordenadoBy.equals("")) {
			qrySentencia.append(" order by " +ordenadoBy);
		}


		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");

		return qrySentencia.toString();

	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		StringBuffer contenidoArchivoD = new StringBuffer();
		String origen ="", folio_solic ="", nombre_pyme ="", epo_relacionada ="", num_docto ="", fecha_Emision ="",
				fecha_ven_solic ="", moneda ="", tipoFactoraje ="", monto_docto ="", por_desc ="", recurso_garan ="",
				monto_desc ="", interes  ="", tasa_pyme ="",  plazo ="", monto_operar ="", referencia_pyme ="", fecha_solic_desc ="",
				estatus ="", num_prestamo ="",  fecha_operacion ="", tasa_fondeo ="", beneficiario ="", banco_beneficiario ="",
				sucursal_beneficiario ="", cuenta_beneficiario ="", porc_beneficiario ="", importe_beneficiario ="",
				neto_recibir ="", referencia ="", claveEstatusSolic="",cc_acuse="", ic_oficina = "",
				cg_pyme_epo_interno ="", in_numero_sirac ="",claveEPO ="", claveIF ="", razon_if ="", monedaClave ="",
				df_alta ="", cg_causa_rechazo="",cg_campo1 ="", cg_campo2 ="", cg_campo3 ="", cg_campo4 ="", cg_campo5 ="",
				rs_monto_ant ="", rs_docto_asoc ="", ct_referencia ="";

				int cols =0;
		try {

			log.debug("tipoArchivo ------------------->" +tipoArchivo);
			
			if(tipo.equals("PDF") ) {

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				if(bTipoFactoraje.equals("N")) {  	cols +=1; 	}
				if(bOperaFactorajeDist.equals("N") || bOperaFactorajeVencINFO.equals("N")){  	cols +=7;  }

				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);

				if (tipoArchivo.equals("NORMAL") ) {
					pdfDoc.setTable(17,100);
					pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Origen","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Folio de Solicitud","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("EPO Relacionada","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("No. de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Vencimiento Solicitud","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Recurso en garant�a","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Intereses","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa a PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a operar","celda01",ComunesPDF.CENTER);

					pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Solicitud del Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Pr�stamo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa de Fondeo","celda01",ComunesPDF.CENTER);
					if(bTipoFactoraje.equals("S")) {
                  pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					}
					else
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencINFO.equals("S")){
                  pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Cuenta Beneficiaria","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
                  pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
					}else  {
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER,7);
					}
					pdfDoc.setCell("Detalle Aplic.","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER); //FODEA 17
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					/*if(bTipoFactoraje.equals("N")) {
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}*/

				}

				if (tipoArchivo.equals("INTERFASE") ) {

					pdfDoc.setTable(11,100);

					pdfDoc.setCell("Fecha Generacion :","celda01",ComunesPDF.LEFT,2);
					pdfDoc.setCell(fechaActual +" "+horaActual,"forma",ComunesPDF.LEFT,9);
					pdfDoc.setCell("Reporte :","celda01",ComunesPDF.LEFT,2);
					pdfDoc.setCell("Consulta de Solicitudes ","forma",ComunesPDF.LEFT,9);

					pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Folio","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de acuse asignado","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Clave de Oficina","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Clave de PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Numero Cliente SIRAC","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Clave de EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Clave IF","celda01",ComunesPDF.CENTER);

					pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Vencimiento Solicitud","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					if(bTipoFactoraje.equals("S")) {
						pdfDoc.setCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("Monto Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Recurso en Garant�a","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de Interes","celda01",ComunesPDF.CENTER);
					if(bTipoFactoraje.equals("N")) {
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
					}

					pdfDoc.setCell("C","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Operar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa de inter�s PYME","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Alta","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Solicitud","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operaci�n SIRAC","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de pr�stamo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Causa de rechazo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);

					pdfDoc.setCell("D","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("CG_CAMPO1","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("CG_CAMPO2","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("CG_CAMPO3","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("CG_CAMPO4","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("CG_CAMPO5","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo Solicitud","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 3); 
					if(bOperaFactorajeDist.equals("S")){
						pdfDoc.setCell("E","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Cuenta Beneficiaria","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("% Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Importe a recibir beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER, 3);
					}
				}
			}

			while(rs.next()){
				origen = (rs.getString("ORIGEN")==null)?"":rs.getString("ORIGEN");
				folio_solic = (rs.getString("FOLIO_SOLIC")==null)?"":rs.getString("FOLIO_SOLIC");
				nombre_pyme = (rs.getString("NOMBRE_PYME")==null)?"":rs.getString("NOMBRE_PYME");
				epo_relacionada = (rs.getString("EPO_RELACIONADA")==null)?"":rs.getString("EPO_RELACIONADA");
				num_docto = (rs.getString("NUM_DOCTO")==null)?"":rs.getString("NUM_DOCTO");
				fecha_Emision = (rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION");
				fecha_ven_solic = (rs.getString("FECHA_VENC_SOLIC")==null)?"":rs.getString("FECHA_VENC_SOLIC");
				moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
				tipoFactoraje = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE");
				monto_docto = (rs.getString("MONTO_DOCTO")==null)?"":rs.getString("MONTO_DOCTO");
				por_desc = (rs.getString("POR_DESC")==null)?"":rs.getString("POR_DESC");
				recurso_garan = (rs.getString("RECURSO_GARAN")==null)?"":rs.getString("RECURSO_GARAN");
				monto_desc = (rs.getString("MONTO_DESC")==null)?"":rs.getString("MONTO_DESC");
				interes = (rs.getString("INTERES")==null)?"":rs.getString("INTERES");
				tasa_pyme = (rs.getString("TASA_PYME")==null)?"":rs.getString("TASA_PYME");
				plazo = (rs.getString("PLAZO")==null)?"":rs.getString("PLAZO");
				monto_operar = (rs.getString("MONTO_OPERAR")==null)?"":rs.getString("MONTO_OPERAR");
				referencia_pyme = (rs.getString("REFERENCIA_PYME")==null)?"":rs.getString("REFERENCIA_PYME");	//FODEA 17
				fecha_solic_desc = (rs.getString("FECHA_SOLIC_DESC")==null)?"":rs.getString("FECHA_SOLIC_DESC");
				estatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
				num_prestamo = (rs.getString("NUM_PRESTAMO")==null)?"":rs.getString("NUM_PRESTAMO");
				fecha_operacion = (rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION");
				tasa_fondeo = (rs.getString("TASA_FONDEO")==null)?"":rs.getString("TASA_FONDEO");
				beneficiario = (rs.getString("BENEFICIARIO")==null)?"":rs.getString("BENEFICIARIO");
				banco_beneficiario = (rs.getString("BANCO_BENEFICIARIO")==null)?"":rs.getString("BANCO_BENEFICIARIO");
				sucursal_beneficiario = (rs.getString("SUCURSAL_BENEFICIARIA")==null)?"":rs.getString("SUCURSAL_BENEFICIARIA");
				cuenta_beneficiario = (rs.getString("CUENTA_BENEFICIARIA")==null)?"":rs.getString("CUENTA_BENEFICIARIA");
				porc_beneficiario = (rs.getString("PORC_BENEFICIARIO")==null)?"":rs.getString("PORC_BENEFICIARIO");
				importe_beneficiario = (rs.getString("IMPORTE_BENEFICIARIO")==null)?"":rs.getString("IMPORTE_BENEFICIARIO");
				neto_recibir  = (rs.getString("NETO_RECIBIR")==null)?"":rs.getString("NETO_RECIBIR");
				referencia = (rs.getString("REFERENCIA")==null)?"":rs.getString("REFERENCIA");
				claveEstatusSolic = (rs.getString("IC_ESTATUS")==null)?"":rs.getString("IC_ESTATUS");
				cc_acuse = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
				ic_oficina = (rs.getString("IC_OFICINA")==null)?"":rs.getString("IC_OFICINA");
				cg_pyme_epo_interno = (rs.getString("CG_PYME_EPO_INTERNO")==null)?"":rs.getString("CG_PYME_EPO_INTERNO");
				in_numero_sirac = (rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
				claveEPO = (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO");
				claveIF  = (rs.getString("IC_IF")==null)?"":rs.getString("IC_IF");
				razon_if  = (rs.getString("RAZON_IF")==null)?"":rs.getString("RAZON_IF");
				monedaClave = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
				df_alta = (rs.getString("DF_ALTA")==null)?"":rs.getString("DF_ALTA");
				cg_causa_rechazo = (rs.getString("cg_causa_rechazo")==null)?"":rs.getString("cg_causa_rechazo");
				cg_campo1 = (rs.getString("CG_CAMPO1") == null) ? "" : rs.getString("CG_CAMPO1");
				cg_campo2 = (rs.getString("CG_CAMPO2") == null) ? "" : rs.getString("CG_CAMPO2");
				cg_campo3 = (rs.getString("CG_CAMPO3") == null) ? "" : rs.getString("CG_CAMPO3");
				cg_campo4 = (rs.getString("CG_CAMPO4") == null) ? "" : rs.getString("CG_CAMPO4");
				cg_campo5 = (rs.getString("CG_CAMPO5") == null) ? "" : rs.getString("CG_CAMPO5");
				rs_monto_ant = (rs.getString("fn_monto_ant") == null) ? "" : rs.getString("fn_monto_ant");
		      rs_docto_asoc = (rs.getString("ic_docto_asociado") == null) ? "" : rs.getString("ic_docto_asociado");
				ct_referencia =(rs.getString("ct_referencia") == null) ? "" : rs.getString("ct_referencia");
				if(num_prestamo.equals("0")) {  num_prestamo= "";		}
				if(tasa_fondeo.equals("0")) {  tasa_fondeo= "";		}

				if (tipoArchivo.equals("NORMAL") ) {

					pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(origen,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(folio_solic,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(epo_relacionada,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_Emision,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_ven_solic,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_docto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(por_desc+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(recurso_garan,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(interes,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(tasa_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_operar,2),"formas",ComunesPDF.RIGHT);

					pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_solic_desc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_prestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_operacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tasa_fondeo,"formas",ComunesPDF.CENTER);
					if(bTipoFactoraje.equals("S")) {
						pdfDoc.setCell(tipoFactoraje,"formas",ComunesPDF.CENTER);
					}else
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencINFO.equals("S")){
						pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(banco_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(porc_beneficiario+"%","formas",ComunesPDF.CENTER);
						if(!importe_beneficiario.equals("")) {
							pdfDoc.setCell(Comunes.formatoDecimal(importe_beneficiario,2),"formas",ComunesPDF.RIGHT);
						}else {
							pdfDoc.setCell(importe_beneficiario,"formas",ComunesPDF.RIGHT);
						}
						if(!neto_recibir.equals("")) {
							pdfDoc.setCell(Comunes.formatoDecimal(neto_recibir,2),"formas",ComunesPDF.RIGHT);
						}else {
							pdfDoc.setCell(importe_beneficiario,"formas",ComunesPDF.RIGHT);
						}
					}else  {
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,7);
					}
					/*if(!bTipoFactoraje.equals("S")) {
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					}*/
					if (claveEstatusSolic.equals("16")) {	//16 = Aplicado a credito
						pdfDoc.setCell("Ver ","formas",ComunesPDF.CENTER);
					}else  {
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					}					
					pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER);		//FODEA 17
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					//pdfDoc.setCell("","formas",ComunesPDF.CENTER); //FODEA 17

				}//if (tipoArchivo.equals("NORMAL") ) {

				if (tipoArchivo.equals("INTERFASE") ) {

					pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(folio_solic,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cc_acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ic_oficina,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_pyme_epo_interno,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(in_numero_sirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(claveEPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(epo_relacionada,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(claveIF,"formas",ComunesPDF.CENTER);

					pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(razon_if,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_Emision,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_ven_solic,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					if(bTipoFactoraje.equals("S")) {
						pdfDoc.setCell(tipoFactoraje,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_docto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(por_desc+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(recurso_garan,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(interes,2),"formas",ComunesPDF.RIGHT);
					if(bTipoFactoraje.equals("N")) {
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("C","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_operar,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tasa_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(df_alta,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_solic_desc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_operacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_prestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_causa_rechazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);

					pdfDoc.setCell("D","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(referencia_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_campo1,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_campo2,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_campo3,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_campo4,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cg_campo5,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(origen,"formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 3);	
					
					if(bOperaFactorajeDist.equals("S")){
						pdfDoc.setCell("E","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(banco_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(porc_beneficiario,"formas",ComunesPDF.CENTER);
						if(!importe_beneficiario.equals("")) {
							pdfDoc.setCell(Comunes.formatoDecimal(importe_beneficiario,2),"formas",ComunesPDF.CENTER);
						}else {
							pdfDoc.setCell(importe_beneficiario,"formas",ComunesPDF.CENTER);
						}
						if(!neto_recibir.equals("")) {
							pdfDoc.setCell(Comunes.formatoDecimal(neto_recibir,2),"formas",ComunesPDF.CENTER);
						}else {
							pdfDoc.setCell(neto_recibir,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER, 3);
					}
				}

			}//while
			pdfDoc.addTable();

			if (tipoArchivo.equals("INTERFASE") ) {
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(12,100);

				String cve_moneda ="",   numRegistrosMN = "", numRegistrosDL ="", dblTotalMontoPesos = "", dblTotalDescuentoPesos  = "",
				dblTotalOperarPesos = "", dblTotalRecursosPesos = "", dblTotalInteresPesos = "", dblTotalMontoDolares = "",
				dblTotalDescuentoDolares  = "", dblTotalOperarDolares = "", dblTotalRecursosDolares = "", dblTotalInteresDolares = "";
				ConsSolicIfDE paginador = new ConsSolicIfDE();
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
				Registros vecColumnas = queryHelper.getResultCount(request);

				while (vecColumnas.next())	{
					 cve_moneda =	(vecColumnas.getString("cve_moneda")==null)?"":vecColumnas.getString("cve_moneda").trim();
					 if(cve_moneda.equals("1")) {
						 numRegistrosMN =	(vecColumnas.getString("num_doctos")==null)?"":vecColumnas.getString("num_doctos").trim();
						 dblTotalMontoPesos =	(vecColumnas.getString("total_montos_docto")==null)?"":vecColumnas.getString("total_montos_docto").trim();
						 dblTotalDescuentoPesos =	(vecColumnas.getString("total_montos_desc")==null)?"":vecColumnas.getString("total_montos_desc").trim();
						 dblTotalOperarPesos =	(vecColumnas.getString("monto_recibir_doctos")==null)?"":vecColumnas.getString("monto_recibir_doctos").trim();
						 dblTotalRecursosPesos =	(vecColumnas.getString("monto_recurso")==null)?"":vecColumnas.getString("monto_recurso").trim();
						 dblTotalInteresPesos =	(vecColumnas.getString("monto_interes")==null)?"":vecColumnas.getString("monto_interes").trim();
					 }
					  if(cve_moneda.equals("54")) {
						 numRegistrosDL =	(vecColumnas.getString("num_doctos")==null)?"":vecColumnas.getString("num_doctos").trim();
						 dblTotalMontoDolares =	(vecColumnas.getString("total_montos_docto")==null)?"":vecColumnas.getString("total_montos_docto").trim();
						 dblTotalDescuentoDolares =	(vecColumnas.getString("total_montos_desc")==null)?"":vecColumnas.getString("total_montos_desc").trim();
						 dblTotalOperarDolares =	(vecColumnas.getString("monto_recibir_doctos")==null)?"":vecColumnas.getString("monto_recibir_doctos").trim();
						 dblTotalRecursosDolares =	(vecColumnas.getString("monto_recurso")==null)?"":vecColumnas.getString("monto_recurso").trim();
						 dblTotalInteresDolares =	(vecColumnas.getString("monto_interes")==null)?"":vecColumnas.getString("monto_interes").trim();
					 }
				} //while (vecColumnas.next())

					pdfDoc.setCell("Total Registros MN","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(numRegistrosMN,0,false),"forma",ComunesPDF.CENTER);
					pdfDoc.setCell("Total Monto Documentos MN,","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalMontoPesos,2),"forma",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Recursos Garantia MN,","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalRecursosPesos,2) ,"forma",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Monto Descuento MN," ,"celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalDescuentoPesos,2) ,"forma",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Monto Interes MN,","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalInteresPesos,2) ,"forma",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Monto Operar MN,","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalOperarPesos,2) ,"forma",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Registros DL," ,"celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(numRegistrosDL,0) ,"formA",ComunesPDF.CENTER);
					pdfDoc.setCell("Total Monto Documentos DL," ,"celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalMontoDolares,2),"forma",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Recursos Garantia DL," ,"celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalRecursosDolares,2),"forma",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Monto Descuento DL," ,"celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalDescuentoDolares,2),"forma",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Monto Interes DL," ,"celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalInteresDolares,2) ,"forma",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Monto Operar DL," ,"celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalOperarDolares,2),"forma",ComunesPDF.RIGHT);
					pdfDoc.addTable();
			}

			pdfDoc.endDocument();

		} catch(Throwable e) {
            log.error(e);
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}

		}
		return nombreArchivo;


	}

	/**
	 * Metodo para generar archivos a partir de la consulta que regresa getDocumentQueryFile()
	 * request Httprequest
	 * rs Resultset con los resultados de la ejecuci�n del query obtenido mediante getDocumentQueryFile()
	 * path Ruta en donde colocara los archivos generados
	 * tipo Tipo/Variante de archivo a generar
	 *
	 * @return Cadena con el nombre del archivo generado
	 *
	 **/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		String nombreArchivoSinExtension = "";
		HttpSession session = request.getSession();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		String origen ="", folio_solic ="", nombre_pyme ="", epo_relacionada ="", num_docto ="", fecha_Emision ="",
				fecha_ven_solic ="", moneda ="", tipoFactoraje ="", monto_docto ="", por_desc ="", recurso_garan ="",
				monto_desc ="", interes  ="", tasa_pyme ="",  plazo ="", monto_operar ="", referencia_pyme ="", fecha_solic_desc ="",
				estatus ="", num_prestamo ="",  fecha_operacion ="", tasa_fondeo ="", beneficiario ="", banco_beneficiario ="",
				sucursal_beneficiario ="", cuenta_beneficiario ="", porc_beneficiario ="", importe_beneficiario ="",
				neto_recibir ="", referencia ="", claveEstatusSolic="", fechaVencDoc ="", codigoHASH="", cc_acuse="", ic_oficina = "",
				cg_pyme_epo_interno ="", in_numero_sirac ="",claveEPO ="", claveIF ="", razon_if ="", monedaClave ="",
				df_alta ="", cg_causa_rechazo="",cg_campo1 ="", cg_campo2 ="", cg_campo3 ="", cg_campo4 ="", cg_campo5 ="",
				rs_monto_ant ="", rs_docto_asoc ="", strFecha  ="", strMes	="", strHora ="", strMinuto ="", strSegundo ="", razon_pyme="";
				int i=0;
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				int total = 0;

		log.info("<----- tipoArchivo: " +tipoArchivo + "----->");
		if(tipo.equals("PDF")){
			nombreArchivo = generaPDF(request, rs, path);
		} else{
		try {
			nombreArchivoSinExtension = Comunes.cadenaAleatoria(16);
			if(tipo.equals("CSV") && (  tipoArchivo.equals("DETALLE") ||   tipoArchivo.equals("NORMAL")  || tipoArchivo.equals("IVARIABLE") || tipoArchivo.equals("IVARIABLED") ) ) {
			  nombreArchivo = nombreArchivoSinExtension + ".csv";
			  writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			  buffer = new BufferedWriter(writer);
			}

			if(tipo.equals("TXT") && tipoArchivo.equals("IFIJA") ) {
				nombreArchivo = nombreArchivoSinExtension + ".txt";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
			}

			if(tipo.equals("CSV") ||  tipo.equals("TXT")  ) {

					contenidoArchivo = new StringBuffer();

				if (tipoArchivo.equals("NORMAL") || tipoArchivo.equals("DETALLE")  /*|| tipoArchivo.equals("DETALLE_HASH")*/ ) {

					contenidoArchivo.append("Origen,Folio de Solicitud,Nombre del proveedor,EPO relacionada,No. de documento,Fecha Emision,Fecha Vencimiento Solicitud,Moneda");
					if(bTipoFactoraje.equals("S")) { 	contenidoArchivo.append(",Tipo Factoraje");  	}
					contenidoArchivo.append( ",Monto Documento,Fecha de solicitud,Tasa a PYME,Plazo,Monto a Operar,Estatus,Porcentaje de Descuento,Recurso en garantia,Monto a descontar,Intereses,Numero de Prestamo,Fecha de Operacion");
					if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencINFO.equals("S")) {
						contenidoArchivo.append(",Beneficiario,Banco Beneficiario, Sucursal Beneficiaria, Cuenta Beneficiaria ,% Beneficiario, Importe a Recibir del Beneficiario, Neto a Recibir PyME");
					}

					if(tipoArchivo.equals("DETALLE") /*|| tipoArchivo.equals("DETALLE_HASH")*/ ) {  	contenidoArchivo.append(",Referencia ,Fecha de Vencimiento del Documento");  		} 
					
					contenidoArchivo.append("\n");

				} else if (tipoArchivo.equals("IVARIABLE")  /*||  tipoArchivo.equals("IVARIABLE_HASH")  */
						  || tipoArchivo.equals("IVARIABLED")  /*|| tipoArchivo.equals("IVARIABLED_HASH") */
						  || tipoArchivo.equals("IFIJA") ) {

					GregorianCalendar grcFecha = new GregorianCalendar();
					java.util.Date	 dtmFecha = new java.util.Date();
					grcFecha.setTime(dtmFecha);
					strMes = Integer.toString(grcFecha.get(Calendar.MONTH) + 1);
					strMes = (strMes.length() > 1) ? strMes.toString() : "0" + strMes.toString();
					strHora = Integer.toString(grcFecha.get(Calendar.HOUR_OF_DAY));
					strHora = (strHora.length() > 1) ? strHora.toString() : "0" + strHora.toString();
					strMinuto = Integer.toString(grcFecha.get(Calendar.MINUTE));
					strMinuto = (strMinuto.length() > 1) ? strMinuto.toString() : "0" + strMinuto.toString();
					strSegundo = Integer.toString(grcFecha.get(Calendar.SECOND));
					strSegundo = (strSegundo.length() > 1) ? strSegundo.toString() : "0" + strSegundo.toString();

					strFecha = grcFecha.get(Calendar.DAY_OF_MONTH) + "/" +strMes.toString() + "/" +grcFecha.get(Calendar.YEAR);

					if( tipoArchivo.equals("IVARIABLE")  /*||  tipoArchivo.equals("IVARIABLE_HASH")  */
						  || tipoArchivo.equals("IVARIABLED")  /*|| tipoArchivo.equals("IVARIABLED_HASH")*/ ) {

						// Cabeza de la interface variable.

						contenidoArchivo.append("H," + strFecha + "," + strHora + ":" + strMinuto + ":" + strSegundo + ",Consulta de Solicitudes");

						// Titulos para el archivo variable.
						contenidoArchivo.append("\nD,Numero Folio, Numero Documento, Numero Acuse,Clave Oficina, Clave PYME," +
						" Numero SIRAC, Nombre PYME, Clave EPO, Nombre EPO, Clave IF," +
						" Nombre IF, Fecha Emision, Fecha Vencimiento Solicitud, Clave Moneda, Moneda");

						if(bTipoFactoraje.equals("S") || ( /*tipoArchivo.equals("IVARIABLED_HASH")  ||*/ tipoArchivo.equals("IVARIABLED") ) ){
							contenidoArchivo.append(", Tipo Factoraje");
						}

						contenidoArchivo.append(", Monto Documento,"+
								" Porcentaje Descuento, Recurso Garantia, Monto Descuento, Monto Interes," +
								" Monto Operar, Tasa Interes, Plazo, Clave Estatus, Estatus, Fecha Alta, Fecha Solicitud," +
								" Fecha Operacion SIRAC, Numero Prestamo, Causa Rechazo, Referencia,  " + 
								" CG_CAMPO1, CG_CAMPO2, CG_CAMPO3, CG_CAMPO4, CG_CAMPO5, Tipo Solicitud ");

						if(tipoArchivo.equals("IVARIABLED")  /*|| tipoArchivo.equals("IVARIABLED_HASH")*/ ){
							contenidoArchivo.append(",Monto Original, Documento Aplicado,Tasa de Fondeo");
						}

						if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencINFO.equals("S")) {
							contenidoArchivo.append(",Beneficiario,Banco Beneficiario, Sucursal Beneficiaria, Cuenta Beneficiaria"+
															" ,% Beneficiario, Importe a Recibir del Beneficiario, Neto a Recibir PyME");
						}						

						contenidoArchivo.append("\n");

					} else if (tipoArchivo.equals("IFIJA")) {
						// Cabeza del archivo en general.
						contenidoArchivo.append("H" + Comunes.formatoFijo(strFecha,10,"0","N") + strHora + ":" + strMinuto + ":" + strSegundo + "Consulta de Solicitudes");
						contenidoArchivo.append("\r\n");
					}
				}

				//if(!tipoArchivo.equals("DETALLE_HASH") &&  !tipoArchivo.equals("IVARIABLE_HASH")  &&  !tipoArchivo.equals("IVARIABLED_HASH") ) {
				buffer.write(contenidoArchivo.toString());
				contenidoArchivo = new StringBuffer();//Limpio
				//}
			}

			while(rs.next()){

				origen = (rs.getString("ORIGEN")==null)?"":rs.getString("ORIGEN");
				folio_solic = (rs.getString("FOLIO_SOLIC")==null)?"":rs.getString("FOLIO_SOLIC");
				nombre_pyme = (rs.getString("NOMBRE_PYME")==null)?"":rs.getString("NOMBRE_PYME");
				epo_relacionada = (rs.getString("EPO_RELACIONADA")==null)?"":rs.getString("EPO_RELACIONADA");
				num_docto = (rs.getString("NUM_DOCTO")==null)?"":rs.getString("NUM_DOCTO");
				fecha_Emision = (rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION");
				fecha_ven_solic = (rs.getString("FECHA_VENC_SOLIC")==null)?"":rs.getString("FECHA_VENC_SOLIC");
				moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
				tipoFactoraje = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE");
				monto_docto = (rs.getString("MONTO_DOCTO")==null)?"":rs.getString("MONTO_DOCTO");
				por_desc = (rs.getString("POR_DESC")==null)?"":rs.getString("POR_DESC");
				recurso_garan = (rs.getString("RECURSO_GARAN")==null)?"":rs.getString("RECURSO_GARAN");
				monto_desc = (rs.getString("MONTO_DESC")==null)?"":rs.getString("MONTO_DESC");
				interes = (rs.getString("INTERES")==null)?"":rs.getString("INTERES");
				tasa_pyme = (rs.getString("TASA_PYME")==null)?"":rs.getString("TASA_PYME");
				plazo = (rs.getString("PLAZO")==null)?"":rs.getString("PLAZO");
				monto_operar = (rs.getString("MONTO_OPERAR")==null)?"":rs.getString("MONTO_OPERAR");
				referencia_pyme = (rs.getString("REFERENCIA_PYME")==null)?"":rs.getString("REFERENCIA_PYME");	//FODEA 17
				fecha_solic_desc = (rs.getString("fecha_solic_desc")==null)?"":rs.getString("fecha_solic_desc");
				estatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
				num_prestamo = (rs.getString("NUM_PRESTAMO")==null)?"":rs.getString("NUM_PRESTAMO");
				fecha_operacion = (rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION");
				tasa_fondeo = (rs.getString("TASA_FONDEO")==null)?"":rs.getString("TASA_FONDEO");
				beneficiario = (rs.getString("BENEFICIARIO")==null)?"":rs.getString("BENEFICIARIO");
				banco_beneficiario = (rs.getString("BANCO_BENEFICIARIO")==null)?"":rs.getString("BANCO_BENEFICIARIO");
				sucursal_beneficiario = (rs.getString("SUCURSAL_BENEFICIARIA")==null)?"":rs.getString("SUCURSAL_BENEFICIARIA");
				cuenta_beneficiario = (rs.getString("CUENTA_BENEFICIARIA")==null)?"":rs.getString("CUENTA_BENEFICIARIA");
				porc_beneficiario = (rs.getString("PORC_BENEFICIARIO")==null)?"":rs.getString("PORC_BENEFICIARIO");
				importe_beneficiario = (rs.getString("IMPORTE_BENEFICIARIO")==null)?"":rs.getString("IMPORTE_BENEFICIARIO");
				neto_recibir  = (rs.getString("NETO_RECIBIR")==null)?"":rs.getString("NETO_RECIBIR");
				referencia = (rs.getString("REFERENCIA")==null)?"":rs.getString("REFERENCIA");
				claveEstatusSolic = (rs.getString("IC_ESTATUS")==null)?"":rs.getString("IC_ESTATUS");
				fechaVencDoc = (rs.getString("fechavencdoc")==null)?"":rs.getString("fechavencdoc");
				cc_acuse = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
				ic_oficina = (rs.getString("IC_OFICINA")==null)?"":rs.getString("IC_OFICINA");
				cg_pyme_epo_interno = (rs.getString("CG_PYME_EPO_INTERNO")==null)?"":rs.getString("CG_PYME_EPO_INTERNO");
				in_numero_sirac = (rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
				claveEPO = (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO");
				claveIF  = (rs.getString("IC_IF")==null)?"":rs.getString("IC_IF");
				razon_pyme  = (rs.getString("RAZON_PYME")==null)?"":rs.getString("RAZON_PYME");
				razon_if  = (rs.getString("RAZON_IF")==null)?"":rs.getString("RAZON_IF");
				monedaClave = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
				df_alta = (rs.getString("DF_ALTA")==null)?"":rs.getString("DF_ALTA");
				cg_causa_rechazo = (rs.getString("cg_causa_rechazo")==null)?"":rs.getString("cg_causa_rechazo");
				cg_campo1 = (rs.getString("CG_CAMPO1") == null) ? "" : rs.getString("CG_CAMPO1");
				cg_campo2 = (rs.getString("CG_CAMPO2") == null) ? "" : rs.getString("CG_CAMPO2");
				cg_campo3 = (rs.getString("CG_CAMPO3") == null) ? "" : rs.getString("CG_CAMPO3");
				cg_campo4 = (rs.getString("CG_CAMPO4") == null) ? "" : rs.getString("CG_CAMPO4");
				cg_campo5 = (rs.getString("CG_CAMPO5") == null) ? "" : rs.getString("CG_CAMPO5");
				rs_monto_ant = (rs.getString("fn_monto_ant") == null) ? "" : rs.getString("fn_monto_ant");
		      rs_docto_asoc = (rs.getString("ic_docto_asociado") == null) ? "" : rs.getString("ic_docto_asociado");
				String ct_referencia=   (rs.getString("ct_referencia") == null) ? "" : rs.getString("ct_referencia");
				if(num_prestamo.equals("0")) {  num_prestamo= "";		}
				if(tasa_fondeo.equals("0")) {  tasa_fondeo= "";		}

				if (tipoArchivo.equals("NORMAL") || tipoArchivo.equals("DETALLE")  /*|| tipoArchivo.equals("DETALLE_HASH")*/ ) {

					contenidoArchivo.append(origen+",No. "+folio_solic+","+nombre_pyme.replace(',',' ')+","+epo_relacionada.replace(',',' ')+","+
										num_docto+","+fecha_Emision+","+fecha_ven_solic+","+moneda+",");

					if(bTipoFactoraje.equals("S")) {  	contenidoArchivo.append(tipoFactoraje +",");	}

					contenidoArchivo.append(Comunes.formatoDecimal(monto_docto,2,false)+","+
													fecha_solic_desc+","+
													tasa_pyme+","+plazo +","+
													Comunes.formatoDecimal(monto_operar,2,false)+","+
													estatus+","+por_desc+","+
													Comunes.formatoDecimal(recurso_garan,2,false)+","+
													Comunes.formatoDecimal(monto_desc,2,false)+","+
													Comunes.formatoDecimal(interes,2,false)+","+
													num_prestamo+","+
													fecha_operacion+",");

					if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencINFO.equals("S")) {
						contenidoArchivo.append(beneficiario.replace(',',' ')+","+banco_beneficiario.replace(',',' ')+","+
						sucursal_beneficiario.replace(',',' ')+","+cuenta_beneficiario+","+porc_beneficiario+",");
						if(!importe_beneficiario.equals("")) {
							contenidoArchivo.append(Comunes.formatoDecimal(importe_beneficiario,2,false)+",");
						}else {
							contenidoArchivo.append(importe_beneficiario+",");
						}
						if(!neto_recibir.equals("")) {
							contenidoArchivo.append(Comunes.formatoDecimal(neto_recibir,2,false)+",");
						} else {
							contenidoArchivo.append(neto_recibir+",");
						}

					}
					if( claveEstatusSolic.equals("1") || claveEstatusSolic.equals("3") || claveEstatusSolic.equals("5") ){
						//contenidoArchivo.append(referencia.replace(',',' ') +", ");
					}else{
						//contenidoArchivo.append(" N/A ," );
					}

					if(tipoArchivo.equals("DETALLE") /*|| tipoArchivo.equals("DETALLE_HASH")*/ ) {
						contenidoArchivo.append( referencia_pyme.replace(',',' ') +","+  fechaVencDoc+",");
					}
					
					contenidoArchivo.append("\n");

				} else if (tipoArchivo.equals("IVARIABLE") /*||  tipoArchivo.equals("IVARIABLE_HASH") */
						  || tipoArchivo.equals("IVARIABLED")  /*|| tipoArchivo.equals("IVARIABLED_HASH")*/ ) {

					contenidoArchivo.append("D,"+folio_solic+","+num_docto +","+cc_acuse+","+ic_oficina+","+
													cg_pyme_epo_interno+","+in_numero_sirac +","+nombre_pyme.replace(',',' ')+","+claveEPO +","+epo_relacionada.replace(',',' ')+","+
													claveIF+","+razon_if.replace(',',' ') +","+fecha_Emision+","+fecha_ven_solic+","+monedaClave+","+moneda+",");

					if(bTipoFactoraje.equals("S") || ( /*tipoArchivo.equals("IVARIABLED_HASH")  ||*/ tipoArchivo.equals("IVARIABLED") ) ){
						contenidoArchivo.append(tipoFactoraje +",");
					}

					contenidoArchivo.append(Comunes.formatoDecimal(monto_docto,2,false)+","+
													por_desc+","+
													Comunes.formatoDecimal(recurso_garan,2,false)+","+
													Comunes.formatoDecimal(monto_desc,2,false)+","+
													Comunes.formatoDecimal(interes,2,false)+","+
													Comunes.formatoDecimal(monto_operar,2,false)+","+
													tasa_pyme+","+plazo +","+claveEstatusSolic+","+
													estatus+","+df_alta +","+fecha_solic_desc +","+fecha_operacion +","+
													num_prestamo+","+cg_causa_rechazo.replace(',',' ')+","+
													referencia_pyme.replace(',',' ')+","+
													cg_campo1 + "," + cg_campo2 + "," + cg_campo3 + "," + cg_campo4 + "," +
													cg_campo5 + "," + origen+",");

					if(tipoArchivo.equals("IVARIABLED")  /*||  tipoArchivo.equals("IVARIABLED_HASH")*/  ) {
						contenidoArchivo.append(Comunes.formatoDecimal(rs_monto_ant,2,false) + "," + 	rs_docto_asoc+","+tasa_fondeo+",");
					}

					if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencINFO.equals("S")) {
						contenidoArchivo.append(beneficiario.replace(',',' ')+","+banco_beneficiario.replace(',',' ')+","+
						sucursal_beneficiario.replace(',',' ')+","+cuenta_beneficiario+","+porc_beneficiario+",");

						if(!importe_beneficiario.equals("")) {
							contenidoArchivo.append(Comunes.formatoDecimal(importe_beneficiario,2,false)+",");
						}else {
							contenidoArchivo.append(importe_beneficiario+",");
						}
						if(!neto_recibir.equals("")) {
							contenidoArchivo.append(Comunes.formatoDecimal(neto_recibir,2,false)+",");
						} else {
							contenidoArchivo.append(neto_recibir+",");
						}
					}
										
					 
					contenidoArchivo.append("\n");
				} else if (tipoArchivo.equals("IFIJA")) {
					// Generacion de archivo Fijo.
					contenidoArchivo.append("D" +
						Comunes.formatoFijo(folio_solic,11," ","A") +
						Comunes.formatoFijo(num_docto,15," ","A") +
						Comunes.formatoFijo(cc_acuse,14," ","A") +
						Comunes.formatoFijo(ic_oficina,2," ","A") +
						Comunes.formatoFijo(cg_pyme_epo_interno,25," ","A") +
						Comunes.formatoFijo(in_numero_sirac,12,"0","") +
						//Comunes.formatoFijo(razon_pyme,100," ","A") +
						Comunes.formatoFijo(nombre_pyme,100," ","A") +
						Comunes.formatoFijo(claveEPO,6," ","A") +
						Comunes.formatoFijo(epo_relacionada,100," ","A") +
						Comunes.formatoFijo(claveIF,6," ","A") +
						Comunes.formatoFijo(razon_if,100," ","A") + //Anteriormente era 60 el tama�o pero se ajust� en el Fodea 014-2013 solo en la versi�n nueva
						Comunes.formatoFijo(fecha_Emision,10," ","") +
						Comunes.formatoFijo(fecha_ven_solic,10," ","") +
						Comunes.formatoFijo(new Integer(monedaClave).toString(),3," ","A") +
						Comunes.formatoFijo(moneda,30," ","A"));

					contenidoArchivo.append(Comunes.formatoFijo(tipoFactoraje,1," ","A"));
					contenidoArchivo.append(Comunes.formatoFijo(Comunes.formatoDecimal(monto_docto,2,false),15,"0","") +
													Comunes.formatoFijo(Comunes.formatoDecimal(por_desc,0,false),3,"0","") +
													Comunes.formatoFijo(Comunes.formatoDecimal(recurso_garan,2,false),15,"0","") +
													Comunes.formatoFijo(Comunes.formatoDecimal(monto_operar,2,false),15,"0","") +
													Comunes.formatoFijo(Comunes.formatoDecimal(interes,2,false),15,"0","") +
													Comunes.formatoFijo(Comunes.formatoDecimal(monto_desc,2,false),15,"0","") +
													Comunes.formatoFijo(Comunes.formatoDecimal(tasa_pyme,5,false),9,"0","") +
													Comunes.formatoFijo(plazo,3,"0","") +
													Comunes.formatoFijo(new Integer(claveEstatusSolic).toString(),2," ","A") +
													Comunes.formatoFijo(estatus,20," ","A") +
													Comunes.formatoFijo(df_alta,10," ","") +
													Comunes.formatoFijo(fecha_solic_desc,10," ","") +
													Comunes.formatoFijo(fecha_operacion,10," ","") +
													Comunes.formatoFijo(num_prestamo,8,"0","") +
													Comunes.formatoFijo(cg_causa_rechazo,100," ","A") +													
													Comunes.formatoFijo(referencia_pyme,50," ","A") + //FODEA 17
													Comunes.formatoFijo(cg_campo1,50," ","A") +
													Comunes.formatoFijo(cg_campo2,50," ","A") +
													Comunes.formatoFijo(cg_campo3,50," ","A") +
													Comunes.formatoFijo(cg_campo4,50," ","A") +
													Comunes.formatoFijo(cg_campo5,50," ","A") +
													Comunes.formatoFijo(origen,7," ","A"));
					if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencINFO.equals("S")) {
						contenidoArchivo.append(Comunes.formatoFijo(beneficiario,60," ","")+
						Comunes.formatoFijo(banco_beneficiario,40," ","")+
						Comunes.formatoFijo(sucursal_beneficiario,40," ","")+
						Comunes.formatoFijo(cuenta_beneficiario,15," ","")+
						Comunes.formatoFijo(porc_beneficiario ,6,"0","2")+
						Comunes.formatoFijo(importe_beneficiario,19,"0","2")+
						Comunes.formatoFijo(neto_recibir,19,"0","2"));
					}
					contenidoArchivo.append("\r\n");
				}

				total++;
				if(total==1000){
					total=0;
					//if(!tipoArchivo.equals("DETALLE_HASH") &&    !tipoArchivo.equals("IVARIABLE_HASH") &&  !tipoArchivo.equals("IVARIABLED_HASH")  ) {
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio
					//}
				}

			} //while(rs.next()){

			//if(!tipoArchivo.equals("DETALLE_HASH") &&    !tipoArchivo.equals("IVARIABLE_HASH") &&  !tipoArchivo.equals("IVARIABLED_HASH")  ) {
			buffer.write(contenidoArchivo.toString());
			contenidoArchivo = new StringBuffer();//Limpio
			//}

			if (tipoArchivo.equals("IVARIABLE") /*||tipoArchivo.equals("IVARIABLE_HASH")*/  ||  tipoArchivo.equals("IVARIABLED")  /*||  tipoArchivo.equals("IVARIABLED_HASH")*/  ) {

				String cve_moneda ="",   numRegistrosMN = "", numRegistrosDL ="", dblTotalMontoPesos = "", dblTotalDescuentoPesos  = "",
				dblTotalOperarPesos = "", dblTotalRecursosPesos = "", dblTotalInteresPesos = "", dblTotalMontoDolares = "",
				dblTotalDescuentoDolares  = "", dblTotalOperarDolares = "", dblTotalRecursosDolares = "", dblTotalInteresDolares = "";
				ConsSolicIfDE paginador = new ConsSolicIfDE();
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
				Registros vecColumnas = queryHelper.getResultCount(request);

				while (vecColumnas.next())	{
					 cve_moneda =	(vecColumnas.getString("cve_moneda")==null)?"":vecColumnas.getString("cve_moneda").trim();
					 if(cve_moneda.equals("1")) {
						 numRegistrosMN =	(vecColumnas.getString("num_doctos")==null)?"":vecColumnas.getString("num_doctos").trim();
						 dblTotalMontoPesos =	(vecColumnas.getString("total_montos_docto")==null)?"":vecColumnas.getString("total_montos_docto").trim();
						 dblTotalDescuentoPesos =	(vecColumnas.getString("total_montos_desc")==null)?"":vecColumnas.getString("total_montos_desc").trim();
						 dblTotalOperarPesos =	(vecColumnas.getString("monto_recibir_doctos")==null)?"":vecColumnas.getString("monto_recibir_doctos").trim();
						 dblTotalRecursosPesos =	(vecColumnas.getString("monto_recurso")==null)?"":vecColumnas.getString("monto_recurso").trim();
						 dblTotalInteresPesos =	(vecColumnas.getString("monto_interes")==null)?"":vecColumnas.getString("monto_interes").trim();
					 }
					  if(cve_moneda.equals("54")) {
						 numRegistrosDL =	(vecColumnas.getString("num_doctos")==null)?"":vecColumnas.getString("num_doctos").trim();
						 dblTotalMontoDolares =	(vecColumnas.getString("total_montos_docto")==null)?"":vecColumnas.getString("total_montos_docto").trim();
						 dblTotalDescuentoDolares =	(vecColumnas.getString("total_montos_desc")==null)?"":vecColumnas.getString("total_montos_desc").trim();
						 dblTotalOperarDolares =	(vecColumnas.getString("monto_recibir_doctos")==null)?"":vecColumnas.getString("monto_recibir_doctos").trim();
						 dblTotalRecursosDolares =	(vecColumnas.getString("monto_recurso")==null)?"":vecColumnas.getString("monto_recurso").trim();
						 dblTotalInteresDolares =	(vecColumnas.getString("monto_interes")==null)?"":vecColumnas.getString("monto_interes").trim();
					 }
				} //while (vecColumnas.next())

				contenidoArchivo.append("T," +
							"Total Registros MN,"+Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosMN,0,false), 12, "0","") + "," +
							"Total Monto Documentos MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoPesos,2,false), 15, "0","") + "," +
							"Total Recursos Garantia MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosPesos,2,false), 15, "0","") + "," +
							"Total Monto Descuento MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoPesos,2,false), 15, "0","") + "," +
							"Total Monto Interes MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresPesos,2,false), 15, "0","") + "," +
							"Total Monto Operar MN," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarPesos,2,false), 15, "0","") + "," +
							"Total Registros DL," + Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosDL,0,false), 12, "0","") + "," +
							"Total Monto Documentos DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoDolares,2,false), 15, "0","") + "," +
							"Total Recursos Garantia DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosDolares,2,false), 15, "0","") + "," +
							"Total Monto Descuento DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoDolares,2,false), 15, "0","") + "," +
							"Total Monto Interes DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresDolares,2,false), 15, "0","") + "," +
							"Total Monto Operar DL," + Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarDolares,2,false), 15, "0","") +
							"\n");

			}// if (tipoArchivo.equals("IVARIABLE") )



			if (tipoArchivo.equals("IFIJA")  ) {

				String cve_moneda ="",   numRegistrosMN = "", numRegistrosDL ="", dblTotalMontoPesos = "", dblTotalDescuentoPesos  = "",
				dblTotalOperarPesos = "", dblTotalRecursosPesos = "", dblTotalInteresPesos = "", dblTotalMontoDolares = "",
				dblTotalDescuentoDolares  = "", dblTotalOperarDolares = "", dblTotalRecursosDolares = "", dblTotalInteresDolares = "";
				ConsSolicIfDE paginador = new ConsSolicIfDE();
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS(paginador);
				Registros vecColumnas = queryHelper.getResultCount(request);

				while (vecColumnas.next())	{
					 cve_moneda =	(vecColumnas.getString("cve_moneda")==null)?"":vecColumnas.getString("cve_moneda").trim();
					 if(cve_moneda.equals("1")) {
						 numRegistrosMN =	(vecColumnas.getString("num_doctos")==null)?"":vecColumnas.getString("num_doctos").trim();
						 dblTotalMontoPesos =	(vecColumnas.getString("total_montos_docto")==null)?"":vecColumnas.getString("total_montos_docto").trim();
						 dblTotalDescuentoPesos =	(vecColumnas.getString("total_montos_desc")==null)?"":vecColumnas.getString("total_montos_desc").trim();
						 dblTotalOperarPesos =	(vecColumnas.getString("monto_recibir_doctos")==null)?"":vecColumnas.getString("monto_recibir_doctos").trim();
						 dblTotalRecursosPesos =	(vecColumnas.getString("monto_recurso")==null)?"":vecColumnas.getString("monto_recurso").trim();
						 dblTotalInteresPesos =	(vecColumnas.getString("monto_interes")==null)?"":vecColumnas.getString("monto_interes").trim();
					 }
					  if(cve_moneda.equals("54")) {
						 numRegistrosDL =	(vecColumnas.getString("num_doctos")==null)?"":vecColumnas.getString("num_doctos").trim();
						 dblTotalMontoDolares =	(vecColumnas.getString("total_montos_docto")==null)?"":vecColumnas.getString("total_montos_docto").trim();
						 dblTotalDescuentoDolares =	(vecColumnas.getString("total_montos_desc")==null)?"":vecColumnas.getString("total_montos_desc").trim();
						 dblTotalOperarDolares =	(vecColumnas.getString("monto_recibir_doctos")==null)?"":vecColumnas.getString("monto_recibir_doctos").trim();
						 dblTotalRecursosDolares =	(vecColumnas.getString("monto_recurso")==null)?"":vecColumnas.getString("monto_recurso").trim();
						 dblTotalInteresDolares =	(vecColumnas.getString("monto_interes")==null)?"":vecColumnas.getString("monto_interes").trim();
					 }
				} //while (vecColumnas.next())

				contenidoArchivo.append("T" +
						Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosMN,0,false), 12, "0","") +
						Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoPesos,2,false), 15, "0","") +
						Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosPesos,2,false), 15, "0","") +
						Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoPesos,2,false), 15, "0","") +
						Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresPesos,2,false), 15, "0","") +
						Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarPesos,2,false), 15, "0","") +
						Comunes.formatoFijo(Comunes.formatoDecimal(numRegistrosDL,0,false), 12, "0","") +
						Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalMontoDolares,2,false), 15, "0","") +
						Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalRecursosDolares,2,false), 15, "0","") +
						Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalDescuentoDolares,2,false), 15, "0","") +
						Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalInteresDolares,2,false), 15, "0","") +
						Comunes.formatoFijo(Comunes.formatoDecimal(dblTotalOperarDolares,2,false), 15, "0","") +
						"\n");

			}// if (tipoArchivo.equals("IVARIABLE") )

			if(tipo.equals("CSV") && (  tipoArchivo.equals("DETALLE") ||   tipoArchivo.equals("NORMAL")  || tipoArchivo.equals("IVARIABLE") || tipoArchivo.equals("IVARIABLED") ) ) {
				buffer.write(contenidoArchivo.toString());
				buffer.close();
				contenidoArchivo = new StringBuffer();//Limpio
			}


			if(tipo.equals("TXT") && tipoArchivo.equals("IFIJA") ) {
				buffer.write(contenidoArchivo.toString());
				buffer.close();
				contenidoArchivo = new StringBuffer();//Limpio
			}


			//if( tipoArchivo.equals("DETALLE_HASH")  || tipoArchivo.equals("IVARIABLE_HASH")  ||  tipoArchivo.equals("IVARIABLED_HASH") ) {
				//StringBuffer terminadorLinea = new StringBuffer("\r\n");
				//creaArchivo.make(contenidoArchivo.toString(), path, ".csv", terminadorLinea);
				try {

					String Ruta=path+nombreArchivo;
					java.io.File f = new java.io.File(Ruta);
					byte[] aByte = new byte[4096];//IHJ
					FileInputStream 	fis	= new FileInputStream(f);
					MessageDigest 		md 	= MessageDigest.getInstance("MD5");//IHJ
					md.reset();//IHJ
					while(( i = fis.read(aByte, 0, aByte.length)) != -1) { //IHJ
						md.update(aByte, 0, i);//IHJ
					}
					fis.close();

					byte[] digest = md.digest();//IHJ
					StringBuffer hexString = new StringBuffer();//IHJ
					for (int j=0;j<digest.length;j++) {//IHJ
						String aux = Integer.toHexString(0xFF & digest[j]);//IHJ
						if(aux.length()==1){//IHJ
							aux = "0"+aux;//IHJ
						}hexString.append(aux);//IHJ
					}//IHJ
					codigoHASH = hexString.toString();

				} catch (Exception e) {
				    log.error(e);
					log.info("Error al generar el hash del archivo." + e);
				}

				creaArchivo.make(codigoHASH, path, nombreArchivoSinExtension+"_MD5", ".txt");
				//creaArchivo.make(codigoHASH, path, ".txt");
				//nombreArchivo = creaArchivo.getNombre();
			//}

		} catch(Throwable e) {
		    log.error(e);
			throw new AppException("Error al generar el archivo ", e);
		}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}


/**
 * Como el m�todo crearCustomFile() ya tiene demasiado c�digo, cuando se genera el PDF con todos los registros,
 * es decir, sin implementar paginaci�n, el PDF se genera en este m�todo y solo se regresa el nomnre del archivo.
 * BY: Agust�n Bautista Ruiz
 * @param request
 * @param rs
 * @param path
 * @return nombre del archivo
 */
	public String generaPDF(HttpServletRequest request, java.sql.ResultSet rs, String path){

		log.info("generaPDF (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		int cols =0;

		String origen ="", folio_solic ="", nombre_pyme ="", epo_relacionada ="", num_docto ="", fecha_Emision ="",
				fecha_ven_solic ="", moneda ="", tipoFactoraje ="", monto_docto ="", por_desc ="", recurso_garan ="",
				monto_desc ="", interes  ="", tasa_pyme ="",  plazo ="", monto_operar ="", referencia_pyme ="", fecha_solic_desc ="",
				estatus ="", num_prestamo ="",  fecha_operacion ="", tasa_fondeo ="", beneficiario ="", banco_beneficiario ="",
				sucursal_beneficiario ="", cuenta_beneficiario ="", porc_beneficiario ="", importe_beneficiario ="",
				neto_recibir ="", referencia ="", claveEstatusSolic="",cc_acuse="", ic_oficina = "",
				cg_pyme_epo_interno ="", in_numero_sirac ="",claveEPO ="", claveIF ="", razon_if ="", monedaClave ="",
				df_alta ="", cg_causa_rechazo="",cg_campo1 ="", cg_campo2 ="", cg_campo3 ="", cg_campo4 ="", cg_campo5 ="",
				rs_monto_ant ="", rs_docto_asoc ="", ct_referencia ="";

		try{
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual   = fechaActual.substring(0,2);
			String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual  = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

			if(bTipoFactoraje.equals("N")){
				cols +=1;
			}
			if(bOperaFactorajeDist.equals("N") || bOperaFactorajeVencINFO.equals("N")){
				cols +=7;
			}

			if (tipoArchivo.equals("NORMAL") ) {
				pdfDoc.setLTable(17,100);
				pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Origen","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Folio de Solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre del Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO Relacionada","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. de Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento Solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Porcentaje de Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Recurso en garant�a","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Intereses","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa a PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a operar","celda01",ComunesPDF.CENTER);

				pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Solicitud del Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de Pr�stamo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa de Fondeo","celda01",ComunesPDF.CENTER);
				if(bTipoFactoraje.equals("S")) {
					pdfDoc.setLCell("Tipo Factoraje","celda01",ComunesPDF.CENTER);
				}
				else{
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
				}
				if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencINFO.equals("S")){
					pdfDoc.setLCell("Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Banco Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Sucursal Beneficiaria","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Cuenta Beneficiaria","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("% Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Importe a Recibir Beneficiario","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Neto a Recibir PyME","celda01",ComunesPDF.CENTER);
				}else  {
					pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER,7);
				}
				pdfDoc.setLCell("Detalle Aplic.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Referencia","celda01",ComunesPDF.CENTER); //FODEA 17
				pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
			}
			pdfDoc.setLHeaders();
			while(rs.next()){
				origen = (rs.getString("ORIGEN")==null)?"":rs.getString("ORIGEN");
				folio_solic = (rs.getString("FOLIO_SOLIC")==null)?"":rs.getString("FOLIO_SOLIC");
				nombre_pyme = (rs.getString("NOMBRE_PYME")==null)?"":rs.getString("NOMBRE_PYME");
				epo_relacionada = (rs.getString("EPO_RELACIONADA")==null)?"":rs.getString("EPO_RELACIONADA");
				num_docto = (rs.getString("NUM_DOCTO")==null)?"":rs.getString("NUM_DOCTO");
				fecha_Emision = (rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION");
				fecha_ven_solic = (rs.getString("FECHA_VENC_SOLIC")==null)?"":rs.getString("FECHA_VENC_SOLIC");
				moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
				tipoFactoraje = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE");
				monto_docto = (rs.getString("MONTO_DOCTO")==null)?"":rs.getString("MONTO_DOCTO");
				por_desc = (rs.getString("POR_DESC")==null)?"":rs.getString("POR_DESC");
				recurso_garan = (rs.getString("RECURSO_GARAN")==null)?"":rs.getString("RECURSO_GARAN");
				monto_desc = (rs.getString("MONTO_DESC")==null)?"":rs.getString("MONTO_DESC");
				interes = (rs.getString("INTERES")==null)?"":rs.getString("INTERES");
				tasa_pyme = (rs.getString("TASA_PYME")==null)?"":rs.getString("TASA_PYME");
				plazo = (rs.getString("PLAZO")==null)?"":rs.getString("PLAZO");
				monto_operar = (rs.getString("MONTO_OPERAR")==null)?"":rs.getString("MONTO_OPERAR");
				referencia_pyme = (rs.getString("REFERENCIA_PYME")==null)?"":rs.getString("REFERENCIA_PYME");   //FODEA 17
				fecha_solic_desc = (rs.getString("FECHA_SOLIC_DESC")==null)?"":rs.getString("FECHA_SOLIC_DESC");
				estatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
				num_prestamo = (rs.getString("NUM_PRESTAMO")==null)?"":rs.getString("NUM_PRESTAMO");
				fecha_operacion = (rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION");
				tasa_fondeo = (rs.getString("TASA_FONDEO")==null)?"":rs.getString("TASA_FONDEO");
				beneficiario = (rs.getString("BENEFICIARIO")==null)?"":rs.getString("BENEFICIARIO");
				banco_beneficiario = (rs.getString("BANCO_BENEFICIARIO")==null)?"":rs.getString("BANCO_BENEFICIARIO");
				sucursal_beneficiario = (rs.getString("SUCURSAL_BENEFICIARIA")==null)?"":rs.getString("SUCURSAL_BENEFICIARIA");
				cuenta_beneficiario = (rs.getString("CUENTA_BENEFICIARIA")==null)?"":rs.getString("CUENTA_BENEFICIARIA");
				porc_beneficiario = (rs.getString("PORC_BENEFICIARIO")==null)?"":rs.getString("PORC_BENEFICIARIO");
				importe_beneficiario = (rs.getString("IMPORTE_BENEFICIARIO")==null)?"":rs.getString("IMPORTE_BENEFICIARIO");
				neto_recibir  = (rs.getString("NETO_RECIBIR")==null)?"":rs.getString("NETO_RECIBIR");
				referencia = (rs.getString("REFERENCIA")==null)?"":rs.getString("REFERENCIA");
				claveEstatusSolic = (rs.getString("IC_ESTATUS")==null)?"":rs.getString("IC_ESTATUS");
				cc_acuse = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
				ic_oficina = (rs.getString("IC_OFICINA")==null)?"":rs.getString("IC_OFICINA");
				cg_pyme_epo_interno = (rs.getString("CG_PYME_EPO_INTERNO")==null)?"":rs.getString("CG_PYME_EPO_INTERNO");
				in_numero_sirac = (rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
				claveEPO = (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO");
				claveIF  = (rs.getString("IC_IF")==null)?"":rs.getString("IC_IF");
				razon_if  = (rs.getString("RAZON_IF")==null)?"":rs.getString("RAZON_IF");
				monedaClave = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
				df_alta = (rs.getString("DF_ALTA")==null)?"":rs.getString("DF_ALTA");
				cg_causa_rechazo = (rs.getString("cg_causa_rechazo")==null)?"":rs.getString("cg_causa_rechazo");
				cg_campo1 = (rs.getString("CG_CAMPO1") == null) ? "" : rs.getString("CG_CAMPO1");
				cg_campo2 = (rs.getString("CG_CAMPO2") == null) ? "" : rs.getString("CG_CAMPO2");
				cg_campo3 = (rs.getString("CG_CAMPO3") == null) ? "" : rs.getString("CG_CAMPO3");
				cg_campo4 = (rs.getString("CG_CAMPO4") == null) ? "" : rs.getString("CG_CAMPO4");
				cg_campo5 = (rs.getString("CG_CAMPO5") == null) ? "" : rs.getString("CG_CAMPO5");
				rs_monto_ant = (rs.getString("fn_monto_ant") == null) ? "" : rs.getString("fn_monto_ant");
				rs_docto_asoc = (rs.getString("ic_docto_asociado") == null) ? "" : rs.getString("ic_docto_asociado");
				ct_referencia =(rs.getString("ct_referencia") == null) ? "" : rs.getString("ct_referencia");
				if(num_prestamo.equals("0")){
					num_prestamo= "";
				}
				if(tasa_fondeo.equals("0")){
					tasa_fondeo= "";
				}
				if (tipoArchivo.equals("NORMAL") ) {
					pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(origen,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(folio_solic,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombre_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(epo_relacionada,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(num_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_Emision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_ven_solic,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto_docto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(por_desc+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(recurso_garan,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(interes,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(tasa_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto_operar,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_solic_desc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(num_prestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_operacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tasa_fondeo,"formas",ComunesPDF.CENTER);
					if(bTipoFactoraje.equals("S")) {
						pdfDoc.setLCell(tipoFactoraje,"formas",ComunesPDF.CENTER);
					}else
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					if(bOperaFactorajeDist.equals("S") || bOperaFactorajeVencINFO.equals("S")){
						pdfDoc.setLCell(beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(banco_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(sucursal_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(cuenta_beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(porc_beneficiario+"%","formas",ComunesPDF.CENTER);
						if(!importe_beneficiario.equals("")) {
							pdfDoc.setLCell(Comunes.formatoDecimal(importe_beneficiario,2),"formas",ComunesPDF.RIGHT);
						}else {
							pdfDoc.setLCell(importe_beneficiario,"formas",ComunesPDF.RIGHT);
						}
						if(!neto_recibir.equals("")) {
							pdfDoc.setLCell(Comunes.formatoDecimal(neto_recibir,2),"formas",ComunesPDF.RIGHT);
						}else {
							pdfDoc.setLCell(importe_beneficiario,"formas",ComunesPDF.RIGHT);
						}
					}else  {
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER,7);
					}
					if (claveEstatusSolic.equals("16")) {  //16 = Aplicado a credito
						pdfDoc.setLCell("Ver ","formas",ComunesPDF.CENTER);
					}else  {
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					}
					pdfDoc.setLCell(referencia_pyme,"formas",ComunesPDF.CENTER); //FODEA 17
					pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
				}//if (tipoArchivo.equals("NORMAL") )

			}//while
			pdfDoc.addLTable();
			pdfDoc.endDocument();

		} catch(Throwable e) {
		    log.error(e);
			throw new AppException("Error al generar el archivo PDF", e);
		}

		log.info("generaPDF (S)");
		return nombreArchivo;
	}

	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones
	 */
	public List getConditions() {  return conditions;  }
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}


/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }

  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_folio() {
		return ic_folio;
	}

	public void setIc_folio(String ic_folio) {
		this.ic_folio = ic_folio;
	}

	public String getDf_fecha_solicitudMin() {
		return df_fecha_solicitudMin;
	}

	public void setDf_fecha_solicitudMin(String df_fecha_solicitudMin) {
		this.df_fecha_solicitudMin = df_fecha_solicitudMin;
	}

	public String getDf_fecha_solicitudMax() {
		return df_fecha_solicitudMax;
	}

	public void setDf_fecha_solicitudMax(String df_fecha_solicitudMax) {
		this.df_fecha_solicitudMax = df_fecha_solicitudMax;
	}

	public String getDf_fecha_vencMin() {
		return df_fecha_vencMin;
	}

	public void setDf_fecha_vencMin(String df_fecha_vencMin) {
		this.df_fecha_vencMin = df_fecha_vencMin;
	}

	public String getDf_fecha_vencMax() {
		return df_fecha_vencMax;
	}

	public void setDf_fecha_vencMax(String df_fecha_vencMax) {
		this.df_fecha_vencMax = df_fecha_vencMax;
	}

	public String getIc_estatus_solic() {
		return ic_estatus_solic;
	}

	public void setIc_estatus_solic(String ic_estatus_solic) {
		this.ic_estatus_solic = ic_estatus_solic;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getFn_montoMin() {
		return fn_montoMin;
	}

	public void setFn_montoMin(String fn_montoMin) {
		this.fn_montoMin = fn_montoMin;
	}

	public String getFn_montoMax() {
		return fn_montoMax;
	}

	public void setFn_montoMax(String fn_montoMax) {
		this.fn_montoMax = fn_montoMax;
	}

	public String getTipoFactoraje() {
		return tipoFactoraje;
	}

	public void setTipoFactoraje(String tipoFactoraje) {
		this.tipoFactoraje = tipoFactoraje;
	}

	public String getTipoTasa() {
		return tipoTasa;
	}

	public void setTipoTasa(String tipoTasa) {
		this.tipoTasa = tipoTasa;
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public String getBTipoFactoraje() {
		return bTipoFactoraje;
	}

	public void setBTipoFactoraje(String bTipoFactoraje) {
		this.bTipoFactoraje = bTipoFactoraje;
	}

	public String getBOperaFactorajeDist() {
		return bOperaFactorajeDist;
	}

	public void setBOperaFactorajeDist(String bOperaFactorajeDist) {
		this.bOperaFactorajeDist = bOperaFactorajeDist;
	}

	public String getBOperaFactorajeVencINFO() {
		return bOperaFactorajeVencINFO;
	}

	public void setBOperaFactorajeVencINFO(String bOperaFactorajeVencINFO) {
		this.bOperaFactorajeVencINFO = bOperaFactorajeVencINFO;
	}

	public String getBOperaPubHash() {
		return bOperaPubHash;
	}

	public void setBOperaPubHash(String bOperaPubHash) {
		this.bOperaPubHash = bOperaPubHash;
	}

	public String getOrdenadoBy() {
		return ordenadoBy;
	}

	public void setOrdenadoBy(String ordenadoBy) {
		this.ordenadoBy = ordenadoBy;
	}


   public void setBOperaFactorajeAutomatico(String bOperaFactorajeAutomatico) {
      this.bOperaFactorajeAutomatico = bOperaFactorajeAutomatico;
   }


   public String getBOperaFactorajeAutomatico() {
      return bOperaFactorajeAutomatico;
   }

	public void setAplicaFloating(String aplicaFloating) {
		this.aplicaFloating = aplicaFloating;
	}

	public String getAplicaFloating() {
		return aplicaFloating;
	}

}
