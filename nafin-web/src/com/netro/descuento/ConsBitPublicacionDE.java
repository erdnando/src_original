package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.IQueryGeneratorThreadRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsBitPublicacionDE implements IQueryGeneratorPS, IQueryGeneratorThreadRegExtJS { //IQueryGeneratorRegExtJS

	public ConsBitPublicacionDE() {}

	/**
	 * Obtiene el query necesario para mostrar los totales.
	 * Si la cadena est� vacia, no se contempla los totales al realizar la
	 * paginaci�n
	 *
	 */
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		return "";
	}
	
	/**
	 * Obtiene el query necesario para mostrar los resultados en la consulta
	 *
	 * @param request request del que se obtienen datos necesarios para la consulta
	 * 		(En caso de ser necesario)
	 * @param ids Colecci�n de llaves a consultar
	 *
	 */
	public String getDocumentSummaryQueryForIds(
			HttpServletRequest request, Collection ids) {
		
		StringBuffer strSQL = new StringBuffer();
		
		StringBuffer lstdoctos = new StringBuffer();
		for (Iterator it = ids.iterator(); it.hasNext();){
			it.next();
			lstdoctos.append("?,");
		}
		lstdoctos = lstdoctos.delete(lstdoctos.length()-1,lstdoctos.length());
	
		strSQL.append(
				" SELECT " +
				" 	d.ig_numero_docto, d.fn_monto, " +
				" 	m.cd_nombre as moneda, a1.ic_usuario, " +
				" 	TO_CHAR(a1.df_fechahora_carga, 'dd/mm/yyyy hh:mi:ss') AS df_fechahora_carga," +
				" 	d.cc_acuse, d.ic_epo, "		+		
        "   TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO " +
				"   , 'ConsBitPublicacionDE' as PANTALLA " +				
				" FROM com_documento d, com_acuse1 a1, comcat_moneda m " +
				" WHERE " +
				" 	d.cc_acuse = a1.cc_acuse " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_documento in (" + lstdoctos + ") ");
		
		return strSQL.toString();
	}
	
	/**
	 * Obtiene el query para determinar unicamente las llaves 
	 * de la informaci�n a consultar
	 * @param request Objeto request del jsp, de donde se obtienen 
	 * 		los parametros de busqueda
	 *
	 */
	public String getDocumentQuery(HttpServletRequest request) {
	
		String df_fechahora_carga_ini = (request.getParameter("df_fechahora_carga_ini")==null)?"":request.getParameter("df_fechahora_carga_ini");
		String df_fechahora_carga_fin = (request.getParameter("df_fechahora_carga_fin")==null)?"":request.getParameter("df_fechahora_carga_fin");
	
		String ic_epo = (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String ic_usuario = (request.getParameter("ic_usuario")==null)?"":request.getParameter("ic_usuario");
		String ig_numero_docto = (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String cc_acuse = (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String fn_monto_ini = (request.getParameter("fn_monto_ini")==null)?"":request.getParameter("fn_monto_ini");
		String fn_monto_fin = (request.getParameter("fn_monto_fin")==null)?"":request.getParameter("fn_monto_fin");
		String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
    String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo")==null)?"":request.getParameter("ic_banco_fondeo");  //FODEA 007 - 2009

		int coma = 0,criterio=0;
		String iNoCliente = (String)request.getSession().getAttribute("iNoCliente");
	
		StringBuffer strSQL = new StringBuffer();
		
		
		strSQL.append(
				" SELECT /*+ use_nl (a1,d) index(a1 in_com_acuse1_02_nuk) */ " +
				" 	d.ic_documento, 'ConsBitPublicacionDE' as PANTALLA " +
				" FROM com_acuse1 a1, com_documento d, comcat_epo e " +  //FODEA 007 -2009
				" WHERE " +
				" 	a1.df_fechahora_carga >= TO_DATE(?, 'dd/mm/yyyy') " +
				" 	AND a1.df_fechahora_carga < TO_DATE(?, 'dd/mm/yyyy') + 1 " +
				" 	AND a1.cc_acuse = d.cc_acuse " +
				" 	AND a1.ic_producto_nafin = 1 ");

		this.valoresBind.add(df_fechahora_carga_ini);
		this.valoresBind.add(df_fechahora_carga_fin);

		if (!ic_epo.equals("")) {
			strSQL.append(" 	AND d.ic_epo = ? ");
			this.valoresBind.add(new Integer(ic_epo));
		}
		if (!ic_usuario.equals("")) {
			strSQL.append(" 	AND a1.ic_usuario = ? ");
			this.valoresBind.add(ic_usuario);
		}
		if (!ig_numero_docto.equals("")) {
			strSQL.append(" 	AND d.ig_numero_docto = ? ");
			this.valoresBind.add(ig_numero_docto);
		}
		if (!cc_acuse.equals("")) {
			strSQL.append(" 	AND a1.cc_acuse = ? ");
			this.valoresBind.add(cc_acuse);
		}
		if (!fn_monto_ini.equals("") && !fn_monto_fin.equals("")) {
			strSQL.append(" 	AND d.fn_monto BETWEEN ? AND ? ");
			this.valoresBind.add(new Double(fn_monto_ini));
			this.valoresBind.add(new Double(fn_monto_fin));
		} else if (!fn_monto_ini.equals("") && fn_monto_fin.equals("")) {
			strSQL.append(" 	AND d.fn_monto = ? ");
			this.valoresBind.add(new Double(fn_monto_ini));
		}
		if (!ic_moneda.equals("")) {
			strSQL.append(" 	AND d.ic_moneda = ? ");
			this.valoresBind.add(new Integer(ic_moneda));
			
		}
//FODEA 007 - 2009
		if (!ic_banco_fondeo.equals("")) {
			strSQL.append(" 	AND d.ic_epo = e.ic_epo ");
      strSQL.append(" 	AND e.ic_banco_fondeo = ? ");
			this.valoresBind.add(new Integer(ic_banco_fondeo));
//FODEA 007 - 2009
		}
		strSQL.append(" ORDER BY a1.df_fechahora_carga, d.ig_numero_docto ");
		return strSQL.toString();
	}
	
	
	/**
	 * Obtiene el query necesario para generar el archivo
	 *
	 */
	public String getDocumentQueryFile(HttpServletRequest request){
		return "";
	}
	
	
	/**
	 * Obtiene la lista de valores que deben utilizarse para
	 * reemplazar las variables bind (?)
	 *
	 */
	public ArrayList getConditions(HttpServletRequest request) {
		return this.valoresBind;
	}
  
	/**
	 * De manera predeterminada el valor de numList = 1,
	 * El valor cambia si es necesario utilizar las mismas 
	 * condiciones varias veces. Por ejemplo, cuando se usa un UNION 
	 * y ambos queries usan las misma condiciones en el mismo orden
	 * Esto se ocupa para realizar adecuadamente la sutituci�n de variables
	 * bind
	 *
	 */
	public int getNumList(HttpServletRequest request){
		return numList;	
	}


	/**
	 * De manera predeterminada el valor de numList = 1,
	 * El valor cambia si es necesario utilizar las mismas 
	 * condiciones varias veces. Por ejemplo, cuando se usa un UNION 
	 * y ambos queryes usan las misma condiciones en el mismo orden
	 */
	private int numList = 1;

	private ArrayList valoresBind = new ArrayList();





/****************************************************************************
 * Actualizaci�n de la clase para la migraci�n a los estandes de ExtJs      *
 * ya que esta, se reutilizara para las pantallas nuevas.                   *
 ****************************************************************************/

	private List conditions;
	StringBuffer strQuery;
	private int countReg;
	//Variables de Sesi�n
	private String iNoCliente;

	//Condiciones de la consulta
	private String fechaRegIni;
	private String fechaRegFin;
	private String icBancoFondeo;
	private String icEPO;
	private String moneda;
	private String usuario;
	private String noDocumento;
	private String noAcuse;
	private String montoInicio;
	private String montoFin;

	private static final Log log = ServiceLocator.getInstance().getLog(ConsBitPublicacionDE.class);
	private String mostrarCampos;

	public String getQuery(){
		strQuery = new StringBuffer();
		conditions = new ArrayList();

		strQuery.append(" SELECT d.ic_documento || '' || d.ic_epo as Id, "+
							"         d.ig_numero_docto, d.fn_monto, " +
							"         m.cd_nombre as moneda, a1.ic_usuario, " +
							"         TO_CHAR(a1.df_fechahora_carga, 'dd/mm/yyyy hh:mi:ss') AS df_fechahora_carga, " +
							"         d.cc_acuse, d.ic_epo, " +
							"         TO_CHAR (d.DF_ENTREGA, 'dd/mm/yyyy') DF_ENTREGA, d.CG_TIPO_COMPRA, d.CG_CLAVE_PRESUPUESTARIA, d.CG_PERIODO, " +
							"         'ConsBitPublicacionDE' as PANTALLA " +
						  	"        ,(SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = d.cc_acuse) AS MUESTRA_VISOR " +
							"    FROM com_acuse1 a1, com_documento d, comcat_epo e, comcat_moneda m " +
							"   WHERE d.cc_acuse = a1.cc_acuse " +
							"     AND a1.ic_producto_nafin = 1 " +
							"     AND d.ic_moneda = m.ic_moneda " +
							"     AND d.ic_epo = e.ic_epo "
							);

		if (!fechaRegIni.equals("") && !fechaRegFin.equals("")) {
			strQuery.append ("   AND a1.df_fechahora_carga >= TO_DATE(?, 'dd/mm/yyyy') " +
									" AND a1.df_fechahora_carga < TO_DATE(?, 'dd/mm/yyyy') + 1 "
								);
			conditions.add(fechaRegIni);
			conditions.add(fechaRegFin);
		}

		if (!icBancoFondeo.equals("")) {
			strQuery.append(" 	AND e.ic_banco_fondeo = ? ");
			conditions.add(icBancoFondeo);
		}
		if (!icEPO.equals("")) {
			strQuery.append(" AND d.ic_epo = ? ");
			conditions.add(icEPO);
		}
		if (!moneda.equals("")) {
			strQuery.append(" AND d.ic_moneda = ? ");
			conditions.add(moneda);
		}
		if (!usuario.equals("")) {
			strQuery.append(" AND a1.ic_usuario = ? ");
			conditions.add(usuario);
		}
		if (!noDocumento.equals("")) {
			strQuery.append(" AND d.ig_numero_docto = ? ");
			conditions.add(noDocumento);
		}
		if (!noAcuse.equals("")) {
			strQuery.append(" AND a1.cc_acuse = ? ");
			conditions.add(noAcuse);
		}
		if (!montoInicio.equals("") && !montoFin.equals("")) {
			strQuery.append(" AND d.fn_monto BETWEEN ? AND ? ");
			conditions.add(montoInicio);
			conditions.add(montoFin);
		} else if (!montoInicio.equals("") && montoFin.equals("")) {
			strQuery.append(" AND d.fn_monto = ? ");
			conditions.add(montoInicio);
		}

	 return strQuery.toString();
	}

	public String getAggregateCalculationQuery() {
		conditions = new ArrayList();
		strQuery = new StringBuffer();

		log.debug("getAggregateCalculationQuery)"+strQuery.toString());
		log.debug("getAggregateCalculationQuery)"+conditions);

		return strQuery.toString();
	}

	public String getDocumentQuery() {
		conditions = new ArrayList();
		strQuery = new StringBuffer();

		strQuery.append(getQuery());
		strQuery.append("  ORDER BY a1.df_fechahora_carga, d.ig_numero_docto ");

		log.debug("getDocumentQuery"+strQuery.toString());
		log.debug("getDocumentQuery)"+conditions);

		return strQuery.toString();
	}

	public String getDocumentSummaryQueryForIds(List pageIds) {
		conditions = new ArrayList();
		strQuery = new StringBuffer();

		strQuery.append(getQuery());

		for(int i=0;i<pageIds.size();i++) {
			List lItem = (List)pageIds.get(i);
			if(i==0) {
				strQuery.append(" AND d.ic_documento || '' || d.ic_epo in ( ");
			}
			strQuery.append("?");
			conditions.add(new Long(lItem.get(0).toString()));
			if(i!=(pageIds.size()-1)) {
				strQuery.append(",");
			}else{
				strQuery.append(" ) ");
			}
		}

		strQuery.append("  ORDER BY a1.df_fechahora_carga, d.ig_numero_docto ");

		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);

		return strQuery.toString();
	}

	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		strQuery   = new StringBuffer();

		strQuery.append(getQuery());
		strQuery.append("  ORDER BY a1.df_fechahora_carga, d.ig_numero_docto ");

		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		return strQuery.toString();
	}


/**
 * En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
 * con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
 * de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
 * @param request HttpRequest empleado principalmente para obtener el objeto session
 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
 * @param path Ruta f�sica donde se generar� el archivo
 * @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
 * @return Cadena con la ruta del archivo generado
 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");
		String nombreArchivo = "";

		if(tipo.equals("PDF")){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
				if(!mostrarCampos.equals("S")){
					pdfDoc.setLTable(6,100);
				} else{
					pdfDoc.setLTable(10,100);
				}

				pdfDoc.setLCell("No. de Documento",     "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto",                "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda",               "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Usuario",              "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Publicaci�n", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Acuse",                "celda01",ComunesPDF.CENTER);

				if(mostrarCampos.equals("S")){
					pdfDoc.setLCell("Fecha de Recepci�n de Bienes y Servicios", "celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo de Compra (procedimiento)",           "celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Clasificador por Objeto del Gasto",        "celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Plazo M�ximo",                             "celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setLHeaders();
				countReg = 0;
				while (rs.next()){
					String noDocumento      = (rs.getString("IG_NUMERO_DOCTO")         == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
					String monto            = (rs.getString("FN_MONTO")                == null) ? "" : rs.getString("FN_MONTO");
					String moneda           = (rs.getString("MONEDA")                  == null) ? "" : rs.getString("MONEDA");
					String usuario          = (rs.getString("IC_USUARIO")              == null) ? "" : rs.getString("IC_USUARIO");
					String fechaPublicacion = (rs.getString("DF_FECHAHORA_CARGA")      == null) ? "" : rs.getString("DF_FECHAHORA_CARGA");
					String acuse            = (rs.getString("CC_ACUSE")                == null) ? "" : rs.getString("CC_ACUSE");
					String dfEntrega        = (rs.getString("DF_ENTREGA")              == null) ? "" : rs.getString("DF_ENTREGA");
					String tipoCompra       = (rs.getString("CG_TIPO_COMPRA")          == null) ? "" : rs.getString("CG_TIPO_COMPRA");
					String clasificacionOG  = (rs.getString("CG_CLAVE_PRESUPUESTARIA") == null) ? "" : rs.getString("CG_CLAVE_PRESUPUESTARIA");
					String plazoMaximo      = (rs.getString("CG_PERIODO")              == null) ? "" : rs.getString("CG_PERIODO");
					if (plazoMaximo.equals("0")){
						plazoMaximo = "";
					}
					pdfDoc.setLCell(noDocumento,"formas",ComunesPDF.CENTER);  
					//pdfDoc.setLCell("$ " + monto,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(monto,2) ,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(usuario,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(acuse,"formas",ComunesPDF.CENTER);
					if(mostrarCampos.equals("S")){
						pdfDoc.setLCell(dfEntrega,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(tipoCompra,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(clasificacionOG,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(plazoMaximo,"formas",ComunesPDF.CENTER);
					}
					countReg++;
					//System.out.println("Registros procesados: " + countReg);
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			} catch (Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}


   /** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		
		String nombreArchivo = "";
		
		try {
		 if(tipo.equals("PDF")){
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
				if(!mostrarCampos.equals("S")){
					pdfDoc.setLTable(6,100);
				}else {
				pdfDoc.setLTable(10,100);
				}
				
				pdfDoc.setLCell("No. de Documento", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Usuario","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Publicaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Acuse","celda01",ComunesPDF.CENTER);
				
				if(mostrarCampos.equals("S")){
					pdfDoc.setLCell("Fecha de Recepci�n de Bienes y Servicios","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo de Compra (procedimiento)","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Clasificador por Objeto del Gasto","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Plazo M�ximo","celda01",ComunesPDF.CENTER);						
				}
				
				while (reg.next()) {
					String noDocumento 		=	(reg.getString("IG_NUMERO_DOCTO") == null) ? "" 			: reg.getString("IG_NUMERO_DOCTO");
			      String monto 				= (reg.getString("FN_MONTO") == null) ? "" 						: reg.getString("FN_MONTO");
			      String moneda				=	(reg.getString("MONEDA") == null) ? "" 						: reg.getString("MONEDA");
			      String usuario 			= (reg.getString("IC_USUARIO") == null) ? ""						: reg.getString("IC_USUARIO");
			      String fechaPublicacion	=	(reg.getString("DF_FECHAHORA_CARGA") == null) ? "" 		: reg.getString("DF_FECHAHORA_CARGA");
			      String acuse 				= (reg.getString("CC_ACUSE") == null) ? "" 						: reg.getString("CC_ACUSE");
			      
					String dfEntrega 			= (reg.getString("DF_ENTREGA") == null) ? "" 					: reg.getString("DF_ENTREGA");
			      String tipoCompra 		= (reg.getString("CG_TIPO_COMPRA") == null) ? "" 				: reg.getString("CG_TIPO_COMPRA");
			      String clasificacionOG	=	(reg.getString("CG_CLAVE_PRESUPUESTARIA") == null) ? "" 	: reg.getString("CG_CLAVE_PRESUPUESTARIA");
			      String plazoMaximo 	= (reg.getString("CG_PERIODO") == null) ? "" 						: reg.getString("CG_PERIODO");
			      if (plazoMaximo.equals("0")) plazoMaximo = "";
					pdfDoc.setLCell(noDocumento,"formas",ComunesPDF.CENTER);  
//					pdfDoc.setLCell("$ " + monto,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(monto,2) ,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(usuario,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(acuse,"formas",ComunesPDF.CENTER);
					if(mostrarCampos.equals("S")){
					pdfDoc.setLCell(dfEntrega,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCompra,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(clasificacionOG,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazoMaximo,"formas",ComunesPDF.CENTER);
					
					}
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();
		 }
		}
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
	
		return nombreArchivo;
	}
	
	public List getConditions() {
		return conditions;
	}

	public void setFechaRegIni(String fechaRegIni) {
		this.fechaRegIni = fechaRegIni;
	}

	public String getFechaRegIni() {
		return fechaRegIni;
	}

	public void setFechaRegFin(String fechaRegFin) {
		this.fechaRegFin = fechaRegFin;
	}

	public String getFechaRegFin() {
		return fechaRegFin;
	}

	public void setIcBancoFondeo(String icBancoFondeo) {
		this.icBancoFondeo = icBancoFondeo;
	}

	public String getIcBancoFondeo() {
		return icBancoFondeo;
	}

	public void setIcEPO(String icEPO) {
		this.icEPO = icEPO;
	}

	public String getIcEPO() {
		return icEPO;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setNoDocumento(String noDocumento) {
		this.noDocumento = noDocumento;
	}

	public String getNoDocumento() {
		return noDocumento;
	}

	public void setNoAcuse(String noAcuse) {
		this.noAcuse = noAcuse;
	}

	public String getNoAcuse() {
		return noAcuse;
	}

	public void setMontoInicio(String montoInicio) {
		this.montoInicio = montoInicio;
	}

	public String getMontoInicio() {
		return montoInicio;
	}

	public void setMontoFin(String montoFin) {
		this.montoFin = montoFin;
	}

	public String getMontoFin() {
		return montoFin;
	}

	public String getMostrarCampos() {
		return mostrarCampos;
	}

	public void setMostrarCampos(String mostrarCampos) {
		this.mostrarCampos = mostrarCampos;
	}

	public int getCountReg() {
		return countReg;
	}

}