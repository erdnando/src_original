package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class RepCEstatusEpoDEbean implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

	private int iNoCambioEstatus;
	private String sesIdiomaUsuario;
	private String claveEPO;

	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(RepCEstatusEpoDEbean.class);
	
	public RepCEstatusEpoDEbean() { }

	

	public String getAggregateCalculationQuery() {
			log.info("getAggregateCalculationQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();

			log.debug("..:: qrySentencia "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("getAggregateCalculationQuery(S)");
			return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.debug("..:: qrySentencia "+qrySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}
			
		
	public String getDocumentQueryFile() {
			log.info("getDocumentQueryFile(E)");
			qrySentencia = new StringBuffer();
			conditions = new ArrayList();
		
			String hint = "";
		String sReferencia = ",'RepCEstatusEpoDEbean:getQueryPorEstatus("+iNoCambioEstatus+")' as PANTALLA";
		//Posfijo si es ingles
		//String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 
		
		//4 Negociable a Baja
		//5 Negociable a Descuento Fisico
		//6 Negociable a Pagado Anticipado
		// 29 Programado a Negociable Foda 25-28/04/2005 SMJ
		if(iNoCambioEstatus==4 || iNoCambioEstatus==5 || iNoCambioEstatus==6 || iNoCambioEstatus==29 || iNoCambioEstatus==40 ) {
			//hint = " /*+index(ce CP_COMHIS_CAMBIO_ESTATUS_PK)*/";
			qrySentencia.append(
				" select "+
				hint+
				" (DECODE (pe.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social) "   +
				" ,d.ig_numero_docto "   +
				" ,d.cc_acuse"   +
				" ,TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') "   +
				" ,TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') "   +
				" ,m.cd_nombre as cd_nombre"   +
				" ,d.fn_monto ");
				
			if(iNoCambioEstatus==4 || iNoCambioEstatus==40) {
				qrySentencia.append(
					",DECODE (d.ic_moneda, 1, 50.0, 54, 55.00000000000001) AS fn_porc_anticipo"
				);
			}//if(iNoCambioEstatus==4)
			
			qrySentencia.append(
				" ,d.ic_moneda"   +
				" ,d.cs_dscto_especial "   +
				" ,i2.cg_razon_social AS beneficiario "   +
				" ,d.fn_porc_beneficiario AS por_beneficiario"   +
				" ,(d.fn_porc_beneficiario * d.fn_monto) / 100 AS importe_a_recibir"+
				" ,TO_CHAR (d.df_fecha_venc_pyme, 'DD/MM/YYYY')AS df_fecha_venc_pyme " +
				"  ,es.CD_DESCRIPCION as cambioEstatus ");//sbQuery.append(
				
			qrySentencia.append(sReferencia);				
				
			qrySentencia.append(
				" FROM "   +
				" com_documento d "   +
				" ,comrel_pyme_epo pe "   +
				" ,comcat_pyme p "   +
				" ,comhis_cambio_estatus ce "   +
				" ,comcat_epo e "   +
				" ,comcat_if i2 "   +
				" ,comcat_moneda m "+
				", COMCAT_CAMBIO_ESTATUS  es "
			);
			qrySentencia.append(
				"  WHERE d.ic_documento = ce.ic_documento"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_epo = e.ic_epo"   +
				"    AND p.ic_pyme = pe.ic_pyme"   +
				"    AND d.ic_beneficiario = i2.ic_if (+)"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND TO_CHAR (ce.dc_fecha_cambio, 'DD/MM/YYYY') = TO_CHAR (SYSDATE, 'DD/MM/YYYY')"   +
				"    and ce.IC_CAMBIO_ESTATUS = es.IC_CAMBIO_ESTATUS  ");
				
				if(!claveEPO.equals("")) {
					qrySentencia.append("    AND d.ic_epo = ?"   +
															"    AND pe.ic_epo = ?"   );
					conditions.add(claveEPO);
					conditions.add(claveEPO);
				}	
			
				if(iNoCambioEstatus==4) {
					qrySentencia.append("    AND ce.ic_cambio_estatus in( 4,37 )");				
				}else  if(iNoCambioEstatus==5) {
					qrySentencia.append("    AND ce.ic_cambio_estatus in( 5,38 )");	
				}else  if(iNoCambioEstatus==6) {
					qrySentencia.append("    AND ce.ic_cambio_estatus in( 6,39 )");	
				}else  if(iNoCambioEstatus==40) {
					qrySentencia.append("    AND ce.ic_cambio_estatus in( 40 )");							
				}else{
				  qrySentencia.append("    AND ce.ic_cambio_estatus = ?");	
					conditions.add(String.valueOf(iNoCambioEstatus));	
				}
		}//if(iNoCambioEstatus==4 || iNoCambioEstatus==5 || iNoCambioEstatus==6 iNoCambioEstatus==29)
		

		//8 Modificacion de Montos y Fechas
		//28 Aplicaci�n de Notas de Credito
		if(iNoCambioEstatus==8 || iNoCambioEstatus==28) {
			qrySentencia.append(
				" select "+
				hint+
				" (DECODE (pe.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)"   +
				" ,d.ig_numero_docto"   +
				" ,ce.cc_acuse"   +
				" ,TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY')"   +
				" ,TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY')"   +
				" ,m.cd_nombre"   +		
				" ,d.fn_monto"   +
				" ,d.ic_moneda"   +				
				" ,ce.fn_monto_anterior"   +
				" ,ce.ct_cambio_motivo"   +				
				" ,d.fn_porc_anticipo"
			);
			if(iNoCambioEstatus==8) {
				qrySentencia.append(
					" ,d.ic_estatus_docto"	
				);
				
			}
			qrySentencia.append(				
				" ,d.cs_dscto_especial"
			);
			if(iNoCambioEstatus==8) {
				qrySentencia.append(
					" ,d.fn_monto_dscto"   +
					" ,i2.cg_razon_social AS beneficiario"   +
					" ,d.fn_porc_beneficiario AS por_beneficiario"   +
					" ,DECODE ( d.ic_estatus_docto, "   +
					" 	       3, ds.fn_importe_recibir_benef, "   +
					" 	       4, ds.fn_importe_recibir_benef,"   +
					"         11, ds.fn_importe_recibir_benef,"   +
					"         12, ds.fn_importe_recibir_benef,"   +
					"         16, ds.fn_importe_recibir_benef,"   +
					"         ((d.fn_porc_beneficiario * d.fn_monto) / 100)) AS importe_a_recibir"   +
					" ,TO_CHAR (ce.df_fecha_venc_anterior, 'dd/mm/yyyy') AS fechavencant"
				);
			} else if(iNoCambioEstatus==28) {
				qrySentencia.append(				
					" ,ce.fn_monto_nuevo"   +
					" ,i2.cg_razon_social AS beneficiario"   +
					" ,ce.ct_cambio_motivo"
				);
			}
			
			qrySentencia.append(" ,ce.dc_fecha_cambio");
			qrySentencia.append(" ,TO_CHAR (d.df_fecha_venc_pyme, 'DD/MM/YYYY') as df_fecha_venc_pyme " );	
			qrySentencia.append(" ,TO_CHAR (ce.df_fecha_venc_p_anterior, 'DD/MM/YYYY') AS df_fecha_venc_p_anterior " );
			qrySentencia.append(" ,TO_CHAR (ce.df_fecha_venc_p_nueva, 'DD/MM/YYYY') AS df_fecha_venc_p_nueva " );					
			qrySentencia.append(sReferencia);	
			qrySentencia.append(" ,ma.CG_RAZON_SOCIAL as mandante");  // Fodea 023 2009
			qrySentencia.append(" ,tf.cg_nombre as TIPO_FACTORAJE");  // Fodea 023 2009	
			
			qrySentencia.append(
				" FROM com_documento d"   +
				" ,com_docto_seleccionado ds"   +
				" ,comrel_pyme_epo pe"   +
				" ,comcat_pyme p"   +
				" ,comhis_cambio_estatus ce "   +
				" ,comcat_epo e "   +
				" ,comcat_if i2 "   +
				" ,comcat_moneda m "+
				" ,COMCAT_MANDANTE ma "+	  // Fodea 023 2009	
				" ,COMCAT_TIPO_FACTORAJE tf "+					
				" WHERE d.ic_documento = ce.ic_documento"   +
				" AND d.ic_documento = ds.ic_documento (+)"   +
				" AND d.ic_pyme = p.ic_pyme"   +
				" AND p.ic_pyme = pe.ic_pyme"   +
				" AND d.ic_beneficiario = i2.ic_if (+)"   +
				" AND d.ic_epo = e.ic_epo"   +
				" AND d.ic_epo = pe.ic_epo"   +
				" AND d.ic_moneda = m.ic_moneda"   +
				" AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "+
				" AND d.cs_dscto_especial = tf.cc_tipo_factoraje ");
				if(!claveEPO.equals("")) {
					qrySentencia.append(" AND d.ic_epo = ?" );
					conditions.add(claveEPO);					
				}
					
				qrySentencia.append(" AND TO_CHAR (ce.dc_fecha_cambio, 'DD/MM/YYYY') = TO_CHAR (SYSDATE, 'DD/MM/YYYY')");			
				
				if(iNoCambioEstatus!=0){
					qrySentencia.append(" AND ce.ic_cambio_estatus = ?  "  );
					conditions.add(String.valueOf(iNoCambioEstatus));	
				}
			
				
			if(iNoCambioEstatus==28)
				qrySentencia.append(" order by d.ig_numero_docto, ce.dc_fecha_cambio");
		}//if(iNoCambioEstatus==8 || iNoCambioEstatus==28)
		
	
		log.debug("..:: getQueryPorCambiosEstatus ::--"+iNoCambioEstatus+"----" +qrySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
		
		}
			
			
			
	public String getDocumentQuery() {
	log.info("getDocumentQuery(E)");
			qrySentencia = new StringBuffer();
			conditions = new ArrayList();
	
		log.debug("..:: qrySentencia: "+qrySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();			
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		boolean bOperaFactorajeVencido = false;
		boolean bOperaFactConMandato = false;
		boolean bOperaFactVencimientoInfonavit = false;
		boolean dolaresDocto = false;
		boolean nacionalDocto = false;
		int i = 0, ic_moneda = 0,  columnas =0,  colum =0, columM = 0;		
		String cg_razon_social = "", ig_numero_docto = "", cc_acuse = "", df_fecha_docto = "", df_fecha_venc = "",
					df_fecha_venc_pyme = "", cd_nombre = "", beneficiario   = "", por_beneficiario  = "", importe_a_recibir = "",
					mandante = "", cambioEstatus ="", strAforo ="0", strAforoDL ="", tipoFactoraje = "", tipoFactorajeDesc = "",
					fechaVencAnt ="", fechaVencAnt_pyme ="",  montoAnterior ="",  causa ="", titulo ="";
		double fn_monto = 0,  dblPorcentaje = 0, df_total_mn = 0,  df_total_dolares = 0, df_total_ant_mn = 0.0,
					 df_total_ant_dol = 0.0, MontoAnterior2 = 0.0, MontoNuevo = 0.0, montoDescuento = 0.0, total_MontoAnterior_mn=0.0,
					 total_MontoNuevo_mn=0.0, total_MontoAnterior_dolares=0.0, total_MontoNuevo_dolares=0.0, total_Monto_descontar_mn=0.0,
					 total_Monto_descontar_dolares=0.0;	
		
		strAforo = (String)session.getAttribute("strAforo"); 
		strAforoDL = (String)session.getAttribute("strAforoDL");		
		BigDecimal porcentaje=new BigDecimal(strAforo); 
		BigDecimal porcentajeDL=new BigDecimal(strAforoDL); 
		BigDecimal mtodoc=null;
		BigDecimal mtodesc=new BigDecimal("0.0");
		BigDecimal nac_desc=new BigDecimal("0.0"); BigDecimal dol_desc=new BigDecimal("0.0");	
		BigDecimal mnMontoModif=new BigDecimal("0.0");
		BigDecimal dolMontoModif=new BigDecimal("0.0");
		
		if ("PDF".equals(tipo)) {	
			try {
				ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);
				
				String sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(claveEPO);			
			
				ParametrosDescuento BeanParamDscto = ServiceLocator.getInstance().lookup("ParametrosDescuentoEJB", ParametrosDescuento.class);
				
				Hashtable alParamEPO1 = new Hashtable(); 
				alParamEPO1 = BeanParamDscto.getParametrosEPO(claveEPO,1);	
				if (alParamEPO1!=null) {
					 bOperaFactConMandato = ("N".equals(alParamEPO1.get("PUB_EPO_OPERA_MANDATO").toString()))?false:true;
					 bOperaFactVencimientoInfonavit = ("N".equals(alParamEPO1.get("PUB_EPO_VENC_INFONAVIT").toString()))?false:true;
				}
				String firmaMancomunada= BeanParamDscto.getOperaFirmaMancomunada(claveEPO, 1);
		
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				if(iNoCambioEstatus==5 ) 	titulo ="Estatus : Descuento f�sico";
				if(iNoCambioEstatus==4 ) 	titulo ="Estatus : Baja";
				if(iNoCambioEstatus==6 ) 	titulo ="Estatus : Pagado Anticipado";
				
				if(iNoCambioEstatus==5 ||  iNoCambioEstatus==4  ||  iNoCambioEstatus==6) {
					 columnas =12;
					 if(!"".equals(sFechaVencPyme)) { 	columnas =columnas+1;			 }
					 if(firmaMancomunada.equals("S")){ 	columnas =columnas+1;			 }
						
					pdfDoc.setTable(columnas, 100);
					pdfDoc.setCell(titulo,"celda01",ComunesPDF.LEFT, columnas); 
					pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER); 	
					pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha Emisi�n ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
					if(!"".equals(sFechaVencPyme)) {
					pdfDoc.setCell(" Fecha Vencimiento Proveedor","celda01",ComunesPDF.CENTER);				
					}					
					pdfDoc.setCell("Moneda 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Acuse ","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell(" Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario ","celda01",ComunesPDF.CENTER);
					if(firmaMancomunada.equals("S")){
					pdfDoc.setCell("Tipo de Cambio de Estatus ","celda01",ComunesPDF.CENTER);
					}					
				}
				if(iNoCambioEstatus==8) {
					titulo ="Estatus : Modificaci�n de Montos y/o Fechas de Vencimiento";
					 columnas =15;
					 if(!"".equals(sFechaVencPyme)) { 	columnas =columnas+2;			 }				
					pdfDoc.setTable(columnas, 100);
					pdfDoc.setCell(titulo,"celda01",ComunesPDF.LEFT, columnas); 
					pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER); 	
					pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha Emisi�n ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
					if(!"".equals(sFechaVencPyme)) {
					pdfDoc.setCell(" Fecha Vencimiento Proveedor","celda01",ComunesPDF.CENTER);				
					}	
					pdfDoc.setCell("Moneda 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento 	","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("Fecha vencimiento anterior","celda01",ComunesPDF.CENTER);					
					if(!"".equals(sFechaVencPyme)) {
					pdfDoc.setCell("Fecha Vencimiento Proveedor Anterior","celda01",ComunesPDF.CENTER);
					}					
					pdfDoc.setCell("Monto anterior ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Acuse ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Causa ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario ","celda01",ComunesPDF.CENTER);					
				}
				
				if(iNoCambioEstatus==28) {
					titulo ="Estatus : Aplicaci�n de Nota de Cr�dito";
					columnas =11;
					 if(!"".equals(sFechaVencPyme)) { 	columnas =columnas+1;			 }
					 if(bOperaFactConMandato){ 	columnas =columnas+1;			 }
					 
					pdfDoc.setTable(columnas, 100);
					pdfDoc.setCell(titulo,"celda01",ComunesPDF.LEFT, columnas); 
					pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER); 	
					pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha Emisi�n ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
					if(!"".equals(sFechaVencPyme)) {
					pdfDoc.setCell(" Fecha Vencimiento Proveedor","celda01",ComunesPDF.CENTER);				
					}	
					pdfDoc.setCell("Tipo Factoraje 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto anterior 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Nuevo 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento 	","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Monto a Descontar 	","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Causa ","celda01",ComunesPDF.CENTER);	
					if(bOperaFactConMandato){
						pdfDoc.setCell("Mandatario ","celda01",ComunesPDF.CENTER);	
					}
				}
				
				if(iNoCambioEstatus==29) {
					titulo ="Estatus :Programado a Negociable";
					columnas =9;
					 if(!"".equals(sFechaVencPyme)) { 	columnas =columnas+1;			 }								 
					pdfDoc.setTable(columnas, 100);
					pdfDoc.setCell(titulo,"celda01",ComunesPDF.LEFT, columnas); 
					pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER); 	
					pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha Emisi�n ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
					if(!"".equals(sFechaVencPyme)) {
					pdfDoc.setCell(" Fecha Vencimiento Proveedor","celda01",ComunesPDF.CENTER);				
					}	
					pdfDoc.setCell("Moneda 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento 	","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Monto a Descontar 	","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("N�mero de Acuse ","celda01",ComunesPDF.CENTER);						
				}		
				
				if(iNoCambioEstatus==40) {
					titulo ="Pre negociable a Negociable";
					 columnas =12;
					 if(!"".equals(sFechaVencPyme)) { 	columnas =columnas+1;			 }				
					pdfDoc.setTable(columnas, 100);
					pdfDoc.setCell(titulo,"celda01",ComunesPDF.LEFT, columnas); 
					pdfDoc.setCell("Nombre Proveedor","celda01",ComunesPDF.CENTER); 	
					pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha Emisi�n ","celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("Fecha Vencimiento ","celda01",ComunesPDF.CENTER);
					if(!"".equals(sFechaVencPyme)) {
					pdfDoc.setCell(" Fecha Vencimiento Proveedor","celda01",ComunesPDF.CENTER);				
					}	
					pdfDoc.setCell("Moneda 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto 	","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje de Descuento 	","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell("Monto a Descontar ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Acuse ","celda01",ComunesPDF.CENTER);				
					pdfDoc.setCell("Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Porcentaje Beneficiario ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Importe a Recibir Beneficiario ","celda01",ComunesPDF.CENTER);
					
				}			
					 
				while (rs.next()) {
					
					cg_razon_social = (rs.getString(1)==null)?"":rs.getString(1);
					ig_numero_docto = (rs.getString(2)==null)?"":rs.getString(2);
					cc_acuse = (rs.getString(3)==null)?"":rs.getString(3);
					df_fecha_docto = (rs.getString(4)==null)?"":rs.getString(5);
					df_fecha_venc = (rs.getString(5)==null)?"":rs.getString(5);
					df_fecha_venc_pyme = (rs.getString("df_fecha_venc_pyme")==null)?"":rs.getString("df_fecha_venc_pyme");
					cd_nombre = (rs.getString("cd_nombre")==null)?"":rs.getString("cd_nombre");  
					fn_monto = Double.parseDouble((String)rs.getString(7));
					ic_moneda = Integer.parseInt(rs.getString("ic_moneda"));
					
					if(iNoCambioEstatus ==5  ||  iNoCambioEstatus==4 ||  iNoCambioEstatus==6  ||  iNoCambioEstatus==8  || iNoCambioEstatus==40 ) {
						beneficiario      = (rs.getString("beneficiario")==null)?"":rs.getString("beneficiario");  
						por_beneficiario  = (rs.getString("por_beneficiario")==null)?"":rs.getString("por_beneficiario");  
						importe_a_recibir = (rs.getString("importe_a_recibir")==null)?"":rs.getString("importe_a_recibir");  
					}
				
		
					if(iNoCambioEstatus ==8 ) {
						fechaVencAnt = (rs.getString("fechaVencAnt")==null)?"":rs.getString("fechaVencAnt");
						fechaVencAnt_pyme = (rs.getString("df_fecha_venc_p_anterior")==null)?"":rs.getString("df_fecha_venc_p_anterior");
						montoAnterior = (rs.getString("fn_monto_anterior")==null)?"":rs.getString("fn_monto_anterior").trim();
						causa = rs.getString(10).trim();
					}
			
					if(iNoCambioEstatus ==5  ||  iNoCambioEstatus==4 ||  iNoCambioEstatus==6 ) {
						cambioEstatus =  (rs.getString("cambioEstatus")==null)?"":rs.getString("cambioEstatus");  
					}
				
					if(iNoCambioEstatus ==5  ||  iNoCambioEstatus==6  ||  iNoCambioEstatus==29) {
						if(ic_moneda==1)
							dblPorcentaje = new Double(strAforo).doubleValue() * 100;
						else if(ic_moneda==54)
							dblPorcentaje = new Double(strAforoDL).doubleValue() * 100;
							mtodoc=new BigDecimal(rs.getString(7));
						if(ic_moneda==1)
							mtodesc=mtodoc.multiply(porcentaje);
						else if(ic_moneda==54)
							mtodesc=mtodoc.multiply(porcentajeDL);						
					}	
				
					if(iNoCambioEstatus==4 || iNoCambioEstatus==40 ) {
						dblPorcentaje = Double.parseDouble((String)rs.getString(("fn_porc_anticipo")));
						mtodoc=new BigDecimal(rs.getString(7));
						mtodesc=mtodoc.multiply(porcentaje);
					}
				
					if(iNoCambioEstatus ==28) {
						mandante=  (rs.getString("mandante")==null)?"":rs.getString("mandante");
						montoAnterior = (rs.getString("fn_monto_anterior")==null)?"":rs.getString("fn_monto_anterior").trim();
						MontoAnterior2 = Double.parseDouble(montoAnterior); 
						MontoNuevo = Double.parseDouble((String)rs.getString(("fn_monto_nuevo"))); 
						causa = (rs.getString("ct_cambio_motivo")==null)?"":rs.getString("ct_cambio_motivo").trim();
						tipoFactoraje= (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL").trim(); 
						tipoFactorajeDesc = (rs.getString("TIPO_FACTORAJE")==null)?"":rs.getString("TIPO_FACTORAJE");
						if(tipoFactoraje.equals("V") || tipoFactoraje.equals("D") || tipoFactoraje.equals("C") || tipoFactoraje.equals("I"))  { //para factoraje vencido y distribuido
							dblPorcentaje =  100;
						}
							montoDescuento =  (MontoNuevo*dblPorcentaje)/100;						
					}
				
					if(bOperaFactorajeVencido || bOperaFactVencimientoInfonavit) { // Para Factoraje Vencido o Vencimiento Infonavit
						tipoFactoraje = (rs.getString("CS_DSCTO_ESPECIAL")==null)?"":rs.getString("CS_DSCTO_ESPECIAL");
						if(tipoFactoraje.equals("V") || tipoFactoraje.equals("I"))  {
							dblPorcentaje =  100;
							mtodesc=new BigDecimal (fn_monto);
						}
					}	
				
					// para los totales				
					if(iNoCambioEstatus ==5  ||  iNoCambioEstatus==4 ||  iNoCambioEstatus==6  ||  iNoCambioEstatus==29 ||  iNoCambioEstatus==40) {
						if (ic_moneda == 1) {
							df_total_mn+=fn_monto;
							nac_desc = nac_desc.add(mtodesc);
							nacionalDocto = true;
						}
						if (ic_moneda == 54) {
							df_total_dolares+=fn_monto;
							dol_desc = dol_desc.add(mtodesc);
							dolaresDocto = true;
						}									
					}
					if(iNoCambioEstatus ==8  ) {
						if (ic_moneda == 1) {
							df_total_mn+=fn_monto;
							mnMontoModif = mnMontoModif.add(mtodesc);
							nacionalDocto = true;
							df_total_ant_mn += rs.getDouble(9);
						}
						if (ic_moneda == 54) {
							df_total_dolares+=fn_monto;
							dolMontoModif = dolMontoModif.add(mtodesc);
							dolaresDocto = true;
							df_total_ant_dol += rs.getDouble(9);
						}
					}
		
					if(iNoCambioEstatus==28) {				
						if (ic_moneda == 1) {					
							total_MontoAnterior_mn +=MontoAnterior2;
							total_MontoNuevo_mn +=MontoNuevo;
							total_Monto_descontar_mn += montoDescuento;
							nacionalDocto = true;					
						}
						if (ic_moneda == 54) {					
							total_MontoAnterior_dolares +=MontoAnterior2;
							total_MontoNuevo_dolares +=MontoNuevo;
							total_Monto_descontar_dolares += montoDescuento;
							dolaresDocto = true;				
						}		
					}
						
					if(iNoCambioEstatus==5 ||  iNoCambioEstatus==4   ||  iNoCambioEstatus==6  ) {	
						pdfDoc.setCell(cg_razon_social.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ig_numero_docto.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_docto.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_venc.trim(),"formas",ComunesPDF.CENTER);
						if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell(df_fecha_venc_pyme.trim(),"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell(cd_nombre.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Double.toString (dblPorcentaje)+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(mtodesc.toPlainString().trim(),2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(cc_acuse.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(por_beneficiario+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(importe_a_recibir,"formas",ComunesPDF.CENTER);
						if(firmaMancomunada.equals("S")){
							pdfDoc.setCell(cambioEstatus,"formas",ComunesPDF.CENTER);	
						}
					}
				
					if(iNoCambioEstatus==8 ) {	
						pdfDoc.setCell(cg_razon_social.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ig_numero_docto.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_docto.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_venc.trim(),"formas",ComunesPDF.CENTER);
						if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell(df_fecha_venc_pyme.trim(),"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell(cd_nombre.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Double.toString (dblPorcentaje)+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVencAnt,"formas",ComunesPDF.CENTER);					
						if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell(fechaVencAnt_pyme,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell("$"+Comunes.formatoDecimal(montoAnterior,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(mtodesc.toPlainString().trim(),2),"formas",ComunesPDF.RIGHT);
						
						pdfDoc.setCell(cc_acuse.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(por_beneficiario+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(importe_a_recibir,"formas",ComunesPDF.CENTER);
						
					}
				
					if(iNoCambioEstatus==28 ) {	
						pdfDoc.setCell(cg_razon_social.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ig_numero_docto.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_docto.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_venc.trim(),"formas",ComunesPDF.CENTER);
						if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell(df_fecha_venc_pyme.trim(),"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell(tipoFactorajeDesc.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cd_nombre.trim(),"formas",ComunesPDF.CENTER);						
						pdfDoc.setCell("$ "+Comunes.formatoMoneda2(String.valueOf(MontoAnterior2), false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$ "+Comunes.formatoMoneda2(String.valueOf(MontoNuevo), false),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+"%","formas",ComunesPDF.CENTER);	
						pdfDoc.setCell("$ "+Comunes.formatoMoneda2(String.valueOf(montoDescuento), false),"formas",ComunesPDF.RIGHT);						
						pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
						if(bOperaFactConMandato){
							pdfDoc.setCell(mandante,"formas",ComunesPDF.CENTER);
						}
					}
				
					if(iNoCambioEstatus==29 ) {	
						pdfDoc.setCell(cg_razon_social.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ig_numero_docto.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_docto.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_venc.trim(),"formas",ComunesPDF.CENTER);
						if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell(df_fecha_venc_pyme.trim(),"formas",ComunesPDF.CENTER);
						}					
						pdfDoc.setCell(cd_nombre.trim(),"formas",ComunesPDF.CENTER);						
						pdfDoc.setCell("$ "+Comunes.formatoMoneda2(String.valueOf(fn_monto), false),"formas",ComunesPDF.RIGHT);					
						pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(dblPorcentaje),0,false)+"%","formas",ComunesPDF.CENTER);					
						pdfDoc.setCell("$ "+Comunes.formatoMoneda2(mtodesc.toPlainString(), false),"formas",ComunesPDF.RIGHT);					
						pdfDoc.setCell(cc_acuse,"formas",ComunesPDF.CENTER);					
					}
				
					if(iNoCambioEstatus==40 ) {	
						pdfDoc.setCell(cg_razon_social.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ig_numero_docto.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_docto.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_venc.trim(),"formas",ComunesPDF.CENTER);
						if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell(df_fecha_venc_pyme.trim(),"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setCell(cd_nombre.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_monto,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Double.toString (dblPorcentaje)+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(mtodesc.toPlainString().trim(),2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(cc_acuse.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(beneficiario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(por_beneficiario+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(importe_a_recibir,"formas",ComunesPDF.CENTER);
						
					}
				}//	while (rs.next())
	
					
				//TOTALES
				if(iNoCambioEstatus==5 ||  iNoCambioEstatus==4   ||  iNoCambioEstatus==6 ) {								 
					
					if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell("Total Moneda Nacional:","formas",ComunesPDF.RIGHT, 6); 					
					 }else {
						pdfDoc.setCell("Total Moneda Nacional:","formas",ComunesPDF.RIGHT, 5); 	
					 }					
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(df_total_mn),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoMoneda2(nac_desc.toPlainString(),true),"formas",ComunesPDF.RIGHT);					
					if(firmaMancomunada.equals("S")){
						pdfDoc.setCell("  ","formas",ComunesPDF.CENTER,5);
					}else {
						pdfDoc.setCell("  ","formas",ComunesPDF.CENTER,4);
					}
					if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell(" Total Moneda Dolar:","formas",ComunesPDF.RIGHT, 6); 
					}else {
						pdfDoc.setCell(" Total Moneda Dolar:","formas",ComunesPDF.RIGHT, 5); 						
					}
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(df_total_dolares),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(dol_desc),true),"formas",ComunesPDF.RIGHT);
					if(firmaMancomunada.equals("S")){
						pdfDoc.setCell("  ","formas",ComunesPDF.CENTER,5);
					}else {
						pdfDoc.setCell("  ","formas",ComunesPDF.CENTER,4);
					}				
				}
				
				if(iNoCambioEstatus==8 ) {
					
					if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell("Total Moneda Nacional:","formas",ComunesPDF.RIGHT, 6); 					
					 }else {
						pdfDoc.setCell("Total Moneda Nacional:","formas",ComunesPDF.RIGHT, 5); 	
					 }			
					 
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(df_total_mn),true),"formas",ComunesPDF.RIGHT);
					if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,3);
					}else {
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,2);
					}
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(df_total_ant_mn),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(mnMontoModif),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);
					
					
					if(!"".equals(sFechaVencPyme)) {					
					pdfDoc.setCell("Total Moneda Dolar:","formas",ComunesPDF.RIGHT,6); 
					}else {
						pdfDoc.setCell("Total Moneda Dolar:","formas",ComunesPDF.RIGHT,5); 						
					}
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(df_total_dolares),true),"formas",ComunesPDF.RIGHT);
					if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,3);
					}else {
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,2);
					}
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(df_total_ant_dol),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(dolMontoModif),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);
							
				}		
			
			
				if( iNoCambioEstatus==29) {		
								 
					if(!"".equals(sFechaVencPyme)) {
						pdfDoc.setCell("Total Moneda Nacional:","formas",ComunesPDF.RIGHT, 6); 					
					}else {
						pdfDoc.setCell("Total Moneda Nacional:","formas",ComunesPDF.RIGHT, 5); 	
					}		
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(df_total_mn),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoMoneda2(nac_desc.toPlainString(),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);		
					
					if(!"".equals(sFechaVencPyme)) {					
					pdfDoc.setCell("Total Moneda Dolar:","formas",ComunesPDF.RIGHT,6); 
					}else {
						pdfDoc.setCell("Total Moneda Dolar:","formas",ComunesPDF.RIGHT,5); 						
					}
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(df_total_dolares),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(dol_desc),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);		
					
				}
			
				if( iNoCambioEstatus==40) {		
					colum =4;
					 if(!"".equals(sFechaVencPyme)) { colum =colum+1; }
					pdfDoc.setCell("Total Moneda Nacional:","formas",ComunesPDF.RIGHT, 5); 
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(df_total_mn),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoMoneda2(nac_desc.toPlainString(),true),"formas",ComunesPDF.RIGHT);
					if(!"".equals(sFechaVencPyme)) { 
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);		
					}else{						
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,4);		
					}				
					pdfDoc.setCell("Total Moneda Dolar:","formas",ComunesPDF.RIGHT, 5); 
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(df_total_dolares),true),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoMoneda2(String.valueOf(dol_desc),true),"formas",ComunesPDF.RIGHT);
					if(!"".equals(sFechaVencPyme)) { 
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);		
					}else{						
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,4);		
					}						
				}		
				pdfDoc.addTable();			
				pdfDoc.endDocument();
				
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		
		}
		
		return nombreArchivo;
	}
	
	public String getQueryPorCambiosEstatus() {
		StringBuffer sbQuery = new StringBuffer();
		String hint = "";
		String sReferencia = ",'RepCEstatusEpoDEbean:getQueryPorEstatus("+iNoCambioEstatus+")' as PANTALLA";
		//Posfijo si es ingles
		String idioma = (sesIdiomaUsuario.equals("EN"))?"_ing":""; 
		
		//4 Negociable a Baja
		//5 Negociable a Descuento Fisico
		//6 Negociable a Pagado Anticipado
		// 29 Programado a Negociable Foda 25-28/04/2005 SMJ
		if(iNoCambioEstatus==4 || iNoCambioEstatus==5 || iNoCambioEstatus==6 || iNoCambioEstatus==29 || iNoCambioEstatus==40 ) {
			//hint = " /*+index(ce CP_COMHIS_CAMBIO_ESTATUS_PK)*/";
			sbQuery.append(
				" select "+
				hint+
				" (DECODE (pe.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social) "   +
				" ,d.ig_numero_docto "   +
				" ,d.cc_acuse"   +
				" ,TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY') "   +
				" ,TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') "   +
				" ,m.cd_nombre"+idioma+" as cd_nombre"   +
				" ,d.fn_monto ");
				
			if(iNoCambioEstatus==4 || iNoCambioEstatus==40) {
				sbQuery.append(
					",DECODE (d.ic_moneda, 1, 50.0, 54, 55.00000000000001) AS fn_porc_anticipo"
				);
			}//if(iNoCambioEstatus==4)
			
			sbQuery.append(
				" ,d.ic_moneda"   +
				" ,d.cs_dscto_especial "   +
				" ,i2.cg_razon_social AS beneficiario "   +
				" ,d.fn_porc_beneficiario AS por_beneficiario"   +
				" ,(d.fn_porc_beneficiario * d.fn_monto) / 100 AS importe_a_recibir"+
				" ,TO_CHAR (d.df_fecha_venc_pyme, 'DD/MM/YYYY')AS df_fecha_venc_pyme " +
				"  ,es.CD_DESCRIPCION as cambioEstatus ");//sbQuery.append(
				
			sbQuery.append(sReferencia);				
				
			sbQuery.append(
				" FROM "   +
				" com_documento d "   +
				" ,comrel_pyme_epo pe "   +
				" ,comcat_pyme p "   +
				" ,comhis_cambio_estatus ce "   +
				" ,comcat_epo e "   +
				" ,comcat_if i2 "   +
				" ,comcat_moneda m "+
				", COMCAT_CAMBIO_ESTATUS  es "
			);
			sbQuery.append(
				"  WHERE d.ic_documento = ce.ic_documento"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_epo = e.ic_epo"   +
				"    AND p.ic_pyme = pe.ic_pyme"   +
				"    AND d.ic_beneficiario = i2.ic_if (+)"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND TO_CHAR (ce.dc_fecha_cambio, 'DD/MM/YYYY') = TO_CHAR (SYSDATE, 'DD/MM/YYYY')"   +
				"    and ce.IC_CAMBIO_ESTATUS = es.IC_CAMBIO_ESTATUS  "+
				"    AND d.ic_epo = ?"   +
				"    AND pe.ic_epo = ?"   );
				if(iNoCambioEstatus==4) {
					sbQuery.append("    AND ce.ic_cambio_estatus in( 4,37 )");				
				}else  if(iNoCambioEstatus==5) {
					sbQuery.append("    AND ce.ic_cambio_estatus in( 5,38 )");	
				}else  if(iNoCambioEstatus==6) {
					sbQuery.append("    AND ce.ic_cambio_estatus in( 6,39 )");	
				}else  if(iNoCambioEstatus==40) {
					sbQuery.append("    AND ce.ic_cambio_estatus in( 40 )");							
				}else{
				  sbQuery.append("    AND ce.ic_cambio_estatus = ?");	
				}
		}//if(iNoCambioEstatus==4 || iNoCambioEstatus==5 || iNoCambioEstatus==6 iNoCambioEstatus==29)
		

		//8 Modificacion de Montos y Fechas
		//28 Aplicaci�n de Notas de Credito
		if(iNoCambioEstatus==8 || iNoCambioEstatus==28) {
			sbQuery.append(
				" select "+
				hint+
				" (DECODE (pe.cs_habilitado, 'N', '*', 'S', ' ') || ' ' || p.cg_razon_social)"   +
				" ,d.ig_numero_docto"   +
				" ,ce.cc_acuse"   +
				" ,TO_CHAR (d.df_fecha_docto, 'DD/MM/YYYY')"   +
				" ,TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY')"   +
				" ,m.cd_nombre"   +		
				" ,d.fn_monto"   +
				" ,d.ic_moneda"   +				
				" ,ce.fn_monto_anterior"   +
				" ,ce.ct_cambio_motivo"   +				
				" ,d.fn_porc_anticipo"
			);
			if(iNoCambioEstatus==8) {
				sbQuery.append(
					" ,d.ic_estatus_docto"	
				);
				
			}
			sbQuery.append(				
				" ,d.cs_dscto_especial"
			);
			if(iNoCambioEstatus==8) {
				sbQuery.append(
					" ,d.fn_monto_dscto"   +
					" ,i2.cg_razon_social AS beneficiario"   +
					" ,d.fn_porc_beneficiario AS por_beneficiario"   +
					" ,DECODE ( d.ic_estatus_docto, "   +
					" 	       3, ds.fn_importe_recibir_benef, "   +
					" 	       4, ds.fn_importe_recibir_benef,"   +
					"         11, ds.fn_importe_recibir_benef,"   +
					"         12, ds.fn_importe_recibir_benef,"   +
					"         16, ds.fn_importe_recibir_benef,"   +
					"         ((d.fn_porc_beneficiario * d.fn_monto) / 100)) AS importe_a_recibir"   +
					" ,TO_CHAR (ce.df_fecha_venc_anterior, 'dd/mm/yyyy') AS fechavencant"
				);
			} else if(iNoCambioEstatus==28) {
				sbQuery.append(				
					" ,ce.fn_monto_nuevo"   +
					" ,i2.cg_razon_social AS beneficiario"   +
					" ,ce.ct_cambio_motivo"
				);
			}
			
			sbQuery.append(" ,ce.dc_fecha_cambio");
			sbQuery.append(" ,TO_CHAR (d.df_fecha_venc_pyme, 'DD/MM/YYYY') as df_fecha_venc_pyme " );	
			sbQuery.append(" ,TO_CHAR (ce.df_fecha_venc_p_anterior, 'DD/MM/YYYY') AS df_fecha_venc_p_anterior " );
			sbQuery.append(" ,TO_CHAR (ce.df_fecha_venc_p_nueva, 'DD/MM/YYYY') AS df_fecha_venc_p_nueva " );					
			sbQuery.append(sReferencia);	
			sbQuery.append(" ,ma.CG_RAZON_SOCIAL as mandante");  // Fodea 023 2009
			sbQuery.append(" ,tf.cg_nombre as TIPO_FACTORAJE");  // Fodea 023 2009	
			
			sbQuery.append(
				" FROM com_documento d"   +
				" ,com_docto_seleccionado ds"   +
				" ,comrel_pyme_epo pe"   +
				" ,comcat_pyme p"   +
				" ,comhis_cambio_estatus ce "   +
				" ,comcat_epo e "   +
				" ,comcat_if i2 "   +
				" ,comcat_moneda m "+
				" ,COMCAT_MANDANTE ma "+	  // Fodea 023 2009	
				" ,COMCAT_TIPO_FACTORAJE tf "+					
				" WHERE d.ic_documento = ce.ic_documento"   +
				" AND d.ic_documento = ds.ic_documento (+)"   +
				" AND d.ic_pyme = p.ic_pyme"   +
				" AND p.ic_pyme = pe.ic_pyme"   +
				" AND d.ic_beneficiario = i2.ic_if (+)"   +
				" AND d.ic_epo = e.ic_epo"   +
				" AND d.ic_epo = pe.ic_epo"   +
				" AND d.ic_moneda = m.ic_moneda"   +
				" AND ma.IC_MANDANTE(+) = d.IC_MANDANTE "+
				" AND d.cs_dscto_especial = tf.cc_tipo_factoraje "+
				" AND d.ic_epo = ?"   +
				" AND TO_CHAR (ce.dc_fecha_cambio, 'DD/MM/YYYY') = TO_CHAR (SYSDATE, 'DD/MM/YYYY')"+				
				" AND ce.ic_cambio_estatus = ?  "  );
			if(iNoCambioEstatus==28)
				sbQuery.append(" order by d.ig_numero_docto, ce.dc_fecha_cambio");
		}//if(iNoCambioEstatus==8 || iNoCambioEstatus==28)
		
		System.out.println(" getQueryPorCambiosEstatus ::--"+iNoCambioEstatus+"----" +sbQuery.toString());
		
		return sbQuery.toString();		
	} // getQueryPorCambiosEstatus()



	 //GETTERS*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}



	public String getClaveEPO() {
		return claveEPO;
	}

	public void setClaveEPO(String claveEPO) {
		this.claveEPO = claveEPO;
	}
	
	public void setiNoCambioEstatus(int iNoCambioEstatus) {
		this.iNoCambioEstatus = iNoCambioEstatus;
	}
	public void setsIdiomaUsuario(String sesIdiomaUsuario) {
		this.sesIdiomaUsuario = sesIdiomaUsuario;
	}


	
} // Fin de la clase.
