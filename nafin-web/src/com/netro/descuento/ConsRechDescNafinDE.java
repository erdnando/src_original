package com.netro.descuento;

import com.netro.pdf.ComunesPDF;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;

import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

public class ConsRechDescNafinDE implements IQueryGenerator, IQueryGeneratorRegExtJS{

	public ConsRechDescNafinDE(){}

	public String getAggregateCalculationQuery(HttpServletRequest request){
		// Este metodo generara el query de los totales 
		StringBuffer query = new StringBuffer("");
		// Parametros de la pagina
		String df_operacion_inicio = (request.getParameter("df_operacion_inicio")==null)?"":request.getParameter("df_operacion_inicio");
		String df_operacion_fin=(request.getParameter("df_operacion_fin")==null)?"":request.getParameter("df_operacion_fin");
		String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo")==null)?"":request.getParameter("ic_banco_fondeo"); //FODEA 007 - 2009
		
		query.append( " select DECODE (m.ic_moneda,'1', 'MONEDA NACIONAL', '54', 'DOLAR')AS MONEDA,  "+
				" m.cd_nombre, count(de.ic_documento), sum(d.FN_MONTO),'ConsRechDescNafinDE::getAggregateCalculationQuery()' as ORIGENQRY "+
				" from   COM_DSCTOERROR DE,  "+
				" COM_DOCUMENTO D,  "+
				" COMCAT_EPO E,  "+
				" COMCAT_PYME P , "+
				" COMCAT_MONEDA M "+
			  " WHERE  D.IC_DOCUMENTO=DE.IC_DOCUMENTO  "+
				" AND   D.IC_PYME=P.IC_PYME  "+
				" AND   D.IC_EPO=E.IC_EPO  "+
				" AND   D.IC_MONEDA=M.IC_MONEDA "+
				" AND   E.IC_BANCO_FONDEO = "+ic_banco_fondeo+
				" AND   DE.DF_PROCESO >= trunc(nvl(TO_DATE('"+df_operacion_inicio+"','DD/MM/YYYY'),sysdate)) "   +
				" AND   DE.DF_PROCESO < trunc(nvl(TO_DATE ('"+df_operacion_fin+"','DD/MM/YYYY'),sysdate))	+ 1 "   +
			  " group by m.ic_moneda,m.cd_nombre,'ConsRechDescNafinDE::getAggregateCalculationQuery()'"+
			  " order by m.ic_moneda ");
				
				System.out.println("query  "+query);
		return query.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request){
		// Genera el query que devuelve las llaves primarias de la consulta
		StringBuffer query = new StringBuffer("");
		// Parametros de la pagina
		String df_operacion_inicio = (request.getParameter("df_operacion_inicio")==null)?"":request.getParameter("df_operacion_inicio");
		String df_operacion_fin=(request.getParameter("df_operacion_fin")==null)?"":request.getParameter("df_operacion_fin");
		String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo")==null)?"":request.getParameter("ic_banco_fondeo"); //FODEA 007 - 2009
		// Generacion del query
		query.append(
				" select DE.ic_documento||'-'||TO_CHAR(DE.DF_PROCESO,'DDMMYYYYHH24MISS')"   +
				" from   COM_DSCTOERROR DE, "   +
				"        COM_DOCUMENTO D, COMCAT_EPO E, COMCAT_PYME P " +
				" WHERE  D.IC_DOCUMENTO=DE.IC_DOCUMENTO "+
				"  AND   D.IC_PYME=P.IC_PYME "+
				"  AND   D.IC_EPO=E.IC_EPO "+
				"  AND   E.IC_BANCO_FONDEO = "+ic_banco_fondeo+ //FODEA 007 - 2009
				" AND   DE.DF_PROCESO >= trunc(nvl(TO_DATE('"+df_operacion_inicio+"','DD/MM/YYYY'),sysdate)) "   +
				" AND   DE.DF_PROCESO < trunc(nvl(TO_DATE ('"+df_operacion_fin+"','DD/MM/YYYY'),sysdate))	+ 1 "   +
				" order by DE.IC_DSCTOERROR, E.CG_RAZON_SOCIAL, P.CG_RAZON_SOCIAL, D.IG_NUMERO_DOCTO "); 	
				
				System.out.println("query--->"+query.toString());
				
		return query.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request){
		// Genera el query que devuelve toda la informacion de la consulta
		StringBuffer query = new StringBuffer("");
		// Parametros de la pagina
		String df_operacion_inicio = (request.getParameter("df_operacion_inicio")==null)?"":request.getParameter("df_operacion_inicio");
		String df_operacion_fin=(request.getParameter("df_operacion_fin")==null)?"":request.getParameter("df_operacion_fin");
		String ic_banco_fondeo = (request.getParameter("ic_banco_fondeo")==null)?"":request.getParameter("ic_banco_fondeo"); //FODEA 007 - 2009
		// Generacion del query
		query.append(
				" select decode (E.cs_habilitado,'N','* ','S','')||E.cg_razon_social epo,"   +
				" 	   decode (P.cs_habilitado,'N','* ','S','')||(INITCAP(P.cg_razon_social)) pyme,"   +
				" 	   decode (I.cs_habilitado,'N','* ','S','')||I.cg_razon_social cif,"   +
				"      D.IG_NUMERO_DOCTO,TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY'),TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY'),"   +
				" 	   M.CD_NOMBRE,DECODE(D.CS_DSCTO_ESPECIAL,'N','NORMAL','D','DISTRIBUIDO','V','VENCIDO',D.CS_DSCTO_ESPECIAL), "  +
				" 	   NVL(D.FN_MONTO,0), NVL(DE.FN_PORC_ANTICIPO,0), NVL(DE.FN_PORC_ANTICIPO*D.FN_MONTO/100,0),DE.CS_DSCTO_AUTOMATICO_DIA,DE.IN_DIAS_MINIMO,"   +
				" 	   DE.IN_LIMITE_EPO,REC.CD_DESCRIPCION,TO_CHAR(DE.DF_PROCESO,'DD/MM/YYYY HH:MI'),'ConsRechDescNafinDE::getDocumentQueryFile()' as ORIGENQRY"   +
				" from   COM_DSCTOERROR DE,"   +
				"        COM_DOCUMENTO D, "   +
				" 	   COMCAT_PYME P, "   +
				" 	   COMCAT_EPO E, "   +
				" 	   COMCAT_IF I, "   +
				" 	   COMCAT_CAUSA_RECHAZO REC,"   +
				" 	   COMCAT_MONEDA M"   +
				" WHERE  D.IC_DOCUMENTO=DE.IC_DOCUMENTO"   +
				" AND    D.IC_PYME=P.IC_PYME"   +
				" AND    D.IC_EPO=E.IC_EPO"   +
				" AND    DE.IC_IF=I.IC_IF"   +
				" AND    DE.IC_CAUSA_RECHAZO=REC.IC_CAUSA_RECHAZO"   +
				" AND    D.IC_MONEDA=M.IC_MONEDA"   +
				" AND   E.IC_BANCO_FONDEO = "+ic_banco_fondeo+ //FODEA 007 - 2009
				" AND   DE.DF_PROCESO >= trunc(nvl(TO_DATE('"+df_operacion_inicio+"','DD/MM/YYYY'),sysdate)) "   +
				" AND   DE.DF_PROCESO < trunc(nvl(TO_DATE ('"+df_operacion_fin+"','DD/MM/YYYY'),sysdate))	+ 1 "   +
				" order by DE.IC_DSCTOERROR, E.CG_RAZON_SOCIAL, P.CG_RAZON_SOCIAL, D.IG_NUMERO_DOCTO "  ); 				
		return query.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
		// Genera el query que extrae solo un segmento de la informacion
		StringBuffer query = new StringBuffer("");   
		query.append(" select decode (E.cs_habilitado,'N','* ','S','')||E.cg_razon_social as epo,"   +
				" 	   decode (P.cs_habilitado,'N','* ','S','')||(INITCAP(P.cg_razon_social)) as pyme,"   +
				" 	   decode (I.cs_habilitado,'N','* ','S','')||I.cg_razon_social cif,"   +
				"      D.IG_NUMERO_DOCTO as documento ,TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') as fechaDocto ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') as fechaVen,"   +
				" 	   M.CD_NOMBRE as moneda ,DECODE(D.CS_DSCTO_ESPECIAL,'N','NORMAL','D','DISTRIBUIDO','V','VENCIDO',D.CS_DSCTO_ESPECIAL) as tipoDescuento, "  +
				" 	   NVL(D.FN_MONTO,0) as monto , NVL(DE.FN_PORC_ANTICIPO,0) as porDesc, NVL(DE.FN_PORC_ANTICIPO*D.FN_MONTO/100,0) as montoDes,DE.CS_DSCTO_AUTOMATICO_DIA as desAuto,DE.IN_DIAS_MINIMO as diasMinimos,"   +
				" 	   DE.IN_LIMITE_EPO as limiteEpo ,REC.CD_DESCRIPCION as causaRechazo ,TO_CHAR(DE.DF_PROCESO,'DD/MM/YYYY HH:MI') as FECHADESC ,'ConsRechDescNafinDE::getDocumentSummaryQueryForIds()' as ORIGENQRY"   +
				" from   COM_DSCTOERROR DE,"   +
				"        COM_DOCUMENTO D, "   +
				" 	   COMCAT_PYME P, "   +
				" 	   COMCAT_EPO E, "   +
				" 	   COMCAT_IF I, "   +
				" 	   COMCAT_CAUSA_RECHAZO REC,"   +
				" 	   COMCAT_MONEDA M"   +
				" WHERE  D.IC_DOCUMENTO=DE.IC_DOCUMENTO"   +
				" AND    D.IC_PYME=P.IC_PYME"   +
				" AND    D.IC_EPO=E.IC_EPO"   +
				" AND    DE.IC_IF=I.IC_IF"   +
				" AND    DE.IC_CAUSA_RECHAZO=REC.IC_CAUSA_RECHAZO"   +
				" AND    D.IC_MONEDA=M.IC_MONEDA"   +
				//" AND    DE.IC_DOCUMENTO||'-'||TO_CHAR(DE.DF_PROCESO,'DDMMYYYYHHMISS') IN (");
				" AND ( ");
		int r = 0;
		for (Iterator it = ids.iterator(); it.hasNext();){
			r++;
			if (r > 1) {
				query.append(" OR " );	
			}
			List llaves = Comunes.explode("-", (String) it.next());
			query.append(" ( DE.ic_documento = " + llaves.get(0) + " AND DE.DF_PROCESO = TO_DATE('" + llaves.get(1) + "','DDMMYYYYHH24MISS') )"); 
		}
		//query = query.delete(query.length()-1,query.length());
		query.append(" ) ORDER BY DE.IC_DSCTOERROR, E.CG_RAZON_SOCIAL, P.CG_RAZON_SOCIAL, D.IG_NUMERO_DOCTO ");
		System.out.println("ConsRechDescNafinDE:getDocumentSummaryQueryForIds() " + query);
		return query.toString();
	}

/*******************************************************************************
 *     Migraci�n. Implementa la interfaz IQueryGeneratorRegExtJS.java          *
 *******************************************************************************/
	private static final Log log = ServiceLocator.getInstance().getLog(ConsRechDescNafinDE.class);
	private List conditions;
	private String dfOperacionInicio;
	private String dfOperacionFin;
	private String icBancoFondeo;

	public String getDocumentQuery(){
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids){
		return null;
	}

	public String getAggregateCalculationQuery(){
		return null;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo){
		return null;
	}

/**
 * Obtiene la consulta para generar los archivos CSV y PDF mostrando todos los registros,
 * es decir, no utiliza paginaci�n.
 * @return qrySentencia
 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();
		conditions.add(icBancoFondeo);
		conditions.add(dfOperacionInicio);
		conditions.add(dfOperacionFin);

		qrySentencia.append(" select decode (E.cs_habilitado,'N','* ','S','')||E.cg_razon_social AS EPO,"                                                                     );
		qrySentencia.append("        decode (P.cs_habilitado,'N','* ','S','')||(INITCAP(P.cg_razon_social)) AS PYME,"                                                         );
		qrySentencia.append("        decode (I.cs_habilitado,'N','* ','S','')||I.cg_razon_social AS CIF,"                                                                     );
		qrySentencia.append("        D.IG_NUMERO_DOCTO,TO_CHAR(D.DF_FECHA_DOCTO,'DD/MM/YYYY') AS FECHA_EMISION,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS FECHA_VENCIMIENTO,"   );
		qrySentencia.append("        M.CD_NOMBRE AS MONEDA, DECODE(D.CS_DSCTO_ESPECIAL,'N','NORMAL','D','DISTRIBUIDO','V','VENCIDO',D.CS_DSCTO_ESPECIAL) AS TIPODESC, "       );
		qrySentencia.append("        NVL(D.FN_MONTO,0) AS MONTO_DOCTO, NVL(DE.FN_PORC_ANTICIPO,0) AS PORC_DESCTO,"                                                            );
		qrySentencia.append("        NVL(DE.FN_PORC_ANTICIPO*D.FN_MONTO/100,0) AS MONTO_DESCONTAR, DE.CS_DSCTO_AUTOMATICO_DIA, DE.IN_DIAS_MINIMO,"                            );
		qrySentencia.append("        DE.IN_LIMITE_EPO,REC.CD_DESCRIPCION,TO_CHAR(DE.DF_PROCESO,'DD/MM/YYYY HH:MI') AS FECHA_OPERACION"                                        );
		qrySentencia.append(" from   COM_DSCTOERROR DE,"                                                                                                                      );
		qrySentencia.append("        COM_DOCUMENTO D, "                                                                                                                       );
		qrySentencia.append("        COMCAT_PYME P, "                                                                                                                         );
		qrySentencia.append("        COMCAT_EPO E, "                                                                                                                          );
		qrySentencia.append("        COMCAT_IF I, "                                                                                                                           );
		qrySentencia.append("        COMCAT_CAUSA_RECHAZO REC,"                                                                                                               );
		qrySentencia.append("        COMCAT_MONEDA M"                                                                                                                         );
		qrySentencia.append(" WHERE  D.IC_DOCUMENTO=DE.IC_DOCUMENTO"                                                                                                          );
		qrySentencia.append(" AND    D.IC_PYME=P.IC_PYME"                                                                                                                     );
		qrySentencia.append(" AND    D.IC_EPO=E.IC_EPO"                                                                                                                       );
		qrySentencia.append(" AND    DE.IC_IF=I.IC_IF"                                                                                                                        );
		qrySentencia.append(" AND    DE.IC_CAUSA_RECHAZO=REC.IC_CAUSA_RECHAZO"                                                                                                );
		qrySentencia.append(" AND    D.IC_MONEDA=M.IC_MONEDA"                                                                                                                 );
		qrySentencia.append(" AND    E.IC_BANCO_FONDEO = ?"                                                                                                                   );
		qrySentencia.append(" AND    DE.DF_PROCESO >= trunc(nvl(TO_DATE(?,'DD/MM/YYYY'),sysdate)) "                                                                         );
		qrySentencia.append(" AND    DE.DF_PROCESO < trunc(nvl(TO_DATE (?,'DD/MM/YYYY'),sysdate)) + 1 "                                                                     );
		qrySentencia.append(" ORDER BY DE.IC_DSCTOERROR, E.CG_RAZON_SOCIAL, P.CG_RAZON_SOCIAL, D.IG_NUMERO_DOCTO "                                                            );

		log.info("qrySentencia " + qrySentencia.toString());
		log.info("conditions " + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();

	}

/**
 * Genera el archivo CSV o PDF seg�n el tipo que se le indique.
 * En el caso del PDF, el archivo generado no comtempla paginaci�n, imprime todos los registros que trae la consulta.
 * @param request
 * @param rs
 * @param path
 * @param tipo: CSV o PDF
 * @return nombre del archivo
 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();

		/***** Variables para el request *****/
		String NOM_EPO         = "";
		String NOM_PYME        = "";
		String NOM_IF          = "";
		String NUM_DOCTO       = "";
		String FECHA_EMISION   = "";
		String FECHA_VENC      = "";
		String MONEDA          = "";
		String TIPODESC        = "";
		String MONTO_DOCTO     = "";
		String PORC_DESCTO     = "";
		String MONTO_DESCONTAR = "";
		String TIPO_DSCTOAUTO  = "";
		String DIAS_MINIMOS    = "";
		String LIMITE_IFEPO    = "";
		String CAUSA_RECHAZO   = "";
		String FECHA_OPERACION = "";

		if(tipo.equals("PDF")){
			try{
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Rechazo en Descuento Automatico","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(16, 100);
				pdfDoc.setLCell("Nombre de EPO",             "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("PYME",                      "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("IF",                        "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Num. Documento",            "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Emisi�n",             "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Vencimiento",         "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda",                    "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Descuento",         "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto",                     "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("% Descuento",               "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Descontar",         "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Dscto. Autom�tico", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("D�as M�nimos Cadena",       "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("L�mite IF-EPO",             "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa de Rechazo",          "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha y Hora Descto",       "celda01", ComunesPDF.CENTER);
				pdfDoc.setLHeaders();

				while (rs.next()){

					NOM_EPO         = (rs.getString("EPO")                     == null)?"":rs.getString("EPO");
					NOM_PYME        = (rs.getString("PYME")                    == null)?"":rs.getString("PYME");
					NOM_IF          = (rs.getString("CIF")                     == null)?"":rs.getString("CIF");
					NUM_DOCTO       = (rs.getString("IG_NUMERO_DOCTO")         == null)?"":rs.getString("IG_NUMERO_DOCTO");
					FECHA_EMISION   = (rs.getString("FECHA_EMISION")           == null)?"":rs.getString("FECHA_EMISION");
					FECHA_VENC      = (rs.getString("FECHA_VENCIMIENTO")       == null)?"":rs.getString("FECHA_VENCIMIENTO");
					MONEDA          = (rs.getString("MONEDA")                  == null)?"":rs.getString("MONEDA");
					TIPODESC        = (rs.getString("TIPODESC")                == null)?"":rs.getString("TIPODESC");
					MONTO_DOCTO     = (rs.getString("MONTO_DOCTO")             == null)?"":rs.getString("MONTO_DOCTO");
					PORC_DESCTO     = (rs.getString("PORC_DESCTO")             == null)?"":rs.getString("PORC_DESCTO");
					MONTO_DESCONTAR = (rs.getString("MONTO_DESCONTAR")         == null)?"":rs.getString("MONTO_DESCONTAR");
					TIPO_DSCTOAUTO  = (rs.getString("CS_DSCTO_AUTOMATICO_DIA") == null)?"":rs.getString("CS_DSCTO_AUTOMATICO_DIA");
					DIAS_MINIMOS    = (rs.getString("IN_DIAS_MINIMO")          == null)?"":rs.getString("IN_DIAS_MINIMO");
					LIMITE_IFEPO    = (rs.getString("IN_LIMITE_EPO")           == null)?"":rs.getString("IN_LIMITE_EPO");
					CAUSA_RECHAZO   = (rs.getString("CD_DESCRIPCION")          == null)?"":rs.getString("CD_DESCRIPCION");
					FECHA_OPERACION = (rs.getString("FECHA_OPERACION")         == null)?"":rs.getString("FECHA_OPERACION");

					pdfDoc.setLCell(NOM_EPO,                                       "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell(NOM_PYME,                                      "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell(NOM_IF,                                        "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell(NUM_DOCTO,                                     "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(FECHA_EMISION,                                 "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(FECHA_VENC,                                    "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(MONEDA,                                        "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(TIPODESC,                                      "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(MONTO_DOCTO,2),     "formas", ComunesPDF.RIGHT );
					pdfDoc.setLCell(PORC_DESCTO+"%",                               "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(MONTO_DESCONTAR,2), "formas", ComunesPDF.RIGHT );
					pdfDoc.setLCell(TIPO_DSCTOAUTO,                                "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(DIAS_MINIMOS,                                  "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(LIMITE_IFEPO,                                  "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(CAUSA_RECHAZO,                                 "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell(FECHA_OPERACION,                               "formas", ComunesPDF.CENTER);
			}
			pdfDoc.addLTable();
			//TODO: (30/06/2015) Falta agregar la tabla de totales, esa funcionalidad la est� desarrollando Fabian.
			pdfDoc.endDocument();
			} catch(Exception e){
				log.warn("ConsRechDescNafinDE.crearCustomFile.Exception: " + e);
			}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

/************************************************************
 *                      GETTERS Y SETTERS                   *
 ************************************************************/
	public List getConditions(){
		return conditions; 
	}

	public String getDfOperacionInicio(){
		return dfOperacionInicio;
	}

	public void setDfOperacionInicio(String dfOperacionInicio){
		this.dfOperacionInicio = dfOperacionInicio;
	}

	public String getDfOperacionFin(){
		return dfOperacionFin;
	}

	public void setDfOperacionFin(String dfOperacionFin){
		this.dfOperacionFin = dfOperacionFin;
	}

	public String getIcBancoFondeo(){
		return icBancoFondeo;
	}

	public void setIcBancoFondeo(String icBancoFondeo){
		this.icBancoFondeo = icBancoFondeo;
	}
}
