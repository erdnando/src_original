package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsComiOperFondeoPropio implements IQueryGeneratorRegExtJS  {	     
	
	private int numList = 1;
	private String ses_ic_if;
	private String ic_epo;
	private String df_operacion_inicial;
	private String df_operacion_final;
	private String ic_moneda;
	public static final boolean SIN_COMAS = false;
		
	public ConsComiOperFondeoPropio() {}

	// Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ConsComiOperFondeoPropio.class);
  
	public  StringBuffer strQuery;
	private List conditions = new ArrayList(); 
	
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() { // Obtiene los totales 
		return "";
 	}  
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */	 
	public String getDocumentQuery(){  // para las llaves primarias		
		return "";		
 	}  
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */	  		
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 			
		return "";
 	}  
	
	public String getDocumentQueryFile(){ // para todos los registros				
		conditions = new ArrayList();   
		StringBuffer strQuery = new StringBuffer();   
		strQuery = new StringBuffer(); 
		String strCondiciones;
		
		strCondiciones = " And ci.ic_if = ? ";
		conditions.add(ses_ic_if);

		if (!ic_epo.equals("")){
			strCondiciones += "	And ce.ic_epo = ? ";
			conditions.add(ic_epo);
		}

		if (!df_operacion_inicial.equals("")){
			if (!df_operacion_final.equals("")){
				strCondiciones += " And TO_DATE(TO_CHAR(sa.df_fecha_solicitud,'ddmmyyyy'),'ddmmyyyy') between TO_DATE(?,'dd/mm/yyyy') and TO_DATE(?,'dd/mm/yyyy') ";
				conditions.add(df_operacion_inicial);
				conditions.add(df_operacion_final);
			}else{
				strCondiciones += " And TO_DATE(TO_CHAR(sa.df_fecha_solicitud,'ddmmyyyy'),'ddmmyyyy') = TO_DATE(?,'dd/mm/yyyy') ";
				conditions.add(df_operacion_inicial);
			}
			
		}

		if (!ic_moneda.equals("")){
			strCondiciones += "	And cm.ic_moneda = ? ";
			conditions.add(ic_moneda);
		}
							
			strQuery.append("Select ci.cg_razon_social as NombreIF, " + 
									"       cpn.ic_nombre, " + 
									"       ce.cg_razon_social as NombreEPO, " + 
									"       TO_CHAR(sa.df_fecha_solicitud,'dd/mm/yyyy') as df_fecha_solicitud, " + 
									"       sa.fg_porc_comision_fondeo, " + 
									"		cm.ic_moneda, " + 
									"       cm.cd_nombre, " + 
									"       d.fn_monto, " + 
									"       (ds.in_importe_recibir * (sa.fg_porc_comision_fondeo / 100)) as MontoComision " + 
									"  From com_documento d, " + 
									"       com_docto_seleccionado ds, " + 
									"       comcat_if ci, " + 
									"       comcat_epo ce, " + 
									"       comcat_moneda cm, " + 
									"       com_solicitud sa, " + 
									"       comcat_producto_nafin cpn " + 
									" Where ds.ic_if = ci.ic_if " + 
									"   And sa.ic_documento = ds.ic_documento " + 
									"   And ce.ic_epo = d.ic_epo " + 
									"   And ds.ic_documento = d.ic_documento " + 
									"   And d.ic_moneda = cm.ic_moneda " + 
									"   And cpn.ic_producto_nafin = 1 " + 
									"   And sa.fg_porc_comision_fondeo IS NOT NULL " + 
									"   And cm.ic_moneda = d.ic_moneda " + 
									" 	 AND ce.cs_habilitado = 'S' " +
									"   And cm.ic_moneda in (1,54) " + strCondiciones); 
									
			strQuery.append(" Union All ");
			
			
		strCondiciones = " And ci.ic_if = ? ";
		conditions.add(ses_ic_if);

		if (!ic_epo.equals("")){
			strCondiciones += "	And ce.ic_epo = ? ";
			conditions.add(ic_epo);
		}

		if (!df_operacion_inicial.equals("")){
			if (!df_operacion_final.equals("")){
				strCondiciones += " And TO_DATE(TO_CHAR(sa.df_fecha_solicitud,'ddmmyyyy'),'ddmmyyyy') between TO_DATE(?,'dd/mm/yyyy') and TO_DATE(?,'dd/mm/yyyy') ";
				conditions.add(df_operacion_inicial);
				conditions.add(df_operacion_final);
			}else{
				strCondiciones += " And TO_DATE(TO_CHAR(sa.df_fecha_solicitud,'ddmmyyyy'),'ddmmyyyy') = TO_DATE(?,'dd/mm/yyyy') ";
				conditions.add(df_operacion_inicial);
			}
			
		}

		if (!ic_moneda.equals("")){
			strCondiciones += "	And cm.ic_moneda = ? ";
			conditions.add(ic_moneda);
		}
		
			strQuery.append(" Select ci.cg_razon_social as NombreIF, " + 
									"       cpn.ic_nombre, " + 
									"       ce.cg_razon_social as NombreEPO, " + 
									"       TO_CHAR(sa.df_fecha_solicitud,'dd/mm/yyyy') as df_fecha_solicitud, " + 
									"       sa.fg_porc_comision_fondeo, " + 
									"		cm.ic_moneda, " + 
									"       cm.cd_nombre, " + 
									"       d.fn_monto, " + 
									"       (ds.fn_saldo_total * (sa.fg_porc_comision_fondeo / 100)) as MontoComision " + 
									"  From com_pedido d, " + 
									"       com_pedido_seleccionado ds, " + 
									"       com_linea_credito lc, " + 
									"       comcat_if ci, " + 
									"       comcat_epo ce, " + 
									"       comcat_moneda cm, " + 
									"       com_anticipo sa, " + 
									"       comcat_producto_nafin cpn " + 
									" Where lc.ic_if = ci.ic_if " + 
									"   And ds.ic_linea_credito = lc.ic_linea_credito " + 
									"   And sa.ic_pedido = ds.ic_pedido " + 
									"   And ce.ic_epo = d.ic_epo " + 
									"   And ds.ic_pedido = d.ic_pedido " + 
									"   And d.ic_moneda = cm.ic_moneda " + 
									"   And cpn.ic_producto_nafin = 2 " + 
									"   And sa.fg_porc_comision_fondeo IS NOT NULL " + 
									"   And cm.ic_moneda = d.ic_moneda " + 
									" 	 AND ce.cs_habilitado = 'S' " +
									"   And cm.ic_moneda in (1,54) " + strCondiciones);
			
			strQuery.append("  Order by NombreIF ");
			
		System.out.println("strQuery.toString() --->" + strQuery.toString());
		return strQuery.toString();
 	}   
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();

			if ("XLS".equals(tipo)) {
				try {
					int registros = 0;	
					while(rs.next()){
						if(registros==0) { 
							contenidoArchivo.append("Producto,EPO,Fecha de Operaci�n,% Comisi�n,Moneda,Monto del Documento,Monto del Documento");
						}
						registros++;
						String nombreif = rs.getString("nombreif")==null?"":rs.getString("nombreif");
						String ic_nombre = rs.getString("ic_nombre")==null?"":rs.getString("ic_nombre");
						String nombreepo = rs.getString("nombreepo")==null?"":rs.getString("nombreepo");
						String df_fecha_solicitud = rs.getString("df_fecha_solicitud")==null?"":rs.getString("df_fecha_solicitud");
						String fg_porc_comision_fondeo = rs.getString("fg_porc_comision_fondeo")==null?"":rs.getString("fg_porc_comision_fondeo");
						String ic_moneda = rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
						String cd_nombre = rs.getString("cd_nombre")==null?"":rs.getString("cd_nombre");
						String fn_monto = rs.getString("fn_monto")==null?"":rs.getString("fn_monto");
						String montocomision = rs.getString("montocomision")==null?"":rs.getString("montocomision");
							
						contenidoArchivo.append("\n"+
							ic_nombre.replaceAll(",","") + "," +
							nombreepo.replaceAll(",","") + "," +
							df_fecha_solicitud.replaceAll(",","") + "," +
							fg_porc_comision_fondeo.replaceAll("-","/") + "," +
							cd_nombre + "," +
							Comunes.formatoDecimal(fn_monto, 2, false) + "," +
							Comunes.formatoDecimal(montocomision, 2, false) + ",");
					}
					
					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}	 
				} catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}else if ("PDF".equals(tipo)) {
				try {
					HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
							
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.setTable(7, 100);
					pdfDoc.setCell("Producto","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("% Comisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto del Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Comisi�n","celda01",ComunesPDF.CENTER);
					
					float dblTotalMontoDocumentoMN=0, dblTotalMontoComisionMN=0;
					float dblTotalMontoDocumentoDL=0, dblTotalMontoComisionDL=0;							
						
					while (rs.next()) { 
						String nombreif = rs.getString("nombreif")==null?"":rs.getString("nombreif");
						String ic_nombre = rs.getString("ic_nombre")==null?"":rs.getString("ic_nombre");
						String nombreepo = rs.getString("nombreepo")==null?"":rs.getString("nombreepo");
						String df_fecha_solicitud = rs.getString("df_fecha_solicitud")==null?"":rs.getString("df_fecha_solicitud");
						String fg_porc_comision_fondeo = rs.getString("fg_porc_comision_fondeo")==null?"":rs.getString("fg_porc_comision_fondeo");
						String ic_moneda = rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
						String cd_nombre = rs.getString("cd_nombre")==null?"":rs.getString("cd_nombre");
						String fn_monto = rs.getString("fn_monto")==null?"":rs.getString("fn_monto");
						String montocomision = rs.getString("montocomision")==null?"":rs.getString("montocomision");
						
						if(rs.getString("ic_moneda").equals("1")){
							dblTotalMontoDocumentoMN+=Float.valueOf(rs.getString("fn_monto")).floatValue();
							dblTotalMontoComisionMN+=Float.valueOf(rs.getString("MontoComision")).floatValue();
						}else{
							dblTotalMontoDocumentoDL+=Float.valueOf(rs.getString("fn_monto")).floatValue();
							dblTotalMontoComisionDL+=Float.valueOf(rs.getString("MontoComision")).floatValue();;
						}
						
							
						pdfDoc.setCell(ic_nombre,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(nombreepo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(df_fecha_solicitud,"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(fg_porc_comision_fondeo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cd_nombre,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(fn_monto, 2, false),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocomision, 2, false),"formas",ComunesPDF.CENTER);	
					}					
					pdfDoc.addTable();
					pdfDoc.setTable(3, 50);
					pdfDoc.addText("\r\nTotales","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("Total Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto del Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Comisi�n","celda01",ComunesPDF.CENTER);
					
					pdfDoc.setCell("\t\tTotal Moneda Nacional","formas",ComunesPDF.LEFT);
					pdfDoc.setCell("$ " + dblTotalMontoDocumentoMN+"    ","formas",ComunesPDF.RIGHT);		
					pdfDoc.setCell("$ " + dblTotalMontoComisionMN+"    ","formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("\t\tTotal Dolares","formas",ComunesPDF.LEFT);
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(dblTotalMontoDocumentoDL, 2, false)+"    ","formas",ComunesPDF.RIGHT);		
					pdfDoc.setCell("$ " + Comunes.formatoDecimal(dblTotalMontoComisionDL, 2, false)+"    ","formas",ComunesPDF.RIGHT);						
					pdfDoc.addTable(); 
					pdfDoc.endDocument();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}
		return nombreArchivo;	     
	}	
	
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}


	public void setSes_ic_if(String ses_ic_if) {
		this.ses_ic_if = ses_ic_if;
	}


	public String getSes_ic_if() {
		return ses_ic_if;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setDf_operacion_inicial(String df_operacion_inicial) {
		this.df_operacion_inicial = df_operacion_inicial;
	}


	public String getDf_operacion_inicial() {
		return df_operacion_inicial;
	}


	public void setDf_operacion_final(String df_operacion_final) {
		this.df_operacion_final = df_operacion_final;
	}


	public String getDf_operacion_final() {
		return df_operacion_final;
	}


	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}


	public String getIc_moneda() {
		return ic_moneda;
	}
	
}          