package com.netro.descuento;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;


public class ConsConfirmacionDoctos implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsConfirmacionDoctos.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String tipoConsulta;
	private String ic_documentos;
	private String doctosEstatus4;
	private String doctosEstatus2;
	public String[] regtotales;	
	public String[] cifrasControl;	
	 
	public ConsConfirmacionDoctos() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
			
			
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		StringBuffer qrySentencia2 	= new StringBuffer();
		StringBuffer qrySentencia4 	= new StringBuffer();
		conditions 		= new ArrayList();  
	
			log.debug("doctosEstatus2==  "+doctosEstatus2+" \n doctosEstatus4==  "+doctosEstatus4);
	
		if(tipoConsulta.equals("ConsultarTotales") )  { 
		
			qrySentencia2.append(" Select nvl(t3.NOMBRE_EPO,t4.NOMBRE_EPO) as NOMBRE_EPO , nvl(t3.ESTATUS, t4.ESTATUS) as ESTATUS ,   "+
			"  t3.TOTAL_DOCTOS_MN , t3.MONTO_DESCUENTO_MN  , t3.MONTO_INTERES_MN , t3.MONTO_OPERAR_MN ,  "+
			" t4.TOTAL_DOCTOS_DL , t4.MONTO_DESCUENTO_DL  , t4.MONTO_INTERES_DL , t4.MONTO_OPERAR_DL  "+
			" FROM  ( "+ 
			"  SELECT e.ic_epo,  e.cg_razon_social NOMBRE_EPO,  "+
			" 'Negociable' as ESTATUS,  "+
			" decode(d.ic_moneda, '1',count(*), '54', '0')  TOTAL_DOCTOS_MN , "+
			" decode(d.ic_moneda, '1',sum(d.fn_monto_dscto) ,'54', '0') as MONTO_DESCUENTO_MN, "+
			" decode(d.ic_moneda, '1',  sum(DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_importe_interes_fondeo, 0),  ds.in_importe_interes  )), '54','0' ) AS MONTO_INTERES_MN, "+
			" decode(d.ic_moneda, '1', sum(DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_importe_recibir_fondeo, 0), ds.in_importe_recibir  )), '54','0') AS MONTO_OPERAR_MN   "+
			" FROM  com_documento d ,  "+ 
			" com_docto_seleccionado ds, "+
			" comcat_epo e "+
			" WHERE d.ic_documento = ds.ic_documento "+
			" and  d.ic_epo = e.ic_epo "+
			" and d.ic_moneda = 1  "+
			" and d.ic_documento  in ( "+doctosEstatus2+" )   "+ 
			" group by e.ic_epo, e.cg_razon_social, d.ic_moneda   ) t3   "+
			" full outer join    "+			
			" ( SELECT  e.ic_epo, e.cg_razon_social NOMBRE_EPO,  "+
			" 'Negociable' as ESTATUS,  "+
			" decode(d.ic_moneda, '54',count(*), '1', '0')  TOTAL_DOCTOS_DL , "+
			" decode(d.ic_moneda, '54',sum(d.fn_monto_dscto), '1', '0') as MONTO_DESCUENTO_DL,  "+
			" decode(d.ic_moneda, '54',  sum(DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_importe_interes_fondeo, 0),  ds.in_importe_interes  )), '1','0' ) AS MONTO_INTERES_DL, "+
			" decode(d.ic_moneda, '54', sum(DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_importe_recibir_fondeo, 0), ds.in_importe_recibir  )) ,'1','0') AS MONTO_OPERAR_DL  "+
			" FROM  com_documento d ,  "+
			" com_docto_seleccionado ds, "+
			" comcat_epo e "+
			" WHERE d.ic_documento = ds.ic_documento "+
			" and  d.ic_epo = e.ic_epo "+
			" and d.ic_moneda = 54 "+  
			" and d.ic_documento  in ( "+doctosEstatus2+" )   "+
			" group by e.ic_epo, e.cg_razon_social, d.ic_moneda  ) t4 " +
			" on t3.ic_epo = t4.ic_epo ");
			
			qrySentencia4.append(" Select nvl(t1.NOMBRE_EPO,t1.NOMBRE_EPO) as NOMBRE_EPO , nvl(t1.ESTATUS, t2.ESTATUS) as ESTATUS ,  "+
			"  t1.TOTAL_DOCTOS_MN , t1.MONTO_DESCUENTO_MN  , t1.MONTO_INTERES_MN , t1.MONTO_OPERAR_MN ,  "+
			" t2.TOTAL_DOCTOS_DL , t2.MONTO_DESCUENTO_DL  , t2.MONTO_INTERES_DL , t2.MONTO_OPERAR_DL  "+
			"  FROM ( "+
				" SELECT  e.ic_epo, e.cg_razon_social NOMBRE_EPO, 'Operada' as ESTATUS, "+
				" decode(d.ic_moneda, '1',count(*), '54', '0')  TOTAL_DOCTOS_MN , "+
				" decode(d.ic_moneda, '1',sum(d.fn_monto_dscto) ,'54', '0') as MONTO_DESCUENTO_MN, "+
				" decode(d.ic_moneda, '1',  sum(DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_importe_interes_fondeo, 0),  ds.in_importe_interes  )), '54','0' ) AS MONTO_INTERES_MN, "+
				" decode(d.ic_moneda, '1', sum(DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_importe_recibir_fondeo, 0), ds.in_importe_recibir  )), '54','0') AS MONTO_OPERAR_MN  "+
				" FROM  com_documento d ,  "+
				" com_docto_seleccionado ds, "+
				" comcat_epo e "+
				" WHERE d.ic_documento = ds.ic_documento "+
				" and  d.ic_epo = e.ic_epo "+
				" and d.ic_moneda = 1  " +
				" and d.ic_documento in ( "+doctosEstatus4+" )   "+			
				" group by e.ic_epo, e.cg_razon_social, d.ic_moneda  ) t1   "  +
				" full outer join    "+
				" ( SELECT   e.ic_epo, e.cg_razon_social NOMBRE_EPO, 'Operada' as ESTATUS, "+
				" decode(d.ic_moneda, '54',count(*), '1', '0')  TOTAL_DOCTOS_DL , "+
				" decode(d.ic_moneda, '54',sum(d.fn_monto_dscto), '1', '0') as MONTO_DESCUENTO_DL,  "+
				" decode(d.ic_moneda, '54',  sum(DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_importe_interes_fondeo, 0),  ds.in_importe_interes  )), '1','0' ) AS MONTO_INTERES_DL, "+
				" decode(d.ic_moneda, '54', sum(DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_importe_recibir_fondeo, 0), ds.in_importe_recibir  )) ,'1','0') AS MONTO_OPERAR_DL  "+
				" FROM  com_documento d ,  "+
				" com_docto_seleccionado ds, "+
				" comcat_epo e "+
				" WHERE d.ic_documento = ds.ic_documento "+
				" and  d.ic_epo = e.ic_epo "+
				" AND d.ic_moneda = 54 "+
				" and d.ic_documento in ( "+doctosEstatus4+" )   "+
				" group by e.ic_epo, e.cg_razon_social, d.ic_moneda )t2   " +
				"  on t1.ic_epo = t2.ic_epo   ");
			
			
			
			if(!doctosEstatus2.equals("") && !doctosEstatus4.equals("") ) {
				qrySentencia.append( qrySentencia2.toString() +" union all  "+ qrySentencia4.toString() );
			}else if(!doctosEstatus2.equals("") && doctosEstatus4.equals("") ) {
				qrySentencia.append( qrySentencia2.toString() );
			}else if(doctosEstatus2.equals("") && !doctosEstatus4.equals("") ) {
				qrySentencia.append( qrySentencia4.toString() );
			}
			
	
		} else if(tipoConsulta.equals("ConsultarPreAcuse") || tipoConsulta.equals("ConsultarPreAcuse2")
		|| tipoConsulta.equals("ArchivoAcuse")  || tipoConsulta.equals("ArchivoAcuse_Interface")  )  { 
		
			qrySentencia.append( " 	SELECT   /*+  leading (d) use_nl(ds d p pe e ie i cd m tf)*/ "+         
					" DECODE (ds.cs_opera_fiso,  'S', 99999, p.in_numero_sirac) AS NUM_SIRAC, "+
					" DECODE (ds.cs_opera_fiso, 'S', ifs.cg_razon_social, p.cg_razon_social ) AS NOMBRE_PROVEEDOR, "+
					" d.ig_numero_docto as NUMER0_DOCTO ,  "+
					" TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS FECHA_EMISION, "+
					" TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS FECHA_VENCIMIENTO, "+
					" m.cd_nombre as MONEDA , "+
					" tf.cg_nombre  as TIPO_FACTORAJE, "+
					" d.fn_monto as MONTO,  "+
					" d.fn_porc_anticipo as PORCE_DESCUENTO,"+
					" d.fn_monto - d.fn_monto_dscto AS RECURSO_GARANTIA," +
					" d.fn_monto_dscto as MONTO_DESCUENTO ," +
					" DECODE (ds.cs_opera_fiso, 'S', NVL (ds.in_importe_interes_fondeo, 0),  ds.in_importe_interes  ) AS MONTO_INTERES," +
					" DECODE (ds.cs_opera_fiso,  'S', NVL (ds.in_importe_recibir_fondeo, 0), ds.in_importe_recibir  ) AS MONTO_OPERAR, " +
					" DECODE (ds.cs_opera_fiso,'S', NVL (ds.in_tasa_aceptada_fondeo, 0), ds.in_tasa_aceptada ) AS TASA," +
					" DECODE (ds.cs_opera_fiso, 'S', p.cg_razon_social, d.ct_referencia ) AS REFERENCIA, "+
					" (  d.df_fecha_venc  - DECODE (d.ic_moneda, 1, TRUNC (SYSDATE), 54, TO_DATE ('05/12/2014', 'dd/mm/yyyy') ) ) PLAZO, "+
					" pe.CG_PYME_EPO_INTERNO    as NUM_PROVEEDOR ,    "+
					" d.ic_documento as   IC_DOCUMENTO ,    "+					
					" e.cg_razon_social as NOMBRE_EMISIOR,  "+					
					" 'Factura' as DESCRIPCION_DOCTO ,  "+
					" 'Mexico, D.F.' as  LUGAR_SUSCRIPCION ,  "+
					" 'Al Vto' as  PERIODICIDAD,   "+		
					"  d.ic_moneda as IC_MONEDA ,  "+   
					" '' as CAUSA_RECHAZO , "+
					" '' as ESTATUS_ASIGNAR,  "+
					" ds.cc_acuse as ACUSE_PYME,  " +
					" e.ic_banco_fondeo AS  CLAVE_BFONDEO , "+
					" e.ic_epo  AS  IC_EPO,  "+
					"  TO_CHAR(ds.DF_FECHA_SELECCION,'dd/mm/yyyy')   as FECHA_SELECCION ,  "+
					"  TO_CHAR(d.df_alta,'dd/mm/yyyy')  as FECHA_PUBLICACION,  "+ 
					"  e.cg_razon_social as NOMBRE_EPO " );
					
					if(tipoConsulta.equals("ArchivoAcuse_Interface")  )  { 
						qrySentencia.append( ",  s.ic_folio  as IC_FOLIO ");
					}
					
					qrySentencia.append( "  FROM com_docto_seleccionado ds,"   +
					"       com_documento d,"   +
					"       comrel_pyme_epo pe,"   +
					"       comrel_if_epo ie,"   +
					"       comcat_pyme p,"   +
					"       comcat_epo e,"   +
					"       comcat_if i,"   +
					"       comcat_moneda m,"   +
					"       comcat_estatus_docto cd,"   +
					"       comcat_tipo_factoraje tf,"   +
					"	     comcat_if ifs ");
			
			if(tipoConsulta.equals("ArchivoAcuse_Interface")  )  { 			
				qrySentencia.append( " , com_solicitud s ");
			}
			qrySentencia.append("  WHERE ds.ic_documento = d.ic_documento"   +
					"	AND ifs.ic_if = ds.ic_if "+
					"  AND d.ic_estatus_docto = cd.ic_estatus_docto"   +
					"  AND ds.ic_epo = d.ic_epo"   +
					"  AND d.ic_pyme = p.ic_pyme"   +
					"  AND d.ic_moneda = m.ic_moneda"   +
					"  AND d.cs_dscto_especial = tf.cc_tipo_factoraje"   +
					"  AND pe.cs_habilitado = 'S'"   +
					"  AND d.ic_epo = pe.ic_epo"   +
					"  AND d.ic_pyme = pe.ic_pyme"   +
					"  AND d.ic_epo = e.ic_epo"   +
					"  AND ie.ic_if (+) = d.ic_beneficiario"   +
					"  AND ie.ic_epo (+) = d.ic_epo"   +
					"  AND ie.ic_if = i.ic_if (+)"   +
					"  AND d.ic_documento in ("+ic_documentos +")   ");
					
				if(tipoConsulta.equals("ArchivoAcuse_Interface")  )  { 			
					qrySentencia.append( " and s.ic_documento = d.ic_documento  ");
				} 
			
				qrySentencia.append(" ORDER BY p.cg_razon_social  ");
	
		}else if(tipoConsulta.equals("consultaTotales1")  )  {		
		
			qrySentencia.append( "  SELECT   /*+  leading (d) use_nl(ds d p pe e ie i cd m tf)*/  "+
			"  m.cd_nombre  as MONEDA ,  count(*) as TOTAL_DOCTO  "+
			"  FROM com_docto_seleccionado ds,"   +
			"       com_documento d,"   +
			"       comrel_pyme_epo pe,"   +
			"       comrel_if_epo ie,"   +
			"       comcat_pyme p,"   +
			"       comcat_epo e,"   +
			"       comcat_if i,"   +
			"       comcat_moneda m,"   +
			"       comcat_estatus_docto cd,"   +
			"       comcat_tipo_factoraje tf,"   +
			"	     comcat_if ifs"+
			"  WHERE ds.ic_documento = d.ic_documento"   +
			"	AND ifs.ic_if = ds.ic_if "+
			"  AND d.ic_estatus_docto = cd.ic_estatus_docto"   +
			"  AND ds.ic_epo = d.ic_epo"   +
			"  AND d.ic_pyme = p.ic_pyme"   +
			"  AND d.ic_moneda = m.ic_moneda"   +
			"  AND d.cs_dscto_especial = tf.cc_tipo_factoraje"   +
			"  AND d.ic_estatus_docto = 3"   +
			"  AND pe.cs_habilitado = 'S'"   +
			"  AND d.ic_epo = pe.ic_epo"   +
			"  AND d.ic_pyme = pe.ic_pyme"   +
			"  AND d.ic_epo = e.ic_epo"   +
			"  AND ie.ic_if (+) = d.ic_beneficiario"   +
			"  AND ie.ic_epo (+) = d.ic_epo"   +
			"  AND ie.ic_if = i.ic_if (+)"   +
			"  AND d.ic_documento in ("+ic_documentos +")   "+
			" group by m.cd_nombre  ");
			
		}else if(tipoConsulta.equals("consultaTotales2")  )  {		
		
			qrySentencia.append( "  SELECT   /*+  leading (d) use_nl(ds d p pe e ie i cd m tf)*/  "+
			"  m.cd_nombre AS moneda,  "+
         " COUNT (*) AS total_docto, "+
		   " sum(d.FN_monto) as TOTAL_IMPORTE_DOCTO,  "+
         " sum(DECODE (ds.cs_opera_fiso,  'S', NVL (ds.in_importe_recibir_fondeo, 0), ds.in_importe_recibir  ) ) as TOTAL_IMPORTE_DESC  "+
			" FROM com_docto_seleccionado ds,"   +
			"       com_documento d,"   +
			"       comrel_pyme_epo pe,"   +
			"       comrel_if_epo ie,"   +
			"       comcat_pyme p,"   +
			"       comcat_epo e,"   +
			"       comcat_if i,"   +
			"       comcat_moneda m,"   +
			"       comcat_estatus_docto cd,"   +
			"       comcat_tipo_factoraje tf,"   +
			"	     comcat_if ifs"+
			"  WHERE ds.ic_documento = d.ic_documento"   +
			"	AND ifs.ic_if = ds.ic_if "+
			"  AND d.ic_estatus_docto = cd.ic_estatus_docto"   +
			"  AND ds.ic_epo = d.ic_epo"   +
			"  AND d.ic_pyme = p.ic_pyme"   +
			"  AND d.ic_moneda = m.ic_moneda"   +
			"  AND d.cs_dscto_especial = tf.cc_tipo_factoraje"   +
			"  AND d.ic_estatus_docto = 3"   +
			"  AND pe.cs_habilitado = 'S'"   +
			"  AND d.ic_epo = pe.ic_epo"   +
			"  AND d.ic_pyme = pe.ic_pyme"   +
			"  AND d.ic_epo = e.ic_epo"   +
			"  AND ie.ic_if (+) = d.ic_beneficiario"   +
			"  AND ie.ic_epo (+) = d.ic_epo"   +
			"  AND ie.ic_if = i.ic_if (+)"   +
			"  AND d.ic_documento in ("+ic_documentos +")   "+
			" group by m.cd_nombre  ");

		}
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		try { 
			
			if("PDF".equals(tipo)){
			 
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
						
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
									
					
				//Cifras de Control 
				String regCtl =  cifrasControl[0] ;
				VectorTokenizer vtC = new VectorTokenizer(regCtl,"|");
				Vector regC = vtC.getValuesVector();	
				
				String totalNegocioables = 	(String)regC.get(0);
				String totalOperada = 	(String)regC.get(1);			
				String acuse = 	(String)regC.get(2);
				String fecha = 	(String)regC.get(3);
				String hora = 	(String)regC.get(4);			
				String usuario = 	(String)regC.get(5);
				String recibo = 	(String)regC.get(6);
				
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" La autentificaci�n se llev� a cabo con �xito \n Recibo: "+recibo,"formas",ComunesPDF.CENTER);			
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
					
				pdfDoc.setTable(10,100);	
				pdfDoc.setCell("", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Moneda Nacional", "celda02",ComunesPDF.CENTER, 4);	
				pdfDoc.setCell("Dolares", "celda02",ComunesPDF.CENTER, 4);	
				pdfDoc.setCell("EPO", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Estatus", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Total de Documentos", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Total Monto Descuento", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Total Monto Interes", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto a Operar", "celda02",ComunesPDF.CENTER);				
				pdfDoc.setCell("Total de Documentos", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Total Monto Descuento", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Total Monto Interes", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto a Operar", "celda02",ComunesPDF.CENTER);	
				
				
				for(int i=0;i<regtotales.length;i++){
					String registro =  regtotales[i] ;
					VectorTokenizer vt = new VectorTokenizer(registro,"|");
					Vector reg = vt.getValuesVector();				
					 pdfDoc.setCell( (String)reg.get(0), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell( (String)reg.get(1), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell( (String)reg.get(2), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell( (String)reg.get(3), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell( (String)reg.get(4), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell( (String)reg.get(5), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell( (String)reg.get(6), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell( (String)reg.get(7), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell( (String)reg.get(8), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell( (String)reg.get(9), "formas",ComunesPDF.CENTER);	
				}			
				pdfDoc.addTable();
				 
				pdfDoc.addText("  " ,"formas",ComunesPDF.RIGHT);
				
				
					
				
				pdfDoc.setTable(2,50);	
				pdfDoc.setCell("Cifras de Control", "celda02",ComunesPDF.CENTER, 2);	
				pdfDoc.setCell("No. Total de registros a Negociable", "formas",ComunesPDF.LEFT);	
				pdfDoc.setCell(totalNegocioables, "formas",ComunesPDF.LEFT);
				pdfDoc.setCell("No. Total de registros a Operado", "formas",ComunesPDF.LEFT);	
				pdfDoc.setCell(totalOperada, "formas",ComunesPDF.LEFT);	
				pdfDoc.setCell("N�mero  de Acuse", "formas",ComunesPDF.LEFT);	
				pdfDoc.setCell(acuse, "formas",ComunesPDF.LEFT);	
				pdfDoc.setCell("Fecha de carga", "formas",ComunesPDF.LEFT);	
				pdfDoc.setCell(fecha, "formas",ComunesPDF.LEFT);	
				pdfDoc.setCell("Hora de carga", "formas",ComunesPDF.LEFT);	
				pdfDoc.setCell(hora, "formas",ComunesPDF.LEFT);	
				pdfDoc.setCell("Usuario de captura", "formas",ComunesPDF.LEFT);	
				pdfDoc.setCell(usuario, "formas",ComunesPDF.LEFT);	
				pdfDoc.setCell("Al aceptar realizar el factoraje electr�nico o descuento electr�nico de los derechos que solicita la mipyme, me obligo a su fondeo en los t�rminos y condiciones establecidas con la mipyme. En este acto me obligo a notificar a la empresa de primer orden mi calidad de titular de los derechos sobre el documento descontado o factoreado y  reiterarle su obligaci�n de pagarme en tiempo y forma la cantidad a que se refiere el documento aqu� descontado o factoreado.\n Las operaciones que aparecen en esta p�gina fueron notificadas a la empresa de primer orden de conformidad y para efectos de los art�culos 427 de la Ley General de T�tulos y Operaciones de Cr�dito, 32 C del C�digo Fiscal de la Federaci�n y 2038 del C�digo Civil Federal seg�n corresponda, para los efectos legales conducentes", "celda02",ComunesPDF.JUSTIFIED, 2);	
				pdfDoc.addTable();  
				
				pdfDoc.addText("  " ,"formas",ComunesPDF.RIGHT); 
				
				pdfDoc.setTable(11,100);	
				pdfDoc.setCell("A", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("No. Cliente Sirac", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Proveedor/Cedente", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("N�mero de Documento", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Fecha de Emisi�n", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Fecha de Vencimiento", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Moneda", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Tipo Factoraje", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Porcentaje de Descuento", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Recurso en Garant�a", "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("B", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto a Descontar", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto Inter�s", "celda02",ComunesPDF.CENTER);				
				pdfDoc.setCell("Monto a Operar", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Tasa", "celda02",ComunesPDF.CENTER);				
				pdfDoc.setCell("Plazo", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("No. Proveedor", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("Ic_Documento", "celda02",ComunesPDF.CENTER);	
				pdfDoc.setCell("", "celda02",ComunesPDF.CENTER,3);	
				
				while (rs.next())	{					
					String num_sirac = (rs.getString("NUM_SIRAC") == null) ? "" : rs.getString("NUM_SIRAC");
					String nombreProveedor = (rs.getString("NOMBRE_PROVEEDOR") == null) ? "" : rs.getString("NOMBRE_PROVEEDOR");
					String numero_docto = (rs.getString("NUMER0_DOCTO") == null) ? "" : rs.getString("NUMER0_DOCTO");
					String fechaEmision = (rs.getString("FECHA_EMISION") == null) ? "" : rs.getString("FECHA_EMISION");
					String fechaVencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					String tipoFactoraje = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
					String monto = (rs.getString("MONTO") == null) ? "" : rs.getString("MONTO");
					String porcenDescuento = (rs.getString("PORCE_DESCUENTO") == null) ? "" : rs.getString("PORCE_DESCUENTO");
					String recGarantia = (rs.getString("RECURSO_GARANTIA") == null) ? "" : rs.getString("RECURSO_GARANTIA");
					String montoDesct = (rs.getString("MONTO_DESCUENTO") == null) ? "" : rs.getString("MONTO_DESCUENTO");
					String montoInteres = (rs.getString("MONTO_INTERES") == null) ? "" : rs.getString("MONTO_INTERES");
					String montoOperar = (rs.getString("MONTO_OPERAR") == null) ? "" : rs.getString("MONTO_OPERAR");
					String tasa = (rs.getString("TASA") == null) ? "" : rs.getString("TASA");
					String plaza = (rs.getString("PLAZO") == null) ? "" : rs.getString("PLAZO");
					String num_proveedor = (rs.getString("NUM_PROVEEDOR") == null) ? "" : rs.getString("NUM_PROVEEDOR");
					String ic_documento = (rs.getString("IC_DOCUMENTO") == null) ? "" : rs.getString("IC_DOCUMENTO");
								
					pdfDoc.setCell("A", "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(num_sirac, "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(nombreProveedor, "formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(numero_docto, "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(fechaEmision, "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(fechaVencimiento, "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(moneda, "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(tipoFactoraje, "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2), "formas",ComunesPDF.RIGHT);	
					pdfDoc.setCell(porcenDescuento+"%", "formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("$"+Comunes.formatoDecimal(recGarantia,2) , "formas",ComunesPDF.RIGHT);	
					pdfDoc.setCell("B", "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDesct,2) , "formas",ComunesPDF.RIGHT);	
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoInteres,2) , "formas",ComunesPDF.RIGHT);	
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoOperar,2) , "formas",ComunesPDF.RIGHT);	
					pdfDoc.setCell(tasa+"%", "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(plaza, "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(num_proveedor, "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(ic_documento, "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("", "formas",ComunesPDF.CENTER,3);	
				}
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
					
			}else   if("CSV".equals(tipo)){
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
		
		//Cifras de Control 
				String regCtl =  cifrasControl[0] ;
				VectorTokenizer vtC = new VectorTokenizer(regCtl,"|");
				Vector regC = vtC.getValuesVector();	
				
				String totalNegocioables = 	(String)regC.get(0);
				String totalOperada = 	(String)regC.get(1);			
				String acuse = 	(String)regC.get(2);
				String fecha = 	(String)regC.get(3);
				String hora = 	(String)regC.get(4);			
				String usuario = 	(String)regC.get(5);
				String recibo = 	(String)regC.get(6);
				 
				contenidoArchivo = new StringBuffer();		
				contenidoArchivo.append(" N�mero de Acuse, Fecha de Carga, Hora de Carga, Usuario de Captura  \n");		
			   contenidoArchivo.append("'"+acuse +"',"+ fecha +","+   hora +","+ usuario  +"\n");	
			
			
			
			  contenidoArchivo.append(" \n No. Cliente SIRAC ,  Proveedor ,  N�mero de documento ,  Fecha de Emisi�n,  Fecha de Vencimiento,  "+
			  "  Moneda ,  Monto ,  Monto Descuento,  Monto Inter�s ,  Monto a Operar ,  Tasa ,  Plazo ,  Folio ,  No. Proveedor ,  Porcentaje de Descuento ,  "+
			  " Recurso de Garant�a ,  Tipo de factoraje ,  ic_documento \n ");	
			  
			  	while (rs.next())	{					
					String num_sirac = (rs.getString("NUM_SIRAC") == null) ? "" : rs.getString("NUM_SIRAC");
					String nombreProveedor = (rs.getString("NOMBRE_PROVEEDOR") == null) ? "" : rs.getString("NOMBRE_PROVEEDOR");
					String numero_docto = (rs.getString("NUMER0_DOCTO") == null) ? "" : rs.getString("NUMER0_DOCTO");
					String fechaEmision = (rs.getString("FECHA_EMISION") == null) ? "" : rs.getString("FECHA_EMISION");
					String fechaVencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					String tipoFactoraje = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
					String monto = (rs.getString("MONTO") == null) ? "" : rs.getString("MONTO");
					String porcenDescuento = (rs.getString("PORCE_DESCUENTO") == null) ? "" : rs.getString("PORCE_DESCUENTO");
					String recGarantia = (rs.getString("RECURSO_GARANTIA") == null) ? "" : rs.getString("RECURSO_GARANTIA");
					String montoDesct = (rs.getString("MONTO_DESCUENTO") == null) ? "" : rs.getString("MONTO_DESCUENTO");
					String montoInteres = (rs.getString("MONTO_INTERES") == null) ? "" : rs.getString("MONTO_INTERES");
					String montoOperar = (rs.getString("MONTO_OPERAR") == null) ? "" : rs.getString("MONTO_OPERAR");
					String tasa = (rs.getString("TASA") == null) ? "" : rs.getString("TASA");
					String plaza = (rs.getString("PLAZO") == null) ? "" : rs.getString("PLAZO");
					String num_proveedor = (rs.getString("NUM_PROVEEDOR") == null) ? "" : rs.getString("NUM_PROVEEDOR");
					String ic_documento = (rs.getString("IC_DOCUMENTO") == null) ? "" : rs.getString("IC_DOCUMENTO");
					String ic_folio = (rs.getString("IC_FOLIO") == null) ? "" : rs.getString("IC_FOLIO");
										
					  contenidoArchivo.append( num_sirac.replace(',',' ')+", "+ 
					  nombreProveedor.replace(',',' ')+", "+
					  numero_docto.replace(',',' ')+", "+
					  fechaEmision.replace(',',' ')+", "+
					  fechaVencimiento.replace(',',' ')+", "+
					  moneda.replace(',',' ')+", "+
					  monto.replace(',',' ')+", "+
					  montoDesct.replace(',',' ')+", "+
					  montoInteres.replace(',',' ')+", "+
					  montoOperar.replace(',',' ')+", "+
					  tasa.replace(',',' ')+", "+
					  plaza.replace(',',' ')+", "+
					  "'"+ic_folio.replace(',',' ')+"', "+
					  num_proveedor.replace(',',' ')+", "+					  
					  porcenDescuento.replace(',',' ')+", "+
					  recGarantia.replace(',',' ')+", "+
					  tipoFactoraje.replace(',',' ')+", "+
					  ic_documento.replace(',',' ')+", "+	 				  
					 "\n "); 					  
				}
			
				buffer.write(contenidoArchivo.toString());
				buffer.close();	
				contenidoArchivo = new StringBuffer();//Limpio    
			
			
			}
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)"); 
		}
		return nombreArchivo;
	}
	
	
	 

	public HashMap cargaArchivoConfDocto(String strDirectorioTemp,String  nombreArchivo, String ic_if  ){
		log.info ("cargaArchivoConfDocto  (E) " );
		HashMap  registros = new HashMap();	
		AccesoDB 	con = new AccesoDB();
      boolean		exito = true;	
		String linea = "", ic_documento ="", claveEstatus ="", causaRechazo="", mensaje ="";
		StringBuffer 	mError  = new StringBuffer(); 
		int numLinea = 0, totalLineas =0;
		List documentos = new ArrayList();
		StringBuffer 	datosError  = new StringBuffer(); 		
		StringBuffer 	n_documentos  = new StringBuffer();  		
		StringBuffer 	doctosEstatus4  = new StringBuffer();  
		StringBuffer 	doctosEstatus2  = new StringBuffer();  		
	  int total2 = 0, total4= 0;
	  	
		StringBuffer 	lDocumentos  = new StringBuffer();  	
		 
		try {
		
			con.conexionDB();   
			AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);


		
			java.io.File lofArchivo = new java.io.File(strDirectorioTemp+nombreArchivo);
			
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo),"ISO-8859-1"));
			
			while((linea=br.readLine())!=null) {				
				VectorTokenizer vt = new VectorTokenizer(linea,"|");
				Vector lineaArchivo = vt.getValuesVector();
				int columna =1;
				
				mError  = new StringBuffer(); 
			   int ic_estatus =0;
				String claveEpo = "";	
				int claveMoneda =0;
				
				if(lineaArchivo.size()>0){
					
					numLinea++;
					
					if (lineaArchivo.size() >= columna) { 	ic_documento = (String)lineaArchivo.get(columna-1);  columna++; 	}
					if (lineaArchivo.size() >= columna) { 	claveEstatus = (String)lineaArchivo.get(columna-1);  columna++; 	}
					if (lineaArchivo.size() >= columna) { 	causaRechazo = (String)lineaArchivo.get(columna-1);  columna++; 	}
						
					
					//*************ic_documento **********************
						
					if(!Comunes.esNumero(ic_documento)){ 			 				
						mError.append("Error en la l�nea: "+numLinea+" El documento no es un n�mero valido. \n");															
					}else if( Comunes.esNumero(ic_documento)){  	   					
						
						HashMap regi = getDatosDocumento(ic_documento, ic_if );	
						ic_estatus = Integer.parseInt(regi.get("ic_estatus").toString());
						claveEpo = regi.get("ic_epo").toString();
						claveMoneda =  Integer.parseInt(regi.get("ic_moneda").toString());
						
						if(ic_documento.length() >38){
							mError.append("Error en la l�nea: "+numLinea+" El ic_documento excede la longitud m�xima de 38 caracteres. \n");	
						}else  if(getValidaDoctoExiste(ic_documento, ic_if )==0){
							mError.append("Error en la l�nea: "+numLinea+" El documento no existe o no pertenece al Intermediario. \n	");	
						}else if(ic_estatus !=3) {
							mError.append("Error en la l�nea: "+numLinea+" El documento no se  puede cambiar de estatus en Seleccionada PYME. Estatus # "+ic_estatus +"\n");	
							
						//*************claveEstatus **********************	 
						}else if(!Comunes.esNumero(claveEstatus)){ 			 				
							mError.append("Error en la l�nea: "+numLinea+" La clave del estatus no es un n�mero valido. \n");						
						}else  	if( claveEstatus == null || "".equals(claveEstatus) ){
							mError.append("Error en la l�nea: "+numLinea+" La clave del estatus no es un n�mero valido. \n");
						}else if(claveEstatus.length() >1){
							mError.append("Error en la l�nea: "+numLinea+"	La clave del estatus excede la longitud m�xima de 1 car�cter.	\n");		
						}else if(!"2".equals(claveEstatus)  &&  !"4".equals(claveEstatus)  ){
							mError.append("Error en la l�nea: "+numLinea+" La clave del estatus no es v�lida solo se acepta 2 (Negociable) y 4 (Operada). \n ");
												
						//*************causaRechazo **********************	  
						
						}else if("2".equals(claveEstatus) ){
							if("".equals(causaRechazo)  ){					
								mError.append("Error en la l�nea: "+numLinea+" Las causas de rechazo son obligatorias si el estatus a asignar es 2(Negociable)  \n");	
							}else if(causaRechazo.length() >260 ){	
								mError.append("Error en la l�nea: "+numLinea+" Las causas de rechazo exceden la longitud m�xima de 260  \n");
							}					
						}else if("4".equals(claveEstatus) ){
							
							if(!"".equals(causaRechazo)  ){					
								mError.append("Error en la l�nea: "+numLinea+" Las causas de rechazo no son v�lidas si el estatus es 4 (Operada). \n "); 
							}else if (!BeanAutDescuento.hayClaseDoctoAsociado(claveEpo)) {	
								mError.append("Error en la l�nea: "+numLinea+" No existe una clase de documento asociado para realizar Descuento");								
							}		
								
							// esta validaci�n esta e  linea 2111
							
							int iPlazoMaxBO = 0;
							Map mPlazoBO = null;
							try {
							
								log.debug(" claveMoneda=" + claveMoneda);
							
								//Determina si existe base de operaci�n para la epo-if-moneda
								mPlazoBO = BeanAutDescuento.getPlazoBO(claveEpo, ic_if, new int[] {claveMoneda});
	
								List lPlazos = Comunes.explode(":", (String)mPlazoBO.get("sPlazoBO"));
								log.debug(" lPlazos=" + lPlazos);
								if (lPlazos != null && lPlazos.size() != 0) {
									//Solo debe tener un elemento los plazos, ya que solo se pasa una moneda a getPlazoBO
									List elementos = Comunes.explode( "|", (String)lPlazos.get(0) );
									log.debug("elementos " + elementos);
									if(elementos!=null && elementos.size() !=0) {
										iPlazoMaxBO = Integer.parseInt((String)elementos.get(1));//Plazo
									}
									log.debug("iPlazoMaxBO iPlazoMaxBO=" + iPlazoMaxBO);
								}
	
								if (iPlazoMaxBO == 0) { //No se encontro Base de operacion
									mError.append("Error en la l�nea: "+numLinea+" "+(String)mPlazoBO.get("sMsgError")) ;								
								}	
							
							} catch(Throwable t) {
								throw new Exception("Error al determinar la base de operacion epo-if-moneda.",t);
							}
							 
							
							//Verifica que la Solicitud no exista previamente							
							boolean existeSolicitud = false;
							
							String querySolicitud =
								" SELECT COUNT(*) as numRegistros " +
								" FROM com_solicitud "+
								" WHERE ic_documento = ? ";
								
							log.debug("qrySentencia === "+qrySentencia +"\n ic_documento ==="+ic_documento);	
							
							PreparedStatement 	ps = con.queryPrecompilado(querySolicitud);
					
							ps.setString(1,ic_documento);
							ResultSet rs  = ps.executeQuery();
							if(rs.next()) {
								existeSolicitud = (rs.getInt("numRegistros") > 0)?true:false;
							}
							rs.close();
							
							if (existeSolicitud) {
								mError.append("Error en la l�nea: "+numLinea+" El documento ya tiene registrada una solicitud de descuento previa");							
							}
														
						}	// if("4".equals(claveEstatus) )
					}
					
					
					if(mError.length()==0) {	
						documentos.add(ic_documento+"<br>");	
						n_documentos.append(ic_documento+",");
						
						if("4".equals(claveEstatus) ){
							doctosEstatus4 .append(ic_documento+",");
							total4++;
						}else if("2".equals(claveEstatus) ){
							doctosEstatus2.append(ic_documento+",");
							total2++;
						}	
					
						lDocumentos.append(ic_documento+'|'+claveEstatus+'|'+causaRechazo+",");
						
						
					}else  {
						datosError.append(mError+"<br>"); 						
					}
							
					}//if(lineaArchivo.size()>0){	
					
				}//while((linea=br.readLine())!=null) {
			
			   if(numLinea>350) {
					mensaje ="El archivo a  cargar  debe ser un .txt y deber� de contener 350 registros m�ximo";
				}						
			
				registros.put("REG_ERROR",datosError.toString());						
				registros.put("REG_BIEN",documentos);
				registros.put("MENSAJE",mensaje);
				registros.put("numLinea",String.valueOf(numLinea)); 
				registros.put("n_documentos",String.valueOf(n_documentos)); 
				registros.put("doctosEstatus2",doctosEstatus2.toString()); 
				registros.put("doctosEstatus4",doctosEstatus4.toString()); 
				registros.put("lDocumentos",lDocumentos.toString());  
				registros.put("total2",String.valueOf(total2)); 
				registros.put("total4",String.valueOf(total4)); 
		
		} catch(Throwable e) {
			exito = false;			
			e.printStackTrace();
			log.error ("cargaArchivoConfDocto  " + e);
			throw new AppException("SIST0001");			
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();				
				log.info("  cargaArchivoConfDocto (S) ");
			}			
		}
		
		return registros;
	}
	
	
	public int getValidaDoctoExiste(String icDocumento, String ic_if  ){
		log.info("getValidaDoctoExiste(E)");
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer();	
		List 	lVarBind = new ArrayList();
		ResultSet 	      rs	   =  null;
		PreparedStatement ps    =  null;
		int existeDocto =0;
		try{
		
			con.conexionDB();			
			
			qrySentencia.append(
					" SELECT  count(*)  as total  " +
					" FROM  com_documento   	 "  +
					" WHERE ic_documento =  ?  "+
					" AND ic_if  = ?  ");
					
			lVarBind = new ArrayList();
			lVarBind.add(icDocumento);  
			lVarBind.add(ic_if);  
						
			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();
         if(rs.next()){
				existeDocto =rs.getInt("total");
			}         
			rs.close();
			ps.close(); 
		
		
		}catch(Exception e){				
			e.printStackTrace();		
			log.error("getValidaDoctoExiste(Exception)"+e);		
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getValidaDoctoExiste(S)");
		}
		return existeDocto;
	
	}
	
	public HashMap getDatosDocumento(String icDocumento, String ic_if  ){
		log.info("getDatosDocumento(E)");
		AccesoDB 		con 				= new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer();	
		List 	lVarBind = new ArrayList();
		ResultSet 	      rs	   =  null;
		PreparedStatement ps    =  null;		
		HashMap  registros = new HashMap();	
		
		try{
		
			con.conexionDB();			
			
			qrySentencia.append(
					" SELECT  ic_estatus_docto, ic_epo , ic_moneda "  + 
					" FROM  com_documento   	 "  +
					" WHERE ic_documento =  ?  "+
					" AND ic_if  = ?  ");
					
			lVarBind = new ArrayList();
			lVarBind.add(icDocumento);  
			lVarBind.add(ic_if);   
			
			log.debug("qrySentencia === "+qrySentencia+" \n   lVarBind === "+lVarBind);  
				
			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();
         if(rs.next()){
				String ic_estatus =rs.getString("ic_estatus_docto");
				String ic_epo =rs.getString("ic_epo");
				String ic_moneda =rs.getString("ic_moneda"); 				
				registros.put("ic_estatus",ic_estatus);	
				registros.put("ic_epo",ic_epo);	
				registros.put("ic_moneda",ic_moneda);	
			}else  {
				registros.put("ic_estatus","0");	
				registros.put("ic_epo","0");	
				registros.put("ic_moneda","0"); 	
			}
			rs.close();
			ps.close(); 
			
			
				
		
		
		}catch(Exception e){				
			e.printStackTrace();		
			log.error("getDatosDocumento(Exception)"+e);		
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getDatosDocumento(S)");
		}
		return registros;
	
	}


	public String generacionArcErrores(String path, String datosErrores) {
	
		log.debug("generacionArcErrores (E)");
		String nombreArchivo ="";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		try { 
		
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			
			contenidoArchivo = new StringBuffer();					
			contenidoArchivo.append("Registros con Errores \n ");			
			contenidoArchivo.append(datosErrores+"\n" );		
							
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {

			} catch(Exception e) {}
		  log.debug("generacionArcErrores (S)");
		}
		return nombreArchivo; 
	} 
	  
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getIc_documentos() {
		return ic_documentos;
	}

	public void setIc_documentos(String ic_documentos) {
		this.ic_documentos = ic_documentos;
	}

	public String getDoctosEstatus4() {
		return doctosEstatus4;
	}

	public void setDoctosEstatus4(String doctosEstatus4) {
		this.doctosEstatus4 = doctosEstatus4;
	}

	public String getDoctosEstatus2() {
		return doctosEstatus2;
	}

	public void setDoctosEstatus2(String doctosEstatus2) {
		this.doctosEstatus2 = doctosEstatus2;  
	}
	
	public String[]  getRegtotales() {
		return regtotales;
	}

	public void setRegtotales(String[]  regtotales) {
		this.regtotales = regtotales;
	}
	
	public String[]  getCifrasControl() {
		return cifrasControl;
	}

	public void setCifrasControl(String[]  cifrasControl) {
		this.cifrasControl = cifrasControl;
	}
	
	
	

}