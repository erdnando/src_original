package com.netro.cedi;

import com.netro.cadenas.ConIntermediarios;
import com.netro.pdf.ComunesPDF;

import com.sun.org.apache.xpath.internal.operations.And;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Blob;

import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;


public class ConsSolicitudes  implements IQueryGeneratorRegExtJS    {
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsSolicitudes.class);
	
	private String    paginaOffset;
	private String    paginaNo;
	private List      conditions;
	StringBuffer qrySentencia   = new StringBuffer();
	
	private String ic_intermediario;
	private String txtRFC;
	private String txtFolioSolicitud;
	private String txtFechaSoliIni;
	private String txtFechaSoliFinal;
	private String solicAsignadas;
	
	
	
	
	public ConsSolicitudes() {}

	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
		
		
	public String getDocumentSummaryQueryForIds(List ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}
	
		
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
		
	
		
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
	   qrySentencia.append( " SELECT s.IC_SOLICITUD as NO_SOLICITUD , "+
								   "  a.CG_RAZON_SOCIAL as RAZON_SOCIAL , "+
									"  a.CG_RFC as RFC_EMPRESARIAL ,  "+
									"  to_char(s.DF_ALTA,'DD/MM/YYYY HH24:MI:SS') as FECHA_HORA_REG ,  "+
									"  e.CG_NOMBRE as ESTATUS , "+
									"  e.IC_ESTATUS_SOLICITUD as IC_ESTATUS ,  "+
									"  s.CG_USUARIO_ASIGNADO AS LOGIN_EJECUTIVO , " + 
									"  '' AS EJECUTIVO_CEDI , "+
									" LENGTH (BI_BALANCE)  as LON_BALANCE ,  "+
									" LENGTH (BI_EDO_RESULTADOS)  as LON_RESULTADOS ,  "+
									" LENGTH (BI_RFC)  as LON_RFC ,  "+ 
									" LENGTH (BI_CEDULA_COMPLETA) as LON_CEDULA_COMPLETA "+ 									
									"  FROM  cedi_solicitud s, CEDI_IFNB_ASPIRANTE  a , CEDICAT_ESTATUS_SOLICITUD e "+
									
									"  where  s.IC_IFNB_ASPIRANTE = a.IC_IFNB_ASPIRANTE  "+
									"  and s.IC_ESTATUS_SOLICITUD =   e.IC_ESTATUS_SOLICITUD   " +
									"  and s.IC_ESTATUS_SOLICITUD in ( ?, ? , ? )   ");
	   conditions.add("2"); 
	   conditions.add("3"); 
	   conditions.add("5");  
		
	   if(!ic_intermediario.equals("")){
	      qrySentencia.append("AND  s.IC_IFNB_ASPIRANTE =  ? ");   
	      conditions.add(ic_intermediario);         
		}
		
	   if(!txtRFC.equals("")){			
			qrySentencia.append("AND  a.CG_RFC =  ? ");   
		   conditions.add(txtRFC);         
		}
			
	   if(!txtFolioSolicitud.equals("")){       
	      qrySentencia.append("AND s.IC_SOLICITUD =  ? ");   
	      conditions.add(txtFolioSolicitud);         
	   }
	
	   if(!txtFechaSoliIni.equals("") && !txtFechaSoliFinal.equals("")){
	      qrySentencia.append(" and s.DF_ALTA >=  to_date(?, 'dd/mm/yyyy')  ") ;
	      qrySentencia.append("and s.DF_ALTA < to_date(?, 'dd/mm/yyyy')+1  ") ;
	      conditions.add(txtFechaSoliIni);
	      conditions.add(txtFechaSoliFinal);
		
		}
	 
	
	   if(!solicAsignadas.equals("")){       
	      qrySentencia.append("AND s.IC_SOLICITUD in ( " +solicAsignadas+ ")" );   
	           
	   }
	   
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}
		
		
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		log.debug("crearCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		StringBuffer contenidoArchivo = new StringBuffer("");
		
					
		try {
		
		
		}catch(Exception e) { 
				log.error("Error en la generacion del Archivo CSV: " + e);
		}	

			return nombreArchivo;		 				 				 		
		}
				 
					
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		}catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			
			} catch(Exception e) {}
		  
			}
			return nombreArchivo;
		}
 
 
	/*
	 * metodo que realiza la Asignaci�n de los ejecutivos CEDI
	 */

	public HashMap  asignarEjecutivoCEDI(String ic_solicitud [], String estatus [],  String ejecutivoAsig, String usuario ) throws AppException{
		log.info("adignarEjecutivoCEDI(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();
	   boolean		exito = true;	
		String acuse = "", solicAsignadas ="";
		
	   HashMap datos = new HashMap();
		try { 
		 
		   Cedi cediBean = ServiceLocator.getInstance().lookup("CediEJB", Cedi.class); 

		 	con.conexionDB();
		   
			acuse =	generaAcuse();	   
		  
		   datos.put("ACUSE", acuse);
			
			
		   strSQL = new StringBuffer();
		   strSQL.append(" insert into CEDI_ACUSE_ASIGNACION    "+
		                 " (   CC_ACUSE ,  DF_ASIGNACION ,   CG_USUARIO   )  "+
		                 " values (  ?, Sysdate , ? )  ");
		   
		   lVarBind    = new ArrayList();  
		   lVarBind.add(acuse);        
		   lVarBind.add(usuario);
		   
		   log.debug("strSQL  "+strSQL);
		   log.debug("lVarBind  "+lVarBind);
		                                                                                                                             
		   ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
		   ps.executeUpdate();
		   ps.close();
			
			
			for(int i=0;i<ic_solicitud.length;i++){  
				
			   solicAsignadas +=ic_solicitud[i]+",";
																					
			   strSQL = new StringBuffer();
				strSQL.append(" insert into  CEDIHIS_ASIGNACION_SOLICITUD " +
								  "  (  CC_ACUSE ,  IC_SOLICITUD ,  CG_USUARIO_ASIGNADO ,  IC_ESTATUS_SOLICITUD ) "+
								  " VALUES ( ? ,  ?, ? , ?  ) ");
				
				lVarBind    = new ArrayList();  
			   lVarBind.add(acuse);
				lVarBind.add(ic_solicitud[i]);
			   lVarBind.add(ejecutivoAsig);
				lVarBind.add(estatus[i]);			
			   
				log.debug("strSQL  "+strSQL);
			   log.debug("lVarBind  "+lVarBind);
				String estatusAsignar = estatus[i];
				
				if(estatus[i].equals("2")) 																																							  {
					estatusAsignar = "3";
				}
			   				
				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close(); 
				
				
			   strSQL = new StringBuffer();
			   strSQL.append(" Update  CEDI_SOLICITUD "+
					" set  CG_USUARIO_ASIGNADO  =  ? ,    "+
					" IC_ESTATUS_SOLICITUD  = ?  " +
					" where  IC_SOLICITUD =  ? ");
				   
				lVarBind    = new ArrayList();  
				lVarBind.add(ejecutivoAsig);
			   lVarBind.add(estatusAsignar);
			   lVarBind.add(ic_solicitud[i]);
				
			   log.debug("strSQL  "+strSQL);
			   log.debug("lVarBind  "+lVarBind);
				
			   ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			   ps.executeUpdate();
			   ps.close();
				
			   //4 Asignaci�n de Solicitud 
			    cediBean.setMovimientoSolicitus( ic_solicitud[i], usuario, estatusAsignar, "4", "") ; 
			      
				
			}
		
		   int tamanio2 = solicAsignadas.length();
			String val =  solicAsignadas.substring(tamanio2-1, tamanio2);
			if(val.equals(",")){
				solicAsignadas =solicAsignadas.substring(0,tamanio2-1);
			}
			
			
			log.debug("solicAsignadas ========= "+solicAsignadas);
			
		   datos.put("SOLICASIGNADAS", solicAsignadas);
			
		   enviaCorreoAsignacion(  ejecutivoAsig,  usuario,    solicAsignadas );  
																																		 
			
		} catch(Throwable e) {
			exito = false;			
			e.printStackTrace();
			log.error ("adignarEjecutivoCEDI  " + e);
			throw new AppException("SIST0001");			
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();				
				log.info("  adignarEjecutivoCEDI (S) ");
			}			
		}
	  return datos;
	}
	

	//Metodo para enviar el correo al ejecutivo que se le asignaton las solicitudes
	
	private void enviaCorreoAsignacion(String  ejecutivoAsig, String  usuarioFirmado,  String  solicAsignadas )throws AppException{
	   log.info("  enviaCorreoAsignacion (E) ");		
		
		Correo correo = new Correo();
	  
	   UtilUsr     utilUsr        = new UtilUsr();
		
		try{
			
		   String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
		               "font-size: 10px; "+
		               "font-weight: bold;"+
		               "color: #FFFFFF;"+
		               "background-color: #4d6188;"+
		               "padding-top: 3px;"+
		               "padding-right: 1px;"+
		               "padding-bottom: 1px;"+
		               "padding-left: 3px;"+
		               "height: 25px;"+
		               "border: 1px solid #1a3c54;'";
		            
			String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
		               "color:#000066; "+
		               "padding-top:1px; "+
		               "padding-right:2px; "+
		               "padding-bottom:1px; "+
		               "padding-left:2px; "+
		               "height:22px; "+
		               "font-size:11px;'";
		               
							
		 			
		   String  asunto = "", paraEje ="", nombreDe ="",  correoDe ="";
		   StringBuffer contenido = new StringBuffer(); 
			
		   Usuario usuarioPara = utilUsr.getUsuario(ejecutivoAsig);
		   paraEje = usuarioPara.getEmail();  
		 	
		   Usuario usuario = utilUsr.getUsuario(usuarioFirmado);
		   nombreDe = usuario.getNombre()+" "+usuario.getApellidoPaterno()+" "+usuario.getApellidoMaterno();
		   correoDe = usuario.getEmail();  
			
		   List  registros =  getDatosSolicitudes( solicAsignadas);
			
			
			asunto = "Atenci�n de Solicitudes ";
		   contenido.append( "<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>" +     		 
					"<b>Apreciable Ejecutivo CEDI </b>"+
					"<p>Por este conducto, hago de su conocimiento que el d�a de hoy se le ha(n) asignado la(s) siguiente(s) solicitudes para  el "+
					"Pre-an�lisis correspondiente. ");
			
		   contenido.append(" <br>   </br>");  
			
		   contenido.append("<br> <table width='600' align='left' border='1'  >"+
		                  " <tr> " +
								"<td bgcolor='silver' align='center' width='200' style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;' > <b> Raz�n Social  </td>" +
								"<td bgcolor='silver' align='center' width='200' style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;' > <b> R.F.C.   </td>" +
								"<td bgcolor='silver' align='center' width='200' style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'> <b> Folio de Solicitud  </td>" +
								"<td bgcolor='silver' align='center' width='200' style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'> <b> Fecha de Solicitud  </td>" +
								" </tr> ");
			if(registros.size()>0) {
					
				for (int i = 0; i <registros.size(); i++) {
					HashMap datos = (HashMap)registros.get(i);         
					contenido.append(" <tr> "+
										  " <td  align='left' width='200'  style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>  <b>"+datos.get("RAZON_SOCIAL").toString()+" </td> " +
										  " <td  align='center' width='200' style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>  <b>"+datos.get("RFC_EMPRESARIAL").toString()+" </td> " +
										  " <td  align='center' width='200' style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>  <b>"+datos.get("NO_SOLICITUD").toString()+" </td> " +
										  " <td  align='center' width='200'  style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>  <b>"+datos.get("FECHA_HORA_REG").toString()+" </td> " +
					     				  "</tr>"); 
				}					  
			}					  
							
			contenido.append(" </table> <br>   </br>");  
		   contenido.append(" <br>   </br>");   
			
			 contenido.append("<br> <table width='100' align='left' border='1'  >"+
		   " <tr> <td bgcolor='silver' align='center' width='100'  style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'> <b> N�mero de Solicitudes </td>  </tr> " +					
		  " <tr> <td  width='100' align='center'  style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>  <b>"+registros.size()+" </td> </tr> " +
		  " </table> <br>  </br> "); 
		    				
			contenido.append(" <br>   </br>");   	 		
		   contenido.append(
		         "<BR>Sin Otro Particular, reciba un cordial saludo </BR>"+
		         " <BR>ATENTAMENTE"+ 
				   " <BR>"+nombreDe+
		         " <BR>"+correoDe+" <BR>"+
				   "</span></span>");
			 
		 	correo.setCodificacion("charset=UTF-8");	  			
			correo.enviaCorreoConDatosAdjuntos(correoDe, paraEje,"", asunto, contenido.toString(),null,null);
			
				  
		}catch(Throwable t){    
		   log.error("  enviaCorreoAsignacion  "+ t);   
			throw new AppException("Error al generar y enviar correo", t);		   
		}finally{
		   log.info("El envio del correo de envio exitoso ");
		   log.info("  enviaCorreoAsignacion (S) ");     
		}
	}
	
	
	
	public List getDatosSolicitudes(String solicitudes) {
		log.info("getDatosSolicitudes (E)");
		
		AccesoDB    con = new AccesoDB();
		boolean     commit = true;    
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer   SQL = new StringBuffer();      
		List varBind = new ArrayList();
		HashMap  datos = new HashMap();
		List registros = new ArrayList();
		try{
			con.conexionDB();
			
			SQL.append( "   SELECT s.IC_SOLICITUD as NO_SOLICITUD , " + 
			"  a.CG_RAZON_SOCIAL as RAZON_SOCIAL , " + 
			"  a.CG_RFC as RFC_EMPRESARIAL ,  " + 
			"  to_char(s.DF_ALTA,'DD/MM/YYYY') as FECHA_HORA_REG   " + 							
			"  FROM  cedi_solicitud s, CEDI_IFNB_ASPIRANTE  a , CEDICAT_ESTATUS_SOLICITUD e " + 			
			"  where  s.IC_IFNB_ASPIRANTE = a.IC_IFNB_ASPIRANTE  " + 
			"  and s.IC_ESTATUS_SOLICITUD =   e.IC_ESTATUS_SOLICITUD   " + 
			"  and s.IC_SOLICITUD in ( "+solicitudes+" ) " );
			
			
			log.debug("SQL.toString() "+SQL.toString());
			log.debug("varBind "+varBind);
			
			ps = con.queryPrecompilado(SQL.toString(), varBind);
			rs = ps.executeQuery();
		
			while( rs.next() ){
				datos = new HashMap();
				datos.put("RAZON_SOCIAL", rs.getString("RAZON_SOCIAL")==null?"":rs.getString("RAZON_SOCIAL"));
			   datos.put("NO_SOLICITUD", rs.getString("NO_SOLICITUD")==null?"":rs.getString("NO_SOLICITUD"));
				datos.put("RFC_EMPRESARIAL", rs.getString("RFC_EMPRESARIAL")==null?"":rs.getString("RFC_EMPRESARIAL"));  
			   datos.put("FECHA_HORA_REG", rs.getString("FECHA_HORA_REG")==null?"":rs.getString("FECHA_HORA_REG"));    
				registros.add(datos);   
			}
			
			rs.close();
			ps.close();    
			
		}catch(Throwable e){
			commit = false;
			e.printStackTrace();
			log.error("Error getDatosSolicitudes "+e);
			throw new AppException("Error al getDatosSolicitudes",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getDatosSolicitudes (S)");
		}  
		return registros;
	}

	
	
	public String  generaAcuse() {
		log.info("generaAcuse (E)");
		
		AccesoDB    con = new AccesoDB();
		boolean     commit = true;    
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer   SQL = new StringBuffer(); 
		String  seqNEXTVAL ="0", acuse =""; 
	   int total = 0;
	   StringBuffer   cadena = new StringBuffer(); 
		
		try{
				
			con.conexionDB();
			
		   String fechaHoy = new java.text.SimpleDateFormat("ddMMyy").format(new java.util.Date());        
		   
			
			SQL.append( " select  SEQ_CEDI_ACUSE_ASIGNACION.nextval FROM dual " ); 
			ps = con.queryPrecompilado(SQL.toString());
			rs = ps.executeQuery();
		
			if( rs.next() ){
			   seqNEXTVAL = rs.getString(1)==null?"":rs.getString(1);
			}			
			rs.close();
			ps.close();    
			
			if( seqNEXTVAL.length()<6 ){
				total =   6 - seqNEXTVAL.length(); 		
		      System.out.println(" total =="+total);  
				
				for (int i=0; i < total; i++){
					cadena.append("0");					
				}
		   }
			
			cadena.append(seqNEXTVAL);  
		   acuse =  fechaHoy+"-"+cadena.toString();    
			
			
			
		}catch(Throwable e){
			commit = false;
			e.printStackTrace();
			log.error("Error generaAcuse "+e);
			throw new AppException("Error al generaAcuse",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("generaAcuse (S)");
		}  
		return acuse; 
	}
			 
	
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}



	public void setIc_intermediario(String ic_intermediario) {
		this.ic_intermediario = ic_intermediario;
	}

	public String getIc_intermediario() {
		return ic_intermediario;
	}


	public void setTxtRFC(String txtRFC) {
		this.txtRFC = txtRFC;
	}

	public String getTxtRFC() {
		return txtRFC;
	}
		

	public void setTxtFolioSolicitud(String txtFolioSolicitud) {
		this.txtFolioSolicitud = txtFolioSolicitud;
	}

	public String getTxtFolioSolicitud() {
		return txtFolioSolicitud;
	}
	
	
	public void setTxtFechaSoliIni(String txtFechaSoliIni) {
		this.txtFechaSoliIni = txtFechaSoliIni;
	}

	public String getTxtFechaSoliIni() {
		return txtFechaSoliIni;
	}
	
	public void setTxtFechaSoliFinal(String txtFechaSoliFinal) {
		this.txtFechaSoliFinal = txtFechaSoliFinal;
	}

	public String getTxtFechaSoliFinal() {
		return txtFechaSoliFinal;
	}
	public void setSolicAsignadas(String solicAsignadas) {
		this.solicAsignadas = solicAsignadas;
	}

	public String getSolicAsignadas() {
		return solicAsignadas;
	}
	
	
	
	
	
}//Fin Clase
