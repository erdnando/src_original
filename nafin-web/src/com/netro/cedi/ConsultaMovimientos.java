package com.netro.cedi;

import com.netro.pdf.ComunesPDF;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.usuarios.Usuario;
import java.io.Writer;
import netropology.utilerias.Fecha;
import netropology.utilerias.usuarios.UtilUsr;
import netropology.utilerias.usuarios.Usuario;
import com.netro.xlsx.ComunesXLSX;

import java.io.OutputStreamWriter;

import org.apache.commons.logging.Log;

public class ConsultaMovimientos implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaMovimientos.class);
	private List conditions;
	private String icSolicitud;

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){
		return null;
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){
		return null;
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){
		return null;
	}

	/**
	 * Obtiene la consulta para generar el archivo PDF o CSV sin utilizar paginaci�n
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append("SELECT S.IC_SOLICITUD, S.IC_SOLICITUD_CAMBIO_EST, \n"         );
		qrySentencia.append("       S.IG_USUARIO AS LOGIN, '' AS NOMBRE_USUARIO, \n"       );
		qrySentencia.append("       E.CG_NOMBRE AS ESTATUS, M.CG_NOMBRE AS MOVIMIENTO, \n" );
		qrySentencia.append("       TO_CHAR (S.DF_MOVIMIENTO, \n"                          );
		qrySentencia.append("                'DD/MM/YYYY HH24:MI:SS' \n"                   );
		qrySentencia.append("               ) AS FECHA_HORA_MOVIMIENTO \n"                 );
		qrySentencia.append("  FROM CEDI_SOLICITUD_MOVIMIENTO S, \n"                       );
		qrySentencia.append("       CEDICAT_ESTATUS_SOLICITUD E, \n"                       );
		qrySentencia.append("       CEDICAT_MOVIMIENTO M \n"                               );
		qrySentencia.append(" WHERE S.IC_ESTATUS_SOLICITUD = E.IC_ESTATUS_SOLICITUD \n"    );
		qrySentencia.append("   AND S.IC_MOVIMIENTO = M.IC_MOVIMIENTO \n"                  );
		qrySentencia.append("   AND S.IC_SOLICITUD = ? \n"                                 );
		qrySentencia.append("   ORDER BY S.IC_SOLICITUD_CAMBIO_EST ASC"                    );

		// Se supone que este par�metro es obligatorio, as� que nunca debe ser vac�o.
		if(!this.icSolicitud.equals("")){
			conditions.add(Integer.parseInt(this.icSolicitud));
		}

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentQueryFile(S)");

		return qrySentencia.toString();

	}

	/**
	 * Crea el archivo PDF o CSV seg�n el tipo que se le pasa como par�metro.
	 * Obtiene los datos de la consulta generada en el m�todo getDocumentQueryFile.
	 * @param request
	 * @param rs
	 * @param path
	 * @param tipo
	 * @return nombreArchivo
	 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo){

		log.info(" crearCustomFile(E)");

		String nombreArchivo = "";
		String login         = "";
		String nombre        = "";
		String estatus       = "";
		String movimiento    = "";
		String fechaMov      = "";

		try{

			Usuario usuario     = null;
			UtilUsr utilUsr     = new UtilUsr();
			HttpSession session = request.getSession();

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual   =  fechaActual.substring(0,2);
			String mesActual   =  meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual  =  fechaActual.substring(6,10);
			String horaActual  =  new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			(String)session.getAttribute("sesExterno"),
			(String)session.getAttribute("strNombre"),
			(String)session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),
			(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

			pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
			horaActual, "formas", ComunesPDF.RIGHT);
			pdfDoc.addText(" ", "formas", ComunesPDF.CENTER);
			pdfDoc.addText(" Bit�cora de movimientos por Solicitud: " + this.icSolicitud, "formas", ComunesPDF.CENTER);
			pdfDoc.addText(" ", "formas", ComunesPDF.CENTER);

			pdfDoc.setLTable(5, 100);
			pdfDoc.setLCell("Login",                      "celda01", ComunesPDF.CENTER);
			pdfDoc.setLCell("Nombre Usuario",             "celda01", ComunesPDF.CENTER);
			pdfDoc.setLCell("Estatus",                    "celda01", ComunesPDF.CENTER);
			pdfDoc.setLCell("Movimiento",                 "celda01", ComunesPDF.CENTER);
			pdfDoc.setLCell("Fecha y hora de movimiento", "celda01", ComunesPDF.CENTER);

			while (rs.next()){
				login      = (rs.getString("LOGIN")                 == null) ? "" : rs.getString("LOGIN");
				estatus    = (rs.getString("ESTATUS")               == null) ? "" : rs.getString("ESTATUS");
				movimiento = (rs.getString("MOVIMIENTO")            == null) ? "" : rs.getString("MOVIMIENTO");
				fechaMov   = (rs.getString("FECHA_HORA_MOVIMIENTO") == null) ? "" : rs.getString("FECHA_HORA_MOVIMIENTO");
				if(!login.equals("")){
					usuario = utilUsr.getUsuario(login);
					nombre  = usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno();
				} else{
					nombre = "";
				}
				pdfDoc.setLCell(login,      "formas", ComunesPDF.CENTER);
				pdfDoc.setLCell(nombre,     "formas", ComunesPDF.LEFT  );
				pdfDoc.setLCell(estatus,    "formas", ComunesPDF.CENTER);
				pdfDoc.setLCell(movimiento, "formas", ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaMov,   "formas", ComunesPDF.CENTER);
			}
			pdfDoc.addLTable();
			pdfDoc.endDocument();

		} catch (Throwable e){
			throw new AppException("Error al generar el archivo",e);
		}

		log.info("crearCustomFile(S)");
		return nombreArchivo;
	}

	/**
	 * Crea el archivo PDF utilizando paginaci�n
	 * @param request
	 * @param reg
	 * @param path
	 * @param tipo
	 * @return nombreArchivo
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo){
		return null;
	}

/*****************************************************
 *                GETTERS AND SETTERS                *
 *****************************************************/
	public List getConditions(){
		return conditions;
	}

	public String getIcSolicitud() {
		return icSolicitud;
	}

	public void setIcSolicitud(String icSolicitud) {
		this.icSolicitud = icSolicitud;
	}
}
