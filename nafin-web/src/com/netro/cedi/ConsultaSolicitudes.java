package com.netro.cedi;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import java.util.HashMap;
import java.io.Writer;
import netropology.utilerias.Fecha;
import netropology.utilerias.usuarios.UtilUsr;
import netropology.utilerias.usuarios.Usuario;
import com.netro.xlsx.ComunesXLSX;

import org.apache.commons.logging.Log;

public class ConsultaSolicitudes implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaSolicitudes.class);
	private List conditions;
	private String razonSocial;
	private String ejecutivo;
	private String estatus;
	private String numFolio;
	private String fechaInicio;
	private String fechaFin;
	private String infoIf;
	private String directorioPlantillaXlsx; // Directorio donde se encuentra alamacenada la plantilla Xlsx

	public ConsultaSolicitudes(){}

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		boolean hayRazonSocial = !Comunes.esVacio(this.razonSocial )?true:false;
		boolean hayEjecutivo   = !Comunes.esVacio(this.ejecutivo   )?true:false;
		boolean hayEstatus     = !Comunes.esVacio(this.estatus     )?true:false;
		boolean hayNumFolio    = !Comunes.esVacio(this.numFolio    )?true:false;
		boolean hayFechaInicio = !Comunes.esVacio(this.fechaInicio )?true:false;
		boolean hayFechaFin    = !Comunes.esVacio(this.fechaFin    )?true:false;

		qrySentencia.append("SELECT \n"                                                     );
		qrySentencia.append("    S.IC_SOLICITUD \n"                                         );
		qrySentencia.append("FROM \n"                                                       );
		qrySentencia.append("    CEDI_IFNB_ASPIRANTE       IA, \n"                          );
		qrySentencia.append("    CEDI_SOLICITUD            S, \n"                           );
		qrySentencia.append("    CEDICAT_ESTATUS_SOLICITUD ES \n"                          );
		//qrySentencia.append("    COMCAT_MUNICIPIO          M, \n"                           );
		//qrySentencia.append("    COMCAT_ESTADO             E \n"                            );
		qrySentencia.append("WHERE \n"                                                      );
		qrySentencia.append("    IA.IC_IFNB_ASPIRANTE   = S.IC_IFNB_ASPIRANTE     AND \n"   );
		qrySentencia.append("    S.IC_ESTATUS_SOLICITUD = ES.IC_ESTATUS_SOLICITUD \n"   );
		//qrySentencia.append("    IA.IC_PAIS             = M.IC_PAIS               AND \n"   );
		//qrySentencia.append("    IA.IC_ESTADO           = M.IC_ESTADO             AND \n"   );
		//qrySentencia.append("    IA.IC_MUNICIPIO        = M.IC_MUNICIPIO          AND \n"   );
		//qrySentencia.append("    IA.IC_ESTADO           = E.IC_ESTADO \n"                   );

		// Criterio Razon Social
		if(hayRazonSocial){
			qrySentencia.append("    AND S.IC_IFNB_ASPIRANTE    = ? \n"                      );
		}
		// Criterio Ejecutivo CEDI
		if(hayEjecutivo){
			qrySentencia.append("    AND S.CG_USUARIO_ASIGNADO  = ? \n"                      );
		}
		// Criterio Estatus
		if(hayEstatus){
			qrySentencia.append("    AND S.IC_ESTATUS_SOLICITUD = ? \n"                      );
		}
		// Criterio Folio solicitud
		if(hayNumFolio){
			qrySentencia.append("    AND S.IC_SOLICITUD         = ? \n"                      );
		}
		// Criterio Fecha Solicitud: Inicio y Fin
		if(hayFechaInicio || hayFechaFin ){
			qrySentencia.append("    AND S.IC_SOLICITUD IN ( \n"                             );
			qrySentencia.append("        SELECT \n"                                          );
			qrySentencia.append("            IC_SOLICITUD \n"                                );
			qrySentencia.append("        FROM \n"                                            );
			qrySentencia.append("            CEDI_SOLICITUD_MOVIMIENTO \n"                   );
			qrySentencia.append("        WHERE \n"                                           );
			qrySentencia.append("            IC_ESTATUS_SOLICITUD IN(2, 7) \n"               );
			qrySentencia.append("        GROUP BY \n"                                        );
			qrySentencia.append("            IC_SOLICITUD \n"                                );
			qrySentencia.append("        HAVING \n"                                          );
		}
		// Criterio Fecha Solicitud: Inicio 
		if(hayFechaInicio){
			qrySentencia.append("        MAX(DF_MOVIMIENTO) >= TO_DATE(?,'DD/MM/YYYY') \n"   );
		}
		// Criterio Fecha Solicitud:  Fin
		if(hayFechaInicio && hayFechaFin){
			qrySentencia.append("        AND \n"                                             );
		}
		if(hayFechaFin){
			qrySentencia.append("        MAX(DF_MOVIMIENTO) < TO_DATE(?,'DD/MM/YYYY') + 1 \n");
		}
		if(hayFechaInicio || hayFechaFin ){
			qrySentencia.append("    ) \n"                                                   );
		}
		qrySentencia.append("ORDER BY S.IC_SOLICITUD"                                       );

		// AGREGAR CONDICIONES DEL QUERY
		conditions = new ArrayList();
		// Criterio Razon Social
		if(hayRazonSocial){
			conditions.add(this.razonSocial);
		}
		//Criterio Ejecutivo CEDI
		if(hayEjecutivo){
			conditions.add(this.ejecutivo);
		}
		// Criterio Estatus
		if(hayEstatus){
			conditions.add(this.estatus);
		}
		// Criterio Folio solicitud
		if(hayNumFolio){
			conditions.add(this.numFolio);
		}
		// Criterio Fecha Solicitud: Inicio 
		if(hayFechaInicio){
			conditions.add(this.fechaInicio);
		}
		// Criterio Fecha Solicitud:  Fin
		if(hayFechaFin){
			conditions.add(this.fechaFin);
		}

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append("SELECT \n"                                                           );
		qrySentencia.append("    CG_RAZON_SOCIAL                     AS RAZON_SOCIAL, \n"         );
		qrySentencia.append("    CG_RFC                              AS RFC, \n"                  );
		qrySentencia.append("    IA.CG_CALLE || ' ' || IA.CG_COLONIA AS DOMICILIO, \n"            );
                qrySentencia.append("   (select COMCAT_ESTADO.cd_nombre from COMCAT_ESTADO \n");
                qrySentencia.append("     where COMCAT_ESTADO.IC_ESTADO=IA.IC_ESTADO ) AS ESTADO, \n");
                qrySentencia.append("   (select CD_NOMBRE from COMCAT_MUNICIPIO \n");
                qrySentencia.append("     where COMCAT_MUNICIPIO.IC_PAIS=IA.IC_PAIS \n");
                qrySentencia.append("     and COMCAT_MUNICIPIO.IC_ESTADO=IA.IC_ESTADO \n");
                qrySentencia.append("     and COMCAT_MUNICIPIO.IC_MUNICIPIO=IA.IC_MUNICIPIO) AS MUNICIPIO, \n");
		qrySentencia.append("    CG_CP                               AS CODIGO_POSTAL, \n"        );
		qrySentencia.append("    CG_CONTACTO                         AS CONTACTO, \n"             );
		qrySentencia.append("    CG_EMAIL                            AS EMAIL_CONTACTO, \n"       );
		qrySentencia.append("    CG_TELEFONO                         AS TELEFONO, \n"             );
		qrySentencia.append("    S.IC_SOLICITUD                      AS FOLIO_SOLICITUD, \n"      );
		qrySentencia.append("    S.CG_USUARIO_ASIGNADO               AS LOGIN_EJECUTIVO_CEDI, \n" );
		qrySentencia.append("    ES.CG_NOMBRE                        AS ESTATUS_SOLICITUD, \n"    );
		qrySentencia.append("    S.CS_DICTAMEN_CONFIRMADO            AS DICTAMEN, \n"             );
		qrySentencia.append("    ( \n"                                                            );
		qrySentencia.append("        SELECT \n"                                                   );
		qrySentencia.append("            TO_CHAR( \n"                                             );
		qrySentencia.append("                MAX(DF_MOVIMIENTO), \n"                              );
		qrySentencia.append("                'DD/MM/YYYY HH24:MI:SS' \n"                          );
		qrySentencia.append("            ) \n"                                                    );
		qrySentencia.append("        FROM \n"                                                     );
		qrySentencia.append("            CEDI_SOLICITUD_MOVIMIENTO \n"                            );
		qrySentencia.append("        WHERE \n"                                                    );
		qrySentencia.append("            IC_ESTATUS_SOLICITUD IN(2, 7) AND \n"                    );
		qrySentencia.append("            IC_SOLICITUD  = S.IC_SOLICITUD \n"                       );
		qrySentencia.append("    )                                   AS FECHA_HORA_REGISTRO, \n"  );
		qrySentencia.append("    ( \n"                                                            );
		qrySentencia.append("        SELECT COUNT (*) \n"                                         );
		qrySentencia.append("        FROM \n"                                                     );
		qrySentencia.append("            CEDI_SOLICITUD_MOVIMIENTO \n"                            );
		qrySentencia.append("        WHERE \n"                                                    );
		qrySentencia.append("            IC_ESTATUS_SOLICITUD IN(2, 7) AND \n"                    );
		qrySentencia.append("            IC_SOLICITUD  = S.IC_SOLICITUD \n"                       );
		qrySentencia.append("    )                                   AS EXISTE_CEDULA, \n"        );
		qrySentencia.append("    CASE WHEN S.BI_BALANCE IS NULL THEN 'N' \n"                      );
		qrySentencia.append("    ELSE 'S' END                        AS BALANCE, \n"              );
		qrySentencia.append("    CASE WHEN S.BI_EDO_RESULTADOS IS NULL THEN 'N' \n"               );
		qrySentencia.append("    ELSE 'S' END                        AS EDO_RESULTADOS, \n"       );
		qrySentencia.append("    CASE WHEN S.BI_RFC IS NULL THEN 'N' \n"                          );
		qrySentencia.append("    ELSE 'S' END                        AS BI_RFC, \n"               );
		qrySentencia.append("    (  \n"                                                           );
		qrySentencia.append("     SELECT CH.CG_EXTENSION \n"                                      );
		qrySentencia.append("       FROM CEDI_ARCHIVO_CHECKLIST CH \n"                            );
		qrySentencia.append("      WHERE CH.IC_SOLICITUD = S.IC_SOLICITUD \n"                     );
		qrySentencia.append("            AND IC_DOCUMENTO_CHECKLIST = 1 \n"                       );
		qrySentencia.append("    )                                   AS MANUAL_CREDITO,     \n"   );
		qrySentencia.append("    (  \n"                                                           );
		qrySentencia.append("     SELECT CH.CG_EXTENSION \n"                                      );
		qrySentencia.append("       FROM CEDI_ARCHIVO_CHECKLIST CH \n"                            );
		qrySentencia.append("      WHERE CH.IC_SOLICITUD = S.IC_SOLICITUD \n"                     );
		qrySentencia.append("            AND IC_DOCUMENTO_CHECKLIST = 2 \n"                       );
		qrySentencia.append("    )                                   AS PRESEN_CORPORATIVA, \n"   );
		qrySentencia.append("    (  \n"                                                           );
		qrySentencia.append("     SELECT CH.CG_EXTENSION \n"                                      );
		qrySentencia.append("       FROM CEDI_ARCHIVO_CHECKLIST CH \n"                            );
		qrySentencia.append("      WHERE CH.IC_SOLICITUD = S.IC_SOLICITUD \n"                     );
		qrySentencia.append("            AND IC_DOCUMENTO_CHECKLIST = 3 \n"                       );
		qrySentencia.append("    )                                   AS PASIVOS_BANCARIOS, \n"    );
		qrySentencia.append("    (  \n"                                                           );
		qrySentencia.append("     SELECT CH.CG_EXTENSION \n"                                      );
		qrySentencia.append("       FROM CEDI_ARCHIVO_CHECKLIST CH \n"                            );
		qrySentencia.append("      WHERE CH.IC_SOLICITUD = S.IC_SOLICITUD \n"                     );
		qrySentencia.append("            AND IC_DOCUMENTO_CHECKLIST = 4 \n"                       );
		qrySentencia.append("    )                                   AS CARTERA_VENCIDA, \n"      );
		qrySentencia.append("    (  \n"                                                           );
		qrySentencia.append("     SELECT CH.CG_EXTENSION \n"                                      );
		qrySentencia.append("       FROM CEDI_ARCHIVO_CHECKLIST CH \n"                            );
		qrySentencia.append("      WHERE CH.IC_SOLICITUD = S.IC_SOLICITUD \n"                     );
		qrySentencia.append("            AND IC_DOCUMENTO_CHECKLIST = 5 \n"                       );
		qrySentencia.append("    )                                   AS PORTAFOLIO_CARTERA, \n"   );
		qrySentencia.append("     ''                                 AS EJECUTIVO_CEDI \n"        );
		qrySentencia.append("FROM \n"                                                             );
		qrySentencia.append("    CEDI_IFNB_ASPIRANTE       IA, \n"                                );
		qrySentencia.append("    CEDI_SOLICITUD            S, \n"                                 );
		qrySentencia.append("    CEDICAT_ESTATUS_SOLICITUD ES \n"                                );
		//qrySentencia.append("    COMCAT_MUNICIPIO          M, \n"                                 );
		//qrySentencia.append("    COMCAT_ESTADO             E \n"                                  );
		qrySentencia.append("WHERE \n"                                                            );
		qrySentencia.append("    IA.IC_IFNB_ASPIRANTE   = S.IC_IFNB_ASPIRANTE     AND \n"         );
		qrySentencia.append("    S.IC_ESTATUS_SOLICITUD = ES.IC_ESTATUS_SOLICITUD \n"         );
		//qrySentencia.append("    IA.IC_PAIS             = M.IC_PAIS               AND \n"         );
		//qrySentencia.append("    IA.IC_ESTADO           = M.IC_ESTADO             AND \n"         );
		//qrySentencia.append("    IA.IC_MUNICIPIO        = M.IC_MUNICIPIO          AND \n"         );
		//qrySentencia.append("    IA.IC_ESTADO           = E.IC_ESTADO                 \n"         );

		qrySentencia.append("    AND S.IC_SOLICITUD IN ( "                                        );
		for (int i = 0; i < ids.size(); i++) { 
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				qrySentencia.append(",");
			}
			qrySentencia.append(" ? " );
			conditions.add(new Long(lItem.get(0).toString()));
		}
		qrySentencia.append(") \n"                                                                );

		qrySentencia.append("ORDER BY S.IC_SOLICITUD"                                             );

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		boolean hayRazonSocial = !Comunes.esVacio(this.razonSocial )?true:false;
		boolean hayEjecutivo   = !Comunes.esVacio(this.ejecutivo   )?true:false;
		boolean hayEstatus     = !Comunes.esVacio(this.estatus     )?true:false;
		boolean hayNumFolio    = !Comunes.esVacio(this.numFolio    )?true:false;
		boolean hayFechaInicio = !Comunes.esVacio(this.fechaInicio )?true:false;
		boolean hayFechaFin    = !Comunes.esVacio(this.fechaFin    )?true:false;

		qrySentencia.append("SELECT \n"                                                  );
		qrySentencia.append("    COUNT(S.IC_SOLICITUD) \n"                               );
		qrySentencia.append("FROM \n"                                                    );
		qrySentencia.append("    CEDI_IFNB_ASPIRANTE       IA, \n"                       );
		qrySentencia.append("    CEDI_SOLICITUD            S, \n"                        );
		qrySentencia.append("    CEDICAT_ESTATUS_SOLICITUD ES, \n"                       );
		qrySentencia.append("    COMCAT_MUNICIPIO          M, \n"                        );
		qrySentencia.append("    COMCAT_ESTADO             E \n"                         );
		qrySentencia.append("WHERE \n"                                                   );
		qrySentencia.append("    IA.IC_IFNB_ASPIRANTE   = S.IC_IFNB_ASPIRANTE     AND \n");
		qrySentencia.append("    S.IC_ESTATUS_SOLICITUD = ES.IC_ESTATUS_SOLICITUD AND \n");
		qrySentencia.append("    IA.IC_PAIS             = M.IC_PAIS               AND \n");
		qrySentencia.append("    IA.IC_ESTADO           = M.IC_ESTADO             AND \n");
		qrySentencia.append("    IA.IC_MUNICIPIO        = M.IC_MUNICIPIO          AND \n");
		qrySentencia.append("    IA.IC_ESTADO           = E.IC_ESTADO \n"                );

		// Criterio Razon Social
		if(hayRazonSocial){
			qrySentencia.append("    AND S.IC_IFNB_ASPIRANTE    = ? \n"                   );
		}
		// Criterio Ejecutivo CEDI
		if(hayEjecutivo){
			qrySentencia.append("    AND S.CG_USUARIO_ASIGNADO  = ? \n"                      );
		}
		// Criterio Estatus
		if(hayEstatus){
			qrySentencia.append("    AND S.IC_ESTATUS_SOLICITUD = ? \n"                      );
		}
		// Criterio Folio solicitud
		if(hayNumFolio){
			qrySentencia.append("    AND S.IC_SOLICITUD         = ? \n"                      );
		}
		// Criterio Fecha Solicitud: Inicio y Fin
		if(hayFechaInicio || hayFechaFin ){
			qrySentencia.append("    AND S.IC_SOLICITUD IN ( \n"                             );
			qrySentencia.append("        SELECT \n"                                          );
			qrySentencia.append("            IC_SOLICITUD \n"                                );
			qrySentencia.append("        FROM \n"                                            );
			qrySentencia.append("            CEDI_SOLICITUD_MOVIMIENTO \n"                   );
			qrySentencia.append("        WHERE \n"                                           );
			qrySentencia.append("            IC_ESTATUS_SOLICITUD IN(2, 7) \n"               );
			qrySentencia.append("        GROUP BY \n"                                        );
			qrySentencia.append("            IC_SOLICITUD \n"                                );
			qrySentencia.append("        HAVING \n"                                          );
		}
		// Criterio Fecha Solicitud: Inicio 
		if(hayFechaInicio){
			qrySentencia.append("        MAX(DF_MOVIMIENTO) >= TO_DATE(?,'DD/MM/YYYY') \n"   );
		}
		// Criterio Fecha Solicitud:  Fin
		if(hayFechaInicio && hayFechaFin){
			qrySentencia.append("        AND \n"                                             );
		}
		if(hayFechaFin){
			qrySentencia.append("        MAX(DF_MOVIMIENTO) < TO_DATE(?,'DD/MM/YYYY') + 1 \n");
		}
		if(hayFechaInicio || hayFechaFin ){
			qrySentencia.append("    )"                                                      );
		}

		// AGREGAR CONDICIONES DEL QUERY
		conditions = new ArrayList();
		// Criterio Razon Social
		if(hayRazonSocial){
			conditions.add(this.razonSocial);
		}
		//Criterio Ejecutivo CEDI
		if(hayEjecutivo){
			conditions.add(this.ejecutivo);
		}
		// Criterio Estatus
		if(hayEstatus){
			conditions.add(this.estatus);
		}
		// Criterio Folio solicitud
		if(hayNumFolio){
			conditions.add(this.numFolio);
		}
		// Criterio Fecha Solicitud: Inicio 
		if(hayFechaInicio){
			conditions.add(this.fechaInicio);
		}
		// Criterio Fecha Solicitud:  Fin
		if(hayFechaFin){
			conditions.add(this.fechaFin);
		}

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}

	/**
	 * Obtiene la consulta para generar el archivo PDF o CSV sin utilizar paginaci�n
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		boolean hayRazonSocial = !Comunes.esVacio(this.razonSocial )?true:false;
		boolean hayEjecutivo   = !Comunes.esVacio(this.ejecutivo   )?true:false;
		boolean hayEstatus     = !Comunes.esVacio(this.estatus     )?true:false;
		boolean hayNumFolio    = !Comunes.esVacio(this.numFolio    )?true:false;
		boolean hayFechaInicio = !Comunes.esVacio(this.fechaInicio )?true:false;
		boolean hayFechaFin    = !Comunes.esVacio(this.fechaFin    )?true:false;
		boolean hayInfoIf      = "true".equals   (this.infoIf      )?true:false;

		qrySentencia.append("SELECT \n"                                                           );
		//qrySentencia.append("    IC_IFNB_ASPIRANTE                   AS IC_IFNB, \n"            );
		qrySentencia.append("    CG_RAZON_SOCIAL                     AS RAZON_SOCIAL, \n"         );
		qrySentencia.append("    CG_RFC                              AS RFC, \n"                  );
		qrySentencia.append("    IA.CG_CALLE                         AS CALLE, \n"                );
		qrySentencia.append("    IA.CG_COLONIA                       AS COLONIA, \n"              );
		qrySentencia.append("    E.CD_NOMBRE                         AS ESTADO, \n"               );
		qrySentencia.append("    M.CD_NOMBRE                         AS MUNICIPIO, \n"            );
		qrySentencia.append("    CG_CP                               AS CODIGO_POSTAL, \n"        );
		qrySentencia.append("    CG_CONTACTO                         AS CONTACTO, \n"             );
		qrySentencia.append("    CG_EMAIL                            AS EMAIL_CONTACTO, \n"       );
		qrySentencia.append("    CG_TELEFONO                         AS TELEFONO, \n"             );
		qrySentencia.append("    S.IC_SOLICITUD                      AS FOLIO_SOLICITUD, \n"      );
		qrySentencia.append("    S.CG_USUARIO_ASIGNADO               AS LOGIN_EJECUTIVO_CEDI, \n" );
		qrySentencia.append("    ES.CG_NOMBRE                        AS ESTATUS_SOLICITUD, \n"    );
		qrySentencia.append("    S.CS_DICTAMEN_CONFIRMADO            AS DICTAMEN, \n"             );
		qrySentencia.append("    ( \n"                                                            );
		qrySentencia.append("        SELECT \n"                                                   );
		qrySentencia.append("            TO_CHAR( \n"                                             );
		qrySentencia.append("                MAX(DF_MOVIMIENTO), \n"                              );
		qrySentencia.append("                'DD/MM/YYYY HH24:MI:SS' \n"                          );
		qrySentencia.append("            ) \n"                                                    );
		qrySentencia.append("        FROM \n"                                                     );
		qrySentencia.append("            CEDI_SOLICITUD_MOVIMIENTO \n"                            );
		qrySentencia.append("        WHERE \n"                                                    );
		qrySentencia.append("            IC_ESTATUS_SOLICITUD IN(2, 7) AND \n"                    );
		qrySentencia.append("            IC_SOLICITUD  = S.IC_SOLICITUD \n"                       );
		qrySentencia.append("    ) AS FECHA_HORA_REGISTRO, \n"                                    );
		qrySentencia.append("     ''                                 AS EJECUTIVO_CEDI \n"        );
		qrySentencia.append("FROM \n"                                                             );
		qrySentencia.append("    CEDI_IFNB_ASPIRANTE       IA, \n"                                );
		qrySentencia.append("    CEDI_SOLICITUD            S, \n"                                 );
		qrySentencia.append("    CEDICAT_ESTATUS_SOLICITUD ES, \n"                                );
		qrySentencia.append("    COMCAT_MUNICIPIO          M, \n"                                 );
		qrySentencia.append("    COMCAT_ESTADO             E \n"                                  );
		qrySentencia.append("WHERE \n"                                                            );
		qrySentencia.append("    IA.IC_IFNB_ASPIRANTE   = S.IC_IFNB_ASPIRANTE     AND \n"         );
		qrySentencia.append("    S.IC_ESTATUS_SOLICITUD = ES.IC_ESTATUS_SOLICITUD AND \n"         );
		qrySentencia.append("    IA.IC_PAIS             = M.IC_PAIS               AND \n"         );
		qrySentencia.append("    IA.IC_ESTADO           = M.IC_ESTADO             AND \n"         );
		qrySentencia.append("    IA.IC_MUNICIPIO        = M.IC_MUNICIPIO          AND \n"         );
		qrySentencia.append("    IA.IC_ESTADO           = E.IC_ESTADO                 \n"         );

		// Criterio Razon Social
		if(hayRazonSocial){
			qrySentencia.append("    AND S.IC_IFNB_ASPIRANTE    = ? \n"                            );
		}
		// Criterio Ejecutivo CEDI
		if(hayEjecutivo){
			qrySentencia.append("    AND S.CG_USUARIO_ASIGNADO  = ? \n"                            );
		}
		// Criterio Estatus
		if(hayEstatus){
			qrySentencia.append("    AND S.IC_ESTATUS_SOLICITUD = ? \n"                            );
		}
		// Criterio Folio solicitud
		if(hayNumFolio){
			qrySentencia.append("    AND S.IC_SOLICITUD         = ? \n"                            );
		}
		// Criterio Fecha Solicitud: Inicio y Fin
		if(hayFechaInicio || hayFechaFin ){
			qrySentencia.append("    AND S.IC_SOLICITUD IN ( \n"                                   );
			qrySentencia.append("        SELECT \n"                                                );
			qrySentencia.append("            IC_SOLICITUD \n"                                      );
			qrySentencia.append("        FROM \n"                                                  );
			qrySentencia.append("            CEDI_SOLICITUD_MOVIMIENTO \n"                         );
			qrySentencia.append("        WHERE \n"                                                 );
			qrySentencia.append("            IC_ESTATUS_SOLICITUD IN(2, 7) \n"                     );
			qrySentencia.append("        GROUP BY \n"                                              );
			qrySentencia.append("            IC_SOLICITUD \n"                                      );
			qrySentencia.append("        HAVING \n"                                                );
		}
		// Criterio Fecha Solicitud: Inicio 
		if(hayFechaInicio){
			qrySentencia.append("        MAX(DF_MOVIMIENTO) >= TO_DATE(?,'DD/MM/YYYY') \n"         );
		}
		// Criterio Fecha Solicitud:  Fin
		if(hayFechaInicio && hayFechaFin){
			qrySentencia.append("        AND \n"                                                   );
		}
		if(hayFechaFin){
			qrySentencia.append("        MAX(DF_MOVIMIENTO) < TO_DATE(?,'DD/MM/YYYY') + 1 \n"      );
		}
		if(hayFechaInicio || hayFechaFin ){
			qrySentencia.append("    ) \n"                                                         );
		}
		qrySentencia.append("ORDER BY S.IC_SOLICITUD"                                             );

		// AGREGAR CONDICIONES DEL QUERY
		conditions = new ArrayList();
		// Criterio Razon Social
		if(hayRazonSocial){
			conditions.add(this.razonSocial);
		}
		// Criterio Ejecutivo CEDI
		if(hayEjecutivo){
			conditions.add(this.ejecutivo);
		}
		// Criterio Estatus
		if(hayEstatus){
			conditions.add(this.estatus);
		}
		// Criterio Folio solicitud
		if(hayNumFolio){
			conditions.add(this.numFolio);
		}
		// Criterio Fecha Solicitud: Inicio 
		if(hayFechaInicio){
			conditions.add(this.fechaInicio);
		}
		// Criterio Fecha Solicitud:  Fin
		if(hayFechaFin){
			conditions.add(this.fechaFin);
		}

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentQueryFile(S)");

		return qrySentencia.toString();

	}

	/**
	 * Crea el archivo PDF o CSV seg�n el tipo que se le pasa como par�metro.
	 * Obtiene los datos de la consulta generada en el m�todo getDocumentQueryFile.
	 * @param request
	 * @param rs
	 * @param path
	 * @param tipo
	 * @return nombreArchivo
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String directorioTemporal, String tipo){

		log.debug("crearCustomFile(E)");

		String      nombreArchivo = "";
		ComunesXLSX documentoXLSX = null;
		Writer      writer        = null;

		try{

			if(!"XLSX".equals(tipo)){
				throw new UnsupportedOperationException("Formato de Reporte no soportado.");
			}

			boolean hayInfoIf = "true".equals(this.infoIf)?true:false;

			// Estilos en la plantilla xlsx
			HashMap estilos = new HashMap();
			estilos.put("PERCENT",       "1");
			estilos.put("COEFF",         "2");
			estilos.put("CURRENCY",      "3");
			estilos.put("DATE",          "4");
			estilos.put("HEADER",        "5");
			estilos.put("CSTRING",       "6");
			estilos.put("TITLE",         "7");
			estilos.put("SUBTITLE",      "8");
			estilos.put("RIGHTCSTRING",  "9");
			estilos.put("CENTERCSTRING","10");

			// Crear archivo XLSX
			documentoXLSX = new ComunesXLSX(directorioTemporal, estilos);

			// Crear Detalle Comisiones
			writer = documentoXLSX.creaHoja();
			if(hayInfoIf){
				documentoXLSX.setLongitudesColumna(new String[]{
					"63.42578125",
					"20.7109375",
					"65.42578125",
					"63.42578125",
					"15.7109375",
					"63.42578125",
					"63.42578125",
					"23.7109375",
					"14.7109375",
					"20.4257812",
					"63.42578125",
					"42.49601562"
				});
			} else{
				documentoXLSX.setLongitudesColumna(new String[]{
					"63.42578125",
					"20.7109375",
					"14.7109375",
					"20.4257812",
					"63.42578125",
					"42.49601562"
				});
			}

			final int   NUMERO_COLUMNAS   = hayInfoIf?12:6;
			short       indiceRenglon     = 0;
			short       indiceCelda       = 0;

			// Definir el Encabezado PROGRAMA
			documentoXLSX.agregaRenglon(indiceRenglon++); 
			indiceCelda    = 0;
			documentoXLSX.agregaCelda(indiceCelda++,"Fecha Reporte: " + Fecha.getFechaActual()); // Debug info: evaluar crear estilo de negritas para este caso
			documentoXLSX.finalizaRenglon();

			// Definir cabecera de la tabla de datos
			documentoXLSX.agregaRenglon(indiceRenglon++); 
			indiceCelda    = 0;
			documentoXLSX.agregaCelda(    indiceCelda++,"Raz�n Social",                                "HEADER" );
			documentoXLSX.agregaCelda(    indiceCelda++,"R.F.C.\nEmpresarial",                         "HEADER" );
			if(hayInfoIf){
				documentoXLSX.agregaCelda( indiceCelda++,"Domicilio",                                   "HEADER" );
				documentoXLSX.agregaCelda( indiceCelda++,"Estado y Delegaci�n o Municipio empresarial", "HEADER" );
				documentoXLSX.agregaCelda( indiceCelda++,"C�digo Postal\nEmpresarial",                  "HEADER" );
				documentoXLSX.agregaCelda( indiceCelda++,"Contacto",                                    "HEADER" );
				documentoXLSX.agregaCelda( indiceCelda++,"Correo Electr�nico\nEmpresarial",             "HEADER" );
				documentoXLSX.agregaCelda( indiceCelda++,"Tel�fono\nEmpresarial",                       "HEADER" );
			}
			documentoXLSX.agregaCelda(    indiceCelda++,"Folio\nSolicitud",                            "HEADER" );
			documentoXLSX.agregaCelda(    indiceCelda++,"Fecha y Hora\nde Registro",                   "HEADER" );
			documentoXLSX.agregaCelda(    indiceCelda++,"Ejecutivo CEDI\nResponsable",                 "HEADER" );
			documentoXLSX.agregaCelda(    indiceCelda++,"Estatus Solicitud",                           "HEADER" );
			documentoXLSX.finalizaRenglon();

			// Extraer datos
			boolean        hayDatos                = false;
			int            ctaRegistros            = 0;

			UtilUsr        utilUsr                 = new UtilUsr();
			Usuario        usuario                 = null;
			HashMap        cacheNombresEjecutivo   = new HashMap();
			String         nombreEjecutivo         = null;

			String         razonSocial             = null;
			String         rfc                     = null;
			String         calle                   = null;
			String         colonia                 = null;
			String         estado                  = null;
			String         municipio               = null;
			String         codigoPostal            = null;
			String         contacto                = null;
			String         emailContacto           = null;
			String         telefono                = null;
			String         folioSolicitud          = null;
			String         fechaHoraRegistro       = null;
			String         loginEjecutivoCedi      = null;
			String         estatusSolicitud        = null;
			String         nombreEjecutivoCedi     = null;
			StringBuffer   domicilio               = new StringBuffer(512);
			StringBuffer   estadoMunicipio         = new StringBuffer(512);

			while( rs.next()){

				razonSocial          = (rs.getString("RAZON_SOCIAL")           == null)?"":rs.getString("RAZON_SOCIAL");
				rfc                  = (rs.getString("RFC")                    == null)?"":rs.getString("RFC");
				if(hayInfoIf){
					calle             = (rs.getString("CALLE")                  == null)?"":rs.getString("CALLE");
					colonia           = (rs.getString("COLONIA")                == null)?"":rs.getString("COLONIA");
					estado            = (rs.getString("ESTADO")                 == null)?"":rs.getString("ESTADO");
					municipio         = (rs.getString("MUNICIPIO")              == null)?"":rs.getString("MUNICIPIO");
					codigoPostal      = (rs.getString("CODIGO_POSTAL")          == null)?"":rs.getString("CODIGO_POSTAL");
					contacto          = (rs.getString("CONTACTO")               == null)?"":rs.getString("CONTACTO");
					emailContacto     = (rs.getString("EMAIL_CONTACTO")         == null)?"":rs.getString("EMAIL_CONTACTO");
					telefono          = (rs.getString("TELEFONO")               == null)?"":rs.getString("TELEFONO");
				}
				folioSolicitud       = (rs.getString("FOLIO_SOLICITUD")        == null)?"":rs.getString("FOLIO_SOLICITUD");
				fechaHoraRegistro    = (rs.getString("FECHA_HORA_REGISTRO")    == null)?"":rs.getString("FECHA_HORA_REGISTRO");
				loginEjecutivoCedi   = (rs.getString("LOGIN_EJECUTIVO_CEDI")   == null)?"":rs.getString("LOGIN_EJECUTIVO_CEDI").trim();
				nombreEjecutivoCedi  = "";
				estatusSolicitud     = (rs.getString("ESTATUS_SOLICITUD")      == null)?"":rs.getString("ESTATUS_SOLICITUD");

				// En caso no se tenga nombre del ejecutivo en cach�, agregarlo
				if( !Comunes.esVacio(loginEjecutivoCedi) && !cacheNombresEjecutivo.containsKey(loginEjecutivoCedi) ){

					nombreEjecutivo = null;
					try {
						usuario = utilUsr.getUsuario(loginEjecutivoCedi);
						if( !Comunes.esVacio(usuario.getLogin()) ){
							nombreEjecutivo = usuario.getNombreCompleto();
						}
					} catch(Exception e){
						log.info("crearCustomFile.loginEjecutivoCedi:NO_ENCONTRADO = <" + loginEjecutivoCedi + ">",e);
						nombreEjecutivo    = null;
					}

					if( Comunes.esVacio(nombreEjecutivo) ){
						nombreEjecutivo = "No disponible.";
					}

					cacheNombresEjecutivo.put(loginEjecutivoCedi,nombreEjecutivo);

				}
				// Obtener nombre del ejecutivo.
				if( cacheNombresEjecutivo.containsKey(loginEjecutivoCedi) ){
					nombreEjecutivoCedi = (String) cacheNombresEjecutivo.get(loginEjecutivoCedi);
				}

				if(hayInfoIf){

					// Domicilio
					domicilio.setLength(0);
					domicilio.append(calle);
					if(!Comunes.esVacio(calle) && !Comunes.esVacio(colonia)){
						domicilio.append(", ");
					}
					domicilio.append(colonia);

					// Estado y Municipio
					estadoMunicipio.setLength(0);
					estadoMunicipio.append(estado);
					if(!Comunes.esVacio(estado)&&!Comunes.esVacio(municipio)){
						estadoMunicipio.append(", ");
					}
					estadoMunicipio.append(municipio);

				}

				// Llenar tabla
				documentoXLSX.agregaRenglon(indiceRenglon++); 
				indiceCelda             = 0;
				documentoXLSX.agregaCelda(indiceCelda++,razonSocial,           "CSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,rfc,                   "CENTERCSTRING");
				if(hayInfoIf){
					documentoXLSX.agregaCelda(indiceCelda++,domicilio.toString(),        "CSTRING");
					documentoXLSX.agregaCelda(indiceCelda++,estadoMunicipio.toString(),  "CSTRING");
					documentoXLSX.agregaCelda(indiceCelda++,codigoPostal,                "CENTERCSTRING");
					documentoXLSX.agregaCelda(indiceCelda++,contacto,                    "CSTRING");
					documentoXLSX.agregaCelda(indiceCelda++,emailContacto,               "CENTERCSTRING");
					documentoXLSX.agregaCelda(indiceCelda++,telefono,                    "CENTERCSTRING");
				}
				documentoXLSX.agregaCelda(indiceCelda++,folioSolicitud,        "CENTERCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,fechaHoraRegistro,     "CENTERCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,nombreEjecutivoCedi,   "CSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,estatusSolicitud,      "CSTRING");
				documentoXLSX.finalizaRenglon();

				if( ctaRegistros % 250 == 0 ){
					documentoXLSX.flush();
					ctaRegistros = 1;
				}

				ctaRegistros++;
				hayDatos = true;

			}

			// No hay registros
			if(!hayDatos){

				documentoXLSX.agregaRenglon(indiceRenglon++); 
				indiceCelda    = 0;
				documentoXLSX.agregaCelda(indiceCelda++,"No se encontr� ning�n registro.","HEADER",NUMERO_COLUMNAS);
				documentoXLSX.finalizaRenglon();

			}

			// Finalizar Hoja
			documentoXLSX.finalizaHoja(); 

			// Terminar Documento: Crear en directorioTemporal archivo XLSX con la hoja 
			// generada usando una plantilla: 39ReporteGeneral.template.xlsx 
			nombreArchivo = documentoXLSX.finalizaDocumento( this.directorioPlantillaXlsx + "39ReporteGeneral.template.xlsx" );

		} catch(OutOfMemoryError om){ // Se acabo la memoria

			log.error(
				"crearCustomFile(OutOfMemoryError): Se acab� la memoria.\n" +
				"crearCustomFile.request               = <" + request                            + ">\n"  +
				"crearCustomFile.rs                    = <" + rs                                 + ">\n"  +
				"crearCustomFile.directorioTemporal    = <" + directorioTemporal                 + ">\n"  +
				"crearCustomFile.tipo                  = <" + tipo                               + ">\n"  +
				"crearCustomFile.Free Memory           = <" + Runtime.getRuntime().freeMemory()  + ">",
				om
			);

			throw new AppException("Se acab� la memoria.");

		} catch(Exception e){

			log.error(
				"crearCustomFile(Exception): Error en la generacion del Archivo.\n" +
				"crearCustomFile.request            = <" + request             + ">\n"  +
				"crearCustomFile.rs                 = <" + rs                  + ">\n"  +
				"crearCustomFile.directorioTemporal = <" + directorioTemporal  + ">\n"  +
				"crearCustomFile.tipo               = <" + tipo                + ">",
				e
			);
			throw new AppException(e);

		} finally{

			if( writer != null ){ try { writer.close(); }catch(Exception e){} }

			log.debug("crearCustomFile(S)");

		}

		return nombreArchivo;

	}

	/**
	 * Crea el archivo PDF utilizando paginaci�n
	 * @param request
	 * @param reg
	 * @param path
	 * @param tipo
	 * @return nombreArchivo
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		String nombreArchivo = "";
		HttpSession session = request.getSession();

		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();

		try{

		} catch(Throwable e){
			throw new AppException("Error al generar el archivo ", e);
		} finally{
			try{

			} catch(Exception e){}

		}
		return nombreArchivo;
	}

/*****************************************************
 *                GETTERS AND SETTERS                *
 *****************************************************/
	public List getConditions(){
		return conditions;
	}

	public String getRazonSocial(){
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial){
		this.razonSocial = razonSocial;
	}

	public String getEjecutivo(){
		return ejecutivo;
	}

	public void setEjecutivo(String ejecutivo){
		this.ejecutivo = ejecutivo;
	}

	public String getEstatus(){
		return estatus;
	}

	public void setEstatus(String estatus){
		this.estatus = estatus;
	}

	public String getNumFolio(){
		return numFolio;
	}

	public void setNumFolio(String numFolio){
		this.numFolio = numFolio;
	}

	public String getFechaInicio(){
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio){
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin(){
		return fechaFin;
	}

	public void setFechaFin(String fechaFin){
		this.fechaFin = fechaFin;
	}

	public String getInfoIf(){
		return infoIf;
	}

	public void setInfoIf(String infoIf){
		this.infoIf = infoIf;
	}

	public String getDirectorioPlantillaXlsx(){
		return directorioPlantillaXlsx;
	}

	public void setDirectorioPlantillaXlsx(String directorioPlantillaXlsx){
		this.directorioPlantillaXlsx = directorioPlantillaXlsx;
	}
	
}
