package com.netro.cedi;

import com.netro.cadenas.ConIntermediarios;
import com.netro.pdf.ComunesPDF;

import com.sun.org.apache.xpath.internal.operations.And;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Blob;

import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;


public class ConsSegPreAnalisis  implements IQueryGeneratorRegExtJS    {
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsSegPreAnalisis.class);
	
	private String    paginaOffset;
	private String    paginaNo;
	private List      conditions;
	StringBuffer qrySentencia   = new StringBuffer();
	
	private String rfc_IFNB;
	
	
	public ConsSegPreAnalisis() {}

	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
		
		
	public String getDocumentSummaryQueryForIds(List ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}
	
		
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
		
	
		
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
				             
	   qrySentencia.append(" SELECT  " +
								  "  s.IC_SOLICITUD as NO_SOLICITUD  ,  "+
								  "d.IC_DOCUMENTO_CHECKLIST as  IC_CHECKLIST,  "+
								  " CG_NOMBRE as NOMBRE_DOCTO ,  "+
								  
								  " LENGTH ( BI_ARCHIVO) as LONGITUD ,  "+
								  " CG_EXTENSION  as EXTENSION ,  "+
								  
									" LENGTH ( BI_ARCHIVO_PRE) as LONGITUD_PRE ,  "+
									" CG_EXTENSION_PRE  as EXTENSION_PRE ,  "+
	   
		
								  "  TO_CHAR (a.DF_ULTIMO_ENVIO, 'dd/mm/yyyy, hh:mm:ss am') AS FECHA_ULTIMO_ENVIO,  "+
								  "  a. CS_VISTA_PREVIA AS VISTA_PREVIA , "+
								  "  d.CS_FIJO  as FIJO   "+
								  
								  " FROM cedi_documento_checklist d,  "+
								  " cedi_archivo_checklist a , "+
								  " cedi_solicitud  s ,  "+
								  " CEDI_IFNB_ASPIRANTE  i  "+
								  
								  " WHERE d.ic_documento_checklist = a.ic_documento_checklist "+
								  " and s.IC_SOLICITUD = a.IC_SOLICITUD  "+
								  " and s.IC_IFNB_ASPIRANTE = i.IC_IFNB_ASPIRANTE  "+
								  " and d.cs_activo =  ?  "+
								  " and  i.CG_RFC =  ?  " +
								  " and s.IC_ESTATUS_SOLICITUD in (4, 5)   " +
								  " ORDER BY  d.IC_DOCUMENTO_CHECKLIST ");
						
	   conditions.add("S"); 
	   conditions.add(rfc_IFNB); 	  
								  
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}
		
		
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		log.debug("crearCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		StringBuffer contenidoArchivo = new StringBuffer("");
		
					
		try {
		
		
		}catch(Exception e) { 
				log.error("Error en la generacion del Archivo CSV: " + e);
		}	

			return nombreArchivo;		 				 				 		
		}
				 
					
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		}catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			
			} catch(Exception e) {}
		  
			}
			return nombreArchivo;
		}
 
 
	
	public HashMap  getDatosIFNB(String rfc_IFNB, String solicitud ) {
		log.info("getDatosIFNB (E)");
		
		AccesoDB    con = new AccesoDB();
		boolean     commit = true;    
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer   SQL = new StringBuffer(); 		
	   HashMap datos  = new  HashMap();
		
	   StringBuffer   tipoDestino = new StringBuffer();     
		try{
				
			con.conexionDB();
			
			
		   SQL = new StringBuffer();    
		   SQL.append( " select CG_OPCION_RESPUESTA  from  "+
							" CEDI_RESPUESTA_IFNB  r, "+
							" CEDI_RESPUESTA_PARAMETRIZACION  p "+
							" WHERE r.IC_SOLICITUD = "+solicitud +
							" AND r.IC_RESPUESTA_PARAMETRIZACION =  p.IC_RESPUESTA_PARAMETRIZACION " +
							" and p.IC_PREGUNTA = 4 "+
							" order by p.IC_RESPUESTA_PARAMETRIZACION asc ");
			
		   log.debug("SQL.toString() "+SQL.toString());
			
		   ps =null;
			ps = con.queryPrecompilado(SQL.toString());
		   rs = ps.executeQuery();
		   int i =0;
		   while (rs.next()) {  
				
			   String opcion = rs.getString("CG_OPCION_RESPUESTA")==null?"":rs.getString("CG_OPCION_RESPUESTA");
				if(opcion.equals("Otro")){
				 tipoDestino.append(" y "+ opcion);
				}else  {					
					tipoDestino.append( opcion +",");
					
				}
			   i++;
			}
		   rs.close();
		   ps.close();  
			
			String  destino = tipoDestino.toString(); 
																
		   if(!destino.equals("")){
				int tamanio2 = destino.length();
				String valor =  destino.substring(tamanio2-1, tamanio2);
				if(valor.equals(",")){
					destino =destino.substring(0,tamanio2-1);
				}
		   }





		   
			SQL = new StringBuffer();    
		   SQL.append( " SELECT distinct  i.cg_rfc , s.ic_solicitud AS no_solicitud, i.cg_razon_social AS razon_social, " + 
					" i.cg_rfc AS rfc,  " + 
		         " TO_CHAR (mv.DF_MOVIMIENTO, 'dd/mm/yyyy') AS fecha_registro " + 		         
					"  FROM cedi_solicitud s, cedi_ifnb_aspirante i " + 
		         " , ( SELECT m.DF_MOVIMIENTO , m.ic_solicitud from   CEDI_SOLICITUD_MOVIMIENTO  m, cedi_solicitud s    " +   
			      " WHERE m.ic_solicitud  = s.IC_SOLICITUD     " + 
				   " and  m.IC_ESTATUS_SOLICITUD = 2       " + 
				   " and m.ic_solicitud  = "+solicitud+"  ) mv  " +  
				   "   WHERE s.ic_ifnb_aspirante = i.ic_ifnb_aspirante " + 
					"   AND s.ic_estatus_solicitud IN (4, 5) "+
				   " and  i.CG_RFC = '"+rfc_IFNB+"'");    
			
			  
			log.debug("SQL.toString() "+SQL.toString());
			
			ps =null;																																								  
			ps = con.queryPrecompilado(SQL.toString());
			rs = ps.executeQuery();
		
			while (rs.next()) {     
				datos  = new  HashMap();
			   datos.put("NO_SOLICITUD",rs.getString("NO_SOLICITUD")==null?"":rs.getString("NO_SOLICITUD")); 
			   datos.put("RAZON_SOCIAL",rs.getString("RAZON_SOCIAL")==null?"":rs.getString("RAZON_SOCIAL")); 
			   datos.put("RFC",rs.getString("RFC")==null?"":rs.getString("RFC")); 
			   datos.put("FECHA_REGISTRO",rs.getString("FECHA_REGISTRO")==null?"":rs.getString("FECHA_REGISTRO"));	
			  			   
			}        
			rs.close();
			ps.close();    
			
			 datos.put("TIPO_DESTINO",destino);
			
			
		}catch(Throwable e){
			commit = false;
			e.printStackTrace();
			log.error("Error getDatosIFNB "+e);
			throw new AppException("Error al getDatosIFNB",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getDatosIFNB (S)");
		}  
		return datos; 
	}

	 
	public List  getEstatus(String rfc_IFNB ) {
		log.info("getEstatus (E)");
		
		AccesoDB    con = new AccesoDB();
		boolean     commit = true;    
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer   SQL = new StringBuffer(); 
		String  estatus="", solicitud ="";
		List  datos = new ArrayList();
		
		
		try{
				
			con.conexionDB();
			
		   SQL.append( " select max(IC_SOLICITUD)as IC_SOLICITUD ,   ic_estatus_solicitud AS estatus from cedi_solicitud  s , CEDI_IFNB_ASPIRANTE  i " + 
		   " where  s.IC_IFNB_ASPIRANTE = i.IC_IFNB_ASPIRANTE " + 		  
		   " and  i.CG_RFC =  '"+rfc_IFNB+"'"+
		   " group by ic_estatus_solicitud ");
					  
			log.debug("SQL.toString() "+SQL.toString());
																																											  
			ps = con.queryPrecompilado(SQL.toString());
		
			rs = ps.executeQuery();
		
			if( rs.next() ){
				estatus =rs.getString("ESTATUS");	
			   solicitud =rs.getString("IC_SOLICITUD");  
			   datos.add(estatus);
			   datos.add(solicitud);
			}        
			rs.close();
			ps.close();    
			
		  		  
		   
								
		}catch(Throwable e){
			commit = false;
			e.printStackTrace();
			log.error("Error getEstatus "+e);
			throw new AppException("Error al getEstatus",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getEstatus (S)");
		}  
		return datos; 
	}
	
	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}



	public void setRfc_IFNB(String rfc_IFNB) {
		this.rfc_IFNB = rfc_IFNB;
	}

	public String getRfc_IFNB() {
		return rfc_IFNB;
	}
	
	
	
	
}//Fin Clase
