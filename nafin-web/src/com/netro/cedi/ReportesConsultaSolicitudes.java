package com.netro.cedi;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import java.sql.ResultSet;
import java.sql.PreparedStatement;

import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.Comunes;
import java.util.HashMap;
import java.io.Writer;
import netropology.utilerias.Fecha;
import netropology.utilerias.AccesoDB;
import com.netro.xlsx.ComunesXLSX;

import java.util.Iterator;

import java.util.Map;

import org.apache.commons.logging.Log;

public class ReportesConsultaSolicitudes {

	private final static Log log = ServiceLocator.getInstance().getLog(ReportesConsultaSolicitudes.class);

	private String razonSocial;
	private String ejecutivo;
	private String estatus;
	private String numFolio;
	private String fechaInicio;
	private String fechaFin;
	private String infoIf;
	private String directorioPlantillaXlsx; // Directorio donde se encuentra alamacenada la plantilla XLSX
	private int    contadorDestinoRec = 0;
	private int    contadorSectores = 0;
	private int    contadorMetodologias = 0;
	
	public ReportesConsultaSolicitudes(){}

	/**
	 * Crea el Reporte "Indicadores Financieros" en formato XLSX
	 *
	 * @param request
	 * @param directorioTemporal
	 * @param tipo
	 *
	 * @return String con el nombre del archivo generado.
	 *
	 * @author jshernandez
	 * @since F013 - 2015 -- CEDI-Autoevaluaci�n para los Intermediarios Financieros; 30/09/2015 04:12:03 p.m.
	 *
	 */
	public String generaReporteIndicadoresFinancieros(HttpServletRequest request, String directorioTemporal, String tipo){
		
		log.info("generaReporteIndicadoresFinancieros(E)");
		String      nombreArchivo = "";

		ComunesXLSX documentoXLSX = null;
		Writer      writer        = null;

		AccesoDB          con          = new AccesoDB();
		ResultSet         rs           = null;
		PreparedStatement ps           = null;
		StringBuffer      qrySentencia = null;

		try {

			if(!"XLSX".equals(tipo)){
				throw new UnsupportedOperationException("Formato de Reporte no soportado.");
			}

			boolean hayRazonSocial = !Comunes.esVacio(this.razonSocial )?true:false;
			boolean hayEjecutivo   = !Comunes.esVacio(this.ejecutivo   )?true:false;
			boolean hayEstatus     = !Comunes.esVacio(this.estatus     )?true:false;
			boolean hayNumFolio    = !Comunes.esVacio(this.numFolio    )?true:false;
			boolean hayFechaInicio = !Comunes.esVacio(this.fechaInicio )?true:false;
			boolean hayFechaFin    = !Comunes.esVacio(this.fechaFin    )?true:false;
			boolean hayInfoIf      = "true".equals  (this.infoIf       )?true:false;

			// Conectarse a la Base de Datos
			con.conexionDB();

			// PREPARAR QUERY
			qrySentencia = new StringBuffer();
			qrySentencia.append(
				"SELECT                                                                  "  +
				"   cg_razon_social                AS razon_social,                      "  +
				"   cg_rfc                         AS rfc,                               "  +
				"   s.ic_solicitud                 AS folio,                             "  +
				"   (                                                                    "  +
				"      SELECT                                                            "  +
				"         TO_CHAR(                                                       "  +
				"            MAX(df_movimiento),                                         "  +
				"            'DD/MM/YYYY HH24:MI:SS'                                     "  +
				"         )                                                              "  +
				"      FROM                                                              "  +
				"         cedi_solicitud_movimiento                                      "  +
				"      WHERE                                                             "  +
				"         IC_ESTATUS_SOLICITUD IN(2, 7) AND                              "  +
				"         ic_solicitud    = s.ic_solicitud                               "  +
				"   )                             AS fecha_registro,                     "  +
				"   s.fg_capitalizacion_anio3     AS indice_capitalizacion,              "  +
				"   s.fg_morosidad_anio3          AS morosidad,                          "  +
				"   s.fg_cobertura_reservas_anio3 AS cobertura_reservas,                 "  +
				"   s.fg_roe_anio3                AS roe,                                "  +
				"   s.fg_liquidez_anio3           AS liquidez,                           "  +
				"   s.fg_apalancamiento_anio3     AS apalancamiento,                     "  +
				"   ifin.fg_capital_contable      AS capital_contable,                   "  +
				"   ifin.fg_cartera_vigente       AS cartera_vigente,                    "  +
				"   ifin.fg_cartera_vencida       AS cartera_vencida,                    "  +
				"   s.fg_conc_acred_pm            AS concentracion_pmoral,               "  +
				"   s.fg_conc_acred_pf            AS concentracion_pfisica,              "  +
				"   s.fg_conc_cart_vig            AS concentracion_cvigente              "  +
				"FROM                                                                    "  +
				"   cedi_ifnb_aspirante           ia,                                    "  +
				"   cedi_solicitud                s,                                     "  +
				"   cedi_informacion_financiera   ifin                                   "  +
				"WHERE                                                                   "  +
				"      ia.ic_ifnb_aspirante     = s.ic_ifnb_aspirante                    "  +
				"AND    s.ic_solicitud          = ifin.ic_solicitud                      "  +
				"AND    ifin.ic_anio            = s.ig_anio_inf_financiera               " // Solo toma el del �ltimo a�o
			);
			// Criterio Razon Social
			if(hayRazonSocial){
				qrySentencia.append(
					"AND    s.ic_ifnb_aspirante     = ?                                      "
				);
			}
			// Criterio Ejecutivo CEDI
			if(hayEjecutivo){
				qrySentencia.append(
					"AND    s.cg_usuario_asignado   = ?                                      "
				);
			}
			// Criterio Estatus
			if(hayEstatus){
				qrySentencia.append(
					"AND    s.ic_estatus_solicitud  = ?                                      "
				);
			}
			// Criterio Folio solicitud
			if(hayNumFolio){
				qrySentencia.append(
					"AND    s.ic_solicitud          = ?                                      " 
				);
			}
			// Criterio Fecha Solicitud: Inicio y Fin
			if(hayFechaInicio || hayFechaFin){
				qrySentencia.append(
					"AND    s.ic_solicitud in (                                              "  +
					"         SELECT                                                         "  +
					"            ic_solicitud                                                "  +
					"         FROM                                                           "  +
					"            cedi_solicitud_movimiento                                   "  +
					"         WHERE                                                          "  +
					"            IC_ESTATUS_SOLICITUD IN(2, 7)                               "  +
					"         GROUP BY                                                       "  +
					"            ic_solicitud                                                "  +
					"         HAVING                                                         "
				);
			}
			// Criterio Fecha Solicitud: Inicio
			if(hayFechaInicio){
				qrySentencia.append(
					"               MAX(df_movimiento)   >= TO_DATE(?,'DD/MM/YYYY')          "
				);
			}
			// Criterio Fecha Solicitud: Fin
			if(hayFechaInicio && hayFechaFin){
				qrySentencia.append(
					"         AND  "
				);
			}
			if(hayFechaFin){
				qrySentencia.append(
					"               MAX(df_movimiento)  <  TO_DATE(?,'DD/MM/YYYY') + 1      "
				);
			}
			if(hayFechaInicio || hayFechaFin){
				qrySentencia.append(
					"      )                                                                 "
				);
			}
			qrySentencia.append(
				"ORDER BY cg_razon_social "
			);
			
			// AGREGAR CONDICIONES DEL QUERY
			ps = con.queryPrecompilado(qrySentencia.toString());
			int idx = 1;
			// Criterio Razon Social
			if(hayRazonSocial){
				ps.setString(idx++,this.razonSocial);
			}
			// Criterio Ejecutivo CEDI
			if(hayEjecutivo){
				ps.setString(idx++,this.ejecutivo);
			}
			// Criterio Estatus
			if(hayEstatus){
				ps.setString(idx++,this.estatus);
			}
			// Criterio Folio solicitud
			if(hayNumFolio){
				ps.setString(idx++,this.numFolio);
			}
			// Criterio Fecha Solicitud: Inicio 
			if(hayFechaInicio){
				ps.setString(idx++,this.fechaInicio);
			}
			// Criterio Fecha Solicitud:  Fin
			if(hayFechaFin){
				ps.setString(idx++,this.fechaFin);
			}
			rs = ps.executeQuery();

			// Estilos en la plantilla xlsx
			HashMap estilos = new HashMap();
			estilos.put("PERCENT",        "1");
			estilos.put("COEFF",          "2");
			estilos.put("CURRENCY",       "3");
			estilos.put("DATE",           "4");
			estilos.put("HEADER",         "5");
			estilos.put("CSTRING",        "6");
			estilos.put("TITLE",          "7");
			estilos.put("SUBTITLE",       "8");
			estilos.put("RIGHTCSTRING",   "9");
			estilos.put("CENTERCSTRING", "10");

			// Crear archivo XLSX
			documentoXLSX 	= new ComunesXLSX( directorioTemporal, estilos );

			// Crear Detalle Comisiones
			writer = documentoXLSX.creaHoja();
			documentoXLSX.setLongitudesColumna(new String[]{
				"63.42578125",
				"20.7109375",
				"14.7109375",
				"20.4257812",
				"13.2265625",
				"11.2265625",
				"11.2265625",
				"11.2265625",
				"11.2265625",
				"18.0332031",
				"25.5322265",
				"25.5322265",
				"25.5322265",
				"14.7109375",
				"14.7109375",
				"14.7109375"
			});
			
			final int NUMERO_COLUMNAS = 16;
			short     indiceRenglon   = 0;
			short     indiceCelda     = 0;

			// Definir el Encabezado PROGRAMA
			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda = 0;
			documentoXLSX.agregaCelda(indiceCelda++,"Fecha Reporte: " + Fecha.getFechaActual()); // Debug info: evaluar crear estilo de negritas para este caso
			documentoXLSX.finalizaRenglon();

			// Definir cabecera de la tabla de datos
			documentoXLSX.agregaRenglon(indiceRenglon++); 
			indiceCelda = 0;
			documentoXLSX.agregaCelda( indiceCelda++,"Intermediario Financiero",                "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"R.F.C.\nEmpresarial",                     "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"Folio\nSolicitud",                        "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"Fecha de Registro",                       "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"�ndice de\nCapitalizaci�n (ICAP)",        "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"Morosidad",                               "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"�ndice de Cobertura\nde Reservas (ICOR)", "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"ROE",                                     "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"Liquidez",                                "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"Apalancamiento",                          "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"Capital\nContable",                       "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"Cartera\nVigente",                        "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"Cartera\nVencida",                        "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"% Concentraci�n\nPersona Moral",          "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"% Concentraci�n\nPersona F�sica",         "HEADER");
			documentoXLSX.agregaCelda( indiceCelda++,"% Concentraci�n\nCartera Vigente",        "HEADER");
			documentoXLSX.finalizaRenglon();

			// Extraer datos
			boolean hayDatos     = false;
			int     ctaRegistros = 0;
			
			String razonSocial           = null;
			String rfc                   = null;
			String folio                 = null;
			String fechaRegistro         = null;
			String indiceCapitalizacion  = null;
			String morosidad             = null;
			String coberturaReservas     = null;
			String roe                   = null;
			String liquidez              = null;
			String apalancamiento        = null;
			String capitalContable       = null;
			String carteraVigente        = null;
			String carteraVencida        = null;
			String concentracionPmoral   = null;
			String concentracionPfisica  = null;
			String concentracionCvigente = null;

			while(rs.next()){

				razonSocial           = (rs.getString("RAZON_SOCIAL")           == null)?"":rs.getString("RAZON_SOCIAL");
				rfc                   = (rs.getString("RFC")                    == null)?"":rs.getString("RFC");
				folio                 = (rs.getString("FOLIO")                  == null)?"":rs.getString("FOLIO");
				fechaRegistro         = (rs.getString("FECHA_REGISTRO")         == null)?"":rs.getString("FECHA_REGISTRO");
				indiceCapitalizacion  = (rs.getString("INDICE_CAPITALIZACION")  == null)?"":rs.getString("INDICE_CAPITALIZACION");
				morosidad             = (rs.getString("MOROSIDAD")              == null)?"":rs.getString("MOROSIDAD");
				coberturaReservas     = (rs.getString("COBERTURA_RESERVAS")     == null)?"":rs.getString("COBERTURA_RESERVAS");
				roe                   = (rs.getString("ROE")                    == null)?"":rs.getString("ROE");
				liquidez              = (rs.getString("LIQUIDEZ")               == null)?"":rs.getString("LIQUIDEZ");
				apalancamiento        = (rs.getString("APALANCAMIENTO")         == null)?"":rs.getString("APALANCAMIENTO");
				capitalContable       = (rs.getString("CAPITAL_CONTABLE")       == null)?"":rs.getString("CAPITAL_CONTABLE");
				carteraVigente        = (rs.getString("CARTERA_VIGENTE")        == null)?"":rs.getString("CARTERA_VIGENTE");
				carteraVencida        = (rs.getString("CARTERA_VENCIDA")        == null)?"":rs.getString("CARTERA_VENCIDA");
				concentracionPmoral   = (rs.getString("CONCENTRACION_PMORAL")   == null)?"":rs.getString("CONCENTRACION_PMORAL");
				concentracionPfisica  = (rs.getString("CONCENTRACION_PFISICA")  == null)?"":rs.getString("CONCENTRACION_PFISICA");
				concentracionCvigente = (rs.getString("CONCENTRACION_CVIGENTE") == null)?"":rs.getString("CONCENTRACION_CVIGENTE");

				if( !Comunes.esVacio(indiceCapitalizacion) ){
					indiceCapitalizacion = Comunes.formatoDecimal(indiceCapitalizacion,2,false) + "%";
				}

				if( !Comunes.esVacio(morosidad) ){
					morosidad = Comunes.formatoDecimal(morosidad,2,false) + "%";
				}

				if( !Comunes.esVacio(roe) ){
					roe = Comunes.formatoDecimal(roe,2,false) + "%";
				}

				if( !Comunes.esVacio(coberturaReservas) ){
					coberturaReservas = Comunes.formatoDecimal(coberturaReservas,2,false) + "%";
				}

				if( !Comunes.esVacio(liquidez) ){
					liquidez = Comunes.formatoDecimal(liquidez,2,false);
				}

				if( !Comunes.esVacio(apalancamiento) ){
					apalancamiento = Comunes.formatoDecimal(apalancamiento,2,false);
				}

				if( !Comunes.esVacio(capitalContable) ){
					capitalContable = "$" + Comunes.formatoDecimal(capitalContable,2,true);
				}

				if( !Comunes.esVacio(carteraVigente) ){
					carteraVigente = "$" + Comunes.formatoDecimal(carteraVigente,2,true);
				}

				if( !Comunes.esVacio(carteraVencida) ){
					carteraVencida = "$" + Comunes.formatoDecimal(carteraVencida,2,true);
				}

				if( !Comunes.esVacio(concentracionPmoral) ){
					concentracionPmoral = Comunes.formatoDecimal(concentracionPmoral,2,false) + "%";
				}

				if( !Comunes.esVacio(concentracionPfisica) ){
					concentracionPfisica = Comunes.formatoDecimal(concentracionPfisica,2,false) + "%";
				}

				if( !Comunes.esVacio(concentracionCvigente) ){
					concentracionCvigente = Comunes.formatoDecimal(concentracionCvigente,2,false) + "%";
				}

				// Llenar tabla
				documentoXLSX.agregaRenglon(indiceRenglon++); 
				indiceCelda					= 0;
				documentoXLSX.agregaCelda(indiceCelda++,razonSocial,           "CSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,rfc,                   "CENTERCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,folio,                 "RIGHTCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,fechaRegistro,         "RIGHTCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,indiceCapitalizacion,  "RIGHTCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,morosidad,             "RIGHTCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,coberturaReservas,     "RIGHTCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,roe,                   "RIGHTCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,liquidez,              "RIGHTCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,apalancamiento,        "RIGHTCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,capitalContable,       "CURRENCY");
				documentoXLSX.agregaCelda(indiceCelda++,carteraVigente,        "CURRENCY");
				documentoXLSX.agregaCelda(indiceCelda++,carteraVencida,        "CURRENCY");
				documentoXLSX.agregaCelda(indiceCelda++,concentracionPmoral,   "CENTERCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,concentracionPfisica,  "CENTERCSTRING");
				documentoXLSX.agregaCelda(indiceCelda++,concentracionCvigente, "CENTERCSTRING");
				documentoXLSX.finalizaRenglon();

				if( ctaRegistros % 250 == 0 ){
					documentoXLSX.flush();
					ctaRegistros = 1;
				}

				ctaRegistros++;
				hayDatos = true;

			}

			// No hay registros
			if(!hayDatos){

				documentoXLSX.agregaRenglon(indiceRenglon++);
				indiceCelda = 0;
				documentoXLSX.agregaCelda(indiceCelda++,"No se encontr� ning�n registro.","HEADER",NUMERO_COLUMNAS);
				documentoXLSX.finalizaRenglon();

			}

			// Finalizar Hoja
			documentoXLSX.finalizaHoja();

			// Terminar Documento: Crear en directorioTemporal archivo XLSX con la hoja
			// generada usando una plantilla: 39IndicadoresFinancieros.template.xlsx
			nombreArchivo = documentoXLSX.finalizaDocumento( this.directorioPlantillaXlsx + "39IndicadoresFinancieros.template.xlsx" );

		} catch(OutOfMemoryError om) { // Se acabo la memoria

			log.error(
				"generaReporteIndicadoresFinancieros(OutOfMemoryError): Se acab� la memoria.\n" +
				"generaReporteIndicadoresFinancieros.request            = <" + request                           + ">\n"  +
				"generaReporteIndicadoresFinancieros.directorioTemporal = <" + directorioTemporal                + ">\n"  +
				"generaReporteIndicadoresFinancieros.tipo               = <" + tipo                              + ">\n"  +
				"generaReporteIndicadoresFinancieros.qrySentencia       = <" + qrySentencia                      + ">\n"  +
				"generaReporteIndicadoresFinancieros.Free Memory        = <" + Runtime.getRuntime().freeMemory() + ">",
				om
			);

			throw new AppException("Se acab� la memoria.");

		} catch(Exception e) {

			log.error(
				"generaReporteIndicadoresFinancieros(Exception): Error en la generacion del Archivo.\n" +
				"generaReporteIndicadoresFinancieros.request            = <" + request            + ">\n"  +
				"generaReporteIndicadoresFinancieros.directorioTemporal = <" + directorioTemporal + ">\n"  +
				"generaReporteIndicadoresFinancieros.qrySentencia       = <" + qrySentencia       + ">\n"  +
				"generaReporteIndicadoresFinancieros.tipo               = <" + tipo               + ">",
				e
			);
			throw new AppException(e);

		} finally {

			if( writer != null ){ try { writer.close(); }catch(Exception e){} }

			if(rs != null) { try { rs.close();}catch(Exception e){} }
			if(ps != null) { try { ps.close();}catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.debug("generaReporteIndicadoresFinancieros(S)");

		}

		return nombreArchivo;

	}

	/**
	 * Crea el Reporte "Directorio" en formato XLSX
	 *
	 * @param request
	 * @param directorioTemporal
	 * @param tipo
	 *
	 * @return String con el nombre del archivo generado.
	 *
	 * @author jshernandez
	 * @since F013 - 2015 -- CEDI-Autoevaluaci�n para los Intermediarios Financieros; 01/10/2015 04:33:10 p.m.
	 *
	 */
	public String generaReporteDirectorio(HttpServletRequest request, String directorioTemporal, String tipo){

		log.info("generaReporteDirectorio(E)");
		String 		nombreArchivo 	= "";

		ComunesXLSX documentoXLSX = null;
		Writer writer= null;

		AccesoDB          con          = new AccesoDB();
		ResultSet         rs           = null;
		PreparedStatement ps           = null;
		StringBuffer      qrySentencia = null;

		try {

			if(!"XLSX".equals(tipo)){
				throw new UnsupportedOperationException("Formato de Reporte no soportado.");
			}

			boolean hayRazonSocial = !Comunes.esVacio(this.razonSocial )?true:false;
			boolean hayEjecutivo   = !Comunes.esVacio(this.ejecutivo   )?true:false;
			boolean hayEstatus     = !Comunes.esVacio(this.estatus     )?true:false;
			boolean hayNumFolio    = !Comunes.esVacio(this.numFolio    )?true:false;
			boolean hayFechaInicio = !Comunes.esVacio(this.fechaInicio )?true:false;
			boolean hayFechaFin    = !Comunes.esVacio(this.fechaFin    )?true:false;
			boolean hayInfoIf      = "true".equals   (this.infoIf      )?true:false;

			// Conectarse a la Base de Datos
			con.conexionDB();

			// PREPARAR QUERY
			qrySentencia = new StringBuffer();
			qrySentencia.append(
				"SELECT                                                             "  +
				"   cg_razon_social         AS intermediario_financiero,            "  +
				"   cg_rfc                  AS rfc,                                 "  +
				"   s.ic_solicitud          AS folio_solicitud,                     "  +
				"   (                                                               "  +
				"      SELECT                                                       "  +
				"         TO_CHAR(                                                  "  +
				"            MAX(df_movimiento),                                    "  +
				"            'DD/MM/YYYY HH24:MI:SS'                                "  +
				"         )                                                         "  +
				"      FROM                                                         "  +
				"         cedi_solicitud_movimiento                                 "  +
				"      WHERE                                                        "  +
				"         IC_ESTATUS_SOLICITUD IN(2, 7) AND                         "  +
				"         ic_solicitud     = s.ic_solicitud                         "  +
				"   )                       AS fecha_hora_registro,                 "  +
				"   ia.cg_calle             AS calle,                               "  +
				"   ia.cg_colonia           AS colonia,                             "  +
				"   e.cd_nombre             AS estado,                              "  +
				"   m.cd_nombre             AS municipio,                           "  +
				"   cg_cp                   AS codigo_postal,                       "  +
				"   cg_contacto             AS contacto,                            "  +
				"   cg_email                AS email_contacto,                      "  +
				"   cg_telefono             AS telefono                             "  +
				"FROM                                                               "  +
				"   cedi_ifnb_aspirante        ia,                                  "  +
				"   cedi_solicitud             s,                                   "  +
				"   comcat_municipio           m,                                   "  +
				"   comcat_estado              e                                    "  +
				"WHERE                                                              "  +
				"   ia.ic_ifnb_aspirante    = s.ic_ifnb_aspirante       AND         "  +
				"   ia.ic_pais              = m.ic_pais                 AND         "  +
				"   ia.ic_estado            = m.ic_estado               AND         "  +
				"   ia.ic_municipio         = m.ic_municipio            AND         "  +
				"   ia.ic_estado            = e.ic_estado                           "
			);
			// Criterio Razon Social
			if(hayRazonSocial){
				qrySentencia.append(
					"   AND s.ic_ifnb_aspirante     = ?                              "
				);
			}
			// Criterio Ejecutivo CEDI
			if(hayEjecutivo){
				qrySentencia.append(
					"   AND s.cg_usuario_asignado   = ?                               "
				);
			}
			// Criterio Estatus
			if(hayEstatus){
				qrySentencia.append(
					"   AND s.ic_estatus_solicitud  = ?                               "
				);
			}
			// Criterio Folio Solicitud
			if(hayNumFolio){
				qrySentencia.append(
					"   AND s.ic_solicitud          = ?                               "
				);
			}
			// Criterio Fecha Solicitud: Inicio y Fin
			if(hayFechaInicio || hayFechaFin ){
				qrySentencia.append(
					"   AND s.ic_solicitud in (                                         "  +
					"      SELECT                                                       "  +
					"         ic_solicitud                                              "  +
					"      FROM                                                         "  +
					"         cedi_solicitud_movimiento                                 "  +
					"      WHERE                                                        "  +
					"         IC_ESTATUS_SOLICITUD IN(2, 7)                             "  +
					"      GROUP BY                                                     "  +
					"         ic_solicitud                                              "  +
					"      HAVING                                                       "
				);
			}
			// Criterio Fecha Solicitud: Inicio
			if(hayFechaInicio){
				qrySentencia.append(
					"            MAX(df_movimiento)    >= TO_DATE(?,'DD/MM/YYYY')       "
				);
			}
			if(hayFechaInicio && hayFechaFin){
				qrySentencia.append(
					"         AND                                                       "
				);
			}
			// Criterio Fecha Solicitud: Fin
			if(hayFechaFin){
				qrySentencia.append(
					"            MAX(df_movimiento)    <  TO_DATE(?,'DD/MM/YYYY') + 1   "
				);
			}
			if(hayFechaInicio || hayFechaFin ){
				qrySentencia.append(
					"   )                                                               "
				);
			}
			qrySentencia.append(
				"ORDER BY cg_razon_social                                              "
			);

			// AGREGAR CONDICIONES DEL QUERY
			ps = con.queryPrecompilado(qrySentencia.toString());
			int idx = 1;
			// Criterio Razon Social
			if(hayRazonSocial){
				ps.setString(idx++,this.razonSocial);
			}
			// Criterio Ejecutivo CEDI
			if(hayEjecutivo){
				ps.setString(idx++,this.ejecutivo);
			}
			// Criterio Estatus
			if(hayEstatus){
				ps.setString(idx++,this.estatus);
			}
			// Criterio Folio Solicitud
			if(hayNumFolio){
				ps.setString(idx++,this.numFolio);
			}
			// Criterio Fecha Solicitud: Inicio 
			if(hayFechaInicio){
				ps.setString(idx++,this.fechaInicio);
			}
			// Criterio Fecha Solicitud:  Fin
			if(hayFechaFin){
				ps.setString(idx++,this.fechaFin);
			}
			rs = ps.executeQuery();

			// Estilos en la plantilla xlsx
			HashMap estilos = new HashMap();
			estilos.put("PERCENT",        "1");
			estilos.put("COEFF",          "2");
			estilos.put("CURRENCY",       "3");
			estilos.put("DATE",           "4");
			estilos.put("HEADER",         "5");
			estilos.put("CSTRING",        "6");
			estilos.put("TITLE",          "7");
			estilos.put("SUBTITLE",       "8");
			estilos.put("RIGHTCSTRING",   "9");
			estilos.put("CENTERCSTRING", "10");

			// Crear archivo XLSX
			documentoXLSX 	= new ComunesXLSX( directorioTemporal, estilos );

			// Crear Detalle Comisiones
			writer = documentoXLSX.creaHoja();
			documentoXLSX.setLongitudesColumna(new String[]{
				"63.42578125",
				"20.7109375",
				"14.7109375",
				"20.4257812",
				"65.42578125",
				"65.42578125",
				"15.7109375",
				"63.42578125",
				"63.42578125",
				"23.7109375"
			});
			
			final int NUMERO_COLUMNAS = 10;
			short     indiceRenglon   = 0;
			short     indiceCelda     = 0;

			// Definir el Encabezado PROGRAMA
			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda = 0;
			documentoXLSX.agregaCelda(indiceCelda++,"Fecha Reporte: " + Fecha.getFechaActual()); // Debug info: evaluar crear estilo de negritas para este caso
			documentoXLSX.finalizaRenglon();

			// Definir cabecera de la tabla de datos
			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda = 0;
			documentoXLSX.agregaCelda( indiceCelda++,"Intermediario Financiero",                    "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"R.F.C.\nEmpresarial",                         "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Folio\nSolicitud",                            "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Fecha de Registro",                           "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Domicilio Empresarial",                       "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Estado y Delegaci�n o Municipio empresarial", "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"C�digo Postal\nEmpresarial",                  "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Contacto",                                    "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Correo Electr�nico\nEmpresarial",             "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Tel�fono\nEmpresarial",                       "HEADER" );
			documentoXLSX.finalizaRenglon();

			// Extraer datos
			boolean      hayDatos 						= false;
			int          ctaRegistros            = 0;

			String       intermediarioFinanciero = null;
			String       rfc                     = null;
			String       folioSolicitud          = null;
			String       fechaHoraRegistro       = null;
			String       calle                   = null;
			String       colonia                 = null;
			String       estado                  = null;
			String       municipio               = null;
			String       codigoPostal            = null;
			String       contacto                = null;
			String       emailContacto           = null;
			String       telefono                = null;
			StringBuffer domicilio               = new StringBuffer(512);
			StringBuffer estadoMunicipio         = new StringBuffer(512);

			while(rs.next()){
				
				intermediarioFinanciero = (rs.getString("INTERMEDIARIO_FINANCIERO") == null)?"":rs.getString("INTERMEDIARIO_FINANCIERO");
				rfc                     = (rs.getString("RFC")                      == null)?"":rs.getString("RFC");
				folioSolicitud          = (rs.getString("FOLIO_SOLICITUD")          == null)?"":rs.getString("FOLIO_SOLICITUD");
				fechaHoraRegistro       = (rs.getString("FECHA_HORA_REGISTRO")      == null)?"":rs.getString("FECHA_HORA_REGISTRO");
				calle                   = (rs.getString("CALLE")                    == null)?"":rs.getString("CALLE");
				colonia                 = (rs.getString("COLONIA")                  == null)?"":rs.getString("COLONIA");
				estado                  = (rs.getString("ESTADO")                   == null)?"":rs.getString("ESTADO");
				municipio               = (rs.getString("MUNICIPIO")                == null)?"":rs.getString("MUNICIPIO");
				codigoPostal            = (rs.getString("CODIGO_POSTAL")            == null)?"":rs.getString("CODIGO_POSTAL");
				contacto                = (rs.getString("CONTACTO")                 == null)?"":rs.getString("CONTACTO");
				emailContacto           = (rs.getString("EMAIL_CONTACTO")           == null)?"":rs.getString("EMAIL_CONTACTO");
				telefono                = (rs.getString("TELEFONO")                 == null)?"":rs.getString("TELEFONO");

				// Domicilio
				domicilio.setLength(0);
				domicilio.append(calle);
				if(!Comunes.esVacio(calle) && !Comunes.esVacio(colonia)){
					domicilio.append(", ");
				}
				domicilio.append(colonia);

				// Estado y Municipio
				estadoMunicipio.setLength(0);
				estadoMunicipio.append(estado);
				if(!Comunes.esVacio(estado)&&!Comunes.esVacio(municipio)){
					estadoMunicipio.append(", ");
				}
				estadoMunicipio.append(municipio);

				// Llenar tabla
				documentoXLSX.agregaRenglon(indiceRenglon++);
				indiceCelda = 0;
				documentoXLSX.agregaCelda(indiceCelda++,intermediarioFinanciero,    "CSTRING"       );
				documentoXLSX.agregaCelda(indiceCelda++,rfc,                        "CSTRING"       );
				documentoXLSX.agregaCelda(indiceCelda++,folioSolicitud,             "CENTERCSTRING" );
				documentoXLSX.agregaCelda(indiceCelda++,fechaHoraRegistro,          "CENTERCSTRING" );
				documentoXLSX.agregaCelda(indiceCelda++,domicilio.toString(),       "CSTRING"       );
				documentoXLSX.agregaCelda(indiceCelda++,estadoMunicipio.toString(), "CSTRING"       );
				documentoXLSX.agregaCelda(indiceCelda++,codigoPostal,               "CENTERCSTRING" );
				documentoXLSX.agregaCelda(indiceCelda++,contacto,                   "CSTRING"       );
				documentoXLSX.agregaCelda(indiceCelda++,emailContacto,              "CSTRING"       );
				documentoXLSX.agregaCelda(indiceCelda++,telefono,                   "CENTERCSTRING" );
				documentoXLSX.finalizaRenglon();

				if( ctaRegistros % 250 == 0 ){
					documentoXLSX.flush();
					ctaRegistros = 1;
				}

				ctaRegistros++;
				hayDatos = true;

			}

			// No hay registros
			if(!hayDatos){

				documentoXLSX.agregaRenglon(indiceRenglon++);
				indiceCelda = 0;
				documentoXLSX.agregaCelda(indiceCelda++,"No se encontr� ning�n registro.","HEADER",NUMERO_COLUMNAS);
				documentoXLSX.finalizaRenglon();

			}

			// Finalizar Hoja
			documentoXLSX.finalizaHoja(); 

			// Terminar Documento: Crear en directorioTemporal archivo XLSX con la hoja
			// generada usando una plantilla: 39Directorio.template.xlsx
			nombreArchivo = documentoXLSX.finalizaDocumento( this.directorioPlantillaXlsx + "39Directorio.template.xlsx" );

		} catch(OutOfMemoryError om) { // Se acabo la memoria

			log.error(
				"generaReporteDirectorio(OutOfMemoryError): Se acab� la memoria.\n" +
				"generaReporteDirectorio.request            = <" + request                           + ">\n"  +
				"generaReporteDirectorio.directorioTemporal = <" + directorioTemporal                + ">\n"  +
				"generaReporteDirectorio.tipo               = <" + tipo                              + ">\n"  +
				"generaReporteDirectorio.qrySentencia       = <" + qrySentencia                      + ">\n"  +
				"generaReporteDirectorio.Free Memory        = <" + Runtime.getRuntime().freeMemory() + ">",
				om
			);

			throw new AppException("Se acab� la memoria.");

		} catch(Exception e) {

			log.error(
				"generaReporteDirectorio(Exception): Error en la generacion del Archivo.\n" +
				"generaReporteDirectorio.request            = <" + request            + ">\n"  +
				"generaReporteDirectorio.directorioTemporal = <" + directorioTemporal + ">\n"  +
				"generaReporteDirectorio.qrySentencia       = <" + qrySentencia       + ">\n"  +
				"generaReporteDirectorio.tipo               = <" + tipo               + ">",
				e
			);
			throw new AppException(e);

		} finally {

			if( writer != null ){ try { writer.close(); }catch(Exception e){} }

			if(rs != null) { try { rs.close();}catch(Exception e){} }
			if(ps != null) { try { ps.close();}catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.debug("generaReporteDirectorio(S)");

		}

		return nombreArchivo;

	}

	/**
	 * Crea el Reporte "Informaci�n Cualitativa" en formato XLSX
	 *
	 * @param request
	 * @param directorioTemporal
	 * @param tipo
	 *
	 * @return String con el nombre del archivo generado.
	 *
	 * @author abautista
	 * @since F013 - 2015 -- CEDI-Autoevaluaci�n para los Intermediarios Financieros; 14/10/2015 12:46:15 p.m.
	 *
	 */
	public String generaReporteInformacionCualitativa(HttpServletRequest request, String directorioTemporal, String tipo){

		log.info("generaReporteInformacionCualitativa(E)");
		String  nombreArchivo = "";
		boolean hayDatos      = false;
		int     ctaRegistros  = 0;

		ComunesXLSX documentoXLSX = null;
		Writer      writer        = null;


		// OBTENGO LOS ENCABEZADOS DEL ARCHIVO
		List<Integer> headers = getEncabezados();
		System.out.println("ABR_headers: " + headers.toString());

		// OBTENGO LOS DATOS DEL ARCHIVO
		List<HashMap> listaArchivo = consultaInformacionCualitativa();
		HashMap<String, String> datosArchivo = new HashMap<String, String>();
		System.out.println("ABR_listaArchvo: " + listaArchivo.toString());

		try{
			// Estilos en la plantilla xlsx
			HashMap estilos = new HashMap();
			estilos.put("PERCENT",        "1");
			estilos.put("COEFF",          "2");
			estilos.put("CURRENCY",       "3");
			estilos.put("DATE",           "4");
			estilos.put("HEADER",         "5");
			estilos.put("CSTRING",        "6");
			estilos.put("TITLE",          "7");
			estilos.put("SUBTITLE",       "8");
			estilos.put("RIGHTCSTRING",   "9");
			estilos.put("CENTERCSTRING", "10");

			// Crear archivo XLSX
			documentoXLSX = new ComunesXLSX( directorioTemporal, estilos );

			// Crear Detalle Comisiones
			writer = documentoXLSX.creaHoja();
			documentoXLSX.setLongitudesColumna(new String[]{
			"63.42578125",
			"20.7109375",
			"14.7109375",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812"
			});

			final int NUMERO_COLUMNAS = headers.size();
			short     indiceRenglon   = 0;
			short     indiceCelda     = 0;

			// Definir el Encabezado PROGRAMA
			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda = 0;
			documentoXLSX.agregaCelda(indiceCelda++,"Fecha Reporte: " + Fecha.getFechaActual());
			documentoXLSX.finalizaRenglon();

			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda = 0;
			documentoXLSX.agregaCelda(indiceCelda++,                   " ");
			documentoXLSX.agregaCelda(indiceCelda++,                   " ");
			documentoXLSX.agregaCelda(indiceCelda++,                   " ");
			documentoXLSX.agregaCelda(indiceCelda++,                   " ");
			documentoXLSX.agregaCelda(indiceCelda,                     " Destino de los recursos ",                           "HEADER", contadorDestinoRec   );
			documentoXLSX.agregaCelda(indiceCelda+=contadorDestinoRec, " Sectores que atiende por porcentaje de su cartera ", "HEADER", contadorSectores     );
			documentoXLSX.agregaCelda(indiceCelda+=contadorSectores,   " Metodolog�as de calificaci�n de cartera ",           "HEADER", contadorMetodologias );
			documentoXLSX.finalizaRenglon();

			// Definir cabecera de la tabla de datos
			datosArchivo = listaArchivo.get(0);
			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda = 0;
			documentoXLSX.agregaCelda( indiceCelda++,(String)datosArchivo.get("INTERMEDIARIO_FINANCIERO"), "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,(String)datosArchivo.get("RFC_EMPRESARIAL"),          "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,(String)datosArchivo.get("FOLIO_SOLICITUD"),          "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,(String)datosArchivo.get("FECHA_REGISTRO"),           "HEADER" );
			for(int i = 0; i < headers.size(); i++){
				documentoXLSX.agregaCelda( indiceCelda++,(String)datosArchivo.get("" + headers.get(i)),     "HEADER" );
			}
			documentoXLSX.finalizaRenglon();

			// Llenar tabla
			for(int i = 1; i < listaArchivo.size(); i++){

				datosArchivo = new HashMap<String, String>();
				datosArchivo = listaArchivo.get(i);
				documentoXLSX.agregaRenglon(indiceRenglon++);
				indiceCelda = 0;
				documentoXLSX.agregaCelda(indiceCelda++, (String)datosArchivo.get("INTERMEDIARIO_FINANCIERO"),"CSTRING"       );
				documentoXLSX.agregaCelda(indiceCelda++, (String)datosArchivo.get("RFC_EMPRESARIAL"),         "CSTRING"       );
				documentoXLSX.agregaCelda(indiceCelda++, (String)datosArchivo.get("FOLIO_SOLICITUD"),         "CENTERCSTRING" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)datosArchivo.get("FECHA_REGISTRO"),          "CENTERCSTRING" );
				for(int j = 0; j < headers.size(); j++){
					documentoXLSX.agregaCelda(indiceCelda++,(String)datosArchivo.get("" + headers.get(j)),     "CENTERCSTRING" );
				}
				documentoXLSX.finalizaRenglon();

				if( ctaRegistros % 250 == 0 ){
					documentoXLSX.flush();
					ctaRegistros = 1;
				}

				ctaRegistros++;
				hayDatos = true;

			}

			// No hay registros
			if(!hayDatos){

				documentoXLSX.agregaRenglon(indiceRenglon++);
				indiceCelda = 0;
				documentoXLSX.agregaCelda(indiceCelda++,"No se encontr� ning�n registro.","HEADER",NUMERO_COLUMNAS);
				documentoXLSX.finalizaRenglon();

			}

			// Finalizar Hoja
			documentoXLSX.finalizaHoja(); 

			// Terminar Documento: Crear en directorioTemporal archivo XLSX con la hoja
			// generada usando una plantilla: 39InformacionCualitativa.template.xlsx
			nombreArchivo = documentoXLSX.finalizaDocumento( this.directorioPlantillaXlsx + "39InformacionCualitativa.template.xlsx" );


		}catch(OutOfMemoryError om) { // Se acabo la memoria

			log.error(
				"generaReporteInformacionCualitativa(OutOfMemoryError): Se acab� la memoria.\n" +
				"generaReporteInformacionCualitativa.request            = <" + request                           + ">\n"  +
				"generaReporteInformacionCualitativa.directorioTemporal = <" + directorioTemporal                + ">\n"  +
				"generaReporteInformacionCualitativa.tipo               = <" + tipo                              + ">\n"  +
				"generaReporteInformacionCualitativa.Free Memory        = <" + Runtime.getRuntime().freeMemory() + ">",
				om
			);

			throw new AppException("Se acab� la memoria.");

		} catch(Exception e) {

			log.error(
				"generaReporteInformacionCualitativa(Exception): Error en la generacion del Archivo.\n" +
				"generaReporteInformacionCualitativa.request            = <" + request            + ">\n"  +
				"generaReporteInformacionCualitativa.directorioTemporal = <" + directorioTemporal + ">\n"  +
				"generaReporteInformacionCualitativa.tipo               = <" + tipo               + ">",
				e
			);
			throw new AppException(e);

		} finally {
			log.debug("generaReporteInformacionCualitativa(S)");
		}

		return nombreArchivo;
	}
	/**
	 * Crea el Reporte "Cumplimiento de Indicadores" en formato XLSX
	 *
	 * @param request
	 * @param directorioTemporal
	 * @param tipo
	 *
	 * @return String con el nombre del archivo generado.
	 *
	 * @author abautista
	 * @since F013 - 2015 -- CEDI-Autoevaluaci�n para los Intermediarios Financieros; 14/10/2015 05:05:05 p.m.
	 *
	 */
	public String generaReporteCumplimientoIndicadores(HttpServletRequest request, String directorioTemporal, String tipo){

		log.info("generaReporteCumplimientoIndicadores(E)");
		String      nombreArchivo  = "";

		ComunesXLSX documentoXLSX = null;
		Writer writer = null;

		AccesoDB          con             = new AccesoDB();
		ResultSet         rs              = null;
		ResultSet         rsAux           = null;
		PreparedStatement ps              = null;
		StringBuffer      qrySentencia    = null; // Ejecuta el query principal
		StringBuffer      qrySentenciaAux = null; // Ejecuta el query para determinar en que hoja mostrar la informaci�n

		try {

			if(!"XLSX".equals(tipo)){
				throw new UnsupportedOperationException("Formato de Reporte no soportado.");
			}

			boolean hayRazonSocial = !Comunes.esVacio(this.razonSocial )?true:false;
			boolean hayEjecutivo   = !Comunes.esVacio(this.ejecutivo   )?true:false;
			boolean hayEstatus     = !Comunes.esVacio(this.estatus     )?true:false;
			boolean hayNumFolio    = !Comunes.esVacio(this.numFolio    )?true:false;
			boolean hayFechaInicio = !Comunes.esVacio(this.fechaInicio )?true:false;
			boolean hayFechaFin    = !Comunes.esVacio(this.fechaFin    )?true:false;

			// Conectarse a la Base de Datos
			con.conexionDB();

			// PREPARAR QUERY PARA DETERMINAR EN QUE HOJA SE MOSTRAR� LA INFORMACI�N
			qrySentenciaAux = new StringBuffer();

			qrySentenciaAux.append("SELECT   IC_SOLICITUD, IC_ELEMENTO_CEDULA, CS_VIABLE \n"              );
			qrySentenciaAux.append("    FROM CEDI_CEDULA \n"                                              );
			qrySentenciaAux.append("   WHERE IC_ELEMENTO_CEDULA IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11) \n");
			qrySentenciaAux.append("     AND IC_SOLICITUD = ? \n"                                         );
			qrySentenciaAux.append("ORDER BY IC_SOLICITUD, IC_ELEMENTO_CEDULA "                           );

			// PREPARAR QUERY PRINCIPAL
			qrySentencia = new StringBuffer();

			qrySentencia.append("SELECT IA.CG_RAZON_SOCIAL, IA.CG_RFC, S.IC_SOLICITUD,\n"                          );
			qrySentencia.append("       (SELECT TO_CHAR (MAX (DF_MOVIMIENTO),\n"                                   );
			qrySentencia.append("                        'DD/MM/YYYY HH24:MI:SS'\n"                                );
			qrySentencia.append("                       )\n"                                                       );
			qrySentencia.append("          FROM CEDI_SOLICITUD_MOVIMIENTO\n"                                       );
			qrySentencia.append("         WHERE IC_ESTATUS_SOLICITUD IN(2, 7) AND IC_SOLICITUD = S.IC_SOLICITUD)\n");
			qrySentencia.append("                                                            AS FECHA_REGISTRO,\n" );
			qrySentencia.append("       S.FG_CAPITALIZACION_ANIO3 AS INDICECAPITALIZACION,\n"                      );
			qrySentencia.append("       S.FG_MOROSIDAD_ANIO3 AS MOROSIDAD,\n"                                      );
			qrySentencia.append("       S.FG_COBERTURA_RESERVAS_ANIO3 AS COBERTURARESERVAS,\n"                     );
			qrySentencia.append("       S.FG_ROE_ANIO3 AS ROE, S.FG_LIQUIDEZ_ANIO3 AS LIQUIDEZ,\n"                 );
			qrySentencia.append("       S.FG_APALANCAMIENTO_ANIO3 AS APALANCAMIENTO, S.CS_TIPO_CARTERA, \n"        );
			//qrySentencia.append("       S.FG_APALANCAMIENTO_ANIO3 AS APALANCAMIENTO, S.FG_CARTERA,\n"            );
			qrySentencia.append("       (SELECT S.FG_CARTERA \n"                                                   );
			qrySentencia.append("          FROM CEDI_RESPUESTA_IFNB I, CEDI_RESPUESTA_PARAMETRIZACION P\n"         );
			qrySentencia.append("         WHERE I.IC_RESPUESTA_PARAMETRIZACION = P.IC_RESPUESTA_PARAMETRIZACION\n" );
			qrySentencia.append("           AND S.IC_SOLICITUD = I.IC_SOLICITUD\n "                                );
			qrySentencia.append("           AND P.IC_PREGUNTA = 4\n"                                               );
			qrySentencia.append("           AND P.IC_RESPUESTA_PARAMETRIZACION = 10) AS CARTERA_PYME,\n "          );
			qrySentencia.append("       (SELECT S.FG_CARTERA \n"                                                   );
			qrySentencia.append("          FROM CEDI_RESPUESTA_IFNB I, CEDI_RESPUESTA_PARAMETRIZACION P\n"         );
			qrySentencia.append("         WHERE I.IC_RESPUESTA_PARAMETRIZACION = P.IC_RESPUESTA_PARAMETRIZACION\n" );
			qrySentencia.append("           AND S.IC_SOLICITUD = I.IC_SOLICITUD\n "                                );
			qrySentencia.append("           AND P.IC_PREGUNTA = 4\n"                                               );
			qrySentencia.append("           AND P.IC_RESPUESTA_PARAMETRIZACION = 11) AS MICRO_CREDITO,\n "         );
			qrySentencia.append("       S.FG_CONC_ACRED_PM, S.FG_CONC_ACRED_PF, S.FG_CONC_CART_VIG\n"              );
			qrySentencia.append("  FROM CEDI_IFNB_ASPIRANTE IA, CEDI_SOLICITUD S\n"                                );
			qrySentencia.append(" WHERE IA.IC_IFNB_ASPIRANTE = S.IC_IFNB_ASPIRANTE"                                );

			// Criterio Razon Social
			if(hayRazonSocial){
				qrySentencia.append("   AND S.IC_IFNB_ASPIRANTE = ? \n"                                  );
			}
			// Criterio Ejecutivo CEDI
			if(hayEjecutivo){
				qrySentencia.append("   AND S.CG_USUARIO_ASIGNADO = ? \n"                                );
			}
			// Criterio Estatus
			if(hayEstatus){
				qrySentencia.append("   AND S.IC_ESTATUS_SOLICITUD = ? \n"                               );
			}
			// Criterio Folio Solicitud
			if(hayNumFolio){
				qrySentencia.append("   AND S.IC_SOLICITUD = ? \n"                                       );
			}
			if(hayFechaInicio && hayFechaFin){
				qrySentencia.append("   AND S.IC_SOLICITUD IN ( \n"                                      );
				qrySentencia.append("          SELECT   IC_SOLICITUD \n"                                 );
				qrySentencia.append("              FROM CEDI_SOLICITUD_MOVIMIENTO\n"                     );
				qrySentencia.append("             WHERE IC_ESTATUS_SOLICITUD IN(2, 7) \n"                );
				qrySentencia.append("          GROUP BY IC_SOLICITUD \n"                                 );
				qrySentencia.append("            HAVING MAX (DF_MOVIMIENTO) >= TO_DATE(?,'DD/MM/YYYY')\n");
				qrySentencia.append("               AND \n"                                              );
				qrySentencia.append("               MAX (DF_MOVIMIENTO) < TO_DATE(?,'DD/MM/YYYY') + 1 \n");
				qrySentencia.append("                                              ) \n"                 );
			}
			qrySentencia.append(" ORDER BY S.IC_SOLICITUD"                                              );
			log.debug("qrySentencia: " + qrySentencia.toString());

			// AGREGAR CONDICIONES DEL QUERY
			ps = con.queryPrecompilado(qrySentencia.toString());
			int idx = 1;
			// Criterio Razon Social
			if(hayRazonSocial){
				ps.setString(idx++,this.razonSocial);
			}
			// Criterio Ejecutivo CEDI
			if(hayEjecutivo){
				ps.setString(idx++,this.ejecutivo);
			}
			// Criterio Estatus
			if(hayEstatus){
				ps.setString(idx++,this.estatus);
			}
			// Criterio Folio Solicitud
			if(hayNumFolio){
				ps.setString(idx++,this.numFolio);
			}
			// Criterio Fecha Solicitud: Inicio 
			if(hayFechaInicio){
				ps.setString(idx++,this.fechaInicio);
			}
			// Criterio Fecha Solicitud:  Fin
			if(hayFechaFin){
				ps.setString(idx++,this.fechaFin);
			}
			rs = ps.executeQuery();

			// Estilos en la plantilla xlsx
			HashMap estilos = new HashMap();
			estilos.put("PERCENT",        "1");
			estilos.put("COEFF",          "2");
			estilos.put("CURRENCY",       "3");
			estilos.put("DATE",           "4");
			estilos.put("HEADER",         "5");
			estilos.put("CSTRING",        "6");
			estilos.put("TITLE",          "7");
			estilos.put("SUBTITLE",       "8");
			estilos.put("RIGHTCSTRING",   "9");
			estilos.put("CENTERCSTRING", "10");

			// Crear archivo XLSX
			documentoXLSX  = new ComunesXLSX( directorioTemporal, estilos );

			final int NUMERO_COLUMNAS = 15;
			short     indiceRenglon   = 0;
			short     indiceCelda     = 0;

			// Extraer datos
			boolean hayDatos                = false;
			int     ctaRegistros            = 0;
			int     elementoCedula          = 0;

			String  intermediarioFinanciero = null;
			String  rfc                     = null;
			String  folioSolicitud          = null;
			String  fechaHoraRegistro       = null;
			String  capitalizacion          = null;
			String  morosidad               = null;
			String  coberturaReservas       = null;
			String  roe                     = null;
			String  liquidez                = null;
			String  apalancamiento          = null;
			String  tipoCartera             = null;
			String  carteraPyme             = null;
			String  microcredito            = null;
			String  concentracionPerMoral   = null;
			String  concentracionPerFisica  = null;
			String  concentracionCartVigente= null;
			String  csViable                = null;

			HashMap<String, String> indCumplidos   = new HashMap<String, String>();
			HashMap<String, String> indNoCumplidos = new HashMap<String, String>();
			List<HashMap>   listaIndCumplidos      = new ArrayList<HashMap>();
			List<HashMap>   listaIndNoCumplidos    = new ArrayList<HashMap>();

			double cantPyme = 9000000;
			double tmpPyme  = 0;
			double cantMc   = 5000000;
			double tmpMc    = 0;

			while(rs.next()){

				intermediarioFinanciero  = (rs.getString("CG_RAZON_SOCIAL")      == null)?"":rs.getString("CG_RAZON_SOCIAL");
				rfc                      = (rs.getString("CG_RFC")               == null)?"":rs.getString("CG_RFC");
				folioSolicitud           = (rs.getString("IC_SOLICITUD")         == null)?"":rs.getString("IC_SOLICITUD");
				fechaHoraRegistro        = (rs.getString("FECHA_REGISTRO")       == null)?"":rs.getString("FECHA_REGISTRO");
				capitalizacion           = (rs.getString("INDICECAPITALIZACION") == null)?"":rs.getString("INDICECAPITALIZACION");
				morosidad                = (rs.getString("MOROSIDAD")            == null)?"":rs.getString("MOROSIDAD");
				coberturaReservas        = (rs.getString("COBERTURARESERVAS")    == null)?"":rs.getString("COBERTURARESERVAS");
				roe                      = (rs.getString("ROE")                  == null)?"":rs.getString("ROE");
				liquidez                 = (rs.getString("LIQUIDEZ")             == null)?"":rs.getString("LIQUIDEZ");
				apalancamiento           = (rs.getString("APALANCAMIENTO")       == null)?"":rs.getString("APALANCAMIENTO");
				tipoCartera              = (rs.getString("CS_TIPO_CARTERA")      == null)?"":rs.getString("CS_TIPO_CARTERA");
				carteraPyme              = (rs.getString("CARTERA_PYME")         == null)?"":rs.getString("CARTERA_PYME");
				microcredito             = (rs.getString("MICRO_CREDITO")        == null)?"":rs.getString("MICRO_CREDITO");
				concentracionPerMoral    = (rs.getString("FG_CONC_ACRED_PM")     == null)?"":rs.getString("FG_CONC_ACRED_PM");
				concentracionPerFisica   = (rs.getString("FG_CONC_ACRED_PF")     == null)?"":rs.getString("FG_CONC_ACRED_PF");
				concentracionCartVigente = (rs.getString("FG_CONC_CART_VIG")     == null)?"":rs.getString("FG_CONC_CART_VIG");

				// Ejecuta el query para determinar en que hoja se mostrar� la informaci�n
				rsAux = null;
				ps    = null;
				ps    = con.queryPrecompilado(qrySentenciaAux.toString());
				ps.setInt(1,Integer.parseInt(folioSolicitud));
				rsAux = ps.executeQuery();
				//System.out.println("-->\n" + qrySentenciaAux.toString()); // TODO: Debug
				//System.out.println("-->\n" + folioSolicitud); // TODO: Debug

				indCumplidos = new HashMap<String, String>();
				indCumplidos.put("intermediarioFinanciero",  intermediarioFinanciero );
				indCumplidos.put("rfc",                      rfc                     );
				indCumplidos.put("folioSolicitud",           folioSolicitud          );
				indCumplidos.put("fechaHoraRegistro",        fechaHoraRegistro       );
				indCumplidos.put("capitalizacion",           ""                      );
				indCumplidos.put("morosidad",                ""                      );
				indCumplidos.put("coberturaReservas",        ""                      );
				indCumplidos.put("roe",                      ""                      );
				indCumplidos.put("liquidez",                 ""                      );
				indCumplidos.put("apalancamiento",           ""                      );
				indCumplidos.put("carteraPyme",              ""                      );
				indCumplidos.put("microcredito",             ""                      );
				indCumplidos.put("concentracionPerMoral",    ""                      );
				indCumplidos.put("concentracionPerFisica",   ""                      );
				indCumplidos.put("concentracionCartVigente", ""                      );

				indNoCumplidos = new HashMap<String, String>();
				indNoCumplidos.put("intermediarioFinanciero",  intermediarioFinanciero );
				indNoCumplidos.put("rfc",                      rfc                     );
				indNoCumplidos.put("folioSolicitud",           folioSolicitud          );
				indNoCumplidos.put("fechaHoraRegistro",        fechaHoraRegistro       );
				indNoCumplidos.put("capitalizacion",           ""                      );
				indNoCumplidos.put("morosidad",                ""                      );
				indNoCumplidos.put("coberturaReservas",        ""                      );
				indNoCumplidos.put("roe",                      ""                      );
				indNoCumplidos.put("liquidez",                 ""                      );
				indNoCumplidos.put("apalancamiento",           ""                      );
				indNoCumplidos.put("carteraPyme",              ""                      );
				indNoCumplidos.put("microcredito",             ""                      );
				indNoCumplidos.put("concentracionPerMoral",    ""                      );
				indNoCumplidos.put("concentracionPerFisica",   ""                      );
				indNoCumplidos.put("concentracionCartVigente", ""                      );

				//System.out.println("carteraPyme: " + carteraPyme); // TODO: Debug
				//System.out.println("microcredito: " + microcredito); // TODO: Debug
				//System.out.println("tipoCartera: " + tipoCartera); // TODO: Debug

				if(!carteraPyme.equals("")){
					tmpPyme = Double.parseDouble(carteraPyme);
					//System.out.println("(tmpPyme-cantPyme) = " + (tmpPyme-cantPyme)); // TODO: Debug
					if(tipoCartera.equals("P")){
						if((tmpPyme-cantPyme) >= 0){
							indCumplidos.put("carteraPyme", carteraPyme  );
							indNoCumplidos.put("carteraPyme", ""         );
						} else{
							indCumplidos.put("carteraPyme", ""           );
							indNoCumplidos.put("carteraPyme", carteraPyme);
						}
					} else if(tipoCartera.equals("M")){
						//System.out.println("(tmpPyme-cantMc) = " + (tmpPyme-cantMc)); // TODO: Debug
						if((tmpPyme-cantMc) >= 0){
							indCumplidos.put("carteraPyme", carteraPyme  );
							indNoCumplidos.put("carteraPyme", ""         );
						} else{
							indCumplidos.put("carteraPyme", ""           );
							indNoCumplidos.put("carteraPyme", carteraPyme);
						}
					}
				}
				if(!microcredito.equals("")){
					tmpMc = Double.parseDouble(microcredito);
					//System.out.println("(tmpMc-cantPyme) = " + (tmpMc-cantPyme)); // TODO: Debug
					if(tipoCartera.equals("P")){
						if((tmpMc-cantPyme) >= 0){
							indCumplidos.put("microcredito", microcredito  );
							indNoCumplidos.put("microcredito", ""         );
						} else{
							indCumplidos.put("microcredito", ""           );
							indNoCumplidos.put("microcredito", microcredito);
						}
					} else if(tipoCartera.equals("M")){
						//System.out.println("(tmpMc-cantMc) = " + (tmpMc-cantMc)); // TODO: Debug
						if((tmpMc-cantMc) >= 0){
							indCumplidos.put("microcredito", microcredito  );
							indNoCumplidos.put("microcredito", ""          );
						} else{
							indCumplidos.put("microcredito", ""            );
							indNoCumplidos.put("microcredito", microcredito);
						}
					}
				}

				while(rsAux.next()){

					elementoCedula = rsAux.getInt("IC_ELEMENTO_CEDULA");
					csViable = rsAux.getString("CS_VIABLE");

					if(elementoCedula == 1){

						if( !Comunes.esVacio(capitalizacion) ){
							capitalizacion = Comunes.formatoDecimal(capitalizacion,2,true) + "%";
						}
						if(csViable.equals("S")){
							indCumplidos.put("capitalizacion",      capitalizacion    );
							indNoCumplidos.put("capitalizacion",    ""                );
						} else{
							indCumplidos.put("capitalizacion",      ""                );
							indNoCumplidos.put("capitalizacion",    capitalizacion    );
						}

					} else if(elementoCedula == 2){

						if( !Comunes.esVacio(morosidad) ){
							morosidad = Comunes.formatoDecimal(morosidad,2,true) + "%";
						}
						if(csViable.equals("S")){
							indCumplidos.put("morosidad",           morosidad         );
							indNoCumplidos.put("morosidad",         ""                );
						} else{
							indCumplidos.put("morosidad",           ""                );
							indNoCumplidos.put("morosidad",         morosidad         );
						}

					} else if(elementoCedula == 3){

						if( !Comunes.esVacio(coberturaReservas) ){
							coberturaReservas = Comunes.formatoDecimal(coberturaReservas,2,true) + "%";
						}
						if(csViable.equals("S")){
							indCumplidos.put("coberturaReservas",   coberturaReservas );
							indNoCumplidos.put("coberturaReservas", ""                );
						} else{
							indCumplidos.put("coberturaReservas",   ""                );
							indNoCumplidos.put("coberturaReservas", coberturaReservas );
						}

					} else if(elementoCedula == 4){

						if( !Comunes.esVacio(roe) ){
							roe = Comunes.formatoDecimal(roe,2,true) + "%";
						}
						if(csViable.equals("S")){
							indCumplidos.put("roe",                 roe               );
							indNoCumplidos.put("roe",               ""                );
						} else{
							indCumplidos.put("roe",                 ""                );
							indNoCumplidos.put("roe",               ""                );
						}

					} else if(elementoCedula == 5){

						if(csViable.equals("S")){
							indCumplidos.put("liquidez",            liquidez          );
							indNoCumplidos.put("liquidez",          ""                );
						} else{
							indCumplidos.put("liquidez",            ""                );
							indNoCumplidos.put("liquidez",          ""                );
						}

					} else if(elementoCedula == 6){
						if(csViable.equals("S")){
							indCumplidos.put("apalancamiento",          apalancamiento);
							indNoCumplidos.put("apalancamiento",        ""            );
						} else{
							indCumplidos.put("apalancamiento",          ""            );
							indNoCumplidos.put("apalancamiento",        ""            );
						}
/*
					} else if(elementoCedula == 7){

						if(csViable.equals("S")){
							indCumplidos.put("carteraPyme",             carteraPyme   );
							indNoCumplidos.put("carteraPyme",           ""            );
						} else{
							indCumplidos.put("carteraPyme",             ""            );
							indNoCumplidos.put("carteraPyme",           carteraPyme   );
						}

					} else if(elementoCedula == 8){

						if(csViable.equals("S")){
							indCumplidos.put("microcredito",            microcredito  );
							indNoCumplidos.put("microcredito",          ""            );
						} else{
							indCumplidos.put("microcredito",            ""            );
							indNoCumplidos.put("microcredito",          microcredito  );
						}
*/
					} else if(elementoCedula == 9){

						if(csViable.equals("S")){
							indCumplidos.put("concentracionPerMoral",   concentracionPerMoral );
							indNoCumplidos.put("concentracionPerMoral", ""                    );
						} else{
							indCumplidos.put("concentracionPerMoral",   ""                    );
							indNoCumplidos.put("concentracionPerMoral", concentracionPerMoral );
						}

					} else if(elementoCedula == 10){

						if(csViable.equals("S")){
							indCumplidos.put("concentracionPerFisica",   concentracionPerFisica);
							indNoCumplidos.put("concentracionPerFisica", ""                    );
						} else{
							indCumplidos.put("concentracionPerFisica",   ""                    );
							indNoCumplidos.put("concentracionPerFisica", concentracionPerFisica);
						}

					} else if(elementoCedula == 11){

						if(csViable.equals("S")){
							indCumplidos.put("concentracionCartVigente",   concentracionCartVigente);
							indNoCumplidos.put("concentracionCartVigente", ""                      );
						} else{
							indCumplidos.put("concentracionCartVigente",   ""                      );
							indNoCumplidos.put("concentracionCartVigente", concentracionCartVigente);
						}

					}
				}
				listaIndCumplidos.add(indCumplidos);
				listaIndNoCumplidos.add(indNoCumplidos);
			}

			// Llenar tabla
			writer = documentoXLSX.creaHoja();

			documentoXLSX.setLongitudesColumna(new String[]{
			"63.42578125",
			"20.7109375",
			"14.7109375",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812"
			});

			// Definir el Encabezado PROGRAMA
			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda = 0;
			documentoXLSX.agregaCelda(indiceCelda++,"Fecha Reporte: " + Fecha.getFechaActual());
			documentoXLSX.finalizaRenglon();

			// Definir cabecera de la tabla de datos
			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda = 0;
			documentoXLSX.agregaCelda( indiceCelda++,"Intermediario Financiero",                       "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"R.F.C.\nEmpresarial",                            "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Folio\nSolicitud",                               "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Fecha de Registro",                              "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Capitalizaci�n",                                 "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Morosidad",                                      "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Cobertura de \nReservas",                        "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"ROE",                                            "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Liquidez",                                       "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Apalancamiento",                                 "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Capital Contable Pyme",                          "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Capital Contable Microcr�dito",                  "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Concentraci�n de \nAcreditado Persona \nMoral",  "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Concentraci�n de \nAcreditado Persona \nF�sica", "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Concentraci�n de \nCarta Vigente",               "HEADER" );
			documentoXLSX.finalizaRenglon();
			if(listaIndCumplidos.size() > 0){
				for(int i = 0; i < listaIndCumplidos.size(); i++){

					indCumplidos = new HashMap<String, String>();
					indCumplidos = (HashMap) listaIndCumplidos.get(i);

					documentoXLSX.agregaRenglon(indiceRenglon++);
					indiceCelda = 0;
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("intermediarioFinanciero"),    "CSTRING"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("rfc"),                        "CSTRING"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("folioSolicitud"),             "CENTERCSTRING" );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("fechaHoraRegistro"),          "CENTERCSTRING" );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("capitalizacion"),             "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("morosidad"),                  "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("coberturaReservas"),          "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("roe"),                        "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("liquidez"),                   "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("apalancamiento"),             "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("carteraPyme"),                "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("microcredito"),               "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("concentracionPerMoral"),      "RIGHTCSTRING"  );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("concentracionPerFisica"),     "RIGHTCSTRING"  );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indCumplidos.get("concentracionCartVigente"),   "RIGHTCSTRING"  );
					documentoXLSX.finalizaRenglon();

					if( ctaRegistros % 250 == 0 ){
						documentoXLSX.flush();
						ctaRegistros = 1;
					}

					ctaRegistros++;
					hayDatos = true;
				}
			}
			// Finalizar Hoja
			documentoXLSX.finalizaHoja();

			// Llena la segunda hoja
			documentoXLSX.creaHoja();

			documentoXLSX.setLongitudesColumna(new String[]{
			"63.42578125",
			"20.7109375",
			"14.7109375",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812",
			"20.4257812"
			});

			// Definir el Encabezado PROGRAMA
			indiceRenglon = 0;
			ctaRegistros  = 0;
			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda   = 0;
			documentoXLSX.agregaCelda(indiceCelda++,"Fecha Reporte: " + Fecha.getFechaActual());
			documentoXLSX.finalizaRenglon();

			// Definir cabecera de la tabla de datos
			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda = 0;
			documentoXLSX.agregaCelda( indiceCelda++,"Intermediario Financiero",                       "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"R.F.C.\nEmpresarial",                            "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Folio\nSolicitud",                               "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Fecha de Registro",                              "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Capitalizaci�n",                                 "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Morosidad",                                      "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Cobertura de \nReservas",                        "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"ROE",                                            "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Liquidez",                                       "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Apalancamiento",                                 "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Capital Contable Pyme",                          "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Capital Contable Microcr�dito",                  "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Concentraci�n de \nAcreditado Persona \nMoral",  "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Concentraci�n de \nAcreditado Persona \nF�sica", "HEADER" );
			documentoXLSX.agregaCelda( indiceCelda++,"Concentraci�n de \nCarta Vigente",               "HEADER" );
			documentoXLSX.finalizaRenglon();
			if(listaIndNoCumplidos.size() > 0){
				for(int i = 0; i < listaIndNoCumplidos.size(); i++){

					indNoCumplidos = new HashMap<String, String>();
					indNoCumplidos = (HashMap) listaIndNoCumplidos.get(i);

					documentoXLSX.agregaRenglon(indiceRenglon++);
					indiceCelda = 0;
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("intermediarioFinanciero"),    "CSTRING"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("rfc"),                        "CSTRING"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("folioSolicitud"),             "CENTERCSTRING" );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("fechaHoraRegistro"),          "CENTERCSTRING" );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("capitalizacion"),             "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("morosidad"),                  "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("coberturaReservas"),          "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("roe"),                        "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("liquidez"),                   "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("apalancamiento"),             "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("carteraPyme"),                "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("microcredito"),               "PERCENT"       );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("concentracionPerMoral"),      "RIGHTCSTRING"  );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("concentracionPerFisica"),     "RIGHTCSTRING"  );
					documentoXLSX.agregaCelda(indiceCelda++, (String)indNoCumplidos.get("concentracionCartVigente"),   "RIGHTCSTRING"  );
					documentoXLSX.finalizaRenglon();

					if( ctaRegistros % 250 == 0 ){
						documentoXLSX.flush();
						ctaRegistros = 1;
					}

					ctaRegistros++;
					hayDatos = true;
				}
			}
			// Finalizar Hoja
			documentoXLSX.finalizaHoja();

			// No hay registros
			if(!hayDatos){

				documentoXLSX.agregaRenglon(indiceRenglon++);
				indiceCelda = 0;
				documentoXLSX.agregaCelda(indiceCelda++,"No se encontr� ning�n registro.","HEADER",NUMERO_COLUMNAS);
				documentoXLSX.finalizaRenglon();
				// Finalizar Hoja
				documentoXLSX.finalizaHoja();

			}

			// Terminar Documento: Crear en directorioTemporal archivo XLSX con la hoja
			// generada usando una plantilla: 39CumplimientoIndicadores.template.xlsx
			nombreArchivo = documentoXLSX.finalizaDocumento( this.directorioPlantillaXlsx + "39CumplimientoIndicadores.template.xlsx" );

		} catch(OutOfMemoryError om) { // Se acabo la memoria

			log.error(
				"generaReporteCumplimientoIndicadores(OutOfMemoryError): Se acab� la memoria.\n" +
				"generaReporteCumplimientoIndicadores.request            = <" + request                           + ">\n"  +
				"generaReporteCumplimientoIndicadores.directorioTemporal = <" + directorioTemporal                + ">\n"  +
				"generaReporteCumplimientoIndicadores.tipo               = <" + tipo                              + ">\n"  +
				"generaReporteCumplimientoIndicadores.qrySentencia       = <" + qrySentencia                      + ">\n"  +
				"generaReporteCumplimientoIndicadores.Free Memory        = <" + Runtime.getRuntime().freeMemory() + ">",
				om
			);

			throw new AppException("Se acab� la memoria.");

		} catch(Exception e) {

			log.error(
				"generaReporteCumplimientoIndicadores(Exception): Error en la generacion del Archivo.\n" +
				"generaReporteCumplimientoIndicadores.request            = <" + request            + ">\n"  +
				"generaReporteCumplimientoIndicadores.directorioTemporal = <" + directorioTemporal + ">\n"  +
				"generaReporteCumplimientoIndicadores.qrySentencia       = <" + qrySentencia       + ">\n"  +
				"generaReporteCumplimientoIndicadores.tipo               = <" + tipo               + ">",
				e
			);
			throw new AppException(e);

		} finally {

			if( writer != null ){ try { writer.close(); }catch(Exception e){} }

			if(rs != null)    { try { rs.close();   }catch(Exception e){} }
			if(rsAux != null) { try { rsAux.close();}catch(Exception e){} }
			if(ps != null)    { try { ps.close();   }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.debug("generaReporteCumplimientoIndicadores(S)");

		}

		return nombreArchivo;

	}

	/**
	 * Realiza las consultas necesarias para obtener los datos del reporte "Informaci�n Cualitativa"
	 * debido a que son campos din�micos, se requieren de varias consultas.
	 * @return Lista con los campos a mostrar en el archivo xls
	 */
	public List<HashMap> consultaInformacionCualitativa(){

		log.debug("consultaInformacionCualitativa(E)");

		AccesoDB          con             = new AccesoDB();
		ResultSet         rs              = null;
		PreparedStatement ps              = null;
		StringBuffer      qryPrincipal    = null;

		HashMap<String, String>  datosTabla          = new HashMap<String, String>();
		HashMap<String, String>  datosArchivo        = new HashMap<String, String>();
		HashMap<Integer, String> respDestinoRecursos = new HashMap<Integer, String>();
		HashMap<Integer, String> respSectores        = new HashMap<Integer, String>();
		HashMap<Integer, String> respMetodologia     = new HashMap<Integer, String>();
		List<HashMap>            listaTabla          = new ArrayList<HashMap>();
		List<HashMap>            listaArchvo         = new ArrayList<HashMap>();

		int respuestaParametrizada = 0;
		String opcionRespuesta     = "";
		String respuestaLibre      = "";
		String icSolicitud         = "";

		boolean hayRazonSocial = !Comunes.esVacio(this.razonSocial )?true:false;
		boolean hayEjecutivo   = !Comunes.esVacio(this.ejecutivo   )?true:false;
		boolean hayEstatus     = !Comunes.esVacio(this.estatus     )?true:false;
		boolean hayNumFolio    = !Comunes.esVacio(this.numFolio    )?true:false;
		boolean hayFechaInicio = !Comunes.esVacio(this.fechaInicio )?true:false;
		boolean hayFechaFin    = !Comunes.esVacio(this.fechaFin    )?true:false;

		try{

			// Conectarse a la Base de Datos
			con.conexionDB();

			/***** INICIO: Obtengo los encabezados de la tabla que se armar� en el excel  ******/
			datosArchivo.put("INTERMEDIARIO_FINANCIERO", "Intermediario Financiero");
			datosArchivo.put("RFC_EMPRESARIAL",          "RFC \nEmpresarial");
			datosArchivo.put("FOLIO_SOLICITUD",          "Folio \nSolicitud");
			datosArchivo.put("FECHA_REGISTRO",           "Fecha de Registro");
			// cedi_pregunta.ic_pregunta = 4 Destino de los recursos
			rs = null;
			ps = null;
			ps = con.queryPrecompilado(getRespuestaParametrizacion());
			ps.setInt(1,4);
			rs = ps.executeQuery();
			while(rs.next()){
				respuestaParametrizada = rs.getInt("IC_RESPUESTA_PARAMETRIZACION");
				opcionRespuesta        = (rs.getString("CG_OPCION_RESPUESTA")  == null)?"":rs.getString("CG_OPCION_RESPUESTA");
				if(respuestaParametrizada > 0 && !opcionRespuesta.equals("")){
					respDestinoRecursos.put(respuestaParametrizada, opcionRespuesta);
					datosArchivo.put(""+respuestaParametrizada, opcionRespuesta);
				}
			}
			System.out.println("ABR_respDestinoRecurso: " + respDestinoRecursos.values());

			// cedi_pregunta.ic_pregunta = 3 Sectores que atiende por porcentaje de sus cartera
			rs = null;
			ps = null;
			ps = con.queryPrecompilado(getRespuestaParametrizacion());
			ps.setInt(1,3);
			rs = ps.executeQuery();
			while(rs.next()){
				respuestaParametrizada = rs.getInt("IC_RESPUESTA_PARAMETRIZACION");
				opcionRespuesta        = (rs.getString("CG_OPCION_RESPUESTA")  == null)?"":rs.getString("CG_OPCION_RESPUESTA");
				if(respuestaParametrizada > 0 && !opcionRespuesta.equals("")){
					respSectores.put(respuestaParametrizada, opcionRespuesta);
					datosArchivo.put(""+respuestaParametrizada, opcionRespuesta);
				}
			}
			System.out.println("ABR_respSectores: " + respSectores.values());

			// cedi_pregunta.ic_pregunta = 15 Metodologia de calificacion de cartera
			rs = null;
			ps = null;
			ps = con.queryPrecompilado(getRespuestaParametrizacion());
			ps.setInt(1,15);
			rs = ps.executeQuery();
			while(rs.next()){
				respuestaParametrizada = rs.getInt("IC_RESPUESTA_PARAMETRIZACION");
				opcionRespuesta        = (rs.getString("CG_OPCION_RESPUESTA")  == null)?"":rs.getString("CG_OPCION_RESPUESTA");
				if(respuestaParametrizada > 0 && !opcionRespuesta.equals("")){
					respMetodologia.put(respuestaParametrizada, opcionRespuesta);
					datosArchivo.put(""+respuestaParametrizada, opcionRespuesta);
				}
			}
			System.out.println("ABR_respMetodologia: " + respMetodologia.values());
			listaArchvo.add(datosArchivo); // El primer elemento de la lista debe ser el encabezado
			System.out.println("ABR_listaArchvo: " + listaArchvo.toString());
			/***** FIN: Obtengo los encabezados de la tabla que se armar� en el excel  ******/

			/***** INICIO: QUERY CON LOS DATOS PRINCIPALES *****/
			qryPrincipal = new StringBuffer();

			qryPrincipal.append("SELECT CG_RAZON_SOCIAL, CG_RFC, S.IC_SOLICITUD,                   \n");
			qryPrincipal.append("       (SELECT TO_CHAR(MAX(DF_MOVIMIENTO),'DD/MM/YYYY HH24:MI:SS')\n");
			qryPrincipal.append("          FROM CEDI_SOLICITUD_MOVIMIENTO                          \n");
			qryPrincipal.append("         WHERE IC_ESTATUS_SOLICITUD IN(2, 7)                      \n");
			qryPrincipal.append("           AND IC_SOLICITUD = S.IC_SOLICITUD) AS FECHA_REGISTRO   \n");
			qryPrincipal.append("  FROM CEDI_IFNB_ASPIRANTE IA, CEDI_SOLICITUD S                   \n");
			qryPrincipal.append(" WHERE IA.IC_IFNB_ASPIRANTE = S.IC_IFNB_ASPIRANTE                 \n");

			//CRITERIO RAZON SOCIAL
			if(hayRazonSocial){
				qryPrincipal.append("   AND S.IC_IFNB_ASPIRANTE = ?                                 \n");
			}
			//CRITERIO EJECUTIVO CEDI
			if(hayEjecutivo){
				qryPrincipal.append("   AND S.CG_USUARIO_ASIGNADO = ?                               \n");
			}
			//CRITERIO ESTATUS
			if(hayEstatus){
				qryPrincipal.append("   AND S.IC_ESTATUS_SOLICITUD = ?                              \n");
			}
			//CRITERIO FOLIO SOLICITUD
			if(hayNumFolio){
				qryPrincipal.append("   AND S.IC_SOLICITUD = ?                                      \n");
			}
			//CRITERIO FECHA SOLICITUD
			if(hayFechaInicio && hayFechaFin){
				qryPrincipal.append("   AND S.IC_SOLICITUD IN (                                     \n");
				qryPrincipal.append("         SELECT IC_SOLICITUD                                   \n");
				qryPrincipal.append("           FROM CEDI_SOLICITUD_MOVIMIENTO                      \n");
				qryPrincipal.append("          WHERE IC_ESTATUS_SOLICITUD IN(2, 7)                  \n");
				qryPrincipal.append("       GROUP BY IC_SOLICITUD                                   \n");
				qryPrincipal.append("         HAVING MAX(DF_MOVIMIENTO) >= TO_DATE(?,'DD/MM/YYYY')  \n");
				qryPrincipal.append("            AND MAX(DF_MOVIMIENTO) < TO_DATE(?,'DD/MM/YYYY')+1)\n");
			}

			// AGREGAR CONDICIONES DEL QUERY
			ps = con.queryPrecompilado(qryPrincipal.toString());
			int idx = 1;
			// Criterio Razon Social
			if(hayRazonSocial){
				ps.setString(idx++,this.razonSocial);
			}
			// Criterio Ejecutivo CEDI
			if(hayEjecutivo){
				ps.setString(idx++,this.ejecutivo);
			}
			// Criterio Estatus
			if(hayEstatus){
				ps.setString(idx++,this.estatus);
			}
			// Criterio Folio Solicitud
			if(hayNumFolio){
				ps.setString(idx++,this.numFolio);
			}
			// Criterio Fecha Solicitud: Inicio 
			if(hayFechaInicio){
				ps.setString(idx++,this.fechaInicio);
			}
			// Criterio Fecha Solicitud:  Fin
			if(hayFechaFin){
				ps.setString(idx++,this.fechaFin);
			}
			rs = ps.executeQuery();

			while(rs.next()){
				datosTabla = new HashMap<String, String>();
				datosTabla.put("CG_RAZON_SOCIAL", (String)((rs.getString("CG_RAZON_SOCIAL") == null)?"":rs.getString("CG_RAZON_SOCIAL")));
				datosTabla.put("CG_RFC",          (String)((rs.getString("CG_RFC")          == null)?"":rs.getString("CG_RFC")         ));
				datosTabla.put("IC_SOLICITUD",    (String)((rs.getString("IC_SOLICITUD")    == null)?"":rs.getString("IC_SOLICITUD")   ));
				datosTabla.put("FECHA_REGISTRO",  (String)((rs.getString("FECHA_REGISTRO")  == null)?"":rs.getString("FECHA_REGISTRO") ));
				listaTabla.add(datosTabla);
			}
			/***** FIN: QUERY CON LOS DATOS PRINCIPALES *****/

			/***** INICIO: LEO LA LISTA Y OBTENGO LAS RESPUESTAS POR SOLICITUD PARA ARMAR LA LISTA FINAL *****/
			String claveTmp = "";
			int contador = 0;

			for(int i=0; i<listaTabla.size(); i++){

				datosArchivo = new HashMap<String, String>();
				datosTabla = new HashMap<String, String>();
				datosTabla = (HashMap<String, String>) listaTabla.get(i);
				icSolicitud = (String)datosTabla.get("IC_SOLICITUD");
				System.out.println("ABR_icSolicitud: " + icSolicitud);

				//Asigno los valores finales
				datosArchivo.put("INTERMEDIARIO_FINANCIERO", (String)datosTabla.get("CG_RAZON_SOCIAL"));
				datosArchivo.put("RFC_EMPRESARIAL",          (String)datosTabla.get("CG_RFC"));
				datosArchivo.put("FOLIO_SOLICITUD",          (String)datosTabla.get("IC_SOLICITUD"));
				datosArchivo.put("FECHA_REGISTRO",           (String)datosTabla.get("FECHA_REGISTRO"));

				// Pregunta = 4
				ps = null;
				rs = null;
				ps = con.queryPrecompilado(getRespuestasSeleccionadas());
				ps.setInt(1,4);
				ps.setInt(2,Integer.parseInt(icSolicitud));
				rs = ps.executeQuery();
				while(rs.next()){
					contador++;
					respuestaParametrizada = rs.getInt("IC_RESPUESTA_PARAMETRIZACION");
					respuestaLibre = (rs.getString("CG_RESPUESTA_LIBRE")  == null)?"":rs.getString("CG_RESPUESTA_LIBRE");
					Iterator itD = respDestinoRecursos.entrySet().iterator();
					while(itD.hasNext()){
						Map.Entry e = (Map.Entry)itD.next();
						claveTmp = ""+e.getKey();
						if(claveTmp.equals(""+respuestaParametrizada)){
							if( !Comunes.esVacio(respuestaLibre) ){
								respuestaLibre = Comunes.formatoDecimal(respuestaLibre,2,false) + "%";
							}
							datosArchivo.put(claveTmp, respuestaLibre);
						}
					}
				}
				if(contador == 0){
					Iterator itD = respDestinoRecursos.entrySet().iterator();
					while(itD.hasNext()){
						Map.Entry e = (Map.Entry)itD.next();
						claveTmp = ""+e.getKey();
						datosArchivo.put(claveTmp, " ");
					}
				}
				// Pregunta = 3
				contador = 0;
				claveTmp = "";
				ps = null;
				rs = null;
				ps = con.queryPrecompilado(getRespuestasSeleccionadas());
				ps.setInt(1,3);
				ps.setInt(2,Integer.parseInt(icSolicitud));
				rs = ps.executeQuery();
				while(rs.next()){
					contador++;
					respuestaParametrizada = rs.getInt("IC_RESPUESTA_PARAMETRIZACION");
					respuestaLibre = (rs.getString("CG_RESPUESTA_LIBRE")  == null)?"":rs.getString("CG_RESPUESTA_LIBRE");
					Iterator itS = respSectores.entrySet().iterator();
					while(itS.hasNext()){
						Map.Entry e = (Map.Entry)itS.next();
						claveTmp = ""+e.getKey();
						if(claveTmp.equals(""+respuestaParametrizada)){
							if( !Comunes.esVacio(respuestaLibre) ){
								respuestaLibre = Comunes.formatoDecimal(respuestaLibre,2,false) + "%";
							}
							datosArchivo.put(claveTmp, respuestaLibre);
						}
					}
				}
				if(contador == 0){
					Iterator itS = respSectores.entrySet().iterator();
					while(itS.hasNext()){
						Map.Entry e = (Map.Entry)itS.next();
						claveTmp = ""+e.getKey();
						datosArchivo.put(claveTmp, " ");
					}
				}
				// Pregunta = 15
				contador = 0;
				claveTmp = "";
				ps = null;
				rs = null;
				ps = con.queryPrecompilado(getRespuestasSeleccionadas());
				ps.setInt(1,15);
				ps.setInt(2,Integer.parseInt(icSolicitud));
				rs = ps.executeQuery();
				while(rs.next()){
					contador++;
					respuestaParametrizada = rs.getInt("IC_RESPUESTA_PARAMETRIZACION");
					Iterator itM = respMetodologia.entrySet().iterator();
					while(itM.hasNext()){
						Map.Entry e = (Map.Entry)itM.next();
						claveTmp = ""+e.getKey();
						if(claveTmp.equals(""+respuestaParametrizada)){
							datosArchivo.put(claveTmp, "X");
						}
					}
				}
				if(contador == 0){
					Iterator itM = respMetodologia.entrySet().iterator();
					while(itM.hasNext()){
						Map.Entry e = (Map.Entry)itM.next();
						claveTmp = ""+e.getKey();
						datosArchivo.put(claveTmp, " ");
					}
				}
				//System.out.println("ABR_datosArchivo: " + datosArchivo.values());
				listaArchvo.add(datosArchivo);
			}
			//System.out.println("ABR_listaArchvo: " + listaArchvo.toString());
			/***** FIN: LEO LA LISTA Y OBTENGO LAS RESPUESTAS POR SOLICITUD PARA ARMAR LA LISTA FINAL *****/

		}catch(Exception e){

			log.error("consultaInformacionCualitativa(Exception): Error al generar las consultas.\n" + e);
			throw new AppException(e);

		} finally{

			if(rs != null)    { try { rs.close();   }catch(Exception e){} }
			if(ps != null)    { try { ps.close();   }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.debug("consultaInformacionCualitativa(S)");

		}

		return listaArchvo;
	}

	/**
	 * Obtiene los identificadores de los headers ya que los campos son dinamicos,
	 * este m�todo me ayuda a saber cuales columnas y en que orden se mostraran en el archivo excel
	 * @return Lista con los campos din�micos que se mostrar�n en el reporte de "Informaci�n Cualitativa"
	 */
	public List<Integer> getEncabezados(){

		log.debug("getEncabezados(E)");

		List<Integer> encabezados = new ArrayList<Integer>();

		AccesoDB          con             = new AccesoDB();
		ResultSet         rs              = null;
		PreparedStatement ps              = null;
		try{
			// Conectarse a la Base de Datos
			con.conexionDB();

			//Destino de los recursos
			ps = con.queryPrecompilado(getRespuestaParametrizacion());
			ps.setInt(1,4);
			rs = ps.executeQuery();
			while(rs.next()){
				encabezados.add(rs.getInt("IC_RESPUESTA_PARAMETRIZACION"));
				contadorDestinoRec++;
			}

			//Sectores que atiende por porcentaje de su cartera
			rs = null;
			ps = null;
			ps = con.queryPrecompilado(getRespuestaParametrizacion());
			ps.setInt(1,3);
			rs = ps.executeQuery();
			while(rs.next()){
				encabezados.add(rs.getInt("IC_RESPUESTA_PARAMETRIZACION"));
				contadorSectores++;
			}

			//Metodolog�a de calificaci�n de cartera
			rs = null;
			ps = null;
			ps = con.queryPrecompilado(getRespuestaParametrizacion());
			ps.setInt(1,15);
			rs = ps.executeQuery();
			while(rs.next()){
				encabezados.add(rs.getInt("IC_RESPUESTA_PARAMETRIZACION"));
				contadorMetodologias++;
			}


		} catch(Exception e){
			log.error("getEncabezados(Exception): Error al obtener los headers del archivo.\n" + e);
			throw new AppException(e);
		} finally{
			if(rs != null)    { try { rs.close();   }catch(Exception e){} }
			if(ps != null)    { try { ps.close();   }catch(Exception e){} }
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.debug("getEncabezados(S)");
		}
		return encabezados;
	}

	/*****************************************************
	 *                GETTERS AND SETTERS                *
	 *****************************************************/
	public String getRazonSocial(){
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial){
		this.razonSocial = razonSocial;
	}

	public String getEjecutivo(){
		return ejecutivo;
	}

	public void setEjecutivo(String ejecutivo){
		this.ejecutivo = ejecutivo;
	}

	public String getEstatus(){
		return estatus;
	}

	public void setEstatus(String estatus){
		this.estatus = estatus;
	}

	public String getNumFolio(){
		return numFolio;
	}

	public void setNumFolio(String numFolio){
		this.numFolio = numFolio;
	}

	public String getFechaInicio(){
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio){
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin(){
		return fechaFin;
	}

	public void setFechaFin(String fechaFin){
		this.fechaFin = fechaFin;
	}

	public String getInfoIf(){
		return infoIf;
	}

	public void setInfoIf(String infoIf){
		this.infoIf = infoIf;
	}
	
	public String getDirectorioPlantillaXlsx(){
		return directorioPlantillaXlsx;
	}

	public void setDirectorioPlantillaXlsx(String directorioPlantillaXlsx){
		this.directorioPlantillaXlsx = directorioPlantillaXlsx;
	}

	public String getRespuestaParametrizacion(){
		String query =
		"SELECT IC_RESPUESTA_PARAMETRIZACION, CG_OPCION_RESPUESTA \n" + 
		"  FROM CEDI_RESPUESTA_PARAMETRIZACION \n" + 
		" WHERE IC_PREGUNTA = ?";
		return query;
	}

	public String getRespuestasSeleccionadas(){
		String query = 
		"SELECT IC_SOLICITUD, CG_RESPUESTA_LIBRE, IC_RESPUESTA_PARAMETRIZACION \n" + 
		"  FROM CEDI_RESPUESTA_IFNB \n" + 
		" WHERE IC_RESPUESTA_PARAMETRIZACION IN (SELECT IC_RESPUESTA_PARAMETRIZACION \n" + 
		"                                          FROM CEDI_RESPUESTA_PARAMETRIZACION \n" + 
		"                                         WHERE IC_PREGUNTA = ?) \n" + 
		"   AND IC_SOLICITUD = ?";
		return query;
	}

}
