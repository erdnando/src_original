package com.netro.cedi;

import com.netro.cadenas.ConIntermediarios;
import com.netro.pdf.ComunesPDF;

import com.sun.org.apache.xpath.internal.operations.And;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Blob;

import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;
 

public class ConsAtencionSeguimiento  implements IQueryGeneratorRegExtJS    {
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsAtencionSeguimiento.class);
	
	private String    paginaOffset;
	private String    paginaNo;
	private List      conditions;
	StringBuffer qrySentencia   = new StringBuffer();
	
	private String ic_intermediario;
	private String txtFolioSolicitud;
	private String txtFechaSoliIni;
	private String txtFechaSoliFinal;
	private String tipoConsulta;
	private String tieneDoctos;	
	private String rfc_empresarial;
	private String razon_social;
	private String ejecutivoAsignado;
	
	
	
	public ConsAtencionSeguimiento() {}

	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
		
		
	public String getDocumentSummaryQueryForIds(List ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}
	
		
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
		
	
		
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
	   if (tipoConsulta.equals("Consultar")   ){
			
			qrySentencia.append( " SELECT distinct s.IC_SOLICITUD as NO_SOLICITUD , "+
										" a.IC_IFNB_ASPIRANTE as IC_IFNB,   "+
										"  a.CG_RAZON_SOCIAL as RAZON_SOCIAL , "+
										"  a.CG_RFC as RFC_EMPRESARIAL ,  "+
										"  to_char(s.DF_ALTA,'DD/MM/YYYY HH24:MI:SS') as FECHA_HORA_REG ,  "+
										" cg_calle ||'-'|| cg_colonia  as  DOMICILIO,  "+
									   " es.cd_nombre  as ESTADO,  "+
									   " a.cg_cp as  CODIGO_POSTAL,  "+
									   " a.cg_contacto as  CONTACTO,  "+
									   " a.cg_email as  EMAIL,  "+
									   " a.cg_telefono as  TELEFONO,  "+
										
										"  e.CG_NOMBRE as ESTATUS , "+
										"  e.IC_ESTATUS_SOLICITUD as IC_ESTATUS ,  "+
										"  s.CG_USUARIO_ASIGNADO AS LOGIN_EJECUTIVO , " + 
										"  '' AS EJECUTIVO_CEDI,  "+
										"  '' AS MANUAL_CREDITO, "+
										"  '' AS  PRESENTACION_CORPO  , "+
										"  '' AS DESGLOSE_PASIVO , "+
										"  '' AS CARTERA_VENCIDA , "+
										"  '' AS PORTAFOLIO , "+
										"  '' AS EXTENSION   , "+
										"  '' AS ICONO , "+	
										"  s.cs_dictamen_confirmado  AS dictamen," +
										" LENGTH (BI_BALANCE)  as LON_BALANCE ,  "+
										" LENGTH (BI_EDO_RESULTADOS)  as LON_RESULTADOS ,  "+
										" LENGTH (BI_RFC)  as LON_RFC ,  "+ 
		                        " LENGTH (BI_CEDULA_COMPLETA) as LON_CEDULA_COMPLETA "+     
										
										"  FROM  cedi_solicitud s, CEDI_IFNB_ASPIRANTE  a , CEDICAT_ESTATUS_SOLICITUD e,  "+
										" comcat_estado   es , "+  
										" CEDIHIS_ASIGNACION_SOLICITUD  ea "+
										
										"  where  s.IC_IFNB_ASPIRANTE = a.IC_IFNB_ASPIRANTE  "+
										"  and s.IC_ESTATUS_SOLICITUD =   e.IC_ESTATUS_SOLICITUD   " +
										"  and s.IC_ESTATUS_SOLICITUD = ?    "+
										"  and es.ic_estado = a.ic_estado  "+
										"  AND S.IC_SOLICITUD = ea.IC_SOLICITUD ");   
										conditions.add("5");  
			
			
		   if(!ejecutivoAsignado.equals("")){
		      qrySentencia.append("AND  s.CG_USUARIO_ASIGNADO =  ? ");   
		      conditions.add(ejecutivoAsignado);         
		   }
			
	    
			if(!ic_intermediario.equals("")){
				qrySentencia.append("AND  s.IC_IFNB_ASPIRANTE =  ? ");   
				conditions.add(ic_intermediario);         
			}
			
		  
				
			if(!txtFolioSolicitud.equals("")){       
				qrySentencia.append("AND s.IC_SOLICITUD =  ? ");   
				conditions.add(txtFolioSolicitud);			   
			}
		
			if(!txtFechaSoliIni.equals("") && !txtFechaSoliFinal.equals("")){
				qrySentencia.append(" and s.DF_ALTA >=  to_date(?, 'dd/mm/yyyy')  ") ;
				qrySentencia.append("and s.DF_ALTA < to_date(?, 'dd/mm/yyyy')+1  ") ;
				conditions.add(txtFechaSoliIni);
				conditions.add(txtFechaSoliFinal);
			
			}
		
		}else if (tipoConsulta.equals("ConsCedula")   ||  tipoConsulta.equals("ViabilidadPDF")  ){ 
			
			
		   qrySentencia.append(" SELECT   ca.IC_ELEMENTO_CEDULA  as IC_CEDULA , "+
									  " ce.IC_SOLICITUD  as NO_SOLICITUD , "+
									  " ca.CG_NOMBRE as DESCRIPCION , "+									  
									  " DECODE (CG_VIABLE_FINAL , '', (DECODE (ce.cs_viable, 'S', 'Viable', 'N', 'No Viable') ), CG_VIABLE_FINAL  )   as VIABILIDAD ,  "+
									  " DECODE (ca.ic_elemento_cedula, " +
									  " 	1, s.fg_capitalizacion_anio3  ,  " + 
									  " 	2, s.fg_morosidad_anio3," +
									  "   3, s.FG_COBERTURA_RESERVAS_ANIO3,  "+
									  "   4, s.FG_ROE_ANIO3 , "+
									  "   5, s.FG_LIQUIDEZ_ANIO3  ,  "+
									  "   6, s.FG_APALANCAMIENTO_ANIO3, "+
									  "   7, s.FG_CARTERA , "+
									  "   8, s.FG_CARTERA, "+
									  "   9, s.FG_CONC_ACRED_PM, "+
									  "   10, s.FG_CONC_ACRED_PF  , "+
									  "   11, s.FG_CONC_CART_VIG, " +
									  "   67, s.FG_CONC_ACRED_PM,  "+
									  "   68, s.FG_CONC_ACRED_PF,   "+
									  "   33, s.FG_MONTO_CRED_VINCULADO  "+
									  "   ) AS RESULTADO , "+
									  "   '' as IC_RUBRO,  "+
									  "  '' as DESCRIPCION_RUBRO ,  "+
									  "  decode(s.IG_MES_INF_FINANCIERA , 1, 'Enero', 2,'Febrero', 3,'Marzo', 4,'Abril',  5,'Mayo', 6, 'Junio', 7,'Julio', 8, 'Agosto', 9, 'Septiembre', 10, 'Octubre', 11, 'Noviembre', 12,'Diciembre' )   || '  ' || s.IG_ANIO_INF_FINANCIERA   AS MES_ANIO_FINANCIERO ,  " +
									  " s.IG_ANIO_INF_FINANCIERA  as ANIO_FINANCIERO "+
									  
									  "  FROM  CEDICAT_ELEMENTO_CEDULA  ca,  CEDI_CEDULA ce,  cedi_solicitud s "+
									  "  where  ca.IC_ELEMENTO_CEDULA = ce.IC_ELEMENTO_CEDULA "+
									  "  AND  ce.IC_SOLICITUD = s.IC_SOLICITUD "+
									  "  and  ce.IC_SOLICITUD = ?   ");
			
								 conditions.add(txtFolioSolicitud);     
					
					
		}
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}
		
		
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		log.debug("crearCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		CreaArchivo creaArchivo = new CreaArchivo();
		ComunesPDF pdfDoc = new ComunesPDF();
		
		try {
		
		   nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		   pdfDoc = new ComunesPDF(2, path + nombreArchivo); 
		   
		   String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		   String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		   String diaActual    = fechaActual.substring(0,2);
		   String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		   String anioActual   = fechaActual.substring(6,10);
		   String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		   
			
		   pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		      ((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		      (String)session.getAttribute("sesExterno"),
		      (String) session.getAttribute("strNombre"),
		      (String) session.getAttribute("strNombreUsuario"),
		      (String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));  
		   
		   pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		   
		   pdfDoc.addText("DICTAMEN DE VIABILIDAD DE INCORPORACION - FOLIO SOLICITUD:  "+ txtFolioSolicitud,"formas",ComunesPDF.CENTER);
		   pdfDoc.addText(razon_social, "formas",ComunesPDF.CENTER);
		   pdfDoc.addText("R.F.C. : "+rfc_empresarial, "formas",ComunesPDF.CENTER);
		   
		   pdfDoc.addText("\n", "formas",ComunesPDF.RIGHT); 
		 
		 
		   float[] tamanio = {60, 20,20};

		   pdfDoc.setTable( 3, 100, tamanio,0) ;
		   pdfDoc.setCell("Secci�n ","celda01",ComunesPDF.CENTER);
		   pdfDoc.setCell("Resultado C�dula Electr�nica ","celda01",ComunesPDF.CENTER);
		   pdfDoc.setCell("Resultado Final de Viabilidad ","celda01",ComunesPDF.CENTER);
		
		   String  anioFinaciero ="";
		   while (rs.next()) { 
			   int  ic_cedula = Integer.parseInt( rs.getString("IC_CEDULA") ) ;			   
			   String descripcion = (rs.getString("DESCRIPCION") == null) ? "" : rs.getString("DESCRIPCION");
			   String resultado = (rs.getString("RESULTADO") == null) ? "" : rs.getString("RESULTADO");
			   String viabiliad = (rs.getString("VIABILIDAD") == null) ? "" : rs.getString("VIABILIDAD");
			
			   String mes_anio_financiero =  rs.getString("MES_ANIO_FINANCIERO")==null?"":rs.getString("MES_ANIO_FINANCIERO");
			   int  anio_finaciero = Integer.parseInt( rs.getString("ANIO_FINANCIERO") ) ;
			  
			   				
				if(ic_cedula>=1 &  ic_cedula<=11 ) { 
				   if(ic_cedula==1  ) {
						pdfDoc.setCell("Indicadores Financieros","celda01",ComunesPDF.LEFT, 3);						   
					}
				   if(ic_cedula==1  ||  ic_cedula==2  || ic_cedula==4 || ic_cedula==9  ||  ic_cedula==10 || ic_cedula==11  ) {				      
				      resultado = Comunes.formatoDecimal(resultado,2)+"%";
				   }else  if(ic_cedula==5) {
					   resultado = resultado+" Veces";
					}else  {
					   resultado = Comunes.formatoDecimal(resultado,2);
					}
					
					pdfDoc.setCell(descripcion,"formas",ComunesPDF.LEFT);
			      pdfDoc.setCell(resultado,"formas",ComunesPDF.CENTER);
			      pdfDoc.setCell(viabiliad,"formas",ComunesPDF.CENTER);
									
				}
				
				// Informaci�n Cualitativa 
				if(ic_cedula>=12 &  ic_cedula<=32 ) { 
				  
				   if(ic_cedula==12 ) {				      
				      pdfDoc.setCell("Informaci�n Cualitativa","celda01",ComunesPDF.LEFT, 3);				   
				   }
					
				   pdfDoc.setCell(descripcion,"formas",ComunesPDF.LEFT);
				   //Monto de l�nea solicitado  IC_PREGUNTA =1 , IC_TIPO_RESPUESTA  = 1
					
					if(ic_cedula==12 ) {					   						
				     pdfDoc.setCell("$"+Comunes.formatoDecimal( this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  , "1","1" ) , 2 ) ,"formas",ComunesPDF.CENTER);   
					}
				   
					//'Tipo de empresas apoyadas IC_PREGUNTA =2 ,  IC_TIPO_RESPUESTA  = 2
				   if(ic_cedula==13)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "2","2" ),"formas",ComunesPDF.CENTER);   
					}
				   // Sector  IC_PREGUNTA =3 ,  IC_TIPO_RESPUESTA  = 2
				   if(ic_cedula==14)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "3","2" ),"formas",ComunesPDF.CENTER);  
					}
				   // Destino de recursos solicitados  IC_PREGUNTA =4 ,  IC_TIPO_RESPUESTA  = 2
				   if(ic_cedula==15)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "4","2" ) ,"formas",ComunesPDF.CENTER);  
					}
				   // �Cuenta con al menos 50 MDP de cartera en un mismo producto con destino PyME? IC_PREGUNTA =5 ,  IC_TIPO_RESPUESTA  = 3
				   if(ic_cedula==16)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "5","3" ) ,"formas",ComunesPDF.CENTER );  
				   }
				   // �Cuenta con al menos 25 MDP de cartera en un mismo producto con destino Microcr�dito? IC_PREGUNTA =6 ,  IC_TIPO_RESPUESTA  = 3
					if(ic_cedula==17)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "6","3"  ) ,"formas",ComunesPDF.CENTER );  
					}
				   // 'Experiencia en a�os en el otorgamiento de cr�dito IC_PREGUNTA =7 ,  IC_TIPO_RESPUESTA  = 3
				   if(ic_cedula==18)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "7","3" ) ,"formas",ComunesPDF.CENTER );  
					}
				   // �Realiza consultas y reportes en el C�rculo o Bur� de Cr�dito de sus acreditados? IC_PREGUNTA =8 ,  IC_TIPO_RESPUESTA  = 3
				   if(ic_cedula==19)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "8","3" ) ,"formas",ComunesPDF.CENTER );  
					}
				   // �Cuenta con instalaciones, equipos y sistemas para controlar y administrar la cartera de cr�dito? IC_PREGUNTA =9 ,  IC_TIPO_RESPUESTA  = 3
				   if(ic_cedula==20)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "9","3" ) ,"formas",ComunesPDF.CENTER ); 
				    }
				   // �Cuenta con un Manual de Cr�dito que considere todas las etapas del proceso de cr�dito: desde la promoci�n y originaci�n, hasta el seguimiento y la recuperaci�n de cr�ditos? IC_PREGUNTA =10 ,  IC_TIPO_RESPUESTA  = 3 
				   if(ic_cedula==21)  { 						
				      pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "10","3" ) ,"formas",ComunesPDF.CENTER ); 
					}
					// �Su manual contiene las caracter�sticas, criterios y metodolog�a de evaluaci�n del producto que desea NAFIN fondee? IC_PREGUNTA =11 ,  IC_TIPO_RESPUESTA  = 3
				   if(ic_cedula==22)  { 				      
					   pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "11","3" ) ,"formas",ComunesPDF.CENTER); 
					}
				   // �Cuenta con Comit� de Cr�dito con facultades claramente delimitadas? IC_PREGUNTA =12 ,  IC_TIPO_RESPUESTA  = 3
					if(ic_cedula==23)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "12","3"  ) ,"formas",ComunesPDF.CENTER); 				              
					}
				   // �Cuenta con Consejo de Administraci�n? IC_PREGUNTA =13 ,  IC_TIPO_RESPUESTA  = 2
				   if(ic_cedula==24)  { 				     
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "13","3"  ) ,"formas",ComunesPDF.CENTER);   
				   }
				   // �Cuenta con otro Comit�? IC_PREGUNTA =14 ,  IC_TIPO_RESPUESTA  = 3
				   if(ic_cedula==25)  { 						
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "14","3"  ) ,"formas",ComunesPDF.CENTER);   
					}
				   //�C�mo es su metodolog�a de calificaci�n de cartera? IC_PREGUNTA =15 ,  IC_TIPO_RESPUESTA  = 3
					if(ic_cedula==26)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "15","3"  ) ,"formas",ComunesPDF.CENTER);  
					}
				   // �El registro contable lo realiza con base en los criterios de la Comisi�n Nacional Bancario y de Valores (CNBV)? IC_PREGUNTA =16 ,  IC_TIPO_RESPUESTA  = 3
				   if(ic_cedula==27)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "16","3"  ) ,"formas",ComunesPDF.CENTER);  	
				   }
				   // �Su metodolog�a de calificaci�n de cartera y creaci�n de reservas es conforme a los criterios de la Comisi�n Nacional Bancaria y de Valores (CNBV)? IC_PREGUNTA =17 ,  IC_TIPO_RESPUESTA  = 3
				   if(ic_cedula==28)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "17","3"  ) ,"formas",ComunesPDF.CENTER); 
					}
				   // �Registra cartera vencida y traspasos conforme a los criterios de la Comisi�n Nacional Bancaria y de Valores (CNBV)?  IC_PREGUNTA =18 ,  IC_TIPO_RESPUESTA  = 3
					if(ic_cedula==29)  { 				      
					   pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "18","3"  ) ,"formas",ComunesPDF.CENTER); 
					}
				    // Da tratamiento a las renovaciones y reestructuras de sus cr�ditos otorgados conforme a los criterios de la Comisi�n Nacional Bancaria y de Valores (CNBV)? IC_PREGUNTA =19 ,  IC_TIPO_RESPUESTA  = 3
				   if(ic_cedula==30)  { 				              
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "19","3"  ) ,"formas",ComunesPDF.CENTER); 
					}
				   // �Cuenta con Fondeo de la Banca Comercial? IC_PREGUNTA =20 ,  IC_TIPO_RESPUESTA  = 3
				   if(ic_cedula==31)  { 				     
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "20","3"  ) ,"formas",ComunesPDF.CENTER); 
					}
				   // �Cuenta con Fondeo de la Banca de Desarrollo? IC_PREGUNTA =21 ,  IC_TIPO_RESPUESTA  = 3
				   if(ic_cedula==32)  { 
						pdfDoc.setCell(this.getInfoCualitativa ( rs.getString("NO_SOLICITUD")  ,  "21","3"  ) ,"formas",ComunesPDF.CENTER ); 
					}
					
					 pdfDoc.setCell(viabiliad,"formas",ComunesPDF.CENTER);
				                
				}else  if(ic_cedula>=33 &  ic_cedula<=33 ) { 
			    
					 pdfDoc.setCell("Cr�dito Vinculado","celda01",ComunesPDF.LEFT, 3);
					 pdfDoc.setCell(descripcion,"formas",ComunesPDF.LEFT);
			       pdfDoc.setCell("$"+Comunes.formatoDecimal(resultado,2),"formas",ComunesPDF.CENTER);
			       pdfDoc.setCell(viabiliad,"formas",ComunesPDF.CENTER);
			                 
				}
				
				
			   // Informaci�n Financiera
			   if(ic_cedula>=34 &  ic_cedula<=66 ) { 
					
					List info =  this.getInfoFinanciera ( rs.getString("NO_SOLICITUD")  );
				   
				   if(ic_cedula==34 ) {    
				      anioFinaciero = String.valueOf(anio_finaciero);
					}
				   if(ic_cedula==45 ) {         
				      anio_finaciero = anio_finaciero-1;
				      anioFinaciero = String.valueOf(anio_finaciero);
					}
				   if(ic_cedula==56 ) {           
				      anio_finaciero = anio_finaciero-2;
				      anioFinaciero = String.valueOf(anio_finaciero);
					}
					
				   for (int i = 0; i <info.size(); i++) {
						HashMap  datos = (HashMap)info.get(i); 
				           
				      String anio =  datos.get("IC_ANIO").toString();
				      String activosTotales =  datos.get("FG_ACTIVOS_TOTALES").toString();
				      String inversionCaja =  datos.get("FG_INVERSION_CAJA").toString();
				      String carteraVigente =  datos.get("FG_CARTERA_VIGENTE").toString();
				      String carteraVencida =  datos.get("FG_CARTERA_VENCIDA").toString();
				      String carteraTotal =  datos.get("FG_CARTERA_TOTAL").toString();
				      String estimacionReservas =  datos.get("FG_ESTIMACION_RESERVAS").toString();
				      String pasivosBancarios =  datos.get("FG_PASIVOS_BANCARIOS").toString();
				      String pasivosTotales =  datos.get("FG_PASIVOS_TOTALES").toString();
				      String capitalSocial =  datos.get("FG_CAPITAL_SOCIAL").toString();
				      String capitalContable =  datos.get("FG_CAPITAL_CONTABLE").toString();
				      String utilidadNeta =  datos.get("FG_UTILIDAD_NETA").toString();
				      String nioSolic =  datos.get("IG_ANIO_INF_FINANCIERA").toString();
					    
				      //Activos Totales 
				      if(ic_cedula==34 && anio.equals(anioFinaciero) ){           resultado = "$"+Comunes.formatoDecimal(activosTotales,2);
				      }else if( ic_cedula==45  && anio.equals(anioFinaciero) ){   resultado = "$"+Comunes.formatoDecimal(activosTotales,2);
				      }else if( ic_cedula==56 && anio.equals(anioFinaciero) ) {   resultado = "$"+Comunes.formatoDecimal(activosTotales,2);    }
				            
				      //Inversiones y Caja 
				      if(ic_cedula==35 && anio.equals(anioFinaciero) ){           resultado =  "$"+Comunes.formatoDecimal(inversionCaja,2);
				      }else if( ic_cedula==46  && anio.equals(anioFinaciero) ){   resultado =  "$"+Comunes.formatoDecimal(inversionCaja,2);
				      }else if( ic_cedula==57 && anio.equals(anioFinaciero) ) {   resultado =  "$"+Comunes.formatoDecimal(inversionCaja,2);   }
				           
				      //Cartera Vigentea 
				      if(ic_cedula==36 && anio.equals(anioFinaciero) ){           resultado = "$"+Comunes.formatoDecimal(carteraVigente,2); 
				      }else if( ic_cedula==47  && anio.equals(anioFinaciero) ){   resultado = "$"+Comunes.formatoDecimal(carteraVigente,2); 
				      }else if( ic_cedula==58 && anio.equals(anioFinaciero) ) {   resultado = "$"+Comunes.formatoDecimal(carteraVigente,2);     }
				            
				      //Cartera vencida 
				      if(ic_cedula==37 && anio.equals(anioFinaciero) ){           resultado =  "$"+Comunes.formatoDecimal(carteraVencida,2); 
				      }else if( ic_cedula==48  && anio.equals(anioFinaciero) ){   resultado =  "$"+Comunes.formatoDecimal(carteraVencida,2); 
				      }else if( ic_cedula==59 && anio.equals(anioFinaciero) ) {   resultado =  "$"+Comunes.formatoDecimal(carteraVencida,2);     }
				            
				      //Cartera Total 
				      if(ic_cedula==38 && anio.equals(anioFinaciero) ){           resultado =  "$"+Comunes.formatoDecimal(carteraTotal,2);
				      }else if( ic_cedula==49  && anio.equals(anioFinaciero) ){   resultado =  "$"+Comunes.formatoDecimal(carteraTotal,2);
				      }else if( ic_cedula==60 && anio.equals(anioFinaciero) ) {   resultado =  "$"+Comunes.formatoDecimal(carteraTotal,2);    }
				            
				      //Estimaci�n de Reservas
						if(ic_cedula==39 && anio.equals(anioFinaciero) ){           resultado =  "$"+Comunes.formatoDecimal(estimacionReservas,2); 
				      }else if( ic_cedula==50  && anio.equals(anioFinaciero) ){   resultado =  "$"+Comunes.formatoDecimal(estimacionReservas,2);
				      }else if( ic_cedula==61 && anio.equals(anioFinaciero) ) {   resultado =  "$"+Comunes.formatoDecimal(estimacionReservas,2);     }
				            
				      //Pasivos Bancarios
				      if(ic_cedula==40 && anio.equals(anioFinaciero) ){           resultado =  "$"+Comunes.formatoDecimal(pasivosBancarios,2); 
						}else if( ic_cedula==51  && anio.equals(anioFinaciero) ){   resultado =  "$"+Comunes.formatoDecimal(pasivosBancarios,2); 
				      }else if( ic_cedula==62 && anio.equals(anioFinaciero) ) {   resultado = "$"+Comunes.formatoDecimal(pasivosBancarios,2);    }
				            
				      //Pasivos Totales
				      if(ic_cedula==41 && anio.equals(anioFinaciero) ){           resultado = "$"+Comunes.formatoDecimal(pasivosTotales,2); 
				      }else if( ic_cedula==52  && anio.equals(anioFinaciero) ){   resultado = "$"+Comunes.formatoDecimal(pasivosTotales,2); 
				      }else if( ic_cedula==63 && anio.equals(anioFinaciero) ) {   resultado = "$"+Comunes.formatoDecimal(pasivosTotales,2);     }
				            
				      //Capital Social
				      if(ic_cedula==42 && anio.equals(anioFinaciero) ){           resultado =  "$"+Comunes.formatoDecimal(capitalSocial,2);
				      }else if( ic_cedula==53  && anio.equals(anioFinaciero) ){   resultado =  "$"+Comunes.formatoDecimal(capitalSocial,2); 
				      }else if( ic_cedula==64 && anio.equals(anioFinaciero) ) {   resultado =  "$"+Comunes.formatoDecimal(capitalSocial,2);      }
				                              
				      //Capital Contable
				      if(ic_cedula==43 && anio.equals(anioFinaciero) ){           resultado =  "$"+Comunes.formatoDecimal(capitalContable,2);  
				      }else if( ic_cedula==54  && anio.equals(anioFinaciero) ){   resultado =  "$"+Comunes.formatoDecimal(capitalContable,2);  
				      }else if( ic_cedula==65 && anio.equals(anioFinaciero) ) {   resultado =  "$"+Comunes.formatoDecimal(capitalContable,2);          }
				                              
				      //Utilidad Neta                  
				      if(ic_cedula==44 && anio.equals(anioFinaciero) ){           resultado =  "$"+Comunes.formatoDecimal(utilidadNeta,2);   
				      }else if( ic_cedula==55  && anio.equals(anioFinaciero) ){   resultado =  "$"+Comunes.formatoDecimal(utilidadNeta,2);   
				      }else if( ic_cedula==66 && anio.equals(anioFinaciero) ) {   resultado =  "$"+Comunes.formatoDecimal(utilidadNeta,2);         }
				                              
					}
							
					
					if(ic_cedula>=34 &  ic_cedula<=44 ) { 
						if(ic_cedula==34 ) {    							
							pdfDoc.setCell("Informaci�n Financiera  " +mes_anio_financiero,"celda01",ComunesPDF.LEFT, 3);
						}
						pdfDoc.setCell(descripcion,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(resultado,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(viabiliad,"formas",ComunesPDF.CENTER);
						
					}else  if(ic_cedula>=45 &  ic_cedula<=55 ) { 
						if(ic_cedula==45 ) {         							
							pdfDoc.setCell("Informaci�n Financiera  " +anio_finaciero,"celda01",ComunesPDF.LEFT, 3);
						}
						pdfDoc.setCell(descripcion,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(resultado,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(viabiliad,"formas",ComunesPDF.CENTER);
					  
					}else  if(ic_cedula>=56 &  ic_cedula<=66 ) { 
						if(ic_cedula==56 ) {           							
							pdfDoc.setCell("Informaci�n Financiera  " +anio_finaciero,"celda01",ComunesPDF.LEFT, 3);
						}
						pdfDoc.setCell(descripcion,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(resultado,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(viabiliad,"formas",ComunesPDF.CENTER);			
										
					}									
				}	
				 		   
			   System.out.println("ic_cedula ===="+ic_cedula); 
			   
			   if(ic_cedula>=67 &  ic_cedula<=68 ) { 
			      if(ic_cedula==67 ) {  
			         pdfDoc.setCell("Saldos","celda01",ComunesPDF.LEFT, 3);
					}
			      pdfDoc.setCell(descripcion,"formas",ComunesPDF.LEFT);
			      pdfDoc.setCell("$"+Comunes.formatoDecimal(resultado,2),"formas",ComunesPDF.CENTER);
			      pdfDoc.setCell(viabiliad,"formas",ComunesPDF.CENTER); 
			      
			   }
				
				
			} //while (rs.next()) 
		
		   pdfDoc.addTable();
		   pdfDoc.endDocument();   
		
		}catch(Exception e) { 
				log.error("Error en la generacion del Archivo CSV: " + e);
		}	

			return nombreArchivo;		 				 				 		
		}
				 
		
					
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		}catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			
			} catch(Exception e) {}
		  
			}
			return nombreArchivo;
		}
 

	
	/**
	 * Guardar  los datos d Resultado Final de Viabilidad 
	 * @param ic_solicitud
	 * @param ic_elemento_cedula
	 * @param viabilidad
	 * @return
	 * @throws AppException
	 */
	public boolean  guardarDictamen(String ic_solicitud ,  String ic_elemento_cedula[],  String viabilidad [] ) throws AppException{
		log.info("guardarDictamen(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();
		boolean  exito = true;     
		try { 
				
			con.conexionDB();
			
			for(int i=0;i<ic_elemento_cedula.length;i++){ 
			
				strSQL = new StringBuffer();
				strSQL.append(" Update CEDI_CEDULA  "+
								  " set CG_VIABLE_FINAL = ?  "+                           
								  " WHERE   IC_ELEMENTO_CEDULA  = ?    "+
								  " AND IC_SOLICITUD  = ?  ");
									  
					
				lVarBind    = new ArrayList();  
				lVarBind.add(viabilidad[i] );
				lVarBind.add(ic_elemento_cedula[i] );           
				lVarBind.add(ic_solicitud );
					
				log.debug("strSQL  "+strSQL);
				log.debug("lVarBind  "+lVarBind);
																																													  
				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();                
				
			}                           
			
		} catch(Throwable e) {
			exito = false;       
			e.printStackTrace();
			log.error ("guardarDictamen  " + e);
			throw new AppException("SIST0001");       
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();          
				log.info("  guardarDictamen (S) ");
			}        
		}
	  return exito;
	}
	
	
	/**
	 * Metodo para confirmar el Dictamen de Viabilidad  
	 * @param ic_solicitud
	 * @return
	 * @throws AppException
	 */
	public boolean  confirmarCedula(String ic_solicitud ) throws AppException{
		log.info("confirmarCedula(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();
		boolean  exito = true;     
		try { 
				
			con.conexionDB();
			
			strSQL = new StringBuffer();
			strSQL.append(" Update cedi_solicitud  "+
							  " set cs_dictamen_confirmado = ?  "+                           
							  " WHERE IC_SOLICITUD  = ?  ");
					
			lVarBind    = new ArrayList();
			lVarBind.add("S" );
			lVarBind.add(ic_solicitud );
					
			log.debug("strSQL  "+strSQL);
			log.debug("lVarBind  "+lVarBind);
																																													  
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			ps.executeUpdate();
			ps.close();                
			                           
			
		} catch(Throwable e) {
			exito = false;       
			e.printStackTrace();
			log.error ("confirmarCedula  " + e);
			throw new AppException("SIST0001");       
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();          
				log.info("  confirmarCedula (S) ");
			}        
		}
	  return exito;
	}
	
	
	/**
	 *Metodo para obtener la informaci�n Financiera
	 * @param solicitud
	 * @return
	 */
	public List getInfoFinanciera ( String  solicitud    ){
		log.info("getInfoFinanciera(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		boolean  exito = true; 
		HashMap datos  = new  HashMap();
		List reg  = new  ArrayList();
	   List  lVarBind    = new ArrayList();
		try { 
				
			con.conexionDB();
			
			strSQL = new StringBuffer();
			strSQL.append(" SELECT  " +
			"  f.IC_ANIO,  "+
			"  f.FG_ACTIVOS_TOTALES  ,  "+
			"  f.FG_INVERSION_CAJA , "+
			"  f.FG_CARTERA_VIGENTE  , "+
			"  f.FG_CARTERA_VENCIDA, "+
			"  f.FG_CARTERA_TOTAL , "+
			"  f.FG_ESTIMACION_RESERVAS   , "+
			"  f.FG_PASIVOS_BANCARIOS ,  "+
			"  f.FG_PASIVOS_TOTALES ,   "+
			"  f.FG_CAPITAL_SOCIAL  ,  "+
			"  f.FG_CAPITAL_CONTABLE ,   "+
			"  f.FG_UTILIDAD_NETA ,    "+
			"  s.IG_MES_INF_FINANCIERA ,  "+
			"  s.IG_ANIO_INF_FINANCIERA  "+
			
			" FROM CEDI_INFORMACION_FINANCIERA f  , CEDI_SOLICITUD s   "+
			" WHERE s.ic_solicitud  = f.ic_solicitud   "+
			" AND  f.ic_solicitud  =  ? " +
			" ORDER BY  f.IC_ANIO  desc " );
			
		   lVarBind    = new ArrayList();
		   lVarBind.add(solicitud);
			
		   log.debug("strSQL "+strSQL.toString()) ;
			log.debug("lVarBind "+lVarBind ); 
			
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind );
			rs = ps.executeQuery();
			while (rs.next()) {     
				datos  = new  HashMap();
				datos.put("IC_ANIO",rs.getString("IC_ANIO")==null?"":rs.getString("IC_ANIO")); 
				datos.put("FG_ACTIVOS_TOTALES",rs.getString("FG_ACTIVOS_TOTALES")==null?"":rs.getString("FG_ACTIVOS_TOTALES")); 
				datos.put("FG_INVERSION_CAJA",rs.getString("FG_INVERSION_CAJA")==null?"":rs.getString("FG_INVERSION_CAJA"));  
				datos.put("FG_CARTERA_VIGENTE",rs.getString("FG_CARTERA_VIGENTE")==null?"":rs.getString("FG_CARTERA_VIGENTE"));  
				datos.put("FG_CARTERA_VENCIDA",rs.getString("FG_CARTERA_VENCIDA")==null?"":rs.getString("FG_CARTERA_VENCIDA"));  
				datos.put("FG_CARTERA_TOTAL",rs.getString("FG_CARTERA_TOTAL")==null?"":rs.getString("FG_CARTERA_TOTAL"));  
				datos.put("FG_ESTIMACION_RESERVAS",rs.getString("FG_ESTIMACION_RESERVAS")==null?"":rs.getString("FG_ESTIMACION_RESERVAS"));  
				datos.put("FG_PASIVOS_BANCARIOS",rs.getString("FG_PASIVOS_BANCARIOS")==null?"":rs.getString("FG_PASIVOS_BANCARIOS"));  
				datos.put("FG_PASIVOS_TOTALES",rs.getString("FG_PASIVOS_TOTALES")==null?"":rs.getString("FG_PASIVOS_TOTALES"));  
				datos.put("FG_CAPITAL_SOCIAL",rs.getString("FG_CAPITAL_SOCIAL")==null?"":rs.getString("FG_CAPITAL_SOCIAL"));  
				datos.put("FG_CAPITAL_CONTABLE",rs.getString("FG_CAPITAL_CONTABLE")==null?"":rs.getString("FG_CAPITAL_CONTABLE"));  
				datos.put("FG_UTILIDAD_NETA",rs.getString("FG_UTILIDAD_NETA")==null?"":rs.getString("FG_UTILIDAD_NETA"));  
				datos.put("IG_ANIO_INF_FINANCIERA",rs.getString("IG_ANIO_INF_FINANCIERA")==null?"":rs.getString("IG_ANIO_INF_FINANCIERA"));  
				reg.add(datos);   
			 
			}
			ps.close();
			rs.close(); 
			 
			
		} catch(Throwable e) {
			exito = false;       
			e.printStackTrace();
			log.error ("getInfoFinanciera  " + e);
			throw new AppException("SIST0001");       
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();          
				log.info("  getInfoFinanciera (S) ");
			}        
		}
		
		return reg;
	}
	
	/**
	 * Metodo para obtener la Informaci�n Cualitativa 
	 * @param noSolicitud
	 * @param icPregunta
	 * @param icTipoRespuesta
	 * @return
	 */
	public String  getInfoCualitativa ( String  noSolicitud  , String icPregunta, String icTipoRespuesta   ){
		log.info("getInfoCualitativa(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		StringBuffer respuesta = new StringBuffer();
		boolean  exito = true; 
		List  lVarBind    = new ArrayList();
		
		try { 
				
			con.conexionDB();
			
			strSQL = new StringBuffer();
			strSQL.append(" SELECT   rp.cg_opcion_respuesta as OPCION_RESPUESTA,  "+ 
							  "  r.CG_RESPUESTA_LIBRE  AS RESPUESTA_LIBRE "+                       
							  " FROM  CEDI_RESPUESTA_PARAMETRIZACION rp,   "+
							  " CEDI_RESPUESTA_IFNB r,  "+
							  " CEDI_PREGUNTA  p  "+
							  " WHERE r.IC_RESPUESTA_PARAMETRIZACION  = rp.IC_RESPUESTA_PARAMETRIZACION  "+
							  " AND p.IC_PREGUNTA   =  rp.IC_PREGUNTA  "+
							  " AND rp.IC_TIPO_RESPUESTA  =  p.IC_TIPO_RESPUESTA "+
							  " AND r.IC_SOLICITUD = ? "+
							  " AND p.ic_pregunta= ?  "+
							  " AND rp.IC_TIPO_RESPUESTA =  ? ");
			
			lVarBind    = new ArrayList();   
			lVarBind.add(noSolicitud);
			lVarBind.add(icPregunta);
			lVarBind.add(icTipoRespuesta);
			
			log.debug("strSQL "+strSQL.toString() ); 
			log.debug("lVarBind "+lVarBind ); 
			
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			rs = ps.executeQuery();
			while (rs.next()) {    
				String opcionResp =rs.getString("OPCION_RESPUESTA")==null?"":rs.getString("OPCION_RESPUESTA");
				String respLibre =rs.getString("RESPUESTA_LIBRE")==null?"":rs.getString("RESPUESTA_LIBRE");
				if(icTipoRespuesta.equals("2") &&  respLibre.equals("") ) {
					respuesta.append(opcionResp+",");
				} else  if(icTipoRespuesta.equals("2") &&  !respLibre.equals("")) {
					respuesta.append(opcionResp+" - "+respLibre+"%,");
				
				}else if( icTipoRespuesta.equals("3")  && icPregunta.equals("7") &&  !respLibre.equals("")  ) {
					respuesta.append(opcionResp+", "+respLibre+" A�os ,");
				}else if( icTipoRespuesta.equals("3") ) {
					respuesta.append(opcionResp+",");
				}else if(icTipoRespuesta.equals("1")  ) {
					respuesta.append(respLibre+",");
				}
			}  
			ps.close();
			rs.close(); 
			
			if(!respuesta.toString().equals("")) {
				respuesta.deleteCharAt(respuesta.length()-1); 
			}
			 
		} catch(Throwable e) {
			exito = false;       
			e.printStackTrace();
			log.error ("getInfoCualitativa  " + e);
			throw new AppException("SIST0001");       
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();          
				log.info("  getInfoCualitativa (S) ");
			}        
		}
		
		return respuesta.toString();
	}
	
	 
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}



	public void setIc_intermediario(String ic_intermediario) {
		this.ic_intermediario = ic_intermediario;
	}

	public String getIc_intermediario() {
		return ic_intermediario;
	}


	public void setTxtFolioSolicitud(String txtFolioSolicitud) {
		this.txtFolioSolicitud = txtFolioSolicitud;
	}

	public String getTxtFolioSolicitud() {
		return txtFolioSolicitud;
	}
	
	
	public void setTxtFechaSoliIni(String txtFechaSoliIni) {
		this.txtFechaSoliIni = txtFechaSoliIni;
	}

	public String getTxtFechaSoliIni() {
		return txtFechaSoliIni;
	}
	
	public void setTxtFechaSoliFinal(String txtFechaSoliFinal) {
		this.txtFechaSoliFinal = txtFechaSoliFinal;
	}

	public String getTxtFechaSoliFinal() {
		return txtFechaSoliFinal;
	}

	
	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}
	
	public void setTieneDoctos(String tieneDoctos) {
		this.tieneDoctos = tieneDoctos;
	}

	public String getTieneDoctos() {
		return tieneDoctos;
	}
	
	
	public void setRfc_empresarial(String rfc_empresarial) {
		this.rfc_empresarial = rfc_empresarial;
	}

	public String getRfc_empresarial() {
		return rfc_empresarial;
	}
	
	
	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}

	public String getRazon_social() {
		return razon_social;
	}
	
	public void setEjecutivoAsignado(String ejecutivoAsignado) {
		this.ejecutivoAsignado = ejecutivoAsignado;
	}

	public String getEjecutivoAsignado() {
		return ejecutivoAsignado;
	}
	
	
	
	
	
}//Fin Clase
