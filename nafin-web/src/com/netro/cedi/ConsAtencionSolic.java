package com.netro.cedi;

import com.netro.cadenas.ConIntermediarios;
import com.netro.pdf.ComunesPDF;

import com.sun.org.apache.xpath.internal.operations.And;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Blob;

import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;
 

public class ConsAtencionSolic  implements IQueryGeneratorRegExtJS    {
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsAtencionSolic.class);
	
	private String    paginaOffset;
	private String    paginaNo;
	private List      conditions;
	StringBuffer qrySentencia   = new StringBuffer();
	
	private String ic_intermediario;
	private String txtFolioSolicitud;
	private String txtFechaSoliIni;
	private String txtFechaSoliFinal;
	private String tipoConsulta;
	private String tieneDoctos;
	private String ejecutivoAsignado;
	
	
	public ConsAtencionSolic() {}

	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
		
		
	public String getDocumentSummaryQueryForIds(List ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}
	
		
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
		
	
		
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		if (tipoConsulta.equals("Consultar")  ){
			
			qrySentencia.append( " SELECT DISTINCT s.IC_SOLICITUD as NO_SOLICITUD , "+ 
										" a.IC_IFNB_ASPIRANTE as IC_IFNB,   "+
										"  a.CG_RAZON_SOCIAL as RAZON_SOCIAL , "+
										"  a.CG_RFC as RFC_EMPRESARIAL ,  "+
										"  to_char(s.DF_ALTA,'DD/MM/YYYY HH24:MI:SS') as FECHA_HORA_REG ,  "+
										"  e.CG_NOMBRE as ESTATUS , "+
										"  e.IC_ESTATUS_SOLICITUD as IC_ESTATUS ,  "+
										"  s.CG_USUARIO_ASIGNADO AS LOGIN_EJECUTIVO , " + 
										"  '' AS EJECUTIVO_CEDI,  "+
										" LENGTH (BI_BALANCE)  as LON_BALANCE ,  "+
										" LENGTH (BI_EDO_RESULTADOS)  as LON_RESULTADOS ,  "+
										" LENGTH (BI_RFC)  as LON_RFC,   "+ 
		                        " LENGTH (BI_CEDULA_COMPLETA) as LON_CEDULA_COMPLETA "+      
										
										"  FROM  cedi_solicitud s, CEDI_IFNB_ASPIRANTE  a , CEDICAT_ESTATUS_SOLICITUD e , "+
										" CEDIHIS_ASIGNACION_SOLICITUD  ea "+										
										
										"  where  s.IC_IFNB_ASPIRANTE = a.IC_IFNB_ASPIRANTE  "+
										"  and s.IC_ESTATUS_SOLICITUD =   e.IC_ESTATUS_SOLICITUD   " +
										"  and s.IC_ESTATUS_SOLICITUD = ?    "	+
										"  AND ea.cg_usuario_asignado = s.CG_USUARIO_ASIGNADO   "+
										"  AND S.IC_SOLICITUD = ea.IC_SOLICITUD ");  			
										
			conditions.add("3"); 
			
		   if(!ejecutivoAsignado.equals("")){
		      qrySentencia.append("AND s.CG_USUARIO_ASIGNADO=  ? ");   
		      conditions.add(ejecutivoAsignado);         
		   }
			
			
			if(!ic_intermediario.equals("")){
				qrySentencia.append("AND  s.IC_IFNB_ASPIRANTE =  ? ");   
				conditions.add(ic_intermediario);         
			}
			
		 
				
			if(!txtFolioSolicitud.equals("")){       
				qrySentencia.append("AND s.IC_SOLICITUD =  ? ");   
				conditions.add(txtFolioSolicitud);         
			}
		
			if(!txtFechaSoliIni.equals("") && !txtFechaSoliFinal.equals("")){
				qrySentencia.append(" and s.DF_ALTA >=  to_date(?, 'dd/mm/yyyy')  ") ;
				qrySentencia.append("and s.DF_ALTA < to_date(?, 'dd/mm/yyyy')+1  ") ;
				conditions.add(txtFechaSoliIni);
				conditions.add(txtFechaSoliFinal);
			
			}
		
		}else  if (tipoConsulta.equals("ConsCheckDoctos")  ){
			
		   if(tieneDoctos.equals("S")) {
				
				qrySentencia.append("  SELECT distinct  d.IC_DOCUMENTO_CHECKLIST as  IC_DOCUMENTO "+
										  " , a.IC_SOLICITUD  as  IC_SOLICITUD "+
										  " , d.CG_NOMBRE  as  NOMBRE"+
										  " , d.CS_FIJO   as FIJO" +
										  " , d.CS_ACTIVO  as ACTIVO " +
										  " FROM  CEDI_DOCUMENTO_CHECKLIST d , CEDI_ARCHIVO_CHECKLIST  a " );
						 qrySentencia.append(" WHERE a.IC_SOLICITUD = ? ");
						 conditions.add(txtFolioSolicitud);										
					  qrySentencia.append(" order by  d.IC_DOCUMENTO_CHECKLIST asc "); 
				
			}else   if(tieneDoctos.equals("N")) {
				
			    qrySentencia.append( "SELECT DISTINCT d.ic_documento_checklist AS ic_documento, "+
			                  " d.cg_nombre AS nombre, "+
									" d.cs_fijo AS fijo, d.cs_activo AS activo,  "+
			                  txtFolioSolicitud +"as IC_SOLICITUD" +
									" FROM cedi_documento_checklist d         "+
									" ORDER BY d.ic_documento_checklist  ASC ");
				
			}
			
			
			
		}
	
		log.info("qrySentencia  esto es una prueba  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}
		
		
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		log.debug("crearCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		StringBuffer contenidoArchivo = new StringBuffer("");
		
					
		try {
		
		
		}catch(Exception e) { 
				log.error("Error en la generacion del Archivo CSV: " + e);
		}	

			return nombreArchivo;		 				 				 		
		}
				 
					
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		}catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			
			} catch(Exception e) {}
		  
			}
			return nombreArchivo;
		}
 
 
  /**
	*metodo que verifico si la solicitud tiene documentos chek List
	 * @param ic_solicitud
	 * @return
	 * @throws AppException
	 */
	public String  hayDoctosChekList(String ic_solicitud , String ic_doctoCheckList ) throws AppException{
		log.info("hayDoctosChekList(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();
		boolean  exito = true; 
		String  haydoctos="N";
	   
		try { 
				
			con.conexionDB();
			
		  			
			strSQL = new StringBuffer();
			strSQL.append("select count(*) as total  " +
				"FROM  cedi_archivo_checklist "+
				" where ic_solicitud = "+ic_solicitud );
			
			if(!ic_doctoCheckList.equals("")){
				strSQL.append(" and  IC_DOCUMENTO_CHECKLIST = "+ic_doctoCheckList );
			}
			
			System.out.println("strSQL "+strSQL.toString() ); 
			
			ps = con.queryPrecompilado(strSQL.toString());
			rs = ps.executeQuery();
			if (rs.next()) {						
				int  total =rs.getInt("total");	
				if(total>0) { haydoctos = "S";  }
			}
			ps.close();
			rs.close(); 
				  
			
		} catch(Throwable e) {
			exito = false;       
			e.printStackTrace();
			log.error ("hayDoctosChekList  " + e);
			throw new AppException("SIST0001");       
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();          
				log.info("  hayDoctosChekList (S) ");
			}        
		}
	  return haydoctos;
	}
	
  /**
	 *Metodo que guarda los registros de los documentos del Chek List
	 * @param ic_solicitud
	 * @param ic_documentos
	 * @param activos
	 * @param nombreDoctos
	 * @return
	 * @throws AppException
	 */
  
	public boolean   guardarDoctosCheck(String ic_solicitud ,  String ic_documentos[],  String activos [], String nombreDoctos [] ) throws AppException{
		log.info("guardarDoctosCheck(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();
		boolean  exito = true; 
		String  no_docto="";
		try { 
		 		
			con.conexionDB();
			
		   
				
			for(int i=0;i<ic_documentos.length;i++){ 
				
			   log.debug("***********************************************************");
			   log.debug("ic_documentos[i]  "+ic_documentos[i]+"   nombreDoctos[i]  "+nombreDoctos[i]);
			   log.debug("***********************************************************");
				
				
				if(  ( ic_documentos[i].equals("")  &&   !nombreDoctos[i].equals("") ) ) {   				
					
				   strSQL = new StringBuffer();
				   strSQL.append("SELECT SEQ_CEDI_DOC_CHECKLIST.nextval FROM dual");
					ps = con.queryPrecompilado(strSQL.toString());
					rs = ps.executeQuery();
				   if (rs.next()) {
						no_docto = rs.getString(1)==null?"1":rs.getString(1);
					}
				   ps.close();
					rs.close();
				
					strSQL = new StringBuffer();
					strSQL.append(" insert into CEDI_DOCUMENTO_CHECKLIST  "+
									  " ( IC_DOCUMENTO_CHECKLIST  ,  CG_NOMBRE , CS_ACTIVO     )  "+
									  " values ( ? , ?, ?   )  ");
					
					lVarBind    = new ArrayList();  
				   lVarBind.add(no_docto );
					lVarBind.add(nombreDoctos[i] );
				   lVarBind.add(activos[i] );
					log.debug("strSQL  "+strSQL+"    lVarBind  "+lVarBind);
																																													  
					ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
					ps.executeUpdate();
					ps.close();
					
					
				   strSQL = new StringBuffer();
				   strSQL.append(" insert into CEDI_ARCHIVO_CHECKLIST  "+
				                 " (  IC_DOCUMENTO_CHECKLIST  , IC_SOLICITUD    )  "+
				                 " values ( ? ,  ?   )  ");
				   
				   lVarBind    = new ArrayList(); 
				   lVarBind.add(no_docto );
				   lVarBind.add(ic_solicitud );
					
				   log.debug("strSQL  "+strSQL+"    lVarBind  "+lVarBind);
				                                                                                                                             
				   ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				   ps.executeUpdate();
				   ps.close();
				   				
					
				}else  {
					
				   String  existe =  hayDoctosChekList(ic_solicitud, ic_documentos[i]);
				  
					if(existe.equals("S")) {
					
						strSQL = new StringBuffer();
						strSQL.append(" Update CEDI_DOCUMENTO_CHECKLIST  "+
										  " set CS_ACTIVO = ? ,  "+
										  " CG_NOMBRE =  ?  "+
										  " where  IC_DOCUMENTO_CHECKLIST  = ?    ");
						
						lVarBind    = new ArrayList();  
						lVarBind.add(activos[i] );
						lVarBind.add(nombreDoctos[i] );
						lVarBind.add(ic_documentos[i] );
						
						log.debug("strSQL  "+strSQL+"    lVarBind  "+lVarBind);
																																														  
						ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
						ps.executeUpdate();
						ps.close();	
					
					}else  if(existe.equals("N")) {
						
						strSQL = new StringBuffer();
						strSQL.append(" insert into CEDI_ARCHIVO_CHECKLIST  "+
										  " (  IC_DOCUMENTO_CHECKLIST  , IC_SOLICITUD    )  "+
										  " values ( ? ,  ?   )  ");
						
						lVarBind    = new ArrayList(); 
						lVarBind.add(ic_documentos[i] );
						lVarBind.add(ic_solicitud );
						
					   log.debug("strSQL  "+strSQL+"    lVarBind  "+lVarBind);
																																														  
						ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
						ps.executeUpdate();
						ps.close();
					} 
						
					
				}			
				
			}									 
			
		} catch(Throwable e) {
			exito = false;       
			e.printStackTrace();
			log.error ("adignarEjecutivoCEDI  " + e);
			throw new AppException("SIST0001");       
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();          
				log.info("  adignarEjecutivoCEDI (S) ");
			}        
		}
	  return exito;
	}
	
	
	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}



	public void setIc_intermediario(String ic_intermediario) {
		this.ic_intermediario = ic_intermediario;
	}

	public String getIc_intermediario() {
		return ic_intermediario;
	}


	public void setTxtFolioSolicitud(String txtFolioSolicitud) {
		this.txtFolioSolicitud = txtFolioSolicitud;
	}

	public String getTxtFolioSolicitud() {
		return txtFolioSolicitud;
	}
	
	
	public void setTxtFechaSoliIni(String txtFechaSoliIni) {
		this.txtFechaSoliIni = txtFechaSoliIni;
	}

	public String getTxtFechaSoliIni() {
		return txtFechaSoliIni;
	}
	
	public void setTxtFechaSoliFinal(String txtFechaSoliFinal) {
		this.txtFechaSoliFinal = txtFechaSoliFinal;
	}

	public String getTxtFechaSoliFinal() {
		return txtFechaSoliFinal;
	}

	
	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}
	
	public void setTieneDoctos(String tieneDoctos) {
		this.tieneDoctos = tieneDoctos;
	}

	public String getTieneDoctos() {
		return tieneDoctos;
	}
	
	public void setEjecutivoAsignado(String ejecutivoAsignado) {
		this.ejecutivoAsignado = ejecutivoAsignado;
	}

	public String getEjecutivoAsignado() {
		return ejecutivoAsignado;
	}	
	
}//Fin Clase
