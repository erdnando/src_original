package com.netro.cadenas;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCatalogoClasificacion  {

	private static final Log log = ServiceLocator.getInstance().getLog(ConsCatalogoClasificacion.class);//Variable para enviar mensajes al log.
	//ATRIBUTOS
	private String claveCadena;
	private List 		conditions;
	private String interclave;
	private String descripcion;
	private String operacion;
	private String modifica;
	private String icEpo;
	
	private String cveCat ="";
	private String RegSig = "";
	private String Totalreg;
	StringBuffer qrySentencia = new StringBuffer("");	
	
	public ConsCatalogoClasificacion() {
	}
	public Registros getConsultaData(){
		log.info("getConsultaData(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			StringBuffer condicion  = new StringBuffer("");
			AccesoDB con = new AccesoDB(); 
			Registros registros  = new Registros();
			try{
				con.conexionDB();
				qrySentencia.append(	"	SELECT   t1.ic_calificacion AS interclave, t2.cd_nombre AS clave,	t1.cd_descripcion AS descripcion "+
											"	FROM comrel_calif_epo t1, comcat_calificacion t2	"+
											"	WHERE ic_epo = ? AND t1.ic_calificacion = t2.ic_calificacion	");
	
				conditions.add(claveCadena);
				registros = con.consultarDB(qrySentencia.toString(),conditions);
				con.cierraConexionDB();
			} catch (Exception e) {
				log.error("getConsultaData  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			} 	
		
			log.info("qrySentencia getConsultaData: "+qrySentencia.toString());
			log.info("conditions "+conditions);
			
			log.info("getConsultaData (S) ");
			return registros;
	}	
	public Registros getDataCadenas(){
		log.info("getDataCadenas(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			StringBuffer condicion  = new StringBuffer("");
			AccesoDB con = new AccesoDB(); 
			Registros registros  = new Registros();
			try{
				con.conexionDB();
				qrySentencia.append(	"SELECT DISTINCT (t2.ic_epo_pyme_if) AS clave, "+
											"		t1.cg_razon_social AS descripcion	"+
											"	FROM comcat_epo t1, comrel_nafin t2	"+
											"	WHERE t1.cs_habilitado = 'S' AND t1.ic_epo = t2.ic_epo_pyme_if	"+
										" ORDER BY t1.cg_razon_social ");
	
				registros = con.consultarDB(qrySentencia.toString(),conditions);// con.consultarDB(qrySentencia.toString());
				con.cierraConexionDB();
			} catch (Exception e) {
				log.error("getConsultaData  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			} 	
		
			log.info("qrySentencia getDataCadenas: "+qrySentencia.toString());
			log.info("conditions "+conditions);
			
			log.info("getDataCadenas (S) ");
			return registros;
	}	
	public String getRegSig(){
		log.info("insertModificarData(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			AccesoDB con = new AccesoDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			String resultado  = "";
			try{
				qrySentencia.append( "SELECT nvl(max(T1.IC_CALIFICACION),0)+1 AS CVEREGSIG FROM COMCAT_CALIFICACION T1,COMREL_CALIF_EPO T2 WHERE T2.IC_EPO=? AND T1.IC_CALIFICACION=T2.IC_CALIFICACION ORDER BY T2.IC_CALIFICACION");
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setString(1,claveCadena);
				rs = ps.executeQuery();
				if (rs.next()){
					resultado = rs.getString(1);
				}
				rs.close();
				ps.close();
			}catch(Exception e){
				log.error("insertModificarData  Error: " + e);
				e.printStackTrace();
			}finally{
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}
			
			return resultado;
	}
	public boolean insertModificarEliminarData(){
		log.info("insertModificarData(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			AccesoDB con = new AccesoDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			boolean exito = true;
			try{
				con.conexionDB();
				String totRegSQL = "SELECT COUNT(IC_CALIFICACION) AS TOTAL FROM COMCAT_CALIFICACION";
				ps = con.queryPrecompilado(totRegSQL);
				rs = ps.executeQuery();
				if (rs.next()){
					Totalreg = rs.getString(1);
				}
				rs.close();
				ps.close();
				
				if(modifica.equals("T")){
					if(RegSig.equals("")){
						String SQLRegSig = "SELECT nvl(max(T1.IC_CALIFICACION),0)+1 AS CVEREGSIG FROM COMCAT_CALIFICACION T1,COMREL_CALIF_EPO T2 WHERE T2.IC_EPO=? AND T1.IC_CALIFICACION=T2.IC_CALIFICACION ORDER BY T2.IC_CALIFICACION";
						ps = con.queryPrecompilado(SQLRegSig);
						ps.setString(1,claveCadena);
						rs = ps.executeQuery();
						if (rs.next()){
							RegSig = rs.getString(1);
						}
						rs.close();
						ps.close();
					}
					if(RegSig.equals("1")){
						String SQLcve = "Select IC_CALIFICACION as ClaveCatalogo from COMREL_CALIF_EPO where IC_EPO= ? and IC_CALIFICACION= ?";
						ps = con.queryPrecompilado(SQLcve);
						ps.setString(1, claveCadena);
						ps.setString(2, RegSig);
						rs = ps.executeQuery();
						if (rs.next())
							cveCat = rs.getString(1);
						rs.close();
						ps.close();
					}
					if(cveCat.equals("") && RegSig.equals("1")){
						String SQLprimerreg="INSERT INTO COMREL_CALIF_EPO(CD_DESCRIPCION,IC_CALIFICACION,IC_EPO) VALUES('SIN CALIFICACION'," + RegSig + "," + claveCadena + ")";
						int  intRegSig = 1;
						try{
							con.ejecutaSQL(SQLprimerreg);
							con.terminaTransaccion(true);
							intRegSig ++;
							RegSig = intRegSig + "";
						}catch(Exception e){
							con.terminaTransaccion(false);
						}
					}
				}
				
				if(operacion.equals("INSERTAR")){
					if(Integer.parseInt(RegSig) > Integer.parseInt(Totalreg))
						operacion = "";
				}
				String TABLA = "COMREL_CALIF_EPO"; 
				String CAMPOS = "CD_DESCRIPCION,IC_CALIFICACION,IC_EPO"; 
				String CONDICLION = "IC_CALIFICACION= ? AND IC_EPO= ?";
				String CONDICIONELIMINA = "IC_CALIFICACION IN (?) AND IC_EPO= ?";
				String UPDATESET = "CD_DESCRIPCION= ? ";
				String VALORES = "?,?,?";
								
				if(!"".equals(operacion)){
					if("MODIFICAR".equals(operacion)){
						if(!"".equals(TABLA)&&!"".equals(UPDATESET)&&!"".equals(CONDICLION)){
							qrySentencia.append( "UPDATE " + TABLA + " SET " + UPDATESET + " WHERE " + CONDICLION);
						}
						ps = con.queryPrecompilado(qrySentencia.toString());
						if(descripcion.length() > 25){
							ps.setString(1, descripcion.substring(0,25));	
						}else{
							ps.setString(1, descripcion.substring(0,descripcion.length()));
						}
						ps.setString(2, interclave);
						ps.setString(3, claveCadena);
						exito =(ps.executeUpdate()==1)?true:false;
						
					}
					if("INSERTAR".equals(operacion)){
						log.debug("pasa pasa pasa pasa................INSERTAR");
						if(!"".equals(TABLA)&&!"".equals(CAMPOS)&&!"".equals(VALORES)){
							qrySentencia.append("INSERT INTO " + TABLA + "(" + CAMPOS + ") VALUES(" + VALORES + ")");
							System.out.println("El query del insert :: "+qrySentencia.toString());
						}
						ps = con.queryPrecompilado(qrySentencia.toString());
						
						if(descripcion.length() > 25){
							ps.setString(1, descripcion.substring(0,25));	
						}else{
							ps.setString(1, descripcion.substring(0,descripcion.length()));
						}
						ps.setString(2, RegSig);
						ps.setString(3, claveCadena);
						ps.executeUpdate();
					}   
					if("ELIMINAR".equals(operacion)){
						List lVarBind		= new ArrayList();
						if(!"".equals(TABLA)){
							qrySentencia.append("DELETE  " + TABLA + " WHERE IC_CALIFICACION IN ("+interclave+") AND IC_EPO= "+claveCadena );
						}
						log.debug("................qrySentencia :: "+qrySentencia);
						ps = con.queryPrecompilado(qrySentencia.toString());
						ps.executeUpdate();
						ps.close();	   
					}   
				
				}
			} catch (Exception e) {
				log.error("insertModificarData  Error: " + e);
				e.printStackTrace();
				exito = false;
			} finally {
				if (con.hayConexionAbierta()) {
					con.terminaTransaccion(exito);
					con.cierraConexionDB();
				}
			} 
			log.info("qrySentencia insertModificarData: "+qrySentencia.toString());
			log.info("conditions "+conditions);
			
			log.info("insertModificarData (S) ");
			return exito;
	}
	public void setClaveCadena(String claveCadena) {
		this.claveCadena = claveCadena;
	}


	public String getClaveCadena() {
		return claveCadena;
	}
	
	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setInterclave(String interclave) {
		this.interclave = interclave;
	}


	public String getInterclave() {
		return interclave;
	}


	public void setOperacion(String Operacion) {
		this.operacion = Operacion;
	}


	public String getOperacion() {
		return operacion;
	}


	public void setModifica(String modifica) {
		this.modifica = modifica;
	}


	public String getModifica() {
		return modifica;
	}


	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}


	public String getIcEpo() {
		return icEpo;
	}
}