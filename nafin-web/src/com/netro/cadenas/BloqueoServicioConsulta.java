package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class BloqueoServicioConsulta implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(BloqueoServicioConsulta.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	//PARAMETROS 
	private String icIf;
	
	private List pKeys;
	
	public BloqueoServicioConsulta() {
	
	}

	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getAggregateCalculationQuery() {
		/* log.info("getAggregateCalculationQuery(E)");

		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
		qrySentencia.append(
	"	SELECT /*use_nl(cpe,ce,cn,xxx) aki asterisco/  cpe.ic_producto_nafin, cpe.ic_epo, "+
         " cn.ic_nafin_electronico, ce.cg_razon_social, ce.cg_rfc, "+
         " bb3.cs_bloqueado, bb3.df_fecha, bb3.ic_usuario, "+
         " bb3.cg_nombre_usuario, bb3.cg_causa, "+
         " 'DisBloqServConsultaCA:getDocumentSummaryQueryForIds' "+
   " FROM comrel_producto_epo cpe, "+
        " comcat_epo ce, "+
        " comrel_nafin cn, "+
        " (SELECT bbd.ic_bloqueo_disp, bbd.cs_bloqueado, bbd.ic_epo, "+
                " TO_CHAR (bbd.df_fecha, 'DD/MM/YYYY HH24:MM:SS') df_fecha, "+ 
                " bbd.ic_usuario, bbd.cg_nombre_usuario, bbd.cg_causa "+
           " FROM bit_bloqueo_disp bbd, "+
                " (SELECT ic_bloqueo_disp, ic_epo, cs_bloqueado "+
                   " FROM bit_bloqueo_disp bbd "+
                  " WHERE ic_producto_nafin = 1) bd2 "+
          " WHERE bbd.ic_bloqueo_disp = bd2.ic_bloqueo_disp "+
            " AND bbd.ic_epo = bd2.ic_epo "+
            " AND bbd.ic_producto_nafin = 1) bb3 "+
 "  WHERE cpe.ic_epo = ce.ic_epo "+
   "  AND cpe.ic_epo = cn.ic_epo_pyme_if "+
   "  AND cpe.ic_epo = bb3.ic_epo(+) "	);  
	
		if(!icIf.equals("")){
			qrySentencia.append(
	" AND cpe.ic_epo = ? "+ 
	" AND cpe.ic_producto_nafin = 1 "+
   " AND cpe.cg_dispersion = 'S' "+ 
   " AND cn.cg_tipo = 'E' "+
   " AND bb3.ic_bloqueo_disp IS NOT NULL ");			 
			conditions.add(icIf);
	} else {
			qrySentencia.append(
   " AND cpe.ic_producto_nafin = 1 "+
   " AND cpe.cg_dispersion = 'S' "+ 
   " AND cn.cg_tipo = 'E' "+
   " AND bb3.ic_bloqueo_disp IS NOT NULL ");	
		}

		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	*/ return "";
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		qrySentencia.append(
		"SELECT /*use_nl(cpe,ce,cn,xxx)*/ bb3.ic_bloqueo_disp						"+	
      " FROM comrel_producto_epo cpe,													"+
      "   comcat_epo ce,																	"+
      "   comrel_nafin cn,																	"+
      "   (SELECT bbd.ic_bloqueo_disp, bbd.cs_bloqueado, bbd.ic_epo,			"+
      "           TO_CHAR (bbd.df_fecha, 'DD/MM/YYYY HH24:MM:SS') df_fecha, "+
      "           bbd.ic_usuario, bbd.cg_nombre_usuario, bbd.cg_causa		"+
      "      FROM bit_bloqueo_disp bbd,												"+
      "           (SELECT ic_bloqueo_disp, ic_epo, cs_bloqueado				"+
      "              FROM bit_bloqueo_disp bbd										"+
      "             WHERE ic_producto_nafin = 1) bd2								"+
      "     WHERE bbd.ic_bloqueo_disp = bd2.ic_bloqueo_disp						"+
      "       AND bbd.ic_epo = bd2.ic_epo												"+
      "       AND bbd.ic_producto_nafin = 1) bb3									"+
		" WHERE cpe.ic_epo = ce.ic_epo													"+
      " AND cpe.ic_epo = cn.ic_epo_pyme_if											"+
      " AND cpe.ic_epo = bb3.ic_epo(+)													");
	  
     
		if(icIf.equals("")){
			qrySentencia.append(
			" AND cpe.ic_producto_nafin = 1	"+
			" AND cpe.cg_dispersion = 'S'		"+
			" AND cn.cg_tipo = 'E'				"+
			" AND bb3.ic_bloqueo_disp IS NOT NULL		  "+
			" ORDER BY ce.cg_razon_social, bb3.df_fecha ");
		}
		else {
			qrySentencia.append(
			" AND cpe.ic_epo = ?					"+
			" AND cpe.ic_producto_nafin = 1	"+
			" AND cpe.cg_dispersion = 'S'		"+
			" AND cn.cg_tipo = 'E'				"+
			" AND bb3.ic_bloqueo_disp IS NOT NULL		  "+
			" ORDER BY ce.cg_razon_social, bb3.df_fecha ");
			conditions.add(icIf);
		}

		
		log.info("getDocumentQuery_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}

	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * datos a mostrar en la consulta basandose en los identificadores unicos 
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista 
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
		qrySentencia.append(
	"	SELECT /*use_nl(cpe,ce,cn,xxx)*/ cpe.ic_producto_nafin, cpe.ic_epo, "+
         " cn.ic_nafin_electronico, ce.cg_razon_social, ce.cg_rfc, "+
         " bb3.cs_bloqueado, bb3.df_fecha, bb3.ic_usuario, "+
         " bb3.cg_nombre_usuario, bb3.cg_causa, "+
         " 'DisBloqServConsultaCA:getDocumentSummaryQueryForIds' "+
   " FROM comrel_producto_epo cpe, "+
        " comcat_epo ce, "+
        " comrel_nafin cn, "+
        " (SELECT bbd.ic_bloqueo_disp, bbd.cs_bloqueado, bbd.ic_epo, "+
                " TO_CHAR (bbd.df_fecha, 'DD/MM/YYYY HH24:MM:SS') df_fecha, "+ 
                " bbd.ic_usuario, bbd.cg_nombre_usuario, bbd.cg_causa "+
           " FROM bit_bloqueo_disp bbd, "+
                " (SELECT ic_bloqueo_disp, ic_epo, cs_bloqueado "+
                   " FROM bit_bloqueo_disp bbd "+
                  " WHERE ic_producto_nafin = 1) bd2 "+
          " WHERE bbd.ic_bloqueo_disp = bd2.ic_bloqueo_disp "+
            " AND bbd.ic_epo = bd2.ic_epo "+
            " AND bbd.ic_producto_nafin = 1) bb3 "+
 "  WHERE cpe.ic_epo = ce.ic_epo "+
   "  AND cpe.ic_epo = cn.ic_epo_pyme_if "+
   "  AND cpe.ic_epo = bb3.ic_epo(+) ");
	if(!icIf.equals("")){
		qrySentencia.append(
		" AND cpe.ic_epo = ? ");
		conditions.add(icIf);
		} 
		qrySentencia.append(
     " AND cpe.ic_producto_nafin = 1 "+
     " AND cpe.cg_dispersion = 'S' "+
     " AND cn.cg_tipo = 'E' "+
     " AND bb3.ic_bloqueo_disp IS NOT NULL "+
	  " AND (    ");

	pKeys = new ArrayList();
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0){ 
				qrySentencia.append(" OR (bb3.ic_bloqueo_disp = ?) ");
			} else {
				qrySentencia.append(" (bb3.ic_bloqueo_disp = ? ) ");
			}
			conditions.add(lItem.get(0));
			pKeys.add(pageIds.get(i));			
		}
	qrySentencia.append(
	" ) ORDER BY ce.cg_razon_social, bb3.df_fecha ");

		log.info("getDocumentSummaryQueryForIds_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}

	
	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQueryFile() {
	
		return "";	
	}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
	String nombreArchivo = "";
	HttpSession session = request.getSession();	
	ComunesPDF pdfDoc = new ComunesPDF();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
	
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(2, path + nombreArchivo);
		
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
		pdfDoc.setTable(7,90);
		
		pdfDoc.setCell("N@E"										,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Raz�n Social"							,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("RFC"										,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Bloqueado"								,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha/Hora Bloqueo/Desbloqueo"	,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre del Usuario"					,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Causa"									,"celda01",ComunesPDF.CENTER);
				
		while(reg.next()){
			String nombre 		= (reg.getString("IC_NAFIN_ELECTRONICO") 					== null) ? "" : reg.getString("IC_NAFIN_ELECTRONICO");
			String numlinfon 	= (reg.getString("CG_RAZON_SOCIAL") 	== null) ? "" : reg.getString("CG_RAZON_SOCIAL");
			String codfin 		= (reg.getString("CG_RFC") 		== null) ? "" : reg.getString("CG_RFC");
			String desclincre = (reg.getString("CS_BLOQUEADO") 		== null) ? "" : reg.getString("CS_BLOQUEADO");
			if (desclincre.equals("S")){desclincre="Si";}
			if (desclincre.equals("N")){desclincre="No";}
			String codage 		= (reg.getString("DF_FECHA") 			== null) ? "" : reg.getString("DF_FECHA");
			String codsub 		= (reg.getString("CG_NOMBRE_USUARIO") == null) ? "" : reg.getString("CG_NOMBRE_USUARIO");
			String montas 		= (reg.getString("CG_CAUSA") 			== null) ? "" : reg.getString("CG_CAUSA");
			
			pdfDoc.setCell(nombre		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numlinfon	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(codfin		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(desclincre	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(codage		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(codsub		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(montas		,"formas",ComunesPDF.CENTER);
		}
		pdfDoc.addTable();
		
		
		pdfDoc.endDocument();
		} catch(Exception e){
		throw new AppException("Error al generar el archivo PDF ", e);
	}
	return nombreArchivo;
					
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		return "";
	}

	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	//SETTERS Y GETTERS
	public void setIcIf(String icIf) {
		this.icIf = icIf;
	}


	public String getIcIf() {
		return icIf;
	}


	


	public void setPKeys(List pKeys) {
	pKeys = new ArrayList();
		this.pKeys = pKeys;
	}


	public List getPKeys() {
		return pKeys;
	}

}