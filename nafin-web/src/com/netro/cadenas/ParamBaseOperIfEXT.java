package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ParamBaseOperIfEXT  implements IQueryGeneratorRegExtJS{

	private ArrayList valoresBind = new ArrayList();
	private String intermediario;
	private String baseOperacion;
	private int numList = 1;
	private static Log log = ServiceLocator.getInstance().getLog(com.netro.cadenas.ConsLineafondeIfEXT.class);
	
	public ParamBaseOperIfEXT() {
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentQuery(){
	log.debug("getDocumentQuery (E) ");
	StringBuffer strQuery 	= new StringBuffer();

	strQuery.append(
	" SELECT  fibo.codigo_base_operacion AS codigo_base_opera "+
    " FROM mg_financieras fi, "+
		" no_financieras_x_bo fibo, "+
		" no_bases_de_operacion bo, "+
		" no_productos_banco pb, "+
		" comcat_if i, "+
		" comcat_financiera f "+
   " WHERE fibo.codigo_base_operacion = bo.codigo_base_operacion "+
     " AND fi.tipo_financiera = fibo.tipo_financiera "+
     " AND fi.codigo_financiera = fibo.codigo_financiera "+
     " AND bo.codigo_empresa = pb.codigo_empresa "+
     " AND bo.codigo_producto_banco = pb.codigo_producto_banco "+
     " AND fi.codigo_financiera = i.ic_financiera "+
     " AND fi.tipo_financiera = f.ic_tipo_financiera "+
     " AND i.ic_financiera = f.ic_financiera "+
     " AND ic_if = ? ");
		valoresBind.add(intermediario);
		
		if(!baseOperacion.equals("")){
			strQuery.append(" AND fibo.codigo_base_operacion = ? ");
			valoresBind.add(baseOperacion);
		}
	strQuery.append(" ORDER BY nombre ");	
	
	log.debug(" strQuery identificadores unicos - - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.debug("getDocumentQuery (S) ");
		
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * datos a mostrar en la consulta basandose en los identificadores unicos 
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista 
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentSummaryQueryForIds(List ids){
	
	log.info("getDocumentSummaryQueryForIds(E)");
	StringBuffer strQuery 	= new StringBuffer();
	List 		conditions= new ArrayList();		
		
	strQuery.append(
	" SELECT fi.nombre , fibo.codigo_base_operacion," 				+
	"        bo.descripcion as unodes, bo.codigo_producto_banco, "  			+
	"        pb.descripcion as desdos, bo.dias_minimos_primer_pago, "  			+
	"        bo.opera_factoraje"  			+
	"   FROM mg_financieras fi,"   +
	"        no_financieras_x_bo fibo,"  			+
	"        no_bases_de_operacion bo,"   					+
	"        no_productos_banco pb,"   			+
	"        comcat_if i,"   			+
	"        comcat_financiera f"   			+
	"  WHERE fibo.codigo_base_operacion = bo.codigo_base_operacion"   			+
	"    AND fi.tipo_financiera = fibo.tipo_financiera"   +
	"    AND fi.codigo_financiera = fibo.codigo_financiera"   +
	"    AND bo.codigo_empresa = pb.codigo_empresa"   						  +
	"    AND bo.codigo_producto_banco = pb.codigo_producto_banco"    +
	"    AND fi.codigo_financiera = i.ic_financiera" +
	"    AND fi.tipo_financiera = f.ic_tipo_financiera"+
	"    AND i.ic_financiera = f.ic_financiera" +
	"    AND ic_if = ? ");
	
	valoresBind = new ArrayList();
		valoresBind.add(intermediario);
	
	if(!baseOperacion.equals("")){
			strQuery.append(" AND fibo.codigo_base_operacion = ? ");
			valoresBind.add(baseOperacion);
		}
	strQuery.append(" ORDER BY nombre ");	

	  //*******************************************************************
	// if(!ic_if.equals("0")){
	//	strQuery.append(" AND i.ic_if = ? ");

			
		List pKeys = new ArrayList();
		for(int i = 0; i < ids.size(); i++){
			List lItem = (ArrayList)ids.get(i);
		//	if(numList ==1){				
				conditions.add(lItem);				
			//	valoresBind=(ArrayList)conditions;
				//numList++;
		//	}
		//	pKeys.add(ids.get(i));			
		}		
	 //}
	
	log.info("strQuery basado en identificadores unicos - -  >" + strQuery);
	log.info("valoresBind "+ valoresBind);
	log.info("getDocumentSummaryQueryForIds(S)");	
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getAggregateCalculationQuery(){
	StringBuffer strQuery 	= new StringBuffer();
	log.info("getAggregateCalculationQuery (E)");
	
	strQuery.append(
	" SELECT fi.nombre , fibo.codigo_base_operacion," 				+
	"        bo.descripcion as unodes, bo.codigo_producto_banco, "  			+
	"        pb.descripcion as desdos, bo.dias_minimos_primer_pago, "  			+
	"        bo.opera_factoraje"  			+
	"   FROM mg_financieras fi,"   +
	"        no_financieras_x_bo fibo,"  			+
	"        no_bases_de_operacion bo,"   					+
	"        no_productos_banco pb,"   			+
	"        comcat_if i,"   			+
	"        comcat_financiera f"   			+
	"  WHERE fibo.codigo_base_operacion = bo.codigo_base_operacion"   			+
	"    AND fi.tipo_financiera = fibo.tipo_financiera"   +
	"    AND fi.codigo_financiera = fibo.codigo_financiera"   +
	"    AND bo.codigo_empresa = pb.codigo_empresa"   						  +
	"    AND bo.codigo_producto_banco = pb.codigo_producto_banco"    +
	"    AND fi.codigo_financiera = i.ic_financiera" +
	"    AND fi.tipo_financiera = f.ic_tipo_financiera"+
	"    AND i.ic_financiera = f.ic_financiera" +
	"    AND ic_if = ? " );
	
	//////////////////
//	valoresBind.add(intermediario);
		
		if(!baseOperacion.equals("")){
			strQuery.append(" AND fibo.codigo_base_operacion = ? ");
//			valoresBind.add(baseOperacion);
		}
	strQuery.append(" ORDER BY nombre ");
	/////////////////////////

						
	log.debug(" strQuery obtiene totales- - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.info("getAggregateCalculationQuery (S)");
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
public  List getConditions(){		
	return valoresBind;
	}
	
	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentQueryFile(){
	log.debug("getDocumentQueryFile (E)");
	StringBuffer strQuery 	= new StringBuffer();
		
	strQuery.append(
	" SELECT fi.nombre , fibo.codigo_base_operacion," 				+
	"        bo.descripcion as unodes, bo.codigo_producto_banco, "  			+
	"        pb.descripcion desdos, bo.dias_minimos_primer_pago, "  			+
	"        bo.opera_factoraje"  			+
	"   FROM mg_financieras fi,"   +
	"        no_financieras_x_bo fibo,"  			+
	"        no_bases_de_operacion bo,"   					+
	"        no_productos_banco pb,"   			+
	"        comcat_if i,"   			+
	"        comcat_financiera f"   			+
	"  WHERE fibo.codigo_base_operacion = bo.codigo_base_operacion"   			+
	"    AND fi.tipo_financiera = fibo.tipo_financiera"   +
	"    AND fi.codigo_financiera = fibo.codigo_financiera"   +
	"    AND bo.codigo_empresa = pb.codigo_empresa"   						  +
	"    AND bo.codigo_producto_banco = pb.codigo_producto_banco"    +
	"    AND fi.codigo_financiera = i.ic_financiera" +
	"    AND fi.tipo_financiera = f.ic_tipo_financiera"+
	"    AND i.ic_financiera = f.ic_financiera" +
	"    AND ic_if = ? " );
		valoresBind = new ArrayList();
		valoresBind.add(intermediario);
	
	if(!baseOperacion.equals("")){
			strQuery.append(" AND fibo.codigo_base_operacion = ? ");
			valoresBind.add(baseOperacion);
		}
	strQuery.append(" ORDER BY nombre ");
					
	log.debug(" strQuery para generar archivo- - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.debug("getDocumentQueryFile (S)<- - - ");
		
	return strQuery.toString();
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
public  String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		
	log.debug("crearCustomFile (E)");
	String nombreArchivo ="";
	HttpSession session = request.getSession();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	OutputStreamWriter writer = null;
	BufferedWriter buffer = null;
	int total = 0;
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		buffer = new BufferedWriter(writer);
			
		contenidoArchivo = new StringBuffer();	
		contenidoArchivo.append( "Nombre IF ,C�DIGO BASE OPERACI�N,DESCRIPCI�N DE LA PARAMETRIZACI�N DE BASES DE OPERACI�N,C�DIGO PRODUCTO BANCO,DESCRIPCI�N PRODUCTO BANCO,DIAS MINIMOS PRIMER PAGO,OPERA FACTORAJE\n");
			
		while (rs.next())	{
			String nombreif 	= (rs.getString("NOMBRE") 						== null) ? "" : rs.getString("NOMBRE");
			String codbasop 	= (rs.getString("CODIGO_BASE_OPERACION") 	== null) ? "" : rs.getString("CODIGO_BASE_OPERACION");
			String unodes 		= (rs.getString("UNODES") 					== null) ? "" : rs.getString("UNODES");				
			String codproban	= (rs.getString("CODIGO_PRODUCTO_BANCO") 	== null) ? "" : rs.getString("CODIGO_PRODUCTO_BANCO");
			String desdos	 	= (rs.getString("DESDOS") 					== null) ? "" : rs.getString("DESDOS");				
			String diasmin		= (rs.getString("DIAS_MINIMOS_PRIMER_PAGO")== null) ? "" : rs.getString("DIAS_MINIMOS_PRIMER_PAGO");
			String opfac	 	= (rs.getString("OPERA_FACTORAJE") 		== null) ? "" : rs.getString("OPERA_FACTORAJE");
			
			contenidoArchivo.append(
			nombreif.replace	(',',' ')+","+
			codbasop.replace	(',',' ')+","+		
			unodes.replace		(',',' ')+","+
			codproban.replace	(',',' ')+","+
			desdos.replace		(',',' ')+","+
			diasmin.replace	(',',' ')+","+
			opfac.replace		(',',' ')+","+ "\n");
		}
			
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
		
		}catch (Exception e){
			throw new AppException("Error al generar el archivo ", e);
		}
	log.debug("crearCustomFile (S)");
	return nombreArchivo;
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
	String nombreArchivo = "";
	HttpSession session = request.getSession();	
	ComunesPDF pdfDoc = new ComunesPDF();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
		pdfDoc.setTable(7,90);
			
		pdfDoc.setCell("Nombre IF"								,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("C�digo Base Operaci�n"				,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Descripci�n de la Parametrizaci�n de Bases de Operaci�n","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("C�dig Producto Banco"				,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Descripci�n Producto Banco"		,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("D�as minimos Primer pago"			,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Opera Factoraje"						,"celda01",ComunesPDF.CENTER);

			
		while(reg.next()){
			String nombreif 	= (reg.getString("NOMBRE") 						== null) ? "" : reg.getString("NOMBRE");
			String codbasop 	= (reg.getString("CODIGO_BASE_OPERACION") 	== null) ? "" : reg.getString("CODIGO_BASE_OPERACION");
			String unodes 		= (reg.getString("UNODES") 					== null) ? "" : reg.getString("UNODES");				
			String codproban	= (reg.getString("CODIGO_PRODUCTO_BANCO") 	== null) ? "" : reg.getString("CODIGO_PRODUCTO_BANCO");
			String desdos	 	= (reg.getString("DESDOS") 					== null) ? "" : reg.getString("DESDOS");				
			String diasmin		= (reg.getString("DIAS_MINIMOS_PRIMER_PAGO")== null) ? "" : reg.getString("DIAS_MINIMOS_PRIMER_PAGO");
			String opfac	 	= (reg.getString("OPERA_FACTORAJE") 		== null) ? "" : reg.getString("OPERA_FACTORAJE");

			pdfDoc.setCell(nombreif		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(codbasop	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(unodes		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(codproban	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(desdos		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(diasmin		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(opfac		,"formas",ComunesPDF.CENTER);

		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
	} catch(Exception e){
		throw new AppException("Error al generar el archivo PDF ", e);
	}
	return nombreArchivo;
	}


	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}


	public String getIntermediario() {
		return intermediario;
	}


	public void setBaseOperacion(String baseOperacion) {
		this.baseOperacion = baseOperacion;
	}


	public String getBaseOperacion() {
		return baseOperacion;
	}

}