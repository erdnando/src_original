package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;

public class ConsGenAutCvesRFCCA2 implements IQueryGeneratorRegExtJS {
	StringBuffer  qrySentencia;
   private List  conditions;
   private String  operacion;
   private String  paginaOffset;
   private String  paginaNo;
  
   private String  rfc;
   private String  NomPyme;
   private String  txt_fecha_de;
   private String  txt_fecha_a;
   private String  accion;
	
	private String 	idif;
	private String 	idepo;
   private String 	publicacion;	

	public ConsGenAutCvesRFCCA2() {
		//Constructor
	}
	
	public String getAggregateCalculationQuery() {
		return "";
	}//getAggregateCalculationQuery
	
	/**
   * M�todo que arma el query para obtener los ids de la consulta paginada
   * Nota: es necesario la lista de conditions para las condiciones del where
   * 
   * @return String regresa la cadena armada con el query
   * */
	public String getDocumentQuery(){
		System.out.println("getDocumentQuery: (E) ");

		qrySentencia  = new StringBuffer();
		conditions    = new ArrayList();
	
		qrySentencia.append(" SELECT  DISTINCT /*+ use_nl(ceac" );
		
		if( (rfc!=null&&!"".equals(rfc)) || (NomPyme!=null&&!"".equals(NomPyme)) ) 
			qrySentencia.append(", cpy");
		if( NomPyme!=null&&!"".equals(NomPyme) || idif!=null&&!"".equals(idif) || idepo!=null&&!"".equals(idepo) ) 
			qrySentencia.append(", cpe, ce");
			
		qrySentencia.append(" ) */ CEAC.IC_ENTREGA_AUT_CLAVES , cpe.ic_pyme,  cpe.ic_epo " );
		
		qrySentencia.append("  FROM COM_ENTREGA_AUT_CLAVES CEAC, "+
					"					comcat_pyme cpy, "+
					" 				comrel_pyme_epo cpe, "+
					"					comcat_epo ce, "+
					"					comrel_nafin crn, " +
					"  				(select ic_pyme, cg_celular from com_contacto where cs_primer_contacto = 'S' and ic_pyme is not null) cc " +	
					" WHERE CEAC.CS_TIPO_ENTREGA 	= 2 " +
					" and ceac.ic_pyme        = cpy.ic_pyme " +
					" and ceac.ic_pyme        = cpe.ic_pyme " +
					" and ceac.ic_pyme        = crn.ic_epo_pyme_if " +
					" and ceac.ic_pyme       	= cc.ic_pyme (+) " +
					" and cpe.ic_epo          = ce.ic_epo " );
		
		
		qrySentencia.append(" and  CEAC.CS_TIPO_ENTREGA = 2 " );

		if( (rfc!=null&&!"".equals(rfc)) ||  (NomPyme!=null&&!"".equals(NomPyme))) 
			qrySentencia.append(" and ceac.ic_pyme = cpy.ic_pyme ");

		//Fecha 1	 COM_ENTREGA_AUT_CLAVES
		if(txt_fecha_de!=null&&!"".equals(txt_fecha_de)) {
			qrySentencia.append(" AND CEAC.df_alta > TO_DATE (?, 'dd/mm/yyyy') ");
			conditions.add(txt_fecha_de);
		}

		//Fecha	2	 COM_ENTREGA_AUT_CLAVES
		if(txt_fecha_a!=null&&!"".equals(txt_fecha_a)) {
			qrySentencia.append(" AND CEAC.df_alta < TO_DATE (?, 'dd/mm/yyyy') +1 ");
			conditions.add(txt_fecha_a);
		}   

		if( NomPyme!=null&&!"".equals(NomPyme) || idif!=null&&!"".equals(idif) || idepo!=null&&!"".equals(idepo) ) {
			qrySentencia.append(
							" and ceac.ic_pyme = cpe.ic_pyme " +
							" and cpe.ic_epo   = ce.ic_epo " 		);
		}

		//Solo con Publicacion  COM_ENTREGA_AUT_CLAVES
		if("s".equals(publicacion)) {
			qrySentencia.append(" AND CEAC.cs_publicacion = ? ");
			conditions.add("1");
		}   

		//RFC comcat_pyme				
		if(rfc!=null&&!"".equals(rfc)) {
			qrySentencia.append(" AND cpy.cg_rfc = ? ");
			conditions.add(rfc);
		}

		//Nombre PYME	comcat_pyme	 
		if(NomPyme!=null&&!"".equals(NomPyme)) {
	 		String strTemp = NomPyme;
			NomPyme = "%" + NomPyme + "%";
			qrySentencia.append(" AND cpy.cg_razon_social like ? ");
			conditions.add(NomPyme);
			NomPyme = strTemp;
		}

		//Banco Fondeo comcat_epo
		if(idif!=null&&!"".equals(idif)) {
			qrySentencia.append(" AND CE.IC_BANCO_FONDEO = ? ");
			conditions.add(new Integer(idif));
		} 
	
		//EPO	 comcat_epo
		if(idepo!=null&&!"".equals(idepo)) {
			qrySentencia.append(" AND CE.IC_EPO = ? ");
			conditions.add(new Integer(idepo));
		} 		
		
		qrySentencia.append(" ORDER BY ceac.ic_entrega_aut_claves ");

		System.out.println("qrySentencia: "+qrySentencia);
		System.out.println("conditions: "+conditions);
		
		System.out.println("getDocumentQuery: (S) ");
    
		return qrySentencia.toString();
	}//getDocumentQuery
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		System.out.println("getDocumentSummaryQueryForIds: (E) ");
		
		qrySentencia  = new StringBuffer();
		conditions    = new ArrayList();
		qrySentencia.append(
			" SELECT   DISTINCT /*+ use_nl(ceac, cpy, cpe, cc, ce, crn)*/ ce.cg_razon_social as EPO, " +
			"         cpy.cg_razon_social as PYME, " +
			"         cpy.cg_rfc as PYME_RFC , " +
			"         cpy.CG_NOMBREREP_LEGAL || ' ' || cpy.CG_APPAT_REP_LEGAL || ' ' || cpy.CG_APMAT_LEGAL as REPRESENTANTE_LEGAL, " +
			"         cpy.CG_TELEFONO as TELEFONO, " +
			"         crn.ic_nafin_electronico as NAFIN_ELECTRONICO,  " +
			"         CEAC.CG_CLAVE_USUARIO,  " +
			"         cc.cg_celular as CELULAR, " +
			"         CEAC.CG_NOMBRE,  " +
			"         CEAC.CG_EMAIL as CG_EMAIL_REGISTRADO,  " +
			"         CEAC.CG_EMAIL_PROP, " +
			"         TO_CHAR(CEAC.DF_ALTA,'DD/MM/YYYY hh24:mi:ss') as FECHA,  " +			
			"         CEAC.CS_ENTREGO_USUARIO,  " +
			"         CEAC.CS_PUBLICACION,  " +			
			"         'ClasesIndividuales:ConsGenAutCvesRFCCA:method:getDocumentSummaryQueryForIds' ,  " +
			"         ceac.ic_entrega_aut_claves ,cpe.ic_pyme  , cpe.ic_epo  " +
			" FROM COM_ENTREGA_AUT_CLAVES CEAC, "+
			"					comcat_pyme cpy, "+
			" 				comrel_pyme_epo cpe, "+
			"					comcat_epo ce, "+
			"					comrel_nafin crn, " +
			"  				(select ic_pyme, cg_celular from com_contacto where cs_primer_contacto = 'S' and ic_pyme is not null) cc " +	
			" WHERE  " +
			" 		ceac.ic_pyme = cpe.ic_pyme " +
			" and ceac.ic_pyme = cpy.ic_pyme " +
			" and ceac.ic_pyme = crn.ic_epo_pyme_if " +		
			" and ceac.ic_pyme = cc.ic_pyme (+) " +
			" and cpe.ic_epo   = ce.ic_epo " +
			" AND (" ); 	
		
		
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0) {
				qrySentencia.append("  OR  "); 
			}
			qrySentencia.append("  (  CEAC.ic_entrega_aut_claves = ?   "+
									  " and  cpe.ic_pyme  = ?   "+
									  " and  cpe.ic_epo  =  ?  ) "); 
			conditions.add(new Long(lItem.get(0).toString()));
			conditions.add(new Long(lItem.get(1).toString()));
			conditions.add(new Long(lItem.get(2).toString()));
		}//for(int i=0;i<ids.size();i++)

		qrySentencia.append(")  ");
		
		qrySentencia.append(" AND CEAC.CS_TIPO_ENTREGA = ? ");
		conditions.add(new Integer(2));

		//Banco Fondeo comcat_epo
		if(idif!=null&&!"".equals(idif)) {
			qrySentencia.append(" AND CE.IC_BANCO_FONDEO = ? ");
			conditions.add(new Integer(idif));
		} 
	
		//EPO	 comcat_epo
		if(idepo!=null&&!"".equals(idepo)) {
			qrySentencia.append(" AND CE.IC_EPO = ? ");
			conditions.add(new Integer(idepo));
		} 

		qrySentencia.append(" and crn.cg_tipo = 'P' ");
		
		
			if( (rfc!=null&&!"".equals(rfc)) ||  (NomPyme!=null&&!"".equals(NomPyme))) 
			qrySentencia.append(" and ceac.ic_pyme = cpy.ic_pyme ");

		//Fecha 1	 COM_ENTREGA_AUT_CLAVES
		if(txt_fecha_de!=null&&!"".equals(txt_fecha_de)) {
			qrySentencia.append(" AND CEAC.df_alta > TO_DATE (?, 'dd/mm/yyyy') ");
			conditions.add(txt_fecha_de);
		}

		//Fecha	2	 COM_ENTREGA_AUT_CLAVES
		if(txt_fecha_a!=null&&!"".equals(txt_fecha_a)) {
			qrySentencia.append(" AND CEAC.df_alta < TO_DATE (?, 'dd/mm/yyyy') +1 ");
			conditions.add(txt_fecha_a);
		}   

		if( NomPyme!=null&&!"".equals(NomPyme) || idif!=null&&!"".equals(idif) || idepo!=null&&!"".equals(idepo) ) {
			qrySentencia.append(
							" and ceac.ic_pyme = cpe.ic_pyme " +
							" and cpe.ic_epo   = ce.ic_epo " 		);
		}

		//Solo con Publicacion  COM_ENTREGA_AUT_CLAVES
		if("s".equals(publicacion)) {
			qrySentencia.append(" AND CEAC.cs_publicacion = ? ");
			conditions.add("1");
		}   

		//RFC comcat_pyme				
		if(rfc!=null&&!"".equals(rfc)) {
			qrySentencia.append(" AND cpy.cg_rfc = ? ");
			conditions.add(rfc);
		}

		//Nombre PYME	comcat_pyme	 
		if(NomPyme!=null&&!"".equals(NomPyme)) {
	 		String strTemp = NomPyme;
			NomPyme = "%" + NomPyme + "%";
			qrySentencia.append(" AND cpy.cg_razon_social like ? ");
			conditions.add(NomPyme);
			NomPyme = strTemp;
		}

		//Banco Fondeo comcat_epo
		if(idif!=null&&!"".equals(idif)) {
			qrySentencia.append(" AND CE.IC_BANCO_FONDEO = ? ");
			conditions.add(new Integer(idif));
		} 
	
		//EPO	 comcat_epo
		if(idepo!=null&&!"".equals(idepo)) {
			qrySentencia.append(" AND CE.IC_EPO = ? ");
			conditions.add(new Integer(idepo));
		} 	
		
		qrySentencia.append(" ORDER BY ceac.ic_entrega_aut_claves, cpe.ic_pyme  , cpe.ic_epo ");
		
		System.out.println("qrySentencia: "+qrySentencia);
		System.out.println("conditions: "+conditions);
    
		System.out.println("getDocumentSummaryQueryForIds: (S) ");
		return qrySentencia.toString();
	}//getDocumentSummaryQueryForIds
	
	public String getDocumentQueryFile(){
		System.out.println("getDocumentQueryFile: (E) ");
		
		qrySentencia  = new StringBuffer();
		conditions    = new ArrayList();
		qrySentencia.append(		
					" SELECT  DISTINCT /*+ use_nl(ceac, cpy, cpe, cc, ce, crn)*/ " + 
					" 				ce.cg_razon_social as EPO, " +
					"         crn.ic_nafin_electronico as NAFIN_ELECTRONICO,  " +
					"         cpy.cg_razon_social as NOMBRE_RAZON_SOCIAL, " +
					"         cpy.cg_rfc as RFC, " +
					"         cpy.CG_NOMBREREP_LEGAL || ' ' || cpy.CG_APPAT_REP_LEGAL || ' ' || cpy.CG_APMAT_LEGAL as REPRESENTANTE_LEGAL, " +
					"         CEAC.CG_CLAVE_USUARIO CLAVE_USUARIO,  " +
					"         cpy.CG_TELEFONO as TELEFONO, " +
					"         cc.cg_celular as CELULAR, " +
					"         CEAC.CG_NOMBRE NOMBRE_CONTACTO,  " +
					"         CEAC.CG_EMAIL as EMAIL_REGISTRADO,  " +
					"         CEAC.CG_EMAIL_PROP EMAIL_PROPORCIONADO, " +
					"         TO_CHAR(CEAC.DF_ALTA,'DD/MM/YYYY hh24:mi:ss') as FECHA_DE_SOLICITUD,  " +
					"         DECODE(CEAC.CS_ENTREGO_USUARIO,1,'Si','No') as ENTREGO_USUARIO,  " +
					"         DECODE(CEAC.CS_PUBLICACION,1,'Si','No') as PUBLICACION ,  " +
					"         ceac.ic_entrega_aut_claves , cpe.ic_pyme  , cpe.ic_epo  " +
					" FROM COM_ENTREGA_AUT_CLAVES CEAC, "+
					"					comcat_pyme cpy, "+
					" 				comrel_pyme_epo cpe, "+
					"					comcat_epo ce, "+
					"					comrel_nafin crn, " +
					"  				(select ic_pyme, cg_celular from com_contacto where cs_primer_contacto = 'S' and ic_pyme is not null) cc " +	
					" WHERE CEAC.CS_TIPO_ENTREGA 	= 2 " +
					" and ceac.ic_pyme        = cpy.ic_pyme " +
					" and ceac.ic_pyme        = cpe.ic_pyme " +
					" and ceac.ic_pyme        = crn.ic_epo_pyme_if " +
					" and ceac.ic_pyme       	= cc.ic_pyme (+) " +
					" and cpe.ic_epo          = ce.ic_epo " );
					
					///////////////////////////
				qrySentencia.append(" and ceac.ic_entrega_aut_claves in ( "); 
					 
					 
					qrySentencia.append(" SELECT  DISTINCT /*+ use_nl(ceac" );
		
					if( (rfc!=null&&!"".equals(rfc)) || (NomPyme!=null&&!"".equals(NomPyme)) ) {
						qrySentencia.append(", cpy");}
					if( NomPyme!=null&&!"".equals(NomPyme) || idif!=null&&!"".equals(idif) || idepo!=null&&!"".equals(idepo) ) {
						qrySentencia.append(", cpe, ce");}
			
					qrySentencia.append(" ) */ CEAC.IC_ENTREGA_AUT_CLAVES " );
		
					qrySentencia.append(" FROM com_entrega_aut_claves ceac ");					
		
					if( (rfc!=null&&!"".equals(rfc)) || (NomPyme!=null&&!"".equals(NomPyme)) ) {
						qrySentencia.append(", comcat_pyme cpy ");}
					if( NomPyme!=null&&!"".equals(NomPyme) || idif!=null&&!"".equals(idif) || idepo!=null&&!"".equals(idepo) ) {
						qrySentencia.append(", comrel_pyme_epo cpe, comcat_epo ce ");}

					qrySentencia.append(" WHERE CEAC.CS_TIPO_ENTREGA = 2 " );

					if( (rfc!=null&&!"".equals(rfc)) ||  (NomPyme!=null&&!"".equals(NomPyme))) {
						qrySentencia.append(" and ceac.ic_pyme = cpy.ic_pyme ");}

					//Fecha 1	 COM_ENTREGA_AUT_CLAVES
					if(txt_fecha_de!=null&&!"".equals(txt_fecha_de)) {
						qrySentencia.append(" AND CEAC.df_alta > TO_DATE (?, 'dd/mm/yyyy') ");
						conditions.add(txt_fecha_de);
					}

					//Fecha	2	 COM_ENTREGA_AUT_CLAVES
					if(txt_fecha_a!=null&&!"".equals(txt_fecha_a)) {
						qrySentencia.append(" AND CEAC.df_alta < TO_DATE (?, 'dd/mm/yyyy') +1 ");
						conditions.add(txt_fecha_a);
					}   

					if( NomPyme!=null&&!"".equals(NomPyme) || idif!=null&&!"".equals(idif) || idepo!=null&&!"".equals(idepo) ) {
						qrySentencia.append(
							" and ceac.ic_pyme = cpe.ic_pyme " +
							" and cpe.ic_epo   = ce.ic_epo " 		);
					}

					//Solo con Publicacion  COM_ENTREGA_AUT_CLAVES
					if("s".equals(publicacion)) {
						qrySentencia.append(" AND CEAC.cs_publicacion = ? ");
						conditions.add("1");
					}   

					//RFC comcat_pyme				
					if(rfc!=null&&!"".equals(rfc)) {
						qrySentencia.append(" AND cpy.cg_rfc = ? ");
						conditions.add(rfc);
					}

					//Nombre PYME	comcat_pyme	 
					if(NomPyme!=null&&!"".equals(NomPyme)) {
						String strTemp = NomPyme;
						NomPyme = "%" + NomPyme + "%";
						qrySentencia.append(" AND cpy.cg_razon_social like ? ");
						conditions.add(NomPyme);
						NomPyme = strTemp;
					}

					//Banco Fondeo comcat_epo
					if(idif!=null&&!"".equals(idif)) {
						qrySentencia.append(" AND CE.IC_BANCO_FONDEO = ? ");
						conditions.add(new Integer(idif));
					} 
	
					//EPO	 comcat_epo
					if(idepo!=null&&!"".equals(idepo)) {
						qrySentencia.append(" AND CE.IC_EPO = ? ");
						conditions.add(new Integer(idepo));
					} 		
		
					//qrySentencia.append(" ORDER BY ceac.ic_entrega_aut_claves ");
					 
				qrySentencia.append(" )");		 
				//////////////////////////////
					
    
		if(rfc!=null&&!"".equals(rfc)) {
			qrySentencia.append(" AND cpy.cg_rfc = ? ");
			conditions.add(rfc);
		}
		
		if(NomPyme!=null&&!"".equals(NomPyme)) {
			String strTemp = NomPyme;
			NomPyme = "%" + NomPyme + "%";	 
			qrySentencia.append(" AND cpy.cg_razon_social like ? ");
			conditions.add(NomPyme);
			NomPyme = strTemp;
		}
		
		if(txt_fecha_de!=null&&!"".equals(txt_fecha_de)) {
			qrySentencia.append(" AND CEAC.df_alta > TO_DATE (?, 'dd/mm/yyyy') ");
			conditions.add(txt_fecha_de);
		}
		
		if(txt_fecha_a!=null&&!"".equals(txt_fecha_a)) {
			qrySentencia.append(" AND CEAC.df_alta < TO_DATE (?, 'dd/mm/yyyy') +1 ");
			conditions.add(txt_fecha_a);
		}   

		if("s".equals(publicacion)) {
			qrySentencia.append(" AND CEAC.cs_publicacion = ? ");
			conditions.add("1");
		}   

		if(idif!=null&&!"".equals(idif)) {
			qrySentencia.append(" AND CE.IC_BANCO_FONDEO = ? ");
			conditions.add(new Integer(idif));
		} 
		
		if(idepo!=null&&!"".equals(idepo)) {
			qrySentencia.append(" AND CE.IC_EPO = ? ");
			conditions.add(new Integer(idepo));
		} 		
		qrySentencia.append(" and crn.cg_tipo = 'P'	");
		
		
			if( (rfc!=null&&!"".equals(rfc)) ||  (NomPyme!=null&&!"".equals(NomPyme))) 
			qrySentencia.append(" and ceac.ic_pyme = cpy.ic_pyme ");

		//Fecha 1	 COM_ENTREGA_AUT_CLAVES
		if(txt_fecha_de!=null&&!"".equals(txt_fecha_de)) {
			qrySentencia.append(" AND CEAC.df_alta > TO_DATE (?, 'dd/mm/yyyy') ");
			conditions.add(txt_fecha_de);
		}

		//Fecha	2	 COM_ENTREGA_AUT_CLAVES
		if(txt_fecha_a!=null&&!"".equals(txt_fecha_a)) {
			qrySentencia.append(" AND CEAC.df_alta < TO_DATE (?, 'dd/mm/yyyy') +1 ");
			conditions.add(txt_fecha_a);
		}   

		if( NomPyme!=null&&!"".equals(NomPyme) || idif!=null&&!"".equals(idif) || idepo!=null&&!"".equals(idepo) ) {
			qrySentencia.append(
							" and ceac.ic_pyme = cpe.ic_pyme " +
							" and cpe.ic_epo   = ce.ic_epo " 		);
		}

		//Solo con Publicacion  COM_ENTREGA_AUT_CLAVES
		if("s".equals(publicacion)) {
			qrySentencia.append(" AND CEAC.cs_publicacion = ? ");
			conditions.add("1");
		}   

		//RFC comcat_pyme				
		if(rfc!=null&&!"".equals(rfc)) {
			qrySentencia.append(" AND cpy.cg_rfc = ? ");
			conditions.add(rfc);
		}

		//Nombre PYME	comcat_pyme	 
		if(NomPyme!=null&&!"".equals(NomPyme)) {
	 		String strTemp = NomPyme;
			NomPyme = "%" + NomPyme + "%";
			qrySentencia.append(" AND cpy.cg_razon_social like ? ");
			conditions.add(NomPyme);
			NomPyme = strTemp;
		}

		//Banco Fondeo comcat_epo
		if(idif!=null&&!"".equals(idif)) {
			qrySentencia.append(" AND CE.IC_BANCO_FONDEO = ? ");
			conditions.add(new Integer(idif));
		} 
	
		//EPO	 comcat_epo
		if(idepo!=null&&!"".equals(idepo)) {
			qrySentencia.append(" AND CE.IC_EPO = ? ");
			conditions.add(new Integer(idepo));
		} 	
		
			if( (rfc!=null&&!"".equals(rfc)) ||  (NomPyme!=null&&!"".equals(NomPyme))) 
			qrySentencia.append(" and ceac.ic_pyme = cpy.ic_pyme ");

		//Fecha 1	 COM_ENTREGA_AUT_CLAVES
		if(txt_fecha_de!=null&&!"".equals(txt_fecha_de)) {
			qrySentencia.append(" AND CEAC.df_alta > TO_DATE (?, 'dd/mm/yyyy') ");
			conditions.add(txt_fecha_de);
		}

		//Fecha	2	 COM_ENTREGA_AUT_CLAVES
		if(txt_fecha_a!=null&&!"".equals(txt_fecha_a)) {
			qrySentencia.append(" AND CEAC.df_alta < TO_DATE (?, 'dd/mm/yyyy') +1 ");
			conditions.add(txt_fecha_a);
		}   

		if( NomPyme!=null&&!"".equals(NomPyme) || idif!=null&&!"".equals(idif) || idepo!=null&&!"".equals(idepo) ) {
			qrySentencia.append(
							" and ceac.ic_pyme = cpe.ic_pyme " +
							" and cpe.ic_epo   = ce.ic_epo " 		);
		}

		//Solo con Publicacion  COM_ENTREGA_AUT_CLAVES
		if("s".equals(publicacion)) {
			qrySentencia.append(" AND CEAC.cs_publicacion = ? ");
			conditions.add("1");
		}   

		//RFC comcat_pyme				
		if(rfc!=null&&!"".equals(rfc)) {
			qrySentencia.append(" AND cpy.cg_rfc = ? ");
			conditions.add(rfc);
		}

		//Nombre PYME	comcat_pyme	 
		if(NomPyme!=null&&!"".equals(NomPyme)) {
	 		String strTemp = NomPyme;
			NomPyme = "%" + NomPyme + "%";
			qrySentencia.append(" AND cpy.cg_razon_social like ? ");
			conditions.add(NomPyme);
			NomPyme = strTemp;
		}

		//Banco Fondeo comcat_epo
		if(idif!=null&&!"".equals(idif)) {
			qrySentencia.append(" AND CE.IC_BANCO_FONDEO = ? ");
			conditions.add(new Integer(idif));
		} 
	
		//EPO	 comcat_epo
		if(idepo!=null&&!"".equals(idepo)) {
			qrySentencia.append(" AND CE.IC_EPO = ? ");
			conditions.add(new Integer(idepo));
		} 	
		
		qrySentencia.append(" ORDER BY ceac.ic_entrega_aut_claves, cpe.ic_pyme  , cpe.ic_epo  ");
    
		System.out.println("qrySentencia: "+qrySentencia);
		System.out.println("conditions: "+conditions);
    
		System.out.println("getDocumentQueryFile: (S) ");
		return qrySentencia.toString();
	}//getDocumentQueryFile
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();

		if ("CSV".equals(tipo)) {
			try {

				int registros = 0;
				String  epo= "", num_naf = "", raz_soc= "", rfc="", rep_legal= "", cve_usr ="" , telefono= "" , cel= "" , nom_cont= "" , mail_reg= "" , mail_prop= "" , fec_sol= "" , entrego= "" , pub= "";

				contenidoArchivo.append(" EPO"+ "," +
												  " NAFIN ELECTR�NICO"+ "," +
												  " NOMBRE O RAZ�N SOCIAL"+ "," +
												  " RFC"+ "," +
												  " REPRESENTANTE LEGAL"+ "," +
												  " CLAVE DE USUARIO"+ "," +
												  " TELEFONO"+ "," +
												  " CELULAR"+ "," +
												  " NOMBRE DEL CONTACTO"+ "," +
												  " EMAIL REGISTRADO"+ "," +
												  " EMAIL PROPORCIONADO"+ "," +
												  " FECHA DE SOLICITUD"+ "," +
												  " SE ENTREGO USUARIO"+ "," +
												  " TIENE PUBLICACI�N"/*+ ","*/ +"\n");
												  
				while(rs.next()){
						registros++;
						epo = (rs.getString("EPO") == null) ? "" : rs.getString("EPO");
						num_naf = (rs.getString("NAFIN_ELECTRONICO") == null) ? "" : rs.getString("NAFIN_ELECTRONICO");
						raz_soc= (rs.getString("NOMBRE_RAZON_SOCIAL") == null) ? "" : rs.getString("NOMBRE_RAZON_SOCIAL");
						rfc=(rs.getString("RFC") == null)?"":rs.getString("RFC");
						rep_legal= (rs.getString("REPRESENTANTE_LEGAL") == null) ? "" : rs.getString("REPRESENTANTE_LEGAL");
						cve_usr =(rs.getString("CLAVE_USUARIO") == null) ? "" : rs.getString("CLAVE_USUARIO");
						telefono =(rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
						cel =(rs.getString("CELULAR") == null) ? "" : rs.getString("CELULAR");
						nom_cont =(rs.getString("NOMBRE_CONTACTO") == null) ? "" : rs.getString("NOMBRE_CONTACTO");
						mail_reg =(rs.getString("EMAIL_REGISTRADO") == null) ? "" : rs.getString("EMAIL_REGISTRADO");
						mail_prop =(rs.getString("EMAIL_PROPORCIONADO") == null) ? "" : rs.getString("EMAIL_PROPORCIONADO");
						fec_sol =(rs.getString("FECHA_DE_SOLICITUD") == null) ? "" : rs.getString("FECHA_DE_SOLICITUD");
						entrego =(rs.getString("ENTREGO_USUARIO") == null) ? "" : rs.getString("ENTREGO_USUARIO");
						pub =(rs.getString("PUBLICACION") == null) ? "" : rs.getString("PUBLICACION");

						contenidoArchivo.append( epo.replaceAll(",","") +","+
                                     num_naf.replaceAll(",","") +","+
                                     raz_soc.replaceAll(",","") +","+
                                     rfc.replaceAll(",","") +","+
                                     rep_legal.replaceAll(",","") +","+
												 cve_usr.replaceAll(",","") +","+
												 telefono.replaceAll(",","") +","+
												 cel.replaceAll(",","") +","+
												 nom_cont.replaceAll(",","") +","+
												 mail_reg.replaceAll(",","") +","+
												 mail_prop.replaceAll(",","") +","+
												 fec_sol.replaceAll(",","") +","+
												 entrego.replaceAll(",","") +","+
                                     pub+"\n");
					}

				if(registros >= 1){
					if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
						nombreArchivo="ERROR";
					}else{
						nombreArchivo = archivo.nombre;
					}
				}
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}

		return nombreArchivo;
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombre = "";
		
		return nombre;
	}
	
	//GETERS
	public List getConditions() {
		return conditions;
	}
	public String getOperacion() {
		return operacion;
	}
	public String getPaginaNo() {
		return paginaNo;
	}
	public String getPaginaOffset() {
		return paginaOffset;
	}
	public String getRFC() {
		return rfc;
	}
	public String getNOMPYME() {
		return NomPyme;
	}
	public String getFECHADE() {
		return txt_fecha_de;
	}
	public String getFECHAA() {
		return txt_fecha_a;
	} 

	public String getACCION(){return accion;}

	public String getIDIF(){return idif;}
	public String getEPO(){return idepo;}

	public String getPUBLICACION(){return publicacion;}




  //SETERS
	public void setOperacion(String newOperacion) {
		operacion = newOperacion;
	}
	public void setPaginaNo(String newPaginaNo) {
		paginaNo = newPaginaNo;
	}
  public void setPaginaOffset(String newPaginaOffset) {
    paginaOffset = newPaginaOffset;
  }
  public void setRFC(String newrfc){
    rfc = newrfc;
  }
  public void setNOMPYME(String newNomPyme){
    NomPyme = newNomPyme;
  }
	public void setFECHADE(String newtxt_fecha_de){
		txt_fecha_de = newtxt_fecha_de;
	}
	public void setFECHAA(String newtxt_fecha_a){
		txt_fecha_a = newtxt_fecha_a;
	}   
  
	public void setACCION(String accion){this.accion = accion;} 
	
	public void setIDIF(String idif){this.idif = idif;}
	public void setEPO(String idepo){this.idepo = idepo;}
	
	public void setPUBLICACION(String publicacion){this.publicacion = publicacion;} 	
	
}