package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class BaseOperacionSirac implements IQueryGeneratorRegExtJS {

	private ArrayList valoresBind = new ArrayList();
	private int baseOperacion;
	private static Log log = ServiceLocator.getInstance().getLog(com.netro.cadenas.BaseOperacion.class);
		
	public BaseOperacionSirac() {
	}
	 /**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentQuery(){
	log.debug("getDocumentQuery (E) ");
	StringBuffer strQuery 	= new StringBuffer();
	valoresBind = new ArrayList();
	strQuery.append(
	"SELECT   codigo_cliente, numero_identificacion, nombres, primer_apellido,	"+
   "      segundo_apellido, sexo, fecha_de_nacimiento, razon_social,				"+
   "      adicionado_por, fecha_adicion, modificado_por, fecha_modificacion,	"+
   "      codigo_departamento, codigo_municipio, codigo_pais,						"+
   "      ccodigo_tipo_identificacion_r, no_empleados, ventas_netas,				"+
   "      codigo_subsector, codigo_rama, codigo_clase, codigo_bloqueo			"+
   " FROM mg_clientes			"+
   " WHERE codigo_cliente = ?	"+
	" ORDER BY 1					");

	valoresBind.add(String.valueOf(baseOperacion));
	log.debug(" strQuery identificadores unicos - - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.debug("getDocumentQuery (S) ");
		
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * datos a mostrar en la consulta basandose en los identificadores unicos 
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista 
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentSummaryQueryForIds(List ids){
	
	log.info("getDocumentSummaryQueryForIds(E)");
	StringBuffer strQuery 	= new StringBuffer();
	List 		conditions= new ArrayList();		
		
	strQuery.append(
	"SELECT   codigo_cliente, numero_identificacion, nombres, primer_apellido,	"+
   "      segundo_apellido, sexo, fecha_de_nacimiento, razon_social,				"+
   "      adicionado_por, fecha_adicion, modificado_por, fecha_modificacion,	"+
   "      codigo_departamento, codigo_municipio, codigo_pais,						"+
   "      ccodigo_tipo_identificacion_r, no_empleados, ventas_netas,				"+
   "      codigo_subsector, codigo_rama, codigo_clase, codigo_bloqueo			"+
   " FROM mg_clientes			"+
   " WHERE codigo_cliente = ?	"+
	" ORDER BY 1					");
	valoresBind = new ArrayList();
		valoresBind.add(String.valueOf(baseOperacion));
			
		List pKeys = new ArrayList();
		for(int i = 0; i < ids.size(); i++){
			List lItem = (ArrayList)ids.get(i);
				conditions.add(lItem);			
		}		
	
	log.info("strQuery basado en identificadores unicos - -  >" + strQuery);
	log.info("valoresBind "+ valoresBind);
	log.info("getDocumentSummaryQueryForIds(S)");	
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getAggregateCalculationQuery(){
	StringBuffer strQuery 	= new StringBuffer();
	log.info("getAggregateCalculationQuery (E)");

	strQuery.append(
	"SELECT   codigo_cliente, numero_identificacion, nombres, primer_apellido,	"+
   "      segundo_apellido, sexo, fecha_de_nacimiento, razon_social,				"+
   "      adicionado_por, fecha_adicion, modificado_por, fecha_modificacion,	"+
   "      codigo_departamento, codigo_municipio, codigo_pais,						"+
   "      ccodigo_tipo_identificacion_r, no_empleados, ventas_netas,				"+
   "      codigo_subsector, codigo_rama, codigo_clase, codigo_bloqueo			"+
   " FROM mg_clientes			"+
   " WHERE codigo_cliente = ?	"+
	" ORDER BY 1					");
						

						
	log.debug(" strQuery obtiene totales- - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.info("getAggregateCalculationQuery (S)");
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
public  List getConditions(){		
	return valoresBind;
	}
	
	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentQueryFile(){
	log.debug("getDocumentQueryFile (E)");
	StringBuffer strQuery 	= new StringBuffer();
	valoresBind = new ArrayList();

	strQuery.append(
	"SELECT   codigo_cliente, numero_identificacion, nombres, primer_apellido,	"+
   "      segundo_apellido, sexo, fecha_de_nacimiento, razon_social,				"+
   "      adicionado_por, fecha_adicion, modificado_por, fecha_modificacion,	"+
   "      codigo_departamento, codigo_municipio, codigo_pais,						"+
   "      ccodigo_tipo_identificacion_r, no_empleados, ventas_netas,				"+
   "      codigo_subsector, codigo_rama, codigo_clase, codigo_bloqueo			"+
   " FROM mg_clientes			"+
   " WHERE codigo_cliente = ?	"+
	" ORDER BY 1					");
	
	valoresBind.add(String.valueOf(baseOperacion));
					
	log.debug(" strQuery para generar archivo- - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.debug("getDocumentQueryFile (S)<- - - ");
		
	return strQuery.toString();
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
public  String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		
	log.debug("crearCustomFile (E)");
	String nombreArchivo ="";
	HttpSession session = request.getSession();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	OutputStreamWriter writer = null;
	BufferedWriter buffer = null;
	int total = 0;
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		buffer = new BufferedWriter(writer);
			
		contenidoArchivo = new StringBuffer();	
		contenidoArchivo.append( "Nombre IF ,C�DIGO BASE OPERACI�N,DESCRIPCI�N DE LA PARAMETRIZACI�N DE BASES DE OPERACI�N,C�DIGO PRODUCTO BANCO,DESCRIPCI�N PRODUCTO BANCO,DIAS MINIMOS PRIMER PAGO,OPERA FACTORAJE\n");
			
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
		
		}catch (Exception e){
			throw new AppException("Error al generar el archivo ", e);
		}
	log.debug("crearCustomFile (S)");
	return nombreArchivo;
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
	String nombreArchivo = "";
	HttpSession session = request.getSession();	
	ComunesPDF pdfDoc = new ComunesPDF();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(1, path + nombreArchivo);
			
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
		pdfDoc.setTable(2,50);	
		
		pdfDoc.setCell("Nombre del Campo","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Valor"				,"celda01",ComunesPDF.CENTER);
		
		while(reg.next()){
		
		pdfDoc.setCell("C�digo Cliente","celda01",ComunesPDF.CENTER);	
		String cc	= (reg.getString("CODIGO_CLIENTE") == null) ? "" : reg.getString("CODIGO_CLIENTE");
		pdfDoc.setCell(cc		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("RFC"	,"celda01",ComunesPDF.CENTER);	
		String rfc 	= (reg.getString("NUMERO_IDENTIFICACION") == null) ? "" : reg.getString("NUMERO_IDENTIFICACION");
		pdfDoc.setCell(rfc	,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("Nombre","celda01",ComunesPDF.CENTER);	
		String n 	= (reg.getString("NOMBRES") == null) ? "" : reg.getString("NOMBRES");
		pdfDoc.setCell(n		,"formas",ComunesPDF.CENTER);
	
		pdfDoc.setCell("Apellido Paterno","celda01",ComunesPDF.CENTER);	
		String pa 	= (reg.getString("PRIMER_APELLIDO") == null) ? "" : reg.getString("PRIMER_APELLIDO");
		pdfDoc.setCell(pa		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("Apellido Materno"	,"celda01",ComunesPDF.CENTER);	
		String sa 	= (reg.getString("SEGUNDO_APELLIDO") == null) ? "" : reg.getString("SEGUNDO_APELLIDO");
		pdfDoc.setCell(sa		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("Sexo","celda01",ComunesPDF.CENTER);	
		String sexo 	= (reg.getString("SEXO") 	== null) ? "" : reg.getString("SEXO");
		pdfDoc.setCell(sexo	,"formas",ComunesPDF.CENTER);

	try {
		pdfDoc.setCell("Fecha de Nacimiento","celda01",ComunesPDF.CENTER);	
		String fn 	= (reg.getString("FECHA_DE_NACIMIENTO") == null) ? "" : reg.getString("FECHA_DE_NACIMIENTO");
    String newFecha=(String)reg.getString("FECHA_DE_NACIMIENTO");
        String[] fechaArray = newFecha.split(" ");
        String miFecha ="";
        String miHora  ="";
        for (int zz=0; zz<fechaArray.length; zz++){
            if (zz==0)			miFecha = fechaArray[zz];
            if (zz==1)			miHora  = fechaArray[zz];
          }
		pdfDoc.setCell(miFecha+" 00:00:00.0","formas",ComunesPDF.CENTER);
	}catch(Exception e ){
		pdfDoc.setCell(""		,"formas",ComunesPDF.CENTER);		
	}
		
		pdfDoc.setCell("Raz�n social" ,"celda01",ComunesPDF.CENTER);	
		String rz 	= (reg.getString("RAZON_SOCIAL") == null) ? "" : reg.getString("RAZON_SOCIAL");
		pdfDoc.setCell(rz		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("Adicionado por","celda01",ComunesPDF.CENTER);	
		String ap 	= (reg.getString("ADICIONADO_POR") 	== null) ? "" : reg.getString("ADICIONADO_POR");
		pdfDoc.setCell(ap		,"formas",ComunesPDF.CENTER);

	try {
		pdfDoc.setCell("Fecha de Adici�n","celda01",ComunesPDF.CENTER);	
		String fa 	= (reg.getString("FECHA_ADICION") == null) ? "" : reg.getString("FECHA_ADICION");
		pdfDoc.setCell(fa		,"formas",ComunesPDF.CENTER);
	}catch(Exception e ){
		pdfDoc.setCell(""		,"formas",ComunesPDF.CENTER);		
	}
		
		pdfDoc.setCell("Modificado Por","celda01",ComunesPDF.CENTER);	
		String mp 	= (reg.getString("MODIFICADO_POR") 	== null) ? "" : reg.getString("MODIFICADO_POR");
		pdfDoc.setCell(mp		,"formas",ComunesPDF.CENTER);

	
	try {
		pdfDoc.setCell("Fecha de modificaci�n"	,"celda01",ComunesPDF.CENTER);	
		String fm 	= (reg.getString("FECHA_MODIFICACION") 						== null) ? "" : reg.getString("FECHA_MODIFICACION");
		pdfDoc.setCell(fm		,"formas",ComunesPDF.CENTER);
	}catch(Exception e ){
		pdfDoc.setCell(""		,"formas",ComunesPDF.CENTER);		
	}
			
		pdfDoc.setCell("C�digo departamento","celda01",ComunesPDF.CENTER);	
		String cd 	= (reg.getString("CODIGO_DEPARTAMENTO") == null) ? "" : reg.getString("CODIGO_DEPARTAMENTO");
		pdfDoc.setCell(cd		,"formas",ComunesPDF.CENTER);	

		pdfDoc.setCell("C�digo municipio","celda01",ComunesPDF.CENTER);	
		String cm 	= (reg.getString("CODIGO_MUNICIPIO") == null) ? "" : reg.getString("CODIGO_MUNICIPIO");
		pdfDoc.setCell(cm		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("C�digo pa�s","celda01",ComunesPDF.CENTER);	
		String cp 	= (reg.getString("CODIGO_PAIS") == null) ? "" : reg.getString("CODIGO_PAIS");
		pdfDoc.setCell(cp		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("C�digo tipo de Identificaci�n","celda01",ComunesPDF.CENTER);	
		String cti 	= (reg.getString("CCODIGO_TIPO_IDENTIFICACION_R") 						== null) ? "" : reg.getString("CCODIGO_TIPO_IDENTIFICACION_R");
		pdfDoc.setCell(cti		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("No empleados"		,"celda01",ComunesPDF.CENTER);	
		String ne 	= (reg.getString("NO_EMPLEADOS") == null) ? "" : reg.getString("NO_EMPLEADOS");
		pdfDoc.setCell(ne		,"formas",ComunesPDF.CENTER);
	
		pdfDoc.setCell("Ventas netas"	,"celda01",ComunesPDF.CENTER);	
		String vn 	= (reg.getString("VENTAS_NETAS") == null) ? "" : reg.getString("VENTAS_NETAS");
		pdfDoc.setCell(vn		,"formas",ComunesPDF.CENTER);
	
		pdfDoc.setCell("C�digo Subsector","celda01",ComunesPDF.CENTER);	
		String css 	= (reg.getString("CODIGO_SUBSECTOR") == null) ? "" : reg.getString("CODIGO_SUBSECTOR");
		pdfDoc.setCell(css	,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("C�digo Rama "	,"celda01",ComunesPDF.CENTER);	
		String cr 	= (reg.getString("CODIGO_RAMA") == null) ? "" : reg.getString("CODIGO_RAMA");
		pdfDoc.setCell(cr		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("C�digo clase ","celda01",ComunesPDF.CENTER);	
		String ccl 	= (reg.getString("CODIGO_CLASE") == null) ? "" : reg.getString("CODIGO_CLASE");
		pdfDoc.setCell(ccl		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("C�digo bloqueo "	,"celda01",ComunesPDF.CENTER);	
		String cb 	= (reg.getString("CODIGO_BLOQUEO") == null) ? "" : reg.getString("CODIGO_BLOQUEO");
		pdfDoc.setCell(cb		,"formas",ComunesPDF.CENTER);
		
		}		
		pdfDoc.addTable();		
			
		
		pdfDoc.endDocument();
	} catch(Exception e){
		throw new AppException("Error al generar el archivo PDF ", e);
	}
	return nombreArchivo;
	}


	public void setBaseOperacion(int baseOperacion) {
		this.baseOperacion = baseOperacion;
	}


	public int getBaseOperacion() {
		return baseOperacion;
	}



}