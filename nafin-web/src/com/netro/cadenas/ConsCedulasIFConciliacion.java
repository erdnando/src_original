package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCedulasIFConciliacion implements IQueryGeneratorRegExtJS {
    
    //Variable para enviar mensajes al log.
            private final static Log log = ServiceLocator.getInstance().getLog(ConsCedulasIFConciliacion.class);
            private String  paginaOffset;
            private String  paginaNo;
            private List            conditions;
            StringBuffer qrySentencia = new StringBuffer("");
            private String cs_tipo;
            private String sClaveIF;
            private String sClaveMoneda;
            private String sDateIni;
            private String sDateFin;
            private String tipoConsulta;
            private String fechaCorte;
            private String fechaCarga;
            private String tipoLinea;
            private String numSirac;
            private String estatus;

    
    public ConsCedulasIFConciliacion() {
    }

        public String getAggregateCalculationQuery() {
                log.info("getAggregateCalculationQuery(E)");
                qrySentencia    = new StringBuffer();
                conditions              = new ArrayList();
                
                
                
                log.info("qrySentencia  "+qrySentencia);
                log.info("conditions "+conditions);
                log.info("getAggregateCalculationQuery(S)");
                return qrySentencia.toString(); 
        }
        
        public String getDocumentQuery() {
                
                log.info("getDocumentQuery(E)");
                qrySentencia    = new StringBuffer();
                conditions              = new ArrayList();
                        
                
                
                log.info("qrySentencia  "+qrySentencia);
                log.info("conditions "+conditions);
                log.info("getDocumentQuery(S)");
                return qrySentencia.toString(); 
        }
        
        public String getDocumentSummaryQueryForIds(List pageIds) {
        
                log.info("getDocumentSummaryQueryForIds(E)");
                qrySentencia    = new StringBuffer();
                conditions              = new ArrayList();
                
                
                
                log.info("qrySentencia  "+qrySentencia);
                log.info("conditions "+conditions);
                log.info("getDocumentSummaryQueryForIds(S)");
                return qrySentencia.toString(); 
        }
        
        public String getDocumentQueryFile() {
                log.info("getDocumentQueryFile(E)");
                qrySentencia    = new StringBuffer();
                conditions              = new ArrayList();  
                if(sClaveIF.equals("T")){
                        if( !"C".equals(tipoLinea) ){
                        qrySentencia.append(" SELECT /*+use_nl(c i m) index(c in_com_estado_cuenta_01_nuk) ordered*/"   +
                                                "  i.cg_razon_social,"   +
                                                "  m.cd_nombre, "   +  
                                                "  i.ic_if,  "   +  
                                                "  m.ic_moneda,  " +
                                                "  biedo.IC_USUARIO, "+
                                                "  to_char(biedo.DF_FIN_MES,'dd/mm/yyyy') DF_FIN_MES, "+
                                                "  to_char(biedo.DF_VISTO,'dd/mm/yyyy hh24:mi') DF_VISTO, "+
                                                "  decode(biedo.CG_VISTO,'S','Enterado','No Enterado') CG_VISTO, "+
                                                "  '' AS NOM_USR, "+
                                                "  to_char(hisedo.DF_FECHA_HORA,'dd/mm/yyyy') DF_FECHA_HORA "+
                                                " FROM com_estado_cuenta c  "   +
                                                " , comcat_if i   "   +
                                                " , comcat_moneda m "   +
                                                " , bi_notifica_edo_cta biedo"   +
                                                " , comhis_proc_edo_cuenta hisedo "+
                                                " WHERE c.ic_if = i.ic_if  "   +        
                                                " AND m.ic_moneda = c.ic_moneda"   +
                                                " AND c.ic_if = biedo.ic_if" +
                                                " AND hisedo.ic_proc_edo_cuenta = biedo.ic_proc_edo_cuenta "   +
                                                " AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy') "   +
                                                " AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1  "+
                                                " AND biedo.df_fin_mes >= TO_DATE (?, 'dd/mm/yyyy')" + 
                                                " AND biedo.df_fin_mes  < TO_DATE (?, 'dd/mm/yyyy') + 1" +
                                                " AND i.cs_tipo = ?  ");
                        conditions.add(sDateIni);
                        conditions.add(sDateIni);
                        conditions.add(sDateIni);
                        conditions.add(sDateIni);
                        conditions.add(cs_tipo);
                        if(!sClaveMoneda.equals("")){
                                qrySentencia.append(" AND c.ic_moneda = ? ");
                                conditions.add(sClaveMoneda);
                   }else{
                                qrySentencia.append(" AND c.ic_moneda IN (?, ?) ");
                                conditions.add("1");
                                conditions.add("54");
                        }
                            if(!estatus.equals("") && estatus.equals("C")){
                                    qrySentencia.append(" AND biedo.CG_VISTO is not null ");
                            }else if(!estatus.equals("") && estatus.equals("P")){
                                    qrySentencia.append(" AND biedo.CG_VISTO is null ");
                            }
                                
                        qrySentencia.append(" GROUP BY i.cg_razon_social,"   +
                                                "       m.cd_nombre,"   +
                                                "       i.ic_if, " +
                                                "  m.ic_moneda," +
                                                "  biedo.IC_USUARIO, "+
                                                "  biedo.DF_FIN_MES, "+
                                                "  biedo.DF_VISTO, "+
                                                "  biedo.CG_VISTO, "+
                                                "  hisedo.DF_FECHA_HORA "+
                                                " ORDER BY 1    " ) ;
                        }else
                        {
                                if(!"CE".equals(cs_tipo)){
                                        qrySentencia.append(" SELECT /*+use_nl(c i m) index(c in_com_estado_cuenta_01_nuk) ordered*/"   +
                                                                "  i2.cg_razon_social NOMBRECLIENTE ,"   +
                                                                "  m.cd_nombre, "   +  
                                                                "  c.ig_cliente,  "   +  
                                                                "  m.ic_moneda  ,  " +
                                                                "  biedo.IC_USUARIO, "+
                                                                "  to_char(biedo.DF_FIN_MES,'dd/mm/yyyy') DF_FIN_MES, "+
                                                                "  to_char(biedo.DF_VISTO,'dd/mm/yyyy hh24:mi') DF_VISTO, "+
                                                                "  decode(biedo.CG_VISTO,'S','Enterado','No Enterado') CG_VISTO, "+
                                                                "  '' AS NOM_USR, "+
                                                                "  to_char(hisedo.DF_FECHA_HORA,'dd/mm/yyyy') DF_FECHA_HORA "+
                                                                " FROM com_estado_cuenta c  "   +
                                                                " , comcat_if i   "   +
                                                                " , comcat_if i2          "   +
                                                                " , comcat_moneda m "   +
                                                                " , bi_notifica_edo_cta biedo"   +
                                                                " , comhis_proc_edo_cuenta hisedo "+
                                                                " WHERE c.ic_if = i.ic_if " + 
                                                                " AND c.ic_if = biedo.ic_if"   +
                                                                " AND c.ic_if = 12  "   +       
                                                                " AND m.ic_moneda = c.ic_moneda "   +
                                                                " AND c.ig_cliente = i2.in_numero_sirac " + 
                                                                " AND hisedo.ic_proc_edo_cuenta = biedo.ic_proc_edo_cuenta "   +
                                                                " AND c.ig_cliente is not null  " +
                                                                " AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy') "   +
                                                                " AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1  " +
                                                                " AND biedo.df_fin_mes >= TO_DATE (?, 'dd/mm/yyyy')" + 
                                                                " AND biedo.df_fin_mes  < TO_DATE (?, 'dd/mm/yyyy') + 1" +
                                                                " AND i2.cs_tipo = ?  "
                                                                );
                                        conditions.add(sDateIni);
                                        conditions.add(sDateIni);
                                        conditions.add(sDateIni);
                                        conditions.add(sDateIni);
                                        conditions.add(cs_tipo);
                                        if(!sClaveMoneda.equals("")){
                                                qrySentencia.append(" AND c.ic_moneda = ? ");
                                                conditions.add(sClaveMoneda);
                                        }else{
                                                qrySentencia.append(" AND c.ic_moneda IN (?, ?) ");
                                                conditions.add("1");
                                                conditions.add("54");
                                        }

                                    if(!estatus.equals("") && estatus.equals("C")){
                                            qrySentencia.append(" AND biedo.CG_VISTO is not null ");
                                    }else if(!estatus.equals("") && estatus.equals("P")){
                                            qrySentencia.append(" AND biedo.CG_VISTO is null ");
                                    }
                                        
                                        qrySentencia.append(" GROUP BY i2.cg_razon_social,"   +
                                                        "       m.cd_nombre,"   +
                                                        "       c.ig_cliente, " +
                                                        "  m.ic_moneda,  " +
                                                        "  biedo.IC_USUARIO, "+
                                                        "  biedo.DF_FIN_MES, "+
                                                        "  biedo.DF_VISTO, "+
                                                        "  biedo.CG_VISTO, "+
                                                        "  hisedo.DF_FECHA_HORA "+
                                                        " ORDER BY 1    " ) ;
                                }else
                                {
                                        qrySentencia.append(" SELECT /*+use_nl(c i m) index(c in_com_estado_cuenta_01_nuk) ordered*/"   +
                                                                "  cce.CG_RAZON_SOCIAL NOMBRECLIENTE ,"   +
                                                                "  m.cd_nombre, "   +  
                                                                "  c.ig_cliente,  "   +  
                                                                "  m.ic_moneda,  "   +
                                                                "  cce.ic_nafin_electronico  ,  " +
                                                                "  biedo.IC_USUARIO, "+
                                                                "  to_char(biedo.DF_FIN_MES,'dd/mm/yyyy') DF_FIN_MES, "+
                                                                "  to_char(biedo.DF_VISTO,'dd/mm/yyyy hh24:mi') DF_VISTO, "+
                                                                "  decode(biedo.CG_VISTO,'S','Enterado','No Enterado') CG_VISTO, "+
                                                                "  '' AS NOM_USR, "+
                                                                "  to_char(hisedo.DF_FECHA_HORA,'dd/mm/yyyy') DF_FECHA_HORA "+
                                                                " FROM com_estado_cuenta c  "   +
                                                                //" , comcat_if i         "   +
                                                                " , comcat_moneda m "   +       
                                                                " , comcat_cli_externo cce "   +  
                                                                " , bi_notifica_edo_cta biedo"   +
                                                                " , comhis_proc_edo_cuenta hisedo "+
                                                                " WHERE c.ig_cliente = cce.in_numero_sirac " +
                                                                " AND c.ic_if = biedo.ic_if"   +
                                                                " AND c.ic_if = 12  "   +       
                                                                " AND m.ic_moneda = c.ic_moneda "   +
                                                                " AND hisedo.ic_proc_edo_cuenta = biedo.ic_proc_edo_cuenta "   +
                                                                " AND c.ig_cliente is not null  " +
                                                                " AND c.df_fechafinmes >= TO_DATE (?, 'dd/mm/yyyy') "   +
                                                                " AND c.df_fechafinmes < TO_DATE (?, 'dd/mm/yyyy') + 1  "+
                                                                " AND biedo.df_fin_mes >= TO_DATE (?, 'dd/mm/yyyy')" + 
                                                                " AND biedo.df_fin_mes  < TO_DATE (?, 'dd/mm/yyyy') + 1"

                                                                );
                                        conditions.add(sDateIni);
                                        conditions.add(sDateIni);
                                        conditions.add(sDateIni);
                                        conditions.add(sDateIni);
                                        if(!sClaveMoneda.equals("")){
                                                qrySentencia.append(" AND c.ic_moneda = ? ");
                                                conditions.add(sClaveMoneda);
                                        }else{
                                                qrySentencia.append(" AND c.ic_moneda IN (?, ?) ");
                                                conditions.add("1");
                                                conditions.add("54");
                                        }

                                    if(!estatus.equals("") && estatus.equals("C")){
                                            qrySentencia.append(" AND biedo.CG_VISTO is not null ");
                                    }else if(!estatus.equals("") && estatus.equals("P")){
                                            qrySentencia.append(" AND biedo.CG_VISTO is null ");
                                    }                                        
                                        qrySentencia.append(" GROUP BY " +
                                                        "       cce.CG_RAZON_SOCIAL,"   +
                                                        "       m.cd_nombre,"   +
                                                        "       c.ig_cliente, " +
                                                        "       m.ic_moneda,,  " +
                                                        "  biedo.IC_USUARIO, "+
                                                        "  biedo.DF_FIN_MES, "+
                                                        "  biedo.DF_VISTO, "+
                                                        "  biedo.CG_VISTO, "+
                                                        "  hisedo.DF_FECHA_HORA "+
                                                        "  cce.ic_nafin_electronico  "   +
                                                        " ORDER BY 1    " ) ;
                                }
                                
                                
                        }
        
        
                        
                }else {
                        qrySentencia.append(" SELECT /*+use_nl(c i) index (c in_com_estado_cuenta_01_nuk) ordered*/"+
                                "        i.cg_razon_social, cg_subapl_desc tipo_credito,"   +
                                "        SUM (c.fg_montooperado) monto_operado,"   +
                                "        SUM (c.fg_capitalvigente) capital_vigente, "   +
                                "        SUM (c.fg_capitalvencido) capital_vencido,"   +
                                "        SUM (c.fg_interesprovi) interes_vigente, "   +
                                "        SUM (c.fg_interesvencido) interes_vencido,"   +
                                "        SUM (c.FG_INTERESMORAT + c.FG_SOBRETASAMOR) moras, "   +
                                "        SUM (c.fg_adeudototal) total_adeudo, "   +
                                "        COUNT (c.ig_prestamo) descuentos,"   +
                                "        ig_subapl subaplic, "   +
                                "        SUM (c.fg_saldoinsoluto) saldo_insoluto"   +
                                "        , i.ic_financiera,"   +
                                "                       '' as stipo_credito, "+
                                "                       '' as stipo_credito1, "+
                                "                       '' as smonto_operado, "+
                                "                       '' as scapital_vigente, "+
                                "                       '' as scapital_vencido, "+
                                "                       '' as sinteres_vigente, "+
                                "                       '' as sinteres_vencido, "+
                                "                       '' as smoras, "+
                                "                       '' as stotal_adeudo, "+
                                "                       '' as sdescuentos, "+
                                "                       '' as sclave_sub_aplic, "+
                                "                       '' as ssaldo_insoluto,  " +
                                "  biedo.IC_USUARIO, "+
                                "  to_char(biedo.DF_FIN_MES,'dd/mm/yyyy') DF_FIN_MES, "+
                                "  to_char(biedo.DF_VISTO,'dd/mm/yyyy hh24:mi') DF_VISTO, "+
                                "  decode(biedo.CG_VISTO,'S','Consultado','') CG_VISTO, "+
                                "  '' AS NOM_USR, "+
                                "  to_char(hisedo.DF_FECHA_HORA,'dd/mm/yyyy') DF_FECHA_HORA "+
                                "   FROM com_estado_cuenta c, comcat_if i"   +
                                " , bi_notifica_edo_cta biedo"   +
                                " , comhis_proc_edo_cuenta hisedo "+
                                "  WHERE c.ic_if = i.ic_if"   +
                                " AND c.ic_if = biedo.ic_if"   +
                                "    AND c.df_fechafinmes >= TO_DATE (?, 'DD/MM/YYYY')"   +
                                "    AND c.df_fechafinmes < TO_DATE (?, 'DD/MM/YYYY')+1"   +
                                "    AND c.ic_moneda = ?"   +
                                "    AND c.ic_if = ?"   +
        ((!"C".equals(tipoLinea))?"":"    AND c.ig_cliente = ? " )+
        ((!estatus.equals("") && estatus.equals("C"))?" AND biedo.CG_VISTO is not null ":"" )+
        ((!estatus.equals("") && estatus.equals("P"))?" AND biedo.CG_VISTO is null ":"" )+
                                "  GROUP BY i.cg_razon_social,  cg_subapl_desc, ig_subapl, i.ic_financiera, biedo.IC_USUARIO, biedo.DF_FIN_MES, biedo.DF_VISTO, biedo.CG_VISTO, hisedo.DF_FECHA_HORA");

                        conditions.add(sDateIni);
                        conditions.add(sDateIni);
                        conditions.add(sClaveMoneda);
                
                if(!"C".equals(tipoLinea)){
                        conditions.add(sClaveIF);
                }else
                {
                        conditions.add("12");
                        conditions.add(numSirac);
                }
                }
                log.info("qrySentencia  "+qrySentencia);
                log.info("conditions "+conditions);
                log.info("getDocumentQueryFile(S)");
                return qrySentencia.toString(); 
        }
                
        public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
                String nombreArchivo = "";
                HttpSession session = request.getSession();     
                ComunesPDF pdfDoc = new ComunesPDF();
                CreaArchivo creaArchivo = new CreaArchivo();
                StringBuffer contenidoArchivo = new StringBuffer();
                try {
                
                } catch(Throwable e) {
                        throw new AppException("Error al generar el archivo ", e);
                } finally {
                        try {
                                //rs.close();
                        } catch(Exception e) {}
                  
                }
                return nombreArchivo;
                                        
        }
        
        
        public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
                log.debug("crearCustomFile (E)");
                String nombreArchivo ="";
                HttpSession session = request.getSession();
                ComunesPDF pdfDoc = new ComunesPDF();
                CreaArchivo creaArchivo = new CreaArchivo();
                StringBuffer contenidoArchivo = new StringBuffer();
                try {
                } catch(Throwable e) {
                        throw new AppException("Error al generar el archivo ", e);
                } finally {
                        try {
                                //rs.close();
                        } catch(Exception e) {}
                  log.debug("crearCustomFile (S)");
                }
                return nombreArchivo;
        }
        
        
        /**
         *Query para sacar  las Firmas de la cedula de conciliación
         * @return 
         * @param ic_if
         */
        public HashMap getDatosFirma(String ic_if)  {
                log.info ("getDatosFirma(E)  ");
                AccesoDB        con = new AccesoDB();
        boolean           exito = true;
                StringBuffer strSQL = new StringBuffer();
                List lVarBind           = new ArrayList();      
                PreparedStatement ps    = null;
                ResultSet  rs = null;
                HashMap registros = new HashMap();
                try {
                        con.conexionDB();  
                        strSQL = new StringBuffer();
                        strSQL.append(" SELECT "   +
                                                " IC_FIRMA_CEDULA FIRMA_IF " +
                                                "       ,CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO NOMBRE_IF " +
                                                "         ,CG_PUESTO PUESTO_IF " +
                                                "         ,(SELECT IC_FIRMA_CEDULA FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S'  AND IC_CLIENTE_EXTERNO IS NULL) FIRMA_NAFIN " +
                                                "         ,(SELECT CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO RESP_IF  " +
                                                "        FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) RESP_NAFIN " +
                                                "         ,(SELECT CG_PUESTO FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) PUESTO_NAFIN " +
                                                "         ,TO_CHAR(sysdate,'DD/MM/YYYY') FECHA " +
                                                 " FROM COMCAT_FIRMA_CEDULA"   +
                                                 ((!"CE".equals(cs_tipo))?" WHERE IC_IF = ? ": " WHERE IC_CLIENTE_EXTERNO = ? ")   +
                                                 " AND CS_ACTIVO= ? ");                                                                 
                                                                        
                        lVarBind                = new ArrayList();                              
                        lVarBind.add(ic_if);    
                        lVarBind.add("S");      
                        log.debug("SQL.toString() "+strSQL.toString());
                        log.debug("varBind "+lVarBind);
                        ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                        rs = ps.executeQuery();
                        if( rs.next() ){
                                registros.put("FIRMA_IF", rs.getString("FIRMA_IF")==null?"":rs.getString("FIRMA_IF"));
                                registros.put("NOMBRE_IF", rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF"));       
                                registros.put("PUESTO_IF", rs.getString("PUESTO_IF")==null?"":rs.getString("PUESTO_IF"));       
                                registros.put("FIRMA_NAFIN", rs.getString("FIRMA_NAFIN")==null?"":rs.getString("FIRMA_NAFIN")); 
                                registros.put("RESP_NAFIN", rs.getString("RESP_NAFIN")==null?"":rs.getString("RESP_NAFIN"));    
                                registros.put("PUESTO_NAFIN", rs.getString("PUESTO_NAFIN")==null?"":rs.getString("PUESTO_NAFIN"));      
                        }                       
                        rs.close();
                        ps.close();             
                } catch(Throwable e) {
                        exito = false;
                        e.printStackTrace();
                        log.error ("getDatosFirma  " + e);
                        throw new AppException("SIST0001");                     
                } finally {
                        if (con.hayConexionAbierta()) {
                                con.terminaTransaccion(exito);
                                con.cierraConexionDB();
                                log.info("  getDatosFirma (S) ");
                        }                       
                }
                return registros;
        }
        
        
        /**
         * Obtengo las observaciones del Detalle de  de la cedula de conciliación
         * @return 
         * @param sFechaCorte
         * @param ic_moneda
         * @param ic_if
         */
        public String getNotas (String sFechaCorte, String ic_if, String ic_moneda  )  {
                log.info ("getNotas(E)  ");
                AccesoDB        con = new AccesoDB();
        boolean           exito = true;
                StringBuffer strSQL = new StringBuffer();
                List lVarBind           = new ArrayList();      
                PreparedStatement ps    = null;
                ResultSet  rs = null;
                String  notas = "";
                try {
                        con.conexionDB();  
                        strSQL = new StringBuffer();
                        strSQL.append(" select cg_observaciones "+
                                " from com_observaciones_cedula "+
                                "where trunc(df_fecha_corte) = to_date( ?, 'dd/mm/yyyy') "+
                                "and ic_moneda = ? " +
                                "and ic_if = ? ");
                        if("C".equals(tipoLinea) && !"".equals(numSirac)){
                                strSQL.append(" and ig_cliente = ? ");
                        }
                        lVarBind                = new ArrayList();              
                        lVarBind.add(sFechaCorte);      
                        lVarBind.add(ic_moneda);
                        if("C".equals(tipoLinea) && !"".equals(numSirac)){
                                lVarBind.add("12");
                                lVarBind.add(numSirac);
                        }else
                        {
                        lVarBind.add(ic_if);    
                        }
                                
                        log.debug("SQL.toString() "+strSQL.toString());
                        log.debug("varBind "+lVarBind);
                        ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                        rs = ps.executeQuery();
                   if( rs.next() ){
                                notas =  rs.getString("cg_observaciones")==null?"":rs.getString("cg_observaciones");                    
                        }                       
                        rs.close();
                        ps.close();             
                                                                                                
                } catch(Throwable e) {
                        exito = false;
                        e.printStackTrace();
                        log.error ("getNotas  " + e);
                        throw new AppException("SIST0001");                     
                } finally {
                        if (con.hayConexionAbierta()) {
                                con.terminaTransaccion(exito);
                                con.cierraConexionDB();
                                log.info("  getNotas (S) ");
                        }                       
                }
                return notas;
        }
        /**
         * Funcion que regresa el catalogo de Intermediario Financiero
         * dependiendo al tipo de Banco 
         * @return 
         */
           
        public Registros getDataCatalogoIntermediario(){
                log.info("getDataCatalogoIntermediario(E)");
                qrySentencia    = new StringBuffer();
                conditions              = new ArrayList();
                StringBuffer condicion  = new StringBuffer("");
                AccesoDB con = new AccesoDB(); 
                Registros registros  = new Registros();
                try{
                        con.conexionDB();
        if(!"CE".equals(cs_tipo)){
        if(!"C".equals(tipoLinea)){
                        qrySentencia.append(    "SELECT   \n"+
                                                                                "        i.ic_if AS clave,   \n"+
                                                                                "        '(' || i.ic_financiera || ') ' || ' ' || i.cg_razon_social AS descripcion   \n"+
                                                                                "FROM    \n"+
                                                                                "        comcat_if i   \n"+
                                                                                "WHERE    \n"+
                                                                                "        i.cs_habilitado = 'S'    \n"+
                                                                                "        AND i.cs_tipo = ?   \n"+
                                                                                "ORDER BY    \n"+
                                                                                "        i.ic_financiera        ");
                        conditions.add(cs_tipo);   
        }else
        {
           qrySentencia.append( "SELECT   \n"+
                        "        i.ic_if AS clave,   \n"+
                        "        '(' || i.IN_NUMERO_SIRAC || ') ' || ' ' || i.cg_razon_social AS descripcion   \n"+
                        "FROM    \n"+
                        "        comcat_if i   \n"+
                        "WHERE    \n"+
                        "        i.cs_habilitado = 'S'    \n"+
                        "        AND i.cs_tipo = ?   \n"+
                        "        AND i.IN_NUMERO_SIRAC is not null   \n"+
                        "ORDER BY    \n"+
                        "        i.ic_financiera        ");
          conditions.add(cs_tipo);
        }
        }else
        {
        qrySentencia.append(    "SELECT   i.IC_NAFIN_ELECTRONICO AS clave, " +
                              "            '(' " +
                              "         || i.IN_NUMERO_SIRAC " +
                              "         || ') ' " +
                              "         || ' ' " +
                              "         || i.cg_razon_social AS descripcion " +
                              "    FROM comcat_cli_externo i " +
                              "ORDER BY i.ic_nafin_electronico ");
        }
                        registros = con.consultarDB(qrySentencia.toString(),conditions);
                        con.cierraConexionDB();
                } catch (Exception e) {
                        log.error("getDataCatalogoIntermediario  Error: " + e);
                } finally {
                        if (con.hayConexionAbierta()) {
                                con.cierraConexionDB();
                        }
                }       
        
                log.info("qrySentencia Fecha de corte: "+qrySentencia.toString());
                log.info("conditions "+conditions);
                
                log.info("getDataCatalogoIntermediario (S) ");
                return registros;
                                                                                                                        
        }
        
        
        /**
         Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
         @return Lista con los parametros de las condiciones 
         */
        public List getConditions() {  return conditions;  }  
        /**
         * Obtiene el numero de pagina.
         * @return Cadena con el numero de pagina
         */
        public String getPaginaNo() { return paginaNo;  }
        /**
         * Obtiene el offset de la pagina.
         * @return Cadena con el offset de pagina
         */
        public String getPaginaOffset() { return paginaOffset;  }
         
         
        /*****************************************************
                                         SETTERS
        *******************************************************/
        /**
         * Establece el numero de pagina.
         * @param  newPaginaNo Cadena con el numero de pagina
         */
        public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
        
        /**
         * Establece el offset de la pagina.
         * @param newPaginaOffset Cadena con el offset de pagina
         */
        public void setPaginaOffset(String paginaOffset)                        {this.paginaOffset=paginaOffset;}

        public String getCs_tipo() {
                return cs_tipo;
        }

        public void setCs_tipo(String cs_tipo) {
                this.cs_tipo = cs_tipo;
        }

        public String getSClaveIF() {
                return sClaveIF;
        }

        public void setSClaveIF(String sClaveIF) {
                this.sClaveIF = sClaveIF;
        }

        public String getSClaveMoneda() {
                return sClaveMoneda;
        }

        public void setSClaveMoneda(String sClaveMoneda) {
                this.sClaveMoneda = sClaveMoneda;
        }

        public String getSDateIni() {
                return sDateIni;
        }

        public void setSDateIni(String sDateIni) {
                this.sDateIni = sDateIni;
        }

        public String getSDateFin() {
                return sDateFin;
        }

        public void setSDateFin(String sDateFin) {
                this.sDateFin = sDateFin;
        }

        public String getTipoConsulta() {
                return tipoConsulta;
        }

        public void setTipoConsulta(String tipoConsulta) {
                this.tipoConsulta = tipoConsulta;
        }

        public String getFechaCorte() {
                return fechaCorte;
        }

        public void setFechaCorte(String fechaCorte) {
                this.fechaCorte = fechaCorte;
        }

        public String getFechaCarga() {
                return fechaCarga;
        }

        public void setFechaCarga(String fechaCarga) {
                this.fechaCarga = fechaCarga;
        }


        public void setTipoLinea(String tipoLinea)
        {
        this.tipoLinea = tipoLinea;
        }


        public String getTipoLinea()
        {
        return tipoLinea;
        }


        public void setNumSirac(String numSirac)
        {
                this.numSirac = numSirac;
        }


        public String getNumSirac()
        {
                return numSirac;
        }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getEstatus() {
        return estatus;
    }
}
