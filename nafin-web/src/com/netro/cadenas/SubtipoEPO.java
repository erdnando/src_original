/**
 * @class		:	SubtipoEPO
 *		 				Implementa la interfaz IQueryGeneratorRegExtJS, esta clase es pasada al constructor de la clase
 * 	            CQueryHelperRegExtJS como parametro para obtener la informacion a mostrar en la paginaci�n.
 *             
 * @Pantalla	:	Administracion  - Parametrizacion -Catalogos -> Valor de Combo Subtipo EPO.
 * @Perfil		:	ADMIN NAFIN
 * @date			:	20-10-2014.
 * @by			:	rpastrana.
 */
 
package com.netro.cadenas;

import java.sql.ResultSet;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class SubtipoEPO implements IQueryGeneratorRegExtJS {

  /**
   * Declaracion de variables
	* @log			: muestra informacion de los eventos que suceden en el servidor.
	* @conditions	: almacena la lista de variables que seran usadas como Bind(?).
	* @strQuery		: almacena el query que sera ejecutado.
   */
  private final static Log log = ServiceLocator.getInstance().getLog(SubtipoEPO.class);
  private List conditions;
  StringBuffer strQuery = new StringBuffer();

 /**
  * Constructor by Default
  */
  public SubtipoEPO()  {  }

  /**
	 * Este m�todo debe regresar un query con el que se obtienen los
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQuery(){
  return "";
  }


	/**
	 * Este m�todo debe regresar un query con el que se obtienen los
	 * datos a mostrar en la consulta basandose en los identificadores unicos
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentSummaryQueryForIds(List ids){
  return "";
  }


	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getAggregateCalculationQuery(){
  return "";
  }


	/**
	 * Este m�todo debe regresar una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
	public List getConditions(){
  return conditions;
  }


	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQueryFile(){
		strQuery   = new StringBuffer(); 
		log.info("getDocumentQueryFile(E)");
		strQuery.append(
			" SELECT s.ic_subtipo_epo AS clave, s.cg_descripcion AS descripcion," +
			" t.ic_tipo_epo AS clavetipo, t.cg_descripcion tipo "+
			" FROM comcat_subtipo_epo s, comcat_tipo_epo t"+
			" WHERE s.ic_tipo_epo = t.ic_tipo_epo" );
		log.debug("getDocumentQueryFile: "+strQuery.toString());
		log.info("getDocumentQueryFile(S)");
		return strQuery.toString();
  }


	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo){
  return "";
  }


	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo){
  return "";
  }

}