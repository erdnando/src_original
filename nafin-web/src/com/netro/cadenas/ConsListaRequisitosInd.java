package com.netro.cadenas; 

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;
import com.netro.requisitos.IListaRequisitos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsListaRequisitosInd implements IQueryGeneratorRegExtJS {
	private final static Log log = ServiceLocator.getInstance().getLog(ConsListaRequisitosInd.class);
	List conditions;
	private String claveNafin; 
	private String clavePyme;
	private String claveProducto;
	private String claveEpo;
	private String clavePerfil;
	private String fechaInicio;
	private String fechaFin;
	private Vector epoVector;
	private String epo1[];
	private String valEpo1[];
	private String valNuevoEpo1[];
	private String epo5[];
	private String valEpo5[];
	private String valNuevoEpo5[];
	private String epo8[];
	private String valEpo8[];
	private String valNuevoEpo8[];
	
	StringBuffer qrySentencia = new StringBuffer("");
	
	public ConsListaRequisitosInd() { }
	
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
				
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	//query
	public String getDocumentQueryFile() {	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
				
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		log.debug("crearPageCustomFile (E)");	
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		log.debug("crearPageCustomFile (S)");	
		return nombreArchivo;
					
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.debug("crearCustomFile (E)");	
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		log.debug("crearCustomFile (S)");	
		return nombreArchivo;
	}
	
	public String crearCustomFileFromList(List rs, String path, String tipo){
		log.debug("crearCustomFileFromList (E)");
		String nombreArchivo = "";
		String SEP = ",";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contArchivoCVS = new StringBuffer();
		int numRegistros = 0;
		for(int i=0; i < rs.size(); i++){
			HashMap registro = (HashMap)rs.get(i);
			String columnaDinamica = (registro.get("COLDINAMICA")).toString();
			String requisito = registro.get("NOM_REQUISITO").toString();
			String campoFecha = registro.get("CAMPOFECHA").toString();
			String responsable = registro.get("RESPONSABLE").toString();
			String realizo = registro.get("REALIZO").toString();
			String bloqueda = registro.get("BLOQUEADA").toString();
			
			if(columnaDinamica.equals("PALOMA") || columnaDinamica.equals("CHECKED_DISABLED") || columnaDinamica.equals("CHECKED"))
				columnaDinamica = "S";
			if(columnaDinamica.equals("NADA") || columnaDinamica.equals("CHECK"))
				columnaDinamica = "N";
				
			contArchivoCVS.append(columnaDinamica.replaceAll(",","") + SEP +
						requisito.replaceAll(",","") + SEP +
						campoFecha.replaceAll(",","") + SEP +
						bloqueda.replaceAll(",","") + SEP +
						responsable.replaceAll(",","") + SEP +
						realizo.replaceAll(",","") + "\n");
			
			numRegistros++;
		}
		
		if(tipo.equals("CSV")){
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			if(numRegistros >= 1){
				if(!creaArchivo.make(contArchivoCVS.toString(), path, ".csv")){
					nombreArchivo = "ERROR";
				}else{
					nombreArchivo = creaArchivo.nombre;
				}
			}
		}
		
		return nombreArchivo;
	}
	
	public List getNombrePyme(){
		log.info("getNombrePyme (E)");
		AccesoDB con = new AccesoDB();
		List lista = new ArrayList();
		qrySentencia = new StringBuffer();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			con.conexionDB();
			qrySentencia.append("select ic_epo_pyme_if " +
				" from comrel_nafin " +
				" where ic_nafin_electronico = ? " +
				" and cg_tipo in ('P')");
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, Integer.parseInt(getClaveNafin()));
			rs = ps.executeQuery();
			if(rs.next()){				
				String clavePyme = rs.getString("ic_epo_pyme_if");				
				String qryNombreEpo = "SELECT cg_razon_social FROM comcat_pyme " +
							"WHERE ic_pyme = ? ";
				PreparedStatement ps2 = con.queryPrecompilado(qryNombreEpo);
				ps2.setInt(1, Integer.parseInt(clavePyme));
				ResultSet rs2 = ps2.executeQuery();
				if(rs2.next()){
					lista.add(clavePyme);
					lista.add(rs2.getString("cg_razon_social"));
				}
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("No se pudo obtener el nombre de nafin electronico");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getNombrePyme (S)");
		}
		return lista;
	}
	
	public List getEpoRelacionadas(){
		log.info("getEpoRelacionadas (E)");
		AccesoDB con = new AccesoDB();
		List lista = new ArrayList();
		
		qrySentencia = new StringBuffer();
		boolean existe = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			con.conexionDB();
			qrySentencia.append("SELECT a.ic_epo AS clave, a.cg_razon_social AS descripcion " +
						"FROM comcat_epo a, comrel_pyme_epo b " +
						"WHERE a.ic_epo = b.ic_epo " +
						"AND b.ic_pyme = ? ");
			if(!getClaveProducto().equals("4")){
				qrySentencia.append("AND b.cs_habilitado = 'S' AND a.cs_opera_descuento = 'S' ");
			}else{
				qrySentencia.append("AND b.cs_distribuidores in ('S','R') ");
			}
			qrySentencia.append("ORDER BY descripcion");
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, Integer.parseInt(getClavePyme()));
			rs = ps.executeQuery();
			while(rs.next()){
				HashMap reg = new HashMap();
				reg.put("clave",rs.getString("clave"));
				reg.put("descripcion",rs.getString("descripcion"));
				lista.add(reg);
				if(!getClaveEpo().equals("") && !getClaveEpo().equals("0")){
					if(rs.getString("clave").equals(getClaveEpo())){
						existe = true;
					}
				}
			}
			rs.close();
			ps.close(); 
			
			if(!existe && !getClaveEpo().equals("") && !getClaveEpo().equals("0") && !getClaveProducto().equals("4")){
				lista.add(0,"false");
			}
		}catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("No se pudo obtener las EPO relacionadas");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();				
			}
			log.info("qrySentencia  "+qrySentencia);
			log.info("getEpoRelacionadas (S)");
		}
		return lista;
	}
	
	public List getBusquedaAvanzada(String nombre, String rfc, String proveedor){
		log.info("getBusquedaAvanzada (E)");
		AccesoDB con = new AccesoDB();
		List lista = new ArrayList();
		qrySentencia = new StringBuffer();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int contador = 1;
		int cont =1;
		
		try{
			con.conexionDB();
			qrySentencia.append("SELECT DISTINCT p.ic_pyme, crn.ic_nafin_electronico, " +
						"pe.cg_pyme_epo_interno AS cg_pyme_epo_interno, p.cg_razon_social, " +
						"crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega " +
						"FROM comrel_pyme_epo pe, comrel_nafin crn, comcat_pyme p " +
						"WHERE p.ic_pyme = pe.ic_pyme " +
						"AND p.ic_pyme = crn.ic_epo_pyme_if " +
						"AND crn.cg_tipo = 'P' " +
						"AND pe.ic_epo = ? ");
			if(!nombre.equals(""))
				qrySentencia.append("AND cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
			if(!rfc.equals(""))
				qrySentencia.append("AND cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
			if(!proveedor.equals(""))
				qrySentencia.append("AND cg_pyme_epo_interno LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
				
			qrySentencia.append("GROUP BY p.ic_pyme, pe.cg_pyme_epo_interno, p.cg_razon_social, " +
						"RPAD (pe.cg_pyme_epo_interno, 10, ' ') || p.cg_razon_social, crn.ic_nafin_electronico " +
						"ORDER BY p.cg_razon_social ");
			
			ps = con.queryPrecompilado(qrySentencia.toString());
			
			ps.setInt(contador, Integer.parseInt(getClaveEpo()));
			contador++;
			if(!nombre.equals("")){
				ps.setString(contador, nombre);
				contador++;
			}
			if(!rfc.equals("")){
				ps.setString(contador, rfc);
				contador++;
			}		
			if(!proveedor.equals(""))
				ps.setString(contador, proveedor);
				
			rs = ps.executeQuery();
			while(rs.next()){
			cont++;
				HashMap reg = new HashMap();
		//		reg.put("IC_PYME", rs.getString(1));
				reg.put("CLAVE", rs.getString(2));
		//		reg.put("CG_PYME_EPO_INTERNO", rs.getString(3));
		//		reg.put("CG_RAZON_SOCIAL", rs.getString(4));
				reg.put("DESCRIPCION", rs.getString(5));
				lista.add(reg);
				if(cont > 1005)
					break;
			}			
		}catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("No se pudo obtener el nombre de nafin electronico");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();				
			}
			log.info("qrySentencia  "+qrySentencia);
			log.info("getBusquedaAvanzada (S)");
		}
		return lista;
	}
	
	public List getConsultaGeneral(){
		log.info("getConsultaGeneral (E)");
		AccesoDB con = new AccesoDB();
		List lista = new ArrayList();
		qrySentencia = new StringBuffer();
		
		String SQLPymes = "", busca = "", txtCveEPO = "0", txtCvePYME = "0", SQLNnafin = "", epo_pyme = "";
		String tipo_nnafin = "P", txtDesEPO = "", txtDesPYME = "", SQLdescEPO = "", SQLdescPYME = "", SQLReq1 = "", SQLReq2 = "";
		String SQLReq3 = "", SQLReq4 = "", SQLReq4a = "", SQLReq5 = "", SQLReq6 = "", SQLReq7 = "", SQLReq8 = "", SQLReq9 = "", contenido = "", si_no = "";
		String bandera2 = "", tmp_epo = "", tmp_if = "", fecha_tasa = "", bandera7 = "", bandera8 = "", ruta_destino = "", condicion_fecha = "";
		String qryProductos = "",SQLEposXPyme="";
		String ordenar_x_fecha_req_1 = "", ordenar_x_fecha_req_3 = "", ordenar_x_fecha_req_5_8 = "", ordenar_x_fecha_req_6 = "", ordenar_x_fecha_req_7 = "";
		String condicion_fecha_req_1="", condicion_fecha_req_3="", condicion_fecha_req_5_8="", condicion_fecha_req_6="", condicion_fecha_req_7="",  condicion_fecha_req_2_3_7="",  condicion_fecha_4_5_6="";
		String ordenar_x_fecha_req_2_3_7 = "", ordenar_x_fecha_4_5_6 = "" ;
		String varEpo = "";
		String cboProducto = getClaveProducto();
		String CadenasEpos = getClaveEpo();
		String CadenasPymes = getClavePyme();
		txtCvePYME = getClavePyme();
		String cvePerf = getClavePerfil();
		HashMap reg = new HashMap();
		boolean aplica = false,bExiste = true;
		int i = 0, n=0, cuantos = 0;
		ResultSet rsQry = null, rsQry2 = null,rs = null , rsQry3= null;
		Vector Epo_vec = getEpoVector();
		PreparedStatement ps = null,ps2=null,ps3=null;
		
		
		if(CadenasEpos.equals("")){
			CadenasEpos = (String)Epo_vec.get(0);
		}
		if (Integer.parseInt(CadenasEpos) > 0 && Integer.parseInt(CadenasPymes) == 0 && !getClaveNafin().equals("") && Long.parseLong(getClaveNafin()) == 0) {
			busca = "E";
			Epo_vec.addElement(CadenasEpos);
			System.out.println("primer opcion");
		} else if ( (Integer.parseInt(CadenasEpos) > 0 && Integer.parseInt(CadenasPymes) > 0 && !getClaveNafin().equals("") && (Long.parseLong(getClaveNafin()) == 0  || Long.parseLong(getClaveNafin()) > 0)) ||
					(Integer.parseInt(CadenasPymes) > 0 && !getClaveNafin().equals("") && Long.parseLong(getClaveNafin()) == 0) ) {
			busca = "P";
			System.out.println("segunda opcion");
		} else if (Integer.parseInt(CadenasEpos) == 0 && (Integer.parseInt(CadenasPymes) > 0 || Integer.parseInt(CadenasPymes) == 0) && !getClaveNafin().equals("") && Long.parseLong(getClaveNafin()) > 0) {
			busca = "N";
			System.out.println("tercer opcion");
		}
		
		
		if(!"".equals(getFechaInicio()) && !"".equals(getFechaFin()))	{
			if("1".equals(getClaveProducto())) {
				if (busca.equals("N") && tipo_nnafin.equals("P")) {
					condicion_fecha_req_1 = " AND TO_DATE(TO_CHAR(A.DF_CONV_NAFIN_EPO, 'dd/mm/yyyy'), 'dd/mm/yyyy') between"+
									  		" TO_DATE ('"+getFechaInicio()+"', 'dd/mm/yyyy') and TO_DATE ('"+getFechaFin()+"', 'dd/mm/yyyy') ";
					ordenar_x_fecha_req_1 = " A.DF_CONV_NAFIN_EPO DESC,";
				}else{
					condicion_fecha_req_1 = " AND TO_DATE(TO_CHAR(DF_CONV_NAFIN_EPO, 'dd/mm/yyyy'), 'dd/mm/yyyy') between"+
											" TO_DATE ('"+getFechaInicio()+"', 'dd/mm/yyyy') and TO_DATE ('"+getFechaFin()+"', 'dd/mm/yyyy') ";
					ordenar_x_fecha_req_1 = " DF_CONV_NAFIN_EPO DESC,";
				}
					condicion_fecha_req_3 = " AND TO_DATE(TO_CHAR(B.DF_CARTA_BAJO, 'dd/mm/yyyy'), 'dd/mm/yyyy') between"+
									  		" TO_DATE ('"+getFechaInicio()+"', 'dd/mm/yyyy') and TO_DATE ('"+getFechaFin()+"', 'dd/mm/yyyy') ";
					ordenar_x_fecha_req_3 = " B.DF_CARTA_BAJO DESC,";
					condicion_fecha_req_5_8 = " AND TO_DATE(TO_CHAR(B.DF_ACEPTACION, 'dd/mm/yyyy'), 'dd/mm/yyyy') between"+
											" TO_DATE ('"+getFechaInicio()+"', 'dd/mm/yyyy') and TO_DATE ('"+getFechaFin()+"', 'dd/mm/yyyy') ";
					ordenar_x_fecha_req_5_8 = " B.DF_ACEPTACION DESC,";
					condicion_fecha_req_6 = " AND TO_DATE(TO_CHAR(B.DF_CARTA_ALTO, 'dd/mm/yyyy'), 'dd/mm/yyyy') between"+
											" TO_DATE ('"+getFechaInicio()+"', 'dd/mm/yyyy') and TO_DATE ('"+getFechaFin()+"', 'dd/mm/yyyy') ";
					ordenar_x_fecha_req_6 = " B.DF_CARTA_ALTO DESC,";
					condicion_fecha_req_7 = " AND TO_DATE(TO_CHAR(E.DF_VOBO_IF, 'dd/mm/yyyy'), 'dd/mm/yyyy') between"+
											" TO_DATE ('"+getFechaInicio()+"', 'dd/mm/yyyy') and TO_DATE ('"+getFechaFin()+"', 'dd/mm/yyyy') ";
					ordenar_x_fecha_req_7 = " E.DF_VOBO_IF DESC,";
			}

		}
		try{
			con.conexionDB();
			for(n=0; n < Epo_vec.size(); n++){
				CadenasEpos = (String)Epo_vec.get(n);
				String Epos = CadenasEpos;
				if (busca.equals("E")) {
					txtCveEPO = CadenasEpos;
					txtCvePYME = "0";
				} else if (busca.equals("P")) {
					txtCveEPO = CadenasEpos;
					txtCvePYME = CadenasPymes;
				} else if (busca.equals("N")) {
					if(!"null".equals(Epos) && !"0".equals(Epos)) {busca = "P";}
				}
				
				if(bExiste){
					txtDesEPO = "";
					txtDesPYME = "";
					if (!txtCveEPO.equals("0")) {
						SQLdescEPO= "select cg_razon_social from comcat_epo "+
									"where ic_epo = " + txtCveEPO+
									" and cs_habilitado = 'S'";
						//ps.clearParameters();
						ps = con.queryPrecompilado(SQLdescEPO);
						rsQry = ps.executeQuery();
						if (rsQry.next()) {
							txtDesEPO = (rsQry.getString(1) == null) ? "0" : rsQry.getString(1);
						}
						rsQry.close();
						ps.close();	
						//ps.clearParameters();
					}

					if(getClaveProducto().equals("1")){
						if (busca.equals("N") && tipo_nnafin.equals("P")) {
							SQLReq1="select A.ic_epo, A.cg_razon_social, A.cs_conv_nafin_epo, TO_CHAR(A.df_conv_nafin_epo,'DD/MM/YYYY') " +
									"from comcat_epo A, comrel_pyme_epo B " +
									"where A.ic_epo = B.ic_epo "+
									"and B.ic_pyme = " + txtCvePYME + condicion_fecha_req_1 +
									" order by "+ordenar_x_fecha_req_1+" A.cg_razon_social "; 

							SQLReq2="select A.ic_epo, A.cg_razon_social, B.ic_if, B.cg_razon_social " +
									"from comcat_epo A, comcat_if B, comrel_if_epo C, comrel_pyme_epo D " +
									"where A.ic_epo = C.ic_epo "+
									"and B.ic_if = C.ic_if "+
									"and C.ic_epo = D.ic_epo "+
									"and D.ic_pyme = " + txtCvePYME +
									" order by A.cg_razon_social, B.cg_razon_social";

						}else {
							SQLReq1="select ic_epo, cg_razon_social, cs_conv_nafin_epo, TO_CHAR(df_conv_nafin_epo,'DD/MM/YYYY') "+
									"from comcat_epo "+
									"where ic_epo = " + txtCveEPO + condicion_fecha_req_1 ;
							if(!"".equals(ordenar_x_fecha_req_1))
								SQLReq1 += " order by "+ordenar_x_fecha_req_1.substring(0,ordenar_x_fecha_req_1.length()-1);
							//out.print("<br> SQLReq1 "+SQLReq1);
							SQLReq2="select A.ic_epo, A.cg_razon_social, B.ic_if, B.cg_razon_social " +
									"from comcat_epo A, comcat_if B, comrel_if_epo C " +
									"where A.ic_epo = C.ic_epo "+
									"and B.ic_if = C.ic_if "+
									"and C.ic_epo = " + txtCveEPO +
									" order by B.cg_razon_social";
							//out.print("<br> SQLReq2 "+SQLReq2);
						}
						if (busca.equals("P") || (busca.equals("N") && tipo_nnafin.equals("P")))
							aplica = true;
						System.out.print("EPO---------- "+txtCveEPO);	
						if (aplica == true){
							/*SQLReq4="select ic_usuario, cs_visitas, TO_CHAR(df_visitas,'DD/MM/YYYY') from seg _usuario "+
									"where ic_pyme = " + txtCvePYME +
									" and cs_tipo_afiliacion = 'B'";*/
							if (!txtCveEPO.equals("0")) {
								/*SQLReq3 = "select A.ic_epo, A.cg_razon_social, B.cs_carta_bajo " +
									"from comcat_epo A, comrel_pyme_epo B " +
									"where A.ic_epo = B.ic_epo "+
									"and B.ic_epo = " + txtCveEPO +
									" and B.ic_pyme = " + txtCvePYME+ condicion_fecha_req_3 ;
								if(!"".equals(ordenar_x_fecha_req_3))
									SQLReq3 += " order by "+ordenar_x_fecha_req_3.substring(0,ordenar_x_fecha_req_3.length()-1);*/
	
								SQLReq5="select A.ic_epo, A.cg_razon_social, B.cs_aceptacion, TO_CHAR(B.df_aceptacion,'DD/MM/YYYY') " +
										"from comcat_epo A, comrel_pyme_epo B " +
										"where A.ic_epo = B.ic_epo "+
										"and B.ic_epo = " + txtCveEPO +
										" and B.cs_aceptacion in ('R','S','H') "+
											" and B.ic_pyme = " + txtCvePYME + condicion_fecha_req_5_8 ;
								if(!"".equals(ordenar_x_fecha_req_5_8))
									SQLReq5 += " order by "+ordenar_x_fecha_req_5_8.substring(0,ordenar_x_fecha_req_5_8.length()-1);
								System.out.println("----------SQLReq5----------"+SQLReq5);
								/*
								SQLReq6="select A.ic_epo, A.cg_razon_social, B.cs_carta_alto, TO_CHAR(B.DF_CARTA_ALTO, 'dd/mm/yyyy') " +
										"from comcat_epo A, comrel_pyme_epo B " +
										"where A.ic_epo = B.ic_epo "+
											"and B.ic_epo = " + txtCveEPO +
									" and B.ic_pyme = " + txtCvePYME + condicion_fecha_req_6;
								if(!"".equals(ordenar_x_fecha_req_6))
									SQLReq6 += " order by "+ordenar_x_fecha_req_6.substring(0,ordenar_x_fecha_req_6.length()-1);
								*/
								SQLReq7="select distinct(A.ic_epo), A.cg_razon_social, B.ic_if, B.cg_razon_social," +
										"C.ic_cuenta_bancaria, D.cd_nombre, E.cs_vobo_if, e.df_vobo_if, TO_CHAR(E.DF_VOBO_IF, 'dd/mm/yyyy'),cs_estatus, TO_CHAR(E.DF_ENVIO, 'dd/mm/yyyy'), TO_CHAR(E.DF_AUTORIZACION, 'dd/mm/yyyy') " +
										",  (SELECT cs_bloqueo FROM COMREL_BLOQUEO_PYME_X_IF_EPO  WHERE ic_pyme = "+txtCvePYME+"  AND ic_epo = "+txtCveEPO+"  AND ic_if = B.ic_if) as BLOQUEADA "+ //Fodea 025-2015
										"from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C, comcat_moneda D, comrel_pyme_if E " +
										"where A.ic_epo = E.ic_epo "+
										"and B.ic_if = E.ic_if "+
										"and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria " +
										"and C.ic_moneda = D.ic_moneda "+
										"and E.cs_borrado = 'N' "+
										"and E.ic_epo = " + txtCveEPO +
										" and C.ic_pyme = " + txtCvePYME + condicion_fecha_req_7 +
										" order by "+ordenar_x_fecha_req_7+" A.cg_razon_social, B.cg_razon_social";
								System.out.println("----------SQLReq7----------"+SQLReq7);
								SQLReq8="select A.ic_epo, A.cg_razon_social, B.cs_aceptacion, C.in_numero_sirac " +
										"from comcat_epo A, comrel_pyme_epo B, comcat_pyme C " +
										"where A.ic_epo = B.ic_epo "+
										"and B.ic_pyme = C.ic_pyme "+
										"and B.cs_aceptacion in ('H','R') "+
										"and B.ic_epo = " + txtCveEPO +
										" and B.ic_pyme = " + txtCvePYME + condicion_fecha_req_5_8;
								if(!"".equals(ordenar_x_fecha_req_5_8))
									SQLReq8 += " order by "+ordenar_x_fecha_req_5_8.substring(0,ordenar_x_fecha_req_5_8.length()-1);
								System.out.println("----------SQLReq8----------"+SQLReq8);
							} else {
								SQLReq3="select A.ic_epo, A.cg_razon_social, B.cs_carta_bajo " +
										"from comcat_epo A, comrel_pyme_epo B " +
										"where A.ic_epo = B.ic_epo "+
										"and B.ic_pyme = " + txtCvePYME + condicion_fecha_req_3 +
										" order by "+ordenar_x_fecha_req_3+" A.cg_razon_social";
								SQLReq5="select A.ic_epo, A.cg_razon_social, B.cs_aceptacion, TO_CHAR(B.df_aceptacion,'DD/MM/YYYY') " +
										"from comcat_epo A, comrel_pyme_epo B " +
										"where A.ic_epo = B.ic_epo "+
										"and B.cs_aceptacion in ('R','S','H') "+
										"and B.ic_pyme = " + txtCvePYME + condicion_fecha_req_5_8 +
										" order  by "+ordenar_x_fecha_req_5_8+" A.cg_razon_social";
								/*SQLReq6="select A.ic_epo, A.cg_razon_social, B.cs_carta_alto, TO_CHAR(B.DF_CARTA_ALTO, 'dd/mm/yyyy') " +
										"from comcat_epo A, comrel_pyme_epo B " +
										"where A.ic_epo = B.ic_epo "+
										"and B.cs_aceptacion <> 'N' "+
										"and B.ic_pyme = " + txtCvePYME + condicion_fecha_req_6 +
										" order by "+ordenar_x_fecha_req_6+" A.cg_razon_social";*/
								SQLReq7="select distinct(A.ic_epo), A.cg_razon_social, B.ic_if, B.cg_razon_social," +
										"C.ic_cuenta_bancaria, D.cd_nombre, E.cs_vobo_if, e.df_vobo_if, TO_CHAR(E.DF_VOBO_IF, 'dd/mm/yyyy'),cs_estatus, TO_CHAR(E.DF_ENVIO, 'dd/mm/yyyy'), TO_CHAR(E.DF_AUTORIZACION, 'dd/mm/yyyy') " +
										",  (SELECT cs_bloqueo FROM COMREL_BLOQUEO_PYME_X_IF_EPO  WHERE ic_pyme =  C.ic_pyme AND ic_epo = E.ic_epo  AND ic_if = B.ic_if) as BLOQUEADA "+ //Fodea 025-2015
										"from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C, comcat_moneda D, comrel_pyme_if E, comrel_pyme_epo F " +
										"where A.ic_epo = E.ic_epo "+
										"and B.ic_if = E.ic_if "+
										"and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria " +
									    "and C.ic_moneda = D.ic_moneda "+
										"and F.ic_pyme = C.ic_pyme "+
										"and F.ic_epo = E.ic_epo "+
										"and E.cs_borrado = 'N' "+
										"and F.ic_pyme = " + txtCvePYME +condicion_fecha_req_7 +
										" order by "+ordenar_x_fecha_req_7+" A.cg_razon_social, B.cg_razon_social";
								SQLReq8="select A.ic_epo, A.cg_razon_social, B.cs_aceptacion, C.in_numero_sirac " +
										"from comcat_epo A, comrel_pyme_epo B, comcat_pyme C " +
										"where A.ic_epo = B.ic_epo "+
										"and B.ic_pyme = C.ic_pyme "+
										"and B.cs_aceptacion in ('H','R') "+
										"and B.ic_pyme = " + txtCvePYME + condicion_fecha_req_5_8 +
										" order by "+ordenar_x_fecha_req_5_8+" A.cg_razon_social";
							}
						} // si aplica = true
						
					/*	if (busca.equals("N") && tipo_nnafin.equals("P")){
							SOLO UNA COLUMNA EN EL TITULO (PYME)
						}else 
							COLUMNA DE EPO Y PYME PRESENTES
						}*/
						reg = new HashMap();
						reg.put("EPO_REL", txtDesEPO);
						reg.put("COLDINAMICA", "");
						reg.put("NOM_REQUISITO", "1. Convenio creación cadena (EPO-Nafin)");
						reg.put("CAMPOFECHA", "Fecha de Registro");
						reg.put("RESPONSABLE","");
						reg.put("REALIZO","");
						reg.put("EPO","");
						reg.put("VAL_ACTUAL","");
						reg.put("VAL_NUEVO","");
						reg.put("TIPO","");
						reg.put("BLOQUEADA","Bloqueada");
						lista.add(reg);
						
						System.out.print(" \n-----SQLReq1---------- "+SQLReq1);      
					
						//ps.clearParameters();
						ps = con.queryPrecompilado(SQLReq1);
						rsQry = ps.executeQuery();						
						i = 0;
						while (rsQry.next()){
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							if (aplica == true) {
								if((rsQry.getString(3)).equals("S")){
									reg.put("COLDINAMICA", "PALOMA");
								}else{
									reg.put("COLDINAMICA", "NADA");
								}
							}else{
								if((rsQry.getString(3)).equals("S")){
									reg.put("COLDINAMICA", "CHECKED");
								}
							}
							reg.put("NOM_REQUISITO", "1." + String.valueOf(i+1) + " " + rsQry.getString(2));
							si_no = ((rsQry.getString(3)).equals("S"))?"S":"N";
							//contenido += si_no + ",1." + String.valueOf(i+1) + " " + ((rsQry.getString(2) == null) ? "" : rsQry.getString(2)).replace(',',' ') + ","+((rsQry.getString(4) == null) ? "" : rsQry.getString(4))+"\n";
							reg.put("CAMPOFECHA", rsQry.getString(4));
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO", rsQry.getString(1));
							reg.put("VAL_ACTUAL", rsQry.getString(3));
							reg.put("VAL_NUEVO", rsQry.getString(3));
							reg.put("TIPO","1");
							reg.put("BLOQUEADA","");
							lista.add(reg);
							i++;
						}
						rsQry.close();
						ps.close();
						//ps.clearParameters();
												
						reg = new HashMap();
						reg.put("EPO_REL", txtDesEPO);
						reg.put("COLDINAMICA", "");
						reg.put("NOM_REQUISITO", "2. Convenio de tasas (EPO-IF)");
						reg.put("CAMPOFECHA", "");
						reg.put("RESPONSABLE","");
						reg.put("REALIZO","");
						reg.put("EPO","");
						reg.put("VAL_ACTUAL","");
						reg.put("VAL_NUEVO","");
						reg.put("TIPO","");
						reg.put("BLOQUEADA","");
						lista.add(reg);
						
						//ps.clearParameters();
					   ps = con.queryPrecompilado(SQLReq2); 
						rsQry = ps.executeQuery();					
						i = 0;
						System.out.print("SQLReq2---------- "+SQLReq2);
						
						
						while (rsQry.next()){
							reg = new HashMap();
							bandera2 = "N";
							tmp_epo = (rsQry.getString(1) == null) ? "" : rsQry.getString(1);
							tmp_if = (rsQry.getString(3) == null) ? "" : rsQry.getString(3);
							
							
							
						String SQLReq2_1=" SELECT TO_CHAR (MAX(dc_fecha_tasa), 'DD/MM/YYYY hh24:mi') AS dc_fecha_tasa,"   +
									" '15parametrizacion/15nafin/15admnafparamchklist.jsp'    "   +
									" FROM comrel_tasa_base "   +
									" WHERE ic_epo = ? "   +
									" AND cs_vobo_nafin = 'S'"  ;
									
							//ps2.clearParameters();
							ps2 = con.queryPrecompilado(SQLReq2_1);
							ps2.setInt(1,Integer.parseInt(tmp_epo));
							rsQry2 = ps2.executeQuery();		
						
							if (rsQry2.next()){
								fecha_tasa = (rsQry2.getString(1) == null) ? "" : rsQry2.getString(1);
								
								//ps3.clearParameters();	
								String SQLReq2_2=" SELECT COUNT( * ) AS cuantos, '15parametrizacion/15nafin/15admnafparamchklist.jsp' "   +
										" FROM comrel_tasa_autorizada "   +
										" WHERE ic_if = ? "   +
										" AND ic_epo = ? "   +
										" AND TO_CHAR(dc_fecha_tasa, 'DD/MM/YYYY hh24:mi') = ? "   +
										" AND cs_vobo_epo = 'S' "   +
										" AND cs_vobo_if = 'S' "   +
										" AND cs_vobo_nafin = 'S'"  ;
								ps3 = con.queryPrecompilado(SQLReq2_2);
								ps3.setInt(1,Integer.parseInt(tmp_if));
								ps3.setInt(2,Integer.parseInt(tmp_epo));
								ps3.setString(3,fecha_tasa);
								rsQry3 = ps3.executeQuery();
								if (rsQry3.next())  {
									cuantos = Integer.parseInt(rsQry.getString(1));
									if (cuantos > 0) bandera2 = "S";
								}
								rsQry3.close();
								ps3.close();
							}							
							rsQry2.close();
							ps2.close();
							
							reg.put("EPO_REL", txtDesEPO);
							if(bandera2.equals("S")) {
								reg.put("COLDINAMICA", "PALOMA");
							}else{
								reg.put("COLDINAMICA", "NADA");
							}
							reg.put("NOM_REQUISITO", "2." + String.valueOf(i+1) + " " + ((rsQry.getString(2) == null) ? "" : rsQry.getString(2)) + " - " + ((rsQry.getString(4) == null) ? "" : rsQry.getString(4)));
							si_no = (bandera2.equals("S"))?"S":"N";
							//contenido += si_no + ",2." + String.valueOf(i+1) + " " + ((rsQry.getString(2) == null) ? "" : rsQry.getString(2)).replace(',',' ') + " - " + ((rsQry.getString(4) == null) ? "" : rsQry.getString(4)).replace(',',' ') + "\n";%>
							reg.put("CAMPOFECHA", "");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
							i++;
						}
						rsQry.close();
						ps.close();
						//ps.clearParameters();
						
						if (aplica == true){
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("COLDINAMICA", "");
							reg.put("NOM_REQUISITO", "3. Carta de invitación (Consultas) (Obsoleto)");
							reg.put("CAMPOFECHA", "");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
							/*
							 * CODIGO COMENTADO
							 */
							
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("COLDINAMICA", "");
							reg.put("NOM_REQUISITO", "4. Aceptación de servicio (Obsoleto)");
							reg.put("CAMPOFECHA", "Fecha de Registro");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
							/*
							 * CODIGO COMENTADO
							 */
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("COLDINAMICA", "");
							reg.put("NOM_REQUISITO", "5. Afiliación a descuento electrónico");
							reg.put("CAMPOFECHA", "Fecha de Registro");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
							
							//ps.clearParameters();
							ps = con.queryPrecompilado(SQLReq5);
							rsQry = ps.executeQuery();	
							i = 0;
							while (rsQry.next()){
								reg = new HashMap();
								reg.put("EPO_REL", txtDesEPO);
								reg.put("COLDINAMICA", "CHECK");
								if("8".equals(cvePerf)){
									if((rsQry.getString(3)).equals("R") || (rsQry.getString(3)).equals("H"))
										
										reg.put("COLDINAMICA", "CHECKED_DISABLED");
								}else{
									if((rsQry.getString(3)).equals("R") || (rsQry.getString(3)).equals("H"))
										reg.put("COLDINAMICA", "CHECKED");
								}
								
								reg.put("NOM_REQUISITO", "5." + String.valueOf(i+1) + " " + ((rsQry.getString(2) == null) ? "" : rsQry.getString(2)));
								si_no = ((rsQry.getString(3)).equals("R") || (rsQry.getString(3)).equals("H"))?"S":"N";
								contenido += si_no + ",5." + String.valueOf(i+1) + " " + ((rsQry.getString(2) == null) ? "" : rsQry.getString(2)).replace(',',' ') +","+((rsQry.getString(4) == null) ? "" : rsQry.getString(4))+ "\n";
								reg.put("CAMPOFECHA", rsQry.getString(4));
								reg.put("RESPONSABLE","");
								reg.put("REALIZO","");
								reg.put("EPO", rsQry.getString(1));
								reg.put("VAL_ACTUAL", ((rsQry.getString(3).equals("H"))?"R":rsQry.getString(3)));
								reg.put("VAL_NUEVO", ((rsQry.getString(3).equals("H"))?"R":rsQry.getString(3)));
								reg.put("TIPO","5");
								reg.put("BLOQUEADA","");
								lista.add(reg);
								i++;
							}
							rsQry.close();
							ps.close();
							//ps.clearParameters();
							
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("COLDINAMICA", "");
							reg.put("NOM_REQUISITO", "6. Carta de ingreso a descuento (Obsoleto)");
							reg.put("CAMPOFECHA", "Fecha de Registro");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
							/*
							 * CODIGO COMENTADO
							 */
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("COLDINAMICA", "");
							reg.put("NOM_REQUISITO", "7. Confirmación PYME");
							reg.put("CAMPOFECHA", "Fecha de Registro");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","Bloqueda");
							lista.add(reg);
							
							
							//ps.clearParameters();
							ps = con.queryPrecompilado(SQLReq7);
							rsQry = ps.executeQuery();								
							i = 0;
							bandera7 = "N";
							
							System.out.println(" \n SQLReq7  ====="+SQLReq7);    
							
							while (rsQry.next()){
								reg = new HashMap();
								reg.put("EPO_REL", txtDesEPO);
								//reg.put("COLDINAMICA", "CHECK");
								if("8".equals(cvePerf)){
									if(rsQry.getString(7).equals("S")){
										//reg.put("COLDINAMICA", "CHECKED_DISABLED");
										reg.put("COLDINAMICA", "PALOMA");
									}else{
										reg.put("COLDINAMICA", "NADA");
									}
								}else{
									if(rsQry.getString(7).equals("S")){
										//reg.put("COLDINAMICA", "CHECKED");
										reg.put("COLDINAMICA", "PALOMA");
								}else{
										reg.put("COLDINAMICA", "NADA");
									}
								}
								String notaEstatus = "";
								if(rsQry.getString(10) != null){
									if((rsQry.getString(10)).equals("E")){
										notaEstatus = "<b> ENVIADA ELECTRÓNICAMENTE</b>";
									} else if((rsQry.getString(10)).equals("A")){
										notaEstatus = "<b> AUTORIZADA ELECTRÓNICAMENTE</b>";
									}
								}
								reg.put("NOM_REQUISITO", "7." + String.valueOf(i+1) + " " + ((rsQry.getString(2) == null) ? "" : rsQry.getString(2)) + " - " + ((rsQry.getString(4) == null) ? "" : rsQry.getString(4)) + " - " + ((rsQry.getString(6) == null) ? "" : rsQry.getString(6)) + notaEstatus);
								if ((rsQry.getString(7)).equals("S")) {
									si_no = "S";
									bandera7 = "S";
								} else {
									si_no = "N";
								}

								if (rsQry.getString(10) != null) {
									if ((rsQry.getString(10)).equals("E")) {
									//	out.print("<b>ENVIADA ELECTRÓNICAMENTE</b>");
									}else if ((rsQry.getString(10)).equals("A")) {
									//	out.print("<b>AUTORIZADA ELECTRÓNICAMENTE</b>");
									}
								}
								if(rsQry.getString(10) != null && (rsQry.getString(10)).equals("E")){
									reg.put("CAMPOFECHA", rsQry.getString(11));
								}else{
									reg.put("CAMPOFECHA", rsQry.getString(9));
								}
								String bloqueo =(rsQry.getString("BLOQUEADA") == null) ? "N" : rsQry.getString("BLOQUEADA");
								String bloqueda ="No";
								if(bloqueo.equals("S")) { bloqueda ="Si"; }
								
								reg.put("RESPONSABLE","");
								reg.put("REALIZO","");
								reg.put("EPO","");
									reg.put("VAL_ACTUAL", rsQry.getString(7));
								reg.put("VAL_NUEVO", rsQry.getString(7));
								reg.put("TIPO","7");
								reg.put("BLOQUEADA",bloqueda); 											
								lista.add(reg);
								i++;
							}
							rsQry.close();
							ps.close();
							//ps.clearParameters();
							
							String SQLReq8_bloqueo =
									" SELECT cg_causa_bloqueo FROM comrel_bloqueo_pyme_x_producto " +
									" WHERE ic_producto_nafin = 1 AND ic_pyme = " + txtCvePYME +
									" AND cs_bloqueo = 'B' ";
							//ps.clearParameters();
							ps = con.queryPrecompilado(SQLReq8_bloqueo);
							ResultSet	rsbloq = ps.executeQuery();
							boolean bloqueado = false;
							String causaBloqueo = "";
							if(rsbloq.next()){
								bloqueado = true;
								causaBloqueo = rsbloq.getString("cg_causa_bloqueo");
							}
							rsbloq.close();
							ps.close();
							//ps.clearParameters();
							
							
							String estilo = (bloqueado)?"bloqueado":"formas";
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("COLDINAMICA", "");
							reg.put("NOM_REQUISITO", "8. Habilitación PYME" +
								((bloqueado)?"(Bloqueado por " + causaBloqueo + ")":""));
							reg.put("CAMPOFECHA", "");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
							
							//ps.clearParameters();
							ps = con.queryPrecompilado(SQLReq8);
							rsQry = ps.executeQuery();	
							i = 0;
							while (rsQry.next()){
								reg = new HashMap();
								reg.put("EPO_REL", txtDesEPO);
								bandera8 = "S";
								//reg.put("COLDINAMICA", "CHECK");
								if("8".equals(cvePerf)){
									if(rsQry.getString(3).equals("H")){
										//reg.put("COLDINAMICA", "CHECKED_DISABLED");
										reg.put("COLDINAMICA", "PALOMA");
									}else{
										reg.put("COLDINAMICA", "NADA");
									}
								}else{
									if(rsQry.getString(3).equals("H")){
										//reg.put("COLDINAMICA", "CHECKED");
										reg.put("COLDINAMICA", "PALOMA");
								}else{
										reg.put("COLDINAMICA", "NADA");
									}
								}
								reg.put("NOM_REQUISITO", "8." + String.valueOf(i+1) + " " + ((rsQry.getString(2) == null) ? "" : rsQry.getString(2)));
								//si_no = ((rsQry.getString(3)).equals("H"))?"S":"N";
								//contenido += si_no + ",8." + String.valueOf(i+1) + " " + ((rsQry.getString(2) == null) ? "" : rsQry.getString(2)).replace(',',' ') + "\n";
								reg.put("CAMPOFECHA", "");
								reg.put("RESPONSABLE","");
								reg.put("REALIZO","");
								reg.put("EPO", rsQry.getString(1));
								reg.put("VAL_ACTUAL", rsQry.getString(3));
								reg.put("VAL_NUEVO", rsQry.getString(3));					
								reg.put("TIPO","8");
								reg.put("BLOQUEADA","");
								lista.add(reg);
								i++;
							}
							rsQry.close();
							ps.close();
							//ps.clearParameters();
							
						}//fin if(aplica == true)
						//El Requisito 4 no depende de la Epo y solo se modificar&aacute; en la primer Epo seleccionada
					}//fin claveProducto == 1
					
					
					else if(getClaveProducto().equals("4")){
						String acreditado		= "";
						String tipoCredito		= "";
						String qryAcreditado	=	" SELECT CG_TIPO_CREDITO FROM COMREL_PYME_EPO_X_PRODUCTO"+
									" WHERE IC_EPO = "+Epos+
									" AND IC_PYME = "+txtCvePYME+
									" AND IC_PRODUCTO_NAFIN = 4";
						
						//ps.clearParameters();
						ps = con.queryPrecompilado(qryAcreditado);
						rsQry = ps.executeQuery();	

						if(rsQry.next()){
							acreditado = ("D".equals(rsQry.getString(1)))?"":"NO ";
							tipoCredito = rsQry.getString(1);
						}
						rsQry.close();
						ps.close();
						//ps.clearParameters();
						
						if(!"".equals(tipoCredito)&&tipoCredito!=null){
							//EPO = acreditado + "Acreditada";
						}else{
							//Distribuidor
						}
						//aqui van la epo y la pyme, se pueden jalar del combo y textfield respectivos
						// REQUISITO Se puede meter como column header
						//FECHA Se puede meter como column header
						//RESPONSABLE Se puede meter como column header
						//REALIZO Se puede meter como column header
						if(busca.equals("N") && tipo_nnafin.equals("P")) {
							SQLReq1 = "Select e.cg_razon_social,pe.cs_habilitado,pe.df_habilitado,TO_CHAR(PE.DF_HABILITADO, 'dd/mm/yyyy') from comrel_producto_epo pe, comcat_epo e"+
									" where e.ic_epo = pe.ic_epo and pe.ic_epo in (select ic_epo from comrel_pyme_epo where ic_pyme = "+txtCvePYME+") and pe.ic_producto_nafin = "+cboProducto+ condicion_fecha_req_1+
									" order by "+ordenar_x_fecha_req_1+" e.cg_razon_social";
						}else {
							SQLReq1 = "Select e.cg_razon_social,pe.cs_habilitado,pe.df_habilitado,TO_CHAR(PE.DF_HABILITADO, 'dd/mm/yyyy') from comrel_producto_epo pe, comcat_epo e"+
									" where e.ic_epo = pe.ic_epo and pe.ic_epo = "+CadenasEpos+" and pe.ic_producto_nafin = "+cboProducto+ condicion_fecha_req_1 +
									" order by "+ordenar_x_fecha_req_1+" e.cg_razon_social";
						}
						aplica = false;
						if (busca.equals("P") || (busca.equals("N") && tipo_nnafin.equals("P"))) 
							aplica = true;
						if(aplica){
							if(busca.equals("N") && tipo_nnafin.equals("P")){
								SQLReq2 = "select e.ic_epo,e.cg_razon_social,pep.cs_habilitado,TO_CHAR(pep.DF_ACEPTACION_PYME, 'dd/mm/yyyy'),p.CS_TIPO_AFILIA_DISTRI,rpe.CS_REALIZO_AFIL_BAS " +
									" from comrel_pyme_epo_x_producto pep, comcat_pyme p, comcat_epo e, comrel_pyme_epo rpe " +
									" where e.ic_epo = pep.ic_epo and rpe.ic_epo = pep.ic_epo and rpe.ic_pyme = pep.ic_pyme and pep.ic_pyme=p.ic_pyme and pep.ic_pyme = "+txtCvePYME+" and pep.ic_epo in (select ic_epo from comrel_pyme_epo where ic_pyme = "+txtCvePYME+") and  p.ic_pyme ="+txtCvePYME+" and pep.ic_producto_nafin = "+cboProducto+ condicion_fecha_req_2_3_7 +
									" order by "+ordenar_x_fecha_req_2_3_7+" e.cg_razon_social";
								//DM y CCC
								SQLReq3 = " Select e.cg_razon_social,i.cg_razon_social,lc.cg_tipo_solicitud,TO_CHAR(LC.DF_VENCIMIENTO, 'dd/mm/yyyy') as fecha_vencimiento, mon.cd_nombre as moneda"   +
											" from comcat_epo e,dis_linea_credito_dm lc,comcat_if i, comcat_moneda mon "   +
											" where lc.ic_epo = e.ic_epo and lc.ic_if = i.ic_if and trunc(lc.df_vencimiento) >= trunc(SYSDATE)"   +
											" and lc.cg_tipo_solicitud in ('I','A','R') and lc.ic_moneda = mon.ic_moneda and lc.ic_epo in "   +
											" (select ic_epo from comrel_pyme_epo_x_producto where ic_pyme = "+txtCvePYME+" and ic_producto_nafin = "+cboProducto+" and cg_tipo_credito = 'D') and lc.ic_producto_nafin = "+cboProducto+ condicion_fecha_4_5_6 +
											" union all"   +
											" Select py.cg_razon_social,i.cg_razon_social,lc.cg_tipo_solicitud,TO_CHAR(LC.DF_VENCIMIENTO, 'dd/mm/yyyy') as fecha_vencimiento, mon.cd_nombre as moneda"   +
											" from comcat_pyme py,com_linea_credito lc,comcat_if i, comcat_moneda mon "   +
											" where lc.ic_pyme = py.ic_pyme and lc.ic_if = i.ic_if "   +
											" and trunc(lc.df_vencimiento) >= trunc(SYSDATE)"   +
											" and lc.cg_tipo_solicitud in ('I','A','R') and lc.ic_moneda = mon.ic_moneda "   +
											" and lc.ic_pyme in "   +
											" (select ic_pyme from comrel_pyme_epo_x_producto where ic_pyme = "+txtCvePYME+" and ic_producto_nafin = "+cboProducto+" and cg_tipo_credito = 'C') and lc.ic_producto_nafin = "+cboProducto+ condicion_fecha_4_5_6 ;
								SQLReq4 = " select e.ic_epo,e.cg_razon_social,'P',to_char(max(tp.df_captura),'dd/mm/yyyy') fecha_captura "+
											" from com_tasa_if_pyme tp,comcat_epo e "+
											" where tp.ic_pyme= "+ txtCvePYME +" and tp.ic_producto_nafin= "+cboProducto+"  and e.ic_epo in ( select ic_epo from comrel_pyme_epo_x_producto where ic_pyme = "+txtCvePYME+" and ic_producto_nafin="+cboProducto+
											" and cg_tipo_credito='C') "+
											" group by e.ic_epo,e.cg_razon_social,'P' "+
											" union all "+
											" select e.ic_epo,e.cg_razon_social,'G',to_char(max(tg.df_captura),'dd/mm/yyyy') fecha_captura "+
											" from com_tasa_general tg, comcat_epo e "+
											" where not exists (select 1 from com_tasa_if_pyme ip,comcat_epo e2 where e.ic_epo=e2.ic_epo and ip.ic_pyme= "+ txtCvePYME +" )   "+
											" and ic_producto_nafin= "+cboProducto+"   and e.ic_epo in ( select ic_epo from comrel_pyme_epo_x_producto where ic_pyme = "+txtCvePYME+
											" and ic_producto_nafin="+cboProducto+" and cg_tipo_credito='C')  "+
											" group by e.ic_epo,e.cg_razon_social,'G' "+
											" union all "+
											" select e.ic_epo,e.cg_razon_social,'N','s/f' fecha_captura "+
											" from comcat_epo e "+
											" where not exists (select 1 from com_tasa_if_pyme ip,comcat_epo e2 where e.ic_epo=e2.ic_epo and ip.ic_pyme= "+ txtCvePYME +" ) "+
											" and not exists (select 1 from com_tasa_general tg,comcat_epo e3 where  e.ic_epo=e3.ic_epo and tg.ic_producto_nafin= "+cboProducto+" ) "+
											" and e.ic_epo in ( select ic_epo from comrel_pyme_epo_x_producto where ic_pyme = "+txtCvePYME+" and ic_producto_nafin="+cboProducto+" and cg_tipo_credito='C')  "+
											" group by e.ic_epo,e.cg_razon_social,'N' "+
											" union all "+
											" select e.ic_epo,e.cg_razon_social,'P',to_char(max(te.df_captura),'dd/mm/yyyy') fecha_captura"   +
											" from com_tasa_if_epo te,comcat_epo e"   +
											" where te.ic_epo=e.ic_epo and te.ic_producto_nafin="+cboProducto+"  and  te.ic_epo in ( select ic_epo from comrel_pyme_epo_x_producto where ic_pyme = "+txtCvePYME+" and ic_producto_nafin="+cboProducto+" and cg_tipo_credito='D')"   +
											" group by e.ic_epo,e.cg_razon_social,'P'"   +
											" union all"   +
											" select e.ic_epo,e.cg_razon_social,'N','s/f' fecha_captura"   +
											" from comcat_epo e"   +
											" where not exists (select 1 from com_tasa_if_epo ie,comcat_epo e2 where ie.ic_epo=e2.ic_epo and e.ic_epo=e2.ic_epo)"   +
											" and e.ic_epo in ( select ic_epo from comrel_pyme_epo_x_producto where ic_pyme = "+txtCvePYME+" and ic_producto_nafin="+cboProducto+" and cg_tipo_credito='D')"   +
											" group by e.ic_epo,e.cg_razon_social,'N'"  ;
		
								//ESL agrego requisito 5 recorro los demás
								SQLReq5 =" SELECT epo.cg_razon_social AS nomepo,"   +
											" cif.cg_razon_social AS nomif,"   +
											" TO_CHAR(cap.df_vobo_if, 'dd/mm/yyyy') AS fechaauto, "   +
											" cap.cs_vobo_if "+
											" FROM comrel_cuenta_bancaria_x_prod cbp, com_cuenta_aut_x_prod cap, "   +
											" comcat_epo epo, comcat_if cif "   +
											" WHERE cbp.ic_cuenta_bancaria = cap.ic_cuenta_bancaria "   +
											" AND cbp.ic_epo = epo.ic_epo "   +
											" AND cbp.ic_if = cif.ic_if "   +
											" AND cbp.ic_pyme = "+txtCvePYME+" "   ;
		/*
									"select e.ic_epo,e.cg_razon_social,pep.cs_aceptacion_pyme,TO_CHAR(pep.DF_ACEPTACION_PYME, 'dd/mm/yyyy') "+
									" from comrel_pyme_epo_x_producto pep, comcat_epo e "+
									" where pep.ic_epo = e.ic_epo "+
									" and pep.ic_pyme = "+txtCvePYME+" and pep.ic_epo in (select ic_epo from comrel_pyme_epo where ic_pyme = "+txtCvePYME+") and pep.ic_producto_nafin = "+cboProducto+ condicion_fecha_req_2_3_7 +
									" order by "+ordenar_x_fecha_req_2_3_7+" e.cg_razon_social";
		*/
								SQLReq6 = "select e.ic_epo,e.cg_razon_social,pep.cs_aceptacion_pyme,TO_CHAR(pep.DF_ACEPTACION_PYME, 'dd/mm/yyyy') "+
											" from comrel_pyme_epo_x_producto pep, comcat_epo e "+
											" where pep.ic_epo = e.ic_epo "+
											" and pep.ic_pyme = "+txtCvePYME+" and pep.ic_epo in (select ic_epo from comrel_pyme_epo where ic_pyme = "+txtCvePYME+") and pep.ic_producto_nafin = "+cboProducto+ condicion_fecha_req_2_3_7 +
											" order by "+ordenar_x_fecha_req_2_3_7+" e.cg_razon_social";
								SQLReq7 = "select e.ic_epo,e.cg_razon_social,pep.cs_habilitado,TO_CHAR(pep.DF_ACEPTACION_PYME, 'dd/mm/yyyy'),p.CS_TIPO_AFILIA_DISTRI, P.IN_NUMERO_SIRAC, rpe.CS_REALIZO_AFIL_COMP" +										
											" from comrel_pyme_epo_x_producto pep, comcat_pyme p, comcat_epo e,comrel_pyme_epo rpe " +
											" where e.ic_epo = pep.ic_epo and rpe.ic_epo = pep.ic_epo and rpe.ic_pyme = pep.ic_pyme and pep.ic_pyme=p.ic_pyme and pep.ic_pyme = "+txtCvePYME+" and pep.ic_epo in (select ic_epo from comrel_pyme_epo where ic_pyme = "+txtCvePYME+") and  p.ic_pyme ="+txtCvePYME+" and pep.ic_producto_nafin = "+cboProducto + condicion_fecha_req_2_3_7 +
											" order by "+ordenar_x_fecha_req_2_3_7+" e.cg_razon_social";
								SQLReq8 = "select e.ic_epo,e.cg_razon_social,pep.cs_carta_alto,TO_CHAR(pep.DF_carta_alto, 'dd/mm/yyyy') "+
											" from comrel_pyme_epo_x_producto pep, comcat_epo e "+
											" where e.ic_epo = pep.ic_epo  and pep.ic_pyme = "+txtCvePYME+" and pep.ic_epo in (select ic_epo from comrel_pyme_epo where ic_pyme = "+txtCvePYME+")  and pep.ic_producto_nafin = "+cboProducto+
											" order by e.cg_razon_social";
								SQLReq9 = "select e.ic_epo,e.cg_razon_social,pep.cs_habilitado,TO_CHAR(pep.DF_habilitado, 'dd/mm/yyyy') as fechahab"+
											" from comrel_pyme_epo_x_producto pep, comcat_epo e "+
											" where e.ic_epo = pep.ic_epo  and pep.ic_pyme = "+txtCvePYME+" and pep.ic_epo in (select ic_epo from comrel_pyme_epo where ic_pyme = "+txtCvePYME+")  and pep.ic_producto_nafin = "+cboProducto+
											" order by e.cg_razon_social";
							}else{
								SQLReq2 = "select e.ic_epo,e.cg_razon_social,pep.cs_habilitado,TO_CHAR(pep.DF_ACEPTACION_PYME, 'dd/mm/yyyy'),p.CS_TIPO_AFILIA_DISTRI,rpe.CS_REALIZO_AFIL_BAS " +
											" from comrel_pyme_epo_x_producto pep, comcat_pyme p, comcat_epo e, comrel_pyme_epo rpe " +
											" where e.ic_epo = pep.ic_epo and rpe.ic_epo = pep.ic_epo and rpe.ic_pyme = pep.ic_pyme and pep.ic_pyme=p.ic_pyme and pep.ic_pyme = "+txtCvePYME+" and pep.ic_epo = "+txtCveEPO+" and p.ic_pyme= "+txtCvePYME+" and pep.ic_producto_nafin = "+cboProducto+ condicion_fecha_req_2_3_7 +
											" order by "+ordenar_x_fecha_req_2_3_7+" e.cg_razon_social";
										/*Dm y CCC*/
								if("D".equals(tipoCredito)){
									SQLReq3 = "Select e.cg_razon_social,i.cg_razon_social,lc.cg_tipo_solicitud,TO_CHAR(LC.DF_VENCIMIENTO, 'dd/mm/yyyy') as fecha_vencimiento, mon.cd_nombre as moneda"   +
												" from comcat_epo e,dis_linea_credito_dm lc,comcat_if i,comrel_pyme_epo_x_producto pe, comcat_moneda mon "   +
												" where lc.ic_epo = e.ic_epo and lc.ic_if = i.ic_if and trunc(lc.df_vencimiento) >= trunc(SYSDATE)"   +
												" and lc.cg_tipo_solicitud in ('I','A','R') and lc.ic_epo = "+CadenasEpos+" and lc.ic_producto_nafin = "+cboProducto + condicion_fecha_4_5_6+
												" and lc.ic_moneda = mon.ic_moneda "   +
												" and pe.ic_epo = e.ic_epo"   +
												" and pe.ic_pyme = "+txtCvePYME+
												" and pe.ic_producto_nafin =  "+cboProducto +
												" and pe.cg_tipo_credito = 'D'";
									SQLReq4 = "select e.ic_epo,e.cg_razon_social,'P',to_char(max(te.df_captura),'dd/mm/yyyy') fecha_captura"   +
												" from com_tasa_if_epo te,comcat_epo e"   +
												" where te.ic_epo=e.ic_epo and te.ic_producto_nafin="+cboProducto+" and e.ic_epo ="+txtCveEPO+" and te.ic_epo = "+txtCveEPO+
												" group by e.ic_epo,e.cg_razon_social,'P'"   +
												" union all"   +
												" select e.ic_epo,e.cg_razon_social,'N','s/f' fecha_captura"   +
												" from comcat_epo e"   +
												" where not exists (select 1 from com_tasa_if_epo ie,comcat_epo e2 where ie.ic_epo=e2.ic_epo and e.ic_epo=e2.ic_epo)"   +
												" and e.ic_epo = "+txtCveEPO +
												" group by e.ic_epo,e.cg_razon_social,'N'"  ;
								}else if("C".equals(tipoCredito)){
									SQLReq3 = "Select e.cg_razon_social,i.cg_razon_social,lc.cg_tipo_solicitud,TO_CHAR(LC.DF_VENCIMIENTO, 'dd/mm/yyyy') as fecha_vencimiento, mon.cd_nombre as moneda"   +
												" from comcat_epo e,com_linea_credito lc,comcat_if i,comrel_pyme_epo_x_producto pe, comcat_moneda mon "   +
												" where lc.ic_pyme = pe.ic_pyme and lc.ic_if = i.ic_if and trunc(lc.df_vencimiento) >= trunc(SYSDATE)"   +
												" and lc.cg_tipo_solicitud in ('I','A','R') and pe.ic_epo = "+CadenasEpos+" and lc.ic_producto_nafin = "+cboProducto + condicion_fecha_4_5_6+
												" and lc.ic_moneda = mon.ic_moneda "   +
												" and pe.ic_epo = e.ic_epo"   +
												" and pe.ic_pyme = "+txtCvePYME+
												" and pe.ic_producto_nafin = "+cboProducto +
												" and pe.cg_tipo_credito = 'C'"  ;
									SQLReq4 = "select e.ic_epo,e.cg_razon_social,'P',to_char(max(tp.df_captura),'dd/mm/yyyy') fecha_captura "+
												" from com_tasa_if_pyme tp,comcat_epo e "+
												" where tp.ic_pyme= "+ txtCvePYME +" and tp.ic_producto_nafin= "+cboProducto+"  and e.ic_epo = "+txtCveEPO+" "+
												" group by e.ic_epo,e.cg_razon_social,'P' "+
												" union all "+
												" select e.ic_epo,e.cg_razon_social,'G',to_char(max(tg.df_captura),'dd/mm/yyyy') fecha_captura "+
												" from com_tasa_general tg, comcat_epo e "+
												" where not exists (select 1 from com_tasa_if_pyme ip,comcat_epo e2 where e.ic_epo=e2.ic_epo and ip.ic_pyme= "+ txtCvePYME +" )   "+
												" and ic_producto_nafin= "+cboProducto+"   and e.ic_epo = "+txtCveEPO+"   "+
												" group by e.ic_epo,e.cg_razon_social,'G' "+
												" union all "+
												" select e.ic_epo,e.cg_razon_social,'N','s/f' fecha_captura "+
												" from comcat_epo e "+
												" where not exists (select 1 from com_tasa_if_pyme ip,comcat_epo e2 where e.ic_epo=e2.ic_epo and ip.ic_pyme= "+ txtCvePYME +" ) "+
												" and not exists (select 1 from com_tasa_general tg,comcat_epo e3 where  e.ic_epo=e3.ic_epo and tg.ic_producto_nafin= "+cboProducto+" ) "+
												" and e.ic_epo = "+txtCveEPO+" "+
												" group by e.ic_epo,e.cg_razon_social,'N' "+
												" order by 2,1 ";
								}
								if(!"".equals(ordenar_x_fecha_req_2_3_7))	
									SQLReq3 += "order by "+ordenar_x_fecha_req_2_3_7.substring(0,ordenar_x_fecha_req_2_3_7.length()-1);
										//ESL se recorren ver que trae ordenar_x_fecha_req_2_3_7
										SQLReq5 =
									" SELECT epo.cg_razon_social AS nomepo,"   +
									"     cif.cg_razon_social AS nomif,"   +
									"     TO_CHAR(cap.df_vobo_if, 'dd/mm/yyyy') AS fechaauto, "   +
									"	  cap.cs_vobo_if "+
									" FROM comrel_cuenta_bancaria_x_prod cbp, com_cuenta_aut_x_prod cap, "   +
									"     comcat_epo epo, comcat_if cif "   +
									" WHERE cbp.ic_cuenta_bancaria = cap.ic_cuenta_bancaria "   +
									" AND cbp.ic_epo = epo.ic_epo "   +
									" AND cbp.ic_if = cif.ic_if "   +
									" AND cbp.ic_pyme = "+txtCvePYME+" "   +
									" AND cbp.ic_epo = "+txtCveEPO+" "  ;
		/*						"select e.ic_epo,e.cg_razon_social,pep.cs_aceptacion_pyme,TO_CHAR(pep.DF_ACEPTACION_PYME, 'dd/mm/yyyy') "+
									" from comrel_pyme_epo_x_producto pep, comcat_epo e "+
									" where pep.ic_epo = e.ic_epo "+
									" and pep.ic_pyme = "+txtCvePYME+" and pep.ic_epo = "+txtCveEPO+" and pep.ic_producto_nafin = "+cboProducto+ condicion_fecha_req_2_3_7;*/
								if(!"".equals(ordenar_x_fecha_req_2_3_7))	
									SQLReq5 += "order by "+ordenar_x_fecha_req_2_3_7.substring(0,ordenar_x_fecha_req_2_3_7.length()-1);
		
								// Aqui queda como 6
								/*SQLReq6 = "select e.ic_epo, e.cg_razon_social, pep.cs_aceptacion_pyme, TO_CHAR(pep.DF_ACEPTACION_PYME, 'dd/mm/yyyy') "+
										" from comrel_pyme_epo_x_producto pep, comcat_epo e "+
										" where pep.ic_epo = e.ic_epo "+
										" and pep.ic_pyme = "+txtCvePYME+
										" and pep.ic_epo = "+txtCveEPO+
										" and pep.ic_producto_nafin = "+cboProducto+ condicion_fecha_req_2_3_7;
								if(!"".equals(ordenar_x_fecha_req_2_3_7))
									SQLReq6 += "order by "+ordenar_x_fecha_req_2_3_7.substring(0,ordenar_x_fecha_req_2_3_7.length()-1);*/
		
								SQLReq7 = "select e.ic_epo,e.cg_razon_social,pep.cs_habilitado,TO_CHAR(pep.DF_ACEPTACION_PYME, 'dd/mm/yyyy'),p.CS_TIPO_AFILIA_DISTRI , nvl(P.IN_NUMERO_SIRAC,'0') IN_NUMERO_SIRAC, rpe.CS_REALIZO_AFIL_COMP" +
									" from comrel_pyme_epo_x_producto pep, comcat_pyme p, comcat_epo e,comrel_pyme_epo rpe " +
									" where e.ic_epo = pep.ic_epo and rpe.ic_epo = pep.ic_epo and rpe.ic_pyme = pep.ic_pyme and pep.ic_pyme=p.ic_pyme and pep.ic_pyme = "+txtCvePYME+" and pep.ic_epo = "+txtCveEPO+" and p.ic_pyme= "+txtCvePYME+" and pep.ic_producto_nafin = "+cboProducto+ condicion_fecha_req_2_3_7 +
									" order by "+ordenar_x_fecha_req_2_3_7+" e.cg_razon_social";
								/*
								SQLReq8 = "select e.ic_epo,e.cg_razon_social,pep.cs_carta_alto,TO_CHAR(pep.DF_carta_alto, 'dd/mm/yyyy') "+
											" from comrel_pyme_epo_x_producto pep, comcat_epo e "+
											" where e.ic_epo = pep.ic_epo  and pep.ic_pyme = "+txtCvePYME+" and pep.ic_epo = "+txtCveEPO+"  and pep.ic_producto_nafin = "+cboProducto+
											" order by e.cg_razon_social";
								*/
								SQLReq9 = "select e.ic_epo,e.cg_razon_social,pep.cs_habilitado,TO_CHAR(pep.DF_habilitado, 'dd/mm/yyyy') as fechahab"+
											" from comrel_pyme_epo_x_producto pep, comcat_epo e "+
											" where e.ic_epo = pep.ic_epo  and pep.ic_pyme = "+txtCvePYME+" and pep.ic_epo = "+txtCveEPO+"  and pep.ic_producto_nafin = "+cboProducto+
											" order by e.cg_razon_social";
							}// fin if
						}//fin aplica
						reg = new HashMap();
						// REQUISITO Se puede meter como column header
						//FECHA Se puede meter como column header
						//RESPONSABLE Se puede meter como column header
						//REALIZO Se puede meter como column header
						reg.put("EPO_REL", txtDesEPO);
						reg.put("COLDINAMICA", "");
						reg.put("NOM_REQUISITO", "1. Convenio de Adhesión (EPO-NAFIN)");
						reg.put("CAMPOFECHA", "Fecha de Registro");
						reg.put("RESPONSABLE","");
						reg.put("REALIZO","");
						reg.put("EPO","");
						reg.put("VAL_ACTUAL","");
						reg.put("VAL_NUEVO","");
						reg.put("TIPO","");
						reg.put("BLOQUEADA","");
						lista.add(reg);
						
						//ps.clearParameters();
						ps = con.queryPrecompilado(SQLReq1);
						rsQry = ps.executeQuery();	
						
						i = 0;
						while(rsQry.next()){
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							if((rsQry.getString(2)).equals("S")){
								reg.put("COLDINAMICA", "PALOMA");	
							}else{
								reg.put("COLDINAMICA", "NADA");
							}
							if ((rsQry.getString(2)==null?"":rsQry.getString(2)).equals("S")) {
								si_no = "S";
							} else {
								si_no = "N";
							}	
							reg.put("NOM_REQUISITO", "1." + String.valueOf(i+1) + " " + rsQry.getString(1));						
							reg.put("CAMPOFECHA", rsQry.getString(4));
							reg.put("RESPONSABLE","NAFIN");
							reg.put("REALIZO","S".equals(si_no)?"NAFIN":"");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
							i++;
						}//fin while
						rsQry.close();
						ps.close();
						//ps.clearParameters();
						
						
						if(i == 0){
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("NOM_REQUISITO", "");
							reg.put("CAMPOFECHA", "");
							reg.put("RESPONSABLE","NAFIN");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
						}
						if(aplica){
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("COLDINAMICA", "");
							reg.put("NOM_REQUISITO", "2. Afiliacion distribuidor básica");
							reg.put("CAMPOFECHA", "Fecha de Registro");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
							
							//ps.clearParameters();
							ps = con.queryPrecompilado(SQLReq2);
							rsQry = ps.executeQuery();								
							i = 0;
							while (rsQry.next()){
								reg = new HashMap();
								reg.put("EPO_REL", txtDesEPO);
								if((rsQry.getString(5)==null?"":rsQry.getString(5)).equals("B")||(rsQry.getString(5)==null?"":rsQry.getString(5)).equals("C")){
									reg.put("COLDINAMICA", "PALOMA");
								}else{
									reg.put("COLDINAMICA", "NADA");
								}
								if ((rsQry.getString(5)==null?"":rsQry.getString(5)).equals("B")||(rsQry.getString(5)==null?"":rsQry.getString(5)).equals("C")) {
									si_no = "S";
								} else {
									si_no = "N";
								}
								String responsableBAS = rsQry.getString("CS_REALIZO_AFIL_BAS")==null?"":rsQry.getString("CS_REALIZO_AFIL_BAS");
								if("N".equals(responsableBAS))
									responsableBAS = "NAFIN";
								else if("I".equals(responsableBAS))
									responsableBAS = "IF";
								else
									responsableBAS = "EPO";							
							
								reg.put("NOM_REQUISITO", "2." + String.valueOf(i+1) + " " + rsQry.getString(2));
								reg.put("CAMPOFECHA", rsQry.getString(4));
								reg.put("RESPONSABLE","EPO, IF y NAFIN");							
								reg.put("REALIZO","S".equals(si_no)?responsableBAS:"");
								reg.put("EPO","");
								reg.put("VAL_ACTUAL","");
								reg.put("VAL_NUEVO","");
								reg.put("TIPO","");
								reg.put("BLOQUEADA","");
								lista.add(reg);
								i++;
							}
							rsQry.close();
							ps.close();
							//ps.clearParameters();
						
							
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("COLDINAMICA", "");
							reg.put("NOM_REQUISITO", "3. Alta de la Línea por el IF");
							reg.put("CAMPOFECHA", "Fecha de Vencimiento");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
														
							//ps.clearParameters();
							ps = con.queryPrecompilado(SQLReq3);
							rsQry = ps.executeQuery();		

							i = 0;
							while(rsQry.next()){
								reg = new HashMap();
								reg.put("EPO_REL", txtDesEPO);
								reg.put("COLDINAMICA", "PALOMA");
								reg.put("NOM_REQUISITO", "3." + String.valueOf(i+1)+" "+rsQry.getString(2)+" "+"("+rsQry.getString(3)+")"+" LÍNEA EN "+rsQry.getString("moneda"));
								si_no = "S";
								//contenido += si_no + ",3." + String.valueOf(i+1) +" "+rsQry.getString(1)+"-"+rsQry.getString(2)+" ("+rsQry.getString(3)+"),"+((rsQry.getString("fecha_vencimiento") == null) ? "" : rsQry.getString("fecha_vencimiento"));
								reg.put("CAMPOFECHA", rsQry.getString("fecha_vencimiento"));
								reg.put("RESPONSABLE","IF");
								reg.put("REALIZO","S".equals(si_no)?"IF":"");
								reg.put("EPO","");
								reg.put("VAL_ACTUAL","");
								reg.put("VAL_NUEVO","");
								reg.put("TIPO","");
								reg.put("BLOQUEADA","");
								lista.add(reg);
								i++;
							}//fin while
						   rsQry.close();
							ps.close();
							//ps.clearParameters();
							
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("COLDINAMICA", "");
							reg.put("NOM_REQUISITO", "4. Asignar tasas por el IF");
							reg.put("CAMPOFECHA", "Fecha de Registro");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
							
							
							//ps.clearParameters();
							ps = con.queryPrecompilado(SQLReq4);
							rsQry = ps.executeQuery();	

							i = 0;
							while (rsQry.next()){
								reg = new HashMap();
								reg.put("EPO_REL", txtDesEPO);
								if((rsQry.getString(3)==null?"":rsQry.getString(3)).equals("P")||(rsQry.getString(3)==null?"":rsQry.getString(3)).equals("G")){
									reg.put("COLDINAMICA", "PALOMA");
									si_no = "S";
								}else{
									reg.put("COLDINAMICA", "NADA");
									si_no = "N";
								}
								reg.put("NOM_REQUISITO", "4." + String.valueOf(i+1) + " " + rsQry.getString(2));								
								reg.put("CAMPOFECHA", ((rsQry.getString(3)==null?"":rsQry.getString(3)).equals("P")||(rsQry.getString(3)==null?"":rsQry.getString(3)).equals("G"))?rsQry.getString("fecha_captura"):"");
								reg.put("RESPONSABLE","IF");
								reg.put("REALIZO","S".equals(si_no)?"IF":"");		
								reg.put("EPO","");
								reg.put("VAL_ACTUAL","");
								reg.put("VAL_NUEVO","");
								reg.put("TIPO","");
								reg.put("BLOQUEADA","");
								lista.add(reg);
								i++;
							}
							rsQry.close();
							ps.close();
							//ps.clearParameters();
							
							if(!"C".equals(tipoCredito)){
								reg = new HashMap();
								reg.put("EPO_REL", txtDesEPO);
								reg.put("COLDINAMICA", "");
								reg.put("NOM_REQUISITO", "5. Captura Cuentas Bancarias y Autorización Distribuidor");
								reg.put("CAMPOFECHA", "Fecha de Registro");
								reg.put("RESPONSABLE","");
								reg.put("REALIZO","");
								reg.put("EPO","");
								reg.put("VAL_ACTUAL","");
								reg.put("VAL_NUEVO","");
								reg.put("TIPO","");
								reg.put("BLOQUEADA","");
								lista.add(reg);
								
								
								//ps.clearParameters();
								ps = con.queryPrecompilado(SQLReq5);
								rsQry = ps.executeQuery();				

								i = 0;
								while (rsQry.next()){
									reg = new HashMap();
									reg.put("EPO_REL", txtDesEPO);
									if((rsQry.getString(4)==null?"":rsQry.getString(4)).equals("S")){
										reg.put("COLDINAMICA", "PALOMA");
										si_no = "S";
									}
									else{
										reg.put("COLDINAMICA", "NADA");
										si_no = "N";
									}
									reg.put("NOM_REQUISITO", "5." + String.valueOf(i+1) + " " + rsQry.getString(1));
									reg.put("CAMPOFECHA", rsQry.getString(3));
									reg.put("RESPONSABLE","IF");
									reg.put("REALIZO","S".equals(si_no)?"IF":"");
									reg.put("EPO","");
									reg.put("VAL_ACTUAL","");
									reg.put("VAL_NUEVO","");
									reg.put("TIPO","");
									reg.put("BLOQUEADA","");
									lista.add(reg);
									i++;
								}
								rsQry.close();
								ps.close();
								//ps.clearParameters();
								
								reg = new HashMap();
								reg.put("EPO_REL", txtDesEPO);
								reg.put("COLDINAMICA", "");
								reg.put("NOM_REQUISITO", "6. Cliente ingresa a Cadenas, acepta carta (Obsoleto)");
								reg.put("CAMPOFECHA", "Fecha de Registro");
								reg.put("RESPONSABLE","");
								reg.put("REALIZO","");
								reg.put("EPO","");
								reg.put("VAL_ACTUAL","");
								reg.put("VAL_NUEVO","");
								reg.put("TIPO","");
								reg.put("BLOQUEADA","");
								lista.add(reg);
								/*
								* CODIGO COMENTADO
								*/
							}
							
							String option = "7";
							if("C".equals(tipoCredito)){
								String _opcion = "5";
							}
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("COLDINAMICA", "");
							reg.put("NOM_REQUISITO", option + ". Afiliación distribuidor complementaria");
							reg.put("CAMPOFECHA", "Fecha de Registro");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");
							reg.put("BLOQUEADA","");
							lista.add(reg);
							
							
							//ps.clearParameters();
							ps = con.queryPrecompilado(SQLReq7);
							rsQry = ps.executeQuery();		
								
							i = 0;
							while(rsQry.next()){
								reg = new HashMap();
								reg.put("EPO_REL", txtDesEPO);
								if((rsQry.getString(5)==null?"":rsQry.getString(5)).equals("C")&&!(rsQry.getString(6)==null?"":rsQry.getString(6)).equals("0")){
									reg.put("COLDINAMICA", "PALOMA");
									si_no = "S";
								}else{
									reg.put("COLDINAMICA", "NADA");
									si_no = "N";
								}
								String responsableCOMP = rsQry.getString("CS_REALIZO_AFIL_COMP")==null?"":rsQry.getString("CS_REALIZO_AFIL_COMP");
								if("N".equals(responsableCOMP))
									responsableCOMP = "NAFIN";
								else if("I".equals(responsableCOMP))
									responsableCOMP = "IF";
									
								reg.put("NOM_REQUISITO", "7." + String.valueOf(i+1) + " " + rsQry.getString(2));
								reg.put("CAMPOFECHA", (rsQry.getString(4)==null)?"":rsQry.getString(4));
								reg.put("RESPONSABLE","IF y NAFIN");
								reg.put("REALIZO","S".equals(si_no)?responsableCOMP:"");
								reg.put("EPO","");
								reg.put("VAL_ACTUAL","");
								reg.put("VAL_NUEVO","");
								reg.put("TIPO","");		
								reg.put("BLOQUEADA","");
								lista.add(reg);
								i++;
							}
							rsQry.close();
							ps.close();
							//ps.clearParameters();
							
							if(!"C".equals(tipoCredito)){
								reg = new HashMap();
								reg.put("EPO_REL", txtDesEPO);
								reg.put("COLDINAMICA", "");
								reg.put("NOM_REQUISITO", "8. Nafin genera claves de alto riesgo (Obsoleto)");
								reg.put("CAMPOFECHA", "Fecha de Registro");
								reg.put("RESPONSABLE","");
								reg.put("REALIZO","");
								reg.put("EPO","");
								reg.put("VAL_ACTUAL","");
								reg.put("VAL_NUEVO","");
								reg.put("TIPO","");
								reg.put("BLOQUEADA","");
								lista.add(reg);
								/*
								* CODIGO COMENTADO
								*/
							}
							
							option = "9";
							if("C".equals(tipoCredito)){
								option = "6";
							}
							String SQLReq9_bloqueo =
										" SELECT cg_causa_bloqueo FROM comrel_bloqueo_pyme_x_producto " +
										" WHERE ic_producto_nafin = 4 AND ic_pyme = " + txtCvePYME +
										" AND cs_bloqueo = 'B' ";
							
							//ps.clearParameters();
							ps = con.queryPrecompilado(SQLReq9_bloqueo);
							ResultSet rsbloq = ps.executeQuery();	
							
												
							boolean bloqueado = false;
							String causaBloqueo = "";
							if(rsbloq.next()) {
								bloqueado = true;
								causaBloqueo = rsbloq.getString("cg_causa_bloqueo");
							}
							rsbloq.close();
							ps.close();
							//ps.clearParameters();
							
							
							String estilo = (bloqueado)?"bloqueado":"formas";					
							reg = new HashMap();
							reg.put("EPO_REL", txtDesEPO);
							reg.put("COLDINAMICA", "");
							reg.put("NOM_REQUISITO", option + ". Firma de Contrato del Financiamiento Distribuidores");
							reg.put("CAMPOFECHA", "Fecha de Registro");
							reg.put("RESPONSABLE","");
							reg.put("REALIZO","");
							reg.put("EPO","");
							reg.put("VAL_ACTUAL","");
							reg.put("VAL_NUEVO","");
							reg.put("TIPO","");	
							reg.put("BLOQUEADA","");
							lista.add(reg);
							
							
							//ps.clearParameters();
							ps = con.queryPrecompilado(SQLReq9);
							rsQry = ps.executeQuery();	
							
							i = 0;
							while (rsQry.next()){
								reg = new HashMap();
								reg.put("EPO_REL", txtDesEPO);
								if((rsQry.getString(3)==null?"":rsQry.getString(3)).equals("S")){
									reg.put("COLDINAMICA", "PALOMA");
									si_no = "S";
								}else{
									reg.put("COLDINAMICA", "NADA");
									si_no = "N";
								}
														
								reg.put("NOM_REQUISITO"," "+option +"."+ String.valueOf(i+1) + " " + rsQry.getString(2));
								reg.put("CAMPOFECHA", (rsQry.getString("fechahab")==null)?"":rsQry.getString("fechahab"));
								reg.put("RESPONSABLE","NAFIN");
								reg.put("REALIZO", "S".equals(si_no)?"NAFIN":"");	
								reg.put("EPO","");
								reg.put("VAL_ACTUAL","");
								reg.put("VAL_NUEVO","");
								reg.put("TIPO","");	
								reg.put("BLOQUEADA","");
								lista.add(reg);
								i++;
							}
							rsQry.close();
							ps.close(); 
							//ps.clearParameters();
							
						}//fin aplica
					}//fin claveProducto == 4
				
				}//fin bExiste
			}// fin for epo_vec
		}catch(Exception e) {
			e.printStackTrace();
			throw new AppException("No se pudo obtener la consulta", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();				
			}
			log.info("qrySentencia  "+qrySentencia);
			log.info("getConsultaGeneral (S)");
		}
		return lista;
	}
	
	public String actualizarLista(){
		int i =0;
		int cont = 0;
		String objeto = "";
		String val_nvo = "";
		String strMensaje = "";
		String txtCveProducto = getClaveProducto();
		String txtCvePYME = getClavePyme();
		String epo1[] = getEpo1();
		String epo5[] = getEpo5();
		String epo8[] = getEpo8();
		String val_act1[] = getValEpo1();
		String val_act5[] = getValEpo5();
		String val_act8[] = getValEpo8();
		String val_nvo1[] = getValNuevoEpo1();
		String val_nvo5[] = getValNuevoEpo5();
		String val_nvo8[] = getValNuevoEpo8();
		
		IListaRequisitos BeanListaRequisitos = null;
			
		try{
			BeanListaRequisitos = netropology.utilerias.ServiceLocator.getInstance().lookup("ListaRequisitosEJB", IListaRequisitos.class);
		}catch(Exception Error){
			strMensaje = "EXISTEN DIFICULTADES TECNICAS, INTENTE MÁS TARDE";
		}
		
		if("1".equals(txtCveProducto)){
			cont = (epo1 == null) ? 0 : epo1.length;
			for (i = 0; i < cont; i++) {
				if (!val_act1[i].equals(val_nvo1[i])) {
					objeto = ((val_nvo1[i]) == null) ? "" : (val_nvo1[i]);			
					if(objeto.equals("S")) {
						val_nvo = "S";
					} else {	
						val_nvo = "N";
					}
					try{
						BeanListaRequisitos.vactualizarDescReq1(val_nvo,epo1[i]);
						strMensaje = "La lista de requisitos ha sido actualizada.";
						System.out.println("mensaje: "+ strMensaje);
					}
					catch(NafinException Error){
						strMensaje = Error.getMsgError();
					}catch(Exception e){
										
					}
				}else{
					strMensaje = "La lista de requisitos ha sido actualizada.";
				}
			}

			cont = (epo5 == null) ? 0 : epo5.length;
			for (i = 0; i < cont; i++) {
				if (!val_act5[i].equals(val_nvo5[i])) {
					objeto = ((val_nvo5[i]) == null) ? "" : (val_nvo5[i]);
					if(objeto.equals("S")) {
						val_nvo = "R";
					} else {
						val_nvo = "S";
					}
					try{			
						BeanListaRequisitos.vactualizarDescReq5(val_nvo,epo5[i],txtCvePYME);
						strMensaje = "La lista de requisitos ha sido actualizada.";
					}
					catch(NafinException Error){
						strMensaje = Error.getMsgError();
					}catch(Exception e){
						
					}
				}else{
					strMensaje = "La lista de requisitos ha sido actualizada.";
				}
			}

			cont = (epo8 == null) ? 0 : epo8.length;
			for (i = 0; i < cont; i++) {			
				if (!val_act8[i].equals(val_nvo8[i])) {
					objeto = ((val_nvo8[i]) == null) ? "" : (val_nvo8[i]);
					val_nvo = (objeto.equals("S"))?"H":"R";
					try{
						BeanListaRequisitos.vactualizarDescReq8(val_nvo,epo8[i],txtCvePYME);
						strMensaje = "La lista de requisitos ha sido actualizada.";
					}
					catch(NafinException Error){
						strMensaje = Error.getMsgError();
					}catch(Exception e){
						
					}
				}else{
					strMensaje = "La lista de requisitos ha sido actualizada.";
				}
			}
		}
		else if("4".equals(txtCveProducto)){
			cont = (epo8 == null) ? 0 : epo8.length;
			for (i = 0; i < cont; i++) {
				if (!val_act8[i].equals(val_nvo)) {
					objeto = ((val_nvo8[i]) == null) ? "N" : (val_nvo8[i]);
					if(objeto.equals("S")) {
						val_nvo = "S";
					} else {
						val_nvo = "N";
					}
					try{ 
						BeanListaRequisitos.vactualizarDistClavesAltoR(epo8[i],txtCvePYME,val_nvo);
						strMensaje = "La lista de requisitos ha sido actualizada.";
						System.out.println("mensaje: "+ strMensaje);
					}
					catch(NafinException Error){
						strMensaje = Error.getMsgError();
						//throw Error;
					}catch(Exception e){
						
					}
				}else{
					strMensaje = "La lista de requisitos ha sido actualizada.";
				}
			}
		}
		return strMensaje;
	}
	
	public List getConditions() {  return conditions;  }


	public void setClaveNafin(String claveNafin) {
		this.claveNafin = claveNafin;
	}


	public String getClaveNafin() {
		return claveNafin;
	}


	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}


	public String getClavePyme() {
		return clavePyme;
	}


	public void setClaveProducto(String claveProducto) {
		this.claveProducto = claveProducto;
	}


	public String getClaveProducto() {
		return claveProducto;
	}


	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}


	public String getFechaInicio() {
		return fechaInicio;
	}


	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}


	public String getFechaFin() {
		return fechaFin;
	}


	public void setClavePerfil(String clavePerfil) {
		this.clavePerfil = clavePerfil;
	}


	public String getClavePerfil() {
		return clavePerfil;
	}


	public void setEpoVector(Vector epoVector) {
		this.epoVector = epoVector;
	}


	public Vector getEpoVector() {
		return epoVector;
	}

	public void setValEpo1(String[] valEpo1) {
		this.valEpo1 = valEpo1;
	}


	public String[] getValEpo1() {
		return valEpo1;
	}


	public void setValEpo5(String[] valEpo5) {
		this.valEpo5 = valEpo5;
	}


	public String[] getValEpo5() {
		return valEpo5;
	}


	public void setValEpo8(String[] valEpo8) {
		this.valEpo8 = valEpo8;
	}


	public String[] getValEpo8() {
		return valEpo8;
	}


	public void setValNuevoEpo1(String[] valNuevoEpo1) {
		this.valNuevoEpo1 = valNuevoEpo1;
	}


	public String[] getValNuevoEpo1() {
		return valNuevoEpo1;
	}


	public void setValNuevoEpo5(String[] valNuevoEpo5) {
		this.valNuevoEpo5 = valNuevoEpo5;
	}


	public String[] getValNuevoEpo5() {
		return valNuevoEpo5;
	}


	public void setValNuevoEpo8(String[] valNuevoEpo8) {
		this.valNuevoEpo8 = valNuevoEpo8;
	}


	public String[] getValNuevoEpo8() {
		return valNuevoEpo8;
	}


	public void setEpo1(String[] epo1) {
		this.epo1 = epo1;
	}


	public String[] getEpo1() {
		return epo1;
	}


	public void setEpo5(String[] epo5) {
		this.epo5 = epo5;
	}


	public String[] getEpo5() {
		return epo5;
	}


	public void setEpo8(String[] epo8) {
		this.epo8 = epo8;
	}


	public String[] getEpo8() {
		return epo8;
	}
}