package com.netro.cadenas;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsTasasOper implements IQueryGeneratorRegExtJS  {
	public ConsTasasOper() {
	}
	
	private List 	conditions;
	StringBuffer 	strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsTasa.class);//Variable para enviar mensajes al log.
  
	private String icTasa;
	private String fechaInicio;
	private String fechaFin;
 
	public String getAggregateCalculationQuery() {
		return "";
 	}  
		 
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();   
		strQuery 		= new StringBuffer(); 
		
		log.debug("fechaInicio"+this.fechaInicio);
		log.debug("fechaFin"+this.fechaFin);
		log.debug("icTasa"+this.icTasa);			
		
		
		strQuery.append(
			"SELECT /*+use_nl(v cat)*/ cat.cd_nombre, cat.ic_tasa, v.fn_valor, "+
		   "TO_CHAR(v.dc_fecha,'dd/mm/yyyy') as FECHA "   +
			"	FROM com_mant_tasa v, comcat_tasa cat "   +
			"	WHERE cat.ic_tasa = v.ic_tasa(+) "   +
			"  and    v.dc_fecha >= TO_DATE (?, 'dd/mm/yyyy') "   +
			"  AND v.dc_fecha < TO_DATE (?, 'dd/mm/yyyy') + 1 " +
			" 	AND cat.cs_disponible = ? ");	
			conditions.add(this.fechaInicio);
			conditions.add(this.fechaFin);
			conditions.add("S");
		
		if(!"TODAS".equals(icTasa))
						strQuery.append(" AND cat.ic_tasa = "+icTasa);
			strQuery.append(" ORDER BY dc_fecha, cd_nombre ");
					
			log.debug("getDocumentQueryFile "+strQuery.toString());
			log.debug("getDocumentQueryFile)"+conditions);
		
		return strQuery.toString();
		
 	} 		
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		
		if ("CSV".equals(tipo)) {
			linea = ",TASA, VALOR\n";
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
				
				String  campoFecha="";
				while (rs.next()) {
					String fecha = (rs.getString("FECHA") == null) ? "" : rs.getString("FECHA");
					if(!campoFecha.equals(fecha))
					{
					campoFecha=fecha;
					buffer.write("\n");
					buffer.write("Fecha: ,"+fecha+"\n");
					}
					String cd_nombre = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
					String valor  = (rs.getString("FN_VALOR") == null) ? "" : rs.getString("FN_VALOR");
					linea = ","+cd_nombre.replace(',',' ')+","+valor.replace(',',' ')+"\n";
					
					buffer.write(linea);
				}
				buffer.close();
			} catch (Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		} 
		/*else if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(2, 100);
				pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor","celda01",ComunesPDF.CENTER);
							
				while (rs.next()) {
					String cd_nombre = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
					String valor  = (rs.getString("VALOR") == null) ? "" : rs.getString("VALOR");
					
					pdfDoc.setCell(cd_nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(valor,2),"formas",ComunesPDF.RIGHT);
					
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}*/

		return nombreArchivo;
		
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	
	public List getConditions() {
		return conditions;
	}

  public String getIcTasa()
  {
    return icTasa;
  }

  public void setIcTasa(String icTasa)
  {
    this.icTasa = icTasa;
  }


	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}


	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
}