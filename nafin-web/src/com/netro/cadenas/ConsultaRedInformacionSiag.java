package com.netro.cadenas;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.ServiceLocator;

public class ConsultaRedInformacionSiag implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaRedInformacionSiag.class);
	private List   conditions;
	private String fechaInicial;
	private String fechaFinal;
	private String portafolio;

	public ConsultaRedInformacionSiag(){}


	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery() {
		return null;
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery() {
		return null;
	}

	/**
	 * Realiza la consulta para generar el archivo csv
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append("SELECT CG_RAZON_SOCIAL AS NOMPYME,                              \n ");
		qrySentencia.append("       CG_RFC AS RFCPYME,                                       \n ");
		qrySentencia.append("       CG_ESTADO AS ESTADOPYME,                                 \n ");
		qrySentencia.append("       FN_MONTO_ACTUAL_CRED AS MONTO,                           \n ");
		qrySentencia.append("       IN_PLAZO AS PLAZO,                                       \n ");
		qrySentencia.append("       CG_INTERNEDIARIOF AS INTERMEDIARIO,                      \n ");
		qrySentencia.append("       IN_ESTRATO_INI AS ESTATOINI,                             \n ");
		qrySentencia.append("       IN_ESTRATO_NUEVO AS ESTATONUE,                           \n ");
		qrySentencia.append("       IN_SECTOR AS SECTOR,                                     \n ");
		qrySentencia.append("       TO_CHAR (DF_FECHA_REGISTRO, 'dd/mm/yyyy') AS FREGISTRO,  \n ");
		qrySentencia.append("       CG_PORTAFOLIO AS PORTAFOLIO,                             \n ");
		qrySentencia.append("       IN_CONREC_CLAVE AS CLAVECONREC,                          \n ");
		qrySentencia.append("       CG_CAMPOS_ADICIONALES AS CAMPOSADICIONALES               \n ");
		qrySentencia.append("  FROM COM_CRUCE_SIAG                                           \n ");
		qrySentencia.append(" WHERE DF_FECHA_REGISTRO >= TO_DATE (?, 'dd/mm/yyyy')           \n ");
		qrySentencia.append("   AND DF_FECHA_REGISTRO <= TO_DATE (?, 'dd/mm/yyyy') + 1       \n ");
		qrySentencia.append("   AND CG_PORTAFOLIO IN ('" + this.portafolio + "')                ");

		conditions.add(this.fechaInicial);
		conditions.add(this.fechaFinal);

		log.info("Sentencia:\n"   + qrySentencia.toString());
		log.info("Condiciones: "  + conditions.toString()  );
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();

	}

	/**
	 * Metodo para generar un archivo PDF, utilizando paginaci�n
	 * @return nombreArchivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros rs, String path, String tipo) {
		return null;
	}

	/**
	 * Metodo para generar un archivo CSV, de todos los registros
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");

		String             nombreArchivo    = "";
		String             camposAdicionales= ""; 
		StringBuffer       contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer           = null;
		BufferedWriter     buffer           = null;
		int                total            = 0;

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer        = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer        = new BufferedWriter(writer);

			// Obtengo el nombre del campo adicional
			camposAdicionales = this.getCamposAdicionales();

			contenidoArchivo = new StringBuffer();
			contenidoArchivo.append("Razon Social,"             );
			contenidoArchivo.append("RFC,"                      );
			contenidoArchivo.append("Estado,"                   );
			contenidoArchivo.append("Monto Actual del Credito," );
			contenidoArchivo.append("Plazo,"                    );
			contenidoArchivo.append("Intermediario,"            );
			contenidoArchivo.append("Estrato Inicial,"          );
			contenidoArchivo.append("Nuevo,"                    );
			contenidoArchivo.append("Sector,"                   );
			contenidoArchivo.append("Fecha de Registro,"        );
			contenidoArchivo.append("Descripci�n de Portafolio,");
			contenidoArchivo.append("Clave CONREC,"             );
			contenidoArchivo.append(camposAdicionales           );
			contenidoArchivo.append("\r\n"                      );

			while (rs.next()){
				String nomPyme        = (rs.getString("NOMPYME")           == null) ? "" : rs.getString("NOMPYME");
				String rfcPyme        = (rs.getString("RFCPYME")           == null) ? "" : rs.getString("RFCPYME");
				String estadoPyme     = (rs.getString("ESTADOPYME")        == null) ? "" : rs.getString("ESTADOPYME");
				String monto          = (rs.getString("MONTO")             == null) ? "" : rs.getString("MONTO");
				String plazo          = (rs.getString("PLAZO")             == null) ? "" : rs.getString("PLAZO");
				String intermediario  = (rs.getString("INTERMEDIARIO")     == null) ? "" : rs.getString("INTERMEDIARIO");
				String estatoIni      = (rs.getString("ESTATOINI")         == null) ? "" : rs.getString("ESTATOINI");
				String estatoNue      = (rs.getString("ESTATONUE")         == null) ? "" : rs.getString("ESTATONUE");
				String sector         = (rs.getString("SECTOR")            == null) ? "" : rs.getString("SECTOR");
				String fregistro      = (rs.getString("FREGISTRO")         == null) ? "" : rs.getString("FREGISTRO");
				String portafolio     = (rs.getString("PORTAFOLIO")        == null) ? "" : rs.getString("PORTAFOLIO");
				String claveConrec    = (rs.getString("CLAVECONREC")       == null) ? "" : rs.getString("CLAVECONREC");
				String campoAdicional = (rs.getString("CAMPOSADICIONALES") == null) ? "" : rs.getString("CAMPOSADICIONALES");

				contenidoArchivo.append(nomPyme.replace(',',' ')        +", ");
				contenidoArchivo.append(rfcPyme.replace(',',' ')        +", ");
				contenidoArchivo.append(estadoPyme.replace(',',' ')     +", ");
				contenidoArchivo.append(monto.replace(',',' ')          +", ");
				contenidoArchivo.append(plazo.replace(',',' ')          +", ");
				contenidoArchivo.append(intermediario.replace(',',' ')  +", ");
				contenidoArchivo.append(estatoIni.replace(',',' ')      +", ");
				contenidoArchivo.append(estatoNue.replace(',',' ')      +", ");
				contenidoArchivo.append(sector.replace(',',' ')         +", ");
				contenidoArchivo.append(fregistro.replace(',',' ')      +", ");
				contenidoArchivo.append(portafolio.replace(',',' ')     +", ");
				contenidoArchivo.append(claveConrec.replace(',',' ')    +", ");
				contenidoArchivo.append(campoAdicional.replace(',',' ')      );
				contenidoArchivo.append("\r\n"                               );
				total++;

				if(total==1000){
					total=0;
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio
				}
			}

			//CUANDO NO HAY REGISTROS
			if(total == 0){
				contenidoArchivo.append( " No se encontr� ning�n registro para los criter�os seleccionados. " );
				contenidoArchivo.append( "\r\n");
			}

			buffer.write(contenidoArchivo.toString());
			buffer.close();
			contenidoArchivo = new StringBuffer();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo csv ", e);
		} finally {
			try {
			} catch(Exception e) {}
		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;

	}

/*****************************************
 *          M�TODOS DEL FODEA            *
 *****************************************/
	/**
	 * OBTENGO EL CAMPO ADICIONAL
	 * @return epoCompranet
	 */
	public String getCamposAdicionales(){

		log.info("getCamposAdicionales(E)");

		String             campo  = "";
		String             query  = "";
		AccesoDB           con    = new AccesoDB();
		PreparedStatement  ps     = null;
		ResultSet          rs     = null;

		try{
			con.conexionDB();

			query = "SELECT CG_DESCRIPCION FROM COM_CAMPOS_ADIC_SIAG WHERE IC_NUMERO_LINEA = 1";
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			if(rs != null && rs.next())
				campo = rs.getString("CG_DESCRIPCION");
			else
				campo = "";

			rs.close();
			ps.close();

			log.debug("query: "          + query);
			log.debug("campoAdicional: " + campo);

		} catch(Exception e){
			log.error("getCamposAdicionales(Exception)");
			log.error("query: " + query);
			e.printStackTrace();
		} finally{
			if (rs  != null) try { rs.close(); } catch(Exception e) {}
			if (ps  != null) try { ps.close(); } catch(Exception e) {}
			if (con != null && con.hayConexionAbierta()) con.cierraConexionDB();
		}

		log.info("getCamposAdicionales(S)");
		return campo;

	}

/*****************************************
 *          GETTERS AND SETTERS          *
 *****************************************/
	public List getConditions() {
		return conditions;
	}

	public String getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public String getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getPortafolio() {
		return portafolio;
	}

	public void setPortafolio(String portafolio) {
		this.portafolio = portafolio;
	}

}