package com.netro.cadenas;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsProyectoPEF implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsProyectoPEF.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_epo;
	private String ic_area_promocion;
	private String ic_subdireccion;
	private String ic_lider_promotor;
	private String ic_region;
	private String ic_tipo_epo;
	private String ic_subtipo_epo;
	private String ic_sector_epo;
	
		 
	public ConsProyectoPEF() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
	
		qrySentencia.append(" SELECT " +
				" e.ic_epo AS IC_EPO , "+
				" e.cg_razon_social AS NOMBRE_EPO, " +
				" s.ic_area_promocion AS AREA_PROMOCION, " +
				" o.ic_subdireccion AS SUB_DIRECCION, " +
				" p.ic_lider_promotor AS LIDER_PROMOTOR, "+
				" r.ic_region AS REGION, " +
				" e.ic_tipo_epo AS TIPO_EPO, "+
				" e.ic_subtipo_epo AS SUBTIPO_EPO,  "+
				" e.ic_sector_epo AS SECTOR_EPO " +
            " FROM comcat_epo e, comcat_area_promocion s, comcat_subdireccion o, comcat_lider_promotor p, comcat_region r " +
				" WHERE s.ic_area_promocion(+) = e.ic_area_promocion  " +
				" AND o.ic_subdireccion(+) =  e.ic_subdireccion " +
				" AND r.ic_region(+) =  e.ic_region " +
				" AND p.ic_lider_promotor(+) =  e.ic_lider_promotor ");
				
				
			if(!ic_epo.equals("")) {
				qrySentencia.append(" AND e.ic_epo = ? ");
				conditions.add(ic_epo);        		
			}

			if(!ic_area_promocion.equals("")) {
       		qrySentencia.append(" AND e.ic_area_promocion =  ? ");
				conditions.add(ic_area_promocion);        		
			}
			
			if(!ic_subdireccion.equals("")){
       		qrySentencia.append(" AND e.ic_subdireccion = ? ");
				conditions.add(ic_subdireccion);    
			}
	
			if(!ic_lider_promotor.equals("")) {
       		qrySentencia.append(" AND e.ic_lider_promotor =  ? ");
				conditions.add(ic_lider_promotor);    
			}
			
			if(!ic_region.equals("")) {
       		qrySentencia.append(" AND e.ic_region =  ? ");
				conditions.add(ic_region);   
			}
			if(!ic_tipo_epo.equals("")) {
       		qrySentencia.append(" AND e.ic_tipo_epo =  ? ");
				conditions.add(ic_tipo_epo);   
			}
				
			if(!ic_subtipo_epo.equals("")) {
       		qrySentencia.append(" AND e.ic_subtipo_epo = ?  ");
				conditions.add(ic_subtipo_epo);   
			}
			
			if(!ic_sector_epo.equals("")){
				qrySentencia.append(" AND e.ic_sector_epo = ? ");
				conditions.add( ic_sector_epo); 
			}
				
			qrySentencia.append(" ORDER BY e.cg_razon_social ");
				
		
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
		
		
		
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	
	public void ovactualizarEpos(List ldatos)   throws NafinException{
		AccesoDB con =  new AccesoDB();
		PreparedStatement ps = null;
		List lregistros = null;
		boolean ok = true;
		List varBind = new ArrayList();
		
    	try {
      	       
			con.conexionDB();
			
			String qry = " UPDATE comcat_epo "+
				" set ic_area_promocion = ?, "+
				" ic_subdireccion = ?, "+
				" ic_lider_promotor = ?, "+
				" ic_region = ?, "+
				" ic_tipo_epo = ?,  "+
				" ic_subtipo_epo = ?, "+
				" ic_sector_epo = ? "+
				" where ic_epo = ?";			
				ps = con.queryPrecompilado(qry);
	
				for(int a=0;a<ldatos.size();a++){
					lregistros = new ArrayList();
					lregistros = (List)ldatos.get(a); 
					
					varBind = new ArrayList();
					varBind.add(lregistros.get(1).toString().equals("0")?"":lregistros.get(1).toString());	
					varBind.add(lregistros.get(2).toString().equals("0")?"":lregistros.get(2).toString());	
					varBind.add(lregistros.get(3).toString().equals("0")?"":lregistros.get(3).toString());	
					varBind.add(lregistros.get(4).toString().equals("0")?"":lregistros.get(4).toString());	
					varBind.add(lregistros.get(5).toString().equals("0")?"":lregistros.get(5).toString());	
					varBind.add(lregistros.get(6).toString().equals("0")?"":lregistros.get(6).toString());	
					varBind.add(lregistros.get(7).toString().equals("0")?"":lregistros.get(7).toString());	
					varBind.add(lregistros.get(0).toString().equals("0")?"":lregistros.get(0).toString());	
				
					System.out.println("qry: ::: "+qry);				  
					System.out.println("varBind: ::: "+varBind);				  
					
					ps = con.queryPrecompilado(qry, varBind);
					ps.executeUpdate();
					ps.execute();        		
					ps.close();
				}
        
      	} catch(Exception e){
      		ok = false;
				e.printStackTrace();
      		System.out.println("Exception: ovactualizarEpos::: "+e);
			throw new NafinException("SIST0001");
		} finally {
      		if(con.hayConexionAbierta()){
      			con.terminaTransaccion(ok);
      			con.cierraConexionDB();
      		}       		
      	}
	}
	
	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_area_promocion() {
		return ic_area_promocion;
	}

	public void setIc_area_promocion(String ic_area_promocion) {
		this.ic_area_promocion = ic_area_promocion;
	}

	public String getIc_subdireccion() {
		return ic_subdireccion;
	}

	public void setIc_subdireccion(String ic_subdireccion) {
		this.ic_subdireccion = ic_subdireccion;
	}

	public String getIc_lider_promotor() {
		return ic_lider_promotor;
	}

	public void setIc_lider_promotor(String ic_lider_promotor) {
		this.ic_lider_promotor = ic_lider_promotor;
	}

	public String getIc_region() {
		return ic_region;
	}

	public void setIc_region(String ic_region) {
		this.ic_region = ic_region;
	}

	public String getIc_tipo_epo() {
		return ic_tipo_epo;
	}

	public void setIc_tipo_epo(String ic_tipo_epo) {
		this.ic_tipo_epo = ic_tipo_epo;
	}

	public String getIc_subtipo_epo() {
		return ic_subtipo_epo;
	}

	public void setIc_subtipo_epo(String ic_subtipo_epo) {
		this.ic_subtipo_epo = ic_subtipo_epo;
	}

	public String getIc_sector_epo() {
		return ic_sector_epo;
	}

	public void setIc_sector_epo(String ic_sector_epo) {
		this.ic_sector_epo = ic_sector_epo;
	}
	

}