package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import netropology.utilerias.AppException;

import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

/**
 * Clase para la consulta de cuentas benefiario por autorizar 
 */
public class ConsBeneficiarioCtaAutorizadas implements IQueryGeneratorRegExtJS {
	
	private static final Log LOG = ServiceLocator.getInstance().getLog(ConsBeneficiarioCtaAutorizadas.class);
	
	private List conditions;
	StringBuilder qrySentencia = new StringBuilder("");
	private String 	paginaOffset;
	private String 	paginaNo;
	private String icPyme;
	private String cboEPO;
	private String cboBeneficiario;
	private String fechaAltaIni;
	private String fechaAltaFin;
	
	
	
	
	
	public ConsBeneficiarioCtaAutorizadas() {
	}

	/**
	 * Obtiene las llaves primarias
	 * @return qrySentencia
	 */
	public String getDocumentQuery() {

		LOG.info("getDocumentQuery (E)");
		qrySentencia = new StringBuilder();
		conditions 		= new ArrayList();
		
		qrySentencia.append( " SELECT  c. IC_CONSECUTIVO  "+
									
									" FROM  COMREL_CTA_BAN_DISTRIBUIDO c,   "+
									" comcat_if i,   "+
									" comcat_pyme p,   "+
									" comrel_nafin n,  "+
									" comcat_epo e,  "+
									" comcat_moneda m,  "+
									" comrel_if_epo r,  "+
									" comcat_bancos_tef b,  "+
									" comcat_plaza pl  "+
									" where p.ic_pyme = c.ic_pyme   "+ 
									" and e.ic_epo = c.ic_epo   "+
									" and m.ic_moneda = c.ic_moneda   "+
									" and n.IC_EPO_PYME_IF = c.ic_pyme   "+
									" and n.CG_TIPO = ?  "+
									" and c.IC_BENEFICIARIO = i.ic_if   "+
									" and r.cs_beneficiario = ?    "+
									" and r.ic_epo = e.ic_epo  "+ 
									" and r.ic_if = i. ic_if   "+
									" and b.IC_BANCOS_TEF = c.IC_BANCOS_TEF   "+
									" and pl.ic_plaza = c.ic_plaza ");
		
		conditions.add("P");
		conditions.add("S");
		
		if (!"".equals(icPyme)  ) {
			qrySentencia.append( " and c.ic_pyme = ? ");
			conditions.add(icPyme);
		}
		
		if (!"".equals(cboEPO)  ) {
			qrySentencia.append( " and c.ic_epo = ? ");
			conditions.add(cboEPO);
		}
		
		if (!"".equals(cboBeneficiario)  ) {
			qrySentencia.append( " and c.IC_BENEFICIARIO = ? ");
			conditions.add(cboBeneficiario);
		}
		
	   if (!"".equals(fechaAltaIni)  && !"".equals(fechaAltaFin)   ) {
			qrySentencia.append(" and c.DF_FECHA_ALTA >= TO_DATE(?, 'dd/mm/yyyy') "+
								     " and  c.DF_FECHA_ALTA < TO_DATE(?, 'dd/mm/yyyy')+1 ");
			conditions.add(fechaAltaIni);
			conditions.add(fechaAltaFin);
		}
		

		qrySentencia.append(" ORDER BY c.IC_CONSECUTIVO ASC ");

		LOG.info("Sentencia: " + qrySentencia.toString());
		LOG.info("Condiciones: " + conditions.toString());
		LOG.info("getDocumentQuery (S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene los registros totales
	 * @return qrySentencia
	 */
	public String getAggregateCalculationQuery() {

		LOG.info("getAggregateCalculationQuery (E)");
		qrySentencia = new StringBuilder();		
		conditions 		= new ArrayList();


		LOG.info("Sentencia: " + qrySentencia.toString());
		LOG.info("Condiciones: " + conditions.toString());
		LOG.info("getAggregateCalculationQuery (S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene la consulta a partir de las llaves primarias
	 * @param pageIds
	 * @return
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {

		LOG.info("getDocumentSummaryQueryForIds (E)");
		qrySentencia = new StringBuilder();
	   conditions 		= new ArrayList();
	
	
		LOG.info("pageIds   "+pageIds  );
	
		qrySentencia.append( " SELECT  c.IC_CONSECUTIVO ,  "+
									" i.CG_RAZON_SOCIAL as NOM_BENEFICIARIO,  "+
									"	m.CD_NOMBRE as NOM_MONEDA, "+
									" b.CD_DESCRIPCION as BANCO_SERVICIO, "+
									" c.CG_NUMERO_CUENTA as NO_CUENTA, "+
									" c.CG_CUENTA_CLABE as NO_CUENTA_CLABE, "+
									" c.CG_CUENTA_SPID as NO_CUENTA_SPID, "+
									" c.CG_SWIFT as SWIFT, "+
									" c.CG_ABA as ABA, "+
									" c.CG_SUCURSAL as SUCURSAL, "+
									" p.CG_RAZON_SOCIAL as NOM_PROVEEDOR, "+
									" e.CG_RAZON_SOCIAL as NOM_EPO, "+
									" p.CG_RFC as RFC_PYME, "+
									" pl.CD_DESCRIPCION as PLAZA, "+
									" decode(c.CG_AUTORIZA_IF, 'N', 'No', 'S', 'Si') as AUTO_IF, "+
									" n.IC_NAFIN_ELECTRONICO as NUM_ELECTRONICO, "+
									" TO_CHAR (c.DF_FECHA_ALTA, 'DD/MM/YYYY')  as FECHA_ALTA  "+
									" FROM  COMREL_CTA_BAN_DISTRIBUIDO c,   "+
									" comcat_if i,   "+
									" comcat_pyme p,   "+
									" comrel_nafin n,  "+
									" comcat_epo e,  "+
									" comcat_moneda m,  "+
									" comrel_if_epo r,  "+
									" comcat_bancos_tef b,   "+
									" comcat_plaza pl  "+
									" where p.ic_pyme = c.ic_pyme   "+ 
									" and e.ic_epo = c.ic_epo   "+
									" and m.ic_moneda = c.ic_moneda   "+
									" and n.IC_EPO_PYME_IF = c.ic_pyme   "+
									" and n.CG_TIPO =  ?   "+
									" and c.IC_BENEFICIARIO = i.ic_if   "+
									" and r.cs_beneficiario = ?    "+
									" and r.ic_epo = e.ic_epo  "+ 
									" and r.ic_if = i. ic_if   "+
									" and b.IC_BANCOS_TEF = c.IC_BANCOS_TEF   "+
									" and pl.ic_plaza = c.ic_plaza " );
		
		conditions.add("P");
		conditions.add("S");
		
		
			qrySentencia.append(" AND (");
	
			for (int i = 0; i < pageIds.size(); i++){
				List lItem = (ArrayList)pageIds.get(i);
				if(i > 0){
					qrySentencia.append("  OR  ");
				}
				qrySentencia.append(" c.IC_CONSECUTIVO  = ?");
				conditions.add(new Long(lItem.get(0).toString()));
			}
			qrySentencia.append(" ) ");
			qrySentencia.append(" ORDER BY c.IC_CONSECUTIVO ASC ");
			
		

		LOG.info("Sentencia: " + qrySentencia.toString());
		LOG.info("Condiciones: " + conditions.toString());
		LOG.info("getDocumentSummaryQueryForIds (S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene la consulta con todos los registros para generar los archivos CSV y PDF.
	 * Es decir, no contempla paginaci�n.
	 * @return qrySentencia
	 */
	public String getDocumentQueryFile() {

		LOG.info("getDocumentQueryFile (E)");
		qrySentencia = new StringBuilder();		
		conditions = new ArrayList();


		qrySentencia.append( " SELECT  c. IC_CONSECUTIVO ,  "+
									" i.CG_RAZON_SOCIAL as NOM_BENEFICIARIO,  "+
									"	m.CD_NOMBRE as NOM_MONEDA, "+
									" b.CD_DESCRIPCION as BANCO_SERVICIO, "+
									" c.CG_NUMERO_CUENTA as NO_CUENTA, "+
									" c.CG_CUENTA_CLABE as NO_CUENTA_CLABE, "+
									" c.CG_CUENTA_SPID as NO_CUENTA_SPID, "+
									" c.CG_SWIFT as SWIFT, "+
									" c.CG_ABA as ABA, "+
									" c.CG_SUCURSAL as SUCURSAL, "+
									" p.CG_RAZON_SOCIAL as NOM_PROVEEDOR, "+
									" e.CG_RAZON_SOCIAL as NOM_EPO, "+
									" p.CG_RFC as RFC_PYME, "+
									" pl.CD_DESCRIPCION as PLAZA, "+
									" decode(c.CG_AUTORIZA_IF, 'N', 'No', 'S', 'Si') as AUTO_IF, "+
									" n.IC_NAFIN_ELECTRONICO as NUM_ELECTRONICO, "+
									" TO_CHAR (c.DF_FECHA_ALTA, 'DD/MM/YYYY')  as FECHA_ALTA  "+
									" FROM  COMREL_CTA_BAN_DISTRIBUIDO c,   "+
									" comcat_if i,   "+
									" comcat_pyme p,   "+
									" comrel_nafin n,  "+
									" comcat_epo e,  "+
									" comcat_moneda m,  "+
									" comrel_if_epo r,  "+
									" comcat_bancos_tef b,  "+
									" comcat_plaza pl  "+
									" where p.ic_pyme = c.ic_pyme   "+ 
									" and e.ic_epo = c.ic_epo   "+
									" and m.ic_moneda = c.ic_moneda   "+
									" and n.IC_EPO_PYME_IF = c.ic_pyme   "+
									" and n.CG_TIPO = ?    "+
									" and c.IC_BENEFICIARIO = i.ic_if   "+
									" and r.cs_beneficiario = ?   "+
									" and r.ic_epo = e.ic_epo  "+ 
									" and r.ic_if = i. ic_if   "+
									" and b.IC_BANCOS_TEF = c.IC_BANCOS_TEF   "+
									" and pl.ic_plaza = c.ic_plaza " );
		
		
		conditions.add("P");
		conditions.add("S");
		
		
		if (!"".equals(icPyme)  ) {
			qrySentencia.append( " and c.ic_pyme = ? ");
			conditions.add(icPyme);
		}
		
		if (!"".equals(cboEPO)  ) {
			qrySentencia.append( " and c.ic_epo = ? ");
			conditions.add(cboEPO);
		}
		
		if (!"".equals(cboBeneficiario)  ) {
			qrySentencia.append( " and c.IC_BENEFICIARIO = ? ");
			conditions.add(cboBeneficiario);
		}
		if (!"".equals(fechaAltaIni)  && !"".equals(fechaAltaFin)   ) {
			qrySentencia.append(" and c.DF_FECHA_ALTA >= TO_DATE(?, 'dd/mm/yyyy') "+
								     " and  c.DF_FECHA_ALTA < TO_DATE(?, 'dd/mm/yyyy')+1 ");
			conditions.add(fechaAltaIni);
			conditions.add(fechaAltaFin);
		}
		
		qrySentencia.append(" ORDER BY c.IC_CONSECUTIVO ASC "); 
		 
		LOG.info("Sentencia: " + qrySentencia.toString());
		LOG.info("Condiciones: " + conditions.toString());
		LOG.info("getDocumentQueryFile (S)");
		return qrySentencia.toString();
	}

	/**
	 * Genera los archivos CSV y PDF sin utilizar la paginaci�n, los datos que recibe en el request vienen de la consulta
	 * en el m�todo getDocumentQueryFile()
	 * @param request
	 * @param rs
	 * @param path
	 * @param tipo CSV o PDF
	 * @return nombreArchivo
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {

		LOG.info("crearCustomFile (E)");		
		CreaArchivo creaArchivo = new CreaArchivo();
		String nombreArchivo = "";
		StringBuilder contenidoArchivo = new StringBuilder();

		try {

			contenidoArchivo.append(" Beneficiario, Moneda, Banco de Servicio, No. De Cuenta , No. Cuenta Clabe, "+
											" No. Cuenta Spid, SWIFT, ABA, Sucursal, Proveedor,EPO,RFC, Plaza, Autorizaci�n IF, " +
				                     " N�m. Nafin Electr�nico,  Fecha de Alta  \n ");		
			
			
			while (rs.next())	{		
				String nomBeneficiario   = (rs.getString("NOM_BENEFICIARIO")   == null) ? "" : rs.getString("NOM_BENEFICIARIO");
				String nomMoneda   = (rs.getString("NOM_MONEDA")   == null) ? "" : rs.getString("NOM_MONEDA");
				String bancoServicio   = (rs.getString("BANCO_SERVICIO")   == null) ? "" : rs.getString("BANCO_SERVICIO");
				String noCuenta   = (rs.getString("NO_CUENTA")   == null) ? "" : rs.getString("NO_CUENTA");
				String noCuentaClabe   = (rs.getString("NO_CUENTA_CLABE")   == null) ? "" : rs.getString("NO_CUENTA_CLABE");
				String noCuentaSpid   = (rs.getString("NO_CUENTA_SPID")   == null) ? "" : rs.getString("NO_CUENTA_SPID");
				String swift   = (rs.getString("SWIFT")   == null) ? "" : rs.getString("SWIFT");
				String aba   = (rs.getString("ABA")   == null) ? "" : rs.getString("ABA");
				String sucursal   = (rs.getString("SUCURSAL")   == null) ? "" : rs.getString("SUCURSAL");
				String nomProveedor   = (rs.getString("NOM_PROVEEDOR")   == null) ? "" : rs.getString("NOM_PROVEEDOR");
				String nomEPo   = (rs.getString("NOM_EPO")   == null) ? "" : rs.getString("NOM_EPO");
				String rfcPyme   = (rs.getString("RFC_PYME")   == null) ? "" : rs.getString("RFC_PYME");
				String plaza   = (rs.getString("PLAZA")   == null) ? "" : rs.getString("PLAZA");
				String autoIF   = (rs.getString("AUTO_IF")   == null) ? "" : rs.getString("AUTO_IF");
				String numElectronico   = (rs.getString("NUM_ELECTRONICO")   == null) ? "" : rs.getString("NUM_ELECTRONICO");
				String fechaAlta   = (rs.getString("FECHA_ALTA")   == null) ? "" : rs.getString("FECHA_ALTA");
				
				contenidoArchivo.append( nomBeneficiario.replaceAll(",","")+ ", "+
				 nomMoneda.replaceAll(",","")+ ", "+
				 bancoServicio.replaceAll(",","")+ ", "+
				 noCuenta.replaceAll(",","")+ ", "+
				 noCuentaClabe.replaceAll(",","")+ ", "+
				 noCuentaSpid.replaceAll(",","")+ ", "+
				 swift.replaceAll(",","")+ ", "+
				 aba.replaceAll(",","")+ ", "+
				 sucursal.replaceAll(",","")+ ", "+
				 nomProveedor.replaceAll(",","")+ ", "+
				 nomEPo.replaceAll(",","")+ ", "+
				 rfcPyme.replaceAll(",","")+ ", "+
				 plaza.replaceAll(",","")+ ", "+
				 autoIF.replaceAll(",","")+ ", "+
				 numElectronico.replaceAll(",","")+ ", "+
				 fechaAlta.replaceAll(",","")+ "\n ");
			}
			
			
			
			
			
			if (!creaArchivo.make(contenidoArchivo.toString(), path, ".csv")) {
				nombreArchivo = "ERROR";
			} else {
				nombreArchivo = creaArchivo.nombre;
			}

		} catch (Throwable e) {
			throw new AppException("Error al generar el archivo CSV. ", e);
		}

		LOG.info("crearCustomFile (S)");
		return nombreArchivo;

	}

	/**
	 * Genera los archivos DPF utilizando paginaci�n
	 * @return nombreArchivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path,
												 String tipo) {

		LOG.info("crearPageCustomFile (E)");		
		CreaArchivo creaArchivo = new CreaArchivo();
		String nombreArchivo = "";
		StringBuilder contenidoArchivo = new StringBuilder();

		try {
			if (!creaArchivo.make(contenidoArchivo.toString(), path, ".csv")) {
				nombreArchivo = "ERROR";
			} else {
				nombreArchivo = creaArchivo.nombre;
			}

		} catch (Throwable e) {
			throw new AppException("Error al generar el archivo CSV. ", e);
		}

		return nombreArchivo;
	}


	public void setConditions(List conditions) {
		this.conditions = conditions;
	}

	public List getConditions() {
		return conditions;
	}


	public void setPaginaOffset(String paginaOffset) {
		this.paginaOffset = paginaOffset;
	}

	public String getPaginaOffset() {
		return paginaOffset;
	}

	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}

	public String getPaginaNo() {
		return paginaNo;
	}


	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}

	public String getIcPyme() {
		return icPyme;
	}

	public void setCboEPO(String cboEPO) {
		this.cboEPO = cboEPO;
	}

	public String getCboEPO() {
		return cboEPO;
	}

	public void setCboBeneficiario(String cboBeneficiario) {
		this.cboBeneficiario = cboBeneficiario;
	}

	public String getCboBeneficiario() {
		return cboBeneficiario;
	}

	public void setFechaAltaIni(String fechaAltaIni) {
		this.fechaAltaIni = fechaAltaIni;
	}

	public String getFechaAltaIni() {
		return fechaAltaIni;
	}

	public void setFechaAltaFin(String fechaAltaFin) {
		this.fechaAltaFin = fechaAltaFin;
	}

	public String getFechaAltaFin() {
		return fechaAltaFin;
	}

}
