package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsulProy1 implements  IQueryGeneratorRegExtJS {
	public ConsulProy1() {  }
	private List 	conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	StringBuffer 	strQuery;
	StringBuffer qrySentencia = new StringBuffer("");
	private static final Log log = ServiceLocator.getInstance().getLog(ConsulProy1.class);//Variable para enviar mensajes al log.
	private String txtCadProductiva;
	private String num_electronico;
	private String fec_ini;
	private String fec_fin;
	private String convenio_unico;
	private String codigoCliente;
	private String codigoProyecto;
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
 	}  
		 
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();   
		strQuery 		= new StringBuffer(); 
		StringBuffer condicion    = new StringBuffer();
		log.info("getDocumentQueryFile(E)");
		if(!codigoCliente.equals("")&&codigoCliente!=null){
				condicion.append("  and lc.codigo_cliente = ?");
				conditions.add(codigoCliente);
		}		
		if(!codigoProyecto.equals("")&&codigoProyecto!=null){
				Vector vectorProyecto = Comunes.explode("-",codigoProyecto);
				String agencia = vectorProyecto.get(0).toString();
				String subaplicacion = vectorProyecto.get(1).toString();
				String proyecto = vectorProyecto.get(2).toString();
				condicion.append(" and lc.codigo_agencia = ? ");
				conditions.add(agencia);
				condicion.append(" and lc.codigo_sub_aplicacion = ? ");
				conditions.add(subaplicacion);
				condicion.append(" and lc.numero_linea_credito = ? ");
				conditions.add(proyecto);
		}
		strQuery.append(" select lc.numero_linea_fondeo, lc.codigo_agencia"+
				" , lc.codigo_sub_aplicacion, lc.numero_linea_credito"+
				" , f.nombre, lc.codigo_tipo_linea"+
				" , TO_CHAR(lc.fecha_apertura,'yyyy/mm/dd') as fecha_apertura"+
				" , TO_CHAR(lc.fecha_de_vcmto,'yyyy/mm/dd') as fecha_de_vcmto"+
				" , pb.codigo_producto_banco, pb.descripcion"+
				" , p.codigo_base_operacion, lc.monto_asignado, lc.monto_comprometido"+
				" , lc.monto_disponible, lc.monto_utilizado, c.estado_contrato"+
				" from lc_lineas_creditos lc, lc_proyecto p, pr_contratos c"+
				" , no_productos_banco pb, no_bases_de_operacion bo, mg_financieras f"+
				" where "+
				" p.codigo_financiera = f.codigo_financiera"+
				" and p.tipo_financiera = f.tipo_financiera"+
				" and p.codigo_base_operacion = bo.codigo_base_operacion"+
				" and bo.codigo_producto_banco = pb.codigo_producto_banco"+
				" and lc.numero_linea_credito = p.numero_linea_credito"+
				" and lc.codigo_empresa = p.codigo_empresa"+
				" and lc.codigo_agencia = p.codigo_agencia"+
				" and lc.codigo_sub_aplicacion = p.codigo_sub_aplicacion"+
				" and lc.codigo_aplicacion = p.codigo_aplicacion"+
				" and p.numero_linea_credito = c.codigo_linea_credito"+
				" and p.codigo_empresa = c.codigo_empresa_lc"+
				" and p.codigo_agencia = c.codigo_agencia_lc"+
				" and p.codigo_sub_aplicacion = c.codigo_sub_aplicacion_lc"+
				" and rownum <= 15"+
				" and p.codigo_aplicacion='BLC'"+condicion.toString());
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		log.info("getDocumentQueryFile(S)");
		return strQuery.toString();
		
 	} 
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.debug("::crearCustomFile(E)");
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		int numeroRegistros = 0;
		double totalAdeudo = 0.0;
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		
		BigDecimal montoTotalAsignado = new BigDecimal("0.00");
		BigDecimal montoTotalComprometido = new BigDecimal("0.00");
		BigDecimal montoTotalDisponible = new BigDecimal("0.00");
		BigDecimal montoTotalUtilizado = new BigDecimal("0.00");
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(8, 80);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("L�nea de Fondeo","celda01",ComunesPDF.LEFT);  
				pdfDoc.setCell("Proyecto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Proyecto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Apertura","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Producto Banco","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Base operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Asignado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Comprometido","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Disponible","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Utilizado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				
				while (rs.next()) {
					numeroRegistros++;
					String numLineaFondeo = rs.getString("numero_linea_fondeo")==null?"":rs.getString("numero_linea_fondeo");
					String codigoAgencia = rs.getString("codigo_agencia")==null?"":rs.getString("codigo_agencia");
					String codigoSubAplicacion = rs.getString("codigo_sub_aplicacion")==null?"":rs.getString("codigo_sub_aplicacion");
					String numLineaCredito = rs.getString("numero_linea_credito")==null?"":rs.getString("numero_linea_credito");
					String intermediario = rs.getString("nombre")==null?"":rs.getString("nombre");
					String codigoTipoLinea = rs.getString("codigo_tipo_linea")==null?"":rs.getString("codigo_tipo_linea");
					String fechaApertura = rs.getString("fecha_apertura")==null?"":rs.getString("fecha_apertura");
					String fechaVencimiento = rs.getString("fecha_de_vcmto")==null?"":rs.getString("fecha_de_vcmto");
					String codigoProductoBanco = rs.getString("codigo_producto_banco")==null?"":rs.getString("codigo_producto_banco");
					String productoBanco = rs.getString("descripcion")==null?"":rs.getString("descripcion");
					String codigoBaseOperacion = rs.getString("codigo_base_operacion")==null?"":rs.getString("codigo_base_operacion");
					String montoAsignado = rs.getString("monto_asignado")==null?"":rs.getString("monto_asignado");
					String montoComprometido = rs.getString("monto_comprometido")==null?"":rs.getString("monto_comprometido");
					String montoDisponible = rs.getString("monto_disponible")==null?"":rs.getString("monto_disponible");
					String montoUtilizado = rs.getString("monto_utilizado")==null?"":rs.getString("monto_utilizado");
					String estadoContrato = rs.getString("estado_contrato")==null?"":rs.getString("estado_contrato");
					if (montoAsignado!=null) montoTotalAsignado = montoTotalAsignado.add(new BigDecimal(montoAsignado));
					if (montoComprometido!=null) montoTotalComprometido = montoTotalComprometido.add(new BigDecimal(montoComprometido));
					if (montoDisponible!=null) montoTotalDisponible = montoTotalDisponible.add(new BigDecimal(montoDisponible));
					if (montoUtilizado!=null) montoTotalUtilizado = montoTotalUtilizado.add(new BigDecimal(montoUtilizado));
				
					pdfDoc.setCell("A","fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(numLineaFondeo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(codigoAgencia+"-"+codigoSubAplicacion+"-"+numLineaCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(codigoTipoLinea,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaApertura,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(codigoProductoBanco+" "+productoBanco,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("B","fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(codigoBaseOperacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoAsignado,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoComprometido,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDisponible,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoUtilizado,2),"formas",ComunesPDF.RIGHT);		
					pdfDoc.setCell(estadoContrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					
				}
				/*pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
				pdfDoc.setCell("Monto total Asignado ","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Monto total Comprometido ","formas",ComunesPDF.CENTER,1);
				pdfDoc.setCell("Monto total Disponible ","formas",ComunesPDF.CENTER,1);
				pdfDoc.setCell("Monto total Utilizado ","formas",ComunesPDF.CENTER,1);
				pdfDoc.setCell("Total Proyectos","formas",ComunesPDF.CENTER,1);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
				
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoTotalAsignado,2),"formas",ComunesPDF.RIGHT,2);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoTotalComprometido,1),"formas",ComunesPDF.RIGHT,1);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoTotalDisponible,1),"formas",ComunesPDF.RIGHT,1);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(montoTotalUtilizado,1),"formas",ComunesPDF.RIGHT,1);
				pdfDoc.setCell(Integer.toString(numeroRegistros),"formas",ComunesPDF.CENTER,1);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER,1);*/
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				}
		}
		log.info(":: crearCustomFile(S)");
		return nombreArchivo;			
	}
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		return "";
	}

	public List getConditions() {
		return conditions;
	}
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
	

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	public String getCodigoProyecto() {
		return codigoProyecto;
	}

	public void setCodigoProyecto(String codigoProyecto) {
		this.codigoProyecto = codigoProyecto;
	}

}