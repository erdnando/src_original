package com.netro.cadenas.catalogos;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsManCatProgramaFondeoJR implements  IQueryGeneratorRegExtJS {
	public ConsManCatProgramaFondeoJR() {  }
	private List 	conditions;
	StringBuffer 	strQuery;
	private static final Log log = ServiceLocator.getInstance().getLog(ConsManCatProgramaFondeoJR.class);//Variable para enviar mensajes al log.
	
	public String getAggregateCalculationQuery() {
		return "";
 	}  
		 
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();   
		strQuery 		= new StringBuffer(); 
		log.info("getDocumentQueryFile(E)");
		strQuery.append("select  p.IC_PROGRAMA_FONDOJR, i.CG_RAZON_SOCIAL, p.CG_DESCRIPCION ,  i.ic_if " +
			" from COMCAT_PROGRAMA_FONDOJR p,comcat_if i"+
			" where i.ic_if = p.ic_if  "+
			" order by p.IC_PROGRAMA_FONDOJR  " );
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return strQuery.toString();
 	} 		
	
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		String nombreArchivo = "";
		return nombreArchivo;
	}
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	public List getConditions() {
		return conditions;
	}



	


}