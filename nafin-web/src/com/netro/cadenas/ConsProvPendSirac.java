package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsProvPendSirac implements IQueryGeneratorRegExtJS {
	//Variables
	StringBuffer qrySentencia = new StringBuffer();
	private List 		conditions;
		   

	private static final Log log = ServiceLocator.getInstance().getLog(ConsProvPendSirac.class);
	private String c_numNE;
	private String c_rfc;
	private String c_estatus;
	private String fec_ini;
	private String fec_fin;	
	private String chkRegErrores;
	

	
	
	public String getAggregateCalculationQuery() {
	return "";
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
	return "";
	}
	
	public String getDocumentQuery(){
	return "";
	}
	
	public String getDocumentQueryFile(){
		
		log.debug("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();		
		this.conditions = new ArrayList();
		
		
		qrySentencia.append( "SELECT TO_CHAR(CA.DF_AFILIA_SIRAC,'DD/MM/YYYY') FECAFILIA, CN.IC_NAFIN_ELECTRONICO NAFELEC, CP.CG_RAZON_SOCIAL NOMBREPYME, CP.CG_RFC RFC, CP.IN_NUMERO_SIRAC NUMSIRAC, ");
		qrySentencia.append( "		CA.CG_MENSAJE_ERROR ERRORSIRAC, CA.IC_PYME CVEPYME, CA.IC_EPO CVEEPO, CA.CS_AFILIA_SIRAC_PROC CSAFILIASIRAC, CP.CS_TIPO_PERSONA CSTIPOPERSONA, CA.CS_AFILIA_TROYA CSAFILIATROYA ");
		qrySentencia.append( "FROM COM_AFILIA_SIRAC_PEND CA, COMREL_NAFIN CN, COMCAT_PYME CP ");
		qrySentencia.append( "WHERE CA.IC_PYME = CN.IC_EPO_PYME_IF ");
		qrySentencia.append( "AND CA.IC_PYME = CP.IC_PYME ");
					
			
		if(!c_numNE.equals("")){
			qrySentencia.append(" AND CN.IC_NAFIN_ELECTRONICO = ? ");
			conditions.add(c_numNE);
		}
		if(!c_rfc.equals("")){
			qrySentencia.append(" AND CP.CG_RFC = UPPER(?) ");
			conditions.add(c_rfc);
		}
		if(!c_estatus.equals("")){
			qrySentencia.append(" AND CA.CS_AFILIA_SIRAC_PROC = ? ");
			conditions.add(c_estatus);
		}
		if(!fec_ini.equals("")){
			qrySentencia.append(" AND CA.DF_AFILIA_SIRAC >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
			conditions.add(fec_ini);
		}
		if(!fec_fin.equals("")){
			qrySentencia.append(" AND CA.DF_AFILIA_SIRAC < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
			conditions.add(fec_fin);
		}
		if(chkRegErrores.equals("S")){
			qrySentencia.append(" AND CA.CG_MENSAJE_ERROR IS NOT NULL ");
		}
		qrySentencia.append(" ORDER BY CP.CG_RAZON_SOCIAL ");
		
		
		log.debug("strQuery.toString(): "+qrySentencia.toString());
		log.debug("conditions.toString(): "+conditions);
		log.debug("getDocumentQueryFile(S)");
		
		return qrySentencia.toString();
	
	}
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		try{
		/*String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		ComunesXLS xlsDoc = new ComunesXLS();
		StringBuffer contenidoArchivo = new StringBuffer();		
		ComunesPDF pdfDoc = new ComunesPDF();
		HttpSession session = request.getSession();	
		
		List datos = this.getEncabezado(ic_if, accionBoton );
		System.out.println("datos---->"+datos);
		String lsFechaContrato = datos.get(0).toString();  
		String lsPuesto = datos.get(1).toString();
		String lsDatos = datos.get(2).toString();
		String lsNombre = datos.get(3).toString();
		
		int numReg =0;
		String mensaje = "", lsImporte ="0", lsTipoOperacion ="", lsFolio ="", lsNumPrestamo ="", lsImporteTotal ="", lsEmisor ="", lsLugarEmision ="",
					 lsFechaEmision ="", lsDomicilioPago ="", lsFechaVenc ="", lsNumDocto  ="", lsFechaOper ="",  lsNoAmort ="", lsTasaIF ="",  lsMoneda ="";
						
						
		try {
			
			if(tipo.equals("XLS")) {
			
				nombreArchivo = creaArchivo.nombreArchivo()+".xls";
				xlsDoc = new ComunesXLS(path+nombreArchivo);
			}
			
			if(tipo.equals("PDF"))  {
				
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
						(String)session.getAttribute("sesExterno"),
						(String) session.getAttribute("strNombre"),
						(String) session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			}
			
			if(accionBoton.equals("Consultar") ) {
			
				while(rs.next()){			
					lsTipoOperacion = (rs.getString("TIPO_PLAZO") == null) ? "" : rs.getString("TIPO_PLAZO");
					lsFolio = (rs.getString("NUM_FOLIO") == null) ? "" : rs.getString("NUM_FOLIO");
					lsNumPrestamo = (rs.getString("NUM_PRESTAMO") == null) ? "" : rs.getString("NUM_PRESTAMO");
					lsImporteTotal = (rs.getString("IMPORTE_TOTAL") == null) ? "" : rs.getString("IMPORTE_TOTAL");
					lsEmisor = (rs.getString("EMISOR") == null) ? "" : rs.getString("EMISOR");
					lsLugarEmision = (rs.getString("LUGAR_EMISION") == null) ? "" : rs.getString("LUGAR_EMISION");
					lsFechaEmision = (rs.getString("FECHA_EMISION") == null) ? "" : rs.getString("FECHA_EMISION");
					lsDomicilioPago = (rs.getString("DOMICILIO_PAGO") == null) ? "" : rs.getString("DOMICILIO_PAGO");
					lsFechaVenc = (rs.getString("FECHA_VENC") == null) ? "" : rs.getString("FECHA_VENC");
					lsNumDocto = (rs.getString("NUM_DOCTO") == null) ? "" : rs.getString("NUM_DOCTO");					
					lsFechaOper = (rs.getString("FECHA_OPERACION") == null) ? "" : rs.getString("FECHA_OPERACION");
					lsNoAmort = (rs.getString("NUM_AORTIZACION") == null) ? "" : rs.getString("NUM_AORTIZACION");
					lsTasaIF = (rs.getString("TASA_IF") == null) ? "" : rs.getString("TASA_IF");
					lsMoneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
							
					
					if(tipo.equals("XLS") && accionBoton.equals("Consultar") ) {
					
						if ("F".equals(lsTipoOperacion)) {
							xlsDoc.setCelda("FORMATO DE CONSTANCIA DE DEPOSITO DE DOCUMENTOS DESCONTADOS PARA OPERACIONES DE FACTORAJE ELECTRONICO", "titulo", ComunesXLS.CENTER, 12);
							xlsDoc.setCelda("De conformidad con el convenio que " +lsNombre + " y en la fecha que posteriormente se se�ala. celebr� con Nacional Financiera S.N.C. bajo protesta de decir verdad, se hace constar que para todos los efectos legales, nos asumimos como depositarios de los ", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("t�tulo(s) de cr�dito y/o documento(s) electr�nico(s) sin derecho a honorarios y asumiendo la responsabilidad civil y penal correspondiente al car�cter de depositario judicial, el(los) titulo(s) de cr�dito y/o documento(s) electr�nico(s) en que se consignan ", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("los derechos de cr�dito derivados de las operaciones de factoraje financiero electr�nicas, celebradas con los clientes que se describen en el cuadro siguiente, en donde se especifica la fecha de presentaci�n, importe total y nombre de la empresa acreditada.", "formas", ComunesXLS.LEFT, 12);
				
							xlsDoc.setCelda("La informaci�n del documento en "+lsMoneda+" por un importe de $"+ Comunes.formatoDecimal(new Double(lsImporteTotal),2) +" citado, est�n registrados en el sistema denominado Nafin Electr�nico a disposici�n de Nacional Financiera S.N.C. cuando esta los requiera.", "formas", ComunesXLS.LEFT, 12);
							
							xlsDoc.setCelda("Trat�ndose del t�tulo de cr�dito electr�nico, este se encuentra debidamente transmitido en propiedad y con nuestra responsabilidad a favor de Nacional Financiera S.N.C..", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("En el caso del documento electr�nico, en este acto cedemos a dicha instituci�n de Banca de Desarrollo los derechos  de cr�dito que se derivan de los mismos.", "formas", ComunesXLS.LEFT, 12);
				
							xlsDoc.setCelda("En terminos del art�culo 2044 del C�digo Civil para el Distrito Federal, la cesi�n a la que se refiere el p�rrafo anterior, la realizamos con nuestra responsabilidad, ", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("respecto de la solvencia de los emisores, por el tiempo en el que permanezcan vigentes los adeudos y hasta su liquidaci�n final.", "formas", ComunesXLS.LEFT, 12);
				
							xlsDoc.setCelda("As� mismo y en t�rminos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del t�tulo de cr�dito electr�nico y/o ejercer los derechos de cobro consignados en el documento objeto de descuento, ", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("as� como ejecutar todos los actos que sean necesarios para que el documento se�alado conserve su valor y dem�s derechos que les correspondan.", "formas", ComunesXLS.LEFT, 12);
							
						
						} else {
							xlsDoc.setCelda("FORMATO DE CERTIFICADO DE DEPOSITO DE TITULOS DE CREDITO EN ADMINISTRACION", "titulo", ComunesXLS.CENTER, 12);
							xlsDoc.setCelda("Obran en nuestro poder y nos obligamos a conservar en dep�sito en administraci�n a disposici�n de Nacional Financiera S.N.C. el (los) t�tulo (s) de cr�dito por los importes que abajo se detalla (n) suscrito (s) conforme a la Ley General de T�tulos y Operaciones de Cr�dito el (los) que descontamos con Nacional Financiera S.N.C. consider�ndose para todos los efectos legales que somos depositarios de los mismos.", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("Sin cargo alguno para Nacional Financiera S.N.C. quedamos obligados a efectuar el cobro del (de los) referido(s) t�tulo(s) a su vencimiento y a efectuar en su caso los dem�s actos a que se refiere el Art�culo 278 de la Ley General de T�tulos y Operaciones de Cr�dito.", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("El (los) citado (s) t�tulo(s) ha (n) sido endosado (s) en propiedad con nuestra responsabilidad a Nacional Financiera S.N.C. en virtud del descuento del (de los) mismo (s) en los t�rminos del contrato de apertura de cr�dito que con la fecha que a continuaci�n se describe celebramos con dicha Instituci�n.", "formas", ComunesXLS.LEFT, 12);
						}
						xlsDoc.creaFila();
						xlsDoc.setTabla(2, 4);
						xlsDoc.setBorde(true);
						xlsDoc.setCelda("Importe Total", "celda01");
						xlsDoc.setCelda(Comunes.formatoDecimal(new Double(lsImporteTotal),2), "formas", ComunesXLS.RIGHT);
						xlsDoc.setCelda("Fecha de Contrato", "celda01");
						xlsDoc.setCelda(lsFechaContrato);			
						xlsDoc.cierraTabla();
						
						xlsDoc.setTabla(12);
						xlsDoc.setBorde(true);
						xlsDoc.setCelda("Nombre del emisor", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Beneficiario", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Clase de Documento", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Lugar de Emision", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Fecha de Emision", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Domicilio de Pago", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Tasa de Interes", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Numero de Documento", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Fecha Vencimiento", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Valor Nominal", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Numero de Prestamo", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Tipo de Documento", "celda01", ComunesXLS.CENTER);
								
						xlsDoc.setCelda(lsEmisor);
						xlsDoc.setCelda(lsNombre);
						xlsDoc.setCelda("PAGARE");
						xlsDoc.setCelda(lsLugarEmision);
						xlsDoc.setCelda(lsFechaEmision);
						xlsDoc.setCelda(lsDomicilioPago);
						xlsDoc.setCelda(lsTasaIF);
						xlsDoc.setCelda(lsNumDocto);
						xlsDoc.setCelda(lsFechaVenc);
						xlsDoc.setCelda(Comunes.formatoDecimal(new Double(lsImporteTotal),2));
						xlsDoc.setCelda(lsNumPrestamo);
						xlsDoc.setCelda("Certificado");
						
						xlsDoc.setCelda("Total", "celda01", ComunesXLS.CENTER, 9);
						xlsDoc.setCelda(Comunes.formatoDecimal(new Double(lsImporteTotal),2));
						xlsDoc.setCelda("", "celda01", ComunesXLS.LEFT, 2);
									 
						xlsDoc.cierraTabla();
						
						xlsDoc.creaFila();
						xlsDoc.setTabla(3, 5);
						xlsDoc.setBorde(true);
						xlsDoc.setCelda("Amortizacion", "celda01");
						xlsDoc.setCelda("Monto", "celda01");
						xlsDoc.setCelda("Fecha Vencimiento", "celda01");
						
						xlsDoc.setCelda(lsNoAmort);
						xlsDoc.setCelda(Comunes.formatoDecimal(new Double(lsImporteTotal),2));
						xlsDoc.setCelda(lsFechaVenc);
						xlsDoc.cierraTabla();
						
						xlsDoc.creaFila();
						xlsDoc.setTabla(1, 5);			
						xlsDoc.setCelda(lsDomicilioPago, "formast", ComunesXLS.CENTER, 3);
						xlsDoc.setCelda(lsFechaOper, "formast", ComunesXLS.CENTER, 3);
						xlsDoc.setCelda(lsNombre, "formast", ComunesXLS.CENTER, 3);			
						xlsDoc.setCelda(lsPuesto+" "+lsDatos, "formast", ComunesXLS.CENTER, 3);
						
						xlsDoc.cierraTabla();
						
						xlsDoc.creaFila();
						xlsDoc.creaFila();
						xlsDoc.creaFila();
					
						
						
					}else if(tipo.equals("PDF") && accionBoton.equals("Consultar"))  {				
						
						if(numReg==0){
							if ("F".equals(lsTipoOperacion)) {
								pdfDoc.addText("FORMATO DE CONSTANCIA DE DEPOSITO DE DOCUMENTOS DESCONTADOS PARA OPERACIONES DE FACTORAJE ELECTRONICO ","formasrepB",ComunesPDF.CENTER);
							} else {
								pdfDoc.addText("FORMATO DE CERTIFICADO DE DEPOSITO DE \n TITULOS DE CREDITO EN ADMINISTRACI�N  ","formasrepB",ComunesPDF.CENTER);
							} 
						
							pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
				
							if ("F".equals(lsTipoOperacion)) {
								mensaje = " De conformidad con el convenio que "+lsNombre+" y en la fecha que posteriormente se se�ala. celebr� con Nacional Financiera S.N.C. bajo protesta de decir verdad, "+
											 " se hace constar que para todos los efectos legales, nos asumimos como depositarios de los t�tulo(s) de cr�dito y/o documento(s) electr�nico(s) sin derecho a honorarios y asumiendo "+
											 " la responsabilidad civil y penal correspondiente al car�cter de depositario judicial, el(los) titulo(s) de cr�dito y/o documento(s) electr�nico(s) en que se consignan los derechos de "+
											 " cr�dito derivados de las operaciones de factoraje financiero electr�nicas, celebradas con los clientes que se describen en el cuadro siguiente, en donde se especifica la fecha de presentaci�n, "+
											 " importe total y nombre de la empresa acreditada.\n\n"+
											 " La informaci�n del documento en   por un importe de $"+Comunes.formatoDecimal(new Double(lsImporte),2) +" citado, est�n registrados en el sistema denominado Nafin Electr�nico a disposici�n de Nacional Financiera S.N.C.  cuando esta los requiera.\n\n "+
											 " Trat�ndose del t�tulo de cr�dito electr�nico, este se encuentra debidamente transmitido en propiedad y con nuestra responsabilidad a favor de Nacional Financiera S.N.C.. En el caso del documento electr�nico, en este acto cedemos a dicha instituci�n de Banca de Desarrollo los derechos  de cr�dito que se derivan de los mismos\n\n"+
											 " En terminos del art�culo 2044 del C�digo Civil para el Distrito Federal, la cesi�n a la que se refiere el p�rrafo anterior, la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, por el tiempo en el que permanezcan vigentes los adeudos y hasta su liquidaci�n final\n\n  "+
											 " As� mismo y en t�rminos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del t�tulo de cr�dito electr�nico y/o ejercer los derechos de cobro consignados en el documento objeto de descuento, as� como ejecutar todos los actos que sean necesarios para que el documento se�alado conserve su valor y dem�s derechos que les correspondan. ";		
							} else {
								mensaje = " Obran en nuestro poder y nos obligamos a conservar en dep�sito en administraci�n a disposici�n de Nacional Financiera, S.N.C. el (los) t�tulo (s) de cr�dito por los importes que abajo se detalla (n), suscrito (s) conforme a la Ley General de T�tulos y Operaciones de Cr�dito, el (los) que descontamos con Nacional Financiera, S.N.C. consider�ndose para todos los efectos legales que somos depositarios de los mismos.\n\n"+
								"	Sin cargo alguno para Nacional Financiera, S.N.C. quedamos obligados a efectuar el cobro del (de los) referido(s) t�tulo(s) a su vencimiento, y a efectuar, en su caso, los dem�s actos a que se refiere el Art�culo 278 de la Ley General de T�tulos y Operaciones de Cr�dito.\n\n "+
								"	El (los) citado (s) t�tulo(s) ha (n) sido endosado (s) en propiedad con nuestra responsabilidad a Nacional Financiera, S.N.C., en virtud del descuento del (de los) mismo (s) en los t�rminos del contrato de apertura de cr�dito que con la fecha que a continuaci�n se describe celebramos con dicha Instituci�n. ";
							} 
							
							pdfDoc.addText(mensaje,"formasrep",ComunesPDF.JUSTIFIED);
												
							pdfDoc.setTable(1, 50);
							pdfDoc.setCell("Importe Total              $"+Comunes.formatoDecimal(lsImporteTotal,2,true),"formasB", ComunesPDF.CENTER, 1, 1, 0);
							pdfDoc.setCell("Fecha de Contrato          "+lsFechaContrato,"formasB", ComunesPDF.CENTER, 1, 1, 0);
							pdfDoc.addTable();
							
							pdfDoc.setTable(11, 100);
							pdfDoc.setCell("Nombre del Emisor","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Beneficiario","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Clase de Documento","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Lugar de Emisi�n","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Fecha de Emisi�n","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Domicilio de Pago","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Tasa de Interes","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("N�mero de Documento","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Fecha de Vencimiento","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Valor Nominal","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("N�mero de Pr�stamo","celda01rep",ComunesPDF.CENTER); 											
						}	
						
						pdfDoc.setCell(lsEmisor,"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(lsNombre,"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell("PAGARE","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsLugarEmision,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsFechaEmision,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsDomicilioPago,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsTasaIF,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsNumDocto,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsFechaVenc,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(lsImporteTotal,2,true),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell(lsNumPrestamo,"formasrep",ComunesPDF.CENTER);				
						
					}
					
					numReg++;
				}//while
			}
			if(tipo.equals("XLS") && accionBoton.equals("Consultar") )  {
				xlsDoc.cierraXLS();
			}
			if(tipo.equals("PDF") && accionBoton.equals("Consultar"))  {
			
				pdfDoc.setCell("Total","celda01rep",ComunesPDF.RIGHT,9);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(lsImporteTotal,2,true),"formasrep",ComunesPDF.RIGHT);				
				pdfDoc.setCell(" ","formasrep",ComunesPDF.RIGHT);
				pdfDoc.addTable();
				
				pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
				
				pdfDoc.setTable(3, 30);
				pdfDoc.setCell("Amortizaci�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Montor","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento","celda01rep",ComunesPDF.CENTER);				
				pdfDoc.setCell(lsNoAmort,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(lsImporteTotal,2,true),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell(lsFechaVenc,"formasrep",ComunesPDF.CENTER);
				pdfDoc.addTable();
				
				pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(lsDomicilioPago +"  "+lsFechaOper,"formas",ComunesPDF.CENTER);
				
				pdfDoc.endDocument();
			}
			
			/****************ESTA PARTE CORRESPONDE AL BOTON DE Operaciones Confirmadas por IF  **********************/
			
		
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		return "";
	}
	



	public List getConditions() {  return conditions;  }  


	public void setC_numNE(String c_numNE) {
		this.c_numNE = c_numNE;
	}


	public String getC_numNE() {
		return c_numNE;
	}


	public void setC_rfc(String c_rfc) {
		this.c_rfc = c_rfc;
	}


	public String getC_rfc() {
		return c_rfc;
	}


	public void setC_estatus(String c_estatus) {
		this.c_estatus = c_estatus;
	}


	public String getC_estatus() {
		return c_estatus;
	}


	public void setFec_ini(String fec_ini) {
		this.fec_ini = fec_ini;
	}


	public String getFec_ini() {
		return fec_ini;
	}


	public void setFec_fin(String fec_fin) {
		this.fec_fin = fec_fin;
	}


	public String getFec_fin() {
		return fec_fin;
	}


	public void setChkRegErrores(String chkRegErrores) {
		this.chkRegErrores = chkRegErrores;
	}


	public String getChkRegErrores() {
		return chkRegErrores;
	}


	


}