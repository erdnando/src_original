package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Iva;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ComisionRecursosPropios implements IQueryGeneratorRegExtJS   {

	private List conditions;
	StringBuffer strQuery;
	String ic_if,mes_calculo,anio_calculo,perfilRequerido,ic_moneda,ic_banco_fondeo;
	
	boolean variosTiposComision;
	int maximoNumeroRegistros= 1000;
	private static final Log log = ServiceLocator.getInstance().getLog(ComisionRecursosPropios.class);
	
	public ComisionRecursosPropios() {
	}
	public String getAggregateCalculationQuery() {
			String strSQLConsulta;
			String fecha="";
			if(!mes_calculo.equals("13"))
			fecha = "01/"+ mes_calculo + "/" + anio_calculo; 
			else
			fecha = "01/01/" + anio_calculo; 
			double iva = ((BigDecimal)Iva.getValor(fecha)).doubleValue();
			String cadena =(!mes_calculo.equals("13"))?String.valueOf(1 + iva):"1.15";
			strSQLConsulta = 
						" SELECT ROUND(SUM(fg_contraprestacion) * "+cadena+", 2) as GranTotalMontoComision " + 
						" FROM com_comision_fondeo_det " +
						" WHERE ic_if = " + ic_if +
						" 	AND ic_anio_calculo = " + anio_calculo;
						if(!mes_calculo.equals("13")){
							strSQLConsulta+=" 	AND ic_mes_calculo = " + mes_calculo;
						}
						strSQLConsulta+="	AND ic_moneda = " + ic_moneda +
						"	AND ic_banco_fondeo = " + ic_banco_fondeo;
		return strSQLConsulta;
   }
  
   public String getDocumentQuery() {
			return "";
}
  
  public String getDocumentSummaryQueryForIds(List pageIds) {
		
		
		return "";
  }
  
  public String getDocumentQueryFile() {
	
	conditions =new ArrayList();
		log.info("getComisionRecursosPropios(E)");
		AccesoDB	con							= null;
		StringBuffer	qrySentencia  =new StringBuffer();
		StringBuffer	condicion  =new StringBuffer(); 		
		int 			cont						= 0;
		boolean bandera=!mes_calculo.equals("13");
		String camposEPO = null;
		String camposEPOAgrupacion = null;
		//if(!bandera){
		if (variosTiposComision) {	//Si hay varios tipo de comision en la IF, se realiza desglose por EPO
			camposEPO = " e.ic_epo, e.cg_razon_social as NombreEPO ";
			camposEPOAgrupacion = " e.ic_epo, e.cg_razon_social";
		} else {
			//0 es una clave que no existe, lo que permite en este caso es ignorar la epo y hacer solo el desglose por IF-Producto.
			camposEPO = " 0 as ic_epo, '-' as NombreEPO";	
			camposEPOAgrupacion = " 0, '-' ";
		}
		
		
				qrySentencia.append(" SELECT i.cg_razon_social AS NombreIF, cf.ic_producto_nafin, pn.ic_nombre,  ");
				qrySentencia.append(camposEPO + "," );
				qrySentencia.append(" 	cfd.fg_porcentaje_aplicado, cfd.fg_contraprestacion,  cf.ic_mes_calculo , " );
				if(!bandera){
					qrySentencia.append("   pcf.cg_estatus_pagado, " );
				}
				qrySentencia.append(" 	SUM(cf.ig_doctos_vigentes) AS doctosVigentes, " );
				qrySentencia.append(" 	SUM(cf.fg_monto_doctos) AS montoDocumentos  " );
				qrySentencia.append(" FROM  " );
				qrySentencia.append(" 	com_comision_fondeo cf, comcat_producto_nafin pn,  " );
				qrySentencia.append(" 	comcat_if i, comcat_epo e, com_comision_fondeo_det cfd  " );
				if(!bandera){
									qrySentencia.append(", 		com_pago_comision_fondeo pcf   " );
				}
				qrySentencia.append(" WHERE cf.ic_producto_nafin = pn.ic_producto_nafin  " );
				qrySentencia.append(" 	AND cf.ic_if = i.ic_if  " );
				qrySentencia.append(" 	AND cf.ic_epo = e.ic_epo  " );
				qrySentencia.append(" 	AND cf.ic_producto_nafin = cfd.ic_producto_nafin " );
				qrySentencia.append(" 	AND cf.ic_if = cfd.ic_if " );
				qrySentencia.append(" 	AND cf.ic_mes_calculo = cfd.ic_mes_calculo " );
				qrySentencia.append(" 	AND cf.ic_anio_calculo = cfd.ic_anio_calculo " );
				qrySentencia.append(" 	AND cf.cg_tipo_comision = cfd.cg_tipo_comision " );
				qrySentencia.append(" 	AND cf.ic_if = " + ic_if );
				if(bandera){
					qrySentencia.append(" 	AND cf.ic_mes_calculo = " + mes_calculo );
				}else{
					qrySentencia.append(" AND cf.ic_producto_nafin = pcf.ic_producto_nafin	 " );
					qrySentencia.append(" AND cfd.ic_moneda = pcf.ic_moneda	 " );
					qrySentencia.append(" AND cfd.ic_banco_fondeo = pcf.ic_banco_fondeo	 " );
					qrySentencia.append(" AND cf.ic_if = pcf.ic_if	 " );
					qrySentencia.append(" AND cf.ic_mes_calculo = pcf.ic_mes_calculo	 " );
					qrySentencia.append(" AND cf.ic_anio_calculo = pcf.ic_anio_calculo	 " );


				}
				qrySentencia.append(" 	AND cf.ic_anio_calculo =  " + anio_calculo );
				qrySentencia.append("	AND cfd.ic_moneda = cf.ic_moneda " );
				qrySentencia.append("	AND cfd.ic_banco_fondeo = cf.ic_banco_fondeo " );
				qrySentencia.append("	AND cfd.ic_banco_fondeo = " + ic_banco_fondeo );
				qrySentencia.append("	AND cfd.ic_moneda = " + ic_moneda );
				//"	AND cfd.ic_moneda = 1 " +
				qrySentencia.append(" 	GROUP BY i.cg_razon_social, cf.ic_producto_nafin, pn.ic_nombre, cf.ic_mes_calculo, " );
				qrySentencia.append(camposEPOAgrupacion + "," );
				if(!bandera){
					qrySentencia.append("  pcf.cg_estatus_pagado, 	  " );
				}

				qrySentencia.append(" 	cfd.fg_porcentaje_aplicado, cfd.fg_contraprestacion  " );
				qrySentencia.append(" ORDER BY  cf.ic_producto_nafin, nombreEPO ");
				if(!bandera){
					qrySentencia.append(", cf.ic_mes_calculo" );
				}
		/*}else{
			qrySentencia.append("SELECT  i.cg_razon_social AS NombreIF cfd.cg_tipo_comision AS comision, i.cg_razon_social AS nombreif, "+
         "cf.ic_producto_nafin, pn.ic_nombre, 0 AS ic_epo, '-' AS nombreepo, "+
         "cfd.fg_porcentaje_aplicado, cfd.fg_contraprestacion, "+
         "SUM (cf.ig_doctos_vigentes) AS doctosvigentes, "+
         "SUM (cf.fg_monto_doctos) AS montodocumentos "+
    "FROM com_comision_fondeo cf, "+
     "    comcat_producto_nafin pn, "+
      "   comcat_if i, "+
       "  comcat_epo e, "+
        " com_comision_fondeo_det cfd "+
   "WHERE cf.ic_producto_nafin = pn.ic_producto_nafin "+
    " AND cf.ic_if = i.ic_if "+
     "AND cf.ic_epo = e.ic_epo "+
    " AND cf.ic_producto_nafin = cfd.ic_producto_nafin "+
    " AND cf.ic_if = cfd.ic_if "+
    " AND cf.ic_mes_calculo = cfd.ic_mes_calculo "+
    " AND cf.ic_anio_calculo = cfd.ic_anio_calculo "+
    " AND cf.cg_tipo_comision = cfd.cg_tipo_comision "+
    " AND cf.ic_if =  "+ic_if+
    " AND cf.ic_mes_calculo =  "+mes_calculo+
    " AND cf.ic_anio_calculo =  "+anio_calculo+
    " AND cfd.ic_moneda = cf.ic_moneda "+
    " AND cfd.ic_banco_fondeo = cf.ic_banco_fondeo "+
    " AND cfd.ic_banco_fondeo =  "+ ic_banco_fondeo+
    " AND cfd.ic_moneda =  "+ic_moneda+
" GROUP BY cfd.cg_tipo_comision, "+
 "        i.cg_razon_social, "+
  "       cf.ic_producto_nafin, "+
   "      pn.ic_nombre, "+
    "     0, "+
     "    '-', "+
     "    cfd.fg_porcentaje_aplicado, "+
      "   cfd.fg_contraprestacion "+
" ORDER BY nombreepo, cf.ic_producto_nafin");
		
		}*/
		
				log.debug(" qrySentencia "+qrySentencia.toString());
				return qrySentencia.toString();
  }
  
  
  /**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)
	{
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
      
		try {
		  if(tipo.equals("PDF")){
		  HttpSession session = request.getSession();
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		  ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
		  String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		  String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		  String diaActual = fechaActual.substring(0,2);
		  String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		  String anioActual = fechaActual.substring(6,10);
		  String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
		  pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		  session.getAttribute("iNoNafinElectronico").toString(),
		  (String)session.getAttribute("sesExterno"),
		  (String)session.getAttribute("strNombre"),
		  (String)session.getAttribute("strNombreUsuario"),
		  (String)session.getAttribute("strLogo"),
		  (String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
		  pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
		  pdfDoc.setTable(7,100);
		  pdfDoc.setCell("Mes", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Num. Doctos.", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Moneda", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Monto de Documentos", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Porcentaje comisi�n", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Monto Comisi�n", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Pagado", "celda01",ComunesPDF.CENTER);
		  
		  while (rs.next()) {
				String mes = (rs.getString("ic_mes_calculo") == null) ? "" : rs.getString("ic_mes_calculo");
				String numDoctos = (rs.getString("doctosVigentes") == null) ? "" : rs.getString("doctosVigentes");
				String moneda = (rs.getString("rfc") == null) ? "" : rs.getString("rfc");
				String montoDoctos = (rs.getString("domicilio") == null) ? "" : rs.getString("domicilio");
				String porcentajeCom = (rs.getString("contacto") == null) ? "" : rs.getString("contacto");
				String montoCom = (rs.getString("telefono") == null) ? "" : rs.getString("telefono");
				String pagado = (rs.getString("fecha") == null) ? "" : rs.getString("fecha");
				
				pdfDoc.setCell(mes,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numDoctos,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(montoDoctos,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(porcentajeCom,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(montoCom,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(pagado,"formas",ComunesPDF.CENTER);
			}
		  pdfDoc.addTable();
		  pdfDoc.endDocument();
		 
		 }
		}
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
   }
	
	
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}


	public void setStrQuery(StringBuffer strQuery) {
		this.strQuery = strQuery;
	}


	public StringBuffer getStrQuery() {
		return strQuery;
	}



	public void setMaximoNumeroRegistros(int maximoNumeroRegistros) {
		this.maximoNumeroRegistros = maximoNumeroRegistros;
	}


	public int getMaximoNumeroRegistros() {
		return maximoNumeroRegistros;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public String getIc_if() {
		return ic_if;
	}


	public void setMes_calculo(String mes_calculo) {
		this.mes_calculo = mes_calculo;
	}


	public String getMes_calculo() {
		return mes_calculo;
	}


	public void setAnio_calculo(String anio_calculo) {
		this.anio_calculo = anio_calculo;
	}


	public String getAnio_calculo() {
		return anio_calculo;
	}


	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}


	public String getIc_moneda() {
		return ic_moneda;
	}


	public void setVariosTiposComision(boolean variosTiposComision) {
		this.variosTiposComision = variosTiposComision;
	}


	public boolean isVariosTiposComision() {
		return variosTiposComision;
	}


	public void setIc_banco_fondeo(String ic_banco_fondeo) {
		this.ic_banco_fondeo = ic_banco_fondeo;
	}


	public String getIc_banco_fondeo() {
		return ic_banco_fondeo;
	}
}