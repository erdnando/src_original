 
package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class BitacoraCambiosCargaMasiva implements IQueryGeneratorRegExtJS {

  private String fechaFin 	="";
  private String fechaIni 	="";
  private String claveEpo 	="";
  private List   conditions;
  private StringBuffer strQuery = new StringBuffer();
  private final static Log log  = ServiceLocator.getInstance().getLog(BitacoraCambiosCargaMasiva.class); 

 /**
  * Constructor by Default
  */
  public BitacoraCambiosCargaMasiva()  {  }

  /**
	 * Este m�todo debe regresar un query con el que se obtienen los
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQuery(){
	log.debug("getDocumentQuery (E) ");
	StringBuffer strQuery 	= new StringBuffer();
	conditions = new ArrayList();
		strQuery.append(
			"SELECT   bc.ic_epo"+
			   " FROM bit_carga_masiva_prov bc, comcat_epo ce "+
			   "WHERE bc.ic_epo = ce.ic_epo "+
			    " AND bc.df_carga >= TRUNC (TO_DATE ('"+fechaIni+"', 'dd/mm/yyyy'))"+
			    " AND bc.df_carga < TRUNC (TO_DATE ('"+fechaFin+"', 'dd/mm/yyyy') + 1)" );
		if(!claveEpo.equals("")){
			strQuery.append(" AND bc.ic_epo = " + claveEpo );
		}
		strQuery.append( " ORDER BY bc.ic_bit_carga" );
	
	log.debug(" strQuery identificadores unicos - - ->"+ strQuery);
	log.debug("getDocumentQuery (S) ");
		
	return strQuery.toString();
  }


	/**
	 * Este m�todo debe regresar un query con el que se obtienen los
	 * datos a mostrar en la consulta basandose en los identificadores unicos
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentSummaryQueryForIds(List ids){
	 strQuery   = new StringBuffer(""); 
		conditions = new ArrayList();
		log.info("getDocumentSummaryQueryForIds(E)");
		strQuery.append(
			"SELECT   bc.ic_bit_carga, bc.ic_epo, bc.cg_nombre_arch, bc.ig_total_reg,"+
						"bc.ig_reg_proc, bc.ig_reg_error,"+
						"TO_CHAR (bc.df_carga, 'dd/mm/yyyy hh24:mi:ss') AS df_carga, bc.ic_usuario,"+
						"bc.cg_nombre_usuario, ce.cg_razon_social AS nombre_epo "+
			   " FROM bit_carga_masiva_prov bc, comcat_epo ce "+
			   "WHERE bc.ic_epo = ce.ic_epo "+
			    " AND bc.df_carga >= TRUNC (TO_DATE ('"+fechaIni+"', 'dd/mm/yyyy'))"+
			    " AND bc.df_carga < TRUNC (TO_DATE ('"+fechaFin+"', 'dd/mm/yyyy') + 1)" );
		if(!claveEpo.equals("")){
			strQuery.append(" AND bc.ic_epo = " + claveEpo );
		}
		strQuery.append( " ORDER BY bc.ic_bit_carga" );
		
		log.debug("getDocumentSummaryQueryForIds: "+strQuery.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
		return strQuery.toString();
  }


	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getAggregateCalculationQuery(){
  return "";
  }


	/**
	 * Este m�todo debe regresar una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
	public List getConditions(){
  return conditions;
  }


	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQueryFile(){
		strQuery   = new StringBuffer(""); 
		conditions = new ArrayList();
		log.info("getDocumentQueryFile(E)");
		strQuery.append(
			"SELECT   bc.ic_bit_carga, bc.ic_epo, bc.cg_nombre_arch, bc.ig_total_reg,"+
						"bc.ig_reg_proc, bc.ig_reg_error,"+
						"TO_CHAR (bc.df_carga, 'dd/mm/yyyy hh24:mi:ss') AS df_carga, bc.ic_usuario,"+
						"bc.cg_nombre_usuario, ce.cg_razon_social AS nombre_epo "+
			   " FROM bit_carga_masiva_prov bc, comcat_epo ce "+
			   "WHERE bc.ic_epo = ce.ic_epo "+
			    " AND bc.df_carga >= TRUNC (TO_DATE ('"+fechaIni+"', 'dd/mm/yyyy'))"+
			    " AND bc.df_carga < TRUNC (TO_DATE ('"+fechaFin+"', 'dd/mm/yyyy') + 1)" );
		if(!claveEpo.equals("")){
			strQuery.append(" AND bc.ic_epo = " + claveEpo );
		}
		strQuery.append( " ORDER BY bc.ic_bit_carga" );
		
		log.debug("getDocumentQueryFile: "+strQuery.toString());
		log.info("getDocumentQueryFile(S)");
		return strQuery.toString();
  }


	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo){
	log.debug("crearCustomFile (E)");
	String nombreArchivo ="";
	HttpSession session = request.getSession();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	OutputStreamWriter writer = null;
	BufferedWriter buffer = null;
	int total = 0;
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		buffer = new BufferedWriter(writer);
			
		contenidoArchivo = new StringBuffer();	
		contenidoArchivo.append( "EPO, Fecha de Carga de Archivo,Nombre Archivo,Usuario Responsable, Total de Registros, Registros Procesados,Registros Inv�lidos\n");
			
		while (rs.next())	{
			String nombreif 	= (rs.getString("NOMBRE_EPO") 						== null) ? "" : rs.getString("NOMBRE_EPO");
			String codbasop 	= (rs.getString("DF_CARGA") 	== null) ? "" : rs.getString("DF_CARGA");
			String unodes 		= (rs.getString("CG_NOMBRE_ARCH") 					== null) ? "" : rs.getString("CG_NOMBRE_ARCH");				
			String codproban	= (rs.getString("CG_NOMBRE_USUARIO") 	== null) ? "" : rs.getString("CG_NOMBRE_USUARIO");
			String desdos	 	= (rs.getString("IG_TOTAL_REG") 					== null) ? "" : rs.getString("IG_TOTAL_REG");				
			String diasmin		= (rs.getString("IG_REG_PROC")== null) ? "" : rs.getString("IG_REG_PROC");
			String opfac	 	= (rs.getString("IG_REG_ERROR") 		== null) ? "" : rs.getString("IG_REG_ERROR");
			
			contenidoArchivo.append(
			nombreif.replace	(',',' ')+","+
			codbasop.replace	(',',' ')+","+		
			unodes.replace		(',',' ')+","+
			codproban.replace	(',',' ')+","+
			desdos.replace		(',',' ')+","+
			diasmin.replace	(',',' ')+","+
			opfac.replace		(',',' ')+","+ "\n");
		}
			
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
		
		}catch (Exception e){
			throw new AppException("Error al generar el archivo ", e);
		}
		log.debug("crearCustomFile (S)");
	return nombreArchivo;
  }


	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo){
	String nombreArchivo = "";
	HttpSession session = request.getSession();	
	ComunesPDF pdfDoc = new ComunesPDF();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
		pdfDoc.setTable(7,90);
			
		pdfDoc.setCell("EPO"										,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Carga de Archivo"		,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre Archivo"						,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Usuario responsable"				,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Total de Registros"					,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Registros Procesados"				,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Registros inv�lidos"				,"celda01",ComunesPDF.CENTER);

			
		while(reg.next()){
			String nombreif 	= (reg.getString("NOMBRE_EPO") 						== null) ? "" : reg.getString("NOMBRE_EPO");
			String codbasop 	= (reg.getString("DF_CARGA") 	== null) ? "" : reg.getString("DF_CARGA");
			String unodes 		= (reg.getString("CG_NOMBRE_ARCH") 					== null) ? "" : reg.getString("CG_NOMBRE_ARCH");				
			String codproban	= (reg.getString("CG_NOMBRE_USUARIO") 	== null) ? "" : reg.getString("CG_NOMBRE_USUARIO");
			String desdos	 	= (reg.getString("IG_TOTAL_REG") 					== null) ? "" : reg.getString("IG_TOTAL_REG");				
			String diasmin		= (reg.getString("IG_REG_PROC")== null) ? "" : reg.getString("IG_REG_PROC");
			String opfac	 	= (reg.getString("IG_REG_ERROR") 		== null) ? "" : reg.getString("IG_REG_ERROR");

			pdfDoc.setCell(nombreif		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(codbasop	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(unodes		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(codproban	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(desdos		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(diasmin		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(opfac		,"formas",ComunesPDF.CENTER);

		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
	} catch(Exception e){
		throw new AppException("Error al generar el archivo PDF ", e);
	}
	return nombreArchivo;
  }
 

	/**
	 * @Setters_&_Getters_Methods...
	 */


	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}


	public String getFechaFin() {
		return fechaFin;
	}


	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}


	public String getFechaIni() {
		return fechaIni;
	}


	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}

}