package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsEstatusAfiliacionEcontract implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsEstatusAfiliacionEcontract.class);
  /**
   * Constructor de la clase.
   */
	public ConsEstatusAfiliacionEcontract(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 	strSQL;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
  
  private String bancoFondeo;
	private String claveEpo;
  private String claveIf;
  private String nafinElectronicoPyme;
  private String numeroProveedor;
  
	private String operacion;
	private String orden;
	
	
		/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)  {
			String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
				
				//Encabezado
				linea="NOMBRE EPO,NOMBRE IF,NUMERO DE PROVEEDOR,NAFIN ELECTRONICO,NUMERO SIRAC,NOMBRE PYME,"+
				"ESTATUS AFILIACION,NOMBRE PROCESO,CAUSA RECHAZO,NOTA,NOTA IF\n";
				buffer.write(linea);
				
				String epo,nomIF,numProv,nafEle,sirac,pyme,estatus,proceso,causa,nota,notaIF;
				while (rs.next()) {
					
					 epo= (rs.getString("nombre_epo") == null) ? "" : rs.getString("nombre_epo");
					 nomIF=(rs.getString("nombre_if") == null) ? "" : rs.getString("nombre_if");
					 numProv=(rs.getString("numero_proveedor") == null) ? "" : rs.getString("numero_proveedor");
					 nafEle=(rs.getString("nafin_electronico") == null) ? "" : rs.getString("nafin_electronico");
					 sirac=(rs.getString("numero_sirac") == null) ? "" : rs.getString("numero_sirac");
					 pyme=(rs.getString("nombre_pyme") == null) ? "" : rs.getString("estatus_afiliacion");
					 estatus=(rs.getString("estatus_afiliacion") == null) ? "" : rs.getString("nombre_pyme");
					 proceso=(rs.getString("nombre_proceso") == null) ? "" : rs.getString("nombre_proceso");
					 causa=(rs.getString("causa_rechazo") == null) ? "" : rs.getString("causa_rechazo");
					 nota=(rs.getString("nota") == null) ? "" : rs.getString("nota");
					 notaIF=(rs.getString("nota_if") == null) ? "" : rs.getString("nota_if");

					linea =epo+","+nomIF.replace(',',' ')+","+numProv+","+nafEle.replace(',',' ')+","+sirac.replace(',',' ')+","
					+pyme.replace(',',' ')+","+estatus.replace(',',' ')+","+proceso.replace(',',' ')+","+causa.replace(',',' ').replaceAll("[\n\r]","")
					+","+nota.replace(',',' ').replaceAll("[\n\r]","")+","+notaIF.replace(',',' ').replaceAll("[\n\r]","")+"\n";
					
					buffer.write(linea);
				}
				buffer.close();
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
	
		return nombreArchivo;
	}
	
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
			if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				"",
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				//pdfDoc.setTable(16, 100);
				List lEncabezados = new ArrayList();
				
			
				
				lEncabezados.add("NOMBRE EPO");
				lEncabezados.add("NOMBRE IF");
				lEncabezados.add("NUMERO DE PROVEEDOR");
				lEncabezados.add("NAFIN ELECTRONICO");
				lEncabezados.add("NUMERO SIRAC");
				lEncabezados.add("NOMBRE PYME");
				lEncabezados.add("ESTATUS AFILIACION");
				lEncabezados.add("NOMBRE PROCESO");
				lEncabezados.add("CAUSA RECHAZO");
				lEncabezados.add("NOTA");
				pdfDoc.setTable(lEncabezados, "formasmenB", 100);
				
				String epo,nomIF,numProv,nafEle,sirac,pyme,estatus,proceso,causa,nota,notaIF;
				while (reg.next()){
					epo= (reg.getString("NOMBRE_EPO") == null) ? "" : reg.getString("nombre_epo");
					nomIF=(reg.getString("NOMBRE_IF") == null) ? "" : reg.getString("NOMBRE_IF");
					numProv=(reg.getString("NUMERO_PROVEEDOR") == null) ? "" : reg.getString("NUMERO_PROVEEDOR");
					nafEle=(reg.getString("NAFIN_ELECTRONICO") == null) ? "" : reg.getString("NAFIN_ELECTRONICO");
					sirac=(reg.getString("NUMERO_SIRAC") == null) ? "" : reg.getString("NUMERO_SIRAC");
					pyme=(reg.getString("NOMBRE_PYME") == null) ? "" : reg.getString("NOMBRE_PYME");
					estatus=(reg.getString("ESTATUS_AFILIACION") == null) ? "" : reg.getString("ESTATUS_AFILIACION");
					proceso=(reg.getString("NOMBRE_PROCESO") == null) ? "" : reg.getString("NOMBRE_PROCESO");
					causa=(reg.getString("CAUSA_RECHAZO") == null) ? "" : reg.getString("CAUSA_RECHAZO");
					nota=(reg.getString("NOTA_IF") == null) ? "" : reg.getString("NOTA_IF");
					
					
					pdfDoc.setCell(epo,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(nomIF,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(numProv,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nafEle,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(pyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(proceso,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(causa,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(nota,"formas",ComunesPDF.LEFT);
					
				}
				//pdfDoc.addTable();
				
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		}	
			return  nombreArchivo;
	}
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery(){
    log.info("getAggregateCalculationQuery(I) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		log.info("getAggregateCalculationQuery(F)::..");
		return strSQL.toString();
	}//getAggregateCalculationQuery
	
    /**
    * Obtiene el query para obtener las llaves primarias de la consulta
    * @return Cadena con la consulta de SQL, para obtener llaves primarias
    */
    public String getDocumentQuery(){
      log.info("getDocumentQuery(I) ::..");
      strSQL = new StringBuffer();
      conditions = new ArrayList();

      strSQL.append(" SELECT fact.econ_ope_factoraje_pk, bfact.econ_ope_bit_factoraje_pk");
      strSQL.append(" FROM econ_ope_factoraje fact");
      strSQL.append(", econ_ope_bit_factoraje bfact");
      strSQL.append(", econ_ope_pymes pym");
      if(bancoFondeo != null && !bancoFondeo.equals("")){
        strSQL.append(", comcat_epo epo");
      }
      strSQL.append(" WHERE bfact.econ_ope_factoraje_fk = fact.econ_ope_factoraje_pk");
      strSQL.append(" AND fact.econ_ope_pymes_fk = pym.econ_ope_pymes_pk");
      strSQL.append(" AND bfact.econ_ope_bit_factoraje_pk IN (");
      strSQL.append(" SELECT MAX (bit.econ_ope_bit_factoraje_pk)");
      strSQL.append(" FROM econ_ope_bit_factoraje bit");
      strSQL.append(" WHERE bit.econ_ope_factoraje_fk = fact.econ_ope_factoraje_pk");
      strSQL.append(" AND bit.cregistro_activo_ck = bfact.cregistro_activo_ck)");
      strSQL.append(" AND fact.econ_cat_productos_fk <> ?");
      strSQL.append(" AND fact.cregistro_activo_ck = ?");
      strSQL.append(" AND bfact.cregistro_activo_ck = ?");
      strSQL.append(" AND bfact.econ_sys_proceso_fk <> ?");
      strSQL.append(" AND fact.dingreso < SYSDATE");
      strSQL.append(" AND fact.econ_cat_productos_fk IN (?, ?, ?)");
      strSQL.append(" AND fact.econ_cat_promotoria_fk = ?");

      conditions.add(new Integer(19));
      conditions.add("S");
      conditions.add("S");
      conditions.add(new Integer(29));
      conditions.add(new Integer(1));         //Factoraje
      conditions.add(new Integer(10));        //Reafiliaci�n
      conditions.add(new Integer(11));        //Reafiliaci�n IF
      conditions.add(new Integer(8281));      //Promotoria: ECON_ORIGEN_BANCO
      //Banco de Fondeo
      if(bancoFondeo != null && !bancoFondeo.equals("")){
        strSQL.append(" AND fact.econ_cat_epos_fk = epo.ic_epo");
        strSQL.append(" AND epo.ic_banco_fondeo = ?");
        conditions.add(new Integer(bancoFondeo));
      }
      //EPO
      if(claveEpo != null && !claveEpo.equals("")){
        strSQL.append(" AND fact.econ_cat_epos_fk = ?");
        conditions.add(new Long(claveEpo));
      }
      //IF
      if(claveIf != null && !claveIf.equals("")){
        strSQL.append(" AND fact.econ_cat_int_financieros_fk IN (");
        strSQL.append(" SELECT econ_cat_int_financieros_pk");
        strSQL.append(" FROM comcat_if cif");
        strSQL.append(" , econ_cat_int_financieros ecif");
        strSQL.append(" WHERE cif.cg_rfc = ecif.vrfc");
        strSQL.append(" AND cif.ic_if = ?");
        strSQL.append(" AND ecif.cregistro_activo_ck = ?)");
        //strSQL.append(" AND fact.econ_cat_int_financieros_fk = ?");
        conditions.add(new Long(claveIf));
        conditions.add("S");
      }
      //Nafin Electr�nico Pyme
      if(nafinElectronicoPyme != null && !nafinElectronicoPyme.equals("")){
        strSQL.append(" AND fact.econ_ope_pymes_fk IN (");
        strSQL.append(" SELECT econ_ope_pymes_pk");
        strSQL.append(" FROM comrel_nafin naf");
        strSQL.append(" , comcat_pyme pyme");
        strSQL.append(" , econ_ope_pymes epyme");
        strSQL.append(" WHERE naf.ic_epo_pyme_if = pyme.ic_pyme");
        strSQL.append(" AND pyme.cg_rfc = epyme.vrfc");
        strSQL.append(" AND epyme.cregistro_activo_ck = ?");
        strSQL.append(" AND naf.cg_tipo = ?");
        strSQL.append(" AND naf.ic_nafin_electronico = ?)");
        //strSQL.append(" AND pym.nnafin_electronico = ?");
        conditions.add("S");
        conditions.add("P");
        conditions.add(new Long(nafinElectronicoPyme));
      }
      //N�mero de Proveedor
      if(numeroProveedor != null && !numeroProveedor.equals("")){
        strSQL.append(" AND fact.vnumero_proveedor_epo = ?");
        conditions.add(numeroProveedor.trim());
      }
      
      strSQL.append(" ORDER BY pym.vrazon_social, fact.econ_cat_epos_fk");

      log.debug("strSQL:: " + strSQL.toString());
      log.debug("conditions:: " + conditions);
      
      log.info("getDocumentQuery(F) ::..");
      return strSQL.toString();
    }//getDocumentQuery
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
    log.info("getDocumentSummaryQueryForIds(I) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
    
    strSQL.append(" SELECT epos.vrazon_social nombre_epo,");
    strSQL.append(" intfin.vdescripcion nombre_if,");
    strSQL.append(" fact.vnumero_proveedor_epo numero_proveedor,");
    strSQL.append(" pym.nnafin_electronico nafin_electronico,");
    strSQL.append(" pym.nnum_sirac numero_sirac,");
    strSQL.append(" pym.vrazon_social nombre_pyme,");
    strSQL.append(" DECODE (bfact.cestatus_ck, 'P', 'EN PROCESO', 'A', 'ACEPTADO', 'R', 'RECHAZADO', bfact.cestatus_ck) estatus_afiliacion,");
    strSQL.append(" pro.vdescripcion nombre_proceso,");
    strSQL.append(" bfact.vcausa_rechazo causa_rechazo,");
    strSQL.append(" bfact.vnota_proceso nota,");
    strSQL.append(" bfact.vnotas_if nota_if");
    strSQL.append(" FROM econ_ope_factoraje fact,");
    strSQL.append(" econ_ope_bit_factoraje bfact,");
    strSQL.append(" econ_ope_pymes pym,");
    strSQL.append(" econ_cat_int_financieros intfin,");
    strSQL.append(" econ_cat_epos epos,");
    strSQL.append(" econ_sys_proceso pro");
    strSQL.append(" WHERE fact.econ_ope_pymes_fk = pym.econ_ope_pymes_pk");
    strSQL.append(" AND fact.econ_cat_epos_fk = epos.econ_cat_epos_pk");
    strSQL.append(" AND fact.econ_cat_int_financieros_fk = intfin.econ_cat_int_financieros_pk");
    strSQL.append(" AND bfact.econ_sys_proceso_fk = pro.econ_sys_proceso_pk");
    strSQL.append(" AND bfact.econ_ope_factoraje_fk = fact.econ_ope_factoraje_pk");
    strSQL.append(" AND bfact.econ_ope_bit_factoraje_pk IN (");
    strSQL.append(" SELECT MAX (bit.econ_ope_bit_factoraje_pk)");
    strSQL.append(" FROM econ_ope_bit_factoraje bit");
    strSQL.append(" WHERE bit.econ_ope_factoraje_fk = fact.econ_ope_factoraje_pk");
    strSQL.append(" AND bit.cregistro_activo_ck = bfact.cregistro_activo_ck)");
    strSQL.append(" AND fact.econ_cat_productos_fk <> ?");
    strSQL.append(" AND fact.cregistro_activo_ck = ?");
    strSQL.append(" AND bfact.cregistro_activo_ck = ?");
    strSQL.append(" AND bfact.econ_sys_proceso_fk <> ?");
    
    conditions.add(new Integer(19));
    conditions.add("S");
    conditions.add("S");
    conditions.add(new Integer(29));

    strSQL.append(" AND (");
    
		for(int i = 0; i < pageIds.size(); i++){
      List lItem = (ArrayList)pageIds.get(i);
			
      if(i > 0){strSQL.append("  OR  ");}
      
      strSQL.append(" (fact.econ_ope_factoraje_pk = ? AND bfact.econ_ope_bit_factoraje_pk = ?)");
      conditions.add(new Long(lItem.get(0).toString()));
      conditions.add(new Long(lItem.get(1).toString()));
		}
    
    strSQL.append(" ) ");
    
    strSQL.append(" ORDER BY pym.vrazon_social, fact.econ_cat_epos_fk");

		log.debug("strSQL:: " + strSQL.toString());
		log.debug("conditions:: " + conditions);
		
		log.info("getDocumentSummaryQueryForIds(F) ::..");
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
    log.info("getDocumentQueryFile(I) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

    strSQL.append(" SELECT epos.vrazon_social nombre_epo,");
    strSQL.append(" intfin.vdescripcion nombre_if,");
    strSQL.append(" fact.vnumero_proveedor_epo numero_proveedor,");
    strSQL.append(" pym.nnafin_electronico nafin_electronico,");
    strSQL.append(" pym.nnum_sirac numero_sirac,");
    strSQL.append(" pym.vrazon_social nombre_pyme,");
    strSQL.append(" DECODE (bfact.cestatus_ck, 'P', 'EN PROCESO', 'A', 'ACEPTADO', 'R', 'RECHAZADO', bfact.cestatus_ck) estatus_afiliacion,");
    strSQL.append(" pro.vdescripcion nombre_proceso,");
    strSQL.append(" bfact.vcausa_rechazo causa_rechazo,");
    strSQL.append(" bfact.vnota_proceso nota,");
    strSQL.append(" bfact.vnotas_if nota_if");
    strSQL.append(" FROM econ_ope_factoraje fact");
    strSQL.append(", econ_ope_bit_factoraje bfact");
    strSQL.append(", econ_ope_pymes pym");
    strSQL.append(", econ_cat_int_financieros intfin");
    strSQL.append(", econ_cat_epos epos");
    strSQL.append(", econ_sys_proceso pro");
    if(bancoFondeo != null && !bancoFondeo.equals("")){
      strSQL.append(", comcat_epo epo");
    }
    strSQL.append(" WHERE fact.econ_ope_pymes_fk = pym.econ_ope_pymes_pk");
    strSQL.append(" AND fact.econ_cat_epos_fk = epos.econ_cat_epos_pk");
    strSQL.append(" AND fact.econ_cat_int_financieros_fk = intfin.econ_cat_int_financieros_pk");
    strSQL.append(" AND bfact.econ_sys_proceso_fk = pro.econ_sys_proceso_pk");
    strSQL.append(" AND bfact.econ_ope_factoraje_fk = fact.econ_ope_factoraje_pk");
    strSQL.append(" AND bfact.econ_ope_bit_factoraje_pk IN (");
    strSQL.append(" SELECT MAX (bit.econ_ope_bit_factoraje_pk)");
    strSQL.append(" FROM econ_ope_bit_factoraje bit");
    strSQL.append(" WHERE bit.econ_ope_factoraje_fk = fact.econ_ope_factoraje_pk");
    strSQL.append(" AND bit.cregistro_activo_ck = bfact.cregistro_activo_ck)");
    strSQL.append(" AND fact.econ_cat_productos_fk <> ?");
    strSQL.append(" AND fact.cregistro_activo_ck = ?");
    strSQL.append(" AND bfact.cregistro_activo_ck = ?");
    strSQL.append(" AND bfact.econ_sys_proceso_fk <> ?");
    strSQL.append(" AND fact.econ_cat_productos_fk IN (?, ?, ?)");
    strSQL.append(" AND fact.econ_cat_promotoria_fk = ?");
    
    conditions.add(new Integer(19));
    conditions.add("S");
    conditions.add("S");
    conditions.add(new Integer(29));
    conditions.add(new Integer(1));         //Factoraje
    conditions.add(new Integer(10));        //Reafiliaci�n
    conditions.add(new Integer(11));        //Reafiliaci�n IF
    conditions.add(new Integer(8281));      //Promotoria: ECON_ORIGEN_BANCO
    //Banco de Fondeo
    if(bancoFondeo != null && !bancoFondeo.equals("")){
      strSQL.append(" AND fact.econ_cat_epos_fk = epo.ic_epo");
      strSQL.append(" AND epo.ic_banco_fondeo = ?");
      conditions.add(new Integer(bancoFondeo));
    }
    //EPO
    if(claveEpo != null && !claveEpo.equals("")){
      strSQL.append(" AND fact.econ_cat_epos_fk = ?");
      conditions.add(new Long(claveEpo));
    }
    //IF
    if(claveIf != null && !claveIf.equals("")){
      strSQL.append(" AND fact.econ_cat_int_financieros_fk IN (");
      strSQL.append(" SELECT econ_cat_int_financieros_pk");
      strSQL.append(" FROM comcat_if cif");
      strSQL.append(" , econ_cat_int_financieros ecif");
      strSQL.append(" WHERE cif.cg_rfc = ecif.vrfc");
      strSQL.append(" AND cif.ic_if = ?");
      strSQL.append(" AND ecif.cregistro_activo_ck = ?)");
      //strSQL.append(" AND fact.econ_cat_int_financieros_fk = ?");
      conditions.add(new Long(claveIf));
      conditions.add("S");
    }
    //Nafin Electr�nico Pyme
    if(nafinElectronicoPyme != null && !nafinElectronicoPyme.equals("")){
      strSQL.append(" AND fact.econ_ope_pymes_fk IN (");
      strSQL.append(" SELECT econ_ope_pymes_pk");
      strSQL.append(" FROM comrel_nafin naf");
      strSQL.append(" , comcat_pyme pyme");
      strSQL.append(" , econ_ope_pymes epyme");
      strSQL.append(" WHERE naf.ic_epo_pyme_if = pyme.ic_pyme");
      strSQL.append(" AND pyme.cg_rfc = epyme.vrfc");
      strSQL.append(" AND epyme.cregistro_activo_ck = ?");
      strSQL.append(" AND naf.cg_tipo = ?");
      strSQL.append(" AND naf.ic_nafin_electronico = ?)");
      //strSQL.append(" AND pym.nnafin_electronico = ?");
      conditions.add("S");
      conditions.add("P");
      conditions.add(new Long(nafinElectronicoPyme));
    }
    //N�mero de Proveedor
    if(numeroProveedor != null && !numeroProveedor.equals("")){
      strSQL.append(" AND fact.vnumero_proveedor_epo = ?");
      conditions.add(numeroProveedor.trim());
    }
    
    strSQL.append(" ORDER BY pym.vrazon_social, fact.econ_cat_epos_fk");
    
		log.debug("strSQL:: " + strSQL.toString());
		log.debug("conditions:: " + conditions);

		log.info("getDocumentQueryFile(F) ::..");
		return strSQL.toString();
	}//getDocumentQueryFile
	
	//GETTERS
  /**
	 * Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions() {
		return conditions;
	}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {
		return paginaNo;
	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() {
		return paginaOffset;
	}
	
	//atributos del formulario
  public String getBancoFondeo(){return bancoFondeo;}
	public String getClaveEpo(){return claveEpo;}
  public String getClaveIf(){return claveIf;}
  public String getNafinElectronicoPyme(){return nafinElectronicoPyme;}
  public String getNumeroProveedor(){return numeroProveedor;}
	public String getOrden(){return orden;}
	
	//SETTERS
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {
		paginaNo = newPaginaNo;
	}
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset) {
    this.paginaOffset=paginaOffset;
  }
		
	//atributos del formulario
	public void setBancoFondeo(String bancoFondeo){this.bancoFondeo = bancoFondeo;}
	public void setClaveEpo(String claveEpo){this.claveEpo = claveEpo;}
  public void setClaveIf(String claveIf){this.claveIf = claveIf;}
  public void setNafinElectronicoPyme(String nafinElectronicoPyme){this.nafinElectronicoPyme = nafinElectronicoPyme;}
  public void setNumeroProveedor(String numeroProveedor){this.numeroProveedor = numeroProveedor;}
	public void setOrden(String orden){this.orden = orden;}
}