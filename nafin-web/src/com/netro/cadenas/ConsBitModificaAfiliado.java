package com.netro.cadenas;

import com.netro.exception.NafinException;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * clase para la pantalla  Crear Notificación en pantalla ( Home IF con perfiles ADMIN IF
 * @author Deysi Laura Hernández Contreras
 */
public class ConsBitModificaAfiliado implements IQueryGeneratorRegExtJS {

    private static final Log LOG = LogFactory.getLog(ConsBitModificaAfiliado.class);
   
    private String paginaOffset;
    private String paginaNo;
    private List conditions;
    StringBuilder qrySentencia = new StringBuilder("");
    private String claveIf;
    private String usuarioIF;
    

    public ConsBitModificaAfiliado() {

    }

    /**
     * @return
     */
    public String getAggregateCalculationQuery() {
        LOG.info("getAggregateCalculationQuery(E)");
        qrySentencia = new StringBuilder();
        conditions = new ArrayList();


        LOG.info("qrySentencia  " + qrySentencia);
        LOG.info("conditions " + conditions);
        LOG.info("getAggregateCalculationQuery(S)");
        return qrySentencia.toString();
    }

    /**
     * @return
     */
    public String getDocumentQuery() {

        LOG.info("getDocumentQuery(E)");
        qrySentencia = new StringBuilder();
        conditions = new ArrayList();


        LOG.info("qrySentencia  " + qrySentencia);
        LOG.info("conditions " + conditions);
        LOG.info("getDocumentQuery(S)");
        return qrySentencia.toString();
    }


    /**
     * @param pageIds
     * @return
     */
    public String getDocumentSummaryQueryForIds(List pageIds) {

        LOG.info("getDocumentSummaryQueryForIds(E)");
        qrySentencia = new StringBuilder();
        conditions = new ArrayList();


        LOG.info("qrySentencia  " + qrySentencia);
        LOG.info("conditions " + conditions);
        LOG.info("getDocumentSummaryQueryForIds(S)");
        return qrySentencia.toString();
    }

    /**
     * @return
     */
    public String getDocumentQueryFile() {
        LOG.info("getDocumentQueryFile(E)");
        qrySentencia = new StringBuilder();
        conditions = new ArrayList();

        qrySentencia.append("  SELECT distinct  b.IC_BIT_AFILIADOS as IC_BIT , "+
                            "   b.IC_PYME as IC_PYME , "+
                            "   b.IC_NAFIN_ELECTRONICO  as NA_ELECTRONICO,   " +
                            "   p.CG_RAZON_SOCIAL as RAZON_SOCIAL  ,  " + 
                            "   p.CG_RFC as RFC,  " +
                            "   b.CG_TRAMITE as TRAMITE , " +
                            "   b.IC_USUARIO AS USUARIO, "+
                            "   to_char(b.DF_MODIFICACION , 'DD/MM/YYYY HH24:MI:SS') AS DF_MODIFICACION  "+                           
                            "  FROM  " +
                            "  COM_BIT_AFILIADOS b,  " +
                            "  COMCAT_PYME p  " + 
                            
                            "  WHERE b.IC_PYME = p.IC_PYME  " +
                            "  AND b.ic_pyme in (SELECT DISTINCT C.ic_pyme  " +
                            " FROM comcat_epo A, comcat_if B, comrel_cuenta_bancaria C, comrel_pyme_if E   " +
                            " WHERE A.ic_epo = E.ic_epo   " +
                            " AND B.ic_if = E.ic_if   " +
                            " AND C.ic_cuenta_bancaria = E.ic_cuenta_bancaria   " +
                            " AND E.cs_borrado = ?   " +
                            " AND E.cs_vobo_if = ?  " +
                            " AND  E.ic_if = ?  ) " +        
                            " and b.IC_BIT_AFILIADOS  not in (   " + 
                            " select e.IC_BIT_AFILIADOS from COM_BIT_ENTERADOS e  " + 
                            " where  e.IC_USUARIO = ?  " + 
                            " and  e.ic_if  = ? ) " +
                            " order by DF_MODIFICACION asc " +
                            " ");
        
        
                
        this.conditions.add("N");
        this.conditions.add("S");
        this.conditions.add(claveIf);
        this.conditions.add(usuarioIF);
        this.conditions.add(claveIf);        
             
        LOG.info("qrySentencia  " + qrySentencia + " \n  conditions " + conditions);

        LOG.info("getDocumentQueryFile(S)");
        return qrySentencia.toString();
    }

    /**
     * @param request
     * @param rs
     * @param path
     * @param tipo
     * @return
     */
    public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path,   String tipo) {       
        return "";
    }

    /**
     * @param request
     * @param rs
     * @param path
     * @param tipo
     * @return
     */
    public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
        LOG.debug("crearCustomFile (E)");

        String nombreArchivo = "";
        StringBuilder contenidoArchivo = new StringBuilder();
        OutputStreamWriter writer = null;
        BufferedWriter buffer = null;

        try {

            nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
            writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
            buffer = new BufferedWriter(writer);
            contenidoArchivo = new StringBuilder();
            contenidoArchivo.append(" NE, Razón Social, RFC, Trámite, Fecha Modificación \n ");

            while (rs.next()) {              
                
                String ictramites = (rs.getString("TRAMITE") == null) ? "" : rs.getString("TRAMITE");                
                String tramite =  this.datosTramite(ictramites, "Archivo");
                contenidoArchivo.append((rs.getString("NA_ELECTRONICO") == null) ? "" : rs.getString("NA_ELECTRONICO").replace(',', ' ') + ",");
                contenidoArchivo.append((rs.getString("RAZON_SOCIAL") == null) ? "" : rs.getString("RAZON_SOCIAL").replace(',', ' ') + ",");
                contenidoArchivo.append((rs.getString("RFC") == null) ? "" : rs.getString("RFC").replace(',', ' ') + ",");
                contenidoArchivo.append(tramite.replace(',', ' ') + ",");
                contenidoArchivo.append((rs.getString("DF_MODIFICACION") == null) ? "" : rs.getString("DF_MODIFICACION").replace(',', ' ') + ",");
                contenidoArchivo.append(" \n ");
            }

            buffer.write(contenidoArchivo.toString());
            buffer.close();

        } catch (Throwable e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new AppException("Error al generar el archivo ", e);
        } finally {   
            LOG.info("crearCustomFile (S)");
        }
        return nombreArchivo;
    }

    /**
     * @param claveIf
     * @param iNoUsuario
     * @return
     * @throws NafinException
     */
    public int notificaciones (String claveIf, String iNoUsuario  ) throws NafinException {

        AccesoDB con = new AccesoDB();       
        StringBuilder qrySQL = new StringBuilder();       
        int total = 0;
        List varBind = new ArrayList();
        try {
            con.conexionDB();
           
            qrySQL = new StringBuilder();             
            qrySQL.append(" SELECT COUNT(*) FROM COM_BIT_AFILIADOS  b"+
                                " WHERE IC_PYME IN (  "+
                                " SELECT DISTINCT C.ic_pyme "+
                                " from comcat_epo A, comcat_if B, "+
                                " comrel_cuenta_bancaria C, comrel_pyme_if E "+
                                " where A.ic_epo = E.ic_epo  "+
                                " and B.ic_if = E.ic_if  "+
                                " and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria  "+
                                " and E.cs_borrado = ?  "+
                                " AND E.cs_vobo_if = ?  " +
                                " AND  E.ic_if = ?  )  "+
                                " and b.IC_BIT_AFILIADOS  not in (    select e.IC_BIT_AFILIADOS from COM_BIT_ENTERADOS e  " + 
                                " where  e.IC_USUARIO = ?  " + 
                                " and  e.ic_if  = ? ) " );
            
            varBind = new ArrayList();
            varBind.add("N");
            varBind.add("S");
            varBind.add(claveIf);            
            varBind.add(iNoUsuario);   
            varBind.add(claveIf);   
            LOG.debug ("qrySQL  "+qrySQL.toString() +" varBind    \n "+varBind);            
            PreparedStatement  ps = con.queryPrecompilado(qrySQL.toString(), varBind);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                total = rs.getInt(1);
            }
            rs.close();
            ps.close();        
            
        } catch (Exception e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return total;
    }
    
    
    /**
     * metodo que se obtiene los tramites que se realizaron en la modificación del proveedor 
     * @param icTramites
     * @param tipoCOns
     * @return
     * @throws NafinException
     */
    public String  datosTramite (String icTramites, String tipoCOns ) throws NafinException {

        AccesoDB con = new AccesoDB();
       
        StringBuilder qrySQL = new StringBuilder();       
        StringBuilder datosTramite = new StringBuilder(); 
        List varBind = new ArrayList();
        try {

            con.conexionDB();
           
            String tramites =  icTramites.replace("|", ",");
            
            qrySQL = new StringBuilder();             
            qrySQL.append(" select cg_tramite as TRAMITE "+
                                " from COMCAT_TRAMITE " + 
                                " where ic_tramite in ( " +tramites +" )");
            
            LOG.debug ("qrySQL  "+qrySQL.toString() +" varBind    \n "+varBind);
            
            PreparedStatement  ps = con.queryPrecompilado(qrySQL.toString(), varBind);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String tramite =  rs.getString("TRAMITE") == null ? "" : rs.getString("TRAMITE");                 
                if ("Grid".equals(tipoCOns)  ) {
                    datosTramite.append(datosTramite.length() > 0 ? "<br> " : "").append(tramite);
                }else  if ("Archivo".equals(tipoCOns)  ) {
                    datosTramite.append(datosTramite.length() > 0 ? "/ " : "").append(tramite);  
                }
            }
            rs.close();
            ps.close();
         
            
        } catch (Exception e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return datosTramite.toString();
    }
    
    /**
     * @param idBitAfiliados
     * @param fechaEnterado
     * @param horaEntrerado
     * @param icIf
     * @param usuario
     * @return
     * @throws NafinException
     */
    public boolean  actualizaEnterado (String []idBitAfiliados, String fechaEnterado , String horaEntrerado, String icIf, String usuario ) throws NafinException {

        AccesoDB con = new AccesoDB();       
        StringBuilder qrySQL = new StringBuilder(); 
        List varBind = new ArrayList();
        PreparedStatement ps = null;
        boolean exito = false;
       
        try {

            con.conexionDB();
          
            for(int i=0;i<idBitAfiliados.length;i++){  
                String icBit =  idBitAfiliados[i];
                         
                qrySQL = new StringBuilder();                                  
                qrySQL.append(" INSERT INTO  COM_BIT_ENTERADOS " + 
                                    " ( IC_BIT_ENTERADOS , IC_USUARIO, IC_IF , DF_ENTERADO , HORA_ENTERADO, IC_BIT_AFILIADOS )"+
                                    " VALUES ( COM_BIT_ENTERADOS_SEQ.nextval, ?, ?, to_date(?,'dd/mm/yyyy' ), ? ,  ?  ) ");
                varBind = new ArrayList();           
                varBind.add(usuario );    
                varBind.add(icIf );
                varBind.add(fechaEnterado );
                varBind.add(horaEntrerado );
                varBind.add(icBit  );
                
                LOG.debug ("qrySQL  "+qrySQL.toString() +" varBind    \n "+varBind);
                ps = con.queryPrecompilado(qrySQL.toString(), varBind);
                ps.executeUpdate();
                ps.close();
            }
            exito = true;
           
            
        } catch (Exception e) {
            exito = false;
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(exito);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return exito;
    }

    /**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones
     */
    public List getConditions() {
        return conditions;
    }

    /**
     * Obtiene el numero de pagina.
     * @return Cadena con el numero de pagina
     */
    public String getPaginaNo() {
        return paginaNo;
    }

    /**
     * Obtiene el offset de la pagina.
     * @return Cadena con el offset de pagina
     */
    public String getPaginaOffset() {
        return paginaOffset;
    }


    /*****************************************************
					 SETTERS
*******************************************************/

    /**
     * Establece el numero de pagina.
     * @param  newPaginaNo Cadena con el numero de pagina
     */
    public void setPaginaNo(String newPaginaNo) {
        paginaNo = newPaginaNo;
    }

    /**
     * Establece el offset de la pagina.
     * @param newPaginaOffset Cadena con el offset de pagina
     */
    public void setPaginaOffset(String paginaOffset) {
        this.paginaOffset = paginaOffset;
    }

    /**
     * @param claveIf
     */
    public void setClaveIf(String claveIf) {
        this.claveIf = claveIf;
    }

    /**
     * @return
     */
    public String getClaveIf() {
        return claveIf;
    }

    /**
     * @param usuarioIF
     */
    public void setUsuarioIF(String usuarioIF) {
        this.usuarioIF = usuarioIF;
    }
    
    /**
     * @return
     */
    public String getUsuarioIF() {
        return usuarioIF;
    }
}
