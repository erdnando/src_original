package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class BaseOperacion  implements IQueryGeneratorRegExtJS{

	private ArrayList valoresBind = new ArrayList();
	private int baseOperacion;
	private static Log log = ServiceLocator.getInstance().getLog(com.netro.cadenas.BaseOperacion.class);
	
	public BaseOperacion() {
	}
	
		 /**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentQuery(){
	log.debug("getDocumentQuery (E) ");
	StringBuffer strQuery 	= new StringBuffer();

	strQuery.append(
	"SELECT DISTINCT bo.codigo_base_operacion AS codigo_base_oper,						"+
   "             bo.codigo_empresa AS codigo_emp,											"+
   "             bo.codigo_producto_banco AS codigo_proc_banco, anio,				"+	
   "             modalidad_producto, codigo_grupo_parametros, tipo_certificado,"+
   "             tipo_uso_tasa, vigente, codigo_grupo_mora,"+
   "             relacion_matematica_mora, spread_mora, limite_maximo_producto,"+
   "             limite_maximo_cliente, bo.adicionado_por AS adicionado_px,"+
   "             TO_CHAR (bo.fecha_adicion,"+
   "                      'dd/mm/yyyy hh24:mi'"+
   "                     ) AS fecha_adicion,"+
   "             bo.modificado_por AS modificado_por,"+
   "             TO_CHAR (bo.fecha_modificacion,"+
   "                      'dd/mm/yyyy hh24:mi'"+
   "                     ) AS fecha_mod,"+
   "             bo.descripcion AS descripci, dias_minimos_primer_pago,"+
   "             opera_factoraje, valida_limite_maximo_cliente,"+
   "             con_transferencia, emergente, aplica_modalidad,"+
   "             bo.tipo_base_operacion AS tipo_b_operacion,"+
   "             pb.descripcion AS descripcion"+
   "        FROM no_bases_de_operacion bo, no_productos_banco pb"+
   "       WHERE bo.codigo_empresa = pb.codigo_empresa"+
   "         AND bo.codigo_producto_banco = pb.codigo_producto_banco"+
   "         AND bo.codigo_base_operacion = ? "+
   "    ORDER BY 1		");
	valoresBind.add(String.valueOf(baseOperacion));
	log.debug(" strQuery identificadores unicos - - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.debug("getDocumentQuery (S) ");
		
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * datos a mostrar en la consulta basandose en los identificadores unicos 
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista 
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentSummaryQueryForIds(List ids){
	
	log.info("getDocumentSummaryQueryForIds(E)");
	StringBuffer strQuery 	= new StringBuffer();
	List 		conditions= new ArrayList();		
		
	strQuery.append(
	"SELECT DISTINCT bo.codigo_base_operacion AS codigo_base_oper,		"			
   +"             bo.codigo_empresa AS codigo_emp,											"+
   "             bo.codigo_producto_banco AS codigo_proc_banco, anio,				"+	
   "             modalidad_producto, codigo_grupo_parametros, tipo_certificado,"+
   "             tipo_uso_tasa, vigente, codigo_grupo_mora,"+
   "             relacion_matematica_mora, spread_mora, limite_maximo_producto,"+
   "             limite_maximo_cliente, bo.adicionado_por AS adicionado_px,"+
   "             TO_CHAR (bo.fecha_adicion,"+
   "                      'dd/mm/yyyy hh24:mi'"+
   "                     ) AS fecha_adicion,"+
   "             bo.modificado_por AS modificado_por,"+
   "             TO_CHAR (bo.fecha_modificacion,"+
   "                      'dd/mm/yyyy hh24:mi'"+
   "                     ) AS fecha_mod,"+
   "             bo.descripcion AS descripci, dias_minimos_primer_pago,"+
   "             opera_factoraje, valida_limite_maximo_cliente,"+
   "             con_transferencia, emergente, aplica_modalidad,"+
   "             bo.tipo_base_operacion AS tipo_b_operacion,"+
   "             pb.descripcion AS descripcion"+
   "        FROM no_bases_de_operacion bo, no_productos_banco pb"+
   "       WHERE bo.codigo_empresa = pb.codigo_empresa"+
   "         AND bo.codigo_producto_banco = pb.codigo_producto_banco"+
   "         AND bo.codigo_base_operacion = ? "+
   "    ORDER BY 1		");
	  
	// if(!ic_if.equals("0")){
	//	strQuery.append(" AND i.ic_if = ? ");
	valoresBind = new ArrayList();
		valoresBind.add(String.valueOf(baseOperacion));
			
		List pKeys = new ArrayList();
		for(int i = 0; i < ids.size(); i++){
			List lItem = (ArrayList)ids.get(i);
		//	if(numList ==1){				
				conditions.add(lItem);				
			//	valoresBind=(ArrayList)conditions;
				//numList++;
		//	}
		//	pKeys.add(ids.get(i));			
		}		
	 //}
	
	log.info("strQuery basado en identificadores unicos - -  >" + strQuery);
	log.info("valoresBind "+ valoresBind);
	log.info("getDocumentSummaryQueryForIds(S)");	
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getAggregateCalculationQuery(){
	StringBuffer strQuery 	= new StringBuffer();
	log.info("getAggregateCalculationQuery (E)");
	
	strQuery.append(
	"SELECT DISTINCT bo.codigo_base_operacion AS codigo_base_oper,						"+
   "             bo.codigo_empresa AS codigo_emp,											"+
   "             bo.codigo_producto_banco AS codigo_proc_banco, anio,				"+	
   "             modalidad_producto, codigo_grupo_parametros, tipo_certificado,"+
   "             tipo_uso_tasa, vigente, codigo_grupo_mora,"+
   "             relacion_matematica_mora, spread_mora, limite_maximo_producto,"+
   "             limite_maximo_cliente, bo.adicionado_por AS adicionado_px,"+
   "             TO_CHAR (bo.fecha_adicion,"+
   "                      'dd/mm/yyyy hh24:mi'"+
   "                     ) AS fecha_adicion,"+
   "             bo.modificado_por AS modificado_por,"+
   "             TO_CHAR (bo.fecha_modificacion,"+
   "                      'dd/mm/yyyy hh24:mi'"+
   "                     ) AS fecha_mod,"+
   "             bo.descripcion AS descripci, dias_minimos_primer_pago,"+
   "             opera_factoraje, valida_limite_maximo_cliente,"+
   "             con_transferencia, emergente, aplica_modalidad,"+
   "             bo.tipo_base_operacion AS tipo_b_operacion,"+
   "             pb.descripcion AS descripcion"+
   "        FROM no_bases_de_operacion bo, no_productos_banco pb"+
   "       WHERE bo.codigo_empresa = pb.codigo_empresa"+
   "         AND bo.codigo_producto_banco = pb.codigo_producto_banco"+
   "         AND bo.codigo_base_operacion = ? "+
   "    ORDER BY 1		");
						

						
	log.debug(" strQuery obtiene totales- - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.info("getAggregateCalculationQuery (S)");
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
public  List getConditions(){		
	return valoresBind;
	}
	
	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentQueryFile(){
	log.debug("getDocumentQueryFile (E)");
	StringBuffer strQuery 	= new StringBuffer();
	valoresBind = new ArrayList();	
	strQuery.append(
	"SELECT DISTINCT bo.codigo_base_operacion AS codigo_base_oper,						"+
   "             bo.codigo_empresa AS codigo_emp,											"+
   "             bo.codigo_producto_banco AS codigo_proc_banco, anio,				"+	
   "             modalidad_producto, codigo_grupo_parametros, tipo_certificado,"+
   "             tipo_uso_tasa, vigente, codigo_grupo_mora,"+
   "             relacion_matematica_mora, spread_mora, limite_maximo_producto,"+
   "             limite_maximo_cliente, bo.adicionado_por AS adicionado_px,"+
   "             TO_CHAR (bo.fecha_adicion,"+
   "                      'dd/mm/yyyy hh24:mi'"+
   "                     ) AS fecha_adicion,"+
   "             bo.modificado_por AS modificado_por,"+
   "             TO_CHAR (bo.fecha_modificacion,"+
   "                      'dd/mm/yyyy hh24:mi'"+
   "                     ) AS fecha_mod,"+
   "             bo.descripcion AS descripci, dias_minimos_primer_pago,"+
   "             opera_factoraje, valida_limite_maximo_cliente,"+
   "             con_transferencia, emergente, aplica_modalidad,"+
   "             bo.tipo_base_operacion AS tipo_b_operacion,"+
   "             pb.descripcion AS descripcion"+
   "        FROM no_bases_de_operacion bo, no_productos_banco pb"+
   "       WHERE bo.codigo_empresa = pb.codigo_empresa"+
   "         AND bo.codigo_producto_banco = pb.codigo_producto_banco"+
   "         AND bo.codigo_base_operacion = ? "+
   "    ORDER BY 1		");
	valoresBind.add(String.valueOf(baseOperacion));
					
	log.debug(" strQuery para generar archivo- - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.debug("getDocumentQueryFile (S)<- - - ");
		
	return strQuery.toString();
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
public  String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		
	log.debug("crearCustomFile (E)");
	String nombreArchivo ="";
	HttpSession session = request.getSession();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	OutputStreamWriter writer = null;
	BufferedWriter buffer = null;
	int total = 0;
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		buffer = new BufferedWriter(writer);
			
		contenidoArchivo = new StringBuffer();	
		contenidoArchivo.append( "Nombre IF ,C�DIGO BASE OPERACI�N,DESCRIPCI�N DE LA PARAMETRIZACI�N DE BASES DE OPERACI�N,C�DIGO PRODUCTO BANCO,DESCRIPCI�N PRODUCTO BANCO,DIAS MINIMOS PRIMER PAGO,OPERA FACTORAJE\n");
			
		while (rs.next())	{
			String nombreif 	= (rs.getString("NOMBRE") 						== null) ? "" : rs.getString("NOMBRE");
			String codbasop 	= (rs.getString("CODIGO_BASE_OPERACION") 	== null) ? "" : rs.getString("CODIGO_BASE_OPERACION");
			String unodes 		= (rs.getString("UNODES") 					== null) ? "" : rs.getString("UNODES");				
			String codproban	= (rs.getString("CODIGO_PRODUCTO_BANCO") 	== null) ? "" : rs.getString("CODIGO_PRODUCTO_BANCO");
			String desdos	 	= (rs.getString("DESDOS") 					== null) ? "" : rs.getString("DESDOS");				
			String diasmin		= (rs.getString("DIAS_MINIMOS_PRIMER_PAGO")== null) ? "" : rs.getString("DIAS_MINIMOS_PRIMER_PAGO");
			String opfac	 	= (rs.getString("OPERA_FACTORAJE") 		== null) ? "" : rs.getString("OPERA_FACTORAJE");
			
			contenidoArchivo.append(
			nombreif.replace	(',',' ')+","+
			codbasop.replace	(',',' ')+","+		
			unodes.replace		(',',' ')+","+
			codproban.replace	(',',' ')+","+
			desdos.replace		(',',' ')+","+
			diasmin.replace	(',',' ')+","+
			opfac.replace		(',',' ')+","+ "\n");
		}
			
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
		
		}catch (Exception e){
			throw new AppException("Error al generar el archivo ", e);
		}
	log.debug("crearCustomFile (S)");
	return nombreArchivo;
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
	String nombreArchivo = "";
	HttpSession session = request.getSession();	
	ComunesPDF pdfDoc = new ComunesPDF();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(1, path + nombreArchivo);
			
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
		pdfDoc.setTable(2,50);	
		
		pdfDoc.setCell("Nombre del Campo","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Valor"				,"celda01",ComunesPDF.CENTER);
		while(reg.next()){
		
		pdfDoc.setCell("CODIGO BASE OPERACION" ,"celda01",ComunesPDF.LEFT);	
		String cbo	= (reg.getString("CODIGO_BASE_OPER") 	== null) ? "" : reg.getString("CODIGO_BASE_OPER");
		pdfDoc.setCell(cbo	,"formas",ComunesPDF.CENTER);
		
		pdfDoc.setCell("C�DIGO EMPRESA","celda01",ComunesPDF.LEFT);	
		String ce 	= (reg.getString("CODIGO_EMP")			== null) ? "" : reg.getString("CODIGO_EMP");
		pdfDoc.setCell(ce		,"formas",ComunesPDF.CENTER);		
		
		pdfDoc.setCell("C�DIGO PRODUCTO BANCO","celda01",ComunesPDF.LEFT);	
		String cpb 	= (reg.getString("CODIGO_PROC_BANCO")	== null) ? "" : reg.getString("CODIGO_PROC_BANCO");
		pdfDoc.setCell(cpb	,"formas",ComunesPDF.CENTER);
	
		pdfDoc.setCell("DESCRIPCI�N PRODUCTO BANCO","celda01",ComunesPDF.LEFT);	
		String dpb 	= (reg.getString("DESCRIPCION")			== null) ? "" : reg.getString("DESCRIPCION");
		pdfDoc.setCell(dpb	,"formas",ComunesPDF.CENTER);	
			
		pdfDoc.setCell("A�O","celda01",ComunesPDF.LEFT);
		String a 	= (reg.getString("ANIO")					== null) ? "" : reg.getString("ANIO");
		pdfDoc.setCell(a		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("MODALIDAD PRODUCTO","celda01",ComunesPDF.LEFT);	
		String mp 	= (reg.getString("MODALIDAD_PRODUCTO")	== null) ? "" : reg.getString("MODALIDAD_PRODUCTO");
		pdfDoc.setCell(mp		,"formas",ComunesPDF.CENTER);
	
		pdfDoc.setCell("C�DIGO GRUPO PARAMETROS","celda01",ComunesPDF.LEFT);	
		String cgp 	= (reg.getString("CODIGO_GRUPO_PARAMETROS") == null) ? "" : reg.getString("CODIGO_GRUPO_PARAMETROS");
		pdfDoc.setCell(cgp	,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("TIPO CERTIFICADOS","celda01",ComunesPDF.LEFT);	
		String tc 	= (reg.getString("TIPO_CERTIFICADO")		== null) ? "" : reg.getString("TIPO_CERTIFICADO");
		pdfDoc.setCell(tc		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("TIPO USO TASA","celda01",ComunesPDF.LEFT);	
		String tut 	= (reg.getString("TIPO_USO_TASA") 			== null) ? "" : reg.getString("TIPO_USO_TASA");
		pdfDoc.setCell(tut	,"formas",ComunesPDF.CENTER);
	
		pdfDoc.setCell("VIGENTE","celda01",ComunesPDF.LEFT);	
		String v 	= (reg.getString("VIGENTE") 					== null) ? "" : reg.getString("VIGENTE");
		pdfDoc.setCell(v		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("C�DIGO GRUPO MORA","celda01",ComunesPDF.LEFT);	
		String cgm 	= (reg.getString("CODIGO_GRUPO_MORA")		== null) ? "" : reg.getString("CODIGO_GRUPO_MORA");
		pdfDoc.setCell(cgm	,"formas",ComunesPDF.CENTER);	
			
		pdfDoc.setCell("RELACI�N MATEM�TICA MORA"	,"celda01",ComunesPDF.LEFT);	
		String rmm 	= (reg.getString("RELACION_MATEMATICA_MORA") == null) ? "" : reg.getString("RELACION_MATEMATICA_MORA");
		pdfDoc.setCell(rmm	,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("SPREAD MORA"	,"celda01",ComunesPDF.LEFT);	
		String sm 	= (reg.getString("SPREAD_MORA") 				== null) ? "" : reg.getString("SPREAD_MORA");
		pdfDoc.setCell(sm		,"formas",ComunesPDF.CENTER);	
			
		pdfDoc.setCell("LIMITE M�XIMO PRODUCTO","celda01",ComunesPDF.LEFT);	
		String lmp 	= (reg.getString("LIMITE_MAXIMO_PRODUCTO")== null) ? "" : reg.getString("LIMITE_MAXIMO_PRODUCTO");
		String newLimitPro = Comunes.formatoDecimal(lmp,2);
		pdfDoc.setCell(newLimitPro	,"formas",ComunesPDF.RIGHT);

		pdfDoc.setCell("LIMITE M�XIMO CLIENTE"	,"celda01",ComunesPDF.LEFT);	
		String lmc 	= (reg.getString("LIMITE_MAXIMO_CLIENTE") == null) ? "" : reg.getString("LIMITE_MAXIMO_CLIENTE");
		String newLimitCli = Comunes.formatoDecimal(lmc,2);
		pdfDoc.setCell(newLimitCli	,"formas",ComunesPDF.RIGHT);		
		
		pdfDoc.setCell("ADICIONADO POR"	,"celda01",ComunesPDF.LEFT);	
		String ap 	= (reg.getString("ADICIONADO_PX") 			== null) ? "" : reg.getString("ADICIONADO_PX");
		pdfDoc.setCell(ap		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("FECHA ADICI�N","celda01",ComunesPDF.LEFT);	
		String fa 	= (reg.getString("FECHA_ADICION")			== null) ? "" : reg.getString("FECHA_ADICION");
		pdfDoc.setCell(fa		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("MODIFICADO POR","celda01",ComunesPDF.LEFT);	
		String mpor 	= (reg.getString("MODIFICADO_POR") 		== null) ? "" : reg.getString("MODIFICADO_POR");
		pdfDoc.setCell(mpor	,"formas",ComunesPDF.CENTER);	
			
		pdfDoc.setCell("FECHA MODIFICACI�N"	,"celda01",ComunesPDF.LEFT);	
		String fm 	= (reg.getString("FECHA_MOD") 				== null) ? "" : reg.getString("FECHA_MOD");
		pdfDoc.setCell(fm		,"formas",ComunesPDF.CENTER);
	
		pdfDoc.setCell("DESCRIPCI�N "		,"celda01",ComunesPDF.LEFT);	
		String des 	= (reg.getString("DESCRIPCI") 				== null) ? "" : reg.getString("DESCRIPCI");
		pdfDoc.setCell(des	,"formas",ComunesPDF.LEFT);
		
		pdfDoc.setCell("DIAS MINIMOS PRIMER PAGO " ,"celda01",ComunesPDF.LEFT);	
		String dm 	= (reg.getString("DIAS_MINIMOS_PRIMER_PAGO")== null) ? "" : reg.getString("DIAS_MINIMOS_PRIMER_PAGO");
		pdfDoc.setCell(dm		,"formas",ComunesPDF.CENTER);
	
		pdfDoc.setCell("OPERA FACTORAJE "	,"celda01",ComunesPDF.LEFT);	
		String of 	= (reg.getString("OPERA_FACTORAJE") 		== null) ? "" : reg.getString("OPERA_FACTORAJE");
		pdfDoc.setCell(of		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("VALIDA LIMITE M�XIMO CLIENTE  "	,"celda01",ComunesPDF.LEFT);	
		String vl 	= (reg.getString("VALIDA_LIMITE_MAXIMO_CLIENTE") == null) ? "" : reg.getString("VALIDA_LIMITE_MAXIMO_CLIENTE");
		pdfDoc.setCell(vl		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("CON TRANSFERENCIA "	,"celda01",ComunesPDF.LEFT);	
		String ct 	= (reg.getString("CON_TRANSFERENCIA") 		== null) ? "" : reg.getString("CON_TRANSFERENCIA");
		pdfDoc.setCell(ct		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("EMERGENTE "	,"celda01",ComunesPDF.LEFT);	
		String e 	= (reg.getString("EMERGENTE") 				== null) ? "" : reg.getString("EMERGENTE");
		pdfDoc.setCell(e		,"formas",ComunesPDF.CENTER);

		pdfDoc.setCell("TIPO BASE OPERACI�N "	,"celda01",ComunesPDF.LEFT);	
		String tb 	= (reg.getString("TIPO_B_OPERACION") 		== null) ? "" : reg.getString("TIPO_B_OPERACION");
		pdfDoc.setCell(tb		,"formas",ComunesPDF.CENTER);
	
		pdfDoc.setCell("APLICA MODALIDAD "	,"celda01",ComunesPDF.LEFT);	
		String aplica 	= (reg.getString("APLICA_MODALIDAD") 	== null) ? "" : reg.getString("APLICA_MODALIDAD");
		pdfDoc.setCell(aplica	,"formas",ComunesPDF.CENTER);
		}		
		pdfDoc.addTable();
		
		
		pdfDoc.endDocument();
	} catch(Exception e){
		throw new AppException("Error al generar el archivo PDF ", e);
	}
	return nombreArchivo;
	}


	public void setBaseOperacion(int baseOperacion) {
		this.baseOperacion = baseOperacion;
	}


	public int getBaseOperacion() {
		return baseOperacion;
	}



}