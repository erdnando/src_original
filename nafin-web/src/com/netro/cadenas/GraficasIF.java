package com.netro.cadenas;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class GraficasIF  {

	private final static Log log = ServiceLocator.getInstance().getLog(GraficasIF.class);
	
	public GraficasIF() {
	}
	
	
	/**
	 * Obtiene la informaci�n especifica para armar la grafica de la EPO.
	 * @param claveEpo Clave del IF
	 * @param claveGrafica Clave de la grafica
	 */
	public List getInfoGraficaIF(String claveIf, String grafica) {
		log.info("iClaveIf(E)");
		AccesoDB con = new AccesoDB();
		int iClaveIf = 0;		
		List lstData = new ArrayList();		
		PreparedStatement ps = null;
		ResultSet rs = null;  
		String query = "";
		List listasGraficas = new ArrayList();

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveIf == null || claveIf.equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveIf = Integer.parseInt(claveIf);
			
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos",e);
		}
		//****************************************************************************************
		
		try{
		
			con.conexionDB();
		
			query = "SELECT b.ic_grafica_cache_det, b.cg_grafica, b.cg_atributo, b.cg_valor,b.cg_valorce " +
					"  FROM com_graficas_cache a, com_graficas_cache_det b " +
					" WHERE a.ic_afiliado = b.ic_afiliado " +
					"   AND a.ic_tipo_afiliado = b.ic_tipo_afiliado " +
					"	 AND a.cs_estatus <> 'ERR' " +
					"   AND b.cg_grafica in( "+grafica+" ) " +
					"   AND b.ic_afiliado = ? " +
					"   AND b.ic_tipo_afiliado = ? "+
					"   ORDER BY b.cg_atributo ";
			
			ps = con.queryPrecompilado(query);
			
			ps.setInt(1, iClaveIf);
			ps.setString(2,"I");
			rs = ps.executeQuery();
			
			
			while(rs.next()){
				HashMap mp = new HashMap();
				mp.put("CG_ATRIBUTO",rs.getString("CG_ATRIBUTO"));
				mp.put("CG_VALOR",rs.getString("CG_VALOR"));
				mp.put("CG_VALORCE",rs.getString("CG_VALORCE"));
				mp.put("CG_GRAFICA",rs.getString("CG_GRAFICA"));
				lstData.add(mp);
				
			}
			rs.close();
			ps.close();
								
			return lstData;
			
		}catch(Throwable t){
			throw new AppException("Error al obtener informacion de Grafica", t);	
		}finally{
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
			log.info("iClaveIf(S)");
		}
		
	}


/**/

	public String obtenerInformacionProceso() {
		log.info("obtenerInformacionProceso(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		String  iClaveIf = "";
		List params = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String mensaje = "",  estatusProc = "", strSQL = "", strSQLExis ="", existe ="";
		

		try {
			con.conexionDB();
			
			strSQL = " DELETE FROM com_graficas_cache_det " +
						" WHERE ic_tipo_afiliado = ? ";						
			params = new ArrayList();			
			params.add("I");				
			con.ejecutaUpdateDB(strSQL, params);
				
			strSQL = 
				" DELETE FROM  com_graficas_cache " +
				" WHERE ic_tipo_afiliado = ? ";				
			params = new ArrayList();			
			params.add("I");				
			con.ejecutaUpdateDB(strSQL, params);
			
			con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion
			
						// para Verificarsi existe IF en la tabla de com_graficas_cache
			strSQLExis = " select ic_afiliado  from  com_graficas_cache "+
						    " where  ic_tipo_afiliado = ? "+
							 " and ic_afiliado =? ";   
						
			
			/************************************************************
			***************** Operaci�n mensual*******************
			***************************************************************/			
			strSQL = " SELECT   /*+ index (s IN_COM_SOLICITUD_13_NUK) */ "+
						" d.ic_if as IC_AFILIADO,  "+
						" TO_CHAR (df_fecha_solicitud, 'mm') as CG_ATRIBUTO, "+							
						"	DECODE(d.ic_moneda, 1, 'OPERACIONMN',54, 'OPERACIONDL') AS CG_GRAFICA,  "+
						"	SUM (d.fn_monto_dscto)AS CG_VALOR  "+					
						" FROM com_solicitud s, com_documento d   "+
						"	WHERE s.ic_documento = d.ic_documento  "+
						" AND s.df_fecha_solicitud >=    TO_DATE ('01/01/' || TO_CHAR (SYSDATE, 'yyyy'), 'dd/mm/yyyy')  "+
						"	AND s.df_fecha_solicitud < TRUNC (SYSDATE + 1)       "+  
						" GROUP BY d.ic_moneda,  "+
						"	d.ic_if,  "+
						" TO_CHAR (df_fecha_solicitud, 'mm'),  "+
						"	TRUNC (SYSDATE)  "+
						" ORDER BY 1, d.ic_if ";
					
			params = new ArrayList();			
			Registros regO = con.consultarDB(strSQL, params);
	
			String strInsert = 
				" INSERT INTO com_graficas_cache_det (ic_grafica_cache_det, " +
				" ic_afiliado, ic_tipo_afiliado, " +
				" cg_grafica, cg_atributo, cg_valor, cg_color) " +
				" VALUES(seq_com_graficas_cache_det.nextval,?,?,?,?,?,?)";
			ps = con.queryPrecompilado(strInsert);		
				
			while(regO.next()) {	
								
				iClaveIf = regO.getString("IC_AFILIADO");
								  
				params = new ArrayList();
				params.add("I");	
				params.add(iClaveIf);
				Registros regExis = con.consultarDB(strSQLExis, params);
				existe ="";
				if(regExis.next()) {		
					existe=regExis.getString("IC_AFILIADO");
				}
				if (existe.equals("")) {
					strSQL = " INSERT INTO com_graficas_cache(ic_afiliado, ic_tipo_afiliado, cs_estatus)  VALUES(?,?,?) ";	
					params = new ArrayList();
					params.add(iClaveIf);
					params.add("I");
					params.add("OK");	//En Proceso			
					con.ejecutaUpdateDB(strSQL, params);							
					con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion
				}	
								
				ps.clearParameters();
				ps.setString(1, regO.getString("IC_AFILIADO"));
				ps.setString(2, "I");
				ps.setString(3, regO.getString("CG_GRAFICA"));
				ps.setString(4, regO.getString("CG_ATRIBUTO"));
				ps.setString(5, regO.getString("CG_VALOR"));
				ps.setString(6, "");
				ps.executeUpdate();
					
			}
			ps.close();
			
				/****************************************************************************
				***************** Vencimientos en el mes  para  DESCUENTO *******************
				*****************************************************************************/
			 
				strSQL = " SELECT   /*+ index (d IN_COM_DOCUMENTO_09_NUK) */ "+
							" TO_CHAR ( df_fecha_venc, 'dd')as CG_ATRIBUTO," +
							" d.ic_if as IC_AFILIADO, "+
							" DECODE(d.ic_moneda, 1, 'VENCMESMN',54, 'VENCMESDL') AS CG_GRAFICA,   "+
							" SUM (d.fn_monto_dscto) AS  CG_VALOR  "+
							" FROM com_solicitud s, com_documento d "+
							" WHERE s.ic_documento = d.ic_documento "+
							"	AND d.df_fecha_venc >=  TO_DATE ('01/' || TO_CHAR (SYSDATE, 'mm/yyyy'), 'dd/mm/yyyy')"+
							"	AND d.df_fecha_venc < last_day(trunc(SYSDATE) )+ 1  "+							
							"	GROUP BY d.ic_moneda, "+
							"	d.ic_if,"+						
							" df_fecha_venc "+
							"	ORDER BY 1, d.ic_if  ";						
				 
				params = new ArrayList();				
				Registros regv = con.consultarDB(strSQL, params);
				
				String strInsertV = 
					" INSERT INTO com_graficas_cache_det (ic_grafica_cache_det, " +
					" ic_afiliado, ic_tipo_afiliado, " +
					" cg_grafica, cg_atributo, cg_valor, cg_color) " +
					" VALUES(seq_com_graficas_cache_det.nextval,?,?,?,?,?,?)";
				ps = con.queryPrecompilado(strInsertV);
				while(regv.next()) {	
					
					iClaveIf = regv.getString("IC_AFILIADO");
								
					params = new ArrayList();
					params.add("I");	
					params.add(iClaveIf);
					Registros regExis = con.consultarDB(strSQLExis, params);
					existe ="";
					if(regExis.next()) {	
						existe=regExis.getString("IC_AFILIADO");
					}
					if (existe.equals("")) {
						strSQL = " INSERT INTO com_graficas_cache(ic_afiliado, ic_tipo_afiliado, cs_estatus)  VALUES(?,?,?) ";	
						params = new ArrayList();
						params.add(iClaveIf);
						params.add("I");
						params.add("OK");	//En Proceso			
						con.ejecutaUpdateDB(strSQL, params);							
						con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion
					}	
				
					ps.clearParameters();
					ps.setString(1, regv.getString("IC_AFILIADO"));
					ps.setString(2, "I");
					ps.setString(3, regv.getString("CG_GRAFICA"));
					ps.setString(4, regv.getString("CG_ATRIBUTO"));
					ps.setString(5, regv.getString("CG_VALOR"));
					ps.setString(6, "");
					ps.executeUpdate();				
				}
				ps.close();
			
			
				/****************************************************************************
				**************** Vencimientos en el mes  para  CREDITO ELECTRONICO ********
				*****************************************************************************/
			
				strSQL = " SELECT   /*+ index (s IN_COM_SOLIC_PORTAL_06_NUK) */  " +
							" TO_CHAR ( DF_V_DESCUENTO, 'dd')as CG_ATRIBUTO, " +
							"	ic_if AS IC_AFILIADO, ic_moneda,    " +      
							"	DECODE(ic_moneda, 1, 'VENCMESMN_CE',54, 'VENCMESDL_CE') AS CG_GRAFICA, " +
							"	SUM (FN_IMPORTE_DSCTO)  AS CG_VALOR  " +
							" FROM com_solic_portal s  " +
							"	WHERE DF_V_DESCUENTO >=  TO_DATE ('01/' || TO_CHAR (SYSDATE, 'mm/yyyy'), 'dd/mm/yyyy') " +
							"	AND DF_V_DESCUENTO < last_day(trunc(SYSDATE) )+ 1  " +							
							"	GROUP BY ic_moneda, " +
							"	ic_if,  " +
							"	DF_V_DESCUENTO  " +
							"	ORDER BY 1, ic_if " ;
				
				params = new ArrayList();			
				Registros regvCE = con.consultarDB(strSQL, params);
				
				String strInsertVCE = 
					" INSERT INTO com_graficas_cache_det (ic_grafica_cache_det, " +
					" ic_afiliado, ic_tipo_afiliado, " +
					" cg_grafica, cg_atributo, cg_valor, cg_color) " +
					" VALUES(seq_com_graficas_cache_det.nextval,?,?,?,?,?,?)";
				ps = con.queryPrecompilado(strInsertVCE);
				
				while(regvCE.next()) {	
								
					iClaveIf = regvCE.getString("IC_AFILIADO");
					
					params = new ArrayList();
					params.add("I");	
					params.add(iClaveIf);
					Registros regExis = con.consultarDB(strSQLExis, params);
					existe ="";
					if(regExis.next()) {		
						existe=regExis.getString("IC_AFILIADO");
					}
					if (existe.equals("")) {
						strSQL = " INSERT INTO com_graficas_cache(ic_afiliado, ic_tipo_afiliado, cs_estatus)  VALUES(?,?,?) ";	
						params = new ArrayList();
						params.add(iClaveIf);
						params.add("I");
						params.add("OK");	//En Proceso			
						con.ejecutaUpdateDB(strSQL, params);							
						con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtenci�n de informacion
					}	
				
					ps.clearParameters();
					ps.setString(1, regvCE.getString("IC_AFILIADO"));
					ps.setString(2, "I");
					ps.setString(3, regvCE.getString("CG_GRAFICA"));
					ps.setString(4, regvCE.getString("CG_ATRIBUTO"));
					ps.setString(5, regvCE.getString("CG_VALOR"));
					ps.setString(6, "");
					ps.executeUpdate();				
				}
				ps.close();
				
			
			
			

			bOk = true;
		} catch(Throwable e) {
			bOk = false;
			SimpleDateFormat formatoHora = new SimpleDateFormat ("yyyyMMdd_HHmmss");
			String fechaError = formatoHora.format(new java.util.Date());
			mensaje = fechaError + ": ERROR INESPERADO. " + e.getMessage();
			log.error(mensaje, e);
		} finally {
			if (con.hayConexionAbierta()) {
				try {
					if (rs != null) {
						rs.close();
					}
					if (ps != null) {
						ps.close();
					}
				} catch(Exception e) {
					log.error("Error al cerrar recursos",e);
				}
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("obtenerInformacionProceso(s)");
		}
		return "OK" ;
	}

}