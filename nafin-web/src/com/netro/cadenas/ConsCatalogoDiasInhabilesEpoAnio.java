package com.netro.cadenas;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCatalogoDiasInhabilesEpoAnio  {

	private static final Log log = ServiceLocator.getInstance().getLog(ConsCatalogoDiasInhabilesEpoAnio.class);//Variable para enviar mensajes al log.
	//ATRIBUTOS	
	private String claveCadena;
	private List 		conditions;
	private String interclave;
	private String descripcion;
	private String operacion;
	private String modifica;
	private String dia;
	private String interClaves[];
	private String anio;
	
	private String cveCat ="";
	StringBuffer qrySentencia = new StringBuffer("");
	
	public ConsCatalogoDiasInhabilesEpoAnio() {
	}
	
		/**
	 * Metodo que genera la informacion que se mostrara en pantalla
	 * 
	 * @return Registros con los D�as Inh�biles por EPO por A�o
	 */
	public Registros getConsultaData(){
		log.info("getConsultaData(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			StringBuffer condicion  = new StringBuffer("");
			AccesoDB con = new AccesoDB(); 
			Registros registros  = new Registros();
			try{
				con.conexionDB();
				qrySentencia.append(	"	SELECT ic_dia_inhabil AS interclave, "+
											"		TO_CHAR (df_dia_inhabil, 'dd/mm/yyyy') AS clave, "+
											"	 cd_dia_inhabil AS descripcion, cg_razon_social AS epo	"+
											"	FROM comrel_dia_inhabil_x_epo d, comcat_epo e	"+
											"	WHERE d.df_dia_inhabil IS NOT NULL AND d.ic_epo = e.ic_epo AND e.ic_epo = ?	"+
											"	UNION	"+
											"	SELECT ic_dia_inhabil AS interclave,	"+
											"		TO_CHAR (df_dia_inhabil, 'dd/mm/yyyy') AS clave,	"+
											"		cd_dia_inhabil AS descripcion, 'NAFIN' AS epo	"+
											"	FROM comcat_dia_inhabil	"+
											"	WHERE df_dia_inhabil IS NOT NULL	"+
											"	ORDER BY EPO");
	
				conditions.add(claveCadena);
				registros = con.consultarDB(qrySentencia.toString(),conditions);
				con.cierraConexionDB();
			} catch (Exception e) {
				log.error("getConsultaData  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			} 	
		
			log.info("qrySentencia getConsultaData: "+qrySentencia.toString());
			log.info("conditions "+conditions);
			
			log.info("getConsultaData (S) ");
			return registros;
	}
	/**
	 * Metodo que realiza las operaciones de insertar, modificar y eliminar
	 * @return Boolean true si se se completo la acccion, false en caso contario
	 */
	public boolean insertModificarEliminarData(){
		log.info("insertModificarEliminarData(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			AccesoDB con = new AccesoDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			boolean exito = true;
			try{
				con.conexionDB();
				
				
				String TABLA = "comrel_dia_inhabil_x_epo"; 
				String CAMPOS = "cd_dia_inhabil,ic_dia_inhabil,df_dia_inhabil,ic_epo";
				String CONDICLION = "ic_dia_inhabil = ?"; //intercalve
				String UPDATESET = " df_dia_inhabil = to_date( ? ,'dd/mm/yyyy'),cd_dia_inhabil= ? ";
				String VALORES = "?,?,to_date(?,'dd/mm/yyyy'),?";
								
				if(!"".equals(operacion)){
					if("MODIFICAR".equals(operacion)){
						String diaAnterior ="";
						qrySentencia.append( "Select to_char(df_dia_inhabil,'dd/mm/yyyy') from comrel_dia_inhabil_x_epo where ic_dia_inhabil =  ?");//+Cve;interclave
						ps = con.queryPrecompilado(qrySentencia.toString());
						ps.setString(1,interclave);
						rs = ps.executeQuery();
						if(rs.next()){
							diaAnterior =rs.getString(1).trim();
						}
						rs.close();
						ps.close();
						qrySentencia 	= new StringBuffer();
						qrySentencia.append( "SELECT ic_dia_inhabil  FROM comcat_dia_inhabil  WHERE df_dia_inhabil = to_date( ? ,'dd/mm/yyyy') UNION "+ //? = dia
													"SELECT ic_dia_inhabil  FROM comrel_dia_inhabil_x_epo  WHERE df_dia_inhabil = to_date( ? ,'dd/mm/yyyy') and ic_epo = ?");//DIA, CadenasEpos;
						ps = con.queryPrecompilado(qrySentencia.toString());
						ps.setString(1,dia);
						ps.setString(2,dia);
						ps.setString(3,claveCadena);
						rs = ps.executeQuery();	
						if(rs.next() && !diaAnterior.equals(dia)){
							exito=false;
							rs.close();
							ps.close();
							//Mensaje = "EL DIA INHABIL YA EXISTE";
						}else{
							rs.close();
							ps.close();
							qrySentencia 	= new StringBuffer();
							if(!"".equals(TABLA)&&!"".equals(UPDATESET)&&!"".equals(CONDICLION)){
								qrySentencia.append( "UPDATE " + TABLA + " SET " + UPDATESET + " WHERE " + CONDICLION);
							}
							ps = con.queryPrecompilado(qrySentencia.toString());
							ps.setString(1, dia);
							if(descripcion.length() > 25){
								ps.setString(2, descripcion.substring(0,25));	
							}else{
								ps.setString(2, descripcion.substring(0,descripcion.length()));
							}
							ps.setString(3, interclave);
							exito =(ps.executeUpdate()==1)?true:false;
						}
					}
					if("INSERTAR".equals(operacion)){
						String SQLcve = "SELECT ic_dia_inhabil  FROM comcat_dia_inhabil  WHERE df_dia_inhabil = to_date( ? ,'dd/mm/yyyy') UNION "+
											"SELECT ic_dia_inhabil  FROM comrel_dia_inhabil_x_epo  WHERE df_dia_inhabil = to_date( ? ,'dd/mm/yyyy') and ic_epo = ?";// dia, CadenasEpos;
						ps = con.queryPrecompilado(SQLcve);
						ps.setString(1,dia);
						ps.setString(2,dia);
						ps.setString(3,claveCadena);
						rs = ps.executeQuery();
						if (!rs.next()){
							rs.close();
							ps.close();
							if(modifica.equals("T")){
								SQLcve = "Select nvl(max(ic_dia_inhabil),0)+1 as ClaveCatalogo from comrel_dia_inhabil_x_epo";
								ps = con.queryPrecompilado(SQLcve);
								rs = ps.executeQuery();
								if(rs.next()){
									cveCat = rs.getString(1);
								}
								rs.close();
								ps.close();
							}
							
							log.debug("................INSERTAR");
							if(!"".equals(TABLA)&&!"".equals(CAMPOS)&&!"".equals(VALORES)){
								qrySentencia.append("INSERT INTO " + TABLA + "(" + CAMPOS + ") VALUES(" + VALORES + ")");
								System.out.println("El query del insert :: "+qrySentencia.toString());
							}
							ps = con.queryPrecompilado(qrySentencia.toString());
							
							if(descripcion.length() > 25){
								ps.setString(1, descripcion.substring(0,25));	
							}else{
								ps.setString(1, descripcion.substring(0,descripcion.length()));
							}
							ps.setString(2, cveCat);
							ps.setString(3, dia);
							ps.setString(4, claveCadena);
							ps.executeUpdate();
							
						}else{
							exito=false;
							//Mensaje = "EL DIA INHABIL YA EXISTE";
						}
					}
					if("MODIFICAR_ANIO".equals(operacion)){
						log.debug("................Eliminar");
						String diaAnterior ="";
						String diaAux = "";
						for(int i=0; i<interClaves.length; i++){
							qrySentencia = new StringBuffer();
							cveCat = interClaves[i];
							if(cveCat!=null && !cveCat.equals("")){
								String SQLcve = "Select to_char(df_dia_inhabil,'dd/mm/yyyy') from comrel_dia_inhabil_x_epo where ic_dia_inhabil = ?";///+cveCat;
								ps = con.queryPrecompilado(SQLcve);
								ps.setString(1,cveCat);
								rs = ps.executeQuery();
								if(rs.next()){
									diaAnterior =rs.getString(1).trim();
								}
								rs.close();
								ps.close();
								
								diaAux =diaAnterior.substring(0,6)+anio;
								
								CONDICLION = "ic_dia_inhabil= ? ";// + Cve;	
								UPDATESET = "df_dia_inhabil = to_date( ? ,'dd/mm/yyyy')";//'"+ diaAux + "'
								qrySentencia.append("UPDATE " + TABLA + " SET " + UPDATESET + " WHERE " + CONDICLION );
								ps = con.queryPrecompilado(qrySentencia.toString());
								ps.setString(1, diaAux);
								ps.setString(2, cveCat);
								exito =(ps.executeUpdate()==1)?true:false;
								rs.close();
								ps.close();
							}
						}
					}
					if("ELIMINAR".equals(operacion)){
						log.debug("................Eliminar");
						if(!"".equals(TABLA)&&!"".equals(CONDICLION)){
							qrySentencia.append("DELETE  " + TABLA + " WHERE "+ CONDICLION );
							System.out.println("El query del DELETE :: "+qrySentencia.toString());
						}
						ps = con.queryPrecompilado(qrySentencia.toString());
						ps.setString(1, interclave);
						ps.executeUpdate();
					}
				
				}
			} catch (Exception e) {
				log.error("insertModificarEliminarData  Error: " + e);
				e.printStackTrace();
				exito = false;
			} finally {
				if (con.hayConexionAbierta()) {
					con.terminaTransaccion(exito);
					con.cierraConexionDB();
				}
			} 
			log.info("qrySentencia insertModificarEliminarData: "+qrySentencia.toString());
			log.info("conditions "+conditions);
			
			log.info("insertModificarEliminarData (S) ");
			return exito;
	}


	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  


	public void setClaveCadena(String claveCadena) {
		this.claveCadena = claveCadena;
	}


	public String getClaveCadena() {
		return claveCadena;
	}


	public void setInterclave(String interclave) {
		this.interclave = interclave;
	}


	public String getInterclave() {
		return interclave;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}


	public String getOperacion() {
		return operacion;
	}


	public void setModifica(String modifica) {
		this.modifica = modifica;
	}


	public String getModifica() {
		return modifica;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}


	public String getDia() {
		return dia;
	}

	public void setInterClaves(String[] interClaves) {
		this.interClaves = interClaves;
	}


	public String[] getInterClaves() {
		return interClaves;
	}


	public void setAnio(String anio) {
		this.anio = anio;
	}


	public String getAnio() {
		return anio;
	}
}