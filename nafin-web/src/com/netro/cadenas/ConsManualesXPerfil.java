package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsManualesXPerfil implements IQueryGeneratorRegExtJS {
  
	public ConsManualesXPerfil() {  }

	private List conditions;
	StringBuffer query;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsManualesXPerfil.class);//Variable para enviar mensajes al log.
	private String perfil;
	private String clave_epo;
	private String manual;
	private String descripcion;
	


	public String getAggregateCalculationQuery() {
		
		return "";
 	}
		

	public String getDocumentQuery(){
		conditions = new ArrayList();	
		query 		= new StringBuffer();
					
		log.debug("getDocumentQuery)"+query.toString());
		log.debug("getDocumentQuery)"+conditions);
		return query.toString();
 	}
	
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		query 		= new StringBuffer();
		
		log.debug("getDocumentSummaryQueryForIds "+query.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		
						
		return query.toString();
 	}
		
			
	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		query = new StringBuffer();
		
		query.append( "SELECT "+ 
		"	m.ic_manual as IC_MANUAL,  "+
		" m.cc_perfil as PERFIL ,  "+
		" m.cg_nombre as NOMBRE_MANUAL, "+
		" m.cg_descripcion as  DESCRIPCION_MANUAL , "+
		" e.cg_razon_social as NOMBRE_EPO "+
		" From  com_manuales_perfil_epo  m, "+
		" comcat_epo e "+
    " where m.ic_epo = e.ic_epo (+) "+
		" ORDER BY cc_perfil ") ;
		
	
		
		log.debug("getDocumentQueryFile "+query.toString());
		log.debug("getDocumentQueryFile)"+conditions);
	
		return query.toString();
 	} 		
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		String nombreArchivo = "";
		if("PDF".equals(tipo)){
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[]	=	{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual	=	fechaActual.substring(0,2);
				String mesActual	=	meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual	=	fechaActual.substring(6,10);
				String horaActual	=	new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				//session.getAttribute("iNoNafinElectronico").toString(),
				"",
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "------------------------------" +
				horaActual, "formas", ComunesPDF.RIGHT);
				
				pdfDoc.setTable(4, 100);
				pdfDoc.setCell("Perfil","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre del Manual", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Descripci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				
				while (rs.next()){
					String PERFIL = (rs.getString("PERFIL") == null) ? "" : rs.getString("PERFIL");
					String NOMBRE_MANUAL = (rs.getString("NOMBRE_MANUAL") == null) ? "" : rs.getString("NOMBRE_MANUAL");
					String DESCRIPCION_MANUAL = (rs.getString("DESCRIPCION_MANUAL") == null) ? "" : rs.getString("DESCRIPCION_MANUAL");
					String NOMBRE_EPO = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					
					pdfDoc.setCell(PERFIL,"formas",ComunesPDF.JUSTIFIED);
					pdfDoc.setCell(NOMBRE_MANUAL,"formas",ComunesPDF.JUSTIFIED);
					pdfDoc.setCell(DESCRIPCION_MANUAL,"formas",ComunesPDF.JUSTIFIED);
					pdfDoc.setCell(NOMBRE_EPO,"formas",ComunesPDF.JUSTIFIED);
				}							
				pdfDoc.addTable();
				pdfDoc.endDocument();
				
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo");
			}finally {
				try{
					rs.close();
				}catch(Exception e){}
			}
		}
		
	
		
		
		
		
	
		return nombreArchivo;
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	
	public List getConditions() {
		return conditions;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getClave_epo() {
		return clave_epo;
	}

	public void setClave_epo(String clave_epo) {
		this.clave_epo = clave_epo;
	}

	public String getManual() {
		return manual;
	}

	public void setManual(String manual) {
		this.manual = manual;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	
}