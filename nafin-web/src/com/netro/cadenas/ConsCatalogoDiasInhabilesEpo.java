package com.netro.cadenas;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCatalogoDiasInhabilesEpo  {

	private static final Log log = ServiceLocator.getInstance().getLog(ConsCatalogoDiasInhabilesEpo.class);//Variable para enviar mensajes al log.
	//ATRIBUTOS	
	private String claveCadena;
	private List 		conditions;
	private String interclave;
	private String descripcion;
	private String operacion;
	private String modifica;
	private String dia;
	
	private String cveCat ="";
	StringBuffer qrySentencia = new StringBuffer("");	
	
	public ConsCatalogoDiasInhabilesEpo() {
	}
	/**
	 * Metodo que genera la informacion que se mostrara en pantalla
	 * @return Registros
	 */
	public Registros getConsultaData(){
		log.info("getConsultaData(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			StringBuffer condicion  = new StringBuffer("");
			AccesoDB con = new AccesoDB(); 
			Registros registros  = new Registros();
			try{
				con.conexionDB();
				qrySentencia.append(	"	SELECT ic_dia_inhabil AS interclave, cg_dia_inhabil AS clave, "+
											"	 cd_dia_inhabil AS descripcion, cg_razon_social AS epo	"+
											"	FROM comrel_dia_inhabil_x_epo d, comcat_epo e	"+
											"	WHERE d.df_dia_inhabil IS NULL AND d.ic_epo = e.ic_epo AND e.ic_epo = ?	"+
											"	UNION	"+
											"	SELECT ic_dia_inhabil AS interclave, cg_dia_inhabil AS clave,	"+
											"		cd_dia_inhabil AS descript, 'NAFIN' AS epo	"+
											"	FROM comcat_dia_inhabil	"+
											"	WHERE df_dia_inhabil IS NULL	"+
											"	ORDER BY EPO");
	
				conditions.add(claveCadena);
				registros = con.consultarDB(qrySentencia.toString(),conditions);
				con.cierraConexionDB();
			} catch (Exception e) {
				log.error("getConsultaData  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			} 	
		
			log.info("qrySentencia getConsultaData: "+qrySentencia.toString());
			log.info("conditions "+conditions);
			
			log.info("getConsultaData (S) ");
			return registros;
	}
	
	public Registros getDataCadenas(){
		log.info("getDataCadenas(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			StringBuffer condicion  = new StringBuffer("");
			AccesoDB con = new AccesoDB(); 
			Registros registros  = new Registros();
			try{
				con.conexionDB();
				qrySentencia.append(	"	SELECT    DISTINCT (t2.ic_epo_pyme_if) AS clave, "+
											"		t1.cg_razon_social AS descripcion	"+
											"	FROM comcat_epo t1, comrel_nafin t2	"+
											"	WHERE t1.cs_habilitado = 'S' AND t1.ic_epo = t2.ic_epo_pyme_if	"+
											"	ORDER BY t2.ic_epo_pyme_if	");
	
				registros = con.consultarDB(qrySentencia.toString(),conditions);// con.consultarDB(qrySentencia.toString());
				con.cierraConexionDB();
			} catch (Exception e) {
				log.error("getConsultaData  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			} 	
		
			log.info("qrySentencia getDataCadenas: "+qrySentencia.toString());
			log.info("conditions "+conditions);
			
			log.info("getDataCadenas (S) ");
			return registros;
	}	
	/**
	 * Metodo que realiza las operaciones de insertar, modificar y eliminar
	 * @return Boolean true si se se completo la acccion, false en caso contario
	 */
	public boolean insertModificarEliminarData(){
		log.info("insertModificarEliminarData(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			AccesoDB con = new AccesoDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			boolean exito = true;
			try{
				con.conexionDB();
				String TABLA = "comrel_dia_inhabil_x_epo"; 
				String CAMPOS = "cd_dia_inhabil,ic_dia_inhabil,cg_dia_inhabil,ic_epo"; 
				String CONDICLION = "ic_dia_inhabil= ?"; //intercalve
				String UPDATESET = "cg_dia_inhabil= ? ,cd_dia_inhabil= ? "; 
				String VALORES = "?,?,?,?";
								
				if(!"".equals(operacion)){
					if("MODIFICAR".equals(operacion)){
						String diaAnterior ="";
						qrySentencia.append( "Select cg_dia_inhabil from comrel_dia_inhabil_x_epo where df_dia_inhabil is null and ic_dia_inhabil = ?");//+Cve;interclave
						ps = con.queryPrecompilado(qrySentencia.toString());
						ps.setString(1,interclave);
						rs = ps.executeQuery();
						if(rs.next()){
							diaAnterior =rs.getString(1).trim();
						}
						rs.close();
						ps.close();
						qrySentencia 	= new StringBuffer();
						qrySentencia.append( "SELECT ic_dia_inhabil  FROM comcat_dia_inhabil  WHERE df_dia_inhabil is null and cg_dia_inhabil ='"+ dia +"' UNION "+
									"SELECT ic_dia_inhabil  FROM comrel_dia_inhabil_x_epo  WHERE df_dia_inhabil is null and cg_dia_inhabil = ? and ic_epo = ?");//DIA, CadenasEpos;
						ps = con.queryPrecompilado(qrySentencia.toString());
						ps.setString(1,dia);
						ps.setString(2,claveCadena);
						rs = ps.executeQuery();	
						if(rs.next() && !diaAnterior.equals(dia)){
						
							exito=false;
							rs.close();
							ps.close();
							//Mensaje = "EL DIA INHABIL YA EXISTE";
						}else{
							rs.close();
							ps.close();
							qrySentencia 	= new StringBuffer();
							if(!"".equals(TABLA)&&!"".equals(UPDATESET)&&!"".equals(CONDICLION)){
								qrySentencia.append( "UPDATE " + TABLA + " SET " + UPDATESET + " WHERE " + CONDICLION);
							}
							ps = con.queryPrecompilado(qrySentencia.toString());
							ps.setString(1, dia);
							if(descripcion.length() > 25){
								ps.setString(2, descripcion.substring(0,25));	
							}else{
								ps.setString(2, descripcion.substring(0,descripcion.length()));
							}
							ps.setString(3, interclave);
							exito =(ps.executeUpdate()==1)?true:false;
						}
						
					}
					if("INSERTAR".equals(operacion)){
						String SQLcve = "SELECT ic_dia_inhabil  FROM comcat_dia_inhabil  WHERE df_dia_inhabil is null and cg_dia_inhabil = ? UNION "+
							"SELECT ic_dia_inhabil  FROM comrel_dia_inhabil_x_epo  WHERE df_dia_inhabil is null AND cg_dia_inhabil = ? and ic_epo = ? ";
						ps = con.queryPrecompilado(SQLcve);
						ps.setString(1,dia);
						ps.setString(2,dia);
						ps.setString(3,claveCadena);
						rs = ps.executeQuery();
						if (!rs.next()){
							rs.close();
							ps.close();
							if(modifica.equals("T")){
								SQLcve = "Select nvl(max(ic_dia_inhabil),0)+1 as ClaveCatalogo from comrel_dia_inhabil_x_epo";
								ps = con.queryPrecompilado(SQLcve);
								rs = ps.executeQuery();
								if(rs.next()){
									cveCat = rs.getString(1);
								}
								rs.close();
								ps.close();
							}
							
							log.debug("................INSERTAR");
							if(!"".equals(TABLA)&&!"".equals(CAMPOS)&&!"".equals(VALORES)){
								qrySentencia.append("INSERT INTO " + TABLA + "(" + CAMPOS + ") VALUES(" + VALORES + ")");
								System.out.println("El query del insert :: "+qrySentencia.toString());
							}
							ps = con.queryPrecompilado(qrySentencia.toString());
							
							if(descripcion.length() > 25){
								ps.setString(1, descripcion.substring(0,25));	
							}else{
								ps.setString(1, descripcion.substring(0,descripcion.length()));
							}
							ps.setString(2, cveCat);
							ps.setString(3, dia);
							ps.setString(4, claveCadena);
							ps.executeUpdate();
							
						}else{
							exito=false;
							//Mensaje = "EL DIA INHABIL YA EXISTE";
						}
					}
					if("ELIMINAR".equals(operacion)){
						log.debug("................Eliminar");
						if(!"".equals(TABLA)){
							qrySentencia.append("DELETE  " + TABLA + " WHERE ic_dia_inhabil in("+interclave+")" );
							System.out.println("El query del DELETE :: "+qrySentencia.toString());
							System.out.println("CONDICLION :: "+CONDICLION);
						}
						ps = con.queryPrecompilado(qrySentencia.toString());
						//ps.setString(1, interclave);
						ps.executeUpdate();
						ps.close();	    
					}
				
				}
			} catch (Exception e) {
				log.error("insertModificarEliminarData  Error: " + e);
				e.printStackTrace();
				exito = false;
			} finally {
				if (con.hayConexionAbierta()) {
					con.terminaTransaccion(exito);
					con.cierraConexionDB();
				}
			} 
			log.info("qrySentencia insertModificarEliminarData: "+qrySentencia.toString());
			log.info("conditions "+conditions);
			
			log.info("insertModificarEliminarData (S) ");
			return exito;
	}


	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  


	public void setClaveCadena(String claveCadena) {
		this.claveCadena = claveCadena;
	}


	public String getClaveCadena() {
		return claveCadena;
	}


	public void setInterclave(String interclave) {
		this.interclave = interclave;
	}


	public String getInterclave() {
		return interclave;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}


	public String getOperacion() {
		return operacion;
	}


	public void setModifica(String modifica) {
		this.modifica = modifica;
	}


	public String getModifica() {
		return modifica;
	}

	public void setDia(String dia) {
		this.dia = dia;
	}


	public String getDia() {
		return dia;
	}
}