package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsSeleccionIF implements IQueryGeneratorRegExtJS {
	public ConsSeleccionIF() {  }
	
	private List conditions;
	StringBuffer strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsSeleccionIF.class);
	
	private String clavePyme;
	
	public String getAggregateCalculationQuery(){
		return "";
	}
	
	public String getDocumentQuery() {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentQueryFile(){
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		
		strQuery.append("SELECT CE.IC_EPO, CE.CG_RAZON_SOCIAL AS RAZON_SOCIAL,CI.CG_RAZON_SOCIAL,CM.CD_NOMBRE, CB.CG_BANCO, CB.CG_NUMERO_CUENTA," + 
		                 " CB.CG_SUCURSAL, PI.CS_VOBO_IF" +
							" FROM  COMREL_CUENTA_BANCARIA 	CB," +
									" COMREL_PYME_IF 				PI," +
									" COMREL_PYME_EPO 			PE," +
									" COMCAT_IF 					CI," +
									" COMCAT_EPO 					CE," +
									" COMCAT_MONEDA            CM" +
	// relacion de tablas	
							" WHERE CB.IC_CUENTA_BANCARIA = PI.IC_CUENTA_BANCARIA" +
									" AND   CB.IC_PYME		= PE.IC_PYME" +         
									" AND   PI.IC_IF			= CI.IC_IF" +             
									" AND   PI.IC_EPO			= CE.IC_EPO" +           
									" AND   PI.IC_EPO			= PE.IC_EPO" +            
									" AND   CB.IC_MONEDA		= CM.IC_MONEDA" +       
	// parametros dados
									" AND   CB.IC_PYME            =  ?" +
	// filtros
									" AND   CB.CS_BORRADO         = 'N'" +
									" AND   PE.CS_HABILITADO      = 'S'" +
									" AND   PI.CS_BORRADO         = 'N'" +
									" AND   PI.CS_OPERA_DESCUENTO = 'S'" +
									" AND   CE.cs_habilitado      = 'S'" +
									" ORDER BY RAZON_SOCIAL");
		conditions.add(this.clavePyme);
		
		log.debug("getDocumentQueryFile " + strQuery.toString());
		log.debug("getDocumentQueryFile " + conditions);
		
		return strQuery.toString();
	}
	
	/** M�todo en el cual se debe realizar la implementaci�n de la generaci�n del archivo
	 * en base al resulset que se recibe como par�metro. El ResulSet se obtiene de la consulta
	 * generada al invocar getDocumentQueryFile()
	 * @param request HttRequest empleado principalmente para obtener el onjeto session
	 * @param rs ResulSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta f�sica donde se generar� el archivo
	 * @param tipo cadena que identifica el tipo o variante de archivo que va a generar
	 * @return Cadena con la ruta del archivo generado.
	 */
	 public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)	{
		String nombreArchivo = "";
		
		if("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a" + diaActual + " de " + mesActual + " del " + anioActual + "------------------------------" +
				horaActual, "formas", ComunesPDF.RIGHT);
				pdfDoc.setTable(7,100);
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Instituci�n Financiera","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Banco de Servicio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. de Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Sucursal","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Autorizaci�n","celda01",ComunesPDF.CENTER);
				
				while (rs.next()){
					String RAZON_SOCIAL = (rs.getString("RAZON_SOCIAL")==null)? "" : rs.getString("RAZON_SOCIAL");
					String CG_RAZON_SOCIAL = (rs.getString("CG_RAZON_SOCIAL")==null) ? "" : rs.getString("CG_RAZON_SOCIAL");
					String CD_NOMBRE = (rs.getString("CD_NOMBRE")==null) ? "" : rs.getString("CD_NOMBRE");
					String CG_BANCO = (rs.getString("CG_BANCO")==null) ? "" : rs.getString("CG_BANCO");
					String CG_NUMERO_CUENTA = (rs.getString("CG_NUMERO_CUENTA")==null) ? "" : rs.getString("CG_NUMERO_CUENTA");
					String CG_SUCURSAL = (rs.getString("CG_SUCURSAL")==null) ? "" : rs.getString("CG_SUCURSAL");
					String CS_VOBO_IF = (rs.getString("CS_VOBO_IF")==null) ? "" : rs.getString("CS_VOBO_IF");
					
					pdfDoc.setCell(RAZON_SOCIAL,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CG_RAZON_SOCIAL,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CD_NOMBRE,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CG_BANCO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CG_NUMERO_CUENTA,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CG_SUCURSAL,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CS_VOBO_IF,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
				}catch(Throwable e){
					throw new AppException("Error al generar el archivo",e);
				}finally {
					try{
						rs.close();
					}catch(Exception e){}
				}
			}
			return nombreArchivo;
	 }
	 /** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	  * con base en el objeto registros que recibe como par�metro.
	  * @param request HttRequest empleado principalmente para obtener el objeto session
	  * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	  * @param path Ruta donde se generar� el archivo
	  * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	  * @return Cadena con la ruta del archivo generado
	  */
	  public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		  return "";
	  }
	  
	  public List getConditions() 
	  {
			return conditions;
	  }
	  
	  public String getClavePyme()
	  {
		  return clavePyme;
	  }
	  public void setClavePyme (String clavePyme)
	  {
		 this.clavePyme=clavePyme; 
	  }
}