package com.netro.cadenas;

import com.netro.exception.NafinException;
import com.netro.xls.ComunesXLS;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Registros;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * clase para la pantalla
ADMIN IF  / Administración/Afiliados/Bitácora  de Actualización de Datos
ADMIN NAFIN / Administración/Afiliados/Bitácora  de Actualización de Datos
 */
public class ConsBitaActualizacionDatos implements IQueryGeneratorRegExtJS {


    private static final Log LOG = LogFactory.getLog(ConsBitaActualizacionDatos.class);
    private String paginaOffset;
    private String paginaNo;
    private List conditions;
    StringBuilder qrySentencia = new StringBuilder("");
    private String claveIf;
    private String nafinElectronico;    
    private String fechaNotifDe;
    private String fechaNotifA;
    private String fechaEnteradoIni;
    private String fechaEnteradoFinal;    
    private String fechaModifDe;
    private String fechaModifA;    
    private String icTramite;
    private String rfc;
    private String tipoConsulta;
    private String icBitAfiliados;
   

    public ConsBitaActualizacionDatos() {

    }

    /**
     * @return
     */
    public String getAggregateCalculationQuery() {       
        return qrySentencia.toString();
    }

    /**
     * @return
     */
    public String getDocumentQuery() {
        
        return qrySentencia.toString();
    }


    /**
     * @param pageIds
     * @return
     */
    public String getDocumentSummaryQueryForIds(List pageIds) {
        
        return qrySentencia.toString();
    }

    /**
     * @return
     */
    public String getDocumentQueryFile() {
        LOG.info("getDocumentQueryFile(E)");
        qrySentencia = new StringBuilder();
        conditions = new ArrayList();

        if ("Consulta".equals(tipoConsulta) ||  "GenerarArchivo".equals(tipoConsulta)  ) {
            
            qrySentencia.append("   SELECT    b.IC_BIT_AFILIADOS  as IC_BIT_AFILIADOS,       b.IC_NAFIN_ELECTRONICO  as NA_ELECTRONICO,  "+
                                "    p.CG_RAZON_SOCIAL as RAZON_SOCIAL  ,     p.CG_RFC as RFC,     b.CG_TRAMITE as TRAMITE ," +
                                "    to_char(b.DF_MODIFICACION , 'DD/MM/YYYY HH24:MI:SS') AS DF_MODIFICACION  "+         
                                "    FROM   COM_BIT_AFILIADOS b,   COMCAT_PYME p   where b.IC_PYME = p.IC_PYME   ");    
    
            if (!"".equals(nafinElectronico)) {
                qrySentencia.append("  and b.IC_NAFIN_ELECTRONICO = ?   ");
                this.conditions.add(nafinElectronico);
            }            
            if (!"".equals(rfc)) {
                qrySentencia.append("  and p.CG_RFC = ?   ");
                this.conditions.add(rfc);
            }            
            if (!"".equals(fechaModifDe)  &&  !"".equals(fechaModifA) ) {           
                qrySentencia.append(" AND b.DF_MODIFICACION >= trunc(TO_DATE(?,'DD/MM/YYYY'))  AND b.DF_MODIFICACION < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");  
                this.conditions.add(fechaModifDe);
                this.conditions.add(fechaModifA);
            }      
            
            if (!"".equals(icTramite)) {
                qrySentencia.append("  and b.CG_TRAMITE  LIKE '%"+icTramite+"%'");
            }           
            
            if (!"".equals(claveIf)   &&  "".equals(fechaEnteradoIni) &&  "".equals(fechaEnteradoFinal) && "".equals(fechaNotifDe)  && "".equals(fechaNotifA)  ) {
                qrySentencia.append(" AND  b.IC_BIT_AFILIADOS IN (  "+
                                    " SELECT DISTINCT IC_BIT_AFILIADOS  FROM  ( " + 
                                    " SELECT IC_BIT_AFILIADOS FROM COM_BIT_ENTERADOS WHERE IC_IF =  ?  " +               
                                    " UNION ALL "+
                                    " SELECT IC_BIT_AFILIADOS FROM COM_BIT_ENVIADOSIF  WHERE IC_IF =  ?   ) "+
                                    " ) ");
                this.conditions.add(claveIf);
                this.conditions.add(claveIf);
            }             
            if (!"".equals(claveIf)  &&  !"".equals(fechaEnteradoIni)  &&  !"".equals(fechaEnteradoFinal) && "".equals(fechaNotifDe) && "".equals(fechaNotifA) ) {
                    qrySentencia.append(" AND  b.IC_BIT_AFILIADOS IN (  "+
                                        " SELECT DISTINCT IC_BIT_AFILIADOS  FROM  ( " + 
                                        " SELECT IC_BIT_AFILIADOS FROM COM_BIT_ENTERADOS WHERE IC_IF =  ?  " + 
                                        " AND DF_ENTERADO >= trunc(TO_DATE(? ,'DD/MM/YYYY'))  "+
                                        " AND DF_ENTERADO < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1  ) "+
                                        " ) ");
                    this.conditions.add(claveIf);
                    this.conditions.add(fechaEnteradoIni);
                    this.conditions.add(fechaEnteradoFinal);                 
                }                
                if (!"".equals(claveIf) &&  "".equals(fechaEnteradoIni)   &&  "".equals(fechaEnteradoFinal) && !"".equals(fechaNotifDe) && !"".equals(fechaNotifA) ) {
                    qrySentencia.append(" AND  b.IC_BIT_AFILIADOS IN (  "+
                                        " SELECT DISTINCT IC_BIT_AFILIADOS  FROM  ( " + 
                                        " SELECT IC_BIT_AFILIADOS FROM COM_BIT_ENVIADOSIF  WHERE IC_IF =  ?   "+
                                        " AND DF_ENVIO_EMAIL >= trunc(TO_DATE(? ,'DD/MM/YYYY'))  "+
                                        " AND DF_ENVIO_EMAIL < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1  ) "+
                                        " ) ");
                    this.conditions.add(claveIf);                   
                    this.conditions.add(fechaNotifDe);
                    this.conditions.add(fechaNotifA);
                }
                if (!"".equals(claveIf) &&  !"".equals(fechaEnteradoIni)  &&  !"".equals(fechaEnteradoFinal) && !"".equals(fechaNotifDe) && !"".equals(fechaNotifA) ) {
                    
                    qrySentencia.append(" AND  b.IC_BIT_AFILIADOS IN (  "+
                                        " SELECT DISTINCT IC_BIT_AFILIADOS  FROM  ( " + 
                                        " SELECT IC_BIT_AFILIADOS FROM COM_BIT_ENTERADOS WHERE IC_IF =  ?  " + 
                                        " AND DF_ENTERADO >= trunc(TO_DATE(? ,'DD/MM/YYYY'))  "+
                                        " AND DF_ENTERADO < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1 " +                                        
                                        " UNION ALL "+
                                        " SELECT IC_BIT_AFILIADOS FROM COM_BIT_ENVIADOSIF  WHERE IC_IF =  ?   "+
                                        " AND DF_ENVIO_EMAIL >= trunc(TO_DATE(? ,'DD/MM/YYYY'))  "+
                                        " AND DF_ENVIO_EMAIL < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1  ) "+
                                        " ) ");
                    this.conditions.add(claveIf);
                    this.conditions.add(fechaEnteradoIni);
                    this.conditions.add(fechaEnteradoFinal);
                    this.conditions.add(claveIf);
                    this.conditions.add(fechaNotifDe);
                    this.conditions.add(fechaNotifA);
                }                
                if ("".equals(claveIf) &&  !"".equals(fechaEnteradoIni)  &&  !"".equals(fechaEnteradoFinal) && "".equals(fechaNotifDe)  && "".equals(fechaNotifA) ) {
                    qrySentencia.append(" AND  b.IC_BIT_AFILIADOS IN (  "+
                                        " SELECT DISTINCT IC_BIT_AFILIADOS  FROM  ( " + 
                                        " SELECT IC_BIT_AFILIADOS FROM COM_BIT_ENTERADOS  " + 
                                        " WHERE DF_ENTERADO >= trunc(TO_DATE(? ,'DD/MM/YYYY'))  "+
                                        " AND DF_ENTERADO < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1  ) "+
                                        " ) ");                    
                    this.conditions.add(fechaEnteradoIni);
                    this.conditions.add(fechaEnteradoFinal);
                }
                
                if ("".equals(claveIf) &&  "".equals(fechaEnteradoIni)  &&  "".equals(fechaEnteradoFinal) && !"".equals(fechaNotifDe) &&   !"".equals(fechaNotifA)  ) {
                    
                    qrySentencia.append(" AND  b.IC_BIT_AFILIADOS IN (  "+
                                        " SELECT DISTINCT IC_BIT_AFILIADOS  FROM  ( " + 
                                        " SELECT IC_BIT_AFILIADOS FROM COM_BIT_ENVIADOSIF    "+
                                        " WHERE DF_ENVIO_EMAIL >= trunc(TO_DATE(? ,'DD/MM/YYYY'))  "+
                                        " AND DF_ENVIO_EMAIL < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1  ) "+
                                        " ) ");
                    this.conditions.add(fechaNotifDe);
                    this.conditions.add(fechaNotifA);
                }
                            
                if ("".equals(claveIf) &&  !"".equals(fechaEnteradoIni)  &&  !"".equals(fechaEnteradoFinal) && !"".equals(fechaNotifDe)  && !"".equals(fechaNotifA)  ) {
                    
                    qrySentencia.append(" AND  b.IC_BIT_AFILIADOS IN (  "+
                                        " SELECT DISTINCT IC_BIT_AFILIADOS  FROM  ( " + 
                                        " SELECT IC_BIT_AFILIADOS FROM COM_BIT_ENTERADOS   " + 
                                        " WHERE DF_ENTERADO >= trunc(TO_DATE(? ,'DD/MM/YYYY'))  AND DF_ENTERADO < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1 " +                                        
                                        " UNION ALL "+
                                        " SELECT IC_BIT_AFILIADOS FROM COM_BIT_ENVIADOSIF     "+
                                        " WHERE DF_ENVIO_EMAIL >= trunc(TO_DATE(? ,'DD/MM/YYYY'))  AND DF_ENVIO_EMAIL < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1  )  ) ");
                    this.conditions.add(fechaEnteradoIni);
                    this.conditions.add(fechaEnteradoFinal);                    
                    this.conditions.add(fechaNotifDe);
                    this.conditions.add(fechaNotifA);
                }           
                            
           qrySentencia.append(" order by b.ic_bit_afiliados asc  ");
             
        }else  if ("Cons_Enterado".equals(tipoConsulta)) {
            
            qrySentencia.append(" select i.cg_razon_social AS NOMBRE_INTERMEDIARIO ,  b.IC_USUARIO AS  USUARIO ,  '' AS NOMBRE_USUARIO , "+
                                " to_char(b.DF_ENTERADO , 'DD/MM/YYYY') AS FECHA_ENTERADO ,  b.HORA_ENTERADO  AS HORA_ENTERADO "+                                
                                " from COM_BIT_ENTERADOS b, comcat_if  i  WHERE  b.ic_if = i.ic_if   and b.IC_BIT_AFILIADOS  = ?  ");  
            this.conditions.add(icBitAfiliados);            
            if (!"".equals(claveIf)) {
                qrySentencia.append(" and b.ic_if = ? ");
                this.conditions.add(claveIf);
            }            
            if (  !"".equals(fechaEnteradoIni) &&   !"".equals(fechaEnteradoFinal) ) {
                qrySentencia.append(" AND b.DF_ENTERADO >= trunc(TO_DATE(? ,'DD/MM/YYYY'))  AND b.DF_ENTERADO < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1 "  );
                this.conditions.add(fechaEnteradoIni);
                this.conditions.add(fechaEnteradoFinal);
            }       
            
            qrySentencia.append(" order by i.cg_razon_social asc ");
        
        }else  if ("Cons_EnvioEmail".equals(tipoConsulta)) {            
            
            qrySentencia.append("  select i.cg_razon_social AS NOMBRE_INTERMEDIARIO  ,  to_char(b.DF_ENVIO_EMAIL , 'DD/MM/YYYY HH24:MI:SS') AS FECHA_NOTIFICACION,  "+
                                "  DECODE (b.DF_ENVIO_EMAIL, '', to_char(b.DF_ALTA , 'DD/MM/YYYY HH24:MI:SS'), '' ) ||' '|| b.CG_OBSERVACIONES as OBSERVACIONES  "+
                                " from  COM_BIT_ENVIADOSIF b,  comcat_if i  where b.ic_if = i.ic_if   and  b.IC_BIT_AFILIADOS = ? ");            
            this.conditions.add(icBitAfiliados);      
            
            if (!"".equals(claveIf)) {
                qrySentencia.append(" and b.ic_if = ? ");
                this.conditions.add(claveIf);
            } 
                        
            if (  !"".equals(fechaNotifDe) &&   !"".equals(fechaNotifA) ) {
                qrySentencia.append(" AND b.DF_ENVIO_EMAIL >= trunc(TO_DATE(? ,'DD/MM/YYYY'))   AND b.DF_ENVIO_EMAIL < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1 "  );
                this.conditions.add(fechaNotifDe);
                this.conditions.add(fechaNotifA);
            } 
            
             qrySentencia.append(" order by i.cg_razon_social asc ");
        }       
        
        LOG.info("qrySentencia  " + qrySentencia + " \n  conditions " + conditions);
        LOG.info("getDocumentQueryFile(S)");
        return qrySentencia.toString();
    }

    /**
     * @param request
     * @param rs
     * @param path
     * @param tipo
     * @return
     */
    public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path,
                                      String tipo) {       
        return "";

    }

    /**
     * @param request
     * @param rs
     * @param path
     * @param tipo
     * @return
     */
    public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
        LOG.info("crearCustomFile (E)");
        String nombreArchivo = "";
        try {
            CreaArchivo archivo = new CreaArchivo();
            nombreArchivo = archivo.nombreArchivo()+".xls";
            ComunesXLS xlsDoc = new ComunesXLS(path + nombreArchivo, "Bitacora");            
            xlsDoc.setTabla(5);
            xlsDoc.setCelda("NE",   "formasb", ComunesXLS.CENTER);
            xlsDoc.setCelda("RFC",  "formasb", ComunesXLS.CENTER);
            xlsDoc.setCelda("Razón Social",   "formasb", ComunesXLS.CENTER);
            xlsDoc.setCelda("Trámite",  "formasb", ComunesXLS.CENTER);
            xlsDoc.setCelda("Fecha y hora de modificación", "formasb", ComunesXLS.CENTER);   
            while (rs.next()) {
                String ictramites = (rs.getString("TRAMITE") == null) ? "" : rs.getString("TRAMITE");                
                String tramite =  this.datosTramite(ictramites, "Archivo");                
                xlsDoc.setCelda((rs.getString("NA_ELECTRONICO") == null) ? "" : rs.getString("NA_ELECTRONICO").replace(',', ' ') );
                xlsDoc.setCelda((rs.getString("RFC") == null) ? "" : rs.getString("RFC").replace(',', ' ') + ",");
                xlsDoc.setCelda((rs.getString("RAZON_SOCIAL") == null) ? "" : rs.getString("RAZON_SOCIAL").replace(',', ' ') );
                xlsDoc.setCelda(tramite.replace(',', ' ') );
                xlsDoc.setCelda((rs.getString("DF_MODIFICACION") == null) ? "" : rs.getString("DF_MODIFICACION").replace(',', ' '));                
            }
            xlsDoc.cierraTabla();   
            
            Registros  regEnt = getDetalleRegistros ("Cons_Enterado");            
            if(regEnt.getNumeroRegistros()>0){                
                xlsDoc.creaHoja("If's Enterados");
                xlsDoc.setTabla(6);
                xlsDoc.setCelda("RFC",  "formasb", ComunesXLS.CENTER);
                xlsDoc.setCelda("Fecha y hora de modificación",  "formasb", ComunesXLS.CENTER);
                xlsDoc.setCelda("Nombre del Intermediario",  "formasb", ComunesXLS.CENTER);
                xlsDoc.setCelda("Usuario",  "formasb", ComunesXLS.CENTER);
                xlsDoc.setCelda("Nombre Usuario",  "formasb", ComunesXLS.CENTER);                
                xlsDoc.setCelda("Fecha y hora de enterado  ",  "formasb", ComunesXLS.CENTER);
                    
                while (regEnt.next()){
                    String idUsuario = (regEnt.getString("USUARIO") == null) ? "" : regEnt.getString("USUARIO");                    
                    UtilUsr usuario = new UtilUsr();
                    Usuario user = new Usuario(); 
                    user = usuario.getUsuario(idUsuario);                   
                    String  nombreUsuario =user.getNombreCompleto().replaceAll(",","");
                    
                    xlsDoc.setCelda((regEnt.getString("RFC") == null) ? "" : regEnt.getString("RFC") );
                    xlsDoc.setCelda((regEnt.getString("FECHA_MODIFICACION") == null) ? "" : regEnt.getString("FECHA_MODIFICACION") );
                    xlsDoc.setCelda((regEnt.getString("NOMBRE_INTERMEDIARIO") == null) ? "" : regEnt.getString("NOMBRE_INTERMEDIARIO").replaceAll(",","") );
                    xlsDoc.setCelda(idUsuario );
                    xlsDoc.setCelda( nombreUsuario);                    
                    String fecha = (regEnt.getString("FECHA_ENTERADO") == null) ? "" : regEnt.getString("FECHA_ENTERADO");
                    String hora = (regEnt.getString("HORA_ENTERADO") == null) ? "" : regEnt.getString("HORA_ENTERADO");                    
                    xlsDoc.setCelda(fecha+"  "+hora );
                                    
                }
                xlsDoc.cierraTabla();                 
            }                        
            Registros  regNoti = getDetalleRegistros ("Cons_EnvioEmail");
            if(regNoti.getNumeroRegistros()>0){
             
                xlsDoc.creaHoja("If's Notificados");
                xlsDoc.setTabla(5);
                xlsDoc.setCelda("RFC",  "formasb", ComunesXLS.CENTER);
                xlsDoc.setCelda("Fecha y hora de modificación",  "formasb", ComunesXLS.CENTER);
                xlsDoc.setCelda("Nombre del Intermediario",  "formasb", ComunesXLS.CENTER);
                xlsDoc.setCelda("Fecha y hora de notificación",  "formasb", ComunesXLS.CENTER);
                xlsDoc.setCelda("Observaciones",  "formasb", ComunesXLS.CENTER);                
                                
                while (regNoti.next()){                    
                    String observaciones = (regNoti.getString("OBSERVACIONES") == null) ? "N/A" : regNoti.getString("OBSERVACIONES");
                    String fechaNotificacion= (regNoti.getString("FECHA_NOTIFICACION") == null) ? "" : regNoti.getString("FECHA_NOTIFICACION");                    
                    if (!"".equals(fechaNotificacion)  ) {
                        observaciones = "N/A";
                    }                    
                    xlsDoc.setCelda((regNoti.getString("RFC") == null) ? "" : regNoti.getString("RFC") );
                    xlsDoc.setCelda((regNoti.getString("FECHA_MODIFICACION") == null) ? "" : regNoti.getString("FECHA_MODIFICACION") );
                    xlsDoc.setCelda((regNoti.getString("NOMBRE_INTERMEDIARIO") == null) ? "" : regNoti.getString("NOMBRE_INTERMEDIARIO").replace(',', ' ') );
                    xlsDoc.setCelda((regNoti.getString("FECHA_NOTIFICACION") == null) ? "" : regNoti.getString("FECHA_NOTIFICACION") );
                    xlsDoc.setCelda(observaciones.replace(',', ' ') );                                   
                }
                xlsDoc.cierraTabla();
            }                                                          
                                                                               
            xlsDoc.cierraXLS();
            
        } catch (Throwable e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new AppException("Error al generar el archivo ", e);
        } finally {           
            LOG.info("crearCustomFile (S)");
        }
        return nombreArchivo;
    }


    /**
     * metodo que se obtiene los tramites que se realizaron en la modificación del proveedor 
     * @param icTramites
     * @param tipoCOns
     * @return
     * @throws NafinException
     */
    public String  datosTramite (String icTramites, String tipoCOns ) throws NafinException {
        LOG.info("datosTramite (e)");
        
        AccesoDB con = new AccesoDB();       
        StringBuilder qrySQL = new StringBuilder();       
        StringBuilder datosTramite = new StringBuilder(); 
        List varBind = new ArrayList();
        
        try {

            con.conexionDB();
           
            String tramites =  icTramites.replace("|", ",");
            
            qrySQL = new StringBuilder();             
            qrySQL.append(" select cg_tramite as TRAMITE "+
                                " from COMCAT_TRAMITE " + 
                                " where ic_tramite in ( " +tramites +" )");
            
            LOG.debug ("qrySQL  "+qrySQL.toString() +" varBind    \n "+varBind);
            
            PreparedStatement  ps = con.queryPrecompilado(qrySQL.toString(), varBind);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String tramite =  rs.getString("TRAMITE") == null ? "" : rs.getString("TRAMITE"); 
                
                if ("Grid".equals(tipoCOns)  ) {
                    datosTramite.append(datosTramite.length() > 0 ? "<br> " : "").append(tramite);
                }else  if ("Archivo".equals(tipoCOns)  ) {
                    datosTramite.append(datosTramite.length() > 0 ? "/ " : "").append(tramite);  
                }
            }
            rs.close();
            ps.close();
         
            
        } catch (Exception e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        LOG.info("datosTramite (s)");
        return datosTramite.toString();
    }
    
    
    /**
     * metodo para sacar el detalle que se mostrara en el archivo 
     * @param tipoConsultaDet   
     * @return
     * @throws NafinException
     */
    public Registros  getDetalleRegistros (String tipoConsultaDet) throws NafinException {
        LOG.info("getDetalleRegistros (e)");
        
        AccesoDB con = new AccesoDB();       
        StringBuilder qrySQL = new StringBuilder();     
        List varBind = new ArrayList();
        Registros registros = null;
        try {

            con.conexionDB();            
            
            if ("Cons_Enterado".equals(tipoConsultaDet)) {
                
                qrySQL.append("  select " + 
                                    " p.cg_rfc as RFC,  to_char( b.df_modificacion , 'DD/MM/YYYY HH24:MI:SS') AS FECHA_MODIFICACION , " + 
                                    " i.cg_razon_social AS NOMBRE_INTERMEDIARIO ,  e.IC_USUARIO AS  USUARIO ,  " + 
                                    " '' AS NOMBRE_USUARIO ,  to_char(e.DF_ENTERADO , 'DD/MM/YYYY') AS FECHA_ENTERADO , " + 
                                    " e.HORA_ENTERADO  AS HORA_ENTERADO " + 
                                    " from COM_BIT_ENTERADOS e, " + 
                                    " COM_BIT_AFILIADOS b, " + 
                                    " comcat_if  i , " + 
                                    " comcat_pyme p " + 
                                    " WHERE  e.ic_if = i.ic_if   " + 
                                    " and b.ic_bit_afiliados = e.ic_bit_afiliados " + 
                                    " and b.ic_pyme = p.ic_pyme ");                   
                           
                if (!"".equals(claveIf)) {
                    qrySQL.append(" and e.ic_if = ? ");
                    varBind.add(claveIf);
                }
                         
                if (  !"".equals(fechaEnteradoIni) &&   !"".equals(fechaEnteradoFinal) ) {
                    qrySQL.append(" AND e.DF_ENTERADO >= trunc(TO_DATE(? ,'DD/MM/YYYY'))  "+
                                        " AND e.DF_ENTERADO < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1 "  );
                    varBind.add(fechaEnteradoIni);
                    varBind.add(fechaEnteradoFinal);
                }
                              
                
            }else if ("Cons_EnvioEmail".equals(tipoConsultaDet)) {       
                        
                qrySQL.append("   select " + 
                                    " p.cg_rfc as RFC,  to_char(b.df_modificacion , 'DD/MM/YYYY HH24:MI:SS')  as FECHA_MODIFICACION," + 
                                    " i.cg_razon_social AS NOMBRE_INTERMEDIARIO  ,  to_char(e.DF_ENVIO_EMAIL , 'DD/MM/YYYY HH24:MI:SS') AS FECHA_NOTIFICACION,  " + 
                                    "  DECODE (e.DF_ENVIO_EMAIL, '', to_char(e.DF_ALTA , 'DD/MM/YYYY HH24:MI:SS'), '' ) ||' '||  e.CG_OBSERVACIONES as OBSERVACIONES " + 
                                    " from  COM_BIT_ENVIADOSIF e,  " + 
                                    " COM_BIT_AFILIADOS b , " + 
                                    " comcat_pyme p, " + 
                                    " comcat_if i  " + 
                                    " where e.ic_if = i.ic_if   " + 
                                    " and b.ic_bit_afiliados =    e.ic_bit_afiliados " + 
                                    " and b.ic_pyme = p.ic_pyme   ");
                            
                            
                if (!"".equals(claveIf)) {
                    qrySQL.append(" and e.ic_if = ? ");
                    varBind.add(claveIf);
                } 
                
                if (  !"".equals(fechaNotifDe) &&   !"".equals(fechaNotifA) ) {
                    qrySQL.append(" AND e.DF_ENVIO_EMAIL >= trunc(TO_DATE(? ,'DD/MM/YYYY'))  "+
                                        " AND e.DF_ENVIO_EMAIL < trunc(TO_DATE(? ,'DD/MM/YYYY')) + 1 "  );
                    varBind.add(fechaNotifDe);
                    varBind.add(fechaNotifA);
                }                          
            }  
            
            
            if (!"".equals(nafinElectronico)) {
                qrySQL.append("  and b.IC_NAFIN_ELECTRONICO = ?   ");
                varBind.add(nafinElectronico);
            }
            
            if (!"".equals(rfc)) {
                qrySQL.append("  and p.CG_RFC = ?   ");
                varBind.add(rfc);
            }
            
            if (!"".equals(fechaModifDe)  &&  !"".equals(fechaModifA) ) {           
                qrySQL.append(" AND b.DF_MODIFICACION >= trunc(TO_DATE(?,'DD/MM/YYYY'))  AND b.DF_MODIFICACION < trunc(TO_DATE(?,'DD/MM/YYYY')) + 1 ");  
                varBind.add(fechaModifDe);
                varBind.add(fechaModifA);
            }
            
            if (!"".equals(icTramite)) {
                qrySQL.append("  and b.CG_TRAMITE  LIKE '%"+icTramite+"%'");
            }
            
            
           qrySQL.append(" order by b.ic_bit_afiliados, b.ic_pyme  asc   ");
            
            LOG.debug ("qrySQL  "+qrySQL.toString() +" varBind    \n "+varBind);
            
            registros = con.consultarDB(qrySQL.toString(), varBind, false);
                     
            
        } catch (Exception e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        LOG.info("getDetalleRegistros (s)");
        
        return registros;
    }
    
    

    /**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones
     */
    public List getConditions() {
        return conditions;
    }

    /**
     * Obtiene el numero de pagina.
     * @return Cadena con el numero de pagina
     */
    public String getPaginaNo() {
        return paginaNo;
    }

    /**
     * Obtiene el offset de la pagina.
     * @return Cadena con el offset de pagina
     */
    public String getPaginaOffset() {
        return paginaOffset;
    }


    /*****************************************************
					 SETTERS
*******************************************************/

    /**
     * Establece el numero de pagina.
     * @param  newPaginaNo Cadena con el numero de pagina
     */
    public void setPaginaNo(String newPaginaNo) {
        paginaNo = newPaginaNo;
    }

    /**
     * Establece el offset de la pagina.
     * @param newPaginaOffset Cadena con el offset de pagina
     */
    public void setPaginaOffset(String paginaOffset) {
        this.paginaOffset = paginaOffset;
    }

    /**
     * @param claveIf
     */
    public void setClaveIf(String claveIf) {
        this.claveIf = claveIf;
    }

    /**
     * @return
     */
    public String getClaveIf() {
        return claveIf;
    }
    
    /**
     * @param icTramite
     */
    public void setIcTramite(String icTramite) {
        this.icTramite = icTramite;
    }

    /**    
     * @return
     */
    public String getIcTramite() {
        return icTramite;
    }
    
    /**
     * @param nafinElectronico
     */
    public void setNafinElectronico(String nafinElectronico) {
        this.nafinElectronico = nafinElectronico;
    }
    
    /**
    * @return
    */
    public String getNafinElectronico() {
        return nafinElectronico;
    }

    /**
     * @param rfc
     */
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }
    /**
     * @return
     */
    public String getRfc() {
        return rfc;
    }

    /**
     * @param tipoConsulta
     */
    public void setTipoConsulta(String tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }
    /**
     * @return
     */
    public String getTipoConsulta() {
        return tipoConsulta;
    }
    
    /**
     * @param icBitAfiliados
     */
    public void setIcBitAfiliados(String icBitAfiliados) {
        this.icBitAfiliados = icBitAfiliados;
    }
    /**
     * @return
     */
    public String getIcBitAfiliados() {
        return icBitAfiliados;
    }
    
    /**
     * @param fechaModifDe
     */
    public void setFechaModifDe(String fechaModifDe) {
        this.fechaModifDe = fechaModifDe;
    }
    /**
    * @return
     */
    public String getFechaModifDe() {
        return fechaModifDe;
    }

    /**
     * @param fechaModifA
     */
    public void setFechaModifA(String fechaModifA) {
        this.fechaModifA = fechaModifA;
    }

    /**
     * @return
     */
    public String getFechaModifA() {
        return fechaModifA;
    }
    /**
     * @param fechaNotifDe
     */
    public void setFechaNotifDe(String fechaNotifDe) {
        this.fechaNotifDe = fechaNotifDe;
    }
    
    /**
     * @return
     */
    public String getFechaNotifDe() {
        return fechaNotifDe;
    }

    /**
     * @param fechaNotifA
     */
    public void setFechaNotifA(String fechaNotifA) {
        this.fechaNotifA = fechaNotifA;
    }
    
    /**
     * @return
     */
    public String getFechaNotifA() {
        return fechaNotifA;
    }

    /**
     * @param fechaEnteradoIni
     */
    public void setFechaEnteradoIni(String fechaEnteradoIni) {
        this.fechaEnteradoIni = fechaEnteradoIni;
    }

    /**
     * @return
     */
    public String getFechaEnteradoIni() {
        return fechaEnteradoIni;
    }

    /**
     * @param fechaEnteradoFinal
     */
    public void setFechaEnteradoFinal(String fechaEnteradoFinal) {
        this.fechaEnteradoFinal = fechaEnteradoFinal;
    }

    /**
     * @return
     */
    public String getFechaEnteradoFinal() {
        return fechaEnteradoFinal;
    }

}
