package com.netro.cadenas;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ParametrizacionCatalogos {
  
  private final static Log log = ServiceLocator.getInstance().getLog(ParametrizacionCatalogos.class);
  
  private String cedula;
  private String intermediario;
  private String titulo;
  private String aPaterno;
  private String aMaterno;
  private String nombre;
  private String puesto;
  private String habilitado;
  private String tipoPersona;
  
  public ParametrizacionCatalogos()  {  }
  
  /**
   * Pantalla Pagos de Cartera - Parametrización- Catalogos.
   * Obtiene la informacion a mostrar usando el catalago de lista con clave 47
   * @return lista de Tipo Registros.
   * @return 
   */
  public Registros catalogo(){
    log.info("catalogo (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer("");
    List         conditions	  = new ArrayList();
    Registros    registros    = new Registros();
		AccesoDB     con          = new AccesoDB(); 
		
		try{
			con.conexionDB();
			qrySentencia.append(
      "SELECT cfc.ic_firma_cedula, cif.cg_razon_social, cfc.cg_titulo, "+"\n"+
      "  cfc.cg_appaterno, cfc.cg_apmaterno, cfc.cg_nombre, cfc.cg_puesto, "+"\n"+
      "  cfc.cs_activo, cif.ic_if, cfc.ic_cliente_externo, cce.cg_razon_social cg_razon_social_ce "+"\n"+
      "  FROM comcat_if cif, comcat_firma_cedula cfc, comcat_cli_externo cce "+"\n"+
      " WHERE cif.ic_if(+) = cfc.ic_if "+"\n"+
      " AND cce.ic_nafin_electronico(+) = cfc.ic_cliente_externo "+"\n"+
      " ORDER BY cif.cg_razon_social, cce.cg_razon_social, cfc.cg_appaterno "
      );
      
			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("catalogo  (Exception) : " + e);
		} finally {
			if (con.hayConexionAbierta()) {
          con.cierraConexionDB();
			}
		} 	
		log.info("qrySentencia :\n \n"+qrySentencia.toString()+" \n \n");
		log.info("catalogo (S) ");
		return registros;															
	}//FIN Metodo catalogo
  
  
  /**
   * Pantalla Pagos de Cartera - Parametrización- Catalogos.
   * Obtiene el tipo de empleado de acuerdo al tipo de razon social
   * @return 1 si es IF 2 si es Nafin
   */
  public String persona(){
    log.info("persona (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer("");
    List         conditions	  = new ArrayList();
    Registros    registros    = new Registros();
		AccesoDB     con          = new AccesoDB(); 
		String          miPersona="";
		try{
			con.conexionDB();
      
      if (tipoPersona.equals("C")){
        qrySentencia.append(
        "SELECT cfc.ic_firma_cedula, cce.cg_razon_social, cfc.cg_titulo, "+"\n"+
        "  cfc.cg_appaterno, cfc.cg_apmaterno, cfc.cg_nombre, cfc.cg_puesto, "+"\n"+
        "  cfc.cs_activo, cce.ic_nafin_electronico "+"\n"+
        "  FROM comcat_cli_externo cce, comcat_firma_cedula cfc"+"\n"+
        " WHERE  cfc.cg_appaterno = '"+aPaterno +"'"+"\n"+
        " AND  cfc.cg_apmaterno = '"+aMaterno +"'"+"\n"+
        " AND  cfc.cg_nombre = '"+nombre +"'"+"\n"+
        " AND  cfc.cg_puesto = '"+puesto +"'"+"\n"+
        " AND cce.ic_nafin_electronico(+) = cfc.ic_cliente_externo "
        );
      }else
      {
			qrySentencia.append(
      "SELECT cfc.ic_firma_cedula, cif.cg_razon_social, cfc.cg_titulo, "+"\n"+
      "  cfc.cg_appaterno, cfc.cg_apmaterno, cfc.cg_nombre, cfc.cg_puesto, "+"\n"+
      "  cfc.cs_activo, cif.ic_if "+"\n"+
      "  FROM comcat_if cif, comcat_firma_cedula cfc"+"\n"+
      " WHERE  cfc.cg_appaterno = '"+aPaterno +"'"+"\n"+
      " AND  cfc.cg_apmaterno = '"+aMaterno +"'"+"\n"+
      " AND  cfc.cg_nombre = '"+nombre +"'"+"\n"+
      " AND  cfc.cg_puesto = '"+puesto +"'"+"\n"+
      " AND cif.ic_if(+) = cfc.ic_if "
      );
      }
      
			registros = con.consultarDB(qrySentencia.toString());
      
      ArrayList persona=(ArrayList)registros.getData(0);     
      String razon=(String)persona.get(1);
      if (razon.equals("")){
         miPersona="0";
      } else {
        if (!tipoPersona.equals("C")){
        miPersona="1";
        }else
        {
          miPersona="2";
        }
      }
      
  		con.cierraConexionDB();
		} catch (Exception e) {
			log.error("persona  (Exception) : " + e);
		} finally {
			if (con.hayConexionAbierta()) {
          con.cierraConexionDB();
			}
		} 	
		log.info("qrySentencia :\n \n"+qrySentencia.toString()+" \n \n");
		log.info("persona (S) ");
		return miPersona;															
	}//FIN Metodo persona

  
  /**
   * Pantalla Pagos de Cartera - Parametrización- Catalogos.
   * actualiza un elemento del catalogo
   * @return 
   */
  public String actualiza(){
	log.info("actualiza (E)");
	StringBuffer qrySentencia = new StringBuffer();
	StringBuffer condicion    = new StringBuffer("");
    List         conditions	  = new ArrayList();
    Registros    registros    = new Registros();
		AccesoDB     con          = new AccesoDB(); 
    String estado="error";
    int miCedula=Integer.parseInt(cedula);
    int miInter=0;
    try {
    miInter =Integer.parseInt(intermediario);
    }catch (Exception e){}
	 
	 try{
			con.conexionDB();
			qrySentencia.append(
			"UPDATE comcat_firma_cedula SET  cg_titulo = '"+titulo+"',"+"\n"+
			"  cg_appaterno = '"+aPaterno +"',"+"\n"+
			"  cg_apmaterno = '"+aMaterno +"',"+"\n"+
			"  cg_nombre = '"+nombre +"',"+"\n"+
			"  cg_puesto = '"+puesto +"',"+"\n"+
			"  cs_activo = '"+habilitado +"' " );
			if (intermediario.equals("")){
			  qrySentencia.append( " WHERE ic_firma_cedula= "+miCedula );
			}else {
			  if (tipoPersona.equals("C")){
          qrySentencia.append( ", ic_cliente_externo = "+miInter+" WHERE ic_firma_cedula= "+miCedula );
        }else{
			  qrySentencia.append( ", ic_if = "+miInter+" WHERE ic_firma_cedula= "+miCedula );
			}
			}
			
			//if("S".equals(habilitado)){
				con.ejecutaSQL(qrySentencia.toString());
			//} else {
				//con.ejecutaSQL("");
			//}
			con.terminaTransaccion(true);
			log.info("qrySentencia :\n \n"+qrySentencia.toString()+" \n \n");
		 
			String qrySentencia2="";
			if("S".equals(habilitado)){
				if(!"".equals(intermediario)) {
					qrySentencia2 =
						" UPDATE comcat_firma_cedula"   +
						"    SET cs_activo = 'N'"   +
						"  WHERE ic_firma_cedula != "+miCedula+" "+
						(tipoPersona.equals("C")?" AND ic_cliente_externo = ":" AND ic_if = ")+miInter;
				} else {
					qrySentencia2 =
						" UPDATE comcat_firma_cedula"   +
						"    SET cs_activo = 'N'"   +
						"  WHERE ic_firma_cedula != "+miCedula+" "+
						"  	AND ic_if IS NULL ";
				}
		  }//fin habilitado ==S
		  /*if (qrySentencia2.equals("")){
		  } else {*/
			  con.ejecutaSQL(qrySentencia2);
			  con.terminaTransaccion(true);
			  log.info("qrySentencia2 :\n \n"+qrySentencia2+" \n \n");
		  //}
		  
				con.cierraConexionDB();
				estado="OK";
		} catch (Exception e) {
			estado="error";
			log.error("actualiza  (Exception) : " + e);
			con.terminaTransaccion(false);
		} finally {
			if (con.hayConexionAbierta()) {
          con.cierraConexionDB();
			}
		} 	
		
		log.info("actualiza (S) ");
		return estado;															
	}//FIN Metodo actualiza
  
  /**
   * Pantalla Pagos de Cartera - Parametrización- Catalogos.
   * inserta un nuevo registro
   */
   public String nuevo(){
    log.info("nuevo (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer("");
    List         conditions	  = new ArrayList();
    Registros    registros    = new Registros();
		AccesoDB     con          = new AccesoDB(); 
    String estado="error";
    int miInter=0;
    boolean commit = true;
		try{
			con.conexionDB();
			qrySentencia.append(
      "SELECT NVL (MAX (ic_firma_cedula) + 1, 1) "+"\n"+
      " FROM comcat_firma_cedula "
      );
     ResultSet rs=null;
     String clave="";
     rs = con.queryDB(qrySentencia.toString());
			if (rs.next())
				clave = rs.getString(1);
			rs.close();
      con.cierraStatement();
      
       try {
    miInter =Integer.parseInt(intermediario);
    }catch (Exception e){}
		  qrySentencia = new StringBuffer();
			//con.conexionDB();
      if (tipoPersona.equals("C")){
        qrySentencia.append(
        "INSERT INTO comcat_firma_cedula(ic_firma_cedula,cg_titulo,cg_appaterno,cg_apmaterno,cg_nombre,cg_puesto,cs_activo,ic_cliente_externo) VALUES("+clave+",'"+"\n"+
        titulo+"','"+aPaterno +"','"+"\n"+
        aMaterno+"','"+nombre +"','"+"\n"+
        puesto +"','"+habilitado+"',"+miInter +")" );
      }else if (intermediario.equals("")){
			qrySentencia.append(
      "INSERT INTO comcat_firma_cedula(ic_firma_cedula,cg_titulo,cg_appaterno,cg_apmaterno,cg_nombre,cg_puesto,cs_activo) VALUES("+clave+",'"+"\n"+
      titulo+"','"+aPaterno +"','"+"\n"+
      aMaterno+"','"+nombre +"','"+"\n"+
      puesto +"','"+habilitado +"')" );
      } else {
        qrySentencia.append(
        "INSERT INTO comcat_firma_cedula(ic_firma_cedula,cg_titulo,cg_appaterno,cg_apmaterno,cg_nombre,cg_puesto,cs_activo,ic_if) VALUES("+clave+",'"+"\n"+
        titulo+"','"+aPaterno +"','"+"\n"+
        aMaterno+"','"+nombre +"','"+"\n"+
        puesto +"','"+habilitado+"',"+miInter +")" );
      }
      
      con.ejecutaSQL(qrySentencia.toString());
      log.info("qrySentencia :\n \n"+qrySentencia.toString()+" \n \n");
      
      String qrySentencia2="";
      if("S".equals(habilitado)){
			if(!"".equals(intermediario)) {
				qrySentencia2 =
					" UPDATE comcat_firma_cedula"   +
					"    SET cs_activo = 'N'"   +
					"  WHERE ic_firma_cedula != "+clave+" "+
					(tipoPersona.equals("C")?" AND ic_cliente_externo = ":" AND ic_if =")+intermediario+" ";
          
			} else {
				qrySentencia2 =
					" UPDATE comcat_firma_cedula"   +
					"    SET cs_activo = 'N'"   +
					"  WHERE ic_firma_cedula != "+clave+" "+
					"  	AND ic_if IS NULL ";
			}
     }//fin habilitado ==S
     if (qrySentencia2.equals("")){
     } else {
       con.ejecutaSQL(qrySentencia2);
       log.info("qrySentencia :\n \n"+qrySentencia2+" \n \n");
     }
     
      con.terminaTransaccion(commit);
			con.cierraConexionDB();
      estado="OK";
		} catch (Exception e) {
      commit = false;
      estado="error";
			log.error("actualiza  (Exception) : " + e);
		} finally {
      System.out.println("si hay cambios en el codigo");
			if (con.hayConexionAbierta()) {
          con.terminaTransaccion(commit);
          con.cierraConexionDB();
			}
		} 	
		
		log.info("actualiza (S) ");
		return estado;															
	}//FIN Metodo nuevo

  public void setAPaterno(String aPaterno)
  {
    this.aPaterno = aPaterno;
  }


  public String getAPaterno()
  {
    return aPaterno;
  }


  public void setAMaterno(String aMaterno)
  {
    this.aMaterno = aMaterno;
  }


  public String getAMaterno()
  {
    return aMaterno;
  }


  public void setNombre(String nombre)
  {
    this.nombre = nombre;
  }


  public String getNombre()
  {
    return nombre;
  }


  public void setPuesto(String puesto)
  {
    this.puesto = puesto;
  }


  public String getPuesto()
  {
    return puesto;
  }


  public void setIntermediario(String intermediario)
  {
    this.intermediario = intermediario;
  }


  public String getIntermediario()
  {
    return intermediario;
  }


  public void setCedula(String cedula)
  {
    this.cedula = cedula;
  }


  public String getCedula()
  {
    return cedula;
  }


  public void setHabilitado(String habilitado)
  {
    this.habilitado = habilitado;
  }


  public String getHabilitado()
  {
    return habilitado;
  }


  public void setTitulo(String titulo)
  {
    this.titulo = titulo;
  }


  public String getTitulo()
  {
    return titulo;
  }


  public void setTipoPersona(String tipoPersona)
  {
    this.tipoPersona = tipoPersona;
  }


  public String getTipoPersona()
  {
    return tipoPersona;
  }
  
}//FIN CLASS ParametrizacionCatalogos