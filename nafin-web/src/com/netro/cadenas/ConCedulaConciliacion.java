package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConCedulaConciliacion implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConCedulaConciliacion.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String cs_tipo;
	private String sClaveIF;
	private String sClaveMoneda;
	private String sDateIni;
	private String sDateFin;
	private String tipoConsulta;
	private String fechaCorte;
	private String fechaCarga;
	private String tipoLinea;
	
		 
	public ConCedulaConciliacion() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
	
		if(tipoConsulta.equals("Consultar") ) {
		
			qrySentencia.append(" select"   );
			qrySentencia.append("  /*+ USE_NL(C,I) INDEX (C CP_COM_CEDULA_CONCILIA_PK) */"   );
			if(!cs_tipo.equals("CE"))
				qrySentencia.append("  C.IC_IF  as IC_IF"   );
			else
				qrySentencia.append("  C.IC_CLIENTE_EXTERNO  as IC_IF"   );
			qrySentencia.append(" ,C.IC_MONEDA AS IC_MONEDA "   );
			qrySentencia.append("  , M.CD_NOMBRE as MONEDA  "   ); 
			if(!"C".equals(tipoLinea)){
				qrySentencia.append(" ,I.ic_financiera   as CLAVE_SIRAC_IF  "   );
			}else
			{
				qrySentencia.append(" ,C.IN_NUMERO_SIRAC as CLAVE_SIRAC_CE  "   );
			}
			if(!cs_tipo.equals("CE"))
				qrySentencia.append(" ,I.CG_RAZON_SOCIAL as NOMBRE_IF  "   );
			else
				qrySentencia.append(" ,I.CG_RAZON_SOCIAL as NOMBRE_CE  "   );
			qrySentencia.append(" ,TO_CHAR(C.DF_FECHA_CORTE,'DD/MM/YYYY') as   FECHA_CORTE	  "   );
			qrySentencia.append(" ,TO_CHAR(C.DF_CARGA,'DD/MM/YYYY') FECHA_ENVIO "   );
			qrySentencia.append(" ,C.CG_TIPO_LINEA AS CG_TIPO_LINEA  "  );
			qrySentencia.append(" , '' AS SELECCIONAR  "  );
			qrySentencia.append(" from" );
			qrySentencia.append(" com_cedula_concilia C," );
			if(!cs_tipo.equals("CE"))
				qrySentencia.append(" comcat_if I , ");
			else
				qrySentencia.append(" comcat_cli_externo I , ");
			qrySentencia.append(" comcat_moneda  m  " );
			
			if(!cs_tipo.equals("CE"))
				qrySentencia.append(" where C.IC_IF=I.IC_IF  " );
			else
				qrySentencia.append(" where C.IC_CLIENTE_EXTERNO=I.IC_NAFIN_ELECTRONICO  " );
				
			qrySentencia.append(" and c.ic_moneda = m.ic_moneda  ");
				  
			if(!cs_tipo.equals("") && !cs_tipo.equals("CE")){
						qrySentencia.append(" AND I.CS_TIPO = ?");
						conditions.add(cs_tipo);
					}
					if(!sClaveIF.equals("")){
				if(!cs_tipo.equals("CE"))
						qrySentencia.append(" AND C.IC_IF = ? ");
				else
					qrySentencia.append(" AND C.IC_CLIENTE_EXTERNO = ? ");
					
						conditions.add(sClaveIF);
					}
	  
				  if(!sClaveMoneda.equals("")){
					 qrySentencia.append(" AND C.IC_MONEDA = ? ");
					 conditions.add(sClaveMoneda);
				  }
				  
			if(!"".equals(tipoLinea)){
			 qrySentencia.append(" AND C.CG_TIPO_LINEA = ? ");
			 conditions.add(tipoLinea);
			}
			
				  if(!sDateIni.equals("") && !sDateIni.equals("")){			  
				qrySentencia.append(" AND c.df_fecha_corte >= TO_DATE(?,'dd/mm/yyyy')   " );
				qrySentencia.append(" AND c.df_fecha_corte < LAST_DAY(TO_DATE(?,'dd/mm/yyyy')) + 1 " );	
						  
					conditions.add(sDateIni);
					conditions.add(sDateFin);
			}
				  
			qrySentencia.append(" group by"   );
			if(!cs_tipo.equals("CE"))
				qrySentencia.append(" 	C.IC_IF"   );
			else
				qrySentencia.append(" 	C.IC_CLIENTE_EXTERNO"   );
			qrySentencia.append(" 	,C.IC_MONEDA " );
			qrySentencia.append("  ,M.CD_NOMBRE  " );
			if(!"C".equals(tipoLinea)){
				qrySentencia.append("  ,I.ic_financiera " );
			}else
			{
				qrySentencia.append("  ,C.IN_NUMERO_SIRAC " );
				  }
			qrySentencia.append("  ,I.CG_RAZON_SOCIAL " );
			qrySentencia.append("  ,TO_CHAR(C.DF_FECHA_CORTE,'DD/MM/YYYY')	  " );
			qrySentencia.append("  ,TO_CHAR(C.DF_CARGA,'DD/MM/YYYY') " );
			qrySentencia.append("  ,C.DF_CARGA  " );
			qrySentencia.append("  ,C.CG_TIPO_LINEA  " );
			qrySentencia.append(" order by  C.DF_CARGA	" ) ;
		
		
		
			}else  if(tipoConsulta.equals("ConsDetalle")   ||   tipoConsulta.equals("consDetTotalesData")    ) {
		
		
		
			qrySentencia.append( " select /*+ USE_NL(C) INDEX (C CP_COM_CEDULA_CONCILIA_PK) */  " );
			if(!cs_tipo.equals("CE"))
				qrySentencia.append( "  CS_TIPO  as TIPO ");
			else
				qrySentencia.append( "  ''  as TIPO ");
			qrySentencia.append( " ,C.CG_TIPO_CREDITO as SUB_APLICACION  " );
			qrySentencia.append( " ,C.ig_subaplicacion as CONCEPTO_SUB_APLICACION   " );
			qrySentencia.append( " ,C.fg_saldo_insoluto as SALDO_INSOLUTO  " );
			qrySentencia.append( " ,C.FG_MONTO_OPERADO as MONTO_OPERADO  " );
			qrySentencia.append( " ,C.FG_INTERES_VIGENTE  AS CAPITAL_VIGENTE  " );
			qrySentencia.append( " ,C.FG_CAPITAL_VENCIDO  AS CAPITAL_VENCIDO   " );
			qrySentencia.append( " ,C.FG_INTERES_VIGENTE AS INTERES_VIGENTE   " );
			qrySentencia.append( " ,C.FG_INTERES_VENCIDO AS INTERES_VENCIDO  " );
			qrySentencia.append( " ,C.FG_INTERES_MORA AS MORAS  " );
			qrySentencia.append( " ,C.FG_TOTAL_ADEUDO AS TOTAL_ADEUDO  " );
			qrySentencia.append( " ,C.IG_DESCUENTOS AS DESCUENTOS    " );
			qrySentencia.append( " ,C.CG_OBSERVACIONES as OBSERVACIONES "   );
			qrySentencia.append( " ,ic_usuario as  ic_usuario  ");
			qrySentencia.append( " , cg_nombre_usuario as  nombreUsuario ");
			qrySentencia.append( " ,to_char(df_carga,'dd/mm/yyyy hh24:mi:ss') as fcarga ");
			qrySentencia.append( " ,ig_clave_certificado as cvecertificado ");
			if(!cs_tipo.equals("CE")){
				qrySentencia.append( " from com_cedula_concilia C, comcat_if IF"  );
				qrySentencia.append( " where C.IC_IF=IF.IC_IF " );
			}else
			{
				qrySentencia.append( " from com_cedula_concilia C, comcat_cli_externo IF"  );
				qrySentencia.append( " where C.IC_CLIENTE_EXTERNO=IF.IC_NAFIN_ELECTRONICO " );
			}
				 
					if(!sClaveIF.equals("")){
				if(!cs_tipo.equals("CE"))
						qrySentencia.append(" AND C.IC_IF = ? ");
				else
					qrySentencia.append(" AND C.IC_CLIENTE_EXTERNO = ? ");
						conditions.add(sClaveIF);
					}
					
					if(!sClaveMoneda.equals("")){
					 qrySentencia.append(" AND C.IC_MONEDA = ? ");
					 conditions.add(sClaveMoneda);
				  }
				  
				  
				   if(!fechaCorte.equals("") ){		
						qrySentencia.append(" AND TRUNC(C.DF_FECHA_CORTE) = TO_DATE(?,'DD/MM/YYYY')	  ");  
						conditions.add(fechaCorte);	
				  }
				  
				 if(!fechaCarga.equals("")){						
					qrySentencia.append("  AND TRUNC(C.DF_CARGA) = TO_DATE(?,'DD/MM/YYYY')	") ;
					conditions.add(fechaCarga);
				}
				  
			if(!"".equals(tipoLinea)){
			 qrySentencia.append(" AND C.CG_TIPO_LINEA = ? ");
			 conditions.add(tipoLinea);
			}
				
			}
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	/**
	 *Query para sacar  las Firmas de la cedula de conciliación
	 * @return 
	 * @param ic_if
	 */
	public HashMap getDatosFirma(String ic_if)  {
		
		log.info ("getDatosFirma(E)  ");
		
		AccesoDB 	con = new AccesoDB();
      boolean		exito = true;	
		StringBuffer strSQL = new StringBuffer();
		List lVarBind		= new ArrayList();	
		PreparedStatement ps	= null;
		ResultSet  rs = null;
		HashMap	registros = new HashMap();
		
		try {
			con.conexionDB();  
					 
				strSQL = new StringBuffer();
				strSQL.append(" SELECT IC_FIRMA_CEDULA FIRMA_IF " +
						"       ,CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO NOMBRE_IF " +
						" 	  ,CG_PUESTO PUESTO_IF " +
						" 	  ,(SELECT IC_FIRMA_CEDULA FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S'  AND IC_CLIENTE_EXTERNO IS NULL) FIRMA_NAFIN " +
						" 	  ,(SELECT CG_TITULO||' '||CG_NOMBRE||' '||CG_APPATERNO||' '||CG_APMATERNO RESP_IF  " +
						"        FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) RESP_NAFIN " +
						" 	  ,(SELECT CG_PUESTO FROM COMCAT_FIRMA_CEDULA WHERE IC_IF IS NULL AND CS_ACTIVO='S' AND IC_CLIENTE_EXTERNO IS NULL) PUESTO_NAFIN " +
						"	  ,TO_CHAR(sysdate,'DD/MM/YYYY') FECHA ");
				strSQL.append(" FROM COMCAT_FIRMA_CEDULA"   );
				if(!cs_tipo.equals("CE"))
					strSQL.append(" WHERE IC_IF=? "   );
				else
					strSQL.append(" WHERE IC_CLIENTE_EXTERNO=? "   );
				strSQL.append(" AND CS_ACTIVO= ? ");									
									
				lVarBind		= new ArrayList();				
				lVarBind.add(ic_if);	
				lVarBind.add("S");	
				log.debug("SQL.toString() "+strSQL.toString());
				log.debug("varBind "+lVarBind);
			
				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				rs = ps.executeQuery();
		
         if( rs.next() ){
				registros.put("FIRMA_IF", rs.getString("FIRMA_IF")==null?"":rs.getString("FIRMA_IF"));
				registros.put("NOMBRE_IF", rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF"));	
				registros.put("PUESTO_IF", rs.getString("PUESTO_IF")==null?"":rs.getString("PUESTO_IF"));	
				registros.put("FIRMA_NAFIN", rs.getString("FIRMA_NAFIN")==null?"":rs.getString("FIRMA_NAFIN"));	
				registros.put("RESP_NAFIN", rs.getString("RESP_NAFIN")==null?"":rs.getString("RESP_NAFIN"));	
				registros.put("PUESTO_NAFIN", rs.getString("PUESTO_NAFIN")==null?"":rs.getString("PUESTO_NAFIN"));	
			}			
			rs.close();
			ps.close();		
												
		} catch(Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error ("getDatosFirma  " + e);
			throw new AppException("SIST0001");			
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  getDatosFirma (S) ");
			}			
		}
		return registros;
	}
	
	
	/**
	 * Obtengo las observaciones del Detalle de  de la cedula de conciliación
	 * @return 
	 * @param sFechaCorte
	 * @param ic_moneda
	 * @param ic_if
	 */
	public String getNotas (String sFechaCorte, String ic_if, String ic_moneda  )  {
		
		log.info ("getNotas(E)  ");
		
		AccesoDB 	con = new AccesoDB();
      boolean		exito = true;	
		StringBuffer strSQL = new StringBuffer();
		List lVarBind		= new ArrayList();	
		PreparedStatement ps	= null;
		ResultSet  rs = null;
		String  notas = "";
				
		try {
			con.conexionDB();  
					 
				strSQL = new StringBuffer();
				strSQL.append(" select cg_observaciones "+
				" from com_observaciones_cedula "+
				"where trunc(df_fecha_corte) = to_date( ?, 'dd/mm/yyyy') "+
				"and ic_if = ? "+
				"and ic_moneda = ? ");									
									
				lVarBind		= new ArrayList();		
				lVarBind.add(sFechaCorte);	
				lVarBind.add(ic_if);	
				lVarBind.add(ic_moneda);	
				log.debug("SQL.toString() "+strSQL.toString());
				log.debug("varBind "+lVarBind);
			
				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				rs = ps.executeQuery();
		
         if( rs.next() ){
				notas =  rs.getString("cg_observaciones")==null?"":rs.getString("cg_observaciones");			
			}			
			rs.close();
			ps.close();		
												
		} catch(Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error ("getNotas  " + e);
			throw new AppException("SIST0001");			
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  getNotas (S) ");
			}			
		}
		return notas;
	}
	
	
	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getCs_tipo() {
		return cs_tipo;
	}

	public void setCs_tipo(String cs_tipo) {
		this.cs_tipo = cs_tipo;
	}

	public String getSClaveIF() {
		return sClaveIF;
	}

	public void setSClaveIF(String sClaveIF) {
		this.sClaveIF = sClaveIF;
	}

	public String getSClaveMoneda() {
		return sClaveMoneda;
	}

	public void setSClaveMoneda(String sClaveMoneda) {
		this.sClaveMoneda = sClaveMoneda;
	}

	public String getSDateIni() {
		return sDateIni;
	}

	public void setSDateIni(String sDateIni) {
		this.sDateIni = sDateIni;
	}

	public String getSDateFin() {
		return sDateFin;
	}

	public void setSDateFin(String sDateFin) {
		this.sDateFin = sDateFin;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public String getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(String fechaCarga) {
		this.fechaCarga = fechaCarga;
	}


	public void setTipoLinea(String tipoLinea)
	{
		this.tipoLinea = tipoLinea;
	}


	public String getTipoLinea()
	{
		return tipoLinea;
	}

	
	
		

}