package com.netro.cadenas;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import netropology.utilerias.*;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;

public class ConsAfiliaElectronica implements IQueryGeneratorRegExtJS {
	//Variables
	StringBuffer qrySentencia = new StringBuffer();
	private List 		conditions;
		   

	private static final Log log = ServiceLocator.getInstance().getLog(ConsAfiliaElectronica.class);
	private String ic_if;
	private String ic_epo;
	private String ic_pyme;
	private String txt_fecha_sol_de;
	private String txt_fecha_sol_a;
	private String tipoConsulta;
	
	

	public String getAggregateCalculationQuery() {
	return "";
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
	return "";
	}
	
	public String getDocumentQuery(){
	return "";
	}
	
	public String getDocumentQueryFile(){
		
		log.debug("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();		
		this.conditions = new ArrayList();
		
		
		
			qrySentencia.append(
						" SELECT epo.cg_razon_social AS NOMBRE_EPO,"   +
						"          DECODE (pyme.cs_habilitado,'N', '* ','S', ' ')   || pyme.cg_razon_social AS RAZON_SOCIAL,"   +
						"        pyme.cg_rfc AS RFC,"   +
						"        dom.cg_calle || ' '|| dom.cg_numero_ext|| ' '|| dom.cg_numero_int|| ' '|| dom.cg_colonia|| ' '|| dom.cn_cp   || ' '|| mun.cd_nombre || ' '|| edo.cd_nombre AS DOMICILIO   ,"   +
						"        TO_CHAR(relpymeif.df_envio,'DD/MM/YYYY') as  FECHA_SOLICITUD  , "+
						"			relctabanc.ic_cuenta_bancaria AS IC_CUENTA_BANCARIA ,"   +
						"        relctabanc.cg_numero_cuenta as CUENTA_BANCARIA, "+
						"			relctabanc.cg_banco AS BANCO_SERVICIO,"   +
						"        relctabanc.cg_sucursal as SUCURSAL , "+
						"			NVL (plaza.cd_descripcion, 'no tiene') as PLAZA,"   +
						"        DECODE (relpymeif.cs_estatus,'E', 'Enviado ','A', 'Autorizado')  as ESTATUS,  "   +
						"			relpymeif.df_autorizacion, "   +
						"			epo.ic_epo  AS IC_EPO, "   +
						"			pyme.ic_pyme AS IC_PYME , "   +
						"			relpymeif.ic_if AS IC_IF ,  "   +
						"  		m.cd_nombre as MONEDA , "+	
						"			'' as SELECCIONAR  "+
						"   FROM comrel_cuenta_bancaria relctabanc,"   +
						"        comrel_pyme_if relpymeif,"   +
						"        comcat_epo epo,"   +
						"        comcat_plaza plaza,"   +
						"        comrel_pyme_epo pe,"   +
						"        comcat_pyme pyme,"   +
						"        com_domicilio dom,"   +
						"        comcat_estado edo,"   +
						"        comcat_municipio mun ,  "   +
						" 			comcat_moneda m"+
						"  WHERE relpymeif.ic_cuenta_bancaria = relctabanc.ic_cuenta_bancaria"   +
						"    AND pyme.ic_pyme = relctabanc.ic_pyme"   +
						"    AND relctabanc.ic_plaza = plaza.ic_plaza(+)"   +
						"    AND relctabanc.cs_borrado = 'N' " +
						"    AND relpymeif.ic_epo = epo.ic_epo"   +
						"    AND relpymeif.cs_borrado = 'N'"   +
						"    AND epo.cs_habilitado = 'S'"   +
						"    AND pe.ic_pyme = pyme.ic_pyme"   +
						"    AND relpymeif.ic_epo = pe.ic_epo"   +						
						"    AND pyme.ic_pyme = dom.ic_pyme"   +
						"    AND dom.ic_estado = edo.ic_estado"   +
						"    AND edo.ic_estado = mun.ic_estado"   +
						"    AND dom.ic_municipio = mun.ic_municipio"   +
						"    AND dom.ic_estado = mun.ic_estado"   +
						"    AND dom.ic_pais = mun.ic_pais"   +
						"    AND dom.cs_fiscal = 'S'"   +						
						"    AND m.ic_moneda = relctabanc.ic_moneda " );
					
					
			if (!"".equals(ic_if) && ic_if != null){
				qrySentencia.append("  AND relpymeif.ic_if =? ");
				conditions.add(ic_if);
			}
			if (!"".equals(ic_epo) && ic_epo != null){
				qrySentencia.append(" AND relpymeif.ic_epo =? ");
				conditions.add(ic_epo);
			}
			if (!"".equals(ic_pyme) && ic_pyme != null){
				qrySentencia.append(" AND relctabanc.ic_pyme =? ");
				conditions.add(ic_pyme);   
			}

			if ((!"".equals(txt_fecha_sol_de) && txt_fecha_sol_de != null) && (!"".equals(txt_fecha_sol_a) && txt_fecha_sol_a != null) ){
				qrySentencia.append(" AND relpymeif.df_envio >= TO_DATE (?, 'dd/mm/yyyy')"   +
											"  AND relpymeif.df_envio < TO_DATE (?, 'dd/mm/yyyy')+1");
					conditions.add(txt_fecha_sol_de);
					conditions.add(txt_fecha_sol_a);
			}	
			
			
			if(tipoConsulta.equals("A")) {	
					qrySentencia.append(" AND relpymeif.cs_estatus='E' " +
											  " AND relpymeif.cs_vobo_if = 'N'" );
			}  
			
			if(tipoConsulta.equals("C")) {	
					qrySentencia.append(" AND relpymeif.cs_estatus='A' "+
											  " AND relpymeif.cs_vobo_if = 'S'"  );
			}		
		
		
		log.debug("strQuery.toString(): "+qrySentencia.toString());
		log.debug("conditions.toString(): "+conditions);
		log.debug("getDocumentQueryFile(S)");
		
		return qrySentencia.toString();
	
	}
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		
		try {
		
			contenidoArchivo.append("Nombre EPO,Nombre o Raz�n Social,RFC,Domicilio,Fecha de Solicitud,Cuenta Bancaria,Banco de Servicio,Sucursal,Plaza \n\n");
				
			while(rs.next()){
			
				String nombre_epo 		= rs.getString("NOMBRE_EPO")==null?"":rs.getString("NOMBRE_EPO");
				String razon_social 		= rs.getString("RAZON_SOCIAL")==null?"":rs.getString("RAZON_SOCIAL");
				String rfc 		= rs.getString("RFC")==null?"":rs.getString("RFC");
				String domicilio 		= rs.getString("DOMICILIO")==null?"":rs.getString("DOMICILIO");
				String fecha_solic 		= rs.getString("FECHA_SOLICITUD")==null?"":rs.getString("FECHA_SOLICITUD");
				String moneda 		= rs.getString("MONEDA")==null?"":rs.getString("MONEDA");
				String cuenta_bancaria 		= rs.getString("CUENTA_BANCARIA")==null?"":rs.getString("CUENTA_BANCARIA");
				String sucursal 		= rs.getString("SUCURSAL")==null?"":rs.getString("SUCURSAL");
				String plaza 		= rs.getString("plaza")==null?"":rs.getString("plaza");
				String estatus 		= rs.getString("estatus")==null?"":rs.getString("estatus");
				
				contenidoArchivo.append(nombre_epo.replace(',',' ') +",");
				contenidoArchivo.append(razon_social.replace(',',' ') +",");
				contenidoArchivo.append(rfc.replace(',',' ') +",");
				contenidoArchivo.append(domicilio.replace(',',' ') +",");
				contenidoArchivo.append(fecha_solic.replace(',',' ') +",");
				contenidoArchivo.append(moneda.replace(',',' ') +",");
				contenidoArchivo.append(cuenta_bancaria.replace(',',' ') +",");
				contenidoArchivo.append(sucursal.replace(',',' ') +",");
				contenidoArchivo.append(plaza.replace(',',' ') +",");				
				contenidoArchivo.append("\n");
			}
			
			
			if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
				nombreArchivo="ERROR";
			}else{
			nombreArchivo = archivo.nombre;
			}
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		return nombreArchivo;
	}
	
	public List getConditions() {  return conditions;  }  

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getTxt_fecha_sol_de() {
		return txt_fecha_sol_de;
	}

	public void setTxt_fecha_sol_de(String txt_fecha_sol_de) {
		this.txt_fecha_sol_de = txt_fecha_sol_de;
	}

	public String getTxt_fecha_sol_a() {
		return txt_fecha_sol_a;
	}

	public void setTxt_fecha_sol_a(String txt_fecha_sol_a) {
		this.txt_fecha_sol_a = txt_fecha_sol_a;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}


	
}