package com.netro.cadenas;

import com.netro.exception.NafinException;
import com.netro.fondeopropio.ComisionFondeoPropio;
import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.naming.NamingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Iva;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ComisionCalculo implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ComisionCalculo.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_if;
	private String mes_calculo;
	private String anio_calculo;
	private String ic_moneda;
	private String descripcionMes;
	private String iva;
	private String porcentajeIva;
	private String despMoneda;
	private String tipoConsulta;
	private String fechaFinal;
	
	
		 
	public ComisionCalculo() { }
	
	  
	/**
	 * Metodo que obtiene los montos 
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append(" SELECT  i.CG_RAZON_SOCIAL as  NOMBRE_IF ,  "+
				" SUM(cf.ig_doctos_vigentes) AS TOTAL_NUM_DOCUMENTOS, "+
				" SUM(cf.fg_monto_doctos) AS TOTAL_MONTO_DOCUMENTOS "+				
				" FROM"+
				" com_comision_fondeo cf, comcat_producto_nafin pn, "+ 
				" comcat_if i, comcat_epo e, com_comision_fondeo_det cfd,  "+
				" comcat_moneda m  "+
				" WHERE cf.ic_producto_nafin = pn.ic_producto_nafin"+
				" AND cf.ic_if = i.ic_if"+
				" AND cf.ic_epo = e.ic_epo"+
				" AND cf.ic_producto_nafin = cfd.ic_producto_nafin"+
				" AND cf.ic_if = cfd.ic_if"+
				" AND cf.ic_mes_calculo = cfd.ic_mes_calculo"+
				" AND cf.ic_anio_calculo = cfd.ic_anio_calculo"+
				" AND cf.cg_tipo_comision = cfd.cg_tipo_comision"+
				"	AND cfd.ic_moneda = cf.ic_moneda " +
				"	AND cfd.ic_banco_fondeo = cf.ic_banco_fondeo "+
				" AND cfd.ic_moneda = m.ic_moneda ");
				
				if ( !ic_if.equals("") ) {
					qrySentencia.append(" AND cf.ic_if = ? ");
					conditions.add(ic_if);
				}
				if ( !mes_calculo.equals("") ) {
					qrySentencia.append(" AND cf.ic_mes_calculo = ?  ");
					conditions.add(mes_calculo);
				}
				if ( !anio_calculo.equals("") ) {
					qrySentencia.append(" AND cf.ic_anio_calculo = ?   ");
					conditions.add(anio_calculo);
				}
				
				if ( !ic_moneda.equals("") ) {
					qrySentencia.append(" AND cfd.ic_moneda = ?  ");
					conditions.add(ic_moneda);				
				}
				
			qrySentencia.append("	GROUP BY i.CG_RAZON_SOCIAL		 ");
			
			
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * Metodo que saca las llavez primarias 
	 * @return 
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		qrySentencia.append(" SELECT  e.ic_epo "+
				" FROM"+
				" com_comision_fondeo cf, comcat_producto_nafin pn,"+
				" comcat_if i, comcat_epo e, com_comision_fondeo_det cfd"+
				" WHERE cf.ic_producto_nafin = pn.ic_producto_nafin"+
				" AND cf.ic_if = i.ic_if"+
				" AND cf.ic_epo = e.ic_epo"+
				" AND cf.ic_producto_nafin = cfd.ic_producto_nafin"+
				" AND cf.ic_if = cfd.ic_if"+
				" AND cf.ic_mes_calculo = cfd.ic_mes_calculo"+
				" AND cf.ic_anio_calculo = cfd.ic_anio_calculo"+
				" AND cf.cg_tipo_comision = cfd.cg_tipo_comision"+
				"	AND cfd.ic_moneda = cf.ic_moneda " +
				"	AND cfd.ic_banco_fondeo = cf.ic_banco_fondeo ");
				
				if ( !ic_if.equals("") ) {
					qrySentencia.append(" AND cf.ic_if = ? ");
					conditions.add(ic_if);
				}
				if ( !mes_calculo.equals("") ) {
					qrySentencia.append(" AND cf.ic_mes_calculo = ?  ");
					conditions.add(mes_calculo);
				}
				if ( !anio_calculo.equals("") ) {
					qrySentencia.append(" AND cf.ic_anio_calculo = ?   ");
					conditions.add(anio_calculo);
				}
				
				if ( !ic_moneda.equals("") ) {
					qrySentencia.append(" AND cfd.ic_moneda = ?  ");
					conditions.add(ic_moneda);				
				}
					
			qrySentencia.append(" ORDER BY e.cg_razon_social, cf.ic_producto_nafin  ");
				
		
				
		
		log.info("qrySentencia --------- "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append(" SELECT i.cg_razon_social AS NombreIF, cf.ic_producto_nafin, pn.ic_nombre, "+
        " e.ic_epo, e.cg_razon_social as NOMBRE_EPO , "+
				" cfd.fg_porcentaje_aplicado, cfd.fg_contraprestacion, "+
				" SUM(cf.ig_doctos_vigentes) AS NUM_DOCUMENTOS, "+
				" SUM(cf.fg_monto_doctos) AS MONTO_DOCUMENTOS, "+
				"  m.CD_NOMBRE as MONEDA ,"+
				" '' as MES,   " +
				" '' as  PORCE_CONTRIBUCION ,  "+
				" '' as  MONTO_CONTRIBUCION  ,  "+
				"'"+descripcionMes  + "' as DESCRIPCION_MES ,  "+				
			 " FG_CONTRI_COMISION * ?  as  FG_CONTRI_COMISION "  +				
				" FROM"+
				" com_comision_fondeo cf, comcat_producto_nafin pn, "+ 
				" comcat_if i, comcat_epo e, com_comision_fondeo_det cfd,  "+
				" comcat_moneda m  "+
				" WHERE cf.ic_producto_nafin = pn.ic_producto_nafin"+
				" AND cf.ic_if = i.ic_if"+
				" AND cf.ic_epo = e.ic_epo"+
				" AND cf.ic_producto_nafin = cfd.ic_producto_nafin"+
				" AND cf.ic_if = cfd.ic_if"+
				" AND cf.ic_mes_calculo = cfd.ic_mes_calculo"+
				" AND cf.ic_anio_calculo = cfd.ic_anio_calculo"+
				" AND cf.cg_tipo_comision = cfd.cg_tipo_comision"+
				"	AND cfd.ic_moneda = cf.ic_moneda " +
				"	AND cfd.ic_banco_fondeo = cf.ic_banco_fondeo "+
				" AND cfd.ic_moneda = m.ic_moneda ");
			
			double  ivaT= 1 + Double.parseDouble((String)iva) ; 
			
			conditions.add(String.valueOf(ivaT) );
				
				if ( !ic_if.equals("") ) {
					qrySentencia.append(" AND cf.ic_if = ? ");
					conditions.add(ic_if);
				}
				if ( !mes_calculo.equals("") ) {
					qrySentencia.append(" AND cf.ic_mes_calculo = ?  ");
					conditions.add(mes_calculo);
				}
				if ( !anio_calculo.equals("") ) {
					qrySentencia.append(" AND cf.ic_anio_calculo = ?   ");
					conditions.add(anio_calculo);
				}
				
				if ( !ic_moneda.equals("") ) {
					qrySentencia.append(" AND cfd.ic_moneda = ?  ");
					conditions.add(ic_moneda);				
				}
				
				
				
				
				qrySentencia.append(" AND (");
			
				for (int i = 0; i < pageIds.size(); i++) { 
					List lItem = (ArrayList)pageIds.get(i);
					
					if(i > 0){qrySentencia.append("  OR  ");}
					
					qrySentencia.append(" e.ic_epo  =  ? " );
					conditions.add(new Long(lItem.get(0).toString()));
				}
			
			qrySentencia.append(" ) ");
			
			
			
				
			qrySentencia.append(" GROUP BY i.cg_razon_social, cf.ic_producto_nafin, pn.ic_nombre,  e.ic_epo, e.cg_razon_social, "+
			" cfd.fg_porcentaje_aplicado, cfd.fg_contraprestacion,  m.CD_NOMBRE, FG_CONTRI_COMISION  " );
				
			qrySentencia.append(" ORDER BY e.cg_razon_social, cf.ic_producto_nafin  "); 
				
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  

			log.info("tipoConsulta <============  "+tipoConsulta);   

		if ( tipoConsulta.equals("Consulta") ) {	
		
			qrySentencia.append(" SELECT i.cg_razon_social AS NombreIF, cf.ic_producto_nafin, pn.ic_nombre, "+
			  " e.ic_epo, e.cg_razon_social as NOMBRE_EPO , "+
					" cfd.fg_porcentaje_aplicado, cfd.fg_contraprestacion, "+
					" SUM(cf.ig_doctos_vigentes) AS NUM_DOCUMENTOS, "+
					" SUM(cf.fg_monto_doctos) AS MONTO_DOCUMENTOS, "+
					"  m.CD_NOMBRE as MONEDA ,"+
					" '' as MES,   " +
					" '' as  PORCE_CONTRIBUCION ,  "+
					" '' as  MONTO_CONTRIBUCION  ,  "+
					"'"+descripcionMes  + "' as DESCRIPCION_MES,   "+
					" FG_CONTRI_COMISION * ?  as  FG_CONTRI_COMISION "  +	
					
					" FROM"+
					" com_comision_fondeo cf, comcat_producto_nafin pn, "+ 
					" comcat_if i, comcat_epo e, com_comision_fondeo_det cfd,  "+
					" comcat_moneda m  "+
					" WHERE cf.ic_producto_nafin = pn.ic_producto_nafin"+
					" AND cf.ic_if = i.ic_if"+
					" AND cf.ic_epo = e.ic_epo"+
					" AND cf.ic_producto_nafin = cfd.ic_producto_nafin"+
					" AND cf.ic_if = cfd.ic_if"+
					" AND cf.ic_mes_calculo = cfd.ic_mes_calculo"+
					" AND cf.ic_anio_calculo = cfd.ic_anio_calculo"+
					" AND cf.cg_tipo_comision = cfd.cg_tipo_comision"+
					"	AND cfd.ic_moneda = cf.ic_moneda " +
					"	AND cfd.ic_banco_fondeo = cf.ic_banco_fondeo "+
					" AND cfd.ic_moneda = m.ic_moneda ");
					
					double  ivaT= 1 + Double.parseDouble((String)iva) ; 
			
					conditions.add(String.valueOf(ivaT) );
			
					if ( !ic_if.equals("") ) {
						qrySentencia.append(" AND cf.ic_if = ? ");
						conditions.add(ic_if);
					}
					if ( !mes_calculo.equals("") ) {
						qrySentencia.append(" AND cf.ic_mes_calculo = ?  ");
						conditions.add(mes_calculo);
					}
					if ( !anio_calculo.equals("") ) {
						qrySentencia.append(" AND cf.ic_anio_calculo = ?   ");
						conditions.add(anio_calculo);
					}
					
					if ( !ic_moneda.equals("") ) {
						qrySentencia.append(" AND cfd.ic_moneda = ?  ");
						conditions.add(ic_moneda);				
					}
					
					
				qrySentencia.append(" GROUP BY i.cg_razon_social, cf.ic_producto_nafin, pn.ic_nombre,  e.ic_epo, e.cg_razon_social, "+
				" cfd.fg_porcentaje_aplicado, cfd.fg_contraprestacion,  m.CD_NOMBRE, FG_CONTRI_COMISION  " );
					
				qrySentencia.append(" ORDER BY e.cg_razon_social, cf.ic_producto_nafin  "); 
			
		}else if ( tipoConsulta.equals("Consulta_Resumen") ) {	
			
			qrySentencia.append( " SELECT  " +
					" cfd.ic_producto_nafin AS IC_PRODUCTO  ,"+
					" pn.ic_nombre AS  PRODUCTO ," +  
					" i.cg_razon_social as NOMBRE_IF , " +
					" '"+descripcionMes  + "' as DESCRIPCION_MES,   "+
					" decode(pcf.cg_estatus_pagado,'N','No Pagado','Pagado') as ESTATUS , " +////FODEA-039-2014
					" SUM(cfd.fg_contraprestacion) as CONTRAPRESTACION, " +
					" ?  as POR_IVA ,  " +
					" SUM(cfd.fg_contraprestacion) * ?   as MONTO_IVA, " +
					" SUM(cfd.fg_contraprestacion) *  ?   as CONTRAPRESTACION_TOTAL ,   " +
					" pcf.cg_estatus_pagado  AS CHECK_ESTATUS " +
					" , to_char(pcf.df_fecha_pago,'dd/mm/yyyy')  AS FECHA_PAGO " +
					"	,'N' as EDITABLE	, "	+
					" '"+fechaFinal  + "' as FECHACALCULO	"+
					
					" FROM  " +
					" 	com_comision_fondeo_det cfd, comcat_producto_nafin pn, comcat_if i, " +
					" 	com_pago_comision_fondeo pcf " +
					" WHERE  " +
					" 	cfd.ic_producto_nafin = pcf.ic_producto_nafin " +
					" 	AND cfd.ic_if = pcf.ic_if " +
					" 	AND cfd.ic_anio_calculo = pcf.ic_anio_calculo " +
					" 	AND cfd.ic_mes_calculo = pcf.ic_mes_calculo " +
					" 	AND cfd.ic_producto_nafin = pn.ic_producto_nafin " +
					" 	AND cfd.ic_if = i.ic_if " +
					" 	AND cfd.ic_if = ?  " +
					" 	AND cfd.ic_anio_calculo = ?  " +
					" 	AND cfd.ic_mes_calculo = ?  " +
					"	AND cfd.ic_moneda = pcf.ic_moneda " +
					"	AND cfd.ic_banco_fondeo = pcf.ic_banco_fondeo " +					
					"	AND cfd.ic_moneda = ? " +
					" GROUP BY cfd.ic_producto_nafin, pn.ic_nombre, i.cg_razon_social, " +
					" 	pcf.cg_estatus_pagado,pcf.df_fecha_pago " +
					" ORDER BY cfd.ic_producto_nafin ");
				
				double  ivaT= 1 + Double.parseDouble((String)iva) ; 
				
				conditions.add(porcentajeIva);	
				conditions.add(iva);
				conditions.add(String.valueOf(ivaT) );
				
				conditions.add(ic_if);
				conditions.add(anio_calculo);
				conditions.add(mes_calculo);
				conditions.add(ic_moneda);
				
		}
	
	
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}  
	  
	  
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		try {
		
		
			if(tipo.equals("PDF") )   {
				
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
							
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
						(String)session.getAttribute("sesExterno"),
						(String) session.getAttribute("strNombre"),
						(String) session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				
			}
			
			if ( tipoConsulta.equals("Consulta") ) {	
			
				HashMap  regTotales=   this.getMontoTotales (ic_if , anio_calculo , mes_calculo  , ic_moneda   );
				
				String  montoTotal = regTotales.get("TOTAL_MONTO_DOCUMENTOS").toString();
				BigDecimal 	montoTotalDocumentos = new BigDecimal(montoTotal);
				
				String  montoTotalComi=   this.getMontoTotalComision (ic_if , anio_calculo , mes_calculo  , ic_moneda , Double.parseDouble((String)iva)    );
				BigDecimal montoTotalComision = new BigDecimal(montoTotalComi);
		
				if(tipo.equals("PDF") )   {
								
					pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
					pdfDoc.addText(" Comisi�n Recursos Propios  ","formas",ComunesPDF.CENTER);
					pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
					
					pdfDoc.setTable(2,50);		
					pdfDoc.setCell(" RESUMEN SUMA DE TOTALES  "+despMoneda , "celda02",ComunesPDF.CENTER, 2);  
					pdfDoc.setCell(" IF ", "celda02",ComunesPDF.LEFT);  				
					pdfDoc.setCell(regTotales.get("NOMBRE_IF").toString(), "formas",ComunesPDF.LEFT);		
					pdfDoc.setCell(" Num Documentos  ", "celda02",ComunesPDF.LEFT);	
					pdfDoc.setCell(regTotales.get("TOTAL_NUM_DOCUMENTOS").toString(), "formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(" Monto de los Documentos ", "celda02",ComunesPDF.LEFT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(regTotales.get("TOTAL_MONTO_DOCUMENTOS").toString(),2) , "formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(" Monto de la Comisi�n ", "celda02",ComunesPDF.LEFT);	
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalComi,2),"formas",ComunesPDF.LEFT);		
					pdfDoc.addTable();
					
					
					pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
					pdfDoc.addText(" NOTA: IVA DEL "+porcentajeIva+"%","formas",ComunesPDF.CENTER);
					pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
											
					pdfDoc.setTable(7,100);		
					pdfDoc.setCell(" DETALLE POR EPO ", "celda02",ComunesPDF.CENTER,7 );  
					pdfDoc.setCell(" Mes ", "celda02",ComunesPDF.CENTER);  
					pdfDoc.setCell(" EPO ", "celda02",ComunesPDF.CENTER);	
					pdfDoc.setCell(" Num Documentos ", "celda02",ComunesPDF.CENTER);	
					pdfDoc.setCell(" Moneda ", "celda02",ComunesPDF.CENTER);	
					pdfDoc.setCell(" Monto Documentos ", "celda02",ComunesPDF.CENTER);
					pdfDoc.setCell(" % de contribuci�n a la comisi�n ", "celda02",ComunesPDF.CENTER);
					pdfDoc.setCell(" Monto de contribuci�n a la comisi�n ", "celda02",ComunesPDF.CENTER);
					
					while(rs.next()){
						String  descripcionMes  =  rs.getString("DESCRIPCION_MES")==null?"":rs.getString("DESCRIPCION_MES");			
						String  nombre_EPO  =  rs.getString("NOMBRE_EPO")==null?"":rs.getString("NOMBRE_EPO");			
						String  num_documento  =  rs.getString("NUM_DOCUMENTOS")==null?"0":rs.getString("NUM_DOCUMENTOS");			
						String  moneda  =  rs.getString("MONEDA")==null?"":rs.getString("MONEDA");			
						String  montoDoc  =  rs.getString("MONTO_DOCUMENTOS")==null?"0":rs.getString("MONTO_DOCUMENTOS");			
							
						BigDecimal montoDocumentos = new BigDecimal(montoDoc);
						BigDecimal porcentajeComision = (montoDocumentos.multiply(new BigDecimal("100"))).divide(montoTotalDocumentos, 20, BigDecimal.ROUND_HALF_UP);
						BigDecimal montoComision = (montoTotalComision.multiply(porcentajeComision)).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP);
						String  contriComision  =  rs.getString("FG_CONTRI_COMISION")==null?"0":rs.getString("FG_CONTRI_COMISION");	
			
						pdfDoc.setCell(descripcionMes, "formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_EPO, "formas",ComunesPDF.LEFT);
						pdfDoc.setCell(num_documento, "formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda, "formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDoc,2), "formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(porcentajeComision.toPlainString()+"%", "formas",ComunesPDF.CENTER);
						if(contriComision.toString().equals("0"))  {
							pdfDoc.setCell("$"+Comunes.formatoDecimal(montoComision.toPlainString(),2),"formas",ComunesPDF.RIGHT);				
						}else  {
							pdfDoc.setCell("$"+Comunes.formatoDecimal(contriComision,2),"formas",ComunesPDF.RIGHT);						
						}
						
					}
						
					pdfDoc.addTable();
					pdfDoc.endDocument();
					
				}else if(tipo.equals("XLS") )   {
				
					ComunesXLS xls = null;
					CreaArchivo archivo = new CreaArchivo();
					nombreArchivo = archivo.nombreArchivo() + ".xls";
					xls = new ComunesXLS(path + nombreArchivo);
					
					
					xls.setTabla(1);
					xls.setCelda("Comisi�n Recursos Propios", "formasb", ComunesXLS.CENTER, 1);
					xls.cierraTabla();
					  
					xls.setTabla(2);
					xls.setCelda("RESUMEN SUMA DE TOTALES " + despMoneda, "celda01", ComunesXLS.CENTER, 2);
					xls.setCelda("IF", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda(regTotales.get("NOMBRE_IF").toString(), "formas", ComunesXLS.CENTER, 1);
					xls.setCelda("Num Documentos", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda(regTotales.get("TOTAL_NUM_DOCUMENTOS").toString().toString(), "formas", ComunesXLS.CENTER, 1);			
					xls.setCelda("Monto de los Documentos", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("$ " + Comunes.formatoDecimal(regTotales.get("TOTAL_MONTO_DOCUMENTOS").toString(),2 ,false), "formas", ComunesXLS.CENTER, 1);
					xls.setCelda("Monto de la Comisi�n", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("$ " + Comunes.formatoDecimal(montoTotalComision.toPlainString(), 2), "formas", ComunesXLS.CENTER, 1);
					xls.cierraTabla();
					
					xls.setTabla(1);
					xls.setCelda("NOTA: IVA DEL " + porcentajeIva + " %", "formas", ComunesXLS.CENTER, 1);
					xls.cierraTabla();
					  
					xls.setTabla(7);
					xls.setCelda("DETALLE POR EPO", "celda01", ComunesXLS.CENTER, 7);
					xls.setCelda("Mes", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("EPO", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Num Documentos", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Moneda", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Monto Documentos", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("% de contribuci�n a la comisi�n", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Monto de contribuci�n a la comisi�n", "celda01", ComunesXLS.CENTER, 1);
					
					while(rs.next()){
						String  descripcionMes  =  rs.getString("DESCRIPCION_MES")==null?"":rs.getString("DESCRIPCION_MES");			
						String  nombre_EPO  =  rs.getString("NOMBRE_EPO")==null?"":rs.getString("NOMBRE_EPO");			
						String  num_documento  =  rs.getString("NUM_DOCUMENTOS")==null?"0":rs.getString("NUM_DOCUMENTOS");			
						String  moneda  =  rs.getString("MONEDA")==null?"":rs.getString("MONEDA");			
						String  montoDoc  =  rs.getString("MONTO_DOCUMENTOS")==null?"0":rs.getString("MONTO_DOCUMENTOS");			
							
						BigDecimal montoDocumentos = new BigDecimal(montoDoc);
						BigDecimal porcentajeComision = (montoDocumentos.multiply(new BigDecimal("100"))).divide(montoTotalDocumentos, 20, BigDecimal.ROUND_HALF_UP);
						BigDecimal montoComision = (montoTotalComision.multiply(porcentajeComision)).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP);
						String  contriComision  =  rs.getString("FG_CONTRI_COMISION")==null?"0":rs.getString("FG_CONTRI_COMISION");					
						
						xls.setCelda(descripcionMes, "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(nombre_EPO, "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(num_documento, "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(moneda,"formas", ComunesXLS.CENTER, 1);
						xls.setCelda(Comunes.formatoDecimal(montoDoc,2), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(porcentajeComision.toPlainString()+"%", "formas", ComunesXLS.CENTER, 1);
						if(contriComision.toString().equals("0"))  {
							xls.setCelda(Comunes.formatoDecimal(montoComision.toPlainString(),2),"formas", ComunesXLS.CENTER, 1);
						}else  {
							xls.setCelda(Comunes.formatoDecimal(contriComision,2),"formas", ComunesXLS.CENTER, 1);	
						}
					}
					
					 xls.cierraTabla();
		
					xls.cierraXLS();
	  
				}
			
			}else if ( tipoConsulta.equals("Consulta_Resumen") ) {	
			
				int i = 0;	
				if(tipo.equals("PDF") )   {			
						
					double dblGranTotalContraprestacion =  0,  dblGranTotalIva =  0,  dblGranTotalContraprestacionTotal =  0;		
					pdfDoc.setTable(7,80);	
					
					while(rs.next()){
					
						String  producto  =  rs.getString("PRODUCTO")==null?"":rs.getString("PRODUCTO");			
						String  descripcionMes  =  rs.getString("DESCRIPCION_MES")==null?"":rs.getString("DESCRIPCION_MES");			
						String  contraprestacion  =  rs.getString("CONTRAPRESTACION")==null?"0":rs.getString("CONTRAPRESTACION");			
						String  por_iva  =  rs.getString("POR_IVA")==null?"":rs.getString("POR_IVA");			
						String  monto_iva  =  rs.getString("MONTO_IVA")==null?"0":rs.getString("MONTO_IVA");	
						String  contraprestacion_total  =  rs.getString("CONTRAPRESTACION_TOTAL")==null?"0":rs.getString("CONTRAPRESTACION_TOTAL");	
						String  estatus  =  rs.getString("ESTATUS")==null?"":rs.getString("ESTATUS");
						String  nombreIF  =  rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF");	
						String fechaPago  =  rs.getString("FECHA_PAGO")==null?"":rs.getString("FECHA_PAGO");	//FODEA-039-2014
						if(i ==0){
							pdfDoc.addText("RESUMEN PARA COBRO \n Intermediario Financiero  "+nombreIF,"formas",ComunesPDF.CENTER); 					
						}
						pdfDoc.setCell(producto,"celda02",ComunesPDF.CENTER, 7);  
						pdfDoc.setCell(" Mes C�lculo","celda02",ComunesPDF.CENTER); 
						pdfDoc.setCell(" Contraprestaci�n ", "celda02",ComunesPDF.CENTER);  				
						pdfDoc.setCell(" % IVA ", "celda02",ComunesPDF.CENTER); 
						pdfDoc.setCell(" IVA ", "celda02",ComunesPDF.CENTER); 
						pdfDoc.setCell(" Contraprestacion total ", "celda02",ComunesPDF.CENTER); 
						pdfDoc.setCell(" Estatus ", "celda02",ComunesPDF.CENTER); //FODEA-039-2014
						pdfDoc.setCell(" Fecha de pago ", "celda02",ComunesPDF.CENTER); //FODEA-039-2014
						
						pdfDoc.setCell(descripcionMes, "formas",ComunesPDF.CENTER); 
						pdfDoc.setCell("$"+Comunes.formatoDecimal(contraprestacion,2)  , "formas",ComunesPDF.RIGHT); 
						pdfDoc.setCell(por_iva+"%", "formas",ComunesPDF.CENTER); 
						pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_iva,2) , "formas",ComunesPDF.RIGHT); 
						pdfDoc.setCell("$"+Comunes.formatoDecimal(contraprestacion_total,2)  , "formas",ComunesPDF.RIGHT); 
						pdfDoc.setCell(estatus, "formas",ComunesPDF.CENTER); 						
						pdfDoc.setCell(fechaPago, "formas",ComunesPDF.CENTER); 				//FODEA-039-2014		
						
						dblGranTotalContraprestacion += Double.parseDouble((String)contraprestacion);
						dblGranTotalIva += Double.parseDouble((String)monto_iva);
						dblGranTotalContraprestacionTotal += Double.parseDouble((String)contraprestacion_total);
															
						i++;
					}
			
						pdfDoc.setCell("GRAN TOTAL ", "celda02",ComunesPDF.CENTER); 
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblGranTotalContraprestacion,2)  , "formas",ComunesPDF.RIGHT); 
						pdfDoc.setCell("", "formas",ComunesPDF.CENTER); 
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblGranTotalIva,2) , "formas",ComunesPDF.RIGHT); 
						pdfDoc.setCell("$"+Comunes.formatoDecimal(dblGranTotalContraprestacionTotal,2)  , "formas",ComunesPDF.RIGHT); 
						pdfDoc.setCell("", "formas",ComunesPDF.CENTER); 						pdfDoc.setCell("", "formas",ComunesPDF.CENTER); 						
							
							
					pdfDoc.addTable();
					pdfDoc.endDocument();
					
				}else  if(tipo.equals("CSV") )   {
					
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
					writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
					buffer = new BufferedWriter(writer);
				
					contenidoArchivo = new StringBuffer();		
					
					while(rs.next()){
					
						String  producto  =  rs.getString("PRODUCTO")==null?"":rs.getString("PRODUCTO");			
						String  descripcionMes  =  rs.getString("DESCRIPCION_MES")==null?"":rs.getString("DESCRIPCION_MES");			
						String  contraprestacion  =  rs.getString("CONTRAPRESTACION")==null?"0":rs.getString("CONTRAPRESTACION");			
						String  por_iva  =  rs.getString("POR_IVA")==null?"":rs.getString("POR_IVA");			
						String  monto_iva  =  rs.getString("MONTO_IVA")==null?"0":rs.getString("MONTO_IVA");	
						String  contraprestacion_total  =  rs.getString("CONTRAPRESTACION_TOTAL")==null?"0":rs.getString("CONTRAPRESTACION_TOTAL");	
						String  estatus  =  rs.getString("ESTATUS")==null?"":rs.getString("ESTATUS");
						String  nombreIF  =  rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF");	
						String fechaPago  =  rs.getString("FECHA_PAGO")==null?"":rs.getString("FECHA_PAGO");	
						
						if(i ==0){
							contenidoArchivo.append("RESUMEN PARA COBRO \n Intermediario Financiero  "+nombreIF.replace(',',' ')+" \n");
						}
						contenidoArchivo.append(producto +"\n");
						contenidoArchivo.append(" Mes C�lculo,Contraprestaci�n, % IVA ,  IVA ,  Contraprestacion total,  Estatus, Fecha de Pago \n ");
						
						contenidoArchivo.append(descripcionMes.replace(',',' ')+", "+
														contraprestacion.replace(',',' ')+", "+
														por_iva.replace(',',' ')+", "+
														monto_iva.replace(',',' ')+", "+
														contraprestacion_total.replace(',',' ')+", "+
														estatus.replace(',',' ')+", "+
														fechaPago.replace(',',' ')+"\n ");
															
						i++;
						if(i==1000){					
							i=0;	
							buffer.write(contenidoArchivo.toString());
							contenidoArchivo = new StringBuffer();//Limpio  
						}
					}
					
					buffer.write(contenidoArchivo.toString());
					buffer.close();	
					contenidoArchivo = new StringBuffer();//Limpio    
					
				}
			
			}
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	/**
	 *  Obtengo el monto  
	 * @return 
	 * @param ic_moneda
	 * @param mes_calculo
	 * @param anio_calculo
	 * @param ic_if
	 */
	 
	
	public String  getMontoTotalComision   (String ic_if , String anio_calculo , String mes_calculo  , String ic_moneda , double iva   ){  
   	log.info("getMontoTotal (E)");
		
	   AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer strQry = new StringBuffer();
		List lVarBind		= new ArrayList();
		String montoTotal= "0";
		
		 try{
		 
			con.conexionDB();
			strQry = new StringBuffer();
		 strQry.append(" SELECT ROUND(SUM(fg_contraprestacion) * " + String.valueOf(1 + iva) + ", 2) as GranTotalMontoComision" + 
                      " FROM com_comision_fondeo_det" +
                      " WHERE ic_if =  ? " +
                      " AND ic_anio_calculo =  ? "+
                      " AND ic_mes_calculo =  ? " +                     
                      " AND ic_moneda =  ?   ");
		
			lVarBind		= new ArrayList();
         lVarBind.add(new Integer(ic_if));  
			lVarBind.add(new Integer(anio_calculo));  
			lVarBind.add(new Integer(mes_calculo));  
			lVarBind.add(new Integer(ic_moneda));  
			
			ps = con.queryPrecompilado(strQry.toString(),lVarBind );
         rs = ps.executeQuery();

			if (rs.next() && rs!=null){   
				montoTotal= (rs.getString("GranTotalMontoComision")==null?"0":rs.getString("GranTotalMontoComision"));			
         }
			rs.close();
       	ps.close();  
			
			
			

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getMontoTotal (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getMontoTotal (S)");
      return montoTotal;
    }
	 
	
	/**
	 * para los totales en el gri de Resumen de totales 
	 * * @return 
	 * @param ic_moneda
	 * @param mes_calculo
	 * @param anio_calculo
	 * @param ic_if
	 */
	public HashMap   getMontoTotales  (String ic_if , String anio_calculo , String mes_calculo  , String ic_moneda  ){  
   	log.info("getMontoTotales (E)");
		
	   AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer qrySentencia = new StringBuffer();
		List conditions		= new ArrayList();		
		 
		HashMap	registros = new HashMap();
		
		 try{
		 
			con.conexionDB();
			qrySentencia = new StringBuffer();
			
			qrySentencia.append(
				" SELECT  i.CG_RAZON_SOCIAL as  NOMBRE_IF ,  "+
				" SUM(cf.ig_doctos_vigentes) AS TOTAL_NUM_DOCUMENTOS, "+
				" SUM(cf.fg_monto_doctos) AS TOTAL_MONTO_DOCUMENTOS  "+
				
				" FROM"+
				" com_comision_fondeo cf, comcat_producto_nafin pn, "+ 
				" comcat_if i, comcat_epo e, com_comision_fondeo_det cfd,  "+
				" comcat_moneda m  "+
				" WHERE cf.ic_producto_nafin = pn.ic_producto_nafin"+
				" AND cf.ic_if = i.ic_if"+
				" AND cf.ic_epo = e.ic_epo"+
				" AND cf.ic_producto_nafin = cfd.ic_producto_nafin"+
				" AND cf.ic_if = cfd.ic_if"+
				" AND cf.ic_mes_calculo = cfd.ic_mes_calculo"+
				" AND cf.ic_anio_calculo = cfd.ic_anio_calculo"+
				" AND cf.cg_tipo_comision = cfd.cg_tipo_comision"+
				"	AND cfd.ic_moneda = cf.ic_moneda " +
				"	AND cfd.ic_banco_fondeo = cf.ic_banco_fondeo "+
				" AND cfd.ic_moneda = m.ic_moneda ");
				
				if ( !ic_if.equals("") ) {
					qrySentencia.append(" AND cf.ic_if = ? ");
					conditions.add(ic_if);
				}
				if ( !mes_calculo.equals("") ) {
					qrySentencia.append(" AND cf.ic_mes_calculo = ?  ");
					conditions.add(mes_calculo);
				}
				if ( !anio_calculo.equals("") ) {
					qrySentencia.append(" AND cf.ic_anio_calculo = ?   ");
					conditions.add(anio_calculo);
				}
				
				if ( !ic_moneda.equals("") ) {
					qrySentencia.append(" AND cfd.ic_moneda = ?  ");
					conditions.add(ic_moneda);				
				}
			
			
			qrySentencia.append(" GROUP BY i.cg_razon_social " );
				
			log.debug("qrySentencia" +qrySentencia);
			log.debug("conditions" +conditions);
			
			ps = con.queryPrecompilado(qrySentencia.toString(),conditions );
         rs = ps.executeQuery();

			if (rs.next() && rs!=null){   					
				String nombreIF=  (rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF"));	
				String numTotalDoto=  (rs.getString("TOTAL_NUM_DOCUMENTOS")==null?"0":rs.getString("TOTAL_NUM_DOCUMENTOS"));	
				String montoTotalDoto=  (rs.getString("TOTAL_MONTO_DOCUMENTOS")==null?"0":rs.getString("TOTAL_MONTO_DOCUMENTOS"));	
				
				registros.put("NOMBRE_IF",nombreIF);
				registros.put("TOTAL_NUM_DOCUMENTOS",numTotalDoto);
				registros.put("TOTAL_MONTO_DOCUMENTOS",montoTotalDoto);
				
		   }
			rs.close();
       	ps.close();  

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getMontoTotal (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getMontoTotal (S)");
      return registros;
    }  

	 /**
	 * Metodo para generar  los archivos de descarga del boton Ver Detalle 
	 * @return 
	 * @param path
	 * @param ic_moneda
	 * @param mes_calculo
	 * @param anio_calculo
	 * @param ic_if
	 */
	public HashMap   getArchivosVerDetalle   (String ic_if , String anio_calculo , String mes_calculo  , String ic_moneda , String path  ){  
   	log.info("getArchivosVerDetalle (E)");
		AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer qrySentencia = new StringBuffer();
		List conditions		= new ArrayList();		
		HashMap	registros = new HashMap();		
		String contenidoArchivoR = "", contenidoArchivoF = "", nombreArchivoR 	= "", nombreArchivoF 	= "";		
		CreaArchivo 	archivo 			= new CreaArchivo();
		
		 try{
		 
			con.conexionDB();
			
			// Archivo Comisi�n Fijo 
			qrySentencia = new StringBuffer();
			conditions		= new ArrayList();	
			qrySentencia.append(
				" SELECT tt_contenido_detalle_comision"   +
				" FROM com_comision_fondeo_det "   +
				" WHERE ic_mes_calculo = ? "   +
				" AND ic_anio_calculo = ? "   +
				" AND ic_if = ? "   +
				//" AND cg_tipo_comision = ? " + 
				" AND ic_moneda = ? " );
				conditions.add(mes_calculo);
				conditions.add(anio_calculo);	
				conditions.add(ic_if);
				//conditions.add("F");						
				conditions.add(ic_moneda);		
				
			log.debug("qrySentencia" +qrySentencia);
			log.debug("conditions" +conditions);
			ps = con.queryPrecompilado(qrySentencia.toString(),conditions );
         rs = ps.executeQuery();
			while(rs.next()) {
				Clob loClob  =  rs.getClob(1);		 
				contenidoArchivoF += loClob.getSubString(1, (int)loClob.length())+"\n";   
			}
			rs.close();
			ps.close(); 
			
			if(contenidoArchivoF.length()>1) {
				archivo.make(contenidoArchivoF, path, ".csv"); 
				nombreArchivoF = archivo.getNombre();
			}
						
			registros.put("ARCHIVO_FIJO",nombreArchivoF);

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getVerDetalle (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getVerDetalle (S)");
      return registros;
    }
	 
	 /**
 * Determina si el detalle de comisiones ya fue generado o no
 * @param strIF	Clave del IF.
 * @param strMes	Mes de calculo
 * @param strAnio	A�o de c�lculo
 * @return	true si existe el detalle o false de lo contrario
 */
	public boolean existeDetalleComision(String strIF,
			String strMes, String strAnio, String strBancoFondeo, String strTipoMoneda, String cg_tipo_comision)
			throws NafinException {

		boolean existe = false;

		PreparedStatement ps = null;
		AccesoDB con = new AccesoDB();

		int ic_mes_calculo = 0;
		int ic_anio_calculo = 0;
		int ic_if = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (strIF == null || strMes == null ||
					strAnio == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(strIF);
			ic_mes_calculo = Integer.parseInt(strMes);
			ic_anio_calculo = Integer.parseInt(strAnio);
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" strIF=" + strIF +
					" strMes=" + strMes +
					" strAnio=" + strAnio);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************
		try {
			con.conexionDB();
			String strSQL = " SELECT count(*) " +
					" FROM com_comision_fondeo_det " +
					" WHERE DBMS_LOB.getLength(tt_contenido_detalle_comision) > 1 " +
					" AND ic_if = ? " +
					" AND ic_mes_calculo = ? " +
					" AND ic_anio_calculo = ? " +
					" AND ic_moneda = ? " +
					" AND ic_banco_fondeo = ? " +
					" AND cg_tipo_comision = ? ";
					
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_if);
			ps.setInt(2, ic_mes_calculo);
			ps.setInt(3, ic_anio_calculo);
			ps.setInt(4, Integer.parseInt(strTipoMoneda));
			ps.setInt(5, Integer.parseInt(strBancoFondeo));
			ps.setString(6, cg_tipo_comision);

			ResultSet rs = ps.executeQuery();

			if(rs.next()) { 
				int numRegistros = rs.getInt(1);
				
				if (numRegistros > 0) {
					//Ya existe el contenidos del archivo.
					existe = true;
				} else {
					existe = false;
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return existe;		
	} 
	
	/**
	 * metodo para generar el Detalle 
	 * @return 
	 * @param path
	 * @param ic_moneda
	 * @param ic_mes_calculo
	 * @param ic_anio_calculo
	 * @param ic_if
	 */
	public String    getSaldos_Vigentes   (String ic_if , String ic_anio_calculo , String ic_mes_calculo  , String ic_moneda , String path  ){  
   	log.info("getGenerar_Detalle (E)");
		AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		String	condicion	= "", condicion2 = "",  qrySentencia = "", ic_banco_fondeo = "1",   strClaveEPO="";
			List conditions		= new ArrayList();		
		HashMap	registros = new HashMap();	
		String archivoGenerar_Detalle= "";
		
		// VARIABLES LOCALES
		double sumatoria_mes[] = new double[31];
		String meses[] = {"ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
		String fecha	= "",  _dia		= "",  _mes		= "", _anyio	= "",  cg_tipo_comision = "F";
		boolean existenRegistros = false;
				
		CreaArchivo archivo 	= new CreaArchivo();
		String nombreArchivo 	= "",  contenidoArchivo = "", contenidoArchivo1 = "", contenidoArchivo2 = "", contenidoArchivo4 = "", contenidoArchivo5 = "";

		 try{
		 
			con.conexionDB();
			
			//Se crea la instancia del EJB para su utilizaci�n.
			ComisionFondeoPropio comisionBean = ServiceLocator.getInstance().lookup("ComisionFondeoPropioEJB", ComisionFondeoPropio.class);

			
			String fecha2 = "01/"+ ic_mes_calculo + "/" + ic_anio_calculo; 
			double iva = ((BigDecimal)Iva.getValor(fecha2)).doubleValue();
			String comisionCadenasF ="", comisionCadenasR ="";
			String comsionDistibuidoresF ="", comsionDistibuidoresR ="";
			BigDecimal montoTotalComision = new BigDecimal("0.00");
			String queryComision = " SELECT ROUND(SUM(fg_contraprestacion) * " + String.valueOf(1 + iva) + ", 2) as GranTotalMontoComision" + 
								 " FROM com_comision_fondeo_det" +
								 " WHERE ic_if = " + ic_if +
								 " AND ic_anio_calculo = " + ic_anio_calculo +
								 " AND ic_mes_calculo = " + ic_mes_calculo + 								 
								 " AND ic_moneda = " + ic_moneda;
				 
			rs = con.queryDB(queryComision);
			if (rs.next() ) {
				montoTotalComision = new BigDecimal(rs.getString("GranTotalMontoComision"));
			}
			rs.close();
		
	  
			String queryComision2 = " SELECT cf.ic_producto_nafin as producto, cfd.cg_tipo_comision as comision, "+
									 " cfd.fg_porcentaje_aplicado, cfd.fg_contraprestacion,  "+
									 " SUM(cf.ig_doctos_vigentes) AS doctosVigentes,  "+
									 " SUM(cf.fg_monto_doctos) AS montoDocumentos  "+
									 " FROM "+
									 " com_comision_fondeo cf, comcat_producto_nafin pn, "+
									 " comcat_if i, comcat_epo e, com_comision_fondeo_det cfd "+
									 " WHERE cf.ic_producto_nafin = pn.ic_producto_nafin "+
									 " AND cf.ic_if = i.ic_if "+
									 " AND cf.ic_epo = e.ic_epo "+
									 " AND cf.ic_producto_nafin = cfd.ic_producto_nafin "+
									 " AND cf.ic_if = cfd.ic_if "+
									 " AND cf.ic_mes_calculo = cfd.ic_mes_calculo "+
									 " AND cf.ic_anio_calculo = cfd.ic_anio_calculo "+
									 " AND cf.cg_tipo_comision = cfd.cg_tipo_comision "+
									 " AND cf.ic_if = " + ic_if +
									 " AND cf.ic_mes_calculo = " + ic_mes_calculo +
									 " AND cf.ic_anio_calculo =  " + ic_anio_calculo +
									 " AND cfd.ic_moneda = cf.ic_moneda "+
									 " AND cfd.ic_banco_fondeo = cf.ic_banco_fondeo"+									
									 " AND cfd.ic_moneda = " + ic_moneda+
									 " GROUP BY  cf.ic_producto_nafin, cfd.cg_tipo_comision,"+
									 " cfd.fg_porcentaje_aplicado, cfd.fg_contraprestacion"+
									 " ORDER BY cf.ic_producto_nafin"; 
      
			System.out.println("..:: queryComision: " + queryComision2);          
			rs = con.queryDB(queryComision2);
			BigDecimal montoTotalDocumentos = new BigDecimal("0.00");
			while (rs.next() ) {
				montoTotalDocumentos  = montoTotalDocumentos.add(new BigDecimal(rs.getString("montoDocumentos")==null?"":rs.getString("montoDocumentos")));   				
			}
			rs.close();
			BigDecimal montoDocumentos = new BigDecimal("0.00");
			BigDecimal numeroDocumentos = new BigDecimal("0.00");
			BigDecimal porcentajeComision = new BigDecimal("0.00");
			BigDecimal montoComision = new BigDecimal("0.00");
			
			rs = con.queryDB(queryComision2);
			while (rs.next() ) {
			
			montoDocumentos = new BigDecimal("0.00");
			numeroDocumentos = new BigDecimal("0.00");
			porcentajeComision = new BigDecimal("0.00");
			montoComision = new BigDecimal("0.00");		
			
			if(rs.getString("producto").toString().equals("1")  ){
				montoDocumentos = new BigDecimal(rs.getString("montoDocumentos")==null?"":rs.getString("montoDocumentos")); 
				numeroDocumentos = new BigDecimal(rs.getString("doctosVigentes")==null?"":rs.getString("doctosVigentes")); 
				porcentajeComision = (montoDocumentos.multiply(new BigDecimal("100"))).divide(montoTotalDocumentos, 20, BigDecimal.ROUND_HALF_UP);
				montoComision = (montoTotalComision.multiply(porcentajeComision)).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP);
				comisionCadenasF = montoComision.toPlainString();								
			}	
		
			
			if(rs.getString("producto").toString().equals("4")  ){
				montoDocumentos = new BigDecimal(rs.getString("montoDocumentos")==null?"":rs.getString("montoDocumentos")); 
				numeroDocumentos = new BigDecimal(rs.getString("doctosVigentes")==null?"":rs.getString("doctosVigentes")); 
				porcentajeComision = (montoDocumentos.multiply(new BigDecimal("100"))).divide(montoTotalDocumentos, 20, BigDecimal.ROUND_HALF_UP);
				montoComision = (montoTotalComision.multiply(porcentajeComision)).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP);
				comsionDistibuidoresF  = montoComision.toPlainString();
			}	
			
      }
      rs.close(); 			
		
		Calendar fechaMesEval = new GregorianCalendar();
		fechaMesEval.set(Integer.parseInt(ic_anio_calculo), Integer.parseInt(ic_mes_calculo)-1, 1);
		int diaMaxMes = fechaMesEval.getActualMaximum(Calendar.DAY_OF_MONTH);
		int diaMinMes = fechaMesEval.getActualMinimum(Calendar.DAY_OF_MONTH);
		qrySentencia = 
				"   SELECT /*+ ordered index (sa IN_COM_SOLICITUD_02_NUK) use_nl(sa d iexp)*/"+
				"   		TO_CHAR(MIN(sa.df_fecha_solicitud),'dd/mm/yyyy') AS fecha_solicitud"   +
				"           FROM com_solicitud sa, com_documento d, comrel_if_epo_x_producto iexp "   +
				"          WHERE sa.ic_documento = d.ic_documento"   +
				"            AND sa.ic_estatus_solic = 10"   +
				"            AND sa.df_v_descuento >= TRUNC (TO_DATE (?, 'DD/MM/YYYY')) "   +
				"            AND sa.df_fecha_solicitud < TRUNC (TO_DATE (?, 'DD/MM/YYYY') + 1) "   +
				"            AND d.ic_if = ?	" +
				"            AND d.ic_epo = iexp.ic_epo " +
				"            AND d.ic_if  = iexp.ic_if " +
				"            AND iexp.ic_producto_nafin = 1 " +
				"            AND iexp.CG_TIPO_COMISION = ?" +
				"            AND d.ic_moneda = ? " ;
				
		System.out.println("<br> qrySentencia para la fecha:  "+qrySentencia);
		ps = con.queryPrecompilado(qrySentencia);
		String diaMinimo = new Integer(diaMinMes).toString();
		String diaMaximo = new Integer(diaMaxMes).toString();
		diaMinimo = diaMinimo.length()==1?"0"+diaMinimo:diaMinimo;
		diaMaximo = diaMaximo.length()==1?"0"+diaMaximo:diaMaximo;
		
		ps.setString(1,"01/"+ic_mes_calculo+"/"+ic_anio_calculo);
		ps.setString(2,diaMaximo+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
		ps.setInt(3,Integer.parseInt(ic_if));
		ps.setString(4,cg_tipo_comision);
		ps.setInt(5,Integer.parseInt(ic_moneda));	
		rs = ps.executeQuery();
	
		if(rs.next())
			fecha = rs.getString(1)==null?"":rs.getString("fecha_solicitud");
		rs.close();
		if(ps!=null) ps.close();
		if(!"".equals(fecha)) {	
			existenRegistros = true;
			_dia	= fecha.substring(0, 2);
			_mes	= fecha.substring(3, 5);
			_anyio	= fecha.substring(6, 10);
	
			contenidoArchivo1 += "\nSALDO VIGENTE DIARIO: DESCUENTO ";		
			contenidoArchivo1 += "\nMES DE CALCULO "+meses[Integer.parseInt(ic_mes_calculo)-1]+" "+ic_anio_calculo;
			contenidoArchivo1 += "\nMoneda,"+ (ic_moneda.equals("1") ? "Pesos" : "Dolares");		//Agregar el tipo de moneda al archivo excel
			contenidoArchivo1 += "\nMes de Operaci�n";
			for(int i=0;i<diaMaxMes;i++) {
				sumatoria_mes[i] = 0;
				String auxDia = new Integer(i+1).toString();
				auxDia = auxDia.length()==1?"0"+auxDia:auxDia;
				contenidoArchivo1 += ","+auxDia;
			}//for(int i=0;i<31;i++
			qrySentencia = "  SELECT /*+ ordered index (sa IN_COM_SOLICITUD_02_NUK) use_nl(sa d iexp)*/"+
								"	TO_CHAR(sa.df_fecha_solicitud,'YYYY/MM') ";
			for(int i=0;i<diaMaxMes;i++) {
				qrySentencia += ",SUM(CASE WHEN  TRUNC(TO_DATE(?, 'dd/mm/yyyy')) between TRUNC(sa.df_fecha_solicitud) and TRUNC(sa.df_v_descuento - 1) THEN "+
									"	  	 d.fn_monto "+
									"	ELSE  "+
									"		 0 "+
									"    END) as dia_"+(i+1);
			}
			qrySentencia += " FROM com_solicitud sa, com_documento d, comrel_if_epo_x_producto iexp  "   +
								"          WHERE sa.ic_documento = d.ic_documento"   +
								"            AND sa.ic_estatus_solic = 10"   +
								"            AND sa.df_v_descuento >= TRUNC (TO_DATE (?, 'DD/MM/YYYY')) "   +
								"            AND sa.df_fecha_solicitud < TRUNC (TO_DATE (?, 'DD/MM/YYYY') + 1) "   +
								"            AND d.ic_if = ? " +
								"            AND d.ic_epo = iexp.ic_epo " +
								"            AND d.ic_if  = iexp.ic_if " +
								"            AND iexp.ic_producto_nafin = 1 " +
								"            AND iexp.CG_TIPO_COMISION = ? " +
								"            AND d.ic_moneda = ? "   +											
								"				 GROUP BY TO_CHAR(sa.df_fecha_solicitud,'YYYY/MM') " +
								"				 ORDER BY 1 ";
			System.out.println("qrySentencia DSCTO: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			int cont = 0;
			for(int i=0;i<diaMaxMes;i++) {
				cont++;ps.setString(cont,(i+1)+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
			}
			cont++;ps.setString(cont,"01/"+ic_mes_calculo+"/"+ic_anio_calculo);
			cont++;ps.setString(cont,diaMaximo+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
			cont++;ps.setInt(cont,Integer.parseInt(ic_if));
			cont++;ps.setString(cont,cg_tipo_comision);
			cont++;ps.setInt(cont,Integer.parseInt(ic_moneda)); 	//Tipo de moneda Pesos o Dolares
			rs = ps.executeQuery();
			Calendar fechaApunt = new GregorianCalendar();
			Calendar fechaOp = new GregorianCalendar();
			fechaApunt.set(Integer.parseInt(_anyio), Integer.parseInt(_mes)-1, Integer.parseInt(_dia));
			int mes_opera = Integer.parseInt(ic_mes_calculo)+1;
			String aux_ic_anio_calculo = "";
			if(mes_opera==13) {
				mes_opera = 1;
				aux_ic_anio_calculo = new Integer(Integer.parseInt(ic_anio_calculo)+1).toString();
			} else {
				aux_ic_anio_calculo = ic_anio_calculo;
			}
			String aux_mes_opera = new Integer(mes_opera).toString();
			aux_mes_opera = aux_mes_opera.length()==1?"0"+aux_mes_opera:aux_mes_opera;
			boolean mismoMes;
			fecha = "";
			while(!aux_mes_opera.equals(_mes) || !aux_ic_anio_calculo.equals(_anyio)) {
				double monto = 0;
				fechaOp.set(Integer.parseInt(_anyio), Integer.parseInt(_mes)-1, 1);
				if("".equals(fecha)) {
					if(rs.next() )
						fecha = rs.getString(1)==null?"":rs.getString(1);
				}
				if((_anyio+"/"+_mes).equals(fecha)) {
					mismoMes = true;
				} else
					mismoMes = false;
					for(int i=0;i<diaMaxMes;i++) {
						if(i==0){
							contenidoArchivo1 += "\n"+meses[Integer.parseInt(_mes)-1] +" "+ _anyio;
						}
						if(mismoMes) 
							monto = rs.getDouble(i+2);
							sumatoria_mes[i] += monto;
						contenidoArchivo1 += ","+Comunes.formatoDecimal(monto,2,false);												
					}//for(int i=0;i<31;i++)
					fechaOp.add(Calendar.MONTH, 1);
					String f = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fechaOp.getTime());
					_mes	= f.substring(3, 5);
					_anyio	= f.substring(6, 10);
					if(mismoMes)
						fecha = "";
			}//while(!ic_mes_calculo.equals(_mes) && !ic_anio_calculo.equals(_anyio)
			rs.close();
			
			if(ps!=null) ps.close();
			contenidoArchivo1 += "\nSALDO VIGENTE POR DIA";
			double totSaldoVigente = 0;
			for(int i=0;i<diaMaxMes;i++) {
				totSaldoVigente += sumatoria_mes[i];
				contenidoArchivo1 += ","+ Comunes.formatoDecimal(sumatoria_mes[i],2,false);
			}
			contenidoArchivo1 += "\nTOTAL SALDO VIGENTE,"+Comunes.formatoDecimal(totSaldoVigente,2,false);
			
			double saldoPromedioMensual = totSaldoVigente / Double.parseDouble(new Integer(diaMaxMes).toString());
			contenidoArchivo1 += "\nSALDO PROMEDIO MENSUAL,"+Comunes.formatoDecimal(saldoPromedioMensual,2,false);
			if (cg_tipo_comision.equals("F")) {
				contenidoArchivo1 += "\nMONTO COMISION CADENAS,"+Comunes.formatoDecimal(comisionCadenasF,2,false);
			}
			
			List comision = new ArrayList();
			String porcentajeAplicado = null;
			String contraprestacion = null;
			comision = comisionBean.getComision("1", ic_if, ic_mes_calculo, ic_anio_calculo, cg_tipo_comision, saldoPromedioMensual, ic_banco_fondeo, ic_moneda, porcentajeComision.toPlainString());// strClaveEPO);
			porcentajeAplicado = (String) comision.get(0);
			contraprestacion = (String) comision.get(1);
		}//si existe fecha
			
	//***************************************++		
			
	fecha = "";
	fechaMesEval.set(Integer.parseInt(ic_anio_calculo), Integer.parseInt(ic_mes_calculo)-1, 1);
	diaMaxMes = fechaMesEval.getActualMaximum(Calendar.DAY_OF_MONTH);
	diaMinMes = fechaMesEval.getActualMinimum(Calendar.DAY_OF_MONTH);
	qrySentencia = 
		"         SELECT /*+ index(sa CP_COM_ANTICIPO_PK) index(iexp CP_COMREL_IF_EPO_X_PRODUCTO_PK) use_nl(d ds lc sa cpn iexp)*/ "+
		"			     TO_CHAR(MIN(sa.df_fecha_solicitud),'dd/mm/yyyy') AS fecha_solicitud"   +
		"           FROM com_pedido d, com_pedido_seleccionado ds, com_linea_credito lc, "   +
		"                com_anticipo sa, comcat_producto_nafin cpn, comrel_if_epo_x_producto iexp, comcat_epo epo "   +
		"          WHERE ds.ic_linea_credito = lc.ic_linea_credito"   +
		"            AND sa.ic_pedido = ds.ic_pedido"   +
		"            AND ds.ic_pedido = d.ic_pedido"   +
		"            AND lc.ic_if = iexp.ic_if"   +
		"            AND d.ic_epo = iexp.ic_epo"   +
		"            AND cpn.ic_producto_nafin = iexp.ic_producto_nafin"   +
		"            AND iexp.ic_producto_nafin = 2"   +
		"            AND sa.ic_estatus_antic = 10"   +
		"            AND sa.df_v_descuento >= TRUNC (TO_DATE (?, 'DD/MM/YYYY')) "   +
		"            AND sa.df_fecha_solicitud < TRUNC (TO_DATE (?, 'DD/MM/YYYY')+1) "   +
		"            AND iexp.cg_tipo_comision IN (?)  "   +
		"            AND lc.ic_if = ? " +
		"            AND d.ic_moneda = ? "   +		//Tipo de moneda Pesos o Dolares
		"            AND iexp.ic_epo = epo.ic_epo ";

	ps = con.queryPrecompilado(qrySentencia);
	diaMinimo = new Integer(diaMinMes).toString();
	diaMaximo = new Integer(diaMaxMes).toString();
	diaMinimo = diaMinimo.length()==1?"0"+diaMinimo:diaMinimo;
	diaMaximo = diaMaximo.length()==1?"0"+diaMaximo:diaMaximo;
	ps.setString(1,"01/"+ic_mes_calculo+"/"+ic_anio_calculo);
	ps.setString(2,diaMaximo+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
	ps.setString(3,cg_tipo_comision);
	ps.setInt(4,Integer.parseInt(ic_if));
	ps.setInt(5,Integer.parseInt(ic_moneda));
	rs = ps.executeQuery();
	if(rs.next())
		fecha = rs.getString(1)==null?"":rs.getString("fecha_solicitud");
	rs.close();
	if(ps!=null) ps.close();
	if(!"".equals(fecha)) {
		existenRegistros = true;
		_dia	= fecha.substring(0, 2);
		_mes	= fecha.substring(3, 5);
		_anyio	= fecha.substring(6, 10);
		contenidoArchivo2 += "\nSALDO VIGENTE DIARIO: PEDIDOS ";
		contenidoArchivo2 += "\nMES DE CALCULO "+meses[Integer.parseInt(ic_mes_calculo)-1]+" "+ic_anio_calculo;
		contenidoArchivo2 += "\nMoneda,"+ (ic_moneda.equals("1") ? "Pesos" : "Dolares");	//Agregar el tipo de moneda al archivo excel
		contenidoArchivo2 += "\nMes de Operaci�n";
		for(int i=0;i<diaMaxMes;i++) {
			sumatoria_mes[i] = 0;
			String auxDia = new Integer(i+1).toString();
			contenidoArchivo2 += ","+auxDia;
		}//for(int i=0;i<31;i++)
		qrySentencia = "  SELECT   /*+ index(sa CP_COM_ANTICIPO_PK) index(iexp CP_COMREL_IF_EPO_X_PRODUCTO_PK) use_nl(d ds lc sa cpn iexp)*/ "+
							"	TO_CHAR(sa.df_fecha_solicitud,'YYYY/MM') ";
		for(int i=0;i<diaMaxMes;i++) {
			qrySentencia += ",SUM(CASE WHEN TRUNC(TO_DATE(?, 'dd/mm/yyyy')) between TRUNC(sa.df_fecha_solicitud) and TRUNC(sa.df_v_descuento - 1) THEN "+
								"	  	 d.fn_monto "+
								"	ELSE  "+
								"		 0 "+
								"    END) as dia_"+(i+1);
		}
		qrySentencia += 	"    FROM com_pedido d, com_pedido_seleccionado ds, com_linea_credito lc, "   +
								"    com_anticipo sa, comcat_producto_nafin cpn, comrel_if_epo_x_producto iexp, comcat_epo epo"   +
								"    WHERE ds.ic_linea_credito = lc.ic_linea_credito"   +
								"    AND sa.ic_pedido = ds.ic_pedido"   +
								"    AND ds.ic_pedido = d.ic_pedido"   +
								"    AND lc.ic_if = iexp.ic_if"   +
								"    AND d.ic_epo = iexp.ic_epo"   +
								"    AND cpn.ic_producto_nafin = iexp.ic_producto_nafin"   +
								"    AND iexp.ic_producto_nafin = 2"   +
								"    AND sa.ic_estatus_antic = 10"   +
								"    AND sa.df_v_descuento >= TRUNC (TO_DATE (?, 'DD/MM/YYYY')) "   +
								"    AND sa.df_fecha_solicitud < TRUNC (TO_DATE (?, 'DD/MM/YYYY')+1) "   +
								"    AND iexp.cg_tipo_comision IN (?)  "   +
								"    AND lc.ic_if = ? " +
								"    AND d.ic_moneda = ? "   +
								"    AND iexp.ic_epo = epo.ic_epo "   +
								"		GROUP BY TO_CHAR(sa.df_fecha_solicitud,'YYYY/MM') " +
								"	ORDER BY 1 ";
		ps = con.queryPrecompilado(qrySentencia);
		int cont = 0;
		for(int i=0;i<diaMaxMes;i++) {
			cont++;ps.setString(cont,(i+1)+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
		}
		cont++;ps.setString(cont,"01/"+ic_mes_calculo+"/"+ic_anio_calculo);
		cont++;ps.setString(cont,diaMaximo+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
		cont++;ps.setString(cont,cg_tipo_comision);
		cont++;ps.setInt(cont,Integer.parseInt(ic_if));
		cont++;ps.setInt(cont,Integer.parseInt(ic_moneda));
		rs = ps.executeQuery();
		Calendar fechaApunt = new GregorianCalendar();
		Calendar fechaOp = new GregorianCalendar();
		fechaApunt.set(Integer.parseInt(_anyio), Integer.parseInt(_mes)-1, Integer.parseInt(_dia));
		int mes_opera = Integer.parseInt(ic_mes_calculo)+1;
		String aux_ic_anio_calculo = "";
		if(mes_opera==13) {
			mes_opera = 1;
			aux_ic_anio_calculo = new Integer(Integer.parseInt(ic_anio_calculo)+1).toString();
		} else {
			aux_ic_anio_calculo = ic_anio_calculo;
		}
		String aux_mes_opera = new Integer(mes_opera).toString();
		aux_mes_opera = aux_mes_opera.length()==1?"0"+aux_mes_opera:aux_mes_opera;
		boolean mismoMes;
		fecha = "";
		while(!aux_mes_opera.equals(_mes) || !aux_ic_anio_calculo.equals(_anyio)) {
			double monto = 0;
			fechaOp.set(Integer.parseInt(_anyio), Integer.parseInt(_mes)-1, 1);
			if("".equals(fecha)) {
				if(rs.next() )
					fecha = rs.getString(1)==null?"":rs.getString(1);
			}
			if((_anyio+"/"+_mes).equals(fecha)) {
				mismoMes = true;
			} else
				mismoMes = false;
				for(int i=0;i<diaMaxMes;i++) {
					if(i==0){
						contenidoArchivo2 += "\n"+meses[Integer.parseInt(_mes)-1] +" "+ _anyio;
					}
					if(mismoMes) 
						monto = rs.getDouble(i+2);
					sumatoria_mes[i] += monto;
					contenidoArchivo2 += ","+Comunes.formatoDecimal(monto,2,false);
				}//for(int i=0;i<31;i++)
				fechaOp.add(Calendar.MONTH, 1);
				String f = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fechaOp.getTime());
				_mes	= f.substring(3, 5);
				_anyio	= f.substring(6, 10);
				if(mismoMes)
					fecha = "";
		}//while(!ic_mes_calculo.equals(_mes) && !ic_anio_calculo.equals(_anyio))
		rs.close();
		if(ps!=null) ps.close();
		contenidoArchivo2 += "\nSALDO VIGENTE POR DIA";						
		double totSaldoVigente = 0;
		for(int i=0;i<diaMaxMes;i++) {
			totSaldoVigente += sumatoria_mes[i];
			contenidoArchivo2 += ","+Comunes.formatoDecimal(sumatoria_mes[i],2,false);
		}
		
		contenidoArchivo2 += "\nTOTAL SALDO VIGENTE,"+Comunes.formatoDecimal(totSaldoVigente,2,false);
		double saldoPromedioMensual = totSaldoVigente / Double.parseDouble(new Integer(diaMaxMes).toString());
		contenidoArchivo2 += "\nSALDO PROMEDIO MENSUAL,"+Comunes.formatoDecimal(saldoPromedioMensual,2,false);
		List comision = new ArrayList();
		String porcentajeAplicado = null;
		String contraprestacion = null;
	
		comision = comisionBean.getComision("2", ic_if, ic_mes_calculo, ic_anio_calculo, cg_tipo_comision, saldoPromedioMensual, ic_banco_fondeo, ic_moneda, porcentajeComision.toPlainString());// strClaveEPO);
		porcentajeAplicado = (String) comision.get(0);
		contraprestacion = (String) comision.get(1);
	}//si existe fecha


//***********************************************************

	fecha = "";
	fechaMesEval.set(Integer.parseInt(ic_anio_calculo), Integer.parseInt(ic_mes_calculo)-1, 1);
	diaMaxMes = fechaMesEval.getActualMaximum(Calendar.DAY_OF_MONTH);
	diaMinMes = fechaMesEval.getActualMinimum(Calendar.DAY_OF_MONTH);
	qrySentencia = 
		"         SELECT  "+
		"			     TO_CHAR(MIN(sa.df_fecha_solicitud),'dd/mm/yyyy') AS fecha_solicitud"   +
		"           FROM dis_documento d, " +
    " 	        dis_solicitud sa, " +
    " 	        comrel_if_epo_x_producto iexp " +
		"          WHERE sa.ic_documento = d.ic_documento " +
    " 	        AND sa.ic_if = iexp.ic_if " +
    " 	        AND d.ic_epo = iexp.ic_epo " +
    " 	        AND iexp.ic_producto_nafin = 4 " +
    " 	        AND sa.ic_estatus_solic = 10 " +
		"           AND sa.df_v_credito >= TRUNC (TO_DATE (?, 'DD/MM/YYYY')) "   +
		"           AND sa.df_fecha_solicitud <= TRUNC (TO_DATE (?, 'DD/MM/YYYY')+1) "   +
		"           AND iexp.cg_tipo_comision IN (?)  "   +
		"           AND sa.ic_if = ? " +
		"           AND d.ic_moneda = ? ";
	ps = con.queryPrecompilado(qrySentencia);
	diaMinimo = new Integer(diaMinMes).toString();
	diaMaximo = new Integer(diaMaxMes).toString();
	diaMinimo = diaMinimo.length()==1?"0"+diaMinimo:diaMinimo;
	diaMaximo = diaMaximo.length()==1?"0"+diaMaximo:diaMaximo;
	ps.setString(1,"01/"+ic_mes_calculo+"/"+ic_anio_calculo);
	ps.setString(2,diaMaximo+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
	ps.setString(3,cg_tipo_comision);
	ps.setInt(4,Integer.parseInt(ic_if));
	ps.setInt(5,Integer.parseInt(ic_moneda));
	rs = ps.executeQuery();
	if(rs.next())
		fecha = rs.getString(1)==null?"":rs.getString("fecha_solicitud");
	rs.close();
	if(ps!=null) ps.close();
	if(!"".equals(fecha)) {
			existenRegistros = true;
			_dia	= fecha.substring(0, 2);
			_mes	= fecha.substring(3, 5);
			_anyio	= fecha.substring(6, 10);
		contenidoArchivo4 += "\nSALDO VIGENTE DIARIO: DISTRIBUIDORES ";
		contenidoArchivo4 += "\nMES DE CALCULO "+meses[Integer.parseInt(ic_mes_calculo)-1]+" "+ic_anio_calculo;
		contenidoArchivo4 += "\nMoneda,"+ (ic_moneda.equals("1") ? "Pesos" : "Dolares");	//Agregar el tipo de moneda al archivo excel
		contenidoArchivo4 += "\nMes de Operaci�n";
		for(int i=0;i<diaMaxMes;i++) {
			sumatoria_mes[i] = 0;
			String auxDia = new Integer(i+1).toString();
			contenidoArchivo4 += ","+auxDia;
		}
		qrySentencia = "   SELECT    "+
							"	TO_CHAR(sa.df_fecha_solicitud,'YYYY/MM') ";
		for(int i=0;i<diaMaxMes;i++) {
			qrySentencia += ",SUM(CASE WHEN TRUNC(TO_DATE(?, 'dd/mm/yyyy')) between TRUNC(sa.df_fecha_solicitud) and TRUNC(sa.df_v_credito - 1) THEN "+
								"	 	 d.fn_monto "+
								"	ELSE  "+
								"		 0 "+
								"    END) as dia_"+(i+1);
		}
		qrySentencia += 	 "           FROM dis_documento d, " +
								 " 	        dis_solicitud sa, " +
								 " 	        comrel_if_epo_x_producto iexp " +
								 "          WHERE sa.ic_documento = d.ic_documento " +
								 " 	        AND sa.ic_if = iexp.ic_if " +
								 " 	        AND d.ic_epo = iexp.ic_epo " +
								 " 	        AND iexp.ic_producto_nafin = 4 " +
								 " 	        AND sa.ic_estatus_solic = 10 " +
								 "           AND sa.df_v_credito >= TRUNC (TO_DATE (?, 'DD/MM/YYYY')) "   +
								 "           AND sa.df_fecha_solicitud <= TRUNC (TO_DATE (?, 'DD/MM/YYYY')+1) "   +
								 "           AND iexp.cg_tipo_comision IN (?)  "   +
								 "           AND sa.ic_if = ? " +
								 "           AND d.ic_moneda = ? "   +
								 "		GROUP BY TO_CHAR(sa.df_fecha_solicitud,'YYYY/MM') " +
								"	ORDER BY 1 ";                    
		ps = con.queryPrecompilado(qrySentencia);
		int cont = 0;
		for(int i=0;i<diaMaxMes;i++) {
			cont++;ps.setString(cont,(i+1)+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
		}
		cont++;ps.setString(cont,"01/"+ic_mes_calculo+"/"+ic_anio_calculo);
		cont++;ps.setString(cont,diaMaximo+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
		cont++;ps.setString(cont,cg_tipo_comision);
		cont++;ps.setInt(cont,Integer.parseInt(ic_if));
		cont++;ps.setInt(cont,Integer.parseInt(ic_moneda));
		rs = ps.executeQuery();
		Calendar fechaApunt = new GregorianCalendar();
		Calendar fechaOp = new GregorianCalendar();
		fechaApunt.set(Integer.parseInt(_anyio), Integer.parseInt(_mes)-1, Integer.parseInt(_dia));
		int mes_opera = Integer.parseInt(ic_mes_calculo)+1;
		String aux_ic_anio_calculo = "";
		if(mes_opera==13) {
			mes_opera = 1;
			aux_ic_anio_calculo = new Integer(Integer.parseInt(ic_anio_calculo)+1).toString();
		} else {
			aux_ic_anio_calculo = ic_anio_calculo;
		}
		String aux_mes_opera = new Integer(mes_opera).toString();
		aux_mes_opera = aux_mes_opera.length()==1?"0"+aux_mes_opera:aux_mes_opera;
		boolean mismoMes;
		fecha = "";
		while(!aux_mes_opera.equals(_mes) || !aux_ic_anio_calculo.equals(_anyio)) {
			double monto = 0;
			fechaOp.set(Integer.parseInt(_anyio), Integer.parseInt(_mes)-1, 1);
			if("".equals(fecha)) {
				if(rs.next() )
					fecha = rs.getString(1)==null?"":rs.getString(1);
				}
				if((_anyio+"/"+_mes).equals(fecha)) {
					mismoMes = true;
				} else
					mismoMes = false;
				for(int i=0;i<diaMaxMes;i++) {
					if(i==0){
						contenidoArchivo4 += "\n"+meses[Integer.parseInt(_mes)-1] +" "+ _anyio;
					}
					if(mismoMes) 
						monto = rs.getDouble(i+2);
						sumatoria_mes[i] += monto;
						contenidoArchivo4 += ","+Comunes.formatoDecimal(monto,2,false);
				}//for(int i=0;i<31;i++)
				fechaOp.add(Calendar.MONTH, 1);
				String f = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fechaOp.getTime());
				_mes	= f.substring(3, 5);
				_anyio	= f.substring(6, 10);
				if(mismoMes)
					fecha = "";
		}//while(!ic_mes_calculo.equals(_mes) && !ic_anio_calculo.equals(_anyio))
		rs.close();
		if(ps!=null) ps.close();
		contenidoArchivo4 += "\nSALDO VIGENTE POR DIA";
		double totSaldoVigente = 0;
		for(int i=0;i<diaMaxMes;i++) {
			totSaldoVigente += sumatoria_mes[i];
			contenidoArchivo4 += ","+Comunes.formatoDecimal(sumatoria_mes[i],2,false);
		}	
		contenidoArchivo4 += "\nTOTAL SALDO VIGENTE,"+Comunes.formatoDecimal(totSaldoVigente,2,false);
		double saldoPromedioMensual = totSaldoVigente / Double.parseDouble(new Integer(diaMaxMes).toString());
		contenidoArchivo4 += "\nSALDO PROMEDIO MENSUAL,"+Comunes.formatoDecimal(saldoPromedioMensual,2,false);
		if (cg_tipo_comision.equals("F")) {
			contenidoArchivo4 += "\nMONTO COMISION DISTRIBUIDORES,"+Comunes.formatoDecimal(comsionDistibuidoresF,2,false);
		}
		
		List comision = new ArrayList();
		String porcentajeAplicado = null;
		String contraprestacion = null;
	
		comision = comisionBean.getComision("2", ic_if, ic_mes_calculo, ic_anio_calculo, cg_tipo_comision, saldoPromedioMensual, ic_banco_fondeo, ic_moneda, porcentajeComision.toPlainString());// strClaveEPO);
		porcentajeAplicado = (String) comision.get(0);
		contraprestacion = (String) comision.get(1);
	}//si existe fecha

	//*******************************************
	
	fecha = "";
	fechaMesEval.set(Integer.parseInt(ic_anio_calculo), Integer.parseInt(ic_mes_calculo)-1, 1);
	diaMaxMes = fechaMesEval.getActualMaximum(Calendar.DAY_OF_MONTH);
	diaMinMes = fechaMesEval.getActualMinimum(Calendar.DAY_OF_MONTH);
	qrySentencia = 
		"         SELECT /*+ index(sa CP_INV_SOLICITUD_PK) index(iexp CP_COMREL_IF_EPO_X_PRODUCTO_PK) use_nl(d sa lc cpn ct cm iexp)*/ "+
		"				  TO_CHAR(MIN(sa.df_autorizacion),'dd/mm/yyyy') AS fecha_solicitud"   +
		"           FROM inv_disposicion d, inv_solicitud sa, com_linea_credito lc, "   +
		"                comcat_producto_nafin cpn, comcat_tasa ct, comcat_moneda cm, comrel_if_epo_x_producto iexp, comcat_epo epo"   +
		"          WHERE d.ic_linea_credito = lc.ic_linea_credito"   +
		"            AND sa.cc_disposicion = d.cc_disposicion"   +
		"            AND lc.ic_if = iexp.ic_if"   +
		"            AND d.ic_epo = iexp.ic_epo"   +
		"            AND cpn.ic_producto_nafin = iexp.ic_producto_nafin"   +
		"            AND iexp.ic_producto_nafin = 5"   +
		"            AND sa.ic_estatus_solic = 10"   +
		"            AND d.df_vencimiento >= TRUNC (TO_DATE (?, 'DD/MM/YYYY')) "   +
		"            AND sa.df_autorizacion < TRUNC (TO_DATE (?, 'DD/MM/YYYY')+1) "   +
		"            AND ct.ic_tasa = d.ic_tasa"   +
		"            AND ct.ic_moneda = cm.ic_moneda"   +
		"            AND iexp.cg_tipo_comision IN (?)  "   +
		"            AND lc.ic_if = ? " +
		"            AND cm.ic_moneda = ? "   +
		"            AND iexp.ic_epo = epo.ic_epo "   ;
	ps = con.queryPrecompilado(qrySentencia);
	diaMinimo = new Integer(diaMinMes).toString();
	diaMaximo = new Integer(diaMaxMes).toString();
	diaMinimo = diaMinimo.length()==1?"0"+diaMinimo:diaMinimo;
	diaMaximo = diaMaximo.length()==1?"0"+diaMaximo:diaMaximo;
	ps.setString(1,"01/"+ic_mes_calculo+"/"+ic_anio_calculo);
	ps.setString(2,diaMaximo+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
	ps.setString(3,cg_tipo_comision);
	ps.setInt(4,Integer.parseInt(ic_if));
	ps.setInt(5,Integer.parseInt(ic_moneda));
	rs = ps.executeQuery();
	if(rs.next())
		fecha = rs.getString(1)==null?"":rs.getString(1);
	rs.close();
	if(ps!=null) ps.close();
	if(!"".equals(fecha)) {
		existenRegistros = true;
		_dia	= fecha.substring(0, 2);
		_mes	= fecha.substring(3, 5);
		_anyio	= fecha.substring(6, 10);
		contenidoArchivo5 += "\nSALDO VIGENTE DIARIO: CREDICADENAS ";
		contenidoArchivo5 += "\nMES DE CALCULO "+meses[Integer.parseInt(ic_mes_calculo)-1]+" "+ic_anio_calculo;
		contenidoArchivo5 += "\nMoneda,"+ (ic_moneda.equals("1") ? "Pesos" : "Dolares");	//Agregar el tipo de moneda al archivo excel
		contenidoArchivo5 += "\nMes de Operaci�n";
		for(int i=0;i<diaMaxMes;i++) {
			sumatoria_mes[i] = 0;
			String auxDia = new Integer(i+1).toString();
			contenidoArchivo5 += ","+auxDia;
		}//for(int i=0;i<31;i++)
		qrySentencia = "  SELECT   /*+ index(sa CP_INV_SOLICITUD_PK) index(iexp CP_COMREL_IF_EPO_X_PRODUCTO_PK) use_nl(d sa lc cpn ct cm iexp)*/ "+
							"			TO_CHAR(sa.df_autorizacion,'YYYY/MM') ";
		for(int i=0;i<diaMaxMes;i++) {
			qrySentencia +=",SUM(CASE WHEN  TRUNC(TO_DATE(?, 'dd/mm/yyyy')) between TRUNC(sa.df_autorizacion) and TRUNC(d.df_vencimiento - 1) THEN "+
								"	  	 d.fn_monto_credito "+
								"	ELSE  "+
								"		 0 "+
								"    END) as dia_"+(i+1);
		}
		qrySentencia +="   FROM inv_disposicion d, inv_solicitud sa, com_linea_credito lc, "   +
							"                comcat_producto_nafin cpn, comcat_tasa ct, comcat_moneda cm, comrel_if_epo_x_producto iexp, comcat_epo epo"   +
							"          WHERE d.ic_linea_credito = lc.ic_linea_credito"   +
							"            AND sa.cc_disposicion = d.cc_disposicion"   +
							"            AND lc.ic_if = iexp.ic_if"   +
							"            AND d.ic_epo = iexp.ic_epo"   +
							"            AND cpn.ic_producto_nafin = iexp.ic_producto_nafin"   +
							"            AND iexp.ic_producto_nafin = 5"   +
							"            AND sa.ic_estatus_solic = 10"   +
							"            AND d.df_vencimiento >= TRUNC (TO_DATE (?, 'DD/MM/YYYY')) "   +
							"            AND sa.df_autorizacion < TRUNC (TO_DATE (?, 'DD/MM/YYYY')+1) "   +
							"            AND ct.ic_tasa = d.ic_tasa"   +
							"            AND ct.ic_moneda = cm.ic_moneda"   +
							"            AND iexp.cg_tipo_comision IN (?)  "   +
							"            AND lc.ic_if = ? "  +
							"            AND cm.ic_moneda = ? " +				//Tipo de moneda Pesos o Dolares
							"            AND iexp.ic_epo = epo.ic_epo "   +
							"				 GROUP BY TO_CHAR(sa.df_autorizacion,'YYYY/MM') " +
							"				 ORDER BY 1 ";
		ps = con.queryPrecompilado(qrySentencia);
		int cont = 0;
		for(int i=0;i<diaMaxMes;i++) {
			cont++;ps.setString(cont,(i+1)+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
		}
		cont++;ps.setString(cont,"01/"+ic_mes_calculo+"/"+ic_anio_calculo);
		cont++;ps.setString(cont,diaMaximo+"/"+ic_mes_calculo+"/"+ic_anio_calculo);
		cont++;ps.setString(cont,cg_tipo_comision);
		cont++;ps.setInt(cont,Integer.parseInt(ic_if));
		cont++;ps.setInt(cont,Integer.parseInt(ic_moneda));
		rs = ps.executeQuery();
		Calendar fechaApunt = new GregorianCalendar();
		Calendar fechaOp = new GregorianCalendar();
		fechaApunt.set(Integer.parseInt(_anyio), Integer.parseInt(_mes)-1, Integer.parseInt(_dia));
		int mes_opera = Integer.parseInt(ic_mes_calculo)+1;
		String aux_ic_anio_calculo = "";
		if(mes_opera==13) {
			mes_opera = 1;
			aux_ic_anio_calculo = new Integer(Integer.parseInt(ic_anio_calculo)+1).toString();
		} else {
			aux_ic_anio_calculo = ic_anio_calculo;
		}
		String aux_mes_opera = new Integer(mes_opera).toString();
		aux_mes_opera = aux_mes_opera.length()==1?"0"+aux_mes_opera:aux_mes_opera;
		boolean mismoMes;
		fecha = "";
		while(!aux_mes_opera.equals(_mes) || !aux_ic_anio_calculo.equals(_anyio)) {
			double monto = 0;
			fechaOp.set(Integer.parseInt(_anyio), Integer.parseInt(_mes)-1, 1);
			if("".equals(fecha)) {
				if(rs.next() )
				fecha = rs.getString(1)==null?"":rs.getString(1);
			}
			if((_anyio+"/"+_mes).equals(fecha)) {
				mismoMes = true;
			} else
				mismoMes = false;
				for(int i=0;i<diaMaxMes;i++) {
					if(i==0){
						contenidoArchivo1 += "\n"+meses[Integer.parseInt(_mes)-1] +" "+ _anyio;
					}
					if(mismoMes) 
						monto = rs.getDouble(i+2);
						sumatoria_mes[i] += monto;
						contenidoArchivo1 += ","+Comunes.formatoDecimal(monto,2,false);
				}//for(int i=0;i<31;i++)
				fechaOp.add(Calendar.MONTH, 1);
				String f = new java.text.SimpleDateFormat("dd/MM/yyyy").format(fechaOp.getTime());
				_mes	= f.substring(3, 5);
				_anyio	= f.substring(6, 10);
				if(mismoMes)
					fecha = "";
		}//while(!ic_mes_calculo.equals(_mes) && !ic_anio_calculo.equals(_anyio))
		rs.close();
		if(ps!=null) ps.close();
		contenidoArchivo5 += "\nSALDO VIGENTE POR DIA";
		double totSaldoVigente = 0;
		for(int i=0;i<diaMaxMes;i++) {
			totSaldoVigente += sumatoria_mes[i];
			contenidoArchivo5 += ","+Comunes.formatoDecimal(sumatoria_mes[i],2,false);
		}			
		contenidoArchivo5 += "\nTOTAL SALDO VIGENTE,"+Comunes.formatoDecimal(totSaldoVigente,2,false);
		double saldoPromedioMensual = totSaldoVigente / Double.parseDouble(new Integer(diaMaxMes).toString());
		contenidoArchivo5 += "\nSALDO PROMEDIO MENSUAL,"+Comunes.formatoDecimal(saldoPromedioMensual,2,false);
	
		List comision = new ArrayList();
		String porcentajeAplicado = null;
		String contraprestacion = null;
		comision = comisionBean.getComision("5", ic_if, ic_mes_calculo, ic_anio_calculo,cg_tipo_comision, saldoPromedioMensual, ic_banco_fondeo, ic_moneda, porcentajeComision.toPlainString());// strClaveEPO);
		porcentajeAplicado = (String) comision.get(0);
		contraprestacion = (String) comision.get(1);
		
	}//si existe fecha
	existenRegistros = true;
	System.out.println("existenRegistros:: "+existenRegistros);
	System.out.println("ic_if:: "+ic_if);
	System.out.println("ic_mes_calculo:: "+ic_mes_calculo);
	System.out.println("ic_anio_calculo:: "+ic_anio_calculo);
	System.out.println("ic_banco_fondeo:: "+ic_banco_fondeo);
	System.out.println("ic_moneda:: "+ic_moneda);
	System.out.println("cg_tipo_comision:: "+cg_tipo_comision);
		
	
	if (existenRegistros && !existeDetalleComision(ic_if,ic_mes_calculo,ic_anio_calculo, ic_banco_fondeo, ic_moneda, cg_tipo_comision)) {
		if(!"".equals(contenidoArchivo1)) {
			comisionBean.grabarDetComision(contenidoArchivo1,"1",ic_if,ic_mes_calculo,ic_anio_calculo,cg_tipo_comision, ic_banco_fondeo, ic_moneda);
			System.out.println("15detcomision:: paso 1");
		}
		if(!"".equals(contenidoArchivo2)) {
			comisionBean.grabarDetComision(contenidoArchivo2,"2",ic_if,ic_mes_calculo,ic_anio_calculo,cg_tipo_comision, ic_banco_fondeo, ic_moneda);
			System.out.println("15detcomision:: paso 2");
		}
		if(!"".equals(contenidoArchivo4)) {
			comisionBean.grabarDetComision(contenidoArchivo4,"4",ic_if,ic_mes_calculo,ic_anio_calculo,cg_tipo_comision, ic_banco_fondeo, ic_moneda);
			System.out.println("15detcomision:: paso 4");
		}
		if(!"".equals(contenidoArchivo5)) {
			comisionBean.grabarDetComision(contenidoArchivo5,"5",ic_if,ic_mes_calculo,ic_anio_calculo,cg_tipo_comision, ic_banco_fondeo, ic_moneda);
			System.out.println("15detcomision:: paso 5");
		}
		
		if(!"".equals(contenidoArchivo1))
			contenidoArchivo += contenidoArchivo1;
		if(!"".equals(contenidoArchivo2)) {
			if(!"".equals(contenidoArchivo))
				contenidoArchivo += "\n \n \n ";
			contenidoArchivo += contenidoArchivo2;
		}
		if(!"".equals(contenidoArchivo4)) {
			if(!"".equals(contenidoArchivo))
				contenidoArchivo += "\n \n \n ";
			contenidoArchivo += contenidoArchivo4;
		}
		if(!"".equals(contenidoArchivo5)) {
			if(!"".equals(contenidoArchivo))
				contenidoArchivo += "\n \n \n ";
			contenidoArchivo += contenidoArchivo5;
		}
		archivo.make(contenidoArchivo, path, ".csv");
		nombreArchivo = archivo.getNombre();	
			
	}	
	
		log.info("nombreArchivo ---------------> "+nombreArchivo);
      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);			
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getGenerar_Detalle (S)");
      return nombreArchivo;
    }
	 
	 public String generarArchivoDetalles(String ic_if ,  String anio_calculo ,  String mes_calculo  , String ic_moneda , String strDirectorioTemp){
		log.info("generarArchivoDetalles(E)");
		AccesoDB con  = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		StringBuffer qrySentencia = new StringBuffer("");
		String nombreArchivo= "";
		List conditions		= new ArrayList();		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		try{
			con.conexionDB();
			qrySentencia.append("SELECT /*+ leading(sa) use_nl(sa d)  */	\n"+
									"		 e.CG_RAZON_SOCIAL as epo,	\n"+
									"		 p.CG_RAZON_SOCIAL as pyme,	\n"+
									"		 m.CD_NOMBRE as moneda,	\n"+
									"		 d.IG_NUMERO_DOCTO as num_docto,	\n"+
									"		 d.FN_MONTO as monto,	\n"	+	
									"		 to_char(sa.df_fecha_solicitud, 'dd/mm/yyyy') as fecha_alta,	\n"+
									"		 to_char(sa.df_v_descuento, 'dd/mm/yyyy') as fecha_vencimiento	\n"+
									"	FROM	\n"+
									"		 com_documento d	\n"+
									"		 ,com_solicitud sa	\n"+
									"		 ,comcat_epo e	\n"+
									"	    ,comcat_pyme p	\n"+
									"		 ,comcat_moneda m	\n"+
									"	WHERE	\n"+
									"    sa.ic_documento = d.ic_documento    \n"   +
									"    AND d.ic_epo = e.ic_epo    \n"   +
									"    AND d.ic_pyme = p.ic_pyme    \n"   +
									"    AND d.ic_moneda = m.ic_moneda    \n"   +
									"    and sa.IC_ESTATUS_SOLIC = 10    \n"   +
									"	  AND sa.df_v_descuento >= trunc(TO_DATE(?, 'MM/YYYY'))	\n"+////TOMAR EL PRIMER DIA DEL MES
									"		AND sa.df_fecha_solicitud < last_day(to_date(?,'mm/yyyy'))+1	\n"	+//TOMAR EL ULTIMO DIA DEL MES
									"	  AND d.ic_moneda = ?	\n"	+
									"    AND d.ic_if = ?	");
									
			qrySentencia.append("\nUNION ALL	\n");
			//Query Distribuidores
			qrySentencia.append("SELECT	\n"+
									"		 e.CG_RAZON_SOCIAL as epo,	\n"+
									"		 p.CG_RAZON_SOCIAL as pyme,	\n"+
									"		 m.CD_NOMBRE as moneda,	\n"+
									"		 d.IG_NUMERO_DOCTO as num_docto,	\n"+
									"		 d.FN_MONTO as monto,	\n"	+	
									"		 to_char(sa.df_fecha_solicitud, 'dd/mm/yyyy') as fecha_alta,	\n"+
									"		 to_char(sa.df_v_credito, 'dd/mm/yyyy') as fecha_vencimiento	\n"+
									"	FROM	\n"+
									"		 dis_documento d	\n"+
									"		 ,dis_solicitud sa	\n"+
									"		 ,comcat_epo e	\n"+
									"	    ,comcat_pyme p	\n"+
									"		 ,comcat_moneda m	\n"+
									"	WHERE	\n"+
									"    sa.ic_documento = d.ic_documento    \n"   +
									"    AND d.ic_epo = e.ic_epo    \n"   +
									"    AND d.ic_pyme = p.ic_pyme    \n"   +
									"    AND d.ic_moneda = m.ic_moneda    \n"   +
									"    AND d.ic_producto_nafin = 4    \n"   +
									"    and sa.IC_ESTATUS_SOLIC = 10    \n"   +
									"	  AND sa.df_v_credito >= trunc(TO_DATE(?, 'MM/YYYY'))	\n"+////TOMA EL PRIMER DIA DEL MES
									"	  AND sa.df_fecha_solicitud < last_day(to_date(?,'mm/yyyy'))+1	\n"	+//TOMAR EL ULTIMO DIA DEL MES
									"	  AND d.ic_moneda = ?	\n"	+
									"    AND sa.ic_if = ?	");
			
			
			log.info("qrySentencia :"+qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setString(1,mes_calculo+"/"+anio_calculo);
			ps.setString(2,mes_calculo+"/"+anio_calculo);
			ps.setString(3,ic_moneda);
			ps.setString(4,ic_if);
			ps.setString(5,mes_calculo+"/"+anio_calculo);
			ps.setString(6,mes_calculo+"/"+anio_calculo);
			ps.setString(7,ic_moneda);
			ps.setString(8,ic_if);
			rs = ps.executeQuery();
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(strDirectorioTemp + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
			int total = 0;
			
			contenidoArchivo.append( "Nombre EPO,PYME,Moneda,N�mero de Documento,Monto,Fecha de Alta,Fecha de Vencimiento\n");
			int numRegistros = 0;
			while(rs.next()){
				String epo = rs.getString("EPO")==null?"":rs.getString("EPO");
				String pyme = rs.getString("PYME")==null?"":rs.getString("PYME");
				String moneda = rs.getString("MONEDA")==null?"":rs.getString("MONEDA");
				String numDocto = rs.getString("NUM_DOCTO")==null?"":rs.getString("NUM_DOCTO");
				String monto = rs.getString("MONTO")==null?"":rs.getString("MONTO");
				String fechaAlta = rs.getString("FECHA_ALTA")==null?"":rs.getString("FECHA_ALTA");
				String fechaVencimiento = rs.getString("FECHA_VENCIMIENTO")==null?"":rs.getString("FECHA_VENCIMIENTO");
				
				contenidoArchivo.append(epo.replaceAll(",",""));
				contenidoArchivo.append(",");
				contenidoArchivo.append(pyme.replaceAll(",",""));
				contenidoArchivo.append(",");
				contenidoArchivo.append(moneda.replaceAll(",",""));
				contenidoArchivo.append(",");
				contenidoArchivo.append("'"+numDocto.replaceAll(",",""));
				contenidoArchivo.append(",");
				contenidoArchivo.append(monto.replaceAll(",",""));
				contenidoArchivo.append(",");
				contenidoArchivo.append(fechaAlta.replaceAll(",",""));
				contenidoArchivo.append(",");
				contenidoArchivo.append(fechaVencimiento.replaceAll(",",";"));
				contenidoArchivo.append("\n");
				numRegistros++;
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}	
				
			}
			if(numRegistros==0){
				//no hay datos
				contenidoArchivo.append("NO HAY REGISTROS\n");
			}
			buffer.write(contenidoArchivo.toString());
			total = 0;
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			/*
			CreaArchivo archivo = new CreaArchivo();
			if(archivo.make(contenidoArchivo.toString(), strDirectorioTemp,".csv")){
				nombreArchivo = archivo.getNombre();	
			}*/
			
		}catch(Exception e){
			log.error("generarArchivoDetalles(Catch)::Error "+e.getMessage());
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		log.info("generarArchivoDetalles(S)");
		return nombreArchivo;
	 }
	
 	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getMes_calculo() {
		return mes_calculo;
	}

	public void setMes_calculo(String mes_calculo) {
		this.mes_calculo = mes_calculo;
	}

	public String getAnio_calculo() {
		return anio_calculo;
	}

	public void setAnio_calculo(String anio_calculo) {
		this.anio_calculo = anio_calculo;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getDescripcionMes() {
		return descripcionMes;
	}

	public void setDescripcionMes(String descripcionMes) {
		this.descripcionMes = descripcionMes;
	}

	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public String getPorcentajeIva() {
		return porcentajeIva;
	}

	public void setPorcentajeIva(String porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}

	public String getDespMoneda() {
		return despMoneda;
	}

	public void setDespMoneda(String despMoneda) {
		this.despMoneda = despMoneda;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(String fechaFinal) { 
		this.fechaFinal = fechaFinal;
	}

  


 
	
		

}