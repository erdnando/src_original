package com.netro.cadenas;
import com.netro.pdf.ComunesPDF;
import com.netro.xlsx.ComunesXLSX;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.PreparedStatement;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.util.*;
import com.netro.parametrosgrales.*;

public class ParamInterfDesc1erPiso implements IQueryGeneratorRegExtJS  {
	
		private final static Log log = ServiceLocator.getInstance().getLog(ParamInterfDesc1erPiso.class);
		String sTipoInterfaz;
		String sNombreProducto; 
		String listParam;
		String ic_epo;
		private List 		conditions;
		StringBuffer qrySentencia = new StringBuffer("");
	
		public ParamInterfDesc1erPiso() {}
		
		
		public String getDocumentQuery() {
			log.info("getDocumentQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			log.info("qrySentencia   "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentQuery(S)");
			return qrySentencia.toString();
		}
		
		
		public String getDocumentSummaryQueryForIds(List ids){
			log.info("getDocumentSummaryQueryForIds(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentSummaryQueryForIds(S)");
			return qrySentencia.toString();
		}
	
		
		public String getAggregateCalculationQuery(){
			log.info("getAggregateCalculationQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getAggregateCalculationQuery(S)");
			return qrySentencia.toString();	
		}
		
		public List getConditions(){
			return conditions;
		}
		
	
		public String getDocumentQueryFile(){
			log.info("getDocumentQueryFile(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			String campo = "";
			String tabla = "";
			String condicion = "";
			if(!ic_epo.equals("")){
				campo = ", cpid.ig_param_campo ";
				tabla = ", com_param_interfase_desc cpid ";
				condicion = " AND mi.ic_parametro = cpid.ic_parametro(+)" +
								" AND mi.cc_api = cpid.cc_api(+)" +
								" AND mi.ic_producto = cpid.cc_producto(+)" +
								" AND cpid.IC_EPO(+) = "+ic_epo;
			}
			
			qrySentencia.append(  " select rpa.ic_parametro AS IC_PARAMETRO, "+
					" rpa.cg_nombre AS NOMBRE_PARAM, mi.cc_api " +campo+
						((sNombreProducto.equals("1C")||sNombreProducto.equals("T"))?", MI.ic_producto ":"")+
					" from " +
					" (" +
					"     SELECT   ic_parametro, cc_api,IG_PARAM_CAMPO," +
						"     max(decode(cc_producto, '1C', cc_producto, null)) as ic_producto" +
					"     from com_matriz_interface" +
					"     where ic_parametro is not null" +
					((!sTipoInterfaz.equals("T"))?"     and cc_api = '"+sTipoInterfaz+"'":"") +
					((!sNombreProducto.equals("T"))?"     and cc_producto = '"+sNombreProducto+"'":"") +
					"     AND IG_PARAM_CAMPO IS NOT NULL " +
					"		 AND ic_parametro != 36 "+
					"     GROUP BY ic_parametro, cc_api,IG_PARAM_CAMPO" +
					"     order by cc_api, ic_parametro" +
					" ) MI, comrel_parametro_api RPA, comcat_api CA " +tabla+
					" where RPA.ic_parametro = MI.ic_parametro" +
					" and RPA.cc_api = MI.cc_api(+)" +
					" and RPA.cc_api = CA.cc_api" +condicion+
					" order by CA.ig_orden, RPA.ic_parametro"	);
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentQueryFile(S)");
			return qrySentencia.toString();
		}
		
		
		public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rsAPI, String path, String tipo){
			log.debug("crearCustomFile (E)");
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
			CreaArchivo creaArchivo = new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer();
		
			try{
			
			} catch(Exception e) { 
				log.error("Error en la generacion del Archivo CSV: " + e);
			}	
			return nombreArchivo;		 
		}
			
		
		private String getQuitarEtiquetas(String sCadenaConFormato, String sEtiqueta) {
			String sCadena = sCadenaConFormato.replace(',',' ');
			int iIndice = sCadena.indexOf(sEtiqueta);
			while(iIndice!=-1) {
				sCadena = sCadena.substring(0, iIndice).concat(" ").concat(sCadena.substring(iIndice+sEtiqueta.length(), sCadena.length()));
				iIndice = sCadena.indexOf(sEtiqueta);
			}
			return sCadena;
	}
		 
			
		
		public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
		
			CreaArchivo creaArchivo = new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer();
		
			try {
		
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally {
			try {
				
			} catch(Exception e) {}
		  
			}
				return nombreArchivo;
		}
	/**
	 * 
	 * @return 
	 * @param IC_EPO
	 * @param CC_API
	 * @param CC_PRODUCTO
	 */
		public String getExisteParametrizacion(String  CC_PRODUCTO,String  CC_API,String  IC_EPO ){
		log.info("getExisteParametrizacion(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			StringBuffer condicion  = new StringBuffer("");
			AccesoDB con = new AccesoDB(); 
			PreparedStatement ps = null;
			ResultSet rs = null;
			String resultado = "";  
			try{
				con.conexionDB();
				qrySentencia.append(" select decode(count(IC_EPO),0,'N','S') AS EXISTE_PARAM ");
				qrySentencia.append(" from com_param_interfase_desc  ");
				qrySentencia.append(" where CC_PRODUCTO = '"+CC_PRODUCTO+"' ");
				if(!CC_API.equals("")||!CC_API.equals("T")){
					qrySentencia.append(" AND CC_API = '"+CC_API+"' ");
				}
				
				qrySentencia.append(" AND IC_EPO = "+IC_EPO+" ");
				
				ps = con.queryPrecompilado(qrySentencia.toString(),conditions );
		      rs = ps.executeQuery();
				log.debug("qrySentencia "+qrySentencia);
				log.debug("conditions "+conditions);
				if (rs.next()){
					resultado = rs.getString("EXISTE_PARAM")==null?"":rs.getString("EXISTE_PARAM");
				}
				rs.close();
				ps.close();
			} catch (Exception e) {
				log.error("getExisteParametrizacion  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			} 	
		
			log.debug("qrySentencia getExisteParametrizacion:  "+qrySentencia.toString());
			log.debug("conditions "+conditions);
			
			log.info("getExisteParametrizacion (S)  resultado "+resultado);
			return resultado;
	}
	/**
	 * Metodo para generar generar los archivos de descarga 
	 */
	public String getArchivo(String  tipo,String  api,String  epo, String sNomProducto, String moneda , String path, String strPais, String iNoNafinElectronico, String strNombre, String strNombreUsuario, String strLogo, String strDirectorioPublicacion, String sesExterno){
		log.info("getArchivo(E)");
		log.info("tipo "+tipo);
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		String campo = "";
		String tabla = "";
		AccesoDB con = new AccesoDB(); 
		PreparedStatement ps = null;
		ResultSet rs = null;
		String condicion = "";
		
		Map hmInf = new HashMap();
		List listValPY = new ArrayList();
		HashMap datoPY = new HashMap();
		List listValCN = new ArrayList();
		HashMap datoCN = new HashMap();
		List listValPR = new ArrayList();
		HashMap datoPR = new HashMap();
		List listValDS = new ArrayList();
		HashMap datoDS = new HashMap();
		
		String linea = "";  
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		
		ComunesXLSX documentoXLSX = null;
		Writer      writer        = null;
		
		try{
			con.conexionDB();
				
			if(!ic_epo.equals("")){
				campo = ", cpid.ig_param_campo ";
				tabla = ", com_param_interfase_desc cpid ";
				condicion = " AND mi.ic_parametro = cpid.ic_parametro(+)" +
								" AND mi.cc_api = cpid.cc_api(+)" +
								" AND mi.ic_producto = cpid.cc_producto(+)" +
								" AND cpid.IC_EPO(+) = "+ic_epo;
			}
				
			qrySentencia.append(  " select rpa.ic_parametro AS IC_PARAMETRO, "+
					" rpa.cg_nombre AS NOMBRE_PARAM, mi.cc_api " +campo+
						((sNombreProducto.equals("1C")||sNombreProducto.equals("T"))?", MI.ic_producto ":"")+
					" from " +
					" (" +
					"     SELECT   ic_parametro, cc_api,IG_PARAM_CAMPO," +
						"     max(decode(cc_producto, '1C', cc_producto, null)) as ic_producto" +
					"     from com_matriz_interface" +
					"     where ic_parametro is not null" +
					
					((!sTipoInterfaz.equals("T"))&&(!sTipoInterfaz.equals("") )?"     and cc_api = '"+sTipoInterfaz+"'":"") +
					
					((!sNombreProducto.equals("T"))?"     and cc_producto = '"+sNombreProducto+"'":"") +
					"     AND IG_PARAM_CAMPO IS NOT NULL " +
					"		 AND ic_parametro != 36 "+
					"     GROUP BY ic_parametro, cc_api,IG_PARAM_CAMPO" +
					"     order by cc_api, ic_parametro" +
					" ) MI, comrel_parametro_api RPA, comcat_api CA " +tabla+
					" where RPA.ic_parametro = MI.ic_parametro" +
					" and RPA.cc_api = MI.cc_api(+)" +
					" and RPA.cc_api = CA.cc_api" +condicion+
					" order by CA.ig_orden, RPA.ic_parametro"	);
					
				ps = con.queryPrecompilado(qrySentencia.toString(),conditions );
				rs = ps.executeQuery();
				log.debug("qrySentencia "+qrySentencia);
				log.debug("conditions "+conditions);
				
				listValPY = new ArrayList();
				listValCN = new ArrayList();
				listValPR = new ArrayList();
				listValDS = new ArrayList();
				int		ctaRegistros	= 0;
				boolean hayDatos = false;
				while (rs.next()){
					String Valapi = rs.getString("CC_API")==null?"":rs.getString("CC_API");
					if((Valapi.equals("PY")&&api.equals("T"))||(Valapi.equals("PY")&&api.equals(""))||Valapi.equals("PY")){
						datoPY = new HashMap();
						datoPY.put("IC_PARAMETRO",rs.getString("IC_PARAMETRO")==null?"":rs.getString("IC_PARAMETRO"));
						datoPY.put("NOMBRE_PARAM",rs.getString("NOMBRE_PARAM")==null?"":rs.getString("NOMBRE_PARAM"));
						datoPY.put("CC_API",rs.getString("CC_API")==null?"":rs.getString("CC_API"));
						datoPY.put("IG_PARAM_CAMPO",rs.getString("IG_PARAM_CAMPO")==null?"":rs.getString("IG_PARAM_CAMPO"));
						datoPY.put("IC_PRODUCTO",rs.getString("IC_PRODUCTO")==null?"":rs.getString("IC_PRODUCTO"));
						listValPY.add(datoPY);
					} if((Valapi.equals("CN")&&api.equals("T"))||(Valapi.equals("CN")&&api.equals(""))||Valapi.equals("CN")){
						datoCN = new HashMap();
						datoCN.put("IC_PARAMETRO",rs.getString("IC_PARAMETRO")==null?"":rs.getString("IC_PARAMETRO"));
						datoCN.put("NOMBRE_PARAM",rs.getString("NOMBRE_PARAM")==null?"":rs.getString("NOMBRE_PARAM"));
						datoCN.put("CC_API",rs.getString("CC_API")==null?"":rs.getString("CC_API"));
						datoCN.put("IG_PARAM_CAMPO",rs.getString("IG_PARAM_CAMPO")==null?"":rs.getString("IG_PARAM_CAMPO"));
						datoCN.put("IC_PRODUCTO",rs.getString("IC_PRODUCTO")==null?"":rs.getString("IC_PRODUCTO"));
						listValCN.add(datoCN);
					} if((Valapi.equals("PR")&&api.equals("T"))||(Valapi.equals("PR")&&api.equals(""))||Valapi.equals("PR")){
						datoPR = new HashMap();
						datoPR.put("IC_PARAMETRO",rs.getString("IC_PARAMETRO")==null?"":rs.getString("IC_PARAMETRO"));
						datoPR.put("NOMBRE_PARAM",rs.getString("NOMBRE_PARAM")==null?"":rs.getString("NOMBRE_PARAM"));
						datoPR.put("CC_API",rs.getString("CC_API")==null?"":rs.getString("CC_API"));
						datoPR.put("IG_PARAM_CAMPO",rs.getString("IG_PARAM_CAMPO")==null?"":rs.getString("IG_PARAM_CAMPO"));
						datoPR.put("IC_PRODUCTO",rs.getString("IC_PRODUCTO")==null?"":rs.getString("IC_PRODUCTO"));
						listValPR.add(datoPR);
					} if((Valapi.equals("DS")&&api.equals("T"))||(Valapi.equals("DS")&&api.equals(""))||Valapi.equals("DS")){
						datoDS = new HashMap();
						datoDS.put("IC_PARAMETRO",rs.getString("IC_PARAMETRO")==null?"":rs.getString("IC_PARAMETRO"));
						datoDS.put("NOMBRE_PARAM",rs.getString("NOMBRE_PARAM")==null?"":rs.getString("NOMBRE_PARAM"));
						datoDS.put("CC_API",rs.getString("CC_API")==null?"":rs.getString("CC_API"));
						datoDS.put("IG_PARAM_CAMPO",rs.getString("IG_PARAM_CAMPO")==null?"":rs.getString("IG_PARAM_CAMPO"));
						datoDS.put("IC_PRODUCTO",rs.getString("IC_PRODUCTO")==null?"":rs.getString("IC_PRODUCTO"));
						listValDS.add(datoDS);
					}
					ctaRegistros++;
					hayDatos = true;
				}
				rs.close();
				ps.close();
				try {
					if ("XLS".equals(tipo)) {
						// Estilos en la plantilla xlsx
						String pathPlantilla =  strDirectorioPublicacion + "00archivos/15cadenas/15mantenimiento/";
						HashMap estilos = new HashMap();
						estilos.put("PERCENT",       "1");
						estilos.put("COEFF",         "2");
						estilos.put("CURRENCY",      "3");
						estilos.put("DATE",          "4");
						estilos.put("HEADER",        "5");
						estilos.put("CSTRING",       "6");
						estilos.put("TITLE",         "7");
						estilos.put("SUBTITLE",      "8");
						estilos.put("RIGHTCSTRING",  "9");
						estilos.put("CENTERCSTRING","10");
						estilos.put("LEFTCSTRING",  "13");
						
						documentoXLSX = new ComunesXLSX(path, estilos);
						// Crear Detalle Comisiones
						writer = documentoXLSX.creaHoja();
						documentoXLSX.setLongitudesColumna(new String[]{
							"40.7109375",
							"30.7109375",
							"20.7109375"
						});
						short	indiceRenglon 	= 0;
						short	indiceCelda		= 0;
						documentoXLSX.agregaRenglon(indiceRenglon++); 
						indiceCelda		= 0;
						documentoXLSX.agregaCelda(indiceCelda++,"Descuento Electr�nico 1er Piso Factoraje ", "LEFTCSTRING");
						documentoXLSX.finalizaRenglon();
						
						documentoXLSX.agregaRenglon(indiceRenglon++); 
						indiceCelda		= 0;
						documentoXLSX.agregaCelda(indiceCelda++,epo, "LEFTCSTRING");
						documentoXLSX.finalizaRenglon();
						
						documentoXLSX.agregaRenglon(indiceRenglon++); 
						indiceCelda		= 0;
						documentoXLSX.agregaCelda(indiceCelda++,moneda, "LEFTCSTRING");
						documentoXLSX.finalizaRenglon();
						
						documentoXLSX.agregaRenglon(indiceRenglon++); 
						indiceCelda		= 0;
						documentoXLSX.agregaCelda(indiceCelda++,"", "CSTRING");
						documentoXLSX.finalizaRenglon();
						
						documentoXLSX.agregaRenglon(indiceRenglon++); 
						indiceCelda		= 0;
						documentoXLSX.agregaCelda(indiceCelda++,"N�mero de Par�metro", "CSTRING");
						documentoXLSX.agregaCelda(indiceCelda++,"Nombre", "CSTRING");
						documentoXLSX.agregaCelda(indiceCelda++,"Par�metro del Campo", "CSTRING");
						documentoXLSX.finalizaRenglon();
						
						
						if(listValPY.size()>0){
							documentoXLSX.agregaRenglon(indiceRenglon++); 
							indiceCelda		= 0;
							documentoXLSX.agregaCelda(indiceCelda++,"PROYECTOS", "LEFTCSTRING");
							documentoXLSX.finalizaRenglon();
						
							for(int i = 0; i< listValPY.size(); i++){
								HashMap datos = new HashMap();	
								datos =(HashMap)listValPY.get(i);
							
								documentoXLSX.agregaRenglon(indiceRenglon++); 
								indiceCelda		= 0;
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("IC_PARAMETRO"), "CENTERCSTRING");
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("NOMBRE_PARAM"), "CSTRING");
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("IG_PARAM_CAMPO"), "CENTERCSTRING");
								documentoXLSX.finalizaRenglon();
							
							}
						}
						
						
						if(listValCN.size()>0){
							documentoXLSX.agregaRenglon(indiceRenglon++); 
							indiceCelda		= 0;
							documentoXLSX.agregaCelda(indiceCelda++,"CONTRATOS", "LEFTCSTRING" );
							documentoXLSX.finalizaRenglon();
						
							for(int i = 0; i< listValCN.size(); i++){
								HashMap datos = new HashMap();	
								datos =(HashMap)listValCN.get(i);
								
								documentoXLSX.agregaRenglon(indiceRenglon++); 
								indiceCelda		= 0;
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("IC_PARAMETRO"), "CENTERCSTRING");
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("NOMBRE_PARAM"), "CSTRING");
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("IG_PARAM_CAMPO"), "CENTERCSTRING");
								documentoXLSX.finalizaRenglon();
						
							}
						}
						
						
						if(listValPR.size()>0){
							documentoXLSX.agregaRenglon(indiceRenglon++); 
							indiceCelda		= 0;
							documentoXLSX.agregaCelda(indiceCelda++,"PRESTAMOS", "LEFTCSTRING");
							documentoXLSX.finalizaRenglon();
						
							for(int i = 0; i< listValPR.size(); i++){
								HashMap datos = new HashMap();	
								datos =(HashMap)listValPR.get(i);
								documentoXLSX.agregaRenglon(indiceRenglon++); 
								indiceCelda		= 0;
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("IC_PARAMETRO"), "CENTERCSTRING");
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("NOMBRE_PARAM"), "CSTRING");
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("IG_PARAM_CAMPO"), "CENTERCSTRING");
								documentoXLSX.finalizaRenglon();
							}
						}
						
						
						if(listValDS.size()>0){
							documentoXLSX.agregaRenglon(indiceRenglon++); 
							indiceCelda		= 0;
							documentoXLSX.agregaCelda(indiceCelda++,"DISPOSICIONES", "LEFTCSTRING");
							documentoXLSX.finalizaRenglon();
						
							for(int i = 0; i< listValDS.size(); i++){
								HashMap datos = new HashMap();	
								datos =(HashMap)listValDS.get(i);
								
								documentoXLSX.agregaRenglon(indiceRenglon++); 
								indiceCelda		= 0;
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("IC_PARAMETRO"), "CENTERCSTRING");
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("NOMBRE_PARAM"), "CSTRING");
								documentoXLSX.agregaCelda(indiceCelda++,(String)datos.get("IG_PARAM_CAMPO"), "CENTERCSTRING");
								documentoXLSX.finalizaRenglon();
							}
						}
						if( ctaRegistros % 250 == 0 ){
							documentoXLSX.flush();
							ctaRegistros = 1;
						}
						if(!hayDatos){
				
							documentoXLSX.agregaRenglon(indiceRenglon++); 
							indiceCelda		= 0;
							documentoXLSX.agregaCelda(indiceCelda++,"No se encontr� ning�n registro.","HEADER");
							documentoXLSX.finalizaRenglon();
							
						}
						// Finalizar Hoja
						documentoXLSX.finalizaHoja(); 
							nombreArchivo = documentoXLSX.finalizaDocumento( pathPlantilla+"interDesc.template.xlsx" );
 
						System.out.println("nombreArchivo ***    "+nombreArchivo);
					}else if ("PDF".equals(tipo)) {
						nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
						ComunesPDF pdfDoc = new ComunesPDF(1, path + nombreArchivo);
						
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
					
						pdfDoc.encabezadoConImagenes(pdfDoc,strPais,iNoNafinElectronico,"",strNombre,strNombreUsuario,strLogo,(strDirectorioPublicacion));	
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					
						pdfDoc.setTable(2, 80);
						pdfDoc.setCell("Producto","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell("Descuento Electr�nico 1er Piso Factoraje","formas",ComunesPDF.LEFT);
						pdfDoc.setCell("Nombre de la EPO","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell(epo,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell("Moneda","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.LEFT);
						pdfDoc.addTable();
						
						
						
						if(listValPY.size()>0){
							pdfDoc.addText("\n");
							pdfDoc.addText("Proyectos");
							pdfDoc.setTable(3, 100);
							pdfDoc.setCell("N�mero de Par�metro","celda01",ComunesPDF.LEFT);
							pdfDoc.setCell("Nombre","celda01",ComunesPDF.LEFT);
							pdfDoc.setCell("Par�metros del Campo","celda01",ComunesPDF.LEFT);
							for(int i = 0; i< listValPY.size(); i++){
								HashMap datos = new HashMap();	
								datos =(HashMap)listValPY.get(i);
								pdfDoc.setCell((String)datos.get("IC_PARAMETRO"),"formas",ComunesPDF.CENTER);
								pdfDoc.setCell((String)datos.get("NOMBRE_PARAM"),"formas",ComunesPDF.LEFT);
								pdfDoc.setCell((String)datos.get("IG_PARAM_CAMPO"),"formas",ComunesPDF.CENTER);
							}
							pdfDoc.addTable();
						}
						
						
						
						if(listValCN.size()>0){
							pdfDoc.addText("\n");
							pdfDoc.addText("Contratos ");
							pdfDoc.setTable(3, 100);
							pdfDoc.setCell("N�mero de Par�metro","celda01",ComunesPDF.LEFT);
							pdfDoc.setCell("Nombre","celda01",ComunesPDF.LEFT);
							pdfDoc.setCell("Par�metros del Campo","celda01",ComunesPDF.LEFT);
							for(int i = 0; i< listValCN.size(); i++){
								HashMap datos = new HashMap();	
								datos =(HashMap)listValCN.get(i);
								pdfDoc.setCell((String)datos.get("IC_PARAMETRO"),"formas",ComunesPDF.CENTER);
								pdfDoc.setCell((String)datos.get("NOMBRE_PARAM"),"formas",ComunesPDF.LEFT);
								pdfDoc.setCell((String)datos.get("IG_PARAM_CAMPO"),"formas",ComunesPDF.CENTER);
							}
							pdfDoc.addTable();
						}
						
						
						
						if(listValPR.size()>0){
							pdfDoc.addText("\n");
							pdfDoc.addText("Prestamos");
							pdfDoc.setTable(3, 100);
							pdfDoc.setCell("N�mero de Par�metro","celda01",ComunesPDF.LEFT);
							pdfDoc.setCell("Nombre","celda01",ComunesPDF.LEFT);
							pdfDoc.setCell("Par�metros del Campo","celda01",ComunesPDF.LEFT);
							for(int i = 0; i< listValPR.size(); i++){
								HashMap datos = new HashMap();	
								datos =(HashMap)listValPR.get(i);
								pdfDoc.setCell((String)datos.get("IC_PARAMETRO"),"formas",ComunesPDF.CENTER);
								pdfDoc.setCell((String)datos.get("NOMBRE_PARAM"),"formas",ComunesPDF.LEFT);
								pdfDoc.setCell((String)datos.get("IG_PARAM_CAMPO"),"formas",ComunesPDF.CENTER);
							}
							pdfDoc.addTable();
						}
						
						
						
						if(listValDS.size()>0){
							pdfDoc.addText("\n");
							pdfDoc.addText("Disposiciones");
							pdfDoc.setTable(3, 100);
							pdfDoc.setCell("N�mero de Par�metro","celda01",ComunesPDF.LEFT);
							pdfDoc.setCell("Nombre","celda01",ComunesPDF.LEFT);
							pdfDoc.setCell("Par�metros del Campo","celda01",ComunesPDF.LEFT);
							for(int i = 0; i< listValDS.size(); i++){
								HashMap datos = new HashMap();	
								datos =(HashMap)listValDS.get(i);
								pdfDoc.setCell((String)datos.get("IC_PARAMETRO"),"formas",ComunesPDF.CENTER);
								pdfDoc.setCell((String)datos.get("NOMBRE_PARAM"),"formas",ComunesPDF.LEFT);
								pdfDoc.setCell((String)datos.get("IG_PARAM_CAMPO"),"formas",ComunesPDF.CENTER);
							}
							pdfDoc.addTable();
						}
						
						pdfDoc.endDocument();
						
					}
				
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
					} catch(Exception e) {}
				}
				
		
		} catch (Exception e) {
			log.error("getParamDefault  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.debug("qrySentencia getArchivo:  "+qrySentencia.toString());
		log.debug("conditions "+conditions);
		
		log.info("getArchivo (S)  resultado ");
		return nombreArchivo;
	}
	/**
	 * M�todo que nos regresa una lista con las Epo�s que tengan afiliado un IF de 1er Piso 
	 * @return 
	 */
		
	public Registros catEpoPrimerPiso(String combo_param, String ic_epo, String api){
		log.info("catEpoPrimerPiso(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		PreparedStatement ps = null;
		ResultSet rs = null;
		String resultado = ""; 
		String tabla = "";
		String cond = "";
		String cond_api = "";
		String cond_ic_epo = "";
		Registros reg = new Registros();
		try{
			con.conexionDB();
			if(combo_param.equals("S")){
				tabla = "	, COM_PARAM_INTERFASE_DESC CPID	";
				cond = "	AND CIE.IC_EPO = CPID.IC_EPO	";
				
				if(!api.equals("")&&!api.equals("T")){
					conditions.add(api);
					cond_api = "	AND CPID.CC_API = ? 	";
				}
				if(!ic_epo.equals("")){
					conditions.add(ic_epo);
					cond_ic_epo = "	AND CIE.IC_EPO NOT IN(?) 	";
				}
			}
			
			
			qrySentencia.append(" SELECT CE.IC_EPO AS CLAVE,CE.CG_RAZON_SOCIAL AS DESCRIPCION "+
										"	FROM COMCAT_EPO CE, COMREL_IF_EPO CIE, COMCAT_IF CI "+tabla+
										"	WHERE CIE.IC_EPO = CE.IC_EPO "+
										"	AND CIE.IC_IF = CI.IC_IF	"+
										"	AND CI.IG_TIPO_PISO = 1	"+cond+cond_api+cond_ic_epo+
										
										"	and cie.cs_aceptacion = 'S'	"+
										"	and cie.cs_vobo_nafin = 'S'	"+
										"	and cie.cs_bloqueo != 'S'	"+
										
										"	GROUP BY CE.IC_EPO,CE.CG_RAZON_SOCIAL "+
										"	ORDER BY CE.CG_RAZON_SOCIAL "
			
			);
			
			log.debug("qrySentencia getExisteParametrizacion:  "+qrySentencia.toString());
			log.debug("conditions "+conditions);
			reg= con.consultarDB(qrySentencia.toString(),conditions );
		  
			} catch (Exception e) {
				log.error("getExisteParametrizacion  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}
			log.info("catEpoPrimerPiso (S)  resultado "+resultado);
			return reg;
		
		
	}
	
	public List getParamDefault( String api,String producto ){
		log.info("getParamDefault(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		PreparedStatement ps = null;
		ResultSet rs = null;
		String resultado = ""; 
		String tabla = "";
		String cond = "";
		String cond_api = "";
		String cond_ic_epo = "";
	
		Map hmInf = new HashMap();
		List listVal = new ArrayList();
		HashMap dato = new HashMap();
		try{
			con.conexionDB();
			
			if(!api.equals("")&&!api.equals("T")){
				cond = " AND CC_API = '"+api+"'	";
			}
			
			
			qrySentencia.append(" 	select IC_PARAMETRO,IG_PARAM_CAMPO, CC_API   "+
										"	from com_matriz_interface "+
										"	where CC_PRODUCTO = '"+producto+"' "+cond+
										"	and IG_PARAM_CAMPO is not null "
									
			
			);
			
			log.debug("qrySentencia getExisteParametrizacion:  "+qrySentencia.toString());
			log.debug("conditions "+conditions);
			ps = con.queryPrecompilado(qrySentencia.toString(),conditions );
		   rs = ps.executeQuery();
			log.debug("qrySentencia "+qrySentencia);
			log.debug("conditions "+conditions);
			listVal = new ArrayList();
			while (rs.next()){
				dato = new HashMap();
				dato.put("IC_PARAMETRO",rs.getString("IC_PARAMETRO")==null?"":rs.getString("IC_PARAMETRO"));
				dato.put("IG_PARAM_CAMPO",rs.getString("IG_PARAM_CAMPO")==null?"":rs.getString("IG_PARAM_CAMPO"));
				dato.put("CC_API",rs.getString("CC_API")==null?"":rs.getString("CC_API"));
				
				listVal.add(dato);
			}
			rs.close();
			ps.close();
		  
		} catch (Exception e) {
			log.error("getParamDefault  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("getParamDefault (S)  resultado "+resultado);
		return listVal;
		
		
	}

		/************ SETTERS ***************/

		public void setsTipoInterfaz(String sTipoInterfaz){
			this.sTipoInterfaz = sTipoInterfaz;
		}
	
		public void setsNombreProducto(String sNombreProducto){
			this.sNombreProducto = sNombreProducto;
		}	

	public String getListParam() {
		return listParam;
	}

	public void setListParam(String listParam) {
		this.listParam = listParam;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}
	
}