/**
 * @class		:MantenimientoCatalogo.
 * 
 *					Esta clase implementa la interfaz CatalogoMantenimiento para realizar las operaciones de 
 * 				INSERT - UPDATE - DELETE para el mantenimiento de Catalogos usando los m�todos en el EJB
 *             ActualizacionCatalogosEJB:
 *             @link com.netro.catalogos.ActualizacionCatalogosBean#insertarEnCatalogo( MantenimientoCatalogo) ,
 * 				@link com.netro.catalogos.ActualizacionCatalogosBean#actualizarCatalogo(MantenimientoCatalogo),
 * 				@link com.netro.catalogos.ActualizacionCatalogosBean#eliminarCatalogo(MantenimientoCatalogo) 
 *             
 * @Pantalla	:	Administracion  - Parametrizacion - Catalogos -> *.
 * @Perfil		:	ADMIN NAFIN.
 * @date			:	20-10-2014.
 * @by			:	rpastrana.
 */
 
package com.netro.cadenas;

import com.netro.catalogos.mantenimiento.CatalogoMantenimiento;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public class MantenimientoCatalogo  implements CatalogoMantenimiento, Serializable {
	
	/**
   * Declaracion de variables
	* @bind			: recibe las variables a ser usadas como Bind(?). 
	* @tabla			: recibe la tabla.
	* @campos		: recibe los campos de la tabla.
	* @values		: recibe los nuevos valores para actualzar.
	* @claveVal		: recibe la columna y el valor a actualizar.
	* @condicion	: recibe la condicion WHERE.
	* @conditions	: alamcena la lista de variables Bind(?). NUNCA debe estar vacia.
   */
   private String bind		="";
	private String tabla  	="";
	private String campos 	="";
	private String values 	="";
	private String claveVal ="";
	private String condicion="";
	private List 	conditions;
	
   /**
   * Constructor by Default
   */
	public MantenimientoCatalogo() { }
	
	/**
	 * M�todo para obtener el query que permite determinar si el registro
	 * que se desea insertar/actualizar es valido. 
	 * La instrucci�n de SQL debe ser una consulta que indique el numero 
	 * de registros que existen (count). Adem�s debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo:
	 * @link #getParametrosBind()
	 * @return Cadena con la instrucci�n SQL, para validar existencia
	 */
	public String getQueryExistencia(){
	return "";
	}
	
	
	/**
    * M�todo para obtener el query que permite la inserci�n de un registro en el cat�logo.
	 * La instrucci�n de SQL debe manejar variables bind,
	 * cuyos valores deben ser regresados por el m�todo:
	 * @link   getParametrosBind()
	 * @throw->Exception en caso de que la tabla o la lista de variables Bind(?) este vacia.
	 * @return Cadena con la instruccion SQL, para insertar.
	 */
	public String getQueryInsertar(){
		StringBuffer qry = new StringBuffer("");
		conditions = new ArrayList();
		try {
			if (tabla.equals(""))
				throw new Exception("tabla = null");
			qry.append("INSERT INTO "+tabla);
			
			if (campos.equals("")){
			} else {
				qry.append(" ( "+campos+" ) ");
			}
			
			if (values.equals("")){
			} else {
				qry.append(" VALUES ( "+values+" ) ");
			}
			
			if (condicion.equals("")){
			} else {
				qry.append(" WHERE "+condicion);
			}
			
			if (bind.equals(""))
					throw new Exception("bind = null ");
			conditions.add(bind);
			
		} catch (Exception e){
			System.err.println("getQueryInsertar : "+ e);
		}
		System.err.println("getQueryInsertar: "+ qry.toString());
		System.err.println("conditions: "+ conditions);
		return qry.toString();
	}
	

	/**
	 * M�todo para obtener el query que permite la actualizaci�n de un 
	 * registro en el cat�logo. 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo:
	 * @link   getParametrosBind()
	 * @throw->Exception en caso de que la tabla o la lista de variables Bind(?) este vacia.	 
	 * @return Cadena con la instruccion SQL, para actualizar
	 */
	public String getQueryActualizar(){
		StringBuffer qry = new StringBuffer("");
		conditions = new ArrayList();
		try {
			if (tabla.equals(""))
				throw new Exception("tabla = null");
			qry.append("UPDATE "+tabla);
			
			if (campos.equals("")){
			} else {
				qry.append(" ( "+campos+" ) ");
			}
			if (claveVal.equals("")){
			} else {
				qry.append(" SET "+claveVal );
			}			
			if (condicion.equals("")){
			} else {
				qry.append(" WHERE "+condicion);
			}			
			if (bind.equals(""))
					throw new Exception("bind = null ");
			conditions.add(bind);
			
		} catch (Exception e){
			System.err.println("getQueryActualizar : "+ e);
		}
		System.err.println("getQueryActualizar: "+ qry.toString());
		System.err.println("conditions: "+ conditions);
		return qry.toString();
	}
	
	
	/**
	 * M�todo para obtener el query que permite la eliminaci�n de un 
	 * registro en el cat�logo. 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo:
	 * @link  getParametrosBind()
	 * @throw->Exception en caso de que la tabla o la lista de variables Bind(?) este vacia.
	 * @return Cadena con la instruccion SQL, para eliminar
	 */
	public String getQueryEliminar(){
		StringBuffer qry = new StringBuffer("");
		conditions = new ArrayList();
		try {
			if (tabla.equals(""))
				throw new Exception("tabla = null");
			qry.append("DELETE "+tabla);
			
			if (campos.equals("")){
			} else {
				qry.append(" ( "+campos+" ) ");
			}
			
			if (condicion.equals("")){
			} else {
				qry.append(" WHERE "+condicion);
			}
			
			if (bind.equals(""))
					throw new Exception("bind = null ");
			conditions.add(bind);
			
		} catch (Exception e){
			System.err.println("getQueryEliminar : "+ e);
		}
		System.err.println("getQueryEliminar: "+ qry.toString());
		System.err.println("conditions: "+ conditions);
		return qry.toString();
	}
	
	
	/**
	 * M�todo para obtener el query que permite mostrar los datos
	 * de un registro a modificar. 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo:
	 * @link getParametrosBind() 
	 * @return Cadena con la instruccion SQL.
	 */
	public String getQueryRegistro(){
	return "";
	}
	
	
	/**
	 * M�todo para obtener los valores para cada uno de las variables bind
	 * de la instrucci�n de SQL.
	 * Los valores deberan de estar acorde a la operaci�n que se realice
	 * (inserci�n, actualizacion, eliminaci�n)
	 * @return Lista con los valores de las variables bind.
	 */
	public List getParametrosBind(){
		return conditions;
	}


	/**
	 * @_Setters_&_Getters_Methods_
	 */

	public void setCampos(String campos) {
		this.campos = campos;
	}


	public String getCampos() {
		return campos;
	}


	public void setTabla(String tabla) {
		this.tabla = tabla;
	}


	public String getTabla() {
		return tabla;
	}


	public void setValues(String values) {
		this.values = values;
	}


	public String getValues() {
		return values;
	}


	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}


	public String getCondicion() {
		return condicion;
	}


	public void setBind(String bind) {
		this.bind = bind;
	}


	public String getBind() {
		return bind;
	}


	public void setClaveVal(String claveVal) {
		this.claveVal = claveVal;
	}


	public String getClaveVal() {
		return claveVal;
	}
		
} // FIN CLASE MantenimientoCatalogo