package com.netro.cadenas;
import com.netro.pdf.ComunesPDF;
import java.io.*;
import javax.naming.Context;		
import java.math.*;
import net.sf.json.JSONArray;
import com.netro.exception.*;
import java.sql.Timestamp;
import com.netro.xls.*;
import java.text.*;
import java.util.*;
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpSession;
import netropology.utilerias.*;
import java.sql.*;
import org.apache.commons.logging.Log;
public class EstadisticasCamp implements  IQueryGeneratorRegExtJS {
	public EstadisticasCamp() {  }
	private List 	conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	StringBuffer 	strQuery;
	private static final Log log = ServiceLocator.getInstance().getLog(EstadisticasCamp.class);//Variable para enviar mensajes al log.
	private String txtCampana;
	private String txtEpo;
	private String txtAccion;
	private String txtSelCadena;
	private String txtRespuesta;
	private String txtIc_msg_ini;
	
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		strQuery 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer strQueryAux 		= new StringBuffer();
		if(!txtAccion.equals("verDetalle")){
		strQueryAux.append("	select ic_epo,"+
								 " 	ic_mensaje_ini,"+
								 " 	sum(decode(CG_ESTATUS_RESPUESTA,'S',1,0)) as total_pymes_si, " +
								 "		DECODE (SUM (DECODE (cg_estatus_respuesta, 'S', 1, 0)),0,'N','S') as aux_pyme_si, "+
								 " 	sum(decode(CG_ESTATUS_RESPUESTA,'N',1,0)) as total_pymes_no, " +
								 "		DECODE (SUM (DECODE (cg_estatus_respuesta, 'N', 1, 0)),0,'N','S') as aux_pyme_no "+
								 " 	from com_estadistica_mensaje_ini " +
								 " 	where ic_mensaje_ini = " +txtCampana + 
								 " 	group by ic_epo, ic_mensaje_ini");
		strQuery.append(  "	select SUM ( COUNT (pe.ic_pyme)) AS TOTAL_PYME, SUM (  NVL (emi.total_pymes_si, 0)) TOTAL_SI " +
								"	,  SUM (  NVL (emi.total_pymes_no, 0) ) TOTAL_NO " +
								"	, SUM ((COUNT (pe.ic_pyme))-NVL ((emi.total_pymes_no+emi.total_pymes_si), 0)) TOTAL_NO_CONTES " +
								"	from comrel_epo_mensaje_ini em " +
								"	, comcat_epo e " +
								"	, comrel_pyme_epo pe " +
								"	, ("+ strQueryAux +") emi " +
								"	where em.ic_epo = e.ic_epo " +
								"	AND e.ic_epo = pe.ic_epo " +
								"	AND em.ic_epo = emi.ic_epo (+) " +
								"	AND em.ic_mensaje_ini = emi.ic_mensaje_ini(+) " +
								"	AND em.ic_mensaje_ini = " +txtCampana );
		if(txtRespuesta.equals("2")){
			strQuery.append( "	AND emi.aux_pyme_si = 'S' ");
		
		}else if(txtRespuesta.equals("3")){
			strQuery.append( "	AND emi.aux_pyme_no= 'S' ");
		}
								if(!txtEpo.equals("") && !txtEpo.equals("-1") && !txtEpo.equals("0")){strQuery.append(" and em.ic_epo = "+txtEpo);}//FODEA 008 - 2009 ACF
										strQuery.append( "	GROUP BY em.ic_mensaje_ini, e.cg_razon_social, emi.total_pymes_si, emi.total_pymes_no, em.ic_epo");
					
		}else{
			strQuery.append( 	"	SELECT NVL (sum( decode( NVL (emi.cg_estatus_respuesta, ' '),'N',1)),0) total_n, " +
									"	NVL (sum( decode( NVL (emi.cg_estatus_respuesta, ' '),'S',1) ),0) total_s, " +
									"	NVL ( sum(decode( NVL (emi.cg_estatus_respuesta, ' '),' ',1)),0) total_nc, " +
									"	NVL (sum(decode( NVL (emi.cg_estatus_respuesta, ' '),' ',1,'S',1,'N',1)),0) total " +
									"	FROM comrel_pyme_epo pe, " +
									"	comrel_nafin n, " +
									"	comcat_pyme p, " +
									"	com_contacto c, " +
									"	com_domicilio d, " +
									"	comcat_estado e, " +
									"	(SELECT ic_epo, ic_pyme, ic_mensaje_ini, cg_estatus_respuesta,df_respuesta " +
									"	FROM com_estadistica_mensaje_ini"+
									
									"	WHERE ic_epo = "+txtEpo+" AND ic_mensaje_ini = "+txtCampana+") emi"+
									"	WHERE pe.ic_epo = "+txtEpo+
									"	AND n.ic_epo_pyme_if = pe.ic_pyme	"+
									"	AND n.cg_tipo = 'P'	"+
									"	AND pe.ic_pyme = p.ic_pyme	"+
									"	AND pe.ic_pyme = c.ic_pyme	"+
									"	AND pe.ic_pyme = d.ic_pyme	"+
									
									"	AND d.cs_fiscal = 'S'	"+
									"	AND c.cs_primer_contacto = 'S'	"+
									"	AND d.ic_estado = e.ic_estado	"+
									"	AND emi.ic_pyme(+) = pe.ic_pyme	"
							);
			if(!txtRespuesta.equals("")){
				if(txtRespuesta.equals("2"))
					strQuery.append("	AND emi.cg_estatus_respuesta = 'S'	");
				if(txtRespuesta.equals("3"))
					strQuery.append("	AND emi.cg_estatus_respuesta = 'N'	");
				if(txtRespuesta.equals("4"))
					strQuery.append("	AND emi.ic_pyme is null	");
			}
		}
		log.info("qrySentencia  "+strQuery);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return strQuery.toString();	
 	}  
		 
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer();
		log.debug("getDocumentQuery(E)");
		StringBuffer strQueryAux 		= new StringBuffer();
		strQueryAux.append("	select ic_epo,"+
								 " 	ic_mensaje_ini,"+
								 " 	sum(decode(CG_ESTATUS_RESPUESTA,'S',1,0)) as total_pymes_si, " +
								 "		DECODE (SUM (DECODE (cg_estatus_respuesta, 'S', 1, 0)),0,'N','S') as aux_pyme_si, "+
								 " 	sum(decode(CG_ESTATUS_RESPUESTA,'N',1,0)) as total_pymes_no, " +
								 "		DECODE (SUM (DECODE (cg_estatus_respuesta, 'N', 1, 0)),0,'N','S') as aux_pyme_no "+
								 " 	from com_estadistica_mensaje_ini " +
								 " 	where ic_mensaje_ini = " +txtCampana + 
								 " 	group by ic_epo, ic_mensaje_ini");
							  
		strQuery.append( "	select e.IC_EPO, em.ic_mensaje_ini msg_ini" +
							  "	from comrel_epo_mensaje_ini em " +
							  "	, comcat_epo e " +
							  "	, comrel_pyme_epo pe " +
							  "	, ("+ strQueryAux +") emi " +
							  "	where em.ic_epo = e.ic_epo " +
							  "	AND e.ic_epo = pe.ic_epo " +
							  "	AND em.ic_epo = emi.ic_epo (+) " +
							  "	AND em.ic_mensaje_ini = emi.ic_mensaje_ini(+) " +
							  "	AND em.ic_mensaje_ini = " +txtCampana );
		if(txtRespuesta.equals("2")){
			strQuery.append( "	AND emi.aux_pyme_si = 'S' ");
		
		}else if(txtRespuesta.equals("3")){
			strQuery.append( "	AND emi.aux_pyme_no= 'S' ");
		}
      if(!txtEpo.equals("") && !txtEpo.equals("-1") && !txtEpo.equals("0")){strQuery.append(" 	and em.ic_epo = "+txtEpo);}//FODEA 008 - 2009 ACF
		strQuery.append( "	GROUP BY em.ic_mensaje_ini,e.IC_EPO");
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		log.debug("getDocumentQuery(S)");
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		StringBuffer strQueryAux 		= new StringBuffer();
		log.debug("getDocumentSummaryQueryForIds(E)");
		if(!txtAccion.equals("verDetalle")){   
		strQueryAux.append("	select ic_epo,"+
								 " 	ic_mensaje_ini,"+
								 " 	sum(decode(CG_ESTATUS_RESPUESTA,'S',1,0)) as total_pymes_si, " +
								 " 	sum(decode(CG_ESTATUS_RESPUESTA,'N',1,0)) as total_pymes_no " +
								 " 	from com_estadistica_mensaje_ini " +
								 " 	where ic_mensaje_ini = " +txtCampana + 
                         " 	group by ic_epo, ic_mensaje_ini");
							  
		strQuery.append( 	"	select em.ic_mensaje_ini msg_ini, e.cg_razon_social rzsocial " +
								"	, count(pe.ic_pyme) as total_pymes " +
								"	, NVL(emi.total_pymes_si,0) si " +
								"	, NVL(emi.total_pymes_no,0) no " +
								"	, (COUNT (pe.ic_pyme))-NVL ((emi.total_pymes_no+emi.total_pymes_si), 0) total_no_conte " +
								"	, em.ic_epo epo " +
								"	from comrel_epo_mensaje_ini em " +
								"	, comcat_epo e " +
								"	, comrel_pyme_epo pe " +
								"	, ("+ strQueryAux +") emi " +
								"	where em.ic_epo = e.ic_epo " +
								"	AND e.ic_epo = pe.ic_epo " +
								"	AND em.ic_epo = emi.ic_epo (+) " +
								"	AND em.ic_mensaje_ini = emi.ic_mensaje_ini(+) " +
								"	AND em.ic_mensaje_ini = " +txtCampana );
								List   pKeys = new ArrayList();
								strQuery.append("	 and em.ic_epo in(");
								for (int i = 0; i < pageIds.size(); i++) { 
									List lItem = (ArrayList)pageIds.get(i);
									if(i>0){
										strQuery.append(",?");
									} else{
										strQuery.append("?");
									}
									conditions.add(new Long(lItem.get(0).toString()));
									pKeys.add(pageIds.get(i));
								}
								strQuery.append(" ) ");
								strQuery.append( "	GROUP BY em.ic_mensaje_ini, e.cg_razon_social, emi.total_pymes_si, emi.total_pymes_no, em.ic_epo");
		}else{
			strQuery.append("	SELECT pe.ic_pyme icpyme, n.ic_nafin_electronico numpyme, p.cg_razon_social pyme, p.cg_rfc rfc, " +
								 "	c.cg_nombre||' '||c.cg_appat||' '||c.cg_apmat contacto, NVL(c.cg_email,' ') mail, e.cd_nombre edo, " +
								 "	d.cg_telefono1 tel, NVL(emi.cg_estatus_respuesta,' ') respuesta, NVL(to_char(emi.df_respuesta,'dd/mm/yyyy'),' ') fecha " +
								 "	FROM comrel_pyme_epo pe, comrel_nafin n, comcat_pyme p, com_contacto c, com_domicilio d,comcat_estado e, " +
								 "	( " +
								 "	select ic_epo, ic_pyme, ic_mensaje_ini, cg_estatus_respuesta, df_respuesta " +
								 " from com_estadistica_mensaje_ini " +
								 "	where ic_epo = " + txtEpo + " " +
								 "	and ic_mensaje_ini = " + txtCampana + " " +
								 "	) emi " +
								 "	WHERE pe.ic_epo = " + txtEpo + " " +
								 "	AND n.ic_epo_pyme_if = pe.ic_pyme " +
								 "	AND n.cg_tipo = 'P' " +
								 "	AND pe.ic_pyme = p.ic_pyme " +
								 "	AND pe.ic_pyme = c.ic_pyme " +
								 "	AND pe.ic_pyme = d.ic_pyme " +   
								 "	AND d.cs_fiscal = 'S' " +
								 "	AND c.cs_primer_contacto = 'S' " +
								 "	AND d.ic_estado = e.ic_estado " +
								 "	AND emi.ic_pyme(+) = pe.ic_pyme   ");
			if(!txtRespuesta.equals("")){
				if(txtRespuesta.equals("2"))
					strQuery.append("  AND emi.cg_estatus_respuesta = 'S'");
				if(txtRespuesta.equals("3"))
					strQuery.append("  AND emi.cg_estatus_respuesta = 'N'");
				if(txtRespuesta.equals("4"))
					strQuery.append("  AND emi.ic_pyme is null");   
			}
			
		
		}
		log.debug("getDocumentSummaryQueryForIds(S)");
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	}
	/**
	 * Funci�n que realiza el query para la generaci�n de documentos
	 * 
	 * @return 
	 */
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();   
		String qrySentenciaE	= "";
		String qrySentenciaI	= "";
		String qrySentencia		= "";
		StringBuffer condicion    = new StringBuffer();
		StringBuffer strQueryAux 		= new StringBuffer();
		strQuery 	= new StringBuffer();
		log.debug("getDocumentQueryFile(E)");
		try {
		strQueryAux.append("	select ic_epo,"+
								 " 	ic_mensaje_ini,"+
								 " 	sum(decode(CG_ESTATUS_RESPUESTA,'S',1,0)) as total_pymes_si, " +
								 "		DECODE (SUM (DECODE (cg_estatus_respuesta, 'S', 1, 0)),0,'N','S') as aux_pyme_si, "+
								 " 	sum(decode(CG_ESTATUS_RESPUESTA,'N',1,0)) as total_pymes_no, " +
								 "		DECODE (SUM (DECODE (cg_estatus_respuesta, 'N', 1, 0)),0,'N','S') as aux_pyme_no "+
								 " 	from com_estadistica_mensaje_ini " +
								 " 	where ic_mensaje_ini = " +txtCampana + 
								 " 	group by ic_epo, ic_mensaje_ini");
		strQuery.append( "	select em.ic_mensaje_ini msg_ini, e.cg_razon_social rzsocial " +
							"	, count(pe.ic_pyme) as total_pymes " +
							"	, NVL(emi.total_pymes_si,0) si " +
							"	, NVL(emi.total_pymes_no,0) no " +
							"	, em.ic_epo epo " +
							"	from comrel_epo_mensaje_ini em " +
							"	, comcat_epo e " +
							"	, comrel_pyme_epo pe " +
							"	, ("+ strQueryAux.toString() +") emi " +
							"	where em.ic_epo = e.ic_epo " +
							"	AND e.ic_epo = pe.ic_epo " +
							"	AND em.ic_epo = emi.ic_epo (+) " +
							"	AND em.ic_mensaje_ini = emi.ic_mensaje_ini(+) " +
							"	AND em.ic_mensaje_ini = " +txtCampana );
		if(txtRespuesta.equals("2")){
			strQuery.append( "	AND emi.aux_pyme_si = 'S' ");
		
		}else if(txtRespuesta.equals("3")){
			strQuery.append( "	AND emi.aux_pyme_no= 'S' ");
		}
               if(!txtEpo.equals("") && !txtEpo.equals("-1") && !txtEpo.equals("0")){strQuery.append(" and em.ic_epo = "+txtEpo);}//FODEA 008 - 2009 ACF
							strQuery.append( "	GROUP BY em.ic_mensaje_ini, e.cg_razon_social, emi.total_pymes_si, emi.total_pymes_no, em.ic_epo");
		}catch(Exception e){
      		System.out.println("getDocumentQueryFileException "+e);
    	}
		log.debug("getDocumentQueryFile(S)");	
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		return strQuery.toString();
		
 	} 
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.debug("::crearCustomFile(E)");
		CreaArchivo creaArchivo = new CreaArchivo();
		String nombreArchivo	= creaArchivo.nombreArchivo()+".xls";
		ComunesXLS xls = new ComunesXLS(path+nombreArchivo);
      boolean visible = false;
		int ini = 0;		int fin = 0;
		int inis = 0;
		int fins = 0;
		int totalpsi = 0;
		int totalpno = 0;
		int totalpsinc = 0;
		int total = 0;
		List lDatos = new ArrayList();
		List lColumnas = new ArrayList();
		List lFilas = new ArrayList();
		String nombreEpo = "Estad�sticas Env�o Campa�as";
		String encabezados[] = null;
		String qry = "";
		ResultSet rs1 = null;
		AccesoDB con = new AccesoDB();
		
		try {
			if(txtAccion.equals("A")){
				xls.setTabla(7);
				xls.agregaTitulo("Estad�sticas Env�o Campa�as",8);
				xls.setCelda("Campa�a", "formasb", ComunesXLS.CENTER, 1);
				xls.setCelda("EPO", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("Total de PYMES", "formasb", ComunesXLS.CENTER, 1);
				xls.setCelda("Total de PYMES a quienes se les envio el mensaje", "formasb", ComunesXLS.CENTER, 1);
				xls.setCelda("Total de PYMES que respondieron que SI", "formasb", ComunesXLS.CENTER, 1);
				xls.setCelda("Total de PYMES que respondieron que NO", "formasb", ComunesXLS.CENTER, 1);
				xls.setCelda("Total de PYMES que no han contestado", "formasb", ComunesXLS.CENTER, 1);
				if(!txtSelCadena.equals("")){
					ini = 0;
					fin = 0;
					inis = 0;
					fins = 0;
					while(ini<txtSelCadena.length()){
						lColumnas = new ArrayList();
						lDatos = new ArrayList();
						fin = txtSelCadena.indexOf('@',ini);
						String dato = txtSelCadena.substring(ini,fin);
						inis = 0;
						while(inis<dato.length()){
							fins = dato.indexOf('|',inis);
							if(fins!=-1){
								lDatos.add(dato.substring(inis,fins));
						
								inis = fins+1;
							}else{   
								inis= dato.length()+1;
							}
						}
						xls.setCelda((String)lDatos.get(0), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda((String)lDatos.get(1), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(lDatos.get(2), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(lDatos.get(3), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(lDatos.get(4), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(lDatos.get(5), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(lDatos.get(6), "formas", ComunesXLS.CENTER, 1);
						ini = fin+1;
					}
				}
				xls.cierraTabla();
				xls.cierraXLS();
			}else if(txtAccion.equals("AD")){
				con.conexionDB();
				if(!txtSelCadena.equals("")){
					xls.setTabla(11);
					xls.agregaTitulo("Estad�sticas Env�o Campa�as",11);
					xls.setCelda("Nombre de la EPO", "formasb", ComunesXLS.CENTER, 1);
					xls.setCelda("Campa�a", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("No. PYME", "formasb", ComunesXLS.CENTER, 1);
					xls.setCelda("Nombre de la PYME", "formasb", ComunesXLS.CENTER, 1);
					xls.setCelda("RFC", "formasb", ComunesXLS.CENTER, 1);
					xls.setCelda("Contacto", "formasb", ComunesXLS.CENTER, 1);
					xls.setCelda("E-mail", "formasb", ComunesXLS.CENTER, 1);
					xls.setCelda("Estado", "formasb", ComunesXLS.CENTER, 1);
					xls.setCelda("Lada y Tel�fono", "formasb", ComunesXLS.CENTER, 1);
					xls.setCelda("Respuesta", "formasb", ComunesXLS.CENTER, 1);
					xls.setCelda("Fecha de Respuesta", "formasb", ComunesXLS.CENTER, 1);
					ini = 0;
					fin = 0;
					inis = 0;
					fins = 0;
					while(ini<txtSelCadena.length()){
						total = 0;
						totalpsi = 0;
						totalpno = 0;
						totalpsinc = 0;
						lColumnas = new ArrayList();
						lDatos = new ArrayList();
						fin = txtSelCadena.indexOf('@',ini);
						String dato = txtSelCadena.substring(ini,fin);
						inis = 0;
						while(inis<dato.length()){
							fins = dato.indexOf('|',inis);
							if(fins!=-1){
								lDatos.add(dato.substring(inis,fins));
								inis = fins+1;
							}else{  
								lDatos.add(dato.substring(inis,dato.length()));
								inis= dato.length()+1;
							}
						}
						qry = "	SELECT pe.ic_pyme icpyme, n.ic_nafin_electronico numpyme, p.cg_razon_social pyme, p.cg_rfc rfc, " +
								"	c.cg_nombre||' '||c.cg_appat||' '||c.cg_apmat contacto, NVL(c.cg_email,' ') mail, e.cd_nombre edo, " +
								"	d.cg_telefono1 tel, NVL(emi.cg_estatus_respuesta,' ') respuesta, NVL(to_char(emi.df_respuesta,'dd/mm/yyyy'),' ') fecha " +
								"	FROM comrel_pyme_epo pe, comrel_nafin n, comcat_pyme p, com_contacto c, com_domicilio d,comcat_estado e, " +
								"	( " +
								"	select ic_epo, ic_pyme, ic_mensaje_ini, cg_estatus_respuesta, df_respuesta " +
								"	from com_estadistica_mensaje_ini " +
								"	where ic_epo = " + lDatos.get(7) + " " +
								"	and ic_mensaje_ini = " + lDatos.get(8) + " " +
								"	) emi " +
								"	WHERE pe.ic_epo = " + lDatos.get(7) + " " +
								"	AND n.ic_epo_pyme_if = pe.ic_pyme " +
								"	AND n.cg_tipo = 'P' " +
								"	AND pe.ic_pyme = p.ic_pyme " +
								"	AND pe.ic_pyme = c.ic_pyme " +
								"	AND pe.ic_pyme = d.ic_pyme " +
								"	AND d.cs_fiscal = 'S' " +
								"	AND c.cs_primer_contacto = 'S' " +
								"	AND d.ic_estado = e.ic_estado " +
								"	AND emi.ic_pyme(+) = pe.ic_pyme ";
						if(!txtRespuesta.equals("")){
							if(txtRespuesta.equals("2"))
								qry += "AND emi.cg_estatus_respuesta = 'S'";
							if(txtRespuesta.equals("3"))
								qry += "AND emi.cg_estatus_respuesta = 'N'";
							if(txtRespuesta.equals("4"))
								qry += "AND emi.ic_pyme is null";
						}
						rs1 = con.queryDB(qry);
						while(rs1.next()){
							xls.setCelda((String)lDatos.get(1), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda((String)lDatos.get(0), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(rs1.getString("numpyme"), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(rs1.getString("pyme"), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(rs1.getString("rfc"), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(rs1.getString("contacto"), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(rs1.getString("mail"), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(rs1.getString("edo"), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(rs1.getString("tel"), "formas", ComunesXLS.CENTER, 1);
							
							String respuesta = rs1.getString("respuesta");
							if(respuesta.equals("S")){
								totalpsi++;
								xls.setCelda("Si", "formas", ComunesXLS.CENTER, 1);
							}else if(respuesta.equals("N")){
								totalpno++;
								xls.setCelda("No", "formas", ComunesXLS.CENTER, 1);
							}else{
								totalpsinc++;
								xls.setCelda("No Contestada", "formas", ComunesXLS.CENTER, 1);
							}
							xls.setCelda(rs1.getString("fecha"), "formas", ComunesXLS.CENTER, 1);
							total++;
						}
						String tsi = ""+totalpsi;
						String tno = ""+totalpno;
						String tsinc = ""+totalpsinc;
						String t = ""+total;
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("PYMES SI INTERESADAS", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("PYMES NO INTERESADAS", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("PYMES SIN CONTESTAR", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("TOTAL", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(tsi, "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(tno, "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(tsinc, "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(t, "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);

						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						xls.setCelda("", "formas", ComunesXLS.CENTER, 1);
						ini = fin+1;
					}
				}
				xls.cierraTabla();
				xls.cierraXLS();
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		
		return nombreArchivo;			
	}
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		return "";
	}
	public JSONArray getCatCamp(String BanFondeo)throws Exception{  
		AccesoDB con =new  AccesoDB();
		ResultSet rs = null;
		Registros registros  = new Registros();
		StringBuffer consulta = new StringBuffer();
		JSONArray registrosC = new JSONArray();
		HashMap catCamp = new HashMap();  
		try{
			con.conexionDB();
			consulta.append( "select distinct(mi.ic_mensaje_ini), mi.cg_titulo,  to_char(mi.df_ini_mensaje,'dd/mm/yyyy') as df_ini_mensaje,   to_char(mi.df_fin_mensaje,'dd/mm/yyyy') as df_fin_mensaje"+      
					" from com_mensaje_ini mi  " + 
					" ,comrel_epo_mensaje_ini emi "+
					" ,comcat_epo epo "+
					" where mi.cg_tipo_mensaje = 'C'  "+ 
					" and mi.ic_mensaje_ini = emi.ic_mensaje_ini "+
					" and emi.ic_epo = epo.ic_epo "+
					" and epo.ic_banco_fondeo = "+ BanFondeo +
					" order by 2 ");
			log.debug("Sentencia: " + consulta.toString());
			rs  = con.queryDB(consulta.toString());
			while(rs.next()) {
				catCamp = new HashMap();
				catCamp.put("clave",rs.getString("ic_mensaje_ini"));
				catCamp.put("descripcion",rs.getString("cg_titulo"));  
				catCamp.put("df_ini_mensaje",rs.getString("df_ini_mensaje"));  
				catCamp.put("df_fin_mensaje",rs.getString("df_fin_mensaje"));  
				registrosC.add(catCamp);
			}   
			
		} catch(Exception e){
			log.warn("getCatCampException " + e);
		} finally{
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		log.info("getCatCamp(S)");
		return registrosC;
		
	}

	public JSONArray getCatEPO(String Camp)throws Exception{  
		AccesoDB con =new  AccesoDB();
		ResultSet rs = null;
		Registros registros  = new Registros();
		StringBuffer consulta = new StringBuffer();
		JSONArray registrosE = new JSONArray();
		HashMap catEPO = new HashMap();
		try{
			con.conexionDB();
			if(!Camp.equals("")){
				consulta.append( "select emi.ic_epo, e.cg_razon_social"+      
					" from comrel_epo_mensaje_ini emi, comcat_epo e   " + 
					" where emi.ic_mensaje_ini = "+ Camp +
					" and emi.ic_epo = e.ic_epo");
					
				log.debug("Sentencia: " + consulta.toString());
				rs  = con.queryDB(consulta.toString());
				while(rs.next()) {
					catEPO = new HashMap();
					catEPO.put("clave",rs.getString("ic_epo"));
					catEPO.put("descripcion",rs.getString("cg_razon_social"));  
					registrosE.add(catEPO);
				}  
			}
		} catch(Exception e){
			log.warn("getCatEPOException " + e);
		} finally{
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("getCatEPO(S)");
		return registrosE;
		
	}
	public HashMap getFechaPub(String Camp)throws Exception{  
		AccesoDB con =new  AccesoDB();
		ResultSet rs = null;
		Registros registros  = new Registros();
		StringBuffer consulta = new StringBuffer();
		JSONArray registrosF = new JSONArray();
		HashMap catFecha = new HashMap();
		try{
			con.conexionDB();
			if(!Camp.equals("")){
				consulta.append( "select to_char(df_ini_mensaje,'dd/mm/yyyy') fechai, to_char(df_fin_mensaje,'dd/mm/yyyy') fechaf "+      
					" from com_mensaje_ini   " + 
					" where ic_mensaje_ini = "+ Camp );
				log.debug("Sentencia: " + consulta.toString());
				rs  = con.queryDB(consulta.toString());
				if(rs.next()) {
					catFecha = new HashMap();
					catFecha.put("fecha_in",rs.getString("fechai"));   
					catFecha.put("fecha_fin",rs.getString("fechaf"));  
				}  
			}
		} catch(Exception e){
			log.warn("getFechaPubException:  " + e);
		} finally{
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("getFechaPub(S)");
		return catFecha;
		
	}

	public List getConditions() {
		return conditions;
	}
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
	

	public String getTxtCampana() {
		return txtCampana;
	}

	public void setTxtCampana(String txtCampana) {
		this.txtCampana = txtCampana;
	}
	public String getTxtEpo() {
		return txtEpo;
	}

	public void setTxtEpo(String txtEpo) {
		this.txtEpo = txtEpo;
	}
	
	public String getTxtAccion() {
		return txtAccion;
	}

	public void setTxtAccion(String txtAccion) {
		this.txtAccion = txtAccion;
	}
	
	public String getTxtSelCadena() {
		return txtSelCadena;
	}

	public void setTxtSelCadena(String txtSelCadena) {
		this.txtSelCadena = txtSelCadena;
	}
	
	public String getTxtRespuesta() {
		return txtRespuesta;
	}

	public void setTxtRespuesta(String txtRespuesta) {
		this.txtRespuesta = txtRespuesta;
	}
	
	public String getTxtIc_msg_ini() {
		return txtIc_msg_ini;
	}

	public void setTxtIc_msg_ini(String txtIc_msg_ini) {
		this.txtIc_msg_ini = txtIc_msg_ini;
	}
	
}