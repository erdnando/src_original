package com.netro.cadenas;

import com.netro.cesion.ConsultaNotificacionesCesionDerechos;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsDispersionEPODet implements IQueryGeneratorRegExtJS  {
	public ConsDispersionEPODet() { }
	
	private List conditions;
	StringBuffer strQuery;
	//Variable para enviar mensajes al log
	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaNotificacionesCesionDerechos.class);
	
	private String icFlujoFondos;
	private String	icEpo;
	private String	icPyme;
	private String	icEstatusDocto;
	private String	dfFechaVenc;
	private String	dfOperacion;
	
	public String getQuery(boolean tipoConsulta){
		String strQuery = "";
		
		String montos = " doc.fn_monto as Monto, sel.in_importe_recibir as Importe ";
		
		if("16".equals(icEpo)&&"4".equals(icEstatusDocto)&&!"".equals(icPyme))
				 montos = " doc.fn_monto Monto, sel.in_importe_recibir - NVL (sel.fn_importe_recibir_benef, 0) Importe";
		else if(("1".equals(icEstatusDocto)||"9".equals(icEstatusDocto)||"10".equals(icEstatusDocto))&&!"".equals(icPyme)&&!"".equals(dfFechaVenc))
				 montos = " doc.fn_monto - NVL ((doc.fn_monto * NVL (doc.fn_porc_beneficiario, 0) / 100), 0) Monto, doc.fn_monto - NVL ((doc.fn_monto * NVL (doc.fn_porc_beneficiario, 0) / 100), 0) Importe ";
		
		strQuery = (tipoConsulta?
					   " SELECT doc.ic_documento, doc.ig_numero_docto,"   +
					   "        pym.cg_razon_social AS pyme, doc.ic_moneda, "   +
					            montos +
					   "        ,doc.cs_dscto_especial "
						: 
						" SELECT count(1) as totalRegistros, sum(Monto) as totalImporteDocumento, sum(Importe) as totalImporteDescuento FROM ( " +
						" SELECT " + montos
						) +
					   "   FROM com_documento doc,"   +
					   "        com_docto_seleccionado sel,"   +
					   "        comcat_pyme pym,"   +
					   "        comrel_documento_ff dff"   +
					   "  WHERE doc.ic_documento = dff.ic_documento"   +
					   "    AND doc.ic_documento = sel.ic_documento (+)"   +
					   "    AND doc.ic_pyme = pym.ic_pyme "  +
					   "    AND dff.ic_producto_nafin = 1";
						
		return strQuery;
	}
	
	public String getDocumentQuery() {
		conditions = new ArrayList();
		strQuery	= new StringBuffer();
		
		strQuery.append(getQuery(true));
		
		if(!icFlujoFondos.equals("")){
			strQuery.append("    AND dff.ic_flujo_fondos = ? ");
			conditions.add(icFlujoFondos);
		}
			
		strQuery.append("  ORDER BY doc.cs_dscto_especial DESC, doc.ic_documento"); 
							 
		log.debug("getDocumentQuery)"+strQuery.toString());
		log.debug("getDocumentQuery)"+conditions);
		
		return strQuery.toString();
	}
	
	public String getAggregateCalculationQuery() {
		conditions	= new ArrayList();
		strQuery	= new StringBuffer();
		
		strQuery.append(getQuery(false));
		
		if(!icFlujoFondos.equals("")){
			strQuery.append("    AND dff.ic_flujo_fondos = ? ");
			conditions.add(icFlujoFondos);
		}
		
		strQuery.append(" ) Tab ");
		
		log.debug("getAggregateCalculationQuery)"+strQuery.toString());
		log.debug("getAggregateCalculationQuery)"+conditions);
		
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
		conditions = new ArrayList();
		strQuery	= new StringBuffer();
		
		strQuery.append(getQuery(true));
		
		if(!icFlujoFondos.equals("")){
			strQuery.append("    AND dff.ic_flujo_fondos = ? ");
			conditions.add(icFlujoFondos);
		}
		
		for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					strQuery.append(" AND doc.ic_documento IN ( ");
				}
				strQuery.append("?");
				conditions.add(new Long(lItem.get(0).toString()));					
				if(i!=(pageIds.size()-1)) {
					strQuery.append(",");
				} else {
					strQuery.append(" ) ");
				}			
		}
			
		strQuery.append("  ORDER BY doc.cs_dscto_especial DESC, doc.ic_documento"); 
							 
		log.debug("---------------------------------------------- ");
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		log.debug("---------------------------------------------- ");
		
		return strQuery.toString();
	}
	
	public String getDocumentQueryFile() {
		conditions = new ArrayList();
		strQuery	= new StringBuffer();
		
		strQuery.append(getQuery(true));
		
		if(!icFlujoFondos.equals("")){
			strQuery.append("    AND dff.ic_flujo_fondos = ? ");
			conditions.add(icFlujoFondos);
		}
			
		strQuery.append("  ORDER BY doc.cs_dscto_especial DESC, doc.ic_documento"); 
							 
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		
		return strQuery.toString();
	}
	
	
	/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)
	{
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		try {
			if(tipo.equals("CSV")) {
			 linea = "Num. de Documento, PyME,  Importe Documento, Importe Descuento\n";
			 nombreArchivo = "DISPERSIONEPO_DETALLE_tmp" + Comunes.cadenaAleatoria(10) + ".csv";
			 writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			 buffer = new BufferedWriter(writer);
			 buffer.write(linea);
			 
		while (rs.next()) {
			  String numeroDocumento = (rs.getString("IG_NUMERO_DOCTO") == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
			  String pyme = (rs.getString("PYME") == null) ? "" : rs.getString("PYME");
			  String importeDocumento	=	(rs.getString("MONTO") == null) ? "" : rs.getString("MONTO");
			  String importeDescuento = (rs.getString("IMPORTE") == null) ? "" : rs.getString("IMPORTE");
		
		linea = numeroDocumento.replace(',',' ') + ", " + 
			          pyme.replace(',',' ') + ", " +
						 importeDocumento.replace(',',' ') + ", " +
						 importeDescuento.replace(',',' ') + "\n";				 
			  buffer.write(linea);
			 }
			 buffer.close();
			}
		}
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
	   return nombreArchivo;
	}
	
	
   /** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}

	public void setIcFlujoFondos(String icFlujoFondos) {
		this.icFlujoFondos = icFlujoFondos;
	}

	public String getIcFlujoFondos() {
		return icFlujoFondos;
	}

	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}

	public String getIcEpo() {
		return icEpo;
	}

	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}

	public String getIcPyme() {
		return icPyme;
	}

	public void setIcEstatusDocto(String icEstatusDocto) {
		this.icEstatusDocto = icEstatusDocto;
	}

	public String getIcEstatusDocto() {
		return icEstatusDocto;
	}

	public void setDfFechaVenc(String dfFechaVenc) {
		this.dfFechaVenc = dfFechaVenc;
	}

	public String getDfFechaVenc() {
		return dfFechaVenc;
	}

	public void setDfOperacion(String dfOperacion) {
		this.dfOperacion = dfOperacion;
	}

	public String getDfOperacion() {
		return dfOperacion;
	}
}