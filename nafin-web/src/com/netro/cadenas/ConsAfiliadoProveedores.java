package com.netro.cadenas;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/******************************************************************************************
 * Fodea 11-2014                                                                          *
 * Descripci�n: Migraci�n de la pantalla Administraci�n-Afiliados-Consultas-Proveedores   *
 *              a ExtJS. La clase tiene m�todos que implementan la paginaci�n,            *
 *              y generaci�n de archivos PDF y CSV                                        *
 * Elabor�: Agust�n Bautista Ruiz                                                         *
 * Fecha: 21/03/2014                                                                      *
 ******************************************************************************************/
public class ConsAfiliadoProveedores implements IQueryGeneratorRegExtJS{

	private static final Log log = ServiceLocator.getInstance().getLog(ConsAfiliadoProveedores.class);
	private List conditions;
	private List listaCuentas;
	private String numeroBancoFondeo;
	private String sinNumeroProveedor;
	private String nafinElectronico;
	private String rfc;
	private String cadenaProductiva;
	private String numeroProveedor;
	private String numeroClienteSIRAC;
	private String datosPersModif;
	private String convenioUnico;
	private String numDistribuidor;
	private String tipoAfiliado;
	private String pyme;
	private String datosAfiliado;
	private String icEpo;
	private String estado;
	private String nombre;
	private String operaFideicomiso;
	String noProveedorFile, nafinElecFile, noSiracFile, cadProductivaFile, 
			 razonSocialFile, rfcFile, domicilioFile, estadoFile, telefonoFile, 
			 versionConvenioFile, fechaConvenioFile, convenioUnicoFile, 
			 operaFideicomisoFile;
	
   private String suceptibleFloating; 
	private String operaFactDistribuido;
    private String provExtranjero;
    private String operaDescAutoEPO;
	
	public ConsAfiliadoProveedores() {}

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){ 
		log.info(" getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		String condicion = this.generaCondiciones();
		qrySentencia.append(
			" SELECT t1.ic_pyme, t7.ic_epo "+
			" FROM comcat_epo t7, "+
			" comcat_pyme t2, "+
			" comrel_pyme_epo t5, "+
			" comcat_version_convenio t8, "+
			" comrel_nafin t6, "+
			" com_domicilio t1, "+
			" comcat_estado t3 "+
			" WHERE t5.ic_pyme = t2.ic_pyme "+
			" AND t1.ic_pyme = t2.ic_pyme "+
			" AND t2.ic_pyme = t6.ic_epo_pyme_if "+
			" AND t1.ic_estado = t3.ic_estado "+
			" AND t5.ic_epo = t7.ic_epo "+
			" AND t2.ic_version_convenio = t8.ic_version_convenio(+) "+
			" AND t6.cg_tipo = 'P' "+
			" AND t2.cs_internacional = 'N' "+
			" AND t2.ic_tipo_cliente IN (1, 4)  "+
			" AND (t2.cs_invalido IS NULL OR t2.cs_invalido != 'S') "+
			" AND t1.cs_fiscal = 'S' "+
			condicion +
			" order by t1.ic_pyme" );
		log.info(" qrySentencia: "+ qrySentencia.toString());
		log.info(" condiciones: "+ conditions.toString());
		log.info(" getDocumentQuery(S)");
		return qrySentencia.toString();
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){
		log.info(" getAggregateCalculationQuery (E)");
		StringBuffer qrySentencia = new StringBuffer();
		String condicion = this.generaCondiciones();
		qrySentencia.append(
			" SELECT COUNT(t1.ic_pyme) as total "+
			" FROM comcat_epo t7, "+
			" comcat_pyme t2, "+
			" comrel_pyme_epo t5, "+
			" comcat_version_convenio t8, "+
			" comrel_nafin t6, "+
			" com_domicilio t1, "+
			" comcat_estado t3 "+
			" WHERE t5.ic_pyme = t2.ic_pyme "+
			" AND t1.ic_pyme = t2.ic_pyme "+
			" AND t2.ic_pyme = t6.ic_epo_pyme_if "+
			" AND t1.ic_estado = t3.ic_estado "+
			" AND t5.ic_epo = t7.ic_epo "+
			" AND t2.ic_version_convenio = t8.ic_version_convenio(+) "+
			" AND t6.cg_tipo = 'P' "+
			" AND t2.cs_internacional = 'N' "+
			" AND t2.ic_tipo_cliente IN (1, 4) "+
			" AND (t2.cs_invalido IS NULL OR t2.cs_invalido != 'S') "+
			" AND t1.cs_fiscal = 'S' "+
			condicion  +
			" order by t1.ic_pyme" );
		log.info(" qrySentencia: "+ qrySentencia.toString());
		log.info(" condiciones: "+ conditions.toString());
		log.info(" getAggregateCalculationQuery (S)");	
		return qrySentencia.toString();
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){ 
		log.info(" getDocumentSummaryQueryForIds(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

			if(sinNumeroProveedor.equals("S")){
				condicion.append(" AND t5.cs_num_proveedor = ?"); 
				conditions.add(sinNumeroProveedor);
			}
			if(!nafinElectronico.equals("")){
				condicion.append(" AND t6.ic_nafin_electronico = ?"); 
				conditions.add(nafinElectronico);
			}
			if(!rfc.equals("")){
				condicion.append(" AND t2.cg_rfc = ?"); 
				conditions.add(rfc);
			}
			if(!cadenaProductiva.equals("")){
				condicion.append(" AND t5.ic_epo = ?"); 
				conditions.add(cadenaProductiva);
			}
			if(!numeroProveedor.equals("")){
				condicion.append(" AND t5.cg_pyme_epo_interno = ?"); 
				conditions.add(numeroProveedor);
			}
			if(!numeroClienteSIRAC.equals("")){
				condicion.append(" AND t2.in_numero_sirac = ?"); 
				conditions.add(numeroClienteSIRAC);
			}
			if(datosPersModif.equals("S")){
				condicion.append(" AND t2.cs_datos_pers_modif = ?"); 
				conditions.add(datosPersModif);
			}
			if(convenioUnico.equals("S")){
				condicion.append(" AND t2.cs_convenio_unico = ?"); 
				conditions.add(convenioUnico);
			}
			if(operaFideicomiso.equals("S")){
				condicion.append(" AND t2.CS_OPERA_FIDEICOMISO = ?"); 
				conditions.add(operaFideicomiso);
			}
			if(!numDistribuidor.equals("")){
				condicion.append(" AND t5.cg_num_distribuidor = ?"); 
				conditions.add(numDistribuidor);
			}
			if(!estado.equals("")){
				condicion.append(" AND t1.ic_estado = ? "); 
				conditions.add(estado);
			}
			if(!nombre.equals("")){
				condicion.append(" AND (   (UPPER (TRIM (t2.cg_razon_social)) LIKE UPPER (?))");
				condicion.append("      OR (UPPER (TRIM (   t2.cg_nombre");
				condicion.append("                       || ' '");
				condicion.append("                       || t2.cg_appat");
				condicion.append("                       || ' '");
				condicion.append("                       || t2.cg_apmat");
				condicion.append("                      )");
				condicion.append("                ) LIKE UPPER (?)");
				condicion.append("         )");
				condicion.append("   ) ");
				conditions.add(nombre + "%");
				conditions.add(nombre + "%");
			}
/**
		condicion.append(" AND t1.ic_pyme in (");
			for (int i = 0; i < ids.size(); i++) { 
				List lItem = (ArrayList)ids.get(i);
				if(i > 0){
					condicion.append(",");
				}
				condicion.append(" ? " );
				conditions.add(new Long(lItem.get(0).toString()));
			}
		condicion.append(" ) ");
*/
		condicion.append(" AND (");
			for (int i = 0; i < ids.size(); i++) { 
				List lItem = (ArrayList)ids.get(i);
				if(i > 0){
					condicion.append(" OR ");
				}
				condicion.append(" t1.ic_pyme =  ?  AND t7.ic_epo =  ?   " );
				conditions.add(new Long(lItem.get(0).toString()));
				conditions.add(new Long(lItem.get(1).toString()));
			}
		condicion.append(" ) ");
				
		
		if (!"".equals(suceptibleFloating)  ) {
			condicion.append(" AND t2.CS_TASA_FLOATING = ? "); 
			conditions.add(suceptibleFloating);
		}
		if ("S".equals(operaFactDistribuido)  ) {
			condicion.append(" AND t2.CS_FACT_DISTRIBUIDO = ?"); 
			conditions.add(operaFactDistribuido);			
		}
                
		if (!"".equals(provExtranjero)  ) {
			condicion.append(" AND t2.CS_PROV_EXT = ?"); 
			conditions.add(provExtranjero);      
		}
                
		if (!"".equals(operaDescAutoEPO)  ) {
			condicion.append(" AND t5.CS_DESC_AUTOMA_EPO = ?"); 
			conditions.add(operaDescAutoEPO);      
		}
            

		qrySentencia.append(
			" SELECT t1.ic_pyme AS CLIENTE, "+
			" DECODE (t5.cs_habilitado, 'N', '*', 'S', ' ') "+
			" || ' ' "+
			" || DECODE (t2.cs_tipo_persona, "+
			" 'F', t2.cg_nombre || ' ' || t2.cg_appat || ' ' "+
			" || t2.cg_apmat, "+
			" t2.cg_razon_social "+
			" ) AS RAZON_SOCIAL, "+
			" t1.cg_calle ||' '|| t1.cg_numero_ext ||' '|| t1.cg_numero_int ||' '|| t1.cg_colonia AS DIRECCION, "+
			" t1.cg_calle calle, t1.cg_numero_ext numext, t1.cg_numero_int numint, "+
			" t1.cg_colonia colonia, t3.cd_nombre AS ESTADO, "+
			" t1.cg_telefono1 AS TELEFONO, t5.cg_pyme_epo_interno AS NUMERO_PROVEEDOR, "+
			" t6.ic_nafin_electronico AS NAFIN_ELECTRONICO, t2.cs_tipo_persona AS TIPO_PERSONA, "+
			" t7.cg_razon_social AS CADENA_PRODUCTIVA, t7.ic_epo, t2.in_numero_sirac AS NUMERO_SIRAC, "+
			" t8.cd_version_convenio AS VERSION_CONVENIO, TO_CHAR (t2.df_version_convenio, 'DD/MM/YYYY') AS FECHA_CONVENIO, "+
			" t5.cg_num_distribuidor AS num_distribuidor, t2.cg_rfc AS RFC, "+
			" '15clientes/15forma5.jsp' AS pagina, "+
			" t2.cs_convenio_unico AS CONVENIO_UNICO, "+
			" t2.CS_OPERA_FIDEICOMISO " +			
			" ,decode( t2.CS_TASA_FLOATING, 'S', 'Si', 'N', 'No') as TASA_FLOATING , " +	
			"  (SELECT  TO_CHAR (c.DF_OPERACION, 'DD/MM/YYYY')   FROM   COM_ACUSE_FLOATING c  WHERE  c.CC_ACUSE  =  t2.CC_ACUSE )    as FECHA_PARAM_FLOATING "+
			
			" ,decode( t2.CS_PROV_EXT, 'S', 'Si', 'N', 'No') as PROVEEDOR_EXTRANJERO  " +    
			" ,decode( t5.CS_DESC_AUTOMA_EPO, 'S', 'Si', 'N', 'No') as DESC_AUTO_EPO  " +    
                        
			" FROM comcat_epo t7, "+
			" comcat_pyme t2, "+
			" comrel_pyme_epo t5, "+
			" comcat_version_convenio t8, "+
			" comrel_nafin t6, "+
			" com_domicilio t1, "+
			" comcat_estado t3 "+
			" WHERE t5.ic_pyme = t2.ic_pyme "+
			" AND t1.ic_pyme = t2.ic_pyme "+
			" AND t2.ic_pyme = t6.ic_epo_pyme_if "+
			" AND t1.ic_estado = t3.ic_estado "+
			" AND t5.ic_epo = t7.ic_epo "+
			" AND t2.ic_version_convenio = t8.ic_version_convenio(+) "+
			" AND t6.cg_tipo = 'P' "+
			" AND t2.cs_internacional = 'N' "+
			" AND (t2.cs_invalido IS NULL OR t2.cs_invalido != 'S') "+
			" AND t2.ic_tipo_cliente IN (1, 4) "+
			" AND t1.cs_fiscal = 'S' "+
			condicion  +
			" order by t1.ic_pyme" );
		log.info(" qrySentencia: "+ qrySentencia.toString());
		log.info(" condiciones: "+ conditions.toString());
		log.info(" getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}

	/**
	 * Realiza la consulta para generar el archivo csv
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){
		log.info(" getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		String condicion = this.generaCondiciones();
		qrySentencia.append(
			" SELECT t1.ic_pyme AS CLIENTE, "+
			" DECODE (t5.cs_habilitado, 'N', '*', 'S', ' ') "+
			" || ' ' "+
			" || DECODE (t2.cs_tipo_persona, "+
			" 'F', t2.cg_nombre || ' ' || t2.cg_appat || ' ' "+
			" || t2.cg_apmat, "+
			" t2.cg_razon_social "+
			" ) AS RAZON_SOCIAL, "+
			" t1.cg_calle ||' '|| t1.cg_numero_ext ||' '|| t1.cg_numero_int ||' '|| t1.cg_colonia AS DIRECCION, "+
			" t1.cg_calle calle, t1.cg_numero_ext numext, t1.cg_numero_int numint, "+
			" t1.cg_colonia colonia, t3.cd_nombre AS ESTADO, "+
			" t1.cg_telefono1 AS TELEFONO, t5.cg_pyme_epo_interno AS NUMERO_PROVEEDOR, "+
			" t6.ic_nafin_electronico AS NAFIN_ELECTRONICO, t2.cs_tipo_persona AS TIPO_PERSONA, "+
			" t7.cg_razon_social AS CADENA_PRODUCTIVA, t7.ic_epo, t2.in_numero_sirac AS NUMERO_SIRAC, "+
			" t8.cd_version_convenio AS VERSION_CONVENIO, TO_CHAR (t2.df_version_convenio, 'DD/MM/YYYY') AS FECHA_CONVENIO, "+
			" t5.cg_num_distribuidor AS num_distribuidor, t2.cg_rfc AS RFC, "+
			" '15clientes/15forma5.jsp' AS pagina, "+
			" t2.cs_convenio_unico AS CONVENIO_UNICO, "+
			" t2.CS_OPERA_FIDEICOMISO " +
			" ,decode( t2.CS_TASA_FLOATING, 'S', 'Si', 'N', 'No') as TASA_FLOATING , " +
			" ( SELECT  TO_CHAR (c.DF_OPERACION, 'DD/MM/YYYY')   FROM   COM_ACUSE_FLOATING c  WHERE  c.CC_ACUSE  =  t2.CC_ACUSE )    as FECHA_PARAM_FLOATING "+	
			
			" ,decode( t2.CS_PROV_EXT, 'S', 'Si', 'N', 'No') as PROVEEDOR_EXTRANJERO  " +    
			" ,decode( t5.CS_DESC_AUTOMA_EPO, 'S', 'Si', 'N', 'No') as DESC_AUTO_EPO  " +  
                                    
			" FROM comcat_epo t7, "+
			" comcat_pyme t2, "+
			" comrel_pyme_epo t5, "+
			" comcat_version_convenio t8, "+
			" comrel_nafin t6, "+
			" com_domicilio t1, "+
			" comcat_estado t3 "+
			
			" WHERE t5.ic_pyme = t2.ic_pyme "+
			" AND t1.ic_pyme = t2.ic_pyme "+
			" AND t2.ic_pyme = t6.ic_epo_pyme_if "+
			" AND t1.ic_estado = t3.ic_estado "+
			" AND t5.ic_epo = t7.ic_epo "+
			" AND t2.ic_version_convenio = t8.ic_version_convenio(+) "+
			" AND t6.cg_tipo = 'P' "+
			" AND t2.cs_internacional = 'N' "+
			" AND (t2.cs_invalido IS NULL OR t2.cs_invalido != 'S') "+
			" AND t2.ic_tipo_cliente IN (1, 4) "+
			" AND t1.cs_fiscal = 'S' "+
			condicion  +
			//" AND rownum between 1 and 1000 " +
			" order by t1.ic_pyme" );
		log.info(" qrySentencia: "+ qrySentencia.toString());
		log.info(" condiciones: "+ conditions.toString());
		log.info(" getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}
	
	/**
	 * Metodo para generar un archivo CSV, de todos los registros
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		log.info(" crearCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);

			contenidoArchivo = new StringBuffer();
			contenidoArchivo.append("Cadena Productiva, No Proveedor, No. Nafin Electr�nico, No. Sirac, Nombre o Raz�n Social, "+
											"RFC, Domicilio, Estado, Tel�fono, Versi�n Convenio, Fecha de Consulta, Convenio �nico, Opera Fideicomiso para Desarrollo de Proveedores, "+
											"Proveedores susceptibles a Floating, Fecha Parametrizaci�n Floating  "+
                                                                                        ",Proveedor Extranjero , Descuento Autom�tico EPO \n");
			while (rs.next()){
				noProveedorFile = (rs.getString("NUMERO_PROVEEDOR") == null) ? "" : rs.getString("NUMERO_PROVEEDOR");
				nafinElecFile = (rs.getString("NAFIN_ELECTRONICO") == null) ? "" : rs.getString("NAFIN_ELECTRONICO");
				noSiracFile = (rs.getString("NUMERO_SIRAC") == null) ? "" : rs.getString("NUMERO_SIRAC");
				cadProductivaFile = (rs.getString("CADENA_PRODUCTIVA") == null) ? "" : rs.getString("CADENA_PRODUCTIVA");
				razonSocialFile = (rs.getString("RAZON_SOCIAL") == null) ? "" : rs.getString("RAZON_SOCIAL");
				rfcFile = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				domicilioFile = (rs.getString("DIRECCION") == null) ? "" : rs.getString("DIRECCION");
				estadoFile = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				telefonoFile = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				versionConvenioFile = (rs.getString("VERSION_CONVENIO") == null) ? "" : rs.getString("VERSION_CONVENIO");
				fechaConvenioFile = (rs.getString("FECHA_CONVENIO") == null) ? "" : rs.getString("FECHA_CONVENIO");
				convenioUnicoFile = (rs.getString("CONVENIO_UNICO").equals("S")) ? "Si" : "No";
				operaFideicomisoFile = (rs.getString("CS_OPERA_FIDEICOMISO").equals("S")) ? "Si" : "No";

				String floating = (rs.getString("TASA_FLOATING") == null) ? "" : rs.getString("TASA_FLOATING");
				String fechaParamFloating = "";
				if ("Si".equals(floating)  ) {
					fechaParamFloating = (rs.getString("FECHA_PARAM_FLOATING") == null) ? "" : rs.getString("FECHA_PARAM_FLOATING");
				}
				
				String provExtranjeroC = (rs.getString("PROVEEDOR_EXTRANJERO") == null) ? "" : rs.getString("PROVEEDOR_EXTRANJERO");
				String operaDescAutoEPOC = (rs.getString("DESC_AUTO_EPO") == null) ? "" : rs.getString("DESC_AUTO_EPO");
                            
                            
				contenidoArchivo.append(cadProductivaFile.replace(',',' ')+", "+
												noProveedorFile.replace(',',' ')+", "+
												nafinElecFile.replace(',',' ')+", "+
												noSiracFile.replace(',',' ')+", "+
												razonSocialFile.replace(',',' ')+", "+
												rfcFile.replace(',',' ')+", "+
												domicilioFile.replace(',',' ')+", "+
												estadoFile.replace(',',' ')+", "+
												telefonoFile.replace(',',' ')+", "+
												versionConvenioFile.replace(',',' ')+", "+
												fechaConvenioFile.replace(',',' ')+", "+
												convenioUnicoFile.replace(',',' ')+", "+
												operaFideicomisoFile.replace(',',' ')+", "+
												floating.replace(',',' ')+","+
												fechaParamFloating.replace(',',' ')+", "+
												provExtranjeroC.replace(',',' ')+", "+
												operaDescAutoEPOC.replace(',',' ')+", "+                                                       
                                                                                                "\n");

				total++;
				if(total==1000){
					total=0;
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
			}//while(rs.next()){

			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo csv ", e);
		} finally {
			try {
			} catch(Exception e) {}
		}

		log.info(" crearCustomFile (S)");
		return nombreArchivo;
	}

	/**
	 * Metodo para generar un archivo PDF, utilizando paginaci�n
	 * @return nombreArchivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		log.info("crearPageCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

			pdfDoc.setTable(17,100);
			pdfDoc.setCell("Cadena Productiva ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("No Proveedor ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("No. Nafin Electr�nico ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("No. Sirac ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre o Raz�n Social ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("RFC ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Domicilio ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Estado ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tel�fono ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Versi�n Convenio ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Consulta ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Convenio �nico ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Opera Fideicomiso para Desarrollo de Proveedores ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Proveedores susceptibles a Floating ","celda01",ComunesPDF.CENTER);		
			pdfDoc.setCell("Fecha Parametrizaci�n Floating ","celda01",ComunesPDF.CENTER);
                        pdfDoc.setCell("Proveedor Extranjero ","celda01",ComunesPDF.CENTER);  
                        pdfDoc.setCell("Descuento Autom�tico EPO ","celda01",ComunesPDF.CENTER);                                                          

			while (rs.next()){
				noProveedorFile = (rs.getString("NUMERO_PROVEEDOR") == null) ? "" : rs.getString("NUMERO_PROVEEDOR");
				nafinElecFile = (rs.getString("NAFIN_ELECTRONICO") == null) ? "" : rs.getString("NAFIN_ELECTRONICO");
				noSiracFile = (rs.getString("NUMERO_SIRAC") == null) ? "" : rs.getString("NUMERO_SIRAC");
				cadProductivaFile = (rs.getString("CADENA_PRODUCTIVA") == null) ? "" : rs.getString("CADENA_PRODUCTIVA");
				razonSocialFile = (rs.getString("RAZON_SOCIAL") == null) ? "" : rs.getString("RAZON_SOCIAL");
				rfcFile = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				domicilioFile = (rs.getString("DIRECCION") == null) ? "" : rs.getString("DIRECCION");
				estadoFile = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				telefonoFile = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				versionConvenioFile = (rs.getString("VERSION_CONVENIO") == null) ? "" : rs.getString("VERSION_CONVENIO");
				fechaConvenioFile = (rs.getString("FECHA_CONVENIO") == null) ? "" : rs.getString("FECHA_CONVENIO");
				convenioUnicoFile = (rs.getString("CONVENIO_UNICO").equals("S")) ? "Si" : "No";
				operaFideicomisoFile = (rs.getString("CS_OPERA_FIDEICOMISO").equals("S")) ? "Si" : "No";

				String floating = (rs.getString("TASA_FLOATING") == null) ? "" : rs.getString("TASA_FLOATING");
				String provExtranjeroC = (rs.getString("PROVEEDOR_EXTRANJERO") == null) ? "" : rs.getString("PROVEEDOR_EXTRANJERO");
				String operaDescAutoEPOC = (rs.getString("DESC_AUTO_EPO") == null) ? "" : rs.getString("DESC_AUTO_EPO");
			                              
				String fechaParamFloating = "";
				if ("Si".equals(floating)  ) {
					fechaParamFloating = (rs.getString("FECHA_PARAM_FLOATING") == null) ? "" : rs.getString("FECHA_PARAM_FLOATING");
				}
				pdfDoc.setCell(cadProductivaFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(noProveedorFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nafinElecFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(noSiracFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(razonSocialFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(rfcFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(domicilioFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(estadoFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(telefonoFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(versionConvenioFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaConvenioFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(convenioUnicoFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(operaFideicomisoFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(floating,"formas",ComunesPDF.CENTER);		
				pdfDoc.setCell(fechaParamFloating,"formas",ComunesPDF.CENTER);	
				pdfDoc.setCell(provExtranjeroC,"formas",ComunesPDF.CENTER);  
				pdfDoc.setCell(operaDescAutoEPOC,"formas",ComunesPDF.CENTER);  				
			}

			pdfDoc.addTable();
			pdfDoc.endDocument();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		}

		log.info("crearPageCustomFile (S)");
		return nombreArchivo;
	}

	/**
	 * Genera la condici�n WHERE seg�n los par�metros que se pasaron
	 * @return condicion
	 */
	public String generaCondiciones(){
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		try{
			if(numeroBancoFondeo.equals("2")){
				condicion.append(" AND t7.ic_banco_fondeo = ?"); 
				conditions.add(numeroBancoFondeo);
			}
			if(sinNumeroProveedor.equals("S")){
				condicion.append(" AND t5.cs_num_proveedor = ?"); 
				conditions.add(sinNumeroProveedor);
			}
			if(!nafinElectronico.equals("")){
				condicion.append(" AND t6.ic_nafin_electronico = ?"); 
				conditions.add(nafinElectronico);
			}
			if(!rfc.equals("")){
				condicion.append(" AND t2.cg_rfc = ?"); 
				conditions.add(rfc);
			}
			if(!cadenaProductiva.equals("")){
				condicion.append(" AND t5.ic_epo = ?"); 
				conditions.add(cadenaProductiva);
			}
			if(!numeroProveedor.equals("")){
				condicion.append(" AND t5.cg_pyme_epo_interno = ?"); 
				conditions.add(numeroProveedor);
			}
			if(!numeroClienteSIRAC.equals("")){
				condicion.append(" AND t2.in_numero_sirac = ?"); 
				conditions.add(numeroClienteSIRAC);
			}
			if(datosPersModif.equals("S")){
				condicion.append(" AND t2.cs_datos_pers_modif = ?"); 
				conditions.add(datosPersModif);
			}
			if(convenioUnico.equals("S")){
				condicion.append(" AND t2.cs_convenio_unico = ?"); 
				conditions.add(convenioUnico);
			}
			if(operaFideicomiso.equals("S")){
				condicion.append(" AND t2.CS_OPERA_FIDEICOMISO = ?"); 
				conditions.add(operaFideicomiso);
			}
			if(!numDistribuidor.equals("")){
				condicion.append(" AND t5.cg_num_distribuidor = ?"); 
				conditions.add(numDistribuidor);
			}
			if(!estado.equals("")){
				condicion.append(" AND t1.ic_estado = ?"); 
				conditions.add(estado);
			}
			if(!nombre.equals("")){
				condicion.append(" AND (   (UPPER (TRIM (t2.cg_razon_social)) LIKE UPPER (?))");
				condicion.append("      OR (UPPER (TRIM (   t2.cg_nombre");
				condicion.append("                       || ' '");
				condicion.append("                       || t2.cg_appat");
				condicion.append("                       || ' '");
				condicion.append("                       || t2.cg_apmat");
				condicion.append("                      )");
				condicion.append("                ) LIKE UPPER (?)");
				condicion.append("         )");
				condicion.append("   ) ");
				conditions.add(nombre + "%");
				conditions.add(nombre + "%");
			}
							
			if ("S".equals(operaFactDistribuido)  ) {
				condicion.append(" AND t2.CS_FACT_DISTRIBUIDO = ?"); 
				conditions.add(operaFactDistribuido);			
			}			
			if (!"".equals(suceptibleFloating)  ) {
				condicion.append(" AND t2.CS_TASA_FLOATING = ?"); 
				conditions.add(suceptibleFloating);
			}                    
			if (!"".equals(provExtranjero)  ) {
				condicion.append(" AND t2.CS_PROV_EXT = ?"); 
				conditions.add(provExtranjero);      
			}                        
			if (!"".equals(operaDescAutoEPO)  ) {
				condicion.append(" AND t5.CS_DESC_AUTOMA_EPO = ?"); 
				conditions.add(operaDescAutoEPO);      
			}                    
                    
		} catch(Exception e){
			log.warn("generaCondiciones.Exception: " + e);
		}
		return condicion.toString();
	}
	
	/**
	 * Obtiene los datos del usuario por afiliado, esto es para la opci�n proveedores
	 * @throws java.lang.Exception
	 * @return datos
	 */
	public List obtieneDatosAfiliado() throws Exception {
		log.info("obtieneDatosAfiliado (E)");
		AccesoDB con = new AccesoDB();
		List datos = new ArrayList();
		try {
			con.conexionDB();

			String strSQL = "SELECT afiliado.cg_razon_social cg_razon_social, n.ic_nafin_electronico " +
								 " FROM comcat_pyme afiliado, comrel_nafin n " +
								 " WHERE afiliado.ic_pyme = n.ic_epo_pyme_if " +
								 " AND n.cg_tipo = ? " +
								 " AND n.ic_epo_pyme_if = ? ";
			log.debug("Sentencia: " + strSQL);
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, tipoAfiliado);
			ps.setInt(2, Integer.parseInt(pyme));
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				datos.add(rs.getString("cg_razon_social")== null?"":rs.getString("cg_razon_social"));
				datos.add(rs.getString("ic_nafin_electronico")== null?"":rs.getString("ic_nafin_electronico"));
			}
			rs.close();
			ps.close();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("obtieneDatosAfiliado (S)");
		return datos;
	}

	/**
	 * Genera el PDF de las cuentas de usuarios por afiliado
	 * @return nombre del archivo creado
	 */
	public String imprimeCuentasUsuariosPorAfiliado(HttpServletRequest request, String path){
		log.info("imprimeCuentasUsuariosPorAfiliado (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Cuentas de usuario por afiliado ","formas",ComunesPDF.LEFT);
			pdfDoc.addText(datosAfiliado,"formas",ComunesPDF.CENTER);

			pdfDoc.setTable(1,70);
			pdfDoc.setCell("Login de usuario ","celda01",ComunesPDF.CENTER);

			for(int i=0; i< listaCuentas.size(); i++){
				String cta = (String)listaCuentas.get(i);
				pdfDoc.setCell(cta,"formas",ComunesPDF.CENTER);
			}

			pdfDoc.addTable();
			pdfDoc.endDocument();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
		log.info("imprimeCuentasUsuariosPorAfiliado (S)");
		return nombreArchivo;

	}

	/**
	 * Obtiene los datos de la Pyme seleccionada en el GRID para modificarla
	 * @throws java.lang.Exception
	 * @return Map de resultados de la consulta
	 */
	public HashMap obtieneDatosPyme() throws Exception {
		log.info("obtieneDatosPyme (E)");
		HashMap datosPyme = new HashMap();
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		try {
			con.conexionDB();

			strSQL.append(
							" SELECT /*+ ordered use_nl(pe,p,v,d,u,c) */ "+
							" pe.cg_pyme_epo_interno, p.cg_appat, p.cg_apmat, p.cg_nombre, p.cg_rfc, "+
							" p.cg_razon_social, d.cg_calle, d.cg_numero_ext, d.cg_numero_int, "+
							" d.cg_colonia, d.ic_estado, d.ic_pais, d.cg_municipio, d.cn_cp, "+
							" d.cg_telefono1, d.cg_fax, d.cg_email, c.cg_appat AS appat_contacto, "+
							" c.cg_apmat AS apmat_contacto, c.cg_nombre AS nombre_contacto, "+
							" c.cg_tel AS tel_contacto, c.cg_fax AS fax_contacto, "+
							" c.cg_email AS email_contacto, d.ic_domicilio_epo, c.ic_contacto, "+
							" p.cs_tipo_persona, p.cg_nombre_comercial, p.cg_appat_rep_legal, "+
							" p.cg_apmat_legal, p.cg_nombrerep_legal, p.cg_telefono as TELEFONO_REP_LEGAL,  "+
							" p.cg_fax as FAX_REP_LEGAL, p.cg_email AS EMAIL_REP_LEGAL, p.ic_identificacion,  "+
							" p.ic_grado_esc, p.ic_tipo_categoria, pe.cs_aceptacion, p.cg_ejec_cta,  "+
							" p.cg_no_escritura, p.in_numero_notaria, p.in_empleos_generar, p.fn_activo_total, "+
							" p.fn_capital_contable, p.fn_ventas_net_exp, p.fn_ventas_net_tot, "+
							" TO_CHAR (p.df_constitucion, 'DD/MM/YYYY')  as FECHA_CONST, p.in_numero_emp, "+
							" p.ic_sector_econ, p.ic_subsector, p.ic_rama, p.ic_clase, "+
							" p.ic_tipo_empresa, p.ic_dom_correspondencia, p.cg_productos, "+
							" p.in_numero_sirac AS numsirac, p.ic_estado_civil AS edo_civil, "+
							" p.ic_pais_origen AS pais_origen, "+
							" TO_CHAR (p.df_nacimiento, 'DD/MM/YYYY') AS nacimiento, "+
							" p.cg_regimen_mat AS cg_regimen_mat, p.cg_sexo, p.cg_app_casada, "+
							" d.ic_municipio, p.cg_rfc_legal, p.cg_no_identificacion, "+
							" v.cd_version_convenio, v.ic_version_convenio, "+
							" TO_CHAR (p.df_version_convenio, 'DD/MM/YYYY') AS df_version_convenio, "+
							" p.in_numero_troya, p.cg_ejec_cta AS ejecutivo, "+
							" c.cg_celular AS numero_celular, pe.cs_num_proveedor, "+
							" p.cs_convenio_unico convenio_unico, "+
							" TO_CHAR (p.df_firma_conv_unico, 'dd/mm/yyyy') fecha_convenio_unico, "+
							" p.cs_cesion_derechos AS cesion_derechos, p.cg_curp AS curp, "+
							" p.cn_fiel AS fiel, d.ic_ciudad AS ciudad, "+
							" p.cs_fianza_electronica AS fianza_electronica, p.cs_notificacion_nafin "+
							" ,p.CS_OPERA_FIDEICOMISO AS opera_fideicomiso "+
							" ,P.CS_ENTIDAD_GOBIERNO AS entidad_gobierno  "+ 
							" ,p.CS_FACT_DISTRIBUIDO  as factorajeDistribuido "+                                                        
							" ,p.CS_PROV_EXT  as chkProvExtranjero "+
                            " ,pe.CS_DESC_AUTOMA_EPO  as chkOperaDescAutoEPO "+
							
							" FROM comrel_pyme_epo pe, "+
							" comcat_pyme p, "+
							" comcat_version_convenio v, "+
							" com_domicilio d, "+
							" com_contacto c "+
							" WHERE p.ic_pyme = pe.ic_pyme "+
							" AND p.ic_pyme = d.ic_pyme "+ 
							" AND p.ic_version_convenio = v.ic_version_convenio(+) "+
							" AND p.ic_pyme = c.ic_pyme "+
							" AND d.cs_fiscal = 'S' "+
							" and c.CS_PRIMER_CONTACTO  =  'S' " +   
							" AND p.ic_pyme = "+ pyme +
							" AND pe.ic_epo = "+ icEpo );

			log.debug("Sentencia: " + strSQL.toString());
			PreparedStatement ps = con.queryPrecompilado(strSQL.toString());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				//--> Armo la direcci�n
				String calle = (rs.getString("CG_CALLE")      ==null)?"": rs.getString("CG_CALLE"), 
						 noExt = (rs.getString("CG_NUMERO_EXT") ==null)?"": rs.getString("CG_NUMERO_EXT"), 
						 noInt = (rs.getString("CG_NUMERO_INT") ==null)?"": rs.getString("CG_NUMERO_INT");

				//--> Hay que poner atenci�n en este campo
				String autoriza_mod = (rs.getString("CS_NOTIFICACION_NAFIN") ==null)?"":rs.getString("CS_NOTIFICACION_NAFIN");
				if(!autoriza_mod.equals("")){
					datosPyme.put("autoriza_mod", (!autoriza_mod.equals("S"))?"N":"S");
				} else {
					datosPyme.put("autoriza_mod", "N" );
				}
				//--> Datos ocultos
				datosPyme.put("Numero_Exterior",             noExt);
				datosPyme.put("Sin_Num_Prov",                (rs.getString("cs_num_proveedor").equals("S")) ?"true":"false");
				datosPyme.put("CS_ACEPTACION",               (rs.getString("CS_ACEPTACION")          ==null)?"":rs.getString("CS_ACEPTACION"));
				datosPyme.put("CG_PYME_EPO_INTERNO",         (rs.getString("CG_PYME_EPO_INTERNO")    ==null)?"":rs.getString("CG_PYME_EPO_INTERNO"));
				datosPyme.put("CS_TIPO_PERSONA",             (rs.getString("CS_TIPO_PERSONA")        ==null)?"":rs.getString("CS_TIPO_PERSONA"));
				//--> Persona f�sica
				datosPyme.put("apellido_paterno_mod",        (rs.getString("CG_APPAT")               ==null)?"":rs.getString("CG_APPAT"));
				datosPyme.put("apellido_materno_mod",        (rs.getString("CG_APMAT")               ==null)?"":rs.getString("CG_APMAT"));
				datosPyme.put("nombre_mod",                  (rs.getString("CG_NOMBRE")              ==null)?"":rs.getString("CG_NOMBRE"));
				datosPyme.put("rfc_mod",                     (rs.getString("CG_RFC")                 ==null)?"":rs.getString("CG_RFC"));
				datosPyme.put("curp_mod",                    (rs.getString("CURP")                   ==null)?"":rs.getString("CURP"));
				datosPyme.put("fiel_modF",                    (rs.getString("FIEL")                   ==null)?"":rs.getString("FIEL"));
				datosPyme.put("apellido_casada_mod",         (rs.getString("CG_APP_CASADA")          ==null)?"":rs.getString("CG_APP_CASADA"));
				datosPyme.put("regimen_matrimonial_mod",     (rs.getString("CG_REGIMEN_MAT")         ==null)?"":rs.getString("CG_REGIMEN_MAT"));
				datosPyme.put("sexo_mod",                    (rs.getString("CG_SEXO")                ==null)?"":rs.getString("CG_SEXO"));
				datosPyme.put("estado_civil_mod",            (rs.getString("EDO_CIVIL")              ==null)?"":rs.getString("EDO_CIVIL"));
				datosPyme.put("fecha_nac_mod",               (rs.getString("NACIMIENTO")             ==null)?"":rs.getString("NACIMIENTO"));
				datosPyme.put("pais_origen_modF",             (rs.getString("PAIS_ORIGEN")            ==null)?"":rs.getString("PAIS_ORIGEN"));
				datosPyme.put("identificacion_mod",          (rs.getString("IC_IDENTIFICACION")      ==null)?"":rs.getString("IC_IDENTIFICACION"));
				datosPyme.put("grado_escolaridad_mod",       (rs.getString("IC_GRADO_ESC")           ==null)?"":rs.getString("IC_GRADO_ESC"));
				//--> Persona moral
				datosPyme.put("razon_social_mod",            (rs.getString("CG_RAZON_SOCIAL")        ==null)?"":rs.getString("CG_RAZON_SOCIAL"));
				datosPyme.put("nombre_comer_mod",            (rs.getString("CG_NOMBRE_COMERCIAL")    ==null)?"":rs.getString("CG_NOMBRE_COMERCIAL"));
				datosPyme.put("rfc_modM",                    (rs.getString("CG_RFC")                 ==null)?"":rs.getString("CG_RFC"));
				datosPyme.put("fiel_modM",                   (rs.getString("FIEL")                   ==null)?"":rs.getString("FIEL"));
				datosPyme.put("pais_origen_modM",            (rs.getString("PAIS_ORIGEN")            ==null)?"":rs.getString("PAIS_ORIGEN"));
				datosPyme.put("direccion_mod",               calle + " " + noExt + " " + noInt);
				datosPyme.put("colonia_mod",                 (rs.getString("CG_COLONIA")             ==null)?"":rs.getString("CG_COLONIA"));
				datosPyme.put("estado_mod",                  (rs.getString("IC_ESTADO")              ==null)?"":rs.getString("IC_ESTADO"));
				datosPyme.put("deleg_mun_mod",               (rs.getString("IC_MUNICIPIO")           ==null)?"":rs.getString("IC_MUNICIPIO"));
				datosPyme.put("cod_postal_mod",              (rs.getString("CN_CP")                  ==null)?"":rs.getString("CN_CP"));
				datosPyme.put("pais_mod",                    (rs.getString("IC_PAIS")                ==null)?"":rs.getString("IC_PAIS"));
				datosPyme.put("ciudad_mod",                  (rs.getString("CIUDAD")                 ==null)?"":rs.getString("CIUDAD"));
				datosPyme.put("telefono_mod",                (rs.getString("CG_TELEFONO1")           ==null)?"":rs.getString("CG_TELEFONO1"));
				datosPyme.put("fax_mod",                     (rs.getString("CG_FAX")                 ==null)?"":rs.getString("CG_FAX"));
				datosPyme.put("email_mod",                   (rs.getString("CG_EMAIL")               ==null)?"":rs.getString("CG_EMAIL"));
				//--> Representante legal o persona autorizada
				datosPyme.put("ap_paterno_rep_mod",          (rs.getString("CG_APPAT_REP_LEGAL")     ==null)?"":rs.getString("CG_APPAT_REP_LEGAL"));
				datosPyme.put("ap_materno_rep_mod",          (rs.getString("CG_APMAT_LEGAL")         ==null)?"":rs.getString("CG_APMAT_LEGAL"));
				datosPyme.put("nombre_rep_mod",              (rs.getString("CG_NOMBREREP_LEGAL")     ==null)?"":rs.getString("CG_NOMBREREP_LEGAL"));
				datosPyme.put("telefono_rep_mod",            (rs.getString("TELEFONO_REP_LEGAL")     ==null)?"":rs.getString("TELEFONO_REP_LEGAL"));
				datosPyme.put("fax_rep_mod",                 (rs.getString("FAX_REP_LEGAL")          ==null)?"":rs.getString("FAX_REP_LEGAL"));
				datosPyme.put("email_rep_mod",               (rs.getString("EMAIL_REP_LEGAL")        ==null)?"":rs.getString("EMAIL_REP_LEGAL"));
				datosPyme.put("rfc_rep_mod",                 (rs.getString("CG_RFC_LEGAL")           ==null)?"":rs.getString("CG_RFC_LEGAL"));
				datosPyme.put("no_identificacion_rep_mod",   (rs.getString("CG_NO_IDENTIFICACION")   ==null)?"":rs.getString("CG_NO_IDENTIFICACION"));
				datosPyme.put("identificacion_rep_mod",      (rs.getString("IC_IDENTIFICACION")      ==null)?"":rs.getString("IC_IDENTIFICACION"));
				//--> Datos del contacto
				datosPyme.put("ap_paterno_cont_mod",         (rs.getString("APPAT_CONTACTO")         ==null)?"":rs.getString("APPAT_CONTACTO"));
				datosPyme.put("ap_materno_cont_mod",         (rs.getString("APMAT_CONTACTO")         ==null)?"":rs.getString("APMAT_CONTACTO"));
				datosPyme.put("nombre_cont_mod",             (rs.getString("NOMBRE_CONTACTO")        ==null)?"":rs.getString("NOMBRE_CONTACTO"));
				datosPyme.put("telefono_cont_mod",           (rs.getString("TEL_CONTACTO")           ==null)?"":rs.getString("TEL_CONTACTO"));
				datosPyme.put("fax_cont_mod",                (rs.getString("FAX_CONTACTO")           ==null)?"":rs.getString("FAX_CONTACTO"));
				datosPyme.put("email_cont_mod",              (rs.getString("EMAIL_CONTACTO")         ==null)?"":rs.getString("EMAIL_CONTACTO"));
				datosPyme.put("celular_cont_mod",            (rs.getString("NUMERO_CELULAR")         ==null)?"":rs.getString("NUMERO_CELULAR"));
				//--> Tipo de categor�a y ejecutivo de cuenta se muestran si CS_ACEPTACION = H || R
				datosPyme.put("tipo_categoria_mod",          (rs.getString("IC_TIPO_CATEGORIA")      ==null)?"":rs.getString("IC_TIPO_CATEGORIA"));
				datosPyme.put("ejecut_cuenta_mod",           (rs.getString("CG_EJEC_CTA")            ==null)?"":rs.getString("CG_EJEC_CTA"));
				datosPyme.put("num_proveedor_cont_mod",      (rs.getString("CG_PYME_EPO_INTERNO")    ==null)?"":rs.getString("CG_PYME_EPO_INTERNO"));
				datosPyme.put("Sin_Num_Prov_cont_mod",       (rs.getString("cs_num_proveedor").equals("S")) ?"true":"false");
				//--> Peden estar en Datos del contacto si CS_ACEPTACION != H || R  o Productos si CS_ACEPTACION = H || R
				datosPyme.put("fecha_convenio_cont_mod1",     (rs.getString("FECHA_CONVENIO_UNICO")   ==null)?  "":rs.getString("FECHA_CONVENIO_UNICO"));
				datosPyme.put("convenio_unico_prod_mod1",     (rs.getString("CONVENIO_UNICO").equals("S"))?    "true":"false");
				datosPyme.put("cesion_derechos_cont_mod",    (rs.getString("CESION_DERECHOS").equals("S"))?   "true":"false");
				datosPyme.put("fianza_electronica_mod1",        (rs.getString("FIANZA_ELECTRONICA").equals("S"))?"true":"false");
				datosPyme.put("fideicomiso_desarrollo_prod_mod1",        (rs.getString("opera_fideicomiso").equals("S"))?"true":"false");
				datosPyme.put("chkEntidadGobierno1",        (rs.getString("entidad_gobierno").equals("S"))?"true":"");
				
				//-->Diversos
				datosPyme.put("no_escritura_mod",            (rs.getString("CG_NO_ESCRITURA")        ==null)?"":rs.getString("CG_NO_ESCRITURA"));
				datosPyme.put("no_notaria_mod",              (rs.getString("IN_NUMERO_NOTARIA")      ==null)?"":rs.getString("IN_NUMERO_NOTARIA"));
				datosPyme.put("empleos_gen_mod",             (rs.getString("IN_EMPLEOS_GENERAR")     ==null)?"":rs.getString("IN_EMPLEOS_GENERAR"));
				datosPyme.put("activo_total_mod",            (rs.getString("FN_ACTIVO_TOTAL")        ==null)?"":rs.getString("FN_ACTIVO_TOTAL"));
				datosPyme.put("cap_contable_mod",            (rs.getString("FN_CAPITAL_CONTABLE")    ==null)?"":rs.getString("FN_CAPITAL_CONTABLE"));
				datosPyme.put("ventas_exp_mod",              (rs.getString("FN_VENTAS_NET_EXP")      ==null)?"":rs.getString("FN_VENTAS_NET_EXP"));
				datosPyme.put("ventas_tot_mod",              (rs.getString("FN_VENTAS_NET_TOT")      ==null)?"":rs.getString("FN_VENTAS_NET_TOT"));
				datosPyme.put("fecha_const_mod",             (rs.getString("FECHA_CONST")            ==null)?"":rs.getString("FECHA_CONST"));
				datosPyme.put("no_empleados_mod",            (rs.getString("IN_NUMERO_EMP")          ==null)?"":rs.getString("IN_NUMERO_EMP"));
				datosPyme.put("princip_productos_mod",       (rs.getString("CG_PRODUCTOS")           ==null)?"":rs.getString("CG_PRODUCTOS"));
				datosPyme.put("sector_mod",                  (rs.getString("IC_SECTOR_ECON")         ==null)?"":rs.getString("IC_SECTOR_ECON"));
				datosPyme.put("subsector_mod",               (rs.getString("IC_SUBSECTOR")           ==null)?"":rs.getString("IC_SUBSECTOR"));
				datosPyme.put("rama_mod",                    (rs.getString("IC_RAMA")                ==null)?"":rs.getString("IC_RAMA"));
				datosPyme.put("clase_mod",                   (rs.getString("IC_CLASE")               ==null)?"":rs.getString("IC_CLASE"));
				datosPyme.put("tipo_empresa_mod",            (rs.getString("IC_TIPO_EMPRESA")        ==null)?"":rs.getString("IC_TIPO_EMPRESA"));
				datosPyme.put("domicilio_corr_mod",          (rs.getString("IC_DOM_CORRESPONDENCIA") ==null)?"":rs.getString("IC_DOM_CORRESPONDENCIA"));
				datosPyme.put("num_proveedor_diversos_mod",  (rs.getString("CG_PYME_EPO_INTERNO")    ==null)?"":rs.getString("CG_PYME_EPO_INTERNO"));
				datosPyme.put("Sin_Num_Prov_diversos_mod",   (rs.getString("cs_num_proveedor").equals("S")) ?"true":"false");
				//-->Otros datos
				datosPyme.put("num_sirac_mod",               (rs.getString("NUMSIRAC")               ==null)?"":rs.getString("NUMSIRAC"));
				datosPyme.put("num_troya_mod",               (rs.getString("IN_NUMERO_TROYA")        ==null)?"":rs.getString("IN_NUMERO_TROYA"));
				//--> Productos
				datosPyme.put("version_convenio_mod-",       (rs.getString("CD_VERSION_CONVENIO")    ==null)?"":rs.getString("CD_VERSION_CONVENIO"));
				datosPyme.put("version_convenio_mod",        (rs.getString("IC_VERSION_CONVENIO")    ==null)?"":rs.getString("IC_VERSION_CONVENIO"));
				datosPyme.put("fecha_consulta_convenio_mod", (rs.getString("DF_VERSION_CONVENIO")    ==null)?"":rs.getString("DF_VERSION_CONVENIO"));
				datosPyme.put("fecha_convenio_prod_mod",     (rs.getString("FECHA_CONVENIO_UNICO")   ==null)?"":rs.getString("FECHA_CONVENIO_UNICO"));
				datosPyme.put("IC_DOMICILIO_EPO",            (rs.getString("IC_DOMICILIO_EPO")       ==null)?"":rs.getString("IC_DOMICILIO_EPO"));
				datosPyme.put("IC_CONTACTO",                 (rs.getString("IC_CONTACTO")            ==null)?"":rs.getString("IC_CONTACTO"));
				datosPyme.put("EJECUTIVO",                   (rs.getString("EJECUTIVO")              ==null)?"":rs.getString("EJECUTIVO"));
				datosPyme.put("NUMERO_CELULAR",              (rs.getString("NUMERO_CELULAR")         ==null)?"":rs.getString("NUMERO_CELULAR"));				
				datosPyme.put("chkFactDistribuido1",        ("S".equals(rs.getString("factorajeDistribuido")))?"true":"");
                            
				datosPyme.put("chkProvExtranjero",        ("S".equals(rs.getString("chkProvExtranjero")))?"true":"");
				datosPyme.put("chkOperaDescAutoEPO",        ("S".equals(rs.getString("chkOperaDescAutoEPO")))?"true":"");
                               
                            

			}
			rs.close();
			ps.close();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("obtieneDatosPyme (S)");
		return datosPyme;
	}

/************************************************************************
 *                    GETTERS AND SETTERS                               *
 ************************************************************************/
 	public List getConditions(){
		return conditions; 
	}

 	public List getListaCuentas(){
		return listaCuentas; 
	}

	public void setListaCuentas(List listaCuentas) {
		this.listaCuentas = listaCuentas;
	}
	
	public String getNumeroBancoFondeo() {
		return numeroBancoFondeo;
	}

	public void setNumeroBancoFondeo(String numeroBancoFondeo) {
		this.numeroBancoFondeo = numeroBancoFondeo;
	}

	public String getSinNumeroProveedor() {
		return sinNumeroProveedor;
	}

	public void setSinNumeroProveedor(String sinNumeroProveedor) {
		this.sinNumeroProveedor = sinNumeroProveedor;
	}

	public String getNafinElectronico() {
		return nafinElectronico;
	}

	public void setNafinElectronico(String nafinElectronico) {
		this.nafinElectronico = nafinElectronico;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getCadenaProductiva() {
		return cadenaProductiva;
	}

	public void setCadenaProductiva(String cadenaProductiva) {
		this.cadenaProductiva = cadenaProductiva;
	}

	public String getNumeroProveedor() {
		return numeroProveedor;
	}

	public void setNumeroProveedor(String numeroProveedor) {
		this.numeroProveedor = numeroProveedor;
	}

	public String getNumeroClienteSIRAC() {
		return numeroClienteSIRAC;
	}

	public void setNumeroClienteSIRAC(String numeroClienteSIRAC) {
		this.numeroClienteSIRAC = numeroClienteSIRAC;
	}

	public String getDatosPersModif() {
		return datosPersModif;
	}

	public void setDatosPersModif(String datosPersModif) {
		this.datosPersModif = datosPersModif;
	}

	public String getConvenioUnico() {
		return convenioUnico;
	}

	public void setConvenioUnico(String convenioUnico) {
		this.convenioUnico = convenioUnico;
	}

	public String getNumDistribuidor() {
		return numDistribuidor;
	}

	public void setNumDistribuidor(String numDistribuidor) {
		this.numDistribuidor = numDistribuidor;
	}

	public String getTipoAfiliado() {
		return tipoAfiliado;
	}

	public void setTipoAfiliado(String tipoAfiliado) {
		this.tipoAfiliado = tipoAfiliado;
	}

	public String getPyme() {
		return pyme;
	}

	public void setPyme(String pyme) {
		this.pyme = pyme;
	}

	public String getDatosAfiliado() {
		return datosAfiliado;
	}

	public void setDatosAfiliado(String datosAfiliado) {
		this.datosAfiliado = datosAfiliado;
	}

	public String getIcEpo() {
		return icEpo;
	}

	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getOperaFideicomiso() {
		return operaFideicomiso;
	}

	public void setOperaFideicomiso(String operaFideicomiso) {
		this.operaFideicomiso = operaFideicomiso;
	}
	

	public void setOperaFactDistribuido(String operaFactDistribuido) {
		this.operaFactDistribuido = operaFactDistribuido;
	}

	public String getSuceptibleFloating() {
		return suceptibleFloating;
	}
    
	public String getOperaFactDistribuido() {
		return operaFactDistribuido;
	}

    public void setSuceptibleFloating(String setSuceptibleFloating ){
        this.suceptibleFloating = setSuceptibleFloating;
    }
    
    /**
     * Metodo para validar que datos se han modificado con la pyme  
     * @param datosAnteriores
     * @param datosActuales
     * @param datosPyme
     * @throws NafinException
     */
    public void  validaAfiliados( Map<String, Object>  datosAnteriores  , Map<String, Object>  datosActuales, Map<String, Object>  datosPyme ) throws NafinException{
          StringBuilder tramite = new StringBuilder();         
        try {     
       
            String tipoPersona = datosActuales.get("CS_TIPO_PERSONA").toString();
            String aceptacion = datosActuales.get("CS_ACEPTACION").toString(); 
            
            if ("F".equals(tipoPersona) ) {    // Persona Fisica                 
                //Nombre Completo  (Anteriores)                
                String  apPaternoAn = datosAnteriores.get("apellido_paterno_mod").toString().replaceAll(" ", "");
                String  apMaternoAn = datosAnteriores.get("apellido_materno_mod").toString().replaceAll(" ", "");
                String  apNombreAn = datosAnteriores.get("nombre_mod").toString().replaceAll(" ", "");
                String  curFisAn = datosAnteriores.get("curp_mod").toString().replaceAll(" ", "");
                String  fielFisAn = datosAnteriores.get("fiel_modF").toString().replaceAll(" ", "");
                String  apellidoCasadaAn = datosAnteriores.get("apellido_casada_mod").toString().replaceAll(" ", "");
                String  regimenMatriAn = datosAnteriores.get("regimen_matrimonial_mod").toString().replaceAll(" ", ""); 
                String  sexoFAn = datosAnteriores.get("sexo_mod").toString().replaceAll(" ", ""); 
                String  estadoCivilAn = datosAnteriores.get("estado_civil_mod").toString().replaceAll(" ", ""); 
                String  fechaNaciAn = datosAnteriores.get("fecha_nac_mod").toString().replaceAll(" ", "");
                String  paisOrigenAn = datosAnteriores.get("pais_origen_modF").toString().replaceAll(" ", ""); 
                String  identificacionAn = datosAnteriores.get("identificacion_mod").toString().replaceAll(" ", ""); 
                String  escolaridadAn = datosAnteriores.get("grado_escolaridad_mod").toString().replaceAll(" ", "");                 
                // Nombre Completo  (Actuales)
                String  apPaternoAc = datosActuales.get("apellido_paterno_mod").toString().replaceAll(" ", "");
                String  apMaternoAc = datosActuales.get("apellido_materno_mod").toString().replaceAll(" ", "");
                String  apNombreAc = datosActuales.get("nombre_mod").toString().replaceAll(" ", "");                
                String  curFisAc  = datosActuales.get("curp_mod").toString().replaceAll(" ", "");
                String  fielFisAc = datosActuales.get("fiel_modF").toString().replaceAll(" ", "");
                String  apellidoCasadaAc = datosActuales.get("apellido_casada_mod").toString().replaceAll(" ", "");
                String  regimenMatriAc = datosActuales.get("regimen_matrimonial_mod").toString().replaceAll(" ", ""); 
                String  sexoFAc = datosActuales.get("sexo_mod").toString().replaceAll(" ", "");
                String  estadoCivilAc = datosActuales.get("estado_civil_mod").toString().replaceAll(" ", ""); 
                String  fechaNaciAc = datosActuales.get("fecha_nac_mod").toString().replaceAll(" ", ""); 
                String  paisOrigenAc = datosActuales.get("pais_origen_modF").toString().replaceAll(" ", "");
                String  identificacionAc = datosActuales.get("identificacion_mod").toString().replaceAll(" ", ""); 
                String  escolaridadAc = datosActuales.get("grado_escolaridad_mod").toString().replaceAll(" ", ""); 
                        
                if( ("H".equals(aceptacion)  || "R".equals(aceptacion) )   &&              
                     (  !apPaternoAn.equals(apPaternoAc)   ||  !apMaternoAn.equals(apMaternoAc)   ||   !apNombreAn.equals(apNombreAc)  ||   !curFisAn.equals(curFisAc)  
                          ||   !fielFisAn.equals(fielFisAc)   ||   !apellidoCasadaAn.equals(apellidoCasadaAc)    ||   !regimenMatriAn.equals(regimenMatriAc)   ||   !sexoFAn.equals(sexoFAc) 
                          ||   !estadoCivilAn.equals(estadoCivilAc)   ||   !fechaNaciAn.equals(fechaNaciAc)  ||   !paisOrigenAn.equals(paisOrigenAc)  ||   !identificacionAn.equals(identificacionAc) 
                          ||   !escolaridadAn.equals(escolaridadAc) )   ) {   
                            tramite.append(tramite.length() > 0 ? "|" :"").append("2"); //Cambio Nombre Completo                            
                           
                }else  if(  ("N".equals(aceptacion)  || "S".equals(aceptacion) )   &&  
                            ( !apPaternoAn.equals(apPaternoAc)   || !apMaternoAn.equals(apMaternoAc)  || !apNombreAn.equals(apNombreAc)  
                           || !fechaNaciAn.equals(fechaNaciAc)  || !paisOrigenAn.equals(paisOrigenAc) ) ){ 
                            
                            tramite.append(tramite.length() > 0 ? "|" :"").append("2"); //Cambio Nombre Completo 
                }               
                
            }else   if ("M".equals(tipoPersona) ) {  //Persona Moral 
                                                                  
                String  razonSocialAn= datosAnteriores.get("razon_social_mod").toString().replaceAll(" ", ""); 
                String  nomComercialAn = datosAnteriores.get("nombre_comer_mod").toString().replaceAll(" ", ""); 
                String  fieldAn = datosAnteriores.get("fiel_modM").toString().replaceAll(" ", ""); 
                String  paisOrigenAn = datosAnteriores.get("pais_origen_modM").toString().replaceAll(" ", "");
                
                String  razonSocialAc= datosActuales.get("razon_social_mod").toString().replaceAll(" ", ""); 
                String  nomComercialAc = datosActuales.get("nombre_comer_mod").toString().replaceAll(" ", ""); 
                String  fieldAc = datosActuales.get("fiel_modM").toString().replaceAll(" ", ""); 
                String  paisOrigenAc = datosActuales.get("pais_origen_modM").toString().replaceAll(" ", "");
                
                if( ( ("H".equals(aceptacion) || "R".equals(aceptacion) ) || "N".equals(aceptacion) || "S".equals(aceptacion) )                     
                    &&  ( !razonSocialAn.equals(razonSocialAc)   || !nomComercialAn.equals(nomComercialAc)   
                        || !fieldAn.equals(fieldAc) || !paisOrigenAn.equals(paisOrigenAc)    )  ){     
                        
                        tramite.append(tramite.length() > 0 ? "|" :"").append("1"); //Cambio Nombre de Empresa y/o RFC                    
                }               
                               
                // Datos Anteriores de Representante Legal
                String  apPaternoAn= datosAnteriores.get("ap_paterno_rep_mod").toString().replaceAll(" ", ""); 
                String  apMaternoAn= datosAnteriores.get("ap_materno_rep_mod").toString().replaceAll(" ", ""); 
                String  nombreAn= datosAnteriores.get("nombre_rep_mod").toString().replaceAll(" ", ""); 
                String  telefonoAn= datosAnteriores.get("telefono_rep_mod").toString().replaceAll(" ", ""); 
                String  faxAn= datosAnteriores.get("fax_rep_mod").toString().replaceAll(" ", ""); 
                String  emailAn= datosAnteriores.get("email_rep_mod").toString().replaceAll(" ", ""); 
                String  refAn= datosAnteriores.get("rfc_rep_mod").toString().replaceAll(" ", ""); 
                String  noIdentiAn= datosAnteriores.get("no_identificacion_rep_mod").toString().replaceAll(" ", ""); 
                String  identificacionAn= datosAnteriores.get("identificacion_rep_mod").toString().replaceAll(" ", ""); 
                               
                // Datos Anteriores de Representante Legal
                String  apPaternoAc = datosActuales.get("ap_paterno_rep_mod").toString().replaceAll(" ", ""); 
                String  apMaternoAc  = datosActuales.get("ap_materno_rep_mod").toString().replaceAll(" ", ""); 
                String  nombreAc = datosActuales.get("nombre_rep_mod").toString().replaceAll(" ", ""); 
                String  telefonoAc = datosActuales.get("telefono_rep_mod").toString().replaceAll(" ", ""); 
                String  faxAc = datosActuales.get("fax_rep_mod").toString().replaceAll(" ", ""); 
                String  emailAc = datosActuales.get("email_rep_mod").toString().replaceAll(" ", ""); 
                String  refAc  = datosActuales.get("rfc_rep_mod").toString().replaceAll(" ", ""); 
                String  noIdentiAc = datosActuales.get("no_identificacion_rep_mod").toString().replaceAll(" ", ""); 
                String  identificacionAc = datosActuales.get("identificacion_rep_mod").toString().replaceAll(" ", ""); 
                
                if ( !apPaternoAn.equals(apPaternoAc)  || !apMaternoAn.equals(apMaternoAc)   || !nombreAn.equals(nombreAc)    || !telefonoAn.equals(telefonoAc)
                    || !faxAn.equals(faxAc)     || !emailAn.equals(emailAc)   || !refAn.equals(refAc)    || !noIdentiAn.equals(noIdentiAc)
                    || !identificacionAn.equals(identificacionAc) ){
                         tramite.append(tramite.length() > 0 ? "|" :"").append("4"); // Cambio Representante Legal   
                }
            }
            
            // Datos Anteriores de Domicilio 
            String calleAn = datosAnteriores.get("direccion_mod").toString().replaceAll(" ", ""); 
            String coloniaAn = datosAnteriores.get("colonia_mod").toString().replaceAll(" ", ""); 
            String codPostalAn = datosAnteriores.get("cod_postal_mod").toString().replaceAll(" ", ""); 
            String paisAn = datosAnteriores.get("pais_mod").toString().replaceAll(" ", ""); 
            String estadoAn = datosAnteriores.get("estado_mod").toString().replaceAll(" ", ""); 
            String delegacionAn = datosAnteriores.get("deleg_mun_mod").toString().replaceAll(" ", ""); 
            String ciudadModAn = datosAnteriores.get("ciudad_mod").toString().replaceAll(" ", ""); 
            String telefonoAn = datosAnteriores.get("telefono_mod").toString().replaceAll(" ", ""); 
            String  faxAn = datosAnteriores.get("fax_mod").toString().replaceAll(" ", "");
             
            // Datos Actuales de Domicilio 
            String calleAc = datosActuales.get("direccion_mod").toString().replaceAll(" ", "");
            String coloniaAc = datosActuales.get("colonia_mod").toString().replaceAll(" ", "");
            String codPostalAc = datosActuales.get("cod_postal_mod").toString().replaceAll(" ", "");
            String paisAc = datosActuales.get("pais_mod").toString().replaceAll(" ", "");
            String estadoAc = datosActuales.get("estado_mod").toString().replaceAll(" ", "");
            String delegacionAc = datosActuales.get("deleg_mun_mod").toString().replaceAll(" ", "");
            String ciudadModAc = datosActuales.get("ciudad_mod").toString().replaceAll(" ", ""); 
            String telefonoAc = datosActuales.get("telefono_mod").toString().replaceAll(" ", "");
            String  faxAc = datosActuales.get("fax_mod").toString().replaceAll(" ", "");
            
            if ( !calleAn.equals(calleAc)     || !coloniaAn.equals(coloniaAc)  || !codPostalAn.equals(codPostalAc)  || !paisAn.equals(paisAc)   
                 || !estadoAn.equals(estadoAc)    || !delegacionAn.equals(delegacionAc)   || !ciudadModAn.equals(ciudadModAc)
                || !telefonoAn.equals(telefonoAc)     || !faxAn.equals(faxAc)  ){                
                tramite.append(tramite.length() > 0 ? "|" :"").append("3"); //Cambio Domicilio                
            }
                       
            if (!"".equals(tramite.toString())  ) {           
                this.cambiosBitaAfiliados( datosPyme , tramite.toString() );
            }                                          
       
        } catch (Exception e) {
            log.error(e);   
        }        
    }



    /**
     *  metodo para validar si hubo cambios en la modificaci�n del afiliado proveedor      *
     * @param datosPyme
     * @param cgTramite
     * @throws NafinException
     */
    private void cambiosBitaAfiliados( Map<String, Object>  datosPyme , String cgTramite) throws NafinException{  
        
        log.info("::cambiosBitaAfiliados(e)");   
        
        AccesoDB con = new AccesoDB();
        StringBuilder qrySentencia = new StringBuilder();   
        List lVarBind = new ArrayList();
        boolean lbOK = true; 
        PreparedStatement ps = null;   
        
        try {    
            con.conexionDB();
            
            String  iNoUsuario = datosPyme.get("iNoUsuario").toString();
            String  icPyme = datosPyme.get("icPyme").toString();           
            String  icNafinElectronico = datosPyme.get("icNafinElectronico").toString();  
            
            qrySentencia = new StringBuilder();           
            qrySentencia.append(" INSERT INTO COM_BIT_AFILIADOS  "+
                                " (  IC_BIT_AFILIADOS, IC_NAFIN_ELECTRONICO,  IC_PYME,  CG_TRAMITE ,  IC_USUARIO, DF_MODIFICACION ) "+
                                "  VALUES ( COM_BIT_AFILIADOS_SEQ.nextval, ?,  ?, ?, ?,  Sysdate ) ");
            
            lVarBind = new ArrayList();
            lVarBind.add(icNafinElectronico);           
            lVarBind.add(icPyme);                    
            lVarBind.add(cgTramite);
            lVarBind.add(iNoUsuario);          
                 
            log.debug(" qrySentencia "+qrySentencia.toString()+ " \n lVarBind  "+ lVarBind );            
            ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();
                        
        
        } catch (Exception e) {
            log.error(e);
            lbOK = false;
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(lbOK);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
                log.info("::cambiosBitaAfiliados(S)");
            }
        
    }
    
    /**
     * @param provExtranjero
     */
    public void setProvExtranjero(String provExtranjero) {
        this.provExtranjero = provExtranjero;
    }
    
    /**
     * @return
     */
    public String getProvExtranjero() {
        return provExtranjero;
    }

    /**
     * @param operaDescAutoEPO
     */
    public void setOperaDescAutoEPO(String operaDescAutoEPO) {
        this.operaDescAutoEPO = operaDescAutoEPO;
    }

    /**
     * @return
     */
    public String getOperaDescAutoEPO() {
        return operaDescAutoEPO;
    }


}
