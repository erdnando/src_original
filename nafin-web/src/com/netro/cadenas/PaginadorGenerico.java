/**
 * @class		:PaginadorGenerico
 * 				Implementa la interfaz IQueryGeneratorRegExtJS, esta clase es pasada el constructor de la clase
 *             CQueryHelperRegExtJS como parametro para obtener la informacion a mostrar en la paginacion.
 *             
 * @IMPORTANTE	Para esta clase se deben asignar los campos, la tabla, condiciones y el orden, para formar el query a ser usado por
 * 				getDocumentQueryFile usando los metodos de acceso:
 *             @setTabla()
 *             @setCampos()
 *             @setCondicion()
 *             @serOrden()
 * 		
 * @Pantalla :	Usada en aquellas que no necesitan paginaci�n.
 * @PERFIL	 : Varios.
 * @date		 :	20-10-2014.
 * @by		 :	rpastrana.
 */
 
package com.netro.cadenas;

import java.sql.ResultSet;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class PaginadorGenerico implements IQueryGeneratorRegExtJS {

  /**
   * Declaracion de variables
	* @log			: muestra informacion de los eventos que suceden en el servidor.
	* @orden			: recibe el orden para mostrar la informacion.
	* @tabla			: recibe la tabla.
	* @campos		: recibe los campos a mostrar.
	* @strQuery		: almacena el query que sera ejecutado. 
	* @condicion	: recibe la condicion WHERE.
	* @conditions	: almacena la lista de variables que seran usadas como Bind(?).
   */
	  
  private String tabla  	="";
  private String orden 		="";
  private String campos 	="";
  private String condicion ="";
  private List   conditions;
  private StringBuffer strQuery = new StringBuffer();
  private final static Log log  = ServiceLocator.getInstance().getLog(PaginadorGenerico.class); 

 /**
  * Constructor by Default
  */
  public PaginadorGenerico()  {  }

  /**
	 * Este m�todo debe regresar un query con el que se obtienen los
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQuery(){
  return "";
  }


	/**
	 * Este m�todo debe regresar un query con el que se obtienen los
	 * datos a mostrar en la consulta basandose en los identificadores unicos
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentSummaryQueryForIds(List ids){
  return "";
  }


	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getAggregateCalculationQuery(){
  return "";
  }


	/**
	 * Este m�todo debe regresar una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
	public List getConditions(){
  return conditions;
  }


	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQueryFile(){
		strQuery   = new StringBuffer(""); 
		log.info("getDocumentQueryFile(E)");
		if(!campos.equals("")){
			strQuery.append( " SELECT "+ campos );
		}
		if(!tabla.equals("")){
			strQuery.append( " FROM " + tabla);	
		}
		if(!condicion.equals("")){
			strQuery.append(" WHERE " + condicion );
		}
		if(!orden.equals("")){
			strQuery.append( " ORDER BY " + orden );
		}
		log.debug("getDocumentQueryFile: "+strQuery.toString());
		log.info("getDocumentQueryFile(S)");
		return strQuery.toString();
  }


	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo){
  return "";
  }


	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo){
  return "";
  }
 

	/**
	 * @Setters_&_Getters_Methods...
	 */
	 
	public void setTabla(String tabla) {
		this.tabla = tabla;
	}


	public String getTabla() {
		return tabla;
	}


	public void setCampos(String campos) {
		this.campos = campos;
	}


	public String getCampos() {
		return campos;
	}


	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}


	public String getCondicion() {
		return condicion;
	}


	public void setOrden(String orden) {
		this.orden = orden;
	}


	public String getOrden() {
		return orden;
	}

}