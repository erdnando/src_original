package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsClausulado implements IQueryGeneratorRegExtJS  {

public ConsClausulado() {
	}
		
	private List 	conditions;
	StringBuffer 	strQuery;
		private  String ic_epo="";
		private  String ic_pyme="";
		private  String txt_usuario="";
		private  String txt_fecha_acep_de="";
		private  String txt_fecha_acep_a="";
		private  String ic_if="";			
		private String noBancoFondeo="";
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsClausulado.class);//Variable para enviar mensajes al log.
  
	private int numList = 1;
 
 public int getNumList(HttpServletRequest request){
		return this.numList;

	}
	public String getAggregateCalculationQuery() {
		return "";
 	}  
		 
	public String getDocumentQuery(){
		this.conditions = new ArrayList();
		strQuery = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		
			
		if(!"".equals(ic_epo)) {
			condicion.append(" AND con.ic_epo = ? ");
			this.conditions.add(new Integer(ic_epo));
		}
		if(!"".equals(ic_pyme)) {
			condicion.append(" AND con.ic_pyme = ? ");
			this.conditions.add(new Integer(ic_pyme));
		}
		if(!"".equals(txt_usuario)) {
			condicion.append(" AND con.ic_usuario = ? ");
			this.conditions.add(txt_usuario);
		}
		if(!"".equals(txt_fecha_acep_de)) {
			condicion.append(" AND con.df_aceptacion >= to_date(?, 'dd/mm/yyyy') ");
			this.conditions.add(txt_fecha_acep_de);
		}
		if(!"".equals(txt_fecha_acep_a)) {
			condicion.append(" AND con.df_aceptacion < to_date(?, 'dd/mm/yyyy') + 1 ");
			this.conditions.add(txt_fecha_acep_a);
		}
		if(!"".equals(ic_if)) {
			condicion.append(" AND con.ic_epo in (select ic_epo from comrel_if_epo where ic_if = ? and CS_VOBO_NAFIN = 'S') ");
			this.conditions.add(new Integer(ic_if));
		}
		
		if(!"".equals(noBancoFondeo) ){
			condicion.append(" AND epo.ic_banco_fondeo = ? ");
			this.conditions.add(new Integer(noBancoFondeo));
		}

		strQuery.append(
				" SELECT con.ic_epo,con.ic_consecutivo,con.ic_pyme" +
				" FROM com_aceptacion_contrato con, comcat_epo epo "   +
				" WHERE con.ic_epo IS NOT NULL "   +
				" 	AND con.ic_pyme IS NOT NULL "   +
				"  AND con.ic_epo = epo.ic_epo" +
				condicion);

		log.info(strQuery.toString());
		//System.out.println("getDocumentQuery:strQuery.toString(): "+strQuery.toString());
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer();
		
		strQuery.append(
			" SELECT " +
			" 	      con.ic_epo, con.ic_consecutivo, pym.cg_razon_social nompyme,"   +
			" 	      cpe.cg_pyme_epo_interno proveedor, pym.cg_razon_social nompyme,"   +
			"        epo.cg_razon_social nomepo, con.ic_usuario,"   +
			"        TO_CHAR (con.df_aceptacion, 'dd/mm/yyyy hh24:mi') || ' Hrs.' df_aceptacion, "   +
			"        'ClausuladoNafinCA::getDocumentSummaryQueryForIds()' origen, '' as DOC_CONTRATO "   +
			"   FROM com_aceptacion_contrato con,"   +
			"        comrel_pyme_epo cpe,"   +
			"        comcat_pyme pym,"   +
			"        comcat_epo epo"   +
			"  WHERE con.ic_epo = cpe.ic_epo"   +
			"    AND con.ic_pyme = cpe.ic_pyme"   +
			"    AND cpe.ic_pyme = pym.ic_pyme"   +
			"    AND cpe.ic_epo = epo.ic_epo"  +
			"    AND ( ");
			//con.ic_epo||'|'||con.ic_consecutivo||'|'||con.ic_pyme in ( " + clavesCombinaciones + " )");
			for (int i = 0; i < pageIds.size(); i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if (i > 0) {
				strQuery.append(" OR ");
			}
			strQuery.append(" ( " + 
								 "   con.ic_epo = ? AND con.ic_consecutivo = ? AND con.ic_pyme = ? " + 
								 " ) ");
			conditions.add(lItem.get(0).toString());
			conditions.add(lItem.get(1).toString());
			conditions.add(lItem.get(2).toString());
		}
			strQuery.append(" ) ");
		strQuery.append( " order by con.ic_epo, con.ic_consecutivo, con.ic_pyme ");
		
		log.info(strQuery.toString());
		log.info(conditions.toString());
		//System.out.println("getDocumentSummaryQueryForIds:strQuery.toString(): "+strQuery.toString());
		//System.out.println("Condiciones: "+conditions.toString());
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		StringBuffer strQuery = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		this.conditions = new ArrayList();
		
		//System.out.println("ic_if:::getDocumentQueryFile[[[[[[ "+ic_if);
				
		if(!"".equals(ic_epo)) {
			condicion.append(" AND con.ic_epo = ? ");
			this.conditions.add(new Integer(ic_epo));
		}
		if(!"".equals(ic_pyme)) {
			condicion.append(" AND con.ic_pyme = ? ");
			this.conditions.add(new Integer(ic_pyme));
		}
		if(!"".equals(txt_usuario)) {
			condicion.append(" AND con.ic_usuario = ? ");
			this.conditions.add(txt_usuario);
		}
		if(!"".equals(txt_fecha_acep_de)) {
			condicion.append(" AND con.df_aceptacion >= to_date(?, 'dd/mm/yyyy') ");
			this.conditions.add(txt_fecha_acep_de);
		}
		if(!"".equals(txt_fecha_acep_a)) {
			condicion.append(" AND con.df_aceptacion < to_date(?, 'dd/mm/yyyy') + 1 ");
			this.conditions.add(txt_fecha_acep_a);
		}
		if(!"".equals(ic_if)) {
			condicion.append(" AND con.ic_epo in (select ic_epo from comrel_if_epo where ic_if = ? and CS_VOBO_NAFIN = 'S') ");
			this.conditions.add(new Integer(ic_if));
		}
		
		//System.out.println("Banco de Fondeo>>>>>>>>>>" + noBancoFondeo);
		if(!"".equals(noBancoFondeo) ){
			condicion.append( "    AND con.ic_epo = epo.ic_epo " +
									"    AND epo.ic_banco_fondeo = ? "); 
			this.conditions.add(new Integer(noBancoFondeo));
		}
		strQuery.append(
			" SELECT cpe.cg_pyme_epo_interno proveedor, pym.cg_razon_social nompyme,"   +
			"        epo.cg_razon_social nomepo, con.ic_usuario,"   +
			"        TO_CHAR (con.df_aceptacion, 'dd/mm/yyyy hh24:mi') || ' Hrs.' df_aceptacion, "   +
			"        'ClausuladoNafinCA::getDocumentQueryFile()' origen "   +
			"   FROM com_aceptacion_contrato con,"   +
			"        comrel_pyme_epo cpe,"   +
			"        comcat_pyme pym,"   +
			"        comcat_epo epo"   +
			"  WHERE con.ic_epo = cpe.ic_epo"   +
			"    AND con.ic_pyme = cpe.ic_pyme"   +
			"    AND cpe.ic_pyme = pym.ic_pyme"   +
			"    AND cpe.ic_epo = epo.ic_epo" + condicion+ " order by con.ic_epo, con.ic_consecutivo, con.ic_pyme ") ;
			//"    AND con.ic_epo = epo.ic_epo " +
			//"	  AND epo.ic_banco_fondeo = 2 " +
			//condicion);
			
						
		log.info(strQuery.toString());	
		//System.out.println("getDocumentSummaryQueryForIds:strQuery.toString(): "+strQuery.toString());
		return strQuery.toString();
		
 	} 		
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		if ("XLS".equals(tipo)) {
			
			try {
			int registros = 0;
				while(rs.next()){
						if(registros==0) {
							contenidoArchivo.append(
								"Num. PyME,"+
								"Nombre PyME,"+
								"Nombre EPO,"+
								"Usuario,"+
								"Fecha/Hora");
						}
						registros++;
						String rs_num_pyme 		= rs.getString("proveedor")==null?"":rs.getString("proveedor");
						String rs_nom_pyme 		= rs.getString("nompyme")==null?"":rs.getString("nompyme");
						String rs_nom_epo 		= rs.getString("nomepo")==null?"":rs.getString("nomepo");
						String rs_usuario 		= rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario");
						String rs_fecha_acep 	= rs.getString("df_aceptacion")==null?"":rs.getString("df_aceptacion");
						contenidoArchivo.append("\n"+
							rs_num_pyme+","+
							rs_nom_pyme.replace(',',' ')+","+
							rs_nom_epo.replace(',',' ')+","+
							rs_usuario+","+
							rs_fecha_acep);
					}
					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv"))
							nombreArchivo="ERROR";
						else
							nombreArchivo = archivo.nombre;
					}
				/*nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);

				while (rs.next()) {
					String fecha = (rs.getString("FECHA") == null) ? "" : rs.getString("FECHA");
					buffer.write("Fecha: ,"+fecha+"\n");
					String cd_nombre = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
					String valor  = (rs.getString("FN_VALOR") == null) ? "" : rs.getString("FN_VALOR");
					linea = ","+cd_nombre.replace(',',' ')+","+valor.replace(',',' ')+"\n";
					
					buffer.write(linea);
				}
				buffer.close();*/
				
			} catch (Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		} 
		else if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(5, 100);
				pdfDoc.setCell("Num PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Usuario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha/Hora","celda01",ComunesPDF.CENTER);
							
				while (rs.next()) {
					String rs_num_pyme 		= rs.getString("proveedor")==null?"":rs.getString("proveedor");
						String rs_nom_pyme 		= rs.getString("nompyme")==null?"":rs.getString("nompyme");
						String rs_nom_epo 		= rs.getString("nomepo")==null?"":rs.getString("nomepo");
						String rs_usuario 		= rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario");
						String rs_fecha_acep 	= rs.getString("df_aceptacion")==null?"":rs.getString("df_aceptacion");
						
					pdfDoc.setCell(rs_num_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs_nom_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs_nom_epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs_usuario,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs_fecha_acep,"formas",ComunesPDF.CENTER);
					//pdfDoc.setCell("$"+Comunes.formatoDecimal(valor,2),"formas",ComunesPDF.RIGHT);
					
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}

		return nombreArchivo;
		
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo="";
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico") == null ? "" : session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(5, 100);
				pdfDoc.setCell("Num PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Usuario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha/Hora","celda01",ComunesPDF.CENTER);
							
				while (reg.next()) {
					String rs_num_pyme 		= reg.getString("PROVEEDOR")==null?"":reg.getString("PROVEEDOR");
						String rs_nom_pyme 		= reg.getString("NOMPYME")==null?"":reg.getString("NOMPYME");
						String rs_nom_epo 		= reg.getString("NOMEPO")==null?"":reg.getString("NOMEPO");
						String rs_usuario 		= reg.getString("IC_USUARIO")==null?"":reg.getString("IC_USUARIO");
						String rs_fecha_acep 	= reg.getString("DF_ACEPTACION")==null?"":reg.getString("DF_ACEPTACION");
						
					pdfDoc.setCell(rs_num_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs_nom_pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs_nom_epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs_usuario,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs_fecha_acep,"formas",ComunesPDF.CENTER);
					//pdfDoc.setCell("$"+Comunes.formatoDecimal(valor,2),"formas",ComunesPDF.RIGHT);
					
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					
				} catch(Exception e) {}
			}
		}

		return nombreArchivo;
	}
	public String getTama�oDoc(String IC_EPO, String IC_CONSECUTIVO) throws Exception {
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean		commit = true;
		String tama�o = "";
		try{ 
			con.conexionDB();
			String query =
				" SELECT length (bi_documento) as TAN FROM com_contrato_epo  WHERE ic_epo = "+IC_EPO+"  AND ic_consecutivo = "+ IC_CONSECUTIVO;
				log.debug(" query :: -> "+query);
				rs = con.queryDB(query);
				if (rs.next()){
					tama�o = rs.getString("TAN");
				}
		}catch(Exception e){
			commit = false;
			e.printStackTrace();  
			log.error("Error obtenIVA "+e);
		}finally{
			rs.close();
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
		}
		return tama�o;
	}

	
	public List getConditions() {
		return conditions;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}


	public void setTxt_usuario(String txt_usuario) {
		this.txt_usuario = txt_usuario;
	}


	public void setTxt_fecha_acep_de(String txt_fecha_acep_de) {
		this.txt_fecha_acep_de = txt_fecha_acep_de;
	}


	public void setTxt_fecha_acep_a(String txt_fecha_acep_a) {
		this.txt_fecha_acep_a = txt_fecha_acep_a;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public void setNoBancoFondeo(String noBancoFondeo) {
		this.noBancoFondeo = noBancoFondeo;
	}

 
}
