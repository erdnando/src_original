package com.netro.cadenas;

import com.netro.cesion.ConsultaNotificacionesCesionDerechos;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Migraci�n de la pantalla de Proveedores a ExtJS del Fodea 017 -2011.
 * Esta clase obtiene datos de los proveedores en base a
 * el No. de Cliente, sector, NafinElectronico, nombre, estado, rfc, Sin No. Proveedor - Pymes,
 * y No. de Proveedor.
 *
 * @author Ing. Alejandro Muro Z�rate
 */

public class ConsProveedores implements IQueryGeneratorRegExtJS 
{
	public ConsProveedores() {	}
	
	private List conditions;
	StringBuffer strQuery;
	//Variable para enviar mensajes al log
	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaNotificacionesCesionDerechos.class);
	
	//Variables de Sesi�n
	private String iNoCliente;
	
	//Condiciones de la consulta
	private boolean esProveedor;
	private boolean filtrarxEpo;
	private String sector;
	private String nafinElectronico;
	private String nombre;
	private String estado;
	private String rfc;
	private String pymesSinNumProv;
	private String numeroProveedor;
   
	public String getHeaderQuery(boolean tipoConsulta){
		String strQuery = "";
		
		if(tipoConsulta == true){
			strQuery = " SELECT cpyme.cg_razon_social, "+
		              "        cc.cg_appat || ' ' || cc.cg_apmat || ' ' || cc.cg_nombre as nomcontacto, "+
					     "        cd.cg_telefono1, cd.cg_email, " + 
					     "        cd.cg_calle || ' ' || cd.cg_numero_ext || ' ' ||  cd.cg_numero_int || ' ' || cd.cg_colonia || ' ' || cd.cg_municipio as direccion, "+
					     "        ce.cd_nombre as estado, cp.cd_descripcion as pais " +
						  (esProveedor ?
				         "       ,pe.cg_pyme_epo_interno as numero_proveedor "+
				         "       ,rel.ic_nafin_electronico, "+
				         "        cpyme.cg_rfc, "+
				         "        nvl(cd.cg_telefono1,'')  ||  decode(cd.cg_telefono2, null,'', decode(cd.cg_telefono1,null,'',', ') || cd.cg_telefono2 ) as telefonos, "+
				         "        s.cd_nombre as sector"
				         :""
				        );
		}
		else {
			strQuery = " SELECT count(1) as TotalRegistros ";
		}
		
		return strQuery;
	}
	
	public String getBodyQuery(){
	  String strQuery = "";
	  strQuery = "    FROM com_contacto cc, com_domicilio cd, comcat_estado ce, "+
                "         comcat_pais cp, comcat_pyme cpyme"+
				    ((filtrarxEpo || esProveedor)?" , comrel_pyme_epo pe ":"")+ 
				    ( 
				     esProveedor?
				     "        ,comrel_nafin rel, comcat_sector_econ s	"
				     :""	   
				    )+
				    "    WHERE cpyme.ic_pyme = cd.ic_pyme " +
                "      AND cpyme.ic_pyme = cc.ic_pyme  " +
                "      AND ce.ic_estado = cd.ic_estado " +
                "      AND cp.ic_pais = cd.ic_pais " +
				    ((filtrarxEpo)?" AND cpyme.ic_pyme = pe.ic_pyme AND pe.ic_epo = " + iNoCliente:"")  + "  " +
				    ((!filtrarxEpo && esProveedor)?" AND comcat_pyme.ic_pyme = pe.ic_pyme ":"")  + "  " +
				    ( 
				     esProveedor?
				     "     AND pe.ic_pyme = rel.ic_epo_pyme_if 	"  + 
				     "     AND rel.cg_tipo = 'P' 						"  + 
				     "     AND cpyme.ic_sector_econ  = s.ic_sector_econ(+) "
				     :""
				    )+
				    "      AND cc.cs_primer_contacto = 'S'" +
                "      AND cc.ic_contacto = (SELECT MAX(ic_contacto) FROM com_contacto WHERE ic_pyme = pe.ic_pyme)" +
                "      AND cd.cs_fiscal = 'S'";
					 
		return strQuery;
	}

	public String getAggregateCalculationQuery() {
		conditions	= new ArrayList();
		strQuery	= new StringBuffer();
		
		strQuery.append( getHeaderQuery(false) + getBodyQuery());

		if(!sector.equals("")){
			strQuery.append("   AND cpyme.ic_sector_econ = ? ");
			conditions.add(sector);
		}
		if(!nombre.equals("")){
			strQuery.append("   AND (cpyme.cg_nombre_comercial LIKE UPPER('%"+ nombre +"%') OR cpyme.cg_razon_social LIKE '%"+ nombre +"%' " +
                         "    OR initcap(cpyme.cg_razon_social) LIKE '%"+ nombre +"%' OR lower(cpyme.cg_razon_social) LIKE '%"+ nombre +"%' " +
                         "    OR upper(cpyme.cg_razon_social) LIKE '%"+ nombre +"%')");
		}
		
		if(esProveedor==true){
		   if(!nafinElectronico.equals("")){
				strQuery.append("   AND rel.ic_nafin_electronico = ? ");
			   conditions.add(nafinElectronico);
			}
			if(!estado.equals("")){ 
		      strQuery.append("   AND ce.ic_estado  = ? AND ce.ic_pais = 24 ");
	         conditions.add(estado);
			}
			if(!rfc.equals("")){ 
		      strQuery.append("   AND cpyme.cg_rfc =  Upper(?) ");
	         conditions.add(rfc);
			}
			if(pymesSinNumProv.equals("on")){ 
		      strQuery.append("   AND pe.cs_num_proveedor = 'S' ");
	         }
				else if(!numeroProveedor.equals("")){
				strQuery.append("   AND pe.cg_pyme_epo_interno = ? ");
				conditions.add(numeroProveedor);
				}
		}	
		   
		log.debug("getAggregateCalculationQuery)"+strQuery.toString());
		log.debug("getAggregateCalculationQuery)"+conditions);
		
		return strQuery.toString();
	}
	
	public String getDocumentQuery() {
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		
		strQuery.append( getHeaderQuery(true) + getBodyQuery());

		if(!sector.equals("")){
			strQuery.append("   AND cpyme.ic_sector_econ = ? ");
			conditions.add(sector);
		}
		if(!nombre.equals("")){
			strQuery.append("   AND (cpyme.cg_nombre_comercial LIKE UPPER('%"+ nombre +"%') OR cpyme.cg_razon_social LIKE '%"+ nombre +"%' " +
                         "    OR initcap(cpyme.cg_razon_social) LIKE '%"+ nombre +"%' OR lower(cpyme.cg_razon_social) LIKE '%"+ nombre +"%' " +
                         "    OR upper(cpyme.cg_razon_social) LIKE '%"+ nombre +"%')");
		}
		
		if(esProveedor==true){
		   if(!nafinElectronico.equals("")){
				strQuery.append("   AND rel.ic_nafin_electronico = ? ");
			   conditions.add(nafinElectronico);
			}
			if(!estado.equals("")){ 
		      strQuery.append("   AND ce.ic_estado  = ? AND ce.ic_pais = 24 ");
	         conditions.add(estado);
			}
			if(!rfc.equals("")){ 
		      strQuery.append("   AND cpyme.cg_rfc =  Upper(?) ");
	         conditions.add(rfc);
			}
			if(pymesSinNumProv.equals("on")){ 
		      strQuery.append("   AND pe.cs_num_proveedor = 'S' ");
	         }
				else if(!numeroProveedor.equals("")){
				strQuery.append("   AND pe.cg_pyme_epo_interno = ? ");
				conditions.add(numeroProveedor);
				}
		}	
		
		log.debug("getDocumentQuery)"+strQuery.toString());
		log.debug("getDocumentQuery)"+conditions);
		
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		
		strQuery.append( getHeaderQuery(true) + getBodyQuery());

		if(!sector.equals("")){
			strQuery.append("   AND cpyme.ic_sector_econ = ? ");
			conditions.add(sector);
		}
		if(!nombre.equals("")){
			strQuery.append("   AND (cpyme.cg_nombre_comercial LIKE UPPER('%"+ nombre +"%') OR cpyme.cg_razon_social LIKE '%"+ nombre +"%' " +
                         "    OR initcap(cpyme.cg_razon_social) LIKE '%"+ nombre +"%' OR lower(cpyme.cg_razon_social) LIKE '%"+ nombre +"%' " +
                         "    OR upper(cpyme.cg_razon_social) LIKE '%"+ nombre +"%')");
		}
		
		if(esProveedor==true){
		   if(!nafinElectronico.equals("")){
				strQuery.append("   AND rel.ic_nafin_electronico = ? ");
			   conditions.add(nafinElectronico);
			}
			if(!estado.equals("")){ 
		      strQuery.append("   AND ce.ic_estado  = ? AND ce.ic_pais = 24 ");
	         conditions.add(estado);
			}
			if(!rfc.equals("")){ 
		      strQuery.append("   AND cpyme.cg_rfc =  Upper(?) ");
	         conditions.add(rfc);
			}
			if(pymesSinNumProv.equals("on")){ 
		      strQuery.append("   AND pe.cs_num_proveedor = 'S' ");
	         }
				else if(!numeroProveedor.equals("")){
				strQuery.append("   AND pe.cg_pyme_epo_interno = ? ");
				conditions.add(numeroProveedor);
				}
		}
		
		for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					strQuery.append(" AND rel.ic_nafin_electronico IN ( ");
				}
				strQuery.append("?");
				conditions.add(new Long(lItem.get(8).toString()));					
				if(i!=(pageIds.size()-1)) {
					strQuery.append(",");
				} else {
					strQuery.append(" ) ");
				}			
			}
			
      log.debug("---------------------------------------------- ");
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		log.debug("---------------------------------------------- ");
		
		return strQuery.toString();
	}
	
	public String getDocumentQueryFile() {
	   conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		
		strQuery.append( getHeaderQuery(true) + getBodyQuery());

		if(!sector.equals("")){
			strQuery.append("   AND cpyme.ic_sector_econ = ? ");
			conditions.add(sector);
		}
		if(!nombre.equals("")){
			strQuery.append("   AND (cpyme.cg_nombre_comercial LIKE UPPER('%"+ nombre +"%') OR cpyme.cg_razon_social LIKE '%"+ nombre +"%' " +
                         "    OR initcap(cpyme.cg_razon_social) LIKE '%"+ nombre +"%' OR lower(cpyme.cg_razon_social) LIKE '%"+ nombre +"%' " +
                         "    OR upper(cpyme.cg_razon_social) LIKE '%"+ nombre +"%')");
		}
		
		if(esProveedor==true){
		   if(!nafinElectronico.equals("")){
				strQuery.append("   AND rel.ic_nafin_electronico = ? ");
			   conditions.add(nafinElectronico);
			}
			if(!estado.equals("")){ 
		      strQuery.append("   AND ce.ic_estado  = ? AND ce.ic_pais = 24 ");
	         conditions.add(estado);
			}
			if(!rfc.equals("")){ 
		      strQuery.append("   AND cpyme.cg_rfc =  Upper(?) ");
	         conditions.add(rfc);
			}
			if(pymesSinNumProv.equals("on")){ 
		      strQuery.append("   AND pe.cs_num_proveedor = 'S' ");
	         }
				else if(!numeroProveedor.equals("")){
				strQuery.append("   AND pe.cg_pyme_epo_interno = ? ");
				conditions.add(numeroProveedor);
				}
		}	
		   
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		
		return strQuery.toString();
	}
	
	
	/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)
	{
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		try {
			if(tipo.equals("CSV")) {
			 linea = "No Proveedor, N�mero de Nafin Electr�nico, RFC, Nombre de la Empresa, Contacto, Tel�fono, Correo Electr�nico, Direcci�n, Estado, Tel�fonos, Sector\n";
			 nombreArchivo = "PROVEEDORES_DETALLE_tmp" + Comunes.cadenaAleatoria(10) + ".csv";
			 writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			 buffer = new BufferedWriter(writer);
			 buffer.write(linea);
				
			 while (rs.next()) {
			  String numeroProveedor	=	(rs.getString("NUMERO_PROVEEDOR") == null) ? "" : rs.getString("NUMERO_PROVEEDOR");
			  String numeroNafinElectronico = (rs.getString("IC_NAFIN_ELECTRONICO") == null) ? "" : rs.getString("IC_NAFIN_ELECTRONICO");
			  String rfc	=	(rs.getString("CG_RFC") == null) ? "" : rs.getString("CG_RFC");
			  String nombreEmpresa = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
			  String contacto	=	(rs.getString("NOMCONTACTO") == null) ? "" : rs.getString("NOMCONTACTO");
			  String telefono = (rs.getString("CG_TELEFONO1") == null) ? "" : rs.getString("CG_TELEFONO1");
			  String email	=	(rs.getString("CG_EMAIL") == null) ? "" : rs.getString("CG_EMAIL");
			  String direccion = (rs.getString("DIRECCION") == null) ? "" : rs.getString("DIRECCION");
			  String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
			  String telefonos	=	(rs.getString("TELEFONOS") == null) ? "" : rs.getString("TELEFONOS");
			  String sector = (rs.getString("SECTOR") == null) ? "" : rs.getString("SECTOR");
			  
			  linea = numeroProveedor.replace(',',' ') + ", " + 
			          numeroNafinElectronico.replace(',',' ') + ", " +
						 rfc.replace(',',' ') + ", " +
						 nombreEmpresa.replace(',',' ') + ", " +
						 contacto.replace(',',' ') + ", " +
						 telefono.replace(',',' ') + ", " +
						 email.replace(',',' ') + ", " +
						 direccion.replace(',',' ') + ", " +
						 estado.replace(',',' ') + ", " +
						 telefonos.replace(',',' ') + ", " +
						 sector.replace(',',' ') + "\n";				 
			  buffer.write(linea);
			 }
			 buffer.close();
			}
		}
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
	}
	
	
   /** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}
	
	public void setEsProveedor(boolean esProveedor) {
		this.esProveedor = esProveedor;
	}
	public boolean getEsProveedor() {
	   return esProveedor;
	}
	
	public void setFiltrarxEpo(boolean filtrarxEpo) {
		this.filtrarxEpo = filtrarxEpo;
	}
	
	public boolean getFiltrarxEpo() {
	   return filtrarxEpo;
	}

	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}

	public String getINoCliente() {
		return iNoCliente;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getSector() {
		return sector;
	}

	public void setNafinElectronico(String nafinElectronico) {
		this.nafinElectronico = nafinElectronico;
	}

	public String getNafinElectronico() {
		return nafinElectronico;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}
	
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEstado() {
		return estado;
	}
	
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getRfc() {
		return rfc;
	}

	public void setPymesSinNumProv(String pymesSinNumProv) {
		this.pymesSinNumProv = pymesSinNumProv;
	}

	public String getPymesSinNumProv() {
		return pymesSinNumProv;
	}

	public void setNumeroProveedor(String numeroProveedor) {
		this.numeroProveedor = numeroProveedor;
	}

	public String getNumeroProveedor() {
		return numeroProveedor;
	}
}