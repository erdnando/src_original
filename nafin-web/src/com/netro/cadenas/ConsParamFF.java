package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsParamFF implements  IQueryGeneratorRegExtJS {
	private static final Log log = ServiceLocator.getInstance().getLog(ConsParamFF.class);//Variable para enviar mensajes al log.
	public ConsParamFF() {  }
	private List 	conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	private String 	tipoConsulta;
	private String claveEpo;
	private String claveIf;
	private String viaLiquidacion;
	private String tipoDispercion;
	StringBuffer 	strQuery;
	StringBuffer qrySentencia = new StringBuffer("");
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
 	}  
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		log.info("getDocumentQueryFile(E)");
		qrySentencia = new StringBuffer();
		if(tipoConsulta.equals("consultaInicial")){
		qrySentencia.append(
			" SELECT ic_param_ff," +
			" '' as dist_epo," +
			" '' as dist_if," +
			" DECODE(cg_tipo, 'G', 'S', 'N') as GENERICA, " +
			" DECODE(cg_tipo, 'E', 'S', 'N') as ESPECIFICA, " +
			" DECODE(cg_tipo, 'G', 'checked', '') as AUX_GENERICA, " +
			" DECODE(cg_tipo, 'E', 'checked', '') as AUX_ESPECIFICA," +
			" DECODE(cg_via_liquidacion, 'T', 'S', 'N') as TEF," +
			" DECODE(cg_via_liquidacion, 'A', 'S', 'N') as AMBAS,  " +
			" DECODE(cg_via_liquidacion, 'S', 'S', 'N') as SPEI," +
			" DECODE(cg_via_liquidacion, 'T', 'checked', '') as AUX_TEF, " +
			" DECODE(cg_via_liquidacion, 'A', 'checked', '') as AUX_AMBAS, " +
			" DECODE(cg_via_liquidacion, 'S', 'checked', '') as AUX_SPEI, " +
			" cd_descripcion, " +
			" cg_tipo, " +
			" cg_via_liquidacion, " +
			" ic_producto_nafin  " +
			" FROM com_param_ff " +
			" WHERE ic_param_ff IN (1, 2, 3)  " +
			" ORDER BY 1 " 
			); 
		}else if(tipoConsulta.equals("ConsultarDetalle")||tipoConsulta.equals("ArchivoPDF")){
			
			if("IF".equals(tipoDispercion)  ){
				qrySentencia.append( "SELECT cif.cg_razon_social as razon_social ,cif.ic_if, epo.cg_razon_social,epo.ic_epo, cg_via_liquidacion"   +
					"  FROM comrel_if_epo_x_producto iep, comcat_epo epo, comcat_if cif " +
					"  WHERE iep.ic_epo = epo.ic_epo" +
					" 	 AND iep.ic_producto_nafin = 1" +
					"    AND iep.ic_if = cif.ic_if");
				if(!"".equals(claveIf))
				qrySentencia.append( "    AND iep.ic_if = "+claveIf ); 
			}else {
				qrySentencia.append( "SELECT epo.cg_razon_social,epo.ic_epo, cg_via_liquidacion"   +
					"  FROM comrel_producto_epo iep, comcat_epo epo " +
					"  WHERE iep.ic_epo = epo.ic_epo" +
					" 	 AND iep.ic_producto_nafin = 1");
			}
			if(!"".equals(claveEpo))
				qrySentencia.append( "	 AND iep.ic_epo = "+claveEpo);   
	
			if(!"".equals(viaLiquidacion))
				qrySentencia.append( "    AND cg_via_liquidacion = '" + viaLiquidacion + "'");
			else
				qrySentencia.append( "    AND cg_via_liquidacion IS NOT NULL");
		}
		log.debug("getDocumentQueryFile(S) "); 
		log.debug("qrySentencia "+qrySentencia);
		return qrySentencia.toString();		
		
 	} 
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.debug("::crearCustomFile(E)   "+tipo);
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		int numeroRegistros = 0;
		double totalAdeudo = 0.0;
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
			if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				if("IF".equals(tipoDispercion)  ){
					pdfDoc.setTable(3, 80);
					pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
				}else{
					pdfDoc.setTable(2, 80);
				}
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Vias de Liquidaci�n","celda01",ComunesPDF.CENTER);
				while (rs.next()) {
					String razon_socialIF = "";
					if("IF".equals(tipoDispercion)  ){
						razon_socialIF = rs.getString("RAZON_SOCIAL")==null?"":rs.getString("RAZON_SOCIAL");
					}
					String razon_socialEPO = rs.getString("CG_RAZON_SOCIAL")==null?"":rs.getString("CG_RAZON_SOCIAL");
					String via_liquidacion = rs.getString("CG_VIA_LIQUIDACION")==null?"":rs.getString("CG_VIA_LIQUIDACION");
					if(via_liquidacion.equals("A")){
						via_liquidacion = "Ambas";
					}else if(via_liquidacion.equals("T")){
						via_liquidacion = "TEF";
					}else if(via_liquidacion.equals("S")){
						via_liquidacion = "SPEI";
					}
					if("IF".equals(tipoDispercion)  ){
						pdfDoc.setCell(razon_socialIF,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setCell(razon_socialEPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(via_liquidacion,"formas",ComunesPDF.CENTER);
					
				}
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				}
		}
		log.info(":: crearCustomFile(S)");
		return nombreArchivo;					
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		return "";   
	}
	public List getConditions() {
		return conditions;
	}
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
	
	public int refVerificaIf()throws Exception{
		log.info("refVerificaIf (E)");   
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;  
		boolean		commit = true;
		strQuery 		= new StringBuffer();  
		int regverif = 0;
		try{
			con.conexionDB();
			strQuery.append(
				" SELECT count(1)"   +
				" FROM comrel_if_epo_x_producto "   +
				" WHERE ic_producto_nafin = 1 "   +	
				" and cg_via_liquidacion is not null"
					);
			ps = con.queryPrecompilado(strQuery.toString());
			rs = ps.executeQuery();
			if(rs.next())
				regverif = rs.getInt(1);
				
			rs.close();
			ps.close();	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error catMoneda "+e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("catMoneda (S)");
		}	
		return regverif;
 	}
	
	
	public int refVerificaEpo()throws Exception{
		log.info("refVerificaEpo (E)");   
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;  
		boolean		commit = true;
		strQuery 		= new StringBuffer();
		int regverepo = 0;
		try{
			con.conexionDB();
			strQuery.append(
				" SELECT count(1)"   +
				" FROM comrel_producto_epo "   +
				" WHERE ic_producto_nafin = 1 "   +	
				" and cg_via_liquidacion is not null"
					);
			ps = con.queryPrecompilado(strQuery.toString());
			rs = ps.executeQuery();
			if(rs.next())
				regverepo = rs.getInt(1);
				
			rs.close();
			ps.close();	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error catMoneda "+e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("catMoneda (S)");
		}	
		return regverepo;
 	}
	 public String getTipoConsulta() {
	   return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
	  this.tipoConsulta = tipoConsulta;
	}
	public String getClaveIf() {
	  return claveIf;
	}
   public void setClaveIf(String claveIf) {
     this.claveIf = claveIf;
  }
   public String getClaveEpo() {
      return claveEpo;
   }
   public void setClaveEpo(String claveEpo) {
     this.claveEpo = claveEpo;
	}
	public String getViaLiquidacion() {
	  return viaLiquidacion;
	}

	public void setViaLiquidacion(String viaLiquidacion) {
	    this.viaLiquidacion = viaLiquidacion;
	}
	  
	public String getTipoDispercion() {
	   return tipoDispercion;
	}
	public void setTipoDispercion(String tipoDispercion) {
		this.tipoDispercion = tipoDispercion;
	}
	
}