package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaContratos implements IQueryGeneratorRegExtJS  {
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaContratos.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	//PARAMETROS 
	private String codigoCliente;
	private String proyectoCadena;//esta atributo se descompone en 
	//---------------
		private String agencia;
		private String subaplicacion;
		private String proyecto;
	//--------------------	
	private String numeroContrato;
	
	public ConsultaContratos() {
	}

		public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(" SELECT 	\n"+
									"	 LC.NUMERO_LINEA_CREDITO	\n"+
									"FROM 	\n"+
									"	 LC_PROYECTO P, 	\n"+
									"	 PR_CONTRATOS C, 	\n"+
									"	 MG_FINANCIERAS F, 	\n"+
									"	 LC_LINEAS_CREDITOS LC	\n"+
									"WHERE 	\n"+
									"	 P.CODIGO_FINANCIERA = F.CODIGO_FINANCIERA	\n"+
									"	 AND P.TIPO_FINANCIERA = F.TIPO_FINANCIERA	\n"+
									"	 AND P.NUMERO_LINEA_CREDITO = C.CODIGO_LINEA_CREDITO	\n"+
									"	 AND P.CODIGO_EMPRESA = C.CODIGO_EMPRESA_LC	\n"+
									"	 AND P.CODIGO_AGENCIA = C.CODIGO_AGENCIA_LC	\n"+
									"	 AND P.CODIGO_SUB_APLICACION = C.CODIGO_SUB_APLICACION_LC	\n"+
									"	 AND P.CODIGO_APLICACION = 'BLC'	\n"+
									"	 AND P.NUMERO_LINEA_CREDITO = LC.NUMERO_LINEA_CREDITO	\n"+
									"	 AND P.CODIGO_EMPRESA = LC.CODIGO_EMPRESA	\n"+
									"	 AND P.CODIGO_AGENCIA = LC.CODIGO_AGENCIA	\n"+
									"	 AND P.CODIGO_SUB_APLICACION = LC.CODIGO_SUB_APLICACION	\n"+
									"	 AND P.CODIGO_APLICACION = LC.CODIGO_APLICACION	\n");//+
									
									
				if(!"".equals(codigoCliente)){
					qrySentencia.append("	 AND LC.CODIGO_CLIENTE =  ? \n");
					conditions.add(codigoCliente);				
				}
				
									
				if(!proyectoCadena.equals("")){
					Vector vectorProyecto = Comunes.explode("-",proyectoCadena);
					agencia = vectorProyecto.get(0).toString();
					qrySentencia.append(" AND		c.codigo_agencia = ? \n");
					conditions.add(agencia);
					
					subaplicacion = vectorProyecto.get(1).toString();
					qrySentencia.append(" AND		c.codigo_sub_aplicacion = ? \n");
					conditions.add(subaplicacion);
					
					proyecto = vectorProyecto.get(2).toString();				
					qrySentencia.append(" AND		c.codigo_linea_credito = ? \n");
					conditions.add(proyecto);
				}	
					
				if(!numeroContrato.equals("")){
					qrySentencia.append(" AND		c.numero_contrato = ? \n");
					conditions.add(numeroContrato);
				}
			//qrySentencia.append("ORDER BY ic_moneda, ig_cliente, ig_prestamo	");

		log.info("getDocumentQuery_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}

	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
				qrySentencia.append("SELECT	\n"+ 
											"	 COUNT(1) AS TOTAL_CONTRATOS,	\n"+ 
											"	 SUM(C.MONTO_APROBADO) AS MONTO_APROBADO,	\n"+ 
											"	 SUM(C.MONTO_DISPONIBLE) AS MONTO_DISPONIBLE	\n"+
											"FROM	\n"+ 
											"	 LC_PROYECTO P,	\n"+ 
											"	 PR_CONTRATOS C,	\n"+ 
											"	 MG_FINANCIERAS F,	\n"+ 
											"	 LC_LINEAS_CREDITOS LC	\n"+
											"WHERE	\n"+ 
											"	 P.CODIGO_FINANCIERA = F.CODIGO_FINANCIERA	\n"+
											"	 AND P.TIPO_FINANCIERA = F.TIPO_FINANCIERA	\n"+
											"	 AND P.NUMERO_LINEA_CREDITO = C.CODIGO_LINEA_CREDITO	\n"+
											"	 AND P.CODIGO_EMPRESA = C.CODIGO_EMPRESA_LC	\n"+
											"	 AND P.CODIGO_AGENCIA = C.CODIGO_AGENCIA_LC	\n"+
											"	 AND P.CODIGO_SUB_APLICACION = C.CODIGO_SUB_APLICACION_LC	\n"+
											"	 AND P.CODIGO_APLICACION = 'BLC'	\n"+
											"	 AND P.NUMERO_LINEA_CREDITO = LC.NUMERO_LINEA_CREDITO	\n"+
											"	 AND P.CODIGO_EMPRESA = LC.CODIGO_EMPRESA	\n"+
											"	 AND P.CODIGO_AGENCIA = LC.CODIGO_AGENCIA	\n"+
											"	 AND P.CODIGO_SUB_APLICACION = LC.CODIGO_SUB_APLICACION	\n"+
											"	 AND P.CODIGO_APLICACION = LC.CODIGO_APLICACION	\n");//+
											//"	 AND LC.CODIGO_CLIENTE = ? \n");
					
				if(!"".equals(codigoCliente)){
					qrySentencia.append("	 AND LC.CODIGO_CLIENTE =  ? \n");
					conditions.add(codigoCliente);				
				}
									
				if(!proyectoCadena.equals("")){
					Vector vectorProyecto = Comunes.explode("-",proyectoCadena);
					agencia = vectorProyecto.get(0).toString();
					qrySentencia.append(" AND		c.codigo_agencia = ? \n");
					conditions.add(agencia);
					
					subaplicacion = vectorProyecto.get(1).toString();
					qrySentencia.append(" AND		c.codigo_sub_aplicacion = ? \n");
					conditions.add(subaplicacion);
					
					proyecto = vectorProyecto.get(2).toString();				
					qrySentencia.append(" AND		c.codigo_linea_credito = ? \n");
					conditions.add(proyecto);
				}	
					
				if(!numeroContrato.equals("")){
					qrySentencia.append(" AND		c.numero_contrato = ? \n");
					conditions.add(numeroContrato);
				}
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
		public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
					qrySentencia.append("SELECT	\n"+ 
												"	 C.CODIGO_AGENCIA, 	\n"+
												"	 C.CODIGO_SUB_APLICACION,	\n"+ 
												"	 C.CODIGO_LINEA_CREDITO,	\n"+
												"	 C.CODIGO_AGENCIA||'-'||C.CODIGO_SUB_APLICACION||'-'|| C.CODIGO_LINEA_CREDITO AS PROYECTO,\n"+
												"	 C.NUMERO_CONTRATO, 	\n"+
												"	 P.CODIGO_FINANCIERA, 	\n"+
												"	 F.NOMBRE,	\n"+
												"	 TO_CHAR (C.FECHA_APERTURA, 'YYYY/MM/DD') AS FECHA_APERTURA,	\n"+
												"	 C.CODIGO_LINEA_FINANCIERA, 	\n"+
												"	 C.MONTO_APROBADO, 	\n"+
												"	 C.MONTO_DISPONIBLE,	\n"+
												"	 C.ESTADO_CONTRATO	\n"+
												"FROM 	\n"+
												"	 LC_PROYECTO P, 	\n"+
												"	 PR_CONTRATOS C, 	\n"+
												"	 MG_FINANCIERAS F, 	\n"+
												"	 LC_LINEAS_CREDITOS LC	\n"+
												"WHERE 	\n"+
												"	 P.CODIGO_FINANCIERA = F.CODIGO_FINANCIERA	\n"+
												"	 AND P.TIPO_FINANCIERA = F.TIPO_FINANCIERA	\n"+
												"	 AND P.NUMERO_LINEA_CREDITO = C.CODIGO_LINEA_CREDITO	\n"+
												"	 AND P.CODIGO_EMPRESA = C.CODIGO_EMPRESA_LC	\n"+
												"	 AND P.CODIGO_AGENCIA = C.CODIGO_AGENCIA_LC	\n"+
												"	 AND P.CODIGO_SUB_APLICACION = C.CODIGO_SUB_APLICACION_LC	\n"+
												"	 AND P.CODIGO_APLICACION = 'BLC'	\n"+
												"	 AND P.NUMERO_LINEA_CREDITO = LC.NUMERO_LINEA_CREDITO	\n"+
												"	 AND P.CODIGO_EMPRESA = LC.CODIGO_EMPRESA	\n"+
												"	 AND P.CODIGO_AGENCIA = LC.CODIGO_AGENCIA	\n"+
												"	 AND P.CODIGO_SUB_APLICACION = LC.CODIGO_SUB_APLICACION	\n"+
												"	 AND P.CODIGO_APLICACION = LC.CODIGO_APLICACION	\n"+
												"	 AND P.NUMERO_LINEA_CREDITO IN(");
	 
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0){ qrySentencia.append(","); }
			qrySentencia.append("?");
			conditions.add(lItem.get(0));
			
		}
		qrySentencia.append(") ");//possiblemente un order by
		log.info("getDocumentSummaryQueryForIds_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
		qrySentencia.append("SELECT 	\n"+
									"	 C.CODIGO_AGENCIA, 	\n"+
									"	 C.CODIGO_SUB_APLICACION, 	\n"+
									"	 C.CODIGO_LINEA_CREDITO,	\n"+
									"	 C.NUMERO_CONTRATO, 	\n"+
									"	 C.CODIGO_AGENCIA||'-'||C.CODIGO_SUB_APLICACION||'-'|| C.CODIGO_LINEA_CREDITO AS PROYECTO,\n"+
									"	 P.CODIGO_FINANCIERA, 	\n"+
									"	 F.NOMBRE,	\n"+
									"	 TO_CHAR (C.FECHA_APERTURA, 'YYYY/MM/DD') AS FECHA_APERTURA,	\n"+
									"	 C.CODIGO_LINEA_FINANCIERA, 	\n"+
									"	 C.MONTO_APROBADO, 	\n"+
									"	 C.MONTO_DISPONIBLE,	\n"+
									"	 C.ESTADO_CONTRATO	\n"+
									"FROM 	\n"+
									"	 LC_PROYECTO P, 	\n"+
									"	 PR_CONTRATOS C, 	\n"+
									"	 MG_FINANCIERAS F, 	\n"+
									"	 LC_LINEAS_CREDITOS LC	\n"+
									"WHERE 	\n"+
									"	 P.CODIGO_FINANCIERA = F.CODIGO_FINANCIERA	\n"+
									"	 AND P.TIPO_FINANCIERA = F.TIPO_FINANCIERA	\n"+
									"	 AND P.NUMERO_LINEA_CREDITO = C.CODIGO_LINEA_CREDITO	\n"+
									"	 AND P.CODIGO_EMPRESA = C.CODIGO_EMPRESA_LC	\n"+
									"	 AND P.CODIGO_AGENCIA = C.CODIGO_AGENCIA_LC	\n"+
									"	 AND P.CODIGO_SUB_APLICACION = C.CODIGO_SUB_APLICACION_LC	\n"+
									"	 AND P.CODIGO_APLICACION = 'BLC'	\n"+
									"	 AND P.NUMERO_LINEA_CREDITO = LC.NUMERO_LINEA_CREDITO	\n"+
									"	 AND P.CODIGO_EMPRESA = LC.CODIGO_EMPRESA	\n"+
									"	 AND P.CODIGO_AGENCIA = LC.CODIGO_AGENCIA	\n"+
									"	 AND P.CODIGO_SUB_APLICACION = LC.CODIGO_SUB_APLICACION	\n"+
									"	 AND P.CODIGO_APLICACION = LC.CODIGO_APLICACION	\n"+
									"	 AND LC.CODIGO_CLIENTE = ?	\n" );
				conditions.add(codigoCliente);				
									
				if(!proyectoCadena.equals("")){
					Vector vectorProyecto = Comunes.explode("-",proyectoCadena);
					agencia = vectorProyecto.get(0).toString();
					qrySentencia.append(" AND		c.codigo_agencia = ? \n");
					conditions.add(agencia);
					
					subaplicacion = vectorProyecto.get(1).toString();
					qrySentencia.append(" AND		c.codigo_sub_aplicacion = ? \n");
					conditions.add(subaplicacion);
					
					proyecto = vectorProyecto.get(2).toString();				
					qrySentencia.append(" AND		c.codigo_linea_credito = ? \n");
					conditions.add(proyecto);
				}	
					
				if(!numeroContrato.equals("")){
					qrySentencia.append(" AND		c.numero_contrato = ? \n");
					conditions.add(numeroContrato);
				}	
		log.info("getDocumentQueryFile_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
			pdfDoc.setTable(8,80);
			pdfDoc.setCell("Proyecto","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Num. Contrato","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Intermediario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Apertura","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("L�nea Financiera","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Aprobado ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Disponible","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estado","celda01",ComunesPDF.CENTER);
			
			BigDecimal totalMontoAprovado= new BigDecimal("0");
			BigDecimal totalMontoDisponible= new BigDecimal("0");
			//rs.getNumeroRegistros();
			while (rs.next())	{	
				String codigoAgencia=(rs.getString("CODIGO_AGENCIA") == null) ? "" : rs.getString("CODIGO_AGENCIA");
				String codigoSubAplicacion=(rs.getString("CODIGO_SUB_APLICACION") == null) ? "" : rs.getString("CODIGO_SUB_APLICACION");
				String codigoLineaCredito =(rs.getString("CODIGO_LINEA_CREDITO") == null) ? "" : rs.getString("CODIGO_LINEA_CREDITO");
				
				String proyecto=(rs.getString("PROYECTO") == null) ? "" : rs.getString("PROYECTO");
				String contrato = (rs.getString("NUMERO_CONTRATO") == null) ? "" : rs.getString("NUMERO_CONTRATO");
				String intermediario = (rs.getString("NOMBRE") == null) ? "" : rs.getString("NOMBRE");
				String fechaApertura = (rs.getString("FECHA_APERTURA") == null) ? "" : rs.getString("FECHA_APERTURA");
				String lineaFinanciera = (rs.getString("CODIGO_LINEA_FINANCIERA") == null) ? "" : rs.getString("CODIGO_LINEA_FINANCIERA");
				String montoAprobado = (rs.getString("MONTO_APROBADO") == null) ? "" : rs.getString("MONTO_APROBADO");
				String montoDisponible = (rs.getString("MONTO_DISPONIBLE") == null) ? "" : rs.getString("MONTO_DISPONIBLE");
				String estado = (rs.getString("ESTADO_CONTRATO") == null) ? "" : rs.getString("ESTADO_CONTRATO");
				totalMontoAprovado=totalMontoAprovado.add(new BigDecimal(montoAprobado));
				totalMontoDisponible=totalMontoDisponible.add(new BigDecimal(montoDisponible));
				pdfDoc.setCell(proyecto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(contrato,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(intermediario,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(fechaApertura,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(lineaFinanciera,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoAprobado,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDisponible,2),"formas",ComunesPDF.RIGHT);
				
				pdfDoc.setCell(estado,"formas",ComunesPDF.CENTER);
			}
			/*
			pdfDoc.setCell("Monto Total","celda01",ComunesPDF.LEFT,2);
			pdfDoc.setCell("","formas",ComunesPDF.LEFT,3);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoAprovado.toString(),2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoDisponible.toString(),2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell("","formas",ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Total Contratos","celda01",ComunesPDF.LEFT,2);
			pdfDoc.setCell(""+rs.getNumeroRegistros(),"formas",ComunesPDF.LEFT,6);
			*/
			pdfDoc.addTable();
			pdfDoc.endDocument();	
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
		public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();	
			contenidoArchivo.append("Proyecto,	Num. Contrato,		Intermediario,	Fecha de Apertura,L�nea Financiera,Monto Aprobado,	Monto Disponible,	Estado\n\r");

			
		while (rs.next())	{					
			String codigoAgencia=(rs.getString("CODIGO_AGENCIA") == null) ? "" : rs.getString("CODIGO_AGENCIA");
				String codigoSubAplicacion=(rs.getString("CODIGO_SUB_APLICACION") == null) ? "" : rs.getString("CODIGO_SUB_APLICACION");
				String codigoLineaCredito =(rs.getString("CODIGO_LINEA_CREDITO") == null) ? "" : rs.getString("CODIGO_LINEA_CREDITO");
				
				String proyecto=(rs.getString("PROYECTO") == null) ? "" : rs.getString("PROYECTO");
				String contrato = (rs.getString("NUMERO_CONTRATO") == null) ? "" : rs.getString("NUMERO_CONTRATO");
				String intermediario = (rs.getString("NOMBRE") == null) ? "" : rs.getString("NOMBRE");
				String fechaApertura = (rs.getString("FECHA_APERTURA") == null) ? "" : rs.getString("FECHA_APERTURA");
				String lineaFinanciera = (rs.getString("CODIGO_LINEA_FINANCIERA") == null) ? "" : rs.getString("CODIGO_LINEA_FINANCIERA");
				String montoAprobado = (rs.getString("MONTO_APROBADO") == null) ? "" : rs.getString("MONTO_APROBADO");
				String montoDisponible = (rs.getString("MONTO_DISPONIBLE") == null) ? "" : rs.getString("MONTO_DISPONIBLE");
				String estado = (rs.getString("ESTADO_CONTRATO") == null) ? "" : rs.getString("ESTADO_CONTRATO");
				
				
				
				contenidoArchivo.append(
				proyecto.replace(',',' ')+",	"+
				contrato.replace(',',' ')+",	"+
				intermediario.replace(',',' ')+",	"+
				fechaApertura.replace(',',' ')+",	"+
				lineaFinanciera.replace(',',' ')+",		"+
				"$"+Comunes.formatoDecimal(montoAprobado,2).replace(',',' ')+",	"+
				"$"+Comunes.formatoDecimal(montoDisponible,2).replace(',',' ')+","+
				estado.replace(',',' ')+"\n");	
				
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio   
				
		//creaArchivo.make(contenidoArchivo.toString(), path, ".cvs");
		//nombreArchivo = creaArchivo.getNombre();
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
  /**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}


	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}


	public String getCodigoCliente() {
		return codigoCliente;
	}


	public void setProyectoCadena(String proyectoCadena) {
		this.proyectoCadena = proyectoCadena;
	}


	public String getProyectoCadena() {
		return proyectoCadena;
	}


	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}


	public String getAgencia() {
		return agencia;
	}


	public void setSubaplicacion(String subaplicacion) {
		this.subaplicacion = subaplicacion;
	}


	public String getSubaplicacion() {
		return subaplicacion;
	}


	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}


	public String getProyecto() {
		return proyecto;
	}


	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}


	public String getNumeroContrato() {
		return numeroContrato;
	}
	
	//SETTERS Y GETTERS
	
	
}