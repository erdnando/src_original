package com.netro.cadenas;
import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;
import java.sql.*;

public class ConsCatalogoClasificacionEpo  {

	private static final Log log = ServiceLocator.getInstance().getLog(ConsCatalogoClasificacionEpo.class);//Variable para enviar mensajes al log.
	//ATRIBUTOS
	private List 		conditions;
	private String CveClasifEpo;
	private String CveEpo;
	private String cboCatalogo;
	private String ic_class_epo;
	private String ic_area;
	private String id_responsable;
	private String claveCadena;
	private String operacion;
	private String modifica;
	private String accion;
	private String iNoCliente;
	private String mess;
	private String mensaje;
	
	
	private String cveCat ="";
	private String RegSig = "";
	private String Totalreg;
	boolean bandera = true;
	
	StringBuffer qrySentencia = new StringBuffer("");	
	
	public ConsCatalogoClasificacionEpo() {
	}
	public Registros getConsultaData( ){
		log.info("getConsultaData(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			StringBuffer condicion  = new StringBuffer("");
			AccesoDB con = new AccesoDB(); 
			Registros registros  = new Registros();
			try{
				con.conexionDB();
				qrySentencia.append(	"	SELECT ic_clasificacion_epo, ic_epo, cg_area, cg_responsable FROM cdercat_clasificacion_epo where ic_epo = ? ");
	
				conditions.add(claveCadena);
				registros = con.consultarDB(qrySentencia.toString(),conditions);
				con.cierraConexionDB();
			} catch (Exception e) {
				log.error("getConsultaData  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			} 	
		
			log.debug("qrySentencia getConsultaData: mm "+qrySentencia.toString());
			log.debug("conditions "+conditions);
			
			log.info("getConsultaData (S) ");
			return registros;
	}	
	public Registros getDataCadenas(){
		log.info("getDataCadenas(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			StringBuffer condicion  = new StringBuffer("");
			AccesoDB con = new AccesoDB(); 
			Registros registros  = new Registros();
			try{
				con.conexionDB();
				qrySentencia.append(	"SELECT DISTINCT CE.ic_epo AS CLAVE, CE.cg_razon_social AS DESCRIPCION  FROM  COMCAT_EPO CE  WHERE 1 = 1  AND CE.cs_habilitado = 'S'  AND ce.cs_cesion_derechos = 'S' ORDER BY CE.CG_RAZON_SOCIAL ");
	
				registros = con.consultarDB(qrySentencia.toString());// con.consultarDB(qrySentencia.toString());
				con.cierraConexionDB();
			} catch (Exception e) {
				log.error("getConsultaData  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			} 	
		
			log.info("qrySentencia getDataCadenas: "+qrySentencia.toString());
			
			
			log.info("getDataCadenas (S) ");
			return registros;
	}	
	public String getClasificacionEpo(String claveEpoCesionCatalogo){
		log.info("getClasificacionEpo(E)");
			StringBuffer qrySentencia = new StringBuffer();
			conditions 		= new ArrayList();
			AccesoDB con = new AccesoDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			String resultado  = "";
			List varBind = new ArrayList();
			try{
				con.conexionDB();
				qrySentencia.append( "SELECT cg_clasificacion_epo FROM comcat_epo where ic_epo = ?");
				varBind = new ArrayList();
				varBind.add(claveEpoCesionCatalogo);
					
				ps = con.queryPrecompilado(qrySentencia.toString(),varBind );
		      rs = ps.executeQuery();
				log.debug("varBind "+varBind);
				if (rs.next()){
					resultado = rs.getString("cg_clasificacion_epo")==null?"":rs.getString("cg_clasificacion_epo");
				}
				rs.close();
				ps.close();
			}catch(Exception e){
				log.error("getClasificacionEpo  Error: " + e);
				e.printStackTrace();
			}finally{
				try {
					if (rs != null)
						rs.close();
					if (ps != null)
						ps.close();
				} catch (Exception t) {
					log.error("getClasificacionEpo():: Error al cerrar Recursos: " + t.getMessage());
				}
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}
			
			return resultado;
	}
	public boolean insertModificarEliminarData(){
		log.info("insertModificarData(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			AccesoDB con = new AccesoDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			boolean exito = true;
			
				
			String TABLA2 = ""; 
			String TABLA = ""; 
			String CAMPOS2 = "";
			String CONDICION2 = "";
			String UPDATESET2 = "";
			String UPDATESET = "";
			String SQLcve = "";
			String VALORES = "";
			String VALORES2 = "";
			String CAMPOS ="";
			String SQLUpdateCatalogo = "";
			String CONDICLION = "";
			String SQLUpdateCatalogo2 = "";
				
			try{
				con.conexionDB();
				
				if(accion.equals("E")){
					TABLA = "CDERCAT_CLASIFICACION_EPO"; 
					CONDICLION = "IC_CLASIFICACION_EPO= "+CveClasifEpo+" AND IC_EPO= "+CveEpo;
				}
				if(!"".equals(ic_class_epo)){
					TABLA2 = "COMCAT_EPO"; CAMPOS2 = "IC_EPO, CG_CLASIFICACION_EPO"; 
					CONDICION2 = "IC_EPO = "+CveEpo+" "; 
					UPDATESET2 = " CG_CLASIFICACION_EPO = '"+ic_class_epo+"' ";
					VALORES2 =  " "+CveEpo+", '"+ic_class_epo+"' ";
					int reg_total = 0;
					if("I".equals(accion)){
							SQLcve =
								"SELECT COUNT(1)   " +
								"   FROM COMCAT_EPO  " +
								"  WHERE IC_EPO = '"+CveEpo+"'  " +
								"    AND CG_CLASIFICACION_EPO = '"+ic_class_epo+"' ";
							log.debug("SQLcve>>> "+SQLcve);
							rs = con.queryDB(SQLcve);
							if (rs.next()){
								reg_total = rs.getInt(1);
							}
							rs.close();con.cierraStatement();
							if(reg_total>0) {
								mensaje = "No se puede registrar nuevamente la clasificaci�n.";
							}
					}
				}	
				if(!"".equals(ic_area) && !"".equals(id_responsable) ){	
					if("".equals(CveClasifEpo) && "".equals(CveEpo)){	CveEpo = iNoCliente;	}
						TABLA = "CDERCAT_CLASIFICACION_EPO"; CAMPOS = "IC_CLASIFICACION_EPO, IC_EPO, CG_AREA, CG_RESPONSABLE"; 
						CONDICLION = "IC_CLASIFICACION_EPO = "+CveClasifEpo+" AND IC_EPO = "+CveEpo; 
						UPDATESET = 
							"  CG_AREA='" + ic_area + "'"+
							" ,CG_RESPONSABLE='" + id_responsable + "'";
						VALORES =  "(SELECT NVL(MAX(ic_clasificacion_epo),0) + 1 FROM cdercat_clasificacion_epo), "+CveEpo+", '"+ic_area + "','" + id_responsable +"'";
						int reg_total = 0;
						if("I".equals(accion)){
							SQLcve =
								" SELECT COUNT(1)  " +
								"   FROM CDERCAT_CLASIFICACION_EPO " +
								"  WHERE IC_EPO = "+CveEpo+" "+
								"	  AND CG_AREA = '"+ic_area+"' "+
								"    AND CG_RESPONSABLE = '"+id_responsable+"' ";
							log.debug("SQLcve>>>"+SQLcve);
							rs = con.queryDB(SQLcve);
							if (rs.next()){
								reg_total = rs.getInt(1);
							}
							rs.close();con.cierraStatement();
							if(reg_total>0) {
								bandera = false;
								mensaje = "No se puede registrar nuevamente.";
							}
						}
				}
				if(!"".equals(accion)&&bandera==true){
					if(accion.equals("I")){
						if(!"".equals(TABLA)&&!"".equals(CAMPOS)&&!"".equals(VALORES)){
							SQLUpdateCatalogo = "INSERT INTO " + TABLA + "(" + CAMPOS + ") VALUES(" + VALORES + ")";
							log.debug("El query del insert :: "+SQLUpdateCatalogo);
						}
						if(!"".equals(TABLA2)&&!"".equals(UPDATESET2)&&!"".equals(CONDICION2)){
							SQLUpdateCatalogo2 ="UPDATE " + TABLA2 + " SET " +	UPDATESET2 + " WHERE " + CONDICION2;
						}
						
						if(!mess.equals("G")) { 
							mensaje = "El registro fue actualizado";//mensaje_param.getMensaje("15mancatalogo.RegistroActualizado",sesIdiomaUsuario);
							mensaje += ", favor de capturar �rea y Responsable.";
						} else if(mess.equals("G")){
							mensaje = "Registro Guardado";
						}
					}else if(accion.equals("M")){
						if(!"".equals(TABLA)&&!"".equals(UPDATESET)&&!"".equals(CONDICLION)){
							SQLUpdateCatalogo = "UPDATE " + TABLA + " SET " + UPDATESET + " WHERE " + CONDICLION;
						}
						mensaje = "Registro Guardado";
					}else if(accion.equals("E")){
						SQLUpdateCatalogo = "DELETE " + TABLA + " WHERE " + CONDICLION;
						mensaje = "El registro fue Eliminado";
					}
				
				}
				
				if(!accion.equals("")){
					try{
						
						if(!"".equals(SQLUpdateCatalogo)){
							con.ejecutaSQL(SQLUpdateCatalogo);
						}
						if( !"".equals(SQLUpdateCatalogo2)) con.ejecutaSQL(SQLUpdateCatalogo2);
						con.terminaTransaccion(true);
					}catch(Exception e){
						con.terminaTransaccion(false);
						if(accion.equals("E"))
							mensaje = "No se puede eliminar el registro porque esta relacionado.";
					}
				}
			} catch (Exception e) {
				log.error("insertModificarData  Error: " + e);
				e.printStackTrace();
				exito = false;
				if(accion.equals("E"))
					log.error("NoPuedeEliminar  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.terminaTransaccion(exito);
					con.cierraConexionDB();
				}
			} 
			log.info("qrySentencia insertModificarData: "+qrySentencia.toString());
			log.info("conditions "+conditions);
			
			log.info("insertModificarData (S) ");
			return exito;
	}
	public void setClaveCadena(String claveCadena) {
		this.claveCadena = claveCadena;
	}


	public String getClaveCadena() {
		return claveCadena;
	}
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  

	public String getCveClasifEpo() {
		return CveClasifEpo;
	}

	public void setCveClasifEpo(String CveClasifEpo) {
		this.CveClasifEpo = CveClasifEpo;
	}

	public String getCveEpo() {
		return CveEpo;
	}

	public void setCveEpo(String CveEpo) {
		this.CveEpo = CveEpo;
	}


	public String getCboCatalogo() {
		return cboCatalogo;
	}

	public void setCboCatalogo(String cboCatalogo) {
		this.cboCatalogo = cboCatalogo;
	}

	public String getCveCat() {
		return cveCat;
	}

	public void setCveCat(String cveCat) {
		this.cveCat = cveCat;
	}

	public String getIc_area() {
		return ic_area;
	}

	public void setIc_area(String ic_area) {
		this.ic_area = ic_area;
	}

	public String getIc_class_epo() {
		return ic_class_epo;
	}

	public void setIc_class_epo(String ic_class_epo) {
		this.ic_class_epo = ic_class_epo;
	}

	public String getId_responsable() {
		return id_responsable;
	}

	public void setId_responsable(String id_responsable) {
		this.id_responsable = id_responsable;
	}

	public String getModifica() {
		return modifica;
	}

	public void setModifica(String modifica) {
		this.modifica = modifica;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getINoCliente() {
		return iNoCliente;
	}

	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}

	public String getMess() {
		return mess;
	}

	public void setMess(String mess) {
		this.mess = mess;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	

}