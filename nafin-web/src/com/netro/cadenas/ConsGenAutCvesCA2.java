package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;

public class ConsGenAutCvesCA2 implements IQueryGeneratorRegExtJS {

	StringBuffer  qrySentencia;
	private List  conditions;
	private String  operacion;
	private String  paginaOffset;
	private String  paginaNo;
  
	private String  rfc;
	private String  NomPyme;
	private String  txt_fecha_de;
	private String  txt_fecha_a;
	private String  accion;

	public ConsGenAutCvesCA2() {
		//Constructor
	}
	
	public String getAggregateCalculationQuery() {
		System.out.println("getAggregateCalculationQuery: (E) ");
		System.out.println("getAggregateCalculationQuery: (S) ");
		return qrySentencia.toString();
	}//getAggregateCalculationQuery
	
	/**
   * M�todo que arma el query para obtener los ids de la consulta paginada
   * Nota: es necesario la lista de conditions para las condiciones del where
   * 
   * @return String regresa la cadena armada con el query
   * */
	public String getDocumentQuery(){
		System.out.println("getDocumentQuery: (E) ");
		qrySentencia  = new StringBuffer();
		conditions    = new ArrayList(); 
		qrySentencia.append(
					" SELECT ent.ic_entrega_aut_claves "+  
					" from com_entrega_aut_claves ent  "+
					" where ent.cs_tipo_entrega = 1 ");   
			
		if(rfc!=null&&!"".equals(rfc)) {
			System.out.println("rfc: "+rfc);
			qrySentencia.append(" AND cg_rfc = ? ");
			conditions.add(rfc);
		}
		if(NomPyme!=null&&!"".equals(NomPyme)) {      
			String strTemp = NomPyme;
			NomPyme = "%" + NomPyme + "%";
			System.out.println("NomPyme: "+NomPyme);
			qrySentencia.append(" AND cg_razon_social like ? ");
			conditions.add(NomPyme);
			NomPyme = strTemp;
		}	
		if(txt_fecha_de!=null&&!"".equals(txt_fecha_de)) {
			System.out.println("txt_fecha_de: "+txt_fecha_de);
			qrySentencia.append(" AND df_alta > TO_DATE (?, 'dd/mm/yyyy') ");
			conditions.add(txt_fecha_de);
		}
		if(txt_fecha_a!=null&&!"".equals(txt_fecha_a)) {
			System.out.println("txt_fecha_a: "+txt_fecha_a);
			qrySentencia.append(" AND df_alta < TO_DATE (?, 'dd/mm/yyyy') +1 ");
			conditions.add(txt_fecha_a);
		}   
    
	 
		qrySentencia.append(" ORDER BY ic_entrega_aut_claves ");

		System.out.println("qrySentencia: "+qrySentencia);
		System.out.println("conditions: "+conditions);
		System.out.println("getDocumentQuery: (S) ");
    
		return qrySentencia.toString();
	}//getDocumentQuery
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		System.out.println("getDocumentSummaryQueryForIds: (E) ");
		qrySentencia  = new StringBuffer();
		conditions    = new ArrayList();
		qrySentencia.append(
					"   SELECT ic_entrega_aut_claves,cg_razon_social, cg_representante_legal,  " +
					"   cg_rfc, cg_telefono,cg_email, TO_CHAR(df_alta,'DD/MM/YYYY hh24:mi:ss') as df_alta   " +		
					"   from com_entrega_aut_claves    " + 
					"   where "); 
    
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0) {
				qrySentencia.append("  OR  "); 
			}
			qrySentencia.append("  (ic_entrega_aut_claves = ? ) "); 
			conditions.add(new Long(lItem.get(0).toString()));
		}//for(int i=0;i<ids.size();i++)

		qrySentencia.append(" AND cs_tipo_entrega = ? ");
		conditions.add(new Integer(1));

		qrySentencia.append(" ORDER BY ic_entrega_aut_claves ");
		System.out.println("qrySentencia: "+qrySentencia);
		System.out.println("conditions: "+conditions);
    
		System.out.println("getDocumentSummaryQueryForIds: (S) ");
		return qrySentencia.toString();
  }//getDocumentSummaryQueryForIds
  
	public String getDocumentQueryFile(){
		System.out.println("getDocumentQueryFile: (E) ");
		qrySentencia  = new StringBuffer();
		conditions    = new ArrayList();
		qrySentencia.append(
					"   SELECT cg_razon_social as NOMBRE_PYME, cg_representante_legal as REPRESENTANTE,  " +
					"   cg_rfc as RFC, cg_telefono as TELEFONO, cg_email as EMAIL, TO_CHAR(df_alta,'DD/MM/YYYY hh24:mi:ss') as FECHA_DE_REGISTRO " +	
					"   from com_entrega_aut_claves    " +
					"   where cs_tipo_entrega = 1"); 
    
		if(rfc!=null&&!"".equals(rfc)) {
			System.out.println("rfc: "+rfc);
			qrySentencia.append(" AND cg_rfc = ? ");
			conditions.add(rfc);
		}
		if(NomPyme!=null&&!"".equals(NomPyme)) {
			String strTemp = NomPyme;
			NomPyme = "%" + NomPyme + "%";
			System.out.println("NomPyme: "+NomPyme);
			qrySentencia.append(" AND cg_razon_social like ? ");
			conditions.add(NomPyme);
			NomPyme = strTemp;
		}
		if(txt_fecha_de!=null&&!"".equals(txt_fecha_de)) {
			System.out.println("txt_fecha_de: "+txt_fecha_de);
			qrySentencia.append(" AND df_alta > TO_DATE (?, 'dd/mm/yyyy') ");
			conditions.add(txt_fecha_de);
		}
		if(txt_fecha_a!=null&&!"".equals(txt_fecha_a)) {
			System.out.println("txt_fecha_a: "+txt_fecha_a);
			qrySentencia.append(" AND df_alta < TO_DATE (?, 'dd/mm/yyyy') +1 ");
			conditions.add(txt_fecha_a);
		}   
    
		qrySentencia.append(" ORDER BY ic_entrega_aut_claves ");
    
		System.out.println("qrySentencia: "+qrySentencia);
		System.out.println("conditions: "+conditions);
    
		System.out.println("getDocumentSummaryQueryForIds: (S) ");
		return qrySentencia.toString();
	}//getDocumentQueryFile
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();

		if ("CSV".equals(tipo)) {
			try {

				int registros = 0;
				String  nom_pyme= "", rep_legal = "", rfc= "", telefono="", email= "", fec_reg ="";

				while(rs.next()){
					if(registros==0) {
						contenidoArchivo.append(" NOMBRE PYME"+ "," +
												  " REPRESENTANTE LEGAL"+ "," +
												  " R.F.C"+ "," +
												  " TELEFONO"+ "," +
												  " EMAIL"+ "," +
												  " FECHA DE REGISTRO"/*+ ","*/ +"\n");
					}
						registros++;
						nom_pyme = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
						rep_legal = (rs.getString("REPRESENTANTE") == null) ? "" : rs.getString("REPRESENTANTE");
						rfc= (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
						telefono=(rs.getString("TELEFONO") == null)?"":rs.getString("TELEFONO");
						email= (rs.getString("EMAIL") == null) ? "" : rs.getString("EMAIL");
						fec_reg =(rs.getString("FECHA_DE_REGISTRO") == null) ? "" : rs.getString("FECHA_DE_REGISTRO");

						contenidoArchivo.append( nom_pyme.replaceAll(",","") +","+
                                     rep_legal.replaceAll(",","") +","+
                                     rfc.replaceAll(",","") +","+
                                     telefono.replaceAll(",","") +","+
                                     email.replaceAll(",","") +","+
                                     fec_reg+"\n");
					}

				if(registros >= 1){
					if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
						nombreArchivo="ERROR";
					}else{
						nombreArchivo = archivo.nombre;
					}
				}
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}

		return nombreArchivo;
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombre = "";
		
		return nombre;
	}
	
	//GETERS
	public List getConditions() {
		return conditions;
	}
	public String getOperacion() {
		return operacion;
	}
	public String getPaginaNo() {
		return paginaNo;
	}
	public String getPaginaOffset() {
		return paginaOffset;
	}
	public String getRFC() {
		return rfc;
	}
	public String getNOMPYME() {
		return NomPyme;
	}
	public String getFECHADE() {
		return txt_fecha_de;
	}
	public String getFECHAA() {
		return txt_fecha_a;
	} 
	public String getACCION(){
		return accion;
	}
  
	//SETERS
	public void setOperacion(String newOperacion) {
		operacion = newOperacion;
	}
	public void setPaginaNo(String newPaginaNo) {
		paginaNo = newPaginaNo;
	}
	public void setPaginaOffset(String newPaginaOffset) {
		paginaOffset = newPaginaOffset;
	}
	public void setRFC(String newrfc){
		rfc = newrfc;
	}
	public void setNOMPYME(String newNomPyme){
		NomPyme = newNomPyme;
	}
	public void setFECHADE(String newtxt_fecha_de){
		txt_fecha_de = newtxt_fecha_de;
	}
	public void setFECHAA(String newtxt_fecha_a){
		txt_fecha_a = newtxt_fecha_a;
	}   
  
	public void setACCION(String accion){this.accion = accion;} 
	
}