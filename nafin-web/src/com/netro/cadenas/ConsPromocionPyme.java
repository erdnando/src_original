package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/******************************************************************************
Paginador hecho para la pantalla Admin - Afiliados -	Consulta Promocion Pyme	
 *******************************************************************************/

public class ConsPromocionPyme implements IQueryGeneratorRegExtJS   {

	private String numeroEpo;
	private String nafinElectronico;
	private String icPyme;
	private String usuario;	
	private String fechaInicial;
	private String fechaFinal;
	private String tipoModificacion;	
	
	private List varBind;
	StringBuffer strSQL = new StringBuffer();	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsPromocionPyme.class);
	

	public ConsPromocionPyme() {}
	
		public String getDocumentQuery() {
			log.info("getDocumentQuery(E)");
			strSQL 	= new StringBuffer();
		
			log.info("qrySentencia  "+strSQL);
			log.info("getDocumentQuery(S)");
			return strSQL.toString();
		}

		public String getDocumentQueryFile(){
			log.info("getDocumentQueryFile(E)");
			varBind = new ArrayList();
			
			strSQL.append(" SELECT CRN.IC_NAFIN_ELECTRONICO AS nafinelectronico,");
			strSQL.append(" CCP.CG_RAZON_SOCIAL AS nombrepyme,");
			strSQL.append(" CPP.CG_CLAVE_USUARIO AS claveusuario,");
			strSQL.append(" CPP.CG_NOMBRE_USUARIO AS nombreusuario ,");
			strSQL.append(" TO_CHAR(CPP.DF_PROMOCION, 'DD/MM/YYYY HH:MI AM') AS fecha,");
			strSQL.append(" CCN.CG_APPAT||' '||CCN.CG_APMAT||' '|| CCN.CG_NOMBRE AS nombrecontacto,");
			strSQL.append(" CPP.CS_FACTORAJE_MOVIL AS aceptaservicio,");
			strSQL.append(" CCN.CG_EMAIL AS email,");
			strSQL.append(" CCN.CG_CELULAR AS celular");
			strSQL.append(" FROM COM_PROMO_PYME CPP,");
			strSQL.append(" COMREL_NAFIN CRN,");
			strSQL.append(" COMCAT_PYME CCP,");
			strSQL.append(" COM_CONTACTO CCN");
						
			if(!numeroEpo.equals("")){
				strSQL.append(", COMREL_PYME_EPO CPE");
			}
			
			strSQL.append(" WHERE CPP.IC_PYME = CRN.IC_EPO_PYME_IF");
			strSQL.append(" AND CPP.IC_PYME = CCP.IC_PYME");
			strSQL.append(" AND CPP.IC_PYME = CCN.IC_PYME");
			strSQL.append(" AND CCN.CS_PRIMER_CONTACTO = ?");
			strSQL.append(" AND CRN.CG_TIPO = ?");
			
			varBind.add("S");
			varBind.add("P");
											
			if(!numeroEpo.equals("")){
				strSQL.append(" AND CPP.IC_PYME = CPE.IC_PYME");
				strSQL.append(" AND CPE.IC_EPO = ?");
									
				varBind.add(new Long(numeroEpo));
			}
			
			if(!nafinElectronico.equals("") && !icPyme.equals("")){
				strSQL.append(" AND CRN.IC_NAFIN_ELECTRONICO = ?");
				strSQL.append(" AND CPP.IC_PYME = ?");
									
				varBind.add(new Long(nafinElectronico));
				varBind.add(new Long(icPyme));
			}

			if(!usuario.equals("")){
				strSQL.append(" AND CPP.CG_CLAVE_USUARIO = ?");
				
				varBind.add(usuario);
			}

			if(!fechaInicial.equals("") && !fechaFinal.equals("")){
				strSQL.append(" AND CPP.DF_PROMOCION BETWEEN TO_DATE( ?, 'DD/MM/YYYY') AND TO_DATE( ?, 'DD/MM/YYYY') + 1");
				
				varBind.add(fechaInicial);
				varBind.add(fechaFinal);
			}
		
			if(!tipoModificacion.equals("")){
				if(tipoModificacion.equals("EMAIL")){
					strSQL.append(" AND CPP.CS_EMAIL_MODIF = ?");
					varBind.add("S");					
				}else if(tipoModificacion.equals("CELULAR")){				
					strSQL.append(" AND CPP.CS_CELULAR_MODIF = ?");
					varBind.add("S");
				}
			}	
			log.info("qrySentencia  "+strSQL);
			log.info("conditions "+varBind);
			log.info("getDocumentQueryFile(S)");
			return strSQL.toString();		
		
		}//fin metodo


		public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
			log.debug("crearCustomFile (E)");
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
			CreaArchivo creaArchivo = new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer();
		
			try {
				StringBuffer strBuff = new StringBuffer();
				strBuff.append(",\n");
				strBuff.append(",,,,,,,Datos del Correo,Datos del Celular,\n");
				strBuff.append("Num. PyME"+
											 ",Nombre PyME"+
											 ",Clave Usuario"+
											 ",Nombre Usuario"+
											 ",Fecha/Hora"+
											 ",Nombre del Contacto"+
											 ",Acepto"+
											 ",Email"+
											 ",Celular\n");
				
				while(rs.next()) {
					String cveUsuario = (rs.getString(3) == null)?"":rs.getString(3).trim();
					String nomUsuario = (rs.getString(4) == null)?"":rs.getString(4).trim();
					String fech = (rs.getString(5) == null)?"":rs.getString(5).trim();
					String nomContacto = (rs.getString(6) == null)?"":rs.getString(6).trim();
					String acepta = (rs.getString(7) == null)?"":rs.getString(7).trim();
					String mail = (rs.getString(8) == null)?"":rs.getString(8).trim();
					String cel = (rs.getString(9) == null)?"":rs.getString(9).trim();
					
					String acepUsuario = "";
					
					if(acepta.equals("S"))
						acepUsuario = "SI";
					else
						acepUsuario = "NO";
					
					strBuff.append((rs.getString(1) == null)?"":rs.getString("nafinElectronico").trim()+
										 ","+((rs.getString(2) == null)?"":rs.getString(2).trim()).replaceAll(",", " ")+
										 ","+cveUsuario +
										 ","+nomUsuario +
										 ","+fech +
										 ","+nomContacto +
										 ","+acepUsuario+
										 ","+mail+
										 ","+cel+"\n");
				} 
				
				CreaArchivo archivo = new CreaArchivo();
				if(archivo.make(strBuff.toString(), path, ".csv"))
					nombreArchivo = archivo.getNombre();
			} catch(Exception e) { 
				log.error("Error en la generacion del Archivo CSV: " + e);
			}	
			return nombreArchivo;		 
		}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		return strSQL.toString();
	}

		public List getConditions(){
			return varBind;
		}

	public String getAggregateCalculationQuery(){
		return strSQL.toString();
	}

	public String getDocumentSummaryQueryForIds(List ids){
		return strSQL.toString();
	}

	/************************ SETTERS ************************/

		public void setEpo(String numeroEpo){
			this.numeroEpo = numeroEpo;
		}

		public void setNafin(String nafinElectronico){
			this.nafinElectronico = nafinElectronico;
		}	

		public void setPyme(String icPyme){
			this.icPyme = icPyme;
		}
		
		public void setUsuario(String usuario){
			this.usuario = usuario;
		}

		public void setFechaInicial(String fechaInicial){
			this.fechaInicial = fechaInicial;
		}		

		public void setFechaFinal(String fechaFinal){
			this.fechaFinal = fechaFinal;
		}
		
		public void setModificacion(String tipoModificacion){
			this.tipoModificacion = tipoModificacion;
		}		
}