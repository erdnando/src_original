package com.netro.cadenas;
import com.netro.pdf.ComunesPDF;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

public class ConsEstatusAfiliacion implements IQueryGeneratorRegExtJS {
	public ConsEstatusAfiliacion() {		}
	
	private List conditions;
	StringBuffer strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsEstatusAfiliacion.class);
	
	private String claveEpo;
	private String claveIf;
	private String nafinElectronico;
	private String noProveedor;
	private String fechaHabilitacionMin;//de
	private String fechaHabilitacionMax;//a
	private String perfil;
	private String pymesSN;
	private String pymesHab;
	
	private String chkPymesH;
	
	public String getAggregateCalculationQuery(){
		return "";
	}
	public String getDocumentQuery(){
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		strQuery.append("SELECT   /*+index(cpi) index(cpe) use_nl(cpe cn cpi ccb)*/ ");
		//strQuery.append(" ccb.ic_pyme, cpi.ic_if, cpi.ic_cuenta_bancaria");
		strQuery.append(" ccb.ic_pyme, cpi.ic_if,  cpi.ic_cuenta_bancaria, cpe.ic_pyme");
		strQuery.append(" FROM ");
		strQuery.append("      comrel_nafin cn,");
		strQuery.append("      comcat_pyme cp,");
		strQuery.append("      com_domicilio cd,");
		strQuery.append("      comrel_pyme_if cpi,");
		strQuery.append("      comrel_cuenta_bancaria ccb,");
		strQuery.append("      comcat_estado ce,");
		strQuery.append("      comcat_moneda cm,");
		strQuery.append("      comcat_if ci,");
		
		strQuery.append("      comrel_if_epo cie,");
		strQuery.append("      comcat_epo comce,");
		strQuery.append("comrel_pyme_epo cpe left join(comrel_grupo_x_epo cgxe  inner join comcat_grupo_epo cge on cgxe.ic_grupo_epo = cge.ic_grupo_epo) on cpe.ic_epo = cgxe.ic_epo ");
		
		strQuery.append(" WHERE ");
		strQuery.append("  cpe.ic_pyme = cn.ic_epo_pyme_if");
		strQuery.append(" AND cpe.ic_pyme = ccb.ic_pyme");
		strQuery.append(" AND cpe.ic_epo = cpi.ic_epo");
		strQuery.append(" AND cpe.ic_pyme = cp.ic_pyme");
		strQuery.append(" AND cpe.ic_epo = cie.ic_epo");
		strQuery.append(" AND cpe.ic_epo = comce.ic_epo");
		strQuery.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
		strQuery.append(" AND cp.ic_pyme = ccb.ic_pyme");
		strQuery.append(" AND cd.ic_pyme = cp.ic_pyme");
		strQuery.append(" AND ce.ic_pais = cd.ic_pais");
		strQuery.append(" AND ce.ic_estado = cd.ic_estado");
		strQuery.append(" AND ccb.ic_moneda = cm.ic_moneda");
		strQuery.append(" AND cpi.ic_if = ci.ic_if");
		strQuery.append(" AND ci.ic_if = cie.ic_if");
		
		strQuery.append(" AND cn.cg_tipo = ?");	
		strQuery.append(" AND cd.cs_fiscal = ?");
		strQuery.append(" AND cpi.cs_borrado = ?");
		strQuery.append(" AND cpi.cs_opera_descuento = ?");
		strQuery.append(" AND ccb.cs_borrado = ?");
		

		conditions.add(new String("P"));	
		conditions.add(new String("S"));
		conditions.add(new String("N"));
		conditions.add(new String("S"));
		conditions.add(new String("N"));
  
		log.info("pymesSN>>>>>>>"+pymesSN);

		if(!"".equals(this.claveEpo)){
			strQuery.append(" AND cpe.ic_epo = ?  ");
			conditions.add(claveEpo);
		}
      if(!"".equals(this.fechaHabilitacionMin)){
			strQuery.append(" AND cpi.df_vobo_if >= TO_DATE ( ?, 'dd/mm/yyyy') ");
			conditions.add(this.fechaHabilitacionMin);
		}
      if(!"".equals(this.fechaHabilitacionMax) ){
			strQuery.append(" AND cpi.df_vobo_if < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
			conditions.add(this.fechaHabilitacionMax);
	   } 
		if(!"".equals(this.noProveedor)){
			strQuery.append("	AND cpe.cg_pyme_epo_interno = ?  ");
			conditions.add(this.noProveedor);
		}
		if(!"".equals(this.nafinElectronico)) {
			strQuery.append(" AND cn.ic_nafin_electronico = ? ");
			conditions.add(this.nafinElectronico);
		}
		if(!"".equals(this.pymesSN)) {
			strQuery.append(" AND cpe.cs_num_proveedor = ? ");
			conditions.add(pymesSN);
		}
		if(!"".equals(this.pymesHab) && "S".equals(pymesHab)) {
			strQuery.append(" AND cpe.cs_aceptacion != ? ");
			conditions.add("H");
		}	
		/*else  {  // a solicitud de Irvin Sotuyo Reporte #1390
			strQuery.append(" AND cpe.cs_aceptacion = ? ");
			conditions.add("H");
		} */
		
		if(!"".equals(this.claveIf)) {
			strQuery.append(" AND cpi.ic_if = ? ");
			conditions.add(claveIf);
		}
		
		if(!"".equals(this.chkPymesH)){
			strQuery.append(" AND cpe.cs_aceptacion = ? ");
			conditions.add("H");
		}
		log.info("getDocumentQuery = " + strQuery.toString());
		log.info("conditions = " + conditions);
		return strQuery.toString();
	}
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		strQuery.append(" SELECT /*+index(cpi) index(cpe) use_nl(cpe cp cn cd cpi ccb ce)*/ distinct");
		strQuery.append("		cg_pyme_epo_interno AS no_proveedor,");
		strQuery.append("		cn.ic_nafin_electronico AS numero_nafin_electronico,");
		strQuery.append("		cp.in_numero_sirac AS numero_sirac," );
		strQuery.append("		decode(cpe.CS_ACEPTACION, 'H','Habilitada','Por Habilitar') AS estatus_pyme ,");
		strQuery.append("    cp.cg_rfc rfc, ");
		strQuery.append("		cp.cg_razon_social AS razon_social,");
		strQuery.append("		cd.cg_calle || '  ' || cd.cg_colonia AS domicilio_colonia,");
		strQuery.append("		ce.cd_nombre AS estado, cd.cg_telefono1 AS telefono,");
		strQuery.append("		cp.CG_EMAIL, ");
		strQuery.append("		ci.CG_RAZON_SOCIAL AS nombre_if,");
		strQuery.append("		cm.CD_NOMBRE AS moneda,  " );
		strQuery.append("		decode(cpi.CS_VOBO_IF, 'S', 'Liberado', 'Por Liberar') AS estatus_if  ");
		strQuery.append(" FROM ");
		strQuery.append("      comrel_nafin cn,");
		strQuery.append("      comcat_pyme cp,");
		strQuery.append("      com_domicilio cd,");
		strQuery.append("      comrel_pyme_if cpi,");
		strQuery.append("      comrel_cuenta_bancaria ccb,");
		strQuery.append("      comcat_estado ce,");
		strQuery.append("      comcat_moneda cm,");
		strQuery.append("      comcat_if ci,");
		
		strQuery.append("      comrel_if_epo cie,");
		strQuery.append("      comcat_epo comce,");
		strQuery.append("comrel_pyme_epo cpe left join(comrel_grupo_x_epo cgxe  inner join comcat_grupo_epo cge on cgxe.ic_grupo_epo = cge.ic_grupo_epo) on cpe.ic_epo = cgxe.ic_epo ");
		
		strQuery.append(" WHERE ");
		strQuery.append("  cpe.ic_pyme = cn.ic_epo_pyme_if");
		strQuery.append(" AND cpe.ic_pyme = ccb.ic_pyme");
		strQuery.append(" AND cpe.ic_epo = cpi.ic_epo");
		strQuery.append(" AND cpe.ic_pyme = cp.ic_pyme");
		strQuery.append(" AND cpe.ic_epo = cie.ic_epo");
		strQuery.append(" AND cpe.ic_epo = comce.ic_epo");
		strQuery.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
		strQuery.append(" AND cp.ic_pyme = ccb.ic_pyme");
		strQuery.append(" AND cd.ic_pyme = cp.ic_pyme");
		strQuery.append(" AND ce.ic_pais = cd.ic_pais");
		strQuery.append(" AND ce.ic_estado = cd.ic_estado");
		strQuery.append(" AND ccb.ic_moneda = cm.ic_moneda");
		strQuery.append(" AND cpi.ic_if = ci.ic_if");
		strQuery.append(" AND ci.ic_if = cie.ic_if");
		
		strQuery.append(" AND cn.cg_tipo = ?");	
		strQuery.append(" AND cd.cs_fiscal = ?");
		strQuery.append(" AND cpi.cs_borrado = ?");
		strQuery.append(" AND cpi.cs_opera_descuento = ?");
		strQuery.append(" AND ccb.cs_borrado = ?");
		

		conditions.add(new String("P"));	
		conditions.add(new String("S"));
		conditions.add(new String("N"));
		conditions.add(new String("S"));
		conditions.add(new String("N"));

		log.info("pymesSN>>>>>>>" + this.pymesSN);
		
		if(!"".equals(this.claveEpo)){
			strQuery.append(" AND cpe.ic_epo = ?  ");
			conditions.add(this.claveEpo);
		}
      if(!"".equals(this.fechaHabilitacionMin)){
			strQuery.append(" AND cpi.df_vobo_if >= TO_DATE ( ?, 'dd/mm/yyyy') ");
			conditions.add(this.fechaHabilitacionMin);
		}
      if(!"".equals(this.fechaHabilitacionMax) ){
		strQuery.append(" AND cpi.df_vobo_if < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
		conditions.add(this.fechaHabilitacionMax);
	   }
		if(!"".equals(this.pymesSN)) {
			strQuery.append(" AND cpe.cs_num_proveedor = ? ");
			conditions.add(this.pymesSN);
		}
		if(!"".equals(this.pymesHab)) {
			strQuery.append(" AND cpe.cs_aceptacion != ? ");
			conditions.add("H");			
		}
		/*else  {  // a solicitud de Irvin Sotuyo Reporte #1390
			strQuery.append(" AND cpe.cs_aceptacion = ? ");
			conditions.add("H");
		} */
		
		if(!"".equals(this.claveIf)) {
			strQuery.append(" AND cpi.ic_if = ? ");
			conditions.add(this.claveIf);
		}	
		
		if(!"".equals(this.chkPymesH)){
			strQuery.append(" AND cpe.cs_aceptacion = ? ");
			conditions.add("H");
		}
		
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i==0){
				strQuery.append(" AND (");
			}
			if(i>0){
				strQuery.append(" OR ");
			}
			strQuery.append("(ccb.ic_pyme = ? AND cpi.IC_IF = ?  AND cpi.ic_cuenta_bancaria = ? AND cpe.ic_pyme = ?)");
			if(i==pageIds.size()-1){
				strQuery.append(" )");
			}
			conditions.add(new Long(lItem.get(0).toString()));
			conditions.add(new Long(lItem.get(1).toString()));
			conditions.add(new Long(lItem.get(2).toString()));	
			conditions.add(new Long(lItem.get(3).toString()));	
		}
		log.info("getDocumentSummaryQueryForIds = " + strQuery.toString());
		log.info("conditions = " + conditions);
		return strQuery.toString();
	}
	public String getDocumentQueryFile(){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
	
		strQuery.append("SELECT /*+ index(ccb IN_COMREL_PYME_IF_02_NUK) use_nl(cpe, cp, cn, cd, ce, ccb, cpi)*/");
      strQuery.append(" cpe.cg_pyme_epo_interno AS no_proveedor,");
      strQuery.append("cn.ic_nafin_electronico AS numero_nafin_electronico,");
      strQuery.append("cp.in_numero_sirac AS numero_sirac,");
      strQuery.append("DECODE (cpe.cs_aceptacion,");
      strQuery.append("       'H', 'Habilitada',");
      strQuery.append("       'Por Habilitar') AS estatus_pyme,");			
      if(this.perfil.equals("PROMO NAFIN")){
			strQuery.append(" cp.cg_rfc rfc, ");
      }
      strQuery.append(" cp.cg_razon_social AS razon_social,");
      strQuery.append(" cd.cg_calle || '  ' || cd.cg_colonia AS domicilio_colonia,");
      strQuery.append(" ce.cd_nombre AS estado, cd.cg_telefono1 AS telefono,");          
      strQuery.append(" TO_CHAR (cpi.df_vobo_if, 'dd/mm/yyyy') AS fecha_de_habilitacion_if,");         
		strQuery.append(" cp.cg_email AS correo_electronico,");
		strQuery.append(" ci.cg_razon_social AS intermediario_financiero, cm.cd_nombre AS moneda,");
		strQuery.append(" DECODE (cpi.cs_vobo_if, 'S', 'Liberado', 'Por Liberar') AS estatus_if,");
		
		strQuery.append("cp.cg_rfc,");
        strQuery.append("comce.ic_epo,");
        strQuery.append("cge.cg_descripcion,");
        strQuery.append("DECODE(cie.cs_activa_limite, 'N', 'INACTIVO', 'ACTIVO') AS cs_activa_limite,");
		
		strQuery.append("comce.cg_razon_social as NOMBREPO,");
		strQuery.append("TO_CHAR (cie.df_venc_linea, 'dd/mm/yyyy') AS df_venc_linea,");
		strQuery.append("DECODE(cie.cs_fecha_limite, 'N', 'NO', 'SI') AS cs_fecha_limite");
		
		strQuery.append(" FROM comrel_nafin cn,");		
		strQuery.append("comrel_pyme_if cpi,");
		strQuery.append("comrel_cuenta_bancaria ccb,");
		strQuery.append("comcat_pyme cp,");
		strQuery.append("com_domicilio cd,");
		strQuery.append("comcat_estado ce,");    
		strQuery.append("comcat_moneda cm,");
		strQuery.append("comcat_if ci,");
		strQuery.append("comrel_if_epo cie,");
		strQuery.append("comcat_epo comce,");			
		
		strQuery.append("comrel_pyme_epo cpe left join(comrel_grupo_x_epo cgxe  inner join comcat_grupo_epo cge on cgxe.ic_grupo_epo = cge.ic_grupo_epo) on cpe.ic_epo = cgxe.ic_epo");
	  
		strQuery.append(" WHERE cpe.ic_pyme = cn.ic_epo_pyme_if");
		strQuery.append(" AND cpe.ic_pyme = ccb.ic_pyme");
		strQuery.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
		
		strQuery.append(" AND cpe.ic_epo = cpi.ic_epo");
		strQuery.append(" AND cp.ic_pyme = ccb.ic_pyme");
		strQuery.append(" AND cpe.ic_pyme = cp.ic_pyme");
		strQuery.append(" AND cd.ic_pyme = cp.ic_pyme");
		strQuery.append(" AND ce.ic_pais = cd.ic_pais");
		strQuery.append(" AND ce.ic_estado = cd.ic_estado");
		strQuery.append(" AND ccb.ic_moneda = cm.ic_moneda");
		strQuery.append(" AND cpi.ic_if = ci.ic_if");
		strQuery.append(" AND cie.ic_epo = cpe.ic_epo");
		strQuery.append(" AND ci.ic_if = cie.ic_if");
		strQuery.append(" AND cpe.ic_epo = comce.ic_epo");		
		
		strQuery.append(" AND cd.cs_fiscal = ?");		
		strQuery.append(" AND cn.cg_tipo = ?");		
		strQuery.append(" AND cpi.cs_borrado = ?");
		strQuery.append(" AND cpi.cs_opera_descuento = ?");
		strQuery.append(" AND ccb.cs_borrado = ?");
		

		conditions.add(new String("S"));	
		conditions.add(new String("P"));	
		conditions.add(new String("N"));
		conditions.add(new String("S"));
		conditions.add(new String("N"));
      
		log.info("fecha m�n: " + this.fechaHabilitacionMin);
      if(!"".equals(this.fechaHabilitacionMin)){
			strQuery.append(" AND cpi.df_vobo_if >= TO_DATE ( ?, 'dd/mm/yyyy') ");
			conditions.add(this.fechaHabilitacionMin);
		}
		log.info("fecha m�x: " + this.fechaHabilitacionMax);
      if(!"".equals(this.fechaHabilitacionMax) ){
			strQuery.append(" AND cpi.df_vobo_if < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
			conditions.add(this.fechaHabilitacionMax);
	   } 
		if(!"".equals(this.claveEpo)){
			strQuery.append(" AND cpe.ic_epo = ?  ");
			conditions.add(this.claveEpo);
		}
		if(!"".equals(this.noProveedor)){
			strQuery.append(" AND cpe.cg_pyme_epo_interno = ?  ");
			conditions.add(this.noProveedor);
		}
		if(!"".equals(this.nafinElectronico)) {
			strQuery.append(" AND cn.ic_nafin_electronico = ? ");
			conditions.add(this.nafinElectronico);
		}
		if(!"".equals(this.pymesSN)) {
			strQuery.append(" AND cpe.cs_num_proveedor = ? ");
			conditions.add(this.pymesSN);
		}
		if(!"".equals(this.pymesHab)) {
			strQuery.append(" AND cpe.cs_aceptacion != ? ");
			conditions.add("H");
		}
		/*else  {  // a solicitud de Irvin Sotuyo Reporte #1390
			strQuery.append(" AND cpe.cs_aceptacion = ? ");
			conditions.add("H");
		} */
		
		if(!"".equals(this.claveIf)) {
			strQuery.append(" AND cpi.ic_if = ? ");
			conditions.add(this.claveIf);
		}	
		if(!"".equals(this.chkPymesH)){
			strQuery.append(" AND cpe.cs_aceptacion = ? ");
			conditions.add("H");
		}
		log.info("getDocumentQueryFile = " + strQuery.toString());
		log.info("conditions = " + conditions);
		return strQuery.toString();
	}
		/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)  {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		String rfc = "";
		if("CSV".equals(tipo)){
			linea = "NO PROVEEDOR, N�MERO NAFIN ELECTR�NICO, N�MERO SIRAC, ESTATUS PYME, RAZON SOCIAL, DOMICILIO COLONIA, ESTADO, TEL�FONO, FECHA DE HABILITACION IF, CORREO ELECTR�NICO, INTERMEDIARIO FINANCIERO, MONEDA, ESTATUS IF, IC_EPO, Nombre EPO, GRUPO AL QUE PERTENECE LA EPO, RFC DEL PROVEEDOR, ESTATUS DE LA L�NEA DEL IF, Fecha de vencimiento de L�nea de Cr�dito, Validar fecha de vencimiento de L�nea de Cr�dito\n";
			if(this.perfil.equals("PROMO NAFIN")){
				linea = "NO PROVEEDOR, N�MERO NAFIN ELECTR�NICO, N�MERO SIRAC, ESTATUS PYME, RFC, RAZON SOCIAL, DOMICILIO COLONIA, ESTADO, TEL�FONO, FECHA DE HABILITACION IF, CORREO ELECTR�NICO, INTERMEDIARIO FINANCIERO, MONEDA, ESTATUS IF, IC_EPO, Nombre EPO, GRUPO AL QUE PERTENECE LA EPO, ESTATUS DE LA L�NEA DEL IF, Fecha de vencimiento de L�nea de Cr�dito, Validar fecha de vencimiento de L�nea de Cr�dito\n";
			}
			try{
				nombreArchivo = Comunes.cadenaAleatoria(19) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
				
				while(rs.next()){
					String numProveedor = (rs.getString("NO_PROVEEDOR") == null) ? "" : rs.getString("NO_PROVEEDOR");
					String nafinElectronico = (rs.getString("NUMERO_NAFIN_ELECTRONICO") == null) ? "" : rs.getString("NUMERO_NAFIN_ELECTRONICO");
					String numSirac = (rs.getString("NUMERO_SIRAC") == null) ? "" : rs.getString("NUMERO_SIRAC");
					String estatusPyme = (rs.getString("ESTATUS_PYME") == null) ? "" : rs.getString("ESTATUS_PYME");
					if(this.perfil.equals("PROMO NAFIN")){
						rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
					}
					String razonSocial = (rs.getString("RAZON_SOCIAL") == null) ? "" : rs.getString("RAZON_SOCIAL");
					String domicilio = (rs.getString("DOMICILIO_COLONIA") == null) ? "" : rs.getString("DOMICILIO_COLONIA");
					String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
					String email = (rs.getString("CORREO_ELECTRONICO") == null) ? "" : rs.getString("CORREO_ELECTRONICO");
					String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
					String fechaHabilitacion = (rs.getString("FECHA_DE_HABILITACION_IF") == null) ? "" : rs.getString("FECHA_DE_HABILITACION_IF");
					String intermediarioFinanciero = (rs.getString("INTERMEDIARIO_FINANCIERO") == null) ? "" : rs.getString("INTERMEDIARIO_FINANCIERO");
					String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					String estatusIf = (rs.getString("ESTATUS_IF") == null) ? "" : rs.getString("ESTATUS_IF");
					
					String icEpo = (rs.getString("IC_EPO") == null) ? "" : rs.getString("IC_EPO");
					String nombreEPO = (rs.getString("NOMBREPO") == null) ? "" : rs.getString("NOMBREPO");
					String cgDescripcion = (rs.getString("CG_DESCRIPCION") == null) ? "" : rs.getString("CG_DESCRIPCION");
					String cgRfc = (rs.getString("CG_RFC") == null) ? "" : rs.getString("CG_RFC");
					String csActivaLimite = (rs.getString("CS_ACTIVA_LIMITE") == null) ? "" : rs.getString("CS_ACTIVA_LIMITE");
					String fechVencLinCre = (rs.getString("DF_VENC_LINEA") == null) ? "" : rs.getString("DF_VENC_LINEA");
					String vaFechVencLinCre = (rs.getString("CS_FECHA_LIMITE") == null) ? "" : rs.getString("CS_FECHA_LIMITE");
					
					if(this.perfil.equals("PROMO NAFIN")){
					linea = numProveedor + ", " +
							  nafinElectronico + ", " +
							  numSirac + ", " +
							  estatusPyme + ", " +
							  rfc.replace(',',' ') + "," +
							  razonSocial.replace(',',' ') + "," +
							  domicilio.replace(',',' ') + "," +
							  estado.replace(',',' ') + "," +
							  telefono.replace(',',' ') + "," +
							  fechaHabilitacion + ", " +
							  email.replace(',',' ') + "," +
							  intermediarioFinanciero.replace(',',' ') + "," +
							  moneda.replace(',',' ') + "," +
							  estatusIf.replace(',',' ') + "," +
						
							icEpo.replace(',',' ') + "," +
							nombreEPO.replace(',',' ') + "," +
							cgDescripcion.replace(',',' ') + "," +
							csActivaLimite.replace(',',' ') + "," +
							fechVencLinCre.replace(',',' ') + "," +
							vaFechVencLinCre.replace(',',' ') + "," +"\n";
					} else{
						linea = numProveedor + ", " +
								nafinElectronico + ", " +
								numSirac + ", " +
								estatusPyme + ", " +
								razonSocial.replace(',',' ') + "," +
								domicilio.replace(',',' ') + "," +
								estado.replace(',',' ') + "," +
								telefono.replace(',',' ') + "," +
								fechaHabilitacion + ", " +
								email.replace(',',' ') + "," +
								intermediarioFinanciero.replace(',',' ') + "," +
								moneda.replace(',',' ') + "," +
								estatusIf.replace(',',' ') + "," +
							
							icEpo.replace(',',' ') + "," +
							nombreEPO.replace(',',' ') + "," +
							cgDescripcion.replace(',',' ') + "," +
							cgRfc.replace(',',' ') + "," +
                            csActivaLimite.replace(',',' ') + "," +
							fechVencLinCre.replace(',',' ') + "," +
							vaFechVencLinCre.replace(',',' ') + "," +"\n";
					}
					buffer.write(linea);
				}
				buffer.close();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
					rs.close();
				}catch(Exception e){}
			}
		}else if("PDF".equals(tipo)){
			try{
				float widths[]; 
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
				widths = new float [] {75.0f,25.0f};
				pdfDoc.setTable(2,100,widths);
				pdfDoc.setCell("DATOS DEL PROVEEDOR","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("DATOS INTERMEDIARIO FINANCIERO","celda01",ComunesPDF.CENTER);
				pdfDoc.addTable();
				pdfDoc.setTable(12,100);	
				pdfDoc.setCell("No. Proveedor", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No.Nafin Electr�nico", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. SIRAC", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre o raz�n social", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Domicilio", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Correo Electr�nico","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				while(rs.next()){
					String numProveedor = (rs.getString("NO_PROVEEDOR") == null) ? "" : rs.getString("NO_PROVEEDOR");
					String nafinElectronico = (rs.getString("NUMERO_NAFIN_ELECTRONICO") == null) ? "" : rs.getString("NUMERO_NAFIN_ELECTRONICO");
					String numSirac = (rs.getString("NUMERO_SIRAC") == null) ? "" : rs.getString("NUMERO_SIRAC");
					String estatusPyme = (rs.getString("ESTATUS_PYME") == null) ? "" : rs.getString("ESTATUS_PYME");
					String razonSocial = (rs.getString("RAZON_SOCIAL") == null) ? "" : rs.getString("RAZON_SOCIAL");
					String domicilio = (rs.getString("DOMICILIO_COLONIA") == null) ? "" : rs.getString("DOMICILIO_COLONIA");
					String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
					String email = (rs.getString("CORREO_ELECTRONICO") == null) ? "" : rs.getString("CORREO_ELECTRONICO");
					String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
					String fechaHabilitacion = (rs.getString("FECHA_DE_HABILITACION_IF") == null) ? "" : rs.getString("FECHA_DE_HABILITACION_IF");
					String intermediarioFinanciero = (rs.getString("INTERMEDIARIO_FINANCIERO") == null) ? "" : rs.getString("INTERMEDIARIO_FINANCIERO");
					String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					String estatusIf = (rs.getString("ESTATUS_IF") == null) ? "" : rs.getString("ESTATUS_IF");					
				
					pdfDoc.setCell(numProveedor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nafinElectronico,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numSirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatusPyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(razonSocial,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(domicilio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estado,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(email,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(intermediarioFinanciero,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatusIf,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el PDF",e);
			}finally{
				try{
					rs.close();
				}catch(Exception e){}
			}
		}
		return nombreArchivo;
	}
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		String rfc = "";
		if("PDF".equals(tipo)){
			try{
				float widths[]; 
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico") == null ? "" : session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
				widths = new float [] {75.0f,25.0f};
				pdfDoc.setTable(2,100,widths);
				pdfDoc.setCell("DATOS DEL PROVEEDOR","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("DATOS INTERMEDIARIO FINANCIERO","celda01",ComunesPDF.CENTER);
				pdfDoc.addTable();
				if(this.perfil.equals("PROMO NAFIN")){
					pdfDoc.setTable(13,100);	
					pdfDoc.setCell("No. Proveedor", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("No.Nafin Electr�nico", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("No. SIRAC", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Estatus", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("RFC", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre o raz�n social", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Domicilio", "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Estado","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tel�fono","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Correo Electr�nico","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				} else{
				pdfDoc.setTable(12,100);	
				pdfDoc.setCell("No. Proveedor", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No.Nafin Electr�nico", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. SIRAC", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre o raz�n social", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Domicilio", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Correo Electr�nico","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				}

				while(reg.next()){
					String numProveedor = (reg.getString("NO_PROVEEDOR") == null) ? "" : reg.getString("NO_PROVEEDOR");
					String nafinElectronico = (reg.getString("NUMERO_NAFIN_ELECTRONICO") == null) ? "" : reg.getString("NUMERO_NAFIN_ELECTRONICO");
					String numSirac = (reg.getString("NUMERO_SIRAC") == null) ? "" : reg.getString("NUMERO_SIRAC");
					String estatusPyme = (reg.getString("ESTATUS_PYME") == null) ? "" : reg.getString("ESTATUS_PYME");
					if(this.perfil.equals("PROMO NAFIN")){
						rfc = (reg.getString("RFC") == null) ? "" : reg.getString("RFC");
					}
					String razonSocial = (reg.getString("RAZON_SOCIAL") == null) ? "" : reg.getString("RAZON_SOCIAL");
					String domicilio = (reg.getString("DOMICILIO_COLONIA") == null) ? "" : reg.getString("DOMICILIO_COLONIA");
					String estado = (reg.getString("ESTADO") == null) ? "" : reg.getString("ESTADO");
					String email = (reg.getString("CG_EMAIL") == null) ? "" : reg.getString("CG_EMAIL");
					String telefono = (reg.getString("TELEFONO") == null) ? "" : reg.getString("TELEFONO");
					String intermediarioFinanciero = (reg.getString("NOMBRE_IF") == null) ? "" : reg.getString("NOMBRE_IF");
					String moneda = (reg.getString("MONEDA") == null) ? "" : reg.getString("MONEDA");
					String estatusIf = (reg.getString("ESTATUS_IF") == null) ? "" : reg.getString("ESTATUS_IF");					
				
					pdfDoc.setCell(numProveedor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nafinElectronico,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numSirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatusPyme,"formas",ComunesPDF.CENTER);
					if(this.perfil.equals("PROMO NAFIN")){
						pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setCell(razonSocial,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(domicilio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estado,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(email,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(intermediarioFinanciero,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatusIf,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el PDF",e);
			}
		}
		return nombreArchivo;
	}
	public List getConditions(){
		return conditions;
	}
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}
	public String getClaveEpo() {
		return claveEpo;
	}
	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}
	public String getClaveIf() {
		return claveIf;
	}
	public void setNafinElectronico(String nafinElectronico) {
		this.nafinElectronico = nafinElectronico;
	}
	public String getNafinElectronico() {
		return nafinElectronico;
	}
	public void setNoProveedor(String noProveedor) {
		this.noProveedor = noProveedor;
	}
	public String getNoProveedor() {
		return noProveedor;
	}
	public void setFechaHabilitacionMin(String fechaHabilitacionMin) {
		this.fechaHabilitacionMin = fechaHabilitacionMin;
	}
	public String getFechaHabilitacionMin() {
		return fechaHabilitacionMin;
	}
	public void setFechaHabilitacionMax(String fechaHabilitacionMax) {
		this.fechaHabilitacionMax = fechaHabilitacionMax;
	}
	public String getFechaHabilitacionMax() {
		return fechaHabilitacionMax;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPymesSN(String pymesSN) {
		this.pymesSN = pymesSN;
	}
	public String getPymesSN() {
		return pymesSN;
	}
	public void setPymesHab(String pymesHab) {
		this.pymesHab = pymesHab;
	}
	public String getPymesHab() {
		return pymesHab;
	}
	
	public void setChkPymesH(String chkPymesH) {
		this.chkPymesH = chkPymesH;
	}

	public String getChkPymesH() {
		return chkPymesH;
	}
	
}