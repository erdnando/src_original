package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase obtiene datos de las PYMES, en base a la epo.
 *
 * @author Andrea Isabel Luna Aguill�n
 */
public class ConsDatosPymesNuevo  implements IQueryGeneratorRegExtJS{
	public ConsDatosPymesNuevo() {	}
	
	private List	conditions;
	StringBuffer	strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsDatosPymesNuevo.class);
	
	private String claveEpo;
	private String clavePyme;
	private String claveIf;
	private String clavePais;
	private String claveEstado;
	private String claveDelegacionMunicipio;
	private String fechaActualizacionMin;
	private String fechaActualizacionMax;
	
	public String getAggregateCalculationQuery() {
		return "";
	}
	
	public String getDocumentQuery() {
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		strQuery.append("SELECT distinct(dpe.ic_pyme||'|'||dpe.ic_grupo) ");
		strQuery.append(" FROM com_datos_pyme_ext dpe, comrel_epo_param_grupo epg ");
		strQuery.append(" WHERE dpe.ic_grupo = epg.ic_grupo");
		if(!"".equals(this.claveEpo)){
			strQuery.append(" AND epg.ic_epo = ? ");
			conditions.add(new Integer(this.claveEpo));
		}
		if(!"".equals(this.clavePyme)){
			strQuery.append(" AND dpe.ic_pyme = ? ");
			conditions.add(new Integer(this.clavePyme));
		}
		if(!"".equals(this.claveIf)){
			strQuery.append(" AND epg.ic_epo in (select ic_epo from comrel_if_epo where ic_if = ? and CS_VOBO_NAFIN = 'S') ");
			conditions.add(new Integer(this.claveIf));
		}
		if(!"".equals(this.clavePais)){
			strQuery.append(" and dpe.ic_pais = ? ");
			conditions.add(new Integer(this.clavePais));
		}
		if(!"".equals(this.claveEstado)){
			strQuery.append(" and dpe.ic_estado = ? ");
			conditions.add(new Integer(this.claveEstado));
		}	
		if(!"".equals(this.claveDelegacionMunicipio)){
			strQuery.append(" and dpe.ic_municipio = ? ");
			conditions.add(new Integer(this.claveDelegacionMunicipio));
		}
		if(!"".equals(this.fechaActualizacionMin)){
			strQuery.append(" and trunc(df_fecha_captura) >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(this.fechaActualizacionMin);
		}
		if(!"".equals(this.fechaActualizacionMax)){
			strQuery.append(" and trunc(df_fecha_captura) <= to_date(?,'dd/mm/yyyy') ");
			conditions.add(this.fechaActualizacionMax);
		}
		log.info("getDocumentQuery = " + strQuery.toString());
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer();
		
		strQuery.append("SELECT p.cg_razon_social, dpe.cg_nombre_contacto, dpe.cg_telefono_contacto, dpe.cg_email_contacto,");
		strQuery.append(" dpe.cg_calle_numero||' '||dpe.cg_colonia||' '||m.cd_nombre||' '||e.cd_nombre||' '|| pa.cd_descripcion AS direccion,");
		strQuery.append(" to_char(dpe.df_fecha_captura,'dd/mm/yyyy') AS fecha_actualizacion");
		strQuery.append(" FROM comcat_pyme p, com_datos_pyme_ext dpe");
		strQuery.append(" , comcat_pais pa, comcat_estado e, comcat_municipio m ");
		strQuery.append(" WHERE p.ic_pyme = dpe.ic_pyme  ");
		strQuery.append(			" AND pa.ic_pais = dpe.ic_pais ");
		strQuery.append(			" AND e.ic_estado = dpe.ic_estado ");
		strQuery.append(			" AND e.ic_pais = dpe.ic_pais ");
		strQuery.append(			" AND e.ic_pais = pa.ic_pais ");
		strQuery.append(			" AND m.ic_municipio = dpe.ic_municipio ");
		strQuery.append(			" AND m.ic_pais = dpe.ic_pais ");
		strQuery.append(			" AND m.ic_pais = pa.ic_pais ");
		strQuery.append(			" AND m.ic_estado = dpe.ic_estado ");
		strQuery.append(			" AND m.ic_estado = e.ic_estado ");
		strQuery.append(			" AND dpe.ic_pyme||'|'||dpe.ic_grupo in (");
		for(int i=0;i<pageIds.size();i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0){
				strQuery.append(",");
			}
			strQuery.append(" ? ");
			conditions.add(lItem.get(0).toString());
		}
		strQuery.append(") ");		
		log.info("getDocumentSummaryQueryForIds = " + strQuery.toString());
		return strQuery.toString();
	}
	public String getDocumentQueryFile(){
		conditions	=	new ArrayList();
		strQuery		= new StringBuffer();

		strQuery.append("SELECT p.cg_razon_social, p.cg_rfc, dpe.cg_calle_numero, dpe.cg_colonia, e.cd_nombre AS estado, m.cd_nombre AS delegacion, dpe.cg_cp, ");
		strQuery.append(		" pa.cd_descripcion AS pais, dpe.cg_telefono, dpe.cg_fax, dpe.cg_email, dpe.cg_nombre_contacto, dpe.cg_telefono_contacto, ");
		strQuery.append(		" dpe.cg_fax_contacto, dpe.cg_email_contacto, to_char(dpe.df_fecha_captura,'dd/mm/yyyy') AS fecha_actualizacion ");
		strQuery.append(" FROM comcat_pyme p, com_datos_pyme_ext dpe, com_param_grupo epg, comcat_estado e, comcat_municipio m, ");
		strQuery.append(		" comcat_pais pa, comrel_epo_param_grupo cepg ");
		strQuery.append(" WHERE dpe.ic_grupo = epg.ic_grupo ");
		strQuery.append(" AND p.ic_pyme = dpe.ic_pyme ");
		strQuery.append(" AND pa.ic_pais = dpe.ic_pais ");
		strQuery.append(" AND e.ic_estado = dpe.ic_estado ");
		strQuery.append(" AND e.ic_pais = dpe.ic_pais ");
		strQuery.append(" AND e.ic_pais = pa.ic_pais ");
		strQuery.append(" AND m.ic_municipio = dpe.ic_municipio ");
		strQuery.append(" AND m.ic_pais = dpe.ic_pais ");
		strQuery.append(" AND m.ic_pais = pa.ic_pais ");
		strQuery.append(" AND m.ic_estado = dpe.ic_estado ");
		strQuery.append(" AND m.ic_estado = e.ic_estado ");
		strQuery.append(" AND cepg.ic_grupo = epg.ic_grupo ");
		if(!"".equals(this.claveEpo)){
			strQuery.append(" AND cepg.ic_epo = ? ");
			conditions.add(new Integer(this.claveEpo));
		}
		if(!"".equals(this.clavePyme)){
			strQuery.append(" AND dpe.ic_pyme = ? ");
			conditions.add(new Integer(this.clavePyme));
		}
		if(!"".equals(this.claveIf)){
			strQuery.append(" AND cepg.ic_epo in (select ic_epo from comrel_if_epo where ic_if = ? and CS_VOBO_NAFIN = 'S') ");
			conditions.add(new Integer(this.claveIf));
		}
		if(!"".equals(this.clavePais)){
			strQuery.append(" AND dpe.ic_pais = ? ");
			conditions.add(new Integer(this.clavePais));
		}
		if(!"".equals(this.claveEstado)){
			strQuery.append(" AND dpe.ic_estado = ? ");
			conditions.add(new Integer(this.claveEstado));
		}	
		if(!"".equals(this.claveDelegacionMunicipio)){
			strQuery.append(" AND dpe.ic_municipio = ? ");
			conditions.add(new Integer(this.claveDelegacionMunicipio));
		}
		if(!"".equals(this.fechaActualizacionMin)){
			strQuery.append(" AND trunc(df_fecha_captura) >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(this.fechaActualizacionMin);
		}
		if(!"".equals(this.fechaActualizacionMax)){
			strQuery.append(" AND trunc(df_fecha_captura) <= to_date(?,'dd/mm/yyyy') ");
			conditions.add(this.fechaActualizacionMax);
		}
		log.info("getDocumentQueryFile = " + strQuery.toString());	
		return strQuery.toString();
	}
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		String nombreArchivo = "";
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		if("CSV".equals(tipo)){
			linea = "Nombre o Raz�n Social,RFC,Calle y N�mero,Colonia,Estado,Delegaci�n o Municipio,C�digo Postal,Pa�s,Tel�fono,Fax,E mail,Nombre,Tel�fono,Fax,E mail";
			try{
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
				
				while(rs.next()){
					String nombre = rs.getString("CG_RAZON_SOCIAL")==null?"":rs.getString("CG_RAZON_SOCIAL");
					String rfc  = rs.getString("CG_RFC")==null?"":rs.getString("CG_RFC");
					String callenumero = rs.getString("CG_CALLE_NUMERO")==null?"":rs.getString("CG_CALLE_NUMERO");
					String colonia = rs.getString("CG_COLONIA")==null?"":rs.getString("CG_COLONIA");
					String estadoe = rs.getString("ESTADO")==null?"":rs.getString("ESTADO");
					String delegacion  = rs.getString("DELEGACION")==null?"":rs.getString("DELEGACION");
					String cp = rs.getString("CG_CP")==null?"":rs.getString("CG_CP");
					String paise = rs.getString("PAIS")==null?"":rs.getString("PAIS");
					String telefono = rs.getString("CG_TELEFONO")==null?"":rs.getString("CG_TELEFONO");
					String fax = rs.getString("CG_FAX")==null?"":rs.getString("CG_FAX");
					String email = rs.getString("CG_EMAIL")==null?"":rs.getString("CG_EMAIL");
					String nombrec = rs.getString("CG_NOMBRE_CONTACTO")==null?"":rs.getString("CG_NOMBRE_CONTACTO");
					String telefonoc = rs.getString("CG_TELEFONO_CONTACTO")==null?"":rs.getString("CG_TELEFONO_CONTACTO");
					String faxc  = rs.getString("CG_FAX_CONTACTO")==null?"":rs.getString("CG_FAX_CONTACTO");
					String emailc = rs.getString("CG_EMAIL_CONTACTO")==null?"":rs.getString("CG_EMAIL_CONTACTO");
					
					linea = "\n"+
									nombre.replace(',',' ')+","+
									rfc+","+
									callenumero.replace(',',' ')+","+
									colonia.replace(',',' ')+","+
									estadoe.replace(',',' ')+","+
									delegacion.replace(',',' ')+","+
									cp+","+
									paise.replace(',',' ')+","+
									telefono+","+
									fax+","+
									email+","+
									nombrec.replace(',',' ')+","+
									telefonoc+","+
									faxc+","+
									emailc;
					buffer.write(linea);
				}
				buffer.close();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
					rs.close();
				}catch(Exception e){}
			}
		}else if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);	
				
				pdfDoc.setTable(6,100);
				pdfDoc.setCell("Nombre de la Empresa", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Contacto", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Correo electr�nico", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Direcci�n", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de actualizaci�n", "celda01",ComunesPDF.CENTER);
				
				while(rs.next()){
					String nombre = rs.getString("CG_RAZON_SOCIAL")==null?"":rs.getString("CG_RAZON_SOCIAL");
					String callenumero = rs.getString("CG_CALLE_NUMERO")==null?"":rs.getString("CG_CALLE_NUMERO");
					String colonia = rs.getString("CG_COLONIA")==null?"":rs.getString("CG_COLONIA");
					String estadoe = rs.getString("ESTADO")==null?"":rs.getString("ESTADO");
					String delegacion  = rs.getString("DELEGACION")==null?"":rs.getString("DELEGACION");
					String paise = rs.getString("PAIS")==null?"":rs.getString("PAIS");
					String nombrec = rs.getString("CG_NOMBRE_CONTACTO")==null?"":rs.getString("CG_NOMBRE_CONTACTO");
					String telefonoc = rs.getString("CG_TELEFONO_CONTACTO")==null?"":rs.getString("CG_TELEFONO_CONTACTO");
					String emailc = rs.getString("CG_EMAIL_CONTACTO")==null?"":rs.getString("CG_EMAIL_CONTACTO");
					String fechaActualizacion = rs.getString("FECHA_ACTUALIZACION")==null?"":rs.getString("FECHA_ACTUALIZACION");
					StringBuffer domicilio = new StringBuffer();
					domicilio.append(callenumero);
					domicilio.append(" ");
					domicilio.append(colonia);
					domicilio.append(" ");
					domicilio.append(delegacion);
					domicilio.append(" ");
					domicilio.append(estadoe);
					domicilio.append(" ");
					domicilio.append(paise);
					
					pdfDoc.setCell(nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombrec,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(telefonoc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(emailc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(domicilio.toString(),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaActualizacion,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();				
			}catch(Throwable e){
				throw new AppException("Error al generar el PDF",e);
			}finally{
				try{
					rs.close();
				}catch(Exception e){}
			}
		}
		return nombreArchivo;
	}
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);	
				
				pdfDoc.setTable(6,100);
				pdfDoc.setCell("Nombre de la Empresa", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Contacto", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Correo electr�nico", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Direcci�n", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de actualizaci�n", "celda01",ComunesPDF.CENTER);
				
				while(reg.next()){
					String nombre = reg.getString("CG_RAZON_SOCIAL")==null?"":reg.getString("CG_RAZON_SOCIAL");
					String domicilio = reg.getString("DIRECCION")==null?"":reg.getString("DIRECCION");
					String nombrec = reg.getString("CG_NOMBRE_CONTACTO")==null?"":reg.getString("CG_NOMBRE_CONTACTO");
					String telefonoc = reg.getString("CG_TELEFONO_CONTACTO")==null?"":reg.getString("CG_TELEFONO_CONTACTO");
					String emailc = reg.getString("CG_EMAIL_CONTACTO")==null?"":reg.getString("CG_EMAIL_CONTACTO");
					String fechaActualizacion = reg.getString("FECHA_ACTUALIZACION")==null?"":reg.getString("FECHA_ACTUALIZACION");
					
					pdfDoc.setCell(nombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombrec,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(telefonoc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(emailc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(domicilio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaActualizacion,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();					
			}catch(Throwable e){
				throw new AppException("Error al generar el PDF",e);
			}
		}
		return nombreArchivo;
	}
	public List getConditions(){
		return conditions;
	}
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}
	public String getClaveEpo() {
		return claveEpo;
	}
	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}
	public String getClavePyme() {
		return clavePyme;
	}
	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}
	public String getClaveIf() {
		return claveIf;
	}
	public void setClavePais(String clavePais) {
		this.clavePais = clavePais;
	}
	public String getClavePais() {
		return clavePais;
	}
	public void setClaveEstado(String claveEstado) {
		this.claveEstado = claveEstado;
	}
	public String getClaveEstado() {
		return claveEstado;
	}
	public void setClaveDelegacionMunicipio(String claveDelegacionMunicipio) {
		this.claveDelegacionMunicipio = claveDelegacionMunicipio;
	}
	public String getClaveDelegacionMunicipio() {
		return claveDelegacionMunicipio;
	}
	public void setFechaActualizacionMin(String fechaActualizacionMin) {
		this.fechaActualizacionMin = fechaActualizacionMin;
	}
	public String getFechaActualizacionMin() {
		return fechaActualizacionMin;
	}
	public void setFechaActualizacionMax(String fechaActualizacionMax) {
		this.fechaActualizacionMax = fechaActualizacionMax;
	}
	public String getFechaActualizacionMax() {
		return fechaActualizacionMax;
	}
}