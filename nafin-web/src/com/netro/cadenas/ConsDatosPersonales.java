package com.netro.cadenas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 *
 * @author Andrea Isabel Luna Aguill�n
 */

public class ConsDatosPersonales implements IQueryGeneratorRegExtJS {
	public ConsDatosPersonales() {	}
	
	private List conditions;
	StringBuffer strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsDatosPersonales.class);//Variable para enviar mensajes al Log.
	
	private String claveEPO;
	private String clavePyme;
	
		public String getAggregateCalculationQuery() {
		return "";
	}
	
	public String getDocumentQuery() {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentQueryFile(){
		String ciudad 		= "";
		String paisOrigen	= "";
		String strHabAnt	= "";
		String strHabInv	= "";
		
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		Map map		=	obtenerDatosAdicionales();
		
		if(map !=null){
			ciudad = String.valueOf(map.get("CIUDAD"));
			paisOrigen = String.valueOf(map.get("PAIS_ORIGEN"));
			strHabAnt = String.valueOf(map.get("STRHABANT"));
			strHabInv = String.valueOf(map.get("STRHABINV"));
		}
		
		log.debug("ic_epo= "+this.claveEPO);
		log.debug("ic_pyme= "+this.clavePyme);
		
		strQuery.append(
		"SELECT /*+ ordered use_nl(pe,p,v,d,c) */ ");
		if("".equals(ciudad))
			strQuery.append("'' as ciudad,");
		else if(!"".equals(ciudad))
			strQuery.append("'" + ciudad + "'" + " AS ciudad,");
		if("".equals(paisOrigen))
			strQuery.append("'' as paisOrigen,");
		else if(!"".equals(paisOrigen))
			strQuery.append("'" + paisOrigen + "'" + " AS paisOrigen,");
		if("".equals(strHabAnt))
			strQuery.append("'' as strHabAnt,");
		else if(!"".equals(strHabAnt))
			strQuery.append("'" + strHabAnt + "'" + " AS strHabAnt,");
		if("".equals(strHabInv))
			strQuery.append("'' as strHabInv,");
		else if(!"".equals(strHabInv))
			strQuery.append("'" + strHabInv + "'" + " AS strHabInv,");
		strQuery.append(
					"pe.cs_aceptacion as strTAfilia, pe.cg_pyme_epo_interno as Numero_de_cliente,"+
					"pe.cg_num_distribuidor as Numero_de_cliente_dist,"+
					"p.cg_appat AS apellido_paterno, p.cg_apmat AS apellido_materno,"+
					"p.cg_nombre AS nombre, p.cg_rfc, p.cg_razon_social, d.cg_calle,"+
					"d.cg_numero_ext, d.cg_numero_int, d.cg_colonia, d.ic_estado,"+
					"es.cd_nombre AS estado, d.ic_pais, pa.cd_descripcion AS pais,"+
					"ci.cd_nombre AS delegacion_municipio, d.cn_cp, d.cg_telefono1,"+
					"d.cg_fax, d.cg_email, c.cg_appat AS apellido_paterno_c,"+
					"c.cg_apmat AS apellido_materno_c, c.cg_nombre AS nombre_c,"+
					"c.cg_tel AS telefono_c, c.cg_fax AS fax_c, c.cg_email AS email_c,"+
					"d.ic_domicilio_epo, c.ic_contacto, '' AS cg_login, p.cs_tipo_persona,"+
					"p.cg_nombre_comercial, p.cg_appat_rep_legal, p.cg_apmat_legal,"+
					"p.cg_nombrerep_legal, p.cg_telefono AS telefono_rep_legal,"+
					"p.cg_fax AS fax_rep_legal, p.cg_email AS email_rep_legal,"+
					"p.ic_identificacion, p.ic_grado_esc, p.ic_tipo_categoria,"+
					"pe.cs_aceptacion, p.cg_ejec_cta, p.cg_no_escritura,"+
					"p.in_numero_notaria, p.in_empleos_generar, p.fn_activo_total,"+
					"p.fn_capital_contable, p.fn_ventas_net_exp, p.fn_ventas_net_tot,"+
					"TO_CHAR (p.df_constitucion, 'DD/MM/YYYY') AS df_constitucion,"+
					"p.in_numero_emp, p.ic_sector_econ, p.ic_subsector, p.ic_rama,"+
					"p.ic_clase, p.ic_tipo_empresa, p.ic_dom_correspondencia,"+
					"p.cg_productos, p.in_numero_sirac AS numsirac,"+
					"p.ic_estado_civil AS edo_civil, p.ic_pais_origen AS pais_origen,"+
					"TO_CHAR (p.df_nacimiento, 'DD/MM/YYYY') AS nacimiento,"+
					"p.cg_regimen_mat AS cg_regimen_mat, p.cg_sexo, p.cg_app_casada,"+
					"d.ic_municipio, p.cg_rfc_legal, p.cg_no_identificacion,"+
					"v.cd_version_convenio,"+
					"TO_CHAR (p.df_version_convenio, 'DD/MM/YYYY') AS df_version_convenio,"+
					"p.in_numero_troya, p.cg_curp AS curp, p.cn_fiel AS fiel "+
		"FROM	comrel_pyme_epo pe,"+
				"comcat_pyme p,"+
				"comcat_version_convenio v,"+
				"com_domicilio d,"+
				"com_contacto c,"+
				"comcat_pais pa,                               /*Fodea campos PLD 2010*/ "+
				"comcat_municipio ci,                          /*Fodea campos PLD 2010*/ "+
				"comcat_estado es "+
		"WHERE  p.ic_pyme = pe.ic_pyme "+
			"AND p.ic_pyme = d.ic_pyme "+
			"AND p.ic_version_convenio = v.ic_version_convenio(+) "+
			"AND p.ic_pyme = c.ic_pyme "+
			"AND pa.ic_pais = d.ic_pais                        /*Fodea campos PLD 2010*/ "+
			"AND ci.ic_estado = d.ic_estado                    /*Fodea campos PLD 2010*/ "+
			"AND ci.ic_municipio = d.ic_municipio              /*Fodea campos PLD 2010*/ "+
			"AND d.ic_estado = es.ic_estado "+
			"AND d.cs_fiscal = 'S' "+
			"AND c.cs_primer_contacto = 'S' "+
			"AND p.ic_pyme = ? "+
			"AND pe.ic_epo = ? ");
			
		conditions.add(this.clavePyme);
		conditions.add(this.claveEPO);
		
	   log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile "+conditions);
		return strQuery.toString();
	}
	
		/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)  {
		String nombreArchivo = "";
		return nombreArchivo;
	}
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	public Map obtenerDatosAdicionales(){
		log.debug("ClavePyme="+this.clavePyme);
		log.debug("ClaveEPO="+this.claveEPO);
		
		//Configurar conexi�n
		AccesoDB 		con				=	new AccesoDB();
		StringBuffer	qrySentencia;
		List 				lVarBind			=	null;
		Registros		registros		=	null;
		Registros		registrosHab1	=	null;
		Registros		registrosHab2	=	null;
		boolean			hayExito			=	false;
		Map				map				=	new HashMap();
							lVarBind			=	new ArrayList();
							hayExito			=	true;
		String 			ciudad 			=	"";
		String			paisOrigen		=	"";
		String			strHabAnt		=	"";
		String			strHabInv		=	"";
		
		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append("SELECT /*+ ordered use_nl(pe,p,v,d,c) */"+
											"pa.cd_descripcion AS paisorigen, ci.nombre AS ciudad "+
										"FROM comrel_pyme_epo pe,"+
												"comcat_pyme p,"+
												"comcat_version_convenio v,"+
												"com_domicilio d,"+
												"com_contacto c,"+
												"comcat_pais pa,"+
												"comcat_ciudad ci "+
										"WHERE p.ic_pyme = pe.ic_pyme"+
										" AND p.ic_pyme = d.ic_pyme"+
										" AND p.ic_version_convenio = v.ic_version_convenio(+)"+
										" AND p.ic_pyme = c.ic_pyme"+
										" AND pa.ic_pais = p.ic_pais_origen"+
										" AND ci.codigo_departamento = d.ic_estado"+
										" AND  ci.codigo_ciudad = d.ic_ciudad"+
										" AND d.cs_fiscal = 'S'"+
										" AND c.cs_primer_contacto = 'S'"+
										" AND P.ic_pyme = ?"+
										" AND PE.ic_epo = ?");
			lVarBind.add(new Integer(this.clavePyme));
			lVarBind.add(new Integer(this.claveEPO));
			
			registros = con.consultarDB(qrySentencia.toString(),lVarBind,false);
			
			log.debug("qrySentencia= "+qrySentencia.toString());
			log.debug("lVarBind= "+lVarBind);
			log.debug("registros");
			while (registros != null && registros.next()){
			ciudad = (registros.getString("ciudad")!=null)?registros.getString("ciudad"):"";
			paisOrigen = (registros.getString("paisorigen")!=null)?registros.getString("paisorigen"):"";
			}	
		}catch(Exception e){
			log.error("obtenerDatosAdicionales(Error)",e);
			hayExito=false;
		}finally{
			if(!hayExito && con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		try{
			hayExito = true;
			qrySentencia = new StringBuffer();
			qrySentencia.append("Select cs_habilitado from comrel_pyme_epo_x_producto "+
			"WHERE ic_producto_nafin = 2 and ic_pyme =? and ic_epo = ?");
			lVarBind.clear();
			lVarBind.add(new Integer(this.clavePyme));
			lVarBind.add(new Integer(this.claveEPO));
			
			log.debug("qrySentencia2= "+qrySentencia.toString());
			log.debug("lVarBind= "+lVarBind);
			
			registrosHab1 = con.consultarDB(qrySentencia.toString(),lVarBind);
			if(registrosHab1 != null && registrosHab1.next()){
				strHabAnt = (registrosHab1.getString("cs_habilitado")!=null)?registrosHab1.getString("cs_habilitado"):"";
			}
		}catch(Exception e){
			log.error("obtenerDatosAdicionales(Error2)",e);
			hayExito = false;
		}finally{
			if(!hayExito && con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		try{
			hayExito = true;
			qrySentencia = new StringBuffer();
			qrySentencia.append("Select cs_habilitado from comrel_pyme_epo_x_producto "+
			"WHERE ic_producto_nafin = 5 and ic_pyme =? and ic_epo = ?");
			lVarBind.clear();
			lVarBind.add(new Integer(this.clavePyme));
			lVarBind.add(new Integer(this.claveEPO));
			
			log.debug("qrySentencia3= "+qrySentencia.toString());
			log.debug("lVarBind= "+lVarBind);
			
			registrosHab2 = con.consultarDB(qrySentencia.toString(),lVarBind);
			if(registrosHab2 != null && registrosHab2.next()){
				strHabInv = (registrosHab2.getString("cs_habilitado")!=null)?registrosHab2.getString("cs_habilitado"):"";
			}
			
			map.put("CIUDAD",ciudad);
			map.put("PAIS_ORIGEN",paisOrigen);
			map.put("STRHABANT",strHabAnt);
			map.put("STRHABINV",strHabInv);
			
		}catch(Exception e){
			log.error("obtenerDatosAdicionales(Error3)",e);
			hayExito = false;
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
				log.info("obtenerDatosAdicionales (S)");
			}
		}
		return map;
	}
	public List getConditions()	
	{
		return conditions;
	}
	
	public String getClaveEPO()	
	{
		return	claveEPO;
	}
	
	public void setClaveEPO(String claveEPO)	
	{
		this.claveEPO	=	claveEPO;
	}
	
	public String getClavePyme()	
	{
		return	clavePyme;
	}
	
	public void setClavePyme (String clavePyme)	
	{
		this.clavePyme	=	clavePyme;
	}
	
}