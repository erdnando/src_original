package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConCadenasProductivas implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConCadenasProductivas.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String txtNumElectronico;
	private String txtNombre;
	private String sel_edo;
	private String convenio_unico;
		 
	public ConCadenasProductivas() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		qrySentencia.append("  SELECT dom.ic_epo IC_EPO "+
			" FROM com_domicilio dom,  "+
			" comcat_epo e,  "+
			" comcat_estado edo,  "+
			" comrel_nafin n ,  "+
			" comcat_banco_fondeo fon "+
			" WHERE dom.ic_epo=e.ic_epo AND e.ic_epo=n.ic_epo_pyme_if "+ 
			" AND dom.ic_estado=edo.ic_estado AND n.cg_tipo='E' "+
			" AND e.ic_banco_fondeo = fon.ic_banco_fondeo   "+
			" AND e.CS_INTERNACIONAL = 'N'   "+
			" AND dom.cs_fiscal='S' ");


			if(!txtNombre.equals("")){
				qrySentencia.append(" AND (upper(trim(e.cg_razon_social)) LIKE upper(?))") ;
				conditions.add(txtNombre+"%");
			}
			
			if(!sel_edo.equals("")){
				qrySentencia.append(" AND dom.ic_estado = ? ");
				conditions.add(sel_edo);
			}	

			if (!txtNumElectronico.equals("")){
				qrySentencia.append(" AND n.ic_nafin_electronico = ? ");
				conditions.add(txtNumElectronico);
			}
	
			if(convenio_unico.equals("S")){
				qrySentencia.append(" AND e.cs_convenio_unico = ? ");
				conditions.add(convenio_unico);
			}
		
			qrySentencia.append(" order by  e.ic_epo  asc  ");
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		qrySentencia.append("  SELECT dom.ic_epo IC_EPO, "+
			" e.cg_razon_social as NOMBRE_EPO,  "+
			" dom.cg_calle ||' '|| dom.cg_numero_ext ||' '|| dom.cg_numero_int ||' '|| dom.cg_colonia  as  DOMICILIO,  "+
			" edo.cd_nombre ESTADO,  "+
			" dom.cg_telefono1 as TELEFONO,  "+
			" n.ic_nafin_electronico as NUM_NAFIN_ELEC,  "+
			" fon.cd_descripcion  as BANCO_FODEO ,  "+
			" decode (e.cs_convenio_unico, 'S', 'SI', 'N', 'NO')  as CONVENIO_UNICO ,  "+
			" DECODE(e.cs_habilitado, 'N','Bloqueado por ' ||' '||  e.ic_usuario_bloqueo  ||' '|| TO_CHAR(df_bloqueo, 'dd/mm/yyyy hh24:mi:ss' ) , 'S' ,'')  OBSERVACIONES ," +
			" e.cs_habilitado AS  CS_HABILITADO, "+ 			
			" promotor.cg_descripcion AS LIDER_PROMOTOR  "+ // F021-2015
			" FROM com_domicilio dom,  "+
			" comcat_epo e,  "+
			" comcat_estado edo,  "+
			" comrel_nafin n ,  "+
			" comcat_banco_fondeo fon, "+
			" comcat_lider_promotor promotor " +
			" WHERE dom.ic_epo=e.ic_epo AND e.ic_epo=n.ic_epo_pyme_if "+ 
			" AND dom.ic_estado=edo.ic_estado AND n.cg_tipo='E' "+
			" AND e.ic_banco_fondeo = fon.ic_banco_fondeo   "+
			" AND e.CS_INTERNACIONAL = 'N'   "+
			" AND dom.cs_fiscal='S' ");
			
			if(!txtNombre.equals("")){
				qrySentencia.append(" AND (upper(trim(e.cg_razon_social)) LIKE upper(?))") ;
				conditions.add(txtNombre+"%");
			}
			
			if(!sel_edo.equals("")){
				qrySentencia.append(" AND dom.ic_estado = ? ");
				conditions.add(sel_edo);
			}	

			if (!txtNumElectronico.equals("")){
				qrySentencia.append(" AND n.ic_nafin_electronico = ? ");
				conditions.add(txtNumElectronico);
			}
	
			if(convenio_unico.equals("S")){
				qrySentencia.append(" AND e.cs_convenio_unico = ? ");
				conditions.add(convenio_unico);
			}
			
			
			qrySentencia.append(" AND (");
			
			for (int i = 0; i < pageIds.size(); i++) { 
				List lItem = (ArrayList)pageIds.get(i);
				
				if(i > 0){qrySentencia.append("  OR  ");}
				
				qrySentencia.append(" e.ic_epo =  ? " );
				conditions.add(new Long(lItem.get(0).toString()));
			}
			
			qrySentencia.append(" ) ");
			
			qrySentencia.append( // F021 - 2015
				" AND e.ic_lider_promotor = promotor.ic_lider_promotor(+) "
			);
			
			qrySentencia.append(" order by  e.ic_epo  asc  ");
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
	
		qrySentencia.append("  SELECT dom.ic_epo IC_EPO, "+
			" e.cg_razon_social as NOMBRE_EPO,  "+
			" dom.cg_calle ||' '|| dom.cg_numero_ext ||' '|| dom.cg_numero_int ||' '|| dom.cg_colonia  as  DOMICILIO,  "+
			" edo.cd_nombre ESTADO,  "+
			" dom.cg_telefono1 as TELEFONO,  "+
			" n.ic_nafin_electronico as NUM_NAFIN_ELEC,  "+
			" fon.cd_descripcion  as BANCO_FODEO ,  "+
			" decode (e.cs_convenio_unico, 'S', 'SI', 'N', 'NO')  as CONVENIO_UNICO ,  "+
			" DECODE(e.cs_habilitado, 'N','Bloqueado por ' ||' '||  e.ic_usuario_bloqueo  ||' '|| TO_CHAR(df_bloqueo, 'dd/mm/yyyy hh24:mi:ss' ) , 'S' ,'')  OBSERVACIONES ," +
			" e.cs_habilitado AS  CS_HABILITADO, "+ 					
			" promotor.cg_descripcion AS LIDER_PROMOTOR  "+ // F021-2015
			" FROM com_domicilio dom,  "+
			" comcat_epo e,  "+
			" comcat_estado edo,  "+
			" comrel_nafin n ,  "+
			" comcat_banco_fondeo fon, "+
			" comcat_lider_promotor promotor "+
			" WHERE dom.ic_epo=e.ic_epo AND e.ic_epo=n.ic_epo_pyme_if "+ 
			" AND dom.ic_estado=edo.ic_estado AND n.cg_tipo='E' "+
			" AND e.ic_banco_fondeo = fon.ic_banco_fondeo   "+
			" AND e.CS_INTERNACIONAL = 'N'   "+
			" AND dom.cs_fiscal='S' ");
			
			if(!txtNombre.equals("")){
				qrySentencia.append(" AND (upper(trim(e.cg_razon_social)) LIKE upper(?))") ;
				conditions.add(txtNombre+"%");
			}
			
			if(!sel_edo.equals("")){
				qrySentencia.append(" AND dom.ic_estado = ? ");
				conditions.add(txtNombre);
			}	

			if (!txtNumElectronico.equals("")){
				qrySentencia.append(" AND n.ic_nafin_electronico = ? ");
				conditions.add(txtNumElectronico);
			}
	
			if(convenio_unico.equals("S")){
				qrySentencia.append(" AND e.cs_convenio_unico = ? ");
				conditions.add(convenio_unico);
			}
			
			qrySentencia.append( // F021 - 2015
				" AND e.ic_lider_promotor = promotor.ic_lider_promotor(+) "
			);
			
			qrySentencia.append(" order by  e.ic_epo  asc  ");
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
			pdfDoc.setTable(10,100);
			pdfDoc.setCell("N�mero de Nafin Electr�nico ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("IC_EPO ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre o Raz�n Social ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Domicilio ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estado ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tel�fono ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Banco Fondeo ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("L�der Promotor ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Convenio �nico ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Observaciones ","celda01",ComunesPDF.CENTER);
			
			
			while (rs.next())	{					
				String num_nafin = (rs.getString("NUM_NAFIN_ELEC") == null) ? "" : rs.getString("NUM_NAFIN_ELEC");
				String ic_epo = (rs.getString("IC_EPO") == null) ? "" : rs.getString("IC_EPO");
				String nombreEPO = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
				String domicilio = (rs.getString("DOMICILIO") == null) ? "" : rs.getString("DOMICILIO");
				String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				String bancoFondeo = (rs.getString("BANCO_FODEO") == null) ? "" : rs.getString("BANCO_FODEO");
				String liderPromotor = (rs.getString("LIDER_PROMOTOR") == null) ? "" : rs.getString("LIDER_PROMOTOR");
				String convenioUnico = (rs.getString("CONVENIO_UNICO") == null) ? "" : rs.getString("CONVENIO_UNICO");
				String Observaciones = (rs.getString("OBSERVACIONES") == null) ? "" : rs.getString("OBSERVACIONES");
								
				pdfDoc.setCell(num_nafin,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ic_epo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombreEPO,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(domicilio,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(estado,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(bancoFondeo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(liderPromotor,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(convenioUnico,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Observaciones,"formas",ComunesPDF.LEFT);
			}
		
			pdfDoc.addTable();
			pdfDoc.endDocument();	
				
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();		
			contenidoArchivo.append(" N�mero de Nafin Electr�nico, IC_EPO, Nombre o Raz�n Social, Domicilio , Estado,  "+
											" Tel�fono, Banco Fondeo, L�der Promotor, Convenio �nico , Observaciones  \n");			
		
			while (rs.next())	{	
			
				String num_nafin = (rs.getString("NUM_NAFIN_ELEC") == null) ? "" : rs.getString("NUM_NAFIN_ELEC");
				String ic_epo = (rs.getString("IC_EPO") == null) ? "" : rs.getString("IC_EPO");
				String nombreEPO = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
				String domicilio = (rs.getString("DOMICILIO") == null) ? "" : rs.getString("DOMICILIO");
				String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				String bancoFondeo = (rs.getString("BANCO_FODEO") == null) ? "" : rs.getString("BANCO_FODEO");
				String liderPromotor = (rs.getString("LIDER_PROMOTOR") == null) ? "" : rs.getString("LIDER_PROMOTOR");
				String convenioUnico = (rs.getString("CONVENIO_UNICO") == null) ? "" : rs.getString("CONVENIO_UNICO");
				String Observaciones = (rs.getString("OBSERVACIONES") == null) ? "" : rs.getString("OBSERVACIONES");
			
			
				contenidoArchivo.append(num_nafin.replace(',',' ')+", "+
											ic_epo.replace(',',' ')+", "+
											nombreEPO.replace(',',' ')+", "+
											domicilio.replace(',',' ')+", "+
											estado.replace(',',' ')+", "+
											telefono.replace(',',' ')+", "+
											bancoFondeo.replace(',',' ')+", "+
											liderPromotor.replace(',',' ')+", "+
											convenioUnico.replace(',',' ')+", "+				
											Observaciones.replace(',',' ')+"\n");		
			
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	public String imprimirCuentas(HttpServletRequest request, List cuentas, String tituloF,  String tituloC, String path ) {  
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		try {
				
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText(tituloF,"formas",ComunesPDF.CENTER);
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			
			pdfDoc.setTable(1,30);			
			pdfDoc.setCell(tituloC, "formas",ComunesPDF.LEFT);  
			pdfDoc.setCell("Login del Usuario: ", "celda02",ComunesPDF.CENTER);	
			  
			
			Iterator itCuentas = cuentas.iterator();	
			while (itCuentas.hasNext()) {
				String cuenta = (String) itCuentas.next();		
				pdfDoc.setCell(cuenta, "formas",ComunesPDF.CENTER);	
			}	
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
			
		 
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getTxtNumElectronico() {
		return txtNumElectronico;
	}

	public void setTxtNumElectronico(String txtNumElectronico) {
		this.txtNumElectronico = txtNumElectronico;
	}

	public String getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(String txtNombre) {
		this.txtNombre = txtNombre;
	}

	public String getSel_edo() {
		return sel_edo;
	}

	public void setSel_edo(String sel_edo) {
		this.sel_edo = sel_edo;
	}

	public String getConvenio_unico() {
		return convenio_unico;
	}

	public void setConvenio_unico(String convenio_unico) {
		this.convenio_unico = convenio_unico;
	}

		

}