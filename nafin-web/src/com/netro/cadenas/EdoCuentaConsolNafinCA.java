package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

public class EdoCuentaConsolNafinCA implements IQueryGeneratorReg,IQueryGeneratorRegExtJS {

	StringBuffer 	qrySentencia;
	private List 	conditions;
	private String 	operacion;
	private String 	paginaOffset;
	private String 	paginaNo;
	
	private String 	txt_cliente;
	private String 	txt_num_prestamo;
	private String nombreCliente;
	
	//Constructor
	public EdoCuentaConsolNafinCA(){}
	
	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(
			" SELECT	 /*+index(est ");
			if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
				qrySentencia.append("in_com_estado_cuenta_diario_02 ");
			}
			else if(txt_cliente!=null&&!"".equals(txt_cliente)){
				qrySentencia.append("in_com_estado_cuenta_diario_03 ");
			}
		qrySentencia.append(" ) use_nl(est tip)*/ " +
			" 		  est.ic_moneda, mon.cd_nombre nombre_moneda, COUNT (1) registros,"   +
			" 		  SUM (est.fn_monto_inicial) monto_inicial,"   +
			" 		  SUM (est.fn_capital_vigente) capital_vigente,"   +
			" 		  SUM (est.fn_capital_vencido) capital_vencido,"   +
			" 		  SUM (est.fn_interes_vigente) interes_vigente,"   +
			" 		  SUM (est.fn_interes_vencido) interes_vencido,"   +
			" 		  SUM (est.fn_mora100) interes_moratorio,"   +
			" 		  SUM (est.fn_saldo_insoluto+est.fn_interes_vigente+est.fn_interes_vencido+est.fn_mora100) saldo_insoluto,"   +
			" 		'EdoCuentaConsolNafinCA::getAggregateCalculationQuery' origen"   +
			" 	FROM com_estado_cuenta_diario est,"   +
			" 		 comcat_tipo_credito tip,"   +
			" 		 comcat_moneda mon"   +
			"    WHERE est.ig_codigo_sub_aplicacion = tip.ic_tipo_credito"   +
			" 	 AND est.ic_moneda = mon.ic_moneda"   +
			"      AND tip.cs_info_pago = ?");
		conditions.add("S");
		if(txt_cliente!=null&&!"".equals(txt_cliente)) {
			qrySentencia.append(" AND est.ig_codigo_cliente = ? ");
			conditions.add(txt_cliente);
		}
		if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
			qrySentencia.append(" AND est.ig_numero_prestamo = ? ");
			conditions.add(txt_num_prestamo);
		}
		qrySentencia.append(" GROUP BY est.ic_moneda, mon.cd_nombre ");
System.out.println("qrySentencia: "+qrySentencia);
System.out.println("conditions: "+conditions);
		return qrySentencia.toString();
	}//getAggregateCalculationQuery
	
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(
			"SELECT /*+index(est ");
			if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
				qrySentencia.append("in_com_estado_cuenta_diario_02 ");
			}
			else if(txt_cliente!=null&&!"".equals(txt_cliente)){
				qrySentencia.append("in_com_estado_cuenta_diario_03 ");
			}
		qrySentencia.append(" ) use_nl(est tip)*/ " +
			"       est.ig_codigo_empresa, est.ic_codigo_agencia, est.ig_codigo_sub_aplicacion, est.ig_numero_prestamo, " +
			"       'ConsPrestamosNafinCA::getDocumentQuery' origen " +
			"  FROM com_estado_cuenta_diario est, comcat_tipo_credito tip " +
			" WHERE est.ig_codigo_sub_aplicacion = tip.ic_tipo_credito AND tip.cs_info_pago = ? ");
		conditions.add("S");
		if(txt_cliente!=null&&!"".equals(txt_cliente)) {
			qrySentencia.append(" AND est.ig_codigo_cliente = ? ");
			conditions.add(txt_cliente);
		}
		if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
			qrySentencia.append(" AND est.ig_numero_prestamo = ? ");
			conditions.add(new Integer(txt_num_prestamo));
		}
		qrySentencia.append(" ORDER BY est.ig_codigo_cliente, est.df_fecha_vencimiento ");
System.out.println("qrySentencia: "+qrySentencia);
System.out.println("conditions: "+conditions);
		return qrySentencia.toString();
	}//getDocumentQuery
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(
			" SELECT /*+index(est ");
			if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
				qrySentencia.append("in_com_estado_cuenta_diario_02 ");
			}
			else if(txt_cliente!=null&&!"".equals(txt_cliente)){
				qrySentencia.append("in_com_estado_cuenta_diario_03 ");
			}
		qrySentencia.append(" ) use_nl(est tip)*/ " +
			"         est.ic_estado_cuenta, est.ig_codigo_cliente, est.cg_desc_cliente,"   +
			"         est.ig_numero_prestamo, est.cg_desc_producto_banco,"   +
			"         TO_CHAR (est.df_fecha_apertura, 'dd/mm/yyyy') df_fecha_apertura,"   +
			"         TO_CHAR (est.df_fecha_vencimiento, 'dd/mm/yyyy') df_fecha_vencimiento,"   +
			"         est.fn_monto_inicial, est.fn_capital_vigente, est.fn_capital_vencido,"   +
			"         est.fn_interes_vigente, est.fn_interes_vencido, est.fn_mora100,"   +  // est.fn_moratorios,"   +
			"         est.cg_desc_valor_tasa_cartera, (est.fn_saldo_insoluto + est.fn_interes_vigente + est.fn_interes_vencido + est.fn_mora100 ) as fn_saldo_insoluto, "   +
			"         est.fn_tasa_total, 'ConsPrestamosNafinCA::getDocumentSummaryQueryForIds' origen"   +
			"   FROM com_estado_cuenta_diario est"   +
			"  WHERE ");
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
				if(i>0) {
					qrySentencia.append("  OR  "); 
				}
				qrySentencia.append("  (est.ig_codigo_empresa = ? AND est.ic_codigo_agencia = ? "); 
				qrySentencia.append("   AND est.ig_codigo_sub_aplicacion = ? AND est.ig_numero_prestamo = ?) "); 
				conditions.add(new Long(lItem.get(0).toString()));
				conditions.add(new Long(lItem.get(1).toString()));
				conditions.add(new Long(lItem.get(2).toString()));
				conditions.add(new Long(lItem.get(3).toString()));
		}//for(int i=0;i<ids.size();i++)
		qrySentencia.append(" ORDER BY est.ig_codigo_cliente, est.df_fecha_vencimiento ");
System.out.println("qrySentencia: "+qrySentencia);
System.out.println("conditions: "+conditions);
		return qrySentencia.toString();
	}//getDocumentSummaryQueryForIds
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		String nombreArchivo = "";
		String fechaHoy		="";
		String numeroSIRAC	= "";		
		String RFC				= "";
		String calle			= "";
		String colonia			= "";
		String municipio		= "";
		String ciudad			= "";
		String codigoPostal	= "";
		String clavePyme		= "";
		String numeroPrestamo	= "";
		double MontoOpTot		= 0;
		double CapVigTot		= 0;
		double CapVenTot		= 0;
		double IntVigTot		= 0;
		double IntVenTot		= 0;
		double IntMorTot		= 0;
		double AdTot			= 0;
		HashMap direccionCliente = new HashMap();
		StringBuffer mensaje;
		float widths[]; 
		
		try{
			fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		}catch(Exception e){
			fechaHoy = "";
		}
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				numeroPrestamo	= (request.getParameter("ig_numero_prestamo")!=null)?request.getParameter("ig_numero_prestamo"):"";
				clavePyme = (String) request.getSession().getAttribute("iNoCliente");
				
				EstadoCuentaPyme estadoCuentaPyme = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB", EstadoCuentaPyme.class);
	
				String tipoUsuario = (String) request.getSession().getAttribute("strTipoUsuario");
				if(tipoUsuario.equals("PYME")) {
					numeroSIRAC = estadoCuentaPyme.getNumeroSIRAC(clavePyme);
					direccionCliente = estadoCuentaPyme.getDireccionCliente(numeroSIRAC);
					nombreCliente = estadoCuentaPyme.getNombreCliente(numeroSIRAC,numeroPrestamo);
				}
				RFC = (((String)direccionCliente.get("RFC"))!=null)?((String)direccionCliente.get("RFC")):"";
				calle = (((String)direccionCliente.get("CALLE"))!=null)?((String)direccionCliente.get("CALLE")):"";
				colonia = (((String)direccionCliente.get("COLONIA"))!=null)?((String)direccionCliente.get("COLONIA")):"";
				municipio = (((String)direccionCliente.get("MUNICIPIO"))!=null)?((String)direccionCliente.get("MUNICIPIO")):"";
				ciudad = (((String)direccionCliente.get("CIUDAD"))!=null)?((String)direccionCliente.get("CIUDAD")):"";
				codigoPostal = (((String)direccionCliente.get("CODIGO_POSTAL"))!=null)?((String)direccionCliente.get("CODIGO_POSTAL")):"";
				
				widths = new float [] {10.0f,50.0f,40.0f};
				pdfDoc.setTable(3,100,widths);
				pdfDoc.setCell("Cliente:\n" + numeroSIRAC,"formasrepB",ComunesPDF.LEFT);
				pdfDoc.setCell("Nombre:\n" + nombreCliente,"formasrepB",ComunesPDF.LEFT);
				pdfDoc.setCell("RFC:\n" + RFC,"formasrepB",ComunesPDF.LEFT);
				
				mensaje = new StringBuffer();
				
				mensaje.append(
					"Importante: Los montos y cifras que en esta pantalla aparecen son una mera indicaci�n del estado de cuenta "+
					"que le presenta Nacional Financiera, sin que deban considerarse como definitivas, debido a que puede haber "+
					"montos correspondientes a capital, intereses, comisiones, gastos, abonos, etc., que pueden no estar reflejados "+
					"en las cifras que se presentan en esta pantalla, por lo que al realizar el pago por las cantidades que ah� se indican "+
					"no los libera de otros adeudos.");
				
				pdfDoc.addText("Estado de Cuenta Consolidado al "+ fechaHoy,"formasG",ComunesPDF.CENTER);
				pdfDoc.addText(mensaje.toString(),"formasrep",ComunesPDF.JUSTIFIED);
				pdfDoc.addTable();
				
				widths = new float [] {60.0f,40.0f};
				pdfDoc.setTable(2,100);
				pdfDoc.setCell("Direcci�n:\n"+
									"Calle/N�mero: "+ calle + "\n" +
									"Colonia: " + colonia + "\n" +
									"Municipio/Delegaci�n/Estado: " + municipio + ", "+ciudad + "\n" +
									"C�digo Postal: " + codigoPostal,"formasrepB",ComunesPDF.LEFT);
				pdfDoc.setCell("Fecha Emisi�n:   " + fechaHoy,"formasrepB",ComunesPDF.LEFT);
				
				pdfDoc.addTable();
				
				pdfDoc.setTable(13, 100);
				pdfDoc.setCell("No. Pr�stamo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Descripci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Operado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Capital Vigente","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Capital Vencido","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Inter�s Vigente","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Inter�s Vencido","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Inter�s Moratorio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Tasa","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa Total","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Adeudo total al"+diaActual+"/"+mesActual+"/"+anioActual,"celda01",ComunesPDF.CENTER);
				
				while (rs.next()) {
					String numPrestamo = (rs.getString("IG_NUMERO_PRESTAMO") == null) ? "" : rs.getString("IG_NUMERO_PRESTAMO");
					String descripcion  = (rs.getString("CG_DESC_PRODUCTO_BANCO") == null) ? "" : rs.getString("CG_DESC_PRODUCTO_BANCO");
					String fechaOperacion = (rs.getString("DF_FECHA_APERTURA") == null) ? "" : rs.getString("DF_FECHA_APERTURA");
					String fechaVencimiento  = (rs.getString("DF_FECHA_VENCIMIENTO") == null) ? "" : rs.getString("DF_FECHA_VENCIMIENTO");
					double montoOperado = Double.parseDouble((rs.getString("FN_MONTO_INICIAL") == null) ? "" : rs.getString("FN_MONTO_INICIAL"));
					double capitalVigente = Double.parseDouble((rs.getString("FN_CAPITAL_VIGENTE") == null) ? "" : rs.getString("FN_CAPITAL_VIGENTE"));
					double capitalVencido = Double.parseDouble((rs.getString("FN_CAPITAL_VENCIDO") == null) ? "" : rs.getString("FN_CAPITAL_VENCIDO"));
					double interesVigente = Double.parseDouble((rs.getString("FN_INTERES_VIGENTE") == null) ? "" : rs.getString("FN_INTERES_VIGENTE"));
					double interesVencido = Double.parseDouble((rs.getString("FN_INTERES_VENCIDO") == null) ? "" : rs.getString("FN_INTERES_VENCIDO"));
					double interesMoratorio = Double.parseDouble((rs.getString("FN_MORA100") == null) ? "" : rs.getString("FN_MORA100"));
					String tipoTasa = (rs.getString("CG_DESC_VALOR_TASA_CARTERA") == null) ? "" : rs.getString("CG_DESC_VALOR_TASA_CARTERA");
					String tasaTotal = (rs.getString("FN_TASA_TOTAL") == null) ? "" : rs.getString("FN_TASA_TOTAL");
					double adeudoTotal = Double.parseDouble((rs.getString("FN_SALDO_INSOLUTO") == null) ? "" : rs.getString("FN_SALDO_INSOLUTO"));
					
					pdfDoc.setCell(numPrestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(descripcion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaOperacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoOperado,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(capitalVigente,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(capitalVencido,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(interesVigente,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(interesVencido,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(interesMoratorio,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(tasaTotal+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(adeudoTotal,2),"formas",ComunesPDF.RIGHT);
					
					MontoOpTot		+= montoOperado;
					CapVigTot		+= capitalVigente;
					CapVenTot		+= capitalVencido;
					IntVigTot		+= interesVigente;
					IntVenTot		+= interesVencido;
					IntMorTot		+= interesMoratorio;
					AdTot				+= adeudoTotal;
				}
				pdfDoc.addTable();
				
				pdfDoc.setTable(13,100);
				pdfDoc.setCell("TOTALES","celda01",ComunesPDF.CENTER,4);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(MontoOpTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(CapVigTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(CapVenTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(IntVigTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(IntVenTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(IntMorTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);	
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);	
				pdfDoc.setCell("$" + Comunes.formatoDecimal(AdTot,2),"formasrep",ComunesPDF.RIGHT);
				
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}
		return nombreArchivo;
	}
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path,String tipo) {
		String nombreArchivo = "";
		String fechaHoy		="";
		String numeroSIRAC	= "";		
		String RFC				= "";
		String calle			= "";
		String colonia			= "";
		String municipio		= "";
		String ciudad			= "";
		String codigoPostal	= "";
		String clavePyme		= "";
		String numeroPrestamo	= "";
		double MontoOpTot		= 0;
		double CapVigTot		= 0;
		double CapVenTot		= 0;
		double IntVigTot		= 0;
		double IntVenTot		= 0;
		double IntMorTot		= 0;
		double AdTot			= 0;
		HashMap direccionCliente = new HashMap();
		StringBuffer mensaje;
		float widths[]; 
		
		try{
			fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		}catch(Exception e){
			fechaHoy = "";
		}
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			
			
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				numeroPrestamo	= (request.getParameter("ig_numero_prestamo")!=null)?request.getParameter("ig_numero_prestamo"):"";
				clavePyme = (String) request.getSession().getAttribute("iNoCliente");
				
				EstadoCuentaPyme estadoCuentaPyme = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB", EstadoCuentaPyme.class);
	
				String tipoUsuario = (String) request.getSession().getAttribute("strTipoUsuario");
				if(tipoUsuario.equals("PYME")) {
					numeroSIRAC = estadoCuentaPyme.getNumeroSIRAC(clavePyme);
					direccionCliente = estadoCuentaPyme.getDireccionCliente(numeroSIRAC);
					nombreCliente = estadoCuentaPyme.getNombreCliente(numeroSIRAC,numeroPrestamo);
				
				}else if(tipoUsuario.equals("NAFIN")) {
				
					direccionCliente = estadoCuentaPyme.getDireccionCliente(txt_cliente);
					numeroSIRAC = txt_cliente;					
				}	
				
				RFC = (((String)direccionCliente.get("RFC"))!=null)?((String)direccionCliente.get("RFC")):"";
				calle = (((String)direccionCliente.get("CALLE"))!=null)?((String)direccionCliente.get("CALLE")):"";
				colonia = (((String)direccionCliente.get("COLONIA"))!=null)?((String)direccionCliente.get("COLONIA")):"";
				municipio = (((String)direccionCliente.get("MUNICIPIO"))!=null)?((String)direccionCliente.get("MUNICIPIO")):"";
				ciudad = (((String)direccionCliente.get("CIUDAD"))!=null)?((String)direccionCliente.get("CIUDAD")):"";
				codigoPostal = (((String)direccionCliente.get("CODIGO_POSTAL"))!=null)?((String)direccionCliente.get("CODIGO_POSTAL")):"";
				
				widths = new float [] {10.0f,50.0f,40.0f};
				pdfDoc.setTable(3,100,widths);
				pdfDoc.setCell("Cliente:\n" + numeroSIRAC,"formasrepB",ComunesPDF.LEFT);
				pdfDoc.setCell("Nombre:\n" + nombreCliente,"formasrepB",ComunesPDF.LEFT);
				pdfDoc.setCell("RFC:\n" + RFC,"formasrepB",ComunesPDF.LEFT);
				
				mensaje = new StringBuffer();
				
				mensaje.append(
					"Importante: Los montos y cifras que en esta pantalla aparecen son una mera indicaci�n del estado de cuenta "+
					"que le presenta Nacional Financiera, sin que deban considerarse como definitivas, debido a que puede haber "+
					"montos correspondientes a capital, intereses, comisiones, gastos, abonos, etc., que pueden no estar reflejados "+
					"en las cifras que se presentan en esta pantalla, por lo que al realizar el pago por las cantidades que ah� se indican "+
					"no los libera de otros adeudos.");
				
				pdfDoc.addText("Estado de Cuenta Consolidado al "+ fechaHoy,"formasG",ComunesPDF.CENTER);
				pdfDoc.addText(mensaje.toString(),"formasrep",ComunesPDF.JUSTIFIED);
				pdfDoc.addTable();
				
				widths = new float [] {60.0f,40.0f};
				pdfDoc.setTable(2,100);
				pdfDoc.setCell("Direcci�n:\n"+
									"Calle/N�mero: "+ calle + "\n" +
									"Colonia: " + colonia + "\n" +
									"Municipio/Delegaci�n/Estado: " + municipio + ", "+ciudad + "\n" +
									"C�digo Postal: " + codigoPostal,"formasrepB",ComunesPDF.LEFT);
				pdfDoc.setCell("Fecha Emisi�n:  " + fechaHoy,"formasrepB",ComunesPDF.LEFT);
				
				pdfDoc.addTable();
				
				pdfDoc.setTable(13, 100);
				pdfDoc.setCell("No. Pr�stamo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Descripci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Operado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Capital Vigente","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Capital Vencido","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Inter�s Vigente","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Inter�s Vencido","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Inter�s Moratorio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Tasa","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa Total","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Adeudo total al"+diaActual+"/"+mesActual+"/"+anioActual,"celda01",ComunesPDF.CENTER);				
						
				while (reg.next()) {
					String numPrestamo = (reg.getString("IG_NUMERO_PRESTAMO") == null) ? "" : reg.getString("IG_NUMERO_PRESTAMO");
					String descripcion  = (reg.getString("CG_DESC_PRODUCTO_BANCO") == null) ? "" : reg.getString("CG_DESC_PRODUCTO_BANCO");
					String fechaOperacion = (reg.getString("DF_FECHA_APERTURA") == null) ? "" : reg.getString("DF_FECHA_APERTURA");
					String fechaVencimiento  = (reg.getString("DF_FECHA_VENCIMIENTO") == null) ? "" : reg.getString("DF_FECHA_VENCIMIENTO");
					double montoOperado = Double.parseDouble((reg.getString("FN_MONTO_INICIAL") == null) ? "0" : reg.getString("FN_MONTO_INICIAL"));
					double capitalVigente = Double.parseDouble((reg.getString("FN_CAPITAL_VIGENTE") == null) ? "0" : reg.getString("FN_CAPITAL_VIGENTE"));
					double capitalVencido = Double.parseDouble((reg.getString("FN_CAPITAL_VENCIDO") == null) ? "0" : reg.getString("FN_CAPITAL_VENCIDO"));
					double interesVigente = Double.parseDouble((reg.getString("FN_INTERES_VIGENTE") == null) ? "0" : reg.getString("FN_INTERES_VIGENTE"));
					double interesVencido = Double.parseDouble((reg.getString("FN_INTERES_VENCIDO") == null) ? "0" : reg.getString("FN_INTERES_VENCIDO"));
					double interesMoratorio = Double.parseDouble((reg.getString("FN_MORA100") == null) ? "0" : reg.getString("FN_MORA100"));
					String tipoTasa = (reg.getString("CG_DESC_VALOR_TASA_CARTERA") == null) ? "" : reg.getString("CG_DESC_VALOR_TASA_CARTERA");
					String tasaTotal = (reg.getString("FN_TASA_TOTAL") == null) ? "" : reg.getString("FN_TASA_TOTAL");
					double adeudoTotal = Double.parseDouble((reg.getString("FN_SALDO_INSOLUTO") == null) ? "0" : reg.getString("FN_SALDO_INSOLUTO"));
					
					pdfDoc.setCell(numPrestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(descripcion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaOperacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoOperado,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(capitalVigente,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(capitalVencido,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(interesVigente,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(interesVencido,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(interesMoratorio,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(tasaTotal+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(adeudoTotal,2),"formas",ComunesPDF.RIGHT);
					
					MontoOpTot		+= montoOperado;
					CapVigTot		+= capitalVigente;
					CapVenTot		+= capitalVencido;
					IntVigTot		+= interesVigente;
					IntVenTot		+= interesVencido;
					IntMorTot		+= interesMoratorio;
					AdTot				+= adeudoTotal;
				}
				pdfDoc.addTable();
				
				pdfDoc.setTable(13,100);
				pdfDoc.setCell("TOTALES","celda01",ComunesPDF.CENTER,4);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(MontoOpTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(CapVigTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(CapVenTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(IntVigTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(IntVenTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(IntMorTot,2),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);	
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);	
				pdfDoc.setCell("$" + Comunes.formatoDecimal(AdTot,2),"formasrep",ComunesPDF.RIGHT);
				
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
				} catch(Exception e) {}
			}
		}
		return nombreArchivo;
	}
	public String getDocumentQueryFile() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(
			" SELECT /*+index(est ");
			if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
				qrySentencia.append("in_com_estado_cuenta_diario_02 ");
			}
			else if(txt_cliente!=null&&!"".equals(txt_cliente)){
				qrySentencia.append("in_com_estado_cuenta_diario_03 ");
			}
		qrySentencia.append(" ) use_nl(est tip)*/ " +
			"         est.ic_estado_cuenta, est.ig_codigo_cliente, est.cg_desc_cliente,"   +
			"         est.ig_numero_prestamo, est.cg_desc_producto_banco,"   +
			"         TO_CHAR (est.df_fecha_apertura, 'dd/mm/yyyy') df_fecha_apertura,"   +
			"         TO_CHAR (est.df_fecha_vencimiento, 'dd/mm/yyyy') df_fecha_vencimiento,"   +
			"         est.fn_monto_inicial, est.fn_capital_vigente, est.fn_capital_vencido,"   +
			"         est.fn_interes_vigente, est.fn_interes_vencido, est.fn_mora100,"   +  // est.fn_moratorios,"   +
			"         est.cg_desc_valor_tasa_cartera, (est.fn_saldo_insoluto + est.fn_interes_vigente + est.fn_interes_vencido + est.fn_mora100 ) as fn_saldo_insoluto, "   +
			"         est.fn_tasa_total, 'ConsPrestamosNafinCA::getDocumentSummaryQueryForIds' origen"   +
			"  FROM com_estado_cuenta_diario est, comcat_tipo_credito tip "   +
			"	WHERE est.ig_codigo_sub_aplicacion = tip.ic_tipo_credito AND tip.cs_info_pago = ? ");
		conditions.add("S");
		if(txt_cliente!=null&&!"".equals(txt_cliente)) {
			qrySentencia.append(" AND est.ig_codigo_cliente = ? ");
			conditions.add(new Integer(txt_cliente));
		}
		if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
			qrySentencia.append(" AND est.ig_numero_prestamo = ? ");
			conditions.add(new Integer(txt_num_prestamo));
		}
		qrySentencia.append(" ORDER BY est.ig_codigo_cliente, est.df_fecha_vencimiento ");
		System.out.println("qrySentencia: "+qrySentencia);
		System.out.println("conditions: "+conditions);
		return qrySentencia.toString();
	}//getDocumentQueryFile
	
	//GETERS
	public List getConditions() {
		return conditions;
	}
	public String getOperacion() {
		return operacion;
	}
	public String getPaginaNo() {
		return paginaNo;
	}
	public String getPaginaOffset() {
		return paginaOffset;
	}
	public String getTxt_cliente() {
		return txt_cliente;
	}
	public String getTxt_num_prestamo() {
		return txt_num_prestamo;
	}
		
	//SETERS
	public void setOperacion(String newOperacion) {
		operacion = newOperacion;
	}
	public void setPaginaNo(String newPaginaNo) {
		paginaNo = newPaginaNo;
	}
	public void setPaginaOffset(String newPaginaOffset) {
		paginaOffset = newPaginaOffset;
	}
	public void setTxt_cliente(String newTxt_cliente) {
		txt_cliente = newTxt_cliente;
	}
	public void setTxt_num_prestamo(String newTxt_num_prestamo) {
		txt_num_prestamo = newTxt_num_prestamo;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
			
}//EdoCuentaConsolNafinCA