package com.netro.cadenas;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsSinNumProv implements  IQueryGeneratorRegExtJS {
   
	public ConsSinNumProv() {  }

	private List 	conditions;
	StringBuffer 	strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsSinNumProv.class);//Variable para enviar mensajes al log.
	private String txtCadProductiva;
	private String num_electronico;
	private String fec_ini;
	private String fec_fin;
	private String convenio_unico;
  
	
	public String getAggregateCalculationQuery() {
		return "";
 	}  
		 
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();   
		strQuery 		= new StringBuffer(); 
		
		strQuery.append("SELECT /*+ use_nl(t7, t2,t5,t8,t6,t1,t3)  */ " +
			"       t5.ic_epo as NUM_EPO,"+
			"				t1.ic_pyme as NUM_PYME, " +
			"          DECODE (t5.cs_habilitado, " +
			"                  'N', '<font color=\"Blue\">*</font>', " +
			"                  'S', ' ' " +
			"                 ) " +
			"       || ' ' " +
			"       || DECODE (t2.cs_tipo_persona, " +
			"                  'F', t2.cg_nombre || ' ' || t2.cg_appat || ' ' " +
			"                   || t2.cg_apmat, " +
			"                  t2.cg_razon_social " +
			"                 ) AS NOMBRE_PROVEEDOR, " +
			"       t5.cg_pyme_epo_interno AS NUM_PROVEEDOR, " +
			"       t6.ic_nafin_electronico AS NUM_ELECTRONICO, " +
			"       t2.cg_rfc RFC, "+
			"				to_char(t2.df_alta,'DD/MM/YYYY') as FECHA_AFILIACION, " +
			"				TO_CHAR (SYSDATE, 'DD/MM/YYYY')  as FECHA_ALTA, "+
			"				'N'  as SELECCIONADO, "+
			" DECODE( t2.cs_convenio_unico,'S','SI','N','NO')  AS  CONVENIO_UNICO ,"+
			"       '15clientes/15forma5.jsp' AS pagina " +
			"  FROM comcat_epo t7, " +
			"       comcat_pyme t2, " +
			"       comrel_pyme_epo t5, " +
			"       comcat_version_convenio t8, " +
			"       comrel_nafin t6, " +
			"       com_domicilio t1, " +
			"       comcat_estado t3 " +
			" WHERE t5.ic_pyme = t2.ic_pyme " +
			"   AND t1.ic_pyme = t2.ic_pyme " +
			"   AND t2.ic_pyme = t6.ic_epo_pyme_if " +
			"   AND t1.ic_estado = t3.ic_estado " +
			"   AND t5.ic_epo = t7.ic_epo " +
			"   AND t2.ic_version_convenio = t8.ic_version_convenio(+) " +
			"   AND t2.ic_tipo_cliente IN (1, 4) " +
			"   AND t6.cg_tipo = 'P' " +
			"   AND t2.cs_internacional in ('S','N') " +
			"   AND t5.cg_pyme_epo_interno IS NULL " +
			"   AND t5.cs_num_proveedor = 'S' " +
			"   AND (t2.cs_invalido IS NULL OR t2.cs_invalido != 'S') " +
			"   AND t1.cs_fiscal = 'S' ");
			
		if(!txtCadProductiva.equals("")){
				strQuery.append(" AND t5.ic_epo = ?");
				conditions.add(txtCadProductiva);
		}		
		if(!num_electronico.equals("")){
				strQuery.append(" AND t6.ic_nafin_electronico = ? ");
				conditions.add(num_electronico);
		}
		if(!fec_ini.equals("") && !fec_fin.equals("")) {
			strQuery.append(" AND t2.df_alta >= trunc(to_date('"+fec_ini+"','DD/MM/YYYY') )");
			strQuery.append(" AND t2.df_alta < trunc(to_date('"+fec_fin+"','DD/MM/YYYY') +1)");
		}
	
		if(!convenio_unico.equals("")){
			strQuery.append(" AND t2.cs_convenio_unico =  ? " );
			conditions.add(convenio_unico);
		}
		
			log.debug("getDocumentQueryFile "+strQuery.toString());
			log.debug("getDocumentQueryFile)"+conditions);
		 
		return strQuery.toString();
		
 	} 		
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		
		

		return nombreArchivo;
	}
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	
	public List getConditions() {
		return conditions;
	}

	public String getTxtCadProductiva() {
		return txtCadProductiva;
	}

	public void setTxtCadProductiva(String txtCadProductiva) {
		this.txtCadProductiva = txtCadProductiva;
	}

	public String getNum_electronico() {
		return num_electronico;
	}

	public void setNum_electronico(String num_electronico) {
		this.num_electronico = num_electronico;
	}

	public String getFec_ini() {
		return fec_ini;
	}

	public void setFec_ini(String fec_ini) {
		this.fec_ini = fec_ini;
	}

	public String getFec_fin() {
		return fec_fin;
	}

	public void setFec_fin(String fec_fin) {
		this.fec_fin = fec_fin;
	}

	public String getConvenio_unico() {
		return convenio_unico;
	}

	public void setConvenio_unico(String convenio_unico) {
		this.convenio_unico = convenio_unico;
	}

 




}