package com.netro.cadenas;

import com.netro.exception.NafinException;

import java.sql.Clob;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Vector;

import netropology.utilerias.AccesoDB;

public class ConImagen  {
	String qry_path = "", qry_cons = "", usuario = "", contenido = "", contenidoAll = "";
	
	
	public ConImagen() {
	}
		
	public ArrayList conImg(String cadena) throws NafinException{
		ResultSet rsCons = null, rsCons1 = null;
		ArrayList a = new ArrayList();
		AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();
			qry_cons = "select distinct(ic_publicacion),cg_nombre,TO_CHAR(df_ini_pub,'DD/MM/YYYY'),TO_CHAR(df_fin_pub,'DD/MM/YYYY'),ci_imagen,cg_archivo from com_publicacion where ic_epo = " + cadena + " and ic_tipo_publicacion = 2 order by ic_publicacion";
			rsCons = con.queryDB(qry_cons);
			Vector vDatos = null;
			while (rsCons.next()) {
				vDatos = new Vector();
				contenido = ".....";
				qry_cons = "SELECT tt_contenido FROM com_publicacion where ic_epo = " + cadena + " and ic_tipo_publicacion = 2 and ic_publicacion = " + rsCons.getString(1);
				rsCons1 = con.queryDB(qry_cons);
				if (rsCons1.next()) {
					Clob clob = rsCons1.getClob(1);
					if (clob != null) {
						contenidoAll = clob.getSubString(1,500);
						contenido = clob.getSubString(1,30) + ".....";
					}
				}
				rsCons1.close();
				con.cierraStatement();
				vDatos.add(0,rsCons.getString(1));
				vDatos.add(1,rsCons.getString(2));
				vDatos.add(2,rsCons.getString(3));
				vDatos.add(3,rsCons.getString(4));
				vDatos.add(4,rsCons.getString(5));
				vDatos.add(5,rsCons.getString(6));
				vDatos.add(6,contenido);
				vDatos.add(7,contenidoAll);
				a.add(vDatos);
			}
			
		} catch (Exception err)	{
			System.out.print("Error al conectarse)" + err );
			throw new NafinException("SIST0001");
		} finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return a;	
	}
	
	public void eliminaImgTxt(String numPub, String arch_actual, String imgn_actual)throws NafinException{
			System.out.println("\nEntre a la funcion de Borrar");
			AccesoDB con = new AccesoDB();
			String qry_exec="";
			
			try{
				con.conexionDB();
				qry_exec = "delete from comrel_publicacion_usuario where ic_publicacion = " + numPub;
				con.ejecutaSQL(qry_exec);
				qry_exec = "delete from com_publicacion where ic_publicacion = " + numPub;
				con.ejecutaSQL(qry_exec);
				
				java.io.File f3 = new java.io.File(arch_actual);
				java.io.File f4 = new java.io.File(imgn_actual);
				if (f3.isFile() && f3.exists()) f3.delete();
				if (f4.isFile() && f4.exists()) f4.delete();
					
			} catch (Exception err)	{
				System.out.print("Error al conectarse" + err );
				throw new NafinException("SIST0001");
			} finally{
				if(con.hayConexionAbierta())
				con.cierraConexionDB();
			}
	}
	
}