package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class BloqueoServicio implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(BloqueoServicio.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	
	private String icIf;
	
	private List pKeys;
	
	public BloqueoServicio() {
	
	}

	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
		qrySentencia.append(
		"SELECT /*use_nl(cpe,ce,cn,xxx)*/ cpe.ic_producto_nafin, cpe.ic_epo,	"+
      "    cn.ic_nafin_electronico, ce.cg_razon_social, ce.cg_rfc,			"+
      "    cpe.cs_bloqueado, bb3.df_fecha, bb3.ic_usuario,						"+
      "    bb3.cg_nombre_usuario, bb3.cg_causa										"+	
		" FROM comrel_producto_epo cpe,  comcat_epo ce, comrel_nafin cn,		"+
      "   (SELECT bbd.ic_epo,																"+
      "           TO_CHAR (bbd.df_fecha, 'DD/MM/YYYY HH24:MM:SS') df_fecha,"+
      "           bbd.ic_usuario, bbd.cg_nombre_usuario, bbd.cg_causa		"+
      "      FROM bit_bloqueo_disp bbd,												"+
      "           (SELECT   MAX (ic_bloqueo_disp) ic_bloqueo_disp, ic_epo	"+
      "                FROM bit_bloqueo_disp bbd									"+
      "               WHERE ic_producto_nafin = 1									"+
      "            GROUP BY ic_epo) bd2												"+
      "     WHERE bbd.ic_bloqueo_disp = bd2.ic_bloqueo_disp						"+
      "       AND bbd.ic_epo = bd2.ic_epo												"+
      "       AND bbd.ic_producto_nafin = 1) bb3									"+
		" WHERE cpe.ic_epo = ce.ic_epo													"+		
		"  AND cpe.ic_epo = cn.ic_epo_pyme_if											"+
		"  AND cpe.ic_epo = bb3.ic_epo(+)												");
		if(icIf.equals("")){
			qrySentencia.append(
			"AND cpe.ic_producto_nafin = 1 "+
			"AND cpe.cg_dispersion = 'S'   "+
			"AND cn.cg_tipo = 'E'			 "+
			"ORDER BY ce.cg_razon_social" );	
		}
		else {
			qrySentencia.append(
			" AND cpe.ic_epo = ?					"+
			" AND cpe.ic_producto_nafin = 1	"+
			" AND cpe.cg_dispersion = 'S'		"+
			" AND cn.cg_tipo = 'E'				"+
			" ORDER BY ce.cg_razon_social		");
			conditions.add(icIf);
		}

		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		qrySentencia.append(
		"SELECT /*use_nl(cpe,ce,cn,xxx)*/  cn.ic_nafin_electronico				"+	
		" FROM comrel_producto_epo cpe,  comcat_epo ce, comrel_nafin cn,		"+
      "   (SELECT bbd.ic_epo,																"+
      "           TO_CHAR (bbd.df_fecha, 'DD/MM/YYYY HH24:MM:SS') df_fecha,"+
      "           bbd.ic_usuario, bbd.cg_nombre_usuario, bbd.cg_causa		"+
      "      FROM bit_bloqueo_disp bbd,												"+
      "           (SELECT   MAX (ic_bloqueo_disp) ic_bloqueo_disp, ic_epo	"+
      "                FROM bit_bloqueo_disp bbd									"+
      "               WHERE ic_producto_nafin = 1									"+
      "            GROUP BY ic_epo) bd2												"+
      "     WHERE bbd.ic_bloqueo_disp = bd2.ic_bloqueo_disp						"+
      "       AND bbd.ic_epo = bd2.ic_epo												"+
      "       AND bbd.ic_producto_nafin = 1) bb3									"+
		" WHERE cpe.ic_epo = ce.ic_epo													"+		
		"  AND cpe.ic_epo = cn.ic_epo_pyme_if											"+
		"  AND cpe.ic_epo = bb3.ic_epo(+)												");
		if(icIf.equals("")){
			qrySentencia.append(
			" AND cpe.ic_producto_nafin = 1"+
			" AND cpe.cg_dispersion = 'S'  "+
			" AND cn.cg_tipo = 'E'			"+
			" ORDER BY ce.cg_razon_social" );	
		}
		else {
			qrySentencia.append(
			" AND cpe.ic_epo = ?					"+
			" AND cpe.ic_producto_nafin = 1	"+
			" AND cpe.cg_dispersion = 'S'		"+
			" AND cn.cg_tipo = 'E'				"+
			" ORDER BY ce.cg_razon_social		");
			conditions.add(icIf);
		}
		
		log.info("getDocumentQuery_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}

	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * datos a mostrar en la consulta basandose en los identificadores unicos 
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista 
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
		qrySentencia.append(
		"SELECT /*use_nl(cpe,ce,cn,xxx)*/ cpe.ic_producto_nafin, cpe.ic_epo,	"+
      "    cn.ic_nafin_electronico, ce.cg_razon_social, ce.cg_rfc,			"+
      "    cpe.cs_bloqueado, bb3.df_fecha, bb3.ic_usuario,						"+
      "    bb3.cg_nombre_usuario, bb3.cg_causa										"+	
		" FROM comrel_producto_epo cpe,  comcat_epo ce, comrel_nafin cn,		"+
      "   (SELECT bbd.ic_epo,																"+
      "           TO_CHAR (bbd.df_fecha, 'DD/MM/YYYY HH24:MM:SS') df_fecha,"+
      "           bbd.ic_usuario, bbd.cg_nombre_usuario, bbd.cg_causa		"+
      "      FROM bit_bloqueo_disp bbd,												"+
      "           (SELECT   MAX (ic_bloqueo_disp) ic_bloqueo_disp, ic_epo	"+
      "                FROM bit_bloqueo_disp bbd									"+
      "               WHERE ic_producto_nafin = 1									"+
      "            GROUP BY ic_epo) bd2												"+
      "     WHERE bbd.ic_bloqueo_disp = bd2.ic_bloqueo_disp						"+
      "       AND bbd.ic_epo = bd2.ic_epo												"+
      "       AND bbd.ic_producto_nafin = 1) bb3									"+
		" WHERE cpe.ic_epo = ce.ic_epo													"+		
		"  AND cpe.ic_epo = cn.ic_epo_pyme_if											"+
		"  AND cpe.ic_epo = bb3.ic_epo(+)												");
		if(icIf.equals("")){
			qrySentencia.append(
			"AND cpe.ic_producto_nafin = 1 "+
			"AND cpe.cg_dispersion = 'S'   "+
			"AND cn.cg_tipo = 'E'			 "+
			"ORDER BY ce.cg_razon_social" );	
		}
		else {
			qrySentencia.append(
			" AND cpe.ic_epo = ?					"+
			" AND cpe.ic_producto_nafin = 1	"+
			" AND cpe.cg_dispersion = 'S'		"+
			" AND cn.cg_tipo = 'E'				"+
			" ORDER BY ce.cg_razon_social		");
			conditions.add(icIf);
		}

		//lista pKeys para almacenar los parametros in de la consulta totales parciales
		/*pKeys = new ArrayList();
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0){ qrySentencia.append(","); }
			qrySentencia.append("? )");
			conditions.add(lItem.get(0));
			pKeys.add(pageIds.get(i));
			
		}*/
		
		log.info("getDocumentSummaryQueryForIds_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}

	
	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQueryFile() {
		return "";	
	}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		return "";
					
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		return "";
	}

	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	//SETTERS Y GETTERS
	public void setIcIf(String icIf) {
		this.icIf = icIf;
	}


	public String getIcIf() {
		return icIf;
	}


	


	public void setPKeys(List pKeys) {
	pKeys = new ArrayList();
		this.pKeys = pKeys;
	}


	public List getPKeys() {
		return pKeys;
	}

}