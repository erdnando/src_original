package com.netro.cadenas;

import netropology.utilerias.*;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import java.util.*;     
import com.netro.pdf.ComunesPDF;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;  
import com.netro.exception.*;
import java.io.FileOutputStream;
import javax.naming.*;
import java.io.*;
import com.netro.dispersion.*;  

public class ConFacturas implements IQueryGeneratorRegExtJS {

  
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConFacturas.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_epo;
	private String ic_if;
	private String txt_nafelec;	
	private String fecha_seleccion_de;
	private String fecha_seleccion_a;
	private String ic_producto_nafin;
	private String ic_pyme;
	private String tipo_factura;
	private String informacion;
	private String cmb_factura;

	
		 
	public ConFacturas() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		String indice ="",  tablas ="", condicion = "";
			
		if("1".equals(ic_producto_nafin)){
			
			if(!"T".equals(cmb_factura) && !"".equals(txt_nafelec)) {
				indice = "index(rel CP_COMREL_NAFIN_PK) index(mun CP_COMCAT_MUNICIPIO_PK) ";
				tablas =  " comrel_nafin rel, ";
				condicion =	"    AND pym.ic_pyme = rel.ic_epo_pyme_if "   +
								"    AND rel.cg_tipo = 'P' "   +
								"    AND rel.ic_nafin_electronico = ? ";
			}
				
			qrySentencia.append(" SELECT   /*+"+indice+"use_nl(dom edo)*/"   +
				" 	 distinct   doc.ic_pyme as IC_PYME ,  " + 
				"  TO_char (s.df_operacion, 'mm/yyyy') AS MES_ANIO " + 				
				"   FROM com_documento doc,"   +
				"        com_docto_seleccionado sel,"   +
				"        com_solicitud s,"   +
				"        comcat_pyme pym,"+tablas+" "+
				"        com_domicilio dom, "   +
				"        comcat_estado edo, "   +
				"        comcat_municipio mun, "+
				"		 comrel_nafin r"+
				"  WHERE doc.ic_documento = sel.ic_documento"   +
				"    AND sel.ic_documento = s.ic_documento"   +
				"    AND doc.ic_pyme = pym.ic_pyme"   +
				"    AND pym.ic_pyme = dom.ic_pyme"   +
				"    AND dom.ic_estado = edo.ic_estado"   +
				"    AND edo.ic_estado = mun.ic_estado"   +
				"    AND dom.ic_municipio = mun.ic_municipio "+
				"    AND s.ig_numero_prestamo IS NOT NULL"   +
				"    AND TRUNC (s.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				"    AND doc.ic_epo = ?"   +
				"    AND doc.ic_if = ?"   +
				"    AND dom.cs_fiscal = 'S' "+condicion+" "+
				"	AND pym.ic_pyme = r.ic_epo_pyme_if "+
				"    AND r.cg_tipo = 'P' " +
				"  GROUP BY doc.ic_pyme,  s.df_operacion   "+
				" order by MES_ANIO  asc ");				
				
		} else  if("4".equals(ic_producto_nafin)){
		
		
			if(!"T".equals(cmb_factura) && !"".equals(txt_nafelec)) {
			tablas =  " comrel_nafin rel, ";
			condicion =
				"    AND pym.ic_pyme = rel.ic_epo_pyme_if "   +
				"    AND rel.cg_tipo = 'P' "   +
				"    AND rel.ic_nafin_electronico = ? ";

			}   
			qrySentencia.append(
			" SELECT   /*+index(doc CP_DIS_DOCUMENTO_PK) index(sol CP_DIS_SOLICITUD_PK) index(mun CP_COMCAT_MUNICIPIO_PK) use_nl(doc sel sol lc pym dom edo mun)*/"   +
			"        doc.ic_pyme as IC_PYME , "+  
			"        TO_char (sol.df_operacion, 'mm/yyyy') AS MES_ANIO   "+
		
			//FODEA 017-2010 VFR fin
			"   FROM dis_documento doc,"   +
			"        dis_docto_seleccionado sel,"   +
			"        dis_solicitud sol,"   +
			"        dis_linea_credito_dm lc,"   +
			"        comcat_pyme pym,"+tablas+" "+
			"        com_domicilio dom,"   +
			"        comcat_estado edo,"   +
			"        comcat_municipio mun,"   +
			"		 comrel_nafin r"+
			"  WHERE doc.ic_documento = sel.ic_documento"   +
			"    AND sel.ic_documento = sol.ic_documento"   +
			"    AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm"   +
			"    AND doc.ic_pyme = pym.ic_pyme"   +
			"    AND pym.ic_pyme = dom.ic_pyme"   +
			"    AND dom.ic_estado = edo.ic_estado"   +
			"    AND edo.ic_estado = mun.ic_estado"   +
			"    AND dom.ic_municipio = mun.ic_municipio"   +
			"    AND sol.ig_numero_prestamo IS NOT NULL"   +
			"    AND TRUNC (sol.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
			"    AND doc.ic_epo = ?"   +
			"    AND lc.ic_if = ?"   +
			"    AND dom.cs_fiscal = 'S' "+condicion+" "+
			"	  AND pym.ic_pyme = r.ic_epo_pyme_if "+
			"    AND r.cg_tipo = 'P' " +
			"  GROUP BY doc.ic_pyme,"   +
			"        sol.df_operacion  "+
			"  order by MES_ANIO  asc ");		
		}		
		conditions.add(fecha_seleccion_de);
		conditions.add(fecha_seleccion_a);
		conditions.add(ic_epo);		
		conditions.add(ic_if);
		if(!"T".equals(cmb_factura) && !"".equals(txt_nafelec)){
			conditions.add(txt_nafelec);   
		}
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * obtiene los registros por pagina
	 * @return 
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		 		
		String indice ="",  tablas ="", condicion = "";
				
		if("1".equals(ic_producto_nafin)){
			
			if(!"T".equals(cmb_factura) && !"".equals(txt_nafelec)) {
				indice = "index(rel CP_COMREL_NAFIN_PK) index(mun CP_COMCAT_MUNICIPIO_PK) ";
				tablas =  " comrel_nafin rel, ";
				condicion =	"    AND pym.ic_pyme = rel.ic_epo_pyme_if "   +
								"    AND rel.cg_tipo = 'P' "   +
								"    AND rel.ic_nafin_electronico = ? ";
			}
				
			qrySentencia.append(" SELECT   /*+"+indice+"use_nl(dom edo)*/"   +
				"      distinct  doc.ic_pyme as IC_PYME , pym.cg_razon_social as  CG_RAZON_SOCIAL,  "+
				"      pym.cg_rfc AS RFC ,"   +				
				"      dom.cg_calle || ' ' ||  dom.cg_numero_ext || ' ' ||  dom.cg_numero_int || ' ' ||  'COL. ' || dom.cg_colonia || ', ' ||  'C.P. ' || dom.cn_cp || ', ' || " +
				"      mun.cd_nombre || ', ' ||     edo.cd_nombre AS DOMICILIO ,"  +			
				"		 r.ic_nafin_electronico AS NO_CLIENTE ,"  +
			   "      TO_char (s.df_operacion, 'mm/yyyy') AS MES_ANIO " + 			
				"   FROM com_documento doc,"   +
				"        com_docto_seleccionado sel,"   +
				"        com_solicitud s,"   +
				"        comcat_pyme pym,"+tablas+" "+
				"        com_domicilio dom, "   +
				"        comcat_estado edo, "   +
				"        comcat_municipio mun, "+
				"		 comrel_nafin r"+
				"  WHERE doc.ic_documento = sel.ic_documento"   +
				"    AND sel.ic_documento = s.ic_documento"   +
				"    AND doc.ic_pyme = pym.ic_pyme"   +
				"    AND pym.ic_pyme = dom.ic_pyme"   +
				"    AND dom.ic_estado = edo.ic_estado"   +
				"    AND edo.ic_estado = mun.ic_estado"   +
				"    AND dom.ic_municipio = mun.ic_municipio "+
				"    AND s.ig_numero_prestamo IS NOT NULL"   +
				"    AND TRUNC (s.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				"    AND doc.ic_epo = ?"   +
				"    AND doc.ic_if = ?"   +
				"    AND dom.cs_fiscal = 'S' "+condicion+" "+
				"	AND pym.ic_pyme = r.ic_epo_pyme_if "+
				"    AND r.cg_tipo = 'P' " );
				
				conditions.add(fecha_seleccion_de);
				conditions.add(fecha_seleccion_a);
				conditions.add(ic_epo);		
				conditions.add(ic_if);
				if(!"T".equals(cmb_factura) && !"".equals(txt_nafelec)){
					conditions.add(txt_nafelec);   
				}
		
				qrySentencia.append(" AND ( ");
				for (int i = 0; i < pageIds.size(); i++) { 
					List lItem = (ArrayList)pageIds.get(i);
					if(i > 0){qrySentencia.append("  OR  ");}
					qrySentencia.append("pym.ic_pyme   = ? and  TO_CHAR (s.df_operacion, 'mm/yyyy') = ?  ");
					conditions.add(new Long(lItem.get(0).toString()));
					conditions.add(lItem.get(1).toString());
				}
				qrySentencia.append(" ) ");
			
			 
				
				qrySentencia.append("  GROUP BY s.df_operacion,  doc.ic_pyme,"   +
				"        pym.cg_razon_social,pym.cg_rfc,"   +
				"        dom.cg_calle || ' ' ||"   +
				"        dom.cg_numero_ext || ' ' ||"   +
				"        dom.cg_numero_int || ' ' ||"   +
				"        'COL. ' || dom.cg_colonia || ', ' ||"   +
				"        'C.P. ' || dom.cn_cp || ', ' ||" +
				"         mun.cd_nombre || ', ' || "   +
				"         edo.cd_nombre,"  +
				"		 r.ic_nafin_electronico,"+
			
				"			dom.cg_calle , " +
				"        dom.cg_numero_ext, " +
				"        dom.cg_numero_int, " +
				"        dom.cg_colonia, " +
				"        dom.cn_cp, " +
				"        mun.cd_nombre,  " +
				"        edo.cd_nombre "+
				"    order by MES_ANIO  asc ");
			
		} else  if("4".equals(ic_producto_nafin)){
			
				if(!"T".equals(cmb_factura) && !"".equals(txt_nafelec)) {
			tablas =  " comrel_nafin rel, ";
			condicion =
				"    AND pym.ic_pyme = rel.ic_epo_pyme_if "   +
				"    AND rel.cg_tipo = 'P' "   +
				"    AND rel.ic_nafin_electronico = ? ";

			}
			qrySentencia.append(
			" SELECT   /*+index(doc CP_DIS_DOCUMENTO_PK) index(sol CP_DIS_SOLICITUD_PK) index(mun CP_COMCAT_MUNICIPIO_PK) use_nl(doc sel sol lc pym dom edo mun)*/"   +
			"        doc.ic_pyme as IC_PYME , "+   
			"        TO_char (sol.df_operacion, 'mm/yyyy') AS MES_ANIO ,  "+
			"        pym.cg_razon_social as  CG_RAZON_SOCIAL ,"+
			"        pym.cg_rfc  AS RFC ,"   +
			
			"        dom.cg_calle || ' ' ||   dom.cg_numero_ext || ' ' ||  dom.cg_numero_int || ' ' ||  'COL. ' || dom.cg_colonia || ', ' ||  'C.P. ' || dom.cn_cp || ', ' ||  "+
			"        mun.cd_nombre || ', ' ||  edo.cd_nombre AS DOMICILIO   ,"  +
			"		 r.ic_nafin_electronico as NO_CLIENTE , "  +
			
			//FODEA 017-2010 VFR ini
			"        dom.cg_calle as calle_dom, " +
			"        dom.cg_numero_ext as numext_dom, " +
			"        dom.cg_numero_int as numint_dom, " +
			"        dom.cg_colonia as colonia_dom, " +
			"        dom.cn_cp as cp_dom, " +
			"        mun.cd_nombre as municipio,  " +
			"        edo.cd_nombre as estado " +
			//FODEA 017-2010 VFR fin
			"   FROM dis_documento doc,"   +
			"        dis_docto_seleccionado sel,"   +
			"        dis_solicitud sol,"   +
			"        dis_linea_credito_dm lc,"   +
			"        comcat_pyme pym,"+tablas+" "+
			"        com_domicilio dom,"   +
			"        comcat_estado edo,"   +
			"        comcat_municipio mun,"   +
			"		 comrel_nafin r"+
			"  WHERE doc.ic_documento = sel.ic_documento"   +
			"    AND sel.ic_documento = sol.ic_documento"   +
			"    AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm"   +
			"    AND doc.ic_pyme = pym.ic_pyme"   +
			"    AND pym.ic_pyme = dom.ic_pyme"   +
			"    AND dom.ic_estado = edo.ic_estado"   +
			"    AND edo.ic_estado = mun.ic_estado"   +
			"    AND dom.ic_municipio = mun.ic_municipio"   +
			"    AND sol.ig_numero_prestamo IS NOT NULL"   +
			"    AND TRUNC (sol.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
			"    AND doc.ic_epo = ?"   +
			"    AND lc.ic_if = ?"   +
			"    AND dom.cs_fiscal = 'S' "+condicion+" "+
			"	AND pym.ic_pyme = r.ic_epo_pyme_if "+
			"    AND r.cg_tipo = 'P' " );
			
			conditions.add(fecha_seleccion_de);
				conditions.add(fecha_seleccion_a);
				conditions.add(ic_epo);		
				conditions.add(ic_if);
				if(!"T".equals(cmb_factura) && !"".equals(txt_nafelec)){
					conditions.add(txt_nafelec);   
				}
		
				qrySentencia.append(" AND ( ");
				for (int i = 0; i < pageIds.size(); i++) { 
					List lItem = (ArrayList)pageIds.get(i);
					if(i > 0){qrySentencia.append("  OR  ");}
					qrySentencia.append("pym.ic_pyme   = ? and  TO_CHAR (sol.df_operacion, 'mm/yyyy') = ?  ");
					conditions.add(new Long(lItem.get(0).toString()));
					conditions.add(lItem.get(1).toString());
				}
				qrySentencia.append(" ) ");
			
			
			qrySentencia.append("  GROUP BY doc.ic_pyme,"   +
			"        pym.cg_razon_social, pym.cg_rfc,"   +
			"        dom.cg_calle || ' ' ||"   +
			"        dom.cg_numero_ext || ' ' ||"   +
			"        dom.cg_numero_int || ' ' ||"   +
			"        'COL. ' || dom.cg_colonia || ', ' ||"   +
			"        'C.P. ' || dom.cn_cp || ', ' ||  " +
			"        mun.cd_nombre || ', ' || "   +
			"        edo.cd_nombre,"  +
			"		 r.ic_nafin_electronico,"+
			//FODEA 017-2010 FVR ini
			"			dom.cg_calle , " +
			"        dom.cg_numero_ext, " +
			"        dom.cg_numero_int, " +
			"        dom.cg_colonia, " +
			"        dom.cn_cp, " +
			"        mun.cd_nombre,  " +
			"        edo.cd_nombre,  "+
			"        sol.df_operacion  "+
			"    order by MES_ANIO  asc ");
			
			
		}
				
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	/**
	 * O
	 * @return 
	 */
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
		
		String indice ="",  tablas ="", condicion = "";
		
		log.info("tipo_factura    "+tipo_factura);  
		
		
		if("T".equals(tipo_factura)){  // Transacciones
		
			if("1".equals(ic_producto_nafin)){
				
				if(!"T".equals(cmb_factura) && !"".equals(txt_nafelec)) {
					indice = "index(rel CP_COMREL_NAFIN_PK) index(mun CP_COMCAT_MUNICIPIO_PK) ";
					tablas =  " comrel_nafin rel, ";
					condicion =	"    AND pym.ic_pyme = rel.ic_epo_pyme_if "   +
									"    AND rel.cg_tipo = 'P' "   +
									"    AND rel.ic_nafin_electronico = ? ";
				}
					
				qrySentencia.append(" SELECT   /*+"+indice+"use_nl(dom edo)*/"   +
					"      distinct  doc.ic_pyme as IC_PYME , pym.cg_razon_social as  CG_RAZON_SOCIAL,  "+
					"      pym.cg_rfc AS RFC ,"   +				
					"      dom.cg_calle || ' ' ||  dom.cg_numero_ext || ' ' ||  dom.cg_numero_int || ' ' ||  'COL. ' || dom.cg_colonia || ', ' ||  'C.P. ' || dom.cn_cp || ', ' || " +
					"      mun.cd_nombre || ', ' ||     edo.cd_nombre AS DOMICILIO ,"  +			
					"		 r.ic_nafin_electronico AS NO_CLIENTE ,"  +
					"      TO_char (s.df_operacion, 'mm/yyyy') AS MES_ANIO , " + 
					"      e.cg_razon_social as NOMBRE_EPO ,  "+			
					"      dom.cg_calle as calle_dom, " +
					"      dom.cg_numero_ext as numext_dom, " +
					"      dom.cg_numero_int as numint_dom, " +
					"      dom.cg_colonia as colonia_dom, " +
					"      dom.cn_cp as cp_dom, " +
					"      mun.cd_nombre as municipio,  " +
					"      edo.cd_nombre as estado " +
					
					"   FROM com_documento doc,"   +
					"        com_docto_seleccionado sel,"   +
					"        com_solicitud s,"   +
					"        comcat_pyme pym,"+tablas+" "+
					"        com_domicilio dom, "   +
					"        comcat_estado edo, "   +
					"        comcat_municipio mun, "+
					"		   comrel_nafin r, "+
					"        comcat_epo e "+
					
					"  WHERE doc.ic_documento = sel.ic_documento"   +
					"    AND sel.ic_documento = s.ic_documento"   +
					"    AND doc.ic_pyme = pym.ic_pyme"   +
					"    AND pym.ic_pyme = dom.ic_pyme"   +
					"    AND dom.ic_estado = edo.ic_estado"   +
					"    AND edo.ic_estado = mun.ic_estado"   +
					"    AND dom.ic_municipio = mun.ic_municipio "+
					"    AND s.ig_numero_prestamo IS NOT NULL"   +
					"    AND doc.ic_epo =  e.ic_epo  "+
					"    AND TRUNC (s.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND doc.ic_epo = ?"   +
					"    AND doc.ic_if = ?"   +
					"    AND dom.cs_fiscal = 'S' "+condicion+" "+
					"	AND pym.ic_pyme = r.ic_epo_pyme_if "+
					"    AND r.cg_tipo = 'P' " );
					
					conditions.add(fecha_seleccion_de);
					conditions.add(fecha_seleccion_a);
					conditions.add(ic_epo);		
					conditions.add(ic_if);
					if(!"T".equals(cmb_factura) && !"".equals(txt_nafelec)){
						conditions.add(txt_nafelec);   
					}
					
					qrySentencia.append("  GROUP BY s.df_operacion,  doc.ic_pyme,"   +
					"        pym.cg_razon_social, e.cg_razon_social, pym.cg_rfc,"   +
					"        dom.cg_calle || ' ' ||"   +
					"        dom.cg_numero_ext || ' ' ||"   +
					"        dom.cg_numero_int || ' ' ||"   +
					"        'COL. ' || dom.cg_colonia || ', ' ||"   +
					"        'C.P. ' || dom.cn_cp || ', ' ||" +
					"         mun.cd_nombre || ', ' || "   +
					"         edo.cd_nombre,"  +
					"		 r.ic_nafin_electronico,"+
				
					"			dom.cg_calle , " +
					"        dom.cg_numero_ext, " +
					"        dom.cg_numero_int, " +
					"        dom.cg_colonia, " +
					"        dom.cn_cp, " +
					"        mun.cd_nombre,  " +
					"        edo.cd_nombre "+
					"    order by MES_ANIO  asc ");
			
			} else  if("4".equals(ic_producto_nafin)){
				
				if(!"T".equals(cmb_factura) && !"".equals(txt_nafelec)) {
				tablas =  " comrel_nafin rel, ";
				condicion =
					"    AND pym.ic_pyme = rel.ic_epo_pyme_if "   +
					"    AND rel.cg_tipo = 'P' "   +
					"    AND rel.ic_nafin_electronico = ? ";
	
				}
				qrySentencia.append(
				" SELECT   /*+index(doc CP_DIS_DOCUMENTO_PK) index(sol CP_DIS_SOLICITUD_PK) index(mun CP_COMCAT_MUNICIPIO_PK) use_nl(doc sel sol lc pym dom edo mun)*/"   +
				"        doc.ic_pyme as IC_PYME , "+   
				"        TO_char (sol.df_operacion, 'mm/yyyy') AS MES_ANIO ,  "+
				"        pym.cg_razon_social as  CG_RAZON_SOCIAL ,"+
				"        pym.cg_rfc  AS RFC ,"   +			
				"        dom.cg_calle || ' ' ||   dom.cg_numero_ext || ' ' ||  dom.cg_numero_int || ' ' ||  'COL. ' || dom.cg_colonia || ', ' ||  'C.P. ' || dom.cn_cp || ', ' ||  "+
				"        mun.cd_nombre || ', ' ||  edo.cd_nombre AS DOMICILIO   ,"  +
				"		   r.ic_nafin_electronico as NO_CLIENTE , "  +
				"        pym.cg_razon_social as  NOMBRE_EPO ,"+
							" dom.cg_calle as calle_dom, " +
				"        dom.cg_numero_ext as numext_dom, " +
				"        dom.cg_numero_int as numint_dom, " +
				"        dom.cg_colonia as colonia_dom, " +
				"        dom.cn_cp as cp_dom, " +
				"        mun.cd_nombre as municipio,  " +
				"        edo.cd_nombre as estado " +	
				
				"   FROM dis_documento doc,"   +
				"        dis_docto_seleccionado sel,"   +
				"        dis_solicitud sol,"   +
				"        dis_linea_credito_dm lc,"   +
				"        comcat_pyme pym,"+tablas+" "+
				"        com_domicilio dom,"   +
				"        comcat_estado edo,"   +
				"        comcat_municipio mun,"   +
				"		   comrel_nafin r, "+
				"        comcat_epo e "+
				"  WHERE doc.ic_documento = sel.ic_documento"   +
				"    AND sel.ic_documento = sol.ic_documento"   +
				"    AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm"   +
				"    AND doc.ic_pyme = pym.ic_pyme"   +
				"    AND pym.ic_pyme = dom.ic_pyme"   +
				"    AND dom.ic_estado = edo.ic_estado"   +
				"    AND edo.ic_estado = mun.ic_estado"   +
				"    AND dom.ic_municipio = mun.ic_municipio"   +
				"    AND sol.ig_numero_prestamo IS NOT NULL"   +
				"    AND TRUNC (sol.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				"    AND doc.ic_epo = ?"   +
				"    AND lc.ic_if = ?"   +
				"    AND dom.cs_fiscal = 'S' "+condicion+" "+
				"	AND pym.ic_pyme = r.ic_epo_pyme_if "+
				"  AND r.cg_tipo = 'P' " +
				"  and e.ic_epo = doc.ic_epo " );
				
				conditions.add(fecha_seleccion_de);
					conditions.add(fecha_seleccion_a);
					conditions.add(ic_epo);		
					conditions.add(ic_if);
					if(!"T".equals(cmb_factura) && !"".equals(txt_nafelec)){
						conditions.add(txt_nafelec);   
					}
					
				qrySentencia.append("  GROUP BY doc.ic_pyme,"   +
				"        pym.cg_razon_social, pym.cg_rfc,"   +
				"        dom.cg_calle || ' ' ||"   +
				"        dom.cg_numero_ext || ' ' ||"   +
				"        dom.cg_numero_int || ' ' ||"   +
				"        'COL. ' || dom.cg_colonia || ', ' ||"   +
				"        'C.P. ' || dom.cn_cp || ', ' ||  " +
				"        mun.cd_nombre || ', ' || "   +
				"        edo.cd_nombre,"  +
				"		 r.ic_nafin_electronico,"+				
				"			dom.cg_calle , " +
				"        dom.cg_numero_ext, " +
				"        dom.cg_numero_int, " +
				"        dom.cg_colonia, " +
				"        dom.cn_cp, " +
				"        mun.cd_nombre,  " +
				"        edo.cd_nombre,  "+
				"        sol.df_operacion,   "+
				"        e.cg_razon_social  "+ 
				"    order by MES_ANIO  asc ");
			
				
			}
			
		}else if("C".equals(tipo_factura)){  //Comisiones
		
		
			qrySentencia.append("	SELECT  pym.cg_razon_social as CG_RAZON_SOCIAL  ,    "+
									" pym.cg_rfc as RFC ,  "+
				 " r.ic_nafin_electronico as NO_CLIENTE   ,    "+
				 
				 " dom.cg_calle || ' ' ||   dom.cg_numero_ext || ' ' ||  dom.cg_numero_int || ' ' ||  'COL. ' || dom.cg_colonia || ', ' ||  'C.P. ' || dom.cn_cp || ', ' ||   "+
				 " mun.cd_nombre || ', ' ||  edo.cd_nombre AS DOMICILIO  ,   "+
				"  TO_char (Sysdate, 'mm/yyyy') AS MES_ANIO   "+
				
				 " FROM comcat_pyme pym, "+
				 " com_domicilio dom,    "+
				 " comcat_estado edo,    "+
				 " comcat_municipio mun, "+
				 " comrel_nafin r "+
				 " WHERE pym.ic_pyme = dom.ic_pyme  "+
				 " AND dom.ic_estado = edo.ic_estado   "+ 
				 " AND edo.ic_estado = mun.ic_estado    "+
				 " AND dom.ic_municipio = mun.ic_municipio "+
				 " AND dom.ic_estado = mun.ic_estado "+
				 " AND dom.ic_pais = mun.ic_pais    "+
				 " AND r.ic_nafin_electronico= ? "+
				 " AND pym.ic_pyme = r.ic_epo_pyme_if  "+
				 " AND dom.cs_fiscal = ?   "+
				 " AND r.cg_tipo = ?    "+
				 " GROUP BY pym.cg_razon_social, "+
				 " pym.cg_razon_social,  "+
				 " pym.cg_rfc,  "+
				 " r.ic_nafin_electronico,  "+
				 " dom.cg_calle || ' ' || dom.cg_numero_ext || ' ' || dom.cg_numero_int,  "+
				 " dom.cg_colonia,  "+
				 " dom.cn_cp, "+
				 " mun.cd_nombre, "+
				 " edo.cd_nombre  ");
		
			conditions.add(txt_nafelec);   
			conditions.add("S");   
			conditions.add("P");   
			
		}
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		Fecha fecha = new Fecha();
		try {
		
	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}  
	   
	  
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		Fecha fecha = new Fecha();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		String meses[] 		= {"ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
		String nombreProducto ="";
		
		if("1".equals(ic_producto_nafin)){  nombreProducto = "DESCUENTO ELECTR�NICO";  }
		if("4".equals(ic_producto_nafin)){  nombreProducto = "DISTRIBUIDORES";  }
		String mesFactura ="", anyoFactura ="", fechaInicio ="", fecha_fin ="", title ="", diaFinal =""; 
		String fechaActual  = new java.text.SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date());
		/*String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);*/
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
                fechaActual = fechaActual+"T"+horaActual;
				
		
		try {
		 
			
		   Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);

		
			if("Resumen_Facturas".equals(informacion)  && "CSV".equals(tipo)   ){ 
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
		
				contenidoArchivo = new StringBuffer();	
				Calendar mesAnyoFinal 	= new GregorianCalendar();
			
				while (rs.next())	{	
				
					String nombrePYME = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");	
					String NumCliente = (rs.getString("NO_CLIENTE") == null) ? "" : rs.getString("NO_CLIENTE");	
					String NombreEPO = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");	
					String mesAnio = (rs.getString("MES_ANIO") == null) ? "" : rs.getString("MES_ANIO");	
					String ic_pyme = (rs.getString("IC_PYME") == null) ? "" : rs.getString("IC_PYME");	
				
					mesFactura = mesAnio.substring(0,2);
					String anioFactura = mesAnio.substring(3,7);
					
					fechaInicio ="01/"+mesAnio; 
					String fechaFin  =  fecha.getUltimoDiaDelMes(fechaInicio, "dd/MM/yyyy");
					String fechaFinAux ="";
				
                                        contenidoArchivo.append("FACTURAS "+NombreEPO.replace(',',' ')+" ("+meses[Integer.parseInt(mesFactura)-1]+" "+anioFactura+") DE "+nombreProducto +"\n");
					
					int cont =0;	
				
                                        log.info("ic_pyme === "+ic_pyme);
					log.info("fechaInicio === "+fechaInicio);
					log.info("fechaFin === "+fechaFin);
					log.info("ic_producto_nafin === "+ic_producto_nafin);
					log.info("ic_epo === "+ic_epo);
					log.info("ic_if === "+ic_if);
					
				
					Vector Registros = dispersion.getDatosFactura(ic_pyme,fechaInicio,fechaFin,ic_producto_nafin,ic_epo,ic_if,"R");
					  if(Registros.size()>0) {
						if(!"0".equals(Registros.get(0).toString())) {
							if(!fechaFin.equals(fechaFinAux)) {
								cont = 0;
								fechaFinAux = fechaFin;							
							}
							cont++;							
						}				
						contenidoArchivo.append("Num.,Nombre / Raz�n Social,Num. Cliente \n ");
						contenidoArchivo.append(cont +","+nombrePYME.replace(',',' ')+" ,"+ NumCliente  +"\n ");					
						}	
					}
				
				buffer.write(contenidoArchivo.toString());
				buffer.close();	
				contenidoArchivo = new StringBuffer();//Limpio 		
			
			
			}else if("GenerarTodas_Facturas".equals(informacion) && "PDF".equals(tipo)  ){
				
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
								
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			
				
				while (rs.next())	{	
					String rs_ic_pyme =(rs.getString("ic_pyme") == null) ? "" : rs.getString("ic_pyme");					
					String nombrePYME = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");	
					String NumCliente = (rs.getString("NO_CLIENTE") == null) ? "" : rs.getString("NO_CLIENTE");	
					String NombreEPO = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");	
					String mesAnio = (rs.getString("MES_ANIO") == null) ? "" : rs.getString("MES_ANIO");		
					String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");	
					String domicilio = (rs.getString("DOMICILIO") == null) ? "" : rs.getString("DOMICILIO");	 
									
					if(!mesAnio.equals("") ) { 
						mesFactura = mesAnio.substring(0,2);
					   anyoFactura = mesAnio.substring(3,7);		
						fechaInicio ="01/"+mesFactura+"/"+anyoFactura; 
						fecha_fin  =  fecha.getUltimoDiaDelMes(fechaInicio, "dd/MM/yyyy");	
						//diaFinal =  fecha.getUltimoDiaDelMes(fechaInicio, "dd");	
						diaFinal =  fecha_fin.substring(0,2);
                                                title ="M�xico D.F. a "+diaFinal+" de "+meses[Integer.parseInt(mesFactura)-1]+" del "+anyoFactura; 
					}
				
				 String fechaFact= "M�xico, D.F. a "+diaFinal+" de "+meses[Integer.parseInt(mesFactura)-1]+" de "+anyoFactura;
				 
				 float widths2[] = {30f,40f,30f};
				 pdfDoc.setTable(3, 100, widths2); 
				 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,3,1,0);
				 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,3,1,0);
				 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,3,1,0);
				 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,3,1,0);
				 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,3,1,0);
				 StringBuffer encabezado2 = new StringBuffer(); 
				 encabezado2.append("Periodo de Operaci�n: Del 01 al "+diaFinal+" de "+meses[Integer.parseInt(mesFactura)-1]+" de "+anyoFactura);
				  
				 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,1,1,0);
				 pdfDoc.setCell(encabezado2.toString(), "formas", ComunesPDF.CENTER,1,1,0);
				 pdfDoc.setCell("No. Cliente:  "+NumCliente+" ","formasB",ComunesPDF.RIGHT,1,1,0);
				 pdfDoc.setCell("_________________________________________________________________________________________________________________________________________________________","formas", ComunesPDF.CENTER,3,1,0); 
				 pdfDoc.addTable();	
								
				float[] widths3 = {25f, 40f, 35f};	
				pdfDoc.setTable(3,50);
				pdfDoc.setCell("Moneda:","formasB",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("MONEDA NACIONAL","formas",ComunesPDF.LEFT,2,1,0);
				pdfDoc.setCell("Clave del RFC:","formasB",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(rfc,"formas",ComunesPDF.LEFT,2,1,0);
				pdfDoc.setCell("Nombre o Raz�n Social:","formasB",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(nombrePYME,"formas",ComunesPDF.LEFT,2,1,0);
				pdfDoc.setCell("Domicilio:","formasB",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(domicilio,"formas",ComunesPDF.LEFT,2,1,0);
				pdfDoc.addTable();
			
				  pdfDoc.setTable(1,100);
				  pdfDoc.setCell("_________________________________________________________________________________________________________________________________________________________","formas", ComunesPDF.CENTER,1,1,0); 
				  pdfDoc.addTable();  
				
				Vector	regTotales = regTotales = dispersion.getDatosFactura(rs_ic_pyme,fechaInicio,fecha_fin,ic_producto_nafin,ic_epo,ic_if,"N");
					
				double totalMontoOperado = 0;
				double totalInteresCobrado = 0;
				double totalMontoOperadoSirac = 0;
				double totalInteresCobradoSirac = 0;
	
				pdfDoc.addText("");		
				
	         float widthsTable[] = {17.5f,17.5f,17.5f,17.5f};
				pdfDoc.setTable(4,50,widthsTable);		
				pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,1,1,0);	
				pdfDoc.setCell("MONTO OPERADO","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("TASA","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("�INTERES COBRADO ","celda01",ComunesPDF.CENTER);	
			
				for(int r=0;r<regTotales.size();r++) {
					Vector  renglones = (Vector)regTotales.get(r);
					String rs_montoOperado		= (String)renglones.get(0);
					String rs_tasa				= (String)renglones.get(1);
					String rs_interesCobrado	= (String)renglones.get(2);
					
					totalMontoOperado += Double.parseDouble(rs_montoOperado);
					totalInteresCobrado += Double.parseDouble(rs_interesCobrado);
					String porC ="";					
					if( !rs_tasa.equals("") ) { porC="%";  } 
				
					pdfDoc.setCell("" ,"formas",ComunesPDF.CENTER,1,1,0);	
					pdfDoc.setCell("$"+ Comunes.formatoDecimal( rs_montoOperado,2),"formas",ComunesPDF.RIGHT);	
					pdfDoc.setCell(rs_tasa+porC,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+ Comunes.formatoDecimal(rs_interesCobrado,2),"formas",ComunesPDF.RIGHT);	
				}
				
				pdfDoc.setCell("TOTAL OPERADO" ,"formas",ComunesPDF.CENTER,1,1,0);	
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(Double.toString (totalMontoOperado),2),"formas",ComunesPDF.RIGHT);	
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);	
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(Double.toString (totalInteresCobrado),2),"formas",ComunesPDF.RIGHT);	
				
			
				pdfDoc.setCell("IVA" ,"formas",ComunesPDF.CENTER,1,1,0);	
				pdfDoc.setCell("$"+ Comunes.formatoDecimal("0",2),"formas",ComunesPDF.RIGHT);	
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);	
				pdfDoc.setCell("$"+ Comunes.formatoDecimal("0",2),"formas",ComunesPDF.RIGHT);	
				
				pdfDoc.setCell("GRAN TOTAL" ,"formas",ComunesPDF.CENTER,1,1,0);	
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(Double.toString (totalMontoOperado),2),"formas",ComunesPDF.RIGHT);	
				pdfDoc.setCell("","formas",ComunesPDF.CENTER);	
				pdfDoc.setCell("$"+ Comunes.formatoDecimal(Double.toString (totalInteresCobrado),2),"formas",ComunesPDF.RIGHT);	

			
				String  numeroEnLetra = ConvierteCadena.numeroALetra(totalInteresCobrado);
				String centavos = Comunes.formatoDecimal(new Double(totalInteresCobrado).toString(),2,false);
				int indexOfDot = centavos.indexOf('.');
				centavos = centavos.substring(indexOfDot+1,centavos.length());
				numeroEnLetra = "("+numeroEnLetra+" PESOS "+centavos+"/100 M.N.)";
				String relleno = "";
				int max = 10;
				max -= numeroEnLetra.length();
				for(int i=0;i<(max/2);i++)
					relleno += "-";
					relleno += " "+numeroEnLetra+" ";
					for(int x=0;x<max/2;x++)
						relleno += "-";
						
				pdfDoc.addTable();
			
				pdfDoc.addText("");		
		      
				pdfDoc.setTable(1, 100); 
		      pdfDoc.setCell(relleno,"formas",ComunesPDF.CENTER, 1,1,0 );			
				pdfDoc.addTable();
					
				 StringBuffer	 leyenda = new StringBuffer();
					 
				 pdfDoc.setTable(1,100);      
				 pdfDoc.setCell(fechaFact, "formas", ComunesPDF.CENTER,1,1,0);
				 pdfDoc.setCell("_________________________________________________________________________________________________________________________________________________________","formas", ComunesPDF.CENTER,1,1,0); 
				 pdfDoc.setCell("", "formasmen", ComunesPDF.LEFT,1,1,0);
				 leyenda.append(" I.INSURGENTES SUR 1971(F�BRICA DE CR�DITO), COL.GUADALUPE INN M�XICO 01020, D.F. TEL�FONOS: 01-800-6234672 PARA EL INTERIOR DE LA REP�BLICA Y 50-89-61-07 PARA D.F. CORREO");
				 pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);
			
					 
				 leyenda = new StringBuffer();
				 leyenda.append(" ELECTR�NICO:INFO@NAFIN.GOB.MX P�GINA DE INTERNET:WWW.NAFIN.GOB.MX;CONDUSEF:TEL�FONO 01800 999 80 80,P�GINA DE INTERNET:WWW.CONDUSEF.GOB.MX");
				 pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);
				
				 leyenda = new StringBuffer();
				 leyenda.append(" II.SI SU CR�DITO ES DE TASA VARIABLE, LOS INTERESES MENSUALES, A SU CARGO, QUE SE GENEREN PUEDEN CAMBIAR Y, EN SU CASO, AUMENTAR ANTE UN INCREMENTO EN LAS TASAS DE");
				 pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);
			
				
			
				 leyenda = new StringBuffer();
				 leyenda.append(" INTER�S DE MERCADO.");
				 pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);
			
				 
				 leyenda = new StringBuffer();
				 leyenda.append(" III.SI SU CR�DITO ESTA DENOMINADO EN [MONEDA EXTRANJERA] / [UDIS] SU SALDO PODR�A AUMENTAR ANTE UN INCREMENTO EN [EL TIPO DE CAMBIO] / [LA INFLACI�N].");
				 pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);
				
				 
				 leyenda = new StringBuffer();
				 leyenda.append(" IV.SI SU CR�DITO ESTA SUJETO A UNA VARIABLE DE AJUSTE A LA SUERTE PRINCIPAL, SU SALDO PUDIERA AUMENTAR ANTE UN INCREMENTO EN EL [SALARIO M�NIMO NACIONAL] / [�NDICE");
				 pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);
				 
				 
				 leyenda = new StringBuffer();
				 leyenda.append(" NACIONAL DE PRECIOS AL CONSUMIDOR].");
				 pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);    
			
				 pdfDoc.addTable();	
		       pdfDoc.newPage();
				} //while (rs.next())	{
				
				pdfDoc.endDocument();	
			
			}else if("GenerarTodas_Facturas_TXT".equals(informacion) && "TXT".equals(tipo)  ){
			
			
                                CreaArchivo archivo = new CreaArchivo();
				//nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
				//writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				//buffer = new BufferedWriter(writer);
                                
				contenidoArchivo = new StringBuffer();
                                
				while (rs.next())	{	
					String rs_ic_pyme =(rs.getString("ic_pyme") == null) ? "" : rs.getString("ic_pyme");					
					String nombrePYME = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");	
					String NumCliente = (rs.getString("NO_CLIENTE") == null) ? "" : rs.getString("NO_CLIENTE");	
					String NombreEPO = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");	
					String mesAnio = (rs.getString("MES_ANIO") == null) ? "" : rs.getString("MES_ANIO");		
					String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");	
					String calleDom = (rs.getString("calle_dom") == null) ? "" : rs.getString("calle_dom");
					String numextDom = (rs.getString("numext_dom") == null) ? "" : rs.getString("numext_dom");	
					String numintDom = (rs.getString("numint_dom") == null) ? "" : rs.getString("numint_dom");	
					
					String coloniaDom = (rs.getString("colonia_dom") == null) ? "" : rs.getString("colonia_dom");	
					String municipio = (rs.getString("municipio") == null) ? "" : rs.getString("municipio");	
					String estado = (rs.getString("estado") == null) ? "" : rs.getString("estado");	
					String cpDom = (rs.getString("cp_dom") == null) ? "" : rs.getString("cp_dom");	
										
					if(!mesAnio.equals("") ) { 
						mesFactura = mesAnio.substring(0,2);
					   anyoFactura = mesAnio.substring(3,7);		
						fechaInicio ="01/"+mesFactura+"/"+anyoFactura; 
						fecha_fin  =  fecha.getUltimoDiaDelMes(fechaInicio, "dd/MM/yyyy");	
						//diaFinal =  fecha.getUltimoDiaDelMes(fechaInicio, "dd");	
                                                diaFinal  =  fecha_fin.substring(0,2);
                                                title ="M�xico D.F. a "+diaFinal+" de "+meses[Integer.parseInt(mesFactura)-1]+" del "+anyoFactura; 
					}
				
				
				
				String fechaFact= "Del 01 al "+diaFinal+" de "+meses[Integer.parseInt(mesFactura)-1]+" de "+anyoFactura;
				 
				Vector	regTotales = regTotales = dispersion.getDatosFactura(rs_ic_pyme,fechaInicio,fecha_fin,ic_producto_nafin,ic_epo,ic_if,"N");
				double totalMontoOperado = 0;
				double totalInteresCobrado = 0;
				double totalMontoOperadoSirac = 0;
				double totalInteresCobradoSirac = 0;
				StringBuffer contenidoDet = new StringBuffer();
		
				for(int r=0;r<regTotales.size();r++) {
					Vector  renglones = (Vector)regTotales.get(r);
					String rs_montoOperado		= (String)renglones.get(0);
					String rs_tasa				= (String)renglones.get(1);
					String rs_interesCobrado	= (String)renglones.get(2);
					
					totalMontoOperado += Double.parseDouble(rs_montoOperado);
					totalInteresCobrado += Double.parseDouble(rs_interesCobrado);
					
					contenidoDet.append("07|"+Comunes.formatoDecimal(rs_tasa, 4, false)+"%|"+Comunes.formatoDecimal(rs_interesCobrado, 2, false)+"|"+Comunes.formatoDecimal(rs_montoOperado, 2, false)+"\r\n");
		
				}
				String  numeroEnLetra = ConvierteCadena.numeroALetra(totalInteresCobrado);
				String centavos = Comunes.formatoDecimal(new Double(totalInteresCobrado).toString(),2,false);
				int indexOfDot = centavos.indexOf('.');
				centavos = centavos.substring(indexOfDot+1,centavos.length());
				numeroEnLetra = "("+numeroEnLetra+" PESOS "+centavos+"/100 M.N.)";
				String relleno = "";
				int max = 10;
				max -= numeroEnLetra.length();
				for(int i=0;i<(max/2);i++)
					relleno += "-";
					relleno += " "+numeroEnLetra+" ";
					for(int x=0;x<max/2;x++)
						relleno += "-";
						
				 
					//TRAMA 1
					contenidoArchivo.append("01|Estado de Cuenta|3.3|||"+fechaActual+"||||03||||"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|||"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|Dep�sito en cuenta|I|MXN||Emisor|NFI3406305T0|");
					contenidoArchivo.append("NACIONAL FINANCIERA, S.N.C.|DomicilioFiscal|Avenida Insurgentes Sur|1971||Col. Guadalupe Inn|M�xico||Del. Alavaro Obregon|M�xico D.F.|");
					contenidoArchivo.append("M�xico|01020|Gln|ExpedidoEn|||||||||M�xico|||Receptor|"+ (rfc!=null?rfc.replaceAll("-",""):"")+"|"+nombrePYME+"|Domicilio|"+calleDom+"|"+numextDom+"|"+numintDom+"|");
					contenidoArchivo.append(coloniaDom+"|||"+municipio+"|"+estado+"|M�xico|"+cpDom+"||A2012_1|MEX|P01|||||Regimen|General de ley personas morales\r\n");				
					//TRAMA 2
				  	contenidoArchivo.append("02|93151608||1|B17|No Aplica|Interes Cobrado|"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|\r\n");
					//Trama 3
					contenidoArchivo.append("03||0.00|03.1|03.1.1|03.2|03.2.1|IVA|0.160000|0.00\r\n");					
					//Trama4
					contenidoArchivo.append("04|3|"+fechaFact+"|"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|"+Comunes.formatoDecimal(totalMontoOperado, 2, false)+"|Pesos||||||"+NumCliente+"|||||");
					contenidoArchivo.append("I.INSURGENTES SUR 1971(F�BRICA DE CR�DITO), COL.GUADALUPE INN M�XICO 01020, D.F. TEL�FONOS: 01-800-6234672 PARA EL INTERIOR DE LA REP�BLICA Y 50-89-61-07"+
						" PARA D.F. CORREO ELECTR�NICO:INFO@NAFIN.GOB.MX P�GINA DE INTERNET:WWW.NAFIN.GOB.MX;CONDUSEF:TEL�FONO 01800 999 80 80,P�GINA DE INTERNET:WWW.CONDUSEF.GOB.MX!");
					contenidoArchivo.append("II.SI SU CR�DITO ES DE TASA VARIABLE, LOS INTERESES MENSUALES, A SU CARGO, QUE SE GENEREN PUEDEN CAMBIAR Y, EN SU CASO, AUMENTAR ANTE UN INCREMENTO EN LAS"+
						" TASAS DE INTER�S DE MERCADO.!");
					contenidoArchivo.append("III.SI SU CR�DITO ESTA DENOMINADO EN [MONEDA EXTRANJERA] / [UDIS] SU SALDO PODR�A AUMENTAR ANTE UN INCREMENTO EN"+
						" [EL TIPO DE CAMBIO] / [LA INFLACI�N].!");
					contenidoArchivo.append("IV.SI SU CR�DITO ESTA SUJETO A UNA VARIABLE DE AJUSTE A LA SUERTE PRINCIPAL, SU SALDO PUDIERA AUMENTAR ANTE"+
						" UN INCREMENTO EN EL [SALARIO M�NIMO NACIONAL] / [�NDICE NACIONAL DE PRECIOS AL CONSUMIDOR].|"+numeroEnLetra+"|\r\n");
					//TRAMA 5
					contenidoArchivo.append("05|||||\r\n");					
					//TRAMA 6
					contenidoArchivo.append("06||||||||\r\n");					
					//TRAMA 7
					contenidoArchivo.append(contenidoDet.toString());
					contenidoArchivo.append("07||"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|"+Comunes.formatoDecimal(totalMontoOperado, 2, false)+"||||||||||||||||TOTAL OPERADO\r\n");
					contenidoArchivo.append("07||0.00|0.00||||||||||||||||IVA\r\n");
					contenidoArchivo.append("07||"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|"+Comunes.formatoDecimal(totalMontoOperado, 2, false)+"||||||||||||||||GRAN TOTAL\r\n");
				 
				} // while (rs.next()) 
			
				//buffer.write(contenidoArchivo.toString());
				//buffer.close();
				if (archivo.make(contenidoArchivo.toString(), path, ".txt"))
				    nombreArchivo = archivo.nombre;
				contenidoArchivo = new StringBuffer();//Limpio 		 
			}			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	
	/**
	 * 
	 * @return 
	 * @param numeroEnLetra
	 * @param tituloGridC
	 * @param totales
	 * @param encabezado
	 * @param path
	 * @param request
	 */
	 
	public String facturasGeneralPDF(HttpServletRequest request , String path , String encabezado  , String totales [] , String tituloGridC , String numeroEnLetra  ) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
			Fecha fecha = new Fecha();
			
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			
			
			VectorTokenizer vtEnca = new VectorTokenizer(encabezado,"|");
			Vector vecdat=vtEnca.getValuesVector();		
			String nombre = (String)vecdat.get(0);
			String no_cliente = (String)vecdat.get(1);
			String domicilio = (String)vecdat.get(2);
			String rfc = (String)vecdat.get(3);
			
			String mesAnio = (String)vecdat.get(4);		
			String mesFactura = mesAnio.substring(0,2);
			String anioFactura = mesAnio.substring(3,7);			
						
			String fechaInicio ="01/"+mesAnio; 
                        String diaFinal  =  fecha.getUltimoDiaDelMes(fechaInicio, "dd/MM/yyyy");
                        diaFinal = diaFinal.substring(0,2);
                        String fechaFact =  "";
			if(tipo_factura.equals("T")){  
				fechaFact= "M�xico, D.F. a "+diaFinal+" de "+meses[Integer.parseInt(mesFactura)-1]+" de "+anioFactura;			 
			}
			
			 float widths2[] = {30f,40f,30f};
			 pdfDoc.setTable(3, 100, widths2); 
			 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,3,1,0);
			 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,3,1,0);
			 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,3,1,0);
			 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,3,1,0);
			 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,3,1,0);
			 StringBuffer encabezado2 = new StringBuffer(); 
				if(tipo_factura.equals("T")){   
					encabezado2.append("Periodo de Operaci�n: Del 01 al "+diaFinal+" de "+meses[Integer.parseInt(mesFactura)-1]+" de "+anioFactura);
				}else if(tipo_factura.equals("C")){  
					encabezado2.append( "M�xico, D.F. a "+diaActual+" de "+meses[Integer.parseInt(mesFactura)-1]+" de "+anioFactura);	
				}
			  
			 pdfDoc.setCell("","formasB",ComunesPDF.RIGHT,1,1,0);
			 pdfDoc.setCell(encabezado2.toString(), "formas", ComunesPDF.CENTER,1,1,0);
			 pdfDoc.setCell("No. Cliente:  "+no_cliente+" ","formasB",ComunesPDF.RIGHT,1,1,0);
			 pdfDoc.setCell("_________________________________________________________________________________________________________________________________________________________","formas", ComunesPDF.CENTER,3,1,0); 
			 pdfDoc.addTable();
	 		
				
		   float[] widths3 = {25f, 40f, 35f};	
			pdfDoc.setTable(3,50);
			pdfDoc.setCell("Moneda:","formasB",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell("MONEDA NACIONAL","formas",ComunesPDF.LEFT,2,1,0);
			pdfDoc.setCell("Clave del RFC:","formasB",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(rfc,"formas",ComunesPDF.LEFT,2,1,0);
			pdfDoc.setCell("Nombre o Raz�n Social:","formasB",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(nombre,"formas",ComunesPDF.LEFT,2,1,0);
			pdfDoc.setCell("Domicilio:","formasB",ComunesPDF.LEFT,1,1,0);
			pdfDoc.setCell(domicilio,"formas",ComunesPDF.LEFT,2,1,0);
			
			if(tipo_factura.equals("C")){  
				pdfDoc.setCell("Producto:","formasB",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("Financiamiento a Distribuidores","formas",ComunesPDF.LEFT,2,1,0);
			}
			pdfDoc.addTable();
			
					
		  pdfDoc.setTable(1,100);
		  pdfDoc.setCell("_________________________________________________________________________________________________________________________________________________________","formas", ComunesPDF.CENTER,1,1,0); 
		  pdfDoc.addTable(); 
			
			pdfDoc.addText("");		
			
			
			if(tipo_factura.equals("T")){  
				float widthsTable[] = {17.5f,17.5f,17.5f,17.5f};
				pdfDoc.setTable(4,50,widthsTable);
				pdfDoc.setCell(" ","formas",ComunesPDF.CENTER ,1,1,0);	
				pdfDoc.setCell("MONTO OPERADO","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("TASA","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("INTERES COBRADO ","celda01",ComunesPDF.CENTER);
				
				for(int i=0;i<totales.length;i++){ 
					String reg =  totales[i];
					
					VectorTokenizer vtotales = new VectorTokenizer(reg,"|");
					Vector vecdatos=vtotales.getValuesVector();	
					String porC ="";
					
					if( !((String)vecdatos.get(2)).equals("") ) { porC="%";  } 
				
					pdfDoc.setCell((String)vecdatos.get(0) ,"formas",ComunesPDF.CENTER,1,1,0);	
					pdfDoc.setCell("$"+ Comunes.formatoDecimal((String)vecdatos.get(1),2),"formas",ComunesPDF.RIGHT);	
					pdfDoc.setCell((String)vecdatos.get(2)+porC,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+ Comunes.formatoDecimal((String)vecdatos.get(3),2),"formas",ComunesPDF.RIGHT);	
				}
				pdfDoc.addTable();
			
			}else if(tipo_factura.equals("C")){  			
				
				float widthsTable[] = {17.5f,17.5f,17.5f };
				pdfDoc.setTable(3,50,widthsTable);				
				pdfDoc.setCell("IMPORTE AUTORIZADO","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("COMISION  ","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("IMPORTE","celda01",ComunesPDF.CENTER);	
				
				
				for(int i=0;i<totales.length;i++){ 
					String reg =  totales[i];
					
					VectorTokenizer vtotales = new VectorTokenizer(reg,"|");
					Vector vecdatos=vtotales.getValuesVector();	
					String porC ="";
						
					if( !((String)vecdatos.get(2)).equals("")  &&  !((String)vecdatos.get(4)).equals("LETRA")    ) { porC="%";  } 				
					
					if( !((String)vecdatos.get(1)).equals(""))  {
						pdfDoc.setCell("$"+ Comunes.formatoDecimal((String)vecdatos.get(1),2),"formas",ComunesPDF.RIGHT);	
					}else  {
						pdfDoc.setCell("","formas",ComunesPDF.RIGHT);	
					}
					
					if( !((String)vecdatos.get(2)).equals("")  &&  !((String)vecdatos.get(4)).equals("LETRA")    ) { porC="%";  } 		
						
					
					if( !((String)vecdatos.get(2)).equals("")  &&  !((String)vecdatos.get(4)).equals("LETRA")    ) { 
						pdfDoc.setCell((String)vecdatos.get(2)+porC,"formas",ComunesPDF.CENTER);	
					}else if( !((String)vecdatos.get(2)).equals("")  &&  ((String)vecdatos.get(4)).equals("LETRA") ) { 
						pdfDoc.setCell((String)vecdatos.get(2),"formas",ComunesPDF.CENTER);							
					}
					pdfDoc.setCell("$"+ Comunes.formatoDecimal((String)vecdatos.get(3),2),"formas",ComunesPDF.RIGHT);	
				}
				pdfDoc.addTable();
				
			}		
			
			pdfDoc.addText("");				
			pdfDoc.setTable(1, 100); 
		   pdfDoc.setCell(numeroEnLetra,"formas",ComunesPDF.CENTER, 1,1,0 );			
			pdfDoc.addTable();
				
		
	 StringBuffer	 leyenda = new StringBuffer();
		 
	 pdfDoc.setTable(1,100);      
    pdfDoc.setCell(fechaFact, "formas", ComunesPDF.CENTER,1,1,0);
    pdfDoc.setCell("_________________________________________________________________________________________________________________________________________________________","formas", ComunesPDF.CENTER,1,1,0); 
    pdfDoc.setCell("", "formasmen", ComunesPDF.LEFT,1,1,0);
    leyenda.append(" I.INSURGENTES SUR 1971(F�BRICA DE CR�DITO), COL.GUADALUPE INN M�XICO 01020, D.F. TEL�FONOS: 01-800-6234672 PARA EL INTERIOR DE LA REP�BLICA Y 50-89-61-07 PARA D.F. CORREO");
    pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);

       
    leyenda = new StringBuffer();
    leyenda.append(" ELECTR�NICO:INFO@NAFIN.GOB.MX P�GINA DE INTERNET:WWW.NAFIN.GOB.MX;CONDUSEF:TEL�FONO 01800 999 80 80,P�GINA DE INTERNET:WWW.CONDUSEF.GOB.MX");
    pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);
   
    leyenda = new StringBuffer();
    leyenda.append(" II.SI SU CR�DITO ES DE TASA VARIABLE, LOS INTERESES MENSUALES, A SU CARGO, QUE SE GENEREN PUEDEN CAMBIAR Y, EN SU CASO, AUMENTAR ANTE UN INCREMENTO EN LAS TASAS DE");
    pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);

   

    leyenda = new StringBuffer();
    leyenda.append(" INTER�S DE MERCADO.");
    pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);

    
    leyenda = new StringBuffer();
    leyenda.append(" III.SI SU CR�DITO ESTA DENOMINADO EN [MONEDA EXTRANJERA] / [UDIS] SU SALDO PODR�A AUMENTAR ANTE UN INCREMENTO EN [EL TIPO DE CAMBIO] / [LA INFLACI�N].");
    pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);
   
    
    leyenda = new StringBuffer();
    leyenda.append(" IV.SI SU CR�DITO ESTA SUJETO A UNA VARIABLE DE AJUSTE A LA SUERTE PRINCIPAL, SU SALDO PUDIERA AUMENTAR ANTE UN INCREMENTO EN EL [SALARIO M�NIMO NACIONAL] / [�NDICE");
    pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);
    
    
    leyenda = new StringBuffer();
    leyenda.append(" NACIONAL DE PRECIOS AL CONSUMIDOR].");
    pdfDoc.setCell(leyenda.toString(), "formasrep", ComunesPDF.LEFT,1,1,0);    

    pdfDoc.addTable();
			pdfDoc.endDocument();	
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	

	/**
	 * METDO PARA OBTENER CATALOGO DE LAS PUMES EN BUSQUEDA AVANZADA 
	 * @return 
	 * @param txt_nafelec
	 * @param rfc_prov
	 * @param nombre_pyme
	 */
	public List  getCatalogobusqueda(String  nombre_pyme, String  rfc_prov, String  numProveedor, String ic_epo,  String ic_producto_nafin   ){
		log.info("getCatalogobusqueda (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		PreparedStatement ps = null;
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		StringBuffer strSQL = new StringBuffer("");
		List	varBind = new ArrayList();
		try{
			con.conexionDB();
			
			strSQL = new StringBuffer("");
			varBind = new ArrayList();	
			strSQL.append(" SELECT  /*+index(pe CP_COMREL_PYME_EPO_PK) index(p CP_COMCAT_PYME_PK) use_nl(pe d p)*/ "+
				"  DISTINCT p.ic_pyme  || ' ' || p.cg_razon_social  as CLAVE_PYME , "+   				
				" crn.ic_nafin_electronico || ' ' || p.cg_razon_social as DESCRIPCION,   "+
				"  p.cg_razon_social "+ 	  			
				"    FROM comrel_pyme_epo pe, ");
				
			if(ic_producto_nafin.equals("1")) {  strSQL.append("   com_documento d, ");  }
			if(ic_producto_nafin.equals("4")) {  strSQL.append("   dis_documento d, ");  }
			
			strSQL.append("comrel_nafin crn, "+
				"    comcat_pyme p "+
				"    WHERE p.ic_pyme = pe.ic_pyme "+
				"    AND p.ic_pyme = crn.ic_epo_pyme_if "+
				"    AND d.ic_pyme = pe.ic_pyme  "+
				"    AND d.ic_epo = pe.ic_epo "+
				"    AND crn.cg_tipo = 'P' "+
			  "    AND pe.cs_habilitado = 'S'  "+
				"    AND p.cs_habilitado = 'S' ");
			
			 
			if(ic_producto_nafin.equals("1")) { 		strSQL.append(" AND pe.cg_pyme_epo_interno IS NOT NULL ");   }
				
				if(!ic_epo.equals("")) {
					strSQL.append("  AND pe.ic_epo  = ?   ");
					varBind.add(ic_epo);
				}	
				
				if(!nombre_pyme.equals("")) {
					strSQL.append("  AND cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
					varBind.add(nombre_pyme);
				}						
				if(!rfc_prov.equals("")) {
					strSQL.append(" AND cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
					varBind.add(rfc_prov);
				}
				
				if(!numProveedor.equals("")) {
					strSQL.append(" AND cg_pyme_epo_interno LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
					varBind.add(numProveedor);
				}
				
				
				strSQL.append("   GROUP BY p.ic_pyme, "+
				"    pe.cg_pyme_epo_interno, "+
				"    p.cg_razon_social, "+
				"    RPAD (pe.cg_pyme_epo_interno, 10, ' ') || p.cg_razon_social, "+
				"    crn.ic_nafin_electronico "+
				"    ORDER BY p.cg_razon_social ");
	
			log.info("strSQL   "+strSQL);
			log.info("varBind   "+varBind);  
			
			ps = con.queryPrecompilado(strSQL.toString(),varBind );	
			rs = ps.executeQuery();
			log.info("strSQL   "+strSQL);
			log.info("varBind   "+varBind);
			 
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString("DESCRIPCION") );
				datos.put("descripcion", rs.getString("DESCRIPCION"));
				registros.add(datos);
			}
			rs.close();
			ps.close();
			
			if(registros.size()==0) {
				datos = new HashMap();			
				datos.put("clave","" );
				datos.put("descripcion", "No existe informaci�n con los criterios determinados");
				registros.add(datos);
			}
			
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getCatalogoEPO (S) ");   
		return registros;
	}
	
	
	
	/**
	 *  
	 * @return 
	 * @param num_electronico
	 */
		public List   getBusquedaPyme(String  num_electronico ){
		log.info("getBusquedaPyme (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		PreparedStatement ps = null;
		StringBuffer strSQL = new StringBuffer("");
		List	varBind = new ArrayList();
		String ic_pyme ="";
		List	datosPyme = new ArrayList();
		try{
			con.conexionDB();
			
			strSQL.append(" SELECT  n.IC_EPO_PYME_IF   ,  p.CG_RAZON_SOCIAL   "+
			" from comrel_nafin n,  comcat_pyme p     " +
			" where n.IC_NAFIN_ELECTRONICO  =   ?   "+
			" and  n.IC_EPO_PYME_IF = p.ic_pyme  "+			
			" and cg_tipo = ? ");
			
			varBind.add(num_electronico);
			varBind.add("P");
			
			log.info("strSQL   "+strSQL);
			log.info("varBind   "+varBind);  
			
			ps = con.queryPrecompilado(strSQL.toString(),varBind );	
			rs = ps.executeQuery();
			
			if(rs.next()) {			
				datosPyme.add(rs.getString("IC_EPO_PYME_IF"));	
				datosPyme.add(rs.getString("CG_RAZON_SOCIAL"));		
			}
			rs.close();
			ps.close();		
		
			
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getBusquedaPyme (S) ");   
		return datosPyme;
	}
	
	

	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getTxt_nafelec() {
		return txt_nafelec;
	}

	public void setTxt_nafelec(String txt_nafelec) { 
		this.txt_nafelec = txt_nafelec;
	}

	public String getFecha_seleccion_de() {
		return fecha_seleccion_de;
	}

	public void setFecha_seleccion_de(String fecha_seleccion_de) {
		this.fecha_seleccion_de = fecha_seleccion_de;
	}

	public String getFecha_seleccion_a() {
		return fecha_seleccion_a;
	}

	public void setFecha_seleccion_a(String fecha_seleccion_a) {
		this.fecha_seleccion_a = fecha_seleccion_a;
	}

	public String getIc_producto_nafin() {
		return ic_producto_nafin;
	}

	public void setIc_producto_nafin(String ic_producto_nafin) {
		this.ic_producto_nafin = ic_producto_nafin;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getTipo_factura() {
		return tipo_factura;
	}

	public void setTipo_factura(String tipo_factura) {
		this.tipo_factura = tipo_factura;
	}

	public String getInformacion() {
		return informacion;
	}

	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}

	public String getCmb_factura() {
		return cmb_factura;
	}

	public void setCmb_factura(String cmb_factura) {
		this.cmb_factura = cmb_factura;
	}

		
		

}