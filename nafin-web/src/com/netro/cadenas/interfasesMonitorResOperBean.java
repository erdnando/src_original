package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class interfasesMonitorResOperBean {

	public String getIntermediarios(String producto){
		String ids 			 = "";
		String sep			 = "";
		String qrySentencia  = "";
		ResultSet rs 		 = null;
		PreparedStatement ps = null;
		AccesoDB con 		 = new AccesoDB();

		try {
			System.out.println("interfasesMonitorResOperBean.getIntermediarios(E)");
			if(!producto.equals("")) {
				qrySentencia += " SELECT DISTINCT IFINAN FROM ( ";
				con.conexionDB();
				//Cr�dito Electr�nico
		 		if(producto.equals("0")){
					qrySentencia += " SELECT /*+ index (in_com_solic_portal_04_nuk)*/ DISTINCT S.IC_IF AS IFINAN "+
									" FROM COM_SOLIC_PORTAL S "+
									"      , COMCAT_IF  I "+
									" WHERE S.IC_IF = I.IC_IF "+
									" AND s.df_carga >= TRUNC(SYSDATE) AND s.df_carga < TRUNC(SYSDATE+1)" +
									" UNION " +
									" SELECT /*+ index (s)*/ DISTINCT S.IC_IF AS IFINAN "+
									" FROM COM_SOLIC_PORTAL S "+
									"      , COMCAT_IF  I "+
									" WHERE S.IC_IF = I.IC_IF "+
									" AND s.ic_estatus_solic = 12 and s.ic_bloqueo is null";
				}

				//Descuento Electr�nico
		 		if(producto.equals("1")){
			 		if(producto.equals(""))
						qrySentencia += " UNION ALL ";

					qrySentencia += " SELECT /*+ ORDERED */ DISTINCT D.IC_IF AS IFINAN "+
									" FROM COM_SOLICITUD S "+
									"      , COM_DOCUMENTO D "+
									"      , COM_DOCTO_SELECCIONADO SEL "+									
									"      , COMCAT_IF  I "+
									"      , COMCAT_ESTATUS_SOLIC ES "+
									"      , COMCAT_MONEDA M "+
									" WHERE S.IC_DOCUMENTO = D.IC_DOCUMENTO "+
									" AND D.IC_DOCUMENTO = SEL.IC_DOCUMENTO "+
									" AND SEL.IC_LINEA_CREDITO_EMP IS NULL "+
									" AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
									" AND D.IC_IF = I.IC_IF "+
									" AND D.IC_MONEDA = M.IC_MONEDA "+
									" AND s.df_fecha_solicitud >= TRUNC(SYSDATE) " +
									" AND s.df_fecha_solicitud < TRUNC(SYSDATE+1)";
				}
			/*
				//Financiamiento a Pedidos
		 		if(producto.equals("2")){
			 		if(producto.equals(""))
						qrySentencia += " UNION ALL ";

					qrySentencia += " SELECT DISTINCT LC.IC_IF AS IFINAN "+
									" FROM COM_ANTICIPO A "+
									"      , COM_PEDIDO_SELECCIONADO PS "+
									"      , COM_LINEA_CREDITO LC "+
									"      , COMCAT_IF  I "+
									"      , COMCAT_ESTATUS_SOLIC ES "+
									"      , COMCAT_MONEDA M "+
									" WHERE A.IC_PEDIDO = PS.IC_PEDIDO "+
									" AND PS.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO  "+
									" AND A.IC_ESTATUS_ANTIC = ES.IC_ESTATUS_SOLIC "+
									" AND LC.IC_IF = I.IC_IF "+
									" AND LC.IC_MONEDA = M.IC_MONEDA  "+
									" AND A.DF_FECHA_SOLICITUD >= TRUNC(SYSDATE) " +
									" AND A.DF_FECHA_SOLICITUD < TRUNC(SYSDATE+1) ";
				}
				*/
		 		//Financiamiento a Distribuidores
		 		if(producto.equals("4")){
			 		if(producto.equals(""))
						qrySentencia += " UNION ALL ";

					qrySentencia += " SELECT DISTINCT IC_IF AS IFINAN "+
									" FROM( "+
									" SELECT DISTINCT LC.IC_IF AS IC_IF "+
									" FROM DIS_SOLICITUD S "+
									"      , DIS_DOCTO_SELECCIONADO DS "+
									"     , DIS_DOCUMENTO D "+
									"      , DIS_LINEA_CREDITO_DM LC "+
									"      , COMCAT_IF I "+
									"      , COMCAT_ESTATUS_SOLIC ES "+
									"      , COMCAT_MONEDA M "+
									" WHERE S.IC_DOCUMENTO = DS.IC_DOCUMENTO "+
									" AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO "+
									" AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM "+
									" AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
									" AND LC.IC_IF = I.IC_IF "+
									" AND D.IC_MONEDA = M.IC_MONEDA  "+
									" AND ds.df_fecha_seleccion >= TRUNC(SYSDATE) " +
									" AND ds.df_fecha_seleccion < TRUNC(SYSDATE + 1) " +
									" UNION ALL "+
									" SELECT DISTINCT LC.IC_IF AS IC_IF "+
									" FROM DIS_SOLICITUD S "+
									"      , DIS_DOCTO_SELECCIONADO DS "+
									"      , DIS_DOCUMENTO D "+
									"      , COM_LINEA_CREDITO LC "+
									"      , COMCAT_IF  I "+
									"      , COMCAT_ESTATUS_SOLIC ES "+
									"      , COMCAT_MONEDA M "+
									" WHERE S.IC_DOCUMENTO = DS.IC_DOCUMENTO "+
									" AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO "+
									" AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO "+
									" AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
									" AND LC.IC_IF = I.IC_IF "+
									" AND D.IC_MONEDA = M.IC_MONEDA  "+
									" AND ds.df_fecha_seleccion >= TRUNC(SYSDATE) " +
									" AND ds.df_fecha_seleccion < TRUNC(SYSDATE + 1)) " ;
									//" AND TRUNC(DS.DF_FECHA_SELECCION) = TRUNC(SYSDATE))";
				}

			/*
		 		//Credicadenas
		 		if(producto.equals("5")){
			 		if(producto.equals(""))
						qrySentencia += " UNION ALL ";

					qrySentencia += " SELECT DISTINCT LC.IC_IF AS IFINAN "+
									" FROM INV_SOLICITUD S "+
									"      , INV_DISPOSICION D "+
									"      , COM_LINEA_CREDITO LC "+
									"      , COMCAT_IF  I "+
									"      , COMCAT_ESTATUS_SOLIC ES "+
									"      , COMCAT_MONEDA M "+
									" WHERE S.CC_DISPOSICION = D.CC_DISPOSICION "+
									" AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO "+
									" AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
									" AND LC.IC_IF = I.IC_IF "+
									" AND LC.IC_MONEDA = M.IC_MONEDA  "+
									" AND S.DF_AUTORIZACION >= TRUNC(SYSDATE) " +
									" AND S.DF_AUTORIZACION < TRUNC(SYSDATE+1) ";
				}
				
		 		//NAFIN EMPRESARIAL
//System.out.println("\n producto:"+producto);
		 		if(producto.equals("8")) {
//System.out.println("\n producto2:"+producto);
			 		if(producto.equals(""))
						qrySentencia += " UNION ALL ";

					qrySentencia += " SELECT DISTINCT D.IC_IF AS IFINAN "+
									" FROM COM_SOLICITUD S "+
									"      , COM_DOCTO_SELECCIONADO SEL "+
									"      , COM_DOCUMENTO D "+
									"      , EMP_LINEA_CREDITO LC "+
									"      , COMCAT_IF  I "+
									"      , COMCAT_ESTATUS_SOLIC ES "+
									"      , COMCAT_MONEDA M "+
									" WHERE S.IC_DOCUMENTO = D.IC_DOCUMENTO "+
									" AND D.IC_DOCUMENTO = SEL.IC_DOCUMENTO "+
									" AND SEL.IC_LINEA_CREDITO_EMP = LC.IC_LINEA_CREDITO "+
									" AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
									" AND D.IC_IF = I.IC_IF "+
									" AND D.IC_MONEDA = M.IC_MONEDA "+
									" AND s.df_fecha_solicitud >= TRUNC(SYSDATE) " +
									" AND s.df_fecha_solicitud < TRUNC(SYSDATE+1)"+
									" UNION ALL "+
									" SELECT DISTINCT LC.IC_IF AS IFINAN "+
									" FROM COM_ANTICIPO A "+
									"      , COM_PEDIDO_SELECCIONADO PS "+
									"      , EMP_LINEA_CREDITO LC "+
									"      , COMCAT_IF  I "+
									"      , COMCAT_ESTATUS_SOLIC ES "+
									"      , COMCAT_MONEDA M "+
									" WHERE A.IC_PEDIDO = PS.IC_PEDIDO "+
									" AND PS.IC_LINEA_CREDITO_EMP = LC.IC_LINEA_CREDITO  "+
									" AND A.IC_ESTATUS_ANTIC = ES.IC_ESTATUS_SOLIC "+
									" AND LC.IC_IF = I.IC_IF "+
									" AND LC.IC_MONEDA = M.IC_MONEDA  "+
									" AND A.DF_FECHA_SOLICITUD >= TRUNC(SYSDATE) " +
									" AND A.DF_FECHA_SOLICITUD < TRUNC(SYSDATE+1) "+
									" UNION ALL "+
									" SELECT DISTINCT LC.IC_IF AS IFINAN "+
									" FROM INV_SOLICITUD S "+
									"      , INV_DISPOSICION D "+
									"      , EMP_LINEA_CREDITO LC "+
									"      , COMCAT_IF  I "+
									"      , COMCAT_ESTATUS_SOLIC ES "+
									"      , COMCAT_MONEDA M "+
									" WHERE S.CC_DISPOSICION = D.CC_DISPOSICION "+
									" AND D.IC_LINEA_CREDITO_EMP = LC.IC_LINEA_CREDITO "+
									" AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
									" AND LC.IC_IF = I.IC_IF "+
									" AND LC.IC_MONEDA = M.IC_MONEDA  "+
									" AND S.DF_AUTORIZACION >= TRUNC(SYSDATE) " +
									" AND S.DF_AUTORIZACION < TRUNC(SYSDATE+1) ";
				}
				*/
				qrySentencia += " ) ORDER BY 1";
				System.out.println("interfasesMonitorResOperBean.getIntermediarios(query): " + qrySentencia);
				ps = con.queryPrecompilado(qrySentencia);
				rs = ps.executeQuery();
				while(rs.next()){
					ids += sep;
			      	ids += rs.getString(1);
			      	sep = ",";
			    }
			    System.out.println(ids);
				rs.close();
				ps.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("interfasesMonitorResOperBean.getIntermediarios(S)");
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return ids;
	}//fin getIntermediarios

	public Vector getConsultaOperaciones(String producto,String ic_if,String estatus){
		String qrySentencia  = "";
		String condicion     = "";
		String condicionIF   = "";
		String condicionEst  = "";
		String group		 = "";
		String estatusCred   = "";
		ResultSet rs 		 = null;
		PreparedStatement ps = null;
		Vector vecFilas		 = new Vector();
		Vector vecColumnas	 = new Vector();
		AccesoDB con 		 = new AccesoDB();

		try {
			System.out.println("interfasesMonitorResOperBean.getConsultaOperaciones(E)");
			con.conexionDB();

		//Condiciones

		/*if(producto.equals("0"))
			estatusCred = ",12";*/
		if(!ic_if.equals("")){
			condicion += " AND I.IC_IF = " + ic_if;
			condicionIF = condicion;
		}
		if(!estatus.equals("")) {
			condicion += " AND ES.IC_ESTATUS_SOLIC = " + estatus;
			condicionEst = condicion;
		} else {
			//condicion += " AND ES.IC_ESTATUS_SOLIC in (1,2,3,4 " + estatusCred + ")";
			condicion += " AND ES.IC_ESTATUS_SOLIC in (1,2,3,4) ";
			condicionEst = condicion;
		}
		
		if(producto.equals("0")||producto.equals("1")) {
			condicion += " GROUP BY S.IC_IF, I.CG_RAZON_SOCIAL, ES.CD_DESCRIPCION, S.IC_MONEDA, M.CD_NOMBRE";
			group = " GROUP BY S.IC_IF, I.CG_RAZON_SOCIAL, ES.CD_DESCRIPCION, S.IC_MONEDA, M.CD_NOMBRE";
		} else { //2,4,5
			condicion += " GROUP BY LC.IC_IF, I.CG_RAZON_SOCIAL, ES.CD_DESCRIPCION, LC.IC_MONEDA, M.CD_NOMBRE";
		}

		//Cr�dito Electr�nico
 		if(producto.equals("0")){

			String qrySentenciaA = " SELECT   "   +
								" 	s.ic_if, "   +
								" 	i.cg_razon_social, "   +
								" 	es.cd_descripcion, "   +
								" 	s.ic_moneda,"   +
								"       m.cd_nombre, "   +
								" 	COUNT (1), "   +
								" 	SUM (s.fn_importe_docto),"   +
								"       SUM (s.fn_importe_dscto)"   +
								"     FROM "   +
								" 	com_solic_portal s,"   +
								" 	comcat_if i,"   +
								" 	comcat_estatus_solic es,"   +
								" 	comcat_moneda m"   +
								"    WHERE "   +
								" 	   s.ic_if = i.ic_if"   +
								"      AND s.ic_estatus_solic = es.ic_estatus_solic"   +
								"      AND s.ic_moneda = m.ic_moneda"   +
								"      AND s.df_carga >= TRUNC (SYSDATE) "   +
								"      AND s.df_carga < TRUNC (SYSDATE+1)"   +
								condicionEst + condicionIF + group;


			String qrySentenciaB = " SELECT   /*+ index(s) use_nl(s, i, es, m)*/"   +
								" 	s.ic_if, "   +
								" 	i.cg_razon_social, "   +
								" 	es.cd_descripcion, "   +
								" 	s.ic_moneda,"   +
								"       m.cd_nombre, "   +
								" 	COUNT (1), "   +
								" 	SUM (s.fn_importe_docto),"   +
								"       SUM (s.fn_importe_dscto)"   +
								"     FROM "   +
								" 	com_solic_portal s,"   +
								" 	comcat_if i,"   +
								" 	comcat_estatus_solic es,"   +
								" 	comcat_moneda m"   +
								"    WHERE "   +
								" 	   s.ic_if = i.ic_if"   +
								"      AND s.ic_estatus_solic = es.ic_estatus_solic"   +
								"      AND s.ic_moneda = m.ic_moneda"   +
								"      AND s.ic_estatus_solic = 12"   +
								"      and s.ic_bloqueo is null"   +
								condicionIF + group;


			if(estatus.equals("")) {
				qrySentencia = qrySentenciaA + " union all " + qrySentenciaB;
			} else {
				if(estatus.equals("12")) {
					qrySentencia = qrySentenciaB;
				} else {
					qrySentencia = qrySentenciaA;
				}
			}

			/*qrySentencia += " SELECT S.IC_IF "+
						    " , I.CG_RAZON_SOCIAL "+
						    " , ES.CD_DESCRIPCION "+
						    " , S.IC_MONEDA "+
						    " , M.CD_NOMBRE "+
						    " , COUNT(1) "+
						    " , SUM(S.FN_IMPORTE_DOCTO) "+
						    " , SUM(S.FN_IMPORTE_DSCTO) "+
							" FROM COM_SOLIC_PORTAL S "+
							"      , COMCAT_IF  I "+
							"      , COMCAT_ESTATUS_SOLIC ES "+
							"      , COMCAT_MONEDA M "+
							" WHERE S.IC_IF = I.IC_IF "+
							" AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
							" AND S.IC_MONEDA = M.IC_MONEDA "+
							" AND S.IC_PRODUCTO_NAFIN IS NULL "+
							" AND ( TRUNC(s.DF_CARGA) = TRUNC(SYSDATE) " +
							"		OR s.ic_estatus_solic = 12 ) " +
							condicion;*/


		}

		//Descuento Electr�nico
 		if(producto.equals("1")){
			qrySentencia += " SELECT S.IC_IF "+
						    " , I.CG_RAZON_SOCIAL "+
						    " , ES.CD_DESCRIPCION "+
						    " , S.IC_MONEDA "+
						    " , M.CD_NOMBRE "+
						    " , COUNT(1) "+
						    " , SUM(S.FN_MONTO) "+
						    " , SUM(S.FN_MONTO_DSCTO) "+
							" FROM COM_SOLICITUD D "+
							"      , COM_DOCUMENTO S "+
							"      , COM_DOCTO_SELECCIONADO SEL "+
							"      , COMCAT_IF  I "+
							"      , COMCAT_ESTATUS_SOLIC ES "+
							"      , COMCAT_MONEDA M "+
							" WHERE D.IC_DOCUMENTO = S.IC_DOCUMENTO "+
							" AND D.IC_DOCUMENTO = SEL.IC_DOCUMENTO "+
							" AND SEL.IC_LINEA_CREDITO_EMP IS NULL "+
							" AND D.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
							" AND S.IC_IF = I.IC_IF "+
							" AND S.IC_MONEDA = M.IC_MONEDA "+
							" AND D.df_fecha_solicitud >= TRUNC(SYSDATE) " +
							" AND D.df_fecha_solicitud < TRUNC(SYSDATE+1)"+
							condicion;
		}
		/*
		//Financiamiento a Pedidos
 		if(producto.equals("2")){
			qrySentencia += " SELECT LC.IC_IF "+
						    " , I.CG_RAZON_SOCIAL "+
						    " , ES.CD_DESCRIPCION "+
						    " , LC.IC_MONEDA "+
						    " , M.CD_NOMBRE "+
						    " , COUNT(1) "+
						    " , SUM(P.FN_MONTO) "+
						    " , SUM(PS.FN_CREDITO_RECIBIR) "+
							" FROM COM_ANTICIPO A "+
							"      , COM_PEDIDO_SELECCIONADO PS "+
							"	   , COM_PEDIDO P "+
							"      , COM_LINEA_CREDITO LC "+
							"      , COMCAT_IF  I "+
							"      , COMCAT_ESTATUS_SOLIC ES "+
							"      , COMCAT_MONEDA M "+
							" WHERE A.IC_PEDIDO = PS.IC_PEDIDO "+
							" AND PS.IC_PEDIDO = P.IC_PEDIDO "+
							" AND PS.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO  "+
							" AND A.IC_ESTATUS_ANTIC = ES.IC_ESTATUS_SOLIC "+
							" AND LC.IC_IF = I.IC_IF "+
							" AND LC.IC_MONEDA = M.IC_MONEDA  "+
							" AND A.DF_FECHA_SOLICITUD >= TRUNC(SYSDATE) " +
							" AND A.DF_FECHA_SOLICITUD < TRUNC(SYSDATE+1) "+
//							" AND TRUNC(DF_FECHA_SOLICITUD) = TRUNC(SYSDATE) " +
							condicion;
		}
		*/
 		//Financiamiento a Distribuidores
 		if(producto.equals("4")){
			qrySentencia += " SELECT IC_IF,NOMBRE,ESTATUS,ICMONEDA,MONEDA,SUM(NUM),SUM(MONTO), SUM(MONTOOP) FROM( "+
							" 	SELECT LC.IC_IF AS IC_IF "+
							"     , I.CG_RAZON_SOCIAL AS NOMBRE "+
							"     , ES.CD_DESCRIPCION AS ESTATUS "+
							"     , LC.IC_MONEDA AS ICMONEDA "+
							"     , M.CD_NOMBRE AS MONEDA "+
							"     , COUNT(1) AS NUM "+
							"     , SUM(D.FN_MONTO) AS MONTO "+
							"     , SUM(DS.FN_IMPORTE_RECIBIR) AS MONTOOP "+
							" 	FROM DIS_SOLICITUD S "+
							"      , DIS_DOCTO_SELECCIONADO DS "+
							"      , DIS_DOCUMENTO D "+
							"      , DIS_LINEA_CREDITO_DM LC "+
							"      , COMCAT_IF  I "+
							"      , COMCAT_ESTATUS_SOLIC ES "+
							"      , COMCAT_MONEDA M "+
							" 	WHERE S.IC_DOCUMENTO = DS.IC_DOCUMENTO "+
							" 	AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO "+
							" 	AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM "+
							" 	AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
							" 	AND LC.IC_IF = I.IC_IF "+
							" 	AND D.IC_MONEDA = M.IC_MONEDA  "+
							"   AND ds.df_fecha_seleccion >= TRUNC(SYSDATE) " +
							"   AND ds.df_fecha_seleccion < TRUNC(SYSDATE + 1) " +
							condicion +
							" UNION "+
							" 	SELECT LC.IC_IF AS IC_IF "+
							"     , I.CG_RAZON_SOCIAL AS NOMBRE "+
							"     , ES.CD_DESCRIPCION AS ESTATUS "+
							"     , LC.IC_MONEDA AS ICMONEDA "+
							"     , M.CD_NOMBRE AS MONEDA "+
							"     , COUNT(1) AS NUM "+
							"     , SUM(D.FN_MONTO) AS MONTO "+
							"     , SUM(DS.FN_IMPORTE_RECIBIR) AS MONTOOP "+
							" 	FROM DIS_SOLICITUD S "+
							" 	     , DIS_DOCTO_SELECCIONADO DS "+
							" 	     , DIS_DOCUMENTO D "+
							" 	     , COM_LINEA_CREDITO LC "+
							" 	     , COMCAT_IF  I "+
							" 		 , COMCAT_ESTATUS_SOLIC ES "+
							" 	     , COMCAT_MONEDA M "+
							" 	WHERE S.IC_DOCUMENTO = DS.IC_DOCUMENTO "+
							" 	AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO "+
							" 	AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO "+
							" 	AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
							" 	AND LC.IC_IF = I.IC_IF "+
							" 	AND D.IC_MONEDA = M.IC_MONEDA  "+
							"   AND ds.df_fecha_seleccion >= TRUNC(SYSDATE) " +
							"   AND ds.df_fecha_seleccion < TRUNC(SYSDATE + 1) " +

							condicion + ") "+
							" 	GROUP BY IC_IF,NOMBRE,ESTATUS,ICMONEDA,MONEDA ";
		}
/*
 		//Credicadenas
 		if(producto.equals("5")){
			qrySentencia += " SELECT LC.IC_IF "+
						    " , I.CG_RAZON_SOCIAL "+
						    " , ES.CD_DESCRIPCION "+
						    " , LC.IC_MONEDA "+
						    " , M.CD_NOMBRE "+
						    " , COUNT(1) "+
						    " , SUM(D.FN_MONTO_CREDITO) "+
						    " , SUM(D.FN_MONTO_CREDITO) "+
							" FROM INV_SOLICITUD S "+
							"      , INV_DISPOSICION D "+
							"      , COM_LINEA_CREDITO LC "+
							"      , COMCAT_IF  I "+
							"      , COMCAT_ESTATUS_SOLIC ES "+
							"      , COMCAT_MONEDA M "+
							" WHERE S.CC_DISPOSICION = D.CC_DISPOSICION "+
							" AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO "+
							" AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
							" AND LC.IC_IF = I.IC_IF "+
							" AND LC.IC_MONEDA = M.IC_MONEDA  "+
							" AND S.DF_AUTORIZACION >= TRUNC(SYSDATE) " +
							" AND S.DF_AUTORIZACION < TRUNC(SYSDATE+1) "+
							//" AND TRUNC(S.DF_AUTORIZACION) = TRUNC(SYSDATE) " +
							condicion;
		}
		
 		//NAFIN EMPRESARIAL
 		if(producto.equals("8")){
			qrySentencia += " SELECT S.IC_IF "+
						    " , I.CG_RAZON_SOCIAL "+
						    " , ES.CD_DESCRIPCION "+
						    " , S.IC_MONEDA "+
						    " , M.CD_NOMBRE "+
						    " , COUNT(1) "+
						    " , SUM(S.FN_MONTO) "+
						    " , SUM(S.FN_MONTO_DSCTO) "+
						    " , 'Factoraje con Recurso' modalidad "+
							" FROM COM_SOLICITUD D "+
							"      , COM_DOCUMENTO S "+
							"      , COM_DOCTO_SELECCIONADO SEL "+
							"      , EMP_LINEA_CREDITO LC "+
							"      , COMCAT_IF  I "+
							"      , COMCAT_ESTATUS_SOLIC ES "+
							"      , COMCAT_MONEDA M "+
							" WHERE D.IC_DOCUMENTO = S.IC_DOCUMENTO "+
							" AND D.IC_DOCUMENTO = SEL.IC_DOCUMENTO "+
							" AND SEL.IC_LINEA_CREDITO_EMP = LC.IC_LINEA_CREDITO "+
							" AND D.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
							" AND S.IC_IF = I.IC_IF "+
							" AND S.IC_MONEDA = M.IC_MONEDA "+
							" AND D.df_fecha_solicitud >= TRUNC(SYSDATE) " +
							" AND D.df_fecha_solicitud < TRUNC(SYSDATE+1)"+
							condicionEst + condicionIF+
							" GROUP BY S.IC_IF, I.CG_RAZON_SOCIAL, ES.CD_DESCRIPCION, S.IC_MONEDA, M.CD_NOMBRE"+
							" UNION ALL "+
							" SELECT LC.IC_IF "+
						    " , I.CG_RAZON_SOCIAL "+
						    " , ES.CD_DESCRIPCION "+
						    " , LC.IC_MONEDA "+
						    " , M.CD_NOMBRE "+
						    " , COUNT(1) "+
						    " , SUM(P.FN_MONTO) "+
						    " , SUM(PS.FN_CREDITO_RECIBIR) "+
						    " , 'Financiamiento a Contratos' modalidad "+
							" FROM COM_ANTICIPO A "+
							"      , COM_PEDIDO_SELECCIONADO PS "+
							"	   , COM_PEDIDO P "+
							"      , EMP_LINEA_CREDITO LC "+
							"      , COMCAT_IF  I "+
							"      , COMCAT_ESTATUS_SOLIC ES "+
							"      , COMCAT_MONEDA M "+
							" WHERE A.IC_PEDIDO = PS.IC_PEDIDO "+
							" AND PS.IC_PEDIDO = P.IC_PEDIDO "+
							" AND PS.IC_LINEA_CREDITO_EMP = LC.IC_LINEA_CREDITO  "+
							" AND A.IC_ESTATUS_ANTIC = ES.IC_ESTATUS_SOLIC "+
							" AND LC.IC_IF = I.IC_IF "+
							" AND LC.IC_MONEDA = M.IC_MONEDA  "+
							" AND A.DF_FECHA_SOLICITUD >= TRUNC(SYSDATE) " +
							" AND A.DF_FECHA_SOLICITUD < TRUNC(SYSDATE+1) "+
							condicionEst + condicionIF+
							" GROUP BY LC.IC_IF, I.CG_RAZON_SOCIAL, ES.CD_DESCRIPCION, LC.IC_MONEDA, M.CD_NOMBRE "+
							" UNION ALL "+
							" SELECT LC.IC_IF "+
						    " , I.CG_RAZON_SOCIAL "+
						    " , ES.CD_DESCRIPCION "+
						    " , LC.IC_MONEDA "+
						    " , M.CD_NOMBRE "+
						    " , COUNT(1) "+
						    " , SUM(D.FN_MONTO_CREDITO) "+
						    " , SUM(D.FN_MONTO_CREDITO) "+
						    " , decode(d.cg_exp_emp,'S', 'Financiamiento de Exportacion', 'Libre Disposicion' ) modalidad "+
							" FROM INV_SOLICITUD S "+
							"      , INV_DISPOSICION D "+
							"      , EMP_LINEA_CREDITO LC "+
							"      , COMCAT_IF  I "+
							"      , COMCAT_ESTATUS_SOLIC ES "+
							"      , COMCAT_MONEDA M "+
							" WHERE S.CC_DISPOSICION = D.CC_DISPOSICION "+
							" AND D.IC_LINEA_CREDITO_EMP = LC.IC_LINEA_CREDITO "+
							" AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC "+
							" AND LC.IC_IF = I.IC_IF "+
							" AND LC.IC_MONEDA = M.IC_MONEDA  "+
							" AND S.DF_AUTORIZACION >= TRUNC(SYSDATE) " +
							" AND S.DF_AUTORIZACION < TRUNC(SYSDATE+1) "+
							condicionEst + condicionIF+
							" GROUP BY LC.IC_IF, I.CG_RAZON_SOCIAL, ES.CD_DESCRIPCION, LC.IC_MONEDA, M.CD_NOMBRE, decode(d.cg_exp_emp,'S', 'Financiamiento de Exportacion', 'Libre Disposicion' ) "+
							" ORDER BY 9, 3, 2 ";
		}
*/
		System.out.println("interfasesMonitorResOperBean.getConsultaOperaciones(query): " +  qrySentencia);
		ps = con.queryPrecompilado(qrySentencia);
		rs = ps.executeQuery();
		while(rs.next()){
			vecColumnas = new Vector();

			vecColumnas.add((rs.getString(1)==null)?"":rs.getString(1));
			vecColumnas.add((rs.getString(2)==null)?"":rs.getString(2));
			vecColumnas.add((rs.getString(3)==null)?"":rs.getString(3));
			vecColumnas.add((rs.getString(4)==null)?"":rs.getString(4));
			vecColumnas.add((rs.getString(5)==null)?"":rs.getString(5));
			vecColumnas.add((rs.getString(6)==null)?"":rs.getString(6));
			vecColumnas.add((rs.getString(7)==null)?"":rs.getString(7));
			vecColumnas.add((rs.getString(8)==null)?"":rs.getString(8));

			/*if(producto.equals("8"))
				vecColumnas.add((rs.getString(9)==null)?"":rs.getString(9));
			*/
			vecFilas.add(vecColumnas);
	    }
		rs.close();
		ps.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println("interfasesMonitorResOperBean.getConsultaOperaciones(S)");
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return vecFilas;
	}//fin getIntermediarios
	
	
	
private final static Log log = ServiceLocator.getInstance().getLog(interfasesMonitorResOperBean.class);
	private String tipoProceso;
	private String ic_producto;


public String impDescargaArchivo(  HttpServletRequest request, List regConsu, List regConsuTo,String path , String tipoArchivo) {
		
		log.debug("impDescargaArchivo (E)");
				
		String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		StringBuffer contenidoArchivo = new StringBuffer();
		try {
			if(tipoArchivo.equals("PDF"))  {
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				/*if(ic_producto.equals("8")) {
					pdfDoc.setTable(8, 100);
					pdfDoc.setCell("Modalidad ","celda01",ComunesPDF.CENTER);
				}else {
					pdfDoc.setTable(7, 100);
				}*/
				pdfDoc.setTable(7, 100);
				pdfDoc.setCell("Clave del IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Raz�n Social ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Operado ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Total de Solicitudes ","celda01",ComunesPDF.CENTER);
			}
			
			if(tipoArchivo.equals("CSV"))  {
				/*if(ic_producto.equals("8")) {					
					contenidoArchivo.append("Modalidad , ");
				}*/
				contenidoArchivo.append("Clave del IF , Raz�n Social, Estatus, Moneda , Monto Documento, Monto Operado, Total de Solicitudes \n ");
			}
				
			for (int i = 0; i <regConsu.size(); i++) {
				HashMap datos = (HashMap)regConsu.get(i);
				 String modalidad= datos.get("MODALIDAD").toString();
				 String claveIF= datos.get("CLAVE_IF").toString();
				 String nombreIF= datos.get("NOMBRE_IF").toString();
				 String estatus= datos.get("ESTATUS").toString();
				 String moneda= datos.get("MONEDA").toString();
				 String monto_doc= datos.get("MONTO_DOCTO").toString();
				 String monto_ope= datos.get("MONTO_OPERADO").toString();
				 String total_solic= datos.get("TOTAL_SOLICITUDES").toString();
				
				if(tipoArchivo.equals("PDF"))  {
					/*if(ic_producto.equals("8")) {
					 pdfDoc.setCell(modalidad,"formas",ComunesPDF.LEFT); 			 
					}*/
					 pdfDoc.setCell(claveIF,"formas",ComunesPDF.CENTER); 			 
					 pdfDoc.setCell(nombreIF,"formas",ComunesPDF.LEFT); 			 
					 pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER); 			 
					 pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER); 
					 pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_doc,2),"formas",ComunesPDF.RIGHT);
					 pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_ope,2),"formas",ComunesPDF.RIGHT);
					 pdfDoc.setCell(total_solic,"formas",ComunesPDF.CENTER); 
				
				}else  if(tipoArchivo.equals("CSV"))  {
					/*if(ic_producto.equals("8")) {					
						contenidoArchivo.append(modalidad.replaceAll(",", "")+",");
					}*/
				contenidoArchivo.append(claveIF.replaceAll(",", "")+","+
											   nombreIF.replaceAll(",", "")+","+
												estatus.replaceAll(",", "")+","+
												moneda.replaceAll(",", "")+","+
												monto_doc.replaceAll(",", "")+","+
												monto_ope.replaceAll(",", "")+","+
												total_solic.replaceAll(",", "")+"\n");
			}
		
		}
		
		if(tipoArchivo.equals("PDF")){
		
			for (int i = 0; i <regConsuTo.size(); i++) {
				HashMap datos = (HashMap)regConsuTo.get(i);
				String moneda= datos.get("MONEDA").toString();
				String monto_docto= datos.get("MONTO_DOCTO").toString();
				String monto_ope= datos.get("MONTO_OPERADO").toString();
				String total_solic= datos.get("TOTAL_SOLICITUDES").toString();		
				
				if(ic_producto.equals("8")) {
				//	pdfDoc.setCell(moneda,"celda01",ComunesPDF.RIGHT,5); 
				}else {
					pdfDoc.setCell(moneda,"celda01",ComunesPDF.RIGHT,4); 
				}
				pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_docto,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_ope,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(total_solic,"formas",ComunesPDF.CENTER);				  
			}
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
		}else if(tipoArchivo.equals("CSV"))  {
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
		}
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}

	public String getIc_producto() {
		return ic_producto;
	}

	public void setIc_producto(String ic_producto) {
		this.ic_producto = ic_producto;
	}
	

}//class interfasesMonitorResOperBean