package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsFiados implements IQueryGeneratorRegExtJS {
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsFiados.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	private String numFiado;
	private String nombre;
	private String rfc;
	private String estado;
	private String terminosCodiciones;
	
	public ConsFiados() {
	}
	
		/**
	 * 
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
/**
	 * 
	 * @return 
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append("  SELECT /*+  LEADING(t2) index(t2 IN_COMCAT_PYME_09_NUK) USE_NL (t6 t2 t8 t1 t7 t5 t3) */	"+
								"	ff.ic_fiado	"+/*,
								"	ff.cg_nombre,	"+
								"	ff.cg_rfc,	"+
								"	ff.cg_estado,	"+
								"	ff.ic_estrato,	"+
								"	ce.cd_nombre cd_nombre,	"+ 
								"	ff.cg_rl_telefono,	"+
								"	TO_CHAR (ff.df_alta, 'dd/mm/yyyy HH:mi:ss') df_alta,	"+
								"	ff.cs_aceptacion_terminos, ff.cg_version_terminos,	"+
								"	TO_CHAR (ff.df_acepta_terminos,'dd/mm/yyyy HH:mi:ss') df_acepta_terminos	"+*/
								"  FROM fe_fiado ff,	"+
								"		comrel_nafin cn,	"+
								"		comcat_estrato ce	"+
								"  WHERE ff.ic_fiado = cn.ic_epo_pyme_if	"+
								"		AND ff.ic_estrato = ce.ic_estrato	"+
								"		AND cn.cg_tipo ='F'	");
								
			if (!numFiado.equals("")){
				qrySentencia.append("	AND ff.ic_fiado = ?	");
				conditions.add(numFiado);
			}
			if(!nombre.equals("")){
				qrySentencia.append("	AND ff.cg_nombre like ?	") ;
				conditions.add(nombre.toUpperCase()+"%");
			}
			if(!estado.equals("")){
				qrySentencia.append("	AND ff.cg_estado = ?	");
				conditions.add(estado);
			}	
			if(!rfc.equals("")){
				qrySentencia.append(" AND ff.cg_rfc = ? ");
				conditions.add(rfc);
			}	
			if(terminosCodiciones.equals("S")){
				qrySentencia.append(" AND ff.cs_aceptacion_terminos = ? ");
				conditions.add(terminosCodiciones.toUpperCase());
			}		
			qrySentencia.append("	ORDER BY ff.ic_fiado ASC	");
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
		qrySentencia.append("  SELECT /*+  LEADING(t2) index(t2 IN_COMCAT_PYME_09_NUK) USE_NL (t6 t2 t8 t1 t7 t5 t3) */	"+
								"	ff.ic_fiado,	"+
								"	ff.cg_nombre,	"+
								"	ff.cg_rfc,	"+
								"	ff.cg_estado,	"+
								"	ff.ic_estrato,	"+
								"	ce.cd_nombre cd_nombre,	"+ 
								"	ff.cg_rl_telefono,	"+
								"	TO_CHAR (ff.df_alta, 'dd/mm/yyyy HH:mi:ss') df_alta,	"+
								"	ff.cs_aceptacion_terminos, ff.cg_version_terminos,	"+
								"	TO_CHAR (ff.df_acepta_terminos,'dd/mm/yyyy HH:mi:ss') df_acepta_terminos	"+
								"  FROM fe_fiado ff,	"+
								"		comrel_nafin cn,	"+
								"		comcat_estrato ce	"+
								"  WHERE ff.ic_fiado = cn.ic_epo_pyme_if	"+
								"		AND ff.ic_estrato = ce.ic_estrato	"+
								"		AND cn.cg_tipo ='F'	");
								
			if (!numFiado.equals("")){
				qrySentencia.append("	AND ff.ic_fiado = ?	");
				conditions.add(numFiado);
			}
			if(!nombre.equals("")){
				qrySentencia.append("	AND ff.cg_nombre like ?	") ;
				conditions.add(nombre.toUpperCase()+"%");
			}
			if(!estado.equals("")){
				qrySentencia.append("	AND ff.cg_estado = ?	");
				conditions.add(estado);
			}	
			if(!rfc.equals("")){
				qrySentencia.append(" AND ff.cg_rfc = ? ");
				conditions.add(rfc);
			}	
			if(terminosCodiciones.equals("S")){
				qrySentencia.append(" AND ff.cs_aceptacion_terminos = ? ");
				conditions.add(terminosCodiciones.toUpperCase());
			}		
			
			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					condicion.append(" AND ff.ic_fiado in (");
				}
				condicion.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					condicion.append(",");
				} else {
					condicion.append(" ) ");
				}			
			}
			qrySentencia.append(condicion.toString());
			
			qrySentencia.append("	ORDER BY ff.ic_fiado ASC	");
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
		qrySentencia.append("  SELECT /*+  LEADING(t2) index(t2 IN_COMCAT_PYME_09_NUK) USE_NL (t6 t2 t8 t1 t7 t5 t3) */	"+
								"	ff.ic_fiado,	"+
								"	ff.cg_nombre,	"+
								"	ff.cg_rfc,	"+
								"	ff.cg_estado,	"+
								"	ff.ic_estrato,	"+
								"	ce.cd_nombre cd_nombre,	"+ 
								"	ff.cg_rl_telefono,	"+
								"	TO_CHAR (ff.df_alta, 'dd/mm/yyyy HH:mi:ss') df_alta,	"+
								"	ff.cs_aceptacion_terminos, ff.cg_version_terminos,	"+
								"	TO_CHAR (ff.df_acepta_terminos,'dd/mm/yyyy HH:mi:ss') df_acepta_terminos	"+
								"  FROM fe_fiado ff,	"+
								"		comrel_nafin cn,	"+
								"		comcat_estrato ce	"+
								"  WHERE ff.ic_fiado = cn.ic_epo_pyme_if	"+
								"		AND ff.ic_estrato = ce.ic_estrato	"+
								"		AND cn.cg_tipo ='F'	");
								
			if (!numFiado.equals("")){
				qrySentencia.append("	AND ff.ic_fiado = ?	");
				conditions.add(numFiado);
			}
			if(!nombre.equals("")){
				qrySentencia.append("	AND ff.cg_nombre like ?	") ;
				conditions.add(nombre.toUpperCase()+"%");
			}
			if(!estado.equals("")){
				qrySentencia.append("	AND ff.cg_estado = ?	");
				conditions.add(estado);
			}	
			if(!rfc.equals("")){
				qrySentencia.append(" AND ff.cg_rfc = ? ");
				conditions.add(rfc);
			}	
			if(terminosCodiciones.equals("S")){
				qrySentencia.append(" AND ff.cs_aceptacion_terminos = ? ");
				conditions.add(terminosCodiciones.toUpperCase());
			}		
			qrySentencia.append("	ORDER BY ff.ic_fiado ASC	");
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}


public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
			pdfDoc.setTable(10,100);
			pdfDoc.setCell("No. Fiado ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre o Raz�n Social ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("RFC ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estado	","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estrato	","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tel�fono Representante Legal ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Alta ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Aceptaci�n t�rminos y condiciones ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Versi�n t�rminos y condiciones ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell(" Fecha de Aceptaci�n ","celda01",ComunesPDF.CENTER); 
			
			
			while (rs.next())	{					
				String numFiado = (rs.getString("IC_FIADO") == null) ? "" : rs.getString("IC_FIADO");
				String nombre = (rs.getString("CG_NOMBRE") == null) ? "" : rs.getString("CG_NOMBRE");
				String rfc = (rs.getString("CG_RFC") == null) ? "" : rs.getString("CG_RFC");
				String estado = (rs.getString("CG_ESTADO") == null) ? "" : rs.getString("CG_ESTADO");
				String claveEstrato = (rs.getString("IC_ESTRATO") == null) ? "" : rs.getString("IC_ESTRATO");
				String estrato	=(rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
				String telefono = (rs.getString("CG_RL_TELEFONO") == null) ? "" : rs.getString("CG_RL_TELEFONO");
				String fechaAlta = (rs.getString("DF_ALTA") == null) ? "" : rs.getString("DF_ALTA");
				String terminosCondiciones = (rs.getString("CS_ACEPTACION_TERMINOS") == null) ? "" : rs.getString("CS_ACEPTACION_TERMINOS");//S/N
				String versionTerminos = (rs.getString("CG_VERSION_TERMINOS") == null) ? "" : rs.getString("CG_VERSION_TERMINOS");
				String fechaAceptacion = (rs.getString("DF_ACEPTA_TERMINOS") == null) ? "" : rs.getString("DF_ACEPTA_TERMINOS");
				String terminosCondicionesAux="";
					if(terminosCondiciones.equals("S")){
						terminosCondicionesAux="SI";
					}else{
						terminosCondicionesAux="NO";
					}
								
				pdfDoc.setCell(numFiado,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombre,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(rfc,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(estado,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(estrato,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaAlta,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(terminosCondicionesAux,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(versionTerminos,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(fechaAceptacion,"formas",ComunesPDF.LEFT);
			}
		
			pdfDoc.addTable();
			pdfDoc.endDocument();	
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();	
			
			contenidoArchivo.append("No. Fiado,	Nombre o Raz�n Social,"+
											"RFC,	Estado,	Estrato,	Tel�fono Representante Legal,"+
											"Fecha de Alta,	Aceptaci�n t�rminos y condiciones,"+
											"Versi�n t�rminos y condiciones,	 Fecha de Aceptaci�n\n");

			
		while (rs.next())	{					
				String numFiado = (rs.getString("IC_FIADO") == null) ? "" : rs.getString("IC_FIADO");
				String nombre = (rs.getString("CG_NOMBRE") == null) ? "" : rs.getString("CG_NOMBRE");
				String rfc = (rs.getString("CG_RFC") == null) ? "" : rs.getString("CG_RFC");
				String estado = (rs.getString("CG_ESTADO") == null) ? "" : rs.getString("CG_ESTADO");
				String claveEstrato = (rs.getString("IC_ESTRATO") == null) ? "" : rs.getString("IC_ESTRATO");
				String estrato	=(rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
				String telefono = (rs.getString("CG_RL_TELEFONO") == null) ? "" : rs.getString("CG_RL_TELEFONO");
				String fechaAlta = (rs.getString("DF_ALTA") == null) ? "" : rs.getString("DF_ALTA");
				String terminosCondiciones = (rs.getString("CS_ACEPTACION_TERMINOS") == null) ? "" : rs.getString("CS_ACEPTACION_TERMINOS");//S/N
				String versionTerminos = (rs.getString("CG_VERSION_TERMINOS") == null) ? "" : rs.getString("CG_VERSION_TERMINOS");
				String fechaAceptacion = (rs.getString("DF_ACEPTA_TERMINOS") == null) ? "" : rs.getString("DF_ACEPTA_TERMINOS");
				String terminosCondicionesAux="";
					if(terminosCondiciones.equals("S")){
						terminosCondicionesAux="SI";
					}else{
						terminosCondicionesAux="NO";
					}
			
				contenidoArchivo.append(numFiado.replace(',',' ')+", "+
												nombre.replace(',',' ')+", "+
												rfc.replace(',',' ')+", "+
												estado.replace(',',' ')+", "+
												estrato.replace(',',' ')+", "+
												telefono.replace(',',' ')+", "+
												fechaAlta.replace(',',' ')+", "+
												terminosCondicionesAux.replace(',',' ')+", "+
												versionTerminos.replace(',',' ')+", "+
												fechaAceptacion.replace(',',' ')+"\n "
												);		
			}
				
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}

	public String imprimirCuentas(HttpServletRequest request, List cuentas, String tituloF,  String tituloC, String path ) {  
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		try {
				
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText(tituloF,"formas",ComunesPDF.CENTER);
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			
			pdfDoc.setTable(1,30);			
			pdfDoc.setCell(tituloC, "formas",ComunesPDF.LEFT);  
			pdfDoc.setCell("Login del Usuario: ", "celda02",ComunesPDF.CENTER);	
			  
			
			Iterator itCuentas = cuentas.iterator();	
			while (itCuentas.hasNext()) {
				String cuenta = (String) itCuentas.next();		
				pdfDoc.setCell(cuenta, "formas",ComunesPDF.CENTER);	
			}	
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
			
		 
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	public Registros getInfoModificarFiados(){
		log.info("getInfoModificarFiados(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
		AccesoDB con = new AccesoDB(); 
		
		
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	" SELECT	"+ 
											" ff.ic_fiado,	"+
											" ff.cg_nombre nombrefiado,	"+
											" ff.cg_rfc rfcfiado,	"+
											" ff.cg_calle callefiado, 	"+
											" ff.cg_referencia reffiado,	"+
											" ff.cg_noexterior noexteriorfiado, 	"+
											" ff.cg_municipio municfiado,	"+
											" ff.cg_nointerior nointfiado,	"+
											" ff.cg_estado estadofiado,	"+
											" ff.cg_colonia colfiado, 	"+
											" ff.cg_pais paisfiado, 	"+
											" ff.cg_localidad locfiado,	"+
											" ff.cg_cp cpfiado,	"+ 
											" ff.cg_rl_nombre nombrerl,	"+
											" ff.cg_rl_telefono telefonorl,	"+
											" ff.cg_rl_correo emailrl,	"+
											" ff.fg_ventas_anuales ventfiado,	"+
											" ff.ic_sector sectorecfiado,	"+
											" ff.ig_numero_epleados numempfiado,	"+
											" ff.ic_estrato cveestratofiado,	"+
											" ce.cd_nombre estratofiado	"+
										" FROM fe_fiado ff, comrel_nafin cn, comcat_estrato ce	"+
										" WHERE ff.ic_fiado = cn.ic_epo_pyme_if	"+
											" AND ff.ic_estrato = ce.ic_estrato	"+
											" AND cn.cg_tipo = 'F'	"+
											" AND ff.ic_fiado = ?");  
			conditions.add(numFiado);											

			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getInfoModificar  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("qrySentencia : "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getInfoModificarFiados (S) ");
		return registros;
		
	}
	
	
	public List  getDatosCatalogoSectorEconomicoList( ){
		log.info("getDatosCatalogoSectorEconomicoList (E) ");
		AccesoDB con = new AccesoDB();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
			String 	strSQL =	"select IC_TIPO_SECTOR as clave,initcap(CD_NOMBRE) as descripcion from COMCAT_TIPO_SECTOR	";  
			log.debug(" strSQL SECTOR ECON  "+strSQL );
			pstmt=con.queryPrecompilado(strSQL);
			rs = pstmt.executeQuery();		
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString("clave") );
				datos.put("descripcion", rs.getString("descripcion"));
				registros.add(datos);
			}
			rs.close();
			con.cierraStatement();
		
		} catch (Exception e) {
			log.error("getDatosCatalogoSectorEconomicoList  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getDatosCatalogoSectorEconomicoList (S) ");
		return registros;
	}
	
	public Registros getDatosAfiliado(){
	log.info("getDatosAfiliado (E) ");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
		AccesoDB con = new AccesoDB(); 
		
		
		Registros registros  = new Registros();
		try{
			con.conexionDB();
				qrySentencia.append(	" 	SELECT afiliado.cg_nombre cg_razon_social, n.ic_nafin_electronico	"+ 
										"	FROM fe_fiado afiliado, comrel_nafin n	"+
										"	WHERE afiliado.ic_fiado = n.ic_epo_pyme_if	"+
										"	AND n.cg_tipo = 'F'	"+
										"	AND n.ic_epo_pyme_if = ?");  
			conditions.add(numFiado);											

			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosAfiliado  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("qrySentencia : "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDatosAfiliado (S) ");
		return registros;
	
	
	}
/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/	


	public void setNumFiado(String numFiado) {
		this.numFiado = numFiado;
	}


	public String getNumFiado() {
		return numFiado;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getNombre() {
		return nombre;
	}


	public void setRfc(String rfc) {
		this.rfc = rfc;
	}


	public String getRfc() {
		return rfc;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public String getEstado() {
		return estado;
	}


	public void setTerminosCodiciones(String terminosCodiciones) {
		this.terminosCodiciones = terminosCodiciones;
	}


	public String getTerminosCodiciones() {
		return terminosCodiciones;
	}
	
}