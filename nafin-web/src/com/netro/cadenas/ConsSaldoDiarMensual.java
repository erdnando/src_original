package com.netro.cadenas;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsSaldoDiarMensual implements  IQueryGeneratorRegExtJS {
	private static final Log log = ServiceLocator.getInstance().getLog(ConsSaldoDiarMensual.class);//Variable para enviar mensajes al log.
	public ConsSaldoDiarMensual() {  }
	private List 	conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	
	private String 	selecBoton;
	private String claveIf;
	private String claveCliente;
	StringBuffer 	strQuery;
	StringBuffer qrySentencia = new StringBuffer("");
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
 	}  
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
	
		conditions = new ArrayList();
		String condicion = "";
		log.info("getDocumentQueryFile(E)");
		qrySentencia = new StringBuffer();
		if(selecBoton.equals("Diario")){
			if(!"".equals(claveIf))
				condicion+="    and pec.CODIGO_FINANCIERA = "+claveIf ; 
      if(claveCliente!=null && !"".equals(claveCliente))
        condicion+="    and pec.CODIGO_CLIENTE = "+claveCliente ; 
		qrySentencia.append(
			
			" 	SELECT   /*+ use_nl(ca co ce)*/  pec.MONEDA, ca.CD_NOMBRE,pec.CODIGO_AGENCIA,pec.DESC_AGENCIA, pec.CODIGO_FINANCIERA," +
			" 	pec.DESC_FINANCIERA, pec.CODIGO_BASE_OPERACION,co.CG_DESCRIPCION,pec.CODIGO_SUB_APLICACION," +
			" 	pec.DESC_SUB_APLICACION, pec.CODIGO_CLIENTE, pec.DESC_CLIENTE, pec.NUMERO_PRESTAMO," +
			" 	pec.NUM_ELECTRONICO, pec.CODIGO_EPO,ce.CG_NOMBRE_COMERCIAL, " +
			
			"  TO_CHAR(pec.FECHA_DEBE_DESDE,'dd/mm/yyyy') AS FECHA_DEBE_DESDE, pec.CUOTAS_VENCIDAS, TO_CHAR( pec.FECHA_APERTURA,'dd/mm/yyyy') AS FECHA_APERTURA ," +
			"	TO_CHAR( pec.FECHA_VENCIMIENTO,'dd/mm/yyyy') AS FECHA_VENCIMIENTO,  pec.FRECUENCIA_CAPITAL, pec.FRECUENCIA_INTERES," +
			"	TO_CHAR( pec.FECHA_PRIMER_PAGO_CAPITAL,'dd/mm/yyyy') AS FECHA_PRIMER_PAGO_CAPITAL, TO_CHAR( pec.FECHA_PRIMER_PAGO_INTERES,'dd/mm/yyyy') AS FECHA_PRIMER_PAGO_INTERES, TO_CHAR( pec.FECHA_PROXIMO_PAGO_CAPITAL,'dd/mm/yyyy') AS FECHA_PROXIMO_PAGO_CAPITAL," +
			"	TO_CHAR( pec.FECHA_PROXIMO_PAGO_INTERES,'dd/mm/yyyy') AS FECHA_PROXIMO_PAGO_INTERES, TO_CHAR(pec.FECHA_ULTIMO_PAGO,'dd/mm/yyyy') AS FECHA_ULTIMO_PAGO, pec.CODIGO_VALOR_TASA_CARTERA," +
			"	pec.DESC_VALOR_TASA_CARTERA, pec.TASA_TOTAL,pec.TIPO_INTERES,  " +
			"	pec.DIA_REVISION_TASA, pec.MONTO_INICIAL, pec.SALDO_INSOLUTO,pec.CAPITAL_VIGENTE, " +
			"	pec.CAPITAL_VENCIDO, pec.INTERES_VIGENTE,pec.INTERES_VENCIDO,pec.MORA100  " +
			"	FROM pr_edocta_cartera pec, comcat_moneda ca, COMCAT_BASE_OPERACION co, COMCAT_EPO ce " +
			"	where pec.MONEDA = ca.ic_moneda" +
			"	AND pec.CODIGO_BASE_OPERACION = co.IG_CODIGO_BASE" +
			"	and pec.CODIGO_EPO = ce.IC_EPO(+) "+condicion
			
			);    
		}else if(selecBoton.equals("Mensual")){
			if(!"".equals(claveIf))
				condicion+="    and lp.CODIGO_FINANCIERA = "+claveIf ; 
      if(claveCliente!=null && !"".equals(claveCliente))
        condicion+="    and per.CLIENTE = "+claveCliente ; 
			qrySentencia.append(
			" 	select  /*+ use_nl ( cm cbo) */ TO_CHAR(per.FECHAFINMES,'dd/mm/yyyy') AS FECHAFINMES, per.PRESTAMO,per.MONEDA, cm.CD_NOMBRE,per.INTERMEDIARIO, " +
			" 	per.DESCRIPCION_INTER,per.CODSUCURSAL,per.NOMSUCURSAL, lp.CODIGO_BASE_OPERACION, " +
			" 	cbo.CG_DESCRIPCION, per.SUBAPL,per.DESCRIPCION_SAPL,per.CLIENTE, per.NOMBRECLIENTE," +
			" per.NUMELECTRONICO , TO_CHAR( per.FECHOPERACION,'dd/mm/yyyy') AS FECHOPERACION, TO_CHAR( per.FECHVENCIMIENTO,'dd/mm/yyyy') AS FECHVENCIMIENTO,  " +
			"  per.DESCRIPCIONTASA,per.TASAMORATORIA,TO_CHAR(per.FACUOTAVEN,'dd/mm/yyyy') AS FACUOTAVEN,   " +
			" per.MONTOOPERADO , per.SALDOINSOLUTO , per.CAPITALVIGENTE , per.CAPITALVENCIDO , per.INTERESPROVI , " +
			" per.INTERESVENCIDO , nvl(per.INTERESMORAT,0) + nvl(per.SOBRETASAMOR, 0)   as SOBRETASAMORGRAVADO, per.ADEUDOTOTAL, per.TASATOTAL" +
			" 	from PR_EDOCTA_REP per, COMCAT_MONEDA cm, COMCAT_BASE_OPERACION cbo, LC_PROYECTO lp " +
			" where per.MONEDA = cm.IC_MONEDA" +
			" and per.proyecto = lp.NUMERO_LINEA_CREDITO" +
			" and lp.CODIGO_BASE_OPERACION = cbo.IG_CODIGO_BASE "+
			condicion
			);
		}
		log.debug("getDocumentQueryFile(S) "); 
		log.debug("qrySentencia "+qrySentencia);
		return qrySentencia.toString();		
		
 	} 
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet reg, String path, String tipo) {
	
		
		System.out.println("crearCustomFile()");
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();  //2000 es la capacidad inicial reservada
		String nombreArchivo = "";
		BufferedWriter buffer = null;
		OutputStreamWriter writer = null;
		int numreg = 0;
		try{
			contenidoArchivo = new StringBuffer();	
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
			if (tipo.equals("TXT")) {
		
		/*******************
		Cabecera del archivo
		********************/
			if(selecBoton.equals("Diario")){
				contenidoArchivo.append(
					" MONEDA;  DESCRIPCION_MONEDA;  CODSUCURSAL; NOMSUCURSAL; "+
          //("C".equals(tipoLinea)?"":"INTERMEDIARIO; DESCRIPCION_INTER; ")+
          "CODIGO_BASE_OPERACION; DESCRIPCION_BASE_DE_OPERACION; TIPO_CREDITO;DESCRIPCION_TIPO_CREDITO"+
					";CLIENTE;NOMBRECLIENTE;NUMERO_PRESTAMO;NUM_ELECTRONICO;"+
          //("C".equals(tipoLinea)?"":"CODIGO_EPO;DESCRIPCION_EPO;")+
          "FECHA_DEBE_DESDE;CUOTAS_VENCIDAS;FECHA_APERTURA;FECHA_VENCIMIENTO"+
					";FRECUENCIA_CAPITAL;FRECUENCIA_INTERES;FECHA_PRIMER_PAGO_CAPITAL;FECHA_PRIMER_PAGO_INTERES;FECHA_PROXIMO_PAGO_CAPITAL;FECHA_PROXIMO_PAGO_INTERES;FECHA_ULTIMO_PAGO;TASAREFERENCIAL;DESCRIPCIONTASA;TASA_TOTAL"+
					";TIPO_INTERES;DIA_REVISION_TASA;MONTOOPERADO;SALDO_INSOLUTO;CAPITAL_VIGENTE;CAPITAL_VENCIDO;INTERES_VIGENTE;INTERES_VENCIDO;MORATORIOS \n");
			}else if(selecBoton.equals("Mensual")){
					contenidoArchivo.append(
					"FECHAFINMES;PRESTAMO;MONEDA;DESCRIPCION_MONEDA;"+
          //("C".equals(tipoLinea)?"":"INTERMEDIARIO; DESCRIPCION_INTER; ")+
          "CODSUCURSAL;NOMSUCURSAL;BASE_DE_OPERACI�N;DESCRIPCION_BASE_DE_OPERACI�N;TIPO CREDITO"+
					";DESCRIPCION_TIPO_CREDITO;CLIENTE;NOMBRECLIENTE;NUMELECTRONICO;FECHOPERACION;FECHVENCIMIENTO;DESCRIPCIONTASA;TASATOTAL;TASAMORATORIA;1a CUOTAVENCIDA"+
					";MONTOOPERADO;SALDOINSOLUTO;CAPITALVIGENTE;CAPITALVENCIDO;INTERES_DEVENG_AL_CIERRE;INTERESVENCIDO;TOTAL_MORATORIOS;ADEUDOTOTAL \n"
						);
			}
		/***************************
		 Generacion del archivo  
		/***************************/
		while(reg.next()){
			if(selecBoton.equals("Diario")){
				String MONEDA		= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");	
				String DESCRIPCION_MONEDA		= (reg.getString("CD_NOMBRE")==null)?"":reg.getString("CD_NOMBRE");	
				String CODSUCURSAL		= (reg.getString("CODIGO_AGENCIA")==null)?"":reg.getString("CODIGO_AGENCIA");	
				String NOMSUCURSAL		= (reg.getString("DESC_AGENCIA")==null)?"":reg.getString("DESC_AGENCIA");	
				String INTERMEDIARIO		= (reg.getString("CODIGO_FINANCIERA")==null)?"":reg.getString("CODIGO_FINANCIERA");
				String DESCRIPCION_INTER		= (reg.getString("DESC_FINANCIERA")==null)?"":reg.getString("DESC_FINANCIERA");	
				String CODIGO_BASE_OPERACION		= (reg.getString("CODIGO_BASE_OPERACION")==null)?"":reg.getString("CODIGO_BASE_OPERACION");	
				String DESCRIPCION_BASE_DE_OPERACION		= (reg.getString("CG_DESCRIPCION")==null)?"":reg.getString("CG_DESCRIPCION");	
				String TIPO_CREDITO		= (reg.getString("CODIGO_SUB_APLICACION")==null)?"":reg.getString("CODIGO_SUB_APLICACION");	
				String DESCRIPCION_TIPO_CREDITO		= (reg.getString("DESC_SUB_APLICACION")==null)?"":reg.getString("DESC_SUB_APLICACION");	
				String CLIENTE		= (reg.getString("CODIGO_CLIENTE")==null)?"":reg.getString("CODIGO_CLIENTE");	
				String NOMBRECLIENTE		= (reg.getString("DESC_CLIENTE")==null)?"":reg.getString("DESC_CLIENTE");	
				String NUMERO_PRESTAMO		= (reg.getString("NUMERO_PRESTAMO")==null)?"":reg.getString("NUMERO_PRESTAMO");	
				String NUM_ELECTRONICO		= (reg.getString("NUM_ELECTRONICO")==null)?"":reg.getString("NUM_ELECTRONICO");	
				String CODIGO_EPO		= (reg.getString("CODIGO_EPO")==null)?"":reg.getString("CODIGO_EPO");	
				String DESCRIPCION_EPO		= (reg.getString("CG_NOMBRE_COMERCIAL")==null)?"":reg.getString("CG_NOMBRE_COMERCIAL");	
				String FECHA_DEBE_DESDE		= (reg.getString("FECHA_DEBE_DESDE")==null)?"":reg.getString("FECHA_DEBE_DESDE");
				String CUOTAS_VENCIDAS		= (reg.getString("CUOTAS_VENCIDAS")==null)?"":reg.getString("CUOTAS_VENCIDAS");	
				String FECHA_APERTURA		= (reg.getString("FECHA_APERTURA")==null)?"":reg.getString("FECHA_APERTURA");	
				String FECHA_VENCIMIENTO		= (reg.getString("FECHA_VENCIMIENTO")==null)?"":reg.getString("FECHA_VENCIMIENTO");	
				String FRECUENCIA_CAPITAL		= (reg.getString("FRECUENCIA_CAPITAL")==null)?"":reg.getString("FRECUENCIA_CAPITAL");	
				String FRECUENCIA_INTERES		= (reg.getString("FRECUENCIA_INTERES")==null)?"":reg.getString("FRECUENCIA_INTERES");	
				String FECHA_PRIMER_PAGO_CAPITAL		= (reg.getString("FECHA_PRIMER_PAGO_CAPITAL")==null)?"":reg.getString("FECHA_PRIMER_PAGO_CAPITAL");	
				String FECHA_PRIMER_PAGO_INTERES		= (reg.getString("FECHA_PRIMER_PAGO_INTERES")==null)?"":reg.getString("FECHA_PRIMER_PAGO_INTERES");	
				String FECHA_PROXIMO_PAGO_CAPITAL		= (reg.getString("FECHA_PROXIMO_PAGO_CAPITAL")==null)?"":reg.getString("FECHA_PROXIMO_PAGO_CAPITAL");	
				String FECHA_PROXIMO_PAGO_INTERES		= (reg.getString("FECHA_PROXIMO_PAGO_INTERES")==null)?"":reg.getString("FECHA_PROXIMO_PAGO_INTERES");	
				String FECHA_ULTIMO_PAGO		= (reg.getString("FECHA_ULTIMO_PAGO")==null)?"":reg.getString("FECHA_ULTIMO_PAGO");
				String TASAREFERENCIAL		= (reg.getString("CODIGO_VALOR_TASA_CARTERA")==null)?"":reg.getString("CODIGO_VALOR_TASA_CARTERA");	
				String DESCRIPCIONTASA		= (reg.getString("DESC_VALOR_TASA_CARTERA")==null)?"":reg.getString("DESC_VALOR_TASA_CARTERA");	
				String TASA_TOTAL		= (reg.getString("TASA_TOTAL")==null)?"":reg.getString("TASA_TOTAL");	
				String TIPO_INTERES		= (reg.getString("TIPO_INTERES")==null)?"":reg.getString("TIPO_INTERES");	
				String DIA_REVISION_TASA		= (reg.getString("DIA_REVISION_TASA")==null)?"":reg.getString("DIA_REVISION_TASA");	
				String MONTOOPERADO		= (reg.getString("MONTO_INICIAL")==null)?"":reg.getString("MONTO_INICIAL");
				String SALDO_INSOLUTO		= (reg.getString("SALDO_INSOLUTO")==null)?"":reg.getString("SALDO_INSOLUTO");	
				String CAPITAL_VIGENTE		= (reg.getString("CAPITAL_VIGENTE")==null)?"":reg.getString("CAPITAL_VIGENTE");	
				String CAPITAL_VENCIDO		= (reg.getString("CAPITAL_VENCIDO")==null)?"":reg.getString("CAPITAL_VENCIDO");	
				String INTERES_VIGENTE		= (reg.getString("INTERES_VIGENTE")==null)?"":reg.getString("INTERES_VIGENTE");	
				String INTERES_VENCIDO		= (reg.getString("INTERES_VENCIDO")==null)?"":reg.getString("INTERES_VENCIDO");	
				String MORA100		= (reg.getString("MORA100")==null)?"":reg.getString("MORA100");
				contenidoArchivo.append( 
						MONEDA + 
						";" + DESCRIPCION_MONEDA + 
						";" + CODSUCURSAL + 
						";"+ NOMSUCURSAL+
						//";"+INTERMEDIARIO+
						//";"+DESCRIPCION_INTER+
						";"+CODIGO_BASE_OPERACION+
						";"+DESCRIPCION_BASE_DE_OPERACION+
						";"+TIPO_CREDITO+
						";"+DESCRIPCION_TIPO_CREDITO+
						";"+CLIENTE+
						";"+NOMBRECLIENTE+
						";"+NUMERO_PRESTAMO+
						";"+ NUM_ELECTRONICO +
						//";" + CODIGO_EPO +
						//";"+DESCRIPCION_EPO + 
						";" + FECHA_DEBE_DESDE +
						";"+ CUOTAS_VENCIDAS +
						";" +FECHA_APERTURA + 
						";" + FECHA_VENCIMIENTO +
						";" + FRECUENCIA_CAPITAL +
						";"+ FRECUENCIA_INTERES+
						";"+FECHA_PRIMER_PAGO_CAPITAL+
						";"+FECHA_PRIMER_PAGO_INTERES+
						";"+FECHA_PROXIMO_PAGO_CAPITAL+
						";"+ FECHA_PROXIMO_PAGO_INTERES +
						";" + FECHA_ULTIMO_PAGO +
						";" + TASAREFERENCIAL + 
						";" +DESCRIPCIONTASA+ 
						";"+TASA_TOTAL +
						";" + TIPO_INTERES + 
						";"+ DIA_REVISION_TASA + 
						";"	+ MONTOOPERADO +
						";" + SALDO_INSOLUTO +
						";"+ CAPITAL_VIGENTE  +
						";"+ CAPITAL_VENCIDO  +
						";" + INTERES_VIGENTE +
						";" + INTERES_VENCIDO +
						";"	+ MORA100 +"\n" );
			}else if(selecBoton.equals("Mensual")){
				String FECHAFINMES		= (reg.getString("FECHAFINMES")==null)?"":reg.getString("FECHAFINMES");	
				String PRESTAMO		= (reg.getString("PRESTAMO")==null)?"":reg.getString("PRESTAMO");	
				String MONEDA		= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");	
				String CD_NOMBRE		= (reg.getString("CD_NOMBRE")==null)?"":reg.getString("CD_NOMBRE");	
				String INTERMEDIARIO		= (reg.getString("INTERMEDIARIO")==null)?"":reg.getString("INTERMEDIARIO");	
				String DESCRIPCION_INTER		= (reg.getString("DESCRIPCION_INTER")==null)?"":reg.getString("DESCRIPCION_INTER");	
				String CODSUCURSAL		= (reg.getString("CODSUCURSAL")==null)?"":reg.getString("CODSUCURSAL");	
				String NOMSUCURSAL		= (reg.getString("NOMSUCURSAL")==null)?"":reg.getString("NOMSUCURSAL");	
				String CODIGO_BASE_OPERACION		= (reg.getString("CODIGO_BASE_OPERACION")==null)?"":reg.getString("CODIGO_BASE_OPERACION");	
				String CG_DESCRIPCION		= (reg.getString("CG_DESCRIPCION")==null)?"":reg.getString("CG_DESCRIPCION");	
				String SUBAPL		= (reg.getString("SUBAPL")==null)?"":reg.getString("SUBAPL");	
				String DESCRIPCION_SAPL		= (reg.getString("DESCRIPCION_SAPL")==null)?"":reg.getString("DESCRIPCION_SAPL");	
				String CLIENTE		= (reg.getString("CLIENTE")==null)?"":reg.getString("CLIENTE");	
				String NOMBRECLIENTE		= (reg.getString("NOMBRECLIENTE")==null)?"":reg.getString("NOMBRECLIENTE");
				String NUMELECTRONICO		= (reg.getString("NUMELECTRONICO")==null)?"":reg.getString("NUMELECTRONICO");
				String FECHOPERACION		= (reg.getString("FECHOPERACION")==null)?"":reg.getString("FECHOPERACION");	
				String FECHVENCIMIENTO		= (reg.getString("FECHVENCIMIENTO")==null)?"":reg.getString("FECHVENCIMIENTO");
				String DESCRIPCIONTASA		= (reg.getString("DESCRIPCIONTASA")==null)?"":reg.getString("DESCRIPCIONTASA");	
				String TASAMORATORIA		= (reg.getString("TASAMORATORIA")==null)?"":reg.getString("TASAMORATORIA");	
				String FACUOTAVEN		= (reg.getString("FACUOTAVEN")==null)?"":reg.getString("FACUOTAVEN");
				String MONTOOPERADO		= (reg.getString("MONTOOPERADO")==null)?"":reg.getString("MONTOOPERADO");	
				String SALDOINSOLUTO		= (reg.getString("SALDOINSOLUTO")==null)?"":reg.getString("SALDOINSOLUTO");	
				String CAPITALVIGENTE		= (reg.getString("CAPITALVIGENTE")==null)?"":reg.getString("CAPITALVIGENTE");	
				String CAPITALVENCIDO		= (reg.getString("CAPITALVENCIDO")==null)?"":reg.getString("CAPITALVENCIDO");	
				String INTERESPROVI		= (reg.getString("INTERESPROVI")==null)?"":reg.getString("INTERESPROVI");	
				String INTERESVENCIDO		= (reg.getString("INTERESVENCIDO")==null)?"":reg.getString("INTERESVENCIDO");	
				String SOBRETASAMORGRAVADO		= (reg.getString("SOBRETASAMORGRAVADO")==null)?"":reg.getString("SOBRETASAMORGRAVADO");	
				String ADEUDOTOTAL		= (reg.getString("ADEUDOTOTAL")==null)?"":reg.getString("ADEUDOTOTAL");	
				String TASATOTAL		= (reg.getString("TASATOTAL")==null)?"":reg.getString("TASATOTAL");
				contenidoArchivo.append( FECHAFINMES+";"+PRESTAMO + ";" + MONEDA + ";" + CD_NOMBRE + 
            //";"+ INTERMEDIARIO+";"+DESCRIPCION_INTER+
            ";"+CODSUCURSAL+";"+NOMSUCURSAL+";"+ CODIGO_BASE_OPERACION + ";" + CG_DESCRIPCION +
						"; "+SUBAPL + ";" + DESCRIPCION_SAPL + ";"+ CLIENTE + ";" +NOMBRECLIENTE + ";" + NUMELECTRONICO +
						";" + FECHOPERACION + ";"+ FECHVENCIMIENTO+";"+DESCRIPCIONTASA+";"+TASATOTAL+";"+TASAMORATORIA+";"+ FACUOTAVEN + 
						"; " +  MONTOOPERADO+ ";"+SALDOINSOLUTO +";" + CAPITALVIGENTE + 
						"; "+ CAPITALVENCIDO + "; "	+ INTERESPROVI + ";" + INTERESVENCIDO + ";"+ SOBRETASAMORGRAVADO  +
						"; "+ ADEUDOTOTAL  +"\n" );
					}
				numreg++;
				if(numreg==1000){					   
					numreg=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
			}
			if (numreg == 0)	{
				contenidoArchivo.append("\nNo se Encontr� Ning�n Registro");
			}
		}
		buffer.write(contenidoArchivo.toString());
		buffer.close();	
		contenidoArchivo = new StringBuffer();
	} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
	} finally {
		try {
				//rs.close();
		} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		log.debug("crearCustomFile(S)"+nombreArchivo);
		return nombreArchivo;
	}

	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		return "";   
	}
	public List getConditions() {
		return conditions;
	}
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
	
	public String getClaveFinanciera(String ic_if)throws Exception{
		log.info("getClaveFinanciera (E)");   
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;  
		boolean		commit = true;
		strQuery 		= new StringBuffer();  
		String  financiera = "";
		try{
			con.conexionDB();
			strQuery.append(
				" select ic_financiera from comcat_if"   +
				" where ic_if ="+ ic_if
					);
			ps = con.queryPrecompilado(strQuery.toString());
			rs = ps.executeQuery();
			if(rs.next())
				financiera = rs.getString(1);
				
			rs.close();
			ps.close();	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getClaveFinanciera "+e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getClaveFinanciera (S)");
		}	
		return financiera;
 	}
	
	public String getFecha(String ic_finan)throws Exception{
		log.info("getFecha (E)");   
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;  
		boolean		commit = true;
		strQuery 		= new StringBuffer();  
		String  fecha = "";
		try{
			con.conexionDB();
			strQuery.append(
				" SELECT TO_CHAR (fechafinmes, 'dd/mm/yyyy') AS fechafinmes "   +
				" FROM pr_edocta_rep per, lc_proyecto lp "+
				"	WHERE per.proyecto = lp.numero_linea_credito	"+
				"	AND lp.codigo_financiera = "+ic_finan+
        " AND ROWNUM = 1"
					);
			ps = con.queryPrecompilado(strQuery.toString());
			rs = ps.executeQuery();
			if(rs.next())
				fecha = rs.getString(1);
				
			rs.close();
			ps.close();	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getClaveFinanciera "+e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getFecha (S)");
		}	
		return fecha;
 	}
  
  public String getFechaXcliente(String cliente)throws Exception{
		log.info("getFecha (E)");   
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;  
		boolean		commit = true;
		strQuery 		= new StringBuffer();  
		String  fecha = "";
		try{
			con.conexionDB();
			strQuery.append(
				" SELECT DISTINCT TO_CHAR (fechafinmes, 'dd/mm/yyyy') AS fechafinmes "   +
				" FROM pr_edocta_rep per, lc_proyecto lp "+
				"	WHERE per.proyecto = lp.numero_linea_credito	"+
				"	AND per.cliente = "+cliente +
        " AND ROWNUM = 1"
					);
			ps = con.queryPrecompilado(strQuery.toString());
			rs = ps.executeQuery();
			if(rs.next())
				fecha = rs.getString(1);
				
			rs.close();
			ps.close();	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getClaveFinanciera "+e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getFecha (S)");
		}	
		return fecha;
 	}
  
	public boolean  getDiaHabil()throws Exception{
		log.info("getDiaHabil (E)");   
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;  
		boolean		commit = true;
		strQuery 		= new StringBuffer();  
		String  fecha = "";
		try{
			con.conexionDB();
			strQuery.append(
				"select sysdate from dual@prod_sirac"
					);
			ps = con.queryPrecompilado(strQuery.toString());
			rs = ps.executeQuery();
			if(rs.next())
				fecha = rs.getString(1);
		System.out.println("conexion con sirac "+fecha);		
			rs.close();
			ps.close();	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getClaveFinanciera "+e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getFecha (S)");
		}	
		return commit;
 	}
	 public List catalogoIF(){
    log.info("CatalogoIF(E)");
	StringBuffer qrySentencia = new StringBuffer("");
	conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		HashMap	datos = new HashMap();
		List regist = new ArrayList();
		try{
			con.conexionDB();
			qrySentencia.append( 
			" 	SELECT DISTINCT ci.ic_financiera AS clave,ci.cg_razon_social,decode( ci.ic_financiera,'',ci.cg_razon_social,'(' || ci.ic_financiera ||')' || ci.cg_razon_social) AS descripcion "+
			" 	FROM comcat_if ci"+
			" 	Where ci.cs_habilitado = 'S'"+
			" 	ORDER BY ci.ic_financiera asc ");				   
			registros = con.consultarDB(qrySentencia.toString());
			while(registros != null && registros.next()){
					datos = new HashMap();
					datos.put("clave",registros.getString("clave"));
					datos.put("descripcion", registros.getString("descripcion"));
					regist.add(datos);
			}
			con.cierraConexionDB();
		} catch (Exception e) {   
			log.error("CatalogoIF  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		log.info("qrySentencia CatalogoIF : "+qrySentencia.toString());
		log.info("CatalogoIF (S) ");
		return regist;															
	}
	
	 public String getSelecBoton() {
	   return selecBoton;
	}

	public void setSelecBoton(String selecBoton) {
	  this.selecBoton = selecBoton;
	}
	public String getClaveIf() {
	  return claveIf;
	}
   public void setClaveIf(String claveIf) {
     this.claveIf = claveIf;
  }
   public String getClaveCliente() {
      return claveCliente;
   }
   public void setClaveCliente(String claveCliente) {
     this.claveCliente = claveCliente;
	}
	
	
}