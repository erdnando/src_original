package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsMandanteEpo implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	public ConsMandanteEpo() {}
	
	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(com.netro.cadenas.ConsMandanteEpo.class);
	
	HashMap consultaMandante = new HashMap();
	  StringBuffer 	qrySentencia;
	private   List 	conditions;
	private String 	operacion;
	private String 	paginaOffset;
	private String 	paginaNo;
	
	private String numNafinElec;
	private String nombreMandante;
	private String estado;
	private String rfc;
	private String clave_epo;

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		return "";
	}//getAggregateCalculationQuery

	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		log.info("getDocumentQuery (E)");
		qrySentencia 		= new StringBuffer();
		conditions 			= new ArrayList();
		
		qrySentencia.append( "SELECT cn.ic_nafin_electronico AS numnafinelect,  " +
									"       cm.ic_mandante AS razonsocial,  " +
									"       cest.ic_estado AS estado,  " +
									"       cm.cg_rfc AS rfc  " +
									"  FROM comrel_nafin cn, " +
									"       comcat_mandante cm, " +
									"       comcat_estado cest, " +
									"       comcat_epo epo, " +
									"       comrel_mandante_epo cme " +
									" WHERE cm.ic_mandante = cn.ic_epo_pyme_if " +
									"   AND cm.ic_estado = cest.ic_estado " +
									"   AND cme.ic_epo = epo.ic_epo " +
									"   AND cm.ic_mandante = cme.ic_mandante " +
									"   AND cn.cg_tipo = 'M' " +
									"   AND epo.ic_epo = ? " );
									conditions.add(clave_epo);
									
		 if(numNafinElec!=null&&!"".equals(numNafinElec)) {
		  	 qrySentencia.append(" AND cn.ic_nafin_electronico = ? ");
			 conditions.add(numNafinElec);
			}
			if(nombreMandante!=null&&!"".equals(nombreMandante)) {
				qrySentencia.append (" AND cm.cg_razon_social = ? " );
				conditions.add(nombreMandante);
			}
			if(estado!=null&&!"".equals(estado)) {
				qrySentencia.append( " AND cest.ic_estado = ? ");
				conditions.add(estado);
			}
			if(rfc!=null&&!"".equals(rfc)) {
				qrySentencia.append(" AND cm.cg_rfc = ? ");
				conditions.add(rfc);
			}
		qrySentencia.append(" ORDER BY cm.cg_razon_social ");
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions: "+conditions);
		log.info("getDocumentQuery (S)");
		return qrySentencia.toString();
	}//getDocumentQuery
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds (E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append( "SELECT cn.ic_nafin_electronico AS numnafinelect, " +
									"       cm.cg_razon_social AS razonsocial, cm.cg_rfc AS rfc, " +
									"       cm.cg_calle AS calle, cest.cd_nombre AS estado,  " +
									"       cm.cg_telefono AS telefono " +
									"  FROM comrel_nafin cn, " +
									"       comcat_mandante cm, " +
									"       comcat_estado cest, " +
									"       comcat_epo epo, " +
									"       comrel_mandante_epo cme " +
									" WHERE cm.ic_mandante = cn.ic_epo_pyme_if " +
									"   AND cm.ic_estado = cest.ic_estado " +
									"   AND cme.ic_epo = epo.ic_epo " +
									"   AND cm.ic_mandante = cme.ic_mandante " +
									"   AND cn.cg_tipo = 'M' " +
									"   AND epo.ic_epo = ? " +
									"   AND ( ");
									conditions.add(clave_epo);
									
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
				if(i>0){
					qrySentencia.append(" OR ");
				}
				qrySentencia.append( " (cn.ic_nafin_electronico = ? "+
											"  AND cm.ic_mandante = ? "+  
											"  AND cest.ic_estado = ? "+ 
											"  AND cm.cg_rfc = ?)" ); 
					conditions.add(new Long(lItem.get(0).toString()));
					conditions.add(new Long(lItem.get(1).toString()));
					conditions.add(new Long(lItem.get(2).toString()));
					conditions.add(lItem.get(3));
		}//for(int i=0;i<ids.size();i++)
		
		qrySentencia.append(")");
		qrySentencia.append(" ORDER BY cm.cg_razon_social ");
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds (S)");
		return qrySentencia.toString();
	}//getDocumentSummaryQueryForIds	


	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile (E)");
		qrySentencia 		= new StringBuffer();
		conditions 			= new ArrayList();
			
		qrySentencia.append( "SELECT cn.ic_nafin_electronico AS Num_Nafin_Elect,  " +
									"       cm.cg_razon_social AS Razon_social,  " +
									"       cm.cg_rfc AS RFC,  " +
									"       cm.cg_calle AS Domicilio,   " +
									"       cest.cd_nombre AS Estado,  " + 
									"       cm.cg_telefono AS Telefono   " +
									"  FROM comrel_nafin cn, " +
									"       comcat_mandante cm, " +
									"       comcat_estado cest, " +
									"       comcat_epo epo, " +
									"       comrel_mandante_epo cme " +
									" WHERE cm.ic_mandante = cn.ic_epo_pyme_if " +
									"   AND cm.ic_estado = cest.ic_estado " +
									"   AND cme.ic_epo = epo.ic_epo " +
									"   AND cm.ic_mandante = cme.ic_mandante " +
									"   AND cn.cg_tipo = 'M' " +
									"   AND epo.ic_epo = ? " );
									conditions.add(clave_epo);
		
		 if(numNafinElec!=null&&!"".equals(numNafinElec)) {
		  	 qrySentencia.append(" AND cn.ic_nafin_electronico = ? ");
			 conditions.add(numNafinElec);
			}
			if(nombreMandante!=null&&!"".equals(nombreMandante)) {
				qrySentencia.append (" AND cm.cg_razon_social = ? " );
				conditions.add(nombreMandante);
			}
			if(estado!=null&&!"".equals(estado)) {
				qrySentencia.append( " AND cest.ic_estado = ? ");
				conditions.add(estado);
			}
			if(rfc!=null&&!"".equals(rfc)) {
				qrySentencia.append(" AND cm.cg_rfc = ? ");
				conditions.add(rfc);
			}
		qrySentencia.append(" ORDER BY cm.cg_razon_social ");
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions: "+conditions);
		log.info("getDocumentQueryFile (S)");
		return qrySentencia.toString();	
	}//getDocumentQueryFile

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo	=	"";
		String numnafinelect,razonsocial,rfc,calle,estado,telefono;
		if ("CSV".equals(tipo)) {
			try{
				String linea = "";
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				int rowspan = 0;
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				linea = "Numero Nafin Electr�nico,Nombre,RFC,Domicilio,Estado,Tel�fono,\n";
				buffer.write(linea);
				while (rs.next()) {
					numnafinelect	=	(rs.getString("NUM_NAFIN_ELECT")==null)?"":rs.getString("NUM_NAFIN_ELECT");
					razonsocial		=	(rs.getString("RAZON_SOCIAL")==null)?"":rs.getString("RAZON_SOCIAL");
					rfc				=	(rs.getString("RFC")==null)?"":rs.getString("RFC");
					calle				=	(rs.getString("DOMICILIO")==null)?"":rs.getString("DOMICILIO");
					estado			=	(rs.getString("ESTADO")==null)?"":rs.getString("ESTADO");
					telefono			=	(rs.getString("TELEFONO")==null)?"":rs.getString("TELEFONO");
					linea = numnafinelect.replace(',',' ') + "," + razonsocial.replace(',',' ') + "," + rfc.replace(',',' ') + "," + calle.replace(',',' ') + "," + estado.replace(',',' ') + "," + telefono.replace(',',' ') + ",\n";
					buffer.write(linea);
					rowspan++;
				}
				buffer.close();
			}catch (Exception e) {
					throw new AppException("Error al generar el archivo", e);
			}	finally {
				}
		}
		return nombreArchivo;
	}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		String nombreArchivo	=	"";
		String numnafinelect,razonsocial,rfc,calle,estado,telefono;
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int nRow = 0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				while (reg.next()) {
					numnafinelect	=	(reg.getString("NUMNAFINELECT")==null)?"":reg.getString("NUMNAFINELECT");
					razonsocial		=	(reg.getString("RAZONSOCIAL")==null)?"":reg.getString("RAZONSOCIAL");
					rfc				=	(reg.getString("RFC")==null)?"":reg.getString("RFC");
					calle				=	(reg.getString("CALLE")==null)?"":reg.getString("CALLE");
					estado			=	(reg.getString("ESTADO")==null)?"":reg.getString("ESTADO");
					telefono			=	(reg.getString("TELEFONO")==null)?"":reg.getString("TELEFONO");
					if (nRow == 0){
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							 
						pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						session.getAttribute("iNoNafinElectronico").toString(),
						(String)session.getAttribute("sesExterno"),
						(String) session.getAttribute("strNombre"),
						(String) session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
							 
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
						pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
						pdfDoc.setTable(6,100);
						pdfDoc.setCell("Consulta de Mandantes","celda01rep",ComunesPDF.CENTER,6);
						pdfDoc.setCell("Numero Naf�n Electr�nico","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Nombre o Raz�n Social","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("RFC","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Domicilio","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Estado","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Tel�fono","celda01rep",ComunesPDF.CENTER);					
					}
					pdfDoc.setCell(numnafinelect, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(razonsocial, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(rfc, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(calle, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(estado, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(telefono, "formasmen", ComunesPDF.CENTER);
					nRow++;
				}
				if (nRow == 0)	{
					pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
					pdfDoc.addText("No se Encontro Ning�n Registro","formas",ComunesPDF.LEFT);
				}else	{
					pdfDoc.addTable();
				}
				pdfDoc.endDocument();
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			}finally {
			}
		}
		return nombreArchivo;
	}
	//GETERS
	public List getConditions() { return conditions; }
	public String getOperacion() { return operacion; }
	public String getPaginaNo() {	return paginaNo; }
	public String getPaginaOffset() { return paginaOffset; }
	public String getNumNafinElec() { return numNafinElec; }
	public String getNombreMandante() { return nombreMandante; }
	public String getEstado() { return estado; }
	public String getRfc() { return rfc;}
	public String getClave_epo() { return clave_epo; }
	
	
	//SETERS
	public void setOperacion(String newOperacion) { operacion = newOperacion; }
	public void setPaginaNo(String newPaginaNo) { paginaNo = newPaginaNo; }
	public void setPaginaOffset(String newPaginaOffset) { paginaOffset = newPaginaOffset; }
	public void setNumNafinElec(String numNafinElec) { this.numNafinElec = numNafinElec; }
	public void setNombreMandante(String nombreMandante) { this.nombreMandante = nombreMandante; }
	public void setEstado(String estado) { this.estado = estado; }
	public void setRfc(String rfc) { this.rfc = rfc; }
	public void setClave_epo(String clave_epo) { this.clave_epo = clave_epo; }
	
}