package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class IFReporteEPOs implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

 public IFReporteEPOs() {}

 private static final Log log = ServiceLocator.getInstance().getLog(IFReporteEPOs.class);

 /**
  * Contiene la lista de valores (parametros) a emplear en las condiciones
  */
 StringBuffer qrySentencia;

 private List conditions;
 private String paginaOffset;
 private String paginaNo;
 public String paginar;

 public String epo;
 public int totalRegistros;

 private String usuario; // Intermediario Financiero
 private String ic_moneda;
 private String cs_vobo_if;

 /**
  * Obtiene el query para obtener los totales de la consulta
  * @return Cadena con la consulta de SQL, para obtener los totales
  */
 public String getAggregateCalculationQuery() {
    log.info("getAggregateCalculationQuery");
  qrySentencia = new StringBuffer();
  conditions = new ArrayList();

  qrySentencia.append(
    " SELECT doc.ic_epo,                   "+
    "        epo.cg_razon_social    epo,   "+
    "        doc.ic_pyme,                  "+
    "        pym.cg_razon_social    pyme,  "+
    "        doc.ic_if,                    "+
    "        COUNT(1) TOTAL_REGISTROS      "+
    "   FROM com_documento  doc,           "+
    "        comcat_pyme    pym,           "+
    "        comcat_epo     epo            "+
    "  WHERE doc.ic_epo = epo.ic_epo       "+
    "    AND doc.ic_pyme = pym.ic_pyme     ");

  // Especificar la Clave de la Moneda
  if (ic_moneda != null){
    qrySentencia.append(" AND doc.ic_moneda =  ? ");
    conditions.add(ic_moneda);
  }

  // Especificar Intermediario Financiero
  if (usuario != null) {
   qrySentencia.append(" AND  doc.ic_if = ? ");
   conditions.add(usuario);
  }

  // Especificar la clave de la EPO
  if (epo != null){
    qrySentencia.append(" AND doc.ic_epo =  ? ");
    conditions.add(epo);
  }


  qrySentencia.append(
    "  GROUP BY doc.ic_epo,                "+
    "           epo.cg_razon_social,       "+
    "           doc.ic_pyme,               "+
    "           pym.cg_razon_social,       "+
    "           doc.ic_if                  "+
    "  ORDER BY 1                          ");
  

  log.error("getAggregateCalculationQuery " + conditions.toString());
  log.error("getAggregateCalculationQuery " + qrySentencia.toString());

  return qrySentencia.toString();
 }

 /**
  * Obtiene el query para obtener las llaves primarias de la consulta
  * @return Cadena con la consulta de SQL, para obtener llaves primarias
  */
 public String getDocumentQuery() {
    log.info("getDocumentQuery");
  qrySentencia = new StringBuffer();
  conditions = new ArrayList();
  qrySentencia.append(" SELECT * FROM   ( ");
  qrySentencia.append(                                                                                                                                             
     "         SELECT                                                                                                                                           "+
     "      /*+ leading(com_documento) index(com_documento IN_COM_DOCUMENTO_04_NUK )                                                                            "+
     "               use_nl(com_documento comrel_cuenta_bancaria  comrel_pyme_if comrel_pyme_epo  comcat_epo comcat_pyme  comcat_moneda comrel_nafin com_contacto ) */  "+
     "             com_documento.ic_epo    AS ic_epo,                                                                                                           "+
     "             comcat_epo.cg_razon_social, comcat_if.ic_if as ic_if,                                                                                        "+
     "             com_documento.ic_pyme   AS ic_pyme,                                                                                                          "+
     "             DECODE(comcat_pyme.cs_tipo_persona, 'F', comcat_pyme.cg_nombre                                                                               "+
     "                                                      || ' '                                                                                              "+
     "                                                      || comcat_pyme.cg_appat                                                                             "+
     "                                                      || ' '                                                                                              "+
     "                                                      || comcat_pyme.cg_apmat, 'M', comcat_pyme.cg_razon_social) AS proveedor,                            "+
     "             comcat_pyme.cg_rfc,                                                                                                                          "+
     "             comcat_if.cg_razon_social as NOMBRE_IF,                                                                                                      "+
     "             com_contacto.cg_nombre                                                                                                                       "+
     "             || ' '                                                                                                                                       "+
     "             || com_contacto.cg_appat                                                                                                                     "+
     "             || ' '                                                                                                                                       "+
     "             || com_contacto.cg_apmat nom_contac,                                                                                                         "+
     "             com_contacto.cg_tel,                                                                                                                         "+
     "             com_contacto.cg_email,                                                                                                                       "+
     "             comcat_moneda.cd_nombre,                                                                                                                     "+
     "             SUM(com_documento.fn_monto) AS tot_mn_doctos,                                                                                                "+
     "             COUNT(com_documento.ic_documento) AS tot_doctos,                                                                                             "+
     "             DECODE(comrel_pyme_if.cs_vobo_if, 'S', 'LIBERADO', 'N', 'POR LIBERAR') AS cs_vobo_if,                                                        "+
     "             TO_CHAR(comrel_pyme_if.df_vobo_if, 'dd/mm/yyyy') df_vobo_if                                                                                  "+
     "         FROM                                                                                                                                             "+
     "             com_documento,                                                                                                                               "+
     "             comrel_pyme_epo,                                                                                                                             "+
     "             comrel_cuenta_bancaria,                                                                                                                      "+
     "             comrel_pyme_if,                                                                                                                              "+
     "             comcat_epo,                                                                                                                                  "+
     "             comcat_pyme,                                                                                                                                 "+
     "             comcat_moneda,                                                                                                                               "+
     "             comrel_nafin,                                                                                                                                "+
     "             com_contacto,                                                                                                                                "+
     "             comcat_if                                                                                                                                    "+
     "         WHERE                                                                                                                                            "+
     "            com_documento.ic_pyme = comrel_nafin.ic_epo_pyme_if                                                                        "+
     "            AND comcat_pyme.ic_pyme = comrel_nafin.ic_epo_pyme_if                                                                      "+
     "            AND com_documento.ic_epo = comcat_epo.ic_epo                                                                               "+
     "            AND com_documento.ic_moneda = comcat_moneda.ic_moneda                                                                      "+
     "            AND com_contacto.ic_pyme = com_documento.ic_pyme                                                                           "+
     "            AND com_documento.ic_epo = comrel_pyme_epo.ic_epo                                                                          "+
     "            AND com_documento.ic_pyme = comrel_pyme_epo.ic_pyme                                                                        "+
     "            AND com_documento.ic_pyme = comrel_cuenta_bancaria.ic_pyme                                                                 "+
     "            AND comrel_cuenta_bancaria.ic_moneda = comcat_moneda.ic_moneda                                                             "+
     "            AND comrel_cuenta_bancaria.ic_cuenta_bancaria = comrel_pyme_if.ic_cuenta_bancaria                                          "+
     "            AND comrel_pyme_if.ic_epo = com_documento.ic_epo                                                                           "+
     "            AND comrel_pyme_if.ic_if = comcat_if.ic_if                                                                                 "+
     "            AND com_contacto.cs_primer_contacto = 'S'                                                                                  "+
     "            AND comrel_pyme_epo.cs_aceptacion = 'H'                                                                                    "+
     "            AND comrel_pyme_if.cs_borrado = 'N'                                                                                        "+
     "            AND comrel_cuenta_bancaria.cs_borrado = 'N'                                                                                "+
     "            AND comrel_nafin.cg_tipo = 'P'                                                                                             ");

     // Especificar estatus
     if (cs_vobo_if != null) {
      qrySentencia.append(" AND comrel_pyme_if.cs_vobo_if = ? ");
      conditions.add(cs_vobo_if);
     }

    // Especificar la Clave de la Moneda
    qrySentencia.append(" AND com_documento.ic_moneda = ? ");
    qrySentencia.append(" AND comrel_cuenta_bancaria.ic_moneda = ? ");
    conditions.add(ic_moneda);
    conditions.add(ic_moneda);

     // Especificar la clave de la EPO
     if ( epo != null && !epo.equals("0")) {
      qrySentencia.append(" AND com_documento.ic_epo = ? ");
      conditions.add(epo);
     }

    // Especificar Intermediario Financiero
    if (usuario != null) {
     qrySentencia.append(" AND comrel_pyme_if.ic_if = ? ");
     conditions.add(usuario);
    }



    qrySentencia.append(
     "         AND com_documento.ic_estatus_docto = 2                                                                                                           "+
     "         GROUP BY                                                                                                                                         "+
     "             com_documento.ic_pyme,                                                                                                                       "+
     "             com_documento.ic_epo,                                                                                                                        "+
     "             comcat_epo.cg_razon_social,                                                                                                                  "+
     "             com_documento.ic_moneda,                                                                                                                     "+
     "             DECODE(comcat_pyme.cs_tipo_persona, 'F', comcat_pyme.cg_nombre                                                                               "+
     "                                                      || ' '                                                                                              "+
     "                                                      || comcat_pyme.cg_appat                                                                             "+
     "                                                      || ' '                                                                                              "+
     "                                                      || comcat_pyme.cg_apmat, 'M', comcat_pyme.cg_razon_social),                                         "+
     "             comcat_pyme.cg_rfc,                                                                                                                          "+
     "             comcat_if.cg_razon_social, comcat_if.ic_if,                                                                                                  "+
     "             com_contacto.cg_nombre                                                                                                                       "+
     "             || ' '                                                                                                                                       "+
     "             || com_contacto.cg_appat                                                                                                                     "+
     "             || ' '                                                                                                                                       "+
     "             || com_contacto.cg_apmat,                                                                                                                    "+
     "             com_contacto.cg_tel,                                                                                                                         "+
     "             com_contacto.cg_email,                                                                                                                       "+
     "             comcat_moneda.cd_nombre,                                                                                                                     "+
     "             DECODE(comrel_pyme_if.cs_vobo_if, 'S', 'LIBERADO', 'N', 'POR LIBERAR'),                                                                      "+
     "             TO_CHAR(comrel_pyme_if.df_vobo_if, 'dd/mm/yyyy')                                                                                             "+
     "             ORDER BY com_documento.ic_epo, com_documento.ic_pyme ASC ) todos                                                                             ");

  qrySentencia.append(" , ( ");

  qrySentencia.append(
     "         SELECT                                                                                                                                           "+
     "      /*+ leading(com_documento) index(com_documento IN_COM_DOCUMENTO_04_NUK )                                                                            "+
     "               use_nl(com_documento comrel_cuenta_bancaria  comrel_pyme_if comrel_pyme_epo  comcat_epo comcat_pyme  comcat_moneda comrel_nafin com_contacto ) */  "+
     "             com_documento.ic_epo    AS ic_epo_mes,                                                                                                       "+
     "             com_documento.ic_pyme   AS ic_pyme_mes, comrel_pyme_if.ic_if    as ic_if_mes,                                                                "+
     "             SUM(com_documento.fn_monto) AS tot_mn_doctos_mes,                                                                                            "+
     "             COUNT(com_documento.ic_documento) AS tot_doctos_mes                                                                                          "+
     "         FROM                                                                                                                                             "+
     "             com_documento,                                                                                                                               "+
     "             comrel_pyme_epo,                                                                                                                             "+
     "             comrel_cuenta_bancaria,                                                                                                                      "+
     "             comrel_pyme_if,                                                                                                                              "+
     "             comcat_epo,                                                                                                                                  "+
     "             comcat_pyme,                                                                                                                                 "+
     "             comcat_moneda,                                                                                                                               "+
     "             comrel_nafin,                                                                                                                                "+
     "             com_contacto                                                                                                                                 "+
     "         WHERE                                                                                                                                            "+
     "            com_documento.ic_pyme = comrel_nafin.ic_epo_pyme_if                                                                             "+
     "            AND comcat_pyme.ic_pyme = comrel_nafin.ic_epo_pyme_if                                                                           "+
     "            AND com_documento.ic_epo = comcat_epo.ic_epo                                                                                    "+
     "            AND com_documento.ic_moneda = comcat_moneda.ic_moneda                                                                           "+
     "            AND com_contacto.ic_pyme = com_documento.ic_pyme                                                                                "+
     "            AND com_documento.ic_epo = comrel_pyme_epo.ic_epo                                                                               "+
     "            AND com_documento.ic_pyme = comrel_pyme_epo.ic_pyme                                                                             "+
     "            AND com_documento.ic_pyme = comrel_cuenta_bancaria.ic_pyme                                                                      "+
     "            AND comrel_cuenta_bancaria.ic_moneda = comcat_moneda.ic_moneda                                                                  "+
     "            AND comrel_cuenta_bancaria.ic_cuenta_bancaria = comrel_pyme_if.ic_cuenta_bancaria                                               "+
     "            AND comrel_pyme_if.ic_epo = com_documento.ic_epo                                                                                "+
     "            AND com_contacto.cs_primer_contacto = 'S'                                                                                       "+
     "            AND comrel_pyme_epo.cs_aceptacion = 'H'                                                                                         "+
     "            AND comrel_pyme_if.cs_borrado = 'N'                                                                                             "+
     "            AND comrel_cuenta_bancaria.cs_borrado = 'N'                                                                                     "+
     "            AND comrel_nafin.cg_tipo = 'P'                                                                                                  ");

     // Especificar estatus
     if (cs_vobo_if != null) {
      qrySentencia.append(" AND comrel_pyme_if.cs_vobo_if =  ? ");
      conditions.add(cs_vobo_if);
     }

    // Especificar la Clave de la Moneda
    qrySentencia.append(" AND com_documento.ic_moneda = ? ");
    qrySentencia.append(" AND comrel_cuenta_bancaria.ic_moneda = ? ");
    conditions.add(ic_moneda);
    conditions.add(ic_moneda);

    qrySentencia.append(" AND com_documento.df_fecha_venc <= TRUNC(LAST_DAY(SYSDATE)) ");

     // Especificar la clave de la EPO
     if ( epo != null && !epo.equals("0")) {
      qrySentencia.append(" AND com_documento.ic_epo = ? ");
      conditions.add(epo);
     }
     
    // Especificar Intermediario Financiero
    if (usuario != null) {
     qrySentencia.append(" AND comrel_pyme_if.ic_if = ? ");
     conditions.add(usuario);
    }


  qrySentencia.append(
     "         AND com_documento.ic_estatus_docto = 2                                                                                                           "+
     "         GROUP BY                                                                                                                                         "+
     "             com_documento.ic_pyme,                                                                                                                       "+
     "             com_documento.ic_epo, comrel_pyme_if.ic_if                                                                                                   "+
     "         ORDER BY com_documento.ic_epo, com_documento.ic_pyme ASC                                                                                         "+
     "     ) mes                                                                                                                                                "+
     " WHERE                                                                                                                                                    "+
     "     todos.ic_pyme = mes.ic_pyme_mes (+)                                                                                                                  "+
     "     AND todos.ic_epo = mes.ic_epo_mes (+) AND todos.ic_if = mes.ic_if_mes (+)                                                                            ");   

 log.info("getDocumentQuery " + conditions.toString());
 log.info("getDocumentQuery " + qrySentencia.toString());

  return qrySentencia.toString();

 }

 /**
  * Obtiene el query necesario para mostrar la informaci�n completa de 
  * una p�gina a partir de las llaves primarias enviadas como par�metro
  * @return Cadena con la consulta de SQL, para obtener la informaci�n
  * 	completa de los registros con las llaves especificadas
  */
 public String getDocumentSummaryQueryForIds(List pageIds) {
    log.info("getDocumentSummaryQueryForIds");
  qrySentencia = new StringBuffer();
  conditions = new ArrayList();


  log.debug("getDocumentSummaryQueryForIds " + conditions.toString());
  log.debug(" getDocumentSummaryQueryForIds " + qrySentencia.toString());

  //return qrySentencia.toString();
  //return getDocumentQuery();
  return this.getDocumentQuery();

 }

 /**
  * Genera el archivo 
  * @return String con el query
  */
 public String getDocumentQueryFile() {
    log.info("getDocumentQueryFile");
    
  qrySentencia = new StringBuffer();
  conditions = new ArrayList();

  log.debug("getDocumentQueryFile " + conditions.toString());
  log.debug("getDocumentQueryFile " + qrySentencia.toString());

  //return qrySentencia.toString();
  return this.getDocumentQuery();
  
 } //getDocumentQueryFile


 public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
        log.info("crearPageCustomFile");
  String nombreArchivo = "";
  String linea = "";
  OutputStreamWriter writer = null;
  BufferedWriter buffer = null;
  if ("PDF".equals(tipo)) {
   try {

    HttpSession session = request.getSession();
    nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
    ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

    String meses[] = {
     "Enero",
     "Febrero",
     "Marzo",
     "Abril",
     "Mayo",
     "Junio",
     "Julio",
     "Agosto",
     "Septiembre",
     "Octubre",
     "Noviembre",
     "Diciembre"
    };
    String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
    String diaActual = fechaActual.substring(0, 2);
    String mesActual = meses[Integer.parseInt(fechaActual.substring(3, 5)) - 1];
    String anioActual = fechaActual.substring(6, 10);
    String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

    //String iNoNE = session.getAttribute("iNoNafinElectronico") == null ? "" : (String) session.getAttribute("iNoNafinElectronico");
       String iNoNE = "";
    pdfDoc.encabezadoConImagenes(pdfDoc, (String) session.getAttribute("strPais"),
     iNoNE,
     (String) session.getAttribute("sesExterno"),
     (String) session.getAttribute("strNombre"),
     (String) session.getAttribute("strNombreUsuario"),
     (String) session.getAttribute("strLogo"), (String) session.getServletContext().getAttribute("strDirectorioPublicacion"));

    pdfDoc.addText("M�xico, CDMX. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas", ComunesPDF.RIGHT);

    pdfDoc.setTable(14, 100);
    pdfDoc.setCell("EPO", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("Proveedor", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("RFC", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("Nombre de contacto", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("Tel�fono contacto", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("Correo contacto", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("Moneda", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("N�m. total de documentos", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("Monto total de publicaci�n", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("N�m. documentos con vencimiento mes actual", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("Monto vencimiento mes actual", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("Nombre del Intermediario Financero", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("Estatus con el IF", "celda01", ComunesPDF.CENTER);
    pdfDoc.setCell("Fecha de asignaci�n IF", "celda01", ComunesPDF.CENTER);

    while (reg.next()) {
     String epo = (reg.getString("CG_RAZON_SOCIAL") == null) ? "" : reg.getString("CG_RAZON_SOCIAL");
     String proveedor = (reg.getString("PROVEEDOR") == null) ? "" : reg.getString("PROVEEDOR");
     String rfc = (reg.getString("CG_RFC") == null) ? "" : reg.getString("CG_RFC");
     String contacto = (reg.getString("NOM_CONTAC") == null) ? "" : reg.getString("NOM_CONTAC");
     String telefono = (reg.getString("CG_TEL") == null) ? "" : reg.getString("CG_TEL");
     String correo = (reg.getString("CG_EMAIL") == null) ? "" : reg.getString("CG_EMAIL");
     String moneda = (reg.getString("CD_NOMBRE") == null) ? "" : reg.getString("CD_NOMBRE");
     String totalDoc = (reg.getString("TOT_DOCTOS") == null) ? "" : reg.getString("TOT_DOCTOS");
     String totalMon = (reg.getString("TOT_MN_DOCTOS") == null) ? "" : reg.getString("TOT_MN_DOCTOS");
     String totalDocMen = (reg.getString("TOT_DOCTOS_MES") == null) ? "" : reg.getString("TOT_DOCTOS_MES");
     String totalMonMen = (reg.getString("TOT_MN_DOCTOS_MES") == null) ? "" : reg.getString("TOT_MN_DOCTOS_MES");
     String nombreBanco = (reg.getString("NOMBRE_IF") == null) ? "" : reg.getString("NOMBRE_IF");
     String estatus = (reg.getString("CS_VOBO_IF") == null) ? "" : reg.getString("CS_VOBO_IF");
     String fechaVobo = (reg.getString("DF_VOBO_IF") == null) ? "" : reg.getString("DF_VOBO_IF");

     pdfDoc.setCell(epo, "formas", ComunesPDF.CENTER);
     pdfDoc.setCell(proveedor, "formas", ComunesPDF.CENTER);
     pdfDoc.setCell(rfc, "formas", ComunesPDF.CENTER);
     pdfDoc.setCell(contacto, "formas", ComunesPDF.CENTER);
     pdfDoc.setCell(telefono, "formas", ComunesPDF.CENTER);
     pdfDoc.setCell(correo, "formas", ComunesPDF.CENTER);
     pdfDoc.setCell(moneda, "formas", ComunesPDF.CENTER);
     pdfDoc.setCell(totalDoc, "formas", ComunesPDF.CENTER);
     pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMon, 2), "formas", ComunesPDF.CENTER);
     pdfDoc.setCell(totalDocMen, "formas", ComunesPDF.CENTER);
     pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMonMen, 2), "formas", ComunesPDF.CENTER);
     pdfDoc.setCell(nombreBanco, "formas", ComunesPDF.CENTER);   
     pdfDoc.setCell(estatus, "formas", ComunesPDF.CENTER);
     pdfDoc.setCell(fechaVobo, "formas", ComunesPDF.CENTER);
    }

    pdfDoc.addTable();
    pdfDoc.endDocument();
   } catch (Throwable e) {
    log.error(e.getMessage());
    e.printStackTrace();
    throw new AppException("Error al generar el archivo", e);
   } 
  }
  return nombreArchivo;
 }



 ///para formar el csv o pdf de los todos los registros
 public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
     log.info("crearCustomFile");
  String linea = "";
  OutputStreamWriter writer = null;
  BufferedWriter buffer = null;
  String nombreArchivo = "";
  StringBuffer contenidoArchivo = new StringBuffer();
  CreaArchivo archivo = new CreaArchivo();

  if ("CSV".equals(tipo)) {
   try {
    nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
    writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
    buffer = new BufferedWriter(writer);
    buffer.write(linea);

    //Encabezado
    linea = "EPO, Proveedor, RFC, Nombre de contacto, Tel�fono contacto, Correo contacto, Moneda, N�m. total de documentos, "+
            "Monto total de publicaci�n, N�m. documentos con vencimiento mes actual, Monto vencimientos mes actual,Nombre del Intermediario Financiero, Estatus con el IF, Fecha de asignaci�n IF\n";
    buffer.write(linea);
    while (rs.next()) {
     String epo = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
     String proveedor = (rs.getString("PROVEEDOR") == null) ? "" : rs.getString("PROVEEDOR");
     String rfc = (rs.getString("CG_RFC") == null) ? "" : rs.getString("CG_RFC");
     String contacto = (rs.getString("NOM_CONTAC") == null) ? "" : rs.getString("NOM_CONTAC");
     String telefono = (rs.getString("CG_TEL") == null) ? "" : rs.getString("CG_TEL");
     String correo = (rs.getString("CG_EMAIL") == null) ? "" : rs.getString("CG_EMAIL");
     String moneda = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
     String totalDoc = (rs.getString("TOT_DOCTOS") == null) ? "" : rs.getString("TOT_DOCTOS");
     String totalMon = (rs.getString("TOT_MN_DOCTOS") == null) ? "" : rs.getString("TOT_MN_DOCTOS");
     String totalDocMen = (rs.getString("TOT_DOCTOS_MES") == null) ? "" : rs.getString("TOT_DOCTOS_MES");
     String totalMonMen = (rs.getString("TOT_MN_DOCTOS_MES") == null) ? "" : rs.getString("TOT_MN_DOCTOS_MES");
     String nombreBanco = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
     String estatus = (rs.getString("CS_VOBO_IF") == null) ? "" : rs.getString("CS_VOBO_IF");
     String fechaVobo = (rs.getString("DF_VOBO_IF") == null) ? "" : rs.getString("DF_VOBO_IF");

     linea = "\"" + epo + "\",\"" + proveedor + "\",\"" + rfc + "\",\"" + contacto + "\",\"" + telefono + "\",\"" + correo + "\",\"" + moneda + "\",\"" + totalDoc + "\",\"" + totalMon + "\",\"" + totalDocMen + "\",\"" + totalMonMen + "\",\"" + nombreBanco + "\",\"" + estatus + "\",\"" + fechaVobo + "\"\n";
     buffer.write(linea);
    }

    buffer.close();
   } catch (Throwable e) {
    throw new AppException("Error al generar el archivo", e);
   } finally {
    try {
     rs.close();
    } catch (Exception e) {}
   }
  }

  return nombreArchivo;
 }

 /*****************************************************
 	 GETTERS
 *******************************************************/

 /**
   Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
   @return Lista con los parametros de las condiciones 
  */
 public List getConditions() {
  return conditions;
 }

 /**
  * Obtiene el numero de pagina.
  * @return Cadena con el numero de pagina
  */
 public String getPaginaNo() {
  return paginaNo;
 }

 /**
  * Obtiene el offset de la pagina.
  * @return Cadena con el offset de pagina
  */
 public String getPaginaOffset() {
  return paginaOffset;
 }


 /*****************************************************
 					 SETTERS
 *******************************************************/

 /**
  * Establece el numero de pagina.
  * @param  newPaginaNo Cadena con el numero de pagina
  */
 public void setPaginaNo(String newPaginaNo) {
  paginaNo = newPaginaNo;
 }

 /**
  * Establece el offset de la pagina.
  * @param newPaginaOffset Cadena con el offset de pagina
  */
 public void setPaginaOffset(String paginaOffset) {
  this.paginaOffset = paginaOffset;
 }

 public String getPaginar() {
  return paginar;
 }

 public void setPaginar(String paginar) {
  this.paginar = paginar;
 }

 public String getUsuario() {
  return this.usuario;
 }

 public void setUsuario(String usuario) {
  if (usuario != null && usuario.equals("")) {
   this.usuario = null;
  } else {
   this.usuario = usuario;
  }
 }

 public String getIc_moneda() {
  return this.ic_moneda;
 }

 public void setIc_moneda(String ic_moneda) {
  this.ic_moneda = ic_moneda;
 }

 public void setCs_vobo_if(String cs_vobo_if) {
  if (cs_vobo_if != null && cs_vobo_if.equals("")) {
   this.cs_vobo_if = null;
  } else {
   this.cs_vobo_if = cs_vobo_if;
  }
 }

 public String getCs_vobo_if() {
  return cs_vobo_if;
 }

 public void setEpo(String epo) {
  this.epo = epo;
 }

 public String getEpo() {
  return epo;
 }


    /**
     * Obtiene el query para obtener las llaves primarias de la consulta
     * @return Cadena con la consulta de SQL, para obtener llaves primarias
     */
    public String getFullQuery() {
       log.info("getDocumentQuery");
     qrySentencia = new StringBuffer();
     conditions = new ArrayList();
     qrySentencia.append(" SELECT * FROM   ( ");
     qrySentencia.append(                                                                                                                                             
        "         SELECT                                                                                                                                           "+
        "      /*+ leading(com_documento) index(com_documento IN_COM_DOCUMENTO_04_NUK )                                                                            "+
        "               use_nl(com_documento comrel_cuenta_bancaria  comrel_pyme_if comrel_pyme_epo  comcat_epo comcat_pyme  comcat_moneda comrel_nafin com_contacto ) */  "+
        "             com_documento.ic_epo    AS ic_epo,                                                                                                           "+
        "             comcat_epo.cg_razon_social, comcat_if.ic_if as ic_if,                                                                                        "+
        "             com_documento.ic_pyme   AS ic_pyme,                                                                                                          "+
        "             DECODE(comcat_pyme.cs_tipo_persona, 'F', comcat_pyme.cg_nombre                                                                               "+
        "                                                      || ' '                                                                                              "+
        "                                                      || comcat_pyme.cg_appat                                                                             "+
        "                                                      || ' '                                                                                              "+
        "                                                      || comcat_pyme.cg_apmat, 'M', comcat_pyme.cg_razon_social) AS proveedor,                            "+
        "             comcat_pyme.cg_rfc,                                                                                                                          "+
        "             comcat_if.cg_razon_social as NOMBRE_IF,                                                                                                      "+
        "             com_contacto.cg_nombre                                                                                                                       "+
        "             || ' '                                                                                                                                       "+
        "             || com_contacto.cg_appat                                                                                                                     "+
        "             || ' '                                                                                                                                       "+
        "             || com_contacto.cg_apmat nom_contac,                                                                                                         "+
        "             com_contacto.cg_tel,                                                                                                                         "+
        "             com_contacto.cg_email,                                                                                                                       "+
        "             comcat_moneda.cd_nombre,                                                                                                                     "+
        "             SUM(com_documento.fn_monto) AS tot_mn_doctos,                                                                                                "+
        "             COUNT(com_documento.ic_documento) AS tot_doctos,                                                                                             "+
        "             DECODE(comrel_pyme_if.cs_vobo_if, 'S', 'LIBERADO', 'N', 'POR LIBERAR') AS cs_vobo_if,                                                        "+
        "             TO_CHAR(comrel_pyme_if.df_vobo_if, 'dd/mm/yyyy') df_vobo_if                                                                                  "+
        "         FROM                                                                                                                                             "+
        "             com_documento,                                                                                                                               "+
        "             comrel_pyme_epo,                                                                                                                             "+
        "             comrel_cuenta_bancaria,                                                                                                                      "+
        "             comrel_pyme_if,                                                                                                                              "+
        "             comcat_epo,                                                                                                                                  "+
        "             comcat_pyme,                                                                                                                                 "+
        "             comcat_moneda,                                                                                                                               "+
        "             comrel_nafin,                                                                                                                                "+
        "             com_contacto,                                                                                                                                "+
        "             comcat_if                                                                                                                                    "+
        "         WHERE                                                                                                                                            "+
        "             com_documento.ic_estatus_docto = 2                                                                                                           "+
        "             AND com_documento.ic_pyme = comrel_nafin.ic_epo_pyme_if                                                                                      "+
        "             AND comcat_pyme.ic_pyme = comrel_nafin.ic_epo_pyme_if                                                                                        "+
        "             AND com_documento.ic_epo = comcat_epo.ic_epo                                                                                                 "+
        "             AND com_documento.ic_moneda = comcat_moneda.ic_moneda                                                                                        "+
        "             AND comrel_nafin.cg_tipo = 'P'                                                                                                               "+
        "             AND com_contacto.ic_pyme = com_documento.ic_pyme                                                                                             "+
        "             AND com_contacto.cs_primer_contacto = 'S'                                                                                                    "+
        "             AND comrel_pyme_epo.cs_aceptacion = 'H'                                                                                                      "+
        "             AND com_documento.ic_epo = comrel_pyme_epo.ic_epo                                                                                            "+
        "             AND com_documento.ic_pyme = comrel_pyme_epo.ic_pyme                                                                                          "+
        "             AND com_documento.ic_pyme = comrel_cuenta_bancaria.ic_pyme                                                                                   "+
        "             AND comrel_cuenta_bancaria.cs_borrado = 'N'                                                                                                  "+
        "             AND comrel_cuenta_bancaria.ic_moneda = comcat_moneda.ic_moneda                                                                               "+
        "             AND comrel_cuenta_bancaria.ic_cuenta_bancaria = comrel_pyme_if.ic_cuenta_bancaria                                                            "+
        "             AND comrel_pyme_if.ic_epo = com_documento.ic_epo                                                                                             "+
        "             AND comrel_pyme_if.cs_borrado = 'N'                                                                                                          "+
        "             AND comrel_pyme_if.ic_if = comcat_if.ic_if                                                                                                   ");

        // Especificar estatus
        if (cs_vobo_if != null) {
         qrySentencia.append(" AND comrel_pyme_if.cs_vobo_if = ? ");
         conditions.add(cs_vobo_if);
        }

       // Especificar la Clave de la Moneda
       qrySentencia.append(" AND com_documento.ic_moneda = ? ");
       qrySentencia.append(" AND comrel_cuenta_bancaria.ic_moneda = ? ");
       conditions.add(ic_moneda);
       conditions.add(ic_moneda);

        // Especificar la clave de la EPO
        if ( epo != null && !epo.equals("0")) {
         qrySentencia.append(" AND com_documento.ic_epo = ? ");
         conditions.add(epo);
        }

       // Especificar Intermediario Financiero
       if (usuario != null) {
        qrySentencia.append(" AND comrel_pyme_if.ic_if = ? ");
        conditions.add(usuario);
       }
       qrySentencia.append(
        "         GROUP BY                                                                                                                                         "+
        "             com_documento.ic_pyme,                                                                                                                       "+
        "             com_documento.ic_epo,                                                                                                                        "+
        "             comcat_epo.cg_razon_social,                                                                                                                  "+
        "             com_documento.ic_moneda,                                                                                                                     "+
        "             DECODE(comcat_pyme.cs_tipo_persona, 'F', comcat_pyme.cg_nombre                                                                               "+
        "                                                      || ' '                                                                                              "+
        "                                                      || comcat_pyme.cg_appat                                                                             "+
        "                                                      || ' '                                                                                              "+
        "                                                      || comcat_pyme.cg_apmat, 'M', comcat_pyme.cg_razon_social),                                         "+
        "             comcat_pyme.cg_rfc,                                                                                                                          "+
        "             comcat_if.cg_razon_social, comcat_if.ic_if,                                                                                                  "+
        "             com_contacto.cg_nombre                                                                                                                       "+
        "             || ' '                                                                                                                                       "+
        "             || com_contacto.cg_appat                                                                                                                     "+
        "             || ' '                                                                                                                                       "+
        "             || com_contacto.cg_apmat,                                                                                                                    "+
        "             com_contacto.cg_tel,                                                                                                                         "+
        "             com_contacto.cg_email,                                                                                                                       "+
        "             comcat_moneda.cd_nombre,                                                                                                                     "+
        "             DECODE(comrel_pyme_if.cs_vobo_if, 'S', 'LIBERADO', 'N', 'POR LIBERAR'),                                                                      "+
        "             TO_CHAR(comrel_pyme_if.df_vobo_if, 'dd/mm/yyyy')                                                                                             "+
        "             ORDER BY com_documento.ic_epo, com_documento.ic_pyme ASC ) todos                                                                             ");

     qrySentencia.append(" , ( ");

     qrySentencia.append(
        "         SELECT                                                                                                                                           "+
        "      /*+ leading(com_documento) index(com_documento IN_COM_DOCUMENTO_04_NUK )                                                                            "+
        "               use_nl(com_documento comrel_cuenta_bancaria  comrel_pyme_if comrel_pyme_epo  comcat_epo comcat_pyme  comcat_moneda comrel_nafin com_contacto ) */  "+
        "             com_documento.ic_epo    AS ic_epo_mes,                                                                                                       "+
        "             com_documento.ic_pyme   AS ic_pyme_mes, comrel_pyme_if.ic_if    as ic_if_mes,                                                                "+
        "             SUM(com_documento.fn_monto) AS tot_mn_doctos_mes,                                                                                            "+
        "             COUNT(com_documento.ic_documento) AS tot_doctos_mes                                                                                          "+
        "         FROM                                                                                                                                             "+
        "             com_documento,                                                                                                                               "+
        "             comrel_pyme_epo,                                                                                                                             "+
        "             comrel_cuenta_bancaria,                                                                                                                      "+
        "             comrel_pyme_if,                                                                                                                              "+
        "             comcat_epo,                                                                                                                                  "+
        "             comcat_pyme,                                                                                                                                 "+
        "             comcat_moneda,                                                                                                                               "+
        "             comrel_nafin,                                                                                                                                "+
        "             com_contacto                                                                                                                                 "+
        "         WHERE                                                                                                                                            "+
        "             com_documento.ic_estatus_docto = 2                                                                                                           "+
        "             AND com_documento.ic_pyme = comrel_nafin.ic_epo_pyme_if                                                                                      "+
        "             AND comcat_pyme.ic_pyme = comrel_nafin.ic_epo_pyme_if                                                                                        "+
        "             AND com_documento.ic_epo = comcat_epo.ic_epo                                                                                                 "+
        "             AND com_documento.ic_moneda = comcat_moneda.ic_moneda                                                                                        "+
        "             AND comrel_nafin.cg_tipo = 'P'                                                                                                               "+
        "             AND com_contacto.ic_pyme = com_documento.ic_pyme                                                                                             "+
        "             AND com_contacto.cs_primer_contacto = 'S'                                                                                                    "+
        "             AND comrel_pyme_epo.cs_aceptacion = 'H'                                                                                                      "+
        "             AND com_documento.ic_epo = comrel_pyme_epo.ic_epo                                                                                            "+
        "             AND com_documento.ic_pyme = comrel_pyme_epo.ic_pyme                                                                                          "+
        "             AND com_documento.ic_pyme = comrel_cuenta_bancaria.ic_pyme                                                                                   "+
        "             AND comrel_cuenta_bancaria.cs_borrado = 'N'                                                                                                  "+
        "             AND comrel_cuenta_bancaria.ic_moneda = comcat_moneda.ic_moneda                                                                               "+
        "             AND comrel_cuenta_bancaria.ic_cuenta_bancaria = comrel_pyme_if.ic_cuenta_bancaria                                                            "+
        "             AND comrel_pyme_if.ic_epo = com_documento.ic_epo                                                                                             "+
        "             AND comrel_pyme_if.cs_borrado = 'N'                                                                                                          ");

        // Especificar estatus
        if (cs_vobo_if != null) {
         qrySentencia.append(" AND comrel_pyme_if.cs_vobo_if =  ? ");
         conditions.add(cs_vobo_if);
        }

       // Especificar la Clave de la Moneda
       qrySentencia.append(" AND com_documento.ic_moneda = ? ");
       qrySentencia.append(" AND comrel_cuenta_bancaria.ic_moneda = ? ");
       conditions.add(ic_moneda);
       conditions.add(ic_moneda);

       qrySentencia.append(" AND com_documento.df_fecha_venc BETWEEN trunc(SYSDATE, 'mm') AND SYSDATE  ");

        // Especificar la clave de la EPO
        if ( epo != null && !epo.equals("0")) {
         qrySentencia.append(" AND com_documento.ic_epo = ? ");
         conditions.add(epo);
        }
        
       // Especificar Intermediario Financiero
       if (usuario != null) {
        qrySentencia.append(" AND comrel_pyme_if.ic_if = ? ");
        conditions.add(usuario);
       }


     qrySentencia.append(
        "         GROUP BY                                                                                                                                         "+
        "             com_documento.ic_pyme,                                                                                                                       "+
        "             com_documento.ic_epo, comrel_pyme_if.ic_if                                                                                                   "+
        "         ORDER BY com_documento.ic_epo, com_documento.ic_pyme ASC                                                                                         "+
        "     ) mes                                                                                                                                                "+
        " WHERE                                                                                                                                                    "+
        "     todos.ic_pyme = mes.ic_pyme_mes (+)                                                                                                                  "+
        "     AND todos.ic_epo = mes.ic_epo_mes (+) AND todos.ic_if = mes.ic_if_mes (+)                                                                            ");   

    log.debug("getDocumentQuery " + conditions.toString());
    log.debug("getDocumentQuery " + qrySentencia.toString());

     return qrySentencia.toString();

    }


}