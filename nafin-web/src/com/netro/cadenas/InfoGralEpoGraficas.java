package com.netro.cadenas;
import java.net.URLEncoder;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;

public class InfoGralEpoGraficas extends HttpServlet 
{
  private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

  public void init(ServletConfig config) throws ServletException
  {
    super.init(config);
  }

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    
    String reporte = (request.getParameter("reporte") == null) ? "" : request.getParameter("reporte");
    String pagina = (request.getParameter("pagina") == null) ? "" : request.getParameter("pagina");
    String EPO = (request.getParameter("EPO") == null) ? "" : request.getParameter("EPO");
    
    String url = "http://aplica.nafin.com/ibi_apps8/WFServlet?IBIF_ex="+reporte+"&EPO="+EPO;
    response.sendRedirect(url);
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
    doGet(request,response);
  }
}