package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class AfiliadoCredElectronico implements IQueryGeneratorRegExtJS {

	private static final Log log = ServiceLocator.getInstance().getLog(AfiliadoCredElectronico.class);
	private String numClienteSirac;
	private String rfc;
	private String razonSocial;
	private String fechaDe;
	private String fechaA;
	private String icIf;
	private String datosPersModif;
	private List conditions;	
	
	StringBuffer sentencia = new StringBuffer();
	
	// Variables para generar el archivo
	String numClienteSIRACFile=""; 
	String razonSocialFile=""; 
	String rfcFile=""; 
	String tipoPersonaFile=""; 
	String sectorEconomicoFile=""; 
	String subsectorFile=""; 
	String ramaFile=""; 
	String claseFile=""; 
	String estratoFile=""; 
	String nafinElectronicoFile="";
	private String icPyme;

	public AfiliadoCredElectronico() {}
	
	/**
	 * Obtiene las llaves primarias, 
	 * estoy asumiendo que numero_sirac es la llave primaria y unica
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){ 

		log.info(" getDocumentQuery(E)");		
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		if (!"".equals(numClienteSirac)){
			condicion.append(" and P.in_numero_sirac = ?"); 
			conditions.add(numClienteSirac);
		}	
		if (!"".equals(rfc)){
			condicion.append(" and P.cg_rfc = ?"); 
			conditions.add(rfc);
		}
		if (!"".equals(razonSocial)){
			condicion.append(" and (upper(trim(P.cg_razon_social)) LIKE upper(?))"); 
			conditions.add(razonSocial);
		}
		if (!"".equals(fechaDe)){
			condicion.append(" and trunc(pic.df_alta) >= to_date(?,'dd/mm/yyyy')"); 
			conditions.add(fechaDe);
		}
		if (!"".equals(fechaA)){
			condicion.append(" and trunc(pic.df_alta) <= to_date(?,'dd/mm/yyyy')"); 
			conditions.add(fechaA);
		}
		if (!"".equals(icIf)){
			condicion.append(" and PIC.ic_if = ?"); 
			conditions.add(icIf);
		}			
		if ("on".equals(datosPersModif)){
			condicion.append(" and P.cs_datos_pers_modif = 'S' "); 
		}			
		try {	
			qrySentencia.append(
				"SELECT p.in_numero_sirac as NUM_CLIENTE, 'AfiliadoCredElectronico::getDocumentQuery'"+
				"  FROM comcat_pyme p,"+
				"       comcat_sector_econ se,"+
				"       comcat_subsector sub,"+
				"       comcat_clase c,"+
				"       comcat_rama r,"+
				"       comrel_nafin rn,"+
				"       comrel_pyme_if_credele pic"+
				" WHERE p.cs_credele = 'S'"+
				"   AND rn.ic_epo_pyme_if = p.ic_pyme"+
				"   AND rn.cg_tipo = 'P'"+
				"   AND p.ic_pyme = pic.ic_pyme"+
				"   AND se.ic_sector_econ = sub.ic_sector_econ"+
				"   AND sub.ic_subsector = r.ic_subsector"+
				"   AND sub.ic_sector_econ = r.ic_sector_econ"+
				"   AND r.ic_rama = c.ic_rama"+
				"   AND r.ic_sector_econ = c.ic_sector_econ"+
				"   AND r.ic_subsector = c.ic_subsector"+
				"   AND c.ic_sector_econ = p.ic_sector_econ"+
				"   AND c.ic_subsector = p.ic_subsector"+
				"   AND c.ic_rama = p.ic_rama"+
				"   AND c.ic_clase = p.ic_clase"+
				"   AND (p.cs_invalido IS NULL OR p.cs_invalido != 'S')"+
				condicion +
				" ORDER BY rn.ic_nafin_electronico ASC");
				
		}catch(Exception e){
			log.warn("AfiliadoCredElectronico::getDocumentQueryException "+e);
		}
		log.info(" getDocumentQuery(S)");
		log.debug("Sentencia(getDocumentQuery): " + qrySentencia.toString());
		return qrySentencia.toString();
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){ 

		log.info(" getDocumentSummaryQueryForIds(E)");		
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		
		condicion.append(" AND p.in_numero_sirac in (");
		for (int i = 0; i < ids.size(); i++) { 
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				condicion.append(",");
			}
			condicion.append(" ? " );
			conditions.add(new Long(lItem.get(0).toString()));
		}
		condicion.append(" ) ");
		
		if ("on".equals(datosPersModif)){
			condicion.append(" and P.cs_datos_pers_modif = 'S' "); 
		}
		
		qrySentencia.append(
				"SELECT distinct p.in_numero_sirac as NUM_CLIENTE, p.cg_razon_social as RAZON_SOCIAL, p.cg_rfc as RFC, "+ 
				"       CASE cs_tipo_persona WHEN 'M' THEN 'Moral'"+
				"       WHEN 'F' THEN 'Fisica' ELSE '' END AS tipo_persona,"+
				"       se.cd_nombre AS sector, sub.cd_nombre AS subsector,"+
				"       r.cd_nombre AS rama, c.cd_nombre AS clase, p.ic_estrato AS estrato,"+
				"       rn.ic_nafin_electronico AS nafin_electronico, p.ic_pyme AS IC_PYME"+
				"  FROM comcat_pyme p,"+
				"       comcat_sector_econ se,"+
				"       comcat_subsector sub,"+
				"       comcat_clase c,"+
				"       comcat_rama r,"+
				"       comrel_nafin rn,"+
				"       comrel_pyme_if_credele pic"+
				" WHERE p.cs_credele = 'S'"+
				"   AND rn.ic_epo_pyme_if = p.ic_pyme"+
				"   AND rn.cg_tipo = 'P'"+
				"   AND p.ic_pyme = pic.ic_pyme"+
				"   AND se.ic_sector_econ = sub.ic_sector_econ"+
				"   AND sub.ic_subsector = r.ic_subsector"+
				"   AND sub.ic_sector_econ = r.ic_sector_econ"+
				"   AND r.ic_rama = c.ic_rama"+
				"   AND r.ic_sector_econ = c.ic_sector_econ"+
				"   AND r.ic_subsector = c.ic_subsector"+
				"   AND c.ic_sector_econ = p.ic_sector_econ"+
				"   AND c.ic_subsector = p.ic_subsector"+
				"   AND c.ic_rama = p.ic_rama"+
				"   AND c.ic_clase = p.ic_clase"+
				"   AND (p.cs_invalido IS NULL OR p.cs_invalido != 'S')"+
				condicion+
				" ORDER BY rn.ic_nafin_electronico ASC");
				
		log.info(" getDocumentSummaryQueryForIds(S)");	
		//log.debug("Sentencia(getDocumentSummaryQueryForIds): " + qrySentencia.toString());
		return qrySentencia.toString(); 
	}
	
	/**
	 * Realiza la consulta para generar el archivo csv
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){
	
		log.info(" getDocumentQueryFile(E)");	
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		if (!"".equals(numClienteSirac)){
			condicion.append(" and P.in_numero_sirac = ?"); 
			conditions.add(numClienteSirac);
		}	
		if (!"".equals(rfc)){
			condicion.append(" and P.cg_rfc = ?"); 
			conditions.add(rfc);
		}
		if (!"".equals(razonSocial)){
			condicion.append(" and (upper(trim(P.cg_razon_social)) LIKE upper(?))"); 
			conditions.add(razonSocial);
		}
		if (!"".equals(fechaDe)){
			condicion.append(" and trunc(pic.df_alta) >= to_date(?,'dd/mm/yyyy')"); 
			conditions.add(fechaDe);
		}
		if (!"".equals(fechaA)){
			condicion.append(" and trunc(pic.df_alta) <= to_date(?,'dd/mm/yyyy')"); 
			conditions.add(fechaA);
		}
		if (!"".equals(icIf)){
			condicion.append(" and PIC.ic_if = ?"); 
			conditions.add(icIf);
		}			
		if ("on".equals(datosPersModif)){
			condicion.append(" and P.cs_datos_pers_modif = 'S' "); 
		}	
		qrySentencia.append(
				"SELECT p.in_numero_sirac as NUM_CLIENTE, p.cg_razon_social as RAZON_SOCIAL, p.cg_rfc as RFC, "+ 
				"       CASE cs_tipo_persona WHEN 'M' THEN 'Moral'"+
				"       WHEN 'F' THEN 'Fisica' ELSE '' END AS tipo_persona,"+
				"       se.cd_nombre AS sector, sub.cd_nombre AS subsector,"+
				"       r.cd_nombre AS rama, c.cd_nombre AS clase, p.ic_estrato AS estrato,"+
				"       rn.ic_nafin_electronico AS nafin_electronico, p.ic_pyme AS IC_PYME"+
				"  FROM comcat_pyme p,"+
				"       comcat_sector_econ se,"+
				"       comcat_subsector sub,"+
				"       comcat_clase c,"+
				"       comcat_rama r,"+
				"       comrel_nafin rn,"+
				"       comrel_pyme_if_credele pic"+
				" WHERE p.cs_credele = 'S'"+
				"   AND rn.ic_epo_pyme_if = p.ic_pyme"+
				"   AND rn.cg_tipo = 'P'"+
				"   AND p.ic_pyme = pic.ic_pyme"+
				"   AND se.ic_sector_econ = sub.ic_sector_econ"+
				"   AND sub.ic_subsector = r.ic_subsector"+
				"   AND sub.ic_sector_econ = r.ic_sector_econ"+
				"   AND r.ic_rama = c.ic_rama"+
				"   AND r.ic_sector_econ = c.ic_sector_econ"+
				"   AND r.ic_subsector = c.ic_subsector"+
				"   AND c.ic_sector_econ = p.ic_sector_econ"+
				"   AND c.ic_subsector = p.ic_subsector"+
				"   AND c.ic_rama = p.ic_rama"+
				"   AND c.ic_clase = p.ic_clase"+
				"   AND (p.cs_invalido IS NULL OR p.cs_invalido != 'S')"+
				condicion+
				" ORDER BY rn.ic_nafin_electronico ASC");
		
		log.info(" getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}	

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){

		log.info(" getAggregateCalculationQuery (E)");		
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		if (!"".equals(numClienteSirac)){
			condicion.append(" and P.in_numero_sirac = ?"); 
			conditions.add(numClienteSirac);
		}	
		if (!"".equals(rfc)){
			condicion.append(" and P.cg_rfc = ?"); 
			conditions.add(rfc);
		}
		if (!"".equals(razonSocial)){
			condicion.append(" and (upper(trim(P.cg_razon_social)) LIKE upper(?))"); 
			conditions.add(razonSocial);
		}
		if (!"".equals(fechaDe)){
			condicion.append(" and trunc(pic.df_alta) >= to_date(?,'dd/mm/yyyy')"); 
			conditions.add(fechaDe);
		}
		if (!"".equals(fechaA)){
			condicion.append(" and trunc(pic.df_alta) <= to_date(?,'dd/mm/yyyy')"); 
			conditions.add(fechaA);
		}
		if (!"".equals(icIf)){
			condicion.append(" and PIC.ic_if = ?"); 
			conditions.add(icIf);
		}			
		if ("on".equals(datosPersModif)){
			condicion.append(" and P.cs_datos_pers_modif = 'S' "); 
		}			
		try {	
			qrySentencia.append(
				"SELECT count(p.in_numero_sirac) as total"+
				"  FROM comcat_pyme p,"+
				"       comcat_sector_econ se,"+
				"       comcat_subsector sub,"+
				"       comcat_clase c,"+
				"       comcat_rama r,"+
				"       comrel_nafin rn,"+
				"       comrel_pyme_if_credele pic"+
				" WHERE p.cs_credele = 'S'"+
				"   AND rn.ic_epo_pyme_if = p.ic_pyme"+
				"   AND rn.cg_tipo = 'P'"+
				"   AND p.ic_pyme = pic.ic_pyme"+
				"   AND se.ic_sector_econ = sub.ic_sector_econ"+
				"   AND sub.ic_subsector = r.ic_subsector"+
				"   AND sub.ic_sector_econ = r.ic_sector_econ"+
				"   AND r.ic_rama = c.ic_rama"+
				"   AND r.ic_sector_econ = c.ic_sector_econ"+
				"   AND r.ic_subsector = c.ic_subsector"+
				"   AND c.ic_sector_econ = p.ic_sector_econ"+
				"   AND c.ic_subsector = p.ic_subsector"+
				"   AND c.ic_rama = p.ic_rama"+
				"   AND c.ic_clase = p.ic_clase"+
				"   AND (p.cs_invalido IS NULL OR p.cs_invalido != 'S')"+
				condicion);
				
		}catch(Exception e){
			log.warn("AfiliadoCredElectronico::getAggregateCalculationQueryException "+e);
		}
		log.info(" getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}
	
	/**
	 * Metodo para generar un archivo CSV, de todos los registros
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){	
		
		log.info(" crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;	
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();		
			contenidoArchivo.append("No.cliente SIRAC, Nombre o raz�n Social, RFC, Tipo persona, Sector econ�mico, "+
											"Subsector, Rama, Clase, Estrato, Nafin electr�nico \n");			
		
			while (rs.next())	{	
	
				numClienteSIRACFile = (rs.getString("NUM_CLIENTE") == null) ? "" : rs.getString("NUM_CLIENTE");	
				razonSocialFile = (rs.getString("RAZON_SOCIAL") == null) ? "" : rs.getString("RAZON_SOCIAL");				
				rfcFile = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				tipoPersonaFile = (rs.getString("TIPO_PERSONA") == null) ? "" : rs.getString("TIPO_PERSONA");
				sectorEconomicoFile = (rs.getString("SECTOR") == null) ? "" : rs.getString("SECTOR");
				subsectorFile = (rs.getString("SUBSECTOR") == null) ? "" : rs.getString("SUBSECTOR");
				ramaFile = (rs.getString("RAMA") == null) ? "" : rs.getString("RAMA");
				claseFile = (rs.getString("CLASE") == null) ? "" : rs.getString("CLASE");
				estratoFile = (rs.getString("ESTRATO") == null) ? "" : rs.getString("ESTRATO");
				nafinElectronicoFile = (rs.getString("NAFIN_ELECTRONICO") == null) ? "" : rs.getString("NAFIN_ELECTRONICO");
								
				contenidoArchivo.append(numClienteSIRACFile.replace(',',' ')+", "+
												razonSocialFile.replace(',',' ')+", "+	
												rfcFile.replace(',',' ')+", "+
												tipoPersonaFile.replace(',',' ')+", "+
												sectorEconomicoFile.replace(',',' ')+", "+
												subsectorFile.replace(',',' ')+", "+
												ramaFile.replace(',',' ')+", "+
												claseFile.replace(',',' ')+", "+
												estratoFile.replace(',',' ')+", "+
												nafinElectronicoFile.replace(',',' ')+"\n");		
			
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo csv ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		}
		log.info(" crearCustomFile (S) " + nombreArchivo);
		return nombreArchivo;
	}	

	/**
	 * Metodo para generar un archivo PDF, utilizando paginaci�n
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){	 
		
		log.info("crearPageCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
			pdfDoc.setTable(10,100);
			pdfDoc.setCell("No.cliente SIRAC ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre o raz�n Social ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("RFC ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo persona ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Sector econ�mico ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Subsector ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Rama ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Clase ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estrato ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nafin electr�nico ","celda01",ComunesPDF.CENTER);
			
			
			while (rs.next())	{		
				numClienteSIRACFile = (rs.getString("NUM_CLIENTE") == null) ? "" : rs.getString("NUM_CLIENTE");	
				razonSocialFile = (rs.getString("RAZON_SOCIAL") == null) ? "" : rs.getString("RAZON_SOCIAL");				
				rfcFile = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				tipoPersonaFile = (rs.getString("TIPO_PERSONA") == null) ? "" : rs.getString("TIPO_PERSONA");
				sectorEconomicoFile = (rs.getString("SECTOR") == null) ? "" : rs.getString("SECTOR");
				subsectorFile = (rs.getString("SUBSECTOR") == null) ? "" : rs.getString("SUBSECTOR");
				ramaFile = (rs.getString("RAMA") == null) ? "" : rs.getString("RAMA");
				claseFile = (rs.getString("CLASE") == null) ? "" : rs.getString("CLASE");
				estratoFile = (rs.getString("ESTRATO") == null) ? "" : rs.getString("ESTRATO");
				nafinElectronicoFile = (rs.getString("NAFIN_ELECTRONICO") == null) ? "" : rs.getString("NAFIN_ELECTRONICO");

				
				pdfDoc.setCell(numClienteSIRACFile,"formas",ComunesPDF.CENTER);			
				pdfDoc.setCell(razonSocialFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(rfcFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipoPersonaFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(sectorEconomicoFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(subsectorFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ramaFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(claseFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(estratoFile,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nafinElectronicoFile,"formas",ComunesPDF.CENTER);
			}
		
			pdfDoc.addTable();
			pdfDoc.endDocument();	
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		}		
		
		log.info("crearPageCustomFile (S) "  + nombreArchivo);
		return nombreArchivo;
	}
	
	/**
	 * Obtiene los datos del ic_pyme seleccionado en el grid
	 * @return mapa de resultados
	 */
	public HashMap consultaModificar(){
	
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer tablas = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		HashMap hm = new HashMap();
		try{
			con.conexionDB();     			
			String qrySentencia = "SELECT p.cs_tipo_persona, p.cg_appat, p.cg_apmat, p.cg_nombre,"+
										"       p.cg_razon_social, p.cg_rfc, p.cg_sexo, p.ic_pyme, "+
										"       TO_CHAR (p.df_nacimiento, 'DD/MM/YYYY') AS nacimiento, d.cg_calle,"+
										"       d.cg_colonia, d.cn_cp, d.ic_pais, d.ic_estado, d.ic_municipio,"+
										"       d.cg_municipio, d.cg_telefono1, d.cg_email, p.fn_ventas_net_tot,"+
										"       p.in_numero_emp, p.ic_sector_econ, p.ic_subsector, p.ic_rama,"+
										"       p.ic_clase, p.in_numero_sirac AS numsirac, p.in_numero_troya,"+
										"       d.ic_domicilio_epo, p.ic_estrato, p.ic_pais_origen AS paisorigen,"+
										"       p.cg_curp AS curp, p.cn_fiel AS fiel, d.ic_ciudad AS ciudad"+
										"  FROM comcat_pyme p, com_domicilio d"+
										" WHERE p.ic_pyme = "+icPyme+" AND p.ic_pyme = d.ic_pyme AND d.cs_fiscal = 'S'";
				
			rs = con.queryDB(qrySentencia);
				
			while(rs.next()) {
				String clave = rs.getString(1);
				String descripcion = rs.getString(2);
				
				hm.put("pyme_hide", (rs.getString("ic_pyme") == null) ? "" : rs.getString("ic_pyme"));
				hm.put("tipo_persona", (rs.getString("cs_tipo_persona") == null) ? "" : rs.getString("cs_tipo_persona"));
				hm.put("Apellido paterno", (rs.getString("cg_appat") == null) ? "" : rs.getString("cg_appat"));
				hm.put("Apellido materno", (rs.getString("cg_apmat") == null) ? "" : rs.getString("cg_apmat"));
				hm.put("Nombre", (rs.getString("cg_nombre") == null) ? "" : rs.getString("cg_nombre"));
				hm.put("Razon social", (rs.getString("cg_razon_social") == null) ? "" : rs.getString("cg_razon_social"));
				hm.put("RFC", (rs.getString("cg_rfc") == null) ? "" : rs.getString("cg_rfc"));
				hm.put("RFC.", (rs.getString("cg_rfc") == null) ? "" : rs.getString("cg_rfc"));
				hm.put("Sexo", (rs.getString("cg_sexo") == null) ? "" : rs.getString("cg_sexo"));
				hm.put("Fecha de nacimiento", (rs.getString("nacimiento") == null) ? "" : rs.getString("nacimiento"));
				hm.put("Direccion", (rs.getString("cg_calle") == null) ? "" : rs.getString("cg_calle"));
				hm.put("Colonia", (rs.getString("cg_colonia") == null) ? "" : rs.getString("cg_colonia"));
				hm.put("Codigo Postal", (rs.getString("cn_cp") == null) ? "" : rs.getString("cn_cp"));
				hm.put("Pais", (rs.getString("ic_pais") == null) ? "" : rs.getString("ic_pais"));
				hm.put("Estado", (rs.getString("ic_estado") == null) ? "" : rs.getString("ic_estado"));
				hm.put("Delegacion Municipio", (rs.getString("ic_municipio") == null) ? "" : rs.getString("ic_municipio"));
				hm.put("CG_MUNICIPIO", (rs.getString("cg_municipio") == null) ? "" : rs.getString("cg_municipio"));
				hm.put("Telefono", (rs.getString("cg_telefono1") == null) ? "" : rs.getString("cg_telefono1"));
				hm.put("e-mail", (rs.getString("cg_email") == null) ? "" : rs.getString("cg_email"));
				hm.put("Ventas totales", (rs.getString("fn_ventas_net_tot") == null) ? "" : rs.getString("fn_ventas_net_tot"));
				hm.put("Numero de empleados", (rs.getString("in_numero_emp") == null) ? "" : rs.getString("in_numero_emp"));
				hm.put("Sector economico", (rs.getString("ic_sector_econ") == null) ? "" : rs.getString("ic_sector_econ"));
				hm.put("Subsector", (rs.getString("ic_subsector") == null) ? "" : rs.getString("ic_subsector"));				
				hm.put("Rama", (rs.getString("ic_rama") == null) ? "" : rs.getString("ic_rama"));
				hm.put("Clase", (rs.getString("ic_clase") == null) ? "" : rs.getString("ic_clase"));
				hm.put("SIRAC", (rs.getString("numsirac") == null) ? "" : rs.getString("numsirac"));
				hm.put("NUM_TROYA", (rs.getString("in_numero_troya") == null) ? "" : rs.getString("in_numero_troya"));
				hm.put("IC_DOMICILIO_EPO", (rs.getString("ic_domicilio_epo") == null) ? "" : rs.getString("ic_domicilio_epo"));	
				hm.put("IC_ESTRATO", (rs.getString("ic_estrato") == null) ? "" : rs.getString("ic_estrato"));
				hm.put("pais_origen_mod", (rs.getString("paisorigen") == null) ? "" : rs.getString("paisorigen"));
				hm.put("pais_origen_modF", (rs.getString("paisorigen") == null) ? "" : rs.getString("paisorigen"));
				hm.put("curp_mod", (rs.getString("curp") == null) ? "" : rs.getString("curp"));
				hm.put("fiel_mod", (rs.getString("fiel") == null) ? "" : rs.getString("fiel"));
				hm.put("fiel_modF", (rs.getString("fiel") == null) ? "" : rs.getString("fiel"));
				hm.put("ciudad_mod", (rs.getString("ciudad") == null) ? "" : rs.getString("ciudad"));
			}
			rs.close();
			con.cierraStatement();
	
		} catch (Exception e) {
			System.err.println("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		return hm;
	}

/************************************************************************
 *									GETTERS AND SETTERS									*
 ************************************************************************/
	public String getNumClienteSirac() {
		return numClienteSirac;
	}

	public void setNumClienteSirac(String numClienteSirac) {
		this.numClienteSirac = numClienteSirac;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getFechaDe() {
		return fechaDe;
	}

	public void setFechaDe(String fechaDe) {
		this.fechaDe = fechaDe;
	}

	public String getFechaA() {
		return fechaA;
	}

	public void setFechaA(String fechaA) {
		this.fechaA = fechaA;
	}

	public String getIcIf() {
		return icIf;
	}

	public void setIcIf(String icIf) {
		this.icIf = icIf;
	}

	public String getDatosPersModif() {
		return datosPersModif;
	}

	public void setDatosPersModif(String datosPersModif) {
		this.datosPersModif = datosPersModif;
	}

	public List getConditions(){
		return conditions; 
	}

	public String getIcPyme() {
		return icPyme;
	}

	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}
}