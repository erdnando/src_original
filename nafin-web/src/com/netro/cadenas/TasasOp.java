package com.netro.cadenas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class TasasOp implements IQueryGeneratorRegExtJS {

	//Constructor
	public TasasOp() {
	}

		//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(TasasOp.class);

	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer qrySentencia;
	StringBuffer condicion;
	public List conditions;
	private String paginaOffset="";
	private String paginaNo="";
	private String directorio="";
  	public String cveProducto;
  	public String cveEpo;
	public String cveMoneda;

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		condicion 		= new StringBuffer();
		StringBuffer condicionConsulta = new StringBuffer();

		log.debug("-------------------------:: ");
		log.debug("cveProducto:: "+ cveProducto);
		log.debug("cveEpo:: "+cveEpo);
		log.debug("cveMoneda:: "+cveMoneda);
		log.debug("-------------------------:: ");
		
		if (!cveEpo.equals("TODAS")) {
			condicionConsulta.append(" AND tpe.ic_epo = " + cveEpo);
		} else {
			condicionConsulta.append(" AND tpe.ic_epo is null");
		}
		
		if (!cveMoneda.equals("")) {
			condicionConsulta.append(" AND t.ic_moneda = " + cveMoneda);
		}

	  	qrySentencia.append(" SELECT COUNT(1)" +
									" FROM "   +
									" 	 comrel_tasa_producto_epo tpe, "   +
									" 	 comcat_tasa t, "   +
									" 	 comcat_epo e, "   +
									" 	 comcat_plazo p, "   +
									" 	 comcat_producto_nafin pn"   +
									" WHERE "   +
									" 	tpe.ic_tasa = t.ic_tasa"   +
									" 	AND tpe.ic_epo = e.ic_epo(+) "   +
									" 	AND tpe.ic_producto_nafin = pn.ic_producto_nafin"   +
									" 	AND tpe.IC_PLAZO = p.ic_plazo"   +
									condicionConsulta );
									
        	try{

						if(!"".equals(cveProducto))	{
							condicion.append("AND tpe.ic_producto_nafin = ? ");
							conditions.add(cveProducto);
						}
					
						qrySentencia.append(condicion);

						System.out.println("TasasOp::getAggregateCalculationQuery::qrySentencia:"+qrySentencia.toString());
					} catch(Exception e) {
						System.out.println("TasasOp::getAggregateCalculationQuery "+e);
			    	}
					log.debug(" :::: TasasOp:: getAggregateCalculationQuery (S) :::: ");
					log.debug(getConditions().toString());
		return qrySentencia.toString();

	}

	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery()	{ //genera todos los id's
	    	log.debug(" :::: TasasOp :: getDocumentQuery (E) :::: ");
			StringBuffer qrySentencia = new StringBuffer();
			conditions 		= new ArrayList();
	    	condicion = new StringBuffer();
			StringBuffer condicionConsulta = new StringBuffer();

			log.debug("-------------------------:: ");
			log.debug("cveProducto:: "+ cveProducto);
			log.debug("cveEpo:: "+cveEpo);
			log.debug("cveMoneda:: "+cveMoneda);
			log.debug("-------------------------:: ");

			if (!cveEpo.equals("TODAS")) {
				condicionConsulta.append(" AND tpe.ic_epo = " + cveEpo);
			} else {
				condicionConsulta.append(" AND tpe.ic_epo is null");
			}
		
			if (!cveMoneda.equals("")) {
				condicionConsulta.append(" AND t.ic_moneda = " + cveMoneda);
			}

			qrySentencia.append(" SELECT tpe.ic_tasa_producto_epo " +
										" FROM "   +
										" 	 comrel_tasa_producto_epo tpe, "   +
										" 	 comcat_tasa t, "   +
										" 	 comcat_epo e, "   +
										" 	 comcat_plazo p, "   +
										" 	 comcat_producto_nafin pn"   +
										" WHERE "   +
										" 	tpe.ic_tasa = t.ic_tasa"   +
										" 	AND tpe.ic_epo = e.ic_epo(+) "   +
										" 	AND tpe.ic_producto_nafin = pn.ic_producto_nafin"   +
										" 	AND tpe.IC_PLAZO = p.ic_plazo"   +
										condicionConsulta );

			try{

				if(!"".equals(cveProducto))	{
							condicion.append("AND tpe.ic_producto_nafin = ? ");
							conditions.add(cveProducto);
					}

				qrySentencia.append(condicion +" ORDER BY pn.ic_producto_nafin, tpe.ic_epo, p.IN_PLAZO_DIAS");

				log.debug("EL query de la llave primaria: "+qrySentencia.toString());
			} catch(Exception e) {
				log.debug("TasasOp::getDocumentQueryException "+e);
	    	}
			log.debug(" :::: TasasOp :: getDocumentQuery (S) :::: ");
			log.debug(getConditions());
			return qrySentencia.toString();
	}//getDocumentQuery


	public String getDocumentQueryFile() {

			System.out.println(" :::: TasasOp :: getDocumentQueryFile (E) :::: ");
			StringBuffer 	qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
	    	condicion 		= new StringBuffer();
			StringBuffer condicionConsulta = new StringBuffer();

			log.debug("-------------------------:: ");
			log.debug("cveProducto:: "+ cveProducto);
			log.debug("cveEpo:: "+cveEpo);
			log.debug("cveMoneda:: "+cveMoneda);
			log.debug("-------------------------:: ");

			if (!cveEpo.equals("TODAS")) {
				condicionConsulta.append(" AND tpe.ic_epo = " + cveEpo);
			} else {
				condicionConsulta.append(" AND tpe.ic_epo is null");
			}
		
			if (!cveMoneda.equals("")) {
				condicionConsulta.append(" AND t.ic_moneda = " + cveMoneda);
			}
		
			qrySentencia.append(" SELECT "   +
										" 	tpe.ic_tasa_producto_epo AS IDTASAPROD, "   +
										" 	t.CD_NOMBRE AS TASANOM, pn.IC_NOMBRE AS PRODNOM, "   +
										" 	CASE WHEN e.cg_razon_social is null THEN " +
										" 	'Todas las Epos' ELSE e.cg_razon_social " +
										" 	END as cg_razon_social, " +
										" 	p.CG_DESCRIPCION AS PLAZO, "   +
										" 	p.ic_plazo AS INI_PLAZO, "   +
										" 	p.in_plazo_inicio AS IC_PLAZO, "   +
										" 	t.ic_tasa AS INI_TASA"   +
										" FROM "   +
										" 	 comrel_tasa_producto_epo tpe, "   +
										" 	 comcat_tasa t, "   +
										" 	 comcat_epo e, "   +
										" 	 comcat_plazo p, "   +
										" 	 comcat_producto_nafin pn"   +
										" WHERE "   +
										" 	tpe.ic_tasa = t.ic_tasa"   +
										" 	AND tpe.ic_epo = e.ic_epo(+) "   +
										" 	AND tpe.ic_producto_nafin = pn.ic_producto_nafin"   +
										" 	AND tpe.IC_PLAZO = p.ic_plazo"   +
										condicionConsulta );

			try {

				if(!"".equals(cveProducto))	{
							condicion.append("AND tpe.ic_producto_nafin = ? ");
							conditions.add(cveProducto);
					}

				qrySentencia.append(condicion +" ORDER BY pn.ic_producto_nafin, tpe.ic_epo, p.IN_PLAZO_DIAS");

				log.debug("TasasOp::getDocumentQueryFile::qrySentencia:"+qrySentencia.toString());
		    }catch(Exception e){
				log.debug("TasasOp::getDocumentQueryFileException "+e);
			}
			log.debug(" :::: TasasOp :: getDocumentQueryFile (S) :::: ");
			return qrySentencia.toString();
	}//getDocumentQueryFile

	/**
		 * Obtiene el query necesario para mostrar la información completa de
		 * una página a partir de las llaves primarias enviadas como parámetro
		 * @return Cadena con la consulta de SQL, para obtener la información
		 * 	completa de los registros con las llaves especificadas
	 */
	 //Checar la implementacion de este metodo
	public String getDocumentSummaryQueryForIds(List ids)  {
	  		log.debug(" :::: BitCambiosNafinCA :: getDocumentSummaryQueryForIds (E) :::: ");
			int i=0;
			StringBuffer qrySentencia = new StringBuffer();
			conditions 		= new ArrayList();
			StringBuffer condicionConsulta = new StringBuffer();

			log.debug("-------------------------:: ");
			log.debug("cveProducto:: "+ cveProducto);
			log.debug("cveEpo:: "+cveEpo);
			log.debug("cveMoneda:: "+cveMoneda);
			log.debug("-------------------------:: ");
			
			if (!cveEpo.equals("TODAS")) {
				condicionConsulta.append(" AND tpe.ic_epo = " + cveEpo);
			} else {
				condicionConsulta.append(" AND tpe.ic_epo is null");
			}
		
			if (!cveMoneda.equals("")) {
				condicionConsulta.append(" AND t.ic_moneda = " + cveMoneda);
			}

			StringBuffer clavesCombinaciones = new StringBuffer("");
			
			log.debug("*************************La lista de ids es: "+ids);
			for (Iterator it = ids.iterator(); it.hasNext();) {
				List lItem = (List)it.next();
				clavesCombinaciones.append("?,");
				//this.conditions.add(lItem.get(0));
			}
			clavesCombinaciones = clavesCombinaciones.delete(clavesCombinaciones.length()-1,clavesCombinaciones.length());

			qrySentencia.append(" SELECT "   +
										" 	tpe.ic_tasa_producto_epo AS IDTASAPROD, "   +
										" 	t.CD_NOMBRE AS TASANOM, pn.IC_NOMBRE AS PRODNOM, "   +
										" 	CASE WHEN e.cg_razon_social is null THEN " +
										" 	'Todas las Epos' ELSE e.cg_razon_social " +
										" 	END as cg_razon_social, " +
										" 	p.CG_DESCRIPCION AS PLAZO, "   +
										" 	p.ic_plazo AS INI_PLAZO, "   +
										" 	p.in_plazo_inicio AS IC_PLAZO, "   +
										" 	t.ic_tasa AS INI_TASA"   +
										" FROM "   +
										" 	 comrel_tasa_producto_epo tpe, "   +
										" 	 comcat_tasa t, "   +
										" 	 comcat_epo e, "   +
										" 	 comcat_plazo p, "   +
										" 	 comcat_producto_nafin pn"   +
										" WHERE "   +
										" 	tpe.ic_tasa = t.ic_tasa"   +
										" 	AND tpe.ic_epo = e.ic_epo(+) "   +
										" 	AND tpe.ic_producto_nafin = pn.ic_producto_nafin"   +
										" 	AND tpe.IC_PLAZO = p.ic_plazo"   +
										condicionConsulta );

			if(!"".equals(cveProducto))	{
							conditions.add(cveProducto);
					}

			qrySentencia.append(condicion +" ORDER BY pn.ic_producto_nafin, tpe.ic_epo, p.IN_PLAZO_DIAS");

			log.debug("el query queda de la siguiente manera "+qrySentencia.toString());
			log.debug(this.getConditions().toString());
			log.debug(" :::: TasasOp :: getDocumentSummaryQueryForIds (S) :::: ");
			return qrySentencia.toString();
	}//getDocumentSummaryQueryForIds

	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		String nombreArchivo = "";
		log.debug("crearCustomFile (E)");
	
		return nombreArchivo;
	}

	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		String nombreArchivo = "";
	
		return nombreArchivo;
	}


	/*****************************************************
		 GETTERS
	*******************************************************/

		/**
		  Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
		  @return Lista con los parametros de las condiciones
		 */
		public List getConditions() {  return conditions;  }
	  /**
		 * Obtiene el numero de pagina.
		 * @return Cadena con el numero de pagina
		 */
		public String getPaginaNo() { return paginaNo; 	}
	  /**
		 * Obtiene el offset de la pagina.
		 * @return Cadena con el offset de pagina
		 */
	public String getPaginaOffset() { return paginaOffset; 	}


	/*****************************************************
						 SETTERS
	*******************************************************/
		/**
		 * Establece el numero de pagina.
		 * @param  newPaginaNo Cadena con el numero de pagina
		 */
		public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }

	  /**
		 * Establece el offset de la pagina.
		 * @param newPaginaOffset Cadena con el offset de pagina
		 */
		public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}


		public String getDirectorio() {
			return directorio;
		}

		public void setDirectorio(String directorio) {
			this.directorio = directorio;
		}

	  public String getCve_producto() {
	      return cveProducto;
	    }

	    public void setCve_producto(String cve_producto) {
	      this.cveProducto = cve_producto;
	  }
	  
	  public String getEpo() {
	      return cveEpo;
	    }

	    public void setCveEpo(String cve_epo) {
	      this.cveEpo = cve_epo;
	  }
	  
	  public String getCveMoneda() {
	      return cveMoneda;
	    }

	    public void setCveMoneda(String moneda) {
	      this.cveMoneda = moneda;
	  }

}
