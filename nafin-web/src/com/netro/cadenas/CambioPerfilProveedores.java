package com.netro.cadenas;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.HashMap;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Bitacora;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/******************************************************************************************
 * Fodea 11-2014																									*
 * Descripci�n: Migraci�n de la pantalla Administraci�n-Afiliados-Consultas-Proveedores	*
 * 				 a ExtJS. Realiza la consulta para llenar el form de Cambio de perfil.		*
 * Elabor�: Agust�n Bautista Ruiz																			*
 * Fecha: 24/06/2014																								*
 ******************************************************************************************/
public class CambioPerfilProveedores  {
	
	private static final Log log = ServiceLocator.getInstance().getLog(CambioPerfilProveedores.class);
	private String perfil;
	//	Las actuales
	private String afiliados;
	private String sTipoAfiliado;
	private String sClaveAfiliado;
	private String txtLogin;
	private String txtTipoAfiliado;
	private String txtNafinElectronico;
	private String txtSesion;
	private String valoresAnteriores;
	private String valoresActuales;
	
	public CambioPerfilProveedores() {}
	
	/**
	 * Obtiene los datos del form cambioPerfil
	 * @throws java.lang.Exception
	 * @return datos
	 */
	public HashMap obtieneDatosPerfil() throws Exception {
	
		log.info("obtieneDatosPerfil (E)");
		
		HashMap datos = new HashMap();
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		
		try {	
			
			if( !this.sTipoAfiliado.equals("") ){
					
				con.conexionDB();
				
				String sCampos = null;
				String sTabla  = null;
				String sQuery  = null;
				
				if ("N".equals(sTipoAfiliado)) {
					this.sClaveAfiliado = "0";	//Para usuarios Nafin la clave de afiliado no aplica. se establece un 0
				}
				System.err.println("sClaveAfiliado: "+ this.sClaveAfiliado);	
				
				if( this.sTipoAfiliado.equals("E") && !this.sClaveAfiliado.equals("") ){
					strSQL.append(
								"SELECT  IC_NAFIN_ELECTRONICO, " + 
								"			DATOS.CG_RAZON_SOCIAL, " +
								"			1 AS TIPOPERFIL, " +
								"			DECODE(CS_INTERNACIONAL,'S','I','') AS INTERNACIONAL " +
								"FROM 	COMCAT_EPO DATOS, COMREL_NAFIN N " +
								"WHERE 	DATOS.IC_EPO = N.IC_EPO_PYME_IF " +
								"			AND IC_EPO_PYME_IF = ? " +
								"			AND CG_TIPO = ? "
					);
				} else if( this.sTipoAfiliado.equals("I") && !this.sClaveAfiliado.equals("") ){					
					strSQL.append(
								"SELECT IC_NAFIN_ELECTRONICO, " +
								"			DATOS.CG_RAZON_SOCIAL, " +
								"			DECODE(NVL(DATOS.IC_CONTRAGARANTE, 0), 0, 2, 6) AS TIPOPERFIL " +
								"FROM 	COMCAT_IF DATOS, COMREL_NAFIN N " +
								"WHERE 	DATOS.IC_IF = N.IC_EPO_PYME_IF " +
								"			AND IC_EPO_PYME_IF = ? " +
								"			AND CG_TIPO = ? "
					);
				} else if( this.sTipoAfiliado.equals("P") && !this.sClaveAfiliado.equals("") ){	
					strSQL.append(
								"SELECT 	IC_NAFIN_ELECTRONICO, " +
								"		   DATOS.CG_RAZON_SOCIAL, " +
								"			3 AS TIPOPERFIL " +
								"FROM 	COMCAT_PYME DATOS, COMREL_NAFIN N " +
								"WHERE 	DATOS.IC_PYME = N.IC_EPO_PYME_IF " +
								"			AND IC_EPO_PYME_IF = ? " +
								"			AND CG_TIPO = ? "
					);
				} else if( this.sTipoAfiliado.equals("D") && !this.sClaveAfiliado.equals("") ){
					strSQL.append(
								"SELECT 	IC_NAFIN_ELECTRONICO, " +
								"			DATOS.CG_RAZON_SOCIAL, " +
								"			5 AS TIPOPERFIL " +
								"FROM 	FONCAT_DISTRIBUIDOR DATOS, COMREL_NAFIN N " +
								"WHERE 	DATOS.IC_DISTRIBUIDOR = N.IC_EPO_PYME_IF " +
								"			AND IC_EPO_PYME_IF = ? " +
								"			AND CG_TIPO = ? "
					);
				} else if( this.sTipoAfiliado.equals("C") && !this.sClaveAfiliado.equals("") ){
					strSQL.append(
								"SELECT 	IC_NAFIN_ELECTRONICO, " +
								"			DATOS.CG_RAZON_SOCIAL, " +
								"			3 AS TIPOPERFIL " +
								"FROM 	FONCAT_CLIENTE DATOS, COMREL_NAFIN N " +
								"WHERE 	DATOS.IC_CLIENTE = N.IC_EPO_PYME_IF " +
								"			AND IC_EPO_PYME_IF = ? " +
								"			AND CG_TIPO = ? "
					);
				} else if( this.sTipoAfiliado.equals("M") && !this.sClaveAfiliado.equals("") ){	
					strSQL.append(
								"SELECT 	IC_NAFIN_ELECTRONICO, " +
								"			DATOS.CG_RAZON_SOCIAL, " +
								"			11 AS TIPOPERFIL " +
								"FROM 	COMCAT_MANDANTE DATOS, COMREL_NAFIN N " +
								"WHERE 	DATOS.IC_MANDANTE = N.IC_EPO_PYME_IF " +
								"			AND N.IC_EPO_PYME_IF = ? " +
								"			AND CG_TIPO = ? "
					);
				} else if( this.sTipoAfiliado.equals("A") && !this.sClaveAfiliado.equals("") ){	
					strSQL.append(
								"SELECT 	N.IC_NAFIN_ELECTRONICO, " +
								"			DATOS.CG_RAZON_SOCIAL, " +
								"			1 AS TIPOPERFIL " +
								"FROM 	FE_AFIANZADORA DATOS, COMREL_NAFIN N " +
								"WHERE 	DATOS.IC_AFIANZADORA = N.IC_EPO_PYME_IF " +
								"			AND IC_EPO_PYME_IF = ? " +
								"			AND CG_TIPO = ? "
					);
				} else if( this.sTipoAfiliado.equals("F") && !this.sClaveAfiliado.equals("") ){	
					strSQL.append(
								"SELECT 	N.IC_NAFIN_ELECTRONICO, " +
								"			DATOS.CG_NOMBRE CG_RAZON_SOCIAL, " +
								"			13 AS TIPOPERFIL " +
								"FROM 	FE_FIADO DATOS, COMREL_NAFIN N " +
								"WHERE 	DATOS.IC_FIADO = N.IC_EPO_PYME_IF " +
								"			AND IC_EPO_PYME_IF = ? " +
								"			AND CG_TIPO = ? "
					);
				} else if( this.sTipoAfiliado.equals("N") ){
					
				} else {
					strSQL.append(
								"SELECT 	0 AS IC_NAFIN_ELECTRONICO, " +
								"			'Tipo de afiliado: " + this.sTipoAfiliado + " no implementado' AS CG_RAZON_SOCIAL, " +
								"			0 AS TIPOPERFIL " +
								"FROM 	DUAL "
					);
				}	

				if( !this.sTipoAfiliado.equals("N") ){
					log.debug("Sentencia: " + strSQL.toString());					 
					PreparedStatement ps = con.queryPrecompilado(strSQL.toString());
					ps.setString(1, this.sClaveAfiliado);
					ps.setString(2, this.sTipoAfiliado);
					ResultSet rs = ps.executeQuery();
				
					if (rs.next()) {
						datos.put("S_NOMBRE_EMPRESA", (rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
						datos.put("IC_NAFIN_ELECTRONICO", "" + rs.getInt("IC_NAFIN_ELECTRONICO"));
						datos.put("TIPOPERFIL", "" + rs.getInt("TIPOPERFIL"));
						if( this.sTipoAfiliado.equals("E") ){
							datos.put("INTERNACIONAL", (rs.getString("INTERNACIONAL")==null)?"":rs.getString("INTERNACIONAL"));
						}else{
							datos.put("INTERNACIONAL", "");
						}
					}
					rs.close();
					ps.close();			
				}				
				
				if( this.sTipoAfiliado.equals("N") || this.sTipoAfiliado.equals("A") ){
					strSQL.append( " SELECT SC_TIPO_USUARIO FROM SEG_PERFIL WHERE CC_PERFIL = ? " );	
					log.debug("Sentencia: " + strSQL.toString());					 
					PreparedStatement ps = con.queryPrecompilado(strSQL.toString());
					ps.setString(1, this.perfil);
					ResultSet rs = ps.executeQuery();
					if (rs.next()) {
						datos.put("TIPOPERFIL", "" + rs.getInt("SC_TIPO_USUARIO"));
					}
					rs.close();
					ps.close();	
				}
				
				if( this.sTipoAfiliado.equals("N") ){
					datos.put("S_NOMBRE_EMPRESA", (datos.get("TIPOPERFIL").equals("4"))?"NACIONAL FINANCIERA":"BANCOMEXT");
					datos.put("IC_NAFIN_ELECTRONICO", "0");
				}
				
			} // Fin [!this.sTipoAfiliado.equals("")]
			
		} catch( Exception e){
			log.warn("Error en obtieneDatosPerfil: " + e );
		} finally {
			if ( con.hayConexionAbierta() ) {
				con.cierraConexionDB();
			}
		} //	Fin del try
		
		log.info("obtieneDatosPerfil (S)");
		return datos;
		
	}

	/**
	 * Una vez que modifico el perfil, guardo la acci�n en Bitacora. Esta clase y 
	 * m�todo ya existen, por lo tanto solo se le pasan los par�metros y se recibe
	 * la respuesta
	 * @return true: proceso exitoso
	 */
	public boolean guardaCambiosEnBitacora() throws Exception {
		
		log.info("guardaCambiosEnBitacora (E)");
		boolean exito = false;
		AccesoDB con  = new AccesoDB();
		try{
			con.conexionDB();
			Bitacora.grabarEnBitacora(con, "SEGCAMBIOPERFIL", this.txtTipoAfiliado, this.txtNafinElectronico, this.txtSesion, this.valoresAnteriores, this.valoresActuales);			
			exito = true;
		}catch(Exception e){
			log.warn("Error en guardaCambiosEnBitacora: " + e );
			exito = false;
			throw e;
		} finally {
			if ( con.hayConexionAbierta() == true ){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}	
		}
		log.info("guardaCambiosEnBitacora (S)");
		return exito;
	}

/************************************************************************
 *									GETTERS AND SETTERS									*
 ************************************************************************/
	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getAfiliados() {
		return afiliados;
	}

	public void setAfiliados(String afiliados) {
		this.afiliados = afiliados;
	}

	public String getSTipoAfiliado() {
		return sTipoAfiliado;
	}

	public void setSTipoAfiliado(String sTipoAfiliado) {
		this.sTipoAfiliado = sTipoAfiliado;
	}

	public String getSClaveAfiliado() {
		return sClaveAfiliado;
	}

	public void setSClaveAfiliado(String sClaveAfiliado) {
		this.sClaveAfiliado = sClaveAfiliado;
	}

	public String getTxtLogin() {
		return txtLogin;
	}

	public void setTxtLogin(String txtLogin) {
		this.txtLogin = txtLogin;
	}

	public String getTxtTipoAfiliado() {
		return txtTipoAfiliado;
	}

	public void setTxtTipoAfiliado(String txtTipoAfiliado) {
		this.txtTipoAfiliado = txtTipoAfiliado;
	}

	public String getTxtNafinElectronico() {
		return txtNafinElectronico;
	}

	public void setTxtNafinElectronico(String txtNafinElectronico) {
		this.txtNafinElectronico = txtNafinElectronico;
	}

	public String getTxtSesion() {
		return txtSesion;
	}

	public void setTxtSesion(String txtSesion) {
		this.txtSesion = txtSesion;
	}

	public String getValoresAnteriores() {
		return valoresAnteriores;
	}

	public void setValoresAnteriores(String valoresAnteriores) {
		this.valoresAnteriores = valoresAnteriores;
	}

	public String getValoresActuales() {
		return valoresActuales;
	}

	public void setValoresActuales(String valoresActuales) {
		this.valoresActuales = valoresActuales;
	}
	
}