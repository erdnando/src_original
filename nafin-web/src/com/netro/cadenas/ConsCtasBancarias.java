package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase obtiene datos de las cuentas bancarias de EPO's, en base a
 * la pyme y la epo y moneda selecionadas.
 *
 * @author Andrea Isabel Luna Aguill�n
 */

public class ConsCtasBancarias implements IQueryGeneratorRegExtJS  {
	public ConsCtasBancarias() {	}
	
	private List 	conditions;
	StringBuffer	strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsCtasBancarias.class);//Variable para enviar mensajes al Log.
	
	private String claveEPO;
	private String pyme;
	private String moneda;
	
	public String getAggregateCalculationQuery() {
		return "";
	}
	
	public String getDocumentQuery() {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentQueryFile(){
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		
		log.debug("ic_epo"+this.claveEPO);
		log.debug("ic_pyme"+this.pyme);
		log.debug("ic_moneda"+this.moneda);
		
		strQuery.append(
		"SELECT pi.ic_if,i.cg_razon_social as razon_social,cb.cg_banco,cb.cg_numero_cuenta,cb.cg_sucursal,epo.cg_razon_social"	+
		" FROM comrel_cuenta_bancaria cb, comrel_pyme_if pi, comcat_if i, comcat_epo epo"		+
		" WHERE cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria"		+
		" AND pi.ic_if = i.ic_if"	+
		" AND pi.ic_epo = epo.ic_epo"	+
		" AND epo.ic_epo = ?"		+
		" AND cb.ic_pyme = ?" 	+
		" AND cb.ic_moneda = ?"	+
		" AND cb.cs_borrado = 'N'"	+
		" AND pi.cs_borrado = 'N'"	+
		" AND pi.cs_vobo_if = 'S'"	+
		" ORDER BY razon_social");
		conditions.add(this.claveEPO);
		conditions.add(this.pyme);
		conditions.add(this.moneda);
		
	   log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile "+conditions);
		
		return strQuery.toString();
	}
	
		/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)  {
		String nombreArchivo = "";
		
		if("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[]	=	{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual	=	fechaActual.substring(0,2);
				String mesActual	=	meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual	=	fechaActual.substring(6,10);
				String horaActual	=	new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
				horaActual, "formas", ComunesPDF.RIGHT);
				pdfDoc.setTable(5, 100);
				pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Banco de Servicio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. de Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Sucursal","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				
				while (rs.next()){
					String RAZON_SOCIAL	=	(rs.getString("RAZON_SOCIAL") == null) ? "" : rs.getString("RAZON_SOCIAL");
					String CG_BANCO	=	(rs.getString("CG_BANCO") == null) ? "" : rs.getString("CG_BANCO");
					String CG_NUMERO_CUENTA	=	(rs.getString("CG_NUMERO_CUENTA") == null) ? "" : rs.getString("CG_NUMERO_CUENTA");
					String CG_SUCURSAL = (rs.getString("CG_SUCURSAL") == null) ? "" : rs.getString("CG_SUCURSAL");
					String CG_RAZON_SOCIAL = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
					
					pdfDoc.setCell(RAZON_SOCIAL,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CG_BANCO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CG_NUMERO_CUENTA,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CG_SUCURSAL,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CG_RAZON_SOCIAL, "formas", ComunesPDF.CENTER);
				}		
				pdfDoc.addTable();
				pdfDoc.endDocument();
				
			}catch (Throwable e) {
				throw new AppException("Error al generar el archivo",e);
			}finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}
		return nombreArchivo;
	}
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions()	
	{
		return conditions;
	}
	
	public String getClaveEPO()	
	{
		return	claveEPO;
	}
	
	public void setClaveEPO(String claveEPO)	
	{
		this.claveEPO	=	claveEPO;
	}
	
	public String getPyme()	
	{
		return	pyme;
	}
	
	public void setPyme (String pyme)	
	{
		this.pyme	=	pyme;
	}
	
	public String getMoneda ()	
	{
		return moneda;
	}
	
	public void setMoneda (String moneda)	
	{
		this.moneda	=	moneda;
	}
}