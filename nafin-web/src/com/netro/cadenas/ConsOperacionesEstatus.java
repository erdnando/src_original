package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Clase para la pantalla - Reporte de Operaciones por Estatus
 * (Consulta)
 * Autor: Jose Luis Rodriguez Araujo
 */
public class ConsOperacionesEstatus implements IQueryGeneratorRegExtJS {
	
	private ArrayList conditions = null;
	private final static Log log = ServiceLocator.getInstance().getLog(ConsOperacionesEstatus.class);
	private String nomCliente;
	private String numSirac;
	private String numDescuento;
	private String fechaEmision;
	private String fechaVencimiento;
	private String moneda;
	private String tipoCredito;
	private String montoDocumento;
	private String montoDescontar;
	private String folio;
	private String estatus;
	private String numPrestamo;
	private String operacionEstatus;
	private String tipoUsuario;
	
	public ConsOperacionesEstatus() {
	}
	
	public List getConditions() {  return conditions;  }

	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E) ::..");
		this.conditions = new ArrayList();
		StringBuffer queryCons = new StringBuffer();

		log.debug("lsQrySentencia: "+queryCons);
		log.debug("conditions::"+this.conditions);
		log.info("getAggregateCalculationQuery(S) ::..");
		return queryCons.toString();
	}
	
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E) ::..");
		this.conditions = new ArrayList();
		StringBuffer queryCons = new StringBuffer();

		log.debug("lsQrySentencia: "+queryCons);
		log.debug("conditions::"+this.conditions);
		log.info("getDocumentQuery(S) ::..");
		return queryCons.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		StringBuffer queryCons 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		log.debug("..:: qrySentencia "+queryCons.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return queryCons.toString();
	}
	
	/**
	 * Metodo que permite obtener el query dependiendo del del estatus de la 
	 * operacion 
	 * @return queryCons Es la cadena que contiene el query correspondiente
	 */
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		StringBuffer queryCons 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		queryCons.append("SELECT P.cg_razon_social as nombrePyme, " +
		"SP.ig_clave_sirac as claveSirac, SP.ig_numero_docto as numDocto, " +
		"TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, " +
		"TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, " +
		"M.cd_nombre as nombreMoneda, TC.cd_descripcion as descTipoCred, " +
		"SP.fn_importe_docto as importeDocto, SP.fn_importe_dscto as importeDscto, " +
		"SP.ic_solic_portal as Folio, ES.cd_descripcion as estatusSolic, " +
		"SP.ig_numero_prestamo as numPrestamo FROM com_solic_portal SP, " +
		"comcat_estatus_solic ES, comcat_tipo_credito TC, " +
		"comcat_pyme P, comcat_moneda M "+
		"WHERE SP.ic_estatus_solic = ES.ic_estatus_solic " +
		"AND SP.ic_tipo_credito = TC.ic_tipo_credito " +
		"AND SP.ig_clave_sirac = P.in_numero_sirac(+) " +
		"AND SP.ic_moneda = M.ic_moneda(+) ");
		if(operacionEstatus != null && !operacionEstatus.equals("")){
			queryCons.append("AND SP.ic_estatus_solic = ? ");
			if(operacionEstatus.equals("En Proceso")){
				conditions.add(new Integer(2));
			}else if(operacionEstatus.equals("Operadas")){
				conditions.add(new Integer(3));
			}
		}
		queryCons.append("AND SP.df_carga >= trunc(SYSDATE) AND SP.df_carga < trunc(SYSDATE)+1");
		
		log.debug("..:: qrySentencia "+queryCons.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQueryFile(S)");
		return queryCons.toString();
	}
	
	/**
	 * metodo que genera archivo por pagina
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		return "";
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipoArchivo){
		log.info("crearCustomFile(E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo creaArchivo = new CreaArchivo();
		int contador = 0;
		
		try{			
			if(tipoArchivo.equals("pdf")){
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio",
				"Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);			
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
				
				pdfDoc.setTable(12, 100);
				pdfDoc.setCell("Estatus:","celda01",ComunesPDF.CENTER,2);
				pdfDoc.setCell(getOperacionEstatus(),"formas",ComunesPDF.LEFT,10);
				pdfDoc.setCell("Nombre del Cliente","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("N�mero de Sirac","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento del Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Tipo de Cr�dito","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Monto del Dcoumento","celda01",ComunesPDF.RIGHT);				
				pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.RIGHT);
				pdfDoc.setCell("Folio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("N�mero de Prestamo","celda01",ComunesPDF.CENTER);
			}
			
			while(rs.next()){
				String nombreCliente = (rs.getString("nombrePyme") == null) ? "" : rs.getString("nombrePyme");
				String numeroSirac  = (rs.getString("claveSirac") == null) ? "" : rs.getString("claveSirac");
				String numeroDocumento  = (rs.getString("numDocto") == null) ? "" : rs.getString("numDocto");
				String fechaEmision  = (rs.getString("fechaETC") == null) ? "" : rs.getString("fechaETC");
				String fechaVencimiento  = (rs.getString("fechaDscto") == null) ? "" : rs.getString("fechaDscto");
				String moneda  = (rs.getString("nombreMoneda") == null) ? "" : rs.getString("nombreMoneda");
				String tipoCredito  = (rs.getString("descTipoCred") == null) ? "" : rs.getString("descTipoCred");
				String montoDocumento  = (rs.getString("importeDocto") == null) ? "" : rs.getString("importeDocto");
				String montoDescontar  = (rs.getString("importeDscto") == null) ? "" : rs.getString("importeDscto");
				String folio  = (rs.getString("Folio") == null) ? "" : rs.getString("Folio");
				String estatus  = (rs.getString("estatusSolic") == null) ? "" : rs.getString("estatusSolic");
				String numeroPrestamo  = (rs.getString("numPrestamo") == null) ? "" : rs.getString("numPrestamo");
				if(tipoArchivo.equals("pdf")) {
					pdfDoc.setCell(nombreCliente,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(numeroSirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(tipoCredito,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(montoDocumento, 2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(montoDescontar, 2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(folio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(numeroPrestamo,"formas",ComunesPDF.CENTER);
				}			
				contador++;
			}
			if(tipoArchivo.equals("pdf")) {
				pdfDoc.setCell("Total " + getOperacionEstatus() +":","celda01",ComunesPDF.CENTER,2);
				pdfDoc.setCell(String.valueOf(contador),"formas",ComunesPDF.CENTER,10);
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}
			
		}catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		log.info("crearCustomFile(S)");
		return nombreArchivo;
	}
	
	public void setNomCliente(String nomCliente) {
		this.nomCliente = nomCliente;
	}

	public String getNomCliente() {
		return nomCliente;
	}

	public void setNumSirac(String numSirac) {
		this.numSirac = numSirac;
	}

	public String getNumSirac() {
		return numSirac;
	}

	public void setNumDescuento(String numDescuento) {
		this.numDescuento = numDescuento;
	}

	public String getNumDescuento() {
		return numDescuento;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}

	public String getTipoCredito() {
		return tipoCredito;
	}

	public void setMontoDocumento(String montoDocumento) {
		this.montoDocumento = montoDocumento;
	}

	public String getMontoDocumento() {
		return montoDocumento;
	}

	public void setMontoDescontar(String montoDescontar) {
		this.montoDescontar = montoDescontar;
	}

	public String getMontoDescontar() {
		return montoDescontar;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getFolio() {
		return folio;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setNumPrestamo(String numPrestamo) {
		this.numPrestamo = numPrestamo;
	}

	public String getNumPrestamo() {
		return numPrestamo;
	}

	public void setOperacionEstatus(String operacionEstatus) {
		this.operacionEstatus = operacionEstatus;
	}

	public String getOperacionEstatus() {
		return operacionEstatus;
	}


	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}


	public String getTipoUsuario() {
		return tipoUsuario;
	}
}