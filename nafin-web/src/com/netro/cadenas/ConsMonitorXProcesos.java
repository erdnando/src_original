package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsMonitorXProcesos implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsMonitorXProcesos.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_producto;
	private String ic_if;
	private String fechaOperacDe;
	private String fechaOperacA;
		 
	public ConsMonitorXProcesos() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
	
		if("0".equals(ic_producto)){ //Credito Electronico
		
			qrySentencia.append( " SELECT /*+use_nl(sp tc p m cif)*/ "+
										" sp.ic_solic_portal AS FOLIO,  "+
										" IF.cg_razon_social AS INTERMEDIARIO,  "+
										" py.cg_razon_social AS NOMBRE_CLIENTE,  "+
										" sp.ig_clave_sirac AS NUM_CLIENTE_SIRAC, "+
										" sp.fn_importe_dscto AS MONTO_DESC, "+
										" sp.ig_numero_prestamo AS NUM_PRESTAMO,    "+      
										" sp.ig_numero_linea_credito AS NUM_PROYECTO, "+
										" sp.ig_numero_contrato AS NUM_CONTRATO, "+
										" TO_CHAR (sp.df_operacion, 'dd/mm/yyyy') AS FECHA_OPERACION ,"+
										" '' AS NOMBRE_EPO, " +
										" '' AS NUM_SOLICITUD, " +	
										" '' AS MODALIDAD " +	
										
										" FROM com_solic_portal sp, comcat_if if, comcat_pyme py"   +
										" WHERE sp.ic_if = if.ic_if"   +
										" AND py.in_numero_sirac (+) = sp.ig_clave_sirac "   +
										" AND sp.ic_bloqueo IS NULL "+
										" AND sp.ig_numero_prestamo IS NOT NULL ");
			
			if(!fechaOperacDe.equals("") &&  !fechaOperacA.equals("") ){
				qrySentencia.append(" and SP.df_operacion >= trunc(TO_DATE( ? ,'dd/mm/yyyy'))  "+
										  " and SP.df_operacion < trunc(TO_DATE( ? ,'dd/mm/yyyy')+1) ");
				conditions.add(fechaOperacDe);
				conditions.add(fechaOperacA);
			
			}else{
				qrySentencia.append(" AND sp.df_operacion >= trunc(SYSDATE) " +
										  " AND sp.df_operacion < trunc(SYSDATE+1) ");
			}

			if(!ic_if.equals("") ){
				qrySentencia.append(" and SP.ic_if = ? ");
				conditions.add(ic_if);
			}
			
			qrySentencia.append(" order by MONTO_DESC DESC");
		
		}else  if("1".equals(ic_producto)){ //Descuento Electronico 
			
			qrySentencia.append(	" SELECT /*+use_nl(doc sel sol cif cpe pym epo)*/ "+
										" cif.cg_razon_social AS INTERMEDIARIO,  "+
										" epo.cg_razon_social AS NOMBRE_EPO, "+
										" pym.cg_razon_social AS NOMBRE_CLIENTE,  "+
										" sol.ic_folio AS NUM_SOLICITUD, "+
										" pym.in_numero_sirac AS NUM_CLIENTE_SIRAC, "+
										" sel.in_importe_recibir AS MONTO_DESC, "+
										" sol.ig_numero_prestamo AS NUM_PRESTAMO, "+
										" TO_CHAR (sol.df_operacion, 'dd/mm/yyyy') AS FECHA_OPERACION,  "+
										" sol.ig_numero_linea_credito AS NUM_PROYECTO, "+
										" sol.ig_numero_contrato AS NUM_CONTRATO ,"+
										" '' AS FOLIO, " +
										" '' AS MODALIDAD " +	
										
										"   FROM com_documento doc,"   +
										"        com_docto_seleccionado sel,"   +
										"        com_solicitud sol,"   +
										"        comcat_if cif,"   +
										"        comrel_pyme_epo cpe,"   +
										"        comcat_pyme pym,"   +
										"        comcat_epo epo"   +
										"  WHERE doc.ic_documento = sel.ic_documento"   +
										"    AND sel.ic_documento = sol.ic_documento"   +
										"    AND sel.ic_linea_credito_emp is null"   +
										"    AND sol.ic_estatus_solic IN (3,  5,  6)"   +
										"    AND sel.ic_if = cif.ic_if"   +
										"    AND doc.ic_pyme = cpe.ic_pyme"   +
										"    AND doc.ic_epo = cpe.ic_epo"   +
										"    AND cpe.ic_pyme = pym.ic_pyme"   +
										"    AND cpe.ic_epo = epo.ic_epo"  ) ;
									
			if(!fechaOperacDe.equals("") && !fechaOperacA.equals("")  ){
				qrySentencia.append(" and sol.df_operacion >= trunc(TO_DATE(? ,'dd/mm/yyyy'))  "+
											" and sol.df_operacion < trunc(TO_DATE( ? ,'dd/mm/yyyy')+1) ");
				conditions.add(fechaOperacDe);
				conditions.add(fechaOperacA);
			}else{
				qrySentencia.append(" AND sol.df_operacion >= trunc(SYSDATE) " +
										 " AND sol.df_operacion < trunc(SYSDATE+1) ");
			}
			if(!ic_if.equals("") ){
				qrySentencia.append(" and sel.ic_if = ? ");
				conditions.add(ic_if);
			}
			qrySentencia.append(" order by MONTO_DESC DESC");
			
		} 
		/*else if("2".equals(ic_producto)) { // Financiamiento a Pedidos
		
			qrySentencia.append(
						" SELECT "+
						" cif.cg_razon_social AS INTERMEDIARIO,  " +
						" epo.cg_razon_social AS NOMBRE_EPO, " +
						" pym.cg_razon_social AS NOMBRE_CLIENTE,  " +
						" cpe.ic_pedido AS NUM_SOLICITUD, " +
						" pym.in_numero_sirac AS NUM_CLIENTE_SIRAC,  " +
						" sel.fn_saldo_total AS MONTO_DESC, " +
						" ant.ig_numero_prestamo AS NUM_PRESTAMO, " +
						" TO_CHAR (ant.df_operacion, 'dd/mm/yyyy') AS FECHA_OPERACION, " +
						" ant.ig_numero_linea_credito AS NUM_PROYECTO, " +
						" ant.ig_numero_contrato AS NUM_CONTRATO,  " +
						" '' AS FOLIO ," +		
						" '' AS MODALIDAD  " +	
						"   FROM com_pedido cpe,"   +
						"        com_pedido_seleccionado sel,"   +
						"        com_anticipo ant,"   +
						"        com_linea_credito clc,"   +
						"        comrel_pyme_epo rpe,"   +
						"        comcat_pyme pym,"   +
						"        comcat_epo epo,"   +
						"        comcat_if cif"   +
						"  WHERE cpe.ic_pedido = sel.ic_pedido"   +
						"    AND cpe.ic_pedido = ant.ic_pedido"   +
						"    AND ant.ic_estatus_antic IN (3,  5,  6)"   +
						"    AND sel.ic_linea_credito = clc.ic_linea_credito"   +
						"    AND cpe.ic_epo = rpe.ic_epo"   +
						"    AND cpe.ic_pyme = rpe.ic_pyme"   +
						"    AND rpe.ic_epo = epo.ic_epo"   +
						"    AND rpe.ic_pyme = pym.ic_pyme"   +
						"    AND clc.ic_if = cif.ic_if" ) ;
				
			if(!fechaOperacDe.equals("") && !fechaOperacA.equals("")  ){
				qrySentencia.append(" and ant.df_operacion >= trunc(TO_DATE( ?,'dd/mm/yyyy'))  "+
						   			 " and ant.df_operacion < trunc(TO_DATE( ? ,'dd/mm/yyyy')+1) ");
				conditions.add(fechaOperacDe);
				conditions.add(fechaOperacA);
			}else{
				qrySentencia.append(" AND ant.df_operacion >= trunc(SYSDATE) " +
										  " AND ant.df_operacion < trunc(SYSDATE+1) ");
			}
			if(!ic_if.equals("") ){
				qrySentencia.append(" and clc.ic_if = ? ");
				conditions.add(ic_if);
			}
					
			qrySentencia.append(" order by MONTO_DESC DESC");						
		} */
		
		else if("4".equals(ic_producto)) { //Financiamiento a Distribuidores
		
				qrySentencia.append(
						" select " +
						" i.cg_razon_social INTERMEDIARIO, " +
						" e.cg_razon_social NOMBRE_EPO, " +
						" p.cg_razon_social NOMBRE_CLIENTE,  " +
						" s.ic_documento NUM_SOLICITUD, " +
						" p.in_numero_sirac NUM_CLIENTE_SIRAC,  " +
						" sel.fn_importe_recibir MONTO_DESC, " +
						" s.ig_numero_prestamo NUM_PRESTAMO, " +
						"  TO_CHAR(s.df_operacion,'YYYY/MM/DD HH24:MI:SS') as FECHA_OPERACION , " +
						" s.ig_numero_linea_credito AS NUM_PROYECTO, " +
						" s.ig_numero_contrato AS NUM_CONTRATO, " +	
						" '' AS FOLIO, " +	
						" '' AS MODALIDAD " +							
						" from dis_solicitud s,"   +
						"      dis_documento d,"   +
						"      dis_docto_seleccionado sel,"   +
						" 	 dis_linea_credito_dm lc,"   +
						" 	 comcat_pyme p,"   +
						" 	 comcat_epo e,"   +
						" 	 comcat_if i"   +
						" where s.IC_DOCUMENTO=d.IC_DOCUMENTO"   +
						" and   d.IC_DOCUMENTO=SEL.IC_DOCUMENTO"   +
						" and   d.IC_LINEA_CREDITO_DM=lc.IC_LINEA_CREDITO_DM"   +
						" and   d.IC_PYME=p.IC_PYME"   +
						" and   d.IC_EPO=e.IC_EPO"   +
						" and d.ic_producto_nafin = 4 "+
						" and   lc.IC_IF=i.IC_IF	 "   +
						" and   s.IC_BLOQUEO=4"   +
						" and   s.IC_ESTATUS_SOLIc in (3,5,6)" );
					
					
					if(!fechaOperacDe.equals("") &&  !fechaOperacA.equals("") )  {
						qrySentencia.append(" and s.df_operacion >= trunc(TO_DATE( ? ,'dd/mm/yyyy'))  "+
									 " and s.df_operacion < trunc(TO_DATE( ? ,'dd/mm/yyyy')+1) ");
						conditions.add(fechaOperacDe);
						conditions.add(fechaOperacA);
					}else{
						qrySentencia.append(" AND s.df_operacion >= trunc(SYSDATE) " +
									 " AND s.df_operacion < trunc(SYSDATE+1) ");
					}
					
					if(!ic_if.equals("") ){
						qrySentencia.append(" and i.ic_if = ? ");
						qrySentencia.append(" and lc.ic_if= ? ");
						conditions.add(ic_if);
						conditions.add(ic_if);
					}
				
					qrySentencia.append(
						" UNION select " +
						" i.cg_razon_social INTERMEDIARIO, " +
					   " e.cg_razon_social NOMBRE_EPO, " +
						" p.cg_razon_social NOMBRE_CLIENTE,  " +
						"	s.ic_documento NUM_SOLICITUD, " +
						"	p.in_numero_sirac NUM_CLIENTE_SIRAC,  " +
						"	sel.fn_importe_recibir MONTO_DESC, " +
						"	s.ig_numero_prestamo NUM_PRESTAMO,  " +
						"	 TO_CHAR(s.df_operacion,'YYYY/MM/DD HH24:MI:SS')  AS   FECHA_OPERACION, " +
						"	s.ig_numero_linea_credito AS NUM_PROYECTO, " +
						"	s.ig_numero_contrato AS NUM_CONTRATO, " +
						" '' AS FOLIO,  " +	
						" '' AS MODALIDAD " +	
						" from dis_solicitud s,"   +
						"      dis_documento d,"   +
						"      dis_docto_seleccionado sel,"   +
						" 	 com_linea_credito lc,"   +
						" 	 comcat_pyme p,"   +
						" 	 comcat_epo e,"   +
						" 	 comcat_if i"   +
						" where s.IC_DOCUMENTO=d.IC_DOCUMENTO"   +
						" and   d.IC_DOCUMENTO=SEL.IC_DOCUMENTO"   +
						" and   d.IC_LINEA_CREDITO=lc.IC_LINEA_CREDITO"   +
						" and   d.IC_PYME=p.IC_PYME"   +
						" and   d.IC_EPO=e.IC_EPO"   +
						" and   d.ic_producto_nafin = 4 "+
						" and   lc.IC_IF=i.IC_IF	 "   +
						" and   s.IC_BLOQUEO=4"   +
						" and   s.IC_ESTATUS_SOLIc in (3,5,6)" );
						;
					if(!fechaOperacDe.equals("") &&  !fechaOperacA.equals("") )  {
						qrySentencia.append(" and s.df_operacion >= trunc(TO_DATE( ? ,'dd/mm/yyyy'))  "+
									 " and s.df_operacion < trunc(TO_DATE( ? ,'dd/mm/yyyy')+1) ");
						conditions.add(fechaOperacDe);
						conditions.add(fechaOperacA);
				
					}else{
						qrySentencia.append(" AND s.df_operacion >= trunc(SYSDATE) " +
									 " AND s.df_operacion < trunc(SYSDATE+1) ");
					}
					if(!ic_if.equals("") ){
						qrySentencia.append(" and  i.ic_if = ? ");
						qrySentencia.append(" and   lc.ic_if = ? ");
						conditions.add(ic_if);
						conditions.add(ic_if);						
					}
					qrySentencia.append(" order by MONTO_DESC DESC");	
					
		}
		
		/*
		 else if("5".equals(ic_producto)) {  //Credicadenas
		
			qrySentencia.append(" select "+
							" i.cg_razon_social INTERMEDIARIO,  "+
						  " e.cg_razon_social NOMBRE_EPO,  "+
							" p.cg_razon_social NOMBRE_CLIENTE,  "+
							" s.ig_numero_solic NUM_SOLICITUD, "+
							" p.in_numero_sirac NUM_CLIENTE_SIRAC,  "+
							" d.fn_monto_credito MONTO_DESC, "+
							" s.ig_numero_prestamo NUM_PRESTAMO, "+
							"  TO_CHAR(s.df_operacion,'YYYY/MM/DD HH24:MI:SS')  FECHA_OPERACION, "+
							" s.ig_numero_linea_credito AS NUM_PROYECTO, "+
							" s.ig_numero_contrato AS NUM_CONTRATO, "+		
							" '' AS FOLIO,  " +	
							" '' AS MODALIDAD " +	
						" from inv_solicitud s,"   +
						"      inv_disposicion d,"   +
						" 	 com_linea_credito lc,"   +
						" 	 comcat_pyme p,"   +
						" 	 comcat_epo e,"   +
						" 	 comcat_if i"   +
						" where s.CC_DISPOSICION=d.CC_DISPOSICION"   +
						" and   d.IC_LINEA_CREDITO=lc.IC_LINEA_CREDITO"   +
						" and   d.IC_PYME=p.IC_PYME"   +
						" and   d.IC_EPO=e.IC_EPO"   +
						" and   lc.IC_IF=i.IC_IF	 "   +
						" and   s.IC_BLOQUEO=4"   +
						" and   s.IC_ESTATUS_SOLIc in (3,5,6)" ) ;
						
			if(!fechaOperacDe.equals("")  && !fechaOperacA.equals("") ){
				qrySentencia.append(" and s.df_operacion >= trunc(TO_DATE(?,'dd/mm/yyyy'))  "+
										 " and s.df_operacion < trunc(TO_DATE(?,'dd/mm/yyyy')+1) ");
				conditions.add(fechaOperacDe);
				conditions.add(fechaOperacA);						 
			}else{
				qrySentencia.append(" AND s.df_operacion >= trunc(SYSDATE) " +
										  " AND s.df_operacion < trunc(SYSDATE+1) ");
			}
			if(!ic_if.equals("") ){
						qrySentencia.append(" and  i.ic_if = ? ");
						qrySentencia.append(" and   lc.ic_if = ? ");
						conditions.add(ic_if);
						conditions.add(ic_if);						
			}
			qrySentencia.append(" order by MONTO_DESC DESC");	
		
		} else if("8".equals(ic_producto)) {  //NAFIN Empresarial
		
				qrySentencia.append(" SELECT  "+
				
					" cif.cg_razon_social AS INTERMEDIARIO,  "+
				  " epo.cg_razon_social AS NOMBRE_EPO, "+
					" pym.cg_razon_social AS NOMBRE_CLIENTE,  "+
					" sol.ic_folio AS NUM_SOLICITUD, "+
					" pym.in_numero_sirac AS NUM_CLIENTE_SIRAC, "+
					" sel.in_importe_recibir AS MONTO_DESC, "+
					" sol.ig_numero_prestamo AS NUM_PRESTAMO, "+
					" TO_CHAR (sol.df_operacion, 'dd/mm/yyyy') AS FECHA_OPERACION, "+
					" sol.ig_numero_linea_credito AS NUM_PROYECTO, "+
					" sol.ig_numero_contrato AS NUM_CONTRATO, "+
					" 'Factoraje con Recurso' MODALIDAD,  "+		
					" '' AS FOLIO " +	
					
					"   FROM com_documento doc,"   +
					"        com_docto_seleccionado sel,"   +
					"        com_solicitud sol,"   +
					"        emp_linea_credito lc,"   +
					"        comcat_if cif,"   +
					"        comrel_pyme_epo cpe,"   +
					"        comcat_pyme pym,"   +
					"        comcat_epo epo"   +
					"  WHERE doc.ic_documento = sel.ic_documento"   +
					"    AND sel.ic_documento = sol.ic_documento"   +
					"    AND sel.ic_linea_credito_emp = lc.ic_linea_credito "   +
					"    AND sol.ic_estatus_solic IN (3,  5,  6)"   +
					"    AND sel.ic_if = cif.ic_if"   +
					"    AND doc.ic_pyme = cpe.ic_pyme"   +
					"    AND doc.ic_epo = cpe.ic_epo"   +
					"    AND cpe.ic_pyme = pym.ic_pyme"   +
					"    AND cpe.ic_epo = epo.ic_epo" )  ;

				if(!fechaOperacDe.equals("") && !fechaOperacA.equals("") ) {
					qrySentencia.append(" AND sol.df_operacion >= TO_DATE (? , 'dd/mm/yyyy') "+
						" AND sol.df_operacion < TO_DATE (? , 'dd/mm/yyyy') + 1 ");
					conditions.add(fechaOperacDe);
					conditions.add(fechaOperacA);	
				} else {
					qrySentencia.append(" AND sol.df_operacion >= TRUNC(SYSDATE) "+
											" AND sol.df_operacion < TRUNC(SYSDATE+1) ");
				}
				
				if(!ic_if.equals("") ){
					qrySentencia.append(" and  sel.ic_if = ? ");
					conditions.add(ic_if);						
				}
			
				//FINANCIAMIENTO A CONTRATOS
				qrySentencia.append(	" UNION "   +
					" SELECT  "+
					" cif.cg_razon_social AS INTERMEDIARIO, " +
					" epo.cg_razon_social AS NOMBRE_EPO, " +
					" pym.cg_razon_social AS NOMBRE_CLIENTE, " +
					" TO_CHAR (cpe.ic_pedido) AS NUM_SOLICITUD, " +
					" pym.in_numero_sirac AS NUM_CLIENTE_SIRAC,  " +
					" sel.fn_saldo_total AS MONTO_DESC, " +
					" ant.ig_numero_prestamo AS NUM_PRESTAMO, " +
					" TO_CHAR (ant.df_operacion, 'dd/mm/yyyy') AS FECHA_OPERACION, " +
					" ant.ig_numero_linea_credito AS NUM_PROYECTO, " +
				   "  ant.ig_numero_contrato AS NUM_CONTRATO, " +
					" 'Financiamiento a Contratos' MODALIDAD,   " +
					" '' AS FOLIO " +	
					"   FROM com_pedido cpe,"   +
					"        com_pedido_seleccionado sel,"   +
					"        com_anticipo ant,"   +
					"        emp_linea_credito clc,"   +
					"        comrel_pyme_epo rpe,"   +
					"        comcat_pyme pym,"   +
					"        comcat_epo epo,"   +
					"        comcat_if cif"   +
					"  WHERE cpe.ic_pedido = sel.ic_pedido"   +
					"    AND cpe.ic_pedido = ant.ic_pedido"   +
					"    AND ant.ic_estatus_antic IN (3,  5,  6)"   +
					"    AND sel.ic_linea_credito_emp = clc.ic_linea_credito"   +
					"    AND cpe.ic_epo = rpe.ic_epo"   +
					"    AND cpe.ic_pyme = rpe.ic_pyme"   +
					"    AND rpe.ic_epo = epo.ic_epo"   +
					"    AND rpe.ic_pyme = pym.ic_pyme"   +
					"    AND clc.ic_if = cif.ic_if " ) ;

				if(!fechaOperacDe.equals("") && !fechaOperacA.equals("") ) {
					qrySentencia.append(	" AND ant.df_operacion >= TO_DATE (? , 'dd/mm/yyyy') "+
												" AND ant.df_operacion < TO_DATE ( ? , 'dd/mm/yyyy') + 1 ");
					conditions.add(fechaOperacDe);
					conditions.add(fechaOperacA);
				} else {
					qrySentencia.append(	" AND ant.df_operacion >= TRUNC(SYSDATE) "+
												" AND ant.df_operacion < TRUNC(SYSDATE+1) ");
				}
				if(!ic_if.equals("") ){
					qrySentencia.append(" and  clc.ic_if = ? ");
					conditions.add(ic_if);						
				}
				
			
				//LIBRE DISPOSICION - FACTORAJE CON RECURSO
				qrySentencia.append(" UNION "   +
					" select "   +
					" i.cg_razon_social INTERMEDIARIO,   " +
					" e.cg_razon_social NOMBRE_EPO,  " +
					" p.cg_razon_social NOMBRE_CLIENTE,   " +
					" TO_CHAR (s.ig_numero_solic) NUM_SOLICITUD,  " +
					" p.in_numero_sirac NUM_CLIENTE_SIRAC,   " +
					" d.fn_monto_credito MONTO_DESC,  " +
					" s.ig_numero_prestamo NUM_PRESTAMO,  " +
					" TO_CHAR (s.df_operacion, 'dd/mm/yyyy') FECHA_OPERACION,  " +
					" s.ig_numero_linea_credito AS NUM_PROYECTO,  " +
					" s.ig_numero_contrato AS NUM_CONTRATO,  " +
					" DECODE (d.cg_exp_emp, 'S', 'Financiamiento de Exportacion',  'Libre Disposicion') MODALIDAD , " +
					" '' AS FOLIO " +		
					" from inv_solicitud s,"   +
					"      inv_disposicion d,"   +
					" 	 emp_linea_credito lc,"   +
					" 	 comcat_pyme p,"   +
					" 	 comcat_epo e,"   +
					" 	 comcat_if i"   +
					" where s.CC_DISPOSICION=d.CC_DISPOSICION"   +
					" and   d.IC_LINEA_CREDITO_emp=lc.IC_LINEA_CREDITO"   +
					" and   d.IC_PYME=p.IC_PYME"   +
					" and   d.IC_EPO=e.IC_EPO"   +
					" and   lc.IC_IF=i.IC_IF	 "   +
					" and   s.IC_BLOQUEO=4"   +
					" and   s.IC_ESTATUS_SOLIc in (3,5,6)" ) ;

				if(!fechaOperacDe.equals("")  &&  !fechaOperacA.equals("") ) {
					qrySentencia.append(" AND s.df_operacion >= TO_DATE ( ? , 'dd/mm/yyyy') "+
						" AND s.df_operacion < TO_DATE ( ? , 'dd/mm/yyyy') + 1 ");
						conditions.add(fechaOperacDe);	
						conditions.add(fechaOperacA);	
				} else {
					qrySentencia.append(" AND s.df_operacion >= TRUNC(SYSDATE) "+
												" AND s.df_operacion < TRUNC(SYSDATE+1) ");
				}
				
				if(!ic_if.equals("") ){
					qrySentencia.append(" AND i.ic_if = ? ");
					qrySentencia.append(" AND lc.ic_if  = ? ");
					conditions.add(ic_if);						
					conditions.add(ic_if);											
				}
				
				qrySentencia.append(" order by MONTO_DESC DESC");  
		}
		
		*/
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int total =0;
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
            (String) session.getAttribute("strNombre"),
            (String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
 
			if("0".equals(ic_producto)  && tipo.equals("PDF")){
				pdfDoc.setTable(9,100);
				pdfDoc.setCell("Folio", "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Cliente ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Num. de Cliente SIRAC", "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto descuento ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Pr�stamo ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Proyecto ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Contrato ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Operaci�n ","celda02",ComunesPDF.CENTER);
			
			}else if("0".equals(ic_producto)  && tipo.equals("CSV")){
				contenidoArchivo.append("Folio ,Intermediario, Nombre Cliente ,Num. de Cliente SIRAC, Monto descuento, N�mero de Pr�stamo ,N�mero de Proyecto, N�mero de Contrato ,Fecha de Operaci�n  \n");
			}
			
			if( ("1".equals(ic_producto)  || "4".equals(ic_producto)  /*|| "2".equals(ic_producto)  || "5".equals(ic_producto)*/  )  && tipo.equals("PDF")){
				pdfDoc.setTable(10,100);
				pdfDoc.setCell("Intermediario ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO ","celda02",ComunesPDF.CENTER);				
				pdfDoc.setCell("Nombre Cliente ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de solicitud ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Num. de Cliente SIRAC", "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto descuento ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Pr�stamo ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Proyecto ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Contrato ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Operaci�n ","celda02",ComunesPDF.CENTER);
			
			}else if( ("1".equals(ic_producto) || "4".equals(ic_producto) /* || "2".equals(ic_producto)  || "5".equals(ic_producto)*/  )   && tipo.equals("CSV")){
			
				contenidoArchivo.append("Intermediario, EPO,  Nombre Cliente , N�mero de solicitud, Num. de Cliente SIRAC, Monto descuento, N�mero de Pr�stamo ,N�mero de Proyecto, N�mero de Contrato ,Fecha de Operaci�n  \n");
			}
			/*
			if(  "8".equals(ic_producto)  && tipo.equals("PDF")){
				pdfDoc.setTable(11,100);
				pdfDoc.setCell("Modalidad ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO ","celda02",ComunesPDF.CENTER);				
				pdfDoc.setCell("Nombre Cliente ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de solicitud ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Num. de Cliente SIRAC", "celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto descuento ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Pr�stamo ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Proyecto ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Contrato ","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Operaci�n ","celda02",ComunesPDF.CENTER);
			
			}else if( "8".equals(ic_producto)  && tipo.equals("CSV")){
				contenidoArchivo.append("Modalidad, Intermediario, EPO,  Nombre Cliente , N�mero de solicitud, Num. de Cliente SIRAC, Monto descuento, N�mero de Pr�stamo ,N�mero de Proyecto, N�mero de Contrato ,Fecha de Operaci�n  \n");
			}
			*/
			
		
			while (rs.next())	{
				String folio = (rs.getString("FOLIO") == null) ? "" : rs.getString("FOLIO");
				String intermediario = (rs.getString("INTERMEDIARIO") == null) ? "" : rs.getString("INTERMEDIARIO");
				String nombre_cliente = (rs.getString("NOMBRE_CLIENTE") == null) ? "" : rs.getString("NOMBRE_CLIENTE");
				String num_cliente = (rs.getString("NUM_CLIENTE_SIRAC") == null) ? "" : rs.getString("NUM_CLIENTE_SIRAC");
				String monto_desc = (rs.getString("MONTO_DESC") == null) ? "" : rs.getString("MONTO_DESC");
				String num_prestamo = (rs.getString("NUM_PRESTAMO") == null) ? "" : rs.getString("NUM_PRESTAMO");
				String num_proyecto = (rs.getString("NUM_PROYECTO") == null) ? "" : rs.getString("NUM_PROYECTO");
				String num_contrato = (rs.getString("NUM_CONTRATO") == null) ? "" : rs.getString("NUM_CONTRATO");
				String fecha_operacion = (rs.getString("FECHA_OPERACION") == null) ? "" : rs.getString("FECHA_OPERACION");
				String num_solicitud = (rs.getString("NUM_SOLICITUD") == null) ? "" : rs.getString("NUM_SOLICITUD");
				String nombre_epo = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
				String modalidad = (rs.getString("MODALIDAD") == null) ? "" : rs.getString("MODALIDAD");
				
				total++;
				
				if("0".equals(ic_producto)  && tipo.equals("PDF")){
					pdfDoc.setCell(folio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(intermediario,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(nombre_cliente,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(num_cliente,"formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(num_prestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_proyecto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_operacion,"formas",ComunesPDF.CENTER);	
					
				}else if("0".equals(ic_producto)  && tipo.equals("CSV")){
					contenidoArchivo.append(folio.replaceAll(",", "")+","+
													intermediario.replaceAll(",", "")+","+
													nombre_cliente.replaceAll(",", "")+","+
													num_cliente.replaceAll(",", "")+","+					
													monto_desc.replaceAll(",", "")+","+
													num_prestamo.replaceAll(",", "")+","+
													num_proyecto.replaceAll(",", "")+","+
													num_contrato.replaceAll(",", "")+","+
													fecha_operacion.replaceAll(",", "")+"\n");
				}
				
				
				if(("1".equals(ic_producto)  || "4".equals(ic_producto) /* || "2".equals(ic_producto) || "5".equals(ic_producto) */ ) && tipo.equals("PDF")){
					pdfDoc.setCell(intermediario,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(nombre_cliente,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(num_solicitud,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_cliente,"formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(num_prestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_proyecto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_operacion,"formas",ComunesPDF.CENTER);	
					
				}else if( ("1".equals(ic_producto)  || "4".equals(ic_producto) /*|| "2".equals(ic_producto)  || "5".equals(ic_producto) */  ) && tipo.equals("CSV")){
					contenidoArchivo.append(
													intermediario.replaceAll(",", "")+","+
													nombre_epo.replaceAll(",", "")+","+
													nombre_cliente.replaceAll(",", "")+","+
													num_solicitud.replaceAll(",", "")+","+
													num_cliente.replaceAll(",", "")+","+					
													monto_desc.replaceAll(",", "")+","+
													num_prestamo.replaceAll(",", "")+","+
													num_proyecto.replaceAll(",", "")+","+
													num_contrato.replaceAll(",", "")+","+
													fecha_operacion.replaceAll(",", "")+"\n");
				}
				
				/*
				 if(  "8".equals(ic_producto) && tipo.equals("PDF") ){
					pdfDoc.setCell(modalidad,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(intermediario,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(nombre_epo,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(nombre_cliente,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(num_solicitud,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_cliente,"formas",ComunesPDF.CENTER);					
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(num_prestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_proyecto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_operacion,"formas",ComunesPDF.CENTER);	
					
				}else if(  "8".equals(ic_producto)  && tipo.equals("CSV")){
					contenidoArchivo.append(
													modalidad.replaceAll(",", "")+","+
													intermediario.replaceAll(",", "")+","+
													nombre_epo.replaceAll(",", "")+","+
													nombre_cliente.replaceAll(",", "")+","+
													num_solicitud.replaceAll(",", "")+","+
													num_cliente.replaceAll(",", "")+","+					
													monto_desc.replaceAll(",", "")+","+
													num_prestamo.replaceAll(",", "")+","+
													num_proyecto.replaceAll(",", "")+","+
													num_contrato.replaceAll(",", "")+","+
													fecha_operacion.replaceAll(",", "")+"\n");				
				}	
				*/
				
			}
			
			if(tipo.equals("PDF")){
				pdfDoc.setCell("Total Solicitudes Concluidas ","celda02",ComunesPDF.LEFT, 2);	
				if("0".equals(ic_producto)) pdfDoc.setCell(String.valueOf(total),"celda02",ComunesPDF.LEFT,7);	
				if("1".equals(ic_producto)  || "4".equals(ic_producto) /*|| "2".equals(ic_producto) || "5".equals(ic_producto)*/ ) pdfDoc.setCell(String.valueOf(total),"celda02",ComunesPDF.LEFT,8);	
			//	if( "8".equals(ic_producto) ) pdfDoc.setCell(String.valueOf(total),"celda02",ComunesPDF.LEFT,9);	
			
			}
			
			if(tipo.equals("PDF")){
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}
			if(tipo.equals("CSV")){
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	public String impVerPrestamoo(  HttpServletRequest request, String path , String ic_producto, String claveSolicitud, String  interfase, String logAdicional )  {
		log.debug("impDescargaArchivo (E)");		
		
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		boolean transactionOk = true;
		CreaArchivo creaArchivo = new CreaArchivo();
		String nombreArchivo = "", contenidoArchivo ="",  query = "", srtSelect = "SELECT ", strFrom = " FROM ", strWhere = " WHERE ";
				
		try {
			
			con.conexionDB();
			
			if ("todos".equals(interfase)) {
				srtSelect += "cg_proyectos_log, cg_contratos_log, cg_prestamos_log, cg_disposiciones_log ";
			} else {
				if ("".equals(logAdicional)) {
					srtSelect += "cg_"+ interfase +"_log";
				} else {
					srtSelect += "cg_"+ logAdicional +"_log";
				}
			}
			if ("0".equals(ic_producto)){
				strFrom += "com_interfase ";
				strWhere += "ic_solic_portal = ?";
			} 	else if("1".equals(ic_producto)) {
				strFrom += ("todos".equals(interfase) || "disposiciones".equals(interfase))? "com_control04 " : ("proyectos".equals(interfase))?"com_control01 ":("contratos".equals(interfase))?"com_control02 ": "com_control03 ";
				strWhere += "ic_folio = ?";			
			/*} else if("2".equals(ic_producto)) {
				strFrom += ("todos".equals(interfase) || "disposiciones".equals(interfase))? "com_controlant04 " : ("proyectos".equals(interfase))?"com_controlant01 ":("contratos".equals(interfase))?"com_controlant02 ": "com_controlant03 ";
				strWhere += "ic_pedido = ?";
				*/
			} else if("4".equals(ic_producto)) {
				strFrom += "dis_interfase ";
				strWhere += "ic_documento = ?";
			} 
			/*else if("5".equals(ic_producto)) {
				strFrom += "inv_interfase ";
				strWhere += "cc_disposicion = ?";
			} else if("8".equals(ic_producto)) {
				strFrom += "com_control04 ";
				strWhere += "ic_folio = ?";
			}
			*/
			query = srtSelect + strFrom + strWhere;
			ps = con.queryPrecompilado(query);
			ps.setString(1, claveSolicitud);
			rs = ps.executeQuery();
			if (rs.next()) {
				if ("todos".equals(interfase)) {
					contenidoArchivo += (rs.getString("cg_proyectos_log")==null)?"****NO EXISTE LOG DE PROYECTOS****\n":"****PROYECTOS****\n" + rs.getString("cg_proyectos_log") + "\n";
					contenidoArchivo += (rs.getString("cg_contratos_log")==null)?"****NO EXISTE LOG DE CONTRATOS****\n":"****CONTRATOS****\n" + rs.getString("cg_contratos_log") + "\n";
					contenidoArchivo += (rs.getString("cg_prestamos_log")==null)?"****NO EXISTE LOG DE PRESTAMOS****\n":"****PRESTAMOS****\n" + rs.getString("cg_prestamos_log") + "\n";
					contenidoArchivo += (rs.getString("cg_disposiciones_log")==null)?"****NO EXISTE LOG DE DISPOSICIONES****\n":"****DISPOSICIONES****\n" + rs.getString("cg_disposiciones_log");
		
				} else {
					if ("".equals(logAdicional)) {
						contenidoArchivo += (rs.getString("cg_"+ interfase +"_log")==null)?"****NO EXISTE LOG DE " + interfase.toUpperCase() + "****\n":rs.getString("cg_"+ interfase +"_log");
					} else {
						contenidoArchivo += (rs.getString("cg_"+ logAdicional +"_log")==null)?"****NO EXISTE LOG DE " + logAdicional.toUpperCase() + "****\n":rs.getString("cg_"+ logAdicional +"_log");
					}
				}
			}
			rs.close();
			ps.close();
	
			creaArchivo.make(contenidoArchivo.toString(), path, ".txt");
			nombreArchivo = creaArchivo.getNombre();
	
		} catch(Throwable e) {
			transactionOk=false;	
			e.printStackTrace();
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {				
				
			} catch(Exception e) {}
				if(con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();	
			}  
		}
		log.debug("impDescargaArchivo (S)");
		return nombreArchivo;
	}
	
	
	/**
	 *  metodo para Formar el combo de IF
	 * @return 
	 */
	public List  getCatalogoIF( ){
		log.info("getCatalogoIF (E) ");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
			
			String SQL =" SELECT ic_if, cg_razon_social" +
					"   FROM comcat_if"   +
					"  WHERE cs_habilitado = 'S' order by 2"  ;
	
			rs = con.queryDB(SQL);			
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString(1) );
				datos.put("descripcion", rs.getString(2));
				registros.add(datos);
			}
			rs.close();
			con.cierraStatement();
		
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getCatalogoIF (S) ");
		return registros;
	}
	
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_producto() {
		return ic_producto;
	}

	public void setIc_producto(String ic_producto) {
		this.ic_producto = ic_producto;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getFechaOperacDe() {
		return fechaOperacDe;
	}

	public void setFechaOperacDe(String fechaOperacDe) {
		this.fechaOperacDe = fechaOperacDe;
	}

	public String getFechaOperacA() {
		return fechaOperacA;
	}

	public void setFechaOperacA(String fechaOperacA) {
		this.fechaOperacA = fechaOperacA;
	}




	

}