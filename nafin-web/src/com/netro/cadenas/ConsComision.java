package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Iva;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsComision implements IQueryGeneratorRegExtJS  {	
	
	private int numList = 1;
	private String ic_producto_nafin;
	private String ic_mes_calculo_de;
	private String ic_mes_calculo_a;
	private String ic_anio_calculo;
	private String cg_estatus_pagado;
	private String ic_moneda;
	private String ic_banco_fondeo;
	private String ic_if;
	private String periodo;
	public static final boolean SIN_COMAS = false;
		
	public ConsComision() {}

	// Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ConsComision.class);
  
	public  StringBuffer strQuery;
	private List conditions = new ArrayList(); 
	
	public String getAggregateCalculationQuery() { // Obtiene los totales 
		return "";
	}  
		 
	public String getDocumentQuery(){  // para las llaves primarias		
		return "";
	}  
	  		
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 			
		return "";
	}  
	
	public String getDocumentQueryFile(){ // para todos los registros
		conditions = new ArrayList();   
		StringBuffer strQuery = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		log.info("getDocumentQueryFile(E)");
		
		if(this.periodo.equals("Mensual")){ // MENSUAL
			if (!ic_mes_calculo_de.equals("") && !ic_mes_calculo_a.equals("")) {
				condicion.append(" AND cfd.ic_mes_calculo BETWEEN " + 
						ic_mes_calculo_de + " AND " + ic_mes_calculo_a + " ");
			}
			if (!ic_producto_nafin.equals("")) {
				condicion.append(" AND cfd.ic_producto_nafin = " + ic_producto_nafin + " ");
			}
			if (!cg_estatus_pagado.equals("")) {
				condicion.append(" AND pcf.cg_estatus_pagado = '" + cg_estatus_pagado + "' ");
			}
			
			String restriccionBanco = "";
			if(ic_banco_fondeo.equals("1") || ic_banco_fondeo.equals("2")){
				restriccionBanco = "	AND cfd.ic_banco_fondeo = " + ic_banco_fondeo; //Banco de fondeo
			}			
			
		 //FODEA 001 Tasa IVA 2010 
		  String fecha   = "01/"+ ic_mes_calculo_a + "/" + ic_anio_calculo; // formato fecha: DD/MM/YYYY
		  String porcentajeIva   = Iva.getPorcentaje(fecha);
		  double valorIva = ((BigDecimal)Iva.getValor(fecha)).doubleValue();
	
			strQuery.append(
					" SELECT cfd.ic_producto_nafin, pn.ic_nombre,  " +
					" 	cfd.ic_mes_calculo, pcf.cg_estatus_pagado, " +
					" 	i.cg_razon_social as NombreIF, " +
					" 	SUM(cfd.fg_saldo_promedio) as saldoPromedio,  " +
					" 	SUM(cfd.fg_contraprestacion) as contraprestacion, " +
					" 	SUM(cfd.fg_contraprestacion) * " + String.valueOf(valorIva) +" AS montoIva, " +
					" 	SUM(cfd.fg_contraprestacion) * " + String.valueOf(1 + valorIva) + " AS contraprestacionTotal " +
					" FROM com_comision_fondeo_det cfd, comcat_producto_nafin  pn, " +
					" 	com_pago_comision_fondeo pcf, comcat_if i " +
					" WHERE cfd.ic_producto_nafin = pn.ic_producto_nafin " +
					" 	AND cfd.ic_if = i.ic_if " +
					" 	AND cfd.ic_producto_nafin = pcf.ic_producto_nafin " +
					" 	AND cfd.ic_if = pcf.ic_if " +
					" 	AND cfd.ic_mes_calculo = pcf.ic_mes_calculo " +
					" 	AND cfd.ic_anio_calculo = pcf.ic_anio_calculo " +
					" 	AND cfd.ic_anio_calculo = " + ic_anio_calculo +
					" 	AND cfd.ic_if = " + ic_if +
					"	AND cfd.ic_moneda = " + ic_moneda + " " +
					"	AND cfd.ic_moneda = pcf.ic_moneda " +
					"	AND cfd.ic_banco_fondeo = pcf.ic_banco_fondeo " +
					condicion + " " +
					restriccionBanco + " " +
					" GROUP BY cfd.ic_producto_nafin, pn.ic_nombre, i.cg_razon_social, " +
					" 	cfd.ic_mes_calculo, pcf.cg_estatus_pagado " +
					" ORDER BY cfd.ic_producto_nafin, ic_mes_calculo ");
		}else if(this.periodo.equals("Anual")){ // ANUAL
			if (!ic_producto_nafin.equals("")) {
				condicion.append(" AND cfd.ic_producto_nafin = " + ic_producto_nafin + " ");
			}
			if (!cg_estatus_pagado.equals("")) {
				condicion.append(" AND paaf.cg_estatus_pagado = '" + cg_estatus_pagado + "' ");
			}
			
			String restriccionBanco = "";
			if(ic_banco_fondeo.equals("1") || ic_banco_fondeo.equals("2")){
				restriccionBanco = "	AND cfd.ic_banco_fondeo = " + ic_banco_fondeo; //Banco de fondeo
			}
			
				 //FODEA 001 Tasa IVA 2010 
			  String mes1 = "12";
			  String fecha1   = "01/"+ mes1 + "/" + ic_anio_calculo; // formato fecha: DD/MM/YYYY
			  String porcentajeIva   = Iva.getPorcentaje(fecha1);
			  double valorIva = ((BigDecimal)Iva.getValor(fecha1)).doubleValue();
		 
				strQuery.append(" SELECT i.cg_razon_social as nombreIF, pn.ic_producto_nafin, pn.ic_nombre,  " +
						" 	paaf.fg_saldo_referencia, paaf.fg_porcentaje_ajustado,  " +
						" 	paaf.cg_estatus_pagado, " +
						" 	SUM(cfd.fg_saldo_promedio) as saldoPromedio, " +
						" 	SUM(cfd.fg_contraprestacion_ajustada) as contraprestacionAjustada, " +
						" 	SUM(cfd.fg_contraprestacion_ajustada) * " + String.valueOf(1 + valorIva) + " as contraprestacionTotalAjustada, " +
						" 	SUM(cfd.fg_contraprestacion_ajustada - cfd.fg_contraprestacion) * " + String.valueOf(1 + valorIva) + " as ajuste" +
						" FROM com_comision_fondeo_det cfd, com_pago_ajuste_anual_fondeo paaf, comcat_if i, comcat_producto_nafin pn " +
						" WHERE cfd.ic_if = paaf.ic_if " +
						" 	AND cfd.ic_producto_nafin = pn.ic_producto_nafin " +
						" 	AND cfd.ic_anio_calculo = paaf.ic_anio_calculo " +
						"	AND cfd.ic_moneda = paaf.ic_moneda " +
						"	AND cfd.ic_banco_fondeo = paaf.ic_banco_fondeo " +
						" 	AND cfd.ic_if = i.ic_if " +
						" 	AND cfd.ic_anio_calculo = " + ic_anio_calculo +
						" 	AND cfd.ic_if = " + ic_if +
						"	AND cfd.ic_moneda = " + ic_moneda + " " +
						condicion + " " +
						restriccionBanco + " " +
						" GROUP BY i.cg_razon_social, pn.ic_producto_nafin, pn.ic_nombre, " +
						" 	paaf.fg_saldo_referencia, paaf.fg_porcentaje_ajustado, paaf.cg_estatus_pagado ");
					
		}
		return strQuery.toString();		
	} 
	
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		//String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fecha   = "01/"+ ic_mes_calculo_a + "/" + ic_anio_calculo; // formato fecha: DD/MM/YYYY

		if(periodo.equals("Mensual")){
			if ("XLS".equals(tipo)) {
				try {
					HttpSession session = request.getSession();
					int registros = 0;
					String nomCompara = ".-.)#";
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					
					contenidoArchivo.append((String)session.getAttribute("strNombre") + "\n");
					
					while(rs.next()){
						String ic_mes_calculo 		= rs.getString("ic_mes_calculo")==null?"":rs.getString("ic_mes_calculo");
						String saldopromedio 		= rs.getString("saldopromedio")==null?"":rs.getString("saldopromedio");
						String contraprestacion 	= rs.getString("contraprestacion")==null?"":rs.getString("contraprestacion");
						String montoiva 				= rs.getString("montoiva")==null?"":rs.getString("montoiva");
						String contraprestaciontotal= rs.getString("contraprestaciontotal")==null?"":rs.getString("contraprestaciontotal");
						String cg_estatus_pagado 	= rs.getString("cg_estatus_pagado")==null?"":rs.getString("cg_estatus_pagado");
						String ic_producto_nafin 	= rs.getString("ic_producto_nafin")==null?"":rs.getString("ic_producto_nafin");
						String ic_nombre 				= rs.getString("ic_nombre")==null?"":rs.getString("ic_nombre");
						
						if(!nomCompara.equals(ic_nombre))	{
							contenidoArchivo.append("\nPRODUCTO: " + ic_nombre);
							contenidoArchivo.append("\nMes, A�o, Saldo Promedio, Contraprestacion, % IVA, Monto IVA, Total, Pagado");
						}
						    
						registros++;
						
						contenidoArchivo.append("\n"+
							meses[Integer.parseInt(ic_mes_calculo.replaceAll(" ",""))-1]+","+
							this.ic_anio_calculo+","+
							"$ " + Comunes.formatoDecimal(saldopromedio,2,SIN_COMAS)+","+
							"$ " + Comunes.formatoDecimal(contraprestacion,2,SIN_COMAS)+","+
							Iva.getPorcentaje(fecha)+" %,"+
							"$ " + Comunes.formatoDecimal(montoiva,2,SIN_COMAS)+","+
							"$ " + Comunes.formatoDecimal(contraprestaciontotal,2,SIN_COMAS)+","+
							cg_estatus_pagado);
							
							nomCompara = ic_nombre;
					}
					
					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}	
				} catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}else if ("PDF".equals(tipo)) {
				try {
					HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
							
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					
					pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
					pdfDoc.addText((String)session.getAttribute("strNombre"),"formas",ComunesPDF.CENTER);
					
					boolean v = true;
					double totalSaldoProm=0;
					double totalContraprestacion=0;
					double totalTotal=0;
					String nomCompara = "";
					int x = 0;
													
					while (rs.next()) {
						String ic_mes_calculo 		= rs.getString("ic_mes_calculo")==null?"":rs.getString("ic_mes_calculo");
						String saldopromedio 		= rs.getString("saldopromedio")==null?"":rs.getString("saldopromedio");
						String contraprestacion 	= rs.getString("contraprestacion")==null?"":rs.getString("contraprestacion");
						String montoiva 				= rs.getString("montoiva")==null?"":rs.getString("montoiva");
						String contraprestaciontotal= rs.getString("contraprestaciontotal")==null?"":rs.getString("contraprestaciontotal");
						String cg_estatus_pagado 	= rs.getString("cg_estatus_pagado")==null?"":rs.getString("cg_estatus_pagado");
						String ic_producto_nafin 	= rs.getString("ic_producto_nafin")==null?"":rs.getString("ic_producto_nafin");
						String ic_nombre 				= rs.getString("ic_nombre")==null?"":rs.getString("ic_nombre").trim();
						
						totalSaldoProm += new Double(saldopromedio).doubleValue();
						totalContraprestacion += new Double(contraprestacion).doubleValue();
						totalTotal += new Double(contraprestaciontotal).doubleValue();
								 
						if(!nomCompara.equals(ic_nombre) && x>0) 
							pdfDoc.addTable();
							
						if(!nomCompara.equals(ic_nombre)){  						
							pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
							pdfDoc.addText("PRODUCTO: " + ic_nombre,"formas",ComunesPDF.CENTER);
							pdfDoc.setTable(8, 80);
							pdfDoc.setCell("Mes","celda01",ComunesPDF.LEFT);
							pdfDoc.setCell("A�o","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Saldo Promedio","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Contraprestacion","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("% IVA","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto IVA","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Total","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Pagado","celda01",ComunesPDF.CENTER);
						}
							
						pdfDoc.setCell(meses[Integer.parseInt(ic_mes_calculo.replaceAll(" ",""))-1],"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(this.ic_anio_calculo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(saldopromedio,2,SIN_COMAS),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(contraprestacion,2,SIN_COMAS),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Iva.getPorcentaje(fecha) + " %","formas",ComunesPDF.CENTER);		
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montoiva,2,SIN_COMAS),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(contraprestaciontotal,2,SIN_COMAS),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cg_estatus_pagado,"formas",ComunesPDF.CENTER);		
						
						nomCompara = ic_nombre;				
						x++; 
						
					}
						pdfDoc.addTable();	
						
						pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
						pdfDoc.addText("Gran Total M.N.","celda01",ComunesPDF.CENTER);
						pdfDoc.setTable(4, 60);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Saldo Promedio","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Contraprestacion","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("TOTAL","celda01",ComunesPDF.CENTER);
						
						pdfDoc.setCell("Gran Total M.N.","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(Double.toString(totalSaldoProm).toString(),2,SIN_COMAS),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(Double.toString(totalContraprestacion).toString(),2,SIN_COMAS),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(Double.toString(totalTotal).toString(),2,SIN_COMAS),"formas",ComunesPDF.CENTER);
						
						pdfDoc.addTable();	
					pdfDoc.endDocument();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}
		}
		/********************* ****************************/
		else if(periodo.equals("Anual")){
			if ("XLS".equals(tipo)) {
					try {
						HttpSession session = request.getSession();
						int registros = 0;
						int cont = 0;	
						String nomCompara = "-.-)&#";
						contenidoArchivo = new StringBuffer();
						
						contenidoArchivo.append((String)session.getAttribute("strNombre") + "\n");
						
						while(rs.next()){
							String saldopromedio 		= rs.getString("saldopromedio")==null?"":rs.getString("saldopromedio");
							String saldoreferencia 		= rs.getString("fg_saldo_referencia")==null?"":rs.getString("fg_saldo_referencia");
							String fg_porcentaje_ajustado= rs.getString("fg_porcentaje_ajustado")==null?"":rs.getString("fg_porcentaje_ajustado");
							String contraprestacionajustada= rs.getString("contraprestacionajustada")==null?"":rs.getString("contraprestacionajustada");
							String contraprestaciontotalajustada= rs.getString("contraprestaciontotalajustada")==null?"":rs.getString("contraprestaciontotalajustada");
							String ajuste 	= rs.getString("ajuste")==null?"":rs.getString("ajuste");
							String cg_estatus_pagado 	= rs.getString("cg_estatus_pagado")==null?"":rs.getString("cg_estatus_pagado");
							String ic_nombre 					= rs.getString("ic_nombre")==null?"":rs.getString("ic_nombre");
							
							if(!nomCompara.equals(ic_nombre)){
								contenidoArchivo.append("\nPRODUCTO: " + ic_nombre);
								contenidoArchivo.append("\nA�o, Saldo Promedio, Saldo de Referencia, Tasa Ajustada, Contraprestacion Ajustada, % IVA, Contraprestacion Total Ajustada, Ajuste, Pagado");
							}
							
							registros++;  
							contenidoArchivo.append("\n"+
								this.ic_anio_calculo+","+
								"$ " + Comunes.formatoDecimal(saldopromedio,2,SIN_COMAS)+","+
								"$ " + Comunes.formatoDecimal(saldoreferencia,2,SIN_COMAS)+","+
								"$ " + Comunes.formatoDecimal(fg_porcentaje_ajustado,2,SIN_COMAS)+","+
								"$ " + Comunes.formatoDecimal(contraprestacionajustada,2,SIN_COMAS)+","+
								Iva.getPorcentaje(fecha)+" %,"+
								"$ " + Comunes.formatoDecimal(contraprestaciontotalajustada,2,SIN_COMAS)+","+
								"$ " + Comunes.formatoDecimal(ajuste,2,SIN_COMAS)+","+
								cg_estatus_pagado);
								
								nomCompara = ic_nombre;
						}
						
						if(registros >= 1){
							if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
								nombreArchivo="ERROR";
							}else{
								nombreArchivo = archivo.nombre;
							}
						}	
					} catch (Throwable e) {
							throw new AppException("Error al generar el archivo", e);
					} finally {
						try {
							rs.close();
						} catch(Exception e) {}
					}
				}else if ("PDF".equals(tipo)) {
					try {
						HttpSession session = request.getSession();
						nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
						ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
						ComunesPDF pdfDoc2 = new ComunesPDF(2, path + nombreArchivo);
						
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
								
						pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						session.getAttribute("iNoNafinElectronico").toString(),
						(String)session.getAttribute("sesExterno"),
						(String) session.getAttribute("strNombre"),
						(String) session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
								
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
						
						pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
						pdfDoc.addText((String)session.getAttribute("strNombre"),"formas",ComunesPDF.CENTER);
													
						int cont = 0;
						double totalSaldoProm=0;
						double totalContraprestacion=0;
						double totalTotal=0;
						String nomCompara = "";
						int x = 0;
						
						while (rs.next()) {
							String saldopromedio 			= rs.getString("saldopromedio")==null?"":rs.getString("saldopromedio");
							String fg_saldo_referencia 	= rs.getString("fg_saldo_referencia")==null?"":rs.getString("fg_saldo_referencia");
							String fg_porcentaje_ajustado = rs.getString("fg_porcentaje_ajustado")==null?"":rs.getString("fg_porcentaje_ajustado");
							String contraprestacionajustada= rs.getString("contraprestacionajustada")==null?"":rs.getString("contraprestacionajustada");
							String contraprestaciontotalajustada= rs.getString("contraprestaciontotalajustada")==null?"":rs.getString("contraprestaciontotalajustada");
							String ajuste 						= rs.getString("ajuste")==null?"":rs.getString("ajuste");
							String cg_estatus_pagado 		= rs.getString("cg_estatus_pagado")==null?"":rs.getString("cg_estatus_pagado");
							String ic_nombre 					= rs.getString("ic_nombre")==null?"":rs.getString("ic_nombre").trim();
							
							totalSaldoProm += new Double(saldopromedio).doubleValue();
							totalContraprestacion += new Double(contraprestacionajustada).doubleValue();
							totalTotal += new Double(contraprestaciontotalajustada).doubleValue();
							
							if(!nomCompara.equals(ic_nombre) && x>0) 
								pdfDoc.addTable();
							
							if(!nomCompara.equals(ic_nombre)){
								pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
								pdfDoc.addText("PRODUCTO: " + ic_nombre,"formas",ComunesPDF.CENTER);
								
								pdfDoc.setTable(9, 100);
								pdfDoc.setCell("A�o","celda02",ComunesPDF.CENTER);
								pdfDoc.setCell("Saldo Promedio","celda02",ComunesPDF.CENTER);
								pdfDoc.setCell("Saldo de Referencia","celda02",ComunesPDF.CENTER);
								pdfDoc.setCell("Tasa Ajustada","celda02",ComunesPDF.CENTER);
								pdfDoc.setCell("Contraprestacion Ajustada","celda02",ComunesPDF.CENTER);
								pdfDoc.setCell("% IVA","celda02",ComunesPDF.CENTER);
								pdfDoc.setCell("Contraprestacion Total Ajustada","celda02",ComunesPDF.CENTER);
								pdfDoc.setCell("Ajuste","celda02",ComunesPDF.CENTER);
								pdfDoc.setCell("Pagado","celda02",ComunesPDF.CENTER);
							}
							
							pdfDoc.setCell(this.ic_anio_calculo,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ " + Comunes.formatoDecimal(saldopromedio,2,SIN_COMAS),"formas",ComunesPDF.CENTER);		
							pdfDoc.setCell("$ " + Comunes.formatoDecimal(fg_saldo_referencia,2,SIN_COMAS),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ " + Comunes.formatoDecimal(fg_porcentaje_ajustado,2,SIN_COMAS),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ " + Comunes.formatoDecimal(contraprestacionajustada,2,SIN_COMAS),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(Iva.getPorcentaje(fecha) + " %","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ " + Comunes.formatoDecimal(contraprestaciontotalajustada,2,SIN_COMAS),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ " + Comunes.formatoDecimal(ajuste,2,SIN_COMAS),"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell(cg_estatus_pagado,"formas",ComunesPDF.CENTER);	
							//pdfDoc.addTable();	   
																				
							nomCompara = ic_nombre;				
							x++;
						}
						pdfDoc.addTable();	
						
						pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
						pdfDoc.addText("Gran Total M.N.","celda01",ComunesPDF.CENTER);
						pdfDoc.setTable(4, 60);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Saldo Promedio","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Contraprestacion","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("TOTAL","celda01",ComunesPDF.CENTER);
						
						pdfDoc.setCell("Gran Total M.N.","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(Double.toString(totalSaldoProm).toString(),2,SIN_COMAS),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(Double.toString(totalContraprestacion).toString(),2,SIN_COMAS),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(Double.toString(totalTotal).toString(),2,SIN_COMAS),"formas",ComunesPDF.CENTER);
						
						pdfDoc.addTable();	
						pdfDoc.endDocument();  
					} catch(Throwable e) {
						throw new AppException("Error al generar el archivo", e);
					} finally {
						try {
							rs.close();  
						} catch(Exception e) {}
					}
				}
		}	
		return nombreArchivo;	
	}	
	
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}


	public void setIc_producto_nafin(String ic_producto_nafin) {
		this.ic_producto_nafin = ic_producto_nafin;
	}


	public String getIc_producto_nafin() {
		return ic_producto_nafin;
	}


	public void setIc_mes_calculo_de(String ic_mes_calculo_de) {
		this.ic_mes_calculo_de = ic_mes_calculo_de;
	}


	public String getIc_mes_calculo_de() {
		return ic_mes_calculo_de;
	}


	public void setIc_mes_calculo_a(String ic_mes_calculo_a) {
		this.ic_mes_calculo_a = ic_mes_calculo_a;
	}


	public String getIc_mes_calculo_a() {
		return ic_mes_calculo_a;
	}


	public void setIc_anio_calculo(String ic_anio_calculo) {
		this.ic_anio_calculo = ic_anio_calculo;
	}


	public String getIc_anio_calculo() {
		return ic_anio_calculo;
	}


	public void setCg_estatus_pagado(String cg_estatus_pagado) {
		this.cg_estatus_pagado = cg_estatus_pagado;
	}


	public String getCg_estatus_pagado() {
		return cg_estatus_pagado;
	}


	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}


	public String getIc_moneda() {
		return ic_moneda;
	}


	public void setIc_banco_fondeo(String ic_banco_fondeo) {
		this.ic_banco_fondeo = ic_banco_fondeo;
	}


	public String getIc_banco_fondeo() {
		return ic_banco_fondeo;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public String getIc_if() {
		return ic_if;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}


	public String getPeriodo() {
		return periodo;
	}

}