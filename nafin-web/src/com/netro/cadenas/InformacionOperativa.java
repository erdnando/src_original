package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;
//ESTADO DE CUENTA
public class InformacionOperativa implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(InformacionOperativa.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	//PARAMETROS 
	private String icIf;
	private String moneda;
	private String cliente;
	private String prestamo;
	private String fechaCorte;
	
	private List pKeys;
	
	public InformacionOperativa() {
	
	}

	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
				qrySentencia.append("SELECT   /*+ 		 INDEX (E IN_COM_ESTADO_CUENTA_01_NUK)     */	\n"+
														"    COUNT (1) AS NUMERO_REGISTROS, 	\n"+
														"		SUM (FG_MONTOOPERADO)AS MONTO_OPERADO, 	\n"+
														"		SUM (FG_SALDOINSOLUTO) AS SALDO_INSOLUTO,	\n"+
														"		SUM (FG_CAPITALVIGENTE) AS CAPITAL_VIGENTE, 	\n"+
														"		SUM (FG_CAPITALVENCIDO) AS CAPITAL_VENCIDO,	\n"+
														"		SUM (FG_INTERESVENCIDO) AS INTERES_VENCIDO, 	\n"+
														"		SUM (FG_INTERESMORAT) AS INTERES_MORATORIO, 	\n"+
														"		SUM (FG_ADEUDOTOTAL) AS ADEUDO_TOTAL,	\n"+
														"		'TOTAL '||M.CD_NOMBRE AS TIPO_MONEDA, 	\n"+
														"		E.IC_MONEDA, 	\n"+
														"		'ESTCUENTAIFDE::GETAGGREGATECALCULATIONQUERY'	\n"+
														"FROM COM_ESTADO_CUENTA E, COMCAT_MONEDA M	\n"+
														"WHERE E.IC_MONEDA = M.IC_MONEDA	\n"+
														"	AND E.IC_IF = ? \n");
						conditions.add(icIf);
				if(!moneda.equals("")){
					qrySentencia.append("AND M.ic_moneda = ? \n");
					conditions.add(moneda);
				}
				if(!cliente.equals("")){
					qrySentencia.append(" AND		E.ig_cliente = ? \n");
					conditions.add(cliente);
				}
				if(!prestamo.equals("")){
					qrySentencia.append(" AND		E.ig_prestamo = ? \n");
					conditions.add(prestamo);
				}
				if(!fechaCorte.equals("")){
					qrySentencia.append(" AND	DF_FECHAFINMES >=	to_date(?, 'dd/mm/yyyy') \n");
					conditions.add(fechaCorte);
					qrySentencia.append(" AND	DF_FECHAFINMES <	to_date(?, 'dd/mm/yyyy')+1 \n");
					conditions.add(fechaCorte);
				}
				
				qrySentencia.append(" group by E.ic_moneda,m.cd_nombre");
      	qrySentencia.append(" order by E.ic_moneda");
						
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * 
	 * @return 
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		qrySentencia.append(" SELECT   /*+         INDEX (E IN_COM_ESTADO_CUENTA_01_NUK)     */ \n"+
									"	ic_estado_cuenta--, 'EstCuentaIfDE::getDocumentQuery' \n"+
									"FROM com_estado_cuenta \n"+
									"WHERE \n"+
									"	ic_if = ? \n");
									
					conditions.add(icIf);				
									
				if(!moneda.equals("")){
					qrySentencia.append("AND ic_moneda = ? \n");
					conditions.add(moneda);
				}
				if(!cliente.equals("")){
					qrySentencia.append(" AND		ig_cliente = ? \n");
					conditions.add(cliente);
				}
				if(!prestamo.equals("")){
					qrySentencia.append(" AND		ig_prestamo = ? \n");
					conditions.add(prestamo);
				}
				if(!fechaCorte.equals("")){
					qrySentencia.append(" AND	DF_FECHAFINMES >=	to_date(?, 'dd/mm/yyyy') \n");
					conditions.add(fechaCorte);
					qrySentencia.append(" AND	DF_FECHAFINMES <	to_date(?, 'dd/mm/yyyy')+1 \n");
					conditions.add(fechaCorte);
				}
					

			qrySentencia.append("ORDER BY ic_moneda, ig_cliente, ig_prestamo	");
			

		log.info("getDocumentQuery_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
					qrySentencia.append("SELECT /*INDEX (I CP_COMCAT_IF_PK) INDEX (M CP_COMCAT_MOENDA_PK)*/	\n"+
											"	IC_ESTADO_CUENTA AS CUENTA,"+
											"	IG_CLIENTE  AS CLIENTE,	\n"+
											"	CG_NOMBRECLIENTE  AS NOMBRE_CLIENTE,	\n"+
											"	TO_CHAR (DF_FECHOPERACION, 'DD/MM/YYYY') AS FECHA_OPERACION,	\n"+
											"	IG_PRESTAMO AS PRESTAMO, 	\n"+
											"	IG_TASAREFERENCIAL AS TASA_REFERENCIAL, 	\n"+
											"	FG_SPREAD AS SPREAD,	\n"+
											"	FG_MARGEN AS MARGEN, 	\n"+
											"	FG_MONTOOPERADO AS MONTO_OPERADO, 	\n"+
											"	FG_SALDOINSOLUTO AS SALDO_INSOLUTO, 	\n"+
											"	FG_CAPITALVIGENTE AS CAPITAL_VIGENTE,	\n"+
											"	FG_CAPITALVENCIDO AS CAPITAL_VENCIDO, 	\n"+
											"	FG_INTERESVENCIDO AS INTERES_VENCIDO, 	\n"+
											"	FG_INTERESMORAT AS INTERES_MORATORIO,	\n"+
											"	FG_ADEUDOTOTAL AS ADEUDO_TOTAL, 	\n"+
											"	E.IC_MONEDA, 	\n"+
											"	M.CD_NOMBRE, 	\n"+
											"	'ESTCUENTAIFDE::getDocumentQueryFile'	\n"+
											"FROM 	\n"+
											"	 COM_ESTADO_CUENTA E, 	\n"+
											"	 COMCAT_IF I, 	\n"+
											"	 COMCAT_MONEDA M	\n"+
											"WHERE 	\n"+
											"	 E.IC_MONEDA = M.IC_MONEDA	\n"+
											"	 AND E.IC_IF = I.IC_IF	\n"+
											"	 AND E.IC_ESTADO_CUENTA IN(");
		//lista pKeys para almacenar los parametros in de la consulta totales parciales
		pKeys = new ArrayList();
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0){ qrySentencia.append(","); }
			qrySentencia.append("?");
			conditions.add(lItem.get(0));
			pKeys.add(pageIds.get(i));
			
		}
		qrySentencia.append(") order by ic_moneda, ig_cliente, ig_prestamo ");
		log.info("getDocumentSummaryQueryForIds_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	/**
	 * 
	 * @return objeto de tipo registros.
	 * Calcula los totales parciales segun la pagina donde este.
	 */
	public Registros getTotalesParciales(){
	log.info("getTotalesParciales(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
				qrySentencia.append("SELECT   /*+ 		 INDEX (E IN_COM_ESTADO_CUENTA_01_NUK)     */	\n"+
														"    COUNT (1) AS NUMERO_REGISTROS, 	\n"+
														"		SUM (FG_MONTOOPERADO)AS MONTO_OPERADO, 	\n"+
														"		SUM (FG_SALDOINSOLUTO) AS SALDO_INSOLUTO,	\n"+
														"		SUM (FG_CAPITALVIGENTE) AS CAPITAL_VIGENTE, 	\n"+
														"		SUM (FG_CAPITALVENCIDO) AS CAPITAL_VENCIDO,	\n"+
														"		SUM (FG_INTERESVENCIDO) AS INTERES_VENCIDO, 	\n"+
														"		SUM (FG_INTERESMORAT) AS INTERES_MORATORIO, 	\n"+
														"		SUM (FG_ADEUDOTOTAL) AS ADEUDO_TOTAL,	\n"+
														"		'Total Parcial '||M.CD_NOMBRE AS TIPO_MONEDA, 	\n"+
														"		E.IC_MONEDA, 	\n"+
														"		'ESTCUENTAIFDE::GETAGGREGATECALCULATIONQUERY'	\n"+
														"FROM COM_ESTADO_CUENTA E, COMCAT_MONEDA M	\n"+
														"WHERE E.IC_MONEDA = M.IC_MONEDA	\n"+
														"	AND E.IC_IF = ? \n"
														);
														
						conditions.add(icIf);
						
				if(!moneda.equals("")){
					qrySentencia.append("AND M.ic_moneda = ? \n");
					conditions.add(moneda);
				}else{
					//qrySentencia.append("AND ic_moneda = 1 \n");
				}
				if(!cliente.equals("")){
					qrySentencia.append(" AND		E.ig_cliente = ? \n");
					conditions.add(cliente);
				}
				if(!prestamo.equals("")){
					qrySentencia.append(" AND		E.ig_prestamo = ? \n");
					conditions.add(prestamo);
				}
				if(!fechaCorte.equals("")){
					qrySentencia.append(" AND	DF_FECHAFINMES >=	to_date(?, 'dd/mm/yyyy') \n");
					conditions.add(fechaCorte);
					qrySentencia.append(" AND	DF_FECHAFINMES <	to_date(?, 'dd/mm/yyyy')+1 \n");
					conditions.add(fechaCorte);
				}
				System.err.println(">>>>>>>>>>>>>>>>"+this.getPKeys());
				if(pKeys.size() >0){		
						qrySentencia.append("	AND E.IC_ESTADO_CUENTA IN(");
						for(int i = 0; i < pKeys.size(); i++){
							List lItem = (ArrayList)pKeys.get(i);
							if(i>0){ qrySentencia.append(","); }
							qrySentencia.append("?");
							conditions.add(lItem.get(0));
						}
						qrySentencia.append(")\n");
				}
				qrySentencia.append("group by E.ic_moneda,m.cd_nombre\n");
				qrySentencia.append("order by E.ic_moneda");
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getTotalesParciales  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
		log.info("qrySentencia getTotalesParciales\n: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getTotalesParciales (S) ");
		return registros;
	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
	qrySentencia.append("SELECT  	\n"+
								"	 TO_CHAR (E.DF_FECHAFINMES, 'DD/MM/YYYY') AS FECHAFINMES,	\n"+
								"	 E.IC_MONEDA AS MONEDA, 	\n"+
								"	 M.CD_NOMBRE AS DESCMONEDA,	\n"+
								"	 E.IC_IF AS INTERMEDIARIO, 	\n"+
								"	 I.CG_RAZON_SOCIAL AS DESCIF,	\n"+
								"	 E.IG_CODSUCURSAL AS CODSUCURSAL, 	\n"+
								"	 E.CG_NOMSUCURSAL AS NOMSUCURSAL,	\n"+
								"	 E.IG_TIPOINTERMEDIARIO AS TIPINTERMEDIARIO,	\n"+
								"	 E.CG_TIPOINTERMEDIARIO_DESC AS DESCRIPCION,	\n"+
								"	 E.CG_DESCMODPAGO AS DESCMODPAGO, 	\n"+
								"	 E.IG_PRODBANCO AS PRODBANCO,	\n"+
								"	 E.CG_PRODBANCO_DESC AS DESCRIPCION, 	\n"+
								"	 E.IG_SUBAPL AS SUBAPL,	\n"+
								"	 E.CG_SUBAPL_DESC AS DESCRIPCION, 	\n"+
								"	 E.IG_CLIENTE AS CLIENTE,	\n"+
								"	 E.CG_NOMBRECLIENTE AS NOMBRECLIENTE, 	\n"+
								"	 E.IG_PROYECTO AS PROYECTO,	\n"+
								"	 E.IG_CONTRATO AS CONTRATO, 	\n"+
								"	 E.IG_PRESTAMO AS PRESTAMO,	\n"+
								"	 E.IG_NUMERO_ELECTRONICO AS NUMELECTRONICO,	\n"+
								"	 E.IG_DISPOSICION AS DISPOSICION, 	\n"+
								"	 E.IG_FRECINTERES AS FRECINTERES,	\n"+
								"	 E.IG_FRECCAPITAL AS FRECCAPITAL,	\n"+
								"	 TO_CHAR (E.DF_FECHOPERACION, 'DD/MM/YYYY') AS FECHOPERACION,	\n"+
								"	 TO_CHAR (E.DF_FECHVENCIMIENTO, 'DD/MM/YYYY') AS FECHVENCIMIENTO,	\n"+
								"	 E.IG_TASAREFERENCIAL AS TASAREFERENCIAL,	\n"+
								"	 E.CG_DESCRIPCIONTASA AS DESCRIPCIONTASA, 	\n"+
								"	 E.CG_RELMAT1 AS RELMAT1,	\n"+
								"	 E.FG_SPREAD AS SPREAD, 	\n"+
								"	 E.FG_MARGEN AS MARGEN,	\n"+
								"	 E.FG_TASAMORATORIA AS TASAMORATORIA, 	\n"+
								"	 E.IG_SANCION AS SANCION,	\n"+
								"	 E.DF_1ACUOTAVEN AS ACUOTAVEN, 	\n"+
								"	 E.IG_DIAVENCIMIENTO AS DIAVENCIMIENTO,	\n"+
								"	 E.IG_DIAPROVISION AS DIAPROVISION, 	\n"+
								"	 E.FG_MONTOOPERADO AS MONTOOPERADO,	\n"+
								"	 E.FG_SALDOINSOLUTO AS SALDOINSOLUTO,	\n"+
								"	 E.FG_CAPITALVIGENTE AS CAPITALVIGENTE,	\n"+
								"	 E.FG_CAPITALVENCIDO AS CAPITALVENCIDO,	\n"+
								"	 E.FG_INTERESPROVI AS INTERESPROVI,	\n"+
								"	 E.FG_INTCOBRADOXANTICIP AS INTCOBANT,	\n"+
								"	 E.FG_INTERESGRAVPROV AS INTERESGRAVPROV, 	\n"+
								"	 E.FG_IVAPROV AS IVAPROV,	\n"+
								"	 E.FG_INTERESVENCIDO AS INTERESVENCIDO,	\n"+
								"	 E.FG_INTERESVENCIDOGRAVADO AS INTERESVENCIDOGRAVADO,	\n"+
								"	 E.FG_IVAVENCIDO AS IVAVENCIDO, 	\n"+
								"	 E.FG_INTERESMORAT AS INTERESMORAT,	\n"+
								"	 E.FG_INTERESMORATGRAVADO AS INTERESMORATGRAVADO,	\n"+
								"	 E.FG_IVASOBREMORATORIOS AS IVASOBREMORATORIOS,	\n"+
								"	 E.FG_SOBRETASAMOR AS SOBRETASAMOR,	\n"+
								"	 E.FG_SOBRETASAMORGRAVADO AS SOBRETASAMORGRAVADO,	\n"+
								"	 E.FG_OTROSADEUDOS AS OTROSADEUDOS, 	\n"+
								"	 E.FG_COMISIONGTIA AS COMISIONGTIA,	\n"+
								"	 E.FG_COMISIONES AS COMISIONES,	\n"+
								"	 E.FG_SOBTASAGTIA_PORC AS SOBTASAGTIAPORC,	\n"+
								"	 E.FG_FINADICOTORG AS FINADICOTORG,	\n"+
								"	 E.FG_FINANADICRECUP AS FINANADICRECUP, 	\n"+
								"	 E.FG_TOTALFINAN AS TOTALFINAN,	\n"+
								"	 E.FG_ADEUDOTOTAL AS ADEUDOTOTAL, 	\n"+
								"	 E.FG_CAPITALRECUP AS CAPITALRECUP,	\n"+
								"	 E.FG_INTERESRECUP AS INTERESRECUP,	\n"+
								"	 E.FG_INTERESRECUPGRAVADO AS INTERESRECUPGRAVADO,	\n"+
								"	 E.FG_MORARECUP AS MORARECUP,	\n"+
								"	 E.FG_MORARECUPGRAVADO AS MORARECUPGRAVADO, 	\n"+
								"	 E.FG_IVARECUP AS IVARECUP,	\n"+
								"	 E.FG_SUBSIDIOAPLICADO AS SUBSIDIOAPLICADO,	\n"+
								"	 E.FG_SALDONAFIN AS SALDONAFIN, 	\n"+
								"	 E.FG_SALDOBURSATIL AS SALDOBURSATIL,	\n"+
								"	 E.FG_VALORTASA AS VALORTASA, 	\n"+
								"	 E.FG_TASATOTAL AS TASATOTAL,	\n"+
								"	 E.CG_EDOCARTERA AS EDOCARTERA, I.IC_FINANCIERA,	\n"+
								"	 'ESTCUENTAIFDE::GETDOCUMENTQUERYFILE'	\n"+
								"FROM 	\n"+
								"	 COM_ESTADO_CUENTA E,	\n"+ 
								"	 COMCAT_IF I,	\n"+ 
								"	 COMCAT_MONEDA M	\n"+
								"WHERE 	\n"+
								"	 E.IC_MONEDA = M.IC_MONEDA	\n"+
								"	 AND E.IC_IF = I.IC_IF	\n" );
			
				if(!icIf.equals("")){
					qrySentencia.append(" AND E.IC_IF = ? \n");
					conditions.add(icIf);
				}
				if(!moneda.equals("")){
					qrySentencia.append("AND M.ic_moneda = ? \n");
					conditions.add(moneda);
				}
				if(!cliente.equals("")){
					qrySentencia.append(" AND		E.ig_cliente = ? \n");
					conditions.add(cliente);
				}
				if(!prestamo.equals("")){
					qrySentencia.append(" AND		E.ig_prestamo = ? \n");
					conditions.add(prestamo);
				}
				if(!fechaCorte.equals("")){
					qrySentencia.append(" AND	DF_FECHAFINMES >=	to_date(?, 'dd/mm/yyyy') \n");
					conditions.add(fechaCorte);
					qrySentencia.append(" AND	DF_FECHAFINMES <	to_date(?, 'dd/mm/yyyy')+1 \n");
					conditions.add(fechaCorte);
				}

			qrySentencia.append("ORDER BY M.ic_moneda, ig_cliente, ig_prestamo	");		
			
							
		log.info("getDocumentQueryFile_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
			pdfDoc.setTable(15,90);
			pdfDoc.setCell("Cliente","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre del Cliente","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha Operaci�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Pr�stamo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tasa ref","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Spread","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Margen","celda01",ComunesPDF.CENTER);
			
			pdfDoc.setCell("Monto Operado","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Saldo Insoluto","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Capital vigente ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Capital vencido ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Inter�s vencido ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Inter�s moratorio ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Adeudo Total","celda01",ComunesPDF.CENTER);
			BigDecimal suma= new BigDecimal("0");
			while (rs.next())	{					
				String cliente = (rs.getString("CLIENTE") == null) ? "" : rs.getString("CLIENTE");
				String nombre = (rs.getString("NOMBRE_CLIENTE") == null) ? "" : rs.getString("NOMBRE_CLIENTE");
				String fechaOperacion = (rs.getString("FECHA_OPERACION") == null) ? "" : rs.getString("FECHA_OPERACION");
				String prestamo = (rs.getString("PRESTAMO") == null) ? "" : rs.getString("PRESTAMO");
				String tasaRef = (rs.getString("TASA_REFERENCIAL") == null) ? "" : rs.getString("TASA_REFERENCIAL");
				String moneda = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
				String spread = (rs.getString("SPREAD") == null) ? "" : rs.getString("SPREAD");
				String margen = (rs.getString("MARGEN") == null) ? "" : rs.getString("MARGEN");
				String montoOperado = (rs.getString("MONTO_OPERADO") == null) ? "" : rs.getString("MONTO_OPERADO");
				BigDecimal montoOperadoS= new BigDecimal(montoOperado);
				suma.add(montoOperadoS);
				String saldoInsoluto = (rs.getString("SALDO_INSOLUTO") == null) ? "" : rs.getString("SALDO_INSOLUTO");
				String capitalVigente = (rs.getString("CAPITAL_VIGENTE") == null) ? "" : rs.getString("CAPITAL_VIGENTE");
				String capitalVencido = (rs.getString("CAPITAL_VENCIDO") == null) ? "" : rs.getString("CAPITAL_VENCIDO");
				String interesVencido = (rs.getString("INTERES_VENCIDO") == null) ? "" : rs.getString("INTERES_VENCIDO");
				String interesMoratorio = (rs.getString("INTERES_MORATORIO") == null) ? "" : rs.getString("INTERES_MORATORIO");
				String adeudoTotal = (rs.getString("ADEUDO_TOTAL") == null) ? "" : rs.getString("ADEUDO_TOTAL");
				
				pdfDoc.setCell(cliente,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombre,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaOperacion,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(prestamo,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(tasaRef,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal( spread,5,true),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal( margen,2,true),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( montoOperado,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( saldoInsoluto,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( capitalVigente,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( capitalVencido,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( interesVencido,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( interesMoratorio,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( adeudoTotal,2,true),"formas",ComunesPDF.RIGHT);
				
			}
			
			pdfDoc.addTable();
		/*
			
			pdfDoc.setTable(15,90);
			Registros parciales = getTotalesParciales();
			parciales.rewind();
			while(parciales.next()){
				String tipoMoneda = (parciales.getString("TIPO_MONEDA") == null) ? "" : parciales.getString("TIPO_MONEDA");
				String montoOperado = (parciales.getString("MONTO_OPERADO") == null) ? "" : parciales.getString("MONTO_OPERADO");
				String saldoInsoluto = (parciales.getString("SALDO_INSOLUTO") == null) ? "" : parciales.getString("SALDO_INSOLUTO");
				String capitalVigente = (parciales.getString("CAPITAL_VIGENTE") == null) ? "" : parciales.getString("CAPITAL_VIGENTE");
				String capitalVencido = (parciales.getString("CAPITAL_VENCIDO") == null) ? "" : parciales.getString("CAPITAL_VENCIDO");
				String interesVencido = (parciales.getString("INTERES_VENCIDO") == null) ? "" : parciales.getString("INTERES_VENCIDO");
				String interesMoratorio = (parciales.getString("INTERES_MORATORIO") == null) ? "" : parciales.getString("INTERES_MORATORIO");
				String adeudoTotal = (parciales.getString("ADEUDO_TOTAL") == null) ? "" : parciales.getString("ADEUDO_TOTAL");
				
				pdfDoc.setCell(tipoMoneda,"celda01",ComunesPDF.LEFT,8);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( montoOperado,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( saldoInsoluto,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( capitalVigente,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( capitalVencido,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( interesVencido,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( interesMoratorio,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( adeudoTotal,2,true),"formas",ComunesPDF.RIGHT);
			}
			pdfDoc.addTable();
			

			
			pdfDoc.setTable(15,90);
			Registros totales = getTotales();
			totales.rewind();
			while(totales.next()){
				String tipoMoneda = (totales.getString("TIPO_MONEDA") == null) ? "" : totales.getString("TIPO_MONEDA");
				String numReg = (totales.getString("NUMERO_REGISTROS") == null) ? "" : totales.getString("NUMERO_REGISTROS");
				String montoOperado = (totales.getString("MONTO_OPERADO") == null) ? "" : totales.getString("MONTO_OPERADO");
				BigDecimal montoOperadoS= new BigDecimal(montoOperado);
				String saldoInsoluto = (totales.getString("SALDO_INSOLUTO") == null) ? "" : totales.getString("SALDO_INSOLUTO");
				String capitalVigente = (totales.getString("CAPITAL_VIGENTE") == null) ? "" : totales.getString("CAPITAL_VIGENTE");
				String capitalVencido = (totales.getString("CAPITAL_VENCIDO") == null) ? "" : totales.getString("CAPITAL_VENCIDO");
				String interesVencido = (totales.getString("INTERES_VENCIDO") == null) ? "" : totales.getString("INTERES_VENCIDO");
				String interesMoratorio = (totales.getString("INTERES_MORATORIO") == null) ? "" : totales.getString("INTERES_MORATORIO");
				String adeudoTotal = (totales.getString("ADEUDO_TOTAL") == null) ? "" : totales.getString("ADEUDO_TOTAL");
				
				pdfDoc.setCell(tipoMoneda,"celda01",ComunesPDF.LEFT,5);
				pdfDoc.setCell("N�mero de registros: "+numReg,"celda01",ComunesPDF.RIGHT,3);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( montoOperado,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( saldoInsoluto,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( capitalVigente,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( capitalVencido,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( interesVencido,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( interesMoratorio,2,true),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal( adeudoTotal,2,true),"formas",ComunesPDF.RIGHT);
					
			}
			
			

			pdfDoc.addTable();
			*/
			pdfDoc.endDocument();	
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		
		try {
			if("ZIP".equals(tipo)){
				//archedocuenta_30_04_2013_1_S
				fechaCorte = fechaCorte.replace('/','_');
					nombreArchivo = "archedocuenta_"+fechaCorte+"_"+icIf+"_S.txt";
			}else{
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
			}
			//nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();	
			contenidoArchivo.append("FechaFinMes,	Moneda,		Descripcion,		Intermediario,	Descripcion,			CodSucursal,	NomSucursal,	TipIntermediario,"+
											"	Descripcion,	DescModPago,ProdBanco,Descripcion,SubApl,Descripcion,Cliente,NombreCliente,Proyecto,"+
											"Contrato,Prestamo,NumElectronico,Disposicion,FrecInteres,FrecCapital,FechOperacion ,FechVencimiento,"+     
											"TasaReferencial,DescripcionTasa,RelMat1,Spread,Margen,TasaMoratoria,Sancion,1aCuotaVen,DiaVencimiento,"+
											"DiaProvision,MontoOperado,SaldoInsoluto,CapitalVigente,CapitalVencido,InteresProvi,IntCobAnt,"+
											"InteresGravProv,IVAProv,InteresVencido,InteresVencidoGravado,IVAVencido,InteresMorat,"+
											"InteresMoratGravado,IVAsobreMoratorios,SobreTasaMor,SobreTasaMorGravado ,OtrosAdeudos,"+
											"ComisionGtia,Comisiones,SobTasaGtia%,FinAdicOtorg,FinanAdicRecup,TotalFinan,AdeudoTotal,"+
											"CapitalRecup,InteresRecup,InteresRecupGravado ,MoraRecup,MoraRecupGravado,IVA Recup,"+
											"SubsidioAplicado,SaldoNafin,SaldoBursatil,ValorTasa,TasaTotal,EdoCartera\n\r");

			
		while (rs.next())	{					
				String fechaFinMnes = (rs.getString("FECHAFINMES") == null) ? "" : rs.getString("FECHAFINMES");
				String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
				String descrpcionMoneda=(rs.getString("DESCMONEDA") == null) ? "" : rs.getString("DESCMONEDA");
				String inetermediario=(rs.getString("INTERMEDIARIO") == null) ? "" : rs.getString("INTERMEDIARIO");
				String descInetermediario=(rs.getString("DESCIF") == null) ? "" : rs.getString("DESCIF");
				String codSucursal=(rs.getString("CODSUCURSAL") == null) ? "" : rs.getString("CODSUCURSAL");
				String nombSucursal=(rs.getString("NOMSUCURSAL") == null) ? "" : rs.getString("NOMSUCURSAL");
				String tipoIntemediario=(rs.getString("TIPINTERMEDIARIO") == null) ? "" : rs.getString("TIPINTERMEDIARIO");
				String descTipoIntemediario=(rs.getString("DESCRIPCION") == null) ? "" : rs.getString("DESCRIPCION");
				String descModoPago=(rs.getString("DESCMODPAGO") == null) ? "" : rs.getString("DESCMODPAGO");
				String prodBanco=(rs.getString("PRODBANCO") == null) ? "" : rs.getString("PRODBANCO");
				String descProdBanco=(rs.getString("DESCRIPCION") == null) ? "" : rs.getString("DESCRIPCION");
				String subApl=(rs.getString("SUBAPL") == null) ? "" : rs.getString("SUBAPL");
				String descSubApl=(rs.getString("DESCRIPCION") == null) ? "" : rs.getString("DESCRIPCION");
				String cliente=(rs.getString("CLIENTE") == null) ? "" : rs.getString("CLIENTE");
				String nombreCliente=(rs.getString("NOMBRECLIENTE") == null) ? "" : rs.getString("NOMBRECLIENTE");
				String proyecto=(rs.getString("PROYECTO") == null) ? "" : rs.getString("PROYECTO");
				String contrato=(rs.getString("CONTRATO") == null) ? "" : rs.getString("CONTRATO");
				String prestamo=(rs.getString("PRESTAMO") == null) ? "" : rs.getString("PRESTAMO");
				String numElect=(rs.getString("NUMELECTRONICO") == null) ? "" : rs.getString("NUMELECTRONICO");
				String disposicion=(rs.getString("DISPOSICION") == null) ? "" : rs.getString("DISPOSICION");
				String frecInteres=(rs.getString("FRECINTERES") == null) ? "" : rs.getString("FRECINTERES");
				String frecCapital=(rs.getString("FRECCAPITAL") == null) ? "" : rs.getString("FRECCAPITAL");
				String fechaOperacion=(rs.getString("FECHOPERACION") == null) ? "" : rs.getString("FECHOPERACION");
				String fechaVencimiento=(rs.getString("FECHVENCIMIENTO") == null) ? "" : rs.getString("FECHVENCIMIENTO");
				String tasaReferencial=(rs.getString("TASAREFERENCIAL") == null) ? "" : rs.getString("TASAREFERENCIAL");
				String descTasaReferencial=(rs.getString("DESCRIPCIONTASA") == null) ? "" : rs.getString("DESCRIPCIONTASA");
				String relmat=(rs.getString("RELMAT1") == null) ? "" : rs.getString("RELMAT1");
				String spread=(rs.getString("RELMAT1") == null) ? "" : rs.getString("SPREAD");
				String margen=(rs.getString("MARGEN") == null) ? "" : rs.getString("MARGEN");
				String tasaMoratoria=(rs.getString("TASAMORATORIA") == null) ? "" : rs.getString("TASAMORATORIA");
				String sancion=(rs.getString("SANCION") == null) ? "" : rs.getString("SANCION");
				String aCuotoVen=(rs.getString("ACUOTAVEN") == null) ? "" : rs.getString("ACUOTAVEN");
				String diaVencimiento=(rs.getString("DIAVENCIMIENTO") == null) ? "" : rs.getString("DIAVENCIMIENTO");
				String diaProvision=(rs.getString("DIAPROVISION") == null) ? "" : rs.getString("DIAPROVISION");
				
				String montoOperado=(rs.getString("MONTOOPERADO") == null) ? "" : rs.getString("MONTOOPERADO");
				String saldoInsoluto=(rs.getString("SALDOINSOLUTO") == null) ? "" : rs.getString("SALDOINSOLUTO");
				String capitalVigente=(rs.getString("CAPITALVIGENTE") == null) ? "" : rs.getString("CAPITALVIGENTE");
				String capitalVencido=(rs.getString("CAPITALVENCIDO") == null) ? "" : rs.getString("CAPITALVENCIDO");
				String interesProvi=(rs.getString("INTERESPROVI") == null) ? "" : rs.getString("INTERESPROVI");
				String intCobAnt=(rs.getString("INTCOBANT") == null) ? "" : rs.getString("INTCOBANT");
				String interesGravProv=(rs.getString("INTERESGRAVPROV") == null) ? "" : rs.getString("INTERESGRAVPROV");
				String ivaProb=(rs.getString("IVAPROV") == null) ? "" : rs.getString("IVAPROV");
				String interesVencido=(rs.getString("INTERESVENCIDO") == null) ? "" : rs.getString("INTERESVENCIDO");
				String interesVencidoGravado=(rs.getString("INTERESVENCIDOGRAVADO") == null) ? "" : rs.getString("INTERESVENCIDOGRAVADO");
				String ivaVencido=(rs.getString("IVAVENCIDO") == null) ? "" : rs.getString("IVAVENCIDO");
				String interesMorat=(rs.getString("INTERESMORAT") == null) ? "" : rs.getString("INTERESMORAT");
				String interesMoratGravado=(rs.getString("INTERESMORATGRAVADO") == null) ? "" : rs.getString("INTERESMORATGRAVADO");
				String ivaSobreMorat=(rs.getString("IVASOBREMORATORIOS") == null) ? "" : rs.getString("IVASOBREMORATORIOS");
				String tasaSobreMorat=(rs.getString("SOBRETASAMOR") == null) ? "" : rs.getString("SOBRETASAMOR");
				String tasaSobreMoratGravado=(rs.getString("SOBRETASAMORGRAVADO") == null) ? "" : rs.getString("SOBRETASAMORGRAVADO");
				String otrosAdeudos=(rs.getString("OTROSADEUDOS") == null) ? "" : rs.getString("OTROSADEUDOS");
				String comisionGarantia=(rs.getString("COMISIONGTIA") == null) ? "" : rs.getString("COMISIONGTIA");
				String comisiones=(rs.getString("COMISIONES") == null) ? "" : rs.getString("COMISIONES");
				String sobTasaGarantia=(rs.getString("SOBTASAGTIAPORC") == null) ? "" : rs.getString("SOBTASAGTIAPORC");
				String finaDicOtrog=(rs.getString("FINADICOTORG") == null) ? "" : rs.getString("FINADICOTORG");
				String FinanAdicCreCup=(rs.getString("FINANADICRECUP") == null) ? "" : rs.getString("FINANADICRECUP");
				String totalFinan=(rs.getString("TOTALFINAN") == null) ? "" : rs.getString("TOTALFINAN");
				String adeudoTotal=(rs.getString("ADEUDOTOTAL") == null) ? "" : rs.getString("ADEUDOTOTAL");
				String capitalRecup=(rs.getString("CAPITALRECUP") == null) ? "" : rs.getString("CAPITALRECUP");
				String interesRecup=(rs.getString("INTERESRECUP") == null) ? "" : rs.getString("INTERESRECUP");
				String interesRecupGravado=(rs.getString("INTERESRECUPGRAVADO") == null) ? "" : rs.getString("INTERESRECUPGRAVADO");
				String moraRecup=(rs.getString("MORARECUP") == null) ? "" : rs.getString("MORARECUP");
				String moraRecupGravado=(rs.getString("MORARECUPGRAVADO") == null) ? "" : rs.getString("MORARECUPGRAVADO");
				String ivaRecup=(rs.getString("IVARECUP") == null) ? "" : rs.getString("IVARECUP");
				String subcidioAplicado=(rs.getString("SUBSIDIOAPLICADO") == null) ? "" : rs.getString("SUBSIDIOAPLICADO");
				String saldoFinal=(rs.getString("SALDONAFIN") == null) ? "" : rs.getString("SALDONAFIN");
				String saldoBursatil=(rs.getString("SALDOBURSATIL") == null) ? "" : rs.getString("SALDOBURSATIL");
				String valorTasa=(rs.getString("VALORTASA") == null) ? "" : rs.getString("VALORTASA");
				String tasaTotal=(rs.getString("TASATOTAL") == null) ? "" : rs.getString("TASATOTAL");
				String estadoCartera=(rs.getString("EDOCARTERA") == null) ? "" : rs.getString("EDOCARTERA");
				
				
				contenidoArchivo.append(fechaFinMnes.replace(',',' ')+",	"+moneda.replace(',',' ')+",	"+
				descrpcionMoneda.replace(',',' ')+",	"+inetermediario.replace(',',' ')+",	"+
				descInetermediario.replace(',',' ')+",		"+codSucursal.replace(',',' ')+",	"+
				nombSucursal.replace(',',' ')+",		"+tipoIntemediario.replace(',',' ')+",		"+
				descTipoIntemediario.replace(',',' ')+",		"+descModoPago.replace(',',' ')+","+
				prodBanco.replace(',',' ')+","+descProdBanco.replace(',',' ')+","+
				subApl.replace(',',' ')+","+descSubApl.replace(',',' ')+","+
				cliente.replace(',',' ')+","+nombreCliente.replace(',',' ')+","+
				proyecto.replace(',',' ')+","+contrato.replace(',',' ')+","+
				prestamo.replace(',',' ')+","+numElect.replace(',',' ')+","+
				disposicion.replace(',',' ')+","+frecInteres.replace(',',' ')+","+
				frecCapital.replace(',',' ')+","+fechaOperacion.replace(',',' ')+","+
				fechaVencimiento.replace(',',' ')+","+tasaReferencial.replace(',',' ')+","+
				descTasaReferencial.replace(',',' ')+","+relmat.replace(',',' ')+","+
				spread.replace(',',' ')+","+margen.replace(',',' ')+","+
				tasaMoratoria.replace(',',' ')+","+sancion.replace(',',' ')+","+aCuotoVen.replace(',',' ')+","+
				diaVencimiento.replace(',',' ')+","+diaProvision.replace(',',' ')+","+
				
				montoOperado.replace(',',' ')+","+
				saldoInsoluto.replace(',',' ')+","+
				capitalVigente.replace(',',' ')+","+
				capitalVencido.replace(',',' ')+","+interesProvi.replace(',',' ')+","+
				intCobAnt.replace(',',' ')+","+interesGravProv.replace(',',' ')+","+
				ivaProb.replace(',',' ')+","+interesVencido.replace(',',' ')+","+
				interesVencidoGravado.replace(',',' ')+","+ivaVencido.replace(',',' ')+","+
				interesMorat.replace(',',' ')+","+interesMoratGravado.replace(',',' ')+","+
				ivaSobreMorat.replace(',',' ')+","+tasaSobreMorat.replace(',',' ')+","+
				otrosAdeudos.replace(',',' ')+","+comisionGarantia.replace(',',' ')+","+
				comisiones.replace(',',' ')+","+	sobTasaGarantia.replace(',',' ')+","+
				finaDicOtrog.replace(',',' ')+","+FinanAdicCreCup.replace(',',' ')+","+
				totalFinan.replace(',',' ')+","+	adeudoTotal.replace(',',' ')+","+
				capitalRecup.replace(',',' ')+","+interesRecup.replace(',',' ')+","+
				interesRecupGravado.replace(',',' ')+","+moraRecup.replace(',',' ')+","+
				moraRecupGravado.replace(',',' ')+","+ivaRecup.replace(',',' ')+","+
				subcidioAplicado.replace(',',' ')+","+saldoFinal.replace(',',' ')+","+
				saldoBursatil.replace(',',' ')+","+valorTasa.replace(',',' ')+","+
				tasaTotal.replace(',',' ')+","+estadoCartera.replace(',',' ')+","+"\n");	
				
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio   
				
		//creaArchivo.make(contenidoArchivo.toString(), path, ".cvs");
		//nombreArchivo = creaArchivo.getNombre();
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}


	/**
	 * 
	 * @return un objeto de tipo registros
	 * este metodo es utilizado para crear el pdf
	 */
	public Registros getTotales(){
	log.info("getTotales(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
				qrySentencia.append("SELECT   /*+ 		 INDEX (E IN_COM_ESTADO_CUENTA_01_NUK)     */	\n"+
														"    COUNT (1) AS NUMERO_REGISTROS, 	\n"+
														"		SUM (FG_MONTOOPERADO)AS MONTO_OPERADO, 	\n"+
														"		SUM (FG_SALDOINSOLUTO) AS SALDO_INSOLUTO,	\n"+
														"		SUM (FG_CAPITALVIGENTE) AS CAPITAL_VIGENTE, 	\n"+
														"		SUM (FG_CAPITALVENCIDO) AS CAPITAL_VENCIDO,	\n"+
														"		SUM (FG_INTERESVENCIDO) AS INTERES_VENCIDO, 	\n"+
														"		SUM (FG_INTERESMORAT) AS INTERES_MORATORIO, 	\n"+
														"		SUM (FG_ADEUDOTOTAL) AS ADEUDO_TOTAL,	\n"+
														"		'TOTAL '||M.CD_NOMBRE AS tipo_moneda, 	\n"+
														"		E.IC_MONEDA, 	\n"+
														"		'ESTCUENTAIFDE::GETAGGREGATECALCULATIONQUERY'	\n"+
														"FROM COM_ESTADO_CUENTA E, COMCAT_MONEDA M	\n"+
														"WHERE E.IC_MONEDA = M.IC_MONEDA	\n"+
														"	AND E.IC_IF = ? \n"
														);
														
								conditions.add(icIf);						
				if(!moneda.equals("")){
					qrySentencia.append("AND M.ic_moneda = ? \n");
					conditions.add(moneda);
				}
				if(!cliente.equals("")){
					qrySentencia.append(" AND		E.ig_cliente = ? \n");
					conditions.add(cliente);
				}
				if(!prestamo.equals("")){
					qrySentencia.append(" AND		E.ig_prestamo = ? \n");
					conditions.add(prestamo);
				}
				if(!fechaCorte.equals("")){
					qrySentencia.append(" AND	DF_FECHAFINMES >=	to_date(?, 'dd/mm/yyyy') \n");
					conditions.add(fechaCorte);
					qrySentencia.append(" AND	DF_FECHAFINMES <	to_date(?, 'dd/mm/yyyy')+1 \n");
					conditions.add(fechaCorte);
				}
				
				qrySentencia.append(" group by E.ic_moneda,m.cd_nombre");
      	qrySentencia.append(" order by E.ic_moneda");
				
      	
			
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getTotales  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia getTotales\n: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getTotales (S) ");
		return registros;
	}
	
	
	public Registros getDatosFechaCorte(){
	log.info("getDatosFechaCorte(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"	SELECT DISTINCT  ");
			qrySentencia.append(	"	TO_CHAR (DF_FECHAFINMES, 'DD/MM/YYYY') as CLAVE, ");
			qrySentencia.append(	"	TO_CHAR (DF_FECHAFINMES, 'DD/MM/YYYY') as DESCRIPCION,  ");
			qrySentencia.append(	"	DF_FECHAFINMES  ");
			qrySentencia.append(	"	FROM  ");
			qrySentencia.append(	"	COM_ESTADO_CUENTA ");
			qrySentencia.append(	"	WHERE  ");
			qrySentencia.append(	"	IC_IF = ? ");
			if(cliente!=null && !"".equals(cliente))
				qrySentencia.append(	"	AND IG_CLIENTE = ? ");
			qrySentencia.append(	"	ORDER BY DF_FECHAFINMES DESC	");

			conditions.add(icIf);
			if(cliente!=null && !"".equals(cliente))
				conditions.add(cliente);
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosFechaCorte  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia Fecha de corte: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosFechaCorte (S) ");
		return registros;
															
	}
	public Registros catalogoMoneda(){
	log.info("getDatosFechaCorte(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT DISTINCT	\n"+
										"	 M.IC_MONEDA as clave,		\n"+
										"	 M.CD_NOMBRE as descripcion		\n"+
										"FROM 		\n"+
										"	 COMCAT_MONEDA M, 		\n"+
										"	 COMCAT_TASA T		\n"+
										"WHERE 		\n"+
										"	 M.IC_MONEDA = T.IC_MONEDA AND 		\n"+
										"	 T.CS_DISPONIBLE = 'S' AND		\n"+
										"	 M.IC_MONEDA = 1		\n"+
										"UNION		\n"+
										"SELECT DISTINCT 		\n"+
										"	 M.IC_MONEDA as clave, 		\n"+
										"	 M.CD_NOMBRE as descripcion		\n"+
										"FROM 		\n"+
										"	 COMCAT_MONEDA M , 		\n"+
										"	 COMCAT_TASA T ,		\n"+
										"	 COM_TIPO_CAMBIO TC    	\n"+
										"WHERE     	\n"+
										"	  M.IC_MONEDA = T.IC_MONEDA AND     	\n"+
										"	  T.CS_DISPONIBLE = 'S' AND    	\n"+
										"	  M.IC_MONEDA = TC.IC_MONEDA    	\n"+
										"ORDER BY 2    ");

			
			
			registros = con.consultarDB(qrySentencia.toString());
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosFechaCorte  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia Fecha de corte: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosFechaCorte (S) ");
		return registros;
															
	}
	

		/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	//SETTERS Y GETTERS
	public void setIcIf(String icIf) {
		this.icIf = icIf;
	}


	public String getIcIf() {
		return icIf;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setCliente(String cliente) {
		this.cliente = cliente;
	}


	public String getCliente() {
		return cliente;
	}


	public void setPrestamo(String prestamo) {
		this.prestamo = prestamo;
	}


	public String getPrestamo() {
		return prestamo;
	}


	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}


	public String getFechaCorte() {
		return fechaCorte;
	}


	public void setPKeys(List pKeys) {
	pKeys = new ArrayList();
		this.pKeys = pKeys;
	}


	public List getPKeys() {
		return pKeys;
	}

}
