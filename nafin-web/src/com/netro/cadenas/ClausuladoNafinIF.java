package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ClausuladoNafinIF implements IQueryGeneratorPS, IQueryGeneratorRegExtJS  {	

	private int numList = 1;
	
	public ClausuladoNafinIF() {}
	
	public int getNumList(HttpServletRequest request){return this.numList;}
  
	public ArrayList getConditions(HttpServletRequest request){
	System.out.println("*************getConditions=" + variablesBind);
	return this.variablesBind;
	} // Fin del m�todo getConditions

	public String getAggregateCalculationQuery(HttpServletRequest request) {return "";} // Fin del m�todo getAggregateCalculationQuery

	public String getDocumentQuery(HttpServletRequest request){
		
		this.variablesBind = new ArrayList();
		StringBuffer strQuery = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
				
		String ic_if				   = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String ic_pyme			      = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String txt_usuario	      = (request.getParameter("txt_usuario")==null)?"":request.getParameter("txt_usuario");
		String txt_fecha_acep_de	= (request.getParameter("txt_fecha_acep_de")==null)?"":request.getParameter("txt_fecha_acep_de");
		String txt_fecha_acep_a		= (request.getParameter("txt_fecha_acep_a")==null)?"":request.getParameter("txt_fecha_acep_a");
		String noBancoFondeo			= (request.getParameter("noBancoFondeo")==null)?"":request.getParameter("noBancoFondeo");
						
		if(!"".equals(ic_if)) {    
			condicion.append(" AND con.ic_if = ? ");
			this.variablesBind.add(new Integer(ic_if));
		}
		if(!"".equals(ic_pyme)) {
			condicion.append(" AND con.ic_pyme = ? ");
			this.variablesBind.add(new Integer(ic_pyme));
		}
		if(!"".equals(txt_usuario)) {
			condicion.append(" AND con.ic_usuario = ? ");
			this.variablesBind.add(txt_usuario);
		}
		if(!"".equals(txt_fecha_acep_de)) {
			condicion.append(" AND con.df_aceptacion >= to_date(?, 'dd/mm/yyyy') ");
			this.variablesBind.add(txt_fecha_acep_de);
		}
		if(!"".equals(txt_fecha_acep_a)) {
			condicion.append(" AND con.df_aceptacion < to_date(?, 'dd/mm/yyyy') + 1 ");
			this.variablesBind.add(txt_fecha_acep_a);
		}
		System.out.println("Banco de Fondeo >>>>>>>>>>>> "+noBancoFondeo );
		if(!"".equals(noBancoFondeo) ){
			condicion.append(
			" AND con.ic_if IN (" +
			" select distinct ie.ic_if " +
			" from comrel_if_epo ie, comcat_epo e " +
			" where ie.ic_epo = e.ic_epo and ie.cs_aceptacion=? " +
			" AND ie.cs_vobo_nafin=? AND ie.cs_bloqueo=? AND e.ic_banco_fondeo=? ) ");
			this.variablesBind.add("S");
			this.variablesBind.add("S");
			this.variablesBind.add("N");
			this.variablesBind.add(new Integer(noBancoFondeo));
		}
		 
    
      strQuery.append(
        " SELECT con.ic_if||'|'||con.ic_consecutivo||'|'||con.ic_pyme,"+
        "        'ClausuladoNafinIF::getDocumentQuery()' origen"+
        " FROM com_aceptacion_contrato_if con"+
        " WHERE con.ic_if IS NOT NULL"+
        "   AND con.ic_pyme IS NOT NULL"+
		  condicion);

		System.out.println("strQuery.toString(): "+strQuery.toString());
		return strQuery.toString();
  	} // Fin del metodo getDocumentQuery

	public String getDocumentSummaryQueryForIds(HttpServletRequest request, Collection ids){

		StringBuffer strQuery = new StringBuffer();
		StringBuffer clavesCombinaciones = new StringBuffer("");
		
		for (Iterator it = ids.iterator(); it.hasNext();){
			it.next();
			clavesCombinaciones.append("?,");
		}
		clavesCombinaciones.deleteCharAt(clavesCombinaciones.length()-1);

System.out.println("ids  : "+ids );

		strQuery.append(
    
      "  SELECT con.ic_if,"+
      "         con.ic_consecutivo,"+
      "         pym.cg_razon_social nompyme,"+
      "         cif.cg_razon_social nomif,"+
      "         con.ic_usuario,"+
      "         TO_CHAR(con.df_aceptacion, 'dd/mm/yyyy hh24:mi')||' Hrs.' df_aceptacion,"+
      "         'ClausuladoNafinIF::getDocumentSummaryQueryforIds()' origen,"+
      "         cnt.cg_ext_documento extension"+
      "  FROM com_aceptacion_contrato_if con,"+
      "       com_contrato_if cnt,"+
      "       comcat_pyme pym,"+
      "       comcat_if cif"+
      "  WHERE con.ic_if = cif.ic_if"+
      "    AND con.ic_pyme = pym.ic_pyme"+
      "    AND con.ic_if = cnt.ic_if"+
      "    AND con.ic_consecutivo = cnt.ic_consecutivo"+
      "   AND con.ic_if||'|'||con.ic_consecutivo||'|'||con.ic_pyme IN ( " + clavesCombinaciones + " )");
		System.out.println("strQuery.toString(): "+strQuery.toString());
		
		System.out.println("clavesCombinaciones  : "+clavesCombinaciones );
		
		
		return strQuery.toString();		
  } //Fin del metodo getDocumentSummaryQueryForIds
  
 	public String getDocumentQueryFile(HttpServletRequest request){

		StringBuffer strQuery = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		this.variablesBind = new ArrayList();
		
		String ic_if				= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String ic_consecutivo	= (request.getParameter("ic_consecutivo")==null)?"":request.getParameter("ic_consecutivo");
		String txt_usuario			= (request.getParameter("txt_usuario")==null)?"":request.getParameter("txt_usuario");
		String txt_fecha_acep_de	= (request.getParameter("txt_fecha_acep_de")==null)?"":request.getParameter("txt_fecha_acep_de");
		String txt_fecha_acep_a		= (request.getParameter("txt_fecha_acep_a")==null)?"":request.getParameter("txt_fecha_acep_a");
				
		if(!"".equals(ic_if)) {
			condicion.append(" AND con.ic_if = ? ");
			this.variablesBind.add(new Integer(ic_if));
		}
    if(!"".equals(ic_consecutivo)) {
			condicion.append(" AND con.ic_consecutivo = ? ");
			this.variablesBind.add(new Integer(ic_consecutivo));
		}
		if(!"".equals(ic_pyme)) {
			condicion.append(" AND con.ic_pyme = ? ");
			this.variablesBind.add(new Integer(ic_pyme));
		}
		if(!"".equals(txt_usuario)) {
			condicion.append(" AND con.ic_usuario = ? ");
			this.variablesBind.add(txt_usuario);
		}
		if(!"".equals(txt_fecha_acep_de)) {
			condicion.append(" AND con.df_aceptacion >= to_date(?, 'dd/mm/yyyy') ");
			this.variablesBind.add(txt_fecha_acep_de);
		}
		if(!"".equals(txt_fecha_acep_a)) {
			condicion.append(" AND con.df_aceptacion < to_date(?, 'dd/mm/yyyy') + 1 ");
			this.variablesBind.add(txt_fecha_acep_a);
		}

		strQuery.append(
      " SELECT pym.cg_razon_social nompyme,"+
      "        cif.cg_razon_social nomif,"+
      "        con.ic_usuario,"+
      "        TO_CHAR(con.df_aceptacion, 'dd/mm/yyyy hh24:mi')||' Hrs.' df_aceptacion,"+
      "        'ClausuladoNafinIF::getDocumentQueryFile()' origen"+
      " FROM com_aceptacion_contrato_if con,"+
      "       comcat_pyme pym,"+
      " WHERE con.ic_if = cif.ic_if"+
      "   AND con.ic_pyme = pym.ic_pyme"+
			condicion);
		
		System.out.println("strQuery.toString(): "+strQuery.toString());
		return strQuery.toString();
		
  	} // Fin del metodo getDocumentQueryFile
	private ArrayList variablesBind = null; 
	
	
	/***************************************************************************
	 *			 							MIGRACION 												*
	***************************************************************************/
	// Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ClausuladoNafinIF.class);
  
	public  StringBuffer strQuery;
	private List conditions = new ArrayList(); 
	private String ic_epo="";
	private String ic_if;
	private String ic_pyme;
	private String txt_usuario;
	private String txt_fecha_acep_de;
	private String txt_fecha_acep_a;
	private String noBancoFondeo;
	private String ic_consecutivo;
	
	public String getAggregateCalculationQuery() { // Obtiene los totales 
		return "";
 	}  
		 
	public String getDocumentQuery(){  // para las llaves primarias
		//conditions = new ArrayList();	
		strQuery   = new StringBuffer(); 
		StringBuffer condicion = new StringBuffer();
		
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		
		if(!"".equals(ic_if)) {    
			condicion.append(" AND con.ic_if = ? ");
			conditions.add(new Integer(ic_if));
		}
		if(!"".equals(ic_pyme)) {
			condicion.append(" AND con.ic_pyme = ? ");
			conditions.add(new Integer(ic_pyme));
		}
		if(!"".equals(txt_usuario)) {
			condicion.append(" AND con.ic_usuario = ? ");
			conditions.add(txt_usuario);
		}
		if(!"".equals(txt_fecha_acep_de)) {
			condicion.append(" AND con.df_aceptacion >= to_date(?, 'dd/mm/yyyy') ");
			conditions.add(txt_fecha_acep_de);
		}
		if(!"".equals(txt_fecha_acep_a)) {
			condicion.append(" AND con.df_aceptacion < to_date(?, 'dd/mm/yyyy') + 1 ");
			conditions.add(txt_fecha_acep_a);
		}
		if(!"".equals(noBancoFondeo) ){
			condicion.append(
			" AND con.ic_if IN (" +
			" select distinct ie.ic_if " +
			" from comrel_if_epo ie, comcat_epo e " +
			" where ie.ic_epo = e.ic_epo and ie.cs_aceptacion=? " +
			" AND ie.cs_vobo_nafin=? AND ie.cs_bloqueo=? AND e.ic_banco_fondeo=? ) ");
			conditions.add("S");
			conditions.add("S");
			conditions.add("N");
			conditions.add(new Integer(noBancoFondeo));
		}
		strQuery.append(
        " SELECT con.ic_if||'|'||con.ic_consecutivo||'|'||con.ic_pyme,"+
        "        'ClausuladoNafinIF::getDocumentQuery()' origen"+
        " FROM com_aceptacion_contrato_if con"+
        " WHERE con.ic_if IS NOT NULL"+
        "   AND con.ic_pyme IS NOT NULL"+
		  condicion);

		System.out.println("strQuery.toString(): "+strQuery.toString());
	
		return strQuery.toString();
 	}  
	  
	
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 
		StringBuffer strQuery = new StringBuffer();
		StringBuffer clavesCombinaciones = new StringBuffer("");
		StringBuffer condicion = new StringBuffer();
		conditions  = new ArrayList();
				
		for (int i = 0; i < pageIds.size(); i++) {
			List lItem = (ArrayList)pageIds.get(i);
			clavesCombinaciones.append("'"+lItem.get(0).toString()+"'"+",");
		}
		clavesCombinaciones.deleteCharAt(clavesCombinaciones.length()-1);
		
		strQuery.append(	
      "  SELECT con.ic_if,"+
      "         con.ic_consecutivo,"+
      "         pym.cg_razon_social nompyme,"+
      "         cif.cg_razon_social nomif,"+
      "         con.ic_usuario,"+
      "         TO_CHAR(con.df_aceptacion, 'dd/mm/yyyy hh24:mi')||' Hrs.' df_aceptacion,"+
      "         'ClausuladoNafinIF::getDocumentSummaryQueryforIds()' origen,"+
      "         cnt.cg_ext_documento extension"+
      "  FROM com_aceptacion_contrato_if con,"+
      "       com_contrato_if cnt,"+
      "       comcat_pyme pym,"+
      "       comcat_if cif"+
      "  WHERE con.ic_if = cif.ic_if"+
      "    AND con.ic_pyme = pym.ic_pyme"+
      "    AND con.ic_if = cnt.ic_if"+
      "    AND con.ic_consecutivo = cnt.ic_consecutivo"+
		//"    AND ");
		"   AND con.ic_if||'|'||con.ic_consecutivo||'|'||con.ic_pyme IN ( " + clavesCombinaciones + " )");
		
		return strQuery.toString();
 	}  
	
	
	public String getDocumentQueryFile(){ // para todos los registros
		conditions = new ArrayList();   
		StringBuffer strQuery = new StringBuffer();
		StringBuffer condicion = new StringBuffer();

		if(!"".equals(ic_if)) {
			condicion.append(" AND con.ic_if = ? ");
			conditions.add(new Integer(ic_if));
		}
    if(!"".equals(ic_consecutivo)) {
			condicion.append(" AND con.ic_consecutivo = ? ");
			conditions.add(new Integer(ic_consecutivo));
		}
		if(!"".equals(ic_pyme)) {
			condicion.append(" AND con.ic_pyme = ? ");
			conditions.add(new Integer(ic_pyme));
		}
		if(!"".equals(txt_usuario)) {
			condicion.append(" AND con.ic_usuario = ? ");
			conditions.add(txt_usuario);
		}
		if(!"".equals(txt_fecha_acep_de)) {
			condicion.append(" AND con.df_aceptacion >= to_date(?, 'dd/mm/yyyy') ");
			conditions.add(txt_fecha_acep_de);
		}
		if(!"".equals(txt_fecha_acep_a)) {
			condicion.append(" AND con.df_aceptacion < to_date(?, 'dd/mm/yyyy') + 1 ");
			conditions.add(txt_fecha_acep_a);
		}

		strQuery.append(
      " SELECT pym.cg_razon_social nompyme,"+
      "        cif.cg_razon_social nomif,"+
      "        con.ic_usuario,"+
      "        TO_CHAR(con.df_aceptacion, 'dd/mm/yyyy hh24:mi')||' Hrs.' df_aceptacion,"+
      "        'ClausuladoNafinIF::getDocumentQueryFile()' origen"+
      " FROM com_aceptacion_contrato_if con,"+
      "       comcat_pyme pym,"+
		"		  comcat_if cif" +
      " WHERE con.ic_if = cif.ic_if"+
      "   AND con.ic_pyme = pym.ic_pyme"+
			condicion);
		
		System.out.println("strQuery.toString(): "+strQuery.toString());
		System.out.println("strQuery.toString(): "+strQuery.toString());
		return strQuery.toString();		
 	} 
	
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
	
		if ("XLS".equals(tipo)) {
			try {
				int registros = 0;
				
				while(rs.next()){
					if(registros==0) {
						contenidoArchivo.append("Nombre PyME,Usuario,Fecha/Hora");
					}
					registros++;
					String rs_nom_pyme 		= rs.getString("nompyme")==null?"":rs.getString("nompyme");
					String rs_usuario 		= rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario");
					String rs_fecha_acep 	= rs.getString("df_aceptacion")==null?"":rs.getString("df_aceptacion");
					contenidoArchivo.append("\n"+
						rs_nom_pyme.replace(',',' ')+","+
						"\t" + rs_usuario+" ,"+
						rs_fecha_acep);
				}
				
				if(registros >= 1){
					if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
						nombreArchivo="ERROR";
					}else{
						nombreArchivo = archivo.nombre;
					}
				}	
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}else if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico") == null ? "" : session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(3, 80);
				pdfDoc.setCell("Nombre PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Usuario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha/Hora","celda01",ComunesPDF.CENTER);
								
				while (rs.next()) {
					String rs_nom_pyme 		= rs.getString("nompyme")==null?"":rs.getString("nompyme");
					String rs_usuario 		= rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario");
					String rs_fecha_acep 	= rs.getString("df_aceptacion")==null?"":rs.getString("df_aceptacion");
							
					pdfDoc.setCell(rs_nom_pyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(rs_usuario,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs_fecha_acep,"formas",ComunesPDF.CENTER);				
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}	
		return nombreArchivo;	
	}
	
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	
	public List getConditions() {
		return conditions;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public void set_ic_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String get_ic_if() {
		return ic_if;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}


	public String getIc_pyme() {
		return ic_pyme;
	}


	public void setTxt_usuario(String txt_usuario) {
		this.txt_usuario = txt_usuario;
	}


	public String getTxt_usuario() {
		return txt_usuario;
	}


	public void setTxt_fecha_acep_de(String txt_fecha_acep_de) {
		this.txt_fecha_acep_de = txt_fecha_acep_de;
	}


	public String getTxt_fecha_acep_de() {
		return txt_fecha_acep_de;
	}


	public void setTxt_fecha_acep_a(String txt_fecha_acep_a) {
		this.txt_fecha_acep_a = txt_fecha_acep_a;
	}


	public String getTxt_fecha_acep_a() {
		return txt_fecha_acep_a;
	}


	public void setNoBancoFondeo(String noBancoFondeo) {
		this.noBancoFondeo = noBancoFondeo;
	}


	public String getNoBancoFondeo() {
		return noBancoFondeo;
	}


	public void setIc_consecutivo(String ic_consecutivo) {
		this.ic_consecutivo = ic_consecutivo;
	}


	public String getIc_consecutivo() {
		return ic_consecutivo;
	}
	
}