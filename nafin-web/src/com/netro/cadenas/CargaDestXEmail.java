package com.netro.cadenas;

import com.netro.exception.NafinException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class CargaDestXEmail {
    
    
    private static final Log LOG = LogFactory.getLog(CargaArchivoCorreosIF.class);
    /**
     *  metodo para leer el arhivo txt
     * @param ruta
     * @param nombreArch
     * @return
     * @throws IOException
     */
    public Map<String, String> leerArchivo(String ruta, String nombreArch) throws IOException {

            LOG.info("leerArchivo(E)");

            int ilinea = 1;
            StringBuilder resultado = new StringBuilder();      
            int erroresTotal =0;        
            Map<String, String> mResultado = new HashMap<String, String>();
            List<String> emailValido = new ArrayList<>();
            BufferedReader br = null;
            try {
                java.io.File lofArchivo = new java.io.File(ruta + nombreArch);
                br = new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
                String msg = "";               
                String linea = "";
                linea = br.readLine();				
				
                do{                    
                    if(linea == null){
                        resultado.append("El archivo no se puede cargar. Error en la l�nea " + ilinea +
                                         ", el valor en el campo [lista de correos electr�nicos] es obligatorio \n");
                         erroresTotal++;                         
                         break;
                    }else if(linea.length() >= 1000){
                        resultado.append("El archivo no se puede cargar. Error en la l�nea " + ilinea +
                                                 ", el valor en el campo [lista de correos electr�nicos] no debe ser mayor a 1000 caracteres \n");
                        erroresTotal++;
                        break;
                    }else{
						
						if(!";".equals(linea.substring(linea.length()-1))) {
							msg = linea+";";
						}else{
							msg = linea;
						}
                        emailValido = obtenerEmail(msg);
                        for(String obj : emailValido){
                            if(!isEmail(obj)){                            
                             resultado.append("Correo electr�nico inv�lido");   
                            erroresTotal++;                           
                            linea = null;
                            break;
                            }
                        }
                    }
                    ilinea++;
                }while(br.readLine() != null);
            
                System.out.println (" erroresTotal ===> "+erroresTotal);           

                          
                
                mResultado.put("resultado", resultado.toString());
                if (erroresTotal>0) {
                    mResultado.put("respuesta", "ConError");
                } else if (erroresTotal==0) {
                    mResultado.put("respuesta", "Exitoso");
                }

            } catch (IOException ioe) {
                LOG.error("IOException:..pos:: " + ioe);
            } catch (Exception e) {
                LOG.error("Exception:..pos:: " + e);
            } finally {
				 br.close();
                LOG.info("leerArchivo(S)");
            }

            return mResultado;
        }

    /**
     * metodo que valida si el correo electronico es valddo 
     * @param correo
     * @return
     */
    public boolean isEmail(String correo) {
        Pattern pat = null;
        Matcher mat = null;
        pat = Pattern.compile("^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(\\.[a-z]{2,4})$");
        mat = pat.matcher(correo);

        if (mat.find()) {
            LOG.debug("[" + mat.group() + "]");
            return true;
        } else {
            return false;
        }
    }

   
    public int guardaArchivo(String ruta, String nombreArch, String strLogin) throws NafinException {
        LOG.info("guardaArchivo  (e) "); 
        AccesoDB con = new AccesoDB();
        int totalLineas = 0;
        StringBuilder qrySentencia = new StringBuilder();    
        PreparedStatement ps = null;
        List varBind = new ArrayList();
		boolean exito = false;
        try {
            con.conexionDB();
              
            String icCarga   = getIcCarga();
            java.io.File lofArchivo = new java.io.File(ruta + nombreArch);
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo)));           
            String linea = "";            
            
            while ((linea = br.readLine()) != null) { 
                System.out.println("******** "+linea);
                qrySentencia = new StringBuilder();
                qrySentencia.append(" INSERT INTO  COM_EMAIL_DESTXEMAIL "+
                                    " (IC_CARGA, CG_EMAILS, DF_ALTA, CG_USUARIO) "+
                                    "  VALUES(?, ?, Sysdate , ?)");                
                varBind = new ArrayList();  
                varBind.add(icCarga );                   
                varBind.add(linea );  
                varBind.add(strLogin );  
                
                LOG.debug ("qrySentencia  "+qrySentencia.toString() +" varBind    \n "+varBind);
                ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
                ps.executeUpdate();
                
                exito = true;
                totalLineas++;
                
            }
        } catch (Exception e) {
            LOG.error("Exception en guardaArchivo. " + e);
            throw new AppException("Error al guardar los correos parametrizados para los Intermediarios  ");
        } finally {
            if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				AccesoDB.cerrarStatement(ps);
                con.cierraConexionDB();
            }
        }
        LOG.info("guardaArchivo  (s) "); 
        return totalLineas;
    }
    
    
    /**
     * @return
     * @throws NafinException
     */
    public String getIcCarga() throws NafinException {
        LOG.info("getIcCarga  (e) "); 
        AccesoDB con = new AccesoDB();
        String icCarga=null;
        StringBuilder query = new StringBuilder();   
		PreparedStatement ps = null;
        ResultSet rs = null;
		
        try{        
            con.conexionDB();
            
            query.append("select COM_EMAIL_DESTXEMAIL_SEQ.NEXTVAL from dual");

            ps = con.queryPrecompilado(query.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                icCarga = rs.getString(1);  
            }
            rs.close();
            ps.close();
                    
        } catch (Exception e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
				AccesoDB.cerrarResultSet(rs);
				AccesoDB.cerrarStatement(ps);
                con.cierraConexionDB();
            }
			
        }
        LOG.info("getIcCarga  (s) "); 
        return icCarga;
    }
        
    /**
     * metodo para descargar el ultimo archivo cargado 
     * @param path
     * @return
     * @throws NafinException
     */
    public String getUltimaCarga(String path ) throws NafinException {
        LOG.info("getUltimaCarga  (e) "); 
        
        AccesoDB con = new AccesoDB();        
        StringBuilder query = new StringBuilder();   

        String nombreArchivo = "";
        StringBuilder contenidoArchivo = new StringBuilder();
        OutputStreamWriter writer = null;
        BufferedWriter buffer = null;
        PreparedStatement  ps = null;        
		ResultSet rs = null;
        try{
            con.conexionDB();
            
            nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
            writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
            buffer = new BufferedWriter(writer);
            contenidoArchivo = new StringBuilder();
                        
            query.append(" SELECT CG_EMAILS  "+
                         " FROM COM_EMAIL_DESTXEMAIL " +
                         " WHERE IC_CARGA = (SELECT MAX(IC_CARGA) FROM COM_EMAIL_DESTXEMAIL )  ");

            ps = con.queryPrecompilado(query.toString());
            rs = ps.executeQuery();
            while (rs.next()) {                
                String emails = (rs.getString("CG_EMAILS") == null) ? "" : rs.getString("CG_EMAILS");
                
                contenidoArchivo.append(emails+"\r\n");
            }
            
            buffer.write(contenidoArchivo.toString());
            
            
        } catch (Exception e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
				AccesoDB.cerrarResultSet(rs);
				AccesoDB.cerrarStatement(ps);
                con.cierraConexionDB();
            }
			try {
				buffer.close();
			} catch (IOException e) {
			}			
        }
        LOG.info("getUltimaCarga  (s) "); 
        return nombreArchivo;
    }
    
     public String getExisteCarga( ) throws NafinException {
        LOG.info("getExisteCarga  (e) "); 
        
        AccesoDB con = new AccesoDB();        
        StringBuilder query = new StringBuilder();  
        String existe = "N";
		PreparedStatement  ps = null;        
		ResultSet rs = null;
                
        try{
            con.conexionDB();                                 
            query.append(" SELECT  count(*) AS TOTAL  FROM COM_EMAIL_DESTXEMAIL " );
            
            ps = con.queryPrecompilado(query.toString());
            rs = ps.executeQuery();
            if (rs.next()) {
                existe = (rs.getInt(1)>0)?"S":"N";                
            }
            rs.close();
            ps.close();           
            
        } catch (Exception e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
				AccesoDB.cerrarResultSet(rs);
				AccesoDB.cerrarStatement(ps);
                con.cierraConexionDB();
            }
			
        }        
        LOG.info("getExisteCarga  (s) "); 
        return existe;
    }   
    
	
	 private static List<String> obtenerEmail(String linea) {
		LOG.info("obtenerEmail (e)");
		StringBuilder correo = new StringBuilder();
		List<String> lis = new ArrayList<>();
		String separador = ";";
		String msg = linea.trim();
		String ms = "";
		
		if(!";".equals(msg.substring(msg.length()-1))) {
			ms = msg+";";
		}else {
			ms = msg;
		}
            
		for(int i = 0; i < ms.length(); i++) {
			if(separador.equals(ms.substring(i, i+1)) || i+1 == ms.length()) {                    
				lis.add(correo.toString());                    
				correo = new StringBuilder();
			}else {
				correo.append(ms.substring(i, i+1));
			}                       
		}
		LOG.info("obtenerEmail (s)");
		return lis;
	}
}
