package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class AfiliaCtasBenef implements IQueryGeneratorRegExtJS  {	
	
	private int numList = 1;
	private String ic_if;
	private String ic_epo;
	private String opcion;
	public static final boolean SIN_COMAS = false;
		
	public AfiliaCtasBenef() {}

	// Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(AfiliaCtasBenef.class);
  
	public  StringBuffer strQuery;
	private List conditions = new ArrayList(); 
	
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() { // Obtiene los totales 
		return "";
 	}  
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */	 
	public String getDocumentQuery(){  // para las llaves primarias		
		return "";		
 	}  
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */	  		
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 			
		return "";
 	}  
	
	public String getDocumentQueryFile(){ // para todos los registros				
		conditions = new ArrayList();   
		StringBuffer strQuery = new StringBuffer();   
		    
		if("1".equals(opcion)){
			strQuery = new StringBuffer();   
			strQuery.append("SELECT cb.ic_cuenta_banc_benef AS cveCuenta, cb.ic_epo AS epo, " +
				"       cb.ic_if AS beneficiario, cb.ic_moneda AS moneda, " +
				"       cb.cg_cuenta_clabe AS cuenta, cb.ic_financiera AS banco, " +
				"       cb.cg_sucursal AS sucursal, NVL(cbi.cs_autoriza_if,'N') AS autorizacion, " +
				"       cf.cd_nombre AS nomBanco, cm.cd_nombre AS nomMoneda, ci.cg_razon_social AS nomBenef " +
				"  FROM comrel_cuenta_banc_benef cb, comrel_cuenta_banc_benef_x_if cbi, "+
				"			comcat_financiera cf, comcat_moneda cm, comcat_if ci  " +
				"	WHERE cb.ic_cuenta_banc_benef = cbi.ic_cuenta_banc_benef(+) " +
				"    AND cb.ic_financiera = cf.ic_financiera(+)  " +
				"	  AND cb.ic_if = ci.ic_if "+
				" 	  AND cb.ic_moneda = cm.ic_moneda " +
				"    AND cb.ic_epo = ? "+
				"	  AND cbi.ic_if(+) = ? " +
				" order by cb.ic_cuenta_banc_benef, cb.ic_if ");  
		}else{
			strQuery = new StringBuffer();   
			strQuery.append("SELECT cb.ic_epo AS epo, cb.ic_if AS beneficiario, cb.ic_moneda AS icmoneda, " +
					"       bc.cg_cuenta_clabe AS cuenta, bc.ic_financiera AS banco, " +
					"       bc.cg_sucursal AS sucursal, bc.cs_autoriza_if AS autoriza, " +
					"       bc.cg_usuario AS usuario, " +
					"       TO_CHAR (bc.df_fec_bit, 'dd/mm/yyyy hh24:mi:ss') AS fecha, " +
					"       cm.cd_nombre AS nommoneda, cf.cd_nombre AS nombanco, " +
					"       ci.cg_razon_social as nomif, bc.ic_cuenta_banc_benef as cveCuenta "+
					" from BIT_CUENTA_BANC_BENEF bc, COMREL_CUENTA_BANC_BENEF cb, "+
					" COMREL_CUENTA_BANC_BENEF_X_IF cbi, comcat_if ci, comcat_financiera cf, comcat_moneda cm "+
					" where cb.ic_cuenta_banc_benef = cbi.ic_cuenta_banc_benef "+
					" and cbi.ic_cuenta_banc_benef = bc.ic_cuenta_banc_benef "+
					" and cbi.ic_beneficiario = ci.ic_if "+
					" and cbi.ic_if = bc.ic_if "+   
					" and cb.ic_financiera = cf.ic_financiera(+) "+
					" and cb.ic_moneda = cm.ic_moneda "+
					" and bc.ic_epo = ? and bc.ic_if = ? "+
					" order by bc.df_fec_bit "); 
		} 
		conditions.add(new Integer(ic_if));      
		conditions.add(new Integer(ic_epo));
		System.out.println("strQuery.toString() " + strQuery.toString());
		return strQuery.toString();
 	}   
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();

			if ("XLS".equals(tipo)) {
				try {
					int registros = 0;	
					while(rs.next()){
						if(registros==0) { 
							contenidoArchivo.append("Nombre del proveedor, Numero del proveedor, Numero de Transferencia, Fecha de pago, Montonto total de la transferencia, Moneda, Fecha de la factura, Monto de la factura, Fecha de operacion");
						}
						registros++;
						String nomproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
							
						contenidoArchivo.append("\n"+
							nomproveedor.replaceAll(",","") + "," +
							numproveedor + "," +
							numtrans + "," +
							fechapago.replaceAll("-","/") + "," +
							"$ " +  Comunes.formatoDecimal(montocheque, 2, false) + "," +
							moneda + "," +
							fechafactura.replaceAll("-","/") + "," +
							"$ " + Comunes.formatoDecimal(montofactura, 2, false) + "," +
							fechaoperacion.replaceAll("-","/") + ",");
					}
					
					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}	 
				} catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}else if ("PDF".equals(tipo)) {
				try {
					HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
							
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.setTable(9, 80);
					pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Numero del Proveedor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Numero de Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Pagado","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Total de la Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operacion","celda01",ComunesPDF.CENTER);
													
					while (rs.next()) {
						String nombreproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numeroproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
						 
						pdfDoc.setCell(nombreproveedor,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(numeroproveedor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numtrans,"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(fechapago,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechafactura,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(fechaoperacion,"formas",ComunesPDF.CENTER);				
					}
					pdfDoc.addTable();
					pdfDoc.endDocument();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}
		return nombreArchivo;	
	}	
	
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public String getIc_if() {
		return ic_if;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setOpcion(String opcion) {
		this.opcion = opcion;
	}
   

	public String getOpcion() {
		return opcion;
	}
}      