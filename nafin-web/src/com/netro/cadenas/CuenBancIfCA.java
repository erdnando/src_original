package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CuenBancIfCA implements IQueryGenerator, IQueryGeneratorRegExtJS{
	  
  public CuenBancIfCA() {
  }

	public String getAggregateCalculationQuery(HttpServletRequest request) {
    StringBuffer qrySentencia = new StringBuffer();
    try{
			qrySentencia.append(" select 1 from dual ");
    }catch(Exception e){
      System.out.println("CuenBanIfCA::getDocumentQueryFileException "+e);
    }
    System.out.println("*************************************\n "+qrySentencia.toString());
    System.out.println("*************************************\n");
    return qrySentencia.toString();
  }

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){
	System.out.println("CuenBanIfCA::getDocumentSummaryQueryForIds(I)");
  int i=0;
  String ses_ic_if = (String)request.getSession().getAttribute("iNoCliente");
	StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append(
      " SELECT  "   +
      "     relpymeif.cs_vobo_if,"   +
      "     relctabanc.ic_cuenta_bancaria,"   +
      "     decode (pyme.cs_habilitado, 'N', '* ', 'S', ' ')||pyme.cg_razon_social,"   +
      "     relctabanc.cg_banco as banco, "   +
      "     relctabanc.cg_numero_cuenta,"   +
      "     m.cd_nombre,"   +
      "     relpymeif.ic_epo,"   +
      "     pyme.ic_pyme,   "   +
      "     relctabanc.cg_sucursal,"   +
      "     epo.cg_razon_social,"   +
      "     pyme.cg_rfc,"   +
      "     NVL(plaza.cd_descripcion, 'no tiene'),    "   +
      "     'CuenBancIfCA::getDocumentSummaryQueryForIds', "   +
      "      UPPER(pyme.cg_razon_social) as pyme,"+ //Posible cambio
//============================================================================>> FODEA 002 - 2009 (I) Se obtiene el numero SIRAC de la pyme
			"      pyme.in_numero_sirac"+
//============================================================================>> FODEA 002 - 2009 (F)
//============================================================================>> FODEA 020 - 2009 (I) Se obtiene 4 valores de Convenio Unico
			" ,    relctabanc.CG_CUENTA_CLABE CLABE " +
			"	,		 TO_CHAR(PYME.DF_FIRMA_CONV_UNICO,'DD/MM/YYYY') FECHA_FIRMA  " +
			" ,    TO_CHAR(RELPYMEIF.DF_PARAM_AUT_CONV_UNICO,'DD/MM/YYYY') FECHA_PARAM " +
			" ,    DECODE(PYME.CS_CONVENIO_UNICO,'S','SI','NO') CONVENIO " +		
//============================================================================>> FODEA 020 - 2009 (F)
      " ,    crn.ic_nafin_electronico nafin_electronico " +//FODEA 018 - 2009 ACF
      ", pyme.CS_EXPEDIENTE_EFILE" + 
      " FROM comrel_cuenta_bancaria relctabanc, "   +
      " comrel_pyme_if relpymeif, "   +
      " comcat_epo epo, "   +
      " comcat_plaza plaza, "   +
      " comrel_pyme_epo pe,"   +
      " comcat_pyme pyme,  "   +
      " comrel_nafin crn,  "   +//FODEA 018 - 2009 ACF
      " comcat_moneda m"   +
      " WHERE relpymeif.ic_cuenta_bancaria = relctabanc.ic_cuenta_bancaria "   +
      " AND pyme.ic_pyme = relctabanc.ic_pyme "   +
      " AND pyme.ic_pyme = crn.ic_epo_pyme_if "   +//FODEA 018 - 2009 ACF
      " AND crn.cg_tipo = 'P' "   +//FODEA 018 - 2009 ACF
      " AND relctabanc.ic_moneda = m.ic_moneda "   +
      " AND relctabanc.ic_plaza = plaza.ic_plaza(+) "   +
      " AND relpymeif.ic_epo = epo.ic_epo"   +
      " AND relpymeif.ic_if = "+ses_ic_if+" "   +
      " AND relpymeif.cs_borrado = 'N' "   +
      " AND pe.ic_pyme = pyme.ic_pyme "   +
      " AND relpymeif.ic_epo = pe.ic_epo "   +
      " AND relpymeif.cs_vobo_if = 'S'"   +
//============================================================================>> FODEA 002 - 2009 (I) Condici�n para seleccionar �nicamente pymes que tienen numero SIRAC
			" AND pyme.in_numero_sirac IS NOT NULL"+
//============================================================================>> FODEA 002 - 2009 (F)
      " AND relpymeif.ic_epo||'-'||relctabanc.ic_cuenta_bancaria in (");
      i=0;
			for (Iterator it = ids.iterator(); it.hasNext();i++){
        if(i>0){
          qrySentencia.append(",");
        }
				qrySentencia.append("'"+it.next().toString()+"'");
			}
			qrySentencia.append(") "+
        " ORDER BY pyme, banco ");
    System.out.println("*************************************\n "+qrySentencia.toString());
    System.out.println("*************************************\n");
		System.out.println("CuenBanIfCA::getDocumentSummaryQueryForIds(F)");
		return qrySentencia.toString();
  }

	public String getDocumentQuery(HttpServletRequest request){
		System.out.println("CuenBanIfCA::getDocumentQuery(I)");
    String ses_ic_if = (String)request.getSession().getAttribute("iNoCliente");
    StringBuffer qrySentencia = new StringBuffer();

    String ic_nafin_electronico = (request.getParameter("ic_nafin_electronico") == null)?"":request.getParameter("ic_nafin_electronico");
    String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
    String ic_epo = (request.getParameter("cboEPO") == null)?"":request.getParameter("cboEPO");

		String chkPymesConvenio = (request.getParameter("chkPymesConvenio") == null)?"":request.getParameter("chkPymesConvenio");
		String fechaparamCU_ini = (request.getParameter("fechaparamCU_ini") == null)?"":request.getParameter("fechaparamCU_ini");
		String fechaparamCU_fin = (request.getParameter("fechaparamCU_fin") == null)?"":request.getParameter("fechaparamCU_fin");
		
		String conExpedienteEfile = (String) request.getAttribute("conExpedienteEfile");
		conExpedienteEfile = (conExpedienteEfile != null && conExpedienteEfile.equals("N"))?"N":"S";
		
    try{
			qrySentencia.append("SELECT "   +
        "     relpymeif.ic_epo||'-'||relctabanc.ic_cuenta_bancaria,"   +
        "      UPPER(pyme.cg_razon_social) as pyme, "   +
        "     relctabanc.cg_banco as banco, "   +
		  "		pyme.cg_rfc                 "  +
        " FROM comrel_cuenta_bancaria relctabanc, "   +
        " comrel_pyme_if relpymeif,"   +
        " comcat_pyme pyme,  "   +
        " comrel_nafin cna"   +
        " WHERE relpymeif.ic_cuenta_bancaria = relctabanc.ic_cuenta_bancaria "   +
        " AND pyme.ic_pyme = relctabanc.ic_pyme "   +
        " AND relpymeif.cs_borrado = 'N' "   +
        " AND relpymeif.cs_vobo_if = 'S'"   +
        " AND cna.cg_tipo = 'P'"   +
        " AND cna.ic_epo_pyme_if = relctabanc.ic_pyme"   +
        " AND relpymeif.ic_if = "+ses_ic_if+" ");

			if (!ic_epo.equals(""))
				qrySentencia.append(" AND relpymeif.ic_epo = "+ic_epo);
			if (!ic_pyme.equals("") && !ic_pyme.equals("0"))
				qrySentencia.append(" AND relctabanc.ic_pyme = "+ic_pyme);
			if (!ic_nafin_electronico.equals(""))
				qrySentencia.append(" AND cna.ic_nafin_electronico = "+ic_nafin_electronico);

			if(chkPymesConvenio.equals("S"))
				qrySentencia.append(" AND PYME.CS_CONVENIO_UNICO = 'S' ");

			if(!fechaparamCU_ini.equals(""))
				qrySentencia.append(" AND relpymeif.df_param_aut_conv_unico  >= TO_DATE ('"+fechaparamCU_ini+"'  , 'DD/MM/YYYY')");				
			if(!fechaparamCU_fin.equals(""))
				qrySentencia.append(" AND relpymeif.df_param_aut_conv_unico  < TO_DATE ('"+fechaparamCU_fin+"' , 'DD/MM/YYYY')+1 ");	 
				
			if (!"S".equals(conExpedienteEfile)){
				qrySentencia.append(" AND PYME.CS_EXPEDIENTE_EFILE = '"+conExpedienteEfile+"' ");
			}
			qrySentencia.append(" ORDER BY pyme, banco ");

    }catch(Exception e){
      System.out.println("CuenBancIfCA::getDocumentQueryException "+e);
    }
    System.out.println("*************************************\n "+qrySentencia.toString());
    System.out.println("*************************************\n");
		System.out.println("CuenBanIfCA::getDocumentQuery(F)");
    return qrySentencia.toString();
  }

	public String getDocumentQueryFile(HttpServletRequest request) {
		System.out.println("CuenBanIfCA::getDocumentQueryFile(I)");
    String ses_ic_if = (String)request.getSession().getAttribute("iNoCliente");
    StringBuffer qrySentencia = new StringBuffer();

    String ic_nafin_electronico = (request.getParameter("ic_nafin_electronico") == null)?"":request.getParameter("ic_nafin_electronico");
    String ic_pyme = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
    String ic_epo = (request.getParameter("cboEPO") == null)?"":request.getParameter("cboEPO");
		String sIfConvenioUnico = (request.getParameter("sIfConvenioUnico") == null)?"":request.getParameter("sIfConvenioUnico");
		String chkPymesConvenio = (request.getParameter("chkPymesConvenio") == null)?"":request.getParameter("chkPymesConvenio");
		String fechaparamCU_ini = (request.getParameter("fechaparamCU_ini") == null)?"":request.getParameter("fechaparamCU_ini");
		String fechaparamCU_fin = (request.getParameter("fechaparamCU_fin") == null)?"":request.getParameter("fechaparamCU_fin");

    try{
			qrySentencia.append(
        " SELECT  "   +
        "     relpymeif.cs_vobo_if,"   +
        "     relctabanc.ic_cuenta_bancaria,"   +
        "     decode (pyme.cs_habilitado, 'N', '* ', 'S', ' ')||pyme.cg_razon_social,"   +
        "     relctabanc.cg_banco,"   +
        "     relctabanc.cg_numero_cuenta,"   +
        "     m.cd_nombre,"   +
        "     relpymeif.ic_epo,"   +
        "     pyme.ic_pyme,   "   +
        "     relctabanc.cg_sucursal,"   +
        "     epo.cg_razon_social,"   +
        "     pyme.cg_rfc,"   +
        "     NVL(plaza.cd_descripcion, 'no tiene'),    "   +
        "     NVL(PE.cg_pyme_epo_interno, 'no tiene') NUMPROVEEDOR,    "   +
				"	  NVL(pyme.IN_NUMERO_SIRAC, ''), " +  //Numero sirac de la pyme
//============================================================================>> FODEA 020 - 2009 (I) Se obtiene 4 valores de Convenio Unico
			"      relctabanc.CG_CUENTA_CLABE CLABE " +
			"	,		 TO_CHAR(PYME.DF_FIRMA_CONV_UNICO,'DD/MM/YYYY') FECHA_FIRMA  " +
			" ,    TO_CHAR(RELPYMEIF.DF_PARAM_AUT_CONV_UNICO,'DD/MM/YYYY') FECHA_PARAM " +
			" ,    DECODE(PYME.CS_CONVENIO_UNICO,'S','SI','NO') CONVENIO, " +		
//============================================================================>> FODEA 020 - 2009 (F)				
        "     crn.ic_nafin_electronico nafin_electronico,"+//FODEA 018 - 2009 ACF
        "     'CuenBancIfCA::getDocumentQueryFile'"+
        " FROM comrel_cuenta_bancaria relctabanc, "   +
        " comrel_pyme_if relpymeif, "   +
        " comrel_nafin cna,"   +
        " comcat_epo epo, "   +
        " comcat_plaza plaza, "   +
        " comrel_pyme_epo pe,"   +
        " comcat_pyme pyme,  "   +
        " comrel_nafin crn,  "   +//FODEA 018 - 2009 ACF
        " comcat_moneda m"   +
        " WHERE relpymeif.ic_cuenta_bancaria = relctabanc.ic_cuenta_bancaria "   +
        " AND pyme.ic_pyme = relctabanc.ic_pyme "   +
        " AND pyme.ic_pyme = crn.ic_epo_pyme_if "   +//FODEA 018 - 2009 ACF
        " AND crn.cg_tipo = 'P' "   +//FODEA 018 - 2009 ACF
        " AND relctabanc.ic_moneda = m.ic_moneda "   +
        " AND relctabanc.ic_plaza = plaza.ic_plaza(+) "   +
        " AND relpymeif.ic_epo = epo.ic_epo"   +
        " AND relpymeif.ic_if = "+ses_ic_if+" "+
        " AND relpymeif.cs_borrado = 'N' "   +
        " AND pe.ic_pyme = pyme.ic_pyme "   +
        " AND cna.cg_tipo = 'P'"   +
        " AND cna.ic_epo_pyme_if = relctabanc.ic_pyme"   +
        " AND relpymeif.ic_epo = pe.ic_epo "   +
        " AND relpymeif.cs_vobo_if = 'S'"+
//============================================================================>> FODEA 002 - 2009 (I) Condici�n para seleccionar �nicamente pymes que tienen numero SIRAC
				" AND pyme.in_numero_sirac IS NOT NULL");
//============================================================================>> FODEA 002 - 2009 (F)
			if (!ic_epo.equals(""))
				qrySentencia.append(" AND relpymeif.ic_epo = "+ic_epo);
			if (!ic_pyme.equals("") && !ic_pyme.equals("0"))
				qrySentencia.append(" AND relctabanc.ic_pyme = "+ic_pyme);
			if (!ic_nafin_electronico.equals(""))
				qrySentencia.append(" AND cna.ic_nafin_electronico = "+ic_nafin_electronico);
				
			if(chkPymesConvenio.equals("S"))
				qrySentencia.append(" AND PYME.CS_CONVENIO_UNICO = 'S' ");

			if(!fechaparamCU_ini.equals(""))
				qrySentencia.append(" AND relpymeif.df_param_aut_conv_unico  >= TO_DATE ('"+fechaparamCU_ini+"'  , 'DD/MM/YYYY')");				
			if(!fechaparamCU_fin.equals(""))
				qrySentencia.append(" AND relpymeif.df_param_aut_conv_unico  < TO_DATE ('"+fechaparamCU_fin+"' , 'DD/MM/YYYY')+1 ");	 
						

		qrySentencia.append(" AND PYME.CS_EXPEDIENTE_EFILE = 'S' ");
			
  		qrySentencia.append(" ORDER BY 3, 4 ");

    }catch(Exception e){
      System.out.println("CambioEstatusIfDE::getDocumentQueryFileException "+e);
    }
    System.out.println("*************************************\n "+qrySentencia.toString());
    System.out.println("*************************************\n");
		System.out.println("CuenBanIfCA::getDocumentQueryFile(F)");
    return qrySentencia.toString();
  }



/***  Metodos para la Migraci�n IF***/


	private final static Log log = ServiceLocator.getInstance().getLog(CuenBancIfCA.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String claveEPO;
	private String clavePyme;
	private String claveIF;
	private String nafinElectronico;
	private String fechaparamCU_ini;
	private String fechaparamCU_fin;
	private String chkPymesConvenio;
	private String conExpedienteEfile;
	private String sIfConvenioUnico;
		
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append("SELECT "   +
        "     relpymeif.ic_epo||'-'||relctabanc.ic_cuenta_bancaria,"   +
        "      UPPER(pyme.cg_razon_social) as pyme, "   +
        "     relctabanc.cg_banco as banco, "   +
		  "		pyme.cg_rfc                 "  +
        " FROM comrel_cuenta_bancaria relctabanc, "   +
        " comrel_pyme_if relpymeif,"   +
        " comcat_pyme pyme,  "   +
        " comrel_nafin cna"   +
        " WHERE relpymeif.ic_cuenta_bancaria = relctabanc.ic_cuenta_bancaria "   +
        " AND pyme.ic_pyme = relctabanc.ic_pyme "   +
        " AND relpymeif.cs_borrado = 'N' "   +
        " AND relpymeif.cs_vobo_if = 'S'"   +
        " AND cna.cg_tipo = 'P'"   +
        " AND cna.ic_epo_pyme_if = relctabanc.ic_pyme"   +
        " AND relpymeif.ic_if =  ?  ");		  
		conditions.add(new Long(claveIF));
		
		if (!claveEPO.equals("")) {
			qrySentencia.append(" AND relpymeif.ic_epo = ? ");
			conditions.add(new Long(claveEPO));
		}
		if (!clavePyme.equals("") ){
			qrySentencia.append(" AND relctabanc.ic_pyme = ? ");
			conditions.add(new Long(clavePyme));
		}
		if (!nafinElectronico.equals("")){
			qrySentencia.append(" AND cna.ic_nafin_electronico = ? ");
			conditions.add(new Long(nafinElectronico));
		}
		if(chkPymesConvenio.equals("S")) {
			qrySentencia.append(" AND PYME.CS_CONVENIO_UNICO = ? ");
			conditions.add("S");
		}
		if(!fechaparamCU_ini.equals("")  &&   !fechaparamCU_fin.equals("") ){ 		
			qrySentencia.append(" AND relpymeif.df_param_aut_conv_unico  >= TO_DATE ( ? , 'DD/MM/YYYY')");
			qrySentencia.append(" AND relpymeif.df_param_aut_conv_unico  < TO_DATE (? , 'DD/MM/YYYY')+1 ");	  
			conditions.add(fechaparamCU_ini);
			conditions.add(fechaparamCU_fin);
		}
		if (!"S".equals(conExpedienteEfile)){
			qrySentencia.append(" AND PYME.CS_EXPEDIENTE_EFILE = ?  ");
			conditions.add(conExpedienteEfile);
		}
			
		qrySentencia.append(" ORDER BY pyme, banco ");
			
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append(
			" SELECT  "   +
			"     relpymeif.cs_vobo_if AS AUTORIZACION_IF ,"   +
			"     relctabanc.ic_cuenta_bancaria,"   +
			"     decode (pyme.cs_habilitado, 'N', '* ', 'S', ' ') || pyme.cg_razon_social  as NOMBRE_PYME,"   +
			"     relctabanc.cg_banco as BANCO_SERVICIO, "   +
			"     relctabanc.cg_numero_cuenta as CUENTA,"   +
			"     m.cd_nombre as MONEDA,"   +
			"     relpymeif.ic_epo as CLAVE_EPO,"   +
			"     pyme.ic_pyme as CLAVE_PYME ,   "   +
			"     relctabanc.cg_sucursal  as SUCURSAL,"   +
			"     epo.cg_razon_social as NOMBRE_EPO,"   +
			"     pyme.cg_rfc as RFC ,"   +
			"     NVL(plaza.cd_descripcion, 'no tiene')  AS PLAZA ,    "   +
			"     'CuenBancIfCA::getDocumentSummaryQueryForIds', "   +
			"      UPPER(pyme.cg_razon_social) as NOMBRE_PYME,"+ //Posible cambio
			"      pyme.in_numero_sirac AS NUM_SIRAC  "+
			" ,    relctabanc.CG_CUENTA_CLABE AS CUENTA_CLABE  " +
			"	,		 TO_CHAR(PYME.DF_FIRMA_CONV_UNICO,'DD/MM/YYYY') AS FECHA_FIRMA_CU  " +
			" ,    TO_CHAR(RELPYMEIF.DF_PARAM_AUT_CONV_UNICO,'DD/MM/YYYY') AS FECHA_PARAMETRIZACION_CU " +
			" ,    DECODE(PYME.CS_CONVENIO_UNICO,'S','SI','NO') OPERA_CONVENIO_UNICO " +		
	      " ,    crn.ic_nafin_electronico  AS  NAFIN_ELECTRONICO " +//FODEA 018 - 2009 ACF
		   ", pyme.CS_EXPEDIENTE_EFILE AS CS_EXPEDIENTE_EFILE" + 
			" FROM comrel_cuenta_bancaria relctabanc, "   +
			" comrel_pyme_if relpymeif, "   +
			" comcat_epo epo, "   +
			" comcat_plaza plaza, "   +
			" comrel_pyme_epo pe,"   +
			" comcat_pyme pyme,  "   +
			" comrel_nafin crn,  "   +//FODEA 018 - 2009 ACF
			" comcat_moneda m"   +
			" WHERE relpymeif.ic_cuenta_bancaria = relctabanc.ic_cuenta_bancaria "   +
			" AND pyme.ic_pyme = relctabanc.ic_pyme "   +
			" AND pyme.ic_pyme = crn.ic_epo_pyme_if "   +//FODEA 018 - 2009 ACF
			" AND crn.cg_tipo = 'P' "   +//FODEA 018 - 2009 ACF
			" AND relctabanc.ic_moneda = m.ic_moneda "   +
			" AND relctabanc.ic_plaza = plaza.ic_plaza(+) "   +
			" AND relpymeif.ic_epo = epo.ic_epo"   +
			" AND relpymeif.ic_if = ? "   +
			" AND relpymeif.cs_borrado = 'N' "   +
			" AND pe.ic_pyme = pyme.ic_pyme "   +
			" AND relpymeif.ic_epo = pe.ic_epo "   +
			" AND relpymeif.cs_vobo_if = 'S'"   +
			" AND pyme.in_numero_sirac IS NOT NULL");
			conditions.add(new Long(claveIF));
			
			
			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					qrySentencia.append(" AND relpymeif.ic_epo||'-'||relctabanc.ic_cuenta_bancaria in ( ");
				}
				qrySentencia.append("?");
				conditions.add( lItem.get(0).toString() );					
				if(i!=(pageIds.size()-1)) {
					qrySentencia.append(",");
				} else {
					qrySentencia.append(" ) ");
				}			
			}
	     qrySentencia.append(" ORDER BY pyme.cg_razon_social, relctabanc.cg_banco ");
		  
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(
			" SELECT     relpymeif.cs_vobo_if AS AUTORIZACION_IF ,"   +
			"     relctabanc.ic_cuenta_bancaria,"   +
			"     decode (pyme.cs_habilitado, 'N', '* ', 'S', ' ') || pyme.cg_razon_social  as NOMBRE_PYME,"   +
			"     relctabanc.cg_banco as BANCO_SERVICIO, "   +
			"     relctabanc.cg_numero_cuenta as CUENTA,"   +
			"     m.cd_nombre as MONEDA,"   +
			"     relpymeif.ic_epo as CLAVE_EPO,"   +
			"     pyme.ic_pyme as CLAVE_PYME ,   "   +
			"     relctabanc.cg_sucursal  as SUCURSAL,"   +
			"     epo.cg_razon_social as NOMBRE_EPO,"   +
			"     pyme.cg_rfc as RFC ,"   +
			"     NVL(plaza.cd_descripcion, 'no tiene')  AS PLAZA ,    "   +
			"     'CuenBancIfCA::getDocumentSummaryQueryForIds', "   +
			"      UPPER(pyme.cg_razon_social) as NOMBRE_PYME,"+ //Posible cambio
			"      pyme.in_numero_sirac AS NUM_SIRAC  "+
			" ,    relctabanc.CG_CUENTA_CLABE AS CUENTA_CLABE  " +
			"	,		 TO_CHAR(PYME.DF_FIRMA_CONV_UNICO,'DD/MM/YYYY') AS FECHA_FIRMA_CU  " +
			" ,    TO_CHAR(RELPYMEIF.DF_PARAM_AUT_CONV_UNICO,'DD/MM/YYYY') AS FECHA_PARAMETRIZACION_CU " +
			" ,    DECODE(PYME.CS_CONVENIO_UNICO,'S','SI','NO') OPERA_CONVENIO_UNICO " +		
	      " ,    crn.ic_nafin_electronico  AS  NAFIN_ELECTRONICO " +//FODEA 018 - 2009 ACF
		   ", pyme.CS_EXPEDIENTE_EFILE" + 
        " FROM comrel_cuenta_bancaria relctabanc, "   +
        " comrel_pyme_if relpymeif, "   +
        " comrel_nafin cna,"   +
        " comcat_epo epo, "   +
        " comcat_plaza plaza, "   +
        " comrel_pyme_epo pe,"   +
        " comcat_pyme pyme,  "   +
        " comrel_nafin crn,  "   +//FODEA 018 - 2009 ACF
        " comcat_moneda m"   +
        " WHERE relpymeif.ic_cuenta_bancaria = relctabanc.ic_cuenta_bancaria "   +
        " AND pyme.ic_pyme = relctabanc.ic_pyme "   +
        " AND pyme.ic_pyme = crn.ic_epo_pyme_if "   +//FODEA 018 - 2009 ACF
        " AND crn.cg_tipo = 'P' "   +//FODEA 018 - 2009 ACF
        " AND relctabanc.ic_moneda = m.ic_moneda "   +
        " AND relctabanc.ic_plaza = plaza.ic_plaza(+) "   +
        " AND relpymeif.ic_epo = epo.ic_epo"   +
        " AND relpymeif.ic_if = ? "+
        " AND relpymeif.cs_borrado = 'N' "   +
        " AND pe.ic_pyme = pyme.ic_pyme "   +
        " AND cna.cg_tipo = 'P'"   +
        " AND cna.ic_epo_pyme_if = relctabanc.ic_pyme"   +
        " AND relpymeif.ic_epo = pe.ic_epo "   +
        " AND relpymeif.cs_vobo_if = 'S'"+	  
  		  " AND pyme.in_numero_sirac IS NOT NULL");
			conditions.add(new Long(claveIF));
			
			if (!claveEPO.equals("")){
				qrySentencia.append(" AND relpymeif.ic_epo = ? ");
				conditions.add(new Long(claveEPO));
			}
			if (!clavePyme.equals("")){
				qrySentencia.append(" AND relctabanc.ic_pyme =  ? ");
				conditions.add(new Long(clavePyme));
			}
			if (!nafinElectronico.equals("")){
				qrySentencia.append(" AND cna.ic_nafin_electronico = ? ");
				conditions.add(new Long(nafinElectronico));
			}
				
			if(chkPymesConvenio.equals("S")) {
				qrySentencia.append(" AND PYME.CS_CONVENIO_UNICO = 'S' ");
			}
			
			if(!fechaparamCU_ini.equals("")  &&   !fechaparamCU_fin.equals("") ){ 		
			qrySentencia.append(" AND relpymeif.df_param_aut_conv_unico  >= TO_DATE ( ? , 'DD/MM/YYYY')");
			qrySentencia.append(" AND relpymeif.df_param_aut_conv_unico  < TO_DATE (? , 'DD/MM/YYYY')+1 ");	  
			conditions.add(fechaparamCU_ini);
			conditions.add(fechaparamCU_fin);
		}

		qrySentencia.append(" AND PYME.CS_EXPEDIENTE_EFILE = 'S' ");
			
  		qrySentencia.append(" ORDER BY 3, 4 ");
		
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();	
		String num_sirac ="", nombre_pyme ="", moneda ="", banco_servicio ="",cuenta ="", cuenta_clabe ="",
						sucursal ="", nombre_epo ="",rfc="", plaza ="", autorizacion_if="", operaConvenioUnico ="",
						fecha_firma_cu ="", fecha_para_cu ="",nafin_Electronico ="";		
		try {
		  
			if(tipo.equals("CSV") ) {
			
				contenidoArchivo.append("N�mero SIRAC, PyME, Moneda, Banco de servicio, No. de Cuenta,");
				if(sIfConvenioUnico.equals("S")) {  contenidoArchivo.append("Cuenta Clabe,");  }				
				contenidoArchivo.append("Sucursal, EPO, RFC, Plaza,  Autorizaci�n IF,");
				if(sIfConvenioUnico.equals("S")) {  
					contenidoArchivo.append("	Pyme Opera con Convenio Unico, 	Fecha de Firma Autografa de CU., Fecha Parametrizacion Automatica CU,");
				}
				contenidoArchivo.append("Num. Nafin Electr�nico \n" ); 
				
				while (rs.next())	{					
							
					num_sirac = (rs.getString("NUM_SIRAC") == null) ? "" : rs.getString("NUM_SIRAC");
					nombre_pyme = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
					moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					banco_servicio = (rs.getString("BANCO_SERVICIO") == null) ? "" : rs.getString("BANCO_SERVICIO");
					cuenta = (rs.getString("CUENTA") == null) ? "" : rs.getString("CUENTA");
					cuenta_clabe = (rs.getString("CUENTA_CLABE") == null) ? "" : rs.getString("CUENTA_CLABE");
					sucursal = (rs.getString("SUCURSAL") == null) ? "" : rs.getString("SUCURSAL");
					nombre_epo = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
					plaza = (rs.getString("PLAZA") == null) ? "" : rs.getString("PLAZA");
					autorizacion_if = (rs.getString("AUTORIZACION_IF") == null) ? "" : rs.getString("AUTORIZACION_IF");
					operaConvenioUnico = (rs.getString("OPERA_CONVENIO_UNICO") == null) ? "" : rs.getString("OPERA_CONVENIO_UNICO");
					fecha_firma_cu = (rs.getString("FECHA_FIRMA_CU") == null) ? "" : rs.getString("FECHA_FIRMA_CU");
					fecha_para_cu = (rs.getString("FECHA_PARAMETRIZACION_CU") == null) ? "" : rs.getString("FECHA_PARAMETRIZACION_CU");
					nafin_Electronico = (rs.getString("NAFIN_ELECTRONICO") == null) ? "" : rs.getString("NAFIN_ELECTRONICO");
									
					contenidoArchivo.append(					
						num_sirac.replaceAll(",","")+","+
						nombre_pyme.replaceAll(",","")+","+
						moneda.replaceAll(",","")+","+
						banco_servicio.replaceAll(",","")+","+
						cuenta.replaceAll(",","")+",");
					 
					 if(sIfConvenioUnico.equals("S")) {  
							if(!cuenta_clabe.equals("")) { 
								contenidoArchivo.append("'"+cuenta_clabe.replaceAll(",","")+"',"); 
							}else  {
								contenidoArchivo.append(",");
							}
					 }
					 
					contenidoArchivo.append(sucursal.replaceAll(",","")+","+
					 nombre_epo.replaceAll(",","")+","+
					 rfc.replaceAll(",","")+","+					 
					 plaza.replaceAll(",","")+",");
					if(sIfConvenioUnico.equals("S")) {  
						contenidoArchivo.append(autorizacion_if.replaceAll(",","")+","+
						 operaConvenioUnico.replaceAll(",","")+","+
						 fecha_firma_cu.replaceAll(",","")+","+
						 fecha_para_cu.replaceAll(",","")+",");
					}
					contenidoArchivo.append(nafin_Electronico.replaceAll(",","")+"\n");
								
				}
				
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getClaveEPO() {
		return claveEPO;
	}

	public void setClaveEPO(String claveEPO) {
		this.claveEPO = claveEPO;
	}

	public String getClavePyme() {
		return clavePyme;
	}

	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}

	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	public String getNafinElectronico() {
		return nafinElectronico;
	}

	public void setNafinElectronico(String nafinElectronico) {
		this.nafinElectronico = nafinElectronico;
	}

	public String getFechaparamCU_ini() {
		return fechaparamCU_ini;
	}

	public void setFechaparamCU_ini(String fechaparamCU_ini) {
		this.fechaparamCU_ini = fechaparamCU_ini;
	}

	public String getFechaparamCU_fin() {
		return fechaparamCU_fin;
	}

	public void setFechaparamCU_fin(String fechaparamCU_fin) {
		this.fechaparamCU_fin = fechaparamCU_fin;
	}

	public String getChkPymesConvenio() {
		return chkPymesConvenio;
	}

	public void setChkPymesConvenio(String chkPymesConvenio) {
		this.chkPymesConvenio = chkPymesConvenio;
	}

	public String getConExpedienteEfile() {
		return conExpedienteEfile;
	}

	public void setConExpedienteEfile(String conExpedienteEfile) {
		this.conExpedienteEfile = conExpedienteEfile;
	}

	public String getSIfConvenioUnico() {
		return sIfConvenioUnico;
	}

	public void setSIfConvenioUnico(String sIfConvenioUnico) {
		this.sIfConvenioUnico = sIfConvenioUnico;
	}
	
	
}