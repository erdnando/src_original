package com.netro.cadenas;

import com.netro.model.catalogos.CatalogoMoneda;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CuentasClabeSwiftCA implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	public CuentasClabeSwiftCA() {}
	
	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(com.netro.cadenas.CuentasClabeSwiftCA.class);
	
	StringBuffer 	qrySentencia;
	private List 	conditions;
	private String operacion;
	private String paginaOffset;
	private String paginaNo;

	private String no_producto 			= "";
	private String no_nafin    			= "";	
	private String fecha_registro_de 	= "";
   private String fecha_registro_a 		= "";
   private String cuentas_sin_validar 	= "";
	private String cmb_tipo_afiliado 	= "";
	private String no_epo 					= "";
	private String no_if 					= "";
	private String rfc 						= "";
	private List 	listaUsuariosIF;
	
	/**
	 * M�todo que obtiene los totales de la consulta
	 * @return ""
	 */
	public String getAggregateCalculationQuery() {
		return "";
	}//getAggregateCalculationQuery
	
	/**
	 * M�todo que obtiene las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL que contiene las llaves primarias
	 */
	public String getDocumentQuery(){
		
		log.info("getDocumentQuery (E)");
		qrySentencia 		= new StringBuffer();
		conditions 			= new ArrayList();
		
		qrySentencia.append(
			" SELECT                                                        "   +
			"        c.ic_cuenta  AS ic_cuenta                              "   + 
			"   FROM com_cuentas               c,                           "   +
			"        comrel_nafin              crn,                         "   +
			"        comcat_pyme               pym,                         "   +
			"        comcat_epo                epo,                         "   +
			"        comcat_bancos_tef         bt,                          "   +
			"        comcat_estatus_cecoban    cec,                         "   +
			"        comcat_estatus_tef        tef,                         "   +
			"        comcat_producto_nafin     cpn                          "   +
			"  WHERE c.ic_nafin_electronico   =  crn.ic_nafin_electronico   "   +
			"    AND c.ic_bancos_tef          =  bt.ic_bancos_tef           "   +
			"    AND c.ic_estatus_tef         =  tef.ic_estatus_tef (+)     "   +
			"    AND c.ic_estatus_cecoban     =  cec.ic_estatus_cecoban (+) "   +
			"    AND c.ic_producto_nafin      =  cpn.ic_producto_nafin      "   +
			"    AND c.ic_epo                 =  epo.ic_epo                 "   +
			"    AND crn.ic_epo_pyme_if       =  pym.ic_pyme                "   +
			"    AND c.ic_tipo_cuenta         != ?                          "   +
			"    AND crn.cg_tipo              =  ?                          ");
		
		conditions.add("1");
		conditions.add("P");
		
		if(!"".equals(no_producto)) {
			qrySentencia.append("    AND c.ic_producto_nafin = ? "); 
			conditions.add(no_producto);
		}
		if(!"".equals(no_nafin)) {
			qrySentencia.append("    AND c.ic_nafin_electronico = ? ");
			conditions.add(no_nafin);
		}
		if(!"".equals(fecha_registro_de)) {
			qrySentencia.append("    AND c.df_registro >= TO_DATE(?, 'dd/mm/yyyy') ");
			conditions.add(fecha_registro_de);
		}
		if(!"".equals(fecha_registro_a)) {
			qrySentencia.append("    AND c.df_registro < (TO_DATE(?, 'dd/mm/yyyy')+1) ");
			conditions.add(fecha_registro_a);
		}
		if("S".equals(cuentas_sin_validar)) {
			qrySentencia.append("    AND c.ic_estatus_cecoban IS NULL ");
		}
		if(!"".equals(cmb_tipo_afiliado)) {
			qrySentencia.append("    AND c.cg_tipo_afiliado = ? ");
			conditions.add(cmb_tipo_afiliado);
		}
		if(!"".equals(no_epo)) {
			qrySentencia.append("    AND c.ic_epo = ? ");
			conditions.add(no_epo);
		}
		if(!"".equals(rfc)) {
			qrySentencia.append("    AND pym.cg_rfc = ? ");
			conditions.add(rfc);
		}
		if(listaUsuariosIF != null && listaUsuariosIF.size() > 0){
			qrySentencia.append(" AND C.IC_USUARIO IN ( ");
			for(int i=0;i<listaUsuariosIF.size();i++){
				if(i>0) qrySentencia.append(",");
				qrySentencia.append("?");
				conditions.add((String)listaUsuariosIF.get(i));
			}
			qrySentencia.append(" )");
		}
		
		if(!"".equals(no_if)) {
			qrySentencia.append(
					" AND pym.ic_pyme in ( select "  +
					"	      distinct "  +
					"		      cuenta.ic_pyme "  + 
					"     from  "  +
					"	      comrel_pyme_if rel, "  +
					"	      comrel_cuenta_bancaria cuenta "  +
					"     where "  +
					"	      rel.cs_vobo_if = 'S' and "  + 
					"	      rel.cs_borrado = 'N' and "  +
					"	      rel.ic_if      =  ?  and "  +
					"	      rel.ic_cuenta_bancaria = cuenta.ic_cuenta_bancaria and "  +
					"	cuenta.cs_borrado = 'N' ) ");
			conditions.add(no_if);
		}
		
		qrySentencia.append(" ORDER BY pym.cg_razon_social, bt.cd_descripcion  ASC ");
		
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions:   "+conditions);
		
		log.info("getDocumentQuery (S)");
		
		return qrySentencia.toString();
		
	}//getDocumentQuery
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL que servira para obtener la informaci�n
	 * 	completa de los registros con las llaves primarias especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		
		log.info("getDocumentSummaryQueryForIds (E)");
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append( 
			" SELECT                                                                      "   +
			"        c.ic_cuenta                                   AS ic_cuenta,          "   + 
			"        c.cg_tipo_afiliado                            AS tipoafiliado,       "   +
			"        pym.cg_razon_social                           AS nombre,             "   +
			"        bt.cd_descripcion                             AS banco,              "   +
			"        DECODE (c.ic_tipo_cuenta, 40, c.cg_cuenta)    AS cuentaclabe,        "   +
			"        DECODE (c.ic_tipo_cuenta, 1, c.cg_cuenta)     AS cuentaspeua,        "   +
			"        tef.cd_descripcion                            AS estatus_tef,        "   +
			"        DECODE(c.ic_moneda,54,'',cec.cd_descripcion)  AS estatus_cecoban,    "   +
			"        TO_CHAR (c.df_ultima_mod, 'dd-MM-yyyy hh:mm') AS fecha_ult_mod,      "   +
			"        c.ic_usuario                                  AS usuario,            "   +
			"        epo.cg_razon_social                           AS eporel,             "   +
			"        cpn.ic_nombre                                 AS producto,           "   +
			"        c.ic_estatus_cecoban                          AS ic_estatus_cecoban, "   +
			"        c.ic_epo                                      AS ic_epo_rel,         "   +
			"			DECODE (c.ic_tipo_cuenta, 50, c.cg_cuenta)    AS cuentaswift, 		   "   +
			"			c.ic_moneda                                   AS tipo_moneda, 		   "   +
			"			DECODE(c.ic_moneda,54,cec.cd_descripcion,'')  AS estatus_dolares, 	"   +
			"			c.ic_estatus_cecoban                          AS estatus_cuenta,     "   +
			"        DECODE (c.ic_tipo_cuenta,1,'true','false')    AS es_cuenta_speua,    "   +
			"        DECODE (c.ic_moneda,54,'true','false') 	    AS es_cuenta_dolares,   	  "   +
			"			DECODE (c.ic_tipo_cuenta, 50, c.cg_plaza_swift,   '') AS plaza_swift, 	  "   +
			"			DECODE (c.ic_tipo_cuenta, 50, c.cg_sucursal_swift,'') AS sucursal_swift,  "   +
			"			bt.ic_bancos_tef                              AS ic_bancos_tef,           "   +
			" 			cpn.ic_producto_nafin 								 AS ic_producto_nafin,       "   +
			"			c.ic_nafin_electronico                        AS ic_nafin_electronico,    "   +
			"			pym.cg_rfc												 AS pyme_rfc,                "   +
			"			c.ic_tipo_cuenta 										 AS tipo_cuenta,             "   +
			"        TO_CHAR (c.df_ultima_mod, 'dd-MM-yyyy hh:mm:ss') AS fecha_ult_modSS       "   +
			"   FROM com_cuentas               c,   "   +
			"        comrel_nafin              crn, "   +
			"        comcat_pyme               pym, "   +
			"        comcat_epo                epo, "   +
			"        comcat_bancos_tef         bt,  "   +
			"        comcat_estatus_cecoban    cec, "   +
			"        comcat_estatus_tef        tef, "   +
			"        comcat_producto_nafin     cpn  "   +
			"  WHERE c.ic_nafin_electronico   =  crn.ic_nafin_electronico   "   +
			"    AND c.ic_bancos_tef          =  bt.ic_bancos_tef           "   +
			"    AND c.ic_estatus_tef         =  tef.ic_estatus_tef (+)     "   +
			"    AND c.ic_estatus_cecoban     =  cec.ic_estatus_cecoban (+) "   +
			"    AND c.ic_producto_nafin      =  cpn.ic_producto_nafin      "   +
			"    AND c.ic_epo                 =  epo.ic_epo                 "   +
			"    AND crn.ic_epo_pyme_if       =  pym.ic_pyme                "   +
			"    AND c.ic_tipo_cuenta         != ?                          "   +
			"    AND crn.cg_tipo              =  ?                          "   +
			"    AND c.ic_cuenta              in ( ");
		
		conditions.add("1");
		conditions.add("P");
		
		for(int i=0;i<pageIds.size();i++) {
			
			List lItem = (ArrayList)pageIds.get(i);
			
			if(i>0) qrySentencia.append(",");
			qrySentencia.append(	"?" ); 
			
			conditions.add(lItem.get(0).toString());
					
		}
		
		qrySentencia.append(" )");
				
		log.debug("qrySentencia: "+qrySentencia );
		log.debug("conditions:   "+conditions   );
		
		log.info("getDocumentSummaryQueryForIds (S)");
		
		return qrySentencia.toString();
	}	
 
	public String getDocumentQueryFile(){
		
		log.info("getDocumentQueryFile (E)");
		
		qrySentencia 		= new StringBuffer();
		conditions 			= new ArrayList();
			
		qrySentencia.append(
			" SELECT                                                                      "   +
			"        c.ic_cuenta                                   AS ic_cuenta,          "   + 
			"        c.cg_tipo_afiliado                            AS tipoafiliado,       "   +
			"        pym.cg_razon_social                           AS nombre,             "   +
			"        bt.cd_descripcion                             AS banco,              "   +
			"        DECODE (c.ic_tipo_cuenta, 40, c.cg_cuenta)    AS cuentaclabe,        "   +
			"        DECODE (c.ic_tipo_cuenta, 1, c.cg_cuenta)     AS cuentaspeua,        "   +
			"        tef.cd_descripcion                            AS estatus_tef,        "   +
			"        DECODE(c.ic_moneda,54,'',cec.cd_descripcion)  AS estatus_cecoban,    "   +
			"        TO_CHAR (c.df_ultima_mod, 'dd-MM-yyyy hh:mm') AS fecha_ult_mod,      "   +
			"        c.ic_usuario                                  AS usuario,            "   +
			"        epo.cg_razon_social                           AS eporel,             "   +
			"        cpn.ic_nombre                                 AS producto,           "   +
			"        c.ic_estatus_cecoban                          AS ic_estatus_cecoban, "   +
			"        c.ic_epo                                      AS ic_epo_rel,         "   +
			"			DECODE (c.ic_tipo_cuenta, 50, c.cg_cuenta)    AS cuentaswift, 		   "   +
			"			c.ic_moneda                                   AS tipo_moneda, 		   "   +
			"			DECODE(c.ic_moneda,54,cec.cd_descripcion,'')  AS estatus_dolares, 	"   +
			"			c.ic_estatus_cecoban                          AS estatus_cuenta,     "   +
			"        DECODE (c.ic_tipo_cuenta,1,'true','false')    AS es_cuenta_speua,    "   +
			"        DECODE (c.ic_moneda,54,'true','false') 	    AS es_cuenta_dolares,       "   +
			"			DECODE (c.ic_tipo_cuenta, 50, c.cg_plaza_swift,   '') AS plaza_swift, 	  "   +
			"			DECODE (c.ic_tipo_cuenta, 50, c.cg_sucursal_swift,'') AS sucursal_swift,  "   +
			"        TO_CHAR (c.df_ultima_mod, 'dd-MM-yyyy hh:mm:ss') AS fecha_ult_modSS       "   +
			"   FROM com_cuentas               c,                           "   +
			"        comrel_nafin              crn,                         "   +
			"        comcat_pyme               pym,                         "   +
			"        comcat_epo                epo,                         "   +
			"        comcat_bancos_tef         bt,                          "   +
			"        comcat_estatus_cecoban    cec,                         "   +
			"        comcat_estatus_tef        tef,                         "   +
			"        comcat_producto_nafin     cpn                          "   +
			"  WHERE c.ic_nafin_electronico   =  crn.ic_nafin_electronico   "   +
			"    AND c.ic_bancos_tef          =  bt.ic_bancos_tef           "   +
			"    AND c.ic_estatus_tef         =  tef.ic_estatus_tef (+)     "   +
			"    AND c.ic_estatus_cecoban     =  cec.ic_estatus_cecoban (+) "   +
			"    AND c.ic_producto_nafin      =  cpn.ic_producto_nafin      "   +
			"    AND c.ic_epo                 =  epo.ic_epo                 "   +
			"    AND crn.ic_epo_pyme_if       =  pym.ic_pyme                "   +
			"    AND c.ic_tipo_cuenta         != ?                          "   +
			"    AND crn.cg_tipo              =  ?                          ");
		
		conditions.add("1");
		conditions.add("P");
		
		if(!"".equals(no_producto)) {
			qrySentencia.append("    AND c.ic_producto_nafin = ? "); 
			conditions.add(no_producto);
		}
		if(!"".equals(no_nafin)) {
			qrySentencia.append("    AND c.ic_nafin_electronico = ? ");
			conditions.add(no_nafin);
		}
		if(!"".equals(fecha_registro_de)) {
			qrySentencia.append("    AND c.df_registro >= TO_DATE(?, 'dd/mm/yyyy') ");
			conditions.add(fecha_registro_de);
		}
		if(!"".equals(fecha_registro_a)) {
			qrySentencia.append("    AND c.df_registro < (TO_DATE(?, 'dd/mm/yyyy')+1) ");
			conditions.add(fecha_registro_a);
		}
		if("S".equals(cuentas_sin_validar)) {
			qrySentencia.append("    AND c.ic_estatus_cecoban IS NULL ");
		}
		if(!"".equals(cmb_tipo_afiliado)) {
			qrySentencia.append("    AND c.cg_tipo_afiliado = ? ");
			conditions.add(cmb_tipo_afiliado);
		}
		if(!"".equals(no_epo)) {
			qrySentencia.append("    AND c.ic_epo = ? ");
			conditions.add(no_epo);
		}
		if(!"".equals(rfc)) {
			qrySentencia.append("    AND pym.cg_rfc = ? ");
			conditions.add(rfc);
		}
		if(listaUsuariosIF != null && listaUsuariosIF.size() > 0){
			qrySentencia.append(" AND C.IC_USUARIO IN ( ");
			for(int i=0;i<listaUsuariosIF.size();i++){
				if(i>0) qrySentencia.append(",");
				qrySentencia.append("?");
				conditions.add((String)listaUsuariosIF.get(i));
			}
			qrySentencia.append(" )");
		}
		
		if(!"".equals(no_if)) {
			qrySentencia.append(
					" AND pym.ic_pyme in ( select "  +
					"	      distinct "  +
					"		      cuenta.ic_pyme "  + 
					"     from  "  +
					"	      comrel_pyme_if rel, "  +
					"	      comrel_cuenta_bancaria cuenta "  +
					"     where "  +
					"	      rel.cs_vobo_if = 'S' and "  + 
					"	      rel.cs_borrado = 'N' and "  +
					"	      rel.ic_if      =  ?  and "  +
					"	      rel.ic_cuenta_bancaria = cuenta.ic_cuenta_bancaria and "  +
					"	cuenta.cs_borrado = 'N' ) ");
			conditions.add(no_if);
		}
		
		qrySentencia.append(" ORDER BY pym.cg_razon_social, bt.cd_descripcion  ASC ");
		
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions:   "+conditions);
		
		return qrySentencia.toString();
		
	}
  
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
 
		ComunesPDF 				pdfDoc 			= null;
		String 					nombreArchivo 	= null;
		
		if( "PDF".equals(tipo) ){
			
			try{
	 
				// Leer catalogo de monedas
				CatalogoMoneda cat = new CatalogoMoneda();
				cat.setCampoClave("ic_moneda");
				cat.setCampoDescripcion("cd_nombre");
				cat.setOrden("1");
				JSONArray catalogoMoneda = cat.getJSONArray();
				
				// 1. Definir nombre y ruta del Archivo PDF
				nombreArchivo 			= Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc 					= new ComunesPDF(2, path + nombreArchivo);
					
				// 2. Crear cabecera
				String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				HttpSession session 	= request.getSession();
				pdfDoc.encabezadoConImagenes(
					pdfDoc,
					(String) session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)	session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)	session.getAttribute("strLogo"),
					(String) session.getServletContext().getAttribute("strDirectorioPublicacion")
				);
						 
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
					
				// 3. Definir cabecera de la tabla del reporte
				pdfDoc.setTable(15,100);
				pdfDoc.setCell("Tipo de Afiliado",								"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Producto",											"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre",											"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Banco",												"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta Clabe",									"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta Swift",									"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Plaza Swift",										"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Sucursal Swift",									"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Moneda",									"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("EPO",												"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Descripci�n TEF",								"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Descripci�n CECOBAN",							"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Descripci�n D�LARES",							"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha y hora de �ltima actualizaci�n",	"celda01rep", ComunesPDF.CENTER);
				pdfDoc.setCell("Usuario",											"celda01rep", ComunesPDF.CENTER);
 
				//4. Agregar los registros correspondientes a la consulta
				while (reg.next()) {
					
					String tipoAfiliado 					= reg.getString("TIPOAFILIADO");
					String producto 						= reg.getString("PRODUCTO");
					String nombre 							= reg.getString("NOMBRE");
					String banco 							= reg.getString("BANCO");
					String cuentaClabe 					= reg.getString("CUENTACLABE");
					String cuentaSwift 					= reg.getString("CUENTASWIFT");
					String plazaSwift 					= reg.getString("PLAZA_SWIFT");
					String sucursalSwift 				= reg.getString("SUCURSAL_SWIFT");
					String moneda 							= reg.getString("TIPO_MONEDA");
					String nombreEpo 						= reg.getString("EPOREL");
					String descripcionTef 				= reg.getString("ESTATUS_TEF");
					String descripcionCecoban 			= reg.getString("ESTATUS_CECOBAN");
					String descripcionDolares 			= reg.getString("ESTATUS_DOLARES");
					String fechaUltimaActualizacion 	= reg.getString("FECHA_ULT_MODSS");
					String usuario 						= reg.getString("USUARIO");
					
					// Se agregan ajustes propios de los datos consultados
					boolean 	esCuentaEnDolaresAmericanos 	= "true".equals(reg.getString("ES_CUENTA_DOLARES"))?true:false;
 
					tipoAfiliado 			= ( "P".equals(tipoAfiliado)) ?"Proveedor":"";
					// producto
					// nombre
					// banco
					cuentaClabe				= esCuentaEnDolaresAmericanos	?"N/A":cuentaClabe;
					cuentaSwift				= !esCuentaEnDolaresAmericanos?"N/A":cuentaSwift;
					plazaSwift				= !esCuentaEnDolaresAmericanos?"N/A":plazaSwift;
					sucursalSwift			= !esCuentaEnDolaresAmericanos?"N/A":sucursalSwift;
					// Decodificar Campo Moneda	
					String descripcionMoneda = "";
					for(int i=0;i<catalogoMoneda.size();i++){
						JSONObject 	registro = catalogoMoneda.getJSONObject(i); 
						String 		clave		= registro.getString("clave");
						if( clave != null && clave.equals(moneda)){
							descripcionMoneda = registro.getString("descripcion");
							break;
						}
					}
					moneda 					= descripcionMoneda;
					// nombreEpo
					descripcionTef 		= esCuentaEnDolaresAmericanos	?"N/A":descripcionTef;
					descripcionCecoban 	= esCuentaEnDolaresAmericanos	?"N/A":descripcionCecoban;
					descripcionDolares 	= !esCuentaEnDolaresAmericanos?"N/A":(descripcionDolares.equals("")?"Sin Validar":descripcionDolares);
					//fechaUltimaActualizacion
					// usuario

					// Se agregan los campos con las modificaciones definitivas
					pdfDoc.setCell( tipoAfiliado,					"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( producto,						"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( nombre,							"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( banco,							"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( cuentaClabe,					"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( cuentaSwift,					"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( plazaSwift,					"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( sucursalSwift,				"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( moneda,							"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( nombreEpo,						"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( descripcionTef,				"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( descripcionCecoban,			"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( descripcionDolares,			"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( fechaUltimaActualizacion,	"formasrep", ComunesPDF.CENTER );
					pdfDoc.setCell( usuario,						"formasrep", ComunesPDF.CENTER );
						
				}
					
				pdfDoc.addTable();
				pdfDoc.endDocument();
	
			} catch(Throwable e) {
				
				log.error("crearPageCustomFile(Exception)");
				e.printStackTrace();
				
				throw new AppException("Error al generar el archivo", e);
				
			} finally {
				
			}
			
		}
 
		return nombreArchivo;
		
	}
	
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {
		
		log.info("crearCustomFile(E)");
		
		OutputStreamWriter 	writer 			= null;
		BufferedWriter 		buffer 			= null;
		String 					nombreArchivo 	= null;
		
		try {
			
			if ("CSV".equals(tipo)) {
			
				// Leer catalogo de monedas
				CatalogoMoneda cat = new CatalogoMoneda();
				cat.setCampoClave("ic_moneda");
				cat.setCampoDescripcion("cd_nombre");
				cat.setOrden("1");
				JSONArray catalogoMoneda = cat.getJSONArray();
 
				nombreArchivo 	= Comunes.cadenaAleatoria(16) + ".csv";
				writer 			= new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "Cp1252");
				buffer 			= new BufferedWriter(writer);
				
				// Agregar cabecera
				buffer.write("Tipo de Afiliado,"								);
				buffer.write("Producto,"										);
				buffer.write("Nombre,"											);
				buffer.write("Banco,"											);
				buffer.write("Cuenta Clabe,"									);
				buffer.write("Cuenta Swift,"									);
				buffer.write("Plaza Swift,"									);
				buffer.write("Sucursal Swift,"								);
				buffer.write("Tipo de Moneda,"								);
				buffer.write("EPO,"												);
				buffer.write("Descripci�n TEF,"								);
				buffer.write("Descripci�n CECOBAN,"							);
				buffer.write("Descripci�n D�LARES,"							);
				buffer.write("Fecha y hora de �ltima actualizaci�n,"	);
				buffer.write("Usuario"											);
				buffer.write("\r\n");
				
				int numeroLinea = 0;
				while(rs != null && rs.next()) {
					
					numeroLinea++;
					
					String tipoAfiliado 					= rs.getString("TIPOAFILIADO");
					String producto 						= rs.getString("PRODUCTO");
					String nombre 							= rs.getString("NOMBRE");
					String banco 							= rs.getString("BANCO");
					String cuentaClabe 					= rs.getString("CUENTACLABE");
					String cuentaSwift 					= rs.getString("CUENTASWIFT");
					String plazaSwift 					= rs.getString("PLAZA_SWIFT");
					String sucursalSwift 				= rs.getString("SUCURSAL_SWIFT");
					String moneda 							= rs.getString("TIPO_MONEDA");
					String nombreEpo 						= rs.getString("EPOREL");
					String descripcionTef 				= rs.getString("ESTATUS_TEF");
					String descripcionCecoban 			= rs.getString("ESTATUS_CECOBAN");
					String descripcionDolares 			= rs.getString("ESTATUS_DOLARES");
					String fechaUltimaActualizacion 	= rs.getString("FECHA_ULT_MODSS");
					String usuario 						= rs.getString("USUARIO");
					
					tipoAfiliado 							= tipoAfiliado 					== null?"":tipoAfiliado;
					producto 								= producto 							== null?"":producto;
					nombre 									= nombre 							== null?"":nombre;
					banco 									= banco 								== null?"":banco;
					cuentaClabe 							= cuentaClabe 						== null?"":cuentaClabe;
					cuentaSwift 							= cuentaSwift 						== null?"":cuentaSwift;
					plazaSwift 								= plazaSwift 						== null?"":plazaSwift;
					sucursalSwift 							= sucursalSwift 					== null?"":sucursalSwift;
					moneda 									= moneda 							== null?"":moneda;
					nombreEpo 								= nombreEpo 						== null?"":nombreEpo;
					descripcionTef 						= descripcionTef 					== null?"":descripcionTef;
					descripcionCecoban 					= descripcionCecoban 			== null?"":descripcionCecoban;
					descripcionDolares 					= descripcionDolares 			== null?"":descripcionDolares;
					fechaUltimaActualizacion 			= fechaUltimaActualizacion 	== null?"":fechaUltimaActualizacion;
					usuario 									= usuario 							== null?"":usuario;
 
					// SE AGREGAN AJUSTES PROPIOS DE LOS DATOS CONSULTADOS
					boolean 	esCuentaEnDolaresAmericanos 	= "true".equals(rs.getString("ES_CUENTA_DOLARES"))?true:false;
 
					tipoAfiliado 			= ( "P".equals(tipoAfiliado)) ?"Proveedor":"";
					// producto
					// nombre
					// banco
					cuentaClabe				= esCuentaEnDolaresAmericanos	?"N/A":"'"+cuentaClabe;
					cuentaSwift				= !esCuentaEnDolaresAmericanos?"N/A":"'"+cuentaSwift;
					plazaSwift				= !esCuentaEnDolaresAmericanos?"N/A":plazaSwift;
					sucursalSwift			= !esCuentaEnDolaresAmericanos?"N/A":sucursalSwift;
					// Decodificar Campo Moneda	
					String descripcionMoneda = "";
					for(int i=0;i<catalogoMoneda.size();i++){
						JSONObject 	registro = catalogoMoneda.getJSONObject(i); 
						String 		clave		= registro.getString("clave");
						if( clave != null && clave.equals(moneda)){
							descripcionMoneda = registro.getString("descripcion");
							break;
						}
					}
					moneda 					= descripcionMoneda;
					// nombreEpo
					descripcionTef 		= esCuentaEnDolaresAmericanos	?"N/A":descripcionTef;
					descripcionCecoban 	= esCuentaEnDolaresAmericanos	?"N/A":descripcionCecoban;
					descripcionDolares 	= !esCuentaEnDolaresAmericanos?"N/A":(descripcionDolares.equals("")?"Sin Validar":descripcionDolares);
					//fechaUltimaActualizacion
					// usuario
					
					// SE SUPRIMEN LAS COMAS SEGUN SEA NECESARIO
					//tipoAfiliado 					= tipoAfiliado.replace(',',' ');
					producto 							= producto.replace(',',' ');
					nombre 								= nombre.replace(',',' ');
					banco 								= banco.replace(',',' ');
					//cuentaClabe 						= cuentaClabe.replace(',',' ');
					//cuentaSwift 						= cuentaSwift.replace(',',' ');
					//plazaSwift 						= plazaSwift.replace(',',' ');
					//sucursalSwift 					= sucursalSwift.replace(',',' ');
					moneda 								= moneda.replace(',',' ');
					nombreEpo 							= nombreEpo.replace(',',' ');
					//descripcionTef 					= descripcionTef.replace(',',' ');
					//descripcionCecoban 			= descripcionCecoban.replace(',',' ');
					//descripcionDolares 			= descripcionDolares.replace(',',' ');
					//fechaUltimaActualizacion 	= fechaUltimaActualizacion.replace(',',' ');
					//usuario 							= usuario.replace(',',' ');
 
					// SE AGREGAN LOS CAMPOS AL BUFFER
					buffer.write(tipoAfiliado);					buffer.write(",");
					buffer.write(producto);							buffer.write(",");
					buffer.write(nombre);							buffer.write(",");
					buffer.write(banco);								buffer.write(",");
					buffer.write(cuentaClabe);						buffer.write(",");
					buffer.write(cuentaSwift);						buffer.write(",");
					buffer.write(plazaSwift);						buffer.write(",");
					buffer.write(sucursalSwift);					buffer.write(",");
					buffer.write(moneda);							buffer.write(",");
					buffer.write(nombreEpo);						buffer.write(",");
					buffer.write(descripcionTef);					buffer.write(",");
					buffer.write(descripcionCecoban);			buffer.write(",");
					buffer.write(descripcionDolares);			buffer.write(",");
					buffer.write(fechaUltimaActualizacion);	buffer.write(",");
					buffer.write(usuario);

					buffer.write("\r\n");
					
					if(numeroLinea % 100 == 0){
						buffer.flush();
					}
			
				}
				buffer.flush();
				
			}
			
		} catch (Throwable e) {
				
			log.error("crearCustomFile(Exception)");
			log.error("crearCustomFile.request 		= <" + request		+ ">");
			log.error("crearCustomFile.rs 			= <" + rs			+ ">");
			log.error("crearCustomFile.path 			= <" + path			+ ">");
			log.error("crearCustomFile.tipo 			= <" + tipo			+ ">");
			e.printStackTrace();
			
			throw new AppException("Error al generar el archivo", e);
				
		} finally {
			
			if(rs 		!= null ){ try { rs.close(); 		} catch(Exception e) {} }
			if(buffer 	!= null ){ try { buffer.close(); } catch(Exception e) {} }
			log.info("crearCustomFile(S)");
			
		}
 
		return nombreArchivo;
		
	}
	
	// GETERS
	public List 	getConditions() 			{ return conditions; 					}
	public String 	getOperacion() 			{ return operacion; 						}
	public String 	getPaginaNo() 				{ return paginaNo; 						}
	public String 	getPaginaOffset() 		{ return paginaOffset; 					}
	// GETERS - Parametros de la Consulta
	public String 	getNo_producto()			{ return this.no_producto; 			}
	public String 	getNo_nafin()				{ return this.no_nafin; 				}
	public String 	getFecha_registro_de()	{ return this.fecha_registro_de;		}
	public String 	getFecha_registro_a()	{ return this.fecha_registro_a;		}
	public String 	getCuentas_sin_validar(){ return this.cuentas_sin_validar;	}
	public String 	getCmb_tipo_afiliado()	{ return this.cmb_tipo_afiliado;		}
	public String 	getNo_epo()					{ return this.no_epo;					}
	public String 	getNo_if()					{ return this.no_if;					   }
	public String 	getRfc()						{ return this.rfc; 						}
	public List   	getListaUsuariosIF() 	{ return this.listaUsuariosIF; 		}
 
	// SETERS
	public void setOperacion(String newOperacion) 						{ operacion 					= newOperacion; 			}
	public void setPaginaNo(String newPaginaNo) 							{ paginaNo 						= newPaginaNo; 			}
	public void setPaginaOffset(String newPaginaOffset) 				{ paginaOffset 				= newPaginaOffset; 		}
	// SETERS - Parametros de la Consulta
	public void setNo_producto(String no_producto)						{  this.no_producto 			= no_producto;				}	
	public void setNo_nafin(String no_nafin) 								{  this.no_nafin 				= no_nafin;					}
	public void setFecha_registro_de(String fecha_registro_de)		{ 	this.fecha_registro_de 	= fecha_registro_de;		}
	public void setFecha_registro_a(String fecha_registro_a)			{  this.fecha_registro_a 	= fecha_registro_a;		}
	public void setCuentas_sin_validar(String cuentas_sin_validar)	{ 	this.cuentas_sin_validar= cuentas_sin_validar;	}
	public void setCmb_tipo_afiliado(String cmb_tipo_afiliado)		{  this.cmb_tipo_afiliado 	= cmb_tipo_afiliado;		}
	public void setNo_epo(String no_epo)									{  this.no_epo 				= no_epo;					}
	public void setNo_if(String no_if)										{  this.no_if 					= no_if;					}
	public void setRfc(String rfc)											{  this.rfc 					= rfc; 						}
	public void setListaUsuariosIF(List listaUsuariosIF)				{  this.listaUsuariosIF 	= listaUsuariosIF; 		}

}