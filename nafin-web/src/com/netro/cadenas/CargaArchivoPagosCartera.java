package com.netro.cadenas;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

public class CargaArchivoPagosCartera  {

	private final static Log log = ServiceLocator.getInstance().getLog(CargaArchivoPagosCartera.class);
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	///ATRIBUTOS DE LA CLASE
	private String tipoArchivo;
	private String filePATH;
	private String nombreArchivo;
	
	private String err;
	private int reg;
	
	ArrayList resultados = new ArrayList();

	public CargaArchivoPagosCartera() {
	}
	
	public boolean iniciarCarga(){
		log.info("iniciarCarga(E)");
		boolean exito=true;
		reg = 0;
		err="";
		try{
			if(tipoArchivo.equals("V")){
				resultados = leerArchivoaTmpVenc(filePATH,nombreArchivo);
				if(((Boolean)resultados.get(0)).booleanValue()){
					if(!validarFechasProceso("vencimiento", "com_fechaprobablepago", "com_fechaprobablepago")){
						if(actualizarIFaTmp(tipoArchivo)){
							if(validarCamposVenc(filePATH,nombreArchivo)){
								resultados = insertarDatosVenc(nombreArchivo);
								if(((Boolean)resultados.get(0)).booleanValue()){
									reg = ((Integer)resultados.get(1)).intValue();
									exito = true;
								}else{ // if(insertarDatosVenc(nombreArch))
									exito = false;
									err +="Ocurrieron errores al guardar los datos.";
								}
							}else{ // if(validarCamposVenc(PATH_FILE,nombreArch))
								exito = false;
								err +="Ocurrieron errores al validar datos.";
							}
						}else{ // if(actualizarIFaTmp(tipoArchivo)){
							exito = false;
							err +="El archivo no se pudo cargar.";
						}
					}else{ // if(!validarFechasProceso(String tabla, String campo))
						exito = false;
						err += "No es posible cargar registros con fechas en las que ya se tiene registro de un proceso, por favor verif�quelo";
					}
				}else{ // if(((Boolean)resultados.get(0)).booleanValue())
					exito = false;
					err += "El archivo no se pudo cargar. ";
					err += resultados.get(1).toString();
				}
				limpiarTablaTmp("comtmp_vencimiento");
			}else if(tipoArchivo.equals("O")){ // if(tipoArchivo.equals("V"))
				//if(leerArchivoaTmpOper(PATH_FILE,nombreArch)){
				resultados = leerArchivoaTmpOper(filePATH,nombreArchivo);
				if(((Boolean)resultados.get(0)).booleanValue()){
					if(!validarFechasProceso("operado", "df_fechaoperacion", "df_operacion")){
						if(actualizarIFaTmp(tipoArchivo)){
							if(validarCamposOper(filePATH,nombreArchivo)){
								resultados = insertarDatosOper(nombreArchivo);
								if(((Boolean)resultados.get(0)).booleanValue()){
									reg = ((Integer)resultados.get(1)).intValue();
									exito = true;
								}else{ // if(insertarDatosOper(nombreArch))
									exito = false;
									err +="Ocurrieron errores al guardar los datos.";
								}
							}else{ // if(validarCamposOper(PATH_FILE,nombreArch))
								exito = false;
								err +="Ocurrieron errores al validar datos.";
							}
						}else{ // if(actualizarIFaTmp(tipoArchivo))
							exito = false;
							err +="El archivo no se pudo cargar.";
						}
					}else{ // if(!validarFechasProceso("vencimiento", "com_fechaprobablepago"))
						exito = false;
						err += "No es posible cargar registros con fechas en las que ya se tiene registro de un proceso, por favor verif�quelo";
					}
				}else{ // if(((Boolean)resultados.get(0)).booleanValue())
					exito = false;
					err += "El archivo no se pudo cargar.";
					err += resultados.get(1).toString();
				}
				limpiarTablaTmp("comtmp_operado");
			}
		}catch(Exception e){
			exito=false;
			System.err.println("El archivo no se pudo cargar");
			e.printStackTrace();
		}
		return exito;
	}
	
	
	private String getcamposBDvencimiento(){
		return ("ic_vencimiento,com_fechaprobablepago,cg_modalidadpago,ic_moneda,ic_financiera,ig_baseoperacion,cg_descbaseoper,ig_cliente,"+
				"cg_nombrecliente,ig_sucursal,ig_proyecto,ig_contrato,ig_prestamo,ig_numelectronico,cg_bursatilizado,ig_disposicion,"+
				"ig_subaplicacion,ig_cuotasemit,ig_cuotasporvenc,ig_totalcuotas,cg_aniomodalidad,ig_tipoestrato,df_fechaopera,"+
				"ig_sancionado,ig_frecuenciacapital,ig_frecuenciainteres,ig_tasabase,cg_esquematasas,cg_relmat_1,fg_spread_1,"+
				"cg_relmat_2,fg_spread_2,cg_relmat_3,fg_spread_3,cg_relmat_4,fg_margen,ig_tipogarantia,fg_porcdescfop,fg_totdescfop,"+
				"fg_porcdescfinape,fg_totdescfinape,fg_fcrecimiento,df_periodoinic,df_periodofin,ig_dias,fg_comision,"+
				"fg_porcgarantia,fg_porccomision,fg_sdoinsoluto,fg_amortizacion,fg_interes,fg_intcobradoxanticip,"+
				"fg_totalvencimiento,fg_pagotrad,fg_subsidio,fg_totalexigible,fg_capitalvencido,fg_interesvencido,"+
				"fg_totalcarven,fg_adeudototal,fg_finadicotorgado,fg_finadicrecup,fg_sdoinsnvo,fg_sdoinsbase,ic_proc_vencimiento");
	}

	private ArrayList leerArchivoaTmpVenc(String ruta, String nombreArch) throws IOException{
		System.out.println("leerArchivoaTmpVenc(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String qrySentencia = "", mensaje = "";
		boolean exito = true, validar = true;
		Vector vecdat = null;
		VectorTokenizer vt = null;
		VectorTokenizer vtcampos = new VectorTokenizer(getcamposBDvencimiento(),",");
		Vector vecCampos = vtcampos.getValuesVector();
		int i = 0, contador = 1, j = 0, pos = 0, k = 0, cont = 0, ilinea = 0;
		final int TOTAL_CAMPOS = 65;
		ArrayList lretorno = new ArrayList();
		ZipEntry entry;
		try{
			limpiarTablaTmp("comtmp_vencimiento");
			con.conexionDB();
			StringBuffer strSQL = new StringBuffer();
			strSQL.append("INSERT INTO COMTMP_VENCIMIENTO (" + getcamposBDvencimiento() +") VALUES ( ?,");
			for(k=0;k<TOTAL_CAMPOS-2;k++){
				if(k==0||k==21||k==41||k==42)
					strSQL.append("TO_DATE(?, 'dd/mm/yyyy'),");
				else
					strSQL.append("?,");
			}//for (int i = 0; i < TOTAL_CAMPOS-2;  i++)
			strSQL.append("?)");
			ZipFile zipfile = new ZipFile(ruta+nombreArch);
			Enumeration e = zipfile.entries();
			while(e.hasMoreElements()){
				entry = (ZipEntry) e.nextElement();
				BufferedReader br=new BufferedReader(new InputStreamReader(zipfile.getInputStream(entry)));
				String str;
				while ((str = br.readLine()) != null){
					if(pos>0){
						vt = new VectorTokenizer(str,";");
						vecdat = vt.getValuesVector();
						vecdat.remove(3);
						vecdat.remove(4);
						cont = 1;
						ps = con.queryPrecompilado(strSQL.toString());
						//cont++;
						ps.setInt(cont, contador-1);
		                for(k=0;k<TOTAL_CAMPOS-2;k++){
							String valor = vecdat.get(k).toString().trim();
							switch(k){
								case 2:	 case 3:  case 4:  case 6:  case 8: case 9: case 10: case 11:
								case 12: case 14: case 15: case 16: case 17: case 18: case 20: case 22:
								case 23: case 24: case 35: case 43:
									if(!"".equals(valor)){
										cont++;
										if(valor.length()<8){
											ps.setInt(cont, new Integer(valor).intValue());
										}else{
											ps.setLong(cont, new Long(valor).longValue());
										}
									}else{
										cont++;
										ps.setNull(cont, Types.INTEGER);
									}
									break;
								case 25: case 28: case 30: case 32: case 34: case 36: case 37: case 38:
								case 39: case 40: case 44: case 45: case 46: case 47: case 48: case 49:
								case 50: case 51: case 52: case 53: case 54: case 55: case 56: case 57:
								case 58: case 59: case 60: case 61: case 62:
									if(!"".equals(valor)){
										cont++;
										ps.setDouble(cont, new Double(valor).doubleValue());
									}else{
										cont++;
										ps.setNull(cont, Types.DOUBLE);
									}
									break;
								default:
									if(k==0||k==21||k==41||k==42){
										if (!Comunes.checaFecha(valor)){
											mensaje = "\nError en la linea: "+ilinea+", el campo ["+vecCampos.get(k+1)+"] no contiene una fecha valida.";
											exito = false;
										}
									}
									cont++;
									ps.setString(cont, valor);
									break;
							}//switch(i)
							if(!exito)
								break;
		          		}//for (int i = 0; i < TOTAL_CAMPOS-2;  i++)
						if(!exito)
							break;
		          		cont++;ps.setInt(cont, 1);
						ps.executeUpdate();
						if(ps!=null) ps.close();
					}//fin if(pos>0)
					if(!exito)
						break;
					pos++;
					contador++;
					ilinea++;
				}// fin while((str = br.readLine())!=null)
				br.close();
			}
		}catch(IOException ioe){
			exito = false;
			System.out.println("leerArchivoaTmpVenc:::IOException:..pos:: "+pos);
			ioe.printStackTrace();
		}catch(Exception e){
			exito = false;
			System.out.println("leerArchivoaTmpVenc:::Exception:..pos:: "+pos);
			e.printStackTrace();
		}finally{
			System.out.println("leerArchivoaTmpVenc(S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
		lretorno.add(new Boolean(exito));
		lretorno.add(mensaje);
		return lretorno;
	}
	
	private boolean validarCamposVenc(String ruta, String nombreArch) throws IOException{
	System.out.println("validarCamposVenc(E)");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	ResultSet rs = null;
	ResultSet rsBusca = null;
	String qrySentencia = "Select "+getcamposBDvencimiento()+" from comtmp_vencimiento";
	//String qrySentencia = "Select ic_financiera, ic_moneda from comtmp_vencimiento";
	nombreArch = nombreArch.substring(0,(nombreArch.length()-4));
	boolean exito = true;
	List ldatos = null;
	int ilinea = 1;
	PrintWriter salida = null;
	StringBuffer errores = new StringBuffer();
	String monedaAnt = "";
	try{
		con.conexionDB();
		String tiempo = Long.toString(System.currentTimeMillis());
		FileWriter fw = new FileWriter(ruta+"logerr"+nombreArch+".txt");
		BufferedWriter bw = new BufferedWriter(fw);
		salida = new PrintWriter(bw);
		ps = con.queryPrecompilado(qrySentencia);
		rs = ps.executeQuery();
		VectorTokenizer vt = new VectorTokenizer(getcamposBDvencimiento(),",");
		Vector vecCampos = vt.getValuesVector();
		/*for(int b=0;b<vecCampos.size();b++){
			System.out.println("\n");
			System.out.print(vecCampos.get(b)+"|");
		}*/
		while(rs.next()){
			String msgerr = "Error en la linea: "+ilinea+", ";
			ldatos = new ArrayList();
			for(int a=2;a<65;a++){
				ldatos.add((rs.getString(a)==null)?"":rs.getString(a));
			}
			for(int a=0;a<64;a++){
				switch(a){
					case 2:	 case 3:  case 4:  case 6:  case 8: case 9: case 10: case 11:
					case 12: case 14: case 15: case 16: case 17: case 18: case 20: case 22:
					case 23: case 24: case 35: case 43:
						if(!"".equals(ldatos.get(a).toString())){
							if(!Comunes.esNumero(ldatos.get(a).toString())){
								exito = false;
								errores.append(msgerr + " el campo ["+vecCampos.get(a+1)+"] no es un numero.\n");
							}
						}
						break;
					case 25: case 28: case 30: case 32: case 34: case 36: case 37: case 38:
					case 39: case 40: case 44: case 45: case 46: case 47: case 48: case 49:
					case 50: case 51: case 52: case 53: case 54: case 55: case 56: case 57:
					case 58: case 59: case 60: case 61: case 62:
						if(!"".equals(ldatos.get(a).toString())){
							if (!Comunes.esDecimal(ldatos.get(a).toString())) {
								exito = false;
								errores.append(msgerr + " el campo ["+vecCampos.get(a+1)+"] no contiene un numero.\n");
							}
							else if(ldatos.get(a).toString().indexOf(".") != -1) { // si tiene parte fraccionaria
								if(a==25 || a==28 || a==30 || a==32 || a==34 || a==36 || a==38 || a==45 || a==46) {
									if(ldatos.get(a).toString().substring(ldatos.get(a).toString().indexOf(".")+1,ldatos.get(a).toString().length()).length()>4){
										exito = false;
										errores.append(msgerr + " el campo ["+vecCampos.get(a+1)+"] debe tener maximo 4 decimales.\n");
									}
								}
								else if(a==40){
									if(ldatos.get(a).toString().substring(ldatos.get(a).toString().indexOf(".")+1,ldatos.get(a).toString().length()).length()>13){
										exito = false;
										errores.append(msgerr + " el campo ["+vecCampos.get(a+1)+"] debe tener maximo 13 decimales.\n");
									}
								}
								else {
									if(ldatos.get(a).toString().substring(ldatos.get(a).toString().indexOf(".")+1,ldatos.get(a).toString().length()).length()>2){
										exito = false;
										errores.append(msgerr + " el campo ["+vecCampos.get(a+1)+"] debe tener maximo 2 decimales.\n");
									}
								}
							}
						}
						break;
					default:
						break;
				} // fin switch(a)
			} // fin for(int a=0;a<64;a++)
			//validar la moneda
			String moneda = ldatos.get(2).toString();
			if(!monedaAnt.equals(moneda)){
				String sentencia = "SELECT ic_moneda FROM COMCAT_MONEDA WHERE ic_moneda = " +moneda;
				rsBusca = con.queryDB(sentencia);
				if(!rsBusca.next()){
					exito = false;
					errores.append(msgerr + " la Moneda \""+moneda+"\" no existe.\n");
				}
				rsBusca.close();
				con.cierraStatement();  // Cierra cursor
			}
			monedaAnt = moneda;
			ilinea++;
			if(errores.length()>0)
				salida.print(errores.toString());
				this.err+=errores.toString();
			errores = errores.delete(0,errores.length());
		} // fin while(rs.next())
		salida.close();
	}catch(IOException sqle){
		System.out.println("error:::validarCamposVenc::: "+sqle);
	}catch(Exception e){
		exito = false;
		System.out.println("error:::validarCamposVenc:::"+e);
		e.printStackTrace();
	}finally{
		System.out.println("validarCamposVenc(S)");
		if (con.hayConexionAbierta()){
			//con.terminaTransaccion(exito);
			con.cierraConexionDB();
		}
	}
	return exito;
}

	private ArrayList insertarDatosVenc(String nombreArch) throws SQLException{
		System.out.println("insertarDatosVenc(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean exito = true;
		int registros = 0;
		ArrayList lretorno = new ArrayList();
		try{
			con.conexionDB();
			String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new java.util.Date());

			String strSQL =" SELECT ig_if_primer_piso " +
			 			" FROM com_param_gral " +
			 			" WHERE ic_param_gral = 1 ";
			rs = con.queryDB(strSQL);
			rs.next();
			String claveIFPrimerPiso = (rs.getString("ig_if_primer_piso")==null)?"NULL":rs.getString("ig_if_primer_piso");

				rs.close();
				con.cierraStatement();

			strSQL = "DELETE FROM COM_VENCIMIENTO " +
						" WHERE com_fechaprobablepago <= " +
						" (SELECT sysdate-in_dias_vig_vencimientos " +
						" 	FROM COMCAT_PRODUCTO_NAFIN " +
						" 	WHERE ic_producto_nafin = 1) ";
			con.ejecutaSQL(strSQL);

			strSQL = "select SEQ_comhis_proc_vencimiento.nextval as ic_proc_vencimiento from dual";
			rs = con.queryDB(strSQL);
			rs.next();
			String ic_proc_vencimiento = rs.getString("ic_proc_vencimiento");
			rs.close();
			con.cierraStatement();

			strSQL =	" INSERT INTO com_vencimiento (ic_vencimiento, " +
						" com_fechaprobablepago, ic_if, ic_moneda, ig_baseoperacion, " +
						" ig_cliente, cg_nombrecliente, ig_sucursal, ig_prestamo, ig_subaplicacion, ig_cuotasemit, " +
						" ig_cuotasporvenc, ig_totalcuotas, df_fechaopera, ig_sancionado, ig_frecuenciacapital, " +
						" ig_frecuenciainteres, ig_tasabase, cg_esquematasas, fg_margen, fg_porcdescfop, fg_totdescfop, " +
						" fg_porcdescfinape, fg_totdescfinape, df_periodoinic, df_periodofin, ig_dias, fg_sdoinsoluto, " +
						" fg_amortizacion, fg_interes, fg_totalvencimiento, fg_pagotrad, fg_subsidio, fg_totalexigible, " +
						" fg_sdoinsnvo, fg_sdoinsbase, cg_modalidadpago, cg_descbaseoper, ig_proyecto, ig_contrato, " +
						" ig_numelectronico,cg_bursatilizado, ig_disposicion,cg_aniomodalidad, ig_tipoestrato,cg_relmat_1, " +
						" fg_spread_1,cg_relmat_2,fg_spread_2,cg_relmat_3,fg_spread_3,cg_relmat_4, ig_tipogarantia, " +
						" fg_fcrecimiento, fg_comision, fg_porcgarantia, fg_porccomision, fg_capitalvencido, fg_interesvencido, "+
						" fg_totalcarven, fg_adeudototal, fg_finadicotorgado, fg_finadicrecup, fg_intcobradoxanticip, " +
						" ic_proc_vencimiento) " +
						" SELECT seq_com_vencto_ic_vencto.NEXTVAL, " +
						" com_fechaprobablepago, ic_if, ic_moneda, ig_baseoperacion, " +
						" ig_cliente, cg_nombrecliente, ig_sucursal, ig_prestamo, ig_subaplicacion, ig_cuotasemit, " +
						" ig_cuotasporvenc, ig_totalcuotas, df_fechaopera, ig_sancionado, ig_frecuenciacapital, " +
						" ig_frecuenciainteres, ig_tasabase, cg_esquematasas, fg_margen, fg_porcdescfop, fg_totdescfop, " +
						" fg_porcdescfinape, fg_totdescfinape, df_periodoinic, df_periodofin, ig_dias, fg_sdoinsoluto, " +
						" fg_amortizacion, fg_interes, fg_totalvencimiento, fg_pagotrad, fg_subsidio, fg_totalexigible, " +
						" fg_sdoinsnvo, fg_sdoinsbase, cg_modalidadpago, cg_descbaseoper, ig_proyecto, ig_contrato, " +
						" ig_numelectronico,cg_bursatilizado, ig_disposicion,cg_aniomodalidad, ig_tipoestrato,cg_relmat_1, " +
						" fg_spread_1,cg_relmat_2,fg_spread_2,cg_relmat_3,fg_spread_3,cg_relmat_4, ig_tipogarantia, " +
						" fg_fcrecimiento, fg_comision, fg_porcgarantia, fg_porccomision, fg_capitalvencido, fg_interesvencido, "+
						" fg_totalcarven, fg_adeudototal, fg_finadicotorgado, fg_finadicrecup, fg_intcobradoxanticip, " +
						" "+ ic_proc_vencimiento +
						" FROM comtmp_vencimiento where ic_if is not null";
				con.ejecutaSQL(strSQL);

				strSQL = "	SELECT to_char(cv.com_fechaprobablepago,'dd/mm/yyyy') as fechaprobablepago, cif.cs_tipo, "+
	       				  "	COUNT (distinct cv.ic_if) numifs, COUNT (1) numregistros, "+
	       				  " SUM (DECODE (cv.ic_moneda, 1, 1, 0)) totmn, "+
	       				  " SUM (DECODE (cv.ic_moneda, 54, 1, 0)) totdl, "+
	       				  " SUM (DECODE (cv.ic_moneda, 1, 0, 54, 0, 1)) tot_otro "+
	  					  " FROM  com_vencimiento cv, comcat_if cif "+
	 					  " WHERE cv.ic_if = cif.ic_if "+
	 					  " AND ic_proc_vencimiento = " + ic_proc_vencimiento +
	 					  " GROUP BY cv.com_fechaprobablepago,  cif.cs_tipo" ;
				rs = con.queryDB(strSQL);

				//System.out.println("strSQL Obtiene los datos"+strSQL);
				while (rs.next()){
					String fechaprobablepago = rs.getString("fechaprobablepago");
					String cs_tipo=rs.getString("cs_tipo");
					int numIFs = rs.getInt("numIFs");
					int num_reg_mn= rs.getInt("totmn");
				    int num_reg_dl= rs.getInt("totdl");
				    int numRegistros = rs.getInt("tot_otro");
					registros += rs.getInt("numregistros");

					strSQL = " INSERT INTO comhis_proc_vencimiento (ic_proc_vencimiento , com_fechaprobablepago,cg_nombre_archivo, df_fecha_hora, ig_num_registros, "+
						" ig_num_ifs,ig_num_reg_mn,ig_num_reg_dl, cs_tipo) " +
						" VALUES ("+ic_proc_vencimiento+","+"to_date('"+fechaprobablepago+"','dd/MM/yyyy')"+ ",'"+nombreArch+"',"+ "to_date('"+fechaHoy+"','dd/mm/yyyy HH24:mi:ss')"+","+numRegistros+","+numIFs+","+num_reg_mn+","+num_reg_dl+",'"+cs_tipo+"')";
					con.ejecutaSQL(strSQL);
				}//fin-while rs
				rs.close();
				con.cierraStatement();

		}catch(SQLException sqle){
			exito = false;
			System.out.println("error:::insertarDatosVenc::: "+sqle);
		}catch(Exception e){
			exito = false;
			System.out.println("error:::insertarDatosVenc:::"+e);
			e.printStackTrace();
		}finally{
			System.out.println("insertarDatosVenc(S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
		lretorno.add(new Boolean(exito));
		lretorno.add(new Integer(registros));
		return lretorno;
	}

	private String getcamposBDoperados(){
		return ("ic_operado, ig_estatus, df_fechaoperacion,	ic_moneda, fg_tipocambio, ic_financiera, cg_calificacion, " +
				"ig_baseoperacion, cg_descbaseop, ig_cliente, cg_nombrecliente,	ig_succliente, ig_proyecto, ig_contrato, " +
				"ig_prestamo, ig_numelectronico, ig_disposicion, ig_estado, ig_municipio, ig_subaplicacion, ig_suctram, " +
				"df_fechapripagcap,	ig_tasa, cg_relmat, fg_spread, fg_margen, cg_acteco, cg_aniomod, ig_estrato, cg_afianz,	" +
				"ig_freccap, ig_frecint, ig_numcoutas, ig_tipoamort, cg_modpago, cg_centrofin, ig_sucinter, fg_montooper, " +
				"fg_montoprimcuot, fg_montoultcout, fg_montorecal, fg_montoporgar, fg_comnodisp, fg_intcobradoxanticip, "+
				"ig_tipogar, fg_netootorgado, cg_primabruta, cg_gastos, ic_proc_operado");
	}

	private ArrayList leerArchivoaTmpOper(String ruta, String nombreArch) throws IOException{
		System.out.println("leerArchivoaTmpOper(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String qrySentencia = "", mensaje = "";
		boolean exito = true;
		Vector vecdat = null;
		VectorTokenizer vt = null;
		ArrayList lretorno = new ArrayList();
		VectorTokenizer vtcampos = new VectorTokenizer(getcamposBDoperados(),",");
		Vector vecCampos = vtcampos.getValuesVector();
		int i = 0, contador = 1, j = 0, pos = 0, k = 0, cont = 0, ilinea = 0;
		final int TOTAL_CAMPOS = 49;
		ZipEntry entry;
		try{
			limpiarTablaTmp("comtmp_operado");
			con.conexionDB();
			StringBuffer strSQL = new StringBuffer();
			strSQL.append("INSERT INTO COMTMP_OPERADO (" + getcamposBDoperados() +") VALUES (");
			for(k=0;k<TOTAL_CAMPOS-1;k++){
				if(k==2||k==21)
					strSQL.append("TO_DATE(?, 'dd/mm/yyyy'),");
				else
					strSQL.append("?,");
			}//for (int i = 0; i < TOTAL_CAMPOS-2;  i++)
			strSQL.append("?)");
			ZipFile zipfile = new ZipFile(ruta+nombreArch);
			Enumeration e = zipfile.entries();
			while(e.hasMoreElements()){
				entry = (ZipEntry) e.nextElement();
				BufferedReader br=new BufferedReader(new InputStreamReader(zipfile.getInputStream(entry)));
				String str;
				while ((str = br.readLine()) != null){
					if(pos>0){
						vt = new VectorTokenizer(str,";");
						vecdat = vt.getValuesVector();
						vecdat.remove(3);
						vecdat.remove(5);
						cont = 1;
						ps = con.queryPrecompilado(strSQL.toString());
						//cont++;
						ps.setInt(cont, contador-1);
		                for(k=0;k<TOTAL_CAMPOS-2;k++){
							String valor = vecdat.get(k).toString().trim();
							switch(k){
								case 0:	case 2:  case 4:  case 6: case 8: case 10: case 11: case 12:
								case 13: case 14: case 15: case 16: case 17: case 19: case 21:
								case 27: case 29: case 30: case 31: case 32: case 35:
									if(!"".equals(valor)){
										cont++;
										if(valor.length()<8){
											ps.setInt(cont, new Integer(valor).intValue());
										}else{
											ps.setLong(cont, new Long(valor).longValue());
										}
									}else{
										cont++;
										ps.setNull(cont, Types.INTEGER);
									}
									break;
								case 3: case 23: case 24: case 36:  case 37: case 38: case 39: case 40: case 41: case 42:
								case 44:
									if(!"".equals(valor)){
										cont++;
										ps.setDouble(cont, new Double(valor).doubleValue());
									}else{
										cont++;
										ps.setNull(cont, Types.DOUBLE);
									}
									break;
								default:
									if(k==1||k==20){
										if (!Comunes.checaFecha(valor)){
											mensaje = "\nError en la linea: "+ilinea+", el campo ["+vecCampos.get(k+1)+"] no contiene una fecha valida.";
											exito = false;
										}
									}
									cont++;
									ps.setString(cont, valor);
									break;
							}//switch(i)
							if(!exito)
								break;
		          		}//for (int i = 0; i < TOTAL_CAMPOS-2;  i++)
						if(!exito)
							break;
		          		cont++;ps.setInt(cont, 1);
						ps.executeUpdate();
						if(ps!=null) ps.close();
					}//fin if(pos>0)
					if(!exito)
						break;
					pos++;
					contador++;
					ilinea++;
				}// fin while((str = br.readLine())!=null)
			}
		}catch(IOException ioe){
			exito = false;
			System.out.println("leerArchivoaTmpOper:::IOException:..pos:: "+pos);
			ioe.printStackTrace();
		}catch(Exception e){
			exito = false;
			System.out.println("leerArchivoaTmpOper:::Exception::: "+e);
			e.printStackTrace();
		}finally{
			System.out.println("leerArchivoaTmpOper(S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
		lretorno.add(new Boolean(exito));
		lretorno.add(mensaje);
		return lretorno;
	}

	private boolean validarCamposOper(String ruta, String nombreArch) throws IOException{
		System.out.println("validarCamposOper(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ResultSet rsBusca = null;
		String qrySentencia = "Select "+getcamposBDoperados()+" from comtmp_operado";
		nombreArch = nombreArch.substring(0,(nombreArch.length()-4));
		boolean exito = true;
		List ldatos = null;
		int ilinea = 1;
		PrintWriter salida = null;
		StringBuffer errores = new StringBuffer();
		String monedaAnt = "";
		try{
			con.conexionDB();
			String tiempo = Long.toString(System.currentTimeMillis());
			FileWriter fw = new FileWriter(ruta+"logerr"+nombreArch+".txt");
			BufferedWriter bw = new BufferedWriter(fw);
			salida = new PrintWriter(bw);
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			VectorTokenizer vt = new VectorTokenizer(getcamposBDoperados(),",");
			Vector vecCampos = vt.getValuesVector();
			while(rs.next()){
				String msgerr = "Error en la linea: "+ilinea+", ";
				ldatos = new ArrayList();
				for(int a=2;a<49;a++){
					ldatos.add((rs.getString(a)==null)?"":rs.getString(a));
				}
				for(int a=0;a<48;a++){
					switch(a){
						case 0:	case 2:  case 4:  case 6: case 8: case 10: case 11: case 12:
						case 13: case 14: case 15: case 16: case 17: case 19: case 21:
						case 27: case 29: case 30: case 31: case 32: case 35:
							if(!"".equals(ldatos.get(a).toString())){
								if (!Comunes.esNumero(ldatos.get(a).toString())){
									exito = false;
									errores.append(msgerr + " el campo [" +vecCampos.get(a+1)+ "] no es un numero.\n");
								}
							}
							break;
						case 3: case 23: case 24: case 36:  case 37: case 38: case 39: case 40: case 41: case 42:
						case 44:
							if(!"".equals(ldatos.get(a).toString())){
								if (!Comunes.esDecimal(ldatos.get(a).toString()) && !Comunes.esNumero(ldatos.get(a).toString())) {
									exito = false;
									errores.append(msgerr + " el campo ["+vecCampos.get(a+1)+"] no contiene un numero.\n");
								}
								else if(ldatos.get(a).toString().indexOf(".") != -1) { // si tiene parte fraccionaria
									if(a==3 || a==23 || a==24) {
										if(ldatos.get(a).toString().substring(ldatos.get(a).toString().indexOf(".")+1,ldatos.get(a).toString().length()).length()>4) {
											exito = false;
											errores.append(msgerr + " el campo ["+vecCampos.get(a+1)+"] debe tener maximo 4 decimales.\n");
										}
									}
									else {
										if(ldatos.get(a).toString().substring(ldatos.get(a).toString().indexOf(".")+1,ldatos.get(a).toString().length()).length()>2) {
											exito = false;
											errores.append(msgerr + " el campo ["+vecCampos.get(a+1)+"] debe tener maximo 2 decimales.\n");
										}
									}
								}
							}
							break;
						default:
							break;
					} // fin switch(a)
				} // fin for(int a=0;a<48;a++)
				//validar la moneda
				String moneda = ldatos.get(2).toString();
				if(!monedaAnt.equals(moneda)){
					String sentencia = "SELECT ic_moneda FROM COMCAT_MONEDA WHERE ic_moneda = " +moneda;
					rsBusca = con.queryDB(sentencia);
					if(!rsBusca.next()){
						exito = false;
						errores.append(msgerr + " la Moneda \""+moneda+"\" no existe.\n");
					}
					rsBusca.close();
					con.cierraStatement();  // Cierra cursor
				}
				monedaAnt = moneda;
				ilinea++;
				if(errores.length()>0)
					salida.print(errores.toString());
					this.err+=errores.toString();
				errores = errores.delete(0,errores.length());
			} // fin while(rs.next())
			salida.close();
		}catch(IOException sqle){
			System.out.println("error:::validarCamposOper::: "+sqle);
		}catch(Exception e){
			exito = false;
			System.out.println("error:::validarCamposOper:::"+e);
			e.printStackTrace();
		}finally{
			System.out.println("validarCamposOper(S)");
			if (con.hayConexionAbierta()){
				//con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
		return exito;
	}

	private ArrayList insertarDatosOper(String nombreArch) throws SQLException{
		System.out.println("insertarDatosOper(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean exito = true;
		ArrayList lretorno = new ArrayList();
		int registros = 0;
		try{
			con.conexionDB();
			String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new java.util.Date());

			String strSQL =" SELECT ig_if_primer_piso " +
			 			" FROM com_param_gral " +
			 			" WHERE ic_param_gral = 1 ";
			rs = con.queryDB(strSQL);
			rs.next();
			String claveIFPrimerPiso = (rs.getString("ig_if_primer_piso")==null)?"NULL":rs.getString("ig_if_primer_piso");

			strSQL = "DELETE FROM com_operado " +
						" WHERE df_fechaoperacion <= " +
						" (SELECT sysdate-in_dias_vig_operados " +
						" 	FROM comcat_producto_nafin " +
						" 	WHERE ic_producto_nafin = 1) ";
			con.ejecutaSQL(strSQL);

			strSQL = "select SEQ_COMHIS_PROC_OPERADO.nextval  as ic_proc_operado from dual ";
			rs = con.queryDB(strSQL);
			rs.next();
			String ic_proc_operado = rs.getString("ic_proc_operado");
			rs.close();
			con.cierraStatement();

			strSQL = " INSERT INTO COM_OPERADO " +
						" ( " +
						" 	ic_operado, " +
						" 	ic_if, ic_moneda, ig_estatus, " +
						" 	df_fechaoperacion, fg_tipocambio, cg_calificacion, " +
						" 	ig_baseoperacion, cg_descbaseop, ig_cliente, " +
						" 	cg_nombrecliente, ig_succliente, ig_proyecto, " +
						" 	ig_contrato, ig_prestamo, ig_numelectronico, " +
						" 	ig_disposicion, ig_estado, ig_municipio, " +
						" 	ig_subaplicacion, ig_suctram, df_fechapripagcap, " +
						" 	ig_tasa, cg_relmat, fg_spread, " +
						" 	fg_margen, cg_acteco, cg_aniomod, " +
						" 	ig_estrato, cg_afianz, ig_freccap, " +
						" 	ig_frecint, ig_numcoutas, ig_tipoamort, " +
						" 	cg_modpago, cg_centrofin, ig_sucinter, " +
						" 	fg_montooper, fg_montoprimcuot, fg_montoultcout, " +
						" 	fg_montorecal, fg_montoporgar, fg_comnodisp, " +
						" 	ig_tipogar, fg_netootorgado, cg_primabruta, " +
						" 	cg_gastos, fg_intcobradoxanticip, " +
						" 	ic_proc_operado) " +
						" SELECT  SEQ_COM_OPER_IC_OPER.nextval," +
						" 	ic_if, ic_moneda, ig_estatus, " +
						" 	df_fechaoperacion, fg_tipocambio, cg_calificacion, " +
						" 	ig_baseoperacion, cg_descbaseop, ig_cliente, " +
						" 	cg_nombrecliente, ig_succliente, ig_proyecto, " +
						" 	ig_contrato, ig_prestamo, ig_numelectronico, " +
						" 	ig_disposicion, ig_estado, ig_municipio, " +
						" 	ig_subaplicacion, ig_suctram, df_fechapripagcap, " +
						" 	ig_tasa, cg_relmat, fg_spread, " +
						" 	fg_margen, cg_acteco, cg_aniomod, " +
						" 	ig_estrato, cg_afianz, ig_freccap, " +
						" 	ig_frecint, ig_numcoutas, ig_tipoamort, " +
						" 	cg_modpago, cg_centrofin, ig_sucinter, " +
						" 	fg_montooper, fg_montoprimcuot, fg_montoultcout, " +
						" 	fg_montorecal, fg_montoporgar, fg_comnodisp, " +
						" 	ig_tipogar, fg_netootorgado, cg_primabruta, " +
						" 	cg_gastos, fg_intcobradoxanticip, " +
						" " + ic_proc_operado +
						" FROM comtmp_operado where ic_if is not null";
			con.ejecutaSQL(strSQL);

			strSQL = "	SELECT to_char(co.df_fechaoperacion,'dd/mm/yyyy') as df_fechaoperacion, cif.cs_tipo, "+
	       				  "	COUNT (distinct co.ic_if) numifs, COUNT (1) numregistros, "+
	       				  " SUM (DECODE (co.ic_moneda, 1, 1, 0)) totmn, "+
	       				  " SUM (DECODE (co.ic_moneda, 54, 1, 0)) totdl, "+
	       				  " SUM (DECODE (co.ic_moneda, 1, 0, 54, 0, 1)) tot_otro "+
	  					  " FROM com_operado co, comcat_if cif "+
	 					  " WHERE co.ic_if = cif.ic_if "+
	 					  " AND ic_proc_operado ="+ ic_proc_operado +
	 					  " GROUP BY co.df_fechaoperacion,  cif.cs_tipo" ;

				rs = con.queryDB(strSQL);
				while (rs.next()){
					String df_operacion = rs.getString("df_fechaoperacion");
					String cs_tipo=rs.getString("cs_tipo");
					int numIFs = rs.getInt("numIFs");
					int num_reg_mn= rs.getInt("totmn");
				    int num_reg_dl= rs.getInt("totdl");
				    int numRegistros = rs.getInt("tot_otro");
					registros += rs.getInt("numregistros");

					strSQL =
						" INSERT INTO comhis_proc_operado (ic_proc_operado, df_operacion,cg_nombre_archivo, df_fecha_hora, ig_num_registros, "+
						" ig_num_ifs,ig_num_reg_mn,ig_num_reg_dl, cs_tipo) " +
						" VALUES ("+ic_proc_operado+","+"to_date('"+df_operacion+"','dd/MM/yyyy')"+ ",'"+nombreArch+"',"+ "to_date('"+fechaHoy+"','dd/mm/yyyy HH24:mi:ss')"+","+numRegistros+","+numIFs+","+num_reg_mn+","+num_reg_dl+",'"+cs_tipo+"')";
					con.ejecutaSQL(strSQL);
				}//fin-while rs
				rs.close();
				con.cierraStatement();

		}catch(SQLException sqle){
			exito = false;
			System.out.println("error:::insertarDatosOper::: "+sqle);
		}catch(Exception e){
			exito = false;
			System.out.println("error:::insertarDatosOper:::"+e);
			e.printStackTrace();
		}finally{
			System.out.println("insertarDatosOper(S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
		lretorno.add(new Boolean(exito));
		lretorno.add(new Integer(registros));
		return lretorno;
	}

	private boolean actualizarIFaTmp(String tipo) throws SQLException{
		System.out.println("actualizarIFaTmp(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement psActualiza = null;
		PreparedStatement psBusca = null;
		PreparedStatement psInserta = null;
		PreparedStatement psElimina = null;
		ResultSet rs = null;
		ResultSet rsBusca = null;
		boolean exito = true;
		String tablacampo = "";
		int cont = 0;

		/*String strSQL =" SELECT ig_if_primer_piso " +
			 			" FROM com_param_gral " +
			 			" WHERE ic_param_gral = 1 ";
		rs = con.queryDB(strSQL);
		rs.next();
		String claveIFPrimerPiso = (rs.getString("ig_if_primer_piso")==null)?"NULL":rs.getString("ig_if_primer_piso");
		rs.close();
		con.cierraStatement();*/

		if(tipo.equals("V"))
			tablacampo = "vencimiento";
		else
			tablacampo = "operado";
		String qry = "";
		String qrySentencia = "SELECT ic_financiera, ic_"+tablacampo+" from comtmp_"+tablacampo;
		String qryBusca = "SELECT ic_if FROM comcat_if WHERE ic_financiera = ?";
		String qryActualiza = "UPDATE comtmp_"+tablacampo+" SET ic_if = ? WHERE ic_"+tablacampo+" = ?";
		String qryElimina = "DELETE comtmp_"+tablacampo+" WHERE ic_"+tablacampo+" = ?";
		String ic_financiera = "", ic = "", ic_if = "";
		try{
			con.conexionDB();
			psActualiza = con.queryPrecompilado(qryActualiza);
			psBusca = con.queryPrecompilado(qryBusca);
			psElimina = con.queryPrecompilado(qryElimina);
			rs = con.queryDB(qrySentencia);
			while(rs.next()){
				ic_financiera = rs.getString("ic_financiera");
				ic = rs.getString("ic_"+tablacampo);
				if(ic_financiera != null && !ic_financiera.equals("") && ic != null && !ic.equals("")){
					psBusca.setInt(1,Integer.parseInt(ic_financiera));
					rsBusca = psBusca.executeQuery();
					while(rsBusca.next()){
						//System.out.println("rs principal");
						ic_if = rsBusca.getString("ic_if");
						if(ic_if != null && !ic_if.equals("")){
							if(cont == 0){
								//System.out.println("primera vez");
								//System.out.println("ic::: "+ic);
								//System.out.println("ic_if: "+ic_if);
								psActualiza.setInt(1,Integer.parseInt(ic_if));
								psActualiza.setInt(2,Integer.parseInt(ic));
								psActualiza.executeUpdate();
							}else{
								//System.out.println("mas de 1 if");
								//System.out.println("ic::: "+ic);
								//System.out.println("ic_if::: "+ic_if);
								if(tipo.equals("V")){
									qry = "INSERT INTO comtmp_vencimiento ("+getcamposBDvencimiento()+", ic_if)" +
											" Select (select nvl(max(ic_vencimiento),0)+1 from comtmp_vencimiento), com_fechaprobablepago,cg_modalidadpago,ic_moneda,ic_financiera,ig_baseoperacion,cg_descbaseoper,ig_cliente,"+
											"cg_nombrecliente,ig_sucursal,ig_proyecto,ig_contrato,ig_prestamo,ig_numelectronico,cg_bursatilizado,ig_disposicion,"+
											"ig_subaplicacion,ig_cuotasemit,ig_cuotasporvenc,ig_totalcuotas,cg_aniomodalidad,ig_tipoestrato,df_fechaopera,"+
											"ig_sancionado,ig_frecuenciacapital,ig_frecuenciainteres,ig_tasabase,cg_esquematasas,cg_relmat_1,fg_spread_1,"+
											"cg_relmat_2,fg_spread_2,cg_relmat_3,fg_spread_3,cg_relmat_4,fg_margen,ig_tipogarantia,fg_porcdescfop,fg_totdescfop,"+
											"fg_porcdescfinape,fg_totdescfinape,fg_fcrecimiento,df_periodoinic,df_periodofin,ig_dias,fg_comision,"+
											"fg_porcgarantia,fg_porccomision,fg_sdoinsoluto,fg_amortizacion,fg_interes,fg_intcobradoxanticip,"+
											"fg_totalvencimiento,fg_pagotrad,fg_subsidio,fg_totalexigible,fg_capitalvencido,fg_interesvencido,"+
											"fg_totalcarven,fg_adeudototal,fg_finadicotorgado,fg_finadicrecup,fg_sdoinsnvo,fg_sdoinsbase,ic_proc_vencimiento, ? from comtmp_vencimiento " +
											" where ic_vencimiento = ?";
									//System.out.println("V:: qry::: "+qry);
									psInserta = con.queryPrecompilado(qry);
									psInserta.setInt(1,Integer.parseInt(ic_if));
									psInserta.setInt(2,Integer.parseInt(ic));
									psInserta.executeUpdate();
									psInserta.close();
								}else{
									qry = "INSERT INTO comtmp_operado ("+getcamposBDoperados()+", ic_if)" +
											" Select (select nvl(max(ic_operado),0)+1 from comtmp_operado),ig_estatus, df_fechaoperacion, ic_moneda, fg_tipocambio, ic_financiera, cg_calificacion, " +
											"ig_baseoperacion, cg_descbaseop, ig_cliente, cg_nombrecliente,	ig_succliente, ig_proyecto, ig_contrato, " +
											"ig_prestamo, ig_numelectronico, ig_disposicion, ig_estado, ig_municipio, ig_subaplicacion, ig_suctram, " +
											"df_fechapripagcap,	ig_tasa, cg_relmat, fg_spread, fg_margen, cg_acteco, cg_aniomod, ig_estrato, cg_afianz,	" +
											"ig_freccap, ig_frecint, ig_numcoutas, ig_tipoamort, cg_modpago, cg_centrofin, ig_sucinter, fg_montooper, " +
											"fg_montoprimcuot, fg_montoultcout, fg_montorecal, fg_montoporgar, fg_comnodisp, fg_intcobradoxanticip, "+
											"ig_tipogar, fg_netootorgado, cg_primabruta, cg_gastos, ic_proc_operado, ? from comtmp_operado " +
											" where ic_operado = ?";
									//System.out.println("O:: qry::: "+qry);
									psInserta = con.queryPrecompilado(qry);
									psInserta.setInt(1,Integer.parseInt(ic_if));
									psInserta.setInt(2,Integer.parseInt(ic));
									psInserta.executeUpdate();
									psInserta.close();
								}
							}
						}
						cont++;
					}
					//System.out.println("cont::: "+cont);
					cont = 0;
					rsBusca.close();
				}
			}// fin while(rs.next())
			psBusca.close();
			psActualiza.close();
			psElimina.close();
		}catch(SQLException sqle){
			exito = false;
			System.out.println("error:::actualizarIFaTmp::: "+sqle);
		}catch(Exception e){
			exito = false;
			System.out.println("error:::actualizarIFaTmp:::"+e);
			e.printStackTrace();
		}finally{
			System.out.println("actualizarIFaTmp(S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
		return exito;
	}

	private void limpiarTablaTmp(String tabla) throws SQLException{
		System.out.println("limpiarTablaTmp(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		boolean exito = true;
		try{
			con.conexionDB();
			String qry = "DELETE "+tabla;
			ps = con.queryPrecompilado(qry);
			ps.executeUpdate();
		}catch(SQLException sqle){
			exito = false;
			System.out.println("error:::limpiarTablaTmp::: "+sqle);
		}catch(Exception e){
			exito = false;
			System.out.println("error:::limpiarTablaTmp:::"+e);
			e.printStackTrace();
		}finally{
			System.out.println("limpiarTablaTmp(S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}

	private boolean validarFechasProceso(String tabla, String campo, String campoBit) throws SQLException{
	//!validarFechasProceso("operado", "df_fechaoperacion", "df_operacion")
		System.out.println("validarFechasProceso(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean exito = false;
		int iexisten = 0;
		try{
			con.conexionDB();
			String qry = "select count(*) as existen from comhis_proc_"+tabla+
						" where "+campoBit+" in " +
						"( " +
						"select "+campo+" " +
						"from comtmp_"+tabla+
						"	group by " +campo+
						")";
			System.out.println("qry:::: "+qry);
			rs = con.queryDB(qry);
			if(rs.next()){
				iexisten = rs.getInt("existen");
			}
			System.out.println("iexisten:::: "+iexisten);
			if(iexisten > 0)
				exito = true;
		}catch(SQLException sqle){
			System.out.println("error:::validarFechasProceso::: "+sqle);
		}catch(Exception e){
			exito = false;
			System.out.println("error:::validarFechasProceso:::"+e);
			e.printStackTrace();
		}finally{
			System.out.println("validarFechasProceso(S)");
			if (con.hayConexionAbierta()){
				//con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
		return exito;
	}


	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}


	public String getTipoArchivo() {
		return tipoArchivo;
	}


	public void setFilePATH(String filePATH) {
		this.filePATH = filePATH;
	}


	public String getFilePATH() {
		return filePATH;
	}


	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}


	public String getNombreArchivo() {
		return nombreArchivo;
	}


	public void setErr(String err) {
		this.err = err;
	}


	public String getErr() {
		return err;
	}


	public void setReg(int reg) {
		this.reg = reg;
	}


	public int getReg() {
		return reg;
	}
}