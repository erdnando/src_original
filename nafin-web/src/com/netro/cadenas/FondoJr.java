package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

public class FondoJr {

  private final static Log log = ServiceLocator.getInstance().getLog(FondoJr.class);
  private String cboCvePrograma;
  private String fechas;
  
  public FondoJr(){}  
  
  /**
   * Este metodo se encarga de generar una consulta a un Programa Fondo Jr
   * especificando la clave del programa en las Variables Bind,
   * setCboCvePrograma(String cboCvePrograma).
   * @return una lista de tipo registros.
   */
  public Registros busqueda(){
    log.info("Busqueda (E)");
    
		StringBuffer  qrySentencia 	= new StringBuffer();
		ArrayList     conditions 		= new ArrayList();
		AccesoDB      con           = new AccesoDB(); 
		Registros     registros     = new Registros();  
  
    try{
			con.conexionDB();
      qrySentencia.append(
      " SELECT rcv.ic_reporte, TO_CHAR (rcv.df_fecha_vmto_ini, 'dd/mm/yyyy') AS df_fecha_vmto_ini, " +
				"       TO_CHAR (rcv.df_fecha_vmto_fin, 'dd/mm/yyyy') AS df_fecha_vmto_fin, rcv.ig_dias_vencimiento, " +
				"       rcv.ig_total_reg, rcv.fg_totalvencimiento, rcv.ig_total_reg_x_venc, " +
				"       rcv.fg_totalvenc_x_venc, " +
				"       TO_CHAR (rcv.df_ejecucion_proc, 'dd/mm/yyyy HH24:mi:ss') AS df_ejecucion_proc, " +
				"       pfjr.cg_descripcion " +
				" FROM com_reporte_cred_venc rcv, comcat_programa_fondojr pfjr " +
				" WHERE rcv.ic_programa_fondojr= pfjr.ic_programa_fondojr " +
				" AND  rcv.ic_reporte IN (SELECT MAX (ic_reporte) " +
				"                        FROM com_reporte_cred_venc " +
				"                       WHERE ic_programa_fondojr = ? ) "
      );
      conditions.add(cboCvePrograma);      
      registros = con.consultarDB(qrySentencia.toString(),conditions); 
    } catch (Exception e) {
			log.error("Busqueda  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
    
    log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
    log.info("Busqueda (S) ");
		return registros;  
  }
  
  /**
   * Este metodo se encarga de ordenar el No de Parametrizacion
   * dentro de la variable fechas,
   * setFechas(String fechas).
   * @return respuesta es el valor de la parametrizacion.
   */
  public String validaFechas (){
  String respuesta= "";
  String anterior= "";
  if(!fechas.equals("")){
    List validaFechas2 = new VectorTokenizer(fechas, "\n").getValuesVector();		
    for(int i = 0; i < validaFechas2.size(); i++) 	{
      if(validaFechas2.get(i)!=null){		
        List unaOperacion = new VectorTokenizer((String)validaFechas2.get(i), ",").getValuesVector();								
          for(int e = 0; e < unaOperacion.size(); e++) 	{											
              String unaOperacion2   = (unaOperacion.size()>= 1)?unaOperacion.get(e)==null?"":((String)unaOperacion.get(e)).trim():"";				 
                if(!unaOperacion.equals("") && !unaOperacion2.equals(anterior)){
                respuesta += unaOperacion2+",";
              }
              anterior = unaOperacion2;
          }
      }						
    }
  }
  respuesta =respuesta.substring(0,respuesta.length()-1);
  return respuesta;
  }


  public void setCboCvePrograma(String cboCvePrograma) {
    this.cboCvePrograma = cboCvePrograma;
  }


  public String getCboCvePrograma() {
    return cboCvePrograma;
  }


  public void setFechas(String fechas)  {
    this.fechas = fechas;
  }


  public String getFechas()  {
    return fechas;
  }
  
}