package com.netro.cadenas;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Element;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPCell;

import com.netro.pdf.ComunesPDF;


import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;

public class BitacoraAceptacionConvenioUnico implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	public BitacoraAceptacionConvenioUnico(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 	strSQL;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String banco_fondeo;
	private String clave_epo;
	private String clave_if;
	private String clave_pyme;
	private String numero_nafele;
	private String nombre_pyme;
	private String fecha_convenio_unico_ini;
	private String fecha_convenio_unico_fin;
	private String directorio_publicacion;
	private String orden;
	public String oper ="";
	
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
    
		System.out.println("..:: BitacoraAceptacionConvenioUnico :: getAggregateCalculationQuery(I)");
    
    strSQL.append(" SELECT COUNT(1) total_registros,");
    strSQL.append(" 'BitacoraAceptacionConvenioUnico :: getAggregateCalculationQuery' origen");
    strSQL.append(" FROM bit_convenio_unico bcu");
    strSQL.append(" , comcat_if cif");
    strSQL.append(" , comcat_pyme pym");
    strSQL.append(" , comrel_nafin crn");
    strSQL.append(" , comrel_pyme_epo cpe");
    strSQL.append(" , comcat_epo epo");
    strSQL.append(" WHERE bcu.ic_if = cif.ic_if");
    strSQL.append(" AND bcu.ic_pyme = pym.ic_pyme");
    strSQL.append(" AND pym.ic_pyme = crn.ic_epo_pyme_if");
    strSQL.append(" AND crn.cg_tipo = ?");
    strSQL.append(" AND bcu.ic_pyme = cpe.ic_pyme");
    strSQL.append(" AND cpe.ic_epo = epo.ic_epo");
    strSQL.append(" AND cpe.cs_habilitado = ?");
    strSQL.append(" AND pym.cs_convenio_unico = ?");
    
    conditions.add("P");
    conditions.add("S");
    conditions.add("S");
    
    //Banco de Fondeo
    if(banco_fondeo != null && !banco_fondeo.equals("")){
      strSQL.append(" AND epo.ic_banco_fondeo = ?");
      conditions.add(new Integer(banco_fondeo));
    }
    //Intermediario Financiero
    if(clave_if != null && !clave_if.equals("")){
      strSQL.append(" AND bcu.ic_if = ?");
      conditions.add(new Long(clave_if.trim()));
    }
    //PyME
    if(numero_nafele != null && !numero_nafele.equals("")){
      strSQL.append(" AND crn.ic_nafin_electronico = ?");
      conditions.add(new Long(numero_nafele));
    }
    //EPO
    if(clave_epo != null && !clave_epo.equals("")){
      strSQL.append(" AND cpe.ic_epo = ?");
      conditions.add(new Long(clave_epo));
    }
    //Fechas de Aceptaci�n de Convenio �nico
    if(fecha_convenio_unico_ini != null && !fecha_convenio_unico_ini.equals("") && fecha_convenio_unico_fin != null && !fecha_convenio_unico_fin.equals("")){
      //strSQL.append(" AND bcu.df_autorizacion_if BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
      strSQL.append(" AND bcu.df_aceptacion_pyme >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND bcu.df_aceptacion_pyme < TO_DATE(?, 'dd/mm/yyyy') + 1");
      conditions.add(fecha_convenio_unico_ini);
      conditions.add(fecha_convenio_unico_fin);
    }
    
		System.out.println("strSQL:: " + strSQL.toString());
		System.out.println("conditions:: " + conditions);
		
		//System.out.println("..:: BitacoraAceptacionConvenioUnico :: getAggregateCalculationQuery(F) " + strSQL);
		return strSQL.toString();
	}//getAggregateCalculationQuery
	
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
     
		 System.out.println("..:: BitacoraAceptacionConvenioUnico :: getDocumentQuery(I)");
		if(oper.equals("Clausulado")){
			strSQL.append(" SELECT DISTINCT pym.cg_rfc rfc,");
				strSQL.append(" pym.cg_razon_social nombre_pyme,");
				strSQL.append(" crn.ic_nafin_electronico numero_nafele,");
				strSQL.append(" pym.in_numero_sirac numero_sirac,");
				strSQL.append(" TO_CHAR(bcu.df_autorizacion_if, 'dd/mm/yyyy hh:mi') || ' Hrs. ' || bcu.ic_usuario fecha_aut_if,");
				strSQL.append(" TO_CHAR(bcu.df_aceptacion_pyme, 'dd/mm/yyyy hh:mi') || ' Hrs.' fecha_acep_pyme,");
				strSQL.append(" TO_CHAR(pym.df_firma_conv_unico, 'dd/mm/yyyy hh:mi') || ' Hrs.' fecha_firma_pyme");
				strSQL.append(" FROM bit_convenio_unico bcu");
				strSQL.append(" , comcat_if cif");
				strSQL.append(" , comcat_pyme pym");
				strSQL.append(" , comrel_nafin crn");
				strSQL.append(" , comrel_pyme_epo cpe");
				strSQL.append(" , comcat_epo epo");
				strSQL.append(" WHERE bcu.ic_if = cif.ic_if");
				strSQL.append(" AND bcu.ic_pyme = pym.ic_pyme");
				strSQL.append(" AND pym.ic_pyme = crn.ic_epo_pyme_if");
				strSQL.append(" AND crn.cg_tipo = ?");
				strSQL.append(" AND bcu.ic_pyme = cpe.ic_pyme");
				strSQL.append(" AND cpe.ic_epo = epo.ic_epo");
				strSQL.append(" AND cpe.cs_habilitado = ?");
				strSQL.append(" AND pym.cs_convenio_unico = ?");
				strSQL.append(" AND bcu.ic_if = ?");
				strSQL.append(" AND bcu.ic_pyme = ?");
				
				conditions.add("P");
				conditions.add("S");
				conditions.add("S");
				conditions.add(new Long(clave_if));
				conditions.add(new Long(clave_pyme));
            
            
			 
			 //Banco de Fondeo
			 if(banco_fondeo != null && !banco_fondeo.equals("")){
				strSQL.append(" AND epo.ic_banco_fondeo = ?");
				conditions.add(new Integer(banco_fondeo));
			 }
			 //Intermediario Financiero
			 if(clave_if != null && !clave_if.equals("")){
				strSQL.append(" AND bcu.ic_if = ?");
				conditions.add(new Long(clave_if.trim()));
			 }
			 //PyME
			 if(numero_nafele != null && !numero_nafele.equals("")){
				strSQL.append(" AND crn.ic_nafin_electronico = ?");
				conditions.add(new Long(numero_nafele));
			 }
			 //EPO
			 if(clave_epo != null && !clave_epo.equals("")){
				strSQL.append(" AND cpe.ic_epo = ?");
				conditions.add(new Long(clave_epo));
			 }
			 //Fechas de Aceptaci�n de Convenio �nico
			 if(!fecha_convenio_unico_ini.equals("") && !fecha_convenio_unico_fin.equals("")){
				//strSQL.append(" AND bcu.df_autorizacion_if BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
            strSQL.append(" AND bcu.df_aceptacion_pyme >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND bcu.df_aceptacion_pyme < TO_DATE(?, 'dd/mm/yyyy') + 1");
				conditions.add(fecha_convenio_unico_ini);
				conditions.add(fecha_convenio_unico_fin);
			 }
	
			System.out.println("strSQL (Clausulado) :::::: " + strSQL.toString());
			System.out.println("conditions:: " + conditions);	
		}else{
			strSQL.append(" SELECT DISTINCT bcu.ic_if ic_if, bcu.ic_pyme ic_pyme,");
			 strSQL.append(" 'BitacoraAceptacionConvenioUnico :: getDocumentQuery' origen");
			 strSQL.append(" FROM bit_convenio_unico bcu");
			 strSQL.append(" , comcat_if cif");
			 strSQL.append(" , comcat_pyme pym");
			 strSQL.append(" , comrel_nafin crn");
			 strSQL.append(" , comrel_pyme_epo cpe");
			 strSQL.append(" , comcat_epo epo");
			 strSQL.append(" WHERE bcu.ic_if = cif.ic_if");
			 strSQL.append(" AND bcu.ic_pyme = pym.ic_pyme");
			 strSQL.append(" AND pym.ic_pyme = crn.ic_epo_pyme_if");
			 strSQL.append(" AND crn.cg_tipo = ?");
			 strSQL.append(" AND bcu.ic_pyme = cpe.ic_pyme");
			 strSQL.append(" AND cpe.ic_epo = epo.ic_epo");
			 strSQL.append(" AND cpe.cs_habilitado = ?");
			 strSQL.append(" AND pym.cs_convenio_unico = ?");
			 
			 conditions.add("P");
			 conditions.add("S");
			 conditions.add("S");
			 
			 //Banco de Fondeo
			 if(banco_fondeo != null && !banco_fondeo.equals("")){
				strSQL.append(" AND epo.ic_banco_fondeo = ?");
				conditions.add(new Integer(banco_fondeo));
			 }
			 //Intermediario Financiero
			 if(clave_if != null && !clave_if.equals("")){
				strSQL.append(" AND bcu.ic_if = ?");
				conditions.add(new Long(clave_if.trim()));
			 }
			 //PyME
			 if(numero_nafele != null && !numero_nafele.equals("")){
				strSQL.append(" AND crn.ic_nafin_electronico = ?");
				conditions.add(new Long(numero_nafele));
			 }
			 //EPO
			 if(clave_epo != null && !clave_epo.equals("")){
				strSQL.append(" AND cpe.ic_epo = ?");
				conditions.add(new Long(clave_epo));
			 }
			 //Fechas de Aceptaci�n de Convenio �nico
			 if(!fecha_convenio_unico_ini.equals("") && !fecha_convenio_unico_fin.equals("")){
				//strSQL.append(" AND bcu.df_autorizacion_if BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
            strSQL.append(" AND bcu.df_aceptacion_pyme >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND bcu.df_aceptacion_pyme < TO_DATE(?, 'dd/mm/yyyy') + 1");
            
				conditions.add(fecha_convenio_unico_ini);
				conditions.add(fecha_convenio_unico_fin);
			 }
	
			System.out.println("strSQL (NO Clausulado) :::::  " + strSQL.toString());
			System.out.println("conditions:: " + conditions);			
		}
		// System.out.println("..:: BitacoraAceptacionConvenioUnico :: getDocumentQuery(F) " + strSQL);

		return strSQL.toString();
	}//getDocumentQuery  
	
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
    
    System.out.println("..:: BitacoraAceptacionConvenioUnico :: getDocumentSummaryQueryForIds(I)");
    
    strSQL.append(" SELECT DISTINCT bcu.ic_if ic_if,");
    strSQL.append(" bcu.ic_pyme ic_pyme,");
    strSQL.append(" cif.cg_razon_social nombre_if,");
    strSQL.append(" TO_CHAR(cif.df_alta_convenio_unico, 'dd/mm/yyyy hh:mi') || ' Hrs.' fecha_afil_if,");
    strSQL.append(" TO_CHAR(bcu.df_autorizacion_if, 'dd/mm/yyyy hh:mi') || ' Hrs. ' || bcu.ic_usuario fecha_aut_if,");
    strSQL.append(" crn.ic_nafin_electronico numero_nafele,");
    strSQL.append(" pym.in_numero_sirac numero_sirac,");
    strSQL.append(" pym.cg_rfc rfc,");
    strSQL.append(" pym.cg_razon_social nombre_pyme,");
    strSQL.append(" TO_CHAR(bcu.df_aceptacion_pyme, 'dd/mm/yyyy hh:mi') || ' Hrs.' fecha_acep_pyme,");
    strSQL.append(" TO_CHAR(pym.df_firma_conv_unico, 'dd/mm/yyyy hh:mi') || ' Hrs.' fecha_firma_pyme,");
    strSQL.append(" 'BitacoraAceptacionConvenioUnico :: getDocumentSummaryQueryForIds' origen");
    strSQL.append(" FROM bit_convenio_unico bcu");
    strSQL.append(" , comcat_if cif");
    strSQL.append(" , comcat_pyme pym");
    strSQL.append(" , comrel_nafin crn");
    strSQL.append(" , comrel_pyme_epo cpe");
    strSQL.append(" , comcat_epo epo");
    strSQL.append(" WHERE bcu.ic_if = cif.ic_if");
    strSQL.append(" AND bcu.ic_pyme = pym.ic_pyme");
    strSQL.append(" AND pym.ic_pyme = crn.ic_epo_pyme_if");
    strSQL.append(" AND crn.cg_tipo = ?");
    strSQL.append(" AND bcu.ic_pyme = cpe.ic_pyme");
    strSQL.append(" AND cpe.ic_epo = epo.ic_epo");
    strSQL.append(" AND cpe.cs_habilitado = ?");
    strSQL.append(" AND pym.cs_convenio_unico = ?");
    strSQL.append(" AND (");
    
    conditions.add("P");
    conditions.add("S");
    conditions.add("S");
    
		for(int i = 0; i < pageIds.size(); i++){
      List lItem = (ArrayList)pageIds.get(i);
			
      if(i > 0){strSQL.append("  OR  ");}
      
			strSQL.append("(bcu.ic_if = ? AND bcu.ic_pyme = ?)");
      conditions.add(new Long(lItem.get(0).toString()));
      conditions.add(new Long(lItem.get(1).toString()));
		}
    strSQL.append(" ) ");
    
    //strSQL.append(" ORDER BY bcu.df_aceptacion_pyme");

		System.out.println("strSQL:: " + strSQL.toString());
		System.out.println("conditions:: " + conditions);
		
		//System.out.println("..:: BitacoraAceptacionConvenioUnico :: getDocumentSummaryQueryForIds(F) " + strSQL);
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
    
    System.out.println("..:: BitacoraAceptacionConvenioUnico :: getDocumentQueryFile(I)");
    
    strSQL.append(" SELECT DISTINCT cif.cg_razon_social INTERMEDIARIO_FINANCIERO,");
    strSQL.append(" TO_CHAR(cif.df_alta_convenio_unico, 'dd/mm/yyyy hh:mi') || ' Hrs.' FECHA_DE_AFILIACION_DEL_IF,");
    strSQL.append(" TO_CHAR(bcu.df_autorizacion_if, 'dd/mm/yyyy hh:mi') || ' Hrs. ' || bcu.ic_usuario FECHA_ACEPT_IF_CTA_BANC_USR,");
    strSQL.append(" crn.ic_nafin_electronico NUMERO_ELECTRONICO,");
    strSQL.append(" pym.in_numero_sirac CLAVE_SIRAC,");
    strSQL.append(" pym.cg_rfc RFC,");
    strSQL.append(" pym.cg_razon_social NOMBRE_PYME,");
    strSQL.append(" TO_CHAR(bcu.df_aceptacion_pyme, 'dd/mm/yyyy hh:mi') || ' Hrs.' FECHA_ACEPT_CU_ELECTRONICO,");
    strSQL.append(" TO_CHAR(pym.df_firma_conv_unico, 'dd/mm/yyyy hh:mi') || ' Hrs.' FECHA_FIRMA_AUTOGRAFA_CU");
    strSQL.append(" FROM bit_convenio_unico bcu");
    strSQL.append(" , comcat_if cif");
    strSQL.append(" , comcat_pyme pym");
    strSQL.append(" , comrel_nafin crn");
    strSQL.append(" , comrel_pyme_epo cpe");
    strSQL.append(" , comcat_epo epo");
    strSQL.append(" WHERE bcu.ic_if = cif.ic_if");
    strSQL.append(" AND bcu.ic_pyme = pym.ic_pyme");
    strSQL.append(" AND pym.ic_pyme = crn.ic_epo_pyme_if");
    strSQL.append(" AND crn.cg_tipo = ?");
    strSQL.append(" AND bcu.ic_pyme = cpe.ic_pyme");
    strSQL.append(" AND cpe.ic_epo = epo.ic_epo");
    strSQL.append(" AND cpe.cs_habilitado = ?");
    strSQL.append(" AND pym.cs_convenio_unico = ?");
    
    conditions.add("P");
    conditions.add("S");
    conditions.add("S");
    
    //Banco de Fondeo
    if(banco_fondeo != null && !banco_fondeo.equals("")){
      strSQL.append(" AND epo.ic_banco_fondeo = ?");
      conditions.add(new Integer(banco_fondeo));
    }
    //Intermediario Financiero
    if(clave_if != null && !clave_if.equals("")){
      strSQL.append(" AND bcu.ic_if = ?");
      conditions.add(new Long(clave_if.trim()));
    }
    //PyME
    if(numero_nafele != null && !numero_nafele.equals("")){
      strSQL.append(" AND crn.ic_nafin_electronico = ?");
      conditions.add(new Long(numero_nafele));
    }
    //EPO
    if(clave_epo != null && !clave_epo.equals("")){
      strSQL.append(" AND cpe.ic_epo = ?");
      conditions.add(new Long(clave_epo));
    }
    //Fechas de Aceptaci�n de Convenio �nico
    if(fecha_convenio_unico_ini != null && !fecha_convenio_unico_ini.equals("") && fecha_convenio_unico_fin != null && !fecha_convenio_unico_fin.equals("")){
      //strSQL.append(" AND bcu.df_autorizacion_if BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
      strSQL.append(" AND bcu.df_aceptacion_pyme >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND bcu.df_aceptacion_pyme < TO_DATE(?, 'dd/mm/yyyy') + 1");
      conditions.add(fecha_convenio_unico_ini);
      conditions.add(fecha_convenio_unico_fin);
    }
    
    //strSQL.append(" ORDER BY bcu.df_aceptacion_pyme");

		System.out.println("strSQL:: " + strSQL.toString());
		System.out.println("conditions:: " + conditions);
		
		//System.out.println("..:: BitacoraAceptacionConvenioUnico :: getDocumentQueryFile(F) " + strSQL);
		return strSQL.toString();
	}//getDocumentQueryFile
	
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
			
		if ("XLS".equals(tipo)) {
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";	  
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);	
				
				int registros = 0;
				int total = 0;
				
				while(rs.next()){
					if(registros==0) {
						contenidoArchivo.append("INTERMEDIARIO FINANCIERO,FECHA DE AFILIACION DEL IF,FECH ACEPT IF CTA BANC USR,NUMERO ELECTRONICO,CLAVE SIRAC,RFC,NOMBRE PYME,FECHA ACEPT CU ELECTRONICO,FECHA FIRMA AUTOGRAFA CU");
					}
					registros++;		
					String 	operacion   	= ( request.getParameter("operacion") == null ) ? "1" : request.getParameter("operacion");
					String snom_if 			= rs.getString("intermediario_financiero")==null?"":rs.getString("intermediario_financiero");
					String sfecha_afil_if	= rs.getString("fecha_de_afiliacion_del_if")==null?"":rs.getString("fecha_de_afiliacion_del_if");
					String sfecha_aut_if 	= rs.getString("fecha_acept_if_cta_banc_usr")==null?"":rs.getString("fecha_acept_if_cta_banc_usr");
					String snumero_nafele	= rs.getString("numero_electronico")==null?"":rs.getString("numero_electronico");
					String snumero_sirac 	= rs.getString("clave_sirac")==null?"":rs.getString("clave_sirac");
					String srfc					= rs.getString("rfc")==null?"":rs.getString("rfc"); 
					String snombre_pyme 		= rs.getString("nombre_pyme")==null?"":rs.getString("nombre_pyme");
					String sfecha_acep_pyme	= rs.getString("fecha_acept_cu_electronico")==null?"":rs.getString("fecha_acept_cu_electronico");
					String sfecha_firma_pyme= rs.getString("fecha_firma_autografa_cu")==null?"":rs.getString("fecha_firma_autografa_cu");	
											
					contenidoArchivo.append("\n"+snom_if.replace(',',' ')+","+sfecha_afil_if.replace(',',' ')+","+sfecha_aut_if.replace(',',' ')+","+snumero_nafele.replace(',',' ')+
													","+snumero_sirac.replace(',',' ')+","+srfc.replace(',',' ')+","+snombre_pyme.replace(',',' ')+","+sfecha_acep_pyme.replace(',',' ')+
													","+sfecha_firma_pyme.replace(',',' '));
				
					total++;
					if(total==1000){					
						total=0;						
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio    
					}										
				}
				
				buffer.write(contenidoArchivo.toString());
				buffer.close();	
				contenidoArchivo = new StringBuffer();//Limpio    
													
				/*if(registros >= 1){
					if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
						nombreArchivo="ERROR";
					}else{
						nombreArchivo = archivo.nombre;
					}
				}*/
				
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}else if(tipo.equals("Clausulado")){
			List datos_convenio_unico = new ArrayList();
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String rfc_pyme = "";
				String nombre_pyme = "";
				
				
				while(rs.next()){
					rfc_pyme =rs.getString(1);
					nombre_pyme = rs.getString(2);
					datos_convenio_unico.add(rs.getString(3));
					datos_convenio_unico.add(rs.getString(4));
					datos_convenio_unico.add(rs.getString(5));
					datos_convenio_unico.add(rs.getString(6));
					datos_convenio_unico.add(rs.getString(7));
				}
				
				rs.close();
				
				//Estas fuentes va a servir para ejemplificar como se puede dar formato al contenido que se va a sobreescribir en el pdf.
				Font		formas = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.NORMAL);
				Font		formasB = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.BOLD);
				Font		formasrep = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.NORMAL);
				Font		formasrepB = FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD);
				Font		formasmen = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.NORMAL);
				Font		formasmenB = FontFactory.getFont(FontFactory.HELVETICA, 7, Font.BOLD);
				Font		formasG = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLD);
				Font		celda02G = FontFactory.getFont(FontFactory.HELVETICA, 11, Font.BOLD);
				Font    	subrayado = FontFactory.getFont(FontFactory.HELVETICA, 9, Font.UNDERLINE);
			
				int	LEFT 			= Element.ALIGN_LEFT;
				int	CENTER 		= Element.ALIGN_CENTER;
				int	RIGHT 		= Element.ALIGN_RIGHT;
				int	JUSTIFIED 	= Element.ALIGN_JUSTIFIED;
				
				//Estableciendo Content Type
				//response.setContentType("application/download");
			
				//Se crean los respectivos reader y stamper del pdf.
			//	PdfReader reader = new PdfReader(strDirTrabajo+"contrato_unico_2009.pdf");
				//PdfStamper stamper = new PdfStamper(reader, response.getOutputStream());
			
				//Al reader se le pide el numero de paginas con lo que sabremos el numero de paginas del archivo.
				//int paginas = reader.getNumberOfPages();
				
				//float[] datos_pyme = {20f, 30f, 20f, 30f};
				float[] datos_pyme = {20f, 30f, 50f};
				
				PdfPTable tabla_datos_pyme = new PdfPTable(datos_pyme);
								
				tabla_datos_pyme.setTotalWidth(400);
				tabla_datos_pyme.getDefaultCell().setBorderColor(BaseColor.WHITE);
				tabla_datos_pyme.getDefaultCell().setVerticalAlignment(CENTER);
				tabla_datos_pyme.getDefaultCell().setPadding(30f);
				tabla_datos_pyme.getDefaultCell().setBorderWidth(30f);
				
				PdfPCell cell  = new PdfPCell(new Phrase("RFC: ", formasB));
				cell.setBorderColor( BaseColor.WHITE);
				cell.setVerticalAlignment(CENTER);
				cell.setHorizontalAlignment(CENTER);
				tabla_datos_pyme.addCell(cell);
			
				cell  = new PdfPCell(new Phrase(rfc_pyme, formasB));
				cell.setBorderColor( BaseColor.WHITE);
				cell.setVerticalAlignment(CENTER);
				cell.setHorizontalAlignment(CENTER);
				tabla_datos_pyme.addCell(cell);
			
				cell  = new PdfPCell(new Phrase(nombre_pyme, formasB));
				cell.setBorderColor( BaseColor.WHITE);
				cell.setVerticalAlignment(CENTER);
				cell.setHorizontalAlignment(CENTER);
				tabla_datos_pyme.addCell(cell);
				
				//tabla_datos_pyme.writeSelectedRows(0, -1, 60f, 730f, stamper.getOverContent(reader.getNumberOfPages()));
				
				float[] contenidoEncabezado  = {20f, 20f, 20f, 20f, 20f};
				
				PdfPTable tablaEncabezado = new PdfPTable(contenidoEncabezado);
				
				tablaEncabezado.setTotalWidth(490);
				tablaEncabezado.getDefaultCell().setBorderColor(BaseColor.BLACK);
				tablaEncabezado.getDefaultCell().setVerticalAlignment(CENTER);
				tablaEncabezado.getDefaultCell().setPadding(30f);
				tablaEncabezado.getDefaultCell().setBorderWidth(30f);
			
				cell  = new PdfPCell(new Phrase("N�mero Electr�nico", formasB) );
				cell.setBorderColor( BaseColor.BLACK);
				cell.setVerticalAlignment(CENTER);
				cell.setHorizontalAlignment(CENTER);
				tablaEncabezado.addCell(cell);
			
				cell  = new PdfPCell(new Phrase("Clave SIRAC", formasB) );
				cell.setBorderColor( BaseColor.BLACK);
				cell.setVerticalAlignment(CENTER);
				cell.setHorizontalAlignment(CENTER);
				tablaEncabezado.addCell(cell);
			
				cell  = new PdfPCell(new Phrase("Fecha de Aceptaci�n IF de Cuenta Bancaria / Usuario", formasB) );
				cell.setBorderColor( BaseColor.BLACK);
				cell.setVerticalAlignment(CENTER);
				cell.setHorizontalAlignment(CENTER);
				tablaEncabezado.addCell(cell);
			
				cell  = new PdfPCell(new Phrase("Fecha de Aceptaci�n del Convenio �nico por la PyME", formasB) );
				cell.setBorderColor( BaseColor.BLACK);
				cell.setVerticalAlignment(CENTER);
				cell.setHorizontalAlignment(CENTER);
				tablaEncabezado.addCell(cell);
			
				cell  = new PdfPCell(new Phrase("Fecha de Firma Aut�grafa de Convenio �nico", formasB) );
				cell.setBorderColor( BaseColor.BLACK);
				cell.setVerticalAlignment(CENTER);
				cell.setHorizontalAlignment(CENTER);
				tablaEncabezado.addCell(cell);
			
				
				for(int i = 0; i < datos_convenio_unico.size(); i++){
					cell  = new PdfPCell(new Phrase((String)datos_convenio_unico.get(i), formas) );
					cell.setBorderColor( BaseColor.BLACK);
					cell.setVerticalAlignment(CENTER);
					cell.setHorizontalAlignment(CENTER);
					tablaEncabezado.addCell(cell);
				}
				
				//tablaEncabezado.writeSelectedRows(0, -1, 60f, 700f, stamper.getOverContent(reader.getNumberOfPages()));
				//stamper.close();
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"+ nombreArchivo);
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {	
				try {
					//rs.close();
				} catch(Exception e) {}
			}		
		}
		return nombreArchivo;	
	}
	// por pagina 15 registros
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo 	= new CreaArchivo(); 
		
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico") == null ? "" : session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Bitacora de Aceptaci�n Convenio �nico\n","formas",ComunesPDF.CENTER);
				
            pdfDoc.setTable(9,100);
            pdfDoc.setCell("","formasmenB",ComunesPDF.CENTER,2);            
            pdfDoc.setCell("Datos del Proveedor","formasmenB",ComunesPDF.CENTER,7);

				pdfDoc.setCell("Intermediario Financiero", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Afiliaci�n del IF", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Aceptaci�n IF de Cuenta Bancaria / Usuario", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Electr�nico", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Clave SIRAC", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("RFC", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre PyME", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Aceptaci�n del Convenio �nico Electr�nico", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Firma Aut�grafa de C.U.", "formasmenB", ComunesPDF.CENTER);
				
				while (rs.next()) {
					String snom_if 			= rs.getString("nombre_if")==null?"":rs.getString("nombre_if");
					String sfecha_afil_if	= rs.getString("fecha_afil_if")==null?"":rs.getString("fecha_afil_if");
					String sfecha_aut_if 	= rs.getString("fecha_aut_if")==null?"":rs.getString("fecha_aut_if");
					String snumero_nafele	= rs.getString("numero_nafele")==null?"":rs.getString("numero_nafele");
					String snumero_sirac 	= rs.getString("numero_sirac")==null?"":rs.getString("numero_sirac");
					String srfc					= rs.getString("rfc")==null?"":rs.getString("rfc"); 
					String snombre_pyme 		= rs.getString("nombre_pyme")==null?"":rs.getString("nombre_pyme");
					String sfecha_acep_pyme	= rs.getString("fecha_acep_pyme")==null?"":rs.getString("fecha_acep_pyme");
					String sfecha_firma_pyme= rs.getString("fecha_firma_pyme")==null?"":rs.getString("fecha_firma_pyme");	
												
					pdfDoc.setCell(snom_if,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(sfecha_afil_if,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(sfecha_aut_if,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(snumero_nafele,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(snumero_sirac,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(srfc,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(snombre_pyme,"formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(sfecha_acep_pyme,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(sfecha_firma_pyme,"formas",ComunesPDF.LEFT);										
				}
				
				pdfDoc.addTable();
					pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
		}		
		return nombreArchivo;
	}
	
   
	//GETTERS
  /**
	 * Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions() {
		return conditions;
	}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {
		return paginaNo;
	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() {
		return paginaOffset;
	}
	
	//atributos del formulario
	public String getBanco_fondeo(){return banco_fondeo;}
	public String getClave_epo(){return clave_epo;}
	public String getClave_if(){return clave_if;}
	public String getClave_pyme(){return clave_pyme;}
	public String getNumero_nafele(){return numero_nafele;}
	public String getNombre_pyme(){return nombre_pyme;}
	public String getFecha_convenio_unico_ini(){return fecha_convenio_unico_ini;}
	public String getFecha_convenio_unico_fin(){return fecha_convenio_unico_fin;}
	public String getDirectorio_publicacion(){return directorio_publicacion;}
	public String getOrden(){return orden;}
	
	//SETTERS
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {
		paginaNo = newPaginaNo;
	}
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
		
	//atributos del formulario
	public void setBanco_fondeo(String banco_fondeo){this.banco_fondeo = banco_fondeo;}
	public void setClave_epo(String clave_epo){this.clave_epo = clave_epo;}
	public void setClave_if(String clave_if){this.clave_if = clave_if;}
	public void setClave_pyme(String clave_pyme){this.clave_pyme = clave_pyme;}
	public void setNumero_nafele(String numero_nafele){this.numero_nafele = numero_nafele;}
	public void setNombre_pyme(String nombre_pyme){this.nombre_pyme = nombre_pyme;}
	public void setFecha_convenio_unico_ini(String fecha_convenio_unico_ini){this.fecha_convenio_unico_ini = fecha_convenio_unico_ini;}
	public void setFecha_convenio_unico_fin(String fecha_convenio_unico_fin){this.fecha_convenio_unico_fin = fecha_convenio_unico_fin;}
	public void setDirectorio_publicacion(String directorio_publicacion){this.directorio_publicacion = directorio_publicacion;}
	public void setOrden(String orden){this.orden = orden;}


	public void setOper(String oepr) {
		this.oper = oper;
	}
}
