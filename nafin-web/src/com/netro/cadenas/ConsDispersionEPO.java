package com.netro.cadenas;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;

import com.netro.cesion.ConsultaNotificacionesCesionDerechos;
import com.netro.dispersion.Dispersion;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsDispersionEPO implements IQueryGeneratorRegExtJS 
{
	public ConsDispersionEPO() { }
	
	private List conditions;
	StringBuffer strQuery;
	//Variable para enviar mensajes al log
	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaNotificacionesCesionDerechos.class);
	
	//Condiciones de la consulta
	private String tipoDispersion;
	private String fechaRegIni;
	private String fechaRegFin;
	private String viaLiq;
	private String icEpo;
	private String icIf;
	private String estatus;
	private String estatusFF;
	
	private String condicion_1;
	private String condicion_2;
	private String condicion_3;
	private String epoEspecifica;
   private String ifEspecifica;
   private String condvia;
	private String condConsultaEpo;
	
	public String getQuery(boolean tipoConsulta, int condicion){
	   String strQuery = "";
		
		if(condicion == 1){
		strQuery = (tipoConsulta?
		                " SELECT e.cg_razon_social, i.cg_razon_social as crsI, p.cg_razon_social as crsP, p.cg_rfc, " +
		                "        c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta, ff.fn_importe, " +
							 "        me.error_cen_s, ff.ic_flujo_fondos, " +
							 "        me.status_cen_i || ' ' || eff.cd_descripcion as estatus, " +
							 "        TO_CHAR (ff.df_registro, 'dd/mm/yyyy') as df_registro , ff.cg_via_liquidacion, " +
							 "        c.ic_estatus_cecoban, c.ic_cuenta, ff.cs_reproceso, e.ic_epo, " +
							 "        ff.ic_pyme, ff.ic_if, ff.ic_estatus_docto, ff.df_fecha_venc, " +
							 "        ff.df_operacion, 'FFON' origen, me.status_cen_i, " +
							 "        btef.cc_clave_dcat_s || ' - ' || btef.cd_descripcion as inst_cuenta_benef " +
							 "   FROM int_flujo_fondos ff, cfe_m_encabezado me, " +
							 "        comrel_nafin crn, comcat_epo e, " +
							 "        comcat_if i, comcat_pyme p, " +
							 "        com_cuentas c, comcat_estatus_ff eff, " +
							 "        comcat_bancos_tef btef " +
							 "  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i " +
							 "    AND ff.ic_epo = e.ic_epo " +
							 "    AND ff.ic_if = i.ic_if(+) " +
							 "    AND ff.ic_pyme = p.ic_pyme(+) " +
							 "    AND ff.ic_cuenta = c.ic_cuenta " +
							 "    AND c.ic_bancos_tef = btef.ic_bancos_tef " +
							 "    AND me.status_cen_i = eff.ic_estatus_ff(+) " +
							 "    AND ff.cs_reproceso ='N' " +
							 "    AND ff.cs_concluido = 'N' "
                      :
							 " SELECT sum(totalOperacion) as totalOperacion, sum(totalImporteTD) as totalImporteTD " +
		                "   FROM ( " +
		                " SELECT count(1) as totalOperacion, Sum(ff.fn_importe) as totalImporteTD " +
							 "   FROM int_flujo_fondos ff, cfe_m_encabezado me, " +
							 "        comrel_nafin crn, comcat_epo e, " +
							 "        comcat_if i, comcat_pyme p, " +
							 "        com_cuentas c, comcat_estatus_ff eff, " +
							 "        comcat_bancos_tef btef " +
							 "  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i " +
							 "    AND ff.ic_epo = e.ic_epo " +
							 "    AND ff.ic_if = i.ic_if(+) " +
							 "    AND ff.ic_pyme = p.ic_pyme(+) " +
							 "    AND ff.ic_cuenta = c.ic_cuenta " +
							 "    AND c.ic_bancos_tef = btef.ic_bancos_tef " +
							 "    AND me.status_cen_i = eff.ic_estatus_ff(+) " +
							 "    AND ff.cs_reproceso ='N' " +
							 "    AND ff.cs_concluido = 'N' "
						) +
						(condicion_1 + condvia + epoEspecifica + ifEspecifica+condConsultaEpo);
		}
		else if(condicion == 2){					 							 
	   strQuery =  "  UNION ALL " +
		            (tipoConsulta?
							 " SELECT e.cg_razon_social, i.cg_razon_social as crsI, p.cg_razon_social as crsP, p.cg_rfc, "+
							 "        c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta, ff.fn_importe, " +
							 "        me.error_cen_s, ff.ic_flujo_fondos, " +
							 "        me.status_cen_i || ' ' || eff.cd_descripcion as estatus, " +
							 "        TO_CHAR (ff.df_registro, 'dd/mm/yyyy') as df_registro, ff.cg_via_liquidacion, " +
							 "        c.ic_estatus_cecoban, c.ic_cuenta, ff.cs_reproceso, e.ic_epo, " +
							 "        ff.ic_pyme, ff.ic_if, ff.ic_estatus_docto, ff.df_fecha_venc, " +
							 "        ff.df_operacion, 'ERR' origen, me.status_cen_i, " +
							 "        ' '  as inst_cuenta_benef " +
							 "   FROM int_flujo_fondos_err ff, cfe_m_encabezado_err me, " +
							 "        comrel_nafin crn, comcat_epo e, comcat_if i, " +
							 "        comcat_pyme p, com_cuentas c, comcat_estatus_ff eff " +
							 "  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i " +
							 "    AND ff.ic_epo = e.ic_epo " +
							 "    AND ff.ic_if = i.ic_if(+) " +
							 "    AND ff.ic_pyme = p.ic_pyme(+) " +
							 "    AND ff.ic_cuenta = c.ic_cuenta(+) " +
							 "    AND me.status_cen_i = eff.ic_estatus_ff(+) " +
							 "    AND ff.cs_reproceso ='N' " +
							 "    AND ff.cs_concluido = 'N' "
							 :
							 " SELECT count(1) as totalOperacion, Sum(ff.fn_importe) as totalImporteTD " +
							 "   FROM int_flujo_fondos_err ff, cfe_m_encabezado_err me, " +
							 "        comrel_nafin crn, comcat_epo e, comcat_if i, " +
							 "        comcat_pyme p, com_cuentas c, comcat_estatus_ff eff " +
							 "  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i " +
							 "    AND ff.ic_epo = e.ic_epo " +
							 "    AND ff.ic_if = i.ic_if(+) " +
							 "    AND ff.ic_pyme = p.ic_pyme(+) " +
							 "    AND ff.ic_cuenta = c.ic_cuenta(+) " +
							 "    AND me.status_cen_i = eff.ic_estatus_ff(+) " +
							 "    AND ff.cs_reproceso ='N' " +
							 "    AND ff.cs_concluido = 'N' "
		           ) +
		           (condicion_2 + condvia + epoEspecifica + ifEspecifica+condConsultaEpo);
		}
      else if(condicion == 3){
	   strQuery = "  UNION ALL " +
		           (tipoConsulta?
							 " SELECT e.cg_razon_social, i.cg_razon_social as crsI, p.cg_razon_social as crsP, p.cg_rfc, " +
							 "        c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta, ff.fn_importe, NULL, " +
							 "        ff.ic_flujo_fondos, me.status_cen_i || ' ' || eff.cd_descripcion as estatus, " +
							 "        TO_CHAR (ff.df_registro, 'dd/mm/yyyy') as df_registro, ff.cg_via_liquidacion, " +
							 "        c.ic_estatus_cecoban, c.ic_cuenta, ff.cs_reproceso, e.ic_epo, " +
							 "        ff.ic_pyme, ff.ic_if, ff.ic_estatus_docto, ff.df_fecha_venc, " +
							 "        ff.df_operacion, 'FFON' origen, me.status_cen_i, ' ' as inst_cuenta_benef " +
							 "   FROM int_flujo_fondos ff, cfe_m_encabezado me, " +
							 "        comcat_epo e, comcat_if i, " +
							 "        comcat_pyme p, com_cuentas c, " +
							 "        comcat_estatus_ff eff " +
							 "  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i" +
							 "    AND ff.ic_epo = e.ic_epo " +
							 "    AND ff.ic_if = i.ic_if(+) " +
							 "    AND ff.ic_pyme = p.ic_pyme(+) " +
							 "    AND ff.ic_cuenta = c.ic_cuenta(+) " +
							 "    AND me.status_cen_i = eff.ic_estatus_ff(+) " +
							 "    AND ff.cs_reproceso IN ('N', 'D') " +
							 "    AND ff.cs_concluido = 'S' "
							 :
							 " SELECT count(1) as totalOperacion, Sum(ff.fn_importe) as totalImporteTD " +
							 "   FROM int_flujo_fondos ff, cfe_m_encabezado me, " +
							 "        comcat_epo e, comcat_if i, " +
							 "        comcat_pyme p, com_cuentas c, " +
							 "        comcat_estatus_ff eff " +
							 "  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i" +
							 "    AND ff.ic_epo = e.ic_epo " +
							 "    AND ff.ic_if = i.ic_if(+) " +
							 "    AND ff.ic_pyme = p.ic_pyme(+) " +
							 "    AND ff.ic_cuenta = c.ic_cuenta(+) " +
							 "    AND me.status_cen_i = eff.ic_estatus_ff(+) " +
							 "    AND ff.cs_reproceso IN ('N', 'D') " +
							 "    AND ff.cs_concluido = 'S' "
							 )+
		           (condicion_3 + condvia + epoEspecifica + ifEspecifica + condConsultaEpo +
		           (tipoConsulta?"  ORDER BY 4 ":" ) Tab ")
					  );
		}
		return strQuery;
	}
	
	public void getConditionQuery(){
		String condEPOs = " AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if)" + 
		                  " AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') " +
   				         " AND c.ic_producto_nafin  = 1 ";
								
		String condEPOsErr = " AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) " +
		                     " AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') " +
   				            " AND c.ic_tipo_cuenta (+) = 40 " +
									" AND c.ic_producto_nafin (+) = 1 " + 
									" AND c.ic_moneda (+) = 1 ";

		String condIFs = " AND crn.ic_nafin_electronico = c.ic_nafin_electronico " +
		                 " AND c.ic_producto_nafin  = 1 ";
		
		String condIFsErr = " AND crn.ic_nafin_electronico(+) = c.ic_nafin_electronico " +
		                    " AND c.ic_tipo_cuenta (+) = 40 " +
   				           " AND c.ic_producto_nafin (+) = 1 " +
								  " AND c.ic_moneda (+) = 1 ";
   				
		String condPEMEXs = " AND crn.ic_epo_pyme_if = ff.ic_epo " +
		                    " AND crn.cg_tipo = 'E' " +
								  " AND c.ic_tipo_cuenta (+) = 40 " +
   				           " AND c.ic_producto_nafin (+) = 4 " +
								  " AND c.ic_moneda (+) = 1 ";
		
			
	   epoEspecifica = " ";
      ifEspecifica = " ";
      condvia = " ";
	   condConsultaEpo = " ";
		condicion_1 = " ";
	   condicion_2 = " ";
	   condicion_3 = " ";
		
		if(tipoDispersion.equals("E")){
		   condicion_1 = condEPOs+" AND (ff.df_fecha_venc is not null OR ff.df_operacion is not null) AND ff.ic_estatus_docto in (1, 4, 9,10, 11) ";
			condicion_2 = condEPOsErr+" AND (ff.df_fecha_venc is not null OR ff.df_operacion is not null) AND ff.ic_estatus_docto in (1, 4, 9,10, 11) ";
			condicion_3 = " AND (ff.df_fecha_venc IS NOT NULL OR ff.df_operacion IS NOT NULL) AND ff.ic_estatus_docto IN (1, 4, 9, 10, 11) ";
		}
		else if (tipoDispersion.equals("F")){
		   condicion_1 = condIFs+" AND (ff.ic_if is not null AND ff.df_fecha_venc is null) ";
			condicion_2 = condIFsErr+" AND (ff.ic_if is not null AND ff.df_fecha_venc is null) ";
			condicion_3 = " AND (ff.ic_if IS NOT NULL AND ff.df_fecha_venc IS NULL) ";
		}
		else if(tipoDispersion.equals("P") && "2".equals(estatus))
			condicion_1 = condPEMEXs+" AND (ff.ic_if IS NULL AND ff.df_fecha_venc IS NOT NULL AND ff.ic_estatus_docto = 2) ";
		else if(tipoDispersion.equals("P") && estatus.equals("PA")){
		   condicion_1 = condPEMEXs+" AND (ff.ic_if IS NULL AND ff.df_operacion IS NOT NULL AND ff.ic_estatus_docto IS NULL) ";
			condicion_3 = " AND ( ff.ic_if IS NULL AND ff.df_operacion IS NOT NULL AND ff.ic_estatus_docto IS NULL ) ";
		}
		
		if(!fechaRegIni.equals("") && !fechaRegFin.equals("")){
		   condicion_1 += " AND FF.df_registro >= TO_DATE('"+fechaRegIni+"','dd/mm/yyyy') " +
					         " AND FF.df_registro < (TO_DATE('"+fechaRegFin+"','dd/mm/yyyy') + 1) ";
			condicion_2 += " AND FF.df_registro >= TO_DATE('"+fechaRegIni+"','dd/mm/yyyy') " +
					         " AND FF.df_registro < (TO_DATE('"+fechaRegFin+"','dd/mm/yyyy') + 1) ";
		   condicion_3 += " AND ff.df_registro >= TO_DATE('"+fechaRegIni+"','dd/mm/yyyy') AND ff.df_registro < (TO_DATE('"+fechaRegFin+"','dd/mm/yyyy') + 1) ";
		}

		if(!"".equals(icEpo))
		 epoEspecifica = " AND FF.ic_epo = " +icEpo;
		else if(!"".equals(icIf))
		 ifEspecifica = " AND FF.ic_if = " +icIf;
		 
		if(!"".equals(viaLiq)) {
			if("T".equals(viaLiq))
				condvia += " AND FF.cg_via_liquidacion = 'TEF' ";
			else if("S".equals(viaLiq))
				condvia += " AND FF.cg_via_liquidacion = 'SPEUA' ";
			else if("I".equals(viaLiq))
				condvia += " AND FF.cg_via_liquidacion = 'SPEI' ";
		}

      if (!"".equals(estatusFF)){
				condConsultaEpo = "    AND ME.status_cen_i >= "+estatusFF+" ";
      }
	}
	
	public String getDocumentQuery() {
		conditions = new ArrayList();
		strQuery	= new StringBuffer();
		
		getConditionQuery();
		strQuery.append(getQuery(true,1) + getQuery(true,2) + getQuery(true,3));
							 
		log.debug("getDocumentQuery)"+strQuery.toString());
		log.debug("getDocumentQuery)"+conditions);
		
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
		conditions	= new ArrayList();
		strQuery	   = new StringBuffer();
		
		getConditionQuery();
		strQuery.append(" SELECT * FROM ( " + getQuery(true,1) + getQuery(true,2) + getQuery(true,3) + " ) Tab");
		
		for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					strQuery.append(" WHERE Tab.IC_FLUJO_FONDOS IN ( ");
				}
				strQuery.append("?");
				conditions.add(new Long(lItem.get(9).toString()));					
				if(i!=(pageIds.size()-1)) {
					strQuery.append(",");
				} else {
					strQuery.append(" ) ");
				}			
			}
							 
		log.debug("getDocumentSummaryQueryForIds)"+strQuery.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		
		return strQuery.toString();
	}
	
	public String getAggregateCalculationQuery() {
	   conditions	= new ArrayList();
		strQuery	   = new StringBuffer();
		
		getConditionQuery();
		strQuery.append(getQuery(false,1) + getQuery(false,2) + getQuery(false,3));
							 
		log.debug("getAggregateCalculationQuery)"+strQuery.toString());
		log.debug("getAggregateCalculationQuery)"+conditions);
		
		return strQuery.toString();
	}
	
	public String getDocumentQueryFile() {
	   conditions	= new ArrayList();
		strQuery	   = new StringBuffer();
		
		getConditionQuery();
		strQuery.append(getQuery(true,1) + getQuery(true,2) + getQuery(true,3));
							 
		log.debug("getDocumentQueryFile)"+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		
		return strQuery.toString();
	}
	
	
	/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String formateaClaveRastreo(String claveRastreo){
		
		StringBuffer buffer = new StringBuffer();
		
		for(int i=0;i<claveRastreo.length();i++){
			buffer.append(claveRastreo.charAt(i));
			if(i == 11 || i == 13 || i == 15) buffer.append("-");
		}
		
		return buffer.toString();
	} 
	 
	public String generarPDF(String directorio, String directorioPublicacion, String nombreUsuario, String fecha, List lista, Dispersion dispersion, String TipoDispersion){ 
		String nombreArchivo = null;
		CreaArchivo archivo 	= null;
		//ComunesPDF 	documentoPdf 	= null;
		
		try {
			archivo 			= new CreaArchivo();
			nombreArchivo 	= archivo.nombreArchivo()+".pdf";
			
			// Fuentes
			Font fuenteTitulo 			= FontFactory.getFont("Arial", 12, Font.BOLD);
			Font fuenteSubTitulo 		= FontFactory.getFont("Arial", 10, Font.BOLD);
			Font fuenteSubTituloNormal = FontFactory.getFont("Arial", 10);
			Font fuenteSubTituloBlanco = FontFactory.getFont("Arial", 10, Font.BOLD, BaseColor.WHITE);
			Font fuentePequena 			= FontFactory.getFont("Arial", 5, Font.BOLD);
			Font fuenteEnorme 			= FontFactory.getFont("Arial", 14, Font.BOLD);
			
			// Definir la imagen que se presentara en el logo
			String strLogo = directorioPublicacion +	"/00archivos/15cadenas/15archcadenas/logos/" + "nafinsa_ff.gif";
			Image image = Image.getInstance(strLogo);
			image.setAlignment(Element.ALIGN_LEFT);
			image.scalePercent(82);
			
			// Crear documento PDF
			Document document = new Document(PageSize.A4, 22, 22, 30, 30);
			PdfWriter.getInstance(document,new FileOutputStream(directorio+nombreArchivo));
			document.open();
			
			Vector 	registro 				= null;
			String	fechaEmision 			= "";
			String 	acuse 					= "";
			String	beneficiario			= "";
			String	movimientos				= "";
			String	operacionRealizada 	= "";
			String	claveRastreo			= "";
			String	tipoCuenta				= "";
			String	numeroDeCuenta			= "";
			String	importe					= "";
			String	institucionCuenta 	= "";
         String  nombreIF          = "";
         String  nombrePYME        = "";
				
			for(int i=0;i<lista.size();i++){
				registro = (Vector) lista.get(i);
				registro = (registro == null)?new Vector():registro;
					
				fechaEmision 			= fecha;
				acuse 					= dispersion.getNumeroAcusePdfFlujoFondos();
				nombreIF					= registro.get(1)		== null?"":registro.get(1).toString();
				nombrePYME				= registro.get(2)		== null?"":registro.get(2).toString();
				if(TipoDispersion.equals("F")){ //cuando es If se muestran siempre las Pymes como Beneficiarios
					beneficiario			= nombrePYME;					
				}else{
				   beneficiario			= nombreIF.trim().equals("")?nombrePYME:nombreIF;
				}
				movimientos				= registro.get(12)	== null?"":registro.get(12).toString();
				operacionRealizada 	= registro.get(11)	== null?"":registro.get(11).toString();
				claveRastreo			= registro.get(9) 	== null?"":"OPE-200-"+registro.get(9).toString();
				tipoCuenta				= "CH - CUENTA DE CHEQ";
				numeroDeCuenta			= registro.get(6)		== null?"":registro.get(6).toString();
				importe					= registro.get(7)		== null?"0.00":Comunes.formatoDecimal(registro.get(7).toString(),2,true);
				institucionCuenta 	= registro.get(24)	== null?"":registro.get(24).toString();
				
				claveRastreo 	= 	formateaClaveRastreo(claveRastreo);
				importe			=	"$ " + importe;
				
				// Agregar pagina nueva
				if(i>0){
					document.add(Chunk.NEWLINE);
					document.newPage();
				}
				
				// A�adir logo 
				PdfPCell celdaLogo =  new PdfPCell(image);
				celdaLogo.setBorder(0);
				celdaLogo.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				// Crear Titulo del Documento
				PdfPCell celdaTitulo =  new PdfPCell(new Paragraph("COMPROBANTE DE TRANSFERENCIA ELECTR�NICA",fuenteTitulo));
				celdaTitulo.setBorder(0);
				celdaTitulo.setHorizontalAlignment(Element.ALIGN_CENTER);
				celdaTitulo.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 
				// 1. CREAR TABLA DEL ENCABEZADO
				float[] colsWidth = {2.8f, 7.2f}; 
				PdfPTable table = new PdfPTable(colsWidth); 
				table.setWidthPercentage(100); 
				// 1.1 Agregar celdas
				table.addCell(celdaLogo);
				table.addCell(celdaTitulo);
				// 1.2 Agregar tabla
				document.add(table);
		
				// 2. AGREGAR LINEA EN BLANCO
				document.add(new Paragraph(" ",fuentePequena));
					
				// 3. AGREGAR LINEA INDICANDO LA FECHA DE EMISION
				document.add(new Paragraph("Fecha de Emisi�n",fuenteSubTitulo));
		 
			   // 4. AGREGAR TABLA DEL BENEFICIARIO
			   float 	padding 	= fuenteSubTitulo.getSize();
				colsWidth 			= new float[]{2.5f, 7.2f}; 
				PdfPTable t 		= new PdfPTable(2); 
				t.setWidthPercentage(100f);
				t.setSpacingBefore(0f);
				t.setWidths(colsWidth);
				
				// 4.1 Agregar Fecha de Emision
				PdfPCell cell =  new PdfPCell(new Phrase(fechaEmision,fuenteSubTitulo)); // "FECHA_DE_EMISION"
	         cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell.setBorder( Rectangle.LEFT | Rectangle.TOP | Rectangle.RIGHT );
				cell.setBorderWidth(0.5f);
				cell.setPadding(padding);
	         t.addCell(cell);
				
				// 4.2 Agregar Celda con el titulo del beneficiario
				cell =  new PdfPCell(new Phrase("BENEFICIARIO",fuenteSubTituloBlanco));
	         cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         cell.setBorderWidth(0.5f);
	         cell.setPadding(padding);
	         cell.setBackgroundColor(BaseColor.GRAY);
	         t.addCell(cell);
				
				// 4.3 Agregar Fecha de Emision
				cell =  new PdfPCell(new Phrase(acuse,fuenteSubTitulo)); // ACUSE 
	         cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         cell.setBorder( Rectangle.LEFT | Rectangle.BOTTOM | Rectangle.RIGHT );
	         cell.setBorderWidth(0.5f);
	         cell.setPadding(padding);
	         t.addCell(cell);
				
				// 4.4 Agregar Celda con el titulo del beneficiario
				cell =  new PdfPCell(new Phrase(beneficiario,fuenteSubTitulo));// BENEFICIARIO
	         cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	         cell.setBorderWidth(0.5f);
	         cell.setPadding(padding);
	         t.addCell(cell);
				document.add(t);
				
				// 5. AGREGAR LINEA EN BLANCO
				document.add(new Paragraph(" ",fuentePequena));
				
				// 6. AGREGAR RENGLON DE MOVIMIENTOS
				document.add(new Paragraph("Movimientos: "+movimientos,fuenteEnorme)); // MOVIMIENTOS
				// 7. AGREGAR RENGLON CON FECHA DE LA OPERACION REALIZADA
				document.add(new Paragraph("Operaci�n realizada: "+operacionRealizada,fuenteSubTitulo)); // OPERACION_REALIZADA
				
				// 8. AGREGAR TABLA DE MOVIMIENTOS
				float colsWidth4[] = new float[]{1f,1f,1f,1f}; 
				table = new PdfPTable(colsWidth4); 
				table.setWidthPercentage(100); 
				// 8.1 Agregar Titulo: Clave de Rastreo
				PdfPCell celda =  new PdfPCell(new Paragraph("Clave de Rastreo",fuenteSubTituloNormal));
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				celda.setFixedHeight(26f) ;
				table.addCell(celda);
				// 8.2 Agregar Titulo:  Tipo de Cuenta
				celda =  new PdfPCell(new Paragraph("Tipo de Cuenta",fuenteSubTituloNormal));
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
				// 8.3 Agregar Titulo:  N�mero de Cuenta
				celda =  new PdfPCell(new Paragraph("N�mero de Cuenta",fuenteSubTituloNormal));
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
				// 8.4 Agregar Titulo:  Importe
				celda =  new PdfPCell(new Paragraph("Importe",fuenteSubTituloNormal));
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
				
				// 8.5 Agregar Contenido: Clave de Rastreo
				celda =  new PdfPCell(new Paragraph(claveRastreo,fuenteSubTitulo)); // CLAVE DE RASTREO
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				celda.setFixedHeight(26f);
				table.addCell(celda);
				// 8.6 Agregar Contenido:  Tipo de Cuenta
				celda =  new PdfPCell(new Paragraph(tipoCuenta,fuenteSubTitulo)); // TIPO DE CUENTA
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
				// 8.7 Agregar Contenido:  N�mero de Cuenta
				celda =  new PdfPCell(new Paragraph(numeroDeCuenta,fuenteSubTitulo)); // NUMERO DE CUENTA
				celda.setHorizontalAlignment(Element.ALIGN_CENTER);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
				// 8.8 Agregar Contenido:  Importe
				celda =  new PdfPCell(new Paragraph(importe,fuenteSubTitulo)); // IMPORTE
				celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				table.addCell(celda);
		 
				// 8.8 Agregar Titulo:  Instituci�n de la Cuenta del Beneficiario
				celda =  new PdfPCell(new Paragraph("Instituci�n de la Cuenta del Beneficiario",fuenteSubTituloNormal));
				celda.setHorizontalAlignment(Element.ALIGN_LEFT);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				celda.setIndent(12f);
				celda.setFixedHeight(26f);
				celda.setColspan(4);
				table.addCell(celda);
				// 8.9 Agregar Contenido:  Instituci�n de la Cuenta del Beneficiario
				celda =  new PdfPCell(new Paragraph(institucionCuenta,fuenteSubTitulo)); // INSTITUCION CUENTA
				celda.setHorizontalAlignment(Element.ALIGN_LEFT);
				celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
				celda.setIndent(5f);
				celda.setFixedHeight(26f);
				celda.setColspan(4);
				table.addCell(celda);
				
				document.add(table);				
				document.add(new Paragraph("Gener�: "+nombreUsuario,fuenteTitulo));
			}
			
			// Fin de Documento
			if(lista == null || lista.size() == 0){
				document.add(new Paragraph(" No se encontraron registros",fuenteTitulo));
			}
			document.close();
				
		}catch(Exception e){
			System.out.println("15disperpagoresumen.jsp::getArchivoPDF(Exception)");
			e.printStackTrace();
		}
		System.out.println("15disperpagoresumen.jsp::getArchivoPDF(S)");
		
		return nombreArchivo;	
	} 
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)
	{
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		try {
			if(tipo.equals("CSV")) {
			 linea = "EPO,IF,PYME,RFC,Banco,Tipo de Cuenta,No. de Cuenta,Importe Total del Descuento,Estatus,V�a de Liquidaci�n,Folio,Fecha de Registro\n";
			 nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			 writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			 buffer = new BufferedWriter(writer);
			 buffer.write(linea);
			 
			 while (rs.next()) {
			  String epo	=	(rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
			  String iF = (rs.getString("CRSI") == null) ? "" : rs.getString("CRSI");
			  String pyme	=	(rs.getString("CRSP") == null) ? "" : rs.getString("CRSP");
			  String rfc = (rs.getString("CG_RFC") == null) ? "" : rs.getString("CG_RFC");
			  String banco	=	(rs.getString("IC_BANCOS_TEF") == null) ? "" : rs.getString("IC_BANCOS_TEF");
			  String tipoCuenta = (rs.getString("IC_TIPO_CUENTA") == null) ? "" : rs.getString("IC_TIPO_CUENTA");
			  String numCuenta	=	(rs.getString("CG_CUENTA") == null) ? "" : rs.getString("CG_CUENTA");
			  String importeTD = (rs.getString("FN_IMPORTE") == null) ? "" : rs.getString("FN_IMPORTE");
			  String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
			  String viaLiquidacion	=	(rs.getString("CG_VIA_LIQUIDACION") == null) ? "" : rs.getString("CG_VIA_LIQUIDACION");
			  String folio = (rs.getString("IC_FLUJO_FONDOS") == null) ? "" : rs.getString("IC_FLUJO_FONDOS");
			  String fechaRegistro = (rs.getString("DF_REGISTRO") == null) ? "" : rs.getString("DF_REGISTRO");
		
		     linea = epo.replace(',',' ') + ", " + 
			          iF.replace(',',' ') + ", " +
						 pyme.replace(',',' ') + ", " +
						 rfc.replace(',',' ') + ", " +
						 banco.replace(',',' ') + ", " +
						 tipoCuenta.replace(',',' ') + ", " +
						 numCuenta.replace(',',' ') + ", " +
						 importeTD.replace(',',' ') + ", " +
						 estatus.replace(',',' ') + ", " +
						 viaLiquidacion.replace(',',' ') + ", " +
						 folio.replace(',',' ') + ", " +
						 fechaRegistro.replace(',',' ') + "\n";				 
			  buffer.write(linea);
			 }
			 buffer.close();
			}
			else if(tipo.equals("PDF")){
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"), 
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(12,100);
				pdfDoc.setCell("EPO", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Banco","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. de Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe Total del Descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("V�a de Liquidaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Folio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Registro","celda01",ComunesPDF.CENTER);
				
				while (rs.next()) {
					String epo	=	(rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
			      String iF = (rs.getString("CRSI") == null) ? "" : rs.getString("CRSI");
			      String pyme	=	(rs.getString("CRSP") == null) ? "" : rs.getString("CRSP");
			      String rfc = (rs.getString("CG_RFC") == null) ? "" : rs.getString("CG_RFC");
			      String banco	=	(rs.getString("IC_BANCOS_TEF") == null) ? "" : rs.getString("IC_BANCOS_TEF");
			      String tipoCuenta = (rs.getString("IC_TIPO_CUENTA") == null) ? "" : rs.getString("IC_TIPO_CUENTA");
			      String numCuenta	=	(rs.getString("CG_CUENTA") == null) ? "" : rs.getString("CG_CUENTA");
			      String importeTD = (rs.getString("FN_IMPORTE") == null) ? "" : rs.getString("FN_IMPORTE");
			      String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
			      String viaLiquidacion	=	(rs.getString("CG_VIA_LIQUIDACION") == null) ? "" : rs.getString("CG_VIA_LIQUIDACION");
			      String folio = (rs.getString("IC_FLUJO_FONDOS") == null) ? "" : rs.getString("IC_FLUJO_FONDOS");
			      String fechaRegistro = (rs.getString("DF_REGISTRO") == null) ? "" : rs.getString("DF_REGISTRO");
					
					pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(iF,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(banco,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoCuenta,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numCuenta,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(importeTD,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(viaLiquidacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(folio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaRegistro,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
				
				pdfDoc.endDocument();
				}
				else if(tipo.equals("PDF_ComprobanteTrans")){
				   HttpSession session = request.getSession();
					
					Dispersion dispersion = ServiceLocator.getInstance().lookup("DispersionEJB", Dispersion.class);
					
					Hashtable consulta = dispersion.getResumOperacErrorFF("Consulta", tipoDispersion, fechaRegIni, fechaRegFin,viaLiq,icEpo,icIf,estatus); 	
					Vector lista = ((consulta == null)?new Vector():(Vector)consulta.get("OperError"));
               
					String directorioPublicacion = (String)session.getServletContext().getAttribute("strDirectorioPublicacion");
					String nombreUsuario = (String)session.getAttribute("strNombre");
					String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
					nombreArchivo = generarPDF(path,directorioPublicacion, nombreUsuario,(fechaActual + " - " + horaActual), lista, dispersion, "E");    
				}
		}
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		return nombreArchivo;
	}
	
	
   /** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}
	
	public void setTipoDispersion(String tipoDispersion) {
		this.tipoDispersion = tipoDispersion;
	}

	public String getTipoDispersion() {
		return tipoDispersion;
	}

	public void setFechaRegIni(String fechaRegIni) {
		this.fechaRegIni = fechaRegIni;
	}

	public String getFechaRegIni() {
		return fechaRegIni;
	}

	public void setFechaRegFin(String fechaRegFin) {
		this.fechaRegFin = fechaRegFin;
	}

	public String getFechaRegFin() {
		return fechaRegFin;
	}

	public void setViaLiq(String viaLiq) {
		this.viaLiq = viaLiq;
	}

	public String getViaLiq() {
		return viaLiq;
	}

	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}

	public String getIcEpo() {
		return icEpo;
	}

	public void setIcIf(String icIf) {
		this.icIf = icIf;
	}

	public String getIcIf() {
		return icIf;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatysFF(String estatysFF) {
		this.estatusFF = estatysFF;
	}

	public String getEstatysFF() {
		return estatusFF;
	}
}