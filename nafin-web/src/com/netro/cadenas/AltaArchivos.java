package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Pantalla NAFIN TESORERIA -Tasas de Cr�dito Nafin - Parametrizar Tesoreria - Alta de archivos
 */
 
public class AltaArchivos  {

	private final static Log log = ServiceLocator.getInstance().getLog(AltaArchivos.class);
	private StringBuffer qrySentencia 	= new StringBuffer();
	private List conditions;
	private String id;

	public AltaArchivos() {	}
	
	/**
	 * Obtiene los datos a mostrar en la consulta
	 * @return Objeto de tipo Registros.
	 */
	public Registros carga(){
	log.info("carga(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT ic_param_archivos AS clave, \n"+
										" cg_nombre_archivo AS nombre, \n"+
										" cg_descripcion AS descripcion \n"+
										" FROM cot_param_archivos");

			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("carga  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				 con.cierraConexionDB();
			}
		} 		
		log.info("carga (qry): \n "+qrySentencia.toString());
		log.info("carga (S) ");
		return registros;
															
	}
	
	public void setId(String id) {
		this.id = id;
	}


	public String getId() {
		return id;
	}
	
}