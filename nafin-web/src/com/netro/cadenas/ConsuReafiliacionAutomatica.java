package com.netro.cadenas;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsuReafiliacionAutomatica implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

	public ConsuReafiliacionAutomatica()
	{
	}	
		//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ConsuReafiliacionAutomatica.class);
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;	
	
	private String directorio;
	
	public String ic_pyme;
	public String txt_nombre;
	public String txt_nafelec;
	public String hid_nombre;
	public String banco;
	public String sirac;
	public String fhabilitacioIni;
	public String fhabilitacioFin;
	public String fafiliacionIni;
	public String fafiliacionFin;
	public String sinNumProv;
	public String porhabilitar;	
	public String noepo;
	public String usuario;
	
	

	

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
	log.debug("paginaOffset:: "+paginaOffset);	
	  
	  	qrySentencia.append("SELECT count(1) total, 'ConsuReafiliacionAutomatica ::getAggregateCalculationQuery' origen " );
				
						
		qrySentencia.append(" FROM comcat_pyme p, "+
								  " comrel_pyme_epo er,  "+
								  " comcat_epo e, "+
								  " comrel_nafin n, "+
								  " com_domicilio d, "+
								  " comcat_estado edo, "+
								  "  comrel_cuenta_bancaria c, "+
								  "  comcat_moneda m, "+
								  "  comrel_pyme_if rel ");
									  
									   					
		qrySentencia.append(" WHERE  er.ic_pyme = p.ic_pyme "+
									" AND er.ic_epo = e.ic_epo "+
									" AND er.ic_pyme = n.ic_epo_pyme_if "+
									" AND n.cg_tipo = 'P' "+
									" AND er.ic_pyme = d.ic_pyme "+
									" AND d.ic_estado = edo.ic_estado "+
									" AND d.cs_fiscal='S' "+
									" AND er.ic_pyme = c.ic_pyme  "+
									" AND c.cs_borrado='N' "+
									" AND c.ic_cuenta_bancaria = rel.ic_cuenta_bancaria  "+
									"  AND c.ic_moneda = m.ic_moneda  "+
								   " AND er.ic_epo = rel.ic_epo   "+
									" AND rel.cs_borrado = 'N'  "+
									" AND (rel.df_param_aut_conv_unico IS NOT NULL OR rel.df_reasigno_cta IS NOT NULL) ");
								  
 


		//Intermediario Financiero
		 if((!"".equals(usuario) && usuario!=null))
	 		{
				qrySentencia.append(" AND   rel.ic_if = ? ") ;
				conditions.add(usuario);
			}			

		
		//por Banco de Fondeo
		 if((!"".equals(banco) && banco!=null))
	 		{
				qrySentencia.append(" AND  e.ic_banco_fondeo = ? ") ;
				conditions.add(banco);
			}
		
		//Numero de Epo
		if((!"".equals(noepo) && noepo!=null))
	 		{
				qrySentencia.append(" AND  e.ic_epo = ? ") ;
				conditions.add(noepo);
			}
		
		//Numero de Pyme
			if((!"".equals(ic_pyme) && ic_pyme!=null))
	 		{
				qrySentencia.append(" AND  p.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}			
		
		// Numero de Sirac
		if((!"".equals(sirac) && sirac!=null))
	 		{
				qrySentencia.append(" AND  p.in_numero_sirac = ? ") ;
				conditions.add(sirac);
			}
			
				//Fecha de Habilitacion
		 if( (!"".equals(fhabilitacioIni) && fhabilitacioIni!=null) && (!"".equals(fhabilitacioFin) && fhabilitacioFin!=null) ) {
					qrySentencia.append(" AND  er.df_habilitacion >=  to_date(?, 'dd/mm/yyyy')  ") ;
					qrySentencia.append(" AND  er.df_habilitacion <= to_date(?, 'dd/mm/yyyy')+1  ") ;
					conditions.add(fhabilitacioIni);
					conditions.add(fhabilitacioFin);
		}	
						//Fecha de Afiliacion
		 if( (!"".equals(fafiliacionIni) && fafiliacionIni!=null) && (!"".equals(fafiliacionFin) && fafiliacionFin!=null) ) {
					qrySentencia.append(" AND  p.df_alta >=  to_date(?, 'dd/mm/yyyy')  ") ;
					qrySentencia.append(" AND  p.df_alta <= to_date(?, 'dd/mm/yyyy')+1  ") ;
					conditions.add(fafiliacionIni);
					conditions.add(fafiliacionFin);
			}
		
		//Pymes sin numero de proveedor 
		if("S".equals(sinNumProv) ) 	{
				qrySentencia.append(" AND er.cs_num_proveedor  = ?  ") ;
				conditions.add(sinNumProv);
			}
			

		// Pymes por habilitar  
	
		if("N".equals(porhabilitar) ) 	{	
			qrySentencia.append("  AND rel.cs_vobo_if= 'N'  ") ;
			
		}else {
			qrySentencia.append("  AND rel.cs_vobo_if = 'S'  ") ;
		}
		
		  qrySentencia.append("  order by p.cg_razon_social, e.cg_razon_social, c.ic_cuenta_bancaria  ");
	
					
			log.debug("getAggregateCalculationQuery "+conditions.toString());
			log.debug("getAggregateCalculationQuery "+qrySentencia.toString());
	
		return qrySentencia.toString();
		
	}
		
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.debug("-------------------------:: ");
		log.debug("ic_pyme:: "+ ic_pyme);
		log.debug("txt_nombre:: "+txt_nombre );
		log.debug("txt_nafelec:: "+ txt_nafelec);
		log.debug("hid_nombre:: "+ hid_nombre);
		log.debug("banco:: "+banco );
		log.debug("sirac:: "+sirac );
		log.debug("fhabilitacioIni:: "+ fhabilitacioIni);
		log.debug("fhabilitacioFin:: "+fhabilitacioFin );
		log.debug("fafiliacionIni:: "+fafiliacionIni );
		log.debug("sinNumProv:: "+sinNumProv );
		log.debug("porhabilitar:: "+porhabilitar);
		log.debug("noepo:: "+noepo);
		log.debug("usuario:: "+usuario);
		log.debug("paginaOffset:: "+paginaOffset);
		log.debug("-------------------------:: ");
		
		
		
			
		qrySentencia.append(" SELECT  /*+ index(p) use_nl(er, e, n, d, edo,c, m, rel ) index(rel) */  "+
								  " er.ic_pyme as pyme, er.ic_epo as epo,rel.ic_if as if, m.ic_moneda as moneda1, "+
								  " c.ic_cuenta_bancaria, e.ic_epo AS epo, "+
								  " e.cg_razon_social AS nombreepo,"+
								  " er.cg_pyme_epo_interno AS noproveedor,"+
								  " n.ic_nafin_electronico AS nafinelectronico, "+
								  " p.in_numero_sirac AS sirac, "+
								  " rel.cs_estatus AS estatus, "+
								  " p.cg_razon_social AS razonsocial,"+
								  " d.cg_calle || ' ' || d.cg_numero_ext	 || ' ' || d.cg_numero_int AS domicilio, "+
								  " edo.cd_nombre AS estado,"+
								  " p.cg_telefono AS telefono, "+
								  " p.cg_email AS correo,"+
								   " TO_CHAR(er.df_habilitacion,'DD/MM/YYYY') as fechaHabilitacion,  "+
								  " TO_CHAR(p.df_alta,'DD/MM/YYYY') as FechaAfiliacion,  "+
								  " m.cd_nombre AS moneda, "+
								  " c.cg_numero_cuenta AS cuentacheques, "+
								  " rel.cs_vobo_if as csvoboif ");
								  
		 
		 
		qrySentencia.append(" FROM comcat_pyme p, "+
								  " comrel_pyme_epo er,  "+
								  " comcat_epo e, "+
								  " comrel_nafin n, "+
								  " com_domicilio d, "+
								  " comcat_estado edo, "+
								  "  comrel_cuenta_bancaria c, "+
								  "  comcat_moneda m, "+
								  "  comrel_pyme_if rel ");
									  
									   					
		qrySentencia.append(" WHERE  er.ic_pyme = p.ic_pyme "+
									" AND er.ic_epo = e.ic_epo "+
									" AND er.ic_pyme = n.ic_epo_pyme_if "+
									" AND n.cg_tipo = 'P' "+
									" AND er.ic_pyme = d.ic_pyme "+
									" AND d.ic_estado = edo.ic_estado "+
									" AND d.cs_fiscal='S' "+
									" AND er.ic_pyme = c.ic_pyme  "+
									" AND c.cs_borrado='N' "+
									" AND c.ic_cuenta_bancaria = rel.ic_cuenta_bancaria  "+
									"  AND c.ic_moneda = m.ic_moneda  "+
								   " AND er.ic_epo = rel.ic_epo   "+
									" AND rel.cs_borrado = 'N'  "+
									" AND (rel.df_param_aut_conv_unico IS NOT NULL OR rel.df_reasigno_cta IS NOT NULL) ");
								  
 
	

		//Intermediario Financiero
		 if((!"".equals(usuario) && usuario!=null))
	 		{
					qrySentencia.append(" AND   rel.ic_if = ? ") ;
				conditions.add(usuario);
			}			

		
		//por Banco de Fondeo
		 if((!"".equals(banco) && banco!=null))
	 		{
				qrySentencia.append(" AND  e.ic_banco_fondeo = ? ") ;
				conditions.add(banco);
			}
		
		//Numero de Epo
		if((!"".equals(noepo) && noepo!=null))
	 		{
				qrySentencia.append(" AND  e.ic_epo = ? ") ;
				conditions.add(noepo);
			}
		
		//Numero de Pyme
			if((!"".equals(ic_pyme) && ic_pyme!=null))
	 		{
				qrySentencia.append(" AND  p.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}			
		
		// Numero de Sirac
		if((!"".equals(sirac) && sirac!=null))
	 		{
				qrySentencia.append(" AND  p.in_numero_sirac = ? ") ;
				conditions.add(sirac);
			}
			
				//Fecha de Habilitacion
		 if( (!"".equals(fhabilitacioIni) && fhabilitacioIni!=null) && (!"".equals(fhabilitacioFin) && fhabilitacioFin!=null) ) {
					qrySentencia.append(" AND  er.df_habilitacion >=  to_date(?, 'dd/mm/yyyy')  ") ;
					qrySentencia.append(" AND  er.df_habilitacion <= to_date(?, 'dd/mm/yyyy')+1  ") ;
					conditions.add(fhabilitacioIni);
					conditions.add(fhabilitacioFin);
		}	
						//Fecha de Afiliacion
		 if( (!"".equals(fafiliacionIni) && fafiliacionIni!=null) && (!"".equals(fafiliacionFin) && fafiliacionFin!=null) ) {
					qrySentencia.append(" AND  p.df_alta >=  to_date(?, 'dd/mm/yyyy')  ") ;
					qrySentencia.append(" AND  p.df_alta <= to_date(?, 'dd/mm/yyyy')+1  ") ;
					conditions.add(fafiliacionIni);
					conditions.add(fafiliacionFin);
			}
		
		//Pymes sin numero de proveedor 		
		if("S".equals(sinNumProv) ) 	{
				qrySentencia.append(" AND er.cs_num_proveedor  = ?  ") ;
				conditions.add(sinNumProv);
			}
		
		
			// Pymes por habilitar  
	
		if("N".equals(porhabilitar) ) 	{	
			qrySentencia.append("  AND rel.cs_vobo_if= 'N'  ") ;
			
		}else {
			qrySentencia.append("  AND rel.cs_vobo_if = 'S'  ") ;
		}
		
		
		
		qrySentencia.append("  order by p.cg_razon_social, e.cg_razon_social, c.ic_cuenta_bancaria ");
		
			log.debug("getDocumentQuery "+conditions.toString());
			log.debug("getDocumentQuery "+qrySentencia.toString());
	

			return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		qrySentencia.append(" SELECT /*+ index(p) use_nl(er, e, n, d, edo,c, m, rel ) index(rel) */ "+
								  " er.ic_pyme as pyme, er.ic_epo as epo,rel.ic_if as if, m.ic_moneda as moneda1, "+ 
								  " c.ic_cuenta_bancaria, e.ic_epo AS epo, "+
								  " e.cg_razon_social AS nombreepo,"+
								  " er.cg_pyme_epo_interno AS noproveedor,"+
								  " n.ic_nafin_electronico AS nafinelectronico, "+
								  " p.in_numero_sirac AS sirac, "+
								  " rel.cs_estatus AS estatus, "+
								  " p.cg_razon_social AS razonsocial,"+
								  " d.cg_calle || ' ' || d.cg_numero_ext	 || ' ' || d.cg_numero_int AS domicilio, "+
								  " edo.cd_nombre AS estado,"+
								  " p.cg_telefono AS telefono, "+
								  " p.cg_email AS correo,"+
								   " TO_CHAR(er.df_habilitacion,'DD/MM/YYYY') as fechaHabilitacion,  "+
								  " TO_CHAR(p.df_alta,'DD/MM/YYYY') as FechaAfiliacion,  "+
								  " m.cd_nombre AS moneda, "+
								  " c.cg_numero_cuenta AS cuentacheques, "+
								  " rel.cs_vobo_if as csvoboif ");
								  
								  
	qrySentencia.append(" FROM comcat_pyme p, "+
								  " comrel_pyme_epo er,  "+
								  " comcat_epo e, "+
								  " comrel_nafin n, "+
								  " com_domicilio d, "+
								  " comcat_estado edo, "+
								  "  comrel_cuenta_bancaria c, "+
								  "  comcat_moneda m, "+
								  "  comrel_pyme_if rel ");
									  
									   					
	qrySentencia.append(" WHERE  er.ic_pyme = p.ic_pyme "+
									" AND er.ic_epo = e.ic_epo "+
									" AND er.ic_pyme = n.ic_epo_pyme_if "+
									" AND n.cg_tipo = 'P' "+
									" AND er.ic_pyme = d.ic_pyme "+
									" AND d.ic_estado = edo.ic_estado "+
									" AND d.cs_fiscal='S' "+
									" AND er.ic_pyme = c.ic_pyme  "+
									" AND c.cs_borrado='N' "+
									" AND c.ic_cuenta_bancaria = rel.ic_cuenta_bancaria  "+
									"  AND c.ic_moneda = m.ic_moneda  "+
								   " AND er.ic_epo = rel.ic_epo   "+
									" AND rel.cs_borrado = 'N'  "+
									" AND (rel.df_param_aut_conv_unico IS NOT NULL OR rel.df_reasigno_cta IS NOT NULL) ");
								  
 
		for(int i=0;i<pageIds.size();i++) {
				List lItem = (ArrayList)pageIds.get(i);
				if(i==0) {
					qrySentencia.append("  AND  ( "); 
				} else {
					qrySentencia.append("  OR  "); 
				}
				qrySentencia.append("  (er.ic_pyme = ? AND er.ic_epo = ? AND rel.ic_if = ? AND m.ic_moneda = ? )  "); 

				conditions.add(new Long(lItem.get(0).toString()));
				conditions.add(new Long(lItem.get(1).toString()));
				conditions.add(new Long(lItem.get(2).toString()));
				conditions.add(new Long(lItem.get(3).toString()));
		}//for(int i=0;i<ids.size();i++)
					qrySentencia.append("  ) "); 
					


		
		
			log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
			log.debug(" getDocumentSummaryQueryForIds "+qrySentencia.toString());
	
		
		return qrySentencia.toString();
	}
	

	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

			
			qrySentencia.append(" SELECT  /*+ index(p) use_nl(er, e, n, d, edo,c, m, rel ) index(rel) */  "+
								 " e.cg_razon_social AS nombre__epo,"+
								  " er.cg_pyme_epo_interno AS no__proveedor,"+
								  " n.ic_nafin_electronico AS nafin__electronico, "+
								  " p.in_numero_sirac AS sirac, "+
								  " rel.cs_estatus AS estatus, "+
								  " p.cg_razon_social AS razon__social,"+
								  " d.cg_calle || ' ' || d.cg_numero_ext	 || ' ' || d.cg_numero_int AS domicilio, "+
								  " edo.cd_nombre AS estado,"+
								  " p.cg_telefono AS telefono, "+
								  " p.cg_email AS correo,");
								  
			qrySentencia.append(" TO_CHAR(er.df_habilitacion,'DD/MM/YYYY') as fecha__Habilitacion,  ");
		
			qrySentencia.append(" TO_CHAR(p.df_alta,'DD/MM/YYYY') as Fecha__Afiliacion,  "+
								  " m.cd_nombre AS moneda, "+
								  " c.cg_numero_cuenta AS cuenta__cheques ");
								  
			qrySentencia.append(" FROM comcat_pyme p, "+
										  " comrel_pyme_epo er,  "+
										  " comcat_epo e, "+
										  " comrel_nafin n, "+
										  " com_domicilio d, "+
										  " comcat_estado edo, "+
										  "  comrel_cuenta_bancaria c, "+
										  "  comcat_moneda m, "+
										  "  comrel_pyme_if rel ");
											  
									   					
			qrySentencia.append(" WHERE  er.ic_pyme = p.ic_pyme "+
											" AND er.ic_epo = e.ic_epo "+
											" AND er.ic_pyme = n.ic_epo_pyme_if "+
											" AND n.cg_tipo = 'P' "+
											" AND er.ic_pyme = d.ic_pyme "+
											" AND d.ic_estado = edo.ic_estado "+
											" AND d.cs_fiscal='S' "+
											" AND er.ic_pyme = c.ic_pyme  "+
											" AND c.cs_borrado='N' "+
											" AND c.ic_cuenta_bancaria = rel.ic_cuenta_bancaria  "+
											"  AND c.ic_moneda = m.ic_moneda  "+
											" AND er.ic_epo = rel.ic_epo   "+
											" AND rel.cs_borrado = 'N'  "+
											" AND (rel.df_param_aut_conv_unico IS NOT NULL OR rel.df_reasigno_cta IS NOT NULL) ");
										  
		 
	

		//Intermediario Financiero
		 if((!"".equals(usuario) && usuario!=null))
	 		{
					qrySentencia.append(" AND   rel.ic_if = ? ") ;
				conditions.add(usuario);
			}			

		
		//por Banco de Fondeo
		 if((!"".equals(banco) && banco!=null))
	 		{
				qrySentencia.append(" AND  e.ic_banco_fondeo = ? ") ;
				conditions.add(banco);
			}
		
		//Numero de Epo
		if((!"".equals(noepo) && noepo!=null))
	 		{
				qrySentencia.append(" AND  e.ic_epo = ? ") ;
				conditions.add(noepo);
			}
		
		//Numero de Pyme
			if((!"".equals(ic_pyme) && ic_pyme!=null))
	 		{
				qrySentencia.append(" AND  p.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}			
		
		// Numero de Sirac
		if((!"".equals(sirac) && sirac!=null))
	 		{
				qrySentencia.append(" AND  p.in_numero_sirac = ? ") ;
				conditions.add(sirac);
			}
			
				//Fecha de Habilitacion
		 if( (!"".equals(fhabilitacioIni) && fhabilitacioIni!=null) && (!"".equals(fhabilitacioFin) && fhabilitacioFin!=null) ) {
					qrySentencia.append(" AND  er.df_habilitacion >=  to_date(?, 'dd/mm/yyyy')  ") ;
					qrySentencia.append(" AND  er.df_habilitacion <= to_date(?, 'dd/mm/yyyy')+1  ") ;
					conditions.add(fhabilitacioIni);
					conditions.add(fhabilitacioFin);
		}	
						//Fecha de Afiliacion
		 if( (!"".equals(fafiliacionIni) && fafiliacionIni!=null) && (!"".equals(fafiliacionFin) && fafiliacionFin!=null) ) {
					qrySentencia.append(" AND  p.df_alta >=  to_date(?, 'dd/mm/yyyy')  ") ;
					qrySentencia.append(" AND  p.df_alta <= to_date(?, 'dd/mm/yyyy')+1 ") ;
					conditions.add(fafiliacionIni);
					conditions.add(fafiliacionFin);
			}
		
		//Pymes sin numero de proveedor 
		if("S".equals(sinNumProv) ) 	{
				qrySentencia.append(" AND er.cs_num_proveedor  = ?  ") ;
				conditions.add(sinNumProv);
			}
		
			// Pymes por habilitar  
	
		if("N".equals(porhabilitar) ) 	{	
			qrySentencia.append("  AND rel.cs_vobo_if= 'N'  ") ;
			
		}else {
			qrySentencia.append("  AND rel.cs_vobo_if = 'S'  ") ;
		}
		
		
		qrySentencia.append("  order by p.cg_razon_social, e.cg_razon_social, c.ic_cuenta_bancaria");
		
		
			log.debug("getDocumentQueryFile "+conditions.toString());
			log.debug("getDocumentQueryFile "+qrySentencia.toString());
		
		return qrySentencia.toString();
	}//getDocumentQueryFile
	
public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)
	{
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		try {
		 if(tipo.equals("CSV")) {
		  linea = "EPO,No. Proveedor,No. Nafin Electronico,No. SIRAC,"+
			"Estatus,Nombre o Raz�n Social,Domicilio,Estado,Tel�fono,Correo"+
			"Electr�nico,Fecha Habilitacion,Fecha Afiliacion,Moneda,No. de Cuenta de Cheques\n";
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		  writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		  buffer = new BufferedWriter(writer);
			buffer.write(linea);
			 
		  while (rs.next()) {
					
					String epo = (rs.getString("nombre__epo") == null) ? "" : rs.getString("nombre__epo");
					String noProv = (rs.getString("no__proveedor") == null) ? "" : rs.getString("no__proveedor");
					String noNafin = (rs.getString("nafin__electronico") == null) ? "" : rs.getString("nafin__electronico");
					String sirac = (rs.getString("sirac") == null) ? "" : rs.getString("sirac");
					String estatus = (rs.getString("estatus") == null) ? "" : rs.getString("estatus");
					String razon = (rs.getString("razon__social") == null) ? "" : rs.getString("razon__social");
					String domicilio = (rs.getString("domicilio") == null) ? "" : rs.getString("domicilio");
					String estado = (rs.getString("estado") == null) ? "" : rs.getString("estado");
					String telefono = (rs.getString("telefono") == null) ? "" : rs.getString("telefono");
					String correo = (rs.getString("correo") == null) ? "" : rs.getString("correo");
					String fechaH = (rs.getString("fecha__Habilitacion") == null) ? "" : rs.getString("fecha__Habilitacion");
					String fechaA = (rs.getString("Fecha__Afiliacion") == null) ? "" : rs.getString("Fecha__Afiliacion");
					String moneda = (rs.getString("moneda") == null) ? "" : rs.getString("moneda");
					String cheques = (rs.getString("cuenta__cheques") == null) ? "" : rs.getString("cuenta__cheques");
					
					
					
					linea = epo.replace(',',' ') + ", " + 
									noProv + ", " +
									noNafin + ", " +
									sirac + ", " +
									estatus + ", " +
									razon.replace(',',' ') + ", " +
									domicilio.replace(',',' ') + ", " +
									estado+ ", " +
									telefono + ", " +
									correo + ", " +
									fechaH + ", " +
									fechaA + ", " +
									moneda + ", " +
									cheques.replace(',',' ') + "\n" ;
												 
					buffer.write(linea);
		  }
		  buffer.close();
		 }
		 }catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
	return nombreArchivo;
	}
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
 	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	
	public String getDirectorio() {
		return directorio;
	}

	public void setDirectorio(String directorio) {
		this.directorio = directorio;
	}

public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getTxt_nombre() {
		return txt_nombre;
	}

	public void setTxt_nombre(String txt_nombre) {
		this.txt_nombre = txt_nombre;
	}

	public String getTxt_nafelec() {
		return txt_nafelec;
	}

	public void setTxt_nafelec(String txt_nafelec) {
		this.txt_nafelec = txt_nafelec;
	}

	public String getHid_nombre() {
		return hid_nombre;
	}

	public void setHid_nombre(String hid_nombre) {
		this.hid_nombre = hid_nombre;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getSirac() {
		return sirac;
	}

	public void setSirac(String sirac) {
		this.sirac = sirac;
	}

	public String getFhabilitacioIni() {
		return fhabilitacioIni;
	}

	public void setFhabilitacioIni(String fhabilitacioIni) {
		this.fhabilitacioIni = fhabilitacioIni;
	}

	public String getFhabilitacioFin() {
		return fhabilitacioFin;
	}

	public void setFhabilitacioFin(String fhabilitacioFin) {
		this.fhabilitacioFin = fhabilitacioFin;
	}

	public String getFafiliacionIni() {
		return fafiliacionIni;
	}

	public void setFafiliacionIni(String fafiliacionIni) {
		this.fafiliacionIni = fafiliacionIni;
	}

	public String getFafiliacionFin() {
		return fafiliacionFin;
	}

	public void setFafiliacionFin(String fafiliacionFin) {
		this.fafiliacionFin = fafiliacionFin;
	}

	public String getSinNumProv() {
		return sinNumProv;
	}

	public void setSinNumProv(String sinNumProv) {
		this.sinNumProv = sinNumProv;
	}

	public String getPorhabilitar() {
		return porhabilitar;
	}

	public void setPorhabilitar(String porhabilitar) {
		this.porhabilitar = porhabilitar;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getNoepo() {
		return noepo;
	}

	public void setNoepo(String noepo) {
		this.noepo = noepo;
	}
}