package com.netro.cadenas;

import com.netro.exception.NafinException;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

/**
 * Clase para la paramtrizaci�n de proveedores susceptibles s Floating
 */
public class CargaPymeFloating {

	private static final Log LOG = ServiceLocator.getInstance().getLog(CargaPymeFloating.class);

	/**
	 * @param esNombreArchivo
	 * @return
	 * @throws AppException
	 */
	public Map<String, Object> procesarCargaPymeFloating(String esNombreArchivo) throws AppException {

		LOG.info("  procesarCargaPymeFloating(E)  ");

		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		Map<String, Object> regError = new HashMap<>();
		Map<String, Object> regExito = new HashMap<>();
		Map<String, Object> datos = new HashMap<>();
		Map<String, Object> regCarga = new HashMap<>();
		Map<String, Object> regNOSuceptible = new HashMap<>();
		String regAcuse = "";
		List listaExito = new ArrayList();
		List listaError = new ArrayList();
		List listaCarga = new ArrayList();
		List listNoFloating = new ArrayList();
		StringBuilder listaPymes = new StringBuilder("");

		try {

			con.conexionDB();

			// ABRIR ARCHIVO CARGADO
			java.io.File lofArchivo = new java.io.File(esNombreArchivo);			
			BufferedReader br = new BufferedReader (new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
			String linea = "";
			VectorTokenizer vt = new VectorTokenizer();
			Vector vecdat = new Vector();
			String rfc = "";
			String nafinElectronico = "";
			int numLinea = 0;

			while ((linea = br.readLine()) != null) {

				numLinea++;
				vt = new VectorTokenizer(linea, "|");
				vecdat = vt.getValuesVector();
				boolean rfcValido = true;
				boolean bNafinElect = true;
				regError = new HashMap<>();
				regExito = new HashMap<>();
				regCarga = new HashMap<>();
				regNOSuceptible = new HashMap<>();
				String icPyme = "";

				if (vecdat.size() < 2) {
					regError.put("LINEA", new Integer(numLinea));
					regError.put("CAMPO", "LINEA");
					regError.put("OBSERVACION", "No posee los campos requedidos.");
					rfcValido = false;
					bNafinElect = false;
				}
				if (vecdat.size() > 2) {
					regError.put("LINEA", new Integer(numLinea));
					regError.put("CAMPO", "LINEA");
					regError.put("OBSERVACION", "Excede de los campos requedidos.");
					rfcValido = false;
					bNafinElect = false;
				}

				if (vecdat.size() == 2) {
							  // validacion del rfc de pyme						
							  rfc = (String) vecdat.get(0);

					if (rfcValido) {

						if (rfc.length() == 0) {
							regError.put("LINEA", new Integer(numLinea));
							regError.put("CAMPO", "RFC");
							regError.put("OBSERVACION", "Campo Requerido.");
							rfcValido = false;
						} else if (rfc.length() < 14) {
							regError.put("LINEA", new Integer(numLinea));
							regError.put("CAMPO", "RFC");
							regError.put("OBSERVACION",
											 "El RFC debe ser separado por guiones Persona F�sica = (NNNN-AAMMDD-XXX), Persona Moral = (NNN-AAMMDD-XXX).");

							rfcValido = false;
						} else if (rfc.length() == 14 && !Comunes.validaRFC(rfc, 'M')) { // Se trata de una persona Moral							
											regError.put("LINEA", new Integer(numLinea));
							regError.put("CAMPO", "RFC");
							regError.put("OBSERVACION",
											 "El RFC debe ser separado por guiones Persona Persona Moral = (NNN-AAMMDD-XXX).");
							rfcValido = false;
						} else if (rfc.length() == 15 && !Comunes.validaRFC(rfc, 'F')) { // Se trata de una persona fisica							
											regError.put("LINEA", new Integer(numLinea));
							regError.put("CAMPO", "RFC");
							regError.put("OBSERVACION",
											 "El RFC debe ser separado por guiones Persona F�sica = (NNNN-AAMMDD-XXX)");
							rfcValido = false;
						}

						// Validar que el RFC venga en mayusculas
						if (rfcValido && tieneMinusculas(rfc)) {
							regError.put("LINEA", new Integer(numLinea));
							regError.put("CAMPO", "RFC");
							regError.put("OBSERVACION",
											 "El RFC debe venir en may�sculas y separado por guiones Persona F�sica = (NNNN-AAMMDD-XXX), Persona Moral = (NNN-AAMMDD-XXX).");
							rfcValido = false;
						}


						// Verificar que el RFC pertenezca a un proveedor
						if (rfcValido && "".equals(esProveedor(rfc))) {
							regError.put("LINEA", new Integer(numLinea));
							regError.put("CAMPO", "RFC");
							regError.put("OBSERVACION", "El RFC de la PYME no pertenece a un proveedor.");
							rfcValido = false;
						}

					}

					//validaci�n del nafin electronico de la pyme
					nafinElectronico = (String) vecdat.get(1);

					if (bNafinElect) {

						if (nafinElectronico.length() == 0) {
							regError.put("LINEA", new Integer(numLinea));
							regError.put("CAMPO", "NAFIN ELECTRONICO");
							regError.put("OBSERVACION", "Campo Requerido.");
							bNafinElect = false;
						}
						if (bNafinElect && !Comunes.esNumero(nafinElectronico)) {
							regError.put("LINEA", new Integer(numLinea));
							regError.put("CAMPO", "NAFIN ELECTRONICO");
							regError.put("OBSERVACION", "No es un n&uacute;mero.");
							bNafinElect = false;
						}

						// Verificar que el RFC pertenezca a un proveedor
						if (bNafinElect && "".equals(esProveedorNafinElec(nafinElectronico))) {
							regError.put("LINEA", new Integer(numLinea));
							regError.put("CAMPO", "NAFIN ELECTRONICO");
							regError.put("OBSERVACION", "El Nafin Electr�nico de la Pyme no pertenece a un proveedor.");
							bNafinElect = false;
						}

						if (bNafinElect && rfcValido && !esProveedor(rfc).equals(esProveedorNafinElec(nafinElectronico))) {
							regError.put("LINEA", new Integer(numLinea));
							regError.put("CAMPO", "NAFIN ELECTRONICO");
							regError.put("OBSERVACION", "El Nafin Electr�nico no corresponde a la RFC de la pyme.");
							bNafinElect = false;
						}
					}
				}

				if (bNafinElect && rfcValido) {

					icPyme = esProveedorNafinElec(nafinElectronico);

					listaPymes.append(listaPymes.length() > 0 ? ", " : "").append(icPyme);

					Map<String, Object> info = new HashMap<>();
					info = this.datosPymeFloating(icPyme);
					String floating = (String) info.get("CS_TASA_FLOATING");
					String razonSocial = (String) info.get("CG_RAZON_SOCIAL");
					String descriFloating = "Registro Nuevo";
					String parametrizacion = "Nuevo";

					if ("SI".equals(floating)) {
						descriFloating = "Registro Actualizado";
						parametrizacion = "Actualizado";
					}

					regExito.put("LINEA", new Integer(numLinea));
					regExito.put("CAMPO", rfc);
					regExito.put("OBSERVACION", descriFloating);
					listaExito.add(regExito);

					// estos datos son para mostrar los el acuse						
					regCarga.put("IC_PYME", icPyme);
					regCarga.put("NOMBRE_PYME", razonSocial);
					regCarga.put("RFC", rfc);
					regCarga.put("NAFIN_ELECTRONICO", nafinElectronico);
					regCarga.put("SUCEPTIBLE", floating);
					regCarga.put("PARAMETRIZACION", parametrizacion);
					listaCarga.add(regCarga);

				} else {
					listaError.add(regError);
				}

				datos.put("ERROR", listaError);
				datos.put("EXITO", listaExito);
				datos.put("CARGA", listaCarga);
				datos.put("ACUSE", regAcuse);

			} //while ((linea = br.readLine()) != null)		
			
			//Pymes que dejaran de ser Suceptibles a Floating
			if (!"".equals(listaPymes.toString())) {

				ArrayList infoPyNoSup = new ArrayList();
				infoPyNoSup = this.datosPymeNOFloating(listaPymes.toString());

				for (int y = 0; y < infoPyNoSup.size(); y++) {

					HashMap datN = (HashMap) infoPyNoSup.get(y);

					String icPymeN = (String) datN.get("IC_PYME");
					String razonSocialN = (String) datN.get("CG_RAZON_SOCIAL");
					String rfcN = (String) datN.get("CG_RFC");
					String nafinN = (String) datN.get("IC_NAFIN_ELECTRONICO");

					regNOSuceptible = new HashMap<>();
					regNOSuceptible.put("IC_PYME", icPymeN);
					regNOSuceptible.put("NOMBRE_PYME", razonSocialN);
					regNOSuceptible.put("RFC", rfcN);
					regNOSuceptible.put("NAFIN_ELECTRONICO", nafinN);
					regNOSuceptible.put("PARAMETRIZACION", "Eliminado");
					listNoFloating.add(regNOSuceptible);
				}
				//  agregar la lista de Pymes que dejaran de ser suceptibles a Floating
			}
			
			datos.put("NOFLOATING", listNoFloating);


		} catch (Exception e) {
			LOG.error("procesarCargaPymeFloating(Exception)" + e);
			bOk = false;
			throw new AppException("Error en la Validacion de la Carga Masiva de PYMES suceptibles a Floating ");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			LOG.info("procesarCargaPymeFloating(S)");
		}


		return datos;

	}

	/**
	 * valida si un cadena tiene minusculas
	 * @param cadena
	 * @return
	 */
	private boolean tieneMinusculas(String cadena) {

		if ("".equals(cadena.trim())) {
			return false;
		}

		String letras = "abcdefghijklmn�opqrstuvwxyz������";
		for (int i = 0; i < cadena.length(); i++) {
			char c = cadena.charAt(i);
			if (letras.indexOf(c) != -1) {
				return true;
			}
		}
		return false;
	}


	/**
	 * metodo para validar si rfc pertenece a una pyme
	 * @param rfc
	 * @return
	 */
	private String esProveedor(String rfc) {
		LOG.info("esProveedor(E)");
		AccesoDB con = new AccesoDB();
		StringBuilder query = new StringBuilder();
		String valor = "";
		List lVarBind = new ArrayList();

		try {
			con.conexionDB();

			query.append(" SELECT 	IC_PYME FROM COMCAT_PYME  WHERE CG_RFC 	= ? ");
			lVarBind.add(rfc);

			PreparedStatement ps = con.queryPrecompilado(query.toString(), lVarBind);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				valor = rs.getString("IC_PYME") == null ? "" : rs.getString("IC_PYME");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			LOG.error("esProveedor(Exception)" + e);
			throw new AppException("Error al validar RFC de Proveedor");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			LOG.info("esProveedor(S)");
		}

		return valor;
	}


	/**
	 * metodo que valida si el nafin electronico pertenece a una pyme
	 * @param nafinElectronico
	 * @return
	 */
	private String esProveedorNafinElec(String nafinElectronico) {
		LOG.info("esProveedorNafinElec(E)");
		AccesoDB con = new AccesoDB();
		StringBuilder query = new StringBuilder();
		List lVarBind = new ArrayList();
		String valor = "";

		try {
			con.conexionDB();

			query.append(" select IC_EPO_PYME_IF  from comrel_nafin  where  IC_NAFIN_ELECTRONICO = ?   and cg_tipo = ?  ");
			lVarBind.add(nafinElectronico);
			lVarBind.add("P");

			PreparedStatement ps = con.queryPrecompilado(query.toString(), lVarBind);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				valor = rs.getString("IC_EPO_PYME_IF") == null ? "" : rs.getString("IC_EPO_PYME_IF");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			LOG.error("esProveedorNafinElec(Exception)" + e);
			throw new AppException("Error al validar NAFIN ELECTRONICO DE   Proveedor");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			LOG.info("esProveedorNafinElec(S)");
		}

		return valor;
	}


	/**
	 *  Pymes que seran parametriadas como Susceptible a Floating
	 * @param icPyme
	 * @return
	 * @throws NafinException
	 */
	private Map<String, Object> datosPymeFloating(String icPyme) throws NafinException {
		LOG.info("datosPymeFloating (e)");

		AccesoDB con = new AccesoDB();
		StringBuilder qry = new StringBuilder();
		List lVarBind = new ArrayList();
		Map<String, Object> informacion = new HashMap<>();

		try {
			con.conexionDB();

			qry = new StringBuilder();
			qry.append(" SELECT  IC_PYME, CG_RAZON_SOCIAL , decode (CS_TASA_FLOATING, 'S', 'SI', 'N', 'NO') AS CS_TASA_FLOATING " +
						  " FROM COMCAT_PYME  WHERE IC_PYME  = ?     ");

			lVarBind = new ArrayList();
			lVarBind.add(icPyme);

			PreparedStatement ps = con.queryPrecompilado(qry.toString(), lVarBind);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				informacion = new HashMap<>();
				informacion.put("IC_PYME", rs.getString("IC_PYME") == null ? "" : rs.getString("IC_PYME"));
				informacion.put("CG_RAZON_SOCIAL",
									 rs.getString("CG_RAZON_SOCIAL") == null ? "" : rs.getString("CG_RAZON_SOCIAL"));
				informacion.put("CS_TASA_FLOATING",
									 rs.getString("CS_TASA_FLOATING") == null ? "" : rs.getString("CS_TASA_FLOATING"));
			}
			rs.close();
			ps.close();


		} catch (Exception e) {
			LOG.error("Exception en datosPymeFloating. " + e);
			throw new AppException("Error datosPymeFloating   ");
		} finally {
			LOG.info("datosPymeFloating (S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return informacion;
	}


	/**
	 * Metodo para obtener las pymes que dejaran de ser Susceptibles a Floating
	 * @param icPyme
	 * @return
	 * @throws NafinException
	 */
	private ArrayList datosPymeNOFloating(String lisPymes) throws NafinException {
		LOG.info("datosPymeNOFloating (e)");

		AccesoDB con = new AccesoDB();
		StringBuilder qry = new StringBuilder();
		List lVarBind = new ArrayList();
		Map<String, Object> informacion = new HashMap<>();
		ArrayList respuesta = new ArrayList();

		try {
			con.conexionDB();

			qry = new StringBuilder();
			qry.append(" SELECT IC_PYME   , CG_RAZON_SOCIAL , CG_RFC,  IC_NAFIN_ELECTRONICO  " +
						  " FROM COMCAT_pyme   s, comrel_nafin   n  " + " WHERE  ic_epo_pyme_if = ic_pyme  " +
						  " and CS_TASA_FLOATING = ?  " + " and  cg_tipo = ?   " + " and ic_pyme not in (" + lisPymes + ")  " +
						  " ORDER BY IC_PYME  ");

			lVarBind = new ArrayList();
			lVarBind.add("S");
			lVarBind.add("P");

			LOG.debug("qry.toString():: "+qry.toString() +"lVarBind ::: "+lVarBind);
			
			PreparedStatement ps = con.queryPrecompilado(qry.toString(), lVarBind);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				informacion = new HashMap<>();
				informacion.put("IC_PYME", rs.getString("IC_PYME") == null ? "" : rs.getString("IC_PYME"));
				informacion.put("CG_RAZON_SOCIAL",
									 rs.getString("CG_RAZON_SOCIAL") == null ? "" : rs.getString("CG_RAZON_SOCIAL"));
				informacion.put("CG_RFC", rs.getString("CG_RFC") == null ? "" : rs.getString("CG_RFC"));
				informacion.put("IC_NAFIN_ELECTRONICO",
									 rs.getString("IC_NAFIN_ELECTRONICO") == null ? "" : rs.getString("IC_NAFIN_ELECTRONICO"));
				respuesta.add(informacion);
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			LOG.error("Exception en datosPymeNOFloating. " + e);
			throw new AppException("Error datosPymeNOFloating   ");
		} finally {
			LOG.info("datosPymeNOFloating (S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return respuesta;
	}

	/**
	 * M�todo que realiza la parametrizaci�n
	 * @param datos
	 * @throws NafinException
	 */
	public void paramPymeFloating(Map<String, Object> datos) throws NafinException {

		LOG.info("paramPymeFloating (E)");

		AccesoDB con = new AccesoDB();
		StringBuilder query = new StringBuilder();
		List lVarBind = new ArrayList();
		boolean lbOK = true;

		try {

			con.conexionDB();


			String pymesFloating = (String) datos.get("pymesFloating");
			String acuse = (String) datos.get("acuse");
			String recibo = (String) datos.get("recibo");
			String usuario = (String) datos.get("usuario");
			String fechaHora = (String) datos.get("fechaHora");
			String regValidos = (String) datos.get("regValidos");
			String totalReg = (String) datos.get("totalReg");
			String pymesNOFloating = (String) datos.get("pymesNOFloating");


			// Update para las PYMES que seran suceptible a Floating (CS_TASA_FLOATING =  S)
			List pymes = new VectorTokenizer(pymesFloating, "|").getValuesVector();

			query = new StringBuilder();
			query.append(" INSERT INTO COM_ACUSE_FLOATING " +
							 "( CC_ACUSE, CC_RECIBO ,  DF_OPERACION, CG_USUARIO , IN_REGISTROS_VALIDOS ,  IN_REGISTROS_TOTAL )" +
							 " VALUES (  ?, ?,  TO_DATE(?, 'DD/MM/YYYY HH24:mi:ss'), ?, ?, ?  ) ");

			lVarBind = new ArrayList();
			lVarBind.add(acuse);
			lVarBind.add(recibo);
			lVarBind.add(fechaHora);
			lVarBind.add(usuario);
			lVarBind.add(regValidos);
			lVarBind.add(totalReg);

			PreparedStatement ps1 = con.queryPrecompilado(query.toString(), lVarBind);
			ps1.executeUpdate();
			ps1.close();


			for (int i = 0; i < pymes.size(); i++) {
				String icPyme = (String) pymes.get(i);

				query = new StringBuilder();
				query.append(" UPDATE COMCAT_PYME " + " SET CS_TASA_FLOATING  = ?  " + " , CC_ACUSE =  ? " +
								 " WHERE IC_PYME = ? ");

				lVarBind = new ArrayList();
				lVarBind.add("S");
				lVarBind.add(acuse);
				lVarBind.add(icPyme);

				PreparedStatement ps2 = con.queryPrecompilado(query.toString(), lVarBind);
				ps2.executeUpdate();
				ps2.close();
			}


			// Update para las PYMES que dejaran de ser Suceptibles Floating (CS_TASA_FLOATING =  N)
			List pymesNoFloa = new VectorTokenizer(pymesNOFloating, "|").getValuesVector();

			LOG.debug(" pymesNoFloa." + pymesNoFloa);


			for (int i = 0; i < pymesNoFloa.size(); i++) {
				String icPyme = (String) pymesNoFloa.get(i);

				query = new StringBuilder();
				query.append(" UPDATE COMCAT_PYME  SET CS_TASA_FLOATING  = ?  , CC_ACUSE =  ? " + " WHERE IC_PYME = ? ");

				lVarBind = new ArrayList();
				lVarBind.add("N");
				lVarBind.add(acuse);
				lVarBind.add(icPyme);

				PreparedStatement ps2 = con.queryPrecompilado(query.toString(), lVarBind);
				ps2.executeUpdate();
				ps2.close();
			}


		} catch (Exception e) {
			LOG.error("Exception en paramPymeFloating. " + e);
			lbOK = false;
			throw new AppException("Error paramPymeFloating   ");
		} finally {
			con.terminaTransaccion(lbOK);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			LOG.info("paramPymeFloating (S)");

		}

	}

	/**
	 *  M�todo para imprimir el acuse
	 * @param request
	 * @param path
	 * @param datosPdf
	 * @param infoDatos
	 * @param datosNoSucpPdf
	 * @return
	 */
	public String imprimirDatos(HttpServletRequest request, String path, String[] datosPdf, Map<String, Object> infoDatos,
										 String[] datosNoSucpPdf) {

		LOG.info("imprimirDatos (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();

		try {


			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String[] meses = {
						"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
						"Noviembre", "Diciembre"
			};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual = fechaActual.substring(0, 2);
			String mesActual = meses[Integer.parseInt(fechaActual.substring(3, 5)) - 1];
			String anioActual = fechaActual.substring(6, 10);
			String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc, (String) session.getAttribute("strPais"),
												  ((session.getAttribute("iNoNafinElectronico") == null) ? "" :
													session.getAttribute("iNoNafinElectronico").toString()), (String) session.getAttribute("sesExterno"),
												  (String) session.getAttribute("strNombre"), (String) session.getAttribute("strNombreUsuario"),
												  (String) session.getAttribute("strLogo"),
												  (String) session.getServletContext().getAttribute("strDirectorioPublicacion"));
			pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
								" ----------------------------- " + horaActual, "formas", ComunesPDF.RIGHT);

			pdfDoc.addText("  ", "formas", ComunesPDF.RIGHT);
			pdfDoc.addText("La carga se llev� a cabo con �xito Recibo:" + infoDatos.get("recibo").toString(), "formas",
								ComunesPDF.CENTER);
			pdfDoc.addText("  ", "formas", ComunesPDF.RIGHT);

			pdfDoc.setTable(2, 50);

			pdfDoc.setCell("Acuse", "celda01", ComunesPDF.CENTER, 2);
			pdfDoc.setCell("N�mero de Acuse", "celda01", ComunesPDF.LEFT);
			pdfDoc.setCell(infoDatos.get("acuse").toString(), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Fecha y Hora", "celda01", ComunesPDF.LEFT);
			pdfDoc.setCell(infoDatos.get("fechaHora").toString(), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Usuario", "celda01", ComunesPDF.LEFT);
			pdfDoc.setCell(infoDatos.get("usuario").toString(), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Registros Validos", "celda01", ComunesPDF.LEFT);
			pdfDoc.setCell(infoDatos.get("regValidos").toString(), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Total de Registros ", "celda01", ComunesPDF.LEFT);
			pdfDoc.setCell(infoDatos.get("totalReg").toString(), "formas", ComunesPDF.LEFT);
			pdfDoc.addTable();

			pdfDoc.addText("  ", "formas", ComunesPDF.RIGHT);
			pdfDoc.addText("  ", "formas", ComunesPDF.RIGHT);

			pdfDoc.setTable(4, 80);
			pdfDoc.setCell("Carga de Proveedores Susceptible a Floating", "celda01", ComunesPDF.CENTER, 4);
			pdfDoc.setCell("Nombre Proveedor", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("RFC", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Nafin Electr�nico", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Parametrizaci�n", "celda01", ComunesPDF.CENTER);


			if (datosPdf.length > 1) {
				for (int i = 0; i < datosPdf.length; i++) {

					String linea = datosPdf[i];
					VectorTokenizer vtd = new VectorTokenizer(linea, "|");
					Vector vdata = vtd.getValuesVector();

					String nombrePyme = vdata.get(0).toString();
					String rfcPyme = vdata.get(1).toString();
					String nafinElec = vdata.get(2).toString();
					String parametrizacion = vdata.get(3).toString();

					pdfDoc.setCell(nombrePyme, "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(rfcPyme, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(nafinElec, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(parametrizacion, "formas", ComunesPDF.CENTER);

				}
			}
			pdfDoc.addTable();

			pdfDoc.addText("  ", "formas", ComunesPDF.RIGHT);
			pdfDoc.addText("  ", "formas", ComunesPDF.RIGHT);

			pdfDoc.setTable(4, 80);
			pdfDoc.setCell("Proveedores que dejar�n de ser Susceptible a Floating ", "celda01", ComunesPDF.CENTER, 4);
			pdfDoc.setCell("Nombre Proveedor", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("RFC", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Nafin Electr�nico", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Parametrizaci�n", "celda01", ComunesPDF.CENTER); 
				
			if (datosNoSucpPdf.length > 0) {

				for (int i = 0; i < datosNoSucpPdf.length; i++) {

					String linea = datosNoSucpPdf[i];					
					VectorTokenizer vtd = new VectorTokenizer(linea, "|");					
					Vector vdata = vtd.getValuesVector();	
					
					if(!vdata.isEmpty()){
						
						String nombrePyme = vdata.get(0).toString();
						String rfcPyme = vdata.get(1).toString();
						String nafinElec = vdata.get(2).toString();
						String parametrizacion = vdata.get(3).toString();
	
						pdfDoc.setCell(nombrePyme, "formas", ComunesPDF.LEFT);
						pdfDoc.setCell(rfcPyme, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(nafinElec, "formas", ComunesPDF.CENTER);
						pdfDoc.setCell(parametrizacion, "formas", ComunesPDF.CENTER);
					}

				}
			}
			pdfDoc.addTable();

			pdfDoc.endDocument();

		} catch (Exception e) {
			LOG.error("Exception en imprimir Acuse . " + e);
			throw new AppException("Error al generar el archivo  PDF de acuse ", e);
		} finally {
			LOG.info("imprimirDatos (S)");
		}
		return nombreArchivo;
	}


}
