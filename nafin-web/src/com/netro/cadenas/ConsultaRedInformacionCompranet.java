package com.netro.cadenas;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.ServiceLocator;

public class ConsultaRedInformacionCompranet implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaRedInformacionCompranet.class);
	private List   conditions;
	private String mesIni;
	private String anioIni;
	private String mesFin;
	private String anioFin;
	private String federales;
	private String otros;
	private String sNoEposSelec;
	private String icEpoCompranet;

	public ConsultaRedInformacionCompranet(){}

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery() {
		return null;
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery() {
		return null;
	}


	/**
	 * Realiza la consulta para generar el archivo csv
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		String fechaInicial = "";
		String fechaFinal   = "";

		fechaInicial = "01/" + this.mesIni + "/" + this.anioIni;
		if( this.mesFin.equals("02")){
			fechaFinal =  "28/" + this.mesFin + "/" + this.anioFin;
		}else{
			fechaFinal =  "30/" + this.mesFin + "/" + this.anioFin;
		}

		qrySentencia.append("SELECT E.CG_RAZON_SOCIAL AS DEPENDENCIA,                             \n ");
		qrySentencia.append("       C.CG_LICITACION AS LICITACION,                                \n ");
		qrySentencia.append("       C.CG_UNIDAD AS UNIDAD,                                        \n ");
		qrySentencia.append("       C.CG_TIPO_CONTRATACION AS CONTRATACION,                       \n ");
		qrySentencia.append("       TO_CHAR (C.DF_FECHA_EMISION, 'dd/mm/yyyy') AS FECHA_EMISION,  \n ");
		qrySentencia.append("       C.CG_NUM_PARTIDA AS PARTIDA,                                  \n ");
		qrySentencia.append("       C.FN_IMPORTE_SIN_IVA AS IMPORTE,                              \n ");
		qrySentencia.append("       P.CG_RAZON_SOCIAL AS RAZON_SOCIAL                             \n ");
		qrySentencia.append("  FROM COM_CRUCE_COMPRANET C,                                        \n ");
		qrySentencia.append("       COMCAT_EPO_COMPRANET E,                                       \n ");
		qrySentencia.append("       COMCAT_PYME P,                                                \n ");
		qrySentencia.append("       COMCAT_DEPENDENCIA_COMPRANET D                                \n ");
		qrySentencia.append(" WHERE C.IG_CLASIF_DEPENDENCIA = D.IC_CLASIF_DEPENDENCIA             \n ");
		qrySentencia.append("   AND E.IC_EPO_COMPRANET = C.IC_EPO_COMPRANET                       \n ");
		qrySentencia.append("   AND P.IC_PYME = C.IC_PYME_COMPRANET                               \n ");
		qrySentencia.append("   AND E.CS_NUEVO = 'N'                                              \n ");
		qrySentencia.append("   AND E.IG_CLASIF_DEPENDENCIA != 0                                  \n ");
		if(!"".equals(this.federales) && !"".equals(this.otros)){
			qrySentencia.append("   AND D.IC_CLASIF_DEPENDENCIA IN(?,?)                            \n ");
			conditions.add(this.federales);
			conditions.add(this.otros);
		} else if(!"".equals(this.federales)){
			qrySentencia.append("   AND D.IC_CLASIF_DEPENDENCIA = ?                                \n ");
			conditions.add(this.federales);
		} else if(!"".equals(this.otros)){
			qrySentencia.append("   AND D.IC_CLASIF_DEPENDENCIA = ?                                \n ");
			conditions.add(this.otros);
		}
		if(!"".equals(this.icEpoCompranet)){
			qrySentencia.append("   AND C.IC_EPO_COMPRANET IN("+ this.icEpoCompranet +")           \n ");
		}
		
		qrySentencia.append("   AND DF_FECHA_EMISION >=  TO_DATE(?,'dd/mm/yyyy')                  \n ");
		qrySentencia.append("   AND DF_FECHA_EMISION <=  TO_DATE(?,'dd/mm/yyyy')+1                   ");

		conditions.add(fechaInicial);
		conditions.add(fechaFinal);

		log.info("Sentencia:\n"   + qrySentencia.toString());
		log.info("Condiciones: "  + conditions.toString()  );
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();

	}

	/**
	 * Metodo para generar un archivo PDF, utilizando paginaci�n
	 * @return nombreArchivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros rs, String path, String tipo) {
		return null;
	}

	/**
	 * Metodo para generar un archivo CSV, de todos los registros
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");

		String             nombreArchivo    = "";
		StringBuffer       contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer           = null;
		BufferedWriter     buffer           = null;
		int                total            = 0;

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer        = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer        = new BufferedWriter(writer);

			contenidoArchivo = new StringBuffer();
			contenidoArchivo.append("Dependencia,"           );
			contenidoArchivo.append("Licitaci�n,"            );
			contenidoArchivo.append("Unidad,"                );
			contenidoArchivo.append("Tipo Contrataci�n,"     );
			contenidoArchivo.append("Fecha de Emisi�n Fallo,");
			contenidoArchivo.append("No de Partida,"         );
			contenidoArchivo.append("Importe sin Iva,"       );
			contenidoArchivo.append("Razon Social"           );
			contenidoArchivo.append("\r\n"                   );

			while(rs.next()){
				String dependencia    = (rs.getString("DEPENDENCIA")     == null) ? "" : rs.getString("DEPENDENCIA");
				String licitacion     = (rs.getString("LICITACION")      == null) ? "" : rs.getString("LICITACION");
				String unidad         = (rs.getString("UNIDAD")          == null) ? "" : rs.getString("UNIDAD");
				String contratacion   = (rs.getString("CONTRATACION")    == null) ? "" : rs.getString("CONTRATACION");
				String fechaEmision   = (rs.getString("FECHA_EMISION")   == null) ? "" : rs.getString("FECHA_EMISION");
				String partida        = (rs.getString("PARTIDA")         == null) ? "" : rs.getString("PARTIDA");
				String importe        = (rs.getString("IMPORTE")         == null) ? "" : rs.getString("IMPORTE");
				String razonSocial    = (rs.getString("RAZON_SOCIAL")    == null) ? "" : rs.getString("RAZON_SOCIAL");

				contenidoArchivo.append(dependencia.replace(',',' ')    +", ");
				contenidoArchivo.append(licitacion.replace(',',' ')     +", ");
				contenidoArchivo.append(unidad.replace(',',' ')         +", ");
				contenidoArchivo.append(contratacion.replace(',',' ')   +", ");
				contenidoArchivo.append(fechaEmision.replace(',',' ')   +", ");
				contenidoArchivo.append(partida.replace(',',' ')        +", ");
				contenidoArchivo.append(importe.replace(',',' ')        +", ");
				contenidoArchivo.append(razonSocial.replace(',',' ')         );
				contenidoArchivo.append("\r\n"                               );

				total++;

				if(total==1000){
					total=0;
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio
				}
			}

			//CUANDO NO HAY REGISTROS
			if(total == 0){
				contenidoArchivo.append( " No se encontr� ning�n registro para los criter�os seleccionados. " );
				contenidoArchivo.append( "\r\n");
			}

			buffer.write(contenidoArchivo.toString());
			buffer.close();
			contenidoArchivo = new StringBuffer();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo csv ", e);
		} finally {
			try {
			} catch(Exception e) {}
		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;

	}

/*****************************************
 *          M�TODOS DEL FODEA            *
 *****************************************/
	/**
	 * OBTENGO EL NUMERO DE EPOS COMPRANET
	 * @return epoCompranet
	 */
	public String getEposCompranet(){

		log.info("getEposCompranet(E)");

		String epoCompranet = "";
		String fechaInicial = "";
		String fechaFinal   = "";

		StringBuffer       query  = new StringBuffer();
		StringBuffer       result = new StringBuffer();
		AccesoDB           con    = new AccesoDB();
		PreparedStatement  ps     = null;
		ResultSet          rs     = null;

		fechaInicial = "01/" + mesIni + "/" + anioIni;
		if( mesFin.equals("2")){
			fechaFinal =  "28/" + mesFin + "/" + anioFin;
		}else{
			fechaFinal =  "30/" + mesFin + "/" + anioFin;
		}

		try{
			con.conexionDB();

			query = new StringBuffer();
			query.append("SELECT IC_EPO_COMPRANET FROM comcat_epo_compranet ");
			query.append("WHERE IG_NUMERO_EPO IN ("+sNoEposSelec+")");
			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				result.append(rs.getString("IC_EPO_COMPRANET")==null?"":rs.getString("IC_EPO_COMPRANET")+",");
			}
			rs.close();
			ps.close();

			epoCompranet = result.toString();
			if(!epoCompranet.equals("")){
				int tamanio  = epoCompranet.length()-1;
				epoCompranet = epoCompranet.substring(0,tamanio);
			}

			log.debug("query: " + query.toString());
			log.debug("epoCompranet: " + epoCompranet);

		} catch(Exception e){
			log.error("getEposCompranet(Exception)");
			e.printStackTrace();
		} finally{
			if (rs  != null) try { rs.close(); } catch(Exception e) {}
			if (ps  != null) try { ps.close(); } catch(Exception e) {}
			if (con != null && con.hayConexionAbierta()) con.cierraConexionDB();
		}

		log.info("getEposCompranet(S)");
		return epoCompranet;

	}

/*****************************************
 *          GETTERS AND SETTERS          *
 *****************************************/
	public List getConditions() {
		return conditions;
	}

	public String getMesIni() {
		return mesIni;
	}

	public void setMesIni(String mesIni) {
		this.mesIni = mesIni;
	}

	public String getAnioIni() {
		return anioIni;
	}

	public void setAnioIni(String anioIni) {
		this.anioIni = anioIni;
	}

	public void setMesFin(String mesFin) {
		this.mesFin = mesFin;
	}

	public String getAnioFin() {
		return anioFin;
	}

	public void setAnioFin(String anioFin) {
		this.anioFin = anioFin;
	}

	public String getFederales() {
		return federales;
	}

	public void setFederales(String federales) {
		this.federales = federales;
	}

	public String getOtros() {
		return otros;
	}

	public void setOtros(String otros) {
		this.otros = otros;
	}

	public String getSNoEposSelec() {
		return sNoEposSelec;
	}

	public void setSNoEposSelec(String sNoEposSelec) {
		this.sNoEposSelec = sNoEposSelec;
	}

	public String getIcEpoCompranet() {
		return icEpoCompranet;
	}

	public void setIcEpoCompranet(String icEpoCompranet) {
		this.icEpoCompranet = icEpoCompranet;
	}

}