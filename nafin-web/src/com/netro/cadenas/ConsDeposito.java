package com.netro.cadenas;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;
import com.netro.zip.ComunesZIP;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;


public class ConsDeposito implements IQueryGeneratorRegExtJS {
	//Variables
	StringBuffer qrySentencia = new StringBuffer();
	private List 		conditions;
		   

	private static final Log log = ServiceLocator.getInstance().getLog(ConsDeposito.class);
	private String ic_producto;
	private String ic_if;
	private String ic_epo;
	private String ic_moneda;
	private String fecha_de;
	private String fecha_a;
	private String tipo_piso;
	private String nombreIF;
	private String mensajeEncabezado;
	private String puesto;
	private String nombre_if_p;
	private String total;
	private String total2;
	private String nombre_if_e;
	private String titulo;
	private String importe;
	private String mensajeA;
	private String mensajeB;
	private String mensajeC;
	private String fechaPeriodo;
	private String mensajeD;
	private String fechaConvenio;
		
	
	public String getAggregateCalculationQuery() {
	return "";
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
	return "";
	}
	
	public String getDocumentQuery(){
	return "";
	}
	
	public String getDocumentQueryFile(){
		
		log.debug("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();		
		this.conditions = new ArrayList();
		
		if (ic_producto.equals("1")){			
			
			qrySentencia.append(	
				"Select /*+ ordered   index (b IN_COM_SOLICITUD_02_NUK)   use_nl(a, d, f, g, c, m)*/ "+
				"    a.ic_documento as IC_DOCUMENTO , "+
				"		g.cg_razon_social as NOMBRE_EMISOR , " + 	
				"		f.cg_razon_social as EMPRESA_ACREDITADA , " + 
				"		c.cd_descripcion as DESCRIPCION_DOCTO , " +
				"		b.cg_lugar_firma as LUGAR_SUSCRIPCION , " +
				"		to_char(d.df_fecha_docto,'dd/mm/yyyy') AS FECHA_SUSCRIPCION , " +
				" 		to_char(b.df_operacion,'dd/mm/yyyy') as FECHA_DESCUENTO,  "+
				"		d.fn_monto as IMPORTE_DOCTO , " +
				"		a.in_importe_recibir as IMPORTE_DESC , " +
				"		b.cg_perio_pago_cap as PERIODICIDAD , " +
				"		to_char(d.df_fecha_venc,'dd/mm/yyyy') AS  FECHA_VTO_DESC , " +
				"		a.in_tasa_aceptada as TASA_DESC , " +		
				"		b.ig_numero_prestamo  as NUM_PRESTAMO,  " +
				"		a.fn_remanente AS CAPTURA_REMANENTE,   "+		
				"     ''  as FECHA_VTO_CREDITO, "+
				"     ''  as TASA_CREDITO, "+
				"     '' as PERIODICIDAD_CAPITAL,  "+
				"     '' as PERIODICIDAD_INTERES,  "+
				"     '' as FECHA_VENC,  "+
				"     '' as TASA_INTERES,  "+
				"     '' as IMPORTE_FINANCIAMIENTO,  " +
				"     a.fn_remanente as CAPTURA_REMANENTE_2 "+
				"  From com_solicitud b, " +
				"		com_docto_seleccionado a, " +
				"		comcat_clase_docto c, " +
				"		com_documento d, " +
				"		comcat_pyme f, " +
				"		comcat_epo g, " +
				"		comcat_moneda m "+
				" Where a.ic_documento = b.ic_documento " +
				"	And m.ic_moneda = d.ic_moneda "+				
				"	And ic_estatus_solic in (3,5,6) " +
				"	And a.ic_documento = d.ic_documento " +
				"	And b.ic_clase_docto = c.ic_clase_docto " +
				"	And d.ic_pyme = f.ic_pyme " +
				"	And d.ic_epo=g.ic_epo ");				
				
				if(!"".equals(ic_if)) {
					qrySentencia.append( "	And a.ic_if = ? ");
					conditions.add(ic_if);
				}
				
				if(!"".equals(ic_moneda)) {
					qrySentencia.append( "	And d.ic_moneda = ? ");
					conditions.add(ic_moneda);
				}
				
				if(!"".equals(fecha_de)  &&  !"".equals(fecha_a) ) {
					qrySentencia.append(" And b.df_operacion >= trunc(to_date(?,'dd/mm/yyyy')) and b.df_operacion < trunc(to_date(?,'dd/mm/yyyy')) + 1 " );
					conditions.add(fecha_de);
					conditions.add(fecha_a);
				}

				if(!"".equals(ic_epo)) {
					qrySentencia.append(" and d.ic_epo = ? ");
					conditions.add(ic_epo);
				}
			
		}else if(ic_producto.equals("2")){
		
			qrySentencia.append(
				"Select f.cg_razon_social as NOMBRE_EMISOR ," +
				"		g.cg_razon_social as EMPRESA_ACREDITADA , " +
				"		c.cd_descripcion as DESCRIPCION_DOCTO , " +
				"		b.cg_lugar_firma as LUGAR_SUSCRIPCION , " +
				"		to_char(d.df_fecha_pedido,'dd/mm/yyyy') as FECHA_SUSCRIPCION , " +
				"		d.fn_monto as IMPORTE_DOCTO , " +
				"		to_char(a.df_vencimiento,'dd/mm/yyyy') as FECHA_VTO_CREDITO , " +
				"		a.fn_credito_recibir as TASA_CREDITO , " +
				"		b.cg_perio_pago_cap as PERIODICIDAD , " +
				"		a.fn_tasa_aceptada as TASA_DESC , " +
				"		b.ig_numero_prestamo as NUM_PRESTAMO ,  " + 
				"    ''  as CAPTURA_REMANENTE,  "+
				"    '' as IMPORTE_DESC,  " +
				"    '' as FECHA_DESCUENTO , "+
				" 	  '' as FECHA_VTO_DESC,  "+
				"     '' as PERIODICIDAD_CAPITAL,  "+
				"     '' as PERIODICIDAD_INTERES,  "+
				"     '' as FECHA_VENC,  "+
				"     '' as TASA_INTERES,  "+
				"     '' as IMPORTE_FINANCIAMIENTO,  " +
				"     '' as CAPTURA_REMANENTE_2  "+				
				"  From com_pedido_seleccionado a, " +
				"		com_anticipo b, " +
				"		comcat_clase_docto c, " +
				"		com_pedido d, " +
				"		comrel_pyme_epo e, " +
				"		comcat_pyme f, " +
				"		comcat_epo g, " +
				"		com_linea_credito lc, " +
				"		comcat_moneda m "+
				" Where lc.ic_linea_credito = a.ic_linea_credito " +
				"	And a.ic_pedido = b.ic_pedido " +
				"	And d.ic_moneda = m.ic_moneda "+				
				"	And ic_estatus_antic in (3,5,6) " +
				"	And a.ic_pedido = d.ic_pedido " +
				"	And b.ic_clase_docto = c.ic_clase_docto " +
				"	And d.ic_epo = e.ic_epo " +
				"	And d.ic_pyme = e.ic_pyme " +
				"	And e.ic_pyme = f.ic_pyme " +				
				"	And e.ic_epo = g.ic_epo ");
				
				if(!"".equals(ic_if)) {
					qrySentencia.append(" And lc.ic_if = ? " );
					conditions.add(ic_if);
				}
				if(!"".equals(fecha_de)  &&  !"".equals(fecha_a) ) {
					qrySentencia.append("	And b.df_operacion >= trunc(to_date(?,'dd/mm/yyyy')) and b.df_operacion < trunc(to_date(?,'dd/mm/yyyy')) + 1 " );
					conditions.add(fecha_de);
					conditions.add(fecha_a);
				}
				if(!"".equals(ic_moneda)) {
					qrySentencia.append("And d.ic_moneda = ?");
					conditions.add(ic_moneda);					
				}
				if(!"".equals(ic_epo)) {
					qrySentencia.append(" and d.ic_epo = ? ");
					conditions.add(ic_epo);
				}
	
	
		}else if(ic_producto.equals("4")){
			
			qrySentencia.append(	
				" select e.cg_razon_social as NOMBRE_EMISOR"   +
				"  ,p.cg_razon_social as EMPRESA_ACREDITADA"   +
				"  ,cd.cd_descripcion as DESCRIPCION_DOCTO"   +
				"  ,'Mexico, D.F.' as LUGAR_SUSCRIPCION"   +
				"  ,to_char(s.df_operacion,'dd/mm/yyyy') as FECHA_SUSCRIPCION"   +
				"  ,d.fn_monto as IMPORTE_DOCTO"   +
				"  ,ds.fn_importe_recibir as IMPORTE_FINANCIAMIENTO"   +
				"  ,'Al vencimiento' as PERIODICIDAD_CAPITAL"   +
				"  ,tci.cd_descripcion as PERIODICIDAD_INTERES"   +
				"  ,to_char(d.df_fecha_venc,'dd/mm/yyyy') as FECHA_VENC"   +
				"  ,decode(ds.cg_rel_mat,'+',ds.fn_valor_tasa + ds.fn_puntos"   +
				" 			,'-',ds.fn_valor_tasa - ds.fn_puntos"   +
				" 			,'*',ds.fn_valor_tasa * ds.fn_puntos"   +
				" 			,'/',ds.fn_valor_tasa / ds.fn_puntos"   +
				" 	) as TASA_INTERES "   +
				"  ,s.ig_numero_prestamo AS NUM_PRESTAMO,  "   +
				"  '' as CAPTURA_REMANENTE,  " +
				"  '' as FECHA_DESCUENTO,  "+
				"  '' as IMPORTE_DESC , "+
				"  '' as PERIODICIDAD,  "+
				"  '' as FECHA_VTO_DESC,  "+
				"  '' as TASA_DESC,  " +
				" '' as FECHA_VTO_CREDITO,  "+
				" '' as TASA_CREDITO,  "+
				"  '' as CAPTURA_REMANENTE_2 "+
				"    From  dis_documento d"   +
				"    	,dis_docto_seleccionado ds"   +
				"  	,dis_solicitud s"   +
				" 		,comcat_epo e"   +
				" 		,comcat_pyme p"   +
				" 		,comcat_clase_docto cd"   +
				" 		,com_linea_credito lc"   +
				" 		,comcat_tipo_cobro_interes tci"   +
				"		,comcat_moneda m "+
				"   Where d.ic_documento = ds.ic_documento"   +
				"  	And ds.ic_documento = s.ic_documento"   +
				"  	And s.ic_estatus_solic in (3,5,11)"   +
				" 	And e.ic_epo = d.ic_epo"   +
				" 	And p.ic_pyme = d.ic_pyme"   +
				" 	And cd.ic_clase_docto = s.ic_clase_docto"   +
				" 	AND d.ic_linea_credito = lc.ic_linea_credito"   +
				" 	and lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"	And d.ic_moneda = m.ic_moneda ");
				
				if(!"".equals(ic_epo))  {
					qrySentencia.append(" and d.ic_epo = ? ");
					conditions.add(ic_epo);					
				}
				if(!"".equals(ic_if))  {
					qrySentencia.append(" and lc.ic_if = ?");
					conditions.add(ic_if);		
				}
				if(!"".equals(fecha_de)  &&  !"".equals(fecha_a) ) {
					qrySentencia.append("	And s.df_operacion >= trunc(to_date(?,'dd/mm/yyyy')) and s.df_operacion < trunc(to_date(?,'dd/mm/yyyy')) + 1" );
					conditions.add(fecha_de);	
					conditions.add(fecha_a);	
				}				
				if(!"".equals(ic_moneda))  {
					qrySentencia.append(" And d.ic_moneda = ?");
					conditions.add(ic_moneda);		
				}
				
				
				qrySentencia.append(" 	UNION ALL	"   +
				"   select e.cg_razon_social as NOMBRE_EMISOR"   +
				"  ,p.cg_razon_social as EMPRESA_ACREDITADA"   +
				"  ,cd.cd_descripcion as DESCRIPCION_DOCTO"   +
				"  ,'Mexico, D.F.' as LUGAR_SUSCRIPCION"   +
				"  ,to_char(s.df_operacion,'dd/mm/yyyy') as FECHA_SUSCRIPCION"   +
				"  ,d.fn_monto as IMPORTE_DOCTO"   +
				"  ,ds.fn_importe_recibir as IMPORTE_FINANCIAMIENTO"   +
				"  ,'Al vencimiento' as PERIODICIDAD_CAPITAL"   +
				"  ,tci.cd_descripcion as PERIODICIDAD_INTERES"   +
				"  ,to_char(d.df_fecha_venc,'dd/mm/yyyy') as FECHA_VENC"   +
				"  ,decode(ds.cg_rel_mat,'+',ds.fn_valor_tasa + ds.fn_puntos"   +
				" 			,'-',ds.fn_valor_tasa - ds.fn_puntos"   +
				" 			,'*',ds.fn_valor_tasa * ds.fn_puntos"   +
				" 			,'/',ds.fn_valor_tasa / ds.fn_puntos"   +
				" 	) as TASA_INTERES"   +
				"  ,s.ig_numero_prestamo AS NUM_PRESTAMO ,  "   +
				"  '' as CAPTURA_REMANENTE,  " +
				"  '' as FECHA_DESCUENTO,  "+	
				"  '' as IMPORTE_DESC , "+
				"  '' as PERIODICIDAD ,  "+
				"  '' as FECHA_VTO_DESC,  "+
				"  '' as TASA_DESC , " +
				" '' as FECHA_VTO_CREDITO,  "+
				" '' as TASA_CREDITO,  "+
				" '' as CAPTURA_REMANENTE_2 "+
				"    From  dis_documento d"   +
				"    	,dis_docto_seleccionado ds"   +
				"  		,dis_solicitud s"   +
				" 		,comcat_epo e"   +
				" 		,comcat_pyme p"   +
				" 		,comcat_clase_docto cd"   +
				" 		,dis_linea_credito_dm lc"   +
				" 		,comcat_tipo_cobro_interes tci"   +
				"		,comcat_moneda m "+
				"   Where d.ic_documento = ds.ic_documento"   +
				"  	And ds.ic_documento = s.ic_documento"   +
				"  	And s.ic_estatus_solic in (3,5,11)"   +
				" 	And e.ic_epo = d.ic_epo"   +
				" 	And p.ic_pyme = d.ic_pyme"   +
				" 	And cd.ic_clase_docto = s.ic_clase_docto"   +
				" 	AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm"   +
				" 	and lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +			
				"	And d.ic_moneda = m.ic_moneda ");				
				
				if(!"".equals(ic_epo))  {
					qrySentencia.append(" and d.ic_epo = ? ");
					conditions.add(ic_epo);					
				}
				if(!"".equals(ic_if))  {
					qrySentencia.append(" and lc.ic_if = ?");
					conditions.add(ic_if);		
				}
				if(!"".equals(fecha_de)  &&  !"".equals(fecha_a) ) {
					qrySentencia.append(" and s.df_operacion >= trunc(to_date(?,'dd/mm/yyyy')) and s.df_operacion < trunc(to_date(?,'dd/mm/yyyy')) + 1 "  );
					conditions.add(fecha_de);	
					conditions.add(fecha_a);	
				}				
				if(!"".equals(ic_moneda))  {
					qrySentencia.append(" and d.ic_moneda = ? ");
					conditions.add(ic_moneda);		
				}
			}		
		
		log.debug("strQuery.toString(): "+qrySentencia.toString());
		log.debug("conditions.toString(): "+conditions);
		log.debug("getDocumentQueryFile(S)");
		
		return qrySentencia.toString();
	
	}
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		double total1=0,  totalRemanente = 0;
		ComunesPDF pdfDoc = new ComunesPDF();
		HttpSession session = request.getSession();	
		
		try {
		
			if(tipo.equals("CSV"))  {
		
				contenidoArchivo.append(mensajeEncabezado+"\n");
				
				if(ic_producto.equals("1"))  {
				
					contenidoArchivo.append("\n");
					contenidoArchivo.append(
								" \n Nombre del emisor,Nombre de la empresa acreditada,Descripcion del documento," +
								"Lugar suscripcion,Fecha suscripcion," +
								"Importe documento,Importe descuento,Periodicidad pago Capital e Interes," +
								"Fecha Vto. Descuento,Tasa Descuento,Numero de Prestamo,Tipo de Documento");
						if("1".equals(tipo_piso)){
							contenidoArchivo.append(",Captura de Remanente");
						}
						contenidoArchivo.append("\n");		
				}else if(ic_producto.equals("2"))  {
					
					contenidoArchivo.append("\n");
					contenidoArchivo.append(
							"\n Nombre del emisor,Nombre de la empresa acreditada,Descripcion del documento," +
							"Lugar suscripcion,Fecha suscripcion,Importe documento,Fecha Vto. del Credito, " +
							"Importe del credito, Periodicidad pago Capital e Interes," +
							"Tasa Descuento,Numero de Prestamo,Tipo de Documento \n");
				
				}else if(ic_producto.equals("4"))  {
				  
				  contenidoArchivo.append("\n");
					contenidoArchivo.append(
					"\n Nombre del Emisor,Nombre de la Empresa Acreditada,Descripci�n del Documento,Lugar Suscripci�n,"+
					"Fecha Suscripci�n,Importe del Documento,Importe del Financiamiento,Periodicidad de Pago de Capital,"+
					"Periodicidad Pago de Inter�s,Fecha Vencimiento,Tasa de Inter�s,N�mero de Pr�stamo Nafin,Tipo de Documento \n");	
				}
				
			}else if(tipo.equals("PDF"))  {
				
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.addText("Formato de constancia de dep�sito de "+titulo+"\nCadenas Productivas\n","formasrepB",ComunesPDF.CENTER);
				String mensajeConstancia = 
					"De conformidad con el convenio que "+nombre_if_e+" y en la fecha que posteriormente se se�ala, celebr� con Nacional Financiera,"+
					"S.N.C., bajo protesta de decir verdad, se hace constar que para todos los efectos legales, nos asumimos como depositarios"+
					" de los t�tulo (s) de cr�dito y/o documentos electr�nico (s) sin derecho a honorarios y asumiendo la responsabilidad civil"+
					" y penal correspondiente al car�cter de depositario judicial, el (los) t�tulo (s) de cr�dito y/o documento (s) electr�nico (s)"+
					" en que se consignan los derechos de cr�dito derivados de las operaciones de "+mensajeA+", celebradas con los clientes"+
					" que se describen en el cuadro siguiente, en donde se especifica la fecha de presentaci�n, importe total y nombre de la empresa"+
					" acreditada.\n\n";
				mensajeConstancia +=
					"La informaci�n de el (los) "+total+" documento (s) en "+(ic_moneda.equals("1")?"Moneda Nacional":"D�lares")+" por un importe"+
					" de $"+Comunes.formatoDecimal(importe,2,true)+" citado (s), est�n registrados en el sistema denominado Nafin Electr�nico a"+
					" disposici�n de Nacional Financiera, S.N.C., cuando �sta los requiera.\n\n";
				mensajeConstancia +=
					"Trat�ndose de el (los) t�tulo (s) de cr�dito electr�nico (s), �stos se encuentran debidamente transmitido (s) en propiedad"+
					" y con nuestra responsabilidad a favor de Nacional Financiera, S.N.C. En el caso de el (los) documento (s) electr�nico (s),"+
					" en este acto cedemos a dicha Instituci�n de Banca de Desarrollo, los derechos de cr�dito que se derivan de los mismos.\n\n";
				mensajeConstancia +=
					"En t�rminos del art�culo 2044 del C�digo Civil para el Distrito Federal, la cesi�n a que se refiere el p�rrafo anterior, la"+
					" realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, por el tiempo en que permanezcan vigentes"+
					" los adeudos y hasta su liquidaci�n total.\n\n";
				mensajeConstancia +=
					"As� mismo y en t�rminos de lo estipulado  en el Convenio antes mencionado, nos obligamos a efectuar el cobro de (los)"+
					" t�tulo (s) de cr�dito electr�nico y/o ejercer los derechos de cobro consignados en el (los) documentos objeto de"+
					" "+mensajeB+", as� como ejecutar todos los actos que sean necesarios para que el (los) documento (s) se�alados conserven"+
					" su valor y dem�s derechos que les correspondan.";
				pdfDoc.addText(mensajeConstancia,"formasrep",ComunesPDF.CENTER);
				
				pdfDoc.setTable(2, 50);
				pdfDoc.setCell("Nombre del Intermediario Financiero","formasB", ComunesPDF.CENTER, 1, 1, 0);
				pdfDoc.setCell(nombre_if_e,"formas", ComunesPDF.CENTER, 1, 1, 0);
				pdfDoc.setCell("Fecha del Convenio de "+mensajeC,"formasB", ComunesPDF.CENTER, 1, 1, 0);
				pdfDoc.setCell(fechaConvenio,"formas", ComunesPDF.CENTER, 1, 1, 0);
				pdfDoc.setCell("Fecha del "+mensajeD,"formasB", ComunesPDF.CENTER, 1, 1, 0);
				pdfDoc.setCell("M�xico D.F. "+fechaPeriodo,"formas", ComunesPDF.CENTER, 1, 1, 0);
				pdfDoc.addTable();
				
				int numCols=11;
				if("1".equals(ic_producto)) {
					numCols+=1;
				}
				if("1".equals(ic_producto)&&"1".equals(tipo_piso))
					numCols+=1;
				else if(ic_producto.equals("4"))
					numCols++;
				pdfDoc.setTable(numCols);
				
				if (ic_producto.equals("1")){
					pdfDoc.setCell("Nombre del Emisor","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Nombre de la Empresa Acreditada","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Descripci�n del Documento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Lugar Suscripci�n","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Fecha Suscripci�n","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Fecha de Descuento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Importe Documento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Importe Descuento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Periodicidad pago Capital e Inter�s","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Fecha Vto. Descuento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Tasa Descuento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("N�mero de Pr�stamo","celda01rep",ComunesPDF.CENTER); 
					if("1".equals(tipo_piso)){
						pdfDoc.setCell("Captura de Remanente","celda01rep",ComunesPDF.CENTER); 
					}
				}else if(ic_producto.equals("2")){
					pdfDoc.setCell("Nombre del Emisor","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Nombre de la Empresa Acreditada","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Descripci�n del Documento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Lugar Suscripci�n","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Fecha Suscripci�n","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Importe Documento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Fecha Vto. Cr�dito","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Tasa Cr�dito","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Periodicidad Pago Capital e Inter�s","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Tasa de descuento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("N�mero de Pr�stamo","celda01rep",ComunesPDF.CENTER); 
				}else if(ic_producto.equals("4")){				
					pdfDoc.setCell("Nombre del Emisor","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Nombre de la Empresa Acreditada","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Descripci�n del Documento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Lugar Suscripci�n","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Fecha Suscripci�n","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Importe del Documento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Importe del Financiamiento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Periodicidad de Pago de Capital","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Periodicidad Pago de Inter�s","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Fecha Vencimiento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Tasa de Inter�s","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("N�mero de Pr�stamo Nafin","celda01rep",ComunesPDF.CENTER); 
				} else if (ic_producto.equals("5")){
					pdfDoc.setCell("Nombre del Emisor","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Nombre de la Empresa Acreditada","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Descripci�n del Documento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Lugar Suscripci�n","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Fecha Suscripci�n","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Importe Documento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Importe Descuento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Periodicidad pago Capital e Inter�s","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Fecha Vto. Descuento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("Tasa Descuento","celda01rep",ComunesPDF.CENTER); 
					pdfDoc.setCell("N�mero de Pr�stamo","celda01",ComunesPDF.CENTER); 
				}
			}			
				
				
			while(rs.next()){			
				String nombreEmisior = (rs.getString("NOMBRE_EMISOR") == null) ? "" : rs.getString("NOMBRE_EMISOR");
				String empreAcreditada = (rs.getString("EMPRESA_ACREDITADA") == null) ? "" : rs.getString("EMPRESA_ACREDITADA");
				String descMonto = (rs.getString("DESCRIPCION_DOCTO") == null) ? "" : rs.getString("DESCRIPCION_DOCTO");
				String lugarSuscrip = (rs.getString("LUGAR_SUSCRIPCION") == null) ? "" : rs.getString("LUGAR_SUSCRIPCION");
				String fechaSuscrip = (rs.getString("FECHA_SUSCRIPCION") == null) ? "" : rs.getString("FECHA_SUSCRIPCION");
				String fechadesc = (rs.getString("FECHA_DESCUENTO") == null) ? "" : rs.getString("FECHA_DESCUENTO");
				String impDocto = (rs.getString("IMPORTE_DOCTO") == null) ? "" : rs.getString("IMPORTE_DOCTO");
				String importeDesc = (rs.getString("IMPORTE_DESC") == null) ? "" : rs.getString("IMPORTE_DESC");
				String periodicidad = (rs.getString("PERIODICIDAD") == null) ? "" : rs.getString("PERIODICIDAD");
				String fecVtoDesc = (rs.getString("FECHA_VTO_DESC") == null) ? "" : rs.getString("FECHA_VTO_DESC");
				String tasaDesc = (rs.getString("TASA_DESC") == null) ? "" : rs.getString("TASA_DESC");
				String numPrestamo = (rs.getString("NUM_PRESTAMO") == null) ? "" : rs.getString("NUM_PRESTAMO");
				String capPermanente = (rs.getString("CAPTURA_REMANENTE") == null) ? "" : rs.getString("CAPTURA_REMANENTE");				
				
				String fecVtoCred = (rs.getString("FECHA_VTO_CREDITO") == null) ? "" : rs.getString("FECHA_VTO_CREDITO");
				String tasaCredito = (rs.getString("TASA_CREDITO") == null) ? "" : rs.getString("TASA_CREDITO");
				String perioCapital = (rs.getString("PERIODICIDAD_CAPITAL") == null) ? "" : rs.getString("PERIODICIDAD_CAPITAL");
				String perioInteres = (rs.getString("PERIODICIDAD_INTERES") == null) ? "" : rs.getString("PERIODICIDAD_INTERES");
				String fecha_venc = (rs.getString("FECHA_VENC") == null) ? "" : rs.getString("FECHA_VENC");
				String tasaInte = (rs.getString("TASA_INTERES") == null) ? "" : rs.getString("TASA_INTERES");
				String impFinan = (rs.getString("IMPORTE_FINANCIAMIENTO") == null) ? "" : rs.getString("IMPORTE_FINANCIAMIENTO");
				
				
				total1=total1+rs.getFloat("IMPORTE_DOCTO");
				if(!"".equals(capPermanente)){
					totalRemanente += Double.parseDouble(capPermanente);
				}
									
				
				if(tipo.equals("CSV"))  {
					if(ic_producto.equals("1"))  {
					contenidoArchivo.append(
						nombreEmisior.replaceAll(",","")+","+
						empreAcreditada.replaceAll(",","")+","+
						descMonto.replaceAll(",","")+","+
						lugarSuscrip.replaceAll(",","")+","+
						fechaSuscrip.replaceAll(",","")+","+
						impDocto.replaceAll(",","")+","+
						importeDesc.replaceAll(",","")+","+
						periodicidad.replaceAll(",","")+","+
						fecVtoDesc.replaceAll(",","")+","+
						tasaDesc.replaceAll(",","")+","+
						numPrestamo.replaceAll(",","")+","+
						"Constancia , ");
						if("1".equals(tipo_piso))  {
							contenidoArchivo.append(capPermanente.replaceAll(",","")+",");
						}
						contenidoArchivo.append("\n");
						
					}else if(ic_producto.equals("2"))  {
					
						contenidoArchivo.append(
						nombreEmisior.replaceAll(",","")+","+
						empreAcreditada.replaceAll(",","")+","+
						descMonto.replaceAll(",","")+","+
						lugarSuscrip.replaceAll(",","")+","+
						fechaSuscrip.replaceAll(",","")+","+
						impDocto.replaceAll(",","")+","+
						fecVtoCred.replaceAll(",","")+","+
						tasaCredito.replaceAll(",","")+","+
						periodicidad.replaceAll(",","")+","+						
						tasaDesc.replaceAll(",","")+","+
						numPrestamo.replaceAll(",","")+","+
						"Constancia , ");
						contenidoArchivo.append("\n");
						
					}else if(ic_producto.equals("4"))  {
					
						contenidoArchivo.append(
						nombreEmisior.replaceAll(",","")+","+
						empreAcreditada.replaceAll(",","")+","+
						descMonto.replaceAll(",","")+","+
						lugarSuscrip.replaceAll(",","")+","+
						fechaSuscrip.replaceAll(",","")+","+
						impDocto.replaceAll(",","")+","+
						impFinan.replaceAll(",","")+","+
						perioCapital.replaceAll(",","")+","+
						perioInteres.replaceAll(",","")+","+						
						fecha_venc.replaceAll(",","")+","+
						tasaInte.replaceAll(",","")+","+
						numPrestamo.replaceAll(",","")+","+
						"Constancia , ");
						contenidoArchivo.append("\n");
					}
				} else  if(tipo.equals("PDF") ) {
					if(ic_producto.equals("1"))  {
						pdfDoc.setCell(nombreEmisior,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(empreAcreditada,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(descMonto,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lugarSuscrip,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaSuscrip,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(fechadesc,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(impDocto,2,true),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(importeDesc,2,true),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell(periodicidad,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(fecVtoDesc,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(tasaDesc+"%","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(numPrestamo,"formasrep",ComunesPDF.CENTER);
						if("1".equals(tipo_piso))  {
							pdfDoc.setCell(capPermanente,"formasrep",ComunesPDF.CENTER);
						}
						
					}else if(ic_producto.equals("2"))  { 
					
						pdfDoc.setCell(nombreEmisior,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(empreAcreditada,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(descMonto,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lugarSuscrip,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaSuscrip,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(impDocto,2,true),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell(fecVtoCred,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(tasaCredito,"formasrep",ComunesPDF.RIGHT);						
						pdfDoc.setCell(periodicidad,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(tasaDesc+"%","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(numPrestamo,"formasrep",ComunesPDF.CENTER);					
					
					}else if(ic_producto.equals("4"))  { 
					
						pdfDoc.setCell(nombreEmisior,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(empreAcreditada,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(descMonto,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lugarSuscrip,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaSuscrip,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(impDocto,2,true),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(impFinan,2,true),"formasrep",ComunesPDF.RIGHT);
					
						pdfDoc.setCell(perioCapital,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(perioInteres,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_venc,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(tasaInte+"%","formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(numPrestamo,"formasrep",ComunesPDF.CENTER);
						
					
					}
					
				}
			}//while(rs.next()){
			
			if(tipo.equals("CSV") ) {
				
				contenidoArchivo.append(" Total de Documentos:,"+total+ ", , , Total,"+Comunes.formatoDecimal(total1,2,false)+","+Comunes.formatoDecimal(total2,2,false)+"\n");
				contenidoArchivo.append("Facultado por "+nombre_if_e+"\n");
				contenidoArchivo.append(puesto+" "+nombre_if_p+"\n");
	 
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			
			}else  if(tipo.equals("PDF") ) {
				int numColsTotal = 3;
				pdfDoc.setCell("Total de Documentos:","celda01rep",ComunesPDF.RIGHT);
					
				if(ic_producto.equals("1"))  {
					numColsTotal++;
					pdfDoc.setCell(new Integer(total).toString(),"formasB",ComunesPDF.LEFT,numColsTotal);
					pdfDoc.setCell("Total:","celda01rep",ComunesPDF.RIGHT);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(total1,2,true),"formasBrep",ComunesPDF.RIGHT);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(total2,2,true),"formasBrep",ComunesPDF.RIGHT);
					
					if("1".equals(tipo_piso)) {
						pdfDoc.setCell("Total Remanente:","formasB",ComunesPDF.RIGHT,4);
						pdfDoc.setCell("$ "+Comunes.formatoMoneda2(new Double(totalRemanente).toString(),false),"formasB",ComunesPDF.RIGHT);						
					}else  {
						pdfDoc.setCell("  ","formasB",ComunesPDF.RIGHT,4);
					}
				}else  if(ic_producto.equals("2"))  {
					pdfDoc.setCell(new Integer(total).toString(),"formasB",ComunesPDF.LEFT,numColsTotal);
					pdfDoc.setCell("Total:","celda01rep",ComunesPDF.RIGHT);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(total1,2,true),"formasBrep",ComunesPDF.RIGHT);
					pdfDoc.setCell("","formasrep",ComunesPDF.RIGHT);					
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(total2,2,true),"formasBrep",ComunesPDF.RIGHT);
					pdfDoc.setCell("","formasrep",ComunesPDF.RIGHT,3);
					
				} else  if(ic_producto.equals("4"))  {
					pdfDoc.setCell(new Integer(total).toString(),"formasB",ComunesPDF.LEFT,numColsTotal);
					pdfDoc.setCell("Total:","celda01rep",ComunesPDF.RIGHT);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(total1,2,true),"formasBrep",ComunesPDF.RIGHT);								
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(total2,2,true),"formasBrep",ComunesPDF.RIGHT);
					pdfDoc.setCell("","formasrep",ComunesPDF.RIGHT,5);
				}
				
				pdfDoc.addTable();
				
				pdfDoc.addText("Facultado por "+nombre_if_e+". "+puesto+" "+nombre_if_p,"formasB",ComunesPDF.CENTER);
				
				
				pdfDoc.endDocument();	
			}
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		return nombreArchivo;
	}
	
	/**
	 * metodo para formr el combo de iF 
	 * @return 
	 */

	public List  getCatalogoIF( ){
		log.info("getCatalogoIF (E) ");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
			
			String SQL =" SELECT ic_if, cg_razon_social,  ig_tipo_piso" +
					"   FROM comcat_if"   +
					"  WHERE cs_habilitado = 'S' order by 2 "  ;
	
			rs = con.queryDB(SQL);			
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString(1) );
				datos.put("descripcion", rs.getString(2));
				registros.add(datos);
			}
			rs.close();
			con.cierraStatement();
		
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getCatalogoIF (S) ");
		return registros;
	}
	
	/**
	 * metodo para obtener el tipo_piso del IF
	 * @return 
	 * @param ic_if
	 */
	
	public String  getTipoPiso( String ic_if ){
	log.info("getTipoPiso (E) ");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;	
		String tipoPiso =  "";
		try{
			con.conexionDB();
			
			String SQL =" SELECT  ig_tipo_piso" +
					" FROM comcat_if"   +
					" WHERE ic_if = "+ic_if+
					" and  cs_habilitado = 'S'" +
					" order by 1"  ;
	
			rs = con.queryDB(SQL);			
			while(rs.next()) {			
				tipoPiso =  rs.getString(1);			
			}
			rs.close();
			con.cierraStatement();
		
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getTipoPiso (S) ");
		
		return tipoPiso;
	}


	/**
	 *  metodo para obtener toda la informacion para el encabezado y pie de la pantalla y archivos 
	 * @return 
	 * @param fechaPeriodo
	 * @param fechaConvenio
	 * @param fecha_a
	 * @param fecha_de
	 * @param ic_moneda
	 * @param ic_epo
	 * @param ic_if
	 * @param ic_producto
	 */
	public List  getEncabezado(String ic_producto,  String ic_if,  String ic_epo, String ic_moneda, 
											String fecha_de, String fecha_a, String fechaConvenio, String fechaPeriodo ){
											
		log.info("getEncabezado (E) ");
													
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;	
		PreparedStatement ps = null;
		
		StringBuffer SQLproductos = new StringBuffer();		
		ArrayList condiciones = new ArrayList();
		int total=0;
		double importe=0,total1=0,total2=0;
		boolean bandera=false;
		StringBuffer mensajeEncabezado = new StringBuffer();
		String strTitulo = "", strMensajeA = "", strMensajeB = "", strMensajeC = "", 
				strMensajeCA = "", strMensajeD = "", strMensajeDA = "", nombreIF ="";

		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		
		try{
			con.conexionDB();
			
			SQLproductos = new StringBuffer();		
			String SQL =" SELECT  cg_razon_social" +
					" FROM comcat_if"   +
					" WHERE ic_if = "+ic_if+
					" and  cs_habilitado = 'S'" +
					" order by 1"  ;
	
			rs = con.queryDB(SQL);			
			while(rs.next()) {			
				nombreIF =  rs.getString(1);			
			}
			rs.close();
			
			SQLproductos = new StringBuffer();		
			if (ic_producto.equals("1")){
			
				SQLproductos.append("Select /*+ ordered   index (b IN_COM_SOLICITUD_02_NUK)   use_nl(a, d) */" +
					"	count(1),  SUM(a.in_importe_recibir) " +
					"  From com_solicitud b, com_docto_seleccionado a, " +
					"		com_documento d " +
					" Where a.ic_if = ? "+
					"	And a.ic_documento = b.ic_documento " +
					"	And a.ic_documento = d.ic_documento "+
					"	And d.ic_moneda = ?"+
					"	And b.df_operacion >= trunc(to_date(?,'dd/mm/yyyy')) and b.df_operacion < trunc(to_date(?,'dd/mm/yyyy')) + 1 " +				
					"	And ic_estatus_solic in (3,5,6) ");
					condiciones.add(ic_if);
					condiciones.add(ic_moneda);
					condiciones.add(fecha_de);
					condiciones.add(fecha_a);
					
					if(!"".equals(ic_epo)) {
						SQLproductos.append( " and d.ic_epo = ? ");
						condiciones.add(ic_epo);
					}
					
			}else if(ic_producto.equals("2")){
			
				SQLproductos.append( "Select /*+ ordered   index (b IN_COM_ANTICIPO_01_NUK)   use_nl(a,p,lc) */ "+
					"	count(1),  SUM(a.fn_credito_recibir) " +
					"  From com_anticipo b, com_pedido_seleccionado a, " +
					"		com_pedido p, com_linea_credito lc" +
					" Where lc.ic_linea_credito = a.ic_linea_credito " +
					"	And lc.ic_if = ?" +
					"	And a.ic_pedido = b.ic_pedido " +
					"	And a.ic_pedido = p.ic_pedido "+
					"	And p.ic_moneda = ?"+
					"	And b.df_operacion >= trunc(to_date(?,'dd/mm/yyyy')) and b.df_operacion < trunc(to_date(?,'dd/mm/yyyy')) + 1 " +
					"	And ic_estatus_antic in (3,5,6) ");
					
					condiciones.add(ic_if);
					condiciones.add(ic_moneda);
					condiciones.add(fecha_de);
					condiciones.add(fecha_a);
					
					if(!"".equals(ic_epo)) {
						SQLproductos.append( " and p.ic_epo = ? ");
						condiciones.add(ic_epo);
					}
			}else if(ic_producto.equals("4")){	
			
				SQLproductos.append(" Select /*+ ordered   index (s in_dis_solicitud_01_nuk)   use_nl(ds,d) */"   +
					"     count(1), SUM(ds.fn_importe_recibir)"   +
					"   From dis_solicitud s,dis_docto_seleccionado ds , dis_documento d"   +
					"   Where d.ic_documento = ds.ic_documento"   +
					"  	And ds.ic_documento = s.ic_documento"   +
					"	And d.ic_moneda = ?"+
					"	And s.df_operacion >= trunc(to_date(?,'dd/mm/yyyy')) and s.df_operacion < trunc(to_date(?,'dd/mm/yyyy')) + 1" +
					"  	And s.ic_estatus_solic in (3,5,11) "   +				
					" 	AND (d.ic_linea_credito in(select ic_linea_credito from com_linea_credito where ic_if = ?)"   +
					" 	 OR d.ic_linea_credito_dm in(select ic_linea_credito_dm from dis_linea_credito_dm where ic_if = ?))" )  ;
										
					condiciones.add(ic_moneda);			
					condiciones.add(fecha_de);
					condiciones.add(fecha_a);
					condiciones.add(ic_if);
					condiciones.add(ic_if);
					if(!"".equals(ic_epo))  {
						SQLproductos.append(" and d.ic_epo = ? ");
						condiciones.add(ic_epo);
					}
			}
				
			ps = con.queryPrecompilado(SQLproductos.toString(), condiciones );
	
			rs = ps.executeQuery();
			if(rs.next()){
			  total=rs.getInt(1);
			  importe=rs.getDouble(2);
			  total2=importe;
				if(total>0)
					bandera=true;
				  else
					bandera=false;
			} 	else {
				bandera=false;
			}
			rs.close();
			ps.close();
		
		if(bandera){
			if (ic_producto.equals("1")){
				strTitulo = "documentos descontados para operaciones de factoraje electr�nico";
				strMensajeA = "factoraje financiero electr�nicas";
				strMensajeB = "descuento";
				strMensajeC = "Descuento Electr�nico";
				strMensajeCA = "Descuento Electr�nico";
				strMensajeD = "Descuento";
				strMensajeDA = strMensajeD;
			}else if(ic_producto.equals("2")){
				strTitulo = "cr�ditos operados en financiamiento a pedidos";
				strMensajeA = "financiamiento a pedidos";
				strMensajeB = "anticipo";
				strMensajeC = "Financiamiento a pedidos";
				strMensajeCA = strMensajeC;
				strMensajeD = "Cr�dito ";
				strMensajeDA = "Cr�dito ";
			}else if(ic_producto.equals("4")){
				strTitulo = "cr�ditos operados en financiamiento a distribuidores";
				strMensajeA = "financiamiento a distribuidores";
				strMensajeB = "credito";
				strMensajeC = "Financiamiento a distribuidores";
				strMensajeCA = strMensajeC;
				strMensajeD = "Cr�dito ";
				strMensajeDA = "Cr�dito ";
			}
		
			 mensajeEncabezado.append("De conformidad con el convenio que "+nombreIF+" y en la fecha que posteriormente se se�ala  celebr� con Nacional Financiera S.N.C. bajo protesta de decir verdad se hace constar que para todos los efectos legales nos asumimos como depositarios de los t�tulo (s) de cr�dito y/o documentos electr�nico (s) sin derecho a honorarios y asumiendo la responsabilidad civil y "+
			 " penal correspondiente al car�cter de depositario judicial el (los) t�tulo (s) de cr�dito y/o documento (s) electr�nico (s) en que se consignan los derechos de cr�dito derivados de las operaciones de " + strMensajeA + " celebradas con los clientes que se describen en el cuadro siguiente en donde se especifica la fecha de presentaci�n importe total y nombre de la empresa acreditada.\n"+
			 " La informaci�n de el (los)"+ total+" documento (s) en "+(ic_moneda.equals("1")?"Moneda Nacional":"D�lares")+" por un importe de $"+Comunes.formatoDecimal(importe,2,false) +"  citado (s) est�n registrados en el sistema denominado Nafin Electr�nico a disposici�n de Nacional Financiera S.N.C. cuando �sta los requiera.\n"+
			 "Trat�ndose de el (los) t�tulo (s) de cr�dito electr�nico (s) �stos se encuentran debidamente transmitido (s) en propiedad y con nuestra responsabilidad a favor de Nacional Financiera S.N.C. En el caso de el (los) documento (s) electr�nico (s) en este acto cedemos a dicha Instituci�n de Banca de Desarrollo los derechos de cr�dito que se derivan de los mismos.\n"+
			 "En t�rminos del art�culo 2044 del C�digo Civil para el Distrito Federal la cesi�n a que se refiere el p�rrafo anterior la realizamos con nuestra responsabilidad respecto de la solvencia de los emisores por el tiempo en que permanezcan vigentes los adeudos y hasta su liquidaci�n total.\n"+
			 "As� mismo y en t�rminos de lo estipulado  en el Convenio antes mencionado nos obligamos a efectuar el cobro de (los) t�tulo (s) de cr�dito electr�nico y/o ejercer los derechos de cobro consignados en el (los) documentos objeto de " + strMensajeB + " as� como ejecutar todos los actos que sean necesarios para que el (los) documento (s) se�alados conserven su valor y dem�s derechos que les correspondan.\n");
				
			 mensajeEncabezado.append(" \n Nombre del Intermediario Financiero  "+nombreIF+"   \n  ");
			 mensajeEncabezado.append("Fecha del Convenio de   "+strMensajeC+"   "+fechaConvenio+"  \n ");
			 mensajeEncabezado.append("Periodo de Consulta de   "+strMensajeD+"    M�xico D.F.  "+fechaPeriodo+" ");
			
		}
		
		String nombreIF_  ="", puesto ="";	
		SQLproductos = new StringBuffer();		
		SQLproductos.append( "Select cg_nombre||' '||cg_ap_paterno||' '||cg_ap_materno, " +
									" cg_puesto " + 
									"  From com_personal_facultado " +
									" Where ic_if = " + ic_if +
									"	And ic_producto_nafin = " + ic_producto);

		  rs= con.queryDB(SQLproductos.toString());
		  
		  if(rs.next()){
			 nombreIF_=rs.getString(1);
			 puesto=rs.getString(2);
		  }
		  rs.close();
  
		
		datos.put("TITULO", strTitulo );
		datos.put("MENSAJE_A", strMensajeA );
		datos.put("MENSAJE_B", strMensajeB );
		datos.put("MENSAJE_C", strMensajeC );
		datos.put("MENSAJE_CA", strMensajeCA );
		datos.put("MENSAJE_D", strMensajeD );
		datos.put("MENSAJE_DA", strMensajeDA );
		datos.put("ENCABEZADO", mensajeEncabezado.toString() );
		datos.put("TOTAL", String.valueOf(total) );
		datos.put("IMPORTE", Double.toString (importe));
		datos.put("TOTAL2", String.valueOf(total2) );
		datos.put("NOMBRE_IF_E", nombreIF );
		datos.put("NOMBRE_IF_P", nombreIF_ );
		datos.put("PUESTO", puesto );
		datos.put("FECHA_PERIODO", fechaPeriodo );
		datos.put("FECHA_CONVENIO", fechaConvenio );
		
		
		registros.add(datos);
		
					
		} catch (Exception e) {
			System.err.println("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.debug("getEncabezado (S) ");		
		return registros;
	}



//********************Inicia la parte de Concentrado del d�a  ***************************//

/**
 * Genera el archivo de constancias, para todos los productos
 * 
 * @param	fecha	Fecha de la cual se quiere obtener el archivo
 * @param	strDirectorio	Ruta del directorio de almacenamiento
 * @return	String	Nombre del archivo zip generado. Regresa una
 * 			cadena vacia si no se encontr� informaci�n.
 */
public String generarArchivo(String fecha, String strDirectorioTemp) 
		throws Exception {
	//1=Descuento Electr�nico
	//2=Pedidos
	//4=Distribuidores
	String [] arrProductos = {"1","2","4"};
	
	//1=Moneda Nacional MN
	//2=Dolares Americanos USD
	String [] arrMonedas = {"1", "54"};
	
	List lArchivos = new ArrayList();
	
	for(int i = 0; i < arrProductos.length; i++) {
		for(int j = 0; j < arrMonedas.length; j++) {
			List nombresArchivo = generarArchivo(arrProductos[i], arrMonedas[j], fecha, strDirectorioTemp);
			if (nombresArchivo != null && nombresArchivo.size() > 0) { //Solo si hay archivos para el ZIP
				lArchivos.addAll(nombresArchivo);
			}
		}
	}
	String nombreArchivoZIP = "";
	if (lArchivos.size() > 0) {
		//Generacion del archivo ZIP final 
		String fechaArchivo = new SimpleDateFormat("yyyyMMdd").format(Comunes.parseDate(fecha));
		//String fechaActual = new SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
		nombreArchivoZIP = fechaArchivo + "_Constancias_Cadenas.zip";
		ComunesZIP.comprimir(lArchivos,  strDirectorioTemp, 
				strDirectorioTemp + nombreArchivoZIP);
	} else { //no hay ningun archivo para el zip
		nombreArchivoZIP = "";
	}

	return nombreArchivoZIP;
}



/**
 * Genera el archivo de constancias, para cualquier IF
 * 
 * @param	ic_producto	Clave del producto:
 * 				1.- Descuento electr�nico
 *					2.- Financiamiento a Pedidos
 * 				4.- Financiamiento a Distribuidores
 * @param	ic_moneda	Clave de la moneda
 * @param	fecha	Fecha de la cual se quiere obtener el archivo dd/mm/aaaa
 * @param	strDirectorio	Ruta del directorio de almacenamiento
 * @return	List	Nombres de los archivos generados. Regresa una
 * 			lisgParametros no se encontr� informaci�n.
 */
private List generarArchivo(String ic_producto, String ic_moneda,
		String fecha, String strDirectorioTemp) 
		throws Exception {

	AccesoDB con = new AccesoDB();
	String SQLproductos = null;
	
	
	int numRegistros = 0;
	double importe = 0;
	String nombreIF = null;
	String claveIF = null;
	String nafinElectronico = "";
	
	int numTotalRegistros = 0;
	double importeTotal = 0;
	List nombresArchivo = new ArrayList();
	String nombreArchivo = null;
	String datos = null;
	String puesto = null;
	
	String strTitulo = null;
	String strMensajeA = null;
	String strMensajeB = null;
	String strMensajeC = null;
	String strMensajeCA = null;
	String strMensajeD = null;
	String strMensajeDA = null;
	StringBuffer contenidoArch = new StringBuffer(4000);	//Capacidad inicial de 4000

	ResultSet rsproductos = null;
	PreparedStatement	ps = null;
	
	String nombreMes[] = {"enero", "febrero", "marzo", "abril", "mayo",
			"junio","julio","agosto","septiembre","octubre",
			"noviembre","diciembre"};
	Calendar cal = Calendar.getInstance();

	VectorTokenizer vt = new VectorTokenizer(fecha,"/");
	Vector vFecha = vt.getValuesVector();

	String diaEspecificado = vFecha.get(0).toString();
	//Enero = 0, Febrero = 1 ... Diciembre = 11
	String mesEspecificado = nombreMes[Integer.parseInt(vFecha.get(1).toString()) - 1 ];
	String anioEspecificado = vFecha.get(2).toString();

	String fechaConvenio = null;
	String nombreProducto = null;
	String moneda = "";
	if (ic_moneda.equals("1") ) {
		moneda = "MN";
	} else if (ic_moneda.equals("54") ) {
		moneda = "USD";
	}


	List varBind = new ArrayList();

	if(ic_producto.equals("1")) {
		nombreProducto = "DESCUENTO";
		SQLproductos =
				" SELECT " +
				" 	/*+ use_nl(cpi s d i ds) */ " +
				" 	i.ic_if, i.cg_razon_social as nombreIF,  n.ic_nafin_electronico, " +
				" 	TO_CHAR(cpi.df_convenio,'dd/mm/yyyy') as fechaConvenio, " +
				" 	COUNT(*) AS numRegistros,  " +
				" 	SUM(ds.in_importe_recibir) as importeTotalRecibir " +
				" FROM com_solicitud s, com_docto_seleccionado ds,  " +
				" 	com_documento d, comrel_producto_if cpi, comcat_if i,  " +
				" 	comrel_nafin n " + 
				" WHERE ds.ic_documento = s.ic_documento  " +
				" 	AND ds.ic_documento = d.ic_documento " +
				" 	AND ds.ic_if = i.ic_if  " +
				" 	AND cpi.ic_if = i.ic_if  " +
				" 	AND ds.ic_if = n.ic_epo_pyme_if " +
				" 	AND n.cg_tipo = ? " +
				" 	AND cpi.ic_producto_nafin = ?  " +
				" 	AND d.ic_moneda = ? " + 
				" 	AND s.df_operacion >= TO_DATE(?, 'dd/mm/yyyy') " +
				" 	AND s.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1 " +
				" 	AND ic_estatus_solic IN (?,?,?) " +
				" GROUP BY i.ic_if, i.cg_razon_social, n.ic_nafin_electronico, " +
				" cpi.df_convenio " +
				" ORDER BY i.cg_razon_social ";

		varBind.add("I"); //I = IF
		varBind.add(new Integer(1)); //1 = Descuento Electronico
		varBind.add(new Integer(ic_moneda)); 
		varBind.add(fecha); 
		varBind.add(fecha); 
		varBind.add(new Integer(3)); //3 = Operada
		varBind.add(new Integer(5)); //5 = Operada Pagada
		varBind.add(new Integer(6)); //6 = Operada Pendiente de pago

		strTitulo = "documentos descontados para operaciones de factoraje electronico";
		strMensajeA = "factoraje financiero electr�nicas";
		strMensajeB = "descuento";
		strMensajeC = "Descuento Electronico";
		strMensajeCA = strMensajeC;
		strMensajeD = "Descuento";
		strMensajeDA = strMensajeD;

				
	} else if(ic_producto.equals("2")) {
		nombreProducto = "PEDIDOS";
		SQLproductos =	
				" SELECT " +
				" 	/*+ INDEX (a in_com_anticipo_02_nuk) */ " +
				" 	i.ic_if, " +
				" 	i.cg_razon_social as nombreIF, n.ic_nafin_electronico, " +
				" 	TO_CHAR(cpi.df_convenio, 'dd/mm/yyyy') as fechaConvenio, " +
				" 	COUNT(*) AS numRegistros,  " +
				" 	SUM(ps.fn_credito_recibir) AS importeTotalRecibir " +
				" FROM com_anticipo a, com_pedido_seleccionado ps,  " +
				" 	com_pedido p, com_linea_credito lc, comrel_producto_if cpi, " +
				" comcat_if i, comrel_nafin n " + 
				" Where lc.ic_linea_credito = ps.ic_linea_credito " +
				" 	AND lc.ic_if = i.ic_if  " +
				" 	AND cpi.ic_if = i.ic_if  " +
				" 	AND lc.ic_if = n.ic_epo_pyme_if " +
				" 	AND n.cg_tipo = ? " +
				" 	AND cpi.ic_producto_nafin = ?  " +
				" 	And ps.ic_pedido = a.ic_pedido  " +
				" 	And ps.ic_pedido = p.ic_pedido  " +
				" 	And p.ic_moneda = ? " + 
				" 	AND a.df_operacion >= TO_DATE(?, 'dd/mm/yyyy') " +
				" 	AND a.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1 " +
				" 	And ic_estatus_antic in (?,?,?)  " +
				" GROUP BY i.ic_if, i.cg_razon_social, n.ic_nafin_electronico, " +
				" cpi.df_convenio " +
				" ORDER BY i.cg_razon_social ";
		
		varBind.add("I"); //I = IF
		varBind.add(new Integer(2)); //2 = Pedidos
		varBind.add(new Integer(ic_moneda)); 
		varBind.add(fecha); 
		varBind.add(fecha); 
		varBind.add(new Integer(3)); //3 = Operada
		varBind.add(new Integer(5)); //5 = Operada Pagada
		varBind.add(new Integer(6)); //6 = Operada Pendiente de pago
		
		strTitulo = "creditos operados en financiamiento a pedidos";
		strMensajeA = "financiamiento a pedidos";
		strMensajeB = "anticipo";
		strMensajeC = "Financiamiento a pedidos";
		strMensajeCA = strMensajeC;
		strMensajeD = "Credito";
		strMensajeDA = strMensajeD;

		
	} else if(ic_producto.equals("4")) {
		nombreProducto = "DISTRIBUIDORES";
		SQLproductos =	
				" SELECT dis.ic_if, dis.nombreIF, dis.ic_nafin_electronico, " +
				" 	dis.fechaConvenio, " +
				" 	sum(numRegistros) as numRegistros, " +
				" 	sum(importeTotalRecibir) as importeTotalRecibir " +
				" FROM " +
				" 	( " +
				" 		Select " +
				" 			/*+ index (s in_dis_solicitud_03_nuk) use_nl (s d lc) */ " +
				" 			i.ic_if, " +
				" 			i.cg_razon_social as nombreIF, n.ic_nafin_electronico, " +
				" 			TO_CHAR(cpi.df_convenio, 'dd/mm/yyyy') as fechaConvenio, " +
				" 			COUNT(*) AS numRegistros, " +
				" 			SUM(ds.fn_importe_recibir) AS importeTotalRecibir " +
				" 		From dis_solicitud s, dis_docto_seleccionado ds, " +
				" 			dis_documento d, com_linea_credito lc, comrel_producto_if cpi, " +
				" 			comcat_if i, comrel_nafin n " +
				" 		Where s.ic_documento = d.ic_documento " +
				" 			And s.ic_documento = ds.ic_documento " +
				" 			AND d.ic_linea_credito = lc.ic_linea_credito " +
				" 			AND lc.ic_if = i.ic_if " +
				" 			AND lc.ic_if = n.ic_epo_pyme_if " +
				" 			AND n.cg_tipo = ? " +
				" 			AND cpi.ic_if = i.ic_if  " +
				" 			AND cpi.ic_producto_nafin = ?  " +
				" 			And d.ic_moneda = ? " +
				" 			AND s.df_operacion >= TO_DATE(?, 'dd/mm/yyyy') " +
				" 			AND s.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1 " +
				" 			And s.ic_estatus_solic in (?,?,?) " +
				" 		GROUP BY i.ic_if, i.cg_razon_social, n.ic_nafin_electronico, " +
				" 			cpi.df_convenio " +
				"  " +
				" 		UNION  ALL " +
				"  " +
				" 		Select " +
				" 			/*+ index (s in_dis_solicitud_03_nuk) use_nl (s d lc) */ " +
				" 			i.ic_if, " +
				" 			i.cg_razon_social as nombreIF, n.ic_nafin_electronico, " +
				" 			TO_CHAR(cpi.df_convenio, 'dd/mm/yyyy') as fechaConvenio, " +
				" 			COUNT(*) AS numRegistros, " +
				" 			SUM(ds.fn_importe_recibir) AS importeTotalRecibir " +
				" 		From dis_solicitud s,dis_docto_seleccionado ds, " +
				" 			dis_documento d, dis_linea_credito_dm lc, comrel_producto_if cpi, " +
				" 			comcat_if i, comrel_nafin n " +
				" 		Where s.ic_documento = d.ic_documento " +
				" 			And s.ic_documento = ds.ic_documento " +
				" 			AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
				" 			AND lc.ic_if = i.ic_if " +
				" 			AND lc.ic_if = n.ic_epo_pyme_if " +
				" 			AND n.cg_tipo = ? " +
				" 			AND cpi.ic_if = i.ic_if  " +
				" 			AND cpi.ic_producto_nafin = ? " +
				" 			And d.ic_moneda = ? " +
				" 			AND s.df_operacion >= TO_DATE(?, 'dd/mm/yyyy') " +
				" 			AND s.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1 " +
				" 			And s.ic_estatus_solic in (?,?,?) " +
				" 		GROUP BY i.ic_if, i.cg_razon_social, n.ic_nafin_electronico, " +
				" 			cpi.df_convenio " +
				" 	) dis " +
				" group by dis.ic_if, dis.nombreIF, dis.ic_nafin_electronico, dis.fechaConvenio " +
				" order by dis.nombreIF ";

		varBind.add("I"); //I = IF
		varBind.add(new Integer(4)); //4 = Distribuidores
		varBind.add(new Integer(ic_moneda)); 
		varBind.add(fecha); 
		varBind.add(fecha); 
		varBind.add(new Integer(3)); //3 = Operada
		varBind.add(new Integer(5)); //5 = Operada Pagada
		varBind.add(new Integer(11)); //11 = Pendiente de pago IF
		
		varBind.add("I"); //I = IF
		varBind.add(new Integer(4)); //4 = Distribuidores
		varBind.add(new Integer(ic_moneda)); 
		varBind.add(fecha); 
		varBind.add(fecha); 
		varBind.add(new Integer(3)); //3 = Operada
		varBind.add(new Integer(5)); //5 = Operada Pagada
		varBind.add(new Integer(11)); //11 = Pendiente de pago IF
		
		strTitulo = "creditos operados en financiamiento a distribuidores";
		strMensajeA = "financiamiento a distribuidores";
		strMensajeB = "credito";
		strMensajeC = "Financiamiento a distribuidores";
		strMensajeCA = strMensajeC;
		strMensajeD = "Credito";
		strMensajeDA = strMensajeD;

	}

	try {
		con.conexionDB();
		System.out.println("<br>\n***********************\nSQLproductos: "+SQLproductos + "\n" + varBind);
		
		String fechaArchivo = new SimpleDateFormat("yyyyMMdd").format(Comunes.parseDate(fecha));
		//String fechaActual = new SimpleDateFormat("yyyyMMdd").format(new java.util.Date());

		Registros reg = con.consultarDB(SQLproductos, varBind);

		while ( reg.next() ) {
			String dia = null;	//Dia del convenio
			String mes = null;	//Nombre del mes del convenio
			String anio = null;	//A�o del convenio

			numRegistros = Integer.parseInt(reg.getString("NUMREGISTROS"));	//Total de registros
			importe = Double.parseDouble(reg.getString("IMPORTETOTALRECIBIR"));	//Suma de importes a recibir
			claveIF = reg.getString("IC_IF");
			nombreIF = reg.getString("NOMBREIF");
			nombreIF = nombreIF.replace(',',' ');
			nafinElectronico = reg.getString("IC_NAFIN_ELECTRONICO");
			fechaConvenio = reg.getString("FECHACONVENIO");

			numTotalRegistros += numRegistros;
			importeTotal += importe;
			

			if (fechaConvenio != null && !fechaConvenio.equals("")) {
				StringTokenizer st=new StringTokenizer(fechaConvenio,"/");
				dia = st.nextToken();
				mes = nombreMes[Integer.parseInt(st.nextToken()) - 1];
				anio = st.nextToken();
			} else {
				dia = mes = anio = "";
			}
			
			if(numRegistros > 0) {

				contenidoArch.append(
					"\n \nDe conformidad con el convenio que " + nombreIF +
					" y en la fecha que posteriormente se se�ala  celebr�" +
					" con Nacional Financiera S.N.C. bajo protesta de decir" +
					" verdad se hace constar que para todos los efectos legales" +
					" nos asumimos como depositarios de los t�tulo (s) de cr�dito" +
					" y/o documentos electr�nico (s) sin derecho a honorarios" +
					" y asumiendo la responsabilidad civil y penal correspondiente" +
					" al car�cter de depositario judicial el (los) t�tulo (s)" +
					" de cr�dito y/o documento (s) electr�nico (s) en que se" +
					" consignan los derechos de cr�dito derivados de las" +
					" operaciones de " + strMensajeA + " celebradas con los" +
					" clientes que se describen en el cuadro siguiente en donde" +
					" se especifica la fecha de presentaci�n importe numRegistros" +
					" y nombre de la empresa acreditada.\n" +
					
		    		"La informaci�n de el (los)"+ numRegistros + " documento (s) en " +
					(ic_moneda.equals("1")?"Moneda Nacional":"D�lares") + 
					" por un importe de $" + Comunes.formatoDecimal(importe,2,false) +
					" citado (s) est�n registrados en el sistema denominado Nafin" +
					" Electr�nico a disposici�n de Nacional Financiera S.N.C."+
					" cuando �sta los requiera.\n" +
					
					"Trat�ndose de el (los) t�tulo (s) de cr�dito electr�nico (s)" +
					" �stos se encuentran debidamente transmitido (s) en propiedad" +
					" y con nuestra responsabilidad a favor de Nacional Financiera" +
					" S.N.C. En el caso de el (los) documento (s) electr�nico (s)" +
					" en este acto cedemos a dicha Instituci�n de Banca de Desarrollo" +
					" los derechos de cr�dito que se derivan de los mismos.\n"+
					
					"En t�rminos del art�culo 2044 del C�digo Civil para el Distrito" +
					" Federal la cesi�n a que se refiere el p�rrafo anterior la" +
					" realizamos con nuestra responsabilidad respecto de la" +
					" solvencia de los emisores por el tiempo en que permanezcan" +
					" vigentes los adeudos y hasta su liquidaci�n numRegistros.\n" +
					
					"As� mismo y en t�rminos de lo estipulado  en el Convenio" +
					" antes mencionado nos obligamos a efectuar el cobro de" +
					" (los) t�tulo (s) de cr�dito electr�nico y/o ejercer los" +
					" derechos de cobro consignados en el (los) documentos" +
					" objeto de " + strMensajeB + " as� como ejecutar todos los" +
					" actos que sean necesarios para que el (los) documento (s)" +
					" se�alados conserven su valor y dem�s derechos" +
					" que les correspondan.\n" +
					
					"\n\n" +
					"Nombre del Intermediario Financiero " + nombreIF + " \n" +
					
					"Fecha del Convenio de " + strMensajeCA + 
					": " + ((dia.equals(""))?"":(dia + " de " + mes + " de " + anio)) +
					" \n" +
					
					"Fecha del " + strMensajeDA + " Mexico D.F." +
					" " + diaEspecificado + " de " + mesEspecificado + " de " + anioEspecificado + " \n"
				);

				if(ic_producto.equals("1")) {	//Es Descuento electr�nico
					contenidoArch.append(
							getDetalleDescuentoElectronico(claveIF, fecha, ic_moneda)
					);
				} else if(ic_producto.equals("2")) {	//Es financiamiento a Pedidos
					contenidoArch.append(
							getDetalleFinanciamientoPedidos(claveIF, fecha, ic_moneda)
					);
				} else if(ic_producto.equals("4")) {	//Es financiamiento a Distr.
					contenidoArch.append(
							getDetalleFinanciamientoDistribuidores(claveIF, fecha,
									ic_moneda)
					);
				}
			}	// fin de if()

			String SQLfacultado =	"Select cg_nombre||' '||cg_ap_paterno||' '||cg_ap_materno, " +
					"	cg_puesto " + 
					"  From com_personal_facultado " +
					" Where ic_if = ? " +
					"	And ic_producto_nafin = ? ";

			ps = con.queryPrecompilado(SQLfacultado);
			ps.setInt(1, Integer.parseInt(claveIF));
			ps.setInt(2, Integer.parseInt(ic_producto));
			
			ResultSet rsfacultado = ps.executeQuery();
			datos = "";
			puesto = "";
			if(rsfacultado.next()){
				datos = (rsfacultado.getString(1) == null)?"":rsfacultado.getString(1);
				puesto = (rsfacultado.getString(2) == null)?"":rsfacultado.getString(2);
			}
			rsfacultado.close();
			ps.close();
			contenidoArch.append("Facultado por " + nombreIF + 
					"\n" + puesto + " " + datos + "\n");


			
			if (numTotalRegistros > 0) {
				nombreArchivo = fechaArchivo + "_IF" + nafinElectronico +"_" + nombreProducto + "_" + moneda;
				contenidoArch.append(
						" \n \n N�mero Total de Registros:," + numTotalRegistros + "\n" +
						"Importe Total:," +
						Comunes.formatoDecimal(importeTotal, 2, false)
				);
	
				CreaArchivo archivo = new CreaArchivo();
				if (!archivo.make(contenidoArch.toString(), strDirectorioTemp, nombreArchivo, ".csv")) {
					System.out.println("Error en la generacion del archivo");
					throw new NafinException("SIST0001");
				}
				nombreArchivo = archivo.getNombre();
				nombresArchivo.add(nombreArchivo);
				//SE RESETEAN TOTALES y variables:
				numTotalRegistros = 0;
				importeTotal = 0;
				if (contenidoArch.length() > 0 ) {
					contenidoArch.delete(0,contenidoArch.length()); //Limpia el contenido del StringBuffer
				}
			} 
		}//fin del WHILE

		return nombresArchivo;
		
	} catch(NafinException ne) {
		ne.printStackTrace();
		throw ne;
	} catch(Exception e) {
		e.printStackTrace();
		throw e;
	} finally {
		if (con.hayConexionAbierta()) con.cierraConexionDB();
	}
}	//fin del metodo

/**
 * Genera el archivo de constancias, para cualquier IF
 * en Descuento Electr�nico (Clave de producto: 1)
 * 
 * @param	claveIF	Clave del IF del que se obtienen los detalles
 * @param	fecha	Fecha
 * @param	ic_moneda	Clave de la moneda
 * @return	String	Cadena que contiene los detalles.
 */
private  String getDetalleDescuentoElectronico(String claveIF,	String fecha, String ic_moneda)  {

	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	StringBuffer sb = new StringBuffer(400);	//tama�o inicial de 400
	
	int numRegistros = 0;
	double dblTotalMonto = 0;
	double dblTotalImporteRecibir = 0;
	
	try {
		con.conexionDB();

		String SQLproductosDetalle = 
				"SELECT /*+ use_nl (s ds d p) */ " +
				"       c.cd_descripcion, s.cg_lugar_firma, " +
				"       TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS df_fecha_docto, d.fn_monto, " +
				"       ds.in_importe_recibir, s.cg_perio_pago_cap, " +
				"       TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS df_fecha_venc, " +
				"       ds.in_tasa_aceptada, e.cg_razon_social AS nombreepo, " +
				"       p.cg_razon_social AS nombrepyme, s.ig_numero_prestamo " +
				"  FROM com_solicitud s, " +
				"       com_docto_seleccionado ds, " +
				"       com_documento d, " +
				"       comcat_pyme p, " +
				"       comcat_clase_docto c, " +
				"       comcat_epo e, " +
				"       comcat_moneda m " +
				" WHERE s.df_operacion >= TO_DATE (?, 'dd/mm/yyyy') " +
				"   AND s.df_operacion < TO_DATE (?, 'dd/mm/yyyy') + 1 " +
				"   AND s.ic_estatus_solic IN (?, ?, ?) " +
				"   AND ds.ic_if = ? " +
				"   AND s.ic_documento = ds.ic_documento " +
				"   AND s.ic_documento = d.ic_documento " +
				"   AND s.ic_clase_docto = c.ic_clase_docto " +
				"   AND d.ic_pyme = p.ic_pyme " +
				"   AND d.ic_epo = e.ic_epo " +
				"   AND m.ic_moneda = d.ic_moneda " +
				"   AND d.ic_moneda = ? ";

		
		sb.append(
				"\n\n\n" +
				"Nombre del emisor,Nombre de la empresa acreditada," +
				"Descripcion del documento," +
				"Lugar suscripcion,Fecha suscripcion," +
				"Importe documento,Importe descuento,"+
				"Periodicidad pago Capital e Interes," +
				"Fecha Vto. Descuento,Tasa Descuento,Numero de Prestamo,"+
				"Tipo de Documento\n"
		);
		
		
		ps = con.queryPrecompilado(SQLproductosDetalle);
		ps.setString(1, fecha);
		ps.setString(2, fecha);
		ps.setInt(3, 3); //Operada
		ps.setInt(4, 5); //Operada pagada
		ps.setInt(5, 6); //Operada Pendiente de pago
		ps.setInt(6, Integer.parseInt(claveIF));
		ps.setInt(7, Integer.parseInt(ic_moneda));
		
		rs = ps.executeQuery();
		
		while(rs.next()) {
			
			numRegistros++;
			dblTotalMonto += rs.getDouble("FN_MONTO");
			dblTotalImporteRecibir += rs.getDouble("IN_IMPORTE_RECIBIR");
			sb.append(
					rs.getString("NOMBREEPO").replace(',',' ') +
					"," + rs.getString("NOMBREPYME").replace(',',' ') +
					"," +	rs.getString("CD_DESCRIPCION").replace(',',' ') +
					"," +	rs.getString("CG_LUGAR_FIRMA").replace(',',' ') +
					"," + rs.getString("DF_FECHA_DOCTO") +
					"," +	rs.getString("FN_MONTO") +
					"," + rs.getString("IN_IMPORTE_RECIBIR") + 
					"," + rs.getString("CG_PERIO_PAGO_CAP") +
					"," +	rs.getString("DF_FECHA_VENC") +
					"," + rs.getString("IN_TASA_ACEPTADA") +
					"," + ((rs.getString("IG_NUMERO_PRESTAMO")==null)?"":rs.getString("IG_NUMERO_PRESTAMO")) + 
					",Constancia\n"
			);
		}//fin del while
		rs.close();
		ps.close();
		
		sb.append("Total de Documentos:,"+numRegistros +
				", , , Total," + Comunes.formatoDecimal(dblTotalMonto, 2, false) +
				"," + Comunes.formatoDecimal(dblTotalImporteRecibir, 2, false) + "\n");

			
		
		} catch (Exception e) {
			System.err.println("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return sb.toString();
}

/** 
 * Genera el archivo de constancias, para cualquier IF
 * en Financiamiento a Pedidos (Clave de producto: 2)
 * 
 * @param	claveIF	Clave del IF del que se obtienen los detalles
 * @param	fecha	Fecha
 * @param	ic_moneda	Clave de la moneda
 * @return	String	Cadena que contiene los detalles.
 */
private String getDetalleFinanciamientoPedidos(String claveIF,	String fecha, String ic_moneda) {

	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	StringBuffer sb = new StringBuffer(400);	//tama�o inicial de 400
	
	int numRegistros = 0;
	double dblTotalMonto = 0;
	double dblTotalImporteRecibir = 0;
	
	try {
		con.conexionDB();

		String SQLproductosDetalle =	
				" SELECT  " +
				" 		/*+ INDEX (a in_com_anticipo_02_nuk) */ " +
				" 		py.cg_razon_social as nombrePYME, " +
				"		e.cg_razon_social as nombreEPO,  " +
				"		c.cd_descripcion,  " +
				"		a.cg_lugar_firma,  " +
				"		to_char(p.df_fecha_pedido,'dd/mm/yyyy') as df_fecha_pedido,  " +
				"		p.fn_monto,  " +
				"		to_char(ps.df_vencimiento,'dd/mm/yyyy') as df_vencimiento,  " +
				"		ps.fn_credito_recibir,  " +
				"		a.cg_perio_pago_cap,  " +
				"		ps.fn_tasa_aceptada,  " +
				"		a.ig_numero_prestamo " +
				" FROM com_anticipo a, " +
				"     com_pedido_seleccionado ps,  " +
				"		com_pedido p,  " +
				"		com_linea_credito lc, " +
				"    	comcat_clase_docto c, " +
				"		comrel_pyme_epo pe,  " +
				"		comcat_pyme py,  " +
				"		comcat_epo e,  " +
				"		comcat_moneda m  " +
				" WHERE 	a.ic_pedido = ps.ic_pedido " +
				"	And a.ic_pedido = p.ic_pedido " +
				"	And a.ic_clase_docto = c.ic_clase_docto " +
				"  And ps.ic_linea_credito = lc.ic_linea_credito " +
				"  And p.ic_moneda = m.ic_moneda	" +
				"	And p.ic_epo = pe.ic_epo " +
				"	And p.ic_pyme = pe.ic_pyme " +
				"	And pe.ic_pyme = py.ic_pyme " +
				"	And pe.ic_epo = e.ic_epo " +
				"  And lc.ic_if = ? " +
				" 	AND a.df_operacion >= TO_DATE(?, 'dd/mm/yyyy')  " +
				" 	AND a.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1  " +
				" 	And a.ic_estatus_antic in (?, ?, ?) " +
				"	And p.ic_moneda = ? ";

		sb.append(
				"\n\n\n" +
				"Nombre del emisor,Nombre de la empresa acreditada,"+
				"Descripcion del documento," +
				"Lugar suscripcion,Fecha suscripcion,Importe documento,"+
				"Fecha Vto. del Credito, " +
				"Importe del credito, Periodicidad pago Capital e Interes," +
				"Tasa Descuento,Numero de Prestamo,Tipo de Documento\n"
		);

		ps = con.queryPrecompilado(SQLproductosDetalle);
		ps.setInt(1, Integer.parseInt(claveIF));
		ps.setString(2, fecha);
		ps.setString(3, fecha);
		ps.setInt(4, 3); //Operada
		ps.setInt(5, 5); //Operada Pagada
		ps.setInt(6, 6); //operada pendiente de pago
		ps.setInt(7, Integer.parseInt(ic_moneda));
		
		rs = ps.executeQuery();
		
		while(rs.next()) {
			numRegistros++;
			dblTotalMonto += rs.getDouble("FN_MONTO");
			dblTotalImporteRecibir += rs.getDouble("FN_CREDITO_RECIBIR");
			sb.append(
					rs.getString("NOMBREPYME").replace(',',' ') + "," + 
					rs.getString("NOMBREEPO").replace(',',' ') + "," + 
					rs.getString("CD_DESCRIPCION").replace(',',' ') + "," + 
					rs.getString("CG_LUGAR_FIRMA").replace(',',' ') + "," + 
					rs.getString("DF_FECHA_PEDIDO")+ "," + 
					rs.getString("FN_MONTO") + "," + 
					rs.getString("DF_VENCIMIENTO")+ "," + 
					rs.getString("FN_CREDITO_RECIBIR") + "," + 
					rs.getString("CG_PERIO_PAGO_CAP")+ "," + 
					rs.getString("FN_TASA_ACEPTADA")+ "," + 
					((rs.getString("IG_NUMERO_PRESTAMO") == null) ? "" : rs.getString("IG_NUMERO_PRESTAMO")) + 
					",Constancia\n"
			);
		}	//fin del while
		rs.close();
		ps.close();
		
		sb.append("Total de Documentos:,"+numRegistros +
				", , , Total," + Comunes.formatoDecimal(dblTotalMonto, 2, false) +
				", ," + Comunes.formatoDecimal(dblTotalImporteRecibir, 2, false) + "\n");

		
		
		} catch (Exception e) {
			System.err.println("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	return sb.toString();
}

/**
 * Genera el archivo de constancias, para cualquier IF
 * en Financiamiento a Distribuidores (Clave de producto: 3)
 * 
 * @param	claveIF	Clave del IF del que se obtienen los detalles
 * @param	fecha	Fecha
 * @param	ic_moneda	Clave de la moneda
 * @return	String	Cadena que contiene los detalles.
 */
private String getDetalleFinanciamientoDistribuidores(String claveIF,	String fecha, String ic_moneda)  {

	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	StringBuffer sb = new StringBuffer(400);	//tama�o inicial de 400
	
	int numRegistros = 0;
	double dblTotalMonto = 0;
	double dblTotalImporteRecibir = 0;
	
	try {
		con.conexionDB();

		String SQLproductosDetalle =
					"SELECT /*+ index (s in_dis_solicitud_03_nuk) */    " +
					"       e.cg_razon_social AS epo, p.cg_razon_social AS pyme, " +
					"       cd.cd_descripcion AS clase_docto, 'Mexico, D.F.' AS cg_lugar_firma, " +
					"       TO_CHAR (s.df_operacion, 'dd/mm/yyyy') AS fecha_suscripcion, " +
					"       d.fn_monto AS importe_documento, " +
					"       ds.fn_importe_recibir AS importe_financiamiento, " +
					"       'Al vencimiento' AS periodicidad_capital, " +
					"       tci.cd_descripcion AS periodicidad_interes, " +
					"       TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fecha_venc, " +
					"       DECODE (ds.cg_rel_mat, " +
					"               '+', ds.fn_valor_tasa + ds.fn_puntos, " +
					"               '-', ds.fn_valor_tasa - ds.fn_puntos, " +
					"               '*', ds.fn_valor_tasa * ds.fn_puntos, " +
					"               '/', ds.fn_valor_tasa / ds.fn_puntos " +
					"              ) AS tasa, " +
					"       s.ig_numero_prestamo " +
					"  FROM " +
					"       dis_solicitud s,  " +
					"       dis_docto_seleccionado ds, " +
					"       dis_documento d, " +
					"       com_linea_credito lc,        " +
					"       comcat_pyme p,        " +
					"       comcat_epo e, " +
					"       comcat_clase_docto cd, " +
					"       comcat_tipo_cobro_interes tci, " +
					"       comcat_moneda m " +
					" WHERE  " +
					"    s.ic_documento = ds.ic_documento " +
					"    AND s.ic_documento = d.ic_documento " +
					"    AND s.ic_clase_docto = cd.ic_clase_docto  " +
					"    AND s.ic_estatus_solic IN (?, ?, ?) " +
					"    AND d.ic_pyme = p.ic_pyme " +
					"    AND d.ic_epo = e.ic_epo " +
					"    AND d.ic_linea_credito = lc.ic_linea_credito " +
					"    AND d.ic_moneda = m.ic_moneda  " +
					"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes " +
					"    AND lc.ic_if = ? " +
					" 	  AND s.df_operacion >= TO_DATE(?, 'dd/mm/yyyy')  " +
					" 	  AND s.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1  " +
					"    AND d.ic_moneda = ? " +
					"UNION ALL " +
					"SELECT /*+ use_nl (s ds d lc) */ " +
					"       e.cg_razon_social AS epo, p.cg_razon_social AS pyme, " +
					"       cd.cd_descripcion AS clase_docto, 'Mexico, D.F.' AS cg_lugar_firma, " +
					"       TO_CHAR (s.df_operacion, 'dd/mm/yyyy') AS fecha_suscripcion, " +
					"       d.fn_monto AS importe_documento, " +
					"       ds.fn_importe_recibir AS importe_financiamiento, " +
					"       'Al vencimiento' AS periodicidad_capital, " +
					"       tci.cd_descripcion AS periodicidad_interes, " +
					"       TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fecha_venc, " +
					"       DECODE (ds.cg_rel_mat, " +
					"               '+', ds.fn_valor_tasa + ds.fn_puntos, " +
					"               '-', ds.fn_valor_tasa - ds.fn_puntos, " +
					"               '*', ds.fn_valor_tasa * ds.fn_puntos, " +
					"               '/', ds.fn_valor_tasa / ds.fn_puntos " +
					"              ) AS tasa, " +
					"       s.ig_numero_prestamo " +
					"  FROM dis_solicitud s,  " +
					"       dis_docto_seleccionado ds, " +
					"       dis_documento d, " +
					"       dis_linea_credito_dm lc, " +
					"       comcat_pyme p, " +
					"       comcat_epo e, " +
					"       comcat_clase_docto cd, " +
					"       comcat_tipo_cobro_interes tci, " +
					"       comcat_moneda m " +
					" WHERE s.ic_documento = ds.ic_documento " +
					"   AND s.ic_documento = d.ic_documento " +
					"   AND s.ic_estatus_solic IN (?, ?, ?) " +
					"   AND s.ic_clase_docto = cd.ic_clase_docto " +
					"   AND d.ic_epo = e.ic_epo " +
					"   AND d.ic_pyme = p.ic_pyme " +
					"   AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
					"   AND d.ic_moneda = m.ic_moneda " +
					"   AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes " +
					"   AND lc.ic_if = ? " +
					"   AND s.df_operacion >= TO_DATE(?, 'dd/mm/yyyy')  " +
					"   AND s.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1  " +
					"   AND d.ic_moneda = ? ";

		sb.append(
				"\n\n\n" +
				"Nombre del Emisor,Nombre de la Empresa Acreditada," +
				"Descripci�n del Documento,Lugar Suscripci�n," +
				"Fecha Suscripci�n,Importe del Documento," +
				"Importe del Financiamiento,Periodicidad de Pago de Capital," +
				"Periodicidad Pago de Inter�s,Fecha Vencimiento," +
				"Tasa de Inter�s,N�mero de Pr�stamo Nafin,Tipo de Documento\n"
		);

		ps = con.queryPrecompilado(SQLproductosDetalle);
		ps.setInt(1, 3); //Operada
		ps.setInt(2, 5); //Operada pagada
		ps.setInt(3, 11); //Pendiente de PAgo IF
		ps.setInt(4, Integer.parseInt(claveIF));
		ps.setString(5, fecha);
		ps.setString(6, fecha);
		ps.setInt(7, Integer.parseInt(ic_moneda));
		ps.setInt(8, 3); //Operada
		ps.setInt(9, 5); //Operada pagada
		ps.setInt(10, 11); //Pendiente de PAgo IF
		ps.setInt(11, Integer.parseInt(claveIF));
		ps.setString(12, fecha);
		ps.setString(13, fecha);
		ps.setInt(14, Integer.parseInt(ic_moneda));
		
		rs = ps.executeQuery();
		
		while(rs.next()) {
			
			numRegistros++;
			dblTotalMonto += rs.getDouble("IMPORTE_DOCUMENTO");
			dblTotalImporteRecibir += rs.getDouble("IMPORTE_FINANCIAMIENTO");
			sb.append(
					rs.getString("EPO").replace(',',' ') + "," + 
					rs.getString("PYME").replace(',',' ') + "," + 
					rs.getString("CLASE_DOCTO").replace(',',' ') + "," + 
					rs.getString("CG_LUGAR_FIRMA").replace(',',' ') + "," + 
					rs.getString("FECHA_SUSCRIPCION")+ "," + 
					rs.getString("IMPORTE_DOCUMENTO") + "," + 
					rs.getString("IMPORTE_FINANCIAMIENTO") + "," + 
					rs.getString("PERIODICIDAD_CAPITAL") + "," + 
					rs.getString("PERIODICIDAD_INTERES")+ "," + 
					rs.getString("FECHA_VENC")+ "," + 
					rs.getString("TASA") + ","+
					((rs.getString("IG_NUMERO_PRESTAMO") == null) ? "" : rs.getString("IG_NUMERO_PRESTAMO")) + 
					",Constancia\n"
			);
		} //Fin del while
		rs.close();
		ps.close();
		sb.append("Total de Documentos:,"+numRegistros +
				", , , Total," + Comunes.formatoDecimal(dblTotalMonto, 2, false) +
				"," + Comunes.formatoDecimal(dblTotalImporteRecibir, 2, false) + "\n");
		
	} catch (Exception e) {
		System.err.println("Error: " + e);
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
	return sb.toString();
}



//********************Finaliza  la parte de Concentrado del d�a  ***************************//


/**
 * Genera el archivo de constancias, para cualquier IF
 * 
 * @param	ic_producto	Clave del producto:
 * 				1.- Descuento electr�nico
 *					2.- Financiamiento a Pedidos
 * 				4.- Financiamiento a Distribuidores
 * @param	ic_moneda	Clave de la moneda
 * @param	fecha	Fecha de la cual se quiere obtener el archivo
 * @param	strDirectorio	Ruta del directorio de almacenamiento
 * @return	String	Nombre del archivo generado. Regresa una
 * 			cadena vacia si no se encontr� informaci�n.
 */
public String generarArchivoD(String ic_producto, String ic_moneda, String fecha, String strDirectorioTemp) { 

	AccesoDB con = new AccesoDB();
	String SQLproductos = null;
	
	
	int numRegistros = 0;
	double importe = 0;
	String nombreIF = null;
	String claveIF = null;
	
	int numTotalRegistros = 0;
	double importeTotal = 0;
	
	String nombreArchivo = null;
	String datos = null;
	String puesto = null;
	
	String strTitulo = null;
	String strMensajeA = null;
	String strMensajeB = null;
	String strMensajeC = null;
	String strMensajeCA = null;
	String strMensajeD = null;
	String strMensajeDA = null;
	StringBuffer contenidoArch = new StringBuffer(4000);	//Capacidad inicial de 4000

	ResultSet rsproductos = null;
	PreparedStatement	ps = null;
	
	String nombreMes[] = {"enero", "febrero", "marzo", "abril", "mayo",
			"junio","julio","agosto","septiembre","octubre",
			"noviembre","diciembre"};
	Calendar cal = Calendar.getInstance();

	VectorTokenizer vt = new VectorTokenizer(fecha,"/");
	Vector vFecha = vt.getValuesVector();

	String diaEspecificado = vFecha.get(0).toString();
	//Enero = 0, Febrero = 1 ... Diciembre = 11
	String mesEspecificado = nombreMes[Integer.parseInt(vFecha.get(1).toString()) - 1 ];
	String anioEspecificado = vFecha.get(2).toString();

	String fechaConvenio = null;


	List varBind = new ArrayList();

	if(ic_producto.equals("1")) {
		SQLproductos =
				" SELECT " +
				" 	/*+ use_nl(cpi s d i ds) */ " +
				" 	i.ic_if, i.cg_razon_social as nombreIF,  " +
				" 	TO_CHAR(cpi.df_convenio,'dd/mm/yyyy') as fechaConvenio, " +
				" 	COUNT(*) AS numRegistros,  " +
				" 	SUM(ds.in_importe_recibir) as importeTotalRecibir " +
				" FROM com_solicitud s, com_docto_seleccionado ds,  " +
				" 	com_documento d, comrel_producto_if cpi, comcat_if i  " +
				" WHERE ds.ic_documento = s.ic_documento  " +
				" 	AND ds.ic_documento = d.ic_documento " +
				" 	AND ds.ic_if = i.ic_if  " +
				" 	AND cpi.ic_if = i.ic_if  " +
				" 	AND cpi.ic_producto_nafin = ?  " +
				" 	AND d.ic_moneda = ? " + 
				" 	AND s.df_operacion >= TO_DATE(?, 'dd/mm/yyyy') " +
				" 	AND s.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1 " +
				" 	AND ic_estatus_solic IN (?,?,?) " +
				" GROUP BY i.ic_if, i.cg_razon_social, cpi.df_convenio " +
				" ORDER BY i.cg_razon_social ";

		varBind.add(new Integer(1)); //1 = Descuento Electronico
		varBind.add(new Integer(ic_moneda)); 
		varBind.add(fecha); 
		varBind.add(fecha); 
		varBind.add(new Integer(3)); //3 = Operada
		varBind.add(new Integer(5)); //5 = Operada Pagada
		varBind.add(new Integer(6)); //6 = Operada Pendiente de pago

		strTitulo = "documentos descontados para operaciones de factoraje electronico";
		strMensajeA = "factoraje financiero electr�nicas";
		strMensajeB = "descuento";
		strMensajeC = "Descuento Electronico";
		strMensajeCA = strMensajeC;
		strMensajeD = "Descuento";
		strMensajeDA = strMensajeD;

				
	} else if(ic_producto.equals("2")) {
		SQLproductos =	
				" SELECT " +
				" 	/*+ INDEX (a in_com_anticipo_02_nuk) */ " +
				" 	i.ic_if, " +
				" 	i.cg_razon_social as nombreIF, " +
				" 	TO_CHAR(cpi.df_convenio, 'dd/mm/yyyy') as fechaConvenio, " +
				" 	COUNT(*) AS numRegistros,  " +
				" 	SUM(ps.fn_credito_recibir) AS importeTotalRecibir " +
				" FROM com_anticipo a, com_pedido_seleccionado ps,  " +
				" 	com_pedido p, com_linea_credito lc, comrel_producto_if cpi, comcat_if i " +
				" Where lc.ic_linea_credito = ps.ic_linea_credito " +
				" 	AND lc.ic_if = i.ic_if  " +
				" 	AND cpi.ic_if = i.ic_if  " +
				" 	AND cpi.ic_producto_nafin = ?  " +
				" 	And ps.ic_pedido = a.ic_pedido  " +
				" 	And ps.ic_pedido = p.ic_pedido  " +
				" 	And p.ic_moneda = ? " + 
				" 	AND a.df_operacion >= TO_DATE(?, 'dd/mm/yyyy') " +
				" 	AND a.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1 " +
				" 	And ic_estatus_antic in (?,?,?)  " +
				" GROUP BY i.ic_if, i.cg_razon_social, cpi.df_convenio " +
				" ORDER BY i.cg_razon_social ";

		varBind.add(new Integer(2)); //2 = Pedidos
		varBind.add(new Integer(ic_moneda)); 
		varBind.add(fecha); 
		varBind.add(fecha); 
		varBind.add(new Integer(3)); //3 = Operada
		varBind.add(new Integer(5)); //5 = Operada Pagada
		varBind.add(new Integer(6)); //6 = Operada Pendiente de pago
		
		strTitulo = "creditos operados en financiamiento a pedidos";
		strMensajeA = "financiamiento a pedidos";
		strMensajeB = "anticipo";
		strMensajeC = "Financiamiento a pedidos";
		strMensajeCA = strMensajeC;
		strMensajeD = "Credito";
		strMensajeDA = strMensajeD;

		
	} else if(ic_producto.equals("4")) {
		SQLproductos =	
				" SELECT dis.ic_if, dis.nombreIF, dis.fechaConvenio, " +
				" 	sum(numRegistros) as numRegistros, " +
				" 	sum(importeTotalRecibir) as importeTotalRecibir " +
				" FROM " +
				" 	( " +
				" 		Select " +
				" 			/*+ index (s in_dis_solicitud_03_nuk) use_nl (s d lc) */ " +
				" 			i.ic_if, " +
				" 			i.cg_razon_social as nombreIF, " +
				" 			TO_CHAR(cpi.df_convenio, 'dd/mm/yyyy') as fechaConvenio, " +
				" 			COUNT(*) AS numRegistros, " +
				" 			SUM(ds.fn_importe_recibir) AS importeTotalRecibir " +
				" 		From dis_solicitud s, dis_docto_seleccionado ds, " +
				" 			dis_documento d, com_linea_credito lc, comrel_producto_if cpi, comcat_if i " +
				" 		Where s.ic_documento = d.ic_documento " +
				" 			And s.ic_documento = ds.ic_documento " +
				" 			AND d.ic_linea_credito = lc.ic_linea_credito " +
				" 			AND lc.ic_if = i.ic_if " +
				" 			AND cpi.ic_if = i.ic_if  " +
				" 			AND cpi.ic_producto_nafin = ?  " +
				" 			And d.ic_moneda = ? " +
				" 			AND s.df_operacion >= TO_DATE(?, 'dd/mm/yyyy') " +
				" 			AND s.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1 " +
				" 			And s.ic_estatus_solic in (?,?,?) " +
				" 		GROUP BY i.ic_if, i.cg_razon_social, cpi.df_convenio " +
				"  " +
				" 		UNION  ALL " +
				"  " +
				" 		Select " +
				" 			/*+ index (s in_dis_solicitud_03_nuk) use_nl (s d lc) */ " +
				" 			i.ic_if, " +
				" 			i.cg_razon_social as nombreIF," +
				" 			TO_CHAR(cpi.df_convenio, 'dd/mm/yyyy') as fechaConvenio, " +
				" 			COUNT(*) AS numRegistros, " +
				" 			SUM(ds.fn_importe_recibir) AS importeTotalRecibir " +
				" 		From dis_solicitud s,dis_docto_seleccionado ds, " +
				" 			dis_documento d, dis_linea_credito_dm lc, comrel_producto_if cpi, comcat_if i " +
				" 		Where s.ic_documento = d.ic_documento " +
				" 			And s.ic_documento = ds.ic_documento " +
				" 			AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
				" 			AND lc.ic_if = i.ic_if " +
				" 			AND cpi.ic_if = i.ic_if  " +
				" 			AND cpi.ic_producto_nafin = ? " +
				" 			And d.ic_moneda = ? " +
				" 			AND s.df_operacion >= TO_DATE(?, 'dd/mm/yyyy') " +
				" 			AND s.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1 " +
				" 			And s.ic_estatus_solic in (?,?,?) " +
				" 		GROUP BY i.ic_if, i.cg_razon_social, cpi.df_convenio " +
				" 	) dis " +
				" group by dis.ic_if, dis.nombreIF, dis.fechaConvenio " +
				" order by dis.nombreIF ";

		varBind.add(new Integer(4)); //4 = Distribuidores
		varBind.add(new Integer(ic_moneda)); 
		varBind.add(fecha); 
		varBind.add(fecha); 
		varBind.add(new Integer(3)); //3 = Operada
		varBind.add(new Integer(5)); //5 = Operada Pagada
		varBind.add(new Integer(11)); //11 = Pendiente de pago IF
		varBind.add(new Integer(4)); //4 = Distribuidores
		varBind.add(new Integer(ic_moneda)); 
		varBind.add(fecha); 
		varBind.add(fecha); 
		varBind.add(new Integer(3)); //3 = Operada
		varBind.add(new Integer(5)); //5 = Operada Pagada
		varBind.add(new Integer(11)); //11 = Pendiente de pago IF
		
		strTitulo = "creditos operados en financiamiento a distribuidores";
		strMensajeA = "financiamiento a distribuidores";
		strMensajeB = "credito";
		strMensajeC = "Financiamiento a distribuidores";
		strMensajeCA = strMensajeC;
		strMensajeD = "Credito";
		strMensajeDA = strMensajeD;

	}

	try {
		con.conexionDB();
		System.out.println("<br>\n***********************\nSQLproductos: "+SQLproductos + "\n" + varBind);
		
		Registros reg = con.consultarDB(SQLproductos, varBind);

		while ( reg.next() ) {
			String dia = null;	//Dia del convenio
			String mes = null;	//Nombre del mes del convenio
			String anio = null;	//A�o del convenio

			numRegistros = Integer.parseInt(reg.getString("NUMREGISTROS"));	//Total de registros
			importe = Double.parseDouble(reg.getString("IMPORTETOTALRECIBIR"));	//Suma de importes a recibir
			claveIF = reg.getString("IC_IF");
			nombreIF = reg.getString("NOMBREIF");
			nombreIF = nombreIF.replace(',',' ');
			fechaConvenio = reg.getString("FECHACONVENIO");

			numTotalRegistros += numRegistros;
			importeTotal += importe;
			

			if (fechaConvenio != null && !fechaConvenio.equals("")) {
				StringTokenizer st=new StringTokenizer(fechaConvenio,"/");
				dia = st.nextToken();
				mes = nombreMes[Integer.parseInt(st.nextToken()) - 1];
				anio = st.nextToken();
			} else {
				dia = mes = anio = "";
			}
			
			if(numRegistros > 0) {

				contenidoArch.append(
					"\n \nDe conformidad con el convenio que " + nombreIF +
					" y en la fecha que posteriormente se se�ala  celebr�" +
					" con Nacional Financiera S.N.C. bajo protesta de decir" +
					" verdad se hace constar que para todos los efectos legales" +
					" nos asumimos como depositarios de los t�tulo (s) de cr�dito" +
					" y/o documentos electr�nico (s) sin derecho a honorarios" +
					" y asumiendo la responsabilidad civil y penal correspondiente" +
					" al car�cter de depositario judicial el (los) t�tulo (s)" +
					" de cr�dito y/o documento (s) electr�nico (s) en que se" +
					" consignan los derechos de cr�dito derivados de las" +
					" operaciones de " + strMensajeA + " celebradas con los" +
					" clientes que se describen en el cuadro siguiente en donde" +
					" se especifica la fecha de presentaci�n importe numRegistros" +
					" y nombre de la empresa acreditada.\n" +
					
		    		"La informaci�n de el (los)"+ numRegistros + " documento (s) en " +
					(ic_moneda.equals("1")?"Moneda Nacional":"D�lares") + 
					" por un importe de $" + Comunes.formatoDecimal(importe,2,false) +
					" citado (s) est�n registrados en el sistema denominado Nafin" +
					" Electr�nico a disposici�n de Nacional Financiera S.N.C."+
					" cuando �sta los requiera.\n" +
					
					"Trat�ndose de el (los) t�tulo (s) de cr�dito electr�nico (s)" +
					" �stos se encuentran debidamente transmitido (s) en propiedad" +
					" y con nuestra responsabilidad a favor de Nacional Financiera" +
					" S.N.C. En el caso de el (los) documento (s) electr�nico (s)" +
					" en este acto cedemos a dicha Instituci�n de Banca de Desarrollo" +
					" los derechos de cr�dito que se derivan de los mismos.\n"+
					
					"En t�rminos del art�culo 2044 del C�digo Civil para el Distrito" +
					" Federal la cesi�n a que se refiere el p�rrafo anterior la" +
					" realizamos con nuestra responsabilidad respecto de la" +
					" solvencia de los emisores por el tiempo en que permanezcan" +
					" vigentes los adeudos y hasta su liquidaci�n numRegistros.\n" +
					
					"As� mismo y en t�rminos de lo estipulado  en el Convenio" +
					" antes mencionado nos obligamos a efectuar el cobro de" +
					" (los) t�tulo (s) de cr�dito electr�nico y/o ejercer los" +
					" derechos de cobro consignados en el (los) documentos" +
					" objeto de " + strMensajeB + " as� como ejecutar todos los" +
					" actos que sean necesarios para que el (los) documento (s)" +
					" se�alados conserven su valor y dem�s derechos" +
					" que les correspondan.\n" +
					
					"\n\n" +
					"Nombre del Intermediario Financiero " + nombreIF + " \n" +
					
					"Fecha del Convenio de " + strMensajeCA + 
					": " + ((dia.equals(""))?"":(dia + " de " + mes + " de " + anio)) +
					" \n" +
					
					"Fecha del " + strMensajeDA + " Mexico D.F." +
					" " + diaEspecificado + " de " + mesEspecificado + " de " + anioEspecificado + " \n"
				);

				if(ic_producto.equals("1")) {	//Es Descuento electr�nico
					contenidoArch.append(
							getDetalleDescuentoElectronico(claveIF, fecha, ic_moneda)
					);
				} else if(ic_producto.equals("2")) {	//Es financiamiento a Pedidos
					contenidoArch.append(
							getDetalleFinanciamientoPedidos(claveIF, fecha, ic_moneda)
					);
				} else if(ic_producto.equals("4")) {	//Es financiamiento a Distr.
					contenidoArch.append(
							getDetalleFinanciamientoDistribuidores(claveIF, fecha,
									ic_moneda)
					);
				}
			}	// fin de if()

			String SQLfacultado =	"Select cg_nombre||' '||cg_ap_paterno||' '||cg_ap_materno, " +
					"	cg_puesto " + 
					"  From com_personal_facultado " +
					" Where ic_if = ? " +
					"	And ic_producto_nafin = ? ";

			ps = con.queryPrecompilado(SQLfacultado);
			ps.setInt(1, Integer.parseInt(claveIF));
			ps.setInt(2, Integer.parseInt(ic_producto));
			
			ResultSet rsfacultado = ps.executeQuery();
			datos = "";
			puesto = "";
			if(rsfacultado.next()){
				datos = (rsfacultado.getString(1) == null)?"":rsfacultado.getString(1);
				puesto = (rsfacultado.getString(2) == null)?"":rsfacultado.getString(2);
			}
			rsfacultado.close();
			ps.close();
			contenidoArch.append("Facultado por " + nombreIF + 
					"\n" + puesto + " " + datos + "\n");
		}//fin del while

		nombreArchivo = "";
		if (numTotalRegistros > 0) {

			contenidoArch.append(
					" \n \n N�mero Total de Registros:," + numTotalRegistros + "\n" +
					"Importe Total:," +
					Comunes.formatoDecimal(importeTotal, 2, false)
			);

			CreaArchivo archivo = new CreaArchivo();
			if (!archivo.make(contenidoArch.toString(), strDirectorioTemp, ".csv")) {
				System.out.println("Error en la generacion del archivo");
			} else {
				nombreArchivo = archivo.nombre;
			}
		}

		
	} catch (Exception e) {
		System.err.println("Error: " + e);
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
	return nombreArchivo;
}	//fin del metodo


	/**
	 *  Metodo para Grabar el Remanente 
	 * @return 
	 * @param fn_remanente
	 * @param ic_docto_sel
	 * @param numRegistros
	 */
	public String  grabarRemanente(int  numRegistros, String [] ic_docto_sel , String [] fn_remanente  ){
		log.info("grabarRemanente (E)");
		
		PreparedStatement ps	= null;
		ResultSet			rs	= null;
		AccesoDB con =new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer("");		
		boolean transactionOk = true;
		List varBind = new ArrayList();	
		String mensaje ="";
		
		try{	
			con.conexionDB();
			if(numRegistros>0) {
			
				for (int i=0;i<numRegistros;i++)  {
					String ic_docto = ic_docto_sel[i].toString();
					String fn_rema = fn_remanente[i].toString();
					
					qrySentencia.append(	" UPDATE com_docto_seleccionado"   +
												" SET fn_remanente = ? "   +
												"  WHERE ic_documento = ? ");
					varBind = new ArrayList();									
					varBind.add(fn_rema);
					varBind.add(ic_docto);	
						
					ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
					ps.executeUpdate();
					ps.close();
					mensaje = "Los cambios se almacenaron satisfactoriamente";
			}
		} else  {
			mensaje = "Los cambios se almacenaron satisfactoriamente";
		}
	
					
	} catch(Exception e) {
		transactionOk=false;	
		e.printStackTrace();
		log.error("error al grabarRemanente  " +e); 
	} finally {	
		if(con.hayConexionAbierta()) {
			con.terminaTransaccion(transactionOk);
			con.cierraConexionDB();	
		}
	}    
	log.info("grabarRemanente (S) ");
		return mensaje;
	}
	
	/**
	 * Consulta de Confirmaci�n de Operaciones del d�a
	 * @return 
	 * @param moneda
	 * @param fecha_a
	 * @param fecha_de
	 */
	public List  consConfirmacion( String fecha_de , String fecha_a , String ic_moneda ){
		log.info("consConfirmacion (E)");  
		
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs	= null;
		boolean transactionOk = true;
		
		StringBuffer qrySentencia = new StringBuffer("");		
		List varBind = new ArrayList();	
		List respuesta = new ArrayList();
		      
		try{	
		
			con.conexionDB();	
	
			qrySentencia = new StringBuffer("");	
			qrySentencia.append( 
				" SELECT cif.ic_if, cif.cg_razon_social, SUM (doc.numoperaciones), SUM (doc.monto), doc.moneda "   +
				"   FROM (SELECT   /*+index(sel CP_COM_DOCTO_SELECCIONADO_PK) index(sol CP_COM_SOLICITUD_PK)*/"   +
				"                sel.ic_if, doc.ic_moneda AS moneda, COUNT (1) AS numoperaciones, SUM (sel.in_importe_recibir) AS monto"   +
				"           FROM com_documento doc, com_docto_seleccionado sel, com_solicitud sol"   +
				"          WHERE doc.ic_documento = sel.ic_documento"   +
				"            AND sel.ic_documento = sol.ic_documento"   +
				"            AND sol.df_operacion  >= TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND sol.df_operacion  < TRUNC (TO_DATE (?, 'dd/mm/yyyy')) + 1 "   +
				"            AND sol.ic_estatus_solic IN (3,  5,  6)"   +
				"          GROUP BY sel.ic_if,  doc.ic_moneda"   +
				"         UNION"   +
				"         SELECT   /*+index(sel CP_COM_PEDIDO_SELECCIONADO_PK) index(ant CP_COM_ANTICIPO_PK)*/"   +
				"                lc.ic_if, lc.ic_moneda AS moneda, COUNT (1) AS numoperaciones, SUM (sel.fn_credito_recibir) AS monto"   +
				"           FROM com_pedido ped, com_pedido_seleccionado sel, com_anticipo ant, com_linea_credito lc"   +
				"          WHERE ped.ic_pedido = sel.ic_pedido"   +
				"            AND sel.ic_pedido = ant.ic_pedido"   +
				"            AND sel.ic_linea_credito = lc.ic_linea_credito"   +
				"            AND ant.df_operacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND ant.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy')) + 1"   +
				"            AND ant.ic_estatus_antic IN (3,  5,  6)"   +
				"          GROUP BY lc.ic_if,  lc.ic_moneda"   +
				"         UNION"   +
				"         SELECT   /*+index(doc CP_DIS_DOCUMENTO_PK) index(sel CP_DIS_DOCTO_SELECCIONADO_PK) index(sol CP_DIS_SOLICITUD_PK)*/"   +
				"                lc.ic_if, lc.ic_moneda AS moneda, COUNT (1) AS numoperaciones, SUM (sel.fn_importe_recibir) AS monto"   +
				"           FROM dis_documento doc, dis_docto_seleccionado sel, dis_solicitud sol, com_linea_credito lc"   +
				"          WHERE doc.ic_documento = sel.ic_documento"   +
				"            AND sel.ic_documento = sol.ic_documento"   +
				"            AND doc.ic_linea_credito = lc.ic_linea_credito"   +
				"            AND sol.df_operacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND sol.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy')) + 1 "   +
				"            AND sol.ic_estatus_solic IN (3,  5,  11)"   +
				"          GROUP BY lc.ic_if,  lc.ic_moneda"   +
				"         UNION"   +
				"         SELECT   /*+ index(sel CP_DIS_DOCTO_SELECCIONADO_PK) index(sol CP_DIS_SOLICITUD_PK)*/"   +
				"                lc.ic_if, lc.ic_moneda AS moneda, COUNT (1) AS numoperaciones, SUM (sel.fn_importe_recibir) AS monto"   +
				"           FROM dis_documento doc, dis_docto_seleccionado sel, dis_solicitud sol, dis_linea_credito_dm lc"   +
				"          WHERE doc.ic_documento = sel.ic_documento"   +
				"            AND sel.ic_documento = sol.ic_documento"   +
				"            AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm"   +
				"            AND sol.df_operacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND sol.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy')) + 1 "   +
				"            AND sol.ic_estatus_solic IN (3,  5,  11)"   +
				"          GROUP BY lc.ic_if,  lc.ic_moneda) doc, comcat_if cif "   +
				"  WHERE doc.ic_if = cif.ic_if"   +
				"  GROUP BY cif.ic_if,  cif.cg_razon_social,  moneda ") ;
				
				varBind = new ArrayList();
				varBind.add(fecha_de);
				varBind.add(fecha_a);
				varBind.add(fecha_de);
				varBind.add(fecha_a);
				varBind.add(fecha_de);
				varBind.add(fecha_a);
				varBind.add(fecha_de);
				varBind.add(fecha_a);
				ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
				rs = ps.executeQuery();
				
				Vector doctoMN = new Vector();
				Vector doctoDL = new Vector();
				String confirmaMN = "N";
				String confirmaDL = "N";
				
				while (rs.next()){
					Vector cols = new Vector();
					String rs_ic_if 	= rs.getString(1)==null?"":rs.getString(1);
					String rs_if 		= rs.getString(2)==null?"":rs.getString(2);
					String rs_numOper 	= rs.getString(3)==null?"":rs.getString(3);
					String rs_monto 	= rs.getString(4)==null?"":rs.getString(4);
					String rs_moneda 	= rs.getString(5)==null?"":rs.getString(5);
					cols.addElement(rs_ic_if);
					cols.addElement(rs_if);
					cols.addElement(rs_numOper);
					cols.addElement(rs_monto);
					if("1".equals(rs_moneda)) {
							doctoMN.addElement(cols);
					}else if("54".equals(rs_moneda)) {
							doctoDL.addElement(cols);
					}
				}
				rs.close();
				ps.close();
	
				qrySentencia = new StringBuffer("");	
				qrySentencia.append(
					" SELECT COUNT (1)"   +
					"   FROM comrel_constancia_deposito"   +
					"  WHERE ic_moneda =? "   +
					"    AND cs_confirmacion = 'S'"   +
					"    AND df_operacion >= TRUNC (TO_DATE (?,'dd/mm/yyyy')) AND df_operacion < TRUNC (TO_DATE (?,'dd/mm/yyyy')) + 1 ") ;
				varBind = new ArrayList();
				varBind.add("1");
				varBind.add(fecha_de);
				varBind.add(fecha_a);
				
				ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
				rs = ps.executeQuery();
				if(rs.next()) {
					if(rs.getInt(1)>0) {
						confirmaMN = "S";
					}					
				}		
				rs.close();
				ps.close();
				
				qrySentencia = new StringBuffer("");	
				qrySentencia.append(
					" SELECT COUNT (1)"   +
					"   FROM comrel_constancia_deposito"   +
					"  WHERE ic_moneda =? "   +
					"    AND cs_confirmacion = 'S'"   +
					"    AND df_operacion >= TRUNC(TO_DATE (?,'dd/mm/yyyy')) AND df_operacion < TRUNC(TO_DATE (?,'dd/mm/yyyy')) + 1" );
				varBind = new ArrayList();
				varBind.add("54");
				varBind.add(fecha_de);
				varBind.add(fecha_a);
				
				ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
				rs = ps.executeQuery();
				if(rs.next()) {
					if(rs.getInt(1)>0){
						confirmaDL = "S";
					}
				}
			rs.close();
			ps.close();
			
			Vector registros;
			int numRegistros = 0;
			double totalImporte = 0;
			int numRegistrosDL = 0;
			double totalImporteDL = 0;
				
			HashMap datos =  new HashMap();
			JSONArray  regi  = new JSONArray();
			
			if(ic_moneda.equals("1")){
			
				for(int i=0;i<doctoMN.size();i++) {
					registros = (Vector)doctoMN.get(i);					
					numRegistros += Integer.parseInt(registros.get(2).toString());
					totalImporte += Double.parseDouble(registros.get(3).toString());
					
					datos =  new HashMap();
					datos.put("IC_IF",registros.get(0).toString());
					datos.put("NOMBRE_IF",registros.get(1).toString());
					datos.put("NUM_OPERACIONES",registros.get(2).toString());
					datos.put("MONTO",Comunes.formatoMN(registros.get(3).toString()));
					datos.put("CHECKBOX","");
					datos.put("CONFIRMA","");
					regi.add(datos);
				}
				
				datos =  new HashMap();
				datos.put("IC_IF","");
				datos.put("NOMBRE_IF","");
				datos.put("NUM_OPERACIONES","Total de Registros");
				datos.put("MONTO",String.valueOf(numRegistros));
				datos.put("CHECKBOX","");
				datos.put("CONFIRMA","");
				regi.add(datos);
				
				datos =  new HashMap();
				datos.put("IC_IF","");
				datos.put("NOMBRE_IF","");
				datos.put("NUM_OPERACIONES","Total Importe en M.N");
				datos.put("MONTO",Comunes.formatoMN(totalImporte+""));
				datos.put("CHECKBOX","");
				datos.put("CONFIRMA","");
				regi.add(datos);
				
				datos =  new HashMap();
				datos.put("IC_IF","");
				datos.put("NOMBRE_IF","");
				datos.put("NUM_OPERACIONES","Confirmaci�n de Operaciones al D�a ");
				datos.put("MONTO","");
				datos.put("CHECKBOX","SI");
				datos.put("CONFIRMA",confirmaMN);
				regi.add(datos);	
				
				
			}else  if(ic_moneda.equals("54")){
			
				for(int i=0;i<doctoDL.size();i++) {
					registros = (Vector)doctoDL.get(i);
					numRegistrosDL += Integer.parseInt(registros.get(2).toString());
					totalImporteDL += Double.parseDouble(registros.get(3).toString());
						
					datos =  new HashMap();						
					datos.put("IC_IF",registros.get(0).toString());
					datos.put("NOMBRE_IF",registros.get(1).toString());
					datos.put("NUM_OPERACIONES",registros.get(2).toString());
					datos.put("MONTO",Comunes.formatoMN(registros.get(3).toString()));
					datos.put("CHECKBOX","");
					datos.put("CONFIRMA","");
					regi.add(datos);
				}
				
				datos =  new HashMap();
				datos.put("IC_IF","");
				datos.put("NOMBRE_IF","");
				datos.put("NUM_OPERACIONES","Total de Registros");
				datos.put("MONTO",String.valueOf(numRegistrosDL));
				datos.put("CHECKBOX","");
				datos.put("CONFIRMA","");
				regi.add(datos);
				
				datos =  new HashMap();
				datos.put("IC_IF","");
				datos.put("NOMBRE_IF","");
				datos.put("NUM_OPERACIONES","Total Importe en D�lares");
				datos.put("MONTO",Comunes.formatoMN(totalImporteDL+""));
				datos.put("CHECKBOX","");
				datos.put("CONFIRMA","");
				regi.add(datos);		
				
				datos =  new HashMap();
				datos.put("IC_IF","");
				datos.put("NOMBRE_IF","");
				datos.put("NUM_OPERACIONES","Confirmaci�n de Operaciones al D�a ");
				datos.put("MONTO"," ");
				datos.put("CHECKBOX","SI");
				datos.put("CONFIRMA",confirmaDL);
				regi.add(datos);	
				
			}			
			String consulta =  "{\"success\": true, \"total\": \"" + regi.size() + "\", \"registros\": " + regi.toString()+"}";
			respuesta.add(consulta);
			respuesta.add(String.valueOf(numRegistros));
			respuesta.add(String.valueOf(numRegistrosDL));
			
					
	} catch(Exception e) {
		transactionOk=false;	
		e.printStackTrace();
		log.error("error al consConfirmacion  " +e); 
	} finally {	
		if(con.hayConexionAbierta()) {
			con.terminaTransaccion(transactionOk);
			con.cierraConexionDB();	
		}
	}    
	log.info("consConfirmacion (S) ");
		return respuesta;
	}
	
	/**
	 * Aceptaci�n de Confirmaci�n de Operaciones del d�a
	 * @return 
	 * @param confirmaOperacionDL
	 * @param confirmaOperacionMN
	 * @param ic_ifDL
	 * @param ic_ifMN
	 * @param fecha
	 */
	public String  AceptaConfirmacion( String fecha , String ic_ifMN [],  String ic_ifDL [],
			String numRegistrosMN , String numRegistrosDL, String confirmaOperacionMN, String confirmaOperacionDL ){
		log.info("AceptaConfirmacion (E)");
		
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs	= null;
		boolean transactionOk = true;
		
		StringBuffer qrySentencia = new StringBuffer("");		
		List varBind = new ArrayList();	
		String mensaje ="";
		
		try{	
			con.conexionDB();	
		
			String ic_if = "";
			
			if(Integer.parseInt(numRegistrosMN) >0) {
				for(int i=0;i<ic_ifMN.length;i++) {
					ic_if = ic_ifMN[i];
					
					qrySentencia = new StringBuffer("");	
					qrySentencia.append( 
						" SELECT COUNT (1)"   +
						"   FROM comrel_constancia_deposito"   +
						"  WHERE ic_if = ?"   +
						"    AND ic_moneda = ?"   +
						"    AND TRUNC (df_operacion) = TO_DATE (?, 'dd/mm/yyyy')");
			 
					varBind = new ArrayList();
					varBind.add(ic_if);
					varBind.add("1");					
					varBind.add(fecha);
					ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
					rs = ps.executeQuery();
					int existe = 0;
					if(rs.next()){
						existe = rs.getInt(1);
					}	
					rs.close();
					ps.close();
				
					if(existe==0) {
						qrySentencia = new StringBuffer("");	
						qrySentencia.append( 
						" INSERT INTO comrel_constancia_deposito"   +
						"             (ic_if, df_operacion, ic_moneda, cs_confirmacion)"   +
						"      VALUES(?, TO_DATE (?, 'dd/mm/yyyy'), ? , ?)")  ;
						
						varBind = new ArrayList();
						varBind.add(ic_if);
						varBind.add(fecha);
						varBind.add("1");
						varBind.add(confirmaOperacionMN);
					} else {
					
						qrySentencia = new StringBuffer("");	
						qrySentencia.append( 
						" UPDATE comrel_constancia_deposito"   +
						"    SET cs_confirmacion = ?"   +
						"  WHERE ic_if = ?"   +
						"    AND ic_moneda = ? "   +
						"    AND TRUNC (df_operacion) = TO_DATE (?, 'dd/mm/yyyy')" );
				
						varBind = new ArrayList();
						varBind.add( confirmaOperacionMN);
						varBind.add(ic_if);
						varBind.add("1");
						varBind.add(fecha);
					}
					ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
					ps.executeUpdate();
					ps.close();
				}//for(int i=0;i<ic_if_mn.length;i++)
			}
			
		if(Integer.parseInt(numRegistrosDL) >0) {
				for(int i=0;i<ic_ifDL.length;i++) {
					ic_if = ic_ifDL[i];
					
					qrySentencia = new StringBuffer("");	
					qrySentencia.append( 
						" SELECT COUNT (1)"   +
						"   FROM comrel_constancia_deposito"   +
						"  WHERE ic_if = ?"   +
						"    AND ic_moneda = ? "   +
						"    AND TRUNC (df_operacion) = TO_DATE (?, 'dd/mm/yyyy')");
			
					varBind = new ArrayList();
					varBind.add(ic_if);
					varBind.add("54");
					varBind.add(fecha);
					ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
					rs = ps.executeQuery();
					int existe = 0;
					if(rs.next()){
						existe = rs.getInt(1);
					}
					rs.close();
					ps.close();
					if(existe==0) {
					
						qrySentencia = new StringBuffer("");	
						qrySentencia.append(  
							" INSERT INTO comrel_constancia_deposito"   +
							"             (ic_if, df_operacion, ic_moneda, cs_confirmacion)"   +
							"      VALUES(?, TO_DATE (?, 'dd/mm/yyyy'), ?, ?)")   ;					
						
						varBind = new ArrayList();
						varBind.add(ic_if);						
						varBind.add(fecha);
						varBind.add("54");						
						varBind.add( confirmaOperacionDL);
						
					} else {
						qrySentencia = new StringBuffer("");	
						qrySentencia.append(
							" UPDATE comrel_constancia_deposito"   +
							"    SET cs_confirmacion = ?"   +
							"  WHERE ic_if = ?"   +
							"    AND ic_moneda = ? "   +
							"    AND TRUNC (df_operacion) = TO_DATE (?, 'dd/mm/yyyy')")  ;
							
							varBind = new ArrayList();
							varBind.add( confirmaOperacionDL);
							varBind.add(ic_if);
							varBind.add("54");
							varBind.add(fecha);
					}					
					ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
					ps.executeUpdate();
					ps.close();	
				}//for(int i=0;i<ic_if_mn.length;i++)
			}
			mensaje = "Los cambios se almacenaron satisfactoriamente";
			con.terminaTransaccion(true);
				
		} catch(Exception e) {
			transactionOk=false;	
			e.printStackTrace();
			log.error("error al AceptaConfirmacion  " +e); 
		} finally {	
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();	
			}
		}    
		log.info("AceptaConfirmacion (S) ");
		return mensaje;
	}
	

	public String getMensajeEncabezado() {
		return mensajeEncabezado;
	}

	public void setMensajeEncabezado(String mensajeEncabezado) {
		this.mensajeEncabezado = mensajeEncabezado;
	}



	public List getConditions() {  return conditions;  }  

	public String getIc_producto() {
		return ic_producto;
	}

	public void setIc_producto(String ic_producto) {
		this.ic_producto = ic_producto;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getFecha_de() {
		return fecha_de;
	}

	public void setFecha_de(String fecha_de) {
		this.fecha_de = fecha_de;
	}

	public String getFecha_a() {
		return fecha_a;
	}

	public void setFecha_a(String fecha_a) {
		this.fecha_a = fecha_a;
	}

	public String getTipo_piso() {
		return tipo_piso;
	}

	public void setTipo_piso(String tipo_piso) {
		this.tipo_piso = tipo_piso;
	}

	public String getNombreIF() {
		return nombreIF;
	}

	public void setNombreIF(String nombreIF) {
		this.nombreIF = nombreIF;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public String getNombre_if_p() {
		return nombre_if_p;
	}

	public void setNombre_if_p(String nombre_if_p) {
		this.nombre_if_p = nombre_if_p;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getTotal2() {
		return total2;
	}

	public void setTotal2(String total2) {
		this.total2 = total2;
	}

	public String getNombre_if_e() {
		return nombre_if_e;
	}

	public void setNombre_if_e(String nombre_if_e) {
		this.nombre_if_e = nombre_if_e;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getMensajeA() {
		return mensajeA;
	}

	public void setMensajeA(String mensajeA) {
		this.mensajeA = mensajeA;
	}

	public String getMensajeB() {
		return mensajeB;
	}

	public void setMensajeB(String mensajeB) {
		this.mensajeB = mensajeB;
	}

	public String getMensajeC() {
		return mensajeC;
	}

	public void setMensajeC(String mensajeC) {
		this.mensajeC = mensajeC;
	}

	public String getFechaPeriodo() {
		return fechaPeriodo;
	}

	public void setFechaPeriodo(String fechaPeriodo) {
		this.fechaPeriodo = fechaPeriodo;
	}

	public String getMensajeD() {
		return mensajeD;
	}

	public void setMensajeD(String mensajeD) {
		this.mensajeD = mensajeD;
	}

	public String getFechaConvenio() {
		return fechaConvenio; 
	}

	public void setFechaConvenio(String fechaConvenio) {
		this.fechaConvenio = fechaConvenio;
	}


}