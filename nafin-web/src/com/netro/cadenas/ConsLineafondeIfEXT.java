package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsLineafondeIfEXT implements IQueryGeneratorRegExtJS {
	
	private ArrayList valoresBind = new ArrayList();
	private String ic_if;
	private int numList = 1;
	private static Log log = ServiceLocator.getInstance().getLog(com.netro.cadenas.ConsLineafondeIfEXT.class);
	
public ConsLineafondeIfEXT() { }
		 
	 /**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentQuery(){
	log.debug("getDocumentQuery (E) ");
	StringBuffer strQuery 	= new StringBuffer();

	strQuery.append(
	" SELECT i.ic_if AS ic_if" 				+
	"   FROM lc_lineas_fondeo_inter lc,"   +
	"        mg_financieras fin,"  			+
	"        comcat_if i,"   					+
	"        comcat_financiera f"   			+
	"  WHERE fecha_de_vcmto > ="   			+
	"        TO_DATE (TO_CHAR (SYSDATE, 'ddmmyyyy'), 'ddmmyyyy')"   +
	"    AND lc.codigo_financiera = fin.codigo_financiera"   +
	"    AND lc.tipo_financiera = fin.tipo_financiera"   +
	"    AND lc.codigo_estado = 1"   						  +
	"    AND fin.codigo_financiera = i.ic_financiera"    +
	"    AND fin.tipo_financiera = f.ic_tipo_financiera" +
	"    AND i.ic_financiera = f.ic_financiera" 			  );
						
	if(!ic_if.equals("0")){
		strQuery.append(" AND i.ic_if = ? ");
		valoresBind.add(new Integer(ic_if));
	}
	strQuery.append(" ORDER BY nombre ");		
	
	log.debug(" strQuery identificadores unicos - - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.debug("getDocumentQuery (S) ");
		
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * datos a mostrar en la consulta basandose en los identificadores unicos 
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista 
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentSummaryQueryForIds(List ids){
	
	log.info("getDocumentSummaryQueryForIds(E)");
	StringBuffer strQuery 	= new StringBuffer();
	List 		conditions= new ArrayList();		
		
	strQuery.append(
		" SELECT i.ic_if AS ic_if, nombre, numero_linea_fondeo,"   				+
		"        lc.codigo_financiera AS codigo_financiera, descripcion,"   	+
		"        lc.codigo_agencia as codigo_agencia, lc.codigo_sub_aplicacion as codigo_sub_aplicacion,"   +
		"        lc.tipo_financiera AS tipo_financiera, monto_asignado,"   	+
		"        monto_utilizado, monto_comprometido,"   							+
		"        (  monto_asignado"   													+
		"        - (NVL (monto_utilizado, 0) + NVL (monto_comprometido, 0))" +
		"        ) AS disponible,"   														+
		"        lc.adicionado_por AS adicionado_por,"  							+
		"        TO_CHAR (lc.fecha_adicion, 'dd/mm/yyyy hh24:mm')   AS fecha_adicion,"   		+
		"        lc.modificado_por AS modificado_por,"   													+
		"        TO_CHAR (lc.fecha_modificacion, 'dd/mm/yyyy hh24:mm') AS fecha_modificacion"  +
		" 	FROM lc_lineas_fondeo_inter lc,"		+
      "		  mg_financieras fin,"				+
      "		  comcat_if i,"						+
      "		  comcat_financiera f"				+
		" WHERE fecha_de_vcmto > ="				+
      " 		  TO_DATE (TO_CHAR (SYSDATE, 'ddmmyyyy'), 'ddmmyyyy')"+
      " 	 AND lc.codigo_financiera = fin.codigo_financiera"			+
      "	 AND lc.tipo_financiera = fin.tipo_financiera"				+
      "   AND lc.codigo_estado = 1"											+
      "   AND fin.codigo_financiera = i.ic_financiera"				+
      "   AND fin.tipo_financiera = f.ic_tipo_financiera"			+
      "   AND i.ic_financiera = f.ic_financiera "						);
	  
	 if(!ic_if.equals("0")){
		strQuery.append(" AND i.ic_if = ? ");
		valoresBind.add(ic_if);
			
		List pKeys = new ArrayList();
		for(int i = 0; i < ids.size(); i++){
			List lItem = (ArrayList)ids.get(i);
			if(numList ==1){				
				conditions.add(lItem.get(i));				
				valoresBind=(ArrayList)conditions;
				numList++;
			}
			pKeys.add(ids.get(i));			
		}		
	 strQuery.append(" ORDER BY nombre ");
	 }
	
	log.info("  strQuery basado en identificadores unicos - -  >" + strQuery);
	log.info("valoresBind "+ valoresBind);
	log.info("getDocumentSummaryQueryForIds(S)");	
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getAggregateCalculationQuery(){
	StringBuffer strQuery 	= new StringBuffer();
	log.info("getAggregateCalculationQuery (E)");
	
	strQuery.append(
	" SELECT   i.ic_if AS ic_if, nombre, numero_linea_fondeo,"   				+
	"          lc.codigo_financiera AS codigo_financiera, descripcion,"   	+
	"          lc.codigo_agencia as codigo_agencia, lc.codigo_sub_aplicacion as codigo_sub_aplicacion,"   +
	"          lc.tipo_financiera AS tipo_financiera, monto_asignado,"   	+
	"          monto_utilizado, monto_comprometido,"   							+
	"          (  monto_asignado"   														+
	"           - (NVL (monto_utilizado, 0) + NVL (monto_comprometido, 0))" +
	"          ) AS disponible,"   														+
	"          lc.adicionado_por AS adicionado_por,"   							+
	"          TO_CHAR (lc.fecha_adicion, 'dd/mm/yyyy hh24:mm')   AS fecha_adicion,"   			+
	"          lc.modificado_por AS modificado_por,"   							+
	"          TO_CHAR (lc.fecha_modificacion, 'dd/mm/yyyy hh24:mm') AS fecha_modificacion"   +
	"     FROM lc_lineas_fondeo_inter lc,"   	+
	"          mg_financieras fin,"				+
	"          comcat_if i,"   					+
	"          comcat_financiera f"   			+
	"    WHERE fecha_de_vcmto > ="   			+
	"          TO_DATE (TO_CHAR (SYSDATE, 'ddmmyyyy'), 'ddmmyyyy')"   +
	"      AND lc.codigo_financiera = fin.codigo_financiera"  	+
	"      AND lc.tipo_financiera = fin.tipo_financiera"   		+
	"      AND lc.codigo_estado = 1"   									+
	"      AND fin.codigo_financiera = i.ic_financiera"   		+
	"      AND fin.tipo_financiera = f.ic_tipo_financiera"   	+
	"      AND i.ic_financiera = f.ic_financiera" 					);
						
	if(!ic_if.equals("0")){
		strQuery.append(" AND i.ic_if = ? ");
	}
	strQuery.append(" ORDER BY nombre ");
						
	log.debug(" strQuery obtiene totales- - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.info("getAggregateCalculationQuery (S)");
	return strQuery.toString();
	}
	
	/**
	 * Este m�todo debe regresar una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
public  List getConditions(){		
	return valoresBind;
	}
	
	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
public  String getDocumentQueryFile(){
	log.debug("getDocumentQueryFile (E)");
	StringBuffer strQuery 	= new StringBuffer();
		
	strQuery.append(
	" SELECT   i.ic_if AS ic_if, nombre, numero_linea_fondeo,"   				+
	"          lc.codigo_financiera AS codigo_financiera, descripcion,"   	+
	"          lc.codigo_agencia as codigo_agencia, lc.codigo_sub_aplicacion as codigo_sub_aplicacion,"   +
	"          lc.tipo_financiera AS tipo_financiera, monto_asignado,"   	+
	"          monto_utilizado, monto_comprometido,"   +
	"          (  monto_asignado"   	+
	"           - (NVL (monto_utilizado, 0) + NVL (monto_comprometido, 0))" +
	"          ) AS disponible,"   	+
	"          lc.adicionado_por AS adicionado_por,"   +
	"          TO_CHAR (lc.fecha_adicion, 'dd/mm/yyyy hh24:mm')   AS fecha_adicion,"   			+
	"          lc.modificado_por AS modificado_por,"   +
	"          TO_CHAR (lc.fecha_modificacion, 'dd/mm/yyyy hh24:mm') AS fecha_modificacion"   +
	"     FROM lc_lineas_fondeo_inter lc,"   	+
	"          mg_financieras fin,"   			+
	"          comcat_if i,"   					+
	"          comcat_financiera f"   			+
	"    WHERE fecha_de_vcmto > ="   			+
	"           TO_DATE (TO_CHAR (SYSDATE, 'ddmmyyyy'), 'ddmmyyyy')"  +
	"      AND lc.codigo_financiera = fin.codigo_financiera"   			+
	"      AND lc.tipo_financiera = fin.tipo_financiera"   				+
	"      AND lc.codigo_estado = 1"   											+
	"      AND fin.codigo_financiera = i.ic_financiera"   				+
	"      AND fin.tipo_financiera = f.ic_tipo_financiera"   			+
	"      AND i.ic_financiera = f.ic_financiera" );
	
	if(!ic_if.equals("0")){
		strQuery.append(" AND i.ic_if = ? ");
		valoresBind.add(new Integer(ic_if));
	}
	strQuery.append(" ORDER BY nombre ");
					
	log.debug(" strQuery para generar archivo- - ->"+ strQuery);
	log.debug(" valoresBind - - ->"+ valoresBind);
	log.debug("getDocumentQueryFile (S)<- - - ");
		
	return strQuery.toString();
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
public  String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		
	log.debug("crearCustomFile (E)");
	String nombreArchivo ="";
	HttpSession session = request.getSession();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	OutputStreamWriter writer = null;
	BufferedWriter buffer = null;
	int total = 0;
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		buffer = new BufferedWriter(writer);
			
		contenidoArchivo = new StringBuffer();	
		contenidoArchivo.append( "Nombre IF ,N�mero L�nea Fondeo,C�digo Financiera,Descripci�n de l�nea de cr�dito,C�digo agencia,C�digo Subaplicaci�n,Monto asignado, Monto utilizado, Monto Comprometido, Disponible, Adicionado por, Fecha adici�n, Modificado por, Fecha modificaci�n  \n");
			
		while (rs.next())	{
			String nombreif 	= (rs.getString("NOMBRE") 						== null) ? "" : rs.getString("NOMBRE");
			String numlinfod 	= (rs.getString("NUMERO_LINEA_FONDEO") 	== null) ? "" : rs.getString("NUMERO_LINEA_FONDEO");
			String codfin 		= (rs.getString("CODIGO_FINANCIERA") 		== null) ? "" : rs.getString("CODIGO_FINANCIERA");				
			String desclincre = (rs.getString("TIPO_FINANCIERA") 			== null) ? "" : rs.getString("TIPO_FINANCIERA");
			String codagen 	= (rs.getString("CODIGO_AGENCIA") 			== null) ? "" : rs.getString("CODIGO_AGENCIA");				
			String codsubapli = (rs.getString("CODIGO_SUB_APLICACION") 	== null) ? "" : rs.getString("CODIGO_SUB_APLICACION");
			String montasig 	= (rs.getString("MONTO_ASIGNADO") 			== null) ? "" : rs.getString("MONTO_ASIGNADO");
			String montutil 	= (rs.getString("MONTO_UTILIZADO") 			== null) ? "" : rs.getString("MONTO_UTILIZADO");
			String montcom 	= (rs.getString("MONTO_COMPROMETIDO") 		== null) ? "" : rs.getString("MONTO_COMPROMETIDO");
			String disp 		= (rs.getString("DISPONIBLE") 				== null) ? "" : rs.getString("DISPONIBLE");
			String adpor 		= (rs.getString("ADICIONADO_POR") 			== null) ? "" : rs.getString("ADICIONADO_POR");
			String fecad 		= (rs.getString("FECHA_ADICION") 			== null) ? "" : rs.getString("FECHA_ADICION");
			String modpor 		= (rs.getString("MODIFICADO_POR") 			== null) ? "" : rs.getString("MODIFICADO_POR");
			String fecmod 		= (rs.getString("FECHA_MODIFICACION") 		== null) ? "" : rs.getString("FECHA_MODIFICACION");
				
			contenidoArchivo.append(
			nombreif.replace	(',',' ')+","+
			numlinfod.replace	(',',' ')+","+		
			codfin.replace		(',',' ')+","+
			desclincre.replace(',',' ')+","+
			codagen.replace	(',',' ')+","+
			codsubapli.replace(',',' ')+","+
			montasig.replace	(',',' ')+","+
			montutil.replace	(',',' ')+","+
			montcom.replace	(',',' ')+","+
			disp.replace		(',',' ')+","+
			adpor.replace		(',',' ')+","+
			fecad.replace		(',',' ')+","+
			modpor.replace		(',',' ')+","+
			fecmod.replace		(',',' ')+","+"\n");
		}
			
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
		
		}catch (Exception e){
			throw new AppException("Error al generar el archivo ", e);
		}
	log.debug("crearCustomFile (S)");
	return nombreArchivo;
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
	String nombreArchivo = "";
	HttpSession session = request.getSession();	
	ComunesPDF pdfDoc = new ComunesPDF();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
		pdfDoc.setTable(14,90);
			
		pdfDoc.setCell("Nombre IF"								,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("N�mero L�nea Fondeo"				,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("C�digo Financiera"					,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Descripci�n de l�nea de cr�dito","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("C�digo agencia"						,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("C�digo Subaplicaci�n"				,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto asignado"						,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto utilizado"						,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto Comprometido"					,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Disponible"							,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Adicionado por"						,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha adici�n"						,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Modificado por"						,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha modificaci�n"					,"celda01",ComunesPDF.CENTER);
			
		while(reg.next()){
			String nombre 		= (reg.getString("NOMBRE") 					== null) ? "" : reg.getString("NOMBRE");
			String numlinfon 	= (reg.getString("NUMERO_LINEA_FONDEO") 	== null) ? "" : reg.getString("NUMERO_LINEA_FONDEO");
			String codfin 		= (reg.getString("CODIGO_FINANCIERA") 		== null) ? "" : reg.getString("CODIGO_FINANCIERA");
			String desclincre = (reg.getString("TIPO_FINANCIERA") 		== null) ? "" : reg.getString("TIPO_FINANCIERA");
			String codage 		= (reg.getString("CODIGO_AGENCIA") 			== null) ? "" : reg.getString("CODIGO_AGENCIA");
			String codsub 		= (reg.getString("CODIGO_SUB_APLICACION") == null) ? "" : reg.getString("CODIGO_SUB_APLICACION");
			String montas 		= (reg.getString("MONTO_ASIGNADO") 			== null) ? "" : reg.getString("MONTO_ASIGNADO");
			String montut 		= (reg.getString("MONTO_UTILIZADO") 		== null) ? "" : reg.getString("MONTO_UTILIZADO");
			String montco 		= (reg.getString("MONTO_COMPROMETIDO") 	== null) ? "" : reg.getString("MONTO_COMPROMETIDO");
			String disp 		= (reg.getString("DISPONIBLE") 				== null) ? "" : reg.getString("DISPONIBLE");
			String adpor 		= (reg.getString("ADICIONADO_POR") 			== null) ? "" : reg.getString("ADICIONADO_POR");
			String fecad 		= (reg.getString("FECHA_ADICION") 			== null) ? "" : reg.getString("FECHA_ADICION");
			String modpor 		= (reg.getString("MODIFICADO_POR") 			== null) ? "" : reg.getString("MODIFICADO_POR");
			String fecmod 		= (reg.getString("FECHA_MODIFICACION") 	== null) ? "" : reg.getString("FECHA_MODIFICACION");

			pdfDoc.setCell(nombre		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(numlinfon	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(codfin		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(desclincre	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(codage		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(codsub		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal( montas,2,true),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal( montut,2,true),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(montco		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal( disp,2,true)	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(adpor			,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fecad			,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(modpor		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fecmod		,"formas",ComunesPDF.CENTER);
		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
	} catch(Exception e){
		throw new AppException("Error al generar el archivo PDF ", e);
	}
	return nombreArchivo;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public String getIc_if() {
		return ic_if;
	}
	

}