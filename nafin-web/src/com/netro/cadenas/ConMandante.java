package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConMandante implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConMandante.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String txtNumElectronico;
	private String txtNombre;
	private String sel_edo;
	private String txtRFC;
	private String sel_EPO;
	
		 
	public ConMandante() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
		
		qrySentencia.append("Select  MAN.IC_MANDANTE as IC_MANDANTE ,   EPO.IC_EPO as IC_EPO    "+		
		
		
		" FROM comrel_nafin RN, COMCAT_MANDANTE MAN, "+
		" comcat_estado ES, comcat_epo EPO, COMCAT_BANCO_FONDEO ban, COMREL_MANDANTE_EPO rel  "+
		
		" WHERE MAN.IC_MANDANTE = RN.IC_EPO_PYME_IF "+
		" AND  MAN.IC_ESTADO = ES.IC_ESTADO "+
		" AND rel.IC_EPO = EPO.IC_EPO "+
		" AND ban.IC_BANCO_FONDEO = epo.IC_BANCO_FONDEO "+
		" and MAN.IC_MANDANTE = rel.IC_MANDANTE "+
		" AND RN.CG_TIPO = 'M'  ");
		
		 if (!"".equals(txtNumElectronico) )  {	
			qrySentencia.append("  AND rn.ic_nafin_electronico = ?  ") ;
			conditions.add(txtNumElectronico);
		}
	
		if (!"".equals(txtRFC) )  {	
			qrySentencia.append("  AND  MAN.CG_RFC = ? ") ;
			conditions.add(txtRFC);
		}
			
		if (!"".equals(txtNombre) )  {
			qrySentencia.append("  AND ( UPPER(MAN.CG_NOMBRE) LIKE ? OR UPPER(MAN.CG_RAZON_SOCIAL) LIKE ?  ) ") ;			
			conditions.add(txtNombre+"%");
			conditions.add(txtNombre+"%");				
		}
	
		if (!"".equals(sel_edo) )  {
			qrySentencia.append(" AND es.IC_ESTADO = ? ") ;
			conditions.add(sel_edo);
		}
		
		if (!"".equals(sel_EPO) )  {
			qrySentencia.append("AND epo.IC_EPO = ? ") ;
			conditions.add(sel_EPO);
		}
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		qrySentencia.append("Select MAN.IC_MANDANTE as IC_MANDANTE,   "+
		" epo.CG_RAZON_SOCIAL as  CADENA_PRODUCTIVA, "+
		" RN.ic_nafin_electronico  as NUM_NAFIN_ELEC, "+
		" man.cg_razon_social ||' '||   MAN.CG_NOMBRE  ||' '||   MAN.CG_APPAT ||' '||  MAN.CG_APMAT    as RAZON_SOCIAL , "+
		" MAN.CG_RFC as RFC,  "+
		" MAN.CG_CALLE  ||' '||   MAN.CG_COLONIA  ||' '||   MAN.IC_DELEGACION  AS DOMICILIO, "+
		" ES.CD_NOMBRE as ESTADO,  "+
		" MAN.CG_TELEFONO as TELEFONO , "+
		" MAN.CS_TIPO_PERSONA AS TIPO_PERSONA "+
		
		" FROM comrel_nafin RN, COMCAT_MANDANTE MAN, "+
		" comcat_estado ES, comcat_epo EPO, COMCAT_BANCO_FONDEO ban, COMREL_MANDANTE_EPO rel  "+
		" WHERE MAN.IC_MANDANTE = RN.IC_EPO_PYME_IF "+
		" AND  MAN.IC_ESTADO = ES.IC_ESTADO "+
		" AND rel.IC_EPO = EPO.IC_EPO "+
		" AND ban.IC_BANCO_FONDEO = epo.IC_BANCO_FONDEO "+
		" and MAN.IC_MANDANTE = rel.IC_MANDANTE "+
		" AND RN.CG_TIPO = 'M'  ");
		
		
		 if (!"".equals(txtNumElectronico) )  {	
			qrySentencia.append("  AND rn.ic_nafin_electronico = ?  ") ;
			conditions.add(txtNumElectronico);
		}
	
		if (!"".equals(txtRFC) )  {	
			qrySentencia.append("  AND  MAN.CG_RFC = ? ") ;
			conditions.add(txtRFC);
		}
			
		if (!"".equals(txtNombre) )  {
			qrySentencia.append("  AND ( UPPER(MAN.CG_NOMBRE) LIKE ? OR UPPER(MAN.CG_RAZON_SOCIAL) LIKE ?  ) ") ;			
			conditions.add(txtNombre+"%");
			conditions.add(txtNombre+"%");				
		}
	
		if (!"".equals(sel_edo) )  {
			qrySentencia.append(" AND es.IC_ESTADO = ? ") ;
			conditions.add(sel_edo);
		}
		
		if (!"".equals(sel_EPO) )  {
			qrySentencia.append("AND epo.IC_EPO = ? ") ;
			conditions.add(sel_EPO);
		}
			
		qrySentencia.append(" AND (");
			
			for (int i = 0; i < pageIds.size(); i++) { 
				List lItem = (ArrayList)pageIds.get(i);
				
				if(i > 0){qrySentencia.append("  OR  ");}
				
				qrySentencia.append(" MAN.IC_MANDANTE =  ?  AND     epo.IC_EPO =  ?   " );
				conditions.add(new Long(lItem.get(0).toString()));
				conditions.add(new Long(lItem.get(1).toString()));
			}
			
			qrySentencia.append(" ) ");
			
			qrySentencia.append(" order by  MAN.IC_MANDANTE desc  ");
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
	
		qrySentencia.append("Select MAN.IC_MANDANTE as IC_MANDANTE,   "+
		" epo.CG_RAZON_SOCIAL as  CADENA_PRODUCTIVA, "+
		" RN.ic_nafin_electronico  as NUM_NAFIN_ELEC, "+
		" MAN.CG_NOMBRE  ||' '||   MAN.CG_APPAT ||' '||  MAN.CG_APMAT    as RAZON_SOCIAL , "+
		" MAN.CG_RFC as RFC,  "+
		" MAN.CG_CALLE  ||' '||   MAN.CG_COLONIA  ||' '||   MAN.IC_DELEGACION  AS DOMICILIO, "+
		" ES.CD_NOMBRE as ESTADO,  "+
		" MAN.CG_TELEFONO as TELEFONO , "+
		" MAN.CS_TIPO_PERSONA AS TIPO_PERSONA "+  
		
		" FROM comrel_nafin RN, COMCAT_MANDANTE MAN, "+
		" comcat_estado ES, comcat_epo EPO, COMCAT_BANCO_FONDEO ban, COMREL_MANDANTE_EPO rel  "+
		" WHERE MAN.IC_MANDANTE = RN.IC_EPO_PYME_IF "+
		" AND  MAN.IC_ESTADO = ES.IC_ESTADO "+
		" AND rel.IC_EPO = EPO.IC_EPO "+
		" AND ban.IC_BANCO_FONDEO = epo.IC_BANCO_FONDEO "+
		" and MAN.IC_MANDANTE = rel.IC_MANDANTE "+
		" AND RN.CG_TIPO = 'M'  ");
		
		
		 if (!"".equals(txtNumElectronico) )  {	
			qrySentencia.append("  AND rn.ic_nafin_electronico = ?  ") ;
			conditions.add(txtNumElectronico);
		}
	
		if (!"".equals(txtRFC) )  {	
			qrySentencia.append("  AND  MAN.CG_RFC = ? ") ;
			conditions.add(txtRFC);
		}
			
		if (!"".equals(txtNombre) )  {
			qrySentencia.append("  AND ( UPPER(MAN.CG_NOMBRE) LIKE ? OR UPPER(MAN.CG_RAZON_SOCIAL) LIKE ?  ) ") ;			
			conditions.add(txtNombre+"%");
			conditions.add(txtNombre+"%");				
		}
	
		if (!"".equals(sel_edo) )  {
			qrySentencia.append(" AND es.IC_ESTADO = ? ") ;
			conditions.add(sel_edo);
		}
		
		if (!"".equals(sel_EPO) )  {
			qrySentencia.append("AND epo.IC_EPO = ? ") ;
			conditions.add(sel_EPO);
		}	
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
			pdfDoc.setTable(7,100);
			pdfDoc.setCell("Cadena Productiva ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("N�mero de Nafin Electr�nico ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre o Raz�n Social ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("RFC ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Domicilio ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estado ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tel�fono ","celda01",ComunesPDF.CENTER);
			
			while (rs.next())	{					
				String cadenaProductiva = (rs.getString("CADENA_PRODUCTIVA") == null) ? "" : rs.getString("CADENA_PRODUCTIVA");
				String num_nafin = (rs.getString("NUM_NAFIN_ELEC") == null) ? "" : rs.getString("NUM_NAFIN_ELEC");
				String razon_social = (rs.getString("RAZON_SOCIAL") == null) ? "" : rs.getString("RAZON_SOCIAL");
				String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				String domicilio = (rs.getString("DOMICILIO") == null) ? "" : rs.getString("DOMICILIO");
				String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
								
				pdfDoc.setCell(cadenaProductiva,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(num_nafin,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(razon_social,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(rfc,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(domicilio,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(estado,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);				
			}
		
			pdfDoc.addTable();
			pdfDoc.endDocument();	
				
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();		
			contenidoArchivo.append(" Cadena Productiva,  N�mero de Nafin Electr�nico,  Nombre o Razon Social,  RFC , Domicilio,  Estado,  Tel�fono   \n");			
		
			while (rs.next())	{	
			
				String cadenaProductiva = (rs.getString("CADENA_PRODUCTIVA") == null) ? "" : rs.getString("CADENA_PRODUCTIVA");
				String num_nafin = (rs.getString("NUM_NAFIN_ELEC") == null) ? "" : rs.getString("NUM_NAFIN_ELEC");
				String razon_social = (rs.getString("RAZON_SOCIAL") == null) ? "" : rs.getString("RAZON_SOCIAL");
				String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				String domicilio = (rs.getString("DOMICILIO") == null) ? "" : rs.getString("DOMICILIO");
				String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				
				contenidoArchivo.append(cadenaProductiva.replace(',',' ')+", "+
											num_nafin.replace(',',' ')+", "+
											razon_social.replace(',',' ')+", "+
											rfc.replace(',',' ')+", "+
											domicilio.replace(',',' ')+", "+
											estado.replace(',',' ')+", "+
											telefono.replace(',',' ')+"\n");		
			
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	public String imprimirCuentas(HttpServletRequest request, List cuentas, String tituloF,  String tituloC, String path ) {  
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		try {
				
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText(tituloF,"formas",ComunesPDF.CENTER);
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			
			pdfDoc.setTable(1,30);			
			pdfDoc.setCell(tituloC, "formas",ComunesPDF.LEFT);  
			pdfDoc.setCell("Login del Usuario: ", "celda02",ComunesPDF.CENTER);	
			  
			
			Iterator itCuentas = cuentas.iterator();	
			while (itCuentas.hasNext()) {
				String cuenta = (String) itCuentas.next();		
				pdfDoc.setCell(cuenta, "formas",ComunesPDF.CENTER);	
			}	
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
						
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	/**
	 * Metodo para obtener las EPOs con las que no esta afiliado el Mandante
	 * @return 
	 * @param ic_mandante
	 */
	public List  getCatalogoEPO(String  ic_mandante ){
		log.info("getCatalogoEPO (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
					
			String 	strSQL =	"select ic_epo, cg_razon_social "+
							" FROM comcat_epo "+
							" WHERE ic_epo NOT IN "+
							" ( SELECT CG_CADENAS_PRO "+
							" FROM COMCAT_MANDANTE  "+
							" WHERE IC_MANDANTE = " + ic_mandante + " ) "+
							" ORDER BY cg_razon_social";
	
			rs = con.queryDB(strSQL);	
			
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString(1) );
				datos.put("descripcion", rs.getString(2));
				registros.add(datos);
			}
			rs.close();
			con.cierraStatement();
		
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getCatalogoEPO (S) ");
		return registros;
	}
	
	/**
	 * metodo para obtener las Epos que se reafilian
	 * @return 
	 * @param parametros
	 */
	public List  getConsultaReafiliacion(List  parametros ){
		log.info("getConsultaReafiliacion (E) ");
		AccesoDB con = new AccesoDB(); 
	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		
		StringBuffer 	SQL = new StringBuffer();    
		List varBind  = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			con.conexionDB();			
			
			 String nafinElectronico=  parametros.get(0).toString();
			 String ic_mandante=  parametros.get(1).toString();
			 String razonSocial=  parametros.get(2).toString();
			 String rfc=  parametros.get(3).toString();
			 String clavesEpos=  parametros.get(4).toString();
			 
			SQL.append(" SELECT ic_epo, cg_razon_social   " +
				" FROM comcat_epo " +				
				" WHERE ic_epo IN ("+clavesEpos+ ")" );
			
			log.debug("SQL.toString() "+SQL.toString());
			
			ps = con.queryPrecompilado(SQL.toString());
			rs = ps.executeQuery();
		
         while ( rs.next() ){		
				
				String ic_epo =  rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
				String cadena_productiva=  rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social");
				
				datos =  new HashMap();				
				datos.put("IC_EPO", ic_epo);
				datos.put("CADENA_PRODUCTIVA", cadena_productiva);
				datos.put("NAFIN_ELECTRONICO", nafinElectronico);
				datos.put("IC_MANDANTE", ic_mandante);
				datos.put("RAZON_SOCIAL", razonSocial);
				datos.put("RFC", rfc);
				registros.add(datos);
         }
			
			rs.close();
			ps.close();		
			
			
				
		} catch (Exception e) {
			log.error("getConsultaReafiliacion  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getConsultaReafiliacion (S) ");
		return registros;
	}
	
	
	/**  
	 * Obtienen  los datos del mandate para modificarse 
	 * @return 
	 * @param ic_mandante
	 */
	
	public HashMap  getConsultaMandante(String   ic_mandante ){
		log.info("getConsultaMandante (E) ");
		AccesoDB con = new AccesoDB(); 
	
		HashMap datos =  new HashMap();
		
		StringBuffer 	SQL = new StringBuffer();    
		List varBind  = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{  
			con.conexionDB();			  
			
						 
			SQL.append(" SELECT  pa.CG_RAZON_SOCIAL  AS Razon_Social, pa.CG_APPAT AS Apellido_paterno, "+
										   " pa.CG_APMAT  AS  Apellido_materno, pa.CG_NOMBRE  AS Nombre, "+
											" pa.CG_RFC  AS R_F_C , pa.CG_CALLE   AS Calle, "+
											" pa.CG_COLONIA  AS Colonia, pa.IC_PAIS_ORIGEN AS Pais, "+
											" pa.IC_ESTADO AS Estado1, "+ 
											"  pa.IC_DELEGACION  AS Delegacion_o_municipio, "+
											" pa.CG_CODIGOPOSTAL AS Codigo_postal, pa.CG_TELEFONO  AS Telefono,  "+
											" pa.CG_FAX AS Fax, pa.CG_EMAIL AS Email, "+
											" pa.CG_APPAT_CONT AS Apellido_paterno_C, pa.CG_APMAT_CONT  AS Apellido_materno_C, "+
											" pa.CG_NOMBRE_CONT  AS Nombre_C, pa.CG_TELEFONO_CONT  AS Telefono_C,  "+
											" pa.cG_CELULAR  AS numeroCelular, pa.CG_FAX_CONT  AS Fax_C, "+
											" pa.CG_EMAIL_CONT AS Email_C, pa.CG_EMAIL_CONF AS Confirmacion_Email_C,  "+
											" pa.CG_CADENAS_PRO AS cboEPO, pa.CG_NO_MANDANTE AS Numero_de_cliente,  "+
											" TO_CHAR(pa.CG_FECHA_NACI, 'DD/MM/YYYY ') as  DF_Nacimiento, "+
											" epo.cg_nombre_comercial as Comercial "+											
											" FROM  COMCAT_MANDANTE pa , comcat_epo EPO, COMCAT_ESTADO es,  COMCAT_MUNICIPIO mu  "+
											" WHERE pa.CG_CADENAS_PRO = EPO.IC_EPO  "+
											" and es.IC_ESTADO = pa.IC_ESTADO " +
											" and mu.IC_MUNICIPIO = pa.IC_DELEGACION "+
											" and es.IC_ESTADO = mu.IC_ESTADO "+
											" and pa.IC_MANDANTE = "+ic_mandante);
			
			log.debug("SQL.toString() "+SQL.toString());
			
			ps = con.queryPrecompilado(SQL.toString());
			rs = ps.executeQuery();
		
         while ( rs.next() ){		
				String  	Razon_Social = (rs.getString("Razon_Social")==null)?"":rs.getString("Razon_Social");
				String	Apellido_paterno = (rs.getString("Apellido_paterno")==null)?"":rs.getString("Apellido_paterno");
				String	Apellido_materno = (rs.getString("Apellido_materno")==null)?"":rs.getString("Apellido_materno");
				String	Nombre = (rs.getString("Nombre")==null)?"":rs.getString("Nombre");
				String	R_F_C = (rs.getString("R_F_C")==null)?"":rs.getString("R_F_C");
				String	Calle = (rs.getString("Calle")==null)?"":rs.getString("Calle");
				String   Colonia = (rs.getString("Colonia")==null)?"":rs.getString("Colonia");
				String   Pais = (rs.getString("Pais")==null)?"":rs.getString("Pais");
				String   Estado1 = (rs.getString("Estado1")==null)?"":rs.getString("Estado1");
				String   Delegacion_o_municipio = (rs.getString("Delegacion_o_municipio")==null)?"":rs.getString("Delegacion_o_municipio");
				String   Codigo_postal = (rs.getString("Codigo_postal")==null)?"":rs.getString("Codigo_postal");
				String   Telefono = (rs.getString("Telefono")==null)?"":rs.getString("Telefono");
				String   Fax = (rs.getString("Fax")==null)?"":rs.getString("Fax");
				String   Email = (rs.getString("Email")==null)?"":rs.getString("Email");
				String   Apellido_paterno_C = (rs.getString("Apellido_paterno_C")==null)?"":rs.getString("Apellido_paterno_C");
				String   Apellido_materno_C = (rs.getString("Apellido_materno_C")==null)?"":rs.getString("Apellido_materno_C");
				String   Nombre_C = (rs.getString("Nombre_C")==null)?"":rs.getString("Nombre_C");
				String   Telefono_C = (rs.getString("Telefono_C")==null)?"":rs.getString("Telefono_C");
				String   numeroCelular = (rs.getString("numeroCelular")==null)?"":rs.getString("numeroCelular");
				String   Email_C = (rs.getString("Email_C")==null)?"":rs.getString("Email_C");
				String   Numero_de_cliente = (rs.getString("Numero_de_cliente")==null)?"":rs.getString("Numero_de_cliente");
				String   DF_Nacimiento = (rs.getString("DF_Nacimiento")==null)?"":rs.getString("DF_Nacimiento");
				String   Comercial = (rs.getString("Comercial")==null)?"":rs.getString("Comercial");
				String   Fax_C = (rs.getString("Fax_C")==null)?"":rs.getString("Fax_C");	
				String   confirmacion_Email_C = (rs.getString("confirmacion_Email_C")==null)?"":rs.getString("confirmacion_Email_C");	
				
								
				datos =  new HashMap();				
			   datos.put("Razon_Social", Razon_Social);
				datos.put("Apellido_paterno", Apellido_paterno);
				datos.put("Apellido_materno", Apellido_materno);
				datos.put("Nombre", Nombre);
				datos.put("R_F_C", R_F_C);
				datos.put("Calle", Calle);
				datos.put("Colonia", Colonia);
				datos.put("Pais", Pais);
				datos.put("Estado1", Estado1);
				datos.put("Delegacion_o_municipio", Delegacion_o_municipio);
				datos.put("Codigo_postal", Codigo_postal);
				datos.put("Telefono", Telefono);
				datos.put("Fax", Fax);
				datos.put("Email", Email);
				datos.put("Apellido_paterno_C", Apellido_paterno_C);
				datos.put("Apellido_materno_C", Apellido_materno_C);
				datos.put("Nombre_C", Nombre_C);
				datos.put("Telefono_C", Telefono_C);
				datos.put("numeroCelular", numeroCelular);
				datos.put("Email_C", Email_C);
				datos.put("Numero_de_cliente", Numero_de_cliente);
				datos.put("DF_Nacimiento", DF_Nacimiento);
				datos.put("Comercial", Comercial);
				datos.put("Fax_C", Fax_C);											
				datos.put("confirmacion_Email_C", confirmacion_Email_C);						
         }
			
			rs.close();
			ps.close();		
			
			
				
		} catch (Exception e) {
			log.error("getConsultaReafiliacion  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getConsultaReafiliacion (S) ");
		return datos;
	}
	
	
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getTxtNumElectronico() {
		return txtNumElectronico;
	}

	public void setTxtNumElectronico(String txtNumElectronico) {
		this.txtNumElectronico = txtNumElectronico;
	}

	public String getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(String txtNombre) {
		this.txtNombre = txtNombre;
	}

	public String getSel_edo() {
		return sel_edo;
	}

	public void setSel_edo(String sel_edo) {
		this.sel_edo = sel_edo;
	}

	public String getTxtRFC() {
		return txtRFC;
	}

	public void setTxtRFC(String txtRFC) {
		this.txtRFC = txtRFC;
	}

	public String getSel_EPO() {
		return sel_EPO;
	}

	public void setSel_EPO(String sel_EPO) {
		this.sel_EPO = sel_EPO;
	}

		

}