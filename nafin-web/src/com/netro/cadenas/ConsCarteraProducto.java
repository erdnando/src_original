package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCarteraProducto implements IQueryGeneratorRegExtJS {
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsCarteraProducto.class);
	List conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	public ConsCarteraProducto() {	}
	
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
				
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append("SELECT pn.ic_producto_nafin, pn.ic_nombre, " +
				"DECODE(cg_agencia_sirac, 'F', 'S', 'N') as FISCAL, " +
				"DECODE(cg_agencia_sirac, 'T', 'S', 'N') as TRAMITADORA, " +
				"DECODE(cg_agencia_sirac, 'M', 'S', 'N') as MATRIZ, " +
				"DECODE(cg_agencia_sirac, 'F', 'checked', '') as AUXFISCAL, " +
				"DECODE(cg_agencia_sirac, 'T', 'checked', '') as AUXTRAMITADORA, " +
				"DECODE(cg_agencia_sirac, 'M', 'checked', '') as AUXMATRIZ, " +
				"cxp.cc_tipo, cg_agencia_sirac " +
				"FROM comrel_cartera_x_producto cxp, comcat_producto_nafin pn " +
				"WHERE cxp.ic_producto_nafin = pn.ic_producto_nafin " +
				"AND (cxp.ic_producto_nafin = 1 OR cxp.ic_producto_nafin = 4)");
		log.debug("qrySentencia  "+qrySentencia);
		log.debug("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		log.debug("crearPageCustomFile (E)");	
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		log.debug("crearPageCustomFile (S)");	
		return nombreArchivo;
					
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {		
		log.debug("crearCustomFile (E)");		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		log.debug("crearCustomFile (S)");	
		return nombreArchivo;	
	}
	
	public String guardarCambios(List lista){
		log.debug("guardarCambios(E)");
		String resultado = "Error al actualizar los datos";
		String qrySentencia 	= "";
		
		qrySentencia = "UPDATE comrel_cartera_x_producto " +
							"SET cg_agencia_sirac =  ? " +
							"WHERE ic_producto_nafin = ? " +
							"AND cc_tipo = ? ";
									
		for(int i = 0; i < lista.size(); i++){
			AccesoDB con = new AccesoDB();
			PreparedStatement ps = null;
			List registro = (ArrayList)lista.get(i);
			String agenciaSirac = registro.get(0).toString();
			String icProducto = registro.get(1).toString();
			String tipo = registro.get(2).toString();
			
			try{
				con.conexionDB();
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1, agenciaSirac);
				ps.setInt(2, Integer.parseInt(icProducto));
				ps.setString(3, tipo);
				ps.execute();
				resultado = "Los Datos han sido actualizados con exito";
				con.terminaTransaccion(true);			
			} catch (Exception e) {
				log.error("Error en ConsCarteraProducto.guardarCambios: "+e);
				throw new AppException("Error al guardar los cambios ", e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}
			log.debug("guardarCambios(S)");
		}
		
		return resultado;
	}
	
	public List getConditions() {  return conditions;  }
	
}