package com.netro.cadenas;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaCargaArchivoPyme implements IQueryGeneratorRegExtJS {
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaCargaArchivoPyme.class);
	
	/**
	 * Constructor de la clase.
	 */
	public ConsultaCargaArchivoPyme(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 	strSQL;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String clave_epo;
	private String icFolio;
	private String clave_estatus;
	private String fecha_carga_ini;
	private String fecha_carga_fin;

	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		strSQL.append("");
		log.info("getAggregateCalculationQuery(I)");

		return strSQL.toString();
	}//getAggregateCalculationQuery
	
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getDocumentQuery(I)");
		
		strSQL.append(" SELECT doc.ic_folio "+
							"FROM com_carga_docto_pyme doc"+
							", comcat_epo epo"+
							", comcat_estatus_carga_pyme est"+
							//", com_procesos"+
							" WHERE epo.ic_epo = doc.ic_epo"+
							" AND est.ic_estatus = doc.ic_estatus_carga_pyme");

		//EPO
		if(clave_epo != null && !clave_epo.equals("")){
			strSQL.append(" AND doc.ic_epo = ?");
			conditions.add(new Long(clave_epo));
		}

		//N�mero de folio
		if(icFolio != null && !icFolio.equals("")){
			strSQL.append(" AND doc.ic_folio = ?");
			conditions.add(icFolio);
		}

		//Estatus de la solicitud
		if(clave_estatus != null && !clave_estatus.equals("")){
			strSQL.append(" AND doc.ic_estatus_carga_pyme = ?");
			conditions.add(new Integer(clave_estatus));
		}

		//Fechas de carga
		if(fecha_carga_ini != null && !fecha_carga_ini.equals("") && fecha_carga_fin != null && !fecha_carga_fin.equals("")){
      strSQL.append(" AND doc.df_carga >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND doc.df_carga < TO_DATE(?, 'dd/mm/yyyy') + 1");
			conditions.add(fecha_carga_ini);
			conditions.add(fecha_carga_fin);
		}

		log.debug("..:: strSQL : " + strSQL.toString());
		log.debug("..:: conditions : " + conditions);
		
		log.info("getDocumentQuery(F)");
		
		return strSQL.toString();
	}//getDocumentQuery

	/**
	* Obtiene el query necesario para mostrar la informaci�n completa de 
	* una p�gina a partir de las llaves primarias enviadas como par�metro
	* @return Cadena con la consulta de SQL, para obtener la informaci�n
	* 	completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getDocumentSummaryQueryForIds(I)");

		strSQL.append(
				" SELECT doc.ic_folio "+
				", epo.cg_razon_social AS nombre_epo"+
				", doc.cg_nombre_docto"+
				", TO_CHAR(doc.df_carga,'dd/mm/yyyy HH24:MI:SS') df_carga"+
				", doc.cg_usuario, doc.cg_nombre_usuario"+
				", doc.ic_estatus_carga_pyme"+
				", est.cg_descripcion AS estatus"+
				", rec.cg_descripcion AS DESCRIPCION"+ 
				", doc.ic_resumen AS nombre_proceso"+
				", doc.cs_borrado"+
				", doc.ic_motivo AS IC_MOTIVO"+
				", TO_CHAR(doc.ic_fecha_cambio_estatus,'dd/mm/yyyy HH24:MI:SS') ic_fecha_cambio_estatus"+
//				", doc.ic_fecha_cambio_estatus AS IC_FECHA_CAMBIO_ESTATUS"+
				", doc.cg_obs_motivo AS CG_OBS_MOTIVO"+ 
				
				" FROM com_carga_docto_pyme doc"+
				", comcat_epo epo"+
				", comcat_estatus_carga_pyme est"+
				", comcat_rechaza_docto rec"+ //otro catalogo pero para 
				//", com_procesos"+
				" WHERE epo.ic_epo = doc.ic_epo"+
				" AND est.ic_estatus = doc.ic_estatus_carga_pyme"+
				" AND rec.ic_motivo(+) = doc.ic_motivo"
		);

		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i==0) {
				strSQL.append(" AND doc.ic_folio IN ( ");
			}
			strSQL.append("?");
			conditions.add(new Long(lItem.get(0).toString()));
			if(i!=(pageIds.size()-1)) {
				strSQL.append(",");
			} else {
				strSQL.append(" ) ");
			}
		}//for(int i=0;i<ids.size();i++)

		//EPO
		if(clave_epo != null && !clave_epo.equals("")){
			strSQL.append(" AND doc.ic_epo = ?");
			conditions.add(new Long(clave_epo));
		}

		//N�mero de folio
		if(icFolio != null && !icFolio.equals("")){
			strSQL.append(" AND doc.ic_folio = ?");
			conditions.add(icFolio);
		}

		//Estatus de la solicitud
		if(clave_estatus != null && !clave_estatus.equals("")){
			strSQL.append(" AND doc.ic_estatus_carga_pyme = ?");
			conditions.add(new Integer(clave_estatus));
		}

		//Fechas de carga
		if(fecha_carga_ini != null && !fecha_carga_ini.equals("") && fecha_carga_fin != null && !fecha_carga_fin.equals("")){
      strSQL.append(" AND doc.df_carga >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND doc.df_carga < TO_DATE(?, 'dd/mm/yyyy') + 1");
			conditions.add(fecha_carga_ini);
			conditions.add(fecha_carga_fin);
		}
		//strSQL.append(" ORDER BY doc.ic_folio ASC ");

		log.debug("..:: strSQL : " + strSQL.toString());
		log.debug("..:: conditions : " + conditions);
		
		log.info("getDocumentSummaryQueryForIds(F)");
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
	
	/**
	* Obtiene el query necesario para mostrar la informaci�n COMPLETA...
	* es decir sin paginacion. Se utiliza para la generacion de un archivo
	* con todos los registros disponibles. 
	* @return Cadena con la consulta de SQL, para obtener la informaci�n
	* 	completa 
	*/
	public String getDocumentQueryFile(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getDocumentQueryFile(I)");
    
			strSQL.append(
				" SELECT doc.ic_folio "+
				", epo.cg_razon_social AS nombre_epo"+
				", doc.cg_nombre_docto"+
				", TO_CHAR(doc.df_carga,'dd/mm/yyyy HH24:MI:SS') df_carga"+
				", doc.cg_usuario, doc.cg_nombre_usuario"+
				", doc.ic_estatus_carga_pyme"+
				", est.cg_descripcion AS estatus"+
				", rec.cg_descripcion AS DESCRIPCION"+ 
				", doc.ic_resumen AS nombre_proceso"+
				", doc.cs_borrado"+
				", doc.ic_motivo AS IC_MOTIVO"+
				", doc.ic_fecha_cambio_estatus AS IC_FECHA_CAMBIO_ESTATUS"+
				", doc.cg_obs_motivo AS CG_OBS_MOTIVO"+ 
				
				" FROM com_carga_docto_pyme doc"+
				", comcat_epo epo"+
				", comcat_estatus_carga_pyme est"+
				", comcat_rechaza_docto rec"+ //otro catalogo pero para 
				//", com_procesos"+
				" WHERE epo.ic_epo = doc.ic_epo"+
				" AND est.ic_estatus = doc.ic_estatus_carga_pyme"+
				" AND rec.ic_motivo(+) = doc.ic_motivo"
		);

		//EPO
		if(clave_epo != null && !clave_epo.equals("")){
			strSQL.append(" AND doc.ic_epo = ?");
			conditions.add(new Long(clave_epo));
		}

		//N�mero de folio
		if(icFolio != null && !icFolio.equals("")){
			strSQL.append(" AND doc.ic_folio = ?");
			conditions.add(icFolio);
		}

		//Estatus de la solicitud
		if(clave_estatus != null && !clave_estatus.equals("")){
			strSQL.append(" AND doc.ic_estatus_carga_pyme = ?");
			conditions.add(new Integer(clave_estatus));
		}

		//Fechas de carga
		if(fecha_carga_ini != null && !fecha_carga_ini.equals("") && fecha_carga_fin != null && !fecha_carga_fin.equals("")){
      strSQL.append(" AND doc.df_carga >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND doc.df_carga < TO_DATE(?, 'dd/mm/yyyy') + 1");
			conditions.add(fecha_carga_ini);
			conditions.add(fecha_carga_fin);
		}
		//strSQL.append(" ORDER BY doc.ic_folio ASC ");

		log.debug("..:: strSQL : " + strSQL.toString());
		log.debug("..:: conditions : " + conditions);
		
		log.info("getDocumentQueryFile(F)");
		return strSQL.toString();
	}//getDocumentQueryFile
	
	//GETTERS
  /**
	 * Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions(){return conditions;}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {return paginaNo;}
	
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset(){return paginaOffset;}
	
	//Parametros del formulario
	public String getClave_epo(){return clave_epo;}
	public String getIcFolio(){return icFolio;}
	public String getClave_estatus(){return clave_estatus;}
	public String getFecha_carga_ini(){return fecha_carga_ini;}
	public String getFecha_carga_fin(){return fecha_carga_fin;}
	
	//SETTERS
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo){paginaNo = newPaginaNo;}
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset){this.paginaOffset=paginaOffset;}

	//Parametros del formulario
	public void setClave_epo(String clave_epo){this.clave_epo = clave_epo;}
	public void setIcFolio(String icFolio){this.icFolio = icFolio;}
	public void setClave_estatus(String clave_estatus){this.clave_estatus = clave_estatus;}
	public void setFecha_carga_ini(String fecha_carga_ini){this.fecha_carga_ini = fecha_carga_ini;}
	public void setFecha_carga_fin(String fecha_carga_fin){this.fecha_carga_fin = fecha_carga_fin;}

	/*public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		return "";
	}*/

	/**
	* Genera un nuevo archivo SIN PAGINADOR de tipo CSV o PDF a partir de la informacion
	* obtenida con la consulta declarada en el metodo getDocumentQueryFile().
	* @return  la ruta y el nombre del archivo
	*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		if ("CSV".equals(tipo)){
			try{
				List listaEpos = new ArrayList();
				int rowspan = 0;
				String folioActual = "";
				String folioAnterior = "";
				HttpSession session = request.getSession();
	
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
	
				linea = "N�mero folio, EPO, Nombre archivo, Fecha y Hora de Carga, Usuario, Estatus, Fecha cambio de estatus, Motivo Rechazo,Proceso,\n";
				buffer.write(linea);


				while (rs.next()) {
					String nombre_proceso = (rs.getString("NOMBRE_PROCESO") == null)?"N/A":rs.getString("NOMBRE_PROCESO");
					String motivo =  (rs.getString("IC_MOTIVO") == null) ? "" : rs.getString("IC_MOTIVO");
					
					String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
					if( estatus.equals("Rechazada") )
					{
						if (motivo.equals("6"))
						{	motivo = rs.getString("DESCRIPCION") + "- " + rs.getString("CG_OBS_MOTIVO");  }
						else
						{	motivo = rs.getString("DESCRIPCION");	}
					}
					else
						motivo = "N/A";
						
					String fecha =  (rs.getString("IC_FECHA_CAMBIO_ESTATUS") == null) ? "" : rs.getString("IC_FECHA_CAMBIO_ESTATUS");
					if (!fecha.equals(""))
						fecha = fecha + "'";
					linea = "'" + rs.getString("IC_FOLIO") + "," + rs.getString("NOMBRE_EPO").replaceAll(",", "") + "," + rs.getString("CG_NOMBRE_DOCTO") + "," +
								"'"+rs.getString("DF_CARGA") + "," + rs.getString("CG_USUARIO") +rs.getString("CG_NOMBRE_USUARIO").replaceAll(",", "") + "," + 
								estatus + "," + fecha+ "," + motivo + "," + nombre_proceso + ",\n";
					System.out.println(linea);
					buffer.write(linea);
				}
				buffer.close();
			} catch (Exception e) {
				throw new AppException("Error al generar el archivo", e);
			}	finally {
			}
		}
		return nombreArchivo;
	}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return "";
	}

}