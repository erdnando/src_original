package com.netro.cadenas;
import com.netro.pdf.ComunesPDF;
import java.io.*;
import javax.naming.Context;		
import java.math.*;
import com.netro.exception.*;
import java.sql.Timestamp;
import java.text.*;
import java.util.*;
import javax.servlet.http.HttpServletRequest; 
import javax.servlet.http.HttpSession;
import netropology.utilerias.*;
import java.sql.*;
import org.apache.commons.logging.Log;
public class MensajeAfiliados implements  IQueryGeneratorRegExtJS {
	public MensajeAfiliados() {  }
	private List 	conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	StringBuffer 	strQuery;
	StringBuffer qrySentencia = new StringBuffer("");
	private static final Log log = ServiceLocator.getInstance().getLog(MensajeAfiliados.class);//Variable para enviar mensajes al log.
	private String txtFechaDesde;
	private String txtFechaHasta;
	private String txtTitulo;
	private String txtContenido;
	private String tipoAfiliado;
	private String[] cadenaDestino;
	private String[] ifDestino;
	
	private String bancoFondeo;
	
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
 	}  
		 
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();   
		String qrySentenciaE	= "";
		String qrySentenciaI	= "";
		String qrySentencia		= "";
		StringBuffer condicion    = new StringBuffer();
		StringBuffer	condicionE	= new StringBuffer("");
		StringBuffer	condicionI	= new StringBuffer("");
		log.info("getDocumentQueryFile(E)");
		if(!"".equals(txtFechaDesde))
			condicion.append(" and trunc(mi.df_ini_mensaje) >= to_date('"+txtFechaDesde+"','dd/mm/yyyy')");
		if(!"".equals(txtFechaHasta))
			condicion.append(" and trunc(mi.df_fin_mensaje) <= to_date('"+txtFechaHasta+"','dd/mm/yyyy')");
		if(!"".equals(txtTitulo))
			condicion.append(" and mi.cg_titulo like('%"+txtTitulo.replace(' ','%')+"%')");
		if(!"".equals(txtContenido))
			condicion.append(" and mi.cg_contenido like('%"+txtContenido.replace(' ','%')+"%')");

	condicion.append(" and mi.cg_tipo_afiliado = '"+tipoAfiliado+"'");
		int i=0;

		if("E".equals(tipoAfiliado)||"P".equals(tipoAfiliado)){
		
			if(cadenaDestino!=null){
				for(i=0;i<cadenaDestino.length;i++){
					if(i==0){
						condicionE.append(" and emi.ic_epo in("+cadenaDestino[i]);
					}else{
						condicionE.append(","+cadenaDestino[i]);
					}
				}
				if(i>0){
					condicionE.append(")");
				}
			}
			qrySentenciaE =	
				"  SELECT /*+ use_nl(mi emi epo) */ distinct mi.ic_mensaje_ini"   +
				"   ,to_char(mi.df_ini_mensaje,'dd/mm/yyyy') as fecha_ini"   +
				"   ,to_char(mi.df_fin_mensaje,'dd/mm/yyyy') as fecha_fin"   +
				"   ,mi.cg_titulo"   +
				"   ,mi.cg_contenido   "   +
				"   ,NVL(cg_tipo_mensaje,'M') tipomensaje" +
				"  FROM com_mensaje_ini mi"   +
				"  ,comrel_epo_mensaje_ini emi"   +
				"  ,comcat_epo epo" + //FODEA 008 - 2009 ACF
				"  WHERE mi.ic_mensaje_ini = emi.ic_mensaje_ini" +
				"  AND emi.ic_epo = epo.ic_epo" +//FODEA 008 - 2009 ACF
				"  AND epo.ic_banco_fondeo = 1"+ //FODEA 008 - 2009 ACF
				condicion.toString()+
				condicionE.toString();
		}
		
		if("I".equals(tipoAfiliado)||"P".equals(tipoAfiliado)){
		
			if(ifDestino!=null){
				for(i=0;i<ifDestino.length;i++){
					if(i==0){
						condicionI.append(" and imi.ic_if in("+ifDestino[i]);
					}else{
						condicionI.append(","+ifDestino[i]);
					}
				}
				if(i>0){
					condicionI.append(")");
				}
			}
			qrySentenciaI =	
			"  SELECT /*+ use_nl(mi imi cie epo) */ distinct mi.ic_mensaje_ini"   +
			"   ,to_char(mi.df_ini_mensaje,'dd/mm/yyyy') as fecha_ini"   +
			"   ,to_char(mi.df_fin_mensaje,'dd/mm/yyyy') as fecha_fin"   +
			"   ,mi.cg_titulo"   +
			"   ,mi.cg_contenido   "   +
			"   ,NVL(cg_tipo_mensaje,'M') tipomensaje" +
			"  FROM com_mensaje_ini mi"   +
			"  ,comrel_if_mensaje_ini imi"   +
			"  ,comrel_if_epo cie" +//FODEA 008 - 2009 ACF
			"  ,comcat_epo epo" +//FODEA 008 - 2009 ACF
			"  WHERE mi.ic_mensaje_ini = imi.ic_mensaje_ini" +
			"  AND imi.ic_if = cie.ic_if" +//FODEA 008 - 2009 ACF
			"  AND cie.ic_epo = epo.ic_epo" +//FODEA 008 - 2009 ACF
			"  AND epo.ic_banco_fondeo =1 "+//FODEA 008 - 2009 ACF
			condicion.toString()+
			condicionI.toString();
		}
		if("E".equals(tipoAfiliado))
			qrySentencia = qrySentenciaE;
		else if("I".equals(tipoAfiliado))
			qrySentencia = qrySentenciaI;    
		else if("P".equals(tipoAfiliado))
			qrySentencia = qrySentenciaE + " UNION " + qrySentenciaI;
			
		log.debug("getDocumentQueryFile "+qrySentencia.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
		
 	} 
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.debug("::crearCustomFile(E)");
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		return nombreArchivo;			
	}
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		return "";
	}

	public List getConditions() {
		return conditions;
	}
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
	

	public String getTxtFechaDesde() {
		return txtFechaDesde;
	}

	public void setTxtFechaDesde(String txtFechaDesde) {
		this.txtFechaDesde = txtFechaDesde;
	}
	public String getTxtFechaHasta() {
		return txtFechaHasta;
	}

	public void setTxtFechaHasta(String txtFechaHasta) {
		this.txtFechaHasta = txtFechaHasta;
	}
	
	
	
	public String getTxtTitulo() {
		return txtTitulo;
	}

	public void setTxtTitulo(String txtTitulo) {
		this.txtTitulo = txtTitulo;
	}
	
	public String getTxtContenido() {
		return txtContenido;
	}

	public void setTxtContenido(String txtContenido) {
		this.txtContenido = txtContenido;
	}
	public String getTipoAfiliado() {
		return tipoAfiliado;
	}

	public void setTipoAfiliado(String tipoAfiliado) {
		this.tipoAfiliado = tipoAfiliado;
	}
	
	public String[] getCadenaDestino() {
		return cadenaDestino;
	}

	public void setCadenaDestino(String[] cadenaDestino) {
		this.cadenaDestino = cadenaDestino;
	}
	
	public String getBancoFondeo() {
		return bancoFondeo;
	}

	public void setBancoFondeo(String bancoFondeo) {
		this.bancoFondeo = bancoFondeo;
	}
	
	public String[] getIfDestino() {
		return ifDestino;
	}

	public void setIfDestino(String[] ifDestino) {
		this.ifDestino = ifDestino;
	}
}