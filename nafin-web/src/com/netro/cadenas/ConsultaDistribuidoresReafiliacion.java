package com.netro.cadenas;

import java.sql.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONArray;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.util.*;
import java.util.List;

/**********************************************************************************
 * Fodea:       25-2015                                                           *
 * Descripci�n: La clase tiene m�todos que generan las consultas para obtener los *
 *              datos de reafiliaci�n.                                            *
 * Elabor�:     Agust�n Bautista Ruiz                                             *
 * Fecha:       13/11/2015                                                        *
 **********************************************************************************/

public class ConsultaDistribuidoresReafiliacion implements IQueryGeneratorRegExtJS{
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaDistribuidoresReafiliacion.class);
	private String icPyme;
	private String icEpo;
	private List conditions;

	public ConsultaDistribuidoresReafiliacion(){}

	public String getDocumentQuery(){
		return null;
	}

	public String getAggregateCalculationQuery(){
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids){
		return null;
	}

	public String getDocumentQueryFile(){

		StringBuffer sentencia = new StringBuffer();
		conditions = new ArrayList();
		log.info("getDocumentQueryFile (E)");	

		sentencia.append("SELECT DISTINCT E.IC_EPO,                                  \n");
		sentencia.append("       E.CG_RAZON_SOCIAL AS CADENA_PRODUCTIVA,             \n");
		sentencia.append("       N.IC_NAFIN_ELECTRONICO AS NAFIN_ELECTRONICO,        \n");
		sentencia.append("       P.CG_RAZON_SOCIAL AS RAZON_SOCIAL,                  \n");
		sentencia.append("       P.CG_RFC AS RFC,                                    \n");
		sentencia.append("       CASE WHEN C.CG_TIPOS_CREDITO = 'C'                  \n");
		sentencia.append("       THEN 'Modalidad 2 (Riesgo Distribuidor)'            \n");
		sentencia.append("       WHEN C.CG_TIPOS_CREDITO = 'D'                       \n");
		sentencia.append("       THEN 'Modalidad 1 (Riesgo Empresa de Primer Orden)' \n");
		sentencia.append("       ELSE '' END AS TIPO_CREDITO                         \n");
		sentencia.append("FROM   COMCAT_PYME P, COMREL_NAFIN N,                      \n");
		sentencia.append("       COMCAT_EPO E, COMREL_PRODUCTO_EPO C                 \n");
		sentencia.append("WHERE  P.IC_PYME = N.IC_EPO_PYME_IF                        \n");
		sentencia.append("       AND E.IC_EPO = C.IC_EPO                             \n");
		sentencia.append("       AND C.IC_PRODUCTO_NAFIN = 4                         \n");
		sentencia.append("       AND N.CG_TIPO = 'P'                                 \n");
		sentencia.append("       AND P.IC_PYME = ?                                   \n");
		sentencia.append("       AND E.IC_EPO IN ("+ this.icEpo +")                  \n");
		sentencia.append("       AND E.IC_EPO NOT IN (                               \n");
		sentencia.append("       SELECT IC_EPO                                       \n");
		sentencia.append("         FROM COMREL_PYME_EPO_X_PRODUCTO                   \n");
		sentencia.append("        WHERE IC_PYME = ?)                                   ");

		conditions.add(this.icPyme);
		conditions.add(this.icPyme);

		log.debug("Sentencia: \n" + sentencia.toString() );
		log.debug("Condiciones: " + conditions.toString());
		log.info("getDocumentQueryFile (S)");
		return sentencia.toString();

	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		return null;
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		return null;
	}

/*****************************************
 *       M�TODOS DEL FODEA 25-2015       *
 *****************************************/
	/**
	 * Obtiene el tipo de cr�dito de la epo seleccionada para generar el combo Tipo de Cr�dito
	 * @return 
	 */
	public String getTipoCredito(){

		log.info("getTipoCredito(E)");
		String tipoCreditos = "";
		StringBuffer sentencia = new StringBuffer();
		AccesoDB          con = new AccesoDB();
		PreparedStatement ps  = null;
		ResultSet	      rs  = null;

		sentencia.append("SELECT CG_TIPOS_CREDITO          \n");
		sentencia.append("  FROM COMREL_PRODUCTO_EPO       \n");
		sentencia.append(" WHERE IC_EPO = "+this.icEpo+"   \n");
		sentencia.append("       AND IC_PRODUCTO_NAFIN = 4 \n");
		sentencia.append("       AND CS_HABILITADO = 'S'     ");

		log.debug("Sentencia: \n" + sentencia.toString());

		try{
		
			con.conexionDB();
			rs = con.queryDB(sentencia.toString());
			if(rs.next()){
					tipoCreditos = rs.getString("CG_TIPOS_CREDITO");
				} else{
					tipoCreditos = "";
				}

		} catch(Exception e){
			log.warn("getTipoCredito.Exception. " + e);
		} finally{
			try{
				if(ps!=null)
					ps.close();
			}catch(SQLException ex){
				log.warn(ex);
			}
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

		log.info("getTipoCredito(S)");
		return tipoCreditos;
	}

/*****************************************
 *          GETTERS AND SETTERS          *
 *****************************************/
	public List getConditions(){
		return conditions;
	}

	public String getIcPyme(){
		return icPyme;
	}

	public void setIcPyme(String icPyme){
		this.icPyme = icPyme;
	}

	public String getIcEpo(){
		return icEpo;
	}

	public void setIcEpo(String icEpo){
		this.icEpo = icEpo;
	}

}