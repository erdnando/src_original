package com.netro.cadenas;
import java.util.ArrayList;
import java.text.*;
import java.util.*;
import com.netro.pdf.ComunesPDF;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.sql.*;

public class ConsClientesSusCredi  {

	private static final Log log = ServiceLocator.getInstance().getLog(ConsClientesSusCredi.class);//Variable para enviar mensajes al log.
	//ATRIBUTOS
	private List 		conditions;
	
	private String		eposPEF;
	private String		eposGobMun;
	private String		eposPrivadas;
	private String		eposCredi;
	private String		eposNC;
	private String 	sNoEposSelec; 
	
	private String 	mesInicial;
	private String 	anioInicial;
	private String 	mesFinal;
	private String 	anioFinal;
	private String 	sPeriodo1;
	private String 	sPeriodo2;
	private String 	sPeriodo3;
	private String 	filtroEpo;
	
	private String 	strDirectorioTemp;
	
	
	
	
	
	
	private String cveCat ="";
	private String RegSig = "";
	private String Totalreg;
	boolean bandera = true;
	
	StringBuffer qrySentencia = new StringBuffer("");	
	
	public ConsClientesSusCredi() {
	}
	public Registros getConsultaDataEpo( ){
		log.info("getConsultaDataEpo(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			StringBuffer condicion  = new StringBuffer("");
			AccesoDB con = new AccesoDB(); 
			Registros registros  = new Registros();
			boolean entro = false;
			try{
				con.conexionDB();
				qrySentencia.append(" SELECT e.ic_epo AS clave, e.cg_razon_social|| ' pymes('||COUNT(rpe.ic_pyme)||')' AS descripcion ");
				qrySentencia.append(" FROM comcat_epo e, comrel_pyme_epo rpe");
				qrySentencia.append(" WHERE e.ic_epo = rpe.ic_epo");
				if(!this.filtroEpo.equals("")){
					qrySentencia.append( " AND cg_razon_social LIKE '"+ this.filtroEpo.toUpperCase() +"%' ");
				}
				if (eposPEF.equals("S")) {
					qrySentencia.append("  AND ( e.ic_tipo_epo  = 1 ");
					entro = true;
				}
				if (eposGobMun.equals("S")) {
					qrySentencia.append(((entro)?" OR ": "  AND ( ") +" e.ic_tipo_epo = 2");
					entro = true;
				}
				if (eposPrivadas.equals("S")) {
					qrySentencia.append(((entro)?" OR ": " AND ( ") +" e.ic_tipo_epo = 3");
					entro = true;
				}
				if (eposNC.equals("S")) {
					qrySentencia.append(((entro)?" OR ": " AND ( ") +" e.ic_tipo_epo is null");
					entro = true;
				}
				qrySentencia.append((entro)?" ) ": " ");
				if (eposCredi.equals("S")) {
					qrySentencia.append(" AND e.ic_epo IN (SELECT ic_epo FROM comrel_producto_epo WHERE ic_producto_nafin = 5)");
				}
				if (!sNoEposSelec.trim().equals("")) {
					qrySentencia.append(" AND e.ic_epo NOT IN ("+sNoEposSelec+")");
				}
				qrySentencia.append(" GROUP BY e.ic_epo, e.cg_razon_social");
				qrySentencia.append(" ORDER BY e.cg_razon_social ");
				registros = con.consultarDB(qrySentencia.toString(),conditions);
				con.cierraConexionDB();
			} catch (Exception e) {
				log.error("getConsultaData  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			} 	
		
			log.debug("qrySentencia getConsultaData: mm "+qrySentencia.toString());
			log.debug("conditions "+conditions);
			
			log.info("getConsultaDataEpo (S) ");
			return registros;
	}	
	
	public String getTotalPymes( ){
		log.info("getTotalPymes(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			StringBuffer condicion  = new StringBuffer("");
			AccesoDB con = new AccesoDB(); 
			PreparedStatement ps = null;
			ResultSet rs = null;
			String resultado = "";  
			try{
				con.conexionDB();
				qrySentencia.append(" SELECT sum(COUNT(rpe.ic_pyme)) AS pymes_afiliadas");
				qrySentencia.append(" FROM comcat_epo e, comrel_pyme_epo rpe");
				qrySentencia.append(" WHERE e.ic_epo = rpe.ic_epo");
				qrySentencia.append(" AND e.ic_epo IN ("+sNoEposSelec+")");
				
				
				qrySentencia.append(" GROUP BY e.ic_epo, e.cg_razon_social");
				qrySentencia.append(" ORDER BY e.cg_razon_social ");
			//	conditions.add(sNoEposSelec);
				ps = con.queryPrecompilado(qrySentencia.toString(),conditions );
		      rs = ps.executeQuery();
				log.debug("qrySentencia "+qrySentencia);
				log.debug("conditions "+conditions);
				if (rs.next()){
					resultado = rs.getString("pymes_afiliadas")==null?"":rs.getString("pymes_afiliadas");
				}
				rs.close();
				ps.close();
			} catch (Exception e) {
				log.error("getTotalPymes  Error: " + e);
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			} 	
		
			log.debug("qrySentencia getConsultaData: mm "+qrySentencia.toString());
			log.debug("conditions "+conditions);
			
			log.info("getTotalPymes (S)  resultado "+resultado);
			return resultado;
	}
	
	
	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  

	public String getEposPEF() {
		return eposPEF;
	}

	public void setEposPEF(String eposPEF) {
		this.eposPEF = eposPEF;
	}

	public String getEposGobMun() {
		return eposGobMun;
	}

	public void setEposGobMun(String eposGobMun) {
		this.eposGobMun = eposGobMun;
	}

	public String getEposPrivadas() {
		return eposPrivadas;
	}

	public void setEposPrivadas(String eposPrivadas) {
		this.eposPrivadas = eposPrivadas;
	}

	public String getEposCredi() {
		return eposCredi;
	}

	public void setEposCredi(String eposCredi) {
		this.eposCredi = eposCredi;
	}

	public String getEposNC() {
		return eposNC;
	}

	public void setEposNC(String eposNC) {
		this.eposNC = eposNC;
	}

	public String getSNoEposSelec() {
		return sNoEposSelec;
	}

	public void setSNoEposSelec(String sNoEposSelec) {
		this.sNoEposSelec = sNoEposSelec;
	}

	public String getMesFinal() {
		return mesFinal;
	}

	public void setMesFinal(String mesFinal) {
		this.mesFinal = mesFinal;
	}

	public String getMesInicial() {
		return mesInicial;
	}

	public void setMesInicial(String mesInicial) {
		this.mesInicial = mesInicial;
	}

	public String getAnioFinal() {
		return anioFinal;
	}

	public void setAnioFinal(String anioFinal) {
		this.anioFinal = anioFinal;
	}

	public String getAnioInicial() {
		return anioInicial;
	}

	public void setAnioInicial(String anioInicial) {
		this.anioInicial = anioInicial;
	}

	public String getSPeriodo1() {
		return sPeriodo1;
	}

	public void setSPeriodo1(String sPeriodo1) {
		this.sPeriodo1 = sPeriodo1;
	}

	public String getSPeriodo2() {
		return sPeriodo2;
	}

	public void setSPeriodo2(String sPeriodo2) {
		this.sPeriodo2 = sPeriodo2;
	}

	public String getSPeriodo3() {
		return sPeriodo3;
	}

	public void setSPeriodo3(String sPeriodo3) {
		this.sPeriodo3 = sPeriodo3;
	}

	public String getStrDirectorioTemp() {
		return strDirectorioTemp;
	}

	public void setStrDirectorioTemp(String strDirectorioTemp) {
		this.strDirectorioTemp = strDirectorioTemp;
	}

	public String getFiltroEpo() {
		return filtroEpo;
	}

	public void setFiltroEpo(String filtroEpo) {
		this.filtroEpo = filtroEpo;
	}

	

	

}