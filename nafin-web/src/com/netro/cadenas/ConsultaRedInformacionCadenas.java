package com.netro.cadenas;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.ServiceLocator;

public class ConsultaRedInformacionCadenas implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaRedInformacionCadenas.class);
	private List   conditions;
	private List   alMes;
	private List   alAnio;
	private List   datosmeses;
	private String mesesConT;
	private String clasificacion;

	public ConsultaRedInformacionCadenas(){}

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery() {
		return null;
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery() {
		return null;
	}

	/**
	 * Realiza la consulta para generar el archivo csv
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append(" SELECT  \n "                                 );
		qrySentencia.append(" m.cg_nombre_comercial as nomepo , \n "       );
		qrySentencia.append(" m.cg_razon_social  as nomPyme , \n "         );
		qrySentencia.append(" m.cg_estado as nomEstado ,  \n "             );
		qrySentencia.append(" m.cg_rfc as rfcPyme, \n "                    );
		qrySentencia.append(" m.cg_sector as sector  \n "                  );
		for (int e = 0; e < alMes.size(); e++) {
			qrySentencia.append(" ,SUM(m."+datosmeses.get(e).toString()+")" +" AS "+datosmeses.get(e).toString());
		}
		qrySentencia.append(" FROM (SELECT  \n "                           );
		qrySentencia.append(" e.cg_nombre_comercial, \n "                  );
		qrySentencia.append(" p.cg_razon_social , \n "                     );
		qrySentencia.append(" c.ic_epo,  \n "                              );
		qrySentencia.append(" c.ic_pyme, \n "                              );
		qrySentencia.append(" c.cg_estado, \n "                            );
		qrySentencia.append(" c.cg_rfc, c.cg_sector  \n "                  );
		for (int e = 0; e < alMes.size(); e++) {
			qrySentencia.append(", CASE WHEN  substr(c.CG_MES,1,3) = '"+alMes.get(e).toString()+"' and  substr(c.CG_MES,5,8) = '"+alAnio.get(e).toString()+"' THEN sum(c.fn_monto) ELSE 0 END AS " + datosmeses.get(e).toString());
		}
		qrySentencia.append(" FROM com_cruce_cadenas c, \n "               );
		qrySentencia.append(" comcat_depen_cadenas d, \n "                 );
		qrySentencia.append(" comcat_epo e, \n "                           );
		qrySentencia.append(" comcat_pyme p \n "                           );
		qrySentencia.append(" WHERE c.ic_epo = e.ic_epo \n "               );
		qrySentencia.append(" AND c.ic_pyme = p.ic_pyme \n "               );
		qrySentencia.append(" AND c.ic_dependencia = d.ic_dependencia \n " );
		qrySentencia.append(" AND c.ic_dependencia = ? \n "                );
		qrySentencia.append(" AND c.cg_mes in( "+mesesConT+")\n "          );
		qrySentencia.append(" GROUP BY e.cg_nombre_comercial, \n "         );
		qrySentencia.append(" p.cg_razon_social, \n "                      );
		qrySentencia.append(" c.ic_epo, \n "                               );
		qrySentencia.append(" c.ic_pyme, \n "                              );
		qrySentencia.append(" c.cg_estado, \n "                            );
		qrySentencia.append(" c.cg_rfc, \n "                               );
		qrySentencia.append(" c.cg_sector, \n "                            );
		qrySentencia.append(" c.cg_mes) m \n "                             );
		qrySentencia.append(" GROUP BY m.cg_nombre_comercial, \n "         );
		qrySentencia.append(" m.cg_razon_social, \n "                      );
		qrySentencia.append(" m.ic_epo, \n "                               );
		qrySentencia.append(" m.ic_pyme, \n "                              );
		qrySentencia.append(" m.cg_estado, \n "                            );
		qrySentencia.append(" m.cg_rfc, \n "                               );
		qrySentencia.append(" m.cg_sector \n "                             );
		qrySentencia.append(" ORDER BY m.cg_nombre_comercial, \n "         );
		qrySentencia.append(" m.cg_razon_social "                          );

		conditions.add(clasificacion);

		log.info("Sentencia:\n"   + qrySentencia.toString());
		log.info("Condiciones: "  + conditions.toString()  );
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();

	}

	/**
	 * Metodo para generar un archivo PDF, utilizando paginaci�n
	 * @return nombreArchivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros rs, String path, String tipo) {
		return null;
	}

	/**
	 * Metodo para generar un archivo CSV, de todos los registros
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");

		String             nombreArchivo    = "";
		StringBuffer       contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer           = null;
		BufferedWriter     buffer           = null;
		int                total            = 0;
		double             suma             = 0;

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer        = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer        = new BufferedWriter(writer);

			contenidoArchivo = new StringBuffer();
			contenidoArchivo.append("EPO,"   );
			contenidoArchivo.append("PYME,"  );
			contenidoArchivo.append("ESTADO,");
			contenidoArchivo.append("RFC,"   );
			contenidoArchivo.append("SECTOR,");
			for (int e = 0; e < alMes.size(); e++){
				contenidoArchivo.append(datosmeses.get(e).toString()+",");
			}
			contenidoArchivo.append("SUMA");
			contenidoArchivo.append("\r\n");

			while (rs.next()){
				String nomEpo    = (rs.getString("nomEpo")    == null) ? "" : rs.getString("nomEpo");
				String nomPyme   = (rs.getString("nomPyme")   == null) ? "" : rs.getString("nomPyme");
				String nomEstado = (rs.getString("nomEstado") == null) ? "" : rs.getString("nomEstado");
				String rfcPyme   = (rs.getString("rfcPyme")   == null) ? "" : rs.getString("rfcPyme");
				String sector    = (rs.getString("sector")    == null) ? "" : rs.getString("sector");

				contenidoArchivo.append(nomEpo.replace(',',' ')      +", ");
				contenidoArchivo.append(nomPyme.replace(',',' ')     +", ");
				contenidoArchivo.append(nomEstado.replace(',',' ')   +", ");
				contenidoArchivo.append(rfcPyme.replace(',',' ')     +", ");
				contenidoArchivo.append(sector.replace(',',' ')           );

				for (int y = 0; y < datosmeses.size(); y++){
					contenidoArchivo.append( ","+(rs.getString(datosmeses.get(y).toString())==null?"":rs.getString(datosmeses.get(y).toString()).replace(',',' ')));
					String valor = rs.getString(datosmeses.get(y).toString());
					suma  += Double.parseDouble((String)valor);
				}

				contenidoArchivo.append(","+suma+",");
				contenidoArchivo.append("\r\n");
				suma = 0; //se resetea el valor de la suma de los valores
				total++;

				if(total==1000){
					total=0;
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio
				}
			}

			//CUANDO NO HAY REGISTROS
			if(total == 0){
				contenidoArchivo.append( " No se encontr� ning�n registro para los criter�os seleccionados. " );
				contenidoArchivo.append( "\r\n");
			}

			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo csv ", e);
		} finally {
			try {
			} catch(Exception e) {}
		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;

	}

/*****************************************
 *          GETTERS AND SETTERS          *
 *****************************************/
	public List getConditions() {
		return conditions;
	}

	public List getAlMes() {
		return alMes;
	}

	public void setAlMes(List alMes) {
		this.alMes = alMes;
	}

	public List getAlAnio() {
		return alAnio;
	}

	public void setAlAnio(List alAnio) {
		this.alAnio = alAnio;
	}

	public List getDatosmeses() {
		return datosmeses;
	}

	public void setDatosmeses(List datosmeses) {
		this.datosmeses = datosmeses;
	}

	public String getMesesConT() {
		return mesesConT;
	}

	public void setMesesConT(String mesesConT) {
		this.mesesConT = mesesConT;
	}

	public String getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

}