package com.netro.cadenas;

import java.io.Serializable;

/**
 * Clase que se encarga de generar las informaci�n requerida para
 * presentar las gr�ficas de la EPO.
 */
public class GeneraInfoGraficasThread implements Runnable, Serializable {
	
	private boolean 			started;
	private boolean 			running;
	private GraficasEpo 		graficasEpo;
	private boolean			error;
	private String				cveEpo;
	private String				estatusProc;

	public GeneraInfoGraficasThread() {
		started 			   = false;
		running 			   = false;
		graficasEpo			= new GraficasEpo();
		error					= false;
	}
	 
	/**
	 * Generaci�n de las informaci�n para las gr�fica de EPO en Moneda Nacional
	 * y Dolares
	 * Este m�todo afecta los atributos de esta clase:
	 * Si Existe un error en la obtenci�n de informaci�n para las gr�ficas de MN
	 * o Dolares entonces this.estatusProc = "ERR" y this.setError(true);
	 * Si no hay error regresa el estatus del proceso separado por |. 
	 * El primer valor corresponde al estatus del proceso de Moneda Nacional(MN) 
	 * y el segundo al de Dolares (DL)
	 */
	protected void generaInfoGraficas() {
		String estatusMN = null;
		String estatusDL = null;
		boolean hayErrorMN = false;
		boolean hayErrorDL = false;
		
		try {
			estatusMN = graficasEpo.obtenerInformacion(cveEpo, "MN");
		} catch(Exception e) {
			hayErrorMN = true;
			estatusMN = "ERR";
			e.printStackTrace();
			System.out.println("Error en GeneraInfoGraficasThread::obtenerInformacion al generar informacion de graficas MN");
		}
	
		try {
			estatusDL = graficasEpo.obtenerInformacion(cveEpo, "DL");
		} catch(Exception e) {
			hayErrorDL = true;
			estatusDL = "ERR";
			e.printStackTrace();
			System.out.println("Error en GeneraInfoGraficasThread::obtenerInformacion al generar informacion de graficas DL");
		}
	
		if ("ERR".equals(estatusMN) || "ERR".equals(estatusDL)) {
			this.setRunning(false);
			this.estatusProc = "ERR";
			this.setError(true);
		} else {
			this.estatusProc = estatusMN + "|" + estatusDL;
		}
	}
	

   public synchronized boolean isStarted() {
       return started;
   }

   public synchronized boolean isCompleted() {
		return((estatusProc==null)?false:true);
   }

   public synchronized boolean isRunning() {
       return running;
   }

   public synchronized void setRunning(boolean running) {
       this.running = running;
		if (running) {
			started = true;
		}
	}
	 
	public synchronized void setError(boolean error) {
       this.error = error;
   }
	
	public synchronized boolean hasError() {
		return this.error;
   }
	
	

   public void run() {

		boolean		lbOK	=  true;
		
      try {
			this.setRunning(true);
			
			while (this.isRunning() && estatusProc == null){
					if(lbOK && estatusProc == null){
						lbOK = false;
					this.generaInfoGraficas();
					}
				}
      } finally {
			this.setRunning(false);
      }
    }


	public void setCveEpo(String cveEpo) {
		this.cveEpo = cveEpo;
	}


	public String getCveEpo() {
		return this.cveEpo;
	}


	public void setEstatusProc(String estatusProc) {
		this.estatusProc = estatusProc;
	}


	/**
	 * Regresa el estatus del proceso
	 * @return Cadena con el estatus del proceso de Moneda Nacional y Dolares
	 * El valor regresado esta conformado por 2 valores separados por pipe |.
	 * El primero corresponde a Moneda Nacional (MN) y el segundo a Dolares (DL)
	 * Ejemplo: OK|OK
	 */
	public String getEstatusProc() {
		return this.estatusProc;
	}

}