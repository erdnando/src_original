package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsIfEpoExt implements IQueryGeneratorRegExtJS {
	public ConsIfEpoExt() {}
	
	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(com.netro.cadenas.ConsIfEpoExt.class);
	
	StringBuffer 	   qrySentencia;
	private   List 	conditions;
	private String 	operacion;
	private String 	paginaOffset;
	private String 	paginaNo;
	
	private String numNafinElec;
	//private String nombreMandante;
	private String claveIf;
	private String estado;
	private String cveEpo;
	
	

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		return "";
	}//getAggregateCalculationQuery

	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		log.info("getDocumentQuery (E)");
		qrySentencia 		= new StringBuffer();
		conditions 			= new ArrayList();
		
			
		qrySentencia.append( "  SELECT /*+use_nl(ci, cn, cdom, cest)*/  " +
									"         cn.ic_nafin_electronico AS numnafinelect,  " +
									"         ci.ic_if AS razonsocial,  " +
									"         cest.ic_estado AS estado  " +
									"    FROM comcat_if ci,  " +
									"         comrel_nafin cn,  " +
									"         com_domicilio cdom,  " +
									"         comcat_estado cest,  " +
									"			 comrel_if_epo cip, " +
									"			 comcat_plaza cp "+	//tabla para agregar pantalla de IFs relacionados
									"   WHERE ci.ic_if = cn.ic_epo_pyme_if  " +
									"		AND ci.ic_if = cip.ic_if "+
									"     AND ci.ic_if = cdom.ic_if  " +
									"     AND cdom.ic_estado = cest.ic_estado  " +
									"		AND cip.ic_plaza = cp.ic_plaza (+) "+
									"     AND cip.ic_epo = ? "  +
									"     AND cdom.cs_fiscal = ? " +
									"		AND cip.CS_VOBO_NAFIN = ? "+
									"		AND cn.cg_tipo = ? ");
									
									conditions.add(cveEpo);
									conditions.add("S");
									conditions.add("S");
									conditions.add("I");
		
		 if(numNafinElec!=null&&!"".equals(numNafinElec)) {
		  	 qrySentencia.append(" AND cn.ic_nafin_electronico = ? ");
			 conditions.add(numNafinElec);
			}
			/*if(nombreMandante!=null&&!"".equals(nombreMandante)) {
				qrySentencia.append (" AND ci.cg_razon_social = ? " );
				conditions.add(nombreMandante);
			}*/
			if(claveIf!=null&&!"".equals(claveIf)) {
				qrySentencia.append (" AND ci.ic_if = ? " );
				conditions.add(claveIf);
			}
			if(estado!=null&&!"".equals(estado)) {
				qrySentencia.append( " AND cest.ic_estado = ? ");
				conditions.add(estado);
			}
		qrySentencia.append(" ORDER BY ci.cg_razon_social ");

		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions: "+conditions);
		log.info("getDocumentQuery (S)");
		return qrySentencia.toString();
	}//getDocumentQuery
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds (E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append( " SELECT /*+use_nl(ci, cn, cdom, cest)*/  " +
									"       cn.ic_nafin_electronico AS numnafinelect,  " +
									"       ci.cg_razon_social AS razonsocial,  " +
									"       cdom.cg_calle AS calle,   " +
									"       cest.cd_nombre AS estado,  " +
									"       cdom.cg_telefono1 AS telefono,   " +
									"       ci.cs_tipo, cip.cg_banco, cip.cg_num_cuenta,cip.cs_aceptacion,"+
									"       decode (cp.cd_descripcion, null, ' ', cp.cd_descripcion ||','|| cp.cg_estado) AS plaza"+
									"  FROM comcat_if ci,  " +
									"       comrel_nafin cn,  " +
									"       com_domicilio cdom,  " +
									"       comcat_estado cest,  " +
									"		  comrel_if_epo cip, " +
									"		  comcat_plaza cp "+	//tabla para agregar pantalla de IFs relacionados
									" WHERE ci.ic_if = cn.ic_epo_pyme_if   " +
									"	AND ci.ic_if = cip.ic_if "+
									"	AND ci.ic_if = cdom.ic_if " +
									"	AND cdom.ic_estado = cest.ic_estado " +
									"	AND cip.ic_plaza = cp.ic_plaza (+) "+
									"	AND cip.ic_epo = ? "  +
									"	AND cdom.cs_fiscal = ? " +
									"	AND cip.CS_VOBO_NAFIN = ? " +
									"	AND cn.cg_tipo = ? "+
									"	AND ( ");
									conditions.add(cveEpo);
									conditions.add("S");
									conditions.add("S");
									conditions.add("I");
		
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
				if(i>0){
					qrySentencia.append(" OR ");
				}
				qrySentencia.append( " (cn.ic_nafin_electronico = ? "+
											"  AND ci.ic_if = ? "+  
											"  AND cest.ic_estado = ?) " );
					conditions.add(new Long(lItem.get(0).toString()));
					conditions.add(new Long(lItem.get(1).toString()));
					conditions.add(new Long(lItem.get(2).toString()));
		}//for(int i=0;i<ids.size();i++)
		
		qrySentencia.append(")");
		qrySentencia.append(" ORDER BY ci.cg_razon_social ");
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds (S)");
		return qrySentencia.toString();
	}//getDocumentSummaryQueryForIds	

	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile (E)");
		qrySentencia 		= new StringBuffer();
		conditions 			= new ArrayList();
			
		qrySentencia.append( "  SELECT /*+use_nl(ci, cn, cdom, cest)*/  " +
									"       cn.ic_nafin_electronico AS Numero_de_Nafin_Electronico ,  " +
									"       ci.cg_razon_social AS Nombre_o_Razon_Social ,  " +
									"       cdom.cg_calle AS Domicilio,   " +
									"       cest.cd_nombre AS Estado,  " +
									"       cdom.cg_telefono1 AS Telefono," +
									"       ci.cs_tipo, cip.cg_banco, cip.cg_num_cuenta,cip.cs_aceptacion,"+
									"       decode (cp.cd_descripcion, null, ' ', cp.cd_descripcion ||','|| cp.cg_estado) AS plaza"+
									"    FROM comcat_if ci,  " +
									"         comrel_nafin cn,  " +
									"         com_domicilio cdom,  " +
									"         comcat_estado cest,  " +
									"		    comrel_if_epo cip, " +
									"			 comcat_plaza cp "+	//tabla para agregar pantalla de IFs relacionados
									"   WHERE ci.ic_if = cn.ic_epo_pyme_if  " +
									"		AND ci.ic_if = cip.ic_if "+
									"     AND ci.ic_if = cdom.ic_if  " +
									"     AND cdom.ic_estado = cest.ic_estado  " +
									"		AND cip.ic_plaza = cp.ic_plaza (+) "+
									"		AND cip.ic_epo = ? "  +
									"		AND cdom.cs_fiscal = ? " +
									"		AND cip.CS_VOBO_NAFIN = ? " +
									"		AND cn.cg_tipo = ? ");
									conditions.add(cveEpo);
									conditions.add("S");
									conditions.add("S");
									conditions.add("I");
		
		 if(numNafinElec!=null&&!"".equals(numNafinElec)) {
		  	 qrySentencia.append(" AND cn.ic_nafin_electronico = ? ");
			 conditions.add(numNafinElec);
			}
			/*if(nombreMandante!=null&&!"".equals(nombreMandante)) {
				qrySentencia.append (" AND ci.cg_razon_social = ? " );
				conditions.add(nombreMandante);
			}*/
			if(claveIf!=null&&!"".equals(claveIf)) {
				qrySentencia.append (" AND ci.ic_if = ? " );
				conditions.add(claveIf);
			}
			if(estado!=null&&!"".equals(estado)) {
				qrySentencia.append( " AND cest.ic_estado = ? ");
				conditions.add(estado);
			}
			
		qrySentencia.append(" ORDER BY ci.cg_razon_social ");
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions: "+conditions);
		log.info("getDocumentQueryFile (S)");
		return qrySentencia.toString();	
	}//getDocumentQueryFile

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo	=	"";
		String numnafinelect,razonsocial,calle,estado,telefono,cs_tipo,banco,num_cuenta,plaza,aceptacion;
		if ("CSV".equals(tipo)) {
			try{
				String linea = "";
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				int rowspan = 0;
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				linea = "Numero Nafin Electr�nico,Nombre,Domicilio,Estado,Tel�fono,Tipo IF,Banco,Datos de Cuenta,Plaza,Autorizaci�n,\n";
				buffer.write(linea);
				while (rs.next()) {
					numnafinelect	=	(rs.getString("NUMERO_DE_NAFIN_ELECTRONICO")==null)?"":rs.getString("NUMERO_DE_NAFIN_ELECTRONICO");
					razonsocial		=	(rs.getString("NOMBRE_O_RAZON_SOCIAL")==null)?"":rs.getString("NOMBRE_O_RAZON_SOCIAL");
					calle				=	(rs.getString("DOMICILIO")==null)?"":rs.getString("DOMICILIO");
					estado			=	(rs.getString("ESTADO")==null)?"":rs.getString("ESTADO");
					telefono			=	(rs.getString("TELEFONO")==null)?"":rs.getString("TELEFONO");
					cs_tipo			=	(rs.getString("CS_TIPO")==null)?"":rs.getString("CS_TIPO");
					banco				=	(rs.getString("CG_BANCO")==null)?"":rs.getString("CG_BANCO");
					num_cuenta		=	(rs.getString("CG_NUM_CUENTA")==null)?"":rs.getString("CG_NUM_CUENTA");
					plaza				=	(rs.getString("PLAZA")==null)?"":rs.getString("PLAZA");
					aceptacion		=	(rs.getString("CS_ACEPTACION")==null)?"":rs.getString("CS_ACEPTACION");
					linea =	numnafinelect.replace(',',' ') +","+ razonsocial.replace(',',' ') +","+ calle.replace(',',' ') +","+ estado.replace(',',' ') +","+
								telefono.replace(',',' ') +","+ cs_tipo +","+ banco.replace(',',' ') +","+ num_cuenta +","+ plaza.replace(',',' ') +","+ aceptacion + ",\n";
					buffer.write(linea);
					rowspan++;
				}
				buffer.close();
			}catch (Exception e) {
					throw new AppException("Error al generar el archivo", e);
			}	finally {
				}
		}
		return nombreArchivo;
	}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		String nombreArchivo	=	"";
		String numnafinelect,razonsocial,calle,estado,telefono,cs_tipo,banco,num_cuenta,plaza,aceptacion;
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int nRow = 0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				while (reg.next()) {
					numnafinelect	=	(reg.getString("NUMNAFINELECT")==null)?"":reg.getString("NUMNAFINELECT");
					razonsocial		=	(reg.getString("RAZONSOCIAL")==null)?"":reg.getString("RAZONSOCIAL");
					calle				=	(reg.getString("CALLE")==null)?"":reg.getString("CALLE");
					estado			=	(reg.getString("ESTADO")==null)?"":reg.getString("ESTADO");
					telefono			=	(reg.getString("TELEFONO")==null)?"":reg.getString("TELEFONO");
					cs_tipo			=	(reg.getString("CS_TIPO")==null)?"":reg.getString("CS_TIPO");
					banco				=	(reg.getString("CG_BANCO")==null)?"":reg.getString("CG_BANCO");
					num_cuenta		=	(reg.getString("CG_NUM_CUENTA")==null)?"":reg.getString("CG_NUM_CUENTA");
					plaza				=	(reg.getString("PLAZA")==null)?"":reg.getString("PLAZA");
					aceptacion		=	(reg.getString("CS_ACEPTACION")==null)?"":reg.getString("CS_ACEPTACION");
					if (nRow == 0){
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							 
						pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						session.getAttribute("iNoNafinElectronico").toString(),
						(String)session.getAttribute("sesExterno"),
						(String) session.getAttribute("strNombre"),
						(String) session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
							 
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
						pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
						pdfDoc.setTable(10,100);
						pdfDoc.setCell("Consulta de Intermediarios Financieros","celda01rep",ComunesPDF.CENTER,10);
						//pdfDoc.setCell("IFs Relacionados","celda01rep",ComunesPDF.CENTER,5);
						pdfDoc.setCell("Numero Naf�n Electr�nico","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Nombre o Raz�n Social","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Domicilio","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Estado","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Tel�fono","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Tipo IF","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Banco","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Datos de Cuenta","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Plaza","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Autorizaci�n","celda01rep",ComunesPDF.CENTER);
					}
					pdfDoc.setCell(numnafinelect,	"formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(razonsocial,	"formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(calle,			"formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(estado,			"formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(telefono, 		"formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(cs_tipo, 		"formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(banco, 			"formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(num_cuenta,		"formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(plaza,			"formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(aceptacion,		"formasmen", ComunesPDF.CENTER);
					nRow++;
				}
				if (nRow == 0)	{
					pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
					pdfDoc.addText("No se Encontro Ning�n Registro","formas",ComunesPDF.LEFT);
				}else	{
					pdfDoc.addTable();
				}
				pdfDoc.endDocument();
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			}finally {
			}
		}
		return nombreArchivo;
	}

	//GETERS
	public List getConditions() { return conditions; }
	//public String getOperacion() { return operacion; }
	//public String getPaginaNo() {	return paginaNo; }
	//public String getPaginaOffset() { return paginaOffset; }
	public String getNumNafinElec() { return numNafinElec; }
	//public String getNombreMandante() { return nombreMandante; }
	public String getEstado() { return estado; }
	
	public String getCveEpo() { return cveEpo; }
	
	public String getClaveIf() { return claveIf; }

	//SETERS
	//public void setOperacion(String newOperacion) { operacion = newOperacion; }
	//public void setPaginaNo(String newPaginaNo) { paginaNo = newPaginaNo; }
	//public void setPaginaOffset(String newPaginaOffset) { paginaOffset = newPaginaOffset; }
	public void setNumNafinElec(String numNafinElec) { this.numNafinElec = numNafinElec; }
	//public void setNombreMandante(String nombreMandante) { this.nombreMandante = nombreMandante; }
	public void setEstado(String estado) { this.estado = estado; }

	public void setCveEpo(String cveEpo) { this.cveEpo = cveEpo; }

	public void setClaveIf(String cveIf) { this.claveIf = cveIf; }
	
}