package com.netro.cadenas;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;
//import java.sql.Timestamp;
public class ConsCatalogoTasas implements  IQueryGeneratorRegExtJS {
	private static final Log log = ServiceLocator.getInstance().getLog(ConsCatalogoTasas.class);//Variable para enviar mensajes al log.

	public ConsCatalogoTasas() {  }
	private List 	conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	StringBuffer 	strQuery;
	StringBuffer qrySentencia = new StringBuffer("");

	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
 	}  
		 
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		log.info("getDocumentQueryFile(E)");
		qrySentencia = new StringBuffer();  
		qrySentencia.append(
			"/* Formatted on 2014/07/08 12:34 (Formatter Plus v4.8.8) */" +
			" SELECT DISTINCT (t1.ic_tasa) AS interclave, t1.ic_tasa AS clave, " +
			" t1.cd_nombre AS descript, t1.cd_nombre AS descript_aux, t2.cd_nombre AS moneda,  " +
			" t1.cs_disponible AS disponible,t1.cs_disponible AS disponible_aux, t1.ic_moneda AS ic_moneda, t1.ic_moneda AS ic_moneda_aux, " +
			"  t1.fg_sobretasa AS sobretasa, t1.fg_sobretasa AS sobretasa_aux, " +
			" t1.cs_creditoelec AS habilitadoce, t1.cs_creditoelec AS habilitadoce_aux" +
			"  FROM comcat_tasa t1, comcat_moneda t2" +
			" WHERE t1.ic_moneda = t2.ic_moneda(+) " +
			
			" order by  t1.ic_tasa "   
			); 
		
		log.debug("getDocumentQueryFile(S) "); 
		log.debug("qrySentencia "+qrySentencia);
		return qrySentencia.toString();		
		
 	} 
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.debug("::crearCustomFile(E)");
			try{
			} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			}
		
		log.info(":: crearCustomFile(S)");
		return "";			
	}
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		return "";   
	}
	public List getConditions() {
		return conditions;
	}
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
	
	public List catMoneda()throws Exception{
		log.info("catMoneda (E)");   
		AccesoDB 	con = new AccesoDB();
		conditions = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;  
		boolean		commit = true;
		strQuery 		= new StringBuffer();
		List varBind = new ArrayList();  
		HashMap	registros = new HashMap();
		List	dato = new ArrayList();
		String totalAdeudo = "0";
		try{
			con.conexionDB();
			strQuery.append("SELECT IC_MONEDA AS INTERCLAVE, IC_MONEDA AS CLAVE, CD_NOMBRE AS DESCRIPT "+
					
					"FROM COMCAT_MONEDA ORDER BY IC_MONEDA"
					);
			ps = con.queryPrecompilado(strQuery.toString());
			rs = ps.executeQuery();
			while( rs.next() ){
				registros = new HashMap();
				String clave =  rs.getString("clave")==null?"":rs.getString("clave");
				String descript =  rs.getString("descript")==null?"":rs.getString("descript");
				registros.put("clave",clave);
				registros.put("descripcion",descript);
				dato.add(registros);  
					
			}
			rs.close();
			ps.close();	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error catMoneda "+e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("catMoneda (S)");
		}	
		return dato;
 	}
	  
	   
	public boolean  actualizarTasas(String moneda, String descripcion, String disponible, String sobretasa, String clave,String checkOpcion, String check)throws Exception{
		log.info("actializarTasas (E)");
		AccesoDB 	con = null;
		String strQuery1 		= "";
		boolean resultado = true;
		try{  
			con = new AccesoDB();
			con.conexionDB();
			if(moneda.equals("-1")){
				moneda = "0";
				
			}
			if(check.equals("")){
				strQuery1=" UPDATE COMCAT_TASA "+
							 "  SET CD_NOMBRE='"+descripcion+"', IC_MONEDA="+moneda+", CS_DISPONIBLE='"+disponible+"',  FG_SOBRETASA="+sobretasa+", CS_CREDITOELEC = '"+checkOpcion+"'"+
						    "  WHERE IC_TASA="+clave+"";
				con.ejecutaSQL(strQuery1);
			}else{
				
				strQuery1 = " UPDATE COMCAT_TASA  SET CS_CREDITOELEC = '"+checkOpcion+"' " +
								"  WHERE IC_TASA = "+clave;
				con.ejecutaSQL(strQuery1);
			}
		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally {
          con.terminaTransaccion(resultado);
          if(con.hayConexionAbierta())
            con.cierraConexionDB();
      }
		log.debug("strQuery1 ::: "+strQuery1);
		log.debug("actializarTasas (S)");
		return resultado;
 	}
	

	
}