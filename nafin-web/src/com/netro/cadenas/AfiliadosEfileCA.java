package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class AfiliadosEfileCA implements IQueryGeneratorReg,IQueryGeneratorRegExtJS {
	public AfiliadosEfileCA() {}
	
	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(com.netro.cadenas.AfiliadosEfileCA.class);
	
	StringBuffer 	qrySentencia;
	private List 	conditions;
	private String operacion;
	private String paginaOffset;
	private String paginaNo;

	private String claveBancoFondeo;
	private String claveEpo;	
	private String txt_nafelec;
   private String txt_nombre;
   private String claveIf;
	private String desde;
	private String hasta;
	private String expedienteCargado;
	
	//private String ic_pyme;
	//private String chkPymesConvenio;
	//private String nombre_pyme_aux;
	
public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		log.info("crearPageCustomFile(E)");
		String nombreArchivo="";
		
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico") == null ? "" : session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				pdfDoc.setTable(9,100);
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No SIRAC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre o Raz�n Social","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Parametrizaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Nafin Electr�nico","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Expediente Cargado","celda01",ComunesPDF.CENTER);
															
				while (reg.next()) {
					String epo 		= reg.getString("EPO")==null?"":reg.getString("EPO");
					String ic_if 		= reg.getString("IF")==null?"":reg.getString("IF");
					String sirac 		= reg.getString("NUMERO_SIRAC")==null?"":reg.getString("NUMERO_SIRAC");
					String pyme 		= reg.getString("PYME")==null?"":reg.getString("PYME");
					String moneda 	= reg.getString("MONEDA")==null?"":reg.getString("MONEDA");
					String rfc 		= reg.getString("RFC")==null?"":reg.getString("RFC");
					String fecha 		= reg.getString("FECHA_PARAMETRIZACION")==null?"":reg.getString("FECHA_PARAMETRIZACION");
					String nafin 		= reg.getString("NUM_NAFIN_ELECTRONICO")==null?"":reg.getString("NUM_NAFIN_ELECTRONICO");
					String expediente 	= reg.getString("EXPEDIENTE_EFILE")==null?"":reg.getString("EXPEDIENTE_EFILE");
								
					pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ic_if,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nafin,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(expediente,"formas",ComunesPDF.CENTER);					
				}
				
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally {
				log.info("crearPageCustomFile(S)");
			}
		}
		return nombreArchivo;
}


	/* M�todos �para nueva versi�n */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.info("crearCustomFile(E)");
		CreaArchivo 	archivo 				= new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();		
		String nombreArchivo = "";
		
		try{

			contenidoArchivo.append("EPO,");
			contenidoArchivo.append("IF,");
			contenidoArchivo.append("NUMERO SIRAC,");
			contenidoArchivo.append("PYME,");
			contenidoArchivo.append("MONEDA,");
			contenidoArchivo.append("RFC,");
			contenidoArchivo.append("FECHA PARAMETRIZACION,");
			contenidoArchivo.append("NUM NAFIN ELECTRONICO,");
			contenidoArchivo.append("EXPEDIENTE EFILE \n");
			
			while(rs.next()){

				String epo 		= rs.getString("EPO")==null?"":rs.getString("EPO");
				String ic_if 		= rs.getString("IF")==null?"":rs.getString("IF");
				String sirac 		= rs.getString("NUMERO_SIRAC")==null?"":rs.getString("NUMERO_SIRAC");
				String pyme 		= rs.getString("PYME")==null?"":rs.getString("PYME");
				String moneda 	= rs.getString("MONEDA")==null?"":rs.getString("MONEDA");
				String rfc 		= rs.getString("RFC")==null?"":rs.getString("RFC");
				String fecha 		= rs.getString("FECHA_PARAMETRIZACION")==null?"":rs.getString("FECHA_PARAMETRIZACION");
				String nafin 		= rs.getString("NUM_NAFIN_ELECTRONICO")==null?"":rs.getString("NUM_NAFIN_ELECTRONICO");
				String expediente 	= rs.getString("EXPEDIENTE_EFILE")==null?"":rs.getString("EXPEDIENTE_EFILE");
							
				contenidoArchivo.append(epo.replaceAll(","," ")+",");
				contenidoArchivo.append(ic_if.replaceAll(","," ")+",");	
				contenidoArchivo.append(sirac+",");	
				contenidoArchivo.append(pyme.replaceAll(","," ")+",");	
				contenidoArchivo.append(moneda+",");	
				contenidoArchivo.append(rfc+",");
				contenidoArchivo.append(fecha+",");				
				contenidoArchivo.append(nafin+",");
				contenidoArchivo.append(expediente);
				contenidoArchivo.append("\n");
			}//fin while
			
			if(archivo.make(contenidoArchivo.toString(), path, ".csv")){
				nombreArchivo = archivo.getNombre();
			}
		
		}catch(Exception e){
			log.error("Error en la generaci�n de archivo Excel",e);
		}
		return nombreArchivo;
	}	
	
	/**
	 * M�todo que obtiene los totales de la consulta
	 * @return ""
	 */
	public String getAggregateCalculationQuery() {
		return "";
	}//getAggregateCalculationQuery
	
	/**
	 * M�todo que obtiene las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL que contiene las llaves primarias
	 */
	public String getDocumentQuery(){
		
		log.info("getDocumentQuery (E)");
		qrySentencia 		= new StringBuffer();
		conditions 			= new ArrayList();
		
		qrySentencia.append(
			" SELECT 										"  +
			"	/*+ use_nl(cpi,epo,cctabanc,cif,pyme,m,cn) index(cpi IN_COMREL_PYME_IF_03_NUK)*/  "  +
			"			pyme.ic_pyme 	AS ic_pyme, 	" 	+
         "        m.ic_moneda 	AS ic_moneda, 	" 	+
         "        epo.ic_epo 		AS ic_epo, 		" 	+
         "        cif.ic_if 		AS ic_if, 		" 	+
         "        pyme.cg_rfc 	AS cg_rfc 		" 	+ // Debug info: pendiente de revisar
			"   FROM comrel_pyme_if 			cpi,   		"  +
			"        comcat_epo 					epo,  		"  +
			"        comrel_cuenta_bancaria 	cctabanc,  	"  +
			"        comcat_if 					cif,        "  +
			"        comcat_pyme 				pyme,  		"  +
			"        comcat_moneda 				m,  			"  +
			"        comrel_nafin 				cn  			"  +
			"  WHERE cpi.ic_cuenta_bancaria	= cctabanc.ic_cuenta_bancaria "  +
			"    AND cpi.ic_if               = cif.ic_if  						"  +
			"    AND cpi.ic_epo              = epo.ic_epo   					"  +
			"    AND cctabanc.ic_moneda      = m.ic_moneda  					"  +
			"    AND cctabanc.ic_pyme        = pyme.ic_pyme    				"  +
			//"    AND cpi.df_autorizacion is null  									"  +
			"    AND cn.ic_epo_pyme_if       = pyme.ic_pyme  					"  +
			"    AND cpi.cs_borrado          = ?  									"  +
			"    AND cctabanc.cs_borrado     = ? 									"  +
			"    AND cn.cg_tipo              = ? 									");
		
		   conditions.add("N");
		   conditions.add("N");
		   conditions.add("P");
		   
			// BANCO DE FONDEO
			if(!claveBancoFondeo.equals("")){
				qrySentencia.append(" AND EPO.IC_BANCO_FONDEO = ? ");
            conditions.add(claveBancoFondeo);
			}
			//  EPO 
			if(!claveEpo.equals("")){
				qrySentencia.append(" AND EPO.IC_EPO = ? ");
            conditions.add(claveEpo);
			}
			// PROVEEDOR 
			if(!txt_nafelec.equals("")){
				qrySentencia.append(" AND cn.ic_nafin_electronico = ? ");
            conditions.add(txt_nafelec);
			}
			// INTERMEDIARIO FINANCIERO 
			if(!claveIf.equals("")){
				qrySentencia.append(" AND CIF.IC_IF = ? ");
            conditions.add(claveIf);
			}
			// FECHA DE PARAMETRIZACION 
			if(!desde.equals("")){
				qrySentencia.append(" AND cpi.df_vobo_if >= TO_DATE(?, 'dd/mm/yyyy')  ");
            conditions.add(desde);
			}
			if(!hasta.equals("")){
				qrySentencia.append(" AND cpi.df_vobo_if < TO_DATE(?, 'dd/mm/yyyy')+1 ");
            conditions.add(hasta);
			}
			// EXPEDIENTE CARGADO 
			if(expedienteCargado != null && !expedienteCargado.equals("")){
				qrySentencia.append(" AND pyme.cs_expediente_efile = ? ");
            conditions.add(expedienteCargado);
			}

		qrySentencia.append(" ORDER BY epo.cg_razon_social, cif.cg_razon_social, pyme.cg_razon_social  ASC ");
		
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions:   "+conditions);
		
		log.info("getDocumentQuery (S)");
		
		return qrySentencia.toString();
		
	}//getDocumentQuery
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL que servira para obtener la informaci�n
	 * 	completa de los registros con las llaves primarias especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		
		log.info("getDocumentSummaryQueryForIds (E)");
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append( 
			" SELECT 																						"  +
			"	/*+ use_nl(cpi,epo,cctabanc,cif,pyme,m,cn) index(cpi IN_COMREL_PYME_IF_03_NUK)*/  "  +
			"			epo.cg_razon_social							AS EPO,							"  +
			"			cif.cg_razon_social							AS	IF,							"  +
			"			pyme.in_numero_sirac 						AS NUMERO_SIRAC,  			"  +
			"        pyme.cg_razon_social 						AS PYME,  						"  +
			"        m.cd_nombre 									AS MONEDA,  					"  +
			"        pyme.cg_rfc 									AS RFC,  						"  +
			"        TO_CHAR(cpi.df_vobo_if, 'dd/mm/yyyy') 	AS FECHA_PARAMETRIZACION,  "  +
			"        cn.ic_nafin_electronico 					AS NUM_NAFIN_ELECTRONICO,  "  +
			"			decode(pyme.cs_expediente_efile,'N','NO','SI') AS EXPEDIENTE_EFILE "  +
			"   FROM comrel_pyme_if cpi,   					"  +
			"        comcat_epo epo,  							"  +
			"        comrel_cuenta_bancaria cctabanc,  	"  +
			"        comcat_if cif,        					"  +
			"        comcat_pyme pyme,  						"  +
			"        comcat_moneda m,  						"  +
			"        comrel_nafin cn  							"  +
			"  WHERE cpi.ic_cuenta_bancaria	= cctabanc.ic_cuenta_bancaria "  +
			"    AND cpi.ic_if               = cif.ic_if  						"  +
			"    AND cpi.ic_epo              = epo.ic_epo   					"  +
			"    AND cctabanc.ic_moneda      = m.ic_moneda  					"  +
			"    AND cctabanc.ic_pyme        = pyme.ic_pyme    				"  +
			//"    AND cpi.df_autorizacion is null  									"  +
			"    AND cn.ic_epo_pyme_if       = pyme.ic_pyme  					"  +
			"    AND cpi.cs_borrado          = ?  									"  +
			"    AND cctabanc.cs_borrado     = ? 									"  +
			"    AND cn.cg_tipo              = ? 									"  +
         "    AND ( ");
      
      conditions.add("N");
      conditions.add("N");
      conditions.add("P");
									
		for(int i=0;i<pageIds.size();i++) {
			
			List lItem = (ArrayList)pageIds.get(i);
			
			if(i>0){
				qrySentencia.append(" OR ");
			}
			
			qrySentencia.append(	" ( " + 
										"		pyme.ic_pyme 	= ? AND 	" +
										"  	m.ic_moneda 	= ? AND 	" +  
										"  	epo.ic_epo 		= ? AND 	" +
										"  	cif.ic_if 		= ? 		" + 
										" ) " ); 
			
			conditions.add(new Long(lItem.get(0).toString()));
			conditions.add(new Long(lItem.get(1).toString()));
			conditions.add(new Long(lItem.get(2).toString()));
         conditions.add(new Long(lItem.get(3).toString()));
					
		}
		
		qrySentencia.append(")");
		
		log.debug("qrySentencia: "+qrySentencia );
		log.debug("conditions:   "+conditions   );
		
		log.info("getDocumentSummaryQueryForIds (S)");
		
		return qrySentencia.toString();
	}	
 
	public String getDocumentQueryFile(){
		
		log.info("getDocumentQueryFile (E)");
		
		qrySentencia 		= new StringBuffer();
		conditions 			= new ArrayList();
			
		qrySentencia.append( 
			" SELECT 																						"  +
			"	/*+ use_nl(cpi,epo,cctabanc,cif,pyme,m,cn) index(cpi IN_COMREL_PYME_IF_03_NUK)*/  "  +
			"			epo.cg_razon_social							AS EPO,							"  +
			"			cif.cg_razon_social							AS	IF,							"  +
			"			pyme.in_numero_sirac 						AS NUMERO_SIRAC,  			"  +
			"        pyme.cg_razon_social 						AS PYME,  						"  +
			"        m.cd_nombre 									AS MONEDA,  					"  +
			"        pyme.cg_rfc 									AS RFC,  						"  +
			"        TO_CHAR(cpi.df_vobo_if, 'dd/mm/yyyy') 	AS FECHA_PARAMETRIZACION,  "  +
			"        cn.ic_nafin_electronico 					AS NUM_NAFIN_ELECTRONICO,  "  +
			"			decode(pyme.cs_expediente_efile,'N','NO','SI') AS EXPEDIENTE_EFILE "  +
			"   FROM comrel_pyme_if cpi,   					"  +
			"        comcat_epo epo,  							"  +
			"        comrel_cuenta_bancaria cctabanc,  	"  +
			"        comcat_if cif,        					"  +
			"        comcat_pyme pyme,  						"  +
			"        comcat_moneda m,  						"  +
			"        comrel_nafin cn  							"  +
			"  WHERE cpi.ic_cuenta_bancaria	= cctabanc.ic_cuenta_bancaria "  +
			"    AND cpi.ic_if               = cif.ic_if  						"  +
			"    AND cpi.ic_epo              = epo.ic_epo   					"  +
			"    AND cctabanc.ic_moneda      = m.ic_moneda  					"  +
			"    AND cctabanc.ic_pyme        = pyme.ic_pyme    				"  +
			//"    AND cpi.df_autorizacion is null  									"  +
			"    AND cn.ic_epo_pyme_if       = pyme.ic_pyme  					"  +
			"    AND cpi.cs_borrado          = ?  									"  +
			"    AND cctabanc.cs_borrado     = ? 									"  +
			"    AND cn.cg_tipo              = ? 									");
		
		conditions.add("N");
		conditions.add("N");
		conditions.add("P");

		// BANCO DE FONDEO
		if(!claveBancoFondeo.equals("")){
			qrySentencia.append(" AND EPO.IC_BANCO_FONDEO = ? ");
         conditions.add(claveBancoFondeo);
		}
		//  EPO 
		if(!claveEpo.equals("")){
			qrySentencia.append(" AND EPO.IC_EPO = ? ");
         conditions.add(claveEpo);
		}
		// PROVEEDOR 
		if(!txt_nafelec.equals("")){
			qrySentencia.append(" AND cn.ic_nafin_electronico = ? ");
         conditions.add(txt_nafelec);
		}
		// INTERMEDIARIO FINANCIERO 
		if(!claveIf.equals("")){
			qrySentencia.append(" AND CIF.IC_IF = ? ");
         conditions.add(claveIf);
		}
		// FECHA DE PARAMETRIZACION 
		if(!desde.equals("")){
			qrySentencia.append(" AND cpi.df_vobo_if >= TO_DATE(?, 'dd/mm/yyyy')  ");
         conditions.add(desde);
		}
		if(!hasta.equals("")){
			qrySentencia.append(" AND cpi.df_vobo_if < TO_DATE(?, 'dd/mm/yyyy')+1 ");
         conditions.add(hasta);
		}
		// EXPEDIENTE CARGADO 
		if(expedienteCargado != null && !expedienteCargado.equals("")){
			qrySentencia.append(" AND pyme.cs_expediente_efile = ? ");
         conditions.add(expedienteCargado);
		}

		qrySentencia.append(" ORDER BY EPO, IF, PYME ASC ");
		
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions:   "+conditions);
		
		log.info("getDocumentQueryFile (S)");
		
		return qrySentencia.toString();
		
	}
 
	public String getQueryPymesExpediente(){
		
		log.info("getQueryPymesSinExpediente (E)");
		qrySentencia 		= new StringBuffer();
		conditions 			= new ArrayList();
		
		qrySentencia.append(
			" SELECT 										"  +
			"	/*+ use_nl(cpi,epo,cctabanc,cif,pyme,m,cn) index(cpi IN_COMREL_PYME_IF_03_NUK)*/  "  +
			"			pyme.cs_expediente_efile, 	" 	+
      "     pyme.cg_rfc 	AS cg_rfc 		" 	+ // Debug info: pendiente de revisar
			"   FROM comrel_pyme_if 			cpi,   		"  +
			"        comcat_epo 					epo,  		"  +
			"        comrel_cuenta_bancaria 	cctabanc,  	"  +
			"        comcat_if 					cif,        "  +
			"        comcat_pyme 				pyme,  		"  +
			"        comcat_moneda 				m,  			"  +
			"        comrel_nafin 				cn  			"  +
			"  WHERE cpi.ic_cuenta_bancaria	= cctabanc.ic_cuenta_bancaria "  +
			"    AND cpi.ic_if               = cif.ic_if  						"  +
			"    AND cpi.ic_epo              = epo.ic_epo   					"  +
			"    AND cctabanc.ic_moneda      = m.ic_moneda  					"  +
			"    AND cctabanc.ic_pyme        = pyme.ic_pyme    				"  +
			"    AND cn.ic_epo_pyme_if       = pyme.ic_pyme  					"  +
			"    AND cpi.cs_borrado          = ?  									"  +
			"    AND cctabanc.cs_borrado     = ? 									"  +
			"    AND cn.cg_tipo              = ? 									");
		
		   conditions.add("N");
		   conditions.add("N");
		   conditions.add("P");
		   
			// BANCO DE FONDEO
			if(!claveBancoFondeo.equals("")){
				qrySentencia.append(" AND EPO.IC_BANCO_FONDEO = ? ");
            conditions.add(claveBancoFondeo);
			}
			//  EPO 
			if(!claveEpo.equals("")){
				qrySentencia.append(" AND cpi.IC_EPO = ? ");
            conditions.add(claveEpo);
			}
			// PROVEEDOR 
			if(!txt_nafelec.equals("")){
				qrySentencia.append(" AND cn.ic_nafin_electronico = ? ");
            conditions.add(txt_nafelec);
			}
			// INTERMEDIARIO FINANCIERO 
			if(!claveIf.equals("")){
				qrySentencia.append(" AND cpi.IC_IF = ? ");
            conditions.add(claveIf);
			}
			// FECHA DE PARAMETRIZACION 
			if(!desde.equals("")){
				qrySentencia.append(" AND cpi.df_vobo_if >= TO_DATE(?, 'dd/mm/yyyy')  ");
            conditions.add(desde);
			}
			if(!hasta.equals("")){
				qrySentencia.append(" AND cpi.df_vobo_if < TO_DATE(?, 'dd/mm/yyyy')+1 ");
            conditions.add(hasta);
			}
			// EXPEDIENTE CARGADO 
			if(expedienteCargado != null && !expedienteCargado.equals("")){
				qrySentencia.append(" AND pyme.cs_expediente_efile = ? ");
            conditions.add(expedienteCargado);
			}

		qrySentencia.append(" GROUP BY pyme.cg_rfc, pyme.cs_expediente_efile ");
		
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions:   "+conditions);
		
		log.info("getQueryPymesSinExpediente (S)");
		
		return qrySentencia.toString();
		
	}//getDocumentQuery 
 
 
	// GETERS
	public List 	getConditions() 			{ return conditions; 			}
	public String 	getOperacion() 			{ return operacion; 				}
	public String 	getPaginaNo() 				{ return paginaNo; 				}
	public String 	getPaginaOffset() 		{ return paginaOffset; 			}
	// GETERS - Parametros de la Consulta
	public String 	getClaveBancoFondeo()	{ return claveBancoFondeo; 	}
   public String 	getClaveEpo()				{ return claveEpo; 				}
	public String 	getTxt_nafelec() 			{ return txt_nafelec; 			}
	public String 	getTxt_nombre() 			{ return txt_nombre; 			}
	public String 	getClaveIf()				{ return claveIf; 				}
	public String 	getDesde() 					{ return desde; 					}
	public String 	getHasta() 					{ return hasta; 					}
	public String 	getExpedienteCargado() 	{ return expedienteCargado; 	}
	//public String getIc_pyme() 				{ return ic_pyme; 				}
	//public String getChkPymesConvenio() 	{ return chkPymesConvenio; 	}
	//public String getNombre_pyme_aux() 	{ return nombre_pyme_aux; 		}
	
 
	// SETERS
	public void setOperacion(String newOperacion) 					{ operacion 					= newOperacion; 		}
	public void setPaginaNo(String newPaginaNo) 						{ paginaNo 						= newPaginaNo; 		}
	public void setPaginaOffset(String newPaginaOffset) 			{ paginaOffset 				= newPaginaOffset; 	}
	// SETERS - Parametros de la Consulta
   public void setClaveBancoFondeo(String claveBancoFondeo) 	{ this.claveBancoFondeo 	= claveBancoFondeo; 	}
   public void setClaveEpo(String claveEpo) 							{ this.claveEpo 				= claveEpo; 			}	
	public void setTxt_nafelec(String txt_nafelec) 					{ this.txt_nafelec 			= txt_nafelec; 		}
	public void setTxt_nombre(String txt_nombre) 					{ this.txt_nombre 			= txt_nombre; 			}
	public void setClaveIf(String claveIf) 							{ this.claveIf 				= claveIf; 				}
	public void setDesde(String desde) 									{ this.desde 					= desde; 				}
	public void setHasta(String hasta) 									{ this.hasta 					= hasta; 				}
	public void setExpedienteCargado(String expedienteCargado) 	{ this.expedienteCargado 	= expedienteCargado; }
	//public void setIc_pyme(String ic_pyme) 							{ this.ic_pyme 				= ic_pyme; 				}
	//public void setChkPymesConvenio(String chkPymesConvenio) 	{ this.chkPymesConvenio 	= chkPymesConvenio; 	}
	//public void setNombre_pyme_aux(String nombre_pyme_aux) 	{ this.nombre_pyme_aux 		= nombre_pyme_aux; 	}
	

}
