package com.netro.cadenas;

import com.netro.exception.NafinException;

import java.awt.image.BufferedImage;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.SimpleImageInfo;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;


/**
 * Esta clase representa el dise�o de imagen una Epo.
 * (com_diseno_epo)
 * @author Gilberto Aparicio 01/12/2005
 *
 */
public class DisenoEpo implements Serializable {
	
	public DisenoEpo(){};
	
	/**
	 * Establece la clave de la epo (ic_epo)
	 * Si claveEepo = "nafin" entonces autom�ticamente har� un cambio interno de clave 
	 * a "0" donde 0 es representa al dise�o predeterminado
	 * @param claveEpo Clave de la epo
	 */
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = ((claveEpo != null && claveEpo.equals("nafin"))?"0":claveEpo);
		this.setRutas();
	}
	
	/**
	 * Obtiene la clave de la epo (ic_epo)
	 * Si claveEpo = 0 entonces autom�ticamente har� un cambio interno de clave 
	 * y regresar� "nafin"
	 * @return Cadena con Clave de la epo
	 */
	public String getClaveEpo() {
		return (this.claveEpo != null && this.claveEpo.equals("0"))?
				"nafin":this.claveEpo;
	}


	/**
	 * Establece la fecha de alta o modificacion del dise�o
	 * @param fechaModificacion fecha de modificacion.
	 */
	public void setFechaModificacion(java.util.Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	/**
	 * Obtiene la fecha de alta o modificacion del dise�o
	 * @return (objeto Date) fecha de modificacion.
	 */
	public java.util.Date getFechaModificacion() {
		return this.fechaModificacion;
	}
	
	/**
	 * Establece si es borrado o no
	 * @param boorado true si est� marcado como borrado o false de lo contrario.
	 */
	public void setBorrado(boolean borrado) {
		this.borrado = borrado;
	}
	
	/**
	 * Obtiene si est� marcado como borrado o no
	 * @return true si est� marcado como borrado o false de lo contrario.
	 */
	public boolean getBorrado() {
		return this.borrado;
	}

	/**
	 * Genera el archivo ZIP a partir del contenido en BD.
	 * El nombre del archivo estar� conformado por la clave de la epo y la
	 * extension zip. por ejemplo: 234.zip
	 * Para el caso de que la clave sea 0, entonces el nombre del archivo zip ser�
	 * nafin.zip
	 * @param rutaDestino Ruta destino donde se generara el archivo zip
	 */
	public void generarArchivoZip(String rutaDestino) 
			throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte[] arrByte = new byte[4096];
		java.io.OutputStream outstream = null;
		
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (this.claveEpo == null) {
				throw new Exception("La clave de la epo no estan establecidos");
			}
			
			if (!this.existeDisenoEpo()) {
				throw new Exception("No existe dise�o personalizado para la epo");
			}
			
		} catch(Exception e) {
			System.out.println("Error en los parametros establecidos. " + e.getMessage() +
					" claveEpo=" + this.claveEpo + "\n" +
					" rutaDestino=" + rutaDestino);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		InputStream inStream = null;
		try {	
			con.conexionDB();
			String strSQL = "SELECT bi_archivo_zip " +
					" FROM com_diseno_epo " +
					" WHERE ic_epo = ? ";
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(this.claveEpo));
			rs = ps.executeQuery();
			
			rs.next();
			
			//java.sql.Blob blob = (java.sql.Blob)rs.getObject("bi_archivo_zip");
			//inStream = ((oracle.sql.BLOB)blob).getBinaryStream();
			inStream = rs.getBinaryStream("bi_archivo_zip");
			rs.close();
			ps.close();
			outstream = 
					new BufferedOutputStream(
							new FileOutputStream(rutaDestino + 
							this.getClaveEpo() + ".zip" ));
			
			int i = 0;
			while((i = inStream.read(arrByte, 0, arrByte.length)) != -1) {
				outstream.write(arrByte, 0, i);
			}
			outstream.close();
			inStream.close();
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}



	/**
	 * Guarda el archivo ZIP que contiene el dise�o de la EPO especificada
	 * en la BD.
	 * @param rutaArchivoZip ruta fisica del archivo Zip que contiene el dise�o Epo
	 */
	public void guardar(String rutaArchivoZip) throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte abyte0[] = new byte[4096];
		java.io.OutputStream outstream = null;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (this.claveEpo == null || rutaArchivoZip == null) {
				throw new Exception("La clave de la epo y ruta de archivo no estan establecidos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros establecidos. " + e.getMessage() +
					" claveEpo=" + this.claveEpo +
					" rutaArchivoZip=" + rutaArchivoZip);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {	
			File archivoZip = new File(rutaArchivoZip);
			FileInputStream fileinputstream = new FileInputStream(archivoZip);
	
			con.conexionDB();
			String strSQL = "";
/*			String strSQL = "SELECT count(*) as numRegistros " +
					" from com_diseno_epo " +
					" WHERE ic_epo = ? ";
			
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(this.getClaveEpo()));
			rs = ps.executeQuery();
			
	//		System.out.println("GEAG::" + strSQL);
			
			rs.next();
			boolean existeRegistro = (rs.getInt("numRegistros") == 0)?false:true;
			rs.close();
			ps.close();
*/
			if (!this.existeDisenoEpo()) {
				//Se mete un valor temporal '0'
				strSQL = "INSERT INTO com_diseno_epo (ic_epo, bi_archivo_zip) " +
						" VALUES (?, empty_blob()) ";
				ps = con.queryPrecompilado(strSQL);
				ps.setInt(1, Integer.parseInt(this.claveEpo));
				ps.executeUpdate();
				
	//			System.out.println("GEAG::" + strSQL);
				
				ps.close();
			}

			strSQL = 
					" UPDATE com_diseno_epo " +
					" SET df_modificacion = sysdate, " +
					" 	bi_archivo_zip = ?, " +
					" 	cs_borrado = ? " +
					" WHERE ic_epo = ? ";
			
			ps = con.queryPrecompilado(strSQL);
			ps.setBinaryStream(1, fileinputstream, (int)archivoZip.length());
			ps.setString(2, "N");
			ps.setInt(3, Integer.parseInt(this.claveEpo));
			ps.executeUpdate();
			ps.close();

			fileinputstream.close();
			
/*			strSQL = "SELECT bi_archivo_zip " +
					" FROM com_diseno_epo " +
					" WHERE ic_epo = ? FOR UPDATE NOWAIT ";
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(this.claveEpo));
			rs = ps.executeQuery();
			
	//		System.out.println("GEAG::" + strSQL);
			
			rs.next();
			
			java.sql.Blob blob = (java.sql.Blob)rs.getObject("bi_archivo_zip");
			
			outstream = ((oracle.sql.BLOB)blob).getBinaryOutputStream();
			
			int i;
			while((i = fileinputstream.read(abyte0)) != -1)  {
				outstream.write(abyte0, 0, i);
			}
			outstream.close();
			rs.close();
			ps.close();
*/
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Eliminar el dise�o de la EPO, lo cual permite 
	 * regresar al dise�o predeterminado
	 * @param rutaBase Ruta base donde se encuentran los archivos a eliminar
	 */
	public void eliminar(String rutaBase) 
			throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (this.claveEpo == null) {
				throw new Exception("La clave de la epo no esta establecida");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros establecidos. " + e.getMessage() +
					" claveEpo=" + this.claveEpo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************
		this.eliminarFS(rutaBase);
		try {	
			con.conexionDB();
			String strSQL = 
					" UPDATE com_diseno_epo " +
					" SET cs_borrado = 'S' " +
					" WHERE ic_epo = ? " ;
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(this.claveEpo));
			ps.executeUpdate();
			ps.close();
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Eliminar el dise�o de la EPO del sistema de archivos, lo cual permite 
	 * regresar al dise�o predeterminado
	 * @param rutaBase Ruta F�sica Base, a partir de donde se 
	 *  		encuentran los archivos a eliminar
	 */
	public void eliminarFS(String rutaBase) {
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (rutaBase == null || this.claveEpo == null) {
				throw new Exception("Los parametros/atributos no pueden ser nulos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros/atributos establecidos. " 
					+ e.getMessage() + "\n" +
					" claveEpo=" + this.claveEpo);
			//throw new NafinException("SIST0001");
		}
		//***************************************************************************************
		try {	
			List nombresArchivos = this.getListaNombresArchivosBD();
			Iterator it = nombresArchivos.iterator();

			while (it.hasNext()) {
				String nombreArchivo = (String) it.next();
				String rutaArchivo = rutaBase + this.getRutaDefinitiva(nombreArchivo);
				File f = new File(rutaArchivo);
				if (f.exists()) {
					f.delete();
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * Determina si existe o no dise�o para la EPO en la BD.
	 * Si el dise�o existe aunque est� marcado como borrado se regresa
	 * "true" de cualquier modo
	 * Debe estar establecido el la claveEpo antes de llamar este m�todo
	 * 
	 */
	public boolean existeDisenoEpo() 
			throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean existe = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (this.claveEpo == null) {
				throw new Exception("La clave de la epo");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros establecidos. " 
					+ e.getMessage() + "\n" +
					" claveEpo=" + this.claveEpo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			String strSQL = "SELECT count(*) as numRegistros " +
					" from com_diseno_epo " +
					" WHERE ic_epo = ? ";
			
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(this.claveEpo));
			rs = ps.executeQuery();
			rs.next();
			existe = (rs.getInt("numRegistros") == 0)?false:true;
			rs.close();
			ps.close();
			return existe;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * Descomprime los archivos de dise�o de la EPO provenientes de 
	 * un archivo ZIP en ruta especificada
	 *
	 * @param rutaBase Ruta (Fisica) destino donde se descompacta el dise�o de la Epo
	 * @param rutaArchivoZip Ruta (Fisica) del archivo zip.
	 * @param modo Finalidad de la descompresi�n de archivos: para preview (PREVIEW)
	 *  		� definitiva (FINAL). 
	 * 
	 * @return String que especifica el nombre del logo para mostrar en la vista previa
	 *  (debido a que si existe XXX_home.gif este ser� el mostrado, 
	 *  de lo contrario XXX.gif)
	 * 		
	 */
	public String descomprimirArchivosDeFS(String rutaBase,
			String rutaArchivoZip, String modo) throws NafinException {

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (rutaBase == null || rutaArchivoZip == null || modo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			if (!(modo.equals("PREVIEW") || modo.equals("FINAL"))) {
				throw new Exception("El parametro de modo no es valido: " + modo);
			}
			if (this.claveEpo == null) {
				throw new Exception("La clave de la epo no esta establecida");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos/establecidos. " + 
					e.getMessage() + "\n" +
					"rutaBase=" + rutaBase + "\n" +
					"rutaArchivoZip=" + rutaArchivoZip + "\n" +
					"claveEpo=" + this.claveEpo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		return descomprimirArchivos("ARCHIVO", modo,  rutaBase, rutaArchivoZip);
	}

	/**
	 * Descomprime los archivos de dise�o de la EPO provenientes de 
	 * un archivo ZIP en ruta especificada
	 *
	 * @param rutaBase Ruta (Fisica) destino donde se descompacta el dise�o de la Epo
	 * @param rutaArchivoZip Ruta (Fisica) del archivo zip.
	 * @param modo Finalidad de la descompresi�n de archivos: para preview (PREVIEW)
	 *  		� definitiva (FINAL). 
	 *       
	 * @return String que especifica el nombre del logo para mostrar en la vista previa
	 *  (debido a que si existe XXX_home.gif este ser� el mostrado, 
	 *  de lo contrario XXX.gif)
	 * 		
	 */
	public String descomprimirArchivosDeBD(String rutaBase, String modo)
			throws NafinException {
		//la ruta del archivo no aplica porque la informaci�n s eobtiene de BD

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (rutaBase == null || modo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			if (!(modo.equals("PREVIEW") || modo.equals("FINAL"))) {
				throw new Exception("El modo no es valido: " + modo);
			}
			if (this.claveEpo == null) {
				throw new Exception("La clave de la epo no esta establecida");
			}
			if (!existeDisenoEpo()) {
				throw new Exception("El dise�o para la epo no existe en BD");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos/establecidos. " + 
					e.getMessage() + "\n" +
					"rutaBase=" + rutaBase + "\n" +
					"modo=" + modo + "\n" +
					"claveEpo=" + this.claveEpo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		String rutaArchivoZip = null;		//No aplica porque los datos provienen de la BD;
		return descomprimirArchivos("BASE_DATOS", modo, rutaBase, rutaArchivoZip);
	}

	/**
	 * Obtiene la lista de los nombres de archivos contenidos 
	 * en el archivo zip de la BD
	 * @return Lista con el contenido del archivo zip. Si no hay
	 * archivos regresa una lista vacia, nunca null.
	 */
	public List getListaNombresArchivosBD() 
			throws NafinException {
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (this.claveEpo == null) {
				throw new Exception("La clave de la epo no esta establecida");
			}
			if (!existeDisenoEpo()) {
				throw new Exception("El dise�o para la epo no existe en BD");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos/establecidos. " + 
					e.getMessage() + "\n" +
					"claveEpo=" + this.claveEpo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		String rutaArchivoZip = null;
		try { 
			return this.getListaNombresArchivos("BASE_DATOS", rutaArchivoZip);
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}
	}


	/**
	 * Obtiene la informaci�n del dise�o todas las epos.
	 * 
	 * @return Lista con la informaci�n de los dise�os (objetos DisenoEpo).
	 * 		Si no hay dise�os, regresa un vector vacio, nunca null.
	 */
	public static List getInfoDisenosEpos() throws NafinException {
		List disenosEpos = new ArrayList();

		AccesoDB con = new AccesoDB();
		String logo = "";
				
		try {
			con.conexionDB();
			String strSQL = 
					" SELECT ic_epo, " +
					" df_modificacion, cs_borrado " +
					" FROM com_diseno_epo " +
					" ORDER BY ic_epo";
			ResultSet rs = con.queryDB(strSQL);
			while (rs.next()) {
				DisenoEpo diseno = new DisenoEpo();
				diseno.setClaveEpo(rs.getString("ic_epo"));
				diseno.setBorrado(rs.getString("cs_borrado").equals("S"));
				diseno.setFechaModificacion(rs.getTimestamp("df_modificacion"));
				disenosEpos.add(diseno);
			}
			rs.close();
			con.cierraStatement();
			return disenosEpos;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Verifica que el contenido del archivo sea el correcto 
	 * @param rutaArchivoZip Ruta Fidica del archivo zip a validar
	 * 
	 * @return true si es valido o false de lo contrario
	 */
	public boolean validar(String rutaArchivoZip) {
		boolean valido = false;
		List archivosDiseno = new ArrayList();
		
		if (/*tipoOperacion != null && */this.claveEpo != null) {
			String claveEpo = this.getClaveEpo();
			
//			if (tipoOperacion.equals("MODIFICACION") || 
//					tipoOperacion.equals("CAPTURA")) {
						
			archivosDiseno.add("ba_lat_" + claveEpo + ".jsp");
			archivosDiseno.add("ba1_" + claveEpo + ".gif");
			archivosDiseno.add("ba2_" + claveEpo + ".gif");
			archivosDiseno.add("ba3_" + claveEpo + ".gif");
			archivosDiseno.add("ba_" + claveEpo + ".gif");
			archivosDiseno.add(claveEpo + ".gif");
			archivosDiseno.add("noticias_" + claveEpo + ".jsp");
			archivosDiseno.add(claveEpo + ".css");
			archivosDiseno.add(claveEpo + ".js");
			archivosDiseno.add(claveEpo + ".xml");

//			}
//			if (tipoOperacion.equals("MODIFICACION")) {
//				archivosDiseno.add(this.getClaveEpo() + "_home.gif"); //opcional
//			}
			
			
//			if (tipoOperacion.equals("CAPTURA")) {
			List listaNombresArchivosEnZip = new ArrayList();

			try {
				listaNombresArchivosEnZip = getListaNombresArchivosFS(rutaArchivoZip);
			} catch (IOException e) {
				e.printStackTrace();
			}
				//Todos los elementos de archivosDiseno deben estar en listaNombresArchivosEnZip
				
			valido = 
					(listaNombresArchivosEnZip.
							containsAll(archivosDiseno))?true:false;
//			}
//			else if (tipoOperacion.equals("MODIFICACION")) {
//				List listaNombresArchivosEnZip = getListaNombresArchivos();
				
//				valido = false;
				//Al menos debe existir uno de los archivos validos dentro del zip
//				Iterator itNombreArchivo = listaNombresArchivosEnZip.iterator();
//				while (itNombreArchivo.hasNext()) {
//					if (archivosDiseno.contains(itNombreArchivo.next())) {
//						valido = true;
//						break;
//					}
//				}
//			}
		}
		
		return valido;
	}

	/**
	 * Verifica que el contenido del archivo sea el correcto para el caso del nuevo esquema.
	 * @param rutaArchivoZip Ruta Fidica del archivo zip a validar
	 * 
	 * @return true si es valido o false de lo contrario
	 */
	public boolean validarExtjs(String rutaArchivoZip, boolean tipo) {
		boolean valido = false;
		//boolean siBaner = false;
		//List archivosDiseno = new ArrayList();
		if (this.claveEpo != null) {
			String claveEpo = this.getClaveEpo();
			List archivosBanner = new ArrayList();

			/*
			 * Estos archivos no se van a validar, por default se van a guardar los predeterminados
			 * 
			archivosDiseno.add("dhtmlxmenu_dhx_nafin.css");
			archivosDiseno.add("estilos.css");
			archivosDiseno.add("dhtmlxmenu_bg.gif");
			*/
			
			//Los siguientes archivos son obligatorios, por lo que deben de estar en el archivo .zip
			archivosBanner.add(claveEpo + ".gif");
			archivosBanner.add("banners.jsp");
			archivosBanner.add("banner1s.jpg");
			archivosBanner.add("banner2s.jpg");
			archivosBanner.add("banner3s.jpg");
			archivosBanner.add("banner4s.jpg");
	
			List listaNombresArchivosEnZip = new ArrayList();
	
			try {
				listaNombresArchivosEnZip = getListaNombresArchivosFS(rutaArchivoZip);
			} catch (IOException e) {
				e.printStackTrace();
			}
			//Verificamos que se encuentren los archivo banner...
			valido = (listaNombresArchivosEnZip.containsAll(archivosBanner))?true:false;
			
			/* Esta forma no se va a validar
			 * 
			if (!tipo){
				primero: for (int x=0;x<archivosDiseno.size();x++){
					String arvo = (String)archivosDiseno.get(x);
					for (int h=0; h<listaNombresArchivosEnZip.size();h++){
						if (arvo.equals((String)listaNombresArchivosEnZip.get(h))){
							valido = true;
							break primero;
						}
					}
				}
			} else{
				valido = (listaNombresArchivosEnZip.containsAll(archivosDiseno))?true:false;
				if (valido){
					String baner = "banners.jsp";
					for (int h=0; h<listaNombresArchivosEnZip.size();h++){
						if (baner.equals((String)listaNombresArchivosEnZip.get(h))){
							siBaner = true;
							break;
						}
					}
					if (siBaner){
						valido = (listaNombresArchivosEnZip.containsAll(archivosBanner))?true:false;
					}
				}
			}*/
		}
		return valido;
	}

	public String toString() {
		return "Clave EPO=" + this.claveEpo + "," +
				"Fecha Modificacion=" + this.fechaModificacion;
	}


	//**************************** Metodos Privados **********************
	
	/**
	 * Obtiene la lista de los nombres de archivos contenidos en el archivo zip
	 * @param fuente Indicador de donde se obtiene el listado de nombres:
	 *  		"ARCHIVO" o "BASE_DATOS"
	 * @param rutaArchivoZip Ruta del archivo Zip. (null si fuente es "BASE_DATOS")
	 * @return Lista con el contenido del archivo zip. Si no hay
	 * archivos regresa una lista vacia, nunca null.
	 */
	private List getListaNombresArchivos(String fuente, String rutaArchivoZip) 
			throws IOException{
		List nombreArchivos = new ArrayList();
		
		AccesoDB con = new AccesoDB();
		InputStream inStream = null;
		
		try {	
			if (fuente.equals("ARCHIVO")) {
				inStream = new FileInputStream(rutaArchivoZip);
			} else if (fuente.equals("BASE_DATOS")) {
				String strSQL = "SELECT bi_archivo_zip " +
						" FROM com_diseno_epo " +
						" WHERE ic_epo = ? ";
				try {
					con.conexionDB();
					PreparedStatement ps = con.queryPrecompilado(strSQL);
					ps.setInt(1, Integer.parseInt(this.claveEpo));
					ResultSet rs = ps.executeQuery();
				
					rs.next();
				
					//java.sql.Blob blob = (java.sql.Blob)rs.getObject("bi_archivo_zip");
					//inStream = ((oracle.sql.BLOB)blob).getBinaryStream();
					inStream = rs.getBinaryStream("bi_archivo_zip");
					rs.close();
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		
			ZipArchiveInputStream zipInStream = new ZipArchiveInputStream(inStream);
			ZipArchiveEntry zipEntry = null;
			byte arrByte[] = new byte[4096];

			while ((zipEntry = zipInStream.getNextZipEntry()) != null) {
				if(zipEntry.isDirectory()) {
					continue; //Ignora Directorios
				}
				nombreArchivos.add(zipEntry.getName());
			}
			zipInStream.close();
			return nombreArchivos;
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene la lista de los nombres de archivos contenidos en el archivo zip
	 * @param rutaArchivoZip Ruta del archivo Zip.
	 * @return Lista con el contenido del archivo zip. Si no hay
	 * archivos regresa una lista vacia, nunca null.
	 */
	private List getListaNombresArchivosFS(String rutaArchivoZip) 
			throws IOException {
		return getListaNombresArchivos("ARCHIVO", rutaArchivoZip);
	}

	/**
	 * Establece las rutas (relativas) )donde deben guardarse 
	 * definitivamente los archivos que componen el dise�o de la EPO.
	 * Este m�todo debe llamarse despu�s
	 * de establecer la clave de la EPO, de lo contrario las rutas no ser�n
	 * validas.
	 *
	 */
	private void setRutas() {
		String claveEpo = this.getClaveEpo();
		rutas = new HashMap();
		rutas.put("ba_lat_" + claveEpo + ".jsp",
				"00archivos/15cadenas/15archcadenas/ba_laterales/");

		rutas.put("ba_" + claveEpo + ".gif",
				"00archivos/15cadenas/15archcadenas/banner/");
		rutas.put("ba1_" + claveEpo + ".gif",
				"00archivos/15cadenas/15archcadenas/banner/");
		rutas.put("ba2_" + claveEpo + ".gif",
				"00archivos/15cadenas/15archcadenas/banner/");
		rutas.put("ba3_" + claveEpo + ".gif",
				"00archivos/15cadenas/15archcadenas/banner/");

		rutas.put(claveEpo + "_home.gif",
				"00archivos/15cadenas/15archcadenas/logos/");
		rutas.put(claveEpo + ".gif",
				"00archivos/15cadenas/15archcadenas/logos/");

		rutas.put("noticias_" + claveEpo + ".jsp",
				"00archivos/15cadenas/15archcadenas/noticias/");
		rutas.put("*.gif",
				"00archivos/15cadenas/15archcadenas/noticias/gif/");

		rutas.put(claveEpo + ".css",
				"14seguridad/Seguridad/css/");
		rutas.put(claveEpo + ".js",
				"14seguridad/Seguridad/css/");
		rutas.put(claveEpo + ".xml",
				"14seguridad/Seguridad/xml/");

		rutas.put("banners.jsp",				 "00utils/css/" + claveEpo + "/");
		rutas.put("estilos.css",				 "00utils/css/" + claveEpo + "/");
		rutas.put("dhtmlxmenu_dhx_nafin.css","00utils/css/" + claveEpo + "/");
		
		rutas.put("banner.jpg",					"00utils/css/" + claveEpo + "/imgs/");
		rutas.put("dhtmlxmenu_bg.gif",		"00utils/css/" + claveEpo + "/imgs/");

	}

	/**
	 * Obtiene la ruta definitiva que corresponde al 
	 * nombre de archivo especificado que es parte del dise�o de la EPO.
	 * Este m�todo debe llamarse despu�s de establecer la clave de la EPO,
	 * de lo contrario las rutas no ser�n validas.
	 *
	 * Las rutas estan establecidas en el Mapa "rutas", 
	 * si el nombre del archivo no se encuentra dentro del mapa, 
	 * realiza lo siguiente: Si es un nombre de archivo con extensi�n gif o jpg
	 * determina la ruta predeterminada para este tipo de archivos que no hayen
	 * coincidencia en "rutas", de lo contrario regresa null
	 * 
	 * @param nombreArchivo Nombre del archivo
	 * @param ruta Base Destino
	 *
	 * @return Cadena con la ruta que corresponde al archivo.
	 *
	 */
	private String getRutaDefinitiva(String nombreArchivo) {
		
		String ruta = null;
		ruta = (String)rutas.get(nombreArchivo);
		nombreArchivo.lastIndexOf(".");
		String extensionArchivo = "";
		String comienzoArchivo	= "";
		
		if (nombreArchivo.lastIndexOf('.') != -1) {
			extensionArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf('.'));
		}
		
		if ( ruta == null && (extensionArchivo.equals(".gif") || extensionArchivo.equals(".jpg") || extensionArchivo.equals(".jsp")) ) {
			if (nombreArchivo.length() > 5){
				comienzoArchivo = nombreArchivo.substring(0,6);
			}
			if (extensionArchivo.equals(".jsp")){
				if (comienzoArchivo.equals("banner")){
					ruta = (String)rutas.get("banners.jsp");
				}else{
					ruta = (String)rutas.get("*.gif");
				}
			}else if (extensionArchivo.equals(".jpg")){
				if (comienzoArchivo.equals("banner")){
					ruta = (String)rutas.get("banner.jpg");
				}else{
					ruta = (String)rutas.get("*.gif");
				}
			}else if (extensionArchivo.equals(".gif")){
				if (comienzoArchivo.equals("dhtmlx")){
					ruta = (String)rutas.get("dhtmlxmenu_bg.gif");
				}else{
					ruta = (String)rutas.get("*.gif");
				}
			}
		}
		return (ruta == null)?null:ruta + nombreArchivo;
	}

	/**
	 * Obtiene la ruta temporal que corresponde al 
	 * nombre de archivo especificado que es parte del dise�o de la EPO.
	 * Este m�todo estable la ruta para la nueva version, antes se debe crear
	 *la carpeta imgs/
	 * 
	 * @param nombreArchivo Nombre del archivo
	 *
	 * @return Cadena con la ruta que corresponde al archivo.
	 *
	 */
	private String getRutaTemporal(String nombreArchivo) {
		
		String ruta = "";
		nombreArchivo.lastIndexOf(".");
		String extensionArchivo = "";
		String comienzoArchivo	= "";
		
		if (nombreArchivo.lastIndexOf('.') != -1) {
			extensionArchivo = nombreArchivo.substring(nombreArchivo.lastIndexOf('.'));
		}
		
		if ( extensionArchivo.equals(".gif") || extensionArchivo.equals(".jpg") || extensionArchivo.equals(".jsp") ) {
			if (nombreArchivo.length() > 5){
				comienzoArchivo = nombreArchivo.substring(0,6);
			}
			if (extensionArchivo.equals(".jsp")){
				if (comienzoArchivo.equals("banner")){
					ruta = "imgs/";
				}else{
					ruta = "";
				}
			}else if (extensionArchivo.equals(".jpg")){
				if (comienzoArchivo.equals("banner")){
					ruta = "imgs/";
				}else{
					ruta = "";
				}
			}else if (extensionArchivo.equals(".gif")){
				if (comienzoArchivo.equals("dhtmlx")){
					ruta = "imgs/";
				}else{
					ruta = "";
				}
			}
		}
		return ruta + nombreArchivo;
	}

	/**
	 * Descomprime los archivos de dise�o de la EPO en ruta especificada
	 * Antes de llamar este m�todo es necesario establecer la clave de la epo
	 *
	 * @param fuente Origen de donde se obtiene el contenido comprimido del dise�o.
	 * 		ARCHIVO � BASE_DATOS
	 * @param modo Finalidad de la descompresi�n de archivos: PREVIEW para vista
	 *  		previa � FINAL para colocar los archivos en su ruta definitiva
	 * @param rutaBase Ruta (Fisica) destino donde se descompacta el dise�o de la Epo
	 * @param rutaArchivoZip Ruta (Fisica) del archivo zip. (Para el caso de que
	 *  		fuente = ARCHIVO, de lo contrario el par�metro es ignorado)
	 * @return String que especifica el nombre del logo para mostrar en la vista previa
	 *  (debido a que si existe XXX_home.gif este ser� el mostrado, 
	 *  de lo contrario XXX.gif)
	 * 		
	 */
	private String descomprimirArchivos(String fuente, String modo, String rutaBase,
			String rutaArchivoZip) throws NafinException {

		AccesoDB con = new AccesoDB();
		InputStream inStream = null;
		String logo = "";
		java.util.Date fechaArchivo = null;
		
		try {	
			String strSQL = "SELECT bi_archivo_zip, " +
					" df_modificacion " +
					" FROM com_diseno_epo " +
					" WHERE ic_epo = ? ";
			
			if (fuente.equals("ARCHIVO")) {
				inStream = new FileInputStream(rutaArchivoZip);
				if (modo.equals("FINAL") && this.existeDisenoEpo()) {
					con.conexionDB();
					PreparedStatement ps = con.queryPrecompilado(strSQL);
					ps.setInt(1, Integer.parseInt(this.claveEpo));
					ResultSet rs = ps.executeQuery();
					rs.next();
					fechaArchivo = rs.getTimestamp("df_modificacion");
					rs.close();
					ps.close();
				}
			} else if (fuente.equals("BASE_DATOS")) {
				con.conexionDB();
				PreparedStatement ps = con.queryPrecompilado(strSQL);
				ps.setInt(1, Integer.parseInt(this.claveEpo));
				ResultSet rs = ps.executeQuery();
			
				rs.next();
			
				fechaArchivo = rs.getTimestamp("df_modificacion");
//				java.sql.Blob blob = (java.sql.Blob)rs.getObject("bi_archivo_zip");
//				inStream = ((oracle.sql.BLOB)blob).getBinaryStream();
				inStream = rs.getBinaryStream("bi_archivo_zip");
				rs.close();
				ps.close();
			}
		
			ZipArchiveInputStream zipInStream = new ZipArchiveInputStream(inStream);
			ZipArchiveEntry zipEntry = null;
			byte arrByte[] = new byte[4096];
			boolean existeLogoAlterno = false;

			while ((zipEntry = zipInStream.getNextZipEntry()) != null) {
				if(zipEntry.isDirectory()) {
					continue; //Ignora Directorios
				}
				if(!existeLogoAlterno) {
					if (zipEntry.getName().equals(this.getClaveEpo() + "_home.gif")) {
						existeLogoAlterno = true;
					}
				}

				String rutaArchivo = null;
				if (modo.equals("FINAL")) {
					rutaArchivo = this.getRutaDefinitiva(zipEntry.getName());
				} else if (modo.equals("PREVIEW")) {
					//rutaArchivo = zipEntry.getName();
					Comunes.crearDirectorios(rutaBase + "imgs/");
					rutaArchivo = this.getRutaTemporal(zipEntry.getName());
				}
				
                                //System.out.println("DisenoEPO::descomprimirArchivos(rutaBase +File.pathSeparator+ rutaArchivo):"+rutaBase +File.separatorChar + rutaArchivo);
				OutputStream outstream = new BufferedOutputStream(new FileOutputStream(rutaBase +File.separatorChar + rutaArchivo));
				int i = 0;
				while((i = zipInStream.read(arrByte, 0, arrByte.length)) != -1) {
					outstream.write(arrByte, 0, i);
				}
				outstream.close();

				//Cuando hay fechaArchivo esa es la que le pone al archivo creado
				if (fechaArchivo != null) {
					File archivo = new File(rutaBase + rutaArchivo);
					if (archivo.exists()){
						archivo.setLastModified(fechaArchivo.getTime());
					}
				}

			}
			zipInStream.close();

			if (existeLogoAlterno) {
				logo = this.getClaveEpo() + "_home.gif";
			} else {
				logo = this.getClaveEpo() + ".gif";
			}
			return logo;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

private BufferedImage images[];

  

	public boolean validaTamanioImg(String rutaImg,int height,int width)	throws Exception	{
	//int usigned
	   File file = new File(rutaImg);
		SimpleImageInfo imagen = new SimpleImageInfo(file);
		if(imagen.getHeight()==height&&imagen.getWidth()==width){
		return true;
		
		}
		return false;
	
	}
	private String claveEpo;
	private boolean borrado;
	private java.util.Date fechaModificacion;
	//Contiene las rutas definitivas donde deben ser almacenados 
	//cada unos de los archivos. Se inicializa al momento de establecer
	//la clave de la epo.
	
	private Map rutas;

}