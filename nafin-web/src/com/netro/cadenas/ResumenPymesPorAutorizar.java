package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ResumenPymesPorAutorizar implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	public ResumenPymesPorAutorizar() {}
	
	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(com.netro.cadenas.ResumenPymesPorAutorizar.class);
	
	HashMap consultaMandante = new HashMap();
	  StringBuffer 	qrySentencia;
	private   List 	conditions;
	private String 	operacion;
	private String 	paginaOffset;
	private String 	paginaNo;
	
	private String txt_nafelec;
	private String nombre_pyme_aux;
  private String ic_if;
	private String chkPymesConvenio;
	private String desde;
	private String hasta;
	private String expediente_digital;
	private String perfil;

	/**
	 * M�todo que obtiene los totales de la consulta
	 * @return ""
	 */
	public String getAggregateCalculationQuery() {
		return "";
	}//getAggregateCalculationQuery

	/**
	 * M�todo que obtiene las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL que contiene las llaves primarias
	 */
	public String getDocumentQuery(){
		log.info("getDocumentQuery (E)");
		qrySentencia 		= new StringBuffer();
		conditions 			= new ArrayList();
		
		qrySentencia.append(  
        " SELECT /*+use_nl(cif pyme cpi cctabanc m epo) index(cctabanc) index(cpi) index(cn)*/  "+
        "        pyme.ic_pyme AS ic_pyme, " + 
        "        m.ic_moneda AS ic_moneda, " +
        "        epo.ic_epo AS ic_epo, " +
        "        cif.ic_if AS ic_if " +
        "   FROM comrel_pyme_if cpi,  " +
        "        comcat_epo epo, " +
        "        comrel_cuenta_bancaria cctabanc, " +
        "        comcat_if cif,       " +
        "        comcat_pyme pyme, " +
        "        comcat_moneda m, " +
        "        comrel_nafin cn " +
        "  WHERE cpi.ic_cuenta_bancaria = cctabanc.ic_cuenta_bancaria  " +
        "    AND cpi.ic_if = cif.ic_if " +
        "    AND cpi.ic_epo = epo.ic_epo " +
        "    AND cctabanc.ic_moneda = m.ic_moneda " +
        "    AND cctabanc.ic_pyme = pyme.ic_pyme   " +
        "    AND cpi.df_autorizacion is null " +
        "    AND cn.ic_epo_pyme_if = pyme.ic_pyme " +
        "	   AND cpi.CS_ESTATUS IS NULL "+
        "    AND cpi.cs_borrado = ? " +
        "    AND cctabanc.cs_borrado = ? " +
        "    AND cn.cg_tipo = ? " +
        "    AND cpi.cs_vobo_if = ? " +
        "    AND cif.ic_if = ? " );
        conditions.add("N");
        conditions.add("N");
        conditions.add("P");
        conditions.add("N");
        conditions.add(ic_if);
        
        if(!txt_nafelec.equals("")){
             qrySentencia.append("   AND cn.ic_nafin_electronico = ? ");
             conditions.add(txt_nafelec);
           }
               
		  if (chkPymesConvenio != null && !"".equals(chkPymesConvenio)) {
             chkPymesConvenio = "S";
             qrySentencia.append("   AND pyme.cs_convenio_unico = ? ");
             conditions.add(chkPymesConvenio);
        } 
           
        if(!desde.equals("") && !hasta.equals("")){
            qrySentencia.append(" AND cpi.df_vobo_if >= TO_DATE(?, 'dd/mm/yyyy') ");
            qrySentencia.append(" AND cpi.df_vobo_if <  TO_DATE(?, 'dd/mm/yyyy')+1 ");
            conditions.add(desde);
            conditions.add(hasta);
          }
        if(!expediente_digital.equals("") ){
             qrySentencia.append("    AND pyme.cs_expediente_efile = ? ");
             conditions.add(expediente_digital);
        } 
		qrySentencia.append(" ORDER BY 1,3,4,2 ");
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions: "+conditions);
		log.info("getDocumentQuery (S)");
		return qrySentencia.toString();
	}//getDocumentQuery
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL que servira para obtener la informaci�n
	 * 	completa de los registros con las llaves primarias especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds (E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append( "select st.numero_sirac, st.nombre_pyme, st.nombre_moneda, st.pyme_rfc, st.num_naf_elect, " +
			 "	st.fecha_param, st.nombre_epo, st.expediente_disponible, nvl(vp.CS_DOC_VIGENTES,'N') as CS_DOC_VIGENTES , " +
			"  Decode(nvl(vp.CS_DOC_VIGENTES,'N'),'','','S','SI') as PRIORIDAD "+
			 " from vm_pymes_doc_vig vp,( "+
          " SELECT /*+use_nl(cif pyme cpi cctabanc m epo) index(cctabanc) index(cpi) index(cn)*/  "+
          "        pyme.in_numero_sirac AS numero_sirac, " + 
          "        pyme.cg_razon_social AS nombre_pyme, " +
          "        m.cd_nombre AS nombre_moneda, " +
          "        pyme.cg_rfc AS pyme_rfc, " +
          "        cn.ic_nafin_electronico AS num_naf_elect, " +
          "        TO_CHAR(cpi.df_vobo_if, 'dd/mm/yyyy') AS fecha_param, " +
          "        epo.cg_razon_social AS nombre_epo, " +
          "        decode(pyme.cs_expediente_efile, 'S', 'SI', 'N', 'NO') as expediente_disponible, " +
			 "			 pyme.ic_pyme as ic_pyme, " +
			 "        epo.ic_epo as ic_epo "+
          "   FROM comrel_pyme_if cpi,  " +
          "        comcat_epo epo, " +
          "        comrel_cuenta_bancaria cctabanc, " +
          "        comcat_if cif,       " +
          "        comcat_pyme pyme, " +
          "        comcat_moneda m, " +
          "        comrel_nafin cn " +
          "  WHERE cpi.ic_cuenta_bancaria = cctabanc.ic_cuenta_bancaria  " +
          "    AND cpi.ic_if = cif.ic_if " +
          "    AND cpi.ic_epo = epo.ic_epo  " +
          "    AND cctabanc.ic_moneda = m.ic_moneda " +
          "    AND cctabanc.ic_pyme = pyme.ic_pyme   " +
          "    AND cpi.df_autorizacion is null " +
          "    AND cn.ic_epo_pyme_if = pyme.ic_pyme " +
          "	   AND cpi.CS_ESTATUS IS NULL "+
          "    AND cpi.cs_borrado = ? " +
          "    AND cctabanc.cs_borrado = ? " +
          "    AND cn.cg_tipo = ? " +
          "    AND cpi.cs_vobo_if = ? " +
          "    AND cif.ic_if = ? " +
          "    AND ( ");
          conditions.add("N");
          conditions.add("N");
          conditions.add("P");
          conditions.add("N");
          conditions.add(ic_if);
									
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
				if(i>0){
					qrySentencia.append(" OR ");
				}
				qrySentencia.append( " (    pyme.ic_pyme = ? "+
                             "  AND m.ic_moneda = ? "+  
                             "  AND epo.ic_epo = ? "+
                             "  AND cif.ic_if = ? )" ); 
					conditions.add(new Long(lItem.get(0).toString()));
					conditions.add(new Long(lItem.get(1).toString()));
					conditions.add(new Long(lItem.get(2).toString()));
          conditions.add(new Long(lItem.get(3).toString()));
					
		}//for(int i=0;i<ids.size();i++)
		
		qrySentencia.append(")");
		qrySentencia.append(") st " +
					"where st.ic_pyme = vp.ic_pyme(+) " +
					"and st.ic_epo = vp.ic_epo(+) ");

		qrySentencia.append(" ORDER BY st.nombre_pyme ");
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds (S)");
		return qrySentencia.toString();
	}//getDocumentSummaryQueryForIds	


	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile (E)");
		qrySentencia 		= new StringBuffer();
		conditions 			= new ArrayList();
			
		qrySentencia.append( "select st.EPO, st.numero_sirac, st.Fecha_Firma_Convenio_Unico, st.cuenta_clabe, "+
			 " st.MONEDA, st.PYME, st.RFC, st.NUM_NAFIN_ELECTRONICO, " +
			 "	st.FECHA_PARAMETRIZACION,  st.EXPEDIENTE_DISPONIBLE, Decode(nvl(vp.CS_DOC_VIGENTES,'N'),'N','','S','SI') as PRIORIDAD  " +
			 "	FROM vm_pymes_doc_vig vp,( "+
          " SELECT /*+use_nl(cif pyme cpi cctabanc m epo) index(cctabanc) index(cpi) index(cn)*/ "+
          "        pyme.in_numero_sirac AS NUMERO_SIRAC, " +
          "        pyme.cg_razon_social AS PYME, " +
          "        m.cd_nombre AS MONEDA, " +
          "        pyme.cg_rfc AS RFC, " +
          "        cn.ic_nafin_electronico AS NUM_NAFIN_ELECTRONICO, " +
          "        TO_CHAR(cpi.df_vobo_if, 'dd/mm/yyyy') AS FECHA_PARAMETRIZACION, " +
          "        epo.cg_razon_social AS EPO, " +
          "        decode(pyme.cs_expediente_efile, 'S', 'SI', 'N', 'NO') as EXPEDIENTE_DISPONIBLE, " +
			 "			 pyme.ic_pyme as ic_pyme, " +
			 "        epo.ic_epo as ic_epo, "+
			 "			 to_char(pyme.df_firma_conv_unico,'dd/mm/yyyy') as Fecha_Firma_Convenio_Unico, "+
			 "			 cctabanc.cg_cuenta_clabe as cuenta_clabe "+
          "   FROM comrel_pyme_if cpi,  " +
          "        comcat_epo epo, " +
          "        comrel_cuenta_bancaria cctabanc, " +
          "        comcat_if cif,       " +
          "        comcat_pyme pyme, " +
          "        comcat_moneda m, " +
          "        comrel_nafin cn " +
          "  WHERE cpi.ic_cuenta_bancaria = cctabanc.ic_cuenta_bancaria  " +
          "    AND cpi.ic_if = cif.ic_if " +
          "    AND cpi.ic_epo = epo.ic_epo  " +
          "    AND cctabanc.ic_moneda = m.ic_moneda " +
          "    AND cctabanc.ic_pyme = pyme.ic_pyme   " +
          "    AND cpi.df_autorizacion is null " +
          "    AND cn.ic_epo_pyme_if = pyme.ic_pyme " +
          "	   AND cpi.CS_ESTATUS IS NULL "+
          "    AND cpi.cs_borrado = ? " +
          "    AND cctabanc.cs_borrado = ? " +
          "    AND cn.cg_tipo = ? " +
          "    AND cpi.cs_vobo_if = ? " +
          "    AND cif.ic_if = ? " );
          conditions.add("N");
          conditions.add("N");
					conditions.add("P");
          conditions.add("N");
          conditions.add(ic_if);
		
        if(!txt_nafelec.equals("")){
             qrySentencia.append("   AND cn.ic_nafin_electronico = ? ");
             conditions.add(txt_nafelec);
           }
        
        if (chkPymesConvenio != null && !"".equals(chkPymesConvenio)) {
             chkPymesConvenio = "S";
             qrySentencia.append("   AND pyme.cs_convenio_unico = ? ");
             conditions.add(chkPymesConvenio);
        } 
           
        if(!desde.equals("") && !hasta.equals("")){
            qrySentencia.append(" AND cpi.df_vobo_if >= TO_DATE(?, 'dd/mm/yyyy') ");
            qrySentencia.append(" AND cpi.df_vobo_if < TO_DATE(?, 'dd/mm/yyyy')+1 ");
            conditions.add(desde);
            conditions.add(hasta);
          }
        if(!expediente_digital.equals("") ){
             qrySentencia.append("    AND pyme.cs_expediente_efile = ? ");
             conditions.add(expediente_digital);
        } 
          
		qrySentencia.append(") st " +
				"where st.ic_pyme = vp.ic_pyme(+) " +
				"and st.ic_epo = vp.ic_epo(+) ");

		qrySentencia.append(" ORDER BY st.PYME ");
		log.debug("qrySentencia: "+qrySentencia);
		log.debug("conditions: "+conditions);
		log.info("getDocumentQueryFile (S)");
		return qrySentencia.toString();	
	}//getDocumentQueryFile


public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString()+"", 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Resumen de Pymes  por Autorizar ","font2",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
				pdfDoc.setTable(9,100);								
				pdfDoc.setCell("N�mero SIRAC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�m Nafin Electr�nico","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Parametrizaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				if(perfil.equals("CONSUL IF")) {
				pdfDoc.setCell("Expediente (1)","celda01",ComunesPDF.CENTER);
				}else {
				pdfDoc.setCell("Expediente Disponible (1)","celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setCell("Prioridad","celda01",ComunesPDF.CENTER);
			
				 while(rs.next()){
					
					String num_sirac 		= rs.getString("NUMERO_SIRAC")==null?"":rs.getString("NUMERO_SIRAC");
					String nombre_pyme 		= rs.getString("NOMBRE_PYME")==null?"":rs.getString("NOMBRE_PYME");
					String moneda 		= rs.getString("NOMBRE_MONEDA")==null?"":rs.getString("NOMBRE_MONEDA");
					String rfc 		= rs.getString("PYME_RFC")==null?"":rs.getString("PYME_RFC");
					String num_nafin 		= rs.getString("NUM_NAF_ELECT")==null?"":rs.getString("NUM_NAF_ELECT");
					String fecha_param 		= rs.getString("FECHA_PARAM")==null?"":rs.getString("FECHA_PARAM");
					String nombreEPo 		= rs.getString("NOMBRE_EPO")==null?"":rs.getString("NOMBRE_EPO");
					String expediente 		= rs.getString("EXPEDIENTE_DISPONIBLE")==null?"":rs.getString("EXPEDIENTE_DISPONIBLE");
					String prioridad 		= rs.getString("PRIORIDAD")==null?"":rs.getString("PRIORIDAD");
					
					
					pdfDoc.setCell(num_sirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(num_nafin,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_param,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreEPo,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(expediente,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(prioridad,"formas",ComunesPDF.CENTER);
				}
		
				pdfDoc.addTable();
				pdfDoc.endDocument();	
				
			}
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
			
		try {
		
			if(tipo.equals("CSV") ) {
			
				contenidoArchivo.append("EPO, N�MERO SIRAC,	FECHA FIRMA CONVENIO �NICO,	CUENTA CLABE,	MONEDA,	PYME,	RFC,	N�M. NAFIN ELECTR�NICO,	FECHA PARAMETRIZACI�N,	EXPEDIENTE DISPONIBLE,	PRIORIDAD \n");
	
				while(rs.next()){
					
					String nombre_epo 		= rs.getString("EPO")==null?"":rs.getString("EPO");
					String num_sirac 		= rs.getString("NUMERO_SIRAC")==null?"":rs.getString("NUMERO_SIRAC");
					String pyme 		= rs.getString("PYME")==null?"":rs.getString("PYME");
					String Fecha_Firma_Convenio_Unico 		= rs.getString("Fecha_Firma_Convenio_Unico")==null?"":rs.getString("Fecha_Firma_Convenio_Unico");
					String cuenta_clabe 		= rs.getString("cuenta_clabe")==null?"":rs.getString("cuenta_clabe");
					String moneda 		= rs.getString("MONEDA")==null?"":rs.getString("MONEDA");
					String RFC 		= rs.getString("RFC")==null?"":rs.getString("RFC");
					String num_nafin 		= rs.getString("NUM_NAFIN_ELECTRONICO")==null?"":rs.getString("NUM_NAFIN_ELECTRONICO");
					String fecha_param 		= rs.getString("FECHA_PARAMETRIZACION")==null?"":rs.getString("FECHA_PARAMETRIZACION");
					String expediente 		= rs.getString("EXPEDIENTE_DISPONIBLE")==null?"":rs.getString("EXPEDIENTE_DISPONIBLE");
					String prioridad 		= rs.getString("PRIORIDAD")==null?"":rs.getString("PRIORIDAD");
			
					contenidoArchivo.append(nombre_epo.replaceAll(",","")+","+
												num_sirac.replaceAll(",","")+","+												
												Fecha_Firma_Convenio_Unico.replaceAll(",","")+","+
												cuenta_clabe.replaceAll(",","")+","+
												moneda.replaceAll(",","")+","+
												pyme.replaceAll(",","")+","+
												RFC.replaceAll(",","")+","+
												num_nafin.replaceAll(",","")+","+
												fecha_param.replaceAll(",","")+","+
												expediente.replaceAll(",","")+","+
												prioridad.replaceAll(",","")+"\n");
				}
	
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	

	//GETERS
	public List getConditions() { return conditions; }
	public String getOperacion() { return operacion; }
	public String getPaginaNo() {	return paginaNo; }
	public String getPaginaOffset() { return paginaOffset; }
	public String getTxt_nafelec() { return txt_nafelec; }
	public String getNombre_pyme_aux() { return nombre_pyme_aux; }
	public String getChkPymesConvenio() { return chkPymesConvenio; }
	public String getDesde() { return desde; }
	public String getHasta() { return hasta; }
	public String getIc_if() { return ic_if; }
	public String getExpediente_digital() { return expediente_digital; }
  
	
	//SETERS
	public void setOperacion(String newOperacion) { operacion = newOperacion; }
	public void setPaginaNo(String newPaginaNo) { paginaNo = newPaginaNo; }
	public void setPaginaOffset(String newPaginaOffset) { paginaOffset = newPaginaOffset; }
	public void setTxt_nafelec(String txt_nafelec) { this.txt_nafelec = txt_nafelec; }
	public void setNombre_pyme_aux(String nombre_pyme_aux) { this.nombre_pyme_aux = nombre_pyme_aux; }
	public void setChkPymesConvenio(String chkPymesConvenio) { this.chkPymesConvenio = chkPymesConvenio; }
	public void setDesde(String desde) { this.desde = desde; }
	public void setHasta(String hasta) { this.hasta = hasta; }
  public void setIc_if(String ic_if) { this.ic_if = ic_if; }
  public void setExpediente_digital(String expediente_digital) { this.expediente_digital = expediente_digital; }

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}


}
