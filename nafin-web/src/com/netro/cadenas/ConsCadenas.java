package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCadenas  implements IQueryGeneratorRegExtJS    {
	
		private final static Log log = ServiceLocator.getInstance().getLog(ConsCadenas.class);
		String no_epo;
		String ic_moneda;
		String ic_producto;
		String tipoCartera;
		String tipo_plazo;
		
		private List 		conditions;
		StringBuffer qrySentencia = new StringBuffer("");
	private String tipoPago;
	
		public ConsCadenas() {}

		public String getDocumentQuery() {
			log.info("getDocumentQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentQuery(S)");
			return qrySentencia.toString();
		}
		
		
		public String getDocumentSummaryQueryForIds(List ids){
			log.info("getDocumentSummaryQueryForIds(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentSummaryQueryForIds(S)");
			return qrySentencia.toString();
		}
	
		
		public String getAggregateCalculationQuery(){
			log.info("getAggregateCalculationQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getAggregateCalculationQuery(S)");
			return qrySentencia.toString();	
		}
		
		public List getConditions(){
			return conditions;
		}
		
	
		public String getDocumentQueryFile(){
			log.info("getDocumentQueryFile(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			qrySentencia.append(  " select BO.ic_base_operacion as baseoperacion, E.cg_razon_social as razonsocial, "+ 
					" BO.ig_codigo_base as codigobase, BO.ig_plazo_minimo as plazominimo, BO.ig_plazo_maximo as plazomaximo, "+ 
					" CBO.cg_descripcion as descripcion, CPN.ic_nombre as nombre"+
					", DECODE(BO.cs_recurso,'S','Pyme','N','Epo') as recurso "+
					", M.CD_NOMBRE as moneda, BO.cs_tipo_plazo as tipoplazo, BO.ig_tipo_cartera as tipocartera"+
					", '/15nafin/15basesoperc2.jsp' as pantalla " +
					" ,DECODE(BO.ig_tipo_pago,'1','Financiamiento con intereses','2','Meses sin Intereses ') as TIPOPAGO "+					
					
					" from comrel_base_operacion BO, comcat_base_operacion CBO, "+
					" comcat_epo E, comcat_producto_nafin CPN, comcat_moneda M "+
					" where BO.ig_codigo_base = CBO.ig_codigo_base "+
					" and BO.ic_epo = E.ic_epo "+
					" and BO.ic_producto_nafin = CPN.ic_producto_nafin"+
					" and BO.ic_moneda = M.ic_moneda"+
					((!ic_moneda.equals(""))?" and M.ic_moneda = " + ic_moneda :"")+
					((!ic_moneda.equals(""))?" and BO.ic_moneda = " + ic_moneda :"")+
					" and BO.ic_epo = " + no_epo +
					((!ic_producto.equals(""))?" and BO.ic_producto_nafin = "+ ic_producto:"")+
					((!tipoCartera.equals(""))?" and BO.ig_tipo_cartera = " + tipoCartera:"")+
					((!tipo_plazo.equals("")) ?" and cs_tipo_plazo = '"+tipo_plazo+"'":"")+
					//(ic_producto.equals("1")||ic_producto.equals("5")?" and BO.cs_tipo_plazo = '"+tipo_plazo+"'":"")+
					" and (CPN.ic_producto_nafin = 1 or CPN.ic_producto_nafin = 4 )"+
					
					((!tipoPago.equals(""))?" and BO.ig_tipo_pago = "+ tipoPago:"")+ 
					
					" order by E.cg_razon_social,CPN.ic_nombre"	);
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentQueryFile(S)");
			return qrySentencia.toString();
		}
		
		
		public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
			log.debug("crearCustomFile (E)");
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
			StringBuffer contenidoArchivo = new StringBuffer("");
		
					
			try {
				if ("PDF".equals(tipo)) {

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico") == null ? "" : session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				if(ic_producto.equals("1")){
					pdfDoc.setTable(10,90);
				}else{
					pdfDoc.setTable(10,100);
				}
				pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Producto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Cartera","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Pago","celda01",ComunesPDF.CENTER);
				
				if(ic_producto.equals("1")){
					pdfDoc.setCell("Tipo Operaci�n","celda01",ComunesPDF.CENTER);	
				}
				pdfDoc.setCell("Plazo M�nimo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo M�ximo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("C�digo Base","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Descripci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Acreditado","celda01",ComunesPDF.CENTER);				
							
				while (rs.next()) {
					String operacion = "";
					String nombreEpo 		= rs.getString("RAZONSOCIAL")==null?"":rs.getString("RAZONSOCIAL");
					String moneda 		= rs.getString("MONEDA")==null?"":rs.getString("MONEDA");
					String producto 		= rs.getString("NOMBRE")==null?"":rs.getString("NOMBRE");
					String cartera 		= rs.getString("TIPOCARTERA")==null?"":rs.getString("TIPOCARTERA");
					
					if(ic_producto.equals("1")){
						operacion = rs.getString("TIPOPLAZO")==null?"":rs.getString("TIPOPLAZO");
					}
					
					String plazomin 	= rs.getString("PLAZOMINIMO")==null?"":rs.getString("PLAZOMINIMO");
					String plazomax 		= rs.getString("PLAZOMAXIMO")==null?"":rs.getString("PLAZOMAXIMO");
					String codigobase 		= rs.getString("CODIGOBASE")==null?"":rs.getString("CODIGOBASE");
					String descripcion 		= rs.getString("DESCRIPCION")==null?"":rs.getString("DESCRIPCION");
					String acreditado 		= rs.getString("RECURSO")==null?"":rs.getString("RECURSO");
					String tipoPago 		= rs.getString("TIPOPAGO")==null?"":rs.getString("TIPOPAGO");

					String renplazo ="";

					if(ic_producto.equals("1")){
						if(operacion.equals("F")){
							renplazo = "Factoraje";
						}else if (operacion.equals("C")){
							renplazo = "Cobranza";
						}else if(operacion.equals("O")){
							renplazo = "Factoraje Obra P�blica";
						}
					}
			
					String rencartera="";
					
					if(cartera.equals("1")){
							rencartera = "1er Piso";
					}else{
							rencartera = "2do Piso";
					}
					
					 
					pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(producto,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(rencartera,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoPago,"formas",ComunesPDF.CENTER);
					
					if(ic_producto.equals("1"))
						pdfDoc.setCell(renplazo,"formas",ComunesPDF.CENTER);
						
					pdfDoc.setCell(plazomin,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazomax,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(codigobase,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(descripcion,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(acreditado,"formas",ComunesPDF.CENTER);			
				}
				
				pdfDoc.addTable();
				pdfDoc.endDocument();	
		}

		if(tipo.equals("CSV") ) {	
			contenidoArchivo.append("Nombre EPO,Moneda,Producto,Tipo de Cartera, Tipo de Pago,  ");
			
			if(ic_producto.equals("1"))
				contenidoArchivo.append("Tipo Operaci�n,");
			
			contenidoArchivo.append("Plazo M�nimo,Plazo M�ximo,C�digo Base,Descripci�n,Acreditado\n");
			
			while(rs.next()) {
				contenidoArchivo.append(rs.getString(2).replace(',',' ')+","+rs.getString(9)+","+rs.getString(7).replace(',',' ')+" ");
				if(ic_producto.equals("5")){					
					contenidoArchivo.append((rs.getString("tipoplazo")==null?"":(rs.getString("tipoplazo").equals("E")?"Cr�dito de Exportaci�n":"Tradicional")));				
				}	;
				contenidoArchivo.append(","+(rs.getString("tipocartera").equals("1")?"1er Piso":"2do Piso")+",");
				
				contenidoArchivo.append ( rs.getString("TIPOPAGO")==null?"":rs.getString("TIPOPAGO")+"," );
				
				
				if(ic_producto.equals("1")) {
					if(rs.getString("tipoplazo").equals("F")) 	{
						contenidoArchivo.append("Factoraje,");
					} else if (rs.getString("tipoplazo").equals("C")) {  
					  contenidoArchivo.append("Cobranza,");
					} else if (rs.getString("tipoplazo").equals("O")) {
					  contenidoArchivo.append("Factoraje Obra P�blica,"); // Fodea 045-2009  Fondeo BANOBRAS
					}
				} 
					
				contenidoArchivo.append(rs.getString(4)+","+rs.getString(5)+","+rs.getString(3)+","+rs.getString(6).replace(',',' ')+","+rs.getString("recurso")+ "\n");
			} // while 
			
			CreaArchivo archivo = new CreaArchivo();
			if (archivo.make(contenidoArchivo.toString(), path, ".csv"))
				nombreArchivo = archivo.getNombre();
			}
		
		}catch(Exception e) { 
				log.error("Error en la generacion del Archivo CSV: " + e);
		}	

			return nombreArchivo;		 				 				 		
		}
				 
					
		public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
		
			CreaArchivo creaArchivo = new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer();
		
			try {
		
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally {
			try {
				
			} catch(Exception e) {}
		  
			}
				return nombreArchivo;
		}


/************************SETTERS**********************/
		public void setEpo(String no_epo){
			this.no_epo = no_epo;
		}
	
		public void setMoneda(String ic_moneda){
			this.ic_moneda = ic_moneda;
		}	

		public void setProducto(String ic_producto){
			this.ic_producto = ic_producto;
		}		

		public void setCartera(String tipoCartera){
			this.tipoCartera = tipoCartera;
		}

		public void setPlazo(String tipo_plazo){
			this.tipo_plazo = tipo_plazo;
		}

	public String getTipoPago() {
		return tipoPago;
	}
 
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

}//Fin Clase
